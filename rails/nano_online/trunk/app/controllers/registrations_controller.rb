class RegistrationsController < ApplicationController
  def create
    @customer = Customer.new(params[:customer])

    if @customer.save
      # flash[:notice] = 'Registration created'
      login_and_redirect_customer @customer, :notice => 'Registration created'
    else
      flash[:notice] = 'Error creating account'
      render :new
    end
  end  
end