class CustomersController < ApplicationController
  require 'big_endian_types'
  require 'little_endian_types'

  before_filter :only => [ :index, :export_to_csv ] do |controller|
    controller.authorize :administrator, :imuzdb
  end

  EVERYONE_ACCESS = [ :login, :add, :refresh, :import, :remember_password, :revalidate ]
  CUSTOMER_ACCESS = [ :profile, :edit_profile, :update_profile, :change_password, :show ]

  before_filter :authorize_admin, :except => EVERYONE_ACCESS + CUSTOMER_ACCESS + [:index, :export_to_csv]
  before_filter :authorize_nano, :only => EVERYONE_ACCESS
  before_filter :log_access, :only => EVERYONE_ACCESS
  before_filter :session_expiry, :except => EVERYONE_ACCESS

  layout :set_layout

  def index
    @partner = current_user.partner.name if current_user.partner
    
    if @partner == "imuz_db"
      app = App.find_by_name_short(ImuzDb::GameSession::SHORT_NAME)
      @customers = app.customers.page(params[:page]).per(10)
    else
      @customers = unless params[:filter].blank?
        @filter = params[:filter]
        p = "%#{params[:filter]}%"
        Customer.where(["nickname like ? or email like ? or first_name like ? or last_name like ?", p, p, p, p]).order(:id).page params[:page]
      else
        Customer.order(:id).page params[:page]
      end
    end
    
    respond_to do |format|
      format.html
      format.csv do
        app = App.find_by_name_short(ImuzDb::GameSession::SHORT_NAME)
        csv = Customer.export_to_csv_by_app(app)
        send_data csv, :filename => "users.csv"
      end
    end
  end

  def show
    @customer = Customer.find(params[:id] || session[:user_id])
  end


  ##
  # Método utilizado para validar uma dupla usuário/senha enviado a partir de um
  # aplicativo Nano.
  # POST /customers/login
  ##
  def login( return_user_info = false )
    nickname = ""
    password = ""
    timestamp = DateTime.new
    parse_data( params[ ID_SPECIFIC_DATA ] ) do |stream, id|
      case id
        when PARAM_LOGIN_NICKNAME
          nickname = stream.read_string
        when PARAM_LOGIN_PASSWORD
          password = stream.read_string
        when PARAM_REGISTER_TIMESTAMP
          timestamp = stream.read_datetime
      end
    end
    
    customer = Customer.find_by_nickname( nickname )
    
    if customer
      if customer.check_password( password )
        # usuário autenticado com sucesso
        render_binary do |out|
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_OK )
          out.write_byte( ID_CUSTOMER_ID )
          out.write_int( customer.id )
          
          out.write_byte( ID_SPECIFIC_DATA )
          # só retorna as informações do usuário se forem mais recentes que a última
          # atualização informada, ou então for um import novo no cliente (ou seja,
          # timestamp tem um valor mínimo)
          if ( return_user_info && timestamp < customer.updated_at )
            customer.write_data( out, params )
          else
            out.write_byte( PARAM_REGISTER_INFO_END )
          end
        end
      else
        # usuário encontrado, porém a senha não confere
        render_binary do |out|
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_ERROR_LOGIN_PASSWORD_INVALID )
        end
      end
    else
      # apelido não encontrado
      render_binary do |out|
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_ERROR_LOGIN_NICKNAME_NOT_FOUND )
      end
    end
  end

  def import
    login true
  end


  def new
    @customer = Customer.new
  end


  def edit
    @customer = Customer.find(params[:id])
  end


  # POST /customers
  # POST /customers.xml
  def create
    # TODO o que fazer no caso de não detectar o aparelho? pode significar que
    # está sendo acessado por um pc (administrador), por um aparelho não suportado,
    # ou ainda por alguém tentando hackear o sistema

    @customer = Customer.new(params[:customer])

    respond_to do |format|
      if @customer.save
        flash[:notice] = "Customer \##{ @customer.id } was successfully created."
        format.html { redirect_to(@customer) }
        format.xml  { render :xml => @customer, :status => :created, :location => @customer }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @customer.errors, :status => :unprocessable_entity }
      end
    end
  end

  # Registra um novo usuário a partir de um aplicativo Nano.
  def add
   # @customer, fb_profile = Customer.filter_new( params, session[ 'nano_client' ] )
   @customer = Customer.filter_new( params, session[ 'nano_client' ] )

    render_binary do |out|
      begin
        @customer.save!
        # @customer.send_email_validation

        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_OK )

        # marca o início dos dados específicos (no caso, o id do usuário)
        out.write_byte( ID_SPECIFIC_DATA )

        if ( params[ ID_NANO_ONLINE_VERSION ] < '0.0.3' )
          out.write_byte( ID_CUSTOMER_ID )
          out.write_int( @customer.id )
        else
          out.write_byte( PARAM_REGISTER_CUSTOMER_ID )
          out.write_int( @customer.id )
          out.write_byte( PARAM_REGISTER_TIMESTAMP )
          out.write_datetime( @customer.updated_at )

          if ( params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31' )
            out.write_byte( PARAM_REGISTER_VALIDATE_BEFORE )
            out.write_datetime( @customer.validate_before )
            out.write_byte( PARAM_REGISTER_VALIDATED_AT )
            out.write_datetime( @customer.validated_at )
          end

          out.write_byte( PARAM_REGISTER_INFO_END )
        end
      rescue Exception => e
        out.write_errors( @customer )
        logger.info e.backtrace.join("\n")
      end
    end
  end


  ##
  # Atualiza as informações de um usuário a partir de um aplicativo Nano.
  ##
  def refresh
    render_binary do |out|
      begin
        update_ok, customer = Customer.update_binary( params, session[ 'nano_client' ] )

        if update_ok
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_OK )

          # marca o início dos dados específicos (no caso, o id do usuário)
          out.write_byte( ID_SPECIFIC_DATA )
          out.write_byte( ID_CUSTOMER_ID )
          out.write_int( customer.id )
        else
          out.write_errors( customer )
        end
      rescue Exception => e
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_SERVER_INTERNAL_ERROR )
      end
    end
  end


  ##
  # Reenvia o e-mail de validação de e-mail de um usuário a partir de um aplicativo Nano.
  ##
  def revalidate
    customer = nil
    parse_data( params[ ::ID_SPECIFIC_DATA ] ) do |stream, id|
      case id
        when PARAM_REGISTER_CUSTOMER_ID
          customer = Customer.find( stream.read_int() )
      end
    end

    render_binary do |out|
      if customer
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_OK )

        out.write_byte( ID_SPECIFIC_DATA )
        out.write_byte( PARAM_REGISTER_EMAIL )
        out.write_string( customer.email )
        out.write_byte( PARAM_REGISTER_INFO_END )
      else
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_ERROR_LOGIN_NICKNAME_NOT_FOUND )
      end
    end
  end


  ##
  # Envia para o e-mail cadastrado do usuário a senha.
  ##
  def remember_password()
    render_binary() { |out|
      # begin
        nickname = nil
        parse_data( params[ ::ID_SPECIFIC_DATA ] ) { |stream, id|
          case id
            when PARAM_LOGIN_NICKNAME
              nickname = stream.read_string()
          end
        }

        if ( nickname )
          customer = Customer.find_by_nickname( nickname ) || Customer.find_by_email( nickname )

          if ( customer )
            # envia e-mail para o sujeito contendo a senha
            customer.reset_password()

            out.write_byte( ID_RETURN_CODE )
            out.write_short( RC_OK )

            out.write_byte( ::ID_SPECIFIC_DATA )
            out.write_byte( ::PARAM_REGISTER_EMAIL )
            out.write_utf( customer.email )

            out.write_byte( PARAM_REGISTER_INFO_END )
          else
            out.write_byte( ID_RETURN_CODE )
            out.write_short( RC_ERROR_LOGIN_NICKNAME_NOT_FOUND )
          end
        else
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_ERROR_LOGIN_NICKNAME_NOT_FOUND )
        end
      # rescue Exception => e
      #     out.write_byte( ID_RETURN_CODE )
      #     out.write_short( RC_SERVER_INTERNAL_ERROR )

      #     raise e
      # end
    }
  end


  # PUT /customers/1
  # PUT /customers/1.xml
  def update
    @customer = Customer.find(params[:id])

    respond_to do |format|
      if ( @customer.update_attributes( params[ :customer ] ) )
        flash[:notice] = "Customer \##{ @customer.id } was successfully updated."
        format.html { redirect_to :controller => :customers }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @customer.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /customers/1
  # DELETE /customers/1.xml
  def destroy
    @customer = Customer.find(params[:id])
    @customer.destroy

    respond_to do |format|
      flash[:notice] = "Customer \##{ @customer.id } was successfully destroyed."
      format.html { redirect_to(customers_url) }
      format.xml  { head :ok }
    end
  end


  def profile
    @customer = Customer.find(session[:customer_id])
  end


  def edit_profile
      @customer = Customer.find(session[:customer_id])
  end


  def update_profile
    @customer = Customer.find(session[:customer_id])
    email_antigo = @customer.email

    if @customer.update_attributes(params[:customer])
      email_novo = @customer.email

      unless email_novo == email_antigo
        @customer.validated_at = nil
        @customer.validation_hash = create_validation_hash( @customer )

        #TODO fazer rollback do update_attributes
        unless @customer.save
          flash[:notice] = "Falha na atualizacao do perfil."
          redirect_to :action => :edit_profile
          return
        end

        # @customer.send_email_validation
        flash[:notice] = "Perfil atualizado. E-mail de validacao enviado."
        redirect_to :action => :profile
        return
      end
      flash[:notice] = "Perfil atualizado"
      redirect_to :action => :profile
    else
      flash[:notice] = "Falha na atualizacao do perfil."
      redirect_to :action => :edit_profile
    end
  end

  def change_password
    if ( request.post? )
      customer = Customer.find( session[ :customer_id ] )

      # o password_confirmation não está funcionando no form_tags
      if ( params[ :password ] == params[ :password_confirmation ] )
        if ( customer.hashed_password == Customer.encrypted_password( params[:current_password], customer.salt ) )
          customer.password = params[ :password ]

          if ( customer.save )
              flash[ :notice ] = "Password changed sucessfully."
              redirect_to :action => :profile
          else
            flash[ :notice ] = "Password could not be changed."
            redirect_to :action => :change_password
          end
        else
          flash[ :notice ] = "Wrong password."
        end
      else
        flash[ :notice ] = "Password and Confirmation didn't match."
      end
    else
      @customer = Customer.new
    end
  end
end

