class UnitFormatsController < ApplicationController
  layout :set_layout

  def index
    @unit_formats = UnitFormat.page params[:page]
  end

  def new
    @unit_format = UnitFormat.new
  end

  def create
    @unit_format = UnitFormat.new(params[:unit_format])

    if @unit_format.save
      flash[:notice] = 'Unit Format was successfully created.'
      redirect_to unit_formats_path
    else
      render :action => "new"
    end
  end

  def edit
    @unit_format = UnitFormat.find(params[:id])
  end

  def update
    @unit_format = UnitFormat.find(params[:id])

    if @unit_format.update_attributes(params[:unit_format])
      flash[:notice] = 'Unit Format was successfully updated.'
      redirect_to unit_formats_path
    else
      render :action => "edit"
    end
  end

  def destroy
    @unit_format = UnitFormat.find(params[:id])
    @unit_format.destroy

    redirect_to(unit_formats_url)
  end

end
