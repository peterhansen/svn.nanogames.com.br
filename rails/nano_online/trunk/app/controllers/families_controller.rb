class FamiliesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @families = Family.order(:name).page params[:page]
  end

  def show
    unless @family = Family.find_by_id(params[:id])
      redirect_to families_path, :notice => 'Invalid family'
    end
  end

  def new
    @family = Family.new
  end

  def edit
    @family = Family.find(params[:id])
  end

  def create
    @family = Family.new(params[:family])

    if @family.save
      redirect_to @family, :notice => "Family #{ @family.name } was successfully created."
    else
      render :new
    end
  end

  def update
    @family = Family.find(params[:id])

    if @family.update_attributes(params[:family])
      redirect_to @family, :notice => "Family #{ @family.name } was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @family = Family.find(params[:id])
    @family.destroy
    redirect_to families_url, :notice => "Family #{ @family.name } was successfully destroyed."
  end
end
