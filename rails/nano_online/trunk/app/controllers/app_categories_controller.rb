class AppCategoriesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @app_categories = AppCategory.order(:name).page params[:page]
  end

  def new
    @app_category = AppCategory.new
  end

  def edit
    @app_category = AppCategory.find(params[:id])
  end

  def create
    @app_category = AppCategory.new(params[:app_category])

    if @app_category.save
      flash[:notice] = "App category #{ @app_category.name } was successfully created."
      redirect_to @app_category
    else
      render :new
    end
  end

  def update
    @app_category = AppCategory.find(params[:id])

    if @app_category.update_attributes(params[:app_category])
      flash[:notice] = "App category #{ @app_category.name } was successfully updated."
      redirect_to @app_category
    else
      render :edit
    end
  end

  def destroy
    @app_category = AppCategory.find(params[:id])
    @app_category.destroy

    flash[:notice] = "App category #{ @app_category.name } was successfully destroyed."
    redirect_to app_categories_url
  end
end