class AdPieceVersionsController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @ad_piece_versions = AdPieceVersion.page params[:page]
  end

  def show
    @ad_piece_version = AdPieceVersion.find(params[:id])
  end

  def new
    @ad_piece_version = AdPieceVersion.new
		@ad_piece_version.resources.build

		@ad_pieces = AdPiece.all.order("title")
		@ad_channel_versions = AdChannelVersion.all

		@resources = Resource.all
  end

  def edit
    @ad_piece_version = AdPieceVersion.find(params[:id])
		@ad_pieces = AdPiece.all.order("title")
		@ad_channel_versions = AdChannelVersion.all

		@resources = Resource.all
  end

  def create
    @ad_piece_version = AdPieceVersion.new(params[:ad_piece_version])

		count = 0 # TODO teste - o valor deve ser definido pelo criador do ad_piece_version, na view
    @ad_piece_version.resources.each do |res|
#      res.ad_piece_version= @ad_piece_version
			res.internal_id = count
			count += 1
    end

    if @ad_piece_version.save
      flash[:notice] = 'AdPieceVersion was successfully created.'
      redirect_to @ad_piece_version
    else
      render :action => "new"
    end
  end

  def update
    @ad_piece_version = AdPieceVersion.find(params[:id])

    if @ad_piece_version.update_attributes(params[:ad_piece_version])
      flash[:notice] = 'AdPieceVersion was successfully updated.'
      redirect_to(@ad_piece_version)
    else
      render :action => "edit"
    end
  end

  def destroy
    @ad_piece_version = AdPieceVersion.find(params[:id])
    @ad_piece_version.destroy

    redirect_to(ad_piece_versions_url)
  end


  def delete_file
    @ad_piece_version = AdPieceVersion.find(params[:id])
    if params[ :resource_id ]
      @resource = ResourceFile.find( params[ :resource_id ] )
      @resource.destroy if @resource
    end

    render :partial => 'resources_list', :locals => { :ad_piece_version => @ad_piece_version }
  end
end