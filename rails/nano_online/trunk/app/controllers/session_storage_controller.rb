class SessionStorageController < ApplicationController
  # POST /session_storage?uid=1234
  def create
    session[:uid] = params[:uid]
    render :nothing => true
  end
end