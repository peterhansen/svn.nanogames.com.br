class ApplicationController < ActionController::Base
  NANO_SIGNATURE_LENGTH = 9

  before_filter :set_ie_header
  protect_from_forgery
  helper_method :current_user, :current_customer, :parse_data, :get_input_stream, :last_access

  def login_customer(customer)
    session[:customer_id] = customer.id
  end

  def login_and_redirect_customer(customer, *options)
    session[:customer_id] = customer.id
    redirect_to customer, *options
  end

  def logout_customer
    session[:customer_id] = nil
  end

  def set_ie_header
    response.headers['P3P'] = 'CP="CAO PSA OUR"'
  end

  def current_customer
    @current_customer ||= Customer.find_by_id(session[:customer_id])
  end

  def current_user
    @current_User ||= User.find_by_id(session[:user_id])
  end

  def set_layout
    if current_customer
      return "customer"
    elsif current_user
      return current_user.layout
    else
      return "regular"
    end
  end

  def authorize_admin
    authorize :administrator
  end

  def authorize(*roles)
    if roles.any?
      unless current_user && (roles.include?(current_user.role) || roles == [:all])
        if ( !request.env[ 'HTTP_REFERER' ].blank? && request.env[ 'HTTP_REFERER' ].match( /.*(\/login){1,2}/ ) )
          flash[:notice] = 'Session expired. Please login.'
        else
          redirect_to 'http://www.nanogames.com.br'
        end
      end
    elsif !current_user
      render :text => "You don't have permission to access this area."
    end
  end

  def authorize_nano
    if request.post? || request.put?
      # parameters = request.request_parameters.keys.first
      if check_nano_signature(request.raw_post)
        # remove a assinatura dos dados a serem filtrados
        read_binary_params( request.raw_post.slice( NANO_SIGNATURE_LENGTH, request.raw_post.length ) )
        return true
      end
    end
    # conexão veio de um aparelho não detectado, através do navegador ou de
    # outro aplicativo - responde em html
    redirect_to 'http://www.nanogames.com.br'
    return false
  end


  # Como gerar nano_signature: "#{[137].pack('i')}NANO#{[13].pack('i')}#{[10].pack('i')}#{[26].pack('i')}#{[10].pack('i')}"
  def check_nano_signature(data)
    if data
      # primeiro verifica se veio de um aparelho Java (big endian)
      big_endian = [ 137, 78, 65, 78, 79, 13, 10, 26, 10 ]
      is_big_endian = true

      for i in 0..( big_endian.length - 1 )
        if data[i].ord != big_endian[i]
          is_big_endian = false
          break
        end
      end

      if is_big_endian
        session[ 'nano_client' ] = 'big_endian'
        return true
      else
        # se não for big endian, tenta a detecção do little endian
        little_endian = [ 137, 110, 97, 110, 111, 13, 10, 26, 10 ]

        for j in 0..( little_endian.length - 1 )
          if ( data[ j ].ord != little_endian[ j ] )
            session[ 'nano_client' ] = 'undetected'
            return false
          end
        end
      end

      session[ 'nano_client' ] = 'little_endian'
      return true
    end
  end

  ##
  # Tenta detectar o aparelho do usuário. Caso seja detectado com sucesso, adiciona
  # a chave :device ao hash session, para que o aparelho de origem esteja acessível
  # para os próximos métodos. Se essa chave já estiver definida nos parâmetros,
  # a verificação só é feita caso <code>force_check</code> seja true.
  ##

  def is_iphone_request?
    request.user_agent =~ /Mobile\/.*(Safari)*/ ? true : false
  end

  def set_iphone_format
    request.format = :iphone if is_iphone_request?
  end

  def iphone_authenticate
    if is_iphone_request? and not session[:current_user_id]
      redirect_to "/imuz_db/login"
    end
  end

  def get_device( force_check = false )
    unless session[:device]
      if ( force_check || !params[ :device ] )
        # tenta detectar o aparelho do usuário (caso não seja encontrado, já armazena seu user-agent no banco de dados)
        # @device, matches = Device.parse_user_agent( request.user_agent )
        if ua = UaProfiler::UserAgent.new(request)
          @device = ua.device
        end
      end
    else
      if !user
        redirect_to 'http://www.nanogames.com.br'
      end
    end
  end

  def session_expiry
    if session[ :expires_at ]
      time_left = (session[:expires_at] - Time.now).to_i

      if time_left <= 0
        flash[:notice] = 'Session expired. Please login.'
        redirect_to :controller => 'login', :action => 'login'
        return
      end
    end

    session[:expires_at] = 60.minutes.from_now
    end

  def log_access
    access_params = {
      :user_agent     => request.user_agent || 'not identified',
      :phone_number   => session[:phone_number],
      :phone_hash     => session[:phone_hash],
      :ip_address     => request.remote_ip,
      :http_path      => request.path,
      :http_referrer  => request.referer,
      :device         => params[:device] ? params[:device] : nil
    }
    access = Access.new(access_params)
    session[:last_access] = access.id if access.save
  end

  def last_access
    @access ||= Access.find_by_id(session[:last_access])
  end

  def render_binary( send_news_feeder_data = true, send_ad_server_data = false )
    begin
      out = output_stream

      if params[ :app_version ].blank?
        # se a versão do aplicativo não for informada (ou for incorreta), não há como gravar os recordes
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_APP_NOT_FOUND )
      else
        # retorna atualizações do news feeder, caso haja
        if send_news_feeder_data
          unless params[ ID_NEWS_FEEDER_DATA ].blank?
            NewsFeed.send_data( out, params )
          end
        end
        # retorna atualizações do ad server, caso haja
        if send_ad_server_data
          unless params[ ID_AD_SERVER_DATA ].blank?
            AdCampaign.send_data(out, params)
          end
        end

        # retorna informações do gerenciador de recursos (utilizados por anúncios, notícias, etc.)
        # não é necessário verificar se há recursos para enviar aqui, pois o teste é feito internamente no método send_data().
        Resource.send_data( out, params )

        # permite que o método chamador realize as operações de escrita no stream
        yield out
      end
      # retorna as informações gravadas pelo método chamador
      data = out.flush
      # a partir da versão 0.0.4 do Nano Online, a resposta é comprimida com GZip
      data = ActiveSupport::Gzip::compress( data ) if ( params[ ID_NANO_ONLINE_VERSION ] && params[ ID_NANO_ONLINE_VERSION ] >= '0.0.4' )
      render( :text => data )
    rescue Exception => e
      # caso ocorra algum erro, envia mensagem de erro interno do servidor
      out = output_stream
      out.write_byte( ID_RETURN_CODE )
      out.write_short( RC_SERVER_INTERNAL_ERROR )

      # retorna as informações gravadas pelo método chamador
      render :text => out.flush
      raise e
    end
  end

  def output_stream( value = session[ 'nano_client' ] )
    value == 'big_endian' ? BigEndianTypes::OutputStream.new : LittleEndianTypes::OutputStream.new
  end

  def read_binary_params(data)
    parse_data(data) do |stream, id|
      case id
        when ID_APP
          params[ id ] = stream.read_string
          # tenta achar a aplicação no BD
          params[ :app ] = App.find_by_name_short( params[ id ] )

          # se já tiver lido a versão do aplicativo, armazena a versão
          if params[ ID_APP_VERSION ]
            params[ :app_version ] = AppVersion.find_by_app_id_and_number( params[ :app ].id.to_i, params[ ID_APP_VERSION ].to_s )
          end

        when ID_CUSTOMER_ID
          params[ id ] = stream.read_int
          params[ :customer ] = Customer.find_by_id( params[ id ] )

        when ID_RETURN_CODE
          params[ id ] = stream.read_short

        when ID_LANGUAGE
          params[ id ] = stream.read_byte

        when ID_APP_VERSION
          params[ id ] = stream.read_string

          # se já tiver lido qual o aplicativo de origem, busca a versão
          if params[ :app ]
            params[ :app_version ] = AppVersion.find_by_app_id_and_number( params[ :app ].id, params[ id ] )
          end

        when ID_NANO_ONLINE_VERSION
          params[ id ] = stream.read_string

        when ID_ERROR_MESSAGE
          params[ id ] = stream.read_string

        when ID_AD_SERVER_DATA
          params[ id ] = AdCampaign.parse_data( stream )

        when ID_NEWS_FEEDER_DATA
          params[ id ] = NewsFeed.parse_data( stream )

        when ID_CLIENT_LOCAL_TIME
          params[ id ] = stream.read_datetime

        when ID_EXPIRED_PROFILE_IDS
          total_profiles = stream.read_byte
          profiles = []
          for i in 1..total_profiles
            profiles << stream.read_int
          end
          params[ id ] = profiles

        when ID_SPECIFIC_DATA
          # inicia a leitura dos dados específicos
          params[ id ] = stream.read_available
        else
          raise ArgumentError, "Unknown application parameter", caller
      end
    end

    if ( params[ :app_version ] && params[ ID_NANO_ONLINE_VERSION ] >= NANO_ONLINE_AUTO_UPDATE_MIN_VERSION )
      app_version_update = params[ :app_version ].last_update

      if ( app_version_update && ( params[ :last_app_version_update_number ].blank? ||
              Util::natcmp( app_version_update.updated_app_version.number, params[ :last_app_version_update_number ] ) > 0 ) )
        params[ :app_version_update ] = app_version_update
      end
    end

    # se o usuário for detectado, armazena a informação na tabela de acessos
    if ( params[ :customer ] && last_access )
      last_access.customer = params[ :customer ]
      last_access.save
    end
  end

  def current_user_apps
    @current_apps ||= if current_user.partner
      current_user.partner.apps
    else
      App.all
    end
  end

  def parse_data( data, value = session[ 'nano_client' ] )
      stream = input_stream( data, value )

      # varre o corpo da mensagem, preenchendo a hashtable com os parâmetros encontrados,
      # até que todos os parâmetros sejam lidos (EOF) ou que o id de dados específicos
      # seja encontrado, quando a stream é então passada ao tratador adequado.
      while stream.available > 0
        id = stream.read_byte
        yield stream, id
      end
  end

  def input_stream( data, value = session[ 'nano_client' ] )
    value == 'big_endian' ? BigEndianTypes::InputStream.new( data ) : LittleEndianTypes::InputStream.new( data )
  end

  def default_page_layout
    return session[ 'page_layout' ].nil? ? 'simple' : session[ 'page_layout' ]
  end
end