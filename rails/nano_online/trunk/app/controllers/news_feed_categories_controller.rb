class NewsFeedCategoriesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @news_feed_categories = NewsFeedCategory.includes([:news_category_translations, {:app_versions => :app}]).page params[:page]
  end

  def show
    @news_feed_category = NewsFeedCategory.find(params[:id])
  end

  def new
    if request.post?

        @news_feed_category = NewsFeedCategory.new
        @translation = NewsCategoryTranslation.new(params[:news_category_translation])

        @news_feed_category.news_category_translations << @translation

        if @news_feed_category.save
          flash[:notice] = "News feed category #{ @news_feed_category.id } was successfully created."
          redirect_to :action => "index"
        else
          @languages = Language.find( :all )
        end
    else
        @translation = NewsCategoryTranslation.new
        @languages = Language.find( :all )
    end
  end

  def edit
    @news_feed_category = NewsFeedCategory.find(params[:id])
  end

  def create; end

  def update
    @news_feed_category = NewsFeedCategory.find(params[:id])

    if @news_feed_category.update_attributes(params[:news_feed_category])
      redirect_to @news_feed_category, :notice => "News feed category #{ @news_feed_category.id } was successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @news_feed_category = NewsFeedCategory.find(params[:id])
    @news_feed_category.destroy

    redirect_to news_feed_categories_url, :notice => "News feed category #{ @news_feed_category.id } was successfully destroyed."
  end

  def add_translation
    if request.post?
      feed = NewsFeedCategory.find(params[:news_category_translation][:news_feed_category_id])
      @translation = NewsCategoryTranslation.new(params[:news_category_translation])

      @languages = Language.find( :all )

      feed.news_category_translations << @translation

      if feed.save
        flash[:notice] = "News feed category translation created."
        redirect_to :action => "show", :id => @translation.news_feed_category_id
      else
        flash[:notice] = "News feed category translation could not be saved."
        redirect_to :action => "add_translation"
      end
    else
      @translation = NewsCategoryTranslation.new
      @translation.news_feed_category_id = params[:id]

      @languages = Language.find( :all )
    end
  end

  def edit_category_translation
    @translation = NewsCategoryTranslation.find(params[:id])
    @languages = Language.find( :all )
  end

  def update_category_translation
    @translation = NewsCategoryTranslation.find(params[:id])

    if @translation.update_attributes(params[:news_category_translation])
      flash[:notice] = "Category translation updated"
      redirect_to :action => :show, :id => @translation.news_feed_category_id
    else
      flash[:notice] = "Category translation could not be saved."
      redirect_to :action => :edit
    end
  end

  def delete_category_translation
    @translation = NewsCategoryTranslation.find(params[:id])
    feed = NewsFeedCategory.find(@translation.news_feed_category_id)

    if feed.translations.size > 1
      if @translation.destroy
        flash[:notice] = "Category translation deleted."
      else
        flash[:notice] = "Category translation could not be deleted."
      end
      redirect_to :action => :show, :id => @translation.news_feed_category_id
    else
      if feed.destroy
        flash[:notice] = "Category deleted."
        redirect_to :action => :index
      else
        flash[:notice] = "Category could not be deleted."
        redirect_to :action => :show, :id => @translation.news_feed_category_id
      end
    end
  end
end