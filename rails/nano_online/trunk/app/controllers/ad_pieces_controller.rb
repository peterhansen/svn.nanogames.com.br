class AdPiecesController < ApplicationController
  layout :set_layout

  def index
    @ad_pieces = AdPiece.page params[:page]
  end

  def show
    @ad_piece = AdPiece.find(params[:id])
  end

  def new
    @ad_piece = AdPiece.new
    @channel = AdChannel.order(:title)
  end

  def edit
    @ad_piece = AdPiece.find(params[:id])
    @channel = AdChannel.order(:title)
  end

  def create
    @ad_piece = AdPiece.new(params[:ad_piece])

    if @ad_piece.save
      flash[:notice] = 'AdPiece was successfully created.'
      redirect_to @ad_piece
    else
      @channel = AdChannel.order(:title)
      render :action => :new
    end
  end

  def update
    @ad_piece = AdPiece.find(params[:id])

    if @ad_piece.update_attributes(params[:ad_piece])
      flash[:notice] = 'AdPiece was successfully updated.'
      redirect_to @ad_piece
    else
      @channel = AdChannel.order(:title)
      render :action => :edit
    end
  end

  def destroy
    @ad_piece = AdPiece.find(params[:id])
    @ad_piece.destroy

    redirect_to ad_pieces_url
  end
end
