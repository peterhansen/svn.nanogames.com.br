class DownloadCodesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @download_codes = DownloadCode.order(:id).page params[:page]
  end

  def show
    redirect_to :action => 'index'
  end

  def new
    @download_code = DownloadCode.new
  end


  # GET /download_codes/1/edit
  def edit
    @download_code = DownloadCode.find(params[:id])
  end


  # POST /download_codes
  # POST /download_codes.xml
  def create
    @download_code = DownloadCode.new(params[:download_code])

    respond_to do |format|
      if @download_code.save
        flash[:notice] = "Download code \##{ @download_code.id } was successfully created."
        format.html { redirect_to(@download_code) }
        format.xml  { render :xml => @download_code, :status => :created, :location => @download_code }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @download_code.errors, :status => :unprocessable_entity }
      end
    end
  end


  # PUT /download_codes/1
  # PUT /download_codes/1.xml
  def update
    @download_code = DownloadCode.find(params[:id])

    respond_to do |format|
      if @download_code.update_attributes(params[:download_code])
        flash[:notice] = "Download code \##{ @download_code.id } was successfully updated."
        format.html { redirect_to(@download_code) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @download_code.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /download_codes/1
  # DELETE /download_codes/1.xml
  def destroy
    @download_code = DownloadCode.find(params[:id])
    @download_code.destroy

    respond_to do |format|
      flash[:notice] = "Download code \##{ @download_code.id } was successfully destroyed."
      format.html { redirect_to(download_codes_url) }
      format.xml  { head :ok }
    end
  end

end
