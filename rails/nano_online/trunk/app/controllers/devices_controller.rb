class DevicesController < ApplicationController
  before_filter :authorize_admin, :except => [:index, :list]
  before_filter :session_expiry, :except => [:index, :list]
  has_scope :by_vendor
  respond_to :html, :js, :json
  layout :set_layout

  def index
    @devices = unless params[:filter].blank?
      @filter = params[:filter]
      p = "%#{params[:filter]}%"
      Device.includes(:vendor).where("devices.model like ? or vendors.name like ?", p, p).page params[:page]
    else
      Device.page params[:page]
    end

    respond_with @devices do |format|
      format.json { render :json => @devices }
      format.html do
        redirect_to root_url unless current_user
      end
    end
  end

  def list
    headers['Access-Control-Allow-Origin'] = '*'

    conditions = {}
    conditions[:vendor_id] = params[:vendor] if params[:vendor]

    unless params[:app].blank?
      @devices = Device.find_by_sql(["SELECT distinct devices.id, devices.model, app_versions.status FROM devices LEFT OUTER JOIN families ON families.id = devices.family_id LEFT OUTER JOIN app_versions_families ON app_versions_families.family_id = families.id LEFT OUTER JOIN app_versions ON app_versions.id = app_versions_families.app_version_id LEFT OUTER JOIN apps ON apps.id = app_versions.app_id WHERE devices.vendor_id = ? AND (app_versions.status = 'Available') and apps.name_short = ? ORDER BY devices.model", params[:vendor], params[:app]])
    else
      @devices = Device.find_by_sql(["SELECT distinct devices.id, devices.model, app_versions.status FROM devices LEFT OUTER JOIN families ON families.id = devices.family_id LEFT OUTER JOIN app_versions_families ON app_versions_families.family_id = families.id LEFT OUTER JOIN app_versions ON app_versions.id = app_versions_families.app_version_id LEFT OUTER JOIN apps ON apps.id = app_versions.app_id WHERE devices.vendor_id = ? AND (app_versions.status = 'Available') ORDER BY devices.model", params[:vendor]])
    end

    respond_to do |format|
      format.json { render :json => @devices.to_json(:only => [:id], :methods => :name) }
    end
  end

  def show
    unless @device = Device.find_by_id(params[:id])
      redirect_to :action => :index
    end
  end

  def new
    @device = Device.new
  end

  def edit
    @device = Device.find(params[:id])
  end

  def create
    @device = Device.new(params[:device])

    if @device.save
      flash[:notice] = "Device #{ @device.model } was successfully created."
      redirect_to(@device)
    else
      render :action => "new"
    end
  end

  def update
    params[ :device ][ :band_ids ] ||= []
    params[ :device ][ :data_service_ids ] ||= []
    params[ :device ][ :optional_api_ids ] ||= []
    @device = Device.find(params[:id])

    if @device.update_attributes(params[:device])
      flash[:notice] = "Device #{ @device.model } was successfully updated."
      redirect_to(@device)
    else
      render :action => "edit"
    end
  end

  def destroy
    @device = Device.find(params[:id])
    begin
      if @device.destroy
        flash[:notice] = "Device #{ @device.model } was successfully destroyed."
      else
        flash[:notice] = "Device #{ @device.model } could not be destroyed."
      end
    rescue Exception => e
      flash[:notice] = "An error stopped #{ @device.model } from been deleted."
    ensure
      redirect_to :action => :index
    end
  end
end
