class AppleDownloadsController < ApplicationController
  layout :set_layout

  def index
    apps = current_user_apps.map {|a| a.id }
    conditions = {}
    @initial = Date.new *(params[:start].values.map {|v| v.to_i})   if params['start']
    @final = Date.new *(params[:finish].values.map {|v| v.to_i})    if params['finish']
    conditions[:begin_date] = @initial..@final                              if @initial && @final
    @select_countries = conditions[:country_id] = params["countries"]       if params["countries"]
    @select_apps = conditions['apps.id'] = (params["apps"] || apps)

    @apps = AppleDownload.find_by_sql(["select id, name from apps where id in (select distinct app_id from apple_downloads) && id in (\"#{apps.join("\", \"")}\")"])
    @countries = Country.find_by_sql(["select id, name from countries where id in(select distinct country_id from apple_downloads) order by name"])
    @apple_downloads = AppleDownload.includes(:app).
                                     where(conditions).
                                     order('apple_downloads.begin_date DESC').
                                     page params[:page]
  end

  def create
    @report = AppleDownloadReport.new( :name => params[:file].path, :file => params[:file].path )

    if @report.save
      flash[:notice] = "Report saved."
    else
      flash[:notice] = @report.errors.full_messages
    end

    redirect_to apple_downloads_path
  end

  def upload_file
    add_apple_daily_report(params[:file])
  end

  def add_apple_daily_report( arq )
    if arq != "" && arq.original_filename.match('.txt')
      apple_download_report = AppleDownloadReport.new
      apple_download_report.name = arq.original_filename

      if apple_download_report.save
        arq.read.split("\n").each do |line|
          data = line.split("\t")

          unless data[0] == "Provider"
            apple_download = AppleDownload.new_from_array(data)
            apple_download.apple_download_report = apple_download_report
            apple_download.save
          end
        end
        flash[:notice] = "Report saved."
      else
        flash[:notice] = "Report already imported."
      end
    else
      flash[:notice] = "Invalid file."
    end
    redirect_to :action => :index
  end

  def csv
    conditions = {}
    @initial = Date.new *(params[:start].values.map {|v| v.to_i})     if params['start']
    @final = Date.new *(params[:finish].values.map {|v| v.to_i})      if params['finish']
    conditions[:begin_date] = @initial..@final                        if @initial && @final
    @select_countries = conditions[:country_id] = params["countries"] if params["countries"]
    @select_apps = conditions['apps.id'] = (params["apps"])           if params["apps"]

    # debugger
    @apple_downloads = AppleDownload.includes(:app).where(conditions)

    keys = %w{Date App Country Category Units Price Total}

    csv_string = CSV.generate { }

    csv_string << keys.join("; ") << "\n"

    @apple_downloads.each do |row|
      values = []

      values << row.begin_date
      values << row.app.name
      values << row.country.name
      values << row.apple_product_category.category
      values << row.units
      values << row.developer_proceeds
      values << row.developer_proceeds * row.units

      csv_string << values.join("; ") << "\n"

    end

    filename = "apple_download_report.csv"
    send_data( csv_string,
      :type => "text/csv; charset=utf-8; header=present",
      :filename => filename)
    #TODO dimas: repassar o objeto @apple_downloads para a action :index
    # redirect_to :action => :index
  end
end