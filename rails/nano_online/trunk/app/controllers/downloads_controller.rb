require 'zip/zip'
require 'zip/zipfilesystem'

class DownloadsController < ApplicationController

  CLIENT_ACTIONS = [
    :select_vendor,
    :select_app,
    :download_jad,
    :js_download,
    :select_device,
    :device_selected,
    :download_file_by_id,
    :download_file_by_short_name,
    :download_seda_puzzle,
    :select_specific_model,
    :download_axe,
    :download_axe_mobile,
    :download_wallpaper,
    :download_jar_file,
    :download_app
  ]

#  before_filter :no_cache, :only => CLIENT_ACTIONS
  before_filter :authorize, :except => CLIENT_ACTIONS
  before_filter :log_access, :except => :download
  before_filter :session_expiry, :except => CLIENT_ACTIONS

  layout :set_layout, :except => CLIENT_ACTIONS

  def index
    @downloads = Download.order(session[ :sort_order ] || 'id').page params[:page]
  end

  def show; end

  def new
    @download = Download.new
  end

  def edit
    @download = Download.find( params[ :id ] )
  end

  def create
    @download = Download.new(params[:download])

    if @download.save
      flash[:notice] = "Download #{ @download.id } was successfully created."
      redirect_to(@download)
    else
      render :action => "new"
    end
  end

  def destroy
    @download = Download.find(params[:id])
    @download.destroy

    flash[:notice] = "Download #{ @download.id } was successfully destroyed."
    redirect_to(downloads_url)
  end

  def update
    @download = Download.find(params[:id])

    if @download.update_attributes(params[:download])
      flash[:notice] = "Download #{ @download.id } was successfully updated."
      redirect_to(@download)
    else
      render :action => "edit"
    end
  end

  def select_vendor
    @vendors = Vendor.with_available_versions
    render :layout => default_page_layout
  end

  def select_device
    redirect_to :action => :select_vendor and return if params[:vendor] == 'Vendor'

    if params[ :vendor ]
      @vendor = params[:vendor]
      @devices = Device.with_available_versions.includes(:family => { :app_versions => :app }).where(:vendor_id => params[:vendor]).order('devices.model')
      render :layout => default_page_layout
    else
      redirect_to :action => :select_vendor, :layout => default_page_layout
    end
  end

  def select_app( first_run = true )
    redirect_to(:action => :select_device, :vendor => params[:vendor]) and return if params[:model] == 'Devices'

    @model = params[:model]
    @apps = App.with_available_versions.includes(:app_versions => [:families => [:devices]]).where('devices.id' => params[:model]).order('apps.name')
    render :device_unsupported, :layout => default_page_layout unless @apps
    render :layout => default_page_layout
  end

  def download
    download = Download.new({:app_version_id => params[ :app_version_id ],
                             :access => last_access || (Access.create! :user_agent => 'site'),
                             :device_id => params[ :device_id ]})
    if download.save
      jad_file = JadFile.new( "#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}" )
      # TODO responsabilidade do JadFile.
      # TODO o construtor do JadFile deveria ser JadFile.new(download)
      jad_file.info[ 'MIDlet-Install-Notify' ] = "#{URL_NANO_ONLINE}/notify/i/#{download.id}"
      jad_file.info[ 'MIDlet-Delete-Notify' ] = "#{URL_NANO_ONLINE}/notify/d/#{download.id}"

      send_data( jad_file.data, :type      => default_content_type( 'jad' ),
                                :filename  => "#{download.app_version.app_version_number_clean}.jad" )
    else
      redirect_to '/static/download'
    end
  end

  # /downloads/download_jad?app=BOAD&model=822488299
  def download_jad( send_jad_inline = true )
    headers['Access-Control-Allow-Origin'] = '*'
debugger    
    begin
      session[ :device_chosen ] ||= params[ :device ]
      
      if params[:model]
        session[ :device_chosen ] = model = Device.find(params[:model])
        params[:app_version] = AppVersion.last(:include => [:app, :families], :conditions => ["apps.name_short = ? AND families.id = ?", params[:app], model.family_id]).id
      else
        params[ :app_version ] = params[ :download ][ :chosen ] if params[ :app_version ].nil?
      end
      
      download = Download.new({:app_version_id => params[ :app_version ],
        :access => last_access,
        :device => session[ :device_chosen ]})
        
      filename = "#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}"

      if File::exists? filename
        if download.save
          # cria um novo arquivo .jad contendo os novos links para os arquivos binários (não é necessário salvá-lo, pois é enviado diretamente para o usuário)
          jad_file = JadFile.new( filename )
          # atualiza MIDlet-Install-Notify e MIDlet-Delete-Notify
          jad_file.info[ 'MIDlet-Install-Notify' ] = "#{URL_NANO_ONLINE}/notify/i/#{download.id}"
          jad_file.info[ 'MIDlet-Delete-Notify' ] = "#{URL_NANO_ONLINE}/notify/d/#{download.id}"
          
          # cria os códigos de download de cada arquivo binário
          # TODO no caso do BlackBerry, não é necessário o arquivo .jar (somente os .cod)
            
          if params[:zipped] && params[:zipped] ==  "yes"
            download_zipped(download)
          else
            jad_file.file_paths do |key, value|
              download_code = DownloadCode.new( { :download => download, :file => value } )
              
              if download_code.save
                session[:download_code] = download_code.code
                jad_file.info[ key ] = "#{URL_NANO_ONLINE}/download/#{download_code.code}"
              end
            end
          end
          
          if send_jad_inline
            # envia diretamente o arquivo .jad
            respond_to do |format|
              format.html { send_data(jad_file.data, :type       => default_content_type( 'jad' ),
                                                     :filename   => "#{download.app_version.app_version_number_clean}.jad" )}
              format.js { send_data(jad_file.data, :type       => default_content_type( 'jad' ),
                                                     :filename   => "#{download.app_version.app_version_number_clean}.jad" ) }
            end
          end
        else
          respond_to do |format|
            format.html {render( :template => 'server_error', :layout => default_page_layout )}
            format.js {render :action => 'server_error.js.rjs'}
          end
        end
      else
        head :not_found
      end
    rescue
      redirect_to :server_error, :layout => default_page_layout
    end
  end

  def download_zipped(download)
    zipped = Tempfile.new("temp_zip_#{request.remote_ip}")

    if session[ :device_chosen ].vendor.name == "Blackberry"
      send_file "#{PATH_RELEASES}#{download.app_version.file_bt_cod.filename.url}",
                :disposition => 'attachment',
                :filename    => "bt_#{download.app_version.app_version_number_clean}.cod"
    else
      begin
        Zip::ZipOutputStream.open(zipped.path) do |zip|
          zip.put_next_entry("#{download.app_version.app.name}.jad")
          zip.print IO.read("#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}")

          jad_file.file_paths do |key, value|
            zip.put_next_entry("#{value}")
            zip.print IO.read("#{PATH_RELEASES}/#{download.app_version.app_version_number_clean}/#{value}")
          end
        end

        send_file zipped.path,
                  :type           => 'application/zip',
                  :disposition    => 'attachment',
                  :filename       => "#{download.app_version.app_version_number_clean}.zip"
      rescue
        respond_to do |format|
          format.html {render( :template => 'server_error', :layout => default_page_layout )}
          format.js {render :action => 'server_error.js.rjs'}
        end
      end
    end
  end

  ##
  # Permite acesso a um arquivo através de um código de download, que só pode ser utilizado uma vez.
  ##
  def download_file_by_id
    params[:id] = session[:download_code] if session[:download_code]
    download_code = DownloadCode.where(:code => params[ :id ]).first

    if download_code
#       o código não pode ter sido usado e o user-agent desta conexão deve ser o mesmo do pedido de download
#       TODO além do user-agent, testar IP e/ou número de telefone?
#       if ( @download_code.download.user_agent == request.user_agent )
      if download_code.used_at.nil? || download_code.used_at.blank?
        unless ( request.user_agent.match( /MDS_\d*\.\d+\.(\d+\.)?(\d+)?/ ) )
          download_code.used_at = Time.now
          download_code.save()
        end

        send_file(    "#{PATH_RELEASES}/#{download_code.download.app_version.app_version_number_clean}/#{download_code.file}",
                      :type => default_content_type( File.extname( download_code.file ) ) )
      else
        redirect_to( :controller => :downloads, :action => :vendor_select )
      end
    else
      redirect_to( :controller => :downloads, :action => :vendor_select )
    end
  end


  def download_file_by_short_name
    if params[ :app_short_name ]
      @app = App.find_by_name_short( params[ 'app_short_name' ] )
      @app_versions = AppVersion.where(:app_id => app.id, :status => 'Available')

      @app_versions.each do |app_version|
        devices = app_version.devices

        if devices.include?( params[ :device ] )
          params[ :app_version ]    = app_version.id
          session[ :device_chosen ] = params[ :device ]

          download_jad
          return
        end
      end
    end

    render :action => :device_unsupported_short, :layout => default_page_layout
  end


  def download_seda_puzzle
    app = App.find_by_name_short( 'SEDP' )
    app_versions = AppVersion.where(:app_id => app.id, :status => 'Available')

    app_versions.each do |app_version|
      devices = app_version.devices

      if devices.include?( params[ :device ] )
        params[ :app_version ]    = app_version.id
        session[ :device_chosen ] = params[ :device ]

        download_jad
        return
      end
    end

    render :action => 'seda_unsupported', :layout => default_page_layout
  end

  def download_axe_mobile
    app = App.find_by_name_short( 'AXET' )
    app_versions = AppVersion.where(:app_id => app.id, :status => 'Available')

    app_versions.each do |app_version|
      devices = app_version.devices()

      if ( devices.include?( params[ :device ] ) )
        params[ :app_version ]    = app_version.id
        session[ :device_chosen ] = params[ :device ]

        download_jad
        return
      end
    end
    render :action => :axe_unsupported, :layout => "apps/axe"
  end

  def download_axe
    params[:zipped] = "yes"
    if vendor = Vendor.find_by_name(params[:vendor])
      if device = Device.find_by_vendor_id_and_model(vendor.id, params[:model])
        app = App.find_by_name_short( 'AXET' )
        app_version = app.find_version_with_support_to(device)

        if app_version.instance_of?(Array)
          params[ :app_version ] = app_version.first.id
        else
          params[:app_version] = app_version.id
        end

        session[ :device_chosen ] = device
        download_jad
     end
   end

  end

  def download_wallpaper
    send_file "#{Rails.root}/public/images/#{params[:size]}.png", :type => 'image/png', :filename => "Axe Wallpaper.png"
  end

  def download_home
    @vendors = Vendor.all( :order => "name" )

    render :layout => false
  end

  def select_m_remote
    render :update do |page|
      page.replace_html 'download_form', :partial => "select_model_remote"
    end
  end

  def download_app
    @app = App.find_by_name_short(params[:id])
  end

  # /downloads/download_jar_file?app=BOAD&model=822352997
  def download_jar_file
    model = Device.find(params[:model])
    conditions = {}
    conditions['apps.name_short'] = params[:app] if params[:app]
    conditions['families.id'] = model.family_id if model
    conditions['vendors.id'] = params[:vendor] if params[:vendor]

    @app_version = AppVersion.includes([:app, :families => {:devices => [:vendor]}]).where(conditions).first

    if model.vendor.name == 'Blackberry'
      send_file PATH_RELEASES + @app_version.file_bt_cod.filename.url
    else
      filename = PATH_RELEASES + @app_version.file_jad.filename.url

      JadFile.new(filename).file_paths do |key, value|
        send_file "#{PATH_RELEASES}/#{@app_version.app_version_number_clean}/#{value}"
        return
      end
    end
  end

private

  def default_content_types
    { 'jad'  => 'text/vnd.sun.j2me.app-descriptor',
      'jar'  => 'application/java-archive',
      'cod'  => 'application/vnd.rim.cod',
      'sis'  => 'application/vnd.symbian.install',
      'sisx' => 'x-epoc/x-sisx-app' }
  end

  def default_content_type( extension )
    default_content_types[ extension.delete( '.' ) ]
  end
end