class RankingTypesController < ApplicationController
  layout :set_layout

  def index
    @ranking_types = RankingType.includes([:unit_format, :ranking_system, [:app_versions => :app]]).page params[:page]
  end

  def show
    @ranking_type = RankingType.find(params[:id])
  end

  def new
    @ranking_type = RankingType.new
    @unit_formats = UnitFormat.all
    @ranking_systems = RankingSystem.all
    @apps = App.all
    @ranking_type.app_versions.build
  end

  def create
    @ranking_type = RankingType.new(params[:ranking_type])

    if @ranking_type.save
      params[:app_version_ids].each do |app_version|
        @ranking_type.app_versions << AppVersion.find(app_version)
      end

      flash[:notice] = 'Ranking type was successfully created.'
      redirect_to(@ranking_type)
    else
      @unit_formats = UnitFormat.all
      @ranking_systems = RankingSystem.all
      @apps = App.all
      @ranking_type.app_versions.build
      render :action => "new"
    end
  end

  def edit
    @ranking_type = RankingType.find(params[:id])
    @unit_formats = UnitFormat.all
    @ranking_systems = RankingSystem.all
    @apps = App.includes(:app_versions)
    @ranking_type.app_versions.build
  end

  def update
    @ranking_type = RankingType.find(params[:id])

    if @ranking_type.update_attributes(params[:ranking_type])
      flash[:notice] = 'Ranking entry was successfully updated.'
      redirect_to(@ranking_type)
    else
      @unit_formats = UnitFormat.all
      @ranking_systems = RankingSystem.all
      @apps = App.all
      @ranking_type.app_versions.build
      render :action => "edit"
    end
  end

  def destroy
    @ranking_type = RankingType.find(params[:id])
    @ranking_type.destroy

    redirect_to ranking_types_url
  end
end