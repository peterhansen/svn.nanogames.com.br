class AdChannelsController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @ad_channels = AdChannel.page params[:page]
  end

  def show
    @ad_channel = AdChannel.find(params[:id])
  end

  def new
    @ad_channel = AdChannel.new
    @statuses = AdChannelStatus.select([:status, :id])
  end

  def edit
    @ad_channel = AdChannel.find(params[:id])
    @ad_channel_versions = AdChannelVersion.where(:ad_channel_id => @ad_channel)
    @statuses = AdChannelStatus.select([:status, :id])
  end

  def create
    @ad_channel = AdChannel.new(params[:ad_channel])

    if @ad_channel.save
      flash[:notice] = 'AdChannel was successfully created.'
      redirect_to(@ad_channel)
    else
      @statuses = AdChannelStatus.select([:status, :id])
      render :action => "new"
    end
  end

  def update
    @ad_channel = AdChannel.find(params[:id])

    if @ad_channel.update_attributes(params[:ad_channel])
      flash[:notice] = 'AdChannel was successfully updated.'
      redirect_to @ad_channel
    else
      @statuses = AdChannelStatus.select([:status, :id])
      render :action => "edit"
    end
  end

  def destroy
    @ad_channel = AdChannel.find(params[:id])
    @ad_channel.destroy

    redirect_to ad_channels_url
  end

  protected

  def refresh_app_versions( update_div = true )
    if params[ :id ]
      @ad_channel ||= AdChannel.find(params[:id])
    end

    if update_div
      render :partial => 'app_versions', :locals => { :ad_channel => @ad_channel }
    end
  end
end