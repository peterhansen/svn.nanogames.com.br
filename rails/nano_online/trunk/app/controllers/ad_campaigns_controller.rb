class AdCampaignsController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @ad_campaigns = AdCampaign.page params[:page]
  end

  def show
    @ad = AdCampaign.find(params[:id])
  end

  def new
    @ad = AdCampaign.new
    @advertiser = Advertiser.includes(:partner)
		# TODO o advertiser deve estar travado para o partner do usuário atual (somente Nano Games pode escolher o advertiser livremente)
		@ad_pieces = AdPiece.order(:title)
		# TODO um ad_piece deve ter também um advertiser relacionado, para que um anunciante não possa escolher anúncios de terceiros ao criar/editar uma campanha
  end

  def edit
    @ad = AdCampaign.find(params[:id])
    @advertiser = Advertiser.includes(:partner)
		@ad_pieces = AdPiece.order(:title)
		# TODO um ad_piece deve ter também um advertiser relacionado, para que um anunciante não possa escolher anúncios de terceiros ao criar/editar uma campanha
  end

  def create
    @ad = AdCampaign.new(params[:ad_campaign])

    if @ad.save
      flash[:notice] = 'AdCampaign was successfully created.'
      redirect_to(@ad)
    else
      @advertiser = Advertiser.all

      render :action => "new"
    end
  end

  def update
    @ad = AdCampaign.find(params[:id])

    if @ad.update_attributes(params[:ad_campaign])
      flash[:notice] = 'AdCampaign was successfully updated.'
      redirect_to(@ad)
    else
      @advertiser = Advertiser.all
      render :action => "edit"
    end
  end

  def destroy
    @ad = AdCampaign.find(params[:id])
    @ad.destroy

    redirect_to(ad_campaigns_url)
  end
end
