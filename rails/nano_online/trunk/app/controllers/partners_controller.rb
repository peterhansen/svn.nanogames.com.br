class PartnersController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @partners = Partner.order(:name).includes([:users, :apps]).page params[:page]

    @partner_users = {}
    @partners.each do |partner|
      users = partner.users.collect { |u| u.name }
      @partner_users[ partner ] = users.join( ', ' )
    end
  end

  def show
    redirect_to :action => :index
  end

  def new
    @partner = Partner.new
  end

  def edit
    @partner = Partner.find(params[:id])
  end

  def create
    @partner = Partner.new(params[:partner])

    if @partner.save
       redirect_to partners_url, notice: "Partner #{ @partner.name } was successfully created."
    else
      render :new
    end
  end

  def update
    @partner = Partner.find(params[:id])

    if @partner.update_attributes(params[:partner])
      redirect_to @partner, notice: "Partner #{ @partner.name } was successfully updated."
    else
      format.html { render :action => "edit" }
    end
  end


  def destroy
    @partner = Partner.find(params[:id])
    @partner.destroy

    redirect_to partners_url, notice: "Partner #{ @partner.name } was successfully destroyed."
  end
end