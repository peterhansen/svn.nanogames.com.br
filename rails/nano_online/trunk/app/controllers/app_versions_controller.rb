class AppVersionsController < ApplicationController

  before_filter :only => :index do |controller|
    if self.request.formats.first == "application/json"
      controller.authorize :all
    else
      controller.authorize :administrator
    end
  end

  before_filter :session_expiry, :except => :index

  layout :set_layout

  respond_to :html, :json
  has_scope :by_device

  def index
    if params[:available].blank?
      @available = "Available"
    else
      @available = params[:available]
    end

    @app_versions = unless params[:filter].blank?
      @filter = params[:filter]
      p = "%#{params[:filter]}%"
      AppVersion.includes(:app).where("status = ? and apps.name like ?", @available, p).order('app_versions.created_at desc').page params[:page]
    else
      @app_versions = AppVersion.where("status" => @available).order('app_versions.created_at desc').page params[:page]
    end

    respond_with @app_versions do |format|
      format.json do
        render :json => @app_versions.to_json(:only => :id, :include => {:app => {:only => :name }})
      end
    end
  end

  def show
    @page_title = 'Applications versions'

    begin
      @app_version = AppVersion.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:notice] = "Invalid app_version"
      redirect_to :action => :index
    end
  end

  def new
    @app_version = AppVersion.new
    @news_feed_categories = NewsFeedCategory.all
  end

  def edit
    @app_version = AppVersion.find(params[:id])
    @families = Family.where('ignored = false').order('name')
    @news_feed_categories = NewsFeedCategory.all
    get_other_app_versions
  end

  def create
    @page_title = 'Applications versions'
    params[:app_version][:news_feed_category_ids] ||= []
    @app_version = AppVersion.new(params[:app_version])

    if @app_version.save
      refresh_app_files( false )
      flash[:notice] = "App version #{ @app_version.number } was successfully created."
      redirect_to(@app_version)
    else
      @news_feed_categories = NewsFeedCategory.all
      render :action => "new"
    end
  end

  def update
    @page_title = 'Applications versions'

    params[ :app_version ][ :family_ids ] ||= []
    params[ :app_version ][ :news_feed_category_ids ] ||= []
    @app_version = AppVersion.find(params[:id])
    @families = Family.where('ignored = false').order('name')

    get_other_app_versions

    if @app_version.update_attributes(params[:app_version])
      refresh_app_files( false )

      flash[:notice] = "App version #{ @app_version.number } was successfully updated."
      redirect_to(@app_version)
    else
      @families = Family.where('ignored = false').order('name')
      @page_title = "Editing #{ @app_version.app.name } version #{ @app_version.number }"
      get_other_app_versions

      render :action => "edit"
    end
  end

  def destroy
    @page_title = 'Applications versions'

    @app_version = AppVersion.find(params[:id])
    @app_version.destroy

    flash[:notice] = "App version #{ @app_version.number } was successfully destroyed."
    redirect_to app_versions_url
  end


  def delete_file
    @app_version = AppVersion.find(params[:id])
    @app_file = AppFile.find( params[ :file_id ] )

    @app_file.destroy if @app_file

    render :partial => 'files_list', :locals => { :app_version => @app_version }
  end


  def delete_app_version_update
    @app_version = AppVersion.find(params[:id])
    @app_version_update = AppVersionUpdate.find( params[ :app_version_update_id ] )

    @app_version_update.destroy if @app_version_update

    get_other_app_versions

    render :partial => 'updates', :locals => { :app_version => @app_version }
  end


  def create_updated_app_version
    @app_version = AppVersion.find(params[:id])

    @new_app_version = AppVersion.find( params[ :new_app_version ].blank? ? @app_version : params[ :new_app_version ] )
    @old_app_version = AppVersion.find( params[ :old_app_version ].blank? ? @app_version : params[ :old_app_version ] )

    @update = AppVersionUpdate.new
    @update.app_version = @old_app_version
    @update.updated_app_version = @new_app_version

    @update.save

    get_other_app_versions

    render :partial => 'updates', :locals => { :app_version => @app_version }
  end

  def refresh_app_files( update_div = true )
    if params[ :id ]
      @app_version ||= AppVersion.find(params[:id])
    end

    if params[ :app_files ]
      params[ :app_files ].each do |attr, value|
        unless value[ :app_file ].blank?
          new_app_file = AppFile.new
          new_app_file.app_version = @app_version
          new_app_file.filename = value[ :app_file ]
          new_app_file.save
        end
      end

    end

    if ( update_div )
      render :partial => 'files_list', :locals => { :app_version => @app_version }
    end
  end

  protected

  def get_other_app_versions
    @other_app_versions = AppVersion.where(:app_id => @app_version.app).order('number')
  end
end