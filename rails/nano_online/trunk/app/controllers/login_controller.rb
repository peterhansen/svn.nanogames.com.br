class LoginController < ApplicationController
  # before_filter :authorize_admin, :except => [ :login, :logout, :index ]
  before_filter :session_expiry, :except => [ :login, :logout ]
  
  layout :set_layout, :except => :login
  
  def login
    reset_session
    if request.post?
      user = User.authenticate( params[:name], params[:password] )

      if user
        session[ :user_id ] = user.id
        redirect_to :action => 'index'
      else
        flash[ :notice ] = 'Invalid user/password combination'
        render 'login'
      end
    end
  end

  def logout
    reset_session
    flash[:notice] = 'Logged out.'
    redirect_to :action => 'login'
  end

  def index; end

  def redirect_to_nano_games
    authorize
  end
  
  def delete_user
    if ( request.post? )
      user = User.find( params[ :id ] )
      
      begin
        user.destroy
        flash[ :notice ] = "User #{ user.name } successfully deleted."
      rescue Exception => e
        flash[ :notice ] = e.message
      end
    end
    
    redirect_to( :action => :list_users )
  end
  
  def list_users
    @all_users = User.find( :all )
  end
end
