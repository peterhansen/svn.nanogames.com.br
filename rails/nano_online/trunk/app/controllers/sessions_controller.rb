class SessionsController < ApplicationController
	def new
		redirect_to current_customer if current_customer
	end

	def create
		omniauth_hash = request.env["omniauth.auth"]
		customer = Customer.authenticate_omniauth({ 
			omniauth_hash: omniauth_hash,
			customer_hash: { nickname: params[:username], password: params[:password]}
		})

		if current_customer
			current_customer.merge(customer)
			login_and_redirect_customer current_customer, :notice => 'Signed in.'
		else
			login_and_redirect_customer customer, :notice => 'Signed in.'
		end
	rescue ExternalAuthenticationFailed
		if customer = current_customer
			customer.add_authentication(omniauth_hash)
		else
			customer = Customer.create_with_omniauth(omniauth_hash)
		end

		login_and_redirect_customer customer, :notice => 'Signed in.'
	rescue CustomerNotFound
		flash[:notice] = 'Incorrect nickname or password.'
		render :new
	end

	def destroy
		logout_customer
		redirect_to new_session_url
	end
end