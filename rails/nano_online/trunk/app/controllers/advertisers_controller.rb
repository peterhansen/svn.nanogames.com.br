class AdvertisersController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :set_layout

  def index
    @advertisers = Advertiser.page params[:page]
  end

  def show
    @advertiser = Advertiser.find(params[:id])
  end

  def new
    @advertiser = Advertiser.new
    @partners = Partner.order("name")
  end

  def edit
    @advertiser = Advertiser.find(params[:id])
    @partners = Partner.order("name")
  end

  def create
    @advertiser = Advertiser.new(params[:advertiser])

    if @advertiser.save
      flash[:notice] = 'Advertiser was successfully created.'
      redirect_to(@advertiser)
    else
      @partners = Partner.order("name")
      render :action => "new"
    end
  end

  def update
    @advertiser = Advertiser.find(params[:id])

    if @advertiser.update_attributes(params[:advertiser])
      flash[:notice] = 'Advertiser was successfully updated.'
      redirect_to(@advertiser)
    else
      @partners = Partner.order("name")
      render :action => "edit"
    end
  end

  def destroy
    @advertiser = Advertiser.find(params[:id])
    @advertiser.destroy

    redirect_to advertisers_url
  end
end
