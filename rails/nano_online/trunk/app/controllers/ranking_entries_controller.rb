class RankingEntriesController < ApplicationController
  before_filter :log_access, :except => [ :high_scores, :update_high_scores, :user_range_high_score ]
  before_filter :authorize_nano, :only => [ :submit ]
  before_filter :session_expiry, :except => [ :submit, :high_scores, :update_high_scores, :user_range_high_score ]

  layout "admin"

  def index
    @page_title = 'Ranking entries'

    if current_user.is_admin?
      @ranking_entries = RankingEntry.includes([:ranking_type, {:app_version => :app}, :customer, {:device => :vendor}]).
                                      order('created_at desc').
                                      limit(100).
                                      page params[:page]
    else
      user_app_versions = []
      current_user_apps.each do |a|
        user_app_versions << a.app_versions
      end
      user_app_versions.flatten!

      @ranking_entries = RankingEntry.includes([:ranking_type, {:app_version => :app}, :customer, {:device => :vendor}]).
                                      order('created_at desc').
                                      limit(100).
                                      where(:app_version_id => user_app_versions).
                                      page params(:page)
    end
  end

  def new
    @page_title = 'Ranking entries'
    @app_versions = AppVersion.all
    @customers = Customer.all
    @devices = Device.all(:order => 'vendor_id, model')
    @ranking_types = RankingType.all

    @ranking_entry = RankingEntry.new
  end

  def edit
    @page_title = 'Ranking entries'
    @app_versions = AppVersion.all
    @customers = Customer.all
    @devices = Device.all(:order => 'vendor_id, model')
    @ranking_types = RankingType.all

    @ranking_entry = RankingEntry.find(params[:id])
  end

  def create
    @page_title = 'Ranking entries'

    @ranking_entry = RankingEntry.new(params[:ranking_entry])

    if @ranking_entry.save
      flash[:notice] = "Ranking entry #{ @ranking_entry.id } was successfully created."
      redirect_to ranking_entries_url
    else
      @app_versions = AppVersion.all
      @customers = Customer.all
      @devices = Device.all(:order => 'vendor_id, model')
      @ranking_types = RankingType.all

      render :action => "new"
    end
  end

  def update
    @page_title = 'Ranking entries'

    @ranking_entry = RankingEntry.find(params[:id])

      if @ranking_entry.update_attributes(params[:ranking_entry])
        flash[:notice] = "Ranking entry #{ @ranking_entry.id } was successfully updated."
        redirect_to ranking_entries_url
      else
        @app_versions = AppVersion.all
        @customers = Customer.all
        @devices = Device.all(:order => 'vendor_id, model')
        @ranking_types = RankingType.all
        render :action => "edit"
      end
  end

  def destroy
    @page_title = 'Ranking entries'

    @ranking_entry = RankingEntry.find(params[:id])
    @ranking_entry.destroy

    flash[:notice] = "Ranking entry #{ @ranking_entry.id } was successfully destroyed."
    redirect_to(ranking_entries_url)
  end

  def submit
    render_binary do |out|
      # se o aparelho não tiver sido armazenado, o obtém da sessão (se tiver sido detectado) para que a entrada de ranking o armazene
      params[ :device ] ||= last_access.device

      submitted, profiles_indexes, entries_with_error = RankingEntry.submit_records( params, session[ 'nano_client' ] )

      if params[ ID_NANO_ONLINE_VERSION ] < '0.0.3'
        # submeteu os recordes com sucesso - retorna tabela de ranking atualizada
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_OK )
        out.write_byte( ID_SPECIFIC_DATA )

        entries, players_positions = RankingEntry.best_scores( params,
          RankingEntry::RANKING_ENTRY_DEFAULT_LIMIT,
          RankingEntry::RANKING_ENTRY_DEFAULT_OFFSET,
          profiles_indexes )

        if entries
          # indica a quantidade de entradas lidas (pode ser menor que o máximo pedido)
          out.write_byte( entries.length )

          entries.each do |e|
            # escreve as informações de cada entrada
            out.write_int( e.customer_id )
            out.write_long( e.score )
            out.write_string( e.customer.nickname )
          end
        else
          # não encontrou entradas de ranking
          out.write_byte(0)
        end
      else
        # versões de clientes >= 0.0.3
        if submitted
          # submeteu os recordes com sucesso - retorna tabela de ranking atualizada
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_OK )
          out.write_byte( ID_SPECIFIC_DATA )

          entries, customer_entries = RankingEntry.best_scores( params, params[ RankingEntry::PARAM_LIMIT ], params[ RankingEntry::PARAM_OFFSET ], profiles_indexes )

          if entries
            out.write_byte( RankingEntry::PARAM_N_ENTRIES )
            out.write_short( entries.length )
            # indica a quantidade de entradas lidas (pode ser menor que o máximo pedido)
            entries.each do |e|
              # escreve as informações de cada entrada
              out.write_int( e.customer_id )
              out.write_long( e.score )

              # TODO remover depois que o bug estiver sido corrigido
              begin
                # out.write_string( e.customer.nickname )
                out.write_string( e.customer.nickname.force_encoding('utf-8') )
              rescue Exception => exc
                HoptoadNotifier.notify(
                  :error_class   => "Encoding::CompatibilityError",
                  :error_message => "Erro em BigEndianTypes::OutputStream#write_utf() - #{exc.to_s}",
                  :parameters    => params.merge({nickname: e.customer.nickname})
                )
              end
            end
          end

          # TODO corrigir erro no iPhone - customer_id não encontrado ao retornar lista de jogadores extras
          unless customer_entries.blank?
            out.write_byte( RankingEntry::PARAM_N_OTHER_PROFILES )
            out.write_short( customer_entries.length )
            customer_entries.each do |e|
              begin
                # escreve as informações de cada entrada
                out.write_int( e.id )
                out.write_long( e.score )
                out.write_string( e.nickname )
                out.write_int( e.ranking )
              rescue Exception
                return nil
              end
            end
         end

         out.write_byte( RankingEntry::PARAM_END )
        else
          # TODO não conseguiu submeter as entradas de ranking
          # não encontrou entradas de ranking
          out.write_byte(0)
        end
      end
    end
  end

  # GET /high_scores?ranking_type=1&sub_type=0&limit=10&offset=0&app_version=1
  # GET /high_scores.json?ranking_type=1&sub_type=0&limit=10&offset=0&app_version=1
  def high_scores
    if params[:ranking_type]
      @ranking_entry = params[:ranking_type] = RankingType.find(params[:ranking_type])
    else
      @ranking_entry = params[:ranking_type] =  RankingType.find_by_name("Seda Puzzle")
    end

    params[ :sub_type ] ||= 0 unless params[ :sub_type ]

    # os métodos "to_i" garantem que os dados estarão corretos antes de fazer o select (evita injeção de SQL)
    @limit = params[ :limit ].blank? ? 10 : params[ :limit ].to_i
    @limit = 10 if @limit <= 0
    @offset = params[ :offset ].blank? ? 0 : params[ :offset ].to_i
    @offset = 0 if @offset < 0

    @scores, @customer_scores = RankingEntry.best_scores( params, @limit, @offset )

    respond_to do |format|
      format.html
      format.json do
        render :json => @scores.map {|e| {:id => e.id, :score => e.score, :customer => e.customer.nickname}}.to_json
      end
    end
  end

  # /user_range_high_score?uid=3&ranking_type=1&app_version=1&range=10
  def user_range_high_score
    @position = RankingEntry.position( params[:uid], params[:app_version], params[:ranking_type] )
    limit = params[:range] || 10
    offset = [(@position - (limit.to_i / 2)), 0].max
    @ranking_entries = RankingEntry.best_scores( params, limit, offset ).first

    render :json => [@ranking_entries.to_json(:only => [:id, :score], :include => { :customer => { :only => [ :id, :nickname ] } } ), {'position' => @position}.to_json]
  end

  def update_high_scores
    params[ :app ] = params[:application]
    @scores, @customer_scores = high_scores()
  end
end
