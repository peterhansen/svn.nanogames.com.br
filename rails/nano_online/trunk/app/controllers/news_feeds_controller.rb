class NewsFeedsController < ApplicationController

  before_filter :except => :refresh do |controller|
    controller.authorize :administrator, :imuzdb
  end

  before_filter :session_expiry, :except => :refresh
  before_filter :authorize_nano, :only => [ :refresh ]
  before_filter :log_access, :only => [ :refresh ]

  layout :set_layout, :except => [ :refresh ]

  def index
    @page_title = 'News feeds list'
    @news_feeds = NewsFeed.order('created_at DESC').page params[:page]
  end

  def show
    @page_title = "News feed detail"
    @news_feed = NewsFeed.find(params[:id])
  end

  def new
    @page_title = 'New news feed'
    @news_feed = NewsFeed.new
    @translation = NewsFeedTranslation.new

    @news_feed_categories = NewsFeedCategory.find( :all )
    @languages = Language.find( :all )
  end

  def edit
    @news_feed = NewsFeed.find(params[:id])
    @translation = @news_feed.translations.first
    @news_feed_categories = NewsFeedCategory.find( :all )

    @page_title = "Editing news feed \##{@news_feed.id}"
  end

  def create
    @news_feed = NewsFeed.new(params[:news_feed])
    @translation = NewsFeedTranslation.new(params[:translation])
    @news_feed.news_feed_translation << @translation

    if @news_feed.save
      flash[:notice] = "News feed #{ @news_feed.id } was successfully created."
      redirect_to :action => :index
    else
      @news_feed_categories = NewsFeedCategory.find( :all )
      @languages = Language.find( :all )

      flash[:notice] = "News feed could not be created."
      render :action => "new"
    end
  end

  def update
    @news_feed = NewsFeed.find(params[:id])
    @translation = @news_feed.translations.first

    if @news_feed.update_attributes(params[:news_feed]) && @translation.update_attributes(params[:translation])
      flash[:notice] = "News feed #{ @news_feed.id } was successfully updated."
      redirect_to :action => :index
    else
      @news_feed_categories = NewsFeedCategory.find( :all )
      render :action => "edit"
    end
  end

  def destroy
    @news_feed = NewsFeed.find(params[:id])
    if @news_feed.destroy
      notice = "News feed #{ @news_feed.id } was successfully destroyed."
    else
      notice = "News feed #{ @news_feed.id } could not be destroyed."
    end
    redirect_to news_feeds_url, :notice => notice
  end


  def refresh
    render_binary do |out|
      # como a gerência das notícias é feita em toda conexão ao sistema, só é necessário informar que a conexão foi ok
      out.write_byte(ID_RETURN_CODE)
      out.write_short(RC_OK)
    end
  end

  def add_translation
    if request.post?
      @news_feed = NewsFeed.find(params[:news_feed][:id])
      @translation = NewsFeedTranslation.new(params[:news_feed_translation])
      @news_feed.news_feed_translation << @translation

      @languages = Language.find( :all )

      if @news_feed.save
        flash[:notice] = "Translation saved."
        redirect_to :action => "show", :id => params[:news_feed][:id]
      end
    else
      @news_feed = NewsFeed.find(params[:id])
      @translation = NewsFeedTranslation.new
      @languages = Language.find( :all )
    end
  end

  def edit_translation
    @translation = NewsFeedTranslation.find(params[:id])
  end

  def update_translation
    @translation = NewsFeedTranslation.find(params[:translation][:id])

    if @translation.update_attributes(params[:translation])
      flash[:notice] = "Translation saved."
     redirect_to :action => "show", :id => @translation.news_feed_id
    else
      flash[:notice] = "Translation could not be saved."
      redirect_to :action => "edit_translation"
    end
  end

  def delete_translation
    @translation = NewsFeedTranslation.find(params[:id])
    feed = NewsFeed.find(@translation.news_feed_id)

    if feed.translations.size > 1
      if @translation.destroy
        flash[:notice] = "News translation deleted."
      else
        flash[:notice] = "News translation could not be deleted."
      end
      redirect_to :action => :show, :id => @translation.news_feed_id
    else
      if feed.destroy
        flash[:notice] = "Feed deleted."
        redirect_to :action => :index
      else
        flash[:notice] = "Feed could not be deleted."
      end
    end
  end
end