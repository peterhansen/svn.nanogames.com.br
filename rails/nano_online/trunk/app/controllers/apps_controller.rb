class AppsController < ApplicationController
#  before_filter :only => :index do |controller|
#    controller.authorize :all
#  end
  before_filter :authorize_admin, :except => [:index, :list, :show]
  before_filter :session_expiry, :except => [:index, :list]

  respond_to :html, :json
  has_scope :by_device, :name_and_id

  layout :set_layout

  def index
    headers['Access-Control-Allow-Origin'] = '*'

    conditions = {}
    conditions[:id] = params[:id] if params[:id]
    conditions[:name_short] = params[:name_short] if params[:name_short]
    conditions[:name] = params[:name] if params[:name]
    
    @apps = apply_scopes(App).where(conditions).order(:name).page(params[:page]).per(params[:page_size])

    respond_with @apps do |format|
      format.html do
        redirect_to root_url unless current_user
      end
    end
  end

  def list
    conditions = {}

    @apps = if params[:device]
      App.with_available_versions.includes(:app_versions => [:families => [:devices]]).where('devices.id' => params[:device]).order('apps.name')
    else
      App.with_available_versions.where(conditions).order('apps.name')
    end

    respond_to do |format|
      format.json { render :json => @apps.to_json(:only => [:name_short, :name])}
    end
  end

  def show
    @page_title = 'Applications'
    @app = App.find_by_id(params[:id])

    respond_with @app
  end

  def new
    @page_title = 'Applications'
    @app = App.new
  end

  def edit
    @page_title = 'Applications'
    @app = App.find(params[:id])
  end

  def create
    @page_title = 'Applications'
    @app = App.new(params[:app])

    if @app.save
      flash[:notice] = "App #{ @app.name } was successfully created."
      redirect_to @app
    else
      render :action => "new"
    end
  end

  def update
    @page_title = 'Applications'

    params[ :app ][ :app_category_ids ] ||= []
    params[ :app ][ :partner_ids ] ||= []
    @app = App.find(params[:id])

    if @app.update_attributes(params[:app])
      flash[:notice] = "App #{ @app.name } was successfully updated."
      redirect_to(@app)
    else
      render :action => "edit"
    end
  end

  def destroy
    @page_title = 'Applications'
    @app = App.find(params[:id])
    @app.destroy

    flash[:notice] = "App #{ @app.name } was successfully destroyed."
    redirect_to(apps_url)
  end

end
