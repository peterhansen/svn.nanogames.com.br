module ApplicationHelper
  def flash_error
    content_tag(:h1, flash[:notice], :class => 'error') if flash[:notice]
  end

  def errors_for(record)
    if record && record.errors
      errors = record.errors.full_messages

      content_tag :div do
        content_tag :ul do
          errors.collect { |message| content_tag(:li, message) }.join('').html_safe
        end
      end
    end
  end
end