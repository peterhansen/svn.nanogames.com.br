job_type :runner,  'cd :path && bundle exec script/runner -e :environment ":task"'
job_type :rake,    'cd :path && RAILS_ENV=:environment /usr/bin/env bundle exec rake :task'

every 1.day, :at => '01:01' do
  rake 'db:backup'
end