NanoOnline::Application.routes.draw do
  root :to => 'static#home'
  match 'customer_login/login'    => 'customer_login#login'
  match 'customer_login'          => 'customer_login#index'
  match 'customer_login/cadastro' => 'customer_login#cadastro'
  match 'customer_login/do_login' => 'customer_login#do_login'
  match 'customer_login/create'   => 'customer_login#create'
  match 'customers/remember_password' => 'customers#remember_password'
  match 'validate/:hash'          => 'customer_login#validate'
  match 'users/change_password'   => 'users#change_password'
  match 'users/recover_password'  => 'users#recover_password'
  match 'users/user_info'         => 'users#user_info'
  match 'login/login'             => 'login#login'
  match 'admin'                   => 'login#login'
  match 'login/logout'            => 'login#logout'
  match 'login/index'             => 'login#index'

  match '/apps', :controller => 'apps', :action => 'index', :constraints => {:method => 'OPTIONS'}

  resources :app_categories, :ad_channel_statuses, :ad_piece_versions, :families,
    :midp_versions, :cldc_versions,
    :app_versions, :bands, :data_services, :device_bugs, :optional_apis, :operators, :integrators,
    :download_codes, :user_roles, :partners, :users, :session_storage,
    :midlet_reports, :news_feed_categories, :ranking_types, :unit_formats, :ranking_systems,
    :ad_campaigns, :ad_pieces, :advertisers, :resources, :resource_files,
    :resource_types, :ad_channels, :resource_file_types

  resources :sessions, :only => [:new, :create, :destroy]
  match '/logout' => 'sessions#destroy', :as => 'logout'
  match '/login' => 'sessions#new', :as => 'login'
  match '/auth/:provider/callback' => 'sessions#create'

  resources :registrations

  resources :ad_channel_versions do
    collection do
      get :delete_composition
    end
  end

  resource :app_customer_data do
    collection do
      post :save_data
      post :load_data
    end
  end

  resource :app_versions do
    member do
      get :create_updated_app_version
    end
  end

  resources :ranking_entries, :except => [:show] do
    collection do
      get   :user_range_high_score
      get   :high_scores
      post  :submit
      get   :update_high_scores
    end
  end

  resources :static do
    member do
      post :web_contact
    end
  end

  resources :apple_downloads do
    collection do
      post :csv
    end
  end

  resources :customers do
    collection do
      post :login
      post :import
      post :add
      post :refresh
    end
  end

  resources :submissions do
    member do
      get :generate
    end
  end

  resources :news_feeds do
    member do
      post :refresh
    end
  end

  # resources :downloads do
  #   collection do
  #     get :vendor_select
  #   end
  # end

  # TODO mudar downloads para um resource
  match 'downloads/app' => 'downloads#download_app'
  match 'downloads/select_device' => 'downloads#select_device'
  match 'downloads/select_app' => 'downloads#select_app'
  match 'downloads/download_jar_file' => 'downloads#download_jar_file'
  match 'downloads/download_jad' => 'downloads#download_jad'
  match 'downloads/' => 'downloads#select_vendor'
  match 'downloads/download' => 'downloads#download'
  match 'downloads/index' => 'downloads#index'
  match 'downloads/download_axe_mobile' => 'downloads#download_axe_mobile'
  match 'downloads/download_seda_puzzle' => 'downloads#download_seda_puzzle'
  match 'downloads/server_error' => 'downloads#server_error'

  match 'apple_downloads' => 'apple_downloads#index'
  match 'apple_downloads/upload_file' => 'apple_downloads#upload_file'
  match 'undetected_user_agents' => 'undetected_user_agents#index'
  match 'submissions/generate' => 'submissions#generate'

  match 'boadica/categories' => 'boadica#categories'
  match 'boadica/store' => 'boadica#store'
  match 'boadica/offers' => 'boadica#offers'
  match 'download/:id' => 'downloads#download_file_by_id'
  match 'test/' => 'test#detect'
  # match 'high_scores/' => 'ranking_entries#high_scores'
  # match 'high_scores/.:format' => 'ranking_entries#high_scores'
  # match 'user_range_high_score/.:format' => 'ranking_entries#user_range_high_score'
  match 'notify/i/:id' => 'midlet_reports#install', :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/
  match 'notify/d/:id' => 'midlet_reports#delete', :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/
  match 'notify/install/:id' => 'midlet_reports#install', :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/
  match 'notify/delete/:id' => 'midlet_reports#delete', :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/

  resources :vendors do
    collection do
      get :list
    end
  end

  resources :devices do
    collection do
      get :list
    end
  end

  resources :apps do
    collection do
      get :list
    end
  end

  namespace :imuz_db do
    resource :game_sessions do
      member do
        post  :frame
        get   :frame if Rails.env.development?
        get   :home
        get   :news
        get   :start
        get   :new_question_iphone
        post  :respond
        get   :next_question
        get   :skip_question
        get   :missed_question
        get   :correct
        get   :wrong
        get   :game_over
        get   :ranking
        get   :choose_nickname
        post  :set_nickname
        get   :friend_list
        post  :invite_friends
        get   :facebook_share
        get   :help
        post  :audio_state
        get   :profile
        get   :prepare_share
        get   :facebook_error
      end
    end
    resource :pages
  end

  match 'portfolio' => 'static#portfolio_2010', :as => :portfolio
  match 'vila_virtual' => 'static#vila_virtual', :as => :vila_virtual

  match "/customer_login/update/:id" => 'customer_login#update', :as => :update_customer_login, :method => :put
  match "/imuz_db/mobile/forget_password" => 'customer_login#forget_password', :as => :forget_password_customer_login
  match "/imuz_db/mobile/resend_password" => 'customer_login#resend_password', :as => :resend_password_customer_login, :method => :post
  match "/imuz_db/mobile/login" => 'customer_login#login'
  match "/imuz_db/mobile/sign_up" => 'customer_login#sign_up'
  match "/imuz_db/mobile/home" => 'imuz_db/game_sessions#home'
  match "/imuz_db/mobile/first_use" => 'customer_login#first_use'
  match "/imuz_db/mobile/fb_session" => 'imuz_db/game_sessions#fb_session'
  match "/imuz_db/mobile/fb_login" => 'imuz_db/game_sessions#fb_login'
  match "/imuz_db/mobile/installation" => 'customer_login#installation'
  match "/imuz_db/mobile" => 'customer_login#installation'
  match "/imuz_db/mobile/ajax_login" => 'customer_login#ajax_login'
  match "/imuz_db/mobile/new_question_ajax" => 'imuz_db/game_sessions#new_question_iphone'
  match "/customer_login/edit" => 'customer_login#edit'
  match "imuz_db/do_login" => 'customer_login#do_login'

  # map.resources :customer_login, :collection => {:do_login => :post, :sign_up => :get, :first_use => :get}

  match '/about' => 'static#about'
  match '/down' => 'static#download'
  match '/news' => 'static#news'
  match '/partner' => 'static#partners'
  match '/contact' => 'static#web_contact'
  match '/portfolio' => 'static#portfolio_2010'
  match '/vila_virtual' => 'static#vila_virtual'
end