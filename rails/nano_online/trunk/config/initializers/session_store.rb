# Be sure to restart your server when you modify this file.

NanoOnline::Application.config.session_store :cookie_store, :key => '_nano_online_session', :secret => 'a245858dbe7cc2a84e480b21a67485b8af12c44eefcb897dd7fb55dd0f6b595ad54c5c506ee83137a55006d0ba9d5a3789ddc1f9b4f86c0dd6b4914fd5bc2ce3'

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# NanoOnline::Application.config.session_store :active_record_store
