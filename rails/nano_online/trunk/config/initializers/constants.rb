URL_NANO_ONLINE          = 'http://online.nanogames.com.br'

# caminho dos arquivos privados
PATH_FILES               = "#{Rails.root}/files"

PATH_SUBMISSIONS         = "#{PATH_FILES}/submissions"

PATH_RELEASES            = "#{PATH_FILES}/releases"

# Id de jogo/aplicativo: dummy (usado para testes e debug)
ID_APP_DUMMY             = "DMMY"

# Id de chave global: aplicativo/jogo. Tipo do valor: string
ID_APP                   = -128

# Id de chave global: versão do aplicativo/jogo. Tipo do valor: string
ID_APP_VERSION           = -127

# Id de chave global: idioma atual do aplicativo. Tipo do valor: byte
ID_LANGUAGE              = -126

# Id de chave global: versão do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string
ID_NANO_ONLINE_VERSION   = -125

# Id de chave global: código de retorno. Tipo do valor: short
ID_RETURN_CODE           = -124

# Id de chave global: usuário. Tipo do valor: int
ID_CUSTOMER_ID           = -123

# Id de chave global: mensagem de erro. Tipo do valor: string
ID_ERROR_MESSAGE         = -122

#Id de chave global: início de dados específicos. Tipo do valor: variável
ID_SPECIFIC_DATA         = -121

# Id de chave global: início de dados do news feeder. Tipo do valor: variável
ID_NEWS_FEEDER_DATA      = -120

# Id de chave global: início de dados do ad server. Tipo do valor: variável
ID_AD_SERVER_DATA        = -119

# Id de chave global: horário local do cliente. Tipo do valor: datetime
ID_CLIENT_LOCAL_TIME     = -118

# Id de chave global: lista de jogadores com perfis (possivelmente) expirados.
# Tipo do valor: byte indicando a quantidade de perfis expirados, e um int para cada perfil. (cliente -> servidor)
# Ao retornar informações para o cliente:
# => int indicando id do usuário
# => long indicando a data de expiração.
ID_EXPIRED_PROFILE_IDS   = -117

# Id de chave global: Byte extra enviado nas requisições POST feitas via MacOS e iPhoneOS. Sem fazer isso o Rails ignora o último byte do corpo de uma mensagem quando seu valor é 0, causando bugs estranhos
ID_MACOS_RAILS_BUG_FIX   = -100


# assim como as chaves, os valores de retorno globais possuem valores negativos para que não haja confusão com os
# valores utilizados internamente em cada serviço. A chave 0 (zero) é considerada OK (RC_OK)

# Valor de retorno global: sem erros.
RC_OK                    = 0

# Valor de retorno global: erro interno do servidor.
RC_SERVER_INTERNAL_ERROR = -1

# Valor de retorno global: aparelho não suportado/detectado.
RC_DEVICE_NOT_SUPPORTED  = -2

# Valor de retorno global: aplicativo não encontrado (id de aplicativo inválido).
RC_APP_NOT_FOUND         = -3


# Versão mínima do Nano Online que suporta a atualização automática de versão
NANO_ONLINE_AUTO_UPDATE_MIN_VERSION = '0.0.3'

# Idioma do Nano Online: Inglês (EUA)
LANGUAGE_EN_US = 0

# Idioma do Nano Online: Espanhol (Espanha)
LANGUAGE_ES_ES = 1

# Idioma do Nano Online: Português (Brasil)
LANGUAGE_PT_BR = 2

# Idioma do Nano Online: Alemão (Alemanha)
LANGUAGE_DE_DE = 3

# Idioma do Nano Online: Português (Portugal)
LANGUAGE_PT_PT = 4

# Idioma do Nano Online: Italiano (Itália)
LANGUAGE_IT_IT = 5

# Idioma do Nano Online: Francês (França)
LANGUAGE_FR_FR = 6

# Idioma do Nano Online: Japonês (Japão)
LANGUAGE_JP_JP = 7

# Idioma do Nano Online: Russo (Rússia)
LANGUAGE_RU_RU = 8

# Idioma do Nano Online: Coreano (Coréia)
LANGUAGE_KO_KR = 9

# Idioma do Nano Online: Holandês (Holanda)
LANGUAGE_NL_NL = 10



class MultiplayerController# < ApplicationController
  # Id de parâmetro de conexão para partida multiplayer: quantidade mínima de jogadores da partida. Formato dos dados: byte
  PARAM_MATCH_MIN_PLAYERS = -1
  # Id de parâmetro de conexão para partida multiplayer: quantidade máxima de jogadores da partida. Formato dos dados: byte
  PARAM_MATCH_MAX_PLAYERS = -2
  # Id de parâmetro de conexão para partida multiplayer: id da partida. Formato dos dados: int
  PARAM_MATCH_ID = -3
  # Id de parâmetro de conexão para partida multiplayer: id do jogador (customer). Formato dos dados: int
  PARAM_PLAYER_ID = -4
  # Id de parâmetro de conexão para partida multiplayer: apelido do jogador (customer). Formato dos dados: string
  PARAM_PLAYER_NICKNAME = -5
  # Id de parâmetro de conexão para partida multiplayer: ordem do jogador na partida. Formato dos dados: int
  PARAM_PLAYER_ORDER = -6
  # Id de parâmetro de conexão para partida multiplayer: quantidade de jogadores na partida. Formato dos dados: byte
  PARAM_N_PLAYERS = -7
  # Id de parâmetro de conexão para partida multiplayer: dados da rodada. Formato dos dados: variável (byte[])
  PARAM_ROUND_DATA = -8
  # Id de parâmetro de conexão para partida multiplayer: pontuação global do jogador. Formato dos dados: long
  PARAM_PLAYER_GLOBAL_SCORE = -9
  # Id de parâmetro de conexão para partida multiplayer: posição global do jogador no ranking. Formato dos dados: int
  PARAM_PLAYER_GLOBAL_POSITION = -10
  # Id de parâmetro de conexão para partida multiplayer: fim das informações de um jogador. Formato dos dados: nenhum
  PARAM_PLAYER_INFO_END = -11
  # Id de parâmetro de conexão para partida multiplayer: índice do turno. Formato dos dados: short
  PARAM_TURN_INDEX = -12
  # Id de parâmetro de conexão para partida multiplayer: jogador está saindo do jogo. Formato dos dados: int
  PARAM_PLAYER_QUITTING = -13
  # Id de parâmetro de conexão para partida multiplayer: resultado final de uma partida. Formato dos dados: * byte indicando a quantidade de jogadores, e para cada jogador: * - int: id do jogador * - long: pontuação final * - long: variação na pontuação global do jogador
  PARAM_MATCH_FINAL_SCORES = -14
  # Id de parâmetro de conexão para partida multiplayer: jogador não respondeu a tempo. Formato dos dados: int
  PARAM_PLAYER_TIMEOUT = -15
  # Id de parâmetro de conexão para partida multiplayer: esperando outros jogadores entrarem no jogo. Formato dos dados: nenhum
  PARAM_WAITING_OPPONENTS = -16
  # Id de parâmetro de conexão para partida multiplayer: esperando outros jogadores enviarem sua jogada. Formato dos dados: nenhum
  PARAM_WAITING_ACTIONS = -17
  # Id de parâmetro de conexão para partida multiplayer: jogador já está em uma partida. Formato dos dados: nenhum
  PARAM_ALREADY_IN_MATCH = -18
  # Id de parâmetro de conexão para partida multiplayer: jogador não encontrado. Formato dos dados: nenhum
  PARAM_PLAYER_NOT_FOUND = -19
  # Id de parâmetro de conexão para partida multiplayer: número do turno errado. Formato dos dados: nenhum
  PARAM_WRONG_TURN_NUMBER = -20
  # Id de parâmetro de conexão para partida multiplayer: quantidade de jogadas dos oponentes. Formato dos dados: int
  PARAM_N_ACTIONS = -21
  # Id de parâmetro de conexão para partida multiplayer: customer_id ou match inválidos. Formato dos dados: nenhum
  PARAM_INVALID_PLAYER_OR_MATCH = -22
end # fim MultiplayerController
