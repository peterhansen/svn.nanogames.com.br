Kaminari.configure do |config|
  config.default_per_page = 25
  config.window = 10
  config.outer_window = 5
  config.left = 5
  config.right = 5
  # config.param_name = :page
end
