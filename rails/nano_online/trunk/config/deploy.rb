$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require 'bundler/capistrano'

set :user, "rails"
set :domain, 'staging.nanogames.com.br'
set :application, 'nano_online'
set :deploy_to, "/var/www/rails/#{application}"
set :current_path, "#{deploy_to}/current"
set :deploy_via, :export
set :repository, "http://svn.nanogames.com.br/rails/nano_online/trunk/"
set :scm_username, "nano_online_svn"
set :scm_password, 'nAnO_SvN123'
set :scm_verbose, true
set :chmod775, "app config db lib public vendor script script/* public/disp*"

default_run_options[ :pty ] = true

namespace :deploy do
  desc "Install missing gems"
  task :install_gems do
    run "cd /var/www/rails/nano_online/current; /usr/bin/env rake gems:install"
  end

  task :restart do
    run "touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end

  desc "Link shared files"
  task :create_symlinks do
    run "ln -s #{shared_path}/files #{release_path}/files"
    # run "ln -s #{shared_path}/ymls/newrelic.yml #{release_path}/config/newrelic.yml"
    run "ln -s #{shared_path}/images #{release_path}/public/images"
    run "ln -s #{shared_path}/flash #{release_path}/public/flash"
    run "ln -s #{shared_path}/banners #{release_path}/public/banners"
    run "ln -s #{shared_path}/vilavirtual #{release_path}/public/vilavirtual"
  end
end

after "deploy", "deploy:create_symlinks"
after "deploy:create_symlinks", "deploy:cleanup"

desc "Tail production log files"
task :tail_production do
  run "tail -f /var/www/rails/nano_online/shared/log/production.log" do |channel, stream, data|
    puts  # for an extra line break before the host name
    puts "#{data}"
    break if stream == :err
  end
end

require './config/boot'
require 'hoptoad_notifier/capistrano'

set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"