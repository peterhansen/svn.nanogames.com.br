database_yml = File.expand_path('../config/database.yml',  __FILE__)
RAILS_ENV   = ENV['RAILS_ENV'] || 'development'

require 'yaml'
config = YAML.load_file(database_yml)

Backup::Model.new(:db_backup, 'Backup do banco de dados') do
  database MySQL do |db|
    db.name       = config[RAILS_ENV]["database"]
    db.username   = config[RAILS_ENV]["username"]
    db.password   = config[RAILS_ENV]["password"]
    db.host       = config[RAILS_ENV]["host"]
    db.port       = config[RAILS_ENV]["port"]
    # db.lock       = true
  end

  store_with SCP do |server|
    server.username = 'nano_uploader'
    server.password = '$NaN0$'
    server.ip       = '69.163.164.96'
    server.port     = 22
    server.path     = '~/backup/'
    server.keep     = 5
  end
end

Backup::Model.new(:files, 'Backup dos aquivos') do
  sync_with RSync do |server|
    server.username = 'nano_uploader'
    server.password = '$NaN0$'
    server.ip       = '69.163.164.96'
    server.port     = 22
    server.path     = '~/backup/'
    server.mirror   = true
    server.compress = true

    server.directories do |directory|
      directory.add "/var/www/rails/nano_online/shared/files"
      directory.add "/var/www/rails/nano_online/shared/system"
    end
  end
end