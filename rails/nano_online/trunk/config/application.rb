require File.expand_path('../boot', __FILE__)

require 'rails/all'
require 'csv'
# If you have a Gemfile, require the gems listed there, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env) if defined?(Bundler)

module NanoOnline
  class Application < Rails::Application

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += Dir["#{config.root}/lib/**/", "#{config.root}/app/models/concerns"]
    # config.autoload_paths += %W(#{config.root}/lib #{config.root}/lib/util)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Brasilia'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # JavaScript files you want as :defaults (application.js is always included).
    config.action_view.javascript_expansions[:defaults] = %w(jquery)

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    config.generators do |g|
      g.orm             :active_record
      g.template_engine :erb
      g.test_framework  :rspec, :fixture => true
      g.fixture_replacement :machinist
    end


#    config.action_controller.session = {
#        :key  => '_nano_online_session',
#        :secret       => 'a245858dbe7cc2a84e480b21a67485b8af12c44eefcb897dd7fb55dd0f6b595ad54c5c506ee83137a55006d0ba9d5a3789ddc1f9b4f86c0dd6b4914fd5bc2ce3'
#    }

  # config.load_paths << "#{Rails.root}/app/models/imuz_db"

#    require "memcache"
#    CACHE = MemCache.new('127.0.0.1')

    ActionMailer::Base.delivery_method = :smtp
    ActionMailer::Base.default :charset => 'UTF-8'
    ActionMailer::Base.smtp_settings = {
        :address => 'mail.nanogames.com.br',
        :authentication => :login,
        :domain => 'nanogames.com.br',
        :user_name => 'online@nanogames.com.br',
        :password => '$NaN0$',
        :enable_starttls_auto => false
    }

    ActiveRecord::Base.include_root_in_json = false

    # config.action_mailer.default_url_options = { :host => 'localhost:3000' }
    config.middleware.insert_after 'ActionDispatch::ShowExceptions', HoptoadNotifier::Rack
  end
end
