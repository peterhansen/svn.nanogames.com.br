module BigEndianTypes
  class EOFException < Exception
  end
  
  class UTFDataFormatException < Exception
  end
  
   EULER = 2.718281828
  
  ##
  #
  ##
  class InputStream
    
    def initialize( s )
      # posição de leitura atual (vai de zero a @string.length - 1)
      @pos = 0
      # string que representa os bytes do stream
      @string = s
    end
    
    ##
    # Lê um byte (sem sinal) do stream.
    ##
    def read_ubyte()
      byte = @string[ @pos ]
      @pos += 1
      
      if ( @pos > @string.length )
        raise EOFException
      end
      
      return byte
    end
    
    
    ##
    # Lê um byte (com sinal) do stream
    ##
    def read_byte()
      b = read_ubyte()
      return ( b & 0xff ) > 0x7f ? b - 0x100 : b # TODO melhor algoritmo?
    end
    
    
    ##
    #
    ##
    def read_char()
      return [ read_ushort() ].pack( 'U' )
    end
    
    
    ##
    #
    ##
    def read_short()
      return ( ( read_byte() << 8 ) | ( read_ubyte() ) )
    end
    
    
    ##
    #
    ##
    def read_ushort()
      return ( ( ( read_byte() & 0xff ) << 8 ) | ( read_byte() & 0xff ) )
    end
    
    
    ##
    #
    ##
    def read_int()
      return ( ( ( read_byte()  ) << 24 ) | 
               ( ( read_ubyte()  ) << 16 ) | 
               ( ( read_ubyte()  ) <<  8 ) | 
                 ( read_ubyte()  ) )
    end
    
    
    ##
    #
    ##
    def read_uint() # TODO corrigir
      i = read_int()
      return ( ~i ) + 1
    end
    
    
    ##
    #
    ##
    def read_long()
      return  ( ( ( read_byte() ) << 56 ) |
                ( ( read_ubyte() ) << 48 ) |
                ( ( read_ubyte() ) << 40 ) |
                ( ( read_ubyte() ) << 32 ) |
                ( ( read_ubyte() ) << 24 ) |
                ( ( read_ubyte() ) << 16 ) |
                ( ( read_ubyte() ) <<  8 ) |
                ( ( read_ubyte() ) ) )
    end
    
    
    ##
    #
    ##
    def read_float()
      # TODO testar gravação e leitura de floats/doubles
      bits = read_uint()
      s = ( ( bits >> 31 ) == 0) ? 1 : -1
      e = ( ( bits >> 23 ) & 0xff )
      m = ( e == 0 ) ? ( bits & 0x7fffff ) << 1 : ( bits & 0x7fffff ) | 0x800000

      return s * m * ( 2** ( EULER - 150 ) ) # TODO confirmar se algoritmo está ok
    end
    
    
    ##
    #
    ##
    def read_double()
      bits = read_long()
      s = ( ( bits >> 63 ) == 0 ) ? 1 : -1
      e = ( ( bits >> 52 ) & 0x7ff )
      long m = ( e == 0 ) ? ( bits & 0xfffffffffffff ) << 1 : ( bits & 0xfffffffffffff ) | 0x10000000000000
      
      return s * m * ( 2** ( EULER - 1075 ) ) # TODO confirmar algoritmo
    end
    

    ##
    # Lê uma string no formato Java e retorna uma string unicode.
    ##
    def read_string()
      return read_str( false )
    end
    
    
    ##
    # Lê uma string no formato Java e retorna uma string tamb�m no formato Java.
    ##    
    def read_utf()
      return read_str( true )
    end


    ##
    # Lê uma data do stream no formato Java (long) e retorna um Date que representa essa data.
    ##
    def read_date()
      return read_datetime().to_date()
    end


    ##
    # Lê uma data do stream no formato Java (long) e retorna um TimeWithZone que representa essa data.
    ##
    def read_datetime()
      t = read_long()
      # no Java, o tempo é contado em milisegundos desde 01/01/1970, enquanto em C/Unix é em segundos
      return Time.at( t / 1000 ).utc()
    end
    
    
    ##
    # Avan�a 'n' bytes no stream.
    ##
    def skip( n )
      @pos += n
    end
    
    
    ##
    #
    ##
    def reset()
      @pos = 0
    end
    
    
    ##
    # Lê um boolean do stream.
    ##
    def read_boolean()
      return read_ubyte() != 0
    end
    
    
    ##
    #
    ##
    def available()
      return @string.length - @pos
    end
    
    ##
    #
    ##
    def pos()
      return @pos
    end
    
    
    ##
    # 
    ##
    def read_available()
      return @string.slice( @pos..@string.length )
    end
    
    
    ############################################################################
    # MétodOS PRIVADOS                                                         #
    ############################################################################    
    private
    
    ##
    # Lê uma string formatada no padrão Java. Se add_length for true, retorna uma string
    # também no padrão Java (2 bytes indicando o tamanho, e então 'n' caracteres
    # unicode). Caso seja false, retorna a string sem o indicador de tamanho no
    # início.
    ##
    def read_str( add_length )
      length = read_short()

      utf = ""
      if ( add_length )
        utf << length
      end

      # como tudo já está em unicode, não é necessário mais converter
      1.upto( length ) {
        utf << read_ubyte()
      }
      return utf
    end    
    
  end
  
  
  ##
  #
  ##
  class OutputStream
    
    def initialize()
      @string = ""
    end    
    
    
    ##
    #
    ##
    def write_byte( b )
      @string << [ b.to_i() ].pack( 'c' )
    end
    
    
    ##
    #
    ##
    def write_boolean( b )
      write_byte( b ? 1 : 0 )
    end
    
    
    ##
    #
    ##
    def write_char( c )
      write_short( c.class == String ? c[ 0 ] : c )
    end
    
    
    ##
    #
    ##
    def write_short( s )
      @string << [ s.to_i() ].pack( 'n' )
    end
    
    
    ##
    #
    ##
    def write_int( i )
      # não tem pack() de big endian int...
      small_endian = [ i.to_i() ].pack( 'i' )
      big_endian = small_endian.reverse
      @string << big_endian
    end
    
    
    ##
    #
    ##
    def write_long( l )
      le = [ l.to_i() ].pack( 'q' )
      big_endian = le.reverse
      @string << big_endian
    end
    
    
    ##
    #
    ##
    def write_float( f )
      @string << [ f ].pack( 'g' )
    end
    
    
    ##
    #
    ##
    def write_double( d )
      @string << [ d ].pack( 'G' )
    end
    
    
    ##
    # Escreve uma string já no formato java no stream.
    ##
    def write_utf( utf )
      # evita problemas com strings nulas
      utf ||= ''
      # o comprimento inicial é o número de bytes da string, não o número de caracteres (que é sempre menor ou igual ao número de bytes)
      write_short( utf.length )
      @string << utf
    end
    

    ##
    # Escreve uma string no padrão Ruby no stream, convertendo-a para o formato Java.
    ##
    def write_string( string )
      write_utf( string )
    end
		

		##
		# Escreve um array de bytes no stream.
		##
		def write_byte_array( byte_array )
			@string << byte_array.to_s
		end
    
    def write_binary_data( data )
      data ||= ''
      
      write_int( data.length )
      @string << data
    end
    

    ##
    # Escreve uma data do stream no formato Java (long), a partir de um objeto Date.
    ##
    def write_date( date )
      # no Java, o tempo é contado em milisegundos desde 01/01/1970, enquanto em C/Unix é em segundos
      if ( date )
        unless ( date.class == Date )
          write_datetime( date )
        else
          write_long( date.to_datetime.utc.to_time.to_i * 1000 )
        end
      else
        write_long( -1 )
      end
    end
    

    ##
    # Escreve uma data/hora do stream no formato Java (long), a partir de um objeto TimeWithZone.
    ##
    def write_datetime( datetime )
      # no Java, o tempo é contado em milisegundos desde 01/01/1970, enquanto em C/Unix é em segundos
      if ( datetime )
        if ( datetime.class == Date )
          write_date( datetime )
        else
          write_long( datetime.utc.to_time.to_i * 1000 )
        end
      else
        write_long( -1 )
      end
    end
    
    ##
    # Retorna o array de bytes contendo todos os dados escritos, no formato Java.
    ##
    def flush()
      return @string
    end
    
    
    ##
    # Reinicia o stream, descartando todos os dados já gravados.
    ##
    def reset()
      @string = ""
    end
    
    
    ##
    # Escreve os erros presentes num objeto que deriva de ActiveRecord.
    ##
    def write_errors( obj )
      obj.errors.each_full { |msg|
        write_byte( ID_RETURN_CODE )
        write_short( extract_error_code( msg ) )
        write_byte( ID_ERROR_MESSAGE )
        write_string( msg )
      }     
    end
    
  end
  
end
