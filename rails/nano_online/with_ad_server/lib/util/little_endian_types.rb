module LittleEndianTypes
  class EOFException < Exception
  end

  class UTFDataFormatException < Exception
  end

   EULER = 2.718281828

  ##
  #
  ##
  class InputStream

    def initialize( s )
      # posição de leitura atual (vai de zero a @string.length - 1)
      @pos = 0
      # string que representa os bytes do stream
      @string = s
    end

    ##
    # Lê um byte (sem sinal) do stream.
    ##
    def read_ubyte()
      if ( @pos >= @string.length )
        raise EOFException
      end

      byte = @string[ @pos ]
      @pos += 1;

      return ( byte.to_i() & 0xff );
    end


    ##
    # Lê um byte (com sinal) do stream
    ##
    def read_byte()
      b = read_ubyte() & 0xff;
      return b > 0x7f ? b - 0x100 : b ;# TODO melhor algoritmo?
    end

    ##
    #
    ##
    def read_char()
      # Caracteres do tipo wchar_t, de C e C++, têm 32 bits
      # no MacOSX e no Linux, mas 16 bits no Windows. Por
      # enquanto, como só tratamos o iPhone, utilizamos sempre
      # 32 bits
      return [ read_int() ].pack( 'U' );

      # OLD
      #return read_short().pack( 'U' );
    end

    ##
    #
    ##
    def read_short()
      return ( read_ubyte() | ( read_byte() << 8 ) )
    end


    ##
    #
    ##
    def read_ushort()
      s = read_short()
      return ( s < 0 ) ? 65535 + s + 1 : s
    end


    ##
    #
    ##
    def read_int()
#      a = read_ubyte()
#      b = read_ubyte()
#      c = read_ubyte()
#      d = read_byte()
#      puts( "A = #{a}, B = #{b}, C = #{c}, D = #{d}" )
#      return ( a ) | ( b << 8 ) | ( c << 16 ) | ( d << 24 )

      return ( ( ( read_ubyte()  )       ) |
               ( ( read_ubyte()  ) <<  8 ) |
               ( ( read_ubyte()  ) << 16 ) |
               ( ( read_byte()   ) << 24 ) );
    end


    ##
    #
    ##
    def read_uint()
      i = read_int()

      return ( i < 0 ) ? 4294967295 + i + 1 : i
    end


    ##
    #
    ##
    def read_long()
#      a = read_ubyte();
#      b = read_ubyte();
#      c = read_ubyte();
#      d = read_ubyte();
#      e = read_ubyte();
#      f = read_ubyte();
#      g = read_ubyte();
#      h = read_byte();
#      puts( "A = #{a}, B = #{b}, C = #{c}, D = #{d}, E = #{e}, F = #{f}, G = #{g}, H = #{h}" );
#      return ( a ) | ( b << 8 ) | ( c << 16 ) | ( d << 24 ) | ( e << 32 ) | ( f << 40 ) | ( g << 48 ) | ( h << 56 );

       return ( ( ( read_ubyte() )       ) |
                ( ( read_ubyte() ) <<  8 ) |
                ( ( read_ubyte() ) << 16 ) |
                ( ( read_ubyte() ) << 24 ) |
                ( ( read_ubyte() ) << 32 ) |
                ( ( read_ubyte() ) << 40 ) |
                ( ( read_ubyte() ) << 48 ) |
                ( ( read_byte() ) << 56 ) );
    end


    def read_ulong()
      u = read_long()
      return ( u < 0 ) ? 18446744073709551615 + u + 1 : u
    end


    ##
    #
    ##
    def read_float()
      # TODO testar gravação e leitura de floats/doubles
      bits = read_uint()
      s = ( ( bits >> 31 ) == 0) ? 1 : -1
      e = ( ( bits >> 23 ) & 0xff )
      m = ( e == 0 ) ? ( bits & 0x7fffff ) << 1 : ( bits & 0x7fffff ) | 0x800000

      return s * m * ( 2** ( EULER - 150 ) ) # TODO confirmar se algoritmo est� ok
    end


    ##
    #
    ##
    def read_double()
      # TODO testar gravação e leitura de floats/doubles
      bits = read_long()
      s = ( ( bits >> 63 ) == 0 ) ? 1 : -1
      e = ( ( bits >> 52 ) & 0x7ff )
      long m = ( e == 0 ) ? ( bits & 0xfffffffffffff ) << 1 : ( bits & 0xfffffffffffff ) | 0x10000000000000

      return s * m * ( 2** ( EULER - 1075 ) ) # TODO confirmar algoritmo
    end


    ##
    # Lê uma string no formato Java e retorna uma string unicode.
    ##
    def read_string()
      return read_str( false )
    end


    ##
    # Lê uma string no formato Java e retorna uma string tamb�m no formato Java.
    ##
    def read_utf()
      return read_str( true )
    end


    ##
    # Lê uma data do stream no formato Java (long) e retorna um Date que representa essa data.
    ##
    def read_date()
      return read_datetime().to_date()
    end    


    ##
    # Lê uma data do stream no formato Unix (long) e retorna um TimeWithZone que representa essa data.
    ##
    def read_datetime()
      return Time.at( read_long() ).utc()
    end


    ##
    # Avança 'n' bytes no stream.
    ##
    def skip( n )
      @pos += n
    end


    ##
    #
    ##
    def reset()
      @pos = 0
    end


    ##
    # Lê um boolean do stream.
    ##
    def read_boolean()
      return read_ubyte() != 0
    end


    ##
    #
    ##
    def available()
      return @string.length - @pos
    end

    ##
    #
    ##
    def pos()
      return @pos
    end


    ##
    #
    ##
    def read_available()
      return @string.slice( @pos..@string.length )
    end


    ############################################################################
    # MétodOS PRIVADOS                                                         #
    ############################################################################
    private

    ##
    # Lê uma string formatada no nosso padrão C++. Se add_length for true, retorna uma string
    # com 2 bytes indicando o tamanho em bytes, e então 'n' caracteres unicode. Caso seja false,
    # retorna a string sem o indicador de tamanho no início.
    ##
    def read_str( add_length )

      puts( ">>>>>>>>>>>>>>>> Read Str")

      # Tamanho de cada caractere da string. Assim, a string
      # fica mais flexível, pois podemos utilizar UTF8, UTF16,
      # UTF32, ... Há essa necessidade porque caracteres do tipo
      # wchar_t, de C e C++, têm 32 bits no MacOSX e no Linux, mas
      # 16 bits no Windows
      char_size = read_ubyte();

      puts( ">>>>>>>> Char Size = #{char_size}" );

      # Número de caracteres da string
      str_length = read_short();

      puts( ">>>>>>>> Str Len = #{str_length}" );

      utf = ""
      if ( add_length )
        utf << ( str_length * char_size );
      end

      case( char_size )

        when 2
          # TODO Ainda não foi testado !!!
          1.upto( str_length ) {
            utf << [ read_short() ].pack( 'U' )
          }

        when 4
          1.upto( str_length ) {
            utf << [ read_int() ].pack( 'U' )
          }

      else
      # when 1
          1.upto( str_length ) {
            utf << [ read_byte() ].pack( 'c' )
          }

          puts( ">>>>>>>>>>>>>>>>#{utf}")

      end

      return utf
    end

  end


  ##
  #
  ##
  class OutputStream

    def initialize()
      @string = ""
    end


    ##
    #
    ##
    def write_byte( b )
      @string << [ b.to_i() ].pack( 'c' )
    end
    
    
    ##
    #
    ##
    def write_boolean( b )
      write_byte( b ? 1 : 0 )
    end    


    ##
    #
    ##
    def write_char( c )
      # Caracteres do tipo wchar_t, de C e C++, têm 32 bits
      # no MacOSX e no Linux, mas 16 bits no Windows. Por
      # enquanto, como só tratamos o iPhone, utilizamos sempre
      # 32 bits
      write_int( c.class == String ? c[ 0 ] : c );

      # OLD
      # write_short( c.class == String ? c[ 0 ] : c );
    end


    ##
    # OK
    ##
    def write_short( s )
      @string << [ s.to_i() ].pack( 'v' )
    end


    ##
    # OK
    ##
    def write_int( i )
      le = [ i.to_i() ].pack( 'i' )
      @string << le
    end


    ##
    #
    ##
    def write_long( l )
      le = [ l.to_i() ].pack( 'q' )
      @string << le
    end


    ##
    #
    ##
    def write_float( f )
      @string << [ f ].pack( 'g' )
    end


    ##
    #
    ##
    def write_double( d )
      @string << [ d ].pack( 'G' )
    end


    ##
    # Escreve uma string já no formato java no stream.
    ##
    def write_utf( utf )
      # Evita problemas com strings nulas
      utf ||= ''
      
      # Tamanho de cada caractere da string. Assim, a string
      # fica mais flexível, pois podemos utilizar UTF8, UTF16,
      # UTF32, ... Há essa necessidade porque caracteres do tipo
      # wchar_t, de C e C++, têm 32 bits no MacOSX e no Linux, mas
      # 16 bits no Windows. Por enquanto, como só tratamos o iPhone,
      # utilizamos sempre 32 bits (4 bbytes) quando enviamos uma
      # string para o cliente
      write_byte( 4 );
      
      i = 0;
      utf.each_char { |c|
        i += 1
      }
      
      # Número de caracteres na string
      write_short( i );

      # Escreve a string

      # OLD
      #@string << utf;

      utf.unpack( 'U*' ).each { |c|
        write_char( c );
      }
    end


    ##
    # Escreve uma string no padrão Ruby no stream, convertendo-a para o formato Java.
    ##
    def write_string( string )
      write_utf( string )
    end


		##
		# Escreve um array de bytes no stream.
		##
		def write_byte_array( byte_array )
			@string << byte_array.to_s
		end
    
    
    def write_binary_data( data )
      data ||= ''
      
      write_int( data.length )
      @string << data
    end
    

    ##
    # Escreve uma data do stream no formato Java (long), a partir de um objeto Date.
    ##
    def write_date( date )
      # no Java, o tempo é contado em milisegundos desde 01/01/1970, enquanto em C/Unix é em segundos
      if ( date )
        unless ( date.class == Date )
          write_datetime( date )
        else
          write_long( date.to_datetime.utc.to_time.to_i )
        end
      else
        write_long( -1 )
      end
    end


  ##
  # Escreve uma data do stream no formato Java (long), a partir de um objeto TimeWithZone.
  ##
  def write_datetime( datetime )
      if ( datetime )
        if ( datetime.class == Date )
          write_date( datetime )
        else
          write_long( datetime.utc.to_time.to_i )
      end
      else
        write_long( -1 )
      end
    end


    ##
    # Retorna o array de bytes contendo todos os dados escritos, no formato Java.
    ##
    def flush()
      return @string
    end


    ##
    # Reinicia o stream, descartando todos os dados já gravados.
    ##
    def reset()
      @string = ""
    end


    ##
    # Escreve os erros presentes num objeto que deriva de ActiveRecord.
    ##
    def write_errors( obj )
      obj.errors.each_full { |msg|
        write_byte( ID_RETURN_CODE )
        write_short( extract_error_code( msg ) )
        write_byte( ID_ERROR_MESSAGE )
        write_string( msg )
      }
    end

  end

end
