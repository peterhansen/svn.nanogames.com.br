require 'test_helper'

class ResourceTest < ActiveSupport::TestCase
  fixtures :resources, :resource_types, :resource_files, :ad_piece_versions, :resource_type_elements
  def setup
    @vr = resources(:resource_1)
    @ir = Resource.new

    @type = resource_types(:resource_type_2)
    @file = resource_files(:resource_file_1)
    @piece = ad_piece_versions(:ad_piece_version_1)
    @element1 = resource_type_elements(:resource_type_element1)
    @element2 = resource_type_elements(:resource_type_element2)
  end

  def test_valid_object
    assert(@vr.valid?)
  end

  def test_invalid_object
    assert(!@ir.save)
    assert(@ir.errors.invalid?(:resource_type))
    assert(@ir.errors.invalid?(:resource_files))
  end

  def test_belongs_to_resource_type
    @vr.resource_type = @type
    assert(@vr.save)
    assert_equal(@vr.resource_type, @type)
    assert_equal(@type.resources, [@vr])
  end

  def test_has_many_resource_files
    @vr.resource_files.clear                  # essa instrução apaga os resource_files do banco.
    @file.resource = nil                      # essa instrução funciona porque o resource_file ainda existe como variavel de instancia.
    @vr.resource_files << @file
    assert(@vr.save)
    assert_equal(@vr.resource_files, [@file])
    assert_equal(@file.resource, @vr)
  end

  def test_destroy_dependents
    @vr.resource_files.clear
    @file.resource = nil
    @vr.resource_files << @file
    assert(@vr.save)

    assert(@vr.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {ResourceFile.find(@file.id)}
  end

  def test_has_and_belongs_to_many_ad_piece_version
    @vr.ad_piece_versions.clear
    @piece.resources.clear
    @vr.ad_piece_versions << @piece
    assert(@vr.save)
    assert_equal(@vr.ad_piece_versions, [@piece])
    @piece.reload
    assert_equal(@piece.resources, [@vr])
  end
  
  def test_validate_type_restrictions
    @vr.resource_type.resource_type_elements.clear
    @vr.resource_type.resource_type_elements << @element1
    file = ResourceFile.find_by_resource_file_type_id(@element1.resource_file_type.id)
    @vr.resource_files << file
    @vr.resource_files << file
    assert(!@vr.save)
    assert(@vr.errors.invalid?(:resource_files))
  end
end