require 'test_helper'

class AdChannelVersionTest < ActiveSupport::TestCase
  fixtures  :ad_channel_versions, :ad_channels, :ad_channel_statuses, :ad_piece_versions, :app_versions
  
  def setup
    @vv = ad_channel_versions(:ad_channel_version1)
    @iv = AdChannelVersion.new
    
    @channel = ad_channels(:ad_channel2)
    @status = ad_channel_statuses(:ad_channel_status2)
    @piece = ad_piece_versions(:ad_piece_version2)
    @app = app_versions(:dummy2)
  end
  
  def test_valid_object
    assert(@vv.save)
  end
  
  def test_invalid_object
    assert(!@iv.save)
    assert(@iv.errors.invalid?(:ad_channel))
    assert(@iv.errors.invalid?(:ad_channel_status))
  end
  
  def test_belongs_to_ad_channel
    @vv.ad_channel = nil
    assert(!@vv.save)
    @vv.ad_channel = @channel
    assert(@vv.save)
    assert_equal(@vv.ad_channel, @channel)
    assert(@channel.ad_channel_versions.include?(@vv))
  end
  
  def test_belongs_to_ad_channel_status
    @vv.ad_channel_status = nil
    assert(!@vv.save)
    @vv.ad_channel_status = @status
    assert(@vv.save)
    assert_equal(@vv.ad_channel_status, @status)
    assert(@status.ad_channel_versions.include?(@vv))
  end
  
  def test_has_many_ad_piece_versions
    @vv.ad_piece_versions.clear
    assert(@vv.save)
    @vv.ad_piece_versions << @piece
    assert(@vv.save)
    assert_equal(@vv.ad_piece_versions, [@piece])
    assert_equal(@piece.ad_channel_version, @vv)
  end
  
  def test_destroy_dependent_ad_piece_version
    @vv.ad_piece_versions.clear
    @vv.ad_piece_versions << @piece
    assert(@vv.save)
    
    assert(@vv.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdPieceVersion.find(@piece.id)}
  end
  
  def test_has_and_belongs_to_many_app_versions
    @vv.app_versions.clear
    assert(@vv.save)
    @vv.app_versions << @app
    assert(@vv.save)
    assert_equal(@vv.app_versions, [@app])
    assert(@app.ad_channel_versions.include?(@vv))
  end
end