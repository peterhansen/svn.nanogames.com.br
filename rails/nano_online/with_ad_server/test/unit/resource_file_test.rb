require 'test_helper'

class ResourceFileTest < ActiveSupport::TestCase
  fixtures :resource_files, :resources, :resource_file_types
  def setup
    @irf = ResourceFile.new
    @vrf = resource_files(:resource_file_1)

    @resource = resources(:resource_2)
    @type = resource_file_types(:resource_file_type1)
  end

  def test_invalid_object
    assert(!@irf.save)
    assert(@irf.errors.invalid?(:resource))
    assert(@irf.errors.invalid?(:file))
  end

  def test_valid_object
    assert(@vrf.save)
  end

  def test_belongs_to_resource
    @vrf.resource = nil
    @vrf.resource = @resource
    assert(@vrf.save)
    assert_equal(@vrf.resource, @resource)
    assert_equal(@resource.resource_files, [@vrf])
  end
  
  def test_belongs_to_resource_file_type
    @vrf.resource_file_type = nil
    @vrf.resource_file_type = @type
    assert(@vrf.save)
    assert_equal(@vrf.resource_file_type, @type)
    assert(@type.resource_files.include? @vrf)
  end
end
