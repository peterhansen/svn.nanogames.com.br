require 'test_helper'

class AdPieceTest < ActiveSupport::TestCase
  fixtures :ad_pieces, :ad_piece_versions, :ad_campaigns, :ad_channels
  
  def setup
    @vap = ad_pieces(:ad_piece1)
    @iap = AdPiece.new
    
    @version = ad_piece_versions(:ad_piece_version2)
    @camp = ad_campaigns(:ad_campaign2)
    @channel = ad_channels(:ad_channel2)
  end
  
  def test_valid_object
    assert(@vap.save)
  end
  
  def test_invalid_object
    assert(!@iap.save)
    assert(@iap.errors.invalid?(:ad_channel))
  end
  
  def test_has_many_ad_piece_versions
    @vap.ad_piece_versions.clear
    assert(@vap.save)
    @vap.ad_piece_versions << @version
    assert(@vap.save)
    assert_equal(@vap.ad_piece_versions, [@version])
    @version.reload
    assert_equal(@version.ad_piece, @vap)
  end
  
  def test_destroy_dependent_ad_piece_versions
    @vap.ad_piece_versions.clear
    @version.ad_piece = nil
    @vap.ad_piece_versions << @version
    assert(@vap.save)

    assert(@vap.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdPieceVersion.find(@version.id)}
  end
  
  def test_has_and_belongs_to_many_ad_campaigns
    @vap.ad_campaigns.clear
    assert(@vap.save)
    @vap.ad_campaigns << @camp
    assert(@vap.save)
    assert_equal(@vap.ad_campaigns, [@camp])
    assert(@camp.ad_pieces.include?(@vap))
  end
  
  def test_belongs_to_ad_channel
    @vap.ad_channel = nil
    assert(!@vap.save)
    assert(@vap.errors.invalid?(:ad_channel))
    @vap.ad_channel = @channel
    assert(@vap.save)
    assert_equal(@vap.ad_channel, @channel)
    assert(@channel.ad_pieces.include?(@vap))
  end
end