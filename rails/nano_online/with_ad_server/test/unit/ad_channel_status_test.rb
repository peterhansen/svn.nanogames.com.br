require 'test_helper'

class AdChannelStatusTest < ActiveSupport::TestCase
  fixtures :ad_channel_statuses, :ad_channel_versions, :ad_channels
  
  def setup
    @vs = ad_channel_statuses(:ad_channel_status1)
    @is = AdChannelStatus.new
    
    @version = ad_channel_versions(:ad_channel_version2)
    @channel = ad_channels(:ad_channel2)
  end
  
  def test_valid_object
    assert(@vs.save)
  end
  
  def test_invalid_object
    assert(!@is.save)
    assert(@is.errors.invalid?(:status))
  end
  
  def test_has_many_ad_channel_versions
    @vs.ad_channel_versions.clear
    assert(@vs.save)
    @vs.ad_channel_versions << @version
    assert(@vs.save)
    assert_equal(@vs.ad_channel_versions, [@version])
    @version.reload
    assert_equal(@version.ad_channel_status, @vs)
  end
  
  def test_destroy_dependent_ad_channel_version
    @vs.ad_channel_versions.clear
    @vs.ad_channel_versions << @version
    assert(@vs.save)

    assert(@vs.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdChannelVersion.find(@version.id)}
  end
  
  def test_has_many_ad_channels
    @vs.ad_channels.clear
    assert(@vs.save)
    @vs.ad_channels << @channel
    assert(@vs.save)
    assert_equal(@vs.ad_channels, [@channel])
    @channel.reload
    assert_equal(@channel.ad_channel_status, @vs)
  end
  
  def test_destroy_dependent_ad_channel
    @vs.ad_channels.clear
    @vs.ad_channels << @channel
    assert(@vs.save)

    assert(@vs.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdChannel.find(@channel.id)}
  end
end
