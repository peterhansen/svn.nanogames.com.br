require 'test_helper'

class AdPieceVersionTest < ActiveSupport::TestCase
  fixtures :ad_piece_versions, :ad_pieces, :ad_channel_versions, :resources
  
  def setup
    @vapv = ad_piece_versions(:ad_piece_version1)
    @iapv = AdPieceVersion.new
    
    @piece = ad_pieces(:ad_piece2)
    @channel = ad_channel_versions(:ad_channel_version2)
    @resource = resources(:resource2)
  end
  
  def test_valid_object
    assert(@vapv.save)
  end
  
  def test_invalid_object
    assert(!@iapv.save)
    assert(@iapv.errors.invalid?(:ad_piece))
    assert(@iapv.errors.invalid?(:ad_channel_version))
  end
  
  def test_belongs_to_ad_piece
    @vapv.ad_piece = nil
    assert(!@vapv.save)
    assert(@vapv.errors.invalid?(:ad_piece))
    @vapv.ad_piece = @piece
    assert(@vapv.save)
    assert_equal(@vapv.ad_piece, @piece)
    assert(@piece.ad_piece_versions.include?(@vapv))
  end
  
  def test_belongs_to_ad_channel_version
    @vapv.ad_channel_version = nil
    assert(!@vapv.save)
    assert(@vapv.errors.invalid?(:ad_channel_version))
    @vapv.ad_channel_version = @channel
    assert(@vapv.save)
    assert_equal(@vapv.ad_channel_version, @channel)
    assert(@channel.ad_piece_versions.include?(@vapv))
  end
  
  def test_has_and_belongs_to_many_resources
    @vapv.resources.clear
    assert(@vapv.save)
    @vapv.resources << @resource
    assert(@vapv.save)
    assert_equal(@vapv.resources, [@resource])
    assert(@resource.ad_piece_versions.include?(@vapv))
  end
end
