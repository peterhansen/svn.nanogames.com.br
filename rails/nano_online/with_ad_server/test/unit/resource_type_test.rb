require 'test_helper'

class ResourceTypeTest < ActiveSupport::TestCase
  fixtures :resource_types, :resources, :resource_type_elements, :resource_file_types
  def setup
    @vrt = resource_types(:resource_type_2)
    @irt = ResourceType.new

    @resource = resources(:resource_1)
    @element = resource_type_elements(:resource_type_element1)
    @file_type = resource_file_types(:resource_file_type1)
  end

  def test_valid_object
    assert(@vrt.save)
  end

  def test_invalid_object
    assert(!@irt.save)
    assert(@irt.errors.invalid?(:title))
    assert(@irt.errors.invalid?(:description))
  end

  def test_has_many_resources
    @vrt.resources << @resource
    assert(@vrt.save)
    assert_equal(@vrt.resources, [@resource])
    assert_equal(@resource.resource_type, @vrt)
  end

  def test_destroy_dependent_resource
    @vrt.resources << @resource
    assert(@vrt.save)
    assert(@vrt.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {Resource.find(@resource.id)}
  end
  
  def test_has_many_elements
    @vrt.resource_type_elements << @element
    assert(@vrt.save)
    assert_equal(@vrt.resource_type_elements, [@element])
    assert_equal(@element.resource_type, @vrt)
  end
  
  def test_destroy_dependent_element
    @vrt.resource_type_elements << @element
    assert(@vrt.save)
    assert(@vrt.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {ResourceTypeElement.find(@element.id)}
  end
  
  def test_has_many_resource_file_types
    # @vrt.resource_file_types.clear
    # @vrt.resource_file_types << @file_type
    # @vrt.resource_type_elements.first.quantity = 1
    # assert(@vrt.save)
    # assert(@vrt.destroy)
    # assert_raise(ActiveRecord::RecordNotFound) {ResourceFileType.find(@file_type.id)}
  end
end
