require 'test_helper'

class AdvertiserTest < ActiveSupport::TestCase
  fixtures :advertisers, :partners, :ad_campaigns
  
  def setup
    @va = advertisers(:advertiser1)
    @ia = Advertiser.new
    
    @partner = partners(:mtv)
    @camp = ad_campaigns(:ad_campaign2)
  end
  
  def test_valid_object
    assert(@va.save)
  end
  
  def test_invalid_object
    assert(!@ia.save)
    assert(@ia.errors.invalid?(:partner))
  end
  
  def test_belongs_to_partner
    @va.partner = nil
    assert(!@va.save)
    assert(@va.errors.invalid?(:partner))
    @va.partner = @partner
    assert(@va.save)
    assert_equal(@va.partner, @partner)
    assert(@partner.advertisers.include?(@va))
  end
  
  def test_has_many_ad_campaigns
    @va.ad_campaigns.clear
    assert(@va.save)
    @va.ad_campaigns << @camp
    assert(@va.save)
    assert_equal(@va.ad_campaigns, [@camp])
    assert_equal(@camp.advertiser, @va)
  end
  
  def test_destroy_dependent_ad_campaigns
    @va.ad_campaigns.clear
    @camp.advertiser = nil
    @va.ad_campaigns << @camp
    assert(@va.save)

    assert(@va.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdCampaign.find(@camp.id)}
  end
end
