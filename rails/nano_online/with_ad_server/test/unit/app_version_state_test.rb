require 'test_helper'

class AppVersionStateTest < ActiveSupport::TestCase
  fixtures [:app_version_states, :app_versions]
  
  def setup
    @valid_avs = app_version_states(:avs1)
    @invalid_avs = AppVersionState.new
    
    @invalid_name = long_string(21)
    @av = app_versions("dummy_1.0.0")
  end
  
  def test_try_save_valid_object
    assert(@valid_avs.save)
  end
  
  def test_try_save_invalid_object
    assert(!@invalid_avs.save)
  end
  
  def test_presence_of_attributes
    assert(!@invalid_avs.save)
    assert(@invalid_avs.errors.invalid?(:name))
    assert(@invalid_avs.errors.invalid?(:code))
  end
  
  def test_uniqueness_of_name
    @invalid_avs.name = @valid_avs.name
    @invalid_avs.code = 123
    assert(!@invalid_avs.save)
    assert(@invalid_avs.errors.invalid?(:name))
  end
  
  def test_uniqueness_of_name
    @invalid_avs.name = "valid stuff"
    @invalid_avs.code = @valid_avs.code
    assert(!@invalid_avs.save)
    assert(@invalid_avs.errors.invalid?(:code))
  end
  
  def test_length_of_name
    @valid_avs.name = @invalid_name
    assert(!@valid_avs.save)
  end
  
  def test_insert_valid_app_version
    @valid_avs.app_versions << @av
    assert(@valid_avs.save)
    assert_equal(@av.app_version_state, @valid_avs)
  end
  
  def test_insert_invalid_app_version
    assert_raise(ActiveRecord::AssociationTypeMismatch) { @valid_avs.app_versions << "invalid stuff" }
  end
  
end
