require 'test_helper'

class AppleDownloadReportTest < ActiveSupport::TestCase
  fixtures [ :apple_download_reports, :apple_downloads ]
  
  def setup
    @valid_adr = apple_download_reports( :adr1 )
    @invalid_adr = AppleDownloadReport.new
    
    @invalid_name = long_string( 61 )
    @ad = apple_downloads( :down1 )
  end
  
  def test_try_save_valid
    assert( @valid_adr.save )
  end
  
  def test_try_save_invalid
    assert( !@invalid_adr.save )
    
    assert( @invalid_adr.errors.invalid?( :name ) ) 
  end
  
  def test_invalid_name
    @valid_adr.name = @invalid_name
    assert( !@valid_adr.save )
    assert( @valid_adr.errors.invalid?( :name ) )
  end
  
  def test_valid_name
    @invalid_adr.name = "valid_stuff"
    assert( @invalid_adr.save )
  end
  
  def test_repeated_name
    @invalid_adr.name = @valid_adr.name
    assert( !@invalid_adr.save )
    assert( @invalid_adr.errors.invalid?( :name ) )
  end
  
  def test_insert_valid_apple_downloads
    @valid_adr.apple_downloads << @ad
    assert( @valid_adr.save )
    assert( @ad.apple_download_report == @valid_adr )
  end
  
  def test_insert_invalid_apple_downloads
    assert_raise( ActiveRecord::AssociationTypeMismatch ) { @valid_adr.apple_downloads << "invalid_stuff" }
  end
end