require 'test_helper'

class AdCampaignTest < ActiveSupport::TestCase
  fixtures :ad_campaigns, :apps, :ad_pieces, :advertisers
  
  def setup
    @vc = ad_campaigns(:ad_campaign1)
    @ic = AdCampaign.new
    
    @app = apps(:dummy)
    @piece = ad_pieces(:ad_piece1)
    @advertiser = advertisers(:advertiser1)
  end
  
  def test_valid_object
    assert(@vc.save)
  end
  
  def test_invalid_object
    assert(!@ic.save)
    assert(@ic.errors.invalid?(:advertiser))
  end
  
  def test_has_and_belongs_to_many_apps
    @app.ad_campaigns.clear
    @vc.apps.clear
    # assert(!@vc.save)
    # assert(@vc.errors.invalid?(:apps))
    @vc.apps << @app
    assert(@vc.save)
    assert_equal(@vc.apps, [@app])
    @app.reload
    assert_equal(@app.ad_campaigns, [@vc])
  end

  def test_has_and_belongs_to_many_ad_pieces
    @piece.ad_campaigns.clear
    @vc.ad_pieces.clear
    assert(@vc.save)
    @vc.ad_pieces << @piece
    assert(@vc.save)
    assert_equal(@vc.ad_pieces, [@piece])
    @piece.reload
    assert_equal(@piece.ad_campaigns, [@vc])
  end
  
  def test_belongs_to_advertiser
    @vc.advertiser = nil
    assert(!@vc.save)
    @vc.advertiser = @advertiser
    assert(@vc.save)
    @advertiser.reload
    assert_equal(@vc.advertiser, @advertiser)
    assert(@advertiser.ad_campaigns.include? @vc)
  end
  
  def test_advertiser_dependency
    
  end
end
