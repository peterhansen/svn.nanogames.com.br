require 'test_helper'

# todo não foram testados os métodos que fazem interface com aparelhos nem o mailer
class CustomerTest < ActiveSupport::TestCase
  fixtures :customers, :app_versions, :ranking_entries, :accesses, :downloads, :devices, :facebook_profiles
  def setup
    @ic = Customer.new
    @vc = customers(:dummy1)
    @init_c = init_customer(@vc)

    @appv1 = app_versions(:dummy1)
    @appv2 = app_versions(:dummy2)
    @re1 = ranking_entries(:entry1)
    @re2 = ranking_entries(:entry2)
    @acc1 = accesses(:acc1)
    @down = downloads(:download1)    
    @dev = devices(:device_1)
    @fbp = facebook_profiles(:fbp1)
    
    @string41 = long_string(41)
  end
  
  def init_customer(cus)
    cus.nickname = random_string(10)
    cus.email = "#{random_string(5)}@bla.com"
    
    cus.password = "bla"
    cus.password_confirmation = "bla"
    
    cus.save
    return cus
  end
  
  def test_invalid_object
    assert(!@ic.save)
  end
  
  def test_valid_object
    # como o objeto foi criado por fixture é necessario atribuir valor a esses atributos.
    @vc.password = "teste"
    @vc.password_confirmation = "teste"
    
    assert(@vc.save)
  end
  
  def test_create_customer
    
  end

  def test_presence_of_nickname
    @init_c.nickname = ""
    
    assert(!@init_c.save)
    assert(@init_c.errors.invalid?(:nickname))
  end
  
  def test_length_of_nickname
    @init_c.nickname = random_string(1)
    assert(!@init_c.save)
    assert(@init_c.errors.invalid?(:nickname))
    
    @init_c.nickname = random_string(13)
    assert(!@init_c.save)
    assert(@init_c.errors.invalid?(:nickname))
  end

  def test_uniqueness_of_name
    @init_c.nickname = customers(:dummy2).nickname
    assert(!@init_c.save)
    assert(@init_c.errors.invalid?(:nickname))
  end
  
  def test_presence_of_email
    @init_c.email = ""
    assert(!@init_c.save)
    assert(@init_c.errors.invalid?(:email))
  end
  
  def test_length_of_email
    @ic.email = random_string(3)
    @ic.save
    assert(@ic.errors.invalid?(:email))
    
    @ic.email = random_string(41)
    @ic.save
    assert(@ic.errors.invalid?(:email))
  end

  def test_uniqueness_of_email
    @init_c.email = customers(:dummy2).email
    assert(!@init_c.save)
    assert(@init_c.errors.invalid?(:email))
  end

  def test_email_format
    @ic.email = "teste.com"
    @ic.save
    assert(@ic.errors.invalid?(:email))

    @ic.email = "teste@bla"
    @ic.save
    assert(@ic.errors.invalid?(:email))
  end
  
  def test_password_confirmation_mismatch
    @ic.nickname = random_string(8)
    @ic.email = "#{random_string(8)}@bla.com"
    
    @ic.password = "bla"
    @ic.password_confirmation = "ble"
    
    assert(!@ic.save)
    assert(@ic.errors.invalid?(:password))
    assert(!@ic.errors.invalid?(:nickname))
    assert(!@ic.errors.invalid?(:email))
  end
  
  def test_password_confirmation_missing
    @ic.nickname = random_string(8)
    @ic.email = "#{random_string(8)}@bla.com"
    
    @ic.password = "bla"
    
    assert(!@ic.save)
    assert(@ic.errors.invalid?(:password_confirmation))
  end
  
  def test_length_of_phone
    @init_c.phone_number = random_string(7)
    @init_c.save
    assert(@init_c.errors.invalid?(:phone_number))

    @init_c.phone_number = random_string(21)
    @init_c.save
    assert(@init_c.errors.invalid?(:phone_number))
  end

  def test_length_of_first_name
    @init_c.first_name = random_string(31)
    @init_c.save
    assert(@init_c.errors.invalid?(:first_name))
  end
  
  def test_length_of_last_name
    @init_c.last_name = random_string(31)
    @init_c.save
    assert(@init_c.errors.invalid?(:last_name))
  end

  def test_has_and_belongs_to_many_app_version
    @init_c.app_versions << @appv1 << @appv2
    assert(@init_c.save)
    assert_equal(@init_c.app_versions, [@appv1, @appv2])
    assert_equal(@appv1.customers, [@init_c])
    assert_equal(@appv2.customers, [@init_c])
  end

  def test_has_many_ranking_entries
    @init_c.ranking_entries.clear
    @init_c.ranking_entries << @re1 << @re2
    assert(@init_c.save)
    assert_equal(@init_c.ranking_entries, [@re1, @re2])
    assert_equal(@re1.customer, @init_c)
    assert_equal(@re2.customer, @init_c)
  end

  def test_has_many_accesses
    @init_c.accesses.clear
    @init_c.accesses << @acc1
    assert(@init_c.save)
    assert_equal(@init_c.accesses, [@acc1])
    assert_equal(@acc1.customer, @init_c)
  end

  def test_has_many_downloads
#    @init_c.downloads.clear
#    @init_c.downloads << @down
#    assert(@init_c.save)
#    assert_equal(@init_c.downloads, [@down])
#    assert_equal(@down.customer, @init_c)
  end

  def test_has_many_devices
    @init_c.devices.clear
    @init_c.devices << @dev
    assert(@init_c.save)
    assert_equal(@init_c.devices, [@dev])
    assert_equal(@dev.customers, [@init_c])
  end

  def test_has_one_facebook_profile
    @init_c.facebook_profile = @fbp
    assert(@init_c.save)
    assert_equal(@init_c.facebook_profile, @fbp)
    assert_equal(@fbp.customer, @init_c)
  end

  def test_before_save_default_values
    @init_c.genre = nil
    assert(@init_c.save)
    assert_equal(@init_c.genre, "n")
  end

  def test_filter_new
# todo
  end

  def test_password
    new_password = random_string(18)
    @init_c.password = new_password
    assert(@init_c.save)
    assert_equal(@init_c.password, new_password)
  end

  def test_create_validation_hash
    assert_not_nil(Customer.create_validation_hash(@init_c))
  end

  def test_is_blocked
    @init_c.validate_before = Time.now
    @init_c.save
    assert(@init_c.is_blocked?)

    @init_c.validate_before = Time.now.advance(:months => 1)
    @init_c.save
    assert(@init_c.is_blocked?)

    @init_c.validate_before = nil
    @init_c.save
    assert(!@init_c.is_blocked?)
  end

  def test_authenticate
    @init_c.password = "password"
    @init_c.save
    assert_equal(Customer.authenticate(@init_c.nickname, "password"), @init_c)
    assert_nil(Customer.authenticate(@init_c.nickname, "wrong password"))
  end

  def test_check_password
    @init_c.password = "password"
    @init_c.save
    assert(@init_c.check_password("password"))
    assert(!@init_c.check_password("wrong password"))
  end

  def test_mobile_client
    @init_c.mobile_client = "mob_client"
    @init_c.save
    assert_equal(@init_c.is_mobile_client?, "mob_client")

    @init_c.mobile_client = nil
    @init_c.save
    assert(!@init_c.is_mobile_client?)
  end

  #todo falta testar se o email está sendo enviado
  def test_reset_password
    first_password = random_string(8)
    @init_c.password = first_password
    @init_c.save
    @init_c.reset_password
    assert_not_nil(@init_c.password)
    assert_not_equal(@init_c.password, first_password)
  end

  def test_before_filter
    hash = @init_c.validation_hash
    @init_c.email = "#{random_string(6)}@bla.com"
    @init_c.save
    assert_not_equal(@init_c.validation_hash, hash)
  end

  def random_string(lenght)
    rand(2**256).to_s(36)[1..lenght]
  end
end