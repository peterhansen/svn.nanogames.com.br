require 'test_helper'

class AppleProductCategoryTest < ActiveSupport::TestCase 
  fixtures [:apple_downloads, :apple_product_categories]
  
  def setup
    @invalid_apc = AppleProductCategory.new
    @valid_apc = apple_product_categories(:cat1)
    @ad = apple_downloads(:down1)
  end
  
  def test_try_save_invalid
    assert(!@invalid_apc.valid?)
  end
  
  def test_try_save_valid
    assert_valid(@valid_apc)
  end
  
  def test_category
    @invalid_apc.category = "valid stuff"
    assert_valid(@invalid_apc)
  end
  
  def test_insert_valid_apple_downloads
    @invalid_apc.category = "valid stuff"
    @invalid_apc.apple_downloads << @ad
    assert(@invalid_apc.save)
    assert(@ad.apple_product_category == @invalid_apc)
  end
  
  def test_insert_invalid_apple_downloads
    @invalid_apc.category = "valid stuff"
    assert_raise(ActiveRecord::AssociationTypeMismatch) { @invalid_apc.apple_downloads << "invalid_stuff" }
  end

end
