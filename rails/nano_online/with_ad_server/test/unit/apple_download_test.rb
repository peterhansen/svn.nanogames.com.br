require 'test_helper'

class AppleDownloadTest < ActiveSupport::TestCase 
  fixtures [:apple_downloads, :apps, :apple_download_reports, :countries, :currencies, :apple_product_categories]
  
  def setup
    @invalid_ad = AppleDownload.new
    
    @valid_ad = apple_downloads(:down1)
    @app = apps(:dummy)
    @download_report = apple_download_reports(:adr1)
    @country = countries(:country1)
    @product_category = apple_product_categories(:cat1)
    @currency = currencies(:currency1)
    
    @string21 = long_string(21)
    @string51 = long_string(51)
    @string1025 = long_string(1025)
    
    @f = {
      0 => "service_provider_code", 1 => "service_provider_country_code", 3 => "upc", 4 => "isrc", 5 => "artist_show", 7 => "label_studio_network",
      9 => "units", 10 => "royalty_price", 11 => "begin_date", 12 => "end_date", 13 => "customer_currency", 15 => "royalty_currency", 16 => "preorder",
      17 => "season_pass", 18 => "isan", 20 => "customer_price", 21 => "cma", 22 => "asset_content_flavor", 23 => "vendor_offer_code", 24 => "grid",
      25 => "promo_code", 26 => "parent_identifier"
    }
  end
  
  def test_save_invalid_blank_object
    assert !@invalid_ad.valid?
    
    assert @invalid_ad.errors.invalid?(:units)
    assert @invalid_ad.errors.invalid?(:royalty_price)
    assert @invalid_ad.errors.invalid?(:customer_price)
    assert @invalid_ad.errors.invalid?(:customer_currency)
    assert @invalid_ad.errors.invalid?(:royalty_currency)
    assert @invalid_ad.errors.invalid?(:begin_date)
  end
  
  def test_non_numerical_units
    @invalid_ad.units = "string"
    @invalid_ad.save
    
    assert @invalid_ad.errors.invalid?(:units)
  end
  
  def test_non_numerical_royalty_price
    @invalid_ad.royalty_price = "string"
    @invalid_ad.save
    
    assert @invalid_ad.errors.invalid?(:royalty_price)
  end
  
  def test_non_numerical_customer_price
    @invalid_ad.customer_price = "string"
    @invalid_ad.save
    
    assert @invalid_ad.errors.invalid?(:customer_price)
  end
  
  def test_long_service_provider_code
    @invalid_ad.service_provider_code = @string51
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:service_provider_code)
  end
  
  def test_long_service_provider_country_code
    @invalid_ad.service_provider_country_code = @string51
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:service_provider_country_code)
  end
  
  def test_long_upc
    @invalid_ad.upc = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:upc)
  end
  
  def test_long_isrc
    @invalid_ad.isrc = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:isrc)
  end
  
  def test_long_artist_show
    @invalid_ad.artist_show = @string1025
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:artist_show)
  end
  
  def test_long_label_studio_network
    @invalid_ad.label_studio_network = @string1025
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:label_studio_network)
  end
  
  def test_long_season_pass
    @invalid_ad.season_pass = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:season_pass)
  end
  
  def test_long_isan
    @invalid_ad.isan = @string51
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:isan)
  end
  
  def test_long_cma
    @invalid_ad.cma = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:cma)
  end
  
  def test_long_asset_content_flavor
    @invalid_ad.asset_content_flavor = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:asset_content_flavor)
  end
  
  def test_long_vendor_offer_code
    @invalid_ad.vendor_offer_code = @string1025
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:vendor_offer_code)
  end
  
  def test_long_grid
    @invalid_ad.grid = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:grid)
  end
  
  def test_long_promo_code
    @invalid_ad.promo_code = @string21
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:promo_code)
  end
  
  def test_long_parent_identifier
    @invalid_ad.parent_identifier = @string1025
    @invalid_ad.save
    assert @invalid_ad.errors.invalid?(:parent_identifier)
  end
  
  def test_valide_ad
    assert @valid_ad.valid?
  end
  
  def test_insert_valid_app
    @valid_ad.app = @app
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_invalid_app
    @valid_ad.app = nil
    assert_raise(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_valid_country
    @valid_ad.country = @country
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_invalid_country
    @valid_ad.country = nil
    assert_raise(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_valid_apple_product_category
    @valid_ad.apple_product_category = @product_category
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_invalid_apple_product_category
    @valid_ad.apple_product_category = nil
    assert_raise(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_valid_apple_download_report
    @valid_ad.apple_download_report = @download_report
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_invalid_apple_download_report
    @valid_ad.apple_download_report = nil
    assert_raise(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_valid_cus_currency
    @valid_ad.cus_currency = @currency
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_invalid_cus_currency
    @valid_ad.cus_currency = nil
    assert !@valid_ad.save
    # assert_raise(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_valid_roy_currency
    @valid_ad.roy_currency = @currency
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_insert_invalid_roy_currency
    @valid_ad.roy_currency = nil
    # assert_raise(ActiveRecord::StatementInvalid) { @valid_ad.save }
    assert !@valid_ad.save
  end
  
  def test_insert_valid_begin_date
    @valid_ad.begin_date = 01/01/2000
    assert_nothing_raised(ActiveRecord::StatementInvalid) { @valid_ad.save }
  end
  
  def test_country_exists
    assert_not_nil(@valid_ad.country)
  end
  
  def test_country_name_method
    assert_equal(@valid_ad.country.name, @valid_ad.country_name)
  end
  
  def test_new_from_array
    array = ["apple", "us", @app.apple_title, "", "", "nanogames", "", "", "", 5, 3, "01/01/2000", "01/01/2000", @currency.code, "", @currency.code, "", "", "", "", 4, "", "", "", "", "", ""]
    
    ad = AppleDownload.new_from_array(array)
    ad.apple_download_report = @download_report
    
    assert @valid_ad.save
  end
  
end
