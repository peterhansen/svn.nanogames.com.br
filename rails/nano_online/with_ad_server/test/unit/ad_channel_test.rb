require 'test_helper'

class AdChannelTest < ActiveSupport::TestCase
  fixtures :ad_channels, :ad_pieces, :ad_channel_versions, :ad_channel_statuses
  
  def setup
    @vc = ad_channels(:ad_channel1)
    @ic = AdChannel.new
    
    @piece = ad_pieces(:ad_piece2)
    @version = ad_channel_versions(:ad_channel_version2)
    @status = ad_channel_statuses(:ad_channel_status2)
  end
  
  def test_valid_object
    assert(@vc.save)
  end
  
  def test_invalid_object
    assert(!@ic.save)
    assert(@ic.errors.invalid?(:title))
    assert(@ic.errors.invalid?(:description))
    assert(@ic.errors.invalid?(:ad_channel_status))
  end
  
  def test_has_many_ad_pieces
    @vc.ad_pieces.clear
    assert(@vc.save)
    @vc.ad_pieces << @piece
    assert(@vc.save)
    assert_equal(@vc.ad_pieces, [@piece])
    @piece.reload
    assert_equal(@piece.ad_channel, @vc)
  end
  
  def test_destroy_dependent_ad_piece
    @vc.ad_pieces.clear
    @piece.ad_channel = nil
    @vc.ad_pieces << @piece
    assert(@vc.save)

    assert(@vc.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdPiece.find(@piece.id)}
  end
  
  def test_has_many_ad_channel_versions
    @vc.ad_channel_versions.clear
    assert(@vc.save)
    @vc.ad_channel_versions << @version
    assert(@vc.save)
    assert_equal(@vc.ad_channel_versions, [@version])
    @version.reload
    assert_equal(@version.ad_channel, @vc)
  end
  
  def test_destroy_dependent_ad_channel_version
    @vc.ad_channel_versions.clear
    @version.ad_channel = nil
    @vc.ad_channel_versions << @version
    assert(@vc.save)

    assert(@vc.destroy)
    assert_raise(ActiveRecord::RecordNotFound) {AdChannelVersion.find(@version.id)}
  end
  
  def test_belongs_to_ad_channel_status
    @vc.ad_channel_status = nil
    assert(!@vc.save)
    assert(@vc.errors.invalid?(:ad_channel_status))
    @vc.ad_channel_status = @status
    assert(@vc.save)
    assert_equal(@vc.ad_channel_status, @status)
    assert(@status.ad_channels.include?(@vc))
  end
end
