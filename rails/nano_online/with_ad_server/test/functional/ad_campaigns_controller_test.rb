require 'test_helper'

class AdCampaignsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_campaigns)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ad_campaign
    assert_difference('AdCampaign.count') do
      post :create, :ad_campaign => { }
    end

    assert_redirected_to ad_campaign_path(assigns(:ad_campaign))
  end

  def test_should_show_ad_campaign
    get :show, :id => ad_campaigns(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ad_campaigns(:one).id
    assert_response :success
  end

  def test_should_update_ad_campaign
    put :update, :id => ad_campaigns(:one).id, :ad_campaign => { }
    assert_redirected_to ad_campaign_path(assigns(:ad_campaign))
  end

  def test_should_destroy_ad_campaign
    assert_difference('AdCampaign.count', -1) do
      delete :destroy, :id => ad_campaigns(:one).id
    end

    assert_redirected_to ad_campaigns_path
  end
end
