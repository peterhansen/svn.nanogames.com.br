require 'test_helper'

class AdChannelsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_channels)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ad_channel
    assert_difference('AdChannel.count') do
      post :create, :ad_channel => { }
    end

    assert_redirected_to ad_channel_path(assigns(:ad_channel))
  end

  def test_should_show_ad_channel
    get :show, :id => ad_channels(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ad_channels(:one).id
    assert_response :success
  end

  def test_should_update_ad_channel
    put :update, :id => ad_channels(:one).id, :ad_channel => { }
    assert_redirected_to ad_channel_path(assigns(:ad_channel))
  end

  def test_should_destroy_ad_channel
    assert_difference('AdChannel.count', -1) do
      delete :destroy, :id => ad_channels(:one).id
    end

    assert_redirected_to ad_channels_path
  end
end
