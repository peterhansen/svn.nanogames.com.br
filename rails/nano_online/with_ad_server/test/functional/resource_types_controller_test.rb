require 'test_helper'

class ResourceTypesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:resource_types)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_resource_type
    assert_difference('ResourceType.count') do
      post :create, :resource_type => { }
    end

    assert_redirected_to resource_type_path(assigns(:resource_type))
  end

  def test_should_show_resource_type
    get :show, :id => resource_types(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => resource_types(:one).id
    assert_response :success
  end

  def test_should_update_resource_type
    put :update, :id => resource_types(:one).id, :resource_type => { }
    assert_redirected_to resource_type_path(assigns(:resource_type))
  end

  def test_should_destroy_resource_type
    assert_difference('ResourceType.count', -1) do
      delete :destroy, :id => resource_types(:one).id
    end

    assert_redirected_to resource_types_path
  end
end
