require 'test_helper'

class ResourceFilesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:resource_files)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_resource_file
    assert_difference('ResourceFile.count') do
      post :create, :resource_file => { }
    end

    assert_redirected_to resource_file_path(assigns(:resource_file))
  end

  def test_should_show_resource_file
    get :show, :id => resource_files(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => resource_files(:one).id
    assert_response :success
  end

  def test_should_update_resource_file
    put :update, :id => resource_files(:one).id, :resource_file => { }
    assert_redirected_to resource_file_path(assigns(:resource_file))
  end

  def test_should_destroy_resource_file
    assert_difference('ResourceFile.count', -1) do
      delete :destroy, :id => resource_files(:one).id
    end

    assert_redirected_to resource_files_path
  end
end
