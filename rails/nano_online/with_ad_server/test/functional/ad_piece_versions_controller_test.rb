require 'test_helper'

class AdPieceVersionsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_piece_versions)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ad_piece_version
    assert_difference('AdPieceVersion.count') do
      post :create, :ad_piece_version => { }
    end

    assert_redirected_to ad_piece_version_path(assigns(:ad_piece_version))
  end

  def test_should_show_ad_piece_version
    get :show, :id => ad_piece_versions(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ad_piece_versions(:one).id
    assert_response :success
  end

  def test_should_update_ad_piece_version
    put :update, :id => ad_piece_versions(:one).id, :ad_piece_version => { }
    assert_redirected_to ad_piece_version_path(assigns(:ad_piece_version))
  end

  def test_should_destroy_ad_piece_version
    assert_difference('AdPieceVersion.count', -1) do
      delete :destroy, :id => ad_piece_versions(:one).id
    end

    assert_redirected_to ad_piece_versions_path
  end
end
