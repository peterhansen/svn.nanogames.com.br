require 'test_helper'

class AdvertisersControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:advertisers)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_advertiser
    assert_difference('Advertiser.count') do
      post :create, :advertiser => { }
    end

    assert_redirected_to advertiser_path(assigns(:advertiser))
  end

  def test_should_show_advertiser
    get :show, :id => advertisers(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => advertisers(:one).id
    assert_response :success
  end

  def test_should_update_advertiser
    put :update, :id => advertisers(:one).id, :advertiser => { }
    assert_redirected_to advertiser_path(assigns(:advertiser))
  end

  def test_should_destroy_advertiser
    assert_difference('Advertiser.count', -1) do
      delete :destroy, :id => advertisers(:one).id
    end

    assert_redirected_to advertisers_path
  end
end
