require 'test_helper'

class AdChannelVersionsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_channel_versions)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ad_channel_version
    assert_difference('AdChannelVersion.count') do
      post :create, :ad_channel_version => { }
    end

    assert_redirected_to ad_channel_version_path(assigns(:ad_channel_version))
  end

  def test_should_show_ad_channel_version
    get :show, :id => ad_channel_versions(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ad_channel_versions(:one).id
    assert_response :success
  end

  def test_should_update_ad_channel_version
    put :update, :id => ad_channel_versions(:one).id, :ad_channel_version => { }
    assert_redirected_to ad_channel_version_path(assigns(:ad_channel_version))
  end

  def test_should_destroy_ad_channel_version
    assert_difference('AdChannelVersion.count', -1) do
      delete :destroy, :id => ad_channel_versions(:one).id
    end

    assert_redirected_to ad_channel_versions_path
  end
end
