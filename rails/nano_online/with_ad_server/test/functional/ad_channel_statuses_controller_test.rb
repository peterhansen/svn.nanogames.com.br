require 'test_helper'

class AdChannelStatusesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_channel_statuses)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ad_channel_status
    assert_difference('AdChannelStatus.count') do
      post :create, :ad_channel_status => { }
    end

    assert_redirected_to ad_channel_status_path(assigns(:ad_channel_status))
  end

  def test_should_show_ad_channel_status
    get :show, :id => ad_channel_statuses(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ad_channel_statuses(:one).id
    assert_response :success
  end

  def test_should_update_ad_channel_status
    put :update, :id => ad_channel_statuses(:one).id, :ad_channel_status => { }
    assert_redirected_to ad_channel_status_path(assigns(:ad_channel_status))
  end

  def test_should_destroy_ad_channel_status
    assert_difference('AdChannelStatus.count', -1) do
      delete :destroy, :id => ad_channel_statuses(:one).id
    end

    assert_redirected_to ad_channel_statuses_path
  end
end
