require 'test_helper'

class AdPiecesControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ad_pieces)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ad_piece
    assert_difference('AdPiece.count') do
      post :create, :ad_piece => { }
    end

    assert_redirected_to ad_piece_path(assigns(:ad_piece))
  end

  def test_should_show_ad_piece
    get :show, :id => ad_pieces(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ad_pieces(:one).id
    assert_response :success
  end

  def test_should_update_ad_piece
    put :update, :id => ad_pieces(:one).id, :ad_piece => { }
    assert_redirected_to ad_piece_path(assigns(:ad_piece))
  end

  def test_should_destroy_ad_piece
    assert_difference('AdPiece.count', -1) do
      delete :destroy, :id => ad_pieces(:one).id
    end

    assert_redirected_to ad_pieces_path
  end
end
