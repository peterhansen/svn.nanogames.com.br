require 'test_helper'

class RankingSystemsControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:ranking_systems)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_ranking_system
    assert_difference('RankingSystem.count') do
      post :create, :ranking_system => { }
    end

    assert_redirected_to ranking_system_path(assigns(:ranking_system))
  end

  def test_should_show_ranking_system
    get :show, :id => ranking_systems(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => ranking_systems(:one).id
    assert_response :success
  end

  def test_should_update_ranking_system
    put :update, :id => ranking_systems(:one).id, :ranking_system => { }
    assert_redirected_to ranking_system_path(assigns(:ranking_system))
  end

  def test_should_destroy_ranking_system
    assert_difference('RankingSystem.count', -1) do
      delete :destroy, :id => ranking_systems(:one).id
    end

    assert_redirected_to ranking_systems_path
  end
end
