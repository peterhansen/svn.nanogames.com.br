require 'test_helper'
require 'big_endian_types'
require 'little_endian_types'

class ResourceFlowsTest < ActionController::IntegrationTest
  fixtures :customers, :languages
  
  def setup
    @customer = customers(:dummy1)
    @language = languages(:language1)
    
    @data = create_global_data
  end
  
  def test_get_resource
    post "/resources/index", @data[2..-1]
    assert_response :success
    # assert_redirected_to "http://www.nanogames.com.br"
    
    post "/resources/get_resource", @data[2..-1]
    assert_response :success
    assert_equal Resource.send_data(get_output_stream, "bla"), response.body
  end
  
  def create_global_data
    @output = BigEndianTypes::OutputStream.new
    
    @output.write_string( "\211NANO\r\n\032\n" );
    @output.write_byte(   ID_NANO_ONLINE_VERSION );
    @output.write_utf(    "0.0.31" );
    @output.write_byte(   ID_APP );
    @output.write_utf(    "DMMY" );
    @output.write_byte(   ID_CUSTOMER_ID );
    @output.write_int(    @customer.id );  
    @output.write_byte(   ID_LANGUAGE );
    @output.write_byte(   @language.byte_identifier );
    @output.write_byte(   ID_APP_VERSION );
    @output.write_utf(    "1.0.0" );
    
    @output.write_byte(   ID_SPECIFIC_DATA );
    
    @output.flush
  end
end
  