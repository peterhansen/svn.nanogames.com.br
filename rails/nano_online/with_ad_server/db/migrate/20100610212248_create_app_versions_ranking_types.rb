class CreateAppVersionsRankingTypes < ActiveRecord::Migration
def self.up
    create_table :app_versions_ranking_types do |t|
      t.integer :app_version_id
      t.integer :ranking_type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :app_versions_ranking_types
  end
end
