class CreateCustomers < ActiveRecord::Migration
  def self.up
    create_table :customers do |t|
      t.column :nickname,  :text, :limit => 20, :null => false
      t.column :first_name,  :text, :limit => 30
      t.column :last_name,  :text, :limit => 30
      t.column :email, :text, :limit => 40
      t.column :phone_number, :text, :limit => 20
      t.column :birthday, :date
      t.column :cpf, :text, :limit => 11
      t.column :genre, :string, :limit => 1, :null => false, :default => 'n'
      
      # password
      t.column :hashed_password, :string
      t.column :salt, :string      
      
      t.timestamps
    end
    
    #cria a tabela de países
    create_table :countries do |t|
      t.column :name, :text, :limit => 40, :null => false
      t.column :iso_name_2, :text, :limit => 2, :null => false
      t.column :iso_name_3, :text, :limit => 3, :null => false
      t.column :iso_number, :text, :limit => 3, :null => false
    end
    
    #cria as tabelas de relacionamento N-N
    create_table :customers_devices, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :customer_id, :integer, :null => false
    end

    add_index :customers_devices, [ :device_id, :customer_id ]
    add_index :customers_devices, :customer_id   
    
    create_table :app_versions_customers, :id => false do |t|
      t.column :app_version_id, :integer, :null => false
      t.column :customer_id, :integer, :null => false
    end 
    
    add_index :app_versions_customers, [ :app_version_id, :customer_id ]
    add_index :app_versions_customers, :customer_id    
  end

  
  def self.down
    begin remove_index :customers_devices, :customer_id rescue true end
    begin remove_index :customers_devices, [ :device_id, :customer_id ] rescue true end
    begin remove_index app_versions_customers, :customer_id rescue true end
    begin remove_index app_versions_customers, [ :app_version_id, :customer_id ] rescue true end

    begin drop_table :countries rescue true end
    begin drop_table :app_versions_customers rescue true end
    begin drop_table :customers_devices rescue true end
    begin drop_table :customers rescue true end
  end
end
