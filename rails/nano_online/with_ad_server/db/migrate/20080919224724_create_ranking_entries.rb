class CreateRankingEntries < ActiveRecord::Migration
  def self.up
    create_table :ranking_entries do |t|
      t.column :customer_id,  :integer, :null => false
      t.column :app_version_id,  :integer, :null => false
      t.column :device_id,  :integer
      # utiliza 9 pois o Rails converte inteiros de até 8 bytes para um int em MySQL, quando deveria ser um bigint
      t.column :score,  :integer, :limit => 9, :null => false
      # subtipo do ranking (ex.: maior velocidade, menor tempo, pontuação). Caso
      # o subtipo seja negativo, sua ordem é crescente (ex.: quanto menor o tempo,
      # melhor).
      t.column :sub_type, :integer, :limit => 1, :default => 0, :null => false
      
      t.timestamps
    end
    
    add_index :ranking_entries, [ :customer_id, :app_version_id, :device_id ], :name => 'by_customer_id_appver_id_and_dev_id'
    add_index :ranking_entries, [ :customer_id, :app_version_id ]
    add_index :ranking_entries, :app_version_id
    add_index :ranking_entries, :device_id
    add_index :ranking_entries, :score
    add_index :ranking_entries, :sub_type
  end

  def self.down
    begin remove_index :ranking_entries, :sub_type rescue true end
    begin remove_index :ranking_entries, :score rescue true end
    begin remove_index :ranking_entries, :device_id rescue true end
    begin remove_index :ranking_entries, :app_version_id rescue true end
    begin remove_index :ranking_entries, [ :customer_id, :app_version_id ] rescue true end
    begin remove_index :ranking_entries, [ :customer_id, :app_version_id, :device_id ] rescue true end
    
    begin drop_table :ranking_entries rescue true end
  end
end
