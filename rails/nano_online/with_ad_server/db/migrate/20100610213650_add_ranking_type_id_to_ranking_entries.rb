class AddRankingTypeIdToRankingEntries < ActiveRecord::Migration
  def self.up
    add_column :ranking_entries, :ranking_type_id, :integer
  end

  def self.down
    remove_column :ranking_entries, :ranking_type_id
  end
end
