class CreateAdChannelStatuses < ActiveRecord::Migration
  def self.up
    create_table :ad_channel_statuses do |t|
      t.string    :status, :null => false
      t.timestamps
    end
  end

  def self.down
    begin drop_table :ad_channel_statuses rescue true end
  end
end
