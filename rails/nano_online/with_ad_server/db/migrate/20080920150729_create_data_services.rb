class CreateDataServices < ActiveRecord::Migration
  def self.up
    create_table :data_services do |t|
      t.column :name, :text, :limit => 20
      t.column :max_speed, :integer 
      
      t.timestamps
    end

    #cria as tabelas de relacionamento N-N
    create_table :data_services_devices, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :data_service_id, :integer, :null => false
    end

    add_index :data_services_devices, [ :device_id, :data_service_id ]
    add_index :data_services_devices, :data_service_id
  end

  def self.down
    begin remove_index :data_services_devices, :data_service_id rescue true end
    begin remove_index :data_services_devices, [ :device_id, :data_service_id ] rescue true end

    begin drop_table :data_services_devices rescue true end
    begin drop_table :data_services rescue true end
  end
end
