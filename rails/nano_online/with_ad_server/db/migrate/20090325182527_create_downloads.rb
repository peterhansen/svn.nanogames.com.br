class CreateDownloads < ActiveRecord::Migration
  def self.up
    create_table :accesses do |t|
      t.column :user_agent, :text, :limit => 256, :null => false
      t.column :phone_number, :text, :limit => 20
      # endereão IP do usuário que se conecta (X_FORWARDED_FOR)
      t.column :ip_address, :text
      t.column :http_path, :text, :null => false
      t.column :http_referrer, :text
      t.column :phone_number, :text, :limit => 20
      t.column :customer_id, :integer
      # apesar do modelo poder ser obtido através do user-agent, essa informação é armazenada no caso do usuário
      # informar outro aparelho ou baixar através do pc
      t.column :device_id, :integer

      t.timestamps
    end

    create_table :downloads do |t|
      t.column :access_id, :integer, :null => false
      t.column :app_version_id, :integer, :null => false
      # apesar do modelo já poder ser obtido através da tabela Accesses, o usuário pode ter informado um outro aparelho
      t.column :device_id, :integer, :null => false
      
      t.timestamps
    end

#    add_index :accesses, [ :customer_id, :device_id ]
#    add_index :accesses, [ :device_id ]
#    add_index :downloads, [ :app_version_id, :access_id ]
#    add_index :downloads, [ :access_id ]
  end
  

  def self.down
#    remove_index :accesses, [ :customer_id, :device_id ]
#    remove_index :accesses, [ :device_id ]
#    remove_index :downloads, [ :app_version_id, :access_id ]
#    remove_index :downloads, [ :access_id ]

    begin drop_table :accesses rescue true end
    begin drop_table :downloads rescue true end
  end
end
