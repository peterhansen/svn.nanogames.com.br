class CreateCurrency < ActiveRecord::Migration
  def self.up
    create_table :currencies, :force => true do |t|
      t.string  :code,        :limit => 3 
      t.string  :name,        :limit => 20
      t.timestamps
    end
  end

  def self.down
#    drop_table :currencies
  end
end
