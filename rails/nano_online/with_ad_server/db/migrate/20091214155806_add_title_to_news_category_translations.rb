class AddTitleToNewsCategoryTranslations < ActiveRecord::Migration
  def self.up
    add_column :news_category_translations, :title, :string
  end

  def self.down
    remove_column :news_category_translations, :title
  end
end
