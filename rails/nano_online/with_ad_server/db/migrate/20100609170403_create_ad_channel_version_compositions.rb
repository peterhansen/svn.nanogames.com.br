class CreateAdChannelVersionCompositions < ActiveRecord::Migration
  def self.up
    create_table :ad_channel_version_compositions do |t|
      t.integer :ad_channel_version_id, :null => false
      t.integer :resource_type_id, :null => false
      t.integer :code, :null => false

      t.timestamps
    end
    add_index(:ad_channel_version_compositions, [:ad_channel_version_id, :resource_type_id, :code], :name => "channel_version_resource_type_index", :unique => true)
  end

  def self.down
    drop_table :ad_channel_version_compositions
  end
end
