class CreateAppleProductCategory < ActiveRecord::Migration
  def self.up
    create_table :apple_product_categories, :force => true do |t|
      t.integer     :category, :null => false
      
      t.timestamps
    end
  end

  def self.down
    begin drop_table :apple_product_categories rescue true end
  end
end
