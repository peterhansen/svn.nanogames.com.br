class CreateRankingTypes < ActiveRecord::Migration
  def self.up
    create_table :ranking_types do |t|
      t.string :name
      t.text :description
      t.integer :unit_format_id
      t.integer :sub_type
      t.integer :minimum
      t.integer :maximum

      t.timestamps
    end
  end

  def self.down
    drop_table :ranking_types
  end
end