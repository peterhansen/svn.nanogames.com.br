class CreateFacebookApps < ActiveRecord::Migration
  def self.up
    create_table :facebook_apps do |t|
      t.integer :app_id, :null => false
      
      t.string :api_secret, :null => false
      t.string :api_key, :null => false
      t.integer :facebook_app_id, :limit => 9, :null => false, :references => nil
      t.string :canvas_name, :null => false
      t.text :display_name, :limit => 256, :null => false

      t.timestamps
    end
  end

  def self.down
    begin drop_table :facebook_apps rescue true end
  end
end
