class AddUserInfo < ActiveRecord::Migration
  def self.up
    rename_column :users, :name, :nickname

    add_column :users, :email,      :text, :limit => 40, :null => false
    add_column :users, :first_name, :text, :limit => 30
    add_column :users, :last_name, :text, :limit => 30
  end

  
  def self.down
    rename_column :users, :nickname, :name

    remove_column :users, :email
    remove_column :users, :first_name
    remove_column :users, :last_name
  end
end
