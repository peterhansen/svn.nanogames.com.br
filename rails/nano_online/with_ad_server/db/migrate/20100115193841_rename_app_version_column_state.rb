class RenameAppVersionColumnState < ActiveRecord::Migration
  def self.up
    rename_column :app_versions, :state, :app_version_state_id
  end

  def self.down
    rename_column :app_versions, :app_version_state_id, :state
  end
end
