class CreateAdChannels < ActiveRecord::Migration
  def self.up
    create_table :ad_channels do |t|
      t.string  :title, :null => false
      t.text    :description, :null => false
      t.integer :ad_channel_status_id, :null => false

      t.timestamps
    end

    add_index :ad_channels, :ad_channel_status_id
  end

  def self.down
    begin remove_index :ad_channels, :ad_channel_status_id rescue true end
    
#    drop_table :ad_channels
  end
end
