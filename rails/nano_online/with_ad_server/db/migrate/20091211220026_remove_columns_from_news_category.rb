class RemoveColumnsFromNewsCategory < ActiveRecord::Migration
  def self.up
    remove_column :news_feed_categories, :title
    remove_column :news_feed_categories, :description
  end

  def self.down
    add_column :news_feed_categories, :title,        :text,    :limit => 256,  :null => false
    add_column :news_feed_categories, :description,  :text,    :limit => 512
  end
end
