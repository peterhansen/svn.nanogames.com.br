class AddVisibleToNewsFeedCategory < ActiveRecord::Migration
  def self.up
		add_column :news_feed_categories, :visible, :boolean, :default => false
  end

  def self.down
		remove_column :news_feed_categories, :visible
  end
end
