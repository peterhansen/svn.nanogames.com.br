class RenameDevicesDeviceBugs < ActiveRecord::Migration
  def self.up
    rename_table :devices_device_bugs, :device_bugs_devices
  end

  def self.down
    rename_table :device_bugs_devices, :devices_device_bugs
  end
end
