class CreateAdChannelVersions < ActiveRecord::Migration
  def self.up
    create_table :ad_channel_versions do |t|
      t.integer   :ad_channel_id, :null => false
      t.integer   :ad_channel_status_id, :null => false
			t.string    :title, :null => false

      t.timestamps
    end

    create_table :ad_channel_versions_resource_types, :id => false do |t|
      t.integer   :ad_channel_version_id, :null => false
      t.integer   :resource_type_id, :null => false
    end

    create_table :ad_channel_versions_app_versions, :id => false do |t|
      t.integer   :ad_channel_version_id, :null => false
      t.integer   :app_version_id, :null => false
    end

		add_index :ad_channel_versions_resource_types, [ :ad_channel_version_id, :resource_type_id ], :name => 'by_channel_version_and_resource_type'
    add_index :ad_channel_versions_resource_types, :ad_channel_version_id, :name => 'by_channel_version'

		add_index :ad_channel_versions_app_versions, [ :ad_channel_version_id, :app_version_id ], :name => 'by_channel_version_and_app_version'
    add_index :ad_channel_versions_app_versions, :app_version_id
  end

  def self.down
    begin drop_table :ad_channel_versions rescue true end
    begin drop_table :ad_channel_versions_resource_types rescue true end
    begin drop_table :ad_channel_versions_app_versions rescue true end
    # TODO remove_index
  end
end
