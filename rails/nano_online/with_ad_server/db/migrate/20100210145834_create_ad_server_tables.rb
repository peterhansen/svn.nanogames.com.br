class CreateAdServerTables < ActiveRecord::Migration
  def self.up
 
    create_table :ad_channel_status do |t|
      t.string    :status, :null => false
      t.timestamps
    end
    
  end

  def self.down
  end
end
