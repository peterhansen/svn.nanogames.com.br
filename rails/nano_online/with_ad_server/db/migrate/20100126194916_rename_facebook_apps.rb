class RenameFacebookApps < ActiveRecord::Migration
  def self.up
    rename_table( :facebook_apps, :facebook_datas )

    # por causa do Nano Online, pode ocorrer de uma app do Facebook não ter correspondente nas aplicações Nano
    change_column :facebook_datas, :app_id, :integer, :null => true
  end

  def self.down
    change_column :facebook_datas, :app_id, :integer, :null => false
    rename_table( :facebook_datas, :facebook_apps )
  end
end
