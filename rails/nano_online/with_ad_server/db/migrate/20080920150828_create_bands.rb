class CreateBands < ActiveRecord::Migration
  def self.up
    create_table :bands do |t|
      t.column :name, :text, :limit => 20, :null => false
      t.column :frequency, :integer, :null => false
      
      t.timestamps
    end
    
    create_table :bands_devices, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :band_id, :integer, :null => false
    end

    add_index :bands_devices, [ :device_id, :band_id ]
    add_index :bands_devices, :band_id
  end

  def self.down
    begin remove_index :bands_devices, :band_id rescue true end
    begin remove_index :bands_devices, [ :device_id, :band_id ] rescue true end
    
    begin drop_table :bands_devices rescue true end
    begin drop_table :bands rescue true end
  end
end
