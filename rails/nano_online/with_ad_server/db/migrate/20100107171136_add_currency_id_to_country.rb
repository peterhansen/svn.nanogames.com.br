class AddCurrencyIdToCountry < ActiveRecord::Migration
  def self.up
    add_column :countries, :currency_id, :integer
  end

  def self.down
    remove_column :countries, :currency_id
  end
end
