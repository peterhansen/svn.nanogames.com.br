class CreateNewsFeeds < ActiveRecord::Migration
  def self.up
    create_table :news_feed_categories do |t|
      t.text :title,        :limit => 256, :null => false
      t.text :description,  :limit => 512
      #TODO ícone de categoria de news feed

      t.timestamps
    end

    create_table :news_feeds do |t|
      t.integer :news_feed_category_id,     :null => false
      t.text    :title,    :limit => 256,   :null => false
      t.text    :content,  :limit => 4096,  :null => false
      t.text    :url,      :limit => 256

      t.timestamps
    end

    add_index :news_feeds, :news_feed_category_id
  end

  def self.down
    begin drop_table :news_feeds rescue true end
    begin drop_table :news_feed_categories rescue true end
  end
end
