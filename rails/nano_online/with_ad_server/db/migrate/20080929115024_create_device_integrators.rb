class CreateDeviceIntegrators < ActiveRecord::Migration
  def self.up
    create_table :device_integrators do |t|
      t.column :device_id, :integer, :null => false
      t.column :integrator_id, :integer, :null => false
      t.column :identifier, :text, :limit => 30
      t.timestamps
    end
    
    add_index :device_integrators, [ :device_id, :integrator_id ]
    add_index :device_integrators, [ :integrator_id ]
  end

  def self.down
    begin remove_index :device_integrators, [ :integrator_id ] rescue true end
    begin remove_index :device_integrators, [ :device_id, :integrator_id ] rescue true end
    
    begin drop_table :device_integrators rescue true end
  end
end
