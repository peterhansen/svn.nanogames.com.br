class CreateAppVersionStates < ActiveRecord::Migration
  def self.up
    create_table :app_version_states do |t|
      t.string :name,   :null => false,   :limit => 20
      t.integer :code,  :null => false

      t.timestamps
    end
  end

  def self.down
    begin drop_table :app_version_states rescue true end
  end
end
