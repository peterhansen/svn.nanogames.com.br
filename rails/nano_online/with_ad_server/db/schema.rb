# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20100615144608) do

  create_table "accesses", :force => true do |t|
    t.column "user_agent",    :text,     :null => false
    t.column "phone_number",  :text
    t.column "ip_address",    :text
    t.column "http_path",     :text,     :null => false
    t.column "http_referrer", :text
    t.column "customer_id",   :integer
    t.column "device_id",     :integer
    t.column "created_at",    :datetime
    t.column "updated_at",    :datetime
    t.column "phone_hash",    :string
  end

  add_index "accesses", ["customer_id"], :name => "customer_id"
  add_index "accesses", ["device_id"], :name => "device_id"

  create_table "ad_campaigns", :force => true do |t|
    t.column "advertiser_id", :integer,  :null => false
    t.column "title",         :string,   :null => false
    t.column "expires_at",    :datetime
    t.column "created_at",    :datetime
    t.column "updated_at",    :datetime
  end

  add_index "ad_campaigns", ["advertiser_id"], :name => "advertiser_id"

  create_table "ad_campaigns_ad_pieces", :id => false, :force => true do |t|
    t.column "ad_campaign_id", :integer,  :null => false
    t.column "ad_piece_id",    :integer,  :null => false
    t.column "created_at",     :datetime
    t.column "updated_at",     :datetime
  end

  add_index "ad_campaigns_ad_pieces", ["ad_campaign_id"], :name => "ad_campaign_id"
  add_index "ad_campaigns_ad_pieces", ["ad_piece_id"], :name => "ad_piece_id"

  create_table "ad_campaigns_apps", :id => false, :force => true do |t|
    t.column "ad_campaign_id", :integer, :null => false
    t.column "app_id",         :integer, :null => false
  end

  add_index "ad_campaigns_apps", ["ad_campaign_id"], :name => "ad_campaign_id"
  add_index "ad_campaigns_apps", ["app_id"], :name => "app_id"

  create_table "ad_channel_status", :force => true do |t|
    t.column "status",     :string,   :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "ad_channel_statuses", :force => true do |t|
    t.column "status",     :string,   :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "ad_channel_versions", :force => true do |t|
    t.column "ad_channel_id",        :integer,  :null => false
    t.column "ad_channel_status_id", :integer,  :null => false
    t.column "title",                :string,   :null => false
    t.column "created_at",           :datetime
    t.column "updated_at",           :datetime
  end

  add_index "ad_channel_versions", ["ad_channel_id"], :name => "ad_channel_id"
  add_index "ad_channel_versions", ["ad_channel_status_id"], :name => "ad_channel_status_id"

  create_table "ad_channel_versions_app_versions", :id => false, :force => true do |t|
    t.column "ad_channel_version_id", :integer, :null => false
    t.column "app_version_id",        :integer, :null => false
  end

  add_index "ad_channel_versions_app_versions", ["ad_channel_version_id", "app_version_id"], :name => "by_channel_version_and_app_version"
  add_index "ad_channel_versions_app_versions", ["app_version_id"], :name => "index_ad_channel_versions_app_versions_on_app_version_id"

  create_table "ad_channel_versions_resource_types", :id => false, :force => true do |t|
    t.column "ad_channel_version_id", :integer, :null => false
    t.column "resource_type_id",      :integer, :null => false
  end

  add_index "ad_channel_versions_resource_types", ["resource_type_id"], :name => "resource_type_id"
  add_index "ad_channel_versions_resource_types", ["ad_channel_version_id", "resource_type_id"], :name => "by_channel_version_and_resource_type"
  add_index "ad_channel_versions_resource_types", ["ad_channel_version_id"], :name => "by_channel_version"

  create_table "ad_channels", :force => true do |t|
    t.column "title",                :string,   :null => false
    t.column "description",          :text,     :null => false
    t.column "ad_channel_status_id", :integer,  :null => false
    t.column "created_at",           :datetime
    t.column "updated_at",           :datetime
  end

  add_index "ad_channels", ["ad_channel_status_id"], :name => "index_ad_channels_on_ad_channel_status_id"

  create_table "ad_piece_version_visualizations", :force => true do |t|
    t.column "ad_piece_version_id", :integer,  :null => false
    t.column "customer_id",         :integer
    t.column "device_id",           :integer
    t.column "time_start",          :datetime, :null => false
    t.column "time_end",            :datetime
    t.column "created_at",          :datetime
    t.column "updated_at",          :datetime
  end

  add_index "ad_piece_version_visualizations", ["ad_piece_version_id"], :name => "ad_piece_version_id"
  add_index "ad_piece_version_visualizations", ["customer_id"], :name => "customer_id"
  add_index "ad_piece_version_visualizations", ["device_id"], :name => "device_id"

  create_table "ad_piece_versions", :force => true do |t|
    t.column "ad_piece_id",           :integer,  :null => false
    t.column "ad_channel_version_id", :integer,  :null => false
    t.column "title",                 :string,   :null => false
    t.column "created_at",            :datetime
    t.column "updated_at",            :datetime
  end

  add_index "ad_piece_versions", ["ad_piece_id"], :name => "ad_piece_id"
  add_index "ad_piece_versions", ["ad_channel_version_id"], :name => "ad_channel_version_id"

  create_table "ad_piece_versions_resources", :id => false, :force => true do |t|
    t.column "ad_piece_version_id", :integer, :null => false
    t.column "resource_id",         :integer, :null => false
  end

  add_index "ad_piece_versions_resources", ["ad_piece_version_id", "resource_id"], :name => "by_ad_piece_version_and_resource"
  add_index "ad_piece_versions_resources", ["resource_id"], :name => "index_ad_piece_versions_resources_on_resource_id"

  create_table "ad_pieces", :force => true do |t|
    t.column "title",         :string,   :null => false
    t.column "description",   :text
    t.column "ad_channel_id", :integer,  :null => false
    t.column "created_at",    :datetime
    t.column "updated_at",    :datetime
  end

  add_index "ad_pieces", ["ad_channel_id"], :name => "ad_channel_id"

  create_table "ads", :force => true do |t|
    t.column "ad_piece_id", :integer,  :null => false
    t.column "expires_at",  :datetime
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  add_index "ads", ["ad_piece_id"], :name => "ad_piece_id"

  create_table "advertisers", :force => true do |t|
    t.column "partner_id", :integer,  :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  add_index "advertisers", ["partner_id"], :name => "partner_id"

  create_table "app_categories", :force => true do |t|
    t.column "name",       :text,     :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "app_categories_apps", :id => false, :force => true do |t|
    t.column "app_id",          :integer, :null => false
    t.column "app_category_id", :integer, :null => false
  end

  add_index "app_categories_apps", ["app_id", "app_category_id"], :name => "index_app_categories_apps_on_app_id_and_app_category_id"
  add_index "app_categories_apps", ["app_category_id"], :name => "index_app_categories_apps_on_app_category_id"

  create_table "app_files", :force => true do |t|
    t.column "app_version_id", :integer,  :null => false
    t.column "filename",       :text
    t.column "created_at",     :datetime
    t.column "updated_at",     :datetime
  end

  add_index "app_files", ["app_version_id"], :name => "app_version_id"

  create_table "app_version_states", :force => true do |t|
    t.column "name",       :string,   :limit => 20, :null => false
    t.column "code",       :integer,                :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "app_version_updates", :force => true do |t|
    t.column "app_version_id",         :integer
    t.column "updated_app_version_id", :integer
    t.column "required",               :boolean, :default => true, :null => false
  end

  add_index "app_version_updates", ["updated_app_version_id"], :name => "updated_app_version_id"
  add_index "app_version_updates", ["app_version_id", "updated_app_version_id"], :name => "index_app_version_updated_app_version"
  add_index "app_version_updates", ["app_version_id"], :name => "index_app_version_updates_on_app_version_id"

  create_table "app_versions", :force => true do |t|
    t.column "app_id",               :integer,                 :null => false
    t.column "release_notes",        :text
    t.column "number",               :text,                    :null => false
    t.column "app_version_state_id", :integer,  :default => 0
    t.column "created_at",           :datetime
    t.column "updated_at",           :datetime
  end

  add_index "app_versions", ["app_id"], :name => "app_id"

  create_table "app_versions_customers", :id => false, :force => true do |t|
    t.column "app_version_id", :integer, :null => false
    t.column "customer_id",    :integer, :null => false
  end

  add_index "app_versions_customers", ["app_version_id", "customer_id"], :name => "index_app_versions_customers_on_app_version_id_and_customer_id"
  add_index "app_versions_customers", ["customer_id"], :name => "index_app_versions_customers_on_customer_id"

  create_table "app_versions_families", :id => false, :force => true do |t|
    t.column "app_version_id", :integer, :null => false
    t.column "family_id",      :integer, :null => false
  end

  add_index "app_versions_families", ["app_version_id", "family_id"], :name => "index_app_versions_families_on_app_version_id_and_family_id"
  add_index "app_versions_families", ["family_id"], :name => "index_app_versions_families_on_family_id"

  create_table "app_versions_submissions", :id => false, :force => true do |t|
    t.column "app_version_id", :integer, :null => false
    t.column "submission_id",  :integer, :null => false
  end

  add_index "app_versions_submissions", ["app_version_id", "submission_id"], :name => "ver_subm_id"
  add_index "app_versions_submissions", ["submission_id"], :name => "index_app_versions_submissions_on_submission_id"

  create_table "apple_download_reports", :force => true do |t|
    t.column "name",       :text
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "apple_downloads", :force => true do |t|
    t.column "apple_download_report_id",      :integer,                                                   :null => false
    t.column "service_provider_code",         :text
    t.column "service_provider_country_code", :text
    t.column "upc",                           :text
    t.column "isrc",                          :text
    t.column "artist_show",                   :text
    t.column "app_id",                        :integer,                                                   :null => false
    t.column "label_studio_network",          :text
    t.column "apple_product_category_id",     :integer,                                                   :null => false
    t.column "units",                         :integer,                                                   :null => false
    t.column "royalty_price",                 :decimal,  :precision => 5, :scale => 2,                    :null => false
    t.column "begin_date",                    :date
    t.column "end_date",                      :date
    t.column "customer_currency",             :integer,                                                   :null => false
    t.column "country_id",                    :integer,                                                   :null => false
    t.column "royalty_currency",              :integer,                                                   :null => false
    t.column "preorder",                      :boolean,                                :default => false
    t.column "season_pass",                   :text
    t.column "isan",                          :text
    t.column "customer_price",                :decimal,  :precision => 5, :scale => 2,                    :null => false
    t.column "cma",                           :text
    t.column "asset_content_flavor",          :text
    t.column "vendor_offer_code",             :text
    t.column "grid",                          :text
    t.column "promo_code",                    :text
    t.column "parent_identifier",             :text
    t.column "created_at",                    :datetime
    t.column "updated_at",                    :datetime
  end

  add_index "apple_downloads", ["apple_download_report_id"], :name => "apple_download_report_id"
  add_index "apple_downloads", ["app_id"], :name => "app_id"
  add_index "apple_downloads", ["apple_product_category_id"], :name => "apple_product_category_id"
  add_index "apple_downloads", ["country_id"], :name => "country_id"
  add_index "apple_downloads", ["customer_currency"], :name => "fk_customer_currency"
  add_index "apple_downloads", ["royalty_currency"], :name => "fk_royalty_currency"

  create_table "apple_product_categories", :force => true do |t|
    t.column "category",   :integer,  :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "apps", :force => true do |t|
    t.column "name",                    :text,     :null => false
    t.column "name_short",              :text,     :null => false
    t.column "description_short",       :text
    t.column "description_medium",      :text
    t.column "description_long",        :text
    t.column "created_at",              :datetime
    t.column "updated_at",              :datetime
    t.column "apple_title",             :string
    t.column "apple_vendor_identifier", :string
    t.column "apple_identifier",        :string
  end

  create_table "apps_partners", :id => false, :force => true do |t|
    t.column "app_id",     :integer, :null => false
    t.column "partner_id", :integer, :null => false
  end

  add_index "apps_partners", ["partner_id", "app_id"], :name => "index_apps_partners_on_partner_id_and_app_id"
  add_index "apps_partners", ["app_id"], :name => "index_apps_partners_on_app_id"

  create_table "bands", :force => true do |t|
    t.column "name",       :text,     :null => false
    t.column "frequency",  :integer,  :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "bands_devices", :id => false, :force => true do |t|
    t.column "device_id", :integer, :null => false
    t.column "band_id",   :integer, :null => false
  end

  add_index "bands_devices", ["device_id", "band_id"], :name => "index_bands_devices_on_device_id_and_band_id"
  add_index "bands_devices", ["band_id"], :name => "index_bands_devices_on_band_id"

  create_table "bands_operators", :id => false, :force => true do |t|
    t.column "operator_id", :integer, :null => false
    t.column "band_id",     :integer, :null => false
  end

  add_index "bands_operators", ["operator_id"], :name => "operator_id"
  add_index "bands_operators", ["band_id"], :name => "band_id"

  create_table "cldc_versions", :force => true do |t|
    t.column "version",     :text,     :null => false
    t.column "description", :text
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  create_table "countries", :force => true do |t|
    t.column "name",        :text,    :null => false
    t.column "iso_name_2",  :text,    :null => false
    t.column "iso_name_3",  :text,    :null => false
    t.column "iso_number",  :text,    :null => false
    t.column "currency_id", :integer
  end

  add_index "countries", ["currency_id"], :name => "currency_id"

  create_table "currencies", :force => true do |t|
    t.column "code",       :string,   :limit => 3
    t.column "name",       :string,   :limit => 20
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "customers", :force => true do |t|
    t.column "nickname",        :text,                                   :null => false
    t.column "first_name",      :text
    t.column "last_name",       :text
    t.column "email",           :text
    t.column "phone_number",    :text
    t.column "birthday",        :date
    t.column "cpf",             :text
    t.column "genre",           :string,   :limit => 1, :default => "n", :null => false
    t.column "hashed_password", :string
    t.column "salt",            :string
    t.column "created_at",      :datetime
    t.column "updated_at",      :datetime
    t.column "validation_hash", :string
    t.column "validate_before", :datetime
    t.column "validated_at",    :datetime
  end

  create_table "customers_devices", :id => false, :force => true do |t|
    t.column "device_id",   :integer, :null => false
    t.column "customer_id", :integer, :null => false
  end

  add_index "customers_devices", ["device_id", "customer_id"], :name => "index_customers_devices_on_device_id_and_customer_id"
  add_index "customers_devices", ["customer_id"], :name => "index_customers_devices_on_customer_id"

  create_table "data_services", :force => true do |t|
    t.column "name",       :text
    t.column "max_speed",  :integer
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "data_services_devices", :id => false, :force => true do |t|
    t.column "device_id",       :integer, :null => false
    t.column "data_service_id", :integer, :null => false
  end

  add_index "data_services_devices", ["device_id", "data_service_id"], :name => "index_data_services_devices_on_device_id_and_data_service_id"
  add_index "data_services_devices", ["data_service_id"], :name => "index_data_services_devices_on_data_service_id"

  create_table "device_bugs", :force => true do |t|
    t.column "name",        :text,     :null => false
    t.column "description", :text,     :null => false
    t.column "workaround",  :text
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  create_table "device_bugs_devices", :id => false, :force => true do |t|
    t.column "device_id",     :integer, :null => false
    t.column "device_bug_id", :integer, :null => false
  end

  add_index "device_bugs_devices", ["device_id", "device_bug_id"], :name => "index_devices_device_bugs_on_device_id_and_device_bug_id"
  add_index "device_bugs_devices", ["device_bug_id"], :name => "index_devices_device_bugs_on_device_bug_id"

  create_table "device_integrators", :force => true do |t|
    t.column "device_id",     :integer,  :null => false
    t.column "integrator_id", :integer,  :null => false
    t.column "identifier",    :text
    t.column "created_at",    :datetime
    t.column "updated_at",    :datetime
  end

  add_index "device_integrators", ["device_id", "integrator_id"], :name => "index_device_integrators_on_device_id_and_integrator_id"
  add_index "device_integrators", ["integrator_id"], :name => "index_device_integrators_on_integrator_id"

  create_table "devices", :force => true do |t|
    t.column "midp_version_id",       :integer
    t.column "cldc_version_id",       :integer
    t.column "vendor_id",             :integer,               :null => false
    t.column "family_id",             :integer
    t.column "commercial_name",       :text
    t.column "model",                 :text,                  :null => false
    t.column "screen_width",          :integer,  :limit => 2
    t.column "screen_height_partial", :integer,  :limit => 2
    t.column "screen_height_full",    :integer,  :limit => 2
    t.column "jar_size_max",          :integer,  :limit => 2
    t.column "memory_heap",           :integer
    t.column "memory_image",          :integer
    t.column "user_agent",            :text
    t.column "pointer_events",        :boolean
    t.column "rms_total_size",        :integer
    t.column "rms_max_record_size",   :integer
    t.column "created_at",            :datetime
    t.column "updated_at",            :datetime
  end

  add_index "devices", ["vendor_id"], :name => "index_devices_on_vendor_id"
  add_index "devices", ["midp_version_id"], :name => "index_devices_on_midp_version_id"
  add_index "devices", ["cldc_version_id"], :name => "index_devices_on_cldc_version_id"
  add_index "devices", ["family_id"], :name => "index_devices_on_family_id"

  create_table "devices_optional_apis", :id => false, :force => true do |t|
    t.column "device_id",       :integer, :null => false
    t.column "optional_api_id", :integer, :null => false
  end

  add_index "devices_optional_apis", ["device_id", "optional_api_id"], :name => "index_devices_optional_apis_on_device_id_and_optional_api_id"
  add_index "devices_optional_apis", ["optional_api_id"], :name => "index_devices_optional_apis_on_optional_api_id"

  create_table "devices_submissions", :id => false, :force => true do |t|
    t.column "device_id",     :integer, :null => false
    t.column "submission_id", :integer, :null => false
  end

  add_index "devices_submissions", ["device_id", "submission_id"], :name => "index_devices_submissions_on_device_id_and_submission_id"
  add_index "devices_submissions", ["submission_id"], :name => "index_devices_submissions_on_submission_id"

  create_table "download_codes", :force => true do |t|
    t.column "code",        :text,     :null => false
    t.column "file",        :text,     :null => false
    t.column "used_at",     :datetime
    t.column "download_id", :integer,  :null => false
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  add_index "download_codes", ["download_id"], :name => "index_download_codes_on_download_id"

  create_table "downloads", :force => true do |t|
    t.column "access_id",      :integer,  :null => false
    t.column "app_version_id", :integer,  :null => false
    t.column "device_id",      :integer,  :null => false
    t.column "created_at",     :datetime
    t.column "updated_at",     :datetime
  end

  add_index "downloads", ["access_id"], :name => "access_id"
  add_index "downloads", ["app_version_id"], :name => "app_version_id"
  add_index "downloads", ["device_id"], :name => "device_id"

  create_table "facebook_datas", :force => true do |t|
    t.column "app_id",          :integer
    t.column "api_secret",      :string,   :null => false
    t.column "api_key",         :string,   :null => false
    t.column "facebook_app_id", :integer,  :null => false
    t.column "canvas_name",     :string,   :null => false
    t.column "display_name",    :text,     :null => false
    t.column "created_at",      :datetime
    t.column "updated_at",      :datetime
  end

  add_index "facebook_datas", ["app_id"], :name => "app_id"

  create_table "facebook_profiles", :force => true do |t|
    t.column "uid",                  :integer,  :null => false
    t.column "customer_id",          :integer,  :null => false
    t.column "first_name",           :string
    t.column "last_name",            :string
    t.column "name",                 :string
    t.column "pic_small",            :string
    t.column "pic_big",              :string
    t.column "pic_square",           :string
    t.column "pic",                  :string
    t.column "profile_update_time",  :datetime
    t.column "timezone",             :string
    t.column "religion",             :string
    t.column "birthday",             :string
    t.column "birthday_date",        :string
    t.column "sex",                  :string
    t.column "relationship_status",  :string
    t.column "political",            :string
    t.column "activities",           :string
    t.column "interests",            :string
    t.column "is_app_user",          :boolean
    t.column "music",                :string
    t.column "tv",                   :string
    t.column "movies",               :string
    t.column "books",                :string
    t.column "quotes",               :string
    t.column "about_me",             :string
    t.column "notes_count",          :integer
    t.column "wall_count",           :integer
    t.column "status",               :string
    t.column "online_presence",      :string
    t.column "locale",               :string
    t.column "proxied_email",        :string
    t.column "profile_url",          :string
    t.column "pic_small_with_logo",  :string
    t.column "pic_big_with_logo",    :string
    t.column "pic_square_with_logo", :string
    t.column "pic_with_logo",        :string
    t.column "allowed_restrictions", :string
    t.column "verified",             :string
    t.column "profile_blurb",        :string
    t.column "username",             :string
    t.column "website",              :string
    t.column "is_blocked",           :boolean
    t.column "created_at",           :datetime
    t.column "updated_at",           :datetime
  end

  add_index "facebook_profiles", ["customer_id"], :name => "customer_id"

  create_table "families", :force => true do |t|
    t.column "name",        :text,                        :null => false
    t.column "ignored",     :boolean,  :default => false, :null => false
    t.column "description", :text
    t.column "install_jar", :boolean,  :default => false, :null => false
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  create_table "integrators", :force => true do |t|
    t.column "name",       :text,     :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "integrators_operators", :id => false, :force => true do |t|
    t.column "integrator_id", :integer, :null => false
    t.column "operator_id",   :integer, :null => false
  end

  add_index "integrators_operators", ["integrator_id", "operator_id"], :name => "index_integrators_operators_on_integrator_id_and_operator_id"
  add_index "integrators_operators", ["operator_id"], :name => "index_integrators_operators_on_operator_id"

  create_table "languages", :force => true do |t|
    t.column "name",            :string,   :null => false
    t.column "created_at",      :datetime
    t.column "updated_at",      :datetime
    t.column "byte_identifier", :integer
  end

  create_table "midlet_reports", :force => true do |t|
    t.column "status_code",    :integer,  :limit => 2
    t.column "access_id",      :integer,               :null => false
    t.column "download_id",    :integer
    t.column "app_id",         :integer
    t.column "created_at",     :datetime
    t.column "updated_at",     :datetime
    t.column "app_version_id", :integer
  end

  add_index "midlet_reports", ["download_id", "app_id"], :name => "index_midlet_reports_on_download_id_and_app_id"
  add_index "midlet_reports", ["app_id"], :name => "index_midlet_reports_on_app_id"
  add_index "midlet_reports", ["access_id"], :name => "index_midlet_reports_on_access_id"
  add_index "midlet_reports", ["app_version_id"], :name => "app_version_id"

  create_table "midp_versions", :force => true do |t|
    t.column "version",     :text,     :null => false
    t.column "description", :text
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  create_table "news_category_translations", :force => true do |t|
    t.column "news_feed_category_id", :integer,                 :null => false
    t.column "language_id",           :integer,                 :null => false
    t.column "description",           :string,   :limit => 512
    t.column "created_at",            :datetime
    t.column "updated_at",            :datetime
    t.column "title",                 :string
  end

  add_index "news_category_translations", ["news_feed_category_id"], :name => "news_feed_category_id"
  add_index "news_category_translations", ["language_id"], :name => "language_id"

  create_table "news_feed_categories", :force => true do |t|
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
    t.column "visible",    :boolean,  :default => false
  end

  create_table "news_feed_translations", :force => true do |t|
    t.column "news_feed_id", :integer,  :null => false
    t.column "content",      :text,     :null => false
    t.column "title",        :text,     :null => false
    t.column "url",          :text
    t.column "language_id",  :integer,  :null => false
    t.column "created_at",   :datetime
    t.column "updated_at",   :datetime
  end

  add_index "news_feed_translations", ["news_feed_id"], :name => "news_feed_id"
  add_index "news_feed_translations", ["language_id"], :name => "language_id"

  create_table "news_feeds", :force => true do |t|
    t.column "news_feed_category_id", :integer,  :null => false
    t.column "created_at",            :datetime
    t.column "updated_at",            :datetime
  end

  add_index "news_feeds", ["news_feed_category_id"], :name => "index_news_feeds_on_news_feed_category_id"

  create_table "operators", :force => true do |t|
    t.column "name",       :text,     :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "optional_apis", :force => true do |t|
    t.column "name",        :text,                  :null => false
    t.column "description", :text
    t.column "jsr_index",   :integer,  :limit => 2
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  add_index "optional_apis", ["jsr_index"], :name => "index_optional_apis_on_jsr_index"

  create_table "partners", :force => true do |t|
    t.column "name",       :text,     :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "ranking_entries", :force => true do |t|
    t.column "customer_id",    :integer,                              :null => false
    t.column "app_version_id", :integer,                              :null => false
    t.column "device_id",      :integer
    t.column "score",          :integer,                              :null => false
    t.column "sub_type",       :integer,  :limit => 1, :default => 0, :null => false
    t.column "created_at",     :datetime
    t.column "updated_at",     :datetime
    t.column "record_time",    :datetime
  end

  add_index "ranking_entries", ["customer_id", "app_version_id", "device_id"], :name => "by_customer_id_appver_id_and_dev_id"
  add_index "ranking_entries", ["customer_id", "app_version_id"], :name => "index_ranking_entries_on_customer_id_and_app_version_id"
  add_index "ranking_entries", ["app_version_id"], :name => "index_ranking_entries_on_app_version_id"
  add_index "ranking_entries", ["device_id"], :name => "index_ranking_entries_on_device_id"
  add_index "ranking_entries", ["score"], :name => "index_ranking_entries_on_score"
  add_index "ranking_entries", ["sub_type"], :name => "index_ranking_entries_on_sub_type"

  create_table "resource_file_types", :force => true do |t|
    t.column "name",       :string,                :null => false
    t.column "client_id",  :integer,  :limit => 2
    t.column "content",    :string
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "resource_files", :force => true do |t|
    t.column "resource_id",           :integer,                              :null => false
    t.column "resource_file_type_id", :integer,                              :null => false
    t.column "internal_id",           :integer,  :limit => 3, :default => 0
    t.column "created_at",            :datetime
    t.column "updated_at",            :datetime
    t.column "file_file_name",        :string
    t.column "file_content_type",     :string
    t.column "file_file_size",        :integer
    t.column "file_updated_at",       :datetime
  end

  add_index "resource_files", ["resource_id"], :name => "resource_id"
  add_index "resource_files", ["resource_file_type_id"], :name => "resource_file_type_id"

  create_table "resource_type_elements", :force => true do |t|
    t.column "resource_type_id",      :integer,                 :null => false
    t.column "resource_file_type_id", :integer,                 :null => false
    t.column "quantity",              :integer,  :default => 1, :null => false
    t.column "created_at",            :datetime
    t.column "updated_at",            :datetime
  end

  add_index "resource_type_elements", ["resource_type_id", "resource_file_type_id"], :name => "type_elements_index", :unique => true
  add_index "resource_type_elements", ["resource_file_type_id"], :name => "resource_file_type_id"

  create_table "resource_types", :force => true do |t|
    t.column "title",       :string,                :null => false
    t.column "description", :text,                  :null => false
    t.column "client_id",   :integer,  :limit => 2
    t.column "created_at",  :datetime
    t.column "updated_at",  :datetime
  end

  create_table "resources", :force => true do |t|
    t.column "resource_type_id", :integer,                              :null => false
    t.column "description",      :text
    t.column "internal_id",      :integer,  :limit => 3, :default => 0
    t.column "title",            :string,                               :null => false
    t.column "created_at",       :datetime
    t.column "updated_at",       :datetime
  end

  add_index "resources", ["resource_type_id"], :name => "resource_type_id"

  create_table "submissions", :force => true do |t|
    t.column "integrator_id", :integer,  :null => false
    t.column "created_at",    :datetime
    t.column "updated_at",    :datetime
  end

  add_index "submissions", ["integrator_id"], :name => "index_submissions_on_integrator_id"

  create_table "undetected_user_agents", :force => true do |t|
    t.column "user_agent", :text,                                 :null => false
    t.column "accesses",   :integer,  :limit => 3, :default => 1, :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "user_roles", :force => true do |t|
    t.column "name",       :text
    t.column "layout",     :text
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  create_table "users", :force => true do |t|
    t.column "nickname",        :string
    t.column "hashed_password", :string
    t.column "salt",            :string
    t.column "partner_id",      :integer
    t.column "user_role_id",    :integer,  :null => false
    t.column "created_at",      :datetime
    t.column "updated_at",      :datetime
    t.column "email",           :text,     :null => false
    t.column "first_name",      :text
    t.column "last_name",       :text
  end

  add_index "users", ["partner_id"], :name => "partner_id"
  add_index "users", ["user_role_id"], :name => "user_role_id"

  create_table "vendors", :force => true do |t|
    t.column "name",       :text,     :null => false
    t.column "created_at", :datetime
    t.column "updated_at", :datetime
  end

  add_foreign_key "accesses", ["customer_id"], "customers", ["id"], :name => "accesses_ibfk_1"
  add_foreign_key "accesses", ["device_id"], "devices", ["id"], :name => "accesses_ibfk_2"

  add_foreign_key "ad_campaigns", ["advertiser_id"], "advertisers", ["id"], :name => "ad_campaigns_ibfk_1"

  add_foreign_key "ad_campaigns_ad_pieces", ["ad_campaign_id"], "ad_campaigns", ["id"], :name => "ad_campaigns_ad_pieces_ibfk_1"
  add_foreign_key "ad_campaigns_ad_pieces", ["ad_piece_id"], "ad_pieces", ["id"], :name => "ad_campaigns_ad_pieces_ibfk_2"

  add_foreign_key "ad_campaigns_apps", ["ad_campaign_id"], "ad_campaigns", ["id"], :name => "ad_campaigns_apps_ibfk_1"
  add_foreign_key "ad_campaigns_apps", ["app_id"], "apps", ["id"], :name => "ad_campaigns_apps_ibfk_2"

  add_foreign_key "ad_channel_versions", ["ad_channel_id"], "ad_channels", ["id"], :name => "ad_channel_versions_ibfk_1"
  add_foreign_key "ad_channel_versions", ["ad_channel_status_id"], "ad_channel_statuses", ["id"], :name => "ad_channel_versions_ibfk_2"

  add_foreign_key "ad_channel_versions_app_versions", ["ad_channel_version_id"], "ad_channel_versions", ["id"], :name => "ad_channel_versions_app_versions_ibfk_1"
  add_foreign_key "ad_channel_versions_app_versions", ["app_version_id"], "app_versions", ["id"], :name => "ad_channel_versions_app_versions_ibfk_2"

  add_foreign_key "ad_channel_versions_resource_types", ["ad_channel_version_id"], "ad_channel_versions", ["id"], :name => "ad_channel_versions_resource_types_ibfk_1"
  add_foreign_key "ad_channel_versions_resource_types", ["resource_type_id"], "resource_types", ["id"], :name => "ad_channel_versions_resource_types_ibfk_2"

  add_foreign_key "ad_channels", ["ad_channel_status_id"], "ad_channel_statuses", ["id"], :name => "ad_channels_ibfk_1"

  add_foreign_key "ad_piece_version_visualizations", ["ad_piece_version_id"], "ad_piece_versions", ["id"], :name => "ad_piece_version_visualizations_ibfk_1"
  add_foreign_key "ad_piece_version_visualizations", ["customer_id"], "customers", ["id"], :name => "ad_piece_version_visualizations_ibfk_2"
  add_foreign_key "ad_piece_version_visualizations", ["device_id"], "devices", ["id"], :name => "ad_piece_version_visualizations_ibfk_3"

  add_foreign_key "ad_piece_versions", ["ad_piece_id"], "ad_pieces", ["id"], :name => "ad_piece_versions_ibfk_1"
  add_foreign_key "ad_piece_versions", ["ad_channel_version_id"], "ad_channel_versions", ["id"], :name => "ad_piece_versions_ibfk_2"

  add_foreign_key "ad_piece_versions_resources", ["ad_piece_version_id"], "ad_piece_versions", ["id"], :name => "ad_piece_versions_resources_ibfk_1"
  add_foreign_key "ad_piece_versions_resources", ["resource_id"], "resources", ["id"], :name => "ad_piece_versions_resources_ibfk_2"

  add_foreign_key "ad_pieces", ["ad_channel_id"], "ad_channels", ["id"], :name => "ad_pieces_ibfk_1"

  add_foreign_key "ads", ["ad_piece_id"], "ad_pieces", ["id"], :name => "ads_ibfk_1"

  add_foreign_key "advertisers", ["partner_id"], "partners", ["id"], :name => "advertisers_ibfk_1"

  add_foreign_key "app_categories_apps", ["app_id"], "apps", ["id"], :name => "app_categories_apps_ibfk_1"
  add_foreign_key "app_categories_apps", ["app_category_id"], "app_categories", ["id"], :name => "app_categories_apps_ibfk_2"

  add_foreign_key "app_files", ["app_version_id"], "app_versions", ["id"], :name => "app_files_ibfk_1"

  add_foreign_key "app_version_updates", ["app_version_id"], "app_versions", ["id"], :name => "app_version_updates_ibfk_1"
  add_foreign_key "app_version_updates", ["updated_app_version_id"], "app_versions", ["id"], :name => "app_version_updates_ibfk_2"

  add_foreign_key "app_versions", ["app_id"], "apps", ["id"], :name => "app_versions_ibfk_1"

  add_foreign_key "app_versions_customers", ["app_version_id"], "app_versions", ["id"], :name => "app_versions_customers_ibfk_1"
  add_foreign_key "app_versions_customers", ["customer_id"], "customers", ["id"], :name => "app_versions_customers_ibfk_2"

  add_foreign_key "app_versions_families", ["app_version_id"], "app_versions", ["id"], :name => "app_versions_families_ibfk_1"
  add_foreign_key "app_versions_families", ["family_id"], "families", ["id"], :name => "app_versions_families_ibfk_2"

  add_foreign_key "app_versions_submissions", ["app_version_id"], "app_versions", ["id"], :name => "app_versions_submissions_ibfk_1"
  add_foreign_key "app_versions_submissions", ["submission_id"], "submissions", ["id"], :name => "app_versions_submissions_ibfk_2"

  add_foreign_key "apple_downloads", ["apple_download_report_id"], "apple_download_reports", ["id"], :name => "apple_downloads_ibfk_1"
  add_foreign_key "apple_downloads", ["app_id"], "apps", ["id"], :name => "apple_downloads_ibfk_2"
  add_foreign_key "apple_downloads", ["apple_product_category_id"], "apple_product_categories", ["id"], :name => "apple_downloads_ibfk_3"
  add_foreign_key "apple_downloads", ["country_id"], "countries", ["id"], :name => "apple_downloads_ibfk_4"
  add_foreign_key "apple_downloads", ["customer_currency"], "currencies", ["id"], :name => "fk_customer_currency"
  add_foreign_key "apple_downloads", ["royalty_currency"], "currencies", ["id"], :name => "fk_royalty_currency"

  add_foreign_key "apps_partners", ["app_id"], "apps", ["id"], :name => "apps_partners_ibfk_1"
  add_foreign_key "apps_partners", ["partner_id"], "partners", ["id"], :name => "apps_partners_ibfk_2"

  add_foreign_key "bands_devices", ["device_id"], "devices", ["id"], :name => "bands_devices_ibfk_1"
  add_foreign_key "bands_devices", ["band_id"], "bands", ["id"], :name => "bands_devices_ibfk_2"

  add_foreign_key "bands_operators", ["operator_id"], "operators", ["id"], :name => "bands_operators_ibfk_1"
  add_foreign_key "bands_operators", ["band_id"], "bands", ["id"], :name => "bands_operators_ibfk_2"

  add_foreign_key "countries", ["currency_id"], "currencies", ["id"], :name => "countries_ibfk_1"

  add_foreign_key "customers_devices", ["device_id"], "devices", ["id"], :name => "customers_devices_ibfk_1"
  add_foreign_key "customers_devices", ["customer_id"], "customers", ["id"], :name => "customers_devices_ibfk_2"

  add_foreign_key "data_services_devices", ["device_id"], "devices", ["id"], :name => "data_services_devices_ibfk_1"
  add_foreign_key "data_services_devices", ["data_service_id"], "data_services", ["id"], :name => "data_services_devices_ibfk_2"

  add_foreign_key "device_bugs_devices", ["device_id"], "devices", ["id"], :name => "device_bugs_devices_ibfk_1"
  add_foreign_key "device_bugs_devices", ["device_bug_id"], "device_bugs", ["id"], :name => "device_bugs_devices_ibfk_2"

  add_foreign_key "device_integrators", ["device_id"], "devices", ["id"], :name => "device_integrators_ibfk_1"
  add_foreign_key "device_integrators", ["integrator_id"], "integrators", ["id"], :name => "device_integrators_ibfk_2"

  add_foreign_key "devices", ["midp_version_id"], "midp_versions", ["id"], :name => "devices_ibfk_1"
  add_foreign_key "devices", ["cldc_version_id"], "cldc_versions", ["id"], :name => "devices_ibfk_2"
  add_foreign_key "devices", ["vendor_id"], "vendors", ["id"], :name => "devices_ibfk_3"
  add_foreign_key "devices", ["family_id"], "families", ["id"], :name => "devices_ibfk_4"

  add_foreign_key "devices_optional_apis", ["device_id"], "devices", ["id"], :name => "devices_optional_apis_ibfk_1"
  add_foreign_key "devices_optional_apis", ["optional_api_id"], "optional_apis", ["id"], :name => "devices_optional_apis_ibfk_2"

  add_foreign_key "devices_submissions", ["device_id"], "devices", ["id"], :name => "devices_submissions_ibfk_1"
  add_foreign_key "devices_submissions", ["submission_id"], "submissions", ["id"], :name => "devices_submissions_ibfk_2"

  add_foreign_key "download_codes", ["download_id"], "downloads", ["id"], :name => "download_codes_ibfk_1"

  add_foreign_key "downloads", ["access_id"], "accesses", ["id"], :name => "downloads_ibfk_1"
  add_foreign_key "downloads", ["app_version_id"], "app_versions", ["id"], :name => "downloads_ibfk_2"
  add_foreign_key "downloads", ["device_id"], "devices", ["id"], :name => "downloads_ibfk_3"

  add_foreign_key "facebook_datas", ["app_id"], "apps", ["id"], :name => "facebook_datas_ibfk_1"

  add_foreign_key "facebook_profiles", ["customer_id"], "customers", ["id"], :name => "facebook_profiles_ibfk_1"

  add_foreign_key "integrators_operators", ["integrator_id"], "integrators", ["id"], :name => "integrators_operators_ibfk_1"
  add_foreign_key "integrators_operators", ["operator_id"], "operators", ["id"], :name => "integrators_operators_ibfk_2"

  add_foreign_key "midlet_reports", ["access_id"], "accesses", ["id"], :name => "midlet_reports_ibfk_1"
  add_foreign_key "midlet_reports", ["download_id"], "downloads", ["id"], :name => "midlet_reports_ibfk_2"
  add_foreign_key "midlet_reports", ["app_id"], "apps", ["id"], :name => "midlet_reports_ibfk_3"
  add_foreign_key "midlet_reports", ["app_version_id"], "app_versions", ["id"], :name => "midlet_reports_ibfk_4"

  add_foreign_key "news_category_translations", ["news_feed_category_id"], "news_feed_categories", ["id"], :name => "news_category_translations_ibfk_1"
  add_foreign_key "news_category_translations", ["language_id"], "languages", ["id"], :name => "news_category_translations_ibfk_2"

  add_foreign_key "news_feed_translations", ["news_feed_id"], "news_feeds", ["id"], :name => "news_feed_translations_ibfk_1"
  add_foreign_key "news_feed_translations", ["language_id"], "languages", ["id"], :name => "news_feed_translations_ibfk_2"

  add_foreign_key "news_feeds", ["news_feed_category_id"], "news_feed_categories", ["id"], :name => "news_feeds_ibfk_1"

  add_foreign_key "ranking_entries", ["customer_id"], "customers", ["id"], :name => "ranking_entries_ibfk_1"
  add_foreign_key "ranking_entries", ["app_version_id"], "app_versions", ["id"], :name => "ranking_entries_ibfk_2"
  add_foreign_key "ranking_entries", ["device_id"], "devices", ["id"], :name => "ranking_entries_ibfk_3"

  add_foreign_key "resource_files", ["resource_id"], "resources", ["id"], :name => "resource_files_ibfk_1"
  add_foreign_key "resource_files", ["resource_file_type_id"], "resource_file_types", ["id"], :name => "resource_files_ibfk_2"

  add_foreign_key "resource_type_elements", ["resource_type_id"], "resource_types", ["id"], :name => "resource_type_elements_ibfk_1"
  add_foreign_key "resource_type_elements", ["resource_file_type_id"], "resource_file_types", ["id"], :name => "resource_type_elements_ibfk_2"

  add_foreign_key "resources", ["resource_type_id"], "resource_types", ["id"], :name => "resources_ibfk_1"

  add_foreign_key "submissions", ["integrator_id"], "integrators", ["id"], :name => "submissions_ibfk_1"

  add_foreign_key "users", ["partner_id"], "partners", ["id"], :name => "users_ibfk_1"
  add_foreign_key "users", ["user_role_id"], "user_roles", ["id"], :name => "users_ibfk_2"

end
