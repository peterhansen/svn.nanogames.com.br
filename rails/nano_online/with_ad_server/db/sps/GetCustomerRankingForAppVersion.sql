DELIMITER $$

# Retorna a colocação do usuário no ranking da aplicação ou -1 caso ele ainda não tenha submetido um recorde para a mesma
#	IN appVersionId INT,			# Id da aplicação cujo ranking queremos pesquisar
#	IN customerId INT 		# Id do usuário de quem queremos obter a classificação no ranking
	
CREATE PROCEDURE GetCustomerRankingForAppVersion( IN appVersionId INT, IN customerId INT )
LANGUAGE SQL
NOT DETERMINISTIC
SQL SECURITY INVOKER
BEGIN
	# No MySQL as declarações devem ficar no início do corpo BEGIN ... END
	DECLARE customerRankPos INT;

	# Não podemos retornar uma tabela de uma SP e armazenar seus resultados em outra tabela de uma SP mais externa,
	# com um INSERT, por exemplo. No mySQL também não é possível criar variáveis do tipo table para passarmos
	# como parâmetros de output. Logo, vamos acessar a tabela temporária criada por GetOnlineRankingForApp,
	# cujo nome é Ranking, e que será destruída apenas ao final da conexão da SP mais externa
	# Utilizamos a cláusula [IF NOT EXISTS] pois podemos estar sendo chamados por outra SP que também precisa desta tabela
	CREATE TEMPORARY TABLE IF NOT EXISTS ranking ( RankPos INT AUTO_INCREMENT PRIMARY KEY, ProfileId INT, Nickname TINYTEXT, Score BIGINT );
	
	# Garante que a tabela está vazia (pois pode ter sido criada por uma SP externa)
	TRUNCATE TABLE ranking;

	CALL GetOnlineRankingForAppVersion( appVersionId, 0, 0 );

	# Obtém a colocação do usuário	
	SELECT R.RankPos
	INTO customerRankPos
	FROM ranking R
	WHERE R.ProfileId = customerId;
	
	# Retorna a colocação do usuário no ranking da aplicação ou -1 caso ele ainda não tenha submetido um recorde para a mesma
	IF customerRankPos IS NULL THEN SET customerRankPos = -1;
	END IF;

	SELECT customerRankPos;
END $$

DELIMITER ;
