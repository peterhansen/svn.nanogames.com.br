class AppVersionUpdate < ActiveRecord::Base
  belongs_to :app_version
  belongs_to :updated_app_version, :class_name => 'AppVersion'
end