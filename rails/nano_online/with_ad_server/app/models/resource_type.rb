class ResourceType < ActiveRecord::Base
  # atributos: title(string), description(text)
  
  has_many  :resources,                           # tested 
            :dependent => :destroy
            
  has_many  :resource_type_elements,              # tested
            :dependent => :destroy
            
  has_many  :resource_file_types,                 
            :through => :resource_type_elements
  
  validates_presence_of :title, :description      # tested

  has_many :ad_channel_versions_compositions
	has_many :ad_channel_versions, :through => :ad_channel_versions_compositions

	
  def add_element(file_type_id, quantity)
    if ( type == ResourceFileType.find(file_type_id) )
      element = ResourceTypeElement.new()
      element.quantity = quantity
      element.resource_type = self
      element.resource_file_type = type
      
      if element.save
        self.resource_type_elements << element
      end
    end
  end


  def elements
    resource_type_elements
  end

	
  def elements=(params)
    unless params[:resource_file_type_id].blank? || params[:quantity].blank?
      self.resource_type_elements.build(params) 
    end
  end
	
  
  def file_types
    resource_file_types
  end
end
