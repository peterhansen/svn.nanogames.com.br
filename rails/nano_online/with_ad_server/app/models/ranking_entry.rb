# bla
class RankingEntry < ActiveRecord::Base
  
  # índice de parâmetro de submissão de ranking online: quantidade de entradas sendo enviadas. 
  # Tipo do dado: short (clientes 0.0.3 e superiores) ou byte (clientes < 0.0.3)
  PARAM_N_ENTRIES	= 0
  # índice de parâmetro de submissão de ranking online: subtipo do ranking. Tipo do dado: byte
  PARAM_SUB_TYPE    = 1
  
  # índice de parâmetro de submissão de ranking online: quantidade de perfis extras. Tipo do dado: short. Para cada entrada,
  # é lido um inteiro, indicando o id do perfil.
  PARAM_N_OTHER_PROFILES = 2
  # Índice de parâmetro de submissão de ranking: fim dos dados. Tipo do dado: nenhum.
  PARAM_END = 3
 
  # índice de parâmetro de submissão de ranking online: quantidade de recordes sendo requisitados. Tipo do dado: short
  PARAM_LIMIT = 4
  # índice de parâmetro de submissão de ranking online: offset dos recordes sendo requisitados. Tipo do dado: int 
  PARAM_OFFSET = 5

  # índice de parâmetro de submissão de ranking online: lista de entradas não gravadas. Tipo do dado: short indicando a
  # quantidade, depois um byte indicando o índice de cada entrada no array recebido como parâmetro.
  PARAM_UNSAVED_ENTRIES = 6


  # Código de retorno de inclusão de entrada de ranking: erro: versão do aplicativo não informada.
  RC_ERROR_APP_VERSION_BLANK  = 1
  # Código de retorno de inclusão de entrada de ranking: erro: id do cliente não informado.
  RC_ERROR_CUSTOMER_BLANK     = 2
  # Código de retorno de inclusão de entrada de ranking: erro: pontuação não informada.
  RC_ERROR_SCORE_BLANK        = 3
  # Código de retorno de inclusão de entrada de ranking: erro: tipo de ranking não informada.
  RC_ERROR_RANKING_TYPE_BLANK = 4
  
  # Limite padrão de entradas de ranking retornadas aos clientes.
  RANKING_ENTRY_DEFAULT_LIMIT   = 10
  # Posição inicial padrão das entradas de ranking retornadas aos clientes.
  RANKING_ENTRY_DEFAULT_OFFSET  = 0

  belongs_to :app
  belongs_to :app_version
  belongs_to :customer
  belongs_to :device
  belongs_to :ranking_type
  
  validates_presence_of :app_version, :message => "can't be blank (#{RC_ERROR_APP_VERSION_BLANK})"
  validates_presence_of :customer, :message => "can't be blank (#{RC_ERROR_CUSTOMER_BLANK})"
  validates_presence_of :score, :message => "can't be blank (#{RC_ERROR_SCORE_BLANK})"
  validates_presence_of :ranking_type, :message => "can't be blank (#{RC_ERROR_RANKING_TYPE_BLANK})"

  attr_accessor :sub_type 
  
  def app
    return app_version.app
  end

  def get_score
    return self.record
  end
  
  ##
  # Retorna uma lista de recordes. Por padrão, os 10 primeiros do ranking, mais os
  # que forem passados 'profile_indexes', sem duplicidade.
  ##
  def self.best_scores( params, limit = RANKING_ENTRY_DEFAULT_LIMIT, offset = RANKING_ENTRY_DEFAULT_OFFSET, profiles_indexes = nil )
    all_versions = AppVersion.find_all_by_app_id( params[ :app ] )
    all_versions_ids = []
    crescent = params[ :sub_type ] < 0
    ranking_type = RankingType.find_by_sub_type(params[:sub_type])
    
    all_versions.map {|version| all_versions_ids << version.id}
    
    entries = RankingEntry.all( :select =>      "customer_id, #{ crescent ? 'min( score )' : 'max( score )' } as score",
                                :conditions =>  { :ranking_type_id => ranking_type.id, :app_version_id => all_versions },
                                :order =>       crescent ? "score" : "score DESC",
                                :group =>       'customer_id',
                                :limit =>       limit,
                                :offset =>      offset )

    # array para teste
    # profiles_indexes = [ Customer.find_by_nickname("dummy3").id ]
    customer_entries = []

    unless ( profiles_indexes.blank? )
      begin
        # ActiveRecord::Base.connection.execute("DROP TABLE TEMPORARY Ranking")
        
        sql = "CREATE TEMPORARY TABLE Ranking (
                    ranking int AUTO_INCREMENT PRIMARY KEY,
                    id int,
                    nickname tinytext,
                    score bigint
                    )"
                    
                  
        ActiveRecord::Base.connection.execute(sql)
      
        sql = "INSERT INTO Ranking( id, nickname, score )
                    select id, Nick, score
                    from (
                           select C.id as id, C.nickname as Nick, max( R.score ) as score
                           from ranking_entries R
                           inner join customers C on C.id = R.customer_id
                           where R.app_version_id in (#{all_versions_ids.join(", ")})
                           group by C.id
                          ) as T
                    order by T.score desc"
               
        ActiveRecord::Base.connection.execute(sql)
      
        sql = "SELECT id, ranking, nickname, score from Ranking where id in (?)"
      
        customer_entries = RankingEntry.find_by_sql([sql, profiles_indexes])
      ensure
        ActiveRecord::Base.connection.execute("DROP TEMPORARY TABLE Ranking")
      end
    end
    sql = "select customer_id, 
                  #{ crescent ? 'min(score)' : 'max(score)' } as score, 
                  (select count(id) from ranking_entries e where e.score > r.score) + 1 as ranking
           from ranking_entries r 
           where ranking_type_id = ? and app_version_id in (?)
           group by customer_id, score
           order by #{crescent ? "score" : "score DESC"}
           limit 3"
             
     customer_entries.concat(RankingEntry.uniq_customer(RankingEntry.find_by_sql( [sql,
                                                                                  ranking_type.id,
                                                                                  all_versions])))
      
     # TODO péssimo desempenho!!
     customer_entries.delete_if {|customer_entry| entries.include? customer_entry}
     return entries, customer_entries
  end

  def self.uniq_customer(array)
    entries = []
    ids = []

    array.each do |entry|
      unless ids.include? entry.customer_id
        entries << entry
        ids << entry.customer_id
      end
    end
    return entries
  end
  
  def hash
    [self.id, self.customer_id, self.app_version_id, self.device_id, self.score, self.ranking_type_id].hash
  end
  
  def ==(other)
    other.class ==            self.class &&
    other.customer ==         self.customer &&
    other.app_version ==      self.app_version &&
    other.device ==           self.device &&
    other.score ==            self.score &&
    other.ranking_type_id ==  self.ranking_type_id
  end
  alias :eql? :==

  def formated_score  
    case self.ranking_type.unit_format.name
      # transforama "123456" em "1.234,56"
      when "decimal"
        if self.score.to_s.match(/(.*)(\d\d)/)
          unless $1.blank?
            init = last_three($1)
          else
            init = "0"
          end

          init + "," + $2
        else
          "0,0" + self.score.to_s
        end
      else
        logger.error("Unit Format not found.")
        self.score.to_s
    end
  end

  def last_three(string)
    string.match(/(.*)(\d\d\d)/)
      unless $1.blank?
        return last_three($1) + "." + $2
      end
    return string
  end

  ##############################################################################
  # Métodos protegidos                                                         #
  ##############################################################################
  protected
  
  ##
  # Cria novas entradas de ranking, fazendo o parser dos parâmetros binários. 
  # Retorna true ou false indicando se gravou todas as entradas, um array com os índices lidos dos jogadores e uma
  # referência para os ActiveRecords que tiveram problema na gravação (caso existam)
  ##
  def self.submit_records( params, nano_client_type = nil )
    total_entries = 0
    params[ :sub_type ] = 0

    profiles_indexes = []
    entries_with_errors = []
    
    limit = RANKING_ENTRY_DEFAULT_LIMIT
    offset = RANKING_ENTRY_DEFAULT_OFFSET 
    
    # Primeiro, é lida a quantidade total de entradas a serem submetidas e o subtipo
    # dessas entradas. Depois, para cada entrada, é lido o id do cliente e a pontuação
    # (sem identificadores antes dos valores).
    # Após ler a pontuação, é feita a inserção ou atualização no banco de dados.
    parse_data( params[ ID_SPECIFIC_DATA ], nano_client_type ) do |stream, id|
      case id
        when PARAM_N_ENTRIES
          if ( params[ ID_NANO_ONLINE_VERSION ] < '0.0.3' )
            total_entries = stream.read_byte()
          else
            total_entries = stream.read_short()
          end
        logger.info( "TOTAL ENTRIES: #{total_entries}" )
          
        when PARAM_SUB_TYPE
          params[ :sub_type ] = stream.read_byte()

          case RankingType.find_by_sub_type(params[:sub_type]).ranking_system.name
            when "BestScore"
              # depois de ler o sub-tipo, começa a varredura das entradas submetidas
              1.upto( total_entries ) do |i|
                entry = RankingEntry.new()

                entry.customer_id = stream.read_int()
                profiles_indexes << entry.customer_id

                entry.score = stream.read_long()

                # grava o instante em que o recorde foi efetivamente realizado.
                # TODO adicionar sincronização de relógios do cliente/servidor: cliente envia um datetime
                # indicando sua hora atual nos headers principais. Isso evita cheats de horas, pois
                # alterar a hora local do aparelho passa a não mais interferir na hora de realização do
                # recorde
                if( params[ ID_NANO_ONLINE_VERSION ] >= '0.0.3' )
                  entry.record_time = stream.read_datetime()
                end

                entry.app_version_id = params[ :app_version ].id
                entry.ranking_type_id = RankingType.find_by_sub_type(params[:sub_type]).id
                if ( params[ :device ] )
                  entry.device_id = params[ :device ].id
                end

                # if ( entry.customer.is_blocked? || !entry.save ) TODO
                unless entry.save
                  logger.info( "erro ao salvar entrada: #{entry.errors.full_messages}" )
                  entries_with_errors << entry
                end
              end
          end

        when PARAM_N_OTHER_PROFILES
          # lê os perfis dos outros usuários cujas posições serão pesquisadas
          n_other_profiles = stream.read_short()
          1.upto( n_other_profiles ) do |i|
            profiles_indexes << stream.read_int()
          end
          
        when PARAM_LIMIT
          limit = stream.read_short()
          params[ PARAM_LIMIT ] = limit;
          
        when PARAM_OFFSET
          offset = stream.read_int()
          params[ PARAM_OFFSET ] = offset;
      end
    end
    
    return entries_with_errors.length <= 0, profiles_indexes, entries_with_errors
  end
  
  
  ##############################################################################
  # MétodOS PRIVADOS                                                           #
  ##############################################################################
  def self.contains?( ret, entry )
    if ( entry )
      ret.each { |r|
        if ( r.nil? )
          return false
        end

        if ( r.customer == entry.customer )
          return true
        end
      }
    end
    
    return false
  end
  
end
