class AppleProductCategory < ActiveRecord::Base
  has_many :apple_downloads
  
  validates_presence_of :category
  
end