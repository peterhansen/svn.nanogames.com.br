require 'digest/sha1'

class User < ActiveRecord::Base
  belongs_to :user_role
  belongs_to :partner

  validates_presence_of    :nickname
  validates_uniqueness_of  :nickname, :case_sensitive => false
  validates_presence_of    :user_role

  attr_accessor :password_confirmation 
  validates_confirmation_of :password

  validates_presence_of    :email
  validates_format_of :email, :allow_nil => true, :allow_blank => true, :with => /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i
  validates_length_of :email, :in => 6..40, :allow_nil => true, :allow_blank => true

  validates_length_of :first_name, :maximum => 30, :allow_nil => true, :allow_blank => true
  validates_length_of :last_name, :maximum => 30, :allow_nil => true, :allow_blank => true


  def name()
    return @nickname
  end


  def name=( n )
    return @nickname = n
  end

  def is_admin?()
    return user_role == UserRole.first( :conditions => { :name => 'Administrator' } )
  end


  def validate
    errors.add_to_base( 'Missing password' ) if hashed_password.blank?
  end


  def password
    @password
  end

  
  def password=(pwd)
    @password = pwd
    create_new_salt()
    self.hashed_password = User.encrypted_password( self.password, self.salt )
  end


  def self.authenticate( name, password, key = nil )
    user = self.find_by_nickname( name )
    
    if ( user )
      expected_password = encrypted_password( password, user.salt )

      logger.info( "expected_password: #{expected_password}\nuser.salt: #{user.salt}" )
      logger.info( "user.hashed_password: #{user.hashed_password}" )

      if ( user.hashed_password != expected_password || !validate_key( key ) )
        user = nil
      end
    end
    
    return user
  end
  
  
  def after_destroy
    if ( User.count.zero? )
      raise "Can't delete: last user."
    end
  end


  ##
  # obtém o layout do usuário no sistema.
  ##
  def layout
    return user_role.layout
  end
  
  def resetar_senha
    charset = %w{2 3 4 6 7 8 9 A C D E F G H J K L M N P Q R T V W X Y Z}
    new_password = ""
    
    while !(new_password =~ /\d/)
      new_password = (0...7).map{charset.to_a[rand(charset.size)]}.join
    end
    
    self.password = new_password
    if self.save
      reenviar_senha
    end
  end
  
  def reenviar_senha
    dados = Hash.new
    dados["subject"] = "Nova senha Nano Games."
    dados["message"] = "Ola #{self.nickname}!\nSua nova senha é: #{self.password}\nNão esqueça de alterá-la em seu próximo login."
    dados["recipients"] = self.email
    dados["sender"] = "no-reply@nanogames.com.br"

    Emailer.deliver_send(dados)
  end
  
  private

  def validate_password()
   unless ( password && password.match( /^(?=[\.\,\_\?!\-+@:a-zA-Z0-9]*?[A-Z])(?=[\.\,\_\?!\-+@:a-zA-Z0-9]*?[a-z])(?=[\.\,\_\?!\-+@:a-zA-Z0-9]*?[\.\,\_\?!\-+@:0-9])[\.\,\_\?!\-+@:a-zA-Z0-9]{6,}$/ ) )
      errors.add( :password, "must contain at least 6 characters, including upper-case and lower-case letters, and numbers or special characters (\.\,\_\?!\-+@:) (#{RC_ERROR_PASSWORD_WEAK})" )
    end
  end

  
  def self.encrypted_password( password, salt )
    # os caracteres literais são necessários devido à mudança para UTF8
    string_to_hash = password + "besteira aleat\363ria" + salt #dificulta a adivinhação da senha
    Digest::SHA1.hexdigest( string_to_hash )
  end


  def self.validate_key( key )
    return true
    # TODO validação de hash fora temporariamente para testes!
#    if ( !key.nil? )
#      t = Time.now().utc()
#      return Digest::SHA1.hexdigest( "P#{t.hour.to_s}E*'#{t.day.to_s}T09id_\&saj#{t.year.to_s}E$V#{t.month.to_s}R__1\@V#{t.min.to_s}H=\##{t.yday.to_s}" ) == key
#    end

    return false
  end

  
  def create_new_salt
    self.salt = self.object_id.to_s + rand.to_s
  end

end
