class AdChannelStatus < ActiveRecord::Base
  has_many :ad_channel_versions, :dependent => :destroy       # tested
  has_many :ad_channels, :dependent => :destroy               # tested
  
  validates_presence_of :status                               # tested
end
