##
# Essa classe armazena informações de uma sessão de usuário, ou seja, de um acesso único.
##
class Access < ActiveRecord::Base
  belongs_to :customer
  belongs_to :device
  has_many :downloads

  validates_presence_of :user_agent


  def device_model_full()
    return device.nil? ? '' : device.model_full
  end

end
