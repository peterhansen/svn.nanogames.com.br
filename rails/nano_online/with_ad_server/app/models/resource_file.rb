class ResourceFile < ActiveRecord::Base
  IMAGE = ["image/jpeg", "image/png"]
  AUDIO = ['mp3']

  belongs_to :resource                              # tested
  belongs_to :resource_file_type                    # tested

  has_attached_file :file,
                    # :styles => { :small => "200x200>" },
                    :url => "/:attachment/:id/:style/:basename.:extension",
                    :path => ":rails_root/files/:id/:style/:basename.:extension"

  validates_attachment_presence :file                                                           # tested
#  validates_attachment_size :file, :less_then => 10.megabytes                                   # todo n�o est� validando
#  validates_attachment_content_type :file, :content_type => ["image/jpeg", "image/png", "audio/mpeg"]     # todo n�o est� validando
  
  validates_presence_of :resource                                                               # tested
  validates_presence_of :resource_file_type

  before_validation :validate_type


  def validate_type()
    ResourceFileType.all.each do |type|
      if type.content.match(self.file_content_type)
        self.resource_file_type = type
      else
        errors.add(:resource_file_type, "(#{type}) is invalid.")
      end
	    errors.add(:resource_file_type, "is invalid.") if self.resource_file_type.blank?
    end
  end


  def file_type()
		return ResourceFileType.find_by_content( self.file_content_type )
  end

  def self.write_data ( out, params )
    out.write_byte(PARAM_RESOURCE_DATA_SIZE)

    f = File.open(file.path, "rb")
    content = f.read

    out.write_binary_data(content)
  end

end
