class NewsFeed < ActiveRecord::Base
  # Quantidade máxima de entradas retornadas pelo servidor ao usuário
  MAX_NEWS_ENTRIES = 10

  # Índice de parâmetro de atualização de news feeder: instante da última atualização. Tipo do dado: long
  PARAM_LAST_UPDATE_TIME		= 0
  # Índice de parâmetro de atualização de news feeder: última atualização de versão armazenada (número da versão). Tipo do dado: string
  PARAM_LAST_APP_VERSION_UPDATE	= 1
  # Índice de parâmetro de atualização de news feeder: versão do news feeder. Tipo do dado: string
  PARAM_NEWS_FEEDER_VERSION		= 2
  # Índice de parâmetro de atualização de news feeder: data da entrada. Tipo do dado: long
  PARAM_ENTRY_TIME				= 3
  # Índice de parâmetro de atualização de news feeder: categoria da entrada. Tipo do dado: string
  PARAM_ENTRY_CATEGORY_TITLE			= 4
  # Índice de parâmetro de atualização de news feeder: título da entrada. Tipo do dado: string
  PARAM_ENTRY_TITLE				= 5
  # Índice de parâmetro de atualização de news feeder: conteúdo da entrada. Tipo do dado: string
  PARAM_ENTRY_CONTENT			= 6
  # Índice de parâmetro de atualização de news feeder: URL para acesso completo e/ou download de conteúdo relacionado à
  # entrada. Tipo do dado: string
  PARAM_ENTRY_URL				= 7
  # Índice de parâmetro de atualização de news feeder: quantidade total de entradas a serem lidas. Tipo do dado: byte
  PARAM_TOTAL_ENTRIES			= 8
  # Índice de parâmetro de atualização de news feeder: quantidade total de recursos especiais usados no conteúdo,
  #  como imagens, sons, animações, etc. Caso esse parâmetro não seja encontrado, é considerado que não há recursos
  #  especiais em uso.
  #  Tipo dos dados:
  # 	byte (quantidade de recursos especiais)
  # 	para cada recurso:
  # 		byte (tipo do recurso)
  # 		int (tamanho do recurso em bytes)
  # 		byte[] (do tamanho informado anteriormente)
  PARAM_TOTAL_RESOURCES			= 9
  # Índice de parâmetro de atualização de news feeder: indicador de fim de uma entrada (a quantidade de parâmetros é variável)
  PARAM_ENTRY_END				= 10
  # Índice de parâmetro de atualização de news feeder: indicador de fim de uma entrada (a quantidade de parâmetros é variável)
  PARAM_NEWS_FEEDER_END         = 11
  # Índice de parâmetro de atualização de news feeder: início de leitura de entrada de atualização de aplicativo. 
  # Tipos dos dados: boolean (atualização obrigatória) e string (versão da atualização)
  PARAM_ENTRY_APP_UPDATE        = 12

	# Índice de parâmetro de atualização de news feeder: id da categoria da entrada. Tipo do dado: int
	# @since 0.0.2
	PARAM_ENTRY_CATEGORY_ID		= 13

	# Índice de parâmetro de atualização de news feeder: visibilidade da categoria da entrada. Tipo do dado: boolean
	# @since 0.0.2
	PARAM_ENTRY_CATEGORY_VISIBILITY	= 14

	## Índice de parâmetro de atualização de news feeder: quantidade de categorias. Tipo do dado: short
	# @since 0.0.3
	##
	PARAM_TOTAL_CATEGORIES			= 15

	## Índice de parâmetro de atualização de news feeder: fim dos dados de uma categoria. Tipo do dado: nenhum
	# @since 0.0.3
	##
	PARAM_ENTRY_CATEGORY_END		= 16

	## Índice de parâmetro de atualização de news feeder: descrição de uma categoria de notícias. Tipo do dado: string
	# @since 0.0.3
	##
	PARAM_ENTRY_CATEGORY_DESCRIPTION		= 17
	

  belongs_to              :news_feed_category  
  has_many                :news_feed_translation, :dependent => :destroy
  
  validates_presence_of   :news_feed_category

  ##
  # 
  ##
  def self.parse_data( stream )
    ret = {}

    while ( stream.available() > 0 )
      id = stream.read_byte()
      case id
        when PARAM_LAST_UPDATE_TIME
          ret[ :last_update_time ] = stream.read_datetime()

        when PARAM_LAST_APP_VERSION_UPDATE
          ret[ :last_app_version_update_number ] = stream.read_string()
          
        when PARAM_NEWS_FEEDER_VERSION
          ret[ :news_feeder_version ] = stream.read_string()

        when PARAM_NEWS_FEEDER_END
          break
      end
    end

    return ret
  end


  ##
  #
  ##
  def self.send_data( out, params )
    data = params[ ID_NEWS_FEEDER_DATA ]

    unless ( data.blank? )
      # TODO usar Time.zone.at ou Time.zone ????? Time.zone.at está lançando exceção com valores baixos...
      news_feed_entries = NewsFeed.all( :conditions => [ "created_at > ?", Time.at( data[ :last_update_time ] ) ],
                                        :order => 'created_at DESC',
                                        :limit => NewsFeed::MAX_NEWS_ENTRIES )

      out.write_byte( ID_NEWS_FEEDER_DATA )

      unless ( params[ :app_version_update ].blank? )
        # encontrou atualização - cria uma entrada dinamicamente, contendo as informações da atualização
        translation = NewsFeedTranslation.new()
        entry_app_update = NewsFeed.new()
        
        update = params[ :app_version_update ]

        entry_app_update.created_at = Time.now()
        entry_app_update.news_feed_category_id = NewsFeedCategory.find( 1 )

        translation.news_feed_id = entry_app_update
        translation.content = update.updated_app_version.release_notes
        translation.title = "#{update.updated_app_version.app.name} versão #{update.updated_app_version.number} disponível."
        translation.url = "http://wap.nanogames.com.br"
        translation.created_at = entry_app_update.created_at
        translation.language_id = Language.first # TODO teste
        
        # informação de atualização é uma entrada especial, indicada por esse id
        out.write_byte( PARAM_ENTRY_APP_UPDATE )
        out.write_boolean( update.required )
        out.write_string( update.updated_app_version.number )
        translation.write_data( out )
      end

			# envia informações das categorias de notícias, antes das notícias em si
			if ( data[ :news_feeder_version ] >= '0.0.3' )
				total_categories = []
				news_feed_entries.each { |n|
					total_categories << n.news_feed_category_id
				}
				total_categories.uniq!
				out.write_byte( PARAM_TOTAL_CATEGORIES )
				out.write_short( total_categories.length )
				total_categories.each { |c|
					NewsCategoryTranslation.find( c ).write_data( out )
				}
			end


      # envia o restante das notícias (caso haja)
      out.write_byte( PARAM_TOTAL_ENTRIES )
			logger.info( "-----> ESCREVENDO #{news_feed_entries.length} NOTÍCIAS!!!!" )
      out.write_byte( news_feed_entries.length )

      news_feed_entries.each { |n|
        n.write_data( out )
      }

      # tag de fim das informações do news feeder
      out.write_byte( PARAM_NEWS_FEEDER_END )
    end
  end
  
  
  ##
  #
  ##
  def write_data( out )
		# TODO enviar resposta de acordo com o idioma do cliente
    translations.first.write_data( out )
  end
  
  def translations
    return self.news_feed_translation
  end
  
  def category
    return self.translations.first.category_title # TODO obter idioma correto
    rescue
      return nil
  end
  
  def content
    return self.translations.first.content # TODO obter idioma correto
    rescue
      return nil
  end
  
  def title
    return self.translations.first.title # TODO obter idioma correto
    rescue
      return nil
  end

  def url
    return self.translations.first.url # TODO obter idioma correto
    rescue
      return nil
  end
  
  def language
    return self.translations.first.language.name # TODO obter idioma correto
    rescue
      return nil
  end
  
  def translation_number
    return self.news_feed_translation.size
  end

end
