class ResourceFileType < ActiveRecord::Base
  has_many  :resource_type_elements, 
            :dependent => :destroy
            
  has_many  :resource_types, 
            :through => :resource_type_elements
            
  has_many  :resource_files
  
  validates_presence_of :name
  validates_uniqueness_of :name
end
