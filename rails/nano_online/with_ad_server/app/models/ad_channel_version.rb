class AdChannelVersion < ActiveRecord::Base
  belongs_to :ad_channel                                  # tested
  belongs_to :ad_channel_status                           # tested
  has_many :ad_piece_versions, :dependent => :destroy     # tested
  
  has_and_belongs_to_many :app_versions
  
  has_many :ad_channel_version_compositions
	has_many :resource_types, :through => :ad_channel_version_compositions
  
  validates_presence_of :ad_channel,                      # tested
                        :ad_channel_status                # tested

	def title_full()
		return "#{ad_channel.title} - #{title}"
	end
end
