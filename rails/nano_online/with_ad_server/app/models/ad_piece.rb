class AdPiece < ActiveRecord::Base
  has_and_belongs_to_many :ad_campaigns                       # tested
  has_many :ad_piece_versions, :dependent => :destroy         # tested
  belongs_to :ad_channel                                      # tested
  
  validates_presence_of :ad_channel                           # tested
end
