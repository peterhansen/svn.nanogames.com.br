class Partner < ActiveRecord::Base
  has_and_belongs_to_many :apps
  has_many :users
  has_many :advertisers

  validates_presence_of    :name
  validates_uniqueness_of  :name, :case_sensitive => false
end
