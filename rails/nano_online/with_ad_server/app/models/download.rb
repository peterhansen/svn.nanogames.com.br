##
# Essa classe descreve um pedido de download, a partir do qual são gerados N códigos de download de arquivo (1 por aquivo:
# jad, jar, cod, etc).
##
class Download < ActiveRecord::Base
  belongs_to :app_version
  belongs_to :app
  belongs_to :access
  belongs_to :device

  has_many :download_codes

  validates_presence_of :access_id
  validates_presence_of :app_version_id

end
