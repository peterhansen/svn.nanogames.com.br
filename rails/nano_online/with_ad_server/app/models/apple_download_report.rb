class AppleDownloadReport < ActiveRecord::Base
  has_many :apple_downloads
  
  validates_presence_of :name
  validates_uniqueness_of :name 
  validates_length_of :name, :maximum => 60
end