class Emailer < ActionMailer::Base
  def send(dados)
      @subject = dados["subject"]
      @recipients = dados["recipients"]
      @from = dados["sender"]
      @reply_to = dados["reply_to"]
      @sent_on = Time.now
      
   	  @body = dados
     end
end
