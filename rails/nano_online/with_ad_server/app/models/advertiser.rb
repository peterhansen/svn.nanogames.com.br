class Advertiser < ActiveRecord::Base
  has_many :ad_campaigns, :dependent => :destroy
  belongs_to :partner                                   # tested
  
  validates_presence_of :partner                        # tested
  
  def partner_name
    self.partner.name
  end
end
