class MailSender < ActionMailer::Base

   def send( recipients, subject, title, message, sent_at = Time.now )
      @subject = subject
      @recipients = recipients
      @from = 'noreply@nanogames.com.br'
      @sent_on = sent_at
	  @body[ 'title' ] = title
  	  @body[ 'email' ] = 'sender@yourdomain.com'
   	  @body[ 'message' ] = message
      @headers = {}
   end

end
