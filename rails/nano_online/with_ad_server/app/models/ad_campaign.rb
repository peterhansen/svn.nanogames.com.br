class AdCampaign < ActiveRecord::Base
	# Id de parâmetro do gerenciador de anúncios: versão do gerenciador. Formato dos dados: string
	PARAM_AD_MANAGER_VERSION		= 0;

	# Id de parâmetro do gerenciador de anúncios: quantidade de anúncios. Formato dos dados: short
	PARAM_N_ADS						= 1;

	# Id de parâmetro do gerenciador de anúncios: id da versão do anúncio. Formato dos dados: int
	PARAM_AD_VERSION_ID						= 2;

	# Id de parâmetro do gerenciador de anúncios: id da versão do canal do anúncio. Formato dos dados: int
	PARAM_AD_CHANNEL_VERSION_ID				= 3;

	# Id de parâmetro do gerenciador de anúncios: quantidade de informações de visualização. Formato dos dados:
	# int indicando o id do anúncio
	# int indicando o id do usuário
	# short indicando o número de entradas, e para cada entrada:
	#	  long indicando a hora de início da visualização
	#	  int indicando a duração da visualização
	PARAM_AD_VIEWS				= 4;

	# Id de parâmetro do gerenciador de anúncios: quantidade de anúncios inativos (que devem ser apagados do aparelho).
	# Formato dos dados: byte indicando a quantidade, e então N x int ids de anúncios.
	PARAM_N_INACTIVE_ADS			= 5;

	# Id de parâmetro do gerenciador de anúncios: data/hora de expiração do anúncio. Formato dos dados: long
	PARAM_AD_EXPIRATION_TIME		= 6;

	# Id de parâmetro do gerenciador de anúncios: quantidade de recursos de um anúncio.
	# Formato dos dados: short indicando a quantidade, e então N x int ids de recursos (baixados separadamente)
	PARAM_AD_N_RESOURCES			= 7;

	# Id de parâmetro do gerenciador de anúncios: fim da leitura/gravação de dados do gerenciador de anúncios.
	# Formato dos dados: nenhum
	PARAM_AD_MANAGER_END			= 8;

	# Id de parâmetro do gerenciador de anúncios: fim da leitura/gravação de um anúncio.
	# Formato dos dados: nenhum
	PARAM_AD_END					= 9;
  
  has_and_belongs_to_many :apps               # tested
  has_and_belongs_to_many :ad_pieces          # tested
  belongs_to :advertiser                      # tested
  
  validates_presence_of :advertiser           # tested
  validates_presence_of :title           # tested
  # validates_presence_of :apps                 # tested

	
	##
	#
	##
  def self.parse_data( stream, params = nil )
    ret = {}

    while ( stream.available() > 0 )
      id = stream.read_byte()
			logger.info( "ID: #{id}" ) if ( RAILS_ENV != 'production' )
      case id
        when PARAM_AD_MANAGER_VERSION
          ret[ :ad_manager_version ] = stream.read_string()
					logger.info( "CLIENT MANAGER VERSION: #{ret[ :ad_manager_version ]}" ) if ( RAILS_ENV != 'production' )

				when PARAM_N_ADS
					ret[ :customer_current_ad_piece_versions ] = []
					customer_n_ads = stream.read_short()
					for i in ( 1..customer_n_ads )
						ret[ :customer_current_ad_piece_versions ] << stream.read_int()
					end

					logger.info( "CLIENT CURRENT AD PIECE VERSIONS: #{ret[ :customer_current_ad_piece_versions ].sort.join( ', ' ) }" ) if ( RAILS_ENV != 'production' )

				when PARAM_AD_VIEWS
					ad_piece_version_id = stream.read_int()
					customer_id = stream.read_int()
					total_entries = stream.read_short()

					logger.info( "AD PIECE VERSION ID: #{ad_piece_version_id}" ) if ( RAILS_ENV != 'production' )
					logger.info( "CUSTOMER ID: #{customer_id}" ) if ( RAILS_ENV != 'production' )
					logger.info( "TOTAL ENTRIES: #{total_entries}" ) if ( RAILS_ENV != 'production' )
					
					for i in ( 1..total_entries )
						ad_view = AdPieceVersionVisualization.new()

						logger.info( "ad view \##{i}/#{total_entries}:" ) if ( RAILS_ENV != 'production' )

						# lê a hora de início da visualização
						ad_view.time_start = stream.read_datetime()
						logger.info( "start: #{ad_view.time_start}" ) if ( RAILS_ENV != 'production' )
						# lê a duração da visualização, a converte para segundos e soma ao tempo inicial para obtermos a hora final
						ad_view.time_end = ad_view.time_start + ( stream.read_int() / 1000.0 ).seconds
						logger.info( "end: #{ad_view.time_end}" ) if ( RAILS_ENV != 'production' )
						ad_view.customer_id = customer_id if ( customer_id > 0 )
						ad_view.device = params[ :device ]
						logger.info( "device: #{ad_view.device}" ) if ( RAILS_ENV != 'production' )
						ad_view.ad_piece_version_id = ad_piece_version_id

						ad_view.save
					end

        when PARAM_AD_MANAGER_END
          break
      end
    end

		return ret
  end


	##
	# Envia dados do ad server para um cliente Nano.
	##
  def self.send_data( out, params )
    data = params[ ID_AD_SERVER_DATA ]

    unless ( data.blank? )
      out.write_byte( ID_AD_SERVER_DATA )

			# primeiro obtém todas as campanhas ativas para este cliente
			active_campaigns = AdCampaign.all( :conditions => [ "expires_at > ?", DateTime.now ] )

			logger.info( "ACTIVE CAMPAIGNS: #{active_campaigns.blank? ? 0 : active_campaigns.length}" ) if RAILS_ENV != "production"

			# adiciona os índices dos recursos a serem enviados, para enviá-los posteriormente e evitar duplicatas
			all_resources = []

			active_campaigns.each { |c|
				# obtém todos os ad_pieces ativos suportados pelo ad_channel_version do cliente
				ad_piece_versions = AdPieceVersion.all( :include => [ { :ad_piece => :ad_campaigns }, { :ad_channel_version => :app_versions } ],
																								:conditions => [ "app_versions.id = ? and ad_campaigns.expires_at > ?", params[ 'app_version' ], DateTime.now ] )

				unless ( data[ :customer_current_ad_piece_versions ].blank? )
					new_ad_piece_versions = ad_piece_versions - data[ :customer_current_ad_piece_versions ]
					# exclui as versões de anúncios já existentes no cliente
					ad_piece_versions = new_ad_piece_versions

					temp = data[ :customer_current_ad_piece_versions ] - new_ad_piece_versions
					temp.each { |d|
						# TODO verificar os ids de ads informados pelo cliente, excluí-los dos ads retornados e informar quais devem ser apagados
					}
					# indica ao usuário as versões de anúncios que devem ser excluídas
#					unless ( ad_piece_versions_to_delete.blank? )
#						out.write_byte( PARAM_N_INACTIVE_ADS )
#						out.write_byte( ad_piece_versions_to_delete.length )
#						ad_piece_versions_to_delete.each{ |d|
#							out.write_int( d.id )
#						}
#					end
				end

				# quantidade total de anúncios
				out.write_byte( PARAM_N_ADS )
				out.write_short( ad_piece_versions.length )

				logger.info( "AD PIECES: #{ad_piece_versions.length}" ) if RAILS_ENV != "production"

				ad_piece_versions.each { |apv|
					# TODO fazer um ad_piece_version escrever no stream diretamente para melhorar o encapsulamento
					# tipo do recurso
					out.write_byte( PARAM_AD_CHANNEL_VERSION_ID )
					out.write_int( apv.ad_channel_version_id )

					# id da versão do anúncio
					out.write_byte( PARAM_AD_VERSION_ID )
					out.write_int( apv.id )

					logger.info( "AD VERSION ID: #{apv.id}" ) if RAILS_ENV != "production"

					# data de expiração do anúncio
					out.write_byte( PARAM_AD_EXPIRATION_TIME )
					out.write_datetime( c.expires_at )

					# escreve os ids dos recursos necessários para os anúncios
					resources = apv.resources
					all_resources << resources
					
					out.write_byte( PARAM_AD_N_RESOURCES )
					out.write_short( resources.length )

					logger.info( "RECURSOS: #{resources.length}" ) if RAILS_ENV != "production"

					resources.each { |r|
						out.write_int( r.id )
					}

					out.write_byte( PARAM_AD_END )
				}

				# adiciona informação dos recursos necessários para os ads aos parâmetros globais
				Resource.add_resources_to_send( all_resources, params )
			}

      # tag de fim das informações do gerenciador de recursos
      out.write_byte( PARAM_AD_MANAGER_END )
    end
  end
	
end
