class AdChannel < ActiveRecord::Base
  has_many    :ad_pieces,            :dependent => :destroy     # tested
  has_many    :ad_channel_versions,  :dependent => :destroy     # tested
  belongs_to  :ad_channel_status                                # tested
  
  validates_presence_of :title,                                 # tested
                        :description,                           # tested
                        :ad_channel_status                      # tested
end
