class Language < ActiveRecord::Base
  has_many :news_feed_translation
  has_many :news_category_translation
end
