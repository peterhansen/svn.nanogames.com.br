class UndetectedUserAgent < ActiveRecord::Base
  validates_uniqueness_of :user_agent
  validates_presence_of :user_agent
  
  # TODO armazenar página que cliente tentou acessar?
end
