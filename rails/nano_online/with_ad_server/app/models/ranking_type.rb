class RankingType < ActiveRecord::Base
  has_and_belongs_to_many :app_versions
  has_many :ranking_entries

  belongs_to :unit_format
  belongs_to :ranking_system

  validates_presence_of :name, :description, :unit_format, :sub_type

  validates_uniqueness_of :name, :sub_type

  validates_length_of :name, :maximum => 20
  validates_length_of :description,:maximum => 255

  validates_numericality_of :sub_type, :less_than => 256
end

