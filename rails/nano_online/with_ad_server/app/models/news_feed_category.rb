class NewsFeedCategory < ActiveRecord::Base
  has_many :news_feed, :dependent => :destroy
  has_many :news_category_translation, :dependent => :destroy
  
  def title
    t = self.news_category_translation
    unless t.blank?
      return t.first.title
    else
      return ''
    end
  end

  def description
    d = self.news_category_translation

    unless d.blank?
      return d.first.description
    else
      return ""
    end
  end

  def translations
    return self.news_category_translation
  end

end
