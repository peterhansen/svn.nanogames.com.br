class ResourceTypeElement < ActiveRecord::Base
  belongs_to :resource_type
  belongs_to :resource_file_type
  
  validates_presence_of :resource_type
  validates_presence_of :resource_file_type
  validates_presence_of :quantity
end
