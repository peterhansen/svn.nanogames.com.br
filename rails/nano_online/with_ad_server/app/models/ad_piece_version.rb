class AdPieceVersion < ActiveRecord::Base
  belongs_to :ad_piece                            # tested
  belongs_to :ad_channel_version                  # tested
  has_and_belongs_to_many :resources              # tested

	has_many :ad_piece_version_visualizations
  
  validates_presence_of :ad_piece                 # tested
  validates_presence_of :ad_channel_version       # tested
end
