class Operator < ActiveRecord::Base
  has_and_belongs_to_many :integrators
  has_and_belongs_to_many :bands
  
  validates_presence_of :name
end
