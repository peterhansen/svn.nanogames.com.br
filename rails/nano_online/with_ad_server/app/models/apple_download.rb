class AppleDownload < ActiveRecord::Base
  belongs_to :app
  belongs_to :country
  belongs_to :apple_product_category
  belongs_to :apple_download_report
  belongs_to :cus_currency, :class_name => "Currency", :foreign_key => "customer_currency"
  belongs_to :roy_currency, :class_name => "Currency", :foreign_key => "royalty_currency"
  
  validates_presence_of       :units, 
                              :royalty_price, 
                              :customer_price, 
                              :customer_currency, 
                              :royalty_currency, 
                              :begin_date
  
  validates_numericality_of   :units, 
                              :royalty_price, 
                              :customer_price
  
  validates_length_of :service_provider_code,         :maximum => 50,     :allow_nil => true
  validates_length_of :service_provider_country_code, :maximum => 50,     :allow_nil => true
  validates_length_of :upc,                           :maximum => 20,     :allow_nil => true
  validates_length_of :isrc,                          :maximum => 20,     :allow_nil => true
  validates_length_of :artist_show,                   :maximum => 1024,   :allow_nil => true
  validates_length_of :label_studio_network,          :maximum => 1024,   :allow_nil => true
  validates_length_of :season_pass,                   :maximum => 20,     :allow_nil => true
  validates_length_of :isan,                          :maximum => 40,     :allow_nil => true
  validates_length_of :cma,                           :maximum => 10,     :allow_nil => true
  validates_length_of :asset_content_flavor,          :maximum => 20,     :allow_nil => true
  validates_length_of :vendor_offer_code,             :maximum => 256,    :allow_nil => true
  validates_length_of :grid,                          :maximum => 18,     :allow_nil => true
  validates_length_of :promo_code,                    :maximum => 10,     :allow_nil => true
  validates_length_of :parent_identifier,             :maximum => 100,    :allow_nil => true
  
  @@f = {
    0 => "service_provider_code", 1 => "service_provider_country_code", 3 => "upc", 4 => "isrc", 5 => "artist_show", 7 => "label_studio_network",
    9 => "units", 10 => "royalty_price", 11 => "begin_date", 12 => "end_date", 13 => "customer_currency", 15 => "royalty_currency", 16 => "preorder",
    17 => "season_pass", 18 => "isan", 20 => "customer_price", 21 => "cma", 22 => "asset_content_flavor", 23 => "vendor_offer_code", 24 => "grid",
    25 => "promo_code", 26 => "parent_identifier"
  }
  
  def self.new_from_array(array)
    a = AppleDownload.new
    
    #TODO validar a presença do currency e do country
    array.each_index do |index|
      if @@f[index] == "customer_currency" then array[index] = Currency.find_by_code(array[index]).id end
      if @@f[index] == "royalty_currency" then array[index] = Currency.find_by_code(array[index]).id end

      if array[index].class == String
        eval "a.#{@@f[index]} = '#{array[index]}'" if @@f[index]
      else
        eval "a.#{@@f[index]} = #{array[index]}" if @@f[index]
      end
    end
    a.app_id = App.find_by_apple_title(array[2]).id
    a.apple_product_category = AppleProductCategory.find_by_category(array[8])
    a.country = Country.find_by_iso_name_2(array[14])
    
    return a
  end
  
  def country_name
    self.country.name
  end
  
  def app_name
    self.app.name
  end
  
end
