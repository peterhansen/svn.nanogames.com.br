class ApplicationSql < ApplicationController
  def initialize
    
  end
  
  def get_download_application_devices(initial_date, end_date, partner_id )
    downloads = Download.all( :include => [ :app_version, :access, :device ] )
    downloads = downloads.select {|obj| obj[:created_at] >=  initial_date && obj[:created_at] <= end_date }
    puts( "PARTNER: #{partner_id}" )
    return downloads
  end  

  def get_access_application_devices(initial_date, end_date, partner_id )
    accesses = Access.all( :include => [ :device ] )
    accesses = accesses.select {|obj| obj[:created_at] >=  initial_date && obj[:created_at] <= end_date }
    puts( "PARTNER: #{partner_id}" )
    return accesses
  end

  def get_nodownload_application_devices(initial_date, end_date, partner_id )
    nodownload = Access.all( :include => [ :device ] )
    nodownload = nodownload.select {|obj| obj[:created_at] >=  initial_date && obj[:created_at] <= end_date }
    puts( "PARTNER: #{partner_id}" )
    return nodownload
  end
end
