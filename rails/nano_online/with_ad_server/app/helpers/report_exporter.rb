class ReportExporter
  def initialize(name,collection, filename, fields, applicationControler, action)
    @collection = collection
    @filename = filename
    @fields = fields
    @name = name
    @applicationControler = applicationControler
    @action = action
  end

  def render
    #não pude usar pois o buffer da sessão não pode passar de 4k
    #@applicationControler.session[@name+"_cn"] = @collection
    #@applicationControler.session[@name+"_fin"] = @filename
    #@applicationControler.session[@name+"_fn"] = @fields
    # monta a url de trabalho
    #limpa os requests conhecidos e pega os valores de paginacao      
    know_vars = ["exp_opc"]
    new_query_string = ""
    @applicationControler.request.query_string.split(/&/).inject({}) do |hash, setting|
      key, val = setting.split(/=/)
      if not key.nil?
        if not know_vars.include?(key)
          if val.nil?
            val = ""
          end
          new_query_string += key + "=" + val + "&"
        end
      end
    end

    if @collection.length > 0
      return "
    <script language='javascript'>
    function getReport() {
         document.location.href='#@action/?#{new_query_string}exp_opc='+document.getElementById('exp_opt').value+'&on=#@name';
    }
    </script>
    <select name='exp_opt' id='exp_opt'>
    <option value='1'>CSV</option>
    <option value='2'>Excel</option>
    <option value='3'>XML</option>
    </select>
    <input type='button' value='Download' onclick='getReport()'>
      "
    else
      return ""
    end
  end

  def csv
    #nome da coleção em memória

    @returnCSV = ""
    #monta cabeçalho
    @fields.each do |field|
      if not field.nil?
        field.each {|key,value|  @returnCSV += value.to_s + ";"}
      end
    end
    @returnCSV += "\r\n"
    @returnCSV = @returnCSV.chomp(";")

    # monta o CSV da coleção
    @collection.each_with_index do |obj, index|
      if not index.nil?
        @fields.each do |field|
          if not field.nil?
            field.each { |key,value|
              begin
                v = "#{eval("obj."+key.to_s+".to_s")};"
              rescue
                v = ';'
              end
              
              @returnCSV += v
            }
          end
        end
      end
      #retira o último caracter da linha
      @returnCSV = @returnCSV.chomp(";")
      @returnCSV += "\r\n"
    end
    #envia o conteúdo para o cliente
    return send_content(@returnCSV, @filename + ".csv")
  end

  def xls
    #nome da coleÃ§Ã£o em memÃ³ria
    # se nÃ£o existir na sessao, volta pra pagina anterior

    @returnXLS = ""
    #monta cabeçalho
    @fields.each do |field|
      if not field.nil?
        field.each {|key,value|  @returnXLS += "\"" + value.to_s + "\","}
      end
    end
    @returnXLS += "\r\n"
    @returnXLS = @returnXLS.chomp(",")

    # monta o XLS da coleÃ§Ã£o
    @collection.each_with_index do |obj, index|
      if not index.nil?
        @fields.each do |field|
          if not field.nil?
            field.each { |key,value|
              # o bloco begin/rescue/end evita que problemas na avaliação do comando (como acesso a um objeto nulo)
              # interfiram na geração dos resultados
              begin
                v = eval( "obj.#{key.to_s}.to_s" )
              rescue
                v = ''
              end
              @returnXLS += "\"#{v}\","
            }
          end
        end
      end
      #retira o Ãºltimo caracter da linha
      @returnXLS = @returnXLS.chomp(",")
      @returnXLS += "\r\n"
    end
    #envia o conteÃºdo para o cliente
    return send_content(@returnXLS, @filename+".xls")
  end


  def xml()
    #nome da coleÃ§Ã£o em memÃ³ria
    # se nÃ£o existir na sessao, volta pra pagina anterior

    @returnXML = "<?xml version=\"1.0\"?>\r\n\r\n<RESULTS>\r\n"

    # monta o XML da coleção
    @collection.each_with_index do |obj, index|
      if not index.nil?
        @returnXML += "<LINE>\r\n"
        @fields.each do |field|
          if not field.nil?
            field.each { |key,value|
              # o bloco begin/rescue/end evita que problemas na avaliação do comando (como acesso a um objeto nulo)
              # interfiram na geração dos resultados
              begin
                v = eval( "obj.#{key.to_s}.to_s" ).gsub("&", "&amp;")
              rescue
                v = ''
              end
              
              @returnXML += "<#{key.to_s}>#{v}</#{key.to_s}>"
              
              }
          end
        end
        #retira o Ãºltimo caracter da linha
        @returnXML = @returnXML.chomp(",")
        @returnXML += "\r\n"
        @returnXML += "</LINE>\r\n"
      end
    end
    @returnXML += "</RESULTS>"
    #envia o conteÃºdo para o cliente
    return send_content(@returnXML, @filename+".xml")
  end

  def send_content(content, filename)
    @applicationControler.headers.update(
      'Content-Length'            => content.length,
      'Content-Type'              => 'application/octet-stream\r',
      'Content-Disposition'       => "attachment; filename=#{filename}"
    )

    @applicationControler.headers['Cache-Control'] = 'private' if @applicationControler.headers['Cache-Control'] == 'no-cache'
    return content
  end
end
