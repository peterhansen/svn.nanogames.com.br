require 'gridview'
require 'device'
require 'date'

class WeekReportController < ApplicationController
  before_filter :authorize, :log_access
  before_filter :session_expiry

  layout :user_layout , :except => [:download]
  cattr_accessor :collection


  def index()
    @page_title = 'Weekly Download Report'
    
    redirect_to :action => :show
  end

  
  def init()
    # datas da semana
    @@iniDate = Time.now
    @@endDate = Time.now
    
    # testa se existe data setada
    if (!@_params['iniDate'].nil?)
      arr_date = @_params['iniDate'].split("/")
      @@iniDate = Time.gm(arr_date[2],arr_date[1],arr_date[0],0,0,0)
    else
      # pega o início da semana
      @@iniDate = @@iniDate - (60 * 60 * 24)*(@@iniDate.wday)
    end
    if (!@_params['endDate'].nil?)
      arr_date = @_params['endDate'].split("/")
      @@endDate = Time.gm(arr_date[2],arr_date[1],arr_date[0],0,0,0)
    else
      # pega o início da semana
      @@endDate = @@endDate + (60 * 60 * 24)*(6-(@@endDate.wday))
    end

    # coleção a ser trabalhada
    @@app_sql = ApplicationSql.new
    @@collection = @@app_sql.get_download_application_devices(@@iniDate,@@endDate, session_user.partner_id )

    # campos que serão renderizados
    @@fields = [
      { :id => 'id'},
      { 'app_version.app_version_number' => 'Aplicação'},
      { 'device.model_full' => 'Aparelho informado'},
      { 'access.user_agent' => 'User-agent'},
      { 'access.phone_number' => 'Número'},
      { 'access.ip_address' => 'IP'},
      { 'access.http_referrer' => 'Referrer' },
      { 'access.customer_id' => 'Usuário' },
      { 'access.device_model_full' => 'Aparelho detectado' },
      { 'access.created_at' => 'Hora' },
#      {:phone_number => 'MSISDN'},
#      {:name => 'Aplicação'},
#      {:model => 'Modelos'},
#      {:email_user => 'Usuário'},
#      {:download_date => 'Data de Download'}
    ]
    
  end


  def show()
    init()

    @iniDate = @@iniDate
    @endDate = @@endDate

    # verifica se a busca foi feita
    if (@_params['doSearch'].nil?)
      return
    end

    # iniciando o grid do relatÃ³rio (order inicial pelo vendor_id)
    @gridViewWeekReport = GridView.new("g1",@@collection,@@fields,"id", self)
    
    # definindo paginaÃ§Ã£o
    @gridViewWeekReport.width = 900
    @gridViewWeekReport.hasPager = true
    @gridViewWeekReport.pageSize = 20

    #define a linha vazia
    @gridViewWeekReport.emptyDataText = "<font>Nenhum resultado encontrado.</font>"

    # define fonte e cores das linhas 
    #@gridViewWeekReport.cellColor = "#FF0000"
    #@gridViewWeekReport.cellFontFace = "tahoma"
    #@gridViewWeekReport.cellFontColor = "#FFFFFF"
    @gridViewWeekReport.cellCssClass = "list-title list-line-odd"
    
    # define fonte e cores das linhas alternativas    
    #@gridViewWeekReport.cellColorAlternate = "#00FF00"
    #@gridViewWeekReport.cellFontColorAlternate = "#000000"
    #@gridViewWeekReport.cellFontFaceAlternate = "Verdana"
    @gridViewWeekReport.cellCssClassAlternate = "list-title list-line-even"
   
    #define propriedades do header
    #@gridViewWeekReport.headerColor = "#0000FF"
    @gridViewWeekReport.headerFontFace = "Arial"
    #@gridViewWeekReport.headerFontColor = "#FFFFFF"
    @gridViewWeekReport.headerFontWeight = "bold"
    @gridViewWeekReport.headerCssClass = "list-title"

    #define propriedades do pager
    @gridViewWeekReport.pagerColor = "#0000FF"
    @gridViewWeekReport.pagerFontFace = "Arial"
    @gridViewWeekReport.pagerFontColor = "#FFFFFF"
    @gridViewWeekReport.pagerFontWeight = "bold"
    @gridViewWeekReport.pagerAlign = "center"

    # monta exportador
    download
  end

  
  def download

    if @@collection.nil?
      init
    end

    @exporterReport = ReportExporter.new('rp1', @@collection, "report_"+@@iniDate.strftime("%d%m%Y")+"_"+@@endDate.strftime("%d%m%Y"), @@fields, self,"download")
   if !@_params['exp_opc'].nil?
      if @_params['exp_opc'] == "1"
        @returnContent = @exporterReport.csv
      else
        if @_params['exp_opc'] == "2"
          @returnContent = @exporterReport.xls
        else if @_params['exp_opc'] == "3"
            @returnContent = @exporterReport.xml
          end
        end
      end
    end
  end
end