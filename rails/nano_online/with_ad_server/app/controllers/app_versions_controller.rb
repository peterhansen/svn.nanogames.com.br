class AppVersionsController < ApplicationController
  
  before_filter :authorize, :only => :index
  before_filter :authorize_admin, :except => :index
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /app_versions
  # GET /app_versions.xml
  def index
    @app_versions = AppVersion.find( :all,
                                     :order => 'app_id, number', # TODO ordenar pelo nome da aplicação
                                     :conditions => { :app_id => session_user_apps } )

    @page_title = 'Applications versions'

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @app_versions }
    end
  end


  # GET /app_versions/1
  # GET /app_versions/1.xml
  def show
    @page_title = 'Applications versions'
    
    begin
      @app_version = AppVersion.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid app_version id #{params[:id]}" )
      flash[:notice] = "Invalid app_version"
      redirect_to :action => :index
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @app_version }
      end
    end
  end

  
  # GET /app_versions/new
  # GET /app_versions/new.xml
  def new
    @page_title = 'Applications versions'
    
    @app_version = AppVersion.new
    @states = AppVersionState.find(:all, :order => "name")

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @app_version }
    end
  end

  # GET /app_versions/1/edit
  def edit
    @app_version = AppVersion.find(params[:id])
    @families = Family.find( :all, :order => 'name', :conditions => 'ignored = false' )
    @page_title = "Editing #{ @app_version.app.name } version #{ @app_version.number }"
    @states = AppVersionState.find(:all, :order => "name")
    get_other_app_versions()
  end

  # POST /app_versions
  # POST /app_versions.xml
  def create
    @page_title = 'Applications versions'
    
    @app_version = AppVersion.new(params[:app_version])

    respond_to do |format|
      if @app_version.save
        refresh_app_files( false )
        
        flash[:notice] = "App version #{ @app_version.number } was successfully created."
        format.html { redirect_to(@app_version) }
        format.xml  { render :xml => @app_version, :status => :created, :location => @app_version }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @app_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /app_versions/1
  # PUT /app_versions/1.xml
  def update
    @page_title = 'Applications versions'
    
    params[ :app_version ][ :family_ids ] ||= []
    @app_version = AppVersion.find(params[:id])

    respond_to do |format|
      if @app_version.update_attributes(params[:app_version])
        refresh_app_files( false )
        
        flash[:notice] = "App version #{ @app_version.number } was successfully updated."
        format.html { redirect_to(@app_version) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @app_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /app_versions/1
  # DELETE /app_versions/1.xml
  def destroy
    @page_title = 'Applications versions'
    
    @app_version = AppVersion.find(params[:id])
    @app_version.destroy

    respond_to do |format|
      flash[:notice] = "App version #{ @app_version.number } was successfully destroyed."
      format.html { redirect_to(app_versions_url) }
      format.xml  { head :ok }
    end
  end


  def delete_file()
    @app_version = AppVersion.find(params[:id])
    @app_file = AppFile.find( params[ :file_id ] )

    @app_file.destroy() if ( @app_file )

    render :partial => 'files_list', :locals => { :app_version => @app_version }
  end


  def delete_app_version_update()
    @app_version = AppVersion.find(params[:id])
    @app_version_update = AppVersionUpdate.find( params[ :app_version_update_id ] )

    @app_version_update.destroy() if ( @app_version_update )

    get_other_app_versions()

    render :partial => 'updates', :locals => { :app_version => @app_version }
  end


  def create_updated_app_version()
    @app_version = AppVersion.find(params[:id])
    
    @new_app_version = AppVersion.find( params[ :new_app_version ].blank? ? @app_version : params[ :new_app_version ] )
    @old_app_version = AppVersion.find( params[ :old_app_version ].blank? ? @app_version : params[ :old_app_version ] )

    @update = AppVersionUpdate.new
    @update.app_version = @old_app_version
    @update.updated_app_version = @new_app_version

    @update.save

    get_other_app_versions()

    render :partial => 'updates', :locals => { :app_version => @app_version }
  end


  ##
  # Atualiza a lista de arquivos da versão.
  ##
  def refresh_app_files( update_div = true )
    if ( params[ :id ] )
      @app_version ||= AppVersion.find(params[:id])
    end
    
    if ( params[ :app_files ] )
      params[ :app_files ].each { |attr, value|
        unless value[ :app_file ].blank?
          new_app_file = AppFile.new
          new_app_file.app_version = @app_version
          new_app_file.filename = value[ :app_file ]
          new_app_file.save
        end
      }
      
    end
    
    if ( update_div )
      render :partial => 'files_list', :locals => { :app_version => @app_version }
    end
  end

  protected

  def get_other_app_versions()
    @other_app_versions = AppVersion.all( :conditions => { :app_id => @app_version.app },
                                          :order => 'number' )
  end


end
