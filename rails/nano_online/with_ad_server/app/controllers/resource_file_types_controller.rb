class ResourceFileTypesController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout
  
  # GET /resource_file_types
  # GET /resource_file_types.xml
  def index
    @resource_file_types = ResourceFileType.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @resource_file_types }
    end
  end

  # GET /resource_file_types/1
  # GET /resource_file_types/1.xml
  def show
    @resource_file_type = ResourceFileType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @resource_file_type }
    end
  end

  # GET /resource_file_types/new
  # GET /resource_file_types/new.xml
  def new
    @resource_file_type = ResourceFileType.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @resource_file_type }
    end
  end

  # GET /resource_file_types/1/edit
  def edit
    @resource_file_type = ResourceFileType.find(params[:id])
  end

  # POST /resource_file_types
  # POST /resource_file_types.xml
  def create
    @resource_file_type = ResourceFileType.new(params[:resource_file_type])

    respond_to do |format|
      if @resource_file_type.save
        flash[:notice] = 'ResourceFileType was successfully created.'
        format.html { redirect_to(@resource_file_type) }
        format.xml  { render :xml => @resource_file_type, :status => :created, :location => @resource_file_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @resource_file_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /resource_file_types/1
  # PUT /resource_file_types/1.xml
  def update
    @resource_file_type = ResourceFileType.find(params[:id])

    respond_to do |format|
      if @resource_file_type.update_attributes(params[:resource_file_type])
        flash[:notice] = 'ResourceFileType was successfully updated.'
        format.html { redirect_to(@resource_file_type) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @resource_file_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /resource_file_types/1
  # DELETE /resource_file_types/1.xml
  def destroy
    @resource_file_type = ResourceFileType.find(params[:id])
    @resource_file_type.destroy

    respond_to do |format|
      format.html { redirect_to(resource_file_types_url) }
      format.xml  { head :ok }
    end
  end
end
