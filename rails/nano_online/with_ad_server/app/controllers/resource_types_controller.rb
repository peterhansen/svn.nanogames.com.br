class ResourceTypesController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /resource_types
  # GET /resource_types.xml
  def index
    @resource_types = ResourceType.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @resource_types }
    end
  end

  # GET /resource_types/1
  # GET /resource_types/1.xml
  def show
    @resource_type = ResourceType.find(params[:id])
  end

  # GET /resource_types/new
  # GET /resource_types/new.xml
  def new
    @resource_type = ResourceType.new
    @file_types = ResourceFileType.all(:order => "name")

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @resource_type }
    end
  end

  # GET /resource_types/1/edit
  def edit
    @resource_type = ResourceType.find(params[:id])
    @file_types = ResourceFileType.all(:order => "name")
  end

  # POST /resource_types
  # POST /resource_types.xml
  def create
    @resource_type = ResourceType.new(params[:resource_type])
    params[:elements].each do |element|
      @resource_type.resource_type_elements.build(element) {|e| e.resource_type = @resource_type}
    end

    begin
      if @resource_type.save
        flash[:notice] = 'ResourceType was successfully created.'
        redirect_to(@resource_type)
      else
        @file_types = ResourceFileType.all(:order => "name")

        render :action => "new"
      end
    rescue
      @file_types = ResourceFileType.all(:order => "name")

      flash[:notice] = "Duplicated resource file types."
      render :action => "new"
    end
    # end
  end

  # PUT /resource_types/1
  # PUT /resource_types/1.xml
  def update
    @resource_type = ResourceType.find(params[:id])
    params[:elements].each do |element|
      @resource_type.resource_type_elements.build(element) {|e| e.resource_type = @resource_type}
    end

    respond_to do |format|
      if @resource_type.update_attributes(params[:resource_type])
        flash[:notice] = 'ResourceType was successfully updated.'
        format.html { redirect_to(@resource_type) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @resource_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /resource_types/1
  # DELETE /resource_types/1.xml
  def destroy
    @resource_type = ResourceType.find(params[:id])
    @resource_type.destroy

    respond_to do |format|
      format.html { redirect_to(resource_types_url) }
      format.xml  { head :ok }
    end
  end

  def delete_element()
    @resource_type = ResourceType.find(params[:id])
    @resource_element = ResourceTypeElement.find( params[ :file_id ] )

    @resource_element.destroy() if ( @resource_element )

    render :partial => 'elements_list'#, :locals => { :resource => @resource }
  end
end

