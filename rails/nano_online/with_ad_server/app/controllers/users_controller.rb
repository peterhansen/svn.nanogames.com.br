class UsersController < ApplicationController
  
  CUSTOMER_ACCESS = [:user_info, :change_password]
  EVERYONE_ACCESS = [:index, :recover_password]
  
  # before_filter :authorize,       :except => [:index,  :recover_password]
  before_filter :authorize_admin,   :except =>  CUSTOMER_ACCESS + EVERYONE_ACCESS
  # before_filter :authorize_nano,    :only => CUSTOMER_ACCESS
  before_filter :session_expiry
  
  layout :user_layout, :except => :recover_password

  # GET /users
  # GET /users.xml
  def index
    @users = User.find( :all,
                        :order => 'nickname',
                        :page => { :size => 30, :current => params[ :page ] } )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @users }
    end
  end


  # GET /users/1
  # GET /users/1.xml
  def show
    if @user = User.find(params[:id])
    else
      logger.error( "Invalid user id #{params[:id]}" )
      flash[:notice] = "Invalid user"
      redirect_to :action => :index
    end
  end

  
  # GET /users/new
  # GET /users/new.xml
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @user }
    end
  end


  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    unless ( session_user.is_admin? || @user == session_user )
      redirect_to :controller => :login, :action => :index
    end
  end


  # POST /users
  # POST /users.xml
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        flash[:notice] = "User #{ @user.nickname } was successfully created."
        format.html { redirect_to(@user) }
        format.xml  { render :xml => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # PUT /users/1
  # PUT /users/1.xml
  def update
    @user = User.find(params[:id])

    unless ( session_user.is_admin? )
      if ( params[ :previous_password ].blank? )
        # usuário deixou a password antiga em branco e alterou a nova
        if ( !params[ :user ][ :password ].blank? || !params[ :user ][ :password ].blank? )
          flash.now[:notice] = "Type your previous password."
          format.html { render :action => "edit" }
          format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
          return
        end
      else
        # usuário digitou senha antiga
        if ( !User.authenticate( @user.nickname, params[ :previous_password ][ 0 ] ) )
          # mas digitou errado...
          flash[:notice] = "Your previous password is incorrect."
          redirect_to :action => :edit
#          format.html { render :action => "edit" }
#          format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
          return
        end
      end
    end

    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:notice] = "User #{ @user.nickname } was successfully updated."
        format.html { render :action => "edit" }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /users/1
  # DELETE /users/1.xml
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      flash[:notice] = "User #{ @user.nickname } was successfully destroyed."
      format.html { redirect_to(users_url) }
      format.xml  { head :ok }
    end
  end
  
  def user_info
    @user = User.find(session[:user_id])
  end
  
  def change_password
    @user = User.find(session[:user_id])
    
    if ( request.post? )
      @user = User.find( session[ :user_id ] )
      # o password_confirmation não está funcionando no form_tags
      if ( params[ :password ] == params[ :password_confirmation ] )
        if ( @user.hashed_password == User.encrypted_password( params[:current_password], @user.salt ) )
          @user.password = params[ :password ]
      
          if ( @user.save )
              flash[ :notice ] = "Password changed sucessfully.".t
              redirect_to :controller => :login, :action => :index
          else
            flash[ :notice ] = "Password could not be changed.".t
            redirect_to :action => :change_password
          end
        else
          flash[ :notice ] = "Wrong password."
        end
      else
        flash[ :notice ] = "Password and Confirmation didn't match.".t
      end
    else  
      @user = User.new
    end
  end
  
  def recover_password
    if ( request.post? )
      if ( user = User.find_by_email( params[ :email ] ) )
        user.resetar_senha
        
        flash[:notice] = "Sua nova senha foi enviada para seu email.".t
        redirect_to :controller => :login, :action => :login
      else
        flash[ :notice ] = "Email não encontrado."
      end
    end
  end
end
