class AppCategoriesController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /app_categories
  # GET /app_categories.xml
  def index
    @app_categories = AppCategory.find( :all,
                                        :order => 'name')

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @app_categories }
    end
  end

  # GET /app_categories/1
  # GET /app_categories/1.xml
  def show
    redirect_to :action => 'index'
  end

  # GET /app_categories/new
  # GET /app_categories/new.xml
  def new
    @app_category = AppCategory.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @app_category }
    end
  end

  # GET /app_categories/1/edit
  def edit
    @app_category = AppCategory.find(params[:id])
  end

  # POST /app_categories
  # POST /app_categories.xml
  def create
    @app_category = AppCategory.new(params[:app_category])

    respond_to do |format|
      if @app_category.save
        flash[:notice] = "App category #{ @app_category.name } was successfully created."
        format.html { redirect_to(@app_category) }
        format.xml  { render :xml => @app_category, :status => :created, :location => @app_category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @app_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /app_categories/1
  # PUT /app_categories/1.xml
  def update
    @app_category = AppCategory.find(params[:id])

    respond_to do |format|
      if @app_category.update_attributes(params[:app_category])
        flash[:notice] = "App category #{ @app_category.name } was successfully updated."
        format.html { redirect_to(@app_category) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @app_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /app_categories/1
  # DELETE /app_categories/1.xml
  def destroy
    @app_category = AppCategory.find(params[:id])
    @app_category.destroy

    respond_to do |format|
      flash[:notice] = "App category #{ @app_category.name } was successfully destroyed."
      format.html { redirect_to(app_categories_url) }
      format.xml  { head :ok }
    end
  end
  
end
