class AdChannelStatusesController < ApplicationController
  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /ad_channel_statuses
  # GET /ad_channel_statuses.xml
  def index
    @ad_channel_statuses = AdChannelStatus.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @ad_channel_statuses }
    end
  end

  # GET /ad_channel_statuses/1
  # GET /ad_channel_statuses/1.xml
  def show
    redirect_to :action => 'index'
  end

  # GET /ad_channel_statuses/new
  # GET /ad_channel_statuses/new.xml
  def new
    @ad_channel_status = AdChannelStatus.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ad_channel_status }
    end
  end

  # GET /ad_channel_statuses/1/edit
  def edit
    @ad_channel_status = AdChannelStatus.find(params[:id])
  end

  # POST /ad_channel_statuses
  # POST /ad_channel_statuses.xml
  def create
    @ad_channel_status = AdChannelStatus.new(params[:ad_channel_status])

    respond_to do |format|
      if @ad_channel_status.save
        flash[:notice] = 'AdChannelStatus was successfully created.'
        format.html { redirect_to(@ad_channel_status) }
        format.xml  { render :xml => @ad_channel_status, :status => :created, :location => @ad_channel_status }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @ad_channel_status.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ad_channel_statuses/1
  # PUT /ad_channel_statuses/1.xml
  def update
    @ad_channel_status = AdChannelStatus.find(params[:id])

    respond_to do |format|
      if @ad_channel_status.update_attributes(params[:ad_channel_status])
        flash[:notice] = 'AdChannelStatus was successfully updated.'
        format.html { redirect_to(@ad_channel_status) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ad_channel_status.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_channel_statuses/1
  # DELETE /ad_channel_statuses/1.xml
  def destroy
    @ad_channel_status = AdChannelStatus.find(params[:id])
    @ad_channel_status.destroy

    respond_to do |format|
      format.html { redirect_to(ad_channel_statuses_url) }
      format.xml  { head :ok }
    end
  end
end
