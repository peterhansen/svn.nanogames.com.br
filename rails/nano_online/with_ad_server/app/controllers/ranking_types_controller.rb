class RankingTypesController < ApplicationController
  layout :user_layout

  def index
    @ranking_types = RankingType.all(:include => :unit_format)
  end

  def show
    @ranking_type = RankingType.find(params[:id], :include => :unit_format)
  end

  def new
    @ranking_type = RankingType.new
    @unit_formats = UnitFormat.all
  end

  def create
    @ranking_type = RankingType.new(params[:ranking_type])

    if @ranking_type.save
      flash[:notice] = 'Ranking type was successfully created.'
      redirect_to(@ranking_type)
    else
      @unit_formats = UnitFormat.all
      render :action => "new"
    end
  end

  def edit
    @ranking_type = RankingType.find(params[:id], :include => :unit_format)
    @unit_formats = UnitFormat.all
  end

  def update
    @ranking_type = RankingType.find(params[:id])

    if @ranking_type.update_attributes(params[:ranking_type])
      flash[:notice] = 'Ranking entry was successfully updated.'
      redirect_to(@ranking_type)
    else
      @unit_formats = UnitFormat.all
      render :action => "edit"
    end
  end

  def destroy
    @ranking_type = RankingType.find(params[:id])
    @ranking_type.destroy

    redirect_to(ranking_types_url)
  end

end
