class NewsFeedsController < ApplicationController
  
  before_filter :authorize_admin, :except => :refresh
  before_filter :session_expiry, :except => :refresh
  before_filter :authorize_nano, :only => [ :refresh ]
  before_filter :log_access, :only => [ :refresh ]
  
  layout :user_layout, :except => [ :refresh ]
  
  # GET /news_feeds
  # GET /news_feeds.xml
  def index
    @page_title = 'News feeds list'.t
    # @news_feeds = NewsFeed.find( :all,
    #                              :order => 'created_at DESC')
    @news_feeds = NewsFeed.find( :all,
                                 :order => 'created_at DESC',
                                 :page => { :size => 30, :current => params[ :page ] } )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @news_feeds }
    end
  end

  # GET /news_feeds/1
  # GET /news_feeds/1.xml
  def show
    @page_title = "News feed detail"
    @news_feed = NewsFeed.find(params[:id])
  end

  # GET /news_feeds/new
  # GET /news_feeds/new.xml
  def new
    @page_title = 'New news feed'
    @news_feed = NewsFeed.new
    @translation = NewsFeedTranslation.new
    
    @news_feed_categories = NewsFeedCategory.find( :all )
    @languages = Language.find( :all )

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @news_feed }
    end
  end

  # GET /news_feeds/1/edit
  def edit
    @news_feed = NewsFeed.find(params[:id])
    @news_feed_categories = NewsFeedCategory.find( :all )

    @page_title = "Editing news feed \##{@news_feed.id}"
  end

  # POST /news_feeds
  # POST /news_feeds.xml
  def create
    @news_feed = NewsFeed.new(params[:news_feed])
    @translation = NewsFeedTranslation.new(params[:translation])
    @news_feed.news_feed_translation << @translation

    respond_to do |format|
      if @news_feed.save
        flash[:notice] = "News feed #{ @news_feed.id } was successfully created."
        format.html { redirect_to :action => :index }
        format.xml  { render :xml => @news_feed, :status => :created, :location => @news_feed }
      else  
        @news_feed_categories = NewsFeedCategory.find( :all )
        @languages = Language.find( :all )
        
        flash[:notice] = "News feed could not be created."
        format.html { render :action => "new" }
        format.xml  { render :xml => @news_feed.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /news_feeds/1
  # PUT /news_feeds/1.xml
  def update
    @news_feed = NewsFeed.find(params[:id])

    respond_to do |format|
      if @news_feed.update_attributes(params[:news_feed])
        flash[:notice] = "News feed #{ @news_feed.id } was successfully updated."
        format.html { redirect_to(@news_feed) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @news_feed.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /news_feeds/1
  # DELETE /news_feeds/1.xml
  def destroy
    @news_feed = NewsFeed.find(params[:id])
    if @news_feed.destroy
      respond_to do |format|
        flash[:notice] = "News feed #{ @news_feed.id } was successfully destroyed."
        format.html { redirect_to(news_feeds_url) }
        format.xml  { head :ok }
      end
    else
      flash[:notice] = "News feed #{ @news_feed.id } could not be destroyed."
      redirect_to :action => :index
    end
  end


  def refresh()
    render_binary() { |out|
      # como a gerência das notícias é feita em toda conexão ao sistema, só é necessário informar que a conexão foi ok
      out.write_byte( ID_RETURN_CODE )
      out.write_short( RC_OK )
    }
  end
  
  def add_translation
    if request.post?
      @news_feed = NewsFeed.find(params[:news_feed][:id])
      @translation = NewsFeedTranslation.new(params[:news_feed_translation])
      @news_feed.news_feed_translation << @translation
      
      @languages = Language.find( :all )
      
      if @news_feed.save
        flash[:notice] = "Translation saved."
        redirect_to :action => "show", :id => params[:news_feed][:id]
      end
    else
      @news_feed = NewsFeed.find(params[:id])
      @translation = NewsFeedTranslation.new
      @languages = Language.find( :all )
    end
  end

  def edit_translation
    @translation = NewsFeedTranslation.find(params[:id])
  end

  def update_translation
    @translation = NewsFeedTranslation.find(params[:translation][:id])
    
    if @translation.update_attributes(params[:translation])
      flash[:notice] = "Translation saved."
     redirect_to :action => "show", :id => @translation.news_feed_id
    else
      flash[:notice] = "Translation could not be saved."
      redirect_to :action => "edit_translation"
    end
  end

  def delete_translation
    @translation = NewsFeedTranslation.find(params[:id])
    feed = NewsFeed.find(@translation.news_feed_id)

    if feed.translations.size > 1
      if @translation.destroy
        flash[:notice] = "News translation deleted."
      else
        flash[:notice] = "News translation could not be deleted."
      end
      redirect_to :action => :show, :id => @translation.news_feed_id
    else
      if feed.destroy
        flash[:notice] = "Feed deleted."
        redirect_to :action => :index
      else
        flash[:notice] = "Feed could not be deleted."
      end
    end
  end
  
end
