class AdPiecesController < ApplicationController

  layout :user_layout
  
  # GET /ad_pieces
  # GET /ad_pieces.xml
  def index
    @ad_pieces = AdPiece.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @ad_pieces }
    end
  end

  # GET /ad_pieces/1
  # GET /ad_pieces/1.xml
  def show
    @ad_piece = AdPiece.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ad_piece }
    end
  end

  # GET /ad_pieces/new
  # GET /ad_pieces/new.xml
  def new
    @ad_piece = AdPiece.new
    @channel = AdChannel.find(:all, :order => "title")

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ad_piece }
    end
  end

  # GET /ad_pieces/1/edit
  def edit
    @ad_piece = AdPiece.find(params[:id])
    @channel = AdChannel.find(:all, :order => "title")
  end

  # POST /ad_pieces
  # POST /ad_pieces.xml
  def create
    @ad_piece = AdPiece.new(params[:ad_piece])

    respond_to do |format|
      if @ad_piece.save
        flash[:notice] = 'AdPiece was successfully created.'
        format.html { redirect_to(@ad_piece) }
        format.xml  { render :xml => @ad_piece, :status => :created, :location => @ad_piece }
      else
        @channel = AdChannel.find(:all, :order => "title")
        format.html { render :action => "new" }
        format.xml  { render :xml => @ad_piece.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ad_pieces/1
  # PUT /ad_pieces/1.xml
  def update
    @ad_piece = AdPiece.find(params[:id])

    respond_to do |format|
      if @ad_piece.update_attributes(params[:ad_piece])
        flash[:notice] = 'AdPiece was successfully updated.'
        format.html { redirect_to(@ad_piece) }
        format.xml  { head :ok }
      else
        @channel = AdChannel.find(:all, :order => "title")
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ad_piece.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_pieces/1
  # DELETE /ad_pieces/1.xml
  def destroy
    @ad_piece = AdPiece.find(params[:id])
    @ad_piece.destroy

    respond_to do |format|
      format.html { redirect_to(ad_pieces_url) }
      format.xml  { head :ok }
    end
  end
end
