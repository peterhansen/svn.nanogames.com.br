class AdCampaignsController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /ad_campaigns
  # GET /ad_campaigns.xml
  def index
    @ad_campaigns = AdCampaign.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @ad_campaigns }
    end
  end

  # GET /ad_campaigns/1
  # GET /ad_campaigns/1.xml
  def show
    @ad = AdCampaign.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ad }
    end
  end

  # GET /ad_campaigns/new
  # GET /ad_campaigns/new.xml
  def new
    @ad = AdCampaign.new
    @advertiser = Advertiser.find(:all)
		# TODO o advertiser deve estar travado para o partner do usuário atual (somente Nano Games pode escolher o advertiser livremente)
		@ad_pieces = AdPiece.all( :order => :title )
		# TODO um ad_piece deve ter também um advertiser relacionado, para que um anunciante não possa escolher anúncios de terceiros ao criar/editar uma campanha

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ad }
    end
  end

  # GET /ad_campaigns/1/edit
  def edit
    @ad = AdCampaign.find(params[:id])
    @advertiser = Advertiser.find(:all)
		@ad_pieces = AdPiece.all( :order => :title )
		# TODO um ad_piece deve ter também um advertiser relacionado, para que um anunciante não possa escolher anúncios de terceiros ao criar/editar uma campanha
  end

  # POST /ad_campaigns
  # POST /ad_campaigns.xml
  def create
    @ad = AdCampaign.new(params[:ad_campaign])

    respond_to do |format|
      if @ad.save
        flash[:notice] = 'AdCampaign was successfully created.'
        format.html { redirect_to(@ad) }
        format.xml  { render :xml => @ad, :status => :created, :location => @ad }
      else
        @advertiser = Advertiser.find(:all)
        
        format.html { render :action => "new" }
        format.xml  { render :xml => @ad.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ad_campaigns/1
  # PUT /ad_campaigns/1.xml
  def update
    @ad = AdCampaign.find(params[:id])

    respond_to do |format|
      if @ad.update_attributes(params[:ad_campaign])
        flash[:notice] = 'AdCampaign was successfully updated.'
        format.html { redirect_to(@ad) }
        format.xml  { head :ok }
      else
        @advertiser = Advertiser.find(:all)
        
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ad.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_campaigns/1
  # DELETE /ad_campaigns/1.xml
  def destroy
    @ad = AdCampaign.find(params[:id])
    @ad.destroy

    respond_to do |format|
      format.html { redirect_to(ad_campaigns_url) }
      format.xml  { head :ok }
    end
  end
end
