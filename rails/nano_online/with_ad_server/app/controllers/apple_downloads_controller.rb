class AppleDownloadsController < ApplicationController
  layout :user_layout
  
  def index
    if ( request.post? )
      first_date = params[:range]["start(1i)"] + "/" + params[:range]["start(2i)"] + "/" + params[:range]["start(3i)"]
      last_date = params[:range]["end(1i)"] + "/" + params[:range]["end(2i)"] + "/" + params[:range]["end(3i)"]
      apps = session_user_apps.map {|a| a.id }
      
      unless params[:apps] 
        params[:apps] = apps
      else
        params[:apps].each_index do |index|
          params[:apps][index] = App.find_by_name(params[:apps][index]).id
        end
      end
      
      @apple_downloads = AppleDownload.find(  :all,
                                              :conditions => ["begin_date between ? and ? #{params[:countries].blank? ? "" : "and country_id in (select id from countries where name in (\"#{params[:countries].join("\", \"")}\"))"} #{params[:apps].blank? ? "" : "and app_id in (\"#{params[:apps].join("\", \"")}\")"}",
                                              first_date,
                                              last_date])
                                              
      @first_date = Date.new(params[:range]["start(1i)"].to_i ,params[:range]["start(2i)"].to_i ,params[:range]["start(3i)"].to_i )
      
      @last_date = Date.new(params[:range]["end(1i)"].to_i ,params[:range]["end(2i)"].to_i ,params[:range]["end(3i)"].to_i )

      @countries = AppleDownload.find_by_sql(["select name from countries where id in (select distinct country_id from apple_downloads)"]).map {|a| a.name}
      # @countries.sort! {|a, b| a.country_name <=> b.country_name}
      @apps = AppleDownload.find_by_sql(["select name from apps where id in (select distinct app_id from apple_downloads) && id in (\"#{apps.join("\", \"")}\")"]).map {|a| a.name}
    else
      apps = session_user_apps.map {|a| a.id }
      
      @apple_downloads = AppleDownload.find(:all, 
                                            :conditions => ["#{apps.blank? ? "" : "app_id in (\"#{apps.join("\", \"")}\")"}"]) unless apps.blank?

      date = AppleDownload.find( :all, :order => "begin_date" ).first
      date.blank? ? @first_date = Date.new : @first_date = AppleDownload.find( :all, :order => "begin_date" ).first.begin_date
      date.blank? ? @last_date = Date.new : @last_date = AppleDownload.find( :all, :order => "begin_date" ).last.begin_date
      @countries = AppleDownload.find_by_sql(["select name from countries where id in (select distinct country_id from apple_downloads where app_id in (\"#{apps.join("\", \"")}\"))"]).map {|a| a.name}
      # @countries.sort! {|a, b| a.country_name <=> b.country_name}
      @apps = AppleDownload.find_by_sql(["select name from apps where id in (select distinct app_id from apple_downloads) && id in (\"#{apps.join("\", \"")}\")"]).map {|a| a.name}
    end
  end
  
  def upload_file
    add_apple_daily_report(params[:file])
  end
  
  def add_apple_daily_report( arq )
    if arq != "" && arq.original_filename.match('.txt')
      apple_download_report = AppleDownloadReport.new
      apple_download_report.name = arq.original_filename
    
      if apple_download_report.save
        arq.each_line do |line|
          data = line.split("\t")
          
          unless data[0] == "Provider"
            apple_download = AppleDownload.new_from_array(data)
            apple_download.apple_download_report = apple_download_report
            apple_download.save
          end
        end
        flash[:notice] = "Report saved."
      else
        flash[:notice] = "Report already imported."
      end
    else
      flash[:notice] = "Invalid file."
    end
    redirect_to :action => :index
  end
  
  def csv
    @apple_downloads = AppleDownload.find(params[:data].split(", "))
    
    keys = %w{Date App Country Category Units Price Total}
    
    csv_string = FasterCSV.generate { }
    
    csv_string << keys.join("; ") << "\n"
    
    @apple_downloads.each do |row|
      values = []
    
      values << row.begin_date
      values << row.app.name
      values << row.country.name
      values << row.apple_product_category.category
      values << row.units
      values << row.royalty_price
      values << row.royalty_price * row.units
      
      csv_string << values.join("; ") << "\n"
      
    end
    
    filename = "apple_download_report.csv"
    send_data ( csv_string,
                :type => "text/csv; charset=utf-8; header=present",
                :filename => filename)
    #TODO dimas: repassar o objeto @apple_downloads para a action :index
    # redirect_to :action => :index
  end
end
