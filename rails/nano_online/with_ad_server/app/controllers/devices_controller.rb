class DevicesController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /devices
  # GET /devices.xml
  def index
    @devices = Device.find( :all,
                            :order => 'vendor_id, model' )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @devices }
    end
  end

  # GET /devices/1
  # GET /devices/1.xml
  def show
    begin
      @device = Device.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid device id #{params[:id]}" )
      flash[:notice] = "Invalid device"
      redirect_to :action => :index
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @device }
      end
    end
    
  end

  # GET /devices/new
  # GET /devices/new.xml
  def new
    @device = Device.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @device }
    end
  end

  # GET /devices/1/edit
  def edit
    @device = Device.find(params[:id])
  end

  # POST /devices
  # POST /devices.xml
  def create
    @device = Device.new(params[:device])

    respond_to do |format|
      if @device.save
        flash[:notice] = "Device #{ @device.model } was successfully created."
        format.html { redirect_to(@device) }
        format.xml  { render :xml => @device, :status => :created, :location => @device }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @device.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /devices/1
  # PUT /devices/1.xml
  def update
    params[ :device ][ :band_ids ] ||= []
    params[ :device ][ :data_service_ids ] ||= []  
    params[ :device ][ :optional_api_ids ] ||= []  
    @device = Device.find(params[:id])

    respond_to do |format|
      if @device.update_attributes(params[:device])
        flash[:notice] = "Device #{ @device.model } was successfully updated."
        format.html { redirect_to(@device) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @device.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /devices/1
  # DELETE /devices/1.xml
  def destroy
    @device = Device.find(params[:id])
    begin
      if @device.destroy
        flash[:notice] = "Device #{ @device.model } was successfully destroyed."
      else
        flash[:notice] = "Device #{ @device.model } could not be destroyed."
      end
    rescue Exception => e
      flash[:notice] = "An error stopped #{ @device.model } from been deleted."
    ensure
    redirect_to :action => :index
    end
  end
  
end
