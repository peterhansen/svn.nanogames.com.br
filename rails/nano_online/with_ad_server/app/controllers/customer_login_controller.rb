require 'digest/sha1'

class CustomerLoginController < ApplicationController
  layout :user_layout
  
  def index
    if session[:customer_id]
      @logado = true
    end
  end

  def login
    reset_session
    if request.post?
      user = Customer.authenticate(params[:name], params[:password])
      
      if user
        if user.validated_at
          # armazena somente o id do usuário na sessão, para evitar excesso no tamanho da variável de sessão
          session[:customer_id] = user.id
          flash[:notice] = "Bem vindo!"
          redirect_to :action => 'index'
        else
          redirect_to :action => 'reenviar_validacao', :id => user.id
        end
      else
        flash[:notice] = 'Invalid user/password'
        redirect_to :action => 'login'
      end
    end
  end

  
  def cadastro
    @customer = Customer.new
  end


  def salvar_cadastro
    @customer = Customer.new(params[:customer])
    @customer.validation_hash = create_validation_hash
    
    
    if @customer.save
      @customer.send_email_validation
      return if request.xhr?
      
      flash[:notice] = 'Verifique seu email.'
      render :action => 'index'
    else
      render :action => 'cadastro'
    end
  end
  
  def create_validation_hash
    return Digest::SHA1.hexdigest("#{self.object_id}*\#{@\&KaJDS-\*\&l\=kjjd_as927a")
  end


  def logout
    reset_session
    flash[:notice] = 'Logged out.'
    redirect_to(:action => 'index')
  end


  def validate()
    @customer = Customer.find_by_validation_hash(params[:hash])
    if @customer
      @customer.validated_at = Time.new
      @customer.validation_hash = nil
      @customer.validate_before = nil
      
      if @customer.save
        flash[:notice] = 'E-mail validado com sucesso.'
        redirect_to :action => 'index'
      end
    else
      flash[:notice] = 'Falha na validação do e-mail'
      redirect_to :action => 'index'
    end
  end


  def reenviar_validacao
    @customer = Customer.find(params[:id])
  end


  def reenvio
    @customer = Customer.find(params[:id])
    
    @customer.send_email_validation
    return if request.xhr?
    
    flash[:notice] = "Validação re-enviada para #{@customer.email}."
    redirect_to :action => 'index'
  end
  
  
  # TODO não está aparecendo a mensagem do flash.
  def recuperar_senha
    if request.post?
      if customer = Customer.find_by_email(params[:email])
        customer.reset_password
        
        flash[:notice] = "Sua nova senha foi enviada para seu email.".t
        redirect_to :action => :login
      end
    end
  end
  
  # TODO dá para ficar mais simples?

end
