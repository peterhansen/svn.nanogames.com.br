require 'zip/zip'
require 'zip/zipfilesystem'

class DownloadsController < ApplicationController

  CLIENT_ACTIONS = [ :vendor_select, :select_app, :download_jad, :select_model, :device_selected, :download_file_by_id, :download_file_by_short_name, :download_seda_puzzle, :select_specific_model ]

  before_filter :no_cache, :only => CLIENT_ACTIONS
  before_filter :authorize, :except => CLIENT_ACTIONS
  before_filter :log_access
  before_filter :session_expiry, :except => CLIENT_ACTIONS

  layout :user_layout, :except => CLIENT_ACTIONS

  # GET /downloads
  # GET /downloads.xml
  def index
    if ( session[ :sort_order ].nil? )
      session[ :sort_order ] = 'id'
    end
    @downloads = Download.all( :order => session[ :sort_order ] )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @downloads }
    end
  end


  # GET /downloads/1
  # GET /downloads/1.xml
  def show
    redirect_to :action => :index
  end


  # GET /downloads/new
  # GET /downloads/new.xml
  def new
    @download = Download.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @download }
    end
  end

  
  # GET /downloads/1/edit
  def edit
    @download = Download.find( params[ :id ] )
  end

  
  # POST /downloads
  # POST /downloads.xml
  def create
    @download = Download.new(params[:download])

    respond_to do |format|
      if @download.save
        flash[:notice] = "Download #{ @download.id } was successfully created."
        format.html { redirect_to(@download) }
        format.xml  { render :xml => @download, :status => :created, :location => @download }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @download.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # PUT /downloads/1
  # PUT /downloads/1.xml
  def update
    @download = Download.find(params[:id])

    respond_to do |format|
      if @download.update_attributes(params[:download])
        flash[:notice] = "Download #{ @download.id } was successfully updated."
        format.html { redirect_to(@download) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @download.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # DELETE /downloads/1
  # DELETE /downloads/1.xml
  def destroy
    @download = Download.find(params[:id])
    @download.destroy

    respond_to do |format|
      flash[:notice] = "Download #{ @download.id } was successfully destroyed."
      format.html { redirect_to(downloads_url) }
      format.xml  { head :ok }
    end
  end
  
  
  # GET /downloads
  # GET /downloads.xml
  def select_app( first_run = true )
    # TODO verificar leitura de parâmetro indicando qual aplicativo se está tentando baixar. Exemplo: downloads?short=LUXG
    @supported_versions = []
    begin
      @vendor = Vendor.find(session[:selected_vendor])
      
      # se for a primeira chamada desse método, armazena o aparelho detectado. Caso contrário, armazena o aparelho escolhido.
      session[ :device_chosen ] = params[ :device ] if ( first_run )
    rescue Exception => e
      redirect_to :action => :vendor_select
      return
    end

    
    if ( session[ :device_chosen ] )
      # aparelho detectado; procura aplicativos que o suportem
      state_available = AppVersionState.find_by_name( 'Available' )
      @app_versions = AppVersion.all( :conditions => { :app_version_state_id => state_available } )

      @app_versions.each() { |app_version|
        devices = app_version.devices()

        if ( devices.include?( session[ :device_chosen ] ) )
          @supported_versions << app_version.last_version
        end
      }

      # remove as versões duplicadas e ordena por nome
      @supported_versions.uniq!
      @supported_versions = @supported_versions.sort_by { |v| v.app_version_number }

      if ( @supported_versions.empty? )
        # mostra página indicando que não há aplicativos disponíveis, e armazena a informação
        render :action => :device_unsupported, :layout => default_page_layout
      else
        @page_title = 'Nano Games'
        render :action => :select_app, :layout => default_page_layout
      end

    else
      # aparelho não encontrado - exibe a seleção de aparelho (com o fabricante detectado, se for o caso)
      redirect_to :action => :vendor_select
    end
  end
  
  
  def vendor_select
    params[ :app_version ] ||= nil
    session[:selected_vendor] = nil

    @vendors = Vendor.all( :order => "name" )
    @vendors.each { |v|
      if ( v.devices.length <= 0 )
        @vendors.delete( v )
      end
    }
    @page_title = 'Nano Games'
    render :layout => default_page_layout
  end


  ##
  #
  ##
  def download_jad( send_jad_inline = true )
    begin
      params[ :app_version ] = params[ :download ][ :chosen ] if params[ :app_version ].nil?
      download = Download.new( {
          :app_version_id => params[ :app_version ],
          :access => session[ :last_access ],
          :device => session[ :device_chosen ]
        } )

      filename = "#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}"

      if ( File::exists?( filename ) )
        if ( download.save )
          # trata o caso dos aparelhos que precisam receber o .jar direto (Nokia S60)
          if ( false && download.device.family.install_jar ) # TODO teste - sempre envia o .jad
            jad_file = JadFile.new( filename )

            jad_file.file_paths do |key, value|
              download_code = DownloadCode.new( { :download => download, :file => value } )

              if ( download_code.save )
                redirect_to "#{URL_NANO_ONLINE}/download/#{download_code.code}"
                return
              else
                logger.error( "error saving download code: #{download_code}" )
              end
            end
          else
            # cria um novo arquivo .jad contendo os novos links para os arquivos binários (não é necessário salvá-lo, pois é
            # enviado diretamente para o usuário)
            jad_file = JadFile.new( filename )
            # atualiza MIDlet-Install-Notify e MIDlet-Delete-Notify
            jad_file.info[ 'MIDlet-Install-Notify' ] = "#{URL_NANO_ONLINE}/notify/i/#{download.id}"
            jad_file.info[ 'MIDlet-Delete-Notify' ] = "#{URL_NANO_ONLINE}/notify/d/#{download.id}"

            # cria os códigos de download de cada arquivo binário
            # TODO no caso do BlackBerry, não é necessário o arquivo .jar (somente os .cod)
            
            if ( !params[:zipped].blank? && params[:zipped] ==  "yes" )
              t = Tempfile.new("temp_zip_#{request.remote_ip}")
              
              begin
                Zip::ZipOutputStream.open(t.path) do |zip|
                  
                  zip.put_next_entry("#{download.app_version.app.name}.jad")
                  zip.print IO.read("#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}")
                  
                  jad_file.file_paths do |key, value|
                    zip.put_next_entry("#{value}")
                    zip.print IO.read("#{PATH_RELEASES}/#{download.app_version.app_version_number_clean}/#{value}")
                  end
                end
                send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => "#{download.app_version.app.name}.zip"
                # redirect_to :device_select
                return
              rescue
                render( :action => 'server_error', :layout => default_page_layout )
                return
              end
              
            else
              jad_file.file_paths do |key, value|
                download_code = DownloadCode.new( { :download => download, :file => value } )

                if ( download_code.save )
                  jad_file.info[ key ] = "#{URL_NANO_ONLINE}/download/#{download_code.code}"
                else
                  logger.error( "error saving download code: #{download_code}" )
                end
              end
            end

            if ( send_jad_inline )
              # envia diretamente o arquivo .jad
              send_data( jad_file.data, :type => default_content_type( 'jad' ), :filename => "#{download.app_version.app_version_number_clean}.jad" )
            else
              # TODO cria um download code para o jad também, e mostra uma página com o link
            end
          end
        else
          logger.error( "error saving download: #{download.errors}" )
          render( :action => 'server_error', :layout => default_page_layout )
        end
      else
        logger.error( "File not found: #{filename}" )
        # TODO redirecionar o usuário para página padrão de erro
        render( :action => 'server_error', :layout => default_page_layout )
      end

    rescue Exception => e
      # trata erro ao criar arquivo
      logger.error( "Exception in downloads_controller.download_jad: #{e}" )
      # TODO enviar mensagem de erro padrão
      render( :action => 'server_error', :layout => default_page_layout )
    end
  end


  ##
  # Permite acesso a um arquivo através de um código de download, que só pode ser utilizado uma vez.
  ##
  def download_file_by_id()
    download_code = DownloadCode.first( :conditions => { :code => params[ :id ] } )

    if ( download_code )
      # o código não pode ter sido usado e o user-agent desta conexão deve ser o mesmo do pedido de download
      # TODO além do user-agent, testar IP e/ou número de telefone?
#      if ( @download_code.download.user_agent == request.user_agent )
        if ( download_code.used_at.nil? || download_code.used_at.blank? )
          unless ( request.user_agent.match( /MDS_\d*\.\d+\.(\d+\.)?(\d+)?/ ) )
            download_code.used_at = Time.now
            download_code.save()
          end

          send_file( "#{PATH_RELEASES}/#{download_code.download.app_version.app_version_number_clean}/#{download_code.file}",
                     :type => default_content_type( File.extname( download_code.file ) ) )
        else
          logger.error( "código de download já utilizado: #{download_code.code} em #{download_code.used_at}" )
        end
#      else
#        logger.error( "download code UA != download UA" )
#      end
    else
      logger.error( "código de download inválido: #{params[ :id ]}" )
    end
  end


  def download_file_by_short_name()
    logger.info( "APP SHORT NAME: #{params[ 'app_short_name' ]}" )
    request.env.each { |k, v|
      logger.info( "request[ #{k} ]: #{v}")
    }

    params.each { |k,v|
      logger.info( "params[ #{k} ]: #{v}")
    }
    
    if ( params[ :app_short_name ] )
      @app = App.find_by_name_short( params[ 'app_short_name' ] )
      @app_versions = AppVersion.find_all_by_app_id( @app, :conditions => { :state => AppVersion::STATE_AVAILABLE } )

      @app_versions.each() { |app_version|
        devices = app_version.devices()

        if ( devices.include?( params[ :device ] ) )
          params[ :app_version ]    = app_version.id
          session[ :device_chosen ] = params[ :device ]

          download_jad()
          return
        end
      }

    end

    render( :action => :device_unsupported_short, :layout => default_page_layout )
  end


  def download_seda_puzzle()
    app = App.find_by_name_short( 'SEDP' )
    app_versions = AppVersion.find_all_by_app_id( app, :conditions => { :app_version_state_id => AppVersionState.find_by_name( 'Available' ) } )

    app_versions.each() { |app_version|
      devices = app_version.devices()

      if ( devices.include?( params[ :device ] ) )
        params[ :app_version ]    = app_version.id
        session[ :device_chosen ] = params[ :device ]

        download_jad()
        return
      end
    }

    render( :action => 'seda_unsupported', :layout => default_page_layout )
  end

  
  def select_model()
    if ( params[ :vendor ] )
      if ( params[ :vendor ][ :chosen ] )
        session[:selected_vendor] = params[ :vendor ][ :chosen ]
        
        if vendor_exists?
          if big_vendor?
            @model_range = get_model_ranges
          else
            @all_models = get_models
          end
        
        #params[ :device_vendor ] = Vendor.first( :conditions => { :id => params[ :vendor ][ :chosen ] } )
        #if ( params[ :device_vendor ] )
        # @message_change = "Seu celular não é um #{params[ :device_vendor ].name}? Troque aqui"
          @page_title = 'Nano Games'
          render :layout => default_page_layout
        else
          redirect_to :action => :vendor_select
        end
      else
        @message_change = 'Trocar fabricante'
        redirect_to :back
      end
    else
      redirect_to :action => :vendor_select
    end
  end
  
  
  def vendor_exists?
    if Device.find_all_by_vendor_id(session[:selected_vendor])
      true
    else
      false
    end
  end
  
  
  def big_vendor?
    Device.find_all_by_vendor_id(session[:selected_vendor]).length > 30
  end


  def get_model_ranges
    models = Device.find_all_by_vendor_id(session[:selected_vendor], :order => :model).map {|device| [device.model, device.id]}
    
    model_groups = arrange_models_by_group(30, models)

    model_range = Array.new
    model_groups.each_index do |group_index|
      model_range[group_index] = Hash.new
      model_range[group_index]["#{model_groups[group_index].first[0]} " + "to".t + " #{model_groups[group_index].last[0]}"] = group_index
    end
    
    return model_range
  end
  
  
  def get_models( range = nil )
    if range
      models = Device.find_all_by_vendor_id(session[:selected_vendor], :order => :model).map {|device| [device.model, device.id]}
      model_groups = arrange_models_by_group(30, models)
    
      return model_groups[range.to_i]
    end
    Device.all( :conditions => { :vendor_id => session[:selected_vendor] }, :order => "model" )
  end
  
  
  def arrange_models_by_group(qtd, models)
    group = Array.new
    0.upto((models.length / qtd).ceil) {|c| group[c] = Array.new}
    0.upto(models.length - 1) {|model| group[model/qtd] << models[model]}
    
    return group
  end


  def device_selected
    if ( params[ :model ] && params[ :model ][ :chosen ] )
      session[ :device_chosen ] = Device.first( :conditions => { :id => params[ :model ][ :chosen ] } )
    else
      session[:device_chosen] = nil
    end

    # mostra a tela de seleção de aplicativos
    select_app( false )
  end


  def select_specific_model
    if request.post? && params[:range][:chosen]
      @models = get_models(params[:range][:chosen])

      render :layout => default_page_layout
      return
    end
    
    redirect_to :action => :vendor_select
  end
  
end
