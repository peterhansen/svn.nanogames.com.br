class TestController < ApplicationController
  
  # GET /undetected_user_agent
  # GET /undetected_user_agents.xml
  def detect
    @device = get_device( true )
    logger.info( "CONEXAO RECEBIDA" )
    headers = request.env.select {|k,v| k.match(".*") }
#    for header in headers
    request.env.each { |k, v|
      logger.info( "request[ #{k} ]: #{v}")
    }

    params.each { |k,v|
      logger.info( "params[ #{k} ]: #{v}")
    }

    render :text => "BLA! #{request.method}"
#    render :layout => default_page_layout
    # se quiser redirecionar, é só trocar a linha anterior por essa:
#    redirect_to "http://www.google.com.br"
  end

	def bla()
		for i in ( 1..90 )
			sleep( 1 )
			puts( "sleeping... #{i}")
		end
		
		render :text => "BLA: #{DateTime.now}"
	end
  
end
