class RankingSystemsController < ApplicationController
  layout :user_layout

  def index
    @ranking_systems = RankingSystem.find(:all)
  end

  def show
    @ranking_system = RankingSystem.find(params[:id])
  end

  def new
    @ranking_system = RankingSystem.new
  end

  def edit
    @ranking_system = RankingSystem.find(params[:id])
  end

  def create
    @ranking_system = RankingSystem.new(params[:ranking_system])

    if @ranking_system.save
      flash[:notice] = 'RankingSystem was successfully created.'
      redirect_to(@ranking_system)
    else
      render :action => "new"
    end
  end

  def update
    @ranking_system = RankingSystem.find(params[:id])

    if @ranking_system.update_attributes(params[:ranking_system])
      flash[:notice] = 'RankingSystem was successfully updated.'
      redirect_to(@ranking_system)
    else
      render :action => "edit"
    end
  end

  def destroy
    @ranking_system = RankingSystem.find(params[:id])
    @ranking_system.destroy

    redirect_to(ranking_systems_url)
  end
end
