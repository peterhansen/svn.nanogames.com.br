class FamiliesController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /families
  # GET /families.xml
  def index
    @families = Family.find( :all,
                             :order => 'name',
                             :page => { :size => 30, :current => params[ :page ] } )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @families }
    end
  end

  # GET /families/1
  # GET /families/1.xml
  def show
    begin
      @family = Family.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid family id #{params[:id]}" )
      flash[:notice] = "Invalid family"
      redirect_to :action => :index
    else
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @family }
      end
    end
    
  end

  # GET /families/new
  # GET /families/new.xml
  def new
    @family = Family.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @family }
    end
  end

  # GET /families/1/edit
  def edit
    @family = Family.find(params[:id])
  end

  # POST /families
  # POST /families.xml
  def create
    @family = Family.new(params[:family])

    respond_to do |format|
      if @family.save
        flash[:notice] = "Family #{ @family.name } was successfully created."
        format.html { redirect_to(@family) }
        format.xml  { render :xml => @family, :status => :created, :location => @family }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @family.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /families/1
  # PUT /families/1.xml
  def update
    @family = Family.find(params[:id])

    respond_to do |format|
      if @family.update_attributes(params[:family])
        flash[:notice] = "Family #{ @family.name } was successfully updated."
        format.html { redirect_to(@family) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @family.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /families/1
  # DELETE /families/1.xml
  def destroy
    @family = Family.find(params[:id])
    @family.destroy

    respond_to do |format|
      flash[:notice] = "Family #{ @family.name } was successfully destroyed."
      format.html { redirect_to(families_url) }
      format.xml  { head :ok }
    end
  end
  
end
