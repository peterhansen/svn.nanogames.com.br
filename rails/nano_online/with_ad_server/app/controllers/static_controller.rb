class StaticController < ApplicationController
  def web_contact
    @contact_errors = []
    
    if request.post?
      @contact_errors << "O nome não pode ficar em branco." if params[:name].nil? || params[:name] == "" 
      @contact_errors << "O e-mail não pode ficar em branco." if params[:email].nil? || params[:email] == ""
      @contact_errors << "O comentário não pode ficar em branco." if params[:comments].nil? || params[:comments] == ""
       
      if @contact_errors.empty? 
        dados = Hash.new
        
        dados["subject"] =      "Contato de #{params[:name]} via web."
        dados["recipients"] =   "contato@nanogames.com.br"
        dados["reply_to"] =     params[:email] # por algum motivo o email não é enviado se o sender estiver configurado para params[:email]
        dados["sender"] =       params[:email]
        dados["message"] =      "Nome: #{params[:name]}\nE-mail: #{params[:email]}\nTelefone: #{params[:ddd]}-#{params[:phone]}\nComentário:\n\n#{params[:comments]}"
        
        Emailer.deliver_send(dados)
      
        flash[:notice] = "Seu comentário foi enviado com sucesso."
        params.clear
      end
    end
  end
  def nano
    render :layout => false
  end
  
  def test
    items_per_page = 10
    
    sort = case params['sort']
               when "name"  then "name"
               when "qty"   then "quantity"
               when "price" then "price"
               when "name_reverse"  then "name DESC"
               when "qty_reverse"   then "quantity DESC"
               when "price_reverse" then "price DESC"
               end
               
    conditions = ["name LIKE ?", "%#{params[:query]}%"] unless params[:query].nil?
    
    @total = AppleDownload.count(:conditions => conditions)
    
    @items_pages = @total / items_per_page
    @items = AppleDownload.find(:all, :order => sort, :conditions => conditions)
    
    if request.xml_http_request?
      render :partial => "items_list", :layout => false
    end
  end
end
