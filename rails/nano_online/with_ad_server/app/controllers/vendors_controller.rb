class VendorsController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /vendor
  # GET /vendors.xml
  def index
    @vendors = Vendor.find( :all,
                            :order => 'name',
                            :page => { :size => 30, :current => params[ :page ] } )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @vendors }
    end
  end
  
  def show
    redirect_to :action => :index
  end

  # GET /vendors/new
  # GET /vendors/new.xml
  def new
    @vendor = Vendor.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @vendor }
    end
  end

  # GET /vendors/1/edit
  def edit
    @vendor = Vendor.find(params[:id])
  end

  # POST /vendors
  # POST /vendors.xml
  def create
    @vendor = Vendor.new(params[:vendor])

    respond_to do |format|
      if @vendor.save
        flash[:notice] = 'Vendor was successfully created.'
        format.html { redirect_to(@vendor) }
        format.xml  { render :xml => @vendor, :status => :created, :location => @vendor }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @vendor.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /vendors/1
  # PUT /vendors/1.xml
  def update
    @vendor = Vendor.find(params[:id])

    respond_to do |format|
      if @vendor.update_attributes(params[:vendor])
        flash[:notice] = 'Vendor was successfully updated.'
        format.html { redirect_to(vendors_url) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @vendor.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /vendors/1
  # DELETE /vendors/1.xml
  def destroy
    @vendor = Vendor.find( params[:id] )
    #obtém a lista de aparelhos que ficariam órfãos ao destruir fabricante
    devices_affected = Device.find_all_by_vendor_id( params[ :id ] )
    
    if ( devices_affected.length > 0 )
      respond_to do |format|
        devices = ''
        for device in devices_affected
          devices = devices + device.model + ', '
        end
        flash[ :notice ] = "Can't delete - the following devices are referencing vendor #{ @vendor.name }: #{ devices }"
        format.html { redirect_to( :back ) }
        format.xml  { head :ok }
      end      
    else
      @vendor.destroy

      respond_to do |format|
        flash[:notice] = "Vendor #{ @vendor.name } was successfully destroyed."
        format.html { redirect_to(vendors_url) }
        format.xml  { head :ok }
      end
    end
  end
  
end
