# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

URL_NANO_ONLINE          = 'http://online.nanogames.com.br'

# tamanho em bytes da assinatura do Nano Online.
NANO_SIGNATURE_LENGTH = 9


NANO_ONLINE_SIGNATURE_BIG = "\211NANO\r\n\032\n"

NANO_ONLINE_SIGNATURE_LITTLE = "\211nano\r\n\032\n"

# caminho dos arquivos privados
PATH_FILES               = "#{RAILS_ROOT}/files"
PATH_SUBMISSIONS         = "#{PATH_FILES}/submissions"
PATH_RELEASES            = "#{PATH_FILES}/releases"
PATH_RESOURCES           = "#{PATH_FILES}/resources"

# Id de jogo/aplicativo: dummy (usado para testes e debug) 
ID_APP_DUMMY             = "DMMY"

# Id de chave global: aplicativo/jogo. Formato dos dados: string
ID_APP                   = -128

# Id de chave global: versão do aplicativo/jogo. Formato dos dados: string
ID_APP_VERSION           = -127

# Id de chave global: idioma atual do aplicativo. Formato dos dados: byte
ID_LANGUAGE              = -126

# Id de chave global: versão do Nano Online utilizada no aplicativo/jogo. Formato dos dados: string
ID_NANO_ONLINE_VERSION   = -125

# Id de chave global: código de retorno. Formato dos dados: short
ID_RETURN_CODE           = -124

# Id de chave global: usuário. Formato dos dados: int
ID_CUSTOMER_ID           = -123

# Id de chave global: mensagem de erro. Formato dos dados: string
ID_ERROR_MESSAGE         = -122

#Id de chave global: início de dados específicos. Formato dos dados: variável
ID_SPECIFIC_DATA         = -121

# Id de chave global: início de dados do news feeder. Formato dos dados: variável
ID_NEWS_FEEDER_DATA      = -120

# Id de chave global: início de dados do ad server. Formato dos dados: variável
ID_AD_SERVER_DATA        = -119

# Id de chave global: horário local do cliente. Formato dos dados: datetime
ID_CLIENT_LOCAL_TIME     = -118

# Id de chave global: lista de jogadores com perfis (possivelmente) expirados.
# Formato dos dados: byte indicando a quantidade de perfis expirados, e um int para cada perfil. (cliente -> servidor)
# Ao retornar informações para o cliente:
# => int indicando id do usuário
# => long indicando a data de expiração.
ID_EXPIRED_PROFILE_IDS   = -117

# Id de chave global: início de dados do gerenciador de recursos. Formato dos dados: variável
ID_RESOURCE_DATA				 = -116

# Id de chave global: Byte extra enviado nas requisições POST feitas via MacOS e iPhoneOS. Sem fazer isso o Rails ignora o último byte do corpo de uma mensagem quando seu valor é 0, causando bugs estranhos
ID_MACOS_RAILS_BUG_FIX   = -100


# assim como as chaves, os valores de retorno globais possuem valores negativos para que não haja confusão com os
# valores utilizados internamente em cada serviço. A chave 0 (zero) é considerada OK (RC_OK)

# Valor de retorno global: sem erros. 
RC_OK                    = 0

# Valor de retorno global: erro interno do servidor. 
RC_SERVER_INTERNAL_ERROR = -1

# Valor de retorno global: aparelho não suportado/detectado. 
RC_DEVICE_NOT_SUPPORTED  = -2

# Valor de retorno global: aplicativo não encontrado (id de aplicativo inválido). 
RC_APP_NOT_FOUND         = -3


# Versão mínima do Nano Online que suporta a atualização automática de versão
NANO_ONLINE_AUTO_UPDATE_MIN_VERSION = '0.0.3'

# Idioma do Nano Online: Inglês (EUA) 
LANGUAGE_EN_US = 0

# Idioma do Nano Online: Espanhol (Espanha) 
LANGUAGE_ES_ES = 1

# Idioma do Nano Online: Português (Brasil) 
LANGUAGE_PT_BR = 2

# Idioma do Nano Online: Alemão (Alemanha) 
LANGUAGE_DE_DE = 3

# Idioma do Nano Online: Português (Portugal) 
LANGUAGE_PT_PT = 4

# Idioma do Nano Online: Italiano (Itália) 
LANGUAGE_IT_IT = 5

# Idioma do Nano Online: Francês (França) 
LANGUAGE_FR_FR = 6

# Idioma do Nano Online: Japonês (Japão) 
LANGUAGE_JP_JP = 7

# Idioma do Nano Online: Russo (Rússia) 
LANGUAGE_RU_RU = 8

# Idioma do Nano Online: Coreano (Coréia) 
LANGUAGE_KO_KR = 9

# Idioma do Nano Online: Holandês (Holanda) 
LANGUAGE_NL_NL = 10

@@default_content_types = {
  'jad'  => 'text/vnd.sun.j2me.app-descriptor',
  'jar'  => 'application/java-archive',
  'cod'  => 'application/vnd.rim.cod',
  'sis'  => 'application/vnd.symbian.install',
  'sisx' => 'x-epoc/x-sisx-app',
}

  ##
  # Seta a idioma do site para a opção preferencial do navegador do usuário
  ##
  def get_language
    request.env['HTTP_ACCEPT_LANGUAGE'] =~ /((\w+)-?(\w+))/
    
    if ( $1 == "pt-br" || $1 == "pt")
      SimplyGlobal.locale = :pt_br
    else
      if ( $1 == "en-us" || $1 == "en" )
        SimplyGlobal.locale = :en
      end
    end
  end

  ##
  # Tenta detectar o aparelho do usuário. Caso seja detectado com sucesso, adiciona
  # a chave :device ao hash session, para que o aparelho de origem esteja acessível
  # para os próximos métodos. Se essa chave já estiver definida nos parâmetros, 
  # a verificação só é feita caso <code>force_check</code> seja true.
  ##
  def get_device( force_check = false )
    if ( force_check || !params[ :device ] )
      # tenta detectar o aparelho do usuário (caso não seja encontrado, já armazena seu user-agent no banco de dados)
      @device, matches = Device.parse_user_agent( request.user_agent )
      logger.info( "\tdevice found: #{ @device == nil ? "NONE -> #{request.user_agent}" : @device.model }" )
      
      params[ :device_vendor ] = matches[ :VENDOR ] if ( matches && matches[ :VENDOR ] )

      # tenta extrair o número de telefone do usuário a partir do HTTP request
      @phone_number = request.env[ 'HTTP_MSISDN' ] ||
                      request.env[ 'MSISDN' ] ||
                      request.env[ 'HTTP_X_MSP_WAP_CLIENT_ID' ] ||
                      request.env[ 'HTTP_X_HTS_CLID' ] ||
                      request.env[ 'HTTP_X_FH_MSISDN' ] ||
                      request.env[ 'HTTP_X_MSISDN' ] ||
                      request.env[ 'HTTP_X_NOKIA_MSISDN' ] ||
                      request.env[ 'HTTP_X_UP_CALLING_LINE_ID' ] ||
                      request.env[ 'HTTP_X_WAP_NETWORK_CLIENT_MSISDN' ] ||
                      request.env[ 'HTTP_X_NETWORK_INFO' ]

      if ( @phone_number )
        session[ :phone_hash ] =    @phone_number.hash
        session[ :phone_number ] =  @phone_number[0..-3].insert(-1, "**")
      end
      
      params[ :device ] = @device if @device
    end
    
    return params[ :device ]
  end  
  
  
  ##
  # Varre uma string (array de bytes) representando uma sequência de dados no formato
  # key (1 byte) value (tamanho variável). Este método cria um bloco, com 2 chaves:
  # |stream, id|. A stream é a referência para o array em si, e o id é um byte
  # identificador da chave. A leitura dos dados é feita utilizando-se os métodos
  # das classes BigEndianTypes ou LittleEndianTypes. Caso o método chamador não queira tratar um
  # determinado par key/value, deve pular os bytes correspondentes no array, para
  # que a leitura da próxima chave não seja inválida, o que acaba por invalidar
  # t.odo o restante da leitura do stream.
  #
  # Caso check_nano_signature seja true, é feito inicialmente o teste de formatação da
  # assinatura. Caso seja inválida, nenhum outro dado é lido e o método retorna.
  #
  # O método encerra quando todos os bytes são lidos e/ou pulados.
  #
  # O atributo value é usado para que o método possa ser chamado de dentro de um Model.
  ##
  def parse_data( data, value = session[ 'nano_client' ] )
    begin
      stream = get_input_stream( data, value )

      # varre o corpo da mensagem, preenchendo a hashtable com os parâmetros encontrados,
      # até que todos os parâmetros sejam lidos (EOF) ou que o id de dados específicos
      # seja encontrado, quando a stream é então passada ao tratador adequado.
      while ( stream.available() > 0 )
        id = stream.read_byte()
        yield stream, id
      end
      
    rescue BigEndianTypes::EOFException => e
    rescue BigEndianTypes::UTFDataFormatException => e
    rescue LittleEndianTypes::EOFException => e
    rescue LittleEndianTypes::UTFDataFormatException => e
      logger.error( "UTFDataFormatException: #{e}" )
    rescue => e
      logger.error( e.message )
    end
  end
  
  
  ##
  # Renderiza um conteúdo binário para uma aplicação Nano. Esse
  # método retorna um bloco com uma referência para o OutputStream. Só é necessário
  # realizar as chamadas de gravação no stream - a abertura e fechamento são feitas
  # dentro deste método.
  ##
  def render_binary( send_news_feeder_data = true, send_ad_server_data = true )
    begin
      out = get_output_stream()

      if ( params[ :app_version ].blank? )
        # se a versão do aplicativo não for informada (ou for incorreta), não há como gravar os recordes
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_APP_NOT_FOUND )
      else
        # retorna atualizações do news feeder, caso haja
        if ( send_news_feeder_data )
          unless ( params[ ID_NEWS_FEEDER_DATA ].blank? )
            NewsFeed.send_data( out, params )
          end
        end
        
        # retorna atualizações do ad server, caso haja
        if ( send_ad_server_data )
          unless ( params[ ID_AD_SERVER_DATA ].blank? )
						AdCampaign.send_data( out, params )
          end
        end
        
				# retorna informações do gerenciador de recursos (utilizados por anúncios, notícias, etc.)
				unless ( params[ ID_RESOURCE_DATA].blank? )
					Resource.send_data( out, params )
				end

        unless ( params[ ID_EXPIRED_PROFILE_IDS ].blank? )
          # envia para o cliente informações sobre a validação dos perfis
          out.write_byte( ID_EXPIRED_PROFILE_IDS )
          
          profiles = params[ ID_EXPIRED_PROFILE_IDS ]
          out.write_byte( profiles.length )
          profiles.each { |customer_id|
            c = Customer.find( customer_id )
            out.write_int( customer_id )
            out.write_datetime( c.validate_before )
          }
        end

        # permite que o método chamador realize as operações de escrita no stream
        yield out
      end
      # retorna as informações gravadas pelo método chamador
      render( :text => out.flush() )
    rescue Exception => e
      logger.info( "erro em render_binary: #{e.message}\n#{e.backtrace.join( "\n" ) }")

      # caso ocorra algum erro, envia mensagem de erro interno do servidor
      out = get_output_stream()

      out.write_byte( ID_RETURN_CODE )
      out.write_short( RC_SERVER_INTERNAL_ERROR )
      
      # retorna as informações gravadas pelo método chamador
      render( :text => out.flush() )
    end
  end
  
  
  ##
  # Verifica a assinatura Nano vinda de uma aplicação, e ainda detecta se é big endian (J2ME) ou little endian (iPhone).
  ##
  def check_nano_signature( data )
    # primeiro verifica se veio de um aparelho Java (big endian)
    big_endian = [ 137, 78, 65, 78, 79, 13, 10, 26, 10 ]

    if ( data.length < big_endian.length )
      session[ 'nano_client' ] = 'undetected'
      return false
    end

    is_big_endian = true

    for i in 0..( big_endian.length - 1 )
      if ( data[ i ] != big_endian[ i ] )
        is_big_endian = false
        break
      end
    end

    if ( is_big_endian )
      session[ 'nano_client' ] = 'big_endian'
      return true
    else
      # se não for big endian, tenta a detecção do little endian
      little_endian = [ 137, 110, 97, 110, 111, 13, 10, 26, 10 ]
      
      for j in 0..( little_endian.length - 1 )
        if ( data[ j ] != little_endian[ j ] )
          # falhou em ambos os testes
          session[ 'nano_client' ] = 'undetected'
          return false
        end
      end
    end

    session[ 'nano_client' ] = 'little_endian'
    return true
  end
  
  
  ##
  #
  ##
  def extract_error_code( message )
    m = message.match( /\(\d+\)/ )
    if ( m && m.size > 0 )
      return m[ m.size - 1 ].match( /\d+/ )[ 0 ].to_i()
    end
    
    return 0
  end


  def default_content_type( extension )
    return @@default_content_types[ extension.delete( '.' ) ]
  end


  ##
  # Obtém um objeto com as informações do usuário da sessão.
  ##
  def session_user()
    if ( session[ :user_id ] )
      return User.find( session[ :user_id ] )
    end

    return nil
  end


  def session_user_apps()
    partner_apps = []
    u = session_user
    if ( u )
      p = session_user.partner
      apps = App.all()

      if ( p )
        apps.each { |a|
          if ( a.partners.include?( p ) )
            partner_apps << a
          end
        }
      else
        partner_apps = apps
      end
    end

    return partner_apps
  end


  def default_page_layout()
    return session[ 'page_layout' ].nil? ? 'simple' : session[ 'page_layout' ]
  end


  ##
  # Retorna um novo outputstream, de acordo com a arquitetura (little endian/big endian) do cliente.
  ##
  def get_output_stream( value = session[ 'nano_client' ] )
    return value == 'big_endian' ? BigEndianTypes::OutputStream.new() : LittleEndianTypes::OutputStream.new()
  end


  ##
  # Retorna um novo inputstream, de acordo com a arquitetura (little endian/big endian) do cliente.
  ##
  def get_input_stream( data, value = session[ 'nano_client' ] )
    return value == 'big_endian' ? BigEndianTypes::InputStream.new( data ) : LittleEndianTypes::InputStream.new( data )
  end


  ##
  # Indica se a conexão está vindo de uma aplicação cliente Nano.
  ##
  def is_nano_app()
    if ( params[ :app ] )
      return true
    end

    return false
  end
  
  

class ApplicationController < ActionController::Base
  include ExceptionNotifiable

  helper :all # include all helpers, all the time

  before_filter :get_device
  before_filter :set_charset
  before_filter :get_language


  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  # TODO protect_from_forgery #:secret => '050e52b0800808bc993ff8cf33dcd657'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  # filter_parameter_logging :password

  
  # TODO http://www.railsdev.ws/blog/10/using-subdomains-in-rails-apps/
#  @current_subdomain = (self.request.subdomains.join('.'))

  
  
  ##############################################################################
  # MÉTODOS PROTEGIDOS                                                         #
  ##############################################################################
  protected

  
  ##############################################################################
  # MÉTODOS PRIVADOS                                                           #
  ##############################################################################
  private

  # tamanho em bytes da assinatura do Nano Online.
  # NANO_SIGNATURE_LENGTH = 9


  ##
  # Retorna o tipo de layout definido para o usuário atual.
  ##
  def user_layout
    user = session_user()
    if ( user )
      return user.layout
    else
      return "customer"
    end

    return nil # TODO retornar nulo mesmo?
  end


  ##
  # Resolve o problema de caracteres acentuados nas views.
  ##
  def set_charset()
    headers["Content-Type"] = "text/html; charset=utf-8"
  end


  ##
  # Filtra o endereço IP do cliente a partir da requisição HTTP.
  ##
  def get_ip_address()
    return request.env[ 'HTTP_X_FORWARDED_FOR' ] || request.env[ 'X_FORWARDED_FOR' ] || request.env[ 'REMOTE_ADDR' ]
  end


  ##
  #
  ##
  def log_access()
    access = Access.new()
    access.user_agent =     request.user_agent
    access.phone_number =   session[ :phone_number ]
    access.phone_hash =     session[ :phone_hash ]
    access.ip_address =     get_ip_address()
    access.http_path =      request.path
    access.http_referrer =  request.referer
#    access.customer =       params[ :customer ] if ( params[ :customer ] )
    access.device =         params[ :device ] if ( params[ :device ] )

    if ( access.save )
      session[ :last_access ] = access
    else
      logger.error( "Error saving access data: #{access.errors}" )
    end
  end


  ##
  #
  ##
  def authorize_admin()
    return authorize( UserRole.find_by_name( 'Administrator' ) )
  end

  
  ##
  #
  ##
  def authorize( role = nil )
    case request.env[ 'HTTP_X_FORWARDED_HOST' ]
      when 'game.reconstrucaoem10dias.com.br'
        # link do jogo Seda Puzzle (J2ME)
        reset_session()
        session[ 'page_layout' ] = 'apps/seda'
        # gera uma URL que varia a cada acesso, impedindo cache por parte do cliente
        redirect_to( "#{url_for( :controller => :downloads, :action => :download_seda_puzzle ) }?timestamp=#{ Time.now.to_i() }" )
      else
        u = session_user()
        unless ( u && ( role.nil? || u.user_role == role ) )
          logger.info( "Acesso não autorizado:" )
          
          request.env.each { |k, v|
            logger.info( "request[ #{k} ]: #{v}")
          }

          params.each { |k,v|
            logger.info( "params[ #{k} ]: #{v}")
          }

        # se a sessão tiver expirado e o usuário voltar no navegador, o redireciona à página de login
        if ( !request.env[ 'HTTP_REFERER' ].blank? && request.env[ 'HTTP_REFERER' ].match( /.*(\/login){1,2}/ ) )
          flash[:notice] = 'Session expired. Please login.'
          redirect_to( :controller => :login, :action => :login )
        else
          # acesso direto a um controle do sistema - redireciona para a página principal da Nano Games
          redirect_to( 'http://www.nanogames.com.br' )
        end
      end
    end
  end


  ##
  # Faz o processo de autorização de acesso vindo de aplicações Nano.
  ##
  def authorize_nano()
    if ( request.post? || request.put? ) 
      if ( check_nano_signature( request.raw_post ) )
        # remove a assinatura dos dados a serem filtrados
        read_binary_params( request.raw_post.slice( NANO_SIGNATURE_LENGTH, request.raw_post.length ) )
        return true
      end
    end
    
    # conexão veio de um aparelho não detectado, através do navegador ou de
    # outro aplicativo - responde em html
    redirect_to( 'http://www.nanogames.com.br' )
    return false
  end


  ##
  # Lê os parâmetros binários recebidos (filtrados de request.raw_post) e os adiciona
  # como chaves no hash params. A leitura é interrompida quando todo o request.body
  # é lido, ou então quando o id ID_SPECIFIC_DATA é encontrado (e seu valor armazenado
  # em params[ ::ID_SPECIFIC_DATA ])
  ##
  def read_binary_params( data )
    logger.info( "read_binary_params( #{data} )" ) if ( RAILS_ENV != 'production' )
	
    parse_data( data ) { |stream, id|
			puts( "LEU ID: #{id}") if ( RAILS_ENV != 'production' )
      case ( id )
        when ID_APP
          params[ id ] = stream.read_string()
          # tenta achar a aplicação no BD
          params[ :app ] = App.find_by_name_short( params[ id ] )
          puts( "LEU APP ID: #{params[ id ] } -> #{params[ :app ]}") if ( RAILS_ENV != 'production' )

          # se já tiver lido a versão do aplicativo, armazena a versão
          if ( params[ ID_APP_VERSION ] )
            params[ :app_version ] = AppVersion.find_by_app_id_and_number( params[ :app ].id.to_i, params[ ID_APP_VERSION ].to_s )
          end
          
        when ID_CUSTOMER_ID
          params[ id ] = stream.read_int()
          params[ :customer ] = Customer.find_by_id( params[ id ] )

        when ID_RETURN_CODE
          params[ id ] = stream.read_short()

        when ID_LANGUAGE
          params[ id ] = Language.find_by_byte_identifier( stream.read_byte() )
          puts( "LEU LANGUAGE: #{params[ id ]}") if ( RAILS_ENV != 'production' )

        when ID_APP_VERSION
          params[ id ] = stream.read_string()
          puts( "LEU APP VERSION: #{params[ id ]}") if ( RAILS_ENV != 'production' )

          # se já tiver lido qual o aplicativo de origem, busca a versão
          if ( params[ :app ] )
            params[ :app_version ] = AppVersion.find_by_app_id_and_number( params[ :app ].id, params[ id ] )
          end

        when ID_NANO_ONLINE_VERSION
          params[ id ] = stream.read_string()
          puts( "LEU NANO ONLINE VERSION: #{params[ id ]}") if ( RAILS_ENV != 'production' )

        when ID_ERROR_MESSAGE
          params[ id ] = stream.read_string()

        when ID_AD_SERVER_DATA
          params[ id ] = AdCampaign.parse_data( stream, params )
          puts( "LEU AD SERVER DATA: #{params[ id ]}") if ( RAILS_ENV != 'production' )
          
        when ID_NEWS_FEEDER_DATA
          params[ id ] = NewsFeed.parse_data( stream )
          puts( "LEU NEWS FEEDER DATA: #{params[ id ]}") if ( RAILS_ENV != 'production' )
          
        when ID_CLIENT_LOCAL_TIME
          params[ id ] = stream.read_datetime()
          puts( "LEU CLIENT LOCAL TIME: #{params[ id ]}") if ( RAILS_ENV != 'production' )

        when ID_EXPIRED_PROFILE_IDS
          total_profiles = stream.read_byte()
          profiles = []
          for i in 1..total_profiles
            profiles << stream.read_int()
          end
          params[ id ] = profiles

        when ID_SPECIFIC_DATA
          # inicia a leitura dos dados específicos
          params[ id ] = stream.read_available()
          puts( "INÍCIO DE SPECIFIC DATA") if ( RAILS_ENV != 'production' )
      end
    }

    puts( "APP VERSION DETECTADO: ->#{params[ :app_version ] }<-" ) if ( RAILS_ENV != 'production' )

    if ( params[ :app_version ] && params[ ID_NANO_ONLINE_VERSION ] >= NANO_ONLINE_AUTO_UPDATE_MIN_VERSION )
#      logger.info( "BUSCANDO ATUALIZAÇÃO PARA #{params[:app_version].app_version_number}" )
      app_version_update = params[ :app_version ].last_update

      if ( app_version_update && ( params[ :last_app_version_update_number ].blank? || 
              Util::natcmp( app_version_update.updated_app_version.number, params[ :last_app_version_update_number ] ) > 0 ) )
        
        logger.info( "ENCONTROU ATUALIZAÇÃO: #{app_version_update.updated_app_version.app_version_number}" )
        params[ :app_version_update ] = app_version_update
#      else
#        logger.info( "VERSÃO É A MAIS ATUAL." )
      end
    end

    # se o usuário for detectado, armazena a informação na tabela de acessos
    if ( params[ :customer ] && session[ :last_access ] )
      session[ :last_access ].customer = params[ :customer ]
      session[ :last_access ].save
    end

    if ( RAILS_ENV != 'production' )
      logger.info( "PARAMETROS BINARIOS LIDOS: ")
      params.each() { |key, value|
        logger.info "#{key} => #{value}"
      }
    end
  end


  ##
  # Filtro para evitar que o navegador faça cache de páginas, o que pode causar problemas no sistema de downloads, por exemplo.
  ##
  def no_cache()
    response.headers["Last-Modified"] = Time.now.httpdate
    response.headers["Expires"] = 0
    # HTTP 1.0
    response.headers["Pragma"] = "no-cache"
    # HTTP 1.1 'pre-check=0, post-check=0' (IE specific)
    response.headers["Cache-Control"] = 'no-store, no-cache, must-revalidate, max-age=0, pre-check=0, post-check=0'
  end


  ##
  # Verifica se a sessão do usuário expirou. Se tiver expirado, o redireciona à página de login. Caso contrário,
  # renova seu tempo por mais 1 hora.
  # TODO pode ser if (session[:expires_at] < Time.now)
  ##
  def session_expiry()
    if ( session[ :expires_at ] )
      time_left = (session[:expires_at] - Time.now).to_i
      if ( time_left <= 0 )
        flash[:notice] = 'Session expired. Please login.'
        redirect_to :controller => 'login', :action => 'login'
        return
      end
    end

    session[:expires_at] = 60.minutes.from_now
  end

end
