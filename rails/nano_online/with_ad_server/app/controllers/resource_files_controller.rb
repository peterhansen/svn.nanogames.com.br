class ResourceFilesController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  def index
    @resource_files = ResourceFile.find(:all)
  end

  def show
    @resource_file = ResourceFile.find(params[:id])
  end

  def new
    @resource_file = ResourceFile.new
  end

  def edit
    @resource_file = ResourceFile.find(params[:id])
  end

  def create
    @resource_file = ResourceFile.new(params[:resource_file])
    @resource_file.resource_id = Resource.first.id

    if @resource_file.save
      flash[:notice] = 'ResourceFile was successfully created.'
      redirect_to(@resource_file)
    else
      render :action => "new"
    end
  end

  def update
    @resource_file = ResourceFile.find(params[:id])

    if @resource_file.update_attributes(params[:resource_file])
      flash[:notice] = 'ResourceFile was successfully updated.'
      format.html { redirect_to(@resource_file)
    else
      format.html { render :action => "edit"
    end
  end

  def destroy
    @resource_file = ResourceFile.find(params[:id])
    @resource_file.destroy

    format.html { redirect_to(resource_files_url)
  end
end
