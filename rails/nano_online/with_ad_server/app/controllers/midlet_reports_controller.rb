class MidletReportsController < ApplicationController

  before_filter :authorize, :only => :index
  # before_filter :authorize_admin, :except => [ :index, :midlet_notify, :install, :delete ]
  before_filter :session_expiry, :except => [ :midlet_notify, :install, :delete ]
  before_filter :log_access, :only => [ :midlet_notify, :install, :delete ]

  layout :user_layout, :except => [ :midlet_notify, :install, :delete ]

  def index
  
    @midlet_reports = MidletReport.all( :page => {  :size => 40,
                                                    :order => 'created_at',
                                                    :current => params[ :page ] },
                                        :conditions => { :app_id => session_user_apps } )
  end

  def destroy
    @midlet_report = MidletReport.find(params[:id])
    @midlet_report.destroy

    flash[:notice] = "MidletReport #{ @midlet_report.id } was successfully destroyed."
    redirect_to(midlet_reports_url)
  end


  def midlet_notify
    # a especificação diz que a requisição é POST (fazer o teste evita acessos indevidos)
    if ( request.post? )
        download = Download.first( :conditions => { :id => params[ :id ] } )
        # se o aplicativo for integrado por terceiros, a notificação informa apenas o aplicativo, em vez do download
        app = download.nil? ? App.find_by_name_short( params[ :id ] ) : download.app_version.app
        if download.nil?
          app = App.find_by_name_short(params[:id][0..3])
          if params[:id][5..-1]
            app_version = AppVersion.find(:first, :conditions => ["app_id = ? AND number = ?", app.id, params[:id][5..-1].gsub(/_/, '.')])
          end
        end

        if ( download || app )
          @midlet_report = MidletReport.new( {
              # os 3 primeiros caracteres indicam o código da resposta
              :status_code => request.raw_post[ 0..2 ].to_i(),
              # o download pode ser nulo
              :download_id => download.nil? ? nil : download.id,
              # não precisa testar o id da aplicação, pois ela deverá ser sempre válida
              :app_id => app.id,
              :app_version_id => app_version.nil? ? nil : app_version.id,
              :access_id => session[ :last_access ].id
            } )

          unless @midlet_report.save()
            logger.error( "error saving midlet report: #{@midlet_report.errors}" )
          end
        end
    end

    render :nothing => true, :status => 200
  end


  def install
    return midlet_notify()
  end


  def delete
    return midlet_notify()
  end
end
