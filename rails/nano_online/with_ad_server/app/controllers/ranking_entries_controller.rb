class RankingEntriesController < ApplicationController
  
  before_filter :authorize, :except => [ :submit, :high_scores, :update_high_scores ]
  before_filter :log_access, :except => [ :high_scores, :update_high_scores ]
  before_filter :authorize_nano, :only => [ :submit ]
  before_filter :session_expiry, :except => [ :submit, :high_scores, :update_high_scores ]

  layout :user_layout, :except => [ :submit, :high_scores ]

  def index
    @page_title = 'Ranking entries'

    if ( session_user.is_admin? )
      @ranking_entries = RankingEntry.find( :all,
                                            :include => [:ranking_type, {:app_version => :app}, :customer, {:device => :vendor}],
                                            :order => 'app_version_id, score' ) # TODO ordenar pelo nome da aplicação
    else
      user_app_versions = []
      session_user_apps.each do |a|
        user_app_versions << a.app_versions
      end
      user_app_versions.flatten!

      @ranking_entries = RankingEntry.find( :all,
                                            :include => [:ranking_type, {:app_version => :app}, :customer, {:device => :vendor}],
                                            :order => 'app_version_id, score', # TODO ordenar pelo nome da aplicação
                                            :conditions => { :app_version_id => user_app_versions } )
    end
  end

  def new
    @page_title = 'Ranking entries'
    @app_versions = AppVersion.all
    @customers = Customer.all
    @devices = Device.all(:order => 'vendor_id, model')
    @ranking_types = RankingType.all
    
    @ranking_entry = RankingEntry.new
  end

  def edit
    @page_title = 'Ranking entries'
    @app_versions = AppVersion.all
    @customers = Customer.all
    @devices = Device.all(:order => 'vendor_id, model')
    @ranking_types = RankingType.all

    @ranking_entry = RankingEntry.find(params[:id])
  end

  def create
    @page_title = 'Ranking entries'
    
    @ranking_entry = RankingEntry.new(params[:ranking_entry])

    if @ranking_entry.save
      flash[:notice] = "Ranking entry #{ @ranking_entry.id } was successfully created."
      redirect_to ranking_entries_url
    else
      @app_versions = AppVersion.all
      @customers = Customer.all
      @devices = Device.all(:order => 'vendor_id, model')
      @ranking_types = RankingType.all

      render :action => "new"
    end
  end

  def update
    @page_title = 'Ranking entries'

    @ranking_entry = RankingEntry.find(params[:id])

      if @ranking_entry.update_attributes(params[:ranking_entry])  
        flash[:notice] = "Ranking entry #{ @ranking_entry.id } was successfully updated."
        redirect_to ranking_entries_url
      else
        @app_versions = AppVersion.all
        @customers = Customer.all
        @devices = Device.all(:order => 'vendor_id, model')
        @ranking_types = RankingType.all
        render :action => "edit"
      end
  end

  def destroy
    @page_title = 'Ranking entries'

    @ranking_entry = RankingEntry.find(params[:id])
    @ranking_entry.destroy

    flash[:notice] = "Ranking entry #{ @ranking_entry.id } was successfully destroyed."
    redirect_to(ranking_entries_url)
  end

  def submit()
    render_binary() { |out|
      # se o aparelho não tiver sido armazenado, o obtém da sessão (se tiver sido detectado) para que a entrada de ranking o armazene
      params[ :device ] ||= session[ :last_access ].device
      submitted, profiles_indexes, entries_with_error = RankingEntry.submit_records( params, session[ 'nano_client' ] )

      if ( params[ ID_NANO_ONLINE_VERSION ] < '0.0.3' )
        # submeteu os recordes com sucesso - retorna tabela de ranking atualizada
        out.write_byte( ID_RETURN_CODE )
        out.write_short( RC_OK )
        out.write_byte( ID_SPECIFIC_DATA )

        if ( submitted )
          entries, players_positions = RankingEntry.best_scores( params, RankingEntry::RANKING_ENTRY_DEFAULT_LIMIT, RankingEntry::RANKING_ENTRY_DEFAULT_OFFSET, profiles_indexes )

          if ( entries )
            # indica a quantidade de entradas lidas (pode ser menor que o máximo pedido)
            out.write_byte( entries.length )
            entries.each { |e|
              # escreve as informações de cada entrada
              out.write_int( e.customer_id )
              out.write_long( e.score )
              out.write_string( e.customer.nickname )
            }
          else
            # não encontrou entradas de ranking
            out.write_byte( 0 )
          end
        else
          # não encontrou entradas de ranking
          out.write_byte( 0 )
        end
      else
        # versões de clientes >= 0.0.3
        if ( submitted )
          # submeteu os recordes com sucesso - retorna tabela de ranking atualizada
          out.write_byte( ID_RETURN_CODE )
          out.write_short( RC_OK )
          out.write_byte( ID_SPECIFIC_DATA )

          entries, customer_entries = RankingEntry.best_scores( params, params[ RankingEntry::PARAM_LIMIT ], params[ RankingEntry::PARAM_OFFSET ], profiles_indexes )

          if ( entries )
            out.write_byte( RankingEntry::PARAM_N_ENTRIES )
            out.write_short( entries.length )
            # indica a quantidade de entradas lidas (pode ser menor que o máximo pedido)
            entries.each { |e|
              # escreve as informações de cada entrada
              out.write_int( e.customer_id )
              out.write_long( e.score )
              out.write_string( e.customer.nickname )
            }
          end

          # TODO corrigir erro no iPhone - customer_id não encontrado ao retornar lista de jogadores extras
          unless ( customer_entries.blank? )
            out.write_byte( RankingEntry::PARAM_N_OTHER_PROFILES )
            out.write_short( customer_entries.length )
            customer_entries.each { |e|
              begin
              # escreve as informações de cada entrada
              out.write_int( e.id )
              out.write_long( e.score )
              out.write_string( e.nickname )
              out.write_int( e.ranking )
              rescue Exception => e
                logger.info( "EXCEPTION: #{e}" )
              end
            }
         end

         out.write_byte( RankingEntry::PARAM_END )
        else
          # TODO não conseguiu submeter as entradas de ranking
          # não encontrou entradas de ranking
          out.write_byte( 0 )
        end
      end
    }
  end

  def high_scores()
    unless ( params[:last_app] )
      params[:last_app] = "BRAZ"
    else
      if ( params[:last_app] == "\\'BRAZ\\'")
        params[:last_app] = "SEDP"
      else
        if ( params[:last_app] == "\\'SEDP\\'" )
          params[:last_app] = "BRAZ"
        end
      end
    end
    
    params[ :app ] ||= params[:last_app]
    @app = params[ :app ] = App.find_by_name_short( params[ :app ] )
    # @app = App.find_by_name_short( params[ :app ] )
    params[ :sub_type ] ||= 0 unless params[ :sub_type ]

    # os métodos "to_i" garantem que os dados estarão corretos antes de fazer o select (evita injeção de SQL)
    @limit = params[ :limit ].blank? ? 10 : params[ :limit ].to_i
    @limit = 10 if @limit <= 0
    @offset = params[ :offset ].blank? ? 0 : params[ :offset ].to_i
    @offset = 0 if @offset < 0
    @apps = App.find(:all, :conditions => "name_short in (\"BRAZ\", \"SEDP\", \"LUXG\")", :order => "name_short")

    @scores, @customer_scores = RankingEntry.best_scores( params, @limit, @offset )
    
  end
  
  def update_high_scores
    params[ :app ] = params[:application]
    @scores, @customer_scores = high_scores()
  end
end
