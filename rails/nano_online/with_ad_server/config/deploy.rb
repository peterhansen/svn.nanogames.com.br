require 'mongrel_cluster/recipes'

set :user, 'dimas'
set :domain, 'online.nanogames.com.br'
set :application, 'nano_online'

set :deploy_to, "/var/www/rails/#{application}" 
set :deploy_via, :export
set :repository, "http://svn.nanogames.com.br/rails/nano_online/"
set :scm_username, "nano_online_svn"
set :scm_password, 'nAnO_SvN123'
set :scm_verbose, true
set :chmod755, "app config db lib public vendor script script/* public/disp*"

set :mongrel_config, "/etc/mongrel_cluster/#{application}.yml"

default_run_options[ :pty ] = true

desc "Production environment"
task :production do
  role :web, "#{domain}"
  role :app, "#{domain}"
  role :db, "#{domain}", :primary => true
end

desc "Staging environment"
task :staging do
  role :web, "74.50.59.218"
  role :app, "74.50.59.218"
  role :db, "74.50.59.218", :primary => true
end

namespace :deploy do
  [:start, :stop, :restart].each do |t|
    desc "#{t.to_s.capitalize} the mongrel app server"
    task t do
      sudo "mongrel_rails cluster::#{t.to_s} -C #{mongrel_config}"
    end
  end
end

desc "Link shared files"
task :before_symlink do
  run "ln -s #{shared_path}/files #{release_path}/files"
  run "ln -s #{shared_path}/images #{release_path}/images"
end

