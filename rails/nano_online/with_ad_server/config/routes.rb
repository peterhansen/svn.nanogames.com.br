ActionController::Routing::Routes.draw do |map|
  map.resources :ranking_systems

  map.resources :resource_file_types

  # map.connect ':controller/:action/:id'
  map.connect 'customers/login', :controller => 'customers', :action => 'login', :conditions => { :method => :post }
  map.connect 'customer_login/login', :controller => 'customer_login', :action => 'login'
  map.connect 'validate/:hash', :controller => 'customer_login', :action => 'validate'
  map.connect 'contact', :controller => 'static', :action => 'web_contact'
  map.connect 'users/change_password', :controller => 'users', :action => 'change_password'
  map.connect 'users/recover_password', :controller => 'users', :action => 'recover_password'
  map.connect 'users/user_info', :controller => 'users', :action => 'user_info'

  map.resources :devices, :vendors, :families, :midp_versions, :cldc_versions, :apps, 
                :app_categories, :app_versions, :bands, :data_services, :device_bugs,
                :optional_apis, :operators, :integrators, :submissions, :customers,
                :ranking_entries, :download_codes, :user_roles, :partners, :users,
                :midlet_reports, :news_feed_categories, :news_feeds, :ranking_types,
                :unit_formats, :ranking_systems,

                # ad-server
                :ad_campaigns, :ad_pieces, :ad_piece_versions, :advertisers, :resources,
                :resource_files, :resource_types, :ad_channel_versions, :ad_channels, :ad_channel_statuses

#  map.resources :customers #, :except => :show
#  map.resources :customers, :member => { :login => [:get, :post] }

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.


  map.connect 'download/:id', :controller => 'downloads', :action => 'download_file_by_id'
  map.connect 'downloads/', :controller => 'downloads', :action => 'select_app'
  map.connect 'test/', :controller => 'test', :action => 'detect'
  map.connect 'high_scores/', :controller => 'ranking_entries', :action => 'high_scores'

  #endereços acessados pelos aparelhos após efetuar instalação ou exclusão de um aplicativo
  map.connect 'notify/i/:id', :controller => 'midlet_reports', :action => 'install'
  map.connect 'notify/d/:id', :controller => 'midlet_reports', :action => 'delete'
  map.connect 'notify/install/:id', :controller => 'midlet_reports', :action => 'install'
  map.connect 'notify/delete/:id', :controller => 'midlet_reports', :action => 'delete'

  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'

  # no caso de qualquer página inválida, envia para a página principal da Nano Games
  map.connect "*anything",
              :controller => 'login',
              :action => 'redirect_to_nano_games'
end
