class AddCustomerIdToDownloadCode < ActiveRecord::Migration
  def self.up
    add_column :download_codes, :customer_id, :integer
  end

  def self.down
    remove_column :download_codes, :customer_id
  end
end
