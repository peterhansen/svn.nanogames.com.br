class AddAppleDataToApp < ActiveRecord::Migration
  def self.up
    add_column :apps, :apple_title, :string
    add_column :apps, :apple_vendor_identifier, :string
    add_column :apps, :apple_identifier, :string
  end

  def self.down
    remove_column :apps, :apple_identifier
    remove_column :apps, :apple_vendor_identifier
    remove_column :apps, :apple_title
  end
end
