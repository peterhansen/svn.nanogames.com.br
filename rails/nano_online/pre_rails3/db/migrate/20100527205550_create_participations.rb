class CreateParticipations < ActiveRecord::Migration
  def self.up
    create_table :participations do |t|
      t.integer :customer_id, :null => false
      t.integer :match_id, :null => false
      t.integer :order
      t.integer :pontuation, :limit => 9
      t.string  :player_data

      t.timestamps
    end
  end

  def self.down
    drop_table :participations
  end
end
