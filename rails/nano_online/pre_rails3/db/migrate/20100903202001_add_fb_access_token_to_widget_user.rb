class AddFbAccessTokenToWidgetUser < ActiveRecord::Migration
  def self.up
    rename_column :widget_users, :facebook_key, :fb_code
    add_column :widget_users, :fb_access_token, :string
  end

  def self.down
    remove_column :widget_users, :fb_access_token
    rename_column :widget_users, :fb_code, :facebook_key
  end
end
