class CreateDeviceBugs < ActiveRecord::Migration
  def self.up
    create_table :device_bugs do |t|
      t.column :name,         :text, :limit => 40, :null => false
      t.column :description,  :text, :limit => 100, :null => false
      t.column :workaround,   :text, :limit => 100
      
      t.timestamps
    end
    
    create_table :devices_device_bugs, :id => false do |t|
      t.column :device_id, :integer, :null => false
      t.column :device_bug_id, :integer, :null => false
    end
    
    add_index :devices_device_bugs, [ :device_id, :device_bug_id ]
    add_index :devices_device_bugs, :device_bug_id    
  end

  def self.down
    begin remove_index :devices_device_bugs, :device_bug_id rescue true end
    begin remove_index :devices_device_bugs, [ :device_id, :device_bug_id ] rescue true end

    begin remove_foreign_key :devices_device_bugs, :devices_device_bugs_ibfk_2 rescue true end

    begin drop_table :devices_device_bugs rescue true end
    begin drop_table :device_bugs rescue true end
  end
end
