class CreateResourceTypes < ActiveRecord::Migration
  def self.up
    create_table :resource_types do |t|
      t.string  :title, :null => false
      t.text    :description, :null => false
      t.integer :client_id, :limit => 2, :unique => true, :references => nil

      t.timestamps
    end
  end

  def self.down
    begin drop_table :resource_types rescue true end
  end
end
