class AddRankingSystemIdToRankingType < ActiveRecord::Migration
  def self.up
    add_column :ranking_types, :ranking_system_id, :integer
  end

  def self.down
    remove_column :ranking_types, :ranking_system_id
  end
end
