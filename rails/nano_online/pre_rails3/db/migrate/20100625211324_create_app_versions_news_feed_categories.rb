class CreateAppVersionsNewsFeedCategories < ActiveRecord::Migration
  def self.up
    create_table :app_versions_news_feed_categories, :id => false do |t|
      t.integer :app_version_id, :null => false
      t.integer :news_feed_category_id, :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :app_versions_news_feed_categories
  end
end
