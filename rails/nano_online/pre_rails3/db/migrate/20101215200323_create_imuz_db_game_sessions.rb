class CreateImuzDbGameSessions < ActiveRecord::Migration
  def self.up
    create_table :imuz_db_game_sessions do |t|
      t.integer :level, :default => 1
      t.integer :hits, :default => 0
      t.integer :misses, :default => 0
      t.integer :skips, :default => 0
      t.integer :max_questions, :default => 10
      t.boolean :started, :default => false

      t.timestamps
    end
  end

  def self.down
    drop_table :imuz_db_game_sessions
  end
end
