class AddAnswerIdToImuzDbQuestion < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_questions, :imuz_db_answer_id, :integer, :after => :text
  end

  def self.down
    remove_column :imuz_db_questions, :imuz_db_answer_id
  end
end
