class CreateFacebookFeeds < ActiveRecord::Migration

  # Nomes dos índices criados
  APPID_FEEDTYPE_UNIQUE_INDEX_NAME = "appid_feedype_pair";
  
  # Documentação:
  # http://wiki.developers.facebook.com/index.php/Attachment_(Streams)
  # http://wiki.developers.facebook.com/index.php/Action_Links

  def self.up

    create_table :facebook_feeds do |t|

      # id da aplicação que pode utilizar este feed
      t.integer :app_id,                    :null => false;

      # Tipo de feed
      t.integer :feed_type,                 :null => false, :limit => 2;

      # Dados de attachment
      t.string  :title,                     :null => false;
      t.text    :title_href,                :null => true,  :limit => 1024;
      t.string  :caption,                   :null => false;
      t.text    :description,               :null => true;

      # Só pode possuir os valores "image", "flash" ou "mp3"
      t.string  :media_type,                :null => false, :limit => 5,    :default => "image";

      t.text    :media_src,                 :null => false, :limit => 1024;
      t.text    :media_href,                :null => false, :limit => 1024;

      # Dados de action link
      t.string  :action_link_text,          :null => true,  :limit => 25;
      t.text    :action_link_href,          :null => true,  :limit => 1024;

      # Formatadores dos textos :caption e :description
      t.text    :caption_format_code,       :null => true;
      t.text    :description_format_code,   :null => true;

      # Descreve qual o objetivo do feed indicado por
      t.string  :achievement_description,   :null => false, :limit => 512;

      # Gera as colunas created_at e updated_at
      t.timestamps;
    end
    
    add_index( :facebook_feeds, [:app_id, :feed_type], { :unique => true, :name => APPID_FEEDTYPE_UNIQUE_INDEX_NAME } );

  end

  def self.down
    begin remove_index( :facebook_feeds, APPID_FEEDTYPE_UNIQUE_INDEX_NAME ) rescue true end

    drop_table :facebook_feeds;
  end
end
