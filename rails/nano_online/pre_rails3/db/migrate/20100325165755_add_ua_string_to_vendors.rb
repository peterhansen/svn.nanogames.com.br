class AddUaStringToVendors < ActiveRecord::Migration
  def self.up
    add_column :vendors, :ua_string, :string, :limit => 200
  end

  def self.down
    remove_column :vendors, :ua_string
  end
end
