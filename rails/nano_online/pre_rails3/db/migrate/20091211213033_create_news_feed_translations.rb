class CreateNewsFeedTranslations < ActiveRecord::Migration
  def self.up
    create_table :languages do |t|
      t.string :name, :null => false

      t.timestamps
    end

    create_table :news_feed_translations do |t|
      t.integer :news_feed_id,      :null => false
      t.text    :content,           :limit => 4096, :null => false
      t.text    :title,             :limit => 256,  :null => false
      t.text    :url,               :limit => 256
      t.integer :language_id,       :null => false

      t.timestamps
    end
  end

  def self.down
#    remove_foreign_key(:news_feed_translations, :news_feed_translations_ibfk_2)
    
#    drop_table :news_feed_translations
#    drop_table :languages
    
  end
end
