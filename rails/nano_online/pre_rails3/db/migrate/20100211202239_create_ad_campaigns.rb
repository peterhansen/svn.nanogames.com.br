class CreateAdCampaigns < ActiveRecord::Migration
  def self.up
    create_table :ad_campaigns do |t|
      t.integer   :advertiser_id, :null => false
			t.string    :title, :null => false
      t.datetime  :expires_at

      t.timestamps
    end

    # tabelas de relacionamento NxN
    create_table :ad_campaigns_apps, :id => false do |t|
      t.integer   :ad_campaign_id, :null => false
      t.integer   :app_id, :null => false
    end
#    add_index :ads_apps, [ :ad_campaign_id, :app_id ]
#    add_index :ads_apps, :app_id
  end

  def self.down
    # TODO remove_index
    begin drop_table :ad_campaigns_apps rescue true end
    begin drop_table :ad_campaigns rescue true end
  end
end
