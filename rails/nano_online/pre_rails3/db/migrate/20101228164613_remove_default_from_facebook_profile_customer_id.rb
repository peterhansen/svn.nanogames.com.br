class RemoveDefaultFromFacebookProfileCustomerId < ActiveRecord::Migration
  def self.up
    change_column :facebook_profiles, :customer_id, :integer, :null => true
  end

  def self.down
    change_column :facebook_profiles, :customer_id, :integer, :null => false
  end
end
