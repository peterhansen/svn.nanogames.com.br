class ChangeMidletReportAccessIdDefaultValue < ActiveRecord::Migration
  def self.up
    change_column :midlet_reports, :access_id, :integer, :null => true
  end

  def self.down
    change_column :midlet_reports, :access_id, :integer, :null => false
  end
end
