class AddRecordTimeToRankingEntry < ActiveRecord::Migration
  def self.up
    add_column :ranking_entries, :record_time, :datetime
  end

  def self.down
    remove_column :ranking_entries, :record_time
  end
end