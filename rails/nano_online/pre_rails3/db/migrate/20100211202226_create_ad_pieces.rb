class CreateAdPieces < ActiveRecord::Migration
  def self.up
    create_table :ad_pieces do |t|
      t.string  :title, :null => false
      t.text    :description
      t.integer :ad_channel_id, :null => false

      t.timestamps
    end
  end

  def self.down
    begin drop_table :ad_pieces rescue true end
  end
end
