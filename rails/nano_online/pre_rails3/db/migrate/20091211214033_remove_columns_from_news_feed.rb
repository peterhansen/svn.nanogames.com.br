class RemoveColumnsFromNewsFeed < ActiveRecord::Migration
  def self.up
    remove_column :news_feeds, :title
    remove_column :news_feeds, :content
    remove_column :news_feeds, :url
  end

  def self.down
    add_column :news_feeds, :title,   :text,  :limit => 256,  :null => false    
    add_column :news_feeds, :content, :text,  :limit => 4096, :null => false   
    add_column :news_feeds, :url,     :text,  :limit => 256                    
  end
end
