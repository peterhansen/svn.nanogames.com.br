class CreateAnswersQuestions < ActiveRecord::Migration
  def self.up
    create_table :answers_questions, :id => false, :force => true do |t|
      t.integer :imuz_db_answer_id
      t.integer :imuz_db_question_id
      t.timestamp
    end
  end

  def self.down
    drop_table :answers_questions
  end
end
