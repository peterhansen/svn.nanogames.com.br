class CreateRankingSystems < ActiveRecord::Migration
  def self.up
    create_table :ranking_systems do |t|
      t.string :name

      t.timestamps
    end
  end

  def self.down
    drop_table :ranking_systems
  end
end
