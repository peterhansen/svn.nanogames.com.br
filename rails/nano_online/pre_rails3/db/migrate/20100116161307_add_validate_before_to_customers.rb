class AddValidateBeforeToCustomers < ActiveRecord::Migration
  def self.up
    add_column :customers, :validate_before, :datetime
  end

  def self.down
    remove_column :customers, :validate_before
  end
end
