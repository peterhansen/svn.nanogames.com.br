class CreateFacebookProfiles < ActiveRecord::Migration

  # Nomes dos índices criados
  UID_UNIQUE_INDEX_NAME = "uid_unique_key";
  CUSTOMER_ID_UNIQUE_INDEX_NAME = "customer_id_unique_key";

  def self.up
    create_table :facebook_profiles do |t|

      #	The user ID of the user being queried
      t.integer   :uid, :null => false, :limit => 8

      # ID do usuário Nano Online
      t.integer   :customer_id, :null => false                    

      #	The first name of the user being queried
      t.string  	:first_name

      #	The last name of the user being queried
      t.string  	:last_name

      #	The full name of the user being queried
      t.string  	:name

      #	The URL to the small-sized profile picture for the user being queried. The image can have a maximum width of 50px and a maximum height of 150px. This URL may be blank
      t.string  	:pic_small

      #	The URL to the largest-sized profile picture for the user being queried. The image can have a maximum width of 200px and a maximum height of 600px. This URL may be blank
      t.string  	:pic_big

      #	The URL to the square profile picture for the user being queried. The image can have a maximum width and height of 50px. This URL may be blank
      t.string  	:pic_square

      #	The URL to the medium-sized profile picture for the user being queried. The image can have a maximum width of 100px and a maximum height of 300px. This URL may be blank
      t.string    :pic

      #	The networks to which the user being queried belongs. Starting February 7, 2010, the status field within this field will only return results in English. For more information, see Roadmap User(FQL)
      # t.array   :affiliations

      #	The time the profile of the user being queried was most recently updated. If the user's profile has not been updated in the past three days, this value will be 0
      t.datetime  :profile_update_time

      #	The time zone where the user being queried is located
      t.string    :timezone

      #	The religion of the user being queried
      t.string    :religion

      #	The birthday of the user being queried. The format of this date varies based on the user's locale
      t.string    :birthday

      #	The birthday of the user being queried, rendered as a machine-readable string. The format of this date never changes
      t.string    :birthday_date

      #	The sex of the user being queried. Starting February 7, 2010, this field will only return results in English. For more information, see Roadmap User(FQL)
      t.string  	:sex

      #	The home town (and state) of the user being queried
      # t.array  	:hometown_location

      #	A list of the genders the person the user being queried wants to meet
      # t.array  	:meeting_sex

      #	A list of the reasons the user being queried wants to meet someone
      # t.array  	:meeting_for

      #	The type of relationship for the user being queried. Starting February 7, 2010, this field will only return results in English. For more information, see Roadmap User(FQL)
      t.string  	:relationship_status

      #	The user ID of the partner (for example, husband, wife, boyfriend, girlfriend) of the user being queried
      t.integer   :significant_other_id, :limit => 8, :references => nil

      #	The political views of the user being queried
      t.string   	:political

      #	The current location of the user being queried
      # t.array   :current_location

      #	The activities of the user being queried
      t.string   	:activities

      #	The interests of the user being queried
      t.string   	:interests

      #	Indicates whether the user being queried has logged in to the current application
      t.boolean   :is_app_user

      #	The favorite music of the user being queried
      t.string   	:music

      #	The favorite television shows of the user being queried
      t.string   	:tv

      #	The favorite movies of the user being queried
      t.string   	:movies

      #	The favorite books of the user being queried
      t.string   	:books

      #	The favorite quotes of the user being queried
      t.string   	:quotes

      #	More information about the user being queried
      t.string   	:about_me

      #	Information about high school of the user being queried
      # t.array  	:hs_info

      #	Post-high school information for the user being queried
      # t.array  	:education_history

      #	The work history of the user being queried
      # t.array  	:work_history

      #	The number of notes from the user being queried
      t.integer   :notes_count, :limit => 8

      #	The number of wall posts for the user being queried
      t.integer   :wall_count, :limit => 8

      #	The current status of the user being queried
      t.string  :status

      #	[Deprecated] This value is now equivalent to is_app_user
      # t.bool  :has_added_app

      #	The user's Facebook Chat status. Returns a string, one of active, idle, offline, or error (when Facebook can't determine presence information on the server side). The query does not return the user's Facebook Chat status when that information is restricted for privacy reasons
      t.string  :online_presence

      #	The two-letter language code and the two-letter country code representing the user's locale. Country codes are taken from the ISO 3166 alpha 2 code list
      t.string  :locale

      #	The proxied wrapper for a user's email address
      t.string  :proxied_email

      #	The URL to a user's profile. If the user specified a username for his or her profile, profile_url contains the username
      t.string  :profile_url

      #	An array containing a set of confirmed email hashes for the user. Emails are registered via the connect.registerUsers API call and are only confirmed when the user adds your application. The format of each email hash is the crc32 and md5 hashes of the email address combined with an underscore (_).
      # t.array :email_hashes

      #	The URL to the small-sized profile picture for the user being queried. The image can have a maximum width of 50px and a maximum height of 150px, and is overlaid with the Facebook favicon. This URL may be blank.
      t.string 	:pic_small_with_logo

      #	The URL to the largest-sized profile picture for the user being queried. The image can have a maximum width of 200px and a maximum height of 600px, and is overlaid with the Facebook favicon. This URL may be blank.
      t.string 	:pic_big_with_logo

      #	The URL to the square profile picture for the user being queried. The image can have a maximum width and height of 50px, and is overlaid with the Facebook favicon. This URL may be blank.
      t.string 	:pic_square_with_logo

      #	The URL to the medium-sized profile picture for the user being queried. The image can have a maximum width of 100px and a maximum height of 300px, and is overlaid with the Facebook favicon. This URL may be blank.
      t.string 	:pic_with_logo

      #	A comma delimited list of Demographic Restrictions types a user is allowed to access. Currently, alcohol is the only type that can get returned
      t.string 	:allowed_restrictions

      #	Indicates whether or not Facebook has verified the user
      t.string 	:verified

      #	This string contains the contents of the text box under a user's profile pictures
      t.string 	:profile_blurb

      #	Note: For family information, you should query the family FQL table instead.  This array contains a series of entries for the immediate relatives of the user being queried. Each entry is also an array containing the following fields:  relationship -- A string describing the type of relationship. Can be one of parent, mother, father, sibling, sister, brother, child, son, daughter.  uid (optional) -- The user ID of the relative, which gets displayed if the account is linked to (confirmed by) another account. If the relative did not confirm the relationship, the name appears instead.  name (optional) -- The name of the relative, which could be text the user entered. If the relative confirmed the relationship, the uid appears instead.  birthday -- If the relative is a child, this is the birthday the user entered.  Note: At this time, you cannot query for a specific relationship (like SELECT family FROM user WHERE family.relationship = 'daughter' AND uid = '$x'); you'll have to query on the family field and filter the results yourself
      # t.array :family

      #	The username of the user being queried
      t.string 	:username

      #	The website of the user being queried
      t.string 	:website

      #	Returns true if the user is blocked to the viewer/logged in user
      t.boolean :is_blocked

      # Gera as colunas created_at e updated_at
      t.timestamps
    end

    # Só permitimos associar um customer a cada perfil de Facebook
    add_index( :facebook_profiles, :uid, { :name => UID_UNIQUE_INDEX_NAME, :unique => true } );
    add_index( :facebook_profiles, :customer_id, { :name => CUSTOMER_ID_UNIQUE_INDEX_NAME, :unique => true } );

  end

  def self.down
    begin
      remove_index( :facebook_profiles, CUSTOMER_ID_UNIQUE_INDEX_NAME );
      remove_index( :facebook_profiles, UID_UNIQUE_INDEX_NAME );
    rescue
      true  
    end

    drop_table :facebook_profiles
  end
end
