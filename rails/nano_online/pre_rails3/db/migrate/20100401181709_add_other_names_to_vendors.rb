class AddOtherNamesToVendors < ActiveRecord::Migration
  def self.up
    add_column :vendors, :other_names, :string
  end

  def self.down
    remove_column :vendors, :other_names
  end
end
