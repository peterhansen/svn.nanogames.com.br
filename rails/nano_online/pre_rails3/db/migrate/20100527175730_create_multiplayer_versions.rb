class CreateMultiplayerVersions < ActiveRecord::Migration
  def self.up
    create_table :multiplayer_versions do |t|
      t.integer :app_version_id

      t.timestamps
    end
  end

  def self.down
    drop_table :multiplayer_versions
  end
end
