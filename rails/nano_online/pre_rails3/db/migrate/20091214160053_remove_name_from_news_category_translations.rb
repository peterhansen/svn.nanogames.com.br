class RemoveNameFromNewsCategoryTranslations < ActiveRecord::Migration
  def self.up
    remove_column :news_category_translations, :name
  end

  def self.down
    add_column :news_category_translations, :name, :string, :null => false
  end
end
