class AddIndexes < ActiveRecord::Migration
  def self.up
    execute <<-SQL
        CREATE INDEX app_version_states_name
        ON app_version_states(name)
    SQL
    execute <<-SQL
        CREATE INDEX app_version_states_code
        ON app_version_states(code)
    SQL
    execute <<-SQL
        CREATE INDEX currencies_code
        ON currencies(code)
    SQL
    execute <<-SQL
        CREATE INDEX currencies_name
        ON currencies(name)
    SQL
    execute <<-SQL
        CREATE INDEX apps_apple_title
        ON apps(apple_title)
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`apps`
        ADD INDEX `apps_name`(`name`(30));
    SQL
    execute <<-SQL
        CREATE INDEX apps_name_short
        ON apps(name_short(30))
    SQL
    execute <<-SQL
        CREATE INDEX apple_product_categories_category
        ON apple_product_categories(category)
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`countries`
        ADD INDEX `countries_iso_name_2`(`iso_name_2`(5));
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`user_roles` ADD INDEX `user_roles_name`(`name`(30));
    SQL

#    execute <<-SQL
#        ALTER TABLE `nano_development`.`app_versions` ADD INDEX `app_versions_app_id_number`(`app_id`, `number`(255));
#    SQL

    execute <<-SQL
        CREATE INDEX users_nickname
        ON users(nickname)
    SQL
#    execute <<-SQL
#        ALTER TABLE `nano_development`.`users` ADD INDEX `users_email`(`email`(255));
#    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`users`
        ADD INDEX `users_email_nickname`(`email`(255), `nickname`(255));
    SQL
    execute <<-SQL
        CREATE INDEX customers_validation_hash
        ON customers(validation_hash)
    SQL
#    execute <<-SQL
#        ALTER TABLE `nano_development`.`customers` ADD INDEX `customers_email`(`email`(255));
#    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`customers` ADD INDEX `customers_nickname`(`nickname`(40));
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`vendors` ADD INDEX `vendors_name`(`name`(40));
    SQL
#    execute <<-SQL
#        ALTER TABLE `nano_development`.`devices` ADD INDEX `devices_vendor_id_model`(`vendor_id`, `model`(40));
#    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`devices` ADD INDEX `devices_user_agent`(`user_agent`(255));
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`undetected_user_agents` ADD INDEX `undetected_user_agents_user_agent`(`user_agent`(255));
    SQL
    execute <<-SQL
        CREATE INDEX facebook_profiles_uid
        ON facebook_profiles(uid)
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`news_category_translations` ADD INDEX `news_category_translations_title`(`title`(40));
    SQL
    execute <<-SQL
        CREATE INDEX languages_name
        ON languages(name)
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`apple_download_reports` ADD INDEX `apple_download_reports_name`(`name`(40));
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`midp_versions` ADD INDEX `midp_versions_version`(`version`(40));
    SQL
    execute <<-SQL
        ALTER TABLE `nano_development`.`cldc_versions` ADD INDEX `cldc_versions_version`(`version`(40));
    SQL
  end

  def self.down
    execute <<-SQL
        DROP INDEX app_version_states_name
        ON app_version_states
    SQL
    execute <<-SQL
        DROP INDEX app_version_states_code
        ON app_version_states
    SQL
    execute <<-SQL
        DROP INDEX currencies_code
        ON currencies
    SQL
    execute <<-SQL
        DROP INDEX currencies_name
        ON currencies
    SQL
    execute <<-SQL
        DROP INDEX apps_apple_title
        ON apps
    SQL
    execute <<-SQL
        DROP INDEX apps_name
        ON apps
    SQL
    execute <<-SQL
        DROP INDEX apps_name_short
        ON apps
    SQL
    execute <<-SQL
        DROP INDEX apple_product_categories_category
        ON apple_product_categories
    SQL
    execute <<-SQL
        DROP INDEX countries_iso_name_2
        ON countries
    SQL
    execute <<-SQL
        DROP INDEX user_roles_name
        ON user_roles
    SQL

#    execute <<-SQL
#        DROP INDEX app_versions_app_id_number
#        ON app_versions
#    SQL 
    
    execute <<-SQL
        DROP INDEX users_email_nickname
        ON users
    SQL
    execute <<-SQL
        DROP INDEX customers_validation_hash
        ON customers
    SQL

    execute <<-SQL
        DROP INDEX customers_nickname
        ON customers
    SQL
    execute <<-SQL
        DROP INDEX vendors_name
        ON vendors
    SQL
#    execute <<-SQL
#        DROP INDEX devices_vendor_id_model
#        ON devices
#    SQL
    execute <<-SQL
        DROP INDEX devices_user_agent
        ON devices
    SQL
    execute <<-SQL
        DROP INDEX undetected_user_agents_user_agent
        ON undetected_user_agents
    SQL
    execute <<-SQL
        DROP INDEX facebook_profiles_uid
        ON facebook_profiles
    SQL
    execute <<-SQL
        DROP INDEX news_category_translations_title
        ON news_category_translations
    SQL
    execute <<-SQL
        DROP INDEX languages_name
        ON languages
    SQL
    execute <<-SQL
        DROP INDEX apple_download_reports_name
        ON apple_download_reports
    SQL
    execute <<-SQL
        DROP INDEX midp_versions_version
        ON midp_versions
    SQL
    execute <<-SQL
        DROP INDEX cldc_versions_version
        ON cldc_versions
    SQL
  end
end
