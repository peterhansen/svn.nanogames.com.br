class CreateResourceFiles < ActiveRecord::Migration
  def self.up
    create_table :resource_files do |t|
      t.integer :resource_id, :null => false
      t.integer :resource_file_type_id, :null => false
			t.integer :internal_id, :null => true, :default => 0, :references => nil, :limit => 3

      t.timestamps
    end
    # TODO add_index
  end

  def self.down
    begin drop_table :resource_files rescue true end
  end
end
