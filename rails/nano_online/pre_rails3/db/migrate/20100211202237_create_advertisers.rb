class CreateAdvertisers < ActiveRecord::Migration
  def self.up
    create_table :advertisers do |t|
      t.integer   :partner_id, :null => false

      t.timestamps
    end
  end

  def self.down
    begin drop_table :advertisers rescue true end
  end
end
