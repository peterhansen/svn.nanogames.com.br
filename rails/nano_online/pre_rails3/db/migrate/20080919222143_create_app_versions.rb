class CreateAppVersions < ActiveRecord::Migration

  def self.up
    create_table :app_versions do |t|
      t.integer :app_id, :null => false
      t.text :release_notes, :limit => 100
      t.text :number, :limit => 10, :null => false
      t.integer :state, :limit => 4
      
      t.timestamps
    end

    create_table :app_files do |t|
      t.column :app_version_id,  :integer, :null => false
      t.column :filename, :text

      t.timestamps
    end
    
  
    #cria as tabelas de relacionamento NxN
    create_table :app_versions_families, :id => false do |t|
      t.column :app_version_id, :integer, :null => false
      t.column :family_id, :integer, :null => false
    end     
    add_index :app_versions_families, [ :app_version_id, :family_id ]
    add_index :app_versions_families, :family_id           
    
  end

  def self.down
    begin remove_index :app_versions_families, [ :app_version_id, :family_id ] rescue true end
    begin remove_index :app_versions_families, :family_id rescue true end

    begin remove_foreign_key :app_files, :app_files_ibfk_1 rescue true end
    begin remove_foreign_key :app_versions_families, :app_versions_families_ibfk_1 rescue true end

    begin drop_table :app_versions_families rescue true end
    begin drop_table :app_files rescue true end
    begin drop_table :app_versions rescue true end
  end
end
