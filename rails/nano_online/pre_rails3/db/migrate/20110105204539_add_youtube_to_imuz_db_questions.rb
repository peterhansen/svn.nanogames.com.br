class AddYoutubeToImuzDbQuestions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_questions, :youtube, :string
  end

  def self.down
    remove_column :imuz_db_questions, :youtube
  end
end
