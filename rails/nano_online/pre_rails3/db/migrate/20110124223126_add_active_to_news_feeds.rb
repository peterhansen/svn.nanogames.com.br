class AddActiveToNewsFeeds < ActiveRecord::Migration
  def self.up
    add_column :news_feeds, :active, :boolean, :default => true
  end

  def self.down
    remove_column :news_feeds, :active
  end
end
