class AddValidatedAtToCustomer < ActiveRecord::Migration
  def self.up
    # armazena o momento em que o usuário confirmou a criação do usuário por e-mail (caso seja nulo, ainda não foi confirmado)
    add_column :customers, :validated_at, :datetime
  end

  def self.down
    remove_column :customers, :validated_at
  end
end
