class AddCustomerIdToImuzDbGameSessions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_game_sessions, :customer_id, :integer
  end

  def self.down
    remove_column :imuz_db_game_sessions, :customer_id
  end
end
