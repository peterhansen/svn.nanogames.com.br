class AddInfoToImuzDbQuestions < ActiveRecord::Migration
  def self.up
    add_column :imuz_db_questions, :artist_name, :string
    add_column :imuz_db_questions, :artist_url, :string
    add_column :imuz_db_questions, :artist_photo, :string
    add_column :imuz_db_questions, :album_name, :string
    add_column :imuz_db_questions, :album_url, :string
  end

  def self.down
    remove_column :imuz_db_questions, :album_url
    remove_column :imuz_db_questions, :album_name
    remove_column :imuz_db_questions, :artist_photo
    remove_column :imuz_db_questions, :artist_url
    remove_column :imuz_db_questions, :artist_name
  end
end
