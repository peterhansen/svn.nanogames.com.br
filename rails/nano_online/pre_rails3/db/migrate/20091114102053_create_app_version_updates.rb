class CreateAppVersionUpdates < ActiveRecord::Migration
  def self.up
    create_table :app_version_updates do |t|
      t.integer :app_version_id, :references => :app_versions
      t.integer :updated_app_version_id, :references => :app_versions
      t.boolean :required, :null => false, :default => true
    end

    add_index :app_version_updates, [ :app_version_id, :updated_app_version_id ], :name => 'index_app_version_updated_app_version'
    add_index :app_version_updates, :app_version_id
  end


  def self.down
#    remove_index :app_version_updates, :name => 'index_app_version_updated_app_version'
#    remove_index :app_version_updates, :app_version_id

    begin drop_table :app_version_updates rescue true end
  end
end
