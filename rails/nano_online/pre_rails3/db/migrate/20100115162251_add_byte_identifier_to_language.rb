class AddByteIdentifierToLanguage < ActiveRecord::Migration
  def self.up
    add_column :languages, :byte_identifier, :integer
  end

  def self.down
    remove_column :languages, :byte_identifier
  end
end
