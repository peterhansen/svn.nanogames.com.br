class CreateAppCustomerData < ActiveRecord::Migration
  def self.up
    create_table :app_customer_datas do |t|
      t.integer :app_version_id, :null => false
      t.integer :customer_id, :null => false
      t.binary :data, :null => false
      t.integer :sub_type, :limit => 2, :default => 0, :null => false
      t.integer :slot, :limit => 2, :default => 0, :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :app_customer_datas
  end
end
