ActiveRecord::Base.connection.execute("SET foreign_key_checks = 0")
ActiveRecord::Base.connection.tables.each do |table|
  ActiveRecord::Base.connection.execute("TRUNCATE #{table}") unless table == 'schema_migrations'
end
ActiveRecord::Base.connection.execute("SET foreign_key_checks = 1")


UserRole.create!        :name => 'administrator', :layout => 'admin'
UserRole.create!        :name => 'imuzdb', :layout => 'imuz_db_admin'
Partner.create!         :name => 'imuz_db'
User.create!            :nickname => 'Nanoca', :first_name => 'Nanoca', :last_name => 'Mairnha', :email => 'nanoca@nano.com', :password => 'senha123', :user_role => UserRole.first
User.create!            :nickname => 'imuzdb', :first_name => 'Imuz', :last_name => 'DB', :email => 'imuzdb@imuzdb.com', :password => 'senha123', :user_role => UserRole.last, :partner => Partner.first
Customer.create!        :first_name => 'Nanoca', :nickname => 'Nanoca', :email => 'nanoca@nano.com', :password => 'senha123'
20.times do |i|
  Customer.create!      :first_name => "user_#{i}" , :nickname => "User_#{i}", :email => "user_#{i}@nano.com", :password => 'senha123'
end
FacebookProfile.create! :uid => 123456, :customer => Customer.first

Vendor.create!          :name => 'Nokia'
Vendor.create!          :name => 'Motorola'
Device.create!          :vendor => Vendor.first, :model => 'Device1'
Device.create!          :vendor => Vendor.last, :model => 'Device2'

RankingSystem.create!   :name => 'highest_score'
UnitFormat.create!      :name => 'integer'
RankingType.create!     :name => 'imuz_db_cumulative', :description => 'Ranking cumulativo do ImuzDb', :unit_format => UnitFormat.first, :sub_type => 0, :ranking_system => RankingSystem.first

App.create!             :name => 'ImuzDb Quiz', :name_short => 'IMDB'
AppVersion.create!      :app => App.first, :number => '1.0'
AppFile.create!         :app_version => AppVersion.first, :filename => File.open("#{RAILS_ROOT}/config/fixture.jad")
Family.create!          :name => 'Familia1'
av = AppVersion.last
av.families << Family.first
av.save

Device.all.each {|device| device.update_attributes( :family => Family.first )}

Language.create!        :name => 'inglês'
news_category = NewsFeedCategory.create! :public => true
news_category.app_versions << AppVersion.all
news_category.news_category_translations.build(:title => 'Main', :language => Language.first, :description => 'Categoria de noticias').save
news_category.save

news_feed = NewsFeed.create!        :news_feed_category => news_category
news_feed.news_feed_translation.build(:content => 'Noticia de Teste', :title => 'Teste', :language => Language.first).save

5.times do
  Customer.all.each do |customer|
    customer.ranking_entries.build :app_version => AppVersion.first, :score => rand(1000), :ranking_type => RankingType.first
    customer.save
  end
end