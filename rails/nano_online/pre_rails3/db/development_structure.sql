CREATE TABLE `accesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_agent` text NOT NULL,
  `phone_number` tinytext,
  `ip_address` text,
  `http_path` text NOT NULL,
  `http_referrer` text,
  `customer_id` int(4) DEFAULT NULL,
  `device_id` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `phone_hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `accesses_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `accesses_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

CREATE TABLE `app_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=511754343 DEFAULT CHARSET=utf8;

CREATE TABLE `app_categories_apps` (
  `app_id` int(4) NOT NULL,
  `app_category_id` int(4) NOT NULL,
  KEY `index_app_categories_apps_on_app_id_and_app_category_id` (`app_id`,`app_category_id`),
  KEY `index_app_categories_apps_on_app_category_id` (`app_category_id`),
  CONSTRAINT `app_categories_apps_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`),
  CONSTRAINT `app_categories_apps_ibfk_2` FOREIGN KEY (`app_category_id`) REFERENCES `app_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_version_id` int(4) NOT NULL,
  `filename` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_version_id` (`app_version_id`),
  CONSTRAINT `app_files_ibfk_1` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1070583951 DEFAULT CHARSET=utf8;

CREATE TABLE `app_version_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `code` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=629115618 DEFAULT CHARSET=utf8;

CREATE TABLE `app_version_updates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_version_id` int(4) DEFAULT NULL,
  `updated_app_version_id` int(4) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `updated_app_version_id` (`updated_app_version_id`),
  KEY `index_app_version_updated_app_version` (`app_version_id`,`updated_app_version_id`),
  KEY `index_app_version_updates_on_app_version_id` (`app_version_id`),
  CONSTRAINT `app_version_updates_ibfk_1` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`),
  CONSTRAINT `app_version_updates_ibfk_2` FOREIGN KEY (`updated_app_version_id`) REFERENCES `app_versions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` int(4) NOT NULL,
  `release_notes` text,
  `number` tinytext NOT NULL,
  `app_version_state_id` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `app_id` (`app_id`),
  CONSTRAINT `app_versions_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1030570627 DEFAULT CHARSET=utf8;

CREATE TABLE `app_versions_customers` (
  `app_version_id` int(4) NOT NULL,
  `customer_id` int(4) NOT NULL,
  KEY `index_app_versions_customers_on_app_version_id_and_customer_id` (`app_version_id`,`customer_id`),
  KEY `index_app_versions_customers_on_customer_id` (`customer_id`),
  CONSTRAINT `app_versions_customers_ibfk_1` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`),
  CONSTRAINT `app_versions_customers_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_versions_families` (
  `app_version_id` int(4) NOT NULL,
  `family_id` int(4) NOT NULL,
  KEY `index_app_versions_families_on_app_version_id_and_family_id` (`app_version_id`,`family_id`),
  KEY `index_app_versions_families_on_family_id` (`family_id`),
  CONSTRAINT `app_versions_families_ibfk_1` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`),
  CONSTRAINT `app_versions_families_ibfk_2` FOREIGN KEY (`family_id`) REFERENCES `families` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `app_versions_submissions` (
  `app_version_id` int(4) NOT NULL,
  `submission_id` int(4) NOT NULL,
  KEY `ver_subm_id` (`app_version_id`,`submission_id`),
  KEY `index_app_versions_submissions_on_submission_id` (`submission_id`),
  CONSTRAINT `app_versions_submissions_ibfk_1` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`),
  CONSTRAINT `app_versions_submissions_ibfk_2` FOREIGN KEY (`submission_id`) REFERENCES `submissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `apple_download_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=782537477 DEFAULT CHARSET=utf8;

CREATE TABLE `apple_downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apple_download_report_id` int(4) NOT NULL,
  `service_provider_code` tinytext,
  `service_provider_country_code` tinytext,
  `upc` tinytext,
  `isrc` tinytext,
  `artist_show` text,
  `app_id` int(4) NOT NULL,
  `label_studio_network` text,
  `apple_product_category_id` int(4) NOT NULL,
  `units` int(4) NOT NULL,
  `royalty_price` decimal(5,2) NOT NULL,
  `begin_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `customer_currency` int(4) NOT NULL,
  `country_id` int(4) NOT NULL,
  `royalty_currency` int(4) NOT NULL,
  `preorder` tinyint(1) DEFAULT '0',
  `season_pass` tinytext,
  `isan` tinytext,
  `customer_price` decimal(5,2) NOT NULL,
  `cma` tinytext,
  `asset_content_flavor` tinytext,
  `vendor_offer_code` text,
  `grid` tinytext,
  `promo_code` tinytext,
  `parent_identifier` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `apple_download_report_id` (`apple_download_report_id`),
  KEY `app_id` (`app_id`),
  KEY `apple_product_category_id` (`apple_product_category_id`),
  KEY `country_id` (`country_id`),
  KEY `fk_customer_currency` (`customer_currency`),
  KEY `fk_royalty_currency` (`royalty_currency`),
  CONSTRAINT `apple_downloads_ibfk_1` FOREIGN KEY (`apple_download_report_id`) REFERENCES `apple_download_reports` (`id`),
  CONSTRAINT `apple_downloads_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`),
  CONSTRAINT `apple_downloads_ibfk_3` FOREIGN KEY (`apple_product_category_id`) REFERENCES `apple_product_categories` (`id`),
  CONSTRAINT `apple_downloads_ibfk_4` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `fk_customer_currency` FOREIGN KEY (`customer_currency`) REFERENCES `currencies` (`id`),
  CONSTRAINT `fk_royalty_currency` FOREIGN KEY (`royalty_currency`) REFERENCES `currencies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=280006467 DEFAULT CHARSET=utf8;

CREATE TABLE `apple_product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=801999926 DEFAULT CHARSET=utf8;

CREATE TABLE `apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `name_short` tinytext NOT NULL,
  `description_short` text,
  `description_medium` text,
  `description_long` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `apple_title` varchar(255) DEFAULT NULL,
  `apple_vendor_identifier` varchar(255) DEFAULT NULL,
  `apple_identifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1063866344 DEFAULT CHARSET=utf8;

CREATE TABLE `apps_partners` (
  `app_id` int(4) NOT NULL,
  `partner_id` int(4) NOT NULL,
  KEY `index_apps_partners_on_partner_id_and_app_id` (`partner_id`,`app_id`),
  KEY `index_apps_partners_on_app_id` (`app_id`),
  CONSTRAINT `apps_partners_ibfk_1` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`),
  CONSTRAINT `apps_partners_ibfk_2` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `frequency` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=329199086 DEFAULT CHARSET=utf8;

CREATE TABLE `bands_devices` (
  `device_id` int(4) NOT NULL,
  `band_id` int(4) NOT NULL,
  KEY `index_bands_devices_on_device_id_and_band_id` (`device_id`,`band_id`),
  KEY `index_bands_devices_on_band_id` (`band_id`),
  CONSTRAINT `bands_devices_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `bands_devices_ibfk_2` FOREIGN KEY (`band_id`) REFERENCES `bands` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `bands_operators` (
  `operator_id` int(4) NOT NULL,
  `band_id` int(4) NOT NULL,
  KEY `operator_id` (`operator_id`),
  KEY `band_id` (`band_id`),
  CONSTRAINT `bands_operators_ibfk_1` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`),
  CONSTRAINT `bands_operators_ibfk_2` FOREIGN KEY (`band_id`) REFERENCES `bands` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cldc_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` tinytext NOT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=420576078 DEFAULT CHARSET=utf8;

CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `iso_name_2` tinytext NOT NULL,
  `iso_name_3` tinytext NOT NULL,
  `iso_number` tinytext NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currency_id` (`currency_id`),
  CONSTRAINT `countries_ibfk_1` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=140579095 DEFAULT CHARSET=utf8;

CREATE TABLE `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=411621537 DEFAULT CHARSET=utf8;

CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` tinytext NOT NULL,
  `first_name` tinytext,
  `last_name` tinytext,
  `email` tinytext,
  `phone_number` tinytext,
  `birthday` date DEFAULT NULL,
  `cpf` tinytext,
  `genre` varchar(1) NOT NULL DEFAULT 'n',
  `hashed_password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `validated_at` datetime DEFAULT NULL,
  `validation_hash` varchar(255) DEFAULT NULL,
  `validate_before` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=934891875 DEFAULT CHARSET=utf8;

CREATE TABLE `customers_devices` (
  `device_id` int(4) NOT NULL,
  `customer_id` int(4) NOT NULL,
  KEY `index_customers_devices_on_device_id_and_customer_id` (`device_id`,`customer_id`),
  KEY `index_customers_devices_on_customer_id` (`customer_id`),
  CONSTRAINT `customers_devices_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `customers_devices_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `data_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `max_speed` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `data_services_devices` (
  `device_id` int(4) NOT NULL,
  `data_service_id` int(4) NOT NULL,
  KEY `index_data_services_devices_on_device_id_and_data_service_id` (`device_id`,`data_service_id`),
  KEY `index_data_services_devices_on_data_service_id` (`data_service_id`),
  CONSTRAINT `data_services_devices_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `data_services_devices_ibfk_2` FOREIGN KEY (`data_service_id`) REFERENCES `data_services` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `device_bugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `description` text NOT NULL,
  `workaround` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `device_integrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(4) NOT NULL,
  `integrator_id` int(4) NOT NULL,
  `identifier` tinytext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_device_integrators_on_device_id_and_integrator_id` (`device_id`,`integrator_id`),
  KEY `index_device_integrators_on_integrator_id` (`integrator_id`),
  CONSTRAINT `device_integrators_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `device_integrators_ibfk_2` FOREIGN KEY (`integrator_id`) REFERENCES `integrators` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=819413329 DEFAULT CHARSET=utf8;

CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `midp_version_id` int(4) DEFAULT NULL,
  `cldc_version_id` int(4) DEFAULT NULL,
  `vendor_id` int(4) NOT NULL,
  `family_id` int(4) DEFAULT NULL,
  `commercial_name` tinytext,
  `model` tinytext NOT NULL,
  `screen_width` smallint(2) DEFAULT NULL,
  `screen_height_partial` smallint(2) DEFAULT NULL,
  `screen_height_full` smallint(2) DEFAULT NULL,
  `jar_size_max` smallint(2) DEFAULT NULL,
  `memory_heap` int(4) DEFAULT NULL,
  `memory_image` int(4) DEFAULT NULL,
  `user_agent` tinytext,
  `pointer_events` tinyint(1) DEFAULT NULL,
  `rms_total_size` int(4) DEFAULT NULL,
  `rms_max_record_size` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_devices_on_vendor_id` (`vendor_id`),
  KEY `index_devices_on_midp_version_id` (`midp_version_id`),
  KEY `index_devices_on_cldc_version_id` (`cldc_version_id`),
  KEY `index_devices_on_family_id` (`family_id`),
  CONSTRAINT `devices_ibfk_1` FOREIGN KEY (`midp_version_id`) REFERENCES `midp_versions` (`id`),
  CONSTRAINT `devices_ibfk_2` FOREIGN KEY (`cldc_version_id`) REFERENCES `cldc_versions` (`id`),
  CONSTRAINT `devices_ibfk_3` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`),
  CONSTRAINT `devices_ibfk_4` FOREIGN KEY (`family_id`) REFERENCES `families` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=822623599 DEFAULT CHARSET=utf8;

CREATE TABLE `devices_device_bugs` (
  `device_id` int(4) NOT NULL,
  `device_bug_id` int(4) NOT NULL,
  KEY `index_devices_device_bugs_on_device_id_and_device_bug_id` (`device_id`,`device_bug_id`),
  KEY `index_devices_device_bugs_on_device_bug_id` (`device_bug_id`),
  CONSTRAINT `devices_device_bugs_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `devices_device_bugs_ibfk_2` FOREIGN KEY (`device_bug_id`) REFERENCES `device_bugs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `devices_optional_apis` (
  `device_id` int(4) NOT NULL,
  `optional_api_id` int(4) NOT NULL,
  KEY `index_devices_optional_apis_on_device_id_and_optional_api_id` (`device_id`,`optional_api_id`),
  KEY `index_devices_optional_apis_on_optional_api_id` (`optional_api_id`),
  CONSTRAINT `devices_optional_apis_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `devices_optional_apis_ibfk_2` FOREIGN KEY (`optional_api_id`) REFERENCES `optional_apis` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `devices_submissions` (
  `device_id` int(4) NOT NULL,
  `submission_id` int(4) NOT NULL,
  KEY `index_devices_submissions_on_device_id_and_submission_id` (`device_id`,`submission_id`),
  KEY `index_devices_submissions_on_submission_id` (`submission_id`),
  CONSTRAINT `devices_submissions_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`),
  CONSTRAINT `devices_submissions_ibfk_2` FOREIGN KEY (`submission_id`) REFERENCES `submissions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `download_codes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` tinytext NOT NULL,
  `file` text NOT NULL,
  `used_at` datetime DEFAULT NULL,
  `download_id` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_download_codes_on_download_id` (`download_id`),
  CONSTRAINT `download_codes_ibfk_1` FOREIGN KEY (`download_id`) REFERENCES `downloads` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_id` int(4) NOT NULL,
  `app_version_id` int(4) NOT NULL,
  `device_id` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `access_id` (`access_id`),
  KEY `app_version_id` (`app_version_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `downloads_ibfk_1` FOREIGN KEY (`access_id`) REFERENCES `accesses` (`id`),
  CONSTRAINT `downloads_ibfk_2` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`),
  CONSTRAINT `downloads_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `families` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `ignored` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `install_jar` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1058333802 DEFAULT CHARSET=utf8;

CREATE TABLE `integrators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=937705407 DEFAULT CHARSET=utf8;

CREATE TABLE `integrators_operators` (
  `integrator_id` int(4) NOT NULL,
  `operator_id` int(4) NOT NULL,
  KEY `index_integrators_operators_on_integrator_id_and_operator_id` (`integrator_id`,`operator_id`),
  KEY `index_integrators_operators_on_operator_id` (`operator_id`),
  CONSTRAINT `integrators_operators_ibfk_1` FOREIGN KEY (`integrator_id`) REFERENCES `integrators` (`id`),
  CONSTRAINT `integrators_operators_ibfk_2` FOREIGN KEY (`operator_id`) REFERENCES `operators` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `byte_identifier` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=996332913 DEFAULT CHARSET=utf8;

CREATE TABLE `midlet_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_code` smallint(2) DEFAULT NULL,
  `access_id` int(4) NOT NULL,
  `download_id` int(4) DEFAULT NULL,
  `app_id` int(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `app_version_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_midlet_reports_on_download_id_and_app_id` (`download_id`,`app_id`),
  KEY `index_midlet_reports_on_app_id` (`app_id`),
  KEY `index_midlet_reports_on_access_id` (`access_id`),
  KEY `app_version_id` (`app_version_id`),
  CONSTRAINT `midlet_reports_ibfk_1` FOREIGN KEY (`access_id`) REFERENCES `accesses` (`id`),
  CONSTRAINT `midlet_reports_ibfk_2` FOREIGN KEY (`download_id`) REFERENCES `downloads` (`id`),
  CONSTRAINT `midlet_reports_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `apps` (`id`),
  CONSTRAINT `midlet_reports_ibfk_4` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `midp_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` tinytext NOT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=429095755 DEFAULT CHARSET=utf8;

CREATE TABLE `news_category_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_feed_category_id` int(4) NOT NULL,
  `language_id` int(4) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_feed_category_id` (`news_feed_category_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `news_category_translations_ibfk_1` FOREIGN KEY (`news_feed_category_id`) REFERENCES `news_feed_categories` (`id`),
  CONSTRAINT `news_category_translations_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_feed_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `news_feed_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_feed_id` int(4) NOT NULL,
  `content` text NOT NULL,
  `title` text NOT NULL,
  `url` text,
  `language_id` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_feed_id` (`news_feed_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `news_feed_translations_ibfk_1` FOREIGN KEY (`news_feed_id`) REFERENCES `news_feeds` (`id`),
  CONSTRAINT `news_feed_translations_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `news_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_feed_category_id` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_news_feeds_on_news_feed_category_id` (`news_feed_category_id`),
  CONSTRAINT `news_feeds_ibfk_1` FOREIGN KEY (`news_feed_category_id`) REFERENCES `news_feed_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `operators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=995385790 DEFAULT CHARSET=utf8;

CREATE TABLE `optional_apis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `description` text,
  `jsr_index` smallint(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_optional_apis_on_jsr_index` (`jsr_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=943015804 DEFAULT CHARSET=utf8;

CREATE TABLE `ranking_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(4) NOT NULL,
  `app_version_id` int(4) NOT NULL,
  `device_id` int(4) DEFAULT NULL,
  `score` bigint(9) NOT NULL,
  `sub_type` smallint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `record_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `by_customer_id_appver_id_and_dev_id` (`customer_id`,`app_version_id`,`device_id`),
  KEY `index_ranking_entries_on_customer_id_and_app_version_id` (`customer_id`,`app_version_id`),
  KEY `index_ranking_entries_on_app_version_id` (`app_version_id`),
  KEY `index_ranking_entries_on_device_id` (`device_id`),
  KEY `index_ranking_entries_on_score` (`score`),
  KEY `index_ranking_entries_on_sub_type` (`sub_type`),
  CONSTRAINT `ranking_entries_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `ranking_entries_ibfk_2` FOREIGN KEY (`app_version_id`) REFERENCES `app_versions` (`id`),
  CONSTRAINT `ranking_entries_ibfk_3` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1056155095 DEFAULT CHARSET=utf8;

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrator_id` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_submissions_on_integrator_id` (`integrator_id`),
  CONSTRAINT `submissions_ibfk_1` FOREIGN KEY (`integrator_id`) REFERENCES `integrators` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

CREATE TABLE `undetected_user_agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_agent` text NOT NULL,
  `accesses` smallint(3) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext,
  `layout` tinytext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=872648692 DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) DEFAULT NULL,
  `hashed_password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `partner_id` int(4) DEFAULT NULL,
  `user_role_id` int(4) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `email` tinytext NOT NULL,
  `first_name` tinytext,
  `last_name` tinytext,
  PRIMARY KEY (`id`),
  KEY `partner_id` (`partner_id`),
  KEY `user_role_id` (`user_role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`user_role_id`) REFERENCES `user_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

CREATE TABLE `vendors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` tinytext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1036030853 DEFAULT CHARSET=utf8;

INSERT INTO schema_migrations (version) VALUES ('20080918145220');

INSERT INTO schema_migrations (version) VALUES ('20080919220641');

INSERT INTO schema_migrations (version) VALUES ('20080919221606');

INSERT INTO schema_migrations (version) VALUES ('20080919222116');

INSERT INTO schema_migrations (version) VALUES ('20080919222128');

INSERT INTO schema_migrations (version) VALUES ('20080919222143');

INSERT INTO schema_migrations (version) VALUES ('20080919224712');

INSERT INTO schema_migrations (version) VALUES ('20080919224724');

INSERT INTO schema_migrations (version) VALUES ('20080920150729');

INSERT INTO schema_migrations (version) VALUES ('20080920150828');

INSERT INTO schema_migrations (version) VALUES ('20080923201337');

INSERT INTO schema_migrations (version) VALUES ('20080929022234');

INSERT INTO schema_migrations (version) VALUES ('20080929022245');

INSERT INTO schema_migrations (version) VALUES ('20080929115024');

INSERT INTO schema_migrations (version) VALUES ('20080929123110');

INSERT INTO schema_migrations (version) VALUES ('20081020180834');

INSERT INTO schema_migrations (version) VALUES ('20090325182527');

INSERT INTO schema_migrations (version) VALUES ('20090326195008');

INSERT INTO schema_migrations (version) VALUES ('20090922180517');

INSERT INTO schema_migrations (version) VALUES ('20091020023114');

INSERT INTO schema_migrations (version) VALUES ('20091114102053');

INSERT INTO schema_migrations (version) VALUES ('20091124171156');

INSERT INTO schema_migrations (version) VALUES ('20091204182712');

INSERT INTO schema_migrations (version) VALUES ('20091204194716');

INSERT INTO schema_migrations (version) VALUES ('20091210161136');

INSERT INTO schema_migrations (version) VALUES ('20091211213033');

INSERT INTO schema_migrations (version) VALUES ('20091211214033');

INSERT INTO schema_migrations (version) VALUES ('20091211215103');

INSERT INTO schema_migrations (version) VALUES ('20091211220026');

INSERT INTO schema_migrations (version) VALUES ('20091214155806');

INSERT INTO schema_migrations (version) VALUES ('20091214160053');

INSERT INTO schema_migrations (version) VALUES ('20100104170231');

INSERT INTO schema_migrations (version) VALUES ('20100107160459');

INSERT INTO schema_migrations (version) VALUES ('20100107162351');

INSERT INTO schema_migrations (version) VALUES ('20100107171136');

INSERT INTO schema_migrations (version) VALUES ('20100107171751');

INSERT INTO schema_migrations (version) VALUES ('20100107191120');

INSERT INTO schema_migrations (version) VALUES ('20100107194727');

INSERT INTO schema_migrations (version) VALUES ('20100115162251');

INSERT INTO schema_migrations (version) VALUES ('20100115180447');

INSERT INTO schema_migrations (version) VALUES ('20100115193841');

INSERT INTO schema_migrations (version) VALUES ('20100116161307');