# Be sure to restart your server when you modify this file

# Uncomment below to force Rails into production mode when
# you don't control web/app server and can't set it the proper way
ENV['RAILS_ENV'] ||= 'production'

# ENV['GEM_PATH'] = '/home/rails/.gems/gems/site_ruby:/usr/local/lib/ruby/gems/1.8:/usr/lib/ruby/gems/1.8'
# ENV['GEM_PATH'] = '/users/dimas/.gem/ruby/1.8/gems'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.11' unless defined? RAILS_GEM_VERSION

# http://woss.name/2006/10/25/migrating-your-rails-application-to-unicode/
$KCODE = 'utf8'

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # config.middleware.use "Rack::Bug"
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.
  # See Rails::Configuration for more options.

  # Skip frameworks you're not going to use. To use Rails without a database
  # you must remove the Active Record framework.
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Specify gems that this application depends on.
  # They can then be installed with "rake gems:install" on new installations.
  # config.gem "hpricot", :version => '0.6', :source => "http://code.whytheluckystiff.net"
  config.gem 'nokogiri'
  config.gem 'rest-graph'
  config.gem 'faraday', :version => "0.4.6"
  config.gem 'mysql', :version => '>= 2.7'
  config.gem 'fastercsv'
  config.gem 'ruby-hmac', :lib => 'hmac-sha2', :version => '0.4.0'
  config.gem 'mini_fb'
  config.gem 'json'
  config.gem 'will_paginate'
#  config.gem 'rubyzip'
#  config.gem 'tatyree-cookieless_sessions', :lib => 'cookieless_sessions', :source => 'http://gems.github.com'
  config.gem 'hoptoad_notifier'

  # Only load the plugins named here, in the order given. By default, all plugins
  # in vendor/plugins are loaded in alphabetical order.
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

  # Add additional load paths for your own custom dirs
   config.load_paths += %W( #{RAILS_ROOT}/lib/util )

  # Force all environments to use the same logger level
  # (by default production uses :info, the others :debug)
  # config.log_level = :debug

  # Make Time.zone default to the specified zone, and make Active Record store time values
  # in the database in UTC, and return them converted to the specified local zone.
  # Run "rake -D time" for a list of tasks for finding time zone names. Uncomment to use default local time.
  config.time_zone = 'Brasilia'

  # Your secret key for verifying cookie session data integrity.
  # If you change this key, all old sessions will become invalid!
  # Make sure the secret is at least 30 characters and all random,
  # no regular words or you'll be exposed to dictionary attacks.
  config.action_controller.session = {
    :key  => '_nano_online_session',
    :secret       => 'a245858dbe7cc2a84e480b21a67485b8af12c44eefcb897dd7fb55dd0f6b595ad54c5c506ee83137a55006d0ba9d5a3789ddc1f9b4f86c0dd6b4914fd5bc2ce3'
  }
  # Use the database for sessions instead of the cookie-based default,
  # which shouldn't be used to store highly confidential information
  # (create the session table with "rake db:sessions:create")
#  config.action_controller.session_store = :mem_cache_store

  # Use SQL instead of Active Record's schema dumper when creating the test database.
  # This is necessary if your schema can't be completely dumped by the schema dumper,
  # like if you have constraints or database-specific column types
  # config.active_record.schema_format = :sql

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector

  #require 'will_paginate'
  config.load_paths << "#{RAILS_ROOT}/app/models/imuz_db"
  config.action_controller.allow_forgery_protection = false
end

# configura o plugin de envio automático de e-mails caso o servidor dispare uma exceção
#ExceptionNotifier.exception_recipients = %w(support@nanogames.com.br)
# defaults to exception.notifier@default.com
#ExceptionNotifier.sender_address = %("Nano Online" <error@online.nanogames.com.br>)

# defaults to "[ERROR] "
#if RAILS_ENV == 'production'
#  ExceptionNotifier.email_prefix = "[Nano Online ERROR] "
#else
#  ExceptionNotifier.email_prefix = "[Nano Staging ERROR] "
#end

require "memcache"
CACHE = MemCache.new('127.0.0.1')

# configura o ActionMailer
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.default_charset = 'UTF-8'
ActionMailer::Base.smtp_settings = {
  :address => 'mail.nanogames.com.br',
  :authentication => :login,
  :domain => 'nanogames.com.br',
  :user_name => 'noreply@nanogames.com.br',
  :password => 'J2J3Q6aR',
}
