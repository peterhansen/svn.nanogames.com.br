ActionController::Routing::Routes.draw do |map|

  map.connect 'customers/login', :controller => 'customers', :action => 'login', :conditions => { :method => :post }
  map.connect 'customer_login/login', :controller => 'customer_login', :action => 'login'
  map.connect 'validate/:hash', :controller => 'customer_login', :action => 'validate'
  map.connect 'users/change_password', :controller => 'users', :action => 'change_password'
  map.connect 'users/recover_password', :controller => 'users', :action => 'recover_password'
  map.connect 'users/user_info', :controller => 'users', :action => 'user_info'
  map.connect 'facebook/login', :controller => 'facebook', :action => 'login'

  map.resources :families, :midp_versions, :cldc_versions,
    :app_categories, :app_versions, :bands, :data_services, :device_bugs,
    :optional_apis, :operators, :integrators, :submissions, :customers,
    :ranking_entries, :download_codes, :user_roles, :partners, :users,
    :midlet_reports, :news_feed_categories, :news_feeds, :ranking_types,
    :unit_formats, :ranking_systems, :session_storage,

    # ad-server
    :ad_campaigns, :ad_pieces, :ad_piece_versions, :advertisers, :resources,
    :resource_files, :resource_types, :ad_channel_versions, :ad_channels,
    :ad_channel_statuses, :resource_file_types

  map.resources :vendors, :collection => { :list => :get }
  map.resources :devices, :collection => { :list => :get }
  map.resources :apps,    :collection => { :list => :get }

  map.connect 'boadica/categories', :controller => 'boadica', :action => 'categories'
  map.connect 'boadica/store', :controller => 'boadica', :action => 'store'
  map.connect 'boadica/offers', :controller => 'boadica', :action => 'offers'

  map.connect 'downloads/app', :controller => 'downloads', :action => 'download_app'
  map.connect 'downloads/download_jar_file', :controller => 'downloads', :action => 'download_jar_file'
  map.connect 'download/:id', :controller => 'downloads', :action => 'download_file_by_id'
  map.connect 'downloads/', :controller => 'downloads', :action => 'select_app'
  map.connect 'test/', :controller => 'test', :action => 'detect'
  map.connect 'high_scores/.:format', :controller => 'ranking_entries', :action => 'high_scores'
  map.connect 'user_range_high_score/.:format', :controller => 'ranking_entries', :action => 'user_range_high_score'

  #endereços acessados pelos aparelhos após efetuar instalação ou exclusão de um aplicativo
  map.connect 'notify/i/:id', :controller => 'midlet_reports',
                              :action => 'install',
                              :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/
  map.connect 'notify/d/:id', :controller => 'midlet_reports',
                              :action => 'delete',
                              :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/
  map.connect 'notify/install/:id', :controller => 'midlet_reports',
                                    :action => 'install',
                                    :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/
  map.connect 'notify/delete/:id',  :controller => 'midlet_reports',
                                    :action => 'delete',
                                    :id => /.*(\d{1,2}\.\d{1,2}((\.\d{0,2}){0,1}))?/

  map.namespace :imuz_db do |imuz_db|
    imuz_db.resource :game_sessions
    imuz_db.resource :pages
  end
  
  map.connect "/customers/export_to_csv", :controller => "customers", :action => "export_to_csv"

  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
  map.connect ':controller/:action.:format'

  map.connect 'games/:action', :controller => 'static', :action => :action
  map.connect 'applications/:action', :controller => 'static', :action => :action

  map.connect "/imuz_db/mobile/login", :controller => 'customer_login', :action => 'login'
  map.connect "/imuz_db/mobile/sign_up", :controller => 'customer_login', :action => 'sign_up'
  map.connect "/imuz_db/mobile/home", :controller => 'imuz_db/game_sessions', :action => 'home'
  map.connect "/imuz_db/mobile/first_use", :controller => 'customer_login', :action => 'first_use'
  map.connect "/imuz_db/mobile/fb_session", :controller => 'imuz_db/game_sessions', :action => 'fb_session'
  map.connect "/imuz_db/mobile/fb_login", :controller => 'imuz_db/game_sessions', :action => 'fb_login'
  map.connect "/imuz_db/mobile/installation", :controller => 'customer_login', :action => 'installation'
  map.connect "/imuz_db/mobile", :controller => 'customer_login', :action => 'installation'
  map.connect "/imuz_db/mobile/ajax_login", :controller => 'customer_login', :action => 'ajax_login'
  map.connect "/imuz_db/mobile/new_question_ajax", :controller => 'imuz_db/game_sessions', :action => 'new_question_iphone'
  map.connect "/customer_login/edit", :controller => 'customer_login', :action => 'edit'

  

  map.resources :customer_login, :collection => {:do_login => :post, :sign_up => :get, :first_use => :get}
#  map.imuzdb_do_login "imuz_db/do_login", {:controller => 'customer_login', :action => 'do_login'}


  map.root :controller => 'static', :action => 'home'
  map.about 'about', :controller => 'static', :action => 'about'
  map.down 'down', :controller => 'static', :action => 'download'
  map.news 'news', :controller => 'static', :action => 'news'
  map.partner 'partner', :controller => 'static', :action => 'partners'
  map.contact 'contact', :controller => 'static', :action => 'web_contact'
  map.portfolio 'portfolio', :controller => 'static', :action => 'portfolio_2010'
  map.vixla_virtual 'vila_virtual', :controller => 'static', :action => 'vila_virtual'

#  # no caso de qualquer página inválida, envia para a página principal da Nano Games
#  map.connect "",
#              :controller => 'application',
#              :action => 'check_redirect'

end