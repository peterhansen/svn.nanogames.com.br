class DataService < ActiveRecord::Base
  has_and_belongs_to_many :devices
  
  validates_numericality_of :max_speed
end
