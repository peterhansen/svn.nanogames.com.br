class Emailer < ActionMailer::Base
  def send(dados)
      @subject = dados["subject"]
      @recipients = dados["recipients"]
      @from = dados["sender"]
      @reply_to = dados["reply_to"]
      @sent_on = Time.now
      
   	  @body = dados
  end
     
  def imuzdb_welcome(user, password, host)
    @subject = "Welcome to ImuzDB Quiz"
    @recipients = user.email
    @from         = "ImuzDB <no-reply@imuzdb.com>"
    headers         "Reply-to" => "no-reply@imuzdb.com"
    @content_type = "text/html"
    @full_name = user.first_name
    @username = user.nickname
    @password = password
    @host = host
  end
end
