class App < ActiveRecord::Base
  has_and_belongs_to_many :app_categories
  has_and_belongs_to_many :partners
  has_and_belongs_to_many :ad_campaigns
  has_many :app_versions
  has_many :ranking_entries, :through => :app_versions # TODO necessário?
  has_many :apple_downloads

#  has_many :downloads, :through => :app_versions

  has_one :facebook_data
  has_many :facebook_feeds

  has_and_belongs_to_many :ads

  # TODO adicionar ícones e imagens de preview dos aplicativos (em vários tamanhos, usando image_column)

  validates_presence_of :name, :name_short

  validates_uniqueness_of :name, :case_sensitive => false
  validates_uniqueness_of :name_short, :case_sensitive => false

  validates_length_of :name, :in => 3..50
  validates_length_of :name_short, :is => 4
  validates_length_of :description_short, :maximum => 150, :allow_nil => true
  validates_length_of :description_medium, :maximum => 300, :allow_nil => true
  validates_length_of :description_long, :maximum => 900, :allow_nil => true

  def customers
    Customer.find(:all, 
    :conditions => "ranking_entries.app_version_id in (#{self.app_version_ids})", 
    :include => :ranking_entries)   
  end

  def families
    Family.all(:include => { :app_versions => :app }, :conditions => ["apps.id = ?", self.id])
  end

  def devices
    Device.all(:include => { :family => { :app_versions => :app } }, :conditions => ["apps.id = ?", self.id], :order => 'devices.model')
  end

  def vendors
    Vendor.all(:include => { :devices => { :family => { :app_versions => :app } } }, :conditions => ["apps.id = ?", self.id], :order => 'vendors.name')
  end

  def name_clean()
    return name.gsub( ' ', '_' )
  end

  def find_version_with_support_to(device)
    self.app_versions.each do |version|
      if version.devices.include?(device) && version.state.name == "Available"
        return version
      end
    end
  end
end
