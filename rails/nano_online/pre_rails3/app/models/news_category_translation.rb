class NewsCategoryTranslation < ActiveRecord::Base
  belongs_to :news_feed_category
  belongs_to :language
  
  validates_presence_of :title
  validates_length_of   :title, :maximum => 256
  validates_presence_of :language

  validates_presence_of :description
  validates_length_of   :description, :maximum => 512, :allow_nil => true


  ##
  # Escreve as informações da categoria do News Feeder num OutputStream.
  ##
  def write_data( out )
    out.write_byte( NewsFeed::PARAM_ENTRY_CATEGORY_ID )
    out.write_int( self.news_feed_category_id )

    # escreve estado de visibilidade de uma notícia
    out.write_byte( NewsFeed::PARAM_ENTRY_CATEGORY_VISIBILITY )
    out.write_boolean( self.news_feed_category.public? )

    out.write_byte( NewsFeed::PARAM_ENTRY_CATEGORY_TITLE )
    out.write_string( self.title )

    out.write_byte( NewsFeed::PARAM_ENTRY_CATEGORY_DESCRIPTION )
    out.write_string( self.description )

    # fim dos dados
    out.write_byte( NewsFeed::PARAM_ENTRY_CATEGORY_END )
  end
end