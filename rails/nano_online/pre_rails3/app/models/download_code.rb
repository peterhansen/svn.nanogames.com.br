require 'digest/sha1'

class DownloadCode < ActiveRecord::Base
  belongs_to :download
  belongs_to :access
  has_many :midlet_reports
  belongs_to :customer

  def initialize( params )
    super( params )
    
    self.code = Digest::SHA1.hexdigest( "#{self.object_id}*\#{@\&KaJDS-\*\&l\=kjjd_as927a#{created_at}" )
  end

end
