class AppFile < ActiveRecord::Base

  belongs_to :app_version

  upload_column :filename,
                :root_dir => "#{RAILS_ROOT}/files/releases",
								:permissions => 0775,
                :store_dir => proc{|record| "#{record.app_version.app_version_number_clean}/"}


  ##
  # Método chamado após destruir uma entrada. Apaga o arquivo relacionado no sistema de arquivos.
  ##
  def after_destroy
    # se for último arquivo do diretório, apaga diretório também
    if ( filename && filename.exists? )
      File.delete( filename.path )
    end
  end


end
