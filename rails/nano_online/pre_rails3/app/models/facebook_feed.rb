class FacebookFeed < ActiveRecord::Base
      
  ##############################################################################
  # Códigos de retorno                                                         #
  ##############################################################################

  # Tipo de mídia inválida
  RC_FB_FEED_ERROR_INVALID_MEDIA_TYPE         = 1

  # Este tipo de feed já está definido para este :app_id
  RC_FB_FEED_ERROR_FEED_TYPE_IN_USE           = 2

  # Textos muito longos
  RC_FB_FEED_ERROR_TITLE_HREF_TOO_LONG        = 3
  RC_FB_FEED_ERROR_MEDIA_SRC_TOO_LONG         = 4
  RC_FB_FEED_ERROR_MEDIA_HREF_TOO_LONG        = 5
  RC_FB_FEED_ERROR_ACTION_LINK_TXT_TOO_LONG   = 6
  RC_FB_FEED_ERROR_ACTION_LINK_HREF_TOO_LONG  = 7
  RC_FB_FEED_ERROR_ACHIEVEMENT_DESC_TOO_LONG  = 8

  ##############################################################################
  # Relacionamentos                                                            #
  ##############################################################################

  belongs_to :app;

  ##############################################################################
  # Validações de Colunas                                                      #
  ##############################################################################

  validates_presence_of :app_id, :feed_type, :title, :caption, :media_type, :media_src, :media_href, :achievement_description;
  
  validates_length_of :title_href,
                      :maximum => 1024,
                      :allow_nil => true,
                      :too_long => "The maximum length of title_href is {{count}} chars (#{RC_FB_FEED_ERROR_TITLE_HREF_TOO_LONG})";

  validates_length_of :media_src,
                      :maximum => 1024,
                      :allow_nil => false,
                      :too_long => "The maximum length of media_src is {{count}} chars (#{RC_FB_FEED_ERROR_MEDIA_SRC_TOO_LONG})";

  validates_length_of :media_href,
                      :maximum => 1024,
                      :allow_nil => false,
                      :too_long => "The maximum length of media_href is {{count}} chars (#{RC_FB_FEED_ERROR_MEDIA_HREF_TOO_LONG})";

  validates_length_of :action_link_text,
                      :maximum => 25,
                      :allow_nil => true,
                      :too_long => "The maximum length of action_link_text is {{count}} chars (#{RC_FB_FEED_ERROR_ACTION_LINK_TXT_TOO_LONG})";

  validates_length_of :action_link_href,
                      :maximum => 1024,
                      :allow_nil => true,
                      :too_long => "The maximum length of action_link_href is {{count}} chars (#{RC_FB_FEED_ERROR_ACTION_LINK_HREF_TOO_LONG})";

  validates_length_of :achievement_description,
                      :maximum => 512,
                      :allow_nil => false,
                      :too_long => "The maximum length of achievement_description is {{count}} chars (#{RC_FB_FEED_ERROR_ACHIEVEMENT_DESC_TOO_LONG})";
  
  validates_inclusion_of :media_type,
                         :in => %w( image flash mp3 ),
                         :message => "Accepted values for column media_type are: \"image\", \"flash\" or \"mp3\" (#{RC_FB_FEED_ERROR_INVALID_MEDIA_TYPE})";

  # A combinação de :app_id e :feed_type deve ser única
  validates_uniqueness_of :app_id, :scope => :feed_type,
                          :message => "This feed_type is already related to an app_id (#{FacebookData::RC_FB_DATA_ERROR_APP_ID_IN_USE})"

end
