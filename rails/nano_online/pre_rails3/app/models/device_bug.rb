class DeviceBug < ActiveRecord::Base
  has_and_belongs_to_many :devices
  
  validates_presence_of :name, :description
  
  validates_length_of :name, :in => 5..30
  validates_length_of :description, :in => 10..100
  validates_length_of :workaround, :in => 10..100, :allow_nil => true
end
