class FacebookProfile < ActiveRecord::Base
  belongs_to :customer
  validates_uniqueness_of :uid,         :allow_nil => false
  validates_uniqueness_of :customer_id, :allow_nil => true
end
