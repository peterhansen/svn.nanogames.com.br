class UserRole < ActiveRecord::Base
  has_many :users

  validates_presence_of    :name
  validates_presence_of    :layout
  validates_uniqueness_of  :name, :case_sensitive => false

end
