class DeviceIntegrator < ActiveRecord::Base
  belongs_to :device
  belongs_to :integrator
  
  validates_presence_of :device_id
  validates_presence_of :integrator_id
  validates_length_of :identifier, :maximum => 30, :allow_nil => true
end
