class Match < ActiveRecord::Base
  belongs_to :multiplayer_version
  
  has_many :turns, :dependent => :destroy
  has_one :active_turn, :class_name => 'Turn', :order => 'id desc'
  
  has_many :participations, :dependent => :destroy
  has_many :customers, :through => :participations
  
  def new_turn(number)
    turn = self.turns.build

    turn.number = number
    
    if turn.save
      return turn
    else
      return nil
    end
  end
end
