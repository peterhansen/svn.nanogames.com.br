class AppVersion < ActiveRecord::Base
  require 'find'

  STATE_AVAILABLE   = 0
  STATE_DEPRECATED  = 1
  STATE_UNAVAILABLE = 2

  belongs_to :app
  belongs_to :app_version_state
  belongs_to :multiplayer_version
  has_and_belongs_to_many :families
  has_and_belongs_to_many :submissions
  has_and_belongs_to_many :customers
  has_and_belongs_to_many :ad_channel_versions
  has_and_belongs_to_many :ranking_types

  has_many :ranking_entries
  has_many :downloads
  has_many :midlet_reports
  has_many :app_customer_datas, :dependent => :destroy

  # ao apagar uma versão de aplicativo, suas entradas de AppFile (e consequentemente os arquivos físicos) também são apagados
  has_many :app_files, :dependent => :destroy

  has_many :integrators, :through => :submissions

  # define o relacionamento auto-referencial para obtenção das versões que substituem ou são substituídas por esta versão
  has_many :app_version_updates, :class_name => 'AppVersionUpdate', :foreign_key => 'app_version_id'
  has_many :updated_app_versions, :through => :app_version_updates
  has_many :inverse_app_version_updates, :class_name => 'AppVersionUpdate', :foreign_key => 'updated_app_version_id'
  has_many :previous_app_versions, :through => :inverse_app_version_updates, :source => :app_version

  has_and_belongs_to_many :news_feed_categories

  # TODO garantir que só exista uma única combinação app/número de versão

  validates_presence_of :app_id
  validates_presence_of :number
  validates_length_of :number, :maximum => 10

  # valida o formato da versão (xx.yy ou xx.yy.zz)
  validates_format_of :number, :with => /\A\d{1,2}\.\d{1,2}(\.\d{0,2}){0,1}\z/, :message => "must be in format 'xx.yy' or 'xx.yy.zz'"

  validates_length_of :release_notes, :maximum => 2048, :allow_nil => true


  def validate
    app_versions = AppVersion.all( :conditions => { :app_id => self.app_id, :number => self.number } ) - [ self ]

    unless ( app_versions.blank? )
      errors.add( "#{self.app.name} #{self.number} already exists." )
    end
  end


  ##
  # Verifica se há uma versão mais atual do aplicativo. Retorna uma referência para a versão mais atual, ou uma referência
  # para a própria versão caso não haja versões mais novas disponíveis.
  ##
  def last_version()
    update = last_update()

    if ( update )
      return update.updated_app_version
    else
      return self
    end
  end


  ##
  #
  ##
  def last_update()
    update = nil

		state_available = AppVersionState.find_by_name( 'Available' )

    app_version_updates.each { |u|
      av = u.updated_app_version

      # se houver atualização da atualização...
      uav = av.last_update
      if ( uav )
        av = uav.updated_app_version
      end

      # se for a primeira atualização encontrada ou seja mais recente que a anterior, armazena em update
      if ( ( av && av.app_version_state == state_available ) &&
            ( update.nil? || Util::natcmp( update.updated_app_version.number, av.number ) > 0 ) )
        update = u
      end
    }

    return update
  end


  def devices()
    return Device.all( :conditions => { 'family_id' => families } )
  end


  def file_jad
    app_files.each { |f|
      return f if f.filename.relative_path.split('.').last
    }

    return nil
  end

  def file_bt_cod
    app_files.each { |f|
      if f.filename.basename[0..2] == "bt_"
        return f
      end
    }

    return nil
  end


  def app_version_number()
    if self.app
      self.app.name + ' ' + number
    else
      ""
    end
  end


  def app_version_number_clean
    app_version_number().gsub( ' ', '_' )
  end

  def state
    self.app_version_state
  end

  def state=(state)
    self.app_version_state = state
  end

  def full_name
    if self.app
      "#{self.app.name} - #{self.number}"
    else
      self.number
    end

  end

end
