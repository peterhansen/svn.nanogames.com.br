class Question < ActiveRecord::Base
  set_table_name :imuz_db_questions
end
class ImuzDb::Question < ActiveRecord::Base
  set_table_name :imuz_db_questions
  belongs_to :correct_answer, :class_name => "ImuzDb::Answer", :foreign_key => "imuz_db_answer_id"
  has_one :game_session, :class_name => "ImuzDb::GameSession", :foreign_key => "imuz_db_question_id", :dependent => :nullify
  has_and_belongs_to_many :answers,
                          :class_name => "ImuzDb::Answer",
                          :foreign_key => :imuz_db_question_id,
                          :association_foreign_key => :imuz_db_answer_id
end
