PARAM_REGISTER_NICKNAME          = 0
PARAM_REGISTER_FIRST_NAME        = 1
PARAM_REGISTER_LAST_NAME         = 2
PARAM_REGISTER_PHONE_NUMBER      = 3
PARAM_REGISTER_EMAIL             = 4
PARAM_REGISTER_CPF               = 5
PARAM_REGISTER_PASSWORD          = 6
PARAM_REGISTER_PASSWORD_CONFIRM  = 7

# indica o fim da leitura ou gravação dos dados do usuário
PARAM_REGISTER_INFO_END          = 8

PARAM_REGISTER_GENRE             = 9
PARAM_REGISTER_BIRTHDAY          = 10

PARAM_REGISTER_CUSTOMER_ID       = 11
PARAM_REGISTER_TIMESTAMP         = 12

PARAM_REGISTER_VALIDATE_BEFORE   = 13
PARAM_REGISTER_VALIDATED_AT      = 14

# O Facebook pode retornar datas incompletas (contendo apenas dd/mm ) de acordo
# com a política de privacidade escolhida pelo usuário. Assim, retornamos apenas
# a data parcial e deixamos a aplicação cliente requisitar a informação que falta
PARAM_REGISTER_BIRTHDAY_STR      = 15

# UID do FaceBook. Tipo do dado: inteiro
PARAM_REGISTER_FB_UID            = 25
# Session key para conexão do FaceBook. Tipo do dado: string
PARAM_REGISTER_FB_SESSION_KEY    = 26
# Segredinho para conexão do FaceBook. Tipo do dado: string
PARAM_REGISTER_FB_SESSION_SECRET = 27

PARAM_LOGIN_NICKNAME             = 0
PARAM_LOGIN_PASSWORD             = 1

##############################################################################
# Códigos de retorno do registro de usuário                                  #
##############################################################################
#
# Código de retorno do registro do usuário: erro: apelido já em uso.
RC_ERROR_NICKNAME_IN_USE			= 1

# Código de retorno do registro do usuário: erro: formato do apelido inválido.
RC_ERROR_NICKNAME_FORMAT			= 2

# Código de retorno do registro do usuário: erro: comprimento do apelido inválido.
RC_ERROR_NICKNAME_LENGTH			= 3

# Código de retorno do registro do usuário: erro: formato do e-mail inválido.
RC_ERROR_EMAIL_FORMAT				= 4

# Código de retorno do registro do usuário: erro: comprimento do e-mail inválido.
RC_ERROR_EMAIL_LENGTH				= 5

# Código de retorno do registro do usuário: erro: password muito fraca.
RC_ERROR_PASSWORD_WEAK			= 6

# Código de retorno do registro do usuário: erro: password e confirmação diferentes.
RC_ERROR_PASSWORD_CONFIRMATION	= 7

# Código de retorno do registro do usuário: erro: comprimento do 1º nome inválido.
RC_ERROR_FIRST_NAME_LENGTH		= 8

# Código de retorno do registro do usuário: erro: comprimento do sobrenome inválido.
RC_ERROR_LAST_NAME_LENGTH			= 9

# Código de retorno do registro do usuário: erro: gênero inválido.
RC_ERROR_GENRE                    = 10

# Código de retorno do registro do usuário: erro: comprimento do gênero inválido.
RC_ERROR_GENRE_LENGTH             = 11

# Código de retorno do registro do usuário: erro: formato do número de telefone inválido.
RC_ERROR_PHONE_NUMBER_FORMAT      = 12

# Código de retorno do registro do usuário: erro: comprimento do número de telefone inválido.
RC_ERROR_PHONE_NUMBER_LENGTH      = 13

# Código de retorno do registro do usuário: erro: CPF inválido.
RC_ERROR_CPF_FORMAT      = 14

# Código de retorno do registro do usuário: erro: e-mail já em uso.
RC_ERROR_EMAIL_ALREADY_IN_USE		  = 15


##############################################################################
# Códigos de retorno do login de usuário                                     #
##############################################################################

# Código de retorno do login de usuário: apelido não encontrado.
RC_ERROR_LOGIN_NICKNAME_NOT_FOUND       = 1

# Código de retorno do login de usuário: password incorreta.
RC_ERROR_LOGIN_PASSWORD_INVALID       = 2


class Customer < ActiveRecord::Base

  # tempo que o jogador tem para validar seu e-mail caso tenha criado ou alterado seu perfil a partir de um aplicativo
  VALIDATION_TIME_MOBILE  = 3.days
  # tempo que o jogador tem para validar seu e-mail caso tenha criado ou alterado seu perfil a partir da web
  VALIDATION_TIME_WEB     = 0

  # Tamanhos mínimo e máximo permitidos para o nickname do usuário
  CUSTOMER_NICKNAME_MIN_LEN = 2
  CUSTOMER_NICKNAME_MAX_LEN = 20

  # Tamanhos mínimo e máximo permitidos para o email do usuário
  CUSTOMER_EMAIL_MIN_LEN = 6
  CUSTOMER_EMAIL_MAX_LEN = 100

  # Tamanhos mínimo e máximo permitidos para o telefone do usuário
  CUSTOMER_PHONENUM_MIN_LEN = 8
  CUSTOMER_PHONENUM_MAX_LEN = 20

  # Tamanho máximo permitido para o primeiro nome do usuário
  CUSTOMER_FIRST_NAME_MAX_LEN = 100

  # Tamanho máximo permitido para o último nome do usuário
  CUSTOMER_LAST_NAME_MAX_LEN = 30

  before_save :default_values                                               # tested

  has_and_belongs_to_many :app_versions                                     # tested
  has_and_belongs_to_many :devices                                          # tested
  has_and_belongs_to_many :lobbies
  
  has_many :participations
  has_many :matches, :through => :participations
  
  has_one :active_participation, :class_name => 'Participation', :conditions => 'pontuation is null'
  
  has_many :actions
  has_many :turns, :through => :actions

  has_many :ranking_entries,  :dependent => :destroy                        # tested
  has_many :accesses,         :dependent => :nullify                        # tested

  has_many :downloads,        :dependent => :nullify                        # tested
  has_many :download_codes,   :dependent => :nullify                        # tested

  has_one :facebook_profile,  :dependent => :destroy                        # tested

  has_many :app_customer_datas, :dependent => :destroy

  validates_presence_of   :email                                            # tested

  validates_length_of     :nickname,                                        # tested
                          :within =>  CUSTOMER_NICKNAME_MIN_LEN..CUSTOMER_NICKNAME_MAX_LEN,
                          :message => "must contain between #{CUSTOMER_NICKNAME_MIN_LEN} and #{CUSTOMER_NICKNAME_MAX_LEN} characters (#{RC_ERROR_NICKNAME_LENGTH})",
                          :allow_nil => true

  validates_uniqueness_of :nickname,                                        # tested
                          :message => "already in use (#{RC_ERROR_NICKNAME_IN_USE})",
                          :case_sensitive => false,
                          :allow_nil => true

  validates_format_of     :email,                                           # tested
                          :allow_nil =>   true,
                          :allow_blank => true,
                          :with =>        /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i,
                          :message =>     "the format is invalid (#{RC_ERROR_EMAIL_FORMAT})"

  validates_length_of     :email,                                           # tested
                          :in =>          CUSTOMER_EMAIL_MIN_LEN..CUSTOMER_EMAIL_MAX_LEN,
                          :allow_nil =>   true,
                          :allow_blank => true,
                          :message =>     "must contain between #{CUSTOMER_EMAIL_MIN_LEN} and #{CUSTOMER_EMAIL_MAX_LEN} characters (#{RC_ERROR_EMAIL_LENGTH})"

=begin
  validates_presence_of :password
  
  validates_length_of :password,
                      :in => 6..15,
                      :allow_nil => true,
                      :allow_blank => true,
                      :message => "must contain between 6 and 15 characteres"
  
  validates_format_of :password, 
                      :with => /^.*(?=.*\d)(?=.*[a-z]).*$/,
                      :message => "must contain numbers and characteres"

=end  
  validates_presence_of   :first_name                          

  # o '4?' no início é para detectar números como o recebido no header X_MSP_WAP_CLIENT_ID, que enviou 4552185134918, por exemplo
  #TODO validates_format_of :phone_number, :with => /4?(\+?\d{2}\s?)?((([0-9]{2})|(\([0-9]{2}\)))\s*)?\d{4}(\s|-)?\d{4}/i, :allow_nil => true, :allow_blank => true, :message => "invalid format (#{RC_ERROR_PHONE_NUMBER_FORMAT})"
  validates_length_of     :phone_number,                                    # tested
                          :in =>          CUSTOMER_PHONENUM_MIN_LEN..CUSTOMER_PHONENUM_MAX_LEN,
                          :allow_nil =>   true,
                          :allow_blank => true,
                          :message =>     "must contain between 8 and 20 digits (#{RC_ERROR_PHONE_NUMBER_LENGTH})"


#  validates_format_of :genre,
#                      :with => /(\b(m|f|n)\b){1}/i,
#                      :message => "invalid (#{RC_ERROR_GENRE})"
#  validates_length_of :genre,
#                      :is => 1,
#                      :message => "invalid length (#{RC_ERROR_GENRE_LENGTH})"

  validates_length_of :first_name,                                          # tested
                      :maximum => CUSTOMER_FIRST_NAME_MAX_LEN,
                      :allow_nil => true,
                      :allow_blank => true,
                      :message => "Your full name is too big. Please use less surnames. (#{RC_ERROR_FIRST_NAME_LENGTH})"
  validates_length_of :last_name,                                           # tested
                      :maximum => CUSTOMER_LAST_NAME_MAX_LEN,
                      :allow_nil => true,
                      :allow_blank => true,
                      :message => "(#{RC_ERROR_LAST_NAME_LENGTH})"

  # attr_accessor :password_confirmation
  validates_confirmation_of :password,                                      # tested
                            :on => :create,
                            :message => "(#{RC_ERROR_PASSWORD_CONFIRMATION})"
  # TODO ao atualizar atributos de um cliente, password e confirmação de password só devem ser verificados se não estiverem em branco
  #validates_confirmation_of :password, :on => :update, :if => Proc.new { |customer| }

  before_save :validate_password

  # TODO falta escrever teste
  ##
  # Cria um novo cliente a partir dos parâmetros para criação de um novo cliente,
  # fazendo o parser dos parâmetros binários e retornando a referência para o novo
  # cliente criado (ainda não salvo).
  ##
  def self.find_all_by_app(app)
    Customer.find(:all, 
      :conditions => "ranking_entries.app_version_id in (#{app.app_version_ids})", 
      :include => :ranking_entries)     
  end

  def self.export_to_csv_by_app(app)
    customers = find_all_by_app(app)
    FasterCSV.generate do |csv|
      csv << ['Name', 'Nickname']
      customers.each{|customer| csv << [customer.first_name, customer.nickname]}
    end
  end

  def self.filter_new( params, value = nil )
    customer = Customer.new()
    customer.mobile_client = true
    @send_validation_email = params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31'

    # Integração com Facebook
    fb_uid = -1;
    fb_session_key = "";
    fb_session_secret = "";

    parse_data( params[ ::ID_SPECIFIC_DATA ], value ) { |stream, id|
      logger.info("\n\n\nID DE CUSTOMER LIDO: #{id}\n\n\n")
      case id
        when PARAM_REGISTER_EMAIL
          customer.email = stream.read_string()
        when PARAM_REGISTER_NICKNAME
          customer.nickname = stream.read_string()
        when PARAM_REGISTER_PASSWORD
          customer.password = stream.read_string()
          customer.password_confirmation = customer.password
        when PARAM_REGISTER_PASSWORD_CONFIRM
          customer.password_confirmation = stream.read_string()
        when PARAM_REGISTER_CPF
          customer.cpf = stream.read_string()
        when PARAM_REGISTER_FIRST_NAME
          customer.first_name = stream.read_string()
        when PARAM_REGISTER_LAST_NAME
          customer.last_name = stream.read_string()
        when PARAM_REGISTER_PHONE_NUMBER
          customer.phone_number = stream.read_string()
        when PARAM_REGISTER_GENRE
          customer.genre = stream.read_char()
        when PARAM_REGISTER_BIRTHDAY
          customer.birthday = stream.read_datetime()
        when PARAM_REGISTER_FB_UID
          fb_uid = stream.read_long();
        when PARAM_REGISTER_FB_SESSION_KEY
          fb_session_key = stream.read_string();
        when PARAM_REGISTER_FB_SESSION_SECRET
          fb_session_secret = stream.read_string();
        when PARAM_REGISTER_INFO_END
          break
        else
          raise ArgumentError, "Unknown customer parameter (insert)", caller
      end
    }

    # Se mandou o identificador do Facebook, teremos que criar uma estrutura para armazenar os dados obtidos
    # do site de relacionamentos
    fb_profile = nil;
    if( ( fb_uid != -1 ) && ( fb_session_key.length > 0 ) && ( fb_session_secret.length > 0 ) )
      fb_app_data = FacebookData.find_by_app_id( params[ :app ].id.to_i );
      if( fb_app_data && fb_app_data.api_key )
        fb_profile = FacebookController::create_fb_profile( fb_app_data.api_key, fb_uid, fb_session_key, fb_session_secret );
      end
    end

    return customer, fb_profile;
  end


  def password()
    @password
  end


  def password=(pwd)
    unless pwd.blank?
      @password = pwd
      create_new_salt()
      self.hashed_password = Customer.encrypted_password( self.password, self.salt )
    end
  end


  def self.create_validation_hash( customer )
    return Digest::SHA1.hexdigest("#{customer.object_id}*\#{@\&KaJDS-#{customer.nickname}\*\&l\=#{customer.email}kjjd_as927a#{customer.created_at}")
  end


  ##
  # Indica se o usuário está bloqueado por não ter validado seu e-mail a tempo.
  ##
  def is_blocked?
    return !self.validate_before.nil?

    # se "self.validate_before" for nil "self.validate_before <= Time.now vai dar erro (evaluating nil.<=
#    return !self.validate_before.nil? || self.validate_before <= Time.now
  end

  # TODO falta escrever teste
  ##
  # Escreve os dados do usuário num OutputStream.
  ##
  def write_data( out, params )
    out.write_byte( PARAM_REGISTER_CUSTOMER_ID )
    out.write_int( self.id )
    out.write_byte( PARAM_REGISTER_EMAIL )
    out.write_string( self.email )
    out.write_byte( PARAM_REGISTER_NICKNAME )
    out.write_string( self.nickname )
    out.write_byte( PARAM_REGISTER_CPF )
    out.write_string( self.cpf )
    out.write_byte( PARAM_REGISTER_FIRST_NAME )
    out.write_string( self.first_name )
    out.write_byte( PARAM_REGISTER_LAST_NAME )
    out.write_string( self.last_name )

    out.write_byte( PARAM_REGISTER_PHONE_NUMBER )
    out.write_string( self.phone_number )

    # verifica versão do Nano Online cliente
    if ( params[ ID_NANO_ONLINE_VERSION ] >= '0.0.3' )
      out.write_byte( PARAM_REGISTER_GENRE )
      out.write_char( self.genre )

      out.write_byte( PARAM_REGISTER_BIRTHDAY )
      out.write_datetime( self.birthday )

      if ( params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31' )
        out.write_byte( PARAM_REGISTER_VALIDATE_BEFORE )
        out.write_datetime( self.validate_before )
        out.write_byte( PARAM_REGISTER_VALIDATED_AT )
        out.write_datetime( self.validated_at )
#        out.write_byte( PARAM_REGISTER_FB_UID )
      end
    end

    out.write_byte( PARAM_REGISTER_INFO_END )
  end

  # TODO falta escrever teste
  ##
  # Atualiza os dados de um usuário a partir de parâmetros binários passados por uma aplicação Nano.
  ##
  def self.update_binary( binary_params, value = nil )
    customer_data = {}
    customer_id = -1
    customer_timestamp = -1
    @send_validation_email = binary_params[ ID_NANO_ONLINE_VERSION ] >= '0.0.31'

    # Integração com Facebook
    fb_uid = -1;
    fb_session_key = "";
    fb_session_secret = "";

    parse_data( binary_params[ ::ID_SPECIFIC_DATA ], value ) { |stream, id|
      case id
        when PARAM_REGISTER_EMAIL
          customer_data[ :email ] = stream.read_string()
          logger.info( "EMAIL LIDO: #{customer_data[ :email ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_NICKNAME
          customer_data[ :nickname ] = stream.read_string()
          logger.info( "NICKNAME LIDO: #{customer_data[ :nickname ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_PASSWORD
          customer_data[ :password ] = stream.read_string()
          customer_data[:password_confirmation] = customer_data[:password]
          logger.info( "PASSWORD LIDA: #{customer_data[ :password ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_PASSWORD_CONFIRM
          customer_data[ :password_confirmation ] = stream.read_string()
          logger.info( "CONFIRMAÇÃO LIDA: #{customer_data[ :password_confirmation ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_CPF
          customer_data[ :cpf ] = stream.read_string()
          logger.info( "CPF LIDO: #{customer_data[ :cpf ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_FIRST_NAME
          customer_data[ :first_name ] = stream.read_string()
          logger.info( "NOME LIDO: #{customer_data[ :first_name ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_LAST_NAME
          customer_data[ :last_name ] = stream.read_string()
          logger.info( "SOBRENOME LIDO: #{customer_data[ :last_name ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_PHONE_NUMBER
          customer_data[ :phone_number ] = stream.read_string()
          logger.info( "TELEFONE LIDO: #{customer_data[ :phone_number ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_GENRE
          customer_data[ :genre ] = stream.read_char()
          logger.info( "GÊNERO LIDO: #{customer_data[ :genre ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_BIRTHDAY
          customer_data[ :birthday ] = stream.read_datetime()
          logger.info( "ANIVERSÁRIO LIDO: #{customer_data[ :birthday ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_CUSTOMER_ID
          customer_id = stream.read_int()
          logger.info( "ID LIDO: #{customer_id }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_VALIDATED_AT
          customer_data[ :validated_at ] = stream.read_datetime()
          logger.info( "VALIDAÇÃO LIDA: #{customer_data[ :validated_at ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_VALIDATE_BEFORE
          customer_data[ :validate_before ] = stream.read_datetime()
          logger.info( "LIMITE DE VALIDAÇÃO LIDO: #{customer_data[ :validate_before ] }" ) if RAILS_ENV != 'production'
        when PARAM_REGISTER_TIMESTAMP
          # TODO tratar timestamp do usuário - se informações já estiverem atualizadas, não há necessidade de reenviá-las
          customer_timestamp = stream.read_datetime()
        when PARAM_REGISTER_FB_UID
          fb_uid = stream.read_long();
          logger.info( "FACEBOOK UID: #{ fb_uid }" ) if RAILS_ENV != 'production';
        when PARAM_REGISTER_FB_SESSION_KEY
          fb_session_key = stream.read_string();
          logger.info( "FACEBOOK SESSION KEY: #{ fb_session_key }" ) if RAILS_ENV != 'production';
        when PARAM_REGISTER_FB_SESSION_SECRET
          fb_session_secret = stream.read_string();
          logger.info( "FACEBOOK SESSION SECRET: #{ fb_session_secret }" ) if RAILS_ENV != 'production';
        when PARAM_REGISTER_INFO_END
          break
        else
          logger.info( "Unknown customer parameter (update) - ID = #{id}" ) if RAILS_ENV != 'production';
          raise ArgumentError, "Unknown customer parameter (update)", caller
      end
    }

    if ( customer_id >= 0 )
      customer = Customer.find( customer_id )
    else
      # trata clientes Nano Online < 0.0.3
      customer = Customer.find( binary_params[ :customer ] )
    end

    unless( customer.nil? )
      # Se mandou o identificador do Facebook, teremos que atualizar, ou criar, a estrutura que armazena os dados obtidos
      # do site de relacionamentos
      if( ( fb_uid != -1 ) && ( fb_session_key.length > 0 ) && ( fb_session_secret.length > 0 ) )

        fb_app_data = FacebookData.find_by_app_id( params[ :app ].id.to_i );

        if( fb_app_data && fb_app_data.api_key )
          old_fb_profile = FacebookProfile.find_by_customer_id( customer.id );

          new_fb_profile = FacebookController::create_fb_profile( fb_app_data.api_key, fb_uid, fb_session_key, fb_session_secret );
          new_fb_profile.customer_id = customer.id;

          if( old_fb_profile == nil )
            new_fb_profile.save();
          else
            old_fb_profile.update_attributes( new_fb_profile.attributes );
          end
        end
      end

      customer.mobile_client = true

      if customer.update_attributes( customer_data )
        customer.send_email_validation
        @email_changed = false
        return true, customer
      else
        return false, customer
      end

#      return customer.update_attributes( customer_data ), customer
    end

    return false, nil
  end

  def self.authenticate( nickname_or_email, password )
    user = self.find_by_nickname( nickname_or_email )
    user = self.find_by_email(nickname_or_email) if user.nil?
    if ( user && !user.check_password( password ) )
      user = nil
    end

    return user
  end

  


  def check_password( password )
    expected_password = Customer.encrypted_password( password, self.salt )
    return self.hashed_password == expected_password
  end

  # TODO falta escrever teste
  def send_email_validation
    # TODO alterar idioma do e-mail
    dados = Hash.new

    dados["subject"] = "Validação de cadastro"
    dados["message"] = "Olá #{nickname}! Para validar seu email clique no link abaixo.\n#{URL_NANO_ONLINE}/validate/#{validation_hash}"
    dados["recipients"] = self.email
    dados["sender"] = "no-reply@nanogames.com.br"

    Emailer.deliver_send(dados)
  end

  def mobile_client=(b)
  	@mobile_client = b
  end

  def is_mobile_client?
    @mobile_client ||= false

    return @mobile_client
  end

  # TODO falta escrever teste para o resend_password
  def reset_password
    charset = %w{2 3 4 6 7 8 9 A C D E F G H J K L M N P Q R T V W X Y Z}
    new_password = ""

    while !(new_password =~ /\d/)
      new_password = (0...7).map{charset.to_a[rand(charset.size)]}.join
    end

    self.password = new_password
    if self.save
      resend_password
    end
  end

  # TODO falta escrever teste
  def resend_password
    dados = Hash.new
    dados["subject"] = "Nova senha Nano Games."
    dados["message"] = "Ola #{self.nickname}!\nSua nova senha é: #{self.password}\nNão esqueça de alterá-la em seu próximo login."
    dados["recipients"] = self.email
    dados["sender"] = "no-reply@nanogames.com.br"

    Emailer.deliver_send(dados)
  end

  def active_match
    self.active_participation.match
  end
  
  def active_turn
    self.active_participation.match.active_turn
  end

  ##############################################################################
  # MétodOS PROTEGIDOS                                                         #
  ##############################################################################
  protected

  def default_values
    self.genre = 'n' if self.genre.blank?
  end


  def validate()
    users = Customer.find_all_by_email( self.email ) || []

    if ( users.length > 0 )
      if ( users.length > 1 || users[ 0 ] != self )
        errors.add(:email, "E-mail already in use(#{RC_ERROR_EMAIL_ALREADY_IN_USE})." )
      end
    end
  end


  def validate_on_create()
    return validate_password() && validate_cpf( true )
  end


  def validate_on_update
    if ( password.blank? && password_confirmation.blank? )
      return true
    end

    validate_password()
  end


  ##
  #
  ##
  def before_save()
    if ( self.email_changed? )
      @email_changed = true

      # puts( "--------------> MUDOU E-MAIL:" ) if RAILS_ENV != 'production'
      # como o usuário mudou o e-mail, precisa (re)validá-lo
      self.validation_hash = Customer.create_validation_hash( self )

      unless ( self.validate_before )
        # se o jogador já tinha um prazo para validação, ele deve ser respeitado até que seja validado
        if ( is_mobile_client? )
          self.validate_before = Time.now + VALIDATION_TIME_MOBILE
        else
          self.validate_before = Time.now + VALIDATION_TIME_WEB
        end
      end
    end
  end

  # TODO falta escrever teste
  ##
  #
  ##
  def after_save()
    if ( @email_changed && @send_validation_email )
      send_email_validation()
    end
  end


  ##############################################################################
  # MétodOS PRIVADOS                                                           #
  ##############################################################################
private

  def validate_password
    unless ( password && password.length > 6 && password.match( /^.*(?=.*\d)(?=.*[a-z]).*$/ ) ) 
      errors.add( :password, "must contain at least 6 characters, including upper-case and lower-case letters, and numbers or special characters (\.\,\_\?!\-+@:) (#{RC_ERROR_PASSWORD_WEAK})" )
    end
  end


  ##
  # Valida um número de CPF.
  ##
  def validate_cpf( allow_nil = true )
    if ( cpf && cpf.length > 0 )
      for i in 0..9 do
        j = 0
        digit = i + 48
        while ( j < cpf.length )
          if ( cpf[ j ] != digit )
            j = cpf.length + 1
          else
            j = j + 1
          end
        end

        if ( j == cpf.length )
          # todos os dígitos são iguais, logo é inválido
          return false
        end
      end

      d1 = 0
      d2 = 0
      digito1 = 0
      digito2 = 0
      resto = 0
      digito_cpf = 0
      n_dig_result = ""

      for n_count in  1..cpf.length - 1 do
        digito_cpf = cpf[ ( n_count - 1 )..n_count ].to_i()

        # multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
        d1 = d1 + ( 11 - n_count ) * digito_cpf

        # para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.
        d2 = d2 + ( 12 - n_count ) * digito_cpf
      end

      # Primeiro resto da divisão por 11.
      resto = ( d1 % 11 )

      # Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
      if ( resto < 2 )
        digito1 = 0
      else
        digito1 = 11 - resto
      end

      d2 += 2 * digito1

      # Segundo resto da divisão por 11.
      resto = ( d2 % 11 )

      # Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
      if ( resto < 2 )
        digito2 = 0
      else
        digito2 = 11 - resto
      end

      # Digito verificador do CPF que está sendo validado.
      n_dig_verific = cpf[ ( cpf.length - 2 )..cpf.length ]

      # Concatenando o primeiro resto com o segundo.
      n_dig_result = "#{digito1}#{digito2}"

      # comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
      return n_dig_verific == n_dig_result
    end

    return allow_nil
  end


  def create_new_salt()
    self.salt = self.object_id.to_s + rand.to_s
  end


  ##
  #
  ##
  def self.encrypted_password( password, salt )
    # os caracteres literais são necessários devido à mudança para UTF8
    string_to_hash = password + "QA#%WS\313D%1&R\250FT*3&G6*(U(HTB" + salt #dificulta a adivinhação da senha
    return Digest::SHA1.hexdigest( string_to_hash )
  end


  def resend_password()
    data = Hash.new
    data["subject"] = "Nova senha Nano Games."
    data["message"] = "Olá #{self.nickname}!\nSua nova senha é: #{self.password}\nNão esqueça de alterá-la em seu próximo login."
    data["recipients"] = self.email
    data["sender"] = "no-reply@nanogames.com.br"

    Emailer.deliver_send(data)
  end

end

