class Lobby < ActiveRecord::Base
  belongs_to :multiplayer_version
  
  has_and_belongs_to_many :customers
  
  # todo - esse codigo não era para estar em um model...
  def begin_match
    match = Match.new(:multiplayer_version_id => self.multiplayer_version_id)
    self.customers.each_with_index do |customer, index|
      p = match.participations.build
      p.customer = customer
      p.order = index + 1
    end
    
    self.customers.clear
    match.save
    
    return match
  end
end
