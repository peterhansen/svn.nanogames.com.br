class AppVersionState < ActiveRecord::Base
  has_many :app_versions
  
  validates_presence_of :name, :code
  validates_uniqueness_of :name, :code
  validates_length_of :name, :maximum => 20
end
