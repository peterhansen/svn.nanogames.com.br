class Vendor < ActiveRecord::Base
  has_many :devices
  
  validates_uniqueness_of :name
  validates_presence_of :name
  
      
  def self.with_device
    Vendor.all(:include => :devices, :conditions => { :active => true }, :order => :name ).delete_if {|v| v.devices.length <= 0}
  end
end
