class MultiplayerVersion < ActiveRecord::Base
  has_many :matches
  has_many :app_versions
  has_one  :lobby
end
