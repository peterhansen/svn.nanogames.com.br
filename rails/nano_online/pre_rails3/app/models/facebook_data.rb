class FacebookData < ActiveRecord::Base

  ##############################################################################
  # Códigos de retorno                                                         #
  ##############################################################################

  # A aplicação apontada por :app_id já está associada a outros dados de Facebook
  RC_FB_DATA_ERROR_APP_ID_IN_USE                    = 1

  # A aplicação de Facebook identificada :facebook_app_id já está registrada
  RC_FB_DATA_ERROR_FB_APP_ID_ALREADY_REGISTERED     = 2

  # Esta api key já pertence a outra entrada da tabela
  RC_FB_DATA_ERROR_APIKEY_IN_USE                    = 3

  # Este canvas name já pertence a outra entrada da tabela
  RC_FB_DATA_ERROR_CANVAS_NAME_IN_USE               = 4

  ##############################################################################
  # Relacionamentos                                                            #
  ##############################################################################

  belongs_to :app

  ##############################################################################
  # Validações de Colunas                                                      #
  ##############################################################################

  validates_presence_of   :app_id
  validates_presence_of   :facebook_app_id
  validates_presence_of   :api_key
  validates_presence_of   :api_secret
  validates_presence_of   :canvas_name
  validates_presence_of   :display_name

  validates_uniqueness_of :app_id,
                          :message => "There is another FacebookData related to this app_id (#{RC_FB_DATA_ERROR_APP_ID_IN_USE})"

  validates_uniqueness_of :facebook_app_id,
                          :message => "This facebook_app_id already belongs to another table entry (#{RC_FB_DATA_ERROR_FB_APP_ID_ALREADY_REGISTERED})"

  validates_uniqueness_of :api_key,
                          :message => "This api_key already belongs to another table entry (#{RC_FB_DATA_ERROR_APIKEY_IN_USE})"

  validates_uniqueness_of :canvas_name,
                          :message => "This canvas_name already belongs to another table entry (#{RC_FB_DATA_ERROR_CANVAS_NAME_IN_USE})"

end
