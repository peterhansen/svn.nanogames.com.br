class AppCategory < ActiveRecord::Base
  has_and_belongs_to_many :apps
  
  validates_presence_of :name
  validates_length_of :name, :maximum => 20
end
