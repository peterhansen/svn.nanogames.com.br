class Turn < ActiveRecord::Base
  belongs_to :match
  
  has_many :actions, :dependent => :destroy
  has_many :customers, :through => :actions
end
