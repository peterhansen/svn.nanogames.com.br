class Integrator < ActiveRecord::Base
  has_and_belongs_to_many :operators
  
  has_many :submissions
  has_many :app_versions, :through => :submissions, :source => :integrator
  
  has_many :device_integrators
  has_many :devices, :through => :device_integrators, :source => :device
  
  validates_presence_of :name
end
