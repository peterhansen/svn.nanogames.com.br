class AdPieceVersionVisualization < ActiveRecord::Base
	belongs_to :ad_piece_version
	belongs_to :customer
	belongs_to :device

	validates_presence_of :time_start
end
