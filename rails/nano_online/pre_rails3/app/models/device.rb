class Device < ActiveRecord::Base
  belongs_to :vendor                  # tested
  belongs_to :midp_version            # tested
  belongs_to :cldc_version            # tested
  belongs_to :family                  # tested

  has_many :downloads                 # tested
  has_many :accesses                  # tested
  has_many :device_integrators, :dependent => :destroy       # tested
  has_many :integrators, :through => :device_integrators, :source => :device
  has_many :device_integrators, :dependent => :destroy
  has_many :ad_piece_version_visualizations
  has_many :ranking_entries

  has_and_belongs_to_many :device_bugs
  has_and_belongs_to_many :optional_apis
  has_and_belongs_to_many :customers
  has_and_belongs_to_many :data_services
  has_and_belongs_to_many :bands

  #ver comentário na classe Submission a respeito da necessidade desse relacionamento
  has_and_belongs_to_many :submissions

  validates_uniqueness_of :model, :scope => :vendor_id

  validates_presence_of :vendor, :model                                     # tested

  validates_length_of :commercial_name, :maximum => 20, :allow_nil => true  # tested
  validates_length_of :user_agent, :maximum => 20, :allow_nil => true       # tested
  validates_length_of :model, :in => 1..20                                  # tested

  validates_numericality_of :screen_width, :allow_nil => true               # tested
  validates_numericality_of :screen_height_full, :allow_nil => true         # tested
  validates_numericality_of :screen_height_partial, :allow_nil => true      # tested
  validates_numericality_of :jar_size_max, :allow_nil => true               # tested
  validates_numericality_of :memory_heap, :allow_nil => true                # tested
  validates_numericality_of :memory_image, :allow_nil => true               # tested


  def name
    model
  end

  # retorna a lista de versões de aplicativos que suportam o aparelho.
  def app_versions()
    versions = []

    AppVersion.all.each() do |version|
      if ( version.families.include?( family ) )
        versions << version
      end
    end

    return versions
  end


  def model_full()
    "#{vendor.name} #{model}"
  end


  def full_name
    if self.vendor
      "#{self.vendor.name} - #{self.model}"
    else
      self.model
    end
  end

# adaptado de:
# +----------------------------------------------------------------------+
# | Mobile user agent string parsing class for PHP5.                     |
# | Copyright (C) 2004 Craig Manley                                      |
# +----------------------------------------------------------------------+
# | This library is free software; you can redistribute it and/or modify |
# | it under the terms of the GNU Lesser General Public License as       |
# | published by the Free Software Foundation; either version 2.1 of the |
# | License, or (at your option) any later version.                      |
# |                                                                      |
# | This library is distributed in the hope that it will be useful, but  |
# | WITHOUT ANY WARRANTY; without even the implied warranty of           |
# | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU     |
# | Lesser General Public License for more details.                      |
# |                                                                      |
# | You should have received a copy of the GNU Lesser General Public     |
# | License along with this library; if not, write to the Free Software  |
# | Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  |
# | USA                                                                  |
# |                                                                      |
# | LGPL license URL: http:#opensource.org/licenses/lgpl-license.php    |
# +----------------------------------------------------------------------+
# | Author: Craig Manley                                                 |
# +----------------------------------------------------------------------+
#
# @Id: MobileUserAgent.php,v 1.6 2004/12/21 15:07:29 cmanley Exp @
#



#/**
# * @author    Craig Manley
# * @copyright Copyright é 2004, Craig Manley. All rights reserved.
# * @version   @Revision: 1.6 @
# * @package   com.craigmanley.classes.mobile.MobileUserAgent
# */


#/**
# * Parses a mobile user agent string into it's basic constituent parts, the
# * most important being vendor and model.
# *
# * One reason for doing this would be to use this information to lookup vendor-model
# * specific device characteristics in a database. Of course you could use user agent
# * profiles for this, but not all mobile phones have (valid) user agent profiles, especially
# * the older types of mobile phones.
# *
# * Another reason would be to detect if the visiting client is a mobile handset. You could do
# * it like this:
# * <pre>
# *  require_once('MobileUserAgent.php');
# *  @mua = new MobileUserAgent();
# *  @is_client_mobile = @mua->success();
# * </pre>
# *
# * Some references:
# * <ul>
# *  <li>{@link http://www.handy-ortung.com }</li>
# *  <li>{@link http://www.mobileopera.com/reference/ua }</li>
# *  <li>{@link http://www.appelsiini.net/~tuupola/php/Imode_User_Agent/source/ }</li>
# *  <li>{@link http://www.zytrax.com/tech/web/mobile_ids.html }</li>
# *  <li>{@link http://webcab.de/wapua.htm }</li>
# *  <li>{@link http://www.nttdocomo.co.jp/english/p_s/i/tag/s2.html }</li>
# *  <li>{@link http://test.waptoo.com/v2/skins/waptoo/user.asp }</li>
# * </ul>
# *
# * @package  com.craigmanley.classes.mobile.MobileUserAgent
#  */

  @@regexp_standard_user_agent = /^((ACER|Alcatel|AUDIOVOX|BenQ|BlackBerry|CDM|Ericsson|Gradiente|HTC|LG\b|LGE|LG\/|Motorola|(MOT-PEBL)|MOTO|MOT|moto|NEC|Nokia|Panasonic|QCI|SAGEM|SAMSUNG|SEC|Sanyo|Sendo|SHARP|SIE|SonyEricsson|Telit|Telit_Mobile_Terminals|TSM)[- ]?([^\/\s\_]+))(\/(\S+))?/
  @@regexp_vendor_only = /((ACER|Alcatel|AUDIOVOX|BenQ|BlackBerry|CDM|Ericsson|Gradiente|HTC|LG\b|LGE|Motorola|MOTO|MOT|NEC|Nokia|Panasonic|QCI|SAGEM|SAMSUNG|SEC|Sanyo|Sendo|SHARP|SIE|SonyEricsson|Telit|Telit_Mobile_Terminals|TSM))/


  def self.test_ua()
    list = [
#     "MOT-RAZRV3x/85.9A.70R MIB/BER2.2 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 NokiaN76-1/31.0.014 Profile/MIDP-2.0 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
#     "Mozilla/5.0 (SymbianOS/9.2; U; Series60/3.1 NokiaN95_8GB/10.0.021; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
#     "Mozilla/5.0 (SymbianOS/9.3; U; Series60/3.2 NokiaN96-1/1.00; Profile/MIDP-2.1 Configuration/CLDC-1.1;) AppleWebKit/413 (KHTML, like Gecko) Safari/413",
#     "MOT-PEBL U6/08.84.09R MIB/2.2.1 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "SonyEricssonK550i/R1JD Browser/NetFront/3.3 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "HTC-S411",
#     "SEC-SGHX810/1.0 NetFront/3.2 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "SEC-SGHX820/1.0 NetFront/3.2 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "MOTO-Z10/",
#     "MOT-W6/08.00.0CR MIB/BER2.2 Profile/MIDP-2.0 Configuration/CLDC-1.1 EGE/1.0",
#     "MOT-A1200eam/1.0/R541L7_G_11.00.16R Mozilla/4.0 (compatible; MSIE 6.0; Linux; A1200eam; 781) Profile/MIDP-2.0 Configuration/CLDC-1.1 Opera 8.00",
#     "LG G8000/2.0 PDK/2.5 JAVA",
#     "LG VX5200",
#     "LG/KU990/v10a Browser/Obigo-Q05A/3.6 MMS/LG-MMS-V1.0/1.2 Java/ASVM/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "LG-ME970 MIC/1.1.14 MIDP-2.0/CLDC-1.1",
#     "MOT-EM25/1.0 Release/04.18.2008 Browser/CMCS1.0 Software/0.110 Profile/MIDP-2.0 Configuretion/CLDC-1.1",
#     "MOT-A1200eam/1.0/R541L7_G_11.00.1AR Mozilla/4.0 (compatible; MSIE 6.0; Linux; A1200eam; 781) Profile/MIDP-2.0 Configuration/CLDC-1.1 Opera 8.00",
#     "MOT-A1200i/R532L4_G_11.40.1AR Mozilla/4.0 (compatible; MSIE 6.0; Linux; Motorola A1200i;nnn) Profile/MIDP-2.0 Configuration/CLDC-1.1 Opera 8.00 [en]",
#     "motorazrV8/R601_G_80.53.3CRP Mozilla/4.0 (compatible; MSIE 6.0 Linux; Motorola V8;nnn) Profile/MIDP-2.0 Configuration/CLDC-1.1 Opera 8.50[yy]",
#     "motorazrV8/R601_G_80.56.1BRP Mozilla/4.0 (compatible; MSIE 6.0 Linux; Motorola V8;nnn) Profile/MIDP-2.0 Configuration/CLDC-1.1 Opera 8.50[yy]",
#     "SAMSUNG-SGH-Z310",
#     "SAMSUNG-SGH-Z310/1.0 SHP/VPP/R5 SMM-MMS/1.2.0 profile/MIDP-2.0 configuration/CLDC-1.1",
#
#     "SAMSUNG-SGH-D900i/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "GF500/BSI AU.Browser/2.0 QO3C1 MMP/1.0 GF500/BSI AU.Browser/2.0 QO3C1 MMP/1.0",
#     "SGH-Z300/1.0 SHP/VPP/R5 SMB3.1 SMM-MMS/1.2.0 profile/MIDP-2.0 configuration/CLDC-1.1",
#     "Nokia5130c-2/2.0 (06.65) Profile/MIDP-2.1 Configuration/CLDC-1.1",
#     "Nokia2680s-2/1.0 (04.56) Profile/MIDP-2.1 Configuration/CLDC-1.1",
#     "LG/KF755/v1.0 Profile/MIDP-2.1 Configuration/CLDC-1.1 LG/KF755/v1.0 Profile/MIDP-2.1 Configuration/CLDC-1.1",
#     "SAMSUNG-SGH-F250L/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 UP.Browser/6.2.3.3.c.1.101 (GUI) MMP/2.0",
#     "LG-MG810C UP.Browser/6.2.3 (GUI) MMP/1.0",
#     "Nokia2680s-2/1.0 (05.28) Profile/MIDP-2.1 Configuration/CLDC-1.1",
#     "LG/KE990c/v10a Browser/Obigo-Q05A/3.6 MMS/LG-MMS-V1.0/1.2 Java/ASVM/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 LG/KE990c/v10a Browser/Obigo-Q05A/3.6 MMS/LG-MMS-V1.0/1.2 Java/ASVM/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1",
#     "SEC-SGHF210L/1.0 NetFront/3.4 Profile/MIDP-2.0 Configuration/CLDC-1.1",
      "iPhone ## iPhone OS 3.1.2 ## acd1590844368d8fb0a10a1baa07111b38385af8",
      "iPad ## iPhone OS 3.2 ## 316078d94d731ef64a873a9aab46d665b302b2a",
#      "Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en) AppleWebKit/420+ (KHTML, like Gecko) Version/3.0 Mobile/1A543a Safari/419.3 ",
#      "Mozilla/5.0 (iPod; U; CPU iPhone OS 2_2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5H11a Safari/525.20",
#      "Mozilla/5.0 (iPhone; U; CPU iPhone OS 2_1 like Mac OS X; en-us) AppleWebKit/525.18.1 (KHTML, like Gecko) Version/3.1.1 Mobile/5F136 Safari/525.20",
#      "Mozilla/5.0 (iPhone Simulator; U; CPU iPhone OS 3_1_2 like Mac OS X; pt-pt) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7D11 Safari/528.16",
    ]

    list.each() do |ua|
      puts( "\n\n===========testando: #{ua}===========\n\n")
      ua = UaProfiler::UserAgent.new( ua )
      if ua.device
        puts( "RESULTADO: #{ua.device.vendor.name} - #{ ua.device.model }" )
      else
        puts( "APARELHO NÃO ENCONTRADO!")
      end

#      puts( "RESULTADO: #{ parse_user_agent_standard( ua ) } -> #{ parse_user_agent_mozilla( ua ) }" )
    end
  end


  # busca um aparelho de acordo com seu user-agent.
  # parâmetros:
  # user_agent a ser procurado (pode ser a string completa)
  # log_if_fail: indica se a tabela de detecções falhas de user-agents deve ser atualizada
  # caso não seja encontrado nenhum aparelho com o user-agent recebido como parâmetro.
  # Valores retornados: 1 - referência para o aparelho encontrado; 2 - padrão detectado (útil
  # no caso de não detectar o aparelho, mas detectar que é ALGUM aparelho)
  def self.parse_user_agent( user_agent, log_if_fail = true )
#    puts( "parse_user_agent: #{user_agent}, #{log_if_fail}" )
    device = nil
    matches = nil

    if ( user_agent && !user_agent.empty? )
      # a primeira tentativa é o iPhone, por ter padrão bem diferente dos demais
      matches = parse_user_agent_apple( user_agent )
      if ( matches.nil? || matches[ :MODEL ].nil? )
        puts( "NAO ACHOU UA APPLE" ) if RAILS_ENV != 'production'
        matches = parse_user_agent_mozilla( user_agent )
      end

      # tenta filtrar os user-agents padrão; se não conseguir, tenta os Mozilla-compatible
      if ( matches.nil? || matches[ :MODEL ].nil? )
        matches = parse_user_agent_standard( user_agent )
        if ( matches.nil? || matches[ :MODEL ].nil? )
          puts( "NAO ACHOU UA STANDARD" ) if RAILS_ENV != 'production'
          matches = parse_user_agent_mozilla( user_agent )
        end
      end

      if ( matches[ :MODEL ] && matches[ :VENDOR ] )
        vendor = Vendor.find_by_name( matches[ :VENDOR ] )
        puts "\nACHOU MODELO: =#{vendor}= -> =#{matches[ :MODEL ] }=" if RAILS_ENV != 'production'
        device = Device.find_by_vendor_id_and_model( vendor, matches[ :MODEL ] )
        puts "\nAPOS FIND: #{device}" if RAILS_ENV != 'production'
      end

      unless ( device )
        if ( matches[ :USER_AGENT ] )
          puts "\nTENTANDO: #{matches[ :USER_AGENT ] }" if RAILS_ENV != 'production'
          # conseguiu filtrar o user-agent; procura no banco de dados
          device = Device.find_by_user_agent( matches[ :USER_AGENT ] )
        end
      end

      unless ( device )
        puts "\nNOVA TENTATIVA: #{ user_agent }" if RAILS_ENV != 'production'
        # tenta encontrar o aparelho diretamente pelo user-agent recebido
        device = Device.find_by_user_agent( user_agent )
      end
      puts "\nDEVICE:#{device.nil? ? 'NAO ENCONTRADO' : device.model_full}\n\n" if RAILS_ENV != 'production'

      unless ( device )
        # ou não conseguiu filtrar o user-agent ou não achou aparelho com o user-agent filtrado
        if ( log_if_fail )
          # grava no sistema o user-agent não detectado
          uua = UndetectedUserAgent.find_by_user_agent( user_agent )

          if ( uua )
            uua.accesses += 1
          else
            uua = UndetectedUserAgent.new( :user_agent => user_agent )
          end

          uua.save!
        end
      end
    end

    return device, matches
  end


  ##############################################################################
  protected

#  /**
#   * Parses a standard mobile user agent string with the format vendor-model/version.
#   * If no match can be made, FALSE is returned.
#   * If a match is made, an associative array is returned containing the compulsory
#   * keys "vendor" and "model", and the optional keys "version", and "screendims".
#   *
#   * Below are a few samples of these user agent strings:
#   * <pre>
#   *  Nokia8310/1.0 (05.57)
#   *  NokiaN-Gage/1.0 SymbianOS/6.1 Series60/1.2 Profile/MIDP-1.0 Configuration/CLDC-1.0
#   *  SAGEM-myX-6/1.0 UP.Browser/6.1.0.6.1.c.3 (GUI) MMP/1.0 UP.Link/1.1
#   *  SAMSUNG-SGH-A300/1.0 UP/4.1.19k
#   *  SEC-SGHE710/1.0
#   * </pre>
#   *
#   * @param string @useragent User agent string.
#   * @return hash com as seguintes chaves:
#   :USER_AGENT
#   :VENDOR
#   :MODEL
#   :VERSION
#   */
  def self.parse_user_agent_standard( user_agent )
    # Standard vendor-model/version user agents
    both = nil
    vendor = nil
    model = nil
    version = nil

    matches = user_agent.match( @@regexp_standard_user_agent )

    temp = 0
    if ( RAILS_ENV != 'production' )
       matches.to_a.each { |m|
         puts( "MATCH #{temp}: #{m}")
         temp += 1
       }
    end

    if ( matches )
      both    = matches[ 1 ]
      vendor  = matches[ 2 ].gsub( /\b/, '' )
      model   = matches[ 3 ] || matches[ 4 ]

      if ( matches.length >= 6 )
        version = matches[ 5 ]
      end

      # Fixup vendors and models.
      vendor = vendor_clean( vendor )
      case vendor
        when 'CDM'
          model = "CDM-model"

        when 'Ericsson'
          if ( model == 'T68_NIL' )
            model = 'T68'
          end

        when 'LG', 'LG/'
          m = model.match( /^([A-Za-z\d]+)-/ )
          if ( m ) # LGE510W-V137-AU4.2
            model = m[ 1 ]
          end

        when 'Motorola'
          model.gsub!( /[\._]/, '' )

        when 'Nokia'
          # remove possíveis indicadores de versões remanescentes no modelo, como
          # N76-1 (passando a ser apenas N76)
          model.gsub!( /(\-.*)+/, '' )
          both = vendor.concat( model )

        when 'Philips'
          model.upcase!()

        when 'SAGEM'
          if ( model == '-' )
            return nil
          end

        when 'Samsung'
          model.gsub!( /\*.*/, '' )

        when 'TSM'
          model = both
      end
    else
      # não conseguiu quebrar o user-agent em fabricante, modelo, etc. - tenta pelo
      # menos detectar o fabricante (utiliza expressão regular menos rígida)
      temp = user_agent.match( @@regexp_vendor_only )
      if ( temp )
        vendor = vendor_clean( temp[ 0 ] )
      end
    end

    result = { :USER_AGENT => both,
               :VENDOR  => vendor,
               :MODEL   => model,
               :VERSION => version }

    puts( "user-agent detection result: #{result[ :VENDOR ]} -> #{result[:MODEL]}" ) if RAILS_ENV != 'production'
    return result
  end


  ##
  # Retorna uma versão "limpa" do nome do fabricante.
  ##
  def self.vendor_clean( vendor )
    case ( vendor )
      when 'ACER'
        vendor = 'Acer'

      when 'AUDIOVOX', 'CDM'
        vendor = 'Audiovox'

      when 'MOT', 'MOTO', 'moto'
        vendor = 'Motorola'

      when 'PHILIPS'
        vendor = 'Philips'

      when 'SEC'
        vendor = 'Samsung'

      when 'SIE'
        vendor = 'Siemens'

      when 'Telit_Mobile_Terminals'
        vendor = 'Telit'

      when 'TSM'
        vendor = 'Vitelcom'
    end

    return vendor
  end



#  /**
#   * Parses an i-mode user agent string.
#   * If no match can be made, FALSE is returned.
#   * If a match is made, an associative array is returned containing the compulsory
#   * keys "vendor" and "model", and the optional keys "version", "imode_cache",
#   * and "screendims".
#   *
#   * Below are a few samples of these user agent strings:
#   * <pre>
#   *  portalmmm/1.0 m21i-10(c10)
#   *  portalmmm/1.0 n21i-10(c10)
#   *  portalmmm/1.0 n21i-10(;ser123456789012345;icc1234567890123456789F)
#   *  portalmmm/2.0 N400i(c20;TB)
#   *  portalmmm/2.0 P341i(c10;TB)
#   *  DoCoMo/1.0/modelname
#   *  DoCoMo/1.0/modelname/cache
#   *  DoCoMo/1.0/modelname/cache/unique_id_information
#   *  DoCoMo/2.0 modelname(cache;individual_identification_information)
#   * </pre>
#   *
#   * @param string @useragent User agent string.
#   * @return mixed
#   */
#  protected function _parseUserAgentImode(@useragent) {
#    @vendors = array (
#      'D'  => 'Mitsubishi',
#      'ER' => 'Ericsson',
#      'F'  => 'Fujitsu',
#      'KO' => 'Kokusai', # Hitachi
#      'M'  => 'Mitsubishi',
#      'P'  => 'Panasonic', # Matsushita
#      'N'  => 'NEC',
#      'NM' => 'Nokia',
#      'R'  => 'Japan Radio',
#      'S'  => 'SAMSUNG',
#      'SH' => 'Sharp',
#      'SO' => 'Sony',
#      'TS' => 'Toshiba',
#    );
#    # Standard i-mode user agents
#    if (preg_match('/^(portalmmm|DoCoMo)\/(\d+\.\d+) ((' . implode('|',array_keys(@vendors)) . ')[\w\-]+)\((c(\d+))?/i', @useragent, @matches)) {
#      @result = array('vendor'      => @vendors[strtoupper(@matches[4])],
#                      'model'       => @matches[3],
#                      'version'     => @matches[2]);
#      if ((count(@matches) == 7) && strlen(@matches[6])) {
#        @result['imode_cache'] = @matches[6] + 0;
#      }
#      else {
#        @result['imode_cache'] = 5;
#      }
#      return(@result);
#    }
#
#    # DoCoMo HTML i-mode user agents
#    elseif (preg_match('/^DoCoMo\/(\d+\.\d+)\/((' . implode('|',array_keys(@vendors)) . ')[\w\.\-\_]+)(\/c(\d+))?/i', @useragent, @matches)) {
#      # HTML 1.0: DoCoMo/1.0/modelname
#      # HTML 2.0: DoCoMo/1.0/modelname/cache
#      # HTML 3.0: DoCoMo/1.0/modelname/cache/unique_id_information
#      @result = array('vendor'      => @vendors[strtoupper(@matches[3])],
#                      'model'       => @matches[2],
#                      'version'     => @matches[1]);
#      if (count(@matches) >= 6) {
#        @result['imode_cache'] = @matches[5] + 0;
#      }
#      else {
#        @result['imode_cache'] = 5;
#      }
#      return @result;
#    }
#
#    return false;
#
#
#  }



#  /**
#   * Parses a Mozilla (so called) compatible user agent string.
#   * If no match can be made, FALSE is returned.
#   * If a match is made, an associative array is returned containing the compulsory
#   * keys "vendor" and "model", and the optional keys "version", and "screendims".
#   *
#   * Below are a few samples of these user agent strings:
#   * <pre>
#   *  Mozilla/4.1 (compatible; MSIE 5.0; Symbian OS; Nokia 3650;424) Opera 6.10  [en]
#   *  Mozilla/4.0 (compatible; MSIE 6.0; Nokia7650) ReqwirelessWeb/2.0.0.0
#   *  Mozilla/1.22 (compatible; MMEF20; Cellphone; Sony CMD-Z5)
#   *  Mozilla/1.22 (compatible; MMEF20; Cellphone; Sony CMD-Z5;Pz063e+wt16)
#   *  Mozilla/2.0 (compatible; MSIE 3.02; Windows CE; PPC; 240x320)
#   *  mozilla/4.0 (compatible;MSIE 4.01; Windows CE;PPC;240X320) UP.Link/5.1.1.5
#   *  Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; PPC; 240x320)
#   *  Mozilla/4.0 (compatible; MSIE 4.01; Windows CE; SmartPhone; 176x220)
#   *  Mozilla/2.0 (compatible; MSIE 3.02; Windows CE; 240x320; PPC)
#   *  Mozilla/2.0 (compatible; MSIE 3.02; Windows CE; Smartphone; 176x220; Mio8380; Smartphone; 176x220)
#   *  Mozilla/4.0 (MobilePhone SCP-8100/US/1.0) NetFront/3.0 MMP/2.0
#   *  Mozilla/2.0(compatible; MSIE 3.02; Windows CE; Smartphone; 176x220)
#   *  Mozilla/4.1 (compatible; MSIE 5.0; Symbian OS Series 60 42) Opera 6.0 [fr]
#   *  Mozilla/SMB3(Z105)/Samsung UP.Link/5.1.1.5
#   * </pre>
#   *
#   * @param string @useragent User agent string.
#   * @return mixed
#   */
  def self.parse_user_agent_mozilla( user_agent )
    # SAMSUNG browsers
    matches = user_agent.match( /^Mozilla\/SMB3\((Z105)\)\/(Samsung)/ )
    if ( matches )
      return { :VENDOR => matches[ 2 ].upcase(),
               :MODEL  => matches[ 1 ] }
    end

    # Extract the string between the brackets.
    matches = user_agent.match( /^Mozilla\/\d+\.\d+\s*\(([^\)]+)\)/i )
    unless ( matches )
      return {}
    end

    # split string between brackets on ';' seperator.
    parts = matches[ 1 ].split( ';' )

    # Apple
    # TODO adicionar iPad e testar detecção dos Apple
    if ( parts[ 0 ].include?( 'iPhone' ) || parts[ 0 ].include?( 'iPod' ) )
      model = parts[ 0 ] == 'iPod' ? parts[ 0 ] + ' touch' : parts[ 0 ]

      return { :VENDOR => 'Apple',
               :MODEL => model,
               :USER_AGENT => model
      }

      return result
    end

    # Microoft PPC and Smartphone browsers. Unfortunately, one day, if history repeats itself, this will probably be the only user-agent check necessary.
    if ( ( parts.length >= 4 ) && parts[ 0 ].eql?( 'compatible' ) && parts[ 2 ].eql?( 'Windows CE' ) )
      result = { :VENDOR => 'Microsoft' }

      if ( parts[ 3 ].eql?( 'PPC' ) || parts[ 3 ].downcase().eql?( 'smartphone' ) )
        result[ :MODEL ] = 'SmartPhone'
      elsif ( ( parts.length >= 5 ) && ( parts[ 4 ].eql?( 'PPC' ) || ( parts[ 4 ].downcase().eql?( 'smartphone' ) ) ) )
      	result[ :MODEL ] = 'SmartPhone'
      end

      if ( result[ :MODEL ] )
        return result
      end
    end

    # Nokia's with Opera browsers or SonyEricssons.
    if ( parts.length >= 4 && parts[ 0 ].eql?( 'compatible' ) )
      matches = parts[ 3 ].match( /^(Nokia|Sony)\s*(\S+)/ )
      if ( matches  )
        if ( matches[ 1 ].eql?( 'Sony' ) )
          matches[ 1 ] = 'SonyEricsson';
        end

        return { :VENDOR => matches[ 1 ], :MODEL => matches[ 2 ] }
      end
    else
      # verifica se é um Nokia que utiliza o formato de user-agent do N76, N95, N96, etc.
      # primeiro pega a string inteira
      matches = parts[ 2 ].match( /(Nokia)\s*(\S+)/ )

      if ( matches )
        parts = matches[ 0 ].split( /[\_\/-]/ )

        return { :VENDOR => 'Nokia', :MODEL => parts[ 1 ], :USER_AGENT => parts[ 0 ] }
      end
    end

    # SANYO browsers
    matches = parts[ 0 ].match( /^MobilePhone ([^\/]+)\/([A-Z]+\/)?(\d+\.\d+)/ )
    if ( ( parts.length >= 1 ) && matches ) # MobilePhone PM-8200/US/1.0
      return { :VENDOR => 'Sanyo', :MODEL  => matches[ 1 ], :VERSION => matches[ 3 ] }
    end

    # Nokias with ReqwirelessWeb browser
    if ( ( parts.length >= 3 ) && parts[ 0 ].eql?( 'compatible' ) )
      matches = parts[ 1 ].match( /^(Nokia)\s*(\S+)/ )

      return { :VENDOR => matches[ 1 ], :MODEL => matches[ 2 ] } if ( matches && matches.length > 1 )
    end

    return {}
  end


  ##
  #
  ##
  def self.parse_user_agent_apple( user_agent )
    # padrão utilizado nas apps
    parts = user_agent.split( ' ## ' )
    if ( parts && parts.length == 3 )
      # TODO extrair ID do aparelho
      return { :VENDOR => 'Apple', :MODEL => parts[ 0 ], :VERSION => parts[ 1 ], :USER_AGENT => parts[ 0 ] }
    else
      return nil
    end
  end


#  /**
#   * Parses a non-standard mobile user agent string.
#   * If no match can be made, FALSE is returned.
#   * If a match is made, an associative array is returned containing the compulsory
#   * keys "vendor" and "model", and the optional keys "version", and "screendims".
#   *
#   * Below are a few samples of these user agent strings:
#   * <pre>
#   *  LGE/U8150/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.0
#   *  PHILIPS855 ObigoInternetBrowser/2.0
#   *  PHILIPS 535 / Obigo Internet Browser 2.0
#   *  PHILIPS-FISIO 620/3
#   *  PHILIPS-Fisio311/2.1
#   *  PHILIPS-FISIO311/2.1
#   *  PHILIPS-Xenium9@9 UP/4.1.16r
#   *  PHILIPS-XENIUM 9@9/2.1
#   *  PHILIPS-Xenium 9@9++/3.14
#   *  PHILIPS-Ozeo UP/4
#   *  PHILIPS-V21WAP UP/4
#   *  PHILIPS-Az@lis288 UP/4.1.19m
#   *  PHILIPS-SYSOL2/3.11 UP.Browser/5.0.1.11
#   *  Vitelcom-Feature Phone1.0 UP.Browser/5.0.2.2(GUI
#   *  ReqwirelessWeb/2.0.0 MIDP-1.0 CLDC-1.0 Nokia3650
#   *  SEC-SGHE710
#   * </pre>
#   * Notice how often one certain brand of these user-agents is handled by this function. I say no more.
#   *
#   * @param string @useragent User agent string.
#   * @return mixed
#   */
#  protected function _parseUserAgentRubbish(@useragent) {
#    # Old ReqwirelessWeb browsers for Nokia. ReqwirelessWeb/2.0.0 MIDP-1.0 CLDC-1.0 Nokia3650
#    if (preg_match('/(Nokia)\s*(N-Gage|\d+)@/', @useragent, @matches)) {
#      return(array('vendor' => @matches[1], 'model' => @matches[2]));
#    }
#
#    # LG Electronics
#    elseif (preg_match('/^(LG)E?\/(\w+)(\/(\d+\.\d+))?/', @useragent, @matches)) {  # LGE/U8150/1.0 Profile/MIDP-2.0 Configuration/CLDC-1.0
#      @result = array('vendor' => @matches[1], 'model' => @matches[2]);
#      if ((count(@matches) == 5) && strlen(@matches[4])) {
#        @result['version'] = @matches[4];
#      }
#      return @result;
#    }
#
#    # And now for the worst of all user agents, those that start with the text 'PHILIPS'.
#    elseif (preg_match('/^(PHILIPS)(.+)/', @useragent, @matches)) {
#      @vendor  = @matches[1];
#      @model   = nil;
#      @garbage = trim(strtoupper(@matches[2])); # everything after the word PHILIPS in uppercase.
#      if (preg_match('/^-?(\d+)/', @garbage, @matches)) { # match the model names that are just digits.
#      	@model = @matches[1];
#      	# PHILIPS855 ObigoInternetBrowser/2.0
#        # PHILIPS 535 / Obigo Internet Browser 2.0
#      }
#      elseif (preg_match('/^-?(FISIO)\s*(\d+)/', @garbage, @matches)) { # match the FISIO model names.
#      	@model = @matches[1] . @matches[2];
#      	# PHILIPS-FISIO 620/3
#        # PHILIPS-Fisio311/2.1
#        # PHILIPS-FISIO311/2.1
#      }
#      elseif (preg_match('/^-?(XENIUM)/', @garbage, @matches)) { # match the XENIUM model names.
#      	@model = @matches[1];
#      	# PHILIPS-Xenium9@9 UP/4.1.16r
#        # PHILIPS-XENIUM 9@9/2.1
#        # PHILIPS-Xenium 9@9++/3.14
#      }
#      elseif (preg_match('/^-?([^\s\/]+)/', @garbage, @matches)) { # match all other model names that contain no spaces and no slashes.
#        @model = @matches[1];
#        # PHILIPS-Ozeo UP/4
#        # PHILIPS-V21WAP UP/4
#        # PHILIPS-Az@lis288 UP/4.1.19m
#        # PHILIPS-SYSOL2/3.11 UP.Browser/5.0.1.11
#      }
#      if (isset(@model)) {
#        return(array('vendor' => @vendor, 'model' => @model));
#      }
#    }
#
#    # Vitelcom user-agents (used in Spain)
#    elseif (preg_match('/^(Vitelcom)-(Feature Phone)(\d+\.\d+)/', @useragent, @matches)) {
#      # Vitelcom-Feature Phone1.0 UP.Browser/5.0.2.2(GUI)  -- this is a TSM 3 or a TSM 4.
#      return(array('vendor'  => @matches[1],
#                   'model'   => @matches[2],
#                   'version' => @matches[3]));
#    }
#
#    return false;
#  }



#  /**
#   * Determines if this is a Symbian OS Series 60 user-agent string.
#   *
#   * @return boolean
#   */
#  public function isSeries60() {
#    if (!isset(@this->is_series60)) {
#      #  NokiaN-Gage/1.0 SymbianOS/6.1 Series60/1.2 Profile/MIDP-1.0 Configuration/CLDC-1.0
#      #  Mozilla/4.1 (compatible; MSIE 5.0; Symbian OS Series 60 42) Opera 6.0 [fr]
#      @this->is_series60 = preg_match('/\b(Symbian OS Series 60|SymbianOS\/\S+ Series60)\b/', @this->useragent);
#    }
#    return @this->is_series60;
#  }

end
