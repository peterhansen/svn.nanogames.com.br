class Currency < ActiveRecord::Base
  has_many :roy_apple_downloads, :class_name => "AppleDownload", :foreign_key => "royalty_currency"
  has_many :cus_apple_downloads, :class_name => "AppleDownload", :foreign_key => "customer_currency"
  has_many :countries
  
  validates_presence_of :code, :name
  validates_uniqueness_of :code
  validates_length_of :code, :maximum => 3
  
end