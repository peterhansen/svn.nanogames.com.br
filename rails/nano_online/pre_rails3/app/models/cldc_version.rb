class CldcVersion < ActiveRecord::Base
  has_many :devices, :dependent => :nullify
  
  validates_uniqueness_of :version, :case_sensitive => false
end
