# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def formated_date(date)
    if ( SimplyGlobal.locale == :pt_br )
      date.strftime("%d/%m/%Y")
    else
      date.strftime("%m/%d/%Y")
    end
  end

  def to_url_params(params)
    param_list = []
    params.each_pair {|key, value| param_list << "#{key}=#{value}"}
    param_list.join('&')
  end
end