class ApplicationSql < ApplicationController
  def initialize
    
  end
  
  def get_download_application_devices(initial_date, end_date, partner_id )
    if partner_id
      authorized_app_versions = []
      
      Partner.find(partner_id).apps.each do |app|
        app.app_versions.each {|app_version| authorized_app_versions << app_version.id}
      end
      conditions = "app_version_id in ('#{authorized_app_versions.join(", ")}')"
    else
      conditions = ""
    end
    
    downloads = Download.find(:all, :include => [ :app_version, :access, :device ], :conditions => conditions )
    downloads = downloads.select {|obj| obj[:created_at] >=  initial_date && obj[:created_at] <= end_date }
    puts( "PARTNER: #{partner_id}" )

    return downloads
  end  

  def get_access_application_devices(initial_date, end_date, partner_id )
    accesses = Access.all( :include => [ :device ] )
    accesses = accesses.select {|obj| obj[:created_at] >=  initial_date && obj[:created_at] <= end_date }
    puts( "PARTNER: #{partner_id}" )
    return accesses
  end

  def get_nodownload_application_devices(initial_date, end_date, partner_id )
    nodownload = Access.all( :include => [ :device ] )
    nodownload = nodownload.select {|obj| obj[:created_at] >=  initial_date && obj[:created_at] <= end_date }
    puts( "PARTNER: #{partner_id}" )
    return nodownload
  end
end
