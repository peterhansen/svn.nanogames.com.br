#require 'htmlentities'

class GridView
  #propriedades do gridwiew
  cattr_accessor :width
  cattr_accessor :hasPager
  cattr_accessor :pageSize
  cattr_accessor :pagerSize
  cattr_accessor :cellColor
  cattr_accessor :cellFontFace
  cattr_accessor :cellFontColor
  cattr_accessor :cellCssClass 
  cattr_accessor :cellFontWeight 
  cattr_accessor :cellColorAlternate
  cattr_accessor :cellFontColorAlternate
  cattr_accessor :cellFontFaceAlternate
  cattr_accessor :cellCssClassAlternate
  cattr_accessor :cellFontWeightAlternate 
  cattr_accessor :headerColor
  cattr_accessor :headerFontFace
  cattr_accessor :headerFontColor
  cattr_accessor :headerCssClass
  cattr_accessor :headerFontWeight
  cattr_accessor :pagerColor
  cattr_accessor :pagerFontFace
  cattr_accessor :pagerFontColor
  cattr_accessor :pagerCssClass
  cattr_accessor :pagerFontWeight
  cattr_accessor :pagerAlign
  cattr_accessor :emptyDataText

  
  def initialize(name, collection, fields, fieldorder, applicationControler)
    # inicializa as propriedades
    # inicializa a paginação como falsa
    @@hasPager = false
    @@pageSize = 0
    @@pagerSize = 10    
    
    # define as variaveis de inicialização
    # nome do controle
    @name = name    
    
    #pega a aplicação atual    
    @applicationControler = applicationControler
    
    # monta a url de trabalho
    #limpa os requests conhecidos e pega os valores de paginacao      
    know_vars = [name + "pn",name + "ord"]
    new_query_string = ""
    applicationControler.request.query_string.split(/&/).inject({}) do |hash, setting|
      
      key, val = setting.split(/=/)
      if not key.nil? and not val.nil?
        if not know_vars.include?(key)
          new_query_string += key + "=" + val + "&"
        else
          # inicializa a variavel de pagenumber de get no contexto
          if key == name+"pn"
            #pega pagina enviada
            @pageNumber = val.to_i     
          else 
            if key == name+"ord"
              @fieldorder = val
            end
          end
        end
      end
    end
    
    # pagina atual
    
    # pega da sessão caso não tenha enviado
    if not defined? @pageNumber
      page_number = applicationControler.session[name + "gridPageNumber"]
      @pageNumber = page_number
    end
    
    # teste se o pagenumber é nulo
    if @pageNumber.nil?
      @pageNumber = 0
    else
      applicationControler.session[name + "gridPageNumber"] = @pageNumber
    end
    
    #coleção a ser exibida
    @collection = collection

    #campos mostrados
    @fields = fields
    
    # pega campo de ordenação na sessão se ele não existir
      string_order = ""
    if not defined? @fieldorder
      @fieldorder = applicationControler.session[name + "gridOrder"]
      if @fieldorder.nil?
        #pega campo de ordenaão se não existir na sessão e nem por get
        @fieldorder = fieldorder      
      end
    else
      #troca ordem da ordenação se o último fields order for igual ao atual
      if @fieldorder == applicationControler.session[name + "lastFieldOrder"]
        string_order = ".reverse!"
      end
    end

    #testa campo de ordenação inicial
    if not @fieldorder.nil?
      applicationControler.session[name + "gridOrder"] = @fieldorder
      # faz a ordenacao de um objecto dinamico
      @collection = eval("@collection.sort_by{ |a| ( a.nil? ? \'\' : a.#@fieldorder ) || \'\' }#{string_order}")
#      @collection = eval("@collection.sort{ |a,b| ( a && b ) ? (a.#@fieldorder).to_s <=> (b.#@fieldorder).to_s : 0 }#{string_order}")
      applicationControler.session[name + "lastFieldOrder"] = @fieldorder + string_order
    end
    
    #link de postback
    @linkPage = "http://"+ applicationControler.request.host_with_port + applicationControler.request.path + "?" + new_query_string
  end
  
  def render
    # se existir paginação, monta paginação 
    if @@hasPager
      #monta css pager
      pager_style = ""
      pager_style += if @@pagerColor != nil then "background-color:" + @@pagerColor + ";" else "" end
      pager_style += if @@pagerFontColor != nil then "color:" + @@pagerFontColor + ";" else "" end
      pager_style += if @@pagerFontFace != nil then "font-family:" + @@pagerFontFace + ";" else "" end
      pager_style += if @@pagerFontWeight != nil then "font-weight:" + @@pagerFontWeight + ";" else "" end
      pager_class = if @@pagerCssClass != nil then "class=\"#@@pagerCssClass\"" else "class=\"localCellClass\"" end
      @@pagerAlign = if @@pagerAlign != nil then "align=\"" + @@pagerAlign + "\";" else "" end
    
      #numero de pagers
      pager_count_number = (@collection.length / @@pageSize).truncate + 1

      if @pageNumber <= 0
        @pageNumber = 1
      elsif @pageNumber > pager_count_number
        @pageNumber = pager_count_number
      end
      
      # index de inicio da coleção
      ini_record = (@pageNumber - 1) * @@pageSize
      
      #monta o HTML do pager 
      pager_html = "<tr #{pager_class}><td #{pager_class} #@@pagerAlign style=\"#{pager_style}\" colspan='"+@fields.size.to_s+"'>"
      
      # página inicial do pager
      if @pageNumber > (@@pagerSize / 2).floor and (@pageNumber - (@@pagerSize/2).floor) > 1   
        ini_page = @pageNumber - (@@pagerSize / 2).floor
        #habilita navegação multiplicada caso passe de dez páginas
        pager_html += "<a style=\"#{pager_style}\" href='#@linkPage&" + @name + "pn="+(ini_page - 1).to_s+"'><<</a>"
      else
        ini_page = 1
      end
      
      #pagina final do pager
      if (@pageNumber + (@@pagerSize/2).floor) < pager_count_number
        if @pageNumber > (@@pagerSize/2).floor
          end_page = @pageNumber + (@@pagerSize/2).floor
        else
          end_page = @@pagerSize
        end
      else
        end_page = pager_count_number
      end
      
      # links das paginas 
      for i in ini_page..end_page
        #testa se é maior do que o limite
        if i <= (pager_count_number)
          if @pageNumber == i
            pager_html += " #{i} "
          else
            pager_html += "<a style=\"#{pager_style}\" href='#@linkPage&"+@name+"pn=#{i}'> #{i} </a>"
          end
        end
      end

      #testa se existe mais de uma página
      if (pager_count_number <= 1)
        #limpa variável
        pager_html = ""
      end
      
      # habilita navegação multiplicada
      if end_page != pager_count_number
        pager_html += "<a style=\"#{pager_style}\" href='#@linkPage&" + @name + "pn="+(end_page + 1).to_s+"'>>></a>"
      end
      
      # termina o pager
      pager_html += "</td></tr>"
      
      #separa a coleção de paginação

      # slice pode retornar nil -> para evitar problemas, utiliza-se um array vazio nesse caso
      @collection = @collection.slice(ini_record,@@pageSize) || []
    end
    
    # configura largura
    @@width = if not @@width.nil? then "width=\"" + @@width.to_s + "\"" else "" end

    #inicializa a resposta do gridview
    @gridview = "<table border=0 #@@width cellspacing=0 cellpading=0>"
    
    #monta o header
    #monta o css do header
    header_style = ""
    header_style += if @@headerColor != nil then "background-color:" + @@headerColor + ";" else "" end
    header_style += if @@headerFontColor != nil then "color:" + @@headerFontColor + ";" else "" end
    header_style += if @@headerFontFace != nil then "font-family:" + @@headerFontFace + ";" else "" end
    header_style += if @@headerFontWeight != nil then "font-weight:" + @@headerFontWeight + ";" else "" end
    header_class = if @@headerCssClass != nil then "class=\"#@@headerCssClass\"" else "class=\"localHeaderClass\"" end
    
    # monta as colunas do header
    #objeto de conversão para html entities
    coder = HTMLEntities.new

    @gridview += "<tr style=\"#{header_style}\">" 
    @fields.each { |field|
      field.each { |key,value|
        @gridview += "<td #{header_class} style=\"#{header_style}\">&nbsp;<a style=\"#{header_style}\" href='#@linkPage&"+@name+"ord=#{key}'> "+coder.encode(value)+" </a>&nbsp;</td>"
      }
    }
    @gridview += "</tr>"
    
    # monta o relatorio        
    #monta o css do relatorio
    #linha comum
    cell_style = ""
    cell_style += if @@cellColor != nil then "background-color:" + @@cellColor + ";" else "" end
    cell_style += if @@cellFontColor != nil then "color:" + @@cellFontColor + ";" else "" end
    cell_style += if @@cellFontFace != nil then "font-family:" + @@cellFontFace + ";" else "" end
    cell_style += if @@cellFontWeight != nil then "font-weight:" + @@cellFontWeight + ";" else "" end
    cell_class = if @@cellCssClass != nil then "class=\"#@@cellCssClass\"" else "class=\"localCellClass\"" end
    
    #linha alternativa    
    cell_style_alt = ""
    cell_style_alt += if @@cellColorAlternate != nil then "background-color:" + @@cellColorAlternate + ";" else "" end
    cell_style_alt += if @@cellFontColorAlternate != nil then "color:" + @@cellFontColorAlternate + ";" else "" end
    cell_style_alt += if @@cellFontFaceAlternate != nil then "font-family:" + @@cellFontFaceAlternate + ";" else "" end
    cell_style_alt += if @@cellFontWeightAlternate != nil then "font-weight:" + @@cellFontWeightAlternate + ";" else "" end
    cell_class_alt = if @@cellCssClassAlternate != nil then "class=\"#@@cellCssClassAlternate\"" else "class=\"localCellClass\"" end

    @collection.each_with_index do |obj, index|
      line_class = nil
      line_style = nil
      if not index.nil? 
        if ((index % 2) == 0)
          line_style = cell_style
          line_class = cell_class
        else
          line_style = cell_style_alt
          line_class = cell_class_alt
        end
        @gridview += "<tr #{line_class}>"       
        @fields.each do |field| 
          if not field.nil?
            field.each { |key,value|
              # o bloco begin/rescue/end evita que problemas na avaliação do comando (como acesso a um objeto nulo)
              # interfiram na geração dos resultados
              begin
                v = coder.encode( eval( "obj.#{key.to_s}.to_s" ) )
              rescue
                v = ''
              end
              @gridview += "<td #{line_class} style=\"#{line_style}\">&nbsp;#{ v }&nbsp;</td>\n"}
          end
        end
        @gridview += "</tr>" 
      end
    end
    
    # insere o pager, caso exista
    if defined? pager_html
      @gridview += pager_html
    end
    
    # finaliza grid
    @gridview += "</table>"

    # checa se tem registro
    if (@collection.length == 0)
      if (!@@emptyDataText.nil?)
        @gridview = @@emptyDataText
      else
        @gridview = ""
      end
    end

    return @gridview
  end


  def length()
    return @collection.blank? ? 0 : @collection.length
  end

end