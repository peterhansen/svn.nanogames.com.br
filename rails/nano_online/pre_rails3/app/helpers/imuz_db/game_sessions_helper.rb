module ImuzDb::GameSessionsHelper
  def skip_button_class(game)
    game.skips_left? ? "bt_skip" : "bt_skip bt_skip_disabled"
  end

  def skip_button_href(game)
    game.skips_left? ? { :action => :skip_question, :id => game.id } : "#"
  end

  def skip_button(game)
    content_tag :div, :class => skip_button_class(game) do
      link_to 'Skip Question', skip_button_href(game), :method => :post, :id => 'skip_question_button'
    end
  end
  
  def previous_button(action)
   "<a class='bt_back' onclick=\"redirectTo('#{url_for({:controller => 'imuz_db/game_sessions', :action => action})}')\">
      <span>#{action.to_s.camelize}</span>
    </a>"
  end
end
