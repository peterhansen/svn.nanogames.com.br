require 'digest/sha1'

class CustomerLoginController < ApplicationController
  before_filter :set_iphone_format
  before_filter :logged?, :except => [:first_use, :edit, :update]
  layout 'imuz_db'
  
  def index
  #  if session[:customer_id]
  #    @logado = true
  # end
  
  
  
  end

  def first_use
    @app_title = 'app_title'
  end
  

  def login
    @customer = Customer.new
    render :layout => 'imuz_db'
  end 

  def installation
    render :layout => false
  end

  def ajax_login
    reset_session
    session[:current_user_id] = params[:current_user_id].to_i
    render :nothing => true
  end

  def do_login
    reset_session
    if request.post?
      @customer = Customer.new(params[:customer])
      user = Customer.authenticate(params[:customer][:email], params[:customer][:password])
      
      if user
        if user.validated_at
          # armazena somente o id do usuário na sessão, para evitar excesso no tamanho da variável de sessão
          session[:current_user_id] = user.id
          flash[:notice] = "Bem vindo!"
          redirect_to :controller => 'imuz_db/game_sessions', :action => 'home', :remember => params[:remember_me]
      #  else
      #    redirect_to :action => 'reenviar_validacao', :id => user.id
        end
      else
        flash[:errors] = 'Invalid E-mail or Password.'
        render :action => 'login'
      end
    end
  end
   

  def sign_up
    @customer = Customer.new
  end  
  
  def create
    @customer = Customer.new(params[:customer])
    @customer.validated_at = Time.now if @customer.valid?
    if @customer.save
      Emailer.deliver_imuzdb_welcome(@customer, params[:customer][:password], request.host_with_port )
      do_login
    else
      flash[:errors] = 'Please verify the following errors:;;'
      @customer.errors.collect do |name, type| 
        flash[:errors] << "#{name.gsub('first_name', 'full name')}: #{type.gsub(/\([0-9][0-9]?\)/, '')};"        
      end
      render :action => 'sign_up'
      #redirect_to :controller => 'customer_login', :action => 'sign_up'
    end

  end  

  def edit
    @customer = Customer.find(params[:id])
  end
  

  def update
    #params[:customer] = params[:customer].reject{|k, v| k == "password" and v.blank?}
    @customer = Customer.find(params[:id])
#    new_email = @customer.email != params[:customer][:email]
    if @customer.update_attributes(params[:customer])
      if @customer.email_changed?
        Emailer.deliver_imuzdb_welcome(@customer, params[:customer][:password], request.host_with_port )
      end
      redirect_to :controller => 'imuz_db/game_sessions/profile'
    else
      flash[:errors] = 'Please verify the following errors:;;'
      @customer.errors.collect do |name, type| 
        flash[:errors] << "#{name.gsub('first_name', 'full name')}: #{type.gsub(/\([0-9][0-9]?\)/, '')};"        
      end
      render :action => 'edit'
      #redirect_to :controller => 'customer_login', :action => 'sign_up'
    end
  end
  
  def cadastro
    @customer = Customer.new
  end


  def salvar_cadastro
    @customer = Customer.new(params[:customer])
    @customer.validation_hash = create_validation_hash
    
    
    if @customer.save
      @customer.send_email_validation
      return if request.xhr?
      
      flash[:notice] = 'Verifique seu email.'
      render :action => 'index'
    else
      render :action => 'cadastro'
    end
  end
  
  def create_validation_hash
    return Digest::SHA1.hexdigest("#{self.object_id}*\#{@\&KaJDS-\*\&l\=kjjd_as927a")
  end


  def logout
    reset_session
    flash[:notice] = 'Logged out.'
    redirect_to(:action => 'index')
  end


  def validate()
    @customer = Customer.find_by_validation_hash(params[:hash])
    if @customer
      @customer.validated_at = Time.new
      @customer.validation_hash = nil
      @customer.validate_before = nil
      
      if @customer.save
        flash[:notice] = 'E-mail validado com sucesso.'
        redirect_to :action => 'index'
      end
    else
      flash[:notice] = 'Falha na validação do e-mail'
      redirect_to :action => 'index'
    end
  end


  def reenviar_validacao
    @customer = Customer.find(params[:id])
  end


  def reenvio
    @customer = Customer.find(params[:id])
    
    @customer.send_email_validation
    return if request.xhr?
    
    flash[:notice] = "Validação re-enviada para #{@customer.email}."
    redirect_to :action => 'index'
  end
  
  
  # TODO não está aparecendo a mensagem do flash.
  def recuperar_senha
    if request.post?
      if customer = Customer.find_by_email(params[:email])
        customer.reset_password
        
        flash[:notice] = "Sua nova senha foi enviada para seu email.".t
        redirect_to :action => :login
      end
    end
  end
  
  # TODO dá para ficar mais simples?
  private
    def logged?
      redirect_to :controller => 'imuz_db/game_sessions', :action => 'home' if is_iphone_request? and (session[:current_user_id] or session[:uid])  
    end
end
