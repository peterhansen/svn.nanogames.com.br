class AppCustomerDataController < ApplicationController

  before_filter :authorize, :except => [ :save_data, :load_data ]
  before_filter :log_access, :except => [ :save_data, :load_data ]
  before_filter :authorize_nano, :only => [ :save_data, :load_data ]
  before_filter :session_expiry, :except => [ :save_data, :load_data ]

  layout "admin"

  # GET /app_customer_datas
  # GET /app_customer_datas.xml
  def index
    @page_title = 'App customer data'

    if ( session_user.is_admin? )
      @app_customer_datas = AppCustomerData.find( :all,
                                            :include => [:ranking_type, {:app_version => :app}, :customer, {:device => :vendor}],
                                            :order => 'app_version_id, score' ) # TODO ordenar pelo nome da aplicação
    else
      user_app_versions = []
      session_user_apps.each do |a|
        user_app_versions << a.app_versions
      end
      user_app_versions.flatten!

      @app_customer_datas = AppCustomerData.find( :all,
                                            :include => [:ranking_type, {:app_version => :app}, :customer, {:device => :vendor}],
                                            :order => 'app_version_id, score', # TODO ordenar pelo nome da aplicação,
                                            :conditions => { :app_version_id => user_app_versions } )
    end
  end

  def new
    @page_title = 'App customer data'
    @app_versions = AppVersion.all
    @customers = Customer.all
    @devices = Device.all(:order => 'vendor_id, model')
    @ranking_types = RankingType.all
    
    @app_customer_data = AppCustomerData.new
  end

  def edit
    @page_title = 'App customer data'
    @app_versions = AppVersion.all
    @customers = Customer.all
    @devices = Device.all(:order => 'vendor_id, model')
    @ranking_types = RankingType.all

    @app_customer_data = AppCustomerData.find(params[:id])
  end

  def create
    @page_title = 'App customer data'

    @app_customer_data = AppCustomerData.new(params[:app_customer_data])

    if @app_customer_data.save
      flash[:notice] = "Ranking entry #{ @app_customer_data.id } was successfully created."
      redirect_to app_customer_datas_url
    else
      @app_versions = AppVersion.all
      @customers = Customer.all
      @devices = Device.all(:order => 'vendor_id, model')
      @ranking_types = RankingType.all

      render :action => "new"
    end
  end

  def update
    @page_title = 'App customer data'

    @app_customer_data = AppCustomerData.find(params[:id])

      if @app_customer_data.update_attributes(params[:app_customer_data])
        flash[:notice] = "Ranking entry #{ @app_customer_data.id } was successfully updated."
        redirect_to app_customer_datas_url
      else
        @app_versions = AppVersion.all
        @customers = Customer.all
        @devices = Device.all(:order => 'vendor_id, model')
        @ranking_types = RankingType.all
        render :action => "edit"
      end
  end

  def destroy
    @page_title = 'App customer data'

    @app_customer_data = AppCustomerData.find(params[:id])
    @app_customer_data.destroy

    flash[:notice] = "Ranking entry #{ @app_customer_data.id } was successfully destroyed."
    redirect_to(app_customer_datas_url)
  end

  # TODO método erase
  # TODO implementar CRUD

  def save_data()
    render_binary() { |out|
      info = AppCustomerData.filter_params( params, session[ 'nano_client' ] )
      data = AppCustomerData.find_by_customer_id_and_app_version_id_and_sub_type_and_slot(
        info[ :customer_id ], params[ :app ].app_versions, info[ :sub_type ], info[ :slot ]
      )

      out.write_byte( ID_RETURN_CODE )
      begin
        if ( data && !info[ :overwrite ] )
          # achou a entrada, mas não foi permitido sobrescrever
          out.write_short( AppCustomerData::RC_ERROR_NEEDS_OVERWRITING )
        else
          # se não existe, cria; se existe, não faz nada
          data ||= AppCustomerData.new()
          
          data.customer = Customer.find( info[ :customer_id ] )
          data.data = info[ :app_customer_data ]
          data.app_version = params[ :app_version ]
          data.sub_type = info[ :sub_type ]
          data.slot = info[ :slot ]

          data.save!
          out.write_short( RC_OK )
        end
      rescue Exception => e
        logger.error( "#{e} -> #{e.backtrace.join( "\n" )}" )
        out.write_short( AppCustomerData::RC_ERROR_COULDNT_SAVE )
      end
    }
  end

  def load_data()
    render_binary() { |out|
      info = AppCustomerData.filter_params( params, session[ 'nano_client' ] )
      data = AppCustomerData.find_by_customer_id_and_app_version_id_and_sub_type(
        info[ :customer_id ], params[ :app ].app_versions, info[ :sub_type ], info[ :slot ]
      )

      out.write_byte( ID_RETURN_CODE )
      begin
        if ( data )
          out.write_short( RC_OK )
          out.write_byte( ID_SPECIFIC_DATA )
          data.write_data( out )
        else
          # não achou entradas
          out.write_short( AppCustomerData::RC_ERROR_NO_DATA_TO_LOAD )
        end
      rescue Exception => e
        logger.error( "#{e} -> #{e.backtrace.join( "\n" )}" )
        out.write_short( AppCustomerData::RC_ERROR_COULDNT_LOAD )
      end
    }
  end
  
end
