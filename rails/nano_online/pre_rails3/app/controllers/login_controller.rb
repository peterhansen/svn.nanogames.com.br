class LoginController < ApplicationController
  # before_filter :authorize_admin, :except => [ :login, :logout, :index ]
  before_filter :session_expiry, :except => [ :login, :logout ]
  
  layout :user_layout, :except => :login
  
  def login
    # TODO adicionar bloqueio temporário de IP após muitas tentativas frustradas de login
    reset_session()
    if ( request.post? )
      user = User.authenticate( params[ :name ], params[ :password ], params[ :key ] )

      if ( user )
        # armazena somente o id do usuário na sessão, para evitar excesso no tamanho da variável de sessão
        session[ :user_id ] = user.id

        redirect_to( :action => 'index' )
      else
        flash[ :notice ] = 'Invalid user/password/key combination'
        redirect_to( :action => 'login' )
      end
    end
  end


  def logout
    reset_session()
    flash[ :notice ] = 'Logged out.'
    redirect_to( :action => 'login' )
  end


  def index
    redirect_to :controller => :news_feeds if session_user and session_user.name = "imuzdb"
  end


  def redirect_to_nano_games
    authorize()
#    redirect_to( 'http://www.nanogames.com.br' )
  end

  
  def delete_user
    if ( request.post? )
      user = User.find( params[ :id ] )
      
      begin
        user.destroy
        flash[ :notice ] = "User #{ user.name } successfully deleted."
      rescue Exception => e
        flash[ :notice ] = e.message
      end
    end
    
    redirect_to( :action => :list_users )
  end
  
  def list_users
    @all_users = User.find( :all )
  end
end
