class DeviceBugsController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /device_bugs
  # GET /device_bugs.xml
  def index
    @device_bugs = DeviceBug.all( :order => 'id' )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @device_bugs }
    end
  end

  # GET /device_bugs/1
  # GET /device_bugs/1.xml
  def show
    redirect_to :action => 'index'
  end

  # GET /device_bugs/new
  # GET /device_bugs/new.xml
  def new
    @device_bug = DeviceBug.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @device_bug }
    end
  end

  # GET /device_bugs/1/edit
  def edit
    @device_bug = DeviceBug.find(params[:id])
  end

  # POST /device_bugs
  # POST /device_bugs.xml
  def create
    @device_bug = DeviceBug.new(params[:device_bug])

    respond_to do |format|
      if @device_bug.save
        flash[:notice] = "Device bug \##{ @device_bug.id } was successfully created."
        format.html { redirect_to(@device_bug) }
        format.xml  { render :xml => @device_bug, :status => :created, :location => @device_bug }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @device_bug.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /device_bugs/1
  # PUT /device_bugs/1.xml
  def update
    @device_bug = DeviceBug.find(params[:id])

    respond_to do |format|
      if @device_bug.update_attributes(params[:device_bug])
        flash[:notice] = "Device bug \##{ @device_bug.id } was successfully updated."
        format.html { redirect_to(@device_bug) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @device_bug.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /device_bugs/1
  # DELETE /device_bugs/1.xml
  def destroy
    @device_bug = DeviceBug.find(params[:id])
    @device_bug.destroy

    respond_to do |format|
      flash[:notice] = "Device bug \##{ @device_bug.id } was successfully destroyed."
      format.html { redirect_to(device_bugs_url) }
      format.xml  { head :ok }
    end
  end
  
end
