class ResourcesController < ApplicationController
  NANO_ACCESS = [ :get_resource ]

  before_filter :authorize_admin, :except => NANO_ACCESS
  before_filter :authorize_nano, :only => NANO_ACCESS
  before_filter :session_expiry

  layout :user_layout

  def index
    @resources = Resource.find(:all)
  end

  def show
    @resource = Resource.find(params[:id])
  end

  def new
    @resource = Resource.new
    @resource.resource_files.build
  end

  def edit
    @resource = Resource.find(params[:id])
  end

  def create
    @resource = Resource.new(params[:resource])
    
    if @resource.save
      count = 0 # TODO teste - o valor deve ser definido pelo criador do recurso, na view
      puts( "TOTAL: #{@resource.resource_files.length}" )
      if params[:resource_files]
        params[:resource_files].each do |file|
          rf = @resource.resource_files.create(file)
          rf.internal_id = count
              count += 1
              rf.save
        end
      end
    
      flash[:notice] = 'Resource was successfully created.'
      redirect_to(@resource)
    else
      render :action => "new"
    end
  end

  def update
    @resource = Resource.find(params[:id])

    if @resource.update_attributes(params[:resource])
      flash[:notice] = 'Resource was successfully updated.'
      redirect_to(@resource)
    else
      render :action => "edit"
    end
  end

  def destroy
    @resource = Resource.find(params[:id])
    @resource.destroy

    redirect_to(resources_url)
  end

  def delete_file()
    @resource = Resource.find(params[:id])
    if params[ :file_id ]
      @resource_file = ResourceFile.find( params[ :file_id ] )
      @resource_file.destroy() if ( @resource_file )
    end

    render :partial => 'files_list', :locals => { :resource => @resource }
  end
  
  def delete_file_on_new()
    @resource = Resource.find(params[:id])
    @resource_file = ResourceFile.find( params[ :file_id ] )

    @resource_file.destroy() if ( @resource_file )

    render :partial => 'file', :locals => { :resource => @resource }
  end


  ##
  # Atualiza a lista de arquivos do recurso.
  ##
  def refresh_resource_files( update_div = true )
    if ( params[ :id ] )
      @resource ||= Resource.find(params[:id])
    end

    if ( params[ :resource_files ] )
      params[ :resource_files ].each do |attr, value|
        unless value[ :resource_file ].blank?
          new_resource_file = ResourceFile.new
          new_resource_file.resource = @resource
          new_resource_file.filename = value[ :resource_file ]
          new_resource_file.save
        end
      end
    end

    if ( update_div )
      render :partial => 'files_list', :locals => { :resource => @resource }
    end
  end
end
