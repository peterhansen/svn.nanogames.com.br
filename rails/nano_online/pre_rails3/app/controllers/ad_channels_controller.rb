class AdChannelsController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /ad_channels
  # GET /ad_channels.xml
  def index
    @ad_channels = AdChannel.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @ad_channels }
    end
  end

  # GET /ad_channels/1
  # GET /ad_channels/1.xml
  def show
    @ad_channel = AdChannel.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ad_channel }
    end
  end

  # GET /ad_channels/new
  # GET /ad_channels/new.xml
  def new
    @ad_channel = AdChannel.new
    @statuses = AdChannelStatus.find(:all).collect {|a| [a.status, a.id]}

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ad_channel }
    end
  end

  # GET /ad_channels/1/edit
  def edit
    @ad_channel = AdChannel.find(params[:id])
    @ad_channel_versions = AdChannelVersion.all( :conditions => { :ad_channel_id => @ad_channel } )
    @statuses = AdChannelStatus.find(:all).collect {|a| [a.status, a.id]}
  end

  # POST /ad_channels
  # POST /ad_channels.xml
  def create
    @ad_channel = AdChannel.new(params[:ad_channel])

    respond_to do |format|
      if @ad_channel.save
        flash[:notice] = 'AdChannel was successfully created.'
        format.html { redirect_to(@ad_channel) }
        format.xml  { render :xml => @ad_channel, :status => :created, :location => @ad_channel }
      else
        @statuses = AdChannelStatus.find(:all).collect {|a| [a.status, a.id]}
        format.html { render :action => "new" }
        format.xml  { render :xml => @ad_channel.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ad_channels/1
  # PUT /ad_channels/1.xml
  def update
    @ad_channel = AdChannel.find(params[:id])
    # params[ :ad_channel ][ :app_versions ] ||= []

    respond_to do |format|
      if @ad_channel.update_attributes(params[:ad_channel])
        flash[:notice] = 'AdChannel was successfully updated.'
        format.html { redirect_to(@ad_channel) }
        format.xml  { head :ok }
      else
        @statuses = AdChannelStatus.find(:all).collect {|a| [a.status, a.id]}
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ad_channel.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_channels/1
  # DELETE /ad_channels/1.xml
  def destroy
    @ad_channel = AdChannel.find(params[:id])
    @ad_channel.destroy

    respond_to do |format|
      format.html { redirect_to(ad_channels_url) }
      format.xml  { head :ok }
    end
  end


  protected

  ##
  # Atualiza a lista de versões suportadas.
  ##
  def refresh_app_versions( update_div = true )
    if ( params[ :id ] )
      @ad_channel ||= AdChannel.find(params[:id])
    end

    if ( params[ :app_versions ] )
      params[ :app_versions ].each { |attr, value|
        unless value[ :app_version ].blank?
#          new_app_file = AppFile.new
#          new_app_file.app_version = @app_version
#          new_app_file.filename = value[ :app_file ]
#          new_app_file.save
        end
      }

    end

    if ( update_div )
      render :partial => 'app_versions', :locals => { :ad_channel => @ad_channel }
    end
  end
end
