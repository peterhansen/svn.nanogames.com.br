require 'zip/zip'
require 'zip/zipfilesystem'

class DownloadsController < ApplicationController

  CLIENT_ACTIONS = [  :vendor_select,
    :select_app,
    :download_jad,
    :js_download,
    :select_model,
    :device_selected,
    :download_file_by_id,
    :download_file_by_short_name,
    :download_seda_puzzle,
    :select_specific_model,
    :download_axe,
    :download_axe_mobile,
    :download_wallpaper,
    :download_app,
    :download_jar_file
  ]

  before_filter :no_cache, :only => CLIENT_ACTIONS
  before_filter :authorize, :except => CLIENT_ACTIONS
  before_filter :log_access
  before_filter :session_expiry, :except => CLIENT_ACTIONS

  layout :user_layout, :except => CLIENT_ACTIONS

  def index
    if ( session[ :sort_order ].nil? )
      session[ :sort_order ] = 'id'
    end
    @downloads = Download.all( :order => session[ :sort_order ] )
  end

  def show
    redirect_to :action => :index
  end

  def new
    @download = Download.new
  end

  def edit
    @download = Download.find( params[ :id ] )
  end

  def create
    @download = Download.new(params[:download])

    if @download.save
      flash[:notice] = "Download #{ @download.id } was successfully created."
      redirect_to(@download)
    else
      render :action => "new"
    end
  end

  def destroy
    @download = Download.find(params[:id])
    @download.destroy

    flash[:notice] = "Download #{ @download.id } was successfully destroyed."
    redirect_to(downloads_url)
  end

  def update
    @download = Download.find(params[:id])

    if @download.update_attributes(params[:download])
      flash[:notice] = "Download #{ @download.id } was successfully updated."
      redirect_to(@download)
    else
      render :action => "edit"
    end
  end

  def vendor_select
    params[ :app_version ] ||= nil
    session[:selected_vendor] = nil
    session[:layout] = params[:l] if params[:l]
    params[:device] = nil if params[:redetect]

    if params[:device]
      session[:selected_vendor] = params[:device_vendor]
      redirect_to :action => :select_app
    else
      @vendors = Vendor.with_device

      @page_title = 'Nano Games'

      if params[:layout] == "yes"
        session[:layout] = true
      elsif params[:layout] == "no"
        session[:layout] = false
      end

      respond_to do |format|
        format.html {
          if session[:layout] == false
            render :layout => false
          else
            render :layout => default_page_layout
          end
        }
        format.js
      end

    end
  end

  def select_model()
    if params[:vendor_chosen]
      session[:zipped] =
      params[:vendor] = {}
      params[:vendor][:chosen] = params[:vendor_chosen]
    end

    if ( params[ :vendor ] )
      if ( params[ :vendor ][ :chosen ] )
        session[:selected_vendor] = params[ :vendor ][ :chosen ]

        if vendor_exists?

          if big_vendor?
            @model_range = get_model_ranges
          else
            @all_models = get_models
          end

          @page_title = 'Nano Games'

          respond_to do |format|
            format.html {render :layout => default_page_layout}

            format.js {
              if Vendor.find(session[:selected_vendor]).name == "Blackberry"
                session[:zipped] = true
              end
            }
          end
        else
          redirect_to :action => :vendor_select
        end
      else
        @message_change = 'Trocar fabricante'
        redirect_to :back
      end
    else
      redirect_to :action => :vendor_select
    end
  end


  def vendor_exists?
    if Device.find_all_by_vendor_id(session[:selected_vendor])
      true
    else
      false
    end
  end


  def big_vendor?
    Device.find_all_by_vendor_id(session[:selected_vendor]).length > 30
  end


  def get_model_ranges
    models = Device.find_all_by_vendor_id(session[:selected_vendor], :order => :model).map {|device| [device.model, device.id]}

    model_groups = arrange_models_by_group(30, models)

    model_range = Array.new
    model_groups.each_index do |group_index|
      model_range[group_index] = Hash.new
      if model_groups[group_index].first && model_groups[group_index].last
        model_range[group_index]["#{model_groups[group_index].first.first} " + "to".t + " #{model_groups[group_index].last.first}"] = group_index
      end
    end

    return model_range
  end


  def get_models( range = nil )
    if range
      models = Device.find_all_by_vendor_id(session[:selected_vendor], :order => :model).map {|device| [device.model, device.id]}
      model_groups = arrange_models_by_group(30, models)

      return model_groups[range.to_i]
    end
    Device.all( :conditions => { :vendor_id => session[:selected_vendor] }, :order => "model" )
  end


  def arrange_models_by_group(qtd, models)
    group = Array.new
    0.upto((models.length / qtd).ceil) {|c| group[c] = Array.new}
    0.upto(models.length - 1) {|model| group[model/qtd] << models[model]}

    return group
  end


  def device_selected
    if ( params[ :model ] && params[ :model ][ :chosen ] )
      session[ :device_chosen ] = Device.first( :conditions => { :id => params[ :model ][ :chosen ] } )
    else
      session[:device_chosen] = nil
    end
#     mostra a tela de seleção de aplicativos
    respond_to do |format|
      format.js { redirect_to :action => :select_app}
    end
  end

  def select_specific_model
    if request.post? && params[:range][:chosen]
      @models = get_models(params[:range][:chosen])

      if @models
        respond_to do |format|
          format.html {
            if session[:layout] == "no"
              render :layout => false
            else
              render :layout => default_page_layout
            end
          }
          format.js
        end
        return
      end
    end

    redirect_to :action => :vendor_select
  end

  def select_app( first_run = true )
    if ( params[ :model ] && params[ :model ][ :chosen ] )
      session[ :device_chosen ] = Device.first( :conditions => { :id => params[ :model ][ :chosen ] } )
    else
      session[:device_chosen] = nil unless session[:device_chosen]
    end

#     TODO verificar leitura de parâmetro indicando qual aplicativo se está tentando baixar. Exemplo: downloads?short=LUXG
    @supported_versions = []
    begin
      @vendor = Vendor.find_by_name(session[:selected_vendor])

#       se for a primeira chamada desse método, armazena o aparelho detectado. Caso contrário, armazena o aparelho escolhido.
#      session[ :device_chosen ] = params[ :device ] if ( first_run )
    rescue Exception => e
      redirect_to :action => :vendor_select
      return
    end

    if ( session[ :device_chosen ] )
      # aparelho detectado; procura aplicativos que o suportem
      state_available = AppVersionState.find_by_name( 'Available' )

      if session[ :device_chosen ].family_id
        @supported_versions = AppVersion.all(   :include => [:app, :families],
                                                :conditions => "families.id = #{session[ :device_chosen ].family_id} && app_version_state_id = #{state_available.id}")

        @supported_versions = @supported_versions.sort_by { |v| v.app_version_number }
      end

      # remove as versões duplicadas e ordena por nome
      @supported_versions.uniq!
      @supported_versions = @supported_versions.sort_by { |v| v.app_version_number }

      if ( @supported_versions.blank? )
        # mostra página indicando que não há aplicativos disponíveis, e armazena a informação
        respond_to do |format|
          format.html {
              if session[:layout] == "no"
                render :action => :device_unsupported, :layout => false
              else
                render :action => :device_unsupported, :layout => default_page_layout
              end
            }
          format.js { render :action => "device_unsupported.js.rjs" }
        end
      else
        @page_title = 'Nano Games'

        respond_to do |format|
          format.html {
            if session[:layout] == "no"
              render :action => :select_app, :layout => false
            else
              render :action => :select_app, :layout => default_page_layout
            end
          }
          format.js
        end

      end
    else
      # aparelho não encontrado - exibe a seleção de aparelho (com o fabricante detectado, se for o caso)
      redirect_to :action => :vendor_select
    end
  end

  ##
  # /downloads/download_jad?app=BOAD&model=822488299
  ##
  def download_jad( send_jad_inline = true )
    begin
			session[ :device_chosen ] ||= params[ :device ]

      if params[:model]
        session[ :device_chosen ] = model = Device.find(params[:model])
        params[:app_version] = AppVersion.first(:include => [:app, :families], :conditions => ["apps.name_short = ? AND families.id = ?", params[:app], model.family_id]).id
      else
        params[ :app_version ] = params[ :download ][ :chosen ] if params[ :app_version ].nil?
      end

      download = Download.new({:app_version_id => params[ :app_version ],
                               :access => session[ :last_access ],
                               :device => session[ :device_chosen ]})

      filename = "#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}"

      if ( File::exists?( filename ) )
        if ( download.save )
          # cria um novo arquivo .jad contendo os novos links para os arquivos binários (não é necessário salvá-lo, pois é enviado diretamente para o usuário)
          jad_file = JadFile.new( filename )
          # atualiza MIDlet-Install-Notify e MIDlet-Delete-Notify
          jad_file.info[ 'MIDlet-Install-Notify' ] = "#{URL_NANO_ONLINE}/notify/i/#{download.id}"
          jad_file.info[ 'MIDlet-Delete-Notify' ] = "#{URL_NANO_ONLINE}/notify/d/#{download.id}"

          # cria os códigos de download de cada arquivo binário
          # TODO no caso do BlackBerry, não é necessário o arquivo .jar (somente os .cod)
#
#          if session[ :device_chosen ].vendor.name == "Blackberry"
#            download_zipped(download)
#          end

          if(!params[:zipped].blank? && params[:zipped] ==  "yes")
            download_zipped(download)
          else
            jad_file.file_paths do |key, value|
              download_code = DownloadCode.new( { :download => download, :file => value } )

              if ( download_code.save )
                session[:download_code] = download_code.code
                jad_file.info[ key ] = "#{URL_NANO_ONLINE}/download/#{download_code.code}"
              else
                logger.error( "error saving download code: #{download_code}" )
              end
            end
          end

          if ( send_jad_inline )
            # envia diretamente o arquivo .jad
            respond_to do |format|
              format.html {
                          send_data(jad_file.data,
                                    :type       => default_content_type( 'jad' ),
                                    :filename   => "#{download.app_version.app_version_number_clean}.jad" )}
              format.js {
                        render}
            end
          end
        else
          logger.error( "error saving download: #{download.errors}" )
          respond_to do |format|
            format.html {render( :template => 'server_error', :layout => default_page_layout )}
            format.js {render :action => 'server_error.js.rjs'}
          end
        end
      else
        logger.error( "File not found: #{filename}" )
        # TODO redirecionar o usuário para página padrão de erro
        respond_to do |format|
          format.html {render( :template => 'server_error', :layout => default_page_layout )}
          format.js {render :action => 'server_error.js.rjs'}
        end
      end
    end
  end

  def download_zipped(download)
    t = Tempfile.new("temp_zip_#{request.remote_ip}")

    if session[ :device_chosen ].vendor.name == "Blackberry"

      send_file   "#{PATH_RELEASES}#{download.app_version.file_bt_cod.filename.url}",
                  :disposition    => 'attachment',
                  :filename       => "bt_#{download.app_version.app_version_number_clean}.cod"
      return
    else
      begin
        Zip::ZipOutputStream.open(t.path) do |zip|

          zip.put_next_entry("#{download.app_version.app.name}.jad")
          zip.print IO.read("#{PATH_RELEASES}#{download.app_version.file_jad.filename.url}")

          jad_file.file_paths do |key, value|
            zip.put_next_entry("#{value}")
            zip.print IO.read("#{PATH_RELEASES}/#{download.app_version.app_version_number_clean}/#{value}")
          end
        end

        send_file t.path,
                  :type           => 'application/zip',
                  :disposition    => 'attachment',
                  :filename       => "#{download.app_version.app_version_number_clean}.zip"
        # redirect_to :device_select
        return
      rescue
        respond_to do |format|
          format.html {render( :template => 'server_error', :layout => default_page_layout )}
          format.js {render :action => 'server_error.js.rjs'}
        end
        return
      end
    end
  end

  ##
  # Permite acesso a um arquivo através de um código de download, que só pode ser utilizado uma vez.
  ##
  def download_file_by_id()
    params[:id] = session[:download_code] if session[:download_code]
    download_code = DownloadCode.first( :conditions => { :code => params[ :id ] } )

    if ( download_code )
#       o código não pode ter sido usado e o user-agent desta conexão deve ser o mesmo do pedido de download
#       TODO além do user-agent, testar IP e/ou número de telefone?
#       if ( @download_code.download.user_agent == request.user_agent )
        if ( download_code.used_at.nil? || download_code.used_at.blank? )
          unless ( request.user_agent.match( /MDS_\d*\.\d+\.(\d+\.)?(\d+)?/ ) )
            download_code.used_at = Time.download_code
            now.save()
          end

          send_file(    "#{PATH_RELEASES}/#{download_code.download.app_version.app_version_number_clean}/#{download_code.file}",
                        :type => default_content_type( File.extname( download_code.file ) ) )
        else
          logger.error( "código de download já utilizado: #{download_code.code} em #{download_code.used_at}" )
          redirect_to( :controller => :downloads, :action => :vendor_select )
        end
#      else
#        logger.error( "download code UA != download UA" )
#      end
    else
      logger.error( "código de download inválido: #{params[ :id ]}" )
      redirect_to( :controller => :downloads, :action => :vendor_select )
    end
  end


  def download_file_by_short_name()
    if ( params[ :app_short_name ] )
      @app = App.find_by_name_short( params[ 'app_short_name' ] )
      @app_versions = AppVersion.find_all_by_app_id( @app, :conditions => { :app_version_state_id => AppVersionState.find_by_name( 'Available' ) } )

      @app_versions.each() { |app_version|
        devices = app_version.devices()

        if ( devices.include?( params[ :device ] ) )
          params[ :app_version ]    = app_version.id
          session[ :device_chosen ] = params[ :device ]

          download_jad()
          return
        end
      }

    end

    render( :action => :device_unsupported_short, :layout => default_page_layout )
  end


  def download_seda_puzzle()
    app = App.find_by_name_short( 'SEDP' )
    app_versions = AppVersion.find_all_by_app_id( app, :conditions => { :app_version_state_id => AppVersionState.find_by_name( 'Available' ) } )

    app_versions.each() { |app_version|
      devices = app_version.devices()

      if ( devices.include?( params[ :device ] ) )
        params[ :app_version ]    = app_version.id
        session[ :device_chosen ] = params[ :device ]

        download_jad()
        return
      end
    }

    render( :action => 'seda_unsupported', :layout => default_page_layout )
  end

  def download_axe_mobile
    app = App.find_by_name_short( 'AXET' )
    app_versions = AppVersion.find_all_by_app_id( app, :conditions => { :app_version_state_id => AppVersionState.find_by_name( 'Available' ) } )

    app_versions.each() { |app_version|
      devices = app_version.devices()

      if ( devices.include?( params[ :device ] ) )
        params[ :app_version ]    = app_version.id
        session[ :device_chosen ] = params[ :device ]

        download_jad()
        return
      end
    }
    render :action => :axe_unsupported, :layout => "apps/axe"

    # redirect_to( "http://www.axe.com.br/twist/landing/#twist-the-dice/download" )
    # redirect_to( "http://wap.nanogames.com.br" )
  end

  def download_axe
    params[:zipped] = "yes"
    if vendor = Vendor.find_by_name(params[:vendor])
      if device = Device.find_by_vendor_id_and_model(vendor.id, params[:model])
        app = App.find_by_name_short( 'AXET' )
        app_version = app.find_version_with_support_to(device)

        if app_version.instance_of?(Array)
          params[ :app_version ]    = app_version.first.id
        else
          params[:app_version] = app_version.id
        end

        session[ :device_chosen ] = device
        download_jad
     end
   end

  end
  def download_wallpaper
    send_file "#{RAILS_ROOT}/public/images/#{params[:size]}.png", :type => 'image/png', :filename => "Axe Wallpaper.png"
  end

  def download_home
    @vendors = Vendor.all( :order => "name" )

    render :layout => false
  end

  def select_m_remote
    render :update do |page|
      page.replace_html 'download_form', :partial => "select_model_remote"
    end
  end

  def download_app
    @app = App.find_by_name_short(params[:id])
  end

  # /downloads/download_jar_file?app=BOAD&model=822488299
  def download_jar_file
    model = Device.find(params[:model])
    @app_version = AppVersion.first(:include => [:app, :families], :conditions => ["apps.name_short = ? AND families.id = ?", params[:app], model.family_id])

    if model.vendor.name == 'Blackberry'
      send_file   PATH_RELEASES + @app_version.file_bt_cod.filename.url
    else
      filename = PATH_RELEASES + @app_version.file_jad.filename.url

      JadFile.new(filename).file_paths do |key, value|
        send_file "#{PATH_RELEASES}/#{@app_version.app_version_number_clean}/#{value}"
        return
      end
    end
  end
end