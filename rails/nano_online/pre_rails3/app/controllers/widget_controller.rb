class WidgetController < ApplicationController

  FB_APP_ID = 'a927367872e36d819cf17f80e1030c10'
  FB_SECRET = '29b8638266642298b230090cc8dc72cb'
  REDIRECT_URL = 'http://staging.nanogames.com.br/widget/facebook_callback'
  
  require 'digest/sha1'

  def get_nano_key
    user = WidgetUser.new
    user.access_key = Digest::SHA1.hexdigest(session.session_id)
    user.save

    render :text => user.access_key
  end

  def get_facebook_key
    if user = WidgetUser.find_by_access_key(params[:access_key])
      unless user.fb_access_token.blank?
        render :text => user.fb_access_token
      else
        render :text => 'false'
      end
    else
      render :text => 'invalid_key'
    end
  end

  def facebook_callback
    if referer = request.env["QUERY_STRING"]
      referer.match(/nano_key=(.+)/)
      key = $1[0..39]

      if user = WidgetUser.find_by_access_key(key)
        user.fb_code = params[:code]
    
        fb_access = client.web_server.get_access_token(params[:code], :redirect_uri => 'http://www.nanogames.com.br/widget/facebook_callback?nano_key=' + key)
        data = JSON.parse(fb_access.get('/me'))
        
        user.fb_access_token = fb_access.token
        user.save
          
        render :text => "Por favor, reinicie o Niver Lembrator para sincronizar seus contados."
      else
        render :text => 'Invalid Key'
      end
    else
      render :text => "Error on http_referer/n/nRequest:/n#{request.env}"
    end
  end

  def get_friends
    if user = WidgetUser.find_by_access_key(params[:access_key])
      if user.fb_access_token

        fb_access = OAuth2::AccessToken.new(client, user.fb_access_token)

        render :json => fb_access.get('/me/friends?fields=name,email,birthday')
        return
      else
        render :text => 'false'
        return
      end
      render :text => 'false'
    end
  end

  def client
    OAuth2::Client.new(FB_APP_ID, FB_SECRET, :site => 'https://graph.facebook.com')
  end
end
