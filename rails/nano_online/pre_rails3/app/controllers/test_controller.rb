class TestController < ApplicationController

  def detect
    @device = get_device( true )
    headers = request.env.select {|k,v| k.match(".*") }
#    for header in headers
    begin
      @user_agent = UaProfiler::UserAgent.new(request)
    rescue Exception => e
      ExceptionNotifier.deliver_exception_notification(e, self, request)
    end
    # render :text => "BLA! #{request.method}"
   render :layout => default_page_layout
    # se quiser redirecionar, é só trocar a linha anterior por essa:
#    redirect_to "http://www.google.com.br"
  end

end
