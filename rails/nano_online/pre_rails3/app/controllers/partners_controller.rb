class PartnersController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /partners
  # GET /partners.xml
  def index
    @partners = Partner.find( :all, :order => 'name', :include => [:users, :apps] )

    @partner_users = {}
    @partners.each { |partner|
      users = partner.users.collect { |u| u.name }
      @partner_users[ partner ] = users.join( ', ' )
    }

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @partners }
    end
  end


  # GET /partners/1
  # GET /partners/1.xml
  def show
    redirect_to :action => :index
  end


  # GET /partners/new
  # GET /partners/new.xml
  def new
    @partner = Partner.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @partner }
    end
  end


  # GET /partners/1/edit
  def edit
    @partner = Partner.find(params[:id])
  end


  # POST /partners
  # POST /partners.xml
  def create
    @partner = Partner.new(params[:partner])

    respond_to do |format|
      if @partner.save
        flash[:notice] = "Partner #{ @partner.name } was successfully created."
        format.html { redirect_to partners_url }
        format.xml  { render :xml => @partner, :status => :created, :location => @partner }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @partner.errors, :status => :unprocessable_entity }
      end
    end
  end


  # PUT /partners/1
  # PUT /partners/1.xml
  def update
    @partner = Partner.find(params[:id])

    respond_to do |format|
      if @partner.update_attributes(params[:partner])
        flash[:notice] = "Partner #{ @partner.name } was successfully updated."
        format.html { redirect_to(@partner) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @partner.errors, :status => :unprocessable_entity }
      end
    end
  end


  # DELETE /partners/1
  # DELETE /partners/1.xml
  def destroy
    @partner = Partner.find(params[:id])
    @partner.destroy

    respond_to do |format|
      flash[:notice] = "Partner #{ @partner.name } was successfully destroyed."
      format.html { redirect_to(partners_url) }
      format.xml  { head :ok }
    end
  end

end

