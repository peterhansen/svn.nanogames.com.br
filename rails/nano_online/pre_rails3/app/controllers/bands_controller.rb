class BandsController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /bands
  # GET /bands.xml
  def index
    @bands = Band.find( :all,
                        :order => 'name, frequency' )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @bands }
    end
  end

  # GET /bands/1
  # GET /bands/1.xml
  def show
    redirect_to :action => :index
  end

  # GET /bands/new
  # GET /bands/new.xml
  def new
    @band = Band.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @band }
    end
  end

  # GET /bands/1/edit
  def edit
    @band = Band.find(params[:id])
  end

  # POST /bands
  # POST /bands.xml
  def create
    @band = Band.new(params[:band])

    respond_to do |format|
      if @band.save
        flash[:notice] = "Band #{ @band.name + ' ' + @band.frequency.to_s } was successfully created."
        format.html { redirect_to(@band) }
        format.xml  { render :xml => @band, :status => :created, :location => @band }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @band.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /bands/1
  # PUT /bands/1.xml
  def update
    @band = Band.find(params[:id])

    respond_to do |format|
      if @band.update_attributes(params[:band])
        flash[:notice] = "Band #{ @band.name + ' ' + @band.frequency.to_s } was successfully updated."
        format.html { redirect_to(@band) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @band.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /bands/1
  # DELETE /bands/1.xml
  def destroy
    @band = Band.find(params[:id])
    @band.destroy

    respond_to do |format|
      flash[:notice] = "Band #{ @band.name + ' ' + @band.frequency.to_s } was successfully destroyed."
      format.html { redirect_to(bands_url) }
      format.xml  { head :ok }
    end
  end
  
end
