class OptionalApisController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout

  def index
    @optional_apis = OptionalApi.all( :order => 'id' )
  end

  # GET /optional_apis/1
  # GET /optional_apis/1.xml
  def show
    redirect_to :action => 'index'
  end

  # GET /optional_apis/new
  # GET /optional_apis/new.xml
  def new
    @optional_api = OptionalApi.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @optional_api }
    end
  end

  # GET /optional_apis/1/edit
  def edit
    @optional_api = OptionalApi.find(params[:id])
  end

  # POST /optional_apis
  # POST /optional_apis.xml
  def create
    @optional_api = OptionalApi.new(params[:optional_api])

    respond_to do |format|
      if @optional_api.save
        flash[:notice] = "Optional API #{ @optional_api.name } was successfully created."
        format.html { redirect_to(@optional_api) }
        format.xml  { render :xml => @optional_api, :status => :created, :location => @optional_api }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @optional_api.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /optional_apis/1
  # PUT /optional_apis/1.xml
  def update
    @optional_api = OptionalApi.find(params[:id])

    respond_to do |format|
      if @optional_api.update_attributes(params[:optional_api])
        flash[:notice] = "Optional API #{ @optional_api.name } was successfully updated."
        format.html { redirect_to(@optional_api) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @optional_api.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /optional_apis/1
  # DELETE /optional_apis/1.xml
  def destroy
    @optional_api = OptionalApi.find(params[:id])
    @optional_api.destroy

    respond_to do |format|
      flash[:notice] = "Optional API #{ @optional_api.name } was successfully destroyed."
      format.html { redirect_to(optional_apis_url) }
      format.xml  { head :ok }
    end
  end
  
end
