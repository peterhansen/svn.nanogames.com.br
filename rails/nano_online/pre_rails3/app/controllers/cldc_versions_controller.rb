class CldcVersionsController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /cldc_versions
  # GET /cldc_versions.xml
  def index
    @cldc_versions = CldcVersion.find( :all, :order => 'version' )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @cldc_versions }
    end
  end

  # GET /cldc_versions/1
  # GET /cldc_versions/1.xml
  def show
    redirect_to :action => :index
  end

  # GET /cldc_versions/new
  # GET /cldc_versions/new.xml
  def new
    @cldc_version = CldcVersion.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cldc_version }
    end
  end

  # GET /cldc_versions/1/edit
  def edit
    @cldc_version = CldcVersion.find(params[:id])
  end

  # POST /cldc_versions
  # POST /cldc_versions.xml
  def create
    @cldc_version = CldcVersion.new(params[:cldc_version])

    respond_to do |format|
      if @cldc_version.save
        flash[:notice] = "CLDC version #{ @cldc_version.version } was successfully created."
        format.html { redirect_to(cldc_versions_url) }
        format.xml  { render :xml => @cldc_version, :status => :created, :location => @cldc_version }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @cldc_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /cldc_versions/1
  # PUT /cldc_versions/1.xml
  def update
    @cldc_version = CldcVersion.find(params[:id])

    respond_to do |format|
      if @cldc_version.update_attributes(params[:cldc_version])
        flash[:notice] = "CLDC version #{ @cldc_version.version } was successfully updated."
        format.html { redirect_to(cldc_versions_url) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @cldc_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /cldc_versions/1
  # DELETE /cldc_versions/1.xml
  def destroy
    @cldc_version = CldcVersion.find(params[:id])
    @cldc_version.destroy

    respond_to do |format|
      flash[:notice] = "CLDC version #{ @cldc_version.version } was successfully destroyed."
      format.html { redirect_to(cldc_versions_url) }
      format.xml  { head :ok }
    end
  end
  
end
