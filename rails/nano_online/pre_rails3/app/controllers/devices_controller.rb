class DevicesController < ApplicationController
  before_filter :authorize_admin, :except => :list
  before_filter :session_expiry

  layout :user_layout

  def index
    @devices = Device.find( :all,
                            :order => 'vendor_id, model',
                            :include => [:vendor, :midp_version, :cldc_version, :family] )
  end

  def list
    @devices = if (params[:app] && params[:vendor])
      app = App.find_by_name_short(params[:app])
      Device.all(:include => { :family => { :app_versions => :app } }, :conditions => ["apps.id = ? AND vendor_id = ?", app, params[:vendor] ], :order => 'devices.model')
    else
      Device.all( :order => 'model' )
    end

    respond_to do |format|
      format.json { render :json => @devices.to_json(:only => [:id], :methods => :name)}
    end
  end

  def show
    begin
      @device = Device.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid device id #{params[:id]}" )
      flash[:notice] = "Invalid device"
      redirect_to :action => :index
    end
  end

  def new
    @device = Device.new
  end

  def edit
    @device = Device.find(params[:id])
  end

  def create
    @device = Device.new(params[:device])

    if @device.save
      flash[:notice] = "Device #{ @device.model } was successfully created."
      redirect_to(@device)
    else
      render :action => "new"
    end
  end

  def update
    params[ :device ][ :band_ids ] ||= []
    params[ :device ][ :data_service_ids ] ||= []
    params[ :device ][ :optional_api_ids ] ||= []
    @device = Device.find(params[:id])

    if @device.update_attributes(params[:device])
      flash[:notice] = "Device #{ @device.model } was successfully updated."
      redirect_to(@device)
    else
      render :action => "edit"
    end
  end

  def destroy
    @device = Device.find(params[:id])
    begin
      if @device.destroy
        flash[:notice] = "Device #{ @device.model } was successfully destroyed."
      else
        flash[:notice] = "Device #{ @device.model } could not be destroyed."
      end
    rescue Exception => e
      flash[:notice] = "An error stopped #{ @device.model } from been deleted."
    ensure
      redirect_to :action => :index
    end
  end
end
