class MidpVersionsController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /midp_versions
  # GET /midp_versions.xml
  def index
    @midp_versions = MidpVersion.find( :all, :order => 'version' )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @midp_versions }
    end
  end

  
  # GET /midp_versions/1
  # GET /midp_versions/1.xml
  def show
    redirect_to :action => :index
  end

  
  # GET /midp_versions/new
  # GET /midp_versions/new.xml
  def new
    @midp_version = MidpVersion.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @midp_version }
    end
  end

  
  # GET /midp_versions/1/edit
  def edit
    @midp_version = MidpVersion.find(params[:id])
  end
  
  
  # POST /midp_versions
  # POST /midp_versions.xml
  def create
    @midp_version = MidpVersion.new(params[:midp_version])

    respond_to do |format|
      if @midp_version.save
        flash[:notice] = "MIDP version #{ @midp_version.version } was successfully created."
        format.html { redirect_to(@midp_version) }
        format.xml  { render :xml => @midp_version, :status => :created, :location => @midp_version }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @midp_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # PUT /midp_versions/1
  # PUT /midp_versions/1.xml
  def update
    @midp_version = MidpVersion.find(params[:id])

    respond_to do |format|
      if @midp_version.update_attributes(params[:midp_version])
        flash[:notice] = "MIDP version #{ @cldc_version.version } was successfully updated."
        format.html { redirect_to(@midp_version) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @midp_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  
  # DELETE /midp_versions/1
  # DELETE /midp_versions/1.xml
  def destroy
    @midp_version = MidpVersion.find(params[:id])
    @midp_version.destroy

    respond_to do |format|
      flash[:notice] = "MIDP version #{ @midp_version.version } was successfully destroyed."
      format.html { redirect_to(midp_versions_url) }
      format.xml  { head :ok }
    end
  end
  
end
