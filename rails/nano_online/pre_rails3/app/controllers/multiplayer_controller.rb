#class MultiplayerController < ApplicationController
#  before_filter :authorize_nano
#  before_filter :parse_params
#
#  def enter_game
#    # verifica se o usuário está cadastrado.
#    if player = Customer.find(params[:player_id])
#      # não faz nada se o usuário já estiver em uma partida.
#      unless Participation.active.find_by_customer_id(player.id)
#        app_version = params[ :app_version ]
#
#        # buscando 0 lobby para a versão de multiplayer do app_version passado
#        # TODO - criar automaticamente o lobby para as versões que ainda não tiverem um.
#        lobby = Lobby.find_by_multiplayer_version_id(app_version.multiplayer_version_id)
#
#        # verifica se o usuário já está no lobby
#        unless lobby.customers.include?(player)
#          lobby.customers << player
#        end
#
#        # se o lobby não contiver usuários suficientes para começar uma partida.
#        # TODO - verificar no MultiplayerVersion qual a quantidade de jogadores.
#        if lobby.customers.size < 2
#          render_binary do |out|
#            out.write_byte(PARAM_WAITING_OPONENTS)
#          end
#        else # caso contrario. Inicia a partida.
#          match = lobby.begin_match
#
#          render_binary do |out|
#            out.write_byte(PARAM_MATCH_ID)
#            out.write_short(match.id)
#            out.write_byte(PARAM_N_PLAYERS)
#            out.write_byte(2)
#
#            match.participations.each do |p|
#              out.write_byte(PARAM_PLAYER_ID)
#              out.write_int(p.customer.id)
#              out.write_byte(PARAM_PLAYER_ORDER)
#              out.write_int(p.order)
#              out.write_byte(PARAM_PLAYER_INFO_END)
#            end
#          end
#        end
#
#      else
#        render_binary do |out|
#          out.write_byte(PARAM_ALREADY_IN_MATCH)
#        end
#      end
#    else
#      render_binary do |out|
#        out.write_byte(PARAM_PLAYER_NOT_FOUND)
#      end
#    end
#  end
#
#  ##
#  # Registra a jogada do jogador atual e verifica se existe resposta do adversário
#  ##
#  def action(player_id = params[:player_id], match_id = params[:match], turn_number = params[:turn], action = params[:action])
#    # se o jogador existir. Retorna o jogador(player), a partida atual(player.matches.first) e os turnos da partida atual(player.matches.first.turns).
#    if player = Customer.find(player_id, :include => {:matches => :turns}, :conditions => ['participations.match_id = ?', match_id])
#      match = player.matches.first
#
#      # se não existirem turnos na partida cria-se o primeiro
#      if match.turns.size == 0
#        turn = match.new_turn(0)
#      else
#        # pega o turno que foi passado pelo parametro
#        turn = match.turns.select {|t| t.number == turn_number.to_i}.first
#
#        # testa se exite o determinado turno na partida
#        unless turn.blank?
#          # testa se o turno enviado é válido
#          if turn.ended
#            render_binary do |out|
#              out.write_byte(PARAM_WRONG_TURN_NUMBER)
#            end
#            return
#          end
#        else
#          # se o turno não existir na partida atual
#          turn = match.new_turn(turn_number)
#        end
#      end
#
#      # verifica se o jogador já enviou uma jogada nesse turno
#      old_action = turn.actions.select {|act| act.customer_id == player.id}
#      if old_action.blank?
#        # se não tiver enviado, registra a ação atual do jogador
#        action = turn.actions.build(:customer_id => player.id,
#                                    :turn_id => turn.id,
#                                    :move => action)
#      else
#        # se já tiver enviado
#        old_action.first.move = action
#        old_action.save
#        turn.reload
#      end
#
#
#      if turn.save
#        # checa se existe resposta do adiversário
#        unless turn.actions.select {|act| act.customer_id != player.id}.blank?
#          turn.ended = true
#          turn.save
#          match.new_turn(turn_number.to_i + 1)
#
#          render_binary do |out|
#            out.write_byte(PARAM_N_ACTIONS)
#            out.write_int(turn.actions.length)
#
#            turn.actions.each do |action|
#              out.write_byte(PARAM_PLAYER_ID)
#              out.write_int(action.customer.id)
#              out.write_byte(PARAM_ROUND_DATA)
#              out.write_int(action.move)
#            end
#          end
#        else
#          render_binary do |out|
#            out.write_byte(PARAM_WAITING_ACTIONS)
#          end
#        end
#      else
#        render_binary do |out|
#          out.write_byte(PARAM_ERROR_SAVING_TURN)
#        end
#      end
#    else
#      render_binary do |out|
#        out.write_byte(PARAM_INVALID_PLAYER_OR_MATCH)
#      end
#    end
#  end
#
#  def reset
#    session[:match] = nil
#    Participation.all.each {|p| p.destroy}
#    Match.all.each {|m| m.destroy}
#    Action.all.each {|a| a.destroy}
#    Turn.all.each {|t| t.destroy}
#
#    render :text => "Jogo resetado."
#  end
#
#protected
#
#  def parse_params
#    parse_data(params[ID_SPECIFIC_DATA]) do |stream, id|
#      case id
#        when PARAM_PLAYER_ID
#          params[:player_id] = stream.read_int()
#        when PARAM_MATCH_ID
#          params[:match_id] = stream.read_int()
#        when PARAM_TURN_INDEX
#          params[:turn_number] = stream.read_short()
#        when PARAM_ROUND_DATA
#          params[:action] = stream.read_byte()
#      end
#    end
#  end
#end
