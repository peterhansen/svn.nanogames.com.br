class AdPieceVersionsController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /ad_piece_versions
  # GET /ad_piece_versions.xml
  def index
    @ad_piece_versions = AdPieceVersion.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @ad_piece_versions }
    end
  end

  # GET /ad_piece_versions/1
  # GET /ad_piece_versions/1.xml
  def show
    @ad_piece_version = AdPieceVersion.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @ad_piece_version }
    end
  end

  # GET /ad_piece_versions/new
  # GET /ad_piece_versions/new.xml
  def new
    @ad_piece_version = AdPieceVersion.new
		@ad_piece_version.resources.build

		@ad_pieces = AdPiece.find(:all, :order => "title")
		@ad_channel_versions = AdChannelVersion.find( :all )

		@resources = Resource.all() # TODO filtrar

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @ad_piece_version }
    end
  end

  # GET /ad_piece_versions/1/edit
  def edit
    @ad_piece_version = AdPieceVersion.find(params[:id])
		@ad_pieces = AdPiece.find(:all, :order => "title")
		@ad_channel_versions = AdChannelVersion.find( :all )

		@resources = Resource.all() # TODO filtrar
  end

  # POST /ad_piece_versions
  # POST /ad_piece_versions.xml
  def create
    @ad_piece_version = AdPieceVersion.new(params[:ad_piece_version])

		count = 0 # TODO teste - o valor deve ser definido pelo criador do ad_piece_version, na view
		puts( "TOTAL: #{@ad_piece_version.resources.length}" )
    @ad_piece_version.resources.each do |res|
#      res.ad_piece_version= @ad_piece_version
			res.internal_id = count
			count += 1
    end

    respond_to do |format|
      if @ad_piece_version.save
        flash[:notice] = 'AdPieceVersion was successfully created.'
        format.html { redirect_to(@ad_piece_version) }
        format.xml  { render :xml => @ad_piece_version, :status => :created, :location => @ad_piece_version }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @ad_piece_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /ad_piece_versions/1
  # PUT /ad_piece_versions/1.xml
  def update
    @ad_piece_version = AdPieceVersion.find(params[:id])

    respond_to do |format|
      if @ad_piece_version.update_attributes(params[:ad_piece_version])
        flash[:notice] = 'AdPieceVersion was successfully updated.'
        format.html { redirect_to(@ad_piece_version) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @ad_piece_version.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /ad_piece_versions/1
  # DELETE /ad_piece_versions/1.xml
  def destroy
    @ad_piece_version = AdPieceVersion.find(params[:id])
    @ad_piece_version.destroy

    respond_to do |format|
      format.html { redirect_to(ad_piece_versions_url) }
      format.xml  { head :ok }
    end
  end


  def delete_file()
    @ad_piece_version = AdPieceVersion.find(params[:id])
    if params[ :resource_id ]
      @resource = ResourceFile.find( params[ :resource_id ] )
      @resource.destroy() if ( @resource )
    end

    render :partial => 'resources_list', :locals => { :ad_piece_version => @ad_piece_version }
  end
	
end
