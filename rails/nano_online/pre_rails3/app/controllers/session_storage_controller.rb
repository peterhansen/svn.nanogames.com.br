class SessionStorageController < ApplicationController
  def create
    session.merge!(params[:hash])
    render :nothing => true
  end
end