class SubmissionsController < ApplicationController

  before_filter :authorize_admin
  before_filter :session_expiry

  layout :user_layout

  # GET /submissions
  # GET /submissions.xml
  def index
    @submissions = Submission.find( :all, :include => [:integrator, :devices] )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @submissions }
    end
  end

  # GET /submissions/1
  # GET /submissions/1.xml
  def show
    @submission = Submission.find_by_id(params[:id])

    respond_to do |format|
      if @submission
        format.html
        format.zip do
          if file = @submission.zipped
            send_data File.open(file).read, :type => 'application/zip', :disposition => 'inline', :filename => "#{@submission.integrator.name}_submissions.zip"
          else
            flash[:notice] = "Zip file not available"
            redirect_to submissions_path
          end
        end
      else
        format.html do
          flash[:notice] = "Invalid submission"
          redirect_to submissions_path
        end
        format.zip { render :nothing => true }
       end
     end

  end

  # GET /submissions/new
  # GET /submissions/new.xml
  def new
    @submission = Submission.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @submission }
    end
  end

  # GET /submissions/1/edit
  def edit
    @submission = Submission.find(params[:id])
    @app_versions = AppVersion.find( :all, :order => 'app_id, number' )
  end

  # POST /submissions
  # POST /submissions.xml
  def create
    @submission = Submission.new(params[:submission])

    respond_to do |format|
      if @submission.save
        flash[:notice] = "Submission \##{ @submission.id } was successfully created."
        format.html { redirect_to(@submission) }
        format.xml  { render :xml => @submission, :status => :created, :location => @submission }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @submission.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /submissions/1
  # PUT /submissions/1.xml
  def update
    params[ :submission ][ :app_version_ids ] ||= []
    @submission = Submission.find(params[:id])

    respond_to do |format|
      if @submission.update_attributes(params[:submission])
        #atualiza a lista de aparelhos suportados
        @submission.update_devices()

        flash[:notice] = "Submission \##{ @submission.id } was successfully updated."
        format.html { redirect_to(@submission) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @submission.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /submissions/1
  # DELETE /submissions/1.xml
  def destroy
    @submission = Submission.find(params[:id])
    @submission.destroy

    respond_to do |format|
      flash[:notice] = "Submission \##{ @submission.id } was successfully destroyed."
      format.html { redirect_to(submissions_url) }
      format.xml  { head :ok }
    end
  end


  # GET /app_versions/1
  # GET /app_versions/1.xml
  def generate
    begin
      @submission = Submission.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      logger.error( "Invalid app_version id #{params[:id]}" )
      flash[:notice] = "Invalid app_version"
      redirect_to :action => :index
    else
      @submission.generate_zip
      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @submission }
      end
    end

  end


end

