class ImuzDb::GameSessionsController < ApplicationController
  MOBILE_APP_ID = 156964327690065
  MOBILE_APP_SECRET = '026fb3dedd872fcdf3970bf90effabb9'
  MOBILE_APP_KEY = '28664894faf6118df4290edd1af8ca56'
  MOBILE_CALLBACK_HOST = "http://www.nanogames.com.br"
  MOBILE_CALLBACK_PAGE = "#{MOBILE_CALLBACK_HOST}/imuz_db/mobile/fb_session"
  IMUZDB_APP_ID = 133030040091788
  IMUZDB_CALLBACK_PAGE = "http://apps.facebook.com/imuzdbquiz/"

#  require 'mini_fb'
  before_filter :simple_facebook_authentication, :only => :home
  before_filter :check_phone_is_logged, :only => :home
  before_filter :ensure_game_is_running, :only => [:respond, :skip_question, :missed_question]
  before_filter :set_profile_info
  before_filter :set_iphone_format
  before_filter :iphone_ajax
  
  
  layout "imuz_db"

  @@local = ( Rails.env == 'production' ) ? false : true
  def frame
    render :layout => false
  end

  def home
    @latest_new = news_feeds.first if news_feeds
  end
  
   def fb_login
    redirect_to facebook_login_page
  end
  
  def news
    @news = news_feeds
  end
  
  # TODO POST create
  def start
    # TODO deve ficar em um before_filter
    if current_user
      @game = ImuzDb::GameSession.create(:max_questions => ImuzDb::GameSession::MAX_QUESTIONS, :customer_id => current_user.id)
      session[:game_running] = true
      @game.start
      # TODO redirect_to :action => :show
      redirect_to :action => :next_question, :game_id => @game.id
    else
      # TODO redirect_to :controller => :pages, :action => :home
      redirect_to :action => :home, :p => self.hash
    end
  end

  def new_question_iphone
    @game = ImuzDb::GameSession.find(params[:game_id])
    @game_state = if params[:choosed]
       @game.respond(params[:choosed])
    elsif params[:time_up]
      @game.respond nil 
    elsif params[:skipped]
      @game.skip
    else
      nil
    end
    question = @game.current_question || @game.last_question
    correct_option = ImuzDb::Answer.find_by_id(question.correct_answer.id)
    respond_to do |format|
      format.js do
        render :json => {:game_state => @game_state, :user_score => current_user_score, :game => @game, 
                        :question => question, :options => question.answers, :correct_option_id => correct_option.id,
                        :correct_option => correct_option.text, :game_over => game_over_iphone(@game) } 
      end
    end
  end

  # TODO PUT update turns_controller params(:choosed => 1234)
  def respond
    @game = ImuzDb::GameSession.find(params[:game_id])

    # TODO deveria ficar em um before_filter
    if @game.started?
      prev_question = @game.current_question || @game.last_question
      @game_state = @game.respond(params[:choosed])
      # TODO redirect_to :action => :show
      respond_to do |format|
        format.html { redirect_to :action => @game_state, :game_id => @game.id if @game_state }
        format.js do
           question = @game.current_question || @game.last_question
           correct_option = ImuzDb::Answer.find_by_id(prev_question.correct_answer.id).text
           render :json => {:game_state => @game_state, :user_score => current_user_score, :game => @game, 
                            :question => {:current => question, :previous => prev_question}, 
                            :options => question.answers, :correct_option => correct_option,
                            :game_over => game_over_iphone(@game) } 
        end
      end
    else
      # TODO redirect_to :controller => :pages, :action => :home
      redirect_to :action => :home, :p => self.id
    end
  end

  # TODO GET show turns controller
  def next_question
    @game = ImuzDb::GameSession.find_by_id(params[:game_id] || params[:id])
    @current_user_score = current_user_score
    if @game && @game.ended?
      @game.terminate
      session[:game_running] = false

      # TODO redirect_to :action => :show
      redirect_to :action => :game_over, :game_id => @game.id
    elsif @game.blank? || @game.not_started?
      # TODO redirect_to :controller => :pages, :action => :home
      redirect_to :action => :home, :p => self.id
    end
  end

  # TODO PUT update turns_controller params(:skip => true)
  def skip_question
    @game = ImuzDb::GameSession.find(params[:game_id] || params[:id])
    if request.post? or request.xhr?
      @game.skip
      respond_to do |format|
        format.html { render :action => :next_question }
        format.js do
           question = @game.current_question || @game.last_question
           options = question.answers.collect {|opt| opt}           
           render :json => {:game => @game, :user_score => current_user_score,
                            :question => {:current => question},
                            :options => question.answers, :skipping => true} 
        end
      end
    else
      redirect_to :action => :start
    end
  end

  # TODO PUT update turns_controller params(:choosed => nil)
  def missed_question
    @game = ImuzDb::GameSession.find(params[:game_id] || params[:id])
    @game.respond nil
    respond_to do |format|
      format.html { render :action => :next_question }
      format.js do
        question = @game.current_question || @game.last_question
        options = question.answers.collect {|opt| opt.text}           
        render :json => {:game => @game, :user_score => current_user_score,
                            :question => {:current => question},
                            :options => question.answers, :timeup => true, 
                            :game_over => game_over_iphone(@game)} 
      end
    end
  end

  def correct
    @game = ImuzDb::GameSession.find(params[:game_id] || params[:id])
    @question_count = (@game.hits + @game.misses) - 1
  end

  def wrong
    @game = ImuzDb::GameSession.find(params[:game_id] || params[:id])
    @correct = @game.ended? ? @game.current_question.correct_answer : @game.last_question.correct_answer
    @question_count = (@game.hits + @game.misses) - 1
  end

  def game_over
    @game = ImuzDb::GameSession.find(params[:game_id])
    @score = current_user_score
  end

  def ranking
    @app = App.find_by_name_short(ImuzDb::GameSession::SHORT_NAME)
    @ranking_entries = RankingEntry.best_scores({:ranking_type => RankingType.find_by_name('imuz_db_cumulative'),
                                                 :app_version => @app.app_versions.first}, 50).first
    @current_user = current_user
    @current_user_score = current_user_score
    @current_user_position = RankingEntry.sum(:score,
                                              :conditions => "ranking_type_id = #{RankingType.find_by_name('imuz_db_cumulative').id} and app_version_id = #{@app.app_versions.first.id}",
                                              :group => :customer_id,
                                              :having => "sum(score) > #{@current_user_score || 0}").size + 1
                                              
  end

  def choose_nickname
  end
  
  def set_nickname
    @success = create_facebook_user(session[:uid], params[:nickname])
    if is_iphone_request?
      redirect_to :action => :home
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def set_profile_info
    if @user = Customer.find_by_id(params[:customer_id]) || current_user
      @patent = ImuzDb::GameSession.patent_for_user(@user)
      @score = ImuzDb::GameSession.score_for_user(@user) || 0
      
      questions_info = ImuzDb::GameSession.find_by_sql("SELECT sum(hits) as total_hits, sum(misses) as total_misses FROM imuz_db_game_sessions where customer_id = #{@user.id} group by customer_id").first
      @hits = questions_info ? questions_info.total_hits : 0
      @misses = questions_info ? questions_info.total_misses : 0
    end
  end

  def friend_list
    return @friends = [{:uid => 1234, :name => "Silas"},
                       {:uid => 5678, :name => "Patinho"},
                       {:uid => 9012, :name => "Gaiteiro"},
                       {:uid => 3456, :name => "Gansinho"}] if @@local

    @friends = MiniFB.fql(session[:oauth_token],
                          "SELECT uid, name, pic FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1==#{session[:uid]})")
  end

  def invite_friends
    params[:friends_to_invite].each do |friend|
      MiniFB.post(session[:oauth_token],
                  friend,
                  :type => "feed",
                  :params=>{ :message => "Come join me on the iMuzDB Musical Quiz! Test your musical knowledge and prove yourself a virtuoso in the Ranking!" })
    end
    render :nothing => true
  end

  def facebook_share
    if oauth_token = session[:oauth_token] || session["oauth_token"]
      last_game = ImuzDb::GameSession.last_game_for_user(current_user)
      MiniFB.post(oauth_token,
                  'me',
                  :type => "feed",
                  :params=>{ :message => 
                  	"I just played the iMuzDB Quiz and got #{last_game.hits} questions right! 
                  	 Can you beat my musical knowledge?"                    
                  })
      redirect_to :action => :home, :p => self.id, :message => "Your performance was posted to your wall!"
    else
      redirect_to facebook_login_page('true')
    end
  end

  def help; end

  def audio_state
    if request.post?
      session[:audio_state] = (params[:state] == "on")
    end

    render :text => session[:audio_state]
  end

  def fb_session
    session["oauth_token"] = MiniFB.oauth_access_token(MOBILE_APP_ID, MOBILE_CALLBACK_PAGE, MOBILE_APP_SECRET, params[:code])['access_token']
    session[:uid] = MiniFB.get(session["oauth_token"], 'me').id
    user = Customer.find_by_id(session[:current_user_id])
    if user
      flash[:fb_account_is_taken] = ! create_facebook_user(session[:uid], user.nickname).save
      redirect_to :controller => 'imuz_db/game_sessions', :action => 'profile'
    elsif FacebookProfile.find_by_uid( session[:uid] ) 
      redirect_to :action => 'home'
    else
      redirect_to :action => 'choose_nickname'
    end
  end
  
  def prepare_share
    callback_page = "#{MOBILE_CALLBACK_HOST}/imuz_db/game_sessions/prepare_share"
    session["oauth_token"] = MiniFB.oauth_access_token(MOBILE_APP_ID, callback_page, MOBILE_APP_SECRET, params[:code])['access_token']
    session[:uid] = MiniFB.get(session["oauth_token"], 'me').id
    redirect_to :action => 'facebook_share'
  end
  
  private
  def simple_facebook_authentication
    return false if is_iphone_request?
    return session[:uid] = FacebookProfile.last.uid if @@local
    unless params[:p]
      decoded_hash = decode_facebook_hash(params[:signed_request]) 
      if decoded_hash["user_id"]
        session[:uid] = decoded_hash["user_id"]
        session[:oauth_token] = decoded_hash["oauth_token"]
        redirect_to :action => :choose_nickname unless FacebookProfile.find_by_uid( session[:uid] )
      else
        top_redirect_to facebook_login_page
      end
    end
  end

  def news_feeds
    app = App.find_by_name_short('IMDB', :include => {:app_versions => [:news_feed_categories => [:news_feeds, :news_category_translations]]},
                                          :conditions => 
                                          "app_versions.number = '1.0' and news_category_translations.title = 'Main' and news_feeds.active = 1")
    if app && !app.app_versions.blank? && app.app_versions.first.news_feed_categories
      app.app_versions.first.news_feed_categories.first.news_feeds
    else
      nil
    end
  end

  def ensure_game_is_running
    if parameters = params[:game_id] || params[:id]
      redirect_to :action => :home, :p => self.id unless ImuzDb::GameSession.find(parameters).started?
    else
      redirect_to :action => :home, :p => self.id
    end
  end

  def current_user
    if not @@local
      if is_iphone_request? and session[:current_user_id]
        return Customer.find_by_id(session[:current_user_id])
      end
    end
    return FacebookProfile.last.customer if @@local
    if session[:uid]
      profile = FacebookProfile.find_by_uid( session[:uid] )
      profile.customer if profile
    end
  end

  def current_user_score(user = nil)
    ImuzDb::GameSession.score_for_user(user || current_user)
  end

  
  
  def create_facebook_user(uid, nickname)
    begin 
      facebook_info = MiniFB.fql(session["oauth_token"],
                                 "SELECT name, email, pic, birthday FROM user WHERE uid=#{session[:uid]}").first
    rescue
      redirect_to :action => :choose_nickname, :p => 'error'
    end
    
    facebook_info[:email] = nil if facebook_info[:email] and facebook_info[:email].length > 40
    unless customer = Customer.first(:conditions => ["nickname = ? or email = ?", nickname, (uid.to_s + "@facebook.com")])
      customer = Customer.create!(:nickname => nickname,
                                  :first_name => facebook_info[:name],
                                  :password => "#{uid}uid",
                                  :email => facebook_info[:email] || session[:uid].to_s + "@facebook.com")
                                  
      if facebook_info['birthday'] 
        customer.update_attributes(:birthday => Date.parse(facebook_info['birthday']), 
                                   :show_birthday => true)        
      end
    end

    customer.update_attribute(:nickname, nickname) unless customer.nickname == nickname
    
    FacebookProfile.create( :uid => uid,
                             :customer => customer,
                             :name => nickname,
                             :pic => facebook_info[:pic])
  end

  def facebook_login_page(params = nil)
    app_id, callback_page, options = decide_app
    callback_page = "#{MOBILE_CALLBACK_HOST}/imuz_db/game_sessions/prepare_share" if params
    MiniFB.oauth_url( app_id, callback_page, options)
  end
  
  def decide_app
    if is_iphone_request?
      return MOBILE_APP_ID, MOBILE_CALLBACK_PAGE, {:scope=> "email, user_birthday, publish_stream, offline_access", :display => 'touch'}
    else
      return IMUZDB_APP_ID, IMUZDB_CALLBACK_PAGE, {:scope=> "email, publish_stream, offline_access"}
    end
  end

  

  def decode_facebook_hash(signed_request)
    signature, encoded_hash = signed_request.split('.') 

    begin
      ActiveSupport::JSON.decode(Base64.decode64(encoded_hash))
    rescue
      begin
        ActiveSupport::JSON.decode(Base64.decode64(encoded_hash) + "}")
      rescue
        ActiveSupport::JSON.decode(Base64.decode64(encoded_hash) + "\"}")        
      end
    end
  end

  
  def top_redirect_to(*args)
    @redirect_url = url_for(*args)

    render :layout => false, :inline => <<-HTML
      <html><head>
        <script type="text/javascript">  
          window.top.location.href = <%= @redirect_url.to_json -%>;
        </script>
        <noscript>
          <meta http-equiv="refresh" content="0;url=<%=h @redirect_url %>" />
          <meta http-equiv="window-target" content="_top" />
        </noscript>                
      </head></html>
    HTML
  end


  def test_local
    user = Customer.last
    session[:current_user_id] = user.id
  end

  def iphone_ajax
    if request.xhr? && request.format == :iphone
      request.format = :js
    end
  end

  def check_phone_is_logged
    if is_iphone_request? and not current_user
      redirect_to "/imuz_db/mobile/login"
    end
  end
  
  def game_over_iphone(game)
    if game.ended?
      game.terminate
      game_over_type = game.hits >= (ImuzDb::GameSession::MAX_QUESTIONS / 2) ? 'positive' : 'negative'
      {:type => game_over_type}
    end
  end
  
end
