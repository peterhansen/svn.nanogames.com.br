class NewsFeedCategoriesController < ApplicationController
  
  before_filter :authorize_admin
  before_filter :session_expiry
  
  layout :user_layout
  
  # GET /news_feed_categories
  # GET /news_feed_categories.xml
  def index
    @news_feed_categories = NewsFeedCategory.find(  :all, :include => [:news_category_translations, {:app_versions => :app}] )

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @news_feed_categories }
    end
  end

  # GET /news_feed_categories/1
  # GET /news_feed_categories/1.xml
  def show
    @news_feed_category = NewsFeedCategory.find(params[:id])    
  end

  # GET /news_feed_categories/new
  # GET /news_feed_categories/new.xml
  def new
    if request.post?
      
        @news_feed_category = NewsFeedCategory.new
        @translation = NewsCategoryTranslation.new(params[:news_category_translation])

        @news_feed_category.news_category_translations << @translation

        if @news_feed_category.save
          flash[:notice] = "News feed category #{ @news_feed_category.id } was successfully created."
          redirect_to :action => "index"
        else
          @languages = Language.find( :all )
        end
    else
        @translation = NewsCategoryTranslation.new
        @languages = Language.find( :all )
    end
  end

  # GET /news_feed_categories/1/edit
  def edit
    @news_feed_category = NewsFeedCategory.find(params[:id])
  end

  # POST /news_feed_categories
  # POST /news_feed_categories.xml
  def create

  end

  # PUT /news_feed_categories/1
  # PUT /news_feed_categories/1.xml
  def update
    @news_feed_category = NewsFeedCategory.find(params[:id])

    respond_to do |format|
      if @news_feed_category.update_attributes(params[:news_feed_category])
        flash[:notice] = "News feed category #{ @news_feed_category.id } was successfully updated."
        format.html { redirect_to(@news_feed_category) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @news_feed_category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /news_feed_categories/1
  # DELETE /news_feed_categories/1.xml
  def destroy
    @news_feed_category = NewsFeedCategory.find(params[:id])
    @news_feed_category.destroy

    respond_to do |format|
      flash[:notice] = "News feed category #{ @news_feed_category.id } was successfully destroyed."
      format.html { redirect_to(news_feed_categories_url) }
      format.xml  { head :ok }
    end
  end

  def add_translation
    if request.post?
      feed = NewsFeedCategory.find(params[:news_category_translation][:news_feed_category_id])
      @translation = NewsCategoryTranslation.new(params[:news_category_translation])

      @languages = Language.find( :all )

      feed.news_category_translations << @translation
    
      if feed.save
        flash[:notice] = "News feed category translation created."
        redirect_to :action => "show", :id => @translation.news_feed_category_id
      else
        flash[:notice] = "News feed category translation could not be saved."
        redirect_to :action => "add_translation"
      end
    else
      @translation = NewsCategoryTranslation.new
      @translation.news_feed_category_id = params[:id]

      @languages = Language.find( :all )
    end
  end

  def edit_category_translation
    @translation = NewsCategoryTranslation.find(params[:id])

    @languages = Language.find( :all )

  end

  def update_category_translation
    @translation = NewsCategoryTranslation.find(params[:id])

    if @translation.update_attributes(params[:news_category_translation])
      flash[:notice] = "Category translation updated"
      redirect_to :action => :show, :id => @translation.news_feed_category_id
    else
      flash[:notice] = "Category translation could not be saved."
      redirect_to :action => :edit
    end
  end

  def delete_category_translation
    @translation = NewsCategoryTranslation.find(params[:id])
    feed = NewsFeedCategory.find(@translation.news_feed_category_id)

    if feed.translations.size > 1
      if @translation.destroy
        flash[:notice] = "Category translation deleted."
      else
        flash[:notice] = "Category translation could not be deleted."
      end
      redirect_to :action => :show, :id => @translation.news_feed_category_id
    else
      if feed.destroy
        flash[:notice] = "Category deleted."
        redirect_to :action => :index
      else
        flash[:notice] = "Category could not be deleted."
        redirect_to :action => :show, :id => @translation.news_feed_category_id
      end
    end
  end
  
end
