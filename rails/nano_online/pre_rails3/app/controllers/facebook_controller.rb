class FacebookController < ApplicationController

  EVERYONE_ACCESS = [ :get_data_to_fill_no_profile, :publish_feed ]

  before_filter :authorize_nano, :only => EVERYONE_ACCESS
  before_filter :log_access, :only => EVERYONE_ACCESS
  
  layout :user_layout

  ##############################################################################
  # Parâmetros permitidos para o controlador do Facebook                       #
  ##############################################################################

  PARAM_FACEBOOK_INFO_END           = 0
  PARAM_FACEBOOK_UID                = 1
  PARAM_FACEBOOK_SESSION_KEY        = 2
  PARAM_FACEBOOK_SESSION_SECRET     = 3
  PARAM_FACEBOOK_FEED_TYPE          = 4
  PARAM_FACEBOOK_FEED_PARAMS        = 5

  ##############################################################################
  # Códigos de retorno relacionados ao Facebook                                #
  ##############################################################################

  # Não foi possível obter os dados do usuário via Facebook, mesmo ele já
  # estando logado
  RC_ERROR_FACEBOOK_COULD_NOT_RETRIEVE_DATA	= 1

  # O feed solicitado pela aplicação não possui registro no BD
  RC_ERROR_FACEBOOK_INVALID_FEED_TYPE       = 2

  # O usuário não forneceu a permissão necessária para a realização da ação
  RC_ERROR_FACEBOOK_NO_PERMISSION           = 3

  # Não foi possível achar as informações do facebook relativas à aplicação cliente
  RC_ERROR_FACEBOOK_INVALID_APP             = 4

  # Erro desconhecido ao tentar publicar o feed
  RC_ERROR_FACEBOOK_COULDNT_PUBLISH_FEED    = 5

  # É usuário do Facebook, mas não é usuário do NanoOnline. Isto acontece quando
  # não conseguimos, a partir de um UID, chegar a um CustomerId utilizando as
  # tabelas Customers e FacebookProfiles
  RC_ERROR_FACEBOOK_NOT_NANO_ONLINE_USER    = 6

  ##############################################################################
  # Tipos de parâmetros que podem ser passados para o preenchimento dos campos #
  # 'caption' e 'description' de um feed                                       #
  ##############################################################################

  FB_FEED_PARAM_TYPE_END    = 0
  FB_FEED_PARAM_TYPE_INT8   = 1
  FB_FEED_PARAM_TYPE_INT16  = 2
  FB_FEED_PARAM_TYPE_INT32  = 3
  FB_FEED_PARAM_TYPE_INT64  = 4
  FB_FEED_PARAM_TYPE_STRING = 5
  FB_FEED_PARAM_TYPE_FLOAT  = 6
  FB_FEED_PARAM_TYPE_DOUBLE = 7

  def login()
    # Documentação:
    # http://wiki.developers.facebook.com/index.php/How_Facebook_Authenticates_Your_Application
    # http://wiki.developers.facebook.com/index.php/Auth.getSession
    # http://www.wetware.co.nz/blog/2009/03/how-do-i-use-facebooks-restserverphp/

    # Parâmetros do método Auth.getSession
    # required
    # - api_key                   string    The application key associated with the calling application. If you specify the API key in your client, 
    #                                       you don't need to pass it with every call.
    # - sig                       string    An MD5 hash of the current request and your secret key, as described in the How Facebook Authenticates Your
    #                                       Application. Facebook computes the signature for you automatically.
    # - v                         string    This must be set to 1.0 to use this version of the API. If you specify the version in your client, 
    #                                       you don't need to pass it with every call.
    # - auth_token                string    The token returned by auth.createToken and passed into login.php
    # optional
    # - format                    string    The desired response format, which can be either XML or JSON. (Default value is XML.)
    # - callback                  string    Name of a function to call. This is primarily to enable cross-domain JavaScript requests using the <script> tag, 
    #                                       also known as JSONP, and works with both the XML and JSON formats. The function will be called with the 
    #                                       response passed as the parameter.
    # - generate_session_secret   bool      Whether to generate a temporary session secret associated with this session. This is for use only with regular 
    #                                       sessions where the user hasn't granted your site or application the offline_access extended permission, 
    #                                       for applications and sites that want to use a client-side component without exposing the application secret. 
    #                                       Note that the application secret is still required for all server-side calls, for security reasons.
    # - host_url                  string    The full URL of the page being constructed. By providing the host URL, we can determine what base domain to use 
    #                                       when setting cookies on the client's browser.

    # Faz a requisição
    fb_params = { "api_key" => params[ "api_key" ],
                  "auth_token" => params[ "auth_token" ],
                  "generate_session_secret" => params[ "generate_session_secret" ],
                  };

    xml_data = send_fb_request( "Auth.getSession", fb_params, FB_DEFAULT_PARAMS_METHOD | FB_DEFAULT_PARAMS_CALLID | FB_DEFAULT_PARAMS_VERSION, nil );
    
    # Lê o xml retornado
    xml_doc = REXML::Document.new( xml_data );
    response = xml_doc.elements[ "Auth_getSession_response" ];
    
    # TODO Talvez fosse possível fazer estas operações em uma outra thread. Assim não atrasaríamos a resposta de login
    if response
      # Transforma o xml em uma Hash Table. Agora temos os campos session_key, uid, expires e secret
      user_info = {};
      response.each_element do |element|
        text = element.get_text;
        user_info[ element.name ] = (text == nil ? nil : text.to_s)
      end

      if( user_info[ "uid" ] && user_info[ "session_key" ] && user_info[ "secret" ] )
        # Se já possuímos um cadastro no Nano Online, atualiza os dados do Facebook
        old_fb_profile = FacebookProfile.find_by_uid( user_info[ "uid" ] );

        if( old_fb_profile )
          new_fb_profile = create_fb_profile( params[ "api_key" ], user_info[ "uid" ], user_info[ "session_key" ], user_info[ "secret" ] );
          new_fb_profile.customer_id = old_fb_profile.customer_id;
          old_fb_profile.update_attributes( new_fb_profile.attributes );
        end
      end
      
    end

    # Retorna a resposta para o cliente
    render :xml => xml_data;
  end

  def get_data_to_fill_no_profile()

    # Lê os parâmetros enviados pelo cliente
    uid = -1;
    session_key = "";
    session_secret = "";

    parse_data( params[ ID_SPECIFIC_DATA ] ) { |stream, id|
      case id
        when PARAM_FACEBOOK_UID
          uid = stream.read_long();

        when PARAM_FACEBOOK_SESSION_KEY
          session_key = stream.read_string();

        when PARAM_FACEBOOK_SESSION_SECRET
          session_secret = stream.read_string();

        when PARAM_FACEBOOK_INFO_END
          break
      end
    }

    # Executa a query
    params[ "query" ] = "select first_name, last_name, name, username, birthday_date, sex from user where uid = %d" % [ uid ];
    params[ "session_key" ] = session_key;
    params[ "session_secret" ] = session_secret;
    xml_data = fql( params );

    # Lê o xml retornado formatando os dados para o cliente
    xml_doc = REXML::Document.new( xml_data );
    user = xml_doc.root.elements[ "user" ];

    # render_binary é o responsável por retornar as informações para o cliente
    if( user != nil )
        # Transforma o xml em uma Hash Table
        user_info = Hash.new();
        user.each_element{ |el|
          txt = el.get_text;
          user_info[ el.name ] = txt == nil ? nil : txt.to_s;
        }

        render_binary(){ |out|

          out.write_byte( ID_RETURN_CODE );
          out.write_short( RC_OK );

          out.write_byte( ID_SPECIFIC_DATA );

          aux = user_info[ "first_name" ];
          aux ||= "";
          if( aux.length > 0 )
            out.write_byte( PARAM_REGISTER_FIRST_NAME );
            out.write_string( aux.slice( 0, Customer::CUSTOMER_FIRST_NAME_MAX_LEN ) );
          end

          aux = user_info[ "last_name" ];
          aux ||= "";
          if( aux.length > 0 )
            out.write_byte( PARAM_REGISTER_LAST_NAME );
            out.write_string( aux.slice( 0, Customer::CUSTOMER_LAST_NAME_MAX_LEN ) );
          end

          aux = user_info[ "username" ];
          aux ||= "";
          if( aux.length <= 0 )
            aux = user_info[ "name" ];
            aux ||= "";
          end

          if( aux.length > 0 )
            out.write_byte( PARAM_REGISTER_NICKNAME );
            out.write_string( aux.slice( 0, Customer::CUSTOMER_NICKNAME_MAX_LEN ) );
          end

          aux = user_info[ "birthday_date" ];
          aux ||= "";
          if( aux.length > 0 )
            out.write_byte( PARAM_REGISTER_BIRTHDAY_STR );
            out.write_string( aux );
          end

          aux = user_info[ "sex" ];
          aux ||= "";
          if( aux.length > 0 )
            out.write_byte( PARAM_REGISTER_GENRE );
            out.write_char( aux == 'male' ? 'm' : 'f' );
          end

          out.write_byte( PARAM_REGISTER_INFO_END );
        }
    else
      render_binary() { |out|
        out.write_byte( ID_RETURN_CODE );
        out.write_short( RC_ERROR_FACEBOOK_COULD_NOT_RETRIEVE_DATA );
      }
    end
  end

  def publish_feed()
    # Documentação:
    # http://wiki.developers.facebook.com/index.php/Stream.publish
    # http://wiki.developers.facebook.com/index.php/Extended_permissions/Stream_permissions
    # http://wiki.developers.facebook.com/index.php/Extended_permissions
    # http://wiki.developers.facebook.com/index.php/Attachment_(Streams)
    # http://wiki.developers.facebook.com/index.php/Action_Links

    # Parâmetros do método Stream.publish
    # optional
    # - session_key   string    The session key of the logged in user, or the session key provided when the user granted your application the 
    #                           offline_access extended permission. The session key is automatically included by our PHP client. This is only required 
    #                           if you don't specify a uid, or if a desktop application calls stream.publish.
    #	- format        string    The desired response format, which can be either XML or JSON. (Default value is XML.)
    # - callback      string    Name of a function to call. This is primarily to enable cross-domain JavaScript requests using the <script> tag, 
    #                           also known as JSONP, and works with both the XML and JSON formats. The function will be called with the response passed 
    #                           as the parameter.
    # - message       string    The message the user enters for the post at the time of publication.
    # - attachment    object    A JSON-encoded object containing the text of the post, relevant links, a media type (image, mp3, flash), as well as any 
    #                           other key/value pairs you may want to add. See Attachment (Streams) for more details.
    #                           Note: If you want to use this call to update a user's status, don't pass an attachment; the content of the message 
    #                           parameter will become the user's new status and will appear at the top of the user's profile.
    # - action_links  array     A JSON-encoded array of action link objects, containing the link text and a hyperlink.
    # - target_id     string    The ID of the user, Page, group, or event where you are publishing the content. If you specify a target_id, the post appears 
    #                           on the Wall of the target profile, Page, group, or event, not on the Wall of the user who published the post. This mimics 
    #                           the action of posting on a friend's Wall on Facebook itself.
    #                           Note: If you specify a Page ID as the uid, you cannot specify a target_id. Pages cannot write on other users' Walls.
    # - uid           string    The user ID or Page ID of the user or Page publishing the post. If this parameter is not specified, then it defaults to 
    #                           the session user. If you specified a session_key, and that session user is a Page admin, then you can specify a Page ID 
    #                           here to publish to one Page for which the session user is an admin.
    #                           Note: If you specify a Page ID as the uid, you cannot specify a target_id. Pages cannot write on other users' Walls.

    # Lê os parâmetros enviados pelo cliente
    uid = -1;
    feed_type = -1;
    session_key = "";
    session_secret = "";

    # Hash que irá conter os parâmetros do código dinâmico
    vars = Hash.new();

    parse_data( params[ ID_SPECIFIC_DATA ] ){ |stream, id|
      case id
        when PARAM_FACEBOOK_UID
          uid = stream.read_long();

        when PARAM_FACEBOOK_SESSION_KEY
          session_key = stream.read_string();

        when PARAM_FACEBOOK_SESSION_SECRET
          session_secret = stream.read_string();

        when PARAM_FACEBOOK_FEED_TYPE
          feed_type = stream.read_short();

        when PARAM_FACEBOOK_FEED_PARAMS

          while( ( feed_param_type = stream.read_byte() ) != FB_FEED_PARAM_TYPE_END ) do

            case feed_param_type
              when FB_FEED_PARAM_TYPE_INT8
                vars[ stream.read_string() ] = stream.read_byte();

              when FB_FEED_PARAM_TYPE_INT16
                vars[ stream.read_string() ] = stream.read_short();

              when FB_FEED_PARAM_TYPE_INT32
                vars[ stream.read_string() ] = stream.read_int();

              when FB_FEED_PARAM_TYPE_INT64
                vars[ stream.read_string() ] = stream.read_long();

              when FB_FEED_PARAM_TYPE_STRING
                vars[ stream.read_string() ] = stream.read_string();

              when FB_FEED_PARAM_TYPE_FLOAT
                vars[ stream.read_string() ] = stream.read_float();

              when FB_FEED_PARAM_TYPE_DOUBLE
                vars[ stream.read_string() ] = stream.read_double();
            end

          end

        when PARAM_FACEBOOK_INFO_END
          break;
      end
    }

    fb_data = FacebookData.find_by_app_id( params[ :app ].id.to_i );
    if( fb_data == nil )

      render_binary(){ |out|
          out.write_byte( ID_RETURN_CODE );
          out.write_short( RC_ERROR_FACEBOOK_INVALID_APP );
      }

    elsif( !app_has_ext_permission_for( fb_data.api_key, session_key, uid, "publish_stream" ) )

      render_binary(){ |out|
          out.write_byte( ID_RETURN_CODE );
          out.write_short( RC_ERROR_FACEBOOK_NO_PERMISSION );
      }

    else

      feed = FacebookFeed.find_by_app_id_and_feed_type( params[ :app ].id.to_i, feed_type );

      if( feed != nil )
        
        customer_id = FacebookProfile.find_by_uid( uid ).customer_id;

        if( customer_id == nil )
          render_binary(){ |out|
              out.write_byte( ID_RETURN_CODE );
              out.write_short( RC_ERROR_FACEBOOK_NOT_NANO_ONLINE_USER );
          }
        else
          # Acrescenta os parâmetros default:
          # - Id da aplicação
          # - Versão da aplicação
          # - Id do usuário
          vars[ "app_id" ] = params[ :app ].id.to_i;
          vars[ "app_version" ] = params[ :app_version ].number.to_s;
          vars[ "customer_id" ] = customer_id;

          # Roda o código que preenche caption
          vars[ "str_to_fill" ] = feed.caption;
          caption = fill_str_using_code( feed.caption_format_code, vars );

          # Roda o código que preenche description
          vars[ "str_to_fill" ] = feed.description;
          description = fill_str_using_code( feed.description_format_code, vars );

          # Formata o action link
          attachment = "{\"name\":\"%s\",\"href\":\"%s\",\"caption\":\"%s\",\"description\":\"%s\",\"media\":[{\"type\":\"%s\",\"src\":\"%s\",\"href\":\"%s\"}]}" % [ feed.title, feed.title_href, caption, description, feed.media_type, feed.media_src, feed.media_href ];
          action_link = "[{\"text\":\"%s\",\"href\":\"%s\"}]" % [ feed.action_link_text, feed.action_link_href ];

          # Envia a requisição para o facebook
          fb_params = {
                        "action_links" => action_link,
                        "api_key" => fb_data.api_key,
                        "attachment" => attachment,
                        "session_key" => session_key,
                        "target_id" => uid,
                        "uid" => uid
                      };

          xml_data = send_fb_request( "Stream.publish", fb_params, FB_DEFAULT_PARAMS_METHOD | FB_DEFAULT_PARAMS_CALLID | FB_DEFAULT_PARAMS_VERSION, nil );
          
          # Lê o xml retornado
          xml_doc = REXML::Document.new( xml_data );
          response = xml_doc.elements[ "Stream_publish_response" ];

          # As possíveis respostas são: erro ou "<uid>_<post_id>"
          if( ( response != nil ) && ( response.get_text.to_s.split( "_" )[0] == uid.to_s ) )
            render_binary(){ |out|
              out.write_byte( ID_RETURN_CODE );
              out.write_short( RC_OK );
            }
          else
            render_binary(){ |out|
              out.write_byte( ID_RETURN_CODE );
              out.write_short( RC_ERROR_FACEBOOK_COULDNT_PUBLISH_FEED );
            }
          end
        end
      else

        render_binary(){ |out|
          out.write_byte( ID_RETURN_CODE );
          out.write_short( RC_ERROR_FACEBOOK_INVALID_FEED_TYPE );
        }

      end
    end
  end

  def create_fb_profile( api_key, fb_uid, fb_session_key, fb_session_secret )
    # Retirado de http://wiki.developers.facebook.com/index.php/User_(FQL):
    # Privacy Note:
    # For any user submitted to this method, the following user fields are visible to an application only if that user has authorized that application:
    # - meeting_for
    # - meeting_sex
    # - religion
    # - significant_other_id
    # In addition, the visibility of all fields, with the exception of affiliations, first_name, last_name, name, and uid may be restricted by the user's Facebook
    # privacy settings in relation to the calling user (the user associated with the current session)

    # Executa a query
    fql_params = Hash.new();
    fql_params[ "api_key" ] = api_key;
    fql_params[ "query" ] = "select first_name, last_name, name, pic_small, pic_big, pic_square, pic, profile_update_time, timezone, religion, birthday, birthday_date, sex, relationship_status, significant_other_id, political, activities, interests, is_app_user, music, tv, movies, books, quotes, about_me, notes_count, wall_count, status, online_presence, locale, proxied_email, profile_url, pic_small_with_logo, pic_big_with_logo, pic_square_with_logo, pic_with_logo, allowed_restrictions, verified, profile_blurb, username, website, is_blocked from user where uid = %d" % [ fb_uid ];
    fql_params[ "session_key" ] = fb_session_key;
    fql_params[ "session_secret" ] = fb_session_secret;
    xml_data = fql( fql_params );

    # Lê o xml retornado formatando os dados para o cliente
    xml_doc = REXML::Document.new( xml_data );
    user = xml_doc.root.elements[ "user" ];

    # render_binary é o responsável por retornar as informações para o cliente
    if( user == nil )
      return nil;
    else
      # Transforma o xml em uma hash table
      user_info = Hash.new();
      user.each_element{ |el|
        txt = el.get_text;
        user_info[ el.name ] = txt == nil ? nil : txt.to_s;
      }

      # Armazena os dados do usuário
      fb_profile = FacebookProfile.new();
      fb_profile.uid = fb_uid;
      fb_profile.first_name = user_info[ "first_name" ];
      fb_profile.last_name = user_info[ "last_name" ];
      fb_profile.name = user_info[ "name" ];
      fb_profile.pic_small = user_info[ "pic_small" ];
      fb_profile.pic_big = user_info[ "pic_big" ];
      fb_profile.pic_square = user_info[ "pic_square" ];
      fb_profile.pic = user_info[ "pic" ];
      fb_profile.profile_update_time = user_info[ "profile_update_time" ];
      fb_profile.timezone = user_info[ "timezone" ];
      fb_profile.religion = user_info[ "religion" ];
      fb_profile.birthday = user_info[ "birthday" ];
      fb_profile.birthday_date = user_info[ "birthday_date" ];
      fb_profile.sex = user_info[ "sex" ];
      fb_profile.relationship_status = user_info[ "relationship_status" ];
      fb_profile.significant_other_id = user_info[ "significant_other_id" ];
      fb_profile.political = user_info[ "political" ];
      fb_profile.activities = user_info[ "activities" ];
      fb_profile.interests = user_info[ "interests" ];
      fb_profile.is_app_user = user_info[ "is_app_user" ];
      fb_profile.music = user_info[ "music" ];
      fb_profile.tv = user_info[ "tv" ];
      fb_profile.movies = user_info[ "movies" ];
      fb_profile.books = user_info[ "books" ];
      fb_profile.quotes = user_info[ "quotes" ];
      fb_profile.about_me = user_info[ "about_me" ];
      fb_profile.notes_count = user_info[ "notes_count" ];
      fb_profile.wall_count = user_info[ "wall_count" ];
      fb_profile.status = user_info[ "status" ];
      fb_profile.online_presence = user_info[ "online_presence" ];
      fb_profile.locale = user_info[ "locale" ];
      fb_profile.proxied_email = user_info[ "proxied_email" ];
      fb_profile.profile_url = user_info[ "profile_url" ];
      fb_profile.pic_small_with_logo = user_info[ "pic_small_with_logo" ];
      fb_profile.pic_big_with_logo = user_info[ "pic_big_with_logo" ];
      fb_profile.pic_square_with_logo = user_info[ "pic_square_with_logo" ];
      fb_profile.pic_with_logo = user_info[ "pic_with_logo" ];
      fb_profile.allowed_restrictions = user_info[ "allowed_restrictions" ];
      fb_profile.verified = user_info[ "verified" ];
      fb_profile.profile_blurb = user_info[ "profile_blurb" ];
      fb_profile.username = user_info[ "username" ];
      fb_profile.website = user_info[ "website" ];
      fb_profile.is_blocked = user_info[ "is_blocked" ];

      return fb_profile;
    end

  end

  private

  FB_DEFAULT_PARAMS_METHOD          = 0x01;
  FB_DEFAULT_PARAMS_CALLID          = 0x02;
  FB_DEFAULT_PARAMS_APIKEY          = 0x04;
  FB_DEFAULT_PARAMS_VERSION         = 0x08;
  FB_DEFAULT_PARAMS_ALL_MASK        = 0x0F;

  def register_session( params )
#    fb_profile = FacebookProfile.find_by_customer_id( params[ ID_CUSTOMER_ID ] );
#
#    # Se ainda não possui um perfil do facebook associado, cria um
#    if( fb_profile == nil )
#      fb_profile =
#    end
#
#    # Atualiza os dados do perfil do facebook
#    # TODO
#
#    fb_profile.session_key = params[ ID_FACEBOOK_SESSION_KEY ];
#
#    # Salva os dados
#    fb_profile.save();
  end
  
  def fill_str_using_code( code_str, vars )
    return eval( code_str );
  end

  def app_has_ext_permission_for( api_key, session_key, uid, ext_perm )
    # Documentação:
    # http://wiki.developers.facebook.com/index.php/Users.hasAppPermission
    # http://wiki.developers.facebook.com/index.php/Extended_permissions

    # Parâmetros do método FQL.query
    # required
    # - api_key     string    The application key associated with the calling application. If you specify the API key in your client, you don't need to pass it with every call.
    # - call_id     float     The request's sequence number. Each successive call for any session must use a sequence number greater than the last. We suggest using the current time in milliseconds, such as PHP's microtime(true) function. If you specify the call ID in your client, you don't need to pass it with every call.
    # - sig         string    An MD5 hash of the current request and your secret key, as described in the How Facebook Authenticates Your Application. Facebook computes the signature for you automatically.
    # - v           string    This must be set to 1.0 to use this version of the API. If you specify the version in your client, you don't need to pass it with every call.
    # - ext_perm    string    String identifier for the extended permission that is being checked for. Must be one of email, read_stream, publish_stream, offline_access, status_update, photo_upload, create_event, rsvp_event, sms, video_upload, create_note, share_item.
    # optional
    # - session_key string    The session key of the user whose permissions you are checking. Note: A session key is always required for desktop applications. It is required for Web applications only when the uid is not specified.
    # - format      string    The desired response format, which can be either XML or JSON. (Default value is XML.)
    # - callback    string    Name of a function to call. This is primarily to enable cross-domain JavaScript requests using the <script> tag, also known as JSONP, and works with both the XML and JSON formats. The function will be called with the response passed as the parameter.
    # - uid         int       The user ID of the user whose permissions you are checking. If this parameter is not specified, then it defaults to the session user. Note: This parameter applies only to Web applications and is required by them only if the session_key is not specified. Facebook ignores this parameter if it is passed by a desktop application.

    fb_params = {
                  "api_key" => api_key,
                  "ext_perm" => ext_perm,
                  "session_key" => session_key,
                  "uid" => uid,
                };

    # Envia a requisição
    xml_data = send_fb_request( "Users.hasAppPermission", fb_params, FB_DEFAULT_PARAMS_METHOD | FB_DEFAULT_PARAMS_CALLID | FB_DEFAULT_PARAMS_VERSION, nil );

    # Lê o xml retornado
    xml_doc = REXML::Document.new( xml_data );
    response = xml_doc.elements[ "Users_hasAppPermission_response" ];

    # As possíveis respostas são: erro, "0" ou "1"
    return ( response != nil ) && ( response.get_text == "1" );

  end

  def fql( fql_params )
    # Documentação:
    # http://wiki.developers.facebook.com/index.php/Fql.query
    # http://wiki.developers.facebook.com/index.php/FQL

    # Parâmetros do método FQL.query
    # required
    # - api_key     string     The application key associated with the calling application. If you specify the API key in your client, you don't need to pass it with every call.
    # - call_id     float      The request's sequence number. Each successive call for any session must use a sequence number greater than the last. We suggest using the current time in milliseconds, such as PHP's microtime(true) function. If you specify the call ID in your client, you don't need to pass it with every call.
    # - sig         string     An MD5 hash of the current request and your secret key, as described in the How Facebook Authenticates Your Application. Facebook computes the signature for you automatically.
    # - v           string     This must be set to 1.0 to use this version of the API. If you specify the version in your client, you don't need to pass it with every call.
    # - query       string    The query to perform, as described in the FQL documentation.
    # optional
    # - session_key string    The session key of the logged in user, or the session key provided when the user granted your application the offline_access extended permission. The session key is automatically included by our PHP client. Depending upon the table you are querying, you might get only certain information without a session key. For example, querying the user table is like calling users.getInfo without a session key -- you can query only on a few fields. You can query the page_fan table without a session to return the list of Pages the user has fanned that would be visible to a logged-out user.
    # - format      string    The desired response format, which can be either XML or JSON. (Default value is XML.)
    # - callback    string    Name of a function to call. This is primarily to enable cross-domain JavaScript requests using the <script> tag, also known as JSONP, and works with both the XML and JSON formats. The function will be called with the response passed as the parameter.

    # Method specific args
    fb_params = {
                  "query" => fql_params[ "query" ],
                  "session_key" => fql_params[ "session_key" ],
                };

    # Envia a requisição e retorna a resposta para o cliente
    if( fql_params[ "api_key" ] )
      fb_params[ "api_key" ] = fql_params[ "api_key" ];
      return send_fb_request( "FQL.query", fb_params, FB_DEFAULT_PARAMS_METHOD | FB_DEFAULT_PARAMS_CALLID | FB_DEFAULT_PARAMS_VERSION, fql_params[ "session_secret" ] );
    else
      fb_params[ "app_id" ] = fql_params[ :app ].id;
      return send_fb_request( "FQL.query", fb_params, FB_DEFAULT_PARAMS_ALL_MASK, fql_params[ "session_secret" ] );
    end

  end

  def add_fb_defaults_and_method( method, fb_params, default_params_mask = FB_DEFAULT_PARAMS_ALL_MASK, session_secret = nil )

    if( ( default_params_mask & FB_DEFAULT_PARAMS_METHOD ) != 0 )
      fb_params[ "method" ] = method;
    end

    if( ( default_params_mask & FB_DEFAULT_PARAMS_CALLID ) != 0 )
      fb_params[ "call_id" ] = Time.now.to_f.to_s;
    end

    if( ( default_params_mask & FB_DEFAULT_PARAMS_APIKEY ) != 0 )

      fb_data = FacebookData.find_by_app_id( fb_params[ "app_id" ].to_i );

      if( fb_data != nil )
        fb_params[ "api_key" ] = fb_data.api_key;

        # Não podemos mandar app_id para o rest server, mas precisamos dele para obter a api_key caso
        # a aplicação cliente não a tenha enviado
        fb_params.delete( "app_id" );
      end
    end

    if( ( default_params_mask & FB_DEFAULT_PARAMS_VERSION ) != 0 )
      fb_params[ "v" ] = "1.0";
    end

#    if( session_secret != nil )
#      fb_params[ "ss" ] = "1";
#    end

    return fb_params;
  end

  def get_sorted_args( fb_params )
    return fb_params.collect{ |a|
      a[0].to_s + "=" + a[1].to_s
    }.sort;
  end

  def send_fb_request( method, fb_params, default_params_mask = FB_DEFAULT_PARAMS_ALL_MASK, session_secret = nil )

    add_fb_defaults_and_method( method, fb_params, default_params_mask, session_secret );

    if( RAILS_ENV != "production" )
      fb_params.each{ |k, v|
        puts( "@@@@@@@@@@@@@@@@@@@@@@@@ fb_params[ #{k} ]: #{v}");
      }
    end

    # Array of args to the request, not counting sig, formatted in non-urlencoded arg=val pairs
    sorted_args_array = get_sorted_args( fb_params );

    # Se possuímos session_secret, devemos acrescentá-la no final de sorted_args_array
#    if( session_secret != nil )
#      sorted_args_array += session_secret;
#    end

    # Na URL devemos separar os argumentos com '&' e retirar os caracteres especiais
    # http://en.wikipedia.org/wiki/Query_string
    sig_args = ""
    fb_params.sort.each{ |k, v|
      sig_args += k + "=" + CGI::escape( v.to_s ) + "&";
    }
    sig_args += "sig=" + generate_fb_sig( fb_params[ "api_key" ], sorted_args_array );

    # To make a request, the client should POST data to http://api.new.facebook.com/restserver.php
    res = Net::HTTP.get_response( URI.parse( "http://api.new.facebook.com/restserver.php?" + sig_args ) );

    return res.body;

  end

  # The signature can be generated by calling generate_sig in facebook.php. generate_sig takes two parameters: an array of arg=val pairs and your app secret.
  # The signature can also be constructed using the following algorithm (after all the other arguments have been determined):
  # - args = array of args to the request, not counting sig, formatted in non-urlencoded arg=val pairs
  # - sorted_array = alphabetically_sort_array_by_keys(args);
  # - request_str = concatenate_in_order(sorted_array);
  # - signature = md5(concatenate(request_str, secret))
  def generate_fb_sig( api_key, sorted_args_array )
    # Para gerar a assinatura, não utilizamos separadores entre os argumentos
    api_secret_key = FacebookData.find_by_api_key( api_key ).api_secret;
    sig = sorted_args_array.join + api_secret_key;
    return Digest::MD5.hexdigest( sig );
  end
  
end
