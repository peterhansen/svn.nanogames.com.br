class StaticController < ApplicationController
  # TODO Refatorar essa porra toda! Do jeito que está tem que criar uma action para cada novo jogo.
  # O bom seria integrar isso com um gerenciador de conteúdo.
#  layout 'static'

  def home
    rankings = RankingType.all
    params[:ranking_type] = rankings[rand(rankings.size)]
    
    @scores, @customer_scores = RankingEntry.best_scores( params )
  end  
    
  def download
    @vendors = Vendor.with_device
  end
  
  def web_contact
    @contact_errors = []
    
    if request.post?
      @contact_errors << "O nome não pode ficar em branco." if params[:name].nil? || params[:name] == "" 
      @contact_errors << "O e-mail não pode ficar em branco." if params[:email].nil? || params[:email] == ""
      @contact_errors << "O comentário não pode ficar em branco." if params[:comments].nil? || params[:comments] == ""
       
      if @contact_errors.empty? 
        dados = Hash.new
        
        dados["subject"] =      "Contato de #{params[:name]} via web."
        dados["recipients"] =   "contato@nanogames.com.br"
        dados["reply_to"] =     params[:email] # por algum motivo o email não é enviado se o sender estiver configurado para params[:email]
        dados["sender"] =       params[:email]
        dados["message"] =      "Nome: #{params[:name]}\nE-mail: #{params[:email]}\nTelefone: #{params[:ddd]}-#{params[:phone]}\nComentário:\n\n#{params[:comments]}"
        
        Emailer.deliver_send(dados)
      
        flash[:notice] = "Seu comentário foi enviado com sucesso."
        params.clear
      end
    end
  end
  
  def brazooka_soccer
    if params[:mostra] == "video"
      @video = true
    end
  end  
  
  def space_goo
    if params[:mostra] == "video"
      @video = true
    end
  end
  
  def portfolio_2010
    render :layout => false
  end

  def vila_virtual
    render :template => '/public/vila_virtual/main.html', :layout => false
  end
end
