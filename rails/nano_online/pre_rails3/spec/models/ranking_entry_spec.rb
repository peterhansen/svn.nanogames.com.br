require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RankingEntry do
  describe '#position' do
    describe 'with valid params' do
      it "return the user position" do
        RankingEntry.position(customers(:nanoca), app_versions(:release_version), ranking_types(:top_score_ranking)).should == 3
      end

      describe 'if there is two customers with the same score' do
        it 'returns the correct position' do
          RankingEntry.position(customers(:quentin), app_versions(:release_version), ranking_types(:top_score_ranking)).should == 2
          RankingEntry.position(customers(:patinho), app_versions(:release_version), ranking_types(:top_score_ranking)).should == 2
        end
      end

      describe 'if the customer have no ranking entry' do
        it "returns nil" do
          RankingEntry.position(customers(:gaiteiro), app_versions(:release_version), ranking_types(:top_score_ranking)).should be_nil
        end
      end
    end
  end
end