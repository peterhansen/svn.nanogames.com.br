require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe SessionStorageController do
  describe 'POST create' do
    it "includes the hash passed on params[:hash] to the session hash" do
      post :create, :hash => { :uid => '1234' }
      session[:uid].should == '1234'
    end
  end
end