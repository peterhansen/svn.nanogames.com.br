require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe RankingEntriesController do
  describe 'GET user_range_high_score' do
    before(:each) do
      @customer_mock = mock_model(Customer, :id => 12345, :nickname => 'Dummy', :attribute_names => [:nickname]).as_null_object
      Customer.should_receive(:find).at_least(1).times.and_return(@customer_mock)
    end

    it "assigns @customer" do
      RankingEntry.should_receive(:position).and_return(1)
      get :user_range_high_score, params_with_offset
      assigns(:customer).should == @customer_mock
    end

    it "assigns @position" do
      RankingEntry.should_receive(:position).and_return(1)
      get :user_range_high_score, params_with_offset(3)
      assigns(:position).should == 1
    end

    it "assings @ranking_entries" do
      RankingEntry.should_receive(:position).and_return(4)
      get :user_range_high_score, params_with_offset(1)
      assigns(:ranking_entries).should == [ranking_entries(:nanoca_entry_1), ranking_entries(:boris_entry_1), ranking_entries(:dilas_entry_1)]
    end

    it "returns ranking entries as json" do
      RankingEntry.should_receive(:position).and_return(4)
      get :user_range_high_score, params_with_offset(1)
      response.body.should == "[{\"ranking_entry\":{\"id\":735086511,\"customer\":{},\"score\":400}},{\"ranking_entry\":{\"id\":608186645,\"customer\":{},\"score\":300}},{\"ranking_entry\":{\"id\":80733207,\"customer\":{},\"score\":200}}]"
    end
  end

  def params_with_offset(value = 0)
    { :uid => '1234', :ranking_type => ranking_types(:top_score_ranking).id, :app_version => app_versions(:release_version).id, :offset => value }
  end
end