/* Question Feedback Animation */

// Correct Answer Animation

function correctAnswerAnimation(){
    $('.your_answer').gx(
      { left: '0px'},
      200,
      'Linear',
      isCorrect
    );

    function isCorrect(){
      $('.correct').gx(
          { left: '0px'},
          800,
          'Bounce',
          function(){
              $('.bt_next').show();
          }
      );
    }
}

// Wrong Answer Animation

function wrongAnswerAnimation(){
    $('.your_answer').gx(
      { bottom: '120px'},
      500,
      'Linear',
      isWrong
    );

    function isWrong(){
      $('.wrong').gx(
          { top: '40px'},
          1500,
          'Bounce',
          function(){
              $('.correct_was').fadeIn();
              $('.bt_next').show();
          }
      );
    }
}

function start_animation(){
  $('.correct_answer').gx(
      { height: '425px'},
      1000,
      'Bounce',
      correctAnswerAnimation
  );

  $('.wrong_answer').gx(
      { width: '470px'},
      1000,
      'Bounce',
      wrongAnswerAnimation
  );
}

$(document).ready(function(){
  setTimeout(function(){
    start_animation();
  }, 1200);
});





