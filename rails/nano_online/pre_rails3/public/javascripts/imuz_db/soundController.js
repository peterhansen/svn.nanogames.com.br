var SoundController = {
  sounds: new Array(),
  playNow: null,
  on: true,

  init: function() {
    soundManager.url = '/swf/soundmanager2.swf';
    soundManager.useFlashBlock = false;
    soundManager.debugMode = false;

    this.on = this.isRemoteStateOn();

    soundManager.onload = function() {
      if(SoundController.playNow) {
        setTimeout(function(){
          SoundController.play(SoundController.playNow.name, SoundController.playNow.path);
        }, 1500);
      }
    };
  },

  bind: function(name, path, element, event) {
    this.sounds = this.sounds.concat({name: name, path: path});
    $(element).live(event, function(){
      SoundController.play(name, path);
    });
  },

  onLoadSound: function(path) {
    this.playNow = {name: 'play_now', path: path}
  },

  play: function(name, path){
    if(SoundController.on) {
      soundManager.play(name, path);
    }
  },

  turnOn: function() {
    this.on = true;
    this.setRemoteState('on');
  },

  turnOff: function() {
    this.on = false;
    this.setRemoteState('off');
  },

  toggleState: function() {
    if(SoundController.on){
      SoundController.turnOff();
    } else {
      SoundController.turnOn();
    }
  },

  bindStateToggleToElement: function(element) {
    $(element).click(this.toggleState)
  },

  isRemoteStateOn: function() {
    if($('.on_to_off').css("display") == 'none') {
      return false;
    } else {
      return true;
    }
  },

  setRemoteState: function(st) {
//    debugger
//    alert(st);
    jQuery.post('/imuz_db/game_sessions/audio_state', {state: st});
  }
};
