var warning_image
var viewport_element

function appIsInstalled(){
    if(!window.navigator.standalone){
        window.location = '/imuz_db/mobile/installation'  
    }
}

function appIsRegistered(){
    if(!localStorage.getItem('registered')){
        window.location = '/imuz_db/mobile/first_use'  
    }  
}


function rememberMe(){
    remember_me = document.getElementById('remember').innerHTML
    c_user_id = document.getElementById('current_user_id').innerHTML
    if(remember_me == 1){
      localStorage.setItem('current_user_id', c_user_id)  
    }
}

function isRemember(){
    current_user_id = localStorage.getItem('current_user_id')
    if(!!current_user_id){
       jQuery.ajax({
            url: '/imuz_db/mobile/ajax_login',
            type: "GET",
            async: false,
            data: {'current_user_id': current_user_id},
            success: function(){
                window.location = '/imuz_db/mobile/home'
            }
       })        
    }
}

function registerApp(){
    if(!localStorage.getItem('registered')){
        localStorage.setItem('registered', true)
    }
}

function appAlreadyInstalled(){
    if(!!window.navigator.standalone){
        window.location = '/imuz_db/mobile/first_use'  
    }  
}

function appAlreadyRegistered(){
    if(!!localStorage.getItem('registered')){
        window.location = '/imuz_db/mobile/login'  
    }  
}

function showViewPort(){
    warning_image.style.display = 'none'
    viewport_element.style.display = 'block'
}

function hideViewPort(){
    viewport_element.style.display = 'none'
    warning_image.style.display = 'block'
}

function orientationHandler(e){
    if (window.orientation == 90 || window.orientation == -90) {
        hideViewPort()
    }
    else {
        showViewPort()
    }
}
/*Filter('before', appIsInstalled, {exceptions: ["/imuz_db/mobile/installation", "/imuz_db/mobile"]})*/
Filter('before', appAlreadyInstalled, {only: ["/imuz_db/mobile/installation", "/imuz_db/mobile"]})

/*Filter('before', appIsRegistered, {exceptions: ["/imuz_db/mobile/first_use", "/imuz_db/mobile/sign_up", "/customer_login/sign_up", 
                                         "/imuz_db/mobile/home", "/imuz_db/game_sessions/home"]})*/

Filter('before', appAlreadyRegistered, {only: ["/imuz_db/mobile/first_use", "/customer_login/first_use"]})                                         


Filter('before', registerApp, {only: ["/imuz_db/mobile/home", "/imuz_db/game_sessions/home"]})
                                         
Filter('before', isRemember, {only: ["/imuz_db/mobile/login", "/customer_login/login"]})       
Filter('after', rememberMe, {only: ["/imuz_db/mobile/home", "/imuz_db/game_sessions/home"]})

window.addEventListener('load', setupLinks)
window.addEventListener('load', function(){
    warning_image = document.getElementById('orientation_warning')
    viewport_element = document.getElementById('viewport')
    window.onorientationchange = orientationHandler
    if (window.orientation == 90 || window.orientation == -90) {
        hideViewPort()
    }
})

/*function Track(src, spriteLength, audioLead) {
  var track = this,
      audio = document.createElement('audio');
  audio.src = src;
  audio.autobuffer = true;
  audio.load();
  audio.muted = true; // makes no difference on iOS :(
  
  /* This is the magic. Since we can't preload, and loading requires a user's 
     input. So we bind a touch event to the body, and fingers crossed, the 
     user taps. This means we can call play() and immediate pause - which will
     start the download process - so it's effectively preloaded.
     
     This logic is pretty insane, but forces iOS devices to successfully 
     skip an unload audio to a specific point in time.
     first we play, when the play event fires we pause, allowing the asset
     to be downloaded, once the progress event fires, we should have enough
     to skip the currentTime head to a specific point. 
     
  var force = function () {
    audio.pause();
    audio.removeEventListener('play', force, false);
  };
  
  var progress = function () {
    audio.removeEventListener('progress', progress, false);
    if (track.updateCallback !== null) track.updateCallback();
  };
  
  audio.addEventListener('play', force, false);
  audio.addEventListener('progress', progress, false);
  
  var kickoff = function () {
    audio.play();
    document.documentElement.removeEventListener('click', kickoff, true);
  };
  
  document.documentElement.addEventListener('click', kickoff, true);
  
  this.updateCallback = null;
  this.audio = audio;
  this.playing = false;
  this.lastUsed = 0;
  this.spriteLength = spriteLength;
  this.audioLead = audioLead;
}
 
Track.prototype.play = function (position) {
  var track = this,
      audio = this.audio,
      lead = this.audioLead,
      length = this.spriteLength,
      time = lead + position * length,
      nextTime = time + length;
      
  clearInterval(track.timer);
  track.playing = true;
  track.lastUsed = +new Date;
  
  audio.muted = false;
  audio.pause();
  try {
    if (time == 0) time = 0.01; // yay hacks. Sometimes setting time to 0 doesn't play back
    audio.currentTime = time;
    audio.play();
  } catch (e) {
    this.updateCallback = function () {
      track.updateCallback = null;
      audio.currentTime = time;
      audio.play();
    };
    audio.play();
  }
 
  track.timer = setInterval(function () {
    if (audio.currentTime >= nextTime) {
      audio.pause();
      audio.muted = true;
      clearInterval(track.timer);
      player.playing = false;
    }
  }, 10);
};

var player = (function (src, n, spriteLength, audioLead) {
  var tracks = [],
      total = n,
      i;
  
  while (n--) {
    tracks.push(new Track(src, spriteLength, audioLead));
  }
  
  return {
    tracks: tracks,
    play: function (position) {
      var i = total,
          track = null;
          
      while (i--) {
        if (tracks[i].playing === false) {
          track = tracks[i];
          break;
        } else if (track === null || tracks[i].lastUsed < track.lastUsed) {
          track = tracks[i];
        }
      }
      
      if (track) {
        track.play(position);
      } else {
        // console.log('could not find a track to play :(');
      }
    }
  };
})('/audio/music/sound_sprite_02.wav', 1, 0.5, 0.00);*/
             

application = function(){
    return {
        sound: function(){
            audio = function(){
                audio = document.createElement('audio')
                audio.autobuffer = true
                audio.muted = false
                return audio
            }()
            canPlay = function(){
                return localStorage.getItem('canPlay') == "true"
            }
            audio_path =  '/audio/music/'
            play =  function(src){
                if(canPlay()){
                    audio.src = src
                    audio.load()
                    audio.play()
                }
            }
           
            
            setAudioStateView = function(src, text, class_name, func){
                knob = document.getElementById('audio_knob')
                label = document.getElementById('audio_label')
                if(knob){
                    knob.onclick = func
                    knob.src = src
                    knob.className = class_name
                    label.innerHTML = "Audio " + text
                }
        
            }
            
            saveOptions = function(state){
                localStorage.setItem('canPlay', state)
            }
            
       
            return {
                turnOn: function(){
                    setAudioStateView("/imuz_db/mobile/images/knob_volume_on.png", "On", "turn_on", application.sound.turnOff)
                    saveOptions(true)
                },
                turnOff: function(){
                    setAudioStateView("/imuz_db/mobile/images/knob_volume_off.png", "Off", "turn_off", application.sound.turnOn)
                    saveOptions(false)
                },
                loadView: function(){
                    if((localStorage.getItem('canPlay') == null) || localStorage.getItem('canPlay') == "true"){
                        application.sound.turnOn()
                    }
                    else{
                        application.sound.turnOff()
                    }
                        
                },
                answer: {
                    wrong: function(){
                        play(audio_path + 'wrong_answer.mp3')
                    },
                    correct: function(){
                        play(audio_path + 'correct_answer.mp3')
                    }
                },
                game_over: function(){
                    play(audio_path + 'game_over.mp3')
                }
            }
        }()
    }
}()

window.addEventListener('load', application.sound.loadView, false)

