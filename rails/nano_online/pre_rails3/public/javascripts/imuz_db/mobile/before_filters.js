Filter = function(order, filter, params){
    this.param_views = params.exceptions || params.only
    this.type = !!params.exceptions ? false : true
    this.renderOnView = function(){
        for(index in this.param_views){
            if(this.param_views[index] == window.location.pathname){
                return this.type
            }
        }
    }
    if(this.renderOnView()){
        if(order == 'before'){
            filter()
        }
        else{
            window.addEventListener('load', filter, false)
        }
    }
}






