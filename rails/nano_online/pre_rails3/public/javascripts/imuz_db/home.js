/* Intro Screen Animation */
//-------------------------------------------------------------------------------------------
var easingFunction = "Expo";
var movementByState;
var bandMembers;
var bandCurrentStates;
var bandInitialPositions;
var bandMemberKind;
//-------------------------------------------------------------------------------------------
 $(document).ready(readyBandMember); 
//-------------------------------------------------------------------------------------------
function readyBandMember()
{
	var currentBandMember;

	animationCycles = 
		{
			0: {left: '-=6', top:'-=2'},			
			1: {left: '-=2', top:'-=5'},			
			2: {left:'+=6' , top:'+=2'},			
			3: {left:'+=2' , top:'+=5'},
			4: {left:'+=4', top:'+=3'},
			5: {left: '-=4', top:'-=3'},		
			6: {left: '-=10'},			
			7: {left: '-=8'},			
			8: {left:'+=10'},			
			9: {left: '-=4'},
			10: {left:'+=8'},
			11: {left:'+=4'},			
			12: {top: '-=5'},			
			13: {top:'+=10'},
			14: {top:'+=6'},												
			15: {top: '-=10'},			
			16: {top:'+=5'},
			17: {top: '-=6'}	
		};

	timePerCycle =
			[
				2000,
				1000,
				2000,
				1000,
				3000,
				3000,
				3000,
				1000,
				2000,
				2000,
				1000,
				3000,
				2000,
				1000,
				2000,
				1000,
				1000,
				2000		
			];

	BandMemberAnimationState = 0;	
	bandInitialPositions = [['90px', '60px'],['350px','20px'],['0px','25px'],['150px','35px']];	
	bandMembers = ["vocalist", "guitarrist", "guitarrist2", "bassist"];
	bandCurrentStates = [ Math.round(Math.random() * 17),
						            Math.round(Math.random() * 17),
						            Math.round(Math.random() * 17),
						            Math.round(Math.random() * 17) ];

	bandMemberKind = [Math.round(Math.random() * 2),1,2,Math.round(Math.random() * 2)];
	
	for (var c=0;c< bandMembers.length;c++) {
    currentBandMember = bandMembers[c];
    $('.' + currentBandMember).css('left', bandInitialPositions[c][0]);
    $('.' + currentBandMember).css('top' , bandInitialPositions[c][1]);
    $('.' + currentBandMember).css('display', 'block');
  }

  setTimeout(function(){updateState()}, 2000);
}
//-------------------------------------------------------------------------------------------
function callbackBandMember()
{
	var localState;
	
	for (var c=0;c< bandMembers.length;c++)
		{
			localState = bandCurrentStates[c];
			localState++;
			localState = localState % 6;
			bandCurrentStates[c] = localState;
		}
	updateState();
}
//-------------------------------------------------------------------------------------------
function updateState()
{
	var localState;	
	var currentBandMember;
	
	for (var c=0;c< bandMembers.length - 1 ;c++)
		{

			localState = bandCurrentStates[c];
			currentBandMember = bandMembers[c];
			var finalPosition =  animationCycles[(6 * bandMemberKind[c]) + localState ];
            $('.' + currentBandMember).show();
			$('.' + currentBandMember).gx( finalPosition, timePerCycle[(6 * bandMemberKind[c]) + localState], easingFunction, dummyFunction);
		}

	if ( bandMembers.length > 0)
		{
			var d = bandMembers.length - 1;
			localState = bandCurrentStates[d];
			currentBandMember = bandMembers[d];
			var finalPosition =  animationCycles[ (6 * bandMemberKind[c]) + localState ];
            $('.' + currentBandMember).show();            	
			$('.' + currentBandMember).gx( finalPosition, timePerCycle[(6 * bandMemberKind[c]) + localState], easingFunction, callbackBandMember);	
		}
}
//-------------------------------------------------------------------------------------------
function debug_monty (msg)
{
//	console.log(msg);
}
//-------------------------------------------------------------------------------------------
function dummyFunction()
{
	//DUMMY KICK!
}
//-------------------------------------------------------------------------------------------
