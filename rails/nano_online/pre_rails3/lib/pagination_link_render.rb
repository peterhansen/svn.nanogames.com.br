class PaginationListLinkRenderer < WillPaginate::LinkRenderer

  def to_html
    links = @options[:page_links] ? windowed_links : []

    links.unshift(page_link_or_span(@collection.previous_page, 'prev'))
    links.push(page_link_or_span(@collection.next_page, 'next'))

    html = links.join(@options[:separator])
    @options[:container] ? pag = @template.content_tag(:ul, html, html_attributes) : pag = html
    "<div class='pagination'>#{pag}</div>"
  end

protected

  def windowed_links
    visible_page_numbers.map { |n| page_link_or_span(n, (n == current_page ? nil : nil)) }
  end

  def page_link_or_span(page, span_class, text = nil)
    text ||= page.to_s
    if page && page != current_page
      page_link(page, text, :class => span_class)
    else
      if text.to_i != 0
        page_span(page, "<span class='current'>#{text}</span>", :class => span_class)
      else
        page_span(page, text, :class => span_class)                
      end
    end
  end

  def page_link(page, text, attributes = {})
    if attributes[:class] == "prev" || attributes[:class] == "next"
      text = ""
    end
    @template.content_tag(:li, @template.link_to(text, url_for(page)), attributes)
  end

  def page_span(page, text, attributes = {})
    if attributes[:class] == "prev" || attributes[:class] == "next"
      text = ""
    end
    @template.content_tag(:li, text, attributes)
  end
end
