task :change_phone => :environment do
  accesses = Access.find(:all)
      
    accesses.each do |access| 
        if access.phone_number
          access.phone_hash = access.phone_number.hash
          access.phone_number = access.phone_number[0..-3].insert(-1, "**")
          access.save
        end
      end
end