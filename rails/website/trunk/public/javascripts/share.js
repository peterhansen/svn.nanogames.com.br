var params = {};
var connection = null;

$(document).ready(function(){
   $("#facebook_share").click(shareOnFacebook);
   $("#twitter_share").click(shareOnTwitter);
   $("#orkut_share").click(shareOnOrkut);
   $("#googleplus_share").click(shareOnGoogleplus);
});

function getLocation(current_page){
    if (current_page){
        return window.location.href
    }
    else {
        return "http://" + window.location.host
    }
}

function shareOnOrkut(current_page){
    window.open("http://promote.orkut.com/preview?nt=orkut.com&du="+ getLocation(current_page), '_blank','width=1000,height=600,scrollbar=no')
}

function shareOnFacebook(current_page){
    window.open("http://www.facebook.com/sharer.php?u="+ "http://" + window.location.host, '_blank','width=1000,height=600,scrollbar=no')
}

function shareOnTwitter(current_page){
    window.open("http://twitter.com/intent/tweet?url="+ getLocation(current_page), '_blank','width=1000,height=600,scrollbar=no')
}

function shareOnMyspace(current_page){
    window.open("http://www.myspace.com/Modules/PostTo/Pages/?u="+ getLocation(current_page), '_blank','width=1000,height=600,scrollbar=no')
}
function shareOnGoogleplus(current_page){
    window.open("https://m.google.com/app/plus/x/?v=compose&content="+ getLocation(current_page), '_blank','width=1000,height=600,scrollbar=no') 
}