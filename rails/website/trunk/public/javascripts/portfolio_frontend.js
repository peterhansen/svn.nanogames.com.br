$(function() {
    Nano.config({host:'http://www.nanogames.com.br', port:'80'});

    var vendorSelector = $('#marca');
    var vendorOptions = $('#marca').attr('options');
    var deviceSelector = $('#modelo');
    var deviceOptions = $('#modelo').attr('options');
    var vendorDefaultValue = 'Vendor';
    var nameShort = $('#name_short').val();

    Nano.Vendor.allFor(nameShort, function (vendors) {
        $.each(vendors, function(index, vendor) {
            // vendorSelector.append(new Option(vendor.name, vendor.id));
            vendorOptions[vendorSelector.length] = new Option(vendor.name, vendor.id);
        });
    });

    //  Download Area
    vendorSelector.change(function(e) {
        var vendor = $(e.target).find(':selected').val();
        deviceSelector.children().remove();
        // deviceSelector.append(new Option('Selecione o modelo', ''));
        deviceOptions[deviceOptions.length] = new Option('Selecione o modelo', '');

        Nano.Device.allFor(nameShort, vendor, function (devices) {
            $.each(devices, function(index, device) {
                // deviceSelector.append(new Option(device.name, device.id));
                deviceOptions[deviceOptions.length] = new Option(device.name, device.id);
            });
        });
    });

    $('#download_submit').click(function() {
        $(this).parent().attr('action', Nano.App.downloadPath());
        $('#model').val($('#modelo').find(':selected').val());
    });

    var portfolio_entry_url = "<%= portfolio_project_url(@master_entry, @portfolio_entry) if ::Refinery::Portfolio.multi_level? %>";
    var portfolioImagesCarousel = new Carousel("#portfolio_images", 10, 1);
    portfolioImagesCarousel.registerNextHandler($('#seta_dir'));
    portfolioImagesCarousel.registerPreviousHandler($('#seta_esq'));

    $("a[rel^='prettyPhoto']").prettyPhoto();
    $("#fb_share").click(function() {
        shareOnFacebook(window.location);
    });

    // Portfolio Navigation
    var GamesCarousel = new Carousel("#games_list", 7, 7);
    GamesCarousel.registerNextHandler($('#games_nav_dir'));
    GamesCarousel.registerPreviousHandler($('#games_nav_esq'));

    var AppsCarousel = new Carousel("#apps_list", 7, 7);
    AppsCarousel.registerNextHandler($('#apps_nav_dir'));
    AppsCarousel.registerPreviousHandler($('#apps_nav_esq'));

    if ($('#filter').val()) {
        setTimeout(function() {
            AppsCarousel.setFilter($('#filter').val());
            GamesCarousel.setFilter($('#filter').val());
            $('#app_categoria').click();
            $('#game_categoria').click();
        }, 1);
    }

    if ($('#filter').val()) {
        GamesCarousel.setFilter($('#filter').val());
    }

    $('#games').change(function(e) {
        var app = $(e.target).val();
        
        if (app != 'Select')
            window.location = '/portfolio/' + $(e.target).val();
    });

    $('#apps').change(function(e) {
        window.location = '/portfolio/' + $(e.target).val();
    });

    $('#games_categories').change(function(e) {
        GamesCarousel.setFilter($(e.target).val());
        $('#filter').val($(e.target).val());
    });

    $('#apps_categories').change(function(e) {
        AppsCarousel.setFilter($(e.target).val());
        $('#filter').val($(e.target).val());
    });

    $('#game_categoria').click(function() {
        $('#game_category_select').show();
        $('#game_title_select').hide();
    });

    $('#game_titulo').click(function() {
        $('#game_category_select').hide();
        $('#game_title_select').show();
    });

    $('#app_categoria').click(function() {
        $('#app_category_select').show();
        $('#app_title_select').hide();
    });

    $('#app_titulo').click(function() {
        $('#app_category_select').hide();
        $('#app_title_select').show();
    });

    $('.tab_switcher').click(function(e) {
        $('.list').hide();

        var link_html = $(e.currentTarget).children().html();
        var self = $(e.currentTarget);

        self.parent().removeClass();
        self.parent().addClass("abas " + link_html)
        $('#' + link_html).show();
        $('.tab_switcher').removeClass('aba_ativa');
        self.addClass('aba_ativa');
    });
});