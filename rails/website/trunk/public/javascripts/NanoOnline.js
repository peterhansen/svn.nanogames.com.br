var Nano = {
  App: App,
  Vendor: Vendor,
  Device: Device,
  host: 'http://online.nanogames.com.br',
  port: 80,
  format: '.json?page_size=99',

  config: function(data) {
    this.host = data.host;
    this.port = data.port;
  },

  get: function(path, params, callback) {
    var req;
    var url = this.host + ':' + this.port + path + params;

    if ($.browser.msie && window.XDomainRequest) {
      a = new XDomainRequest();
      a.onload = function() { callback($.parseJSON(a.responseText)); }
      a.open('get', url);
      a.send();
    } else {
      req = new XMLHttpRequest()
      req.open('GET', url, true);

      req.onreadystatechange = function (aEvt) {
        if (req.readyState == 4) {
           if(req.status == 200)
            callback($.parseJSON(req.responseText));
        }
      };
      req.send(null);
    }
  },

  hostWithPort: function() {
    return this.host + ':' + this.port;
  }
};

function App(id, name, nameShort) {
  this.id = id;
  this.name = name;
  this.nameShort = nameShort;
};

App.path = function () {
  return '/apps'
}

App.downloadPath = function() {
  return Nano.hostWithPort() + '/downloads/download_jad';
}

App.all = function(callback) {
  Nano.get(App.path() + Nano.format, '', function(response) {
    var apps = new Array();

    $.each(response, function(index, app) {
      apps.push(new App(app.id, app.name, app.name_short))
    });

    callback(apps);
  });
};

function Vendor(id, name) {
  this.id = id;
  this.name = name;
}

Vendor.path = function () {
  return '/vendors/list'
}

Vendor.allFor = function(app, callback) {
  Nano.get(Vendor.path() + Nano.format, '&app=' + app, function(response) {
    var vendors = new Array();

    $.each(response, function(index, vendor) {
      vendors.push(new Vendor(vendor.id, vendor.name));
    });

    callback(vendors);
  });
};

function Device(id, model) {
  this.id = id;
  this.model = model;
}

Device.path = function () {
  return '/devices/list'
}

Device.allFor = function(app, vendor_id, callback) {
  Nano.get(Device.path() + Nano.format, '&app=' + app + '&vendor=' + vendor_id, function(response) {
    var devices = new Array();

    $.each(response, function(index, device) {
      devices.push(new Vendor(device.id, device.name));
    });

    callback(devices);
  });
};