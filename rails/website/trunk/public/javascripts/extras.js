function debug(element){
  if(window.console){
    console.log(element);
  } 
  else{
    alert(element);
  }
}

var _oldhide = $.fn.hide;
$.fn.hide = function(speed, callback) {
    $(this).trigger('hide');
    return _oldhide.apply(this,arguments);
}

var _oldshow = $.fn.show;
$.fn.show = function(speed, callback) {
    $(this).trigger('show');
    return _oldshow.apply(this,arguments);
}

// $(document).ready(function() {
//     $(".numeric").keydown(function(event) {
//         // Allow only backspace and delete
//         if ( event.keyCode == 46 || event.keyCode == 8 ) {
//             // let it happen, don't do anything
//         } else {
//             // Ensure that it is a number and stop the keypress
//             if (event.keyCode < 48 || event.keyCode > 57 ) {
//                 event.preventDefault(); 
//             }   
//         }
//     });
// });

jQuery.fn.ForceNumericOnly = function() {
    return this.each(function() {
        $(this).keydown(function(e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, arrows, numbers and keypad numbers ONLY
            return (
                key == 8 || 
                key == 9 ||
                key == 46 ||
                (key >= 37 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

$(document).ready(function() {
  $('.numeric').ForceNumericOnly();
});