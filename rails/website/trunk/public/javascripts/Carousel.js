function Carousel (element, size, step) {
  this.element = element;
  this.size = size;
  this.position = 0;
  this.step = step || 1;
  this.draw();
  this.filter = null;
  this.previousButton = null;
  this.nextButton = null;
}

Carousel.prototype.next = function(){
  this.position = Math.min((this.position + this.step), this.childrenCount() - this.size);
  this.draw();
}

Carousel.prototype.previous = function(){
  this.position = Math.max((this.position - this.step), 0);
  this.draw();
}

// Era para ser private.
Carousel.prototype.draw = function () {
  this.hideAll();

  for(i = 0; i < this.size; i++)
    this.showChildOnPosition(this.position + i)

  if ((this.previousButton != null) && (this.position > 0)) {
    this.previousButton.parent().removeClass('seta_inativa');
  } else if (this.previousButton != null) {
    this.previousButton.parent().addClass('seta_inativa');
  }

  if ((this.nextButton != null) && (this.position >= this.childrenCount() - this.size)) {
    this.nextButton.parent().addClass('seta_inativa');
  } else if (this.nextButton != null) {
    this.nextButton.parent().removeClass('seta_inativa');
  }
}

// Era para ser private.
Carousel.prototype.hideAll = function () {
  $(this.element).children(':visible').hide();
}

// Era para ser private.
Carousel.prototype.showChildOnPosition = function (position) {
  $($(this.element).children(this.filter)[position]).show();
}

Carousel.prototype.registerNextHandler = function(element){
  this.nextButton = element;
  element.click($.proxy(this.next, this));
  this.draw();
}

Carousel.prototype.registerPreviousHandler = function(element){
  this.previousButton = element;
  element.click($.proxy(this.previous, this));
  this.draw();
}

Carousel.prototype.setStep = function(step) {
  this.step = step;
}

Carousel.prototype.setFilter = function (filter) {
  if (this.filterIsValid(filter)) {
    this.redefineLinks(filter);
    this.filter = '.' + filter;
  } else {
    this.clearLinks();
    this.filter = null;
  }

  this.draw();
}

Carousel.prototype.filterIsValid = function (filter) {
  return $(this.element).children('.' + filter).size() > 0
}

Carousel.prototype.redefineLinks = function (filter) {
  if (this.filter)
    var oldFilter = this.filter.replace('.', '');
  
  $(this.element).find('a').attr('href', function() {
    if(oldFilter) {
      return this.href.replace(oldFilter, filter);
    } else {
      return this.href + "?filter=" + filter;
    }
  });
}

Carousel.prototype.clearLinks = function () {
  $(this.element).find('a').attr('href', function() {
    return this.href.replace(/\?filter=.*/, '');
  });
}

Carousel.prototype.childrenCount = function() {
  return $(this.element).children(this.filter).size();
}