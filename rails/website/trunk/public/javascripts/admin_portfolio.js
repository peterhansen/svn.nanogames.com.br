Nano.config({host:'http://www.nanogames.com.br', port:'80'});

var oldValue;
var defaultSelect = 'New Nano App';
var appSelector = $('#portfolio_entry_name_short');
var nameShort = $('#name_short').val();
var title = $('#title').val();
var portfolioEntries = $('#portfolio_entries').val();

Nano.App.all(function(apps) {
  var options = appSelector.attr('options');

  if(isNull(apps)) {
    options[options.length] = new Option('No App Found', 1);
  } else {
    $.each(apps, function(index, app) {
      options[options.length] = new Option(app.name, app.nameShort);
    });
  }
  appSelector.find('option[value="' + nameShort + '"]').attr('selected',true);
});

function isNull(e) {
  return (e === null || e === undefined);
}

function notNull(e) {
  return !isNull(e);
}

$(function() {
  var downloadOrigin = $('#portfolio_entry_download_origin');
  var nanoRadio = $('#download_origin_nano');
  var noneRadio = $('#download_origin_none');
  var otherRadio = $('#download_origin_other');
  var text = $('#download_origin_text');
  
  if(downloadOrigin.val() == "Nano Online") {
    nanoRadio.attr('checked', 'checked');
    text.attr('disabled', 'disabled');
  } else if (downloadOrigin.val() == '') {
    noneRadio.attr('checked', 'checked');
  } else {
    otherRadio.attr('checked', 'checked');
    text.val(downloadOrigin.val());
  }

  nanoRadio.click(function() {
    nanoRadio.attr('checked', 'checked');
    otherRadio.attr('checked', '');
    noneRadio.attr('checked', '');
    downloadOrigin.val('Nano Online');
    text.attr('disabled', 'disabled');
  });

  noneRadio.click(function() {
    noneRadio.attr('checked', 'checked');
    otherRadio.attr('checked', '');
    nanoRadio.attr('checked', '');
    downloadOrigin.val('');
    text.attr('disabled', 'disabled');
  });

  otherRadio.click(function() {
    otherRadio.attr('checked', 'checked');
    nanoRadio.attr('checked', '');
    noneRadio.attr('checked', '');
    downloadOrigin.val(text.val());
    text.attr('disabled', '');
  });

  text.blur(function() {
    downloadOrigin.val(text.val());
  });
});