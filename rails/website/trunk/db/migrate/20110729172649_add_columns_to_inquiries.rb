class AddColumnsToInquiries < ActiveRecord::Migration
  def self.up
    add_column :inquiries, :ddd, :integer
  end

  def self.down
    remove_column :inquiries, :ddd
  end
end
