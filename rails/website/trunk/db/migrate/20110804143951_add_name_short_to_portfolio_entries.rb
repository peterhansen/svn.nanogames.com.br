class AddNameShortToPortfolioEntries < ActiveRecord::Migration
  def self.up
    add_column :portfolio_entries, :name_short, :string
  end

  def self.down
    remove_column :portfolio_entries, :name_short
  end
end
