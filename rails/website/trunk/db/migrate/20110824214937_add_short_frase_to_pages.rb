class AddShortFraseToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :short_frase, :text
  end

  def self.down
    remove_column :pages, :short_frase
  end
end
