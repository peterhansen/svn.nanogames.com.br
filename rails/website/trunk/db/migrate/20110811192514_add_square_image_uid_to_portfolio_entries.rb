class AddSquareImageUidToPortfolioEntries < ActiveRecord::Migration
  def self.up
    add_column :portfolio_entries, :square_image_uid, :string
  end

  def self.down
    remove_column :portfolio_entries, :square_image_uid
  end
end
