class AddBodyImageUidColumnToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :body_image_uid, :string
  end

  def self.down
    remove_column :pages, :body_image_uid
  end
end
