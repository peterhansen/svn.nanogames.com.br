class AddIconImageUidColumnToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :icon_image_uid, :string
  end

  def self.down
    remove_column :pages, :icon_image_uid
  end
end
