class AddInterestAreaColumnToJobApplications < ActiveRecord::Migration
  def self.up
    remove_column :job_applications, :interest
    add_column :job_applications, :interest_area, :integer
  end

  def self.down
    remove_column :job_applications, :interest_area
    add_column :job_applications, :interest, :string
  end
end
