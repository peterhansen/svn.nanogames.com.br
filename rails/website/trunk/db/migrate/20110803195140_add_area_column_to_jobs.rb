class AddAreaColumnToJobs < ActiveRecord::Migration
  def self.up
    add_column :jobs, :area, :integer
  end

  def self.down
    remove_column :jobs, :area
  end
end
