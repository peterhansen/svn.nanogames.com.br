class AddExcerptedIdColumnToPageParts < ActiveRecord::Migration
  def self.up
    add_column :page_parts, :excerpted_id, :integer
  end

  def self.down
    remove_column :page_parts, :excerpted_id
  end
end
