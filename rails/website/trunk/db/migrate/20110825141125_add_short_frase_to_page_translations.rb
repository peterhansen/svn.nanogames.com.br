class AddShortFraseToPageTranslations < ActiveRecord::Migration
  def self.up
    add_column :page_translations, :short_frase, :string
  end

  def self.down
    remove_column :page_translations, :short_frase
  end
end
