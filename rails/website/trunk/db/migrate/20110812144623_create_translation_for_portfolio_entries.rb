class CreateTranslationForPortfolioEntries < ActiveRecord::Migration
  def self.up
    PortfolioEntry.create_translation_table! :title => :string, :body => :text, :demo_url => :string
  end

  def self.down
    PortfolioEntry.drop_translation_table!
  end
end
