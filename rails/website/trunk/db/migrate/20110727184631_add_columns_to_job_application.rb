class AddColumnsToJobApplication < ActiveRecord::Migration
  def self.up
    add_column :job_applications, :ddd, :integer
    add_column :job_applications, :address, :string
    add_column :job_applications, :academic_formation, :string
    add_column :job_applications, :interest, :string
    add_column :job_applications, :how_found_us, :string
  end

  def self.down
    remove_column :job_applications, :how_found_us
    remove_column :job_applications, :interest
    remove_column :job_applications, :academic_formation
    remove_column :job_applications, :address
    remove_column :job_applications, :ddd
  end
end
