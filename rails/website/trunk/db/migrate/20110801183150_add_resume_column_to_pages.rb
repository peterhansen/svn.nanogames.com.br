class AddResumeColumnToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :resume, :text
  end

  def self.down
    remove_column :pages, :resume
  end
end
