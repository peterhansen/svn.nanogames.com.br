class AddDownloadOriginToPortfolioEntries < ActiveRecord::Migration
  def self.up
    add_column :portfolio_entries, :download_origin, :string
  end

  def self.down
    remove_column :portfolio_entries, :download_origin
  end
end
