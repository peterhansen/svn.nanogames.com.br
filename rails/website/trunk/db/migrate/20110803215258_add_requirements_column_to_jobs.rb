class AddRequirementsColumnToJobs < ActiveRecord::Migration
  def self.up
    add_column :jobs, :requirements, :text
    add_column :jobs, :active, :boolean
  end

  def self.down
    remove_column :jobs, :requirements
    remove_column :jobs, :active
  end
end
