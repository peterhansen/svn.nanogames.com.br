class AddPagePartIdToPlaceColumnToPages < ActiveRecord::Migration
  def self.up
    add_column :pages, :page_part_id, :integer
  end

  def self.down
    remove_column :pages, :page_part_id
  end
end
