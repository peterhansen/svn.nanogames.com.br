class AddFieldsToPortfolioEntries < ActiveRecord::Migration
  def self.up
    add_column :portfolio_entries, :demo_url, :string
    add_column :portfolio_entries, :header_image_uid, :string
    add_column :portfolio_entries, :thumb_image_uid, :string
  end

  def self.down
    remove_column :portfolio_entries, :thumb_image_uid
    remove_column :portfolio_entries, :header_image_uid
    remove_column :portfolio_entries, :demo_url
  end
end
