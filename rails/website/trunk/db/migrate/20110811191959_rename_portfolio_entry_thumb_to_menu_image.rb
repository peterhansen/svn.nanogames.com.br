class RenamePortfolioEntryThumbToMenuImage < ActiveRecord::Migration
  def self.up
    rename_column :portfolio_entries, :thumb_image_uid, :menu_image_uid
  end

  def self.down
    rename_column :portfolio_entries, :thumb_image_uid, :menu_image_uid
  end
end
