set :rails_env, "staging"
role :web, "staging.nanogames.com.br"
role :app, "staging.nanogames.com.br"
role :db, "staging.nanogames.com.br", :primary => true