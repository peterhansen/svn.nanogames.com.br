require "bundler/capistrano"

set :user, 'rails'
set :application, "website"
set :deploy_to, "/var/www/rails/#{application}"
set :current_path, "#{deploy_to}/current"
set :deploy_via, :export
set :repository,  "http://svn.nanogames.com.br/rails/website/trunk/"
set :scm, :subversion
set :scm_username, "nano_online_svn"
set :scm_password, 'nAnO_SvN123'
set :scm_verbose, true
set :use_sudo, false

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  desc 'Compile CSS & JS for public/assets/ (see assets.yml)'
  task :jammit do
    run "cd #{current_release}; bundle exec jammit --base-url http://nanogames.com.br"

    # For Apache content negotiation with Multiviews, we need to rename .css files to .css.css and .js files to .js.js.
    # They will live alongside .css.gz and .js.gz files and the appropriate file will be served based on Accept-Encoding header.
    run "cd #{current_release}/public/assets; for f in *.css; do mv $f `basename $f .css`.css.css; done; for f in *.js; do mv $f `basename $f .js`.js.js; done"
  end


  after "deploy:symlink", "deploy:jammit"
end