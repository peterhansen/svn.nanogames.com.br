module ApplicationHelper
  def menu_item_active?(page)
   	if ((params[:id] || params[:action]) == page.to_s)|| params[:section] == page.to_s || (@page && (@page.root.to_param == page.to_s))
      'ativo'
    end
  end

  def id_to(page)
    "id='#{@page.translation.title.parameterize}'" if @page && @page.translation.title
  end
end
