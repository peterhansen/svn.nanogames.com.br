# encoding: utf-8

module PortfolioEntryHelper
  def thumb_list_item_for(game)
    content_tag(:li, selected_class(game).merge(:style => "display:none;")) do
      content = content_tag(:span, '', :class => 'marcador')
      content << content_tag(:a, :href => "/portfolio/#{game.to_param}", :title => game.title) do
        image_tag(game.menu_image.url, :width => 221, :height => 80, :alt => '') if game.menu_image
      end
      content
    end  
  end	

  def categories_for_children(item)
    if item && item.children
      categories = item.children.map {|c| c.category_list }.flatten! || []
      categories -= ["Advergames",
                     "Games",
                     "Apps",
                     "Web e Social Games",
                     "Mobile Games",
                     "Smartphone e Tablet Games",
                     "Nano Volksphone Framework",
                     "Nano Online",
                     "Apps nativos Multi-plataforma",
                     "Apps Web Móveis",
                     "Nano Volksphone Framework",
                     "Nano Online"]
      return categories.any? ? (["Todos"] + categories.uniq.sort { |a, b| a.casecmp b }) : []
    end
  end

  def tab_active? tab_name
    # binding.pry
    return "aba_ativa" if @portfolio_entry.parent.blank? && (tab_name == "Games")

    "aba_ativa" if @portfolio_entry.parent && @portfolio_entry.parent.title == tab_name
  end

  def active_tab_class
    @portfolio_entry.parent.title if @portfolio_entry && @portfolio_entry.parent
  end

  def div_active? div_name
    return "display: block" if @portfolio_entry.parent.blank? && (div_name == "Games")

    if @portfolio_entry && @portfolio_entry.parent && @portfolio_entry.parent.title == div_name
      'display: block'
    else
      'display: none;' 
    end
  end

  def selected_class game
    selected = params[:id] == game.to_param ? ' selecionado' : ''
    {:class => game.category_list.join(" ") << selected}
  end
end