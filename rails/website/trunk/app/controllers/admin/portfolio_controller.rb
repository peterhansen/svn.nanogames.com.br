class Admin::PortfolioController < Admin::BaseController
  crudify :portfolio_entry,
          :order => 'lft ASC',
          :conditions => {:parent_id => nil},
          :sortable => true

  def globalize!
    if params[:l]    
      Thread.current[:globalize_locale] = params[:l]
    else
      Thread.current[:globalize_locale] = ::Refinery::I18n.default_frontend_locale
    end
  end
end