class PagesController < ApplicationController
  def home
    twitted = Twitter.user_timeline("nanostudios", {:include_rts => true}).first
    @twitter_status = if twitted && twitted.retweeted_status
        twitted.retweeted_status.text
    else
        twitted.text
    end

  rescue Twitter::NotFound
    # Fuckoff
  ensure
    error_404 unless (@page = Page.where(:link_url => '/').first).present?
  end

  def show
    @page = Page.find("#{params[:path]}/#{params[:id]}".split('/').last)

    if @page.try(:live?) || (refinery_user? && current_user.authorized_plugins.include?("refinery_pages"))
      # if the admin wants this to be a "placeholder" page which goes to its first child, go to that instead.
      if @page.skip_to_first_child && (first_live_child = @page.children.order('lft ASC').live.first).present?
        redirect_to first_live_child.url and return
      elsif @page.link_url.present?
        redirect_to @page.link_url and return
      end
    else
      error_404
    end
  end

  def services; end

  def service_details
    if @page = Page.find(params[:service] || 'games')
      @page = @page.children.first unless @page.children.empty?
      @menu_pages = @page.parent.children
      @menu_parents = @page.parent.parent.children
      entries = PortfolioEntry.tagged_with(@page.translations.first.title)
      @service_examples = entries.limit(3) if entries
    else
      raise 'Page not found'
    end
  end

  def contact;end
end