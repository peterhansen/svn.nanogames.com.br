require 'paperclip'
  
class JobApplication < ActiveRecord::Base
  HUMANIZED_COLUMNS = {:resume_file_name => "Resume"}

  def self.human_attribute_name(attribute)
    HUMANIZED_COLUMNS[attribute.to_sym] || super
  end
  validates_presence_of :name, :phone, :email, :cover_letter, :ddd, :interest_area
  validates_length_of :phone, :in => 7..8, :allow_blank => true
  validates_length_of :ddd, :in => 2..3, :allow_blank => true
  validates_format_of :email, :with => /^([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})$/i
  belongs_to :job, :class_name => "Job", :foreign_key => "job_id"

  has_attached_file :resume
end
