module Taggable
	extend ActiveSupport::Concern

	included do
		acts_as_taggable
	  acts_as_taggable_on :categories

	  attr_accessible :category_list
	end
end