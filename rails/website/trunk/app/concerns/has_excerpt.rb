module HasExcerpt
  extend ActiveSupport::Concern

  included do
    has_many :excerpts, :class_name => "PagePart"
    belongs_to :part_to_place, :class_name => "PagePart", :foreign_key => 'page_part_id'
    attr_accessible :resume, :icon_image, :body_image, :page_part_id, :part_to_place
    image_accessor :icon_image
    image_accessor :body_image

    accepts_nested_attributes_for :parts
  end
end