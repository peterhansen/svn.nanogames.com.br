module PortfolioExtensions
  extend ActiveSupport::Concern

  included do
    image_accessor :header_image
    image_accessor :menu_image
    image_accessor :square_image

    attr_accessible :title, :body, :images_attributes, :demo_url, :header_image, :menu_image, :parent_id,
                    :name_short, :download_origin, :square_image, :title_image_id

    translates :title, :body, :demo_url
  end
end