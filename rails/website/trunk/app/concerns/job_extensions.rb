module JobExtensions
  extend ActiveSupport::Concern

  included do
    AREAS = ["Web Developer", "Desginer", "Game Developer"]
    scope :active_jobs, where("active = 1 AND job_title != 'Generico'").order("created_at desc")
  end
  module InstanceMethods
    def resume
      description.split('p>')[1].split('</')[0]
    end
  end
end