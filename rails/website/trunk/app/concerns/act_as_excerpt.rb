module ActAsExcerpt
  extend ActiveSupport::Concern

  included do
   belongs_to :excerpted, :class_name => "Page"
   has_many :pages_placed, :class_name => "Page"
   attr_accessible :excerpted_id
  end
end