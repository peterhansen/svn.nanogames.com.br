describe("Carousel", function() {    
  beforeEach(function () {
    loadFixtures('portfolio.html');
    carousel = new Carousel('#image_list', 2);
  });

  it("With size 2 it shows 2 visible thumbs", function() {
    expect($('#image_list').children(':visible').size()).toEqual(2);
  });

  it("shows the next 2 thumbs", function() {
    carousel.next();
    expect($($('#image_list').children()[0])).toBeHidden();
    expect($($('#image_list').children()[1])).toBeVisible();
    expect($($('#image_list').children()[2])).toBeVisible();
    expect($($('#image_list').children()[3])).toBeHidden();
    expect(carousel.position).toEqual(1);
  });
  
  it("shows the previous 2 thumbs", function() {
    carousel.next();
    carousel.previous();
    expect($($('#image_list').children()[0])).toBeVisible();
    expect($($('#image_list').children()[1])).toBeVisible();
    expect($($('#image_list').children()[3])).toBeHidden();
    expect(carousel.position).toEqual(0);
  });

  it("don't allow next when on last position", function() {
    carousel.next();  
    carousel.next();
    carousel.next();
    carousel.next();
    carousel.next();
    expect($('#image_list').children(':visible').size()).toEqual(2);
  });

  it("don't allow previous when on first position", function() {
    carousel.next();  
    carousel.next();
    carousel.previous();
    carousel.previous();
    carousel.previous();
    expect(carousel.position).toEqual(0);
  });

  describe("next Key bindings", function() {
    beforeEach(function () {
      seta = $("#games_nav_dir");
    });
    
    it("register the next event handler", function(){
      spyOn(seta, 'bind');
      carousel.registerNextHandler(seta);
      expect(seta.bind).toHaveBeenCalled();
    });
      
    it("call next when click the next handler", function(){
      spyOn(carousel, 'next');
      carousel.registerNextHandler(seta);
      seta.click();
      expect(carousel.next).toHaveBeenCalled();
    });

    it("change carousel position when the next click", function(){
      carousel.registerNextHandler(seta);
      seta.click();
      expect(carousel.position).toEqual(1);
    })
  });
  describe("previous Key bindings", function() {
    beforeEach(function () {
      seta = $("#games_nav_esq");
    });
    
    it("register the next event handler", function(){
      spyOn(seta, 'bind');
      carousel.registerPreviousHandler(seta);
      expect(seta.bind).toHaveBeenCalled();
    });
      
    it("call next when click the next handler", function(){
      spyOn(carousel, 'previous');
      carousel.registerPreviousHandler(seta);
      seta.click();
      expect(carousel.previous).toHaveBeenCalled();
    });

    it("change carousel position when the next click", function(){
      carousel.next();
      carousel.registerPreviousHandler(seta);
      seta.click();
      expect(carousel.position).toEqual(0);
    })
  });

  describe("steps", function() {
    it("setStep() sets the step attribute", function() {
      carousel.setStep(4);
      expect(carousel.step).toEqual(4);
    });

    describe("to right", function() {
      it("show the next 2 elements when step is set to 2", function() {
        carousel.setStep(2);
        carousel.next();
        expect($($('#image_list').children()[0])).toBeHidden();
        expect($($('#image_list').children()[1])).toBeHidden();
        expect($($('#image_list').children()[2])).toBeVisible();
        expect($($('#image_list').children()[3])).toBeVisible();
        expect(carousel.position).toEqual(2);
      });

      it("should not show less than 'size' elements", function() {
        carousel = new Carousel('#image_list', 3, 2);
        carousel.next();
        expect($('#image_list').children(':visible').size()).toEqual(3);
      })
    });
      
    describe('to left', function () {
      it('hides the 2 rightmost elements', function () {
        carousel.next();
        carousel.next();
        carousel.previous();
        expect($($('#image_list').children()[0])).toBeHidden();
        expect($($('#image_list').children()[1])).toBeVisible();
        expect($($('#image_list').children()[2])).toBeVisible();
        expect($($('#image_list').children()[3])).toBeHidden();
        expect(carousel.position).toEqual(1);
      });
    });
  });

  describe('filter', function() {
    beforeEach(function () {
      carousel.setFilter('Category1');
    });
    
    it('show only the filtered itens', function() {
      expect($($('#image_list').children()[0])).toBeVisible();
      expect($($('#image_list').children()[1])).toBeHidden();
      expect($($('#image_list').children()[2])).toBeVisible();
      expect($($('#image_list').children()[3])).toBeHidden();
    });

    it('childrenCount should return 2', function() {
      expect(carousel.childrenCount()).toEqual(2);
    });

    it('shows all children when category is not found', function() {
      carousel.setFilter('notACategory');
      expect($($('#image_list').children()[0])).toBeVisible();
      expect($($('#image_list').children()[1])).toBeVisible();
      expect($($('#image_list').children()[2])).toBeHidden();
      expect($($('#image_list').children()[3])).toBeHidden();
    });
  });

  describe('childrenCount', function() {
    it("returns 4", function() {
      expect(carousel.childrenCount()).toEqual(4);
    });
  });

  describe('navigation buttons', function() {
    beforeEach(function () {
      carousel.registerPreviousHandler($('#games_nav_esq'));
      carousel.registerNextHandler($('#games_nav_dir'));
    });

    it('should register navigation elementes', function(){
      expect(carousel.previousButton).toEqual($('#games_nav_esq'));
      expect(carousel.nextButton).toEqual($('#games_nav_dir'));
    });

    describe('previous button', function() {
      it('should be disabled on init', function() {
        expect($('.seta_esq')).toHaveClass('seta_inativa');
      });

      it('should be enabled when hit next', function() {
        carousel.next();
        expect($('.seta_esq')).not.toHaveClass('seta_inativa');
      });

      it('should be disabled when returning to the first position', function() {
        carousel.next();
        carousel.previous();
        expect($('.seta_esq')).toHaveClass('seta_inativa');
      })
    });

    describe('next button', function() {
      it('should be enable on init', function() {
        expect($('.seta_dir')).not.toHaveClass('seta_inativa');
      });

      it('should be disabled when hit next 2 times', function() {
        carousel.next();
        carousel.next() ;
        expect($('.seta_dir') ).toHaveClass('seta_inativa');
      });

      it('should be enable when hit next 2 times and then previous', function() {
        carousel.next();
        carousel.next();
        carousel.previous();
        expect($('.seta_dir')).not.toHaveClass('seta_inativa');
      })
    });
  });
});