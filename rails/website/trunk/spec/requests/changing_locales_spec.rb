require 'spec_helper'

describe "ChangingLocales" do
	before(:each) do
    Page.create(:title => 'Home', :link_url => '/')
    Page.create(:title => 'Games', :link_url => '/games')
	  Page.create(:title => 'Apps', :link_url => '/apps')
	  User.make.save
	end

	context "in the english version of the site" do
		before(:each) do
		  I18n.locale = :en
		end
		  
    it "shows an 'change language' link" do
      # pending
      visit '/'
      page.should have_link('Mudar para Portugues')
    end

    it "changes the site for english if you click the link" do
      # pending
      visit '/'
      click_link 'Mudar para Portugues'
      page.should have_link('Change to English')
    end

    it "renders the same page" do
      # pending
		  Page.create(:title => 'Contact', :link_url => '/contact')
      visit '/contact'
      click_link 'Mudar para Portugues'
      current_path.should == '/pt-BR/contact'
    end
	end
end
