require 'machinist/active_record'

User.blueprint do
	username		{"person#{sn}"}
	email				{"person#{sn}@cucumber.com"}
	password		{'senha123'}
	password_confirmation		{'senha123'}
	roles				{[Role[:refinery]]}
end