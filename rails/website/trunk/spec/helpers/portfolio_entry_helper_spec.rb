require 'spec_helper'

describe PortfolioEntryHelper do
  it "does something" do
    portfolio_entry = mock( PortfolioEntry, 
                            :title => 'Brazookas', 
                            :menu_image => stub(:url => '/url/para/imagem.png'), 
                            :to_param => 'brazookas', 
                            :model_name => PortfolioEntry,
                            :category_list => ["Category1", "Category2"]).as_null_object
    helper.thumb_list_item_for(portfolio_entry).should eql(%q{<li class="Category1 Category2" style="display:none;"><span class="marcador"></span><a href="/portfolio/brazookas" title="Brazookas"><img alt="" height="80" src="/url/para/imagem.png" width="221" /></a></li>})
  end
end