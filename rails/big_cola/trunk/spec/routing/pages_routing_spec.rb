require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe PagesController do
  describe "routing" do
    it "recognizes and generates root" do
      { :get => "/" }.should route_to( :controller => "pages", :action => "main" )
    end
    
    it "routes everything to pages#main" do
      pending "Por algum motivo o teste nao passa, mas a rota funciona." do
        { :get => "/qualquer/coisa" }.should route_to( :controller => "pages", :action => "main")
      end
    end
  end
end