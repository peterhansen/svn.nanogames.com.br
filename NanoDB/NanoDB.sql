-- phpMyAdmin SQL Dump
-- version 2.8.2.4
-- http://www.phpmyadmin.net
-- 
-- Servidor: db.nanogames.com.br
-- Tempo de Gera��o: Set 17, 2008 as 06:47 AM
-- Vers�o do Servidor: 5.0.67
-- Vers�o do PHP: 4.4.7
-- 
-- Banco de Dados: `nanoonline`
-- 

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppAds`
-- 

CREATE TABLE `AppAds` (
  `idAppAd` mediumint(9) NOT NULL auto_increment,
  `date` datetime default NULL,
  `expireDate` datetime default NULL,
  `path` varchar(45) NOT NULL,
  `appAdvertisers_idAppAdvertisers` smallint(6) NOT NULL,
  PRIMARY KEY  (`idAppAd`,`appAdvertisers_idAppAdvertisers`),
  KEY `fk_appAds_appAdvertisers` (`appAdvertisers_idAppAdvertisers`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `AppAds`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppAdvertisers`
-- 

CREATE TABLE `AppAdvertisers` (
  `idAppAdvertiser` smallint(6) NOT NULL auto_increment,
  `version` varchar(10) NOT NULL,
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`idAppAdvertiser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `AppAdvertisers`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppCategories`
-- 

CREATE TABLE `AppCategories` (
  `idAppCategory` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  PRIMARY KEY  (`idAppCategory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `AppCategories`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppVersionAtIntegrator`
-- 

CREATE TABLE `AppVersionAtIntegrator` (
  `appVersions_idappVersions` mediumint(9) NOT NULL,
  `integrators_idIntegrator` tinyint(3) unsigned NOT NULL,
  `submissionDate` datetime default NULL,
  PRIMARY KEY  (`appVersions_idappVersions`,`integrators_idIntegrator`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `AppVersionAtIntegrator`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppVersionSupportsAdvertisers`
-- 

CREATE TABLE `AppVersionSupportsAdvertisers` (
  `appVersions_idAppVersions` mediumint(9) NOT NULL,
  `appAdvertisers_idAppAdvertisers` smallint(6) NOT NULL,
  PRIMARY KEY  (`appVersions_idAppVersions`,`appAdvertisers_idAppAdvertisers`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `AppVersionSupportsAdvertisers`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppVersionSupportsSeries`
-- 

CREATE TABLE `AppVersionSupportsSeries` (
  `appVersions_idappVersions` mediumint(9) NOT NULL,
  `series_idSerie` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`appVersions_idappVersions`,`series_idSerie`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `AppVersionSupportsSeries`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `AppVersions`
-- 

CREATE TABLE `AppVersions` (
  `idAppVersion` mediumint(8) unsigned NOT NULL,
  `applications_idApplication` smallint(5) unsigned NOT NULL,
  `date` datetime default NULL,
  `description` varchar(100) default NULL,
  `path` varchar(45) default NULL,
  `version` varchar(10) default NULL,
  PRIMARY KEY  (`idAppVersion`,`applications_idApplication`),
  KEY `fk_appVersions_applications` (`applications_idApplication`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `AppVersions`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `ApplicationHasCategory`
-- 

CREATE TABLE `ApplicationHasCategory` (
  `Application_idApplication` smallint(5) unsigned NOT NULL,
  `AppCategory_idAppCategory` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`Application_idApplication`,`AppCategory_idAppCategory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `ApplicationHasCategory`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Applications`
-- 

CREATE TABLE `Applications` (
  `idApplication` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(32) NOT NULL,
  `nameShort` varchar(4) NOT NULL,
  `description` varchar(45) default NULL,
  PRIMARY KEY  (`idApplication`),
  UNIQUE KEY `NAME` (`name`),
  UNIQUE KEY `NAMESHORT` (`nameShort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Applications`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Bands`
-- 

CREATE TABLE `Bands` (
  `idBand` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `frequency` int(10) unsigned default NULL,
  PRIMARY KEY  (`idBand`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Bands`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `BrazilStates`
-- 

CREATE TABLE `BrazilStates` (
  `idBrazilState` tinyint(3) unsigned NOT NULL auto_increment,
  `nameISO` varchar(2) NOT NULL,
  `nameFull` varchar(25) default NULL,
  PRIMARY KEY  (`idBrazilState`),
  UNIQUE KEY `NAME` (`nameISO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `BrazilStates`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Bugs`
-- 

CREATE TABLE `Bugs` (
  `idBug` smallint(5) unsigned NOT NULL auto_increment,
  `description` varchar(100) default NULL,
  `workaround` varchar(100) default NULL,
  PRIMARY KEY  (`idBug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Bugs`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `CLDC`
-- 

CREATE TABLE `CLDC` (
  `idCLDC` tinyint(3) unsigned NOT NULL auto_increment,
  `version` varchar(5) default NULL,
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`idCLDC`),
  UNIQUE KEY `VERSION` (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `CLDC`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Countries`
-- 

CREATE TABLE `Countries` (
  `idCountry` smallint(5) unsigned NOT NULL auto_increment,
  `nameISO` varchar(3) NOT NULL,
  `nameFull` varchar(25) default NULL,
  PRIMARY KEY  (`idCountry`),
  UNIQUE KEY `NAME` (`nameISO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Countries`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `CustomerHasApplication`
-- 

CREATE TABLE `CustomerHasApplication` (
  `Customer_idCustomer` mediumint(8) unsigned NOT NULL,
  `Application_idApplication` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`Customer_idCustomer`,`Application_idApplication`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `CustomerHasApplication`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `CustomerHasDevice`
-- 

CREATE TABLE `CustomerHasDevice` (
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  `Customer_idCustomer` int(10) unsigned NOT NULL,
  `imei` varchar(45) default NULL,
  PRIMARY KEY  (`Device_idDevice`,`Customer_idCustomer`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `CustomerHasDevice`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Customers`
-- 

CREATE TABLE `Customers` (
  `idCustomer` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(30) default NULL,
  `number` varchar(20) default NULL,
  `email` varchar(45) default NULL,
  `nickname` varchar(20) NOT NULL,
  `lastAccess` datetime default NULL,
  `totalAccess` mediumint(9) default NULL,
  PRIMARY KEY  (`idCustomer`),
  UNIQUE KEY `NICKNAME` (`nickname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Customers`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `DataTransmissions`
-- 

CREATE TABLE `DataTransmissions` (
  `idDataTransmission` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `maxSpeed` int(10) unsigned default NULL,
  PRIMARY KEY  (`idDataTransmission`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `DataTransmissions`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `DeviceHasBug`
-- 

CREATE TABLE `DeviceHasBug` (
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  `Bug_idBug` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`Device_idDevice`,`Bug_idBug`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `DeviceHasBug`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `DeviceHasOptionalAPI`
-- 

CREATE TABLE `DeviceHasOptionalAPI` (
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  `OptionalAPI_idOptionalAPI` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`Device_idDevice`,`OptionalAPI_idOptionalAPI`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `DeviceHasOptionalAPI`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `DeviceSupportsBand`
-- 

CREATE TABLE `DeviceSupportsBand` (
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  `Band_idBand` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`Device_idDevice`,`Band_idBand`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `DeviceSupportsBand`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `DeviceSupportsDataTransmission`
-- 

CREATE TABLE `DeviceSupportsDataTransmission` (
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  `DataTransmissions_idDataTransmission` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`Device_idDevice`,`DataTransmissions_idDataTransmission`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `DeviceSupportsDataTransmission`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Devices`
-- 

CREATE TABLE `Devices` (
  `idDevice` smallint(5) unsigned NOT NULL auto_increment,
  `CLDC_idCLDC` tinyint(3) unsigned default NULL,
  `MIDP_idMIDP` tinyint(3) unsigned default NULL,
  `Vendor_idVendor` tinyint(3) unsigned NOT NULL,
  `series_idSerie` tinyint(3) unsigned NOT NULL,
  `commercialName` varchar(20) default NULL,
  `model` varchar(20) NOT NULL,
  `screenWidth` smallint(5) unsigned default NULL,
  `screenHeightPartial` smallint(5) unsigned default NULL,
  `screenHeightFull` smallint(5) unsigned default NULL,
  `maxJarSize` smallint(5) unsigned default NULL,
  `memoryHeap` int(10) unsigned default NULL,
  `memoryImage` int(10) unsigned default NULL,
  `user-agent` varchar(20) default NULL,
  `hasPointerEvents` tinyint(1) default NULL,
  `storageSize` int(10) unsigned default NULL,
  `maxRecordSize` int(10) unsigned default NULL,
  PRIMARY KEY  (`idDevice`),
  UNIQUE KEY `MODEL` (`model`),
  KEY `vendor` (`Vendor_idVendor`),
  KEY `midp` (`MIDP_idMIDP`),
  KEY `cldc` (`CLDC_idCLDC`),
  KEY `fk_devices_series` (`series_idSerie`),
  KEY `USERAGENT` (`user-agent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Devices`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `IntegratorHasDevice`
-- 

CREATE TABLE `IntegratorHasDevice` (
  `Integrator_idIntegrator` tinyint(3) unsigned NOT NULL,
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  `identifier` varchar(20) default NULL,
  PRIMARY KEY  (`Integrator_idIntegrator`,`Device_idDevice`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `IntegratorHasDevice`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `IntegratorWorksWithOperator`
-- 

CREATE TABLE `IntegratorWorksWithOperator` (
  `Integrator_idIntegrator` tinyint(3) unsigned NOT NULL,
  `Operator_idOperator` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`Integrator_idIntegrator`,`Operator_idOperator`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `IntegratorWorksWithOperator`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Integrators`
-- 

CREATE TABLE `Integrators` (
  `idIntegrator` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  PRIMARY KEY  (`idIntegrator`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Integrators`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `MIDP`
-- 

CREATE TABLE `MIDP` (
  `idMIDP` tinyint(3) unsigned NOT NULL auto_increment,
  `version` varchar(5) default NULL,
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`idMIDP`),
  UNIQUE KEY `VERSION` (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `MIDP`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Modules`
-- 

CREATE TABLE `Modules` (
  `idModules` mediumint(9) NOT NULL auto_increment,
  `description` varchar(100) default NULL,
  `name` varchar(45) NOT NULL,
  `path` varchar(45) NOT NULL,
  PRIMARY KEY  (`idModules`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Modules`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `OperatorHasBand`
-- 

CREATE TABLE `OperatorHasBand` (
  `Operator_idOperator` tinyint(3) unsigned NOT NULL,
  `Band_idBand` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`Operator_idOperator`,`Band_idBand`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `OperatorHasBand`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `OperatorHasDevice`
-- 

CREATE TABLE `OperatorHasDevice` (
  `Operator_idOperator` tinyint(3) unsigned NOT NULL,
  `Device_idDevice` smallint(5) unsigned NOT NULL,
  PRIMARY KEY  (`Operator_idOperator`,`Device_idDevice`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- Extraindo dados da tabela `OperatorHasDevice`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Operators`
-- 

CREATE TABLE `Operators` (
  `idOperator` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  PRIMARY KEY  (`idOperator`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Operators`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `OptionalAPIs`
-- 

CREATE TABLE `OptionalAPIs` (
  `idOptionalAPI` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `description` varchar(100) default NULL,
  `jsrIndex` smallint(5) unsigned default NULL,
  PRIMARY KEY  (`idOptionalAPI`),
  UNIQUE KEY `JSR` (`jsrIndex`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `OptionalAPIs`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `RankingEntries`
-- 

CREATE TABLE `RankingEntries` (
  `idRankingEntry` int(10) unsigned NOT NULL auto_increment,
  `customers_idCustomer` mediumint(8) unsigned NOT NULL,
  `appVersions_idappVersions` mediumint(9) NOT NULL,
  `devices_idDevice` smallint(5) unsigned default NULL,
  `score` bigint(20) NOT NULL,
  PRIMARY KEY  (`idRankingEntry`),
  KEY `fk_rankingEntries_customers` (`customers_idCustomer`),
  KEY `fk_rankingEntries_appVersions` (`appVersions_idappVersions`),
  KEY `fk_rankingEntries_devices` (`devices_idDevice`),
  KEY `SCORE` (`score`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `RankingEntries`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Series`
-- 

CREATE TABLE `Series` (
  `idSerie` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`idSerie`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Series`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Vendors`
-- 

CREATE TABLE `Vendors` (
  `idVendor` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY  (`idVendor`),
  UNIQUE KEY `NAME` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Vendors`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `WallpaperCategories`
-- 

CREATE TABLE `WallpaperCategories` (
  `idWallpaperCategory` tinyint(4) NOT NULL auto_increment,
  `category` varchar(45) default NULL,
  PRIMARY KEY  (`idWallpaperCategory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `WallpaperCategories`
-- 


-- --------------------------------------------------------

-- 
-- Estrutura da tabela `Wallpapers`
-- 

CREATE TABLE `Wallpapers` (
  `idWallpaper` mediumint(9) NOT NULL auto_increment,
  `path` varchar(45) NOT NULL,
  `description` varchar(100) default NULL,
  `WallpaperCategories_idWallpaperCategory` tinyint(4) NOT NULL,
  PRIMARY KEY  (`idWallpaper`,`WallpaperCategories_idWallpaperCategory`),
  KEY `fk_Wallpapers_WallpaperCategories` (`WallpaperCategories_idWallpaperCategory`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- 
-- Extraindo dados da tabela `Wallpapers`
-- 

