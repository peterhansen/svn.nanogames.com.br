/**
 * ManageEventActivity
 * Tela de ger�ncia do evento atual.
 * Copyright 2011 - Nano Studio 
 * @author Daniel Monteiro
  */
package br.com.nanostudio.eticketando;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import br.com.nanostudio.eticketando.engine.DataModel;
import br.com.nanostudio.eticketando.engine.Event;
import br.com.nanostudio.eticketando.engine.TicketCostumer;

public class ManageEventActivity extends Activity implements OnClickListener {
	
	/**
	 * Evento sendo atualmente trabalhado
	 */
	private Event currentEvent;
	private Button btnGotoTicketList;
	private Button btnGotoManualValidation;
	private Button btnGotoScan;	
	private Button btnLogOut;
	private Button btnBack;	
	private TextView tvPlace;
	private TextView tvTime;
	private TextView tvDate;
	private TextView tvFilters;
	private TextView tvEventName;
	private TextView tvAttending;
	private TextView tvSold;
	
    /** 
     * Called when the activity is first created. 
     * */
    @Override
    public void onCreate( Bundle savedInstanceState ) {    	
        super.onCreate(savedInstanceState);        
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.checkin_tickets_layout );        
        setupUIHandlers();
        loadCurrentEventData();
    }

    
    /**
     * Configura os callbacks da interface gr�fica
     */
    private void setupUIHandlers() {
    	btnGotoScan = (Button) findViewById( R.id.btnScan );    	
    	btnGotoScan.setOnClickListener( this );
    	btnGotoTicketList = (Button) findViewById( R.id.btnTicketList );
    	btnGotoTicketList.setOnClickListener( this );    	
    	btnGotoManualValidation = ( Button ) findViewById( R.id.btnManualValidation );
    	btnGotoManualValidation.setOnClickListener( this );    	
    	btnLogOut = ( Button ) findViewById( R.id.btnLogOut );
    	btnLogOut.setOnClickListener( this );
    	btnBack = ( Button ) findViewById( R.id.btnBack );
    	btnBack.setOnClickListener( this );	
    	
    	tvPlace = ( TextView ) findViewById( R.id.tvPlace );
    	tvTime = ( TextView ) findViewById( R.id.tvTime );
    	tvDate = ( TextView ) findViewById( R.id.tvDate );
    	tvFilters = ( TextView ) findViewById( R.id.tvFilters );
    	tvEventName = ( TextView ) findViewById( R.id.tvEventName );
    	tvAttending = ( TextView ) findViewById( R.id.tvCheckedTickets );
    	tvSold = ( TextView ) findViewById( R.id.tvSoldTickets );
	}
    
	@Override
	public void onClick(View v) {
		
		switch ( v.getId() ) {
		case R.id.btnLogOut:
			this.finish();				
			break;
		case R.id.btnBack:
			this.finish();
			break;
			case R.id.btnScan:
				Intent intent = new Intent("com.google.zxing.client.android.SCAN");
				intent.setPackage("com.google.zxing.client.android");
				intent.putExtra("SCAN_MODE", "DATA_MATRIX_MODE");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
				intent.putExtra("SCAN_MODE", "ONE_D_MODE");
				startActivityForResult( intent, 0 );		
				break;
			case R.id.btnManualValidation:
				EticketandoMainActivity.getInstance().navigateTo( this , ManualValidationActivity.class );
			break;				
			case R.id.btnTicketList:
				EticketandoMainActivity.getInstance().navigateTo( this , TicketListActivity.class );
			break;
			
		}
	}
	


	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		startActivityForResult(intent, 0);
        if ( requestCode == 0 ) {
            if ( resultCode == RESULT_OK ) {

            	String contents = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");
//                
                DataModel.getInstance().setCurrentCode( contents );
                startActivity( new Intent( this, ManualValidationActivity.class ) );
            } 
        }
    }	

	/**
     * Adiciona entrada. tmp.
     */
    private void addEntry( String entry ) {
//    	LinearLayout ll = (LinearLayout) findViewById( R.id.buyers_list );
//    	TextView tv = new TextView( getBaseContext() );
//    	tv.setText( entry );
//    	tv.setTextColor( 0 );
//    	ll.addView( tv );
//    	ll.postInvalidate();
    }
    
    public void updateUI() {
//    	tvPlace = ( TextView ) findViewById( R.id.tvPlace );
//    	tvTime = ( TextView ) findViewById( R.id.tvTime );
//    	tvDate = ( TextView ) findViewById( R.id.tvDate );
//    	tvFilters = ( TextView ) findViewById( R.id.tvFilters );
//    	tvEventName = ( TextView ) findViewById( R.id.tvEventName );    	
    	
    	tvPlace.setText( currentEvent.getPlace() );
    	tvTime.setText( currentEvent.getTime() );
    	tvDate.setText( currentEvent.getDate() );
    	tvEventName.setText( currentEvent.getName() );
    	
    	tvAttending.setText( currentEvent.getAttendingCostumersCount() + "/" + currentEvent.getExpectedCostumersCount() );
    	tvSold.setText( currentEvent.getExpectedCostumersCount() + "/" + currentEvent.getTotalTickets() );

    }
    
    /**
     * Carrega os dados do evento atual
     */
	private void loadCurrentEventData() {
		currentEvent = DataModel.getInstance().getEventWithId( 0 );
		updateUI();
	}
	
	/**
	 * Sink hole padr�o para tratamento de erros.
	 */
	private void onError() {
		//exibe Activity de erro?
	}
}