/**
 * 
 */
package br.com.nanostudio.eticketando;

import java.util.ArrayList;

import br.com.nanostudio.eticketando.engine.DataModel;
import br.com.nanostudio.eticketando.engine.Event;
import br.com.nanostudio.eticketando.engine.TicketCostumer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.View.OnClickListener;

/**
 * @author danielmonteiro
 * 
 */
public class TicketListActivity extends Activity implements OnClickListener {

	private TableLayout buyersList;
	private Event currentEvent;
	private Button btLogOut;
	private Button btBack;

	/**
	 * Called when the activity is first created.
	 * */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature( Window.FEATURE_NO_TITLE );
		setContentView(R.layout.ticket_list_layout);
		setupUIHandlers();
		loadCurrentEventData();
	}

	private void setupUIHandlers() {
		buyersList = (TableLayout) findViewById(R.id.tlBuyers);
		btLogOut = ( Button ) findViewById( R.id.btnLogOut );
		btLogOut.setOnClickListener( this );
		btBack = ( Button ) findViewById( R.id.btnBack );
		btBack.setOnClickListener( this );
	}

	private void loadCurrentEventData() {
		currentEvent = DataModel.getInstance().getEventWithId(0);
		ArrayList<TicketCostumer> list;
		TicketCostumer tc;
		TableRow tr;
		TextView tv;
		Button btn;
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TicketTableRowView ttrv;
		
		list = currentEvent.getAllTicketsList();

		for (int c = 0; c < list.size(); ++c) {
			tc = list.get(c);

			tr = (TableRow) inflater.inflate(R.layout.ticket_table_row, null);
			tr.setLayoutParams( new LinearLayout.LayoutParams( LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT ) );
			
			tv = (TextView) tr.findViewById(R.id.tvBuyerName);
			tv.setText(tc.getName());
			tv.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			
			
			tv = (TextView) tr.findViewById(R.id.tvBuyerEmail);
			tv.setText(tc.getEmail());
			tv.setVisibility(View.VISIBLE);
			tv.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			tv = (TextView) tr.findViewById(R.id.tvBuyerCategory);
			tv.setText(tc.getCategory());
			tv.setVisibility(View.VISIBLE);
			tv.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			btn = (Button) tr.findViewById(R.id.btnForceValidate);
			btn.setVisibility(View.INVISIBLE);
			btn.setLayoutParams(new LinearLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, 0));
			btn.setOnClickListener( this );

			tr.setOnClickListener( this );
			
			ttrv = new TicketTableRowView( this );
			ttrv.setTicketCostumer( tc );
			ttrv.setTableRow( tr );
			ttrv.update();
			buyersList.addView( ttrv );
		}

		buyersList.postInvalidate();

	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

	}

	@Override
	public void onClick(View v) {

		if (v instanceof TableRow) {
			TableRow tr;
			TextView tv;
			Button btn;

			btn = (Button) v.findViewById(R.id.btnForceValidate);

			if (btn.getVisibility() == View.INVISIBLE) {

				btn.setVisibility(View.VISIBLE);
				btn.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

				tv = (TextView) v.findViewById(R.id.tvBuyerCategory);
				tv.setVisibility(View.INVISIBLE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, 0));

				tv = (TextView) v.findViewById(R.id.tvBuyerEmail);
				tv.setVisibility(View.INVISIBLE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, 0));

			} else {

				btn.setVisibility(View.INVISIBLE);
				btn.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, 0));

				tv = (TextView) v.findViewById(R.id.tvBuyerCategory);
				tv.setVisibility(View.VISIBLE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

				tv = (TextView) v.findViewById(R.id.tvBuyerEmail);
				tv.setVisibility(View.VISIBLE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

			}

			for (int c = 0; c < buyersList.getChildCount(); ++c) {

				if (!(buyersList.getChildAt(c) instanceof TicketTableRowView )
						|| ( ( TicketTableRowView ) buyersList.getChildAt(c) ).getTableRow() == v )
					continue;

				tr = (TableRow) ( ( TicketTableRowView ) buyersList.getChildAt(c) ).getTableRow();

				btn = (Button) tr.findViewById(R.id.btnForceValidate);
				btn.setVisibility(View.INVISIBLE);
				btn.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, 0));

				tv = (TextView) tr.findViewById(R.id.tvBuyerCategory);
				tv.setVisibility(View.VISIBLE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

				tv = (TextView) tr.findViewById(R.id.tvBuyerEmail);
				tv.setVisibility(View.VISIBLE);
				tv.setLayoutParams(new LinearLayout.LayoutParams(
						LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
			}
		} else {
			switch ( v.getId() ) {
			case R.id.btnForceValidate: {
				TicketCostumer tc;
				LinearLayout ll1 = ( LinearLayout ) v.getParent();
				LinearLayout ll2 = ( LinearLayout ) ll1.getParent();
				TableRow p = ( TableRow ) ll2.getParent();
				TicketTableRowView ttrv = (TicketTableRowView) p.getParent();
				tc = ttrv.getTicketCostumer(); 
				
				if ( currentEvent.isAttending( tc ) ) {
					if ( currentEvent.takeTicketBack( tc ) ) {
						
					} else {
						//erro
					}
				} else {
					if ( currentEvent.attended( tc.getQRCode() ) ) {
					} else {
						//erro
					}
				}
				ttrv.update();
				
				
			}
				break;
			case R.id.btnLogOut:
				this.finish();				
				break;
			case R.id.btnBack:
				EticketandoMainActivity.getInstance().navigateTo( this , ManageEventActivity.class );
				break;
			}
		}
	}
}
