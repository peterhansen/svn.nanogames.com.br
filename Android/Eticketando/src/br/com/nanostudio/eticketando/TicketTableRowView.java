/**
 * 
 */
package br.com.nanostudio.eticketando;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TableRow;
import br.com.nanostudio.eticketando.engine.TicketCostumer;

/**
 * @author danielmonteiro
 *
 */
public class TicketTableRowView extends FrameLayout {

	public TicketTableRowView(Context context) {
		super(context);
 	}

	private TicketCostumer ticketCostumer;
	private TableRow tr;
	

	public TicketCostumer getTicketCostumer() {
		return ticketCostumer;
	}

	public void setTicketCostumer(TicketCostumer ticketCostumer) {
		this.ticketCostumer = ticketCostumer;
	}
	
	
	public void update() {
		ImageView iv = (ImageView) tr.findViewById( R.id.imgCheck );
		iv.setVisibility( ticketCostumer.getStatus() ? View.VISIBLE : View.INVISIBLE );
		tr.postInvalidate();
	}
	
	public TableRow getTableRow() {
		return tr;
	}
	
	public void setTableRow( TableRow tr ) {
		this.tr = tr;
		this.addView( tr );
	}
}
