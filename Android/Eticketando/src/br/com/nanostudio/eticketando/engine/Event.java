/**
 * Event
 * Um evento para ser gerenciado
 * Copyright 2011 Nano Studio
 * @author Daniel "Monty" Monteiro 
 */
package br.com.nanostudio.eticketando.engine;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Event {
	/**
	 * Constante indicando o nome do parametro de servidor para o id do evento
	 */
	private static final String TAG_EVENTID = "eventId";
	
	/**
	 * Lista de usu�rios ja presentes	
	 */
	private ArrayList< TicketCostumer > attendingCostumers;
	/**
	 * Usu�rios esperados.
	 */
	private ArrayList< TicketCostumer > expectedCostumers;
	/**
	 * Id de evento
	 */
	private int id;
	private String name;
	private String place;
	private String time;
	private String date;
	private long totalTickets;
	private ArrayList< String > filters;
	private HashMap < String, Boolean > valid;
/**
 * 	Constroi objeto do evento ja obtendo os dados do servidor
 * @param id do evento
 */
	public Event( int id ) {
		this.id = id;
		valid = new HashMap<String, Boolean>();
		attendingCostumers = new ArrayList< TicketCostumer >();		
		expectedCostumers = new ArrayList< TicketCostumer >();
		getServerData();
	}
	
/**
 * Constroi objeto do evento n�o apenas obtendo os dados do servidor, mas tamb�m acrescentando uma lista extra
 * @param id do evento
 * @param expected lista extra de usu�rios esperados
 */
	public Event( int id, ArrayList< TicketCostumer > expected ) {
		this( id );
		
		TicketCostumer tc;
		
		for ( int c = 0; c < expected.size(); ++c ) {
			tc = expected.get( c );
			expectedCostumers.add( tc );
		}
	}
	
	public int getId() {
		return id;
	}
	
	/**
	 * Indica que um novo usu�rio esta presente no evento
	 * @param user Usu�rio em quest�o
	 * @return 
	 */
	public boolean attended( String code ) {
		TicketCostumer tc = getExpectedCostumerByCode( code );
		if ( tc != null ) {
			
			if ( !expectedCostumers.contains( tc ) )
				return false;
			
			expectedCostumers.remove( tc );
			attendingCostumers.add( tc );
			
			if ( !attendingCostumers.contains( tc ) )
				return false;
			
		} else {
			return false;
		}
		
		tc.setStatus( true );
		return true;
	}
	
	
	/**
	 * Obtem ingresso ainda n�o validado pelo c�digo
	 * @param code C�digo equivalente ao QRCode
	 * @return Instancia do ticket
	 */
	//TODO: fazer uma pesquisa melhor.
	public TicketCostumer getExpectedCostumerByCode( String code ) {
		
		TicketCostumer tc = null;
		TicketCostumer candidate = null;
		
		for ( int c = 0; c < expectedCostumers.size() && tc == null; ++c ) {
			
			candidate = expectedCostumers.get( c );
			
			if ( candidate.getQRCode().equals( code ) ) {
				tc = candidate;
			}
		}
		
		return tc;
	}

	/**
	 * @return Quantos Tickets ja est�o presentes
	 */
	public int getAttendingCostumersCount() {
		return attendingCostumers.size();
	}

	/**
	 * @return Quantos Tickets ainda s�o esperados
	 */
	public int getExpectedCostumersCount() {
		return expectedCostumers.size();
	}
	
	
	private void getServerData() {
		
		if ( loadFromCache() )
			return;
		
		String JSONContent = null;
		String url = null; 
		JSONArray buyers;
		JSONObject buyer;
		Hashtable< String, String > buyerData;
		TicketCostumer tc;
		
		url = ServerProxy.BASE_URL + ServerProxy.EVENT_DATA_REQUEST + ServerProxy.PARAMS_START_MARKER  + TAG_EVENTID + ServerProxy.VALUE_START + id + ServerProxy.PARAMS_END;
		
		
		try {
				JSONContent = ServerProxy.getJSONFrom( url );
			
				buyers = new JSONArray( JSONContent );
				
				for ( int i = 0; i < buyers.length(); i++ ) {
					buyer = buyers.getJSONObject( i );
					buyerData = ServerProxy.parseJSON( buyer );
					//fa�o o passo indireto para garantir que se chegou at� aqui, � porque o parsing deu certo					
					tc = new TicketCostumer( buyerData ); 
					expectedCostumers.add( tc );
				}
				
			//TODO: exce��es de HTTP e de parsing de JSON
			} catch (IOException e) {
				Log.d("Eticketando" , "Exception " + e.getClass() + " : " + e.getMessage() + " : " + e.getCause() + " : " + e.getStackTrace() );
				onError();
			} catch (JSONException e) {
				Log.d("Eticketando" , "Exception " + e.getClass() + " : " + e.getMessage() + " : " + e.getCause() + " : " + e.getStackTrace() );
				onError();
			} 		
	}

	
	/**
	 * Tenta ler dados do cache, para evitar ficar baixando toda hora
	 * @return Se conseguiu tudo, retorna true. Se n�o conseguiu, descarta o lido e retorna false
	 */
	private boolean loadFromCache() {
		//TODO: ler dados do cache ( SQL? )
		return false;
	}

	private void onError() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @return Lista contendo copias de todos os tickets cadastrados. Tanto os que ja est�o presentes quanto os ainda n�o presentes
	 */
	public ArrayList<TicketCostumer> getAllTicketsListCopy() {
		TicketCostumer tc;
		ArrayList< TicketCostumer > toReturn = new ArrayList<TicketCostumer>();
		
		for ( int c = 0; c < this.getAttendingCostumersCount(); ++c ) {
			tc = new TicketCostumer( this.attendingCostumers.get( c ) );
			toReturn.add( tc );
		}
		
		for ( int c = 0; c < this.getExpectedCostumersCount(); ++c ) {
			tc = new TicketCostumer( this.expectedCostumers.get( c ) );
			toReturn.add( tc );
		}
		
		return toReturn;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTotalTickets() {
		return totalTickets;
	}

	public void setTotalTickets(long totalTickets) {
		this.totalTickets = totalTickets;
	}

	public ArrayList<TicketCostumer> getAllTicketsList() {
		TicketCostumer tc;
		ArrayList< TicketCostumer > toReturn = new ArrayList<TicketCostumer>();
		
		for ( int c = 0; c < this.getAttendingCostumersCount(); ++c ) {
			tc = ( this.attendingCostumers.get( c ) );
			toReturn.add( tc );
		}
		
		for ( int c = 0; c < this.getExpectedCostumersCount(); ++c ) {
			tc = ( this.expectedCostumers.get( c ) );
			toReturn.add( tc );
		}
		
		return toReturn;
	}

	public CharSequence getFiltersString() {
		String toReturn = "VIP";
		
//		for ( int c = 0; c < filters.size(); ++c ) {
//			
//			if ( c != 0 )
//				toReturn += " ";			
//			
//			if ( valid.get( filters.get( c ) ) )
//				toReturn += filters.get( c );
//		}
		
		return toReturn;
	}

	public TicketCostumer getCostumerByCode( String code ) {
		TicketCostumer tc = null;
		TicketCostumer candidate = null;
		
		for ( int c = 0; c < expectedCostumers.size() && tc == null; ++c ) {
			
			candidate = expectedCostumers.get( c );
			
			if ( candidate.getQRCode().equals( code ) ) {
				tc = candidate;
			}
		}
		
		for ( int c = 0; c < attendingCostumers.size() && tc == null; ++c ) {
			
			candidate = attendingCostumers.get( c );
			
			if ( candidate.getQRCode().equals( code ) ) {
				tc = candidate;
			}
		}
		
		return tc;
	}

	public boolean isAttending( TicketCostumer ticket ) {
		TicketCostumer candidate = null;
		for ( int c = 0; c < attendingCostumers.size(); ++c ) {
			
			candidate = attendingCostumers.get( c );
			
			if ( candidate.getQRCode().equals( ticket.getQRCode() ) ) {
				return true;
			}
		}

		return false;
	}

	public boolean takeTicketBack( TicketCostumer tc ) {
		
		if ( tc != null ) {
			
			if ( !attendingCostumers.contains( tc ) )
				return false;
			
			attendingCostumers.remove( tc );
			expectedCostumers.add( tc );
			
			if ( !expectedCostumers.contains( tc ) )
				return false;
			
		} else {
			return false;
		}
		tc.setStatus( false );
		return true;	
	}	
	
	public boolean takeTicketBack( String code ) {
		TicketCostumer tc = getExpectedCostumerByCode( code );
		if ( tc != null ) {
			
			if ( !expectedCostumers.contains( tc ) )
				return false;
			
			expectedCostumers.remove( tc );
			attendingCostumers.add( tc );
			
			if ( !attendingCostumers.contains( tc ) )
				return false;
			
		} else {
			return false;
		}
		
		tc.setStatus( false );
		return true;	
	}

	public boolean isAttending(String code ) {
		TicketCostumer tc = getCostumerByCode( code );
		return isAttending( tc );
	}
}
