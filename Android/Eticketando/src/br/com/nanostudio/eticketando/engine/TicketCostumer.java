/**
 * Representa o consumir ( ou na verdade, um ticket ) que esta tentando entrar no evento.
 * @author Daniel "Monty" Monteiro
 * Copyright 2011 Nano Studio
 */
package br.com.nanostudio.eticketando.engine;

import java.util.Hashtable;

import org.json.JSONObject;

/**
 * @author danielmonteiro
 *
 */
public class TicketCostumer {
	//Constantes
	/**
	 * Define o nome do cliente no JSON
	 */
	public static final String TAG_NAME = "buyer-name";
	/**
	 * Id do usu�rio/ticket(?) no servidor no JSON
	 */
	public static final String TAG_ID = "id";
	/**
	 * C�digo do ingresso no JSON
	 */
	public static final String TAG_QRCODE = "QRcode";
	/**
	 * Categoria do ticket ("VIP", "Pista", etc ) no JSON
	 */
	public static final String TAG_CATEGORY = "buyer-category";
	/**
	 * Status do ticket, no JSON
	 */
	public static final String TAG_STATUS = "status";
	
	public static final String TAG_EMAIL = "email";
	
	//Campos da classe
	/**
	 * Nome do caboclo
	 */
	private String name;
	/**
	 * Id do ticket
	 */
	private String id;
	/**
	 * C�digo do ingresso ( QRCode )
	 */
	private String QRCode;
	/**
	 * Categoria do ticket ("VIP", "Pista", etc )
	 */
	private String category;
	/**
	 * Status do ticket.
	 */
	private boolean status;
	
	
	private String email;
	/**
	 * Constroi a classe por meio de um dicion�rio JSON
	 * @param data String contendo o JSON
	 * @see ServerProxy.parseJSON
	 */
	public TicketCostumer( JSONObject data ) {
		this( ServerProxy.parseJSON( data ) );
	}

	
	/**
	 * Constroi a classe usando um dicion�rio
	 * @param data � o dicion�rio de informa��es do usu�rio 
	 */
	public TicketCostumer( Hashtable< String, String > data ) {
		setName( data.get( TAG_NAME ) );
		setId( data.get( TAG_ID ) );
		setQRCode( data.get( TAG_QRCODE ) );
		setCategory( data.get( TAG_CATEGORY ) );
		setEmail( data.get( TAG_EMAIL ) );
	}
	
	public TicketCostumer(TicketCostumer ticketCostumer) {
		setName( ticketCostumer.getName() );
		setId( ticketCostumer.getId() );
		setQRCode( ticketCostumer.getQRCode() );
		setCategory( ticketCostumer.getCategory() );
		setEmail( ticketCostumer.getEmail() );
	}

	/**
	 * 
	 * @return
	 */
	public String getQRCode() {
		return QRCode;
	}
	
	/**
	 * 
	 * @param qRCode
	 */
	public void setQRCode(String qRCode) {
		QRCode = qRCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 		
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Retorna representa��o do objeto na seguinte forma: "[ C�digo ] \t [ Nome do cliente ] \t [ Categoria ]" 
	 */
	public String toString() {
		String toReturn = "";
		toReturn += this.name + '\t' + this.category;
		return toReturn;
	}

	/**
	 * 
	 * @return
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * 
	 * @param category
	 */
	public void setCategory(String category) {
		this.category = category;
	}


	public void setStatus( String status ) {
		this.status = Boolean.getBoolean( status );
	}

	public void setStatus( boolean status ) {
		this.status = status;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public boolean getStatus() {
		return status;
	}
}
