/**
 * ServerProxy.java
 * Serve de centralizador das requisi��es para o servidor.
 * copyright 2011 - Nano Studio
 * @author Daniel "Monty" Monteiro
 */

package br.com.nanostudio.eticketando.engine;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class ServerProxy {
	
	/**
	 * URL basica da aplica��o. Todas as requisi��es HTTP s�o feitas em cima dessa URL
	 */
	public static final String BASE_URL = "http://jad.nanogames.com.br/eticketando/";
	/**
	 * Requisi��o: busca os dados do evento atual
	 */
	public static final String EVENT_DATA_REQUEST = "buyers.json";
	/**
	 * Indica fim de parametros
	 */
	public static final String PARAMS_END = ";";
	/**
	 * Indica come�o dos parametros
	 */
	public static final String PARAMS_START_MARKER = "?";
	/**
	 * Indica o come�o do valor do parametro
	 */
	public static final String VALUE_START = "=";	
	/**
	 * Indica separador de parametros
	 */
	public static final String PARAM_SEPARATOR = "&";

	/**
	 * Caracter de fim de linha
	 * */
	public static String NEW_LINE = System.getProperty("line.separator");
	
	/**
	 * Apenas obtem os dados recebidos pelo servidor, buscando em uma URL 
	 * @param addr a URL que se deseja cutucar ( contendo ou n�o parametros )
	 * @return Os dados resultantes da requisi��o, em formato JSON
	 * @throws IOException 
	 */
	public static String getJSONFrom( String addr ) throws IOException {
		
		String toReturn = "";
		HttpURLConnection request = null;
		URL url = null;
		
		url = new URL( addr );
		request = (HttpURLConnection) url.openConnection();
		request.setDoInput(true);
		request.connect();
		
		DataInputStream dis = new DataInputStream( request.getInputStream() );
		
		while ( dis.available() > 0 ) {
			toReturn += dis.readLine() /*+ NEW_LINE*/;
		}
		
		return toReturn;
	}
	
	
	/**
	 * Faz a decodifica��o do JSON
	 * @param JSONContent String contendo o JSON
	 * @return Hashtable< String, String >  contendo os pares key = value
	 */
	public static Hashtable< String, String > parseJSON( JSONObject object ) {
		Hashtable< String, String > toReturn = new Hashtable< String, String >();

		JSONArray objects = object.names();
		
		for ( int c = 0; c < objects.length(); ++c ) {
			try {
				toReturn.put( objects.getString( c ), object.getString( objects.getString( c ) ) );
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.d("Eticketando" , "Exception " + e.getClass() + " : " + e.getMessage() + " : " + e.getCause() + " : " + e.getStackTrace() );
			}
		}
		
		return toReturn;
	}
	
}
