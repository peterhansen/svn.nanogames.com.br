/**
 * 
 */
package br.com.nanostudio.eticketando.engine;

import java.util.ArrayList;

/**
 * @author Daniel "Monty" Monteiro
 *
 */
public class DataModel {
	
	/**
	 * 
	 * @return
	 */
	public static DataModel getInstance() {
		
		if ( instance == null ) {
			instance = new DataModel();
		}
		
		return instance;
	}
	
	private void getEventsSummary() {
		
		Event event = null;
		
		for ( int c = 0; c < 1; ++c ) {
			event = new Event( 0 );
			events.add( event );
			//tmp
			event.setName("The rise of ziggy stardust");
			event.setDate("10/12/1984");
			event.setPlace("Hammersmith-Odeon");
			event.setTime("11:38");
			event.setTotalTickets( 100 );
		}
	}
	
	private DataModel() {
		events = new ArrayList< Event >();
		getEventsSummary();
	}
	
	public Event getEventWithId( int id ) {
		
		if ( id < events.size() && events.get( id ).getId() == id )
			return events.get( id );
		
		for ( int c = 0; c < events.size(); ++c ) {
			if ( events.get( c ).getId() == id )
				return events.get( c );			
		}
		
		return null;
	}

	
	private ArrayList< Event > events;
	private String currentTicketCode;
	private static DataModel instance;
	
	public void setCurrentCode( String contents ) {
		currentTicketCode = contents;		
	}

	public String getCurrentCode() {		
		return currentTicketCode;
	}
}
