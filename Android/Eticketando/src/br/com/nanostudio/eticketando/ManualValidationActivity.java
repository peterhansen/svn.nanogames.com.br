/**
 * 
 */
package br.com.nanostudio.eticketando;

import br.com.nanostudio.eticketando.engine.DataModel;
import br.com.nanostudio.eticketando.engine.Event;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author danielmonteiro
 *
 */
public class ManualValidationActivity extends Activity implements OnClickListener {
	private Button btValidate;
	private EditText etCode;
	private TextView tvTicketInfo;
	private TextView tvEventName;
	private TextView tvDate;
	private TextView tvTime;	
	private TextView tvPlace;
	private TextView tvFilters;	
	private Button btBack;
	private Button btLogOut;
	
	private Event currentEvent;
	private TextView tvBuyerInformation;
    /** 
     * Called when the activity is first created. 
     * */
    @Override
    public void onCreate( Bundle savedInstanceState ) {    	
        super.onCreate(savedInstanceState);        
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.manual_validation_layout );        
        
        setupUIHandlers();
        loadCurrentEventData();
    }

    private void loadCurrentEventData() {
		currentEvent = DataModel.getInstance().getEventWithId( 0 );
		updateUI();
	}

	private void updateUI() {
    	tvPlace.setText( currentEvent.getPlace() );
    	tvTime.setText( currentEvent.getTime() );
    	tvDate.setText( currentEvent.getDate() );
    	tvEventName.setText( currentEvent.getName() );
    	tvFilters.setText( currentEvent.getFiltersString() );
    	
    	if ( DataModel.getInstance().getCurrentCode() != null && DataModel.getInstance().getCurrentCode() != "" ) {
    		etCode.setText( DataModel.getInstance().getCurrentCode() );
    		DataModel.getInstance().setCurrentCode("");
    	}
	}

	private void setupUIHandlers() {
		tvPlace = ( TextView ) findViewById( R.id.tvPlace );
		tvTime = ( TextView ) findViewById( R.id.tvTime );
		tvDate = ( TextView ) findViewById( R.id.tvEventDate );
		tvEventName = ( TextView ) findViewById( R.id.tvEventName );
		tvFilters = ( TextView ) findViewById( R.id.tvActiveFilters );
		tvBuyerInformation = ( TextView ) findViewById( R.id.tvBuyerInformation );
		
		btValidate = ( Button ) findViewById( R.id.btnValidate );
		btValidate.setOnClickListener( this );
		btBack = ( Button ) findViewById( R.id.btnBack );
		btBack.setOnClickListener( this );
		btLogOut = ( Button ) findViewById( R.id.btnLogOut );
		btLogOut.setOnClickListener( this );
		etCode = ( EditText ) findViewById( R.id.edtCode );
		
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	
    }

	@Override
	public void onClick(View v) {
		switch ( v.getId() ) {
		case R.id.btnValidate:
			 if ( currentEvent.isAttending( etCode.getText().toString() ) ) {
					tvBuyerInformation.setText( currentEvent.getCostumerByCode( etCode.getText().toString() ).toString().replace( '\t', '\n' ) + "\nTicket j� validado!" );				 
			 } else {
					if ( currentEvent.attended( etCode.getText().toString() ) ) {				
						tvBuyerInformation.setText( currentEvent.getCostumerByCode( etCode.getText().toString() ).toString().replace( '\t', '\n' ) );
					} else {
						tvBuyerInformation.setText( "C�digo n�o encontrado" );
					}				 
			 }
			
			break;
		case R.id.btnBack:
			EticketandoMainActivity.getInstance().navigateTo( this , ManageEventActivity.class );
//			startActivity( new Intent( this, ManageEventActivity.class ) );
			break;
		case R.id.btnLogOut:
			finish();
//			startActivity( new Intent( this, EticketandoMainActivity.class ) );
			break;		
		}		
	}
}
