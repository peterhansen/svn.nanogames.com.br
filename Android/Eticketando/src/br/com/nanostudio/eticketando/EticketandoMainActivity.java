package br.com.nanostudio.eticketando;

import br.com.nanostudio.eticketando.engine.Event;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

/**
 * Activity responsavel unicamente pelo r�pido splash da aplica��o e por dar o kickstart em todo o resto.
 * @author Daniel "Monty" Monteiro
 *
 */
public class EticketandoMainActivity extends Activity {
	
	public static EticketandoMainActivity getInstance() {
		return instance;
	}
	
    /** 
     * Called when the activity is first created. 
     * */
    @Override
    public void onCreate(Bundle savedInstanceState) {    	
        super.onCreate(savedInstanceState);        
        
        instance = this;
        
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        setContentView( R.layout.splash );
        
        showLogin();
    }

    /**
     * Carrega e exibe a tela de login ( na verdade, pede ao sistema para trocar para tal tela )
     */
	private void showLogin() {
		//TODO: exibir tela de login ao inv�s da tela de ger�ncia de evento		
		startActivity( new Intent( this, ManageEventActivity.class ) );
	}
	
	public void navigateTo( Activity from, Class activityClass ) {
		from.finish();
		startActivity( new Intent( this, activityClass ) );
	}

	private static EticketandoMainActivity instance;
}