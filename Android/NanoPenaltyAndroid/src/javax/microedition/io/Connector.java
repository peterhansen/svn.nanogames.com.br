//#if JAVA_VERSION == "ANDROID"
package javax.microedition.io;

import java.io.IOException;

/**
 *
 * @author danielmonteiro
 */
public class Connector {
    
public static final int	READ = 1;
public static final int	READ_WRITE = 3;
public static final int	WRITE = 2;

    public static Connection open( String address, int access, boolean timeouts ) throws IOException, Exception {
        return new HttpConnection( address, access, timeouts );
    }

    public static Connection open( String address) throws IOException, Exception {
        return new HttpConnection( address );
    }
}
//#endif