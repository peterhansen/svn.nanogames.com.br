//#if JAVA_VERSION == "ANDROID"
package javax.microedition.io;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author danielmonteiro
 */
public class HttpConnection extends Connection {
    

public static final String GET = "GET";
public static final String HEAD = "HEAD";
public static final int	HTTP_ACCEPTED = 202;
public static final int	HTTP_BAD_GATEWAY = 502;
public static final int	HTTP_BAD_METHOD	= 405;
public static final int	HTTP_BAD_REQUEST = 400;
public static final int	HTTP_CLIENT_TIMEOUT = 408;
public static final int	HTTP_CONFLICT = 409;
public static final int	HTTP_CREATED = 201;
public static final int	HTTP_ENTITY_TOO_LARGE = 413;
public static final int	HTTP_EXPECT_FAILED = 417;
public static final int	HTTP_FORBIDDEN = 403;
public static final int	HTTP_GATEWAY_TIMEOUT = 504;
public static final int	HTTP_GONE = 410;
public static final int	HTTP_INTERNAL_ERROR = 500;
public static final int	HTTP_LENGTH_REQUIRED = 411;
public static final int	HTTP_MOVED_PERM = 301;
public static final int	HTTP_MOVED_TEMP = 302;
public static final int	HTTP_MULT_CHOICE = 300;
public static final int	HTTP_NO_CONTENT	= 204;
public static final int	HTTP_NOT_ACCEPTABLE = 406;
public static final int	HTTP_NOT_AUTHORITATIVE = 203;
public static final int	HTTP_NOT_FOUND = 404;
public static final int	HTTP_NOT_IMPLEMENTED = 501;
public static final int	HTTP_NOT_MODIFIED = 304;
public static final int	HTTP_OK	= 200;
public static final int	HTTP_PARTIAL = 206;
public static final int	HTTP_PAYMENT_REQUIRED = 402;
public static final int	HTTP_PRECON_FAILED = 412;
public static final int	HTTP_PROXY_AUTH	= 407;
public static final int	HTTP_REQ_TOO_LONG = 414;
public static final int	HTTP_RESET = 205;
public static final int	HTTP_SEE_OTHER = 303;
public static final int	HTTP_TEMP_REDIRECT = 307;
public static final int	HTTP_UNAUTHORIZED = 401;
public static final int	HTTP_UNAVAILABLE = 503;
public static final int	HTTP_UNSUPPORTED_RANGE = 416;
public static final int	HTTP_UNSUPPORTED_TYPE = 415;
public static final int	HTTP_USE_PROXY = 305;
public static final int	HTTP_VERSION = 505;
public static final String POST = "POST";

private URL url;
private HttpURLConnection urlConnection;
public static int DEFAULT_TIMEOUT = 5000;

private boolean connected = false;

private OutputStream out = null;
private InputStream in = null;        

    public HttpConnection( String address ) throws Exception {                
        try {
            url = new URL( address );
            urlConnection = (HttpURLConnection)url.openConnection();            
            urlConnection.setDoOutput(true);            
            urlConnection.setDoInput(true);            
        } catch ( Exception e ) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, e );
            throw e;
        }        
    }

    public HttpConnection(String address, int access, boolean timeouts) throws Exception {
        try {
            url = new URL( address );
            urlConnection = (HttpURLConnection)url.openConnection();            
            urlConnection.setDoOutput(true);            
            urlConnection.setDoInput(true);            
            
            if ( timeouts ) {
                urlConnection.setReadTimeout( DEFAULT_TIMEOUT );
            }
            
        } catch ( Exception e ) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, e );
            throw e;
        }        
    }
            
    public void setRequestProperty(String k, String v) {
        urlConnection.addRequestProperty( k, v );
    }

    public OutputStream openDataOutputStream() throws Exception {
        

        DataOutputStream dos = null;        
        try {
            if ( !connected )  {
                connected = true;
                urlConnection.connect();
            }
            
            if ( out == null )
                out = urlConnection.getOutputStream();
            
            dos = new DataOutputStream( out );            
        } catch ( Exception ex ) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
//            try {
//                out.close();
//            } catch (IOException ex) {
//                Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
        return dos;        
    }

    public InputStream openDataInputStream() throws Exception{
        

        DataInputStream dis = null;
        try {
            if ( !connected )  {
                connected = true;
                urlConnection.connect();
            }
            
            if ( in == null )
                in = urlConnection.getInputStream();

            dis = new DataInputStream( in );  
        } catch (Exception ex) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
//            try {
//                in.close();
//            } catch (IOException ex) {
//                Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
        return dis;        
    }

    public int getResponseCode() throws Exception {    
         
        
        try {
            if ( !connected )  {
                connected = true;
                urlConnection.connect();
            }
                
           
            return urlConnection.getResponseCode();
        } catch ( Exception ex) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public void close() {
        try {
            connected = false;
            
            if ( in != null )
                in.close();
            
            in = null;
            
            if ( out != null )
                out.close();
            
            out = null;
            
            urlConnection.disconnect();
        } catch (IOException ex) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public InputStream openInputStream() throws Exception {        
        try {
            if ( !connected )  {
                connected = true;
                urlConnection.connect();
            }

            if ( in == null )
                in = urlConnection.getInputStream();
                       
            return in; 
        } catch ( Exception ex) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    public int getLength() throws Exception {
        if ( !connected ) 
            try {
                        if ( !connected )  {
                connected = true;
                urlConnection.connect();
            }

        } catch ( Exception ex) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

//        Log.d("NanoComponents", "Chamando o novo getLength de HttpConnection");
        return 0;
    }

    public void setRequestMethod(String method ) throws Exception {
        
        try {
            urlConnection.setRequestMethod(method);
        } catch ( Exception ex) {
            Logger.getLogger(HttpConnection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
}
//#endif