//#if JAVA_VERSION == "ANDROID"
package javax.microedition.io;

import java.util.HashMap;

import android.content.res.Resources;
import java.io.InputStream;

public class ResourceDictionary
{
	private static HashMap resourceForPath;
        private static HashMap streamPerAsset;
	private static Resources resourceGroup;
	
	static {
		resourceForPath = new HashMap();		
                streamPerAsset = new HashMap();
	}
	
	public static void setGroup(Resources resGroup) {		
		resourceGroup =  resGroup;
	}
	
	public static void addEntry(String path, int res) {
		resourceForPath.put(path, res);
	}
	
	public static int getResourceFor(String path) {
		return ((Integer) resourceForPath.get(path)).intValue();
	}

	public static Resources getCurrentResourceGroup() {
		return resourceGroup;
	}
        
        public static InputStream openResource( String path ) {
            int id = ResourceDictionary.getResourceFor( path );
            InputStream input = ResourceDictionary.getCurrentResourceGroup().openRawResource( id );
            streamPerAsset.put( input, new Integer( id ) );
            return input;
        }
        
        public static int getIdForRes( InputStream input ) {
            Integer intBox = (Integer) streamPerAsset.get( input );
            return intBox.intValue();
        }

        public static void removeResToIdEntry(InputStream input) {
            streamPerAsset.remove( input );
        }
}
//#endif