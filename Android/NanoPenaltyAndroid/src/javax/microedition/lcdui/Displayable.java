//#if JAVA_VERSION == "ANDROID"
package javax.microedition.lcdui;

/**
 *
 * @author danielmonteiro
 */
public interface Displayable {
    public int getWidth();
    
    public int getHeight();
    
}
//#endif