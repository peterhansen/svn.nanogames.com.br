//#if JAVA_VERSION == "ANDROID"
package javax.microedition.lcdui;

import android.view.View;
import br.com.nanogames.MIDP.MainActivity;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.midlet.MIDlet;


/**
 *
 * @author danielmonteiro
 */
public class Display {
    
    private static Display instance = null;

    private static ScreenManager current = null;

        
    static Canvas getCurrentDisplayable() {
        return current;
    }

    public static void refreshCurrent() {
        if ( current != null ) {
            current.createSurfaceView();
            instance.setCurrent( current );
        }
    }

        
    public Display () {
        
    }
    
    public static Display getDisplay( MIDlet midlet ) {
        
        if ( instance == null )
            instance = new Display();
        
        return instance;
    }

    public void setCurrent( ScreenManager displayable) {        
        MainActivity.getActivity().getContainerLayout().removeView( current );
        current = displayable;
        MainActivity.getActivity().getContainerLayout().addView( current );            
    }

    public Canvas getCurrent() {
        return current;
    }    
}
//#endif