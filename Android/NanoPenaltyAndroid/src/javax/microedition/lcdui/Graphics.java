//#if JAVA_VERSION == "ANDROID"
package javax.microedition.lcdui;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.util.Log;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.game.Sprite;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Graphics {
    /**
     * 
     */
    public static final int BASELINE = 64;
    /**
     * 
     */
    public static final int BOTTOM = 32;
    /**
     * 
     */
    public static final int DOTTED = 1;
    /**
     * 
     */
    public static final int HCENTER = 1;
    /**
     * 
     */
    public static final int LEFT = 4;
    /**
     * 
     */
    public static final int RIGHT = 8;
    /**
     * 
     */
    public static final int SOLID = 0;
    /**
     * 
     */
    public static final int TOP = 16;
    /**
     * 
     */
    public static final int VCENTER = 2;
    /**
     * 
     */
    private static Graphics instance = null;
    /**
     * 
     */
    private Paint paint;
    /**
     * 
     */
    private android.graphics.Canvas internalCanvas;
    /**
     * 
     */
    private Point translate;
    
    
    /**
     * 
     */
    public Graphics() {
        paint = new Paint();
        translate = new Point();
        paint.setAntiAlias( false );
        paint.setFilterBitmap( true );
        paint.setDither( false );
//        paint.setStyle( Paint.Style.FILL );
    }
    
    
    /**
     * 
     * @param androidCanvas 
     */
    public Graphics( android.graphics.Canvas androidCanvas ) {
        this();
        setCurrentCanvas( androidCanvas );
    }
    
    
    /**
     * 
     * @param androidCanvas
     * @return 
     */
    public static Graphics getInstance( android.graphics.Canvas androidCanvas ) {
        
        if ( instance == null ) {
            instance = new Graphics();
        }
        
        instance.setCurrentCanvas( androidCanvas );
        
        return instance;
    }
    
    
    /**
     * 
     * @return 
     */
    public int getTranslateX() {
        return translate.x;
    }

    
    /**
     * 
     * @return 
     */
    public int getTranslateY() {
        return translate.y;
    }

    
    /**
     * 
     * @param x
     * @param y 
     */
    public void translate( int x, int y ) {
        translate.x = x;
        translate.y = y;
    }
    
    
    /**
     * 
     * @return 
     */
    public int getClipX() {
        Rect rect = internalCanvas.getClipBounds();
        return rect.left;
    }
    
    
    /**
     * 
     * @return 
     */
    public int getClipY() {
        Rect rect = internalCanvas.getClipBounds();
        return rect.top;
    }
    
    
    /**
     * 
     * @return 
     */
    public int getClipWidth() {
        Rect rect = internalCanvas.getClipBounds();
        return rect.right - rect.left;
    }

    
    /**
     * 
     * @return 
     */
    public int getClipHeight() {
        Rect rect = internalCanvas.getClipBounds();        
        return rect.bottom - rect.top;        
    }

    
    /**
     * 
     * @param color 
     */
    public void setColor(int color) {
        int r;
        int g;
        int b;
        
        r = color >> 16;
        g = color >> 8;
        b = color;
        
        setColor( r, g,b );
    }
    
    
    /**
     * 
     * @param i
     * @param j
     * @param k 
     */
    public void setColor(int i, int j, int k) {
        paint.setColor( Color.rgb(i, j, k) );
    }

    
    /**
     * 
     * @param x
     * @param y
     * @param x0
     * @param y0
     * @param startAngle
     * @param endAngle 
     */
    public void fillArc(int x, int y, int x0, int y0, short startAngle, short endAngle) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    
    /**
     * @see Label
     * @param image
     * @param x
     * @param y
     * @param anchor 
     */
    public void drawImage(Image image, int x, int y, int anchor ) {
        
        float _x = x;
        float _y = y;
        
        if ( anchor != TOP + LEFT && anchor != 0 ) {
//            Log.d( "NanoComponents", "ANCHOR:" + anchor );
//            throw new UnsupportedOperationException("Not yet implemented");
            
            if ( ( anchor & HCENTER ) == HCENTER )
                _x -= image.getWidth() / 2;
            
            if ( ( anchor & VCENTER ) == VCENTER )            
                _y -= image.getHeight() / 2;
            
            
            
        } 
            
        internalCanvas.drawBitmap(image.getRawBitmap(), _x /*+ 0.375f*/, _y /*+ 0.375f*/, paint );
    }

    
    /**
     * 
     * @param image
     * @param xSrc
     * @param ySrc
     * @param width
     * @param height
     * @param transformMIDP
     * @param xDst
     * @param yDst
     * @param anchor 
     */
    public void drawRegion( Image image, int xSrc, int ySrc, int width, int height, int transformMIDP, int xDst, int yDst, int anchor ) {        
        /// y + height <= bitmap.height. linha 327
//        if ( anchor != TOP + LEFT && anchor != 0 ) {
//            Log.d( "NanoComponents", "ANCHOR:" + anchor );
//            throw new UnsupportedOperationException("Not yet implemented");
//        }
 
        Rect dst = new Rect();
        Rect src = new Rect();
        
        src.left = xSrc;
        src.top = ySrc;
        src.right = xSrc + width;
        src.bottom = ySrc + height;
        
        dst.left = xDst;
        dst.top = yDst;
        dst.right = xDst + width;
        dst.bottom = yDst + height;        

        
        Matrix result = new Matrix();
        
        switch ( transformMIDP ) {
          case Sprite.TRANS_NONE:
            break;
          case Sprite.TRANS_MIRROR_ROT180:
            result.setScale(-1, 1);
            result.postTranslate( width, 0);
            result.postRotate(180);
            break;
          case Sprite.TRANS_MIRROR:
            result.setScale(-1, 1);
            result.postTranslate(width, 0);
            break;
          case Sprite.TRANS_ROT180:
            result.postRotate(180);
            break;
          case Sprite.TRANS_MIRROR_ROT270:
            result.setScale(-1, 1);
            result.postTranslate(width, 0);
            result.postRotate(270);
            break;
          case Sprite.TRANS_ROT90:
            result.postRotate(90);
            break;
          case Sprite.TRANS_ROT270:
            result.postRotate(270);
            break;
          case Sprite.TRANS_MIRROR_ROT90:
            result.setScale(-1, 1);
            result.postTranslate(width, 0);
            result.postRotate(90);
            break;
        }
   

        height = NanoMath.min( image.getHeight(), ( height ) );
        width = NanoMath.min( image.getWidth(), ( width ) );
        Bitmap tmp = Bitmap.createBitmap( image.getRawBitmap(), xSrc, ySrc, width, height, result, false );        
        internalCanvas.drawBitmap( tmp, xDst/* + 0.375f*/, yDst /*+ 0.375f*/, paint );    
    }

    
    /**
     * 
     * @param p1x
     * @param p1y
     * @param p2x
     * @param p2y
     * @param p3x
     * @param p3y 
     */
    public void fillTriangle(int p1x, int p1y, int p2x, int p2y, int p3x, int p3y) {
        paint.setStyle( Paint.Style.FILL_AND_STROKE ); 
        throw new UnsupportedOperationException("Not yet implemented");
    }

    
    /**
     * 
     * @param p1x
     * @param p1y
     * @param p2x
     * @param p2y 
     */
    public void drawLine(int p1x, int p1y, int p2x, int p2y) {
        paint.setStyle( Paint.Style.STROKE ); 
        internalCanvas.drawLine( p1x, p1y, p2x, p2y, paint);
        paint.setStyle( Paint.Style.FILL_AND_STROKE ); 
    }

    
    /**
     * 
     * @param x
     * @param y
     * @param w
     * @param h 
     */
    public void drawRect(int x, int y, int w, int h) {
        paint.setStyle( Paint.Style.STROKE );
        internalCanvas.drawRect(x, y, x + w, y + h, paint);
        paint.setStyle( Paint.Style.FILL_AND_STROKE ); 
    }
    
    
    /**
     * @see Label
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void clipRect(int x, int y, byte width, byte height) {
        
        RectF newClip = new RectF();
        newClip.top = y;
        newClip.left = x;
        newClip.bottom = y + height;
        newClip.right = x + width - 0.375f;
        
        internalCanvas.clipRect( newClip, Region.Op.INTERSECT );
    }
    
    
    /*
     * 
     */
    public void setClip(int x, int y, int width, int height) {
        
        Rect newClip = new Rect();
        newClip.top = y;
        newClip.left = x;
        newClip.bottom = y + height;
        newClip.right = x + width;
         
        internalCanvas.clipRect( newClip, Region.Op.REPLACE );
    }

    
    /**
     * 
     * @param androidCanvas 
     */
    private void setCurrentCanvas(Canvas androidCanvas) {
        internalCanvas = androidCanvas;
    }    
    
    
    /**
     * 
     * @param i
     * @param j
     * @param width
     * @param height 
     */
    public void fillRect(int i, int j, int width, int height) {
        paint.setStyle( Paint.Style.FILL ); 
	internalCanvas.drawRect(i, j, i + width,  j + height, paint);	
        paint.setStyle( Paint.Style.FILL_AND_STROKE );         
    }    
    
    
    /**
     * 
     * @param string
     * @param i
     * @param j
     * @param k 
     */
    public void drawString(String string, int i, int j, int k) {
        internalCanvas.drawText(string, i, j, paint);		
    }    
    
    
    /**
     * 
     * @param x
     * @param y
     * @param width
     * @param height
     * @param rs
     * @param rs2 
     */
    public void fillRoundRect(int x, int y, int width, int height, int rs, int rs2) {		
        paint.setStyle( Paint.Style.FILL_AND_STROKE ); 
        internalCanvas.drawRoundRect( new RectF( x, y, x + width, y + height ), rs, rs2, paint);
    }

    
    /**
     * 
     * @param color
     * @return 
     */
    public int getDisplayColor(int color) {
        return color;
    }

    
    /**
     * 
     * @param rgbData
     * @param offset
     * @param scanlength
     * @param x
     * @param y
     * @param width
     * @param height
     * @param processAlpha 
     */
    public void drawRGB(int[] rgbData, int offset, int scanlength, int x, int y, int width, int height, boolean processAlpha) {
        internalCanvas.drawBitmap( rgbData, offset, scanlength, x /*+ 0.375f*/, y /*+ 0.375f*/, width, height, processAlpha, paint );
    }

    public void clipRect(int x, int y, int width, int height) {
        RectF newClip = new RectF();
        newClip.top = y;
        newClip.left = x;
        newClip.bottom = y + height;
        newClip.right = x + width - 0.375f;
        
        internalCanvas.clipRect( newClip, Region.Op.INTERSECT );
    }
}
//#endif