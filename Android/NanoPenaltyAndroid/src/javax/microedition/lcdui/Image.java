//#if JAVA_VERSION == "ANDROID"
package javax.microedition.lcdui;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import javax.microedition.io.ResourceDictionary;
import java.io.InputStream;

/**
 *
 * @author danielmonteiro
 */
public class Image {
    
    private static BitmapFactory.Options options;
    
    static {
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = 1;
//        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
    }
    
    private android.graphics.Bitmap sprite;
    public Graphics graphics;
    
    public Image( Bitmap decodeResource )
    {
        sprite = decodeResource;
    }    

    public static Image createRGBImage(int[] data, int width, int height, boolean processAlpha ) {     
        throw new UnsupportedOperationException("Not yet implemented");        
//        byte[] blob = new byte[ ( data.length * Integer.SIZE ) / Byte.SIZE ];
//        
//        int d = 0;
//        int c = 0;
//        
//        while ( d < data.length ) {
//            
//            int a = ( data[ d ] >> 24 ) & 0xff;
//            int r = ( data[ d ] >> 16 ) & 0xff;
//            int g = ( data[ d ] >> 8  ) & 0xff;
//            int b = ( data[ d ]       ) & 0xff;
//             
//            blob[ c ] = (byte) ( a - 128 );
//            ++c;
//            blob[ c ] = (byte) ( r - 128 );
//            ++c;
//            blob[ c ] = (byte) ( g - 128 );
//            ++c;
//            blob[ c ] = (byte) ( b - 128 );
//            ++c;
//            
//            ++d;
//        }       
//        
//        Bitmap bmp = BitmapFactory.decodeByteArray( blob, 0, blob.length );
//        return new Image( bmp );
    }
    
    public int getHeight() {
        return sprite.getHeight();
    }

    public int getWidth() {
        return sprite.getWidth();
    }
    
    public static Image createImage( InputStream input ) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    public static Image createImage( Image image, int x, int y, int width, int height, int transform  ) {    
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public static Image createImage(int width, int height) {
        return new Image(Bitmap.createBitmap(width, height, Config.ARGB_8888 ) );
    }
    
    public static Image createImage( String path ) {    
        int resId = -1;
	resId = ResourceDictionary.getResourceFor( path );
	if ( resId != -1 ) {
            Image img = new Image( BitmapFactory.decodeResource( ResourceDictionary.getCurrentResourceGroup(), resId, options ) );
            return img;
        } else
            return null;
    }
    
    public static Image createImage(byte[] imageData, int imageOffset, int imageLength) {    
        byte[] blob = imageData;
        Bitmap bmp=BitmapFactory.decodeByteArray( blob, 0, blob.length );
        return new Image( bmp );        
    }

    
    public Graphics getGraphics() {
        
        if ( graphics == null ){
            android.graphics.Canvas canvas = new android.graphics.Canvas();
            canvas.setBitmap( sprite );
            graphics = new Graphics( canvas );
	}
        
    return graphics;
    }

    public void getRGB(int[] data, int offset, int stride, int x, int y, int width, int height) {
        sprite.getPixels( data, offset, stride, x, y, width, height );
    }

    public Bitmap getRawBitmap() {
        return sprite;
    }

    public boolean isMutable() {
        return true;
    }
    
}
//#endif