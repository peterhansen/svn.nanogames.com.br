//#if JAVA_VERSION == "ANDROID"
package javax.microedition.media.control;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;
import br.com.nanogames.MIDP.MainActivity;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.microedition.media.Control;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;

/**
 *
 * @author danielmonteiro
 */
public class VolumeControl implements Control {
    
    static {
        AudioManager am = (AudioManager) MainActivity.getActivity().getSystemService( Context.AUDIO_SERVICE );
        int max = am.getStreamMaxVolume( AudioManager.STREAM_MUSIC );
        am.setStreamVolume( AudioManager.STREAM_MUSIC, ( max / 2 ), ~( AudioManager.FLAG_PLAY_SOUND + AudioManager.FLAG_SHOW_UI ) );
    }

    public void setLevel(byte volume) {        
        AudioManager am = (AudioManager) MainActivity.getActivity().getSystemService( Context.AUDIO_SERVICE );
//        Log.d("Audio", "Audio volume is:" + am.getStreamVolume( AudioManager.STREAM_MUSIC ) + " of " + am.getStreamMaxVolume( AudioManager.STREAM_MUSIC ) );
        int max = am.getStreamMaxVolume( AudioManager.STREAM_MUSIC );
        int level = ( ( ( int ) volume ) * max ) / 100;
        am.setStreamVolume( AudioManager.STREAM_MUSIC, level, ~( AudioManager.FLAG_PLAY_SOUND + AudioManager.FLAG_SHOW_UI ) );        
    }
 

}
//#endif