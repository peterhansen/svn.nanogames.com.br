//#if JAVA_VERSION == "ANDROID"
package javax.microedition.media;

import android.media.MediaPlayer;
import android.util.Log;
import br.com.nanogames.components.userInterface.AppMIDlet;
import javax.microedition.media.control.VolumeControl;


/**
 *
 * @author danielmonteiro
 */
public class Player implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
    public static final int STARTED = 400;
    public int id = 0;
    private MediaPlayer mp;
    private int loopCount = 0;
    private static VolumeControl vc = null;
    
    static {
        if ( vc == null )
            vc = new VolumeControl();
    }
    
    public void setAndroidMediaPlayer( MediaPlayer mp ) {
        this.mp = mp;        
        
        if ( mp != null ) {
            mp.setOnCompletionListener( this );            
        }
    }    
    
    public void realize() {        
    }

    public void prefetch() {
//        try {
//            mp.prepare();
//        } catch (IOException ex) {
//            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IllegalStateException ex) {
//            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
//        } catch ( Exception e ) {
//            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, e );
//        }
    }

    public Control[] getControls() {
        return null;
    }

    
    public void deallocate() {
        if ( mp != null ) {
            mp.release();
            Manager.idToPlayer.remove( id );
            mp = null;
            AppMIDlet.gc();            
        }            
    }

    public void close() {
//        if ( mp != null && mp.isPlaying() ) {
//            mp.stop();
//            mp.reset();            
//        }
        stop();
    }

    public void stop() {
        if ( mp != null ) {
            mp.seekTo( 0 );
            mp.pause();   
        }
            
    }

    public void setMediaTime(int i) {
    }

    public void setLoopCount(int loopCount) {
        this.loopCount = loopCount;
    }

    public void start() {
        if ( mp != null ) {
            if ( mp.isPlaying() ) {
             //     ++loopCount;
            }              
            else
                mp.start();        

            if ( loopCount > 1 )
                mp.setLooping( true );            
        }        
    }
    
    public Control getControl( String controlStr ) {
        
        if ( controlStr.equals( "VolumeControl" ) )
            return vc;
        
        return null;
    }
    
    public int getState() {
        return 0;
    }

    public boolean onError(MediaPlayer mp, int what, int extra ) {
        switch ( what ) {
            
            case MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:                
//                break;
            case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
//                break;
            case MediaPlayer.MEDIA_ERROR_UNKNOWN:
//                break;
            case MediaPlayer.MEDIA_INFO_BAD_INTERLEAVING:
//                break;
            case MediaPlayer.MEDIA_INFO_NOT_SEEKABLE:
//                break;
//                break;
            case MediaPlayer.MEDIA_INFO_VIDEO_TRACK_LAGGING:
//                break;                
//                Log.d("NanoComponents", "MediaPlayer Error " + what + " extra " + extra );
                return true;
        }       
        return false;
    }

    public void onCompletion(MediaPlayer mp) {
        
        if ( loopCount > 0 )
            --loopCount;
        
        if ( loopCount <= 0 ) {
//            mp.stop();
            mp.setLooping( false );
        }
            
    }

    public MediaPlayer getAndroidMediaPlayer() {
        return mp;
    }
}
//#endif