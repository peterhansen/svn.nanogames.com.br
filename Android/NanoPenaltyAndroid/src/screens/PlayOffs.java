/*
 * Championship.java
 *
 * Created on October 9, 2007, 2:56 PM
 *
 */
package screens;

import core.PlayoffKey;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Serializable;
import core.Constants;
import java.io.DataInputStream;
import java.io.DataOutputStream;

//#if TOUCH == "true"
 import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 * @author peter
 */
public final class PlayOffs extends UpdatableGroup implements Constants, KeyListener, Serializable
	//#if TOUCH == "true"
 		, PointerListener
	//#endif
{

	public static final int EIGHTH = 8;
	public static final int QUARTER = 4;
	public static final int SEMI = 2;
	public static final int THIRD = 1;
	public static final int FINAL = 1;

	private final MUV speedX = new MUV();
	private final MUV speedY = new MUV();
	private final int SPEED;

	private final Label labelChampion;

	public static final byte CENTER = 0;
	public static final byte LEFT = 1;
	public static final byte RIGHT = 2;

	private final PlayoffKey[] keyEighth	= new PlayoffKey[EIGHTH];
	private final PlayoffKey[] keyQuarter	= new PlayoffKey[QUARTER];
	private final PlayoffKey[] keySemi		= new PlayoffKey[SEMI];
	private final PlayoffKey[] keyThird		= new PlayoffKey[THIRD];
	private final PlayoffKey[] keyFinal		= new PlayoffKey[FINAL];

	//#if SCREEN_SIZE != "SMALL"
		/** Última posição do ponteiro. */
		private final Point pointerPos = new Point();
	//#endif

	private final DrawableGroup group;

	private int playerTeamIndex = -1;


	public PlayOffs() throws Exception {
		super( 8 );

		SPEED = Math.max( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT ) / 3;

		group = new DrawableGroup( 70 );
		group.setSize( PLAYOFFS_TOTAL_WIDTH, PLAYOFFS_TOTAL_HEIGHT );
		
		insertDrawable( group );
		keyThird[ 0 ] = new PlayoffKey( CENTER, THIRD );
		
		for (int i = 0; i < FINAL; i++) {
			keyFinal[i] = new PlayoffKey(CENTER, FINAL);
		}
		setKeys();

		labelChampion = new Label( FONT_INDEX_DEFAULT, TEXT_CHAMPION_TITLE );
		labelChampion.setPosition( keyFinal[ 0 ].getRefPixelX() - ( labelChampion.getWidth() >> 1 ), keyFinal[ 0 ].getPosY() - labelChampion.getHeight() );
		group.insertDrawable( labelChampion );
	}


	public final PlayoffKey[] getKeys( int stage ) {
		switch ( stage ) {
			case STAGE_EIGHTH_FINALS:
				return keyEighth;

			case STAGE_QUARTER_FINALS:
				return keyQuarter;

			case STAGE_SEMI_FINALS:
				return keySemi;

			case STAGE_3RD_PLACE_MATCH:
				return keyThird;

			case STAGE_FINAL_MATCH:
				return keyFinal;

			default:
				return null;
		}
	}

	
	public final void setKeys() throws Exception {
		int keyInitialPositionX = 0;
		final int OFFSET_X = group.getWidth() - PlayoffKey.KEY_WIDTH;
		
		for ( byte oitavas = 0; oitavas < EIGHTH; oitavas++) {
			final boolean left = oitavas < EIGHTH / 2;
			keyEighth[oitavas] = new PlayoffKey( left ? LEFT : RIGHT, EIGHTH);
			group.insertDrawable(keyEighth[oitavas]);
			
			final int y = PlayoffKey.PLAYOFF_BOX_SPACE_Y + ( oitavas % ( EIGHTH >> 1 ) ) * keyEighth[ oitavas ].getHeight();
			keyEighth[oitavas].setPosition( left ? keyInitialPositionX : OFFSET_X - keyInitialPositionX, y );
		}

		keyInitialPositionX += PlayoffKey.KEY_WIDTH;
		for ( byte quartas = 0; quartas < QUARTER; quartas++) {
			final boolean left = quartas < QUARTER / 2;
			keyQuarter[quartas] = new PlayoffKey( left ? LEFT : RIGHT, QUARTER);
			group.insertDrawable(keyQuarter[quartas]);

			final int y = keyEighth[ quartas << 1 ].getPosY() + ( ( keyEighth[ quartas << 1 ].getHeight() - PlayoffKey.PLAYOFF_BOX_SPACE_Y ) >> 1 ) - ( PLAYOFF_BOX_HEIGHT >> 1 );
			keyQuarter[quartas].setPosition( left ? keyInitialPositionX : OFFSET_X - keyInitialPositionX, y );
		}

		keyInitialPositionX += PlayoffKey.KEY_WIDTH;
		for ( byte semi = 0; semi < SEMI; semi++) {
			final boolean left = semi < SEMI / 2;
			keySemi[semi] = new PlayoffKey( left ? LEFT : RIGHT, SEMI );
			group.insertDrawable(keySemi[semi]);

			final int y = keyQuarter[ semi << 1 ].getPosY() + ( ( keyQuarter[ semi << 1 ].getHeight() - PlayoffKey.PLAYOFF_BOX_SPACE_Y ) >> 1 ) - ( PLAYOFF_BOX_HEIGHT >> 1 );
			keySemi[semi].setPosition( left ? keyInitialPositionX : OFFSET_X - keyInitialPositionX, y );
		}

		group.insertDrawable(keyThird[0]);
		keyThird[ 0 ].setPosition( ( group.getWidth() - keyFinal[ 0 ].getWidth() ) >> 1, keyEighth[ ( keyEighth.length >> 1 ) - 1 ].getPosY() - PLAYOFF_BOX_HEIGHT );

		group.insertDrawable(keyFinal[0]);
		keyFinal[ 0 ].setPosition( ( group.getWidth() - keyFinal[ 0 ].getWidth() ) >> 1,
								   ( group.getHeight() >> 1 ) - keyFinal[ 0 ].getHeight() );
	}


	public final void setPlayerTeamIndex( int playerTeamIndex ) {
		this.playerTeamIndex = playerTeamIndex;
		refreshKeys();
		scrollToPlayer( playerTeamIndex );
	}


	public final void refreshKeys() {
		for ( byte i = 0; i < EIGHTH; ++i )
			keyEighth[i].refreshBorders( playerTeamIndex );
		for ( byte i = 0; i < QUARTER; ++i )
			keyQuarter[i].refreshBorders( playerTeamIndex );
		for ( byte i = 0; i < SEMI; ++i )
			keySemi[i].refreshBorders( playerTeamIndex );
		for ( byte i = 0; i < THIRD; ++i )
			keyThird[i].refreshBorders( playerTeamIndex );
		for ( byte i = 0; i < FINAL; ++i )
			keyFinal[i].refreshBorders( playerTeamIndex );
	}

	
	public final void update(int delta) {
		super.update(delta);
		
		final int diffX = speedX.updateInt(delta);
		final int diffY = speedY.updateInt(delta);
		setScroll( group.getPosX() + diffX, group.getPosY() + diffY );
	}


	public final void setScroll( int x, int y ) {
		final int LIMIT_X = 20;
		final int LIMIT_Y = 50;
		group.setPosition( NanoMath.clamp( x, -LIMIT_X + Math.min( -LIMIT_X, getWidth() - group.getWidth() ), LIMIT_X ),
						   NanoMath.clamp( y, -LIMIT_Y + Math.min( -LIMIT_X, getHeight() - group.getHeight() ), LIMIT_Y ) );
	}

	
	public final void keyPressed(int key) {
		switch ( key ) {
			case ScreenManager.KEY_NUM9:
				speedY.setSpeed( -SPEED );
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				speedX.setSpeed( -SPEED );
			break;

			case ScreenManager.KEY_NUM7:
				speedY.setSpeed( -SPEED );
			case ScreenManager.KEY_NUM4:
			case ScreenManager.LEFT:
				speedX.setSpeed( SPEED );
			break;

			case ScreenManager.KEY_NUM1:
				speedX.setSpeed( SPEED );
			case ScreenManager.KEY_NUM2:
			case ScreenManager.UP:
				speedY.setSpeed( SPEED );
			break;

			case ScreenManager.KEY_NUM3:
				speedX.setSpeed( -SPEED );
			case ScreenManager.KEY_NUM8:
			case ScreenManager.DOWN:
				speedY.setSpeed( -SPEED );
			break;
		}
	}


	public final void keyReleased(int key) {
		speedX.setSpeed( 0 );
		speedY.setSpeed( 0 );
	}


	//#if SCREEN_SIZE != "SMALL"
		public final void onPointerDragged(int x, int y) {
			setScroll( group.getPosX() + x - pointerPos.x, group.getPosY() + y - pointerPos.y );
			pointerPos.set(x, y);
		}


		public final void onPointerPressed(int x, int y) {
			pointerPos.set(x, y);
		}

		
		public final void onPointerReleased(int x, int y) {
		}
	//#endif


	/**
	 * Posiciona a tela na chave mais avançada do jogador.
	 * @param playerTeam
	 */
	public final void scrollToPlayer( int playerTeam ) {
		PlayoffKey[][] allKeys = new PlayoffKey[][] { keyEighth, keyQuarter, keySemi, keyThird, keyFinal };

		for ( int i = allKeys.length - 1; i >= 0; --i ) {
			final PlayoffKey[] keys = allKeys[ i ];
			
			for ( byte j = 0; j < keys.length; ++j ) {
				if ( keys[ j ].getTeam1Index() == playerTeam || keys[ j ].getTeam2Index() == playerTeam ) {
					setScroll( -keys[ j ].getRefPixelX() + ( getWidth() >> 1 ), -keys[ j ].getRefPixelY() + ( getHeight() >> 1 ) );
					return;
				}
			}
		}
	}


	public final boolean getPlayerMatch( int stage, int playerTeam, int[] match ) {
		PlayoffKey[] keys = null;
		switch ( stage ) {
			case STAGE_EIGHTH_FINALS:
				keys = keyEighth;
			break;

			case STAGE_QUARTER_FINALS:
				keys = keyQuarter;
			break;

			case STAGE_SEMI_FINALS:
				keys = keySemi;
			break;

			case STAGE_3RD_PLACE_MATCH:
				keys = keyThird;
			break;

			case STAGE_FINAL_MATCH:
				keys = keyFinal;
			break;

			default:
				return false;
		}
		
		for ( byte i = 0; i < keys.length; ++i ) {
			if ( keys[ i ].getTeam1Index() == playerTeam || keys[ i ].getTeam2Index() == playerTeam ) {
				match[ 0 ] = keys[ i ].getTeam1Index();
				match[ 1 ] = keys[ i ].getTeam2Index();
				return true;
			}
		}

		return false;
	}


	public final void write( DataOutputStream output ) throws Exception {
		for ( byte i = 0; i < EIGHTH; ++i )
			keyEighth[i].write( output );
		for ( byte i = 0; i < QUARTER; ++i )
			keyQuarter[i].write( output );
		for ( byte i = 0; i < SEMI; ++i )
			keySemi[i].write( output );
		for ( byte i = 0; i < THIRD; ++i )
			keyThird[i].write( output );
		for ( byte i = 0; i < FINAL; ++i )
			keyFinal[i].write( output );
	}


	public final void read( DataInputStream input ) throws Exception {
		for ( byte i = 0; i < EIGHTH; ++i )
			keyEighth[i].read( input );
		for ( byte i = 0; i < QUARTER; ++i )
			keyQuarter[i].read( input );
		for ( byte i = 0; i < SEMI; ++i )
			keySemi[i].read( input );
		for ( byte i = 0; i < THIRD; ++i )
			keyThird[i].read( input );
		for ( byte i = 0; i < FINAL; ++i )
			keyFinal[i].read( input );
	}
	
}// fim da classe Playoff
