/**
 * PenaltyForm.java
 *
 * Created on Jun 4, 2010 10:20:55 AM
 *
 */

package screens;

import android.util.Log;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.borders.DrawableBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import core.Border;
import core.Constants;

/**
 *
 * @author peter
 */
public final class PenaltyForm extends Form implements Constants, EventListener {

	private final Container contentContainer;

	private final Container buttonContainer;

	private final MarqueeLabel label;

	public static final byte BORDER_THICKNESS = 5;

	private final Component content;

	private final byte TITLE_BAR_HEIGHT;
    
    private int accTime;


	public PenaltyForm( Drawable content, String title, Button[] buttons ) throws Exception {
		label = new MarqueeLabel( GameMIDlet.GetFont( FONT_INDEX_DEFAULT ), title );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		final DrawableComponent labelComponent = new DrawableComponent( label );
		setTitleBar( labelComponent );

		final DrawableBorder b = new DrawableBorder( new Pattern( new DrawableImage( PATH_IMAGES + "barraverde.png" ) ) );
		b.set( BORDER_THICKNESS, 1, BORDER_THICKNESS, 1 );

		labelComponent.setBorder( b );

		TITLE_BAR_HEIGHT = ( byte ) ( label.getHeight() + b.getBorderHeight() );

		if ( content instanceof Component )
			this.content = ( Component ) content;
		else
			this.content = new DrawableComponent( content );

		contentContainer = new Container( 2 );
		setContentPane( contentContainer );
		contentContainer.insertDrawable( this.content );

		if ( buttons == null )
			buttons = new Button[ 0 ];
		buttonContainer = new Container( buttons.length );
		switch ( buttons.length ) {
			case 1:
				buttons[ 0 ].setId( ScreenManager.KEY_SOFT_RIGHT );
			break;

			case 2:
				buttons[ 0 ].setId( ScreenManager.KEY_SOFT_LEFT );
				buttons[ 1 ].setId( ScreenManager.KEY_SOFT_RIGHT );
			break;
		}
		for ( byte i = 0; i < buttons.length; ++i ) {
			buttons[ i ].addEventListener( this );
			buttonContainer.insertDrawable( buttons[ i ] );
		}
		contentContainer.insertDrawable( buttonContainer );
	}


	public final void setSize( int width, int height ) {
		if ( buttonContainer != null ) {
			size.set( width, height );

			buttonContainer.setSize( getWidth(), 40 );

			final byte TOTAL_BUTTONS = ( byte ) buttonContainer.getUsedSlots();
			switch ( TOTAL_BUTTONS ) {
				case 1:
					final Drawable b = buttonContainer.getDrawable( 0 );
					b.setPosition( ( getWidth() - b.getWidth() ) >> 1 , 0 );
				break;

				case 2:
					final Drawable b0 = buttonContainer.getDrawable( 0 );
					b0.setPosition( AbstractContainer.LAYOUT_START_X , 0 );
					final Drawable b1 = buttonContainer.getDrawable( 1 );
					b1.setPosition( getWidth() - b0.getWidth() - AbstractContainer.LAYOUT_START_X , 0 );
				break;
			}

			titleBar.setSize( getWidth(), TITLE_BAR_HEIGHT );
			titleBar.setPosition( 0, TITLE_BAR_SPACING_Y );

			label.setTextOffset( ( width - label.getFont().getTextWidth( label.getText() ) ) >> 1 );

			contentContainer.setPosition( BORDER_OFFSET, BORDER_OFFSET + TITLE_BAR_SPACING_Y + getTitleBar().getHeight() );
			contentContainer.setSize( getWidth() - ( BORDER_OFFSET << 1 ), getHeight() - getTitleBar().getHeight() - TITLE_BAR_SPACING_Y - ( BORDER_OFFSET << 1 ) );

			buttonContainer.setPosition( 0, contentContainer.getHeight() - buttonContainer.getHeight() );

			content.setSize( contentContainer.getWidth(), contentContainer.getHeight() - buttonContainer.getHeight() - BORDER_OFFSET );
		}
	}


	public final void eventPerformed( Event evt ) {
//        Log.d("Penalty","eventPerformed");
        if ( accTime < 150 )
            return;
        
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				content.keyPressed( evt.source.getId() );
			break;
		}
	}

    @Override
    public void update(int delta) {
        super.update(delta);
        
        accTime += delta;
    }

    @Override
    public void showNotify(boolean deviceEvent) {
        super.showNotify(deviceEvent);
        
        accTime = 0;
    }
    
    

}
