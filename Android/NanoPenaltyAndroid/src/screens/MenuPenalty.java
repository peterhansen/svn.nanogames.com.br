/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import core.ArrowGroup;
import core.Constants;
import java.util.Hashtable;

/**
 *
 * @author Edson
 */
public final class MenuPenalty extends Menu implements Constants, ScreenListener {

    /** Estrutura que armazena a última entrada selecionada de cada menu. A chave de cada entrada é o id do menu,
     * e o valor da chave é o índice da última entrada utilizada.
     */
    protected static final Hashtable LAST_INDEX = new Hashtable();
    private final int[] entries;
    private final MenuBar menuBar;
    private long accTime = 0;
    
    public MenuPenalty(MenuListener listener, int screen, int[] entries, int indexTitle) throws Exception {
        this(listener, screen, entries, indexTitle >= 0 ? GameMIDlet.getText(indexTitle) : null);
    }

    public MenuPenalty(MenuListener listener, int screen, int[] entries, String title) throws Exception {
        super(listener, screen, entries.length + 10);
        this.entries = entries;

        menuBar = new MenuBar();
        insertDrawable(menuBar);


        setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);

        if (title != null) {
            menuBar.setTitle(title);
        }

        final Integer lastIndex = (Integer) LAST_INDEX.get(new Integer(menuId));
        if (lastIndex == null) {
            setCurrentIndex(0);
        } else {
            final int index = lastIndex.intValue();
            if (entries.length > 2 && index == entries.length - 1) {
                setCurrentIndex(0);
            } else {
                setCurrentIndex(index);
            }
        }
    }

    public final void setCurrentIndex(int index) {
        if (index >= 0 && index < entries.length) {
            currentIndex = index;

            // grava o índice atual para futuras instâncias de menu com o mesmo id
            LAST_INDEX.put(new Integer(menuId), new Integer(currentIndex));

            if (listener != null) {
                listener.onItemChanged(this, menuId, index);
            }
        } // fim if ( index != currentIndex && index >= 0 && index < activeDrawables )

        // gambiarra usada para tratar a 1ª visualização da opção de volume
        switch (entries[ index]) {
            case TEXT_VOLUME:
                menuBar.setText(GameMIDlet.getText(entries[ index]) + MediaPlayer.getVolume() + '%');
                break;

            default:
                menuBar.setText(entries[ index]);
        }
    }

    public final void nextIndex() {
        if (currentIndex < entries.length - 1) {
            setCurrentIndex(currentIndex + 1);
        } else if (circular) {
            setCurrentIndex(0);
        }
    }

    public final void previousIndex() {
        if (currentIndex > 0) {
            setCurrentIndex(currentIndex - 1);
        } else if (circular) {
            setCurrentIndex(entries.length - 1);
        }
    }

    public final void setSize(int width, int height) {
        super.setSize(width, height);
        menuBar.setSize(size);
    }

    public final void setText(int textIndex) {
        menuBar.setText(textIndex);
    }

    public final void setText(String text) {
        menuBar.setText(text);
    }

    public final void keyPressed(int key) {
        switch (key) {
            case ScreenManager.RIGHT:
            case ScreenManager.KEY_NUM6:
                super.keyPressed(ScreenManager.DOWN);
                break;

            case ScreenManager.LEFT:
            case ScreenManager.KEY_NUM4:
                super.keyPressed(ScreenManager.UP);
                break;

            case ScreenManager.SOFT_KEY_LEFT:
                keyPressed(ScreenManager.KEY_NUM5);
                break;

            case ScreenManager.KEY_BACK:
            case ScreenManager.KEY_CLEAR:
            case ScreenManager.KEY_SOFT_RIGHT:
                setCurrentIndex(entries.length - 1);
                keyPressed(ScreenManager.FIRE);
                break;

            default:
                super.keyPressed(key);
        }
    }

    @Override
    public void onPointerDragged(int x, int y) {
//        super.onPointerDragged(x, y);
    }

    public final void onPointerPressed(int x, int y) {
        
        if ( accTime < 100 )
            return;
        
        if (y >= menuBar.labelGroup.getPosY() && y <= menuBar.labelGroup.getRefPixelY()) {
            switch (menuBar.arrowGroup.getActionAt(x)) {
                case ArrowGroup.INDEX_LEFT:
                    previousIndex();
                    break;

                case ArrowGroup.INDEX_CENTER:
                    keyPressed(ScreenManager.FIRE);
                    break;

                case ArrowGroup.INDEX_RIGHT:
                    nextIndex();
                    break;
            }
        }
    } // fim do método onPointerPressed( int, int )

    public final void hideNotify(boolean deviceEvent) {        
    }

    public final void showNotify(boolean deviceEvent) {
        accTime = 0;
    }

    public final void sizeChanged(int width, int height) {
        setSize(width, height);
    }

    @Override
    public void update(int delta) {
        super.update(delta);
        
        accTime += delta;
    }   
}
