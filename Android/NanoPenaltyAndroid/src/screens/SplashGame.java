/**
 * SplashGame.java
 *
 * Created on Jun 19, 2010 1:22:01 PM
 *
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.PointerListener;

import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.BkgMenu;
import core.Constants;

/**
 *
 * @author peter
 */
public final class SplashGame extends DrawableGroup implements Updatable, ScreenListener, Constants, KeyListener
	//#if TOUCH == "true"
 		, PointerListener
	//#endif
{
	private static final short TIME_ANIMATION = 1400;

	private final BezierCurve bezier = new BezierCurve();

	private short accTime;

	private final DrawableImage title;

	private final MarqueeLabel pressAnyKeyLabel;
	
	private static final byte STATE_LOGO_APPEARS		= 0;
	private static final byte STATE_TITLE_APPEARS	= 1;
	private static final byte STATE_PRESS_ANY_KEY	= 2;

	private byte state;

	private final Drawable logo;
	

	public SplashGame() throws Exception {
		super( 3 );
		//#if SCREEN_SIZE == "MEDIUM"
//
//    if (GameMIDlet.isLowMemory()) {
//			logo = null;
//		} else {
//			logo = BkgMenu.getLogo();
//		}
//
//		if (logo != null) {
//			insertDrawable(logo);
//		}
		//#else
 			logo = BkgMenu.getLogo();
 			insertDrawable(logo);

		//#endif

		title = new DrawableImage( PATH_SPLASH + "title.png" );
		title.defineReferencePixel( ANCHOR_TOP | ANCHOR_HCENTER );
		insertDrawable( title );

		pressAnyKeyLabel = new MarqueeLabel( FONT_INDEX_DEFAULT, TEXT_PRESS_ANY_KEY );
		pressAnyKeyLabel.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_ALWAYS );
		pressAnyKeyLabel.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT );
		insertDrawable( pressAnyKeyLabel );

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		setState( STATE_LOGO_APPEARS );
	}


	private final void setState( int state ) {
		this.state = ( byte ) state;
		accTime = 0;

		switch (state) {
			case STATE_LOGO_APPEARS:
				if (logo != null) {
					bezier.origin.set(getWidth() >> 1, -logo.getHeight());
				}
				bezier.control1.set(getWidth() >> 1, getHeight() >> 1);
				bezier.control2.set(getWidth() >> 1, getHeight());
				bezier.destiny.set(getWidth() >> 1, BkgMenu.getLogoY());
				if (logo != null) {
					logo.setRefPixelPosition(bezier.origin);
				}

				pressAnyKeyLabel.setVisible( false );
				title.setVisible( false );
			break;

			case STATE_TITLE_APPEARS:
				title.setVisible( true );
				if (logo != null) {
					logo.setRefPixelPosition(bezier.destiny);
				}

				bezier.origin.set( getWidth() >> 1, -title.getHeight() );
//				bezier.control1.set( getWidth() >> 1, getHeight() );
//				bezier.control2.set( getWidth() >> 1, getHeight() >> 1 );
				if (logo != null) {
				bezier.destiny.set( getWidth() >> 1, logo.getPosY() );}
			break;

			case STATE_PRESS_ANY_KEY:
				pressAnyKeyLabel.setTextOffset( getWidth() );
				pressAnyKeyLabel.setVisible( true );
			break;
		}
		update( 0 );
	}


	public final void update( int delta ) {
		accTime += delta;

		switch ( state ) {
			case STATE_LOGO_APPEARS:
			case STATE_TITLE_APPEARS:
				final Point p = new Point();
				bezier.getPointAtFixed( p, NanoMath.divInt( accTime, TIME_ANIMATION ) );

				if (state == STATE_LOGO_APPEARS) {
					if (logo != null) {
						logo.setRefPixelPosition(p);
					}
				} else {
					title.setRefPixelPosition( p );
				}

				if ( accTime >= TIME_ANIMATION )
					setState( state + 1 );
			break;

			case STATE_PRESS_ANY_KEY:
				pressAnyKeyLabel.update( delta );
			break;
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
        ScreenManager.setPointerListener( null );        
	}


	public final void showNotify( boolean deviceEvent ) {
        ScreenManager.setPointerListener( this );
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		pressAnyKeyLabel.setSize( width, pressAnyKeyLabel.getHeight() );
		pressAnyKeyLabel.setPosition( 0, getHeight() - pressAnyKeyLabel.getHeight() );
	}


	public final void keyPressed( int key ) {
		//#if DEBUG == "true"
			setState( STATE_PRESS_ANY_KEY );
		//#endif
		if ( state == STATE_PRESS_ANY_KEY )
			GameMIDlet.setScreen( SCREEN_LOADING_2 );
	}


	public final void keyReleased( int key ) {
	}


	public final void onPointerDragged( int x, int y ) {
	}


	public final void onPointerPressed( int x, int y ) {
		keyPressed( 0 );
	}


	public final void onPointerReleased( int x, int y ) {
	}

}
