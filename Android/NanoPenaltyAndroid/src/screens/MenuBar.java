/**
 * MenuBar.java
 *
 * Created on Jun 1, 2010 11:48:40 PM
 *
 */

package screens;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.util.BezierCurve;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.ArrowGroup;
import core.Constants;

/**
 *
 * @author peter
 */
public class MenuBar extends Container implements Constants, ScreenListener {

	protected final Pattern pattern;
	protected final RichLabel options;
	protected final RichLabel title;

	protected final UpdatableGroup labelGroup;

	protected final ArrowGroup arrowGroup;


	public MenuBar() throws Exception {
		super( 6 );

		pattern = new Pattern(new DrawableImage(PATH_IMAGES + "barrainicial.png"));

		labelGroup = new UpdatableGroup( 5 );
		insertDrawable( labelGroup );

		options = new RichLabel( GameMIDlet.GetFont( FONT_INDEX_BUTTON ), null/*, ScreenManager.SCREEN_WIDTH */);
        options.setSize( ScreenManager.SCREEN_WIDTH, labelGroup.getHeight() );
		labelGroup.insertDrawable(pattern);
		arrowGroup = new ArrowGroup();
		labelGroup.insertDrawable(arrowGroup);
		labelGroup.insertDrawable(options);

		title = new RichLabel( GameMIDlet.GetFont( FONT_INDEX_DEFAULT ), null/*, ScreenManager.SCREEN_WIDTH */);
        options.setSize( ScreenManager.SCREEN_WIDTH, labelGroup.getHeight() );
		insertDrawable( this.title );
	}


	public void setSize(int width, int height) {
		super.setSize(width, height);

		pattern.setSize(width, pattern.getHeight());
		labelGroup.setSize(width, pattern.getHeight());
		labelGroup.defineReferencePixel( ANCHOR_BOTTOM );
		labelGroup.setPosition(0, height - labelGroup.getHeight() - MENUBAR_BOTTOM_SPACING );

		arrowGroup.setSize( width, labelGroup.getHeight() );

		options.setPosition( arrowGroup.getInternalPosX(), ( labelGroup.getHeight() - options.getTextTotalHeight() ) >> 1 );
		options.setSize( arrowGroup.getInternalWidth(), labelGroup.getHeight() );

		if (title != null) {
			setTitle( title.getText() );
		}
	}


	public final void setText( int textIndex ) {
        if ( textIndex < 0 ) 
            setText( null );
        else
            setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setText( String text ) {
		if ( text == null ) {
			labelGroup.setVisible( false );
            options.setText("<ALN_H>Dummy", true );
			options.setSize( options.getWidth(), options.getTextTotalHeight() );
			options.setPosition( options.getPosX(), ( pattern.getHeight() - options.getHeight() ) >> 1 );			
            options.setText("");
		} else {
			labelGroup.setVisible( true );
			options.setText( "<ALN_H>" + text, true );
			options.setSize( options.getWidth(), options.getTextTotalHeight() );
			options.setPosition( options.getPosX(), ( pattern.getHeight() - options.getHeight() ) >> 1 );
		}
	}


	public final void setTitle( int titleIndex ) {
		setTitle( GameMIDlet.getText( titleIndex ) );
	}


	public final void setTitle( String title ) {
		if ( this.title != null ) {
//			this.title.setSize( this.title.getWidth(), getHeight() );
//			this.title.setText( "<ALN_H>" + title, true, true );            
			
			this.title.setText( "<ALN_H>" + title, true, true );                        
            this.title.setSize( getWidth(), this.title.getTextTotalHeight() + 3 * MENUBAR_BOTTOM_SPACING );
//			this.title.setPosition( ( getWidth() >> 1 ) - ( this.title.getWidth() >> 1 ), labelGroup.getPosY() - this.title.getHeight() >> 1 );
			this.title.setPosition( 0, labelGroup.getPosY() - this.title.getTextTotalHeight() - MENUBAR_BOTTOM_SPACING );            
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setAnimationActive( boolean active ) {
		arrowGroup.setAnimationActive( active );
	}


}
