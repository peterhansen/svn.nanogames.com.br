/**
 * Stadium.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:47:44 AM.
 */
package screens;

import android.util.Log;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import core.Ball;
import core.Constants;
import core.Control;
import core.GenericInfoBox;
import core.InfoBox;
import core.Keeper;
import core.Play;
import core.Player;
import core.ScoreBox;
import core.Team;
import javax.microedition.lcdui.Graphics;
import core.ReplayControl;
import core.MessageBox;
import core.AnimatedLabel;

//#if JAR != "min"
import core.Replay;
import core.Fireworks;
//#endif
import core.Stadium;
//import br.com.nanogames.components.online.ad.AdManager;
//import br.com.nanogames.components.online.ad.ResourceManager;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import core.Border;
import core.PlayResultBox;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.userInterface.AppMIDlet;

//#if MULTIPLAYER == "true"
//import core.MultiplayerMatchInfo;
//import core.TurnBasedMultiplayerConstants;
//import java.io.ByteArrayInputStream;
//import java.io.ByteArrayOutputStream;
//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//#endif

//#if TOUCH == "true"
 import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 * 
 * @author Peter
 */
public final class Match extends UpdatableGroup implements Constants, KeyListener, SpriteListener, MenuListener, EventListener, ScreenListener
		//#if TOUCH == "true"
 		, PointerListener
		//#endif
		
		//#if MULTIPLAYER == "true"
//			, ConnectionListener, TurnBasedMultiplayerConstants
		//#endif
{

	private static final byte TOTAL_ITEMS = 30;
	private static final byte FIELD_GROUP_TOTAL_ITEMS = 25;
	
	private static final byte STATE_CREATED					= 0;
	private static final byte STATE_PRESENTATION			= 1;
	private static final byte STATE_LOAD_PLAYERS_MSG		= 2;
	private static final byte STATE_BEGIN_TURN				= 3;
	private static final byte STATE_WAITING_PLAYER_1		= 4;	// esperando que o primeiro jogador defina a jogada
	private static final byte STATE_WAITING_PLAYER_2		= 5;	// esperando que o segundo jogador defina a jogada
	private static final byte STATE_WHISTLE					= 6;	// apito do juiz e reposicionamento de câmera, se necessário
	private static final byte STATE_MOVING					= 7;	// jogada em andamento
	private static final byte STATE_PLAY_RESULT				= 8;	// mostra se foi gol, trave, etc
	private static final byte STATE_PARTIAL_RESULT			= 9;
	private static final byte STATE_REPLAY					= 10;	// DEVE SER PAR!
	private static final byte STATE_REPLAY_WAIT				= 11;	// DEVE SER ÍMPAR!
	
	//#if JAR != "min"
		private static final byte STATE_REPLAY_ONLY				= 12;	// mostra apenas o replay de uma jogada (volta ao menu de replay após visualização do mesmo)
		private static final byte STATE_REPLAY_ONLY_WAIT		= 13;	// DEVE SER ÍMPAR! (STATE_REPLAY_ONLY DEVE SER PAR)
	//#endif
	
	private static final byte STATE_FINAL_RESULT			= 14;
	private static final byte STATE_ENDED					= 15;
	private static final byte STATE_CHAMPION_APPEARS		= 16;
	private static final byte STATE_CHAMPION_SHOWN			= 17;
	private static final byte STATE_PLAYER_1_TURN			= 18;
	private static final byte STATE_PLAYER_2_TURN			= 19;

	//#if MULTIPLAYER == "true"
//		private static final byte STATE_SENDING_PLAY		= 20;
//		private static final byte STATE_CONNECTION_ERROR	= 21;
//
//		/** Número máximo de tentativas de reconexão. */
//		private static final byte CONNECTION_MAX_RETRIES	= 5;
	//#endif
	
	private byte state;

	// tipos de disputa de pênalti
	/** Partida normal, com jogador chutando primeiro */
	public static final byte MODE_CHAMPIONSHIP_MATCH_HOME		= 0;
	/** Partida normal, com jogador defendendo primeiro. */
	public static final byte MODE_CHAMPIONSHIP_MATCH_AWAY	= 1;
	/** Modo da tela de jogo: treino (jogador sempre começa chutando). */
	public static final byte MODE_TRAINING						= 2;
	
	//#if JAR != "min"
	/** Modo da tela de jogo: exibe replay. */
	public static final byte MODE_REPLAY						= 3;
	
	/** Modo da tela de jogo: exibe a animação de vitória do campeonato. */
	public static final byte MODE_CHAMPION						= 4;
	//#endif
	
	/** Modo da tela de jogo: 2 jogadores. */
	public static final byte MODE_2_PLAYERS						= 5;

	//#if MULTIPLAYER == "true"
//		/** Modo de jogo: multiplayer (rede), com jogador chutando primeiro. */
//		public static final byte MODE_MULTIPLAYER_MATCH_HOME			= 6;
//		/** Modo de jogo: multiplayer (rede), com jogador defendendo primeiro. */
//		public static final byte MODE_MULTIPLAYER_MATCH_AWAY			= 7;
//
//		/** Mutex usado para evitar problemas de sincronização entre eventos de conexão do modo multiplayer. */
//		private final Mutex multiplayerMutex = new Mutex();
	//#endif

	/***/
	private final byte mode;

	/** Placar da partida. */
	private final short[] score = new short[ 2 ];

	/** Turno atual (número total de pênaltis cobrados). */
	private short currentTurn;
	
	// duração dos timers em milisegundos
	private static final short TIME_PLAY_RESULT_TRAINING		= 2500;
	private static final short TIME_PRESENTATION				= 6000;
	
	// tempo que a barra leva para aparecer/sumir
	private static final short TIME_PARTIAL_RESULT_ANIM	= 400;
	public static final short TIME_BOARD_ANIMATION		= 5500;
	
	/** Tempo que a mensagem indicando o jogador da vez é exibido. */
	private static final short TIME_PLAYER_TURN_MESSAGE	= 2500;
	
	// tempo entre a definição dos controles pelo jogador e o início da movimentação do batedor
	private static final short TIME_WHISTLE				= 1200;
	
	/** Controles do jogador. */
	private final Control control;
	
	/** Última jogada realizada pelo batedor. */
	private final Play lastShooterPlay = new Play();
	
	/** Última jogada realizada pelo goleiro. */
	private final Play lastKeeperPlay = new Play();
	
	/** Bola do jogo. */
	private final Ball ball;
	
	/** Imagem da rede (é separada do gol para permitir que a bola fique entre as traves e a rede). */
	private static DrawableImage net;
	
	/** Grupo do gol (trave esquerda, travessão e trave direita). */
	private static DrawableGroup goal;
	
	/** Goleiro atual. */
	private Keeper currentKeeper;

	/** Batedor atual. */
	private Player currentShooter;

	private Keeper otherKeeper;

	private Player otherShooter;
	
	// tempo em milisegundos até a próxima transição (utilizado para emular o comportamento dos times em BREW)
	private int timeToNextTransition;
	
	/** Tempo mínimo restante para que possa ser acelerada a troca de estado. */
	private int timeMinToAccelerate;
	
	/** Intervalo de tempo efetivamente passado aos jogadores e bola, considerando a velocidade do jogo ou do replay. */
	private int lastDelta;

	/** Imagem da rede balançando. */
	private static DrawableImage netAnimation;
	
	/** Viewport da animação da rede (utilizado para evitar que a imagem seja desenhada além da área interna do gol). */
	private final Rectangle netAnimationViewport = new Rectangle();
	
	/** Controles do replay. */
	private static ReplayControl replayControl;

	private final Pattern lineBack;

	private final Pattern lineSmallArea;

	private final DrawableImage lineSmallAreaLeft;
	private final DrawableImage lineSmallAreaRight;

	private final Pattern lineBigArea;

	private final DrawableImage halfMoonLeft;
	private final DrawableImage halfMoonRight;
	
	/***/
	
	/** Tela que mostra o resultado parcial do jogo. */
	private final ScoreBox scoreBox;
	
	/** Tela que mostra a apresentação dos times e o resultado final. */
	private final InfoBox infoBox;
	
	private final Team team1;
	private final Team team2;
	
	private final int difficultyLevel;
	
	//<editor-fold desc="PARÂMETROS DA CÂMERA" defaultstate="collapsed">
	
	/** Tempo padrão em segundos que a câmera leva para chegar à posição de destino. */
	private static final int FP_CAMERA_MOVE_DEFAULT_TIME = NanoMath.divInt( 1200, 1000 );
	
	/** Posição y do topo da tela ao mostrar controles do jogador. */
	private short CAMERA_CONTROL_Y;
	
	/** Posição y (topo) máxima da câmera. */
	private short CAMERA_MAX_Y;
	
	/** Posição y da câmera ao mostrar o controle de curva. */
	private short CAMERA_CURVE_Y;
	
	/** Velocidade da câmera. */
	private int fp_cameraSpeed;
	
	/** Posição atual da câmera. */
	private int fp_cameraY;
	
	/** Tempo total em segundos até a câmera chegar à posição de destino. */
	private int fp_timeToDestination;
	
	/** Tempo atual da câmera é acumulado para agilizar teste de chegada ao destino. */
	private int fp_cameraCurrentTime;
	
	// estados da câmera
	private static final byte CAMERA_STATE_STOPPED	= 0;
	private static final byte CAMERA_STATE_MOVING	= 1;
	
	/** Estado atual da câmera. */
	private byte cameraState;

	//</editor-fold>

	private final AnimatedLabel animatedLabel;

	private final Border animatedLabelBorder;
	
	private UpdatableGroup fieldGroup;
	
	/** Mutex utilizado para evitar problemas de sincronização entre onSequenceEnded e setState. */
	private final Mutex mutex = new Mutex();
	
	/** Indica se está dentro da área crítica do método setState (utilizado para evitar que o estado possa ser pulado 
	 * mesmo que o tempo mínimo não tenha sido expirado).
	 */
	private boolean changingState;
	
	/** Grupo utilizado para indicar uma mensagem ao jogador (carregando, passe para o jogador 1, passe para o jogador 2). */
	private final MessageBox messageBox;
	
	private final DrawableImage mark;

	private final Pattern patternGrass;

	private final byte SHOOTER_DRAWABLE_INDEX;
	private final byte KEEPER_DRAWABLE_INDEX;

	private final PlayResultBox playResultBox;

	private static final byte MESSAGE_TYPE_NO_BUTTON	= 0;
	private static final byte MESSAGE_TYPE_OK			= 1;
	private static final byte MESSAGE_TYPE_CANCEL		= 2;

	private Replay replay;

	private final Stadium stadium;

	private final Fireworks fireworks;

	private final Drawable iconPause;

	//#if MULTIPLAYER == "true"
//		private MultiplayerMatchInfo matchInfo;
//
//		public Match( MultiplayerMatchInfo matchInfo ) throws Exception {
//			this( matchInfo.getPlayers()[ 0 ].getId() == NanoOnline.getCurrentCustomer().getId() ? MODE_CHAMPIONSHIP_MATCH_HOME : MODE_CHAMPIONSHIP_MATCH_AWAY, 0, 0, new Team( 0 ), new Team( 1 ) );
//		}
	//#endif

	
	/**
	 * Aloca uma nova tela de jogo para exibir um replay.
	 * 
	 * @param replay
	 * @throws java.lang.Exception
	 */
	public Match( Replay replay ) throws Exception {
		this( MODE_REPLAY, new Team( replay.shooterTeamIndex ), new Team( replay.keeperTeamIndex )  );

		this.replay = replay;
	}
	
	
	public Match( byte mode, Team team1, Team team2 ) throws Exception {
		this( mode, 0, 0, team1, team2 );
	}

    
	public Match( byte mode, int difficultyLevel, int stage, Team team1, Team team2 ) throws Exception {
		super( TOTAL_ITEMS );

		this.mode = mode;
		
		fieldGroup = new UpdatableGroup( FIELD_GROUP_TOTAL_ITEMS );
		insertDrawable( fieldGroup );
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		// deve ser chamado antes da criação da tela do estádio
//		try {
//			ResourceManager.load();
//			AdManager.load();
//		} catch ( Exception e ) {
//			//#if DEBUG == "true"
//				e.printStackTrace();
//			//#endif
//		}
		//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "SMALL"
//		if (GameMIDlet.isLowMemory()) {
//			stadium = new Stadium(false);
//		} else {
//			stadium = new Stadium(mode != MODE_TRAINING);
//		}
		//#else
 		stadium = new Stadium(mode != MODE_TRAINING);
		//#endif
		fieldGroup.insertDrawable( stadium );
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		// se a altura da tela permitir, insere a cor sólida do gramado abaixo das imagens do gramado
		patternGrass = new Pattern( new DrawableImage( PATH_IMAGES + "grass.png" ) );
		fieldGroup.insertDrawable( patternGrass );

		lineBack = new Pattern( new DrawableImage( PATH_IMAGES + "line_small.png" ) );
		fieldGroup.insertDrawable( lineBack );

		lineSmallArea = new Pattern( new DrawableImage( ( DrawableImage ) lineBack.getFill() ) );
		fieldGroup.insertDrawable( lineSmallArea );

		lineBigArea = new Pattern( new DrawableImage( PATH_IMAGES + "line_big.png" ) );
		fieldGroup.insertDrawable( lineBigArea );

		lineSmallAreaLeft = new DrawableImage( PATH_IMAGES + "small_area.png" );
		lineSmallAreaLeft.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
		fieldGroup.insertDrawable( lineSmallAreaLeft );

		lineSmallAreaRight = new DrawableImage( lineSmallAreaLeft );
		lineSmallAreaRight.mirror( TRANS_MIRROR_H );
		fieldGroup.insertDrawable( lineSmallAreaRight );

		halfMoonLeft = new DrawableImage( PATH_IMAGES + "moon.png" );
		halfMoonLeft.defineReferencePixel( ANCHOR_TOP | ANCHOR_RIGHT );
		fieldGroup.insertDrawable( halfMoonLeft );
		
		halfMoonRight = new DrawableImage( halfMoonLeft );
		halfMoonRight.mirror( TRANS_MIRROR_H );
		fieldGroup.insertDrawable( halfMoonRight );

		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		// insere a marca do pênalti
		mark = new DrawableImage( PATH_IMAGES + "mark.png" );
		mark.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		fieldGroup.insertDrawable( mark );
		
		//#if JAR != "min"
			// insere a animação da rede do gol (já alocada)
			fieldGroup.insertDrawable( netAnimation );
			netAnimation.setViewport( netAnimationViewport );
			netAnimation.setVisible( false );
		//#endif
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		fieldGroup.insertDrawable( net );
		fieldGroup.insertDrawable( goal );

		// insere a bola do jogo
		ball = new Ball( this );
		fieldGroup.insertDrawable( ball );
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		// insere os controles do jogador
		control = new Control();
		final int CONTROL_DRAWABLE_INDEX = fieldGroup.insertDrawable( control );

		KEEPER_DRAWABLE_INDEX = ( byte ) ( CONTROL_DRAWABLE_INDEX + 1 );
 		SHOOTER_DRAWABLE_INDEX = ( byte ) ( KEEPER_DRAWABLE_INDEX + 1 );

		this.team1 = team1;
		this.team2 = team2;
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		
		this.difficultyLevel = difficultyLevel;

		animatedLabelBorder = new Border( Border.BORDER_COLOR_BLACK );
		insertDrawable( animatedLabelBorder );
        
        animatedLabel = new AnimatedLabel( animatedLabelBorder );
		insertDrawable( animatedLabel );
		//#if JAR != "min"
		if ( mode == MODE_CHAMPION ) {
			scoreBox = null;
			infoBox = null;
		} else {
		//#endif
			Player.setMatch( this );
			Ball.setMatch( this );

			// insere a caixa de texto que mostra o resultado parcial
			scoreBox = new ScoreBox( team1, team2 );
			scoreBox.setEventListener( this );
			scoreBox.setState( ScoreBox.STATE_HIDDEN );
			insertDrawable( scoreBox );	
			Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
			
			// insere a caixa de texto que mostra a tela de apresentação e de resultado final
			infoBox = new InfoBox(( byte ) stage, team1, team2, score );
			infoBox.setEventListener( this );
			infoBox.setState( InfoBox.STATE_HIDDEN );
			insertDrawable( infoBox );		
		// fecha as chaves
		//#if JAR != "min"
		}
		//#endif
		
		replayControl = new ReplayControl( this, mode != MODE_REPLAY );
		insertDrawable( replayControl );
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola

		playResultBox = new PlayResultBox();
		playResultBox.setEventListener( this );
		insertDrawable( playResultBox );
        
		// cria e insere o grupo que mostra mensagens ao jogador
		messageBox = new MessageBox();
		messageBox.setEventListener( this );
		messageBox.setVisible( false );
		insertDrawable( messageBox );

		if ( ScreenManager.getInstance().hasPointerEvents() ) {
			iconPause = new DrawableImage( PATH_IMAGES + "pause.png" );
			iconPause.setSize( iconPause.getWidth() + BORDER_OFFSET, iconPause.getHeight() + BORDER_OFFSET );
		} else {
			iconPause = null;
		}
		
		Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		switch ( mode ) {
			case MODE_TRAINING:
			case MODE_REPLAY:
				setState( STATE_LOAD_PLAYERS_MSG );
				fireworks = null;
			break;

			case MODE_CHAMPION:
				fireworks = new Fireworks();
				insertDrawable( fireworks );
				setState( STATE_CHAMPION_APPEARS );
			break;

			default:
				fireworks = null;
				setState( STATE_PRESENTATION );
		}
	}


	public final Team getTeam1() {
		return team1;
	}


	public final Team getTeam2() {
		return team2;
	}


	public final void keyPressed( int key ) {
		if ( messageBox.isVisible() ) {
			messageBox.keyPressed( key );
			return;
		}
		
		switch ( key ) {
			case ScreenManager.KEY_NUM5:
			case ScreenManager.FIRE:
				switch ( state ) {
					case STATE_WAITING_PLAYER_1:
					case STATE_WAITING_PLAYER_2:
						// esperando que jogador defina jogada
						if ( control.setControl() ) {
							timerExpired();
						} else {
							if ( control.getCurrentControl() == Control.CONTROL_CURVE )
								moveCameraTo( CAMERA_CURVE_Y, FP_CAMERA_MOVE_DEFAULT_TIME );
						}
						playResultBox.setState( PlayResultBox.STATE_HIDING );
					break; // fim case STATE_WAITING

					//#if JAR != "min"
						case STATE_REPLAY_ONLY_WAIT:
							GameMIDlet.setScreen( SCREEN_LOAD_REPLAY );
						break;
					//#endif
					
					case STATE_PLAY_RESULT:   
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:
					case STATE_PARTIAL_RESULT:
						checkKeyPressedTimerExpired();
					break;

					case STATE_FINAL_RESULT:
						scoreBox.setState( GenericInfoBox.STATE_HIDING );
						setState( STATE_ENDED );
					break;

					case STATE_REPLAY_ONLY:
					case STATE_REPLAY:
					case STATE_REPLAY_WAIT:
						replayControl.keyPressed( key );
					break;

					case STATE_PRESENTATION:
					case STATE_ENDED:
					case STATE_CHAMPION_APPEARS:
					case STATE_CHAMPION_SHOWN:
						timerExpired();
					break;

					//#if MULTIPLAYER == "true"
//						case STATE_CONNECTION_ERROR:
//							// TODO botão "OK" da mensagem de erro fica pressionado
//						break;
					//#endif
				} // fim switch ( state )
			break;

			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
				switch ( state ) {
					case STATE_CREATED:
					case STATE_PRESENTATION:
					break;
					
					//#if JAR != "min"
						case STATE_REPLAY_ONLY:
						case STATE_REPLAY_ONLY_WAIT:
							GameMIDlet.setScreen( SCREEN_LOAD_REPLAY );
						break;
					//#endif
					
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:	
						checkKeyPressedTimerExpired();
					break;
					
					case STATE_PARTIAL_RESULT:
					case STATE_REPLAY:
					case STATE_REPLAY_WAIT:
					case STATE_CHAMPION_APPEARS:
					case STATE_CHAMPION_SHOWN:
						timerExpired();
					break;

					default:
						GameMIDlet.setScreen( SCREEN_PAUSE );
				}
			break; // fim case ScreenManager.KEY_CLEAR

			//#if JAR != "min"
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.KEY_POUND:
			case ScreenManager.KEY_STAR:
			case ScreenManager.KEY_NUM0:
			case ScreenManager.KEY_NUM7:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM9:
			case ScreenManager.UP:
			case ScreenManager.DOWN:
				switch ( state ) {
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:
						checkKeyPressedTimerExpired();
					break;
				}
			break;
			//#endif

			default:
				switch ( state ) {
					case STATE_REPLAY_ONLY:
					case STATE_REPLAY_ONLY_WAIT:
					case STATE_REPLAY_WAIT:
					case STATE_REPLAY:
						replayControl.keyPressed( key );
					break;
					
					case STATE_PLAYER_1_TURN:
					case STATE_PLAYER_2_TURN:		
						checkKeyPressedTimerExpired();
					break;
					
					case STATE_CHAMPION_APPEARS:
					case STATE_CHAMPION_SHOWN:
						timerExpired();
					break;
				} // fim switch ( state )
		} // fim switch ( key )		
	}


	public final void keyReleased( int key ) {
		if ( messageBox.isVisible() ) {
			messageBox.keyReleased( key );
			return;
		}
		
		//#if MULTIPLAYER == "true"
//			switch ( state ) {
//				case STATE_CONNECTION_ERROR:
//					switch ( key ) {
//						case ScreenManager.KEY_NUM5:
//						case ScreenManager.FIRE:
//							timerExpired();
//						break;
//					}
//				break;
//
//				case STATE_REPLAY:
//				case STATE_REPLAY_ONLY:
//				case STATE_REPLAY_ONLY_WAIT:
//				case STATE_REPLAY_WAIT:
//					replayControl.keyReleased( key );
//				break;
//			}
		//#endif
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		playResultBox.setSize( size );
		
		animatedLabel.setSize( size );
		
		fieldGroup.setSize( getWidth(), stadium.getHeight() + patternGrass.getHeight() );

		if ( fireworks != null )
			fireworks.setSize( size );

		patternGrass.setSize( ScreenManager.SCREEN_WIDTH, patternGrass.getFill().getHeight() );
		patternGrass.setPosition( 0, fieldGroup.getHeight() - patternGrass.getHeight() );

		lineBack.setSize( width, lineBack.getFill().getHeight() );
		lineBack.setPosition( 0, patternGrass.getPosY() + BACK_LINE_Y_OFFSET );

		stadium.setPosition( 0, patternGrass.getPosY() - stadium.getHeight() );

		lineSmallAreaLeft.setRefPixelPosition( ( width >> 1 ) - LINE_SMALL_AREA_X_OFFSET, lineBack.getPosY() );
		lineSmallAreaRight.setPosition( ( width >> 1 ) + LINE_SMALL_AREA_X_OFFSET, lineBack.getPosY() );

		lineSmallArea.setSize( lineSmallAreaRight.getPosX() - ( lineSmallAreaLeft.getPosX() + lineSmallAreaLeft.getWidth() ), lineBack.getFill().getHeight() );
		lineSmallArea.setPosition( lineSmallAreaLeft.getPosX() + lineSmallAreaLeft.getWidth(), lineSmallAreaLeft.getPosY() + lineSmallAreaLeft.getHeight() - lineSmallArea.getHeight() );

		lineBigArea.setPosition( 0, lineBack.getPosY() + BIG_AREA_LINE_Y );
		lineBigArea.setSize( width, lineBack.getFill().getHeight() );
		halfMoonLeft.setRefPixelPosition( width >> 1, lineBigArea.getPosY() + lineBigArea.getHeight() );
		halfMoonRight.setPosition( halfMoonLeft.getRefPixelPosition() );

		mark.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, lineBack.getPosY() + PENALTY_MARK_TO_GOAL );

		// posiciona o gol
		net.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, lineBack.getPosY() );
		goal.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, lineBack.getPosY() + GOAL_OFFSET_Y );

		// define a posição da câmera durante a exibição dos controles e da jogada
		control.setInfo( goal, mark.getRefPixelY(), fieldGroup.getHeight() );
		CAMERA_MAX_Y = ( short ) NanoMath.min( 0, getHeight() - fieldGroup.getHeight() );
		CAMERA_CURVE_Y = ( short ) NanoMath.max( CAMERA_MAX_Y, -control.getPowerPosY() + BORDER_OFFSET );
		CAMERA_CONTROL_Y = ( short ) NanoMath.max( CAMERA_MAX_Y, -control.getDirectionIconPosY() + playResultBox.getBorderHeight() + BORDER_OFFSET );

		ball.setInitialY( mark.getRefPixelY() );
		ball.setSize( fieldGroup.getSize() );

		refreshPlayers();
		
		netAnimationViewport.set( goal.getPosX() + NET_OFFSET, goal.getPosY() + NET_OFFSET, goal.getWidth() - ( NET_OFFSET << 1 ), NET_HEIGHT );

		replayControl.setSize( size );
		moveCameraTo( CAMERA_MAX_Y, 1 );
	}


	private final void refreshPlayers() {
		// TODO armazenar jogadores em array, para evitar carregamentos desnecessários
		if ( currentKeeper != null )
			currentKeeper.setInitialPosition( goal );

		if ( currentShooter != null ) {
			currentShooter.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
			currentShooter.setRefPixelPosition( getWidth() >> 1, fieldGroup.getHeight() + SHOOTER_OFFSET_Y );
		}
	}


	/**
	 * Atualiza a partida após "time" milisegundos.
	 */
	public final void update( int delta ) {
		int cameraDelta;
		switch ( state ) {
			//#if JAR != "min"
				case STATE_REPLAY_ONLY:
				case STATE_REPLAY_ONLY_WAIT:
			//#endif
			case STATE_MOVING:
			case STATE_PLAY_RESULT:
			case STATE_REPLAY:
			case STATE_PARTIAL_RESULT:
			case STATE_FINAL_RESULT:
			case STATE_REPLAY_WAIT:
				lastDelta = replayControl.getReplayDelta( delta );
				cameraDelta = lastDelta;
			break;

			case STATE_LOAD_PLAYERS_MSG:
				if ( messageBox.getState() == MessageBox.STATE_SHOWN ) {
					Thread.yield();
					return;
				}
			
			default:
				lastDelta = 0;
				cameraDelta = delta;
		}
		
		super.update( delta );
		
		// reposiciona a câmera na tela
		switch ( cameraState ) {
			case CAMERA_STATE_MOVING:
				final int fp_delta = NanoMath.divInt( cameraDelta, 1000 );
				fp_cameraCurrentTime += fp_delta;
				
				if ( fp_cameraCurrentTime >= fp_timeToDestination ) {
					fp_cameraCurrentTime = fp_timeToDestination;
					cameraState = CAMERA_STATE_STOPPED;
				}
				
				fp_cameraY += NanoMath.mulFixed( fp_cameraSpeed, fp_delta );
				
				fieldGroup.setPosition( 0, NanoMath.clamp( NanoMath.toInt( fp_cameraY ), CAMERA_MAX_Y, 0 ) );
			break;
		}		
		
		if ( timeToNextTransition > 0 ) {
			timeToNextTransition -= delta;
			if ( timeToNextTransition <= 0 ) {
				timerExpired();
			}
		}

		switch ( state ) {
			case STATE_MOVING:		   
				// jogada em andamento
				switch ( ball.getState() ) {
					case Ball.BALL_STATE_GOAL:
					case Ball.BALL_STATE_POST_GOAL:
						// gol - atualiza o placar
						final int index = ( currentTurn & 1 );
						++score[ index ];
						if ( score[ index ] > 999 )
							score[ index ] = 999;
					break;

					case Ball.BALL_STATE_OUT:
					case Ball.BALL_STATE_POST_OUT:
					case Ball.BALL_STATE_POST_BACK:
					case Ball.BALL_STATE_REJECTED_KEEPER:
					break;

					default:
						// jogada ainda está em andamento
						return;
				} // fim switch ( ball.getState() )

				// jogada foi encerrada
				timerExpired();
			break;

			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
			//#endif
			case STATE_REPLAY:
 			case STATE_REPLAY_WAIT:
			//#if JAR == "min"
//# 				replayAccTime -= delta;
//# 				if ( replayAccTime < 0 ) {
//# 					replayAccTime = REPLAY_BLINK_TIME;
//# 					replayLabel.setVisible( !replayLabel.isVisible() );
//# 				}
			//#else
			case STATE_REPLAY_ONLY_WAIT:
			case STATE_PLAY_RESULT:
				switch ( ball.getState() ) {
					case Ball.BALL_STATE_GOAL:
					case Ball.BALL_STATE_POST_GOAL:
						netAnimationViewport.y = goal.getPosY() + NET_OFFSET + fieldGroup.getPosY();
						netAnimation.setRefPixelPosition( ball.getSpriteRefPixelX(), ball.getSpriteRefPixelY() );
						netAnimation.setVisible( true );
					break;
				}
			//#endif
			break;
			
			case STATE_CHAMPION_APPEARS:
			break;
			
			//#if SCREEN_SIZE != "SMALL"
				case STATE_CHAMPION_SHOWN:
				break;
			//#endif
			
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				// reduz o tempo para que seja verificado o tempo mínimo para passar de estado (troca somente ao pressionar tecla)
				if ( timeToNextTransition < ( TIME_PLAYER_TURN_MESSAGE >> 2 ) )
					timeToNextTransition = TIME_PLAYER_TURN_MESSAGE >> 2;
			break; 
		} // fim switch( state )
		
		// atualiza a ordem de desenho dos drawables
		updateZOrder();		
	} // fim do método update( int )


	/**
	 * Define os softkeys da tela de jogo, de acordo com o tipo de partida e estado atual.
	 */
	public final void setSoftKeyLabel() {
		switch ( state ) {
			case STATE_REPLAY_ONLY:
			case STATE_REPLAY_ONLY_WAIT:
			case STATE_REPLAY:
			case STATE_REPLAY_WAIT:
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
			case STATE_CHAMPION_APPEARS:
			case STATE_CHAMPION_SHOWN:
			case STATE_ENDED:
			case STATE_FINAL_RESULT:
			case STATE_LOAD_PLAYERS_MSG:
				ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_RIGHT, null );
			break;
			
			case STATE_BEGIN_TURN:
			case STATE_MOVING:
				ScreenManager.getInstance().setSoftKey( ScreenManager.SOFT_KEY_RIGHT, iconPause );
		}
	}


	private final void showMessage( String message, byte type ) {
		messageBox.setText( message );
		switch ( type ) {
			case MESSAGE_TYPE_CANCEL:
				messageBox.setButton( GameMIDlet.getText( TEXT_CANCEL ) );
			break;

			case MESSAGE_TYPE_NO_BUTTON:
				messageBox.setButton( null );
			break;

			case MESSAGE_TYPE_OK:
				messageBox.setButton( GameMIDlet.getText( TEXT_OK ) );
			break;
		}
		
		messageBox.setState( MessageBox.STATE_APPEARING );
	}


	public final void unloadMatch() {
		Player.setMatch( null );
		Ball.setMatch( null );
		
		currentKeeper = null;
		currentShooter = null;
		otherKeeper = null;
		otherShooter = null;
		ball.setKeeper( null );
		
		if ( fieldGroup != null ) {
			while ( fieldGroup.getUsedSlots() > 0 )
				fieldGroup.removeDrawable( 0 );
			
			fieldGroup = null;
		}

		while ( activeDrawables > 0 )
			removeDrawable( 0 );
		
		AppMIDlet.gc();
	}

	
	/**
	 * Método chamado quando um timer expira (utilizado para emular o comportamento dos timers em BREW).
	 */
	private final void timerExpired() {
		switch ( state ) {
			// obtém o estado atual da partida (o estado que será encerrado)
			case STATE_PRESENTATION:
				scoreBox.setState( GenericInfoBox.STATE_HIDING );
				infoBox.setState( GenericInfoBox.STATE_HIDING );
				infoBox.setEventData( new Byte( STATE_LOAD_PLAYERS_MSG ) );
			break;
			
			case STATE_LOAD_PLAYERS_MSG:
				final Thread loadThread = new Thread() {
					public final void run() {
						try {
							while ( messageBox.getState() != MessageBox.STATE_SHOWN )
								Thread.sleep( 400 );
							
							fieldGroup.setVisible( false );
							fieldGroup.removeDrawable( currentKeeper );
							fieldGroup.removeDrawable( currentShooter );

							if ( otherKeeper != null ) {
								// há memória suficiente pra alocar os 2 goleiros; apenas faz a troca
								final Keeper temp = otherKeeper;
								otherKeeper = currentKeeper;
								currentKeeper = temp;
							} else {
								AppMIDlet.gc();
								if ( Runtime.getRuntime().freeMemory() > MEMORY_FULL )
									otherKeeper = currentKeeper;
								
								// desaloca o goleiro antigo
								ball.setKeeper( null );
								currentKeeper = null;
								AppMIDlet.gc();

								if ( ( currentTurn & 1 ) == 0 ) {
									currentKeeper = team2.loadKeeper( goal );
									// define o nível de dificuldade do goleiro da IA
									if ( mode == MODE_CHAMPIONSHIP_MATCH_HOME )
										currentKeeper.setDifficultyLevel( difficultyLevel );
								} else {
									currentKeeper = team1.loadKeeper( goal );
									// define o nível de dificuldade do goleiro da IA
									if ( mode == MODE_CHAMPIONSHIP_MATCH_AWAY )
										currentKeeper.setDifficultyLevel( difficultyLevel );
								}
							}

							// desaloca o batedor antigo
							currentShooter = null;
							AppMIDlet.gc();

							if (!GameMIDlet.isLowMemory()) {
								if ((currentTurn & 1) == 0) {
									currentShooter = team1.getShooter(currentTurn >> 1);
								} else {
									currentShooter = team2.getShooter(currentTurn >> 1);
								}
							}else{
								if ((currentTurn & 1) == 0) {
									currentShooter = team1.getShooter(0 >> 1);
								} else {
									currentShooter = team2.getShooter(0 >> 1);
								}
							}

							fieldGroup.insertDrawable( currentKeeper, KEEPER_DRAWABLE_INDEX );
							fieldGroup.insertDrawable( currentShooter, SHOOTER_DRAWABLE_INDEX );
							refreshPlayers();

							if ( mode == MODE_REPLAY ) {
								messageBox.setState( MessageBox.STATE_HIDING );
								messageBox.setEventData( new Byte( STATE_REPLAY_ONLY ) );
							} else {
								setState( STATE_BEGIN_TURN );
							}
						} catch ( Exception e ) {
							//#if DEBUG == "true"
								e.printStackTrace();
							//#endif
							// TODO ir para tela de erro
							GameMIDlet.exit();
						} finally {
							fieldGroup.setVisible( true );
						}
					}
				};
				loadThread.setPriority( Thread.MAX_PRIORITY );
				loadThread.start();
			break;	

			case STATE_BEGIN_TURN:
				setState( STATE_WAITING_PLAYER_1 );
			break;

			case STATE_WAITING_PLAYER_2:
			case STATE_WAITING_PLAYER_1:
				// jogador fez sua jogada
				switch ( mode ) {
					case MODE_2_PLAYERS:
						switch ( state ) {
							case STATE_WAITING_PLAYER_1:
								lastShooterPlay.set( control.getCurrentPlay() );
								setState( ( byte ) ( STATE_PLAYER_2_TURN - ( currentTurn & 1 ) ) );
							break;
							
							case STATE_WAITING_PLAYER_2:
								lastKeeperPlay.set( control.getCurrentPlay() );
								setState( STATE_WHISTLE );
							break;
						}
					break;
					
					default:
						if ( isPlayerShooting() ) {
							lastShooterPlay.set( control.getCurrentPlay() );
							lastKeeperPlay.set( control.getRandomPlay() );
						} else {
							lastShooterPlay.set( control.getRandomPlay() );
							lastKeeperPlay.set( control.getCurrentPlay() );
						}
						
						setState( STATE_WHISTLE );
				}
			break;

			case STATE_WHISTLE:
				setState( STATE_MOVING );
			break;

			case STATE_MOVING:        
				// jogada em andamento
				setState( STATE_PLAY_RESULT );
			break;

			case STATE_PLAY_RESULT:
				playResultBox.setState( PlayResultBox.STATE_HIDING );

				switch ( mode ) {
					case MODE_TRAINING:
						// no modo treino não mostra o replay
						setState( STATE_REPLAY_WAIT );
						timerExpired();
					break;

					default:
						setState( STATE_REPLAY );
				}
			break;

			//#if JAR != "min"
				case STATE_REPLAY_ONLY:
					setState( STATE_REPLAY_ONLY_WAIT );
				break;
			//#endif

			case STATE_REPLAY:
				setState( STATE_REPLAY_WAIT );
			break;

			case STATE_REPLAY_WAIT:
				++currentTurn;
				
				//#if DEBUG == "true"
				System.out.println( "PLACAR APÓS " + currentTurn + " CHUTE(S): " + score[ 0 ] + " X " + score[ 1 ] );
				//#endif				
				
				//#if I_AM_THE_PENALTY_KING == "true"
//# 					score[ 0 ] = 1;
//# 					score[ 1 ] = 0;
//#
//# 					setState( STATE_FINAL_RESULT );
				//#else
				switch ( mode ) {
					case MODE_TRAINING:
						currentShooter.setSequence( SHOOTER_SEQUENCE_STOPPED );

						if ( GameMIDlet.isLowMemory() )
							setState( STATE_LOAD_PLAYERS_MSG );
						else
							setState( STATE_BEGIN_TURN );
					break;

					default:
						if ( hasEnded( getCurrentTurn(), score ) )
							setState( STATE_FINAL_RESULT );
						else
							setState( STATE_PARTIAL_RESULT );
					break;

				}
				//#endif
			break; // fim case STATE_REPLAY_WAIT

			case STATE_PARTIAL_RESULT:
				scoreBox.setState( GenericInfoBox.STATE_HIDING );
				scoreBox.setEventData( new Byte( ( true || GameMIDlet.isLowMemory() ) ? STATE_LOAD_PLAYERS_MSG : STATE_BEGIN_TURN ) );
			break;

			case STATE_ENDED:
			case STATE_FINAL_RESULT:
				GameMIDlet.matchEnded( this );
			break;
			
			case STATE_CHAMPION_APPEARS:
				setState( STATE_CHAMPION_SHOWN );
			break;
			
			case STATE_CHAMPION_SHOWN:
				GameMIDlet.setScreen( SCREEN_CHAMPIONSHIP );
			break;
			
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				final byte diff = ( byte ) ( state - STATE_PLAYER_1_TURN );
				
				if ( diff == ( currentTurn & 1 ) ) 
					setState( STATE_BEGIN_TURN );
				else
					setState( STATE_WAITING_PLAYER_2 );
					
				setSoftKeyLabel();
			break;

			//#if MULTIPLAYER == "true"
//				case STATE_CONNECTION_ERROR:
//					GameMIDlet.setScreen( SCREEN_NEW_GAME_MENU );
//				break;
//
//				case STATE_SENDING_PLAY:
//					setState( STATE_WHISTLE );
//				break;
			//#endif
		} // fim switch( pMatch.state )
	} // fim do método timerExpired()


	public final byte getMode() {
		return mode;
	}


	/**
	 * Simula o resultado da partida, armazenando o placar em score e retornando o índice do time vencedor.
	 * 
	 * @param score
	 * @return 
	 */
	public static final byte simulate( short[] score ) {
		score[ 0 ] = score[ 1 ] = 0;
		short turn = 0;

		do {
			if ( NanoMath.randInt( 100 ) > 19 ) {
				 // gol
				++( score[ turn & 1 ] );
			}
			++turn;
		} while ( !hasEnded( turn, score ) );

		return ( byte ) ( score[ 0 ] > score[ 1 ] ? 0 : 1 );
	} // fim do método simulate()


	/**
	 * 
	 * @param state
	 */
	private final void setState( byte state ) {
		mutex.acquire();
		
		changingState = true;
		
		//#if DEBUG == "true"
		System.out.println( "Match.setState: " + this.state + " -> " + state );
		//#endif

		this.state = state;
		timeToNextTransition = 0;		
		timeMinToAccelerate = 0;
		
		fieldGroup.setVisible( true );
		
		switch ( state ) {
			case STATE_PRESENTATION:
				infoBox.setType( InfoBox.TYPE_PRESENTATION, score );
				infoBox.setState( GenericInfoBox.STATE_APPEARING );
				playResultBox.setState( PlayResultBox.STATE_HIDDEN );
               
				timeToNextTransition = TIME_PRESENTATION;
				timeMinToAccelerate = TIME_PRESENTATION >> 1;
			break; 
			
			case STATE_LOAD_PLAYERS_MSG:
				showMessage( "<ALN_H>" + GameMIDlet.getText( TEXT_LOADING ), MESSAGE_TYPE_NO_BUTTON );
				control.setVisible( false );
				messageBox.setState( MessageBox.STATE_APPEARING );
				timeToNextTransition = 171;
			break;
			
			case STATE_BEGIN_TURN:
				//#if JAR == "min"
//# 				replayLabel.setVisible( false );
				//#else
					netAnimation.setVisible( false );
					replayControl.setReplayIconSequence( ReplayControl.REPLAY_ICON_SEQUENCE_NONE );
					replayControl.setVisible( false );
				//#endif

				messageBox.setState( MessageBox.STATE_HIDING );

				// jogador chuta caso seja uma rodada ímpar fora de casa, ou rodada par quando
				// joga em casa ou treino.

				// define a vez do jogador (chutar ou defender)
				if ( isPlayerShooting() ) {
					control.setTurn( Control.TURN_ATTACK );

				    if ( mode == MODE_2_PLAYERS ) {
						playResultBox.setText( GameMIDlet.getText( TEXT_PLAYER_1 + ( currentTurn & 1 ) ) + GameMIDlet.getText( TEXT_ATTACK ) );
				    } else {
						playResultBox.setText( TEXT_ATTACK );
					}
				} else {
					control.setTurn( Control.TURN_DEFEND );

					if ( mode == MODE_2_PLAYERS ) {
						playResultBox.setText( GameMIDlet.getText( TEXT_PLAYER_1 + ( currentTurn & 1 ) ) + GameMIDlet.getText( TEXT_DEFEND ) );
					} else {
						playResultBox.setText( TEXT_DEFEND );
					}
				}

				currentShooter.setVisible( true );
				currentKeeper.setVisible( true );

				replayControl.setPaused( false );
				replayControl.setReplaySpeed( ReplayControl.REPLAY_SPEED_NORMAL );
				
				ball.reset( currentKeeper );
				control.reset();

				currentKeeper.reset();
				
				currentShooter.setListener( this, currentTurn & 1 );

				updateZOrder();
				
				setZOrder( currentKeeper, goal, 1 );
				setZOrder( control, currentKeeper, 1 );
				
				moveCameraTo( CAMERA_CONTROL_Y, FP_CAMERA_MOVE_DEFAULT_TIME );

				//#if BAD_MP3 == "false"
				if (mode != MODE_TRAINING) {
					MediaPlayer.play(SOUND_INDEX_CROWD_AMBIENT, MediaPlayer.LOOP_INFINITE);
				}
				//#endif

				timeToNextTransition = TIME_PARTIAL_RESULT_ANIM;
			break; 

			case STATE_WAITING_PLAYER_2:
				control.setTurn( Control.TURN_DEFEND );
				control.reset();
				
				moveCameraTo( CAMERA_CONTROL_Y, FP_CAMERA_MOVE_DEFAULT_TIME );
				playResultBox.setText( GameMIDlet.getText( TEXT_PLAYER_2 - ( currentTurn & 1 ) ) + GameMIDlet.getText( TEXT_DEFEND ) );
			break;

			case STATE_WHISTLE:
				// toca o apito do juiz
				MediaPlayer.play( SOUND_INDEX_WHISTLE );
				
				playResultBox.setState( PlayResultBox.STATE_HIDING );

				ball.reset( currentKeeper );
				// esconde os controles
				control.setVisible( false ); 

				// agenda o início da movimentação do jogador
				timeToNextTransition = TIME_WHISTLE;
				
				moveCameraTo( CAMERA_MAX_Y, FP_CAMERA_MOVE_DEFAULT_TIME  );
			break;
			
			case STATE_MOVING:        
				// jogada em andamento
				playResultBox.setState( PlayResultBox.STATE_HIDING );

				// jogador se movimenta para chutar a bola (goleiro só se move após o chute em si)
				currentShooter.setSequence( SHOOTER_SEQUENCE_SHOOTING );
			break;
			
			case STATE_PLAY_RESULT:   
				replayControl.setReplaySpeed( ReplayControl.REPLAY_SPEED_NORMAL );

				if ( mode != MODE_TRAINING ) {
					switch ( ball.getLastPlayResult() ) {
						case BALL_STATE_POST_GOAL:
						case BALL_STATE_GOAL:
							if (isPlayerShooting()) {
								//#if BAD_MP3 == "false"
								MediaPlayer.play(SOUND_INDEX_PLAY_GOOD_MP3);
							//#endif
							} else {
								//#if BAD_MP3 == "false"
								MediaPlayer.play(SOUND_INDEX_PLAY_BAD);
							//#endif
							}
							break;

						case BALL_STATE_POST_BACK:
						case BALL_STATE_POST_OUT:
							//#if BAD_MP3 == "false"/
							MediaPlayer.play(SOUND_INDEX_PLAY_UUUUH);
							//#endif
							break;

						default:
							if (isPlayerShooting()) {
								//#if BAD_MP3 == "false"

								MediaPlayer.play(SOUND_INDEX_PLAY_BAD);
							//#endif
							} else {
								//#if BAD_MP3 == "false"
								MediaPlayer.play(SOUND_INDEX_PLAY_GOOD_MP3);
							//#endif
							}
					}
				}
				
				
				// mostra se foi gol, trave, etc
				switch ( mode ) {
					case MODE_CHAMPIONSHIP_MATCH_AWAY:
					case MODE_CHAMPIONSHIP_MATCH_HOME:
					case MODE_2_PLAYERS:
						switch ( ball.getLastPlayResult() ) {
							case BALL_STATE_GOAL:
							case BALL_STATE_POST_GOAL:
								animatedLabel.showText( GameMIDlet.getText( TEXT_PLAY_RESULT_GOAL ) );
							break;

							case BALL_STATE_OUT:
							case BALL_STATE_POST_OUT:
								animatedLabel.showText( GameMIDlet.getText( TEXT_PLAY_RESULT_OUT ) );
							break;

							case BALL_STATE_REJECTED_KEEPER:
								animatedLabel.showText( GameMIDlet.getText( TEXT_PLAY_RESULT_DEFENDED ) );
							break;

							case BALL_STATE_POST_BACK:
								animatedLabel.showText( GameMIDlet.getText( TEXT_PLAY_RESULT_POST ) );
							break;

//							case PLAY_RESULT_ANIMATION_ENDING:
//								animatedLabel.setText( TEXT_CHAMPION );
//							break;
						}
						scoreBox.updateBoard( score, ball.getLastPlayResult(), currentTurn );
						timeToNextTransition = TIME_BOARD_ANIMATION;
					break;
					
					case MODE_TRAINING:
						scoreBox.updateBoard( score, ball.getLastPlayResult(), currentTurn );
					//#if JAR != "min"
					case MODE_REPLAY:
					//#endif
						timeToNextTransition = TIME_PLAY_RESULT_TRAINING;
					break;
				}

				timeMinToAccelerate = timeToNextTransition - 1500;
			break; // fim case STATE_PLAY_RESULT

			//#if JAR != "min"
			case STATE_REPLAY_ONLY:
				// recupera a jogada armazenada no replay
				currentTurn = ( short ) ( replay.shooterPlayerIndex << 1 );
				lastKeeperPlay.set( replay.keeperPlay );
				lastShooterPlay.set( replay.shooterPlay );
				control.getCurrentPlay().set( lastShooterPlay );
				ball.loadReplayData( replay );
				currentKeeper.setLastGiveUpBehaviour( replay.keeperGaveUp );

				// define o batedor e goleiro atuais
				currentShooter.setVisible( true );
				currentKeeper.setVisible( true );

				currentShooter.setListener( this, 0 );
			//#endif
				
			case STATE_REPLAY:
				moveCameraTo( CAMERA_MAX_Y, 0 );
				animatedLabel.setVisible( false );

				//#if JAR == "min"
//# 				replayLabel.setVisible( true );
				//#else
					netAnimation.setVisible( false );
					replayControl.setReplayIconSequence( ReplayControl.REPLAY_ICON_SEQUENCE_APPEARING );
					replayControl.setVisible( true );
				//#endif

				// se o jogador tiver reiniciado o replay manualmente, não altera sua velocidade
				replayControl.setPaused( false );
				replayControl.setReplaySpeed( replayControl.getReplaySpeed() );

				ball.reset( currentKeeper );
				currentKeeper.reset();
				currentShooter.setSequence( SHOOTER_SEQUENCE_SHOOTING );
				
				timeToNextTransition = 0;
				
				updateZOrder();
			break; 
			
			case STATE_PARTIAL_RESULT:
				replayControl.setVisible( false );
				scoreBox.setState( GenericInfoBox.STATE_APPEARING );
				playResultBox.setState( GenericInfoBox.STATE_HIDING );

				timeToNextTransition = TIME_BOARD_ANIMATION;
				timeMinToAccelerate = TIME_BOARD_ANIMATION - 1500;
			break; // fim case STATE_PARTIAL_RESULT
			
			case STATE_ENDED:
				infoBox.setState( GenericInfoBox.STATE_HIDING );
				timeToNextTransition = GenericInfoBox.TRANSITION_TIME + 50;
				timeMinToAccelerate = timeToNextTransition;
			break;
			
			case STATE_FINAL_RESULT:
				moveCameraTo( 0, FP_CAMERA_MOVE_DEFAULT_TIME );
				replayControl.setVisible( false );
				replayControl.setReplayIconSequence( ReplayControl.REPLAY_ICON_SEQUENCE_HIDING );
				
				infoBox.setType( InfoBox.TYPE_FINAL_RESULT, score );
				infoBox.setState( GenericInfoBox.STATE_APPEARING );
				playResultBox.setState( GenericInfoBox.STATE_HIDING );

				switch ( mode ) {
					//#if MULTIPLAYER == "true"
//						case MODE_MULTIPLAYER_MATCH_HOME:
					//#endif
					case MODE_CHAMPIONSHIP_MATCH_HOME:
						MediaPlayer.play( score[ 0 ] > score[ 1 ] ? SOUND_INDEX_GAME_WIN : SOUND_INDEX_GAME_LOSE );
					break;

					//#if MULTIPLAYER == "true"
//					case MODE_MULTIPLAYER_MATCH_AWAY:
					//#endif
					case MODE_CHAMPIONSHIP_MATCH_AWAY:
						MediaPlayer.play( score[ 0 ] > score[ 1 ] ? SOUND_INDEX_GAME_LOSE : SOUND_INDEX_GAME_WIN );
					break;
					
					case MODE_2_PLAYERS:
						MediaPlayer.play( SOUND_INDEX_GAME_WIN );
					break;
				}
				
				GameMIDlet.setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, -1 );
			break; // fim case STATE_FINAL_RESULT
			
			case STATE_CHAMPION_APPEARS:
				MediaPlayer.play( SOUND_INDEX_CHAMPION );
				animatedLabel.showText( GameMIDlet.getText( TEXT_CHAMPION ) );
			break;
			
			case STATE_CHAMPION_SHOWN:
			break;
			
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				showMessage( "<ALN_H>" + GameMIDlet.getText( TEXT_PASS_TO_PLAYER_1 + ( state - STATE_PLAYER_1_TURN ) ), MESSAGE_TYPE_OK );
				control.setVisible( false );
				messageBox.setState( MessageBox.STATE_APPEARING );
				
				timeToNextTransition = TIME_PLAYER_TURN_MESSAGE;
				timeMinToAccelerate = timeToNextTransition >> 1;
			break;

			//#if MULTIPLAYER == "true"
//				case STATE_SENDING_PLAY:
//					sendCurrentPlay();
//					control.getCurrentPlay();
//				break;
//
//				case STATE_CONNECTION_ERROR:
//
//				break;
			//#endif
		} // fim switch ( state )
		setSoftKeyLabel();
		
		changingState = false;
		
		mutex.release();
	} // fim do método setState()	


	/**
	 * Verifica se o jogo acabou, ou seja, se a diferença de gols é maior do que o número de pênaltis restantes.
	 */
	private static final boolean hasEnded( short currentTurn, short[] score ) {
		//#if I_AM_THE_PENALTY_KING == "true"
//# 			if ( score[ 0 ] == score[ 1 ] )
//# 				return false;
//#
//# 			return true;
		//#else
		// diferença (absoluta) de placar
		int diff = Math.abs( score[ 0 ] - score[ 1 ] );
		if ( currentTurn < 10 ) {
			// primeiros 5 pênaltis para cada time
			if ( ( diff > 5 - ( currentTurn >> 1 ) ) || ( ( 5 - ( ( currentTurn + 1 ) >> 1 ) ) < score[ 1 ] - score[ 0 ] ) ) {
				// se a diferença de gols for maior que o número de penaltis a serem batidos, termina
				return true;
			}
		} else if ( diff > 0 && ( currentTurn & 1 ) == 0 ) {
			// cada time bate 1 pênalti - módulo de currentTurn = 0 significa que próximo batedor é do primeiro time;
			// logo, o segundo time acabou de bater o pênalti.
			// se houver diferença de gols após segundo time bater, termina
			return true;
		}

		return false;
		//#endif
	} // fim do método hasEnded()	


	/**
	 * Move a câmera para a posição "position" real em "time" segundos. Caso "time" seja menor ou igual a zero, 
	 * a câmera move-se instantaneamente para a posição recebida.
	 * 
	 * @param y posição y em pixels do topo da visualização da câmera.
	 * @param fp_time tempo em segundos que a câmera levará até chegar ao destino.
	 */
	private final void moveCameraTo( int y, int fp_time ) {
		if ( y > 0 ) {
			y = 0;
		} else if ( y < CAMERA_MAX_Y ) {
			y = CAMERA_MAX_Y;
		}

		if ( fp_time > 0 ) {
			y = NanoMath.toFixed( y );
			
			fp_timeToDestination = fp_time;
			fp_cameraCurrentTime = 0;
			cameraState = CAMERA_STATE_MOVING;
			fp_cameraY = NanoMath.toFixed( fieldGroup.getPosY() );
			fp_cameraSpeed = NanoMath.divFixed( y - fp_cameraY, fp_time );
		} else {
			fieldGroup.setPosition( 0, y );
			fp_timeToDestination = 0;
			fp_cameraSpeed = 0;
			cameraState = CAMERA_STATE_STOPPED;
		}
	} // fim do método moveCameraTo()


	public final byte getState() {
		return state;
	}


	public final short getCurrentTurn() {
		return currentTurn;
	}


	public final void fillScore( short[] score ) {
//		//#if I_AM_THE_PENALTY_KING == "true"
//			switch ( getMode() ) {
//				case Match.MODE_CHAMPIONSHIP_MATCH_AWAY:
//					this.score[ 0 ] = 0;
//					this.score[ 1 ] = 3;
//				break;
//
//				case Match.MODE_CHAMPIONSHIP_MATCH_HOME:
//					this.score[ 0 ] = 3;
//					this.score[ 1 ] = 0;
//				break;
//			}
//		//#endif

		score[ 0 ] = this.score[ 0 ];
		score[ 1 ] = this.score[ 1 ];
	}


	public final void onSequenceEnded( int id, int sequence ) {
		mutex.acquire();
		
		switch ( id ) {
			default:
			switch ( sequence ) {
				case SHOOTER_SEQUENCE_SHOOTING:
					final boolean replay = state != STATE_MOVING && state != STATE_PLAY_RESULT;
					ball.kick( lastShooterPlay, replay );

					currentKeeper.jump( lastKeeperPlay, lastShooterPlay, replay );
					currentShooter.setSequence( SHOOTER_SEQUENCE_SHOT );
					
					MediaPlayer.play( SOUND_INDEX_KICK_BALL );
					
					// move a câmera para mostrar a jogada
					//#if JAR == "min"
//# 					// não mostra tanto a parte superior do cenário nessa versão, já que é vazio
//# 					moveCameraTo( CAMERA_CONTROL_Y - 8 - playResultBox.PARTIAL_HEIGHT, ball.getTimeToGoal() );
					//#else
					moveCameraTo( CAMERA_CONTROL_Y, ball.getTimeToGoal() );
					//#endif
					
					switch ( state ) {
						//#if JAR != "min"
						case STATE_REPLAY_ONLY:
						//#endif
							
						case STATE_REPLAY:
							timeToNextTransition = TIME_PARTIAL_RESULT_ANIM + NanoMath.toInt( NanoMath.mulFixed( ball.getTimeToGoal(), NanoMath.toFixed( 1000 ) ) );
						break;
					}
				break;
			}
		}
		
		mutex.release();
	}
	
	public final void onFrameChanged( int id, int frameSequenceIndex ) {
	}
	
	
	/**
	 * Indica se é a vez do jogador (humano) chutar.
	 * 
	 * @return boolean indicando se é a vez do jogador chutar a gol.
	 */
	private final boolean isPlayerShooting() {
		switch ( mode ) {
			case MODE_CHAMPIONSHIP_MATCH_HOME:
			case MODE_TRAINING:
				return ( currentTurn & 1 ) == 0;
				
			case MODE_2_PLAYERS:
				return true;
			
			default:
				return ( currentTurn & 1 ) == 1;
		}
	}
	
	
	/**
	 * Retorna o intervalo considerado para efeitos de atualização do estado dos jogadores e bola, considerando
	 * a velocidade do replay.
	 * 
	 * @return 
	 */
	public final int getDelta() {
		return lastDelta;
	}
	

	private final void updateZOrder() {
        synchronized( this ) {
            final int fp_ballZ = ball.getRealPosition().z;

            if ( fp_ballZ < 0 ) {
                switch ( ball.getState() ) {
                    case Ball.BALL_STATE_GOAL:
                    case Ball.BALL_STATE_POST_GOAL:
                        setZOrder( ball, net, 1 );
                    break;

                    case Ball.BALL_STATE_POST_OUT:
                    case Ball.BALL_STATE_OUT:
                        setZOrder( ball, net, -1 );
                    break;
                }
            } else if ( fp_ballZ <= FP_REAL_PENALTY_TO_GOAL ) {
                // bola está entre a linha de fundo e a marca de pênalti
                // o teste da referência para currentKeeper é necessário pois pode ocorrer de este método ser chamado entre a
                // alocação/desalocação dos goleiros (no caso de aparelhos com pouca memória)
                if ( currentKeeper != null )
                    setZOrder( ball, currentKeeper, 1 );
            } else {
                // bola está antes da marca do pênalti (somente no caso de pênaltis batidos fortes na trave)
                // o teste da referência para currentShooter é necessário pois pode ocorrer de este método ser chamado entre a
                // alocação/desalocação dos batedores (no caso de aparelhos com pouca memória)
                if ( currentShooter != null )			
                    setZOrder( ball, currentShooter, 1 );
            }            
        }
	}
	
	
	private final void setZOrder( Drawable d1, Drawable d2, int offset ) {
		int d1Index = -1;
		int d2Index = -1;

		// try/catch em setZOrder (elimina travamentos aleatórios)
		try {
			for ( int i = 0; i < fieldGroup.getUsedSlots(); ++i ) {
				final Drawable d = fieldGroup.getDrawable( i );

				if ( d == d2 ) {
					d2Index = i;
					if ( d1Index >= 0 )
						break;
				} else if ( d == d1 ) {
					d1Index = i;
					if ( d2Index >= 0 )
						break;
				}
			}

			if ( d2Index < 0 || d1Index < 0 ) {
				//#if DEBUG == "true"
//					throw new RuntimeException( "setZOrder: índice inválido. dIndex: " + d2Index + ", ballIndex: " + d1Index );
				//#else
// 					return;
				//#endif
			}
			
			d1Index = Math.max( d1Index, 0 );
			d2Index = Math.max( d2Index + offset, 0 );

			if ( d1Index != d2Index && d1Index < fieldGroup.getUsedSlots() && d2Index < fieldGroup.getUsedSlots() )		
				fieldGroup.setDrawableIndex( d1Index, d2Index );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
				e.printStackTrace();
			//#endif
		}
	}

	
	//#if DEBUG == "true"

	private final Point getScreenPos( int fp_x, int fp_y ) {
		final int INITIAL_X = size.x >> 1;
		final int INITIAL_Y = goal.getPosY() + goal.getHeight();

		return new Point( INITIAL_X + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fp_x, FP_PENALTY_GOAL_WIDTH ), FP_REAL_GOAL_WIDTH ) ),
						  INITIAL_Y - NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fp_y, FP_PENALTY_FLOOR_TO_BAR ), FP_REAL_FLOOR_TO_BAR ) ) );
	}


	public final void paint( Graphics g ) {
        super.paint( g );
		
//		g.setColor( 0xffff00 );
//		g.drawRect( netAnimationViewport.x, netAnimationViewport.y, netAnimationViewport.width, netAnimationViewport.height );

//		g.setColor( 0xff0000 );
//		g.setClip( 0, 0, 1000, 1000 );
//
//
//		final int[][] positions = new int[][] {
//			{ Ball.FP_REAL_LEFT_POST_START, Ball.FP_REAL_BAR_BOTTOM  },
//			{ Ball.FP_REAL_LEFT_POST_START, 0 },
//			{ Ball.FP_REAL_LEFT_POST_END, Ball.FP_REAL_BAR_TOP },
//			{ Ball.FP_REAL_LEFT_POST_END, 0 },
//
//			{ Ball.FP_REAL_RIGHT_POST_START, Ball.FP_REAL_BAR_BOTTOM  },
//			{ Ball.FP_REAL_RIGHT_POST_START, 0 },
//			{ Ball.FP_REAL_RIGHT_POST_END, Ball.FP_REAL_BAR_TOP },
//			{ Ball.FP_REAL_RIGHT_POST_END, 0 },
//		};
//		for ( int i = 0; i < positions.length; ++i ) {
//			final Point pos = getScreenPos( positions[ i ][ 0 ], positions[ i ][ 1 ] );
//			g.fillRect( pos.x - 1, fieldGroup.getPosY() + pos.y - 1, 2, 2 );
//		}

//
//		if ( currentKeeper != null ) {
//			final Quad keeperQuad = currentKeeper.getCollisionArea();
//
//			final int INITIAL_X = goal.getPosX() + ( goal.getWidth() >> 1 );
//			final int INITIAL_Y = goal.getPosY() + goal.getHeight() + 8;
//
//			final Point3f realPosition = new Point3f( keeperQuad.position );
//
//			final Point topLeft = new Point( ( int ) ( INITIAL_X + ( realPosition.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//										 	 ( int ) ( INITIAL_Y - ( realPosition.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//
//			final Point3f temp = new Point3f( realPosition );
//
//			temp.addEquals( keeperQuad.right.mul( keeperQuad.width ) );
//			final Point topRight = new Point( ( int ) ( INITIAL_X + ( temp.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//										 	 ( int ) ( INITIAL_Y - ( temp.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//
//			temp.set( realPosition );
//			temp.addEquals( keeperQuad.top.mul( -keeperQuad.height ) );
//			final Point bottomLeft = new Point( ( int ) ( INITIAL_X + ( temp.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//										 	 ( int ) ( INITIAL_Y - ( temp.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//
//			temp.set( realPosition );
//			temp.addEquals( keeperQuad.top.mul( -keeperQuad.height ) );
//			temp.addEquals( keeperQuad.right.mul( keeperQuad.width ) );
//
//			final Point bottomRight = new Point( ( int ) ( INITIAL_X + ( temp.x * PENALTY_GOAL_WIDTH / FP_REAL_GOAL_WIDTH ) ),
//												 ( int ) ( INITIAL_Y - ( temp.y * PENALTY_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) ) );
//
//			g.setColor( 0xff0000 );
//			g.drawLine( topLeft.x, topLeft.y, topRight.x, topRight.y );
//			g.drawLine( topLeft.x, topLeft.y, bottomLeft.x, bottomLeft.y );
//			g.drawLine( bottomLeft.x, bottomLeft.y, bottomRight.x, bottomRight.y );
//			g.drawLine( topRight.x, topRight.y, bottomRight.x, bottomRight.y );
//		}
	}
	//#endif
	

	public final void saveReplayData( Replay replay ) {
		replay.shooterTeamIndex = ( currentTurn & 1 ) == 0 ? team1.getIndex() : team2.getIndex();
		replay.shooterPlayerIndex = ( byte ) ( ( currentTurn >> 1 ) % 100 );
		replay.keeperTeamIndex = ( currentTurn & 1 ) == 0 ? team2.getIndex() : team1.getIndex();
		replay.shooterPlay.set( lastShooterPlay );
		replay.keeperPlay.set( lastKeeperPlay );
		ball.saveReplayData( replay );
		replay.keeperGaveUp = currentKeeper.getLastGiveUp();
		replay.saveTime = System.currentTimeMillis();
	}
	
	
	/**
	 * Verifica se já é possível acelerar um estado do jogo através de teclas.
	 */
	private final void checkKeyPressedTimerExpired() {
		if ( !changingState && timeToNextTransition <= timeMinToAccelerate )
			timerExpired();
	}
	
	
	/**
	 * Pré-carrega alguns recursos sempre utilizados na tela de jogo, de forma a acelerar futuros carregamentos e evitar
	 * fragmentação de memória.
	 * 
	 * @throws java.lang.Exception 
	 */
	public static final void loadImages() throws Exception {
		AppMIDlet.gc();
		
		// gol (traves e travessão - rede é separada para permitir que a bola fique entre a trave e a rede)
		net = new DrawableImage( PATH_IMAGES + "goal_net.png" );
		net.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		
		final DrawableImage postLeft = new DrawableImage( PATH_IMAGES + "post_left.png" );
		
 		final DrawableImage postRight = new DrawableImage( postLeft );
 		postRight.setTransform( TRANS_MIRROR_H );
		final DrawableImage bar = new DrawableImage( PATH_IMAGES + "bar.png" );
		bar.setPosition( BAR_OFFSET_X, 0 );
		
		postLeft.setPosition( POST_ADJUST, bar.getHeight() );
		postRight.setPosition( bar.getPosX() + bar.getWidth() - postRight.getWidth() + BAR_OFFSET_X - POST_ADJUST, postLeft.getPosY() );
		
		goal = new DrawableGroup( 3 );
		goal.setSize( bar.getPosX() + bar.getWidth(), postRight.getPosY() + postRight.getHeight() );
		goal.insertDrawable( postLeft );
		goal.insertDrawable( postRight );
		goal.insertDrawable( bar );
		
		goal.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );		

		// animação da rede balançando após um gol
		netAnimation = new DrawableImage( PATH_IMAGES + "net.png" );
		netAnimation.defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
	}

	
//#if TOUCH == "true"

 	public final void onPointerDragged( int x, int y ) {
// 		if ( messageBox.isVisible() ) {
// 			messageBox.onPointerDragged( x, y );
// 			return;
// 		}
 	}


 	public final void onPointerPressed( int x, int y ) {
 		if ( messageBox.isVisible() ) {
 			messageBox.onPointerPressed( x, y );
 			return;
 		}

 		switch ( state ) {
 			case STATE_REPLAY:
 			case STATE_REPLAY_ONLY:
 			case STATE_REPLAY_WAIT:
 			case STATE_REPLAY_ONLY_WAIT:
 				// controla o replay através do ponteiro
 				replayControl.onPointerPressed( x, y );
 			break;

 			default:
 				keyPressed( ScreenManager.FIRE );
// 				keyPressed( ScreenManager.KEY_NUM5 );
 		}
 	}


 	public final void onPointerReleased( int x, int y ) {
 		if ( messageBox.isVisible() ) {
 			messageBox.onPointerReleased( x, y );
 			return;
 		}

 		switch ( state ) {
 			case STATE_REPLAY:
 			case STATE_REPLAY_ONLY:
 			case STATE_REPLAY_WAIT:
 			case STATE_REPLAY_ONLY_WAIT:
 				// controla o replay através do ponteiro
 				replayControl.onPointerReleased( x, y );
 			break;
 		}
 	}

	//#endif


	//#if MULTIPLAYER == "true"
//
//		private final void sendCurrentPlay() {
//			// envia os recordes de cada perfil para o servidor
//			try {
//				multiplayerMutex.acquire();
//
//				final ByteArrayOutputStream b = new ByteArrayOutputStream();
//				final DataOutputStream out = new DataOutputStream( b );
//
//				final Play currentPlay = control.getCurrentPlay();
//				out.writeInt( currentPlay.fp_curve );
//				out.writeInt( currentPlay.fp_direction );
//				out.writeInt( currentPlay.fp_height );
//				out.writeInt( currentPlay.fp_power );
//				// TODO escrever também a direção esperada da bola após a colisão, para que ambos os clientes exibam o
//				// mesmo resultado (existem componentes aleatórios nessa direção)
//
//				// escreve os dados globais do Nano Online
//				NanoOnline.writeGlobalData( out );
//				out.flush();
//
////			TODO	NanoConnection.post( NANO_ONLINE_URL + "multiplayer/play", b.toByteArray(), this, false );
//			} catch ( Exception e ) {
//				//#if DEBUG == "true"
//					e.printStackTrace();
//				//#endif
//			} finally {
//				multiplayerMutex.release();
//			}
//		}
//
//
//		public final void processData( int id, byte[] data ) {
//			try {
//				multiplayerMutex.acquire();
//				//#if DEBUG == "true"
//					System.out.println( "Match.processData( " + id + ", " + data + " )" );
//				//#endif
//
//				DataInputStream input = null;
//				boolean reading = true;
//				input = new DataInputStream( new ByteArrayInputStream( data ) );
//
//				byte paramId;
//
//				do {
//					paramId = input.readByte();
//
//					//#if DEBUG == "true"
//						System.out.println ( "Match.processData->id lido: " + id );
//					//#endif
//
//					switch ( paramId ) {
//						case PARAM_MATCH_FINAL_SCORES:
//						break;
//
//						case PARAM_MATCH_ID:
//						break;
//
//						case PARAM_MATCH_MIN_PLAYERS:
//						break;
//
//						case PARAM_PLAYER_GLOBAL_POSITION:
//						break;
//
//						case PARAM_PLAYER_GLOBAL_SCORE:
//						break;
//
//						case PARAM_PLAYER_ID:
//						break;
//
//						case PARAM_PLAYER_INFO_END:
//						break;
//
//						case PARAM_PLAYER_NICKNAME:
//						break;
//
//						case PARAM_PLAYER_ORDER:
//						break;
//
//						case PARAM_PLAYER_QUITTING:
//						break;
//
//						case PARAM_ROUND_DATA:
//						break;
//
//						case PARAM_TURN_INDEX:
//						break;
//					}
//				} while ( reading );
//			} catch ( Exception e ) {
//				//#if DEBUG == "true"
//					e.printStackTrace();
//				//#endif
//			} finally {
//				multiplayerMutex.release();
//			}
//		}
//
//
//		public final void onInfo( int id, int infoIndex, Object extraData ) {
//			try {
//				multiplayerMutex.acquire();
//
//				//#if DEBUG == "true"
//					System.out.println( "Match.onInfo( " + id + ", " + infoIndex + ", " + extraData + " )" );
//				//#endif
//			} finally {
//				multiplayerMutex.release();
//			}
//		}
//
//
//		public final void onError( int id, int errorIndex, Object extraData ) {
//			try {
//				multiplayerMutex.acquire();
//
//				//#if DEBUG == "true"
//					System.out.println( "Match.onError( " + id + ", " + errorIndex + ", " + extraData + " )" );
//				//#endif
//
//				switch ( errorIndex ) {
//					case ERROR_HTTP_CONNECTION_NOT_SUPPORTED:
//					case ERROR_CANT_GET_RESPONSE_CODE:
//					case ERROR_URL_BAD_FORMAT:
//					case ERROR_CONNECTION_EXCEPTION:
//					case ERROR_CANT_CLOSE_INPUT_STREAM:
//					case ERROR_CANT_CLOSE_CONNECTION:
//					case ERROR_CANT_CLOSE_OUTPUT_STREAM:
//						final Exception e = ( Exception ) extraData;
//						// TODO mostrar mensagem de erro e sair da partida
//// TODO						showMessage( MSG_TYPE_ERROR, NanoOnline.getText( TEXT_ERROR ) + errorIndex + ": " + e.getMessage() );
//					break;
//				}
//			} finally {
//				multiplayerMutex.release();
//			}
//		}
//
	//#endif


	public final void onChoose( Menu menu, int id, int index ) {
		switch ( index ) {
			case ReplayControl.INDEX_REWIND:
				setState( ( byte ) ( state - ( state & 1 ) ) );
			break;

			case ReplayControl.INDEX_RECORD:
                
                if ( mode == MODE_REPLAY ) //significa que na verdade temos um eject...
                    GameMIDlet.setScreen( SCREEN_LOAD_REPLAY );                
                else                
                    GameMIDlet.setScreen( SCREEN_SAVE_REPLAY );
            break;
                
			case ReplayControl.INDEX_EJECT:
                
                if ( mode == MODE_REPLAY )
                    GameMIDlet.setScreen( SCREEN_LOAD_REPLAY );                
                else
                    timerExpired();
                
			break;
		}
	}


	public final void onItemChanged( Menu menu, int id, int index ) {
	}


	public final void eventPerformed( Event evt ) {
		switch ( evt.eventType ) {
			case GenericInfoBox.EVENT_TYPE_BOX_HIDDEN:
				if ( evt.data != null ) {
					final byte nextState = ( ( Byte ) evt.data ).byteValue();
					if ( nextState != STATE_CREATED )
						setState( nextState );
				}
			break;

			default:
				switch ( state ) {
					case STATE_LOAD_PLAYERS_MSG:
					break;

					default:
						messageBox.setState( MessageBox.STATE_HIDING );
						timerExpired();
				}
		}
	}


	public final void hideNotify( boolean deviceEvent ) {
		// envia o evento para o estádio atualizar o tempo de exibição dos anúncios
		stadium.hideNotify( deviceEvent );
		
		if ( deviceEvent )
			keyPressed( ScreenManager.KEY_SOFT_RIGHT );
	}


	public final void showNotify( boolean deviceEvent ) {
		// envia o evento para o estádio atualizar o tempo de exibição dos anúncios
		stadium.showNotify( deviceEvent );

		setSoftKeyLabel();

		switch ( state ) {
			case STATE_BEGIN_TURN:
			case STATE_WAITING_PLAYER_1:
			case STATE_WAITING_PLAYER_2:
			case STATE_PLAYER_1_TURN:
			case STATE_PLAYER_2_TURN:
				if (mode != MODE_TRAINING) {
					//#if BAD_MP3 == "false"
					MediaPlayer.play(SOUND_INDEX_CROWD_AMBIENT, MediaPlayer.LOOP_INFINITE);
				//#endif
				}
			break;
		}
	}
	
}
