/**
 * NewsSelect.java
 *
 * Created on Apr 13, 2010 6:47:55 PM
 * 
 */

package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.newsfeeder.NewsFeederEntry;
import br.com.nanogames.components.online.newsfeeder.NewsFeederManager;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;
import core.Border;
import core.Constants;
import core.PenaltyNewsEntry;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Hashtable;

/**
 *
 * @author peter
 */
public final class NewsSelect extends AbstractContainer implements Constants, ScreenListener {

	private static final byte TOTAL_ENTRIES = NewsFeederManager.MAX_NEWS_ENTRIES + 10;

	protected final byte firstEntry;

	/** Estrutura que armazena a última entrada selecionada de cada menu. A chave de cada entrada é o id do menu,
	 * e o valor da chave é o índice da última entrada utilizada.
	 */
	private static int lastIndex;

	private final PenaltyNewsEntry[] entries;

	private static PenaltyNewsEntry entrySelected;

	/** Id de categoria de notícias: notícias gerais (deve estar sempre de acordo com o id do servidor). */
	private static final int CATEGORY_ID_GENERAL	= 1;
	/** Id de categoria de notícias: artilheiros (deve estar sempre de acordo com o id do servidor). */
	private static final int CATEGORY_ID_SCORERS	= 4;
	/** Id de categoria de notícias: tabela da copa (deve estar sempre de acordo com o id do servidor). */
	private static final int CATEGORY_ID_TABLE		= 5;

	
	public NewsSelect( MenuListener listener, int id, int backScreenIndex ) throws Exception {
		super( TOTAL_ENTRIES );

		firstEntry = 0;
		setBackIndex( backScreenIndex );

		setLayout( new FlowLayout( FlowLayout.AXIS_VERTICAL ) );
		NanoOnline.load( /*NanoOnline.LANGUAGE_pt_BR, APP_SHORT_NAME,*/ SCREEN_MAIN_MENU );
		final NewsFeederManager newsManager = new NewsFeederManager();
		newsManager.load();

		// de acordo com a opção selecionada, troca o id da categoria (artilharia, notícias, tabela, etc.)
		int newsId = -1;
		setId( id );
		switch ( id ) {
			case SCREEN_NEWS_LIST_SCORERS:
				newsId = CATEGORY_ID_SCORERS;
			break;

			case SCREEN_NEWS_LIST_TABLE:
				newsId = CATEGORY_ID_TABLE;
			break;

			case SCREEN_NEWS_LIST_GENERAL:
				newsId = CATEGORY_ID_GENERAL;
			break;
		}
		final NewsFeederEntry[] newsEntries = newsId < 0 ? newsManager.getEntries() : newsManager.getEntries( newsId );

		entries = new PenaltyNewsEntry[ newsEntries.length ];

		if ( entries.length <= 0 ) {
			final FormText text = new FormText( GameMIDlet.GetFont( FONT_INDEX_TITLE )/*, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT */);
			text.setText( GameMIDlet.getText( TEXT_NO_NEWS_FOUND ) );
			text.setScrollableY( true );
//			text.setScrollBarV( GameMIDlet.get );
//			text.getScrollBarV().setSize( 0, 1 );
			text.setFocusable( true );
			text.addEventListener( this );
			insertDrawable( text );
		} else {
			for ( byte i = 0; i < entries.length; ++i ) {
				final PenaltyNewsEntry e = new PenaltyNewsEntry( newsEntries[ i ] );
				entries[ i ] = e;
				e.setId( i );
				insertDrawable( e );
				e.addEventListener( this );
			}
		}

		setBorder( GameMIDlet.getBorder( Border.BORDER_COLOR_BLUE ) );
	}


	public final void eventPerformed( Event evt ) {
		final PenaltyNewsEntry e = ( evt.source.getId() < entries.length && evt.source.getId() >= 0 ) ? entries[ evt.source.getId() ] : null;
		switch ( evt.eventType ) {
			case Event.EVT_KEY_PRESSED:
				final Integer key = ( Integer ) evt.data;
				switch ( ScreenManager.getSpecialKey( key.intValue() ) ) {
					case ScreenManager.KEY_SOFT_LEFT:
						GameMIDlet.setScreen( SCREEN_NEWS_REFRESH );
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_SOFT_MID:
						if ( e != null ) {
							entrySelected = e;
							GameMIDlet.setScreen( SCREEN_NEWS_VIEW_GENERAL + ( getId() - SCREEN_NEWS_LIST_GENERAL ) );
						}
					break;

					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						GameMIDlet.setScreen( getId() );
					break;

					default:
						keyPressed( ScreenManager.getSpecialKey( key.intValue() ) );
				}
			break;

			//#if TOUCH == "true"
 			case Event.EVT_POINTER_PRESSED:
 				if ( e != null && e.isFocused() ) {
 					entrySelected = e;
 					GameMIDlet.setScreen( SCREEN_NEWS_VIEW_GENERAL + ( getId() - SCREEN_NEWS_LIST_GENERAL ) );
 				}
 			break;
				//#endif

			case Event.EVT_FOCUS_GAINED:
				if ( e != null )
					e.setFocused( true );
			break;

			case Event.EVT_FOCUS_LOST:
				if ( e != null )
					e.setFocused( false );
			break;
		}
	}


	public static final String getSelectedEntryText() {
		return entrySelected == null ? null : "<ALN_H>" + entrySelected.getEntry().getTitle() + "\n\n<ALN_L>" + entrySelected.getEntry().getContent();
	}


	public static final PenaltyNewsEntry getSelectedEntry() {
		return entrySelected;
	}


	public final void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_SOFT_LEFT:
				GameMIDlet.setScreen( SCREEN_NEWS_REFRESH );
			break;

			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_BACK:
				onBack();
			break;

			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}


	public final void hideNotify( boolean deviceEvent ) {
	}


	public final void showNotify( boolean deviceEvent ) {
	}


	public final void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

}
