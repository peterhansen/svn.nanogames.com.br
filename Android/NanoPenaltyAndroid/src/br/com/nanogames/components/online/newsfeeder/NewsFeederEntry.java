/**
 * NewsFeederEntry.java
 * 
 * Created on Oct 19, 2009, 9:55:44 PM
 *
 */

package br.com.nanogames.components.online.newsfeeder;

//#if AD_MANAGER == "true"
//# 	import br.com.nanogames.components.online.ad.Resource;
//#endif
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 *
 * @author Peter
 */
public final class NewsFeederEntry implements Serializable {

	public static final byte CATEGORY_APP_UPDATE_RECOMMENDED	= 1;
	public static final byte CATEGORY_APP_UPDATE_REQUIRED		= 2;
	public static final byte CATEGORY_NEW_APP_RELEASED			= 3;

	//<editor-fold desc="ÍNDICES DOS PARÂMETROS DO SERVIDOR">

	/** Índice de parâmetro de atualização de news feeder: instante da última atualização. Tipo do dado: long */
	public static final byte PARAM_LAST_UPDATE_TIME			= 0;

	/** Índice de parâmetro de atualização de news feeder: última atualização de versão armazenada (número da versão). Tipo do dado: string */
	public static final byte PARAM_LAST_APP_VERSION_UPDATE	= 1;

	/** Índice de parâmetro de atualização de news feeder: versão do news feeder. Tipo do dado: string */
	public static final byte PARAM_NEWS_FEEDER_VERSION		= 2;

	/** Índice de parâmetro de atualização de news feeder: data da entrada. Tipo do dado: long */
	public static final byte PARAM_ENTRY_TIME				= 3;

	/** Índice de parâmetro de atualização de news feeder: categoria da entrada. Tipo do dado: string */
	public static final byte PARAM_ENTRY_CATEGORY_TITLE		= 4;

	/** Índice de parâmetro de atualização de news feeder: título da entrada. Tipo do dado: string */
	public static final byte PARAM_ENTRY_TITLE				= 5;

	/** Índice de parâmetro de atualização de news feeder: conteúdo da entrada. Tipo do dado: string */
	public static final byte PARAM_ENTRY_CONTENT			= 6;

	/** Índice de parâmetro de atualização de news feeder: URL para acesso completo e/ou download de conteúdo relacionado
	 * à entrada. Tipo do dado: string */
	public static final byte PARAM_ENTRY_URL				= 7;

	/** Índice de parâmetro de atualização de news feeder: quantidade total de entradas a serem lidas. Tipo do dado: byte */
	public static final byte PARAM_TOTAL_ENTRIES			= 8;

	/** Índice de parâmetro de atualização de news feeder: quantidade total de recursos especiais usados no conteúdo,
	 * como imagens, sons, animações, etc. Caso esse parâmetro não seja encontrado, é considerado que não há recursos
	 * especiais em uso.
	 * Tipo dos dados:
	 *	byte (quantidade de recursos especiais)
	 *	para cada recurso:
	 *		byte (tipo do recurso)
	 *		byte[] (dados dependem do tipo do recurso)
	 */
	public static final byte PARAM_TOTAL_RESOURCES			= 9;

	/** Índice de parâmetro de atualização de news feeder: indicador de fim de uma entrada (a quantidade de parâmetros é variável) */
	public static final byte PARAM_ENTRY_END				= 10;

	/** Índice de parâmetro de atualização de news feeder: indicador de fim de uma entrada (a quantidade de parâmetros é variável) */
	public static final byte PARAM_NEWS_FEEDER_END			= 11;

	/** Índice de parâmetro de atualização de news feeder: início de leitura de entrada de atualização de aplicativo.
	 * Tipos dos dados: boolean (indicando se é obrigatória) e string (versão da atualização) */
	public static final byte PARAM_ENTRY_APP_UPDATE         = 12;

	/** Índice de parâmetro de atualização de news feeder: id da categoria da entrada. Tipo do dado: int
	 * @since 0.0.3
	 */
	public static final byte PARAM_ENTRY_CATEGORY_ID		= 13;

	/** Índice de parâmetro de atualização de news feeder: visibilidade da categoria da entrada. Tipo do dado: boolean
	 * @since 0.0.3
	 */
	public static final byte PARAM_ENTRY_CATEGORY_VISIBILITY	= 14;

	/** Índice de parâmetro de atualização de news feeder: quantidade de categorias. Tipo do dado: short
	 * @since 0.0.3
	 */
	public static final byte PARAM_TOTAL_CATEGORIES			= 15;

	/** Índice de parâmetro de atualização de news feeder: fim dos dados de uma categoria. Tipo do dado: nenhum
	 * @since 0.0.3
	 */
	public static final byte PARAM_ENTRY_CATEGORY_END		= 16;

	/** Índice de parâmetro de atualização de news feeder: descrição de uma categoria de notícias. Tipo do dado: string
	 * @since 0.0.3
	 */
	public static final byte PARAM_ENTRY_CATEGORY_DESCRIPTION		= 17;

	//</editor-fold>

	/** Data da entrada. */
	private long time;

	/***/
	private int categoryId = -1;

	/** Título da entrada. */
	private String title = "";

	/** Conteúdo (texto) da entrada. */
	private String content = "";

	/** Indica se a entrada já foi lida pelo usuário. */
	private boolean read; // TODO trocar por quantidade de leituras

	/** URL para download de conteúdo e/ou acesso à informação completa. */
	private String URL = "";


	//#if AD_MANAGER == "true"
//# 		/** IDs dos recursos extras. Os recursos só são carregados ao chamar loadResources(). */
//# 		private int[] resourcesIds = new int[ 0 ];
//#
//# 		/**
//# 		 * Recursos extras da entrada. Para evitar desperdício de memória e demora na leitura, os recursos não são
//# 		 * carregados automaticamente com a entrada, e sim somente após uma chamada a loadResources(). */
//# 		private Resource[] resources;
	//#endif


	public final void write( DataOutputStream output ) throws Exception {
		output.writeLong( getTime() );
		output.writeBoolean( isRead() );
		output.writeUTF( getTitle() );
		output.writeUTF( getContent() );
		output.writeUTF( getURL() );
		output.writeInt( getCategoryId() );

		//#if AD_MANAGER == "true"
//# 			// deixa para gravar recursos extras (caso existam) no final
//# 			output.writeByte( resourcesIds.length );
		//#endif
	}


	public final void read( DataInputStream input ) throws Exception {
		setTime( input.readLong() );
		setRead( input.readBoolean() );
		setTitle( input.readUTF() );
		setContent( input.readUTF() );
		setURL( input.readUTF() );
		setCategoryId( input.readInt() );

		//#if AD_MANAGER == "true"
//# 			setTotalResources( input.readByte() );
		//#endif
	}


	public final int getCategoryId() {
		return categoryId;
	}


	public final void setCategoryId( int categoryId ) {
		this.categoryId = categoryId;
	}


	public final String getContent() {
		return content;
	}


	public final void setContent( String content ) {
		this.content = content;
	}


	public final boolean isRead() {
		return read;
	}


	public final void setRead( boolean read ) {
		this.read = read;
	}


	public final long getTime() {
		return time;
	}


	public final void setTime( long time ) {
		this.time = time;
	}


	public final String getTitle() {
		return title;
	}


	public final void setTitle( String title ) {
		this.title = title;
	}


	//#if AD_MANAGER == "true"
//# 		public final int getTotalResources() {
//# 			return resourcesIds.length;
//# 		}
//# 
//# 
//# 		private final void setTotalResources( int totalResources ) {
//# 			resourcesIds = new int[ totalResources ];
//# 		}
	//#endif


	/**
	 * Obtém a URL para acesso aos detalhes da entrada.
	 * @return String representando a URL da entrada. Caso não haja URL específica, é retornada uma String vazia (nunca retorna null).
	 */
	public final String getURL() {
		return URL;
	}


	public final void setURL( String URL ) {
		if ( URL == null )
			URL = "";
		
		this.URL = URL;
	}


	public final void parseParams( DataInputStream input ) throws Exception {
		byte paramId = -1;

		do {
			paramId = input.readByte();

			switch ( paramId ) {
				case PARAM_ENTRY_CONTENT:
					setContent( input.readUTF() );
				break;

				case PARAM_ENTRY_TIME:
					setTime( input.readLong() );
				break;

				case PARAM_ENTRY_TITLE:
					setTitle( input.readUTF() );
				break;

				case PARAM_ENTRY_URL:
					setURL( input.readUTF() );
				break;

				case PARAM_ENTRY_CATEGORY_ID:
					setCategoryId( input.readInt() );
				break;

				case PARAM_TOTAL_RESOURCES:
					//#if AD_MANAGER == "true"
//# 						final short totalResources = input.readShort();
//#
//# 						// o indicador de total de recursos indica também o início da leitura dos dados dos recursos
//# 						final Resource[] temp = new Resource[ totalResources ];
//#
//# 						for ( short i = 0; i < totalResources; ++i ) {
//# 							temp[ i ] = new Resource();
//# 							temp[ i ].read( input );
//# 						}
//#
//# 						setResources( temp );
					//#endif
				break;

				case PARAM_ENTRY_END:
				break;
			}
		} while ( paramId != PARAM_ENTRY_END );
		//#if DEBUG == "true"
//# 			System.out.println( "News feeder entry read: " + this );
		//#endif
	} // fim do método parseParams( DataInputStream )


	//#if AD_MANAGER == "true"
//# 		/**
//# 		 * Carrega os recursos usados pela entrada.
//# 		 *
//# 		 * @param force
//# 		 * @throws java.lang.Exception
//# 		 *
//# 		 * @see #unloadResources()
//# 		 * @see #getResources(boolean)
//# 		 */
//# 		public final void loadResources( boolean force ) throws Exception {
//# 			if ( force || resources == null || resources.length != resourcesIds.length ) {
//# 				resources = null;
//# 				resources = new Resource[ resources.length ];
//#
//# 				for ( short i = 0; i < resources.length; ++ i ) {
//# 	//			TODO	resources[ i ] = ResourceManager.getResource( resourcesIds[ i ] );
//# 				}
//# 			}
//# 		}
//#
//#
//# 		/**
//# 		 * Descarrega os recursos usados pela entrada.
//# 		 * @see #loadResources(boolean)
//# 		 * @see #getResources(boolean)
//# 		 */
//# 		public final void unloadResources() {
//# 			resources = null;
//# 		}
//#
//#
//# 		/**
//# 		 * Obtém a lista de recursos usados pela entrada.
//# 		 * @param loadIfNecessary indica se os recursos devem ser carregados caso necessário.
//# 		 * @return array dos recursos usados pela entrada.
//# 		 *
//# 		 * @see #loadResources(boolean)
//# 		 * @see #unloadResources()
//# 		 */
//# 		public final Resource[] getResources( boolean loadIfNecessary ) throws Exception {
//# 			if ( loadIfNecessary )
//# 				loadResources( false );
//#
//# 			return resources;
//# 		}
//#
//#
//# 		private final void setResources( Resource[] resources ) {
//# 			this.resources = resources;
//#
//# 			resourcesIds = new int[ resources == null ? 0 : resources.length ];
//# 			for ( short i = 0; i < resourcesIds.length; ++i ) {
//# 	//		TODO	resourcesIds[ i ] = resources[ i ].getResourceRMSId();
//# 			}
//# 		}
	//#endif


	//#if DEBUG == "true"
//# 		public final String toString() {
//# 			final StringBuffer b = new StringBuffer();
//# 			b.append( "NewsFeederEntry" );
//# 			b.append( "\n\tcategory id: " );
//# 			b.append( categoryId );
//# 			b.append( "\n\ttitle: " );
//# 			b.append( title );
//# 			b.append( "\n\tread: " );
//# 			b.append( read );
//# 			b.append( "\n\tURL: " );
//# 			b.append( URL );
//# 			b.append( "\n\ttime: " );
//# 			b.append( time );
//# 			b.append( "\n\tcontent: " );
//# 			b.append( content );
//# 			b.append( '\n' );
//# 
//# 			return b.toString();
//# 		}
	//#endif


}
