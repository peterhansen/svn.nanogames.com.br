/**
 * NanoConnection.java
 * 
 * Created on 31/Out/2008, 14:40:36
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.DynamicByteArray;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;

//#if J2SE == "false"
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
//#endif

/**
 *
 * @author Peter
 */
public class NanoConnection {
	
	/** Método HTTP: GET */
	public static final byte METHOD_GET		= 0;
	/** Método HTTP: POST */
	public static final byte METHOD_POST	= 1;
	/** Método HTTP: PUT (emulado, pois não há suporte nativo a PUT em J2ME) */
	public static final byte METHOD_PUT		= 2;
	/** Método HTTP: DELETE (emulado, pois não há suporte nativo a DELETE em J2ME) */
	public static final byte METHOD_DELETE	= 3;
	/** Método HTTP: HEAD */
	public static final byte METHOD_HEAD	= 4;
	
	/** Identificador da conexão atual. */
	protected static int currentId;
	
	/***/
	protected static final Mutex mutex = new Mutex();
	
	/** Armazena os ids das conexões ativas. */
	protected static final Hashtable activeConnections = new Hashtable();
	
	
	/**
	 * Chamada equivalente a <code>open( url, METHOD_GET, null, listener, false )</code>.
	 * 
	 * @param url
	 * @param listener
	 * @return
	 * @see post(String, byte[], ConnectionListener, boolean)
	 * @see open(String, byte, byte[], ConnectionListener, boolean)
	 * @see #cancel(int)
	 */
	public static final int get( final String url, final ConnectionListener listener ) {
		return open( url, METHOD_GET, null, listener, false );
	}
	

	/**
	 * Equivalente à chamada de <code>open( url, METHOD_POST, data, listener, useKeyValuePairs )</code>.
	 * 
	 * @param url
	 * @param data
	 * @param listener
	 * @param useKeyValuePairs
	 * @return
	 * @see get(String, ConnectionListener)
	 * @see open(String, byte, byte[], ConnectionListener, boolean)
	 * @see #cancel(int)
	 */
	public static final int post( final String url, final byte[] data, final ConnectionListener listener, final boolean useKeyValuePairs ) {
		return open( url, METHOD_POST, data, listener, useKeyValuePairs );
	}
	
	
	/**
	 * Equivalente à chamada de <code>open( url, METHOD_PUT, data, listener, useKeyValuePairs )</code>.
	 *
	 * @param url
	 * @param data
	 * @param listener
	 * @param useKeyValuePairs
	 * @return
	 * @see get(String, ConnectionListener)
	 * @see open(String, byte, byte[], ConnectionListener, boolean)
	 * @see #cancel(int)
	 */
	public static final int put( final String url, final byte[] data, final ConnectionListener listener, final boolean useKeyValuePairs ) {
		return open( url, METHOD_PUT, data, listener, useKeyValuePairs );
	}


	/**
	 * Abre uma conexão HTTP.
	 * 
	 * @param url endereço a ser conectado.
	 * @param method método de conexão HTTP. Valores válidos:
	 * <ul>
	 * <li>METHOD_GET</li>
	 * <li>METHOD_POST</li>
	 * <li>METHOD_PUT</li>
	 * <li>METHOD_DELETE</li>
	 * <li>METHOD_HEAD</li>
	 * </ul>
	 * @param data dados da conexão. Esse parâmetro é desconsiderado no caso de conexão GET, ou caso seja nulo.
	 * @param listener listener que receberá os eventos indicando o progresso da conexão, eventuais erros e que receberá os
	 * dados retornados pelo servidor no caso de uma conexão bem-sucedida.
	 * @param useKeyValuePairs TODO comentar
	 * @return o id da conexão atual, que será o mesmo repassado ao listener para identificar de que pedido de conexão o
	 * evento foi gerado.
	 * @throws NullPointerException caso <code>url</code>, <code>method</code> ou <code>listener</code> sejam nulos.
	 * @see #cancel(int)
	 * @see get(String, ConnectionListener)
	 * @see post(String, byte[], ConnectionListener, boolean)
	 */
	public static final int open( final String url, final byte method, final byte[] data, final ConnectionListener listener, final boolean useKeyValuePairs ) throws NullPointerException {
		// TODO modo de leitura dos dados através de streams
		// TODO adicionar opção para compressão dos dados em conexões http
		try {
			mutex.acquire();
			
			//#if DEBUG == "true"
//# 			System.out.print( "NanoConnection.open:\n\turl: " + ( url == null ? "null" : url ) + "\n\tmethod: " + method + "\n\tdata: " );
//#
			//#endif
			

			final int ID = currentId++;
			activeConnections.put( new Integer( ID ), new Object() );
			final Thread connectThread = new Thread() {
				public final void run() {
					// dá chance à thread chamadora de continuar a execução, e assim poder armazenar o ID
					checkYield( ID );

					//#if J2SE == "false"
						HttpConnection c = null;
					//#else
					//#endif
					InputStream is = null;
					OutputStream os = null;

					//#if BLACKBERRY_API == "false"
						// TODO seguir dica de http://stackoverflow.com/questions/1126018/tunnel-failed-in-blackberry-bold-why ????
						try {
							Class.forName( "javax.microedition.io.HttpConnection" );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
	//# 							AppMIDlet.log( e , "17");
							//#endif

							listener.onError( ID, ConnectionListener.ERROR_HTTP_CONNECTION_NOT_SUPPORTED, e );
							return;
						}
					//#endif

					DynamicByteArray array = new DynamicByteArray();

					try {
						//#if J2SE == "false"
							// os métodos PUT e DELETE são emulados, por isso precisam de alteração na URL
							String finalURL;
							switch ( method ) {
								case METHOD_PUT:
									finalURL = url + "?_method=PUT";
								break;

								case METHOD_DELETE:
									finalURL = url + "?_method=DELETE";
								break;

								default:
									finalURL = url;
							}

							//#if BLACKBERRY_API == "true"
	//# 							// usa a pilha TCP diretamente -> http://www.blackberry.com/knowledgecenterpublic/livelink.exe/fetch/2000/348583/800451/800563/What_Is_-_Different_ways_to_make_an_HTTP_or_socket_connection.html?nodeid=826935&vernum=0
	//# 							finalURL += ";deviceside=true";
							//#endif

							c = ( HttpConnection ) Connector.open( finalURL, Connector.READ_WRITE, false );
							listener.onInfo( ID, ConnectionListener.INFO_CONNECTION_OPENED, null );
							checkYield( ID );

							// Set the request method and headers
							boolean post = false;
							switch ( method ) {
								case METHOD_GET:
									c.setRequestMethod( HttpConnection.GET );
								break;

								case METHOD_HEAD:
									c.setRequestMethod( HttpConnection.HEAD );
								break;

								case METHOD_PUT:
								case METHOD_DELETE:
								case METHOD_POST:
									c.setRequestMethod( HttpConnection.POST );
									post = true;
								break;

								//#if DEBUG == "true"
//# 							default:
//# 								throw new IllegalArgumentException( "Invalid HTTP method: " + method );
								//#endif
							}

							final String platform = AppMIDlet.getPlatform();
							// alguns aparelhos, como o Nokia N76, não enviam o user-agent ao conectar pelo J2ME. Nesse caso, envia
							// a plataforma para tentar a detecção.
							c.setRequestProperty( "User-Agent", platform == null ? "Profile/MIDP-2.0 Configuration/CLDC-1.0" : platform );
							c.setRequestProperty( "Content-Language", "en-US" ); // TODO pt-BR?
							c.setRequestProperty( "Connection", "close" );

							if ( post && data != null ) {
								//#if DEBUG == "true"
//# 								System.out.println( "POST DATA LENGTH: " + data.length );
								//#endif

								// content-type é diferente para enviar arquivos binários
								c.setRequestProperty( "Content-Type", useKeyValuePairs ? "application/x-www-form-urlencoded" : "application/octet-stream" ); 
								c.setRequestProperty( "Content-Length", String.valueOf( data.length ) );

								os = c.openDataOutputStream();
								listener.onInfo( ID, ConnectionListener.INFO_OUTPUT_STREAM_OPENED, null );
								checkYield( ID );

								os.write( data );
								listener.onInfo( ID, ConnectionListener.INFO_DATA_WRITTEN, null );
								checkYield( ID );
							}

							// Não deve-se chamar output.flush() para evitar problemas com alguns aparelhos
							// http.getResponseCode() implicitamente chama flush()

							// Getting the response code will open the connection, send the request, and read 
							// the HTTP response headers. The headers are stored until requested.
							try {
								final int rc = c.getResponseCode();
								listener.onInfo( ID, ConnectionListener.INFO_RESPONSE_CODE, new Integer( rc ) );
								checkYield( ID );
							} catch ( Exception e ) {
								//#if DEBUG == "true"
	//# 								AppMIDlet.log( e , "18" );
								//#endif

								listener.onError( ID, ConnectionListener.ERROR_CANT_GET_RESPONSE_CODE, e );
							}

							is = c.openDataInputStream();
							listener.onInfo( ID, ConnectionListener.INFO_INPUT_STREAM_OPENED, null );
							checkYield( ID );

							// Get the length and process the data
							final int len = ( int ) c.getLength();
							listener.onInfo( ID, ConnectionListener.INFO_CONTENT_LENGTH_TOTAL, new Integer( len ) );
							checkYield( ID );

							// Cria o buffer temporário que irá armazenar os dados lidos
							// não utiliza o método array.readInputStream() porque ele não permite acompanhamento do progresso
							final byte[] buffer = new byte[ Runtime.getRuntime().totalMemory() < 1200000 ? 8096 : 65536 ];

							// Lê os dados da stream de entrada
							int nBytes;
							int bytesRead = 0;
							while( ( nBytes = is.read( buffer, 0, buffer.length ) ) != -1 ) {
								array.insertData( buffer, 0, nBytes );

								bytesRead += nBytes;
								listener.onInfo( ID, ConnectionListener.INFO_CONTENT_LENGTH_READ, new Integer( bytesRead ) );
								checkYield( ID );
							}

							final byte[] readData = array.getData();
							//#if DEBUG == "true"
//# 							System.out.println( "DATA READ LENGTH: " + readData.length );
//# 						//		NanoMath.printByteArray( readData );
							//#endif
							listener.processData( ID, readData );

	//						is.read(); // TODO necessário? (aparentemente é necessário para fechar corretamente a conexão)
							listener.onInfo( ID, ConnectionListener.INFO_CONNECTION_ENDED, null );
						//#else
						//#endif
					} catch ( ClassCastException e ) {
						//#if DEBUG == "true"
//# 						AppMIDlet.log( e , "19");
						//#endif

						listener.onError( ID, ConnectionListener.ERROR_URL_BAD_FORMAT, e );
					} catch ( Throwable e ) {
						//#if DEBUG == "true"
 						AppMIDlet.log( e , "20");
						//#endif

						listener.onError( ID, ConnectionListener.ERROR_CONNECTION_EXCEPTION, e );
					} finally {
						cancel( ID );
						array = null;
						
						if ( is != null ) {
							try {
								is.close();
							} catch ( Exception e ) {
								listener.onError( ID, ConnectionListener.ERROR_CANT_CLOSE_INPUT_STREAM, e );
							}
						}

						if ( os != null ) {
							try {
								os.close();
							} catch ( Exception e ) {
                                //#if JAVA_VERSION != "ANDROID"
								//# listener.onError( ID, ConnectionListener.ERROR_CANT_CLOSE_OUTPUT_STREAM, e );
                                //#endif
							}
						}

						//#if J2SE == "false"
							if ( c != null ) {
								try {
									c.close();
								} catch ( Exception e ) {
									listener.onError( ID, ConnectionListener.ERROR_CANT_CLOSE_CONNECTION, e );
								}
							}
						//#endif
					} // fim finally
				}
			};
//			connectThread.setPriority( Thread.NORM_PRIORITY - 1 ); // TODO testar prioridade ideal para Thread de conexão
//			connectThread.setPriority( Thread.MIN_PRIORITY );
			connectThread.start();

			return ID;
		} finally {
			mutex.release();
		}
	}
	
	
	/**
	 * 
	 * @param id
	 * @throws java.lang.RuntimeException
	 */
	protected static final void checkYield( int id ) throws RuntimeException {
		if ( !activeConnections.containsKey( new Integer( id ) ) ) {
			//#if DEBUG == "true"
//# 				throw new RuntimeException( "NanoConnection: conexão cancelada #" + id );
			//#else
			throw new RuntimeException();
			//#endif
		}
		
		Thread.yield();
	}
	
	
	/**
	 * Cancela uma conexão. Como a conexão é feita numa Thread própria, o cancelamento pode não ser imediato.
	 * @param id id da conexão, conforme retornado pelo método <code>open(String, String, byte[], ConnectionListener)</code>.
	 * @see get(String, ConnectionListener)
	 * @see post(String, byte[], ConnectionListener, boolean)
	 * @see #open(String, String, byte[], ConnectionListener)
	 */
	public static final void cancel( int id ) {
		activeConnections.remove( new Integer( id ) );
	}
	
}
