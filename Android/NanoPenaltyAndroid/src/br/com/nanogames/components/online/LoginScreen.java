/**
 * RegisterScreen2.java
 * 
 * Created on 21/Dez/2008, 15:51:00
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.CheckBox;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.TextBox;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Hashtable;

/**
 * Classe que descreve a funcionalidade das telas de importar perfil e de login. 
 * 
 * @author Peter
 */
public final class LoginScreen extends NanoOnlineContainer implements ConnectionListener {

	// <editor-fold defaultstate="collapsed" desc="CÓDIGOS DE RETORNO DO LOGIN DE USUÁRIO JÁ EXISTENTE">

	// ATENÇÃO: ESSES CÓDIGOS DEVEM *SEMPRE* ESTAR DE ACORDO COM OS CÓDIGOS RETORNADOS PELO SERVIDOR!

	// obs.: código de retorno zero é OK (RC_OK)
	
	/** Código de retorno do login de usuário: erro: usuário não encontrado. */
	private static final byte RC_ERROR_NICKNAME_NOT_FOUND		= 1;
	
	/** Código de retorno do login de usuário: erro: password inválida. */
	private static final byte RC_ERROR_PASSWORD_INVALID			= 2;
	
	// </editor-fold>

	
	/***/
	private static final byte ENTRY_NICKNAME				= 0;
	/***/
	private static final byte ENTRY_PASSWORD				= ENTRY_NICKNAME + 1;
	
	/** Checkbox para lembrar (ou não) a password do jogador automaticamente. */
	private static final byte ENTRY_REMEMBER_PASSWORD		= ENTRY_PASSWORD + 1;
	
	/***/
	private static final byte ENTRY_BUTTON_OK				= ENTRY_REMEMBER_PASSWORD + 1;
	/***/
	private static final byte ENTRY_BUTTON_BACK			= ENTRY_BUTTON_OK + 1;
	
	private static final byte ENTRY_BUTTON_FORGOT_PASSWORD = ENTRY_BUTTON_BACK + 1;
	
	/***/
	private static final byte ENTRIES_TOTAL = ENTRY_BUTTON_FORGOT_PASSWORD + 3;
	
	/***/
	private static final byte EDIT_LABELS_TOTAL = 2;
	
	/***/
	private final TextBox[] textBoxes = new TextBox[ EDIT_LABELS_TOTAL ];

	/** Indica se a password do usuário deve ser lembrada automaticamente. */
	private final CheckBox checkBoxRememberPassword;

	/***/
	private final Button buttonOK;
	
	/***/
	private final Button buttonBack;
	
	/***/
	private final Button buttonForgot;

	/***/
	private int currentConnection = -1;
	
	private final int screenId;
	
	private final int profileIndex;

	private int lastAction;
	
	
	public LoginScreen( int profileIndex, int screenId, int backIndex ) throws Exception {
		super( ENTRIES_TOTAL );
		
		setBackIndex( backIndex >= 0 ? backIndex : SCREEN_PROFILE_SELECT );
		
		this.screenId = screenId;
		this.profileIndex = profileIndex;
		
		Customer customer;
		if ( profileIndex == Customer.ID_NONE ) {
			customer = null;
		} else {
			customer = NanoOnline.getCustomer( profileIndex );
		}
		
		switch ( screenId ) {
			case SCREEN_LOGIN:
				final FormText textLogin = new FormText( NanoOnline.getFont( FONT_BLACK ), 0 );
				textLogin.setText( NanoOnline.getText( TEXT_LOGIN_TEXT ) );			
				textLogin.addEventListener( this );
				insertDrawable( textLogin );
			break;
			
			case SCREEN_SYNC_CUSTOMER_INFO:
				final FormText textSync = new FormText( NanoOnline.getFont( FONT_BLACK ), 0 );
				textSync.setText( NanoOnline.getText( TEXT_SYNC_REQUIRED ) );			
				textSync.addEventListener( this );
				insertDrawable( textSync );
			break;
		}
		
		final byte[] MAX_CHARS = { Customer.EMAIL_MAX_CHARS, 20 };
		final byte[] INPUT_MODE = { TextBox.INPUT_MODE_ANY, TextBox.INPUT_MODE_PASSWORD };
		final byte[] titles = { TEXT_NICKNAME_OR_EMAIL, TEXT_PASSWORD };
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			final TextBox textBox = NanoOnline.getTextBox( i, MAX_CHARS[ i ], INPUT_MODE[ i ], titles[ i ] );
			textBoxes[ i ] = textBox;
			
			if ( customer != null ) {
				switch ( i ) {
					case ENTRY_NICKNAME:
						textBoxes[ i ].setText( customer.getNickname(), false );
					break;

					case ENTRY_PASSWORD:
						if ( customer != null && customer.isRememberPassword() )
							textBoxes[ i ].setText( customer.getPassword(), false );
					break;
				}
			}
			
			textBox.addEventListener( this );
			textBox.setPasswordMask( PASSWORD_MASK_DEFAULT );
			
			insertDrawable( textBox );
		}
		
		// adiciona o checkbox "lembrar minha senha"
		checkBoxRememberPassword = new CheckBox( NanoOnline.getCheckBoxButton(), NanoOnline.getFont( FONT_BLACK ), NanoOnline.getText( TEXT_REMEMBER_PASSWORD ), true );
		checkBoxRememberPassword.setChecked( customer == null || customer.isRememberPassword() );
		insertDrawable( checkBoxRememberPassword );

		buttonOK = NanoOnline.getButton( TEXT_OK, ENTRY_BUTTON_OK, this );
		insertDrawable( buttonOK );

		buttonBack = NanoOnline.getButton( TEXT_BACK, ENTRY_BUTTON_BACK, this );
		insertDrawable( buttonBack );
		
		buttonForgot = NanoOnline.getButton( TEXT_FORGOT_PASSWORD, ENTRY_BUTTON_FORGOT_PASSWORD, this );
		insertDrawable( buttonForgot );
	}
	

	public final void processData( int id, byte[] data ) {
		onConnectionEnded( false );

		//#if DEBUG == "true"
//# 			System.out.println( "processData: " + ( data == null ? -1 : data.length ) );
		//#endif
			
		if ( data != null ) {
			DataInputStream input = null;
			
			try {
				final Hashtable table = NanoOnline.readGlobalData( data );
				
				// o código de retorno é obrigatório
				final short returnCode = ( ( Short ) table.get( new Byte( ID_RETURN_CODE ) ) ).shortValue();
				byte errorEntryIndex = -1;
				byte errorText = -1;
				
				switch ( returnCode ) {
					case RC_OK:
						// grava as informações do usuário no RMS e mostra a tela de confirmação
						final Customer customer = profileIndex >= 0 ? NanoOnline.getCustomer( profileIndex ) : new Customer();
						
						final byte[] specificData = ( byte[] ) table.get( new Byte( ID_SPECIFIC_DATA ) );

						switch ( lastAction ) {
							case ENTRY_BUTTON_FORGOT_PASSWORD:
								String email = null;
								
								try {
									input = new DataInputStream( new ByteArrayInputStream( specificData ) );
									byte paramId = -1;

									do {
										paramId = input.readByte();

										switch ( paramId ) {
											case ProfileScreen.PARAM_EMAIL:
												email = input.readUTF();
											break;
										}
									} while ( paramId != ProfileScreen.PARAM_INFO_END );
								} finally {
									if ( input != null ) {
										try {
											input.close();
										} catch ( Exception e ) {
										}
									}
								}

								removeAllComponents();
								
								final FormText text = new FormText( NanoOnline.getFont( FONT_BLACK ), 5 );
								text.setText( NanoOnline.getText( TEXT_NEW_PASSWORD_SENT ) + email + "." );
								text.addEventListener( this );
								insertDrawable( text );

								insertDrawable( buttonBack );
								refreshLayout();
								
								NanoOnline.getProgressBar().showConfirmation( TEXT_NEW_PASSWORD_SENT_CONFIRMATION );
							break;

							default:
								customer.parseParams( specificData );
								customer.setPassword( textBoxes[ ENTRY_PASSWORD ].getText() );
								customer.setRememberPassword( checkBoxRememberPassword.isChecked() );

								switch ( screenId ) {
									case SCREEN_LOGIN:
										NanoOnline.saveProfiles();
										NanoOnline.setCurrentCustomerById( customer.getId() );
										onBack();
									break;

									case SCREEN_SYNC_CUSTOMER_INFO:
										if ( customer.getId() == ProfileScreen.getProfileId() ) {
											NanoOnline.saveProfiles();
											NanoOnline.setScreen( SCREEN_PROFILE_EDIT );
										} else {
											// jogador tentou editar um perfil e efetuar login com outro
											NanoOnline.getProgressBar().showError( TEXT_LOGIN_ERROR_INVALID_NICKNAME );
										}
									break;

									default:
										NanoOnline.addProfile( customer );
										onBack();
										NanoOnline.getProgressBar().showConfirmation( TEXT_CUSTOMER_REGISTERED );
									break;
								}
							break;
						}
					break;
					
					case RC_SERVER_INTERNAL_ERROR:
						errorText = TEXT_ERROR_SERVER_INTERNAL;
					break;
					
					case RC_APP_NOT_FOUND:
						errorText = TEXT_ERROR_APP_NOT_FOUND;
					break;
					
					case RC_DEVICE_NOT_SUPPORTED:
						errorText = TEXT_ERROR_DEVICE_NOT_SUPPORTED;
					break;
					
					case RC_ERROR_NICKNAME_NOT_FOUND:
						errorEntryIndex = ENTRY_NICKNAME;
						errorText = TEXT_LOGIN_ERROR_NICKNAME_NOT_FOUND;
					break;
					
					case RC_ERROR_PASSWORD_INVALID:
						errorEntryIndex = ENTRY_PASSWORD;
						errorText = TEXT_LOGIN_ERROR_PASSWORD_INVALID;
					break;
				}
				
				if ( errorText >= 0 ) {
					if ( errorEntryIndex >= 0 ) {
						textBoxes[ errorEntryIndex ].getBorder().setState( Border.STATE_ERROR );
						NanoOnline.getForm().requestFocus( textBoxes[ errorEntryIndex ] );
					}
					
					NanoOnline.getProgressBar().showError( errorText );
				}

				//#if DEBUG == "true"
//# 					System.out.print( "data(" + data.length + "): " );
//# 					for ( int i = 0; i < data.length; ++i ) {
//# 						System.out.print( ( int ) data[ i ] );
//# 						System.out.print( ", " );
//# 					}
//# 					System.out.println();
//# 					System.out.println( new String( data ) );
				//#endif
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 				AppMIDlet.log( ex , "14");
				//#endif
			}
		}
		NanoOnline.getProgressBar().processData( id, data );
	}


	public final void onInfo( int id, int infoIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onInfo( id, infoIndex, extraData );
			switch ( infoIndex ) {
				case ConnectionListener.INFO_CONNECTION_ENDED:
					onConnectionEnded( false );
				break;
			}
		}
	}


	public final void onError( int id, int errorIndex, Object extraData ) {
		if ( id == currentConnection ) {
			NanoOnline.getProgressBar().onError( id, errorIndex, extraData );
			onConnectionEnded( false );
		}
	}
	
	
	/**
	 * Valida os campos de texto antes de enviar ao servidor.
	 * 
	 * @return <code>true</code>, caso estejam todos preenchidos da maneira correta, e <code>false</code> caso contrário.
	 */
	private final boolean validate( boolean validatePassword ) {
		byte firstError = -1;
		byte errorMessage = -1;
		
		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setText( textBoxes[ i ].getText().trim(), false );
		}
		
		String text = textBoxes[ ENTRY_NICKNAME ].getText();
		if ( text.length() < Customer.NICKNAME_MIN_CHARS ) {
			firstError = ENTRY_NICKNAME;
			errorMessage = TEXT_REGISTER_ERROR_NICKNAME_LENGTH;
			textBoxes[ ENTRY_NICKNAME ].getBorder().setState( Border.STATE_ERROR );
		} else {
			// garante que primeiro caracter seja válido (a-z, A-Z)
			if ( ImageFont.getCharType( text.charAt( 0 ) ) != ImageFont.CHAR_TYPE_REGULAR ) {
				firstError = ENTRY_NICKNAME;
				errorMessage = TEXT_REGISTER_ERROR_NICKNAME_FORMAT;
				textBoxes[ ENTRY_NICKNAME ].getBorder().setState( Border.STATE_ERROR );
			}
		}

		if ( validatePassword ) {
			text = textBoxes[ ENTRY_PASSWORD ].getText();
			if ( text.length() < Customer.PASSWORD_MIN_CHARS ) {
				// password muito curta
				if ( firstError < 0 ) {
					firstError = ENTRY_PASSWORD;
					errorMessage = TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT;
				}
				textBoxes[ ENTRY_PASSWORD ].getBorder().setState( Border.STATE_ERROR );
			}
		}

		if ( firstError >= 0 ) {
			textBoxes[ firstError ].getBorder().setState( Border.STATE_ERROR );
			NanoOnline.getProgressBar().showError( errorMessage );
			NanoOnline.getForm().requestFocus( textBoxes[ firstError ] );
		}
		
		// se o índice ainda for negativo, é porque não houve erro na validação
		return firstError < 0;
	}
	
	
	/**
	 * 
	 */
	private final void onConnectionEnded( boolean forceCancel ) {
		if ( forceCancel ) {
			NanoConnection.cancel( currentConnection );
		}
		currentConnection = -1;
		
		setInConnection( false );
	}
	
	
	public final void setFocus( boolean focus ) {
		super.setFocus( focus );
		
		if ( currentConnection < 0 && focus ) {
			onConnectionEnded( false );
		}
	}


	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();

		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				lastAction = sourceId;
				
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_BUTTON_BACK:
						checkBackButton();
					break;
					
					case ENTRY_BUTTON_OK:
						if ( validate( true ) ) {
							try {
								final ByteArrayOutputStream b = new ByteArrayOutputStream();
								final DataOutputStream out = new DataOutputStream( b );

								// escreve os dados globais antes de adicionar as informações específicas para registro do usuário
								NanoOnline.writeGlobalData( out );

								out.writeByte( ENTRY_NICKNAME );
								out.writeUTF( textBoxes[ ENTRY_NICKNAME ].getText() );

								out.writeByte( ENTRY_PASSWORD );
								out.writeUTF( textBoxes[ENTRY_PASSWORD].getText() );
								
								switch ( screenId ) {
									case SCREEN_SYNC_CUSTOMER_INFO:
										if ( profileIndex != Customer.ID_NONE ) {
											final Customer customer = NanoOnline.getCustomer( profileIndex );
											out.writeByte( ProfileScreen.PARAM_TIMESTAMP );
											out.writeLong( customer.getTimeStamp() );
										}
									break;
									
									default:
								}
								
								out.flush();
								currentConnection = NanoConnection.post( NANO_ONLINE_URL + "customers/import", b.toByteArray(), this, false );

								setInConnection( true );
							} catch ( Exception ex ) {
								//#if DEBUG == "true"
//# 								AppMIDlet.log( ex , "15");
								//#endif
							}
						}
					break;
					
					case ENTRY_BUTTON_FORGOT_PASSWORD:
						if ( validate( false ) ) {
							try {
								final ByteArrayOutputStream b = new ByteArrayOutputStream();
								final DataOutputStream out = new DataOutputStream( b );

								// escreve os dados globais antes de adicionar as informações específicas para registro do usuário
								NanoOnline.writeGlobalData( out );

								out.writeByte( ENTRY_NICKNAME );
								out.writeUTF( textBoxes[ ENTRY_NICKNAME ].getText() );

								out.flush();
								currentConnection = NanoConnection.post( NANO_ONLINE_URL + "customers/remember_password", b.toByteArray(), this, false );
								
								setInConnection( true );
							} catch ( Exception ex ) {
								//#if DEBUG == "true"
//# 									AppMIDlet.log( ex , "16" );
								//#endif
							}
						}
					break;
				}
			break;
			
			case Event.EVT_TEXTBOX_BACK:
				( ( TextBox ) evt.source ).setHandlesInput( false );
			break;
			
			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				switch ( sourceId ) {
					case ENTRY_NICKNAME:
					case ENTRY_PASSWORD:
						if ( evt.source.getBorder().getState() != Border.STATE_ERROR )
							evt.source.getBorder().setState( evt.eventType == Event.EVT_FOCUS_GAINED ? Border.STATE_FOCUSED : Border.STATE_UNFOCUSED );

						// se estiver no modo editável, altera o label da soft key direita ao dar foco a um dos textBox
						if ( buttonBack.getId() == ENTRY_BUTTON_BACK && currentConnection < 0 )
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CLEAR ) );
					break;
					
					default:
						if ( currentConnection < 0 )
							NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
						checkBackButton();
					break;
				} // fim switch ( key )
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )
	
	
	private final void setInConnection( boolean inConnection ) {
		requestFocus();

		for ( byte i = 0; i < EDIT_LABELS_TOTAL; ++i ) {
			textBoxes[ i ].setEnabled( !inConnection );
		}
		buttonOK.setEnabled( !inConnection );
		buttonBack.setEnabled( !inConnection );
		
		if ( inConnection ) {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_CANCEL ) );
		} else {
			NanoOnline.getProgressBar().setSoftKey( NanoOnline.getText( TEXT_BACK ) );
		}
	}
	
	
	private final void checkBackButton() {
		if ( currentConnection >= 0 )
			onConnectionEnded( true );
		else
			onBack();	
	}	
	

}
