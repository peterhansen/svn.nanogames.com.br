package br.com.nanogames.components.util;

/*
 * Point3f.java
 *
 * Created on 21 de Agosto de 2007, 22:27
 *
 */

/**
 *
 * @author peter
 */
public final class Point3f {
	
	/** Coordenada x do ponto em notação de ponto fixo. */
	public int x;
	/** Coordenada y do ponto em notação de ponto fixo. */
	public int y;
	/** Coordenada z do ponto em notação de ponto fixo. */
	public int z;
	
	
	public Point3f() {
	}
	
	
	public Point3f( Point3f xyz ) {
		this( xyz.x, xyz.y, xyz.z );
	}
	
	
	public Point3f( int fp_x ) {
		this( fp_x, 0, 0 );
	}

	
	public Point3f( int fp_x, int fp_y ) {
		this( fp_x, fp_y, 0 );
	}

	
	public Point3f( int fp_x, int fp_y, int fp_z ) {
		set( fp_x, fp_y, fp_z );
	}
	
	
	public final void set() {
		set( 0, 0, 0 );
	}
	
	
	public final void set( int fp_x ) {
		set( fp_x, 0, 0 );
	}
	
	
	public final void set( int fp_x, int fp_y ) {
		set( fp_x, fp_y, 0 );
	}	

	
	public final void set( int fp_x, int fp_y, int fp_z ) {
		this.x = fp_x;
		this.y = fp_y;
		this.z = fp_z;
	}
	
	
	public final void set( Point3f p ) {
		set( p.x, p.y, p.z );
	}

	
	public final Point3f sub( Point3f p ) {
		return sub( p.x, p.y, p.z );
	}


	public final Point3f sub( int fp_x, int fp_y, int fp_z ) {
		return new Point3f( x - fp_x, y - fp_y, z - fp_z );
	}
	
	
	public final Point3f add( Point3f p ) {
		return add( p.x, p.y, p.z );
	}


	public final Point3f add( int fp_x, int fp_y, int fp_z ) {
		return new Point3f( x + fp_x, y + fp_y, z + fp_z );
	}

	
	public Point3f div( int fp_d ) {
		//#if DEBUG == "true"
//# 			if ( fp_d == 0 )
//# 				throw new IllegalArgumentException( "Division by zero." );
		//#endif

		return new Point3f( NanoMath.divFixed( x, fp_d ), NanoMath.divFixed( y, fp_d ), NanoMath.divFixed( z, fp_d ) );
	} // fim do operador div

	
	public final Point3f mul( int fp_m ) {
		return new Point3f( NanoMath.mulFixed( x, fp_m ), NanoMath.mulFixed( y, fp_m ), NanoMath.mulFixed( z, fp_m ) );
	}

	 
	 public final void subEquals( Point3f p ) {
		 x -= p.x; 
		 y -= p.y; 
		 z -= p.z; 
	 }
	 
	 
	 public final void addEquals( Point3f p ) {
		 x += p.x; 
		 y += p.y; 
		 z += p.z;
	 }

	 
	 public final void divEquals( int fp_d ) {
		//#if DEBUG == "true"
//# 			if ( fp_d == 0 )
//# 				throw new IllegalArgumentException( "Division by zero." );
		//#endif
			
		set( NanoMath.divFixed( x, fp_d ), NanoMath.divFixed( y, fp_d ), NanoMath.divFixed( z, fp_d ) );
	} // fim do operador /=

	 
	 public final void mulEquals( int fp_m ) {
		 x = NanoMath.mulFixed( x, fp_m );
		 y = NanoMath.mulFixed( y, fp_m );
		 z = NanoMath.mulFixed( z, fp_m );
	 }

	 
	// calcula o módulo do vetor
	public final int getModule() {
		return NanoMath.sqrtFixed( NanoMath.mulFixed( x, x ) + NanoMath.mulFixed( y, y ) + NanoMath.mulFixed( z, z ) ); 
	}

	
	// retorna o vetor normalizado
	public final Point3f getNormal() {
		return div( getModule() );
	}

	
	// retorna o produto vetorial por p
	public final Point3f cross( Point3f p ) {
		return new Point3f( NanoMath.mulFixed( y, p.z ) - NanoMath.mulFixed( z, p.y ), NanoMath.mulFixed( z, p.x ) - NanoMath.mulFixed( x, p.z ), NanoMath.mulFixed( x, p.y ) - NanoMath.mulFixed( y, p.x ) );
	}

	
	// retorna o produto interno por p
	public final int dot( Point3f p ) {
		return ( NanoMath.mulFixed( x, p.x ) + NanoMath.mulFixed( y, p.y ) + NanoMath.mulFixed( z, p.z ) ); 
	}


	// normaliza o vetor
	public final void normalize() { 
		set( getNormal() ); 
	}


	//#if DEBUG == "true"
//# 		public final String toString() {
//# 			return "(" + NanoMath.toString( x ) + ", " + NanoMath.toString( y ) + ", " + NanoMath.toString( z ) + ")";
//# 		}
	//#endif
	
}
