/*
 * NanoMath.java
 *
 * Created on October 26, 2007, 11:48 AM
 *
 */

package br.com.nanogames.components.util;

import java.util.Random;

/**
 * Classe utilitária com funções matemáticas para inteiros em notação padrão e em ponto fixo.
 * 
 * <h3>Utilização da notação de ponto fixo</h3>
 * 
 * <p>A notação de ponto fixo desta classe utiliza os primeiros 16 bits de um inteiro para a parte inteira de um valor, e os
 * 16 bits à direita para representar a parte fracionária. Pode-se converter um inteiro em notação padrão para ponto fixo
 * ou vice-versa utilizando-se os métodos <code>toFixed(int)</code> e <code>toInt(int)</code>, respectivamente.</p>
 * 
 * <p>Deve-se ter atenção especial à notação utilizada nos parâmetros passados em cada método, uma vez que de maneira geral
 * todos recebem inteiros. Métodos que recebem um inteiro em notação padrão utilizam o sufixo <code>int</code> (como em
 * <code>divInt</code>), e os que recebem inteiros em notação de ponto fixo utilizam o sufixo <code>Fixed</code> (como em
 * <code>divFixed</code>).</p>
 * 
 * <h4>Operações em ponto fixo</h4>
 * 
 * <p>As seguintes operações <b>devem</b> ser realizadas somente através dos métodos específicos da classe:
 * <ul>
 * <li>multiplicação: <code>mulFixed(int)</code></li>
 * <li>divisão: <code>divFixed(int)</code></li>
 * </ul>
 * </p>
 * 
 * <p>As seguintes operações podem ser realizadas através dos operadores padrão:
 * <ul>
 * <li>adição: <code>+</code></li>
 * <li>subtração (binário) e negativo (unário): <code>-</code></li>
 * <li>shift de bits: <code>&lt&lt, &gt&gt e &gt&gt&gt</code></li>
 * <li>comparação: <code>==, !=, &gt, &lt, &gt= e &lt=</code></li>
 * <li>atribuição: <code>=, +=, -=, &lt&lt=, &gt&gt=, &gt&gt&gt=</code></li>
 * </ul>
 * </p>
 * 
 * @author peter
 */
public final class NanoMath {
	

	/**
	 *	Number of bits used for 'fraction'.
	 */
	public static final byte FIXED_POINT = 16;
	/**
	 *	Decimal one as represented by the Fixed class.
	 */
	public static final int ONE = 1 << FIXED_POINT;
	
	/** Valor inteiro máximo que pode ser representado por um ponto fixo. */
	public static final int MAX_INTEGER_VALUE = 1 << FIXED_POINT;
	
	/**
	 *	Half in fixed point.
	 */
	public static final int HALF = ONE >> 1;
	/**
	 * Número de bits utilizados para fazer a divisão de 1/4 de círculo.
	 */
	public static final byte QUARTER_CIRCLE_BITS = 6;
	/**
	 * Número de bits utilizados para fazer a divisão do círculo completo.
	 */
	public static final byte FULL_CIRCLE_BITS = QUARTER_CIRCLE_BITS + 2;
	/**
	 *	Quarter circle resolution for trig functions (should be a power of
	 *	two). This is the number of discrete steps in 90 degrees.
	 */
	public static final byte QUARTER_CIRCLE = ( byte ) ( 1 << QUARTER_CIRCLE_BITS );
	/**
	 *	Mask used to limit angles to one revolution. If a quarter circle is 64
	 * (i.e. 90 degrees is broken into 64 steps) then the mask is 255.
	 */
	public static final int FULL_CIRCLE_MASK = ( QUARTER_CIRCLE << 2 ) - 1;
	/**
	 *	The trig table is generated at a higher precision than the typical
	 *	16.16 format used for the rest of the fixed point maths. The table
	 *	values are then shifted to match the actual fixed point used.
	 */
	private static final byte TABLE_SHIFT = 30;
	/**
	 *	Equivalent to: sin((2 * PI) / (QUARTER_CIRCLE * 4))
	 *	<p>
	 *	Note: if either QUARTER_CIRCLE or TABLE_SHIFT is changed this value
	 *	will need recalculating (put the above formular into a calculator set
	 *	radians, then shift the result by <code>TABLE_SHIFT</code>).
	 */
	private static final int SIN_PRECALC = 26350943;
	/**
	 *	Equivalent to: cos((2 * PI) / (QUARTER_CIRCLE * 4)) * 2
	 *
	 *	Note: if either QUARTER_CIRCLE or TABLE_SHIFT is changed this value
	 *	will need recalculating ((put the above formular into a calculator set
	 *	radians, then shift the result by <code>TABLE_SHIFT</code>).
	 */
	private static final int COS_PRECALC = 2146836866;
	/**
	 *	One quarter sine wave as fixed point values.
	 */
	private static final int[] SINE_TABLE = new int[ QUARTER_CIRCLE + 1 ];
	/**
	 *	Scale value for indexing ATAN_TABLE[].
	 */
	private static final int ATAN_SHIFT;
	/**
	 *	Reverse atan lookup table.
	 */
	private static final byte[] ATAN_TABLE;
	/**
	 *	ATAN_TABLE.length
	 */
	private static final int ATAN_TABLE_LEN;

	/*
	 *	Generates the tables and fills in any remaining static ints.
	 */
	
	
	// <editor-fold desc="VARIÁVEIS UTILIZADAS NO GERADOR DE NÚMEROS ALEATÓRIOS">
	
	/** Número de chamadas de nextInt() para que o seed seja atualizado (deve-se utilizar números potências de 2). */
	private static final byte SEED_CHANGE_FREQUENCY = 8;
	
	private static final byte SEED_CHANGE_FREQUENCY_MODULE = SEED_CHANGE_FREQUENCY - 1;
	
	private static int calls;
    
    /** Número primo utilizado para incrementar o seed a cada nova instância. */
    private static final int SEED_CHANGE = 999983;
    
    /** Valor utilizado para reduzir a probabilidade de objetos Random alocados consecutivamente utilizarem o mesmo seed 
	 * e assim gerarem muitos padrões de repetições. */
    private static long SEED = System.currentTimeMillis();
	
	private static Random random;
    
	// </editor-fold>	
	

	static {
		// Generate the sine table using recursive synthesis.
		SINE_TABLE[0] = 0;
		SINE_TABLE[1] = SIN_PRECALC;
		for ( int n = 2; n < QUARTER_CIRCLE + 1; ++n ) {
			SINE_TABLE[n] = ( int ) ( ( ( long ) SINE_TABLE[n - 1] * COS_PRECALC ) >> TABLE_SHIFT ) - SINE_TABLE[n - 2];
		}
		// Scale the values to the fixed point format used.
		for ( int n = 0; n < QUARTER_CIRCLE + 1; ++n ) {
			SINE_TABLE[n] = SINE_TABLE[n] + ( 1 << ( TABLE_SHIFT - FIXED_POINT - 1 ) ) >> TABLE_SHIFT - FIXED_POINT;
		}

		// Calculate a shift used to scale atan lookups
		int rotl = 0;
		int tan0 = tanInt( 0 );
		int tan1 = tanInt( 1 );
		while ( rotl < 32 ) {
			if ( ( tan1 >>= 1 ) > ( tan0 >>= 1 ) ) {
				rotl++;
			} else {
				break;
			}
		}
		ATAN_SHIFT = rotl;
		ATAN_TABLE = null; // TODO corrigir geração da tabela de arco tangente
		ATAN_TABLE_LEN = 0;
//		// Create the a table of tan values
//		int[] lut = new int[ QUARTER_CIRCLE ];
//		for ( int n = 0; n < QUARTER_CIRCLE; ++n ) {
//			lut[n] = tanInt( bitsToRadian( n ) ) >> rotl;
//		}
//		ATAN_TABLE_LEN = lut[QUARTER_CIRCLE - 1];
//		// Then from the tan values create a reverse lookup
//		ATAN_TABLE = new byte[ ATAN_TABLE_LEN ];
//		for ( byte n = 0; n < QUARTER_CIRCLE - 1; ++n ) {
//			int min = lut[n];
//			int max = lut[n + 1];
//			for ( int i = min; i < max; ++i ) {
//				ATAN_TABLE[i] = n;
//			}
//		}
	}
	
	
	/**
	 * Converte um dado ângulo em radianos para a divisão interna do círculo trigonométrico.
	 * 
	 * @param angle ângulo em radianos na notação padrão (0-360).
	 * @return ângulo da notação interna relativo a "angle" graus radianos.
	 */
	private static final int radianToBits( int angle ) {
		return ( angle << FULL_CIRCLE_BITS ) / 360;
	}
	
	
	/**
	 * Converte um dado ângulo na divisão interna do círculo trigonométrico em radianos.
	 * 
	 * @param angle ângulo utilizando a divisão interna do círculo trigonométrico.
	 * @return ângulo da notação interna relativo a "angle" graus internos.
	 */	
	private static final int bitsToRadian( int angle ) {
		return ( angle * 360 ) >> FULL_CIRCLE_BITS;
	}
	
	
	/** o construtor é privado pois a classe não pode ser instanciada (todos os seus métodos são estáticos) */
	private NanoMath() {
	}	
	
	
	/**
	 *	How many decimal places to use when converting a fixed point value to
	 *	a decimal string.
	 *
	 *	@see #toString
	 */
	private static final byte STRING_MAX_DECIMAL_PLACES = 4;
	/**
	 *	Value to add in order to round down a fixed point number when
	 *	converting to a string.
	 */
	private static final int STRING_DECIMAL_PLACES_ROUND;


	static {
		int i = 10;
		for ( int n = 1; n < STRING_MAX_DECIMAL_PLACES; ++n ) {
			i *= i;
		}
		if ( STRING_MAX_DECIMAL_PLACES == 0 ) {
			STRING_DECIMAL_PLACES_ROUND = ( ONE >> 1 );
		} else {
			STRING_DECIMAL_PLACES_ROUND = ONE / ( 2 * i );
		}
	}


	/**
	 * Returns an integer as a fixed point value.
	 */
	public static final int toFixed( int i  ) {
		return i << FIXED_POINT;
	}
	
	
	/**
	 * Retorna o valor inteiro de um valor de ponto fixo.
	 * 
	 * @param f ponto fixo cujo valor inteiro será obtido.
	 * @return valor inteiro de um ponto fixo.
	 */
	public static final int toInt( int f ) {
		return f >> FIXED_POINT;
	}
	
	
	/**
	 * Retorna uma string que representa o valor decimal de um valor em notação de ponto fixo.
	 * 
	 * @param f valor em notação de ponto fixo.
	 * @return string representando o valor, em notação decimal.
	 */
	public static final String toString( int f ) {
		return toString( f, 2 );
	}


	/**
	 *	Converts a fixed point value into a decimal string.
	 */
	public static final String toString( int f, int decimalPlaces ) {
		final StringBuffer sb = new StringBuffer( 16 );
		
		// se o número for negativo, adiciona o indicador de sinal e o inverte, para que os cálculos futuros não tenham que
		// ser alterados (sem esse teste, -0.1 retornava "-1.9", -1.8 retornava "-2.2", etc.
		if ( f < 0 ) {
			sb.append( '-' );
			f = -f;
		}
		sb.append( ( f += STRING_DECIMAL_PLACES_ROUND ) >> FIXED_POINT );
		sb.append( '.' );
		f &= ONE - 1;
		
		if ( decimalPlaces > STRING_MAX_DECIMAL_PLACES )
			decimalPlaces = STRING_MAX_DECIMAL_PLACES;
		
		for ( int i = 0; i < decimalPlaces; ++i ) {
			f *= 10;
			sb.append( ( f / ONE ) % 10 );
		}
		
		return sb.toString();
	}


	public static final void printByteArray( final byte[] data ) {
		if ( data != null ) {
			for ( int i = 0; i < data.length; ++i ) {
				final String s = Integer.toHexString( data[ i ] & 0xff );
				if ( s.length() <= 1 )
					System.out.print( '0' );
				System.out.print( s );

				switch ( i % 40 ) {
					case  3: case  7: case 15: case 19:
					case 23: case 27: case 31: case 35:
						System.out.print( ' ' );
					break;

					case 39:
						System.out.println();
					break;
				}
			}
			System.out.println();
		}
	}


	/**
	 *	Multiplies two fixed point values and returns the result.
	 */
	public static final int mulFixed( int fp_a, int fp_b ) {
		return ( int ) ( ( long ) fp_a * ( long ) fp_b >> FIXED_POINT );
	}


	/**
	 * Multiplica dois inteiros em notação padrão e retorna o resultado na notação de ponto fixo.
	 */
	public static final int mulInt( int int_a, int int_b  ) {
		return mulFixed( toFixed( int_a ), toFixed( int_b ) );
	}


	/**
	 *	Divides two fixed point values and returns the result.
	 */
	public static final int divFixed( int fp_a, int fp_b ) {
		return ( int ) ( ( ( long ) fp_a << ( FIXED_POINT << 1 ) ) / ( long ) fp_b >> FIXED_POINT );
	}


	/**
	 * Divide dois valores inteiros em notação padrão e retorna o resultado na notação de ponto fixo.
	 */
	public static final int divInt( int a, int b ) {
		return divFixed( toFixed( a ), toFixed( b ) );
	}
	

	/**
	 *	Seno de um ângulo.
	 *	
	 * @param f inteiro na representação de ponto fixo.
	 * @return o seno de n, na representação de ponto fixo.
	 */	
	public static final int sinFixed( int f ) {
		return sinInt( toInt( f ) );
	}


	/**
	 *	Seno de um ângulo.
	 *	
	 * @param n inteiro na representação normal.
	 * @return o seno de n, na representação de ponto fixo.
	 */
	public static final int sinInt( int n ) {
		n = radianToBits( n ) & FULL_CIRCLE_MASK;
		
		if ( n < ( QUARTER_CIRCLE << 1 ) ) {
			if ( n < QUARTER_CIRCLE ) {
				return SINE_TABLE[ n ];
			} else {
				return SINE_TABLE[ ( QUARTER_CIRCLE << 1 ) - n];
			}
		} else {
			if ( n < QUARTER_CIRCLE * 3 ) {
				return -SINE_TABLE[ n - ( QUARTER_CIRCLE << 1 ) ];
			} else {
				return -SINE_TABLE[ ( QUARTER_CIRCLE << 2 ) - n ];
			}
		}
	}
	
	
	/**
	 *	Cosseno de um ângulo.
	 *	
	 * @param n inteiro na representação de ponto fixo.
	 * @return o cosseno de n, na representação de ponto fixo.
	 */
	public static final int cosFixed( int f ) {
		return cosInt( toInt( f ) );
	}


	/**
	 *	Seno de um ângulo.
	 *	
	 * @param n inteiro na representação normal.
	 * @return o seno de n, na representação de ponto fixo.
	 */
	public static final int cosInt( int n  ) {
		n = radianToBits( n ) & FULL_CIRCLE_MASK;
		
		if ( n < ( QUARTER_CIRCLE << 1 ) ) {
			if ( n < QUARTER_CIRCLE ) {
				return SINE_TABLE[QUARTER_CIRCLE - n];
			} else {
				return -SINE_TABLE[n - QUARTER_CIRCLE];
			}
		} else {
			if ( n < QUARTER_CIRCLE * 3 ) {
				return -SINE_TABLE[ QUARTER_CIRCLE * 3 - n ];
			} else {
				return SINE_TABLE[ n - QUARTER_CIRCLE * 3 ];
			}
		}
	}


	/**
	 *	Tangente de um ângulo.
	 *	
	 * @param n inteiro na representação normal.
	 * @return a tangente de n na representação de ponto fixo.
	 * @see #QUARTER_CIRCLE
	 */
	public static final int tanInt( int n ) {
		return divFixed( sinInt( n ), cosInt( n ) );
	}


	/**
	 *	Returns the arc tangent of an angle. TODO conferir valor do ângulo recebido
	 * 
	 * @param f inteiro na representação de ponto fixo.
	 */
	public static final int atan( int f ) {
		f = f + ( 1 << ( ATAN_SHIFT - 1 ) ) >> ATAN_SHIFT;
		if ( f < 0 ) {
			if ( f <= -ATAN_TABLE_LEN ) {
				return -( QUARTER_CIRCLE - 1 );
			}
			return -ATAN_TABLE[-f];
		} else {
			if ( f >= ATAN_TABLE_LEN ) {
				return QUARTER_CIRCLE - 1;
			}
			return ATAN_TABLE[f];
		}
	}


	/**
	 *	Returns the polar angle of a rectangular coordinate.
	 */
	public static final int atan( int x, int y ) {
		int n = atan( divFixed( x, abs( y ) + 1 ) ); // kludge to prevent ArithmeticException
		if ( y > 0 ) {
			return n;
		}
		if ( y < 0 ) {
			if ( x < 0 ) {
				return -( QUARTER_CIRCLE  << 1 ) - n;
			}
			if ( x > 0 ) {
				return ( QUARTER_CIRCLE << 1 ) - n;
			}
			return ( QUARTER_CIRCLE << 1 );
		}
		if ( x > 0 ) {
			return QUARTER_CIRCLE;
		}
		return -QUARTER_CIRCLE;
	}


	/**
	 *	Returns the angle of a rectangular coordinate.
	 */
	public static final int atan2( int x, int y ) {
		if( x > 0 ) {
			return atan( divFixed( y, x ) );
		} else {
			if( x < 0 ) {
				if( y >= 0 )
					return 180 + atan( divFixed( y, x ) );
				else
					return -180 + atan( divFixed( y, x ) );
			} else {
				if( y > 0 )
					return 90;
				else if( y < 0 )
					return -90;
				else
					return 0; // UNDEFINED but for not call exception return anything
			}
		}
	}
	
	
	/**
	 * 
	 * @param p1
	 * @param p2
	 * @return
	 * @see hyp(int, int, int, int)
	 */
	public static final int hyp( Point p1, Point p2 ) {
		return hyp( p1.x, p1.y, p2.x, p2.y );
	}


	/**
	 *	Rough calculation of the hypotenuse. Whilst not accurate it is very fast.
	 *	<p>
	 *	Derived from a piece in Graphics Gems.
	 */
	public static final int hyp( int x1, int y1, int x2, int y2 ) {
		if ( ( x2 -= x1 ) < 0 ) {
			x2 = -x2;
		}
		if ( ( y2 -= y1 ) < 0 ) {
			y2 = -y2;
		}
		return x2 + y2 - ( ( ( x2 > y2 ) ? y2 : x2 ) >> 1 );
	}


	/**
	 *	Fixed point square root.
	 *	<p>
	 *	Derived from a 1993 Usenet algorithm posted by Christophe Meessen.
	 */
	public static final int sqrtFixed( int f  ) {
		if ( f <= 0 ) {
			return 0;
		}
		long sum = 0;
		int bit = 0x40000000;
		while ( bit >= 0x100 ) { // lower values give more accurate results
			final long tmp = sum | bit;
			if ( f >= tmp ) {
				f -= tmp;
				sum = tmp + bit;
			}
			bit >>= 1;
			f <<= 1;
		}
		return ( int ) ( sum >> 16 - ( FIXED_POINT >> 1 ) );
	}


	/**
	 *	Returns the absolute value.
	 */
	public static final int abs( int n ) {
		return ( n < 0 ) ? -n : n;
	}


	/**
	 *	Returns the sign of a value, -1 for negative numbers, otherwise 1.
	 */
	public static final int sgn( int n ) {
		return ( n < 0 ) ? -1 : 1;
	}


	/**
	 *	Returns the minimum of two values.
	 */
	public static final int min( int a, int b ) {
		return ( a < b ) ? a : b;
	}


	/**
	 *	Returns the maximum of two values.
	 */
	public static final int max( int a, int b ) {
		return ( a > b ) ? a : b;
	}


	/**
	 *	Clamps the value n between min (inclusive) and max (inclusive).
	 */
	public static final int clamp( int n, int min, int max ) {
		return n <= min ? min : n >= max ? max : n;
	}


	/**
	 *	Wraps the value n between 0 and the required limit.
	 */
	public static final int wrap( int f, int limit ) {
		return ( ( f %= limit ) < 0 ) ? limit + f : f;
	}


	/**
	 *	Returns the nearest int to a fixed point value. Equivalent to <code>
	 *	Math.round()</code> in the standard library.
	 */
	public static final int round( int n ) {
		return n + HALF >> FIXED_POINT;
	}


	/**
	 *	Returns the nearest int rounded down from a fixed point value.
	 *	Equivalent to <code>Math.floor()</code> in the standard library.
	 */
	public static final int floor( int n ) {
		return n >> FIXED_POINT;
	}
	

	/**
	 *	Returns the nearest int rounded up from a fixed point value.
	 *	Equivalent to <code>Math.ceil()</code> in the standard library.
	 */
	public static final int ceil( int n ) {
		return n + ( ONE - 1 ) >> FIXED_POINT;
	}
	
	
	/**
	 *	Retorna um valor interpolado linearmente entre v1 e v2, tal que o valor de retorno seja v1 quando
	 *  percentage é 0 e v2 quando percentage é 100
	 */
	public static final int lerpInt( int v1, int v2, int percentage ) {
		percentage = clamp( percentage, 0, 100 );
		return v1 + ( ( percentage * ( v2 - v1 ) ) / 100 );
	}
	
	
	/**
	 *	Retorna um valor ponto fixo interpolado linearmente entre v1 e v2, tal que o valor de retorno seja v1
	 * quando percentage é 0 e v2 quando percentage é 100
	 */
	public static final int lerpFixed( int f1, int f2, int percentage ) {
		// 6553600 == 100 em ponto fixo
		percentage = clamp( percentage, 0, 6553600 );
		return f1 + divFixed( mulFixed( percentage, f2 - f1 ), 6553600 );
	}


	/**
	 * Obtém o próximo inteiro múltiplo de m.
	 * @param n
	 * @param m
	 * @return
	 */
    public static final int nextMulOf( int n, int m ) {
        final int aux = n % m;
        if( aux == 0 )
            return n;
        return n + ( m - aux );
    }


	/**
	 * Obtém o próximo inteiro múltiplo de m. Otimizado para potências de 2.
	 * @param n
	 * @param m
	 * @return
	 */
	public static final int nextMulOfPot2( int n, int m ) {
		--m;
		return ( n + m ) & ~m;
	}


	/**
	 * Converte um número long em um array de bytes
	 * @param l Número a ser convertido
	 * @return Número recebido como parâmetro convertido em um array de bytes
	 */
	public static final byte[] longToByteArray( long l ) {
		final byte[] bytes = new byte[ 8 ];

		bytes[0] = ( byte )( l >> 56 );
		bytes[1] = ( byte )( ( l & 0x00FF000000000000L ) >> 48 );
		bytes[2] = ( byte )( ( l & 0x0000FF0000000000L ) >> 40 );
		bytes[3] = ( byte )( ( l & 0x000000FF00000000L ) >> 32 );
		bytes[4] = ( byte )( ( l & 0x00000000FF000000L ) >> 24 );
		bytes[5] = ( byte )( ( l & 0x0000000000FF0000L ) >> 16 );
		bytes[6] = ( byte )( ( l & 0x000000000000FF00L ) >>  8 );
		bytes[7] = ( byte )(   l & 0x00000000000000FFL );

		return bytes;
	}


	/**
	 * Converte um número inteiro em um array de bytes.
	 * @param i Número a ser convertido
	 * @return Número recebido como parâmetro convertido em um array de bytes.
	 */
	public static final byte[] intToByteArray( int i ) {
		final byte[] bytes = new byte[ 4 ];

		bytes[ 0 ] = ( byte )( i >> 24 );
		bytes[ 1 ] = ( byte )( ( i & 0x00FF0000 ) >> 16 );
		bytes[ 2 ] = ( byte )( ( i & 0x0000FF00 ) >>  8 );
		bytes[ 3 ] = ( byte )(   i & 0x000000FF );

		return bytes;
	}


	/**
	 * Converte um array de bytes em um array de números inteiros.
	 *
	 * @param bytes Array de bytes que será convertido
	 * @param offset Índice do array bytes a partir do qual a conversão deve começar
	 * @param lenght Quantos bytes serão convertidos. Deve ser múltiplo de 4
	 * @return Um array de inteiros contendo a conversão dos bytes recebidos como parâmetro
	 * @throws IllegalArgumentException Caso length não seja múltiplo de 4
	 */
	public static final int[] byteArrayToIntArray( byte[] bytes, int offset, int length ) throws IllegalArgumentException
	{
            // Lembrando que "& 2" == "% 4"
            if( ( length & 2 ) != 0 )
            {
                    //#if DEBUG == "true"
//# 				throw new IllegalArgumentException( "O par�metro length deve ser m�ltiplo de 4" );
                    //#else
                    throw new IllegalArgumentException();
                    //#endif
            }

            int[] ints = new int[length >> 2];

            int intcount = 0;
            for( int bytecount = offset ; bytecount < offset + length ; bytecount += 4 )
            {
                    ints[intcount++] =
                            ( ( ( ( ( int ) ( bytes[bytecount    ] ) ) & 0xFF ) << 24 ) ) |
                            ( ( ( ( ( int ) ( bytes[bytecount + 1] ) ) & 0xFF ) << 16 ) ) |
                            ( ( ( ( ( int ) ( bytes[bytecount + 2] ) ) & 0xFF ) <<  8 ) ) |
                            ( ( ( ( ( int ) ( bytes[bytecount + 3] ) ) & 0xFF ) ) );
            }
            return ints;
	}


	/**
	 * Converte um array de inteiros num array de bytes. Equivalente à chamada de <code>intArrayToByteArray( array, 0, array.length )</code>.
	 * @param array array de inteiros a ser convertido.
	 * @return array de bytes correspondente ao int[] recebido.
	 */
	public static final byte[] intArrayToByteArray( int[] array ) {
		return intArrayToByteArray( array, 0, array.length );
	}


	/**
	 * Converte um array de inteiros num array de bytes.
	 * 
	 * @param array array de inteiros a ser convertido.
	 * @param offset índice do primeiro inteiro a ser convertido.
	 * @param length total de inteiros a serem convertidos.
	 * @return array de bytes correspondente ao int[] recebido.
	 */
	public static final byte[] intArrayToByteArray( int[] array, int offset, int length ) {
		final byte[] ret = new byte[ length << 2 ];

		final int LIMIT = offset + length;
		for( int intCount = offset, byteCount = 0; intCount < LIMIT; ++intCount ) {
			final int i = array[ intCount ];

			ret[ byteCount++ ] = ( byte ) ( i >> 24 );
			ret[ byteCount++ ] = ( byte ) ( ( i & 0x00ff0000 ) >> 16 );
			ret[ byteCount++ ] = ( byte ) ( ( i & 0x0000ff00 )  >> 8 );
			ret[ byteCount++ ] = ( byte ) ( ( i & 0x000000ff ) );
		}

		return ret;
	}

	
	/**
	 *	Returns a fixed point value greater than or equal to decimal 0.0 and
	 *	less than 1.0 (in 16.16 format this would be 0 to 65535 inclusive).
	 */
	public static final int randFixed() {
		loadRandom();
		
		calls = ( calls + 1 ) & SEED_CHANGE_FREQUENCY_MODULE;
		if ( calls == 0 )
			randomSeed();		
		
		return random.nextInt() >>> ( 32 - FIXED_POINT );
	}


	/**
	 *	Returns a random number between 0 and <code>n</code> (exclusive).
	 */
	public static final int randFixed( int f ) {
		return mulFixed( randFixed(), f );
	}
	
	
	/**
	 * Retorna um número aleatório inteiro, na notação padrão.
	 */
	public static final int randInt() {
		loadRandom();
		
		return random.nextInt();
	}
	
	
	/**
	 * Retorna um número aleatório inteiro entre 0 e n (exclusivo), na notação padrão.
	 */
	public static final int randInt( int n ) {
		loadRandom();
		
		calls = ( calls + 1 ) & SEED_CHANGE_FREQUENCY_MODULE;
		if ( calls == 0 )
			randomSeed();
		
		return Math.abs( random.nextInt() % n );
	}
	
	
	/**
	 * Define um novo seed para o gerador aleatório.
	 */
	public static final void randomSeed() {
		loadRandom();
		
		random.setSeed( SEED += SEED_CHANGE );
	}	
	
	
	private static final void loadRandom() {
		if ( random == null )
			random = new Random();
	}
	
	
	
	// <editor-fold defaulstate="collapsed" desc="CALCULO OTIMIZADO DA RAIZ QUADRADA INTEIRA">
	/** tabela utilizada para cálculos da raiz quadrada */
	private static final short[] squareRootTable = {
	      0,  16,  22,  27,  32,  35,  39,  42,  45,  48,  50,  53,  55,  57,
	     59,  61,  64,  65,  67,  69,  71,  73,  75,  76,  78,  80,  81,  83,
	     84,  86,  87,  89,  90,  91,  93,  94,  96,  97,  98,  99, 101, 102,
	    103, 104, 106, 107, 108, 109, 110, 112, 113, 114, 115, 116, 117, 118,
	    119, 120, 121, 122, 123, 124, 125, 126, 128, 128, 129, 130, 131, 132,
	    133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 144, 145,
	    146, 147, 148, 149, 150, 150, 151, 152, 153, 154, 155, 155, 156, 157,
	    158, 159, 160, 160, 161, 162, 163, 163, 164, 165, 166, 167, 167, 168,
	    169, 170, 170, 171, 172, 173, 173, 174, 175, 176, 176, 177, 178, 178,
	    179, 180, 181, 181, 182, 183, 183, 184, 185, 185, 186, 187, 187, 188,
	    189, 189, 190, 191, 192, 192, 193, 193, 194, 195, 195, 196, 197, 197,
	    198, 199, 199, 200, 201, 201, 202, 203, 203, 204, 204, 205, 206, 206,
	    207, 208, 208, 209, 209, 210, 211, 211, 212, 212, 213, 214, 214, 215,
	    215, 216, 217, 217, 218, 218, 219, 219, 220, 221, 221, 222, 222, 223,
	    224, 224, 225, 225, 226, 226, 227, 227, 228, 229, 229, 230, 230, 231,
	    231, 232, 232, 233, 234, 234, 235, 235, 236, 236, 237, 237, 238, 238,
	    239, 240, 240, 241, 241, 242, 242, 243, 243, 244, 244, 245, 245, 246,
	    246, 247, 247, 248, 248, 249, 249, 250, 250, 251, 251, 252, 252, 253,
	    253, 254, 254, 255
	};
	
	
	/**
	 * Calcula a raiz quadrada <strong>inteira</strong> de um valor.
	 * 
	 * @param n valor cuja raiz quadrada será calculada, em sua representação normal (não utilizando ponto fixo).
	 * @return a raiz quadrada inteira de x (valor truncado), em notação padrão.
	 */
	public static final int sqrtInt( int n ) {
		int xn;
		
		if (n >= 0x10000)
			if (n >= 0x1000000)
				if (n >= 0x10000000)
					if (n >= 0x40000000) {
						if (n >= Integer.MAX_VALUE)
							return Integer.MAX_VALUE;
						xn = squareRootTable[n>>24] << 8;
					} else
						xn = squareRootTable[n>>22] << 7;
				else
					if (n >= 0x4000000)
						xn = squareRootTable[n>>20] << 6;
					else
						xn = squareRootTable[n>>18] << 5;
			else {
				if (n >= 0x100000)
					if (n >= 0x400000)
						xn = squareRootTable[n>>16] << 4;
					else
						xn = squareRootTable[n>>14] << 3;
				else
					if (n >= 0x40000)
						xn = squareRootTable[n>>12] << 2;
					else
						xn = squareRootTable[n>>10] << 1;

				xn = (xn + 1 + n / xn) >> 1;

				if (xn * xn > n) /* Correct rounding if necessary */
					--xn;

				return xn;
			}
		else
			if (n >= 0x100) {
				if (n >= 0x1000)
					if (n >= 0x4000)
						xn = (squareRootTable[n>>8] >> 0) + 1;
					else
						xn = (squareRootTable[n>>6] >> 1) + 1;
				else
					if (n >= 0x400)
						xn = (squareRootTable[n>>4] >> 2) + 1;
					else
						xn = (squareRootTable[n>>2] >> 3) + 1;

				if (xn * xn > n) /* Correct rounding if necessary */
					--xn;

				return xn;
			} else
				return squareRootTable[n] >> 4;

		if (xn * xn > n) /* Correct rounding if necessary */
		    --xn;
	
		return xn;
	} // fim do método sqrt( int )	
	
	// </editor-fold>
	
	
}
