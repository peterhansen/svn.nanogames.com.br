package br.com.nanogames.components.gestures;

//package br.com.nanogames.components.gestures;
//
//import java.util.Vector;
//
//
//public class HMM {
//
//	private int numStates;
//	private int sigmaSize;
//	public double pi[];
//	public double a[][];
//	public double b[][];
//
//
//	public HMM( int numStates, int sigmaSize ) {
//		this.numStates = numStates;
//		this.sigmaSize = sigmaSize;
//		pi = new double[ numStates ];
//		a = new double[ numStates ][ numStates ];
//		b = new double[ numStates ][ sigmaSize ];
//		this.reset();
//	}
//
//
//	private void reset() {
//		int jumplimit = 2;
//
//
//		pi[0] = 1;
//		for ( int i = 1; i < numStates; i++ ) {
//			pi[i] = 0;
//		}
//
//
//		for ( int i = 0; i < numStates; i++ ) {
//			for ( int j = 0; j < numStates; j++ ) {
//				if ( i == numStates - 1 && j == numStates - 1 ) { // last row
//					a[i][j] = 1.0;
//				} else if ( i == numStates - 2 && j == numStates - 2 ) { // next to last row
//					a[i][j] = 0.5;
//				} else if ( i == numStates - 2 && j == numStates - 1 ) { // next to last row
//					a[i][j] = 0.5;
//				} else if ( i <= j && i > j - jumplimit - 1 ) {
//					a[i][j] = 1.0 / ( jumplimit + 1 );
//				} else {
//					a[i][j] = 0.0;
//				}
//			}
//		}
//
//
//
//		for ( int i = 0; i < numStates; i++ ) {
//			for ( int j = 0; j < sigmaSize; j++ ) {
//				b[i][j] = 1.0 / ( double ) sigmaSize;
//			}
//		}
//	}
//
//
//	public void train( Vector trainsequence ) {
//
//		double[][] a_new = new double[ a.length ][ a.length ];
//		double[][] b_new = new double[ b.length ][ b[0].length ];
//
//		for ( int i = 0; i < a.length; i++ ) {
//			for ( int j = 0; j < a[i].length; j++ ) {
//				double za = 0;
//				double ne = 0;
//
//				for ( int k = 0; k < trainsequence.size(); k++ ) {
//					this.reset();
//					int[] sequence = ( int[] ) trainsequence.elementAt( k );
//
//					double[][] fwd = this.forwardProc( sequence );
//					double[][] bwd = this.backwardProc( sequence );
//					double prob = this.getProbability( sequence );
//
//					double za_ = 0;
//					double ne_ = 0;
//
//
//					for ( int t = 0; t < sequence.length - 1; t++ ) {
//						za_ += fwd[i][t] * a[i][j] * b[j][sequence[t + 1]] * bwd[j][t + 1];
//						ne_ += fwd[i][t] * bwd[i][t];
//					}
//					za += ( 1 / prob ) * za_;
//					ne += ( 1 / prob ) * ne_;
//				} // k
//
//				a_new[i][j] = za / ne;
//			} // j
//		} // i
//
//
//		for ( int i = 0; i < b.length; i++ ) {
//			for ( int j = 0; j < b[i].length; j++ ) {
//				double za = 0;
//				double ne = 0;
//
//				for ( int k = 0; k < trainsequence.size(); k++ ) {
//					this.reset();
//					int[] sequence = ( int[] ) trainsequence.elementAt( k );
//
//					double[][] fwd = this.forwardProc( sequence );
//					double[][] bwd = this.backwardProc( sequence );
//					double prob = this.getProbability( sequence );
//
//					double za_ = 0;
//					double ne_ = 0;
//
//
//					for ( int t = 0; t < sequence.length - 1; t++ ) {
//						if ( sequence[t] == j ) {
//							za_ += fwd[i][t] * bwd[i][t];
//						}
//						ne_ += fwd[i][t] * bwd[i][t];
//					}
//					za += ( 1 / prob ) * za_;
//					ne += ( 1 / prob ) * ne_;
//				} // k
//
//				b_new[i][j] = za / ne;
//			} // j
//		} // i
//
//		this.a = a_new;
//		this.b = b_new;
//	}
//
//
//	private double[][] forwardProc( int[] o ) {
//		double[][] f = new double[ numStates ][ o.length ];
//		for ( int l = 0; l < f.length; l++ ) {
//			f[l][0] = pi[l] * b[l][o[0]];
//		}
//		for ( int i = 1; i < o.length; i++ ) {
//			for ( int k = 0; k < f.length; k++ ) {
//				double sum = 0;
//				for ( int l = 0; l < numStates; l++ ) {
//					sum += f[l][i - 1] * a[l][k];
//				}
//				f[k][i] = sum * b[k][o[i]];
//			}
//		}
//		return f;
//	}
//
//
//	public double getProbability( int[] o ) {
//		double prob = 0.0;
//		double[][] forward = this.forwardProc( o );
//
//		for ( int i = 0; i < forward.length; i++ ) {
//			prob += forward[i][forward[i].length - 1];
//		}
//		return prob;
//	}
//
//
//	public double[][] backwardProc( int[] o ) {
//		int T = o.length;
//		double[][] bwd = new double[ numStates ][ T ];
//
//		for ( int i = 0; i < numStates; i++ ) {
//			bwd[i][T - 1] = 1;
//		}
//		for ( int t = T - 2; t >= 0; t-- ) {
//			for ( int i = 0; i < numStates; i++ ) {
//				bwd[i][t] = 0;
//				for ( int j = 0; j < numStates; j++ ) {
//					bwd[i][t] += ( bwd[j][t + 1] * a[i][j] * b[j][o[t + 1]] );
//				}
//			}
//		}
//		return bwd;
//	}
//
//
//	public double[][] getA() {
//		return this.a;
//	}
//
//
//	public void setA( double[][] a ) {
//		this.a = a;
//	}
//
//
//	public double[][] getB() {
//		return this.b;
//	}
//
//
//	public void setB( double[][] b ) {
//		this.b = b;
//	}
//
//
//}
//
