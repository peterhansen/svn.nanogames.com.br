package br.com.nanogames.components.gestures;

//package br.com.nanogames.components.gestures;
//
//import java.util.Vector;
//
//
//public class GestureModel {
//
//	private int states;
//	private int observations;
//	private int id;
//	private Quantizer quantizer;
//	private HMM markovmodell;
//	private double defaultprobability;
//
//
//	public GestureModel( int id ) {
//		this.id = id;
//		this.states = 8;
//		this.observations = 14;
//		this.markovmodell = new HMM( states, observations );
//		this.quantizer = new Quantizer( states );
//	}
//
//
//	public void train( Vector trainsequence ) {
//
//		double maxacc = 0;
//		double minacc = 0;
//		Gesture sum = new Gesture();
//
//		for ( int i = 0; i < trainsequence.size(); i++ ) {
//			Vector t = ( ( Gesture ) trainsequence.elementAt( i ) ).getData();
//
//
//			maxacc += ( ( Gesture ) trainsequence.elementAt( i ) ).getMaxAcceleration();
//			minacc += ( ( Gesture ) trainsequence.elementAt( i ) ).getMinAcceleration();
//
//
//			for ( int j = 0; j < ( ( Gesture ) trainsequence.elementAt( i ) ).getData().size(); j++ ) {
//				sum.add( ( AccelerationEvent ) t.elementAt( j ) );
//			}
//
//		}
//
//
//		sum.setMaxAcceleration( maxacc / trainsequence.size() );
//		sum.setMinAcceleration( minacc / trainsequence.size() );
//
//
//		this.quantizer.trainCenteroids( sum );
//
//
//		Vector seqs = new Vector();
//		for ( int i = 0; i < trainsequence.size(); i++ ) {
//			seqs.addElement( this.quantizer.getObservationSequence( ( Gesture ) trainsequence.elementAt( i ) ) );
//		}
//
//
//		this.markovmodell.train( seqs );
//
//
//		this.setDefaultProbability( trainsequence );
//	}
//
//
//	public double matches( Gesture gesture ) {
//		int[] sequence = quantizer.getObservationSequence( gesture );
//		return this.markovmodell.getProbability( sequence );
//	}
//
//
//	public int getId() {
//		return this.id;
//	}
//
//
//	public void setId( int id ) {
//		this.id = id;
//	}
//
//
//	private void setDefaultProbability( Vector defsequence ) {
//		double prob = 0;
//		for ( int i = 0; i < defsequence.size(); i++ ) {
//			prob += this.matches( ( Gesture ) defsequence.elementAt( i ) );
//		}
//
//		this.defaultprobability = ( prob ) / defsequence.size();
//	}
//
//
//	public double getDefaultProbability() {
//		return this.defaultprobability;
//	}
//
//
//}
//
