/*
 * Sprite.java
 *
 * Created on September 28, 2007, 5:11 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.ImageLoader;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.util.Vector;


//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//# import java.awt.Image;
//#endif

/**
 *
 * @author peter
 */
public class Sprite extends Drawable implements Updatable {

	/** Extensão de arquivo utilizada nos descritores de frameSets. */
	public static final String FRAMESET_DESCRIPTOR_EXTENSION = ".bin";

	/** Caracter utilizado para indicar para o leitor de descritores o início de uma sequência de frames com durações
	 * fixas (o primeiro valor lido é a duração dos frames, e em seguida os índices da sequência). */
	public static final char FRAMESET_DESCRIPTOR_SEQUENCE_FIXED = 'f';

	/** Caracter utilizado para indicar para o leitor de descritores o início de uma sequência de frames com durações
	 * variáveis. Os valores presentes na sequência devem sempre obedecer a ordem índice - duração. */
	public static final char FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE = 'v';

	/** Sequências dos índices dos frames que compõem a animação. */
	protected final byte[][] sequences;

	/** Duração em milisegundos de cada frame. */
	protected final short[][] frameTimes;

	/** Offset na posição de desenho de cada frame, causado pelas variações de tamanho de cada imagem. */
	protected final Point[] offsets;

	/**	Posição (x, y) e área visível (largura, altura) de cada frame dentro da imagem. */
	protected final Rectangle[] frames;

	/** índice da sequência de frames atual */
	protected byte sequenceIndex;

	/** Índice do frame atual na sequência. */
	protected byte frameSequenceIndex;

	/** Índice real do frame atual (índice da imagem no array de frames). */
	protected byte frameRealIndex;

	/** tempo acumulado no frame atual */
	protected short accTime;

	/** Imagem que contém os frames da animação. */
	protected final Image image;

 	/** Referência para um objeto da classe sprite, que serve para desenhar de forma otimizada no caso de serem aplicadas
 	 * transformações na imagem. Utilizando-se apenas a imagem, ocorrem erros e/ou travamentos ao rotacionar imagens em
 	 * aparelhos Samsung e Siemens.
 	 */
 	protected javax.microedition.lcdui.game.Sprite frameSprite;

	/** Duração do frame atual em milisegundos. */
	protected short frameTime;

	/** Referência para a sequência atual, para acelerar acessos futuros (em vez de referenciar o array o tempo todo) */
	protected byte[] sequence;

	/** Listener que receberá os eventos de fim de sequência (pode ser nulo) */
	protected SpriteListener listener;

	/** Id do sprite, a ser passado para o listener poder identificar qual sprite o chamou */
	protected int id;

	/** Indica se a animação do sprite está pausada no momento */
	protected boolean paused;


	// TODO tratar offset nos casos de rotação


	/**
	 * Aloca um novo sprite, a partir das informações presentes no descritor.
	 *
	 * @param descriptorFilename
	 * @param filenamePrefix
	 * @throws java.lang.Exception
	 */
	public Sprite( String descriptorFilename, String filenamePrefix ) throws Exception {
		this( descriptorFilename, filenamePrefix, null );
	}


	/**
	 * TODO comentar
	 * @param prefixPath
	 * @throws java.lang.Exception
	 */
	public Sprite( String prefixPath ) throws Exception {
		this( prefixPath + FRAMESET_DESCRIPTOR_EXTENSION, prefixPath + ".png" );
	}


	/**
	 *
	 * @param prefixPath
	 * @param map
	 * @throws Exception
	 */
	public Sprite( String prefixPath, PaletteMap[] map ) throws Exception {
		this( prefixPath + FRAMESET_DESCRIPTOR_EXTENSION, prefixPath + ".png", map );
	}


	/**
	 * Aloca um novo sprite, a partir das informações presentes no descritor.
	 *
	 * @param descriptorFilename
	 * @param filenamePrefix
	 * @param map Mapa para conversão de cores de paleta ou null caso deseje utilizar as cores originais
	 * @throws java.lang.Exception
	 */
	public Sprite( String descriptorFilename, String filename, PaletteMap[] map ) throws Exception {
		this( new FrameReader( descriptorFilename, filename, map ) );
	}


	/**
	 * Aloca um novo sprite, a partir das informações presentes no descritor.
	 *
	 * @param descriptorFilename
	 * @param filenamePrefix
	 * @param lightColor Cor mais clara utilizada na interpolação de cores
	 * @param darkColor Cor mais escura utilizada na interpolação de cores
	 * @throws java.lang.Exception
	 */
	public Sprite( String descriptorFilename, String filenamePrefix, int lightColor, int darkColor ) throws Exception {
		this( new FrameReader( descriptorFilename, filenamePrefix, lightColor, darkColor ) );
	}


	/**
	 *
	 * @param reader
	 * @throws java.lang.Exception
	 */
	private Sprite( FrameReader reader ) throws Exception {
		this( reader.image, reader.offsets, reader.frames, reader.getSequences(), reader.getFrameTimes() );
		setSize( reader.totalSize );
	}


	/**
	 * Cria uma cópia de um sprite.
	 *
	 * @param s sprite a ser copiado. O novo sprite possuirá os mesmos frames, offsets de frames, sequências e duração
	 * de frames do original. Atributos como posição, pixel de referência, visibilidade e etc NÃO são copiados.
	 */
	public Sprite( Sprite s ) {
		setSize( s.getSize() );

		image = s.image;

		// Faz uma cópia dos offsets (não utiliza os mesmos objetos senão alterações causadas por transformações serão
		// refletidas em todos os sprites que utilizam a mesma origem). A transformação atual do sprite utilizado como
		// base da cópia é necessária para evitar que os valores copiados dos offsets estejam modificados em função
		// da transformação.
		final int previousTransform = s.getTransform();
		s.setTransform( TRANS_NONE );

		offsets = new Point[ s.offsets.length ];
		for ( int i = 0; i < offsets.length; ++i )
			offsets[ i ] = new Point( s.offsets[ i ] );

		frames = new Rectangle[ s.frames.length ];
		for ( int i = 0; i < frames.length; ++i )
			frames[ i ] = new Rectangle( s.frames[ i ] );

		sequences = new byte[ s.sequences.length ][];
		for ( int i = 0; i < sequences.length; ++i ) {
			sequences[ i ] = new byte[ s.sequences[ i ].length ];
			System.arraycopy( s.sequences[ i ], 0, sequences[ i ], 0, sequences[ i ].length );
		}

		frameTimes = new short[ s.frameTimes.length ][];
		for ( int i = 0; i < frameTimes.length; ++i ) {
			frameTimes[ i ] = new short[ s.frameTimes[ i ].length ];
			System.arraycopy( s.frameTimes[ i ], 0, frameTimes[ i ], 0, frameTimes[ i ].length );
		}

		s.setTransform( previousTransform );
		setSequence( 0 );
	}


	/**
	 *
	 * @param frames
	 * @param offsets
	 * @param sequences
	 * @param frameTimes
	 */
	protected Sprite( Image image, Point[] offsets, Rectangle[] frames, byte[][] sequences, short[][] frameTimes ) {
		this.image = image;
		this.offsets = offsets;
		this.frames = frames;
		this.sequences = sequences;
		this.frameTimes = frameTimes;

		setSequence( 0 );
	}


	public void draw( Graphics g ) {
		if ( visible ) {
			final Point p = position.add( offsets[ frameRealIndex ] );
			if ( clipTest ) {
				pushClip( g );
				final Rectangle frame = frames[ frameRealIndex ];
				translate.addEquals( p );

				final Rectangle clip = clipStack[ currentStackSize ];
				clip.setIntersection( translate.x, translate.y, frame.width, frame.height );

				if ( clip.width > 0 && clip.height > 0 ) {
					g.setClip( clip.x, clip.y, clip.width, clip.height );
					paint( g );
				}

				translate.subEquals( p );
				popClip( g );
			} else {
				// desenha direto, ignorando o teste de interseção
				translate.addEquals( p );
				paint( g );
				translate.subEquals( p );
			}
		} // fim if ( visible )
	}


	protected void paint( Graphics g ) {
		final Rectangle frame = frames[ frameRealIndex ];
		//#if J2SE == "false"
			if ( USE_MIDP2_SPRITE ) {
				frameSprite.setTransform( transformMIDP );
				frameSprite.setPosition( translate.x - frame.x, translate.y + -frame.y );
				frameSprite.paint( g );
			} else {
				if ( transform == TRANS_NONE )
					g.drawImage( image, translate.x - frame.x, translate.y - frame.y, 0 );
				else
					g.drawRegion( image, frame.x, frame.y, frame.width, frame.height, transformMIDP, translate.x, translate.y, 0 );
			}
		//#else
//# 			if ( transform == TRANS_NONE )
//# 				g.drawImage( frame, translate.x + getFrameOffset( frameRealIndex ).x, translate.y + getFrameOffset( frameRealIndex ).y, null );
//# 			else
//# 				g.drawImage( frame, translate.x + getFrameOffset( frameRealIndex ).x, translate.y + getFrameOffset( frameRealIndex ).y, null ); // TODO desenhar imagem com transformação em J2SE
		//#endif
	}


	public final short getCurrFrameAccTime() {
		return accTime;
	}


	public final void setCurrFrameAccTime( int time ) {
		accTime = ( short )time;
	}


	public final void pause( boolean b ) {
		paused = b;
	}


	public final boolean isPaused() {
		return paused;
	}


	public SpriteListener getListener() {
		return listener;
	}


	public void update( int delta ) {
		// se o frame tiver duração 0 (zero), o controle da animação é feito externamente, através de chamadas explícitas
		// a nextFrame, previousFrame e etc.
		if ( frameTime > 0 && !paused ) {
			accTime += delta;
			if ( accTime >= frameTime ) {
				// trocou de frame
				accTime %= frameTime;

				nextFrame();

				// Se houver um listener registrado ...
				if ( listener != null )
				{
					// Avisa que mudou de frame
					listener.onFrameChanged( id, frameSequenceIndex  );

					// Avisa que a sequência acabou
					if ( frameSequenceIndex == 0 )
						listener.onSequenceEnded( id, sequenceIndex );
				}
			}
		}
	}


	/**
	 * Avança um frame na sequência atual.
	 *
	 * @see #previousFrame()
	 * @see #setFrame(int)
	 */
	public void nextFrame() {
		if ( frameSequenceIndex < sequence.length - 1 )
			setFrame( frameSequenceIndex + 1 );
		else
			setFrame( 0 );
	}


	/**
	 * Retrocede um frame na sequência atual.
	 *
	 * @see #nextFrame()
	 * @see #setFrame(int)
	 */
	public void previousFrame() {
		if ( frameSequenceIndex > 0 )
			setFrame( frameSequenceIndex - 1 );
		else
			setFrame( sequence.length - 1 );
	}


	/**
	 * Define o frame atual do sprite.
	 *
	 * @param index índice do frame do sprite na sequência de animação atual.
	 * @see #nextFrame()
	 * @see #previousFrame()
	 */
	public void setFrame( int index ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif

		frameSequenceIndex = ( byte ) index;
		frameRealIndex = sequence[ frameSequenceIndex ];
		frameTime = frameTimes[ sequenceIndex ][ frameSequenceIndex ];

		if ( USE_MIDP2_SPRITE ) {
			frameSprite = new javax.microedition.lcdui.game.Sprite( image );
			frameSprite.defineReferencePixel( referencePixel.x, referencePixel.y );
		}

		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
//# 				System.err.println( "Exceção em Sprite.setFrame. frameSequenceIndex: " + frameSequenceIndex + ", frameRealIndex: " + frameRealIndex + " / " + frames.length + ", sequenceIndex: " + sequenceIndex );
//# 				AppMIDlet.log( e , "10" );
//# 			}
		//#endif
	}


	/**
	 * Define a sequência atual de animação do sprite. A sequência será iniciada a partir do frame de índice 0 (zero), e
	 * as dimensões do sprite serão atualizadas de acordo com as dimensões dos frames da nova sequência. Atenção: o ponto
	 * de referência não é alterado.
	 * @param sequence índice da sequência de animação.
	 */
	public void setSequence( int sequence ) {
		//#if DEBUG == "true"
//# 			try {
		//#endif
		sequenceIndex = ( byte ) sequence;
		this.sequence = sequences[ sequenceIndex ];
		setFrame( 0 );
		accTime = 0;

		//#if DEBUG == "true"
//# 			} catch ( Exception e ) {
//# 				System.err.println( "Exceção em Sprite.setSequence. sequence: " + sequence + ", sequences.length: " + ( sequences == null ? -1 : sequences.length ) );
//# 				AppMIDlet.log( e , "11"  );
//# 			}
		//#endif
	} // fim do método setSequence( int )


	/**
	 * Obtém o índice da sequência atual.
	 *
	 * @return índice da sequência atual.
	 */
	public final byte getSequenceIndex() {
		return sequenceIndex;
	}


	/**
	 *
	 * @param index
	 * @return
	 */
	public final byte[] getSequence( int index ) {
		return sequences[ index ];
	}


	/**
	 * Retorna a sequência atual usada pelo sprite
	 *
	 * @return sequência atual
	 */
	public final byte[] getCurrentSequence() {
		return sequences[ sequenceIndex ];
	}


	/**
	 * Retorna o número de sequências que o sprite possui.
	 *
	 * @return número total de sequências.
	 */
	public final int getNSequences() {
		return sequences.length;
	}


	/**
	 * Obtém o índice do frame atual na sequência.
	 *
	 * @return índice do frame atual na sequência.
	 */
	public final byte getFrameSequenceIndex() {
		return frameSequenceIndex;
	}


	/**
	 * Obtém a referência para a imagem do frame atual.
	 *
	 * @return referência para a imagem do frame atual.
	 */
	public final Image getCurrentFrameImage() {
		return image;
	}


	/**
	 * Obtém a referência para a imagem de um frame.
	 *
	 * @param sequenceIndex índice da sequência cuja imagem será obtida.
	 * @param frameIndex índice do frame na sequência cuja imagem será obtida.
	 * @return referência para a imagem do frame.
	 */
	public final Image getFrameImage( int sequenceIndex, int frameIndex ) {
		return image;
	}
	
	
	public final Rectangle getFrame( int sequenceIndex, int frameIndex ) {
		return getFrame( getSequence( sequenceIndex )[ frameIndex ] );
	}


	public final Rectangle getFrame( int frameRealIndex ) {
		return frames[ frameRealIndex ];
	}
	
	
	public final Rectangle getCurrentFrame() {
		return frames[ frameRealIndex ];
	}


	/**
	 * Obtém o offset do frame passado como parâmetro.
	 *
	 * @param frameIndex Índice do frame
	 * @return Offset do frame.
	 */
	public Point getFrameOffset( int frameIndex ) {
		return offsets[ frameIndex ];
	}


	/**
	 * Retorna o índice do frame atual no array de frames do sprite
	 *
	 * @return Índice do frame atual
	 */
	public final byte getCurrFrameIndex() {
		return frameRealIndex;
	}


	/**
	 * Define o listener que terá seu método <i>sequenceEnded</i> chamado quando uma sequência do sprite terminar.
	 *
	 * @param listener referência para o listener do sprite. Passar <i>null</i> remove o listener anterior.
	 * @param id identificação do sprite, que será passada para o listener identificar qual sprite teve uma sequência encerrada.
	 */
	public final void setListener( SpriteListener listener, int id ) {
		this.listener = listener;
		this.id = id;
	}


	public final int getId() {
		return id;
	}


	public boolean mirror( int mirrorType ) {
		mirrorType &= TRANS_MASK_MIRROR;
		final int newMirrorStatus = ( transform & TRANS_MASK_MIRROR ) ^ mirrorType;

		// FIXME erro no posicionamento interno da imagem em algumas combinações de rotação e espelhamento
		// se houver mudança no espelhamento horizontal, atualiza o pixel de referência e os offsets dos frames
		if ( ( newMirrorStatus & TRANS_MIRROR_H ) != ( transform & TRANS_MIRROR_H ) ) {
			defineReferencePixel( size.x - referencePixel.x, referencePixel.y );

			for ( int i = 0; i < offsets.length; ++i ) {
				//#if J2SE == "false"
					offsets[ i ].x = size.x - offsets[ i ].x - frames[ i ].width;
				//#else
//# 					offsets[ i ].x = size.x - offsets[ i ].x - frames[ i ].getWidth( null );
				//#endif
			}
		}

		// se houver mudança no espelhamento vertical, atualiza o pixel de referência e os offsets dos frames
		if ( ( newMirrorStatus & TRANS_MIRROR_V ) != ( transform & TRANS_MIRROR_V ) ) {
			defineReferencePixel( referencePixel.x, size.y - referencePixel.y );

			for ( int i = 0; i < offsets.length; ++i ) {
				//#if J2SE == "false"
					offsets[ i ].y = size.y - offsets[ i ].y - frames[ i ].height;
				//#else
//# 					offsets[ i ].y = size.y - offsets[ i ].y - frames[ i ].getHeight( null );
				//#endif
			}
		}

		// a transformação passa a ser a união da rotação acumulada com o novo estado de espelhamento
		transform = ( transform & TRANS_MASK_ROTATE ) | newMirrorStatus;
		updateTransformMIDP();

		return true;
	} // fim do método mirror( int )


	public boolean rotate( int rotationType ) {
		super.rotate( rotationType );

		 // atualiza os offsets do sprite após rotações
		switch ( rotationType ) {
			case TRANS_ROT90:
			case TRANS_ROT270:
				for ( byte i = 0; i < offsets.length; ++i )
					offsets[ i ].set( offsets[ i ].y, offsets[ i ].x );
			break;
		}

		return true;
	}


	public void defineReferencePixel( int refPixelX, int refPixelY ) {
		super.defineReferencePixel( refPixelX, refPixelY );

		if ( USE_MIDP2_SPRITE )
			frameSprite.defineReferencePixel( refPixelX, refPixelY );
	}


	/**
	 *
	 * @param sequencesFrameTime
	 */
	private final void setFrameTimes( short[] sequencesFrameTime ) {
		// preenche o array da duração de frames
		for ( int i = 0; i < frameTimes.length; ++i ) {
			frameTimes[ i ] = new short[ sequences[ i ].length ];

			final short currentSequenceFrameTime = sequencesFrameTime[ i ];

			for ( int j = 0; j < frameTimes[ i ].length; ++j )
				frameTimes[ i ][ j ] = currentSequenceFrameTime;
		}
	}


	/**
	 * Obtém o array de duração dos frames de cada sequência.
	 * @return array com a duração dos frames de cada sequência.
	 */
	public final short[][] getFrameTimes() {
		return frameTimes;
	}


	//<editor-fold defaultstate="collapsed" desc="Classe interna FrameReader">

	private static final class FrameReader implements Serializable {

		private static final byte READ_STATE_FRAMES_SIZE			= 0;
		private static final byte READ_STATE_TOTAL_FRAMES			= READ_STATE_FRAMES_SIZE + 1;
		private static final byte READ_STATE_FRAMES_INFO			= READ_STATE_TOTAL_FRAMES + 1;
		private static final byte READ_STATE_READ_SEQUENCE_TYPE		= READ_STATE_FRAMES_INFO + 1;
		private static final byte READ_STATE_FIXED_READ_TIME		= READ_STATE_READ_SEQUENCE_TYPE + 1;
		private static final byte READ_STATE_FIXED_READ_INDEXES		= READ_STATE_FIXED_READ_TIME + 1;
		private static final byte READ_STATE_VARIABLE_READ_TIME		= READ_STATE_FIXED_READ_INDEXES + 1;
		private static final byte READ_STATE_VARIABLE_READ_INDEX	= READ_STATE_VARIABLE_READ_TIME + 1;

		private byte readState = READ_STATE_FRAMES_SIZE;

		/** Offset na posição de cada frame (valor temporário). */
		private Point[] offsets;

		private Rectangle[] frames;

		private final Vector indexes = new Vector();
		private final Vector times = new Vector();
		private final Vector sequenceIndexVector = new Vector();
		private final Vector sequenceTimeVector = new Vector();

		private final Image image;

		private final Point totalSize = new Point();


		private FrameReader( String descritorFilename, String framesFilename, PaletteMap[] map ) throws Exception
		{
			AppMIDlet.openJarFile( descritorFilename, this );

			// aloca os frames
			if( map == null ) {
				image = ImageLoader.loadImage( framesFilename );
			} else {
				final PaletteChanger pc = new PaletteChanger( framesFilename );
				image = pc.createImage( map );
			}
		}


		private FrameReader( String descritorFilename, String framesFilename, int lightColor, int darkColor ) throws Exception {
			AppMIDlet.openJarFile( descritorFilename, this );


			final PaletteChanger pc = new PaletteChanger( framesFilename );
			image = pc.createImage( lightColor, darkColor );
		}


		public final void write( DataOutputStream output ) throws Exception {
		}


		public final void read( DataInputStream input ) throws Exception {
			try {
				short currentFrame = 0;

				while ( true ) {
					switch ( readState ) {
						case READ_STATE_FRAMES_SIZE:
							totalSize.set( readNumber( input ), readNumber( input ) );
							setReadState( READ_STATE_TOTAL_FRAMES );
						break;

						case READ_STATE_TOTAL_FRAMES:
							offsets = new Point[ readNumber( input )];
							frames = new Rectangle[ offsets.length ];

							setReadState( READ_STATE_FRAMES_INFO );
						break;

						case READ_STATE_FRAMES_INFO:
							frames[ currentFrame ] = new Rectangle( readNumber( input ), readNumber( input ), readNumber( input ), readNumber( input ) );
							offsets[ currentFrame ] = new Point( readNumber( input ), readNumber( input ) );
							++currentFrame;
							if ( currentFrame >= offsets.length ) {
								setReadState( READ_STATE_READ_SEQUENCE_TYPE );
							}
						break;

						case READ_STATE_READ_SEQUENCE_TYPE:
							char c;
							do {
								c = ( char ) input.readUnsignedByte();
							} while ( c != FRAMESET_DESCRIPTOR_SEQUENCE_FIXED && c != FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE );
							setReadState( c == FRAMESET_DESCRIPTOR_SEQUENCE_FIXED ? READ_STATE_FIXED_READ_TIME : READ_STATE_VARIABLE_READ_TIME );
						break;

						case READ_STATE_FIXED_READ_TIME:
							times.addElement( new Integer( readNumber( input, false ) ) );
							setReadState( READ_STATE_FIXED_READ_INDEXES );
						break;

						case READ_STATE_FIXED_READ_INDEXES:
							int fixedIndex = readNumber( input, false );
							while ( fixedIndex >= 0 ) {
								indexes.addElement( new Integer( fixedIndex ) );
								fixedIndex = readNumber( input, false );
							}
							setReadState( READ_STATE_READ_SEQUENCE_TYPE );
						break;

						case READ_STATE_VARIABLE_READ_TIME:
							final int variableTime = readNumber( input, false );
							if ( variableTime >= 0 ) {
								times.addElement( new Integer( variableTime ) );
								setReadState( READ_STATE_VARIABLE_READ_INDEX );
							}
							// não é necessário tratar o "else", pois o tratamento adequado já foi feito dentro de readNumber()
						break;

						case READ_STATE_VARIABLE_READ_INDEX:
							final int frameIndex = readNumber( input, false );
							// Se for um índice válido, o insere no array, e lê a próxima informação (duração do próximo frame)
							// Caso contrário, o tratamento já terá sido feito dentro de readNumber.
							if ( frameIndex >= 0 ) {
								indexes.addElement( new Integer( frameIndex ) );
								setReadState( READ_STATE_VARIABLE_READ_TIME );
							}
						break;
					}
				} // fim while ( true )
			} catch ( EOFException eof ) {
				// leu todos os valores presentes no arquivo
				stateEnd();

				//#if DEBUG == "true"
//# 				// no modo debug, lança exceção caso o leitor termine num estado intermediário, o que indica erro no
//# 				// arquivo descritor de frames.
//# 				switch ( readState ) {
//# 					case READ_STATE_FIXED_READ_TIME:
//# 					case READ_STATE_VARIABLE_READ_INDEX:
//# 					throw new Exception( "Erro no descritor de sprites: fim detectado no estado de leitura #" + readState + "." );
//# 				}
//#
//# 				if ( sequenceIndexVector.size() == 0 )
//# 					throw new Exception( "Erro no descritor de sprites: nenhuma sequência definida." );
				//#endif
			}
		}


		/**
		 * Lê um número do stream, desprezando quaisquer outros caracteres.
		 * @param input
		 * @return
		 */
		private final short readNumber( DataInputStream input ) throws Exception {
			return readNumber( input, true );
		}

		/**
		 * Lê um número do stream, desprezando quaisquer outros caracteres.
		 * @param input
		 * @param ignoreEndOfLine 
		 * @return
		 */
		private final short readNumber( DataInputStream input, final boolean ignoreEndOfLine ) throws Exception {
			final StringBuffer b = new StringBuffer();
			boolean startedNumber = false;
			boolean reading = true;

			while ( reading ) {
				final char c = ( char ) input.readUnsignedByte();

				switch ( c ) {
					case '0': case '1': case '2': case '3': case '4':
					case '5': case '6': case '7': case '8': case '9':
						startedNumber = true;
						b.append( c );
					break;

					case '\r':
					case '\n':
						if ( !ignoreEndOfLine || startedNumber ) {
							reading = false;
							switch ( readState ) {
								case READ_STATE_FIXED_READ_INDEXES:
								case READ_STATE_VARIABLE_READ_INDEX:
									indexes.addElement( new Integer( Short.parseShort( b.toString() ) ) );
									stateEnd();
									setReadState( READ_STATE_READ_SEQUENCE_TYPE );
									return -1;
							}
						}
					break;

					default:
						if ( startedNumber )
							reading = false;
				}
			}

			if ( b.length() > 0 )
				return Short.parseShort( b.toString() );

			return -1;
		}


		private final void setReadState( int readState ) {
			switch ( readState ) {
				case READ_STATE_READ_SEQUENCE_TYPE:
				case READ_STATE_FIXED_READ_TIME:
					times.removeAllElements();
				case READ_STATE_FIXED_READ_INDEXES:
					indexes.removeAllElements();
				break;
			}

			this.readState = ( byte ) readState;
		}


		private final void stateEnd() {
			switch ( readState ) {
				case READ_STATE_FIXED_READ_INDEXES:
				case READ_STATE_VARIABLE_READ_INDEX:
					final byte[] sequence = new byte[ indexes.size() ];
					for ( byte i = 0; i < sequence.length; ++i ) {
						sequence[ i ] = ( ( Integer ) indexes.elementAt( i ) ).byteValue();
					}
					sequenceIndexVector.addElement( sequence );

					final short[] time = new short[ sequence.length ];
					if ( times.size() == time.length ) {
						for ( byte i = 0; i < sequence.length; ++i ) {
							time[ i ] = ( ( Integer ) times.elementAt( i ) ).shortValue();
						}
					} else {
						final short DEFAULT_TIME = ( ( Integer ) times.elementAt( 0 ) ).shortValue();
						for ( byte i = 0; i < sequence.length; ++i ) {
							time[ i ] = DEFAULT_TIME;
						}
					}
					sequenceTimeVector.addElement( time );
				break;
			}
		} // fim do método stateEnd()


		private final byte[][] getSequences() {
			final byte[][] sequences = new byte[ sequenceIndexVector.size() ][];

			for ( byte i = 0; i < sequences.length; ++i ) {
				sequences[ i ] = ( byte[] ) ( sequenceIndexVector.elementAt( i ) );
			}

			return sequences;
		}


		private final short[][] getFrameTimes() {
			final short[][] frameTimes = new short[ sequenceIndexVector.size() ][];

			for ( byte i = 0; i < frameTimes.length; ++i ) {
				frameTimes[ i ] = ( short[] ) ( sequenceTimeVector.elementAt( i ) );
			}

			return frameTimes;
		}

	}; // fim da classe interna FileHandler

	//</editor-fold>

}