/**
 * ScrollBar.java
 * 
 * Created on 11/Nov/2008, 11:41:25
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public abstract class ScrollBar extends Component {

	/***/
	public static final byte TYPE_VERTICAL		= 0;

	/***/
	public static final byte TYPE_HORIZONTAL	= 1;

	/***/
	protected final byte type;
	
	/** Modo de arrasto do ponteiro: sem arrasto. */
	protected static final byte DRAG_MODE_NONE			= 0;
	/** Modo de arrasto do ponteiro: arrastando barra de scroll da página atual. */
	protected static final byte DRAG_MODE_SCROLL_PAGE	= 1;
	
	/** Indica o tipo de arrasto do ponteiro. */
	protected byte dragMode;
	
	/** Posição de início do arrasto da barra de scroll da página. */
	protected short dragYStart;	
	
	protected Drawable barFull;
	
	protected Drawable barPage;
	
	
	public ScrollBar( int nSlots, byte type ) {
		super( nSlots );
		
		this.type = type;
	}


	public void destroy() {
		super.destroy();
		
		barFull = null;
		barPage = null;
	}
	
	
	/**
	 * 
	 * @param currentValue
	 * @param currentPosition
	 * @param maxValue
	 */
	public abstract void refreshScroll( int currentValue, int currentPosition, int maxValue );
	
	
	/**
	 * Atualiza o scroll com base nas informações de um componente. Equivalente à chamada de:
	 * <p><code>refreshScroll( c.getHeight(), -c.getPosY(), c.getPreferredHeight() )</code>, no caso de scroll vertical</p>
	 * <p><code>refreshScroll( c.getWidth(), -c.getPosX(), c.getPreferredWidth() )</code>, no caso de scroll horizontal</p>
	 * @param c componente base da barra de scroll.
	 */
	public final void refreshScroll( Container c ) {
		if ( type == TYPE_VERTICAL )
			refreshScroll( c.getHeight(), -c.getScrollY(), c.getMaximumHeight() );
		else
			refreshScroll( c.getWidth(), -c.getScrollX(), c.getMaximumWidth() );
	}


	/**
	 * 
	 * @param x posição x do ponteiro relativa ao seu pai.
	 * @param y posição y do ponteiro relativa ao seu pai.
	 */
	public void onPointerPressed( int x, int y ) {
		x -= position.x;
		y -= position.y;
		
		if ( barPage != null && barPage.contains( x, y ) ) {
			// usuário clicou na barra de scroll da página atual
			dragMode = DRAG_MODE_SCROLL_PAGE;
			dragYStart = ( short ) y;
		} else if ( barFull != null && barFull.contains( x, y ) ) {
			// usuário clicou na barra de scroll total
			dragMode = DRAG_MODE_NONE;

			if ( barPage != null ) {
				// avança ou retrocede uma página, de acordo com a posição clicada na barra
				parent.setScrollY( parent.getScrollY() + ( y < barPage.getPosY() ? getHeight(): -getHeight() ) );
			}
		}		
	}

	
	/**
	 * 
	 * @param x posição x do ponteiro relativa ao seu pai.
	 * @param y posição y do ponteiro relativa ao seu pai.
	 */
	public void onPointerDragged( int x, int y ) {
		switch ( dragMode ) {
			case DRAG_MODE_SCROLL_PAGE:
				x -= position.x - parent.getScrollX();
				y -= position.y - parent.getScrollY();
				
				// posiciona o texto de acordo com a posição relativa da barra de scroll
				final int FP_SCROLL_RELATION = NanoMath.divInt( size.y, barPage.getHeight() );
				final int FP_DIFF = NanoMath.toFixed( y - dragYStart );
				parent.setScrollY( parent.getScrollY() - NanoMath.toInt( NanoMath.mulFixed( FP_SCROLL_RELATION, FP_DIFF ) ) );
				dragYStart = ( short ) NanoMath.clamp( y, 0, getHeight() );
			break;
		}
	}
	
	
	/**
	 * 
	 * Observação: para o correto funcionamento desse método, os valores devem ser absolutos (os mesmos recebidos
	 * pelo form que originou o evento)
	 * @param x posição x do ponteiro.
	 * @param y posição y do ponteiro.
	 */	
	public void onPointerReleased( int x, int y ) {
		dragMode = DRAG_MODE_NONE;
	}
	
	
	public String getUIID() {
		return "scrollbar";
	}
	
	
}
