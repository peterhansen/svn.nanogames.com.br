/**
 * BorderLayout.java
 * 
 * Created on 22/Nov/2008, 11:46:35
 *
 */
package br.com.nanogames.components.userInterface.form.layouts;

import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.util.Point;


/**
 *
 * @author Peter
 */
public class BorderLayout extends Layout {

	/**
	 * The north layout constraint (top of container).
	 */
	public static final String NORTH = "NORTH";
	/**
	 * The south layout constraint (bottom of container).
	 */
	public static final String SOUTH = "SOUTH";
	/**
	 * The center layout constraint (middle of container)
	 */
	public static final String CENTER = "CENTER";
	/**
	 * The west layout constraint (left of container).
	 */
	public static final String WEST = "WEST";
	/**
	 * The east layout constraint (right of container).
	 */
	public static final String EAST = "EAST";
	protected Component north;
	protected Component south;
	protected Component center;
	protected Component west;
	protected Component east;


	/**
	 * @inheritDoc
	 */
	public void addLayoutComponent( Container c, Component comp, Object name ) {
		Component previous = null;

		/* Assign the component to one of the known regions of the layout.
		 */
		if ( CENTER.equals( name ) ) {
			previous = center;
			center = comp;
		} else if ( NORTH.equals( name ) ) {
			previous = north;
			north = comp;
		} else if ( SOUTH.equals( name ) ) {
			previous = south;
			south = comp;
		} else if ( EAST.equals( name ) ) {
			previous = east;
			east = comp;
		} else if ( WEST.equals( name ) ) {
			previous = west;
			west = comp;
		} else {
			//#if DEBUG == "true"
//# 				throw new IllegalArgumentException( "cannot add to layout: unknown constraint: " + name );
			//#else
			throw new IllegalArgumentException();
		//#endif
		}

		if ( previous != null && previous != comp ) {
			c.removeDrawable( previous );
		}
	}


	/**
	 * @inheritDoc
	 */
	public void removeLayoutComponent( Component comp ) {
		if ( comp == center ) {
			center = null;
		} else if ( comp == north ) {
			north = null;
		} else if ( comp == south ) {
			south = null;
		} else if ( comp == east ) {
			east = null;
		} else if ( comp == west ) {
			west = null;
		}
	}


	/**
	 * @inheritDoc
	 */
	public void layoutContainer( Container container ) {
		int top = 0;
		int bottom = container.getLayoutHeight();
		int left = 0;
		int right = container.getLayoutWidth();
		int targetWidth = container.getWidth();
		int targetHeight = container.getHeight();

		Component c = null;

		if ( north != null ) {
			c = north;
			c.setSize( right - left, Math.min( targetHeight, c.getPreferredHeight() ) ); //verify I want to use tge prefered size
			c.setPosition( left, top );
			top += c.getHeight();
		}

		if ( south != null ) {
			c = south;
			c.setSize( right - left, Math.min( targetHeight, c.getPreferredHeight() ) ); //verify I want to use tge prefered size
			c.setPosition( left, bottom - c.getHeight() );

			bottom -= c.getHeight();
		}

		if ( east != null ) {
			c = east;
			c.setSize( Math.min( targetWidth, c.getPreferredWidth() ), bottom - top ); //verify I want to use tge prefered size
			c.setPosition( right - c.getWidth(), top );

			right -= c.getWidth();
		}

		if ( west != null ) {
			c = west;
			c.setSize( Math.min( targetWidth, c.getPreferredWidth() ), bottom - top ); //verify I want to use tge prefered size
			c.setPosition( left, top );

			left += c.getWidth();
		}

		if ( center != null ) {
			c = center;
			c.setSize( right - left, bottom - top ); //verify I want to use the remaining size
			c.setPosition( left, top );
		}
	}


	public Point calcPreferredSize( Container parent, Point maxSize ) {
		final Point size = new Point();

		if ( east != null )
			size.set( east.getPreferredSize() );
		
		if ( west != null )
			size.set( size.x + west.getPreferredWidth(), Math.max( west.getPreferredHeight(), size.y ) );
		
		if ( center != null )
			size.set( size.x + center.getPreferredWidth(), Math.max( center.getPreferredHeight(), size.y ) );
		
		if ( north != null )
			size.set( Math.max( north.getPreferredWidth(), size.x ), size.y + north.getPreferredHeight() );
		
		if ( south != null )
			size.set( Math.max( south.getPreferredWidth(), size.x ), size.y + south.getPreferredHeight() );
		
		
		if ( maxSize != null ) {
			if ( size.x > maxSize.x )
				size.x = maxSize.x;
			
			if ( size.y > maxSize.y )
				size.y = maxSize.y;
		}

		return size;
	}
}
