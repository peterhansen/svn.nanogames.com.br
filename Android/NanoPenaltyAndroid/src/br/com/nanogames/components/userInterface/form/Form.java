/**
 * Form.java
 * 
 * Created on 5/Nov/2008, 14:31:47
 *
 */
package br.com.nanogames.components.userInterface.form;

import android.util.Log;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.layouts.BorderLayout;
import br.com.nanogames.components.util.Point;
import java.util.Vector;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//#endif

/**
 *
 * *<pre>
 *
 *       **************************
 *       *        TitleBar        *
 *       **************************
 *       *                        *
 *       *                        *
 *       *      ContentPane       *
 *       *                        *
 *       *                        *
 *       **************************
 *       *         MenuBar        *
 *       **************************
 * </pre> 
 * 
 * @author Peter
 */
public class Form extends Container implements ScreenListener {
	
	private static final byte TOTAL_ITEMS = 3;

	/** Referência para o componente que está com o foco de arrasto do ponteiro (caso haja um). */
	protected Component dragged;
	
	/** Barra de título (sempre na parte superior da tela). */
	protected Component titleBar;

	/** Barra de status (sempre na parte inferior da tela). */
	protected Component statusBar;

	protected Component previousStatusBar;
	
	/** Conteúdo central do form. */
	protected Container contentPane;
	
	/** Componente com foco atualmente. */
	protected Component focused;
	
    /**
     * Allows us to cache the next focus component ordered from top to down, this
     * vector is guaranteed to have all focusable children in it.
     */
    protected Component[] focusDownSequence;

	protected short focusDownIndex;
	protected short focusRightIndex;
	
    /**
     * Allows us to cache the next focus component ordered from left to right, this
     * vector is guaranteed to have all focusable children in it.
     */
    protected Component[] focusRightSequence;

	/***/
	protected boolean focusVectorInitialized;

	protected boolean keyPressed;

	//#if TOUCH == "true"
		/***/
		protected TouchKeyPad touchKeyPad;
	//#endif


	/**
	 * 
	 * @throws java.lang.Exception
	 */
	public Form() throws Exception {
		this( null );
	}
	
	
	/**
	 * 
	 * @param contentPane
	 * @throws java.lang.Exception
	 */
	public Form( Container contentPane ) throws Exception {
		super( TOTAL_ITEMS, new BorderLayout() );
		
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		
		setContentPane( contentPane );
	}
	
	
	/**
	 * 
	 */
	public void destroy() {
		super.destroy();
		
		clearFocusVectors();
		setDraggedComponent( null );
		setFocused( null );
		setContentPane( null );
	}
	
	
    /**
     * Resets the cache focus vectors, this is a good idea when we remove
     * or add an element to the layout.
     */
    protected void clearFocusVectors() {
		focusDownSequence = null;
		focusRightSequence = null;
		focusVectorInitialized = false;
		focusDownIndex = -1;
		focusRightIndex = -1;
    }	
	
	
	/**
	 * Chamada equivalente a <code>setContentPane( contentPane, true )</code>.
	 * @param contentPane
	 * @return
	 */
	public final Container setContentPane( Container contentPane ) {
		return setContentPane( contentPane, true );
	}
	
	
	/**
	 * 
	 * @param contentPane
	 */
	public Container setContentPane( Container contentPane, boolean destroyPrevious ) {
		keyPressed = false;
		
		final Container previous = this.contentPane;
		if ( previous != null ) {
			removeDrawable( previous, destroyPrevious );
			layout.removeLayoutComponent( previous );
		}
		
		if ( contentPane != null ) {
			this.contentPane = contentPane;
			insertDrawable( contentPane, BorderLayout.CENTER );
		}
		
		setDraggedComponent( null );
		setFocused( null );
		
		refreshLayout();
//		updateFocusVectors();
		initFocus(); // TODO teste
		
		return previous;
	}	
	
	
	/**
	 * 
	 * @return
	 */
	public Container getContentPane() {
		return contentPane;
	}
	
	
	/**
	 * 
	 * @param contentPane
	 */
	public void setTitleBar( Component titleBar ) {
		if ( this.titleBar != null ) {
			removeDrawable( this.titleBar );
			layout.removeLayoutComponent( this.titleBar );
		}
		
		this.titleBar = titleBar;
		insertDrawable( titleBar, BorderLayout.NORTH );
	}
	
	
	/**
	 * 
	 * @return
	 */
	public Component getTitleBar() {
		return titleBar;
	}		
	
	
	/**
	 * 
	 * @param contentPane
	 */
	public void setStatusBar( Component statusBar, boolean refreshLayout ) {
		//#if TOUCH == "true"
			previousStatusBar = this.statusBar instanceof TouchKeyPad ? null : this.statusBar;
		//#else
//# 			previousStatusBar = this.statusBar;
		//#endif
		
		if ( this.statusBar != null ) {
			removeDrawable( this.statusBar );
			layout.removeLayoutComponent( this.statusBar );
		}
		
		this.statusBar = statusBar;
		if ( statusBar != null )
			insertDrawable( statusBar, BorderLayout.SOUTH );

		if ( refreshLayout )
			refreshLayout();
	}


	//#if TOUCH == "true"
		public final void hideTouchKeyPad() {
			setStatusBar( previousStatusBar, true );

			if ( touchKeyPad.getTextBox() != null )
				touchKeyPad.getTextBox().requestFocus();
		}
	//#endif


	/**
	 * 
	 * @return
	 */
	public Component getStatusBar() {
		return statusBar;
	}


	/**
     * @inheritDoc
     */
    public final Form getComponentForm() {
        return this;
    }	
	
	
	/**
	 * 
	 * @return
	 */
	public String getUIID() {
		return "form";
	}
	
	
    /**
     * Sets the current dragged Component
     */
    protected void setDraggedComponent( Component c ) {
        dragged = c;
    }	
	
	
    /**
     * Sets the focused component and fires the appropriate events to make it so
     * 
     * @param focused the newly focused component or null for no focus
     */
    public void setFocused( Component focused ) {
		if ( this.focused == focused && focused != null ) {
			return;
		}
		final Component oldFocus = this.focused;
		this.focused = focused;

		if ( oldFocus != null ) {
			oldFocus.setFocus( false );
		}
		// a listener might trigger a focus change event essentially
		// invalidating focus so we shouldn't break that 
		if ( focused != null && this.focused == focused ) {
			focused.setFocus( true );
		}

		if ( focusDownSequence != null ) {
			for ( short i = 0; i < focusDownSequence.length; ++i ) {
				if ( focusDownSequence[ i ] == focused ) {
					focusDownIndex = i;
					break;
				}
			}
		} else {
			focusDownIndex = -1;
		}

		if ( focusRightSequence != null ) {
			for ( short i = 0; i < focusRightSequence.length; ++i ) {
				if ( focusRightSequence[ i ] == focused ) {
					focusRightIndex = i;
					break;
				}
			}
		} else {
			focusRightIndex = -1;
		}

		scrollToComponent( focused );
	}


    /**
     * Makes sure the component is visible in the scroll if this container 
     * is scrollable
     * 
     * @param c the component to be visible
 	 */
	public void scrollToComponent( Component c ) {
		if ( c != null ) {
			Container p = c.getParent();
			while ( p != null ) {
				if ( p.isScrollable() )
					p.scrollToComponent( c );
				
				// faz o scroll até a raiz, para garantir que os pais também estejam visíveis
				p = p.getParent();
			}
		}
	}
	
	
    /**
     * Request focus for a form child component
     * 
     * @param cmp the form child component
     */
    public void requestFocus( Component cmp ) {
        if ( cmp.isFocusable() && contains( cmp ) ) {
            setFocused( cmp );
        }
    }	
	

    /**
     * Returns the current focus component for this form
     * 
     * @return the current focus component for this form
     */
    public Component getFocused() {
        return focused;
    }


	//#if TOUCH == "true"
		public TouchKeyPad getTouchKeyPad() {
			return touchKeyPad;
		}


		public void setTouchKeyPad( TouchKeyPad touchKeyPad ) {
			this.touchKeyPad = touchKeyPad;
		}
	//#endif
	
	
	protected void initFocus() {
		if ( !focusVectorInitialized )
			updateFocusVectors();		
		
		// ao trocar de tela, o novo ContentPane é definido, e um novo componente ganha o foco. Porém, esse
		// novo componente em foco não deve receber o evento keyReleased, por não ter sido ele que originou o keyPressed
		if ( contentPane != null && focusDownSequence != null && focusDownSequence.length > 0 )
			setFocused( focusDownSequence[ 0 ] );
	}
	
	
	public void updateFocus( int key ) {
		Component currentFocus = getFocused();
		if ( !focusVectorInitialized )
			updateFocusVectors();
		
		switch ( key ) {
			case ScreenManager.DOWN: {
				if ( currentFocus.getNextFocusDown() != null ) {
					currentFocus = currentFocus.getNextFocusDown();
				} else {
					if ( focusDownSequence != null ) {
						int i = focusDownIndex;
						
						do {
							++i;
							if ( i == focusDownSequence.length )
								i = 0;
							
							final Component nextFocus = focusDownSequence[ i ];
							if ( nextFocus.isFocusable() ) {
								currentFocus = nextFocus;
								break;
							}
						} while ( i > focusDownIndex );
					}
				}
				break;
			}
			
			case ScreenManager.UP: {
				if ( currentFocus.getNextFocusUp() != null ) {
					currentFocus = currentFocus.getNextFocusUp();
				} else {
					if ( focusDownSequence != null ) {
						int i = focusDownIndex;
						
						do {
							--i;
							if ( i < 0 )
								i = focusDownSequence.length - 1;
							
							final Component nextFocus = focusDownSequence[ i ];
							if ( nextFocus.isFocusable() ) {
								currentFocus = nextFocus;
								break;
							}
						} while ( i < focusDownIndex );
					}
				}
				break;
			}
			
			case ScreenManager.RIGHT: {
				if ( currentFocus.getNextFocusRight() != null ) {
					currentFocus = currentFocus.getNextFocusRight();
				} else {
					if ( focusRightSequence != null ) {
						int i = focusRightIndex + 1;
						if ( i == focusRightSequence.length )
							i = 0;
						
						currentFocus = focusRightSequence[ i ];
					}
				}
				break;
			}
			
			case ScreenManager.LEFT: {
				if ( currentFocus.getNextFocusLeft() != null ) {
					currentFocus = currentFocus.getNextFocusLeft();
				} else {
					if ( focusRightSequence != null ) {
						int i = focusRightIndex - 1;
						if ( i < 0 )
							i = focusRightSequence.length - 1;

						currentFocus = focusRightSequence[ i ];
					}
				}
				break;
			}
			
			default:
				return;
		}
		setFocused( currentFocus );
	}
	

	public void updateFocusVectors() {
//		setFocused( null ); TODO necessário?
		clearFocusVectors();

		if ( contentPane != null ) {
			focusDownSequence = findAllFocusable( contentPane, false );
			focusRightSequence = findAllFocusable( contentPane, true );
		
			focusVectorInitialized = true;
		}
	}


	private final Component[] findAllFocusable( Container c, boolean toTheRight ) {
		final Vector v = new Vector();

		findAllFocusable( c, v, toTheRight );

		final Component[] ret = new Component[ v.size() ];
		for ( short i = 0; i < ret.length; ++i )
			ret[ i ] = ( Component ) v.elementAt( i );

		return ret;
	}


	/**
	 * Finds all focusable components in the hierarchy 
	 */
	private final void findAllFocusable( Container c, Vector v, boolean toTheRight ) {
		if ( c.isFocusable() ) {
			v.addElement( c );
		}
		
		final int count = c.getComponentCount();

		for ( short i = 0; i < count; ++i ) {
			Component current = c.getComponentAt( i );
			if ( current instanceof Container ) {
				findAllFocusable( ( Container ) current, v, toTheRight );
			} else if ( current.isFocusable() ) {
				v.addElement( current );
//				addSortedComponent( v, current, toTheRight );
			}
		}
	}


	/**
	 * @inheritDoc
	 */
	public void keyPressed( int key ) {
		// ao segurar o botão de confirmação num botão e apertar uma seta (mudando de componente), soltar o botão não deve mais confirmar o botão inicial
		keyPressed = true;
		
		if ( focused != null ) {
			// o evento é repassado antes de se testar focused.handlesInput() para que uma possível alteração
			// em setHandlesInput() já seja refletida aqui. Ex.: pressionar para baixo num campo de texto
			final Component f = focused;
			f.keyPressed( key );
			if ( !f.handlesInput() ) {
				// atualiza o foco
				updateFocus( key );
			}
		} else {
			initFocus();
		}
	}

	
	/**
	 * @inheritDoc
	 */
	public void keyReleased( int key ) {
		// TODO é necessário tratar casos onde o form pai do componente não é o form que recebe eventos?
		if ( keyPressed && focused != null && focused.getComponentForm() == this ) {
			focused.keyReleased( key );
		}
	}
	

	//#if TOUCH == "true"
		/**
		 * @inheritDoc
		 */
		public void onPointerPressed( int x, int y ) {
			final Point diff = new Point();
			final Component c = getComponentAt( x, y, diff );

			if ( getStatusBar() instanceof TouchKeyPad ) {
				x -= diff.x;
				y -= diff.y;

				// tratamento especial para o caso do teclado touchscreen estar visível
				if ( c == getStatusBar() ) {
					touchKeyPad.onPointerPressed( x, y );
					return;
				} else if ( c == touchKeyPad.getTextBox() ) {
					c.onPointerPressed( x, y );
					return;
				} else {
					hideTouchKeyPad();
				}
			} 
			
			if ( c != null ) {
				x -= diff.x;
				y -= diff.y;

				if ( c.isFocusable() ) {
					// se o usuário clicar num textbox ativo, mostra o teclado de touchscreen
					if ( touchKeyPad != null && c.isEnabled() && c instanceof TextBox ) {
						touchKeyPad.setTextBox( ( TextBox ) c );
						if ( getStatusBar() != touchKeyPad ) {
							touchKeyPad.refreshSize( getWidth(), getHeight() );
							setStatusBar( touchKeyPad, true );
						}

						setFocused( c );
						return;
					}

					setFocused( c );
				}

                if ( c != this )
                    c.onPointerPressed( x, y );
                
				setDraggedComponent( c );
			}
		}


		/**
		 * @inheritDoc
		 */
		public void onPointerDragged( int x, int y ) {
			if ( getStatusBar() instanceof TouchKeyPad ) {
				touchKeyPad.onPointerDragged( x, y );
			} else {
				if ( dragged == null ) {
					final Point diff = new Point();
					final Component c = getComponentAt( x, y, diff );

					if ( c != null ) {
						x -= diff.x;
						y -= diff.y;

						if ( c.isFocusable() )
							setFocused( c );

                        if ( c != this )
                            c.onPointerDragged( x, y );
					}
				} else if ( dragged != this ) {
					final Point diff = new Point();
					dragged.getAbsolutePos( diff );
					// gambiarra: como a posição absoluta soma a posição do drawable e ess
					diff.subEquals( dragged.getPosition() );

					dragged.onPointerDragged( x - diff.x, y - diff.y );
				}
			}
		}


		/**
		 * @inheritDoc
		 */
		public void onPointerReleased( int x, int y ) {
			if ( getStatusBar() instanceof TouchKeyPad ) {
				touchKeyPad.onPointerReleased( x, y );
			} else {
				if ( dragged == null ) {
					final Point diff = new Point();
					final Component c = getComponentAt( x, y, diff );

					if ( c != null ) {
						x -= diff.x;
						y -= diff.y;

						if ( c.isFocusable() )
							setFocused( c );

                        if ( c != this )
                            c.onPointerReleased( x, y );
                        
						setDraggedComponent( c );
					}
				} else if ( dragged != this ) {
					final Point diff = new Point();
					dragged.getAbsolutePos( diff );
					diff.subEquals( dragged.getPosition() );

					dragged.onPointerReleased( x - diff.x, y - diff.y );
					setDraggedComponent( null );
				}
			}
		}
	//#endif

	
	public void hideNotify( boolean deviceEvent ) {
	}

	
	public void showNotify( boolean deviceEvent ) {
	}

	
	public void sizeChanged( int width, int height ) {
		setAutoCalcPreferredSize( true );

		//#if TOUCH == "true"
			if ( touchKeyPad != null )
				touchKeyPad.refreshSize( width, height );
		//#endif

		setSize( width, height );
	}

}
