/*
 * Alert.java
 *
 * Created on April 9, 2010, 5:07 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class Alert extends UpdatableGroup implements KeyListener, ScreenListener
		//#if TOUCH == "true"
			, PointerListener
		//#endif
{

	/** Tipo de alerta: alarme. */
	public static final byte TYPE_ALARM			= 0;

	/** Tipo de alerta: confirmação. */
	public static final byte TYPE_CONFIRMATION	= 1;

	/** Tipo de alerta: erro. */
	public static final byte TYPE_ERROR			= 2;

	/** Tipo de alerta: informação. */
	public static final byte TYPE_INFO			= 3;

	/** Tipo de alerta: aviso. */
	public static final byte TYPE_WARNING		= 4;

	private static final short TIME_TRANSITION = 500;

	private static final byte STATE_HIDDEN = 0;

	private static final byte STATE_APPEARING = 1;

	private static final byte STATE_SHOWN = 2;

	private static final byte STATE_HIDING = 3;

	private byte state;

	private short accTime;

	private Drawable screen;

	private boolean drawScreen;

	private final Button[] buttons;

	private final RichLabel text;

	private final Drawable border;

	private final byte type;

	private byte currentIndex;

	private MenuListener listener;

	private int id;

	private Button dragged;


	public Alert( byte type, String text, Button[] buttons, ImageFont font, Drawable border ) throws Exception {
		super( 10 );

		this.type = type;

		this.border = border;
		insertDrawable( border );

		this.text = new RichLabel( font, "<ALN_H>" + text );
		insertDrawable( this.text );

		this.buttons = new Button[ buttons.length ];
		final EventListener eventListener = new EventListener() {
			public final void eventPerformed( Event evt ) {
				switch ( evt.eventType ) {
					case Event.EVT_BUTTON_CONFIRMED:
						setState( STATE_HIDING );
						currentIndex = ( byte ) evt.source.getId();
					break;
				}
			}
		};
		for ( byte i = 0; i < buttons.length; ++i ) {
			this.buttons[ i ] = buttons[ i ];
			buttons[ i ].setId( i );
			buttons[ i ].addEventListener( eventListener );
			insertDrawable( buttons[ i ] );
		}

		setViewport( new Rectangle() );

		autoSize();
	}


	public void setListener( MenuListener listener, int id ) {
		this.listener = listener;
		this.id = id;
	}


	public void autoSize() {
		final int w = ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2;

		text.setSize( w * 9 / 10, 0 );
		text.formatText( false );
		text.setSize( text.getWidth(), text.getTextTotalHeight() + text.getFont().getHeight() );
		text.setPosition( ( w - text.getWidth() ) >> 1, text.getFont().getHeight() );

		setSize( w, text.getPosY() + text.getHeight() + text.getFont().getHeight() + ( buttons[ 0 ].getHeight() ) );
	}


	public final void show() {
		final Drawable currentScreen = ScreenManager.getInstance().getCurrentScreen();
		if ( currentScreen != this ) {
			if ( currentScreen instanceof Alert && ( ( Alert ) currentScreen ).getState() != STATE_HIDDEN ) {
				// se a tela for um Alert, primeiro encerra o anterior, copia seu screen e então mostra esse alerta
				final Alert previous = ( Alert ) currentScreen;
				screen = previous.screen;
				previous.screen = this;
				previous.setState( STATE_HIDING );
			} else {
				screen = currentScreen;
				ScreenManager.getInstance().setCurrentScreen( this );
				setState( STATE_APPEARING );
			}
		}
	}


	public final void update( int delta ) {
		super.update( delta );

		switch ( state ) {
			case STATE_APPEARING:
				accTime += delta;
				setVisibleArea( accTime * 100 / TIME_TRANSITION );
				if ( accTime >= TIME_TRANSITION ) {
					setState( STATE_SHOWN );
				}
			break;
			
			case STATE_HIDING:
				accTime -= delta;
				setVisibleArea( accTime * 100 / TIME_TRANSITION );
				if ( accTime <= 0 ) {
					setState( STATE_HIDDEN );
				}
			break;
		}
	}


	public final byte getState() {
		return state;
	}


	public final void setState( int state ) {
		this.state = ( byte ) state;

		switch ( state ) {
			case STATE_HIDDEN:
				setVisibleArea( 0 );
				if ( screen instanceof Alert )
					( ( Alert ) screen ).setState( STATE_APPEARING );

				ScreenManager.getInstance().setCurrentScreen( screen );

				if ( listener != null )
					listener.onChoose( null, id, currentIndex );
			break;

			case STATE_APPEARING:
				accTime = 0;
				setVisibleArea( 0 );
			break;

			case STATE_SHOWN:
				accTime = TIME_TRANSITION;
				setVisibleArea( 100 );
				break;

			case STATE_HIDING:
				break;
		}
	}


	public final void setVisibleArea( int percent ) {
		percent = NanoMath.clamp( percent, 0, 100 );

		final int width = getWidth() * percent / 100;
		final int height = getHeight() * percent / 100;
		viewport.set( getRefPixelX() - ( width >> 1 ), getRefPixelY() - ( height >> 1 ), width, height );
	}


	public void keyPressed( int key ) {
		if ( state == STATE_SHOWN ) {
			switch ( key ) {
				case ScreenManager.KEY_NUM4:
				case ScreenManager.LEFT:
					setCurrentIndex( currentIndex - 1 );
				break;

				case ScreenManager.KEY_NUM6:
				case ScreenManager.RIGHT:
					setCurrentIndex( currentIndex + 1 );
				break;

				default:
					buttons[ currentIndex ].keyPressed( key );
			}
		}
	}


	public void keyReleased( int key ) {
		if ( state == STATE_SHOWN ) {
			buttons[ currentIndex ].keyReleased( key );
		}
	}


	private final void setCurrentIndex( int index ) {
		index = NanoMath.clamp( index, 0, buttons.length - 1 );
		buttons[ currentIndex ].setFocus( false );
		buttons[ index ].setFocus( true );

		currentIndex = ( byte ) index;
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
		drawScreen = true;
	}


	public void sizeChanged( int width, int height ) {
		autoSize();
	}


	public void setSize( int width, int height ) {
		super.setSize( width, height );

		short spacing = 0;
		for ( byte i = 0; i < buttons.length; ++i ) {
			spacing += buttons[ i ].getWidth();
		}
		spacing = ( short ) ( ( width - spacing ) / ( buttons.length + 1 ) );

		for ( int i = 0, posX = spacing; i < buttons.length; ++i ) {
			buttons[ i ].setPosition( posX, text.getPosY() + text.getHeight() );
			posX += spacing + buttons[ i ].getWidth();
		}
		border.setSize( size );

		defineReferencePixel( ANCHOR_HCENTER | ANCHOR_VCENTER );
		setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, ScreenManager.SCREEN_HALF_HEIGHT );
	}


	public void draw( Graphics g ) {
		switch ( state ) {
			case STATE_HIDDEN:
				if ( screen != null )
					screen.draw( g );
			return;

			case STATE_APPEARING:
			case STATE_SHOWN:
				if ( drawScreen ) {
					screen.draw( g );
					drawScreen = false;
				}
			break;

			case STATE_HIDING:
				if ( screen != null )
					screen.draw( g );
			break;
		}

		super.draw( g );
	}


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			x -= getPosX();
			y -= getPosY();

			if ( dragged != null ) {
				dragged.onPointerDragged( x, y );
			}
		}


		public void onPointerPressed( int x, int y ) {
			x -= getPosX();
			y -= getPosY();

			for ( byte i = 0; i < buttons.length; ++i ) {
				if ( buttons[ i ].contains( x, y ) ) {
					dragged = buttons[ i ];
					dragged.onPointerPressed( x, y );
					return;
				}
			}
		}


		public void onPointerReleased( int x, int y ) {
			x -= getPosX();
			y -= getPosY();
			
			if ( dragged != null ) {
				dragged.onPointerReleased( x, y );
				dragged = null;
			}
		}

	//#endif

}
