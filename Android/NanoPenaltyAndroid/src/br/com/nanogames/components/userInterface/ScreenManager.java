/*
 * ScreenManager.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import android.content.Context;
import android.util.AttributeSet;
import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import java.util.Hashtable;

//#if J2SE == "false"
import javax.microedition.lcdui.Canvas;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.GameCanvas;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.event.MouseEvent;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//#endif

//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.system.Application;
//# import net.rim.device.api.system.KeypadListener;
//# import net.rim.device.api.system.TrackwheelListener;
//# import net.rim.device.api.ui.Keypad;
//#endif

//#if SHOW_FPS == "true"
//# import br.com.nanogames.components.util.NanoMath;
//# import javax.microedition.lcdui.Font;
//#endif

//#if SHOW_FREE_MEM == "true"
//# import javax.microedition.lcdui.Font;
//#endif


/**
 *
 * @author peter
 */
public final class ScreenManager extends
        //#if J2SE == "false"
			//#if GAMECANVAS == "true"
//# 				GameCanvas
			//#else
				Canvas
			//#endif
        //#else
//#             JFrame
        //#endif

        implements Runnable
//#if BLACKBERRY_API == "true"
//# 	, net.rim.device.api.system.KeyListener, TrackwheelListener
//#elif J2SE == "true"
//# 		, java.awt.event.KeyListener, MouseListener, MouseMotionListener
//#endif
{

	private boolean paused;
	
	private boolean running;
	 
	private Thread thread;
	 
	/** tempo em milisegundos decorridos no último frame */
	private int lastFrameTime;	
 
	/** Se a tela atual for atualizável, armazena uma referência já com o typecast, para acelerar futuros acessos. */
	private Updatable currentScreenUpdatable;
	 
	/** Listener dos eventos de teclas (pode ser null). */
	private static KeyListener keyListener;

	//#if TOUCH == "true"
		/** Listener dos eventos de ponteiro (caneta stylus) (pode ser null). */
		private static PointerListener pointerListener;
	//#endif
	
	/** objeto que recebe os eventos showNotify/hideNotify (pode ser null) */
	private static ScreenListener screenListener;
	 
	/** cor padrão de preenchimento do background */
	public static final int DEFAULT_BG_COLOR = 0x000000;
	
	/** cor de preenchimento do background */
	private int bgColor = DEFAULT_BG_COLOR;
	
	/** Drawable de fundo (caso seja nulo, o fundo é preenchido com a cor de fundo definida) */
	private Drawable background;
	/** referência para o drawable de fundo, caso ele seja atualizável */
	private Updatable updatableBackground;
	
	/** Referência para a tela atual. */
	private Drawable currentScreen;
	
	// "labels" dos softkeys (drawables que indicam as ações dos softkeys esquerdo, central e direito)
	private final Drawable[] softKeys = new Drawable[ 3 ];
	private final Updatable[] softKeysUpdatables = new Updatable[ 3 ];
	
	// índices dos softkeys
	/** Índice do soft key esquerdo (valor utilizado para definir o drawable do soft key, não o índice da tecla). */
	public static final byte SOFT_KEY_LEFT	= 0;
	/** Índice do soft key central (valor utilizado para definir o drawable do soft key, não o índice da tecla). */
	public static final byte SOFT_KEY_MID	= 1;
	/** Índice do soft key direito (valor utilizado para definir o drawable do soft key, não o índice da tecla). */
	public static final byte SOFT_KEY_RIGHT	= 2;
	 
	/** Tempo máximo de um frame. Caso o intervalo de atualização seja maior, ele é reduzido a esse valor, impedindo
	 * que intervalos muito grandes de atualização causem inconsistência (como não detecção de colisão entre objetos
	 * cujas trajetórias se cruzaram nesse intervalo, por exemplo).
	 */
	private final int MAX_FRAME_TIME;
	 
	// teclas especiais
	public static final int KEY_SOFT_LEFT		= 0xff0001;
	public static final int KEY_SOFT_RIGHT		= 0xff0002;
	public static final int KEY_CLEAR			= 0xff0003;
	public static final int KEY_SOFT_MID		= 0xff0004;
	public static final int KEY_BACK			= 0xff0005;
	
	// apenas para manter o padrão KEY_TECLA
	public static final int KEY_FIRE			= FIRE;
	public static final int KEY_RIGHT			= RIGHT;
	public static final int KEY_LEFT			= LEFT;
	public static final int KEY_DOWN			= DOWN;
	public static final int KEY_UP				= UP;
	
	/** Hashtable que contém o mapeamento de códigos de teclas para suas funções, como soft keys, apagar, voltar, etc. */
	public static final Hashtable SPECIAL_KEYS_TABLE = new Hashtable();
	
	
	//<editor-fold defaultstate="collapsed" desc="CONSTANTES DAS TECLAS DE CADA FABRICANTE">
	
	// softkey esquerdo (todos os aparelhos possuem)
	private static final byte KEY_SOFT_LEFT_GENERIC			= -6;
	private static final byte KEY_SOFT_LEFT_LG				= KEY_SOFT_LEFT_GENERIC;
	private static final short KEY_SOFT_LEFT_LG_2			= -202;
	private static final byte KEY_SOFT_LEFT_NOKIA			= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_MOTOROLA		= -21;
	private static final byte KEY_SOFT_LEFT_MOTOROLA_2		= 21;
	private static final byte KEY_SOFT_LEFT_SAMSUNG			= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_SONYERICSSON	= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_SIEMENS			= -1;
	private static final byte KEY_SOFT_LEFT_SIEMENS_2		= 105;
	private static final byte KEY_SOFT_LEFT_SAGEM_GRADIENTE	= -7; // igual à soft key direita dos SonyEricsson
	private static final int  KEY_SOFT_LEFT_INTENT_JTE		= 57345;
	private static final int  KEY_SOFT_LEFT_INTENT_JTE_2	= -KEY_SOFT_LEFT_INTENT_JTE;
	private static final byte KEY_SOFT_LEFT_INTELBRAS		= KEY_SOFT_LEFT_GENERIC;
	private static final byte KEY_SOFT_LEFT_BLACKBERRY		= 17; // Keypad.KEY_SEND (4.1 não possui essa constante, por isso o "número magico")

	// softkey direito (todos os aparelhos possuem)
	private static final byte KEY_SOFT_RIGHT_GENERIC		= -7;
	private static final byte KEY_SOFT_RIGHT_LG				= KEY_SOFT_RIGHT_GENERIC;
	private static final short KEY_SOFT_RIGHT_LG_2			= -203;
	private static final byte KEY_SOFT_RIGHT_NOKIA			= KEY_SOFT_RIGHT_GENERIC;
	private static final byte KEY_SOFT_RIGHT_MOTOROLA		= -22;
	private static final byte KEY_SOFT_RIGHT_MOTOROLA_2		= 22;
	private static final byte KEY_SOFT_RIGHT_SAMSUNG		= KEY_SOFT_RIGHT_GENERIC;
	private static final byte KEY_SOFT_RIGHT_SONYERICSSON	= KEY_SOFT_RIGHT_GENERIC;
	private static final byte KEY_SOFT_RIGHT_SIEMENS		= -4;
	private static final byte KEY_SOFT_RIGHT_SIEMENS_2		= 106;
	private static final byte KEY_SOFT_RIGHT_SAGEM_GRADIENTE= KEY_SOFT_LEFT_SONYERICSSON; // igual à soft key esquerda dos SonyEricsson
	private static final int  KEY_SOFT_RIGHT_INTENT_JTE		= 57346;
	private static final int  KEY_SOFT_RIGHT_INTENT_JTE_2	= -KEY_SOFT_RIGHT_INTENT_JTE;
	private static final byte KEY_SOFT_RIGHT_INTELBRAS		= KEY_SOFT_RIGHT_GENERIC;
	
	// softkey central (somente alguns aparelhos possuem)
	private static final byte KEY_SOFT_MID_MOTOROLA			= -23;
	private static final byte KEY_SOFT_MID_MOTOROLA_2		= 23;
	private static final byte KEY_SOFT_MID_BLACKBERRY		= 19;
	private static final short KEY_SOFT_MID_BLACKBERRY_2	= 4098; // Keypad.KEY_MENU; (4.1 não possui essa constante, por isso o "número magico")

	// tecla de clear (somente alguns aparelhos possuem)
	private static final byte KEY_CLEAR_GENERIC				= -8;
	private static final byte KEY_CLEAR_LG					= -16;
	private static final short KEY_CLEAR_LG_2				= -204;
	private static final byte KEY_CLEAR_LG_3				= 11; // TODO deviceInfo informa 11 p/ ME970 e KP210, mas na prática não funciona... (mesmo valor de GAME_C...)
	private static final byte KEY_CLEAR_NOKIA				= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_MOTOROLA			= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_SAMSUNG				= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_SONYERICSSON		= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_SIEMENS				= -12;
	private static final short KEY_CLEAR_SIEMENS_2			= -12345;
	private static final byte KEY_CLEAR_SAGEM				= KEY_CLEAR_SONYERICSSON;
	private static final byte KEY_CLEAR_INTENT_JTE			= KEY_CLEAR_GENERIC;
	private static final byte KEY_CLEAR_BLACKBERRY			= 127;
	private static final byte KEY_CLEAR_BLACKBERRY_2		= 8; // tecla DEL (código igual ao FIRE)
	
	// tecla específica para voltar (somente alguns aparelhos possuem)
	private static final byte KEY_BACK_SONYERICSSON			= -11;
	private static final byte KEY_BACK_SAGEM				= -KEY_BACK_SONYERICSSON;
	private static final byte KEY_BACK_INTENT_JTE			= 27;
	private static final byte KEY_BACK_BLACKBERRY			= 27;
	
	// outras teclas
	private static final byte KEY_FIRE_BLACKBERRY			= -8;
	private static final byte KEY_ENTER_BLACKBERRY			= 10;
	private static final int KEY_SEND_BLACKBERRY			= 0xFF0001; // botão verde
	private static final int KEY_MENU_BLACKBERRY			= 0xFF0004; // tecla dos "quadradinhos"

	//#if BLACKBERRY_API == "true"
//# 		private static final byte KEY_END_BLACKBERRY			= Keypad.KEY_END; // botão vermelho
//# 		private static final byte KEY_CONVENIENCE_LEFT_BLACKBERRY	= 21; // botão de comando de voz à esquerda
//# 
//# 		private static final short KEY_VOLUME_UP_BLACKBERRY		= 4096; // Keypad.KEY_VOLUME_UP (4.1 não possui essa constante, por isso o "número magico")
//# 		private static final short KEY_VOLUME_DOWN_BLACKBERRY	= 4097; // Keypad.KEY_VOLUME_DOWN (4.1 não possui essa constante, por isso o "número magico")
//# 
//# 		private static final int STATUS_TRACKWHEEL = 1073741824; // KeypadListener.STATUS_TRACKWHEEL
	//#endif

	//#if J2SE == "true"
//# 		public static final byte DOWN		= 6;
//# 		public static final byte FIRE		= 8;
//# 		public static final byte GAME_A 	= 9;
//# 		public static final byte GAME_B 	= 10;
//# 		public static final byte GAME_C 	= 11;
//# 		public static final byte GAME_D 	= 12;
//# 		public static final byte KEY_NUM0 	= 48;
//# 		public static final byte KEY_NUM1 	= 49;
//# 		public static final byte KEY_NUM2 	= 50;
//# 		public static final byte KEY_NUM3 	= 51;
//# 		public static final byte KEY_NUM4 	= 52;
//# 		public static final byte KEY_NUM5 	= 53;
//# 		public static final byte KEY_NUM6 	= 54;
//# 		public static final byte KEY_NUM7 	= 55;
//# 		public static final byte KEY_NUM8 	= 56;
//# 		public static final byte KEY_NUM9 	= 57;
//# 		public static final byte KEY_POUND 	= 35;
//# 		public static final byte KEY_STAR 	= 42;
//# 		public static final byte LEFT		= 2;
//# 		public static final byte RIGHT		= 5;
//# 		public static final byte UP			= 1;
	//#endif
	
	//</editor-fold>

	// as constantes de tecla dos BlackBerry são armazenadas aqui pois algumas das constantes não estão definidas em
	// versões de software mais antigas, como a 4.1
	public static final int HW_LAYOUT_REDUCED		= 1364346180; // Keypad.HW_LAYOUT_REDUCED;
	public static final int HW_LAYOUT_REDUCED_24	= 1364341300; // Keypad.HW_LAYOUT_REDUCED_24;
	public static final int HW_LAYOUT_32			= 1364669234; // Keypad.HW_LAYOUT_32;
	public static final int HW_LAYOUT_39			= 1364669241; // Keypad.HW_LAYOUT_39;
	public static final int HW_LAYOUT_LEGACY		= 1295594807; // Keypad.HW_LAYOUT_LEGACY;
	public static final byte HW_LAYOUT_NUM_ROW		= 0;		  // Keypad.HW_LAYOUT_NUM_ROW;
	public static final int HW_LAYOUT_PHONE			= 1179602501; // Keypad.HW_LAYOUT_PHONE;
	public static final int HW_LAYOUT_SIEMENS_NEO	= 1179534671; // Keypad.HW_LAYOUT_SIEMENS_NEO;
	
	/** Instância do gerenciador de telas. */
	private static ScreenManager instance;
	
	// dimensões da tela. Os valores referentes à tela cheia só são preenchidos no momento da primeira visualização da 
	// tela, ou seja, após a primeira chamada ao método paint().
	/** Largura da tela. */
	public static int SCREEN_WIDTH;
	/** Posição horizontal central da tela. */
	public static int SCREEN_HALF_WIDTH;
	/** Altura da tela. */
	public static int SCREEN_HEIGHT;
	/** Posição vertical central da tela. */
	public static int SCREEN_HALF_HEIGHT;
	
	/** Área da tela efetivamente visível. */
	public static final Rectangle viewport = new Rectangle();
	
	// variáveis utilizadas somente no modo SHOW_FREE_MEM, que mostra na tela a memória heap livre
	//#if SHOW_FREE_MEM == "true"
//# 	// intervalo de atualização da memória livre em milisegundos
//# 	private static final short SHOW_FREE_MEM_UPDATE_INTERVAL = 150;
//# 	// tempo restante até a próxima atualização da memória livre
//# 	private int timeToNextUpdate;
//# 	// memória livre
//# 	private String freeMem = "";
	//#endif
	
	
	// variáveis utilizadas somente no modo SHOW_FPS, que mostra a taxa de quadros por segundo
	//#if SHOW_FPS == "true"
//# 		private static final int FP_FPS_UPDATE_INTERVAL = NanoMath.ONE;
//# 
//# 		private int fp_accFpsTime;
//# 
//# 		private short frameCounter;
//# 
//# 		private String strFps = "";
	//#endif

	/** Back buffer de desenho da tela. */
	//#if GAMECANVAS == "true"
//# 		private Graphics screenBuffer;
	//#endif

	//#if J2SE == "true"
//# 		private BufferedImage backBuffer;
	//#endif

	private final Mutex mutex = new Mutex();

	/** Indica se o método flushGraphics deve ser usado. O valor é obtido a partir da propriedade do jad NANO_FLUSH_GRAPHICS.
	 * De maneira geral, aparelhos Blackberry e LG novos (ME770, ME970 e mais recentes) devem usar essa propriedade para
	 * evitar flickering no redesenho da tela.
	*/
	private final boolean useFlushGraphics;
	

	/**
	 * Cria uma nova instância do gerenciador de telas. Ele somente deve ser definido como a tela ativa do aparelho
	 * posteriormente, ao iniciar sua thread. Defini-lo como tela ativa no construtor pode causar problemas em alguns
	 * aparelhos, como o Gradiente GF690, que eventualmente exibia somente uma tela preta ao iniciar a aplicação.
	 * 
	 * @param maxFrameTime
	 */
	protected ScreenManager( int maxFrameTime ) {
        //#if J2SE == "false"
            // essa chamada é necessária aqui para garantir que o buffer utilize a tela cheia (sem a chamada, Nokia S60 3rd Edition
            // utilizam tela parcial, por exemplo)
			//#if GAMECANVAS == "true"
//# 				super( false );
			//#endif
				
            //#if JAVA_VERSION == "ANDROID"
            super();
            //#endif
        
			setFullScreenMode( true );

			//#if GAMECANVAS == "true"
//# //				screenBuffer = getGraphics();
			//#endif

            //#if BLACKBERRY_API == "true"
//#     			Application.getApplication().addKeyListener( this );
//#     			Application.getApplication().addTrackwheelListener( this );
            //#endif
		//#else
//# 			addKeyListener( this );
//# 			addMouseListener( this );
//# 			addMouseMotionListener( this );
//# 			setResizable( false );
//# 			setDefaultCloseOperation( EXIT_ON_CLOSE );
//# 			setVisible( true );
		//#endif

        MAX_FRAME_TIME = maxFrameTime;

//		final String flush = AppMIDlet.getProperty( "NANO_FLUSH_GRAPHICS" );
		//#if BLACKBERRY_API == "true"
//# 			useFlushGraphics = flush == null || flush.equals( "true" );
		//#elif GAMECANVAS == "true"
//# //			useFlushGraphics = flush != null && flush.equals( "true" );
		//#else
			useFlushGraphics = false;
		//#endif
	}


    //#if JAVA_VERSION == ""
    public ScreenManager( Context context, AttributeSet attr ) {
        this( 100 );
    }
    
	public static final ScreenManager createInstance() {
        return createInstance( AppMIDlet.getInstance().MAX_FRAME_TIME );
    }    
    //#endif
    
	/**
	 * Cria uma nova instância do gerenciador de telas.
	 *
	 * @param maxFrameTime
	 * @return
	 */
	public static final ScreenManager createInstance( int maxFrameTime ) {
		instance = new ScreenManager( maxFrameTime );

		//#if J2SE == "false"
			// obtém as dimensões da tela - nesse ponto, os valores podem não referir-se à tela cheia; para se obter os valores
			// referentes à tela cheia, somente após a primeira chamada ao método paint().
			instance.setFullScreenMode( true );

			//#if GAMECANVAS == "true"
//# 			//	instance.sizeChanged( instance.screenBuffer.getClipWidth(), instance.screenBuffer.getClipHeight() );
			//#endif
		//#endif
			
		resetSpecialKeysTable();

		return instance;
	} // fim do método createInstance( boolean, int )


	public static final void resetSpecialKeysTable() {
		SPECIAL_KEYS_TABLE.clear();

		// TODO evitar alocação de vários Integer para os mesmos valores

		// adiciona o 0 para evitar quebra de alguns aparelhos caso seja chamado getGameAction( 0 ), dentro de getSpecialKey(int)
		SPECIAL_KEYS_TABLE.put( new Integer( 0 ), new Integer( 0 ) );
		SPECIAL_KEYS_TABLE.put( new Integer( FIRE ), new Integer( FIRE ) );
		SPECIAL_KEYS_TABLE.put( new Integer( UP ), new Integer( UP ) );
		SPECIAL_KEYS_TABLE.put( new Integer( DOWN ), new Integer( DOWN ) );
		SPECIAL_KEYS_TABLE.put( new Integer( LEFT ), new Integer( LEFT ) );
		SPECIAL_KEYS_TABLE.put( new Integer( RIGHT ), new Integer( RIGHT ) );

		// adiciona as teclas de 0 a 9, para evitar que sejam capturadas por getGameAction(), dentro de getSpecialKey(),
		// em especial evitando que sejam associadas aos valores GAME_A, GAME_B, GAME_C e GAME_D (como ocorre nos
		// aparelhos Motorola, por exemplo). Não é necessário adicioná-las, já que há um switch capturando-as antes
		// de verificar os valores da hash table, mas é importante no caso de chamadas diretas a getSpecialKey
		for ( int i = KEY_NUM0; i <= KEY_NUM9; ++i )
			SPECIAL_KEYS_TABLE.put( new Integer( i ), new Integer( i ) );

		// adiciona também os soft keys, para que não haja erro no repasse de eventos em pointerPressed (caso contrário,
		// o valor repassado nao seria reconhecido como o de um soft key)
		SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT ), new Integer( KEY_SOFT_LEFT ) );
		SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_MID ), new Integer( KEY_SOFT_MID ) );
		SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT ), new Integer( KEY_SOFT_RIGHT ) );
		SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR ), new Integer( KEY_CLEAR ) );

		SPECIAL_KEYS_TABLE.put( new Integer( KEY_STAR ), new Integer( KEY_STAR ) );
		SPECIAL_KEYS_TABLE.put( new Integer( KEY_POUND ), new Integer( KEY_POUND ) );

		//#if J2SE == "false"
			// preenche a tabela com os índices específicos de cada fabricante
			switch ( AppMIDlet.getVendor() ) {
				case AppMIDlet.VENDOR_LG:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_LG ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_LG_2 ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_LG ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_LG_2 ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_LG ), new Integer( KEY_CLEAR ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_LG_2 ), new Integer( KEY_CLEAR ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_LG_3 ), new Integer( KEY_CLEAR ) );
				break;

				case AppMIDlet.VENDOR_NOKIA:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_NOKIA ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_NOKIA ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_NOKIA ), new Integer( KEY_CLEAR ) );
				break;

				case AppMIDlet.VENDOR_MOTOROLA:
					// TODO dá erro... key down é -6 em alguns Motorolas malditos... SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_GENERIC ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_MOTOROLA ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_MOTOROLA_2 ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_MID_MOTOROLA ), new Integer( KEY_SOFT_MID ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_MID_MOTOROLA_2 ), new Integer( KEY_SOFT_MID ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_MOTOROLA ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_MOTOROLA_2 ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_GENERIC ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_MOTOROLA ), new Integer( KEY_CLEAR ) );
				break;

				case AppMIDlet.VENDOR_SIEMENS:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_SIEMENS ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_SIEMENS_2 ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_SIEMENS ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_SIEMENS_2 ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_SIEMENS ), new Integer( KEY_CLEAR ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_SIEMENS_2 ), new Integer( KEY_CLEAR ) );
				break;

				case AppMIDlet.VENDOR_SAMSUNG:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_SAMSUNG ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_SAMSUNG ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_SAMSUNG ), new Integer( KEY_CLEAR ) );
				break;

				case AppMIDlet.VENDOR_SONYERICSSON:
					/**
					 * Teclas ignoradas do SonyEricsson. Algumas das funções destas teclas são: controle de volume, browser,
					 * walkman, câmera, etc. Estas teclas são ignoradas pois podem causar congelamento em alguns aparelhos,
					 * uma vez que algumas não enviam eventos keyReleased ou então causam problemas em getSpecialKey.
					 * Exemplos: a tecla de browser do K700 e de volume no W600.
					 */
					// TODO adicionar códigos de teclas de volume (caso sejam de mesmo valor para vários aparelhos) e
					// adicionar listener para tratar mudanças no volume
					final byte[] KEY_IGNORED_SONYERICSSON = { -10, -12, -13, -14, -20, -21, -22, -23, -24, -25, -26, -27, -28, -34, -35, -36, -37 };
					for ( byte i = 0; i < KEY_IGNORED_SONYERICSSON.length; ++i )
						SPECIAL_KEYS_TABLE.put( new Integer( KEY_IGNORED_SONYERICSSON[ i ] ), new Integer( 0 ) );

					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_SONYERICSSON ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_SONYERICSSON ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_SONYERICSSON ), new Integer( KEY_CLEAR ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_BACK_SONYERICSSON ), new Integer( KEY_BACK ) );
				break;

				case AppMIDlet.VENDOR_HTC:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_INTENT_JTE ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_INTENT_JTE_2 ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_INTENT_JTE ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_INTENT_JTE_2 ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_INTENT_JTE ), new Integer( KEY_CLEAR ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_BACK_INTENT_JTE ), new Integer( KEY_BACK ) );
				break;

				case AppMIDlet.VENDOR_BLACKBERRY:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_BACK_BLACKBERRY ), new Integer( KEY_BACK ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_BLACKBERRY ), new Integer( KEY_CLEAR ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_FIRE_BLACKBERRY ), new Integer( FIRE ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_ENTER_BLACKBERRY ), new Integer( FIRE ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_MENU_BLACKBERRY ), new Integer( KEY_SOFT_MID ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SEND_BLACKBERRY ), new Integer( KEY_SOFT_LEFT ) );

					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_BLACKBERRY ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_MID_BLACKBERRY ), new Integer( KEY_SOFT_MID ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_MID_BLACKBERRY_2 ), new Integer( KEY_SOFT_MID ) );

					// teclas ignoradas: Trackball	-8, Volume Up	-150, Volume Down	-151, Convenience Key 1	-19, Convenience Key 2	-21
	//				final short[] KEY_IGNORED_BLACKBERRY = { -150, -151, -19, -21 }; TODO teste
	//				for ( byte i = 0; i < KEY_IGNORED_BLACKBERRY.length; ++i )
	//					SPECIAL_KEYS_TABLE.put( new Integer( KEY_IGNORED_BLACKBERRY[ i ] ), new Integer( 0 ) );
				break;

				case AppMIDlet.VENDOR_SAGEM_GRADIENTE:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_SAGEM_GRADIENTE ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_SAGEM_GRADIENTE ), new Integer( KEY_SOFT_RIGHT ) );
				break;

				case AppMIDlet.VENDOR_INTELBRAS:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_INTELBRAS ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_INTELBRAS ), new Integer( KEY_SOFT_RIGHT ) );
				break;

				default:
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_LEFT_GENERIC ), new Integer( KEY_SOFT_LEFT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_SOFT_RIGHT_GENERIC ), new Integer( KEY_SOFT_RIGHT ) );
					SPECIAL_KEYS_TABLE.put( new Integer( KEY_CLEAR_GENERIC ), new Integer( KEY_CLEAR ) );
			} // fim switch ( AppMIDlet.getVendor() )
		//#else
//# 			SPECIAL_KEYS_TABLE.put( new Integer( KeyEvent.VK_UP ), new Integer( UP ) );
//# 			SPECIAL_KEYS_TABLE.put( new Integer( KeyEvent.VK_DOWN ), new Integer( DOWN ) );
//# 			SPECIAL_KEYS_TABLE.put( new Integer( KeyEvent.VK_LEFT ), new Integer( LEFT ) );
//# 			SPECIAL_KEYS_TABLE.put( new Integer( KeyEvent.VK_RIGHT ), new Integer( RIGHT ) );
//# 			SPECIAL_KEYS_TABLE.put( new Integer( KeyEvent.VK_BACK_SPACE ), new Integer( KEY_CLEAR ) );
//# 			SPECIAL_KEYS_TABLE.put( new Integer( KeyEvent.VK_ENTER ), new Integer( FIRE ) );
		//#endif
	}


	//#if J2SE == "false"
		/**
		 * Tenta descobrir o fabricante do aparelho através dos seus códigos de teclas.
		 * @return indice do fabricante, conforme definido na class AppMIDlet. Caso não consiga detectá-lo pelas teclas,
		 * retorna AppMIDlet.VENDOR_GENERIC.
		 */
		public static final byte checkVendorByKeys() {
			// tenta descobrir se códigos das soft keys correspondem aos dos Motorola
			final String SOFT_WORD = "SOFT";

			try {
				if ( instance.getKeyName( KEY_SOFT_LEFT_MOTOROLA ).toUpperCase().indexOf( SOFT_WORD ) > -1 )
					return AppMIDlet.VENDOR_MOTOROLA;
			} catch ( Throwable e ) {
				try {
					if ( instance.getKeyName( KEY_SOFT_LEFT_MOTOROLA_2 ).toUpperCase().indexOf( SOFT_WORD ) > -1 )
						return AppMIDlet.VENDOR_MOTOROLA;
				} catch ( Throwable e1 ) {
				}
			}

			return AppMIDlet.VENDOR_GENERIC;
		}
	//#endif


	/**
	 * Define o Drawable a ser utilizado como indicador de um softkey.
	 * @param softKey índice do softkey a ser definido. Valores válidos: KEY_SOFT_LEFT, KEY_SOFT_MID e KEY_SOFT_RIGHT.
	 * @param d Drawable a ser utilizado como indicador do softkey. Caso seja nulo, o softkey anterior (caso exista) é removido.
	 *
	 */
	public final void setSoftKey( int softKey, Drawable d ) {
		try {
			softKeys[ softKey ] = d;
			if ( d instanceof Updatable )
				softKeysUpdatables[ softKey ] = ( Updatable ) d;
			else
				softKeysUpdatables[ softKey ] = null;
		} catch ( ArrayIndexOutOfBoundsException e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e , "69");
			//#endif

			return;
		}

		if ( d != null ) {
			// posiciona o softKey
			final Point size = d.getSize();

			switch ( softKey ) {
				case SOFT_KEY_LEFT:
					d.defineReferencePixel( 0, size.y );
					d.setRefPixelPosition( 0, SCREEN_HEIGHT );
				break;
				case SOFT_KEY_MID:
					d.defineReferencePixel( size.x >> 1, size.y );
					d.setRefPixelPosition( SCREEN_HALF_WIDTH, SCREEN_HEIGHT );
				break;
				case SOFT_KEY_RIGHT:
					d.defineReferencePixel( size.x, size.y );
					d.setRefPixelPosition( SCREEN_WIDTH, SCREEN_HEIGHT );
				break;
			} // fim switch ( softKey )
		} // fim if ( d != null )
	} // fim do método setSoftKey( int, Drawable )


	/**
	 * Troca a tela atual, podendo realizar uma animação de transição.
	 * @param screen nova tela a ser exibida. Métodos serão repassados a essa nova tela caso ela implemente pelo menos uma
	 * das seguintes interfaces:
	 * <ul>
	 * <li>KeyListener</li>
	 * <li>Updatable</li>
	 * <li>ScreenListener</li>
	 * </ul>
	 *
	 * @see KeyListener
	 * @see Updatable
	 */
	public final void setCurrentScreen( Drawable screen ) {
		currentScreen = screen;
		AppMIDlet.gc();

		// registra a tela atual como listener de atualização, teclas, ponteiro e/ou eventos de suspend
		if ( currentScreen instanceof Updatable )
			currentScreenUpdatable = ( Updatable ) currentScreen;
		else
			currentScreenUpdatable = null;

		if ( currentScreen instanceof KeyListener )
			keyListener = ( KeyListener ) currentScreen;
		else
			keyListener = null;

		//#if TOUCH == "true"
			if ( currentScreen instanceof PointerListener )
				pointerListener = ( PointerListener ) currentScreen;
			else
				pointerListener = null;
		//#endif

		// avisa a tela atual que ela sairá de foco
		if ( screenListener != null )
			screenListener.hideNotify( false );

		if ( currentScreen instanceof ScreenListener )
			screenListener = ( ScreenListener ) currentScreen;
		else
			screenListener = null;

		// avisa a nova tela que ela entrará em foco
		if ( screenListener != null )
			screenListener.showNotify( false );
	} // fim do método setCurrentScreen( Drawable, byte )


	/**
	 * Define a cor de preenchimento do fundo, utilizada quando não há Drawable definido como background.
	 * @param bgColor cor sólida usada para preenchimento do background. Valores negativos indicam para não preencher
	 * o fundo da tela com uma cor sólida, mesmo que não haja drawable definido como background.
	 */
	public final void setBackgroundColor( int bgColor ) {
		this.bgColor = bgColor;
	}


	/**
	 * Obtém a tecla especial associada a um código de tecla pressionada.
	 * @param keyCode índice da tecla pressionada.
	 * @return tipo de tecla especial associada, ou 0 (zero) caso não haja função especial associada a esse código.
	 */
	public static final int getSpecialKey( int keyCode ) {
		final Integer value = ( Integer ) SPECIAL_KEYS_TABLE.get( new Integer( keyCode ) );

		if ( value != null )
			return value.intValue();

		//#if J2SE == "false"
		// não foi encontrada tecla especial (soft keys, back, etc) associada à tecla. Retorna a ação de jogo associada
		// à tecla, se houver uma. Senão, retorna o código da própria tecla. O bloco try/catch é necessário pois evita
		// problemas com teclas especiais em alguns aparelhos (como as teclas de Web e SMS no Motorola)
		try {
			final int ret = instance.getGameAction( keyCode );
			return ret == 0 ? keyCode : ret;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e ,"70");
			//#endif

			return 0;
		}
		//#else
//# 			return 0;
		//#endif
	} // fim do método getSpecialKey( int )


	/**
	 * Evento enviado pelo aparelho, informando que o gerenciador de telas passou a ser a tela exibida pelo aparelho.
	 */
	protected synchronized final void showNotify() {
		//#if TRY_CATCH_ALL == "true"
		try {
		//#endif

		paused = false;
		notifyAll();

		start();

		if ( screenListener != null )
			screenListener.showNotify( true );

		//#if TRY_CATCH_ALL == "true"
		} catch ( Throwable e ) {
			AppMIDlet.log( e , "71");
		}
		//#endif				
	}


	/**
	 * Evento enviado pelo aparelho, informando que a tela ativa (no caso, o gerenciador de telas) não é mais a
	 * tela sendo exibida.
	 */
	protected synchronized final void hideNotify() {
	 	//#if TRY_CATCH_ALL == "true"
		try {
		//#endif

		paused = true;

		// é utilizado o bloco try/catch aqui pois pode ocorrer de, ao encerrar a aplicação, hideNotify ser chamado após
		// destroyApp, o que faz com que referências a AppMIDlet.instance causem NullPointerException
		try {
			keyReleased( 0 );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( e , "72");
			//#endif
		}

		if ( screenListener != null )
			screenListener.hideNotify( true );

		// pára de tocar um som, caso necessário. A exclusão mútua não é utilizada aqui para evitar atrasos na resposta da
		// chamada caso o método MediaPlayer.play() esteja na sua região crítica.
		MediaPlayer.stop( false );

		//#if TRY_CATCH_ALL == "true"
		} catch ( Throwable e ) {
			AppMIDlet.log( e , "73");
		}
		//#endif		
	}


	/**
	 * Callback chamada pelo aparelho. Aqui são desenhados o fundo de tela, tela ativa, tela anterior (caso esteja em
	 * transição) e soft keys, caso estejam definidos.
	 *
	 * @param g referência para o Graphics onde as operações de desenho serão realizadas.
	 */
	//#if J2SE == "true"
//# 		public void update( Graphics g ) {
//# 			mutex.acquire();
//# 			g.drawImage( backBuffer, 0, 0, this );
//# 			mutex.release();
//# 		}
//#
//#
//# 		public final void paint( Graphics g ) {
//# 			update( g );
//# 		}
//#
//#
//# 		private final void draw() {
//# 			final Graphics2D g = ( Graphics2D ) screenBuffer;
//#
	//#else
		public final void paint( Graphics g ) {
	//#endif
		// a sincronização deste método com as teclas é necessária para evitar erros de desenho em vários aparelhos, notadamente
		// Siemens, Samsung e LG (drawables podem ser desenhados com offset incorreto por 1 frame durante pressionamento de teclas)
 		mutex.acquire();

		//#if GAMECANVAS == "true"
//# 		//	if ( useFlushGraphics )
//# 		//		g = screenBuffer;
		//#endif

	 	//#if TRY_CATCH_ALL == "true"
		try {
		//#endif

		// Zera a translação acumulada, para evitar que eventuais "lixos" na deixados na última chamada de paint causem
		// erro de posicionamento nas futuras chamadas de desenho.
		//#if J2SE == "false"
			Drawable.resetDrawStack( g );
			setFullScreenMode( true );
		//#else
//# 			final Insets insets = getInsets();
//# 			Drawable.translate.set( insets.left, insets.top );
		//#endif

		g.setClip( viewport.x, viewport.y, viewport.width, viewport.height );

		if ( background == null ) {
			if ( bgColor >= 0 ) {
				//#if J2SE == "false"
					g.setColor( bgColor );
				//#else
//# 					g.setColor( new Color( bgColor ) );
				//#endif
				g.fillRect( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
			}
		} else {
			background.draw( g );
		}

		if ( currentScreen != null )
			currentScreen.draw( g );

		// desenha os indicadores de softkeys (caso existam)
		for ( int i = 0; i < softKeys.length; ++i ) {
			if ( softKeys[ i ] != null )
				softKeys[ i ].draw( g );
		} // fim for ( int i = 0; i < softKeys.length; ++i )

		//#if SHOW_FREE_MEM == "true"
//# 		g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
//# 		g.setColor( 0xff0000 );
//# 		g.setFont( Font.getFont( Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_MEDIUM ) );
//# 		g.drawString( freeMem, SCREEN_WIDTH, 0, Graphics.TOP | Graphics.RIGHT );
		//#endif

		//#if SHOW_FPS == "true"
//# 			g.setClip( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
//# 			g.setColor( 0x00ff00 );
//# 			final Font font = Font.getFont( Font.FACE_MONOSPACE, Font.STYLE_BOLD, Font.SIZE_MEDIUM );
//# 			g.setFont( font );
//# 			g.drawString( strFps, SCREEN_WIDTH, font.getHeight(), Graphics.TOP | Graphics.RIGHT );
		//#endif

		//#if TRY_CATCH_ALL == "true"
		} catch ( Throwable t ) {
			AppMIDlet.log( t , "1:" );
		}
		//#endif				

		//#if GAMECANVAS == "true"
//# 		//	if ( useFlushGraphics )
//# 		// 		flushGraphics();
		//#endif

		// a sincronização deste método com as teclas é necessária para evitar erros de desenho em vários aparelhos, notadamente
		// Siemens, Samsung e LG (drawables podem ser desenhados com offset incorreto por 1 frame durante pressionamento de teclas)
 		mutex.release();
	} // fim do método paint( Graphics )


	/**
	 * Trata o evento de tecla pressionada e o repassa ao keyListener, caso esteja registrado.
	 *
	 * @param keyCode índice da tecla pressionada, recebido pelo aparelho. Caso haja keyListener registrado, seu
	 * método keyPressed será chamado, recebendo o mesmo valor de keyCode (no caso de <i>handleSpecialKey</i> ser false),
	 * ou com as funções especiais tratadas (caso <i>handleSpecialKey</i> seja true).
	 *
	 * @see #keyReleased(int)
	 * @see #setKeyListenerBehavior(boolean)
	 * @see KeyListener
	 */
	protected final void keyPressed( int keyCode ) {
		//#if TRY_CATCH_ALL == "true"
		try {
		//#endif
		if ( keyListener != null ) {
			switch ( keyCode ) {
				//#if BLACKBERRY_API == "true"
//# 					case KEY_VOLUME_DOWN_BLACKBERRY:
//# 						MediaPlayer.setVolume( MediaPlayer.getVolume() - MediaPlayer.VOLUME_STEP );
//# 					break;
//# 
//# 					case KEY_VOLUME_UP_BLACKBERRY:
//# 						MediaPlayer.setVolume( MediaPlayer.getVolume() + MediaPlayer.VOLUME_STEP );
//# 					break;
//# 
				//#endif

				case KEY_STAR:
					//#if DEBUG == "true"
//# 						sizeChanged( SCREEN_HEIGHT, SCREEN_WIDTH );
					//#endif
				case KEY_NUM0:
				case KEY_NUM1:
				case KEY_NUM2:
				case KEY_NUM3:
				case KEY_NUM4:
				case KEY_NUM5:
				case KEY_NUM6:
				case KEY_NUM7:
				case KEY_NUM8:
				case KEY_NUM9:
				case KEY_POUND:
				case FIRE:
				case UP:
				case DOWN:
				case LEFT:
				case RIGHT:
					keyListener.keyPressed( keyCode );
				break;

				default:
					keyListener.keyPressed( getSpecialKey( keyCode ) );
			}
		}

		//#if TRY_CATCH_ALL == "true"
		} catch ( Throwable e ) {
			AppMIDlet.log( e , "74");
		} finally {
				//unlockKeys();
			}
		//#endif	

	} // fim do método keyPressed( int )


	//#if TOUCH == "true"
		/**
		 * Evento recebido ao arrastar um ponteiro numa tela sensível ao toque. Caso haja um <code>pointerListener</code>
		 * registrado, o evento será repassado para ele.
		 *
		 * @param x posição x do ponteiro.
		 * @param y posição y do ponteiro.
		 * @see #pointerPressed(int, int)
		 * @see #pointerReleased(int, int)
		 * @see PointerListener
		 */
		protected final void pointerDragged( int x, int y ) {
			if ( pointerListener != null )
				pointerListener.onPointerDragged( x, y );
		}


		/**
		 * Evento recebido quando o usuário pressiona a tela com um ponteiro. Caso o usuário clique sobre um soft key e
		 * haja um keyListener definido, o keyListener irá receber o evento do soft key pressionado. Caso haja um PointerListener
		 * definido, este irá receber o evento.
		 *
		 * @param x posição x do ponteiro.
		 * @param y posição y do ponteiro.
		 * @see #pointerDragged(int, int)
		 * @see #pointerReleased(int, int)
		 * @see PointerListener
		 */
		protected final void pointerPressed( int x, int y ) {
			// verifica colisão com os softkeys
			for ( byte i = 0; i < softKeys.length; ++i ) {
				if ( softKeys[ i ] != null && softKeys[ i ].contains( x, y ) ) {
					switch ( i ) {
						case SOFT_KEY_LEFT:
							keyPressed( KEY_SOFT_LEFT );
						return;

						case SOFT_KEY_MID:
							keyPressed( KEY_SOFT_MID );
						return;

						case SOFT_KEY_RIGHT:
							keyPressed( KEY_SOFT_RIGHT );
						return;
					}
				} //  fim if ( softKeys[ i ] != null && softKeys[ i ].contains( x, y ) )
			} // fim for ( byte i = 0; i < softKeys.length; ++i )

			if ( pointerListener != null )
				pointerListener.onPointerPressed( x, y );
		} // fim do método pointerPressed( int, int )


		/**
		 * Evento recebido quando o usuário pára de pressionar a tela utilizando um ponteiro. Caso haja um <code>PointerListener</code>
		 * definido, este evento será repassado para ele.
		 *
		 * @param x posição x onde o ponteiro foi solto.
		 * @param y posição y onde o ponteiro foi solto.
		 * @see #pointerPressed(int, int)
		 * @see #pointerDragged(int, int)
		 * @see PointerListener
		 */                
		protected final void pointerReleased( int x, int y ) {
			if ( pointerListener != null )
				pointerListener.onPointerReleased( x, y );
		}

	//#endif


	/**
	 * Trata o evento de tecla liberada e o repassa ao keyListener, caso esteja registrado.
	 *
	 * @param keyCode índice da tecla liberada, recebido pelo aparelho. Caso haja keyListener registrado, seu
	 * método keyReleased será chamado, recebendo o mesmo valor de keyCode (no caso de <i>handleSpecialKey</i> ser false),
	 * ou com as funções especiais tratadas (caso <i>handleSpecialKey</i> seja true).
	 *
	 * @see #keyPressed(int)
	 * @see #setKeyListenerBehavior(boolean)
	 * @see KeyListener
	 */
	protected final void keyReleased( int keyCode ) {
		//#if TRY_CATCH_ALL == "true"
		try {
		//#endif

		if ( keyListener != null )
			keyListener.keyReleased( getSpecialKey( keyCode ) );

		//#if TRY_CATCH_ALL == "true"
		} catch ( Throwable e ) {
			AppMIDlet.log( e , "75");
		}
		//#endif	
	} // fim do método keyReleased( int )


	/**
	 *
	 */
	public final void run() {
		//#if TRY_CATCH_ALL == "true"
		try {
		//#endif

		//#if J2SE == "false"
			Display.getDisplay( AppMIDlet.getInstance() ).setCurrent( this );
		//#endif
		// inicializa as variáveis ligadas ao tamanho de tela
		sizeChanged( getWidth(), getHeight() );

        lastFrameTime = 0;

		long time;
		long lastFrameMS = System.currentTimeMillis();

		while ( running ) {
			synchronized ( this ) {
				while ( paused ) {
					try {
						wait();
					} catch ( Exception e ) {
						//#if DEBUG == "true"
//# 						AppMIDlet.log( e , "76");
						//#endif
					}
				} // fim while ( paused )
			} // fim synchronized( this )

			mutex.acquire();

			time = System.currentTimeMillis();

			lastFrameTime = ( int ) ( time - lastFrameMS );
			// limita o tempo máximo do frame
			if ( lastFrameTime > MAX_FRAME_TIME )
				lastFrameTime = MAX_FRAME_TIME;

			if ( lastFrameTime > 0 ) {
				if ( updatableBackground != null )
					updatableBackground.update( lastFrameTime );

				// se a tela atual for atualizável, chama seu método update
				if ( currentScreenUpdatable != null )
					currentScreenUpdatable.update( lastFrameTime );

				// atualiza os softkeys, caso sejam atualizáveis
				for ( int i = 0; i < softKeysUpdatables.length; ++i ) {
					if ( softKeysUpdatables[ i ] != null )
						softKeysUpdatables[ i ].update( lastFrameTime );
				} // fim for ( int i = 0; i < softKeysUpdatables.length; ++i )
			} // fim if ( lastFrameTime > 0 )

			lastFrameMS = System.currentTimeMillis();

			//#if SHOW_FREE_MEM == "true"
//# 			timeToNextUpdate -= lastFrameTime;
//# 			if ( timeToNextUpdate <= 0 ) {
//# 				timeToNextUpdate = SHOW_FREE_MEM_UPDATE_INTERVAL;
//# 
//# 				AppMIDlet.gc();
//# 				freeMem = String.valueOf( Runtime.getRuntime().freeMemory() );
//# 			}
			//#endif

			//#if SHOW_FPS == "true"
//# 			++frameCounter;
//# 
//# 			fp_accFpsTime += NanoMath.divInt( lastFrameTime, 1000 );
//# 			if ( fp_accFpsTime > 0 ) {
//# 				strFps = NanoMath.toString( NanoMath.divFixed( NanoMath.toFixed( frameCounter ), fp_accFpsTime ) );
//# 
//# 				if ( fp_accFpsTime >= FP_FPS_UPDATE_INTERVAL ) {
//# 					frameCounter = 0;
//# 					fp_accFpsTime %= FP_FPS_UPDATE_INTERVAL;
//# 				}
//# 			}
			//#endif

			mutex.release();

			// Redesenha a tela. Importante: a dupla repaint()/serviceRepaints() é uma opção melhor para
			// fazer o redesenho da tela do que paint(offScreenBuffer)/flushGraphics(). A utilização da 2ª opção causa
			// muita lentidão no tratamento de teclas em alguns aparelhos, como SonyEricsson Z550 e K500, independentemente
			// do uso ou não de mutex no método paint(Graphics).
			//#if J2SE == "false"
				//#if GAMECANVAS == "true"
//# //					if ( useFlushGraphics ) {
//# //						paint( screenBuffer );
//# //					} else {
//# //						repaint();
//# //						// bloqueia até que paint( Graphics ) seja chamado, para evitar flickering e outros problemas de redesenho
//# //						serviceRepaints();
//# //					}
				//#else
 					repaint();
 					// bloqueia até que paint( Graphics ) seja chamado, para evitar flickering e outros problemas de redesenho
 					serviceRepaints();
				//#endif
			//#else
//# 				draw();
//# 				repaint();
			//#endif

			// TODO testar TimerTask.scheduleAtFixedRate para fazer o loop do ScreenManager

			// TODO armazenar último estado de teclas e só repassá-los uma vez por iteração do loop?

			// permite que uma outra thread execute - resolve o problema da extrema lentidão no tratamento de teclas
			// em alguns LGs, como o KG800
			try {
				Thread.sleep( 1 );
			} catch ( Exception ex ) {
				//#if DEBUG == "true"
//# 					AppMIDlet.log( ex , "77");
				//#endif
			}
		} // fim while ( running )

		//#if TRY_CATCH_ALL == "true"
		} catch ( Throwable e ) {
			AppMIDlet.log( e , "78");
		}
		//#endif
	} // fim do método run()


	/**
	 * Define o fundo.
	 *
	 * @param background Drawable a ser usado como fundo de tela. Caso seja null, o fundo é preenchido com a cor definida.
	 * @param update indica se o background deve ser atualizado juntamente com a tela ativa. Para isso, o drawable deve
	 * implementar a interface <i>Updatable</i>. Para manter o background atual e somente alterar a opção de atualizá-lo
	 * ou não, basta chamar esse método passando o mesmo drawable utilizado como background (pode-se utilizar o método
	 * <i>getBackground</i> dessa classe, por exemplo), e alterar somente o parâmentro <i>update</i>.
	 */
	public final void setBackground( Drawable background, boolean update ) {
		this.background = background;

		if ( update && ( background instanceof Updatable ) )
			updatableBackground = ( Updatable ) background;
		else
			updatableBackground = null;
	} // fim do método setBackground( Drawable, boolean )


	/**
	 * Obtém o Drawable sendo atualmente utilizado como fundo de tela.
	 * @return referência para o Drawable utilizado como fundo de tela, ou null caso o fundo esteja sendo preenchido
	 * com uma cor sólida definida anteriormente.
	 */
	public final Drawable getBackgroundDrawable() {
		return background;
	}


	/**
	 * Obtém uma referência para a instância do gerenciador de telas.
	 * @return referência para o gerenciador de telas.
	 */
	public static final ScreenManager getInstance() {
		return instance;
	}


	/**
	 * Obtém a referência para o back buffer da tela.
	 *
	 * @return referência para o Graphics onde a tela é desenhada.
	 */
	public static final Graphics getScreenBuffer() {
		//#if GAMECANVAS == "true"
//#			return instance.screenBuffer;
		//#else
 			return null;
		//#endif
	}


	/**
	 * Inicia a execução do ScreenManager, definindo-o como a tela ativa do aparelho e executando o loop principal de
	 * atualização e desenho de tela.
	 */
    public synchronized final void start() {
        if ( !running ) {
			running = true;
            thread = new Thread( this );
            thread.start();
        } 
		//MOTOROLAS: chamar a linha abaixo mais de uma vez, neste ponto do código, causa problemas...
		//Display.getDisplay( AppMIDlet.getInstance() ).setCurrent( this );
    }


    /**
	 * Interrompe a execução da thread do gerenciador de telas.
	 */
    public final void stop() {
		running = false;
		thread = null;
    }


	/**
	 * Método repassado pelo aparelho, informando uma alteração nas dimensões da tela.
	 *
	 * @param w nova largura da tela, em pixels.
	 * @param h nova altura da tela, em pixels.
	 */
	public final void sizeChanged( int w, int h ) {
		if ( w > 0 && h > 0 ) {
			// gambiarra nojenta causada pelo LG ME970, que indica que tem 307 de altura quando na verdade tem 304
			if ( h == 307 && AppMIDlet.getVendor() == AppMIDlet.VENDOR_LG ) {
				h = 304;
			}

			//#if J2SE == "false"
                            super.sizeChanged( w, h );
			//#endif

			SCREEN_WIDTH = w;
			SCREEN_HALF_WIDTH = w >> 1;

			SCREEN_HEIGHT = h;
			SCREEN_HALF_HEIGHT = h >> 1;

			viewport.set( 0, 0, w, h );
			
			//#if J2SE == "true"
//# 				super.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
//# 				if ( backBuffer == null || backBuffer.getWidth() != SCREEN_WIDTH || backBuffer.getHeight() != SCREEN_HEIGHT ) {
//# 					backBuffer = new BufferedImage( SCREEN_WIDTH, SCREEN_HEIGHT, BufferedImage.TYPE_INT_ARGB );
//# 					screenBuffer = backBuffer.getGraphics();
//# 				}
			//#endif

			// atualiza o tamanho do background
			if ( background != null )
				background.setSize( w, h );

			// atualiza a posição dos labels dos softkeys (caso existam)
			for ( int i = SOFT_KEY_LEFT; i <= SOFT_KEY_RIGHT; ++i )
				setSoftKey( i, softKeys[ i ] );

			if ( screenListener != null )
				screenListener.sizeChanged( w, h );
		}
	} // fim do método sizeChanged( int, int )


	/**
	 * Retorna a referência para a tela corrente.
	 * @return referência para a tela corrente.
	 */
	public final Drawable getCurrentScreen() {
		return currentScreen;
	}


	public static final KeyListener getKeyListener() {
		return keyListener;
	}


	public static final void setKeyListener( KeyListener keyListener ) {
		ScreenManager.keyListener = keyListener;
	}


	//#if TOUCH == "true"
		public static final PointerListener getPointerListener() {
			return pointerListener;
		}


		public static final void setPointerListener( PointerListener pointerListener ) {
			ScreenManager.pointerListener = pointerListener;
		}
	//#endif


	public static final ScreenListener getScreenListener() {
		return screenListener;
	}


	public static final void setScreenListener( ScreenListener screenListener ) {
		ScreenManager.screenListener = screenListener;
	}


	//#if J2SE == "true"
//# 		public final boolean hasPointerEvents() {
//# 			return true;
//# 		}
//#
//#
//# 		public final void keyTyped( KeyEvent e ) {
//# 		}
//#
//#
//# 		public final void keyPressed( KeyEvent e ) {
//# 			keyPressed( e.getKeyCode() );
//# 		}
//#
//#
//# 		public final void keyReleased( KeyEvent e ) {
//# 			keyReleased( e.getKeyCode() );
//# 		}
//#
//#
//# 		public final void mouseClicked( MouseEvent e ) {
//# 			final Insets insets = getInsets();
//# 			pointerPressed( e.getX() - insets.left, e.getY() - insets.top );
//# 		}
//#
//#
//# 		public final void mousePressed( MouseEvent e ) {
//# 		}
//#
//#
//# 		public final void mouseReleased( MouseEvent e ) {
//# 			final Insets insets = getInsets();
//# 			pointerReleased( e.getX() - insets.left, e.getY() - insets.top );
//# 		}
//#
//#
//# 		public final void mouseEntered( MouseEvent e ) {
//# 		}
//#
//#
//# 		public final void mouseExited( MouseEvent e ) {
//# 		}
//#
//#
//# 		public final void mouseDragged( MouseEvent e ) {
//# 			final Insets insets = getInsets();
//# 			pointerDragged( e.getX() - insets.left, e.getY() - insets.top );
//# 		}
//#
//#
//# 		public final void mouseMoved( MouseEvent e ) {
//# 		}
//#
//#
//# 		public final void setSize( int width, int height ) {
//# 			sizeChanged( width, height );
//# 		}
	//#endif


	//#if BLACKBERRY_API == "true"
//# 
//# 	public final boolean keyChar( char key, int status, int time ) {
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean keyDown( int keycode, int time ) {
//# 		final int key = Keypad.key( keycode );
//# 
//# //		try { keyPressed( Keypad.key( keycode ) ); } catch ( Exception e ) {}
//# //		try { keyPressed( keycode ); } catch ( Exception e ) {}
//# //		try { keyPressed( key ); } catch ( Exception e ) {}
//# 
//# 		switch ( key ) {
//# 			case KEY_SOFT_LEFT_BLACKBERRY:
//# 			case KEY_SEND_BLACKBERRY:
//# 				keyPressed( KEY_SOFT_LEFT );
//# 			break;
//# 
//# 			case KEY_SOFT_MID_BLACKBERRY:
//# 			case KEY_SOFT_MID_BLACKBERRY_2:
//# 			case KEY_MENU_BLACKBERRY:
//# 				keyPressed( KEY_SOFT_MID );
//# 			break;
//# 
//# 			case KEY_VOLUME_UP_BLACKBERRY:
//# 			case KEY_VOLUME_DOWN_BLACKBERRY:
//# 				keyPressed( key );
//# 			break;
//# 
//# 			case KEY_END_BLACKBERRY:
//# 				// envia aplicação para background (Keypad.key( keycode ) quando a tecla é pressionada não funciona;
//# 				// somente quando ela é solta)
//# 				try {
//# //					UiApplication.getApplication().requestBackground();
//# 					Display.getDisplay( AppMIDlet.getInstance() ).setCurrent( null );
//# 				} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 						AppMIDlet.log( e );
					//#endif
//# 				}
//# 			break;
//# 
//# 			case KEY_CLEAR_BLACKBERRY_2:
//# 				keyPressed( KEY_CLEAR );
//# 			break;
//# 
//# 			default:
//# 				keyPressed( Keypad.map( keycode ) );
//# 		}
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean keyUp( int keycode, int time ) {
//# 		final int key = Keypad.key( keycode );
//# 
//# //		try { keyReleased( Keypad.key( keycode ) ); } catch ( Exception e ) {}
//# //		try { keyReleased( keycode ); } catch ( Exception e ) {}
//# //		try { keyReleased( key ); } catch ( Exception e ) {}
//# 
//# 		switch ( key ) {
//# 			case KEY_SOFT_LEFT_BLACKBERRY:
//# 			case KEY_SEND_BLACKBERRY:
//# 				keyReleased( KEY_SOFT_LEFT );
//# 			break;
//# 
//# 			case KEY_SOFT_MID_BLACKBERRY:
//# 			case KEY_SOFT_MID_BLACKBERRY_2:
//# 			case KEY_MENU_BLACKBERRY:
//# 				keyReleased( KEY_SOFT_MID );
//# 			break;
//# 
//# 			case KEY_VOLUME_UP_BLACKBERRY:
//# 			case KEY_VOLUME_DOWN_BLACKBERRY:
//# 				// TODO mudar o volume com tecla do Blackberry
//# 				// não é necessário repassar o evento
//# 			break;
//# 
//# 			case KEY_END_BLACKBERRY:
//# 				// envia aplicação para background (Keypad.key( keycode ) quando a tecla é pressionada não funciona;
//# 				// somente quando ela é solta)
//# 				try {
//# 					// em testes nos aparelhos 8310, 8100 e 8700g, ambos os métodos funcionaram para enviar a aplicação
//# 					// para background (há vantagem em utilizar um dos 2?)
//# //					UiApplication.getApplication().requestBackground();
//# 					Display.getDisplay( AppMIDlet.getInstance() ).setCurrent( null );
//# 				} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 						AppMIDlet.log( e );
					//#endif
//# 				}
//# 			break;
//# 
//# 			case KEY_CLEAR_BLACKBERRY_2:
//# 				keyReleased( KEY_CLEAR );
//# 			break;
//# 
//# 			default:
//# 				keyReleased( Keypad.map( keycode ) );
//# 		}
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean keyRepeat( int keycode, int time ) {
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean keyStatus( int keycode, int time ) {
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean trackwheelClick( int status, int time ) {
//# 		// evita envio duplicado de evento em alguns aparelhos (um pela tecla, outro pelo trackball)
//# 		switch ( Keypad.getHardwareLayout() ) {
//# 			case HW_LAYOUT_32:
//# 			case HW_LAYOUT_PHONE:
//# 			case HW_LAYOUT_REDUCED:
//# 				keyPressed( FIRE );
//# 			break;
//# 		}
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean trackwheelUnclick( int status, int time ) {
//# 		// evita envio duplicado de evento em alguns aparelhos (um pela tecla, outro pelo trackball)
//# 		switch ( Keypad.getHardwareLayout() ) {
//# 			case HW_LAYOUT_32:
//# 			case HW_LAYOUT_PHONE:
//# 			case HW_LAYOUT_REDUCED:
//# 				keyReleased( FIRE );
//# 			break;
//# 		}
//# 		return true;
//# 	}
//# 
//# 
//# 	public final boolean trackwheelRoll( int amount, int status, int time ) {
//# 		// verifica se o layout do hardware é o de aparelhos com trackwheel (sem essa verificação, rolagem para cima
//# 		// ou baixo em aparelhos com trackball podem enviar 2 eventos)
//# 		switch ( Keypad.getHardwareLayout() ) {
//# 			case HW_LAYOUT_32:
//# 			case HW_LAYOUT_PHONE:
//# 			case HW_LAYOUT_REDUCED:
//# 				switch ( status ) {
//# 					case KeypadListener.STATUS_ALT:
//# 					case KeypadListener.STATUS_ALT_LOCK:
//# 						keyPressed( amount < 0 ? LEFT : RIGHT );
//# 					break;
//# 
//# 					default:
//# 						keyPressed( amount < 0 ? UP : DOWN );
//# 					break;
//# 				}
//# 			break;
//# 		}
//# 		return true;
//# 	}
//# 
//# 
	//#endif

    public boolean hasPointerEvents() {
        return true;
    }
}

