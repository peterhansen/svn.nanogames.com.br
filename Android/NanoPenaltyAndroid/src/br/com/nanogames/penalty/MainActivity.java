//#if JAVA_VERSION == "ANDROID"
package br.com.nanogames.penalty;

import android.os.Bundle;
import br.com.nanogames.penalty.R;
import javax.microedition.io.ResourceDictionary;
import screens.GameMIDlet;

public class MainActivity extends br.com.nanogames.MIDP.MainActivity
{
      public void setupAssetsForNanoGamesSplash() {            
            ResourceDictionary.addEntry( "/splash/transparency.png", R.drawable.transparency );        
            ResourceDictionary.addEntry( "/splash/nano.png", R.drawable.nano );        
            ResourceDictionary.addEntry( "/splash/light.png", R.drawable.light );        
            ResourceDictionary.addEntry( "/splash/games.png", R.drawable.games );
            ResourceDictionary.addEntry( "/splash/turtle.png", R.drawable.turtle );
            ResourceDictionary.addEntry( "/splash/font_credits.png", R.drawable.font_credits );
            ResourceDictionary.addEntry( "/splash/font_credits.bin", R.raw.font_credits );
        }
        
//        public void setupAssetsForNanoStudioSplash() {
//            ResourceDictionary.addEntry( "/splash/nanostudio.png", R.drawable.nanostudio );        
//            ResourceDictionary.addEntry( "/splash/font_credits.png", R.drawable.font_credits );
//            ResourceDictionary.addEntry( "/splash/font_credits.bin", R.raw.font_credits );        
//            ResourceDictionary.addEntry( "/splash/transparency.png", R.drawable.transparency );
//        }          
      
        public void setupAssetsForNanoOnline() {
                    
            ResourceDictionary.addEntry( "/online/clear.png", R.drawable.clear );
            ResourceDictionary.addEntry( "/online/shift.png", R.drawable.shift );        
            ResourceDictionary.addEntry( "/online/bar.png", R.drawable.nobar );
            ResourceDictionary.addEntry( "/online/bl.png", R.drawable.bl );
            ResourceDictionary.addEntry( "/online/bt.png", R.drawable.bt );
            ResourceDictionary.addEntry( "/online/c.png", R.drawable.c );        
            ResourceDictionary.addEntry( "/online/d.png", R.drawable.d );
            ResourceDictionary.addEntry( "/online/font_0.png", R.drawable.nfont_0 );
            ResourceDictionary.addEntry( "/online/perfil.png", R.drawable.perfil );
            ResourceDictionary.addEntry( "/online/pr.png", R.drawable.pr );
            ResourceDictionary.addEntry( "/online/pt.png", R.drawable.pt );        
            ResourceDictionary.addEntry( "/online/t.png", R.drawable.t );
            ResourceDictionary.addEntry( "/online/0.dat", R.raw.l0 );
            ResourceDictionary.addEntry( "/online/1.dat", R.raw.l1 );
            ResourceDictionary.addEntry( "/online/2.dat", R.raw.l2 );            
            ResourceDictionary.addEntry( "/online/font_0.bin", R.raw.nfont_0 );        

        }
    
    @Override
    public void loadResources( Bundle mainBundle )
    {
        setContentView( R.layout.main );  
        super.startMIDlet( GameMIDlet.class.getName() );
        
        setupAssetsForNanoGamesSplash();
        
        setupAssetsForNanoOnline();

        ResourceDictionary.addEntry( "/splash/title.png", R.drawable.title );        
        ResourceDictionary.addEntry( "/ad_0.png", R.drawable.ad_0 );        
        ResourceDictionary.addEntry( "/ad_1.png", R.drawable.ad_1 );        
        ResourceDictionary.addEntry( "/ball.png", R.drawable.ball );        
        ResourceDictionary.addEntry( "/bar.png", R.drawable.bar );                
        ResourceDictionary.addEntry( "/barrainicial.png", R.drawable.barrainicial );                
        ResourceDictionary.addEntry( "/barraverde.png", R.drawable.barraverde );                
        ResourceDictionary.addEntry( "/bottom.png", R.drawable.bottom );                
        ResourceDictionary.addEntry( "/control_power_level.png", R.drawable.control_power_level );                
        ResourceDictionary.addEntry( "/control_power.png", R.drawable.control_power );                
        ResourceDictionary.addEntry( "/crowd.png", R.drawable.crowd );                
        ResourceDictionary.addEntry( "/cursor_0.png", R.drawable.cursor_0 );                
        ResourceDictionary.addEntry( "/cursor_1.png", R.drawable.cursor_1 );                
        ResourceDictionary.addEntry( "/cursor_2.png", R.drawable.cursor_2 );                
        ResourceDictionary.addEntry( "/cursor_3.png", R.drawable.cursor_3 );                
        ResourceDictionary.addEntry( "/cursor_4.png", R.drawable.cursor_4 );                
        ResourceDictionary.addEntry( "/cursor_5.png", R.drawable.cursor_5 );                
        ResourceDictionary.addEntry( "/direction.png", R.drawable.direction );                        
        ResourceDictionary.addEntry( "/divider.png", R.drawable.divider );                
        ResourceDictionary.addEntry( "/eject.png", R.drawable.eject );                        
        ResourceDictionary.addEntry( "/font_0.png", R.drawable.font_0 );        
        ResourceDictionary.addEntry( "/font_1.png", R.drawable.font_1 );        
        ResourceDictionary.addEntry( "/font_2.png", R.drawable.font_2 );        
        ResourceDictionary.addEntry( "/goal_net.png", R.drawable.goal_net );        
        ResourceDictionary.addEntry( "/grass.png", R.drawable.grass );        
        ResourceDictionary.addEntry( "/height.png", R.drawable.height );        
        ResourceDictionary.addEntry( "/key_0.png", R.drawable.key_0 );        
        ResourceDictionary.addEntry( "/key_1.png", R.drawable.key_1 );        
        ResourceDictionary.addEntry( "/key_2.png", R.drawable.key_2 );        
        ResourceDictionary.addEntry( "/key_3.png", R.drawable.key_3 );
        ResourceDictionary.addEntry( "/line_big.png", R.drawable.line_big );        
        ResourceDictionary.addEntry( "/line_small.png", R.drawable.line_small );        
        ResourceDictionary.addEntry( "/mark.png", R.drawable.mark );        
        ResourceDictionary.addEntry( "/moon.png", R.drawable.moon );        
        ResourceDictionary.addEntry( "/net.png", R.drawable.net );        
        ResourceDictionary.addEntry( "/pause.png", R.drawable.pause );        
        ResourceDictionary.addEntry( "/play.png", R.drawable.play );        
        ResourceDictionary.addEntry( "/post_left.png", R.drawable.post_left );        
        ResourceDictionary.addEntry( "/rails.png", R.drawable.rails );        
        ResourceDictionary.addEntry( "/rec.png", R.drawable.rec );        
        ResourceDictionary.addEntry( "/rewind.png", R.drawable.rewind );        
        ResourceDictionary.addEntry( "/rm.png", R.drawable.rm );        
        ResourceDictionary.addEntry( "/rr.png", R.drawable.rr );        
        ResourceDictionary.addEntry( "/setadir.png", R.drawable.setadir );        
        ResourceDictionary.addEntry( "/shadow.png", R.drawable.shadow );        
        ResourceDictionary.addEntry( "/sky.png", R.drawable.sky );        
        ResourceDictionary.addEntry( "/small_area.png", R.drawable.small_area );                
        ResourceDictionary.addEntry( "/speed.png", R.drawable.speed );        
        ResourceDictionary.addEntry( "/top.png", R.drawable.top );        
        
        ResourceDictionary.addEntry( "/teams/0.png", R.drawable.t0 );        
        ResourceDictionary.addEntry( "/teams/1.png", R.drawable.t1 );        
        ResourceDictionary.addEntry( "/teams/2.png", R.drawable.t2 );        
        ResourceDictionary.addEntry( "/teams/3.png", R.drawable.t3 );        
        ResourceDictionary.addEntry( "/teams/4.png", R.drawable.t4 );        
        ResourceDictionary.addEntry( "/teams/5.png", R.drawable.t5 );        
        ResourceDictionary.addEntry( "/teams/6.png", R.drawable.t6 );        
        ResourceDictionary.addEntry( "/teams/7.png", R.drawable.t7 );        
        ResourceDictionary.addEntry( "/teams/8.png", R.drawable.t8 );        
        ResourceDictionary.addEntry( "/teams/9.png", R.drawable.t9 );        
        ResourceDictionary.addEntry( "/teams/10.png", R.drawable.t10 );        
        ResourceDictionary.addEntry( "/teams/11.png", R.drawable.t11 );        
        ResourceDictionary.addEntry( "/teams/12.png", R.drawable.t12 );        
        ResourceDictionary.addEntry( "/teams/13.png", R.drawable.t13 );        
        ResourceDictionary.addEntry( "/teams/14.png", R.drawable.t14 );        
        ResourceDictionary.addEntry( "/teams/15.png", R.drawable.t15 );        
        ResourceDictionary.addEntry( "/teams/16.png", R.drawable.t16 );        
        ResourceDictionary.addEntry( "/teams/17.png", R.drawable.t17 );        
        ResourceDictionary.addEntry( "/teams/18.png", R.drawable.t18 );        
        ResourceDictionary.addEntry( "/teams/19.png", R.drawable.t19 );        
        ResourceDictionary.addEntry( "/teams/20.png", R.drawable.t20 );        
        ResourceDictionary.addEntry( "/teams/21.png", R.drawable.t21 );        
        ResourceDictionary.addEntry( "/teams/22.png", R.drawable.t22 );        
        ResourceDictionary.addEntry( "/teams/23.png", R.drawable.t23 );        
        ResourceDictionary.addEntry( "/teams/24.png", R.drawable.t24 );        
        ResourceDictionary.addEntry( "/teams/25.png", R.drawable.t25 );        
        ResourceDictionary.addEntry( "/teams/26.png", R.drawable.t26 );        
        ResourceDictionary.addEntry( "/teams/27.png", R.drawable.t27 );        
        ResourceDictionary.addEntry( "/teams/28.png", R.drawable.t28 );        
        ResourceDictionary.addEntry( "/teams/29.png", R.drawable.t29 );        
        ResourceDictionary.addEntry( "/teams/30.png", R.drawable.t30 );        
        ResourceDictionary.addEntry( "/teams/31.png", R.drawable.t31 );        

        ResourceDictionary.addEntry( "/bkg/logo.png", R.drawable.logo );        
        ResourceDictionary.addEntry( "/bkg/fundobandeiras.png", R.drawable.fundobandeiras );        
        
        ResourceDictionary.addEntry( "/board/box.png", R.drawable.box );                
        
        ResourceDictionary.addEntry( "/border/bl_0.png", R.drawable.bl_0 );                
        ResourceDictionary.addEntry( "/border/bl_1.png", R.drawable.bl_1 );                
        ResourceDictionary.addEntry( "/border/bl_2.png", R.drawable.bl_2 );                
        ResourceDictionary.addEntry( "/border/bottom_0.png", R.drawable.bottom_0 );                
        ResourceDictionary.addEntry( "/border/bottom_1.png", R.drawable.bottom_1 );                
        ResourceDictionary.addEntry( "/border/bottom_2.png", R.drawable.bottom_2 );                
        ResourceDictionary.addEntry( "/border/fill_0.png", R.drawable.fill_0 );                
        ResourceDictionary.addEntry( "/border/fill_1.png", R.drawable.fill_1 );                
        ResourceDictionary.addEntry( "/border/fill_2.png", R.drawable.fill_2 );                
        ResourceDictionary.addEntry( "/border/tl_0.png", R.drawable.tl_0 );                
        ResourceDictionary.addEntry( "/border/tl_1.png", R.drawable.tl_1 );                
        ResourceDictionary.addEntry( "/border/tl_2.png", R.drawable.tl_2 );                
        ResourceDictionary.addEntry( "/border/top_0.png", R.drawable.top_0 );                
        ResourceDictionary.addEntry( "/border/top_1.png", R.drawable.top_1 );                
        ResourceDictionary.addEntry( "/border/top_2.png", R.drawable.top_2 );                
        
               
                        
        ResourceDictionary.addEntry( "/scroll/0.png", R.drawable.s0 );                
        ResourceDictionary.addEntry( "/scroll/1.png", R.drawable.s1 );                
        ResourceDictionary.addEntry( "/scroll/2.png", R.drawable.s2 );                
        
        ResourceDictionary.addEntry( "/shooters/b_1_0.png", R.drawable.sb_1_0 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_1.png", R.drawable.sb_1_1 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_2.png", R.drawable.sb_1_2 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_3.png", R.drawable.sb_1_3 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_4.png", R.drawable.sb_1_4 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_5.png", R.drawable.sb_1_5 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_6.png", R.drawable.sb_1_6 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_7.png", R.drawable.sb_1_7 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_8.png", R.drawable.sb_1_8 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_9.png", R.drawable.sb_1_9 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_10.png", R.drawable.sb_1_10 );                        
        ResourceDictionary.addEntry( "/shooters/b_1_11.png", R.drawable.sb_1_11 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_0.png", R.drawable.sh_1_0 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_1.png", R.drawable.sh_1_1 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_2.png", R.drawable.sh_1_2 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_3.png", R.drawable.sh_1_3 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_4.png", R.drawable.sh_1_4 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_5.png", R.drawable.sh_1_5 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_6.png", R.drawable.sh_1_6 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_7.png", R.drawable.sh_1_7 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_8.png", R.drawable.sh_1_8 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_9.png", R.drawable.sh_1_9 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_10.png", R.drawable.sh_1_10 );                        
        ResourceDictionary.addEntry( "/shooters/h_1_11.png", R.drawable.sh_1_11 );                        
        ResourceDictionary.addEntry( "/shooters/h_1.png", R.drawable.sh_1 );                        
        ResourceDictionary.addEntry( "/shooters/h_2.png", R.drawable.sh_2 );                        
        ResourceDictionary.addEntry( "/shooters/j_2.png", R.drawable.sj_2 );                        
        ResourceDictionary.addEntry( "/shooters/p.png", R.drawable.sp );                        
        ResourceDictionary.addEntry( "/shooters/b_1.png", R.drawable.sb_1 );   

        
        ResourceDictionary.addEntry( "/shooters/b_1_p.bin", R.raw.sb_1_p );                
        ResourceDictionary.addEntry( "/shooters/b_1.bin", R.raw.sb_1 );                
        ResourceDictionary.addEntry( "/shooters/h_1_p.bin", R.raw.sh_1_p );                
        ResourceDictionary.addEntry( "/shooters/h_1.bin", R.raw.sh_1 );                
        ResourceDictionary.addEntry( "/shooters/h_2.bin", R.raw.sh_2 );                
        ResourceDictionary.addEntry( "/shooters/j_2.bin", R.raw.sj_2 );                
        ResourceDictionary.addEntry( "/shooters/p.bin", R.raw.sp );                
        
        
        
        

        ResourceDictionary.addEntry( "/keepers/b_1.png", R.drawable.kb_1 );                
        ResourceDictionary.addEntry( "/keepers/h_1.png", R.drawable.kh_1 );                
        ResourceDictionary.addEntry( "/keepers/p.png", R.drawable.kp ); 
        
        ResourceDictionary.addEntry( "/keepers/b_1_p.bin", R.raw.kb_1_p );                
        ResourceDictionary.addEntry( "/keepers/b_1.bin", R.raw.kb_1 );                
        ResourceDictionary.addEntry( "/keepers/h_1_p.bin", R.raw.kh_1_p );                
        ResourceDictionary.addEntry( "/keepers/h_1.bin", R.raw.kh_1 );                
        ResourceDictionary.addEntry( "/keepers/p_p.bin", R.raw.kp_p );
        ResourceDictionary.addEntry( "/keepers/p.bin", R.raw.kp );                

        
        
        ResourceDictionary.addEntry( "/ball.bin", R.raw.ball );       
        ResourceDictionary.addEntry( "/curve.bin", R.raw.curve );       
        ResourceDictionary.addEntry( "/direction.bin", R.raw.direction );       
        ResourceDictionary.addEntry( "/height.bin", R.raw.height );       
        ResourceDictionary.addEntry( "/shadow.bin", R.raw.shadow );       
        
        ResourceDictionary.addEntry( "/board/box.bin", R.raw.box );       
        
        ResourceDictionary.addEntry( "/font_0.bin", R.raw.font_0 );        
        ResourceDictionary.addEntry( "/font_1.bin", R.raw.font_1 );        
        ResourceDictionary.addEntry( "/font_2.bin", R.raw.font_2 );        
                
        
        ResourceDictionary.addEntry( "/teams/0.bin", R.raw.t0 );        
        ResourceDictionary.addEntry( "/teams/1.bin", R.raw.t1 );        
        ResourceDictionary.addEntry( "/teams/2.bin", R.raw.t2 );        
        ResourceDictionary.addEntry( "/teams/3.bin", R.raw.t3 );        
        ResourceDictionary.addEntry( "/teams/4.bin", R.raw.t4 );        
        ResourceDictionary.addEntry( "/teams/5.bin", R.raw.t5 );        
        ResourceDictionary.addEntry( "/teams/6.bin", R.raw.t6 );        
        ResourceDictionary.addEntry( "/teams/7.bin", R.raw.t7 );        
        ResourceDictionary.addEntry( "/teams/8.bin", R.raw.t8 );        
        ResourceDictionary.addEntry( "/teams/9.bin", R.raw.t9 );        
        ResourceDictionary.addEntry( "/teams/10.bin", R.raw.t10 );        
        ResourceDictionary.addEntry( "/teams/11.bin", R.raw.t11 );        
        ResourceDictionary.addEntry( "/teams/12.bin", R.raw.t12 );        
        ResourceDictionary.addEntry( "/teams/13.bin", R.raw.t13 );        
        ResourceDictionary.addEntry( "/teams/14.bin", R.raw.t14 );        
        ResourceDictionary.addEntry( "/teams/15.bin", R.raw.t15 );        
        ResourceDictionary.addEntry( "/teams/16.bin", R.raw.t16 );        
        ResourceDictionary.addEntry( "/teams/17.bin", R.raw.t17 );        
        ResourceDictionary.addEntry( "/teams/18.bin", R.raw.t18 );        
        ResourceDictionary.addEntry( "/teams/19.bin", R.raw.t19 );        
        ResourceDictionary.addEntry( "/teams/20.bin", R.raw.t20 );        
        ResourceDictionary.addEntry( "/teams/21.bin", R.raw.t21 );        
        ResourceDictionary.addEntry( "/teams/22.bin", R.raw.t22 );        
        ResourceDictionary.addEntry( "/teams/23.bin", R.raw.t23 );        
        ResourceDictionary.addEntry( "/teams/24.bin", R.raw.t24 );        
        ResourceDictionary.addEntry( "/teams/25.bin", R.raw.t25 );        
        ResourceDictionary.addEntry( "/teams/26.bin", R.raw.t26 );        
        ResourceDictionary.addEntry( "/teams/27.bin", R.raw.t27 );        
        ResourceDictionary.addEntry( "/teams/28.bin", R.raw.t28 );        
        ResourceDictionary.addEntry( "/teams/29.bin", R.raw.t29 );        
        ResourceDictionary.addEntry( "/teams/30.bin", R.raw.t30 );        
        ResourceDictionary.addEntry( "/teams/31.bin", R.raw.t31 );       
        
        ResourceDictionary.addEntry( "/textsEn.dat", R.raw.textsen );        
        ResourceDictionary.addEntry( "/textsPt.dat", R.raw.textspt );        
        
        ResourceDictionary.addEntry( "/0.mid", R.raw.m0 );        
        ResourceDictionary.addEntry( "/1.mid", R.raw.m1 );        
        ResourceDictionary.addEntry( "/2.mp3", R.raw.m2 );        
        ResourceDictionary.addEntry( "/3.mp3", R.raw.m3 );        
        ResourceDictionary.addEntry( "/4.mid", R.raw.m4 );        
        ResourceDictionary.addEntry( "/5.mid", R.raw.m5 );        
        ResourceDictionary.addEntry( "/6.mid", R.raw.m6 );        
        ResourceDictionary.addEntry( "/7.mid", R.raw.m7 );        
        ResourceDictionary.addEntry( "/8.mid", R.raw.m8 );        
        ResourceDictionary.addEntry( "/9.mp3", R.raw.m9 );        
        ResourceDictionary.addEntry( "/10.mp3", R.raw.m10 );        
        ResourceDictionary.addEntry( "/11.mid", R.raw.m11 );        
        ResourceDictionary.addEntry( "/12.mid", R.raw.m12 );                
    }
}
//#endif