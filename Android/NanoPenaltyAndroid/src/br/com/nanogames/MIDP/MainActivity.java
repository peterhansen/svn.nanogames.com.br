//#if JAVA_VERSION == "ANDROID"
package br.com.nanogames.MIDP;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.penalty.R;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.ResourceDictionary;
import javax.microedition.lcdui.Display;
import javax.microedition.media.Manager;
import javax.microedition.midlet.MIDlet;

public class MainActivity extends Activity implements AdListener {
    

    public RelativeLayout getContainerLayout() {
        return (RelativeLayout) findViewById( R.id.svScreenManager );
    }

    public void onReceiveAd(Ad ad) {
        System.out.println("Ad received: " + adView.toString());
        LinearLayout layout = ( LinearLayout ) findViewById( R.id.adViewLayout );
        layout.setMinimumHeight( 50 );
        layout.setMinimumWidth( 320 );
        adView.setMinimumHeight( 50 );
        adView.setMinimumWidth( 320 );
        adView.setVisibility( View.VISIBLE );
        
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
        params.height = 50;
        layout.setLayoutParams(params);
        
        findViewById( R.id.svScreen ).requestLayout();
        ScreenManager.getInstance().requestLayout();
    }

    public void onFailedToReceiveAd(Ad ad, ErrorCode ec) {
        System.err.println("Ad failed: " + adView.toString() + ec.toString());
        adView.setVisibility(AdView.GONE);
    }

    public void onPresentScreen(Ad ad) {
        System.out.println("Ad onPresentScreen: " + adView.toString());
    }

    public void onDismissScreen(Ad ad) {
        System.out.println("Ad onDismissScreen: " + adView.toString());        
    }

    public void onLeaveApplication(Ad ad) {        
        System.out.println("Ad onLeaveApplication: " + adView.toString());        
    }
    
    private class MIDletPhoneStateListener extends PhoneStateListener {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            
            if ( state == TelephonyManager.CALL_STATE_RINGING )
                midlet.pauseApp();
        }
    } 
    
    private AdView adView;    
    private static MainActivity instance;
    private MIDlet midlet;
    private static String midletName;
    public static final String TAG = "MainActivity";
    private MIDletPhoneStateListener phoneListener;
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        adView.destroy();        
        midlet.notifyDestroyed();       
        Manager.Destroy();
        System.exit( 0 );
    }

    @Override
    protected void onPause() {
        super.onPause();
        
        if ( midlet != null )
            midlet.pauseApp();
    }
    
    
    
    @Override 
    public void onConfigurationChanged(Configuration newConfig) { 
      super.onConfigurationChanged(newConfig); 
    } 
    
    public int getWidth() {
        if ( midlet != null && Display.getDisplay( midlet ).getCurrent() != null && Display.getDisplay( midlet ).getCurrent().getWidth() != 0 )
            return Display.getDisplay( midlet ).getCurrent().getWidth();
        else 
        if ( getWindowManager().getDefaultDisplay().getWidth() != 0 )
            return getWindowManager().getDefaultDisplay().getWidth();
        else //morra!
            return 0;
    }
    
    public int getHeight() {
        if ( midlet != null && Display.getDisplay( midlet ).getCurrent() != null && Display.getDisplay( midlet ).getCurrent().getHeight() != 0 )        
            return Display.getDisplay( midlet ).getCurrent().getHeight();
        else 
        if ( getWindowManager().getDefaultDisplay().getHeight() != 0 )
            return getWindowManager().getDefaultDisplay().getHeight();
        else //morra!
            return 0;
    }
    
    public static MainActivity getActivity() {
        return instance;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ( midlet == null ) {
            try {
                midlet = (MIDlet) Class.forName( midletName ).newInstance();
               } catch (Exception ex) {           
                Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }               

        
        // Create the adView
        adView = new AdView( this, AdSize.BANNER, "a14e9475f15813d" );
        adView.setAdListener( this );
        adView.setVisibility( View.GONE );
        // Lookup your LinearLayout assuming it’s been given
        // the attribute android:id="@+id/mainLayout"
        LinearLayout layout = ( LinearLayout ) findViewById( R.id.adViewLayout );

        // Add the adView to it
        layout.addView(adView);

        // Initiate a generic request to load it with an ad
        AdRequest request = new AdRequest();
        request.setTesting( false );        
//        request.addTestDevice("B3EEABB8EE11C2BE770B684D95219ECB");
        adView.loadAd( request );        
        
        
        midlet.start();
        
        findViewById( R.id.svScreen ).setFocusable(true);
        findViewById( R.id.svScreen ).setClickable(true);
        findViewById( R.id.svScreen ).setLongClickable(true);
        findViewById( R.id.svScreen ).requestFocus();
        findViewById( R.id.svScreen ).setFocusableInTouchMode(true);
        findViewById( R.id.svScreen ).setVisibility(View.VISIBLE);

        
    }

    @Override
    protected void onStop() {
        super.onStop();

        midlet.pauseApp();
    }


    
    
    @Override
    protected void onStart() {
        super.onStart();       
        
        if ( phoneListener == null )
            phoneListener = new MIDletPhoneStateListener();
        
        TelephonyManager tm = (TelephonyManager) getSystemService( Context.TELEPHONY_SERVICE );
        tm.listen( phoneListener, PhoneStateListener.LISTEN_CALL_STATE );
    }
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        instance = this;
        super.onCreate(savedInstanceState);
        
        requestWindowFeature( Window.FEATURE_NO_TITLE );
        this.getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN ); 
        
        
        
        ResourceDictionary.setGroup( this.getResources() );
        loadResources( savedInstanceState );
        setContentView( R.layout.main );        
    }
    
    public static void startMIDlet( String midletName ) {
        MainActivity.midletName = midletName;
    }

    public void loadResources( Bundle mainBundle ) {
        
    }
    

}
//#endif