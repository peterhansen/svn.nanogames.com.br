/**
 * Stadium.java
 *
 * Created on Jun 10, 2010 8:09:44 PM
 *
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
//import br.com.nanogames.components.online.ad.AdChannel;
//import br.com.nanogames.components.online.ad.Resource;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class Stadium extends UpdatableGroup implements ScreenListener, Constants {

	
	private final int[] height = {
			1,
			BOARDS_HEIGHT, ///tamanho das placas de propaganda
			1,
			WALL_HEIGHT, ///10
			1,
			CROWD_HEIGHT, //68
			0,
			CEILING_HEIGHT, ///15
			1,
			CROWD_HEIGHT, //68
			WALL_HEIGHT, //10
			0,
			0,
			0
		};

	private final int[] colors = {
		0x504124,
		0xededed,
		0x504124,
		0x86a5b6,
		0xbedce6,
		0x404e70,
		0,
		0x86a5b6,
		0xbedce6,
		0x5c738b,
		0x404e70,
		0,
		0,
		0,
	};

	private static final byte INDEX_PATTERN_1 = 5;
	private static final byte INDEX_PATTERN_2 = 9;
	private static final byte INDEX_SKY = 13;
	private static final byte INDEX_DIVIDER = 7;
	private static final byte INDEX_TOP = 12;

	private static final short HEIGHT =  FIELD_IMAGE_SIZE;
	private static final short[] CROWD_OFFSETS_Y = { -BOARDS_HEIGHT - 2, CROWD_OFFSET };



	

	/** Id do canal de anúncios no servidor. */
	//#if SCREEN_SIZE == "GIANT"
 		private static final int AD_CHANNEL_VERSION_ID = 0;
	//#elif SCREEN_SIZE == "BIG"
//# 		private static final int AD_CHANNEL_VERSION_ID = 0;
	//#elif SCREEN_SIZE == "MEDIUM"
//		private static final int AD_CHANNEL_VERSION_ID = 0;
	//#else
//# 		private static final int AD_CHANNEL_VERSION_ID = 0;
	//#endif
	private static final int AD_RESOURCE_ID_LOGO = 0;

	private final Pattern adPattern;


	public Stadium( boolean crowd ) throws Exception {
            super( 20 );
//		super( AD_CHANNEL_VERSION_ID, 20, true );

		for ( int i = 0, y = HEIGHT; i < height.length; ++i ) {
			Pattern p = null;
			
			switch ( i ) {
				case INDEX_DIVIDER:
					p = new Pattern( new DrawableImage( PATH_IMAGES + "divider.png" ) );
					p.setSize( Short.MAX_VALUE, p.getFill().getHeight() );
				break;

				case INDEX_TOP:
					p = new Pattern( new DrawableImage( PATH_IMAGES + "top.png" ) );
					p.setSize( Short.MAX_VALUE, p.getFill().getHeight() );
				break;

				case INDEX_SKY:
					//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE =="SMALL"
//					if (GameMIDlet.isLowMemory()) {
//						p = new Pattern(0x008ff4);
//					} else {
//						p = new Pattern(new DrawableImage(PATH_IMAGES + "sky.png"));
//					}
					//#else
 					p = new Pattern(new DrawableImage(PATH_IMAGES + "sky.png"));
					//#endif
					p.setSize(Short.MAX_VALUE, y);
					break;

				case INDEX_PATTERN_1:
				case INDEX_PATTERN_2:
					if ( crowd ) {
						// TODO não desenhar pattern sob a torcida
//						y -= height[ i ];
//						break;
					}

				default:
					p = new Pattern( colors[ i ] );
					p.setSize( Short.MAX_VALUE, height[ i ] );
			}

			if ( p != null ) {
				y -= p.getHeight();
				p.setPosition( 0, y );

				insertDrawable( p );
			}
		}

		if ( crowd ) {
			//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE =="SMALL"
//			if (!GameMIDlet.isLowMemory()) {
				//#endif
				final DrawableImage crowdImage = new DrawableImage(PATH_IMAGES + "crowd.png");

				for (byte i = 0; i < CROWD_OFFSETS_Y.length; ++i) {
					final Pattern crowdPattern = new Pattern(crowdImage);
					crowdPattern.setSize(Short.MAX_VALUE, crowdPattern.getFill().getHeight());
					crowdPattern.setPosition(0, HEIGHT - crowdPattern.getHeight() + CROWD_OFFSETS_Y[i]);
					insertDrawable(crowdPattern);

					final Pattern railsPattern = new Pattern(new DrawableImage(PATH_IMAGES + "rails.png"));
					railsPattern.setSize(Short.MAX_VALUE, railsPattern.getFill().getHeight());
					railsPattern.setPosition(0, HEIGHT - railsPattern.getHeight() + CROWD_OFFSETS_Y[i]);
					insertDrawable(railsPattern);
				}
				//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE =="SMALL"
//			}
			//#endif
		}

		Drawable fill = null;
//		if ( ad != null ) {
//			try {
//				final Resource[] resources = ad.getResources();
//				fill = ( DrawableImage ) resources[ 0 ].getData();
//				fill.setSize( fill.getWidth() + 5, Math.min( fill.getHeight(), BOARDS_HEIGHT ) );
//			} catch ( Exception e ) {
//				//#if DEBUG == "true"
//					e.printStackTrace();
//				//#endif
//			}
//		}
		if ( fill == null )
			fill = new DrawableImage( PATH_IMAGES + "ad_" + NanoMath.randInt( 2 ) + ".png" );
//
		adPattern = new Pattern( fill );
		adPattern.setSize( Short.MAX_VALUE, Math.min( BOARDS_HEIGHT, fill.getHeight() ) );
		adPattern.setPosition( 0, HEIGHT - 1 - BOARDS_HEIGHT + ( ( BOARDS_HEIGHT - fill.getHeight() ) >> 1 ) );
		insertDrawable( adPattern );

		setSize( Short.MAX_VALUE, HEIGHT );
	}


	public final void sizeChanged( int width, int height ) {
	}

    public void hideNotify(boolean deviceEvent) {
    }

    public void showNotify(boolean deviceEvent) {
    }

}
