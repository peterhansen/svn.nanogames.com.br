/**
 * PlayResultBox.java
 *
 * Created on May 30, 2010 10:50:44 PM
 *
 */

package core;

import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import screens.GameMIDlet;

/**
 *
 * @author peter
 */
public final class PlayResultBox extends GenericInfoBox implements Constants {

	private final MarqueeLabel text;

	private String nextText;


	public PlayResultBox() throws Exception {
		super( 4 );

		text = new MarqueeLabel( FONT_INDEX_TITLE, null );
		text.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		text.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		insertDrawable( text );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		if ( text != null ) {
			text.setSize( width - ( ITEMS_SPACING << 1 ), text.getFont().getHeight() );
			text.setPosition( ITEMS_SPACING, ( getHeight() - text.getHeight() ) >> 1 );
		}
		setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ITEMS_SPACING );
	}
	
	
	public final void setText( int textIndex ) {
		setText( GameMIDlet.getText( textIndex ) );
	}


	public final void setText( String text ) {
		nextText = text;
		setState( STATE_HIDING );
	}


	private final void switchTexts() {
		if ( text != null ) {
			text.setText( nextText );

            text.setTextOffset( ( text.getWidth() < getWidth() ) ? ( (  getWidth() - text.getWidth() ) >> 1 ) : 0 );
            
			nextText = null;
			setSize( Math.min( text.getWidth() + ( ITEMS_SPACING << 1 ), ScreenManager.SCREEN_WIDTH ), border.getMinHeight() );

			setState( STATE_APPEARING );
		}
	}


	public final int getBorderHeight() {
		return text.getHeight() + ITEMS_SPACING + ( BORDER_OFFSET << 1 );
	}


	public final void setState( int state ) {
		super.setState( state );

		if ( this.state == STATE_HIDDEN && nextText != null ) {
			switchTexts();
		}
	}

}
