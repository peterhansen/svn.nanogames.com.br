/**
 * Team.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:52:41 AM.
 */
package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import screens.ChooseTeamScreen;
import screens.GameMIDlet;

/**
 * 
 * @author Peter
 */
public final class Team implements Constants, Serializable {

	private final byte teamIndex;
	private byte victories;
	private short goalsScored;
	private short goalsDiff;
	private final DrawableImage flag;

	private static DrawableImage[] FLAGS;

	
	public Team(int teamIndex) {
		this.teamIndex = (byte) teamIndex;
		flag = getFlag( teamIndex );
	}


	public static final void loadFlags() throws Exception {
		FLAGS = new DrawableImage[ TEAMS_TOTAL ];
		for ( byte i = 0; i < TEAMS_TOTAL; ++i )
			FLAGS[ i ] = new DrawableImage(PATH_IMAGES + "teams/" + i + ".png" );
	}


	public static final DrawableImage getFlag( int index ) {
		return new DrawableImage( FLAGS[ index ] );
	}


	public final byte getIndex() {
		return teamIndex;
	}

	
	public final short getGoalsDiff() {
		return goalsDiff;
	}

	
	public final short getGoalsScored() {
		return goalsScored;
	}

	
	public final byte getVictories() {
		return victories;
	}

	
	public final Player getShooter( int index ) throws Exception {
		return new Player( teamIndex, index );
	}


	public final Keeper loadKeeper( Drawable goal ) throws Exception {
		return new Keeper( teamIndex );
	}

	
	public final void updateStatus(boolean won, int goalsScored, int goalsDiff) {
		if (won) {
			++victories;
		}

		this.goalsScored += goalsScored;
		this.goalsDiff += goalsDiff;
	}


	public final void write(DataOutputStream output) throws Exception {
		output.writeByte(victories);
		output.writeShort(goalsScored);
		output.writeShort(goalsDiff);
	}


	public final DrawableImage getFlag() {
		return flag;
	}


	public final void read(DataInputStream input) throws Exception {
		victories = input.readByte();
		goalsScored = input.readShort();
		goalsDiff = input.readShort();
	}

	public final String getName() {
		return getName(teamIndex);
	}


	public static final String getName(int indexteam) {
		return GameMIDlet.getText(indexteam + TEXT_SOUTH_AFRICA);
	}


	public final String getNameShort() {
		return GameMIDlet.getText( TEXT_SOUTH_AFRICA_SHORT + teamIndex );
	}
	
}

