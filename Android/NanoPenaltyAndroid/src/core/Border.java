/**
 * Border.java
 *
 * Created on May 31, 2010 7:02:18 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;

/**
 *
 * @author peter
 */
public final class Border extends DrawableGroup implements Constants {

	public static final byte BORDER_COLOR_BLACK = 0;
	public static final byte BORDER_COLOR_BLUE = 1;
	public static final byte BORDER_COLOR_GREEN = 2;

	private static final byte BORDER_COLORS_TOTAL = 3;

	protected final Pattern patternLeft;
	protected final Pattern patternRight;
	protected final Pattern patternTop;
	protected final Pattern patternBottom;

	protected final Pattern fill;
	protected final Pattern fillTop;
	protected final Pattern fillBottom;

	protected final DrawableImage topLeft;
	protected final DrawableImage topRight;
	protected final DrawableImage bottomLeft;
	protected final DrawableImage bottomRight;

	protected static DrawableImage[] TOP;
	protected static DrawableImage[] BOTTOM;
	protected static DrawableImage[] TOP_LEFT;
	protected static DrawableImage[] BOTTOM_LEFT;
	protected static DrawableImage[] FILL;

	private static final int COLOR_BLACK_TOP		= 0x484848;
	private static final int COLOR_BLACK_BOTTOM		= 0x171717;
	private static final int COLOR_BLUE_TOP			= 0x0d1743;
	private static final int COLOR_BLUE_BOTTOM		= 0x234274;
	private static final int COLOR_GREEN_TOP		= 0x6bb01e;
	private static final int COLOR_GREEN_BOTTOM		= 0x226c0d;


	public Border( byte color ) {
		super( 12 );

		final int[][] colors = new int[][] {
			{ COLOR_BLACK_TOP, COLOR_BLUE_TOP, COLOR_GREEN_TOP },
			{ COLOR_BLACK_BOTTOM, COLOR_BLUE_BOTTOM, COLOR_GREEN_BOTTOM },
		};

		fillTop = new Pattern( colors[ 0 ][ color ] );
		insertDrawable( fillTop );

		fillBottom = new Pattern( colors[ 1 ][ color ] );
		insertDrawable( fillBottom );

		patternLeft = new Pattern( 0x000000 );
		patternLeft.setSize( 1, 0 );
		insertDrawable( patternLeft );

		patternTop = new Pattern( new DrawableImage( TOP[ color ] ) );
		patternTop.setSize( 0, patternTop.getFill().getHeight() );
		insertDrawable( patternTop );

		patternRight = new Pattern( 0x000000 );
		patternBottom = new Pattern( new DrawableImage( BOTTOM[ color ] ) );
		patternRight.setSize( 1, 0 );
		insertDrawable( patternRight );
		patternBottom.setSize( 0, patternBottom.getFill().getHeight() );
		insertDrawable( patternBottom );

		fill = new Pattern( new DrawableImage( FILL[ color ] ) );
		insertDrawable( fill );

		topLeft = new DrawableImage( TOP_LEFT[ color ] );
		insertDrawable( topLeft );

		bottomLeft = new DrawableImage( BOTTOM_LEFT[ color ] );
		bottomLeft.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		insertDrawable( bottomLeft );

		topRight = new DrawableImage( topLeft );
		topRight.mirror( TRANS_MIRROR_H );
		topRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_TOP );
		insertDrawable( topRight);

		bottomRight = new DrawableImage( bottomLeft );
		bottomRight.mirror( TRANS_MIRROR_H );
		bottomRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_BOTTOM );
		insertDrawable( bottomRight );

		patternTop.setPosition( topLeft.getWidth(), 0 );
		patternLeft.setPosition( 0, topLeft.getHeight());
		patternRight.setPosition( 0, topRight.getHeight() );
		fill.setPosition( patternLeft.getWidth(), topLeft.getHeight() );
	}


	public static final void load() throws Exception {
		TOP = new DrawableImage[ BORDER_COLORS_TOTAL ];
		TOP_LEFT = new DrawableImage[ BORDER_COLORS_TOTAL ];
		BOTTOM = new DrawableImage[ BORDER_COLORS_TOTAL ];
		BOTTOM_LEFT = new DrawableImage[ BORDER_COLORS_TOTAL ];
		FILL = new DrawableImage[ BORDER_COLORS_TOTAL ];

		for ( byte i = 0; i < BORDER_COLORS_TOTAL; ++i ) {
			final String end = i + ".png";
			
			TOP[ i ] = new DrawableImage( PATH_MENU_BORDER + "top_" + String.valueOf(end) );
			TOP_LEFT[ i ] = new DrawableImage( PATH_MENU_BORDER + "tl_" +  String.valueOf(end) );
			BOTTOM[ i ] = new DrawableImage( PATH_MENU_BORDER + "bottom_" +  String.valueOf(end) );
			BOTTOM_LEFT[ i ] = new DrawableImage( PATH_MENU_BORDER + "bl_" +  String.valueOf(end) );
			FILL[ i ] = new DrawableImage( PATH_MENU_BORDER + "fill_" +  String.valueOf(end) );
		}
	}


	public final int getMinHeight() {
		return bottomLeft.getHeight() + topLeft.getHeight();
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		bottomLeft.setRefPixelPosition( 0, height );
		topRight.setRefPixelPosition( width, 0 );
		bottomRight.setRefPixelPosition( width, height );

		patternTop.setSize( width - patternTop.getPosX() - topRight.getWidth(), patternTop.getHeight() );
		patternBottom.setSize( patternTop.getWidth(), patternBottom.getHeight() );
		patternLeft.setSize( patternLeft.getWidth(), height - patternLeft.getPosY() - bottomLeft.getHeight() );
		patternRight.setSize( patternRight.getWidth(), patternLeft.getHeight() );

		patternRight.setPosition( getWidth() - patternRight.getWidth(), topRight.getHeight() );

		fill.setSize( patternRight.getPosX() - fill.getPosX(), Math.min( fill.getFill().getHeight(), bottomRight.getPosY() - topLeft.getHeight() ) );
		fill.setPosition( fill.getPosX(), ( getHeight() - fill.getHeight() ) >> 1 );
		
		fillTop.setPosition( fill.getPosX(), topLeft.getHeight() );
		fillTop.setSize( fill.getWidth(), fill.getPosY() - fillTop.getPosY() );
		fillBottom.setSize( fill.getWidth(), bottomRight.getPosY() - ( fill.getPosY() + fill.getHeight() ) );
		fillBottom.setPosition( fill.getPosX(), fill.getPosY() + fill.getHeight() );

		patternBottom.setPosition( bottomRight.getWidth(), bottomLeft.getPosY() );
	}

}
