/**
 * BlackBoard.java
 * ©2008 Nano Games.
 *
 * Created on Apr 14, 2008 8:21:28 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class BkgMenu extends UpdatableGroup implements Constants {

	private static final byte TOTAL_ITEMS = 6;
	
	private final Pattern patternFill;

	private static DrawableImage PATTERN;
	private static Drawable logo;
	private static BasicAnimatedPattern bkg;


	public BkgMenu() throws Exception {
		super( TOTAL_ITEMS );
		
		patternFill = getAnimatedBkg();
		insertDrawable( patternFill );
		setAnimation(NanoMath.randInt(5)+1);
		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
	}

	
	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		patternFill.setSize( width, height );

		if ( logo != null )
			logo.setRefPixelPosition( getWidth() >> 1, getLogoY() );
	}


	public static final short getLogoY() {
		return ( short ) ( ScreenManager.SCREEN_HEIGHT >> 1 );
	}


	public static final DrawableImage getPattern() throws Exception {
		if ( PATTERN == null )
			PATTERN = new DrawableImage( PATH_IMAGES + "bkg/fundobandeiras.png" );

		return new DrawableImage( PATTERN );
	}


	public static final BasicAnimatedPattern getAnimatedBkg() throws Exception {
		bkg = new BasicAnimatedPattern(getPattern(), ScreenManager.SCREEN_HALF_WIDTH / 5, ScreenManager.SCREEN_HALF_WIDTH / 5);
		bkg.setAnimation(BasicAnimatedPattern.ANIMATION_DIAGONAL);

		return bkg;
	}

	
	public final void setAnimation(int animation) {
		bkg.setAnimation( animation);
	}

	
	public final void setLogo( boolean visible ) throws Exception {
		if ( !visible ) {
			if (logo != null) {
				removeDrawable( logo );
				logo = null;
			}
		} else if ( visible ) {
			if ( logo == null )
				insertDrawable( getLogo() );
		}
	}


	public final boolean isLoaded() {
		return logo != null;
	}


	public static final Drawable getLogo() throws Exception {
		//#if SCREEN_SIZE != "GIANT"
//			if ( logo == null ) {
//				logo = new DrawableGroup( 2 );
//				
//				final DrawableImage ballImage = new DrawableImage(PATH_IMAGES + "bkg/ball.png");
//				final DrawableImage logoImage = new DrawableImage(PATH_IMAGES + "bkg/logo.png");
//
//				( ( DrawableGroup ) logo ).insertDrawable( logoImage );
//
//				ballImage.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
//				( ( DrawableGroup ) logo ).insertDrawable( ballImage );
//				
//				logo.setSize( logoImage.getSize() );
//				ballImage.setRefPixelPosition( logo.getWidth() >> 1, logo.getHeight() );
//				logo.defineReferencePixel( ANCHOR_CENTER );
//			}
		//#else
 			if ( logo == null ) {
 				logo = new DrawableImage(PATH_IMAGES + "bkg/logo.png");
 				logo.defineReferencePixel( ANCHOR_CENTER );
 			}
		//#endif
		
		return logo;
	}

	
	public final int getVisibleAreaY() {
		return 0;
	}

	
	public final int getVisibleAreaHeight() {
		return getHeight() ;
	}
	
	
}
