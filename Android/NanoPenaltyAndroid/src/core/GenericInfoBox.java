/**
 * GenericInfoBox.java
 * ©2008 Nano Games.
 *
 * Created on Apr 6, 2008 11:52:59 AM.
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;


/**
 * 
 * @author Peter
 */
public class GenericInfoBox extends Container implements Constants {
	
	public static final byte STATE_HIDDEN		= 0;
	public static final byte STATE_APPEARING	= 1;
	public static final byte STATE_SHOWN		= 2;
	public static final byte STATE_HIDING		= 3;
	
	protected byte state;

	/** Espaçamento entre os items da barra. */
	public static final byte ITEMS_SPACING = 6;

	protected final Border border;

	/** Duração da transição (estados STATE_APPEARING e STATE_HIDING). */
	public static final short TRANSITION_TIME = 620;
	
	protected int visibleTime;

	protected int accTime;

	protected EventListener eventListener;

	public static final byte EVENT_TYPE_BOX_HIDDEN = -1;

	protected Object eventData;

	
	protected GenericInfoBox( int nItems ) throws Exception {
		super( nItems + 16 );

		border = new Border( Border.BORDER_COLOR_BLUE );
		insertDrawable( border );
		
		setViewport( new Rectangle() );

		setState( STATE_HIDDEN );
	}


	public final void setEventListener( EventListener e ) {
		eventListener = e;
	}


	public final void setEventData( Object data ) {
		eventData = data;
	}


	public final byte getState() {
		return state;
	}

	
	public void setState( int state ) {
		if ( state == this.state )
			return;
		
		switch ( state ) {
			case STATE_HIDDEN:
				setVisible( false );
				accTime = 0;
				if ( eventListener != null ) {
					eventListener.eventPerformed( new Event( this, EVENT_TYPE_BOX_HIDDEN, eventData ) );
					eventData = null;
				}
			break;

			case STATE_APPEARING:
				if ( this.state == STATE_SHOWN )
					return;

//				setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - getHeight() ) >> 1 );
				viewport.width = 0;
				viewport.height = 0;

				setVisible( true );
			break;

			case STATE_SHOWN:
				setVisible( true );
				accTime = TRANSITION_TIME;
				refreshViewport();
//				viewport.width = getWidth();
//				viewport.height = getHeight();
			break;

			case STATE_HIDING:
				if ( this.state == STATE_HIDDEN )
					return;

				setVisible( true );
			break;
		}

		this.state = ( byte ) state;
	}
	

	public void update( int delta ) {
		switch ( state ) {
			case STATE_HIDDEN:
			return;

			case STATE_APPEARING:
				accTime += delta;
				refreshViewport();

				if ( accTime >= TRANSITION_TIME )
					setState( STATE_SHOWN );
			return;

			case STATE_SHOWN:
				if ( visibleTime > 0 ) {
					visibleTime -= delta;

					if ( visibleTime <= 0 ) {
						visibleTime = 0;
						setState( STATE_HIDING );
					}
				}
			break;

			case STATE_HIDING:
				accTime -= delta;
				refreshViewport();

				if ( accTime <= 0 )
					setState( STATE_HIDDEN );
			break;
		}

		super.update( delta );
	}


	private final void refreshViewport() {
		final int fp_progress = NanoMath.clamp( NanoMath.divInt( accTime, TRANSITION_TIME ), 0, NanoMath.ONE );

		viewport.width = NanoMath.toInt( NanoMath.mulFixed( fp_progress, NanoMath.toFixed( getWidth() ) ) );
		viewport.height = NanoMath.toInt( NanoMath.mulFixed( fp_progress, NanoMath.toFixed( getHeight() ) ) );

		viewport.x = getPosX() + ( ( getWidth() - viewport.width ) >> 1 );
		viewport.y = getPosY() + ( ( getHeight() - viewport.height ) >> 1 );
	}
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		border.setSize( width, height );
		
		setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - getHeight() ) >> 1 );
	}
	
	
	public final void setVisibleTime( int time ) {
		visibleTime = time;
	}

	
}
