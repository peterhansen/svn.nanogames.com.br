/**
 * Control.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 5:05:54 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point3f;
import screens.GameMIDlet;


/**
 * 
 * @author Peter
 */
public final class Control extends UpdatableGroup implements Constants {

	// tipos de controle disponíveis
	public static final byte CONTROL_NONE		= 0;
	public static final byte CONTROL_DIRECTION	= 1;
	public static final byte CONTROL_HEIGHT		= 2;
	public static final byte CONTROL_CURVE		= 3;
	public static final byte CONTROL_POWER		= 4;
	
	public static final byte CONTROL_DONE		= 5;
	
	private static final byte CONTROL_TOTAL		= 6;

	// tempo em segundos que o controle leva para percorrer toda a faixa de valores
	public static final int FP_CONTROL_TIME = 34865;//NanoMath.divInt( 532, 1000 );

	// turnos possíveis
	public static final byte TURN_ATTACK = 0;
	public static final byte TURN_DEFEND = 1;

	// número de pontos desenhados para indicar o efeito na bola
	public static final byte CURVE_N_POINTS = 8;

	// fatores multiplicados pela jogada aleatória, de forma a reduzir o número de chutes para fora
	public static final int FP_RANDOM_DIRECTION_FACTOR	= 60293;//NanoMath.divInt( 92, 100 );
	public static final int FP_RANDOM_HEIGHT_FACTOR		= 57016;//NanoMath.divInt( 87, 100 );
	
	
	// valores mínimos e máximos de cada controle no modo pênalti
	public static final int FP_CONTROL_DIRECTION_MIN_VALUE	= -349177;//NanoMath.mulFixed( FP_REAL_GOAL_WIDTH, NanoMath.divInt( -8, 10 )  );
	public static final int FP_CONTROL_DIRECTION_MAX_VALUE	= -FP_CONTROL_DIRECTION_MIN_VALUE; 
	public static final byte FP_CONTROL_HEIGHT_MIN_VALUE	= 0;
	public static final int FP_CONTROL_HEIGHT_MAX_VALUE		= 383385;//NanoMath.mulFixed( FP_REAL_FLOOR_TO_BAR, NanoMath.divInt( 195, 100 ) );
	
	public static final int FP_CONTROL_DIRECTION_KEEPER_FACTOR	= 29491;//NanoMath.divInt( 45, 100 );
	public static final int FP_CONTROL_HEIGHT_KEEPER_FACTOR		= 17694;//NanoMath.divInt( 27, 100 );
	public static final int FP_CONTROL_DIRECTION_MAX_KEEPER_VALUE	= 157128;//NanoMath.mulFixed( FP_CONTROL_DIRECTION_MAX_VALUE, FP_CONTROL_DIRECTION_KEEPER_FACTOR );
	public static final int FP_CONTROL_HEIGHT_MAX_KEEPER_VALUE	= 103509;//NanoMath.mulFixed( FP_CONTROL_HEIGHT_MAX_VALUE, FP_CONTROL_HEIGHT_KEEPER_FACTOR );
	
	// o ponto mais alto do controle que o jogador visualiza
	public static final int FP_CONTROL_HEIGHT_MAX_VISIBLE	= 263454;//NanoMath.mulFixed( FP_REAL_FLOOR_TO_BAR, NanoMath.divInt( 134, 100 ) );
	public static final int FP_CONTROL_POWER_MIN_VALUE 	= -707796;//NanoMath.mulFixed( FP_REAL_PENALTY_TO_GOAL, NanoMath.divInt( -12, 10 )  );
	public static final int FP_CONTROL_POWER_MAX_VALUE 	= -1592532;//NanoMath.mulFixed( FP_REAL_PENALTY_TO_GOAL, NanoMath.divInt( -27, 10 )  );
	
	private int DIRECTION_PENALTY_MIN_X;
	private int DIRECTION_PENALTY_MAX_X;
	
	private int HEIGHT_PENALTY_MIN_Y;
	private int HEIGHT_PENALTY_MAX_Y;
	
	private int POWER_Y;
	private final int FP_POWER_HEIGHT;

	// passo de tempo em segundos do cálculo dos pontos da curva
	public static final int FP_CURVE_POINT_STEP = 4718;//NanoMath.divInt( 72, 1000 );

	/** Número máximo de itens da coleção. */
	private static final byte CONTROL_MAX_ITEMS = ( 4 + CURVE_N_POINTS );
	
	/** Valor mínimo e máximo da curva. */
	public static final int FP_CURVE_MIN_VALUE	= -NanoMath.ONE;
	public static final int FP_CURVE_MAX_VALUE = NanoMath.ONE;
	
	/** Array que contém as imagens dos pontos do efeito da bola */
	private final Sprite[] curvePoints = new Sprite[ CURVE_N_POINTS ]; 
	
	private final Drawable direction;
	
	private final Drawable height;
	
	private final DrawableImage powerBar;
	
	private final DrawableImage powerLevel;

	// jogada armazenada temporariamente
	private final Play currentPlay = new Play();		
	
	// controle atualmente sendo usado
	private byte currentControl;	

	// TURN_ATTACK/TURN_DEFEND
	private byte turn;
	
	private final Point3f ballDirectionUnit = new Point3f();

	private int fpCurrentValue;
	private int fpCurrentMinValue;
	private int fpCurrentMaxValue;
	private int currentMinPos;
	private int currentMaxPos;
	
	private int fpAccTime;

	private boolean increasing;
	
	private int CURVE_INITIAL_Y;
	
	private final Mutex mutex = new Mutex();
	
	
	/**
	 * 
	 * @param goal 
	 * @param ballPosY 
	 * @throws java.lang.Exception
	 */
	public Control() throws Exception {
		super( CONTROL_MAX_ITEMS );
		//#if SCREEN_SIZE != "SMALL"
		if (!GameMIDlet.isLowMemory()) {
			direction = new Sprite(PATH_IMAGES + "direction");
			direction.defineReferencePixel(direction.getWidth() >> 1, direction.getHeight());
		} else {
			direction = new DrawableImage(PATH_IMAGES + "direction.png");
			direction.defineReferencePixel(direction.getWidth() >> 1, direction.getHeight());
		}
		insertDrawable(direction);

		// insere a seta indicadora de altura
		if (!GameMIDlet.isLowMemory()) {
			height = new Sprite(PATH_IMAGES + "height");

			height.defineReferencePixel(height.getWidth(), height.getHeight() >> 1);
		} else {
			height = new DrawableImage(PATH_IMAGES + "height.png");
			height.mirror(TRANS_MIRROR_H);

			height.defineReferencePixel(height.getWidth(), height.getHeight() >> 1);


		}
		insertDrawable(height );
		//#else
//# 		direction = new DrawableImage(PATH_IMAGES + "direction.png");
//# 		direction.defineReferencePixel(direction.getWidth() >> 1, direction.getHeight());
//# 		insertDrawable(direction);
//#
//# 		// insere a seta indicadora de altura
//# 		height = new DrawableImage(PATH_IMAGES + "height.png");
//# 		height.mirror(TRANS_MIRROR_H);
//#
//# 		height.defineReferencePixel(height.getWidth(), height.getHeight() >> 1);
//# 		insertDrawable(height);
//#
		//#endif
		
		// insere o controle de curva
		final Sprite curve = new Sprite( PATH_IMAGES + "curve.bin", PATH_IMAGES + "ball.png" );
		curve.defineReferencePixel( ANCHOR_CENTER );
		insertDrawable( curve );
		
		curvePoints[ CURVE_N_POINTS - 1 ] = curve;
		
		for ( byte i = CURVE_N_POINTS - 2; i >= 0; --i ) {
			final Sprite s = new Sprite( curve );
			curvePoints[ i ] = s;
			s.defineReferencePixel( s.getWidth() >> 1, s.getHeight() >> 1 );
			insertDrawable( s );
		}
		
		// insere o controle de força
		powerBar = new DrawableImage( PATH_IMAGES + "control_power.png" );
		insertDrawable( powerBar );
		
		FP_POWER_HEIGHT = NanoMath.toFixed( powerBar.getHeight() - ( CONTROL_POWER_BAR_INTERNAL_Y << 1 ) );
		
		powerLevel = new DrawableImage( PATH_IMAGES + "control_power_level.png" );
		powerLevel.defineReferencePixel( ANCHOR_VCENTER | ANCHOR_RIGHT );
		insertDrawable( powerLevel );

		setVisible( false );
	}


	public final void setInfo( Drawable goal, int ballPosY, int totalHeight ) {
		CURVE_INITIAL_Y = ballPosY;
		DIRECTION_PENALTY_MIN_X = goal.getPosX() - direction.getWidth();
		DIRECTION_PENALTY_MAX_X = DIRECTION_PENALTY_MIN_X + goal.getWidth() + ( direction.getWidth() << 1 );
		direction.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, goal.getPosY() );
		
		// insere a seta indicadora de altura
		HEIGHT_PENALTY_MIN_Y = goal.getPosY() + goal.getHeight();
		HEIGHT_PENALTY_MAX_Y = HEIGHT_PENALTY_MIN_Y - NanoMath.divFixed( NanoMath.mulFixed( FP_CONTROL_HEIGHT_MAX_VISIBLE, PENALTY_FLOOR_TO_BAR ), FP_REAL_FLOOR_TO_BAR );
		height.setRefPixelPosition( goal.getPosition() );
		
		setSize( ScreenManager.SCREEN_WIDTH, totalHeight );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );
		powerBar.setPosition( width - powerLevel.getWidth() + ( ( powerLevel.getWidth() - powerBar.getWidth() ) >> 1 ), direction.getPosY()+ direction.getHeight() );
		POWER_Y = powerBar.getPosY() + CONTROL_POWER_BAR_INTERNAL_Y;
	}
	
	
	/**
	 *
	 */
	public final void reset() {
		mutex.acquire();
		
		setVisible( true );
		
		for ( byte i = 0; i < CONTROL_MAX_ITEMS; ++i )
			getDrawable( i ).setVisible( false );
		
		currentControl = CONTROL_DIRECTION;
		
		fpCurrentMinValue = FP_CONTROL_DIRECTION_MIN_VALUE;
		fpCurrentMaxValue = FP_CONTROL_DIRECTION_MAX_VALUE;
		
		currentMinPos = DIRECTION_PENALTY_MIN_X;
		currentMaxPos = DIRECTION_PENALTY_MAX_X;
		
		resetCurrentControlRandomly();
		
		direction.setVisible( true );
		
		mutex.release();
	} // fim do método reset()
	
	
	/**
	 * 
	 * @return 
	 */
	public final boolean setControl() {
		mutex.acquire();
		
		switch ( currentControl ) {
			case CONTROL_DIRECTION:
				// o próximo controle é da altura
				currentPlay.fp_direction = fpCurrentValue;
				
				fpCurrentMinValue = FP_CONTROL_HEIGHT_MIN_VALUE;
				fpCurrentMaxValue = FP_CONTROL_HEIGHT_MAX_VALUE;
				
				currentMinPos = HEIGHT_PENALTY_MIN_Y;
				currentMaxPos = HEIGHT_PENALTY_MAX_Y;
				
				height.setVisible( true );
			break;

			case CONTROL_CURVE:
				// o próximo controle é da força
				currentPlay.fp_curve = fpCurrentValue;
				
				updateCurvePosition();
				
				fpCurrentMinValue = FP_CONTROL_POWER_MIN_VALUE;
				fpCurrentMaxValue = FP_CONTROL_POWER_MAX_VALUE;
				
				powerBar.setVisible( true );
				powerLevel.setVisible( true );
			break;
				
			case CONTROL_HEIGHT:
				// se o jogador estiver defendendo, encerra a jogada; caso esteja atacando, o próximo controle é
				// da curva
				currentPlay.fp_height = fpCurrentValue;
				if ( turn == TURN_DEFEND ) {
					mutex.release();
					return true;
				}
				
				for ( byte i = 0; i < CURVE_N_POINTS; ++i ) {
					curvePoints[ i ].setVisible( true );
					curvePoints[ i ].setSequence( i );
				}
				
				fpCurrentMinValue = FP_CURVE_MIN_VALUE;
				fpCurrentMaxValue = FP_CURVE_MAX_VALUE;
			break;
				
			case CONTROL_POWER:
				// encerrou a jogada
				currentPlay.fp_power = fpCurrentValue;
				
				//#if DEBUG == "true"
				if ( turn != TURN_ATTACK )
					System.out.println( "turno inválido para controle de força!" );
				//#endif
				mutex.release();
			return true;
			
			case CONTROL_DONE:
				//#if DEBUG == "true"
				default:
					System.out.println( "Estado inválido do controle: " + currentControl );
			//#endif
				mutex.release();
				return false;
		} // fim switch ( currentControl )
		
		resetCurrentControlRandomly();
		currentControl = ( byte ) ( ( currentControl + 1 ) % CONTROL_TOTAL );
		
		mutex.release();
		
		update( 0 );
		
		return false;
	} // fim do método setControl()
	
	
	private final void resetCurrentControlRandomly() {
		fpAccTime = 0;
		increasing = NanoMath.randInt( 100 ) < 50;
		fpCurrentValue = fpCurrentMinValue + NanoMath.mulFixed( NanoMath.randFixed(), ( fpCurrentMaxValue - fpCurrentMinValue ) );
	}
	
	
	/**
	 * Gera uma jogada aleatória, a partir da média de duas jogadas aleatórias, para reduzir
	 * o número de chutes para fora do computador.
	 * 
	 * @return 
	 */
	public final Play getRandomPlay() {
		final Play randomPlay = new Play();
		
		int dirMinValue = FP_CONTROL_DIRECTION_MIN_VALUE;
		int dirMaxValue = FP_CONTROL_DIRECTION_MAX_VALUE;
		int heightMinValue = FP_CONTROL_HEIGHT_MIN_VALUE;
		int heightMaxValue = FP_CONTROL_HEIGHT_MAX_VALUE;
		int powerMinValue = FP_CONTROL_POWER_MIN_VALUE;
		int powerMaxValue = FP_CONTROL_POWER_MAX_VALUE;
		int curveMaxValue = FP_CURVE_MAX_VALUE;
		int curveMinValue = FP_CURVE_MIN_VALUE;
		
		final Play temp = new Play();
		
		final int FP_2 = NanoMath.toFixed( 2 );
		
		temp.fp_direction =  dirMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), dirMaxValue - dirMinValue ) );
		
		randomPlay.fp_direction =  dirMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), dirMaxValue - dirMinValue ) );
		randomPlay.fp_direction = NanoMath.divFixed( NanoMath.mulFixed( FP_RANDOM_DIRECTION_FACTOR, ( randomPlay.fp_direction + temp.fp_direction ) ), FP_2 );
		
		temp.fp_height =  heightMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), heightMaxValue - heightMinValue ) );
		
		randomPlay.fp_height =  heightMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), heightMaxValue - heightMinValue ) );
		randomPlay.fp_height = NanoMath.divFixed( NanoMath.mulFixed( FP_RANDOM_HEIGHT_FACTOR, ( randomPlay.fp_height + temp.fp_height ) ), FP_2 );
		
		temp.fp_curve =  curveMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), curveMaxValue - curveMinValue ) );
		
		randomPlay.fp_curve =  curveMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), curveMaxValue - curveMinValue ) );
		randomPlay.fp_curve = NanoMath.divFixed( randomPlay.fp_curve + temp.fp_curve, NanoMath.toFixed( 2 ) );
		
		randomPlay.fp_power = powerMinValue + ( NanoMath.mulFixed( NanoMath.randFixed(), powerMaxValue - powerMinValue ) );
		
		return randomPlay;
	} // fim do método getRandomPlay()
	
	
	public final void update( int delta ) {
		super.update( delta );
		
		// converte o tempo de milisegundos para segundos
		final int fp_delta = NanoMath.divInt( delta, 1000 );
		
		switch ( currentControl ) {
			case CONTROL_DIRECTION:
			case CONTROL_HEIGHT:
			case CONTROL_CURVE:
				mutex.acquire();
				
				fpAccTime += fp_delta;
		
				if ( increasing ) {
					fpCurrentValue = fpCurrentMinValue + NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpCurrentMaxValue - fpCurrentMinValue ), FP_CONTROL_TIME );
					if ( fpCurrentValue >= fpCurrentMaxValue ) {
						increasing = !increasing;
						fpAccTime = 0;
					}
				} else {
					fpCurrentValue = fpCurrentMaxValue - NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpCurrentMaxValue - fpCurrentMinValue ), FP_CONTROL_TIME );
					if ( fpCurrentValue <= fpCurrentMinValue ) {
						increasing = !increasing;
						fpAccTime = 0;
					}
				}
				
				fpCurrentValue = NanoMath.clamp( fpCurrentValue, fpCurrentMinValue, fpCurrentMaxValue );
				if ( currentControl == CONTROL_CURVE ) {
					// calcula o valor do coeficiente de efeito
					updateCurvePosition();
				}
				
				mutex.release();
			break;
			
			case CONTROL_POWER:
				mutex.acquire();
				
				fpAccTime = ( fpAccTime + fp_delta ) % FP_CONTROL_TIME;
				fpCurrentValue = fpCurrentMinValue + NanoMath.divFixed( NanoMath.mulFixed( fpAccTime, fpCurrentMaxValue - fpCurrentMinValue ), FP_CONTROL_TIME );
				
				mutex.release();
			break;
			
			default:
			return;
		} // fim switch ( currentControl )

		// atualiza apenas a posição do controle atual
		final int fp_diffPos = NanoMath.toFixed( currentMaxPos - currentMinPos );

		// atualiza posição
		switch ( currentControl ) {
			case CONTROL_POWER:
				powerLevel.setRefPixelPosition( ScreenManager.SCREEN_WIDTH, POWER_Y + NanoMath.toInt( 
								NanoMath.divFixed( NanoMath.mulFixed( FP_CONTROL_POWER_MAX_VALUE - fpCurrentValue, FP_POWER_HEIGHT ), 
										   FP_CONTROL_POWER_MAX_VALUE - FP_CONTROL_POWER_MIN_VALUE ) ) );
			break;

			case CONTROL_DIRECTION:
				direction.setRefPixelPosition( ( ( currentMinPos + currentMaxPos ) >> 1 ) + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fpCurrentValue, fp_diffPos ), fpCurrentMaxValue - fpCurrentMinValue ) ), 
						direction.getRefPixelY() );
			break;

			case CONTROL_HEIGHT:
				height.setRefPixelPosition( height.getRefPixelX(), currentMinPos + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( fpCurrentValue, fp_diffPos ), fpCurrentMaxValue - fpCurrentMinValue ) ) );
			break;
		}
	} // fim do método update( int )
	
	
	public final void updateCurvePosition() {
		// calcula o valor do coeficiente de efeito
		final Point3f yVector = new Point3f( 0, NanoMath.ONE, 0 );
		final Point3f curveVector = new Point3f();
		final Point3f pointPosition = new Point3f();
		ballDirectionUnit.set( -currentPlay.fp_direction, 0, FP_REAL_PENALTY_TO_GOAL  );
		ballDirectionUnit.normalize();		
		int fp_time;
		
		for ( byte i = 0; i < CURVE_N_POINTS; ++i ) {
			fp_time = NanoMath.mulFixed( NanoMath.toFixed( i + 1 ), FP_CURVE_POINT_STEP );
			
			curveVector.set( ballDirectionUnit.cross( yVector ).mul( NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.mulFixed( FP_CONTROL_POWER_MAX_VALUE, fpCurrentValue ), FP_BALL_CURVE_FACTOR ), fp_time ) ) );
			pointPosition.set( ballDirectionUnit.mul( NanoMath.mulFixed( FP_CONTROL_POWER_MIN_VALUE, fp_time ) ).add( curveVector.mul( NanoMath.mulFixed( fp_time, fp_time ) ) ) );
			
			curvePoints[ i ].setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( pointPosition.x, FP_PENALTY_GOAL_WIDTH ), FP_REAL_GOAL_WIDTH ) ),
						CURVE_INITIAL_Y + NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( pointPosition.z, FP_PENALTY_MARK_TO_GOAL ), FP_REAL_PENALTY_TO_GOAL ) ) );
		}
	} // fim do método updateCurvePosition()
	
	
	public final void setTurn( byte t ) { 
		turn = t; 
	}
	 
	 
	public final byte getTurn() { 
		return turn; 
	}
	 
	 
	public byte getCurrentControl() { 
		return currentControl; 
	}	
	 
	 
	public final Play getCurrentPlay() {
		return currentPlay;
	}


    @Override
	public final boolean contains( int x, int y ) {
		return currentControl != CONTROL_DONE && currentControl != CONTROL_NONE && super.contains( x, y );
	}
	
	
	/**
	 * Obtém a posição y do controle de direção.
	 */
	public final int getDirectionIconPosY() {
		return direction.getPosY();
	}


	public final int getPowerPosY() {
		return powerBar.getPosY();
	}
	
	
}
