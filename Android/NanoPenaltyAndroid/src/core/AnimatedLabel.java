/**
 * AnimatedLabel.java
 *
 * Created on Jun 3, 2010 8:54:35 PM
 *
 */

package core;

import br.com.nanogames.components.Label;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import javax.microedition.lcdui.Graphics;
import screens.Match;

/**
 *
 * @author peter
 */
public final class AnimatedLabel extends MarqueeLabel implements Constants, Updatable {

	public static final short ANIMATION_TIME = Match.TIME_BOARD_ANIMATION;

	private int start;

	private final MUV angleSpeed = new MUV( 280 );

	private static final byte ANGLE_STEP = -20;

	private final int FP_MAX_OFFSET_Y;

	private final Border border;


	public AnimatedLabel( Border b ) throws Exception {
		super( FONT_INDEX_DEFAULT, null );
		this.border = b;
		FP_MAX_OFFSET_Y = NanoMath.toFixed( getFont().getHeight() >> 1 );

		setVisible( false );
	}


	public final void update( int delta ) {
		if ( isVisible() ) {
			start += angleSpeed.updateInt( delta );
			super.update( delta );
		}
	}


	public final void setSize( int width, int height ) {
		super.setSize( /*width*/ScreenManager.SCREEN_WIDTH, Math.max( height, getInternalHeight() ) );
//		setPosition( 0, ( getHeight() >> 1 ) );
		if ( border != null ) {
			border.setSize( /*width*/ScreenManager.SCREEN_WIDTH, getInternalHeight() );
			border.setPosition( getPosX(), getPosY() + BORDER_OFFSET );
		}
	}


	public final void drawString( Graphics g, char[] text, int x, int y ) {
		// não desenha caracteres que estejam fora da área de clip horizontal
		final int limitLeft = getClip().x;
		final int limitRight = limitLeft + getClip().width;

		final int textLength = text.length;

		int i = 0;

		y += getFont().getHeight();

		int currentValue = start;
		for ( ; i < textLength && x < limitRight; ++i ) {
			final int currentY = y + Math.min( NanoMath.toInt( NanoMath.mulFixed( NanoMath.sinInt( currentValue ), FP_MAX_OFFSET_Y ) ), 0 );
			currentValue += ANGLE_STEP;

			// o teste da largura do caracter é usado para evitar chamadas desnecessárias de funções no caso
			// de caracteres não presentes na fonte (são ignorados)
			final char c = text[ i ];
			// aqui a largura tem que ser a do caracter somente (sem o offset extra)
			final int charWidth = font.getCharWidth( c );

			if ( charWidth > 0 && ( x + charWidth >= limitLeft ) ) {
				// faz a interseção da área de clip do caracter com a área de clip do texto todo
				pushClip( g );

				g.clipRect( x, currentY, charWidth, font.getHeight() );
				g.drawImage( font.getImage(), x - font.getCharOffset( c ), currentY, 0 );

				// restaura a área de clip total
				popClip( g );
			} // fim if ( charWidth > 0 && ( x + charWidth >= limitLeft ) )

			x += font.getCharWidth( c );
		} // fim for ( int i = 0; i < textArray.length && x < limitRight; ++i )
	}


	public final void showText( String text ) {
		setText( text, true );
		setTextOffset( getWidth() );
		setSpeed( -( getWidth() + getFont().getTextWidth( text ) ) * 1000 / ANIMATION_TIME );
		setVisible( true );
	}


	public final void setVisible( boolean visible ) {
		super.setVisible( visible );
        border.setVisible( visible );
	}


	public final int getInternalHeight() {
		return getFont().getHeight() << 1;
	}


}
