/**
 * Keeper.java
 * ©2008 Nano Games.
 *
 * Created on Mar 24, 2008 11:15:25 AM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteChanger;
import br.com.nanogames.components.util.PaletteMap;
import br.com.nanogames.components.util.Point3f;
import br.com.nanogames.components.util.Quad;


/**
 * 
 * @author Peter
 */
public final class Keeper extends Player implements SpriteListener {
	
	/** Sequência do goleiro parado, esperando a cobrança. */
	public static final byte SEQUENCE_STOPPED			= 0;
	/** Sequência do goleiro preparando-se para um pulo na região horizontal central do gol. */
	public static final byte SEQUENCE_PREPARING_MIDDLE	= 1;
	/** Sequência do goleiro preparando-se para um pulo nos cantos, ou para desistir. */
	public static final byte SEQUENCE_PREPARING_SIDE	= 2;
	/** Sequência do pulo alto e no meio do gol. */
	public static final byte SEQUENCE_JUMP_CENTER_HIGH	= 3;
	/** Sequência do pulo baixo e no meio do gol. */
	public static final byte SEQUENCE_JUMP_CENTER_LOW	= 4;
	/** Sequência do pulo alto e no canto. */
	public static final byte SEQUENCE_JUMP_SIDE_HIGH	= 5;
	/** Sequência do pulo à meia altura e no canto. */
	public static final byte SEQUENCE_JUMP_SIDE_MIDDLE	= 6;
	/** Sequência do pulo rasteiro e no canto. */
	public static final byte SEQUENCE_JUMP_SIDE_LOW		= 7;
	/** Sequência do goleiro desistindo da bola (pulou para o lado contrário ao da bola). */
	public static final byte SEQUENCE_GIVE_UP			= 8;
	/** Sequência do goleiro quicando após bater no chão. */
	public static final byte SEQUENCE_BOUNCING_UP		= 9;
	/** Sequência do goleiro caindo após quicar no chão. */
	public static final byte SEQUENCE_BOUNCING_DOWN		= 10;
	/** Sequência do goleiro deitado no chão. */
	public static final byte SEQUENCE_LAY_DOWN			= 11;
	/** Sequência do goleiro caindo de bunda. */
	public static final byte SEQUENCE_FALLING			= 12;
	
	
	/** Tipo do pulo atual. Valores válidos:
	 * <ul>
	 * <li>SEQUENCE_JUMP_CENTER_HIGH</li>
	 * <li>SEQUENCE_JUMP_CENTER_LOW</li>
	 * <li>SEQUENCE_JUMP_SIDE_HIGH</li>
	 * <li>SEQUENCE_JUMP_SIDE_MIDDLE</li>
	 * <li>SEQUENCE_JUMP_SIDE_LOW</li>
	 * <li>SEQUENCE_GIVE_UP</li>
	 * </ul>
	 */
	private byte jumpType;

	// estados do pulo do goleiro
	/** Estágio do pulo: nenhum pulo. */
	private static final byte JUMP_STATE_NONE				= 0;
	/** Estágio do pulo: subindo. */
	private static final byte JUMP_STATE_RISING				= 1;
	/** Estágio do pulo: caindo. */
	private static final byte JUMP_STATE_FALLING			= 2;
	/** Estágio do pulo: subindo após primeiro quique no chão. */
	private static final byte JUMP_STATE_BOUNCING_UP_1		= 3;
	/** Estágio do pulo: caindo após primeiro quique no chão. */
	private static final byte JUMP_STATE_BOUNCING_DOWN_1	= 4;
	/** Estágio do pulo: subindo após segundo quique no chão (somente SEQUENCE_JUMP_CENTER_HIGH ou SEQUENCE_JUMP_SIDE_HIGH). */
	private static final byte JUMP_STATE_BOUNCING_UP_2		= 5;
	/** Estágio do pulo: caindo após segundo quique no chão (somente SEQUENCE_JUMP_CENTER_HIGH ou SEQUENCE_JUMP_SIDE_HIGH). */
	private static final byte JUMP_STATE_BOUNCING_DOWN_2	= 6;
	/** Estágio do pulo: freando o movimento, já em contato com o chão (somente SEQUENCE_JUMP_CENTER_LOW ou SEQUENCE_JUMP_SIDE_LOW). */
	private static final byte JUMP_STATE_BRAKING			= 7;
	/** Estágio do pulo: movimento do pulo encerrado. */
	private static final byte JUMP_STATE_FINISHED			= 8;
	
	private byte jumpState;
	
	
	/** 
	 * Porcentagem da maior posição horizontal possível numa jogada considerada como região central do gol (separa
	 * os tipos de pulo CENTER dos SIDE).
	 */
	private static final int FP_REGION_H_MIDDLE_PERCENT = 24903;//NanoMath.divInt( 38, 100 );
	
	/** Porcentagem da altura máxima de uma jogada que separa os tipos de pulo SEQUENCE_JUMP_CENTER_HIGH e SEQUENCE_JUMP_CENTER_LOW. */
	private static final int FP_REGION_V_MIDDLE_PERCENT = 41943;//NanoMath.divInt( 64, 100 );
	
	/** Porcentagem máxima da altura de uma jogada para que o pulo seja do tipo SEQUENCE_JUMP_SIDE_LOW. */
	private static final int FP_REGION_SIDE_BOTTOM_PERCENT = 22282;//NanoMath.divInt( 34, 100 );
	
	/** Porcentagem da altura máxima de uma jogada que separa os tipos de pulo SEQUENCE_JUMP_SIDE_MIDDLE e SEQUENCE_JUMP_SIDE_HIGH. */
	private static final int FP_REGION_SIDE_MIDDLE_PERCENT = 43909;//NanoMath.divInt( 67, 100 );
	
	/** Porcentagem das bolas em que o goleiro desiste ao perceber que pulou para o lado contrário ao da bola. */
	private static final byte GIVE_UP_PERCENT = 60;
	
	/** Indica se o goleiro desistiu do pulo na última jogada (valor utilizado para que o replay seja igual à última jogada). */
	private boolean gaveUpLastPlay;
	
	public static final byte GIVE_UP_BEHAVIOUR_RANDOM		= 0;
	public static final byte GIVE_UP_BEHAVIOUR_LAST_PLAY	= 1;
	
	/** Porcentagem da velocidade x do pulo no caso do goleiro desistir. */
	private static final int FP_GIVE_UP_JUMP_X_FACTOR		= 24248;//NanoMath.divInt( 37, 100 );
	
	/** Porcentagem da velocidade y do pulo no caso do goleiro desistir. */
	private static final int FP_GIVE_UP_JUMP_Y_FACTOR		= 54394;//NanoMath.divInt( 83, 100 );
	
	// valores dos vetores para cada ângulo de inclinação do goleiro (usados para posicionar
	// corretamente a área de colisão do goleiro)
	private static final int FP_SQRT_3_DIV_2		= 54318;//NanoMath.divInt( 866025, 1000000 );
	private static final int FP_SQRT_2_DIV_2		= 22016;//NanoMath.divInt( 70710678, 100000000 );
	private static final int FP_HALF				= NanoMath.HALF;
	
	public static final Point3f VECTOR_UP_90			= new Point3f( 0, NanoMath.ONE, 0 );
	public static final Point3f VECTOR_RIGHT_90			= new Point3f( NanoMath.ONE, 0, 0 );

	public static final Point3f VECTOR_UP_60			= new Point3f( FP_HALF, FP_SQRT_3_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_60			= new Point3f( FP_SQRT_3_DIV_2, -FP_HALF, 0 );
	public static final Point3f VECTOR_UP_60_LEFT		= new Point3f( -FP_HALF, FP_SQRT_3_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_60_LEFT	= new Point3f( FP_SQRT_3_DIV_2, FP_HALF, 0 );

	public static final Point3f VECTOR_UP_45			= new Point3f( FP_SQRT_2_DIV_2, FP_SQRT_2_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_45			= new Point3f( FP_SQRT_2_DIV_2, -FP_SQRT_2_DIV_2, 0 );
	public static final Point3f VECTOR_UP_45_LEFT		= new Point3f( -FP_SQRT_2_DIV_2, FP_SQRT_2_DIV_2, 0 );
	public static final Point3f VECTOR_RIGHT_45_LEFT	= VECTOR_UP_45;

	public static final Point3f VECTOR_UP_30			= VECTOR_RIGHT_60_LEFT;
	public static final Point3f VECTOR_RIGHT_30			= new Point3f( FP_HALF, -FP_SQRT_3_DIV_2, 0 );
	public static final Point3f VECTOR_UP_30_LEFT		= new Point3f( -FP_SQRT_3_DIV_2, FP_HALF, 0 );
	public static final Point3f VECTOR_RIGHT_30_LEFT	= VECTOR_UP_60;

	public static final Point3f VECTOR_UP_0				= VECTOR_RIGHT_90;
	public static final Point3f VECTOR_RIGHT_0			= new Point3f( 0, -NanoMath.ONE, 0 );
	public static final Point3f VECTOR_UP_0_LEFT		= new Point3f( -NanoMath.ONE, 0, 0 );
	public static final Point3f VECTOR_RIGHT_0_LEFT		= VECTOR_UP_90;

	// largura real do goleiro com os braços próximos ao corpo
	public static final int FP_REAL_KEEPER_WIDTH		= NanoMath.ONE;
	
	/** largura real do goleiro com os braços abertos */
	public static final int FP_REAL_KEEPER_WIDTH_ARMS	= 108134;//NanoMath.divInt( 165, 100 );
	
	/** Altura do goleiro. */
	public static final int FP_REAL_KEEPER_HEIGHT		= 124518;//NanoMath.divInt( 190, 100 );
	
	/** Altura do goleiro abaixado. */
	public static final int FP_REAL_KEEPER_HEIGHT_DOWN	= 104857;//NanoMath.divInt( 160, 100 );
	
	/** altura real do goleiro com os braços esticados. */
	public static final int FP_REAL_KEEPER_HEIGHT_ARMS	= 172222;//NanoMath.divInt( 265, 100 );
	
	/** Duração mínima do pulo em segundos. */
	private static final int FP_MIN_JUMP_TIME			= 53149;//NanoMath.divInt( 811, 1000 );	
	
	/** Tempo de frenagem do pulo atual. */
	private int fp_currentBrakingTime;
	
	/** Aceleração causada pelo atrito com o chão, quando o pulo está no estado JUMP_STATE_BRAKING. */
	private static final int FP_BRAKE_ACCELERATION = -294806;//NanoMath.divFixed( -Control.FP_CONTROL_DIRECTION_MAX_KEEPER_VALUE, FP_MAX_BRAKING_TIME );
	
	/** Retângulo que define a área de colisão no espaço 3D. */
	private final Quad realCollisionArea = new Quad();
	
	/** Posição real do goleiro. */
	private final Point3f realPosition = new Point3f();
	
	/** Posição real inicial do goleiro. */
	private final Point3f realInitialPosition = new Point3f();
	
	/** Velocidade real inicial do pulo. */
	private final Point3f jumpSpeed = new Point3f();
	
	/** Velocidade real vertical instantânea. */
	private int fp_instantYSpeed;
	
	private int fp_accRealTime;
	
	private int INITIAL_X;
	private int INITIAL_Y;
	
	/** 
	 * Nível de dificuldade do goleiro da CPU (porcentagem de jogadas em que ele pula para o mesmo lado da bola). Caso
	 * seja menor ou igual a zero, o comportamento é normal (aleatório).
	 */
	private byte difficultyLevel;


	public Keeper( int teamIndex ) throws Exception {
		super( teamIndex, PLAYER_INDEX_KEEPER );
	}
	
	
	public Keeper( int teamIndex, boolean loadAll ) throws Exception {
		super( teamIndex, PLAYER_INDEX_KEEPER, loadAll );
	}


	protected void insertDrawable( Sprite s ) {
		super.insertDrawable( s );
		if ( getUsedSlots() == 1 ) {
			// acabou de inserir o sprite do goleiro em si
			defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_HCENTER );
		}
	}


	/**
	 * Obtém o quad de colisão do goleiro na posição atual.
	 * 
	 * @return quad representando a área de colisão atual.
	 */
	public final Quad getCollisionArea() {
		Point3f up = null;
		Point3f right = null;
		int fp_width = 0;
		int fp_height = 0;
		final boolean mirrored = getTransform() == TRANS_MIRROR_H;
		byte anchor = Quad.ANCHOR_BOTTOM;
		
		switch ( sprite.getSequenceIndex() ) {
			case SEQUENCE_STOPPED:
			case SEQUENCE_PREPARING_MIDDLE:
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT;
			break;

			case SEQUENCE_PREPARING_SIDE:
				if ( mirrored ) {
					up = VECTOR_UP_60_LEFT;
					right = VECTOR_RIGHT_60_LEFT;
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				} else {
					up = VECTOR_UP_60;
					right = VECTOR_RIGHT_60;
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT;
			break;
			
			case SEQUENCE_JUMP_CENTER_HIGH:
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT;
			break;
			
			case SEQUENCE_JUMP_CENTER_LOW:
				if ( mirrored ) {
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				} else {
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				}				
				
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT_DOWN;
			break;
			
			case SEQUENCE_JUMP_SIDE_HIGH:
				if ( mirrored ) {
					up = VECTOR_UP_60_LEFT;
					right = VECTOR_RIGHT_60_LEFT;
				} else {
					up = VECTOR_UP_60;
					right = VECTOR_RIGHT_60;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
			break;
			
			case SEQUENCE_JUMP_SIDE_MIDDLE:
				if ( mirrored ) {
					up = VECTOR_UP_30_LEFT;
					right = VECTOR_RIGHT_30_LEFT;
				} else {
					up = VECTOR_UP_30;
					right = VECTOR_RIGHT_30;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
			break;
			
			case SEQUENCE_JUMP_SIDE_LOW:
			case SEQUENCE_LAY_DOWN:
				if ( mirrored ) {
					up = VECTOR_UP_0_LEFT;
					right = VECTOR_RIGHT_0_LEFT;
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				} else {
					up = VECTOR_UP_0;
					right = VECTOR_RIGHT_0;
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				}
				
				fp_width = FP_REAL_KEEPER_WIDTH;
				fp_height = FP_REAL_KEEPER_HEIGHT_ARMS;
			break;
			
			case SEQUENCE_GIVE_UP:
			case SEQUENCE_BOUNCING_UP:
			case SEQUENCE_BOUNCING_DOWN:
				if ( mirrored ) {
					anchor = Quad.ANCHOR_BOTTOM_RIGHT;
				} else {
					anchor = Quad.ANCHOR_BOTTOM_LEFT;
				}
				up = VECTOR_UP_90;
				right = VECTOR_RIGHT_90;
				fp_width = FP_REAL_KEEPER_WIDTH_ARMS;
				fp_height = FP_REAL_KEEPER_HEIGHT_DOWN;
			break;
		}
		
		realCollisionArea.setValues( realPosition, anchor, up, right, fp_width, fp_height );
		return realCollisionArea;
	}


	public final boolean getLastGiveUp() {
		return gaveUpLastPlay;
	}


	/**
	 * Prepara o goleiro para um pulo.
	 * 
	 * @param keeperPlay direção e altura do pulo do goleiro.
	 * @param shooterPlay jogada realizada pelo batedor. Essa jogada é utilizada para definir se o goleiro irá completar
	 * seu pulo, ou se irá desistir no meio, caso perceba que escolheu o canto errado.
	 * @param replay indica se é a repetição da jogada anterior.
	 */
	public final void jump( Play keeperPlay, Play shooterPlay, boolean replay ) {
		if ( !replay ) {
			keeperPlay.fp_direction = NanoMath.mulFixed( keeperPlay.fp_direction, Control.FP_CONTROL_DIRECTION_KEEPER_FACTOR );
			keeperPlay.fp_height = NanoMath.mulFixed( keeperPlay.fp_height, Control.FP_CONTROL_HEIGHT_KEEPER_FACTOR );
			
			final int rnd = NanoMath.randInt( 100 );
			if ( rnd < difficultyLevel ) {
				// goleiro pula para o mesmo lado da bola
				if ( NanoMath.sgn( keeperPlay.fp_direction ) != NanoMath.sgn( shooterPlay.fp_direction ) )
					keeperPlay.fp_direction = -keeperPlay.fp_direction;
			}
		}
		
		final int fp_horizontalPercent = Math.abs( NanoMath.divFixed( keeperPlay.fp_direction, Control.FP_CONTROL_DIRECTION_MAX_KEEPER_VALUE ) );
		final int fp_verticalPercent = NanoMath.divFixed( keeperPlay.fp_height, Control.FP_CONTROL_HEIGHT_MAX_KEEPER_VALUE );
		
		setTransform( keeperPlay.fp_direction < 0 ? TRANS_MIRROR_H : TRANS_NONE );
		
		if ( fp_horizontalPercent < FP_REGION_H_MIDDLE_PERCENT ) {
			// goleiro realizou um pulo na região horizontal central do gol
			
			if ( fp_verticalPercent < FP_REGION_V_MIDDLE_PERCENT ) {
				// goleiro fez um pulo rasteiro e no meio
				jumpType = SEQUENCE_JUMP_CENTER_LOW;
				calculateJumpSpeed( keeperPlay.fp_direction, 0 );
			} else {
				// pulo do goleiro é alto e no meio
				jumpType = SEQUENCE_JUMP_CENTER_HIGH;
				calculateJumpSpeed( keeperPlay.fp_direction, keeperPlay.fp_height );
			}
			
			setSequence( SEQUENCE_PREPARING_MIDDLE );
		} else {
			// goleiro realiza um pulo nos cantos, ou desiste. A primeira verificação é a da direção da bola - caso
			// seja contrária à direção do pulo do goleiro, sorteia seu comportamento (finaliza o pulo ou desiste).
			
			gaveUpLastPlay = replay ? gaveUpLastPlay : ( ( ( shooterPlay.fp_direction > 0 && keeperPlay.fp_direction < 0 ) || ( shooterPlay.fp_direction < 0 && keeperPlay.fp_direction > 0 ) ) 
								&& NanoMath.randInt( 100 ) < GIVE_UP_PERCENT );

			if ( gaveUpLastPlay ) {
				// goleiro percebeu que pulou para o lado contrário ao da bola e desistiu do pulo.
				jumpType = SEQUENCE_GIVE_UP;
				
				calculateJumpSpeed( NanoMath.mulFixed( keeperPlay.fp_direction, FP_GIVE_UP_JUMP_X_FACTOR  ),
									NanoMath.mulFixed( keeperPlay.fp_height, FP_GIVE_UP_JUMP_Y_FACTOR  ) );
			} else {
				// goleiro pulou para a direção certa, ou então segue seu pulo mesmo após perceber que não terá sucesso.
				if ( fp_verticalPercent < FP_REGION_SIDE_BOTTOM_PERCENT ) {
					// goleiro deu um pulo rasteiro e no canto
					jumpType = SEQUENCE_JUMP_SIDE_LOW;
				} else if ( fp_verticalPercent < FP_REGION_SIDE_MIDDLE_PERCENT ) {
					// goleiro realizou um pulo à meia altura e no canto
					jumpType = SEQUENCE_JUMP_SIDE_MIDDLE;
				} else {
					// o pulo do goleiro é do tipo alto e no canto
					jumpType = SEQUENCE_JUMP_SIDE_HIGH;
				}
				
				calculateJumpSpeed( keeperPlay.fp_direction, keeperPlay.fp_height );
			}
			
			setSequence( SEQUENCE_PREPARING_SIDE );
		}
		
		//#if DEBUG == "true"
		System.out.println( "GOLEIRO: " + NanoMath.toString( fp_horizontalPercent ) + " -  " + NanoMath.toString( fp_verticalPercent ) + " -  " + jumpType );
		//#endif
		
	}


	public final void onSequenceEnded( int id, int sequence ) {
		switch ( sequence ) {
			case SEQUENCE_PREPARING_MIDDLE:
			case SEQUENCE_PREPARING_SIDE:
				setSequence( jumpType );
				
				switch ( jumpType ) {
					case SEQUENCE_JUMP_CENTER_LOW:
					case SEQUENCE_JUMP_SIDE_LOW:
					case SEQUENCE_GIVE_UP:
						setJumpState( JUMP_STATE_BRAKING );
					break;
					
					default:
						setJumpState( JUMP_STATE_RISING );
				}
				
			break;
		}
	}
	
	
	public final void reset() {
		super.reset();
		fp_accRealTime = 0;
		setTransform( TRANS_NONE );
		setSequence( SEQUENCE_STOPPED );
		
		realPosition.set();
		realInitialPosition.set();
		
		jumpSpeed.set();
		jumpType = -1;
		jumpState = JUMP_STATE_NONE;
		fp_instantYSpeed = 0;
		fp_currentBrakingTime = 0;

		refreshScreenPosition();
	}


	public final void setDifficultyLevel( int difficultyLevel ) {
		this.difficultyLevel = ( byte ) difficultyLevel;
	}
	
	
	private final void setJumpState( int state ) {
		switch ( state ) {
			case JUMP_STATE_BOUNCING_UP_1:
			case JUMP_STATE_BOUNCING_UP_2:
				realInitialPosition.x = realPosition.x;
				jumpSpeed.x = NanoMath.mulFixed( jumpSpeed.x, NanoMath.HALF );
				jumpSpeed.y = NanoMath.mulFixed( jumpSpeed.y, NanoMath.divInt( 60, 100 ) );
				fp_accRealTime = 0;
				
				if ( jumpType == SEQUENCE_JUMP_CENTER_HIGH )
					setSequence( SEQUENCE_FALLING );
				else
					setSequence( SEQUENCE_BOUNCING_UP );
			break;
			
			case JUMP_STATE_BOUNCING_DOWN_1:
			case JUMP_STATE_BOUNCING_DOWN_2:
				if ( jumpType == SEQUENCE_JUMP_CENTER_HIGH )
					setSequence( SEQUENCE_FALLING );
				else
					setSequence( SEQUENCE_BOUNCING_DOWN );
			break;
			
			case JUMP_STATE_BRAKING:
				realInitialPosition.x = realPosition.x;
				realPosition.y = 0;
				realInitialPosition.y = 0;
				fp_currentBrakingTime = Math.abs( NanoMath.divFixed( jumpSpeed.x, FP_BRAKE_ACCELERATION ) );
			break;
			
			case JUMP_STATE_FALLING:
				nextFrame();
			break;
			
			case JUMP_STATE_FINISHED:
				switch ( jumpType ) {
					case SEQUENCE_GIVE_UP:
					case SEQUENCE_JUMP_CENTER_LOW:
					break;
					
					case SEQUENCE_JUMP_CENTER_HIGH:
						nextFrame();
					break;
					
					default:
						setSequence( SEQUENCE_LAY_DOWN );
				}
			break;
		}
		
		jumpState = ( byte ) state;
	}
	
	
	private final void calculateJumpSpeed( int fp_direction, int fp_height ) {
		jumpSpeed.y = NanoMath.sqrtFixed( NanoMath.mulFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), FP_GRAVITY_ACCELERATION ), fp_height ) );

		int fp_jumpTime = NanoMath.divFixed( NanoMath.mulFixed( NanoMath.toFixed( -2 ), jumpSpeed.y ), FP_GRAVITY_ACCELERATION );
		if ( fp_jumpTime < FP_MIN_JUMP_TIME )
			fp_jumpTime = FP_MIN_JUMP_TIME;
		
		jumpSpeed.x = NanoMath.divFixed( fp_direction, fp_jumpTime );
	}
	
	
	public final void update( int delta ) {
		if ( match != null ) {
			delta = match.getDelta();

			super.update( delta );

			// converte o intervalo de atualização de milisegundos para segundos
			delta = NanoMath.divInt( delta, 1000 );

			switch ( jumpState ) {
				case JUMP_STATE_RISING:
					updateRealPosition( delta );
					if ( fp_instantYSpeed <= 0 )
						setJumpState( JUMP_STATE_FALLING );
				break;

				case JUMP_STATE_BOUNCING_UP_1:
				case JUMP_STATE_BOUNCING_UP_2:
					updateRealPosition( delta );
					if ( fp_instantYSpeed <= 0 )
						setJumpState( jumpState + 1 );
				break;

				case JUMP_STATE_FALLING:
					updateRealPosition( delta );
					if ( realPosition.y <= 0 ) {
						switch ( jumpType ) {
							case SEQUENCE_JUMP_CENTER_HIGH:
							case SEQUENCE_JUMP_SIDE_HIGH:
							case SEQUENCE_JUMP_SIDE_MIDDLE:
								setJumpState( JUMP_STATE_BOUNCING_UP_2 );
//							break;

							case SEQUENCE_JUMP_CENTER_LOW:
							case SEQUENCE_JUMP_SIDE_LOW:
								setJumpState( JUMP_STATE_BRAKING );
							break;
						}
					}
				break;

				case JUMP_STATE_BOUNCING_DOWN_1:
					updateRealPosition( delta );
					if ( realPosition.y <= 0 ) {
						switch ( jumpType ) {
							case SEQUENCE_JUMP_SIDE_HIGH:
							case SEQUENCE_JUMP_CENTER_HIGH:
								setJumpState( JUMP_STATE_BOUNCING_UP_2 );
							break;

							case SEQUENCE_JUMP_SIDE_MIDDLE:
								setJumpState( JUMP_STATE_FINISHED );
							break;
						}
					}
				break;

				case JUMP_STATE_BOUNCING_DOWN_2:
					updateRealPosition( delta );
					if ( realPosition.y <= 0 )
						setJumpState( JUMP_STATE_FINISHED );
				break;

				case JUMP_STATE_BRAKING:
					// calcula a posição x real atual do goleiro (não é necessário atualizar a posição y)
					fp_accRealTime += delta;

					if ( fp_accRealTime > fp_currentBrakingTime )
						fp_accRealTime = fp_currentBrakingTime;

					// x = x0 + v0t + at²/2;
					final int fp_t2_div2 = NanoMath.mulFixed( NanoMath.mulFixed( fp_accRealTime, fp_accRealTime ), NanoMath.HALF );
					realPosition.x = realInitialPosition.x + NanoMath.mulFixed( jumpSpeed.x, fp_accRealTime ) +
							NanoMath.mulFixed( ( jumpSpeed.x >= 0 ? FP_BRAKE_ACCELERATION : -FP_BRAKE_ACCELERATION ), fp_t2_div2 );

					if ( fp_accRealTime >= fp_currentBrakingTime ) {
						setJumpState( JUMP_STATE_FINISHED );
					}
				break;
			}

			refreshScreenPosition();
		}
	}
	
	
	private final void updateRealPosition( int delta ) {
		fp_accRealTime += delta;

		realPosition.x = realInitialPosition.x + NanoMath.mulFixed( jumpSpeed.x, fp_accRealTime );

		final int fp_gt = NanoMath.mulFixed( FP_GRAVITY_ACCELERATION, fp_accRealTime );
		fp_instantYSpeed = jumpSpeed.y + fp_gt;

		realPosition.y = NanoMath.mulFixed( jumpSpeed.y, fp_accRealTime ) + NanoMath.divFixed( NanoMath.mulFixed( fp_gt, fp_accRealTime ), NanoMath.toFixed( 2 ) );
		if ( realPosition.y < 0 )
			realPosition.y = 0;
	}
	
	
	public final void refreshScreenPosition() {
		setRefPixelPosition( INITIAL_X + ( NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_GOAL_WIDTH, NanoMath.divFixed( realPosition.x, FP_REAL_GOAL_WIDTH ) ) ) ),
							 INITIAL_Y - ( NanoMath.toInt( NanoMath.mulFixed( FP_PENALTY_FLOOR_TO_BAR, NanoMath.divFixed( realPosition.y, FP_REAL_FLOOR_TO_BAR ) ) ) ) );
	}
	
	
	public final void setLastGiveUpBehaviour( boolean giveUp ) {
		gaveUpLastPlay = giveUp;
	}
	
	
	public final Point3f getRealPosition() {
		return realPosition;
	}


	public final void setInitialPosition( Drawable goal ) {
		// posiciona o goleiro em relação ao gol
		INITIAL_X = goal.getPosX() + ( goal.getWidth() >> 1 );
		INITIAL_Y = goal.getPosY() + goal.getHeight() + KEEPER_OFFSET_Y;
	}

	
}
