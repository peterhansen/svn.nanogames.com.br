/**
 * MessageBox.java
 *
 * Created on May 30, 2010 10:06:02 PM
 *
 */

package core;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.online.ConnectionListener;
import br.com.nanogames.components.online.NanoConnection;
import br.com.nanogames.components.online.NanoOnline;
import br.com.nanogames.components.online.NanoOnlineConstants;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.MenuListener;

import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.NanoMath;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import screens.GameMIDlet;

//#if TOUCH == "true"
 import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public class MessageBox extends GenericInfoBox implements Constants, ConnectionListener, KeyListener,EventListener
//#if TOUCH == "true"
 			, PointerListener
		//#endif
{

	public static final byte INDEX_CANCEL	= 0;
	public static final byte INDEX_CONFIRM	= 1;

	public static final byte MSG_TYPE_WARNING		= 0;
	public static final byte MSG_TYPE_ERROR			= 1;
	public static final byte MSG_TYPE_INFO			= 2;
	public static final byte MSG_TYPE_CONFIRMATION	= 3;

	private short barTotalWidth;

	protected final MarqueeLabel message;

	private static final short KILOBYTE = 1 << 10;

	private static final short HALF_KILOBYTE = KILOBYTE >> 1;

	/***/
	private final BasicAnimatedPattern barRead;

	/***/
	private final Pattern barTotal;

	private static final int[] COLORS = { 0x008291,
										  0xffff00,
										  0x00ff00,
										  0xff0000 };

	/** Velocidade da animaçao da barra de progresso, em pixels por segundo. */
	private static final byte BAR_PATTERN_SPEED = 20;

	/** Diferença na posição x da barra de progresso em relação às bordas. */
	private static final byte BAR_OFFSET_X = 6;

	/** Diferença de posiçao da borda em relação à barra de progresso. */
	private static final byte BAR_BORDER_OFFSET = 1;

	/** Intervalo de atualizaÃ§Ã£o do texto. */
	private static final short CHANGE_TEXT_INTERVAL = 600;

	private long lastUpdateTime;

	private static final byte MAX_DOTS = 4;

	private static final byte MAX_DOTS_MODULE = MAX_DOTS - 1;

	private byte dots;

	private final RichLabel title;

	protected int contentLengthTotal;
	protected int contentLengthRead;

	private String url;

	private byte[] data;

	private ConnectionListener connectionListener;

	private int nextScreen = -1;

	private final Button button;

	private int currentConnection;


	public MessageBox() throws Exception {
		super( 20 );

		barRead = new BasicAnimatedPattern( new DrawableImage( NanoOnlineConstants.PATH_NANO_ONLINE_IMAGES + "bar.png" ), BAR_PATTERN_SPEED, 0 );
		barRead.setPosition( BAR_OFFSET_X, 100 );
		barRead.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		barRead.setVisible( false );
		insertDrawable( barRead );

		barTotal = new Pattern( COLORS[ MSG_TYPE_INFO ] );
		barTotal.setSize( 0, barRead.getHeight() );
		barTotal.setPosition( barRead.getPosition() );
		barTotal.setVisible( false );
		insertDrawable( barTotal );

		title = new RichLabel( FONT_INDEX_DEFAULT, null/*, ScreenManager.SCREEN_WIDTH */);
		title.setSize( 0, title.getFont().getHeight() << 1 );
		insertDrawable( title );

		button = GameMIDlet.getButton( null, 0, this );
		button.addEventListener( this );
		insertDrawable( button );
		message = new MarqueeLabel( ImageFont.createMultiSpacedFont( PATH_ONLINE + "font_0" ), null );
		message.setPosition( barRead.getPosX(), barRead.getPosY() + ( ( barRead.getHeight() - message.getHeight() ) >> 1 ) );
		message.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		message.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		insertDrawable( message );
	}


	public MessageBox( String url, byte[] data ) throws Exception {
		this();
		this.url = url;
		this.data = data;

		setButton( GameMIDlet.getText( TEXT_CANCEL ) );

		setText( GameMIDlet.getText( TEXT_CONNECTING ) );

		barRead.setVisible( true );
		barTotal.setVisible( true );

		setState( STATE_APPEARING );
	}


	public final void setButton( String buttonText ) {
		button.setText( buttonText );
		button.setVisible( buttonText != null );
		setSize( size );
	}


	public final void setText( String text ) {
		title.setText( "<ALN_H>" + text );
	}


	public final void setNextScreen( int nextScreen ) {
		this.nextScreen = nextScreen;
	}


	public final void setConnectionListener( ConnectionListener listener ) {
		connectionListener = listener;
	}


	public final void changeToScreen( int screen ) {
		nextScreen = screen;
		setState( STATE_HIDING );
	}


	public final void setTitle( int textIndex ) {
		setTitle( GameMIDlet.getText( textIndex ) );
	}


	public final void setTitle( String text ) {
		title.setText( text );
	}


	public final void showMessage( byte type, String text ) {
		message.setText( text == null ? null : text.toUpperCase(), false );
		message.setTextOffset( 0 );
		barTotal.setFillColor( COLORS[ type ] );

		switch ( type ) {
			case MSG_TYPE_CONFIRMATION:
			case MSG_TYPE_ERROR:
				contentLengthRead = 0;
			break;
		}

		refreshProgress();
	}


	public final void setSize( int width, int height ) {
		int y = border.getMinHeight() >> 1;

		final int WIDTH = ( ScreenManager.SCREEN_WIDTH * 3 ) >> 2;

		if ( title != null ) {
			title.setSize( WIDTH * 9 / 10, title.getHeight() );
			title.setPosition( ( WIDTH - title.getWidth() ) >> 1, y );
			title.formatText( false );
			title.setSize( title.getWidth(), title.getTextTotalHeight() );

			y += title.getHeight() + ITEMS_SPACING;

			if ( button.isVisible() ) {
				button.setPosition( ( WIDTH - button.getWidth() ) >> 1, y );
				y += button.getHeight() + ITEMS_SPACING;
			}

			if ( barRead.isVisible() ) {
				barTotalWidth = ( short ) ( WIDTH - ( BAR_OFFSET_X << 1 ) );

				message.setPosition( BAR_OFFSET_X, y );
				message.setSize( barTotalWidth, message.getHeight() );

				y += message.getHeight() + ITEMS_SPACING;

				barTotal.setSize( message.getSize() );
				barTotal.setPosition( message.getPosition() );
				barRead.setSize( barRead.getWidth(), message.getHeight() );
				barRead.setPosition( barTotal.getPosition() );
			}

			refreshProgress();
		}

		super.setSize( WIDTH, y );
		refreshLayout();
	}


	public final void refreshLayout() {
		setPosition( ( ScreenManager.SCREEN_WIDTH - getWidth() ) >> 1, ( ScreenManager.SCREEN_HEIGHT - getHeight() ) >> 1 );
	}


	public final void setState( int state ) {
		super.setState( state );

		switch ( state ) {
			case STATE_HIDDEN:
				if ( nextScreen > 0 )
					GameMIDlet.setScreen( nextScreen );
			break;

			case STATE_SHOWN:
				connect();
			break;
		}
	}


	protected final void connect() {
		if ( url != null ) {
			try {
				final ByteArrayOutputStream b = new ByteArrayOutputStream();
				final DataOutputStream out = new DataOutputStream( b );

				// escreve os dados globais - inclusive as informações do news feeder
				NanoOnline.writeGlobalData( out );
				if ( data != null )
					out.write( data );

				out.flush();

				NanoConnection.post( url, b.toByteArray(), this, false );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
					e.printStackTrace();
				//#endif
			}
		}
	}


	public final void update( int delta ) {
		super.update( delta );
		switch ( state ) {
			case STATE_SHOWN:
				if ( url != null ) {
					if ( System.currentTimeMillis() - lastUpdateTime >= CHANGE_TEXT_INTERVAL ) {
						lastUpdateTime = System.currentTimeMillis();

						dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
						String temp = GameMIDlet.getText( TEXT_CONNECTING );
						for ( byte i = 0; i < dots; ++i ) {
							temp += '.';
						}

						title.setText( temp );
						try {
							// permite que a thread de carregamento dos recursos continue sua execuÃ§Ã£o
							Thread.sleep( CHANGE_TEXT_INTERVAL );
						} catch ( Exception e ) {
							//#if DEBUG == "true"
								e.printStackTrace();
							//#endif
						}
					}
				}
			break;
		}
	}


	public void processData( int id, byte[] data ) {
		//#if DEBUG == "true"
			System.out.println( "MessageBox.processData( " + id + ", " + data + " )" );
		//#endif

		if ( connectionListener != null )
			connectionListener.processData( id, data );
	}


	public void onInfo( int id, int infoIndex, Object extraData ) {
		//#if DEBUG == "true"
			System.out.println( "Match.onInfo( " + id + ", " + infoIndex + ", " + extraData + " )" );
		//#endif

		final StringBuffer buffer = new StringBuffer();
		switch ( infoIndex ) {
			case INFO_CONNECTION_OPENED:
				buffer.append( NanoOnline.getText( NanoOnlineConstants.TEXT_CONNECTION_OPENED ) );
			break;

			case INFO_OUTPUT_STREAM_OPENED:
			case INFO_DATA_WRITTEN:
				buffer.append( NanoOnline.getText( NanoOnlineConstants.TEXT_SENDING_DATA ) );
			break;

			case INFO_RESPONSE_CODE:
				buffer.append( NanoOnline.getText( NanoOnlineConstants.TEXT_RESPONSE_CODE ) );
				buffer.append( ( ( Integer ) extraData ).intValue() );
			break;

			case INFO_INPUT_STREAM_OPENED:
				buffer.append( NanoOnline.getText( NanoOnlineConstants.TEXT_RECEIVING_DATA ) );
			break;

			case INFO_CONTENT_LENGTH_TOTAL:
				contentLengthTotal = ( ( Integer ) extraData ).intValue();
				refreshProgress();
				refreshLabel( buffer );
			break;

			case INFO_CONTENT_LENGTH_READ:
				contentLengthRead = ( ( Integer ) extraData ).intValue();
				refreshProgress();

				buffer.append( NanoOnline.getText( NanoOnlineConstants.TEXT_RECEIVING_DATA ) );
				refreshLabel( buffer );
			break;

			case INFO_CONNECTION_ENDED:
				// TODO limpar barra de progresso
			return;
		}

		showMessage( MSG_TYPE_INFO, buffer.toString() );

		if ( connectionListener != null )
			connectionListener.onInfo( id, infoIndex, extraData );
	}


	public void onError( int id, int errorIndex, Object extraData ) {
		//#if DEBUG == "true"
			System.out.println( "Match.onError( " + id + ", " + errorIndex + ", " + extraData + " )" );
		//#endif

		switch ( errorIndex ) {
			case ERROR_HTTP_CONNECTION_NOT_SUPPORTED:
			case ERROR_CANT_GET_RESPONSE_CODE:
			case ERROR_URL_BAD_FORMAT:
			case ERROR_CONNECTION_EXCEPTION:
			case ERROR_CANT_CLOSE_INPUT_STREAM:
			case ERROR_CANT_CLOSE_CONNECTION:
			case ERROR_CANT_CLOSE_OUTPUT_STREAM:
				final Exception e = ( Exception ) extraData;
				showMessage( MSG_TYPE_ERROR, NanoOnline.getText( NanoOnlineConstants.TEXT_ERROR ) + errorIndex + ": " + e.getMessage() );
			break;
		}

		if ( connectionListener != null )
			connectionListener.onError( id, errorIndex, extraData );
	}


	/**
	 *
	 */
	private final void refreshProgress() {
		if ( contentLengthTotal > 0 ) {
			barRead.setSize( barTotalWidth * contentLengthRead / contentLengthTotal, barRead.getHeight() );
		} else {
			barRead.setSize( 0, barRead.getHeight() );
		}
		barTotal.setSize( barTotalWidth - barRead.getWidth(), barTotal.getHeight() );
		barTotal.setPosition( barRead.getPosX() + barRead.getWidth(), barTotal.getPosY() );
	}


	/**
	 *
	 * @param buffer
	 */
	private final void refreshLabel( StringBuffer buffer ) {
		buffer.append( ' ' );

		String unit = "";
		// primeiro atualiza o contador de progresso efetuado
		if ( contentLengthRead < HALF_KILOBYTE ) {
			buffer.append( contentLengthRead );
			unit = " bytes";
		} else if ( contentLengthTotal < NanoMath.MAX_INTEGER_VALUE ) {
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthRead, KILOBYTE ) ) );
			unit = " kB";
		} else {
			// ultrapassa o valor máximo permitido no fixedPoint, então arredonda o valor
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthRead / KILOBYTE, KILOBYTE ), 4 ) );
			unit = "  MB";
		}

		// agora atualiza o indicador do total de bytes, caso ele seja válido (nem sempre o tamanho total é conhecido)
		if ( contentLengthTotal > 0 ) {
			buffer.append( '/' );

			if ( contentLengthTotal < HALF_KILOBYTE ) {
				buffer.append( contentLengthTotal );
			} else if ( contentLengthTotal < NanoMath.MAX_INTEGER_VALUE ) {
				buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthTotal, KILOBYTE ) ) );
			} else {
				// ultrapassa o valor máximo permitido no fixedPoint, então arredonda o valor
				buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthTotal / KILOBYTE, KILOBYTE ), 4 ) );
			}
		}

		buffer.append( unit );

		// TODO adicionar "..." indicando progresso
		//#if DEBUG == "true"
//			try {
//				Thread.sleep( 40 ); // reduz velocidade para poder acompanhar barra de download no emulador
//			} catch ( InterruptedException ex ) {
//			}
		//#endif
	}


	public final void keyPressed( int key ) {
		button.keyPressed( key );
	}


	public final void keyReleased( int key ) {
		button.keyReleased( key );
	}

//#if TOUCH == "true"
 	public final void onPointerDragged( int x, int y ) {
 		x -= getPosX();
 		y -= getPosY();
 		button.onPointerDragged( x, y );
 	}


 	public final void onPointerPressed( int x, int y ) {
 		x -= getPosX();
 		y -= getPosY();
 		button.onPointerPressed( x, y );
 	}


 	public final void onPointerReleased( int x, int y ) {
 		x -= getPosX();
 		y -= getPosY();
 		button.onPointerReleased( x, y );
 	}
	//#endif


	public final void eventPerformed( Event evt ) {
		// só o botão está registrado
		if ( state == STATE_SHOWN ) {
			switch ( evt.eventType ) {
				case Event.EVT_BUTTON_CONFIRMED:
					if ( eventListener == null )
						changeToScreen( nextScreen );
					else
						eventListener.eventPerformed( evt );
				break;
			}
		}
	}

}
