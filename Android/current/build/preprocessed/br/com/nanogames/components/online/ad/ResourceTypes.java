//#if AD_MANAGER == "true"
//# /**
//#  * ResourceTypes.java
//#  *
//#  * Created on 7:00:18 PM.
//#  *
//#  */
//# package br.com.nanogames.components.online.ad;
//# 
//# /**
//#  * Interface que descreve os identificadores dos diferentes tipos de recursos e arquivos que podem ser usados nas aplicações
//#  * clientes Nano. Os identificadores <b>precisam</b> estar de acordo com os usados pelo servidor, para evitar
//#  * inconsistência na gerência dos mesmos.
//#  *
//#  * @author Peter
//#  */
//# public interface ResourceTypes {
//# 
//# 	/************************************************
//# 	 * Tipos de recursos
//# 	 ************************************************/
//# 
//# 	 /** Id de tipo de recurso: nenhum. */
//# 	public static final byte RESOURCE_TYPE_NONE					= -1;
//# 	/** Id de tipo de recurso: sprite no formato iPhone. */
//# 	public static final byte RESOURCE_TYPE_SPRITE_IPHONE		= RESOURCE_TYPE_NONE - 1;
//# 	/** Id de tipo de recurso: sprite no formato J2ME. */
//# 	public static final byte RESOURCE_TYPE_SPRITE_J2ME			= RESOURCE_TYPE_SPRITE_IPHONE - 1;
//# 	/** Id de tipo de recurso: som. */
//# 	public static final byte RESOURCE_TYPE_SOUND				= RESOURCE_TYPE_SPRITE_J2ME - 1;
//# 	/** Id de tipo de recurso: texto (sem formatação específica). */
//# 	public static final byte RESOURCE_TYPE_TEXT_SIMPLE			= RESOURCE_TYPE_SOUND - 1;
//# 	/** Id de tipo de recurso: fonte de imagem (formato utilizado no J2ME). */
//# 	public static final byte RESOURCE_TYPE_IMAGE_FONT			= RESOURCE_TYPE_TEXT_SIMPLE - 1;
//# 	/** Id de tipo de recurso: textura. */
//# 	public static final byte RESOURCE_TYPE_TEXTURE				= RESOURCE_TYPE_IMAGE_FONT - 1;
//# 	/** Id de tipo de recurso: vídeo. */
//# 	public static final byte RESOURCE_TYPE_VIDEO				= RESOURCE_TYPE_TEXTURE - 1;
//# 	/** Id de tipo de recurso: pacote de textos (como conjunto de textos de um aplicativo, por exemplo). */
//# 	public static final byte RESOURCE_TYPE_TEXT_PACK			= RESOURCE_TYPE_VIDEO - 1;
//# 	/** Id de tipo de recurso: fonte no formato TTF (True Type Font). */
//# 	public static final byte RESOURCE_TYPE_TRUE_TYPE_FONT		= RESOURCE_TYPE_TEXT_PACK - 1;
//# 	/** Id de tipo de recurso: imagem. */
//# 	public static final byte RESOURCE_TYPE_IMAGE				= RESOURCE_TYPE_TRUE_TYPE_FONT - 1;
//# 
//# 
//# 	/************************************************
//# 	 * Tipos de arquivos
//# 	 ************************************************/
//# 
//# 	/** Id de tipo de arquivo: nenhum. */
//# 	public static final byte FILE_TYPE_NONE						= 1;
//# 	/** Id de tipo de arquivo: imagem PNG. */
//# 	public static final byte FILE_TYPE_IMAGE_PNG				= FILE_TYPE_NONE + 1;
//# 	/** Id de tipo de arquivo: imagem BMP. */
//# 	public static final byte FILE_TYPE_IMAGE_BMP				= FILE_TYPE_IMAGE_PNG + 1;
//# 	/** Id de tipo de arquivo: imagem JPEG. */
//# 	public static final byte FILE_TYPE_IMAGE_JPEG				= FILE_TYPE_IMAGE_BMP + 1;
//# 	/** Id de tipo de arquivo: imagem GIF. */
//# 	public static final byte FILE_TYPE_IMAGE_GIF				= FILE_TYPE_IMAGE_JPEG + 1;
//# 	/** Id de tipo de arquivo: descritor de sprite no formato J2ME. */
//# 	public static final byte FILE_TYPE_SPRITE_DESCRIPTOR_J2ME	= FILE_TYPE_IMAGE_GIF + 1;
//# 	/** Id de tipo de arquivo: descritor de sprite no formato iPhone. */
//# 	public static final byte FILE_TYPE_SPRITE_DESCRIPTOR_IPHONE	= FILE_TYPE_SPRITE_DESCRIPTOR_J2ME + 1;
//# 	/** Id de tipo de arquivo: som no formato MIDI. */
//# 	public static final byte FILE_TYPE_SOUND_MIDI				= FILE_TYPE_SPRITE_DESCRIPTOR_IPHONE + 1;
//# 	/** Id de tipo de arquivo: som no formato MP3. */
//# 	public static final byte FILE_TYPE_SOUND_MP3				= FILE_TYPE_SOUND_MIDI + 1;
//# 	/** Id de tipo de arquivo: som no formato AAC. */
//# 	public static final byte FILE_TYPE_SOUND_AAC				= FILE_TYPE_SOUND_MP3 + 1;
//# 	/** Id de tipo de arquivo: som no formato WAV. */
//# 	public static final byte FILE_TYPE_SOUND_WAV				= FILE_TYPE_SOUND_AAC + 1;
//# 
//# }
//# 
//#endif