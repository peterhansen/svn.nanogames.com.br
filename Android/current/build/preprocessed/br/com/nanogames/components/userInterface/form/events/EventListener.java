/**
 * EventListener.java
 * 
 * Created on Dec 13, 2008, 1:25:49 PM
 *
 */
package br.com.nanogames.components.userInterface.form.events;


/**
 *
 * @author Peter
 */
public interface EventListener {

	/**
	 * Método invocado quando um componente recebe um evento. Importante notar que algumas ações podem desencadear
	 * mais de uma chamada desse método. Por exemplo: soltar o ponteiro que está pressionando um botão resulta em 2
	 * chamadas: primeiro com o evento <code>EVT_POINTER_RELEASED</code> e então <code>EVT_BUTTON_CONFIRMED</code>.
	 *
	 * @param evt event object describing the source of the action as well as
	 * its trigger
	 * @see br.com.nanogames.components.userInterface.form.events.Event
	 */
	public void eventPerformed( Event evt );


}
