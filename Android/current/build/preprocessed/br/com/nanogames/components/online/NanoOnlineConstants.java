/**
 * NanoOnlineConstants.java
 * 
 * Created on 30/Out/2008, 15:54:55
 *
 */

package br.com.nanogames.components.online;

/**
 *
 * @author Peter
 */
public interface NanoOnlineConstants {

	//#if DEBUG == "true"
//# 		/** Endereço raiz do Nano Online. */
//#   		//public static final String NANO_ONLINE_URL = "http://127.0.0.1:3000/";
//#  		//public static final String NANO_ONLINE_URL = "http://192.168.1.11:3000/";
//# 		 public static final String NANO_ONLINE_URL = "http://online.nanogames.com.br/";
//# 		 //public static final String NANO_ONLINE_URL = "http://staging.nanogames.com.br/"; //TO tirar staging como servidor
	//#else
		/** Endereço raiz do Nano Online. */
		 public static final String NANO_ONLINE_URL = "http://online.nanogames.com.br/";
		 //public static final String NANO_ONLINE_URL = "http://staging.nanogames.com.br/";

	//#endif

	/** Caminho das imagens do Nano Online no .jar */
	public static final String PATH_NANO_ONLINE_IMAGES = "/online/";
	
	/** Índice da fonte branca usada no Nano Online. */
	public static final byte FONT_WHITE = 0;

	/** Índice da fonte padrão utilizada no Nano Online. */
	public static final byte FONT_DEFAULT = FONT_WHITE;
	
	/** Índice da fonte padrão utilizada no Nano Online. */
	public static final byte FONT_TEXT = 1;

	/** Índice da fonte padrão utilizada no Nano Online. */
	public static final byte FONT_BLACK = 2;

	/** Índice da fonte pequena utilizada na barra de progresso do Nano Online. */
	public static final byte FONT_SMALL = 3;

	/** Quantidade total de fontes utilizadas no Nano Online. */
	public static final byte FONT_TYPES_TOTAL = 4;

	/** Total de fontes a carregar. */
	public static final byte FONT_TYPES_TO_LOAD = 2;

	public static final int COLOR_FONT_WHITE = 0xffffff;
	public static final int COLOR_FONT_BLACK = 0x003f46;
	
	public static final byte BORDER_COLOR_TYPE_DEFAULT		= 0;
	public static final byte BORDER_COLOR_TYPE_HIGHLIGHT	= 1;

	public static final String PATH_BORDER = PATH_NANO_ONLINE_IMAGES + "border/";

	/** Espaçamento horizontal entre os componentes. */
	public static final byte LAYOUT_GAP_X = 3;
	/** Espaçamento vertical entre os componentes. */
	public static final byte LAYOUT_GAP_Y = 4;
	/** Posição horizontal de início dos componentes. */
	public static final byte LAYOUT_START_X = 3;
	/** Posição vertical de início dos componentes. */
	public static final byte LAYOUT_START_Y = 13;

	/** Largura mínima dos botões do Nano Online. */
	public static final byte BUTTON_MIN_WIDTH = 112 - LAYOUT_GAP_X;
	
	public static final char PASSWORD_MASK_DEFAULT = '*';
	
	/** Máximo de perfis gravados. */
	public static final byte MAX_PROFILES = 10;

	// <editor-fold defaultstate="collapsed" desc="IDIOMAS DO NANO ONLINE">

	/** Idioma do Nano Online: Inglês (EUA) */
	public static final byte LANGUAGE_en_US	= 0;

	/** Idioma do Nano Online: Espanhol (Espanha) */
	public static final byte LANGUAGE_es_ES	= 1;

	/** Idioma do Nano Online: Português (Brasil) */
	public static final byte LANGUAGE_pt_BR	= 2;

	/** Idioma do Nano Online: Alemão (Alemanha) */
	public static final byte LANGUAGE_de_DE	= 3;

	/** Idioma do Nano Online: Português (Portugal) */
	public static final byte LANGUAGE_pt_PT	= 4;

	/** Idioma do Nano Online: Italiano (Itália) */
	public static final byte LANGUAGE_it_IT	= 5;

	/** Idioma do Nano Online: Francês (França) */
	public static final byte LANGUAGE_fr_FR	= 6;

	/** Idioma do Nano Online: Japones (Japão) */
	public static final byte LANGUAGE_jp_JP	= 7;

	/** Idioma do Nano Online: Russo (Rússia) */
	public static final byte LANGUAGE_ru_RU	= 8;

	/** Idioma do Nano Online: Coreano (Coréia) */
	public static final byte LANGUAGE_ko_KR	= 9;

	/** Idioma do Nano Online: Holandês (Holanda) */
	public static final byte LANGUAGE_nl_NL	= 10;

	/** Idioma padrão do Nano Online: Inglês (EUA) */
	public static final byte LANGUAGE_DEFAULT = LANGUAGE_en_US;

	public static final byte LANGUAGES_TOTAL = LANGUAGE_nl_NL + 1;


	// </editor-fold>


	/** Nome da base de dados onde serão armazenadas as informações do Nano Online. */
	public static final String DATABASE_NAME					= "no";

	/** Índice do slot de gravação das informações dos usuários na base de dados. */
	public static final byte DATABASE_SLOT_PROFILES				= 1;

	/** Índice do primeiro slot do news feeder (há um para cada idioma). */
	public static final byte DATABASE_SLOT_NEWS_FEEDER_FIRST	= 2;

	/** Quantidade total de slots na base de dados. */
	public static final byte DATABASE_TOTAL_SLOTS				= 2 + LANGUAGES_TOTAL;
	
	
	// <editor-fold desc="TEXTOS UTILIZADOS NO NANO ONLINE" >
	
	public static final byte TEXT_YES									= 0;
	public static final byte TEXT_NO									= TEXT_YES + 1;
	public static final byte TEXT_BACK									= TEXT_NO + 1;
	public static final byte TEXT_CANCEL								= TEXT_BACK + 1;
	public static final byte TEXT_CONFIRM								= TEXT_CANCEL + 1;
	public static final byte TEXT_OK									= TEXT_CONFIRM + 1;
	public static final byte TEXT_EDIT									= TEXT_OK + 1;
	public static final byte TEXT_EXIT									= TEXT_EDIT + 1;
	public static final byte TEXT_CLEAR									= TEXT_EXIT + 1;
	public static final byte TEXT_TIME_FORMAT							= TEXT_CLEAR + 1;
	public static final byte TEXT_REQUIRED_FIELDS						= TEXT_TIME_FORMAT + 1;
	public static final byte TEXT_NICKNAME								= TEXT_REQUIRED_FIELDS + 1;
	public static final byte TEXT_FIRST_NAME							= TEXT_NICKNAME + 1;
	public static final byte TEXT_LAST_NAME								= TEXT_FIRST_NAME + 1;
	public static final byte TEXT_CPF									= TEXT_LAST_NAME + 1;
	public static final byte TEXT_PHONE_NUMBER							= TEXT_CPF + 1;
	public static final byte TEXT_EMAIL									= TEXT_PHONE_NUMBER + 1;
	public static final byte TEXT_NICKNAME_OR_EMAIL						= TEXT_EMAIL + 1;
	public static final byte TEXT_BIRTHDAY								= TEXT_NICKNAME_OR_EMAIL + 1;
	public static final byte TEXT_GENRE									= TEXT_BIRTHDAY + 1;
	public static final byte TEXT_GENDER_NOT_INFORMED					= TEXT_GENRE + 1;
	public static final byte TEXT_GENDER_FEMALE							= TEXT_GENDER_NOT_INFORMED + 1;
	public static final byte TEXT_GENDER_MALE							= TEXT_GENDER_FEMALE + 1;
	public static final byte TEXT_PASSWORD								= TEXT_GENDER_MALE + 1;
	public static final byte TEXT_PASSWORD_CONFIRM						= TEXT_PASSWORD + 1;
	public static final byte TEXT_FORGOT_PASSWORD						= TEXT_PASSWORD_CONFIRM + 1;
	public static final byte TEXT_NEW_PASSWORD_SENT						= TEXT_FORGOT_PASSWORD + 1;
	public static final byte TEXT_NEW_PASSWORD_SENT_CONFIRMATION		= TEXT_NEW_PASSWORD_SENT + 1;
	public static final byte TEXT_CONNECTING							= TEXT_NEW_PASSWORD_SENT_CONFIRMATION + 1;
	public static final byte TEXT_PROFILES								= TEXT_CONNECTING + 1;
	public static final byte TEXT_LOGIN									= TEXT_PROFILES + 1;
	public static final byte TEXT_LOGIN_TEXT							= TEXT_LOGIN + 1;
	public static final byte TEXT_SYNC_REQUIRED							= TEXT_LOGIN_TEXT + 1;
	public static final byte TEXT_NEWS									= TEXT_SYNC_REQUIRED + 1;
	public static final byte TEXT_NO_NEWS_FOUND							= TEXT_NEWS + 1;
	public static final byte TEXT_RECORDS								= TEXT_NO_NEWS_FOUND + 1;
	public static final byte TEXT_NANO_ONLINE							= TEXT_RECORDS + 1;
	public static final byte TEXT_OPTIONS								= TEXT_NANO_ONLINE + 1;
	public static final byte TEXT_SUBMIT_RECORDS						= TEXT_OPTIONS + 1;
	public static final byte TEXT_UPDATE								= TEXT_SUBMIT_RECORDS + 1;
	public static final byte TEXT_ERASE_RECORDS							= TEXT_UPDATE + 1;
	public static final byte TEXT_RECORDS_ERASED						= TEXT_ERASE_RECORDS + 1;
	public static final byte TEXT_HELP_TITLE							= TEXT_RECORDS_ERASED + 1;
	public static final byte TEXT_HELP_TITLE_COMMANDS					= TEXT_HELP_TITLE + 1;
	public static final byte TEXT_HELP_TITLE_RANKING					= TEXT_HELP_TITLE_COMMANDS + 1;
	public static final byte TEXT_HELP_TITLE_TAXES						= TEXT_HELP_TITLE_RANKING + 1;
	public static final byte TEXT_HELP_TITLE_PRIVACY					= TEXT_HELP_TITLE_TAXES + 1;
	public static final byte TEXT_HELP_TITLE_SUPPORT					= TEXT_HELP_TITLE_PRIVACY + 1;
	public static final byte TEXT_HELP_TEXT_COMMANDS					= TEXT_HELP_TITLE_SUPPORT + 1;
	public static final byte TEXT_HELP_TEXT_RANKING						= TEXT_HELP_TEXT_COMMANDS + 1;
	public static final byte TEXT_HELP_TEXT_TAXES						= TEXT_HELP_TEXT_RANKING + 1;
	public static final byte TEXT_HELP_TEXT_PRIVACY						= TEXT_HELP_TEXT_TAXES + 1;
	public static final byte TEXT_HELP_TEXT_SUPPORT						= TEXT_HELP_TEXT_PRIVACY + 1;
	public static final byte TEXT_CONNECTION_OPENED						= TEXT_HELP_TEXT_SUPPORT + 1;
	public static final byte TEXT_RESPONSE_CODE							= TEXT_CONNECTION_OPENED + 1;
	public static final byte TEXT_SENDING_DATA							= TEXT_RESPONSE_CODE + 1;
	public static final byte TEXT_RECEIVING_DATA						= TEXT_SENDING_DATA + 1;
	public static final byte TEXT_REMEMBER_ME							= TEXT_RECEIVING_DATA + 1;
	public static final byte TEXT_REMEMBER_PASSWORD						= TEXT_REMEMBER_ME + 1;
	public static final byte TEXT_IMPORT_PROFILE						= TEXT_REMEMBER_PASSWORD + 1;
	public static final byte TEXT_SET_AS_CURRENT_PROFILE				= TEXT_IMPORT_PROFILE + 1;
	public static final byte TEXT_ERASE_PROFILE							= TEXT_SET_AS_CURRENT_PROFILE + 1;
	public static final byte TEXT_ERASE_PROFILE_CONFIRM					= TEXT_ERASE_PROFILE + 1;
	public static final byte TEXT_USE_THIS_PROFILE						= TEXT_ERASE_PROFILE_CONFIRM + 1;
	public static final byte TEXT_CHOOSE_PROFILE						= TEXT_USE_THIS_PROFILE + 1;
	public static final byte TEXT_NO_PROFILES_FOUND						= TEXT_CHOOSE_PROFILE + 1;
	public static final byte TEXT_PROFILES_FOUND						= TEXT_NO_PROFILES_FOUND + 1;
	public static final byte TEXT_CREATE_PROFILE						= TEXT_PROFILES_FOUND + 1;
	public static final byte TEXT_CURRENT_PROFILE						= TEXT_CREATE_PROFILE + 1;
	public static final byte TEXT_PROFILE_ID							= TEXT_CURRENT_PROFILE + 1;
	public static final byte TEXT_NONE									= TEXT_PROFILE_ID + 1;
	public static final byte TEXT_RESEND_CONFIRMATION_TITLE				= TEXT_NONE + 1;
	public static final byte TEXT_RESEND_CONFIRMATION_TEXT				= TEXT_RESEND_CONFIRMATION_TITLE + 1;
	public static final byte TEXT_VALIDATION_EMAIL_SENT_1				= TEXT_RESEND_CONFIRMATION_TEXT + 1;
	public static final byte TEXT_VALIDATION_EMAIL_SENT_2				= TEXT_VALIDATION_EMAIL_SENT_1 + 1;
	public static final byte TEXT_VALIDATION_EMAIL_RESENT				= TEXT_VALIDATION_EMAIL_SENT_2 + 1;
	public static final byte TEXT_EMAIL_VALIDATION_EXPIRED				= TEXT_VALIDATION_EMAIL_RESENT + 1;
	public static final byte TEXT_VALIDATE_EMAIL_BEFORE					= TEXT_EMAIL_VALIDATION_EXPIRED + 1;
	public static final byte TEXT_EMAIL_VALIDATED					= TEXT_VALIDATE_EMAIL_BEFORE + 1;
	public static final byte TEXT_REMEMBER_TO_VALIDATE					= TEXT_EMAIL_VALIDATED + 1;
	public static final byte TEXT_NO_RECORDS_FOUND						= TEXT_REMEMBER_TO_VALIDATE + 1;
	public static final byte TEXT_UPDATED_AT							= TEXT_NO_RECORDS_FOUND + 1;
	public static final byte TEXT_NO_ONLINE_RECORDS						= TEXT_UPDATED_AT + 1;
	public static final byte TEXT_SAVE_LOCAL_RECORD						= TEXT_NO_ONLINE_RECORDS + 1;
	public static final byte TEXT_LOCAL_RANKING							= TEXT_SAVE_LOCAL_RECORD + 1;
	public static final byte TEXT_GLOBAL_RANKING						= TEXT_LOCAL_RANKING + 1;
	public static final byte TEXT_CUSTOMER_REGISTERED					= TEXT_GLOBAL_RANKING + 1;
	public static final byte TEXT_CUSTOMER_UPDATED						= TEXT_CUSTOMER_REGISTERED + 1;
	public static final byte TEXT_REGISTER_ERROR_NICKNAME_IN_USE		= TEXT_CUSTOMER_UPDATED + 1;
	public static final byte TEXT_REGISTER_ERROR_NICKNAME_FORMAT		= TEXT_REGISTER_ERROR_NICKNAME_IN_USE + 1;
	public static final byte TEXT_REGISTER_ERROR_NICKNAME_LENGTH		= TEXT_REGISTER_ERROR_NICKNAME_FORMAT + 1;
	public static final byte TEXT_REGISTER_ERROR_NAME_FORMAT			= TEXT_REGISTER_ERROR_NICKNAME_LENGTH + 1;
	public static final byte TEXT_REGISTER_ERROR_PHONE_LENGTH			= TEXT_REGISTER_ERROR_NAME_FORMAT + 1;
	public static final byte TEXT_REGISTER_ERROR_PHONE_FORMAT			= TEXT_REGISTER_ERROR_PHONE_LENGTH + 1;
	public static final byte TEXT_REGISTER_ERROR_EMAIL_FORMAT			= TEXT_REGISTER_ERROR_PHONE_FORMAT + 1;
	public static final byte TEXT_REGISTER_ERROR_EMAIL_ALREADY_IN_USE	= TEXT_REGISTER_ERROR_EMAIL_FORMAT + 1;
	public static final byte TEXT_REGISTER_ERROR_EMAIL_LENGTH			= TEXT_REGISTER_ERROR_EMAIL_ALREADY_IN_USE + 1;
	public static final byte TEXT_REGISTER_ERROR_CPF_LENGTH				= TEXT_REGISTER_ERROR_EMAIL_LENGTH + 1;
	public static final byte TEXT_REGISTER_ERROR_CPF_INVALID			= TEXT_REGISTER_ERROR_CPF_LENGTH + 1;
	public static final byte TEXT_REGISTER_ERROR_PASSWORD_WEAK			= TEXT_REGISTER_ERROR_CPF_INVALID + 1;
	public static final byte TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT		= TEXT_REGISTER_ERROR_PASSWORD_WEAK + 1;
	public static final byte TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION	= TEXT_REGISTER_ERROR_PASSWORD_TOO_SHORT + 1;
	public static final byte TEXT_REGISTER_ERROR_INVALID_DAY			= TEXT_REGISTER_ERROR_PASSWORD_CONFIRMATION + 1;
	public static final byte TEXT_REGISTER_ERROR_INVALID_MONTH			= TEXT_REGISTER_ERROR_INVALID_DAY + 1;
	public static final byte TEXT_REGISTER_ERROR_INVALID_YEAR			= TEXT_REGISTER_ERROR_INVALID_MONTH + 1;
	public static final byte TEXT_REGISTER_ERROR_REPLY_FORMAT			= TEXT_REGISTER_ERROR_INVALID_YEAR + 1;
	public static final byte TEXT_LOGIN_ERROR_NICKNAME_NOT_FOUND		= TEXT_REGISTER_ERROR_REPLY_FORMAT + 1;
	public static final byte TEXT_LOGIN_ERROR_INVALID_NICKNAME			= TEXT_LOGIN_ERROR_NICKNAME_NOT_FOUND + 1;
	public static final byte TEXT_LOGIN_ERROR_PASSWORD_INVALID			= TEXT_LOGIN_ERROR_INVALID_NICKNAME + 1;
	public static final byte TEXT_ACCESS_URL							= TEXT_LOGIN_ERROR_PASSWORD_INVALID + 1;
	public static final byte TEXT_ERROR									= TEXT_ACCESS_URL + 1;
	public static final byte TEXT_ERROR_SERVER_INTERNAL					= TEXT_ERROR + 1;
	public static final byte TEXT_ERROR_APP_NOT_FOUND					= TEXT_ERROR_SERVER_INTERNAL + 1;
	public static final byte TEXT_ERROR_DEVICE_NOT_SUPPORTED			= TEXT_ERROR_APP_NOT_FOUND + 1;
	
	
	public static final byte TEXTS_TOTAL = TEXT_ERROR_DEVICE_NOT_SUPPORTED + 1;
	
	// </editor-fold>
	
	
	// <editor-fold desc="TELAS USADAS NO NANO ONLINE" >
	
	public static final byte SCREEN_MAIN_MENU				= 100;
	public static final byte SCREEN_REGISTER_PROFILE		= SCREEN_MAIN_MENU + 1;
	public static final byte SCREEN_PROFILE_SELECT			= SCREEN_REGISTER_PROFILE + 1;
	public static final byte SCREEN_PROFILE_VIEW			= SCREEN_PROFILE_SELECT + 1;
	public static final byte SCREEN_PROFILE_EDIT			= SCREEN_PROFILE_VIEW + 1;
	public static final byte SCREEN_LOAD_PROFILE			= SCREEN_PROFILE_EDIT + 1;
	public static final byte SCREEN_HELP_MENU				= SCREEN_LOAD_PROFILE + 1;
	public static final byte SCREEN_HELP_COMMANDS			= SCREEN_HELP_MENU + 1;
	public static final byte SCREEN_HELP_RANKING			= SCREEN_HELP_COMMANDS + 1;
	public static final byte SCREEN_HELP_TAXES				= SCREEN_HELP_RANKING + 1;
	public static final byte SCREEN_HELP_PRIVACY			= SCREEN_HELP_TAXES + 1;
	public static final byte SCREEN_HELP_SUPPORT			= SCREEN_HELP_PRIVACY + 1;
	public static final byte SCREEN_ERROR					= SCREEN_HELP_SUPPORT + 1;
	public static final byte SCREEN_OPTIONS					= SCREEN_ERROR + 1;
	public static final byte SCREEN_RECORDS					= SCREEN_OPTIONS + 1;
	public static final byte SCREEN_NEW_RECORD				= SCREEN_RECORDS + 1;
	public static final byte SCREEN_ENTER_LOCAL_NAME		= SCREEN_NEW_RECORD + 1;
	public static final byte SCREEN_NEWS					= SCREEN_ENTER_LOCAL_NAME + 1;
	public static final byte SCREEN_LOGIN					= SCREEN_NEWS + 1;
	public static final byte SCREEN_SYNC_CUSTOMER_INFO		= SCREEN_LOGIN + 1;
	
	// </editor-fold>
	
	
	// <editor-fold desc="IDS GLOBAIS UTILIZADOS COMO CHAVES NO PROTOCOLO NANO ONLINE" >
	
	// por padrão, os ids de chaves globais são negativos, para evitar possíveis confusões com ids específicos de
	// um serviço (registro de usuário, ranking online, multiplayer, propagandas, etc.), que devem utilizar ids
	// positivos.
	
	/** Id de chave global: aplicativo/jogo. Tipo do valor: String */
	public static final byte ID_APP							= Byte.MIN_VALUE;
	
	/** Id de chave global: versão do aplicativo/jogo. Tipo do valor: string */
	public static final byte ID_APP_VERSION					= ID_APP + 1;
	
	/** Id de chave global: idioma atual do aplicativo. Tipo do valor: byte */
	public static final byte ID_LANGUAGE					= ID_APP_VERSION + 1;
	
	/** Id de chave global: versão do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string */
	public static final byte ID_NANO_ONLINE_VERSION			= ID_LANGUAGE + 1;
	
	/** Id de chave global: código de retorno. Tipo do valor: short */
	public static final byte ID_RETURN_CODE					= ID_NANO_ONLINE_VERSION + 1;
	
	/** Id de chave global: usuário. Tipo do valor: int */
	public static final byte ID_CUSTOMER_ID					= ID_RETURN_CODE + 1;
	
	/** Id de chave global: mensagem de erro. Tipo do valor: string */
	public static final byte ID_ERROR_MESSAGE				= ID_CUSTOMER_ID + 1;
	
	/** Id de chave global: início de dados específicos. Tipo do valor: byte[] (tamanho variável) */
	public static final byte ID_SPECIFIC_DATA				= ID_ERROR_MESSAGE + 1;

	/** Id de chave global: início de dados do news feeder. Tipo do valor: byte[] (tamanho variável) */
	public static final byte ID_NEWS_FEEDER_DATA			= ID_SPECIFIC_DATA + 1;

	/** Id de chave global: início de dados do ad server. Tipo do valor: byte[] (tamanho variável) */
	public static final byte ID_AD_SERVER_DATA				= ID_NEWS_FEEDER_DATA + 1;
	
	/** Id de chave global: horário local do cliente. Tipo do valor: long */
	public static final byte ID_CLIENT_LOCAL_TIME			= ID_AD_SERVER_DATA + 1;

	/** Id de chave global: lista de jogadores com perfis (possivelmente) expirados.
	 * Tipo do valor: byte indicando a quantidade de perfis expirados, e um int para cada perfil. (cliente -> servidor)
	 * Ao retornar informações para o cliente, além do int é retornado um long indicando a data de expiração.
	 */
	public static final byte ID_EXPIRED_PROFILE_IDS			= ID_CLIENT_LOCAL_TIME + 1;

	/** Id de chave global: início de dados do gerenciador de recursos. Formato dos dados: byte[] (tamanho variável). */
	public static final byte ID_RESOURCE_DATA				= ID_EXPIRED_PROFILE_IDS + 1;

	/** Id de chave global: Byte extra enviado nas requisições POST feitas via MacOS e iPhoneOS. Sem fazer isso o Rails
	 * ignora o último byte do corpo de uma mensagem quando seu valor é 0, causando bugs estranhos */
	public static final byte ID_MACOS_RAILS_BUG_FIX	= -100;
	
	// </editor-fold>
	
	
	// <editor-fold desc="Valores de retorno globais" >

	// assim como as chaves, os valores de retorno globais possuem valores negativos para que não haja confusão com os
	// valores utilizados internamente em cada serviço. A chave 0 (zero) é considerada OK (RC_OK)

	/** Valor de retorno global: sem erros. */
	public static final byte RC_OK						= 0;
	
	/** Valor de retorno global: erro interno do servidor. */
	public static final byte RC_SERVER_INTERNAL_ERROR	= RC_OK - 1;
	
	/** Valor de retorno global: aparelho não suportado/detectado. */
	public static final byte RC_DEVICE_NOT_SUPPORTED	= RC_SERVER_INTERNAL_ERROR - 1;
	
	/** Valor de retorno global: aplicativo não encontrado (id de aplicativo inválido). */
	public static final byte RC_APP_NOT_FOUND			= RC_DEVICE_NOT_SUPPORTED - 1;
	
	// </editor-fold>	
	
	
	// <editor-fold desc="IDS DE JOGOS E APLICATIVOS" >
	
	/** Id de jogo/aplicativo: dummy (usado para testes e debug) */
	public static final String ID_APP_DUMMY									= "DMMY";
	
	// </editor-fold>
	
}
