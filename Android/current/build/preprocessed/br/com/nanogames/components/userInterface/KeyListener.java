/*
 * KeyListener.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

/**
 *
 * @author peter
 */
public interface KeyListener {
 
	
	/**
	 * Método chamado quando uma tecla é pressionada.
	 * @param key índice da tecla pressionada.
	 */
	public abstract void keyPressed( int key );
	
	
	/**
	 * Método chamado quando uma tecla que estava pressionada é liberada.
	 * @param key índice da tecla liberada.
	 */
	public abstract void keyReleased( int key );
}
 
