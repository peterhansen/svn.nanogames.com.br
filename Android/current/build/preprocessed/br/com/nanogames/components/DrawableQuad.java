/**
 * DrawableQuad.java
 * 
 * Created on 15/Dez/2009, 12:56:07
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.util.Point;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Color;
//# import java.awt.Insets;
//# import java.awt.Graphics;
//# import java.awt.Graphics2D;
//# import java.awt.event.KeyEvent;
//# import java.awt.event.MouseListener;
//# import java.awt.image.BufferedImage;
//# import javax.swing.JFrame;
//# import java.awt.event.KeyListener;
//# import java.awt.event.MouseMotionListener;
//# import java.awt.Dimension;
//# import java.awt.Polygon;
//#endif

/**
 * 
 * @author Peter
 */
public class DrawableQuad extends DrawableTriangle {

	public final Point bottomLeft;
	
	private boolean fill = true;
	
	
	public DrawableQuad() {
		this( new Point(), new Point(), new Point(), new Point() );
	}
	
	
	public DrawableQuad( Point topLeft, Point topRight, Point bottomRight, Point bottomLeft ) {
		super( topLeft, topRight, bottomRight );
		this.bottomLeft = bottomLeft;
	}


	protected void paint( Graphics g ) {
		final int p1x = translate.x + p1.x;
		final int p1y = translate.y + p1.y;
		final int p2x = translate.x + p2.x;
		final int p2y = translate.y + p2.y;
		final int p3x = translate.x + p3.x;
		final int p3y = translate.y + p3.y;
		final int p4x = translate.x + bottomLeft.x;
		final int p4y = translate.y + bottomLeft.y;
		
		//#if J2SE == "false"
			g.setColor( color );
			if ( fill ) {
				g.fillTriangle( p1x, p1y,
								p2x, p2y,
								p3x, p3y );

				g.fillTriangle( p1x, p1y,
								p4x, p4y,
								p3x, p3y );
			} else {
				g.drawLine( p1x, p1y, p2x, p2y );
				g.drawLine( p1x, p1y, p4x, p4y );
				g.drawLine( p2x, p2y, p4x, p4y );
				g.drawLine( p3x, p3y, p4x, p4y );
			}
		//#else
//# 			g.setColor( new Color( color ) );
//# 			final Polygon p = new Polygon();
//# 			p.addPoint( p1x, p1y );
//# 			p.addPoint( p2x, p2y );
//# 			p.addPoint( p3x, p3y );
//# 			p.addPoint( p4x, p4y );
//# 
//# 			if ( fill )
//# 				g.fillPolygon( p );
//# 			else
//# 				g.drawPolygon( p );
		//#endif
	}
	
	
}
