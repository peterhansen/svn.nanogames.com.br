//#if AD_MANAGER == "true"
//# 
//# /**
//#  * AdChannel.java
//#  *
//#  * Created on 4:52:57 PM.
//#  *
//#  */
//# package br.com.nanogames.components.online.ad;
//# 
//# import br.com.nanogames.components.UpdatableGroup;
//# import br.com.nanogames.components.online.NanoOnline;
//# import br.com.nanogames.components.userInterface.ScreenListener;
//# import br.com.nanogames.components.util.Point;
//# import java.util.Date;
//# import java.util.Hashtable;
//# import java.util.Vector;
//# 
//# 
//# /**
//#  * Classe que descreve um canal publicitário abstrato.
//#  * 
//#  * @author Peter
//#  */
//# public abstract class AdChannel extends UpdatableGroup implements ResourceTypes, ScreenListener {
//# 
//# 	/**
//# 	 * Id da versão do veículo. Esse id é usado para que o servidor possa identificar a versão do canal.
//# 	 * Atenção: especificações diferentes (tamanhos de tela, uso ou não de recursos especiais, etc.)
//# 	 * precisam ter ids diferentes, ou poderá haver erro na exibição do canal.
//# 	 */
//# 	private final int adChannelVersionId;
//# 
//# 	/***/
//# 	protected Ad ad;
//# 
//# 	/***/
//# 	private static final byte TIME_NONE = -1;
//# 
//# 	/** Instante de tempo do ínicio da última visualização. */
//# 	private long startTime = TIME_NONE;
//# 
//# 	/** Visualizações desse canal. A chave do Hashtable é o id do usuário, e a chave é um Vector contendo vários objetos Point,
//# 	 * cujos x indicam a hora de início da visualização e y a duração.
//# 	 */
//# 	private final Hashtable visualizations = new Hashtable();
//# 
//# 	/** Hora inicial considerada pelo canal. Em vez de considerar a hora padrão do Java (01/01/1970), utilizando-se uma
//# 	 * hora próxima podemos trabalhar com int em vez de long, reduzindo o consumo total de memória e armazenamento.
//# 	 */
//# 	protected static final long START_TIME;
//# 
//# 	/** Número de visualizações mínimo para que seja feita uma gravação automática dos dados no RMS. */
//# 	public static final byte SAVE_RATE = 100;
//# 
//# 
//# 	static {
//# 		START_TIME = System.currentTimeMillis();
//# 	}
//# 
//# 
//# 	protected AdChannel( int adChannelVersionId, int slots ) {
//# 		this( adChannelVersionId, slots, true );
//# 	}
//# 
//# 
//# 	/**
//# 	 * 
//# 	 * @param adChannelVersionId
//# 	 * @param slots
//# 	 * @param autoLoadAd
//# 	 */
//# 	protected AdChannel( int adChannelVersionId, int slots, boolean autoLoadAd ) {
//# 		super( slots );
//# 		this.adChannelVersionId = adChannelVersionId;
//# 
//# 		if ( autoLoadAd ) {
//# 			// TODO o que fazer se não houver/não conseguir carregar um anúncio do AdManager?
//# 			try {
//# 				setAd( AdManager.getAdByChannelId( adChannelVersionId ) );
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					AppMIDlet.log( e );
				//#endif
//# 			}
//# 		}
//# 	}
//# 
//# 
//# 	public Ad getAd() {
//# 		return ad;
//# 	}
//# 
//# 
//# 	/**
//# 	 * Define o anúncio atual do canal.
//# 	 * 
//# 	 * @param ad anúncio atual do canal.
//# 	 */
//# 	public void setAd( Ad ad ) {
//# 		flushVisualizations();
//# 		this.ad = ad;
//# 	}
//# 
//# 
//# 	/**
//# 	 * Método que deve ser chamado quando o canal começa a ser visualizado.
//# 	 * @see #endVisualization()
//# 	 */
//# 	public void startVisualization() {
//# 		// se o startTime não for igual a TIME_NONE, já está sendo visualizado
//# 		if ( ad != null && startTime == TIME_NONE ) {
//# 			startTime = System.currentTimeMillis();
			//#if DEBUG == "true"
//# 				System.out.println( "startVisualization: ad #" + ad.getVersionId() + ", " + new Date( startTime ) );
			//#endif
//# 
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * Método que deve ser chamado quando o canal deixa de ser visualizado.
//# 	 * @see #startVisualization() 
//# 	 */
//# 	public void endVisualization() {
//# 		if ( ad != null && startTime != TIME_NONE ) {
//# 			final Vector v = getCurrentVector();
//# 			v.addElement( new Point( ( int ) ( startTime - START_TIME ), ( int ) ( System.currentTimeMillis() - startTime ) ) );
			//#if DEBUG == "true"
//# 				System.out.println( "endVisualization: ad #" + ad.getVersionId() + ", " + new Date( startTime ) + " -> " + new Date( System.currentTimeMillis() ) );
			//#endif
//# 
//# 			startTime = TIME_NONE;
//# 
//# 			if ( v.size() >= SAVE_RATE )
//# 				flushVisualizations();
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * Evento recebido quando o canal deixa de ser visualizado. Por padrão, se não for evento do aparelho (ou seja,
//# 	 * é o ScreenManager sinalizando que o canal deixou de ser a tela ativa), o método flushVisualizations() é chamado.
//# 	 *
//# 	 * @param deviceEvent
//# 	 * @see #flushVisualizations() 
//# 	 */
//# 	public void hideNotify( boolean deviceEvent ) {
//# 		endVisualization();
//# 
//# 		if ( !deviceEvent ) {
//# 			// o canal saiu de foco; grava as informações de visualização no RMS
//# 			flushVisualizations();
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * Salva as visualizações acumuladas no RMS e limpa o vetor.
//# 	 */
//# 	protected void flushVisualizations() {
//# 		endVisualization();
//# 		
//# 		if ( !visualizations.isEmpty() ) {
//# 			try {
//# 				AdManager.saveVisualizations( visualizations, ad );
//# 				visualizations.clear();
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					AppMIDlet.log( e );
				//#endif
//# 			}
//# 		}
//# 	}
//# 
//# 
//# 	public void showNotify( boolean deviceEvent ) {
//# 		startVisualization();
//# 	}
//# 
//# 
//# 	/**
//# 	 * Obtém o vetor correspondente ao customer atual.
//# 	 * @return
//# 	 */
//# 	private final Vector getCurrentVector() {
//# 		final Integer key = new Integer( NanoOnline.getCurrentCustomer().getId() );
//# 		Vector v = ( Vector ) visualizations.get( key );
//# 		if ( v == null ) {
//# 			v = new Vector();
//# 			visualizations.put( key, v );
//# 		}
//# 
//# 		return v;
//# 	}
//# 
//# 
//# }
//# 
//#endif