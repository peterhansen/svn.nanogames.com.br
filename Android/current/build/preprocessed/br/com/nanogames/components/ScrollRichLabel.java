/**
 * ScrollRichLabel.java
 * ©2008 Nano Games.
 *
 * Created on Jul 8, 2008 10:59:48 PM.
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.ScrollBar;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;

//#if TOUCH == "true"
	import br.com.nanogames.components.userInterface.PointerListener;
//#endif


/**
 * 
 * @author Peter
 */
public class ScrollRichLabel extends UpdatableGroup implements KeyListener
	//#if TOUCH == "true"
		, PointerListener
	//#endif
{
	
	protected static final byte INDEX_LABEL = 0;
	protected static final byte INDEX_SCROLL_FULL = 1;
	protected static final byte INDEX_SCROLL_PAGE = 2;
	
	/** Modo de arrasto do ponteiro: sem arrasto. */
	protected static final byte DRAG_MODE_NONE			= 0;
	/** Modo de arrasto do ponteiro: arrastando barra de scroll da página atual. */
	protected static final byte DRAG_MODE_SCROLL_PAGE	= 1;
	/** Modo de arrasto do ponteiro: arrastando texto. */
	protected static final byte DRAG_MODE_TEXT			= 2;

	protected final RichLabel label;
	
	protected Drawable scrollFull;
	
	protected Drawable scrollPage;

	/** Velocidade de movimentação do texto. */
	protected final MUV textSpeed = new MUV();
	
	/** Velocidade máxima de movimentação do texto. No caso de movimentação automática ou através de teclas, esse valor
	 * é sempre utilizado. No caso de movimentação através de ponteiro, a velocidade é gradual.
	 */
	protected short TEXT_SPEED;
	
	/** Limite superior do texto. */
	protected short textLimitTop;
	
	/** Limite inferior do texto. */
	protected short textLimitBottom;
	
	/** Indica se o scroll do texto está no modo automático. */
	protected boolean autoScroll;	
	
	/** Última posição do ponteiro (valor utilizado para fazer scroll). */
	protected int lastPointerY;	
	
	/** Indica o tipo de arrasto do ponteiro. */
	protected byte dragMode;
	
	/** Posição de início do arrasto da barra de scroll da página. */
	protected short dragYStart;
	
	/** Offset do texto no momento em que a barra de scroll da página começou a ser arrastada. */
	protected short textOffsetStart;
	
	/** Armazena a última tecla apertada. Ver keyPressed() e keyReleased() */
	private int lastKeyPressed;

	/** Indica se houve alteração no scroll do texto (necessário em aparelhos BlackBerry com rolagem, pois não é possível
	 * manter pressionado para baixo, por exemplo). */
	private boolean scrollChanged;
	
	
	public ScrollRichLabel( RichLabel label ) {
		super( 3 );

		//#if DEBUG == "true"
//# 		if ( label == null )
//# 			throw new IllegalArgumentException( "ScrollRichLabel: label não pode ser nulo." );
		//#endif

		this.label = label;
		insertDrawable( label );
	}
	
	
	public ScrollRichLabel( RichLabel label, Drawable scrollFull, Drawable scrollPage ) {
		this( label );
		
		setScrollFull( scrollFull );
		setScrollPage( scrollPage );
		
		setAutoScroll( false );
	}


	public void update( int delta ) {
		super.update( delta );

		if ( textSpeed.getSpeed() != 0 ) {
			final int dy = textSpeed.updateInt( delta );

			if ( dy != 0 ) {
				scrollChanged = true;
				setTextOffset( label.getTextOffset() + dy );
			}
		}
	}
	
	
	public void setTextOffset( int offset ) {
		if ( offset > textLimitBottom ) {
			offset = textLimitBottom;
		} else if ( offset < textLimitTop ) {
			if ( autoScroll ) {
				offset = textLimitBottom;
			} else {
				offset = textLimitTop;
				setTextSpeed( 0 );
			}
		}
		
		label.setTextOffset( offset );
		updateScroll();
	}


	public final int getTextOffset() {
		return label.getTextOffset();
	}


	public final int getTextLimitTop() {
		return textLimitTop;
	}


	public final int getTextLimitBottom() {
		return textLimitBottom;
	}
	
	
	protected void setTextSpeed( int speed ) {
		if ( autoScroll ) {
			textSpeed.setSpeed( -Math.abs( TEXT_SPEED ) );
		} else {
			textSpeed.setSpeed( speed );
			scrollChanged = false;
		}
	}	
	

	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			if ( !autoScroll ) {
				y -= position.y;

				switch ( dragMode ) {
					case DRAG_MODE_SCROLL_PAGE:
						// posiciona o texto de acordo com a posição relativa da barra de scroll
						final int FP_SCROLL_TO_TEXT = NanoMath.divInt( size.y, scrollPage.getHeight() );
						final int FP_TEMP = NanoMath.toFixed( y - dragYStart );
						setTextOffset( textOffsetStart - NanoMath.toInt( NanoMath.mulFixed( FP_SCROLL_TO_TEXT, FP_TEMP ) ) );
					break;

					case DRAG_MODE_TEXT:
						setTextOffset( label.getTextOffset() + y - lastPointerY );
						lastPointerY = y;
					break;
				}
			}
		}


		public void onPointerPressed( int x, int y ) {
			if ( !autoScroll ) {
				x -= position.x;
				y -= position.y;

				if ( scrollPage != null && scrollPage.contains( x, y ) ) {
					// usuário clicou na barra de scroll da página atual
					dragMode = DRAG_MODE_SCROLL_PAGE;
					lastPointerY = y;
					dragYStart = ( short ) y;
					textOffsetStart = ( short ) label.getTextOffset();
				} else if ( scrollFull != null && scrollFull.contains( x, y ) ) {
					// usuário clicou na barra de scroll total
					dragMode = DRAG_MODE_NONE;

					if ( scrollPage != null ) {
						// avança ou retrocede uma página, de acordo com a posição clicada na barra
						keyPressed( y < scrollPage.getPosY() ? ScreenManager.LEFT : ScreenManager.RIGHT );
					}
				} else {
					// usuário clicou no texto
					dragMode = DRAG_MODE_TEXT;
					lastPointerY = y;
				}
			}
		}


		public void onPointerReleased( int x, int y ) {
			dragMode = DRAG_MODE_NONE;
		}
	//#endif
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );

		label.setSize( width, label.getHeight() );
		final int TEXT_TOTAL_HEIGHT = label.getTextTotalHeight();
		
		if ( scrollFull != null ) {
			if ( TEXT_TOTAL_HEIGHT <= height ) {
				scrollFull.setVisible( false );
				
				label.setSize( size );
			} else {
				scrollFull.setVisible( true );
				
				scrollFull.setPosition( width - scrollFull.getWidth(), 0 );
				scrollFull.setSize( scrollFull.getWidth(), size.y );

				label.setSize( scrollFull.getPosX(), height );
			}
		} else {
			label.setSize( size );
		}
		
		if ( scrollPage != null ) {
			scrollPage.setVisible( TEXT_TOTAL_HEIGHT > height );
			scrollPage.setPosition( width - scrollPage.getWidth(), 0 );
		}

		label.formatText( false );
		
		setAutoScroll( autoScroll );
		
		if ( autoScroll )
			setTextOffset( textLimitBottom );
		else
			updateScroll();
	}


	public final void setInitialLine( int i ) {
		label.setInitialLine( i );
	}


	public final void setFonts( ImageFont[] fonts ) {
		label.setFonts( fonts );
	}


	public final int getTextTotalHeight() {
		return label.getTextTotalHeight();
	}


	public final short getInitialLine() {
		return label.getInitialLine();
	}


	public final ImageFont getFont( int index ) {
		return label.getFont( index );
	}

	
	protected void updateScroll() {
		if ( scrollPage != null ) {
			final int TEXT_TOTAL_HEIGHT = label.getTextTotalHeight();
			
			if( TEXT_TOTAL_HEIGHT > 0 ) {
				scrollPage.setSize( scrollPage.getWidth(), Math.min( ( size.y * size.y / TEXT_TOTAL_HEIGHT ) + 1, size.y ) );
				scrollPage.setPosition( scrollPage.getPosX(), size.y * -label.getTextOffset() / TEXT_TOTAL_HEIGHT );
			}
		}
	}


	public void keyPressed( int key ) {
		// Armazena a tecla apertada para posterior utilização em keyReleased()
		lastKeyPressed = key;

		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				setTextSpeed( Math.abs( TEXT_SPEED ) );
			break;
			
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				setTextSpeed( -Math.abs( TEXT_SPEED ) );
			break;
			
			case ScreenManager.LEFT:
			case ScreenManager.KEY_NUM4:
				if ( !autoScroll )
					setTextOffset( label.getTextOffset() + size.y );
			break;			
			
			case ScreenManager.RIGHT:
			case ScreenManager.KEY_NUM6:
				if ( !autoScroll )
					setTextOffset( label.getTextOffset() - size.y );
			break;
			
			default:
				setTextSpeed( 0 );
		} // fim switch ( key )
	}


	public void keyReleased( int key ) {
		// Esse if faz com que os aparelhos não travem a rolagem ao mudarmos sua direção repentinamente. No
		// entanto, alguns aparelhos podem não mandar o evento de release necessário se duas teclas forem
		// apertadas simultaneamente.
		if ( key == lastKeyPressed ) {
			// se o usuário soltar a tecla antes de ter feito algum scroll, faz o scroll mínimo (altura da fonte)
			if ( !scrollChanged ) {
				switch ( key ) {
					case ScreenManager.UP:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.DOWN:
					case ScreenManager.KEY_NUM8:
						setTextOffset( label.getTextOffset() + ( textSpeed.getSpeed() > 0 ? label.getFont().getHeight() : -label.getFont().getHeight() ) );
					break;
				} // fim switch ( key )
			}

			setTextSpeed( 0 );
		}
	}
	
	
	public void setAutoScroll( boolean autoScroll ) {
		this.autoScroll = autoScroll;
		
		if ( autoScroll ) {
			TEXT_SPEED = ( short ) ( ( label.getFont().getHeight() * 3 ) >> 1 );
			textLimitTop = ( short ) -label.getTextTotalHeight();
			textLimitBottom = ( short ) getHeight();

			setTextSpeed( TEXT_SPEED );
		} else {
			TEXT_SPEED = ( short ) ( ( label.getFont().getHeight() * 13 ) >> 1 );
			textLimitBottom = 0;
			textLimitTop = ( short ) NanoMath.min( -label.getTextTotalHeight() + size.y, 0 );
		}			
	}
	
	
	public final Drawable getScrollFull() {
		return scrollFull;
	}
	
	
	public final Drawable getScrollPage() {
		return scrollPage;
	}
	
	
	public void setScrollFull( Drawable scrollFull ) {
		if ( this.scrollFull != null )
			removeDrawable( INDEX_SCROLL_FULL );
		
		if ( scrollFull != null )
			insertDrawable( scrollFull, INDEX_SCROLL_FULL );
		
		this.scrollFull = scrollFull;
		
		setSize( size );
	}
	
	
	public void setScrollPage( Drawable scrollPage ) {
		if ( this.scrollPage != null )
			removeDrawable( scrollFull == null ? INDEX_SCROLL_FULL : INDEX_SCROLL_PAGE );
		
		if ( scrollFull != null )
			insertDrawable( scrollPage, INDEX_SCROLL_PAGE );
		
		this.scrollPage = scrollPage;

		setSize( size );
	}


	public final void setText( String text, boolean formatText, boolean autoSize ) {
		label.setText( text, formatText, autoSize );
		setSize( size );
	}

	
	public final void setText( String text ) {
		label.setText( text );
		setSize( size );
	}

	
	public final String getText() {
		return label.getText();
	}
	
	
	
}
