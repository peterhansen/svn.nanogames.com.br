/**
 * RankingMenu.java
 * 
 * Created on 1/Fev/2009, 11:45:05
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.userInterface.AppMIDlet;

/**
 *
 * @author Peter
 */
public class RankingMenu extends BasicMenu {
	
	public static final byte RANKING_MENU_ALL		= 0;
	public static final byte RANKING_MENU_LOCAL		= 1;
	public static final byte RANKING_MENU_GLOBAL	= 2;
	
	private static final byte MENU_ENTRY_LOCAL	= 0;
	private static final byte MENU_ENTRY_GLOBAL	= 1;
	private static final byte MENU_ENTRY_BACK	= 2;
	
	private final byte type;
	

	/**
	 * Cria um novo menu de ranking geral. Equivalente ao contrutor <code>RankingMenu( RANKING_MENU_ALL )</code>.
	 * @throws java.lang.Exception
	 */
	public RankingMenu() throws Exception {
		this( RANKING_MENU_ALL );
	}

	
	public RankingMenu( byte type ) throws Exception {
		this( type, -1 );
	}


	/**
	 * 
	 * @param type
	 * @throws java.lang.Exception
	 */
	public RankingMenu( byte type, int backIndex ) throws Exception {
		super( Math.max( RankingScreen.getTotalTypes(), 10 ) );
		
		this.type = type;
		setId( SCREEN_RECORDS );

		backScreenIndex = backIndex;
		
		String[] entriesText = null;
		switch ( type ) {
			case RANKING_MENU_ALL:
				if ( backScreenIndex < 0 )
					backScreenIndex = SCREEN_MAIN_MENU;
				
				entriesText = new String[] { NanoOnline.getText( TEXT_LOCAL_RANKING ), NanoOnline.getText( TEXT_GLOBAL_RANKING ), NanoOnline.getText( TEXT_BACK ) };
				backEntry = MENU_ENTRY_BACK;
			break;
			
			case RANKING_MENU_LOCAL:
			case RANKING_MENU_GLOBAL:
				if ( backScreenIndex < 0 )
					backScreenIndex = SCREEN_RECORDS;
				
				final int[] entries = RankingScreen.getTypesEntries();
				entriesText = new String[ entries.length + 1 ];

				for ( byte i = 0; i < entries.length; ++i ) {
					entriesText[ i ] = AppMIDlet.getText( entries[ i ] );
				}
				
				backEntry = ( byte ) ( entriesText.length - 1 );
				entriesText[ backEntry ] = NanoOnline.getText( TEXT_BACK );
			break;
		}
		
		addItems( entriesText );
	}


	protected void buttonPressed( int index ) {
		try {
			switch ( type ) {
				case RANKING_MENU_ALL:
					switch ( index ) {
						case MENU_ENTRY_LOCAL:
							if ( RankingScreen.getTotalTypes() > 1 )
								NanoOnline.getForm().setContentPane( new RankingMenu( RANKING_MENU_LOCAL, SCREEN_MAIN_MENU ) );
							else
								NanoOnline.getForm().setContentPane( new RankingScreen( false, 0, getId() ) );
						break;

						case MENU_ENTRY_GLOBAL:
							if ( RankingScreen.getTotalTypes() > 1 )
								NanoOnline.getForm().setContentPane( new RankingMenu( RANKING_MENU_GLOBAL, SCREEN_MAIN_MENU ) );
							else
								NanoOnline.getForm().setContentPane( new RankingScreen( true, 0, getId() ) );
						break;

						case MENU_ENTRY_BACK:
							onBack();
						break;
					}
				break;

				case RANKING_MENU_LOCAL:
				case RANKING_MENU_GLOBAL:
					if ( index == backEntry ) {
						NanoOnline.setScreen( SCREEN_MAIN_MENU );
					} else if ( index >= 0 ) {
						// TODO adicinar tipo de recorde ao título da tela
						NanoOnline.getForm().setContentPane( new RankingScreen( type == RANKING_MENU_GLOBAL, index, getId() ) );
						setBackIndex( getId() );
					} else {
						onBack();
					}
				break;
			}
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "36");
			//#endif
		}
		
	}

}
