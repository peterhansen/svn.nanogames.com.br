//#if AD_MANAGER == "true"
//# /*
//#  * ResourceManager.java
//#  *
//#  * Created on February 19, 2010, 18:51:04
//#  *
//#  */
//# package br.com.nanogames.components.online.ad;
//# 
//# import br.com.nanogames.components.userInterface.AppMIDlet;
//# import br.com.nanogames.components.util.Point;
//# import java.io.ByteArrayInputStream;
//# import java.io.ByteArrayOutputStream;
//# import java.io.DataInputStream;
//# import java.io.DataOutputStream;
//# import java.util.Enumeration;
//# import java.util.Hashtable;
//# import javax.microedition.rms.InvalidRecordIDException;
//# import javax.microedition.rms.RecordStore;
//# import javax.microedition.rms.RecordStoreException;
//# import javax.microedition.rms.RecordStoreFullException;
//# import javax.microedition.rms.RecordStoreNotOpenException;
//# 
//# 
//# /**
//#  *
//#  * @author Peter
//#  */
//# public final class ResourceManager implements ResourceTypes {
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: versão do gerenciador. Tipo de dado: string */
//# 	protected static final byte PARAM_RESOURCE_MANAGER_VERSION	= 0;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade total de recursos. Formato dos dados:
//# 	 * short (quantidade total de recursos)
//# 	 * leitura dos N recursos
//# 	 */
//# 	protected static final byte PARAM_RESOURCES_TOTAL			= PARAM_RESOURCE_MANAGER_VERSION + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: tamanho dos dados (em bytes). Tipo do dado: int, 
//# 	 * depois os n bytes indicados pelo tamanho. */
//# 	protected static final byte PARAM_RESOURCE_DATA_SIZE		= PARAM_RESOURCES_TOTAL + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: tipo do recurso. Formato do dado: short */
//# 	protected static final byte PARAM_RESOURCE_TYPE				= PARAM_RESOURCE_DATA_SIZE + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: id do recurso. Formato do dado: int */
//# 	protected static final byte PARAM_RESOURCE_ID				= PARAM_RESOURCE_TYPE + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade de recursos "filhos".
//# 	 * Formato dos dados: short indicando a quantidade, e então N * int ids dos filhos. */
//# 	protected static final byte PARAM_N_CHILDREN				= PARAM_RESOURCE_ID + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade de recursos órfãos. 
//# 	 * Esse parâmetro é necessário quando ocorre de um recurso ser necessário mas não estar disponível, e o cliente então
//# 	 * informa ao servidor os ids desses recursos para que seja refeito o download.
//# 	 * Formato dos dados: short indicando a quantidade, e então N * int ids dos filhos.
//# 	 */
//# 	protected static final byte PARAM_ORPHAN_RESOURCES			= PARAM_N_CHILDREN + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: fim de todos os dados. Formato dos dados: nenhum. */
//# 	protected static final byte PARAM_RESOURCE_MANAGER_END		= PARAM_ORPHAN_RESOURCES + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: quantidade total de arquivos (recursos filhos).
//# 	 * Formato dos dados:
//# 	 * short (quantidade total de arquivos)
//# 	 * leitura dos N arquivos
//# 	 */
//# 	protected static final byte PARAM_FILES_TOTAL				= PARAM_RESOURCE_MANAGER_END + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: fim dos dados de um recurso. Formato dos dados: nenhum. */
//# 	protected static final byte PARAM_RESOURCE_END				= PARAM_FILES_TOTAL + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: fim dos dados de um arquivo. Formato dos dados: nenhum. */
//# 	protected static final byte PARAM_FILE_END					= PARAM_RESOURCE_END + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: tipo do arquivo. Formato do dado: short */
//# 	protected static final byte PARAM_FILE_TYPE					= PARAM_FILE_END + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: id do arquivo. Formato do dado: int */
//# 	protected static final byte PARAM_FILE_ID				= PARAM_FILE_TYPE + 1;
//# 
//# 	/** Id de parâmetro de leitura de dados do gerenciador de recursos: identificador interno em relação ao recurso pai. Formato do dado: short */
//# 	protected static final byte PARAM_RESOURCE_INTERNAL_ID			= PARAM_FILE_ID + 1;
//# 
//# 
//# 
//# 	/** Versão do gerenciador de recursos. */
//# 	public static final String RESOURCE_MANAGER_VERSION = "0.0.1";
//# 
//# 	/** Nome da base de dados do gerenciador de recursos. */
//# 	public static final String RESOURCE_MANAGER_DATABASE_NAME = "rm";
//# 
//# 	private static final byte SLOT_RESOURCES_INDEXES	= 1;
//# 	private static final byte SLOT_FILES_INDEXES		= 2;
//# 
//# 	private static final byte SLOTS_INITIAL				= 2;
//# 
//# 	private static final byte TABLE_INDEX_RESOURCES		= 0;
//# 	private static final byte TABLE_INDEX_FILES			= 1;
//# 	private static final byte TABLE_INDEX_BOTH			= 2;
//# 
//# 	private final Hashtable[] tables = new Hashtable[ SLOTS_INITIAL ];
//# 
//# 	/***/
//# 	private static ResourceManager instance;
//# 
//# 	/***/
//# 	private final RecordStore rsResources;
//# 
//# 
//# 	/**
//# 	 * 
//# 	 * @throws java.lang.Exception
//# 	 */
//# 	private ResourceManager() throws Exception {
//# 		final boolean newDatabase = AppMIDlet.createDatabase( RESOURCE_MANAGER_DATABASE_NAME, SLOTS_INITIAL );
//# 		rsResources = RecordStore.openRecordStore( RESOURCE_MANAGER_DATABASE_NAME, false );
//# 
//# 		for ( byte i = 0; i < tables.length; ++i )
//# 			tables[ i ] = new Hashtable();
//# 	}
//# 
//# 
//# 	/**
//# 	 * Indica se o gerenciador de recursos foi carregado.
//# 	 * @return boolean indicando se o gerenciador de recursos foi carregado.
//# 	 * @see #load()
//# 	 * @see #unload()
//# 	 */
//# 	public static final boolean isLoaded() {
//# 		return instance != null;
//# 	}
//# 
//# 
//# 	/**
//# 	 * Carrega o gerenciador de recursos. Esse método deve ser chamado antes de qualquer operação de leitura ou escrita de
//# 	 * dados de recursos.
//# 	 * @throws java.lang.Exception
//# 	 * @see #isLoaded()
//# 	 * @see #unload() 
//# 	 */
//# 	public static final void load() throws Exception {
//# 		if ( instance == null ) {
//# 			instance = new ResourceManager();
//# 
//# 			// preenche a tabela de associação recurso x slot
//# 			ByteArrayInputStream byteInput = null;
//# 			DataInputStream dataInput = null;
//# 
//# 			for ( byte slot = SLOT_RESOURCES_INDEXES, tableIndex = 0; slot <= SLOT_FILES_INDEXES; ++slot, ++tableIndex ) {
//# 				try {
//# 					final byte[] data = instance.rsResources.getRecord( slot );
//# 					byteInput = new ByteArrayInputStream( data );
//# 					dataInput = new DataInputStream( byteInput );
//# 
//# 					// lê a versão do gerenciador de recursos
//# 					final String version = dataInput.readUTF();
//# 					final short totalResources = dataInput.readShort();
//# 					for ( short i = 0; i < totalResources; ++i ) {
//# 						addIndex( tableIndex, dataInput.readInt(), dataInput.readInt() );
//# 					}
                    //#if DEBUG == "true"
//#                         printResources();
                    //#endif
//# 				} catch ( Exception e ) {
					//#if DEBUG == "true"
//# 						AppMIDlet.log( e );
					//#endif
//# 						
//# 					save( tableIndex );
//# 				} finally {
//# 					// libera os recursos dos streams
//# 					if ( byteInput != null ) {
//# 						try {
//# 							byteInput.close();
//# 						} catch ( Exception e ) {}
//# 						byteInput = null;
//# 					}
//# 
//# 					if ( dataInput != null ) {
//# 						try {
//# 							dataInput.close();
//# 						} catch ( Exception e ) {}
//# 						dataInput = null;
//# 					}
//# 				}
//# 			}
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * 
//# 	 * @param hashTableIndex
//# 	 * @param resourceId
//# 	 * @param slotIndex
//# 	 */
//# 	private static final void addIndex( int hashTableIndex, int resourceId, int slotIndex ) {
//# 		instance.tables[ hashTableIndex ].put( new Integer( resourceId ), new Integer( slotIndex ) );
//# 
		//#if DEBUG == "true"
//# 			System.out.println( "addIndex( " + resourceId + " ): " + instance.tables[ hashTableIndex ].get( new Integer( resourceId ) ) );
		//#endif
//# 	}
//# 
//# 
//# 	/**
//# 	 *
//# 	 * @param hashTableIndex
//# 	 * @param resourceId
//# 	 * @param slotIndex
//# 	 */
//# 	private static final void removeIndex( int hashTableIndex, int resourceId ) {
//# 		instance.tables[ hashTableIndex ].remove( new Integer( resourceId ) );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Descarrega o gerenciador de recursos da memória. Métodos chamados posteriormente sem uma chamada a <code>load</code>
//# 	 * lançarão exceções.
//# 	 * 
//# 	 * @see #isLoaded()
//# 	 * @see #load()
//# 	 */
//# 	public static final void unload() {
//# 		if ( instance != null ) {
//# 			try {
//# 				instance.rsResources.closeRecordStore();
//# 			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 					AppMIDlet.log( e );
				//#endif
//# 			}
//# 			instance = null;
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * 
//# 	 * @throws java.lang.Exception
//# 	 */
//# 	protected static final void save() throws Exception {
//# 		save( TABLE_INDEX_BOTH );
//# 	}
//# 
//# 
//# 	/**
//# 	 * 
//# 	 * @throws java.lang.Exception
//# 	 */
//# 	protected static final void save( byte tablesToSave ) throws Exception {
//# 		load();
//# 		
//# 		ByteArrayOutputStream byteOutput = null;
//# 		DataOutputStream dataOutput = null;
//# 
//# 		byte start;
//# 		byte end;
//# 		switch ( tablesToSave ) {
//# 			case TABLE_INDEX_BOTH:
//# 				start = SLOT_RESOURCES_INDEXES;
//# 				end = SLOT_FILES_INDEXES;
//# 			break;
//# 
//# 			default:
//# 				start = end = ( byte ) ( tablesToSave + 1 );
//# 		}
//# 
//# 		for ( int slot = start, tableIndex = start - 1; slot <= end; ++slot, ++tableIndex ) {
			//#if DEBUG == "true"
//# 				System.out.println( "ResourceManager.save: " + slot + ", TABLEINDEX: " + tableIndex + ", END: " + end + ", INSTANCE: " + instance + ", LOADED: " + isLoaded() );
			//#endif
//# 			try {
//# 				byteOutput = new ByteArrayOutputStream();
//# 				dataOutput = new DataOutputStream( byteOutput );
//# 
//# 				final Hashtable t = instance.tables[ tableIndex ];
//# 
//# 				dataOutput.writeUTF( RESOURCE_MANAGER_VERSION );
//# 
//# 				final int totalResources = t.size();
//# 				dataOutput.writeShort( totalResources );
//# 
//# 				final Enumeration r = t.keys();
//# 				while ( r.hasMoreElements() ) {
//# 					final int key = ( ( Integer ) r.nextElement() ).intValue();
//# 					dataOutput.writeInt( key );
//# 					dataOutput.writeInt( ( ( Integer ) t.get( new Integer( key ) ) ).intValue() );
//# 				}
//# 
//# 				dataOutput.flush();
//# 
//# 				final byte[] data = byteOutput.toByteArray();
//# 				instance.rsResources.setRecord( slot, data, 0, data.length );
				//#if DEBUG == "true"
//# 					System.out.println( "SAVED" );
				//#endif
//# 			} catch ( RecordStoreFullException rsfe ) {
				//#if DEBUG == "true"
//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados: não há espaço disponível." );
				//#else
//# 					throw rsfe;
				//#endif
//# 			} catch ( InvalidRecordIDException ire ) {
				//#if DEBUG == "true"
//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inválido: " + slot );
				//#else
//# 					throw ire;
				//#endif
//# 			} finally {
//# 				if ( byteOutput != null ) {
//# 					try {
//# 						byteOutput.close();
//# 					} catch ( Exception e ) {
//# 					}
//# 					byteOutput = null;
//# 				} // fim if ( byteOutput != null )
//# 
//# 				if ( dataOutput != null ) {
//# 					try {
//# 						dataOutput.close();
//# 					} catch ( Exception e ) {
//# 					}
//# 					dataOutput = null;
//# 				} // fim if ( dataOutput != null )
//# 			} // fim finally
//# 		} // fim for ( byte slot = start, tableIndex = 0; slot <= end; ++slot, ++tableIndex )
//# 	}
//# 
//# 
//# 	/**
//# 	 * Apaga um recurso do RMS.
//# 	 * @param resourceId id do recurso, segundo informado pelo servidor.
//# 	 */
//# 	public static final void deleteResource( final int resourceId ) {
//# 		deleteResource( TABLE_INDEX_RESOURCES, resourceId );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Apaga um arquivo do RMS.
//# 	 * @param fileId id do arquivo, segundo informado pelo servidor.
//# 	 */
//# 	public static final void deleteFile( final int fileId ) {
//# 		deleteResource( TABLE_INDEX_FILES, fileId );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Apaga um recurso (ou arquivo) do RMS.
//# 	 * @param resourceId id do recurso a ser apagado.
//# 	 * @param tableIndex índice da tabela onde o recurso deve ser buscado. Valores válidos são: <code>TABLE_INDEX_FILES</code>
//# 	 * e <code>TABLE_INDEX_RESOURCES</code>.
//# 	 */
//# 	private static final void deleteResource( final byte tableIndex, final int resourceId ) {
//# 		try {
//# 			final int slot = ( ( Integer ) instance.tables[ tableIndex ].get( new Integer( resourceId ) ) ).intValue();
//# 			final Resource res = getResource( tableIndex, resourceId );
//# 
//# 			instance.rsResources.deleteRecord( slot );
//# 			removeIndex( tableIndex, resourceId );
//# 
//# 			if ( res != null && !res.isFile() ) {
//# 				final int[] childrenIds = res.getChildrenIds();
//# 				for ( short i = 0; i < childrenIds.length; ++i ) {
//# 					final Resource child = getResource( TABLE_INDEX_FILES, childrenIds[ i ] );
//# 					if ( child.decrementUse() ) {
//# 						// salva o recurso no RMS
//# 						saveResource( child );
//# 					} else {
//# 						// recurso não tem mais uso; apaga do RMS
//# 						delete( child );
//# 					}
//# 				}
//# 			}
//# 		} catch ( InvalidRecordIDException ex ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( ex );
			//#endif
//# 		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( e );
			//#endif
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * Indica se um determinado recurso existe localmente.
//# 	 * @param resourceId id do recurso, segundo informado pelo servidor.
//# 	 * @return boolean indicando se o recurso existe localmente.
//# 	 */
//# 	public static final boolean hasResource( final int resourceId ) {
//# 		return instance.tables[ TABLE_INDEX_RESOURCES ].containsKey( new Integer( resourceId ) );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Obtém um recurso a partir do seu id.
//# 	 * 
//# 	 * @param resourceId id go recurso, segundo informado pelo servidor.
//# 	 * @return recurso pedido, ou <code>null</code> caso não seja encontrado um recurso com esse id.
//# 	 * 
//# 	 * @see #getFile(int) 
//# 	 */
//# 	public static final Resource getResource( final int resourceId ) {
//# 		return getResource( TABLE_INDEX_RESOURCES, resourceId );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Obtém um recurso a partir de seu id.
//# 	 *
//# 	 * @param resourceId
//# 	 * @param tableIndex
//# 	 * @return
//# 	 */
//# 	private static final Resource getResource( byte tableIndex, final int resourceId ) {
//# 		try {
//# 			final int slot = ( ( Integer ) instance.tables[ tableIndex ].get( new Integer( resourceId ) ) ).intValue();
//# 			return new Resource( instance.rsResources.getRecord( slot ) );
//# 		} catch ( Exception e ) {
//# 			return null;
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * Obtém a tabela e o slot onde se encontra um determinado recurso.
//# 	 * @param res
//# 	 * @return Point indicando a tabela (x) e slot (y) do recurso.
//# 	 */
//# 	private static final Point getTableAndSlot( final Resource res ) {
//# 		final byte tableIndex = res.isFile() ? TABLE_INDEX_FILES : TABLE_INDEX_RESOURCES;
//# 		return new Point( tableIndex, ( ( Integer ) instance.tables[ tableIndex ].get( new Integer( res.getId() ) ) ).intValue() );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Obtém um arquivo do RMS.
//# 	 * @param fileId id do arquivo, conforme informado pelo servidor.
//# 	 * @return referência para o arquivo carregado.
//# 	 * 
//# 	 * @see #hasFile(int)
//# 	 * @see #getResource(int)
//# 	 */
//# 	public static final Resource getFile( final int fileId ) {
//# 		return getResource( TABLE_INDEX_FILES, fileId );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Indica se um arquivo existe localmente.
//# 	 * @param fileId id do arquivo a ser verificado, conforme informado pelo servidor.
//# 	 * @return boolean indicando se o arquivo existe localmente.
//# 	 */
//# 	public static final boolean hasFile( final int fileId ) {
//# 		return instance.tables[ TABLE_INDEX_FILES ].containsKey( new Integer( fileId ) );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Apaga um recurso do RMS.
//# 	 * @param resource recurso a ser apagado do RMS.
//# 	 * @throws NullPointerException caso <code>resource</code> seja nulo.
//# 	 */
//# 	public static final void delete( final Resource resource ) {
//# 		deleteResource( resource.isFile() ? TABLE_INDEX_FILES : TABLE_INDEX_RESOURCES, resource.getId() );
//# 	}
//# 
//# 
//# 	/**
//# 	 * Grava um recurso no RMS.
//# 	 * @param r
//# 	 * @throws java.lang.Exception
//# 	 */
//# 	protected static final void saveResource( final Resource res ) throws Exception {
//# 		ByteArrayOutputStream byteOutput = null;
//# 		DataOutputStream dataOutput = null;
//# 
//# 		try {
//# 			byteOutput = new ByteArrayOutputStream();
//# 			dataOutput = new DataOutputStream( byteOutput );
//# 
//# 			res.write( dataOutput );
//# 			dataOutput.flush();
//# 
//# 			final Point pos = getTableAndSlot( res );
//# 
//# 			final byte[] data = byteOutput.toByteArray();
//# 			instance.rsResources.setRecord( pos.y, data, 0, data.length );
//# 		} catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados: não há espaço disponível." );
			//#else
//# 				throw rsfe;
			//#endif
//# 		} catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inválido: " );
			//#else
//# 				throw ire;
			//#endif
//# 		} finally {
//# 			if ( byteOutput != null ) {
//# 				try {
//# 					byteOutput.close();
//# 				} catch ( Exception e ) {}
//# 			} // fim if ( byteOutput != null )
//# 
//# 			if ( dataOutput != null ) {
//# 				try {
//# 					dataOutput.close();
//# 				} catch ( Exception e ) {}
//# 			} // fim if ( dataOutput != null )
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * 
//# 	 * @param r
//# 	 * @param saveTables indica se as tabelas devem ser salvas
//# 	 * @throws java.lang.Exception
//# 	 */
//# 	protected static final void addResource( final Resource r, final boolean saveTables ) throws Exception {
//# 		ByteArrayOutputStream byteOutput = null;
//# 		DataOutputStream dataOutput = null;
//# 
//# 		try {
//# 			byteOutput = new ByteArrayOutputStream();
//# 			dataOutput = new DataOutputStream( byteOutput );
//# 
//# 			r.write( dataOutput );
//# 			dataOutput.flush();
//# 
//# 			final byte[] data = byteOutput.toByteArray();
//# 			final int newSlotIndex = instance.rsResources.addRecord( data, 0, data.length );
//# 			byte tableIndex = r.isFile() ? TABLE_INDEX_FILES : TABLE_INDEX_RESOURCES;
//# 			addIndex( tableIndex, r.getId(), newSlotIndex );
//# 
//# 			if ( saveTables )
//# 				save( tableIndex );
//# 
            //#if DEBUG == "true"
//#                 printResources();
            //#endif
//# 		} catch ( RecordStoreFullException rsfe ) {
			//#if DEBUG == "true"
//# 				throw new RecordStoreFullException( "Erro ao gravar na base de dados: não há espaço disponível." );
			//#else
//# 				throw rsfe;
			//#endif
//# 		} catch ( InvalidRecordIDException ire ) {
			//#if DEBUG == "true"
//# 				throw new InvalidRecordIDException( "Erro ao gravar na base de dados: slot inválido: " );
			//#else
//# 				throw ire;
			//#endif
//# 		} finally {
//# 			if ( byteOutput != null ) {
//# 				try {
//# 					byteOutput.close();
//# 				} catch ( Exception e ) {}
//# 			} // fim if ( byteOutput != null )
//# 
//# 			if ( dataOutput != null ) {
//# 				try {
//# 					dataOutput.close();
//# 				} catch ( Exception e ) {}
//# 			} // fim if ( dataOutput != null )
//# 		}
//# 	}
//# 
//# 
//# 	/**
//# 	 * Faz o parser dos dados vindos do servidor.
//# 	 *
//# 	 * @param input stream de leitura dos dados. 
//# 	 * @throws java.lang.Exception
//# 	 */
//# 	public static final void parseParams( DataInputStream input ) throws Exception {
//# 		load();
//# 		byte paramId;
//# 		boolean readingData = true;
//# 
//# 		do {
//# 			paramId = input.readByte();
			//#if DEBUG == "true"
//# 				System.out.println( "ID DO RESOURCE MANAGER LIDO: " + paramId );
			//#endif
//# 
//# 			switch ( paramId ) {
//# 				case PARAM_FILES_TOTAL:
//# 				case PARAM_RESOURCES_TOTAL:
//# 					final short totalResources = input.readShort();
					//#if DEBUG == "true"
//# 						System.out.println( "LENDO " + totalResources + ( paramId == PARAM_RESOURCES_TOTAL ? " RECURSOS" : " ARQUIVOS" ) );
					//#endif
//# 					for ( short i = 0; i < totalResources; ++i ) {
//# 						// aloca um recurso "dummy" para leitura dos dados
//# 						final Resource dummy = new Resource();
//# 						dummy.parseParams( input );
//# 						
//# 						// verifica se o recurso/arquivo já existe
//# 						final boolean exists = ( paramId == PARAM_FILES_TOTAL ) ? hasFile( dummy.getId() ) : hasResource( dummy.getId() );
						//#if DEBUG == "true"
//# 							System.out.println( "EXISTE? " + exists );
						//#endif
//# 						if ( exists ) {
//# 							// se recurso ou arquivo existir, mas não tiver dados, o salva com novas informações
//# 							dummy.setUses( ( ( paramId == PARAM_FILES_TOTAL ) ? getFile( dummy.getId() ) : getResource( dummy.getId() ) ).getUses() );
//# 							saveResource( dummy );
//# 						} else {
//# 							// recurso não existe - cria um novo
//# 							addResource( dummy, false );
//# 						}
//# 
//# 						// adiciona os filhos (arquivos), incrementando seus contadores
//# 						final int[] resourceChildren = dummy.getChildrenIds();
//# 						for ( short childIndex = 0; childIndex < resourceChildren.length; ++childIndex ) {
//# 							Resource r = getFile( resourceChildren[ childIndex ] );
//# 							if ( r == null ) {
//# 								r = new Resource();
//# 								r.setId( resourceChildren[ childIndex ] );
//# 								r.setType( FILE_TYPE_NONE );
//# 								r.incrementUse();
//# 								addResource( r, false );
//# 							} else {
//# 								r.incrementUse();
//# 								saveResource( r );
//# 							}
//# 						}
//# 					}
//# 				break;
//# 
//# 				case PARAM_RESOURCE_MANAGER_END:
					//#if DEBUG == "true"
//# 						System.out.println( "RESOURCE MANAGER: FIM DOS DADOS" );
					//#endif
//# 					readingData = false;
//# 					save( TABLE_INDEX_BOTH );
//# 				break;
//# 
				//#if DEBUG == "true"
//# 					default:
//# 						throw new IllegalArgumentException( "id inválido lido em ResourceManager: " + paramId );
				//#endif
//# 			}
//# 		} while ( readingData );
//# 	}
//# 
//# 
    //#if DEBUG == "true"
//#         public static final void printResources() {
//#             System.out.println( "\nResourceManager ----> LISTA DE RECURSOS E ARQUIVOS: <----" );
//#             for ( byte i = TABLE_INDEX_RESOURCES; i <= TABLE_INDEX_FILES; ++i ) {
//#                 final Enumeration keys = instance.tables[ i ].keys();
//#                 while ( keys.hasMoreElements() ) {
//#                     final Integer key = ( Integer ) keys.nextElement();
//#                     System.out.println( ( i == TABLE_INDEX_RESOURCES ? "RECURSO" : "ARQUIVO" ) + " ID " + key + " -> SLOT " + instance.tables[ i ].get( key ) );
//#                 }
//#             }
//#             System.out.println( "ResourceManager ----> FIM DA LISTA DE RECURSOS E ARQUIVOS <----\n" );
//#         }
    //#endif
//# 
//# }
//# 
//#endif