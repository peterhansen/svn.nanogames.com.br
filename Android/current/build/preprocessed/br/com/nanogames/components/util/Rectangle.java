/*
 * Rectangle.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.util;

/**
 *
 * @author peter
 */
public final class Rectangle {
 
    /** Posição x (esquerda) do retângulo. */
	public int x;
    
	/** Posição y (topo) do retângulo. */
	public int y;
    
	/** Largura do retângulo. */
	public int width;
	
    /** Altura do retângulo. */
	public int height;

    
	public Rectangle( Rectangle r ) {
		this( r.x, r.y, r.width, r.height );
	}
    
	 
	public Rectangle() {
	}
	 
    
	public Rectangle( int x, int y, int width, int height ) {
        set( x, y, width, height );
	}


	public Rectangle( Point position, Point size ) {
		this( position.x, position.y, size.x, size.y );
	}
    
	 
	public final boolean contains( int x, int y ) {
		return x >= this.x && y >= this.y && ( x <= this.x + width ) && ( y <= this.y + height );
	}
	 
    
	public final boolean contains( Point point ) {
		return contains( point.x, point.y );
	}
    
	 
	public final void set( int x, int y, int width, int height ) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
	}
	 
    
	public final void set( Rectangle r ) {
		set( r.x, r.y, r.width, r.height );
	}


	public final void set( Point position, Point size ) {
		set( position.x, position.y, size.x, size.y );
	}
    
	 
	public final boolean intersects( Rectangle r ) {
		return x <= ( r.x + r.width ) 
               && ( x + width ) >= r.x
			   && y <= ( r.y + r.height )
			   && ( y + height ) >= r.y;
	}
    
	 
	public final Rectangle union( Rectangle r ) {
		return new Rectangle( Math.min( x, r.x ), Math.min( y, r.y ),
                              Math.max( x + width, r.x + r.width ), Math.max( y + height, r.y + r.height )  );
	}
	
	
	public final void setInsersection( Rectangle r ) {
		setIntersection( r.x, r.y, r.width, r.height );
	}


	public final void setIntersection( Rectangle r1, Rectangle r2 ) {
		final int right1 = r1.x + r1.width;
		final int bottom1 = r1.y + r1.height;
		final int right2 = r2.x + r2.width;
		final int bottom2 = r2.y + r2.height;
		
		if ( r1.x <= right2 && right1 >= r2.x && r1.y <= bottom2 && bottom1 >= r2.y ) {
			x = Math.max( r1.x, r2.x );
			y = Math.max( r1.y, r2.y );
			width = Math.min( right1, right2 ) - x;
			height = Math.min( bottom1, bottom2 ) - y;
		} else {
			set( 0, 0, 0, 0 );
		}				
	}
	
	
	public final void setIntersection( int x, int y, int width, int height ) {
		final int right1 = this.x + this.width;
		final int bottom1 = this.y + this.height;
		final int right2 = x + width;
		final int bottom2 = y + height;
		
		if ( this.x <= right2 && right1 >= x && this.y <= bottom2 && bottom1 >= y ) {
			this.x = Math.max( this.x, x );
			this.y = Math.max( this.y, y );
			this.width = Math.min( right1, right2 ) - this.x;
			this.height = Math.min( bottom1, bottom2 ) - this.y;
		} else {
			set( 0, 0, 0, 0 );
		}		
	}




	//#if DEBUG == "true"
//# 		public final String toString() {
//# 			return "( " + x + ", " + y + ", " + width + ", " + height + " )";
//# 		}
	//#endif
	
}
 
