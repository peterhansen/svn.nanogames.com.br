/*
Nano Logger - Copyright 2009 - Nano Games.
 */
package br.com.nanogames.components.util;

import br.com.nanogames.components.userInterface.AppMIDlet;
import java.io.ByteArrayOutputStream;
import br.com.nanogames.components.util.DynamicByteArray;
import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Classe de log de eventos de sistema.
 * @author Daniel Monteiro
 */
public final class NanoLogger  {


	/** Constantes*/
    /** Aplicacao acabou de ser iniciada*/
    public static final byte APP_STARTED = -128;
    /** Aplicacao suspensa*/
    public static final byte APP_SUSPENDED = -127;
    /** Aplicacao retomada*/
    public static final byte APP_RESUMED = -126;
    /** Aplicacao finalizada*/
    public static final byte APP_ENDED = -125;
    /** Detectado erro no log*/
    public static final byte APP_LOG_ERROR = -124;
	/** entrou no jogo*/
    public static final byte GAME_STARTED = -123;
    /** suspendeu o jogo (possivelmente pause)*/
    public static final byte GAME_SUSPENDED = -122;
    /** retornou ao jogo*/
    public static final byte GAME_RESUMED = -121;
    /** saiu do jogo*/
    public static final byte GAME_ENDED = -120;
    /** Valor do sinal base para os sinais personalizados da aplicacao*/
    public static final byte GAME_BASE_SIGNAL = 0;
	/** Constantes de limite */
    /** Tamanho maximo do log em RMS - Ainda nao e' usado. */
    public static final short SIZE_LIMIT_IN_RMS = 3072;
    /** Tamanho maximo do log em memoria*/
    public static final short SIZE_LIMIT_IN_MEMORY = 512;
	/** Fontes de evento */
    /** Evento interno ao logger*/
    public static final byte INTERNAL_EVENT		= -2;
    /** Evento interno aos componentes, mas externo ao logger*/
    public static final byte COMPONENTS_EVENT	= -1;
    /** Evento de jogo */
    public static final byte GAME_EVENT			= 0;


	/** Campos */
    private static int slot;
    private final DynamicByteArray loggerBuffer;
	/** instancia unica
	 * @see #getInstance()
	 */
    private static NanoLogger iInstance;
    private final static String DATABASE_NAME = "nl";
    private final static byte DATABASE_SLOT_MANAGER = 1;

	
	/** Metodos */


    /**
     * Descarrega e manda para o RMS os dados atuais na memoria
	 * @see #load()
     */
    public static final void unload() {
        NanoLogger.finishLog();
        flush();
        ///previne o ressurgimento de eventos antigos, de execucoes anteriores
        ///no buffer, que se tornariam duplicados
        System.gc();
        iInstance = null;
    }


	/**
     * Pre-carrega na memoria uma instancia da classe
	 * @see #unload()
     */
    public static final void load() {
        iInstance = new NanoLogger();
    }


    /**
     * Inicializacao dos membros estaticos da classe
	 * @see #load
	 * @see #getInstance
     */
    static {
        try {
            ///obtem o proximo slot livre
            AppMIDlet.loadData(DATABASE_NAME, DATABASE_SLOT_MANAGER, new Serializable() {

                public final void write(DataOutputStream output) throws Exception {
                }

                public final void read(DataInputStream input) throws Exception {
                    //se o registro existe, assume que ele esta correto
                    slot = input.readInt();
                }
            });
        } catch (Exception ex) {
            try {
                ///nao existe ou corrompido. Criando um registro limpo
                AppMIDlet.createDatabase(DATABASE_NAME, DATABASE_SLOT_MANAGER);
                updateSlotManager(DATABASE_SLOT_MANAGER);
                //#if DEBUG == "true"
//# 				NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
				//#else
							NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
				//#endif

            } catch (Exception ex1) {
                //#if DEBUG == "true"
//# 				AppMIDlet.log( ex , "102");
//# 				NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
				//#else
							NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
				//#endif
            }
        }
        NanoLogger.startLog();
    }


    /**
     * Obtem instancia unica para a classe
     * @return A instancia corrente. caso nao haja uma, ocorre a criacao primeiro
	 * @see #load()
     */
    public static final NanoLogger getInstance() {
        if (iInstance == null) 
            iInstance = new NanoLogger();        
        return iInstance;
    }


    /**
     * Insere evento de inicio de log.
	 * @see #finishLog()
     */
    public static final void startLog() {
        NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_STARTED);
    }


    /**
     * Insere evento de fim de log
	 * @see #startLog()
     */
    public static final void finishLog() {
        NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_ENDED);
    }


    /**
     * Executa mandatoriamente a descarga de eventos para armazenamento
	 * @see #flush(boolean)
     */
    public static final void flush() {
        flush(true);
    }


    /**
     * Executa o flush mediante condicoes de limite de tamanho ou por ordem explicita
     * @param force Se o flush deve ou nao ser obrigatoriamente executado
	 * @see #saveData()
     */
    public static final void flush(boolean force) {
        if (force || NanoLogger.bufferLength() >= SIZE_LIMIT_IN_MEMORY)
            saveData();        
    }


    /**
     * Constutor privado. Instancia o buffer de eventos em memoria
	 * @see #getInstance()
     */
    private NanoLogger() {
        loggerBuffer = new DynamicByteArray();
    }


	/**
	 *
	 * @param caller
	 * @param event
	 * @see #insertEvent(int, int, java.lang.String)
	 */
	public static final void insertEvent( int caller, int event ) {
		insertEvent(caller, event, null);
	}


    /**
     * Insere evento no log
     * @param caller ID da entidade que esta efetuando/sofrendo o evento
     * @param event ID do evento
     * @param aux string auxiliar para debug
	 * @see #insertEvent(int, int)
     */
    public static final void insertEvent(int caller, int event, String aux) {
        ByteArrayOutputStream BOS = new ByteArrayOutputStream();
        DataOutputStream DOS = new DataOutputStream(BOS);
        long sysdate = System.currentTimeMillis();

        if (aux == null)
            aux = "";

        try {
            DOS.writeShort(event);
            DOS.writeShort(caller);
            DOS.writeLong(sysdate);
            DOS.writeUTF(aux);
            DOS.flush();
            getInstance().loggerBuffer.insertData(BOS.toByteArray(), 0, BOS.size());
        } catch (IOException ex) {
            //#if DEBUG == "true"
//#             AppMIDlet.log( ex , "103");
//# 			NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
			//#else
						NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
			//#endif
        } finally {
            if (BOS != null) {

                try {
                    BOS.close();
                } catch (Exception ex) {
                //#if DEBUG == "true"
//# 				NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
				//#else
							NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
				//#endif
                }

                try {
                    DOS.close();
                } catch (Exception ex) {
                //#if DEBUG == "true"
//# 				NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
				//#else
							NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
				//#endif
                }
            }
        }
    }


    /**
     * Escreve os dados armazenados de todas as sessoes no stream de saida
     * @param output stream de saida para onde os dados serao escritos
	 * @see #insertEvent(int, int)
	 * @see #flush()
     */
    public static final void writeData(final DataOutputStream output) {
        try {
            for (int c = DATABASE_SLOT_MANAGER + 1; c <= slot; c++) {
                try {
                    AppMIDlet.loadData(DATABASE_NAME, c, new Serializable() {

                        public final void write(DataOutputStream output) throws Exception {
                        }

                        public final void read(DataInputStream input) throws Exception {                             
                            ///primeiro obtem o tamanho dos dados a serem lidos e depois gravados...
                            final int len = input.readInt();
                            ///depois realiza a transferencia de streams.
                            for (int d = 0; d < len; d++) 
                                output.writeByte(input.readByte());
                        }
                    });

                } catch (Exception ex) {
                    //#if DEBUG == "true"
//#                     AppMIDlet.log( ex , "104");
//# 					NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
					//#else
								NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
					//#endif
                }
            }
            output.write(getInstance().loggerBuffer.getData(), 0, NanoLogger.bufferLength());
        } catch (IOException ex) {
            //#if DEBUG == "true"
//#             AppMIDlet.log( ex , "105");
//# 			NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
			//#else
						NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
			//#endif
        }
    }


    /**
     * Obtem o tamanho do buffer
     * @return tamanho atual do buffer em bytes
	 * @see #loggerBuffer
     */
    public static final int bufferLength() {
        return getInstance().loggerBuffer.length();
    }


    /**
     * Re-insere os dados retirados do slot atual do RMS na memoria
	 * @see #saveData()
     */
    public static final void loadData() {
        try {
            AppMIDlet.loadData(DATABASE_NAME, slot, new Serializable() {

                public final void write(DataOutputStream output) throws Exception {
                }

                public final void read(DataInputStream input) throws Exception {
                    final int len = input.readInt();
                    final byte[] buffer = new byte[len];
                    input.read(buffer, 0, len);
                    getInstance().loggerBuffer.insertData(buffer, 0, len);
                }
            });
        } catch (Exception ex) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( ex , "106");
//# 			NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
			//#else
						NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
			//#endif
        }
    }


    /**
     * Salva os dados de log em RMS
	 * @see #loadData
     */
    public static final void saveData() {
        if (getInstance().loggerBuffer.length() > 0) {
            try {
                ///Garante que nao estamos passando dos limites
                slot = slot + 1 % Integer.MAX_VALUE;

                ///trata situacao de borda limite superior, apesar de ser altamente improvavel
                if (slot == 0)
                    slot = DATABASE_SLOT_MANAGER + 1;

                ///abre o slot livre e salva as informacoes contidas no buffer de memoria
                AppMIDlet.createDatabase(DATABASE_NAME, slot);
                AppMIDlet.saveData(DATABASE_NAME, slot, new Serializable() {

                    public final void write(DataOutputStream output) throws Exception {
                        output.writeInt(getInstance().loggerBuffer.length());
                        output.write(getInstance().loggerBuffer.getData(), 0, getInstance().loggerBuffer.length());
                        output.flush();
                    }

                    public final void read(DataInputStream input) throws Exception {
                    }
                });

                ///Atualiza o slot de indice
                updateSlotManager(slot);
            } catch (Exception ex) {
			//#if DEBUG == "true"
//#                 AppMIDlet.log( ex , "107");
//# 			NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
			//#else
						NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
			//#endif
            }
        }
    }


    /**
     * Obtem o nome da base de dados do log
     * @return retorna a string do nome da base
	 * @see #DATABASE_NAME
     */
    public static final String getDatabaseName() {
        return DATABASE_NAME;
    }


    /**
     * Obtem o numero do slot de dados do log
     * @return retorna o inteiro referente ao slot indice
	 * @see #DATABASE_SLOT_MANAGER
     */
    public static final int getDatabaseSlot() {
        return DATABASE_SLOT_MANAGER;
    }


    /**
     * Atualiza o slot de indice com o valor do slot atual
     * @param newSlot O novo slot atual
	 * @see #saveData()
	 * @see #slot
     */
    private static final void updateSlotManager(final int newSlot) {
        try {
            AppMIDlet.saveData(DATABASE_NAME, DATABASE_SLOT_MANAGER, new Serializable() {

                public final void write(DataOutputStream output) throws Exception {
                    output.writeInt(newSlot);
                    output.flush();
                    slot = newSlot;

                }

                public final void read(DataInputStream input) throws Exception {
                }
            });
        } catch (Exception ex) {
			//#if DEBUG == "true"
//# 			AppMIDlet.log( ex ,"108");
//# 			NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR, ex.getClass() + "\n" + ex.getMessage() );
			//#else
						NanoLogger.insertEvent(INTERNAL_EVENT, NanoLogger.APP_LOG_ERROR );
			//#endif
        }

    }
}
