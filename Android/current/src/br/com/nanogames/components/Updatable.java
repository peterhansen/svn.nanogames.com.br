package br.com.nanogames.components;

/*
 * Updatable.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

/**
 *
 * @author peter
 */
public interface Updatable {
 
	/**
	 * Atualiza o estado de um objeto após <i>delta</i> milisegundos.
	 * @param delta intervalo de tempo considerado para a atualização.
	 */
	public abstract void update( int delta );
}
 
