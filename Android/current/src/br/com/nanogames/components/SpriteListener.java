/*
 * SpriteListener.java
 *
 * Created on October 5, 2007, 7:32 PM
 *
 */

package br.com.nanogames.components;

/**
 *
 * @author peter
 */
public interface SpriteListener {
	
	/**
	 * Método chamado por um sprite, indicando o fim de uma sequência de animação.
	 * @param id identificação do sprite.
	 * @param sequence índice da sequência encerrada.
	 */
	public void onSequenceEnded( int id, int sequence );
	
	
	/**
	 * Método chamado por um sprite, indicando a mudança de um frame
	 * @param id identificação do sprite.
	 * @param frameSequenceIndex índice do frame na sequência atual.
	 */
	public void onFrameChanged( int id, int frameSequenceIndex );
}
