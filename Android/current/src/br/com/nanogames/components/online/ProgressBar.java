/**
 * ProgressBar.java
 * 
 * Created on 16/Dez/2008, 17:06:42
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.borders.DrawableBorder;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.util.NanoMath;

/**
 *
 * @author Peter
 */
public final class ProgressBar extends Container implements NanoOnlineConstants, ConnectionListener {

	// TODO criar classe AbstractProgressBar, para poder reproduzir o comportamento dessa classe, porém com outras identidades visuais

	/** Tipo de mensagem exibida pela barra de progresso: informação (cinza). */
	public static final byte MSG_TYPE_INFO			= 0;

	/** Tipo de mensagem exibida pela barra de progresso: aviso (amarelo). */
	public static final byte MSG_TYPE_WARNING		= 1;

	/** Tipo de mensagem exibida pela barra de progresso: confirmação (verde). */
	public static final byte MSG_TYPE_CONFIRMATION	= 2;

	/** Tipo de mensagem exibida pela barra de progresso: erro (vermelho). */
	public static final byte MSG_TYPE_ERROR			= 3;

	private static final int[] COLORS = { 0x008291,
										  0xffff00,
										  0x00ff00,
										  0xff0000 };

	/** Velocidade da animaçao da barra de progresso, em pixels por segundo. */
	private static final byte BAR_PATTERN_SPEED = 20;

	/** Diferença na posição x da barra de progresso em relação às bordas. */
	private static final byte BAR_OFFSET_X = 6;

	/** Diferença de posiçao da borda em relação à barra de progresso. */
	private static final byte BAR_BORDER_OFFSET = 1;

	/** Espaçamento entre os items da barra. */
	private static final byte ITEMS_SPACING = 3;
	
	/** Índice da tecla da soft key direita. */
	public static final byte ID_SOFT_RIGHT = -123;
	
	private static final byte TOTAL_ITEMS = 10;
	
	private static final short KILOBYTE = 1 << 10;
	
	private static final short HALF_KILOBYTE = KILOBYTE >> 1;

	private static final byte LABEL_OFFSET_X = 2;
	
	/** Label que indica o status atual. */
	private final MarqueeLabel label;
	
	/***/
	private final BasicAnimatedPattern barRead;
	
	/***/
	private final Pattern barTotal;

	private final Border barBorder;

	private short barTotalWidth;

	private final DrawableImage divider;
	
	/***/
	public final Button softkeyRight;
	
	/** Total de bytes a baixar. */
	private int contentLengthTotal;
	
	/** bytes baixados até o momento. */
	private int contentLengthRead;
	
	
	public ProgressBar() throws Exception {
		super( TOTAL_ITEMS );
		
		final Pattern bkg = new Pattern( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "bl.png" ) );
		bkg.setSize( Short.MAX_VALUE, bkg.getHeight() );
		insertDrawable( bkg);
		
		barRead = new BasicAnimatedPattern( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "bar.png" ), BAR_PATTERN_SPEED, 0 );
		barRead.setPosition( BAR_OFFSET_X, ( bkg.getHeight() - barRead.getHeight() ) >> 1 );
		barRead.setAnimation( BasicAnimatedPattern.ANIMATION_HORIZONTAL );
		insertDrawable( barRead );
		
		barTotal = new Pattern( COLORS[ MSG_TYPE_INFO ] );
		barTotal.setSize( 0, barRead.getHeight() );
		barTotal.setPosition( barRead.getPosition() );
		insertDrawable( barTotal );

		barBorder = new LineBorder( 0x888888, LineBorder.TYPE_ROUND_RAISED );
		barBorder.setPosition( barRead.getPosX() - BAR_BORDER_OFFSET, barRead.getPosY() - BAR_BORDER_OFFSET );
		insertDrawable( barBorder );

		divider = new DrawableImage( PATH_NANO_ONLINE_IMAGES + "d.png" );
		insertDrawable( divider );
		
		label = new MarqueeLabel( NanoOnline.getFont( FONT_SMALL ), null );
		label.setPosition( barRead.getPosX() + LABEL_OFFSET_X, barRead.getPosY() + ( ( barRead.getHeight() - label.getHeight() ) >> 1 ) );
		label.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		label.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		insertDrawable( label );
		
		softkeyRight = new Button( NanoOnline.getFont( FONT_WHITE ), null );
		softkeyRight.setFocusable( false );
		softkeyRight.addEventListener( new EventListener() {
			public void eventPerformed( Event evt ) {
				System.out.println( evt );
				switch ( evt.eventType ) {
					//#if TOUCH == "true"
						case Event.EVT_BUTTON_CONFIRMED:
							try {
								if ( softkeyRight.getText().equals( NanoOnline.getText( TEXT_BACK ) ) ) {
									System.out.println( "A" );
									( ( NanoOnlineContainer )NanoOnline.getForm().getContentPane() ).onBack();
//									evt.consume();
								} else if ( softkeyRight.getText().equals( NanoOnline.getText( TEXT_EXIT ) ) ) {
									System.out.println( "B" );
									NanoOnline.exit();
								} else {
									System.out.println( "C" );
									NanoOnline.getForm().keyPressed( ScreenManager.KEY_SOFT_RIGHT );
								}
							} catch ( Exception e ) {
								//#if DEBUG == "true"
//# 									AppMIDlet.log( e , "29");
								//#endif
							}
						break;
					//#endif
				}
			}
		} );
		insertDrawable( softkeyRight );

		setBorder( new DrawableBorder( new Pattern( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "bl.png" ) ) ) );

		setSize( 0, bkg.getHeight() );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		softkeyRight.setPosition( width - softkeyRight.getWidth() - ITEMS_SPACING, ( height - softkeyRight.getHeight() ) >> 1 );
		divider.setPosition( softkeyRight.getPosX() - divider.getWidth() - ITEMS_SPACING, ( height - divider.getHeight() ) >> 1 );

		barTotalWidth = ( short ) ( divider.getPosX() - ( BAR_OFFSET_X << 1 ) );
		barBorder.setSize( barTotalWidth + ( BAR_BORDER_OFFSET << 1 ), barRead.getHeight() + ( BAR_BORDER_OFFSET << 1 ) );

		label.setSize( barTotalWidth - ( LABEL_OFFSET_X << 1 ), label.getHeight() );

		refreshProgress();
	}


	public final String getUIID() {
		return "progressbar";
	}


	/**
	 *
	 * @param id
	 * @param data
	 */
	public final void processData( int id, byte[] data ) {
	}


	/**
	 *
	 * @param id
	 * @param infoIndex
	 * @param extraData
	 */
	public final void onInfo( int id, int infoIndex, Object extraData ) {
		//#if DEBUG == "true"
//# 			System.out.println( "onInfo( " + id + ", " + infoIndex + ", " + ( extraData == null ? "" : extraData.toString() ) + ")");
		//#endif
			
		final StringBuffer buffer = new StringBuffer();
		switch ( infoIndex ) {
			case INFO_CONNECTION_OPENED:
				buffer.append( NanoOnline.getText( TEXT_CONNECTION_OPENED ) );
			break;

			case INFO_OUTPUT_STREAM_OPENED:
			case INFO_DATA_WRITTEN:
				buffer.append( NanoOnline.getText( TEXT_SENDING_DATA ) );
			break;

			case INFO_RESPONSE_CODE:
				buffer.append( NanoOnline.getText( TEXT_RESPONSE_CODE ) );
				buffer.append( ( ( Integer ) extraData ).intValue() );
			break;

			case INFO_INPUT_STREAM_OPENED:
				buffer.append( NanoOnline.getText( TEXT_RECEIVING_DATA ) );
			break;

			case INFO_CONTENT_LENGTH_TOTAL:
				contentLengthTotal = ( ( Integer ) extraData ).intValue();
				refreshProgress();
				refreshLabel( buffer );
			break;

			case INFO_CONTENT_LENGTH_READ:
				contentLengthRead = ( ( Integer ) extraData ).intValue();
				refreshProgress();

				buffer.append( NanoOnline.getText( TEXT_RECEIVING_DATA ) );
				refreshLabel( buffer );
			break;

			case INFO_CONNECTION_ENDED:
			return;
		}

		showMessage( MSG_TYPE_INFO, buffer.toString() );
		refreshLayout();
	} // fim do método onInfo( int, int, Object )


	/**
	 * 
	 * @param id
	 * @param errorIndex
	 * @param extraData
	 */
	public final void onError( int id, int errorIndex, Object extraData ) {
		switch ( errorIndex ) {
			case ERROR_HTTP_CONNECTION_NOT_SUPPORTED:
			case ERROR_CANT_GET_RESPONSE_CODE:
			case ERROR_URL_BAD_FORMAT:
			case ERROR_CONNECTION_EXCEPTION:
			case ERROR_CANT_CLOSE_INPUT_STREAM:
			case ERROR_CANT_CLOSE_CONNECTION:
			case ERROR_CANT_CLOSE_OUTPUT_STREAM:
				final Exception e = ( Exception ) extraData;
				showMessage( MSG_TYPE_ERROR, NanoOnline.getText( TEXT_ERROR ) + errorIndex + ": " + e.getMessage() );
			break;
		}
	}
	

	/**
	 *
	 */
	private final void refreshProgress() {
		if ( contentLengthTotal > 0 ) {
			barRead.setSize( barTotalWidth * contentLengthRead / contentLengthTotal, barRead.getHeight() );
		} else {
			barRead.setSize( 0, barRead.getHeight() );
		}
		barTotal.setSize( barTotalWidth - barRead.getWidth(), barTotal.getHeight() );
		barTotal.setPosition( barRead.getPosX() + barRead.getWidth(), barTotal.getPosY() );
	}
	

	/**
	 *
	 * @param buffer
	 */
	private final void refreshLabel( StringBuffer buffer ) {
		buffer.append( ' ' );

		String unit = "";
		// primeiro atualiza o contador de progresso efetuado
		if ( contentLengthRead < HALF_KILOBYTE ) {
			buffer.append( contentLengthRead );
			unit = " bytes";
		} else if ( contentLengthTotal < NanoMath.MAX_INTEGER_VALUE ) {
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthRead, KILOBYTE ) ) );
			unit = " kB";
		} else {
			// ultrapassa o valor máximo permitido no fixedPoint, então arredonda o valor
			buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthRead / KILOBYTE, KILOBYTE ), 4 ) );
			unit = "  MB";
		}

		// agora atualiza o indicador do total de bytes, caso ele seja válido (nem sempre o tamanho total é conhecido)
		if ( contentLengthTotal > 0 ) {
			buffer.append( '/' );
			
			if ( contentLengthTotal < HALF_KILOBYTE ) {
				buffer.append( contentLengthTotal );
			} else if ( contentLengthTotal < NanoMath.MAX_INTEGER_VALUE ) {
				buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthTotal, KILOBYTE ) ) );
			} else {
				// ultrapassa o valor máximo permitido no fixedPoint, então arredonda o valor
				buffer.append( NanoMath.toString( NanoMath.divInt( contentLengthTotal / KILOBYTE, KILOBYTE ), 4 ) );
			}
		}

		buffer.append( unit );
		
		// TODO adicionar "..." indicando progresso
		//#if DEBUG == "true"
//# 			try {
//# 				Thread.sleep( 40 ); // reduz velocidade para poder acompanhar barra de download no emulador
//# 			} catch ( InterruptedException ex ) {
//# 			}
		//#endif
	}
	

	/**
	 * 
	 * @param text
	 */
	public final void setSoftKey( String text ) {
		softkeyRight.setText( text, true );
		setSize( size );
	} // fim do método setSoftKey( byte, Drawable, boolean, int )	


	/**
	 *
	 * @param errorIndex
	 */
	public final void showError( int errorIndex ) {
		showMessage( MSG_TYPE_ERROR, errorIndex );
	}


	/**
	 *
	 * @param infoIndex
	 */
	public final void showInfo( int infoIndex ) {
		showMessage( MSG_TYPE_INFO, infoIndex );
	}


	/**
	 *
	 * @param info
	 */
	public final void showInfo( String info ) {
		showMessage( MSG_TYPE_INFO, info );
	}


	/**
	 *
	 * @param errorIndex
	 */
	public final void showWarning( int errorIndex ) {
		showMessage( MSG_TYPE_WARNING, errorIndex );
	}


	/**
	 *
	 * @param errorIndex
	 */
	public final void showConfirmation( int textIndex ) {
		showMessage( MSG_TYPE_CONFIRMATION, textIndex );
	}


	/**
	 *
	 * @param type
	 * @param msgIndex
	 */
	public final void showMessage( byte type, int msgIndex ) {
		showMessage( type, NanoOnline.getText( msgIndex ) );
	}


	/**
	 *
	 * @param type
	 * @param message
	 */
	public final void showMessage( byte type, String message ) {
		//#if DEBUG == "true"
//# 			System.out.println( "ProgressBar.showMessage: " + type + " -> " + message );
		//#endif

		label.setText( message == null ? null : message.toUpperCase(), false );
		label.setTextOffset( 0 );
		barTotal.setFillColor( COLORS[ type ] );

		switch ( type ) {
			case MSG_TYPE_CONFIRMATION:
			case MSG_TYPE_ERROR:
				contentLengthRead = 0;
			break;
		}

		refreshProgress();
	}


	/**
	 *
	 */
	public final void clearProgressBar() {
		showMessage( MSG_TYPE_INFO, null );
		contentLengthRead = 0;
		contentLengthTotal = 0;
		refreshProgress();
	}
	
}
