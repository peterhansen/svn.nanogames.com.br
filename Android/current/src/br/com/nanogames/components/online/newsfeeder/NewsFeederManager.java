package br.com.nanogames.components.online.newsfeeder;

import br.com.nanogames.components.online.*;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreFullException;


/**
 *
 */
public final class NewsFeederManager implements Serializable, NanoOnlineConstants {

	/** Versão do gerenciador de notícias. */
	public static final String NEWS_FEEDER_VERSION = "0.0.3";

	/** Indica o título da categoria de atualização disponível (usado para fazer o controle de atualizações). */
	public static final String NEWS_FEEDER_CATEGORY_APP_UPDATE_TITLE = "APP_UPDATE";

	/** Máximo de entradas de notícias de cada categoria exibidas e armazenadas no celular. */
	public static final byte MAX_NEWS_ENTRIES = 20;

	/** Momento da última atualização do news feeder. */
	private static long lastUpdated;

	// <editor-fold desc="Informações de atualizações de versão">

	/** Indica se a última notificação de atualização é obrigatória. */
	private static boolean updateRequired;

	/** Número da última versão disponível informada ao usuário. */
	private static String updateVersion;

	/** Entrada contendo as informações de atualização do aplicativo. */
	private static NewsFeederEntry updateEntry;

	// </editor-fold>

	/**  */
	private NewsFeederCategory[] categories = new NewsFeederCategory[ 0 ];


	public final void load() {
		try {
			AppMIDlet.loadData( DATABASE_NAME, DATABASE_SLOT_NEWS_FEEDER_FIRST + NanoOnline.getLanguage(), this );
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "42");
			//#endif
			NewsFeederManager.lastUpdated = 0;
			try {
				save();
			} catch ( Exception e2 ) {
			}
		}
	}


	/**
	 *
	 * @return
	 */
	public static final NewsFeederEntry getUpdateEntry() {
		return updateEntry;
	}


	/**
	 *
	 * @return
	 */
	public static final long getLastUpdated() {
		return lastUpdated;
	}


	/**
	 * Obtém todas as entradas de notícias de categorias públicas.
	 * 
	 * @return
	 */
	public NewsFeederEntry[] getEntries() {
		final Vector entriesVector = new Vector();
		for ( short i = 0; i < categories.length; ++i ) {
			if ( categories[ i ].isVisible() ) {
				// obtém o slot da categoria atual e carrega suas entradas
				final SlotManager slotManager = new SlotManager();
				slotManager.load( categories[ i ].getSlot() );
				for ( byte j = 0; j < slotManager.entries.length; ++j )
					entriesVector.addElement( slotManager.entries[ j ] );
			}
		}
		final NewsFeederEntry[] entries = new NewsFeederEntry[ entriesVector.size() ];
		entriesVector.copyInto( entries );

		return entries;
	}


	/**
	 * Obtém todas as entradas de notícias de uma determinada categoria, seja pública ou privada.
	 * @param categoryId id da categoria, conforme salvo no banco de dados.
	 * @return array das entradas de notícias, ou null a categoria não seja encontrada.
	 */
	public NewsFeederEntry[] getEntries( int categoryId ) {
		final int slot = getSlot( categoryId );
		if ( slot > 0 ) {
			final SlotManager slotManager = new SlotManager();
			slotManager.load( slot );
			return slotManager.entries;
		}

		return new NewsFeederEntry[ 0 ];
	}


	/**
	 *
	 * @throws java.lang.Exception
	 */
	public final void save() throws Exception {
		AppMIDlet.saveData( DATABASE_NAME, DATABASE_SLOT_NEWS_FEEDER_FIRST + NanoOnline.getLanguage(), this );
	}


	/**
	 * 
	 * @param category
	 * @return
	 */
	public final int getSlot( int categoryId ) {
		for ( short i = 0; i < categories.length; ++i ) {
			if ( categories[ i ].getId() == categoryId )
				return categories[ i ].getSlot();
		}

		return -1;
	}


	/**
	 * 
	 * @param output
	 * @throws java.lang.Exception
	 */
	public final void write( DataOutputStream output ) throws Exception {
		output.writeUTF( NEWS_FEEDER_VERSION );
		output.writeLong( lastUpdated );
		// se houver informação de atualização do aplicativo, armazena as informações
		output.writeBoolean( updateVersion != null );
		if ( updateVersion != null ) {
			output.writeBoolean( updateRequired );
			output.writeUTF( updateVersion );
			updateEntry.write( output );
		}

		// quantidade total de categorias
		output.writeShort( categories.length );
		for ( short i = 0; i < categories.length; ++i ) {
			categories[ i ].write( output );
		}
	}


	/**
	 * 
	 * @param input
	 * @throws java.lang.Exception
	 */
	public final void read( DataInputStream input ) throws Exception {
		// lê a versão do news feeder
		final String version = input.readUTF();
		if ( version.compareTo( NEWS_FEEDER_VERSION ) < 0 ) {
			// havia versão mais antiga do gerenciador salva no RMS
			// TODO apagar versão antiga do slot do news feeder
		}
		
		lastUpdated = input.readLong();
		if ( input.readBoolean() ) {
			// há notificação de atualização salva
			updateRequired = input.readBoolean();
			updateVersion = input.readUTF();
			updateEntry = new NewsFeederEntry();
			updateEntry.read( input );
			// se a atualização armazenada for menor ou igual à versão atual, a atualização não é necessária
			if ( updateVersion.compareTo( AppMIDlet.getMIDletVersion() ) < 0 ) {
				updateRequired = false;
				updateVersion = null;
			}
		}

		// lê as informações das categorias de notícias
		final short totalCategories = input.readShort();
		final NewsFeederCategory[] categoriesTemp = new NewsFeederCategory[ totalCategories ];
		for ( short i = 0; i < totalCategories; ++i ) {
			categoriesTemp[ i ] = new NewsFeederCategory();
			categoriesTemp[ i ].read( input );
		}
		categories = categoriesTemp;
	}


	/**
	 * Lê os dados de entradas de news feeder a partir dos dados globais de um InputStream. Após a leitura das novas
	 * entradas de news feeder (caso haja alguma), o InputStream segue aberto para leitura de possíveis dados posteriores.
	 * Caso o limite de entradas seja excedido, as entradas mais antigas são removidas até que todas sejam comportadas.
	 * @param input
	 */
	public static final void readGlobalData( DataInputStream input ) throws Exception {
		boolean readingData = true;
		final NewsFeederManager newsFeederManager = new NewsFeederManager();
		newsFeederManager.load();

		do {
			final byte paramId = input.readByte();

			switch ( paramId ) {
				case NewsFeederEntry.PARAM_TOTAL_ENTRIES:
					final byte totalEntries = input.readByte();

					if ( totalEntries > 0 ) {
						// separa as entradas em
						final Hashtable entriesPerCategory = new Hashtable();
						byte entryIndex = 0;
						while ( entryIndex < totalEntries ) {
							// lê uma nova entrada do stream
							final NewsFeederEntry e = new NewsFeederEntry();
							e.parseParams( input );
							++entryIndex;

							// verifica se já foi lida uma entrada da mesma categoria
							final Integer categoryId = new Integer( e.getCategoryId() );
							Vector v = ( Vector ) entriesPerCategory.get( categoryId );
							if ( v == null ) {
								// categoria ainda não lida; cria o vetor de entradas dessa categoria (já que não sabemos
								// de antemão quantas entradas de cada categoria estão sendo baixadas)
								v = new Vector();
								entriesPerCategory.put( categoryId, v );
							}
							// adiciona a entrada a esse vetor
							v.addElement( e );
						}
						
						lastUpdated = System.currentTimeMillis();

						final Enumeration keys = entriesPerCategory.keys();
						while ( keys.hasMoreElements() ) {
							// converte o Vector num array
							final Integer categoryId = ( Integer ) keys.nextElement();
							final Vector v = ( Vector ) entriesPerCategory.get( categoryId );
							final NewsFeederEntry[] newEntries = new NewsFeederEntry[ v.size() ];
							v.copyInto( newEntries );
							
							// obtém o slot correspondente
							final int slot = newsFeederManager.getSlot( categoryId.intValue() );
							final SlotManager slotManager = new SlotManager();
							slotManager.load( slot );

							// TODO dar feedback ao usuário enquanto grava as novas informações no RMS

							// soma as novas entradas às antigas (caso existam)
							final NewsFeederEntry[] temp = new NewsFeederEntry[ Math.min( slotManager.entries.length + newEntries.length, MAX_NEWS_ENTRIES ) ];
							System.arraycopy( newEntries, 0, temp, 0, newEntries.length );
							System.arraycopy( slotManager.entries, 0, temp, newEntries.length, temp.length - newEntries.length );

							// salva as entradas no RMS
							slotManager.entries = temp;
							slotManager.save( slot );
						}
					}
				break;

				case NewsFeederEntry.PARAM_ENTRY_APP_UPDATE:
					updateRequired = input.readBoolean();
					updateVersion = input.readUTF();
					updateEntry = new NewsFeederEntry();
					updateEntry.parseParams( input );
				break;

				case NewsFeederEntry.PARAM_TOTAL_CATEGORIES:
					// TODO criar mecanismo para atualizar somente as categorias necessárias, em vez de sempre recriá-las
					// lê todas as informações de categorias
					final Vector newCategories = new Vector();

					final short totalCategories = input.readShort();
					for ( short i = 0; i < totalCategories; ++i ) {
						final NewsFeederCategory c = new NewsFeederCategory();
						c.parseParams( input );

						// se não encontrar um slot com esse id de categoria, o adiciona ao vetor de categorias a serem adicionadas
						if ( newsFeederManager.getSlot( c.getId() ) < 0 ) {
							newCategories.addElement( c );
						}
					}
					
					if ( newCategories.size() > 0 ) {
						final int previousLength = newsFeederManager.categories.length;
						final NewsFeederCategory[] temp = new NewsFeederCategory[ previousLength + newCategories.size() ];
						System.arraycopy( newsFeederManager.categories, 0, temp, 0, previousLength );

						int i = previousLength;
						for ( Enumeration e = newCategories.elements(); e.hasMoreElements(); ++i ) {
							final NewsFeederCategory c = ( NewsFeederCategory ) e.nextElement();

							// garantidamente o slot da nova categoria deve ser criado
							c.setSlot( addCategorySlot() );
							temp[ i ] = c;
						}
						newsFeederManager.categories = temp;
					}
				break;

				case NewsFeederEntry.PARAM_NEWS_FEEDER_END:
					readingData = false;
				break;

				//#if DEBUG == "true"
//# 					default:
//# 						throw new IllegalArgumentException( "Id inválido em NewsFeederManager.readGlobalData: "+ paramId );
				//#endif
			}
		} while ( readingData );
		
		// salva o mapeamento categoria x slot e outras informações
		newsFeederManager.save();
	}


	/**
	 *
	 */
	private static final int addCategorySlot() {
		RecordStore rs = null;
		try {
			rs = RecordStore.openRecordStore( DATABASE_NAME, false );
			final int newSlotIndex = rs.addRecord( new byte[ 0 ], 0, 0 );

			return newSlotIndex;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
//# 				AppMIDlet.log( e , "43");
			//#endif

			return -1;
		} finally {
			if ( rs != null ) {
				try {
					rs.closeRecordStore();
				} catch ( Exception e ) {}
			}
		}
	}


	/**
	 *
	 * @param output
	 * @throws java.lang.Exception
	 */
	public static final void writeGlobalData( DataOutputStream output ) throws Exception {
		output.writeByte( ID_NEWS_FEEDER_DATA );

		// informa a versão do news feeder, para evitar que o servidor retorne informações num formato
		// mais novo (e possivelmente não suportado)
		output.writeByte( NewsFeederEntry.PARAM_NEWS_FEEDER_VERSION );
		output.writeUTF( NEWS_FEEDER_VERSION );

		// data da última atualização
		output.writeByte( NewsFeederEntry.PARAM_LAST_UPDATE_TIME );
		output.writeLong( lastUpdated );

		if ( updateVersion != null ) {
			output.writeByte( NewsFeederEntry.PARAM_LAST_APP_VERSION_UPDATE );
			output.writeUTF( updateVersion );
		}

		// informa ao servidor as categorias já gravadas no aparelho
//		final NewsFeederManager m = new NewsFeederManager();
//		m.load();
//		if ( m.categories.length > 0 ) {
//			output.writeByte( NewsFeederEntry.PARAM_TOTAL_CATEGORIES );
//			for ( short i = 0; i < m.categories.length; ++i )
//				output.writeInt( m.categories[ i ].getId() );
//		}

		output.writeByte( NewsFeederEntry.PARAM_NEWS_FEEDER_END );
	}


	public static final void save( NewsFeederEntry[] entries ) {
		if ( entries.length > 0 ) {
			final NewsFeederManager manager = new NewsFeederManager();
			manager.load();

			final int slot = manager.getSlot( entries[ 0 ].getCategoryId() );
			if ( slot > 0 ) {
				final SlotManager slotManager = new SlotManager();
				slotManager.entries = entries;
				try {
					slotManager.save( slot );
				} catch ( Exception ex ) {
					//#if DEBUG == "true"
	//# 					AppMIDlet.log( ex , "44");
					//#endif
				}
			}
		}
	}


	private static final class SlotManager implements Serializable {

		private NewsFeederEntry[] entries = new NewsFeederEntry[ 0 ];

		
		public final void write( DataOutputStream output ) throws Exception {
			// armazena a quantidade de entradas e as informações de cada entrada
			output.writeByte( entries.length );
			for ( byte i = 0; i < entries.length; ++i ) {
				entries[i].write( output );
			}
		}


		public final void read( DataInputStream input ) throws Exception {
			final byte totalEntries = input.readByte();
			final NewsFeederEntry[] tempEntries = new NewsFeederEntry[ totalEntries ];
			for ( byte i = 0; i < totalEntries; ++i ) {
				tempEntries[i] = new NewsFeederEntry();
				tempEntries[i].read( input );
			}
			entries = tempEntries;
		}


		public final void load( final int slot ) {
			try {
				AppMIDlet.loadData( DATABASE_NAME, slot, this );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
	//# 				AppMIDlet.log( e , "45");
				//#endif
			}
		}


		public final void save( final int slot ) throws Exception {
			try {
				AppMIDlet.saveData( DATABASE_NAME, slot, this );
			} catch ( Exception e ) {
				//#if DEBUG == "true"
	//# 				AppMIDlet.log( e ,"46");
				//#endif
			}
		}
		
	}
	
}
