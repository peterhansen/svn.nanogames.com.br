/**
 * ProfileSelector.java
 * 
 * Created on 28/Jan/2009, 21:49:27
 *
 */

package br.com.nanogames.components.online;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.MarqueeLabel;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.userInterface.form.Button;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.userInterface.form.DrawableComponent;
import br.com.nanogames.components.userInterface.form.Form;
import br.com.nanogames.components.userInterface.form.FormLabel;
import br.com.nanogames.components.userInterface.form.FormText;
import br.com.nanogames.components.userInterface.form.borders.DrawableBorder;
import br.com.nanogames.components.userInterface.form.borders.LineBorder;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import br.com.nanogames.components.util.Rectangle;
import javax.microedition.lcdui.Graphics;

/**
 *
 * @author Peter
 */
public final class ProfileSelector extends NanoOnlineContainer {

	/***/
	private static final byte ENTRY_FIRST_PROFILE		= 0;
	/***/
	private static final byte ENTRY_BUTTON_NEW			= ENTRY_FIRST_PROFILE + MAX_PROFILES + 1;
	/***/
	private static final byte ENTRY_BUTTON_USE_EXISTING	= ENTRY_BUTTON_NEW + 1;
	/***/
	private static final byte ENTRY_BUTTON_BACK			= ENTRY_BUTTON_USE_EXISTING + 1;
	private static final byte ENTRY_CONTAINER			= ENTRY_BUTTON_BACK + 1;
	/***/
	private static final byte TOTAL_ITEMS = ENTRY_BUTTON_BACK + 7;

	private static final int COLOR_FILL = 0xbff7fd;
	private static final int COLOR_DIVIDER = 0x818181;

	/***/
	private static final byte PROFILES_BOX_OFFSET_X = 3;

	private static final byte PROFILES_BOX_FILL_HEIGHT = 36;

	private static final byte PROFILES_ICON_SPACING = 5;

	private static final byte CURSOR_BORDER_THICKNESS = 4;

	/** Caixa de seleção dos perfis. */
	private final Container profilesBox;
	private final Container profilesBoxComponent;

	private final Container profilesContainer;

	private final DrawableImage borderLeft;
	private final DrawableImage borderRight;
	private final Pattern borderTop;
	private final Pattern borderBottom;
	private final Pattern divider;
	private final Pattern fill;

	private final byte ARROW_WIDTH;

	/** Label que mostra o nome do usuário atual. */
	private final MarqueeLabel nicknameLabel;

	private final DrawableImage cursor;

	private static final short TIME_CURSOR_COLOR_CHANGE = 666;

	private final MUV cursorColorMUV = new MUV( 255 * 1000 / TIME_CURSOR_COLOR_CHANGE );

	private final Pattern profileBorder;


	public ProfileSelector( int backIndex ) throws Exception {
		super( TOTAL_ITEMS );
		
		setBackIndex( backIndex >= 0 ? backIndex : NanoOnline.isSubmittingNewRecord() ? SCREEN_NEW_RECORD : SCREEN_MAIN_MENU );
		
		final ImageFont fontWhite = NanoOnline.getFont( FONT_DEFAULT );
		final ImageFont fontBlack = NanoOnline.getFont( FONT_BLACK );

		// a quantidade de itens na caixa refere-se ao contâiner de perfis, mais as setas de scroll e o
		// nome do perfil atual selecionado
		profilesBox = new Container( 10 );
		profilesBox.setId( ENTRY_CONTAINER );
		profilesBox.addEventListener( this );
		profilesBoxComponent = new Container( 1 );
		profilesBoxComponent.insertDrawable( profilesBox );
		insertDrawable( profilesBoxComponent );

		borderRight = new DrawableImage( PATH_NANO_ONLINE_IMAGES + "pr.png" );
		borderLeft = new DrawableImage( borderRight );
		borderLeft.mirror( TRANS_MIRROR_H );
		borderTop = new Pattern( new DrawableImage( PATH_NANO_ONLINE_IMAGES + "pt.png" ) );
		borderBottom = new Pattern( borderTop.getFill() );

		ARROW_WIDTH = ( byte ) borderLeft.getWidth();

		fill = new Pattern( COLOR_FILL );
		fill.setPosition( borderLeft.getWidth(), borderTop.getHeight() );
		profilesBox.insertDrawable( fill );

		// imagem padrão do perfil de cada usuário
		final DrawableImage img = new DrawableImage( PATH_NANO_ONLINE_IMAGES + "perfil.png" );
		cursor = new DrawableImage( PATH_NANO_ONLINE_IMAGES + "c.png" );
		cursor.setVisible( false );
		final LineBorder dummyBorder = new LineBorder();
		dummyBorder.set( CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS );
//		cursor.set( CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS );

		final FlowLayout flowLayout = new FlowLayout( FlowLayout.AXIS_HORIZONTAL );
		flowLayout.gap.set( PROFILES_ICON_SPACING, 0 );
		flowLayout.start.set( PROFILES_ICON_SPACING, ( PROFILES_BOX_FILL_HEIGHT - cursor.getHeight() ) >> 1 );
		profilesContainer = new Container( MAX_PROFILES + 4, flowLayout );
		profilesContainer.setScrollableX( true );
		profilesBox.insertDrawable( profilesContainer );
		
		profilesBox.insertDrawable( borderLeft );
		profilesBox.insertDrawable( borderRight );

		profilesBox.setSize( 0, borderLeft.getHeight() );

		borderTop.setPosition( borderLeft.getWidth(), 0 );
		borderBottom.setTransform( TRANS_MIRROR_V );
		borderBottom.setPosition( borderTop.getPosX(), borderLeft.getHeight() - borderBottom.getHeight() );

		divider = new Pattern( COLOR_DIVIDER );
		divider.setSize( 0, 1 );
		divider.setPosition( borderLeft.getWidth(), fill.getPosY() + PROFILES_BOX_FILL_HEIGHT );
		profilesBox.insertDrawable( divider );

		profilesBox.insertDrawable( borderTop );
		profilesBox.insertDrawable( borderBottom );

		nicknameLabel = new MarqueeLabel( fontBlack, null );
		nicknameLabel.setScrollMode( MarqueeLabel.SCROLL_MODE_LEFT_RIGHT );
		nicknameLabel.setScrollFrequency( MarqueeLabel.SCROLL_FREQ_IF_BIGGER );
		nicknameLabel.setPosition( divider.getPosX(), divider.getPosY() + divider.getHeight() );
		profilesBox.insertDrawable( nicknameLabel );

		// insere os perfis já cadastrados no aparelho
		Pattern temp = null;
		final Customer[] customers = NanoOnline.getCustomers();
		if ( customers.length <= 0 ) {
			nicknameLabel.setText( NanoOnline.getText( TEXT_NO_PROFILES_FOUND ), false );
		} else {
			for ( byte i = 0; i < customers.length; ++i ) {
				final DrawableComponent c = new DrawableComponent( img );
				c.addEventListener( this );
				c.setId( ENTRY_FIRST_PROFILE + i );
				c.addEventListener( this );

				if ( customers[ i ].getId() == NanoOnline.getCurrentCustomer().getId() ) {
					final Pattern p = new Pattern( 0x00ff00 ) {

						protected final void paint( Graphics g ) {
							final Rectangle r = getClip();
							r.x += CURSOR_BORDER_THICKNESS >> 1;
							r.y += CURSOR_BORDER_THICKNESS >> 1;
							r.width -= CURSOR_BORDER_THICKNESS;
							r.height -= CURSOR_BORDER_THICKNESS;
							g.setClip( r.x, r.y, r.width, r.height );

							super.paint( g );
						}
					};

					temp = p;
					final DrawableBorder b = new DrawableBorder( p );
					b.set( CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS, CURSOR_BORDER_THICKNESS );
					c.setBorder( b );
				} else {
					c.setBorder( dummyBorder.getCopy() );
					c.getBorder().setVisible( false );
				}

				profilesContainer.insertDrawable( c );
			}
			
			profilesContainer.insertDrawable( cursor );
		}
		profileBorder = temp;

		final Customer currentCustomer = NanoOnline.getCurrentCustomer();
		final FormText currentCustomerLabel = new FormText( fontBlack );
		currentCustomerLabel.setText( NanoOnline.getText( TEXT_CURRENT_PROFILE ) + ": " +
				( currentCustomer.getId() == Customer.ID_NONE ? NanoOnline.getText( TEXT_NONE ) : currentCustomer.getNickname() ) );
		insertDrawable( currentCustomerLabel );
		
		// insere um espaçamento
		final Container dummy = new Container( 0 );
		dummy.setFocusable( false );
		dummy.setSize( 0, fontWhite.getHeight() );
		insertDrawable( dummy );
		
		// insere os botões

		final Button buttonNew = NanoOnline.getButton( TEXT_CREATE_PROFILE, ENTRY_BUTTON_NEW, this );
		insertDrawable( buttonNew );
		
		final Button buttonUse = NanoOnline.getButton( TEXT_IMPORT_PROFILE, ENTRY_BUTTON_USE_EXISTING, this );
		insertDrawable( buttonUse );
		
		final Button buttonBack = NanoOnline.getButton( TEXT_BACK, ENTRY_BUTTON_BACK, this );
		insertDrawable( buttonBack );

		// melhora a navegação entre os perfis
		if ( customers.length > 1 ) {
			profilesContainer.getComponentAt( 0 ).setNextFocusLeft( profilesContainer.getComponentAt( customers.length - 1 ) );
			profilesContainer.getComponentAt( customers.length - 1 ).setNextFocusRight( profilesContainer.getComponentAt( 0 ) );

			final short total = profilesContainer.getComponentCount();
			for ( byte i = 0; i < total; ++i ) {
				profilesContainer.getComponentAt( i ).setNextFocusDown( buttonNew );
				profilesContainer.getComponentAt( i ).setNextFocusUp( buttonBack );
			}
		}
	}


	public final void update( int delta ) {
		super.update( delta );

		if ( profileBorder != null ) {
			int c = ( profileBorder.getFillColor() & 0x0000ff ) + cursorColorMUV.updateInt( delta );

			if ( c < 0 || c > 0xff ) {
				c = NanoMath.clamp( c, 0, 0xff );
				cursorColorMUV.setSpeed( -cursorColorMUV.getSpeed() );
			}
			profileBorder.setFillColor( c << 16 | 0x00ff00 | c );
		}
	}
	

	public final void eventPerformed( Event evt ) {
		final int sourceId = evt.source.getId();
		
		switch ( evt.eventType ) {
			case Event.EVT_BUTTON_CONFIRMED:
				switch ( sourceId ) {
					case ProgressBar.ID_SOFT_RIGHT:
					case ENTRY_BUTTON_BACK:
						onBack();
					break;
					
					case ENTRY_BUTTON_USE_EXISTING:
						NanoOnline.setScreen( SCREEN_LOAD_PROFILE, getId() );
					break;
					
					case ENTRY_BUTTON_NEW:
						NanoOnline.setScreen( SCREEN_REGISTER_PROFILE, getId() );
					break;
				}
			break;
			
			case Event.EVT_KEY_PRESSED:
				final int key = ( ( Integer ) evt.data ).intValue();
				
				switch ( key ) {
					case ScreenManager.KEY_SOFT_RIGHT:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_BACK:
						onBack();
					break;

					case ScreenManager.KEY_NUM5:
					case ScreenManager.FIRE:
						// selecionou um dos perfis
						ProfileScreen.setProfileIndex( sourceId - ENTRY_FIRST_PROFILE );
						NanoOnline.setScreen( SCREEN_PROFILE_VIEW );
					break;
				} // fim switch ( key )
			break;

			//#if TOUCH == "true"
				case Event.EVT_POINTER_PRESSED:
					final Form f = getComponentForm();
					switch ( sourceId ) {
						case ENTRY_CONTAINER:
							final Point pos = ( Point ) evt.data;

							// repassa o evento ao Form para o tratamento de foco
							if ( pos.x <= ARROW_WIDTH ) {
								f.keyPressed( ScreenManager.LEFT );
							} else if ( pos.x >= profilesBox.getWidth() - ARROW_WIDTH ) {
								f.keyPressed( ScreenManager.RIGHT );
							}
						break;

						default:
							// mostra as informações do perfil selecionado
							if ( sourceId >= 0 && sourceId < MAX_PROFILES )
								f.keyPressed( ScreenManager.FIRE );
					}
				break;
			//#endif

			case Event.EVT_FOCUS_GAINED:
			case Event.EVT_FOCUS_LOST:
				final Customer[] customers = NanoOnline.getCustomers();
				final byte CUSTOMER_INDEX = ( byte ) ( sourceId - ENTRY_FIRST_PROFILE );
				cursor.setVisible( false );

				if ( customers != null && CUSTOMER_INDEX < customers.length ) {
					if ( evt.eventType == Event.EVT_FOCUS_GAINED ) {
						nicknameLabel.setText( customers[ CUSTOMER_INDEX ].getNickname(), false );
						nicknameLabel.setTextOffset( nicknameLabel.getWidth() - nicknameLabel.getFont().getTextWidth( nicknameLabel.getText() ) >> 1 );
						cursor.setVisible( true );
						cursor.setPosition( profilesContainer.getComponentAt( CUSTOMER_INDEX ).getPosition() );
					}
				}
			break;
		} // fim switch ( evt.eventType )
	} // fim do método eventPerformed( Event )


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		profilesBox.setSize( width - getScrollBarV().getWidth() - ( PROFILES_BOX_OFFSET_X << 1 ), profilesBox.getHeight() );
		borderRight.setPosition( profilesBox.getWidth() - borderRight.getWidth(), 0 );
		borderTop.setSize( borderRight.getPosX() - borderTop.getPosX(), borderTop.getHeight() );
		borderBottom.setSize( borderTop.getWidth(), borderBottom.getHeight() );
		fill.setSize( borderTop.getWidth(), PROFILES_BOX_FILL_HEIGHT );
		divider.setSize( borderTop.getWidth(), divider.getHeight() );
		nicknameLabel.setSize( borderTop.getWidth(), nicknameLabel.getHeight() );
		nicknameLabel.setTextOffset( nicknameLabel.getWidth() - nicknameLabel.getFont().getTextWidth( nicknameLabel.getText() ) >> 1 );

		profilesContainer.setPosition( fill.getPosition() );
		profilesContainer.setSize( borderTop.getWidth(), fill.getHeight() );

		profilesBoxComponent.setSize( profilesBox.getSize() );
		profilesBoxComponent.refreshLayout();
		profilesBoxComponent.setPosition( PROFILES_BOX_OFFSET_X, profilesBoxComponent.getPosY() );
	}

}
