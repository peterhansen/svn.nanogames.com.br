//#if AD_MANAGER == "true"
/**
 * GenericResource.java
 *
 * Created on 6:56:28 PM.
 *
 */
package br.com.nanogames.components.online.ad;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.util.DynamicByteArray;
import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import javax.microedition.lcdui.Image;

// TODO gerenciador de recursos está associado aos anúncios - para usá-los em separado, devemos rever o uso da ability AD_MANAGER


/**
 *
 * @author Peter
 */
public final class Resource implements ResourceTypes, Serializable {

	/** Número de usos do recurso. */
	private short uses;

	/** Id do recurso (segundo informado pelo servidor). */
	private int id = -1;

	/** Tipo do recurso. */
	private short type;

	/** Dados do recurso. */
	private byte[] data;

	/** Identificador em relação ao recurso pai. */
	private short internalId = -1;

	/** Ids dos filhos (caso tenha). */
	private int[] childrenIds = new int[ 0 ];


	public Resource() {
	}


	public Resource( final byte[] data ) throws Exception {
		ByteArrayInputStream byteInput = null;
		DataInputStream dataInput = null;

		try {
			byteInput = new ByteArrayInputStream( data );
			dataInput = new DataInputStream( byteInput );
			read( dataInput );
		} finally {
			if ( byteInput != null ) {
				try {
					byteInput.close();
				} catch ( Exception e ) {}
			}
			if ( dataInput != null ) {
				try {
					dataInput.close();
				} catch ( Exception e ) {}
			}
		}
	}


	/**
	 * Decrementa o contador de usos do recurso.
	 *
	 * @return boolean indicando se o recurso ainda está em uso, ou seja, se <code>uses</code> é maior que zero.
	 */
	public final boolean decrementUse() {
		return --uses > 0;
	}


	/**
	 * Incrementa o número de usos do recurso.
	 */
	public final void incrementUse() {
		++uses;
	}


	/**
	 * Obtém o id do recurso (conforme informado pelo servidor).
	 */
	public final int getId() {
		return id;
	}


	/**
	 * Define o id do recurso, conforme informado pelo servidor.
	 */
	protected final void setId( int id ) {
		this.id = id;
	}


	/**
	 * Obtém o tipo do recurso. Os valores válidos estão definidos na interface <code>ResourceTypes</code>.
	 * @see ResourceTypes
	 */
	public final short getType() {
		return type;
	}


	protected final void setType( int type ) {
		this.type = ( short ) type;
	}


	public final short getUses() {
		return uses;
	}


	protected final void setUses( short uses ) {
		this.uses = uses;
	}


	public final int[] getChildrenIds() {
		return childrenIds;
	}


	private final void setChildrenIds( int[] childrenIds ) {
		this.childrenIds = childrenIds;
	}


	public final short getInternalId() {
		return internalId;
	}


	public final void setInternalId( short internalId ) {
		this.internalId = internalId;
	}


	/**
	 * 
	 * @return
	 */
	public final boolean isFile() {
		return getType() >= FILE_TYPE_NONE;
	}


	public final void write( DataOutputStream output ) throws Exception {
		//#if DEBUG == "true"
//# 			System.out.println( "Writing resource to RMS:" );
//# 			System.out.println( "\tuses: " + getUses() );
//# 			System.out.println( "\tid:" + getId() );
//# 			System.out.println( "\ttype: " + getType() );
//# 			System.out.print( "\tchildren: " );
//# 			for ( int i = 0; i < childrenIds.length; ++i )
//# 				System.out.print( childrenIds[ i ] + ", " );
//# 			System.out.println();
//# 			System.out.println( "\tdata saved: " + ( data != null ) );
//# 			System.out.println( "\tdata: " + data );
		//#endif
		output.writeShort( getUses() );
		output.writeInt( getId() );
		output.writeShort( getType() );
		output.writeShort( getInternalId() );

		output.writeShort( childrenIds.length );
		for ( int i = 0; i < childrenIds.length; ++i ) {
			output.writeInt( childrenIds[ i ] );
		}

		output.writeBoolean( data != null );
		if ( data != null )
			output.write( data );
	}


	public final void read( DataInputStream input ) throws Exception {
		setUses( input.readShort() );
		setId( input.readInt() );
		setType( input.readShort() );
		setInternalId( input.readShort() );

		childrenIds = new int[ input.readShort() ];
		for ( int i = 0; i < childrenIds.length; ++i ) {
			childrenIds[ i ] = input.readInt();
		}

		final boolean dataSaved = input.readBoolean();
		if ( dataSaved ) {
			final DynamicByteArray dynArray = new DynamicByteArray();
			dynArray.readInputStream( input );
			data = dynArray.getData();
		}

		//#if DEBUG == "true"
//# 			System.out.println( "Read resource from RMS:" );
//# 			System.out.println( "\tuses: " + getUses() );
//# 			System.out.println( "\tid:" + getId() );
//# 			System.out.println( "\ttype: " + getType() );
//# 			System.out.print( "\tchildren: " );
//# 			for ( int i = 0; i < childrenIds.length; ++i )
//# 				System.out.print( childrenIds[ i ] + ", " );
//# 			System.out.println();
//# 			System.out.println( "\tdata saved: " + dataSaved );
//# 			System.out.println( "\tdata: " + data );
		//#endif
	}


	protected final void parseParams( DataInputStream input ) throws Exception {
		byte paramId;

		boolean readingData = true;
		
		do {
			paramId = input.readByte();
			//#if DEBUG == "true"
//# 				System.out.println( "LEU ID DE RECURSO: " + paramId );
			//#endif

			switch ( paramId ) {
				case ResourceManager.PARAM_RESOURCE_TYPE:
				case ResourceManager.PARAM_FILE_TYPE:
					setType( input.readShort() );
				break;

				case ResourceManager.PARAM_RESOURCE_ID:
				case ResourceManager.PARAM_FILE_ID:
					setId( input.readInt() );
				break;

				case ResourceManager.PARAM_RESOURCE_INTERNAL_ID:
					setInternalId( input.readShort() );
				break;

				case ResourceManager.PARAM_RESOURCE_DATA_SIZE:
					final int dataSize = input.readInt();
					//#if DEBUG == "true"
//# 						System.out.println( "TAMANHO DOS DADOS A LER: " + dataSize );
					//#endif
					final DynamicByteArray dynArray = new DynamicByteArray();
					dynArray.readInputStream( input, dataSize );
					data = dynArray.getData();

					//#if DEBUG == "true"
//# 						System.out.println( "DADOS DO RECURSO: " + data );
					//#endif
				break;

				case ResourceManager.PARAM_N_CHILDREN:
					// lê os ids dos recursos-filho
					final short totalChildren = input.readShort();
					final int[] newChildrenIds = new int[ totalChildren ];

					for ( short i = 0; i < totalChildren; ++i ) {
						newChildrenIds[ i ] = input.readInt();
					}

					setChildrenIds( newChildrenIds );
					//#if DEBUG == "true"
//# 						System.out.print( "FILHOS: " );
//# 						for ( int i = 0; i < newChildrenIds.length; ++i ) {
//# 							System.out.print( newChildrenIds[ i ] + ", " );
//# 						}
					//#endif
				break;

				case ResourceManager.PARAM_RESOURCE_END:
				case ResourceManager.PARAM_FILE_END:
					readingData = false;
				break;

				default:
					//#if DEBUG == "true"
//# 						throw new IllegalArgumentException( "paramId inválido: " + paramId );
					//#else
						throw new IllegalArgumentException();
					//#endif
			}
		} while ( readingData );
	}


	/**
	 * 
	 * @return
	 * @throws Exception
	 */
	public Object getData() throws Exception {
		final ByteArrayInputStream input = ( data == null ) ? null : new ByteArrayInputStream( data );
		final Resource[] children = new Resource[ childrenIds.length ];
		for ( short i = 0; i < children.length; ++i ) {
			children[ i ] = ResourceManager.getFile( childrenIds[ i ] );
		}

		try {
			switch ( type ) {
//				case RESOURCE_TYPE_SPRITE_J2ME:
//				break;
//
//				case RESOURCE_TYPE_SOUND:
//				break;
//
//				case RESOURCE_TYPE_TEXT_SIMPLE:
//				break;
//
//				case RESOURCE_TYPE_IMAGE_FONT:
//				break;
//
//				case RESOURCE_TYPE_VIDEO:
//				break;
//
//				case RESOURCE_TYPE_TEXT_PACK:
//				break;

				case RESOURCE_TYPE_IMAGE:
					//#if DEBUG == "true"
//# 						System.out.println( "LOADING IMAGE: " + children[ 0 ] );
					//#endif
					return new DrawableImage( children[ 0 ] );

				// arquivos

				case FILE_TYPE_IMAGE_PNG:
				case FILE_TYPE_IMAGE_BMP:
				case FILE_TYPE_IMAGE_JPEG:
				case FILE_TYPE_IMAGE_GIF:
					return new DrawableImage( input );

//				case FILE_TYPE_SPRITE_DESCRIPTOR_J2ME:
//				break;
//
//				case FILE_TYPE_SOUND_MIDI:
//				case FILE_TYPE_SOUND_MP3:
//				case FILE_TYPE_SOUND_AAC:
//				case FILE_TYPE_SOUND_WAV:
//				break;

				default:
					return null;
			}
		} finally {
			if ( input != null ) {
				try {
					input.close();
				} catch ( Exception e ) {}
			}
		}
	}


}
//#endif