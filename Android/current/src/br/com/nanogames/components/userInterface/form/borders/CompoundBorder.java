/**
 * CompoundBorder.java
 * 
 * Created on 8/Dez/2008, 20:24:43
 *
 */

package br.com.nanogames.components.userInterface.form.borders;

/**
 *
 * @author Peter
 */
public class CompoundBorder extends Border {
	
	/***/
	public static final byte MAX_LEVELS_DEFAULT = 5;
	
	
	public CompoundBorder() throws Exception {
		this( MAX_LEVELS_DEFAULT );
	}
	
	
	public CompoundBorder( int maxLevels ) throws Exception {
		super( maxLevels );
	}


	public Border getCopy() throws Exception {
		return new CompoundBorder();
	}


	
}
