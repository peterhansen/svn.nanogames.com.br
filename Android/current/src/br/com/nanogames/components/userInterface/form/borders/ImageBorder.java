/**
 * Border.java
 * 
 * Created on Mar 2, 2009, 2:33:55 PM
 *
 */

package br.com.nanogames.components.userInterface.form.borders;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;

/**
 *
 * @author Peter
 */
public final class ImageBorder extends Border {

	private static final int COLOR_FILL = 0xe1c27e;
	private static final int COLOR_FILL_FOCUSED = 0xefdeb3;
	private static final int COLOR_FILL_PRESSED = 0xd1a553;

	private final int colorUnfocused;
	private final int colorFocused;
	private final int colorPressed;

	private final Pattern patternLeft;
	private final Pattern patternRight;
	private final Pattern patternTop;
	private final Pattern patternBottom;

	private final Pattern fill;

	private final DrawableImage topLeft;
	private final DrawableImage topRight;
	private final DrawableImage bottomLeft;
	private final DrawableImage bottomRight;

	private final String path;


	public ImageBorder( String path ) throws Exception {
		this( path, COLOR_FILL, COLOR_FILL_FOCUSED, COLOR_FILL_PRESSED );
	}


	public ImageBorder( String path, int colorUnfocused, int colorFocused, int colorPressed ) throws Exception {
		super( 9 );

		this.path = path;
		this.colorFocused = colorFocused;
		this.colorUnfocused = colorUnfocused;
		this.colorPressed = colorPressed;

		patternLeft = new Pattern( new DrawableImage( path + "l.png" ) );
		patternLeft.setSize( patternLeft.getFill().getWidth(), 0 );
		insertDrawable( patternLeft );

		patternTop = new Pattern( new DrawableImage( path + "t.png" ) );
		patternTop.setSize( 0, patternTop.getFill().getHeight() );
		insertDrawable( patternTop );

		patternRight = new Pattern( new DrawableImage( path + "r.png" ) );
		patternBottom = new Pattern( new DrawableImage( path + "b.png" ) );
		patternRight.setSize( patternRight.getFill().getWidth(), 0 );
		insertDrawable( patternRight );
		patternBottom.setSize( 0, patternBottom.getFill().getHeight() );
		insertDrawable( patternBottom );

		fill = new Pattern( colorUnfocused );
		insertDrawable( fill );

		topLeft = new DrawableImage( path + "tl.png" );
		insertDrawable( topLeft );

		bottomLeft = new DrawableImage( path + "bl.png" );
		bottomLeft.defineReferencePixel( ANCHOR_BOTTOM | ANCHOR_LEFT );
		insertDrawable( bottomLeft );
		
		topRight = new DrawableImage( path + "tr.png" );
		topRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_TOP );
		insertDrawable( topRight);

		bottomRight = new DrawableImage( path + "br.png" );
		bottomRight.defineReferencePixel( ANCHOR_RIGHT | ANCHOR_BOTTOM );
		insertDrawable( bottomRight );

		patternTop.setPosition( topLeft.getWidth(), 0 );
		patternLeft.setPosition( 0, topLeft.getHeight());
		patternRight.setPosition( 0, topRight.getHeight() );
		fill.setPosition( topLeft.getSize() );

		setLeft( patternLeft.getWidth() );
		setRight( patternRight.getWidth() );
		setTop( patternTop.getHeight() );
		setBottom( patternBottom.getHeight() );
	}


	public final void setSize( int width, int height ) {
		super.setSize( width, height );

		bottomLeft.setRefPixelPosition( 0, height );
		topRight.setRefPixelPosition( width, 0 );
		bottomRight.setRefPixelPosition( width, height );

		patternTop.setSize( width - patternTop.getPosX() - topRight.getWidth(), patternTop.getHeight() );
		patternBottom.setSize( patternTop.getWidth(), patternBottom.getHeight() );
		patternLeft.setSize( patternLeft.getWidth(), height - patternLeft.getPosY() - bottomLeft.getHeight() );
		patternRight.setSize( patternRight.getWidth(), patternLeft.getHeight() );

		fill.setSize( patternTop.getWidth(), patternLeft.getHeight() );

		patternBottom.setPosition( fill.getPosX(), bottomLeft.getPosY() );
		patternRight.setPosition( topRight.getPosX(), topRight.getHeight() );
	}


	public final br.com.nanogames.components.userInterface.form.borders.Border getCopy() throws Exception {
		return new ImageBorder( path );
	}


	public final void setState( int state ) {
		super.setState( state );

		if ( fill != null)  {
			switch ( state ) {
				case STATE_FOCUSED:
					fill.setFillColor( colorFocused );
				break;

				case STATE_PRESSED:
					fill.setFillColor( colorPressed );
				break;

				case STATE_ERROR:
				case STATE_UNFOCUSED:
				case STATE_WARNING:
					fill.setFillColor( colorUnfocused );
				break;
			}
		}
	}


	public final Pattern getFill() {
		return fill;
	}

}
