/*
 * Menu.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components.userInterface;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.util.Point;

//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//#endif


/**
 *
 * @author peter
 */
public class Menu extends UpdatableGroup implements KeyListener
	//#if TOUCH == "true"
	, PointerListener
	//#endif

{
 
	/** índice atualmente selecionado no menu */
	protected int currentIndex;
	 
	/** indica se o menu é circular */
	protected boolean circular;
	 
	/** listener do menu. Esse listener receberá eventos cada vez que houver mudança no item atualmente selecionado ou
	 * quando o usuário confirmar uma seleção no menu.
	 */
	protected MenuListener listener;
	 
	/** id do menu. Esse valor é recebido como parâmetro pelo construtor, e passado ao listener (caso exista) para que 
	 * este determine qual o menu o está chamando, no caso do listener estar escutando vários menus simultaneamente.
	 */
	protected final int menuId;
	 
	/** drawable utilizado como cursor */
	protected Drawable cursor;
	
	/** caso o cursor seja atualizável, mantém uma segunda referência para ele, de forma a evitar chamadas desnecessárias
	 * de instanceof e typecasts durante a atualização do menu
	 */
	protected Updatable updatableCursor;
	
	
	// ordens de desenho disponíveis para o cursor
	public static final byte CURSOR_DRAW_AFTER_MENU		= 0;
	public static final byte CURSOR_DRAW_BEFORE_MENU	= 1;
	
	protected byte cursorDrawOrder;

	/** região do item atualmente selecionado utilizado como referência para o cursor se posicionar. Os valores válidos são 
	 * combinações formadas pelo operador binário OU ('|') de um tipo de alinhamento horizontal e outro vertical, 
	 * definidos na classe Drawable: ANCHOR_LEFT, ANCHOR_HCENTER, ANCHOR_RIGHT e ANCHOR_TOP, ANCHOR_VCENTER, ANCHOR_BOTTOM.
	 * @see #ANCHOR_TOP
	 * @see #ANCHOR_LEFT
	 * @see #ANCHOR_BOTTOM
	 * @see #ANCHOR_RIGHT
	 * @see #ANCHOR_VCENTER
	 * @see #ANCHOR_HCENTER
	 */
	protected int cursorAlignment;
	
	
	/**
	 * 
	 * @param listener 
	 * @param id 
	 * @param entries 
	 * @throws java.lang.Exception 
	 */
	public Menu( MenuListener listener, int id, int entries ) throws Exception {
		super( entries );
	
		this.listener = listener;
		this.menuId = id;
	} // fim do construtor Menu( MenuListener, int, int )	
	 
	
	public void nextIndex() {
		if ( currentIndex < activeDrawables - 1 )
			setCurrentIndex( currentIndex + 1 );
		else if ( circular )
			setCurrentIndex( 0 );
	}
	
	 
	public void previousIndex() {
		if ( currentIndex > 0 )
			setCurrentIndex( currentIndex - 1 );
		else if ( circular )
			setCurrentIndex( activeDrawables - 1 );
	}
	 
	
	public int getCurrentIndex() {
		return currentIndex;
	}
	 
	
	public void setCurrentIndex( int index ) {
		if ( index >= 0 && index < activeDrawables ) {
			currentIndex = index;
			
			if ( listener != null )
				listener.onItemChanged( this, menuId, index );
			
			updateCursorPosition();
		} // fim if ( index != currentIndex && index >= 0 && index < activeDrawables )
	} // fim do método setCurrentIndex( int )
	 
	
	public boolean isCircular() {
		return circular;
	}
	 
	
	public void setCircular( boolean circular ) {
		this.circular = circular;
	}
	 
	
	/**
	 * Define qual é o drawable do grupo utilizado como cursor.
	 * 
	 * @param cursor drawable a ser utilizado como cursor.
	 * @param drawOrder ordem de desenho do cursor em relação ao menu. Valores válidos:
	 * <ul>
	 * <li>CURSOR_DRAW_AFTER_MENU: cursor é desenhado após o menu.</li>
	 * <li>CURSOR_DRAW_BEFORE_MENU: cursor é desenhado antes do menu.</li>
	 * </ul>
	 * @param alignment indica que área da opção selecionada deve ser tomada como referência para posicionamento do cursor.
	 * A posição final do cursor depende dessa área e do pixel de referência definido no cursor. Os valores válidos são 
	 * combinações formadas pelo operador binário OU ('|') de um tipo de alinhamento horizontal e outro vertical, 
	 * definidos na classe Drawable:
	 * <ul>
	 * <li>ANCHOR_LEFT: alinhado à esquerda.</li>
	 * <li>ANCHOR_HCENTER: centralizado horizontalmente.</li>
	 * <li>ANCHOR_RIGHT: horizontal à direita.</li>
	 * <br></br>
	 * <li>ANCHOR_TOP: alinhamento vertical no topo da linha.</li>
	 * <li>ANCHOR_VCENTER: centralizado verticalmente.</li>
	 * <li>ANCHOR_BOTTOM:	alinhamento vertical na parte inferior da linha.</li>
	 * </ul>
	 * 
	 * @see br.com.nanogames.components.Drawable#ANCHOR_LEFT
	 * @see br.com.nanogames.components.Drawable#ANCHOR_HCENTER
	 * @see br.com.nanogames.components.Drawable#ANCHOR_RIGHT
	 * @see br.com.nanogames.components.Drawable#ANCHOR_TOP
	 * @see br.com.nanogames.components.Drawable#ANCHOR_VCENTER
	 * @see br.com.nanogames.components.Drawable#ANCHOR_BOTTOM
	 */
	public void setCursor( Drawable cursor, byte drawOrder, int alignment ) {
		this.cursor = cursor;
		if ( cursor instanceof Updatable )
			updatableCursor = ( Updatable ) cursor;
		else
			updatableCursor = null;
		
		switch ( drawOrder ) {
			case CURSOR_DRAW_BEFORE_MENU:
			case CURSOR_DRAW_AFTER_MENU:
				cursorDrawOrder = drawOrder;
			break;
			
			default:
				cursorDrawOrder = CURSOR_DRAW_AFTER_MENU;
		} // fim switch ( drawOrder )
		
		cursorDrawOrder = drawOrder;
		cursorAlignment = alignment;
		setCurrentIndex( currentIndex );
	} // fim do método setCursor( Drawable, byte, int )
	
	
	/**
	 * Obtém a referência para o cursor.
	 * @return referência para o cursor.
	 */
	public final Drawable getCursor() {
		return cursor;
	}
	
	
	/**
	 *@see userInterface.KeyListener#keyPressed(int)
	 */
	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
				previousIndex();
			break;
			
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
				nextIndex();
			break;
			
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				if ( listener != null )
					listener.onChoose( this, menuId, currentIndex );
			break;
		} // fim switch ( key )
	} // fim do método keyPressed( int )


	/**
	 *@see userInterface.KeyListener#keyReleased(int)
	 */
	public void keyReleased( int key ) {
	}


	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		super.update( delta );
		
		if ( updatableCursor != null )
			updatableCursor.update( delta );
	}

	
	protected void updateCursorPosition() {
		if ( cursor != null ) {
			final Drawable current = drawables[ currentIndex ];
			final Point refPoint = new Point();

            if ( ( cursorAlignment & ANCHOR_TOP ) != 0 ) {
				refPoint.y = current.getPosY();
			} else if ( ( cursorAlignment & ANCHOR_VCENTER ) != 0 ) {
				refPoint.y = current.getPosY() + ( current.getHeight() >> 1 );
			} else {
				refPoint.y = current.getPosY() + current.getHeight();
			}

            if ( ( cursorAlignment & ANCHOR_LEFT ) != 0 ) {
				refPoint.x = current.getPosX();
			} else if ( ( cursorAlignment & ANCHOR_HCENTER ) != 0) {
				refPoint.x = current.getPosX() + ( current.getWidth() >> 1 );
			} else {
				refPoint.x = current.getPosX() + current.getWidth();
			}

			cursor.setRefPixelPosition( refPoint );
		} // fim if ( cursor != null )
	} // fim do método updateCursorPosition()

	
	protected void paint( Graphics g ) {
		if ( cursor != null && visible ) {
			switch ( cursorDrawOrder ) {
				case CURSOR_DRAW_AFTER_MENU:
					super.paint( g );
					cursor.draw( g );
				return;
				
				case CURSOR_DRAW_BEFORE_MENU:
					cursor.draw( g );
			} // fim switch ( cursorDrawOrder )
		} // fim if ( cursor != null && visible )
		super.paint( g );
	} // fim do método paint( Graphics )


	//#if TOUCH == "true"
		public void onPointerDragged( int x, int y ) {
			final int index = getEntryAt( x, y );

			if ( index >= 0 )
				setCurrentIndex( index );
		}


		public void onPointerPressed( int x, int y ) {
			final int index = getEntryAt( x, y );
			if ( currentIndex == index ) {
				// o usuário confirmou a opção atual
				keyPressed( ScreenManager.FIRE );
			} else {
				// o usuário trocou a opção selecionada
				setCurrentIndex( index );
			}
		} // fim do método onPointerPressed( int, int )


		public void onPointerReleased( int x, int y ) {
		}
	//#endif
	
	
	/**
	 * Obtém o índice da opção selecionada pelo ponteiro.
	 * 
	 * @param x posição x do evento de ponteiro na tela.
	 * @param y posição y do evento de ponteiro na tela.
	 * @return índice da opção selecionada, ou -1 caso a posição não intercepte nenhuma entrada.
	 */
	protected int getEntryAt( int x, int y ) {
		// TODO caso o menu esteja inserido em outra coleção em vez de ser a tela ativa, o cálculo da posição pressionada estará incorreto.
		x -= position.x;
		y -= position.y;
		
		for ( byte i = 0; i < activeDrawables; ++i ) {
			final Drawable entry = drawables[ i ];
			if ( entry.contains( x, y ) )
				return i;
		}
		return -1;
	}

	
}
 
