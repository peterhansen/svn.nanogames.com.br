/**
 * Container.java
 * 
 * Created on 5/Nov/2008, 13:59:22
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.userInterface.form.borders.Border;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.userInterface.form.layouts.Layout;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//#endif

/**
 *
 * @author Peter
 */
public class Container extends Component {
	
	/** Tipo de adição automática da barra de scroll: não adiciona barra de scroll. */
	public static final byte AUTO_SCROLLBAR_NONE			= 0; 
	public static final byte AUTO_SCROLLBAR_HORIZONTAL		= 1;
	public static final byte AUTO_SCROLLBAR_VERTICAL		= 2;
	public static final byte AUTO_SCROLLBAR_BOTH			= AUTO_SCROLLBAR_HORIZONTAL | AUTO_SCROLLBAR_VERTICAL;
	
	/***/
    protected boolean scrollableX;
	
	/***/
    protected boolean scrollableY;	
	
	/** Barra de rolagem horizontal. */
	protected ScrollBar scrollBarH;

	/** Barra de rolagem vertical. */
	protected ScrollBar scrollBarV;

	/** Offset na posição do(s) componente(s) interno(s) devido ao scroll. */
	protected final Point scroll = new Point();

	/** Array que contém referências para todos os componentes, para otimizar a atualização do grupo. */
	protected final Component[] components;
	
	/** Quantidade de componentes no array */
	protected short activeComponents;
	
	protected Layout layout;
	

	/**
	 * Cria um novo contâiner, sem um Layout definido.
	 * @param nComponents
	 */
	public Container( int nComponents ) {
		this( nComponents, null );
	}
	
	
	public Container( int nComponents, Layout layout ) {
		super( nComponents );
		
		// por padrão, os containers não podem ter foco direto (somente seus filhos). Esse tratamento é utilizado
		// devido a classes que derivam de container (como FormText), mas que possuem tratamentos específicos de foco,
		// por exemplo
		focusable = false;
		
		components = new Component[ nComponents ];
		
		setLayout( layout );
	}
	
	
	public void destroy() {
		// remover todos os drawables aqui pode parecer redundante por causa da chamada em Component.destroy, mas é importante
		// pois só aqui é possível chamar os métodos destroy() dos componentes removidos também.
		removeAllDrawables( true );
		
		super.destroy();
		
		if ( scrollBarH != null ) {
			scrollBarH.destroy();
			scrollBarH = null;
		}

		if ( scrollBarV != null ) {
			scrollBarV.destroy();
			scrollBarV = null;
		}

		while ( activeDrawables > 0 ) {
			removeDrawable( 0, true );
		}
	}
	
	
	/**
	 * Insere um componente no próximo slot disponível.
	 * 
	 * @param c componente a ser inserido no grupo.
	 * @return o índice onde o componente foi inserido <b>no array de drawables</b>, ou -1 caso não haja mais slots disponíveis.
	 */
	public short insertDrawable( Component c ) {
		//#if DEBUG == "true"
//# 			if ( c.getParent() != null ) {
//# 				throw new IllegalArgumentException( "Component is already contained in Container: " + c.getParent() );
//# 			}
//# 			if ( c instanceof Form ) {
//# 				throw new IllegalArgumentException( "A form cannot be added to a container" );
//# 			}
		//#endif
		
		final short retValue = super.insertDrawable( c );
		
		// caso o componente tenha sido inserido com sucesso e seja atualizável, o insere também no array de atualizáveis
		if ( retValue >= 0 ) {
			c.setParent( this );
			Form f = getComponentForm();
			if ( f != null ) {
				f.clearFocusVectors();
			}
			
			components[ activeComponents++ ] = c;
			
			setAutoCalcPreferredSize( true );
		}
			
		return retValue;
	} // fim do método insertDrawable( Component )	
	
	
	/**
	 * Insere um componente no próximo slot disponível.
	 * 
	 * @param c componente a ser inserido no grupo.
	 * @param constraints 
	 * @return o índice onde o componente foi inserido <b>no array de drawables</b>, ou -1 caso não haja mais slots disponíveis.
	 */
	public short insertDrawable( Component c, Object constraints ) {
		final short ret = insertDrawable( c );
		
		if ( ret >= 0 && layout != null )
			layout.addLayoutComponent( this, c, constraints );
		
		return ret;
	}
	
	
	public Drawable removeDrawable( Drawable d, boolean destroyComponent ) {
		final Drawable removed = removeDrawable( d );

		// componente fica órfão após sua remoção
		if ( removed != null ) {
			( ( Component ) removed ).setParent( null );
		
			if ( destroyComponent && ( removed instanceof Component ) )
				( ( Component ) removed ).destroy();
		}
		
		return removed;
	}
	
	
    /**
     * Remove um drawable do grupo. Equivalente à chamada de <code>removeDrawable( index, false )</code>, ou seja
	 * mesmo que o drawable removido seja um componente seu método <code>destroy()</code> <b>não</b> é chamado.
     * @param index índice do drawable a ser removido no grupo.
     * @return uma referência para o drawable removido, ou null caso o índice seja inválido.
	 * @see #removeDrawable(int, boolean)
     */
	public Drawable removeDrawable( int index ) {
		return removeDrawable( index, false );
	} // fim do método removeDrawable( int )
	
	
	/**
	 * Remove todos os componentes do container, chamando seu método destroy caso indicado.
	 */
	public final void removeAllDrawables( boolean destroy ) {
		while ( getUsedSlots() > 0 )
			removeDrawable( 0, destroy );
	}
	
	
    /**
     * Remove um drawable do grupo.
     * @param index índice do drawable a ser removido no grupo.
	 * @param destroyComponent caso seja <code>true</code> e o drawable removido seja um Component, chama automaticamente
	 * seu método <code>destroy()</code> (ou seja, o Component já é retornado no estado destruído.
     * @return uma referência para o drawable removido, ou null caso o índice seja inválido.
	 * @see #removeDrawable(int)
     */
	public Drawable removeDrawable( int index, boolean destroyComponent ) {
		final Drawable removed = super.removeDrawable( index );
		
		// caso o drawable removido seja atualizável, o remove também da lista de atualizáveis
		if ( removed instanceof Component ) {
			( ( Component ) removed ).setAutoCalcPreferredSize( true );
			
			// é necessário varrer a lista pois os índices dos drawables não necessariamente são os mesmos índices
			// dos componentes
			for ( short i = 0; i < activeComponents; ++i ) {
				if ( components[ i ] == removed ) {
					final int LIMIT = components.length - 1;
					for ( int j = i; j < LIMIT; ++j )
						components[ j ] = components[ j + 1 ];

					// reduz o contador de componentes
					--activeComponents;
					// anula a referência no array para o último elemento (caso contrário, haveria entradas
					// duplicadas no array, podendo causar futuros vazamentos de memória)
					components[ LIMIT ] = null;
					
					break;
				} // fim if ( components[ i ] == removed )
			} // fim for ( short i = 0; i < activeComponents; ++i )

			if ( removed != null ) {
				( ( Component ) removed ).setParent( null ); // TODO teste

				if ( destroyComponent )
					( ( Component ) removed ).destroy();
			}
			
			final Form form = getComponentForm();
			if ( form != null ) {
				form.clearFocusVectors();
				// TODO passar foco para pai, ou para anterior/próximo?
			}			
		} // fim if ( removed instanceof Component )
		
		setAutoCalcPreferredSize( true );
		
		return removed;		
	}


	public boolean setDrawableIndex( int oldIndex, int newIndex ) {
		final Drawable oldDrawable = drawables[ oldIndex ];
		final Drawable newDrawable = drawables[ newIndex ];
		
		// faz a possível alteração na ordem do array de componentes também, para garantir que o método getComponentAt(int, int)
		// sempre retorne o componente mais à frente no eixo z na posição clicada
		final boolean swapped = super.setDrawableIndex( oldIndex, newIndex );
		if ( swapped ) {
			if ( ( oldDrawable instanceof Component ) && ( newDrawable instanceof Component ) ) {
				short oldComponentIndex = -1;
				short newComponentIndex = -1;
				
				for ( short i = 0; i < activeComponents; ++i ) {
					if ( components[ i ] == oldDrawable ) {
						oldComponentIndex = i;
						if ( newComponentIndex >= 0 )
							break;
					} else if ( components[ i ] == newDrawable ) {
						newComponentIndex = i;
						if ( oldComponentIndex >= 0 )
							break;
					}
				}
				final Component old = components[ oldComponentIndex ];

				if ( oldComponentIndex < newComponentIndex ) {
					for ( int i = oldComponentIndex; i < newComponentIndex; ++i )
						components[ i ] = components[ i + 1 ];
				} else {
					for ( int i = oldComponentIndex; i > newComponentIndex; --i )
						components[ i ] = components[ i - 1 ];
				}

				components[ newComponentIndex ] = old;				
			}
		} // fim if ( swapped )
		return swapped;
	}
	
	
    /**
     * Returns true if the given component is within the hierarchy of this container
     *
     * @param cmp a Component to check
     * @return true if this Component contains in this Container
     */
    public boolean contains( Component cmp ) {
		for ( int i = 0; i < components.length; ++i ) {
			final Component c = components[ i ];
			if ( c == cmp )
				return true;

			if ( c instanceof Container ) {
				if ( ( ( Container ) c ).contains( cmp ) )
					return true;
			}
		}
		return false;
	}
	
	
    /**
     * Returns the Component at a given index
     * 
     * @param index of the Component you wish to get
     * @return a Component
     * @throws ArrayIndexOutOfBoundsException if an invalid index was given.
     */
    public Component getComponentAt( int index ) {
        return components[ index ];
    }	
	
	
    /**
     * Returns the number of components
     * 
     * @return the Component count
     */
    public final short getComponentCount() {
        return activeComponents;
    }	

	
	/**
	 * Obtém o componente presente numa posição x,y do contâiner. Observação: somente Components podem ser retornados
	 * nessa busca, ou seja, caso haja um Drawable na posição x,y recebida que não seja um Component, ele <b>não</b> é retornado.
	 * 
	 * @param x posição x do ponteiro.
	 * @param y posição y do ponteiro.
	 * @return o componente presente na posição x,y (pode ser o próprio contâiner), ou <code>null</code> caso o ponto não
	 * esteja dentro da área do contâiner.
	 */
	public Component getComponentAt( int x, int y ) {
		return getComponentAt( x, y, null );
	}		
	
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param diff
	 * @return
	 */
	protected Component getComponentAt( int x, int y, Point diff ) {
		if ( contains( x, y ) ) {
			x -= position.x;
			y -= position.y;

			// desconta a posição top-left da borda, caso exista (clique pode ter sido dado dentro do contâiner, porém
			// fora dos seus componentes internos)
			if ( border != null ) {
				x -= border.getLeft();
				y -= border.getTop();
			}
			
			if ( scrollBarV != null && scrollBarV.isVisible() && scrollBarV.contains( x, y ) ) {
				updateDiff( diff, false );
				return scrollBarV;
			}			
			
			if ( scrollBarH != null && scrollBarH.isVisible() && scrollBarH.contains( x, y ) ) {
				updateDiff( diff, false );
				return scrollBarH;
			}			
			
			x -= getScrollX();
			y -= getScrollY();
			
			for ( int i = activeComponents - 1; i >= 0; --i ) {
				Component c = components[ i ];
			
				if ( c instanceof Container ) {
					// pode haver mais de 1 contâiner dentro de outro - não interrompe a busca caso não ache um componente
					// na posição x,y recebida dentro de um deles
					c = ( ( Container ) c ).getComponentAt( x, y, diff );
					if ( c != null ) {
						updateDiff( diff, true );
						return c;
					}
				} else {
					if ( c.contains( x, y ) ) {
						// aqui não é necessário descontar a borda, pois a borda do componente é considerada parte interna dele
						updateDiff( diff, true );
						
						return c;
					}
				}
			}
			
			return this;
		}
		
		return null;		
	}
	
	
	private final void updateDiff( Point diff, boolean addScroll ) {
		if ( diff != null ) {
			final int borderOffsetX = ( border == null ? 0 : border.getLeft() );
			final int borderOffsetY = ( border == null ? 0 : border.getTop() );

			if ( addScroll ) {
				diff.x += position.x + getScrollX() + borderOffsetX;
				diff.y += position.y + getScrollY() + borderOffsetY;
			} else {
				diff.x += position.x + borderOffsetX;
				diff.y += position.y + borderOffsetY;
			}
		}		
	}
	

	/**
	 * @inheritDoc
	 */
	public boolean isScrollableX() {
		return scrollableX;
//		return scrollableX && getPreferredWidth() > getWidth(); // TODO testar
//		return scrollableX && getMaximumWidth() > getWidth();
	}


	/**
	 * @inheritDoc
	 */
	public boolean isScrollableY() {
		return scrollableY; // TODO testar
//		return scrollableY && getPreferredHeight() > getHeight(); // TODO testar
//		return scrollableY && getMaximumHeight() > getHeight();
	}


	/**
	 * Indicates whether the component should/could scroll by default a component
	 * is not scrollable.
	 * 
	 * @return whether the component is scrollable
	 */
	protected boolean isScrollable() {
		return isScrollableX() || isScrollableY();
	}


	/**
	 * Sets whether the component should/could scroll on the X axis
	 * 
	 * @param scrollableX whether the component should/could scroll on the X axis
	 */
	public void setScrollableX( boolean scrollableX ) {
		this.scrollableX = scrollableX;
	}


	/**
	 * Sets whether the component should/could scroll on the Y axis
	 * 
	 * @param scrollableY whether the component should/could scroll on the Y axis
	 */
	public void setScrollableY( boolean scrollableY ) {
		this.scrollableY = scrollableY;
	}


	/**
	 * The equivalent of calling both setScrollableY and setScrollableX
	 * 
	 * @param scrollable whether the component should/could scroll on the 
	 * X and Y axis
	 */
	public final void setScrollable( boolean scrollable ) {
		setScrollableX( scrollable );
		setScrollableY( scrollable );
	}	
	

	/**
	 * Altera o scroll do container de forma a exibir o componente.
	 * @param c componente a ser exibido pelo container.
	 */
	public void scrollToComponent( Component c ) {
		if ( c != null /*&& c.getParent() == this */ ) {
			if ( isScrollableX() ) {
				int absX = getScrollX();
				// caso o componente a ser exibido não seja filho direto deste contâiner, deve-se adicionar a posição
				// relativa de cada pai, até chegar a este contâiner
				Container p = c.getParent();
				while ( p != this && p != null ) {
					absX += p.getPosX();
					if ( p.getBorder() != null )
						absX += p.getBorder().getBorderWidth();

					p = p.getParent();
				}

				final int RELATIVE_X = absX + c.getPosX();
				if ( RELATIVE_X + c.getWidth() > getWidth() ) {
					setScrollX( getScrollX() + getWidth() - RELATIVE_X - c.getWidth() );
				} else if ( RELATIVE_X < 0 ) {
					setScrollX( -c.getPosX() );
				}
			}
			
			if ( isScrollableY() ) {
				int absY = c.getPosY();
				int PARENT_BORDER_TOP = 0;
				int PARENT_BORDER_BOTTOM = 0;
				// caso o componente a ser exibido não seja filho direto deste contâiner, deve-se adicionar a posição
				// relativa de cada pai, até chegar a este contâiner
				Container p = c.getParent();
				while ( p != this && p != null && !p.isScrollableY() ) {
					absY += p.getPosY();
					if ( p.getBorder() != null ) {
						PARENT_BORDER_TOP += p.getBorder().getTop();
						PARENT_BORDER_BOTTOM += p.getBorder().getBottom();
					}
					
					p = p.getParent();
				}

				final int BORDER_BOTTOM = c.getBorder() == null ? 0 : c.getBorder().getBottom();
				final int BORDER_TOP = c.getBorder() == null ? 0 : c.getBorder().getTop();


				
				if ( absY + c.getHeight() + BORDER_BOTTOM > getHeight() - getScrollY() ) {
					setScrollY( getHeight() - absY - BORDER_BOTTOM - c.getHeight() - PARENT_BORDER_TOP );
				} else if ( getScrollY() + absY < BORDER_TOP ) {
					setScrollY( -absY + BORDER_TOP + PARENT_BORDER_BOTTOM );
				}
			}
		}
	}

	
	/**
	 * Returns the width for layout manager purposes, this takes scrolling
	 * into consideration unlike the getWidth method.
	 * 
	 * @return the layout width
	 */
	public int getLayoutWidth() {
		final int WIDTH = super.getLayoutWidth();
		
		if ( isScrollableX() ) {
			final int scrollHeight = scrollBarH == null ? 0 : scrollBarH.getHeight();
			return Math.max( WIDTH + scrollHeight, getPreferredWidth() + scrollHeight );
		} else {
			final Container scrollableParent = getScrollableParent();
			if ( scrollableParent != null && scrollableParent.isScrollableX() ) {
				return Math.max( WIDTH, getPreferredWidth() );
			}
			if ( WIDTH <= 0 ) {
				if ( parent != null ) {
					return Math.min( getPreferredWidth(), getPreferredSize( new Point( parent.getLayoutWidth(), parent.getLayoutHeight() ) ).x );
				}
				return getPreferredWidth();
			}
			return WIDTH;
		}
	}


	/**
	 * Returns the height for layout manager purposes, this takes scrolling
	 * into consideration unlike the getWidth method.
	 * 
	 * @return the layout height
	 */
	public int getLayoutHeight() {
		final int HEIGHT = super.getLayoutHeight();
		
		if ( isScrollableX() ) {
			final int scrollHeight = scrollBarH == null ? 0 : scrollBarH.getHeight();
			return Math.max( HEIGHT, getPreferredHeight() ) + scrollHeight;
		} else {
//			final Container scrollableParent = getScrollableParent();
//			if ( scrollableParent != null && scrollableParent.isScrollableY() ) {
			if ( isScrollableY() ) {
				return Math.max( HEIGHT, getPreferredHeight() );
			}
			if ( HEIGHT <= 1 ) {
				return getPreferredHeight();
			}
			return HEIGHT;
		}
	}



	public void setBorder( Border border, boolean fitSize ) {
		super.setBorder( border, fitSize );

		// atualiza as barras de scroll pois pode ter ocorrido mudança do tamanho interno, e com isso é necessário
		// reposicionar as barras de scroll (caso existem) de acordo.
		refreshScrollH();
		refreshScrollV();
	}
	
	
	public void setSize( int width, int height ) {
		super.setSize( width, height );
		
		if ( width < getPreferredWidth() || height < getPreferredHeight() ) {
			refreshLayout();
			
			final Form form = getComponentForm();
			if ( form != null )
				form.scrollToComponent( form.getFocused() );
		}
	}


	public final ScrollBar getScrollBarH() {
		return scrollBarH;
	}


	public void setScrollBarH( ScrollBar scrollBarH ) {
		// anula a referência anterior da barra de rolagem para o pai (no caso, este contâiner)
		if ( this.scrollBarH != null )
			this.scrollBarH.setParent( null );

		this.scrollBarH = scrollBarH;
		
		if ( scrollBarH != null ) {
			scrollBarH.setParent( this );
		}		
	}


	public final ScrollBar getScrollBarV() {
		return scrollBarV;
	}


	public void setScrollBarV( ScrollBar scrollBarV ) {
		// anula a referência anterior da barra de rolagem para o pai (no caso, este contâiner)
		if ( this.scrollBarV != null )
			this.scrollBarV.setParent( null );

		this.scrollBarV = scrollBarV;
		
		if ( scrollBarV != null ) {
			scrollBarV.setParent( this );
		}
	}
	
	
    /**
     * @inheritDoc
     */
    public Point calcPreferredSize( Point maximumSize ) {
		if ( layout == null )
			return super.calcPreferredSize( maximumSize );
		
        return layout.calcPreferredSize( this, maximumSize );
    }	
	
	
	/**
	 * Atualiza o layout do contâiner e de seus filhos, levando em consideração seu tamanho (com as barras de scroll,
	 * se existentes).
	 */
	public void refreshLayout() {
		if ( layout != null )
			layout.layoutContainer( this );

		setScroll( getScroll() );

		for ( short i = 0; i < activeComponents; ++i ) {
			if ( components[ i ] instanceof Container ) {
				( ( Container ) components[ i ] ).refreshLayout();
			}
		}
	}
	
	
	/**
	 * Atualiza ambas as barras de scroll (horizontal e vertical), caso existam.
	 */
	public final void refreshScrollBars() {
		refreshScrollH();
		refreshScrollV();
	}
	
	
	protected void refreshScrollH() {
		// se a barra de scroll não existir, não há nada a atualizar
		if ( scrollBarH != null ) {
			if ( isScrollableX() ) {
				final boolean scrollV = isScrollableY() && scrollBarV != null;
				scrollBarH.setSize( getWidth() - ( scrollV ? scrollBarV.getWidth() : 0 ), scrollBarH.getHeight() );
				scrollBarH.setPosition( 0, getLayoutHeight() - scrollBarH.getHeight() );

				scrollBarH.refreshScroll( this );
				scrollBarH.setVisible( true );
			} else {
				scrollBarH.setVisible( true );
			}		
		}
	}


	protected void refreshScrollV() {
		// se a barra de scroll não existir, não há nada a atualizar
		if ( scrollBarV != null ) {
//			if ( isScrollableY() ) { // TODO teste
			if ( isScrollableY() && getMaximumHeight() > getHeight() ) {
				final boolean scrollH = isScrollableX() && scrollBarH != null;
				scrollBarV.setSize( scrollBarV.getWidth(), getHeight() - ( scrollH ? scrollBarH.getHeight() : 0 ) );
				scrollBarV.setPosition( getLayoutWidth() - scrollBarV.getWidth(), 0 );

				scrollBarV.refreshScroll( this );
				
				scrollBarV.setVisible( true );
			} else {
				setScrollY( 0, false );
				scrollBarV.setVisible( false );
			}
		}
	}


	protected void paint( Graphics g ) {
		translate.addEquals( getScroll() );
	
		super.paint( g );
		
		translate.subEquals( getScroll() );
		
		if ( scrollBarH != null )
			scrollBarH.draw( g );
		if ( scrollBarV != null )
			scrollBarV.draw( g );
	}
	

	//#if TOUCH == "true"
		/**
		 * @inheritDoc
		 */
		public void onPointerPressed( int x, int y ) {
			if ( pointerListener == null ) {
				x -= position.x;
				y -= position.y;

				final Point diff = new Point();
				final Component cmp = getComponentAt( x, y, diff );

				x -= diff.x;
				y -= diff.y;

				if ( cmp == this ) {
					super.onPointerPressed( x, y );
				} else if ( cmp != null ) {
					cmp.onPointerPressed( x, y );
				}
			} else {
				pointerListener.onPointerPressed( x - position.x, y - position.y );
			}
		}


		public void onPointerDragged( int x, int y ) {
			if ( pointerListener == null ) {
				x -= position.x;
				y -= position.y;

				final Point diff = new Point();
				final Component cmp = getComponentAt( x, y, diff );

				x -= diff.x;
				y -= diff.y;

				if ( cmp == this ) {
					super.onPointerDragged(  x, y );
				} else if ( cmp != null ) {
					cmp.onPointerDragged( x, y );
				}
			} else {
				pointerListener.onPointerDragged( x - position.x, y - position.y );
			}
		}


		public void onPointerReleased( int x, int y ) {
			if ( pointerListener == null ) {
				x -= position.x;
				y -= position.y;

				final Point diff = new Point();
				final Component cmp = getComponentAt( x, y, diff );

				x -= diff.x;
				y -= diff.y;

				if ( cmp == this ) {
					super.onPointerReleased( x, y );
				} else if ( cmp != null ) {
					cmp.onPointerReleased( x, y );
				}
			} else {
				pointerListener.onPointerReleased( x - position.x, y - position.y );
			}
		}

	//#endif
	
	
	/**
	 * Returns the layout manager responsible for arranging this container
	 * 
	 * @return the container layout manager
	 */
	public final Layout getLayout() {
		return layout;
	}


	/**
	 * Sets the layout manager responsible for arranging this container
	 * 
	 * @param layout the specified layout manager
	 */
	public void setLayout( Layout layout ) {
		this.layout = layout;
	}
	
	
	public int getScrollX() {
		return scroll.x;
	}
	
	
	public int getScrollY() {
		return scroll.y;
	}
	
	
	public Point getScroll() {
		return scroll;
	}
	
	
	public final void setScrollX( int x ) {
		scroll.x = NanoMath.clamp( x, getWidth() - getLayoutWidth(), 0 );
		refreshScrollH();
	}
	
	
	public void setScrollY( int y ) {
		setScrollY( y, true );
	}


	protected final void setScrollY( int y, boolean refresh ) {
		final int diff = getHeight() - getMaximumHeight();
		if ( y > 0 )
			scroll.y = 0;
		else if ( diff < 0 && y < diff )
			scroll.y = diff;
		else
			scroll.y = y;
		
		if ( refresh )
			refreshScrollV();
	}
	
	
	public final void setScroll( int x, int y ) {
		setScrollX( x );
		setScrollY( y );
	}
	
	
	public final void setScroll( Point scroll ) {
		setScroll( scroll.x, scroll.y );
	}
	
	
	/**
	 * Returns the absolute position location based on the component hierarchy, this method
	 * calculates a location on the screen for the component rather than a relative
	 * location as returned by getPosition()
	 * 
	 * @see #getPosition
	 */
	public void getAbsolutePos( Point p ) {
		p.addEquals( getScroll() );
		
		if ( border != null )
			p.addEquals( border.getTopLeft() );
		
		super.getAbsolutePos( p );
	}
	
	
	
	public String getUIID() {
		return "container";
	}
	
}
