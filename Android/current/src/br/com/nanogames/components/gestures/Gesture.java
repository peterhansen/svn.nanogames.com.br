//package br.com.nanogames.components.gestures;
//
//import java.util.Vector;
//
//
//public class Gesture {
//
//	private double maxacc;
//	private double minacc;
//	Vector data;
//
//
//	public Gesture() {
//		this.maxacc = Double.MIN_VALUE;
//		this.data = new Vector();
//	}
//
//
//	public Gesture( Gesture original ) {
//		this.data = new Vector();
//		Vector origin = original.getData();
//		for ( int i = 0; i < origin.size(); i++ ) {
//			this.add( ( AccelerationEvent ) origin.elementAt( i ) );
//		}
//		this.maxacc = original.getMaxAcceleration();
//		this.minacc = original.getMinAcceleration();
//	}
//
//
//	public void add( AccelerationEvent event ) {
//		this.data.addElement( event );
//	}
//
//
//	public AccelerationEvent getLastData() {
//		return ( AccelerationEvent ) this.data.elementAt( this.data.size() - 1 );
//	}
//
//
//	public Vector getData() {
//		return this.data;
//	}
//
//
//	public int getCountOfData() {
//		return this.data.size();
//	}
//
//
//	public void setMaxAcceleration( double maxacc ) {
//		this.maxacc = maxacc;
//	}
//
//
//	public double getMaxAcceleration() {
//		return this.maxacc;
//	}
//
//
//	public void setMinAcceleration( double minacc ) {
//		this.minacc = minacc;
//	}
//
//
//	public double getMinAcceleration() {
//		return this.minacc;
//	}
//
//
//}
//
