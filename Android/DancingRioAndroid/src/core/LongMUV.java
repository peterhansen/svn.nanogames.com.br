/**
 * LongMUV.java
 * ©2008 Nano Games.
 *
 * Created on 17/07/2008 10:28:08.
 */
package core;

/**
 * @author Daniel L. Alves
 */

public final class LongMUV
{
	private long speed;

	private long lastSpeedModule;

	public LongMUV()
	{
	}

	/**
	 * Creates a new instance of MUV
	 */
	public LongMUV( long speed )
	{
		setSpeed( speed );
	}

	/**
	 * Define a velocidade do movimento, e zera o resto da divisão do último cálculo. Equivalente à chamada de <i>setSpeed(speed, true)</i>.
	 * @param speed velocidade do movimento em unidades por segundo.
	 * @see #setSpeed(long,boolean)
	 */
	public final void setSpeed( long speed )
	{
		setSpeed( speed, true );
	}

	/**
	 * Define a velocidade do movimento.
	 * @param speed velocidade do movimento em unidades por segundo.
	 * @param resetModule indica se o resto da divisão do último cálculo realizado deve ser zerado.
	 * @see #setSpeed(long)
	 */
	public final void setSpeed( long speed, boolean resetModule )
	{
		this.speed = speed;
		if( resetModule )
			lastSpeedModule = 0;
	}

	public final long update( long delta )
	{
		final long ds = lastSpeedModule + ( speed * delta );
		final long dx = ds / 1000;

		lastSpeedModule = ds % 1000;

		return dx;
	}

	public final long getSpeed()
	{
		return speed;
	}
}
