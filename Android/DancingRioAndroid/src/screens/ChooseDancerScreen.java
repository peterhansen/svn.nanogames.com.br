/*
 * ChooseCharacterScreen.java
 *
 * Created on June 22, 2007, 5:17 PM
 *
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Point;
import core.Constants;
import core.GameMIDlet;
import br.com.nanogames.components.userInterface.ScreenListener;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

/**
 *
 * @author peter
 */
public final class ChooseDancerScreen extends UpdatableGroup implements Constants, KeyListener,ScreenListener
		//#if TOUCH == "true"
	, PointerListener
		//#endif
{

	private static final byte INDEX_DANCER = 1;
	private static final byte NUMBER_OF_ITEMS = 7;
	private DrawableImage dancer;
	private final Label name;
	private final Sprite arrowLeft;
	private final Sprite arrowRight;

	// sequencias de animação das setas
	private final byte ARROW_SEQUENCE_STOPPED = 1;
	private final byte ARROW_SEQUENCE_PRESSED = 0;
	public byte index;
	private final RichLabel title;
	private static byte currentDancer;

	
	/** Creates a new instance of ChooseCharacterScreen */
	public ChooseDancerScreen(MenuListener listener, int id) throws Exception {
		super(NUMBER_OF_ITEMS);

		title = new RichLabel(GameMIDlet.getInstance().getFont(FONT_MENU), TEXT_CHOOSE_DANCER/*, ScreenManager.SCREEN_WIDTH*/);
		insertDrawable(title);

		name = new Label(GameMIDlet.getInstance().getFont(FONT_MENU), null );
		insertDrawable(name);

		arrowLeft = new Sprite(ROOT_DIR + "right");
		arrowLeft.setTransform(TRANS_MIRROR_H);

		insertDrawable(arrowLeft);

		arrowRight = new Sprite(arrowLeft);
		insertDrawable(arrowRight);

		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );
		setCurrentDancer(NanoMath.randInt(TOTAL_DANCERS));
		try{
	
			if (!MediaPlayer.isPlaying()) {
				MediaPlayer.play(SOUND_INDEX_CHOOSE_CHARACTER, MediaPlayer.LOOP_INFINITE);
			}
		} catch (Throwable t) {
			GameMIDlet.setScreen(SCREEN_MAIN_MENU);
		}
	}

	
	public static final byte getDancerIndex() {
		return currentDancer;
	}

	
	public final void setCurrentDancer(int index) {
		if (index < 0) {
			index = TOTAL_DANCERS - 1;
		} else if (index >= TOTAL_DANCERS) {
			index = 0;
		}

		currentDancer = (byte) index;

		// remove e desaloca o personagem antigo
		removeDrawable(dancer);
		dancer = null;
		System.gc();

		try {
			switch (currentDancer) {
				case INDEX_DANCER_FUNK:
					dancer = new DrawableImage(ROOT_DIR + "Shu/funkeira_0.png");
					name.setText(TEXT_FUNKERA);
					break;

				case INDEX_DANCER_SKULL:
					dancer = new DrawableImage(ROOT_DIR + "Caveira/caveirex_0.png");
					name.setText(TEXT_SKULL);
					break;
			}
			insertDrawable(dancer, INDEX_DANCER);
			refreshDancerPosition();
		} catch (Exception e) {
			//#if DEBUG == "true"
			e.printStackTrace();
		//#endif
		}
	}

	
	public final void keyPressed(int key) {
		switch (key) {
			case ScreenManager.KEY_SOFT_RIGHT:
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
				GameMIDlet.setScreen(SCREEN_MAIN_MENU);
				return;

			case ScreenManager.KEY_SOFT_MID:
			case ScreenManager.KEY_SOFT_LEFT:
			case ScreenManager.FIRE:
			case ScreenManager.KEY_NUM5:
				GameMIDlet.setScreen(SCREEN_CHOOSE_COLORS);
				break;

			case ScreenManager.RIGHT:
			case ScreenManager.DOWN:
			case ScreenManager.KEY_NUM8:
			case ScreenManager.KEY_NUM6:
				nextDancer();
				break;

			case ScreenManager.LEFT:
			case ScreenManager.UP:
			case ScreenManager.KEY_NUM2:
			case ScreenManager.KEY_NUM4:
				previousDancer();
				break;
		} // fim switch ( ScreenManager.getSpecialKey( key ) )
	} // fim do m�todo keyPressed( int )

	
	public final void keyReleased(int key) {
		arrowLeft.setSequence(ARROW_SEQUENCE_STOPPED);
		arrowRight.setSequence(ARROW_SEQUENCE_STOPPED);
	}

	
	public final void previousDancer() {
		setCurrentDancer(currentDancer - 1);
		arrowLeft.setSequence(ARROW_SEQUENCE_PRESSED);
		arrowRight.setSequence(ARROW_SEQUENCE_STOPPED);
	}

	
	public final void nextDancer() {
		setCurrentDancer(currentDancer + 1);
		arrowRight.setSequence(ARROW_SEQUENCE_PRESSED);
		arrowLeft.setSequence(ARROW_SEQUENCE_STOPPED);
	}


	//#if TOUCH == "true"
		public final void onPointerPressed(int x, int y) {
			if ( x < getWidth() / 3 ) {
				previousDancer();
			} else if ( x > ( getWidth() << 1 ) / 3 ) {
				nextDancer();
			} else {
				keyPressed(ScreenManager.FIRE);
			}
		}


		public final void onPointerDragged(int x, int y) {
		}


		public final void onPointerReleased(int x, int y) {
			keyReleased(0);
		}
	//#endif
		

	public final void setSize( int width, int height ) {

		title.setSize( width * 9 / 10, height);
		title.formatText(false);
		title.setPosition((width - title.getWidth()) >> 1, Math.max(title.getPosY(), MENU_ITEMS_SPACING));
		
		final int DANCER_Y = title.getPosY() + title.getTextTotalHeight();
		
		super.setSize( width, DANCER_Y + ( MENU_ITEMS_SPACING * 3 ) + CHOOSE_PLAYER_CHARACTER_HEIGHT + name.getFont().getHeight() );
		refreshDancerPosition();
		setPosition(0, 0);
		arrowLeft.defineReferencePixel(ANCHOR_LEFT | ANCHOR_VCENTER);
		arrowLeft.setRefPixelPosition(arrowLeft.getWidth(), getHeight() >> 1);

		arrowRight.defineReferencePixel(ANCHOR_VCENTER | ANCHOR_RIGHT);
		arrowRight.setRefPixelPosition( width - arrowRight.getWidth(), getHeight() >> 1);
	}

	
	private final void refreshDancerPosition() {
		if (dancer != null) {
			final int INITIAL_Y = title.getPosY() + title.getTextTotalHeight() + MENU_ITEMS_SPACING;
			dancer.defineReferencePixel(ANCHOR_HCENTER | ANCHOR_TOP);
			name.defineReferencePixel(name.getWidth() >> 1, 0);
			name.setRefPixelPosition(getWidth() >> 1, INITIAL_Y + CHOOSE_PLAYER_CHARACTER_HEIGHT);


			switch (currentDancer) {
				case INDEX_DANCER_FUNK:
					dancer.setRefPixelPosition((getWidth() >> 1) + FUNKEIRA_OFFSET_X, INITIAL_Y + CHOOSE_PLAYER_CHARACTER_HEIGHT - dancer.getHeight());
					break;

				case INDEX_DANCER_SKULL:
					dancer.setRefPixelPosition(getWidth() >> 1, INITIAL_Y + CHOOSE_PLAYER_CHARACTER_HEIGHT - dancer.getHeight());
					break;
			}
		}
	}

	public final void sizeChanged(int width, int height) {
		if ( width != getWidth() || height != getHeight() ){
			setSize(width, height);
		}
	}

    public void hideNotify(boolean deviceEvent) {
    }

    public void showNotify(boolean deviceEvent) {
    }
}
