/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import core.GameMIDlet;

/**
 *
 * @author Edson
 */
public final class BkgLapa extends UpdatableGroup implements Constants , ScreenListener {
	
	private static final int COLOR_SKY_DARK = 0x040837;
	private static final int COLOR_SKY_BOTTOM = 0x040837;

	/* imagens do cenario*/
	private final Pattern bkgSkyTop;
	private final Pattern bkgSky;
	private final Pattern bkgBottom;
	private final DrawableImage morro;
	private final Drawable calcadaEsquerda;
	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "SUPER_BIG"
		private final Drawable calcadaDireita;
	//#endif
		
	protected final Pattern arcos;
	private final Pattern floor2;
	private final DrawableImage bonde;
	private final DrawableImage nuvem;
	//#if SCREEN_SIZE =="SUPER_BIG"
 	private  DrawableImage nuvem2;
 	private  DrawableImage nuvem3;
 	private  Pattern auxsky ;
 	private final Pattern plateia2;
    private final DrawableImage pino;
 	private final Pattern pinos;
	//#endif
	private final DrawableImage palmeira;
	public final MUV speedX;
	public final MUV speedXNuv;
	protected int velXAmb;
	private final Pattern plateia;
	
	private final Drawable predio;
	
	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "SUPER_BIG"
		private final Pattern floor2Aux;
	//#endif

	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM" 
//		private final byte DIRECTION_RIGHT = 0;
//		private final byte DIRECTION_LEFT = 1;
//		private int direction = 1;
//		private final Sprite figurante1;
//		private final Sprite ambulante;
//		private final MUV speedXAmbu;
//		private byte nanoX;
//		private final DrawableImage nano;
//		private final Sprite figurante2;
	//#endif
		
	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "SUPER_BIG" || SCREEN_SIZE == "MEDIUM"
		private final Pattern floor_1;
	//#endif

	
	
	public static final BkgLapa getInstance() throws Exception {
		return new BkgLapa();
	}

	
	private BkgLapa() throws Exception {
		super( 28 );

		velXAmb = 30;
		speedX = new MUV( 25 );
		speedXNuv = new MUV( 5 );

		floor2 = new Pattern(new DrawableImage(PATH_BKG + "traschao.png"));
		
		//#if SCREEN_SIZE == "BIG"
//			calcadaEsquerda = new DrawableImage(PATH_BKG + "calcada.png");
//			calcadaDireita = new DrawableImage( ( DrawableImage ) calcadaEsquerda );
//			floor2Aux = new Pattern(0x78787d);
//			calcadaDireita.mirror( TRANS_MIRROR_H );
		//#elif SCREEN_SIZE == "SUPER_BIG"
 			calcadaEsquerda = new DrawableImage(PATH_BKG + "calcada.png");
 			calcadaDireita = new DrawableImage( ( DrawableImage ) calcadaEsquerda );
 			floor2Aux = new Pattern(0x78787d);
 			calcadaDireita.mirror( TRANS_MIRROR_H );
 			auxsky = new Pattern(0x27318d);
 			plateia2 = new Pattern(new Sprite(PATH_BKG + "multidaolapa"));
		//#elif SCREEN_SIZE == "MEDIUM"
//# 			if ( GameMIDlet.isLowMemory() ) {
//# 				calcadaEsquerda = new Pattern( new DrawableImage( PATH_BKG + "sidewalk.png" ) );
//# 			} else {
//# 				calcadaEsquerda = new DrawableImage(PATH_BKG + "calcada.png");
//# 			}
		//#else
//# 		if ( GameMIDlet.isLowMemory() )
//# 				calcadaEsquerda = new Pattern( new DrawableImage( ( DrawableImage ) floor2.getFill() ) );
//# 		else
//# 				calcadaEsquerda = new DrawableImage(PATH_BKG + "calcada.png");
		//#endif
		
		arcos = new Pattern( getArcos() );
		bkgSkyTop = new Pattern(COLOR_SKY_DARK);
		bkgBottom = new Pattern(COLOR_SKY_BOTTOM);
		
		if ( GameMIDlet.isLowMemory() )
			bkgSky = new Pattern( COLOR_SKY_DARK );
		else
			bkgSky = new Pattern(new DrawableImage(PATH_BKG + "pattern_sky.png"));
		
		bonde = new DrawableImage(PATH_BKG + "bondinho.png");
		//#if SCREEN_SIZE != "BIG"
 		if (!GameMIDlet.isLowMemory()) {
 			plateia = new Pattern(new Sprite(PATH_BKG + "multidaolapa"));
 		} else {
 			plateia= new Pattern(new DrawableImage(PATH_BKG + "multidaolapa_0.png"));
 		}
		//#else
//			plateia = new Pattern(new Sprite(PATH_BKG + "multidaolapa"));
		//#endif

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM"
//
//			if ( !GameMIDlet.isLowMemory() ) {
//				figurante1 = new Sprite(PATH_BKG + "negamaluca");
//			} else {
//				figurante1=null;
//			}
//			speedXAmbu = new MUV( velXAmb );
//			if ( !GameMIDlet.isLowMemory() ) {
//				figurante2 = new Sprite(PATH_BKG + "gringo");
//			} else {
//				figurante2=null;
//			}
//
//			if (GameMIDlet.isLowMemory()) {
//				nano = null;
//				ambulante = null;
//			} else {
//				nano = new DrawableImage(PATH_BKG + "nano.png");
//				ambulante = new Sprite(PATH_BKG + "vendedor");
//			}
		//#endif

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM" || SCREEN_SIZE == "SUPER_BIG"
			floor_1 = new Pattern(new DrawableImage(PATH_BKG + "frentefundo.png"));
		//#endif

		insertDrawable(bkgSkyTop);
		insertDrawable(bkgSky);
		//#if SCREEN_SIZE=="SUPER_BIG"
		 insertDrawable(auxsky);
		//#endif
		insertDrawable(bkgBottom);

		//#if SCREEN_SIZE != "SMALL"
			predio = getBuilding();
		//#endif
		
		if ( GameMIDlet.isLowMemory() ) {
			//#if SCREEN_SIZE == "SMALL"
//# 				predio = null;
			//#endif
				
			morro = null;
			nuvem = null;
			palmeira = null;
		} else {
			//#if SCREEN_SIZE == "SMALL"
//# 				predio = getBuilding();
			//#endif
				
			morro = new DrawableImage(PATH_BKG + "morro.png");
			if(morro != null)
			insertDrawable(morro);
			
			nuvem = new DrawableImage(PATH_BKG + "nuvem1.png");
			if(nuvem != null)
			insertDrawable(nuvem);

			//#if SCREEN_SIZE == "SUPER_BIG"
 			nuvem2 = new DrawableImage(PATH_BKG + "nuvem1.png");
 			nuvem3 = new DrawableImage(PATH_BKG + "nuvem1.png");
 			insertDrawable(nuvem2);
 			insertDrawable(nuvem3);
			//#endif

			palmeira = getPalm();
			if(getPalm() != null )
			insertDrawable(palmeira);
		}

		if ( predio != null )
			insertDrawable(predio);
			
		insertDrawable(floor2);
		insertDrawable(calcadaEsquerda);
		
		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE == "SUPER_BIG"
			insertDrawable(calcadaDireita);
			insertDrawable(floor2Aux);
		//#endif
			
		insertDrawable(arcos);
		insertDrawable(plateia);
		//#if SCREEN_SIZE == "SUPER_BIG"
 		 insertDrawable(plateia2);
 		pino = new DrawableImage(PATH_BKG + "pino.png");
 		pino.setSize(80, pino.getHeight());
 		pinos = new Pattern(pino);
 			insertDrawable(floor_1);
 			insertDrawable(pinos);
		//#endif

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM"
//			insertDrawable(floor_1);
//			insertDrawable(floor_1);
//			if (figurante1 != null) {
//				insertDrawable(figurante1);
//			}
//			if (figurante2 != null) {
//				insertDrawable(figurante2);
//			}
//			if (!GameMIDlet.isLowMemory()) {
//				insertDrawable(ambulante);
//				insertDrawable(nano);
//			}
		//#endif
		insertDrawable(bonde);
	
	}
	
	private static final Drawable getBuilding() throws Exception {

		if (GameMIDlet.isLowMemory()) {
			return new DrawableImage(PATH_BKG + "predioxx_0.png");
		}

		return new Sprite(PATH_BKG + "predioxx");
	}

	public static final DrawableImage getArcos() throws Exception {

		return new DrawableImage(PATH_BKG + "arcos.png");
	}

	public static final DrawableImage getPalm() throws Exception {

		return new DrawableImage(PATH_BKG + "arvore.png");
	}

	public static final Drawable getCrowd() throws Exception {

		if (GameMIDlet.isLowMemory()) {
			return new DrawableImage(PATH_BKG + "multidaolapa_0.png");
		}

		return new Sprite(PATH_BKG + "multidaolapa");

	}


	public final void setSize(int width, int height) {
		super.setSize(width, height);
        setPosition(0, 0);
		final int mulherDistY;
		final int mulherDistX;
		final int gringoDistY;
		final int alsfaltoDistY;
		final int floor_1Y;
		int predioDistY;
		int predioDistX;
		byte ajusteArcosY = 10;
		int maxHeight = 320;
		int heightBb = 260;


		//#if  SCREEN_SIZE == "BIG"
//		if (width < height) {
//			floor_1Y = 15;
//
//			floor_1.setSize(ScreenManager.SCREEN_WIDTH, floor_1.getHeight());
//			floor_1.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight() + floor_1Y);
//
//			if (height <= heightBb) {
//				arcos.setPosition(0, floor2.getPosY() - arcos.getHeight());
//				arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//				bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//
//			} else if (height <= maxHeight) {
//				arcos.setPosition(0, floor2.getPosY() - arcos.getHeight());
//				arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//				bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//			} else {
//				if (height > maxHeight) {
//					arcos.setPosition(0, floor2.getPosY() - arcos.getHeight());
//					arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//					bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//				}
//			}
//
//			if (!GameMIDlet.isLowMemory() )
//			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
//
//			calcadaEsquerda.setSize(ScreenManager.SCREEN_HALF_WIDTH, calcadaEsquerda.getHeight());
//			calcadaEsquerda.setPosition(0, floor_1.getPosY() - calcadaEsquerda.getHeight());
//
//			calcadaDireita.setSize(ScreenManager.SCREEN_HALF_WIDTH, calcadaDireita.getHeight());
//			calcadaDireita.setPosition(calcadaEsquerda.getPosX() + calcadaEsquerda.getWidth(), calcadaEsquerda.getPosY());
//
//			alsfaltoDistY = calcadaEsquerda.getPosY() + 8;
//
//			if (height <= maxHeight) {
//
//				floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight());
//				floor2.setPosition(0, calcadaEsquerda.getPosY() - floor2.getHeight());
//				arcos.setPosition(0, floor2.getPosY() - arcos.getHeight());
//				arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//				bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//				predioDistX = 2;
//				predioDistY = 2;
//				predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight() - predioDistY);
//
//				final byte ambulantY = 13;
//				nanoX = 7;
//				final byte nanoY = 51;
//               if (!GameMIDlet.isLowMemory() ){
//				ambulante.setPosition(ScreenManager.SCREEN_WIDTH - ambulante.getWidth(), calcadaEsquerda.getPosY() - ambulante.getHeight() + ambulantY);
//				nano.setPosition(ambulante.getPosX() + nanoX, ambulante.getPosY() + nanoY);}
//			} else {
//				if (height > maxHeight) {
//					floor2Aux.setSize(width, 2 * floor2.getHeight());
//					floor2Aux.setPosition(0, calcadaEsquerda.getPosY() - floor2Aux.getHeight());
//					floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight());
//					floor2.setPosition(0, floor2Aux.getPosY() - floor2.getHeight());
//					arcos.setPosition(0, floor2.getPosY() - arcos.getHeight());
//					arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//					bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//
//					predioDistX = 25;
//					predioDistY = 15;
//					predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight() - predioDistY);
//
//					final byte ambulantY = 13;
//					nanoX = 7;
//					final byte nanoY = 51;
//
//					ambulante.setPosition(ScreenManager.SCREEN_WIDTH - ambulante.getWidth(), floor2Aux.getPosY() - ambulante.getHeight() + ambulantY);
//					nano.setPosition(ambulante.getPosX() + nanoX, ambulante.getPosY() + nanoY);
//				}
//			}
//
//			if (palmeira != null )
//				palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth(), floor2.getPosY() - palmeira.getHeight() - 10);
//
//			mulherDistY = arcos.getPosY() + arcos.getHeight() / 2 - 5;
//			gringoDistY = arcos.getPosY() + arcos.getHeight() / 2;
//            if (figurante1 != null)
//				figurante1.setPosition(figurante1.getWidth(), mulherDistY);
//			if (figurante2 != null) {
//				figurante2.setPosition(ScreenManager.SCREEN_WIDTH - 2 * figurante2.getWidth(), gringoDistY);
//			}
//			/*arcos.setPosition(0, floor2.getPosY() - arcos.getHeight() + ajusteArcosY);
//			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());*/
//		} else {
//			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT)/2);
//
//			floor_1.setSize(ScreenManager.SCREEN_WIDTH, floor_1.getHeight() / 2);
//			floor_1.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight());
//
//			calcadaEsquerda.setSize(ScreenManager.SCREEN_HALF_WIDTH, calcadaEsquerda.getHeight());
//			calcadaEsquerda.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight() - calcadaEsquerda.getHeight());
//
//			calcadaDireita.setSize(ScreenManager.SCREEN_HALF_WIDTH, calcadaDireita.getHeight());
//			calcadaDireita.setPosition(calcadaEsquerda.getPosX() + calcadaEsquerda.getWidth(), calcadaEsquerda.getPosY());
//
//			floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight());
//			floor2.setPosition(0, calcadaEsquerda.getPosY() - floor2.getHeight());
//
//			arcos.setPosition(0, floor2.getPosY()-arcos.getHeight());
//			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//
//            alsfaltoDistY = calcadaEsquerda.getPosY() + 8;
//
//			predio.setPosition(morro.getPosX() + morro.getWidth() + predio.getWidth() / 3, floor2.getPosY() - predio.getHeight());
//
//			if ( palmeira != null )
//				palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth(), floor2.getPosY() - palmeira.getHeight() + 2);
//
//			mulherDistY = arcos.getPosY() + arcos.getHeight() / 2 ;
//			mulherDistX = 50;
//			gringoDistY = arcos.getPosY() + arcos.getHeight() / 2 ;
//           if (figurante1 != null) {
//				figurante1.setPosition(figurante1.getWidth() + mulherDistX, mulherDistY);
//			}
//			if (figurante2 != null) {
//				figurante2.setPosition(ScreenManager.SCREEN_WIDTH - figurante2.getWidth(), gringoDistY);
//			}
//
//			if ( ambulante != null ) {
//				final byte ambulantY = 13;
//				final byte nanoY = 50;
//				nanoX= 7;
//
//				ambulante.setPosition(ScreenManager.SCREEN_WIDTH - ambulante.getWidth(), calcadaEsquerda.getPosY() - ambulante.getHeight() + ambulantY);
//				nano.setPosition(ambulante.getPosX()+ nanoX, ambulante.getPosY() + nanoY);
//			}
//		}

		//#elif  SCREEN_SIZE == "SUPER_BIG"
 		if (width < height) {

 			floor_1Y = 20;



 			floor2Aux.setVisible(false);

 			floor_1.setSize(ScreenManager.SCREEN_WIDTH, floor_1.getHeight());
 			floor_1.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight()+ 2*floor_1Y);
 			if (height > 480) {

 				floor_1.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight() + floor_1Y);
 			}


 			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
             nuvem2.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
 			nuvem3.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));

 			calcadaEsquerda.setSize(ScreenManager.SCREEN_HALF_WIDTH, calcadaEsquerda.getHeight());
 			calcadaEsquerda.setPosition(0, floor_1.getPosY() - calcadaEsquerda.getHeight());

 			calcadaDireita.setSize(ScreenManager.SCREEN_HALF_WIDTH, calcadaDireita.getHeight());
 			calcadaDireita.setPosition(calcadaEsquerda.getPosX() + calcadaEsquerda.getWidth(), calcadaEsquerda.getPosY());

 			alsfaltoDistY = calcadaEsquerda.getPosY() + 8;
           if (height > 480) {
 				floor2.setSize(width, floor2.getHeight());
 			 } else  {
 			  floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight() -40);

 		  }
 		    ajusteArcosY = 10;

 			predioDistX = 2;
 			predioDistY = 2;


 			palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth() - 40, floor2.getPosY() - palmeira.getHeight() + 10 );


 			if(height > 600){
 				auxsky.setSize(width,arcos.getHeight() );
 				auxsky.setPosition(0, bkgSky.getPosY()+bkgSky.getHeight()- 5);
 			}
 		    floor2.setPosition(0, calcadaEsquerda.getPosY() - floor2.getHeight());
 			arcos.setPosition(0, floor2.getPosY() - arcos.getHeight() + ajusteArcosY);
 			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
 			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());

 			pinos.setSize(width, pino.getHeight());
 			pinos.setPosition(10, calcadaDireita.getPosY()+calcadaDireita.getHeight() - pino.getHeight());

 		} else {

 			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT/2));
 			nuvem2.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT)/2);
 			nuvem3.setPosition(NanoMath.randInt(ScreenManager.SCREEN_WIDTH), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT)/2);

 		    floor_1.setVisible(false);

 			calcadaDireita.setVisible(false);
 			calcadaEsquerda.setVisible(false);

 			floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight());
 			floor2.setPosition(0, height - floor2.getHeight());
 			ajusteArcosY = 10;
 			arcos.setPosition(0, floor2.getPosY()-arcos.getHeight()+ ajusteArcosY);
 			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
 			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());

 			alsfaltoDistY = calcadaEsquerda.getPosY() + 8;
 			predio.setPosition(getWidth() -2*predio.getWidth(), floor2.getPosY() - predio.getHeight() - 40);

 			if (palmeira != null) {
 				palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth(), floor2.getPosY() - palmeira.getHeight() + 2);
 			}

 			pinos.setSize(width, pino.getHeight());
 			pinos.setPosition(20, height - 2*pino.getHeight());

 		}
	//#elif SCREEN_SIZE == "MEDIUM"
//# 		if (width < height) {
//# 			floor_1Y = 5;
//#
//# 			floor_1.setSize(ScreenManager.SCREEN_WIDTH, floor_1.getHeight());
//# 			floor_1.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight() + floor_1Y);
//#
//# 			arcos.setPosition(0, ScreenManager.SCREEN_HALF_HEIGHT - arcos.getHeight()/3 + ajusteArcosY  );
//# 			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//#
//# 			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//#
//# 			if ( nuvem != null )
//# 			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH/2), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT));
//#
//# 			calcadaEsquerda.setSize(ScreenManager.SCREEN_WIDTH, calcadaEsquerda.getHeight());
//#
//# 			calcadaEsquerda.setPosition(0, floor_1.getPosY() - calcadaEsquerda.getHeight());
//#
//# 			floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight());
//# 			floor2.setPosition(0, calcadaEsquerda.getPosY() - floor2.getHeight());
//#
//# 			alsfaltoDistY = calcadaEsquerda.getPosY() + 8;
//#
//# 			predioDistX = 2;
//# 			predioDistY = 2;
//# 			final byte palmeiraY = 8;
//#
//# 			predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight());
//#
//# 			if ( palmeira != null )
//# 				palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth(), floor2.getPosY() - palmeira.getHeight() - palmeiraY);
//#
//# 			mulherDistY = arcos.getPosY() + arcos.getHeight() / 2 ;
//# 			gringoDistY = arcos.getPosY() + arcos.getHeight() / 2+3;
//# 			if (figurante1 != null) {
//# 				figurante1.setPosition(figurante1.getWidth(), mulherDistY);
//# 			}
//#
//# 			final byte ambulantY = 13;
//# 			if(!GameMIDlet.isLowMemory())
//# 			ambulante.setPosition(ScreenManager.SCREEN_WIDTH - ambulante.getWidth(), calcadaEsquerda.getPosY() - ambulante.getHeight() + ambulantY);
//# 			nanoX = 7;
//# 			final byte nanoY = 45;
//# 			if(!GameMIDlet.isLowMemory())
//# 			nano.setPosition(ambulante.getPosX() + nanoX, ambulante.getPosY() + nanoY);
//#             if (figurante2 != null) {
//# 				figurante2.setPosition(ScreenManager.SCREEN_WIDTH - 2 * figurante2.getWidth(), gringoDistY);
//# 			}
//#
//# 		} else {
//# 		    ajusteArcosY = 6;
//#
//# 			arcos.setPosition(0, ScreenManager.SCREEN_HALF_HEIGHT - ScreenManager.SCREEN_HALF_HEIGHT / 2+ajusteArcosY);
//# 			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//#
//# 			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//#
//# 			if ( nuvem != null )
//# 			nuvem.setPosition(NanoMath.randInt(ScreenManager.SCREEN_HALF_WIDTH/2), NanoMath.randInt(ScreenManager.SCREEN_HALF_HEIGHT/2));
//#
//# 			floor_1.setSize(ScreenManager.SCREEN_WIDTH, floor_1.getHeight() / 2);
//# 			floor_1.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight());
//#
//# 			calcadaEsquerda.setSize(ScreenManager.SCREEN_WIDTH, calcadaEsquerda.getHeight());
//# 			calcadaEsquerda.setPosition(0, ScreenManager.SCREEN_HEIGHT - floor_1.getHeight() - calcadaEsquerda.getHeight());
//#
//# 			floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight() + floor2.getHeight());
//# 			floor2.setPosition(0, calcadaEsquerda.getPosY() - floor2.getHeight());
//#
//# 			predioDistX = 2;
//# 			predioDistY = 2;
//# 			predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight());
//#
//# 			if (palmeira != null) {
//# 				palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth(), floor2.getPosY() - palmeira.getHeight() + 2);
//# 			}
//#
//# 			mulherDistY = arcos.getPosY() + arcos.getHeight() / 2;
//# 			gringoDistY = arcos.getPosY() + arcos.getHeight() / 2 +3;
//# 			if (figurante1 != null) {
//# 				figurante1.setPosition(figurante1.getWidth(), mulherDistY);
//# 			}
//# 			if (figurante2 != null) {
//# 				figurante2.setPosition(ScreenManager.SCREEN_WIDTH - figurante2.getWidth(), gringoDistY);
//# 			}
//#
//# 			if (ambulante != null) {
//# 				final byte ambulantY = 13;
//#
//#                 if(!GameMIDlet.isLowMemory())
//# 				ambulante.setPosition(ScreenManager.SCREEN_WIDTH - ambulante.getWidth(), calcadaEsquerda.getPosY() - ambulante.getHeight() + ambulantY);
//# 				final byte nanoY = 45;
//# 				nanoX= 7;
//# 				if(!GameMIDlet.isLowMemory())
//# 				nano.setPosition(ambulante.getPosX()+ nanoX, ambulante.getPosY() + nanoY);
//# 			}
//#
//#
//# 		}
		//#elif SCREEN_SIZE == "SMALL"
//# 		    ajusteArcosY = 15;
//#
//# 			calcadaEsquerda.setSize(ScreenManager.SCREEN_WIDTH, calcadaEsquerda.getHeight());
//# 			calcadaEsquerda.setPosition(0, getHeight() - calcadaEsquerda.getHeight() );
//#
//# 			floor2.setSize(ScreenManager.SCREEN_WIDTH, floor2.getHeight());
//# 			floor2.setPosition(0, calcadaEsquerda.getPosY() - floor2.getHeight() );
//#
//# 			arcos.setSize(ScreenManager.SCREEN_WIDTH, arcos.getHeight());
//# 			arcos.setPosition(0, floor2.getPosY() - arcos.getHeight() );
//#
//# 			bonde.setPosition(-5 * bonde.getWidth(), arcos.getPosY() - bonde.getHeight());
//#
//#
//# 			predioDistX = 2;
//# 			predioDistY = 2;
//# 			if(!GameMIDlet.isLowMemory())
//#  			predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight());
//#
//# 			if ( nuvem != null )
//# 				nuvem.setPosition(ScreenManager.SCREEN_HALF_WIDTH / 2, arcos.getPosY() - 2 * nuvem.getHeight());
//#
//# 			alsfaltoDistY = calcadaEsquerda.getPosY() + 4;
//#
//# 			if ( palmeira != null )
//# 				palmeira.setPosition(ScreenManager.SCREEN_WIDTH - palmeira.getWidth(), floor2.getPosY() - palmeira.getHeight() + 2);
//#
//# 			mulherDistY = arcos.getPosY() + arcos.getHeight() / 2 -10;
//# 			mulherDistX = 50;
//# 			gringoDistY = arcos.getPosY() + arcos.getHeight() / 2-10 ;
//# //		}
		//#endif
		
		if ( morro == null ) {
			if ( predio != null )
				predio.setPosition(getWidth() >> 1, floor2.getPosY() - predio.getHeight());
		} else {
			//#if SCREEN_SIZE == "SUPER_BIG"

 			morro.setPosition(-62, arcos.getPosY() + arcos.getHeight() - morro.getHeight());

 			if (predio != null) {
 				if (height > maxHeight) {
 					predioDistX = 25;
 					predioDistY = 15;
 					predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight() - predioDistY);

 				}
 			}
				//#else
//			morro.setPosition(0, arcos.getPosY() + arcos.getHeight() - morro.getHeight());
//			if (predio != null) {
//				if (height > maxHeight) {
//					predioDistX = 25;
//					predioDistY = 15;
//					predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight() - predioDistY);
//
//				}
//			} else {
//				if (height <= maxHeight) {
//					predio.setPosition(morro.getPosX() + morro.getWidth() + predio.getWidth() / 3, floor2.getPosY() - predio.getHeight());
//				}
//			}
//
			//#endif
			if (predio != null) {
				if (height > maxHeight) {
					predioDistX =10;
					predio.setPosition(ScreenManager.SCREEN_WIDTH - predio.getWidth() - predioDistX, floor2.getPosY() - predio.getHeight() );
					/*plateia.setVisible(false);
					figurante1.setVisible(false);
					figurante2.setVisible(false);*/

				}
			} else {
				if (height <= maxHeight) {
					predio.setPosition(morro.getPosX() + morro.getWidth() + predio.getWidth() / 3, floor2.getPosY() - predio.getHeight());
				}
			}
		}

//#if BLACKBERRY_API == "true"
//# 		plateia.setPosition(0, arcos.getPosY() + (arcos.getHeight() >> 1) + CROWD_OFFSET_Y);
		//#else

		plateia.setPosition(0, arcos.getPosY() + (arcos.getHeight() >> 1) + CROWD_OFFSET_Y);
//#endif

//#if SCREEN_SIZE == "SUPER_BIG"
 		plateia2.setPosition(-100, plateia.getPosY()+plateia.getHeight()/3);
 		if(height<480){
 		plateia2.setVisible(false);
 		}
//#endif

		plateia.setSize( width, plateia.getFill().getHeight() );

		if ( GameMIDlet.isLowMemory() ) {
			bkgSky.setSize(width, arcos.getPosY() + arcos.getHeight() );
		} else {
			bkgSky.setSize(width, bkgSky.getFill().getHeight());
//			//#if SCREEN_SIZE != "SUPER_BIG" && SCREEN_SIZE == "BIG"
//			if (height > maxHeight) {
//				bkgSky.setSize(width, height - floor2.getHeight()-floor2Aux.getHeight()-floor_1.getHeight() - calcadaDireita.getHeight());
//			}
//			//#endif
			bkgSky.setPosition( 0, floor2.getPosY() - bkgSky.getHeight() );
		}

		// pode ser necessário preencher o topo da tela com uma cor sólida
		bkgSkyTop.setSize(width, bkgSky.getPosY());
		bkgBottom.setPosition(0, arcos.getPosY() + arcos.getHeight());
		bkgBottom.setSize(width, calcadaEsquerda.getPosY() - bkgBottom.getPosY() );
	}

	
	//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM"
//		public final void setPosAmb(int pos) {
//			if ( ambulante != null ) {
//				direction = pos;
//				short nanoXDiff = 2;
//				switch (pos) {
//					case DIRECTION_RIGHT:
//						ambulante.mirror(TRANS_MIRROR_H);
//						ambulante.setPosition(-ambulante.getWidth(), ambulante.getPosY());
//						nano.setPosition(ambulante.getPosX() + ambulante.getWidth() - nano.getWidth() - nanoX - nanoXDiff, nano.getPosY());
//						break;
//
//					case DIRECTION_LEFT:
//						ambulante.mirror(TRANS_MIRROR_H);
//						ambulante.setPosition(ScreenManager.SCREEN_WIDTH + ambulante.getWidth(), ambulante.getPosY());
//						nano.setPosition(ambulante.getPosX() + nanoX, nano.getPosY());
//						break;
//				}
//			}
//		}
	//#endif



	
	public final void update(int delta) {
		try{
		super.update(delta);
		final int diffX = speedX.updateInt(delta);
		bonde.move(diffX, 0);

		if (bonde.getPosX() > ScreenManager.SCREEN_WIDTH) {
			bonde.setPosition(-5 * bonde.getWidth(), bonde.getPosY());
		}
		
		if ( nuvem != null ) {
			final int diffNuv = speedXNuv.updateInt(delta);
			nuvem.move(diffNuv, 0);
			if (nuvem.getPosX() > ScreenManager.SCREEN_WIDTH) {
				nuvem.setPosition(-nuvem.getWidth(), nuvem.getPosY());
			}
			//#if SCREEN_SIZE =="SUPER_BIG"
 			nuvem2.move(diffNuv, 0);
 			nuvem3.move(diffNuv, 0);
 			if (nuvem3.getPosX() > ScreenManager.SCREEN_WIDTH) {
 				nuvem3.setPosition(-nuvem3.getWidth(), nuvem3.getPosY());
 			}
 			if (nuvem3.getPosX() > ScreenManager.SCREEN_WIDTH) {
 				nuvem2.setPosition(-nuvem2.getWidth(), nuvem2.getPosY());
 			}
		//#endif
		}

		//#if SCREEN_SIZE == "BIG" || SCREEN_SIZE =="MEDIUM"
//			if ( ambulante != null ) {
//				final int diffAmb = speedXAmbu.updateInt(delta);
//				if (ambulante.getPosX() + ambulante.getWidth() < -4 * ambulante.getWidth()) {
//					setPosAmb(DIRECTION_RIGHT);
//				} else if (ambulante.getPosX() > ScreenManager.SCREEN_WIDTH + 4 * ambulante.getWidth()) {
//					setPosAmb(DIRECTION_LEFT);
//				}
//
//				switch (direction) {
//					case DIRECTION_RIGHT:
//						nano.move(diffAmb, 0);
//						ambulante.move(diffAmb, 0);
//						break;
//
//					case DIRECTION_LEFT:
//						ambulante.move(-diffAmb, 0);
//						nano.move(-diffAmb, 0);
//						break;
//				}
//			}
		//#endif
	}
	catch (Throwable t) {
			GameMIDlet.setScreen(SCREEN_MAIN_MENU);
		}
	}


	public final void sizeChanged(int width, int height) {
		if ( width != getWidth() || height != getHeight() ){
			setSize(width, height);
		}
	}

    public void hideNotify(boolean deviceEvent) {
    }

    public void showNotify(boolean deviceEvent) {
    }
}
