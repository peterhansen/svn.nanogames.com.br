/**
 * PlayScreen.java
 */
package screens;

//#if TOUCH == "true"
import br.com.nanogames.components.userInterface.PointerListener;
//#endif

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.RichLabel;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.SpriteListener;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.basic.BasicAnimatedPattern;
import br.com.nanogames.components.online.RankingScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.MediaPlayer;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.PaletteMap;
import core.Constants;
import core.GameMIDlet;
import game.LifeBarListener;
import game.PushItBar;
import game.PushItBarListener;
import game.ScoreBar;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 * ©2007 Nano Games
 * @author Daniel L. Alves
 */
public final class PlayScreen extends UpdatableGroup implements KeyListener, Constants, ScreenListener
		//#if TOUCH == "true"
		, PointerListener
		//#endif
{
	//<editor-fold defaultstate="collapsed" desc="Posicionamento dos elementos da tela de jogo">

	//#if SCREEN_SIZE == "BIG"

//		/** Distância em pixels do label dos hits para o canto direito da tela */
//		private final byte FEEDBACKS_HITS_DIST_FROM_RIGHT = 15;
//
//        private final byte LABELHITS_HITS_DIST_FROM_BAR = 5;
//
//        private final byte BONUS_DIST_RIGHT = 5;
//
//        private final byte INTERFACEX = 6;
//
//        private final byte SCOREBAR_DIST = 6;
//
//		private final byte SCOREBAR_DISTY = 8;
//
//        private final byte FEEDBACKS_RESULT_DISTY = 10;
//
//        private final byte FEEDBACKS_RESULT_DISTX = 40;
//
//        private final byte INTERFACE_5_DIST_Y = -1;
//
//        private final byte FUNK_Y = 15;
//
//
// 	    private final byte fbPosX =15;
//
//        private final byte fbPosY =40;
//
//	    private final byte lifeBarx =1;
//
//        private final byte lifeBarY =0;
	//#elif SCREEN_SIZE == "MEDIUM"
//# 
//# 		/** Distância em pixels da barra dos pushIts para o topo da tela */
//# 		private final byte PUSHIT_BAR_DIST_FROM_TOP = 20;
//# 
//# 		/** Distância em pixels do label dos hits para o canto direito da tela */
//# 		private final byte FEEDBACKS_HITS_DIST_FROM_RIGHT = 15;
//# 
//#         private final byte LABELHITS_HITS_DIST_FROM_BAR = 5;
//# 
//# 		/** Distância em pixels das imagens de feedback de acerto para o label dos hits */
//# 		private final byte FEEDBACK_IMGS_DIST_FROM_HITS_LB = 3;
//# 
//#         private final byte SCORE_LABELX = 3;
//# 
//#         private final byte BONUS_DIST_RIGHT = 5;
//# 
//#         private final byte INTERFACEX = 6;
//# 
//# 		 private final byte INTERFA_2_CEX = 0;
//# 
//#         private final byte SCOREBAR_DIST = 3;
//# 
//# 		private final byte SCOREBAR_DISTY = 6;
//# 
//#         private final byte FEEDBACKS_RESULT_DISTY = 5;
//# 
//#         private final byte FEEDBACKS_RESULT_DISTX = 20;
//# 
//#         private final byte INTERFACE_5_DIST_Y = 0;
//# 
//#         private final byte SKULL_Y = 3;
//# 
//# 		private final byte fbPosX =20;
//# 
//#         private final byte fbPosY =20;
//# 
//# 		private final byte lifeBarx =2;
//# 
//# 		private final byte lifeBarY =-1;
//# 
	//#elif SCREEN_SIZE == "SMALL"
//# 	/** Distância em pixels do label dos hits para o canto direito da tela */
//# 	private final byte FEEDBACKS_HITS_DIST_FROM_RIGHT = 8;
//# 	private final byte LABELHITS_HITS_DIST_FROM_BAR = 3;
//# 	private final byte BONUS_DIST_RIGHT = 1;
//# 	private final byte INTERFACEX = 6;
//# 	private final byte SCOREBAR_DIST = 3;
//# 	private final byte SCOREBAR_DISTY = 3;
//# 	private final byte FEEDBACKS_RESULT_DISTY = 10;
//# 	private final byte FEEDBACKS_RESULT_DISTX = 40;
//# 	private final byte INTERFACE_5_DIST_Y = 0;
//# 	private final byte SKULL_Y = 8;
//# 	private final byte fbPosX = 15;
//# 	private final byte fbPosY = 40;
//# 	private final byte lifeBarx = 4;
//# 	private final byte lifeBarY = -2;
//#     private final byte INTERFA_2_CEX = 2;
//#
	//#elif SCREEN_SIZE == "SUPER_BIG"
 	/** Distância em pixels do label dos hits para o canto direito da tela */
 	private final byte FEEDBACKS_HITS_DIST_FROM_RIGHT = 15;
 	private final byte LABELHITS_HITS_DIST_FROM_BAR = 5;
 	private final byte BONUS_DIST_RIGHT = 5;
 	private final byte INTERFACEX = 6;
 	private final byte SCOREBAR_DIST = 10;
 	private final byte SCOREBAR_DISTY = 9;
 	private final byte FEEDBACKS_RESULT_DISTY = 10;
 	private final byte FEEDBACKS_RESULT_DISTX = 40;
 	private final byte INTERFACE_5_DIST_Y = 0;
 	private final byte FUNK_Y = 15;
 	private final byte fbPosX = 15;
 	private final byte fbPosY = 40;
 	private final byte lifeBarx = 1;
 	private final byte lifeBarY = 1;
	//#endif

	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="Estados do jogo">
	// Estados do jogo
	public static final byte STATE_NONE = -1;
	public static final byte STATE_RUNNING = 0;
	public static final byte STATE_PAUSED = 1;
	public static final byte STATE_LEVEL_ENDED = 2; // Retira alguns elementos da tela de jogo
	public static final byte STATE_RESULT_SCREEN = 3;
	public static final byte STATE_RETURN_RESULT_SCREEN = 4;
	public static final byte STATE_BEGIN_LEVEL = 5;
	public static final byte STATE_TOUCH_INTRO = 6; // Explica como jogar em aparelhos touch screen
	public static final byte STATE_GAME_OVER = 7;
	public static final byte STATE_NEW_RECORD = 8;
	/** Estado atual do jogo */
	private byte currState;
	/** Estado anterior ao atual. Necessário para voltarmos do estado STATE_PAUSED */
	private byte prevState;

	//</editor-fold>
	/** Fase atual do jogo */
	private short level;
	private final RichLabel labelMessage;
	//#if SCREEN_SIZE == "SUPER_BIG"
 	private final DrawableImage imgX;
	//#endif
	/** Espaçamento da fonte dos hits */
	//#if SCREEN_SIZE == "MEDIUM" || SCREEN_SIZE == "BIG"
//		private final byte FONT_HITS_CHAR_OFFSET = -1;
	//#else
//# 	private final byte FONT_HITS_CHAR_OFFSET = -2;
	//#endif
	private final byte PLAYSCREEN_AWAY_INDEX = 0;
	
	/** Personagem */
	private final Sprite dancer;
	protected BkgLapa bkgLapa;
	private BkgSematary bkgSematary;
	public static byte type;
	protected final LifeBar lifeBar;
	private final int MED_MIN = 25;
	private final int MED_MAX = 80;
	private static PaletteMap map[];
	private int timeLife = 0;
	private int maxTime = 1000;
	private final short TIME_X = 300;
	private int timeX;
	private int stateHit;
	private final int ACERTANDO = 0;
	private final int ERRANDO = 1;
	private final DrawableGroup feedBackGroup;
	private final Label fbPerfeito;
	private final Label fbOtimo;
	private final Label fbBom;
	private final Label fbmedio;
	private final Label fbruim;
	private final Label fbErrou;
	private final DrawableImage interface1;
	private final Pattern interface2;
	private final DrawableImage interface3;
	private final Pattern interface4;
	private final DrawableImage interface5;
	/** Alguma tecla foi pressionada ?  sabe dançar? **/
	private boolean dancing;
	private byte dancerState;
	private final byte STATE_DANCING = 0;
	private final byte STATE_WAITING = 1;

	/* De acordo com o parametro recebido no contrutor , altera-se a dificuldade do jogo */
	protected static byte difficulty;

	/* Variaveis de Tempo em que o personagem permanece dançando apos receber o ultimo comando */
	private final int TIME_KEEP_DANCING = 3000; // milisegundos
	private int time; // tempo incrementado
	private static final short TIME_TRANSITION = 5000;
	private short transitionTime;
	/** Barra que contém os botões a serem apertados */
	private final PushItBar pushItBar;
	/** Conta quantos hits ( acertos consecutivos ) o jogador está fazendo */
	private short hitCount;
	/** Conta o número de aceertos totais da fase */
	private short levelHits;
	/** Multiplicador de pontos atual */
	private short bonusMultiplier;
	/** Indica quando devemos aumentar o multiplicador bônus */
	private short bonusMultiplierCounter;
	/** Label que exibe o número de hits atual */
	private final Label labelHits;
	/** Label que exibe o bônus multiplicador de pontos */
	private final Label labelBonusMultiplier;
	/** Barra que exibe os pontos do jogador */
	private final ScoreBar scoreBar;
	/** Parte do label de bônus que nunca muda */
	private final String bonusStr;
	/** Número de botões exibidos na tela na versão touch screen */
	private final byte N_TOUCH_SCREEN_BUTTONS = 5;
	/** Armazena o tipo do último acerto. Auxilia na contagem de pontos dos rastros */
	private byte lastHitStatus = PushItBar.PUSHIT_HIT_MISS;
	/** Controla o tempo de visibilidade das imagens que indicam o tipo acerto */
	private short feedBackCounter;
	/** Tempo de duração da tela que indica se o jogador perdeu ou passou para a próxima fase */
	private final short RESULT_SCREEN_DUR = 3000;
	private final ResultScreen resultScreen;
	//#ifndef NO_SOUND
	/** Som que está sendo tocado na fase atual */
	private byte currSoundIndex;
	private int fp_currentSoundTempo;
	//#endif

	// Próxima sequência a ser executada pelo Away
	private byte awayNextSeq = -1;
	private final MUV barMUV;

	//#if TOUCH == "true"
	/** Possíveis retornos de peterTriangleMethod() */
	private final byte TRIANGLE_NONE = -1; // Inicialização
	private final byte TRIANGLE_TOP = 0;
	private final byte TRIANGLE_LEFT = 1;
	private final byte TRIANGLE_RIGHT = 2;
	private final byte TRIANGLE_BOTTOM = 3;
	private final byte TRIANGLE_STAR = 4; // Nome feio mas que serve ao seu propósito
	private byte lastPressed = TRIANGLE_NONE;

	// Tratam a animação das áreas da tela de toque
	private final short TOUCH_SCREEN_AREA_COLOR_TIME = 100;
	private final byte TOUCH_SCREEN_AREA_N_COLORS = 6;//6
	private final short[] counterTsAreaColor = {-1, -1, -1, -1};
	private final int tsAreaColors[][] = new int[][]{
		{0xFFFFDF05, 0xFFFFE90E, 0xFFFFF605, 0xFFFFFD33, 0xFFFFFB86, 0xFFFFFDC6},
		{0xFF0072FF, 0xFF0099FF, 0xFF00BDFF, 0xFF00CFFF, 0xFF8BE2FF, 0xFFC5F1FF},
		{0xFFDE5E00, 0xFFFF6C00, 0xFFFF8A00, 0xFFFFA200, 0xFFFFC869, 0xFFFCE498},
		{0xFF049A01, 0xFF04B900, 0xFF05CE00, 0xFF37E23F, 0xFF84FF8A, 0xFFCBFFD2}
	};
	private static final short TIME_TOUCH_INTRO = 3200;
	private static final short TIME_TOUCH_INTRO_STEP = 250;
	private short colorTime;
	private byte touchScreenColorIndex;
	private final boolean hasPointerEvents = ScreenManager.getInstance().hasPointerEvents();
	/** Conserta um problema de sincronia de eventos */
	private boolean receivedPointerPressed;

	//#endif
	/** Construtor */
	public PlayScreen() throws Exception {
		// Inicializa o grupo
		super(25);
		bonusStr = GameMIDlet.getText(TEXT_BONUS);

		/** inicializando os bkgs**/
		//#if SCREEN_SIZE == "SUPER_BIG"
 			imgX = new DrawableImage(ROOT_DIR + "x.png");
		//#endif
		resultScreen = new ResultScreen();

		/* de acordo com o indice , aloca-se um personagem diferente */
		switch (ChooseDancerScreen.getDancerIndex()) {
			case INDEX_DANCER_FUNK:
				bkgLapa = BkgLapa.getInstance();
				insertDrawable(bkgLapa);
				//#if SCREEN_SIZE == "SMALL"
//# 						dancer = new Sprite( ROOT_DIR + "Shu/funkeira", map );
//# 						dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight() );
				//#elif SCREEN_SIZE == "MEDIUM"
//# 					if ( GameMIDlet.isLowMemory() ){
//# 					dancer = new Sprite( ROOT_DIR + "fs/funkeira", map );
//# 					}
//# 					else{
//# 					dancer = new Sprite( ROOT_DIR + "Shu/funkeira", map );}
//# 					dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight() );
				//#elif SCREEN_SIZE == "BIG"
//				switch (GameMIDlet.getVendor()) {
//					case GameMIDlet.VENDOR_SAMSUNG:
//					case GameMIDlet.VENDOR_LG:
//						if (Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT) {
//							dancer = new Sprite(ROOT_DIR + "ShuSamsung/funkeira", map);
//							dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight() + FUNK_Y);
//						} else {
//							dancer = new Sprite(ROOT_DIR + "Shu/funkeira", map);
//							dancer.defineReferencePixel(dancer.getWidth() / 3, dancer.getHeight());
//						}
//						break;
//					default:
//						dancer = new Sprite(ROOT_DIR + "Shu/funkeira", map);
//						dancer.defineReferencePixel(dancer.getWidth() / 3, dancer.getHeight());
//				}
				//#elif SCREEN_SIZE == "SUPER_BIG"
 				dancer = new Sprite(ROOT_DIR + "Shu/funkeira", map);
 				dancer.defineReferencePixel(ANCHOR_BOTTOM | ANCHOR_HCENTER);
				//#endif
				currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_FUNK;
				break;

			case INDEX_DANCER_SKULL:
				bkgSematary = new BkgSematary();
				insertDrawable(bkgSematary);
				//#if SCREEN_SIZE == "SMALL"
//#                if (GameMIDlet.isLowMemory()) {
//# 				   dancer = new Sprite(ROOT_DIR+"Caveira/caveirexlow.bin" ,ROOT_DIR + "Caveira/caveirex", map);
//# 				   dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight());
//# 				} else {
//# 					dancer = new Sprite(ROOT_DIR + "Caveira/caveirex", map);
//# 					dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight());
//# 				}
				//#elif SCREEN_SIZE == "MEDIUM"
//# 					if ( GameMIDlet.isLowMemory() ){
//# 					dancer = new Sprite( ROOT_DIR + "cs/caveirex", map );
//# 				}
//# 					else{
//# 					dancer = new Sprite( ROOT_DIR + "Caveira/caveirex", map );}
//# 					dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight() );
				//#elif SCREEN_SIZE == "BIG"
//				switch (GameMIDlet.getVendor()) {
//					case GameMIDlet.VENDOR_SAMSUNG:
//					case GameMIDlet.VENDOR_LG:
//						if (Runtime.getRuntime().totalMemory() < LOW_MEMORY_LIMIT) {
//							dancer = new Sprite(ROOT_DIR + "CaveiraSamsung/caveirex", map);
//						} else {
//							dancer = new Sprite(ROOT_DIR + "Caveira/caveirex", map);
//						}
//						break;
//					default:
//						dancer = new Sprite(ROOT_DIR + "Caveira/caveirex", map);
//				}
				//#elif SCREEN_SIZE == "SUPER_BIG"
 				dancer = new Sprite(ROOT_DIR + "Caveira/caveirex", map);
 				dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight());
				//#endif
				currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_SAMBA;
				break;

			default:
				throw new IllegalArgumentException();
		}
		insertDrawable(dancer);
		

		dancer.setListener(new SpriteListener() {

			public final void onSequenceEnded(int id, int sequence) {
				if (id == PLAYSCREEN_AWAY_INDEX) {
					if (sequence == AWAY_SEQ_VANISH) {
						dancer.pause(true);
					} else {
						int newSeq;
						if (awayNextSeq < 0) {
							// Sorteia a nova sequência
							newSeq = AWAY_SEQ_OPENING_LEGS + NanoMath.randInt(AWAY_SEQ_N_LEVEL_SEQS);
							if (newSeq == dancer.getSequenceIndex()) {
								++newSeq;
								if (newSeq > AWAY_SEQ_N_LEVEL_SEQS) {
									newSeq = AWAY_SEQ_OPENING_LEGS;
								}
							}
						} else {
							newSeq = awayNextSeq;
							awayNextSeq = -1;
						}
						if (dancerState == STATE_WAITING) {
							newSeq = AWAY_SEQ_STATIC;
						}
						dancer.setSequence(newSeq);
					}
				}
			}

			public final void onFrameChanged(int id, int frameSequenceIndex) {
			}
		}, PLAYSCREEN_AWAY_INDEX);

		// Aloca a barra dos pontos
		scoreBar = new ScoreBar();
		insertDrawable(scoreBar);

		// Aloca a barra dos pushIts
		pushItBar = new PushItBar(new PushItBarListener() {

			public final void onHit(byte hit, byte hitStatus) {
				if (currState != STATE_RUNNING) {
					return;
				}

				switch (hit) {
					case ON_HIT_FALSE:
						/*//#if SCREEN_SIZE=="SUPER_BIG"
						x.setPosition(PushItBar.aim.getPosX()+x.getWidth()/2, PushItBar.aim.getPosY()+PushItBar.aim.getHeight()-10);
						x.setVisible(true);
						////#endif*/
						// Cancela o multiplicador bônus
						bonusMultiplierCounter = 0;
						setBonusMultiplier(1);

						dancer.setSequence(AWAY_SEQ_COMPLAINING);

						// Cancela a contagem de hits
						hitCount = 0;
						labelHits.setText(Math.min(hitCount, MAX_HIT_COUNT) + "H");
						labelHits.setVisible(false);

						setHitState(ERRANDO);
						// Exibe a frase indicando o que aconteceu
						showFeedback(PLAY_FEEDBACK_MISS);
						resultScreen.increaseCounter(PLAY_FEEDBACK_MISS);

						//#if BLACKBERRY_API == "false"
						MediaPlayer.vibrate(DEFAULT_VIBRATION_TIME);
						//#endif


						break;

					case ON_HIT_FALSE_QM:
						// Se errou o ponto de interrogação, divide o multiplicador bônus atual por 2
						setBonusMultiplier(bonusMultiplier >> 1);

						// Se errou o ponto de interrogação personagem reclama
						dancer.setSequence(AWAY_SEQ_COMPLAINING);
						//setHitState(ERRANDO);
						//#if SCREEN_SIZE=="SUPER_BIG"
 							imgX.setPosition(PushItBar.aim.getPosX() + imgX.getWidth() / 2, PushItBar.aim.getPosY() + PushItBar.aim.getHeight() -7 );
 							imgX.setVisible(true);
						//#endif
						// Vibra
						MediaPlayer.vibrate(DEFAULT_VIBRATION_TIME);
						break;

					case ON_HIT_TRUE:
						// Incrementa o contador de acertos totais
						++levelHits;

						setHitState(ACERTANDO);
						timeLife = 0;

						/* personagem dança sempre que um HIT for certo*/
						dancing = true;
						dancerState = STATE_DANCING;

						// Armazena o tipo de acerto
						lastHitStatus = hitStatus;

						resultScreen.increaseCounter(PLAY_FEEDBACK_PERFECT + hitStatus);

						// Atualiza o label dos hits
						++hitCount;

						if (hitCount > 1) {
							labelHits.setText(Math.min(hitCount, MAX_HIT_COUNT) + "H");
							labelHits.setPosition(ScreenManager.SCREEN_WIDTH - labelHits.getWidth() - LABELHITS_HITS_DIST_FROM_BAR, labelHits.getPosY() );
							//#if SCREEN_SIZE == "MEDIUM"
//# 							labelHits.setPosition(PushItBar.aim.getPosX(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
							//#endif
							//#if SCREEN_SIZE == "SUPER_BIG"
 	//							labelHits.setPosition(PushItBar.aim.getPosX(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
							//#endif
							labelHits.setVisible(true);
						}

						// Atualiza a pontuação
						scoreBar.changeScore(((((100L - ((long) hitStatus * (long) PushItBar.UNDER_PERFECT_HIT_PERCENTAGE)) * (long) PushItBar.PERFECT_HIT_N_POINTS) / 100L) * (long) bonusMultiplier) + (long) hitCount);

						// Exibe a frase indicando o que aconteceu
						showFeedback((byte) (PLAY_FEEDBACK_PERFECT + hitStatus));

						// Verifica se deve aumentar o multiplicador bônus
						incBonusCounter(hitStatus);
						break;

					case ON_HIT_TRUE_QM:
						// Se acertou o ponto de interrogação, dobra o multiplicador bônus
						bonusMultiplierCounter = 0;
						dancing = true;
						dancerState = STATE_DANCING;
						setBonusMultiplier(bonusMultiplier << 1);
						break;

					case ON_HIT_TRUE_TRAIL:
						// Atualiza a pontuação
						scoreBar.changeScore(((((100L - ((long) lastHitStatus * (long) PushItBar.UNDER_PERFECT_HIT_PERCENTAGE)) * (long) PushItBar.PERFECT_HIT_N_POINTS) / 100L) * (long) bonusMultiplier) + (long) hitCount);

						// Verifica se deve aumentar o multiplicador bônus
						incBonusCounter(lastHitStatus);

						// Fica exibindo a frase indicando o que aconteceu enquanto o rastro durar
						showFeedback((byte) (PLAY_FEEDBACK_PERFECT + lastHitStatus));

						break;

					case ON_HIT_TRUE_SPECIAL_BOMB:
						setBonusMultiplier(bonusMultiplier >> 2);
						dancerState = STATE_WAITING;
						dancer.setSequence(AWAY_SEQ_COMPLAINING);
						break;

					case ON_HIT_TRUE_SPECIAL_BONUS:
						setBonusMultiplier(bonusMultiplier << 2);
						break;

					case ON_HIT_TRUE_ROULETTE:
						++hitCount;
						break;
					case ON_HIT_FALSE_ROULETTE:
						//#if SCREEN_SIZE=="SUPER_BIG"
 						imgX.setPosition(PushItBar.aim.getPosX() + imgX.getWidth() / 2, PushItBar.aim.getPosY() + PushItBar.aim.getHeight()-7 );
 						imgX.setVisible(true);
						//#endif
						break;
				}
			}

			public final void onLevelEnded() {
				if (currState == STATE_RUNNING) {
					// Retira alguns elementos da tela
					noFeedback();
					labelHits.setVisible(false);
					labelBonusMultiplier.setVisible(false);

					// Ver onSequenceEnded()
					dancing = true;
					dancerState = STATE_DANCING;
					awayNextSeq = AWAY_SEQ_VANISH;

					//#if TOUCH == "true"
					if (hasPointerEvents) {
						// Pára de desenhar as áreas do touchscreen
						for (int i = 0; i < counterTsAreaColor.length; ++i) {
							counterTsAreaColor[i] = -1;
						}
					}
					//#endif


					setState(STATE_LEVEL_ENDED);
				}
			}
		});
		insertDrawable(pushItBar);


		//#if SCREEN_SIZE == "SUPER_BIG"
 		barMUV = new MUV(-2 * pushItBar.getHeight());
		//#else
//		barMUV = new MUV( -pushItBar.getHeight() );
		//#endif

		// Aloca o label dos hits
		labelHits = new Label(GameMIDlet.getInstance().getFont(FONT_HITS), null);
		insertDrawable(labelHits);
		labelHits.setVisible(false);

		// Aloca o label do bônus
		labelBonusMultiplier = new Label(GameMIDlet.getInstance().getFont(FONT_BONUS), String.valueOf(MAX_BONUS_MULIPLIER + "x"));
		insertDrawable(labelBonusMultiplier);
		labelBonusMultiplier.setVisible(false);
		setBonusMultiplier(1);

		// Aloca as imagens de feedback do tipo de acerto
		fbPerfeito = new Label(GameMIDlet.getInstance().getFont(FONT_PERFECT), TEXT_MOVE_PERFECT);
		fbOtimo = new Label(GameMIDlet.getInstance().getFont(FONT_GREAT), TEXT_MOVE_GREAT);
		fbBom = new Label(GameMIDlet.getInstance().getFont(FONT_GOOD), TEXT_MOVE_GOOD);
		fbmedio = new Label(GameMIDlet.getInstance().getFont(FONT_REGULAR), TEXT_MOVE_AVERAGE);
		fbruim = new Label(GameMIDlet.getInstance().getFont(FONT_BAD), TEXT_MOVE_BAD);
		fbErrou = new Label(GameMIDlet.getInstance().getFont(FONT_MISS), TEXT_MOVE_ERROR);

		lifeBar = new LifeBar(new LifeBarListener() {

			public final void manageBar(int life) {
				if (currState == STATE_RUNNING) {
					if (life <= 0 && lifeBar.getLifeShown() <= 0) {
						setState(STATE_LEVEL_ENDED);
					}
				}
			}
		});
		insertDrawable(lifeBar);
		
		interface1 = new DrawableImage(ROOT_DIR + "interface1.png");
		interface2 = new Pattern(new DrawableImage(ROOT_DIR + "interface2.png"));
		interface3 = new DrawableImage(ROOT_DIR + "interface3.png");
		interface4 = new Pattern(new DrawableImage(ROOT_DIR + "interface4.png"));
		interface5 = new DrawableImage(ROOT_DIR + "interface5.png");

		feedBackGroup = new DrawableGroup(PushItBar.PUSHIT_N_HIT_TYPES);
		feedBackGroup.insertDrawable(fbPerfeito);
		feedBackGroup.insertDrawable(fbOtimo);
		feedBackGroup.insertDrawable(fbBom);
		feedBackGroup.insertDrawable(fbmedio);
		feedBackGroup.insertDrawable(fbruim);
		feedBackGroup.insertDrawable(fbErrou);

		insertDrawable(interface1);
		insertDrawable(interface2);
		insertDrawable(interface3);
		insertDrawable(interface4);
		insertDrawable(interface5);
		//#if SCREEN_SIZE == "SUPER_BIG"
 		insertDrawable(imgX);
 		imgX.setVisible(false);
		//#endif

		fbPerfeito.setVisible(false);
		fbOtimo.setVisible(false);
		fbBom.setVisible(false);
		fbmedio.setVisible(false);
		fbruim.setVisible(false);
		fbErrou.setVisible(false);

		Thread.yield();

		insertDrawable(feedBackGroup);

		// Cria o label que irá exibir a mensagem informando se o jogador passou ou não de fase
		labelMessage = new RichLabel(GameMIDlet.getInstance().getFont(FONT_MENU), null/*, ScreenManager.SCREEN_WIDTH*/);
		insertDrawable(labelMessage);
		labelMessage.setVisible(false);
		Thread.yield();



		setSize(ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT);
		onNewLevel();
	//	GameMIDlet.log("PLAYSCREEN CONSTRUTOR");
	}

	private final void showMessage(String message) {
		labelMessage.setText("<ALN_H>" + message, true );
		labelMessage.setSize(ScreenManager.SCREEN_WIDTH, labelMessage.getTextTotalHeight());
		labelMessage.setPosition(0, (size.y - labelMessage.getHeight() >> 1));
		labelMessage.setVisible(true);

	//#if SCREEN_SIZE == "SMALL"
//# 		//	setInfoVisible( true );
	//#endif
	}

	/*protected void paint(Graphics g) {
	try {
	super.paint(g);
	} catch (Throwable t) {
	GameMIDlet.log("PAINT " + t.getMessage() + " " + t.getClass());
	GameMIDlet.setScreen(SCREEN_MAIN_MENU);
	}
	}*/
	public final ResultScreen getResultScreen() {
		return resultScreen;
	}

	public static final void setDifficulty(int difficulty) {
		PlayScreen.difficulty = (byte) difficulty;
	}

	public static final void setDancerImage(PaletteMap map[]) {
		PlayScreen.map = map;
	}

	private final int getMedMin() {
		return NanoMath.lerpInt(MED_MIN, MED_MAX, Math.min(getLevel(), MAX_DIFFICULTY_LEVEL) * 100 / MAX_DIFFICULTY_LEVEL);
	}

	/** Inicializa os membros da classe para o início da nova fase */
	public final void onNewLevel() throws Exception {

		resultScreen.setMin(getMedMin());
		if (level > 0) {
			if (resultScreen.getScorePercent() < getMedMin()) {
				setState(STATE_GAME_OVER);
				return;
			}
		}
		// Atualiza a fase atual
		++level;
		if (level > MAX_LEVEL) {
			level = MAX_LEVEL;
		}


//		currSoundIndex = ( byte ) ( ( currSoundIndex + 1 ) % SOUND_N_LEVEL_SOUNDS );

		//#if DEBUG == "true"
			System.out.println( "VELOCIDADE ATUAL DA MÚSICA: " + NanoMath.toString( fp_currentSoundTempo ) );
		//#endif

		dancerState = STATE_WAITING;
		levelHits = 0;
		lifeBar.reset();

		// Exibe os labels que devem estar visíveis
		if (hitCount > 1) {
			labelHits.setVisible(true);
		}
		labelBonusMultiplier.setVisible(bonusMultiplier > 1);


		dancer.setSequence(AWAY_SEQ_STATIC);
		dancer.pause(false);

		//#if TOUCH == "true"
		if (hasPointerEvents) {
			lastPressed = TRIANGLE_NONE;
			for (int i = 0; i < counterTsAreaColor.length; ++i) {
				counterTsAreaColor[i] = -1;
			}
		}
		//#endif

		// Posiciona as barras dos pushIts e dos pontos corretamente
		// OBS: Vai acontecer um reposicionamento desnecessário apenas na primeira fase, quando estivermos vindo
		// diretamente do construtor. No entanto, não causa perda de desempenho no decorrer do jogo
		scoreBar.setPosition(SCOREBAR_DIST, interface1.getHeight() - scoreBar.getHeight() - SCOREBAR_DISTY);
		lifeBar.setPosition(lifeBarx, lifeBarY);
		interface1.setPosition(0, 0);
		interface2.setPosition(interface2.getPosX() , 0);
		interface3.setPosition(interface3.getPosX(), 0);
		interface4.setPosition(interface4.getPosX() , 0);
		interface5.setPosition(interface5.getPosX(), INTERFACE_5_DIST_Y);


		//#if SCREEN_SIZE == "SUPER_BIG"
 			pushItBar.setPosition(0, interface3.getHeight() - INTERFACEX);
 //			pushItBar.aim.setPosition(pushItBar.aim.getPosX(), INTERFACEX);
 			pushItBar.setSize(pushItBar.getWidth(),100);
		//#else
//		pushItBar.setPosition(0, interface3.getHeight() - SCOREBAR_DIST);
//		#endif

		//#if TOUCH == "true"
		setState((level <= 1 && hasPointerEvents) ? STATE_TOUCH_INTRO : STATE_BEGIN_LEVEL);
	//#else
//# 			setState( STATE_BEGIN_LEVEL );
	//#endif
	}

	public final int getLevel() {
		return level;
	}

	/** Chama a tela do menu de pausa */
	private synchronized final void pause() {
		if (currState != STATE_PAUSED) {
			setState(STATE_PAUSED);
			GameMIDlet.setScreen(GameMIDlet.SCREEN_PAUSE);
		}
	}

	/** Retoma o jogo */
	public synchronized final void unpause() {
		// Não chama setState( prevState ) pois repetiríamos animações
		if (currState == STATE_PAUSED) {
			currState = prevState;

			playMusic();

			//#if TOUCH == "true"
			receivedPointerPressed = false;
		//#endif
		}
	}

	private final void playMusic() {
		if (!MediaPlayer.isPlaying()) {
			//#if SCREEN_SIZE == "SMALL"
//# 				if ( GameMIDlet.isLowMemory() )
//# 					MediaPlayer.play( currSoundIndex, MediaPlayer.LOOP_INFINITE );
//# 				else
//# 					MediaPlayer.play( currSoundIndex, MediaPlayer.LOOP_INFINITE, fp_currentSoundTempo );
			//#else
            
            //#if JAVA_VERSION == "ANDROID"
                MediaPlayer.play(currSoundIndex, MediaPlayer.LOOP_INFINITE );
            //#else
//#                MediaPlayer.play(currSoundIndex, MediaPlayer.LOOP_INFINITE, fp_currentSoundTempo);
            //#endif
		//#endif
		}
	}

	/** Obtém a apontuação atual do jogo */
	public final long getScore() {
		return scoreBar.getScore();
	}

	public final void update(int delta) {
		/*if (larguraux != larguraAtual || altutaux != altutaAtual) {
		sizeChanged(larguraAtual, altutaAtual);
		}*/
		//#if TOUCH == "true"
		if (hasPointerEvents) {
			updateTouchScreenAreaColor(delta);
		}
		//#endif
		switch (currState) {
			case STATE_RUNNING:
				/*tempo em que o personagem permavece dançando após o ultimo comando " Funciona =P " */
				switch (dancerState) {
					case STATE_DANCING:
						time += delta;
						if (time >= TIME_KEEP_DANCING) {
							dancerState = STATE_WAITING;
							time = 0;
							break;
						}
				}

				switch (stateHit) {
					case ERRANDO:
						timeLife += delta;
						break;
					case ACERTANDO:
						timeLife = 0;
						break;
				}
				//#if SCREEN_SIZE=="SUPER_BIG"
 				if (imgX.isVisible()) {
 					timeX += delta;
 					if (timeX >= TIME_X) {
 						imgX.setVisible(false);
 						timeX = 0;
 					}
 				}
				//#endif
				updateLevel(delta);
				break;

			case STATE_BEGIN_LEVEL:
				updateLevel(delta);
				transitionTime -= delta;
				if (transitionTime <= TIME_TRANSITION) {
					showMessage(GameMIDlet.getText(TEXT_GET_READY) + String.valueOf(level));
				}
				if (transitionTime <= TIME_TRANSITION / 2) {
                    showMessage(GameMIDlet.getText(TEXT_MINIMUM) + " " + (getMedMin()) + "%");
				}
				if (transitionTime <= 0) {
					resultScreen.reset();
					MediaPlayer.stop();
					setState(STATE_RUNNING);
				}
				break;

			case STATE_GAME_OVER:
				updateLevel(delta);
				transitionTime -= delta;

				if (transitionTime <= 0) {
					if (GameMIDlet.isHighScore(getScore()) != RankingScreen.SCORE_STATUS_NONE) {
						setState(STATE_NEW_RECORD);
					} else {
						GameMIDlet.gameOver(getScore());
					}
				}
				break;

			case STATE_NEW_RECORD:
				updateLevel(delta);
				transitionTime -= delta;
				if (transitionTime <= 0) {
					GameMIDlet.gameOver(getScore());
				}
				break;

			//#if TOUCH == "true"
			case STATE_TOUCH_INTRO:
				transitionTime += delta;
				colorTime += delta;

				if (colorTime >= TIME_TOUCH_INTRO_STEP) {
					colorTime %= TIME_TOUCH_INTRO_STEP;

					switch (lastPressed) {
						case TRIANGLE_BOTTOM:
							lastPressed = TRIANGLE_LEFT;
							break;

						case TRIANGLE_LEFT:
							lastPressed = TRIANGLE_TOP;
							break;

						case TRIANGLE_RIGHT:
							lastPressed = TRIANGLE_BOTTOM;
							break;

						case TRIANGLE_TOP:
							lastPressed = TRIANGLE_RIGHT;
							break;

						case TRIANGLE_NONE:
							lastPressed = TRIANGLE_BOTTOM;
							break;
					}
				}

				if (transitionTime >= TIME_TOUCH_INTRO) {
					setState(STATE_BEGIN_LEVEL);
				}
				break;
			//#endif

			case STATE_LEVEL_ENDED:
				updateLevel(delta);

				final int dy = barMUV.updateInt(delta);
				if (dy == 0) {
					return;
				}
				pushItBar.move(0, dy);
				scoreBar.move(0, dy);
				lifeBar.move(0, dy);
				interface1.move(0, dy);
				interface2.move(0, dy);
				interface3.move(0, dy);
				interface4.move(0, dy);
				interface5.move(0, dy);

				if (pushItBar.getPosY() <= -(PushItBar.aim.getPosY() + PushItBar.aim.getHeight())) {
					if (lifeBar.getLife() > 0) {
						setState(STATE_RESULT_SCREEN);
					} else {
						setState(STATE_GAME_OVER);
					}
				}
				break;
		}
	}

	public static void setTyperanking(byte type) {
		PlayScreen.type = type;
	}


	//#if TOUCH == "true"
	protected final void paint(Graphics g) {
		super.paint(g);
		paintTouchScreenAreas(g);
	}
	//#endif

	private final void updateLevel(int delta) {
		// Atualiza os componentes do grupo
		super.update(delta);

		// Verifica se deve apagar a palavra de feedback
		if (feedBackCounter > 0) {
			feedBackCounter -= delta;
			if (feedBackCounter <= 0) {
				noFeedback();
			}
		}
	}


	//#if TOUCH == "true"
	private final void updateTouchScreenAreaColor(int delta) {
		if ((lastPressed >= TRIANGLE_TOP) && (lastPressed <= TRIANGLE_BOTTOM)) {
			counterTsAreaColor[lastPressed] -= delta;
			if (counterTsAreaColor[lastPressed] <= 0) {
				counterTsAreaColor[lastPressed] = TOUCH_SCREEN_AREA_COLOR_TIME;
				touchScreenColorIndex = (byte) ((touchScreenColorIndex + 1) % TOUCH_SCREEN_AREA_N_COLORS);
			}
		}
	}
	//#endif

	public synchronized final void setState(int state) {
		final byte stateBeforeChange = currState;
		currState = (byte) state;
		transitionTime = TIME_TRANSITION;
		labelMessage.setVisible(false);
		time = 0;

		switch (state) {
			case STATE_RUNNING:

				final short difficultyLevel = (short) Math.min(level + difficulty, MAX_DIFFICULTY_LEVEL);
				fp_currentSoundTempo = NanoMath.lerpFixed(FP_SOUND_MIN_TEMPO_MULTIPLIER, FP_SOUND_MAX_TEMPO_MULTIPLIER, NanoMath.divInt(difficultyLevel * 100, MAX_DIFFICULTY_LEVEL));

				try {
					pushItBar.onNewLevel(difficultyLevel);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				switch (ChooseDancerScreen.getDancerIndex()) {
					case INDEX_DANCER_FUNK:
						if (getLevel() % 2 == 1) {
							currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_FUNK;
						} else {
							currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_FUNK_2;

						}
						break;
					case INDEX_DANCER_SKULL:
						if (getLevel() % 2 == 1) {
							currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_SAMBA;
						} else {
							currSoundIndex = SOUND_INDEX_LEVEL_MUSIC_SAMBA_2;
						}
						break;
				}
				playMusic();
				switch (dancerState) {
					case STATE_WAITING:
						dancing = false;
						break;

				}
			case STATE_LEVEL_ENDED:
			case STATE_PAUSED:
				prevState = stateBeforeChange;
				break;

			case STATE_BEGIN_LEVEL:
				//#if TOUCH == "true"
				onPointerReleased(0, 0);
				//#endif

				if (level > 1) {
					MediaPlayer.play(SOUND_INDEX_NEW_LEVEL);
				}

				transitionTime = TIME_TRANSITION;

				break;

			case STATE_GAME_OVER:
				dancer.pause(true);
				transitionTime = TIME_TRANSITION;
				showMessage(GameMIDlet.getText(TEXT_GAMEOVER));
				MediaPlayer.play(SOUND_INDEX_GAME_OVER);
				break;

			case STATE_NEW_RECORD:
				if (type == RankingScreen.SCORE_STATUS_NEW_LOCAL_RECORD) {
					showMessage(GameMIDlet.getText(TEXT_NEW_RECORD));
				} else if ((type == RankingScreen.SCORE_STATUS_NEW_PERSONAL_RECORD)) {
					showMessage(GameMIDlet.getText(TEXT_NEW_PERSONAL_RECORD));
				} else if ((type == RankingScreen.SCORE_STATUS_NEW_PERSONAL_AND_LOCAL_RECORD)) {
					showMessage(GameMIDlet.getText(TEXT_NEW_PERSONAL_AND_LOCAL_RECORD));
				}
				type = RankingScreen.SCORE_STATUS_NONE;
				transitionTime = TIME_TRANSITION;
				break;

			case STATE_RESULT_SCREEN:
				MediaPlayer.play(SOUND_INDEX_EXPECTATION);
				GameMIDlet.setScreen(SCREEN_RESULT);
				break;

			//#if TOUCH == "true"
			case STATE_TOUCH_INTRO:
				transitionTime = 0;
				showMessage(GameMIDlet.getText(TEXT_TOUCH_SCREEN_INTRO));
				break;
			//#endif
		} // fim switch ( currState )
	}

	//#if TOUCH == "true"
	private final void paintTouchScreenAreas(Graphics g) {
		if (hasPointerEvents) {
			drawTouchScreenArea(g, lastPressed, false);
		}
	}

	//#endif
	//#if TOUCH == "true"
	public final void onPointerDragged(int x, int y) {
//			switch ( currState ) {
//				case STATE_BEGIN_LEVEL:
//				case STATE_RUNNING:
//					if ( receivedPointerPressed ) {
//						final byte currPressed = getPressedPointIndex( x, y );
//
//						if( currPressed != lastPressed )
//						{
//							lastPressed = TRIANGLE_NONE;
//							for( int i = 0 ; i < counterTsAreaColor.length ;++i )
//								counterTsAreaColor[i] = -1;
//
//							touchPointToButton( currPressed );
//						}
//					}
//				break;
//			}
		}

	// Verifica em qual triângulo da tela o ponto recebido como parâmetro está
	private final byte peterTriangleMethod(int x, int y) {
		final int pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();
		final int halfAreaHeight = pushItBarEnd + ((ScreenManager.SCREEN_HEIGHT - pushItBarEnd) >> 1);

		// whatTheHell:
		// x / y = H / W
		// Com regra de 3:
		// y <= ( x * H ) / W
		final boolean whatTheHell = NanoMath.min(y, ScreenManager.SCREEN_HEIGHT - y) <= (NanoMath.min(x, ScreenManager.SCREEN_WIDTH - x) * ScreenManager.SCREEN_HEIGHT) / ScreenManager.SCREEN_WIDTH;

		if (x < ScreenManager.SCREEN_HALF_WIDTH) {
			if (y > halfAreaHeight) {
				return whatTheHell ? TRIANGLE_BOTTOM : TRIANGLE_LEFT;
			} else {
				return whatTheHell ? TRIANGLE_TOP : TRIANGLE_LEFT;
			}
		} else {
			if (y > halfAreaHeight) {
				return whatTheHell ? TRIANGLE_BOTTOM : TRIANGLE_RIGHT;
			} else {
				return whatTheHell ? TRIANGLE_TOP : TRIANGLE_RIGHT;
			}
		}
	}

	private final byte getPressedPointIndex(int x, int y) {
		return peterTriangleMethod(x, y);
	}

	public final void onPointerPressed(int x, int y) {
		switch (currState) {
			case STATE_BEGIN_LEVEL:
			case STATE_RUNNING:
				receivedPointerPressed = true;
				touchPointToButton(getPressedPointIndex(x, y));
				break;
		}
	}

	private final void touchPointToButton(byte touchScreenAreaIndex) {
		lastPressed = touchScreenAreaIndex;

		for (int i = 0; i < counterTsAreaColor.length; ++i) {
			counterTsAreaColor[i] = -1;
		}

		switch (lastPressed) {
			case TRIANGLE_TOP:
			case TRIANGLE_LEFT:
			case TRIANGLE_RIGHT:
			case TRIANGLE_BOTTOM:
				touchScreenColorIndex = 0;
				counterTsAreaColor[lastPressed] = TOUCH_SCREEN_AREA_N_COLORS;
				pushItBar.keyPressed(ScreenManager.KEY_NUM2 + ((lastPressed - TRIANGLE_TOP) << 1));
				break;

			case TRIANGLE_STAR:
				pushItBar.keyPressed(ScreenManager.KEY_STAR);
				break;
		}
	}

	private final void drawTouchScreenArea(Graphics g, byte touchScreenAreaIndex, boolean fill) {
		//#if SCREEN_SIZE=="SUPER_BIG"
 		final byte POINT_SIZE = 10;
		//#else
//		final byte POINT_SIZE = 3;
		//#endif



		final int pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();
		final int halfAreaHeight = pushItBarEnd + ((ScreenManager.SCREEN_HEIGHT - pushItBarEnd) >> 1);

		switch (touchScreenAreaIndex) {
			case TRIANGLE_TOP:
				if (counterTsAreaColor[TRIANGLE_TOP] >= 0) {
					g.setColor(tsAreaColors[TRIANGLE_TOP][touchScreenColorIndex]);

					if (!fill) {
						drawTriangle(g, 0, pushItBarEnd, ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE);
					} else {
						g.fillTriangle(0, pushItBarEnd, ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight);
					}
				}
				break;

			case TRIANGLE_LEFT:
				if (counterTsAreaColor[TRIANGLE_LEFT] >= 0) {
					g.setColor(tsAreaColors[TRIANGLE_LEFT][touchScreenColorIndex]);

					if (!fill) {
						drawTriangle(g, 0, pushItBarEnd, 0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE);
					} else {
						g.fillTriangle(0, pushItBarEnd, 0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight);
					}
				}
				break;

			case TRIANGLE_RIGHT:
				if (counterTsAreaColor[TRIANGLE_RIGHT] >= 0) {
					g.setColor(tsAreaColors[TRIANGLE_RIGHT][touchScreenColorIndex]);

					if (!fill) {
						drawTriangle(g, ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE);
					} else {
						g.fillTriangle(ScreenManager.SCREEN_WIDTH, pushItBarEnd, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight);
					}
				}
				break;

			case TRIANGLE_BOTTOM:
				if (counterTsAreaColor[TRIANGLE_BOTTOM] >= 0) {
					g.setColor(tsAreaColors[TRIANGLE_BOTTOM][touchScreenColorIndex]);

					if (!fill) {
						drawTriangle(g, 0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight, POINT_SIZE);
					} else {
						g.fillTriangle(0, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT, ScreenManager.SCREEN_HALF_WIDTH, halfAreaHeight);
					}
				}
				break;
		}
	}

	private final void drawTriangle(Graphics g, int x0, int y0, int x1, int y1, int x2, int y2, int pointSize) {
		drawLine(g, x0, y0, x1, y1, pointSize);
		drawLine(g, x1, y1, x2, y2, pointSize);
		drawLine(g, x2, y2, x0, y0, pointSize);
	}

	private final void drawLine(Graphics g, int x0, int y0, int x1, int y1, int pointSize) {
		int change;
		for (int i = 0; i < pointSize; ++i) {
			change = ((i + 1) >> 1) * (-(i & 1));
			g.drawLine(x0 + change, y0, x1 + change, y1);
		}
	}

	public final void onPointerReleased(int x, int y) {
		lastPressed = TRIANGLE_NONE;
		for (int i = 0; i < counterTsAreaColor.length; ++i) {
			counterTsAreaColor[i] = -1;
		}

		pushItBar.keyReleased(0 /* Nesse caso, tanto faz o valor passado */);
	}

	//#endif
	public final PushItBar getPushItBar() {
		return pushItBar;
	}

	private final void incBonusCounter(byte hitStatus) {
		if (bonusMultiplier < MAX_BONUS_MULIPLIER) {
			final byte inc = (byte) (PERFECT_BONUS_COUNTER_INC - hitStatus);
			if (inc > 0) {
				bonusMultiplierCounter += inc;
				final int nextBonusMultiplier = getPATerm(BONUS_PA_A1, BONUS_PA_R, bonusMultiplier);
				if (bonusMultiplierCounter > nextBonusMultiplier) {
					bonusMultiplierCounter -= nextBonusMultiplier;
					setBonusMultiplier(bonusMultiplier + 1);
				}
			}
		}
	}

	private final int getPATerm(int a1, int r, int n) {
		return a1 + ((n - 1) * r);
	}

	/** Determina um novo multiplicador bônus */
	public final void setBonusMultiplier(int newBonusMultiplier) {
		if (newBonusMultiplier <= 1) {
			bonusMultiplier = 1;
			labelBonusMultiplier.setVisible(false);
		} else {
			if (newBonusMultiplier > MAX_BONUS_MULIPLIER) {
				newBonusMultiplier = MAX_BONUS_MULIPLIER;
			}

			if (bonusMultiplier != newBonusMultiplier) {
				bonusMultiplier = (short) newBonusMultiplier;
				labelBonusMultiplier.setVisible(true);
				labelBonusMultiplier.setText(bonusMultiplier + "x");
			}
		}
	}

	/** Exibe a frase de feedback recebida como parâmetro */
	private final void showFeedback(byte index) {
		noFeedback();
		feedBackCounter = FEEDBACK_DUR;

		switch (index) {
			case PLAY_FEEDBACK_PERFECT:
				fbPerfeito.setVisible(true);
				break;
			case PLAY_FEEDBACK_GREAT:
				fbOtimo.setVisible(true);
				break;
			case PLAY_FEEDBACK_GOOD:
				fbBom.setVisible(true);
				break;
			case PLAY_FEEDBACK_AVERAGE:
				fbmedio.setVisible(true);
				break;
			case PLAY_FEEDBACK_BAD:
				fbruim.setVisible(true);
				break;
			case PLAY_FEEDBACK_MISS:
				fbErrou.setVisible(true);
				break;
		}

	}

	/** Apaga o feedback visível no momento */
	private final void noFeedback() {
		fbPerfeito.setVisible(false);
		fbOtimo.setVisible(false);
		fbBom.setVisible(false);
		fbmedio.setVisible(false);
		fbruim.setVisible(false);
		fbErrou.setVisible(false);
	}

	public final void setSize(int width, int height) {
		super.setSize(width, height);

		lifeBar.setSize(size);
		feedBackGroup.setSize(width, height);
		int pushItBarEnd;
		int feedBackY;
		int feedBackX;
		int bonusY;
		int ajustInterface;
		int maxHeight = 320;

		if (width <= height) {

			interface3.setSize(interface3.getSize());

			//#if SCREEN_SIZE == "BIG"
//				ajustInterface = 9;
//				interface2.setSize(scoreBar.getWidth() - INTERFACEX, interface2.getHeight());
//				interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
//				interface4.setSize(ScreenManager.SCREEN_WIDTH / 4 + ajustInterface, interface4.getHeight());
			//#elif SCREEN_SIZE == "SUPER_BIG"
 			pushItBar.setSize(pushItBar.getWidth(), PushItBar.aim.getHeight() + 10);
 			pushItBar.setPosition(pushItBar.getPosX(), pushItBar.getPosY() + 20);


 			interface2.setSize(scoreBar.getWidth() - INTERFACEX, interface2.getHeight());
 			interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);

 			ajustInterface = 49;
 			interface4.setSize(ajustInterface, interface4.getHeight());
 			if (height > 600) {
 				ajustInterface = 60;
 				interface4.setSize(ajustInterface, interface4.getHeight());
 			}


 			switch (ChooseDancerScreen.getDancerIndex()) {
 			case INDEX_DANCER_FUNK:

 				break;
 		}

			//#elif SCREEN_SIZE == "MEDIUM"
//# 				ajustInterface = 1;
//# 				interface2.setSize(scoreBar.getWidth(), interface2.getHeight());
//# 				interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
//# 				interface4.setSize( ScreenManager.SCREEN_WIDTH / 5 - ajustInterface, interface4.getHeight() );
			//#elif SCREEN_SIZE == "SMALL"
//# 		     	ajustInterface =10;
//# 				interface2.setSize(scoreBar.getWidth() - INTERFA_2_CEX, interface2.getHeight());
//# 				interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
//# 				interface4.setSize( ScreenManager.SCREEN_WIDTH / 3 - ajustInterface, interface4.getHeight() );
			//#endif

			interface3.setPosition(interface2.getPosX() - INTERFACEX + interface2.getWidth(), 0);
			interface4.setPosition(interface3.getPosX() + interface3.getWidth(), 0);
			interface5.setPosition(interface4.getPosX() + interface4.getWidth(), INTERFACE_5_DIST_Y);

			pushItBar.setPosition(0, interface3.getHeight() - SCOREBAR_DIST);
			scoreBar.setPosition(0, interface1.getHeight() - scoreBar.getHeight());
			pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();

			//#if  SCREEN_SIZE =="BIG"
//				bonusY = 2;
//				labelHits.setPosition(PushItBar.aim.getPosX() - PushItBar.aim.getWidth(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
//				labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd + bonusY);

			//#elif  SCREEN_SIZE =="SUPER_BIG"
 			bonusY = 2;
 			labelHits.setPosition(PushItBar.aim.getPosX(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
 			labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd + bonusY);
 			imgX.setPosition(PushItBar.aim.getPosX(), PushItBar.aim.getPosY());
 			pushItBar.setPosition(getWidth() - pushItBar.getWidth(), interface3.getHeight() - SCOREBAR_DIST);
			//#elif SCREEN_SIZE == "MEDIUM"
//# 				bonusY = 1;
//# 				labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd + bonusY);
			//#else
//# 			bonusY = 2;
//# 			labelHits.setPosition(PushItBar.aim.getPosX(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
//# 			labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd - bonusY);
			//#endif

			feedBackY = labelBonusMultiplier.getPosY() + labelBonusMultiplier.getHeight();
			feedBackX = labelBonusMultiplier.getPosX();
			fbPerfeito.setPosition(feedBackX, feedBackY);
			fbOtimo.setPosition(feedBackX, feedBackY);
			fbBom.setPosition(feedBackX, feedBackY);
			fbmedio.setPosition(feedBackX, feedBackY);
			fbruim.setPosition(feedBackX, feedBackY);
			fbErrou.setPosition(feedBackX, feedBackY);
		} else {
			interface1.setSize(interface1.getSize());
			interface1.setPosition(0, 0);

			//#if SCREEN_SIZE == "BIG"
//			interface2.setSize(scoreBar.getWidth() - INTERFACEX, interface2.getHeight());
//			interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
//		    interface4.setSize( ScreenManager.SCREEN_WIDTH / 4, interface4.getHeight() );

			//#elif SCREEN_SIZE == "SUPER_BIG"

 			interface1.setSize(interface1.getWidth(), interface1.getHeight());
 			interface2.setSize(scoreBar.getWidth() - INTERFACEX, interface2.getHeight());
 			interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
 			interface3.setSize(interface3.getSize());
 			ajustInterface = 49;
 			interface4.setSize(ajustInterface, interface4.getHeight());
 			if (width > 600) {
 				ajustInterface = 60;
 				interface4.setSize(ajustInterface, interface4.getHeight());
 			}

			//#elif SCREEN_SIZE == "MEDIUM"
//# 			    ajustInterface =3;
//# 				interface2.setSize(scoreBar.getWidth() , interface2.getHeight());
//# 				interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
//# 				interface4.setSize( ScreenManager.SCREEN_WIDTH / 7+ajustInterface, interface4.getHeight() );
			//#elif SCREEN_SIZE == "SMALL"
//# 		    	ajustInterface =18;
//# 				interface2.setSize(scoreBar.getWidth() - INTERFA_2_CEX, interface2.getHeight());
//# 				interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
//# 			    interface4.setSize( ScreenManager.SCREEN_WIDTH / 3 - ajustInterface, interface4.getHeight() );
			//#endif

			interface3.setPosition(interface2.getPosX() - INTERFACEX + interface2.getWidth(), 0);
			interface4.setPosition(interface3.getPosX() + interface3.getWidth(), 0);
			interface5.setPosition(interface4.getPosX()+ interface4.getWidth(), INTERFACE_5_DIST_Y);

			pushItBar.setPosition(0, interface3.getHeight() - SCOREBAR_DIST);
			scoreBar.setPosition(SCOREBAR_DIST, interface1.getHeight() - scoreBar.getHeight());
			pushItBarEnd = pushItBar.getPosY() + pushItBar.getHeight();

			//#if  SCREEN_SIZE =="BIG"
//				bonusY = 70;
//				labelHits.setPosition(PushItBar.aim.getPosX() - PushItBar.aim.getWidth(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
//				labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd  );

			//#elif  SCREEN_SIZE =="SUPER_BIG"
 				bonusY = 70;
 				labelHits.setPosition( pushItBar.getPosX() + PushItBar.aim.getPosX(), pushItBar.getPosY() + PushItBar.aim.getPosY() + PushItBar.aim.getHeight() + 6 );
 				labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, getHeight()- 3* labelBonusMultiplier.getHeight());
 				imgX.setPosition(PushItBar.aim.getPosX(), PushItBar.aim.getPosY());
 				pushItBar.setPosition(getWidth() - pushItBar.getWidth(), interface3.getHeight() - SCOREBAR_DIST);
			//#elif SCREEN_SIZE == "MEDIUM"
//# 			bonusY = 50;
//# 			labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd + bonusY);
//# 
			//#else
//# 				bonusY = 7;
//# 				labelHits.setPosition(PushItBar.aim.getPosX(), interface4.getPosY() + interface4.getHeight() + LABELHITS_HITS_DIST_FROM_BAR);
//# 				labelBonusMultiplier.setPosition(BONUS_DIST_RIGHT, pushItBarEnd - bonusY);
			//#endif

			feedBackY = labelBonusMultiplier.getPosY() + labelBonusMultiplier.getHeight() * 3 / 2;
			feedBackX = labelBonusMultiplier.getPosX();
			fbPerfeito.setPosition(feedBackX, feedBackY);
			fbOtimo.setPosition(feedBackX, feedBackY);
			fbBom.setPosition(feedBackX, feedBackY);
			fbmedio.setPosition(feedBackX, feedBackY);
			fbruim.setPosition(feedBackX, feedBackY);
			fbErrou.setPosition(feedBackX, feedBackY);
		}
		//#if SCREEN_SIZE =="BIG" || SCREEN_SIZE == "SUPER_BIG"
			// caso o tamanho da tela seja modificado durante o jogo
			scoreBar.setPosition(SCOREBAR_DIST, interface1.getHeight() - scoreBar.getHeight() - SCOREBAR_DISTY);
			lifeBar.setPosition(lifeBarx, lifeBarY);
			interface1.setPosition(0, 0);
			interface2.setPosition(interface1.getPosX() + interface1.getWidth(), 0);
			interface3.setPosition(interface2.getPosX() - INTERFACEX + interface2.getWidth(), 0);
			interface4.setPosition(interface3.getPosX() + interface3.getWidth(), 0);
			interface5.setPosition(interface4.getPosX() + interface4.getWidth(), INTERFACE_5_DIST_Y);

		//#endif

		lifeBar.setSize(interface5.getPosX() + interface5.getWidth() - interface1.getPosX(), lifeBar.getHeight());


		if (bkgLapa != null) {
			bkgLapa.setSize(width, height);
		}
		if (bkgSematary != null) {
			bkgSematary.setSize(width, height);
		}

		switch (ChooseDancerScreen.getDancerIndex()) {
			case INDEX_DANCER_FUNK:
				//#if SCREEN_SIZE == "SUPER_BIG"
 					if (width < height) {
 						dancer.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, getHeight() - FUNK_Y * 3 / 2 );
 					} else {
 						dancer.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, getHeight() - FUNK_Y);
 					}

				//#elif SCREEN_SIZE == "SMALL" 
//# 				dancer.setRefPixelPosition((ScreenManager.SCREEN_WIDTH / 2), getHeight());
				//#elif SCREEN_SIZE  == "MEDIUM"
//# 				dancer.setRefPixelPosition((ScreenManager.SCREEN_WIDTH / 2), getHeight()- 3);
				//#else
//				if (width < height) {
//					dancer.setRefPixelPosition((ScreenManager.SCREEN_HALF_WIDTH), getHeight());
//				} else {
//					dancer.setRefPixelPosition((ScreenManager.SCREEN_HALF_WIDTH), getHeight()+ FUNK_Y);
//				}

				//#endif
			break;

			case INDEX_DANCER_SKULL:
				//#if SCREEN_SIZE == "BIG" 
//				dancer.defineReferencePixel(dancer.getWidth() / 2, dancer.getHeight());
//				dancer.setRefPixelPosition((ScreenManager.SCREEN_WIDTH/2 ) , getHeight());
				//#elif SCREEN_SIZE == "SUPER_BIG"
 					dancer.defineReferencePixel( dancer.getWidth() >> 1, dancer.getHeight());
 					dancer.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, getHeight() - ( width < height ? 15 : 0 ) );
				//#elif SCREEN_SIZE == "MEDIUM"
//# 				dancer.setRefPixelPosition((ScreenManager.SCREEN_WIDTH/2 ) , getHeight());
				//#elif SCREEN_SIZE == "SMALL"
//# 					dancer.setRefPixelPosition((ScreenManager.SCREEN_WIDTH/2 ) , getHeight());
//#
				//#endif
			break;
		}
	}

	
	public final void setHitState(int hitState) {
		stateHit = hitState;
		switch (stateHit) {
			case ERRANDO:
				if (timeLife >= maxTime) {
					lifeBar.changeLife(DANO_SEGUIDO);
				} else {
					lifeBar.changeLife(DANO);
				}
				break;

			case ACERTANDO:
				lifeBar.changeLife(LIFE_UP);
				break;
		}
	}


	public final void keyPressed(int key) {
		switch (currState) {
			case STATE_RUNNING:
				//GameMIDlet.log("STATE_RUNNING");
				switch (key) {
					case ScreenManager.UP:
						//GameMIDlet.log("UP");
						pushItBar.keyPressed(ScreenManager.KEY_NUM2);
						break;

					case ScreenManager.DOWN:
						//GameMIDlet.log("DOWN");
						pushItBar.keyPressed(ScreenManager.KEY_NUM8);
						break;

					case ScreenManager.LEFT:
						//	GameMIDlet.log("LEFT");
						pushItBar.keyPressed(ScreenManager.KEY_NUM4);
						break;

					case ScreenManager.RIGHT:
						//	GameMIDlet.log("RIGHT");
						pushItBar.keyPressed(ScreenManager.KEY_NUM6);
						break;

					case ScreenManager.FIRE:
						//	GameMIDlet.log("FIRE");
						pushItBar.keyPressed(ScreenManager.KEY_STAR);
						break;

					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_STAR:
					case ScreenManager.KEY_NUM5:
						//GameMIDlet.log("case ScreenManager.KEY_NUM2: 6 8 ...");
						pushItBar.keyPressed(key);
						break;

					case ScreenManager.KEY_BACK:
					case ScreenManager.KEY_CLEAR:
					case ScreenManager.KEY_SOFT_RIGHT:
						pause();
					break;
				}
			break;

			case STATE_GAME_OVER:
			case STATE_NEW_RECORD:
				if (transitionTime <= (TIME_TRANSITION >> 1)) {
					transitionTime = 0;
				}
			break;
		}
	//	GameMIDlet.log("keyPressed fim ");
	} // fim do método keyPressed( int )

	public final void keyReleased(int key) {

		switch (currState) {
			case STATE_RUNNING:
				switch (key) {
					// Não convertemos os direcionais, como em keyPressed, pois pushItBar.keyReleased() não faz uso
					// de key
					case ScreenManager.UP:
					case ScreenManager.DOWN:
					case ScreenManager.LEFT:
					case ScreenManager.RIGHT:
					case ScreenManager.FIRE:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM8:
					case ScreenManager.KEY_STAR:
						pushItBar.keyReleased(key);

				}
		}
	}

	public final void sizeChanged(int width, int height) {
		if (width != getWidth() || height != getHeight() ) {
			setSize(width, height);
		}
	}

    public void hideNotify(boolean deviceEvent) {
		switch (currState) {
			case STATE_RUNNING:
				GameMIDlet.setScreen(SCREEN_PAUSE);
				break;
		}
    }

    public void showNotify(boolean deviceEvent) {
    }
}