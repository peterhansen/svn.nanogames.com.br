/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;
import game.LifeBarListener;


/**
 *
 * @author Edson
 */
public final class LifeBar extends DrawableGroup implements Constants, Updatable {

	private static final byte BORDER_HEIGHT = 6;

	private final DrawableImage bar;

    private LifeBarListener l;

	private int life;

	private int lifeShown;

	private int maxWidth;

    PlayScreen playScreen;

	/** Velocidade de atualização do indicador de tempo. */
	private final MUV lifeSpeed = new MUV();


	public LifeBar(LifeBarListener l) throws Exception {
		super( 4 );
		bar = new DrawableImage( ROOT_DIR +"barritcha.png" );
		bar.setPosition( LIFEBAR_START_X, BORDER_HEIGHT );
		insertDrawable( bar );
        this.l = l ;
		reset();
        setSize(0, bar.getHeight());
	}


	public final void setLife( int life ) {
		lifeSpeed.setSpeed( ( life - lifeShown ) >> 1, false );
		this.life = NanoMath.clamp( life, 0, LIFE_MAX );
	}


	public final void changeLife( int lifeDiff ) {
		setLife( life + lifeDiff );
	}


	public final int getLife() {
		return life;
	}


	public final void reset() {
		lifeShown = 0;
		setLife( LIFE_MAX );
        bar.setVisible(true);
		update( 0 );
	}


	public final void update( int delta ) {
		if ( lifeShown != life ) {
			lifeShown += lifeSpeed.updateInt( delta );
			if ( ( lifeSpeed.getSpeed() > 0 && lifeShown >= life ) || ( lifeSpeed.getSpeed() < 0 && lifeShown <= life ) ) {
				lifeShown = life;
			}

			final int fp_percent = NanoMath.mulFixed( NanoMath.divInt( lifeShown, LIFE_MAX ), NanoMath.toFixed( 100 ) );
			final int FP_100 = NanoMath.toFixed( 100 );

			bar.setSize( NanoMath.toInt( NanoMath.divFixed( NanoMath.mulFixed( NanoMath.toFixed( maxWidth ), fp_percent ), FP_100 ) ), bar.getHeight() );
		}
        l.manageBar(getLife() );
    }


	public final void setSize(int width, int height) {
		super.setSize(width, height);

		maxWidth = (width - (BORDER_THICKNESS << 1) - AJUST_LIFE );
	}
	
	
	public final int getBarWidth() {
		return bar.getWidth();
	}


	public final int getLifeShown() {
		return lifeShown;
	}
}
