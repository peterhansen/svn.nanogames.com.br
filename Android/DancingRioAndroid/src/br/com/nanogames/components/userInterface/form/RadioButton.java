/**
 * RadioButton.java
 * 
 * Created on 6/Nov/2008, 17:15:49
 *
 */

package br.com.nanogames.components.userInterface.form;

import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.userInterface.form.events.Event;
import br.com.nanogames.components.userInterface.form.events.EventListener;
import br.com.nanogames.components.userInterface.form.layouts.FlowLayout;
import br.com.nanogames.components.util.Point;

/**
 *
 * @author Peter
 */
public class RadioButton extends Container {

	private final CheckBox[] checkBoxes;

	private byte checkedIndex = -1;


	public RadioButton( Button checkButton, ImageFont font, String[] entries ) throws Exception {
		this( checkButton, font, entries, 0 );
	}


	/**
	 *
	 * @param checkButton
	 * @param font
	 * @param entries
	 * @throws Exception
	 */
	public RadioButton( Button checkButton, ImageFont font, String[] entries, int defaultIndex ) throws Exception {
		super( entries.length );

		final byte totalEntries = ( byte ) entries.length;

		checkBoxes = new CheckBox[ totalEntries ];

		final FlowLayout flow = new FlowLayout( FlowLayout.AXIS_VERTICAL );
		flow.gap.y = 2;
		setLayout( flow );

		final EventListener checkBoxListener = new EventListener() {
			public final void eventPerformed( Event evt ) {
				if ( evt.eventType == Event.EVT_CHECKBOX_STATE_CHANGED ) {
					final CheckBox c = checkBoxes[ evt.source.getId() ];
					if ( isEnabled() ) {
						if ( c.isChecked() ) {
							setCheckedIndex( evt.source.getId() );
						} else {
							// se não houver opção selecionada, refaz a última seleção
							for ( byte i = 0; i < checkBoxes.length; ++i ) {
								if ( checkBoxes[ i ].isChecked() ) {
									return;
								}
							}
							// se chegou aqui, não há opção selecionada; refaz a seleção
							c.setChecked( true );
						}
					} else {
						// se o radio button estiver desabilitado, desfaz a alteração do check box
						c.setChecked( !c.isChecked(), false );
					}
				}
			}
		};

		for ( byte i = 0; i < totalEntries; ++i ) {
			final CheckBox checkBox = new CheckBox( new Button( checkButton ), font, entries[ i ] );
			checkBox.setId( i );
			checkBox.setChecked( false );
			checkBox.addEventListener( checkBoxListener );
			checkBoxes[ i ] = checkBox;
			insertDrawable( checkBox );
		}

		setCheckedIndex( defaultIndex );
	}


	public String getUIID() {
		return "radiobutton";
	}


	public void setCheckedIndex( int index ) {
		if ( isEnabled() && index != checkedIndex ) {
			if ( checkedIndex >= 0 && checkedIndex < checkBoxes.length )
				checkBoxes[ checkedIndex ].setChecked( false, false );

			checkedIndex = ( byte ) index;

			if ( index >= 0 && index < checkBoxes.length )
				checkBoxes[ index ].setChecked( true, false );
		}
	}


	public byte getCheckedIndex() {
		return checkedIndex;
	}


	public Point calcPreferredSize( Point maximumSize ) {
		// TODO borda reduz área útil do radio button, não cabendo mais o conteúdo
		if ( getBorder() == null )
			return super.calcPreferredSize( maximumSize );

		return super.calcPreferredSize( maximumSize ).add( getBorder().getBorderSize() ).add( 4, 4 );
	}


	public void setEnabled( boolean enabled ) {
		super.setEnabled( enabled );

		for ( byte i = 0; i < checkBoxes.length; ++i )
			checkBoxes[ i ].setEnabled( isEnabled() );
	}

	
}
