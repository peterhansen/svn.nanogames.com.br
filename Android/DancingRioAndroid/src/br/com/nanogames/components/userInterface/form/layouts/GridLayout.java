/**
 * GridLayout.java
 * 
 * Created on 22/Nov/2008, 11:46:46
 *
 */
package br.com.nanogames.components.userInterface.form.layouts;

import br.com.nanogames.components.userInterface.form.Component;
import br.com.nanogames.components.userInterface.form.Container;
import br.com.nanogames.components.util.Point;


/**
 *
 * @author Peter
 */
public class GridLayout extends Layout {

	private short rows;
	private short columns;


	/** 
	 * Creates a new instance of GridLayout with the given rows and columns
	 * 
	 * @param rows - the rows, with the value zero meaning any number of rows.
	 * @param columns - the columns, with the value zero meaning any number of 
	 * columns.
	 */
	public GridLayout( int rows, int columns ) {
		this.rows = ( short ) rows;
		this.columns = ( short ) columns;
	}


	/**
	 * @inheritDoc
	 */
	public void layoutContainer( Container container ) {
		int width = container.getLayoutWidth();
		int height = container.getLayoutHeight();
		int x = 0;
		int y = 0;
		final short numOfcomponents = container.getComponentCount();

		final int cmpWidth = ( width ) / columns;
		int cmpHeight = 0;
		
		// quantidade total de linhas
		final int N_ROWS = numOfcomponents > rows * columns ? ( ( numOfcomponents / columns ) + ( numOfcomponents % columns == 0 ? 0 : 1 ) ) : rows;
		int TOTAL_HEIGHT;
		
		if ( container.isScrollableY() && container.getScrollBarV() != null ) {
			for ( int i = 0; i < numOfcomponents; ++i ) {
				final Component c = container.getComponentAt( i );
				cmpHeight = Math.max( cmpHeight, c.getPreferredHeight() );
			}			
			
			// se a altura total das linhas for maior que a altura atual do contâiner, reserva espaço horizontal para a
			// barra de rolagem
			TOTAL_HEIGHT = cmpHeight * N_ROWS;
			if ( TOTAL_HEIGHT > container.getHeight() )
				width -= container.getScrollBarV().getWidth();
		} else {
			cmpHeight = height / N_ROWS;
			TOTAL_HEIGHT = cmpHeight * N_ROWS;
		}

		for ( int i = 0, row = 0; i < numOfcomponents; ++i ) {
			final Component c = container.getComponentAt( i );
			c.setSize( cmpWidth, cmpHeight );
			c.setPosition( x + ( i % columns ) * cmpWidth, y + row * cmpHeight );
			if ( ( i + 1 ) % columns == 0 ) {
				++row;
			}
		}
		container.setMaximumSize( new Point( width, TOTAL_HEIGHT ) ); // TODO testar
	}


	/**
	 * @inheritDoc
	 */
	public Point calcPreferredSize( Container parent, Point maxSize ) {
		int width = 0;
		int height = 0;

		final short numOfcomponents = parent.getComponentCount();
		for ( int i = 0; i < numOfcomponents; ++i ) {
			final Component c = parent.getComponentAt( i );
			width = Math.max( width, c.getPreferredWidth() );
			height = Math.max( height, c.getPreferredHeight() );
		}

		if ( columns > 1 ) {
			width = width * columns;
		}

		if ( rows > 1 ) {
			if ( numOfcomponents > rows * columns ) { //if there are more components than planned
				height = height * ( numOfcomponents / columns + ( numOfcomponents % columns == 0 ? 0 : 1 ) );
			} else {
				height = height * rows;
			}
		}
		
		if ( maxSize != null ) {
			if ( width > maxSize.x )
				width = maxSize.x;
			
			if ( height > maxSize.y )
				height = maxSize.y;
		}

		return new Point( width, height );
	}

}
