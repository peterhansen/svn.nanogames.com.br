/*
 * RichLabel.java
 *
 * Created on March 7, 2007, 3:33 PM
 *
 */

package br.com.nanogames.components;

import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.util.Mutex;
import br.com.nanogames.components.util.NanoMath;
import java.util.Vector;

//#if J2SE == "false"
import javax.microedition.lcdui.Graphics;
//#else
//# import java.awt.Graphics;
//# import java.awt.Rectangle;
//#endif

/**
 * Implementa um label com formatação especial, possibilitando quebras de linha, várias formas de alinhamento 
 * de texto e inserção de Drawables no texto (animados ou não). Para ter acesso a esses recursos, utiliza-se uma 
 * formatação similar ao padrão HTML.
 *
 * <h4>Alinhamento</h4>
 * Por padrão, o alinhamento é top-left. O alinhamento considerado ao escrever uma linha é a combinação do último alinhamento
 * horizontal com o último alinhamento vertical definidos. Por exemplo: ao encontrar uma tag &lt;ALN_R&gt;, todo o texto
 * subsequente é alinhado à direita, até se chegar ao fim do texto ou ser lida uma tag indicando outro tipo de alinhamento
 * horizontal.
 * <p>Valores válidos:</p>
 * <ul>
 * <li>&lt;ALN_L&gt; alinhado à esquerda.</li>
 * <li>&lt;ALN_H&gt; centralizado horizontalmente.</li>
 * <li>&lt;ALN_R&gt; horizontal à direita.</li>
 * <li>&lt;ALN_T&gt; alinhamento vertical no topo da linha.</li>
 * <li>&lt;ALN_V&gt; centralizado verticalmente.</li>
 * <li>&lt;ALN_B&gt; alinhamento vertical na parte inferior da linha.</li>
 * </ul>
 *
 * <h4>Inserção de Drawables</h4>
 * Drawables podem ser inseridos no texto, através do uso da tag <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" é o índice do
 * Drawable no array passado ao contrutor do label. Os Drawables seguem também as regras de alinhamento válidas para o 
 * texto. É importante observar que o alinhamento vertical define a forma como o texto e os Drawables de uma linha irão
 * se posicionar, no caso de a altura do Drawable não ser igual à altura da fonte utilizada no texto. Esse alinhamento 
 * toma como base o elemento de maior altura encontrado numa linha.
 * <p>É possível também inserir Drawables animados no texto. Para isso, basta que o Drawable implemente a interface
 * <i>Updatable</i>, e faça o gerenciamento da animação através do método <i>update</i> implementado.</p>
 *
 * <h4>Exemplo</h4>
 * A seguinte String:<br></br> <b>&lt;ALN_H&gt;Título\n&lt;ALN_R&gt;Texto à direita\n&lt;ALN_V&gt;Texto à direita e centralizado verticalmente&lt;IMG_0&gt;</b>
 *
 * <p>resulta num texto com a primeira linha centralizada horizontalmente, a segunda alinhada horizontalmente à direita,
 * e a terceira alinhada à direita, porém com alinhamento vertical do texto no centro do Drawable colocado no fim do texto.
 * Para se utilizar Drawables no texto, deve-se passar um array contendo referências a eles, e depois utilizar a tag no padrão
 * <i>&lt;IMG_INDEX&gt;</i>, onde "INDEX" é o índice do Drawable no array.</p>
 *
 *
 * @author peter
 */
public class RichLabel extends Label implements Updatable {
 
	// tags de alinhamento do texto
	public static final char ALIGN_LEFT					= 'L';
	public static final char ALIGN_RIGHT				= 'R';
	public static final char ALIGN_HCENTER				= 'H';
	public static final char ALIGN_TOP					= 'T';
	public static final char ALIGN_VCENTER				= 'V';
	public static final char ALIGN_BOTTOM				= 'B';
	public static final char TAG_START					= '<';
	public static final char TAG_SEPARATOR				= '_';
	public static final char TAG_END					= '>';
	/** Tag: alinhamento de cada linha. */
	public static final String TAG_IMAGE				= "IMG_";
	/** Tag: alinhamento de cada linha. */
	public static final String TAG_ALIGN				= "ALN_";
	/** Tag: indica o índice da fonte. */
	public static final String TAG_FONT					= "FNT_";
	/** Tag: alinhamento de cada linha. */
	public static final String TAG_SMS					= "SMS_"; // TODO implementar links para web, SMS e telefone no corpo do RichLabel
	/** Tag: alinhamento de cada linha. */
	public static final String TAG_URL					= "URL_";
	/** Tag: alinhamento de cada linha. */
	public static final String TAG_PHONE_CALL			= "TEL_";
	 
	// array de Drawables que podem ser inseridos no texto
	protected Drawable[] drawables;
	
	// array dos drawables atualizáveis. Esse é um subconjunto gerado automaticamente a partir do array de drawables, e
	// tem como função acelerar o método update, varrendo somente os elementos que podem ser atualizados.
	protected Updatable[] updatables;
	
	// índices de cada informação na matriz de informações das linhas
	/** Índice da informação das linhas referente ao índice do caracter de início da linha. */
	public static final byte INFO_LINE_START			= 0;
	/** Índice da informação das linhas referente ao índice do caracter de fim da linha. */
	public static final byte INFO_LINE_END				= INFO_LINE_START				+ 1;
	/** Índice da informação das linhas referente à largura da linha. */
	public static final byte INFO_LINE_WIDTH			= INFO_LINE_END					+ 1;
	/** Índice da informação das linhas referente à altura da linha. */
	public static final byte INFO_LINE_HEIGHT			= INFO_LINE_WIDTH				+ 1;
	/** Índice da informação das linhas referente ao tipo de alinhamento horizontal da linha. */
	public static final byte INFO_LINE_ALIGN_HORIZONTAL	= INFO_LINE_HEIGHT				+ 1;
	/** Índice da informação das linhas referente ao tipo de alinhamento vertical da linha. */
	public static final byte INFO_LINE_ALIGN_VERTICAL	= INFO_LINE_ALIGN_HORIZONTAL	+ 1;
	/** Índice da informação das linhas referente à fonte inicialmente usada na linha. */
	public static final byte INFO_LINE_FONT				= INFO_LINE_ALIGN_VERTICAL	+ 1;
	
	protected static final byte LINE_INFO_TOTAL_INDEXES	= INFO_LINE_FONT	+ 1;
	
	protected static final byte TAG_TYPE_NONE		= 0;
	protected static final byte TAG_TYPE_ALIGN		= 1;
	protected static final byte TAG_TYPE_IMAGE		= 2;
	protected static final byte TAG_TYPE_SMS		= 3;
	protected static final byte TAG_TYPE_URL		= 4;
	protected static final byte TAG_TYPE_PHONE_CALL	= 5;
	protected static final byte TAG_TYPE_FONT		= 6;
	
	protected static final int DEFAULT_TEXT_ALIGNMENT = ANCHOR_BOTTOM | ANCHOR_LEFT;
	
	// número de espaços de cada '\t' 
	protected static final byte TAB_SPACES = 3;
	
	/** Matriz de informações das linhas. O primeiro índice refere-se à linha, e o segundo a cada tipo de informação
	 * conforme descrito em INFO_LINE_START, INFO_LINE_END, etc.
	 */
	protected short[][] linesInfo = new short[ 0 ][ 0 ];
	
	/** Offset na posição de desenho do texto (utilizado para fazer scroll do texto) */
	protected int textOffset;
	
	/** linha inicial a partir da qual é feita a varredura para desenho */
	protected short initialLine;
	
	/** Variação na posição inicial de desenho causada pela posição atual do texto - valor utilizado para evitar varreduras
	 * desnecessárias em linhas que não estão dentro da área visível.
	 */
	protected int initialLineOffset;
	
	// StringBuffers usados no método paint( Graphics )
	protected final StringBuffer bufferType = new StringBuffer( 3 );
	protected final StringBuffer bufferNumber = new StringBuffer();
	
	/** evita que o texto seja desenhado e/ou alterado enquanto o texto está sendo reformatado, evitando-se assim 
	 * problemas de inconsistência que podem gerar travamentos
	 */
	protected final Mutex textMutex = new Mutex();

	/** Outras fontes que podem ser usadas no texto, trocadas com a tag de fontes. */
	protected ImageFont[] fonts;


	/**
	 * Cria um novo label formatado. Após a criação, seu tamanho estará definido de forma a comportar todo o texto.
	 * Equivalente à chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 * @see #RichLabel(int, String, int, Drawable[])
	 * @see #RichLabel(int, String, int)
	 * @see #RichLabel(ImageFont, int, int)
	 */
	public RichLabel( ImageFont font ) throws Exception {
		this( font, null );
	}


	/**
	 * Cria um novo label formatado. Após a criação, seu tamanho estará definido de forma a comportar todo o texto.
	 * Equivalente à chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 * @see #RichLabel(int, String, int, Drawable[])
	 * @see #RichLabel(int, String, int)
	 * @see #RichLabel(ImageFont, int, int)
	 */
	public RichLabel( int fontIndex ) throws Exception {
		this( AppMIDlet.GetFont( fontIndex ), null );
	}


	/**
	 * Cria um novo label formatado. Após a criação, seu tamanho estará definido de forma a comportar todo o texto.
	 * Equivalente à chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 * @see #RichLabel(int, String, int, Drawable[])
	 * @see #RichLabel(int, String, int)
	 * @see #RichLabel(ImageFont, int, int)
	 */
	public RichLabel( int fontIndex, int textIndex ) throws Exception {
		this( AppMIDlet.GetFont( fontIndex ), AppMIDlet.getText( textIndex ) );
	}


	/**
	 * Cria um novo label formatado. Após a criação, seu tamanho estará definido de forma a comportar todo o texto.
	 * Equivalente à chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 * @see #RichLabel(int, String, int, Drawable[])
	 * @see #RichLabel(int, int, int)
	 * @see #RichLabel(ImageFont, int, int)
	 */
	public RichLabel( int fontIndex, String text ) throws Exception {
		this( AppMIDlet.GetFont( fontIndex ), text );
	}


	/**
	 * Cria um novo label formatado. Após a criação, seu tamanho estará definido de forma a comportar todo o texto.
	 * Equivalente à chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 * @see #RichLabel(int, String, int, Drawable[])
	 * @see #RichLabel(int, int, int)
	 * @see #RichLabel(ImageFont, int, int)
	 */
	public RichLabel( ImageFont font, int textIndex ) throws Exception {
		this( font, AppMIDlet.getText( textIndex ) );
	}


	/**
	 * Cria um novo label formatado. Após a criação, seu tamanho estará definido de forma a comportar todo o texto. 
	 * Equivalente à chamada de <code>RichLabel( font, text, maxLineWidth, null )</code>.
	 * @param font fonte utilizada para desenhar o texto.
	 * @param text texto a ser desenhado.
	 * @throws java.lang.Exception
	 * @see #RichLabel(ImageFont, String, int, Drawable[])
	 * @see #RichLabel(int, String, int, Drawable[])
	 * @see #RichLabel(int, int, int)
	 * @see #RichLabel(ImageFont, int, int)
	 */
	public RichLabel( ImageFont font, String text ) throws Exception {
		super( font, text, false );
	}

    
	public RichLabel( ImageFont font, String text, boolean autoSize ) throws Exception {
		super( font, text, autoSize );
	}
    

	/**
	 * 
	 * @param specialChars
	 */
	public final void setSpecialChars( Drawable[] specialChars ) {
		if ( specialChars != null && specialChars.length > 0 ) {
			this.drawables = specialChars;

			Vector updatableVector = new Vector();

			// varre o array de caracteres especiais, procurando por caracteres animados; essa varredura tem por
			// objetivo otimizar a varredura do método update(), varrendo somente os objetos que podem ser atualizados
			for ( int i = 0; i < specialChars.length; ++i ) {
				if ( specialChars[ i ] instanceof Updatable )
					updatableVector.addElement( specialChars[ i ] );
			}

			// cria o array de Updatables somente com a quantidade necessária de objetos
			if ( updatableVector.size() > 0 ) {
				updatables = new Updatable[ updatableVector.size() ];

				for ( int i = 0; i < updatables.length; ++i ) {
					updatables[ i ] = ( Updatable ) updatableVector.elementAt( i );
				}
			} else {
				// não foram encontrados elementos atualizáveis no array de caracteres especiais; não é necessário
				// criar o array de atualizáveis
				updatables = null;
			}

		} else {
			this.drawables = null;
			updatables = null;
		}
	}
	
	
	/**
	 * 
	 * @param autoSize
	 */
	public void formatText( boolean autoSize ) {
		if ( getWidth() <= 0 )
			return;
		
        final Vector lines = new Vector( 1 );
        final short[] lineTemp = new short[ LINE_INFO_TOTAL_INDEXES ];
        
		int lineStart = 0;
		// índices na string que delimitam uma linha a ser desenhada
		int lineEnd = 0;
		// largura da linha de texto atual
		int lineWidth = 0;
		// altura da linha de texto atual
		int lineHeight = 0;
		int verticalAlignment = ANCHOR_TOP;
		int horizontalAlignment = ANCHOR_LEFT;
		
		// largura em pixels do espaço é armazenado para acelerar o loop
		int spaceWidth = font.getCharWidth( ' ' );
        
		// linha atual
        int currentLine = 0;
		// posição x atual
		int x = 0;
		// caracteres lidos
        int read = 0;
        
		// indica se a linha ainda não foi totalmente preenchida
        boolean lineIncomplete = true; 
        
        // constantes calculadas previamente de forma a acelerar o loop
        final int toRead = charBuffer.length;

		ImageFont currentFont = getFont();
		int lineInitialFont = 0;
		int lineLastFont = 0;
		
        while ( read < toRead ) {
            int i = read;
            lineIncomplete = true;
			
			// posição no textArray de char onde a última palavra iniciou
            int lastWordStart = 0;
            
            lineStart = read;
            lineWidth = x = 0;

			// indica se uma palavra já foi iniciada, ou se o caracter lido é o primeiro de uma palavra (no caso de caracteres imprimíveis)
            boolean word = false; 
			
            lineHeight = 0;
            
        	// enquanto não completa uma linha                
            while ( lineIncomplete && ( i < toRead ) ) {
                final char c = charBuffer[ i ];
				// indica o tipo do caracter lido 
                boolean printableRead = false;
                switch ( c ) {
                	case 0: // não desenha caso o caracter não esteja definido.
					break;
						
                	case ' ':
						if( lineHeight == 0 )
							lineHeight = currentFont.getHeight();
						x += spaceWidth;
                	    if ( !autoSize && x > size.x ) {
                	        lineIncomplete = false;
                	        i++;
                	    }
               	        lineWidth = x;
					break; // fim case ' '
						
                	case '\n':
						if( lineHeight == 0 )
							lineHeight = currentFont.getHeight();
            	        lineWidth = x;
                	    lineIncomplete = false;
                	    read = i++;
					break; // fim case '\n'
						
                	case TAG_START:
                	    final StringBuffer strBufferType = new StringBuffer( 3 );
                	    final StringBuffer strBufferInfo = new StringBuffer();
                	    char alignType = 0;

                	    byte type = TAG_TYPE_NONE; 
                	    int j = i + 1;
                	    while ( charBuffer[ j ] != TAG_END ) {
                	        switch ( type ) {
                	        	case TAG_TYPE_NONE:
                       	            strBufferType.append( charBuffer[ j ] );
                        	        if ( charBuffer[ j ] == TAG_SEPARATOR ) {
                        	            final String tagType = strBufferType.toString();
                        	            if ( tagType.equals( TAG_ALIGN ) )
                        	                type = TAG_TYPE_ALIGN;
                        	            else if ( tagType.equals( TAG_IMAGE ) )
                        	                type = TAG_TYPE_IMAGE;
                        	            else if ( tagType.equals( TAG_FONT ) )
                        	                type = TAG_TYPE_FONT;
                        	            else if ( tagType.equals( TAG_URL ) )
                        	                type = TAG_TYPE_URL;
                        	            else if ( tagType.equals( TAG_SMS ) )
                        	                type = TAG_TYPE_SMS;
                        	            else if ( tagType.equals( TAG_PHONE_CALL ) )
                        	                type = TAG_TYPE_PHONE_CALL;
                        	        }
                	        	break;

								case TAG_TYPE_ALIGN:
                	        	    alignType = charBuffer[ j ];
                	        	break;

                	        	case TAG_TYPE_IMAGE:
                	        	case TAG_TYPE_URL:
                	        	case TAG_TYPE_SMS:
                	        	case TAG_TYPE_FONT:
                	        	case TAG_TYPE_PHONE_CALL:
									// lê a informação adicional da tag (índice da imagem, url para acessar, número de telefone, etc.)
                	        	    strBufferInfo.append( charBuffer[ j ] );
                	        	break;
                	        } // fim switch ( type )
                	        ++j;
                	    } // fim while ( textArray[ j ] != TAG_END )
						
						switch ( type ) {
							case TAG_TYPE_ALIGN:
								switch ( alignType ) {
									case ALIGN_HCENTER:
										horizontalAlignment = ANCHOR_HCENTER;
									break;
									
									case ALIGN_LEFT:
										horizontalAlignment = ANCHOR_LEFT;
									break;
									
									case ALIGN_RIGHT:
										horizontalAlignment = ANCHOR_RIGHT;
									break;
									
									case ALIGN_BOTTOM:
										verticalAlignment = ANCHOR_BOTTOM;
									break;
									
									case ALIGN_VCENTER:
										verticalAlignment = ANCHOR_VCENTER;
									break;
									
									case ALIGN_TOP:
										verticalAlignment = ANCHOR_TOP;
									break;
								} // fim switch ( alignType )
							break;
							
							case TAG_TYPE_IMAGE:
								if ( drawables != null ) {
									final int index = Integer.parseInt( strBufferInfo.toString() );
									printableRead = true;
									// Iniciou uma nova "palavra" (um drawable). Marca a posição de início como a
									// última posição de fim de linha conhecida.
									lastWordStart = i;
									lineEnd = lastWordStart;
									lineWidth = x;

									// se uma tag de imagem for o primeiro caracter imprimível da linha, marca o início da linha
									if ( lastWordStart == 0 )
										lineStart = i;

									final int drawableWidth = drawables[ index ].getWidth();
									final int drawableHeight = drawables[ index ].getHeight();
									
									final int tempWidth = x + drawableWidth;
									
									if ( !autoSize && tempWidth > size.x ) {
										// terminou uma linha
										lineIncomplete = false;
										
										if ( lastWordStart == lineStart ) {
											// drawable é o primeiro caracter da linha - inicia uma nova linha
											
											// o marcador de fim de linha é posicionado logo após o fim da tag
											lineEnd = j;

											// o drawable tem largura maior ou igual à largura do label, logo considera-se
											// que a linha tem também a largura do label.
											lineWidth = size.x;
											
											// atualiza a altura da linha, caso necessário
											if ( drawableHeight > lineHeight )
												lineHeight = drawableHeight;
										} else {
											// há caracteres na linha antes do drawable - o imprime por completo na linha seguinte.
											lineEnd = lastWordStart;
											lineWidth = x;
										}
										
										read = lineEnd;
									} else {
										x = tempWidth;
										
										// atualiza a altura da linha, caso necessário
										if ( drawableHeight > lineHeight )
											lineHeight = drawableHeight;
									}
									
								} // fim if ( drawables != null )
							break;

							case TAG_TYPE_FONT:
								lineLastFont = Integer.parseInt( strBufferInfo.toString() );
								currentFont = getFont( lineLastFont );
								spaceWidth = currentFont.getCharWidth( ' ' );
							break;
						} // fim switch ( type )

            	        if ( lineIncomplete )
            	            i = j;
					break; // fim case TAG_START
						
            		default:
                		printableRead = true;            			
						if ( !word ) {
							lineEnd = lastWordStart = i;
							lineWidth = x;
						} 
						
						if ( currentFont.getHeight() > lineHeight )
							lineHeight = currentFont.getHeight();
						
						// Testa se ultrapassa limite da margem direita. caso ultrapasse, a palavra será impressa na 
						// linha seguinte. caso a palavra seja grande demais, é quebrada por caracter.
						if ( lastWordStart == lineStart ) {
							// palavra é maior que a largura da linha (palavra é quebrada por caracter).
							read = lineEnd = i;
							lineWidth = x;
						} else {
							// palavra é menor que a largura da linha - é impressa por completo na linha seguinte
							read = lineEnd = lastWordStart;
						}
						
						x += currentFont.getCharWidth( c );
						if ( !autoSize && x > size.x )
							lineIncomplete = false;
						word = true;
    	                break;
                } // fim switch ( c )
				
				if ( !lineIncomplete ) {
					if ( !printableRead ) {
						//i++; // com essa linha não comentada, os espaços no início de cada linha não são mostrados
						read = i;
						lineEnd = i;
					}
				} else { // if (lineIncomplete)
					++i;
					if ( !printableRead ) {
						lineEnd = i;
						word = false;
					}
					read = i;
				}
            } // fim while (lineIncomplete)
			
            if ( i >= toRead ) {
    	        read = lineEnd = i;
    	        lineWidth = x;
            } else
                read = lineEnd;
            
            lineTemp[ INFO_LINE_START ] = ( short ) lineStart;
            lineTemp[ INFO_LINE_END ] = ( short ) lineEnd;
			// espaços e tabulações podem ultrapassar a margem da imagem, porém a largura da linha não pode ser maior 
			// que a largura do label, senão ocorrem problemas com alinhamentos horizontais.
			if ( autoSize ) {
				// define suas dimensões de forma a comportar todos os caracteres numa única linha.
				lineTemp[ INFO_LINE_WIDTH ] = ( short ) lineWidth;
				setSize( lineWidth, lineHeight );
			} else {
				lineTemp[ INFO_LINE_WIDTH ] = ( short ) Math.min( lineWidth, size.x );
			}
			
            lineTemp[ INFO_LINE_HEIGHT ] = ( short ) lineHeight;
            lineTemp[ INFO_LINE_ALIGN_HORIZONTAL ] = ( short ) horizontalAlignment;
			lineTemp[ INFO_LINE_ALIGN_VERTICAL ] = ( short ) verticalAlignment;
			
			lineTemp[ INFO_LINE_FONT ] = ( short ) lineInitialFont;
			lineInitialFont = lineLastFont;

            final short[] t = new short[ lineTemp.length ];
            System.arraycopy( lineTemp, 0, t, 0, lineTemp.length );
            lines.addElement( t );
            
            ++currentLine;
        } // fim while ( read < toRead )
        
        linesInfo = new short[ lines.size() ][ LINE_INFO_TOTAL_INDEXES ];
        for ( int index = 0; index < linesInfo.length; ++index ) {
            final short[] t = ( short[] ) lines.elementAt( index );
            for ( int i = 0; i < LINE_INFO_TOTAL_INDEXES; ++i )
                linesInfo[ index ][ i ] = t[ i ];
        }
		
		// volta a exibir o texto a partir do início
		setTextOffset( 0 );
	} // fim do método formatText()


	/**
	 * 
	 * @param width
	 * @param height
	 */
	public void setSize( int width, int height ) {
		final int previousWidth = getWidth();
		
		super.setSize( width, height );

		if ( previousWidth != getWidth() )
			formatText( false );
	}
	
	
	/**
	 * 
	 * @param g 
	 */
    public void paint( Graphics g ) {
		// FIXME pode ocorrer erro no desenho de partes do texto após drawable - não é exibido texto após drawable, porém, após o scroll descer um pouco, é exibido um bloco inteiro de texto que deveria estar visível antes
		textMutex.acquire();

		final int totalLines = linesInfo.length;
		int x;
		int	y = translate.y + textOffset - initialLineOffset;
		int	verticalAlignment;
		int	currentLine;
		boolean imageEnd = false;

		ImageFont currentFont = getFont();

		// altura da fonte
		int fontHeight = currentFont.getHeight();

		pushClip( g );
		//#if J2SE == "false"
			final int yLimit = g.getClipY() + g.getClipHeight();
		//#else
//# 			final Rectangle clip = g.getClipBounds();
//# 			final int yLimit = clip.y + clip.height;
		//#endif

		// se o topo da linha estiver além da área de clip, pára a varredura, pois garantidamente todos os caracteres
		// e/ou drawables das linhas seguintes estarão fora da área de clip.		
		for ( currentLine = initialLine; currentLine < totalLines && !imageEnd && y < yLimit; ++currentLine ) {
			// armazena uma referência para as informações atuais, de forma a evitar acessos desnecessários ao array
			final short[] LINE_INFO = linesInfo[ currentLine ];

			// como são puladas algumas linhas, pode ocorrer de uma linha anterior ter definido outra fonte
			currentFont = getFont( LINE_INFO[ INFO_LINE_FONT ] );
			fontHeight = currentFont.getHeight();
			
			// ajusta a posição do texto de acordo com o alinhamento horizontal
			switch ( LINE_INFO[ INFO_LINE_ALIGN_HORIZONTAL ] ) {
				case ANCHOR_RIGHT:
					x = translate.x + size.x - LINE_INFO[ INFO_LINE_WIDTH ];
				break;
				
				case ANCHOR_HCENTER:
					x = translate.x + ( ( size.x - LINE_INFO[ INFO_LINE_WIDTH ] ) >> 1 );
				break;
				
				default:
					x = translate.x;
			}

			// ajusta a posição do texto de acordo com o alinhamento vertical
			verticalAlignment = LINE_INFO[ INFO_LINE_ALIGN_VERTICAL ];
			switch ( verticalAlignment ) {
				case ANCHOR_BOTTOM:
					y += LINE_INFO[ INFO_LINE_HEIGHT ] - fontHeight;
				break;
				
				case ANCHOR_VCENTER:
					y += ( LINE_INFO[ INFO_LINE_HEIGHT ] - fontHeight ) >> 1;
				break;
			}

			// o seguinte teste é utilizado para que só as linhas a partir da área visível sejam desenhadas
			if ( y + LINE_INFO[ INFO_LINE_HEIGHT ] >= translate.y ) {
				for ( int charIndex = LINE_INFO[ INFO_LINE_START ]; ( charIndex < LINE_INFO[ INFO_LINE_END ] ); ++charIndex ) {
					final char c = charBuffer[ charIndex ];
					
					switch ( c ) {
						case 0: // não desenha caso o caracter não esteja definido.
						break;

						case TAG_START:
							byte type = TAG_TYPE_NONE;
							int tagEndIndex = charIndex + 1;
							while ( charBuffer[ tagEndIndex ] != TAG_END ) {
								switch ( type ) {
									case TAG_TYPE_NONE:
										bufferType.append( charBuffer[ tagEndIndex ] );
										if ( charBuffer[ tagEndIndex ] == TAG_SEPARATOR ) {
											final String tagType = bufferType.toString();
											if ( tagType.equals( TAG_IMAGE ) )
												type = TAG_TYPE_IMAGE;
											else if ( tagType.equals( TAG_FONT ) )
												type = TAG_TYPE_FONT;
										}
										break;
										
									case TAG_TYPE_IMAGE:
									case TAG_TYPE_FONT:
										bufferNumber.append( charBuffer[ tagEndIndex ] );
										break;
								}
								++tagEndIndex;
							} // fim while ( textArray[ tagEndIndex ] != TAG_END )
							charIndex = tagEndIndex;		          	    

							switch ( type ) {
								case TAG_TYPE_IMAGE:
									final int index = Integer.parseInt( bufferNumber.toString() );

									// verifica se o índice é válido
									//#if DEBUG == "true"
	//# 								try {
									//#endif
										final int drawableWidth = drawables[ index ].getWidth();
										int drawableHeight = drawables[ index ].getHeight();
										if ( y + drawableHeight > yLimit ) {
											drawableHeight = yLimit - y;
											imageEnd = true;
										} else {
											drawableHeight += y - position.y;
										}

										if ( verticalAlignment == ANCHOR_TOP ) {
											drawables[ index ].setPosition( x - translate.x, y - translate.y );
										} else if ( verticalAlignment == ANCHOR_VCENTER ) {
											drawables[ index ].setPosition( x - translate.x, y - translate.y + ( ( fontHeight - drawables[ index ].getHeight() ) >> 1 ) );
										} else {
											drawables[ index ].setPosition( x - translate.x, y - translate.y + fontHeight - drawables[ index ].getHeight() );
										}
										drawables[ index ].draw( g );
										x += drawableWidth;
									//#if DEBUG == "true"
	//# 								} catch ( Exception e ) {
	//# 									// desenha uma área vermelha e escreve o índice do drawable não encontrado
	//# 									final String text = "<IMG_" + index + ">";
	//# 									final int textWidth = font.getTextWidth( text );
	//# 									g.setColor( 0xff0000 );
	//# 									g.fillRect( x, y, textWidth, fontHeight );
	//#
	//# 									drawString( g, text, x, y );
	//# 									x += textWidth;
	//# 								}
									//#endif
								break;

								case TAG_TYPE_FONT:
									currentFont = getFont( Integer.parseInt( bufferNumber.toString() ) );
									fontHeight = currentFont.getHeight();
								break;
							}
							bufferNumber.delete( 0, bufferNumber.length() );
							bufferType.delete( 0, bufferType.length() );
						break; // fim case TAG_START

						default:
							drawChar( g, currentFont, c, x, y );
							x += currentFont.getCharWidth( c );
						break;

					} // fim switch ( c ) 
				} // fim for ( int charIndex = LINE_INFO[ INFO_LINE_START ]; ( charIndex < LINE_INFO[ INFO_LINE_END ] ); ++charIndex )
			} // fim if ( y + LINE_INFO[ INFO_LINE_HEIGHT ] >= translate.y )

			// posiciona a variável y no topo da próxima linha (o cálculo para cada tipo de alinhamento vertical varia)
			switch ( verticalAlignment ) {
				case ANCHOR_TOP:
					y += LINE_INFO[ INFO_LINE_HEIGHT ];
				break;

				case ANCHOR_VCENTER:
					y += LINE_INFO[ INFO_LINE_HEIGHT ] - ( ( LINE_INFO[ INFO_LINE_HEIGHT ] - fontHeight ) >> 1 );
				break;

				case ANCHOR_BOTTOM:
					y += fontHeight;
				break;
			} // fim switch ( verticalAlignment )
		} // fim for ( currentLine = initialLine; currentLine < totalLines && !imageEnd; ++currentLine )
		popClip( g );

		textMutex.release();
    } // fim do método paint( Graphics )
	
	 
	/**
	 *@see Updatable#update(int)
	 */
	public void update( int delta ) {
		if ( updatables != null ) {
			for ( int i = 0; i < updatables.length; ++i ) {
				updatables[ i ].update( delta );
			}
		}
	} // fim do método update( int )
	

	/**
	 * 
	 * @return 
	 */
	public final short[][] getLinesInfo() {
		return linesInfo;
	} // fim do método getLinesInfo()

	 
	/**
	 * Obtém a altura total do texto.
	 * @return altura total do texto, em pixels (pode exceder a altura definida do label).
	 */
	public int getTextTotalHeight() {
		int height = 0;
		for ( int i = 0; i < linesInfo.length; ++i )
			height += linesInfo[ i ][ INFO_LINE_HEIGHT ];
		
		return height;
	} // fim do método getTextTotalHeight()
	
	
	/**
	 * Define o offset na posição inicial de desenho do texto.
	 * @param offset offset na posição inicial de desenho do texto, em pixels.
	 */
	public void setTextOffset( int offset ) {
		textOffset = offset;
		
		initialLineOffset = 0;
		int i = 0;
		for ( ; i < linesInfo.length; ++i ) {
			if ( initialLineOffset - linesInfo[ i ][ INFO_LINE_HEIGHT ] <= textOffset )
				break;
			
			initialLineOffset -= linesInfo[ i ][ INFO_LINE_HEIGHT ];
		}
		
		// armazena a primeira linha visível, de forma a otimizar o desenho do RichLabel
		initialLine = ( short ) i;
	} // fim do método setTextOffset( int )
	
	
	/** 
	 * Retorna a linha a partir da qual o texto começa a ser desenhado.
	 * @return 
	 * @see #setInitialLine(int)
	 */
	public final short getInitialLine() {
		return initialLine;
	}
	
	
	/** 
	 * Determina a linha a partir da qual o texto começa a ser desenhado.
	 * @param i 
	 * @see #getInitialLine
	 */
	public final void setInitialLine( int i ) {
		initialLine = ( short ) i;
		
		initialLineOffset = 0;
		for( int j = 0 ; j < i ; ++j )
			initialLineOffset -= linesInfo[ j ][ INFO_LINE_HEIGHT ];
	}


	/**
	 * Define fontes extras para o texto, que podem ser trocadas com a tag FNT_INDEX. Observação: FNT_0 utiliza a fonte
	 * original do texto, ou seja, para usar a 1ª fonte extra deve-se usar a tag FNT_1, mesmo que ela esteja no índice 0
	 * do array passado como parâmetro.
	 * @param fonts
	 */
	public final void setFonts( ImageFont[] fonts ) {
		this.fonts = fonts;
	}


	/**
	 * 
	 * @param index
	 * @return
	 */
	public final ImageFont getFont( int index ) {
		if ( index == 0 )
			return getFont();
		else {
			try {
				return fonts[ index ];
			} catch ( Exception e ) {
				//#if DEBUG == "true"
//# 			//		System.out.println( "Fonte inválida: " + index );
				//#endif
				return getFont();
			}
		}
	}


	/**
	 * Define o texto do label. Equivalente à chamada de <code>setText( text, true )</code>.
	 *
	 * @param text texto do label.
	 * @see #setText(String, boolean)
	 * @see #setText(int)
	 */
	public void setText( String text ) {
		setText( text, false );
	}
	
	
	/**
	 * Define o texto do label, com opção para reformatar o texto ou não. Caso o texto deva ser reformatado, suas
	 * dimensões não serão alteradas - equivalente à chamada de <code>setText( text, formatText, false )</code>.
	 * 
	 * @param text texto do label.
	 * @param formatText indica se o texto deve ser reformatado, de forma a preencher de forma ótima a nova área de texto.
	 * @see #setText(String)
	 * @see #setText(String, boolean, boolean)
	 */
	public final void setText( String text, boolean formatText ) {
		setText( text, formatText, false );
	}
	
	
	/**
	 * Define o texto do label.
	 * @param text texto do label.
	 * @param formatText indica se o texto deve ser reformatado, de forma a preencher de forma ótima a nova área de texto.
	 * @param autoSize indica se as dimensões do texto devem ser redefinidas automaticamente. Esse parâmetro é desprezado
	 * caso <code>formatText</code> seja false.
	 * @see #setText(String)
	 * @see #setText(String, boolean)
	 */
	public void setText( String text, boolean formatText, boolean autoSize ) {
		// evita que o texto seja desenhado e/ou alterado enquanto o texto está sendo reformatado, evitando-se assim
		// problemas de inconsistência que podem gerar travamentos. Durante o construtor, textMutex é null, por isso
		// a verificação do objeto.
		if ( textMutex != null )
			textMutex.acquire();
		
		if ( text == null )
			text = "";

		charBuffer = text.toCharArray();
		
		if ( formatText )
			formatText( autoSize );

		// notifica todas as possíveis threads esperando para alterar o texto, e volta a permitir desenhos do texto
		if ( textMutex != null )
			textMutex.release();
	} // fim do método setText( String, boolean )

	
	public int getTextOffset() {
		return textOffset;
	}
	
}
 
