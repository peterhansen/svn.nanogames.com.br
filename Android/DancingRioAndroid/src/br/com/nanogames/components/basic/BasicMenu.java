/*
 * BasicMenu.java
 *
 * Created on October 4, 2007, 10:24 AM
 *
 */

package br.com.nanogames.components.basic;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import java.util.Hashtable;

/**
 *
 * @author peter
 */
public class BasicMenu extends Menu implements ScreenListener {
	
	protected final byte backIndex;
	
	protected final byte firstEntry;
	
	/** Estrutura que armazena a última entrada selecionada de cada menu. A chave de cada entrada é o id do menu,
	 * e o valor da chave é o índice da última entrada utilizada.
	 */
	protected static final Hashtable LAST_INDEX = new Hashtable();

	private final Drawable title;

	private final ImageFont font;

	private final Drawable[] labels;

	private final byte itemSpacing;
	

	/** 
	 * Aloca um novo menu básico, com as seguintes características: centralizado na tela, circular, textos centralizados
	 * horizontalmente.
	 * 
	 * @param listener listener do menu, que receberá os eventos de troca de item do menu e confirmação de um item.
	 * @param id identificação deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entries índices dos textos que compõem cada entrada de menu.
	 * @param itemSpacing espaçamento vertical entre os itens. Por padrão, o espaçamento é a altura de cada entrada
	 * de menu. Este valor pode ser usado tanto para aumentar essa distância (valores positivos), quanto para reduzi-la
	 * (valores negativos).
	 * @param firstEntryIndex índice da primeira opção selecionável do menu. Caso o menu use a primeira entrada como
	 * título, por exemplo, basta passar o valor 1. Caso todas as entradas sejam selecionáveis, basta passar 0 (zero).
	 * @param backIndex indica se a última entrada de menu possui a mesma função das teclas de retorno, como 
	 * ScreenManager.KEY_BACK, ScreenManager.KEY_SOFT_RIGHT e ScreenManager.KEY_CLEAR.
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */	
	public BasicMenu( MenuListener listener, int id, ImageFont font, int[] entries, int itemSpacing, int firstEntryIndex, int backIndex ) throws Exception {
		this( listener, id, font, entries, itemSpacing, firstEntryIndex, backIndex, null );
	}
	
	
	/** 
	 * Aloca um novo menu básico, com as seguintes características: centralizado na tela, circular, textos centralizados
	 * horizontalmente.
	 * 
	 * @param listener listener do menu, que receberá os eventos de troca de item do menu e confirmação de um item.
	 * @param id identificação deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entries índices dos textos que compõem cada entrada de menu.
	 * @param itemSpacing espaçamento vertical entre os itens. Por padrão, o espaçamento é a altura de cada entrada
	 * de menu. Este valor pode ser usado tanto para aumentar essa distância (valores positivos), quanto para reduzi-la
	 * (valores negativos).
	 * @param firstEntryIndex índice da primeira opção selecionável do menu. Caso o menu use a primeira entrada como
	 * título, por exemplo, basta passar o valor 1. Caso todas as entradas sejam selecionáveis, basta passar 0 (zero).
	 * @param backIndex indica se a última entrada de menu possui a mesma função das teclas de retorno, como 
	 * ScreenManager.KEY_BACK, ScreenManager.KEY_SOFT_RIGHT e ScreenManager.KEY_CLEAR.
	 * @param title 
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */
	public BasicMenu( MenuListener listener, int id, ImageFont font, int[] entries, int itemSpacing, int firstEntryIndex, int backIndex, Drawable title ) throws Exception {
		this( listener, id, font, entries, null, null, itemSpacing, firstEntryIndex, backIndex, title );
	}


	public BasicMenu( MenuListener listener, int id, Drawable[] entries, int itemSpacing, int firstEntryIndex, int backIndex, Drawable title ) throws Exception {
		this( listener, id, null, new int[ entries.length ], entries, null, itemSpacing, firstEntryIndex, backIndex, title );
	}
	
	
	protected BasicMenu( MenuListener listener, int id, ImageFont font, int[] entriesIndexes, Drawable[] entries, String[] entriesText, int itemSpacing, int firstEntryIndex, int backIndex, Drawable title ) throws Exception {
		super( listener, id, ( entriesIndexes == null ? entriesText.length : entriesIndexes.length ) + ( title == null ? 0 : 1 ) );
		
		this.backIndex = ( byte ) backIndex;
		this.firstEntry = ( byte ) firstEntryIndex;
		this.itemSpacing = ( byte ) itemSpacing;
		
		final int FONT_HEIGHT = font == null ? 0 : font.getHeight();

		this.title = title;
		this.font = font;
		
		if ( title != null ) {
			insertDrawable( title );
			title.defineReferencePixel( title.getWidth() >> 1, 0 );
			title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 0 );
		}

		final int LENGTH = ( entriesIndexes == null ? entriesText.length : entriesIndexes.length );
		labels = new Drawable[ LENGTH ];

		if ( entries == null ) {
			for ( int i = 0; i < LENGTH; ++i ) {
				final Label label = new Label( font, entriesText == null ? AppMIDlet.getText( entriesIndexes[ i ] ) : entriesText[ i ] );
				labels[ i ] = label;
				insertDrawable( label );
			}
		} else {
			for ( int i = 0; i < entries.length; ++i ) {
				insertDrawable( entries[ i ] );
				labels[ i ] = entries[ i ];
                labels[ i ].setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT / entries.length );
			}
		}

		setCircular( true );
		
		final Integer lastIndex = ( Integer ) LAST_INDEX.get( new Integer( id ) );
		if ( lastIndex == null )
			setCurrentIndex( firstEntryIndex );
		else {
			final int index = lastIndex.intValue();
			if ( index == backIndex )
				setCurrentIndex( 0 );
			else
				setCurrentIndex( index );		
		}

		setSize( ScreenManager.SCREEN_WIDTH, ScreenManager.SCREEN_HEIGHT );

		// atualiza o cursor (largura do texto pode mudar)
		setCurrentIndex( currentIndex );			
	}
	

	/** 
	 * Aloca um novo menu básico, com as seguintes características: centralizado na tela, circular, textos centralizados
	 * horizontalmente. Equivalente à chamada do construtor BasicMenu( listener, id, font, entries, 0, 0, entries.length - 1 ).
	 * 
	 * @param listener listener do menu, que receberá os eventos de troca de item do menu e confirmação de um item.
	 * @param id identificação deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entries índices dos textos que compõem cada entrada de menu.
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */
	public BasicMenu( MenuListener listener, int id, ImageFont font, int[] entries ) throws Exception {
		this( listener, id, font, entries, 0, 0, entries.length - 1 );
	}	
	
	
	/** 
	 * Aloca um novo menu básico, com as seguintes características: centralizado na tela, circular, textos centralizados
	 * horizontalmente.
	 * 
	 * @param listener listener do menu, que receberá os eventos de troca de item do menu e confirmação de um item.
	 * @param id identificação deste menu, passado para o listener.
	 * @param font fonte utilizada para criar as entradas de menu.
	 * @param entriesText textos que compõem cada entrada de menu.
	 * @param itemSpacing espaçamento vertical entre os itens. Por padrão, o espaçamento é a altura de cada entrada
	 * de menu. Este valor pode ser usado tanto para aumentar essa distância (valores positivos), quanto para reduzi-la
	 * (valores negativos).
	 * @param firstEntryIndex índice da primeira opção selecionável do menu. Caso o menu use a primeira entrada como
	 * título, por exemplo, basta passar o valor 1. Caso todas as entradas sejam selecionáveis, basta passar 0 (zero).
	 * @param backIndex indica se a última entrada de menu possui a mesma função das teclas de retorno, como 
	 * ScreenManager.KEY_BACK, ScreenManager.KEY_SOFT_RIGHT e ScreenManager.KEY_CLEAR.
	 * @param title 
	 * @throws java.lang.Exception caso entries ou a fonte sejam nulos, ou haja erro ao alocar recursos.
	 */	
	public BasicMenu( MenuListener listener, int id, ImageFont font, String[] entriesText, int itemSpacing, int firstEntryIndex, int backIndex, Drawable title ) throws Exception {
		this( listener, id, font, null, null, entriesText, itemSpacing, firstEntryIndex, backIndex, title );
	}


	public void setSize( int width, int height ) {
		final int CURSOR_HEIGHT_DIFF = getCursor() == null ? 0 : Math.max( getCursor().getHeight() - labels[ 0 ].getHeight(), 0 );
		int y = CURSOR_HEIGHT_DIFF >> 1;

		if ( title != null ) {
			title.defineReferencePixel( title.getWidth() >> 1, 0 );
			title.setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, 0 );
			y += title.getHeight();
		}

		for ( int i = 0; i < labels.length; ++i ) {
			labels[ i ].defineReferencePixel( ANCHOR_HCENTER | ANCHOR_TOP );
			labels[ i ].setRefPixelPosition( ScreenManager.SCREEN_HALF_WIDTH, y );

            y += labels[ i ].getHeight() + itemSpacing;
		}

		super.setSize( ScreenManager.SCREEN_WIDTH, y + CURSOR_HEIGHT_DIFF );

		// atualiza o cursor (largura do texto pode mudar)
		setCurrentIndex( currentIndex );
	}


	public void setCursor( Drawable cursor, byte drawOrder, int alignment ) {
		super.setCursor( cursor, drawOrder, alignment );

		setSize( getSize() );
	}
	
	
	public void keyPressed( int key ) {
		switch ( key ) {
			case ScreenManager.KEY_BACK:
			case ScreenManager.KEY_CLEAR:
			case ScreenManager.KEY_SOFT_RIGHT:
				if ( backIndex >= 0 ) {
					setCurrentIndex( backIndex );
					super.keyPressed( ScreenManager.KEY_NUM5 );
				}
			break;
			
			case ScreenManager.KEY_SOFT_LEFT:
				super.keyPressed( ScreenManager.KEY_NUM5 );
			break;

			default:
				super.keyPressed( key );
		} // fim switch ( key )
	}

	
	public void setCurrentIndex( int index ) {
		final int previousIndex = currentIndex;
		
		super.setCurrentIndex( index );
		
		if ( currentIndex <= firstEntry ) {
			if ( previousIndex < firstEntry || previousIndex == activeDrawables - 1 )
				super.setCurrentIndex( firstEntry );
			else if ( previousIndex == firstEntry && currentIndex < firstEntry ) {
				if ( firstEntry == 0 )
					super.setCurrentIndex( 0 );
				else 
					super.setCurrentIndex( circular ? activeDrawables - 1 : firstEntry );
			}
		}
		
		// grava o índice atual para futuras instâncias de menu com o mesmo id
		LAST_INDEX.put( new Integer( menuId ), new Integer( currentIndex ) );

		final Drawable entry = getDrawable( currentIndex );
		final int relativeY = getPosY() + entry.getPosY();
		if ( relativeY < 0 ) {
			setPosition( getPosX(), -entry.getPosY() );
		} else if ( relativeY + entry.getHeight() > ScreenManager.SCREEN_HEIGHT ) {
			setPosition( getPosX(), getPosY() + ScreenManager.SCREEN_HEIGHT - relativeY - entry.getHeight() );
		}
	}
	
	
	/**
	 * Obtém o índice da opção selecionada pelo ponteiro.
	 * 
	 * @param x posição x do evento de ponteiro na tela.
	 * @param y posição y do evento de ponteiro na tela.
	 * @return índice da opção selecionada, ou -1 caso a posição não intercepte nenhuma entrada.
	 */
	protected final int getEntryAt( int x, int y ) {
		x -= position.x;
		y -= position.y;
		
		for ( byte i = firstEntry; i < activeDrawables; ++i ) {
			final Drawable entry = drawables[ i ];
			if ( entry.contains( x, y ) )
				return i;
		}
		return -1;
	}


	public void hideNotify( boolean deviceEvent ) {
	}


	public void showNotify( boolean deviceEvent ) {
	}


	public void sizeChanged( int width, int height ) {
		setSize( width, height );
	}

}
