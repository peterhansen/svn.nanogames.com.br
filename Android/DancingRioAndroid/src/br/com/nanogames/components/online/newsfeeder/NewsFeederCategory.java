/*
 * NewsFeederCategory.java
 *
 * Created on April 13, 2010, 14:18:04
 *
 */
package br.com.nanogames.components.online.newsfeeder;

import br.com.nanogames.components.util.Serializable;
import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author peter
 */
public final class NewsFeederCategory implements Serializable {

	/** Slot no RMS onde as informações da categoria estão salvas. */
	private int slot = -1;

	/** Indica se a categoria é visível. */
	private boolean visible;

	/** Título da categoria. */
	private String title = "";

	/** Id da categoria no banco de dados. */
	private int id = -1;

	/** Descrição da categoria. */
	private String description = "";


	public final int getId() {
		return id;
	}


	public final void setId( int id ) {
		this.id = id;
	}


	public final int getSlot() {
		return slot;
	}


	public final void setSlot( int slot ) {
		this.slot = slot;
	}


	public final String getTitle() {
		return title;
	}


	public final void setTitle( String title ) {
		this.title = title;
	}


	public final boolean isVisible() {
		return visible;
	}


	public final void setVisible( boolean v ) {
		this.visible = v;
	}


	public final String getDescription() {
		return description;
	}


	public final void setDescription( String description ) {
		this.description = description;
	}


	public final void write( DataOutputStream output ) throws Exception {
		output.writeInt( getId() );
		output.writeInt( getSlot() );
		output.writeBoolean( isVisible() );
		output.writeUTF( getTitle() );
		output.writeUTF( getDescription() );
	}


	public final void read( DataInputStream input ) throws Exception {
		setId( input.readInt() );
		setSlot( input.readInt() );
		setVisible( input.readBoolean() );
		setTitle( input.readUTF() );
		setDescription( input.readUTF() );
	}


	public final void parseParams( DataInputStream input ) throws Exception {
		byte paramId = -1;

		do {
			paramId = input.readByte();

			switch ( paramId ) {
				case NewsFeederEntry.PARAM_ENTRY_CATEGORY_ID:
					setId( input.readInt() );
				break;

				case NewsFeederEntry.PARAM_ENTRY_CATEGORY_TITLE:
					setTitle( input.readUTF() );
				break;

				case NewsFeederEntry.PARAM_ENTRY_CATEGORY_DESCRIPTION:
					setDescription( input.readUTF() );
				break;

				case NewsFeederEntry.PARAM_ENTRY_CATEGORY_VISIBILITY:
					setVisible( input.readBoolean() );
				break;

				case NewsFeederEntry.PARAM_ENTRY_CATEGORY_END:
				break;
			}
		} while ( paramId != NewsFeederEntry.PARAM_ENTRY_CATEGORY_END );
		//#if DEBUG == "true"
//# 			System.out.println( "News feeder category read: " + this );
		//#endif
	} // fim do método parseParams( DataInputStream )


	//#if DEBUG == "true"
//# 		public final String toString() {
//# 			final StringBuffer b = new StringBuffer();
//# 			b.append( "NewsFeederCategory" );
//# 			b.append( "\n\tid: " );
//# 			b.append( getId() );
//# 			b.append( "\n\ttitle: " );
//# 			b.append( getTitle() );
//# 			b.append( "\n\tslot: " );
//# 			b.append( getSlot() );
//# 			b.append( "\n\tvisible: " );
//# 			b.append( isVisible() );
//# 			b.append( "\n\tdescription: " );
//# 			b.append( getDescription() );
//# 			b.append( '\n' );
//#
//# 			return b.toString();
//# 		}
	//#endif

}
