package br.com.nanogames.components.online;

import br.com.nanogames.components.util.Serializable;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;


public final class Customer implements Serializable {

	/** Id do usuário "dummy", ou seja, não há usuário registrado ainda. */
	public static final int ID_NONE = -1;

	/** Comprimento máximo do campo "primeiro nome". */
	public static final byte FIRST_NAME_MAX_CHARS = 30;
	/** Comprimento máximo do campo "último nome". */
	public static final byte LAST_NAME_MAX_CHARS = 30;

	/** Comprimento mínimo do campo "apelido". */
	public static final byte NICKNAME_MIN_CHARS = 2;
	/** Comprimento máximo do campo "apelido". */
	public static final byte NICKNAME_MAX_CHARS = 12;

	/** Comprimento mínimo do campo "email". */
	public static final byte EMAIL_MIN_CHARS = 6;
	/** Comprimento máximo do campo "email". */
	public static final byte EMAIL_MAX_CHARS = 40;

	/** Comprimento mínimo do campo "número de telefone". */
	public static final byte PHONE_NUMBER_MIN_CHARS = 8;
	/** Comprimento máximo do campo "número de telefone". */
	public static final byte PHONE_NUMBER_MAX_CHARS = 20;

	/** Comprimento mínimo do campo "password". */
	public static final byte PASSWORD_MIN_CHARS = 6;

	/** Comprimento do campo CPF. */
	public static final byte CPF_LENGTH = 11;

	public static final char GENDER_NOT_INFORMED = 'n';
	public static final char GENDER_FEMALE = 'f';
	public static final char GENDER_MALE = 'm';

	/***/
	private int id = ID_NONE;

	//#if DEBUG == "true"
//# 	
//# 	private String nickname = "dadada";
//# 	private String email = "abc@nanogames.com.br";
//# 	private String password = ".adgj6";
//# 	private String firstName = "peter";
//# 	private String lastName = "hansen";
//# 		private String cpf = "09321087761";
//# 		private String phoneNumber = "2193335052";
//# 
	//#else

		private String nickname = "MontyOnTheRun";
		private String email = "@.com.br";
		private String password = "";
		private String firstName = "";
		private String lastName = "";
		private String cpf = "";
		private String phoneNumber = "";

	//#endif
		
	private boolean rememberPassword = true;

	/** @since Nano Online 0.0.3 */
	private char gender = GENDER_NOT_INFORMED;

	/***/
	public static final long BIRTHDAY_DEFAULT_VALUE = -1;
			
	/** @since Nano Online 0.0.3 */
	private long birthDay = BIRTHDAY_DEFAULT_VALUE;

	/** @since Nano Online 0.0.31 */
	private long validateBefore = -1;

	/** @since Nano Online 0.0.31 */
	private long validatedAt = -1;


	/** 
	 * Instante da última alteração de dados do usuário. Ao editar os dados de um usuário, se esse valor for menor (ou seja,
	 * mais antigo) que o do servidor, é sinal de que o usuário fez alterações mais recentes pelo site ou por outro aparelho,
	 * logo os dados são sincronizados antes da edição.
	 * @since Nano Online 0.0.3 
	 */
	private long timeStamp;


	public final void write( DataOutputStream output ) throws Exception {
		//#if DEBUG == "true"
//# 			System.out.println( "Saving customer -> id: " + id + 
//# 									", nickname: "+ nickname +
//# 									", email: " + email +
//# 									", nome: " + firstName +
//# 									", sobrenome: " + lastName +
//# 									", validado em: " + validatedAt +
//# 									", validar até: " + validateBefore +
//# 									", timeStamp: " + timeStamp +
//# 									", aniversário: " + birthDay );
		//#endif
		output.writeInt( id );
		output.writeUTF( nickname );
		output.writeUTF( email );
		output.writeUTF( firstName );
		output.writeUTF( lastName );
		output.writeUTF( cpf );
		output.writeUTF( phoneNumber );

		output.writeBoolean( rememberPassword );
		if ( rememberPassword ) {
			output.writeUTF( password );
		}
		
		// dados gravados somente a partir do Nano Online 0.0.3
		output.writeUTF( NanoOnline.VERSION );
		output.writeChar( gender );
		output.writeLong( birthDay );
		output.writeLong( timeStamp );
		output.writeLong( validateBefore );
		output.writeLong( getValidatedAt() );
	}


	public final void read( DataInputStream input ) throws Exception {
		id = input.readInt();
		nickname = input.readUTF();
		email = input.readUTF();
		firstName = input.readUTF();
		lastName = input.readUTF();
		cpf = input.readUTF();
		phoneNumber = input.readUTF();

		rememberPassword = input.readBoolean();
		if ( rememberPassword ) {
			password = input.readUTF();
		}

		// a partir do Nano Online 0.0.3
		// lê a versão do Nano Online
		final String version = input.readUTF();
		setGender( input.readChar() );
		setBirthDay( input.readLong() );
		setTimeStamp( input.readLong() );
		setValidateBefore( input.readLong() );
		setValidatedAt( input.readLong() );
	}


	public final long getBirthDay() {
		return birthDay;
	}


	public final void setBirthDay( long birthDay ) {
		this.birthDay = birthDay;
	}


	/**
	 * Indica se o jogador já está registrado, ou seja, com um id >= 0.
	 * @return
	 */
	public final boolean isRegistered() {
		return id != ID_NONE;
	}


	public final String getEmail() {
		return email;
	}


	public final void setEmail( String email ) {
		this.email = email;
	}


	public final String getFirstName() {
		return firstName;
	}


	public final void setFirstName( String firstName ) {
		this.firstName = firstName;
	}


	public final char getGender() {
		return gender;
	}


	public final void setGender( char gender ) {
		this.gender = gender;
	}


	public final int getId() {
		return id;
	}


	public final void setId( int id ) {
		this.id = id;
	}


	public final String getLastName() {
		return lastName;
	}


	public final void setLastName( String lastName ) {
		this.lastName = lastName;
	}


	public final String getNickname() {
		return nickname;
	}


	public final void setNickname( String nickname ) {
		this.nickname = nickname;
	}


	public final String getPassword() {
		return password;
	}


	public final void setPassword( String password ) {
		this.password = password;
	}


	public final boolean isRememberPassword() {
		return rememberPassword;
	}


	public final void setRememberPassword( boolean rememberPassword ) {
		this.rememberPassword = rememberPassword;
	}


	public final String getCPF() {
		return cpf;
	}


	public final void setCPF( String cpf ) {
		this.cpf = cpf;
	}


	public final String getPhoneNumber() {
		return phoneNumber;
	}


	public final void setPhoneNumber( String phoneNumber ) {
		this.phoneNumber = phoneNumber;
	}
	
	
	public final long getTimeStamp() {
		return timeStamp;
	}
	
	
	public final void setTimeStamp( long timeStamp ) {
		this.timeStamp = timeStamp;
	}

	
	public final long getValidateBefore() {
		return validateBefore;
	}


	public final void setValidateBefore( long validateBefore ) {
		this.validateBefore = validateBefore;
	}
	
	
	public final long getValidatedAt() {
		return validatedAt;
	}


	public final void setValidatedAt( long validatedAt ) {
		this.validatedAt = validatedAt;
	}


	public final boolean hasExpirationDate() {
		return validateBefore != -1;
	}


	public final boolean isExpired() {
		return hasExpirationDate() && validateBefore < System.currentTimeMillis();
	}


	public final void parseParams( byte[] data ) throws Exception {
		DataInputStream input = null;
		try {
			input = new DataInputStream( new ByteArrayInputStream( data ) );

			byte paramId = -1;

			do {
				paramId = input.readByte();

				switch ( paramId ) {
					case ProfileScreen.PARAM_CUSTOMER_ID:
						setId( input.readInt() );
					break;

					case ProfileScreen.PARAM_NICKNAME:
						setNickname( input.readUTF() );
					break;

					case ProfileScreen.PARAM_FIRST_NAME:
						setFirstName( input.readUTF() );
					break;

					case ProfileScreen.PARAM_LAST_NAME:
						setLastName( input.readUTF() );
					break;

					case ProfileScreen.PARAM_PHONE_NUMBER:
						setPhoneNumber( input.readUTF() );
					break;

					case ProfileScreen.PARAM_EMAIL:
						setEmail( input.readUTF() );
					break;

					case ProfileScreen.PARAM_CPF:
						setCPF( input.readUTF() );
					break;

					case ProfileScreen.PARAM_GENDER:
						setGender( input.readChar() );
					break;

					case ProfileScreen.PARAM_BIRTHDAY:
						setBirthDay( input.readLong() );
					break;
					
					case ProfileScreen.PARAM_TIMESTAMP:
						setTimeStamp( input.readLong() );
					break;

					case ProfileScreen.PARAM_VALIDATE_BEFORE:
						setValidateBefore( input.readLong() );
					break;

					case ProfileScreen.PARAM_VALIDATED_AT:
						setValidatedAt( input.readLong() );
					break;
				}
			} while ( input.available() > 0 && paramId != ProfileScreen.PARAM_INFO_END );		
		} finally {
			if ( input != null ) {
				try {
					input.close();
				} catch ( Exception e ) {
				}
			}
		}
	}


	// TODO criar validadores de informações do usuário

	/**
	 * Faz a validação de um número de CPF.
	 * @param cpf
	 * @return
	 */
//	public static final boolean checkCPF( String cpf ) {
//		final char[] chars = cpf.toCharArray();
//		for ( byte i = 0; i < 10; ++i ) {
//			final byte digit = ( byte ) ( chars[i] - '0' );
//			int j = 0;
//			for ( ; j < chars.length; ++j ) {
//				if ( chars[ j ] != digit ) {
//					j = chars.length + 1;
//				}
//			}
//
//			if ( j == chars.length ) {
//				// todos os dígitos são iguais, logo é inválido
//				return false;
//			}
//		}
//
//		int d1, d2;
//		int digito1, digito2, resto;
//		int digitoCPF;
//
//		d1 = d2 = 0;
//		digito1 = digito2 = resto = 0;
//
//		for ( int nCount = 1; nCount < cpf.length() - 1; ++nCount ) {
//			digitoCPF = Integer.valueOf( cpf.substring( nCount - 1, nCount ) ).intValue();
//
//			//multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.
//			d1 = d1 + ( 11 - nCount ) * digitoCPF;
//
//			//para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.
//			d2 = d2 + ( 12 - nCount ) * digitoCPF;
//		}
//
//		//Primeiro resto da divisão por 11.
//		resto = ( d1 % 11 );
//
//		//Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
//		if ( resto < 2 ) {
//			digito1 = 0;
//		} else {
//			digito1 = 11 - resto;
//		}
//
//		d2 += 2 * digito1;
//
//		//Segundo resto da divisão por 11.
//		resto = ( d2 % 11 );
//
//		//Se o resultado for 0 ou 1 o digito é 0 caso contrário o digito é 11 menos o resultado anterior.
//		if ( resto < 2 ) {
//			digito2 = 0;
//		} else {
//			digito2 = 11 - resto;
//		}
//
//		//Digito verificador do CPF que está sendo validado.
//		final String nDigVerific = cpf.substring( cpf.length() - 2, cpf.length() );
//
//		//Concatenando o primeiro resto com o segundo.
//		final String nDigResult = String.valueOf( digito1 ) + String.valueOf( digito2 );
//
//		//comparar o digito verificador do cpf com o primeiro resto + o segundo resto.
//		return nDigVerific.equals( nDigResult );
//	}
	
}
