package br.com.nanogames.components.util;
//#if JAVA_VERSION == "ANDROID"
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.gsm.SmsManager;
import br.com.nanogames.MIDP.MainActivity;
import br.com.nanogames.components.util.SmsSenderListener;


/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class SmsSender {
    /**
     * Identificador de eventos.
     */
    public static final String ACTION_SMS_SENT = "br.com.nanogames.components.smssender.SMS_SENT_ACTION";
    /**
     * Classe cliente do envio de SMS
     */
    private static SmsSenderListener listener;    
    
    /**
     * Inicialização estática da classe.
     * Registra o listener de intent do evento de envio de SMS.
     */
    static  {
        try {
            MainActivity.getActivity().registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    String message = null;
                    boolean error = true;
                    switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        SmsSender.listener.onSMSSent( 0, true );                    
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        SmsSender.listener.onSMSSent( 0, false );
                        break;
                    }
                }
            }, new IntentFilter( ACTION_SMS_SENT ) );        
        } catch ( Throwable t ) {
            SmsSender.listener.onSMSSent( 0, false );
        }
    }
    
    
    /**
     * Envia uma mensagem de texto
     * @param number numero do telefone de destino
     * @param message a mensagem a ser enviada
     * @param listener classe cliente.
     */
    public void send(String number, String message, SmsSenderListener listener ) {
        SmsSender.listener = listener;
        try {
            SmsManager sms = SmsManager.getDefault();        
            sms.sendTextMessage( number, null, message, PendingIntent.getBroadcast( MainActivity.getActivity(), 0, new Intent( ACTION_SMS_SENT ), 0 ), null);            
        } catch ( Throwable t ) {
            SmsSender.listener.onSMSSent( 0, false );
        }
    }    
}
//#else
//# /**
//#  * SmsSender.java
//#  * Â©2008 Nano Games.
//#  *
//#  * Created on 22/07/2008 18:06:46.
//#  */
//# 
//# /**
//#  * @author Daniel L. Alves
//#  */
//# 
//# import br.com.nanogames.components.userInterface.AppMIDlet;
//# import br.com.nanogames.components.userInterface.ScreenManager;
//# import javax.microedition.io.Connector;
//# import javax.wireless.messaging.MessageConnection;
//# import javax.wireless.messaging.TextMessage;
//# 
//#if BLACKBERRY_API == "true"
//# import net.rim.device.api.ui.Keypad;
//#endif
//# 
//#if SAMSUNG_API == "true"
//# import com.samsung.util.SM;
//# import com.samsung.util.SMS;
//#endif
//# 
//# 
//# /** Classe responsÃ¡vel pelo envio de mensagens SMS */
//# public final class SmsSender extends Thread
//# {
//# 	/** Tempo mÃ¡ximo pelo qual esperamos a thread de envio de SMS terminar suas tarefas */
//# 	public static final short SMS_SENDER_MAX_WAIT_TIME = 25000;
//# 	
//# 	/** Thread de envio de SMS deste objeto */
//# 	private SmsThread smsThread;
//# 	
//# 	/** Objeto que receberÃ¡ informaÃ§Ãµes sobre o andamento do envio do SMS. Esta referÃªncia tambÃ©m Ã© armazenada
//# 	 * nesta classe (e nÃ£o apenas em SmsThread) para que o mesmo objeto possa ser chamado pela thread de envio
//# 	 * de SMS e pela thread de timeout da conexÃ£o
//# 	 */
//# 	private SmsSenderListener listener;
//# 	
//# 	/** Controla o acesso Ã s regiÃµes crÃ­ticas da operaÃ§Ã£o de envio de SMS */
//# 	private final Mutex mutex = new Mutex();
//# 	
//# 	/** Retorna se o device suporta o envio de SMS */
//# 	public static final boolean isSupported()
//# 	{
		//#if SAMSUNG_API == "true"
//# 			// Ã importante lembrar que nem todos aparelhos da Samsung suportam a prÃ³pria API da Samsung!
//# 			// A preferÃªncia Ã© pela API WMA do J2ME, jÃ¡ que na bateria de testes mostrou ser implementada por
//# 			// um maior nÃºmero de devices desta fabricante
//# 			if( !supportsDefault() )
//# 			{
//# 				try
//# 				{
//# 					return SMS.isSupported();
//# 				}
//# 				catch( Throwable t )
//# 				{
//# 					return false;
//# 				}
//# 			}
//# 			return true;
		//#elif BLACKBERRY_API == "true"
//# 			// em aparelhos antigos (e/ou com versÃµes de software antigos - confirmar!), o popup do aparelho ao tentar
//# 			// enviar um sms nÃ£o recebe os eventos de teclado corretamente, sumindo somente apÃ³s a aplicaÃ§Ã£o ser encerrada
//# 			switch ( Keypad.getHardwareLayout() ) {
//# 				case ScreenManager.HW_LAYOUT_32:
//# 				case ScreenManager.HW_LAYOUT_PHONE:
//# 				case ScreenManager.HW_LAYOUT_REDUCED:
//# 					return false;
//# 					
//# 				default:
//# 					return supportsDefault();
//# 			}
		//#else
//# 			return supportsDefault();
		//#endif
//# 	}
//# 
//# 	
//# 	/** Retorna se o device suporta o envio de SMS atravÃ©s das APIs opcionais do J2ME ( WMA - Wireless Messaging API )*/
//# 	private static final boolean supportsDefault() {
//# 		// resolve verification error em alguns aparelhos BlackBerry
		//#if BLACKBERRY_API == "true"
//# 			return true;
		//#else
//# 			try
//# 			{
//# 				Class.forName( "javax.wireless.messaging.MessageConnection" );
//# 				Class.forName( "javax.wireless.messaging.TextMessage" );
//# 				return true;
//# 			}
//# 			catch( Throwable t )
//# 			{
//# 				return false;
//# 			}
		//#endif
//# 	}
//# 
//# 	
//# 	/** Envia uma mensagem SMS
//# 	 * @param phoneNumber Telefone para o qual a mensagem serÃ¡ enviada
//# 	 * @param smsText Texto da mensagem SMS
//# 	 * @param listener Objeto que receberÃ¡ informaÃ§Ãµes sobre o andamento do envio do SMS
//# 	 */
//# 	public final void send( String phoneNumber, String smsText, SmsSenderListener listener )
//# 	{
//# 		mutex.acquire();
//# 
//# 		// Termina alguma thread de envio de SMS que possa estar ativa
//# 		killSmsThread();
//# 
//# 		// Troca o listener apenas depois de chamar killSmsThread(), jÃ¡ que tal mÃ©todo pode querer invocar
//# 		// o listener anterior
//# 		this.listener = listener;
//# 		
//# 		// Cria uma thread para o envio do SMS. AlÃ©m de ser uma melhor prÃ¡tica de programaÃ§Ã£o, se torna 
//# 		// OBRIGATÃRIO, pois alguns devices (i.e A1200) disparam uma exceÃ§Ã£o quando tentamos executar uma
//# 		// operaÃ§Ã£o bloqueante na thread principal da aplicaÃ§Ã£o
//# 		smsThread = new SmsThread( phoneNumber, smsText, listener, this, mutex );
//# 		smsThread.setPriority( Thread.MAX_PRIORITY );
//# 		smsThread.start();
//# 		
//# 		mutex.release();
//# 	}
//# 	
//# 	/** Controla o timeout da conexÃ£o de envio de SMS. Se faz necessÃ¡rio pois em alguns devices (i.e. LG KG800,
//# 	 * LG MG320c) a exceÃ§Ã£o de timeout da conexÃ£o nÃ£o Ã© disparada. Uma das razÃµes para isso acontecer Ã© que nem
//# 	 * mesmo o padrÃ£o J2ME obriga o fabricante a fazÃª-lo (vide documentaÃ§Ã£o da classe MessageConnection)
//# 	 */
//# 	public final void run()
//# 	{
		//#if DEBUG == "true"
//# 		if( listener != null )
//# 			listener.onSMSLog( smsThread.getID(), "\n\nChamou o timer" );
		//#endif
//# 
//# 		try
//# 		{
//# 			sleep( SMS_SENDER_MAX_WAIT_TIME );
//# 		}
//# 		catch( Exception ex )
//# 		{
//# 			// Pode acontecer de sleep() disparar uma exceÃ§Ã£o e acabarmos chamando killSmsThread() sem esperarmos
//# 			// um tempo razoÃ¡vel para o envio do SMS. Como na prÃ¡tica o disparo de uma exceÃ§Ã£o por sleep() se
//# 			// mostrou inexistente, ignoramos a possibilidade
//# 		}
//# 
//# 		mutex.acquire();
//# 		
//# 		if( killSmsThread() && ( listener != null ) )
//# 		{
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "Erro: ForÃ§ou o fim da execuÃ§Ã£o" );
			//#endif
//# 
//# 			listener.onSMSSent( smsThread.getID(), false );
//# 		}
//# 		
//# 		mutex.release();
//# 	}
//# 	
//# 
//# 	/** Tenta forÃ§ar o tÃ©rmino da thread de envio de SMS. O ideal seria se pudÃ©ssemos chamar Thread.interrupt()
//# 	 * em todos os devices. Mas como tal mÃ©todo sÃ³ estÃ¡ presente a partir do CLDC 1.1 e nÃ£o conseguimos pegar
//# 	 * a versÃ£o do CLDC em tempo de execuÃ§Ã£o, tentamos contornar o problema forÃ§ando o fim da conexÃ£o. Nos
//# 	 * aparelhos LG KG800 e MG320c foi descoberta a possibilidade de um erro: caso o usuÃ¡rio tente enviar um SMS
//# 	 * e nÃ£o possua crÃ©ditos para fazÃª-lo, a thread de envio ficarÃ¡ eternamente na memÃ³ria, jÃ¡ que o mÃ©todo
//# 	 * MessageConnection.send() nÃ£o dispara exceÃ§Ãµes por timeout e/ou conexÃ£o fechada.
//# 	 * 
//# 	 * O mutex Ã© sempre utilizado por fora desse mÃ©todo
//# 	 * 
//# 	 * @return Se teve ou nÃ£o que matar a thread
//# 	 */
//# 	private final boolean killSmsThread() {
		//#if DEBUG == "true"
//# 		if( listener != null )
//# 			listener.onSMSLog( smsThread.getID(), "\n\nkillSmsThread():" );
		//#endif
//# 
//# 		if( ( smsThread != null ) && ( smsThread.isAlive() ) ) {
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "\n- ForÃ§ou o fechamento da conexÃ£o" );
			//#endif
//# 
//# 			smsThread.closeSmsConnection();
//# 			return true;
//# 		}
		//#if DEBUG == "true"
//# 		else {
//# 			if( listener != null )
//# 				listener.onSMSLog( smsThread.getID(), "\n- ConexÃ£o jÃ¡ estava fechada" );
//# 		}
		//#endif
//# 		return false;
//# 	}
//# 	
//# 	/** Thread responsÃ¡vel pelo envio do SMS */
//# 	private static final class SmsThread extends Thread {
//# 		/** Telefone para o qual a mensagem serÃ¡ enviada */
//# 		private final String phoneNumber;
//# 			
//# 		/** Texto da mensagem SMS */
//# 		private final String smsText;
//# 
//# 		/** Objeto que receberÃ¡ informaÃ§Ãµes sobre o andamento do envio do SMS */
//# 		private final SmsSenderListener listener;
//# 		
//# 		/** ConexÃ£o atravÃ©s da qual o SMS serÃ¡ enviado. MantÃ©m esta referÃªncia na classe para que seja possÃ­vel
//# 		 * fechar a conexÃ£o por fora do mÃ©todo que a criou. SÃ³ Ã© utilizada em conjunto com a API WMA. A API
//# 		 * da Samsung nÃ£o faz uso deste atributo
//# 		 */
//# 		private MessageConnection smsConn;
//# 
//# 		/** Timer que irÃ¡ cancelar a conexÃ£o de envio de SMS caso o tempo limite seja atingido. Se faz necessÃ¡rio
//# 		 * pois em alguns devices (i.e. LG KG800, LG MG320c) a exceÃ§Ã£o de timeout da conexÃ£o nÃ£o Ã© disparada. Uma
//# 		 * das razÃµes para isso acontecer Ã© que nem mesmo o padrÃ£o J2ME obriga o fabricante a fazÃª-lo (vide
//# 		 * documentaÃ§Ã£o da classe MessageConnection). No entanto, sÃ³ Ã© utilizado em conjunto com a API WMA.
//# 		 * A API da Samsung nÃ£o faz uso deste atributo
//# 		 */
//# 		private final Thread timeoutThread;
//# 
//# 		/** Controla o acesso Ã s regiÃµes crÃ­ticas da operaÃ§Ã£o de envio de SMS */
//# 		private final Mutex mutex;
//# 		
//# 		/** VariÃ¡vel responsÃ¡vel por gerar os ids das threads de envio de SMS */
//# 		private static int idCounter = Integer.MIN_VALUE;
//# 		
//# 		/** ID Ãºnico desta thread */
//# 		private final int id;
//# 		
//# 		/** Cria uma thread de envio de SMS
//# 		 * @param phoneNumber Telefone para o qual a mensagem serÃ¡ enviada
//# 		 * @param smsText Texto da mensagem SMS
//# 		 * @param listener Objeto que receberÃ¡ informaÃ§Ãµes sobre o andamento do envio do SMS
//# 		 * @param timeoutThread Thread que controlarÃ¡ o timeout da conexÃ£o
//# 		 * @param mutex Controlador de acesso Ã s regiÃµes crÃ­ticas da operaÃ§Ã£o de envio de SMS
//# 		 */
//# 		public SmsThread( String phoneNumber, String smsText, SmsSenderListener listener, Thread timeoutThread, Mutex mutex ) {
//# 			this.phoneNumber = phoneNumber;
//# 			this.smsText = smsText;
//# 			this.listener = listener;
//# 			this.timeoutThread = timeoutThread;
//# 			this.mutex = mutex;
//# 			id = newID();
//# 		}
//# 		
//# 		/** Retorna um ID Ãºnico para o objeto chamador */
//# 		private static synchronized final int newID() {
//# 			return idCounter++;
//# 		}
//# 		
//# 		/** ObtÃ©m o ID Ãºnico da thread */
//# 		public final int getID() {
//# 			return id;
//# 		}
//# 		
		//#if SAMSUNG_API == "true"
//# 			public final void run() {
//# 				// Verifica qual mÃ©todo de execuÃ§Ã£o deve ser chamado de acordo com as APIs suportadas pelo device
//# 				// Ã importante lembrar que nem todos aparelhos da Samsung suportam a prÃ³pria API da Samsung!
//# 				// Ver mÃ©todo SmsSender.isSupported()
//# 				if( supportsDefault() )
//# 					runDefault();
//# 				else
//# 					runSamsung();
//# 			}
//# 
//# 			/** Envia um SMS utilizando a API da Samsung. NÃ£o precisa utilizar mutex!!! */
//# 			private void runSamsung() {
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n\nrunSamsung():" );
				//#endif
//# 			
//# 				boolean ret = true;
//# 
//# 				try {
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Criando a mensagem" );
					//#endif
//# 
//# 					final SM message = new SM();
//# 					message.setData( smsText );
//# 					message.setDestAddress( phoneNumber );
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Enviando sms" );
					//#endif
//# 
//# 					SMS.send( message );
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- SMS enviado" );
					//#endif
//# 				} catch( Throwable t ) {
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Disparou uma excecao ao tentar enviar o sms:" + t.getMessage() );
					//#endif
//# 					
//# 					ret = false;
//# 				}
//# 
//# 				if( listener != null )
//# 					listener.onSMSSent( getID(), ret );
//# 			}
		//#else
//# 			public final void run() {
//# 				runDefault();
//# 			}
		//#endif
//# 		
//# 		/** Envia um SMS utilizando a API opcional WMA do J2ME */
//# 		private final void runDefault() {
			//#if DEBUG == "true"
//# 			if( listener != null )
//# 				listener.onSMSLog( getID(), "\n\nrunDefault():" );
			//#endif
//# 			
//# 			boolean ret = true;
//# 
//# 			try {
//# 				mutex.acquire();
//# 
//# 				smsConn = null;
//# 
//# 				// SÃ³ queremos enviar
//# 				//final String smsConnection = "sms://:" + midlet.getAppProperty( "SMS-Port" );
//# 				final String smsConnection = "sms://" + phoneNumber;
//# 
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Abrindo a conexÃ£o" );
				//#endif
//# 
//# 				smsConn = ( MessageConnection )Connector.open( smsConnection, Connector.WRITE, true );
//# 
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Criando a mensagem" );
				//#endif
//# 
//# 				final TextMessage sms = ( TextMessage )smsConn.newMessage( MessageConnection.TEXT_MESSAGE );
//# 
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Determinando o texto da mensagem: " + smsText );
				//#endif
//# 
//# 				sms.setPayloadText( smsText  );
//# 
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Setando o timer" );
				//#endif
//# 
//# 				timeoutThread.start();
//# 
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Enviando o sms" );
				//#endif
//# 				
//# 				mutex.release();
//# 
//# 				// O mÃ©todo MessageConnection.send() pode causar alguns problemas:
//# 				// - Samsung: Alguns Samsungs (i.e. D820) nÃ£o disparam execeÃ§Ãµes quando o usuÃ¡rio tenta enviar 
//# 				// um SMS sem possuir crÃ©ditos. Logo, retornaremos que o SMS foi enviado sem isso ter ocorrido
//# 				// de verdade
//# 				// - LG: Caso o usuÃ¡rio tente enviar um SMS e nÃ£o possua crÃ©ditos para fazÃª-lo, a execuÃ§Ã£o desta
//# 				// thread ficarÃ¡ eternamente bloqueada dentro de send() em alguns devices (i.e. KG800, MG320c).
//# 				// Ver SmsSender.killSmsThread() para maiores informaÃ§Ãµes sobre o workaround utilizado
//# 				smsConn.send( sms );
//# 			} catch( Throwable t ) {
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Erro:" + t.getMessage() );
				//#endif
//# 				
//# 				ret = false;
//# 			} finally {
//# 				closeSmsConnection();
//# 			}
//# 
//# 			if( listener != null )
//# 				listener.onSMSSent( getID(), ret );
//# 			
//# 			mutex.release();
//# 		}
//# 		
//# 		/** Fecha a conexÃ£o de envio de SMS desta thread. Este mÃ©todo Ã© pÃºblico para que seja possÃ­vel
//# 		 * forÃ§ar o fechamento da conexÃ£o por fora do objeto
//# 		 * 
//# 		 * O mutex Ã© sempre utilizado por fora desse mÃ©todo
//# 		 */
//# 		public final void closeSmsConnection() {
//# 			if( smsConn != null ) {
				//#if DEBUG == "true"
//# 				if( listener != null )
//# 					listener.onSMSLog( getID(), "\n- Fechando a conexÃ£o" );
				//#endif
//# 
//# 				try {
//# 					smsConn.close();
//# 
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- ConexÃ£o fechada" );
					//#endif
//# 
//# 					smsConn = null;
//# 					AppMIDlet.gc();
//# 					yield();
//# 				} catch( Throwable t ) {
					//#if DEBUG == "true"
//# 					if( listener != null )
//# 						listener.onSMSLog( getID(), "\n- Erro ao fechar a conexÃ£o:" + t.getMessage() );
					//#endif
//# 					
//# 					// Pode nÃ£o conseguir fechar a conexÃ£o e esssa ficar eternamente aberta, no entanto isso
//# 					// nÃ£o deveria acontecer
//# 				}
//# 			}
//# 		}
//# 	}
//# }
//#endif