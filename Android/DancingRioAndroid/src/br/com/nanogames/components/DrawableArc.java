/**
 * DrawableArc.java
 *
 * Created on Oct 11, 2010 10:57:40 AM
 *
 */

package br.com.nanogames.components;

import javax.microedition.lcdui.Graphics;

/**
 *
 * @author peter
 */
public class DrawableArc extends Drawable {

	/** Ângulo inicial do arco, em graus. */
	private short startAngle;

	/** Ângulo final do arco, em graus. */
	private short endAngle = 360;

	/** Cor de preenchimento do arco. */
	private int color;


	public DrawableArc() {
	}
	

	public DrawableArc( int color ) {
		setColor( color );
	}


	protected void paint( Graphics g ) {
		g.setColor( color );
		g.fillArc( translate.x, translate.y, size.x, size.y, startAngle, endAngle );
	}


	public int getColor() {
		return color;
	}


	public void setColor( int color ) {
		this.color = color;
	}


	public short getEndAngle() {
		return endAngle;
	}


	public void setEndAngle( int endAngle ) {
		this.endAngle = ( short ) endAngle;
	}


	public short getStartAngle() {
		return startAngle;
	}


	public void setStartAngle( int startAngle ) {
		this.startAngle = ( short ) startAngle;
	}



}
