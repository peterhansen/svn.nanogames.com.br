//#if JAVA_VERSION == "ANDROID"
package br.com.nanostudio.dancingrio;

import android.os.Bundle;
import javax.microedition.io.ResourceDictionary;
import core.GameMIDlet;

public class MainActivity extends br.com.nanogames.MIDP.MainActivity
{
      public void setupAssetsForNanoGamesSplash() {            
            ResourceDictionary.addEntry( "/splash/transparency.png", R.drawable.transparency );        
            ResourceDictionary.addEntry( "/splash/nano.png", R.drawable.nano );        
            ResourceDictionary.addEntry( "/splash/light.png", R.drawable.light );        
            ResourceDictionary.addEntry( "/splash/games.png", R.drawable.games );
            ResourceDictionary.addEntry( "/splash/turtle.png", R.drawable.turtle );
            ResourceDictionary.addEntry( "/splash/font_credits.png", R.drawable.font_credits );
            ResourceDictionary.addEntry( "/splash/font_credits.bin", R.raw.font_credits );
        }
        
//        public void setupAssetsForNanoStudioSplash() {
//            ResourceDictionary.addEntry( "/splash/nanostudio.png", R.drawable.nanostudio );        
//            ResourceDictionary.addEntry( "/splash/font_credits.png", R.drawable.font_credits );
//            ResourceDictionary.addEntry( "/splash/font_credits.bin", R.raw.font_credits );        
//            ResourceDictionary.addEntry( "/splash/transparency.png", R.drawable.transparency );
//        }          
      
        public void setupAssetsForNanoOnline() {
                    
            ResourceDictionary.addEntry( "/online/clear.png", R.drawable.clear );
            ResourceDictionary.addEntry( "/online/shift.png", R.drawable.shift );        
            ResourceDictionary.addEntry( "/online/bar.png", R.drawable.nobar );
            ResourceDictionary.addEntry( "/online/bl.png", R.drawable.bl );
            ResourceDictionary.addEntry( "/online/bt.png", R.drawable.bt );
            ResourceDictionary.addEntry( "/online/c.png", R.drawable.c );        
            ResourceDictionary.addEntry( "/online/d.png", R.drawable.d );
            ResourceDictionary.addEntry( "/online/font_0.png", R.drawable.nfont_0 );
            ResourceDictionary.addEntry( "/online/perfil.png", R.drawable.perfil );
            ResourceDictionary.addEntry( "/online/pr.png", R.drawable.pr );
            ResourceDictionary.addEntry( "/online/pt.png", R.drawable.pt );        
            ResourceDictionary.addEntry( "/online/t.png", R.drawable.t );
            ResourceDictionary.addEntry( "/online/0.dat", R.raw.l0 );
            ResourceDictionary.addEntry( "/online/1.dat", R.raw.l1 );
            ResourceDictionary.addEntry( "/online/2.dat", R.raw.l2 );            
            ResourceDictionary.addEntry( "/online/font_0.bin", R.raw.nfont_0 );        

        }
    
    @Override
    public void loadResources( Bundle mainBundle )
    {
        setContentView( R.layout.main );  
        super.startMIDlet( GameMIDlet.class.getName() );
        
        setupAssetsForNanoGamesSplash();
        
        setupAssetsForNanoOnline();


        ResourceDictionary.addEntry( "/splash/font_credits_big.png", R.drawable.font_credits_big );        
        ResourceDictionary.addEntry( "/splash/font_credits_big.bin", R.raw.font_credits_big );        
        
        ResourceDictionary.addEntry( "/2.png", R.drawable.a2 );
        ResourceDictionary.addEntry( "/4.png", R.drawable.a4 );
        ResourceDictionary.addEntry( "/6.png", R.drawable.a6 );
        ResourceDictionary.addEntry( "/8.png", R.drawable.a8 );
        
        ResourceDictionary.addEntry( "/clear.png", R.drawable.clear );
        
        
        ResourceDictionary.addEntry( "/cursorMenu.png", R.drawable.cursormenu );
        ResourceDictionary.addEntry( "/cursorMenu_0.png", R.drawable.cursormenu_0 );
        ResourceDictionary.addEntry( "/cursorMenu_1.png", R.drawable.cursormenu_1 );
        
        
        ResourceDictionary.addEntry( "/46x36.png", R.drawable.icon46x36 );
        ResourceDictionary.addEntry( "/60x40.png", R.drawable.icon60x40 );
        
        
        ResourceDictionary.addEntry( "/aim.png", R.drawable.aim );
        ResourceDictionary.addEntry( "/aim_0.png", R.drawable.aim_0 );
        ResourceDictionary.addEntry( "/aim_1.png", R.drawable.aim_1 );
        ResourceDictionary.addEntry( "/aim_2.png", R.drawable.aim_2 );        
        
        ResourceDictionary.addEntry( "/barritcha.png", R.drawable.barritcha );        
        
        ResourceDictionary.addEntry( "/x4.png", R.drawable.x4 );        
        ResourceDictionary.addEntry( "/x.png", R.drawable.x );        
        
        ResourceDictionary.addEntry( "/s.png", R.drawable.s );
        ResourceDictionary.addEntry( "/shine.png", R.drawable.shine );
        ResourceDictionary.addEntry( "/shine_0.png", R.drawable.shine_0 );
        ResourceDictionary.addEntry( "/shine_1.png", R.drawable.shine_1 );
        ResourceDictionary.addEntry( "/shine_2.png", R.drawable.shine_2 );
        ResourceDictionary.addEntry( "/shine.bin", R.raw.shine );   
        
        ResourceDictionary.addEntry( "/right.png", R.drawable.right );
        ResourceDictionary.addEntry( "/right_0.png", R.drawable.right_0 );
        ResourceDictionary.addEntry( "/right_1.png", R.drawable.right_1 );
        
        
        ResourceDictionary.addEntry( "/rastro2.png", R.drawable.rastro2 );
        ResourceDictionary.addEntry( "/rastro4.png", R.drawable.rastro4 );
        ResourceDictionary.addEntry( "/rastro6.png", R.drawable.rastro6 );
        ResourceDictionary.addEntry( "/rastro8.png", R.drawable.rastro8 );
        
        
        ResourceDictionary.addEntry( "/q.png", R.drawable.q );
        ResourceDictionary.addEntry( "/q_7.png", R.drawable.q_7 );
        ResourceDictionary.addEntry( "/q_6.png", R.drawable.q_6 );
        ResourceDictionary.addEntry( "/q_5.png", R.drawable.q_5 );
        ResourceDictionary.addEntry( "/q_4.png", R.drawable.q_4 );
        ResourceDictionary.addEntry( "/q_3.png", R.drawable.q_3 );
        ResourceDictionary.addEntry( "/q_2.png", R.drawable.q_2 );
        ResourceDictionary.addEntry( "/q_1.png", R.drawable.q_1 );
        ResourceDictionary.addEntry( "/q_0.png", R.drawable.q_0 );
        
        
        ResourceDictionary.addEntry( "/interface5.png", R.drawable.interface5 );        
        ResourceDictionary.addEntry( "/interface4.png", R.drawable.interface4 );                
        ResourceDictionary.addEntry( "/interface3.png", R.drawable.interface3 );        
        ResourceDictionary.addEntry( "/interface2.png", R.drawable.interface2 );        
        ResourceDictionary.addEntry( "/interface1.png", R.drawable.interface1 );                
        
        
        ResourceDictionary.addEntry( "/corpo.png", R.drawable.corpo );        
        ResourceDictionary.addEntry( "/chapeu.png", R.drawable.chapeu );
        ResourceDictionary.addEntry( "/caret.png", R.drawable.caret );
        
        ResourceDictionary.addEntry( "/camisa.png", R.drawable.camisa );   
        ResourceDictionary.addEntry( "/calca.png", R.drawable.calca );   
        ResourceDictionary.addEntry( "/cabelo.png", R.drawable.cabelo );   
        ResourceDictionary.addEntry( "/bomba.png", R.drawable.bomba );   
        ResourceDictionary.addEntry( "/acessorios.png", R.drawable.acessorios );   
        
        
        ResourceDictionary.addEntry( "/aim.bin", R.raw.aim );   
        ResourceDictionary.addEntry( "/cursorMenu.bin", R.raw.cursormenu );   
        ResourceDictionary.addEntry( "/q.bin", R.raw.q );   
        ResourceDictionary.addEntry( "/right.bin", R.raw.right );   

        
        ResourceDictionary.addEntry( "/Bkg/monstros.bin", R.raw.monstros );           
        ResourceDictionary.addEntry( "/Bkg/morcego.bin", R.raw.morcego );   
        ResourceDictionary.addEntry( "/Bkg/multidaolapa.bin", R.raw.multidaolapa );   
        ResourceDictionary.addEntry( "/Bkg/predioxx.bin", R.raw.predioxx );   
        ResourceDictionary.addEntry( "/Bkg/vendedor.bin", R.raw.vendedor );   
        
        ResourceDictionary.addEntry( "/Bkg/arcos.png", R.drawable.arcos );   
        ResourceDictionary.addEntry( "/Bkg/arvore.png", R.drawable.arvore );   
        ResourceDictionary.addEntry( "/Bkg/arvoreCemiterio.png", R.drawable.arvorecemiterio );   
        ResourceDictionary.addEntry( "/Bkg/placaNano.png", R.drawable.placanano );   
        
        ResourceDictionary.addEntry( "/Bkg/auxchao.png", R.drawable.auxchao );   
        ResourceDictionary.addEntry( "/Bkg/bondinho.png", R.drawable.bondinho );   
        ResourceDictionary.addEntry( "/Bkg/calcada.png", R.drawable.calcada );   
        ResourceDictionary.addEntry( "/Bkg/chao.png", R.drawable.chao );   
        ResourceDictionary.addEntry( "/Bkg/cristo.png", R.drawable.cristo );   
        ResourceDictionary.addEntry( "/Bkg/frentefundo.png", R.drawable.frentefundo );   
        ResourceDictionary.addEntry( "/Bkg/lapide.png", R.drawable.lapide );   
        ResourceDictionary.addEntry( "/Bkg/lapide2.png", R.drawable.lapide2 );   
        
        ResourceDictionary.addEntry( "/Bkg/monstros.png", R.drawable.monstros );   
        ResourceDictionary.addEntry( "/Bkg/monstros_0.png", R.drawable.monstros_0 );   
        ResourceDictionary.addEntry( "/Bkg/monstros_1.png", R.drawable.monstros_1 );   
        ResourceDictionary.addEntry( "/Bkg/morcego.png", R.drawable.morcego );           
        ResourceDictionary.addEntry( "/Bkg/morcego_0.png", R.drawable.morcego_0 );   
        ResourceDictionary.addEntry( "/Bkg/morcego_1.png", R.drawable.morcego_1 );   
        ResourceDictionary.addEntry( "/Bkg/morro.png", R.drawable.morro );   
        ResourceDictionary.addEntry( "/Bkg/multidaolapa_0.png", R.drawable.multidaolapa_0 );   
        ResourceDictionary.addEntry( "/Bkg/multidaolapa_1.png", R.drawable.multidaolapa_1 );   
        ResourceDictionary.addEntry( "/Bkg/multidaolapa.png", R.drawable.multidaolapa );   
        
        ResourceDictionary.addEntry( "/Bkg/muro.png", R.drawable.muro );   
        ResourceDictionary.addEntry( "/Bkg/nuvem1.png", R.drawable.nuvem1 );           
        ResourceDictionary.addEntry( "/Bkg/pattern_menu.png", R.drawable.pattern_menu );           
        ResourceDictionary.addEntry( "/Bkg/pattern_sky.png", R.drawable.pattern_sky );           
        ResourceDictionary.addEntry( "/Bkg/pattern_tira.png", R.drawable.pattern_tira );           
        ResourceDictionary.addEntry( "/Bkg/patternfundomenu.png", R.drawable.patternfundomenu );           
        
        ResourceDictionary.addEntry( "/Bkg/pino.png", R.drawable.pino );           
        ResourceDictionary.addEntry( "/Bkg/portao.png", R.drawable.portao );           
        
        ResourceDictionary.addEntry( "/Bkg/predioxx.png", R.drawable.predioxx );             
        ResourceDictionary.addEntry( "/Bkg/predioxx_0.png", R.drawable.predioxx_0 );           
        ResourceDictionary.addEntry( "/Bkg/predioxx_1.png", R.drawable.predioxx_1 );
        
        
        ResourceDictionary.addEntry( "/Bkg/traschao.png", R.drawable.traschao );
        
        ResourceDictionary.addEntry( "/Bkg/vendedor.png", R.drawable.vendedor );        
        ResourceDictionary.addEntry( "/Bkg/vendedor_0.png", R.drawable.vendedor_0 );
        ResourceDictionary.addEntry( "/Bkg/vendedor_1.png", R.drawable.vendedor_1 );
        ResourceDictionary.addEntry( "/Bkg/vendedor_2.png", R.drawable.vendedor_2 );
        ResourceDictionary.addEntry( "/Bkg/vendedor_3.png", R.drawable.vendedor_3 );
        
       
        ResourceDictionary.addEntry( "/border/b.png", R.drawable.b_b );
        ResourceDictionary.addEntry( "/border/bl.png", R.drawable.b_bl );
        ResourceDictionary.addEntry( "/border/br.png", R.drawable.b_br );
        ResourceDictionary.addEntry( "/border/l.png", R.drawable.b_l );
        ResourceDictionary.addEntry( "/border/r.png", R.drawable.b_r );
        ResourceDictionary.addEntry( "/border/t.png", R.drawable.b_t );        
        ResourceDictionary.addEntry( "/border/tl.png", R.drawable.b_tl );
        ResourceDictionary.addEntry( "/border/tr.png", R.drawable.b_tr );
        
        ResourceDictionary.addEntry( "/Caveira/caveirex.png", R.drawable.caveirex );
        ResourceDictionary.addEntry( "/Caveira/caveirex_0.png", R.drawable.caveirex_0 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_1.png", R.drawable.caveirex_1 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_2.png", R.drawable.caveirex_2 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_3.png", R.drawable.caveirex_3 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_4.png", R.drawable.caveirex_4 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_5.png", R.drawable.caveirex_5 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_6.png", R.drawable.caveirex_6 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_7.png", R.drawable.caveirex_7 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_8.png", R.drawable.caveirex_8 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_9.png", R.drawable.caveirex_9 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_10.png", R.drawable.caveirex_10 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_11.png", R.drawable.caveirex_11 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_12.png", R.drawable.caveirex_12 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_13.png", R.drawable.caveirex_13 );                
        ResourceDictionary.addEntry( "/Caveira/caveirex_14.png", R.drawable.caveirex_14 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_15.png", R.drawable.caveirex_15 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_16.png", R.drawable.caveirex_16 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_17.png", R.drawable.caveirex_17 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_18.png", R.drawable.caveirex_18 );
        ResourceDictionary.addEntry( "/Caveira/caveirex_19.png", R.drawable.caveirex_19 );
        ResourceDictionary.addEntry( "/Caveira/caveirex.bin", R.raw.caveirex );
        
        ResourceDictionary.addEntry( "/fonts/0.png", R.drawable.f0 );
        ResourceDictionary.addEntry( "/fonts/1.png", R.drawable.f1 );
        ResourceDictionary.addEntry( "/fonts/2.png", R.drawable.f2 );
        ResourceDictionary.addEntry( "/fonts/3.png", R.drawable.f3 );
        ResourceDictionary.addEntry( "/fonts/4.png", R.drawable.f4 );
        ResourceDictionary.addEntry( "/fonts/5.png", R.drawable.f5 );
        ResourceDictionary.addEntry( "/fonts/6.png", R.drawable.f6 );
        ResourceDictionary.addEntry( "/fonts/7.png", R.drawable.f7 );
        ResourceDictionary.addEntry( "/fonts/8.png", R.drawable.f8 );
        ResourceDictionary.addEntry( "/fonts/9.png", R.drawable.f9 );
        ResourceDictionary.addEntry( "/fonts/10.png", R.drawable.f10 );
        ResourceDictionary.addEntry( "/fonts/11.png", R.drawable.f11 );

        
        ResourceDictionary.addEntry( "/fonts/0.bin", R.raw.f0 );
        ResourceDictionary.addEntry( "/fonts/1.bin", R.raw.f1 );
        ResourceDictionary.addEntry( "/fonts/2.bin", R.raw.f2 );
        ResourceDictionary.addEntry( "/fonts/3.bin", R.raw.f3 );
        ResourceDictionary.addEntry( "/fonts/4.bin", R.raw.f4 );
        ResourceDictionary.addEntry( "/fonts/5.bin", R.raw.f5 );
        ResourceDictionary.addEntry( "/fonts/6.bin", R.raw.f6 );
        ResourceDictionary.addEntry( "/fonts/7.bin", R.raw.f7 );
        ResourceDictionary.addEntry( "/fonts/8.bin", R.raw.f8 );
        ResourceDictionary.addEntry( "/fonts/9.bin", R.raw.f9 );
        ResourceDictionary.addEntry( "/fonts/10.bin", R.raw.f10 );
        ResourceDictionary.addEntry( "/fonts/11.bin", R.raw.f11 );        
        
        ResourceDictionary.addEntry( "/Shu/funkeira.png", R.drawable.funkeira );        
        ResourceDictionary.addEntry( "/Shu/funkeira_0.png", R.drawable.funkeira_0 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_1.png", R.drawable.funkeira_1 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_2.png", R.drawable.funkeira_2 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_3.png", R.drawable.funkeira_3 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_4.png", R.drawable.funkeira_4 );                
        ResourceDictionary.addEntry( "/Shu/funkeira_5.png", R.drawable.funkeira_5 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_6.png", R.drawable.funkeira_6 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_7.png", R.drawable.funkeira_7 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_8.png", R.drawable.funkeira_8 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_9.png", R.drawable.funkeira_9 );                
        ResourceDictionary.addEntry( "/Shu/funkeira_10.png", R.drawable.funkeira_10 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_11.png", R.drawable.funkeira_11 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_12.png", R.drawable.funkeira_12 );        
        ResourceDictionary.addEntry( "/Shu/funkeira_13.png", R.drawable.funkeira_13 );        
        
        ResourceDictionary.addEntry( "/Shu/funkeira.bin", R.raw.funkeira );        
        
        ResourceDictionary.addEntry( "/en.dat", R.raw.en );        
        ResourceDictionary.addEntry( "/pt.dat", R.raw.pt );        
        
        ResourceDictionary.addEntry( "/SplashGame/d.png", R.drawable.sd );        
        
        ResourceDictionary.addEntry( "/SplashGame/r.png", R.drawable.r );        
        
        ResourceDictionary.addEntry( "/SplashGame/pandeiro.png", R.drawable.pandeiro );                
        ResourceDictionary.addEntry( "/SplashGame/pandeiro_0.png", R.drawable.pandeiro_0 );        
        ResourceDictionary.addEntry( "/SplashGame/pandeiro_1.png", R.drawable.pandeiro_1 );        
        
        ResourceDictionary.addEntry( "/SplashGame/sombra.png", R.drawable.sombra );        
        ResourceDictionary.addEntry( "/SplashGame/sombra_0.png", R.drawable.sombra_0 );        
        ResourceDictionary.addEntry( "/SplashGame/sombra_1.png", R.drawable.sombra_1 );                

        
        ResourceDictionary.addEntry( "/SplashGame/pandeiro.bin", R.raw.pandeiro );        
        ResourceDictionary.addEntry( "/SplashGame/sombra.bin", R.raw.sombra );        
        

        ResourceDictionary.addEntry( "/0.mid", R.raw.m0 );        
        ResourceDictionary.addEntry( "/1.mid", R.raw.m1 );        
        ResourceDictionary.addEntry( "/2.mid", R.raw.m2 );        
        ResourceDictionary.addEntry( "/3.mid", R.raw.m3 );        
        ResourceDictionary.addEntry( "/4.mid", R.raw.m4 );        
        ResourceDictionary.addEntry( "/5.mid", R.raw.m5 );        
        ResourceDictionary.addEntry( "/6.mid", R.raw.m6 );        
        ResourceDictionary.addEntry( "/7.mid", R.raw.m7 );        
        ResourceDictionary.addEntry( "/8.mid", R.raw.m8 );        
    }
}
//#endif