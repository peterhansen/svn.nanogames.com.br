/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.MUV;
import br.com.nanogames.components.util.NanoMath;
import core.Constants;

/**
 *
 * @author Edson
 */
public class PushItRoulette extends UpdatableGroup implements Constants {

	// protected DrawableGroup pushRoulette;
	/** Caixas que informam o que o usuário deve apertar */
	public final DrawableImage pushRoulette2,  pushRoulette4,  pushRoulette6,  pushRoulette8;
	public final MUV speedY;
	private final int TOP;


	public PushItRoulette() throws Exception {
		super(4);
		
		final PushItBar pushItbar = PushItBar.getInstance();
		insertDrawable(pushRoulette2 = new DrawableImage(pushItbar.pushIt2));
		insertDrawable(pushRoulette4 = new DrawableImage(pushItbar.pushIt4));
		insertDrawable(pushRoulette6 = new DrawableImage(pushItbar.pushIt6));
		insertDrawable(pushRoulette8 = new DrawableImage(pushItbar.pushIt8));

		pushRoulette4.setSize(pushRoulette2.getSize());
		pushRoulette6.setSize(pushRoulette2.getSize());
		pushRoulette8.setSize(pushRoulette2.getSize());

		pushRoulette2.setPosition(0, 0);
		pushRoulette4.setPosition(0, pushRoulette2.getPosY() + pushRoulette2.getHeight());
		pushRoulette6.setPosition(0, pushRoulette4.getPosY() + pushRoulette4.getHeight());
		pushRoulette8.setPosition(0, pushRoulette6.getPosY() + pushRoulette6.getHeight());

		pushRoulette2.setVisible(true);
		pushRoulette4.setVisible(true);
		pushRoulette6.setVisible(true);
		pushRoulette8.setVisible(true);
		setSize(pushRoulette2.getWidth(), pushRoulette2.getHeight());
		TOP = getPosY() - (3 * getHeight());
		speedY = new MUV( -pushRoulette2.getHeight() * ( 50 + NanoMath.randInt(150) ) / 100 );

		update(NanoMath.randInt(1600));
	}

	
	public void update(int delta) {
		super.update(delta);

		final int diffY = speedY.updateInt(delta);
		pushRoulette2.move(0, diffY);
		pushRoulette6.move(0, diffY);
		pushRoulette4.move(0, diffY);
		pushRoulette8.move(0, diffY);


		if (pushRoulette2.getPosY() < TOP) {
			pushRoulette2.setPosition(0, pushRoulette8.getPosY() + getHeight());
		}
		if (pushRoulette4.getPosY() < TOP) {
			pushRoulette4.setPosition(0, pushRoulette2.getPosY() + getHeight());
		}
		if (pushRoulette6.getPosY() < TOP) {
			pushRoulette6.setPosition(0, pushRoulette4.getPosY() + getHeight());
		}
		if (pushRoulette8.getPosY() < TOP) {
			pushRoulette8.setPosition(0, pushRoulette6.getPosY() + getHeight());
		}
	}

	
	private final byte getIndexImage() {
		if (pushRoulette2.getPosY() < getPosY() + getHeight() / 2 &&
				pushRoulette2.getPosY() + pushRoulette2.getHeight() > getPosY() + getHeight() / 2) {
			return ROULETTE_2;
		}
		if (pushRoulette4.getPosY() < getPosY() + getHeight() / 2 &&
				pushRoulette4.getPosY() + pushRoulette4.getHeight() > getPosY() + getHeight() / 2) {
			return ROULETTE_4;
		}

		if (pushRoulette6.getPosY() < getPosY() + getHeight() / 2 &&
				pushRoulette6.getPosY() + pushRoulette6.getHeight() > getPosY() + getHeight() / 2) {
			return ROULETTE_6;
		}
		if (pushRoulette8.getPosY() < getPosY() + getHeight() / 2 &&
				pushRoulette8.getPosY() + pushRoulette8.getHeight() > getPosY() + getHeight() / 2) {
			return ROULETTE_8;
		}

		return -1;
	}


	public boolean hit(int keyPressed) {
		switch (keyPressed) {
			case ScreenManager.KEY_NUM2:
			case ScreenManager.UP:
				return ( getIndexImage() == ROULETTE_2 );
			
			case ScreenManager.KEY_NUM4:
			case ScreenManager.LEFT:
				return ( getIndexImage() == ROULETTE_4 );
			
			case ScreenManager.KEY_NUM6:
			case ScreenManager.RIGHT:
				return ( getIndexImage() == ROULETTE_6 );

			case ScreenManager.KEY_NUM8:
			case ScreenManager.DOWN:
				return ( getIndexImage() == ROULETTE_8 );

			default:
				return false;
//			case ScreenManager.KEY_NUM0:
//				PushItBar.setIndexRoulette((short) index);
//				PushIt.setIndexRoulette((short) index);
//
		}
	}
}
