/**
 * PushIt.java
 * ©2008 Nano Games.
 *
 * Created on 05/06/2008 17:41:27.
 */

package game;

import br.com.nanogames.components.DrawableImage;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Sprite;
import br.com.nanogames.components.UpdatableGroup;
import br.com.nanogames.components.userInterface.KeyListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.NanoMath;
import br.com.nanogames.components.util.Rectangle;
import core.Constants;
import core.GameMIDlet;

/**
 * @author Daniel L. Alves
 */
public  class PushIt extends UpdatableGroup implements Constants 
{
    /*indice da imagem que ocupa o maior espaço na roleta*/
    private static short  index;

	//<editor-fold defaultstate="collapsed" desc="Códigos que indicam o que o jogador deve apertar">

	public static final byte  PUSHIT_NOT_SET				= -2;
	
	public static final byte  PUSHIT_HALF_NONE				= -1;
	public static final byte  PUSHIT_NONE					= 0;
	public static final byte  PUSHIT_ONE_N_HALF_NONE		= 1;
	public static final byte  PUSHIT_DOUBLE_NONE			= 2;
	public static final byte  PUSHIT_DOUBLE_N_HALF_NONE		= 3;
	public static final byte  PUSHIT_TRIPLE_NONE			= 4;
	
	public static final byte  PUSHIT_2						= 5;
	public static final byte  PUSHIT_ONE_N_HALF_2			= 6;
	public static final byte  PUSHIT_DOUBLE_2				= 7;
	public static final byte  PUSHIT_DOUBLE_N_HALF_2		= 8;
	public static final byte  PUSHIT_TRIPLE_2				= 9;
	
	public static final byte  PUSHIT_4						= 10;
	public static final byte  PUSHIT_ONE_N_HALF_4			= 11;
	public static final byte  PUSHIT_DOUBLE_4				= 12;
	public static final byte  PUSHIT_DOUBLE_N_HALF_4		= 13;
	public static final byte  PUSHIT_TRIPLE_4				= 14;
	
	public static final byte  PUSHIT_6						= 15;
	public static final byte  PUSHIT_ONE_N_HALF_6			= 16;
	public static final byte  PUSHIT_DOUBLE_6				= 17;
	public static final byte  PUSHIT_DOUBLE_N_HALF_6		= 18;
	public static final byte  PUSHIT_TRIPLE_6				= 19;
	
	public static final byte  PUSHIT_8						= 20;
	public static final byte  PUSHIT_ONE_N_HALF_8			= 21;
	public static final byte  PUSHIT_DOUBLE_8				= 22;
	public static final byte  PUSHIT_DOUBLE_N_HALF_8		= 23;
	public static final byte  PUSHIT_TRIPLE_8				= 24;

	public static final byte  PUSHIT_QUESTION_MARK			= 25;

	public static final byte  PUSHIT_STAR					= 26;

    public static final byte PUSHIT_ROULETTE                = 27;

    public static final byte PUSHIT_SPECIAL                 = 28;

	
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="Índices das imagens da coleção">
	
	private final byte PUSHIT_HIGHLIGHTED_TRAIL		= 0;
	protected final byte PUSHIT_HIGHLIGHTED_BOX		= 1;
	private final byte PUSHIT_HIGHLIGHTED_BOX_QM	= 2;
	
	//</editor-fold>
	
	/** Chance que o jogador possui de acertar o código escondido pela interrogação */
	private static final byte PUSHIT_QUESTION_MARK_EXTRA_CHANCE_TO_HIT = 33;

	/** Código atual deste pushit */
    protected   byte currCode;

	private final byte pushItAjuste = 1;
	
	/** Código escondido pelo ponto de interrogação */
	protected  byte questionMarkRealCode;
	
	/** Indica se o pushIt já sofreu uma tentativa de acerto*/
	protected boolean used;
	
	
	/** Código atual deste pushit */
	public PushIt() throws Exception
	{
		super( 10 );
		currCode = PUSHIT_NOT_SET;
		
		questionMarkRealCode = PUSHIT_NOT_SET;

		// insere drawables dummies para iniciar o array de drawables
		insertDrawable( new Pattern( 0x000000 ) );
		insertDrawable( new Pattern( 0x000000 ) );
		insertDrawable( new Pattern( 0x000000 ) );
	}


	/** Retorna o código atual do pushIt */
	public byte getCode()
	{
		return currCode;
	}

	
	/** Verifica se o pushIt já sofreu uma tentativa de acerto */
	public boolean getUsed()
	{
		return used;
	}


	/** Indica que o pushIt já foi utilizado */
	public void setUsed()
	{
		used = true;
	}


     public short getIdex() {
        return index;
      }

	public static void setIndexRoulette(short index){
      PushIt.index = index;
    }


	/** Indica que o pushIt sofreu uma tentativa de acerto */
	public boolean hit( int keyPressed )
	{
		setUsed();
        final PushItBar pushItbar = PushItBar.getInstance();
		final boolean isQuestionMark = currCode == PUSHIT_QUESTION_MARK;
		final int codeToCheck = isQuestionMark ? questionMarkRealCode : currCode;
		
		boolean ret = false;
		switch (currCode) {
			case PUSHIT_ROULETTE:
				ret = ((PushItRoulette) drawables[PUSHIT_HIGHLIGHTED_BOX]).hit(keyPressed);
				break;

			case PUSHIT_SPECIAL:

				ret = currCode == PUSHIT_STAR;
				if (index == INDEX_STAR) {
					ret = currCode == PUSHIT_SPECIAL;
					pushItbar.setStarBonus();
				}
				if (index == INDEX_BOMB) {
					ret = currCode == PUSHIT_SPECIAL;
				}
				if (index == INDEX_BONUS) {
					ret = currCode == PUSHIT_SPECIAL;
				}
				break;
			case PUSHIT_STAR:
				ret = true;
				pushItbar.setStarBonus();
				break;

			default:
				switch (keyPressed) {
					default:
					case ScreenManager.KEY_NUM2:
					case ScreenManager.KEY_NUM4:
					case ScreenManager.KEY_NUM6:
					case ScreenManager.KEY_NUM8:
						final int motherCode = (((keyPressed - ScreenManager.KEY_NUM2) >> 1) + 1) * 5;

						ret = prevMulOf5(codeToCheck) == motherCode;

						// Rouba a favor do jogador
						if (isQuestionMark && !ret) {
							ret = NanoMath.randInt( 75 ) < PUSHIT_QUESTION_MARK_EXTRA_CHANCE_TO_HIT;
						}

						break;
				}

		}

		// Se acertou, não aparece o cinza depois da mira
		if( ret )
		{
			if( !hasTrail() )
				setVisible( false );
		}
		else if( isQuestionMark )
		{
			drawables[ PUSHIT_HIGHLIGHTED_BOX_QM ].setVisible( true );
		}
		return ret;
	}


	/** Obtém o primeiro múltiplo de 5 anterior a n */
	private static int prevMulOf5( int n )
	{
		final int mod = n % 5;
		if( n == 0 )
			return n;
		return n - mod;
	}

	
	/** Indica se o psuhIt */
	public boolean hasTrail()
	{
		return currCode >= PUSHIT_2 && currCode <= PUSHIT_TRIPLE_8 && (( currCode % 5 ) != 0 );
	}

	
	/** Determina como deve ser a aparência deste pushit */
	public void setPushIt( int code ) throws Exception {
		used = false;
		currCode = ( byte )code;
		
		questionMarkRealCode = PUSHIT_NOT_SET;

		final PushItBar pushItbar = PushItBar.getInstance();
	    int pushItWidth = pushItbar.pushIt2.getWidth();
	    int pushItHalfWidth = pushItWidth >> 1;
		
		int extraWidth = 0;
		
		short percent = 0;

		// Aloca qualquer coisa só para driblar os componentes
		drawables[ PUSHIT_HIGHLIGHTED_TRAIL ] = new DrawableImage( pushItbar.trail2 );
		drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setVisible( false );

        for ( short i = 0; i < activeUpdatables; ++i )
			updatables[ i ] = null;
        activeUpdatables = 0;

		setVisible( true );
		
		switch( currCode )
		{
			case PUSHIT_TRIPLE_2:
			case PUSHIT_DOUBLE_N_HALF_2:
			case PUSHIT_DOUBLE_2:
			case PUSHIT_ONE_N_HALF_2:
				drawables[PUSHIT_HIGHLIGHTED_TRAIL] = new DrawableImage(pushItbar.trail2);
				//#if   SCREEN_SIZE == "BIG"
//				drawables[PUSHIT_HIGHLIGHTED_TRAIL].setPosition(drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosX(), drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosY() + pushItAjuste);
			//#endif
			case PUSHIT_2:
                pushItHalfWidth = pushItbar.pushIt2.getWidth() >> 1;
				percent = ( short ) ( ( currCode - PUSHIT_2 ) * 25 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt2 );
				break;
	
			case PUSHIT_TRIPLE_4:
			case PUSHIT_DOUBLE_N_HALF_4:
			case PUSHIT_DOUBLE_4:
			case PUSHIT_ONE_N_HALF_4:
				drawables[PUSHIT_HIGHLIGHTED_TRAIL] = new DrawableImage(pushItbar.trail4);
				//#if   SCREEN_SIZE == "BIG"
//				drawables[PUSHIT_HIGHLIGHTED_TRAIL].setPosition(drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosX(), drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosY() + pushItAjuste);
			//#endif
			case PUSHIT_4:
                 pushItHalfWidth = pushItbar.pushIt4.getWidth() >> 1;
				percent = ( short ) ( ( currCode - PUSHIT_4 ) * 25 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt4 );
				break;

			case PUSHIT_TRIPLE_6:
			case PUSHIT_DOUBLE_N_HALF_6:
			case PUSHIT_DOUBLE_6:
			case PUSHIT_ONE_N_HALF_6:
				drawables[PUSHIT_HIGHLIGHTED_TRAIL] = new DrawableImage(pushItbar.trail6);
				//#if   SCREEN_SIZE == "BIG"
//				drawables[PUSHIT_HIGHLIGHTED_TRAIL].setPosition(drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosX(), drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosY() + pushItAjuste);
			//#endif
			case PUSHIT_6:
                pushItHalfWidth = pushItbar.pushIt6.getWidth() >> 1;
				percent = ( short ) ( ( currCode - PUSHIT_6 ) * 25 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt6 );
				break;

			case PUSHIT_TRIPLE_8:
			case PUSHIT_DOUBLE_N_HALF_8:
			case PUSHIT_DOUBLE_8:
			case PUSHIT_ONE_N_HALF_8:
				drawables[PUSHIT_HIGHLIGHTED_TRAIL] = new DrawableImage(pushItbar.trail8);
				//#if   SCREEN_SIZE == "BIG"
//				drawables[PUSHIT_HIGHLIGHTED_TRAIL].setPosition(drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosX(), drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosY() + pushItAjuste);
			//#endif
			case PUSHIT_8:
                pushItHalfWidth = pushItbar.pushIt8.getWidth() >> 1;
				percent = ( short ) ( ( currCode - PUSHIT_8 ) * 25 );
				drawables[ PUSHIT_HIGHLIGHTED_BOX ] = new DrawableImage( pushItbar.pushIt8 );
				break;

			case PUSHIT_QUESTION_MARK:
				// Sorteia um desses valores: PUSHIT_2, PUSHIT_4, PUSHIT_6 e PUSHIT_8
				questionMarkRealCode = ( byte )( ( 1 + NanoMath.randInt( 4 ) ) * 5 );

				removeDrawable(PUSHIT_HIGHLIGHTED_BOX);
                insertDrawable( new PushItQuestion(), PUSHIT_HIGHLIGHTED_BOX );
				break;
				
			case PUSHIT_STAR:
				drawables[PUSHIT_HIGHLIGHTED_BOX] = new DrawableImage(pushItbar.pushItStar);
				//#if   SCREEN_SIZE == "BIG"
//				drawables[PUSHIT_HIGHLIGHTED_TRAIL].setPosition(drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosX(), drawables[PUSHIT_HIGHLIGHTED_TRAIL].getPosY() + pushItAjuste);
				//#endif
				break;
			
			case PUSHIT_HALF_NONE:
			case PUSHIT_TRIPLE_NONE:
			case PUSHIT_DOUBLE_N_HALF_NONE:
			case PUSHIT_DOUBLE_NONE:
			case PUSHIT_ONE_N_HALF_NONE:
			case PUSHIT_NONE:
				extraWidth = ( currCode - PUSHIT_NONE ) * pushItHalfWidth;
				setVisible( false );
				break;

            case PUSHIT_ROULETTE:
                removeDrawable(PUSHIT_HIGHLIGHTED_BOX);
                insertDrawable( new PushItRoulette(), PUSHIT_HIGHLIGHTED_BOX );
           break;

            case PUSHIT_SPECIAL:
				removeDrawable(PUSHIT_HIGHLIGHTED_BOX);
				insertDrawable( new PushItSpecial(), PUSHIT_HIGHLIGHTED_BOX );
                break;
		}

		// Como estamos trocando várias imagens, deixamos nossa consciência limpa chamando gc()
		System.gc();
		
		if ( extraWidth == 0 )
			extraWidth = ( percent * drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].getWidth() / 100 );
		
		// Se possui rastro...
		final int pushItHeight = pushItbar.pushIt2.getHeight();
        final int trailWidthDiff = 2;
		if( isVisible() )
		{
			if( extraWidth > 0 )
			{
				final int trailWidth = extraWidth;// + PushItBar.PUSHIT_TRAIL_OFFSET;
				
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setVisible( true );
				drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].setSize( trailWidth, pushItHeight );
				
			}
			
			if ( percent > 0 ) {
				drawables[ PUSHIT_HIGHLIGHTED_BOX ].setPosition( extraWidth - pushItHalfWidth, 0 );
			} else {
				drawables[ PUSHIT_HIGHLIGHTED_BOX ].setPosition( extraWidth, 0 );
			}
			
		}
		
		// Determina o novo tamanho da coleção
		setSize( pushItWidth + extraWidth, pushItHeight );
	}
	
	
	/** Retorna o código do pushIt sem rastro */
	public static int untrailCode( int code )
	{
		switch( code )
		{
			case PUSHIT_HALF_NONE:
				return PUSHIT_NONE;
				
			case PUSHIT_STAR:
				return PUSHIT_STAR;
				
			default:
				return prevMulOf5( code );
		}
	}
	
	
	/** Obtém a área do coleção que pode sofrer colisão */
	public Rectangle getCollisionArea()
	{
		return new Rectangle( position.x + drawables[ PUSHIT_HIGHLIGHTED_BOX ].getPosX(), position.y + drawables[ PUSHIT_HIGHLIGHTED_BOX ].getPosY(), drawables[ PUSHIT_HIGHLIGHTED_BOX ].getWidth(), drawables[ PUSHIT_HIGHLIGHTED_BOX ].getHeight() );
	}

	
	/** Reduz o tamanho do rastro do pushIt. Retorna se ainda há ratro para ser reduzido */
	public boolean reduceTrail( int dx, int aimCollisionAreaX )
	{
		drawables[PUSHIT_HIGHLIGHTED_TRAIL].setSize( drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].getWidth() - dx, drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].getHeight() );
		drawables[PUSHIT_HIGHLIGHTED_TRAIL].move( dx, 0 );
		
		final boolean ret = ( position.x + drawables[ PUSHIT_HIGHLIGHTED_TRAIL ].getPosX() < aimCollisionAreaX );
		setVisible( ret );
		return ret;
	}

}
