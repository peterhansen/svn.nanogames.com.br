//#if JAVA_VERSION == "ANDROID"
package javax.microedition.rms;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import br.com.nanogames.MIDP.MainActivity;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author danielmonteiro
 */
public class RecordStore {
    
   private static HashMap< String, RecordStore > dbs = new HashMap< String, RecordStore >();

    private String databaseName = null;
    private int numRecords = 0;
    /**
     * 
     * @return
     * @throws RecordStoreNotFoundException 
     */
    public static String[] listRecordStores() {
        String[] toReturn;
        
        SharedPreferences settings = MainActivity.getActivity().getSharedPreferences( "ROOT", 0); 
        Set set = settings.getAll().entrySet(); 
        toReturn = new String[ settings.getAll().size() ];
        // Get an iterator 
        Iterator i = set.iterator(); 
        int index = 0;
        // Display elements 
        while(i.hasNext()) { 
            Map.Entry me = (Map.Entry)i.next(); 
            toReturn[ index ] = (String) me.getKey();
            ++index;
        } 

        return toReturn;
    }
    
    public static RecordStore openRecordStore(String name, boolean createIfNecessary ) throws RecordStoreException,
                                          RecordStoreFullException,
                                          RecordStoreNotFoundException {
        
        SharedPreferences settings = MainActivity.getActivity().getSharedPreferences( "ROOT", 0); 
        SharedPreferences.Editor editor = settings.edit();
        if ( !settings.contains( name ) ) {
            editor.putString( name, "0" );
            editor.commit();            
        }
        
        
        RecordStore instance;
        
        if ( !dbs.containsKey( name ) ) {            
            instance = new RecordStore();
            instance.databaseName = name;
            instance.numRecords = Integer.parseInt( settings.getString( name , "0" ) );
            dbs.put( name, instance );            
        } else
            instance = dbs.get( name );
        
        return instance;
    }

    /**
     * 
     * @param dataBaseName 
     */
    public static void deleteRecordStore(String dataBaseName) {        
        SharedPreferences settings = MainActivity.getActivity().getSharedPreferences( "ROOT", 0); 
        SharedPreferences.Editor editor = settings.edit();
        editor.remove( dataBaseName );
        editor.commit();    
    }

    /**
     * 
     * @return 
     */
    public int getNumRecords() {   
        return numRecords;
    }

    /**
     * 
     */
    public void closeRecordStore() {
        //////////
    }

    /**
     * 
     * @param object
     * @param i
     * @param i0
     * @return 
     */
    public int addRecord( byte[] data, int offset, int numBytes ) {
        ++numRecords;
        SharedPreferences settings = MainActivity.getActivity().getSharedPreferences( "ROOT", 0); 
        SharedPreferences.Editor editor = settings.edit();
        editor.putString( databaseName, String.valueOf( numRecords ) );
        return numRecords;
    }

    

    /**
     * 
     * @param slot
     * @param data
     * @param i
     * @param length 
     */
    public void setRecord(int slot, byte[] data, int offset, int length) throws RecordStoreNotOpenException,
                      InvalidRecordIDException,
                      RecordStoreException,
                      RecordStoreFullException {        
        FileOutputStream fos = null;
        try {
            String filename = this.databaseName + String.valueOf( slot );
            fos = MainActivity.getActivity().openFileOutput( filename, Context.MODE_PRIVATE );
//            fos = new FileOutputStream( filename );
            DataOutputStream dos = new DataOutputStream( fos );
            dos.writeInt( data.length - offset );            
            dos.write( data, offset, data.length - offset );
            dos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);        
        } catch (IOException ex) {            
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    
    /**
     * 
     * @param slot
     * @return 
     */
    public byte[] getRecord(int slot) {
    byte[] toReturn = null;
    
        try {
            BufferedInputStream buf = null;    
            String filename = this.databaseName + String.valueOf( slot );
//            FileInputStream fis = new FileInputStream( filename );
            FileInputStream fis = MainActivity.getActivity().openFileInput( filename );
            buf = new BufferedInputStream( fis );
            DataInputStream dis = new DataInputStream( buf );
            int length = dis.readInt();
            toReturn = new byte[ length ];
            dis.readFully( toReturn );
            dis.close();
            buf.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {            
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return toReturn;
    }

    public void deleteRecord(int slot) {
        try {
            setRecord( slot, new byte[] {}, 0, 0 );
        } catch (RecordStoreNotOpenException ex) {
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidRecordIDException ex) {
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RecordStoreException ex) {
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RecordStoreFullException ex) {
            Logger.getLogger(RecordStore.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
//#endif