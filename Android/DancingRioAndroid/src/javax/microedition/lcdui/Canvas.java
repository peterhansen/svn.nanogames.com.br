//#if JAVA_VERSION == "ANDROID"
package javax.microedition.lcdui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import br.com.nanogames.MIDP.MainActivity;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.ScreenManager;
import br.com.nanogames.components.util.Mutex;

/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class Canvas extends SurfaceView implements SurfaceHolder.Callback {
    private boolean sensible = true;
    public static final int LEFT = 2;
    public static final int UP = 1;
    public static final int DOWN = 6;
    public static final int RIGHT = 5;
    public static final int FIRE = 8;
    public static final int KEY_NUM0 = 48;
    public static final int KEY_NUM1 = 49;
    public static final int KEY_NUM2 = 50;
    public static final int KEY_NUM3 = 51;
    public static final int KEY_NUM4 = 52;
    public static final int KEY_NUM5 = 53;
    public static final int KEY_NUM6 = 54;
    public static final int KEY_NUM7 = 55;
    public static final int KEY_NUM8 = 56;
    public static final int KEY_NUM9 = 57;
    public static final int KEY_STAR = 42;
    public static final int KEY_POUND = 35;
    /**
     * Controla se o shift esta ativado ou não...
     */
    public boolean shifted = false;
    public static Runnable runnable;
    private boolean enabled = true;

    /**
     * 
     */
    public Canvas() {
        super(MainActivity.getActivity());
        createSurfaceView();
    }
    
    public void createSurfaceView() {    
        
        getHolder().setSizeFromLayout();
        getHolder().addCallback(this);
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_NORMAL);

        setFocusable(true);
        setClickable(true);
        setLongClickable(true);
        requestFocus();
        setFocusableInTouchMode(true);
        setVisibility(View.VISIBLE);
    }

    /**
     * 
     * @param keyCode
     * @return 
     */
    public String getKeyName(int keyCode) {
//        throw new UnsupportedOperationException("Not yet implemented");
        return "";
    }

    /**
     * 
     */
    public void serviceRepaints() {

//            Log.d("CanvasSurfaceView", "SurfaceView::serviceRepaints");

            android.graphics.Canvas c = null;
            try {

                c = getHolder().lockCanvas(null);
                if (c != null) {

                    synchronized ( this ) {
                        onCustomDraw(c);
                    }
                }

            } catch (Exception e) {
//                Log.d("CanvasSurfaceView", "e::SurfaceView::serviceRepaints");
            } finally {
                if (c != null) {
                    getHolder().unlockCanvasAndPost(c);
                }
            }


    }

    /**
     * 
     */
    public void repaint() {

//            Log.d("CanvasSurfaceView", "SurfaceView::repaint");
            android.graphics.Canvas c = null;
            try {

                c = getHolder().lockCanvas(null);
                if (c != null) {

                    synchronized ( this ) {
                        onCustomDraw(c);
                    }
                }

            } catch (Exception e) {
//                Log.d("CanvasSurfaceView", "e::SurfaceView::repaint");
            } finally {
                if (c != null) {
                    getHolder().unlockCanvasAndPost(c);
                }
            }

    }

    /**
     * 
     * @param b 
     */
    public void setFullScreenMode(boolean b) {
        synchronized ( this ) {
            DisplayMetrics metrics = new DisplayMetrics();
            MainActivity.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

            enabled = false;
            sizeChanged( metrics.widthPixels, metrics.heightPixels );
            enabled = true;            
        }

    }

    /**
     * 
     * @param keyCode
     * @return 
     */
    public int getGameAction(int keyCode) {
//        throw new UnsupportedOperationException("Not yet implemented");
        return 0;
    }

    /**
     * 
     * @param w
     * @param h
     * @param oldw
     * @param oldh 
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        
        synchronized ( this ) {
            enabled = false;
            sizeChanged(w, h);
            enabled = true;            
        }
    }

    /**
     * 
     * @param canvas 
     */
    protected void onCustomDraw(android.graphics.Canvas canvas) {
//        Log.d("CanvasSurfaceView", "SurfaceView::onDraw");
        Graphics g;

        if (canvas == null) {
            return;
        }

//        super.onDraw(canvas);

        g = Graphics.getInstance(canvas);

        if (g == null) {
            return;
        }

//        synchronized (this)
        {
            if (runnable != null) {
                runnable.run();
                runnable = null;
                AppMIDlet.gc();
            }
        }

        if (enabled) {
            paint(g);
        }
    }

    /**
     * 
     * @param event
     * @return 
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        Log.d( "NanoComponents", "pointer event" );
        
        if ( event.getDownTime() < 50 )
            return true;        
                
        synchronized( this ) {

            int x = (int) event.getX();
            int y = (int) event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pointerPressed(x, y);
                    return true;
                case MotionEvent.ACTION_MOVE:
                    pointerDragged(x, y);
                    return true;
                case MotionEvent.ACTION_UP:
                    pointerReleased(x, y);
                    return true;
                default:
                    return super.onTouchEvent(event);        
                }               
        }    
    }

    /**
     * 
     * @param keyCode
     * @param event
     * @return 
     */
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        int newkeycode = -1;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
                newkeycode = Canvas.UP;
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                newkeycode = Canvas.DOWN;
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                newkeycode = Canvas.LEFT;
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                newkeycode = Canvas.RIGHT;
                break;
            case KeyEvent.KEYCODE_MENU:
                newkeycode = ScreenManager.KEY_SOFT_LEFT;
                break;
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                newkeycode = Canvas.KEY_NUM0 + (KeyEvent.KEYCODE_9 - KeyEvent.KEYCODE_0);
                break;

            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                newkeycode = Canvas.FIRE;
                break;
            case KeyEvent.KEYCODE_STAR:
                newkeycode = Canvas.KEY_STAR;
                break;
            case KeyEvent.KEYCODE_POUND:
                newkeycode = Canvas.KEY_POUND;
                break;
            case KeyEvent.KEYCODE_CLEAR:
                newkeycode = ScreenManager.KEY_CLEAR;
                break;

            case KeyEvent.KEYCODE_BACK:
                newkeycode = ScreenManager.KEY_BACK;
        }

        if (shifted && ((keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) || (keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT))) {
            shifted = false;
        }

        keyReleased(newkeycode);

        return super.onKeyUp(keyCode, event);
    }

    /**
     * 
     * @param keyCode
     * @param event
     * @return 
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int newkeycode = -1;

        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
                newkeycode = Canvas.UP;
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                newkeycode = Canvas.DOWN;
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                newkeycode = Canvas.LEFT;
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                newkeycode = Canvas.RIGHT;
                break;
            case KeyEvent.KEYCODE_MENU:
                newkeycode = ScreenManager.KEY_SOFT_LEFT;
                break;

            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_1:
            case KeyEvent.KEYCODE_5:
            case KeyEvent.KEYCODE_2:
            case KeyEvent.KEYCODE_3:
            case KeyEvent.KEYCODE_4:
            case KeyEvent.KEYCODE_6:
            case KeyEvent.KEYCODE_7:
            case KeyEvent.KEYCODE_8:
            case KeyEvent.KEYCODE_9:
                newkeycode = Canvas.KEY_NUM0 + (KeyEvent.KEYCODE_9 - KeyEvent.KEYCODE_0);
                break;

            case KeyEvent.KEYCODE_DPAD_CENTER:
            case KeyEvent.KEYCODE_ENTER:
                newkeycode = Canvas.FIRE;
                break;
            case KeyEvent.KEYCODE_STAR:
                newkeycode = Canvas.KEY_STAR;
                break;
            case KeyEvent.KEYCODE_POUND:
                newkeycode = Canvas.KEY_POUND;
                break;
            case KeyEvent.KEYCODE_CLEAR:
                newkeycode = ScreenManager.KEY_CLEAR;
                break;

            case KeyEvent.KEYCODE_BACK:
                newkeycode = ScreenManager.KEY_BACK;
                break;
            case KeyEvent.KEYCODE_SPACE:
                newkeycode = ' ';
        }

        if (!shifted) {
            shifted = (keyCode == KeyEvent.KEYCODE_SHIFT_LEFT) || (keyCode == KeyEvent.KEYCODE_SHIFT_RIGHT);
        }

        if (keyCode >= KeyEvent.KEYCODE_A && keyCode <= KeyEvent.KEYCODE_Z) {
            newkeycode = shifted ? 'A' : 'a';
            newkeycode += (keyCode - KeyEvent.KEYCODE_A);
        }

        keyPressed(newkeycode);

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * 
     * @param keyCode 
     */
    protected void keyReleased(int keyCode) {
    }

    /**
     * 
     * @param keyCode 
     */
    protected void keyPressed(int keyCode) {
    }

    /**
     * 
     * @param x
     * @param y 
     */
    protected void pointerDragged(int x, int y) {
    }

    /**
     * 
     * @param x
     * @param y 
     */
    protected void pointerPressed(int x, int y) {
    }

    /**
     * 
     * @param x
     * @param y 
     */
    protected void pointerReleased(int x, int y) {
    }

    /**
     * 
     * @param w
     * @param h 
     */
    public void sizeChanged(int w, int h) {
//        Log.d("CanvasSurfaceViewThread", "sizeChanged da base chamada!");
    }

    /**
     * 
     * @param g 
     */
    public void paint(Graphics g) {
    }

    public void surfaceCreated(SurfaceHolder sh) {
//        Log.d("CanvasSurfaceViewThread", "surfaceCreated!");
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
//        Log.d("CanvasSurfaceViewThread", "surfaceChanged!");

        setFocusable(true);
        setClickable(true);
        setLongClickable(true);
        requestFocus();
        setFocusableInTouchMode(true);


        setVisibility(View.VISIBLE);
        enabled = false;
        sizeChanged(w, h);
        enabled = true;
    }

    public void surfaceDestroyed(SurfaceHolder sh) {
//        Log.d("CanvasSurfaceViewThread", "surfaceDestroyed!");
    }

    /** Inform the view that the activity is paused.*/
    public void onPause() {
    }

    /** Inform the view that the activity is resumed. */
    public void onResume() {
    }

    /** Inform the view that the window focus has changed. */
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    protected void stopDrawing() {
    }
}
//#endif