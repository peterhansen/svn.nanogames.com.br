//#if JAVA_VERSION == "ANDROID"
package javax.microedition.lcdui.game;

import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.Image;

/**
 *
 * @author danielmonteiro
 */
public class Sprite {
    public static final int TRANS_NONE = 0;
    public static final int TRANS_MIRROR  = 2;
    public static final int TRANS_MIRROR_ROT180  = 1;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT90 = 5;
    public static final int TRANS_ROT270 = 6;
    public static final int TRANS_MIRROR_ROT90 = 7;
    public static final int TRANS_MIRROR_ROT270  = 4;

    public Sprite(Image image) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setTransform(int transformMIDP) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void setPosition(int x, int y) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void paint(Graphics g) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void defineReferencePixel(int refPixelX, int refPixelY) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
}
//#endif