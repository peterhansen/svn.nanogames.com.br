//#if JAVA_VERSION == "ANDROID"
package javax.microedition.media;

import android.media.MediaPlayer;
import br.com.nanogames.MIDP.MainActivity;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.ResourceDictionary;

/**
 *
 * @author danielmonteiro
 */
public class Manager {
    
    private static String[] supportedFormats = {
        	"audio/x-wav",
            "audio/basic",
            "audio/mpeg",
            "audio/midi",
            "audio/amr",
            "audio/x-tone-seq"
        };

    public static HashMap idToPlayer;
    
    static {
        idToPlayer = new HashMap();
    }
    
    public static String[] getSupportedContentTypes(Object object) {
        return supportedFormats;
    }

    public static void playTone(int note, int duration, int volume) {
    }


    public static void Destroy() {        

        Set set = idToPlayer.entrySet();
        
        // Get an iterator 
        Iterator i = set.iterator(); 
        int index = 0;
        // Display elements 
        while(i.hasNext()) { 
            Map.Entry me = ( Map.Entry )i.next(); 
            ( ( Player ) me.getValue() ).deallocate();
        } 
        
        idToPlayer.clear();        
    }
    
    

    public static Player createPlayer(InputStream input, String type) {
        Player player = null;        
        int id = ResourceDictionary.getIdForRes( input );
        
        try {
            
            if ( idToPlayer.containsKey( id ) ) {
                
                player = (Player) idToPlayer.get( id );
                
            } else {
                
                player = new Player();
                MediaPlayer mp;
                mp = MediaPlayer.create(MainActivity.getActivity(), id);
                player.setAndroidMediaPlayer(mp);
                ResourceDictionary.removeResToIdEntry( input );            
                input.close();
                idToPlayer.put( id, player );
                player.id = id;
            
            }            
            
        } catch (IOException ex) {
            Logger.getLogger(Manager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return player;
    }
}
//#endif