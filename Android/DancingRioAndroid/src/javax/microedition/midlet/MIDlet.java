//#if JAVA_VERSION == "ANDROID"
package javax.microedition.midlet;

import android.util.Log;
import br.com.nanogames.MIDP.MainActivity;
import javax.microedition.lcdui.Display;


/**
 *
 * @author Daniel "Monty" Monteiro
 */
public class MIDlet {
    public static String getAppProperty( String original ) {
        String key = original.replaceAll("-", "__slash__");
        MainActivity activity = MainActivity.getActivity();
        int resId = activity.getResources().getIdentifier( key, "string", activity.getPackageName());
//        Log.d("MyApp", "resource id is " + resId + " and value is " + activity.getString(resId));
        return activity.getString(resId);        
    }
    
    public boolean platformRequest(String string) {
        return true;
    }    
    
    public void notifyDestroyed() {
        MainActivity.getActivity().finish();
        
    }

    public void start() {
        Display.refreshCurrent();        
    }

    public void pauseApp() {
        
    }
            
}
//#endif