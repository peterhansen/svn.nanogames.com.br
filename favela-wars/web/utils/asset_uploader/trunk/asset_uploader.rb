class AssetUploader < Sinatra::Base
  before do
    @conn = Fog::Storage.new provider: 'AWS',
      aws_access_key_id: 'AKIAILVD4BVW4NQMA5VA',
      aws_secret_access_key: '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'
  end

  post '/assets' do
    begin
      bucket_name = params['bucket']
      directory = @conn.directories.get(bucket_name) || create_directory(bucket_name)
      filename = params['directory'] ? "#{params['directory']}/#{params['asset'][:filename]}" : params['asset'][:filename]

      file = directory.files.create(
        key:    filename,
        body:   params['asset'][:tempfile],
        public: true
      )
    rescue Exception => e
      [500, Log.info(e.message)]
    end
  end

private

  def create_directory(bucket_name)
    @conn.directories.create(key: bucket_name, public: true)
  end
end
