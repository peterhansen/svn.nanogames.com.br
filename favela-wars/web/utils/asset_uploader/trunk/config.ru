require 'sinatra'
require 'logger'
require 'fog'
require File.join(File.dirname(__FILE__), 'asset_uploader')

Log = Logger.new(STDOUT)

run AssetUploader.new
