Feature: Consultando a lista de exemplos


  Scenario: Consultando a lista de exemplos
    Given que um usuário '1' tenha sido notificado com a mensagem '1' com texto 'Fuck it!'
    And que um usuário '1' tenha sido notificado com a mensagem '2' com texto 'Bloody!'
    When eu envio um GET para '/notifications?user_id=1&unread=true'
    Then eu devo receber o JSON:
    """
    {"notifications":
      [ {"message": {"body": "Fuck it!", "id": 1,"user_id": 1}},
        {"message": {"body": "Bloody!", "id": 2,"user_id": 1}} ],
     "total": 2
     }
    """
    And eu devo receber o status code '200'
