Feature: Atualizando mensagens
  Background:
    Given que um usuário '1' tenha sido notificado com a mensagem '1' com texto 'Fuck it!'


  Scenario: Atualizando dados de um exemplo pelo id
    When eu envio um PUT para '/notifications/1.json' com os dados:
    """
    {"message": {
      "id": "1",
      "unread":"false"
    }}
    """
    And eu devo receber o status code '200'