Feature: Deletando um exemplo
  Background:
    Given que um usuário '1' tenha sido notificado com a mensagem '1' com texto 'se vira!'

  Scenario: Deletando uma notificação pelo id
    When eu envio um DELETE para '/notifications/1.json'
    And eu devo receber o status code '200'
