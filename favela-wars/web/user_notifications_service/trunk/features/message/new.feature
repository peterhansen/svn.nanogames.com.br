Feature: Criando mensagens


  Scenario: Criando um mensagem
    When eu envio um POST para '/notifications.json' com os dados:
    """
    {"message": {
      "user_id":"1",
      "body":"Bloddy"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"message": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'