# encoding: utf-8

Given "que um usuário '$user_id' tenha sido notificado com a mensagem '$id' com texto '$body'" do |user_id, id, body|
  FactoryGirl.create(:message, id: id, user_id: user_id, body: body)
end


