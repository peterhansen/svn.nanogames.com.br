ENV["SINATRA_ENV"] = "test"

require_relative '../../lib/notification_service'
require 'rack/test'

DatabaseCleaner.strategy = :truncation

Dir.glob(File.join(File.dirname(__FILE__), '../../spec/factories.rb')).each {|f| require f }

Before do
  ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'users_notification_test')
  DatabaseCleaner.clean
end

module AppHelper
  def app
    NotificationService::App
  end
end

World(Rack::Test::Methods, AppHelper)
