class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages, force: true do |t|
      t.integer  :user_id
      t.string   :body
      t.boolean  :unread, :default => true
      t.timestamps
    end

    add_index :messages, :user_id
  end

  def self.down
    drop_table :messages
  end
end
