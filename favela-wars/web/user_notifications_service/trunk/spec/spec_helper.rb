ENV["SINATRA_ENV"] ||= "test"

#require 'capybara/rspec'
require_relative '../lib/notification_service'
require 'rack/test'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)
require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])


set :environment, :test

DatabaseCleaner.strategy = :truncation

Dir.glob(File.join(File.dirname(__FILE__), 'factories.rb')).each {|f| require f }

RSpec.configure do |config|
  config.before(:each) do
    ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'examples_test')
    DatabaseCleaner.clean
  end
end
