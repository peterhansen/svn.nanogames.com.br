# encoding: utf-8
require_relative '../spec_helper'

set :show_exceptions, false

describe "Consultando os exemplos" do
  include Rack::Test::Methods

  def app
    NotificationService::App.new
  end

  let(:message) { FactoryGirl.create(:message, name: 'Examplo1') }

=begin
  it "deve retornar o exemplo" do
    NotificationResource.should_receive(:get_all).and_return([example])

    get '/messages.json'
    ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'user_notification_test') # Gambi
    last_response.body.should == "{\"examples\":[{\"example\":{\"id\":1,\"name\":\"Examplo1\"}}],\"total\":1}"
  end
=end
end
