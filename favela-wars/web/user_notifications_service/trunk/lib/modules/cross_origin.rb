module NotificationService
  class CrossOrigin
    ORIGIN_HEADER = "Access-Control-Allow-Origin"

    def initialize(app)
      @app = app
    end

    def call(env)
      status, header, body = @app.call(env)
      # TODO dimas - headers "ORIGIN_HEADER" => '*'
      header[ORIGIN_HEADER] = '*' if header
      [status, header, body]
    end
  end
end
