# encoding: utf-8

class Message < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :user_id, :body]

  def as_json(options = {})
    options = {} unless options
    super({only: PUBLIC_FIELDS}.merge(options))
  end
end
