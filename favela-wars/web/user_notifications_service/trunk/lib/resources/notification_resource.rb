# encoding: utf-8
require_relative '../data/message'

class NotificationResource < BaseResource

  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    example = Message.find(id)
    example.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Message not found')
  end

  def self.get_all(conditions = {})
    where = {}

    conditions.keys.each do |key|
      if Message.column_names.include?(key)
        where[key] = conditions[key]
      end
    end

    additional_fields = extract_extra_fields(conditions)

    Message.where(where).order('created_at desc').select(Message::PUBLIC_FIELDS + additional_fields)
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    example = Message.create(attributes)
    raise ValidationException.new(example.errors.messages) unless example.valid?
    return example.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    example = Message.find_by_id(id)

    if example
      example.update_attributes!(attributes)
      return example.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Message not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(example.errors.messages)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id, conditions = {})
    where = {}

    conditions.keys.each do |key|
      if Message.column_names.include?(key)
        where[key] = conditions[key]
      end
    end

    additional_fields = extract_extra_fields(conditions)
    
    Message.where(where).delete_all

    {success: 'Message disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('Message not found')
  end

  def self.count
    {user_count: Message.count}.to_json
  end


  protected

  def self.extract_extra_fields(params)
    fields = []

    if params['include']
      params['include'].split(',').each do |field|
        fields << field if Character.column_names.include?(field)
      end
    end

    fields
  end

end
