require_relative 'boot'

module NotificationService
  class App < Sinatra::Base
    set :environment, :development
    use ConnectionManager
    use CrossOrigin
    use Router
  end
end
