# language: pt
# encoding: utf-8

@sprint03
Funcionalidade: Atualizando produtos
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Contexto:
    Dado que exite um produto com o nome 'AR-15' e com a tag 'arma_de_descruicao_em_massa'

  Cenário: Atualizando dados de um produto
    Quando eu envio um PUT para '/products/1234.json' com os dados:
    """
    {"product": {
      "name": "AR-15",
      "price": 0,
      "published": false,
      "category_id": 1,
      "tag_list": []
    }}
    """
    E eu envio um GET para '/products/1234?include=tag_list'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "category_id": 1,
      "properties": null,
      "published": false,
      "tag_list": []
    }}
    """
    E eu devo receber o status code '200'

#  Cenário: Atualizando dados de um produto
#    Quando eu envio um PUT para '/products/1234.json' com os dados:
#    """
#    {"product": {
#      "id": 1,
#      "tag_list": ["banana", "pera", "maça", "salada-mista"]
#    }}
#    """
#    Então eu devo receber o JSON:
#    """
#    {"product": {
#      "id": 1234
#    }}
#    """
#    E eu devo receber o status code '200'
#
#
#  Cenário: Tentando atualizar dados de um produto inexistente
#    Quando eu envio um PUT para '/products/9999.json' com os dados:
#    """
#    {"product": {
#      "name": "AK-47"
#    }}
#    """
#    Então eu devo receber o status code '404'
#
#  Cenário: Tentando atualizando dados de um produto com dados inválidos
#    Dado que exite um produto com o nome 'AK-47' e id '4567'
#    Quando eu envio um PUT para '/products/4567.json' com os dados:
#    """
#    {"product": {
#      "name": "AR-15"
#    }}
#    """
#    Então eu devo receber o JSON:
#    """
#    {"errors": {"name": ["has already been taken"]}}
#    """
#    E eu devo receber o status code '409'
