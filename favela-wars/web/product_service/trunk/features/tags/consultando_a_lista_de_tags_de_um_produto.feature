# language: pt
# encoding: utf-8

@sprint03
Funcionalidade: Consultando produtos com descrição
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Contexto:
    Dado que exite um produto com o nome 'AR-15' e com a tag 'arma_de_descruicao_em_massa'

  Cenário: Consultando um produto válido
    Quando eu envio um GET para '/products/1234?include=tag_list'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "category_id": null,
      "properties": null,
      "published": false,
      "tag_list": ["arma_de_descruicao_em_massa"]
    }}
    """
    E eu devo receber o status code '200'

  Cenário: Consultando um produto válido
    Quando eu envio um GET para '/products/1234?include=tag_list'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "category_id": null,
      "properties": null,
      "published": false,
      "tag_list": ["arma_de_descruicao_em_massa"]
    }}
    """
    E eu devo receber o status code '200'
