# language: pt

@sprint04
Funcionalidade: a lista de tags

  Cenário: Consultando a lista de tags
    Dado que exite a tag 'Ruby'
    E que exite a tag 'Python'
    Quando eu envio um GET para '/tags'
    Então eu devo receber o JSON:
    """
    { "tags": [
        {"tag": {
          "id": 1,
          "name": "Ruby",
          "slug": "ruby" }},
        {"tag": {
          "id": 2,
          "name": "Python",
          "slug": "python" }}
        ],
      "total": 2
    }
    """
    E eu devo receber o status code '200'
