ENV["SINATRA_ENV"] ||= "test"

require_relative '../../lib/product_service'
require 'rack/test'

Mail.defaults do
  delivery_method :test
end

DatabaseCleaner.strategy = :truncation

Dir.glob(File.join(File.dirname(__FILE__), '../../spec/factories.rb')).each {|f| require f }

Before do
  ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'products_test')
  DatabaseCleaner.clean
  Mail::TestMailer.deliveries.clear
end

module AppHelper
  def app
    ProductService::App
  end
end

World(Rack::Test::Methods, AppHelper)
