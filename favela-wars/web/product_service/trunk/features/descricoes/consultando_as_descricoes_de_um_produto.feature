# language: pt
# encoding: utf-8

@sprint03
Funcionalidade: Consultando produtos com descrição
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Contexto:
    Dado que exite um produto com o nome 'AR-15' e id '1234'

  Cenário: Consultando um produto válido
    Quando eu envio um GET para '/products/1234?include=descriptions'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "category_id": null,
      "properties": null,
      "published": false,
      "descriptions": [
      {"description": {
        "id": 1,
        "locale": "pt-BR",
        "title": "AR-15",
        "shorted": null,
        "complete": null
      }}]
    }}
    """
    E eu devo receber o status code '200'

  Cenário: Atualizando um produto
    Quando eu envio um PUT para '/products/1234' com os dados:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "properties": null,
      "published": false,
      "descriptions_attributes": [{
        "id": 1,
        "locale": "pt-BR",
        "title": "Outro",
        "shorted": "Descricao curta",
        "complete": "Descricao completa"
      }]
    }}
    """
    E eu envio um GET para '/products/1234?include=descriptions'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "category_id": null,
      "properties": null,
      "published": false,
      "descriptions": [
        {"description": {
          "id": 1,
          "locale": "pt-BR",
          "title": "Outro",
          "shorted": "Descricao curta",
          "complete": "Descricao completa"
        }}
      ]
    }}
    """
    E eu devo receber o status code '200'
