# encoding: utf-8

@sprint03
Feature: Criando produtos com descrição
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Scenario: Criando um produto com descrição
    When eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15",
      "category_id": 1,
      "descriptions_attributes": [{
        "locale": "pt-BR",
        "title": "Arma Boladona",
        "shorted": "Descrição curta da AR-15",
        "complete": "Descrição longa da AR-15"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"product": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'

  Scenario: Criando um produto com descrição mas sem locale
    When eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15",
      "descriptions_attributes": [{
        "title": "Arma Boladona",
        "shorted": "Descrição curta da AR-15",
        "complete": "Descrição longa da AR-15"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": {"descriptions.locale": ["Linguagem não pode ficar em branco"]}}
    """
    And eu devo receber o status code '409'

  Scenario: Criando um produto com descrição mas sem title
    When eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15",
      "descriptions_attributes": [{
        "locale": "pt-BR",
        "shorted": "Descrição curta da AR-15",
        "complete": "Descrição longa da AR-15"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": {"descriptions.title": ["Título não pode ficar em branco"]}}
    """
    And eu devo receber o status code '409'

  Scenario: Criando um produto com descrição mas sem shorted
    When eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15",
      "descriptions_attributes": [{
        "locale": "ṕt-BR",
        "title": "Arma Boladona",
        "complete": "Descrição longa da AR-15"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": {"descriptions.shorted": ["Descrição reduzida não pode ficar em branco"]}}
    """
    And eu devo receber o status code '409'

  Scenario: Criando um produto com descrição mas sem complete
    When eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15",
      "descriptions_attributes": [{
        "locale": "ṕt-BR",
        "title": "Arma Boladona",
        "shorted": "Descrição curta da AR-15"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": {"descriptions.complete": ["Descrição completa não pode ficar em branco"]}}
    """
    And eu devo receber o status code '409'