# encoding: utf-8

When "que exite uma categoria com o nome '$name' e id '$id'" do |name, id|
  FactoryGirl.create(:category, name: name, id: id)
end

When "a categoria '$name_filho' for filha de '$name_pai'" do |name_filho, name_pai|
  pai = FactoryGirl.create(:category, name: name_pai)
  FactoryGirl.create(:category, name: name_filho, parent_id: pai.id)
end

When "a categoria '$categoria' deve ter sido deletada" do |category|
  Category.find_by_id(category).should be_nil
end

When "que exite uma categoria com o nome '$name', id '$id' e parent_id '$parent_id'" do |name, id, parent_id|
  FactoryGirl.create(:category, name: name, id: id, parent_id: parent_id)
end
