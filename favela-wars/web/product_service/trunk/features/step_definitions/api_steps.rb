# encoding: utf-8

When "eu devo receber o status code '$code'" do |code|
  last_response.status.should == code.to_i
end

When "eu envio um GET para '$path'" do |path|
  get path
end

When "eu envio um POST para '$path' com os dados:" do |path, params|
  post path, params
end

When "eu envio um PUT para '$path' com os dados:" do |path, params|
  put path, JSON.parse(params).to_json
end

When "eu envio um DELETE para '$host/$path/$id'" do |host, path, id|
  delete "#{path}/#{id}"
end

Then "eu devo receber o link para a próxima página '$url'" do |url|
  parsed_response['next_page'].to_i.should == url.to_i
end

Then "eu devo receber o link para a página anterior '$url'" do |url|
  parsed_response['previous_page'].to_i.should == url.to_i
end

Then /^eu não devo receber um link para a página anterior$/ do
  parsed_response['previous_page'].should be_nil
end

Then /^eu não devo receber um link para a próxima página$/ do
  parsed_response['next_page'].should be_nil
end

Then "eu devo receber o JSON:" do |json|
  JSON.parse(last_response.body).should == JSON.parse(json)
end


def parsed_response
  JSON.parse(last_response.body)
end
