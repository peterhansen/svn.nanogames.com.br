# encoding: utf-8

When "que existe um destaque com id '$id'" do |id|
  FactoryGirl.create(:highlight, id: id)
end