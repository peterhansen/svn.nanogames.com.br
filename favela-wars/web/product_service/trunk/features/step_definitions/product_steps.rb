# encoding: utf-8

When "que exite um produto com o nome '$n' e id '$n' com a descrição:" do |name, id, description|
  FactoryGirl.create(:product, name: name, id: id, descriptions_attributes: [JSON.parse(description)])
end

When "que exite um produto com o nome '$n' e com a tag '$tag'" do |name, tag|
  FactoryGirl.create(:product, name: name, id: '1234', tag_list: [tag])
end

When "que exite um produto com o nome '$n' e id '$n'" do |name, id|
  FactoryGirl.create(:product, name: name, id: id)
end

When "o produto '$product' deve ter sido disabilitado" do |user|
  ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'products_test')
  Product.find(user).should_not be_published
end

When "o produto não deve ter nenhuma tag" do
  Product.find(1234).tag_list.should be_nil
end

Given "que exite a tag '$name'" do |name|
  FactoryGirl.create(:tag, name: name)
end

Given /^que existem (\d+) produtos cadastrados$/ do |n|
  n.to_i.times do
    FactoryGirl.create(:product)
  end
end

Given /^que existem os produtos:$/ do |products|
  products.hashes.each do |attributes|
    FactoryGirl.create :product, attributes
  end
end
