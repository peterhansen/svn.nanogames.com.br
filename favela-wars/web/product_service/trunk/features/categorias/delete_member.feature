# language: pt

@sprint03
Funcionalidade: categorias
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Contexto:
    Dado que exite uma categoria com o nome 'Armas' e id '1234'

  Cenário: uma categoria
    Quando eu envio um DELETE para '/categories/1234.json'
    Então eu devo receber o JSON:
  """
    {"success": "Category deleted"}
    """
  #    E a categoria '1234' deve ter sido deletada
    E eu devo receber o status code '200'

  Cenário: deletar uma categoria que não existe
    Quando eu envio um DELETE para '/categories/9999.json'
    Então eu devo receber o status code '404'