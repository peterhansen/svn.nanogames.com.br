@sprint04
Feature: Consultando categorias
  Background:
    Given que exite uma categoria com o nome 'Armas' e id '1234'

  Scenario: Consultando uma categoria válida
    When eu envio um GET para '/categories/1234'
    Then eu devo receber o JSON:
    """
    {"category": {
      "id": 1234,
      "name": "Armas",
      "position": null,
      "parent_id": null,
      "slug": null
    }}
    """
    And eu devo receber o status code '200'

  Scenario: Consultando um usuário inválido
    When eu envio um GET para '/categories/9999'
    And eu devo receber o status code '404'
