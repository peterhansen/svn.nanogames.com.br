# language: pt
# encoding: utf-8

@sprint04
Funcionalidade: Consultando a lista de categorias
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Cenário: a lista de categorias
    Dado que exite uma categoria com o nome 'Armas' e id '1234'
    E que exite uma categoria com o nome 'Escudos' e id '4567'
    Quando eu envio um GET para '/categories'
    Então eu devo receber o JSON:
    """
    { "categories": [
        {"category": {
          "id": 1234,
          "name": "Armas",
          "parent_id": null,
          "position": null,
          "slug": null }},
        {"category": {
          "id": 4567,
          "name": "Escudos",
          "parent_id": null,
          "position": null,
          "slug": null }}
        ],
      "total": 2
    }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando a lista de categorias com paginação
    Dado que exite uma categoria com o nome 'Granadas' e id '4321'
    E que exite uma categoria com o nome 'Machetes' e id '9876'
    E que exite uma categoria com o nome 'Armas' e id '1234'
    E que exite uma categoria com o nome 'Escudos' e id '4567'
    Quando eu envio um GET para '/categories?page=2&page_size=2'
    Então eu devo receber o JSON:
    """
    { "categories": [
        {"category": {
          "id": 4567,
          "name": "Escudos",
          "parent_id": null,
          "position": null,
          "slug": null }},
        {"category": {
          "id": 4321,
          "name": "Granadas",
          "parent_id": null,
          "position": null,
          "slug": null }}
        ],
      "total": 4,
      "previous_page": 1
    }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando o contador de categorias
    Dado que exite uma categoria com o nome 'Armas' e id '1234'
    E que exite uma categoria com o nome 'Escudos' e id '4567'
    Quando eu envio um GET para '/categories/count'
    Então eu devo receber o JSON:
    """
    { "category_count": 2 }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando a lista de categorias aninhadas
    Dado a categoria 'Ant Fire' for filha de 'Fire Machines'
    Quando eu envio um GET para '/categories/get_all_nested'
    Então eu devo receber o JSON:
    """
    { "categories": [
      { "category": {
        "id": 1,
        "name": "Fire Machines",
        "parent_id": null,
        "position": null,
        "slug": null,
        "children": [
          { "id": 2,
            "name": "Ant Fire",
            "position": null,
            "parent_id": 1,
            "slug": null }
          ]}
        }
      ]
    }
    """
    E eu devo receber o status code '200'
