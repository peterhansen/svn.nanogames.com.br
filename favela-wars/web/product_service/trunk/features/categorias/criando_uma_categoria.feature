@sprint04
Feature: Criando categorias
  Operação relacionadas.

  Scenario: Criando uma categoria
    When eu envio um POST para '/categories.json' com os dados:
    """
    {"category": {
      "name": "Armas"
    }}
    """

    Then eu devo receber o JSON:
    """
    {"category": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'

  Scenario: Tentando criar uma categoria com campos inválidos
    When eu envio um POST para '/categories.json' com os dados:
    """
    {"category": {
      "attributo_inválido": "Chuck Norris",
      "name": "Armas"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": "unknown attribute: attributo_inválido"}
    """
    And eu devo receber o status code '400'

  Scenario: Tentando criar uma categoria sem nome
    When eu envio um POST para '/categories.json' com os dados:
    """
      {"category": {
        "name": ""
      }}
      """
    Then eu devo receber o JSON:
    """
      { "errors": { "name": [ "can't be blank" ] } }
      """
    And eu devo receber o status code '409'

  Scenario: Tentando criar uma categoria com um nome já utilizado
    Given que exite uma categoria com o nome 'Armas' e id '1234'
    When eu envio um POST para '/categories.json' com os dados:
    """
    {"category": {
      "name": "Armas"
    }}
    """
    Then eu devo receber o JSON:
    """
    { "errors": { "name": [ "Este nome já esta sendo utilizado em outra categoria." ] } }
    """

  Scenario: Criando uma categoria com descrição
    When eu envio um POST para '/categories.json' com os dados:
    """
    {"category": {
      "name": "Arma",
      "descriptions_attributes": [{
        "locale": "pt-BR",
        "title": "Arma Boladona",
        "shorted": "Descricao curta da AR-15",
        "complete": "Descricao longa da AR-15"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"category": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'