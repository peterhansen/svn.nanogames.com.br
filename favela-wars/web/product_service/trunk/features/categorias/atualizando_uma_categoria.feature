# encoding: utf-8

@sprint04
Feature: Atualizando catagorias
  O serviço 'ProductService' será o hub central de funcionalidades de produtos, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Background:
    Given que exite uma categoria com o nome 'Armas' e id '1234'

  Scenario: Atualizando dados de uma categoria
    When eu envio um PUT para '/categories/1234.json' com os dados:
    """
    {"category": {
      "name": "Granadas"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"category": {
      "id": 1234
    }}
    """
    And eu devo receber o status code '200'

  Scenario: Tentando atualizar dados de uma categoria inexistente
    When eu envio um PUT para '/categories/9999.json' com os dados:
    """
    {"category": {
      "name": "Granadas"
    }}
    """
    Then eu devo receber o status code '404'

  Scenario: Tentando atualizando dados de uma categoria com dados inválidos
    Given que exite uma categoria com o nome 'Granadas' e id '4567'
    When eu envio um PUT para '/categories/4567.json' com os dados:
    """
    {"category": {
      "name": "Armas"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": {"name": ["Este nome já esta sendo utilizado em outra categoria."]}}
    """
    And eu devo receber o status code '409'
