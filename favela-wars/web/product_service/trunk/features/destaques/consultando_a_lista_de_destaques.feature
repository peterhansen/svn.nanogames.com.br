# language: pt

@sprint06
Funcionalidade: Consultando a lista de destaque
  Cenário: Consultando a lista de destaque
    Dado que existe um destaque com id '1234'
    Quando eu envio um GET para '/highlights.json'
    Então eu devo receber o JSON:
    """
    {"highlights": [{
      "highlight": {
        "id": 1234,
        "link": "link qualquer"
      }
    }],
    "total": 1}
    """
    E eu devo receber o status code '200'
