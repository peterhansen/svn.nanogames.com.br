# language: pt

@sprint06
Funcionalidade: Atualizando um destaque
  Cenário: Criando um produto
    Dado que existe um destaque com id '1234'
    Quando eu envio um PUT para '/highlights/1234.json' com os dados:
    """
    {"highlight": {
      "link": "outro link"
    }}
    """
    Então eu devo receber o JSON:
    """
    {"highlight": {
      "id": 1234,
      "link": "outro link"
    }}
    """
    E eu devo receber o status code '200'
