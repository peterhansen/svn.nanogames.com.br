# encoding: utf-8
# language: pt

#@sprint07
#Funcionalidade: Trabalhos com promoções de produtos
  #Cenário: Criar uma promoção já informando os produtos dela
    #Dado que existem os produtos:
      #| name    | id   |
      #| AR-15   | 1 |
      #| AK-47   | 2 |
      #| AK-48   | 3 |
    #Quando eu envio um POST para '/promotions.json' com os dados:
    #"""
    #{"promotion": {
      #"name": "Promoção de versão",
      #"discount_mode":"1",
      #"discount_value":"90.0",
      #"start":"10/01/2012 15:40",
      #"end": "10/12/2012 23:59",
      #"product_ids": [ 1, 2, 3 ]
    #}}
    #"""
    #Então eu devo receber o JSON:
    #"""
    #{"promotion": {
      #"id": 1
    #}}
    #"""
    #E eu devo receber o status code '201'

  #Cenário: Incluir novos produtos numa promoção existente
    #Dado que existem os produtos:
      #| name     | id |
      #| AR-122   | 4  |
      #| AK-471   | 5  |
      #| AK-482   | 6  |
    #Quando eu envio um PUT para '/promotions/1' com os dados:
    #"""
    #{"promotion": {
      #"product_ids": [4, 5, 6]
    #}}
    #"""
    #Então eu devo receber o status code '201'
