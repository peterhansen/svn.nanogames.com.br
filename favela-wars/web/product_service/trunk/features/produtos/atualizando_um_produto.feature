# encoding: utf-8
# language: pt

@sprint03
Funcionalidade: Atualizando produtos

  Contexto:
    Dado que exite um produto com o nome 'AR-15' e id '1234'

  Cenário: Atualizando dados de um produto
    Quando eu envio um PUT para '/products/1234.json' com os dados:
    """
    {"product": {
      "name": "AK-47",
      "price": 10,
      "price_black": 30,
      "published": false
    }}
    """
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234
    }}
    """
    E eu devo receber o status code '200'

  Cenário: Atualizando dados de um produto
    Quando eu envio um PUT para '/products/1234.json' com os dados:
    """
    {"product": {
      "id": 1,
      "tag_list": ["banana", "pera", "maça", "salada-mista"]
    }}
    """
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234
    }}
    """
    E eu devo receber o status code '200'

  Cenário: Tentando atualizar dados de um produto inexistente
    Quando eu envio um PUT para '/products/9999.json' com os dados:
    """
    {"product": {
      "name": "AK-47"
    }}
    """
    Então eu devo receber o status code '404'

  Cenário: Tentando atualizando dados de um produto com dados inválidos
    Dado que exite um produto com o nome 'AK-47' e id '4567'
    Quando eu envio um PUT para '/products/4567.json' com os dados:
    """
    {"product": {
      "name": "AR-15"
    }}
    """
    Então eu devo receber o JSON:
    """
    {"errors": {"name": ["Este nome já esta sendo utilizado em outro produto."]}}
    """
    E eu devo receber o status code '409'

  Cenário: Atualizando dados de um produto
    Quando eu envio um PUT para '/products/1234.json' com os dados:
    """
    {"product": {
      "properties": {
        "damage": 999
      }
    }}
    """
    E eu envio um GET para '/products/1234.json?include=properties'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "category_id": null,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "published": false,
      "properties": {
        "damage": 999
      }
    }}
    """
    E eu devo receber o status code '200'

  Cenário: Adicionando assets a um produto
    Quando eu envio um PUT para '/products/1234.json' com os dados:
    """
    {
      "product": {
        "asset_ids": [1]
      }
    }
    """
    E eu devo receber o status code '200'
    E eu envio um GET para '/products/1234.json?include=assets'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "category_id": null,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "published": false,
      "properties": null,
      "assets": [{
        "asset": {"name": "mapa_teste", "version":1}
      }]
    }}
    """

  @wip
  Cenário: Adicionando um asset a um produto
    Quando eu envio um POST para '/products/1234/assets.json' com os dados:
    """
    {
      "asset": {
        "id": 1
      }
    }
    """
    E eu devo receber o status code '201'
    E eu envio um GET para '/products/1234.json?include=assets'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "category_id": null,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "published": false,
      "properties": null,
      "assets": [{
        "asset": {"name": "mapa_teste", "version":1}
      }]
    }}
    """