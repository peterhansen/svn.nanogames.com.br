# language: pt

@sprint03
Funcionalidade: Consultando a lista de produtos
  Cenário: Consultando a lista de produtos
    Dado que existem os produtos:
      | name    | id   | created_at |
      | AR-15   | 1234 | 03/03/1970 |
      | AK-47   | 4567 | 04/04/1970 |
    Quando eu envio um GET para '/products'
    Então eu devo receber o JSON:
    """
    { "products": [
        {"product": {
          "id": 4567,
          "name": "AK-47",
          "price": 0,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }},
        {"product": {
          "id": 1234,
          "name": "AR-15",
          "price": 0,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }}
        ],
      "total": 2
    }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando a lista de produtos com paginação
    Dado que existem os produtos:
      | name    | id   | created_at |
      | Revorve | 789  | 01/01/1970 |
      | Pistola | 951  | 02/02/1970 |
      | AR-15   | 1234 | 03/03/1970 |
      | AK-47   | 4567 | 04/04/1970 |
    Quando eu envio um GET para '/products?page=2&page_size=2'
    Então eu devo receber o JSON:
    """
    { "products": [
        {"product": {
          "id": 951,
          "name": "Pistola",
          "price": 0,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }},
        {"product": {
          "id": 789,
          "name": "Revorve",
          "price": 0,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }}
        ],
      "total": 4,
      "previous_page": 1
    }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando o contador de produtos
    Dado que exite um produto com o nome 'AR-15' e id '1234'
    E que exite um produto com o nome 'AK-47' e id '4567'
    Quando eu envio um GET para '/products/count'
    Então eu devo receber o JSON:
    """
    { "product_count": 2 }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando uma lista com paginação
    Dado que existem 10 produtos cadastrados
    Quando eu envio um GET para '/products.json?page=2&page_size=3'
    Então eu devo receber o link para a próxima página '3'
    E eu devo receber o link para a página anterior '1'

  Cenário: Consultando a primeira página de uma lista
    Dado que existem 10 produtos cadastrados
    Quando eu envio um GET para '/products.json?page_size=5'
    Então eu devo receber o link para a próxima página '2'
    E eu não devo receber um link para a página anterior

  Cenário: Consultando a última página de uma lista
    Dado que existem 10 produtos cadastrados
    Quando eu envio um GET para '/products.json?page=5&page_size=2'
    Então eu não devo receber um link para a próxima página
    E eu devo receber o link para a página anterior '4'

  Cenário: Consultando a lista de produtos ordenado por preço
    Dado que existem os produtos:
      | name    | price |
      | AR-15   | 10    |
      | AK-47   | 15    |
      | Revorve | 5     |
    Quando eu envio um GET para '/products?order=price&page_size=2'
    Então eu devo receber o JSON:
    """
    { "products": [
        {"product": {
          "id": 3,
          "name": "Revorve",
          "price": 5,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }},
        {"product": {
          "id": 1,
          "name": "AR-15",
          "price": 10,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }}
        ],
      "total": 3,
      "next_page": 2,
      "order": "price"
    }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando uma lista de produtos por id
    Dado que existem os produtos:
      | name    | price | id |
      | AR-15   | 10    | 1  |
      | AK-47   | 15    | 2  |
      | Revorve | 5     | 3  |
      | Trabuco | 9     | 4  |
    Quando eu envio um GET para '/products.json?ids=2,4'
    Então eu devo receber o JSON:
    """
    { "products": [
        {"product": {
          "id": 2,
          "name": "AK-47",
          "price": 15,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }},
        {"product": {
          "id": 4,
          "name": "Trabuco",
          "price": 9,
          "price_black": 0,
          "properties": null,
          "category_id": null,
          "published": false }}
        ],
        "total": 2
      }
    """
