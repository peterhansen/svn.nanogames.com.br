# language: pt

@sprint03
Funcionalidade: Deletando produtos

  Contexto:
    Dado que exite um produto com o nome 'AR-15' e id '1234'

  Cenário: Deletando um produto
    Quando eu envio um DELETE para '/products/1234'
    Então eu devo receber o JSON:
    """
    {"success": "Product disabled"}
    """
    E o produto '1234' deve ter sido disabilitado
    E eu devo receber o status code '200'

#  Cenário: Deletando um usuário pelo login
#    Quando eu envio um DELETE para '/users/chucknorris'
#    Então eu devo receber o JSON:
#    """
#    {"success": "User deleted"}
#    """
#    E o usuário 'chucknorris' deve ter sido deletado do banco
#    E eu devo receber o status code '200'
#
#  Cenário: Tentando deletar um usuário que não existe
#    Quando eu envio um DELETE para '/users/lindomar'
#    Então eu devo receber o status code '404'