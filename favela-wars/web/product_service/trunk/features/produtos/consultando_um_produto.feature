# language: pt

@sprint03
Funcionalidade: Consultando um produto

  Contexto:
    Dado que exite um produto com o nome 'AR-15' e id '1234'

  Cenário: Consultando um produto válido
    Quando eu envio um GET para '/products/1234?include=main_image_id'
    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1234,
      "name": "AR-15",
      "price": 0,
      "price_black": 0,
      "category_id": null,
      "published": false,
      "properties": null,
      "main_image_id": null
    }}
    """
    E eu devo receber o status code '200'

  Cenário: Consultando um usuário inválido
    Quando eu envio um GET para '/products/9999'
    Então eu devo receber o status code '404'
