# language: pt

@sprint03
Funcionalidade: Criando produtos

  Cenário: Criando um produto
    Quando eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15"
    }}
    """

    Então eu devo receber o JSON:
    """
    {"product": {
      "id": 1
    }}
    """
    E eu devo receber o status code '201'

  Cenário: Tentando criar um produto com campos inválidos
    Quando eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "attributo_inválido": "Chuck Norris",
      "name": "AR-15"
    }}
    """
    Então eu devo receber o JSON:
    """
    {"errors": "unknown attribute: attributo_inválido"}
    """
    E eu devo receber o status code '400'

  Cenário: Tentando criar um usuário sem login
    Quando eu envio um POST para '/products.json' com os dados:
    """
      {"product": {
        "name": ""
      }}
      """
    Então eu devo receber o JSON:
    """
      { "errors": { "name": [ "can't be blank" ] } }
      """
    E eu devo receber o status code '409'

  Cenário: Tentando criar um usuário com um login já utilizado
    Quando que exite um produto com o nome 'AR-15' e id '1234'
    Quando eu envio um POST para '/products.json' com os dados:
    """
    {"product": {
      "name": "AR-15"
    }}
    """
    Então eu devo receber o JSON:
    """
    { "errors": { "name": [ "Este nome já esta sendo utilizado em outro produto." ] } }
    """