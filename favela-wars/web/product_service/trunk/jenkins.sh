#!/bin/bash
zero="0"

export RVM_HOME=$HOME/.rvm/bin
export GEM_HOME=$HOME/.rvm/gems
export RUBIES_HOME=$HOME/.rvm/rubies
export SINATRA_ENV=test

$RVM_HOME/rvm 1.9.3@product_service
# $RUBIES_HOME/ruby-1.9.3-p0/bin/gem install bundler
$RVM_HOME/rvm --with-rubies 1.9.3 exec $GEM_HOME/ruby-1.9.3-p0@global/bin/bundle
$RVM_HOME/rvm --with-rubies 1.9.3 exec $RUBIES_HOME/ruby-1.9.3-p0/bin/rake migrate
$RVM_HOME/rvm --with-rubies 1.9.3 exec $GEM_HOME/ruby-1.9.3-p0@product_service/bin/cucumber -f progress

if [ $? -gt $zero ]
then
    exit 5
fi

$RVM_HOME/rvm --with-rubies 1.9.3 exec $GEM_HOME/ruby-1.9.3-p0@product_service/bin/rspec

if [$? -gt $zero];
then
    exit 5
else
    echo Success
    exit 0
fi