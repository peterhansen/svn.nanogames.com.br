FactoryGirl.define do
  sequence :name do |n|
    "Name-#{rand}"
  end

  factory :product, class: Product do
    name

    # after_create do |product, evaluator|
    #   FactoryGirl.create_list(:description, 1, descriptible: product)
    # end
  end

  factory :pack do
    name
  end

  factory :item do
    name
  end

  factory :description do
    title  'Titulo da descricao'
    locale {rand.to_s}
    shorted 'Descricao curta'
    complete 'Descricao completa'
  end

  factory :category, class: Category do
    name
  end

  factory :tag, class: Tag  do
    name
    slug 'new_tag'
  end

  factory :highlight, class: Highlight do
    link 'link qualquer'
  end
end
