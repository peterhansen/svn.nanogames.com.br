ENV["SINATRA_ENV"] ||= "test"

require_relative '../lib/product_service'
require 'rack/test'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)
require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])

set :environment, :test

Mail.defaults do
  delivery_method :test
end

DatabaseCleaner.strategy = :truncation

Dir.glob(File.join(File.dirname(__FILE__), 'factories.rb')).each {|f| require f }

RSpec.configure do |config|
  config.before(:each) do
    ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'products_test')
    DatabaseCleaner.clean
    Mail::TestMailer.deliveries.clear
  end
end
