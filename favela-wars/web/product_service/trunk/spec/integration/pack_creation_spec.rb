# encoding: utf-8
require_relative '../spec_helper'

set :show_exceptions, false

describe "Criando pacotes", type: :request do
  include Rack::Test::Methods

  def app
    ProductService::App.new
  end

  it "deve criar um pacote" do
    post '/products.json', {product: {name: 'Pacote', type: 'Pack'}}.to_json
    post '/products.json', {product: {name: 'Treinamento', type: 'Trainning'}}.to_json
    post '/products.json', {product: {name: 'Produto'}}.to_json
    ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'products_test')
    Pack.all.size.should == 1
    Trainning.all.size.should == 1
    Product.all.size.should == 3
  end

  it "deve retornar os produtos" do
    description = {
      locale: "pt-BR",
      title: "Arma Boladona",
      shorted: "Descrição curta da AR-15",
      complete: "Descrição longa da AR-15"
    }

    post '/products.json', {product: {name: 'Pacote', type: 'Pack', descriptions_attributes: [description]}}.to_json
    ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'products_test')
    get '/products.json'
    last_response.body.should == "{\"products\":[{\"pack\":{\"category_id\":null,\"id\":1,\"name\":\"Pacote\",\"price\":0,\"properties\":null,\"published\":false}}],\"total\":1}"
  end
end
