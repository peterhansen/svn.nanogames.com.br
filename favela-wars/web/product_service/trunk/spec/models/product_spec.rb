require_relative '../spec_helper'

describe Product do
  let(:product) { Product.new }

  it "should accept nested attributes" do
    product.should respond_to('descriptions_attributes=')
  end

  describe 'Tags' do
    it "should create tags when creating or updating" do
      lambda do
        Product.create(name: 'Teste', tag_list: ['Tag1', 'Tag2'])
      end.should change(Tag, :count).by(2)
    end
  end

  describe 'Pack' do
    it "should exist" do
      Pack.new.class.should == Pack
    end

    it "deve retornar packs e items" do
      pack = FactoryGirl.create(:pack)
      item = FactoryGirl.create(:item)
      products = Product.all
      products.should include(pack)
      products.should include(item)
    end
  end

  describe 'Item' do
    it "spec_name" do
      Item.new.class.should == Item
    end
  end

  describe 'Trainning' do
    it "spec_name" do
      Trainning.new.class.should == Trainning
    end
  end
end
