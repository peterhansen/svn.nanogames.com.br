require_relative '../spec_helper'

describe Tag do
  it "should create convert name to slug" do
    tag = Tag.create name: 'Testando Slug'
    tag.slug.should == 'testando_slug'
  end
end