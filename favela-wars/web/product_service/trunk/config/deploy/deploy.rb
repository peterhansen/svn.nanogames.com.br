namespace :deploy do
  desc <<-DESC
  A macro-task that updates the code and fixes the symlink.
  DESC
  task :default do
    transaction do
      update_code
      create_symlink
    end
  end

  after 'deploy:setup' do
    transaction do
      update_code
      create_symlink
      nginx.config
      unicorn.config
      nginx.restart
    end
  end

  after 'deploy:update_code' do
    bundle
    db.symlink
    db.create
    migrate
  end

  after 'deploy:create_symlink' do
    cleanup
    custom_symlinks
    unicorn.restart
  end

  desc "This task updates the gem dependencies in the server by running the 'bundle' command."
  task :bundle do
    pp "Running Bundler"
    run "cd #{release_path} && bundle --local --without=#{env == 'production' ? 'development test' : 'test'}"
  end

  task :custom_symlinks do
    pp "Creating custom symlinks"
    run "rm -r #{release_path}/lib/public/uploads; ln -s #{shared_path}/public/uploads #{release_path}/lib/public/uploads"
    run "ln -s #{shared_path}/log/ #{release_path}/log"
  end

  before 'deploy:cleanup' do
    pp "Cleaning up old releases"
  end

  before 'deploy:update_code' do
    pp "Exporting code to the server"
  end

  before 'deploy:migrate' do
    pp "Migrating database"
  end

  before 'deploy:symlink' do
    pp "Criando symlink"
  end

  task :update_code, :except => {:no_release => true} do
    on_rollback { run "rm -rf #{release_path}; true" }
    strategy.deploy!
  end
end
