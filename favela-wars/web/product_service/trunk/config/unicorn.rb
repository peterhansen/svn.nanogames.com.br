worker_processes 1
working_directory "/home/ubuntu/product_service/current"

listen "/tmp/product_service.sock", :backlog => 64
timeout 30

pid "/home/ubuntu/product_service/shared/pids/unicorn.pid"
stderr_path "/home/ubuntu/product_service/shared/log/unicorn.stderr.log"
stdout_path "/home/ubuntu/product_service/shared/log/unicorn.stdout.log"

preload_app true

GC.respond_to?(:copy_on_write_friendly=) and GC.copy_on_write_friendly = true