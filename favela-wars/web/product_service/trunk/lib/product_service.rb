# encoding: utf-8

require_relative 'boot'

module ProductService
  class App < Sinatra::Base
    set :environment, :production
    use ConnectionManager
    use CrossOrigin
    use Router

    # TODO dimas - adicionar config block com configurações do projeto
    # TODO dimas - configurar o content_type em um before filter

    #configure do
    #  # para impedir que retorne uma página de erro html
    #  set :show_exceptions, false
    #  content_type :json
    #end

    #error do
    #  # retornar json do erro
    #end
  end
end