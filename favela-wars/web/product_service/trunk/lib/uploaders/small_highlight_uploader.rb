class SmallHighlightUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  ::ServiceContractBase.settings.thumb_sizes['highlight_small'].each do |size|
    size.match /(\d+)x(\d+)/

    version "v#{$1}", if: :generate_thumbs? do
      process :resize_to_fill => [$1.to_i, $2.to_i]
    end
  end

  def store_dir
    'uploads'
  end

  def filename
    if original_filename.present? and super.present?
      if model && model.read_attribute(:small_image).present?
        @name ||= model.read_attribute(:small_image)
      else
        @name ||= "#{timestamp}-#{super}"
      end
    end
  end

  def timestamp
    var = :"@#{mounted_as}_timestamp"
    model.instance_variable_get(var) or model.instance_variable_set(var, Time.now.to_i)
  end

  protected

  def generate_thumbs?(new_file)
    #!model.skip_thumbs?
    true
  end
end
