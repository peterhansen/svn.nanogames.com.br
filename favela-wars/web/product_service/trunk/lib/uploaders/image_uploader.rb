class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::RMagick
  storage :file

  ::ServiceContractBase.settings.thumb_sizes['image'].each do |size|
    size.match /(\d+)x(\d+)/

    version "v#{$1}", if: :generate_thumbs? do
      process :resize_to_fill => [$1.to_i, $2.to_i]
    end
  end

  def store_dir
    'uploads'
  end
  
  protected

  def generate_thumbs?(new_file)
    !model.skip_thumbs?
  end
end