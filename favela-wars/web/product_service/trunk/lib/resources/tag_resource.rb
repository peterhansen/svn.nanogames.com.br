# encoding: utf-8
require_relative '../data/product'
require_relative '../data/tag'

class TagResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include)
    tag = Tag.find(id)
    tag.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Tag not found')
  end

  # @return [String]
  def self.get_all(conditions = {})
    Tag.order('created_at desc').select(Tag::PUBLIC_FIELDS).paginate(:page => conditions[:page], :per_page => conditions[:page_size])
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    tag = Tag.create(attributes)
    raise ValidationException.new(tag.errors.messages) unless tag.valid?
    return tag.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    tag = Tag.find_by_id(id)
    if tag
      tag.update_attributes!(attributes)
      return tag.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Tag not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(tag.errors.messages)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    tag = Tag.find(id)

    if tag
      tag.destroy
      {success: 'Tag disabled'}.to_json
    else
      raise ResourceNotFoundException.new('Tag not found')
    end
  end

  def self.count
    Tag.count
  end
end