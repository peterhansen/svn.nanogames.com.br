# encoding: utf-8
require_relative '../data/image'

#module ProductService
class ImageResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include)
    image = Image.find(id)
    image.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Image not found')
  end

  # @return [String]
  def self.get_all(page = nil, page_size = nil)
    {images: Image.order('created_at desc').select(Image::PUBLIC_FIELDS).paginate(:page => page, :per_page => page_size)}.to_json
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    image = Image.new
image.skip_thumbs = attributes['generate_thumbs'] != '1'
    #image.define_singleton_method :generate_thumbs? do
    #  attributes['generate_thumbs'] == '1'
    #end

    image.update_attributes(attributes)
    raise ValidationException.new(image.errors.messages) unless image.valid?
    return image.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

# @param id [String]
# @param attributes [Hash]
# @return [String]
  def self.update_resource(id, attributes)
    image = Image.find_by_id(id)
    if image.inspect
      image.update_attributes!(attributes)
      return image.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Image not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(image.errors.messages)
  end

# @param id [Sting]
# @return [String]
  def self.delete_resource(id)
    image = Image.find(id)

    if image
      image.destroy
      {success: 'Image disabled'}.to_json
    else
      raise ResourceNotFoundException.new('Image not found')
    end
  end

  def self.count
    {image_count: Image.count}.to_json
  end
end