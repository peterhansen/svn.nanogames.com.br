# encoding: utf-8
require_relative '../data/product'
require_relative '../data/promotion'

class PromotionResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include)
    entity = Promotion.find(id)
    entity.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Tag not found')
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    entity = Promotion.create(attributes)
    raise ValidationException.new(entity.errors.messages) unless entity.valid?
    return entity.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end


end