# encoding: utf-8
require_relative '../data/category'

class CategoryResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]

  def self.get_resource(id, include = '')
    category = Category.find(id)
    category.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Category not found')
  end

  # @return [String]
  # @param page [Numeric]
  # @param page_size [Numeric]ca
  def self.get_all(conditions = {})
    Category.order('created_at desc')
        .select(Category::PUBLIC_FIELDS)
        .paginate(:page => conditions[:page], :per_page => conditions[:page_size])
  end

  # @return [String]
  def self.get_all_nested
    {categories:
         Category.where('parent_id is null')
         .includes(:children)
         .order('name desc')
         .select(Category::PUBLIC_FIELDS)
    }.to_json(:include => {
        :children => {
            :except => [:created_at,
                        :updated_at,
                        :parent_id]
        }
    })
  end

  # @return [String]
  def self.count
    Category.count
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    category = Category.create(attributes)
    raise ValidationException.new(category.errors.messages) unless category.valid?
    return category.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    category = Category.find_by_id(id)
    if category
      category.destroy
      {success: 'Category deleted'}.to_json
    else
      raise ResourceNotFoundException.new('Category not found')
    end
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    category = Category.find_by_id(id)

    if category
      category.update_attributes!(attributes)
      return category.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Category not found')
    end
  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(category.errors.messages)
  end

  def self.parent_candidates_for(id)
    category = Category.find(id)
    all = Category.all
    return ((all - [category]) - category.descendants).to_json
  end

  def self.get_roots
    {categories: Category.where("parent_id is NULL").select([:id, :name])}.to_json
  end
end