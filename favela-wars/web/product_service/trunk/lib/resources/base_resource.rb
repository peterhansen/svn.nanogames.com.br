#module ProductService
  class BaseResource
    class BasicException < StandardError
      attr_accessor :details
      HTTP_STATUS_CODE = 400

      def initialize details
        @details = details
      end
    end

    class ResourceNotFoundException < BasicException
      HTTP_STATUS_CODE = 404
    end

    class InvalidRequestException < BasicException
      HTTP_STATUS_CODE = 400
    end

    class ValidationException < BasicException
      HTTP_STATUS_CODE = 409
    end

    def self.extract_options(fields)
      opt = {:methods => [], :include => {}}
      if fields
        fields = fields.split(',')
        fields.each do |field|
          # TODO generalizar isso
          #if Product.instance_methods.include?("#{field}_attributes=".to_sym)
          #  opt[:include][field.to_sym] = {:except => Description::PRIVATE_FIELDS}
          #else
            opt[:methods] << field.to_sym
          #end
        end
      end
      opt
    end
  end
#end