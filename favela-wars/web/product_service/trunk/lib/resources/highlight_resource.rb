# encoding: utf-8
require_relative '../data/highlight'

class HighlightResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, options = '')
    highlight = Highlight.find(id)
    highlight.to_json(extract_options(options))

  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Destaque não encontrado')
  end

  # @return [Array]
  # @param conditions [Hash]
  def self.get_all(conditions = {})
    Highlight.select(Highlight::PUBLIC_FIELDS).all
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    highlight = Highlight.find_by_id(id)

    if highlight
      highlight.update_attributes(attributes)
      highlight.valid?
      return highlight.reload.to_json only: :id, methods: []
    else
      raise ResourceNotFoundException.new('Destaque não encontrado')
    end
  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(highlight.errors.messages)
  end
end
