# encoding: utf-8
require_relative '../data/product'
require_relative '../data/tag'
require_relative '../data/tagging'
require_relative '../data/description'
require_relative '../data/pack'
require_relative '../data/item'
require_relative '../data/trainning'
require_relative '../data/assets_products'

class ProductResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, options)
    product = Product.find(id)
    product.to_json(extract_options(options))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Product not found')
  end

  # @return [Array]
  # @param conditions [Hash]
  def self.get_all(conditions = {})
    query = 'descriptions.locale = "pt-BR" '
    values = []

    if conditions[:category]
      category = Category.find(conditions[:category])
      categories = [conditions[:category]]
      categories << category.descendants.map { |desc| desc.id.to_s }
      if categories.flatten!.any?
        query += ' and products.category_id in (?)'
        values << categories
      else
        return []
      end
    end

    if conditions[:search]
      query += 'and ('

      query += ' descriptions.title like ?'
      values << "%#{conditions[:search]}%"

      query += ' or descriptions.shorted like ?'
      values << "%#{conditions[:search]}%"

      query += ' or descriptions.complete like ?'
      values << "%#{conditions[:search]}%"

      query += ')'
    end

    if conditions[:ids]
      query += "and products.id in (?)"
      values << conditions[:ids].split(',')
    end

    Product.order(conditions[:order_by] || 'products.created_at desc').
        select(Product::PUBLIC_FIELDS).
        includes(:descriptions).
        where(query, *values).
        paginate(:page => conditions[:page], :per_page => conditions[:page_size])
  end

# @param attributes [Hash]
# @return [String]
  def self.create_resource(attributes)
    Log.info attributes
    product = Product.new(attributes)
    Log.info product.inspect
    product['type'] = attributes['type'] if attributes['type']
    product.save
    raise ValidationException.new(product.errors.messages) unless product.valid?
    return product.to_json(only: :id, methods: [])
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

# @param id [String]
# @param attributes [Hash]
# @return [String]
  def self.update_resource(id, attributes)
    product = Product.find_by_id(id)
    if product
      product.update_attributes!(attributes)
      return product.reload.to_json only: :id, methods: []
    else
      raise ResourceNotFoundException.new('Product not found')
    end
  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(product.errors.messages)
  end

# @param id [Sting]
# @return [String]
  def self.delete_resource(id)
    product = Product.find(id)

    if product
      product.update_attribute :published, false
      {success: 'Product disabled'}.to_json
    else
      raise ResourceNotFoundException.new('Product not found')
    end
  end

  def self.count
    Product.count
  end

end
