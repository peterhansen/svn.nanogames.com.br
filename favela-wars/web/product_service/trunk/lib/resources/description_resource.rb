# encoding: utf-8
require_relative '../data/description'

#module ProductService
  class DescriptionResource < BaseResource
    # @param id [Sting]
    # @param fields [Hash]
    # @return [String]
    def self.get_resource(id, include)
      description = Description.find(id)
      description.to_json(extract_options(include))
    rescue ActiveRecord::RecordNotFound => e
      raise ResourceNotFoundException.new('Description not found')
    end

    # @return [String]
    def self.get_all(page = nil, page_size = nil)
      {descriptions: Description.order('created_at desc').select(Description::PUBLIC_FIELDS).paginate(:page => page, :per_page => page_size)}.to_json
    end

    # @param attributes [Hash]
    # @return [String]
    def self.create_resource(attributes)
      p "\n\nAttributes: #{attributes}\n\n"
      description = Description.create(attributes)
      raise ValidationException.new(description.errors.messages) unless description.valid?
      return description.to_json(only: :id)
    rescue ActiveRecord::UnknownAttributeError => e
      raise InvalidRequestException.new(e.message)
    end

    # @param id [String]
    # @param attributes [Hash]
    # @return [String]
    def self.update_resource(id, attributes)
      description = Description.find_by_id(id)

      if description
        description.update_attributes!(attributes)
        return description.reload.to_json only: :id
      else
        raise ResourceNotFoundException.new('Description not found')
      end

    rescue ActiveRecord::RecordInvalid
      raise ValidationException.new(description.errors.messages)
    end

    # @param id [Sting]
    # @return [String]
    def self.delete_resource(id)
      description = Description.find(id)

      if description
        description.destroy
        {success: 'Description disabled'}.to_json
      else
        raise ResourceNotFoundException.new('Description not found')
      end
    end

    def self.count
      {description_count: Description.count}.to_json
    end
  end
#end