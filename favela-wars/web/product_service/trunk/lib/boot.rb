# encoding: utf-8

# TODO dimas - mudar para RACK_ENV pois é comum entre o Sinatra e o Rails
ENV['SINATRA_ENV'] ||= 'development'

require 'bundler/setup'
Bundler.require(:default, ENV['SINATRA_ENV'])

Game::Client.site = ENV['SINATRA_ENV'] ? 'http://game.staging.nanostudio.com.br' :
                                                'http://localhost:9595'

require 'carrierwave/orm/activerecord'

# TODO dimas - fazer um require dinâmico para esses arquivos.
require_relative 'modules/connection_manager'
require_relative 'modules/router'
require_relative 'modules/cross_origin'

require_relative 'service_contracts/service_contract_base'

ServiceContractBase.set :thumb_sizes,
                        YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/thumb_sizes.yml')

require_relative 'uploaders/image_uploader'
require_relative 'uploaders/thumb_uploader'
require_relative 'uploaders/big_highlight_uploader'
require_relative 'uploaders/small_highlight_uploader'

require_relative 'resources/base_resource'
require_relative 'resources/product_resource'
require_relative 'resources/category_resource'
require_relative 'resources/description_resource'
require_relative 'resources/tag_resource'
require_relative 'resources/image_resource'
require_relative 'resources/highlight_resource'
require_relative 'resources/promotion_resource'

require_relative 'service_contracts/products_service'
require_relative 'service_contracts/categories_service'
require_relative 'service_contracts/descriptions_service'
require_relative 'service_contracts/tags_service'
require_relative 'service_contracts/images_service'
require_relative 'service_contracts/highlights_service'
require_relative 'service_contracts/promotions_service'

#db_config = YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/database.yml')[ENV['SINATRA_ENV']]
#ActiveRecord::Migrator.migrate(ActiveRecord::Migrator.migrations_paths)

Log = ::Logger.new(STDOUT)

# TODO sugestão
#UserCore.configuration do |config|
#  config.env ENV['SINATRA_ENV']
#  config.server_root File.expand_path(File.dirname(__FILE__) + '/../')
#end
#end

module ProductService
  def self.env
    ENV['SINATRA_ENV']
  end

  def self.root
    File.expand_path(File.dirname(__FILE__) + '/..')
  end

  def self.logger
    @logger ||= Logger.new(log_file)
  end

  def self.log_file
    @log_file ||= File.open(log_dir + '/' + env + '.log', "a")
  end

  def self.log_dir
    dir = root + '/log'
    Dir::mkdir(dir) unless FileTest::directory?(dir)
    dir
  end

  def self.db_config
    @db_config ||= YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/database.yml')[ENV['SINATRA_ENV']]
  end
end

ServiceContractBase.set :thumb_sizes, YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/thumb_sizes.yml')

CarrierWave.configure do |config|
  config.permissions = 0666
  config.root = ProductService.root + '/lib/public'
end
