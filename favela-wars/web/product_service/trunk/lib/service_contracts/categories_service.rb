class CategoriesService < ServiceContractBase
  get %r{/categories/get_all_nested.?[\w+]?} do
    begin
      CategoryResource.get_all_nested
    rescue CategoryResource::ResourceNotFoundException => e
      e.class::HTTP_STATUS_CODE
    end
  end

  get '/categories/:id/parent_candidates.json' do |id|
    CategoryResource.parent_candidates_for(id)
  end

  get '/categories/roots.json' do
    CategoryResource.get_roots
  end
end