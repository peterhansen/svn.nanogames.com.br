class ImagesService < ServiceContractBase
  # TODO delete '/images/:id.json'
  post %r{/images/(\d+)/remove_thumbs\.?[\.*]} do |id|
    image = Image.find(id)
    image.remove_image_file!
    image.delete

    200
  end

  post '/images/:id/refresh_thumbs.json' do |id|
    image = Image.find(id)
    image.image_file.recreate_versions!
    200
  end

  delete '/images/:id/thumbs/:size' do |id, size|
    image = Image.find(id)
    file_path = image.thumb_path(size)

    begin
      File.delete file_path
      200
    rescue Errno::ENOENT => e
      [404, {error: 'file not found'}.to_json]
    end
  end

  options '/images/:id/thumbs/:size' do |id, size|
    response.headers['Access-Control-Allow-Methods'] = 'DELETE, POST, GET, OPTIONS, PUT'
    response.headers['Access-Control-Allow-Origin'] = '*'
  end

  post '/images/:id/thumbs/:size.json' do |id, size|
    image       = Image.find(id)
    thumb_name  = image.thumb_path(size)
    parameters  = extract_parameters_from_body

    uploader = ThumbUploader.new
    uploader.download!(parameters["remote_image_file_url"])
    uploader.store!

    thumb_name.match /.*\/(.+)/
    uploader.file.move_to("lib/public/#{uploader.store_dir}/#{$1}")
  end
end