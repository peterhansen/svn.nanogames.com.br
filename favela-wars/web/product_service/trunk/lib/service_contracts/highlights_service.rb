class HighlightsService < ServiceContractBase
  post '/highlights/:id/images/:size/regenerate.json' do |id, size|
    highlight = Highlight.find(id)
    highlight.send("#{size}_image").recreate_versions!
    200
  end

  delete '/highlights/:id/small_image/:thumb.json' do |id, thumb|
    highlight = Highlight.find(id)
    file_path = highlight.thumb_path(:small, thumb)

    begin
      File.delete file_path
      200
    rescue Errno::ENOENT => e
      [404, {error: 'file not found'}.to_json]
    end
  end

  delete '/highlights/:id/big_image/:thumb.json' do |id, thumb|
    highlight = Highlight.find(id)
    file_path = highlight.thumb_path(:big, thumb)

    begin
      File.delete file_path
      200
    rescue Errno::ENOENT => e
      [404, {error: 'file not found'}.to_json]
    end
  end

  put '/highlights/:id/images/:size/:thumb.json' do |id, size, thumb|
    highlight = Highlight.find(id)
    file_path = highlight.thumb_path(size, thumb)
    parameters  = extract_parameters_from_body

    uploader = ThumbUploader.new
    uploader.download!(parameters["highlight"]["remote_#{size}_image_url"])
    uploader.store!

    file_path.match /.*\/(.+)/

    uploader.file.move_to("lib/public/#{uploader.store_dir}/#{$1}")
    200
  end
end
