class ProductsService < ServiceContractBase
  post '/products/:id/refresh_thumbs.json' do |id|
    product = Product.find(id)
    product.images.each do |image|
      image.image_file.recreate_versions!
    end
    200
  end

  post '/products/:id/assets.json' do |id|
    product = Product.find(id)
    parameters = extract_parameters_from_body

    asset = product.assets_products.create(asset_id: parameters['asset']['id'])
    
    201
  end

  delete '/products/:id/assets/:asset_id.json' do |id, asset_id|
    product = Product.find(id)
    asset = product.assets_products.where(asset_id: asset_id)
    asset.delete_all if asset
    200
  end

  def extend_resource(resource, params)
    resource[:category] = params[:category]
  end
end
