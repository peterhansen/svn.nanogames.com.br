class DescriptionsService < ServiceContractBase
  options '/descriptions/:id.json' do |id|
    response.headers['Access-Control-Allow-Methods'] = 'DELETE, POST, GET, OPTIONS, PUT'
    response.headers['Access-Control-Allow-Origin'] = '*'
  end
end