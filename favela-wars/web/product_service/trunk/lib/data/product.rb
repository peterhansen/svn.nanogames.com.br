# encoding: utf-8

class Product < ActiveRecord::Base
  self.inheritance_column = :type
  PUBLIC_FIELDS = [:id, :name, :price, :price_black, :published, :category_id, :properties]

  has_many :descriptions, as: :descriptible
  has_many :taggings
  has_many :tags, through: :taggings
  has_many :images
  has_many :assets_products
  belongs_to :category
  belongs_to :main_image, :class_name => 'Image'

  serialize :properties

  accepts_nested_attributes_for :descriptions
  accepts_nested_attributes_for :images

  validates :name, presence: true, uniqueness: {message: 'Este nome já esta sendo utilizado em outro produto.'}
  validates :price, numericality: {less_than: 1_000_000}

  before_create :ensure_description

  def display_image
    main_image || images.first
  end

  def increment_view_count=(condition)
    if condition
      self.increment(:view_count)
      self.save
    end
  end

  def category_name
    root_category.name if root_category
  end

  def root_category_id
    root_category.id if root_category
  end

  def root_category
    root = category
    return unless root

    while root.parent_name
      root = root.parent
    end

    root
  end

  def tag_list=(tags)
    new_tags = []
    tags.each do |tag|
      new_tags << Tag.find_or_create_by_name(tag) unless tag == '_'
    end
    self.tags = new_tags
  end

  def tag_list
    tags.map { |tag| tag.name }
  end

  def thumbs
    images.map { |image| {name: image.name, url: image.thumbs} }
  end

  def thumb
    display_image.image_file.to_s[9..-1] if display_image
  end

  def as_json(options = {})
    super({only: PUBLIC_FIELDS}.merge(options || {}))
  end

  def ensure_description
    self.descriptions.build locale: 'pt-BR', title: self.name if self.descriptions.blank?
  end

  def asset_ids=(ids)
    assets = []

    ids.each do |id|
      assets << AssetsProduct.find_or_create_by_asset_id(id)
    end

    self.assets_products = assets
  end

  def asset_ids
    self.assets_products.map {|a| a.asset_id }
  end

  def assets
    Game::Client.get_assets(self.asset_ids)['assets']
  end
end
