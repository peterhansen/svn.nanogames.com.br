class Tag < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :name, :slug]
  has_many :taggings
  has_many :products, through: :taggings

  validates :name, uniqueness: true, presence: true

  before_save :generate_slug

  def generate_slug
    slugged = self.name.dup
    slugged.gsub! /['`]/, ""
    slugged.gsub! /\s*@\s*/, " at "
    slugged.gsub! /\s*&\s*/, " and "
    slugged.gsub! /\s*[^A-Za-z0-9\.\-]\s*/, '_'
    slugged.gsub! /_+/, "_"
    slugged.gsub! /\A[_\.]+|[_\.]+\z/, ""
    self.slug = slugged.downcase!
  end

  def self.find_or_create_by_name(tag_name)
    Tag.where(name: tag_name).first || Tag.create(name: tag_name)
  end

  def as_json(options = {})
    super({only: PUBLIC_FIELDS}.merge(options || {}))
  end
end