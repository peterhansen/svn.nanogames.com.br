# encoding: utf-8

class Description < ActiveRecord::Base
  PRIVATE_FIELDS = [:descriptible_id, :descriptible_type, :created_at, :updated_at]
  PUBLIC_FIELDS = [:id, :locale, :title, :shorted, :complete]

  attr_accessible :descriptible_id, :descriptible_type, :locale, :title, :shorted, :complete

  belongs_to :descriptible, polymorphic: true

  validates_presence_of :locale, message: 'Linguagem não pode ficar em branco'
  validates_presence_of :title, message: 'Título não pode ficar em branco'
  validates_presence_of :shorted, message: 'Descrição reduzida não pode ficar em branco'
  validates_presence_of :complete, message: 'Descrição completa não pode ficar em branco'
  validates :shorted, length: {maximum: 300, message: 'Descrição reduzida pode ter no máximo 300 caracteres'}
  validates_uniqueness_of :locale, scope: [:descriptible_id, :descriptible_type], message: 'Linguagem já utilizada'

  def as_json options = {}
    super({only: PUBLIC_FIELDS})
  end
end