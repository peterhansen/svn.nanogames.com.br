class Image < ActiveRecord::Base
  PUBLIC_FIELDS = [:id]
  attr_accessible :product_id, :remote_image_file_url
  belongs_to :product

  attr_accessor :skip_thumbs

  mount_uploader :image_file, ImageUploader

  def thumb_path(size)
    if image_file.present?
      image_file.to_s.match /\/uploads\/(\w+).(\w+)/
      "#{image_file.root}/uploads/v#{size}_#{$1}.#{$2}"
    end
  end

  def thumb_url(size)
    if image_file.present?
      image_file.to_s.match /\/uploads\/(\w+).(\w+)/
      "/uploads/v#{size}_#{$1}.#{$2}"
    end
  end

  def thumbs
    if image_file.present?
      versions = image_file.versions.as_json
      versions.delete :url

      thumbs = {}
      versions.each do |key, value|
        thumbs[key.to_s[1..-1]] = value[:url] if File.exists? thumb_path(key.to_s[1..-1])
      end
      thumbs
    end
  end

  def name
    image_file.url.gsub('/uploads/', '') if image_file.present?
  end

  def skip_thumbs?
    self.skip_thumbs
  end

  def as_json(options = {})
    super({only: PUBLIC_FIELDS, methods: [:name, :thumbs]}.merge(options || {}))
  end
end
