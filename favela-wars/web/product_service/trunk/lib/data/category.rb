# encoding: utf-8

class Category < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :name, :slug, :position, :parent_id]

  has_many :products, dependent: :nullify
  belongs_to :parent, class_name: 'Category'
  has_many :children, class_name: 'Category', foreign_key: :parent_id, dependent: :nullify
  has_many :descriptions, as: :descriptible

  accepts_nested_attributes_for :descriptions, reject_if: proc { |attributes| attributes['title'].blank? }

  validates :name, presence: true, uniqueness: {message: 'Este nome já esta sendo utilizado em outra categoria.'}

  def parent_name
    parent.name if parent_id && parent
  end

  def roots
    where(parent_id: nil)
  end

  def descendants
    desc = []
    desc << children

    children.each do |category|
      desc << category.descendants
    end

    desc.flatten!
  end

  def as_json(options = {})
    super({only: PUBLIC_FIELDS}.merge(options || {}))
  end
end