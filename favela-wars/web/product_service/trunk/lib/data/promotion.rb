# encoding: utf-8

class Promotion< ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :name, :discount_mode, :discount_value, :start, :end]

  has_many :product

  validates :name, uniqueness: {message: 'Promoção já existe com este nome.'}
  validates :discount_value, numericality: {less_than: 1_000_000}

end