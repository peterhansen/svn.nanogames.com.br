# encoding: utf-8

class Highlight < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :link]

  mount_uploader :big_image, BigHighlightUploader
  mount_uploader :small_image, SmallHighlightUploader

  def thumbs
    thumbs = {}

    ['small', 'big'].each do |size|
      versions = send("#{size}_image").versions.as_json
      versions.delete :url
      thumbs[size] = {}

      versions.each do |key, value|
        thumbs[size][key.to_s[1..-1]] = value[:url] if File.exists? "#{send("#{size}_image").root}#{value[:url]}"
      end
    end

    thumbs
  end

  def thumb_path(size, thumb)
    image = send("#{size}_image")

    image.versions.each_pair do |key, value|
      if key.to_s[1..-1] == thumb
        return "#{image.root}#{value.url}"
      end
    end
  end

  def as_json(options = {})
    super(options.merge(only: PUBLIC_FIELDS))
  end

  def big_image_changed?; end
  def small_image_changed?; end
end
