class CreateImages < ActiveRecord::Migration
  def self.up
    create_table :images, force: true do |t|
      t.integer :product_id
      t.string  :file
      t.timestamps
    end

    add_index :images, :product_id
  end

  def self.down
    drop_table :images
  end
end