class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories, force: true do |t|
      t.string :name
      t.string :slug
      t.integer :position
      t.integer :parent_id
      t.timestamps
    end
    
    add_index :categories, :name
    add_index :categories, :slug
  end
  
  def self.down
    drop_table :categories
  end
end