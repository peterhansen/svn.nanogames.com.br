class AddViewCountToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :view_count, :integer
  end

  def self.down
    remove_column :products, :view_count
  end
end


