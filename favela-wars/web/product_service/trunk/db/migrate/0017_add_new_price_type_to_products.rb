class AddNewPriceTypeToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :price_black, :integer,  default: 0
  end

  def self.down
    remove_column :products, :price_black
  end
end