class PolymorphicDescription < ActiveRecord::Migration
  def self.up
    rename_column :descriptions, :product_id, :descriptible_id
    add_column    :descriptions, :descriptible_type, :string
  end

  def self.down
    remove_column :descriptions, :descriptible_type
    rename_column :descriptions, :descriptible_id, :product_id
  end
end
