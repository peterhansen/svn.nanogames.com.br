class CreateHighlights < ActiveRecord::Migration
  class Highlight < ActiveRecord::Base; end

  def self.up
    create_table :highlights, force: true do |t|
      t.string :link
      t.timestamps
    end

    3.times do
      Highlight.create
    end
  end

  def self.down
    drop_table :highlights
  end
end


