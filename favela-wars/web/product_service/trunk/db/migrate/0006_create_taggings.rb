class CreateTaggings < ActiveRecord::Migration
  def self.up
    create_table :taggings, force: true do |t|
      t.integer :product_id
      t.integer :tag_id
      t.timestamps
    end

    add_index :taggings, :product_id
    add_index :taggings, :tag_id
    add_index :taggings, [:tag_id, :product_id], unique: true
  end

  def self.down
    drop_table :taggings
  end
end