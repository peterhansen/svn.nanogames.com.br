class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products, force: true do |t|
      t.string :name
      t.integer :price, default: 0
      t.boolean :published, default: false
      t.timestamps
    end

    add_index :products, :name
  end

  def self.down
    drop_table :products
  end
end


