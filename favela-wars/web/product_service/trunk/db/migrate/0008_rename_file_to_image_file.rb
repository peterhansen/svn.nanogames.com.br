class RenameFileToImageFile < ActiveRecord::Migration
  def self.up
    rename_column :images, :file, :image_file
  end

  def self.down
    rename_column :images, :image_file, :file
  end
end