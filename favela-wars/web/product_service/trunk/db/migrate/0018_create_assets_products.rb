class CreateAssetsProducts < ActiveRecord::Migration
	def self.up
		create_table :assets_products, force: true do |t|
			t.integer :product_id
			t.integer :asset_id
			t.timestamps
		end	
	end	

	def self.down
		drop_table :assets_products
	end
end