class AddImageFieldsToHighlights < ActiveRecord::Migration
  def self.up
    add_column :highlights, :big_image, :string
    add_column :highlights, :small_image, :string
  end

  def self.down
    remove_column :highlights, :big_image
    remove_column :highlights, :small_image
  end
end