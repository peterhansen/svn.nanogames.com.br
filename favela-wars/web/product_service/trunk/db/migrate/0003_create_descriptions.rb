class CreateDescriptions < ActiveRecord::Migration
  def self.up
    create_table :descriptions, force: true do |t|
      t.string :locale
      t.string :title
      t.string :shorted
      t.text :complete
      t.integer :product_id
      t.timestamps
    end

    add_index :descriptions, :title
  end

  def self.down
    drop_table :descriptions
  end
end
