class AddPropertiesToProducts < ActiveRecord::Migration
  def self.up
    add_column :products, :properties, :string
  end

  def self.down
    remove_column :products, :properties
  end
end


