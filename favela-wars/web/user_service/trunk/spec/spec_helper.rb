ENV["SINATRA_ENV"] ||= "test"

require 'rspec'
require 'rack/test'
require 'active_record'
require 'database_cleaner'
require 'factory_girl'
require 'pry'
require 'pry-debugger'

require_relative '../lib/user_service'

Mail.defaults do
  delivery_method :test
end

DatabaseCleaner.strategy = :truncation

RSpec.configure do |config|
  config.include Rack::Test::Methods

  config.before(:each) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean
    Mail::TestMailer.deliveries.clear
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  FactoryGirl.find_definitions

  def app
    UserService::App.new
  end
end
