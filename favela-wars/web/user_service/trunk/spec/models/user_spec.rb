require_relative '../spec_helper'

describe User do
  it 'should require an username' do
    User.new(email: 'chucknorris@nanostudio.com.br', password: 'senha123').should_not be_valid
  end

  it 'should require an email' do
    User.new(login: 'chucknorris', password: 'senha123').should_not be_valid
  end

  it 'should require a password on create' do
    User.new(login: 'chucknorris', email: 'chucknorris@nanostudio.com.br').should_not be_valid
  end

  it 'should have an encrypted password after creation' do
    user = FactoryGirl.create :user
    user.password_salt.should_not be_nil
    user.password_hash.should_not be_nil
  end

  it 'should save the birthday' do
    User.new('birthday(1i)' => '1981', 'birthday(2i)' => '12', 'birthday(3i)' => '21').birthday.should == Date.parse('1981/12/21')
  end

  it 'should have a json representation' do
    JSON.parse User.new(login: 'chuck').to_json
  end

  it 'should accept nested addresses' do
    User.new.should respond_to('addresses_attributes=')
  end

  it 'should responde as_json with an id' do
    User.new.should respond_to('addresses_attributes=')
  end

  it 'should send welcome email after creation' do
    FactoryGirl.create :user
    Mail::TestMailer.deliveries.should_not be_empty
  end

  it 'should create address when creating the object, if address_attributes is present' do
    lambda do
      User.create(email: 'chucknorris@nanostudio.com.br',
                  password: 'senha123',
                  login: 'chucknorris',
                  addresses_attributes: [{
                                             street: 'Rua tal'
                                         }]
      )
    end.should change(Address, :count).by(1)
  end

  describe '#authenticate' do
    it 'should authenticate with correct password' do
      user = FactoryGirl.create :user
      user.authenticate('senha123').should be_true
    end
    it 'should not authenticate with incorrect password' do
      user = FactoryGirl.create :user
      user.authenticate('senhaerrada').should_not be_true
    end
  end

  describe '#new_password' do
    User.new_password.length.should == 9
  end

  describe '#reset_password' do
    it 'should create a new password' do
      User.should_receive(:new_password)
      FactoryGirl.create(:user).reset_password
    end

    it 'should not authenticate with the old password' do
      user = FactoryGirl.create(:user)
      user.reset_password
      user.authenticate('senha123').should be_false
    end

    it 'should authenticate with the new password' do
      user = FactoryGirl.create(:user)
      User.should_receive(:new_password).and_return 'senhanova123'
      user.reset_password
      user.authenticate('senhanova123').should be_true
    end

    it 'should send email' do
      FactoryGirl.create(:user).reset_password
      Mail::TestMailer.deliveries.should_not be_empty
    end
  end
end
