FactoryGirl.define do
  factory :user do
    sequence(:name) { |n| "User #{n}" }
    sequence(:login) { |n| "user_#{n}" }
    sequence(:email) { |n| "user_#{n}@morra.com" }
    birthday  Date.parse('1970-01-01')
    password  'senha123'
  end
end
