# encoding: utf-8

require_relative '../spec_helper'

describe UserMailer do
  describe '#welcome' do
    let(:email) { UserMailer.welcome(FactoryGirl.create(:user, email: 'chucknorris@nanostudio.com.br')) }

    it 'should be sent by no-reply@nanostudio.com.br' do
      email.from.should == ['no-reply@nanostudio.com.br']
    end

    it 'should be sent to chucknorris@nanostudio.com.br' do
      email.to.should == ['chucknorris@nanostudio.com.br']
    end

    it 'should be sent with subject "Welcome"' do
      email.subject.should == 'Welcome'
    end

    it 'should be sent with body "Welcome"' do
      email.body.should == 'Welcome'
    end
  end

  describe '#remember_password' do
    let(:email) { UserMailer.remember_password(FactoryGirl.create(:user, email: 'chucknorris@nanostudio.com.br'), 'novasenha123') }

    it 'should be sent by no-reply@nanostudio.com.br' do
      email.from.should == ['no-reply@nanostudio.com.br']
    end

    it 'should be sent to chucknorris@nanostudio.com.br' do
      email.to.should == ['chucknorris@nanostudio.com.br']
    end

    it 'should be sent with subject "Nova senha"' do
      email.subject.should == 'Nova senha'
    end

    it 'should be sent with body "Welcome"' do
      email.body.should == 'Sua nova senha é novasenha123'
    end
  end
end