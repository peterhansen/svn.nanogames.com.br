# encoding: utf-8
require_relative '../spec_helper'

describe "Comprando itens", type: :request do
  it "deve buscar o usuário" do
    User.should_receive(:find)
    post '/users/1234/purchases.json'
  end

  context "quando não existir um usuário válido" do
    it "deve retornar 404" do
      post '/users/1235/purchases.json'
      last_response.status.should == 404
    end
  end

  context "quando existir um usuário válido" do
    let(:user) { FactoryGirl.create(:user) }

    before(:each) do
      User.should_receive(:find).and_return(user)
    end

    context "e o produto não for válido" do
      it "deve retornar 502 se o Product Service não estiver disponivel" do
        post '/users/1234/purchases.json', {purchase: {product_id: 1234}}.to_json
        last_response.status.should == 502
      end
    end

    context "e o produto for válido" do
      let(:product) { stub(id: 1234, price: 10) }

      before(:each) do
        Products::Client.should_receive(:find).and_return(product)
        Game::Client.should_receive(:add_item)
      end

      it "deve fazer uma chamada ao ProductClient" do
        post '/users/1234/purchases.json', {purchase: {product_id: 1234}}.to_json
        last_response.status.should == 201
      end

      # it "deve adicionar o produto a lista de itens do usuário" do
      #   post '/users/1234/purchases.json', {purchase: {product_id: 1234}}.to_json
      # end

      it "deve adicionar o produto a lista de compras do usuário" do
        post '/users/1234/purchases.json', {purchase: {product_id: 1234}}.to_json
        user.purchases.map{|p| p.product_id}.should include(product.id)
      end
    end

    context "erro de comunicação com GameService" do
      let(:product) { stub(id: 1234, price: 10) }

      before(:each) do
        Products::Client.should_receive(:find).and_return(product)
      end


      it "não deve retornar 201" do
        post '/users/1234/purchases.json', {purchase: {product_id: 1234}}.to_json
        last_response.status.should == 502
      end
    end
  end
end


describe 'consultando compras' do
  let(:product) { stub(id: 1234, price: 10) }
  let(:user) { FactoryGirl.create(:user) }

  it "deve retornar a lista de compras" do
    User.should_receive(:find).twice.and_return(user)
    Products::Client.should_receive(:find).and_return(product)
    post '/users/1234/purchases.json', {purchase: {product_id: 1234}}.to_json
    get '/users/1234/purchases.json'
    last_response.body.should == "{\"purchases\":[{\"purchase\":{\"product_id\":1234,\"user_id\":1}}]}"
  end
end

describe "comprando vaga na tropa" do
  it "deve funcionar" do
    Products::Client.should_receive(:get_troop_place_items).and_return("{\"products\":[{\"product\":{\"ag_modifier\":0,\"at_modifier\":0,\"category_id\":1,\"df_modifier\":0,\"id\":1,\"in_modifier\":0,\"name\":\"Morra\",\"pr_modifier\":0,\"published\":false,\"tc_modifier\":0,\"asset_ids\":[],\"properties\":[]}}],\"total\":1}")
    Game::Client.should_receive(:add_item).and_return(true)

    user = FactoryGirl.create :user
    post "/users/#{user.id}/purchase_troop_place.json"
    last_response.body.should eq("{\"message\":\"Troop place purchased\"}")
  end
end