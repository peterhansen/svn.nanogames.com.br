namespace :deploy do
  namespace :unicorn do
    desc <<-DESC
    This task configure unicorn to run the application. It copies the unicorn_init script to the init.d folder.
    DESC
    task :config do
      if unicorn_script_exists?
        pp "Redeploying Unicorn init script."
        sudo "rm /etc/init.d/unicorn_#{application}"
      else
        pp "Deploying Unicorn init script"
      end
      copy_script
      create_pids_folder
      copy_unicorn_rb
    end

    desc 'Restart unicorn'
    task :restart do
      pp "Restarting Unicorn."
      sudo "service unicorn_#{application} restart"
    end
  end
end

def unicorn_script_exists?
  file_exists? "/etc/init.d/unicorn_#{application}"
end

def copy_script
  sudo "cp #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
  sudo "chmod +x /etc/init.d/unicorn_#{application}"
end

def create_pids_folder
  run "mkdir -p #{shared_path}/log/pids"
end

def copy_unicorn_rb
  run "mkdir -p #{shared_path}/config"
  run "cp -f #{current_path}/config/unicorn.rb #{shared_path}/config/unicorn.rb"
end
