# encoding: utf-8

Given "que '$name' tem um personagem chamado '$character_name' com o id '$character_id'" do |name, character_name, character_id|
  FactoryGirl.create(:character, id: character_id, name: character_name, user_id: User.find_by_login(name).id)
end

Given "que o personagem '$character' está equipado com uma '$weapon' de id '$id'" do |character, weapon, id|
  character = Character.find_by_name(character)
  user = character.user
  item = Item.find_by_product_id(id)
  item = FactoryGirl.create(:item, user_id: user.id, product_id: id) unless item

  if character
    character.items << item
    character.save
  end
end

Given "que o personagem '$char_name' tem as seguintes propriedades:" do |char_name, data|
  char = Character.find_by_name(char_name)
  char.update_attribute :properties, JSON.parse(data)
end
