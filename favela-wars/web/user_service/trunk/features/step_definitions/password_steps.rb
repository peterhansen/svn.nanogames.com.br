When "que eu acabei de resetar minha senha" do
  step "eu envio um POST para '/users/reset_password.json' com os dados:", '{"email": "chucknorris@nanostudio.com.br"}'
end

When "eu tento logar com a senha antiga" do
  step "eu envio um POST para '/users/auth' com os dados:", '{"credentials": {"login": "chucknorris", "password": "senha123" }}'
end
