# encoding: utf-8
# pt

Given "que o usuário '$user' existe com o id '$id'" do |user, id|
  step "que existe um usuário com o login '#{user}', email '#{user}@test.com' e id '#{id}'"
end

Given "que existe um usuário com o login '$n', email '$n' e id '$n'" do |login, email, id|
  FactoryGirl.create(:user, login: login, email: email, id: id)
end

When "o usuário '$user' deve ter sido deletado do banco" do |user|
  User.find_by_id_or_login(user).should be_nil
end

Given "que o usuário possui endereço" do
  User.first.addresses.create street: 'Street 1'
end

Given "que existe um usuário com login '$login' e senha '$password'" do |login, password|
  FactoryGirl.create :user
end

Given "que o usuário possui o item '$item_id'" do |item_id|
  item = Factory(:item)
  user = User.find_or_create_by_id(1234)
  item = user.items << item
  user.save
end

Given "que exista o usuário '$user_id' com '$amount' granas na carteira '$wallet_money'" do |user_id, amount, wallet_money|
  if wallet_money.downcase == "black"
    FactoryGirl.create(:user, id: user_id, wallet: 0, wallet_black: amount)
  else
    FactoryGirl.create(:user, id: user_id, wallet: amount, wallet_black: 0)
  end
end
