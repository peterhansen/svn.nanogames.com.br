Given "que '$user_name' comprou o produto de id '$product_id'" do |user_name, product_id|
  user = User.find_by_login(user_name)
  user.items.build product_id: product_id
  user.save
end

Given "que exista um produto com '$product_id' valendo '$amount' granas '$wallet_money'" do |product_id, amount, wallet_money|

  product             = Product.new
  product.id          = product_id
  product.category_id = 1
  product.name        = "Training X"

  if wallet_money.downcase == "black"
    product.price_black = amount
  else
    product.price       = amount
  end

  Products::Client.should_receive(:find).and_return(product)
end

Given "que a compra seja realizada com sucesso" do
  Game::Client.should_receive(:add_item)
end
