# encoding: utf-8

Given /^que existam os pacotes de cŕedito:$/ do |credits|
  credits.hashes.each do |attributes|
    FactoryGirl.create :credit_pack, attributes
  end
end

Given /^que existam os pedidos:$/ do |orders|
  orders.hashes.each do |attributes|
    FactoryGirl.create :order, attributes
  end
end