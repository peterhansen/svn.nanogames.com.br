# encoding: utf-8

When "um email deve ter sido enviado para 'chucknorris@nanostudio.com.br'" do
  Mail::TestMailer.deliveries.should_not be_blank
end

When "um nenhum email deve ser enviado" do
  Mail::TestMailer.deliveries.should be_blank
end