# encoding: utf-8


When "eu envio um POST para '$path' com os dados:" do |path, params|
  post path, params
end

When "eu envio um POST para '$path' com $artigo $field '$value'" do |path, artigo, field, value|
  post path, {user: user_attributes.merge(field => value) }.to_json
end

When "eu envio um PUT para '$path' com os dados:" do |path, params|
  put path, params
end

When "eu envio um PUT para 'api.v1.nanostudio.com/users/debit' com os dados:" do |params|
  put '/users/debit.json', JSON.parse(params).to_json
end

#When "eu envio um PUT para 'api.v1.nanostudio.com.br/users/$n.json' com os dados:" do |path, params|
  #put '/users/' + path + '.json', JSON.parse(params).to_json
#end

When "eu envio um GET para '/$path/$id'" do |path, id|
  get path + '/' + id
end

When "eu envio um GET para '/users'" do
  get '/users'
end

When "eu envio um DELETE para '$host/$path/$id'" do |host, path, id|
  delete path + '/' + id
end

Then "eu devo receber o JSON:" do |json|
  JSON.parse(last_response.body).should == JSON.parse(json)
end

When "eu devo receber o status code '$code'" do |code|
  last_response.status.should == code.to_i
end

When "eu envio um HEAD para '$path'" do |path|
  head path
end
