ENV["SINATRA_ENV"] = "test"

require 'rack/test'
require_relative '../../lib/user_service'
require 'cucumber/rspec/doubles'

Mail.defaults do
  delivery_method :test
end

DatabaseCleaner.strategy = :truncation

Dir.glob(File.join(File.dirname(__FILE__), '../../spec/factories.rb')).each {|f| require f }

Before do
  ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'users_test')
  DatabaseCleaner.clean
  Mail::TestMailer.deliveries.clear
end

module AppHelper
  def app
    UserService::App
  end
end

World(Rack::Test::Methods, AppHelper)