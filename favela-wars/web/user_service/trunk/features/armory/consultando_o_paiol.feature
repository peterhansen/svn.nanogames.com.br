# language: pt

#Funcionalidade: Consultando o Paiol
  #Contexto:
    #Dado que o usuário 'Fulano' existe com o id '1'
    #E que 'Fulano' comprou uma 'Pistola' de id '2'
    #E que 'Fulano' comprou uma 'Bazooka' de id '3'

  #Cenário: Consultando o paiol de um usuário que possui itens
    #Quando eu envio um GET para '/users/1/armory.json'
    #Então eu devo receber o JSON:
    #"""
    #{"items": [
      #{"item": {
        #"id": 1
      #}},
      #{"item":{
        #"id": 2
      #}}
    #]}
    #"""

  #Cenário: Consultando o paiol de um usuário que tem itens atribuidos a personagens
    #Dado que 'fulano' tem um personagem chamado 'Zé' com o id '4'
    #E que o personagem 'Zé' está equipado com uma 'Bazooka' de id '3'
    #Quando eu envio um GET para '/users/1/armory.json'
    #Então eu devo receber o JSON:
    #"""
    #{"items": [
      #{"item": {
        #"id": 1
        #}
      #}]
    #}
    #"""
