@sprint02 @done
Feature: Criando usuários
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Scenario: Criando um usuário
    When eu envio um POST para '/users.json' com os dados:
    """
    {"user": {
      "name": "Chuck Norris",
      "login": "chucknorris",
      "email": "endereco@de.email",
      "birthday": "1970-01-01",
      "password": "senha123"
    }}
    """

    Then eu devo receber o JSON:
    """
    {"user": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'

  Scenario: Criando um usuário com endereço
    When eu envio um POST para '/users' com os dados:
    """
    {"user": {
      "name": "Chuck Norris",
      "login": "chucknorris",
      "email": "endereco@de.email",
      "birthday": "1970-01-01",
      "password": "senha123",
      "addresses_attributes": [{
        "street": "Street 1",
        "complement": "101",
        "city": "Madagascar",
        "postal_code": "00000",
        "country_code": "br"
      }]
    }}
    """
    Then eu devo receber o JSON:
    """
    {"user": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'

  Scenario: Tentando criar um usuário com campos inválidos
    When eu envio um POST para '/users' com os dados:
    """
    {"user": {
      "attributo_inválido": "Chuck Norris",
      "login": "chucknorris",
      "email": "endereco@de.email",
      "birthday": "1970-01-01",
      "password": "senha123"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": "unknown attribute: attributo_inválido"}
    """
    And eu devo receber o status code '400'

  Scenario: Tentando criar um usuário sem login
    When eu envio um POST para '/users' com os dados:
    """
      {"user": {
        "name": "Chuck Norris",
        "email": "endereco@de.email",
        "birthday": "1970-01-01",
        "password": "senha123"
      }}
      """
    Then eu devo receber o JSON:
    """
      { "errors": { "login": [ "can't be blank", "Wrong login format." ] } }
    """
    And eu devo receber o status code '409'

  Scenario Outline: Tentando criar um usuário com um email inválido
    Given que existe um usuário com o login 'bla', email 'chucknorris@nanostudio.com.br' e id '1'
    When eu envio um POST para '/users' com o email '<email>'
    Then eu devo receber o JSON:
    """
    { "errors": { "email": [ "<mensagem>" ] } }
    """
    And eu devo receber o status code '409'
  Examples:
    | email                         | mensagem               |
    | chucknorris@nanostudio.com.br | has already been taken |
    | chucknorris                   | is invalid             |
    | chucknorris@nanostudio        | is invalid             |
    | ''                            | is invalid             |

  Scenario: Tentando criar um usuário sem password
    When eu envio um POST para '/users' com os dados:
    """
      {"user": {
        "name": "Chuck Norris",
        "login": "chucknorris",
        "email": "endereco@de.email",
        "birthday": "1970-01-01"
      }}
      """
    Then eu devo receber o JSON:
    """
      { "errors": { "password": [ "Password size must be between 6 and 15 character. It must contain letters AND numbers." ] } }
    """
    And eu devo receber o status code '409'

  Scenario Outline: Tentando criar um usuário com uma senha inválida
    When eu envio um POST para '/users' com a password '<password>'
    Then eu devo receber o JSON:
    """
    { "errors": { "password": [ "Password size must be between 6 and 15 character. It must contain letters AND numbers." ] } }
    """
    And eu devo receber o status code '409'
  Examples:
    | password |
    | abef1    |
#      | asdd#_aa  |

  Scenario Outline: Tentando criar um usuário inválido
    When eu envio um POST para '/users' com o login '<login>'
    Then eu devo receber o JSON:
    """
    { "errors": { "login": [ "Wrong login format." ] } }
    """
    And eu devo receber o status code '409'
  Examples:
    |login|
    | Eduardo Xavier  |
    | Mestre Splinter |

  Scenario: Tentando criar um usuário com um login já utilizado
    Given que existe um usuário com o login 'chucknorris', email 'bla@ble.com' e id '1'
    When eu envio um POST para '/users' com o login 'chucknorris'
    Then eu devo receber o JSON:
    """
    { "errors": { "login": [ "has already been taken" ] } }
    """
