@sprint02 @done
Feature: Lembrar senha
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Scenario: Lembrando uma senha
    Given que existe um usuário com o login 'bla', email 'chucknorris@nanostudio.com.br' e id '1'
    When eu envio um POST para '/users/reset_password.json' com os dados:
    """
    {"email": "chucknorris@nanostudio.com.br"}
    """
    And eu devo receber o status code '200'
    And um email deve ter sido enviado para 'chucknorris@nanostudio.com.br'

  Scenario: Lembrando uma senha
    When eu envio um POST para '/users/reset_password.json' com os dados:
    """
    {"email": "chucknorris@nanostudio.com.br"}
    """
    And eu devo receber o status code '404'
    And um nenhum email deve ser enviado

  Scenario: Tentando acessar com senha antiga
    Given que existe um usuário com o login 'bla', email 'chucknorris@nanostudio.com.br' e id '1'
    And que eu acabei de resetar minha senha
    When eu tento logar com a senha antiga
    Then eu devo receber o status code '409'
