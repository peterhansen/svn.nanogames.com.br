# encoding: utf-8

@sprint02 @done
Feature: Consultando usuários
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Background:
    Given que existe um usuário com o login 'chucknorris', email 'chucknorris@nanostudio.com.br' e id '1234'


  Scenario: Atualizando dados de um usuário pelo id
    When eu envio um PUT para '/users/1234.json' com os dados:
    """
    {"user": {
    "login": "chucknorris",
    "email": "outroemail@gmail.com",
    "password":"life123"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"user": {
    "id": 1234
    }}
    """
    And eu devo receber o status code '200'

  Scenario: Atualizando dados de um usuário pelo login
    When eu envio um PUT para '/users/1234.json' com os dados:
    """
    {"user": {
    "email": "outroemail@gmail.com"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"user": {
    "id": 1234
    }}
    """
    And eu devo receber o status code '200'

  Scenario: Tentando atualizar dados de um usuário inexistente
    When eu envio um PUT para '/users/4321.json' com os dados:
    """
    {"user": {
    "email": "lindomar@zubzero.com.br"
    }}
    """
    Then eu devo receber o status code '404'

  Scenario: Tentando atualizando dados de um usuário com dados inválidos
    When eu envio um PUT para '/users/1234.json' com os dados:
    """
    {"user": {
    "email": "wrong"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": {"email": ["is invalid"]}}
    """
    And eu devo receber o status code '409'
