@sprint02
Feature: Consultando a lista de usuários
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Scenario: Consultando a lista de usuários
    Given que existe um usuário com o login 'chucknorris', email 'chucknorris@nanostudio.com.br' e id '1234'
    And que existe um usuário com o login 'lindomar', email 'lindomar@nanostudio.com.br' e id '4567'
    When eu envio um GET para '/users'
    Then eu devo receber o JSON:
    """
    {"users": [
      {"user": {
        "id": 1234,
        "name": "Chuck Norris",
        "login": "chucknorris",
        "email": "chucknorris@nanostudio.com.br",
        "birthday": "1970-01-01",
        "nickname": null,
        "wallet": null}},
      {"user": {
        "id": 4567,
        "name": "Chuck Norris",
        "login": "lindomar",
        "email": "lindomar@nanostudio.com.br",
        "birthday": "1970-01-01",
        "nickname": null,
        "wallet": null}}
    ],
    "total": 0}
    """
    And eu devo receber o status code '200'
