@sprint02 @done
Feature: Consultando usuários
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Background:
    Given que existe um usuário com o login 'chucknorris', email 'chucknorris@nanostudio.com.br' e id '1234'

  Scenario: Consultando um usuário válido pelo id
    When eu envio um GET para '/users/1234'
    Then eu devo receber o JSON:
    """
    {"user": {
      "id": 1234,
      "name": "Chuck Norris",
      "login": "chucknorris",
      "email": "chucknorris@nanostudio.com.br",
      "birthday": "1970-01-01",
      "nickname": null,
      "wallet": null
    }}
    """
    And eu devo receber o status code '200'

  #Scenario: Consultando usuário e endereço pelo id
    #Given que o usuário possui endereço
    #When eu envio um GET para '/users/1234?fields=addresses'
    #Then eu devo receber o JSON:
    #"""
    #{"user": {
      #"id": 1234,
      #"name": "Chuck Norris",
      #"login": "chucknorris",
      #"email": "chucknorris@nanostudio.com.br",
      #"addresses": [{
        #"city": null,
        #"complement": null,
        #"country_code": null,
        #"id": 1,
        #"postal_code": null,
        #"street": "Street 1"
      #}],
      #"birthday": "1970-01-01",
      #"nickname": null,
      #"wallet": null
    #}}
    #"""
    #And eu devo receber o status code '200'

  Scenario: Consultando um usuário inválido
    When eu envio um GET para '/users/lindomar'
    Then eu devo receber o status code '404'
