@sprint02 @done
Feature: Consultando usuários
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Background:
    Given que existe um usuário com o login 'chucknorris', email 'chucknorris@nanostudio.com.br' e id '1234'

  Scenario: Deletando um usuário pelo id
    When eu envio um DELETE para 'api.v1.nanostudio.com.br/users/1234'
    Then eu devo receber o JSON:
    """
    {"success": "User disabled"}
    """
    And eu devo receber o status code '200'

  Scenario: Tentando deletar um usuário que não existe
    When eu envio um DELETE para 'api.v1.nanostudio.com.br/users/4321'
    Then eu devo receber o status code '404'
