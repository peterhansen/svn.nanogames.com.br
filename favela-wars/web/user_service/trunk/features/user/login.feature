@sprint02 @done
Feature: Criando usuários
  O serviço 'UserService' será o hub central de funcionalidades de usuário, e servirá a todos as aplicações do projeto 'Favela Wars'.
  Ele funcionará como um web service REST e será utilizado atraves de uma API web.

  Scenario: Logando
    Given que existe um usuário com login 'chucknorris' e senha 'senha123'
    When eu envio um POST para '/users/auth.json' com os dados:
    """
    {"credentials": {
      "login": "chucknorris",
      "password": "senha123"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"user":
      {"birthday":"1970-01-01",
       "email":"chucknorris@nanostudio.com.br",
       "id":1,
       "login":"chucknorris",
       "name":"Chuck Norris",
       "nickname": null,
       "favorite_products": [],
       "wallet": null}}
    """
    And eu devo receber o status code '200'

  Scenario: Tenatando logar com o login errado
    Given que existe um usuário com login 'chucknorris' e senha 'senha123'
    When eu envio um POST para '/users/auth' com os dados:
    """
    {"credentials": {
      "login": "joselito",
      "password": "senha123"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": "Wrong login or password"}
    """
    And eu devo receber o status code '409'
