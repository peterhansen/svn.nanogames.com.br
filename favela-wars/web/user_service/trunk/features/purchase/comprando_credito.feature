# language: pt

@sprint07
Funcionalidade: Como um usuário gostaria de iniciar a compra de crédito

  Cenário: Iniciando um pedido de crédito
    Dado que o usuário 'chucknorris' existe com o id '1234'
    E que existam os pacotes de cŕedito:
      | id    | price | value | locale  |
      | 1     | 12    | 15000 | 'pt-BR' |
      | 2     | 15    | 45000 | 'pt-BR' |
    Quando eu envio um POST para '/users/1234/orders/create.json' com os dados:
    """
    {"order": {
      "pack_credit_id":"1",
      "payment_method":"pague_seguro"
    }}
    """
    Então eu devo receber o status code '201'

  Cenário: Atualizando o retorno de pagamento do integrador
    Dado que o usuário 'chucknorris' existe com o id '1234'
    E que existam os pacotes de cŕedito:
      | id    | price | value | locale  |
      | 3     | 11    | 25000 | 'pt-BR' |
    E que existam os pedidos:
      | id    | user_id | credit_pack_id | payment_method  | gross_amount | net_amount  | status | purpose |
      | 3     | 1234    | 3              | 'pagseguro'     | 35.90        | 00.00       | 0      |  0   |
    Quando eu envio um POST para '/users/orders/update.json' com os dados:
    """
    {"order": {
      "id":"3",
      "status":"3",
      "net_amount":"35.90"
    }}
    """
    Então eu devo receber o status code '201'
