# language: pt

@sprint07
Funcionalidade: Como um usuário gostaria de visualizar os pacotes de cŕedito disponíveis para compra

  Cenário: Requisitando informações de cŕedito
    Dado que existe um usuário com o login 'eduardoxavier', email 'eduardo.xavier@nanostudio.com' e id '1'
    E que existam os pacotes de cŕedito:
      | id    | price | value | locale  |
      | 1     | 12    | 15000 | 'pt-BR' |
      | 2     | 15    | 45000 | 'pt-BR' |
    Quando eu envio um GET para '/users/pt-br/active-packs.json'
    Então eu devo receber o status code '200'