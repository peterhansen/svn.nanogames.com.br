# language: pt

@sprint07
Funcionalidade: Como um usuário gostaria de comprar produtos na loja


  Cenário: Comprando um produto com grana preta
    Dado que exista o usuário '1' com '1000' granas na carteira 'black'
    E que exista um produto com '1' valendo '10' granas 'black'
    E que a compra seja realizada com sucesso
    Quando eu envio um POST para '/users/1/purchases.json' com os dados:
    """
    {"purchase":
      {
        "product_id": "1",
        "money": "black"
      }
    }
    """
    Então eu devo receber o status code '201'

  Cenário: Comprando um produto com grana normal
    Dado que exista o usuário '1' com '1000' granas na carteira 'normal'
    E que exista um produto com '1' valendo '10' granas 'normal'
    E que a compra seja realizada com sucesso
    Quando eu envio um POST para '/users/1/purchases.json' com os dados:
    """
    {"purchase":
      {
        "product_id": "1",
        "money": "normal"
      }
    }
    """
    Então eu devo receber o status code '201'


  Cenário: Comprando um produto com grana normal, porém sem fundos
    Dado que exista o usuário '1' com '50' granas na carteira 'normal'
    E que exista um produto com '1' valendo '51' granas 'normal'
    Quando eu envio um POST para '/users/1/purchases.json' com os dados:
    """
    {"purchase":
      {
        "product_id": "1",
        "money": "normal"
      }
    }
    """
    Então eu devo receber o status code '505'

  Cenário: Comprando um produto com grana normal, porém sem fundos
    Dado que exista o usuário '1' com '50' granas na carteira 'black'
    E que exista um produto com '1' valendo '51' granas 'black'
    Quando eu envio um POST para '/users/1/purchases.json' com os dados:
    """
    {"purchase":
      {
        "product_id": "1",
        "money": "normal"
      }
    }
    """
    Então eu devo receber o status code '505'