class Purchase < ActiveRecord::Base
  PUBLIC_FIELDS = [:product_id, :user_id]

  belongs_to :user

  def as_json(options = {})
    options = {} unless options
    super(options.merge(only: PUBLIC_FIELDS))
  end
end
