class Favorite < ActiveRecord::Base
  belongs_to :user

  validates_uniqueness_of :product_id, scope: :user_id
end