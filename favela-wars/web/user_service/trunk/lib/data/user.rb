# encoding: utf-8

class User < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :login, :email, :birthday, :name, :nickname]
  attr_accessor :password, :password_confirmation

  before_save :encrypt_password
  after_create :send_welcome_mail

  has_many :addresses
  has_many :favorites
  # has_many :items
  has_many :purchases
  # has_many :characters

  accepts_nested_attributes_for :addresses
  accepts_nested_attributes_for :favorites, allow_destroy: true

  validates :login,
            presence: true,
            format: {with: /^[A-Za-z0-9_-]+\Z/,
                     message: "Wrong login format."},
            on: :create
  validates_uniqueness_of :login

  validates :email,
            uniqueness: true,
            format: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/

  validates :password,
            format: {with: /(?=.*[a-z]|[A-Z])(?!.*s).{6,15}/,
                     message: "Password size must be between 6 and 15 character. It must contain letters AND numbers."},
            on: :create

  def as_json(options = {})
    options = {} unless options
    super({only: PUBLIC_FIELDS}.merge(options))
  end

  def favorite_products
    favorites.map {|fav| fav.product_id}
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def self.find_by_id_or_login(id)
    where('id = ? or login = ?', id, id).first
  end

  def authenticate(password)
    password_hash == BCrypt::Engine.hash_secret(password, password_salt)
  end

  def reset_password
    password = self.class.new_password
    update_attribute :password, password
    send_remember_password(password)
  end

  def self.new_password
    pass = ""
    8.times do
      pass << %w{a b c d e f g h i j k l m n o p q r s t u v x z A B C D E F G H I J K L M N O P Q R S T U V X Z}.sample
    end
    pass << %w{1 2 3 4 5 6 7 8 9}.sample
  end

  def send_welcome_mail
    UserMailer.welcome(self).deliver
  end

  def send_remember_password(password)
    UserMailer.remember_password(self, password).deliver
  end
end
