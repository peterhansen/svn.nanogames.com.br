class Address < ActiveRecord::Base
  def as_json(options = {})
    super({except: [:created_at, :updated_at, :user_id]}.merge(options))
  end
end