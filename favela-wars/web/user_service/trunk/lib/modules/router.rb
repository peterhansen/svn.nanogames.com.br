module UserService
  class Router
    def initialize(app)
      @app = app
    end

    def call(env)
      env['PATH_INFO'].match(/^\/(\w+)[\/|\.]?.*/)
      return [200, {"Content-Type" => "text/plain"}, ['<?xml version="1.0"?> <cross-domain-policy> <allow-access-from domain="*"/> </cross-domain-policy>']] if $1 == 'crossdomain'
      return [200, {"Content-Type" => "text/plain"}, ['']] if ['favicon', '__sinatra__'].include? $1
      "#{$1.camelize}Service".constantize.new.call(env)
    rescue NameError
      return [404, {"Content-Type" => "text/plain"}, ['']]  
    end
  end
end
