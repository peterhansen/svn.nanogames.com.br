module UserService
  class ConnectionManager
    def initialize(app)
      @app = app
    end

    def call(env)
      ActiveRecord::Base.establish_connection(UserService.db_config)
      status, header, body = @app.call(env)
      [status, header, body]
    end
  end
end