require_relative 'boot'

module UserService
  class App < Sinatra::Base
    set :environment, :development
    use ConnectionManager
    use CrossOrigin
    use Router
  end
end
