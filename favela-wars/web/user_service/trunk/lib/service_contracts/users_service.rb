class UsersService < ServiceContractBase
  post %r{/users/auth.?[\w+]?} do
    begin
      parameters = extract_parameters_from_body
      UserResource.authenticate_user(parameters['credentials']['login'], parameters['credentials']['password'])
    rescue UserResource::AuthenticationFailedException => e
      [e.class::HTTP_STATUS_CODE, { errors: e.details }.to_json]
    end
  end

  post %r{/users/reset_password.?[\w+]?} do
    begin
      parameters = extract_parameters_from_body
      UserResource.reset_password(parameters['email'])
    rescue UserResource::ResourceNotFoundException => e
      [e.class::HTTP_STATUS_CODE, { errors: e.details }.to_json]
    end
  end

  get '/users/:id/purchases.json' do |id|
    begin
      user = User.find(id)
      purchases = user.purchases
      [200, ({purchases: purchases.as_json}).to_json]
    rescue ActiveRecord::RecordNotFound => e
      [404, {errors: e.message}.to_json]
    end
  end

  post '/users/:id/purchase_troop_place.json' do |id|
    begin
      content_type 'application/json'
      user = User.find(id)

      items = Products::Client.get_troop_place_items
      troop_place = JSON.parse(items)['products'].first['product']

      # @todo Fazer requisição para LiveGamer
      # response = LiveGamer::Client.purchase(id, item.id)

      user.purchases.create user_id: user.id, product_id: troop_place['id']
      Game::Client.add_item(user.id, troop_place['id'])
      [200, {message: 'Troop place purchased'}.to_json]
    rescue Exception => e
      binding.pry
    end
  end

  post '/users/:id/purchases.json' do |id|
    begin
      parameters = extract_parameters_from_body
      user       = User.find(id)
      product    = Products::Client.find(parameters["purchase"]["product_id"])

      User.transaction do
        user.purchases.create(product_id: product.id)
        Game::Client.add_item(user.id, product.id)
        user.save
      end

      [201, { status:'ok'}.to_json]

    rescue Errno::ECONNREFUSED => e
      [502, { errors: e.message }.to_json]
    rescue ActiveRecord::RecordNotFound => e
      [404, {errors: e.message}.to_json]
    rescue Exception => e
      [505, {errors: e.message}.to_json]
    end
  end

  error do
    binding.pry
    [500, {error: env['sinata.error']}.to_json]
  end
end
