require_relative 'models/user'
require_relative 'models/favorite'
require_relative 'models/address'
require_relative 'mailers/user_mailer'

module UserCore
  # @param attributes [Hash]
  # @return [String]
  def self.create_user(attributes)
    user = User.create(attributes)

    raise ValidationException.new(user.errors.messages) unless user.valid?

    return user.to_json(only: :id)

  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_user(id, fields)
    user = User.find_by_id_or_login(id)

    if user
      user.to_json(include: extract_options(fields))
    else
      raise UserNotFoundException.new('User not found')
    end
  end

  def self.get_all(page = nil, page_size = nil)
    {users: User.order('created_at desc').select(User::PUBLIC_FIELDS).paginate(:page => page, :per_page => page_size)}.to_json
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_user(id, attributes)
    user = User.find_by_id_or_login(id)

    if user
      user.update_attributes!(attributes)
      return user.reload.to_json only: :id
    else
      raise UserNotFoundException.new('User not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(user.errors.messages)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_user(id)
    user = User.find_by_id_or_login(id)

    if user
      user.destroy
      { success: 'User deleted' }.to_json
    else
      raise UserNotFoundException.new('User not found')
    end
  end

  # @param login [String]
  # @param password [String]
  # @return [String]
  def self.authenticate_user(login, password)
    user = User.where(login: login).first

    if user && user.authenticate(password)
      logger.info "SUCCESS - user: #{login}(#{user.id})"
      user.to_json
    else
      logger.info "ERROR - user: #{login}"
      raise AuthenticationFailedException.new('Wrong login or password')
    end
  end

  # @param email [String]
  # @return [String]
  def self.reset_password(email)
    user = User.where(email: email).first

    if user.present?
      user.reset_password
      { }.to_json
    else
      raise UserNotFoundException.new('User not found')
    end
  end

  def self.count
    {product_count: User.count}.to_json
  end

  class BasicException < StandardError
    attr_accessor :details
    HTTP_STATUS_CODE = 400

    def initialize details
      @details = details
    end
  end

  class ValidationException < BasicException
    HTTP_STATUS_CODE = 409
  end
  class InvalidRequestException < BasicException
    HTTP_STATUS_CODE = 400
  end
  class UserNotFoundException < BasicException
    HTTP_STATUS_CODE = 404
  end
  class AuthenticationFailedException < BasicException
    HTTP_STATUS_CODE = 409
  end

  private

  def self.extract_options(fields)
    fields.split(',').map { |a| a.to_sym } if fields
  end
end
