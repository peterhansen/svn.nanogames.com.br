# encoding: utf-8

class UserMailer
  Mail.defaults do
    delivery_method :smtp, {
        from:                 'no-reply@nanostudio.com.br',
        address:              'mail.nanogames.com.br',
        domain:               'nanogames.com.br',
        user_name:            'online@nanogames.com.br',
        password:             '$NaN0$',
        authentication:       'login',
        enable_starttls_auto: false
    }
  end

  def self.welcome(user)
    Mail.new do
      from 'no-reply@nanostudio.com.br'
      to user.email
      subject 'Welcome'
      body "Welcome"
    end
  end

  def self.remember_password(user, password)
    Mail.new do
      from 'no-reply@nanostudio.com.br'
      to user.email
      subject 'Nova senha'
      body "Sua nova senha é #{password}"
    end
  end
end