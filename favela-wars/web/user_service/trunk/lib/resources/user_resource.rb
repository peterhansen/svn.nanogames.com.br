# encoding: utf-8
require_relative '../data/user'
require_relative '../data/address'
require_relative '../data/favorite'
require_relative '../data/purchase'

class UserResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    user = User.find(id)
    user.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('User not found')
  end

  # @return [String]
  def self.get_all(page = nil, page_size = nil)
    User.order('created_at desc').select(User::PUBLIC_FIELDS)
    #{users: User.order('created_at desc').select(User::PUBLIC_FIELDS).paginate(:page => page, :per_page => page_size)}.to_json
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    user = User.create(attributes)
    raise ValidationException.new(user.errors.messages) unless user.valid?
    return user.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    user = User.find_by_id(id)

    if user
      user.update_attributes!(attributes)
      return user.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('User not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(user.errors.messages)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    user = User.find(id)
    user.destroy
    {success: 'User disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('User not found')
  end

  def self.count
    {user_count: User.count}.to_json
  end

  # @param login [String]
  # @param password [String]
  # @return [String]
  def self.authenticate_user(login, password)
    user = User.where(login: login).first

    if user && user.authenticate(password)
      user.to_json
    else
      raise AuthenticationFailedException.new('Wrong login or password')
    end
  end

  # @param email [String]
  # @return [String]
  def self.reset_password(email)
    user = User.where(email: email).first

    if user.present?
      user.reset_password
      {}.to_json
    else
      raise ResourceNotFoundException.new('User not found')
    end
  end
end
