class CreateOrders < ActiveRecord::Migration
  def self.up
    create_table :orders, force: true do |t|
      t.integer :user_id
      t.integer :credit_pack_id
      t.string  :payment_method, :limit => 30, :default => '', :null => false
      t.decimal :gross_amount, :net_amount, :precision => 14, :scale => 2, :default => 0.00
      t.integer :status, :default => 0
      t.integer :purpose, :default => 0
      t.timestamps
    end

    add_index :orders, [:user_id, :credit_pack_id]
  end

  def self.down
    drop_table :orders
  end
end
