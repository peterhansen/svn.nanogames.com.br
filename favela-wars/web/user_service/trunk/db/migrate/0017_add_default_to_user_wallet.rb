class AddDefaultToUserWallet < ActiveRecord::Migration
  def self.up
    change_column :users, :wallet, :integer, :default => 0
  end

  def self.down
    
  end
end
