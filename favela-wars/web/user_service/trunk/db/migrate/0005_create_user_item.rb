class CreateUserItem < ActiveRecord::Migration
  def self.up
    create_table :users_items, force: true do |t|
      t.integer :userId
      t.integer :productId
      t.integer :credit
      t.string  :type
      t.timestamps
    end

    add_index :users_items, :userId
  end

  def self.down
    drop_database :users_items
  end
end