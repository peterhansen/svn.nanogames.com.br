class CreateCreditPack < ActiveRecord::Migration
  def self.up
    create_table :credit_packs, force: true do |t|
      t.decimal :price,  :precision => 14, :scale => 2
      t.integer :value,  :null => false
      t.string  :locale, :limit => 5,      :default => 'pt-BR', :null => false
      t.boolean :active, :default => true
      t.timestamps
    end
  end

  def self.down
    drop_table :credit_packs
  end
end