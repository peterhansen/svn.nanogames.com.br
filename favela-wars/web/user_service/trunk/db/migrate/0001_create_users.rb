class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users, force: true do |t|
      t.string :login
      t.string :name
      t.string :email
      t.date   :birthday
      t.string :password_hash
      t.string :password_salt
      t.timestamps
    end

    add_index :users, :login
    add_index :users, :email
    add_index :users, :name
  end

  def self.down
    drop_table :users
  end
end
