class RemoveCharactersAndItems < ActiveRecord::Migration
  def self.up
    drop_table :characters_items
    drop_table :characters
    drop_table :items
  end

  def self.down

  end
end
