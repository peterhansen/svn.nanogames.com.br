class CreatePurchases < ActiveRecord::Migration
  def self.up
    create_table :purchases, force: true do |t|
      t.integer :user_id
      t.integer :product_id
      t.timestamps
    end

    add_index :purchases, [:user_id, :product_id]
  end

  def self.down
    #drop_table :purchases
  end
end
