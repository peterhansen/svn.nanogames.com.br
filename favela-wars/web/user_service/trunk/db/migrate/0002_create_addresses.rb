class CreateAddresses < ActiveRecord::Migration
  def self.up
    create_table :addresses, force: true do |t|
      t.string :street
      t.string :complement
      t.string :city
      t.string :postal_code
      t.string :country_code
      t.integer :user_id
      t.timestamps
    end

    add_index :addresses, :city
    add_index :addresses, :country_code
  end

  def self.down
    drop_table :addresses
  end
end
