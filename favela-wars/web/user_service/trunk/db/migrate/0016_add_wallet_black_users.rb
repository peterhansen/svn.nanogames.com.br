class AddWalletBlackUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :wallet_black, :integer, :default => 0
  end

  def self.down
    remove_column :users, :wallet_black
  end
end
