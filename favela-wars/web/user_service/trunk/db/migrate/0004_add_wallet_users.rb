class AddWalletUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :wallet, :integer
  end

  def self.down
    remove_column :users, :wallet
  end
end
