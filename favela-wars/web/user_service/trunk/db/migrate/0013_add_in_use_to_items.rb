class AddInUseToItems < ActiveRecord::Migration
  def self.up
    add_column :items, :in_use, :boolean, default: false
  end

  def self.down
    remove_column :items, :in_use
  end
end
