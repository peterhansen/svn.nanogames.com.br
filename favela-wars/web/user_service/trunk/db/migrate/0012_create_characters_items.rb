class CreateCharactersItems < ActiveRecord::Migration
  def self.up
    create_table :characters_items, force: true do |t|
      t.integer :item_id
      t.integer :character_id
      t.timestamps
    end
  end

  def self.down
    drop_table :characters_items
  end
end
