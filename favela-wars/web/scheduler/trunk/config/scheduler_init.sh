#!/bin/bash 
### BEGIN INIT INFO
#
# Provides :        scheduler
# Required-Start :  $syslog
# Required-Stop  :  $syslog
# Should-Start:	    $local_fs
# Should-Stop:      $local_fs
# Default-Start     : 2 3 4 5
# Default-Stop      : 0 1 6
# Short-Description : Scheduler
# Description : see Short-Description
#
### END INIT INFO

PROG="./scheduler_service"
NICE_NAME="scheduler"
USER="ubuntu"
APP_PATH="/home/$USER/scheduler"
PROG_PATH="$APP_PATH/current/"
PID_FILE="$PROG_PATH/scheduler.rb.pid"

start() {
  if [ -e "$PID_FILE" ]; then
    echo "Error! Scheduler is currently running, or the pid file in $PID_FILE is stale!" 1>&2
    exit 1
  else
    su $USER -c "cd $PROG_PATH && APP_ENV=production $PROG start 2>&1 >/dev/null &" &
    echo "Scheduler"
  fi
}

stop() {
  if [ -e "$PID_FILE" ]; then
    su $USER -c "cd $PROG_PATH && $PROG stop 2>&1 >/dev/null &" &
    echo " Scheduler stopped."
  else
    echo "Error! Scheduler is not running!" 1>&2
    exit 1
  fi
}

if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root." 1>&2
  exit 1
fi

case "$1" in
  start)
    start
    exit 0
    ;;
  stop)
    stop
    exit 0
    ;;
  reload|restart|force-reload)
    stop
    start
    exit 0
    ;;
  **)
    echo "Yo, brah. Usage: $0 {start|stop|reload}" 1>&2
    exit 1
    ;;
esac
