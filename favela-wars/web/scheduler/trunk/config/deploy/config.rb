namespace :deploy do
  namespace :scheduler do
    desc <<-DESC
    This task configure the scheduler. It copies the scheduler_init script to the init.d folder.
    DESC
    task :config do
      if scheduler_script_exists?
        pp "Redeploying Scheduler init script."
        sudo "rm /etc/init.d/scheduler"
      else
        pp "Deploying Scheduler init script"
      end
      copy_script
      create_pids_folder
    end

    desc 'Restart scheduler'
    task :restart do
      pp "Restarting scheduler."
      sudo "service scheduler restart"
    end
  end
end

def scheduler_script_exists?
  file_exists? "/etc/init.d/scheduler"
end

def copy_script
  sudo "cp #{current_path}/config/scheduler_init.sh /etc/init.d/scheduler"
  sudo "chmod +x #{current_path}/scheduler.rb"
  sudo "chmod +x /etc/init.d/scheduler"
end

def create_pids_folder
  run "mkdir -p #{release_path}/tmp"
end