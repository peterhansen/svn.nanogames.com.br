def symlink_exists? path
  'true' == capture("if [ -L #{path} ]; then echo 'true'; fi").strip
end

def file_exists? path
  'true' == capture("if [ -e #{path} ]; then echo 'true'; fi").strip
end

def directory_exists? path
  'true' == capture("if [ -d #{path} ]; then echo 'true'; fi").strip
end

def pp string
  puts "\n\n=== #{string} ===\n\n"
end
