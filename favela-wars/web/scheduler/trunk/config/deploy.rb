ENV['APP_ENV'] = 'production'
ENV['DATABASE_ENV'] = 'production'

require 'bundler/capistrano'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/deploy.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/db.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/nginx.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/helpers.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/config.rb'

set :env, ENV['APP_ENV']
set :user, 'ubuntu'
set :domain, 'scheduler.staging.nanostudio.com.br'
set :application, 'scheduler'
set :scm, :subversion
set :deploy_via, :export
set :repository, "http://svn.nanogames.com.br/favela-wars/web/#{application}/trunk/"
set :scm_username, 'nano_online_svn'
set :scm_password, 'nAnO_SvN123'
set :deploy_to, "/home/#{user}/#{application}"
set :current, "#{deploy_to}/current"
set :keep_releases, 5
set :use_sudo, false

server "#{domain}", :app, :web, :db, :primary => true

ssh_options[:keys] = [File.join(ENV['HOME'], '.ssh', 'amazon-sa.pem')]
