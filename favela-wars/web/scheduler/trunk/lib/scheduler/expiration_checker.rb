class ExpirationChecker
  include EM::Deferrable

  def initialize
    expired_tasks = Task.where('expires_on < ?', Time.now).limit(TRAINING_LIMIT)

    if expired_tasks.any?
      self.succeed(expired_tasks)
    else
      self.fail
    end
  end
end