# encoding: utf-8

class Notifier
  def self.notify(notification_type, exception)
    Log.info "Sending email notification."

    # Defer this shit
    mail = Mail.new do
      from    'webdev@nanostudio.com.br'
      to      'webdev@nanostudio.com.br'
      subject '[ERROR] Scheduler'
      body    "Ocorreu um erro no serviço Scheduler.\n\nError:\n\n#{exception.message}"
    end

    mail.delivery_method :sendmail
    mail.deliver
  rescue Exception => e
    Log.info '[ERROR] Erron on exception notification'
    true
  end
end
