class SchedulerEntryPoint < EM::Connection
  include EM::HttpServer

  def process_http_request
    response = EM::DelegatedHttpResponse.new(self)
    response.content_type 'application/json'

    content = JSON.parse @http_post_content
    p "Received #{content['task']['type']} task."
    
    ActiveRecord::Base.connection_pool.with_connection do
      # task = Task.new content['task']
      Task.create! content['task']
    end

    response.status = 200
    response.content = '{"success":"ok"}'
    response.send_response
  rescue JSON::ParserError
    response.status = 500
    response.content = '{"error": "bad format" }'
    response.send_response
  rescue Exception => e
    response.status = 500
    response.content = '{"error": "' + e.message + '" }'
    response.send_response
  end
end
