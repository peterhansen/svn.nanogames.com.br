ENV['APP_ENV'] ||= 'development'
ENV['BUNDLE_GEMFILE'] = "#{File.expand_path(File.dirname(__FILE__))}/../Gemfile"

require 'logger'
require 'bundler/setup'
Bundler.require(:default, ENV['APP_ENV'])

require_relative '../lib/scheduler/entry_point'
require_relative '../lib/scheduler/expiration_checker'
require_relative '../lib/scheduler/notifier'
require_relative '../lib/data/task'

db_config = YAML::load_file(File.expand_path(File.dirname(__FILE__)) + '/../config/database.yml')[ENV['APP_ENV']]
ActiveRecord::Base.establish_connection(db_config)

config = YAML::load_file(File.expand_path(File.dirname(__FILE__)) + '/../config/config.yml')

RESOLVER_ADDRESS = "http://trainings.staging.nanostudio.com.br/trainings/scheduler_callback.json"

NORMAL_INTERVAL = config['normal_interval']
QUIRK_MODE_INTERVAL = config['quirk_mode_interval']
TRAINING_LIMIT = config['training_limit_per_query']

Log = Logger.new(STDOUT)