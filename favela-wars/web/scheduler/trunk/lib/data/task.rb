class Task < ActiveRecord::Base
  set_inheritance_column "not_used"

  serialize :body

  validates_presence_of :type, :expires_on
end
