#!/usr/bin/env ruby
require_relative 'lib/boot'

EventMachine.run do
  Log.info 'Starting main loop'
  
  EM.run { EM.start_server 'localhost', 8080, SchedulerEntryPoint }

  timer = EM.add_periodic_timer(NORMAL_INTERVAL) do
    checker = ExpirationChecker.new

    checker.callback do |expired_tasks|
      Log.info "Sending #{expired_tasks.size} expired task to #{RESOLVER_ADDRESS}"

      begin
        response = HTTParty.post(RESOLVER_ADDRESS, body: {training_ids: expired_tasks.map{|t| t.external_id}}.to_json)

        expired_tasks.delete_all

        unless timer.interval == NORMAL_INTERVAL
          Log.info "Exiting Quirk Mode"
          timer.interval = NORMAL_INTERVAL
        end 
      rescue Exception => e
        Log.info "[ERROR] Error while sending expired tasks: #{e.class}."

        unless timer.interval == QUIRK_MODE_INTERVAL
          Notifier.notify(:exception, e)
          Log.info 'Entering Quirk Mode'
        end
        timer.interval = QUIRK_MODE_INTERVAL
      end
    end

    checker.errback do
    end
  end
end
