class CreateTasks < ActiveRecord::Migration
  def self.up
    create_table :tasks, force: true do |t|
      t.string :type
      t.string :body
      t.datetime :expires_on
      t.timestamp
    end
  end

  def self.down
    drop_table :tasks
  end
end
