class AddExternalIdToTasks < ActiveRecord::Migration
  def self.up
    add_column :tasks, :external_id, :integer
  end

  def self.down
    remove_column :tasks, :external_id
  end
end