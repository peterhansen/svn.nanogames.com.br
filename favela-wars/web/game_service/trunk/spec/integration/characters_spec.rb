# encoding: utf-8
require_relative '../spec_helper'

set :show_exceptions, false

describe "Consultando os exemplos" do
  include Rack::Test::Methods

  def app
    GameService::App.new
  end

  let(:character) { FactoryGirl.create(:character, name: 'Ze Pequeno') }

  it "deve retornar o personagens" do
    CharacterResource.should_receive(:get_all).and_return([character])

    get '/characters.json'

    ActiveRecord::Base.establish_connection(
      adapter: 'mysql2',
      database: 'characters_test'
    ) # Gambi

    response = "{\"characters\":[{\"character\":{\"id\":1,\"name\":\"Ze Pequeno\"}}],\"total\":1}"
    last_response.body.should == response
  end

  it "deve retornar os personagens de um jogador" do
    Factory(:character, name: 'Ze Pequeno', user_id: 1)
    Factory(:character, name: 'Mane Galinha', user_id: 2)

    get '/characters.json?user_id=1'
    last_response.body.should == "{\"characters\":[{\"character\":{\"id\":1,\"name\":\"Ze Pequeno\"}}],\"total\":1}"
  end

  it "deve retornar os itens dos personagens" do
    character = Factory(:character, name: 'Bla')
    item = Factory(:item, product_id: 1, user_id: 1)

    character.items << item
    character.save

    Products::Client.should_receive(:find_list).and_return({"products"=>[{"product"=>{"category_id"=>nil, "id"=>1, "name"=>"AR-15", "price"=>100, "properties"=>nil, "published"=>false}}, {"product"=>{"category_id"=>nil, "id"=>1, "name"=>"Revorve", "price"=>34, "properties"=>nil, "published"=>false}}], "total"=>2})
    get '/characters.json?include=items'
    last_response.body.should == "{\"characters\":[{\"character\":{\"id\":1,\"name\":\"Bla\",\"items\":[{\"item\":{\"id\":1}}]}}],\"total\":1}"
  end

  it "deve permitir que um item que o jogador possua seja atribuido a um personagem" do
    character = Factory(:character, name: 'Bla', user_id: 1)
    item = Factory(:item, product_id: 1, user_id: 1)
    item = Factory(:item, product_id: 1, user_id: 2)

    put '/characters/1.json', {character: {item_ids: [1]}}.to_json
    get '/characters/1.json?include=items'
    last_response.body.should == "{\"character\":{\"id\":1,\"name\":\"Bla\",\"items\":[{\"item\":{\"id\":1}}]}}"
  end

  it "não deve permitir que um item que o jogador não possua seja atribuido a um personagem" do
    character = Factory(:character, name: 'Bla', user_id: 1)
    item = Factory(:item, product_id: 1, user_id: 1)
    item = Factory(:item, product_id: 1, user_id: 2)

    put '/characters/1.json', {character: {item_ids: [2]}}.to_json
    get '/characters/1.json?include=items'
    last_response.body.should == "{\"character\":{\"id\":1,\"name\":\"Bla\",\"items\":[]}}"
  end

  it "batch update" do
    character = Factory(:character, name: 'Bla', user_id: 1)
    chars = {characters: [{character: {id: 1, name: 'Bla', properties: {bla: 'ble'}}}]}

    put '/characters/batch.json', chars.to_json
    get '/characters/1.json?include=properties'

    response = "{\"character\":{\"id\":1,\"name\":\"Bla\",\"properties\":{\"bla\":\"ble\"}}}"
    last_response.body.should == response
  end
end
