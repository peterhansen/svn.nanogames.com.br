class CreateItems < ActiveRecord::Migration
  def self.up
    create_table :items, force: true do |t|
      t.integer :user_id
      t.integer :product_id
      t.timestamps
    end

    add_index :items, [:user_id, :product_id]
  end

  def self.down
    drop_table :items
  end
end
