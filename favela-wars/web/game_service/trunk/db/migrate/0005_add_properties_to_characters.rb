class AddPropertiesToCharacters < ActiveRecord::Migration
  def self.up
    add_column :characters, :properties, :string
  end

  def self.down
    remove_column :characters, :properties
  end
end
