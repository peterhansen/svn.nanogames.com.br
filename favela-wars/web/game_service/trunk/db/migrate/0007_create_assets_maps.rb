class CreateAssetsMaps < ActiveRecord::Migration
  def self.up
    create_table :assets_maps, id: false, force: true do |t|
      t.integer :asset_id
      t.integer :map_id
    end
  end
end
