class CreateCharacters < ActiveRecord::Migration
  def self.up
    create_table :characters, force: true do |t|
      t.integer :user_id
      t.string :name
      t.timestamps
    end

    add_index :characters, :user_id
  end

  def self.down
    drop_table :characters
  end
end
