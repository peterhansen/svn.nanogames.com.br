class CreateChecksums < ActiveRecord::Migration
  def self.up
    create_table :checksums, force: true do |t|
      t.string :value
      t.integer :map_id
      t.integer :platform_id
      t.timestamps
    end
  end

  def self.down
    drop_table :checksums
  end
end
