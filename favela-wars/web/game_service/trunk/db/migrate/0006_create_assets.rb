class CreateAssets < ActiveRecord::Migration
  def self.up
    create_table :assets, force: true do |t|
      t.string :name
      t.string :checksum
      t.integer :version, default: 1
      t.string :type
      t.timestamps
    end

    add_index :assets, :name
  end

  def self.down
    drop_table :assets
  end
end
