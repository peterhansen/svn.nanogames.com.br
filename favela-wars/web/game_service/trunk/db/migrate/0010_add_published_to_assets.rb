class AddPublishedToAssets < ActiveRecord::Migration
  def self.up
    add_column :assets, :published, :boolean, default: false
  end

  def self.down
    remove_column :assets, :published
  end
end
