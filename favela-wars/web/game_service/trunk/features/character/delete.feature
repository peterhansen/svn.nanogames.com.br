Feature: Deletando um personagem
  Background:
    Given que o personagem 'exemplo1' existe com o id '1234' e user_id '1'

  Scenario: Deletando um personagem pelo id
    When eu envio um DELETE para '/characters/1234'
    Then eu devo receber o JSON:
    """
    {"success": "Character disabled"}
    """
    And eu devo receber o status code '200'

  Scenario: Tentando deletar um personagem que não existe
    When eu envio um DELETE para '/characters/4321'
    Then eu devo receber o status code '404'
