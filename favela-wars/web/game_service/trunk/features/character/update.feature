Feature: Atualizando personagens
  Background:
    Given que o personagem 'personagem1' existe com o id '1234' e user_id '1'

  @wip
  Scenario: Atualizando dados de um personagem pelo id
    When eu envio um PUT para '/characters/1234.json?bla=ble' com os dados:
    """
    {"character": {
        "name": "outro_nome"
      },
      "method": "PUT"
    }
    """
    Then eu devo receber o JSON:
    """
    {"character": {
      "id": 1234
    }}
    """
    And eu devo receber o status code '200'

  Scenario: Tentando atualizar dados de um personagem inexistente
    When eu envio um PUT para '/characters/4321.json' com os dados:
    """
    {"character": {
      "name": "outro_nome"
    }}
    """
    Then eu devo receber o status code '404'
