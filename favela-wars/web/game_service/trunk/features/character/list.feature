Feature: Consultando a lista de personagens

  Scenario: Consultando a lista de personagens
    Given que o personagem 'personagem1' existe com o id '1234' e user_id '1'
    And que o personagem 'personagem2' existe com o id '4567' e user_id '1'
    When eu envio um GET para '/characters.json'
    Then eu devo receber o JSON:
    """
    {"characters": [
      {"character": {
        "id": 1234,
        "name": "personagem1"}},
      {"character": {
        "id": 4567,
        "name": "personagem2"}}
    ],
    "total": 2}
    """
    And eu devo receber o status code '200'
