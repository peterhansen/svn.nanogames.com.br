Feature: Criando personagens
  Scenario: Criando um personagem
    When eu envio um POST para '/characters.json' com os dados:
    """
    {"character": {
      "name": "personagem1"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"character": {
      "id": 1
    }}
    """
    And eu devo receber o status code '201'

  Scenario: Tentando criar um personagem com campos inválidos
    When eu envio um POST para '/characters.json' com os dados:
    """
    {"character": {
      "attributo_inválido": "ble"
    }}
    """
    Then eu devo receber o JSON:
    """
    {"errors": "unknown attribute: attributo_inválido"}
    """
    And eu devo receber o status code '400'
