Feature: Consultando um personagem
  Background:
    Given que o personagem 'personagem1' existe com o id '1234' e user_id '1'

  Scenario: Consultando um personagem válido pelo id
    When eu envio um GET para '/characters/1234'
    Then eu devo receber o JSON:
    """
    {"character": {
      "id": 1234,
      "name": "personagem1"
    }}
    """
    And eu devo receber o status code '200'

  Scenario: Consultando um personagem inválido
    When eu envio um GET para '/characters/9999'
    Then eu devo receber o status code '404'
