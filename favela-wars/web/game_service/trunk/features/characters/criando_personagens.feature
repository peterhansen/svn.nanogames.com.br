# language: pt

Funcionalidade: Criando personagens
  Cenário: Criando um personagem
    Quando eu envio um POST para '/characters.json' com os dados:
    """
    {"character": {
      "name": "Maluco",
      "user_id": 1
    }}
    """
    Então eu devo receber o JSON:
    """
    {"character": {
      "id": 1
    }}
    """

  Cenário: Consultando um personagem após criação
    Quando eu envio um POST para '/characters.json' com os dados:
    """
    {"character": {
      "name": "Maluco",
      "user_id": 1
    }}
    """
    E eu envio um GET para '/characters.json?user_id=1'
    Então eu devo receber o JSON:
    """
    { "characters": [
        {"character": {
          "id":1,
          "name":"Maluco"
        }}
      ],
      "total":1
    }
    """

  Cenário: Criando um personagem com atributos
    Quando eu envio um POST para '/characters.json' com os dados:
    """
    {"character": {
      "name": "Maluco",
      "user_id": 1,
      "properties": {
        "qualquer": "coisa"
      }
    }}
    """
    Então eu devo receber o JSON:
    """
    {"character": {
      "id": 1
    }}
    """
