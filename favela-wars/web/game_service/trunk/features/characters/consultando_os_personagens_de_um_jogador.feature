# language: pt

Funcionalidade: Consultando os personagens de um jogador
  Contexto:
    Dado que o personagem 'personagem1' existe com o id '1234' e user_id '1'
    E que o personagem 'personagem2' existe com o id '4567' e user_id '1'

  Cenário: Consultandos os personagens do jogador
    Quando eu envio um GET para '/characters.json?user_id=1'
    Então eu devo receber o JSON:
    """
    {"characters":[
      { "character": {
        "id": 1234,
        "name": "personagem1"
        }},
      { "character": {
        "id": 4567,
        "name": "personagem2"
        }}
      ],
      "total": 2
    }
    """

  Cenário: Consultando personagens e a lista de item_ids
    Dado que o personagem '1234' está equipado com o item '1'
    Dado que o personagem '4567' está equipado com o item '2'
    Quando eu envio um GET para '/characters.json?user_id=1&include=item_ids'
    Então eu devo receber o JSON:
    """
    {"characters":[
      { "character": {
          "id": 1234,
          "name": "personagem1",
          "item_ids": [1]}
        },
        { "character": {
          "id": 4567,
          "name": "personagem2",
          "item_ids": [2]}
        }
      ],
      "total": 2
    }
    """

  Cenário: Consultando personagens e seus atributos
    Dado que o personagem '1234' tem as seguintes propriedades:
    """
    {"qualquer": "coisa"}
    """
    Quando eu envio um GET para '/characters.json?user_id=1&include=properties'
    Então eu devo receber o JSON:
    """
    { "characters": [{
        "character": {
          "id": 1234,
          "name": "personagem1",
          "properties": {"qualquer": "coisa"}
        }
      },
      { "character": {
          "id": 4567,
          "name": "personagem2",
          "properties": null
        }
      }],
      "total": 2
    }
    """
