# language: pt

Funcionalidade: Atualizando um personagem
  Contexto:
    Dado que o personagem 'Zé' existe com o id '2' e user_id '1'

  Cenário: Atualizando o personagem
    Quando eu envio um PUT para '/characters/2.json' com os dados:
    """
      {"character": {
        "name": "Joaquim"
      }}
    """
    Então eu devo receber o JSON:
    """
      {"character": {
        "id": 2
      }}
    """

  Cenário: Tentando atualizar um personagem que não existe
    Quando eu envio um PUT para '/characters/999.json' com os dados:
    """
      {"character": {
        "name": "Joaquim"
      }}
    """
    Então eu devo receber o JSON:
    """
    {"errors": "Couldn't find Character with id=999"}
    """

  Cenário: Equipando um personagem
    Dado que o item '1' existe com o user_id '1'
    Quando eu envio um PUT para '/characters/2.json' com os dados:
    """
      {"character": {
        "item_ids": [1]
      }}
    """
    E eu envio um GET para '/characters/2.json?include=items'
    Então eu devo receber o JSON:
    """
    {
      "character": {
        "id": 2,
        "name": "Zé",
        "items":[
          {
            "item":{
              "id":1
            }
          }
        ]
      }
    }
    """

  Cenário: Tentando equipando um personagem com um item que o usuário não possui
    Dado que o item '1' existe com o user_id '2'
    Quando eu envio um PUT para '/characters/2.json' com os dados:
    """
    {
      "character": {
        "item_ids": [1]
      }
    }
    """
    E eu envio um GET para '/characters/2.json?include=items'
    Então eu devo receber o JSON:
    """
    {
      "character": {
        "id": 2,
        "name": "Zé",
        "items":[]
      }
    }
    """
