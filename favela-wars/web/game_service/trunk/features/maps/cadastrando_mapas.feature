# language: pt

Funcionalidade: Cadastrando Mapas
  Cenário: Cadastrando um mapa
    Quando eu envio um POST para '/maps.json' com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksums": [
          {"android": "1234"},
          {"iphone": "6548"},
          {"web": "4567"}
        ]
      }
    }
    """
    Então eu devo receber o JSON:
    """
    {"map": {"name": "lapa_1", "version": 1, "asset_list": [], "published": false}}
    """
    E eu devo receber o status code '201'

  Cenário: Cadastrando um mapa com lista de assets
    Dado que existem os seguintes assets:
      | name      | checksum |
      | textura_1 | 1234     |
      | modelo_2  | 4567     |
    Quando eu envio um POST para '/maps.json' com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksums": [
          {"android": "1234"},
          {"iphone": "6548"},
          {"web": "4567"}
        ],
        "asset_list": [
          "textura_1",
          "modelo_2"
        ]
      }
    }
    """
    Então eu devo receber o JSON:
    """
    { "map": {
        "name": "lapa_1",
        "asset_list": [
          {"asset": {"name": "textura_1", "version": 1}},
          {"asset": {"name": "modelo_2", "version": 1}}
        ],
        "published": false,
        "version": 1
      }
    }
    """
    E eu devo receber o status code '201'
