# language: pt

Funcionalidade: Publicando um Mapa
  Cenário: Publicando um mapa
    Dado que existem os seguintes assets:
      | name      | checksum |
      | textura_1 | 1234     |
      | modelo_2  | 4567     |
    E que existe um mapa com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksums": [
          {"android": "1234"},
          {"iphone": "6548"},
          {"web": "4567"}
        ],
        "asset_list": [
          "textura_1",
          "modelo_2"
        ]
      }
    }
    """
    Quando eu envio um PUT para '/maps/lapa_1.json' com os dados:
    """
    { "map": {
        "published": "true"
      }
    }
    """
    E eu devo receber o JSON:
    """
    {"map": {"id": 3, "published": true}}
    """
    E eu devo receber o status code '200'
