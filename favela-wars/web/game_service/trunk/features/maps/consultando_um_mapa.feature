# language: pt

Funcionalidade: Consultando um Mapa
  Cenário: Consultando um mapa
    Dado que existe um mapa com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksum": "1234"
      }
    }
    """
    Quando eu envio um GET para '/maps/lapa_1.json'
    Então eu devo receber o JSON:
    """
    { "map": {
        "name": "lapa_1",
        "asset_list": [],
        "published": false,
        "version": 1
      }
    }
    """
    E eu devo receber o status code '200'

  Cenário: Consultando um mapa com assets
    Dado que existem os seguintes assets:
      | name      | checksum |
      | textura_1 | 1234     |
      | modelo_2  | 4567     |
    E que existe um mapa com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksum": "1234",
        "asset_list": [
          "textura_1",
          "modelo_2"
        ]
      }
    }
    """
    Quando eu envio um GET para '/maps/lapa_1.json'
    Então eu devo receber o JSON:
    """
    { "map": {
        "name": "lapa_1",
        "asset_list": [
          {"asset": {"name": "textura_1", "version": 1}},
          {"asset": {"name": "modelo_2", "version": 1}}
        ],
        "published": false,
        "version": 1
      }
    }
    """
    E eu devo receber o status code '200'
