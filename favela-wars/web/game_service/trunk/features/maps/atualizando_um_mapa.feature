# language: pt

Funcionalidade: Atualizando um Mapa
  Cenário: Atualizando um mapa deve aumentar o número da versão
    Dado que existem os seguintes assets:
      | name      | checksum |
      | textura_1 | 1234     |
      | modelo_2  | 4567     |
    E que existe um mapa com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksums": [
          {"android": "1234"},
          {"iphone": "6548"},
          {"web": "4567"}
        ],
        "asset_list": [
          "textura_1",
          "modelo_2"
        ]
      }
    }
    """
    Quando eu envio um POST para '/maps.json' com os dados:
    """
    { "map": {
        "name": "lapa_1",
        "checksums": [
          {"android": "1234"},
          {"iphone": "6548"},
          {"web": "4567"}
        ]
      }
    }
    """
    E eu envio um GET para '/maps/lapa_1.json'
    Então eu devo receber o JSON:
    """
    { "map": {
        "name": "lapa_1",
        "asset_list": [
          {"asset": {"name": "textura_1", "version": 1}},
          {"asset": {"name": "modelo_2", "version": 1}}
        ],
        "published": false,
        "version": 2
      }
    }
    """
    E eu devo receber o status code '200'
