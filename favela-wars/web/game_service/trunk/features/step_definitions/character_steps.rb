# encoding: utf-8

Given "que o personagem '$name' existe com o id '$id' e user_id '$user_id'" do |name, id, user_id|
  FactoryGirl.create(:character, name: name, id: id, user_id: user_id)
end

Given "que o personagem '$character_id' está equipado com o item '$item_id'" do |character_id, item_id|
  item = Item.find_by_id(item_id)
  item = Factory(:item, id: item_id, user_id: 1, product_id: 1) unless item
  character = Character.find_or_create_by_id(character_id)
  character.items << item
  character.save
end

Given "que o personagem '$char_id' tem as seguintes propriedades:" do |char_id, data|
  char = Character.find(char_id)
  char.properties = JSON.parse(data)
  char.save
end

Given "que o personagem '$character_id' esteja no treinamento '$training_id' com status '$status_text'" do |character_id, training_id, status_text|

  case status_text
    when "on_training"
      status_code = ::Training::RUNNING
    when "pause"
      status_code = ::Training::PAUSED
    when "stop"
      status_code = ::Training::STOPPED
    else
      status_code = 0
  end

  FactoryGirl.create(:training, id: training_id,
                                character_id: character_id,
                                duration:1800,
                                duration_on_pause:600,
                                product_id: 10,
                                status: status_code,
                                expires_at: (Time.now + 20 * 60),
                                user_id: 1)
end

Given "que exista um treinamento '$exist' duracao cadastrada" do |exist|
  product             = Product.new
  product.id          = 1
  product.category_id = 1
  product.name        = "Training X"
  product.price       = 1.50

  product.properties[:life] = 100
  product.properties[:hp] = 10
  product.properties[:hp_on_running] = 100

  if exist == 'com'
    product.properties[:duration] = 120
  end

  Products::Client.should_receive(:find).and_return(product)
end
