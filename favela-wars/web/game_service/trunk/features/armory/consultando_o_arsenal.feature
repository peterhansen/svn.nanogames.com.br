# language: pt

Funcionalidade: Consultando o Arsenal
  Cenário: Consultando o Arsenal de um usuário que possui itens
    Dado que o item '1' existe com o user_id '1'
    Dado que o item '2' existe com o user_id '1'
    Quando eu envio um GET para '/items.json?user_id=1'
    Então eu devo receber o JSON:
    """
    {
      "items": [
        {
          "item": {
            "id": 1
          }
        },
        {
          "item":{
            "id": 2
          }
        }
      ],
      "total": 2
    }
    """

  Cenário: Consultando o Arsenal de um usuário que tem itens atribuidos a personagens
    Dado que o item '1' existe com o user_id '1'
    Dado que o item '2' existe com o user_id '1'
    E que o personagem '1' está equipado com o item '1'
    Quando eu envio um GET para '/items.json?user_id=1'
    Então eu devo receber o JSON:
    """
    {
      "items": [
        {
          "item": {
            "id": 2
          }
        }
      ],
      "total": 1
    }
    """
