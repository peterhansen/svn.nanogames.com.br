# language: pt

Funcionalidade: Adicionando uma arma ao arsenal
  Cenário: Adicionando uma arma
    Quando eu envio um POST para '/items.json' com os dados:
    """
    { "item": {
        "product_id": 1,
        "user_id": 1
      }
    }
    """
    Então eu devo receber o JSON:
    """
    { "item": {
        "id": 1
      }
    }
    """
