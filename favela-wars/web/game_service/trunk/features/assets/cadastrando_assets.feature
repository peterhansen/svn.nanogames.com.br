# language: pt

Funcionalidade: Cadastrando Assets
  Cenário: Cadastrando um Asset
    Quando eu envio um POST para '/assets.json' com os dados:
    """
    {"asset": {
      "name": "textura_1",
      "checksum": 1234
    }}
    """
    Então eu devo receber o JSON:
    """
    {"asset": {
      "id": 1
    }}
    """
    E eu devo receber o status code '201'

  Cenário: Cadastrando vários Asset
    Quando eu envio um POST para '/assets/batch.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_1",
            "checksum": 1234
          }
        },
        {
          "asset": {
            "name": "modelo_1",
            "checksum": 4567
          }
        }
      ]
    }
    """
    Então eu devo receber o JSON:
    """
    {"success":"Assets cadastrados com sucesso"}
    """
    E eu devo receber o status code '201'

  Cenário: Tentando cadastrando vários Asset com erro
    Quando eu envio um POST para '/assets/batch.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_1",
            "checksum": 1234
          }
        },
        {
          "asset": {
            "name": "textura_1",
            "checksum": 4567
          }
        }
      ]
    }
    """
    Então eu devo receber o JSON:
    """
    {"errors": {"name": ["has already been taken"]}}
    """
    E eu devo receber o status code '409'
