# language: pt

Funcionalidade: Atualizando Assets
  Contexto:
    Dado que existem os seguintes assets:
      | name      | checksum |
      | textura_1 | 1234     |
      | modelo_2  | 4567     |

  Cenário: Atualizando um asset
    Quando eu envio um PUT para '/assets/batch.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_1",
            "checksum": "4321"
          }
        }
      ]
    }
    """
    Então eu devo receber o JSON:
    """
    {"success": "Assets atualizados com sucesso"}
    """
    E eu devo receber o status code '200'

  Cenário: Tentando atualizando um asset não cadastrado deve criar um asset novo
    Quando eu envio um PUT para '/assets/batch.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_2",
            "checksum": "9513"
          }
        }
      ]
    }
    """
    Então eu devo receber o JSON:
    """
    {"success": "Assets atualizados com sucesso"}
    """
    E eu devo receber o status code '200'

  Cenário: Atualizar um asset deve mudar a sua versão
    Quando eu envio um PUT para '/assets/batch.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_1",
            "checksum": "9513"
          }
        }
      ]
    }
    """
    E eu envio um GET para '/assets.json'
    Então eu devo receber o JSON:
    """
    { "assets": [
        {"asset": {"name": "textura_1", "version": 2}},
        {"asset": {"name": "modelo_2", "version": 1}}
      ],
      "total": 2
    }
    """
