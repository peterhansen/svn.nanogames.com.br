# language: pt

Funcionalidade: Checando Assets
  Contexto:
    Dado que existem os seguintes assets:
      | name      | checksum |
      | textura_1 | 1234     |
      | modelo_2  | 4567     |

  Cenário: Checando Asset que está na versão atual
    Quando eu envio um POST para '/assets/check.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_1",
            "checksum": "1234"
          }
        }
      ]
    }
    """
    Então eu devo receber o JSON:
    """
    {"assets": []}
    """
    E eu devo receber o status code '200'

  Cenário: Checando Asset que não está na versão atual
    Quando eu envio um POST para '/assets/check.json' com os dados:
    """
    { "assets": [
        {
          "asset": {
            "name": "textura_1",
            "checksum": "9876"
          }
        },
        {
          "asset": {
            "name": "modelo_2",
            "checksum": "4567"
          }
        }
      ]
    }
    """
    Então eu devo receber o JSON:
    """
    {"assets": [{"asset": {"name": "textura_1", "version": 1}}]}
    """
    E eu devo receber o status code '200'


