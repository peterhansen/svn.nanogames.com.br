class CharactersService < ServiceContractBase
  get '/characters.json' do
    page = params[:page] ? params[:page].to_i : 1
    page_size = params[:page_size] ? params[:page_size].to_i : 10

    characters = CharacterResource.get_all(params)

    # TODO Tratamento de relacioamento remoto com Item
    if params['include'] && params['include'] == ('items')
      item_ids = characters.map {|character| character.item_ids}.flatten.uniq
      products = Products::Client.find_list(item_ids)

      # TODO not good. Isso era para estar dentro de CharacterResource.
      unless products['products'].empty?
        characters.each do |character|
          character.items.each do |item|
            items = products['products'].map {|product| product['product']['id'] == item.product_id ? product : nil}.flatten.compact

            unless items.empty?
              item.update_attributes items.first['product']
              item.name = items.first['product']['name']
              item.save
            end
          end
        end
      end
    end

    total = characters.count

    begin
      response = {}
      response[:characters] = characters.map { |r| r.as_json(methods: params[:include]) }
      response[:total] = total
      response[:next_page] = page + 1 unless (page * page_size >= total)
      response[:previous_page] = page -1 unless page == 1
      response[:order] = params[:order] if params[:order]
      response[:search] = params[:search] if params[:search]
      response[:category] = params[:category] if params[:category]

      response.to_json
    rescue CharacterResource::ResourceNotFoundException => e
      e.class::HTTP_STATUS_CODE
    end
  end

  put '/characters/batch.json' do
    begin
      parameters = extract_parameters_from_body

      Character.transaction do
        parameters['characters'].each do |char|
          c = Character.find(char['character']['id'])
          c.update_attributes(char['character'])
        end
      end

      [200, {success: 'Characters updated successfully'}.to_json]
    rescue Exception => e
      Log.info e.message
      [409, {error: 'Characters not updated'}.to_json]
    end
  end

  put '/characters/:id.json' do |id|
    begin
      parameters = extract_parameters_from_body
      parameters = check_item_attribution!(id, parameters['character'])

      CharacterResource.update_resource(id, parameters['character'])
    rescue CharacterResource::ResourceNotFoundException, CharacterResource::ValidationException => e
      [e.class::HTTP_STATUS_CODE, {errors: e.details}.to_json]
    rescue ActiveRecord::RecordNotFound => e
      [404, {errors: e.message}.to_json]
    rescue Exception => e
      [500, {errors: e.message}.to_json]
    end
  end

  private

  def check_item_attribution!(char_id, params)
    user_id = Character.find(char_id).user_id

    if params && params.key?('item_ids')
      item_ids = params['item_ids']
      user_item_ids = Item.where(user_id: user_id).select(:id).map {|i| i.id}

      params['item_ids'] = item_ids & user_item_ids
    end
    {'character' => params}
  end
end
