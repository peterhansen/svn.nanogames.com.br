class AssetsService < ServiceContractBase
  post '/assets/check.json' do
    parameters = extract_parameters_from_body
    invalid_assets = []

    parameters['assets'].each do |asset|
      invalid_assets << AssetResource.check(asset['asset'])
    end

    [200, {assets: invalid_assets.compact}.to_json]
  end

  put '/assets/batch.json' do
    begin
      parameters = extract_parameters_from_body

      parameters['assets'].each do |asset|
        AssetResource.update_or_create_resource(asset['asset'])
      end

      [200, {success: 'Assets atualizados com sucesso'}.to_json]
    rescue Exception => e
      [500, e.message.to_json]
    end
  end
end
