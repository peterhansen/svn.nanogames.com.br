module GameService
  class ConnectionManager
    def initialize(app)
      @app = app
    end

    def call(env)
      ActiveRecord::Base.establish_connection(GameService.db_config)
      status, header, body = @app.call(env)
      ActiveRecord::Base.remove_connection
      [status, header, body]
    end
  end
end
