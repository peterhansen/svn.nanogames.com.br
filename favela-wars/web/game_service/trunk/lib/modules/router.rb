# encoding: utf-8

module GameService
  class Router
    def initialize(app)
      @app = app
    end

    # Este é o endpoint Rack que é executado a cada requisição.
    # Ele mapeia o path de uma requisição para um controlador e encaminha todo
    # o payload da requisição (env) para esse controlador.
    #
    # Exemplo:
    # GET 'users/1234'
    # É mapeado para UsersService
    #
    def call(env)
      env['PATH_INFO'].match(/^\/(\w+)[\/|\.]?.*/)

      env = process_nano_headers(env)

      if $1
        return [200, {"Content-Type" => "text/plain"}, ['<?xml version="1.0"?> <cross-domain-policy> <allow-access-from domain="*"/> </cross-domain-policy>']] if $1 == 'crossdomain'
        return [200, {"Content-Type" => "text/plain"}, ['']] if ['favicon', '__sinatra__'].include? $1

        ActiveRecord::Base.establish_connection(GameService.db_config)
        GameService.const_get("#{$1.capitalize}Service").new.call(env)
      end
    rescue Exception => e
      Log.info "Error: #{e.message}"
      raise e
    end

    def process_nano_headers(env)
      env['REQUEST_METHOD'] = env['HTTP_NANO_METHOD'] if (env['REQUEST_METHOD'] == 'POST') && env['HTTP_NANO_METHOD'].present?
      return env
    end
  end
end
