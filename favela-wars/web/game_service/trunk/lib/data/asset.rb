class Asset < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :name, :version]

  validates_uniqueness_of :name
  validates_presence_of :name
  validates_presence_of :checksum, if: Proc.new { |asset| asset.type.nil? }

  def as_json(options = {})
    options = {} unless options
    super({only: PUBLIC_FIELDS}.merge(options))
  end
end
