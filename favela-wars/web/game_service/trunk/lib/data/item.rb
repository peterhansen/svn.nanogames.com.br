class Item < ActiveRecord::Base
  PUBLIC_FIELDS = [:id]
  attr_accessor :category_id, :name, :price, :properties, :published

  validates_presence_of :user_id, :product_id

  def as_json(options = {})
    options = {} unless options
    super({only: PUBLIC_FIELDS}.merge(options))
  end
end
