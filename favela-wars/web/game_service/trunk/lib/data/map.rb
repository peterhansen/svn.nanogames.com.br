# encoding: utf-8

class Map < Asset
  PUBLIC_FIELDS = [:name, :version, :published]

  has_and_belongs_to_many :assets
  has_many :checksums

  def asset_list=(asset_names)
    asset_names.each do |asset_name|
      asset = Asset.find_by_name(asset_name)
      self.assets << asset if asset
    end
  end

  def asset_list
    self.assets.as_json
  end

  def checksums=(checksums)
    checksums.each do |checksum|
      value = checksum.values.first
      platform = checksum.keys.first

      self.checksums.build value: value, platform: Platform.find_by_name(platform)
    end
  end

  def as_json(options = {})
    options = {} unless options
    super({only: PUBLIC_FIELDS}.merge(options))
  end
end
