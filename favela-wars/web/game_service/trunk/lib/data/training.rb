
  class Training < ActiveRecord::Base
    PUBLIC_FIELDS = [:id,
                     :user_id,
                     :character_id,
                     :product_id,
                     :status,
                     :duration,
                     :duration_on_pause,
                     :expires_at]

    status = {
        (RUNNING = 1) => "Running",
        (PAUSED  = 2) => "Paused",
        (RESTARTED  = 3) => "Restarted",
        (BYPASSED  = 4) => "Bypassed",
        (STOPPED = 5) => "Stopped"
    }

    def status_str(item)
      status[item]
    end

    has_one :character

  end


