# encoding: utf-8

class Character < ActiveRecord::Base
  PUBLIC_FIELDS = [:id, :name]
  attr_accessor :dirty

  serialize :properties
  has_and_belongs_to_many :items,
                          :after_add    => :add_item,
                          :after_remove => :remove_item
  accepts_nested_attributes_for :items

  def as_json(options = {})
    options = {} unless options
    options[:include] = [options[:include]] unless options[:include].nil? or options[:include].class == Array

    if options[:include]
      options[:include].each do |arg|
        options[:methods] ||= []
        options[:only] ||= PUBLIC_FIELDS

        if self.attributes.keys.include?(arg)
          options[:only] << arg if arg
          options[:include].delete arg
        else
          options[:methods] << arg if arg
          options[:include].delete arg
        end
      end
    end

    options.delete_if {|k,v| v.blank?}
    super({only: PUBLIC_FIELDS}.merge(options))
  end

  def changed?
    dirty || super
  end

  private

  def add_item(record)
    record.update_attribute :in_use, true
    self.dirty = true
  end

  def remove_item(record)
    record.update_attribute :in_use, false
    self.dirty = true
  end
end
