# encoding: utf-8
require_relative '../data/character'
require_relative '../data/item'

class CharacterResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    character = Character.find(id)
    character.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Character not found')
  end

  # @return [String]
  def self.get_all(conditions = {})
    where = {}

    conditions.keys.each do |key|
      if Character.column_names.include?(key)
        where[key] = conditions[key]
      end
    end

    additional_fields = extract_extra_fields(conditions)

    Character.where(where).order('created_at desc').select(Character::PUBLIC_FIELDS + additional_fields)
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    character = Character.create(attributes)
    raise ValidationException.new(character.errors.messages) unless character.valid?
    return character.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    character = Character.find_by_id(id)

    if character
      character.update_attributes!(attributes)
      return character.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Character not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(character.errors.messages)
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new(e.message)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    character = Character.find(id)
    character.destroy
    {success: 'Character disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('Character not found')
  end

  def self.count
    {user_count: Character.count}.to_json
  end

  protected

  def self.extract_extra_fields(params)
    fields = []

    if params['include']
      params['include'].split(',').each do |field|
        fields << field if Character.column_names.include?(field)
      end
    end

    fields
  end
end
