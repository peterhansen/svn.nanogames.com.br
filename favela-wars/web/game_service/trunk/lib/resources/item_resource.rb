# encoding: utf-8
require_relative '../data/item'

class ItemResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    item = Item.find(id)
    item.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Item not found')
  end


  # @return [String]
  def self.get_all(conditions = {})
    conditions['in_use'] = false
    where = {}

    conditions.keys.each do |key|
      if Item.column_names.include?(key)
        where[key] = conditions[key]
      end
    end

    additional_fields = extract_extra_fields(conditions)

    Item.where(where).order('created_at desc').select(Item::PUBLIC_FIELDS + additional_fields)
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    item = Item.create(attributes)

    raise ValidationException.new(item.errors.messages) unless item.valid?
    return item.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    item = Item.find_by_id(id)

    if item
      item.update_attributes!(attributes)
      return item.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Item not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(item.errors.messages)
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new(e.message)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    item = Item.find(id)
    item.destroy
    {success: 'Item disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('Item not found')
  end

  def self.count
    {user_count: Item.count}.to_json
  end

  protected

  def self.extract_extra_fields(params)
    fields = []

    if params['include']
      params['include'].split(',').each do |field|
        fields << field if Item.column_names.include?(field)
      end
    end

    fields
  end
end
