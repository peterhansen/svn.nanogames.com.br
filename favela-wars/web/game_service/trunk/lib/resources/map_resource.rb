# encoding: utf-8
require_relative '../data/map'
require_relative '../data/platform'
require_relative '../data/checksum'

class MapResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    map = Map.find_by_name(id)
    map.to_json(extract_options(include).merge({methods: [:asset_list, :version]}))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Map not found')
  end

  # @return [String]
  def self.get_all(conditions = {})
    where = {}

    conditions.keys.each do |key|
      if Map.column_names.include?(key)
        conditions[key] = true if conditions[key] == 'true'
        conditions[key] = false if conditions[key] == 'false'

        where[key] = conditions[key]
      end
    end

    additional_fields = extract_extra_fields(conditions)

    Map.where(where).order('created_at desc').select(Map::PUBLIC_FIELDS + additional_fields)
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    map = Map.find_by_name(attributes['name'])

    if map
      map.update_attributes(attributes.merge(version: map.version + 1))
    else
      map = Map.create(attributes)
    end

    raise ValidationException.new(map.errors.messages) unless map.valid?
    return map.to_json(methods: 'asset_list')
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(name, attributes)
    map = Map.find_by_name(name)

    if map
      map.update_attributes!(attributes)
      return map.reload.to_json only: [:id, :published]
    else
      raise ResourceNotFoundException.new('Map not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(map.errors.messages)
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new(e.message)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    map = Map.find(id)
    map.destroy
    {success: 'Map disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('Map not found')
  end

  def self.count
    {user_count: Map.count}.to_json
  end

  protected

  def self.extract_extra_fields(params)
    fields = []

    if params['include']
      params['include'].split(',').each do |field|
        fields << field if Map.column_names.include?(field)
      end
    end

    fields
  end
end
