# encoding: utf-8
require_relative '../data/asset'

class AssetResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    asset = Asset.find(id)
    asset.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Asset not found')
  end


  # @return [String]
  def self.get_all(conditions = {})
    conditions['in_use'] = false
    where = ''
    values = []

    conditions.keys.each do |key|
      if Asset.column_names.include?(key)
        where << "asset.#{key} = ?"
        values = conditions[key]
      end
    end

    if conditions[:ids]
      where += "assets.id in (?)"
      values << conditions[:ids].split(',')
    end

    additional_fields = extract_extra_fields(conditions)

    Asset.where(where, *values).order('created_at desc').select(Asset::PUBLIC_FIELDS + additional_fields)
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    asset = Asset.create(attributes)

    raise ValidationException.new(asset.errors.messages) unless asset.valid?
    return asset.to_json(only: :id)
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_or_create_resource(attributes)
    asset = Asset.find_by_name(attributes['name'])

    if asset
      asset.update_attributes!(attributes.merge(version: asset.version + 1))
    else
      Asset.create(attributes.merge(version: 1))
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(asset.errors.messages)
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new(e.message)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    asset = Asset.find(id)
    asset.destroy
    {success: 'Asset disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('Asset not found')
  end

  def self.count
    {user_count: Asset.count}.to_json
  end

  def self.check(attributes)
    asset = Asset.find_by_name(attributes['name'])
    return asset if asset.checksum != attributes['checksum'].to_s
  end

  protected

  def self.extract_extra_fields(params)
    fields = []

    if params['include']
      params['include'].split(',').each do |field|
        fields << field if Asset.column_names.include?(field)
      end
    end

    fields
  end
end
