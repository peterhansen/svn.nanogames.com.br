require_relative 'boot'

module GameService
  class App < Sinatra::Base
    set :environment, :development
    use ConnectionManager
    use CrossOrigin
    use Router
  end
end
