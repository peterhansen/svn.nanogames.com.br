# encoding: utf-8
ENV['SINATRA_ENV'] ||= 'development'

require 'bundler/setup'
Bundler.require(:default, ENV['SINATRA_ENV'])

Products::Client.site = 'http://localhost:9393'

db_config = YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/database.yml')['development']
ActiveRecord::Base.establish_connection(db_config)

Log = ::Logger.new(STDOUT)

# TODO dimas - fazer um require dinâmico para esses arquivos.
require_relative 'modules/connection_manager'
require_relative 'modules/router'
require_relative 'modules/cross_origin'

require_relative 'service_contracts/service_contract_base'
require_relative 'service_contracts/characters_service'
require_relative 'service_contracts/items_service'
require_relative 'service_contracts/assets_service'
require_relative 'service_contracts/maps_service'

require_relative 'resources/base_resource'
require_relative 'resources/character_resource'
require_relative 'resources/item_resource'
require_relative 'resources/asset_resource'
require_relative 'resources/map_resource'

module GameService
  def self.env
    ENV['SINATRA_ENV']
  end

  def self.root
    File.expand_path(File.dirname(__FILE__) + '/../')
  end

  def self.logger
    @logger ||= Logger.new(log_file)
  end

  def self.log_file
    @log_file ||= File.open(log_dir + '/' + env + '.log', "a")
  end

  def self.log_dir
    dir = root + '/log'
    Dir::mkdir(dir) unless FileTest::directory?(dir)
    dir
  end

  def self.db_config
    @db_config ||= YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/database.yml')[ENV['SINATRA_ENV']]
  end
end
