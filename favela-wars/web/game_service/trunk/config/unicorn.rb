worker_processes 1
working_directory "/home/ubuntu/game_service/current"

listen "/tmp/game_service.sock", :backlog => 64
timeout 30

pid "/home/ubuntu/game_service/shared/pids/unicorn.pid"
stderr_path "/home/ubuntu/game_service/shared/log/unicorn.stderr.log"
stdout_path "/home/ubuntu/game_service/shared/log/unicorn.stdout.log"

preload_app true

GC.respond_to?(:copy_on_write_friendly=) and GC.copy_on_write_friendly = true
