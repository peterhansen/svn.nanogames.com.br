module LiveGamer
	class BaseClient
		SITE = "https://eval.tfelements.com/tfel2rs/v2"

		# @private
		def self.get(collection, conditions = {})
			query = {
				"auth.partnerId" => LiveGamer::PARTNER_ID,
				"auth.accessKey" => LiveGamer::ACCESS_KEY,
				"auth.appFamilyId" => LiveGamer::APP_FAMILY_ID,
				"auth.namespaceId" => 0
			}.merge(conditions)

			HTTParty.get("#{SITE}/#{collection}?#{query.to_query}")	
		end

		# @private
		def self.post(collection, conditions = {})
			query = {
				"auth.partnerId" => LiveGamer::PARTNER_ID,
				"auth.accessKey" => LiveGamer::ACCESS_KEY,
				"auth.appFamilyId" => LiveGamer::APP_FAMILY_ID,
				"auth.namespaceId" => 0,
				"namespaceId" => 0
			}.merge(conditions)

			HTTParty.post("#{SITE}/#{collection}", body: query.to_query)		
		end
	end
end