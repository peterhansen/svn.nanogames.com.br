# encoding: utf-8

module LiveGamer
	# Essa classe implementa a comunicação com a api de usuários do LiveGamer.
	class Users < BaseClient
		# Retorna um usuário pela chave externa (id do usuário no UserService)
		#
		# @param [String] name Chave externa
		# @return [LiveGamer::User, HTTParty::Response] Usuário
		def self.find(name)
			response = get('user', externalKey: name)

			if response.success?
				return User.new(response['user'])
			end

			response
		end

		# Retorna a lista de usuários
		# @todo Ainda está retornando apenas um Hash
		#
		# @return [Array<LiveGamer::User>] Lista de usuários
		def self.all
			get 'users'
		end

		# Cria um usuário no LiveGamer
		#
		# @param [Hash] attributes Lista de atributos
		# @option attributes [String] :name Nome do usuário REQUERIDO
		# @option attributes [String] :namespace_id Id do namespace (normalmente '0') REQUERIDO
		# @option attributes [Integer] :external_key Chave externa
		# @option attributes [String] :user_class_id Id da classe do usuário
		# @option attributes [String] :excluded_user_class_id Id da classe do usuário excluido
		# @option attributes [String] :email_address Endereço de email
		# @option attributes [String] :gender Gênero do usuário
		# @option attributes [DateTime] :date_of_birth Data de nascimento
		# @option attributes [Hash] :properties Propriedades
		# @option attributes [Array<String>] :tags Lista de Tags
		# @option attributes [String] :blocked Se o usuário está bloqueado
		# @option attributes [Hash] :accounts Contas do usuário
		#
		# @return [LiveGamer::User] Usuário recém-criado
		def self.create_user(attributes)
			post 'user', {namespaceId: 0}.merge(attributes)
		end
	end
end