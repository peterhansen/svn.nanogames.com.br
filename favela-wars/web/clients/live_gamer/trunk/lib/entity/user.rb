# encoding: utf-8

module LiveGamer
	# Essa classe reflete um objeto User do LiveGamer.
	class User
		include ActiveAttr::Attributes
		include ActiveAttr::TypecastedAttributes

		# @!attribute rw id
		attribute :id, type: Integer
		# @!attribute rw name
		attribute :name
		# @!attribute rw namespace_id
		attribute :namespace_id
		# @!attribute rw external_key
		attribute :external_key, type: Integer
		# @!attribute rw user_class_id
		attribute :user_class_id
		# @!attribute rw excluded_user_class_id
		attribute :excluded_user_class_id
		# @!attribute rw email_address
		attribute :email_address
		# @!attribute rw gender
		attribute :gender
		# @!attribute rw date_of_birth
		attribute :date_of_birth, type: DateTime
		# @!attribute rw properties
		attribute :properties, type: Hash
		# @!attribute rw tags
		attribute :tags, type: Array
		# @!attribute rw blocked
		attribute :blocked
		# @!attribute rw accounts
		attribute :accounts, type: Hash

		def initialize(attributes = {})
			self.tags = []
			self.properties = {}

			attributes.each do |k,v|
				self.class.send "attr_accessor", k.to_sym
				self.send "#{k}=", v
			end
		end
	end
end