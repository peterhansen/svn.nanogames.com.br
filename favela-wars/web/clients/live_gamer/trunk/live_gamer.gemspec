# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "version.rb"

Gem::Specification.new do |s|
  s.name        = "live_gamer"
  s.version     = LiveGamer::VERSION
  s.authors     = ["Dimas Cyriaco"]
  s.email       = ["dimascyriaco@gmail.com"]
  s.homepage    = ""
  s.summary     = %q{Nano Product LiveGamer client libraries}
  s.description = %q{Nano Product LiveGamer client libraries}

  s.rubyforge_project = "live_gamer"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  # s.add_development_dependency "rspec"
  # s.add_development_dependency "mysql2"
  # s.add_development_dependency "activerecord"
  # s.add_development_dependency "database_cleaner"

  s.add_runtime_dependency "httparty"
  s.add_runtime_dependency "active_attr"
end
