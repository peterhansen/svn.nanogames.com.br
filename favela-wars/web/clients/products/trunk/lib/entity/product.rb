class Product < NanoSoa::Entity::Base
  # include ActionView::Helpers::NumberHelper

  attr_accessor :id, :name, :price, :price_black, :published, :main_image_id,
    :thumb, :category_id, :price_discounted, :discount_term, :properties,
    :highlights, :category_name, :root_category_id, :tag_list, :descriptions,
    :images

  def initialize(attributes = {})
    attributes.each do |k,v|
      self.class.send "attr_accessor", k.to_sym
      self.send "#{k}=", v
    end

    self.tag_list = [] unless self.tag_list
  end

  def formatted_price
    number_to_currency(self.price, unit: 'G', format: "%n %u", precision: 0, delimiter: '.')
  end
end
