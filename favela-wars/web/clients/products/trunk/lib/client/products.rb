module Products
  class Client < NanoSoa::Client::Base
    resource_name :product
    self.site = 'http://products.staging.nanostudio.com.br'

    ASSOCIATIONS         = [:description]
    @@DEFAULT_PAGE_SIZE  = 10
    @@FORMAT             = '.json'

    def self.all(params = {})
      query = {}
      query["nextpage"]  = params["next_page"] if params["next_page"]
      query["order"]     = params["order"] if params["order"]
      query["search"]    = params["search"] if params["search"]
      query["category"]  = params["category"] if params["category"]
      query["page"]      = params["next_page"] ? params["next_page"] : 1
      query["page_size"] = params["page_size"] ? params["page_size"] : @@DEFAULT_PAGE_SIZE
      query["include"]   = 'thumb'
      query["order"]     = params[:order] ? params[:order] : 'view_count desc'

      collection = get("?#{query.to_query}")
      collection['products'].map! { |resource| Product.new(resource['product']) }

      collection
    end

    def self.highlights
      get('/highlights.json')
    end

    def self.create_product(attributes)
      HTTParty.post("#{site}/products.json", body: {product: attributes}.to_json)
    end

    def self.create_category(attributes)
      HTTParty.post("#{site}/categories.json", body: {category: attributes}.to_json)
    end

    def self.find_list(ids)
      get ".json?ids=#{ids.join(',')}"
    rescue Exception => e
      raise ::StandardError.new('Unreachable Service')
    end

    def self.increment_view_count(id)
      put('/' + id.to_s + @@FORMAT, body: { product: { increment_view_count: true }}.to_json)
    end

    def self.get_product(id, options = {})
      query_string = ''

      unless options.blank?
        query_string << '?'

        options.each do |k, v|
          if v.is_a? Array
            query_string << "#{k.to_s}=#{v.join(',')}"
          else
            query_string << "#{k.to_s}=#{v.to_s}"
          end
        end
      end

      get("/#{id}.json#{query_string}")
    end

    def self.add_asset_to_product(asset_id, product_id)
      HTTParty.post "#{site}/products/#{product_id}/assets.json", body: {asset: {id: asset_id}}.to_json
    end

    def self.remove_asset_from_product(asset_id, product_id)
      HTTParty.delete "#{site}/products/#{product_id}/assets/#{asset_id}.json"
    end

    def self.get_troop_place_items
      HTTParty.get "#{site}/categories/troop_place/products.json"
    end
  end
end
