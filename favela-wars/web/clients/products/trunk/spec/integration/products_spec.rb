require 'spec_helper'

describe Products::Client do
  context "procurando um produto" do
    it "deve fazer uma chamada para HTTParty.get" do
      # HTTParty.should_receive(:get).with("http://localhost:9494/products/1.json?include=thumb,category_name,root_category_id").
      # and_return(stub(parsed_response: {}.to_json))
      # Products::Client.find(1, include: [:thumb, :category_name, :root_category_id])
    end
  end

  context "procurando lista de produtos" do
    it "deve retornar os produtos" do
      Products::Client.create_product(name: 'Trabuco', published: true)
      response = Products::Client.find_list([1,2,3])
      response['products'].size.should == 1
    end
  end

  context "configurando o site" do
    it "permitir que eu configure o site" do
      Products::Client.site = 'http://qualquercoisa.com'
      Products::Client.site.should == 'http://qualquercoisa.com'
    end
  end

  context "adicionando asset a produto" do
    it "deve adicionar o asset ao produto" do
      Products::Client.site = 'http://localhost:9494'

      response = Products::Client.create_product(name: 'Trabuco')
      product_id = JSON.parse(response)['product']['id']

      Products::Client.add_asset_to_product(1234, product_id)

      response = Products::Client.get_product(product_id, include: 'asset_ids')
      response['product']['asset_ids'].should == [1234]
    end
  end

  context "consultando os produtos do tipo troop_place" do
    it "deve funfar" do
      response = Products::Client.create_category(name: 'troop_place')
      category = JSON.parse(response.body)['category']
      Products::Client.create_product(name: 'Morra', category_id: category['id'])

      response = Products::Client.get_troop_place_items
      json_response = JSON.parse(response)
      json_response['products'].first['product']['category_id'].should eq(category['id'])
    end
  end
end
