# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "version.rb"

Gem::Specification.new do |s|
  s.name        = "game_client"
  s.version     = Game::VERSION
  s.authors     = ["Dimas Cyriaco"]
  s.email       = ["dimascyriaco@gmail.com"]
  s.homepage    = ""
  s.summary     = %q{Nano Game client libraries}
  s.description = %q{Nano Game client libraries}

  s.rubyforge_project = "game_client"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  s.add_development_dependency "rspec"
  s.add_development_dependency "activerecord"
  s.add_development_dependency "database_cleaner"

  s.add_runtime_dependency "nano_soa_client", '0.0.11.alpha'
end
