module Game
  class Client < NanoSoa::Client::Base
    resource_name :item
    self.site = 'http://localhost:9292'

    ASSOCIATIONS         = []
    @@DEFAULT_PAGE_SIZE  = 10
    @@FORMAT             = '.json'

    def self.add_item(user_id, item_id)
      HTTParty.post("#{full_path}.json", body: {item:{user_id: user_id, product_id: item_id}}.to_json)
    end

    def self.create_map(attributes)
      HTTParty.post("#{site}/maps.json", body: {map: attributes}.to_json).parsed_response
    end

    def self.update_map(map_name, attributes)
      HTTParty.put("#{site}/maps/#{map_name}.json", body: {map: attributes}.to_json).parsed_response
    end

    def self.get_maps
      HTTParty.get("#{site}/maps.json").parsed_response
    end

    def self.get_maps_filtered(filters)
      query = ''

      filters.each do |k, v|
        query << "#{k}=#{v}"
      end

      HTTParty.get("#{site}/maps.json?#{query}").parsed_response
    end

    def self.publish_map(map_name)
      HTTParty.put("#{site}/maps/#{map_name}.json", body: {map: {published: true}}.to_json).parsed_response
    end

    def self.unpublish_map(map_name)
      HTTParty.put("#{site}/maps/#{map_name}.json", body: {map: {published: false}}.to_json).parsed_response
    end

    def self.get_character(id, filters = {})
      query = ''

      filters.each do |k, v|
        query << "#{k}=#{v}"
      end

      HTTParty.get("#{site}/characters/#{id}.json?#{query}").parsed_response
    end

    def self.create_character(attributes)
      HTTParty.post("#{site}/characters.json", body: {character: attributes}.to_json).parsed_response
    end

    def self.update_characters_batch(attributes)
      HTTParty.put("#{site}/characters/batch.json", body: {characters: attributes}.to_json).parsed_response
    end

    def self.get_assets(asset_ids)
      JSON.parse HTTParty.get("#{site}/assets.json?ids=#{asset_ids.join(',')}").parsed_response
    end
  end
end
