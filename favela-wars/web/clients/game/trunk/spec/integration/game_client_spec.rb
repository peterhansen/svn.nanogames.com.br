require 'spec_helper'

describe Game::Client do
  context "Adicionando um item ao arsenal" do
    it "deve fazer uma chamada para HTTParty.get" do
      json_response = Game::Client.add_item(1, 2)
      response = JSON.parse(json_response)
      response.key?('item').should be_true
    end
  end

  context "consultando mapas" do
    it "deve retornar uma lista de mapas" do
      json_response = Game::Client.get_maps
      response = JSON.parse(json_response)
      response.key?('maps').should be_true
    end

    it "deve publicar um mapa" do
      Game::Client.create_map(name: 'Teste1')
      maps = JSON.parse(Game::Client.get_maps)
      map = maps['maps'].first['map']
      json_response = Game::Client.publish_map(map['name'])
      JSON.parse(json_response)['map']['published'].should == true
    end

    it "deve despublicar um mapa" do
      Game::Client.create_map(name: 'Teste1')
      maps = JSON.parse(Game::Client.get_maps)
      map = maps['maps'].first['map']
      json_response = Game::Client.publish_map(map['name'])
      JSON.parse(json_response)['map']['published'].should == true
      json_response = Game::Client.unpublish_map(map['name'])
      JSON.parse(json_response)['map']['published'].should == false
    end

    it "deve retornar mapas filtrados" do
      Game::Client.create_map(name: 'Teste1')
      Game::Client.create_map(name: 'Teste2')
      json_response = Game::Client.update_map('Teste1', published: true)
      JSON.parse(json_response)['map']['published'].should == true
      json_response = Game::Client.get_maps_filtered(published: :true)
      JSON.parse(json_response)['maps'].size.should == 1
    end
  end

  context "atualizando personagens" do
    it "deve atualizar os personagens" do
      Game::Client.create_character(name: 'Lindomar')
      chars = {characters: {character: {id: 1, name: 'Lindomar', properties: 'ble'}}}
      json_response = Game::Client.update_characters_batch(chars)
      json_response.should == "{\"success\":\"Characters updated successfully\"}"
      character = Game::Client.get_character(1, include: 'properties')
      character.should == "{\"character\":{\"id\":1,\"name\":\"Lindomar\",\"properties\":\"ble\"}}"
    end
  end

  context "consultando assets" do
    it "deve retornar os assets" do
      Game::Client.create_map(name: 'Teste1')
      assets = Game::Client.get_assets([1])['assets']
      assets.first['asset']['name'].should_not be_nil
    end
  end
end
