require 'game_client.rb'

Game::Client.site = 'http://localhost:9292'

# Set up gems listed in the Gemfile.
ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __FILE__)
require 'bundler/setup' if File.exists?(ENV['BUNDLE_GEMFILE'])

require 'active_record'
require 'active_record/connection_adapters/abstract_adapter'
require 'active_record/connection_adapters/abstract_mysql_adapter'
require 'database_cleaner'

DatabaseCleaner.strategy = :truncation

class Asset < ActiveRecord::Base; end

RSpec.configure do |config|
  config.before(:each) do
    # ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'game_test')
    # DatabaseCleaner.clean
  end
end
