class AddTrainingTypeToTrainings < ActiveRecord::Migration
	def self.up
		add_column :trainings, :training_type, :string
	end

	def self.down
		remove_column :trainings, :training_type
	end
end