class CreateTrainingRequests < ActiveRecord::Migration
  def self.up
    create_table :training_requests, force: true do |t|
      t.string :body
      t.timestamps
    end
  end

  def self.down
    drop_table :training_requests
  end
end
