class CreateTrainings < ActiveRecord::Migration
  def self.up
    create_table :trainings, force: true do |t|
      t.integer  :character_id
      t.integer  :user_id
      t.integer  :product_id
      t.integer  :status
      t.integer  :duration
      t.integer  :duration_on_pause
      t.datetime :expires_at
      t.timestamps
    end

    add_index :trainings, [:user_id, :product_id]
  end

  def self.down
    drop_table :trainings
  end
end
