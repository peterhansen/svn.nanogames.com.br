# encoding: utf-8

Given "que exista um treinamento $exist duracao cadastrada" do |exist|
  product = if exist == 'com'
    Product.new properties: {'duration' => 10}
  else
    Product.new
  end

  Products::Client.should_receive(:find).and_return(product)
end
