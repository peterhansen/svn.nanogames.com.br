# encoding: utf-8

When "eu envio um GET para '$path'" do |path|
  get path
end

When "eu envio um POST para '$path' com os dados:" do |path, params|
  post path, params
end

When "eu envio um PUT para '$path' com os dados:" do |path, params|
  put path, params
end

Then "eu devo receber o JSON:" do |json|
  JSON.parse(last_response.body).should == JSON.parse(json)
end

When "eu devo receber o status code '$code'" do |code|
  last_response.status.should == code.to_i
end
