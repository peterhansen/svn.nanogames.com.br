# encoding: utf-8

Given "que o personagem '$name' existe com o id '$id' e user_id '$user_id'" do |name, id, user_id|
  Game::Client.should_receive(:get_character).with(name).and_return ""
end

Given "que o personagem '$character_id' esteja no treinamento '$training_id' com status '$status_text'" do |character_id, training_id, status_text|
  case status_text
    when "on_training"
      status_code = ::Training::RUNNING
    when "pause"
      status_code = ::Training::PAUSED
    when "stop"
      status_code = ::Training::STOPPED
    else
      status_code = 0
  end

  FactoryGirl.create(:training, id: training_id,
                                character_id: character_id,
                                duration:1800,
                                duration_on_pause:600,
                                product_id: 10,
                                status: status_code,
                                expires_at: (Time.now + 20 * 60),
                                user_id: 1)
end
