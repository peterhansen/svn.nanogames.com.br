ENV["SINATRA_ENV"] = "test"

require_relative '../../lib/training_service'
require 'rack/test'
require 'cucumber/rspec/doubles'

Dir.glob(File.join(File.dirname(__FILE__), '../../spec/factories.rb')).each {|f| require f }

DatabaseCleaner.strategy = :truncation

Before do
  ActiveRecord::Base.establish_connection(adapter: 'mysql2', database: 'training_test')
  DatabaseCleaner.clean
end

module AppHelper
  def app
    TrainingService::App
  end
end

World(Rack::Test::Methods, AppHelper)
