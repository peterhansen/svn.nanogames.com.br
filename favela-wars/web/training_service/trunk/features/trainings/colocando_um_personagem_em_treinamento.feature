# language: pt

Funcionalidade: Colocando um personagem em treinamento
  Contexto:
    #Dado que o personagem 'Zé' existe com o id '2' e user_id '1'

  Cenário: Colocando o personagem em treinamento
    Dado que exista um treinamento com duracao cadastrada
    E que o scheduler está rodando
    Quando eu envio um POST para '/trainings' com os dados:
    """
      {"training": {
        "user_id": 1,
        "character_id":1,
        "product_id": "2"
      }}
    """
    Então eu devo receber o status code '201'

  Cenário: Colocando o personagem num treinamento sem duração cadastrada
    Dado que exista um treinamento 'sem' duracao cadastrada
    Quando eu envio um POST para '/trainings' com os dados:
    """
      {"training": {
        "user_id": "1",
        "product_id": "2"
      }}
    """
    Então eu devo receber o status code '501'

  Cenário: Pausando o treinamento do personagem
    Dado que o personagem '2' esteja no treinamento '1' com status 'on_training'
    Quando eu envio um PUT para '/trainings/1' com os dados:
    """
      {"training": {
        "character_id": 1,
        "action":"pause"
      }}
    """
    Então eu devo receber o status code '200'
    E eu envio um GET para '/trainings/1'
    Então eu devo receber o JSON:
    """
    {"training": {"id": 1, "product_id": 10, "status": 2}}
    """

  Cenário: Reiniciando o treinamento do personagem
    Dado que o personagem '2' esteja no treinamento '1' com status 'pause'
    Quando eu envio um PUT para '/trainings/1' com os dados:
    """
      {"training": {
        "character_id": 1,
        "action":"restart"
      }}
    """
    E eu envio um GET para '/trainings/1'
    Então eu devo receber o JSON:
    """
    {"training": {
      "id": 1,
      "product_id": 10,
      "status": 3}
    }
    """

  Cenário: Retirando o personagem do treinamento
    Dado que o personagem '2' esteja no treinamento '1' com status 'pause'
    Quando eu envio um PUT para '/trainings/1' com os dados:
    """
      {"training": {
        "character_id": 1,
        "action":"stop"
      }}
    """
    E eu envio um GET para '/trainings/1'
    Então eu devo receber o status code '404'

  @wip
  Cenário: Fazendo o personagem bypassar o tempo do treinamento
    Dado que o personagem '2' esteja no treinamento '1' com status 'on_training'
    Quando eu envio um PUT para '/trainings/1' com os dados:
    """
      {"training": {
        "character_id": 1,
        "action":"bypass"
      }}
    """
    E eu envio um GET para '/trainings/1'
    Então eu devo receber o status code '404'
