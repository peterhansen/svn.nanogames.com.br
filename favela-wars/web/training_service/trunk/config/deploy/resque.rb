require 'resque'
# Start a worker with proper env vars and output redirection
def run_worker(queue = "*", count = 1)
  puts "Starting #{count} worker(s) with QUEUE: #{queue}"

  ops = {
    :pgroup => true,
    :err => [("log/workers_error.log").to_s, "a"], 
    :out => [("log/workers.log").to_s, "a"]
  }

  env_vars = {
    "QUEUE" => queue.to_s,
    "DATABASE_ENV" => 'production'
  }

  count.times {
    ## Using Kernel.spawn and Process.detach because regular system() call would
    ## cause the processes to quit when capistrano finishes
    pid = spawn(env_vars, "bin/rake resque:work", ops)
    Process.detach(pid)
  }
end

namespace :resque do
  desc "Restart running workers"
  task :restart_workers do
    Rake::Task['resque:stop_workers'].invoke
    Rake::Task['resque:start_workers'].invoke
  end
  
  desc "Quit running workers"
  task :stop_workers do
    pids = Array.new

    Resque.workers.each do |worker|
      pids.concat(worker.worker_pids)
    end

    if pids.empty?
      puts "No workers to kill"
    else
      syscmd = "kill -s QUIT #{pids.join(' ')}"
      puts "Running syscmd: #{syscmd}"
      system(syscmd)
    end
  end
  
  desc "Start workers"
  task :start_workers do
    run_worker
  end
end