namespace :deploy do
  namespace :db do
    desc <<-DESC
    Make a copy of the database.[ENV].yml to shared/config/database.yml and them symlinks it to the config folder.
    This file is required to the creation of the database.
    DESC
    task :symlink do
      check_shared_config
      run "cd #{release_path}; cp -f config/database.example.yml #{shared_path}/config/database.yml"
      run "ln -sf #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    end

    desc 'Create database if it doesnt exists'
    task :create do
      pp "Creating database (if it does not exists)."
      run "mysql -u root -e 'CREATE DATABASE IF NOT EXISTS trainings_#{env}'"
    end
  end
end

def check_shared_config
  config_dir = shared_path + '/config'
  pp "Redeploying symlinks to database.yml."

  unless directory_exists? config_dir
    pp "shared/config does not exists. Creating it..."
    run "mkdir #{config_dir}"
  end
end
