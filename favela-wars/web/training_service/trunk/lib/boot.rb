# encoding: utf-8
ENV['SINATRA_ENV'] ||= 'development'

require 'bundler/setup'
Bundler.require(:default, ENV['SINATRA_ENV'])

if ENV['SINATRA_ENV'] == 'production'
  Products::Client.site = 'http://products.staging.nanostudio.com.br'
  Game::Client.site = 'http://game.staging.nanostudio.com.br'
else
  Products::Client.site = 'http://localhost:9595'
  Game::Client.site = 'http://localhost:9696'
end

db_config = YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/database.yml')['development']
ActiveRecord::Base.establish_connection(db_config)

Log = ::Logger.new(STDOUT)

# TODO dimas - fazer um require dinâmico para esses arquivos.
require_relative 'modules/connection_manager'
require_relative 'modules/router'
require_relative 'modules/cross_origin'

require_relative 'services/service_contract_base'
require_relative 'services/training_requests_service'
require_relative 'services/trainings_service'

require_relative 'resources/base_resource'
require_relative 'resources/training_request_resource'
require_relative 'resources/training_resource'

require_relative 'workers/training_resolver'

require_relative 'rules/training/start'
require_relative 'rules/training/pause'
require_relative 'rules/training/restart'
require_relative 'rules/training/stop'
require_relative 'rules/training/bypass'

module TrainingService
  def self.env
    ENV['SINATRA_ENV']
  end

  def self.root
    File.expand_path(File.dirname(__FILE__) + '/../')
  end

  def self.logger
    @logger ||= Logger.new(log_file)
  end

  def self.log_file
    @log_file ||= File.open(log_dir + '/' + env + '.log', "a")
  end

  def self.log_dir
    dir = root + '/log'
    Dir::mkdir(dir) unless FileTest::directory?(dir)
    dir
  end

  def self.db_config
    @db_config ||= YAML::load_file(File.expand_path(File.dirname(__FILE__) + '/..') + '/config/database.yml')[ENV['SINATRA_ENV']]
  end
end
