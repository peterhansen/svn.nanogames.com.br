class SampleTraining
  def get_trained_character(body)
    Log.info body.class
    # Game::Client.get_character(training_body['character_id'])
    { character: { id: body['character_id'], properties: { has_sample_training: true }}}
  end
end
