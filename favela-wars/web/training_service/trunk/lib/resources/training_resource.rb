# encoding: utf-8
#require_relative '../data/character'
#require_relative '../data/training'
#require_relative '../data/item'

class TrainingResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    entity = Training.find(id)
    entity.to_json(extract_options(include).merge({methods: [:asset_list, :version]}))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('Training not found')
  end

  # @return [String]
  def self.get_all(conditions = {})
    where = {}

    conditions.keys.each do |key|
      if Training.column_names.include?(key)
        where[key] = conditions[key]
      end
    end

    additional_fields = extract_extra_fields(conditions)

    Training.where(where).order('created_at desc').select(Training::PUBLIC_FIELDS + additional_fields)
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    attrs = JSON.parse(attributes)
    training = Training.create(attrs['training'])

    raise ValidationException.new(Training.errors.messages) unless training.valid?
    return training.to_json
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    entity = Training.find_by_id(id)

    if entity
      entity.update_attributes!(attributes)
      return entity.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('Training not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(entity.errors.messages)
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new(e.message)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    entity = Training.find(id)
    entity.destroy
    {success: 'Training disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('Training not found')
  end

  def self.count
    {count: Training.count}.to_json
  end

  protected

  def self.extract_extra_fields(params)
    fields = []

    if params['include']
      params['include'].split(',').each do |field|
        fields << field if Training.column_names.include?(field)
      end
    end

    fields
  end
end
