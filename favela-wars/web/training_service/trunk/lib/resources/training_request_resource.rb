# encoding: utf-8
require_relative '../data/training_request'

class TrainingRequestResource < BaseResource
  # @param id [Sting]
  # @param fields [Hash]
  # @return [String]
  def self.get_resource(id, include = '')
    training_request = TrainingRequest.find(id)
    training_request.to_json(extract_options(include))
  rescue ActiveRecord::RecordNotFound => e
    raise ResourceNotFoundException.new('TrainingRequest not found')
  end

  # @return [String]
  def self.get_all(page = nil, page_size = nil)
    TrainingRequest.order('created_at desc').select(TrainingRequest::PUBLIC_FIELDS)
    #{training_requests: TrainingRequest.order('created_at desc').select(TrainingRequest::PUBLIC_FIELDS).paginate(:page => page, :per_page => page_size)}.to_json
  end

  # @param attributes [Hash]
  # @return [String]
  def self.create_resource(attributes)
    # body = {"tasks":[{"task":{"body":"---\ncharacter_id: 2\n","expires_on":"2012-05-29T14:47:33-03:00","id":6,"type":"sample"}}]}
    training_request = TrainingRequest.create(body: attributes)
    Resque.enqueue(TrainingResolver, training_request.id)

    raise ValidationException.new(training_request.errors.messages) unless training_request.valid?
    return training_request.to_json(only: :id) if training_request
  rescue ActiveRecord::UnknownAttributeError => e
    raise InvalidRequestException.new(e.message)
  end

  # @param id [String]
  # @param attributes [Hash]
  # @return [String]
  def self.update_resource(id, attributes)
    training_request = TrainingRequest.find_by_id(id)

    if training_request
      training_request.update_attributes!(attributes)
      return training_request.reload.to_json only: :id
    else
      raise ResourceNotFoundException.new('TrainingRequest not found')
    end

  rescue ActiveRecord::RecordInvalid
    raise ValidationException.new(training_request.errors.messages)
  end

  # @param id [Sting]
  # @return [String]
  def self.delete_resource(id)
    training_request = TrainingRequest.find(id)
    training_request.destroy
    {success: 'TrainingRequest disabled'}.to_json
  rescue
    raise ResourceNotFoundException.new('TrainingRequest not found')
  end

  def self.count
    {training_request_count: TrainingRequest.count}.to_json
  end
end
