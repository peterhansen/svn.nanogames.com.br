# encoding: utf-8
require_relative 'change_base'

module Rules
  module Training
    class Bypass < ChangeBase
      def execute_core
        remove_schedule
        save_character
        notify_user
        @training.destroy
        @worked = true
      end

      def save_character
        TrainingRequestResource.create_resource(tasks: [@training.to_result])
      end

      def notify_user
        #TODO: not implemented!
        message = {message: {user_id: @training.user_id, body: "Seu treinamento acabou"}}.to_json
        HTTParty.post("#{NOTIFICATION_SERVICE}/messages", body: message)
      end
    end
  end
end