# encoding: utf-8
require_relative '../../data/training'
require_relative 'change_base'

module Rules
  module Training
    #<remarks>
    #pega a duração do treinamento,
    #quando o usuário pausa o treinamento, guardamos a quantidade de tempo que o personagem já treinou.
    #Duranta a reiniciação do treinamento, pegamos o tempo treinado - o tempo do treinamento e determinamos
    #o próximo momento de expiração.
    #</remarks>
    class Restart < ChangeBase

      def execute_core

        if @training.status == ::Training::PAUSED

          remaining_minutes = @training.duration -  @training.duration_on_pause
          rescheduled_time  = Time.now +  remaining_minutes * 60

          Log.info "Elapsed time during pause #{@training.duration_on_pause}"
          Log.info "Remaining minutes to finish: #{remaining_minutes}"
          Log.info "Rescheduled time expiration #{rescheduled_time}"

          @training.status            = ::Training::RESTARTED
          @training.expires_at        = rescheduled_time
          @training.duration          = remaining_minutes
          @training.duration_on_pause = 0
          @training.save

          Log.info "Scheduler, please watch over this message..."
          @worked  = true
        else
          @message = {error_message: "Restart command is only possible if the training is paused."}
          @worked  = false
        end
      end
    end
  end
end
