# encoding: utf-8
require_relative '../../data/training'
require_relative 'change_base'

module Rules
  module Training
    class Stop < ChangeBase
      def execute_core
          @training.destroy
          # remove_schedule
          @worked = true
      end
    end
  end
end
