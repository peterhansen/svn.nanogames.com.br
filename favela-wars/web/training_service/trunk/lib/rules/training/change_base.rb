require_relative '../../data/training'
require_relative 'base_works'

module Rules
  module Training
    class ChangeBase < BaseWorks

      attr_accessor :character_id, :character_training_id, :worked, :message

      def initialize
        @character_id  = 0
        @worked        = false
        @character_training_id = 0
        @training = nil
        @message = ""
      end

      def execute
        begin
          @training = ::Training.find(@character_training_id)

          ActiveRecord::Base.transaction do
            if @training
              execute_core
            else
              @message = { error_message: "Training doesn't exist." }
              @worked  = false
              return
            end
          end

        rescue Exception => e
          @message = {error_message: e.message}
          @worked  = false
          Log.info "Error during updating #{e.message}"
        end
      end

      def execute_core
        #abstract implementatissssssssdddsason?
      end

      def remove_schedule
        raise StandardError.new('Not implemented')
      end
    end
  end
end

