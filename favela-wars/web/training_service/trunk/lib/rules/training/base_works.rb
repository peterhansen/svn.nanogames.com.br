# language: pt
require_relative 'base_works'

module Rules
  module Training
    class BaseWorks
      def define_expiration_time(product_id)
        @product = Products::Client.find(product_id)

        if @product.properties && @product.properties.key?('duration')
          @duration  = @product.properties['duration']
          @expires_at = Time.now +  @duration * 60
          return true
        else
          return false
        end
      rescue Exception => e
        p e.message
        raise e
      end

      def make_appointment(training)
        task = {
          type: training.training_type,
          expires_on: training.expires_at,
          body: {
            product_id: training.product_id,
            character_id: training.character_id,
            user_id: training.user_id
          }
        }
        response = HTTParty.post('http://localhost:8080', body: {task: task}.to_json)
        puts "Sending to scheduler... Response: #{response.parsed_response}"
        raise "Scheduler error #{response.parsed_response}." unless response.success?
      rescue Exception => e
        p e.message
        raise e
      end
    end
  end
end
