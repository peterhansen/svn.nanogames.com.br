# encoding: utf-8
require_relative '../../data/training'
require_relative 'base_works'

module Rules
  module Training
    class Start < BaseWorks
      attr_accessor :worked, :character_training_id, :status

      def initialize(character_id, product_id, user_id)
        @training_id  = product_id
        @character_id = character_id
        @user_id      = user_id
        @worked       = false
        @expires_at   = 0     #duração em minutos
        @character_training_id = 0
        @duration = 0
      end

      def execute
        if define_expiration_time(@training_id)
          ActiveRecord::Base.transaction do
            training = set_on_training
            make_appointment training
          end

          @worked = true
        end
      rescue Exception => e
        @worked = false
        @status = e.message
      end

      def set_on_training
        training = ::Training.create character_id: @character_id,
          product_id:    @product.id,
          training_type: @product.properties['type'],
          user_id:       @user_id,
          status:        ::Training::RUNNING,
          duration:      @duration,
          expires_at:    @expires_at

        @character_training_id = training.id

        training
      end
    end
  end
end
