# encoding: utf-8
require_relative '../../data/training'
require_relative 'change_base'

module Rules
  module Training
    class Pause < ChangeBase
      def execute_core
        if  @training.status == ::Training::RUNNING
          passed_time = @training.expires_at - Time.now

          @training.duration_on_pause = (passed_time * 24 * 60).to_i
          @training.status            = ::Training::PAUSED
          @training.expires_at        = nil
          @training.save

          # remove_schedule
          @worked = true
        else
          @message = {error_message: "training cannot be paused unless it is running"}
          p @message
          @worked = false
        end
      end
    end
  end
end
