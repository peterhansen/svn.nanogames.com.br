require 'active_support/inflector'
ResqueLog = ::Logger.new('log/redis.log')

class TrainingResolver
  @queue = :trainings

  def self.perform(training_request_id)
    ResqueLog.info "Training: #{training_request_id}"
    training_request = TrainingRequest.find(training_request_id)
    ResqueLog.info training_request

    trainings = JSON.parse training_request.body
    ResqueLog.info trainings['tasks']
    characters = []

    trainings['tasks'].each do |training|
      ResqueLog.info training
      resolver = self.get_resolver(training['task']['type'])
      character = resolver.get_trained_character(training['task']['body'])
      ResqueLog.info character

      characters << character if character
    end
    ResqueLog.info characters.inspect

    response = Game::Client.update_characters_batch(characters)
    ResqueLog.info response

    # raise "Error updating character" unless response.success?

    training_request.destroy
  end

  def self.get_resolver(training_type)
    Object.const_get("#{training_type.camelize}Training").new
  end
end
