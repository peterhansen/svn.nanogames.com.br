module TrainingService
  class Router
    def initialize(app)
      @app = app
    end

    # Este é o endpoint Rack que é executado a cada requisição.
    # Ele mapeia o path de uma requisição para um controlador e encaminha todo
    # o payload da requisição (env) para esse controlador.
    #
    # Exemplo:
    # GET 'users/1234'
    # É mapeado para UsersService
    #
    def call(env)
      env['PATH_INFO'].match(/^\/(\w+)[\/|\.]?.*/)

      if $1
        return [200, {"Content-Type" => "text/plain"}, ['']] if ['favicon', '__sinatra__'].include? $1

        ActiveRecord::Base.establish_connection(TrainingService.db_config)
        TrainingService.const_get("#{$1.camelize}Service").new.call(env)
      end
    end
  end
end
