require_relative 'boot'

module TrainingService
	NOTIFICATION_SERVICE = 'http://localhost:9797'

  class App < Sinatra::Base
    set :environment, :development
    use ConnectionManager
    use CrossOrigin
    use Router
  end
end
