class ServiceContractBase < Sinatra::Base
  # Essa é a classe base para todos os controladores do serviço.
  # Ao instanciar um objeto de uma classe herdada de BaseControler
  # as variaveis @resource_name e @resource são configuradas com base no nome do controlador.
  #
  # Exemplo:
  #
  # ProductsService.new
  # @resource_name == 'products'
  # @resource == ProductResource
  #
  # A variavel @resource_name será utilizada para configuração das rotas.
  # No exemplo acima as rotas seriam:
  #
  # GET '/products'
  # GET '/products/:id'
  # etc...
  #
  # A variavel @resource aponta para a classe que será utilizada para o processamento da requisição.
  # No exemplo acima a classe ProductResource será utilizada.
  #
  # Esse processo é automatizado. Caso queria criar um controlador para ImageResource
  # basta criar um controlador chamado ImagesService e herdar de ServiceContractBase.
  def initialize
    super
    self.class.to_s.match /^(\w+)Service$/
    @resource_name = $1.underscore
    @resource ||= Object.const_get "#{$1.singularize}Resource"


    self.class.get %r{/#{@resource_name}/count.?[\w+]?} do
      {"#{@resource_name.singularize}_count".to_sym => @resource.count}.to_json
    end

    self.class.get %r{/#{@resource_name}/new.?[\w+]?} do
      @resource.new
    end

    self.class.get %r{/#{@resource_name}/(\w+).?[\w+]?} do |id|
      begin
        [200, @resource.get_resource(id, params['include'])]
      rescue @resource::ResourceNotFoundException => e
        [e.class::HTTP_STATUS_CODE, {errors: e.details}.to_json]
      end
    end

    self.class.post %r{\/#{@resource_name}\.?(\w+)?$} do
      begin
        parameters = request_body

        [201, @resource.create_resource(parameters)]
      rescue @resource::ValidationException, @resource::InvalidRequestException => e
        [e.class::HTTP_STATUS_CODE, {errors: e.details}.to_json]
      end
    end

    self.class.get %r{/#{@resource_name}.?[\w+]?} do
      page = params[:page] ? params[:page].to_i : 1
      page_size = params[:page_size] ? params[:page_size].to_i : 10

      resource_query = {
          page: page,
          page_size: page_size,
          order_by: params[:order],
          search: params[:search]
      }

      extend_resource(resource_query, params)
      resource = @resource.get_all(resource_query)

      total = 0
      #total = resource.count

      begin
        response = {}
        response[@resource_name.to_sym] = resource.map { |product| product.reload.as_json(methods: params[:include]) }
        response[:total] = total
        response[:next_page] = page + 1 unless (page * page_size >= total)
        response[:previous_page] = page -1 unless page == 1
        response[:order] = params[:order] if params[:order]
        response[:search] = params[:search] if params[:search]
        response[:category] = params[:category] if params[:category]

        response.to_json
      rescue @resource::ResourceNotFoundException => e
        e.class::HTTP_STATUS_CODE
      end
    end

    self.class.put %r{/#{@resource_name}/(\w+).?[\w+]?} do |id|
      begin
        parameters = extract_parameters_from_body
        @resource.update_resource(id, parameters[@resource_name.singularize])
      rescue @resource::ResourceNotFoundException, @resource::ValidationException => e
        [e.class::HTTP_STATUS_CODE, {errors: e.details}.to_json]
      rescue Exception => e
        [500, {errors: e.message}.to_json]
      end
    end

    self.class.delete %r{/#{@resource_name}/?(\w+).?[\w+]?} do |id|
      begin
        @resource.delete_resource(id)
      rescue @resource::ResourceNotFoundException => e
        e.class::HTTP_STATUS_CODE
      end
    end
  end

  #configure do
    #disable :show_exceptions
  #end

  #error do
    #content_type :json
    #{error: env['sinatra.error'].message}.to_json
  #end

  private

  #método virtual para aumentar os parâmetros do resource
  def extend_resource(resource, params)
    #exemplo implementado no products_service
    #Log.warn 'Not Implemented'
  end

  def next_page_for_collection(page = 1, page_size = 10)
    host + request.env['PATH_INFO'] + "?page=#{page.to_i + 1}&page_size=#{page_size}"
  end

  def previous_page_for_collection(page = 1, page_size = 10)
    page.to_i > 1 ? host + request.env['PATH_INFO'] + "?page=#{page.to_i - 1}&page_size=#{page_size || 10}" : nil
  end

  # @return [Hash]
  def extract_parameters_from_body
    JSON.parse(request_body)
  end

  def request_body
    @request_body ||= request.body.read
  end
end
