class TrainingsService < ServiceContractBase
  post '/trainings.json' do
    begin
      parameters = extract_parameters_from_body
      character_id = parameters["training"]["character_id"]
      product_id = parameters["training"]["product_id"]
      user_id = parameters["training"]["user_id"]

      handler = Rules::Training::Start.new(character_id, product_id, user_id)
      handler.execute
      if handler.worked
        [201, {status: "ok"}.to_json]
      else
        [501, {status: handler.status}.to_json]
      end
    rescue Exception => e
      p e.message
      [500, {error: e.message}.to_json]
    end
  end

  put '/trainings/:training_id.json' do |training_id|
    begin
      parameters      = extract_parameters_from_body
      action_request  = parameters["training"]["action"]

      return [400, {status: "Must contain an action"}.to_json] unless action_request

      action = Rules::Training.const_get(action_request.capitalize).new

      if action
        action.character_training_id = training_id
        action.character_id = parameters['character_id']

        action.execute

        if action.worked
          [200, {status: "ok"}.to_json]
        else
          [500, {status: action.message || "fucked"}.to_json]
        end
      end

    rescue ActiveRecord::RecordNotFound => e
      [404, {errors: e.message}.to_json]
    rescue Exception => e
      p e.message
      [500, {errors: e.message}.to_json]
    end
  end
end
