class TrainingRequest < ActiveRecord::Base
  PUBLIC_FIELDS = [:id]

  serialize :body

  def as_json(options = {})
    options = {} unless options
    super(options.merge(only: PUBLIC_FIELDS))
  end
end
