class Training < ActiveRecord::Base
  PUBLIC_FIELDS = [:id,
                   :product_id,
                   :status]
  RUNNING   = 1
  PAUSED    = 2
  RESTARTED = 3
  BYPASSED  = 4
  STOPPED   = 5

  STATUS_NAMES = {
    RUNNING   => "Running",
    PAUSED    => "Paused",
    RESTARTED => "Restarted",
    BYPASSED  => "Bypassed",
    STOPPED   => ""
  }

  def status_name(item)
    STATUS_NAMES[item]
  end

  has_one :character

  def as_json(options = {})
    options = {} unless options
    super({only: PUBLIC_FIELDS}.merge(options))
  end

  def to_result
    {
      task: {
        body: {
          id: id,
          character_id: character_id,
          expires_on: expires_at,
          type: training_type
        }
      }
    }
  end
end


