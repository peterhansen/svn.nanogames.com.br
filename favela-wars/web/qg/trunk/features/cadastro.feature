# language: pt

@sprint02 @done
Funcionalidade: de usuários
  Como usuário
  Eu gostaria de me cadastrar na Loja

  Cenário: Cadastrando um usuário
    Dado que eu estou na pagina de cadastro
    Quando eu submeto o formulário de login com dados validos
    Então eu devo ir para página inicial

  Cenário: Tentando cadastrar um usuario sem confirmação de senha
    Dado que eu estou na pagina de cadastro
    Quando eu submeto o formulario de login sem a confirmação de senha
    Então eu devo ver 'Password confirmation don't match'
    E meus dados devem continuar preenchidos

  Cenário: Tentando cadastrar um usuario com senha e confirmação diferentes
    Dado que eu estou na pagina de cadastro
    Quando eu submeto o formulario de login com senha e confirmação diferentes
    Então eu devo ver 'Password confirmation don't match'
    E meus dados devem continuar preenchidos

  Cenário: Tentando cadastrar um usuario sem login
    Dado que eu estou na pagina de cadastro
    Quando eu submeto o formulario de login sem login
    Então eu devo ver 'Login can't be blank'
    E meus outros dados devem continuar preenchidos

  Cenário: Tentando cadastrar um usuario sem email
    Dado que eu estou na pagina de cadastro
    Quando eu submeto o formulario de login sem email
    Então eu devo ver 'Email can't be blank'
    E meus dados devem continuar preenchidos

  Cenário: Tentando cadastrar um usuário com um email já utilizado
    Dado que eu estou na pagina de cadastro
    E que existe um usuário com email 'chucknorris@nanostudio.com.br'
    Quando eu submeto o formulário de login com dados validos
    Então eu devo ver 'Email has already been taken'
    E meus dados devem continuar preenchidos

  @wip
  Cenário: Tentando cadastrar um usuário com um login já utilizado
    Dado que eu estou na pagina de cadastro
    E que existe um usuário com login 'chucknorris'
    Quando eu submeto o formulário de login com dados validos
    Então eu devo ver 'Email has already been taken'
    E meus dados devem continuar preenchidos