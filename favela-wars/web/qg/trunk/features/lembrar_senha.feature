@sprint02 @done
Feature: Cadastro de usuários
  Como usuário
  Eu gostaria de lembrar minha senha

  @ws_success
  Scenario: Lembrando minha senha
    Given que eu sou um usuário cadastrado com o email 'chucknorris@nanostudio.com.br'
    And que eu estou na página de login
    When eu clico em 'Esqueci minha senha'
    And preencho meu email com 'chucknorris@nanostudio.com.br'
    Then eu devo ver 'Sua nova senha foi enviada para chucknorris@nanostudio.com.br'
    And eu devo receber um email

  @ws_reset_password_error
  Scenario: Tentando lembrar a senha usando um email errado
    Given que eu sou um usuário cadastrado com o email 'chucknorris@nanostudio.com.br'
    And que eu estou na página de login
    When eu clico em 'Esqueci minha senha'
    And preencho meu email com 'email@errado.com'
    Then eu não devo ver 'Sua nova senha foi enviada para email@errado.com'
    And eu devo ver 'Email incorreto'
    And eu não devo receber um email