# encoding: utf-8
Given /^que eu estou na (\D+)$/ do |page|
  visit path_to(page)
end

When /^eu vou para a (\D+)$/ do |page|
  visit path_to(page)
end

When /^eu preencho '(\w+)' com '(.+)'$/ do |field, data|
  fill_in field, with: data
end

When /^eu aperto '(\D+)'$/ do |name|
  click_on name
end

When /^eu clico em '(\D+)'$/ do |name|
  click_on name
end

Then /^eu devo ir para (\D+)$/ do |page_name|
  current_path.should == path_to(page_name)
end

Then /^eu devo estar na (\D+)$/ do |page_name|
  current_path.should == path_to(page_name)
end

When /^eu devo ver '(\D+)'$/ do |text|
  page.should have_content(text)
end

When /^eu não devo ver '(\D+)'$/ do |text|
  page.should_not have_content(text)
end

When /^meus dados devem continuar preenchidos$/ do
  find('#user_login').value.should_not be_blank
end

When /^meus outros dados devem continuar preenchidos$/ do
  find('#user_email').value.should_not be_blank
end

When /^preencho meu email com '(\D+)'$/ do |email|
  step "eu preencho 'email' com '#{email}'"
  step "eu aperto 'Resetar senha'"
end

When /^eu devo receber um email$/ do
  ActionMailer::Base.deliveries.should_not be_empty
end

When /^eu não devo receber um email$/ do
  ActionMailer::Base.deliveries.should be_empty
end