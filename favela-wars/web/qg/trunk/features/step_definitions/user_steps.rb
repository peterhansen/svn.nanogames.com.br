# encoding: utf-8

When /^eu submeto o formulário de login com dados validos$/ do
  step "eu preencho 'user_login' com 'chucknorris'"
  step "eu preencho 'user_email' com 'chucknorris@nanostudio.com.br'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu preencho 'user_password_confirmation' com 'senha123'"
  step "eu aperto 'register'"
end

When /^eu submeto o formulario de login sem a confirmação de senha$/ do
  step "eu preencho 'user_login' com 'chucknorris'"
  step "eu preencho 'user_email' com 'chucknorris@nanostudio.com.br'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu aperto 'register'"
end

When /^eu submeto o formulario de login com senha e confirmação diferentes$/ do
  step "eu preencho 'user_login' com 'chucknorris'"
  step "eu preencho 'user_email' com 'chucknorris@nanostudio.com.br'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu preencho 'user_password_confirmation' com 'wrong'"
  step "eu aperto 'register'"
end

When /^eu submeto o formulario de login sem email$/ do
  step "eu preencho 'user_login' com 'chucknorris'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu preencho 'user_password_confirmation' com 'senha123'"
  step "eu aperto 'register'"
end

When /^eu submeto o formulario de login sem login$/ do
  step "eu preencho 'user_email' com 'chucknorris@nanogames.com.br'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu preencho 'user_password_confirmation' com 'senha123'"
  step "eu aperto 'register'"
end

When /^que existe um usuário com (\w+) '\D+'$/ do |field|

end

Given /^que eu sou um usuário cadastrado com o login 'chucknorris'$/ do

end

Given /^que eu sou um usuário cadastrado com o email '(\D+)'$/ do |email|
end

When /^eu preencho o formulário de login e submeto$/ do
  step "eu preencho 'login' com 'chucknorris'"
  step "eu preencho 'password' com 'senha123'"
  step "eu aperto 'login'"
end

When /^que eu sou um usuário cadastrado com o login 'joselito'$/ do
end


Given /^que eu estou logado$/ do
  step "que eu estou na página de login"
  step "eu preencho o formulário de login e submeto"
end