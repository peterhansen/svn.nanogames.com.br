module ProductServiceClient
  class Railtie < Rails::Railtie
    initializer 'product_service_client.controller_additions' do
      ActiveSupport.on_load :action_controller do
        include ProductServiceClient
      end
    end
  end
end