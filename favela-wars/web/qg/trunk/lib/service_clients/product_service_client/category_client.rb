# module ProductServiceClient
class CategoryClient < NanoSoa::Client::Base
  resource_name :categories
  self.site = Rails.env.production? ? 'http://products.staging.nanostudio.com.br' :
  'http://localhost:9393'

  def self.all(pageIndex, pageSize)
    collection = get("?include=main_image&pageSize=#{pageSize}&pageIndex=#{pageIndex}")
    Rails.logger.info collection
    collection['categories'].map { |resource| Category.new(resource['category']) }
  end

  def self.all_root
    collection = get('/roots.json')
    collection['categories'].map { |resource| Category.new(resource['category']) }
  end

  def self.find(id)
    entity = get('/' + id.to_s +  '.json')
    Category.new(entity['category'])
  end
end