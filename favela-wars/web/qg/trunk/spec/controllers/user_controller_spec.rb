require_relative '../spec_helper'

describe UsersController do
  describe '#reset_password' do
    describe 'with correct email' do
      it 'should call User.post :reset_password' do
        User.should_receive(:post).with(:reset_password, {}, "{\"email\":\"chucknorris@nanostudio.com.br\"}")
        post :reset_password, email: 'chucknorris@nanostudio.com.br'
      end

      it 'should send email' do
        Notifier.should_receive(:reset_password).with('chucknorris@nanostudio.com.br').and_return(stub(deliver: true))
        post :reset_password, email: 'chucknorris@nanostudio.com.br'
      end
    end
  end
end