Store::Application.routes.draw do
  resources :products

  match 'remember_password', to: 'users#remember_password', as: 'remember_password', via: :get
  match 'reset_password', to: 'users#reset_password', as: 'reset_password', via: :post
  match 'profile', to: 'users#profile', as: 'profile', via: :get
  match 'purchases', to: 'users#purchases', as: 'purchases', via: :get

  resources :users do
    get :profile, on: :collection
    put :update_profile, on: :collection
    post :buy, on: :collection
    get :recharge, on: :collection
    post :add_favorite, on: :collection
  end

  resource :sessions do
    get :delete, on: :collection
  end

  match 'login', to: 'sessions#new', as: 'login', via: :get
  match 'logout', to: 'sessions#delete', as: 'logout', via: :get

  match 'purchase', to: 'pages#purchase', as: 'logout', via: :get

  match 'payments/execute', to: 'payments#execute', via: :post

  match 'return/pagseguro', to: 'payments#return_pagseguro', via: :post

  root :to => 'pages#home', via: :get
end
