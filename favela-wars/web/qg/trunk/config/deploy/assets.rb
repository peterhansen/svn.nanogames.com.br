namespace :deploy do
  namespace :assets do
    desc 'This tasks precompile the assets'
    task :precompile do
      pp 'Precompiling Assets'
      run "cd #{release_path} && bundle exec rake RAILS_ENV=production RAILS_GROUPS=assets assets:precompile"
    end
  end

  after 'deploy:bundle' do
    assets.precompile
  end
end

