class Notifier < ActionMailer::Base
  default from: 'no-reply@nanostudio.com'

  def reset_password(email)
    mail :to => email, :subject => 'New Password'
  end
end