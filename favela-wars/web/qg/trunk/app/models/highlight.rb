class Highlight < NanoSoa::Entity::Base
  include ActionView::Helpers::NumberHelper

  attr_accessor :id, :link, :big_image, :small_image, :created_at, :updated_at
  ASSOCIATIONS = []
end
