class ProductsController < ApplicationController
  before_filter :login_required

  respond_to :json, :html

  def index
    @page_size = 1
    # raise params.merge({page_size: @page_size}).inspect
    @collection = Products::Client.all(params.merge({page_size: @page_size, next_page: params['page']}))
    @current_category = CategoryClient.find(@collection['category']) if @collection['category']
    @collection = WillPaginate::Collection.create(params['page'], 1, @collection['total']) do |pager|
      pager.replace @collection['products']
    end
    respond_with(@collection)
  end

  def show
    response = Products::Client.get_product(params[:id], :include => [:thumb, :properties, :category, :descriptions])
    @product = response["product"]
    
    @product_category = @product["category"]["category"] if @product['category'] #TODO: morra dimas!

    increment = Thread.new do
      Products::Client.increment_view_count(@product.id)
    end

    increment.run
  end

  def add_favorite

  end
end
