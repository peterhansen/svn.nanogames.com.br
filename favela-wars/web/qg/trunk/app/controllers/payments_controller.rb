# encoding: utf-8

class PaymentsController < ApplicationController


    @@email_loja = "eduardo.xavier@bendingbits.com"
    @@pague_seguro_token = "F1B7E36314DB4E3E8D6EF04DD827556E"
    @@service_uri = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9292'

    def create_order(payment_method, pack_credit_id, locale, user_id)



      response = HTTParty.post("#{@@service_uri}/users/#{user_id}/orders/create.json",
                    body: { order:  {
                        pack_credit_id: pack_credit_id ,
                        payment_method: payment_method,
                        locale: locale
                    }}.to_json)

      JSON.parse(response)

    end

    def execute
      #request form
      pack_credit_id =  params[:pack_credit]

      #order the item
      response            = create_order("pagseguro", pack_credit_id, "pt-BR", current_user.id)
      order               = Order.new
      order.id            = response["order"]["id"]
      order.gross_amount  = sprintf("%.2f", response["order"]["gross_amount"])
      product_value       = response["item"]["value"]

      #TODO: DIMAS - colocar chaves do pague seguro num arquivo de configuração  e atribuir o nome do usuário na hora de logar
      sender_name = current_user.name
      if !sender_name.present?
        sender_name = "Floriano Lopes Villas Boas"
      end

      options = {
          :body => {
              :email            => "eduardo.xavier@bendingbits.com",
              :token            => "F1B7E36314DB4E3E8D6EF04DD827556E",
              :currency         => "BRL",
              :itemId1          => pack_credit_id,
              :itemDescription1 => "Moeda Virtual - $#{product_value} Granas",
              :itemAmount1      => order.gross_amount,
              :itemQuantity1    => 1,
              :reference        => order.id,
              :senderName       => sender_name,
              :senderEmail      => current_user.email
          }
      }

      #response = HTTParty.post("http://192.168.1.27:9090/checkout/checkout.jhtml", options)
      response = HTTParty.post("https://ws.pagseguro.uol.com.br/v2/checkout", options)

      logger.info "response do pagseguro: #{response.inspect}"

      redirect_to 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' + response["checkout"]["code"]
    end

    def update_order(order)
      begin
        logger.info "detalhes do pedido: #{order.inspect}"
        logger.info "URL de atualização:  #{@@service_uri}/users/orders/update.json"

        HTTParty.post("#{@@service_uri}/users/orders/update.json",
                      body: { order:  {
                          id: order.id ,
                          status: order.status,
                          net_amount: order.net_amount }}.to_json)

      rescue Exception => e
        logger.info "Erro na atualização do pedido #{e.message}"
      end
    end

    def get_order_reference

      notificationCode = params["notificationCode"]
      #notificationType = params["notificationType"]

      logger.info "https://ws.pagseguro.uol.com.br/v2/transactions/notifications/#{notificationCode}?email=#{@@email_loja}&token=#{@@pague_seguro_token}"

      response = HTTParty.get("https://ws.pagseguro.uol.com.br/v2/transactions/notifications/#{notificationCode}?email=#{@@email_loja}&token=#{@@pague_seguro_token}")
      #response = HTTParty.get("http://localhost:3005/statics/retorno_pa.xml")

      logger.info "detalhes da transação recebida"
      logger.info "Referência: #{response["transaction"]["reference"]}"
      logger.info "Status: #{response["transaction"]["status"]}"
      logger.info "netAmount: #{response["transaction"]["netAmount"]}"

      order = Order.new
      order.id          = response["transaction"]["reference"]
      order.status      = response["transaction"]["status"]
      order.net_amount  = response["transaction"]["netAmount"]

      order
    end

    def return_pagseguro

      order = get_order_reference()

      logger.info "order.status: #{order.status}"

      #increment = Thread.new do

        #---------------------------------------------------------------------------------------------------------
        #como o pagseguro foi o primeiro serviço de integração, usaremos sua lista de código de status.
        #---------------------------------------------------------------------------------------------------------
        # 1	Aguardando pagamento: o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.
        # 2	Em análise: o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.
        # 3	Paga: a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.
        # 4	Disponível: a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.
        # 5	Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.
        # 6	Devolvida: o valor da transação foi devolvido para o comprador.
        # 7	Cancelada: a transação foi cancelada sem ter sido finalizada.
        #---------------------------------------------------------------------------------------------------------

        logger.info "order.status code: #{order.status}"

        update_order(order)
      #end

     # increment.run

      head :ok
    end
end