class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :fetch_categories

  def fetch_categories
    @categories ||= CategoryClient.all_root
  end

  def login_required
    return true if user_logged?
    redirect_to '/login'
  end

  def user_logged?
    session[:current_user].present?
  end
  helper_method :user_logged?

  def current_user
    unless @user
      path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9191'
      response = HTTParty.get("#{path}/users/#{session['current_user'].id}.json?include=favorite_products")

      @user = User.new.from_json response.parsed_response
    end
    @user
  end
  helper_method :current_user
end
