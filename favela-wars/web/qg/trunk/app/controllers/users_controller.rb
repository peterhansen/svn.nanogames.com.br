# encoding: utf-8

class UsersController < ApplicationController
  before_filter :login_required, except: [:new, :create, :remember_password, :reset_password]
  respond_to :html, :json

  def new
    @user = User.new
  end

  def profile
    if params[:user]
      @user = User.new(params[:user])
    else
      @user = User.find(current_user.id)
    end
  end

  def create
    @user = User.create(params['user'])

    unless @user.valid?
      flash.now[:notice] = "Erro de validação: #{@user.errors.full_messages}"
      render :new and return
    end

    redirect_to '/'
  rescue ActiveResource::ResourceConflict => e
    @user = User.new(params['user'])
    @user.errors.from_hash(ActiveSupport::JSON.decode(e.response.body)['errors'])

    flash.now[:notice] = "#{@user.errors.full_messages}"
    render :new
  end

  def add_favorite
    path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9191'
    response = HTTParty.put("#{path}/users/#{current_user.id}.json", body: { user: params[:user] }.to_json)
    head :ok
  end

  def update_profile
    path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9191'
    response = HTTParty.put("#{path}/users/#{current_user.id}.json", body: { user: params[:user] }.to_json)

    if response.code == 409
      @user = User.new(params['user'])
      @user.errors.from_hash(ActiveSupport::JSON.decode(response.parsed_response)['errors'])

      flash[:alert] = @user.errors.messages
      render :profile
    else
      flash[:notice] = 'Perfil atualizado com sucesso'
      redirect_to :profile
    end
  end

  def reset_password
    User.post(:reset_password, {}, {email: params[:email]}.to_json)
    Notifier.reset_password(params[:email]).deliver
    redirect_to root_path, notice: "Sua nova senha foi enviada para #{params[:email]}"
  rescue ActiveResource::ResourceNotFound => e
    flash.now[:notice] = "Email incorreto"
    render :remember_password
  end

  def purchases
    path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9191'
    response = HTTParty.get("#{path}/users/#{current_user.id}/purchases.json")

    @purchases = ActiveSupport::JSON.decode(response.parsed_response)
  end

  def buy
    Rails.logger.info "Entrou no metodo buy"
    path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9191'

    Rails.logger.info "tentando a url #{path}/users/#{current_user.id}/purchases.json"

    response = HTTParty.post("#{path}/users/#{current_user.id}/purchases.json",
                             body: {
                                    purchase: {
                                                  product_id: params[:purchase][:product_id],
                                                  money:     params[:purchase][:money]
                                              }

                             }.to_json)

    Rails.logger.info "Resposta #{response.inspect}"

    if response.code == 505
      respond_with 'Compre mais créditos seu pão duro (eu disse PÃO).', status: 505, location: nil
    else
      respond_with response, location: nil
    end
  end

  def recharge
    path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9292'
    response = HTTParty.put("#{path}/users/#{current_user.id}.json", body: {user: {wallet: current_user.wallet + 100 }}.to_json )
    redirect_to :back
  end
end
