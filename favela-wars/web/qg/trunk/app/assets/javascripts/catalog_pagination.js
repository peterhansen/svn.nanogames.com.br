var smartLoader = {
  total: null,
  nextPage: null,
  showMore: null,
  count: null,
  asset_server_path: null,

  load: function ($objects) {
    smartLoader.total = $('#total').val();
    smartLoader.nextPage = escape($('#next_page').val());
    smartLoader.showMore = document.getElementById("mostrarMais");
    smartLoader.count = $('#lista').children('li').length;
    smartLoader.asset_server_path = $('#env').val() == 'production' ? 'http://products.staging.nanostudio.com.br' : 'http://localhost:9393';

    var objects = $objects;

    $(smartLoader.showMore).click(smartLoader.next);
    execute();

    function execute() {
      var extensionPattern = /\.\w+$/gi;

      //percebe a resolução da tela e constrói o caminho da imagem
      var resolution = getResolution();
      var control = {};

      //carrega as imagens certas de acordo com a resolução
      for (var i = 0; i < objects.length; i++) {
        control = document.getElementById(objects[i].control);

        if (control != null && control !== undefined)
          control.src = setFile(objects[i].src, resolution);
      }

      function setFile(url, size) {
        return url.replace(extensionPattern, size + url.match(extensionPattern));
      }
    }

    function getResolution() {
      //definir os grupos que serão usados com o método screen.availWidth
      var width = 0;
      if (document.documentElement && document.documentElement.clientWidth) {
        width = document.documentElement.clientWidth;
      }
      else if (document.compatMode == 'CSS1Compat' &&
          document.documentElement &&
          document.documentElement.offsetWidth) {
        width = document.documentElement.offsetWidth;
      }
      else if (window.innerWidth && window.innerHeight) {
        width = window.innerWidth;
      }
      if (width >= 0 && width <= 320) {
        return 320;
      }
      else if (width > 320 && width <= 480) {
        return 480;
      }
      else if (width > 480) {
        return 768;
      }
      return 0;
    }
  },

  totalRecords:10,

  sort: function (event) {
    var lista = document.getElementById("lista");

    while (lista.hasChildNodes()) {
      lista.removeChild(lista.lastChild);
    }

    smartLoader.dataBind();
  },

  next: function () {
    smartLoader.dataBind();
    return false;
  },

  dataBind: function () {
    $.getJSON('products.json?next_page=' + smartLoader.nextPage, render);

    function render(response) {
      if (response.next_page == null)
        smartLoader.disableLoadButton();

      smartLoader.nextPage = escape(response.next_page);
      smartLoader.count += response.products.length;

      $('#count').html(smartLoader.count);

      var lista = document.getElementById("lista");

      for (item in response.products) {
        li = createItem();
        li.children[0].src = smartLoader.asset_server_path + response.products[item].thumb;
        li.children[0].width = 80;
        li.children[1].children[0].innerHTML = response.products[item].name;
        li.children[1].children[1].innerHTML = response.products[item].price;
        li.children[1].children[2].href = "/products/" + response.products[item].id;
        lista.appendChild(li);
      }

      function createItem() {
        var li = document.createElement("li"),
            img = document.createElement("img"),
            div = document.createElement("div"),
            nome = document.createElement("p"),
            preco = document.createElement("p"),
            link = document.createElement("a");

        preco.className = "normalPrice";
        div.className = "productInfo";
        link.innerText = "Ver Detalhes";

        div.appendChild(nome);
        div.appendChild(preco);
        div.appendChild(link);

        li.appendChild(img);
        li.appendChild(div);

        return li;
      }
    }
  },

  disableLoadButton: function(){
    button = $(smartLoader.showMore);

    button.addClass('disabled');
    button.off('click');
    button.click(function(e) {
      e.preventDefault();
    });
  }
};
