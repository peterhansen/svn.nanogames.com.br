var details = {
  favorite: {
    label: {added:"Favorito",free:"<span>+</span>Favoritos"},
    path: '/users/add_favorite',
    button: '#buttonAddFavorite'
  },

  load: function(){
    var that = this;
    var buttonFavorite = $(this.favorite.button);
    buttonFavorite.html(this.favorite.label.free);

    $("#pFavorited").html(this.favorite.label.added);

    control_bind();

    function control_bind(){
      buttonFavorite.click(function(e){
        e.preventDefault();

        this.setAttribute("class","buttonDarkBlue buttonDarkBlueDisable");
        this.innerHTML = details.favorite.label.added;
        var path = details.favorite.path;

        try{
          $.ajax({
            type: "post",
            url: path,
            data: {user: { favorites_attributes: [{product_id: this.getAttribute("data-id")}]}},
            timeout: 2500,
            error: function(xhr, errorType) { console.error(errorType);
              unFavorite();
              console.log("deu erro");
              buttonFavorite.off('click');
            }
          });
        }
        catch(e){
          unFavorite();
        }
      });
    }

    function render(response){
      try{
        if (response!="ok")
          unFavorite();
      }
      catch(e){
        unFavorite();
      }
    }

    function unFavorite(){
      buttonFavorite.removeClass('buttonDarkBlue buttonDarkBlueDisable') ;
      buttonFavorite.addClass("buttonDarkBlue");
      buttonFavorite.html(details.favorite.label.free);
    }
  }
};

$(function () {
  var thumb = $('#thumb').val();

  nano.httpImg.server = $('#env').val() == 'production' ? 'http://products.staging.nanostudio.com.br/' : "http://localhost:9393/";
  nano.httpImg.load([
    {"control":"imgMain", "src":thumb}
  ]);

  details.load();

  $('#buy').click(function() {
    var productId = $('#product_id').val();
    var money = this.getAttribute("data-command");
    var path = '/users/buy.json';

    $.ajax({
      type: "post",
      url: path,
      data: {purchase: { product_id: productId, money: money}},
      timeout: 2500,
      success: function(xhr, e) {
        alert('Produto comprado com sucesso.');
        window.location.reload(true);
      },
      error: function(xhr, errorType) {
        alert(xhr.responseText);
        window.location.reload(true);
      }
    });
  });
});

