var listLoader = {
  httpImg:{},
  total:0,
  pageSize:10,
  nextPage:2,
  showMore:null,
  search:null,
  count:null,
  order:null,
  category:null,
  itemsId:1000,

  load:function () {
    var mostrarMais = document.getElementById("mostrarMais");
    mostrarMais.onclick = this.next;

      with(listLoader)
      {
          total = $('#total').val();
          search = $('#search').val();
          count = $('#lista').children('li').length;
          order = $('#order').val();
          category = $('#category').val();
          showMore = mostrarMais;
      }
  },

  next:function () {
    listLoader.dataBind();
    return false;
  },

  dataBind:function () {
    var that = this;

    var path = 'products.json?next_page=' + listLoader.nextPage;

    if (listLoader.search != '')
      path += '&search=' + listLoader.search;

    if (listLoader.order != '')
      path += '&order=' + listLoader.order;

    if (listLoader.category != undefined && listLoader.category != '')
      path += '&category=' + listLoader.category;

    $.getJSON(path, render);

    function render(response) {
      listLoader.nextPage = response.next_page;
      listLoader.count += response.products.length;

      $('#count').html(listLoader.count);

      var lista = document.getElementById("lista");
      var products = response.products;
      var thumbs = [];

      for (item in products) {
        var control_id = "control" + listLoader.itemsId;

        li = createItem(control_id);

        var preco = li.children[1].children[1];
        var discount = products[item].price_discounted;
        preco.innerHTML = products[item].price + ' G';

        li.children[1].children[0].innerHTML = products[item].name;
        li.children[1].children[2].href = "/products/" + products[item].id;

        if (discount !== undefined && discount > 0) {
          li.children[0].children[1].innerHTML = (products[item].discount_term == 1) ? discount + "% desconto" : "Promoção";
          preco.innerHTML = "<span>De: $" + products[item].price + "</span> Por: $" + products[item].price_discounted;
        }
        else {
          li.children[0].removeChild(li.children[0].children[1]);
        }

        lista.appendChild(li);

        thumbs.push({control:control_id, src:products[item].thumb});
        listLoader.itemsId++;
      }

      listLoader.httpImg.load(thumbs);
      listLoader.showMore.disabled = (response.next_page === undefined);

      function createItem(id) {
          with(document)
          {
              var li = createElement("li"),
                  img = createElement("img"),
                  productInfo = createElement("div"),
                  imgWrapper = createElement("div"),
                  stripeSale = createElement("span"),
                  nome = createElement("p"),
                  preco = createElement("p"),
                  link = createElement("a");
          }


        preco.className = "price";
        productInfo.className = "productInfo";
        imgWrapper.className = "imgWrapper";
        link.innerText = "Ver Detalhes";
        link.className = 'buttonLightBlue';
        stripeSale.className = 'stripeSale';
        img.id = id;

        imgWrapper.appendChild(img);
        imgWrapper.appendChild(stripeSale);

        productInfo.appendChild(nome);
        productInfo.appendChild(preco);
        productInfo.appendChild(link);

        li.appendChild(imgWrapper);
        li.appendChild(productInfo);

        return li;
      }

      listLoader.updatePageCounter();
    }
  },

  updatePageCounter: function(){
    if (listLoader.total - listLoader.count == 0) {
      $(listLoader.showMore).addClass('buttonLightBlueDisabled');
      $(listLoader.showMore).html('Não há mais produtos');
    }

    $('#next_page_size').html(Math.min(listLoader.pageSize, (listLoader.total - listLoader.count)))
  }
};

$(function () {
  listLoader.httpImg = nano.httpImg;
  listLoader.load();

  $('#ordenacao').val($('#order').val().replace('%20', ' '));

  $('#ordenacao').on('change', function () {
    var select = $('#ordenacao');
    var value = select.val();
    var queryString = encodeURI('order=' + value);

    if (listLoader.search != '')
      queryString += '&search=' + encodeURI(listLoader.search);

    if (listLoader.category != null)
      queryString += '&category=' + encodeURI(listLoader.category);

    window.location = '/products?' + queryString;
  });

  ////
  // Contador de paginação
  ////
  listLoader.updatePageCounter();
});

