nano.httpImg = {
    server:"",
    load: function($objects)
    {
        var objects = $objects;
        var that = this;

        execute();

        //Percebe a resolução da tela e constrói o caminho da imagem
        function execute()
        {
            var resolution       = getResolution();
            var control          = {};

            for (var i = 0; i < objects.length;i++)
            {
                control = document.getElementById(objects[i].control);

                if (control)
                {
                    control.src = that.server +  "uploads/v" + resolution + "_" + objects[i].src;
                }
            }
        }
        function getResolution()
        {
            //definir os grupos que serão usados com o método screen.availWidth
            var width = 0;
            if (document.documentElement && document.documentElement.clientWidth) {
                width = document.documentElement.clientWidth;
            }
            else if (document.compatMode=='CSS1Compat' &&
                document.documentElement &&
                document.documentElement.offsetWidth ) {
                width = document.documentElement.offsetWidth;
            }
            else if (window.innerWidth && window.innerHeight) {
                width = window.innerWidth;
            }
            if (width >= 0 && width <= 320)
            {
                return 64;
            }
            else if (width > 320 && width <= 480)
            {
                return 128;
            }
            else if (width > 480)
            {
                return 256;
            }
            //480, 600, 768, 992
            return 0;
        }
    }
};
