module ActiveResource
  class Errors < ActiveModel::Errors
    def from_hash(messages, save_cache = false)
      clear unless save_cache

      messages.each do |(key, errors)|
        errors.each do |error|
          if @base.attributes.keys.include?(key)
            add key, error
          else
            self[:base] << "#{key.humanize} #{error}"
          end
        end
      end
    end

    def from_json(json, save_cache = false)
      hash = ActiveSupport::JSON.decode(json)['errors'] || { } rescue { }
      from_hash hash, save_cache
    end
  end
end