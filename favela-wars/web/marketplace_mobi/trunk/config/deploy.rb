require 'bundler/capistrano'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/deploy.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/assets.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/db.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/nginx.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/unicorn.rb'
load File.expand_path(File.dirname(__FILE__)) + '/deploy/helpers.rb'

set :env, 'development'
set :user, 'ubuntu'
set :domain, 'staging.nanostudio.com.br'
set :application, 'storehouse'
set :scm, :subversion
set :deploy_via, :export
set :repository, "http://svn.nanogames.com.br/favela-wars/web/marketplace_mobi/trunk/"
set :scm_username, 'nano_online_svn'
set :scm_password, 'nAnO_SvN123'
set :deploy_to, "/home/#{user}/#{application}"
set :current, "#{deploy_to}/current"
set :keep_releases, 5
set :use_sudo, false

server "#{domain}", :app, :web, :db, :primary => true

ssh_options[:keys] = [File.join(ENV['HOME'], '.ssh', 'amazon-sa.pem')]