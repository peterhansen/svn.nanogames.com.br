@sprint02 @done
Feature: Login de usuários
  Como um usuário
  Eu gostaria de poder logar na loja

  Scenario: Logando
    Given que eu estou na página de login
    And que eu sou um usuário cadastrado com o login 'chucknorris'
    When eu preencho o formulário de login e submeto
    Then eu devo ver 'Bem-vindo, chucknorris'

  @ws_login_error
  Scenario: Tentando logar com um usuário errado
    Given que eu estou na página de login
    And que eu sou um usuário cadastrado com o login 'joselito'
    When eu preencho o formulário de login e submeto
    Then eu devo ver 'Wrong login or password'

  Scenario: Deslogando
    Given que eu sou um usuário cadastrado com o login 'chucknorris'
    And que eu estou logado
    When eu vou para a página de logout
    Then eu devo estar na página inicial
    And eu não devo ver 'Bem-vindo, chucknorris'