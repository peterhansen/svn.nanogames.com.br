class SessionsController < ApplicationController
  before_filter :login_required, except: [:new, :create]

  def new
  end

  def create
    status = User.post 'auth', {}, { credentials: { login: params['login'], password: params['password'] } }.to_json
    session[:current_user] = User.new.from_json status.read_body
    redirect_to root_path
  rescue ActiveResource::ResourceConflict => e
    flash[:notice] = ActiveSupport::JSON.decode(e.response.body)['errors']
    render :new
  end

  def delete
    session[:current_user] = nil
    redirect_to root_path
  end
end
