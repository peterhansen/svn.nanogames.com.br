class PagesController < ApplicationController
  include HTTParty
  before_filter :login_required

  def home
    @products = Products::Client.all("page_size" => 5)["products"]
    @highlights = Products::Client.highlights["highlights"].map! {|highlight| Highlight.new highlight['highlight'] }
  end

  def purchase

      path = Rails.env.production? ? 'http://users.staging.nanostudio.com.br' : 'http://localhost:9191'

      response = HTTParty.get("#{path}/creditspacks.json")

      @granas =  Array.new(4)

      @granas[0] =  GranaPack.new
      @granas[0].id = 1
      @granas[0].price = 15.00
      @granas[0].value = 100

      @granas[1] =  GranaPack.new
      @granas[1].id = 2
      @granas[1].price = 35.00
      @granas[1].value = 800

      @granas[2] =  GranaPack.new
      @granas[2].id = 3
      @granas[2].price = 50.00
      @granas[2].value = 1200

      @granas[3] =  GranaPack.new
      @granas[3].id = 4
      @granas[3].price = 90.00
      @granas[3].value = 10000
  end
end

