module ProductsHelper
  def is_favorite?(product_id)
    current_user.favorite_products.include? product_id
  end
end
