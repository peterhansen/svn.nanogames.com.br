module ApplicationHelper
  def class_for_user_input(user, field)
    classes = []
    classes << 'error' if user.errors.messages.has_key?(field) and !user.errors.messages[field].blank?
    classes << 'locked' if user.persisted?
    classes.join ' '
  end
end
