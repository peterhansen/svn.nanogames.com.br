var slider = {
    frames: ['#img01','#img02','#img03'],
    controls: {},
    frameBorder: "#frame",
    lastFrame: 0,
    actualFrame: 0,
    interval: 5000,
    intervalAfterClick: 10000,
    intervalSameClick: 1000,
    onAnimate: false,
    timerWorks:{},
    reAnimate: function()
    {
        clearTimeout(slider.timerWorks);

        var newFrame = parseInt($(this).text()) - 1;

        if (newFrame == slider.actualFrame)
        {
            animationReturn();
            return;
        }

        if (slider.onAnimate)
        {
            setTimeout("slider.reAnimate()",slider.intervalSameClick);
            return;
        }

        slider.lastFrame   = slider.actualFrame;
        slider.actualFrame = newFrame;
        slider.timerWorks  = null;
        slider.animate();

        animationReturn();

        function animationReturn()
        {
            slider.timerWorks = setTimeout("slider.animateSlide()", slider.intervalAfterClick);
        }
    },
    animateSlide: function()
    {
        var me = slider;

        if (me.actualFrame == me.frames.length - 1)
            me.actualFrame = 0;
        else
            me.actualFrame++;

        me.animate();
    },
    animate: function()
    {
        var me = slider;
        me.onAnimate = true;

        //novo frame aparece
        var actual  = $(me.frames[me.actualFrame]);
        var last    = $(me.frames[me.lastFrame]);

        last.removeAttr('class');
        last.removeAttr('style');
        last.attr('style','display:block;margin-top:0px;position:absolute;z-index:2;');

        actual.removeAttr('class');
        actual.removeAttr('style');
        actual.attr('style','display:block;margin-top:200px;position:absolute;z-index:3;');

        actual.anim({ translateY: '-200px'}, 0.8, 'ease-out', function(){
            last.removeAttr('class');
            last.removeAttr('style');
            last.attr('style','display:none;margin-top:0px;position:absolute');
            me.onAnimate = false;
        });

        $(slider.controls[me.actualFrame]).attr('class','active');
        $(slider.controls[me.lastFrame]).attr('class','');
        me.lastFrame = me.actualFrame;
        if (me.timerWorks != null)
            me.timerWorks = setTimeout("slider.animateSlide()", me.interval);
    },
    run: function ()
    {
        var that = this;
        this.timerWorks = setTimeout("slider.animateSlide()", this.interval);

        slider.controls = $('.control nav ul li');
        slider.controls.click(slider.reAnimate);
    }
};
$(function () {
    slider.run();
});
