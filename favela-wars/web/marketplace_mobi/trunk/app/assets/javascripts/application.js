// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//*=  require zepto/zepto
//*=  require zepto/event
//*=  require zepto/ajax
//*=  require zepto/fx
//*=  require nano
//*=  require nano.httpImg
//*=  require_self

$.extend($.ajaxSettings, {
  beforeSend : function (xhr, settings) {
    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
  }
});
