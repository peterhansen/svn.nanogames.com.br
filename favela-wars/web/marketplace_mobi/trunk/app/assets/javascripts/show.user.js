//*= require lockable_form

$(function() {
  lockableForm.form = 'edit_user_form';
  lockableForm.button = 'edit_button';
  lockableForm.fieldsToSave = ['user_nickname', 'user_login', 'user_email'];

  hasValidationErrors = $('.fail').length == 0;
  lockableForm.init(hasValidationErrors);
});
