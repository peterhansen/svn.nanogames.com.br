var lockableForm = {
  button: '',
  fieldsToSave: [],
  form: '',
  formData: {},
  lockedOnInit: true,
  updating: false,

  init: function(startLocked) {
    if (startLocked != null)
      lockableForm.lockedOnInit = startLocked;

    if (lockableForm.lockedOnInit){
      lockableForm.lockForm();
    } else {
      lockableForm.enableForm();
    }

    $('#' + lockableForm.button).click(lockableForm.onClick);
    lockableForm.saveFormState();
  },

  resetForm: function(){
    for (field in lockableForm.fieldsToSave) {
      var fieldName = lockableForm.fieldsToSave[field];
      $('#' + fieldName).val(lockableForm.formData[fieldName]);
    }
  },

  disableForm: function() {
    lockableForm.resetForm();
    lockableForm.lockForm();
    $('input').addClass('locked');
    $(lockableForm.button).html('Editar');
    lockableForm.updating = false;
  },

  enableForm: function() {
    $('.locked').removeClass('locked');
    $('input[disabled="disabled"]').removeAttr('disabled');
    $('#' + lockableForm.form).children('button[type="submit"]').removeAttr('disabled');
    $('#' + lockableForm.button).html('Cancelar');
    lockableForm.updating = true;
  },

  saveFormState: function() {
    for (field in lockableForm.fieldsToSave) {
      var fieldName = lockableForm.fieldsToSave[field];
      lockableForm.formData[fieldName] = $('#' + fieldName).val();
    }
  },

  lockForm: function() {
    $('input').attr('disabled', 'disabled');
    $('#' + lockableForm.form).children('button[type="submit"]').attr('disabled', 'disabled');
  },

  onClick: function() {
    if (lockableForm.updating) {
      lockableForm.disableForm();
      return;
    }

    lockableForm.enableForm();
  }
}

