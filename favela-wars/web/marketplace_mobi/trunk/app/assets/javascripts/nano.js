/**
 * Created by JetBrains RubyMine.
 * User: eduardo xavier
 * Date: 3/19/12
 * Time: 2:51 PM
 */
var nano = {};

nano.menu = {
    contents: [{'id':'#categoryContent', 'height':'120'}, {'id':'#searchContent', 'height':'90'}],
    lineBody: "header nav",
    triggers: ['bt_busca','bt_minha_conta','bt_Comprar','search_box','bodyLine','navSearch','ordenacao','a_comprar','bt_Comprar'],
    active: {},
    activate : function()
    {
        $("#buyButton").click(function(){
            nano.menu.show(nano.menu.contents[0]);
        });

        $("#searchButton").click(function(){
            nano.menu.show(nano.menu.contents[1]);
        });
        $("#search_box").click(function(){
            $(this).val("");
        });

        $("#headFav").click(function(e){
            nano.menu.blur(e.target);
        });
        $(".pageContent").click(function(e){
            nano.menu.blur(e.target);
        });
    },
    blur: function(target)   //e.target
    {
        if (target.id.indexOf('cat') != -1)
            return;

        triggers = nano.menu.triggers;

        for (var i = 0; i < triggers.length; i++) {
            if (target.id==triggers[i])
                return;
        }

        nano.menu.closeAll('');
    },
    show: function(menu)
    {
        var sender = $(menu.id);

        nano.menu.closeAll(menu.id);

        var body   = $(nano.menu.lineBody);

        if (sender.css('height') == '0px')
        {
            if (body.css("visibility")=="hidden")
                body.css("visibility","visible");

            sender.css('height','0px');
            sender.anim({height: menu.height + 'px'}, 0.3, 'ease-out');
            $("select").hide();
        }
        else
        {
            sender.css('height','0px');
            body.css("visibility","hidden");
            $("select").show();
        }
    },
    closeAll: function(but)
    {
        $("select").show();
        for (var item in this.contents)
        {
            if (but==this.contents[item].id)
                continue;

            var sender = $(this.contents[item].id);
            var actualHeight = sender.css('height');

            if (actualHeight !== undefined && actualHeight != '0px')
            {
                //nano.menu.close(sender);
                sender.css('height','0px');
            }
        }
        $(nano.menu.lineBody).css("visibility","hidden");
    }
};

$(function () {
    nano.menu.activate();
});