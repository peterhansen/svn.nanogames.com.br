class User < ActiveResource::Base
  self.site = Rails.env.production? ? 'http://users.staging.nanostudio.com.br/' :
                                      'http://localhost:9191/'
  schema do
    string 'login', 'email', 'password', 'nickname', 'favorite_products', 'wallet'
  end

  validates_presence_of [:login, :email]
  validates :email,     format: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/
  validates_confirmation_of :password, if: lambda {|user| user.password.present? }
  validates_presence_of :password_confirmation, if:
      lambda { |user|
        unless user.attributes.has_key?(:id)
          password_confirmation = user.attributes['password_confirmation']
          user.attributes.delete('password_confirmation')
          user.password.present? and (user.password != password_confirmation)
        end
      }, message: "don't match"

  def persisted?
    id.present?
  end
end
