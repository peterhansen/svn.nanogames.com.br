class Category < NanoSoa::Entity::Base
  SITE = Rails.env.production? ? 'http://products.staging.nanostudio.com.br' : 'http://localhost:9393'
  RESOURCE_NAME = name.downcase.pluralize
  PREFIX = "/#{RESOURCE_NAME}"
  attr_accessor :id, :name, :parent_id, :position, :slug, :parent_id

  #def self.all(page = 1, page_size = 10)
  #  response = HTTParty.get(SITE + PREFIX + FORMAT + "?pageSize=#{page_size}&pageIndex=#{page}")
  #
  #  collection = JSON.parse response.parsed_response
  #  Rails.logger.info collection
  #  collection['categories'].map { |resource| Category.new(resource['category']) }
  #end
  #
  #def self.find(id)
  #  path = Category::SITE + Category::PREFIX + '/' + id.to_s + Category::FORMAT
  #
  #  response = HTTParty.get(path)
  #  category = JSON.parse response.parsed_response
  #  Category.new(category['category'])
  #end
  #
  #def self.roots
  #  response = HTTParty.get(SITE + PREFIX + '/roots' + FORMAT)
  #  collection = JSON.parse response.parsed_response
  #  collection['categories'].map { |resource| Category.new(resource['category']) }
  #end

  #def self.all_top
  #  response = HTTParty.get(SITE + PREFIX + FORMAT)
  #  collection = JSON.parse response.parsed_response
  #  collection['categories'].map { |resource| Category.new(resource['category']) }
  #end
end