# encoding: utf-8

class ThumbUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    'uploads'
  end
end
