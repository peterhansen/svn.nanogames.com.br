class HighlightDecorator < ApplicationDecorator
  decorates :highlight
  include Draper::LazyHelpers

  def image_thumbs(size)
    p = content_tag :legend, "Imagem tela #{t size}:"
    image = highlight.send("#{size}_image")

    content = if image && image.url.present?
      render partial: 'images/thumbs', locals: {
          id: "#{highlight.id}_#{size}",
          name: self.name(size),
          image_host: Highlight.site,
          delete_link: "/highlights/#{highlight.id}/remove_#{size}_image",
          regenerate_link: "/highlights/#{highlight.id}/regenerate_#{size}_image",
          images: highlight.thumbs.send(size),
          main_image: highlight.send("#{size}_image").attributes.delete(:url)
      }
    else
      content_tag :div, class: 'imageButtons' do
        link_to 'Adicionar Imagem', '#', class: "linkAdd add_image", 'data-id' => "#{highlight.id.to_s}_#{size}"
      end
    end

    p + content
  end

  def name(size)
    highlight.send("#{size}_image").url.match /\/uploads\/(.+)/
    $1
  end
end
