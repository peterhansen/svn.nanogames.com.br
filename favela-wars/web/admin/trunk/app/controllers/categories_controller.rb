# encoding: utf-8

class CategoriesController < ApplicationController
  DEFAULT_PAGE_SIZE = 10

  def index
    categories_request = ::HTTParty.get("#{Category.site}categories.json?include=parent_name&page=#{params[:page] || 1}&page_size=#{params[:page_size] || 50}")
    categories_request = JSON.parse categories_request.parsed_response
    @total = categories_request['total']
    @current = params[:page]
    @categories = categories_request['categories'].map{ |category| Category.new category['category']}
    @next = categories_request['next_page']
    @previous = categories_request['previous_page']
  end

  def new
    @category = Category.new
    @categories = Category.all['categories']
    @collection_category = ["Heavy MachineGun", "Stiling", "Frozen Gun", "Macumber"]
  end

  def show
    response = Category.get(params[:id], :include => 'descriptions')
    @category = Category.new(response)
  rescue ActiveResource::ResourceNotFound => e
    redirect_to products_path, :alert =>'Product not found'
  end

  def create
    @category = Category.new(params[:category])
    @category.save
    redirect_to edit_category_path(@category), :notice => "Categoria '#{@category.name}' criada com sucesso."
  rescue ActiveResource::ResourceConflict => e
    @categories = Category.all['categories']
    @descriptions_attributes = params[:category][:descriptions_attributes]
    @category.errors.from_hash(ActiveSupport::JSON.decode(e.response.body)['errors'])
    #raise @category.errors.map{|key, value| value}.inspect
    flash.now[:alert] = @category.errors.map{|key, value| value}
    render :new
  end

  def edit
    # response = Category.get(params[:id], include: 'descriptions')
    response = JSON.parse ::HTTParty.get("#{Products::Client.site}/categories/#{params[:id]}.json?include=descriptions")
    @category = Category.new(response)
    @categories = @category.get_parent_candidates
  end

  def update
    @category = Category.find(params[:id])
    @category.update_attributes(params[:category])
    redirect_to categories_path, :notice => 'Categoria atualizada com sucesso'
  rescue ActiveResource::ResourceConflict => e
    response = Category.get(params[:id], :include => 'descriptions')
    @category = Category.new(response)
    @categories = Category.all['categories']
    @category.errors.from_hash(ActiveSupport::JSON.decode(e.response.body)['errors'])
    flash.now[:alert] = @category.errors.map{|key, value| value}
    render :edit
  end

  def destroy
    @category = Category.find(params[:id])

    if @category
      @category.destroy
      Category.refresh_cache
      redirect_to categories_path, :notice => "Categoria '#{@category.name}' deletada com sucesso."
    else
      flash[:alert] = 'Não foi possivel deletar a categoria.'
      render :edit
    end
  end
end