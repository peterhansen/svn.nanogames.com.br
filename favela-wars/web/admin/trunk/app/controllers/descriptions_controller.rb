class DescriptionsController < ApplicationController
  def destroy
    Description.find(params[:id]).destroy
    redirect_to :back
  end

  def update
    Description.find(params[:id]).update_attributes(params[:description])
    redirect_to :back
  end
end