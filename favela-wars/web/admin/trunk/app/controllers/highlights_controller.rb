class HighlightsController < ApplicationController
  ADMIN_HOST = Rails.env.development? ? "http://localhost:3000" : "http://admin.staging.nanostudio.com.br"

  def index
    request = ::HTTParty.get "#{Highlight.site}/highlights.json?include=thumbs"
    response = JSON.parse request.parsed_response
    @highlights = HighlightDecorator.decorate(response['highlights'].map { |product| Highlight.new product['highlight'] })
  end

  def update
    @highlight = Highlight.find(params[:id])
    @highlight.update_attributes(params[:highlight])
    redirect_to "/highlights?ih=#{params[:id]}", notice: 'Destaque atualizado com sucesso'
  end

  def remove_big_image
    remove_image :big
  end

  def remove_small_image
    remove_image :small
  end

  def regenerate_small_image
    ::HTTParty.post "#{Highlight.site}/highlights/#{params[:id]}/images/small/regenerate.json"
    redirect_to "/highlights?ih=#{params[:id]}", notice: 'Imagem regerada com sucesso'
  end

  def regenerate_big_image
    ::HTTParty.post "#{Highlight.site}/highlights/#{params[:id]}/images/big/regenerate.json"
    redirect_to "/highlights?ih=#{params[:id]}", notice: 'Imagem regerada com sucesso'
  end

  def remove_thumb
    ::HTTParty.delete "#{Highlight.site}/highlights/#{params[:id]}/#{params[:image_size]}_image/#{params[:thumb]}.json"
    redirect_to "/highlights?ih=#{params[:id]}", notice: 'Thumbnail removido com sucesso'
  end

  def replace_thumb
    uploader = ImageUploader.new
    uploader.store!(params[:datafile])

    ::HTTParty.put("#{Highlight.site}/highlights/#{params[:id]}/images/#{params[:highlight_size]}/#{params[:thumb_size]}.json",
                   body: {highlight: {"remote_#{params[:highlight_size]}_image_url" => "#{ADMIN_HOST}#{uploader.url}"}}.to_json)
    redirect_to "/highlights?ih=#{params[:id]}", notice: 'Thumbnail substituido com sucesso'
  end

private

  def remove_image(size)
    ::HTTParty.put "#{Highlight.site}/highlights/#{params[:id]}.json",
    body: {highlight: {"remove_#{size}_image" => true}}.to_json
    redirect_to "/highlights?ih=#{params[:id]}", notice: 'Imagem removida com sucesso'
  end
end
