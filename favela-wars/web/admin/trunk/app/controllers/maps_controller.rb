class MapsController < ApplicationController
  def index
    @map_collection = JSON.parse Game::Client.get_maps_filtered(published: params[:published] || false)
  end

  def update
    if params['map']['published'] == 'true'
      #response = Game::Client.update_map(params[:id], {published: true}
      response = Game::Client.publish_map(params[:id])
    else
      response = Game::Client.unpublish_map(params[:id])
    end

    redirect_to "/maps?published=#{params['published']}"
  end
end
