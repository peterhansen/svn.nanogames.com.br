class ImagesController < ApplicationController
  PRODUCTS_HOST = Rails.env.development? ? "http://localhost:9393" : "http://products.staging.nanostudio.com.br"
  ADMIN_HOST = Rails.env.development? ? "http://localhost:3000" : "http://admin.staging.nanostudio.com.br"

  def create
    uploader = ImageUploader.new
    uploader.store!(params[:datafile])

    Image.create remote_image_file_url: ADMIN_HOST + uploader.url,
                 product_id: params[:product_id],
                 generate_thumbs: params[:generate_thumbs]

    redirect_to :back, notice: 'Imagem cadastrada com sucesso'
  end

  def remove_thumbs
    image = Image.find(params[:id])
    image.post(:remove_thumbs)
    redirect_to :back
  end

  def refresh_thumbs
    image = Image.find(params[:id])
    image.post(:refresh_thumbs)
    redirect_to :back
  end

  def replace_thumb
    uploader = ThumbUploader.new
    uploader.store!(params[:datafile])
    Image.post("#{params[:image_id]}/thumbs/#{params[:thumb_size]}",
               {},
               {remote_image_file_url: () + uploader.url}.to_json)
    redirect_to :back
  end

  def create_highlight_big_image
    uploader = ImageUploader.new
    uploader.store!(params[:datafile])

    ::HTTParty.put("#{PRODUCTS_HOST}/highlights/#{params[:highlight_id]}.json",
                   body: {highlight: {remote_big_image_url: "#{ADMIN_HOST}#{uploader.url}"}}.to_json)

    redirect_to "/highlights?ih=#{params[:highlight_id]}", notice: 'Imagem cadastrada com sucesso'
  end

  def create_highlight_small_image
    uploader = ImageUploader.new
    uploader.store!(params[:datafile])

    ::HTTParty.put("#{PRODUCTS_HOST}/highlights/#{params[:highlight_id]}.json",
                   body: {highlight: {remote_small_image_url: "#{ADMIN_HOST}#{uploader.url}"}}.to_json)

    redirect_to "/highlights?ih=#{params[:highlight_id]}", notice: 'Imagem cadastrada com sucesso'
  end
end
