# encoding: utf-8

class ProductsController < ApplicationController
 PRODUCTS_PER_PAGE = 50

 def index
  products_request = ::HTTParty.get("#{Products::Client.site}/products.json?include=category_name&page=#{params[:page] || 1}&page_size=#{params[:page_size] || 50}")
  products_response = JSON.parse products_request.parsed_response
  @total = products_response['total']
  @current = params[:page]
  @products = products_response['products'].map{ |product| Product.new product['product']}
  @next = products_response['next_page']
  @previous = products_response['previous_page']
end

def new
  @product = Product.new
  response = ::HTTParty.get("#{Products::Client.site}/categories/get_all_nested.json") 
  @categories = JSON.parse response

  tags_request = ::HTTParty.get("#{Products::Client.site}/tags.json")
  tags_response = JSON.parse tags_request.parsed_response
  @tags = tags_response['tags'].map { |tag| tag['tag']['name'] }
end

def show
  response    = Products::Client.get_product(params[:id], include: [:descriptions, :images])
  @product    = Product.new(response)
  @categories = Category.get(:get_all_nested)
  @tags = Tag.all.map { |tag| tag.name }
rescue Exception => e
  redirect_to products_path, notice: 'Product not found'
end

def create
  params[:product][:descriptions_attributes] = [params[:product][:descriptions_attributes]]
  @product = Products::Client.create_product params[:product].merge(type: 'Product')
  redirect_to "/products/#{@product['product']}", notice: 'Produto criado com sucesso'
rescue Exception => e
  @descriptions_attributes = params[:product][:descriptions_attributes]
  @product.errors.from_hash(ActiveSupport::JSON.decode(e.response.body)['errors'])
  flash.now[:alert] = @product.errors.map{|key, value| value}
  render :new
end

def edit
  includes = [:descriptions, :tag_list, :images, :thumbs, :main_image_id, :assets]
  product_response = Products::Client.get_product(params[:id], include: includes)
  tags_request = ::HTTParty.get("#{Products::Client.site}/tags.json")
  tags_response = JSON.parse tags_request.parsed_response
  @tags = tags_response['tags'].map { |tag| tag['tag']['name'] }
  categories_response = ::HTTParty.get("#{Products::Client.site}/categories/get_all_nested.json") 
  @categories = JSON.parse(categories_response || {categories: []}.to_json)
  @product = Product.new(product_response['product'])
  @assets = JSON.parse HTTParty.get("#{Game::Client.site}/assets.json").parsed_response
end

def update
    @product = ::HTTParty.put(
      "#{Products::Client.site}/products/#{params[:id]}",
      body: {product: params[:product]}.to_json
    )

    redirect_to products_path, notice: 'Produto atualizado com sucesso'
  rescue ActiveResource::ResourceConflict => e
    @categories = Category.get(:get_all_nested)
    tags_request = ::HTTParty.get("#{Tag.site}tags.json")
    tags_response = JSON.parse tags_request.parsed_response
    @tags = tags_response['tags'].map { |tag| tag['tag']['name'] }
    @descriptions_attributes = params[:product][:descriptions_attributes]
    @product.errors.from_hash(ActiveSupport::JSON.decode(e.response.body)['errors'])
    flash.now[:alert] = @product.errors.map{|key, value| value}
    render :edit
  end

  def refresh_thumbs
    product = Product.find(params[:id])
    product.post(:refresh_thumbs)
    redirect_to :back
  end

  def add_asset
    response = Products::Client.add_asset_to_product(params[:asset_id], params[:id])

    if response.success?
      flash[:notice] = 'Asset adicionado com sucesso.'
    else
      flash[:alert] = 'O asset não foi adicionado ao produto.'
    end

    redirect_to "/products/#{params[:id]}/edit"
  end

  def remove_asset
    Products::Client.remove_asset_from_product(params[:asset_id], params[:id])
    redirect_to "/products/#{params[:id]}/edit"
  end
end
