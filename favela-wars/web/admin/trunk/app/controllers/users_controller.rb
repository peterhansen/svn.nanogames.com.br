# encoding: utf-8

class UsersController < ApplicationController
  USERS_PER_PAGE = 10

  def index
    # TODO Funcionalidade de paginação não está implementada no serviço
    users_request = ::HTTParty.get("#{User.site}users.json?page=#{params[:page] || 1}&page_size=#{params[:page_size] || 50}")
    users_response = JSON.parse users_request.parsed_response
    @total = users_response['total']
    @current = params[:page]
    @users = users_response['users'].map{ |user| User.new user['user']}
    @next = users_response['next_page']
    @previous = users_response['previous_page']

    @count = User.get(:count)
    #@users = WillPaginate::Collection.create params[:page] || 1, USERS_PER_PAGE, @count do |pager|
    #  pager.replace User.find(:all, params: {page: params[:page] || 1, page_size: USERS_PER_PAGE})
    #end
  end

  def new
    @user = User.new
  end

  def show
    @user = User.find(params[:id])
  rescue
    redirect_to users_path, notice: 'User not found'
  end

  def edit
    @user = User.find(params[:id])
  rescue Exception => e
    redirect_to users_path, notice: 'User not found'
  end

  def create
    User.create(params[:user])
    redirect_to users_path, notice: 'Usuário criado com sucesso'
  rescue ActiveResource::ResourceConflict => e
    @user = User.new(params[:user])
    @user.errors.from_hash(ActiveSupport::JSON.decode(e.response.body)['errors'])
    flash.now[:alert] = @user.errors.full_messages
    render :new
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(params[:user])
      redirect_to users_path, notice: "Usuário '#{@user.login}' atualizado com sucesso."
    else
      flash.now[:alert] = 'Ocorreu um erro ao atualizar o usuário.'
      render :edit
    end
  end
end
