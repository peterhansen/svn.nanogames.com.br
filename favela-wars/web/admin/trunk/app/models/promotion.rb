class Promotion < ActiveResource::Base
  schema do
    string :name, :type, :discount, :discount_value, :categories, :weapons, :start_day, :start_hour, :end_day, :end_hour
  end
end