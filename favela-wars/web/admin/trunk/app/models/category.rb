class Category < ActiveResource::Base
  self.site = Rails.env.production? ? 'http://products.staging.nanostudio.com.br/' : 'http://localhost:9393/'

  schema do
    integer 'id'
    string 'name'
    string 'slug'
    integer 'position'
    integer 'parent_id'
    string 'descriptions'
  end

  def self.all
    request = ::HTTParty.get("#{site}categories.json?include=parent_name")
    response = JSON.parse request.parsed_response
    response['categories'].map! { |category| Category.new category['category'] }
    response
  end

  def self.from_error(resquest_body)
    category = Category.new
    category.errors.from_hash(ActiveSupport::JSON.decode(resquest_body)['errors'])
    category
  end

  def self.refresh_cache
    @all = nil
  end

  def persisted?
    self.id.present?
  end

  def get_parent_candidates
    request = ::HTTParty.get("#{self.class.site}categories/#{id}/parent_candidates.json")
    response = JSON.parse request.parsed_response
    #raise request.inspect
    response.map! { |category| Category.new category['category'] }
    response
  end
end