class Highlight < ActiveResource::Base
  self.site = Rails.env.production? ? 'http://products.staging.nanostudio.com.br' : 'http://localhost:9393'

  schema do
    string :small_image, :big_image
  end

  def big_image_url
    "#{self.class.site}#{big_image.url}"
  end

  def small_image_url
    "#{self.class.site}#{small_image.url}"
  end

  def image_url_for(image)
    "#{self.class.site}#{image}"
  end
end
