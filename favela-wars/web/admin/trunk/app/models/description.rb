class Description < ActiveResource::Base
  self.site = Rails.env.development? ? 'http://localhost:9393/' : 'http://products.staging.nanostudio.com.br/'

  schema do
    string 'locale', 'title', 'shorted', 'complete'
  end

  def as_json(options)
    super()
  end

  private

  def load(attributes, remove_root = false)
    #raise :hell
    super attributes, true
  end
end