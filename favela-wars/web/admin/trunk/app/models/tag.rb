class Tag < ActiveResource::Base
  self.site = Rails.env.production? ? 'http://products.staging.nanostudio.com.br/' : 'http://localhost:9393/'

  schema do
    string 'name'
    string 'slug'
  end
end