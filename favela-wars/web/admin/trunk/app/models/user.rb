class User < ActiveResource::Base
  self.site = Rails.env.production? ? 'http://users.staging.nanostudio.com.br/' :
  																		'http://localhost:9191/'

  schema do
    string 'login', 'email', 'password', 'favorite_products'
  end

  def persisted?
    self.id.present?
  end

  def self.find_by_id_or_name(param)
    where("users.name = '?' or user.id = '?'", param, param)
  end
end
