module ApplicationHelper
  def asset_host
    Rails.env.development? ? "http://localhost:9393" : "http://products.staging.nanostudio.com.br"
  end

  def thumb_to(image, size = nil)
    url = image.attributes['image_file'].url
    return url unless size
    url.match /(\/\w+\/)(\S+)/
    path = $1
    filename = $2
    "#{path}thumb_v#{size}_#{filename}"
  end

  def title(title)
    content_for :title, title || "Admin"
  end
end
