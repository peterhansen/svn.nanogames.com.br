//= require form

var imageWork = {
  product_service_url: $('#env').val() == 'production' ? 'http://products.staging.nanostudio.com.br' : 'http://localhost:9393',

  OnRemoveImage:{},
  OnReplaceImage:{},

  activate:function () {
    //working setup
    var modalOption = null;

    var onContext = function () {
      //redesenhando quando for necessário..
      if (modalOption != null)
        removeScreen(modalOption);

      modalOption = createScreen(this);

      var e = window.event;
      if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
      }
      else if (e.clientX || e.clientY) {
        posx = e.clientX + document.body.scrollLeft
            + document.documentElement.scrollLeft;
        posy = e.clientY + document.body.scrollTop
            + document.documentElement.scrollTop;
      }

      modalOption.style.top = ( posy) + 'px';
      modalOption.style.left = ( posx - 150) + 'px';
      document.body.appendChild(modalOption);
      e.preventDefault();
    };

    var thumbnails = $("fieldset.thumbnail ul li img");

    for (var i = 0, j = thumbnails.length; i < j; i++)
    {
        with(thumbnails[i])
        {
            onclick = onContext;
            oncontextmenu = onContext
        }
    }


    function removeScreen(sender) {
      if (sender == null)
        return;

      document.body.removeChild(modalOption);
      modalOption = null;
    }

    function createScreen(sender) {
      var onImageOut = function () {
        if (modalOption === undefined && modalOption === null)
          return;

        removeScreen(this);
      };
      var onRemove = function (e) {

        with (document)
        {
            var liReference     = getElementById(this.getAttribute('data-reference')),
                progressBar     = createElement("img"),
                progressWrapper = createElement("div");
        }

        liReference.style.opacity = "0.50";
        progressBar.src = "img/loader-4.gif";

        progressWrapper.className = "loader";
        progressWrapper.appendChild(progressBar);

        liReference.insertBefore(progressWrapper, liReference.lastChild);

        imageWork.OnRemoveImage(e.target, function (success) {
          if (success) {
            liReference.parentNode.removeChild(liReference);
          } else {
            liReference.style.opacity = "1";
          }
        });

          progressWrapper.parentNode.removeChild(progessWrapper);
          progressWrapper = null;
        e.preventDefault();
      };
      var onReplace = function (e) {
        imageWork.OnReplaceImage(e.target);

        e.preventDefault();
      }
      var onAreaOut = function(e)
      {
          e = event.toElement || event.relatedTarget;
          if (e.parentNode == this || e == this) {
              return;
          }
          removeScreen(this);
      }

      var dataImageId   = sender.parentNode.parentNode.parentNode.getAttribute('data-image-id');
      var liSender      = sender.parentNode;
      var dataThumbSize = liSender.getAttribute('data-thumb-size');


        with(document)
        {
            var remove = createElement("a");
            var replace = createElement("a");
            var area = createElement("div");
        }
        with(remove)
        {
            appendChild(document.createTextNode("excluir"));
            onclick = onRemove;
            setAttribute('data-image-id', dataImageId);
            setAttribute('data-thumb-size', dataThumbSize);
            setAttribute('data-reference', liSender.id);
            href = "#";
        }
        with(replace)
        {
            appendChild(document.createTextNode("trocar imagem"));
            onclick = onReplace;
            setAttribute('data-image-id', dataImageId);
            setAttribute('data-thumb-size', dataThumbSize);
            setAttribute('data-reference', liSender.id);
            href = "#";
        }
        with(area)
        {
            className = "links";
            onclick = onImageOut;
            onmouseout = onAreaOut;
            appendChild(replace);
            appendChild(remove);
        }
        return area;
    }
  }
}

// Exibe o formulário de upload de imagens.
//
// Essa função utiliza o 'data-id' do link que foi clicado para identificar o
// overlay que deverá ser exibido (que deverá possuir o mesmo 'data-id').
$('.add_image').click(function (e) {
  var id = $(e.target).data('id');

  $('.image_overlay[data-id=' + id + ']').show();
  e.preventDefault();
});

// Cancela o upload e oculta o formulário
$('.cancel_upload').click(function (event) {
  $('.overlay').fadeOut('fast');
  $('.upload_image').each(function () {
    this.reset();
  });
  disableSubmit();
  event.preventDefault();
})
