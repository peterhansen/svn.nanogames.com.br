// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery-ui-1.8.core-and-interactions.min
//= require jquery-ui-1.8.autocomplete.min
//= require jquery-ui-1.8.tag-it
//= require jquery.validate

////
// Re-abilitar botão de enviar imagem
////
$('.datafile').live('change', function (e) {
  $(e.target).parents('form').find(':submit').attr('disabled', false);
});

////
// Reabilitar submit de form após interação
////
function disableSubmit(){
  var saveButton = $('form.dynamic :submit');
  saveButton.attr('disabled', true);
}
disableSubmit();

var enableForm = function (e) {
  $(e.target).valid();
  $(e.target).parents('form').find(':submit').attr('disabled', false);
};

$('form').find('input').live('change', enableForm);
$('form').find('input').live('keyup', enableForm);
$('form').find('textarea').live('keyup', enableForm);
$('form').find('select').live('change', enableForm);