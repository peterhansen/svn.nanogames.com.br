$(function () {
  $('.cancel').click(function (event) {
    $('.overlay').fadeOut('fast');
    event.preventDefault();
  })

  $('#new_description_button').click(function (event) {
    $('#method').val('post');
    $('#new_description_overlay').fadeIn('fast');
    event.preventDefault();
  });

  $('.edit_description').live('click', function (event) {
    $('#method').val('put');

    var tr = $(event.target).parent().parent();
    var id = $(event.target).parent().parent().attr('description-id-data');
    var locale = tr.find('.locale').html();
    var title = tr.find('.title').html();
    var shorted = tr.find('.shorted').html();
    var complete = tr.find('.complete').html();

    $("#new_description").find('#description_id').val(id);
    $("#new_description").find('#description_locale').val(locale);
    $("#new_description").find('#description_title').val(title);
    $("#new_description").find('#description_shorted').val(shorted);
    $("#new_description").find('#description_complete').val(complete);

    $('#new_description_overlay').fadeIn('fast');
    event.preventDefault();
  });

  $('#buttonSave').live('click', function (event) {
    event.preventDefault();

    var dataContract = {
      description:{
        descriptible_id:$("#descriptible_id").val(),
        descriptible_type:$("#descriptible_type").val(),
        shorted:$("#description_shorted").val(),
        complete:$("#description_complete").val(),
        locale:$("#description_locale").val(),
        title:$("#description_title").val()
      }
    };

    var server = $('#env').val() == 'production' ? 'http://products.staging.nanostudio.com.br' : 'http://localhost:9393';

    if ($('#method').val() == 'put') {
      $.ajax({
        url:server + "/descriptions/" + $('#description_id').val() + '.json',
        data:JSON.stringify(dataContract),
        type:'PUT',
        dataType:'json'
      }).success(updateDescription).error(onError);
    } else {
      $.post(server + "/descriptions", JSON.stringify(dataContract)).success(addDescription).error(onError);
    }

    function updateDescription(data) {
      var tr = $('tr[description-id-data=' + data.description.id + ']');

      tr.find('.locale').html(data.description.locale);
      tr.find('.title').html(data.description.title);
      tr.find('.shorted').html(data.description.shorted);
      tr.find('.complete').html(data.description.complete);

      hideDescriptionForm();

      $('#description_updated').slideDown();

      setTimeout(function () {
        $('#description_updated').slideUp();
      }, 3000);
    }

    function addDescription(data) {
      var data = JSON.parse(data);
      var description_id = data.description.id;

      $('#no_description').hide();
      var table = $("#table_descriptions");
      var tr = $("<tr description-id-data=" + description_id + "/>");

      addRow(dataContract.description.locale, 'locale');
      addRow(dataContract.description.title, 'title');
      addRow(dataContract.description.shorted, 'shorted');
      addRow(dataContract.description.complete, 'complete');
      addRow("<a href='#' class='edit_description'>editar</a>", 'actions');
      addRow("<a href='/descriptions/" + description_id + "' data-confirm='Tem certeza?' data-method='delete' rel='nofollow'>deletar</a>", 'actions');
      table.append(tr);

      function addRow(value, klass) {
        var td = $("<td class='" + klass + "'/>");
        td.append(value);
        tr.append(td);
      }

      hideDescriptionForm();

      $('#description_created').slideDown();

      setTimeout(function () {
        $('#description_created').slideUp();
      }, 3000);
    }

    function hideDescriptionForm() {
      $('.overlay').fadeOut('fast');
      $('.description_error_area').hide();
      $('#new_description :input[type=text]').val("");
      $('#new_description textarea').val("");
    }

    function onError(data) {
      response = JSON.parse(data.responseText);

      if (response.errors)
        showErrors(response.errors);
    }

    function showErrors(errors) {
      clearErrors();

      for (error in errors) {
        var p = newDescriptionError(errors[error][0]);
        $('.description_error_area').append(p);
      }

      $('.description_error_area').show();
    }

    function clearErrors() {
      var element = $('#description_error_template').clone();
      $('.description_error_area').html('');
      $('.description_error_area').append(element);
    }

    function newDescriptionError(message) {
      var element = $('#description_error_template').clone();
      element.html(message);
      $(element).show();
      return element;
    }
  });


  ////
  // Habititando formulário após interação
  ////

  function enableSubmit() {
    if ($('.description_submit_form').attr('disabled') == 'disabled')
      $('.description_submit_form').attr('disabled', false);
  }

  $('.description_submit_form').attr('disabled', true);
  $('.description_form').find('input').live('keyup', enableSubmit);
  $('.description_form').live('change', enableSubmit);
});