//= require form
//= require form.images

window.onload = function () {
  imageWork.activate();

  ///sender: a tag que enviou acionou a remoção
  imageWork.OnRemoveImage = function (sender, callback) {
    var imageId = sender.getAttribute('data-image-id');
    var thumbSize = sender.getAttribute('data-thumb-size');

    if (confirm('Tem certeza?')) {
      $.ajax({
        url:imageWork.product_service_url + '/images/' + imageId + '/thumbs/' + thumbSize,
        type:'delete',
        success:function () {
          callback( true);
        },
        error:function () {
          callback(false);
        }
      })
    } else {
      callback(false);
    }
  };

  imageWork.OnReplaceImage = function (sender) {
    var imageId = sender.getAttribute('data-image-id');
    var thumbSize = sender.getAttribute('data-thumb-size');

    $('#new_thumb_overlay').fadeIn('fast');
    $('#replace_thumb_form').attr('action', '/images/' + imageId + '/replace_thumb');

    $('#image_id').val(imageId);
    $('#thumb_size').val(thumbSize);
  };

  $("#mytags").tagit({
    availableTags:getAllTags(),
    usedTags:getUsedTags(),
    hiddenFieldName:"product[tag_list][]"
  });

  function getAllTags() {
    return getArrayFromField('#all_tags');
  }

  function getUsedTags() {
    return getArrayFromField('#used_tags');

  }

  function getArrayFromField(id) {
    var value = $(id).val();

    if (value != "")
      return value.split(' ');

    return null;
  }

  $('.overlay').hide();
};

$(function () {
  ////
  // Setando o título da descrição padrão
  ////
  $('#create_product').click(function () {
    $('#product_descriptions_attributes_title').val($('#product_name').val());
  });

  ////
  // Validações
  ////
  $(".product_form").validate({
    invalidHandler:function () {
      $('#error').show();
    },

    rules:{
      "product[name]":{
        required:true
      },

      "product[price]":{
        required:true,
        number:true,
        max:1000000
      },

      "product[descriptions_attributes][shorted]":{
        required:true,
        maxlength:300
      },

      "product[descriptions_attributes][complete]":{
        required:true
      }
    },

    messages:{
      "product[name]":{
        required:"Definda o título"
      },

      "product[price]":{
        required:"Defina o preço",
        number:"O preço precisa ser um número",
        max:'O preço máximo é 1.000.000'
      },

      "product[descriptions_attributes][shorted]":{
        required:"Defina a descrição reduzida",
        maxlength:'O tamanho máximo da descrição reduzida é 300 caracteres'
      },

      "product[descriptions_attributes][complete]":{
        required:"Defina a descrição completa"
      }
    },
    onkeyup:false
  });

  ////
  // Funcionalidade de marcar imagem como principal
  ////
  $('.set_as_main').click(function (e) {
    var target = $(e.target);
    var imageId = target.parents('fieldset:first').data('image-id').toString();

    $('#product_main_image_id').val(imageId);
    $(e.target).parents('form').submit();

    e.preventDefault();
  });
});