// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery-1.7.1
//= require jquery_ujs

////
// Timer para ocultar mensagens
////

function delayedHide(){
    setTimeout(function() {
      $('.success').slideUp();
    }, 3000);
}

delayedHide();

////
// Links desabilitados
////
$('a.disabled').unbind('click');
$('a.disabled').click(function (e) {
  e.preventDefault();
})