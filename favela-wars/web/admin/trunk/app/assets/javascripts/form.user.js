// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery-ui-1.8.core-and-interactions.min
//= require jquery-ui-1.8.autocomplete.min
//= require jquery-ui-1.8.tag-it
//= require jquery.validate


$(function(){
  $("#new_user").validate({
    invalidHandler: function() {
      $('#error').show();
    },

    rules:{
      "user[login]":{
        required:true
      },

      "user[email]":{
        required: true,
        email: true
      },

      "user[password]":{
        required: true,
        rangelength: [6, 15]
//        passwordRegex: true
      }
    },

    messages:{
      "user[login]":{
        required:"Campo requerido"
      },
      "user[email]":{
        required: "Campo requerido",
        email: 'Formato incorreto'
      },
      "user[password]":{
        required:"Campo requerido",
        rangelength: 'Deve ter entre 6 e 15 caracteres'
//        passwordRegex: "A senha deve conter letras E números"
      }
    },
    onkeyup:false
  });

  function enableSubmit(){
    $('.submit_form').attr('disabled', false);
  }

  $('.submit_form').attr('disabled', true);
  $('.form').find('input').live('keyup', enableSubmit);
  $('.form').live('change', enableSubmit);

  function delayedHide(){
    setTimeout(function() {
      $('.success').slideUp();
    }, 3000);
  }

  delayedHide();
});