// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery.validate

$(function () {
  $(".form").validate({
    invalidHandler:function () {
      $('#error').show();
    },

    rules:{
      "category[name]":{
        required:true
      }
    },

    messages:{
      "category[login]":{
        required:"Campo requerido"
      }
    },
    onkeyup:false
  });

  ////
  // Setando o título da descrição padrão
  ////
  $('#button_save').click(function () {
    var shorted = $('#category_descriptions_attributes_shorted').val();
    var complete = $('#category_descriptions_attributes_complete').val();

    if (shorted != '' || complete != '')
      $('#category_descriptions_attributes_title').val($('#category_name').val());
  });

  ////
  // Reabilitar submit de form após interação
  ////
  var saveButton = $('form :submit');
  saveButton.attr('disabled', true);

  var enableForm = function (e) {
    $(e.target).valid();
    saveButton.attr('disabled', false);
  };

  $('form').find('input').live('change', enableForm);
  $('form').find('input').live('keyup', enableForm);
  $('form').find('textarea').live('keyup', enableForm);
  $('form').find('select').live('change', enableForm);
});