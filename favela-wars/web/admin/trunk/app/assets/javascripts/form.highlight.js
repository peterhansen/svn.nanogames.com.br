//= require form
//= require form.images

$(function () {
  var initialHighlight = $('#initial_highlight').val() - 1;
  $('#highlights').val(initialHighlight);
  selectInitialHighlight(initialHighlight);

  imageWork.product_service_url = $('#env').val() == 'production' ? 'http://admin.staging.nanostudio.com.br' : 'http://localhost:3000'
  imageWork.activate();

  imageWork.OnRemoveImage = function (sender, callback) {
    var reference = sender.getAttribute('data-reference');
    var highlightId = reference.split('_')[0];
    var size = reference.split('_')[1];
    var thumb = sender.getAttribute('data-thumb-size');

    if (confirm('Tem certeza?')) {
      $.ajax({
        url: imageWork.product_service_url + '/highlights/' + highlightId + '/remove_thumb',
        type: 'post',
        data: {
          image_size: size,
          thumb: thumb
        },
        success: function () {
          callback(true);
        },
        error: function () {
          callback(false);
        }
      })
    } else {
      callback(false);
    }
  };

  // Trocar Imagens
  imageWork.OnReplaceImage = function (sender) {
    var reference = sender.getAttribute('data-reference');
    var highlightSize = reference.split('_')[1];
    var highlightId = reference.split('_')[0];
    var thumbSize = sender.getAttribute('data-thumb-size');

    $('#new_thumb_overlay').fadeIn('fast');
    $('#replace_thumb_form').attr('action', '/highlights/' + highlightId + '/replace_thumb');

    $('#highlight_size').val(highlightSize);
    $('#thumb_size').val(thumbSize);
  };

  ////
  // Validações
  ////
  var validationConditions = {
    invalidHandler:function () {
      $(this).find('.fail').show();
    },

    rules: {
      "highlight[link]": {
        required: true
      }
    },

    messages: {
      "highlight[link]": {
        required: 'Campo requerido'
      }
    },

    onkeyup: false
  }

  $('#highlight_form_1').validate(validationConditions);
  $('#highlight_form_2').validate(validationConditions);
  $('#highlight_form_3').validate(validationConditions);
});

function selectInitialHighlight(index) {
  $('.highlight').hide();
  $('#' + index).show();
}

// Toogle de destaques
$('#highlights').change(function (e) {
  var index = $(e.target).val();

  selectInitialHighlight(index);
})
