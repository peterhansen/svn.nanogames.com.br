namespace :deploy do
  namespace :nginx do
    desc <<-DESC
    This task configure the nginx server. It copies the config/nginx.vhost to /etc/nginx/sites-available/project-name
    and symlinks it to /etc/nginx/sites-enabled/project-name.
    DESC
    task :config do
      if nginx_vhost_exists?
        pp "Redeploying vhost."
        delete_vhost
      else
        pp "Creating vhost"
      end
      copy_vhost
      enable
    end

    desc <<-DESC
    This task enables the system. It works by symlinking the system's vhost to /etc/nginx/sites-enabled.
    DESC
    task :enable do
      pp 'Enabling the application.'
      sudo "ln -sf /etc/nginx/sites-available/#{application} /etc/nginx/sites-enabled/#{application}"
    end

    desc 'Restart NginX'
    task :restart do
      pp 'Restarting NginX'
      sudo "service nginx restart"
    end
  end
end

def nginx_vhost_exists?
  file_exists? "/etc/nginx/sites-available/#{application}"
end

def delete_vhost
  sudo "rm /etc/nginx/sites-available/#{application}"
end

def copy_vhost
  sudo "cp #{current_path}/config/nginx.vhost /etc/nginx/sites-available/#{application}"
  pp 'Copying htpasswd file.'
  sudo "cp #{current_path}/config/nginx.htpasswd /etc/nginx/htpasswd"
end
