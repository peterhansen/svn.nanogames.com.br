CarrierWave.configure do |config|
  config.permissions = 0666
  config.root = Rails.root.join(Rails.public_path).to_s
end