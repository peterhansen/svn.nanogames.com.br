worker_processes 2
working_directory "/home/ubuntu/admin/current"

listen "/tmp/admin.sock", :backlog => 64
timeout 30

pid "/home/ubuntu/admin/shared/pids/unicorn.pid"
stderr_path "/home/ubuntu/admin/shared/log/unicorn.stderr.log"
stdout_path "/home/ubuntu/admin/shared/log/unicorn.stdout.log"

preload_app true

GC.respond_to?(:copy_on_write_friendly=) and GC.copy_on_write_friendly = true