Admin::Application.routes.draw do
  resources :users, :categories, :descriptions, :promotions, :maps

  resources :highlights do
    post :remove_big_image, on: :member
    post :regenerate_big_image, on: :member
    post :remove_small_image, on: :member
    post :regenerate_small_image, on: :member
    post :regenerate_big_image, on: :member
    post :remove_thumb, on: :member
    post :replace_thumb, on: :member
  end

  resources :products do
    post :refresh_thumbs, on: :member
    post :add_asset, on: :member
    post :remove_asset, on: :member
  end

  resources :images do
    post :remove_thumbs, on: :member
    post :refresh_thumbs, on: :member
    post :replace_thumb, on: :member
    post :create_highlight_big_image, on: :collection
    post :create_highlight_small_image, on: :collection
  end

  root to: 'products#index', via: :get
end
