# encoding: utf-8

require 'spec_helper'

describe 'Product' do
	context 'Criando produto' do
		it 'deve criar produto normalmente' do
			HTTParty.should_receive(:get).
				with("http://localhost:9494/categories/get_all_nested.json").
				and_return({categories: []}.to_json)
			HTTParty.should_receive(:get).
				with("http://localhost:9494/tags.json").
				and_return(stub(parsed_response: {tags: []}.to_json))
			HTTParty.should_receive(:get).
				with("http://localhost:9494/products.json?include=category_name&page=1&page_size=50").
				and_return(stub(parsed_response: {products: []}.to_json))
			Products::Client.should_receive(:create_product).
				and_return({product: {id: 1}}.to_json)

			visit '/products/new'
	    fill_in 'Nome oficial', with: 'AR-15'
      fill_in 'Descrição reduzida', with: 'Uma descrição qualquer'
	    fill_in 'Descrição completa', with: 'Uma outra descrição mais bem elaborada'
	    fill_in 'Preço', with: '15'
	    click_link_or_button 'Salvar'
		end
	end
end