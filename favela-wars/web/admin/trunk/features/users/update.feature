# language: pt

@sprint03
Funcionalidade: Atualizando um usuário
  Com o objetivo de administrar os usuários
  Como um administrador
  Eu gostaria de atualizar os dados de um usuário

  @wip
  Cenário: Atualizando os dados de um usuários
    Dado que existe um usuário de nome 'chucknorris'
    Quando eu vou para a página de edição do usuário '1'
    E eu preencho 'Email' com 'outro@email.com'
    E eu clico em 'Salvar Usuário'
    Então eu devo estar no indice de usuários
    E eu devo ver 'outro@email.com'

  Cenário: Tentando atualizar um usuário inexistente
    Quando eu vou para a página de edição do usuário 'invalid_user'
    Então eu devo estar no indice de usuários
    E eu devo ver 'User not found'