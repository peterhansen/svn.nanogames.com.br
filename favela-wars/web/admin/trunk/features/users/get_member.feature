@sprint03 @done
Feature: Consultando um usuário
  Com o objetivo de administrar os usuários
  Como um administrador
  Eu gostaria de accessar os dados de um usuário

#  @get_user
#  Scenario: Consultando um usuários
#    Given que existe um usuário de nome 'chucknorris'
#    When eu vou para a página do usuário 'chucknorris'
#    Then eu devo ver 'chucknorris@nanostudio.com.br'

  @invalid_user
  Scenario: Consultando um usuários invalido
    When eu vou para a página do usuário 'invalid_user'
    Then eu devo ver 'User not found'