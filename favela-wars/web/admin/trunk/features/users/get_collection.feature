Feature: Consultando a lista de usuários
  Com o objetivo de administrar os usuários
  Como um administrador
  Eu gostaria de accessar uma lista de usuários

#  Scenario: Consultando a lista de usuários
#    Given que existem os seguintes usuários:
#      | login       | email                         |
#      | chucknorris | chucknorris@nanostudio.com.br |
#      | lindomar    | lindomar@nanostudio.com.br    |
#      | chapolin    | chapolin@nanostudio.com.br    |
#    When eu vou para o indice de usuários
#    Then eu devo ver:
#      | chucknorris |
#      | lindomar    |
#      | chapolin    |