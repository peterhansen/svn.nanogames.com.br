# language: pt

@sprint03
Funcionalidade: usuários
  Com o objetivo de administrar os usuários
  Como um administrador
  Eu gostaria de criar usuários

  Cenário: Criando um novo usuário
    Quando eu vou para a página de criação de usuário
    E eu preencho 'Login' com 'chucknorris'
    E eu preencho 'Email' com 'chucknorris@nano.com.br'
    E eu preencho 'Senha' com 'senha123'
    E eu clico em 'Salvar Usuário'
    Então eu devo ver 'Usuário criado com sucesso'

#  Scenario: Tentando criar um novo usuário com dados inválidos
#    Given que existe um usuário de nome 'chucknorris'
#    When eu vou para a página de criação de usuário
#    And eu prencho os campos com:
#      | field         | value                         |
#      | user_login    | chucknorris                   |
#      | user_email    | chucknorris@nanostudio.com.br |
#      | user_password | senha123                      |
#    And eu clico em 'Create'
#    Then eu devo ver 'Login has already been taken'
#
#  Scenario: Tentando cadastrar um usuario sem login
#    Given eu vou para a página de criação de usuário
#    When eu submeto o formulario de login sem login
#    Then eu devo ver 'Login can't be blank'
#
#  Scenario: Tentando cadastrar um usuario sem email
#    Given eu vou para a página de criação de usuário
#    When eu submeto o formulario de login sem email
#    Then eu devo ver 'Email is invalid'
#    And meus dados devem continuar preenchidos

#  Scenario: Tentando cadastrar um usuário com um email já utilizado
#    Given eu vou para a página de criação de usuário
#    And que existe um usuário com email 'chucknorris@nanostudio.com.br'
#    When eu submeto o formulário de login com dados validos
#    Then eu devo ver 'Email has already been taken'
#    And meus dados devem continuar preenchidos