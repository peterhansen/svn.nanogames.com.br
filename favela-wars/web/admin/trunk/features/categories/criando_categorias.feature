# language: pt

@allow_network
Funcionalidade: Criando categorias
  Com o objetivo de administrar as categorias
  Como um administrador
  Eu gostaria de criar categorias

  @javascript
  Cenário: Criando uma categoria
    Quando eu vou para a página de criação de categorias
    E eu preencho 'Nome' com 'Armamento Pesado'
    E eu preencho 'Descrição reduzida' com 'Uma descrição qualquer'
    E eu preencho 'Descrição completa' com 'Uma outra descrição mais bem elaborada'
    E eu clico em 'Salvar'
    Então eu devo ver 'Categoria 'Armamento Pesado' criada com sucesso.'

#  Cenário: Tentando criar um produto com um nome já utilizado
#    Dado que o produto 'AR-15' existe
#    Quando eu vou para a página de criação de produtos
#    E eu preencho 'Nome oficial' com 'AR-15'
#    E eu preencho 'Descrição reduzida' com 'Uma descrição qualquer'
#    E eu preencho 'Descrição completa' com 'Uma outra descrição mais bem elaborada'
#    E eu preencho 'Preço' com '15'
#    E eu clico em 'Salvar'
#    Então eu devo ver 'Name has already been taken'

#  Cenário: Após erro de validação os dados devem permanecer preenchidos
#    Quando eu vou para a página de criação de produtos
#    E eu preencho 'Nome oficial' com 'AR-15'
#    E eu preencho 'Descrição reduzida' com 'Uma descrição qualquer'
#    E eu clico em 'Salvar'
#    Então o campo 'Nome oficial' deve conter 'AR-15'
#    E o campo 'Descrição reduzida' deve conter 'Uma descrição qualquer'