# encoding: utf-8

When /^eu vou para [a|o] (.+)$/ do |page|
  visit path_to(page)
end

When /^eu preencho '(\D+)' com '(.+)'$/ do |field, data|
  fill_in field, with: data
end

Then /^eu devo ver '(.+)'$/ do |text|
  page.should have_css('div', :text => text, :visible => true)
end

Then /^eu não devo ver '(.+)'$/ do |text|
  page.should_not have_content(text)
end

Then /^eu devo ver:$/ do |table|
  table.raw.each do |text|
    step "eu devo ver '#{text.first}'"
  end
end

When /^eu prencho os campos com:$/ do |table|
  table.hashes.each do |row|
    fill_in row[:field], with: row[:value]
  end
end

When /^eu clico em '(\D+)'$/ do |name|
  click_link_or_button name
end

Then /^eu devo estar n[a|o] (\D+)$/ do |path|
  current_path.should == path_to(path)
end

When /^eu submeto o formulario de login sem email$/ do
  step "eu preencho 'user_login' com 'lindomar'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu clico em 'Create'"
end

When /^eu submeto o formulario de login sem login$/ do
  step "eu preencho 'user_email' com 'chucknorris@nanogames.com.br'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu clico em 'Create'"
end

When /^eu submeto o formulário de login com dados validos$/ do
  step "eu preencho 'user_login' com 'chucknorris'"
  step "eu preencho 'user_email' com 'chucknorris@nanostudio.com.br'"
  step "eu preencho 'user_password' com 'senha123'"
  step "eu clico em 'Create'"
end

When /^meus dados devem continuar preenchidos$/ do
  find('#user_login').value.should_not be_blank
end

When /^eu abro a página$/ do
  save_and_open_page
end

When /^o campo '(\D*)' deve conter '(.*)'$/ do |field, value|
  find_field(field).value.should == value
end

When /^que o produto '(.*)' existe com id '(\d+)'$/ do |product, id|
  step "que o produto '#{product}' existe"
end

When /^eu seleciono '(\D+)' em '(\D+)'$/ do |option, field|
  select option, from: field
end

When "eu clico em '$link' dentro de '$div'" do |link, div|
  Game::Client.should_receive(:publish_map).and_return(true)

  within("##{div}") do
    click_on 'Publicar'
  end
end
