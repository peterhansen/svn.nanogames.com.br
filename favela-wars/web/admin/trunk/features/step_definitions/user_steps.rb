# encoding: utf-8

Given /^que existe um usuário de nome '(\w+)'$/ do |user|
  step "eu vou para a página de criação de usuário"
  step "eu preencho 'Login' com '#{user}'"
  step "eu preencho 'Email' com 'chucknorris@nano.com.br'"
  step "eu preencho 'Senha' com 'senha123'"
  step "eu clico em 'Salvar Usuário'"
end

Given /^que existem os seguintes usuários:$/ do |table|
  #users = []
  #
  #table.raw.each do |user|
  #  users << { user: { login: user[0], email: user[1] } }
  #end

  #ActiveResource::HttpMock.respond_to do |mock|
  #  mock.get "/users.json?page=1&page_size=10", { }, {users: users}.to_json
  #  mock.get '/users/count.json', {}, {user_count: 2}.to_json
  #end
end

Given /^que não existem usuários cadastrados$/ do
  #ActiveResource::HttpMock.respond_to do |mock|
  #  mock.get "/users.json", { }, {users: []}.to_json
  #end
end

When /^que existe um usuário com email 'chucknorris@nanostudio.com.br'$/ do

end