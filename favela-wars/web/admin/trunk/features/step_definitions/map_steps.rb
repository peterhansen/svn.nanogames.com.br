Given 'que existem os mapas cadastrados:' do |table|
  # maps = [Game::Client::Mock.new(:map)]
  maps = {"maps" => (table.hashes.map {|m| {"map" => m}})}.to_json
  Game::Client.should_receive(:get_maps_filtered).at_least(1).times.and_return(maps)
end
