# encoding: utf-8

Given /^que existe um produto de nome 'AR-15' e id '1234'$/ do

end

When /^que o produto 'AR-15' existe$/ do
  step "eu vou para a página de criação de produtos"
  step "eu preencho 'Nome oficial' com 'AR-15'"
  step "eu preencho 'Descrição reduzida' com 'Uma descrição qualquer'"
  step "eu preencho 'Descrição completa' com 'Uma outra descrição mais bem elaborada'"
  step "eu preencho 'Preço' com '15'"
  step "eu clico em 'Salvar'"
end