# encoding: utf-8

module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition
  #
  def path_to(page_name)
    case page_name

      when /^página inicial$/
        '/'

      when /^indice de usuários$/
        '/users'

      when /^indice de mapas/
        '/maps'

      when /^página de criação de usuário$/
        '/users/new'

      when /^página do usuário '(\w+)'$/
        user_path($1)

      when /^indice de produtos$/
        '/products'

      when 'página de criação de produtos'
        new_product_path

      when /^página de edição do produto '(\d+)'$/
        "/products/#{$1}/edit"

      when /^página de edição do usuário '(\w+)'$/
        "/users/#{$1}/edit"

      when 'página de criação de categorias'
        new_category_path

      # Add more mappings here.
      # Here is an example that pulls values out of the Regexp:
      #
      #   when /^(.*)'s profile page$/i
      #     user_profile_path(User.find_by_login($1))

      else
        begin
          page_name =~ /^the (.*) page$/
          path_components = $1.split(/\s+/)
          self.send(path_components.push('path').join('_').to_sym)
        rescue NoMethodError, ArgumentError
          raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
                    "Now, go and add a mapping in #{__FILE__}"
        end
    end
  end
end

World(NavigationHelpers)
