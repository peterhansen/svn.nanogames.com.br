# language: pt

Funcionalidade: Listando os mapas
  Cenário: Listando os mapas
    Dado que existem os mapas cadastrados:
      | name    | published |
      | lapa_1  | false     |
      | boreo_1 | false     |
    Quando eu vou para o indice de mapas
    Então eu devo ver 'lapa_1'
