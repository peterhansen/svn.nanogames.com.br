# language: pt

Funcionalidade: Atualizando mapas
  Cenário: Atualizando um mapas
    Dado que existem os mapas cadastrados:
      | name    | published |
      | lapa_1  | false     |
      | boreo_1 | false     |
    Quando eu vou para o indice de mapas
    E eu clico em 'Publicar' dentro de 'lapa_1'
