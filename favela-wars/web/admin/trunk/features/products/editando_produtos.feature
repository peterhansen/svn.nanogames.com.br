# language: pt

@sprint03
Funcionalidade: Editando um produto
  Com o objetivo de administrar os produto
  Como um administrador
  Eu gostaria de editar os dados de um produto

  @javascript
  Cenário: Adicionando uma descrição
    Dado que o produto 'AR-15' existe com id '1'
    Quando eu vou para a página de edição do produto '1'
    E eu clico em 'adicionar nova descrição'
    E eu seleciono 'en' em 'Linguagem'
    E eu preencho 'Título' com 'Titulo da nova descrição'
    E eu preencho 'Descrição reduzida' com 'Qualquer coisa'
    E eu preencho 'Descrição completa' com 'Qualquer coisa de novo'
    E eu clico em 'Salvar Descrição'
    Então eu devo ver 'Descrição cadastrada com sucesso'

  @javascript
  Cenário: Tentando adicionar uma descrição repetida
    Dado que o produto 'AR-15' existe com id '1'
    Quando eu vou para a página de edição do produto '1'
    E eu clico em 'adicionar nova descrição'
    E eu preencho 'Título' com 'Teste'
    E eu preencho 'Descrição reduzida' com 'Qualquer coisa'
    E eu preencho 'Descrição completa' com 'Qualquer coisa de novo'
    E eu clico em 'Salvar Descrição'
    Então eu devo ver 'Linguagem já utilizada'
