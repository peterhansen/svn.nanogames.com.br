# language: pt

@sprint05
Funcionalidade: Criando produtos
  Com o objetivo de administrar os produtos
  Como um administrador
  Eu gostaria de criar produtos


#  @javascript
#  Cenário: Tentando criar um produto com um nome já utilizado
#    Dado que o produto 'AR-15' existe
#    Quando eu vou para a página de criação de produtos
#    E eu preencho 'Nome oficial' com 'AR-15'
#    E eu preencho 'Descrição reduzida' com 'Uma descrição qualquer'
#    E eu preencho 'Descrição completa' com 'Uma outra descrição mais bem elaborada'
#    E eu preencho 'Preço' com '15'
#    E eu clico em 'Salvar'
#    Então eu devo ver 'Este nome já esta sendo utilizado em outro produto.'

#  Cenário: Após erro de validação os dados devem permanecer preenchidos
#    Quando eu vou para a página de criação de produtos
#    E eu preencho 'Nome oficial' com 'AR-15'
#    E eu preencho 'Descrição reduzida' com 'Uma descrição qualquer'
#    E eu clico em 'Salvar'
#    Então o campo 'Nome oficial' deve conter 'AR-15'
#    E o campo 'Descrição reduzida' deve conter 'Uma descrição qualquer'
