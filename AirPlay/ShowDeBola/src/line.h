/** \file line.h
	2011-06-14
*/
#include <components/defines.h>
#include <components/drawable.h>
#include <components/drawableimage.h>
#include <components/pattern.h>
#include <components/sprite.h>
#include <components/listeners.h>
#include <components/gamemanager.h>

#include <src/constants.h>
using namespace Constants;

#include <src/move.h>

#ifndef LINE_H
#define LINE_H

// forward declaration
class GameScreen;

/** \class Line
	\brief Representa um dos canudos da tela de jogo.
*/
class Line : public DrawableGroup
{
public:
	static int targetY;

	Line() : DrawableGroup()	{ clear(); }
	~Line();

	Line( int lineIndex, GameScreen *gameScreenPtr );

	virtual void setSize( Point size );

	void start() { /* TODO: implementar fade in */ }

	void gameOver() { /* TODO: implementar, fazendo fade out */ }

	void prepare( iwfixed difficultyLevel );

	int insertMove( MovePtr move, int currentY );

	void update( unsigned int dt );

	void onPressed();
	void onReleased();

	void showFeedback( PrecisionLevel type );

	inline bool isOver()
	{ 
		return currentMoveIndex >= moves.size();
	}

	inline DrawableGroupPtr getMovesGroup() { return movesGroup; }

	void setBigMode( bool bigMode );

	void setBigLevel( iwfixed percent )
	{
		const int h = NanoMath::lerpFixed( 0, IW_FIXED( HACK_BOTTLE_HEIGHT ), percent ) >> IW_GEOM_POINT;
			
		//const bottleMask : Shape = new Shape();
		bottleFill->setSize( Point( bottleFill->getWidth(), h ) ); 

		//bottleMask.graphics.beginFill( 0xffffff );
		//bottleMask.graphics.drawRect( 0, Constants.BOTTLE_Y + bottle.height - h, 1000, 1000 );
		//bttleMask.graphics.endFill();
		//bottle.mask = bottleMask;
	}

	/** Detecta se o ponto p se encontra dentro do alvo da linha. */
	inline bool isInsideTarget( Point p ) 
	{ 
		Point relativeP = p - getAbsolutePosition();
		return target->contains( relativeP );
	}

	void correctMovesPosition( int width );

	static void setMoveToFade( Move* move );
	static void fadeMoves( unsigned int dt );

	// TODO: proteger
	static CIwArray<Move*> fadingMoves;

protected:
	
	iwfixed difficultyLevel;

	//onHitCB onHitCallback;
	int index;

	int currentMoveIndex;

	CIwArray<MovePtr> moves;
	MovePtr movePressed;
	int lastVisibleMoveIndex;

	iwfixed speed;
	
	SpritePtr target;
	
	DrawablePtr bottle;
	DrawablePtr bottleFill;
	DrawablePtr emptyBottle;

	DrawableGroupPtr movesGroup;

	GameScreen *gameScreen;

	ImagePtr topImgPtr;
	ImagePtr lineImgPtr;
	ImagePtr targetImg;

	int currentMinDistance;
	int currentMaxDistance;

	DrawableImagePtr lineTop;
	PatternPtr line;

	MovePtr getCurrentMove();

	bool isCurrentMoveInTarget();

	inline int getTargetYCenter() { return target->getPosY() + ( target->getHeight() / 2 ); }
	inline int getTargetYBottom() { return target->getPosY() + target->getHeight(); }

	void clear();
};
#endif