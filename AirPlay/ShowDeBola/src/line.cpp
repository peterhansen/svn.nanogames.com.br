
#include <components/drawableimage.h>
#include <src/gamescreen.h>
#include <src/line.h>

#include <src/constants.h>
//using namespace Constants::Path;

// �ndices das anima��es para cada target
#define TARGET_STOP		0
#define TARGET_HIT		1
#define TARGET_LOOP		2

// defini��es est�ticas
int Line::targetY = 0;
CIwArray<Move*> Line::fadingMoves = CIwArray<Move*>();

Line::Line( int lineIndex, GameScreen *gameScreenPtr ) 
	: DrawableGroup()
{
	clear();
	this->gameScreen = gameScreenPtr;
	this->index = lineIndex;

	//soundTrans = new SoundTransform();
	//soundTrans.volume = Constants.VOLUME_SOUND_MOVE_HIT;
		
	// TODO: getImageIndex
	topImgPtr = GameManager::getImage( 0 );
	lineTop = DrawableImagePtr( new DrawableImage( topImgPtr ) );
	lineTop->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	insertDrawable( lineTop );
			
	lineImgPtr = GameManager::getImage( 1 );
	line = PatternPtr( new Pattern( lineImgPtr ) );
	line->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	insertDrawable( line );

	insertDrawable( movesGroup );

			
	// TODO: fazer carga das imagens das garrafas
	/*int emptyBottleId = GameManager::createImage( Path::IMAGE_PATH + "garrafa_black.png" );
	emptyBottle = DrawablePtr( new DrawableImage( GameManager::getImage( emptyBottleId ) ) );
	emptyBottle->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	//emptyBottle.x = -emptyBottle.width / 2;
	//emptyBottle.y = Constants.BOTTLE_Y;
	insertDrawable( emptyBottle );

	int bottleId = GameManager::createImage( Path::IMAGE_PATH + "garrafa_empty_black.png" );
	bottle = DrawablePtr( new DrawableImage( GameManager::getImage( bottleId ) ) );
	bottle->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	//bottle.x = -bottle.width / 2;
	//bottle.y = Constants.BOTTLE_Y;
	insertDrawable( bottle );*/

	bottleFill = DrawablePtr( new Pattern( BIG_COLA_COLOR ) );
	bottleFill->setRefRatio( Point( IW_GEOM_HALF, IW_GEOM_ONE ) );
	insertDrawable( bottleFill );
						
	// TODO: remover men��es a m�scara. Acreditamos que n�o precisaremos de
	// m�scaras (desenharemos o fundo colado a garrafa para ter o mesmo efeito?)
	//const maskGroup : MovieClip = new MovieClip();
	//maskNeck = new Mask();
	//maskNeck.x = ( 0.5 + GameScreen.LINES_X_PERCENT[ index ] ) * Constants.STAGE_WIDTH;
	//maskNeck.y = Constants.BOTTLE_Y;
	//maskGroup.addChild( maskNeck );
			
	//const bottleMask : MovieClip = getBottle( index );
	//bottleMask.x = ( 0.5 + GameScreen.LINES_X_PERCENT[ index ] ) * Constants.STAGE_WIDTH - bottleMask.width / 2;
	//bottleMask.y = Constants.BOTTLE_Y;
	//maskGroup.addChild( bottleMask );
		
	//const linesMask : Shape = new Shape();
	//linesMask.graphics.beginFill( 0xffffff );
	//linesMask.graphics.drawRect( 0, 0, 1000, Constants.BOTTLE_Y - maskNeck.height );
	//linesMask.graphics.endFill();
	//maskGroup.addChild( linesMask );			
	//movesGroup.mask = maskGroup;
	
	// TODO:
	/*
		bottleSpecial = new EspecialGarrafas();
		bottleSpecial.alpha = 0;
		bottleSpecial.scaleY = 0;
		bottleSpecial.scaleX = 0;
		bottleSpecial.y = Constants.BOTTLE_Y + 14;
		addChild( bottleSpecial );
	*/
		
	targetImg = GameManager::getImage( 2 );

	// TODO: levar para GameManager? Como lidar com sprites?
	target = SpritePtr( new Sprite( targetImg, Path::IMAGE_PATH + "miras.seq" ) );

	target->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	insertDrawable( target );
}

Line::~Line()
{
	for( int i = 1, size = moves.size(); i < size; i++ )
		moves[i].get()->~Move();

	movesGroup->~DrawableGroup();
}

void Line::setSize( Point size )
{
	DrawableGroup::setSize( size );

	lineTop->setPosition( Point( size.x / 2, 0 ) );
		
	line->setPosition( lineTop->getPosition() + Point( 0, lineTop->getHeight() ) );
	line->setSize( Point( line->getImageSize().x, size.y ) );

	target->setPosition( Point( size.x / 2, Line::targetY ) );
	target->setSize( Point( HACK_MAGNIFICAR, HACK_MAGNIFICAR ) );

	// TODO: implementar tamanho das garrafas
	//emptyBottle->setPosition( Point( size.x / 2, target->getPosY() + target->getHeight() ) );
	//emptyBottle->setSize( Point( ) );
	//bottle->setPosition( emptyBottle->getPosition() );

	bottleFill->setSize( Point( HACK_BOTTLE_WIDTH, 0 ) );
	bottleFill->setPosition( Point( size.x / 2, target->getPosY() + target->getHeight() + HACK_BOTTLE_HEIGHT ) );
	IwTrace( MYAPP, ( "bottle fill y: %d", bottleFill->getPosY() ) );

	
	movesGroup->setSize( Point( size.x, movesGroup->getHeight() ) );

	// TODO: para quando precisarmos redimensionar tela
	// posicionamento do movesGroup vai ter que ser feito
	// com rela��o ao tamanho antigo da tela (propor��o)
	movesGroup->setPosition( Point( 0, movesGroup->getPosY() ) );
}

void Line::prepare( iwfixed difficultyLevel )
{
	this->difficultyLevel = NanoMath::clamp( difficultyLevel, 0, IW_FIXED(1) );

	// TODO: lidar com labels ativas
	//activeLabels.splice( 0, activeLabels.length );

	speed = NanoMath::lerpFixed( SPEED_MIN, SPEED_MAX, this->difficultyLevel );

	movePressed = MovePtr();
	lastVisibleMoveIndex = 0;
			
	moves = CIwArray<MovePtr>();
	movesGroup->removeAllDrawables();
	movesGroup->setPosition( Point( 0, 0 ) );
	currentMoveIndex = -1;
			
	currentMinDistance = NanoMath::lerpFixed( DISTANCE_MIN_EASY, DISTANCE_MIN_HARD, difficultyLevel ) >> IW_GEOM_POINT;
	currentMaxDistance = NanoMath::lerpFixed( DISTANCE_MAX_EASY, DISTANCE_MAX_HARD, difficultyLevel ) >> IW_GEOM_POINT;
}

void Line::setBigMode( bool bigMode )
{
	// TODO
	//TweenManager.tween( this, "animationStatus", Regular.easeInOut, bigMode ? 0 : 1, bigMode ? 1 : 0, 2 );
}

int Line::insertMove( MovePtr move, int currentY )
{
	moves.push_back( move );
		
	// Aten��o! Esse setPosition guarda apenas uma marca��o, para ser 
	// usada no m�todo correctMovesPosition.
	move->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	move->setPosition( Point( 0, currentY + move->getLength()) );
	move->setSize( Point( 0, move->getLength() ) );

	movesGroup->setSize( Point( movesGroup->getWidth(), move->getHeight() + currentY ) );
	movesGroup->setPosition( Point( 0, -movesGroup->getHeight() ) );

	return currentY + ( move->getLength() + NanoMath::randomRange(0, currentMaxDistance - currentMinDistance ) );
}

void Line::correctMovesPosition( int width )
{
	for( int i = 0; i < moves.size(); i++ )
	{
		//MovePtr move = moves[ i ];
		moves[ i ]->setSize( Point( width, moves[ i ]->getLength() ) );
		moves[ i ]->setPosition( Point( movesGroup->getWidth() / 2, movesGroup->getHeight() - moves[ i ]->getPosY() ) );
	}
}

void Line::update( unsigned int dt )
{
	int dY = NanoMath::max( 1, ( speed * dt ) >> IW_GEOM_POINT );
	movesGroup->setPosition( Point( movesGroup->getPosX(), movesGroup->getPosition().y + dY ) );
			
	if ( lastVisibleMoveIndex < moves.size() ) 
	{
		MovePtr lastMove = moves[ lastVisibleMoveIndex ];

		if ( currentMoveIndex >= lastVisibleMoveIndex || movesGroup->getPosition().y + lastMove->bottom() >= 0 )
		{
			movesGroup->insertDrawable( lastMove );
			++lastVisibleMoveIndex;
					
			if ( currentMoveIndex < 0 )
				currentMoveIndex = 0;
		}
	}
			
	if ( currentMoveIndex >= 0 && currentMoveIndex < moves.size() )
	{
		MovePtr m = moves[ currentMoveIndex ];
		if ( isCurrentMoveInTarget() ) 
		{
			m->update( dt );
		} 
		else
		{
			++currentMoveIndex;
			m->onExit();
					
			switch( m->getState() )
			{
			case MOVE_STATE_COMPLETE:
			case MOVE_STATE_PARTIAL:
				switch ( m->getType() )
				{
				case MOVE_TYPE_CONTINUOUS:

					// se o movimento foi solto, j� foi dado o feedback em onReleased
					if ( movePressed ) {

						// TODO: tratar release
						movePressed->onReleased( target->getPosY() );
						movePressed = MovePtr();

						// TODO: tratar desenho do target
						//target->setSequenceIndex( TARGET_STOP );
						//target.gotoAndStop( getLabel( TARGET_STOP ) );
					} else {
						break;
					}
									
					case MOVE_TYPE_MULTIPLE:
						//onHitCallback( 
						gameScreen->onHit( index, m->getPrecisionLevel(), m->getTotalHits() );
						showFeedback( m->getPrecisionLevel() ); 
					break;
				}
				break;
						
			default:
				showFeedback( PRECISION_MISSED );
				gameScreen->onHit( index, PRECISION_MISSED, m->getTotalHits() );
				//onHitCallback( index, PRECISION_MISSED, m->getTotalHits());
			}
		}
	}

	// tratando desvanecimento dos moves
	Line::fadeMoves( dt );
}

void Line::onPressed()
{
	// TODO: atualizar desenho do target
	target->setSize( Point( 60, 60 ) );
	//target.gotoAndPlay( getLabel( TARGET_HIT ) );
			
	if ( currentMoveIndex >= 0 && currentMoveIndex < moves.size() )
	{
		const MovePtr move = moves[ currentMoveIndex ];
				
		if( move->getPosY() + movesGroup->getPosY() + DISTANCE_APROXIMATION > getTargetYBottom() )
		{
			// apertou tecla antes do movimento estar na �rea da mira
			//trace( "Line[ " + index + " ] APERTOU ANTES -> " + currentMoveIndex );
			movePressed = MovePtr();
			gameScreen->onHit( index, PRECISION_MISSED, 1 );
		} 
		else if ( move->bottom() + movesGroup->getPosY() + DISTANCE_APROXIMATION >= target->getPosY() )
		{
			// apertou dentro da �rea da mira
			//trace( "Line[ " + index + " ] ACERTOU LINHA " + index + " -> "+ currentMoveIndex );
			movePressed = move;
			const bool newHit = move->onPressed( getTargetYCenter() );
					
			switch ( move->getType() )
			{
				case MOVE_TYPE_CONTINUOUS:
					// TODO: atualizar anima��o do target
					//target.gotoAndPlay( getLabel( TARGET_LOOP ) );
				break;
						
				case MOVE_TYPE_MULTIPLE:
					if ( newHit )
					{
						gameScreen->onHit( index, PRECISION_GOOD, 1 );//onHitCallback( index, PRECISION_GOOD, 1 );
						showFeedback( PRECISION_GOOD );
					}
					else
					{
						// se o jogador for muito "apressadinho" e apertar demais, considera como erro
						gameScreen->onHit( index, PRECISION_TERRIBLE, 1 );
						showFeedback( PRECISION_TERRIBLE );
					}
				break;
			}
					
			if ( move->getState() == MOVE_STATE_COMPLETE )
			{
				++currentMoveIndex;
				//move->setVisible( false );
				//trace( "LINE #" + index + " REMOVE " + lastVisibleMoveIndex + " / " + movesGroup.numChildren );
				//movesGroup->removeDrawable( move );
				//trace( "-->" + movesGroup.contains( move ) );
						
				switch ( move->getType() )
				{
					case MOVE_TYPE_MULTIPLE:
					case MOVE_TYPE_DEFAULT:
						gameScreen->onHit( index, move->getPrecisionLevel(), move->getTotalHits() );
						showFeedback( move->getPrecisionLevel() );
					break;
				}
			}
		} 
		else
		{
			// apertou depois da pe�a passar
			//trace( "Line[ " + index + " ] APERTOU DEPOIS -> "+ currentMoveIndex );
			movePressed = MovePtr();
			gameScreen->onHit( index, PRECISION_MISSED, 1 );
		}
	}
}

void Line::onReleased()
{	
	target->setSize( Point( HACK_MAGNIFICAR, HACK_MAGNIFICAR ) );

	if ( movePressed )
	{
		movePressed->onReleased( target->getPosY() );
		switch ( movePressed->getType() )
		{
			case MOVE_TYPE_CONTINUOUS:
				// TODO: fazer anima��o do alvo
				// target.gotoAndStop( getLabel( TARGET_STOP ) );

				if ( movePressed->getState() == MOVE_STATE_COMPLETE )
				{
					gameScreen->onHit( index, movePressed->getPrecisionLevel(), movePressed->getTotalHits() );							
					showFeedback( movePressed->getPrecisionLevel() );
				}
			break;
		}
				
		movePressed = MovePtr();
	}
}

void Line::showFeedback( PrecisionLevel type )
{ /* TODO: implementar */ 
	
				/*var obj : DisplayObject;
		var soundIndex : uint = Constants.SOUND_MOVE_HIT;
			
		switch( type ) {
			case Constants.PRECISION_MISSED:
				obj = new ScoreErrou();
				soundIndex = Constants.SOUND_MOVE_MISS;
			break;
				
			case Constants.PRECISION_TERRIBLE:
			case Constants.PRECISION_BAD:
				obj = new ScoreRuim();
				soundIndex = Constants.SOUND_MOVE_MISS;
			break;
				
			case Constants.PRECISION_GOOD:
				obj = new ScoreBom();
			break;
				
			case Constants.PRECISION_GREAT:
				obj = new ScoreOtimo();
			break;
				
			case Constants.PRECISION_PERFECT:
				obj = new ScoreShow();
			break;
				
			case Constants.PRECISION_BIG:
			case Constants.PRECISION_LEVEL_TOTAL:
				var square:Shape = new Shape();
				square.graphics.beginFill(0x990000);
				square.graphics.drawRect(0, 0, 200, 100);
				square.graphics.endFill();
				square.width = 200;
				square.height = 100;
				obj = square;
			break;
		}

		addChild( obj );
			
		activeLabels.push( obj );
			
		const t : Tween = TweenManager.tween( obj, "y", Regular.easeInOut, target.y - 30, target.y - 40, FEEDBACK_TIME );
		t.addEventListener( TweenEvent.MOTION_FINISH, onFeedbackEnd );
			
		TweenManager.tween( obj, "scaleX", Regular.easeInOut, 0.25, 0.5, FEEDBACK_TIME );
		TweenManager.tween( obj, "scaleY", Regular.easeInOut, 0.25, 0.5, FEEDBACK_TIME );			
		TweenManager.tween( obj, "alpha", Regular.easeInOut, 1.0, 0.0, FEEDBACK_TIME );

		SoundManager.GetInstance().playSoundAtIndex( soundIndex, 0, 0, soundTrans );*/
}

MovePtr Line::getCurrentMove()
{
	if( currentMoveIndex >= 0 && currentMoveIndex < moves.size() )
		return moves[ currentMoveIndex ];
			
	return MovePtr();
}

bool Line::isCurrentMoveInTarget()
{
	MovePtr currentMove = getCurrentMove();

	switch ( currentMove->getType() ) 
	{
	case MOVE_TYPE_CONTINUOUS:
		switch ( currentMove->getState() ) 
		{
		case MOVE_STATE_PARTIAL:
		case MOVE_STATE_COMPLETE:
			return currentMove->getPosY() + movesGroup->getPosY() <= target->getPosY() + target->getHeight() + DISTANCE_APROXIMATION;
						
		default:
			return currentMove->getPosY() + movesGroup->getPosY() + MOVE_CONTINUOUS_HIT_AREA <= target->getPosY() + target->getHeight() + DISTANCE_APROXIMATION;
		}
				
	default:
		int v = currentMove->getPosY() + movesGroup->getPosY();
		int v2 = target->getPosY() + target->getHeight() + DISTANCE_APROXIMATION;

		return v <= v2;
	}
}

void Line::setMoveToFade( Move* move )
{
	fadingMoves.push_back( move );
}

void Line::fadeMoves( unsigned int dt )
{
 	for( int i = 0; i < fadingMoves.size(); i++ )
	{
		Color c = fadingMoves[ i ]->getColor();
		int newAlpha = c.a - ( ( dt * FADING_SPEED ) >> IW_GEOM_POINT );

		if( newAlpha > 0 )
		{
			c.a = newAlpha;
			fadingMoves[ i ]->setColor( c );
		}
		else
		{
			fadingMoves.erase( i, i+1 );
			i--;
		}	
	}
}

void Line::clear()
{
	gameScreen = NULL;
	index = -1;
	difficultyLevel = -1;
	speed = 0;
	moves = CIwArray<MovePtr>();
	movePressed = MovePtr();
	lastVisibleMoveIndex = -1;
	currentMoveIndex = -1;
	movesGroup = DrawableGroupPtr( new DrawableGroup() );
}






	/*
		static const int FEEDBACK_TIME = 0.8;
		
		static const int STOP_ANIMATION_TIME = 2.8;
		
		static const int VANISH_ANIMATION_TIME = STOP_ANIMATION_TIME * 1.5;
		
		private var onHitCallback : Function;
		
		private var index : uint;
		
		private var movePressed : Move;
		
		private var feedbackTween : Tween;
		
//		private var feedbackLabel : TextField;
		
		private var lastVisibleMoveIndex : int;
		
		private var maskNeck : Mask;
		
//		private var particleEmitter : SparkEmitter;
		
		private var bottleSpecial : EspecialGarrafas;
		
		static const LINE_MASK_Y = Constants.BOTTLE_Y + 130;
		
		private var soundTrans : SoundTransform;
		
		private var activeLabels : Vector.< DisplayObject > = new Vector.< DisplayObject >;
};					
		
		protected function onRemovedFromStage( e : Event ) : void {
			removeEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			removeAllChildren();
			
			movesGroup = null;
			bottle = null;
			target = null;
//			particleEmitter = null;
			bottleSpecial = null;
		}
		
		
		public function getIndex() : uint {
			return index;
		}
		
		
		public function set animationStatus( n : Number ) : void {
			bottleSpecial.alpha = n * 0.6;
			bottleSpecial.scaleX = n;
			bottleSpecial.scaleY = n;
		}		
		
		private function getLabel( targetAnimation : uint ) : String {
			const labels : Array = [ "AlvoRed", "AlvoYellow", "AlvoBlack", "AlvoGreen" ];
			const anims : Array = [ "", "Hit", "Continuos" ];
			
			return labels[ index ] + anims[ targetAnimation ];
		}
		
		
		static function getTarget( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new alvo_BLACK();
					
				case Constants.COLOR_GREEN:
					return new alvo_GREEN();
					
				case Constants.COLOR_RED:
					return new alvo_RED();
					
				case Constants.COLOR_YELLOW:
					return new alvo_YELLOW();
					
				default:
					return null;
			}
		}	
		
		protected function showFeedback( type : uint ) : void {			
		}
		
		
		private function onFeedbackEnd( e : Event ) : void {
			if ( activeLabels.length > 0 ) {
				removeChild( activeLabels.shift() );
			}
		}
		
	}*/