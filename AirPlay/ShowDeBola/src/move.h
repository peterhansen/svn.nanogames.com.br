/** \file line.h
	2011-06-14
*/
#include <components/defines.h>
#include <components/drawablegroup.h>
#include <components/drawableimage.h>
#include <components/listeners.h>
#include <components/gamemanager.h>

#include <src/constants.h>
using namespace Constants;

#ifndef MOVE_H
#define MOVE_H


//// TODO: levar para defines???
//#include <boost/shared_ptr.hpp>
//#define LinePtr boost::shared_ptr<Line>

enum MoveState
{
	MOVE_STATE_ACTIVE,
	MOVE_STATE_COMPLETE,
	MOVE_STATE_MISSED,
	MOVE_STATE_PARTIAL,
	MOVE_STATE_NONE
};

enum MoveType
{
	MOVE_TYPE_DEFAULT,
	MOVE_TYPE_CONTINUOUS,
	MOVE_TYPE_MULTIPLE,
	MOVE_TYPE_RANDOM
};

class Line;
class Move : public DrawableGroup
{

public:
	
	// TODO: implementar uma vers�o sem par�metros decente
	Move() {}

	Move( 
		LinePtr parent, 
		uint color, 
		MoveType moveType, 
		int moveTotalHits = -1, 
		int difficultyLevel = -1 );

	~Move()
	{
		unit = DrawableImagePtr();
		parent = DrawablePtr();
	}

	inline int bottom() const { return position.y + size.y; }

	inline MoveState getState() const { return state; }

	inline MoveType getType() const { return type; }

	PrecisionLevel getPrecisionLevel();
	
	inline int getTotalHits() const { return totalHits; }

	bool onPressed( int targetYCenter );
	void onReleased( int targetYCenter );

	void update( unsigned int dt );

	inline int getLength() const { return length; }

	void onExit() { /* TODO: implementar */ }

	void setSize( Point size );

protected:

	static const int HITS_CONTINUOUS_MIN = 5;

	MoveState state;		//!< Estado atual do movimento. @see MoveState
	MoveType type;			//!< Tipo do movimento. @see MoveType

	DrawablePtr parent;

	ImagePtr unitImg;
	DrawableImagePtr unit;

	uint totalHits;
	int hits;

	int length;
	iwfixed precision;
	int yPressed;

	// TODO: essa fun��o por enquanto
	// simplesmente ignora os efeitos da cor nas linhas
	static DrawableImagePtr getDefaultMove();

	void setState( MoveState state );

	inline int getDistance( int targetY, bool center ) const;

	inline int getYCenter() const;

	void checkContinuousPrecision();

	inline DrawablePtr continuousArea() { return drawables[ 0 ]; }
	inline DrawablePtr topCap() { return drawables[ 1 ]; }
	inline DrawablePtr bottomCap() { return drawables[ 2 ]; }
};
#endif



/**
package
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoSounds.SoundManager;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.filters.BlurFilter;
	import flash.geom.Rectangle;
	import flash.media.SoundTransform;

	public final class Move extends MovieClipVS {
		public static const MOVE_TYPE_RANDOM		: int	= -1;
		
		public static const MOVE_TYPE_DEFAULT		: uint	= 0;
		public static const MOVE_TYPE_CONTINUOUS	: uint	= 1;
		public static const MOVE_TYPE_MULTIPLE		: uint	= 2;
		
		private static const MOVE_TYPES_PERCENT		: Array = [ 0.75, 0.17, 0.082 ];
		
		private static const MOVE_TYPES_TOTAL		: uint = 3;
		
		/** Dist�ncia m�xima considerada para um acerto.
		private static const MAX_DISTANCE : uint = 20;
		
		private static const MAX_HALF_DISTANCE : uint = MAX_DISTANCE / 2;
		
		public static const HITS_MIN : uint = 2;
		
		private static const HITS_CONTINUOUS_MIN : uint = 5;
		
		public static const HITS_MAX : uint = 25;

		private static const MULTIPLE_SPACING_EASY : Number = 0;
		
		private static const MULTIPLE_SPACING_HARD : Number = 0;
		
		private static const CONTINUOUS_PRECISION_PERFECT : Number = 0.95;
		
		private static const ALPHA_TRANSITION_TIME : Number = 0.8;
		
		private static const ALPHA_LEVEL_MISSED : Number = 0.4;
		
		private var type : uint = MOVE_TYPE_DEFAULT;
		
		private var state : MoveState = MoveState.NONE;
		
		private var precision : Number = 0;
		
		private var length : Number = 0;
		
		private var hits : uint = 0;
		
		private var totalHits : uint = 1;
		
		private var yPressed : Number = 0;
		
		private var line : Line;
		
		private var soundTrans : SoundTransform;		
		
		private static function getTop( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_top_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_top_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_top_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_top_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getBottom( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_bottom_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_bottom_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_bottom_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_bottom_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getFill( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_center_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_center_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_center_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_center_YELLOW();
					
				default:
					return null;
			}
		}
		
		public function getState() : MoveState {
			return state;
		}
		
		
		public function getLength() : Number {
			return length;
		}
		
		
		public function getType() : Number {
			return type;
		}
		
		private function get bottomCap() : DisplayObject {
			return getChildAt( 2 );
		}				
		
		public function get currentTop() : Number {
			return y;// TODO - ( height / 2 );
		}
		
		
		public function get currentBottom() : Number {
//			return y + ( height / 2 );
			return y + height;
		}		
		
		/**
		 * M�todo chamado quando o movimento sai da �rea da mira da linha.
		public function onExit() : void {
			switch ( type ) {
				case MOVE_TYPE_CONTINUOUS:
					switch ( state ) {
						case MoveState.PARTIAL:
							checkContinuousPrecision();
						break;
						
						case MoveState.ACTIVE:
							setState( MoveState.MISSED );
						break;
					}
				break;
				
				case MOVE_TYPE_DEFAULT:
					if ( state != MoveState.COMPLETE )
						setState( MoveState.MISSED );
				break;
				
				case MOVE_TYPE_MULTIPLE:
					for ( var i : int = numChildren - 1; i >= 0; --i )
						TweenManager.tween( getChildAt( i ), "alpha", Regular.easeInOut, getChildAt( i ).alpha, ALPHA_LEVEL_MISSED, ALPHA_TRANSITION_TIME );
					
					if ( hits <= 0 )
						setState( MoveState.MISSED );
				break;
			}
		}		
		
		public function get yCenter() : Number {
			return y + ( length / 2 );
		}
		
		
		public override function get top() : Number {
			return y; // - ( length / 2 );
		}
		
		
		public override function get bottom() : Number {
//			return y + ( length / 2 );
			return y + length;
		}
		
		
		public override function get left() : Number {
			return x;
		}
		
		
		public override function get right() : Number {
			return x + width;
		}
		
		
		public override function set bottom( b : Number ) : void {
			y = b - height;
		}
		
		
		public override function set top( t : Number ) : void {
			y = t;
		}
	}
}*/