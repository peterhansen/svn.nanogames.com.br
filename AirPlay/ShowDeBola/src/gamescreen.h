/** \file gamescreen.h
	2011-06-14
*/

#include <components/defines.h>
#include <src/constants.h>
#include <components/listeners.h>
#include <src/line.h>

#include <components\label.h>

#ifndef GAMESCREEN_H
#define GAMESCREEN_H

/** \enum GameState
	\brief Enum que define o estado da tela de jogo.
*/
enum GameState
{
	NONE,
	CHOOSE_DIFFICULTY,
	START_LEVEL,
	PLAYING,
	GAME_OVER,
	LEVEL_COMPLETE,
	PAUSED,
	GAME_OVER_MESSAGE,
	LOGIN_MESSAGE
};

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class GameScreen : public Screen
{
public:

	GameScreen();
	~GameScreen();
	
	void setSize( Point size );

	virtual void update( unsigned int dt );
	
	void start();

	void setBigMode( bool b );

	void setState( GameState newState );

	void prepare( int level );

	void onHit( uint lineIndex, uint precisionLevel, uint moveHits );


	void keyboardKeyCB( const s3eKeyboardEvent* ev );
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}

	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	// Nosso jogo n�o funciona em modo single touch
	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	virtual void devicePauseCB() {}
	virtual void deviceUnpauseCB() {}

protected:

	static const int LINES_TOTAL = 4;
	static const int DIFFICULTY_LEVEL_MAX = 18;
	static const int MOVES_PER_LEVEL_EASY = 10;
	static const int MOVES_PER_LEVEL_HARD = 150;

	CIwArray<LinePtr> lines;
	bool paused;
	GameState gameState;

	int pressedLines[LINES_TOTAL];
	int activeLines;

	iwfixed difficultyOffset;
	uint level;

	bool _bigMode;
	float bigModeRemainingTime;
	int bigLevel;

	int bigLevelShown;

	// TODO: remover label temporaria de feedback
	LabelPtr HACKfeedBackLabel;
	LabelPtr HACKprecisionLabel;

	int score;
	int scoreShown;

	int hits;
	int biggestCombo;

	int bonusMultiplier;
	int bonusMultiplierCounter;

	int perfectMoves;
	CIwArray<int> uncheckedMoves;

	int bigLevelSpeed;
	
	void changeBigLevel( int diff );
	
	void setHits( uint hits );

	void changeScore( int points );

	inline uint currentBonusMultiplier() const
	{
		return bonusMultiplier * ( ( getBigMode() ) ? BIG_MODE_MULTIPLIER : 1 );
	}
		
	/** Determina um novo multiplicador b�nus */
	void setBonusMultiplier( uint newBonusMultiplier );

	inline uint getPATerm( uint a1, uint r, uint n ) const { return a1 + ( ( n - 1 ) * r ); }

	void incBonusCounter( uint hitStatus );

	void setPaused( bool p );

	int getLineIndexForKey( s3eKey key );

	inline bool getBigMode() const 
	{ 
		return _bigMode; 
	}

	void updateBigLevel( unsigned int dt, bool changeNow );
};
#endif


/*
package
{
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoSounds.SoundManager;
	
	import fl.controls.Button;
	import fl.motion.Color;
	import fl.motion.easing.Bounce;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import org.osmf.events.TimeEvent;
	import org.osmf.media.MediaPlayer;
	
	public final class GameScreen extends AppScene {
		// Intervalo do looping de atualiza��o do jogo
		private static const TIMER_INTERVAL_MILIS : uint = 20;
		
		// Intervalo m�ximo de atualiza��o permitido
		private static const MAX_UPDATE_INTERVAL_MILIS : int = 125;

		/*
		public static const LINES_X_PERCENT : Array = [ -0.45, -0.35, -0.25, -0.15 ];

		/*
		private static const SCORES : Array = [ 0, 1, 2, 5, 10, 20 ];

		private static const BIG_LEVEL_WIDTH_MAX : uint = 100;
		
		/*
		private static const SIMULTANEOUS_CHANCE_EASY : Number = 0.2;

		/*
		private static const SIMULTANEOUS_CHANCE_HARD : Number = 0.5;
		
		private static const TIME_MESSAGE_DEFAULT : uint = 3;
		
		private static const SCORE_CHANGE_TIME : Number = 1.9;
		
		private static const BIG_LEVEL_CHANGE_TIME : Number = 0.4;
		
		private static const BIG_LEVEL_BAR_X : Number = 300;
		
		private static const BIG_LEVEL_BAR_Y : Number = -150;
		
		private static const BOARD_X : Number = Constants.STAGE_RIGHT - 30;
		
		private static const BOARD_Y : Number = Constants.STAGE_TOP + 15;
		
		private static const SIMULTANEOUS_REPEAT_EASY : Number = 1.0;
		
		private static const SIMULTANEOUS_REPEAT_HARD : Number = 0.44;
		
		private static const BIG_MODE_TIME : Number = 15;
		
		private static const RECENT_MIN_MOVES : uint = 7; 
		
		private static const BIG_MODE_MULTIPLIER : Number = 2;
		
		private static const ALPHA_TRANSITION_TIME : Number = 1.0;
		
		private static const GAME_OVER_MESSAGE_TIME : Number = 1600;
		
		private static const COMBO_LABEL_SCALE : Number = 1.3;
		
		private static const COMBO_LABEL_ANIMATION_TIME : Number = 0.3;
		
		private static const PAUSE_BUTTON_WIDTH : Number = 50;

		private static const PAUSE_BUTTON_HEIGHT : Number = 50;
		
		private static const DIFFICULTY_BUTTON_WIDTH : Number = 100;

		private static const DIFFICULTY_BUTTON_HEIGHT : Number = 100;
		
		private static const PAUSE_BUTTON_X : Number = Constants.STAGE_RIGHT - PAUSE_BUTTON_WIDTH - 20;
		
		private static const PAUSE_BUTTON_Y : Number = Constants.STAGE_BOTTOM - PAUSE_BUTTON_HEIGHT - 20;
		
		private static const MESSAGE_BOX_WIDTH : Number = 600 * 0.6;
		
		private static const MESSAGE_BOX_HEIGHT : Number = 168.7 * 0.6;
		
		/**
		 * Controla o looping de atualiza��o do jogo 
	
		private var _gameLoopTimer : Timer;
		
		/**
		private var messageTimer : Timer;
		
		private var messageOnEndCallback : Function;
		
		private var messageOnEndCallbackParams : *;
		
		/**
		 * Momento da �ltima atualiza��o (em milissegundos) 
		 
		private var _lastUpdateTime : int;
		
		/**
		
		private var soundTimer : Timer;
		
		private var alphaTimer : Timer;
		
		/**
		private var scoreShown : Number = 0;
		
		/**
		private var scoreSpeed : Number = 0;

		/**
		private var scoreLabel : TextField;
		
		/**
		private var bonusLabel : TextField;
		
		private var bigLevelBar : MovieClip;
		
		private var bigLevelShown : Number = 0;
		
		private var _bigMode : Boolean;
		
		private var bigModeRemainingTime : Number;
		
		private var messageLabel : TextField;
		private var messageTween : Tween;
		
		private var hitsLabel : TextField;
		
		private var player : Player;
		
		private var pauseCountdown : int;
		
		private var pauseButton : MovieClip;
		
		private var difficultyBox : MovieClip;
		
		private var board : MovieClip;
		
		private var levelComplete : MovieClip;
		
		private var gameOver : MovieClip;
		
		
		private var levelLabel : TextField;
		
		private var scoreFont : TextFormat;
		private var bonusFont : TextFormat;
		private var hitsFont : TextFormat;
		private var messageFont : TextFormat;
		private var levelFont : TextFormat;		
		
		private var pausePopUp : PopupPause;
		private var btPauseYes : GenericButton;
		private var btPauseNo : GenericButton;
		private var btPauseOn : GenericButton;
		private var btPauseOff : GenericButton;	
		
		private var level : uint;

		private static var static_score : Number;
		private static var static_biggestCombo : uint;
		private static var static_perfectMoves : uint;
		private static var static_level : uint;
		private static var recorSended : Boolean = true;
		
		public function GameScreen() {
			super( null, false, true );
		}		
		
		private function onClickHandler( e : Event ) : void {
			if ( gameState == GameState.PLAYING )
				setPaused( !paused );
		}
		
		
		private function getDifficultyBox() : MovieClip {
			const d : MovieClip = new Dificuldade();
			
			for ( var i : int = 0; i < 3; ++i ) {
				var b : DisplayObject;
				switch ( i ) {
					case 0:
						b = new BotaoEasy();
						b.addEventListener(MouseEvent.CLICK, onStartEasy );
					break;
					
					case 1:
						b = new BotaoMedium();
						b.addEventListener(MouseEvent.CLICK, onStartMedium );
					break; 
				
					case 2:
						b = new BotaoHard();
						b.addEventListener(MouseEvent.CLICK, onStartHard );
					break;
				}				
				
				b.width = DIFFICULTY_BUTTON_WIDTH;
				b.height = DIFFICULTY_BUTTON_HEIGHT;
				
				b.x = ( ( d.width - b.width ) / 2 ) - 21;
				b.y = 100 + ( i * 1.1 * b.height );
				d.addChild( b );
			}
			
			d.scaleX = 0.7;
			d.scaleY = 0.7;
			
			return d;
		}
		
		
		private function onStartEasy( e : Event = null ) : void {
			difficultyOffset = 0;
			start();
		}
		
		
		private function onStartMedium( e : Event = null ) : void {
			difficultyOffset = DIFFICULTY_LEVEL_MAX * 0.33;
			start();
		}
		
		
		private function onStartHard( e : Event = null ) : void {
			difficultyOffset = DIFFICULTY_LEVEL_MAX * 0.66;
			start();
		}
		
		private function backToGameEvent( e : Event ) : void { backToGame( null ); }
		private function backToGame( clickedBt : GenericButton ) : void { setPaused( false ); }
		
		
		private function disableSound( clickedBt : GenericButton ) : void {
			SoundManager.GetInstance().setMute( true );
			updateSound();
		}
		
		
		private function enableSound( clickedBt : GenericButton ) : void {
			SoundManager.GetInstance().setMute( false );
			updateSound();
		}
		
		
		private function exitGame( clickedBt : GenericButton ) : void {
			prepareRanking();
			
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
		}
		
		/** Se o jogador est� logado tenta submeter novos recordes
		public static function submitRanking() : void {
			if ( Application.GetInstance().getLoggedProfile() != null && !recorSended ) {
				Application.submitRecords( static_score, static_biggestCombo, static_perfectMoves, static_level );
				recorSended = true;
			}
		}
		
		private function prepareRanking() : void {
			recorSended = false;
			static_score = score;
			static_biggestCombo = biggestCombo;
			static_perfectMoves = perfectMoves;
			static_level = level;
			
			if( Application.GetInstance().getLoggedProfile() != null ) {
				submitRanking();
			}
		}
		
		
		private function updateSound() : void {
			var b : Boolean = SoundManager.GetInstance().isMute();
			btPauseOn.visible = !b;
			btPauseOff.visible = b;
		}
		
		
		protected override function onRemovedFromStage( e : Event ) : void {
			super.onRemovedFromStage( e );
			
			for ( var i : uint = 0; i < LINES_TOTAL; ++i )
				lines[ i ] = null;
			lines = null;
			
			player = null;
			pauseButton = null;
			bigLevelBar = null;
			board = null;
			scoreFont = null;
			scoreLabel = null;
			bonusFont = null;
			bonusLabel = null;
			hitsFont = null;
			hitsLabel = null;
			levelFont = null;
			levelLabel = null;
			messageFont = null;
			messageLabel = null; 
			levelComplete = null;
			gameOver = null;
			
			if( _gameLoopTimer ) {
				_gameLoopTimer.stop();
				_gameLoopTimer.removeEventListener( TimerEvent.TIMER, gameLoop );
				_gameLoopTimer = null;
			}
			
			mouseEnabled = false;
			mouseChildren = false;
			
			stage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
		}
		
		
		private function showMessage( message : String, time : Number = TIME_MESSAGE_DEFAULT, onEndCallback : Function = null, onEndCallbackParams : * = null ) : void {
			if ( messageTimer )
				onMessageTimeEnded();

			if ( time > 0 ) {
				messageOnEndCallback = onEndCallback;
				messageOnEndCallbackParams = onEndCallbackParams;
				
				messageTimer = new Timer( time * 1000 );
				messageTimer.addEventListener( TimerEvent.TIMER, onMessageTimeEnded );
				messageTimer.start();
			}
			
			messageLabel.text = message;
			doMessageTween( 1 );
		}
		
		
		private function cancelMessageTween() : void {
			if ( messageTween != null ) {
				messageTween.stop();
				messageTween = null;
			}
		} 
		
		
		private function doMessageTween( finalValue : Number ) : void {
			cancelMessageTween();
			if( messageLabel != null ) 
				messageTween = TweenManager.tween( messageLabel, "alpha", Regular.easeInOut, messageLabel.alpha, finalValue, ALPHA_TRANSITION_TIME );
		}
		
		
		private function onMessageTimeEnded( e : TimerEvent = null ) : void {
			doMessageTween( 0 );
			
			if ( messageTimer ) {
				messageTimer.stop();
				messageTimer.removeEventListener( TimerEvent.TIMER, onMessageTimeEnded );
				messageTimer = null;
				
				if ( messageOnEndCallback != null ) {
					const p : * = messageOnEndCallbackParams;
					const f : Function = messageOnEndCallback;
					
					messageOnEndCallback = null;
					messageOnEndCallbackParams = null;
					
					f.call( this, p );
				}
			}
		}
		
		
		/**
		 * Inicia o looping de atualiza��o do jogo  
		public function startRunning() : void {
			// Inicia o looping do jogo
			_gameLoopTimer.start();
		}				
		
		/**
		 * Atualiza o label da pontua��o
		 * @param delta Tempo decorrido desde a �ltima chama ao m�todo
		 * @param changeNow Indica se a pontua��o mostrada deve ser automaticamente igualada � pontua��o real
		public function updateScore( delta : Number, changeNow : Boolean ) : void {
			if( scoreShown != score || changeNow ) {
				if( changeNow ) {
					scoreShown = score;
				} else {
					const dp : Number = scoreSpeed * delta;
					
					// Opa! Houve um erro...
					if( dp < 0 ) {
						scoreShown = score;
					} else {
						const nextScoreShown : Number = scoreShown + dp;
						
						if( ( nextScoreShown < scoreShown ) || ( nextScoreShown >= score ) )
							scoreShown = score;
						else
							scoreShown = nextScoreShown;
					}
				}
				scoreLabel.text = Math.floor( scoreShown ).toString();
				scoreLabel.x = -( 360 * board.scaleX );
				scoreLabel.y = -( 3 * board.scaleY );
				scoreLabel.width = ( 260 * board.scaleX );
				scoreLabel.height = ( 30 * board.scaleY );
//				scoreLabel.x = left + scoreLabel.textWidth / 2; 
//				scoreLabel.y = ( -Constants.STAGE_HEIGHT / 2 ) + scoreLabel.height / 2; 
			}
		}	
		
		public function getRecentPerformance() : Number {
			if ( bigMode )
				return Constants.PRECISION_BIG;
			
			const total : uint = uncheckedMoves.length;
			
			if ( total >= RECENT_MIN_MOVES ) {
				var sum : Number = 0;
				while ( uncheckedMoves.length > 0 )
					sum += uncheckedMoves.shift();
				
				return sum / total;
			}
			
			return Constants.PRECISION_GOOD;
		}
		
		
		private function hitsColor( value : uint ) : uint {
			return ( ( ( value < 150 ) ? ( 250 - ( Math.abs( value - 50 ) * 250 / 100 ) ) : 0 ) << 16 ) | // calculando vermelho
					( ( ( value < 150 ) ? ( 255 - ( value * 255 / 150 ) ) : 0 ) << 8 ) | // calculando fator verde
						( ( ( value < 75 ) ? ( 75 - value ) : 0 ) ); // calculando fator azul
		}
		
		
		private function reduceBonusLabel( e : Event ) : void {
			TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, COMBO_LABEL_SCALE, 1, COMBO_LABEL_ANIMATION_TIME );
			TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, COMBO_LABEL_SCALE, 1, COMBO_LABEL_ANIMATION_TIME );
		}
		
		private function raiseBonusLabel( e : Event ) : void {
			TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, 2 - ( COMBO_LABEL_SCALE ), 1, COMBO_LABEL_ANIMATION_TIME );
			TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, 2 - ( COMBO_LABEL_SCALE ), 1, COMBO_LABEL_ANIMATION_TIME );
		}			
		
		private function updateBonusText() : void {
			if( bigMode ) {
				bonusFont.color = 0x963118;
			} else {
				bonusFont.color = 0xffffff;				
			}
			bonusLabel.defaultTextFormat = bonusFont;
			bonusLabel.text = currentBonusMultiplier().toString();
		}
		
		
		private function repositionBonusLabel() : void {
			bonusLabel.x = -( 455 * board.scaleX );
			bonusLabel.y = ( 38 * board.scaleY );
			bonusLabel.width = ( 48 * board.scaleX );
		}		
		
		/**
		 *		
		
		private function playTheme() : void {
			if ( !SoundManager.GetInstance().isPlaying( Constants.SOUND_MUSIC_GAME ) )
				SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_MUSIC_GAME, 0, 9999 );
		}
		
		
		private function gameOverTimer( e : Event = null ) : void {
			if ( alphaTimer != null ) {
				alphaTimer.stop();
				alphaTimer = null;
			}
			
			alphaTimer = new Timer( GAME_OVER_MESSAGE_TIME );
			alphaTimer.addEventListener( TimerEvent.TIMER, setGameOverState );
			alphaTimer.start();
		}
		
		
		private function loginMessageTimer( e : Event = null ) : void {
			if ( alphaTimer != null ) {
				alphaTimer.stop();
				alphaTimer = null;
			}
			
			alphaTimer = new Timer( GAME_OVER_MESSAGE_TIME );
			alphaTimer.addEventListener( TimerEvent.TIMER, setLoginMessageState );
			alphaTimer.start();
		}
		
		
		private function setGameOverState( e : Event = null ) : void {
			alphaTimer.stop();
			
			const t : Tween = TweenManager.tween( gameOver, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			t.addEventListener( TweenEvent.MOTION_FINISH, function() : void {
				setState( GameState.GAME_OVER );
			} );
		}
		
		
		private function setLoginMessageState( e : Event = null ) : void {
			alphaTimer.stop();

			const t : Tween = TweenManager.tween( gameOver, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			t.addEventListener( TweenEvent.MOTION_FINISH, function() : void {
				setState( GameState.LOGIN_MESSAGE );
			} );
		}
		
		private function goToLoginScreen() : void { }
		
		
		public function getState() : GameState {
			return gameState;
		}
		
		
		public function onLoginButtonYes() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_LOGIN );
		}
		
		
		public function onLoginButtonNo() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH ); 
		}
		
		private function onPauseTimer( e : TimerEvent ) : void {
			--pauseCountdown;
			if ( pauseCountdown <= 0 ) {
				paused = false;
				pauseButton.visible = true;
			} else {
				showMessage( String( pauseCountdown ) + "...", 1, onPauseTimer );
			}
		}		
		
		/**
		 * Looping do jogo. Atualiza objetos e trata colis�es. A renderiza��o � feita pela AVM. 
		 * @param e Dados sobre o evento do timer que controla o looping	
		private function gameLoop( e : TimerEvent ) : void
		{
			if(	_lastUpdateTime == 0 )
				_lastUpdateTime = getTimer();
			
			// Calcula o tempo transcorrido real
			var currTime : int = getTimer();
			var timeElapsed : int = currTime - _lastUpdateTime;
			
			if( timeElapsed > Constants.MAX_UPDATE_INTERVAL_MILIS )
				timeElapsed = Constants.MAX_UPDATE_INTERVAL_MILIS;
			
			// Atualiza os objetos da cena
			update( ( timeElapsed as Number ) / 1000.0 );
			
			_lastUpdateTime = getTimer();
		}
		
		
		private function prepareNextLevel( e : Event = null ) : void {
			TweenManager.tween( levelComplete, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			prepare( level + 1 );
		}				

		private function bigSound( e : Event ) : void {
			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_BIG_MODE );
			soundTimer.stop();
		}
		
		
	
		
		private function getLabelFormatter() : TextFormat {
			var _txtFormat : TextFormat = new TextFormat();
			_txtFormat.font = Constants.FONT_NAME_CARTOONERIE;
			_txtFormat.size = 20;
			_txtFormat.color = 0xffffff;
			_txtFormat.align = TextFormatAlign.CENTER;
			
			return _txtFormat;
		}
		
		
		private function getLabel() : TextField {
			var label : TextField = new TextField();
			label.width = 100;
			label.height = 100;
			label.embedFonts = false;
			label.antiAliasType = AntiAliasType.ADVANCED;
			label.mouseEnabled = false;
			label.multiline = false;
			label.wordWrap = true;
			label.border = false;
			label.selectable = false;
			label.autoSize = TextFieldAutoSize.CENTER;
			label.gridFitType = GridFitType.SUBPIXEL;
			label.defaultTextFormat = getLabelFormatter();
			
			return label;
		}
		
	}
}*/