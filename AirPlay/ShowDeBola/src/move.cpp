#include <src/move.h>

#include <src/constants.h>
#include <src/line.h>


#define MAX_HALF_DISTANCE	( MAX_DISTANCE / 2 )


Move::Move(
	LinePtr parent, 
	uint color, 
	MoveType moveType, 
	int moveTotalHits, 
	iwfixed difficultyLevel ) 
	: DrawableGroup (),
	totalHits( 1 ),
	length( 0 ),
	yPressed( 0 ),
	parent( parent->getMovesGroup() ),
	precision( 0 ),
	hits( 0 )
{
	// TODO: implementar som
	//soundTrans = new SoundTransform();
	//soundTrans.volume = Constants.VOLUME_SOUND_MOVE_HIT;

	if ( moveType == MOVE_TYPE_RANDOM )
	{
		const iwfixed r = NanoMath::random();
		int total = 0;
		
		for ( int bla = 0; bla < MOVE_TYPES_TOTAL; ++bla )
		{
			total += MOVE_TYPES_PERCENT[ bla ];
			if ( r <= total )
			{ 
				type = (MoveType) bla;
				break;
			}
		}
	}
	else
	{
		type = moveType;
	}
			
	setState( MOVE_STATE_ACTIVE );
			
	PatternPtr roundRectFill;
	DrawableImagePtr t;
	DrawableImagePtr b;

	switch ( type )
	{
		case MOVE_TYPE_CONTINUOUS: 
			if ( moveTotalHits <= 0 )
				totalHits = HITS_CONTINUOUS_MIN + NanoMath::randomRange( 0, HITS_MAX >> IW_GEOM_POINT );
			else
				totalHits = HITS_CONTINUOUS_MIN + moveTotalHits;
			
			roundRectFill = PatternPtr( new Pattern( GameManager::getImage( 4 ) ) );
			t = DrawableImagePtr( new DrawableImage( GameManager::getImage( 5 ) ) );
			b = DrawableImagePtr( new DrawableImage( GameManager::getImage( 5 ) ) );
			b->setTransformation( IW_2D_IMAGE_TRANSFORM_FLIP_Y );

			roundRectFill->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
			t->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
			b->setRefRatio( Point( IW_GEOM_HALF, 0 ) );

			length = totalHits * roundRectFill->getImageSize().y;

			roundRectFill->setPosition( Point( 0, t->getHeight() ) );
			roundRectFill->setSize( Point( t->getWidth(), length - 2 * t->getHeight() ) );

			insertDrawable( roundRectFill );
					
			insertDrawable( t );

			b->setPosition( Point( 0, roundRectFill->getPosY() + roundRectFill->getHeight() - 1 ) );
			insertDrawable( b );
		break;
				
		case MOVE_TYPE_DEFAULT:
			totalHits = 1;
				
			unit = getDefaultMove();
			insertDrawable( unit );

			length = unit->getHeight();
		break;
				
		case MOVE_TYPE_MULTIPLE:
			if ( moveTotalHits <= 0 )
				totalHits = ( HITS_MIN >> IW_GEOM_POINT ) + NanoMath::randomRange( 0, HITS_MAX );
			else
				totalHits = moveTotalHits;
					
			const iwfixed currentSpacing = NanoMath::lerpFixed( MULTIPLE_SPACING_EASY, MULTIPLE_SPACING_HARD, difficultyLevel );
					
			int y = 0;
			for( uint i = 0; i < totalHits; ++i )
			{
				unit = getDefaultMove();

				// a posi��o negativa � apenas um marcador para que
				// possamos posicion�-lo no loop de posicionamento
				unit->setPosition( Point( 0, y ) );
				insertDrawable( unit );
						
				y += unit->getHeight() + currentSpacing;
			}

			// loop de posicionamento
			for( uint i = 0; i < totalHits; i++ )
				unit->setPosition( Point( unit->getPosX(), y - unit->getPosY() ) );

			length = y;
		break;
	}
}

void Move::setSize( Point size )
{
	DrawableGroup::setSize( size );
	for( int i = 0, dSize = drawables.size(); i < dSize; i++ )
		drawables[ i ]->setPosition( Point( size.x / 2, drawables[ i ]->getPosY() ) );
}

void Move::update( unsigned int delta )
{
	switch ( type ) 
	{
		case MOVE_TYPE_CONTINUOUS:
			switch ( state ) 
			{
				case MOVE_STATE_PARTIAL:
					if( bottom() > yPressed )
					{
						const int diff = abs( parent->getPosY() + getPosY() + getHeight() - yPressed );
						const DrawablePtr c = continuousArea();
						const DrawablePtr t = topCap();
						const DrawablePtr b = bottomCap();

						c->setSize( Point( c->getWidth(), NanoMath::max( 0, length - diff - t->getHeight() /* + b->getHeight()*/ ) ) );
						
						t->setPosition( Point( t->getPosX(), c->getPosY() - t->getHeight() ) );
						b->setPosition( Point( b->getPosX(), c->getPosY() + c->getHeight() ) );
								
						if ( c->getHeight() <= 0 ) 
						{
							precision = IW_GEOM_ONE;
							setState( MOVE_STATE_COMPLETE );
						}
					} 
					else 
					{
						precision = IW_GEOM_ONE;
						setState( MOVE_STATE_COMPLETE );
					}
				break;
			}
		break;
				
		case MOVE_TYPE_MULTIPLE:
			const int parentY = getPosY() + parent->getPosY();

			for ( int i = 0, drawablesSize = drawables.size(); i < drawablesSize; ++i ) 
			{
				//const child : DisplayObject = getChildAt( i );
				const DrawablePtr child = drawables[ i ];

				//if ( child->getAlpha() == 1.0 && ( parentY + child.y + ( child.height ) ) < Constants.TARGET_Y )
				//{
					// se est� fora do alcance, n�o pode mais ser atingido
				//	TweenManager.tween( child, "alpha", Regular.easeInOut, child.alpha, ALPHA_LEVEL_MISSED, ALPHA_TRANSITION_TIME );
				//}
			} 
		break;
	}
}

// TODO: esse m�todo deve levar em considera��o a cor da linha 
// na hora de buscar as imagens
DrawableImagePtr Move::getDefaultMove()
{
	ImagePtr unitImg = GameManager::getImage( 3 );
	DrawableImagePtr unit = DrawableImagePtr( new DrawableImage( unitImg ) );
	unit->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
	return unit;
}

void Move::setState( MoveState state ) 
{
	const MoveState previousState = this->state;
			
	this->state = state;
	switch ( state ) {
		case MOVE_STATE_ACTIVE:
		break;

		case MOVE_STATE_COMPLETE:
			Line::setMoveToFade( this );
			// TODO:
			//if ( type == MOVE_TYPE_CONTINUOUS )
			//TweenManager.tween( this, "alpha", Regular.easeInOut, alpha, 0, 0.5 );
		break;
				
		case MOVE_STATE_MISSED:
			// TODO: o alfa do missed � diferente do complete?
			// olhar "case" anterior.
			// o m�ltiplo j� trata os erros parciais
			//if ( type != MOVE_TYPE_MULTIPLE )
			//	TweenManager.tween( this, "alpha", Regular.easeInOut, alpha, ALPHA_LEVEL_MISSED, ALPHA_TRANSITION_TIME );
		break;
				
		case MOVE_STATE_PARTIAL:
		break;
	}
}

bool Move::onPressed( int targetYCenter ) 
{
	bool newHit = true;
			
	int distance;
	switch( type )
	{
		case MOVE_TYPE_CONTINUOUS:
			switch ( state ) {
				case MOVE_STATE_ACTIVE:
					setState( MOVE_STATE_PARTIAL );
					yPressed = NanoMath::max( parent->getPosY() + getPosY(), targetYCenter );
				break;
			}
		break;

		case MOVE_TYPE_DEFAULT:
			distance = getDistance( targetYCenter, true );
			precision = IW_FIXED(1) - ( IW_FIXED( distance ) / MAX_DISTANCE );
			setState( MOVE_STATE_COMPLETE );
		break;
				
		case MOVE_TYPE_MULTIPLE:
			const int hitY = ( parent->getPosY() + getPosY() );
					
			switch ( getState() )
			{
				case MOVE_STATE_MISSED:
				break;
						
				case MOVE_STATE_ACTIVE:
					setState( MOVE_STATE_PARTIAL );
				default:
					if ( hits < totalHits ) 
					{
						int nDrawables = drawables.size();
						int i = nDrawables - 1; 
						
						for ( ; i >= 0; --i ) 
						{
							const DrawablePtr c = drawables[ i ];

							bool fitsBelow = hitY + c->getPosY() + ( c->getHeight() ) >= Line::targetY + -MAX_HALF_DISTANCE;
							bool fitsAbove = hitY + c->getPosY() - ( c->getHeight() ) <= Line::targetY + 3*MAX_HALF_DISTANCE;

							if( fitsBelow && fitsAbove ) 
							{
								++hits;
								removeDrawableAt( i );
								break;
							}
						}
						newHit = i != nDrawables || nDrawables == 0;
								
						if ( hits >= totalHits )
							setState( MOVE_STATE_COMPLETE );
					} else {
						hits = NanoMath::max( hits - 1, 0 );
					}
					precision = NanoMath::clamp( IW_FIXED(hits) / totalHits, 0, IW_FIXED(1) );
				break;
			}
		break;
	}			
	return newHit;
}

void Move::onReleased( int targetYCenter )
{
	switch ( type ) {
		case MOVE_TYPE_CONTINUOUS:
			switch ( state ) {
				case MOVE_STATE_PARTIAL:
					checkContinuousPrecision();
					setState( MOVE_STATE_COMPLETE );
				break;
			}
		break;

		case MOVE_TYPE_DEFAULT:				
		case MOVE_TYPE_MULTIPLE:
		break;
	}
}

PrecisionLevel Move::getPrecisionLevel()
{
	if ( precision < IW_FIXED(15)/100 )
	{
		return PRECISION_MISSED;
	} 
	else if ( precision < IW_FIXED(45)/100 )
	{
		return PRECISION_TERRIBLE;
	} 
	else if ( precision < IW_FIXED(60)/100 )
	{
		return PRECISION_BAD;
	} 
	else if ( precision < IW_FIXED(75)/100 )
	{
		return PRECISION_GOOD;
	} 
	else if ( precision < IW_FIXED(90)/100 )
	{
		return PRECISION_GREAT;
	} 
	else
	{
		return PRECISION_PERFECT;
	}
}

int Move::getDistance( int targetY, bool center ) const 
{
	int r = abs( parent->getPosY() + getYCenter() - targetY );
	return NanoMath::max( 0, r - DISTANCE_APROXIMATION );
}

int Move::getYCenter() const
{
	return getPosY() + ( length / 2 );
}

void Move::checkContinuousPrecision()
{
	precision = NanoMath::clamp( IW_FIXED( length - continuousArea()->getHeight() ) / length, 0, IW_FIXED(1) );
	if ( precision >= CONTINUOUS_PRECISION_PERFECT )
		precision = IW_FIXED(1);
			
	//trace( "PRECISION: " + ( precision * 100.0 ) + "%" );
}