/** \file texts.h
	\brief Contém enum dos textos da aplicação.
*/

/** \enum AppTextsIds
	\brief Enum com índices dos textos da aplicação.
	Essa Enum precisa estar em concordância com o número de índices que existe no arquivo TEXTS_PATH\X.txt, onde X é o índice da língua.
	@see GameManager::loadTexts
*/
enum AppTextsIds {
	TEXT_LINE_1,
	TEXT_LINE_2,
	TEXT_LINE_3,
	NUMBER_OF_TEXTS
};