
// include principal, que importa funcionalidade da API s3eFile
// e da API de imagens e fontes
#include <s3eFile.h>
#include <IwImage.h>
#include <IwGxFont.h>

#include <components\defines.h>
#include <components\gamemanager.h>
#include <src\gamescreen.h>

/**
* Chamada principal da aplicação.
*/
S3E_MAIN_DECL void IwMain() 
{
	// inicialização dos módulos necessários
	Iw2DInit();
	IwGxFontInit();

	// inicializa Game Manager e carrega alguns recursos
	if( !GameManager::init() )
	{
		// TODO: avisar de erro na inicialização
		return;
	}
	GameManager::setCurrentScreen( ScreenPtr( new GameScreen() ) );

	// principal loop da aplicação
	while( GameManager::update() )
		GameManager::draw();
	 
	// executa funcoes necessarias do término da aplicacao
	GameManager::terminate();
		
	// terminamos dos módulos necessários
	IwGxFontTerminate();
	Iw2DTerminate();
}