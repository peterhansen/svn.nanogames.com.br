
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

// TODO: por enquanto esse � o defines do projeto (e n�o dos componentes)
#include <boost/shared_ptr.hpp>
#define LinePtr			boost::shared_ptr<Line>
#define MovePtr			boost::shared_ptr<Move>
#define GameScreenPtr	boost::shared_ptr<GameScreen>



namespace Constants
{
	namespace Path
	{
		const std::string IMAGE_PATH = "images/";
	};

	//namespace LineConsts
	//{
		const int LINES_COLOR[4] = { 0xff0000, 0x00ff00, 0xffffff, 0xffff00 };

		const int LINE_SCREEN_BORDER = 20;

		const iwfixed DISTANCE_MIN_EASY = IW_FIXED(35);
		const iwfixed DISTANCE_MIN_HARD = IW_FIXED(5);
		const iwfixed DISTANCE_MAX_EASY = IW_FIXED(140);
		const iwfixed DISTANCE_MAX_HARD = IW_FIXED(80);
		const iwfixed SIMULTANEOUS_REPEAT_EASY = IW_GEOM_ONE;
		const iwfixed SIMULTANEOUS_REPEAT_HARD = IW_FIXED(44)/100;
		const iwfixed SIMULTANEOUS_CHANCE_EASY = IW_FIXED(2)/10;
		const iwfixed SIMULTANEOUS_CHANCE_HARD = IW_FIXED(5)/10;
		const iwfixed HITS_MAX = IW_FIXED(25);
		const iwfixed HITS_MIN = IW_FIXED(2);
		const iwfixed SPEED_MIN = IW_FIXED(16)/100;	//!< Velocidade m�nima da linha, em pixels/dt.
		const iwfixed SPEED_MAX = IW_FIXED(32)/100;	//!< Velocidade m�xima da linha, em pixels/dt.
		const int MOVE_CONTINUOUS_HIT_AREA = 30;
		const int DISTANCE_APROXIMATION = 2;				//!< Dist�ncia dada de "b�nus" para o jogador no momento do hit.
		const int MOVE_TYPES_TOTAL = 3;
		const iwfixed MOVE_TYPES_PERCENT[3] = { IW_FIXED(75)/100, IW_FIXED(17)/100, IW_FIXED(82)/1000 };
		const iwfixed MULTIPLE_SPACING_EASY = 0;
		const iwfixed MULTIPLE_SPACING_HARD = 0;
		
		const int MAX_DISTANCE = 20;

		// TODO: remover quando tivermos o tamanho correto da nossa imagem de target
		const int HACK_MAGNIFICAR = 90;
		
		// TODO: remover, quando estivermos desenhando a garrafa
		const int HACK_BOTTLE_WIDTH = 20;
		const int HACK_BOTTLE_HEIGHT = 30;

		const int TARGET_Y = 166;
		const float BIG_MODE_TIME = 15;
		const float BIG_MODE_SPEED = 0.002;
		const int BIG_MODE_MULTIPLIER = 2;
		const int MAX_SCORE = 999999999;
		const iwfixed CONTINUOUS_PRECISION_PERFECT = IW_FIXED(95)/100;
		const int MAX_BONUS_MULIPLIER = 99;
		const int SCORES[6] = { 0, 1, 2, 5, 10, 20 };

		const Color BIG_COLA_COLOR = CreateColor( 128, 41, 19, 255 );

		/* Termo inicial da PA que regula o crescimento do multiplicador b�nus */
		const int BONUS_PA_A1 = 5;
		
		/* Raz�o da PA que regula o crescimento do multiplicador b�nus. Quanto menor o termo, mais r�pido cresce o multiplicador. */
		const int BONUS_PA_R = 5;
				
		/** Varia��o no marcador Big de acordo com cada resultado de jogada. */
		const int BIG_LEVEL_DIFF[6] = { -4, -2, -1, 1, 2, 3 };

		const int BIG_LEVEL_MIN = 0;
		const int BIG_LEVEL_MAX = 100;

		const float BIG_LEVEL_SPECIAL = BIG_LEVEL_MIN + ( BIG_LEVEL_MAX - BIG_LEVEL_MIN ) * 0.3;
		const float BIG_LEVEL_CHANGE_TIME = 0.4;

		const iwfixed FADING_SPEED = IW_FIXED(50)/100;

		/** \enum PrecisionLevels
			\brief N�veis de precis�o dos Hits.
			@see Move
		*/
		enum PrecisionLevel
		{
			PRECISION_MISSED,
			PRECISION_TERRIBLE,
			PRECISION_BAD,
			PRECISION_GOOD,
			PRECISION_GREAT,
			PRECISION_PERFECT
		};
	//};
};

#endif