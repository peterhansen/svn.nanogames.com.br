#include <src/gamescreen.h>

GameScreen::GameScreen() 
	: Screen(),
	paused( false ),
	activeLines( -1 ),
	difficultyOffset( 0 ),
	_bigMode( false ),
	score( 0 ),
	scoreShown( 0 ),
	bonusMultiplier( 1 ),
	uncheckedMoves( CIwArray<int>() ),
	biggestCombo( 0 ),
	hits( 0 ),
	bonusMultiplierCounter( 0 ),
	perfectMoves( 0 ),
	bigLevel( 0 ),
	bigLevelSpeed( 0 ),
	bigLevelShown( 0 )
{
	for( int i = 0; i < LINES_TOTAL; i++ )
	{
		pressedLines[ i ] = -1;
	}
			
		//_gameLoopTimer = new Timer( TIMER_INTERVAL_MILIS );
		//_gameLoopTimer.addEventListener( TimerEvent.TIMER, gameLoop );
			
	/*
		stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
		stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
		*/

	// TODO: melhorar manager de resources
	// carregando imagens das linhas
	GameManager::createImage( Constants::Path::IMAGE_PATH + "canudo_end_red.png" );
	GameManager::createImage( Constants::Path::IMAGE_PATH + "canudo_base_red.png" );
	GameManager::createImage( Constants::Path::IMAGE_PATH + "miras.png" );
	GameManager::createImage( Constants::Path::IMAGE_PATH + "canudo_unidade_red.png" );
	GameManager::createImage( Constants::Path::IMAGE_PATH + "canudo_center_red.png" );
	GameManager::createImage( Constants::Path::IMAGE_PATH + "canudo_top_red.png" );

	lines = CIwArray<LinePtr>();
	for( uint i = 0; i < LINES_TOTAL; ++i )
	{
		LinePtr line = LinePtr( new Line( i, this ) );
		line->setRefRatio( Point( IW_GEOM_HALF, 0 ) );
		lines.push_back( line );
		insertDrawable( line );
	}
			
	//player = new Player( getRecentPerformance );
	//player.alpha = 0;
	//addChild( player );
	//player.x = 120;
	//player.y = player.y + 30;
			
	/*pauseButton = new BotaoPause();
	pauseButton.x = PAUSE_BUTTON_X;
	pauseButton.y = PAUSE_BUTTON_Y;
	pauseButton.width = PAUSE_BUTTON_WIDTH;
	pauseButton.height = PAUSE_BUTTON_HEIGHT;
	pauseButton.buttonMode = true;
	pauseButton.useHandCursor = true;
	pauseButton.addEventListener(MouseEvent.CLICK, onClickHandler);
	pauseButton.alpha = 0;
	addChild( pauseButton );
			
	bigLevelBar = new MovieClip();
	bigLevelBar.stop();
	bigLevelBar.x = BIG_LEVEL_BAR_X;
	bigLevelBar.y = BIG_LEVEL_BAR_Y;
	addChild( bigLevelBar );
			
	board = new Placar();
	board.alpha = 0;
	board.x = BOARD_X;
	board.y = BOARD_Y;
	addChild( board );
			
	var florida : FloridaProjectOne = new FloridaProjectOne();
			
	scoreFont = new TextFormat();
	scoreFont.font = florida.fontName;
	scoreFont.size = ( 30 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
	scoreFont.align = TextFormatAlign.RIGHT;
			
	scoreLabel = getLabel();
	scoreLabel.defaultTextFormat = scoreFont;
	scoreLabel.embedFonts = true;
	scoreLabel.antiAliasType = AntiAliasType.ADVANCED;
	board.addChild( scoreLabel );
			
	bonusFont = new TextFormat();
	//bonusFont.color = 0x77eaff;
	bonusFont.font = florida.fontName;
	bonusFont.align = TextFormatAlign.RIGHT;
	bonusFont.size = ( 40 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );

	bonusLabel = getLabel();
	bonusLabel.defaultTextFormat = bonusFont;
	bonusLabel.embedFonts = true;
	bonusLabel.antiAliasType = AntiAliasType.ADVANCED;
	board.addChild( bonusLabel );
			
	setBonusMultiplier( 0 );
			
	hitsFont = new TextFormat();
	hitsFont.font = florida.fontName;
	hitsFont.size = ( 40 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
	hitsFont.align = TextFormatAlign.RIGHT;
			
	hitsLabel = getLabel();
	hitsLabel.defaultTextFormat = hitsFont;
	hitsLabel.embedFonts = true;
	hitsLabel.antiAliasType = AntiAliasType.ADVANCED;
	board.addChild( hitsLabel );
						
	levelFont = new TextFormat();
	levelFont.font = florida.fontName;
	levelFont.size = ( 45 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
	levelFont.align = TextFormatAlign.CENTER;
			
	levelLabel = getLabel();
	levelLabel.x = -( 85 * board.scaleX );
	levelLabel.y = ( 13 * board.scaleY );
	levelLabel.width = ( 75 * board.scaleX );
	levelLabel.defaultTextFormat = levelFont;
	levelLabel.embedFonts = true;
	levelLabel.antiAliasType = AntiAliasType.ADVANCED;
	board.addChild( levelLabel );
			
	messageFont = new TextFormat();
	messageFont.font = florida.fontName;
	messageFont.size = 50;
	messageFont.align = TextFormatAlign.CENTER;
			
	messageLabel = getLabel();
	messageLabel.defaultTextFormat = messageFont;
	messageLabel.embedFonts = true;
	messageLabel.width = Constants.STAGE_WIDTH;
	messageLabel.x = Constants.STAGE_LEFT;
	messageLabel.antiAliasType = AntiAliasType.ADVANCED;
	addChild( messageLabel );
			
	levelComplete = new QuadroGanhou();
	levelComplete.width = MESSAGE_BOX_WIDTH;
	levelComplete.height = MESSAGE_BOX_HEIGHT;
	levelComplete.alpha = 0;
	levelComplete.x = -levelComplete.width / 2;
	levelComplete.y = -levelComplete.height / 2;
	addChild( levelComplete );
			
	gameOver = new QuadroPerdeu();
	gameOver.width = MESSAGE_BOX_WIDTH;
	gameOver.height = MESSAGE_BOX_HEIGHT;
	gameOver.alpha = 0;
	gameOver.x = -gameOver.width / 2;
	gameOver.y = -gameOver.height / 2;
	addChild( gameOver );
			
	pausePopUp = new PopupPause();
	addChild( pausePopUp );			
	pausePopUp.visible = false;
			
	pausePopUp.removeChild( pausePopUp.BtSounds );
	btPauseOn = new GenericButton( "Ligado", 1, disableSound, GenericButton.BUTTON_GREEN );
	btPauseOff = new GenericButton( "Desligado", 1, enableSound, GenericButton.BUTTON_PURPLE );
	pausePopUp.addChild( btPauseOn );
	pausePopUp.addChild( btPauseOff );
	btPauseOff.x = btPauseOn.x = pausePopUp.BtSounds.x;
	btPauseOff.y = btPauseOn.y = pausePopUp.BtSounds.y;
	updateSound();
			
	pausePopUp.removeChild( pausePopUp.BtYes );
	btPauseYes = new GenericButton( "Sim", 1, exitGame, GenericButton.BUTTON_GREEN );
	pausePopUp.addChild( btPauseYes );
	btPauseYes.x = pausePopUp.BtYes.x;
	btPauseYes.y = pausePopUp.BtYes.y;
			
	pausePopUp.removeChild( pausePopUp.BtNo );
	btPauseNo = new GenericButton( "N�o", 1, backToGame, GenericButton.BUTTON_GREEN );
	pausePopUp.addChild( btPauseNo );
	btPauseNo.x = pausePopUp.BtNo.x;
	btPauseNo.y = pausePopUp.BtNo.y;
			
	pausePopUp.btClose.addEventListener( MouseEvent.CLICK, backToGameEvent );*/
			
	// TODO: remover label de feedBack tempor�ria
	HACKfeedBackLabel = LabelPtr( new Label( FontPtr( new Font("fonts\\GenBasB.ttf", 10, 100 ) ), "SCORE: 0 (x1) -- Level: 1" ) );
	HACKfeedBackLabel->setSize( Point( 200, 200 ) );
	insertDrawable( HACKfeedBackLabel );

	HACKprecisionLabel = LabelPtr( new Label( HACKfeedBackLabel->getFont(), "" ) );
	HACKprecisionLabel->setSize( Point( 200, 80 ) );
	insertDrawable( HACKprecisionLabel );

	// TODO: n�o vamos precisar fazer isso aqui no futuro, pois
	// n�o a caixa de dificuldades vai chamar o start
	start();

	setState( PLAYING /*CHOOSE_DIFFICULTY*/ );
}

GameScreen::~GameScreen()
{
	for( int i = 0; i < lines.size(); i++ )
		lines[i].get()->~Line();

	
	Line::fadingMoves.clear_optimised();
	lines.clear_optimised();
}

void GameScreen::update( unsigned int dt )
{
	if ( !paused ) {

		// TODO: remover essas mensagens
		char text[300];
		sprintf( text, "SCORE: %d (x%d) \nACERTOS: %d\nLevel: %d\nBIG LEVEL: %d \n(BIG MODE: %d)", score, bonusMultiplier, hits, level, bigLevel, _bigMode );
		IwTrace( MYAPP, ( text ) );
		if( gameState == PLAYING )
			HACKfeedBackLabel->setText( text );

		// TODO : Seria melhor e mais otimizado se utiliz�ssemos o padr�o STRATEGY. Poder�amos armazenar os m�todos
		// de atualiza��o em ponteiros para fun��o / functors e modificar o m�todo de atualiza��o atual apenas alterando
		// o valor de uma vari�vel em setState. Assim n�o ter�amos que executar este switch toda vez qua chamamos update
		switch( gameState )
		{
			case PLAYING:
			case GAME_OVER:
			case GAME_OVER_MESSAGE:
				uint over = 0;
				for( int i = 0; i < activeLines; ++i )
				{
					lines[ i ]->update( dt );
					if ( lines[ i ]->isOver() )
						++over;
				}
						
				if ( gameState == PLAYING && over >= activeLines && gameState == PLAYING )
					setState( LEVEL_COMPLETE );
			break;
		}
			
		// TODO: atualizar jogador e atualizar score
		//player.update( timeElapsed );
		//updateScore( timeElapsed, false );

		updateBigLevel( dt, false );
	}
}

void GameScreen::setSize( Point size )
{
	Screen::setSize( size );

	Line::targetY = size.y - TARGET_Y;
	int lineWidth = ( size.x / 2 ) / activeLines;

	for ( uint i = 0; i < LINES_TOTAL; ++i )
	{
		lines[ i ]->setPosition( Point( ( lineWidth / 2 ) + ( lineWidth * i ) + LINE_SCREEN_BORDER, 0 ) );
		lines[ i ]->setSize( Point ( lineWidth, size.y ) );
	}

	// TODO: labels tempor�rias devem ser removidas
	HACKfeedBackLabel->setPosition( Point( size.x / 2, size.y / 2 ) );
	HACKprecisionLabel->setPosition( Point( size.x / 2, 20 ) );
}

void GameScreen::start()
{
	//difficultyBox.mouseEnabled = false;
	//difficultyBox.mouseChildren = false;
			
	//TweenManager.tween( difficultyBox, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
	//TweenManager.tween( board, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
	//TweenManager.tween( player, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
	//TweenManager.tween( pauseButton, "alpha", Regular.easeOut, 0, 1, ALPHA_TRANSITION_TIME );
	prepare( 1 );
	//startRunning();
	//}		
}

void GameScreen::prepare( int level )
{
	this->level = level;

	// TODO: setar texto da label de n�vel
	//levelLabel.text = level.toString();

	iwfixed difficultyLevel = NanoMath::clamp( (IW_FIXED(level - 1) + difficultyOffset ) / DIFFICULTY_LEVEL_MAX, 0, IW_GEOM_ONE );
			
	const uint previousActiveLines = activeLines;

	iwfixed fixedActiveLines = NanoMath::lerpFixed( IW_FIXED(18)/10, IW_FIXED(LINES_TOTAL), NanoMath::min( ( difficultyLevel * 145 )/100 , IW_FIXED(1) ) );
	activeLines = fixedActiveLines >> IW_GEOM_POINT;
			
	for ( uint i = 0; i < activeLines; ++i )
		lines[ i ]->prepare( difficultyLevel );
			
	for ( uint i = previousActiveLines; i < activeLines; ++i )
		lines[ i ]->start();
			
	const uint totalMoves = NanoMath::lerpFixed( IW_FIXED( MOVES_PER_LEVEL_EASY ), IW_FIXED( MOVES_PER_LEVEL_HARD ), difficultyLevel ) >> IW_GEOM_POINT;
			
	const uint maxSimultaneous = NanoMath::min( activeLines, NanoMath::lerpFixed( IW_FIXED(17)/10, IW_FIXED( LINES_TOTAL ) + IW_GEOM_HALF, difficultyLevel ) >> IW_GEOM_POINT );

	for ( uint l = 0; l < LINES_TOTAL; ++l )
		lines[ l ]->setVisible( l < activeLines );
			
	const int maxHits = NanoMath::lerpFixed( HITS_MIN, HITS_MAX, difficultyLevel ) >> IW_GEOM_POINT;
			
	const iwfixed repeatSimultaneous = NanoMath::lerpFixed( SIMULTANEOUS_REPEAT_EASY, SIMULTANEOUS_REPEAT_HARD, difficultyLevel );
			
	const iwfixed simultaneousChance = NanoMath::lerpFixed( SIMULTANEOUS_CHANCE_EASY, SIMULTANEOUS_CHANCE_HARD, difficultyLevel );
					
	int y = 0;
	for ( uint m = 0; m < totalMoves; )
	{
		const uint quantity = ( NanoMath::random() <= simultaneousChance ? ( 1 + ( NanoMath::randomRange( 0, maxSimultaneous ) ) ) : 1 );
		int yMax = y;
				
		bool usedLines[4] = { false, false, false, false };
		CIwArray<MovePtr> moves = CIwArray<MovePtr>();

		for ( uint current = 0; current < quantity; ++current ) 
		{
			uint lineIndex = NanoMath::randomRange( 0, activeLines );
			while ( usedLines[ lineIndex ] )
			{
				lineIndex = ( lineIndex + 1 ) % activeLines;
			}
			usedLines[ lineIndex ] = true;
				
			MovePtr move;
			if ( current > 0 && NanoMath::random() < repeatSimultaneous )
			{
				const uint copyIndex = NanoMath::randomRange( 0, current );	
				move = MovePtr( new Move( lines[ lineIndex ], LINES_COLOR[ lineIndex ], moves[ copyIndex ]->getType(), moves[ copyIndex ]->getTotalHits(), difficultyLevel ) );
			} 
			else
			{
				move = MovePtr( new Move( lines[ lineIndex ], LINES_COLOR[ lineIndex ], MOVE_TYPE_RANDOM, ( HITS_MIN >> IW_GEOM_POINT ) + NanoMath::randomRange( 0, maxHits ), difficultyLevel ) );
			}

			moves.push_back( move );
			yMax = NanoMath::max( yMax, lines[ lineIndex ]->insertMove( move, y ) );
		}
				
		y = yMax;
		m += quantity;
	}

	changeBigLevel( ( ( BIG_LEVEL_MAX - BIG_LEVEL_MIN ) / 2 ) - bigLevel );
	
	// TODO: atualizar score na interface
	//updateScore( 0, true );

	setState( START_LEVEL );
	setSize( GameManager::getDeviceScreenSize() );
	
	for( int i = 0; i < LINES_TOTAL; i++ )
		lines[ i ]->correctMovesPosition( GameManager::getDeviceScreenSize().y );
}

void GameScreen::setBigMode( bool b )
{
	if( _bigMode != b )
	{
		for( uint i = 0; i < activeLines; ++i )
		{
			lines[i]->setBigMode( b );
		}
		_bigMode = b;
				
		if( b )
		{
			bigModeRemainingTime = BIG_MODE_TIME;
				
			// TODO: lidar com som
			//soundTimer = new Timer( 1000 );
			//soundTimer.addEventListener( TimerEvent.TIMER, bigSound );
			//soundTimer.start();
		}
		// TODO:
		//updateBonusText();
	}
			
	// TODO: lidar com player
	//player.setBigMode( b );
}

void GameScreen::changeBigLevel( int diff )
{
	if ( !getBigMode() )
	{
		bigLevel = NanoMath::clamp( bigLevel + diff, BIG_LEVEL_MIN, BIG_LEVEL_MAX );
				
		if ( bigLevel >= BIG_LEVEL_MAX )
		{
			// ativa modo Big
			bigLevel = BIG_LEVEL_SPECIAL;
			setBigMode( true );
				
			// TODO: colocar express�o do jogador
			//player.setExpression( true );

			bigLevelSpeed = (int) ( bigLevel - bigLevelShown ) / BIG_MODE_TIME;
		} 
		else
		{
			bigLevelSpeed = ( bigLevel - bigLevelShown ) / BIG_LEVEL_CHANGE_TIME;
		}
	}
}

void GameScreen::setState( GameState newState )
{
	gameState = newState;
	switch ( newState ) {
		// TODO:
		/*
		case CHOOSE_DIFFICULTY:
			difficultyBox = getDifficultyBox();
			difficultyBox.sacleX = 0.75;
			difficultyBox.sacleY = 0.75;
			difficultyBox.x = ( -difficultyBox.width/2 ) - 115;
			difficultyBox.y = ( -difficultyBox.height/2 ) - 40;
			TweenManager.tween( difficultyBox, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
			addChild( difficultyBox );
		break;
			*/
		case GAME_OVER_MESSAGE:
			for ( int i = 0; i < activeLines; ++i )
				lines[ i ]->gameOver();
					
			/*player.stopMoving( false, Player.LABEL_BAD, true );
			player.setExpression( false );*/
			setBigMode( false );

			// TODO: remover essa label
			HACKfeedBackLabel->setText( "GAME OVER" );
		
			/*var nextState : GameState;					
			const profile : NOCustomer = Application.GetInstance().getLoggedProfile();
			var f : Function;
					
			prepareRanking();
					
			if ( profile != null ) {
				f = gameOverTimer;
			} else {
				f = loginMessageTimer;
			}
					
			const gameOverTween : Tween = TweenManager.tween( gameOver, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
			gameOverTween.addEventListener( TweenEvent.MOTION_FINISH, f );
					
			TweenManager.tween( board, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			TweenManager.tween( pauseButton, "alpha", Regular.easeOut, 1, 0, ALPHA_TRANSITION_TIME );
					
			SoundManager.GetInstance().stopSound( Constants.SOUND_MUSIC_GAME );
			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_GAME_OVER );*/
		break;
			
		/*
		case GameState.LOGIN_MESSAGE:
			Application.GetInstance().showPopUp( "Que tal enviar seus pontos e competir com os melhores jogadores do mundo? � r�pido e f�cil!", "Show de bola!", onLoginButtonYes, "Quem sabe outra hora...", onLoginButtonNo );
		break;
			
			
		case GAME_OVER:
			//Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
		break;
		*/

		case LEVEL_COMPLETE:
			setBigMode( false );
				
			// TODO: remover essa label
			HACKfeedBackLabel->setText( "next stage! touch the screen :)" );

			/*player.stopMoving( true, Player.LABEL_GOOD, false );
			player.setExpression( true );
					
			const levelCompleteTween : Tween = TweenManager.tween( levelComplete, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
			levelCompleteTween.addEventListener( TweenEvent.MOTION_FINISH, prepareNextLevel );
					
			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_LEVEL_COMPLETE );*/
		break;
				
		case PLAYING:
			setPaused( false );
			for ( uint l = 0; l < LINES_TOTAL; ++l )
			{
				pressedLines[ l ] = -1;
			}
				//pressedLines[ l ] = false;
					
			// TODO: implementar
			//playTheme();
		break;
				
		case START_LEVEL:
			// TODO: implementar anima��es de in�cio de n�vel
			//showMessage( "N�vel " + level, level <= 1 ? TIME_MESSAGE_DEFAULT * 1.5 : TIME_MESSAGE_DEFAULT, setState, GameState.PLAYING );
			//player.resetBall( true );
			//playTheme();
		break;
	}
}

void GameScreen::onHit( uint lineIndex, uint precisionLevel, uint moveHits )
{
	if ( getBigMode() )
		moveHits *= BIG_MODE_MULTIPLIER;
			
	changeScore( SCORES[ precisionLevel ] * currentBonusMultiplier() * moveHits );
	//IwTrace( MYAPP, ("precis�o: %d", precisionLevel ) );

	uncheckedMoves.push_back( precisionLevel );
			
	switch ( precisionLevel )
	{
		case PRECISION_MISSED:
		case PRECISION_TERRIBLE:
			setHits( 0 );
			bonusMultiplierCounter = 0;
			setBonusMultiplier( 1 );
		case PRECISION_BAD:
		break;
				
		case PRECISION_PERFECT:
			++perfectMoves;
			// No Breaks
		default:
			setHits( hits + 1 );
			incBonusCounter( precisionLevel * ( getBigMode() ? BIG_MODE_MULTIPLIER : 1 ) );
	}

	// TODO: remover as chamadas para essas labels
	switch ( precisionLevel )
	{
		case PRECISION_MISSED:
			HACKprecisionLabel->setText( "MISSED!" );
			break;
		case PRECISION_TERRIBLE:
			HACKprecisionLabel->setText( "TERRIBLE..." );
			break;
		case PRECISION_BAD:
			HACKprecisionLabel->setText( "BAD" );
			break;
		case PRECISION_PERFECT:
			HACKprecisionLabel->setText( "PERFECT!" );
			break;
		default:
			HACKprecisionLabel->setText( "OK" );
			break;
	}
	
	changeBigLevel( BIG_LEVEL_DIFF[ precisionLevel ] );
			
	if( gameState == PLAYING && bigLevel <= BIG_LEVEL_MIN )
		setState( GAME_OVER_MESSAGE );
}

void GameScreen::keyboardKeyCB( const s3eKeyboardEvent* ev  )
{
	if( paused ) 
		return;

	int lineIndex;
	if( ev->m_Pressed == 1 )
	{
		switch ( gameState )
		{
			case PLAYING:			
				lineIndex = getLineIndexForKey( ev->m_Key );
						
				if ( lineIndex >= 0 && pressedLines[ lineIndex ] == -1 ) 
				{
					// para teclado, n�o precisamos do id de toque,
					// podemos usar valor "1"
					pressedLines[ lineIndex ] = 1;
					lines[ lineIndex ]->onPressed();
				}
			break;

			//case GAME_OVER_MESSAGE:
			//	GameManager::endGame();
			//	break;
		}
	}
	else
	{
		switch ( gameState )
		{
		case PLAYING:
			const int lineIndex = getLineIndexForKey( ev->m_Key );
						
			if ( lineIndex >= 0 && pressedLines[ lineIndex ] ) 
			{
				pressedLines[ lineIndex ] = -1;
				lines[ lineIndex ]->onReleased();
			}
		break;
		}
	}
}

void GameScreen::pointerButtonCB( const s3ePointerEvent *ev )
{
	const s3ePointerTouchEvent touchEv = { 0, ev->m_Pressed, ev->m_x, ev->m_y };
	touchButtonCB( &touchEv );
}

void GameScreen::pointerMotionCB( const s3ePointerMotionEvent *ev )
{
	const s3ePointerTouchMotionEvent touchMotionEv = { 0, ev->m_x, ev->m_y };
	touchMotionCB( &touchMotionEv );
}

void GameScreen::touchButtonCB( const s3ePointerTouchEvent *ev )
{
	if( paused ) 
		return;

	switch ( gameState )
	{

	// TODO: remover esse caso futuramente
	case LEVEL_COMPLETE:
		if( ev->m_Pressed == 1 )
		{
			prepare( ++level );
			setState( PLAYING );
		}
		break;

	// TODO: remover esse caso futuramente
	case GAME_OVER_MESSAGE:
		if( ev->m_Pressed == 1 )
		{
			GameManager::endGame();
		}
		break;

	case PLAYING:
		for( int i = 0; i < LINES_TOTAL; i++ )
		{
			if( lines[ i ]->isInsideTarget( Point( ev->m_x, ev->m_y ) ) )
			{
				if( ev->m_Pressed == 1 )
				{
					lines[ i ]->onPressed();
					pressedLines[ i ] = ev->m_TouchID;
				}
				else
				{
					pressedLines[ i ] = -1;
					lines[ i ]->onReleased();
				}
				break;
			}
		}
		break;
	}
}

void GameScreen::touchMotionCB( const s3ePointerTouchMotionEvent *ev )
{
	if( paused ) 
		return;

	switch ( gameState )
	{
	case PLAYING:
		for( int i = 0; i < LINES_TOTAL; i++ )
		{
			if( pressedLines[ i ] == ev->m_TouchID )
			{
				if( !lines[ i ]->isInsideTarget( Point( ev->m_x, ev->m_y ) ) )
				{
					pressedLines[ i ] = -1;
					lines[ i ]->onReleased();
				}
				break;
			}
		}
		break;
	}
}

void GameScreen::setHits( uint hits )
{
	this->hits = hits;

	// TODO: atualizar label de hits
	//hitsLabel.visible = hits > 0;
	//hitsLabel.text = hits.toString();
	//hitsLabel.x = -( 230 * board.scaleX );
	//hitsLabel.y = ( 35 * board.scaleY );
	//hitsLabel.width = ( 120 * board.scaleX );
	//hitsFont.color = hitsColor( hits );
	//hitsLabel.defaultTextFormat = hitsFont;
			
	if ( hits > biggestCombo ) 
		biggestCombo = hits;
}

void GameScreen::changeScore( int points )
{
	// Estourou a precis�o, pois nesse jogo a pontua��o nunca diminui
	const int nextScore = score + points;
	if( nextScore < score ) {
		score = MAX_SCORE;
	} else {
		score = nextScore;
				
		if( score > MAX_SCORE )
			score = MAX_SCORE;
		else if( score < 0 )
			score = 0;
	}
			
	const int diff = score - scoreShown;
			
	// Opa! Houve um erro...
	if( diff < 0 )
	{
		IwTrace( MYAPP, ("erro durante altera��o do score: m�todo GameScreen::changeScore") );
		return;
	}
			
	//scoreSpeed = diff / SCORE_CHANGE_TIME;
}

void GameScreen::setBonusMultiplier( uint newBonusMultiplier )
{
	if ( newBonusMultiplier <= 1)
	{
		if (bonusMultiplier != newBonusMultiplier)
		{
			bonusMultiplier =  newBonusMultiplier;

			// TODO: lidar com a label
			//repositionBonusLabel();
					
			// TODO: tratar tween da etiqueta de combo
			//var t1 : Tween = TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, 1, 2 - ( COMBO_LABEL_SCALE ), COMBO_LABEL_ANIMATION_TIME );
			//t1.addEventListener( TweenEvent.MOTION_FINISH, raiseBonusLabel );
			//TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, 1, 2 - ( COMBO_LABEL_SCALE ), COMBO_LABEL_ANIMATION_TIME );
		}
		bonusMultiplier = 1;

		// TODO: atualizar texto do b�nus
		//updateBonusText();
				
	}
	else
	{
		if (newBonusMultiplier > MAX_BONUS_MULIPLIER)
		{
			newBonusMultiplier = MAX_BONUS_MULIPLIER;
		}
				
		if (bonusMultiplier != newBonusMultiplier)
		{
			bonusMultiplier =  newBonusMultiplier;

			// TODO: lidar com esses m�todos
			//updateBonusText();
			//repositionBonusLabel();
		}
				
		// TODO: lidar com o tween
		//var t2 : Tween = TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, 1, COMBO_LABEL_SCALE, COMBO_LABEL_ANIMATION_TIME );
		//t2.addEventListener( TweenEvent.MOTION_FINISH, reduceBonusLabel );
		//TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, 1, COMBO_LABEL_SCALE, COMBO_LABEL_ANIMATION_TIME );
	}
}

void GameScreen::incBonusCounter( uint hitStatus )
{
	if ( bonusMultiplier < MAX_BONUS_MULIPLIER )
	{
		if ( hitStatus > 0 )
		{
			bonusMultiplierCounter += hitStatus;
			const uint nextBonusMultiplier = getPATerm( BONUS_PA_A1, BONUS_PA_R, bonusMultiplier );
			if (bonusMultiplierCounter > nextBonusMultiplier)
			{
				bonusMultiplierCounter -= nextBonusMultiplier;
				setBonusMultiplier(bonusMultiplier + 1);
			}
		}
	}
}

void GameScreen::setPaused( bool p ) 
{
	if ( p != paused )
	{
		if ( p )
		{
			paused = true;

			// TODO: mostrar tela de pausa, esconder seu bot�o e tocar som
			//pausePopUp.visible = true;
			//pauseButton.visible = false;
			//SoundManager.GetInstance().stopSound( Constants.SOUND_MUSIC_GAME );
		} 
		else 
		{
			// TODO: come�ar contagem de retorno ao jogo
			//playTheme();
			//pauseButton.visible = false;
			//pausePopUp.visible = false;
			//pauseCountdown = 3;
			//showMessage( String( pauseCountdown ) + "...", 1, onPauseTimer );

			paused = false;
		}
	}
}

int GameScreen::getLineIndexForKey( s3eKey key )
{
	switch ( key ) {
		case s3eKeyA: case s3eKeyH: return 0;
		case s3eKeyS: case s3eKeyJ: return 1;
		case s3eKeyD: case s3eKeyK: return 2;
		case s3eKeyF: case s3eKeyL: return 3;
		default: return -1;
	}
}

void GameScreen::updateBigLevel( unsigned int dt, bool changeNow )
{
	if ( getBigMode() )
	{
		for ( int bla = 0; bla < activeLines; ++bla )
			lines[ bla ]->setBigLevel( IW_GEOM_ONE );
				
		bigModeRemainingTime -= dt * BIG_MODE_SPEED;
		if ( bigModeRemainingTime <= 0 )
			setBigMode( false );
	} 
	else if ( bigLevelShown != bigLevel || changeNow )
	{
		if ( changeNow )
		{
			bigLevelShown = bigLevel;
		} 
		else
		{
			const int dp = bigLevelSpeed * dt;
					
			const int nextBigLevelShown = bigLevelShown + dp;
			if ( ( bigLevel > bigLevelShown && nextBigLevelShown >= bigLevel ) || ( bigLevel < bigLevelShown && nextBigLevelShown <= bigLevel ) )
			{
				bigLevelShown = bigLevel;
			} else
			{
				bigLevelShown = nextBigLevelShown;
			}
		}

		const iwfixed bigPercent = NanoMath::clamp( IW_FIXED(bigLevelShown) / BIG_LEVEL_MAX, 0, IW_GEOM_ONE );
		for (  int i = 0; i < activeLines; ++i )
			lines[ i ]->setBigLevel( bigPercent );
	}
}	