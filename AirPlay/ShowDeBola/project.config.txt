[Language]
DefaultLanguage		Variável de localização. Indica língua padrão utilizada para os textos da aplicação. Ver "defines.h" para os códigos das línguas.