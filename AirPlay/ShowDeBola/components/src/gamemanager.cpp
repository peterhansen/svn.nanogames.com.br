
#include <s3e.h>
#include <s3eTimer.h>
#include <Iw2D.h>
#include <IwGxFont.h>

#include <components\defines.h>
#include <components\drawablegroup.h>
#include <components\gamemanager.h>

// TODO: remover a definição de Screen daqui
#include <components\listeners.h>



// -----------------------------------------------------
// definições estáticas
// -----------------------------------------------------
uint64 GameManager::timer = 0;
ScreenPtr GameManager::currentScreen = ScreenPtr();
s3eBool	GameManager::gameOver = false;
CIwArray<std::string> GameManager::texts = CIwArray<std::string>();
bool GameManager::hasMultiTouch = false;
Point GameManager::deviceScreenSize = Point( 0, 0 );
CIwArray<ImagePtr> GameManager::images = CIwArray<ImagePtr>();




bool GameManager::init()
{
	// inicialização da pilha de clipping dos drawables
	Drawable::init();

	// registrando eventos de pausa
	s3eDeviceRegister( S3E_DEVICE_PAUSE, (s3eCallback) devicePauseCB, NULL);
	s3eDeviceRegister( S3E_DEVICE_UNPAUSE, (s3eCallback) deviceUnpauseCB, NULL);

	// TODO: registro de volume up ou down
	// TODO: registro de push no device
	// ver enum s3eDeviceCallback

	// registrando eventos de mudança de tela e atualizando
	// tamanho corrente da tela do aparelho
	s3eSurfaceRegister( S3E_SURFACE_SCREENSIZE, (s3eCallback) screenSizeChangeCB, NULL);
	deviceScreenSize = Point( Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight() );

	// TODO: detectar se um sistema dá suporte para teclado parece ser bem mais complicado,
	// pois existem bastante constantes de compatibilidade, como S3E_KEYBOARD_HAS_KEYPAD, 
	// S3E_KEYBOARD_HAS_NUMPAD, etc
	if( true /* keyboard available */ )
	{
		s3eKeyboardRegister( S3E_KEYBOARD_KEY_EVENT, (s3eCallback) keyboardKeyCB, NULL );
		s3eKeyboardRegister( S3E_KEYBOARD_CHAR_EVENT, (s3eCallback) keyboardCharCB, NULL );
	}

	// verificando suporte a multitouch
	hasMultiTouch = s3ePointerGetInt(S3E_POINTER_MULTI_TOUCH_AVAILABLE) ? true : false;
	if( hasMultiTouch )
	{
 		s3ePointerRegister( S3E_POINTER_TOUCH_EVENT, (s3eCallback) multiPointerButtonCB, NULL );
		s3ePointerRegister( S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback) multiPointerMotionCB, NULL );
	}
	else
	{
		s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback) singlePointerButtonCB, NULL);
		s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback) singlePointerMotionCB, NULL);
	}

	// inicializando timer
	timer = s3eTimerGetMs();
	
	// TODO: criar uma entrada nas configurações do projeto para poder
	// usar uma semente fixa (para testes)?
	// gerando seed para números aleatórios baseados no tempo
	IwRandSeed((int32)timer);

	// pegando a língua padrão da configuração e carrega os respectivos textos
	int languageId;
	s3eConfigGetInt( "Language", "DefaultLanguage", &languageId );
	return loadTexts( languageId );
}

void GameManager::terminate()
{
	// terminamos a nossa pilha de clipping para os drawables
	Drawable::terminate();

	// limpando textos carregados
	texts.clear_optimised();

	if( currentScreen )
 		currentScreen = ScreenPtr();

	images.clear_optimised();
}

bool GameManager::update()
{
	// cede para o device pelo tempo minimo suficiente
	// para executar processamentos devidos
	s3eDeviceYield(0);

	// atualiza device com o estado do teclado atual, vindo do OS
	s3eKeyboardUpdate();

	// cálculo do delta para essa iteração do update
	uint64 delta = s3eTimerGetMs() - timer;
	timer += delta;
	delta = NanoMath::clamp( delta, 0, MAX_DELTA );

	// caso tenhamos uma tela associada ao Game Manager
	// a atualizamos
	if( currentScreen )
		currentScreen->update( uint(delta) );
	
	// caso tenhamos recebido um pedido para desligar,
	// o jogo acaba.
	gameOver |= s3eDeviceCheckQuitRequest();
	return !gameOver;
}

void GameManager::draw()
{
	Iw2DSurfaceClear(0xffffffff);

	if( currentScreen )
		currentScreen->draw();

	Iw2DSurfaceShow();
}

void GameManager::setCurrentScreen( ScreenPtr screen )
{
	currentScreen = screen;
	currentScreen->setPosition( Point( 0, 0 ) );
	updateCurrentScreenSize( Point( Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight() ) );
}

void GameManager::updateCurrentScreenSize( Point size )
{
	if( currentScreen ) 
		currentScreen->setSize( size );
}

bool GameManager::playSound( SoundPtr sound, int times )
{
	int channel = s3eSoundGetFreeChannel();
	bool hasChannel = ( channel == -1 );
	if( hasChannel )
		s3eSoundChannelPlay( channel, sound->buffer, sound->size, times, 0 );
	
	return hasChannel;
}

bool GameManager::loadTexts( int id )
{
	// TODO: checar linguas disponíveis?

	// pegamos um arquivo de texto baseado na língua selecionada.
	char filePath[MAX_FILENAME_LENGTH];
	sprintf(filePath, "texts\\%d.txt", id);		// <--- TODO: desmarretar MAX_FILENAME_LENGTH
	s3eFile *file = s3eFileOpen( filePath, "rb" );

	// lendo textos do arquivo
	if( file )
	{
		char line[TEXT_MAXCHARSLINE];
		while( s3eFileReadString(line, TEXT_MAXCHARSLINE, file ) )
			texts.push_back(line);

		// garante que índice dos textos está consistente
		// com enum AppTextsIds
		return NUMBER_OF_TEXTS == texts.size();
	}

	return false;
}

void GameManager::singlePointerButtonCB( void *systemData, void *userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

    const s3ePointerEvent* ev = (s3ePointerEvent*) systemData;
  
	if( currentScreen )
		currentScreen->pointerButtonCB( ev );
}

void GameManager::singlePointerMotionCB( void *systemData, void *userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

    const s3ePointerMotionEvent* ev = (s3ePointerMotionEvent*) systemData;
  
	if( currentScreen )
		currentScreen->pointerMotionCB( ev );
}

void GameManager::multiPointerButtonCB( void *systemData, void *userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	const s3ePointerTouchEvent* ev = (s3ePointerTouchEvent*) systemData;

	if( currentScreen )
		currentScreen->touchButtonCB( ev );
}

void GameManager::multiPointerMotionCB( void *systemData, void *userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	const s3ePointerTouchMotionEvent* ev = (s3ePointerTouchMotionEvent*) systemData;

	if( currentScreen )
		currentScreen->touchMotionCB( ev );
}

// TODO: verificar S3E Pointer Multi-Touch Example para exemplo de implementação
// para múltiplos toques
/*void GameManager::multiPointerButtonCB( void *systemData, void *userData )
{
}

void GameManager::multiPointerMotionCB( void *systemData, void *userData )
{
}*/

void GameManager::devicePauseCB( void* systemData, void* userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	if( currentScreen )
		currentScreen->devicePauseCB();
}

void GameManager::deviceUnpauseCB( void* systemData, void* userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	if( currentScreen )
		currentScreen->deviceUnpauseCB();
}

void GameManager::screenSizeChangeCB( void* systemData, void* userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	const s3eSurfaceOrientation* ev = (s3eSurfaceOrientation*) systemData;
	
	// atualiza tamanho da tela do aparelho e repassa evento
	deviceScreenSize = Point( ev->m_Width, ev->m_Height);
	updateCurrentScreenSize( deviceScreenSize );
}

void GameManager::keyboardKeyCB( void* systemData, void* userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	const s3eKeyboardEvent* ev = (s3eKeyboardEvent*) systemData;

	if( currentScreen )
		currentScreen->keyboardKeyCB( ev );
}

void GameManager::keyboardCharCB( void* systemData, void* userData )
{
	// recupera controle do sistema operacional e atualiza pointer
    s3eDeviceUnYield();
    s3ePointerUpdate();

	const s3eKeyboardCharEvent* ev = (s3eKeyboardCharEvent*) systemData;

	if( currentScreen )
		currentScreen->keyboardCharCB( ev );
}