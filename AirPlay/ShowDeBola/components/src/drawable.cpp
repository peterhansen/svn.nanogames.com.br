
#include <IwGx.h>

#include <components\defines.h>
#include <components\drawable.h>

#ifdef IW_DEBUG
#include <components\gamemanager.h>
#endif

ClipStack* Drawable::clipStack = NULL;

Drawable::Drawable()
	: visible( true ),
	clipTest( true ),
	viewport( RectanglePtr() ),
	position( Point( 0, 0 ) ),
	size( Point( 0, 0 ) ),
	angle( iwangle( 0 ) ),
	refRatio( Point( 0, 0 ) ),
	refPosition( Point( 0, 0 ) ),
	color( CreateColor( 255, 255, 255, 255 ) ),
	transform( IW_2D_IMAGE_TRANSFORM_NONE )
{}

void Drawable::pushClip()
{
	if( clipStack->currentStackSize >= MAX_CLIP_STACK_SIZE )
	{
		IwDebugErrorShow( "ERRO: Tentou-se adicionar uma �rea de clipping acima da altura m�xima permitida da pilha (%d).", MAX_CLIP_STACK_SIZE );
		return;
	}
	
	CIwRect currentClip = IwGxGetScissorScreenSpace();

	clipStack->stack[ clipStack->currentStackSize++ ].set( Point(currentClip.x, currentClip.y) , Point(currentClip.w, currentClip.h) );
	if( !viewport ) {
		Rectangle prevClip = clipStack->stack[ clipStack->currentStackSize - 1 ];
		clipStack->stack[ clipStack->currentStackSize ].set( prevClip.getPosition(), prevClip.getSize() );
	}
	else
		clipStack->stack[ clipStack->currentStackSize ] = clipStack->stack[ clipStack->currentStackSize - 1 ].calcIntersection( *viewport );
}

void Drawable::popClip()
{
	if ( clipStack->currentStackSize > 0 ) {
		Rectangle clip = clipStack->stack[ --clipStack->currentStackSize ];
		IwGxSetScissorScreenSpace( clip.getPosition().x, clip.getPosition().y, clip.getSize().x, clip.getSize().y );
	} 
	else
		IwDebugErrorShow( "ERRO: Tentou-se remover uma �rea de clipping da pilha quando ela estava vazia." );
}

void Drawable::draw()
{
	if( !visible ) return;

	if ( clipTest )
	{
		pushClip();
		clipStack->translate += position ;

		Rectangle clip = clipStack->stack[ clipStack->currentStackSize ];
		clip = clip.calcIntersection( Rectangle( clipStack->translate, size ) );

		Point clipSize = clip.getSize();
		if ( clipSize.x > 0 && clipSize.y > 0 ) {
			Point clipPosition = clip.getPosition();
			IwGxSetScissorScreenSpace( clipPosition.x, clipPosition.y, clipSize.x + 1, clipSize.y + 1 );

			#ifdef IW_DEBUG
				// desenhando ret�ngulo envolvente
				CIwColour c = Iw2DGetColour();
				Iw2DSetColour( 0xFFFF00FF );
				Iw2DDrawRect( clipPosition, clipSize );
				Iw2DSetColour( c );
				Iw2DFinishDrawing();
			#endif

			saveColorState();
			saveImageTransformState();
			
			// compondo cor da pilha
			Iw2DSetColour( ComposeColor( COLOR_STATE, color ) );

			Iw2DSetImageTransform( transform );

			saveTransformState();

			// TODO: melhorar, para n�o se criar a cada
			// draw uma nova matriz
			CIwMat2D rot;
			rot.SetRot( angle, CIwVec2( position + refPosition ) );
			Iw2DSetTransformMatrix( rot );

			paint();

			
			#ifdef IW_DEBUG
				// desenhando referencePosition
				setColorState( CreateColor( 0, 0, 200, 255 ) );
				Point positionBBox = Point( position.x, NanoMath::clamp( position.y + refPosition.y, 0, GameManager::getDeviceScreenSize().y ) );

				Iw2DFillRect( positionBBox/*position + refPosition*/, Point( 1, 1 ) );
			#endif

			restoreColorState();
			restoreImageTransformState();
			restoreTransformState();

			Iw2DFinishDrawing();
		}

		clipStack->translate -= position;
		popClip();
	}
	else
	{
		clipStack->translate += position;
		paint();
		clipStack->translate -= position;
	}
}	

void Drawable::setSize( Point size )
{
	Point newRefPosition = Point( ( refRatio.x * size.x ) >> IW_GEOM_POINT, ( refRatio.y * size.y ) >> IW_GEOM_POINT );
		
	position -= newRefPosition - refPosition;
	refPosition = newRefPosition;

	this->size = size;
}

bool Drawable::contains( Point p )
{
	return Rectangle( position, size ).contains( p );
}

void Drawable::init()
{
	if(!clipStack)
	{
		clipStack = new ClipStack();
		clipStack->currentStackSize = 0;
		clipStack->translate = Point( 0,0 );
		clipStack->stack = CIwArray<Rectangle>(Drawable::MAX_CLIP_STACK_SIZE);
		
		for( int i = 0; i < MAX_CLIP_STACK_SIZE; i++ )
			clipStack->stack.push_back(Rectangle());
	}
}

void Drawable::terminate()
{
	if( clipStack ) 
		delete clipStack;
}

void Drawable::setTransformation( Transform transform )
{
	this->transform = transform;
	// TODO: lidar com troca de tamanho e de posi��o da posi��o de refer�ncia para transforma��es que n�o s�o mirror.
}

void Drawable::setRefRatio( Point refRatio )
{
	this->refRatio = refRatio;
	refPosition = Point( ( refRatio.x * size.x ) >> IW_GEOM_POINT, ( refRatio.y * size.y ) >> IW_GEOM_POINT );

	// atualiza posi��o, levando em considera��o a nova posi��o de refer�ncia
	setPosition(position);
}

void Drawable::setViewPort( Rectangle viewport )
{
	this->viewport = RectanglePtr( new Rectangle( viewport ) );
}

DrawablePtr Drawable::getDrawableAt( Point p )
{
	return DrawablePtr( visible && contains(p)? this: NULL);
}