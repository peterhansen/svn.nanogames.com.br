
#include <components\defines.h>
#include <components\drawable.h>
#include <components\drawablegroup.h>

DrawablePtr DrawableGroup::getDrawable( int index )
{
	return drawables[ index ];
}

DrawablePtr DrawableGroup::getDrawableAt( Point p )
{
	if( contains( p ) )
	{
		p -= position;
		for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
			return drawables[ i ]->getDrawableAt( p );
		
		return DrawablePtr( this );
	}
	return DrawablePtr();
}

int DrawableGroup::insertDrawable( DrawablePtr drawable )
{
	return drawables.push_back(drawable);
}

void DrawableGroup::removeDrawable( DrawablePtr drawable )
{
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
	{
		DrawablePtr d = drawables[ i ];
		if( drawable.get() == d.get() )
			drawables.erase( i, i + 1 );
		break;
	}
}

void DrawableGroup::removeDrawableAt( int index )
{
	drawables.erase( index, index + 1 );
}

void DrawableGroup::removeAllDrawables()
{
	drawables.clear_optimised();
}

void DrawableGroup::paint()
{
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
		drawables[ i ]->draw();
}

void DrawableGroup::update( unsigned int dt )
{
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
		drawables[ i ]->update( dt );
}