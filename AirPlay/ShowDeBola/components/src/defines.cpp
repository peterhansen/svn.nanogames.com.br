#include <components\defines.h>

CIwColour CreateColor( int r, int g, int b, int a )
{
	r = NanoMath::clamp( r, 0, 255 );
	g = NanoMath::clamp( g, 0, 255 );
	b = NanoMath::clamp( b, 0, 255 );
	a = NanoMath::clamp( a, 0, 255 );
	CIwColour c = CIwColour();
	c.Set( r, g, b, a );
	return c;
}

CIwColour ComposeColor( CIwColour c1, CIwColour c2 )
{
	return CreateColor( (c1.r * c2.r) / 255, (c1.g * c2.g) / 255, (c1.b * c2.b) / 255, (c1.a * c2.a) / 255 );
}