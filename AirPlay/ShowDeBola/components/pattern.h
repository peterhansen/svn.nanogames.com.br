/** \file sprite.h
	30/05/2011
*/

#include <components\defines.h>
#include <components\drawable.h>

#ifndef PATTERN_H
#define PATTERN_H

/** \class Pattern 
	\brief Padrão que se repete ao longo de seu tamanho.
*/
class Pattern : public Drawable
{
public:
	/** Constrói pattern usando uma cor como padrão. */
	Pattern( Color color );

	/** Constrói pattern usando uma imagem como padrão. */
	Pattern( ImagePtr image );

	/** Retorna tamanho da imagem original usada no padrão. Caso esse padrão
	não seja de uma imagem, esse tamanho será (0,0). */
	inline Point getImageSize() const { return image? Point( image->GetWidth(), image->GetHeight() ): Point( 0, 0 ); }

protected:
	/** Imagem usada no desenho do padrão. Caso esse ponteiro seja nulo, 
	estamos desenhando um pattern de cor. */
	ImagePtr image;

	virtual void paint();
};
#endif