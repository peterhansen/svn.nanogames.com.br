/** \file
	drawableimage.h
	25/05/2011
*/
#include <IwGxTypes.h>

#include <components\defines.h>
#include <components\drawable.h>

#ifndef DRAWABLEIMAGE_H
#define DRAWABLEIMAGE_H

/** \class DrawableImage 
	\brief Representa uma imagem desenhável na tela.
*/
class DrawableImage : public Drawable
{
public:
	DrawableImage();
	DrawableImage( ImagePtr image );

	/** Retorna imagem a ser desenhada. */
	inline Image* getImage() { return image.get(); }

	/** Determina se a imagem deve preencher todo seu tamanho. */
	inline void setScaleToSize( bool scaleToSize ) { this->scaleToSize = scaleToSize; }

protected:
	ImagePtr image;				//!< Imagem encapsulada
	bool scaleToSize;			//!< Caso a imagem deva sofrer escala para que preencha o tamanho de sua borda.
	void paint();
};
#endif