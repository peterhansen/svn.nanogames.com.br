/*
 * Copyright (C) 2001-2008 Ideaworks3D Ltd.
 * All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Ideaworks3D Ltd.
 * This file consists of source code released by Ideaworks3D Ltd under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

//-----------------------------------------------------------------------------
/*!
	\file IwGxInit_Bespoke.cpp
	\brief Auto-generated file which links in only the pixel drawing loops used previously.
*/
//-----------------------------------------------------------------------------
// AUTO-GENERATED File. Do Not Edit.
//This file is generated in IwGxTerminate() using the data recorded in IwGxScanloopUsage.bin
//

#include "s3eTypes.h"
// self contained forward defines
typedef struct _scanLine ScanLine;

typedef void (*IwGxScanFunc)(ScanLine* );
extern void IwRendInitScanFunc(uint32 , IwGxScanFunc );
extern void _IwGxFinishSWInit();

extern int g_RendInitType;

// externs for all the functions mentioned below
extern void IwRendConnectStandard();
extern void IwRend_EnableEnvMap();
typedef void (*IwPolyRendrFunc)(uint32 *);
extern void SetLinkPipe(int32 , IwPolyRendrFunc );
extern void PolyRendrLINE(uint32*);
extern void PolyRendrLINE3D(uint32*);
extern void PolyRendrSPRITE(uint32*);
extern void PolyRendrPOLY_F(uint32*);
extern void PolyRendrPOLY_G(uint32*);
extern void PolyRendrPOLY_G(uint32*);
extern void PolyRendrPOLY_FT(uint32*);
extern void PolyRendrPOLY_FT(uint32*);
extern void PolyRendrPOLY_GTC(uint32*);
extern void Scanloop0(ScanLine* );
extern void Scanloop24(ScanLine* );
extern void Scanloop56(ScanLine* );
extern void Scanloop88(ScanLine* );
extern void Scanloop120(ScanLine* );
extern void Scanloop152(ScanLine* );
extern void Scanloop3(ScanLine* );
extern void Scanloop4(ScanLine* );
extern void Scanloop19(ScanLine* );
extern void Scanloop36(ScanLine* );
extern void Scanloop38(ScanLine* );
extern void Scanloop54(ScanLine* );
extern void Scanloop133(ScanLine* );
extern void Scanloop134(ScanLine* );
extern void Scanloop147(ScanLine* );
extern void Scanloop149(ScanLine* );
extern void Scanloop150(ScanLine* );
extern void Scanloop158(ScanLine* );

//-------------------------------------------------------------
void IwGxInit_Bespoke()
{

    IwRendConnectStandard();
    _IwGxFinishSWInit();
    IwRend_EnableEnvMap();
    SetLinkPipe( 0, PolyRendrLINE);
    SetLinkPipe( 1, PolyRendrLINE3D);
    SetLinkPipe( 20, PolyRendrSPRITE);
    SetLinkPipe( 32, PolyRendrPOLY_F);
    SetLinkPipe( 33, PolyRendrPOLY_G);
    SetLinkPipe( 35, PolyRendrPOLY_G);
    SetLinkPipe( 36, PolyRendrPOLY_FT);
    SetLinkPipe( 38, PolyRendrPOLY_FT);
    SetLinkPipe( 39, PolyRendrPOLY_GTC);
    IwRendInitScanFunc(0, Scanloop0);
    IwRendInitScanFunc(24, Scanloop24);
    IwRendInitScanFunc(56, Scanloop56);
    IwRendInitScanFunc(88, Scanloop88);
    IwRendInitScanFunc(120, Scanloop120);
    IwRendInitScanFunc(152, Scanloop152);
    IwRendInitScanFunc(259, Scanloop3);
    IwRendInitScanFunc(260, Scanloop4);
    IwRendInitScanFunc(275, Scanloop19);
    IwRendInitScanFunc(292, Scanloop36);
    IwRendInitScanFunc(294, Scanloop38);
    IwRendInitScanFunc(310, Scanloop54);
    IwRendInitScanFunc(389, Scanloop133);
    IwRendInitScanFunc(390, Scanloop134);
    IwRendInitScanFunc(403, Scanloop147);
    IwRendInitScanFunc(405, Scanloop149);
    IwRendInitScanFunc(406, Scanloop150);
    IwRendInitScanFunc(414, Scanloop158);
    g_RendInitType = 45;
}
