/** \file label.h
	31/05/2011
*/
#include <string>

#include <components\defines.h>
#include <components\drawable.h>

#ifndef LABEL_H
#define LABEL_H

/** \class Label
	\brief Classe utilizada para desenho de textos na tela.
*/
class Label : public Drawable
{
public:
	Label( FontPtr font, std::string text );
	Label( FontPtr font, std::wstring text );

	/** Determina a formatação usada no desenho do Label. */
	void setFormat( FontHAlignment horAlign = IW_GX_FONT_ALIGN_LEFT, 
					FontVAlignment verAlign = IW_GX_FONT_ALIGN_TOP, 
					FontFormatFlags formatFlags = IW_GX_FONT_DEFAULT_F );

	/** Determina novo conteúdo de texto para a Label */
	void setText( std::string text );

	virtual void setText( std::wstring text );

	/** Retorna fonte utilizada por essa Label */
	FontPtr getFont() { return font; }

	void autoSize();

	void setFont( FontPtr font );

protected:
	FontPtr font;					//!< Fonte usada para o texto do label.
	std::wstring text;				//!< Texto do conteúdo do label.

	FontHAlignment horAlign;		//!< Alinhamento horizontal da label.
	FontVAlignment verAlign;		//!< Alinhamento vertical da label.
	FontFormatFlags formatFlags;	//!< Flags de formatação da label.

	virtual void paint();
};
#endif