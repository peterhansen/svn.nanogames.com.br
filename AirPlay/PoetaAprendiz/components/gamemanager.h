/** \file gamemanager.h
	2011-06-01
*/

#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

#include <vector>

#include <Iw2D.h>

#include <components\defines.h>
#include <components\drawable.h>
#include <components\sound.h>

// TODO: remover a definição de Screen daqui
#include <components\listeners.h>
#include <src\texts.h>

/** \class GameManager
	\brief Classe responsável por controlar o loop principal de jogo e intermediar os eventos do aparelho.
*/
class GameManager
{
public:

	/** Inicializa variáveis de controle da aplicação e pilha de clipping e registra funções de evento.
		O Game Manager assume responsabilidade sobre todos os eventos que vêm do aparelho, delegando-os
		para a tela corrente.
		@see currentScreen 
	*/
	static bool init();
	static void terminate();

	/** Método de atualização da aplicação. Deve ser executado no loop principal.
		@return Caso retorne falso, sai-se da aplicação.
	*/
	static bool update();

	/** Método de desenho da aplicação */
	static void draw();

	/** Determina a tela corrente do jogo, à qual são delegados todos os eventos do aparelho. */
	static void setCurrentScreen( ScreenPtr screen );

	/** Toca um som, havendo canal disponível para tal.
		@param sound Som a ser tocado.
		@param times Quantidade de vezes que o som deve ser tocado. Utiliza-se 0 para 
		que ele repita até que seja explicitamente interrompido.
		@return Caso tenha sido encontrado um canal disponível.
	*/
	static bool playSound( SoundPtr sound, int times = 1 );

	/** Carrega textos da língua selecionada para a array de textos do Game Manager. 
		@return Caso a aplicação não dê suporte para a língua selecionada, nenhuma mudança acontece e 
		a função retorna false. 
		@see texts
	*/
	static bool loadTexts( int id );

	/** Retorna texto previamente carregado. */
	static inline std::string getText( AppTextsIds id ) { return texts[id]; }

	/** Avisa para o Game Manager que o jogo deve ser encerrado ao
	fim do próximo loop principal. */
	static inline void endGame() { gameOver = true; }

	/** Retorna tamanho da tela do aparelho */
	static inline Point getDeviceScreenSize() { return deviceScreenSize; }

	/** Cria imagem. 
		@return id da imagem. 
	*/
	static int createImage( std::string path );
	
	/** Retorna imagem previamente carregada. */
	static ImagePtr getImage( int id ) { return images[id]; }

protected:

	static const int TEXT_MAXCHARSLINE = 300;	//!< Número máximo de caracteres por linha na leitura de um arquivo de texto. */
	static const unsigned int MAX_DELTA = 100;	//!< Limite do delta por frame.

	static s3eBool gameOver;			//!< Determina se a aplicação pode escapar do loop de jogo e terminar.
	static uint64 timer;				//!< Cronômetro do jogo.
	static ScreenPtr currentScreen;		//!< Tela corrente do jogo, quem é desenhada no e recebe os eventos do aparelho.

	static bool hasMultiTouch;				//!< Caso o aparelho permita o uso de toques múltiplos.
	static Point deviceScreenSize;			//!< Tamanho atualizado da tela do aparelho.

	static CIwArray<std::string> texts;		//!< Textos utilizados pela aplicação. @see loadTexts

	/** Atualiza dimensões da tela corrente. */
	static void updateCurrentScreenSize( Point size );

	// funções de callback
	static void singlePointerButtonCB( void* systemData, void* userData );
	static void singlePointerMotionCB( void* systemData, void* userData );
	static void multiPointerButtonCB( void* systemData, void* userData );
	static void multiPointerMotionCB( void* systemData, void* userData );
	static void keyboardKeyCB( void* systemData, void* userData );
	static void keyboardCharCB( void* systemData, void* userData );
	static void devicePauseCB( void* systemData, void* userData );
	static void deviceUnpauseCB( void* systemData, void* userData );
	static void screenSizeChangeCB( void* systemData, void* userData );

	// resource management
	static CIwArray<ImagePtr> images;

private:
	// classe estática
	GameManager();
	~GameManager();
};
#endif

