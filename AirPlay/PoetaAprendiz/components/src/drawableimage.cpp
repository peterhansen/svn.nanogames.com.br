#include <components/drawableimage.h>
#include <Iw2D.h>

#include <components\drawableimage.h>
#include <components\defines.h>

DrawableImage::DrawableImage()
	: Drawable(),
	image( boost::shared_ptr<Image>() ),
	scaleToSize( true )
{}

DrawableImage::DrawableImage( ImagePtr image )
	: Drawable(),
	image(image),
	scaleToSize( true )
{
	size = Point( image->GetWidth(), image->GetHeight() );
}


DrawableImage::DrawableImage( std::string path ) : 
	Drawable(),
	image( ImagePtr( Iw2DCreateImage( path.data() ) ) ),
	scaleToSize( true )	
{
	size = Point( image->GetWidth(), image->GetHeight() );
}


void DrawableImage::paint() {
	Iw2DSetColour( color );

	if( !scaleToSize )
		Iw2DDrawImage( image.get(), clipStack->translate );
	else
		Iw2DDrawImage( image.get(), clipStack->translate, Point( ( int ) ( size.x * scale ), ( int ) ( size.y * scale ) ) );
}
