
#include <components\rectangle.h>
#include <components\defines.h>

Rectangle Rectangle::calcIntersection( Rectangle r )
{
	Rectangle intersection( Point(0,0), Point(0,0) );
	const int right1 = box.x + box.w;
	const int bottom1 = box.y + box.h;
	
	const Point rPosition = r.getPosition();
	const Point rSize = r.getSize();

	const int right2 = r.getPosition().x + r.getSize().x;
	const int bottom2 = r.getPosition().y + r.getSize().y;
		
	if ( box.x <= right2 && right1 >= rPosition.x && box.y <= bottom2 && bottom1 >= rPosition.y ) {
		Point newPos = Point( NanoMath::max( box.x, rPosition.x ), NanoMath::max( box.y, rPosition.y ) );
		Point newSize = Point( NanoMath::min( right1, right2 ), NanoMath::min( bottom1, bottom2 ) ) - newPos;
		intersection.set( newPos, newSize );
	}
	return intersection;
}

bool Rectangle::contains( Point p )
{
	return p.x >= box.x && p.y >= box.y && p.x < box.x + box.w && p.y < box.y + box.h;
}