#include <Iw2D.h>
#include <IwGx.h>

#include <components\defines.h>
#include <components\sprite.h>

/** Caracter utilizado para indicar para o leitor de descritores o início de uma sequência de frames com durações fixas (o primeiro valor lido é a duração dos frames, e em seguida os índices da sequência). */
#define FRAMESET_DESCRIPTOR_SEQUENCE_FIXED 'f'

/** Caracter utilizado para indicar para o leitor de descritores o início de uma sequência de frames com durações variáveis. Os valores presentes na sequência devem sempre obedecer a ordem índice - duração. */
#define FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE 'v'


typedef enum {
	READ_STATE_FRAMES_SIZE,
	READ_STATE_TOTAL_FRAMES,
	READ_STATE_FRAMES_INFO,
	READ_STATE_READ_SEQUENCE_TYPE,
	READ_STATE_FIXED_READ_TIME,
	READ_STATE_FIXED_READ_INDEXES,
	READ_STATE_VARIABLE_READ_TIME,
	READ_STATE_VARIABLE_READ_INDEX
} readState;


Sprite::Sprite( ImagePtr image, std::string seqFile ) : Drawable(),
	image( image ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( true ),
	frameIndex( 0 ),
	paused( false ),
	listener( NULL )
{
	readSequence( seqFile );
}


Sprite::Sprite( std::string imageFile, std::string seqFile ) : Drawable(),
	image( ImagePtr( Iw2DCreateImage( imageFile.data() ) ) ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( true ),
	frameIndex( 0 ),
	paused( false ),
	listener( NULL )
{
	readSequence( seqFile );
}


Sprite::Sprite( std::string prefix ) : Drawable(),
	image( ImagePtr( Iw2DCreateImage( ( prefix + ".png" ).data() ) ) ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( true ),
	frameIndex( 0 ),
	paused( false ),
	listener( NULL )
{
	readSequence( prefix + ".bin" );
}


Sprite::~Sprite() {
	sequences.clear_optimised();
}


void Sprite::setFrameIndex( uint8 frameIndex ) {
	this->frameIndex = NanoMath::absmod( frameIndex, sequences[sequenceIndex].size() );
}


void Sprite::setSequenceIndex( uint8 sequenceIndex ) {
	this->sequenceIndex = sequenceIndex; 
	frameIndex = 0;
}


void Sprite::update( unsigned int dt ) {
	// se o frame tiver duração 0 (zero), o controle da animação é feito externamente, através de chamadas explícitas
	// a nextFrame, previousFrame e etc.
	int frameTime = currentSequence[ frameIndex ].y;
	if ( frameTime > 0 && !paused ) {
		accTime += dt;
		if ( accTime >= frameTime ) {
			// trocou de frame
			accTime %= frameTime;
			nextFrame();

			if ( listener != NULL ) {
				listener->OnFrameChanged( listenerID, frameIndex );

				if ( frameIndex == 0 )
					listener->OnSequenceEnded( listenerID, sequenceIndex );
			}

		//	for ( uint i = 0; i < listeners.size(); ++i ) {
		//		SpriteListenerStruct l = listeners[ i ];
		//		l.listener->OnFrameChanged( l.id, frameIndex );

		//		if ( frameIndex == 0 )
		//			l.listener->OnSequenceEnded( l.id, sequenceIndex );
		//	}
		}
	}
}


void Sprite::addListener( int id, SpriteListener* listener ) {
	this->listener = listener;
	this->listenerID = id;
}


void Sprite::removeListener( int id, SpriteListener* listener ) {
	listener = NULL;
	id = -1;
}


Frame Sprite::getCurrentFrame() {
	return frames[ frameIndex ];
}


void Sprite::paint() {
	Iw2DSetColour( color );

	Rectangle bounds = Rectangle( currentFrame.bounds );
	Point offset = currentFrame.offset;
	if ( transform & IW_2D_IMAGE_TRANSFORM_FLIP_X )
		offset.x = totalSize.x - offset.x - bounds.getSize().x;
	if ( transform & IW_2D_IMAGE_TRANSFORM_FLIP_Y )
		offset.y = totalSize.y - offset.y - bounds.getSize().y;


	if( !scaleToSize ) {
		Iw2DDrawImageRegion( image.get(), clipStack->translate + offset, bounds.getSize(), bounds.getPosition(), bounds.getSize() );
	} else {
		Point s = Point( ( int ) ( size.x * scale ), ( int ) ( size.y * scale ) );

		float scaleX = s.x / ( float ) totalSize.x;
		float scaleY = s.y / ( float ) totalSize.y;
		
		Point scaledFrameSize = Point( ( int ) ( scaleX * bounds.getSize().x ), ( int ) ( scaleY * bounds.getSize().y ) );
		Point scaledOffset = Point( ( int ) ( scaleX * offset.x ), ( int ) ( scaleY * offset.y ) );

		Iw2DDrawImageRegion( image.get(), clipStack->translate + scaledOffset, scaledFrameSize, bounds.getPosition(), bounds.getSize() );
	}
}


void Sprite::readSequence( std::string path ) {
	s3eFile* file = s3eFileOpen( path.data(), "rb" );
	
	char line[ SEQUENCE_MAXCHARSLINE ];
	int8 currentFrameRead = 0;
	int16 time = 0;
	char* charPtr = NULL;
	int8 totalFrames = 0;
	Frame f;
	char* temp;
	Sequence s;

	readState state = READ_STATE_FRAMES_SIZE;
	bool continueLine = false;

	while ( continueLine || s3eFileReadString( line, SEQUENCE_MAXCHARSLINE, file ) ) {
		if ( !continueLine )
			charPtr = strtok( line, " \n\r\t" );

		continueLine = false;

		switch ( state ) {
			case READ_STATE_FRAMES_SIZE:
				totalSize.x = readNumber( &charPtr );
				totalSize.y = readNumber( &charPtr );
				setSize( totalSize );
				state = READ_STATE_TOTAL_FRAMES;
			break;

			case READ_STATE_TOTAL_FRAMES:
				totalFrames = readNumber( &charPtr );
				state = READ_STATE_FRAMES_INFO;
			break;

			case READ_STATE_FRAMES_INFO:
			{
				f = Frame();
				int x = readNumber( &charPtr );
				int y = readNumber( &charPtr );
				Point p = Point( x, y );

				int w = readNumber( &charPtr );
				int h = readNumber( &charPtr );
				Point s = Point( w, h );

				f.bounds.set( p, s );
				f.offset.x = readNumber( &charPtr );
				f.offset.y = readNumber( &charPtr );
				frames.push_back( f );

				++currentFrameRead;

				if ( currentFrameRead >= totalFrames ) {
					state = READ_STATE_READ_SEQUENCE_TYPE;
				}
			}
			break;

			case READ_STATE_READ_SEQUENCE_TYPE:
				temp = charPtr;//strtok( charPtr, " " );
				if ( temp ) {
					char c = temp[ 0 ];
					charPtr = strtok( NULL, " " );
					state = ( c == FRAMESET_DESCRIPTOR_SEQUENCE_FIXED ? READ_STATE_FIXED_READ_TIME : READ_STATE_VARIABLE_READ_TIME );
					continueLine = true;
				}
			break;

			case READ_STATE_FIXED_READ_TIME:
				time = readNumber( &charPtr );
				state = READ_STATE_FIXED_READ_INDEXES;
				continueLine = true;
			break;

			case READ_STATE_FIXED_READ_INDEXES:
				//charPtr = strtok ( charPtr, " " );

				//while ( charPtr && ( &charPtr != NULL ) ) {
				while ( charPtr ) {
					s.push_back( Point( readNumber( &charPtr ), time ) );
					//charPtr = strtok( NULL, " " );
				}
				state = READ_STATE_READ_SEQUENCE_TYPE;
			break;

			case READ_STATE_VARIABLE_READ_TIME:
				time = readNumber( &charPtr );
				if ( time >= 0 ) {
					state = READ_STATE_VARIABLE_READ_INDEX;
					continueLine = true;
				}
				// não é necessário tratar o "else", pois o tratamento adequado já foi feito dentro de readNumber()
			break;

			case READ_STATE_VARIABLE_READ_INDEX:
				int frameIndex = readNumber( &charPtr );
				// Se for um índice válido, o insere no array, e lê a próxima informação (duração do próximo frame)
				// Caso contrário, o tratamento já terá sido feito dentro de readNumber.
				if ( frameIndex >= 0 ) {
					s.push_back( Point( readNumber( &charPtr ), time ) );
					state = READ_STATE_VARIABLE_READ_TIME;
					continueLine = true;
				}
			break;
		}

		switch ( state ) {
			case READ_STATE_FIXED_READ_INDEXES:
			case READ_STATE_VARIABLE_READ_INDEX:
				if ( s.size() > 0 )
					sequences.push_back( s );

				s = Sequence();
			break;
		}
	}

	if ( s.size() > 0 )
		sequences.push_back( s );

	printf( "SEQUENCES: %d\n", sequences.size() );
	s3eFileClose( file );
}

