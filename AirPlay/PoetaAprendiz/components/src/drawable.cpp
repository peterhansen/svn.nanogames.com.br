
#include <IwGx.h>

#include <components\defines.h>
#include <components\drawable.h>

#ifdef IW_DEBUG
#include <components\gamemanager.h>
#endif

ClipStack* Drawable::clipStack = NULL;

Drawable::Drawable()
	: visible( true ),
	clipTest( true ),
	viewport( RectanglePtr() ),
	position( Point( 0, 0 ) ),
	size( Point( 0, 0 ) ),
	angle( iwangle( 0 ) ),
	refRatio( Point( 0, 0 ) ),
	refPosition( Point( 0, 0 ) ),
	color( CreateColor( 0xffffffff ) ),
	transform( IW_2D_IMAGE_TRANSFORM_NONE ),
	scale( 1.0f )
{}


void Drawable::pushClip() {
	if( clipStack->currentStackSize >= MAX_CLIP_STACK_SIZE )
	{
		IwDebugErrorShow( "ERRO: Tentou-se adicionar uma �rea de clipping acima da altura m�xima permitida da pilha (%d).", MAX_CLIP_STACK_SIZE );
		return;
	}
	
	CIwRect currentClip = IwGxGetScissorScreenSpace();

	clipStack->stack[ clipStack->currentStackSize++ ].set( Point(currentClip.x, currentClip.y) , Point(currentClip.w, currentClip.h) );
	if( !viewport ) {
		Rectangle prevClip = clipStack->stack[ clipStack->currentStackSize - 1 ];
		clipStack->stack[ clipStack->currentStackSize ].set( prevClip.getPosition(), prevClip.getSize() );
	}
	else
		clipStack->stack[ clipStack->currentStackSize ] = clipStack->stack[ clipStack->currentStackSize - 1 ].calcIntersection( *viewport );
}


void Drawable::popClip() {
	if ( clipStack->currentStackSize > 0 ) {
		Rectangle clip = clipStack->stack[ --clipStack->currentStackSize ];
		IwGxSetScissorScreenSpace( clip.getPosition().x, clip.getPosition().y, clip.getSize().x, clip.getSize().y );
	} 
	else
		IwDebugErrorShow( "ERRO: Tentou-se remover uma �rea de clipping da pilha quando ela estava vazia." );
}


void Drawable::draw() {
	if( !visible ) return;

	if ( clipTest )
	{
		pushClip();
		clipStack->translate += position;

		Rectangle clip = clipStack->stack[ clipStack->currentStackSize ];
		
		// TODO �rea de clip n�o retangular
		if ( angle == 0 )
			clip = clip.calcIntersection( Rectangle( clipStack->translate, size ) );

		if ( viewport )
			clip = clip.calcIntersection( *viewport.get() );

		Point clipSize = clip.getSize();
		if ( clipSize.x > 0 && clipSize.y > 0 ) {
			Point clipPosition = clip.getPosition();

			// TODO �rea de clip n�o retangular
			//if ( angle == 0 )
				IwGxSetScissorScreenSpace( clipPosition.x, clipPosition.y, clipSize.x + 1, clipSize.y + 1 );

			#ifdef IW_DEBUG
				// desenhando ret�ngulo envolvente
				CIwColour c = Iw2DGetColour();
				Iw2DSetColour( 0xFFFF00FF );
				Iw2DDrawRect( clipPosition, clipSize );
				Iw2DSetColour( c );
				Iw2DFinishDrawing();
			#endif

			saveColorState();
			saveImageTransformState();
			
			// compondo cor da pilha
			Iw2DSetColour( ComposeColor( COLOR_STATE, color ) );

			Iw2DSetImageTransform( transform );

			saveTransformState();

			CIwMat2D previousMatrix = Iw2DGetTransformMatrix();

			// TODO: melhorar, para n�o se criar a cada
			// draw uma nova matriz
			CIwMat2D rot;
			rot.SetRot( angle, CIwVec2( position + refPosition ) );
			Iw2DSetTransformMatrix( rot.PostMult( previousMatrix ) );

			paint();

			Iw2DSetTransformMatrix( previousMatrix );

			
			#ifdef IW_DEBUG
				// desenhando referencePosition
				setColorState( CreateColor( 0, 0, 200, 255 ) );
				Point positionBBox = Point( position.x, NanoMath::clamp( position.y + refPosition.y, 0, GameManager::getDeviceScreenSize().y ) );

				Iw2DFillRect( positionBBox/*position + refPosition*/, Point( 1, 1 ) );
			#endif

			restoreColorState();
			restoreImageTransformState();
			restoreTransformState();

			Iw2DFinishDrawing();
		}

		clipStack->translate -= position;
		popClip();
	} else {
		clipStack->translate += position;
		paint();
		clipStack->translate -= position;
	}
}	


void Drawable::setSize( int16 width, int16 height ) {
	Point newRefPosition = Point( ( refRatio.x * width ) >> IW_GEOM_POINT, ( refRatio.y * height ) >> IW_GEOM_POINT );
		
	position -= newRefPosition - refPosition;
	setAnchor( newRefPosition );

	size.x = width;
	size.y = height;
}


bool Drawable::contains( Point p ) {
	// TODO oferecer op��o para detec��o por pixel
	// TODO contains deve levar visibilidade em conta?
	return isVisible() && Rectangle( position, size ).contains( p );
}


void Drawable::init() {
	if( !clipStack ) {
		clipStack = new ClipStack();
		clipStack->currentStackSize = 0;
		clipStack->translate = Point( 0,0 );
		clipStack->stack = CIwArray<Rectangle>(Drawable::MAX_CLIP_STACK_SIZE);
		
		for( int i = 0; i < MAX_CLIP_STACK_SIZE; i++ )
			clipStack->stack.push_back(Rectangle());
	}
}

void Drawable::terminate() {
	if( clipStack ) 
		delete clipStack;
}


void Drawable::setTransformation( Transform transform ) {
	this->transform = transform;
	// TODO: lidar com troca de tamanho e de posi��o da posi��o de refer�ncia para transforma��es que n�o s�o mirror.
}

void Drawable::setRefRatio( Point refRatio ) {
	setAnchor( ( refRatio.x * size.x ) >> IW_GEOM_POINT, ( refRatio.y * size.y ) >> IW_GEOM_POINT );

	// TODO atualiza posi��o, levando em considera��o a nova posi��o de refer�ncia
	// setPosition(position);
}

void Drawable::setAnchor( uint8 anchor ) {
	switch ( anchor ) {
		case ANCHOR_TOP_LEFT: 
			setAnchor( 0, 0 );
		break;

		case ANCHOR_TOP:
			setAnchor( getWidth() >> 1, 0 );
		break;
		
		case ANCHOR_TOP_RIGHT:
			setAnchor( getWidth(), 0 );
		break;
		
		case ANCHOR_LEFT:
			setAnchor( 0, getHeight() >> 1 );
		break;
		
		case ANCHOR_CENTER:
			setAnchor( getWidth() >> 1, getHeight() >> 1 );
		break;
		
		case ANCHOR_RIGHT:
			setAnchor( getWidth(), getHeight() >> 1 );
		break;
		
		case ANCHOR_BOTTOM_LEFT:
			setAnchor( 0, getHeight() );
		break;
		
		case ANCHOR_BOTTOM:
			setAnchor( getWidth() >> 1, getHeight() );
		break;
		
		case ANCHOR_BOTTOM_RIGHT:
			setAnchor( getWidth(), getHeight() );
		break;
	}
}


void Drawable::setAnchor( int anchorX, int16 anchorY ) {
	// TODO melhorar ordem de chamadas de setAnchor / setRefRatio
	refPosition.x = anchorX;
	refPosition.y = anchorY;

	refRatio.x = getWidth() > 0 ? anchorX / getWidth() : 0;
	refRatio.y = getHeight() > 0 ? anchorY / getHeight() : 0;
}


Point Drawable::getAnchorPosition( uint8 anchor ) {
	switch ( anchor ) {
		case ANCHOR_TOP_LEFT: 
			return position;

		case ANCHOR_TOP:
			return position + Point( getWidth() >> 1, 0 );
		
		case ANCHOR_TOP_RIGHT:
			return position + Point( getWidth(), 0 );
		
		case ANCHOR_LEFT:
			return position + Point( 0, getHeight() >> 1 );
		
		case ANCHOR_CENTER:
			return position + Point( getWidth() >> 1, getHeight() >> 1 );
		
		case ANCHOR_RIGHT:
			return position + Point( getWidth(), getHeight() >> 1 );
		
		case ANCHOR_BOTTOM_LEFT:
			return position + Point( 0, getHeight() );
		
		case ANCHOR_BOTTOM:
			return position + Point( getWidth() >> 1, getHeight() );
		
		case ANCHOR_BOTTOM_RIGHT:
			return position + Point( getWidth(), getHeight() );

		default:
			return position;
	}
}


void Drawable::setScale( float scale ) {
	this->scale = scale;
}

float Drawable::getScale() {
	return scale;
}


void Drawable::setViewPort( Rectangle viewport ) {
	this->viewport = RectanglePtr( new Rectangle( viewport ) );
}


DrawablePtr Drawable::getDrawableAt( Point p ) {
	return DrawablePtr( visible && contains(p)? this: NULL);
}