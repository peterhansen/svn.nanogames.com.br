
#include <components\defines.h>
#include <components\drawable.h>
#include <components\drawablegroup.h>


DrawablePtr DrawableGroup::getDrawable( int index ) {
	return drawables[ index ];
}


DrawablePtr DrawableGroup::getDrawableAt( Point p ) {
	if ( contains( p ) ) {
		p -= position;
		for ( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ ) {
			DrawablePtr d = drawables[ i ]->getDrawableAt( p );
			if ( d.get() != NULL )
				return d;
		}
		
		return DrawablePtr( this );
	}

	return DrawablePtr();
}


int DrawableGroup::insertDrawable( DrawablePtr drawable ) {
	return drawables.push_back(drawable);
}


void DrawableGroup::removeDrawable( DrawablePtr drawable ) {
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ ) {
		DrawablePtr d = drawables[ i ];
		if ( drawable.get() == d.get() )
			drawables.erase( i, i + 1 );
		break;
	}
}


void DrawableGroup::removeDrawableAt( int index ) {
	drawables.erase( index, index + 1 );
}


void DrawableGroup::removeAllDrawables() {
	drawables.clear_optimised();
}


void DrawableGroup::setDrawableIndex( uint oldIndex, uint newIndex ) {
	oldIndex = NanoMath::min( oldIndex, count() - 1 );
	newIndex = NanoMath::min( newIndex, count() - 1 );

	if ( oldIndex == newIndex )
		return;
	
	DrawablePtr old = drawables[ oldIndex ];
	if ( oldIndex < newIndex ) {
		for ( uint i = oldIndex; i < newIndex; ++i )
			drawables[ i ] = drawables[ i + 1 ];
	} else {
		for ( uint i = oldIndex; i > newIndex; --i )
			drawables[ i ] = drawables[ i - 1 ];
	}

	drawables[ newIndex ] = old;
}


void DrawableGroup::setDrawableIndex( DrawablePtr d, uint newIndex ) {
	int oldIndex = getDrawableIndex( d );
	if ( oldIndex >= 0 )
		setDrawableIndex( oldIndex, newIndex);
}


int DrawableGroup::getDrawableIndex( DrawablePtr d ) {
	return drawables.find( d );
}


uint DrawableGroup::count() {
	return drawables.size();
}


void DrawableGroup::paint() {
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
		drawables[ i ]->draw();
}


void DrawableGroup::update( unsigned int dt ) {
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
		drawables[ i ]->update( dt );
}