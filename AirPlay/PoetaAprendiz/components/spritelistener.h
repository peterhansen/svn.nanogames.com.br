#ifndef SPRITE_LISTENER_H
#define SPRITE_LISTENER_H

//#include <components\defines.h>


class SpriteListener {

public:

	virtual void OnFrameChanged( int id, int frameIndex ) = 0;

	virtual void OnSequenceEnded( int id, int sequenceIndex ) = 0;

};

#endif