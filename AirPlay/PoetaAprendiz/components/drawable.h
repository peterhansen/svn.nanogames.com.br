/** \file
	Drawable.h
	24/05/2011
	Baseado no codigo dos componentes feito para o J2ME por Peter Hansen.
*/
#include <components\defines.h>
#include <components\rectangle.h>

#ifndef DRAWABLE_H
#define DRAWABLE_H

/** \struct ClipStack
	\brief Pilha de clipping.
*/
struct ClipStack
{
	Point translate;				//!< Translação atual da pilha. Todos os desenhos devem ser transladados de acordo.
	int currentStackSize;			//!< Tamanho atual da pilha.
	CIwArray<Rectangle> stack;		//!< Pilha de retângulos envolventes das áreas de clipping.
};


enum {
	ANCHOR_TOP_LEFT,
	ANCHOR_TOP,
	ANCHOR_TOP_RIGHT,
	ANCHOR_LEFT,
	ANCHOR_CENTER,
	ANCHOR_RIGHT,
	ANCHOR_BOTTOM_LEFT,
	ANCHOR_BOTTOM,
	ANCHOR_BOTTOM_RIGHT,
};

/** \class Drawable
	\brief Representa um objeto desenhável na tela.
*/
class Drawable
{
public:
	static const unsigned short MAX_CLIP_STACK_SIZE = 32;		//!< Tamanho máximo para a pilha de clipping
	static void init();
	static void terminate();

	Drawable();
	virtual ~Drawable()	{}

	/** Retorna visibilidade do drawable */
	inline bool isVisible() const {
		return visible; 
	}

	/** Retorna coordenadas da origem do Drawable. Caso esse drawable 
	esteja inserido em algum grupo, essa posição é relativa a do grupo. */
	inline Point getPosition() const {
		return position;
	}


	inline Point getRefPosition() const {
		return position + refPosition; 
	}


	inline int16 getRefPosX() const {
		return position.x + refPosition.x;
	}


	inline int16 getRefPosY() const {
		return position.y + refPosition.y;
	}


	/** Retorna coordenadas absolutas do Drawable com relação à tela. */
	inline Point getAbsolutePosition() const
	{
		return position;
	}

	/** Retorna tamanho do drawable (em pixels). */
	inline Point getSize() const				{ return size; }
	
	/** Retorna posição X da origem do drawable. */
	inline int getPosX() const { return getPosition().x; }

	/** Retorna posição Y da origem do drawable. */
	inline int getPosY() const { return getPosition().y; }

	/** Retorna largura do drawable. */
	inline int getWidth() const { return getSize().x; }

	/** Retorna altura do drawable. */
	inline int getHeight() const { return getSize().y; }

	/** Determina visibilidade do drawable. */
	inline void setVisible( bool visible )		{ this->visible = visible; }

	/** Determina posição da origem do Drawable em pixels. Caso esse 
	drawable esteja inserido em algum grupo, essa posição é relativa a do grupo. */
	inline void setPosition( Point position ) {
		setPosition( position.x, position.y );
	}

	inline virtual void setPosition( int16 x, int16 y ) { 
		position.x = x;
		position.y = y;
	}

	inline void setAnchorPosition( Point pos ) {
		setAnchorPosition( pos.x, pos.y );
	}


	inline void setAnchorPosition( int16 x, int16 y ) {
		setPosition( x - refPosition.x, y - refPosition.y );
	}


	inline void move( Point dp ) { 
		move( dp.x, dp.y ); 
	}

	inline void move( int16 dx, int16 dy ) {
		setPosition( position.x + dx, position.y + dy ); 
	}

	/** Determina tamanho máximo do drawable (em pixels). */
	inline void setSize( Point size ) {
		setSize( size.x, size.y ); 
	}

	virtual void setSize( int16 width, int16 height );

	/** Retorna tonalidade de cor do Drawable. */
	inline Color getColor() const {
		return this->color;
	}

	/** Determina tonalidade de cor do Drawable. */
	inline void setColor( Color color )			
	{ 
		this->color = color;
	}


	inline void setColor( uint32 rgba ) {
		setColor( CreateColor( rgba ) );
	}


	/** Determina a transformação que deve ser aplicada ao desenho desse drawable.
		@see Transform 
	*/
	virtual void setTransformation( Transform transform );

	/** Determina posição do pixel de referência do drawable, com relação ao tamanho do drawable.
		@param refRatio Point cujas coordenadas devem ser ints convertidos para valores iwfixed.
		@see referencePosition
	*/
	void setRefRatio( Point refRatio );

	/**
	*/
	void setAnchor( uint8 anchor );

	void setAnchor( int anchorX, int16 anchorY );

	inline void setAnchor( Point anchor ) { setAnchor( anchor.x, anchor.y ); }

	Point getAnchorPosition( uint8 anchor );

	// TODO definir comportamento de scale x size
	void setScale( float scale );

	float getScale();

	/** Determina se o drawable deve realizar o teste de clipping antes de seu desenho. Nesse teste,
	o drawable só é desenhado caso sua área de desenho intercepte a área de clipping.
		@see clipStack
		@see Drawable::draw
	*/
	inline void setClipTest( bool isClipTest )	{ this->clipTest = isClipTest; }

	/** Restringe área de desenho do drawable a uma região determinada da tela. 
		@param viewport Retângulo envolvente da área de viewport.
	*/
	void setViewPort( Rectangle viewport );

	/** Atualiza o estado do drawable.
		@param dt delta que indica quanto tempo foi passado desde o último update.
	*/
	virtual void update( unsigned int dt ) {}

	/** Verifica se o ponto p está contido no drawable. */
	bool contains( Point p );

	/** Retorna o drawable o qual é incidido pelo ponto p. */
	DrawablePtr getDrawableAt( Point p );

	/** Retorna o ângulo de rotação desse drawable. 
		@returns Ângulo de rotação em graus.
	*/
	inline int32 getAngle()					{ return IW_ANGLE_TO_DEGREES(angle); }

	/** Determina ângulo de rotação.
		@param degrees Ângulo de rotação (em graus).
	*/
	inline void setAngle( int32 degrees )	{ angle = IW_ANGLE_FROM_DEGREES(degrees); }

	virtual void draw();

protected:
	Point position;				//!< Posição do objeto na tela ou relativo ao seu grupo, em pixels
	Point size;					//!< Tamanho do objeto, em pixels
	bool visible;				//!< Indica se o objeto esta visível
	bool clipTest;				//!< Se o desenho deve ser testado contra a área de clip

	Point refRatio;				//!< Razão entre as coordenadas do ponto de referência e as das dimensões da imagem
	Point refPosition;			//!< Coordenadas do ponto de referência

	iwangle angle;				//!< Ângulo de rotação no sentido antihorário.

	float scale;				//!< Escala do objeto
	
	// TODO: implementar área de colisão que pode ser configurada manualmente
	//Rectangle collisionArea;

	RectanglePtr viewport;		//!< Retângulo ao qual o desenho desse drawable está restrito. Caso seja nulo, esse drawable não possui viewport.
	Color color;				//!< Cor RGBA utilizada como tom para o drawable.
	Transform transform;		//!< Transformação associada ao desenho do drawable.

	static ClipStack* clipStack;	//!< Singleton para administração da pilha de clipping

	/** Função de desenho. Deve levar em consideração a posição atual do translate do clipStack. */
	virtual void paint() = 0;

	void pushClip();			//!< Adiciona um retângulo ao topo da pilha de clipping
	void popClip();				//!< Remove um retângulo do topo da pilha de clipping
};
#endif