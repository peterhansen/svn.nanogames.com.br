/** \file pageobject.h
	2011-10-15
*/

#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>

#define TextBoxPtr	boost::shared_ptr<TextBox>

typedef enum {
	STATE_NONE,
	STATE_HIDDEN,
	STATE_APPEARING,
	STATE_VISIBLE,
	STATE_HIDING,
} TextBoxState;

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class TextBox : public Label {

public:
	TextBox();

	TextBox( bool autoSizeMode );

	static void unload();

	void pointerButtonCB( const s3ePointerEvent *ev ) {}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	void setSize( int16 width, int16 height );

	void setState( TextBoxState state );

	TextBoxState getState() {
		return state;
	}

	void show();

	void hide();

	void update( uint delta );

	int getTextWidth();

	void autoSize();

	void autoPosition();

	void setAutoSizeMode( bool autoSize );

	void setText( std::string text );

	void setText( std::wstring text );

	float getTextAlpha();


protected:

	TextBoxState state;

	int accTime;

	Point tween;

	bool autoSizeMode;

	static FontPtr getDefaultFont();

private:

	static FontPtr defaultFont;

	void refresh();

	void setTween( int initialValue, int finalValue );

};
#endif