// include principal, que importa funcionalidade da API s3eFile
// e da API de imagens e fontes
#include <s3eFile.h>
#include <IwImage.h>
#include <IwGxFont.h>
#include <IwResManager.h>
#include <IwUI.h>

#include <components\defines.h>
#include <components\gamemanager.h>
#include <src/testscreen.h>
#include <src/pagemanager.h>
#include <src/loadscreen.h>

/**
* Chamada principal da aplicação.
*/
S3E_MAIN_DECL void IwMain()  {
	// inicialização dos módulos necessários
	Iw2DInit();

	IwGxFontInit();
	IwResManagerInit();
	IwUIInit();
	
	{
		CIwUIView view;

		// inicializa Game Manager e carrega alguns recursos
		if ( GameManager::init() ) {
			Iw2DSetUseMipMapping( false );

			Color bgColor = CreateColor( 0, 0, 0, 0xff );
			Iw2DSurfaceClear( bgColor );
			Iw2DSurfaceShow();

			LoadScreenPtr loadScreen = LoadScreenPtr( new LoadScreen() );
			GameManager::setCurrentScreen( loadScreen );

			PageManagerPtr screen = PageManager::getInstance();
			bool loadComplete = false;
			do {
				GameManager::update();
				loadComplete = screen->loadNextPage( loadScreen );
			} while ( !loadComplete );

			#ifdef START_PAGE
				screen->setPageIndex( START_PAGE );
			#else
				screen->setPageIndex( 1 );
			#endif
			GameManager::setCurrentScreen( screen );

			// principal loop da aplicação
			while( GameManager::update() )
				GameManager::draw();
		} else {
			printf( "ERRO em GameManager::init()\n" );
		}
	 
		// executa funcoes necessarias do término da aplicacao
		GameManager::terminate();

		// if IwResourceManager for usado
		IwResManagerTerminate();

		// desaloca variáveis estáticas
		PageManager::unload();
		TextBox::unload();
	}

	IwUITerminate();
	IwGxFontTerminate();
	Iw2DTerminate();
}


