#ifndef DOUBLE_LABEL_H
#define DOUBLE_LABEL_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/sprite.h>
#include <components/label.h>
#include <src/constants.h>
#include <components/gamemanager.h>

#define DoubleLabelPtr	boost::shared_ptr< DoubleLabel >

class DoubleLabel : public DrawableGroup {

public:
	DoubleLabel();
	DoubleLabel( std::wstring topText, std::wstring bottomText );

	void setTopText( std::string text );
	void setTopText( std::wstring text );

	void setBottomText( std::string text );
	void setBottomText( std::wstring text );

protected:

	void build();

	LabelPtr topLabel;

	LabelPtr bottomLabel;

	void autoSize();

};
#endif