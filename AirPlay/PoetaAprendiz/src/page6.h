#ifndef PAGE_6_H
#define PAGE_6_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page6 : public Page {

enum {
	STATE_SHOW_TEXT,
	STATE_BOY_JUMPING_WALL,
};

public:
	Page6();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_BOY_JUMPING_WALL; 
	}

protected:

	CIwArray< SpritePtr > stars;

	DrawableImagePtr bkg;

	DrawableImagePtr wall;

	CIwArray< SpritePtr > boy;

private:

	void setBoySequence( int sequence );
	
	void setBoyFrame( int boyFrame );

	void nextBoyFrame();

	void setBoyPosition( int x, int y );

	int accTime;

	int boyFrame;

	int accMoveTime;

	int boySequence;

	int lastSequenceFrame;

};
#endif