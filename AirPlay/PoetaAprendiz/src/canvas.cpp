#include <src/canvas.h>

#include <iostream>
#include <fstream>
#include <string>


Canvas::Canvas( std::string cloudPath ) : Drawable(), surface( NULL ), redraw( false ), cloudColor( CreateColor( 0xffffffff ) ) {
	brush = ImagePtr( Iw2DCreateImage( ( Constants::Path::IMAGES + "brush.png" ).c_str() ) );
	cloud = ImagePtr( Iw2DCreateImage( cloudPath.c_str() ) );

	loadHitmap( cloudPath );
}


Canvas::Canvas( std::string cloudPath, Point size ) : Drawable(), surface( NULL ), redraw( false ), cloudColor( CreateColor( 0xffffffff ) ) {
	setCanvasSize( size.x, size.y );
	brush = ImagePtr( Iw2DCreateImage( ( Constants::Path::IMAGES + "brush.png" ).c_str() ) );
	cloud = ImagePtr( Iw2DCreateImage( cloudPath.c_str() ) );

	loadHitmap( cloudPath );
}


Canvas::~Canvas() {
	DELETE( surface );
}


void Canvas::loadHitmap( std::string path ) {
	std::string STRING;
	std::ifstream infile;
	//infile.open( path.c_str() );
	infile.open( "images/19/cloud.txt" );

	int y = 0;
    while( !infile.eof() ) {
	    getline( infile, STRING ); // Saves the line in STRING.

		for ( uint i = 0; i < STRING.length(); ++i ) {
			hitMap[ y * CLOUD_WIDTH + i ] = STRING.at( i ) == 'O';
		}
	    //cout<<STRING; // Prints our STRING.

		++y;
    }
	infile.close();

	////Get the image into memory
 //   CIwImage img;
	//img.LoadFromFile( path.c_str() );
	//img.SetBuffers();

 //   //build hitmap - we store an 8bit per pixel alpha map
	//cloudHitMap.SetFormat( CIwImage::A_8 );
	////cloudHitMap.SetFormat( CIwImage::ARGB_8888 );
 //   cloudHitMap.SetWidth( img.GetWidth() );
 //   cloudHitMap.SetHeight( img.GetHeight() );

 //   //Convert the loaded image into an A_8 format image
 //   img.ConvertToImage( &cloudHitMap );

	////uint8* morra = cloudHitMap.GetTexels();
	////for ( int y = 0; y < img.GetHeight(); ++y ) {
	////	for ( int x = 0; x < img.GetWidth(); ++x ) {
	////		int carai = ( int ) morra[ ( y * img.GetWidth() + x ) ];
	////		int fodase = 0;
	////		//printf( "%c", morra[ y * img.GetWidth() + x ] <= 0x7f ? '.' : 'o' );
	////	}
	////	printf("\n");
	////}
}


void Canvas::paint() {
	if ( image.get() != NULL ) {
		if ( redraw ) {
			clear();
			CIwArray< Point > points;
			draw( points );
		}

		Iw2DSetColour( color );
		Iw2DDrawImage( image.get(), clipStack->translate );
	}
}


void Canvas::draw( CIwArray< Point > points ) {
	CIw2DSurface* previousSurface = Iw2DGetSurface();

	Iw2DSetSurface( surface );

	allPoints = points;
	/*.append( points );
	if ( allPoints.size() > 200 )
		allPoints.erase( 0, allPoints.size() - 200 );*/

	CIw2DAlphaMode alpha = Iw2DGetAlphaMode();
	Iw2DSetAlphaMode( IW_2D_ALPHA_HALF );

	//Iw2DSetColour( cloudColor );
	//Iw2DDrawImage( cloud.get(), Point( 0, 0 ), Point( getWidth(), getHeight() ) );
	
	Iw2DSetColour( CreateColor( 0xff000077 ) );
	for ( uint i = 1; i < allPoints.size(); ++i ) {
		Point p1 = allPoints[ i - 1 ];
		Point p2 = allPoints[ i ];
		Point diff = p2 - p1;
		int dx = MAX( ABS( p2.x - p1.x ), 1 );
		int dy = MAX( ABS( p2.y - p1.y ), 1 );

		int x2 = p2.x >= p1.x ? 1 : -1;
		int y2 = p2.y >= p1.y ? 1 : -1;

		if ( ABS( diff.x ) >= ABS( diff.y ) ) {
			for ( int x = p1.x; x != p2.x + x2; x += x2 ) {
				int y = p1.y + ( p2.y - p1.y ) * ABS( x - p1.x ) / dx;
				if ( hitCloud( x, y ) ) {
					//Iw2DFillArc( Point( x, p1.y + ( p2.y - p1.y ) * ABS( x - p1.x ) / dx ), Point( 2, 2 ), 0, 50000 );
					Iw2DDrawImage( brush.get(), Point( x, y ), Point( 4, 4 ) );
				}
			}
		} else {
			for ( int y = p1.y; y != p2.y + y2; y += y2 ) {
				int x = p1.x + ( p2.x - p1.x ) * ABS( y - p1.y ) / dy;
				if ( hitCloud( x, y ) ) {
					//Iw2DFillArc( Point( p1.x + ( p2.x - p1.x ) * ABS( y - p1.y ) / dy, y ), Point( 2, 2 ), 0, 50000 );
					Iw2DDrawImage( brush.get(), Point( x, y ), Point( 4, 4 ) );
				}
			}
		}
	}

	Iw2DSetSurface( previousSurface );

	Iw2DSetAlphaMode( alpha );
}


void Canvas::setCanvasSize( Point size, bool setDrawableSize ) {
	setCanvasSize( size.x, size.y, setDrawableSize );
}


void Canvas::setCanvasSize( int16 width, int16 height, bool setDrawableSize ) {
	if ( setDrawableSize )
		setSize( width, height );

	if ( surface == NULL ) {
		DELETE( surface );
	}

	surface = Iw2DCreateSurface( width, height );
	image = ImagePtr( Iw2DCreateImage( surface ) );
	
	clear();
}


void Canvas::clear( uint32 color ) {
	if ( surface == NULL )
		return;

	// pinta a �rea de desenho de branco inicialmente
	CIw2DAlphaMode alpha = Iw2DGetAlphaMode();
	CIw2DSurface* previousSurface = Iw2DGetSurface();
	Iw2DSetSurface( surface );
	Iw2DSetColour( color );
	Iw2DFillRect( Point( 0, 0 ), Point( image->GetWidth(), image->GetHeight() )  );
	Iw2DSetColour( cloudColor );

	Iw2DSetAlphaMode( IW_2D_ALPHA_HALF );
	Iw2DDrawImage( cloud.get(), Point( 0, 0 ), Point( getWidth(), getHeight() ) );

	Iw2DSetSurface( previousSurface );
	Iw2DSetAlphaMode( alpha );
	Iw2DSetColour( 0xffffffff );
}


CIw2DSurface* Canvas::getSurface() {
	return surface;
}


ImagePtr Canvas::getCloud() {
	return cloud;
}


void Canvas::setRedrawMode( bool r ) {
	redraw = r;
}


void Canvas::setCloudColor( Color color ) {
	cloudColor = color;
}


bool Canvas::hitCloud( int x, int y ) {
	x = ( int ) ( x * ( float ) cloud->GetWidth() / getWidth() );
	y = ( int ) ( y * ( float ) cloud->GetHeight() / getHeight() );

	if ( x < 0 || y < 0 || x > CLOUD_WIDTH || y > CLOUD_HEIGHT )
        return false;

	printf( "%d,%d: %s\n", x, y, hitMap[ y * CLOUD_WIDTH + x ] ? "X" : "." );
	return hitMap[ y * CLOUD_WIDTH + x ];

    //Return a hit if the specified local alpha value is greater than half
	
	/*uint32 t = cloudHitMap.GetTexels()[ y * cloudHitMap.GetWidth() + x ];
	return t >= 0x80;	*/
	//return ( t & 0xff000000 ) >= 0x80000000;	
}
