#include <src/page15.h>

#define PATH ( Constants::Path::IMAGES + "15/" )

#define POSTERS 12
#define POSTER_TYPES 12

#define TOUCH_TOLERANCE ( GameManager::getDeviceScreenSize().x / 100 )
#define GRAVITY			( GameManager::getDeviceScreenSize().y / 16.0f )

#define BOY_HEIGHT_PERCENT 0.36f

#define BOY_TEXT_BOX_SPACING ( ( int ) ( 0.1f * getHeight() ) )

#define POSTER_SEQUENCE_WALL	0
#define POSTER_SEQUENCE_DETACH	1
#define POSTER_SEQUENCE_FALL	3
#define POSTER_SEQUENCE_FLOOR	3

#define TIME_SIN 0.77f


enum {
	STATE_SHOW_TEXT,
};

enum {
	OBJ_BOY,
};


Page15::Page15() : Page() {
	char morraCpp[ 10 ];
	for ( int i = 0; i < POSTERS; ++i ) {
		sprintf( morraCpp, "%d", ( i % POSTER_TYPES ) );
		SpritePtr poster = SpritePtr( new Sprite( PATH + "photo" + morraCpp ) );
		poster->addListener( i, this );
		posters.append( poster );
		postersAccTime.append( PosterAnim() );

		insertDrawable( poster );
	}

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	pageSounds = loadPageFlipSounds();

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page15::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_BOTTOM );
	boy->setAnchorPosition( width >> 1, textBox->getPosY() - BOY_TEXT_BOX_SPACING );

	for ( int i = 0; i < POSTERS; ++i ) {
		scaleDrawable( posters[ i ], scale );
		posters[ i ]->setAnchor( ANCHOR_BOTTOM );
	}

	for ( int i = 0; i < POSTERS; ++i ) {
		SpritePtr p = posters[ i ];
		Point limits = Point( width - p->getWidth(), boy->getPosY() );
		p->setSequenceIndex( POSTER_SEQUENCE_WALL );
		postersAccTime[ i ].reset();

		int tries = 50;
		for ( ; tries >= 0; --tries ) {
			posters[ i ]->setPosition( NanoMath::randomRange( 0, limits.x ), NanoMath::randomRange( 0, limits.y ) );
			if ( checkPostersPositions( p, i ) )
				break;
		}
	}
}


void Page15::update( uint32 delta ) {
	Page::update( delta );

	for ( uint i = 0; i < posters.size(); ++i ) {
		SpritePtr p = posters[ i ];
		if ( p->getSequenceIndex() == POSTER_SEQUENCE_FALL && p->getRefPosY() < boy->getRefPosY() ) {
			PosterAnim a = postersAccTime[ i ];
			a.accTime += delta / 1000.0f;
			postersAccTime[ i ] = a;
			
			p->setAnchorPosition( ( int ) ( a.pos.x + sin( a.accTime * PI / TIME_SIN ) * p->getWidth() * 0.5f ), 
								  MIN( a.pos.y + ( int ) ( a.accTime * GRAVITY + a.accTime * a.accTime * GRAVITY * 0.5f ), boy->getRefPosY() ) );
			
			if ( p->getRefPosY() >= boy->getRefPosY() )
				p->setSequenceIndex( POSTER_SEQUENCE_FLOOR );
		}
	}
}


void Page15::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( L"AMAVA AS ARTISTAS\nDAS CINE-REVISTAS\nAMAVA A MULHER\nA MAIS NÃO PODER" );
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page15::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed ) {
		lastPos = Point( ev->m_x, ev->m_y );
		clickPos = lastPos;

		DrawablePtr d = getObjectAt( lastPos );
		if ( isPoster( d ) ) {
			dragged = d;

			// traz o poster carregado para a frente no desenho
			int boyIndex = getDrawableIndex( boy );
			setDrawableIndex( d, NanoMath::max( boyIndex - 1, 0 ) );
		} else {
			dragged = DrawablePtr();

			//if ( d == boy ) {
			//	GameManager::playSound( pageSounds[ NanoMath::randomRange( 0, pageSounds.size() ) ] );
			//}
		}
	} else {
		if ( dragged.get() ) {
			Point diff = lastPos - clickPos;
			float distance = ( float ) sqrt( diff.x * diff.x + diff.y * diff.y );
			if ( distance <= TOUCH_TOLERANCE ) {
				Sprite* s = ( Sprite* ) dragged.get();
				if ( s->getSequenceIndex() == POSTER_SEQUENCE_WALL ) {
					s->setSequenceIndex( POSTER_SEQUENCE_DETACH );

					int index = getPosterIndex( dragged );
					if ( index >= 0 )
						postersAccTime[ index ].pos = s->getRefPosition();
				}
			}
		}
		dragged = DrawablePtr();
	}
}


void Page15::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	if ( dragged.get() != NULL ) {
		dragged->move( ev->m_x - lastPos.x, ev->m_y - lastPos.y );
		if ( dragged->getPosX() < 0 )
			dragged->move( -dragged->getPosX(), 0 );
		else if ( dragged->getAnchorPosition( ANCHOR_RIGHT ).x > getWidth() )
			dragged->move( getWidth() - dragged->getAnchorPosition( ANCHOR_RIGHT ).x, 0 );

		if ( dragged->getPosY() < 0 ) {
			dragged->setPosition( dragged->getPosX(), 0 );
		} else {
			int diff = dragged->getPosY() - boy->getAnchorPosition( ANCHOR_TOP ).y;
			if ( diff > 0 ) {
				dragged->move( 0, -diff );
				//dragged = DrawablePtr();
			}
		}

		lastPos = Point( ev->m_x, ev->m_y );
	}
}

int Page15::getPosterIndex( DrawablePtr d ) {
	for ( uint i = 0; i < posters.size(); ++i ) {
		if ( posters[ i ] == d )
			return i;
	}

	return -1;
}


bool Page15::isPoster( DrawablePtr d ) {
	return getPosterIndex( d ) >= 0;
}


bool Page15::checkPostersPositions( DrawablePtr p1, int total ) {
	Rectangle r1 = Rectangle( p1->getPosition(), p1->getSize() );
	
	for ( int i = 0; i < total; ++i ) {
		DrawablePtr p2 = posters[ i ];

		if ( p2 == p1 )
			continue;
		
		Rectangle r2 = Rectangle( p2->getPosition(), p2->getSize() );

		Rectangle intersection = r1.calcIntersection( r2 );
		if ( intersection.getSize().x > 0 && intersection.getSize().y > 0 ) {
			return false;
		}
	}

	return true;
}


void Page15::OnFrameChanged( int id, int frameIndex ) {
}


void Page15::OnSequenceEnded( int id, int sequenceIndex ) {
	posters[ id ]->setSequenceIndex( POSTER_SEQUENCE_FALL );
	setDrawableIndex( posters[ id ], 100 );
}
