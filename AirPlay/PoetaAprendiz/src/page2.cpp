#include <src/page2.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "2/" )

#define BOY_HEIGHT_PERCENT 0.25f

#define MAX_CLOUDS 3

#define BKG_COLOR 0x70a9df

#define BKG_WIDTH_PERCENT 0.75f
#define BKG_HEIGHT_PERCENT 0.25f


#define KINETIC_DECAY_PER_SECOND 0.21f

#define MIN_SPEED 0.2f

#define MAX_SPEED 30.0f

#define SPEED_MULTIPLIER 0.18f


Page2::Page2() : Page(), dragging( false ), timeSinceLastPos( 1 ), speedMultiplier( 1.0f ) {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	for ( int i = 0; i < 3; ++i ) {
		addCloud();
	}
	
	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	for ( int i = 0; i < MAX_CLOUDS; ++i ) {
		addCloud();
	}

	createTextBox();
	textBox->setText( L"ANOS TINHA DEZ\nE ASAS NOS PÉS" );
	
	setState( STATE_SHOW_TEXT );
}


void Page2::addCloud() {
	static int numberOfClouds = 0;
	CloudPtr cloud = CloudPtr( new Cloud( numberOfClouds++ ) );
	int halfWidth = GameManager::getDeviceScreenSize().x >> 1;
	cloud->setAnchorPosition( NanoMath::randomRange( -halfWidth, GameManager::getDeviceScreenSize().x + halfWidth ), NanoMath::randomRange( 0, GameManager::getDeviceScreenSize().y ) );

	clouds.append( cloud );
	insertDrawable( cloud );
}


void Page2::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_FLYING );
		break;
	}

	timeSinceLastPos += delta;
	float slow = ( 1.0f - speedMultiplier ) * ( delta / 1000.0f * KINETIC_DECAY_PER_SECOND );
	speedMultiplier += slow;
	for ( uint i = 0; i < clouds.size(); ++i ) {
		clouds[ i ]->setSpeedMultiplier( speedMultiplier );
	}
}


void Page2::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( getAnchorPosition( ANCHOR_CENTER ) );

	setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( size.x >> 1, size.y >> 1 );
}


void Page2::pointerButtonCB( const s3ePointerEvent *ev ) {
	dragging = ( ev->m_Pressed == 1 );
	timeSinceLastPos = 1;
	lastPosX = ev->m_x;
}


void Page2::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	if ( dragging ) {
		float s = ( lastPosX - ev->m_x ) * SPEED_MULTIPLIER / ( float ) ( timeSinceLastPos );
		speedMultiplier = CLAMP( speedMultiplier + s, MIN_SPEED, MAX_SPEED );
		lastPosX = ev->m_x;
	}
}
