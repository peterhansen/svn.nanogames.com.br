#include <src/doublelabel.h>

#define TOP_COLOR 0x3e91d3ff
#define BOTTOM_COLOR 0xffffffff


DoubleLabel::DoubleLabel() : DrawableGroup() {
	build();
}


DoubleLabel::DoubleLabel( std::wstring topText, std::wstring bottomText ) {
	build();

	setTopText( topText );
	setBottomText( bottomText );
}


void DoubleLabel::build() {
	FontPtr font = FontPtr( new Font( "fonts/TheGreatEscape.ttf", 6, 100 ) );
	
	topLabel = LabelPtr( new Label( font, "" ) );
	topLabel->setColor( TOP_COLOR );
	topLabel->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	insertDrawable( topLabel );

	bottomLabel = LabelPtr( new Label( font, "" ) );
	bottomLabel->setColor( BOTTOM_COLOR );
	bottomLabel->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	insertDrawable( bottomLabel );
}


void DoubleLabel::setTopText( std::string text ) {
	topLabel->setText( text );
	autoSize();
}


void DoubleLabel::setTopText( std::wstring text ) {
	topLabel->setText( text );
	autoSize();
}


void DoubleLabel::setBottomText( std::string text ) {
	bottomLabel->setText( text );
	autoSize();
}


void DoubleLabel::setBottomText( std::wstring text ) {
	bottomLabel->setText( text );
	autoSize();
}


void DoubleLabel::autoSize() {
	topLabel->autoSize();
	bottomLabel->autoSize();

	setSize( MAX( topLabel->getWidth(), bottomLabel->getWidth() ), topLabel->getHeight() + bottomLabel->getHeight() );

	topLabel->setPosition( ( getWidth() - topLabel->getWidth() ) >> 1, 0 );
	bottomLabel->setPosition( ( getWidth() - bottomLabel->getWidth() ) >> 1, topLabel->getHeight() );
}
