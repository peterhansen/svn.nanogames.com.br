#include <src/Page13.h>
#include <src/pagemanager.h>
#include <components/gamemanager.h>

#define PATH ( Constants::Path::IMAGES + "13/" )


#define KIDS_HEIGHT_PERCENT 0.45f

#define BKG_COLOR 0xe17798ff

#define BKG_WIDTH_PERCENT 0.65f
#define BKG_HEIGHT_PERCENT 0.25f

#define HEART_X 284
#define HEART_Y 4

#define SEQUENCE_STOPPED 0
#define SEQUENCE_HEART 1


enum {
	OBJ_BKG,
	OBJ_KIDS,
};


Page13::Page13() : Page() {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	kids = SpritePtr( new Sprite( PATH + "kids" ) );
	insertDrawable( kids );

	heart = SpritePtr( new Sprite( PATH + "heart" ) );
	heart->addListener( 0, this );
	heart->setVisible( false );
	insertDrawable( heart );

	kiss = SoundPtr( new Sound( Constants::Path::SOUNDS + "kiss.raw" ) );

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page13::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( getAnchorPosition( ANCHOR_CENTER ) );

	float scale = setHeightPercent( kids, KIDS_HEIGHT_PERCENT );
	kids->setAnchor( ANCHOR_CENTER );
	kids->setAnchorPosition( size.x >> 1, size.y >> 1 );

	scaleDrawable( heart, scale );
	heart->setAnchor( ANCHOR_BOTTOM );
	heart->setAnchorPosition( kids->getPosition() + Point( ( int ) ( HEART_X * scale ), ( int ) ( HEART_Y * scale ) ) );
}


void Page13::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_HEARTS );
		break;
	}
}


void Page13::OnFrameChanged( int id, int frameIndex ) {
}


void Page13::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case SEQUENCE_HEART:
			heart->setSequenceIndex( SEQUENCE_STOPPED );
			heart->setVisible( false );
		break;
	}
}


void Page13::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			heart->setSequenceIndex( SEQUENCE_STOPPED );
			textBox->setText( L"AMAVA SUAS PRIMAS\nCOM BEIJOS E RIMAS" );
		break;
		
		case STATE_HEARTS:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page13::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_KIDS:
			if ( heart->getSequenceIndex() == SEQUENCE_STOPPED ) {
				heart->setSequenceIndex( SEQUENCE_HEART );
				heart->setVisible( true );

				//girl->setSequenceIndex( );
				GameManager::playSound( kiss );
			}
		break;
	}
}


void Page13::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
