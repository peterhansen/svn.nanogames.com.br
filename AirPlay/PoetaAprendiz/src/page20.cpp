#include <src/page20.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "20/" )

#define PHOTO_HEIGHT_PERCENT 0.42f


Page20::Page20() : Page() {
	photo = DrawableImagePtr( new DrawableImage( PATH + "photo.png" ) );
	insertDrawable( photo );

	createTextBox();

	setState( STATE_SHOW_TEXT );
}


void Page20::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	setHeightPercent( photo, PHOTO_HEIGHT_PERCENT );
	photo->setAnchor( ANCHOR_CENTER );
	photo->setAnchorPosition( width >> 1, height >> 1 );
}


void Page20::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_RUNNING );
		break;
		
		default:
		break;
	}
}


void Page20::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( L"PODERIA SER." );
		break;
		
		case STATE_BOY_RUNNING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


