#include <src/page.h>

#include <sstream>

#include <components/gamemanager.h>


Page::Page() : DrawableGroup(), textBoxAutoSize( true ) {
}


Page::Page( bool boxAutoSize ) : DrawableGroup(), textBoxAutoSize( boxAutoSize ) {
}


void Page::createTextBox() {
	textBox = TextBoxPtr( new TextBox( textBoxAutoSize ) );
	insertDrawable( textBox );
}


DrawablePtr Page::getObjectAt( Point p ) {
	for ( int8 i = drawables.size() - 1; i >= 0; --i ) {
		if ( drawables[ i ]->contains( p ) )
			return drawables[ i ];
	}

	return DrawablePtr();
}


int8 Page::getObjectIndexAt( Point p ) {
	for ( int8 i = drawables.size() - 1; i >= 0; --i ) {
		if ( drawables[ i ]->contains( p ) )
			return i;
	}

	return -1;
}


void Page::setState( uint8 state ) { 
	this->state = state; 
	if ( state == 0 ) {
		textBox->setState( STATE_HIDDEN );
		textBox->setState( STATE_APPEARING );
	}
	IwTrace( MYAPP, ( "Page::setState( %d )", state ) );
}


CIwArray< SoundPtr > Page::loadPageFlipSounds() {
	CIwArray< SoundPtr > sounds;

	for ( int i = 0; i < PAGE_FLIPS_N_SOUNDS; ++i ) {
		std::ostringstream morraCarai;
		// TODO verificar se diferen�a de sample rate pode estar causando os ru�dos ao tocar sons
		morraCarai << ( Constants::Path::SOUNDS ) << "pageturn_" << i << ".raw";
		SoundPtr soundPtr = SoundPtr( new Sound( morraCarai.str() ) );
		sounds.append( soundPtr );
	}

	return sounds;
}


void Page::pointerButtonCB( const s3ePointerEvent *ev ) {
	DrawablePtr obj = getObjectAt( Point( ev->m_x, ev->m_y ) );
}


void Page::setSize( int16 width, int16 height ) {
	DrawableGroup::setSize( width, height );
	
	if ( textBox ) {
		if ( textBoxAutoSize ) {
			textBox->autoPosition();
		} else {
			textBox->setSize( size.x, 100 );
			textBox->setPosition( 0, size.y - textBox->getHeight() );
		}
	}
}


bool Page::canAdvance() {
	return true;
}


float Page::setWidthPercent( DrawablePtr d, float percent ) {
	float scale = getWidth() * percent / d->getWidth();
	scaleDrawable( d, scale );

	return scale;
}


float Page::setHeightPercent( DrawablePtr d, float percent ) {
	float scale = getHeight() * percent / d->getHeight();
	scaleDrawable( d, scale );

	return scale;
}


void Page::scaleDrawable( DrawablePtr d, float scale ) {
	//d->setScale( scale );
	d->setSize( ( int ) ( d->getWidth() * scale ), ( int ) ( d->getHeight() * scale ) );
}
