#ifndef PAGE_19_H
#define PAGE_19_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <src/canvas.h>

class Page19 : public Page, public SpriteListener {

enum {
	STATE_SHOW_TEXT,
	STATE_THINKING,
	STATE_DREAM_APPEARS,
	STATE_DRAW,
};

public:
	Page19();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	bool canAdvance() { 
		return state == STATE_DRAW; 
	}

protected:

	PatternPtr bkg;

	SpritePtr boy;

	SpritePtr lamp;

	SpritePtr book;

	DrawableImagePtr table;

	Point lastPos;

	CIwArray< SpritePtr > points;

	bool pressing;

	CanvasPtr canvas;

	int accTime;

	void paint();

private:

	void setDrawableOpacity( DrawablePtr d, float opacity );

	Color getOpacityColor( Color c, float opacity );

};
#endif