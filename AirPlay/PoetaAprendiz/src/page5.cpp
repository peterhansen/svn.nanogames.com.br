#include <src/page5.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "5/" )

#define BOY_SPEED_PERCENT 0.28f

#define BOY_MIN_HEIGHT_PERCENT 0.18f
#define BOY_MAX_HEIGHT_PERCENT 0.6f

#define BOY_MIN_WIDTH ( ( int ) ( boy->getWidth() * BOY_MIN_SCALE ) )
#define BOY_MIN_HEIGHT ( ( int ) ( boy->getHeight() * BOY_MIN_SCALE ) )
#define BOY_MAX_WIDTH ( ( int ) ( boy->getWidth() * BOY_HEIGHT_PERCENT ) )
#define BOY_MAX_HEIGHT ( ( int ) ( boy->getHeight() * BOY_HEIGHT_PERCENT ) )


enum {
	OBJ_BOY,
};


Page5::Page5() : Page() {
	shadow = DrawableImagePtr( new DrawableImage( PATH + "shadow.png" ) );
	shadow->setAnchor( ANCHOR_CENTER );
	shadowOriginalSize = shadow->getSize();
	insertDrawable( shadow );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	boy->setAnchor( ANCHOR_BOTTOM );
	insertDrawable( boy );

	boyOriginalSize = boy->getSize();

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page5::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );
}


void Page5::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( L"SEU CORPO MORENO\nVIVIA CORRENDO" );

			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_RUNNING );
		break;

		case STATE_BOY_RUNNING:
		break;
	}

	float scale = 0;
	if ( totalTime > 0 ) {
		accTime = MIN( accTime + delta, totalTime );
		float progress = accTime / totalTime;
		int y = ( int ) ( origin.y + ( direction.y * progress ) );

		float heightPercent = y / ( float ) ( getHeight() );
		scale = CLAMP( BOY_MIN_HEIGHT_PERCENT + ( BOY_MAX_HEIGHT_PERCENT - BOY_MIN_HEIGHT_PERCENT ) * heightPercent, BOY_MIN_HEIGHT_PERCENT, BOY_MAX_HEIGHT_PERCENT );
		scale = getHeight() * scale / boyOriginalSize.y;

		boy->setSize( ( int ) ( boyOriginalSize.x * scale ), ( int ) ( boyOriginalSize.y * scale ) );
		boy->setAnchor( ANCHOR_BOTTOM );

		int w = boy->getWidth();
		int h = boy->getHeight();

		boy->setAnchorPosition( ( int ) ( origin.x + ( direction.x * progress ) ), y );
				
		if ( boy->getRefPosX() < ( w >> 1 ) )
			boy->setAnchorPosition( w >> 1, boy->getRefPosY() );
		else if ( ( boy->getRefPosX() > getWidth() - ( w >> 1 ) ) )
			boy->setAnchorPosition( getWidth() - ( w >> 1 ), boy->getRefPosY() );

		if ( boy->getRefPosY() < h )
			boy->setAnchorPosition( boy->getRefPosX(), h );
		else if ( ( boy->getPosY() + h > getHeight() ) )
			boy->setAnchorPosition( boy->getRefPosX(), getHeight() - h + boy->getHeight() );

		if ( accTime >= totalTime )
			totalTime = -1;
	}

	//scale = boy->getCurrentFrame().bounds.getSize().x / ( float ) shadowOriginalSize.x;
	scale = boy->getWidth() / ( float ) shadowOriginalSize.x;
	shadow->setSize( ( int ) ( shadowOriginalSize.x * scale ), ( int ) ( shadowOriginalSize.y * scale ) );
	//shadow->setSize( ( int ) ( boy->getWidth() * scale ), ( int ) ( boy->getHeight() * scale ) );
	shadow->setAnchor( ANCHOR_CENTER );
	shadow->setAnchorPosition( boy->getRefPosition() );

}


void Page5::setState( uint8 state ) {
	// atribui somente no final
	Page::setState( state );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			boy->setPosition( NanoMath::randomRange( 0, getWidth() ), NanoMath::randomRange( 0, getHeight() ) );
			s3ePointerEvent e;
			e.m_x = getWidth() >> 1;
			e.m_y = getHeight() >> 1;
			e.m_Pressed = 1;
			pointerButtonCB( &e );
			update( 0 );
		break;
		
		case STATE_BOY_RUNNING:
		break;
	}
}


void Page5::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed ) {
		Point target = Point( NanoMath::clamp( ev->m_x, 0, getWidth() ),
							  NanoMath::clamp( ev->m_y, 0, getHeight() ) );

		direction = target - boy->getRefPosition();
		float distance = ( float ) sqrt( direction.x * direction.x + direction.y * direction.y );

		origin = boy->getRefPosition();
		float boySpeed = MAX( getWidth(), getHeight() ) * BOY_SPEED_PERCENT;
		totalTime = distance * 1000.0f / boySpeed;
		accTime = 0;

		// vira o garoto para o lado certo
		boy->setTransformation( direction.x >= 0 ? IW_2D_IMAGE_TRANSFORM_NONE : IW_2D_IMAGE_TRANSFORM_FLIP_X );
	}
}


void Page5::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	s3ePointerEvent e;
	e.m_x = ev->m_x;
	e.m_y = ev->m_y;
	e.m_Pressed = 1;
	pointerButtonCB( &e );
}
