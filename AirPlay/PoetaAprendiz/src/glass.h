#ifndef GLASS_H
#define GLASS_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/sprite.h>
#include <components/label.h>
#include <src/constants.h>
#include <components/gamemanager.h>

#define GlassPtr	boost::shared_ptr< Glass >

class Glass : public Sprite {

public:
	Glass( int index );

	void update( uint32 delta );

	void setInitialPosition( Point initialPosition );

	void reset();

protected:

	Point speed;

	float angularSpeed;

	Point initialPosition;

	float accTime;

private:

};
#endif