#include <src/Page11.h>
#include <src/pagemanager.h>
#include <components/gamemanager.h>

#define PATH ( Constants::Path::IMAGES + "11/" )


enum {
	OBJ_GIRL,
	OBJ_ARMS,
};

#define GIRL_HEIGHT_PERCENT 0.44f

#define ARMS_X 0
#define ARMS_Y 0

#define ARMS_FRAME_PAGE_FLIP 6

#define SEQUENCE_STOPPED 0
#define SEQUENCE_FLIPPING 1


Page11::Page11() : Page() {
	girl = DrawableImagePtr( new DrawableImage( PATH + "girl.png" ) );
	insertDrawable( girl );

	arms = SpritePtr( new Sprite( PATH + "arms" ) );
	arms->addListener( 0, this );
	insertDrawable( arms );

	pageSounds = loadPageFlipSounds();

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page11::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( girl, GIRL_HEIGHT_PERCENT );
	girl->setAnchor( ANCHOR_CENTER );
	girl->setAnchorPosition( size.x >> 1, size.y >> 1 );

	scaleDrawable( arms, scale );
	arms->setPosition( girl->getPosition() + Point( ( int ) ( ARMS_X * scale ), ( int ) ( ARMS_Y * scale ) ) );
}


void Page11::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_GIRL_READING );
		break;
	}
}


void Page11::OnFrameChanged( int id, int frameIndex ) {
	switch ( frameIndex ) {
		case ARMS_FRAME_PAGE_FLIP:
			GameManager::playSound( pageSounds[ NanoMath::randomRange( 0, pageSounds.size() ) ] );
		break;
	}
}


void Page11::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case SEQUENCE_FLIPPING:
			arms->setSequenceIndex( SEQUENCE_STOPPED );
		break;
	}
}


void Page11::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			arms->setSequenceIndex( SEQUENCE_STOPPED );
			textBox->setText( L"AMAVA LEONORA\nMENINA DE COR" );
		break;
		
		case STATE_GIRL_READING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page11::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_GIRL:
		case OBJ_ARMS:
			if ( arms->getSequenceIndex() == SEQUENCE_STOPPED ) {
				arms->setSequenceIndex( SEQUENCE_FLIPPING );
			}
		break;
	}
}


void Page11::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
