#include <src/page8.h>
#include <src/pagemanager.h>
#include <components/gamemanager.h>

#define PATH ( Constants::Path::IMAGES + "8/" )

#define BOY_HEIGHT_PERCENT 0.5f

#define BKG_COLOR 0x70a9df

#define BKG_WIDTH_PERCENT 0.8f
#define BKG_HEIGHT_PERCENT 0.25f


enum {
	OBJ_BKG,
	OBJ_BOY,
};


Page8::Page8() : Page() {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	sound = SoundPtr( new Sound( Constants::Path::SOUNDS + "sea.raw" ) );

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page8::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( getAnchorPosition( ANCHOR_CENTER ) );

	setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( size.x >> 1, size.y >> 1 );
}


void Page8::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_JUMPING_WALL );
		break;
	}
}


void Page8::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( "" );
		break;
		
		case STATE_BOY_JUMPING_WALL:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page8::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_BOY:
			GameManager::playSound( sound );
		break;
	}
}


void Page8::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
