#include <src/pagemanager.h>

#include <components/gamemanager.h>

#include <cmath>
#include <IwGeomSVec2.h>

#include <src/page1.h>
#include <src/page2.h>
#include <src/page3.h>
#include <src/page4.h>
#include <src/page5.h>
#include <src/page6.h>
#include <src/page7.h>
#include <src/page8.h>
#include <src/page9.h>
#include <src/page10.h>
#include <src/page11.h>
#include <src/page12.h>
#include <src/page13.h>
#include <src/page14.h>
#include <src/page15.h>
#include <src/page16.h>
#include <src/page17.h>
#include <src/page18.h>
#include <src/page19.h>
#include <src/page20.h>

// evita LNK2001 por causa de membro estático (C++ FDP)
PageManagerPtr PageManager::instance;

#define DEFAULT_FLIP_VALUE ( ( int ) ( GameManager::getDeviceScreenSize().x * 0.035f ) )
#define FLIP_SPEED 30

#define PAGE_FLIP_TIME 0.72f

#define TOTAL_PAGES 21

#define FLIP_NONE		0
#define FLIP_FORWARD	1
#define FLIP_BACKWARD	2

#define FONT_COLOR 0x3e91d3ff

#define CONFIRM_SEQUENCE_YES	0
#define CONFIRM_SEQUENCE_NO		1


PageManager::PageManager() : 
	currentPageIndex( 0 ), xTouchValue( DEFAULT_FLIP_VALUE ), yTouchValue( DEFAULT_FLIP_VALUE ), flipState( FLIP_NONE ), moving( false ),
							pressing( false ), oldxF( 0 ), oldyF( 0 ), closeButtonClicked( false ), chosenPageIndex( 0 )
{
	setSize( GameManager::getDeviceScreenSize() );

	A = Point( 10, 0 );
	B = Point( getWidth(), getHeight() );
	C = Point( getWidth(), 0 );
	D = Point( 0, 0 );
	E = Point( 0, 0 );
	F = Point( 0, 0 );

	flipState = FLIP_NONE;

	paper = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "paper.png" ) );
	closeButton = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "close.png" ) );
	closeButton->setAnchor( ANCHOR_BOTTOM_RIGHT );

	closeBoxGroup = DrawableGroupPtr( new DrawableGroup() );

	closeBox = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "box.png" ) );
	closeBoxGroup->insertDrawable( closeBox );
	closeBoxGroup->setSize( closeBox->getSize() );

	FontPtr f = FontPtr( new Font( "fonts/TheGreatEscape.ttf", 8, 100 ) );
	closeLabelTitle = LabelPtr( new Label( f, L"QUER VOLTAR AO MENU?" ) );
	closeLabelTitle->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	closeLabelTitle->autoSize();
	closeLabelTitle->setAnchor( ANCHOR_TOP );
	closeLabelTitle->setAnchorPosition( closeBoxGroup->getWidth() >> 1, ( int ) ( closeBox->getHeight() * 0.3f ) );
	closeBoxGroup->insertDrawable( closeLabelTitle );

	closeButtonYes = SpritePtr( new Sprite( Constants::Path::IMAGES + "confirm" ) );

	float buttonFinalScale = 0.25f;
	float buttonFinalHeight = buttonFinalScale * closeBox->getHeight();
	float buttonScale = buttonFinalHeight / closeButtonYes->getHeight();

	closeButtonYes->setSize( ( int ) ( closeButtonYes->getWidth() * buttonScale ), ( int ) ( closeButtonYes->getHeight() * buttonScale ) );
	closeButtonYes->setSequenceIndex( CONFIRM_SEQUENCE_YES );
	closeButtonYes->setAnchor( ANCHOR_CENTER );
	closeButtonYes->setAnchorPosition( 77, 160 );
	closeBoxGroup->insertDrawable( closeButtonYes );

	closeButtonNo = SpritePtr( new Sprite( Constants::Path::IMAGES + "confirm" ) );
	closeButtonNo->setSequenceIndex( CONFIRM_SEQUENCE_NO );
	closeButtonNo->setSize( closeButtonYes->getSize() );
	closeButtonNo->setAnchor( ANCHOR_CENTER );
	closeButtonNo->setAnchorPosition( 283, 160 );
	closeBoxGroup->insertDrawable( closeButtonNo );

	Point p = closeButtonYes->getAnchorPosition( ANCHOR_RIGHT );
	closeLabelYes = LabelPtr( new Label( f, L"SIM" ) );
	closeLabelYes->setColor( FONT_COLOR );
	closeLabelYes->setAnchor( ANCHOR_LEFT );
	closeLabelYes->setAnchorPosition( p.x + 5, p.y );
	closeBoxGroup->insertDrawable( closeLabelYes );
	
	p = closeButtonNo->getAnchorPosition( ANCHOR_RIGHT );
	closeLabelNo = LabelPtr( new Label( f, L"NÃO" ) );
	closeLabelNo->setColor( FONT_COLOR );
	closeLabelNo->setAnchor( ANCHOR_LEFT );
	closeLabelNo->setAnchorPosition( p.x + 5, p.y );
	closeBoxGroup->insertDrawable( closeLabelNo );

	setSize( GameManager::getDeviceScreenSize() );
}


bool PageManager::loadNextPage( LoadScreenPtr loadScreen ) {
	printf( "Loading page #%d... ", loadedPages.size() );
	if ( loadedPages.size() < TOTAL_PAGES ) {
		PagePtr p;
		#ifdef START_PAGE
			p = loadedPages.size() != START_PAGE ? PagePtr() : loadPage( loadedPages.size() );
		#elif defined LAST_PAGE
			p = loadedPages.size() >= LAST_PAGE ? PagePtr() : loadPage( loadedPages.size() );
		#else
			bool load = true;
			p = load ? loadPage( loadedPages.size() ) : PagePtr();
		#endif
		
		if ( p.get() )
			p->setSize( getWidth(), getHeight() );

		loadedPages.append( p );
	}

	loadScreen->setProgress( loadedPages.size() / ( float ) TOTAL_PAGES );
	loadScreen->redraw();

	printf( "ok!\n" );

	return loadedPages.size() >= TOTAL_PAGES;
}


PageManagerPtr PageManager::getInstance() {
	if ( instance.get() == NULL ) {
		instance = PageManagerPtr( new PageManager() );
	}

	return instance;
}


void PageManager::unload() {
	instance.reset();
}


void PageManager::paint() {
	if ( ( flipState != FLIP_NONE ) || moving ) {
		if ( nextPage ) {
			for ( uint i = 0; i < nextViewports.size(); ++i ) {
				nextPage->setViewPort( nextViewports[ i ] );
				nextPage->draw();
			}
		}

		if ( currentPage ) {
			for ( uint i = 0; i < currentViewports.size(); ++i ) {
				currentPage->setViewPort( currentViewports[ i ] );
				currentPage->draw();
			}
		}

		path = pathOfFlippedPaper();

		// Allocate a material from the IwGx global cache
		CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
			
		// Set this as the active material
		IwGxSetMaterial( pMat );

		static CIwSVec2 points[ 3 ];
		points[ 0 ] = A;
		points[ 1 ] = D;
		points[ 2 ] = F;
		//points[ 3 ] = E;
		IwGxSetVertStreamScreenSpace( points, 3 );

		// Set up vertex colours
		static CIwColour colors[ 4 ] = {
			{ 0xdd, 0xdd, 0xdd },
			{ 0xdd, 0xdd, 0xdd },
			{ 0xff, 0xff, 0xff },
			{ 0xff, 0xff, 0xff },
		};
		IwGxSetColStream( colors, 3 );

		// Draw single triangle
		IwGxDrawPrims( IW_GX_TRI_LIST, NULL, 3 );

		// End drawing
		IwGxFlush();
	} else {
		Screen::paint();
	}

	if ( closeButton->isVisible() ) {
		closeButton->setColor( closeButtonClicked ? 0xffffffff : 0xffffff4f );
		closeButton->setPosition( getWidth() - closeButton->getWidth() - 10, 10 );
		closeButton->draw();

		if ( closeButtonClicked ) {
			closeBoxGroup->setPosition( getWidth() - closeBoxGroup->getWidth() - 5, closeButton->getHeight() + 10 );
			closeBoxGroup->draw();
		}
	}

	Iw2DSetColour( CreateColor( 0xffffff5f ) );
	Iw2DDrawImage( paper->getImage(), Point( 0, 0 ), getSize() );
}


void PageManager::update( uint32 delta ) {
	int width = getWidth();
	int height = getHeight();

	if ( flipState == FLIP_NONE && currentPage.get() ) {
		currentPage->update( delta );
	}

	// TODO TESTE
	//if ( ( flipState != FLIP_NONE ) || moving ) {
	if ( flipState != FLIP_NONE ) {
		flipPage( delta / 1000.0f );
	
		if ( ( flipState == FLIP_FORWARD && xTouchValue < getWidth() ) || ( flipState == FLIP_BACKWARD && xTouchValue >= 0 ) ) {
			pointGenerate( xTouchValue, width, height );

			currentViewports.clear();
			nextViewports.clear();

			int y = MAX( D.y, 0 );
			currentViewports.append( Rectangle( Point( 0, 0 ), Point( A.x, A.y ) ) );
			currentViewports.append( Rectangle( Point( A.x, 0 ), Point( D.x - A.x, y ) ) );

			nextViewports.append( Rectangle( Point( D.x, 0 ), Point( getWidth() - D.x, getHeight() ) ) );
			
			int w = D.x - A.x;
			int h = getHeight() - y;
			int MAX_STEP_HEIGHT = 7;
			int MAX_STEPS = 15;
			int steps = MIN( MAX_STEPS, h / MAX_STEP_HEIGHT );

			for ( int i = 0; i < steps; ++i ) {
				Point p = Point( A.x + ( w * i / steps ) - 2, y );
				Point s = Point( ( w / steps ), ( steps - 1 - i ) * h / steps );

				currentViewports.append( Rectangle( p, s ) );

				Point p2 = Point( p.x + 2, p.y + s.y );
				Point s2 = Point( s.x, getHeight() - p2.y );

				nextViewports.append( Rectangle( p2, s2 ) );
			}
		} else {
			moving = false;
			advanceToChosenPage();
			flipState = FLIP_NONE;
		}
	} else if ( pressing ) {
		pointGenerate2( xTouchValue, yTouchValue, width, height );
	}
}


void PageManager::setPageIndex( uint8 index ) {
	if ( index >= 2 )
		setCloseButtonVisible( true );

	if ( flipState == FLIP_BACKWARD ) {
		removeDrawable( nextPage );

		PagePtr temp = nextPage;
		nextPage = currentPage;
		currentPage = temp;
	} else {
		removeDrawable( nextPage );
	}
	
	if ( index < TOTAL_PAGES ) {
		nextPage = loadedPages[ index ];
		nextPage->setViewPort( Rectangle() );
	} else {
		nextPage.reset();
	}

	if ( currentPage.get() ) {
		removeDrawable( currentPage );
		currentPage.reset();
	}

	currentPage = nextPage;

	if ( index < loadedPages.size() - 1 ) {
		nextPage = loadedPages[ index + 1 ];
		nextPage->setViewPort( Rectangle() );
		insertDrawable( nextPage );
	} else {
		nextPage = loadedPages[ 1 ];
	}

	if ( currentPageIndex == 0 ) {
		nextPage = loadedPages[ index + 1 ];
		nextPage->setViewPort( Rectangle() );
		insertDrawable( nextPage );
	}

	printf( "PageManager changed from page %d to page %d (%d)\n", currentPageIndex, index, drawables.size() );

	currentPageIndex = index;

	if ( currentPage.get() ) {
		insertDrawable( currentPage );
		currentPage->setViewPort( Rectangle( Point( 0, 0 ), getSize() ) );
	}

	setDrawableIndex( nextPage, drawables.size() );

	for ( uint i = 0; i < drawables.size(); ) {
		if ( drawables[ i ] != currentPage && drawables[ i ] != nextPage ) {
			removeDrawableAt( i );
		} else {
			++i;
		}
	}
}


PagePtr PageManager::loadPage( uint8 index ) {
	switch ( index ) {
		case 1: return PagePtr( new Page1() );
		case 2: return PagePtr( new Page2() );
		case 3: return PagePtr( new Page3() );
		case 4: return PagePtr( new Page4() );
		case 5: return PagePtr( new Page5() );
		case 6: return PagePtr( new Page6() );
		case 7: return PagePtr( new Page7() );
		case 8: return PagePtr( new Page8() );
		case 9: return PagePtr( new Page9() );
		case 10: return PagePtr( new Page10() );
		case 11: return PagePtr( new Page11() );
		case 12: return PagePtr( new Page12() );
		case 13: return PagePtr( new Page13() );
		case 14: return PagePtr( new Page14() );
		case 15: return PagePtr( new Page15() );
		case 16: return PagePtr( new Page16() );
		case 17: return PagePtr( new Page17() );
		case 18: return PagePtr( new Page18() );
		case 19: return PagePtr( new Page19() );
		case 20: return PagePtr( new Page20() );

		default:
			printf( "Invalid page number: %d\n", index );
			return PagePtr();
	}
}


void PageManager::flipPage( float delta ) {
	xTouchValue = MIN( xTouchValue + flipSpeedX * ( flipState == FLIP_FORWARD ? delta : -delta ), getWidth() );
}


bool PageManager::checkNextPageButton( const s3ePointerEvent *ev ) {
	return ( ev->m_Pressed && closeButton->isVisible() && 
			 ev->m_y >= getHeight() - DEFAULT_FLIP_VALUE &&
			 ( ev->m_x >= getWidth() - DEFAULT_FLIP_VALUE ) ||
			 ( ev->m_x <= DEFAULT_FLIP_VALUE ) );
}


CIwArray< Point > PageManager::pathOfTheMask() {
	CIwArray< Point > path = CIwArray< Point >();

	path.append( A );
	path.append( B );
	path.append( C );
	path.append( D );
	path.append( A );

	return path;
}	


CIwArray< Point > PageManager::pathOfFlippedPaper() {
	CIwArray< Point > path = CIwArray< Point >();

	path.append( A );
	path.append( D );
	path.append( E );
	path.append( F );
	path.append( A );

	return path;
}


void PageManager::pointGenerate( float distance, int width, int height ) {
	float xA = ( float ) ( width - distance );
	float yA = ( float ) height;

	float xD = 0;
	float yD = 0;
	
	if ( xA > width / 2 ) {
		xD = ( float ) width;
		yD = ( float ) ( height - (width - xA) * height / xA );
	} else {
		xD = 2 * xA;
		yD = 0;
	}

	double a = (height - yD) / (xD + distance - width);
	double alpha = atan( a );
	double _cos = cos( 2 * alpha ), _sin = sin( 2 * alpha );
	// E
	float xE = (float) (xD + _cos * (width - xD));
	float yE = (float) -(_sin * (width - xD));
	// F
	float xF = (float) (width - distance + _cos * distance);
	float yF = (float) (height - _sin * distance);
	
	if ( xA > width / 2 ) {
		xE = xD;
		yE = yD;
	}
		
	A.x = ( int ) xA;
	A.y = ( int ) yA;
	D.x = ( int ) xD;
	D.y = ( int ) yD;
	E.x = ( int ) xE;
	E.y = ( int ) yE;
	F.x = ( int ) xF;
	F.y = ( int ) yF;
}


void PageManager::pointGenerate2( float xTouch, float yTouch, int width, int height ) {
	float yA = ( float ) height;
	float xD = ( float ) width;

	float xF = ( float ) ( width - xTouch + 0.1f );
	float yF = ( float ) ( height - yTouch + 0.1f );
	
	if(A.x==0){
		xF= MIN( xF, oldxF );
		yF= MAX( yF, oldyF );
	}
	float deltaX = width-xF;
	float deltaY = height-yF;
	float BH = (float) ( sqrt( deltaX * deltaX + deltaY * deltaY ) / 2 );
	double tangAlpha = deltaY / deltaX;
	double alpha = atan(tangAlpha);
	double _cos = cos(alpha), _sin = sin(alpha);
	float xA = (float) (width - (BH / _cos));
	float yD = (float) (height - (BH / _sin));

	xA = MAX( 0, xA );
	if( xA == 0 ){
		//yF= Math.max(yF, height-(float) Math.sqrt(width*width-xF*xF));
		oldxF = xF;
		oldyF = yF;
	}
	
	float xE = xD;
	float yE = yD;

	if (yD < 0) {
		xD = width + (float) (tangAlpha * yD);
		yE = 0;
		xE = width + (float) ( tan(2 * alpha) * yD );

	}
		
	A.x = ( int ) xA;
	A.y = ( int ) yA;
	D.x = ( int ) xD;
	D.y = ( int ) MAX( 0, yD );
	E.x = ( int ) xE;
	E.y = ( int ) yE;
	F.x = ( int ) xF;
	F.y = ( int ) yF;
}


void PageManager::touchButtonCB( const s3ePointerTouchEvent *ev ) {
	s3ePointerEvent e;
	e.m_x = ev->m_x;
	e.m_y = ev->m_y;
	e.m_Pressed = ev->m_Pressed;

	pointerButtonCB( &e );
}


void PageManager::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {
	s3ePointerMotionEvent e;
	e.m_x = ev->m_x;
	e.m_y = ev->m_y;

	pointerMotionCB( &e );
}


void PageManager::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( flipState != FLIP_NONE )
		return;

	if ( pressing ) {
	//if ( flipState != FLIP_NONE || pressing ) {
		if ( !ev->m_Pressed ) {
			if( moving ) {
				xTouchValue = ( float ) ( getWidth() - A.x );
				moving = false;
			}
			if ( ev->m_x > ( getWidth() >> 1 ) ) {
				flipState = FLIP_FORWARD;
				flipSpeedX = ( getWidth() - xTouchValue ) / PAGE_FLIP_TIME;

				nextPage->setState( 0 );
				if ( currentPageIndex >= TOTAL_PAGES - 1 ) {
					nextPage->setViewPort( Rectangle() );
					chosenPageIndex = 1;
				}
			} else {
				flipState = FLIP_BACKWARD;
				flipSpeedX = xTouchValue / PAGE_FLIP_TIME;

				nextPage = currentPage;
				removeDrawable( currentPage );

				currentPage = loadedPages[ currentPageIndex - 1 ];
				insertDrawable( currentPage );

				currentPage->setState( 0 );
			}

			pressing = false;
		}

		return;
	}

	if ( ev->m_x >= closeButton->getPosX() && ev->m_y <= closeButton->getRefPosY() ) {
		if ( ev->m_Pressed )
			closeButtonClicked = !closeButtonClicked;
	} else if ( closeButtonClicked ) {
		if ( closeBoxGroup->contains( Point( ev->m_x, ev->m_y ) ) ) {
			Point p = Point( ev->m_x - closeBoxGroup->getPosX(), ev->m_y - closeBoxGroup->getPosY() );
			if ( closeButtonYes->contains( p ) || closeLabelYes->contains( p ) ) {
				if ( currentPageIndex == 1 ) {
					currentPage->setState( 0 );
				} else {
					removeDrawable( nextPage );
					nextPage = loadedPages[ 1 ];
					nextPage->setViewPort( Rectangle() );
					insertDrawable( nextPage );
				
					chosenPageIndex = 1;
					closeButtonClicked = false;
					xTouchValue = 0;
					flipState = FLIP_FORWARD;
					flipSpeedX = getWidth() / PAGE_FLIP_TIME;
					nextPage->setState( 0 );
				}
			} else if ( closeButtonNo->contains( p ) || closeLabelNo->contains( p ) ) {
				closeButtonClicked = false;
			} 
		} else {
			closeButtonClicked = false;
		}
	} else if ( checkNextPageButton( ev ) ) {
		oldTouchX = ( float ) ev->m_x;
		oldTouchY = ( float ) ev->m_y;
		pressing = true;

		if ( oldTouchX > ( getWidth() >> 1 ) ) {
			chosenPageIndex = currentPageIndex + 1;
			xTouchValue = ( float ) DEFAULT_FLIP_VALUE;
			yTouchValue = ( float ) DEFAULT_FLIP_VALUE;
		} else {
			chosenPageIndex = currentPageIndex - 1;
			xTouchValue = ( float ) getWidth();
			yTouchValue = ( float ) DEFAULT_FLIP_VALUE;
		}
	} else {
		if ( currentPage ) 
			currentPage->pointerButtonCB( ev ); 
	}
}


void PageManager::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	if ( pressing ) {
		//int xMouse = ev->m_x;
		//int yMouse = ev->m_y;

		//moving = true;
		//xTouchValue -= ( float ) ( xMouse - oldTouchX );
		//yTouchValue -= ( float ) ( yMouse - oldTouchY );

		//if ( xMouse < oldTouchX ) {
		//	//if (!next) {
		//	//	flip = false;
		//	//}
		//	//next = true;
		//} else {
		//	//if (next) {
		//	//	flip = false;
		//	//}
		//	//next = false;
		//}

		//oldTouchX = ( float ) xMouse;
		//oldTouchY = ( float ) yMouse;
	} else {
		if ( currentPage ) 
			currentPage->pointerMotionCB( ev );
	}
}


void PageManager::setCloseButtonVisible( bool visible ) {
	getInstance()->closeButton->setVisible( visible );
}


void PageManager::advanceToChosenPage() {
	setPageIndex( chosenPageIndex );
}
