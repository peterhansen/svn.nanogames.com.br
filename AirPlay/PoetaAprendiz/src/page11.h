#ifndef PAGE_11_H
#define PAGE_11_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page11 : public Page, public SpriteListener {

enum {
	STATE_SHOW_TEXT,
	STATE_GIRL_READING,
};

public:
	Page11();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_GIRL_READING; 
	}

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

protected:

	DrawableImagePtr girl;

	SpritePtr arms;

	CIwArray< SoundPtr > pageSounds;

private:

};
#endif