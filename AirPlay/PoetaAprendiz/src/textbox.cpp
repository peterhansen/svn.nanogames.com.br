#include <src/textbox.h>

#include <components/pattern.h>
#include <components/gamemanager.h>

#include <IwGxFontPreparedData.h>

#define MAX_LINES 4.5f


/** Dura��o da transi��o (estados STATE_APPEARING e STATE_HIDING). */
#define TRANSITION_TIME 600


FontPtr TextBox::defaultFont = FontPtr();


TextBox::TextBox() : Label( getDefaultFont(), "" ),
	accTime( 0 ), state( STATE_NONE ), autoSizeMode( false )
{
	setFormat( IW_GX_FONT_ALIGN_LEFT, IW_GX_FONT_ALIGN_TOP );
	setState( STATE_APPEARING );
}


TextBox::TextBox( bool autoSize ) : Label( getDefaultFont(), "" ),
	accTime( 0 ), state( STATE_NONE ), autoSizeMode( autoSize )
{
	setFormat( IW_GX_FONT_ALIGN_LEFT, IW_GX_FONT_ALIGN_TOP );
	setState( STATE_APPEARING );
}


void TextBox::unload() {
	defaultFont.reset();
}


FontPtr TextBox::getDefaultFont() {
	if ( !defaultFont.get() ) {
		defaultFont = FontPtr( new Font( "fonts/TheGreatEscape.ttf", 8, 100 ) );
	}

	return defaultFont;
}


void TextBox::setSize( int16 width, int16 height ) {
	Label::setSize( width, height );
}


void TextBox::setText( std::string text ) {
	setText( W2S( text ) );
}

void TextBox::setText( std::wstring text ) {
	Label::setText( text );

	if ( autoSizeMode )
		autoPosition();
}


void TextBox::setAutoSizeMode( bool autoSize ) {
	this->autoSizeMode = autoSize;
}


void TextBox::update( uint delta ) {
	switch ( state ) {
		case STATE_APPEARING:
			accTime += delta;
			refresh();

			if ( accTime >= TRANSITION_TIME ) {
				setState( STATE_VISIBLE );
			}
		break;

		case STATE_HIDING:
			accTime -= delta;
			refresh();

			if ( accTime <= 0 ) {
				setState( STATE_HIDDEN );
			}
		break;

		case STATE_VISIBLE:
			Label::update( delta );
		break;
	}
}


int TextBox::getTextWidth() {
	IwGxFontSetFont( font.get() );

	//container to receive the formatted text
	CIwGxFontPreparedData data;
 
	//Format the string using the current settings
	IwGxFontPrepareText( data, ( uint16* ) text.c_str() );

	return data.GetWidth();
}


void TextBox::autoSize() {
	IwGxFontSetFont( font.get() );

	//container to receive the formatted text
	CIwGxFontPreparedData data;
 
	//Format the string using the current settings
	IwGxFontPrepareText( data, ( uint16* ) text.c_str() );

	setSize( data.GetWidth(), data.GetHeight() );
}


void TextBox::autoPosition() {
	autoSize();

	setAnchor( ANCHOR_TOP );
	int maxHeight = ( int ) ( font->GetHeight() * MAX_LINES );
	int defaultY = GameManager::getDeviceScreenSize().y - maxHeight;
	setAnchorPosition( GameManager::getDeviceScreenSize().x >> 1, defaultY + ( ( maxHeight - getHeight() ) >> 1 ) );
}


void TextBox::show() {
	switch ( state ) {
		case STATE_APPEARING:
		case STATE_VISIBLE:
		return;

		default:
			setState( STATE_APPEARING );
	}
}


void TextBox::hide() {
	switch ( state ) {
		case STATE_HIDDEN:
		case STATE_HIDING:
		return;

		default:
			setState( STATE_HIDING );
	}
}


void TextBox::setState( TextBoxState state ) {
	switch ( state ) {
		case STATE_HIDDEN:
			accTime = TRANSITION_TIME;
			setTween( GameManager::getDeviceScreenSize().y, GameManager::getDeviceScreenSize().y );
		break;

		case STATE_APPEARING:
			switch ( this->state ) {
				case STATE_VISIBLE:
				case STATE_APPEARING:
				return;
			}

			setTween( getPosY(), GameManager::getDeviceScreenSize().y - getHeight() );
				
			accTime = 0;
		break;

		case STATE_HIDING:
			switch ( this->state ) {
				case STATE_HIDDEN:
				case STATE_HIDING:
				return;
			}

			accTime = TRANSITION_TIME;
			setTween( getPosY(), GameManager::getDeviceScreenSize().y );
		break;
	}

	setVisible( state != STATE_HIDDEN );
	this->state = state;
	refresh();
}


void TextBox::setTween( int initialValue, int finalValue ) {
	tween.x = initialValue;
	tween.y = finalValue;
}


void TextBox::refresh() {
	accTime = NanoMath::clamp( accTime, 0, TRANSITION_TIME );
	setColor( ( int ) ( accTime * 255.0f / TRANSITION_TIME ) );
}


float TextBox::getTextAlpha() {
	return accTime / ( float ) TRANSITION_TIME;
}
