#ifndef PAGE_2_H
#define PAGE_2_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <src/cloud.h>

class Page2 : public Page {

enum {
	STATE_SHOW_TEXT,
	STATE_BOY_FLYING,
};


public:
	Page2();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );
	
	bool canAdvance() { 
		return state == STATE_BOY_FLYING; 
	}


protected:

	SpritePtr boy;

	PatternPtr bkg;

	bool dragging;

	int lastPosX;

	int timeSinceLastPos;

	float speedMultiplier;

	CIwArray< CloudPtr > clouds;


private:

	void addCloud();

};
#endif