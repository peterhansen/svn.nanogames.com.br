#ifndef PAGE_15_H
#define PAGE_15_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <components/spritelistener.h>

class Page15 : public Page, public SpriteListener {

public:
	Page15();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	void update( uint32 delta );

protected:

	SpritePtr boy;

	DrawablePtr dragged;

	Point lastPos;

	Point clickPos;

	CIwArray< SoundPtr > pageSounds;

private:

	struct PosterAnim {
		Point pos;
		float accTime;

		void reset() {
			pos = Point();
			accTime = 0;
		}
	};

	CIwArray< SpritePtr > posters;

	CIwArray< PosterAnim > postersAccTime;

	int getPosterIndex( DrawablePtr d );

	bool isPoster( DrawablePtr d );

	bool checkPostersPositions( DrawablePtr p, int total );

};
#endif