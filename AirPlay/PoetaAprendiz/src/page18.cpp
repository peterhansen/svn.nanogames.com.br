#include <src/page18.h>

#define PATH ( Constants::Path::IMAGES + "18/" )


#define BKG_WIDTH_PERCENT 0.9f
#define BKG_HEIGHT_PERCENT 0.36f
#define BKG_Y 118

#define BKG_COLOR 0x666666ff

#define BOY_HEIGHT_PERCENT 0.25f
#define BOY_START_X ( getWidth() >> 1 )
#define BOY_START_Y ( -boy->getHeight() )

#define SEQUENCE_STOPPED 0
#define SEQUENCE_PILLOW 1
#define SEQUENCE_FOOT 2

enum {
	OBJ_BKG,
	OBJ_BOY,
};


Page18::Page18() {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	boy->addListener( 0, this );
	insertDrawable( boy );
	
	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page18::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( getWidth() >> 1, getHeight() >> 1 );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_BOTTOM );
	bkg->setAnchorPosition( boy->getAnchorPosition( ANCHOR_TOP ) + Point( 0, ( int ) ( BKG_Y * scale ) ) );
}


void Page18::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			boy->setSequenceIndex( SEQUENCE_STOPPED );
			textBox->setText( L"POR ISSO SOFRIA\nDE MELANCOLIA" );
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page18::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY);
		break;
	}
}


void Page18::OnFrameChanged( int id, int frameIndex ) {
}


void Page18::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case SEQUENCE_FOOT:
		case SEQUENCE_PILLOW:
			boy->setSequenceIndex( SEQUENCE_STOPPED );
		break;
	}
}


void Page18::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 1 && state == STATE_BOY ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_BOY:
				if ( boy->getSequenceIndex() == SEQUENCE_STOPPED ) {
					if ( ev->m_x > boy->getRefPosX() )
						boy->setSequenceIndex( SEQUENCE_FOOT );
					else
						boy->setSequenceIndex( SEQUENCE_PILLOW );
				}
			break;
		}
	}
}


void Page18::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
