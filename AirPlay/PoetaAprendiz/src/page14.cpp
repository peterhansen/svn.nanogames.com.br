#include <src/Page14.h>

#include <src/pagemanager.h>
#include <components/gamemanager.h>

#define PATH ( Constants::Path::IMAGES + "14/" )

#define KIDS_HEIGHT_PERCENT 0.43f

#define BKG_COLOR 0xa3301cff

#define BKG_WIDTH_PERCENT 0.75f
#define BKG_HEIGHT_PERCENT 0.3f

#define KIDS_BKG_Y_PERCENT 0.3f

#define GIRL_X 364
#define GIRL_Y 200

#define SEQUENCE_STOPPED 0
#define SEQUENCE_LAUGHING 1


enum {
	OBJ_BKG,
	OBJ_KIDS,
	OBJ_GIRL,
};


Page14::Page14() : Page() {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	kids = SpritePtr( new Sprite( PATH + "kids" ) );
	insertDrawable( kids );

	girl = SpritePtr( new Sprite( PATH + "girl" ) );
	girl->addListener( 0, this );
	insertDrawable( girl );

	laughter = SoundPtr( new Sound( Constants::Path::SOUNDS + "laughter.raw" ) );

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page14::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( width >> 1, height / 3 );

	float scale = setHeightPercent( kids, KIDS_HEIGHT_PERCENT );
	kids->setAnchor( ANCHOR_TOP );
	kids->setAnchorPosition( size.x >> 1, ( int ) ( bkg->getPosY() + ( bkg->getHeight() * KIDS_BKG_Y_PERCENT ) ) );

	scaleDrawable( girl, scale );
	girl->setAnchor( ANCHOR_BOTTOM_LEFT );
	girl->setAnchorPosition( kids->getPosition() + Point( ( int ) ( GIRL_X * scale ), ( int ) ( GIRL_Y * scale ) ) );
}


void Page14::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_ANIMATING );
		break;
	}
}


void Page14::OnFrameChanged( int id, int frameIndex ) {
}


void Page14::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case SEQUENCE_LAUGHING:
			girl->setSequenceIndex( SEQUENCE_STOPPED );
		break;
	}
}


void Page14::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			girl->setSequenceIndex( SEQUENCE_STOPPED );
			textBox->setText( L"AMAVA SUAS TIAS\nDE PELES MACIAS" );
		break;
		
		case STATE_ANIMATING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page14::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_GIRL:
		case OBJ_KIDS:
			if ( girl->getSequenceIndex() == SEQUENCE_STOPPED ) {
				girl->setSequenceIndex( SEQUENCE_LAUGHING );
				GameManager::playSound( laughter );
			}
		break;
	}
}


void Page14::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
