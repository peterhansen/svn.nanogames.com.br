#include <src/page9.h>
#include <src/pagemanager.h>
#include <components/gamemanager.h>

#define PATH ( Constants::Path::IMAGES + "9/" )

#define BOY_HEIGHT_PERCENT 0.45f

#define BKG_COLOR 0x70a9df

#define BKG_WIDTH_PERCENT 0.6f
#define BKG_HEIGHT_PERCENT 0.15f


enum {
	OBJ_BKG,
	OBJ_BOY,
};


Page9::Page9() : Page() {
	shadow = DrawableImagePtr( new DrawableImage( PATH + "shadow.png" ) );
	insertDrawable( shadow );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page9::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( size.x >> 1, size.y >> 1 );

	scaleDrawable( shadow, scale );
	shadow->setAnchor( ANCHOR_CENTER );
	shadow->setAnchorPosition( boy->getAnchorPosition( ANCHOR_BOTTOM ) );
}


void Page9::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_PLAYING );
		break;
	}
}


void Page9::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( L"EM BOLA DE MEIA\nJOGANDO DE MEIA OU DE PONTA\nPASSAVA DA CONTA\nDE TANTO DRIBLAR" );
		break;
		
		case STATE_BOY_PLAYING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page9::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_BOY:
		break;
	}
}


void Page9::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
