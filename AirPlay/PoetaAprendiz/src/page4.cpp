#include <src/page4.h>
#include <src/pagemanager.h>
#include <components/tween.h>
#include <stdlib.h>
#include <sstream>

#define PATH ( Constants::Path::IMAGES + "4/" )

#define OBJECTS 3

#define BOY_HEIGHT_PERCENT 0.5f
#define EYES_HEIGHT_PERCENT 0.25f

#define TIME_TEXT 1200

#define TIME_DARKENING 700

#define OBJECTS_X 0.2f, 0.5f, 0.8f
#define OBJECTS_Y 0.25f, 0.75f, 0.5f

#define OBJECTS_HEIGHT 0.14f, 0.14f, 0.28f

enum {
	STATE_SHOW_TEXT,
	STATE_DARKENING,
	STATE_EYES_OPENING,
	STATE_MOVE_EYES,
};

enum {
	OBJ_BKG,
	OBJ_BOY,
	OBJ_0,
	OBJ_LABEL_0,
	OBJ_1,
	OBJ_LABEL_1,
	OBJ_2,
	OBJ_LABEL_2,
	OBJ_UP,
	OBJ_DOWN,
	OBJ_LEFT,
	OBJ_RIGHT,
	OBJ_EYES,
};


Page4::Page4() : Page(), draggedIndex( -1 ) {
	DrawableImagePtr bkgImage = DrawableImagePtr( new DrawableImage( PATH + "pattern.png" ) );
	std::stringstream s;
	s << PATH << "pattern.png";
	bkg = PatternPtr( new Pattern( ImagePtr( Iw2DCreateImage( s.str().data() ) ) ) );
	insertDrawable( bkg );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	char morraCpp[ 100 ];
	for ( int i = 0; i < OBJECTS; ++i ) {
		sprintf( morraCpp, "%d", i );
		DrawableImagePtr img = DrawableImagePtr( new DrawableImage( PATH + morraCpp + ".png" ) );
		insertDrawable( img );

		TextBoxPtr label = TextBoxPtr( new TextBox( true ) );
		label->setColor( 0xaaaaaaff );
		label->setAnchor( ANCHOR_TOP );
		insertDrawable( label );
		
		objects.append( img );
		labels.append( label );
	}

	up = PatternPtr( new Pattern( CreateColor( 0 ) ) );
	insertDrawable( up );

	down = PatternPtr( new Pattern( CreateColor( 0 ) ) );
	insertDrawable( down );

	left = PatternPtr( new Pattern( CreateColor( 0 ) ) );
	insertDrawable( left );

	right = PatternPtr( new Pattern( CreateColor( 0 ) ) );
	insertDrawable( right );

	eyes = DrawableImagePtr( new DrawableImage( PATH + "eyes.png" ) );
	insertDrawable( eyes );

	createTextBox();

	setState( STATE_SHOW_TEXT );
}


void Page4::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( getSize() );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( width >> 1, height >> 1 );

	float objectsXPercent[] = { OBJECTS_X };
	float objectsYPercent[] = { OBJECTS_Y };
	float objectsHeight[] = { OBJECTS_HEIGHT };

	for ( uint i = 0; i < objects.size(); ++i ) {
		setHeightPercent( objects[ i ], objectsHeight[ i ] );
		objects[ i ]->setAnchor( ANCHOR_CENTER );
		objects[ i ]->setAnchorPosition( ( int ) ( getWidth() * objectsXPercent[ i ] ), ( int ) ( getHeight() * objectsYPercent[ i ] ) );
	}
}


void Page4::pointerButtonCB( const s3ePointerEvent *ev ) {
	switch ( state ) {
		case STATE_MOVE_EYES:
			if ( ev->m_Pressed == 1 ) {
				// pressionou a tela
				int index = getObjectIndexAt( ev->m_x, ev->m_y );
				switch ( index ) {
					case OBJ_EYES:
						lastPos.x = ev->m_x;
						lastPos.y= ev->m_y;

						draggedIndex = index;
					break;

					default:
						draggedIndex = -1;
				}
			} else {
				// soltou o toque
				if ( draggedIndex >= 0 ) {
					int releasedIndex = getObjectIndexAt( ev->m_x, ev->m_y );
					draggedIndex = -1;
				}
			}
		break;
	}
}


void Page4::setState( uint8 state ) {
	std::wstring texts[ OBJECTS ] = { L"PARA TANGERINA", L"PIÃO OU", L"MENINA" };

	switch ( state ) {
		case STATE_SHOW_TEXT:
			bkg->setVisible( false );
			boy->setVisible( true );

			textBox->setVisible( true );
			textBox->setText( L"O OLHAR VERDE-GAIO\nPARECIA UM RAIO" );
			setEyesVisibility( 0 );
			for ( uint i = 0; i < objects.size(); ++i ) {
				objects[ i ]->setVisible( false );
				labels[ i ]->setVisible( false );
			}
		break;

		case STATE_DARKENING:
			eyes->setSize( 1, 1 );
			eyes->setPosition( 0, 0 );
			refreshEyes();
		break;

		case STATE_EYES_OPENING: {
			Point eyesHalfSize = Point( eyes->getWidth() >> 1, eyes->getHeight() >> 1 );
			setEyesPosition( NanoMath::randomRange( eyesHalfSize.x, getWidth() - eyesHalfSize.x ), 
							 NanoMath::randomRange( eyesHalfSize.y, getHeight() - eyesHalfSize.y ) );

			bkg->setVisible( true );
			textBox->setVisible( false );
			boy->setVisible( false );

			for ( uint i = 0; i < objects.size(); ++i ) {
				objects[ i ]->setVisible( true );

				labels[ i ]->setText( texts[ i ] );
				labels[ i ]->setAnchorPosition( objects[ i ]->getAnchorPosition( ANCHOR_BOTTOM ) );
				labels[ i ]->setVisible( true );
			}
		}
		break;
		
		case STATE_MOVE_EYES:
			
		break;
	}

	accTime = 0;
	Page::setState( state );
}


void Page4::setEyesVisibility( float level ) {
	int component = ( int ) ( level * 0xff );
	Color color = CreateColor( 0xffffff00 | component );
	eyes->setColor( color );
	
	color = CreateColor( component );
	up->setColor( color );
	down->setColor( color );
	left->setColor( color );
	right->setColor( color );
}


void Page4::update( uint dt ) {
	Page::update( dt );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE ) {
				accTime += dt;
				if ( accTime >= TIME_TEXT )
					setState( STATE_DARKENING );
			}
		break;

		case STATE_DARKENING:
			accTime = CLAMP( accTime + dt, 0, TIME_DARKENING );
			setEyesVisibility( accTime / TIME_DARKENING );

			if ( accTime >= TIME_DARKENING ) {
				setState( STATE_EYES_OPENING );
			}
		break;

		case STATE_EYES_OPENING:
			{
			accTime = CLAMP( accTime + dt, 0, TIME_DARKENING );
			float percent = accTime / TIME_DARKENING;
			Point eyesCenter = eyes->getRefPosition();
			eyes->setSize( eyes->getImage()->GetWidth(), eyes->getImage()->GetHeight() );
			setHeightPercent( eyes, percent * EYES_HEIGHT_PERCENT );
			eyes->setAnchor( ANCHOR_CENTER );
			eyes->setAnchorPosition( eyesCenter );
			refreshEyes();

			if ( accTime >= TIME_DARKENING )
				setState( STATE_MOVE_EYES );
			}
		break;

		case STATE_MOVE_EYES:
		break;
	}
}


void Page4::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	switch ( draggedIndex ) {
		case OBJ_EYES:
			moveEyes( ev->m_x - lastPos.x, ev->m_y - lastPos.y );
		break;
	}

	lastPos.x = ev->m_x;
	lastPos.y = ev->m_y;
}


void Page4::moveEyes( int16 dx, int16 dy ) {
	setEyesPosition( eyes->getPosX() + dx, eyes->getPosY() + dy );
}


void Page4::setEyesPosition( int16 x, int16 y ) {
	x = NanoMath::clamp( x, 0, getWidth() - eyes->getWidth() );
	y = NanoMath::clamp( y, 0, getHeight() - eyes->getHeight() );

	eyes->setPosition( x, y );
	refreshEyes();
}


void Page4::refreshEyes() {
	up->setSize( getWidth(), eyes->getPosY() + 1 );
	
	left->setPosition( 0, eyes->getPosY() );
	left->setSize( eyes->getPosX() + 1, getHeight() - eyes->getPosY() );

	right->setPosition( eyes->getPosX() + eyes->getWidth() - 1, left->getPosY() );
	right->setSize( getWidth() - right->getPosX(), left->getHeight() );

	down->setPosition( eyes->getPosX(), eyes->getPosY() + eyes->getHeight() - 1 );
	down->setSize( eyes->getWidth(), getHeight() - down->getPosY() );

}
