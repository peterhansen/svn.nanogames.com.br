#ifndef PAGE_16_H
#define PAGE_16_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page16 : public Page {

public:
	Page16();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

protected:

	PatternPtr bkg;

	PatternPtr desk;

	SpritePtr boy;

	DrawableImagePtr paper;

	DrawablePtr dragged;

	Point lastPos;

	CIwArray< SoundPtr > pageSounds;

private:

};
#endif