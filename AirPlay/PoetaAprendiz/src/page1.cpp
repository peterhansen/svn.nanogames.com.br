#include <src/page1.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "1/" )

#define BOY_HEIGHT_PERCENT 0.4f
#define CAPE_HEIGHT_PERCENT 0.17f
#define FLOOR_HEIGHT_PERCENT 0.05f

#define BOY_NECK_X 100
#define BOY_NECK_Y 27

#define CAPE_NECK_X 323
#define CAPE_NECK_Y 155

#define BUTTON_SEQUENCE_CREDITS_OFF 2
#define BUTTON_SEQUENCE_CREDITS_ON 3
#define BUTTON_SEQUENCE_PLAY_OFF 0
#define BUTTON_SEQUENCE_PLAY_ON 1
#define BUTTON_SEQUENCE_BACK_OFF 4
#define BUTTON_SEQUENCE_BACK_ON 5

#define BUTTONS_HEIGHT_PERCENT 0.12f
#define BUTTONS_OFFSET_X_PERCENT 0.08f
#define BUTTONS_OFFSET_Y_PERCENT 0.12f

#define LABELS_LEFT_X_PERCENT 0.2f
#define LABELS_RIGHT_X_PERCENT ( 1.0f - LABELS_LEFT_X_PERCENT )
#define SONG_LABEL_Y_PERCENT 0.226f
#define DRAWN_LABEL_Y_PERCENT 0.394f
#define DEVELOPED_LABEL_Y_PERCENT ( 1.0f - DRAWN_LABEL_Y_PERCENT )
#define ANIMATED_LABEL_Y_PERCENT ( 1.0f - SONG_LABEL_Y_PERCENT )

#define FONT_SMALL FontPtr( new Font( "fonts/TheGreatEscape.ttf", 8, 100 ) )
#define FONT_MEDIUM FontPtr( new Font( "fonts/TheGreatEscape.ttf", 12, 100 ) )
#define FONT_BIG FontPtr( new Font( "fonts/TheGreatEscape.ttf", 18, 100 ) )


enum {
	OBJ_BKG,
	OBJ_SHADOW,
	OBJ_BODY,
	OBJ_E,
	OBJ_CAPE,
	OBJ_CREDITS_BUTTON,
	OBJ_BACK_BUTTON,
	OBJ_PLAY_BUTTON,
	OBJ_TITLE,
	OBJ_CREDITS_LABEL,
	OBJ_BACK_LABEL,
	OBJ_PLAY_LABEL,
};


Page1::Page1() : Page( false ) {
	blueBkg = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "bkg.png" ) );
	insertDrawable( blueBkg );

	shadow = DrawableImagePtr( new DrawableImage( PATH + "shadow.png" ) );
	insertDrawable( shadow );

	body = SpritePtr( new Sprite( PATH + "body" ) );
	insertDrawable( body );

	e = DrawableImagePtr( new DrawableImage( PATH + "e.png" ) );
	insertDrawable( e );

	cape = SpritePtr( new Sprite( PATH + "cape" ) );
	insertDrawable( cape );

	FontPtr font = FONT_SMALL;

	creditsButton = SpritePtr( new Sprite( Constants::Path::IMAGES + "buttons" ) );
	creditsButton->setSequenceIndex( BUTTON_SEQUENCE_CREDITS_OFF );
	creditsButton->addListener( 0, this );
	insertDrawable( creditsButton );

	backButton = SpritePtr( new Sprite( Constants::Path::IMAGES + "buttons" ) );
	backButton->setSequenceIndex( BUTTON_SEQUENCE_BACK_OFF );
	backButton->addListener( 0, this );
	insertDrawable( backButton );

	playButton = SpritePtr( new Sprite( Constants::Path::IMAGES + "buttons" ) );
	playButton->setSequenceIndex( BUTTON_SEQUENCE_PLAY_OFF );
	playButton->addListener( 0, this );
	insertDrawable( playButton );

	creditsLabel = LabelPtr( new Label( font, L"CRÉDITOS" ) );
	creditsLabel->setColor( 0xffffffff );
	creditsLabel->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	insertDrawable( creditsLabel );

	backLabel = LabelPtr( new Label( font, "VOLTAR" ) );
	backLabel->setColor( 0xffffffff );
	backLabel->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	insertDrawable( backLabel );

	playLabel = LabelPtr( new Label( font, L"COMEÇAR" ) );
	playLabel->setColor( 0xffffffff );
	playLabel->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	insertDrawable( playLabel );

	font = FONT_BIG;
	title = LabelPtr( new Label( font, "O POETA APRENDIZ" ) );
	title->setColor( 0xffffffff );
	title->setFormat( IW_GX_FONT_ALIGN_CENTRE );
	insertDrawable( title );

	songByLabel = DoubleLabelPtr( new DoubleLabel( L"UMA CANÇÃO DE", L"VINÍCIUS DE MORAES E TOQUINHO" ) );
	songByLabel->setAnchor( ANCHOR_TOP );

	insertDrawable( songByLabel );

	drawnByLabel = DoubleLabelPtr( new DoubleLabel( L"CANTADA E ILUSTRADA POR", L"ADRIANA CALCANHOTTO" ) );
	drawnByLabel->setAnchor( ANCHOR_TOP );
	insertDrawable( drawnByLabel );

	developedByLabel = DoubleLabelPtr( new DoubleLabel( L"DESENVOLVIDO POR", L"PETER HANSEN" ) );
	developedByLabel->setAnchor( ANCHOR_TOP );
	insertDrawable( developedByLabel );

	animatedByLabel = DoubleLabelPtr( new DoubleLabel( L"ANIMADA POR", L"CAMILA QUEIROZ" ) );
	animatedByLabel->setAnchor( ANCHOR_TOP );
	insertDrawable( animatedByLabel );

	createTextBox();

	textBox2 = TextBoxPtr( new TextBox() );
	textBox2->setAnchor( ANCHOR_LEFT );
	insertDrawable( textBox2 );

	textBox->setText( L"LE ERA UM MENINO" );
	textBox2->setText( L"VALENTE E CAPRINO\nUM PEQUENO INFANTE\nSADIO E GRIMPANTE" );

	// como é a primeira página vista, define o tamanho antes de setState
	setSize( GameManager::getDeviceScreenSize().x, GameManager::getDeviceScreenSize().y );
	setState( STATE_SPLASH );
}


void Page1::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	blueBkg->setSize( size );

	float scale = setHeightPercent( body, BOY_HEIGHT_PERCENT );
	float bodyScale = scale;
	body->setAnchor( ANCHOR_CENTER );
	body->setAnchorPosition( size.x >> 1, size.y >> 1 );

	scale = setHeightPercent( cape, CAPE_HEIGHT_PERCENT );
	//cape->setPosition( body->getPosX() + ( int ) ( BOY_NECK_X * bodyScale - cape->getWidth() + CAPE_NECK_X * scale ), 
	//				   body->getPosY() + ( int ) ( BOY_NECK_Y * bodyScale - cape->getHeight() + CAPE_NECK_Y * scale ) );

	if ( scale != 1 ) {
		cape->setPosition( body->getPosX() + ( int ) ( BOY_NECK_X * bodyScale - CAPE_NECK_X * scale ), 
						   body->getPosY() + ( int ) ( BOY_NECK_Y * bodyScale - CAPE_NECK_Y * scale ) );
	}

	setHeightPercent( shadow, FLOOR_HEIGHT_PERCENT );
	shadow->setAnchor( ANCHOR_CENTER );
	shadow->setAnchorPosition( body->getRefPosX(), body->getPosY() + ( int ) ( body->getHeight() * 0.95f ) );

	textBox->autoSize();
	textBox2->autoSize();

	int totalWidth = e->getWidth() + textBox->getWidth();

	e->setPosition( ( width - totalWidth ) >> 1, shadow->getAnchorPosition( ANCHOR_BOTTOM ).y + 10 );
	textBox->setSize( textBox->getWidth(), textBox->getFont()->GetHeight() );
	textBox->setAnchor( ANCHOR_LEFT );
	textBox->setAnchorPosition( e->getPosX() + ( e->getWidth() * 9 / 10 ), e->getAnchorPosition( ANCHOR_CENTER ).y + 5 );
	
	textBox2->setPosition( e->getPosX() + ( e->getWidth() / 10 ), textBox->getPosY() + textBox->getHeight() );

	// splash
	int offsetX = ( int ) ( width * BUTTONS_OFFSET_X_PERCENT );
	int offsetY = ( int ) ( height * BUTTONS_OFFSET_Y_PERCENT );
	
	setHeightPercent( creditsButton, BUTTONS_HEIGHT_PERCENT );
	creditsButton->setAnchor( ANCHOR_CENTER );
	creditsButton->setAnchorPosition( offsetX, height - offsetY );
	creditsLabel->setSize( width, creditsLabel->getFont()->GetHeight() );
	creditsLabel->setAnchor( ANCHOR_TOP );
	creditsLabel->setAnchorPosition( creditsButton->getAnchorPosition( ANCHOR_BOTTOM ) );

	setHeightPercent( backButton, BUTTONS_HEIGHT_PERCENT );
	backButton->setAnchor( ANCHOR_CENTER );
	backButton->setAnchorPosition( offsetX, height - offsetY );
	backLabel->setSize( width, backLabel->getFont()->GetHeight() );
	backLabel->setAnchor( ANCHOR_TOP );
	backLabel->setAnchorPosition( backButton->getAnchorPosition( ANCHOR_BOTTOM ) );

	setHeightPercent( playButton, BUTTONS_HEIGHT_PERCENT );
	playButton->setAnchor( ANCHOR_CENTER );
	playButton->setAnchorPosition( width - offsetX, height - offsetY );
	playLabel->setSize( width, playLabel->getFont()->GetHeight() );
	playLabel->setAnchor( ANCHOR_TOP );
	playLabel->setAnchorPosition( playButton->getAnchorPosition( ANCHOR_BOTTOM ) );

	title->setSize( width, title->getFont()->GetHeight() );
}


void Page1::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_RUNNING );
		break;
	}
}


void Page1::setState( uint8 state ) {
	Page::setState( state );

	switch ( state ) {
		case STATE_SPLASH:
			PageManager::setCloseButtonVisible( false );

			blueBkg->setVisible( true );

			e->setVisible( false );
			textBox->setVisible( false );
			textBox2->setVisible( false );

			playButton->setSequenceIndex( BUTTON_SEQUENCE_PLAY_OFF );
			playButton->setVisible( true );
			playLabel->setVisible( true );
			
			creditsButton->setVisible( true );
			creditsButton->setSequenceIndex( BUTTON_SEQUENCE_CREDITS_OFF );
			creditsLabel->setVisible( true );
			
			backButton->setVisible( false );
			backButton->setSequenceIndex( BUTTON_SEQUENCE_BACK_OFF );
			backLabel->setVisible( false );

			songByLabel->setVisible( true );
			songByLabel->setAnchorPosition( getWidth() >> 1, title->getAnchorPosition( ANCHOR_BOTTOM ).y );
			drawnByLabel->setVisible( true );
			drawnByLabel->setAnchorPosition( getWidth() >> 1, songByLabel->getAnchorPosition( ANCHOR_BOTTOM ).y );

			// créditos
			animatedByLabel->setVisible( false );
			developedByLabel->setVisible( false );

			title->setVisible( true );
			title->setFont( FONT_BIG );
			title->setText( L"O POETA APRENDIZ" );

			textBox->setState( STATE_HIDDEN );
			textBox2->setState( STATE_HIDDEN );
		break;

		case STATE_CREDITS:
			title->setText( L"CRÉDITOS" );
			title->setFont( FONT_MEDIUM );

			playButton->setVisible( false );
			playLabel->setVisible( false );

			songByLabel->setAnchorPosition( ( int ) ( getWidth() * LABELS_LEFT_X_PERCENT ), ( int ) ( getHeight() * SONG_LABEL_Y_PERCENT ) );
			drawnByLabel->setAnchorPosition( ( int ) ( getWidth() * LABELS_LEFT_X_PERCENT ), ( int ) ( getHeight() * DRAWN_LABEL_Y_PERCENT ) );

			// créditos
			developedByLabel->setVisible( true );
			developedByLabel->setAnchorPosition( ( int ) ( getWidth() * LABELS_RIGHT_X_PERCENT ), ( int ) ( getHeight() * DEVELOPED_LABEL_Y_PERCENT ) );

			animatedByLabel->setVisible( true );
			animatedByLabel->setAnchorPosition( ( int ) ( getWidth() * LABELS_RIGHT_X_PERCENT ), ( int ) ( getHeight() * ANIMATED_LABEL_Y_PERCENT ) );

			creditsButton->setVisible( false );
			creditsLabel->setVisible( false );

			backButton->setVisible( true );
			backLabel->setVisible( true );
		break;

		case STATE_SHOW_TEXT:
			PageManager::setCloseButtonVisible( true );

			title->setVisible( false );
			
			blueBkg->setVisible( false );
			backButton->setVisible( false );
			backLabel->setVisible( false );
			creditsButton->setVisible( false );
			creditsLabel->setVisible( false );
			playButton->setVisible( false );
			playLabel->setVisible( false );
			songByLabel->setVisible( false );
			drawnByLabel->setVisible( false );

			e->setVisible( true );
			textBox->setVisible( true );
			textBox2->setVisible( true );
			textBox->setState( STATE_APPEARING );
			textBox2->setState( STATE_APPEARING );
		break;

		case STATE_BOY_RUNNING:
		break;
	}
}


void Page1::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_BACK_BUTTON:
			case OBJ_BACK_LABEL:
				backButton->setSequenceIndex( BUTTON_SEQUENCE_BACK_ON );
			break;

			case OBJ_CREDITS_BUTTON:
			case OBJ_CREDITS_LABEL:
				creditsButton->setSequenceIndex( BUTTON_SEQUENCE_CREDITS_ON );
			break;

			case OBJ_PLAY_BUTTON:
			case OBJ_PLAY_LABEL:
				playButton->setSequenceIndex( BUTTON_SEQUENCE_PLAY_ON );
			break;
		}
	}
}


void Page1::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}


void Page1::OnFrameChanged( int id, int frameIndex ) {
}


void Page1::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case BUTTON_SEQUENCE_BACK_ON:
			backButton->setSequenceIndex( BUTTON_SEQUENCE_BACK_OFF );
			setState( STATE_SPLASH );
		break;

		case BUTTON_SEQUENCE_PLAY_ON:
			playButton->setSequenceIndex( BUTTON_SEQUENCE_PLAY_OFF );
			setState( STATE_SHOW_TEXT );
		break;

		case BUTTON_SEQUENCE_CREDITS_ON:
			creditsButton->setSequenceIndex( BUTTON_SEQUENCE_CREDITS_OFF );
			setState( STATE_CREDITS );
		break;
	}
}

