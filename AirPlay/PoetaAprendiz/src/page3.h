#ifndef PAGE_3_H
#define PAGE_3_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <src/rock.h>
#include <src/glass.h>
#include <components/spritelistener.h>

class Page3 : public Page, public SpriteListener {

public:
	Page3();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void update( uint32 delta );

	void setSize( int16 width, int16 height );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );


protected:

	PatternPtr bkg;

	SpritePtr boy;

	SpritePtr wings;

	DrawableImagePtr plic;

	DrawableImagePtr ploc;

	DrawableImagePtr ploco;

private:

	float plocScale;

	TextBoxPtr textBox2;

	CIwArray< RockPtr > rocks;

	CIwArray< GlassPtr > shards;

	void throwRock();

	void refreshPlicPloc();

};
#endif