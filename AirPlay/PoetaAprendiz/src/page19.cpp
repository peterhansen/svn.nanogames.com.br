#include <src/page19.h>

#define PATH ( Constants::Path::IMAGES + "19/" )

#define BKG_WIDTH_PERCENT 0.26f
#define BKG_HEIGHT_PERCENT 0.47f

#define BKG_COLOR 0xbea845ff

#define BOY_HEIGHT_PERCENT 0.28f
#define BOY_START_X ( getWidth() >> 1 )

#define BOY_START_Y ( -boy->getHeight() )

#define LAMP_X 116
#define LAMP_Y 66

#define BOOK_X 26
#define BOOK_Y 149
#define BOOK_SEQUENCE_STOPPED 0
#define BOOK_SEQUENCE_FLIPPING 1

#define TABLE_Y -22

#define TIME_POINTS_APPEARING 1000
#define TIME_CLOUD_APPEAR 1200

#define POINTS_TOTAL 3


enum {
	OBJ_BKG,
	OBJ_TABLE,
	OBJ_BOY,
	OBJ_BOOK,
	OBJ_LAMP,
};


Page19::Page19() : Page(), pressing( false ), accTime( 0 ) {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	table = DrawableImagePtr( new DrawableImage( PATH + "table.png" ) );
	insertDrawable( table );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	book = SpritePtr( new Sprite( PATH + "book" ) );
	book->addListener( OBJ_BOOK, this );
	insertDrawable( book );

	lamp = SpritePtr( new Sprite( PATH + "lamp" ) );
	insertDrawable( lamp );

	for ( int i = 0; i < POINTS_TOTAL; ++i ) {
		SpritePtr p = SpritePtr( new Sprite( PATH + "point" ) );
		points.append( p );
		p->setAnchor( ANCHOR_CENTER );
		p->setFrameIndex( POINTS_TOTAL - 1 - i );
		insertDrawable( p );
	}

	canvas = CanvasPtr( new Canvas( PATH + "cloud.png" ) );
	insertDrawable( canvas );

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page19::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( getWidth() >> 1, getHeight() >> 1 );

	scaleDrawable( table, scale );
	table->setAnchor( ANCHOR_TOP );
	table->setAnchorPosition( boy->getRefPosX(), boy->getAnchorPosition( ANCHOR_BOTTOM ).y + ( int ) ( TABLE_Y * scale ) );
	
	scaleDrawable( book, scale );
	book->setAnchor( ANCHOR_TOP_LEFT );
	book->setPosition( boy->getPosition() + Point( ( int ) ( BOOK_X * scale ), ( int ) ( BOOK_Y * scale ) ) );

	scaleDrawable( lamp, scale );
	lamp->setAnchor( ANCHOR_BOTTOM_LEFT );
	lamp->setPosition( boy->getPosition() + Point( ( int ) ( LAMP_X * scale ), ( int ) ( LAMP_Y * scale ) ) );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_BOTTOM );
	bkg->setAnchorPosition( table->getAnchorPosition( ANCHOR_TOP ) + Point( 0, 20 ) );

	ImagePtr cloud = canvas->getCloud();
	canvas->setCanvasSize( bkg->getPosX(), cloud->GetHeight() );
	canvas->setPosition( 0, 0 );
	canvas->setAnchor( ANCHOR_BOTTOM_RIGHT );

	Point startPoint = boy->getAnchorPosition( ANCHOR_TOP_LEFT );
	Point endPoint = canvas->getRefPosition();
	for ( uint i = 0; i < points.size(); ++i ) {
		Point p = Point( startPoint.x + ( endPoint.x - startPoint.x ) * ( i + 1 ) / ( points.size() + 1 ), startPoint.y + ( endPoint.y - startPoint.y ) * ( i + 1 ) / ( points.size() + 1 ) );
		points[ i ]->setAnchorPosition( p );
	}
}


void Page19::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			accTime = 0;
			pressing = false;
			canvas->clear();
			for ( uint i = 0; i < points.size(); ++i ) {
				setDrawableOpacity( points[ i ], 0 );
			}
			canvas->setCloudColor( CreateColor( 0x00ffffff ) );

			textBox->setText( L"SONHANDO O POETA\nQUE QUEM SABE UM DIA" );
		break;

		case STATE_DREAM_APPEARS:
			//canvas->setRedrawMode( true );
		break;

		case STATE_DRAW:
			canvas->setRedrawMode( false );
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page19::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_THINKING );
		break;

		case STATE_DREAM_APPEARS:
			accTime += delta;
			for ( uint i = 0; i < points.size(); ++i ) {
				float limit = ( i + 1 ) * TIME_POINTS_APPEARING / ( float ) points.size();
				setDrawableOpacity( points[ i ], accTime / limit );
			}

			if ( accTime >= TIME_POINTS_APPEARING ) {
				Color c = Color();
				c.r = 0xff;
				c.g = 0xff;
				c.b = 0xff;
				canvas->setCloudColor( getOpacityColor( c, ( accTime - TIME_POINTS_APPEARING ) / ( float ) TIME_CLOUD_APPEAR ) );
				canvas->draw( CIwArray< Point >() );

				if ( accTime >= TIME_CLOUD_APPEAR + TIME_POINTS_APPEARING )
					setState( STATE_DRAW );
			}
		break;

		case STATE_THINKING:
		break;

		case STATE_DRAW:
		break;
	}
}


void Page19::OnFrameChanged( int id, int frameIndex ) {
}


void Page19::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( id ) {
		case OBJ_BOOK:
			switch ( sequenceIndex ) {
				case BOOK_SEQUENCE_FLIPPING:
					book->setSequenceIndex( BOOK_SEQUENCE_STOPPED );
				break;
			}
		break;
	}
}


void Page19::pointerButtonCB( const s3ePointerEvent *ev ) {
	switch ( state ) {
		case STATE_DRAW:
			lastPos.x = ev->m_x;
			lastPos.y = ev->m_y;

			if ( ev->m_Pressed ) {
				pressing = true;

				// faz o desenho de um ponto, mesmo que usu�rio n�o tenha movido 
				s3ePointerMotionEvent e;
				e.m_x = ev->m_x;
				e.m_y = ev->m_y;
				pointerMotionCB( &e );
			} else {
				pressing = false;
			}
		break;

		case STATE_THINKING:
			if ( ev->m_Pressed ) {
				int index = getObjectIndexAt( ev->m_x, ev->m_y );
				switch ( index ) {
					case OBJ_BOY:
						//boy->setSequenceIndex( ); TODO
						setState( STATE_DREAM_APPEARS );
					break;

					case OBJ_BOOK:
						if ( book->getSequenceIndex() == BOOK_SEQUENCE_STOPPED ) {
							book->setSequenceIndex( BOOK_SEQUENCE_FLIPPING );
						}
					break;

					case OBJ_LAMP:
						lamp->setFrameIndex( lamp->getFrameSequenceIndex() == 0 ? 1 : 0 );
					break;
				}
			}
		break;
	}
}


void Page19::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	switch ( state ) {
		case STATE_DRAW:
			Point p = Point( ev->m_x, ev->m_y );

			if ( pressing ) {
				CIwArray< Point > points;
				points.append( p );
				points.append( lastPos );

				canvas->draw( points );
				lastPos = p;
			}
		break;
	}
}


void Page19::paint() {
	Page::paint();
}


void Page19::setDrawableOpacity( DrawablePtr d, float opacity ) {
	d->setColor( getOpacityColor( Color( d->getColor() ), opacity ) );
}


Color Page19::getOpacityColor( Color c, float opacity ) {
	opacity = CLAMP( opacity, 0.0f, 1.0f );
	c.a = ( int ) ( 0xff * opacity );
	return c;
}
