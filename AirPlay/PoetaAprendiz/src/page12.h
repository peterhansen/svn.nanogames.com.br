#ifndef PAGE_12_H
#define PAGE_12_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <components/spritelistener.h>

class Page12 : public Page, public SpriteListener {

public:
	Page12();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	void update( uint32 delta );

protected:

	SpritePtr boy;

	SpritePtr broom;

	SpritePtr dust;

	DrawableImagePtr bkg;

	DrawableImagePtr stairs;

	int broomAccTime;

	int broomTargetX;

	int broomInitialX;

private:

};
#endif