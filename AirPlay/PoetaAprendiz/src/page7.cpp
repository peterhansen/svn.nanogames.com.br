#include <src/page7.h>

#define PATH ( Constants::Path::IMAGES + "7/" )

#define BOY_START_X ( getWidth() >> 3 )
#define BOY_START_Y ( -boy->getHeight() )

#define BOY_END_X_PERCENT 0.7f
#define BOY_END_Y_PERCENT 0.8f

#define BOY_END_X ( ( getWidth() * BOY_END_X_PERCENT ) )
#define BOY_END_Y ( getHeight() * BOY_END_Y_PERCENT )

#define BOY_HEIGHT_PERCENT 0.3f

#define BKG_COLOR 0x70a9dfff

#define BKG_WIDTH_PERCENT 0.8f
#define BKG_HEIGHT_PERCENT 0.25f

#define WATER_SEQUENCE_STOPPED 0
#define WATER_SEQUENCE_MOVING 1

#define SPLASH_SEQUENCE_STOPPED 0
#define SPLASH_SEQUENCE_SPLASH 1

#define TIME_BOY_FALLING 1200


enum {
	OBJ_BKG,
	OBJ_WATER,
	OBJ_SPLASH,
	OBJ_BOY,
};


Page7::Page7() : accTime( 0 ) {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	printf( "depois de bkg\n" );

	water = SpritePtr( new Sprite( PATH + "water" ) );
	water->addListener( OBJ_WATER, this );
	insertDrawable( water );

	printf( "depois de water\n" );

	splash = SpritePtr( new Sprite( PATH + "splash" ) );
	splash->addListener( OBJ_SPLASH, this );
	insertDrawable( splash );

	printf( "depois de splash\n" );

	boy = DrawableImagePtr( new DrawableImage( PATH + "boy.png" ) );
	insertDrawable( boy );

	printf( "depois de boy\n" );

	createTextBox();
	setState( STATE_SHOW_TEXT_1 );
}


void Page7::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( getAnchorPosition( ANCHOR_CENTER ) );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setPosition( BOY_START_X, BOY_START_Y );
	
	scaleDrawable( water, scale );
	water->setAnchor( ANCHOR_CENTER );
	water->setAnchorPosition( ( int ) ( width * BOY_END_X_PERCENT ), ( int ) ( height * BOY_END_Y_PERCENT + 50 ) );

	scaleDrawable( splash, scale );
	splash->setAnchor( ANCHOR_BOTTOM );
	splash->setAnchorPosition( water->getAnchorPosition( ANCHOR_CENTER ) );

	boy->setViewPort( Rectangle( Point( 0, 0 ), Point( width, water->getAnchorPosition( ANCHOR_CENTER ).y ) ) );
}


void Page7::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT_1:
			if ( textBox->getState() == STATE_VISIBLE ) {
				setState( STATE_BOY_FALLING );
			}
		break;

		case STATE_BOY_FALLING:
			updateJump( delta );

			if ( accTime >= TIME_BOY_FALLING )
				setState( STATE_SHOW_TEXT_2 );
		break;

		case STATE_SHOW_TEXT_2:
			updateJump( delta );
			switch ( textBox->getState() ) {
				case STATE_HIDDEN:
					textBox->setText( L"E DAVA O MERGULHO\nSEM FAZER BARULHO" );
					textBox->setState( STATE_APPEARING );
				break;
			}
		break;
		
		case STATE_SPLASH:
		break;
	}
}


void Page7::updateJump( int delta ) {
	if ( boy->getPosY() >= getHeight() )
		return;

	accTime += delta;
	float percent = accTime / ( float ) TIME_BOY_FALLING;
	boy->setAnchorPosition( BOY_START_X + ( int ) ( ( BOY_END_X - BOY_START_X ) * percent ),
							BOY_START_Y + ( int ) ( ( BOY_END_Y - BOY_START_Y ) * percent ) );
}


void Page7::OnFrameChanged( int id, int frameIndex ) {
}


void Page7::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( id ) {
		case OBJ_SPLASH:
			splash->setSequenceIndex( SPLASH_SEQUENCE_STOPPED );
			splash->setVisible( false );
		break;

		case OBJ_WATER:
			water->setSequenceIndex( WATER_SEQUENCE_STOPPED );
			water->setVisible( false );
		break;
	}
}


void Page7::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT_1:
			water->setVisible( false );
			splash->setVisible( false );
			boy->setPosition( BOY_START_X, BOY_START_Y );
			accTime = 0;

			textBox->setText( L"SALTAVA DE ANJO\nMELHOR QUE MARMANJO" );
		break;
		
		case STATE_SHOW_TEXT_2:
			//textBox->setText( L"E DAVA O MERGULHO\nSEM FAZER BARULHO" );
			//textBox->setState( STATE_APPEARING );
			textBox->hide();
			
			water->setVisible( true );
			water->setSequenceIndex( WATER_SEQUENCE_MOVING );

			splash->setVisible( true );
			splash->setSequenceIndex( SPLASH_SEQUENCE_SPLASH );
		break;
		
		case STATE_BOY_FALLING:
			//textBox->setState( STATE_HIDING );
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page7::pointerButtonCB( const s3ePointerEvent *ev ) {
}


void Page7::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
