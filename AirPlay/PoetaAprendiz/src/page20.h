#ifndef PAGE_20_H
#define PAGE_20_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page20 : public Page {

enum {
	STATE_SHOW_TEXT,
	STATE_BOY_RUNNING,
};

public:
	Page20();

	void pointerButtonCB( const s3ePointerEvent *ev ) {}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_BOY_RUNNING; 
	}

protected:

	DrawableImagePtr photo;

private:

};
#endif