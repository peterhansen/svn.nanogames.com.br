#include <src/page12.h>

#define PATH ( Constants::Path::IMAGES + "12/" )

#define BKG_HEIGHT_PERCENT 0.63f

#define BOY_X 54
#define BOY_Y 46

enum {
	STATE_SHOW_TEXT,
	STATE_SWEEPING,
};

enum {
	OBJ_BKG,
	OBJ_STAIRS,
	OBJ_BOY,
	OBJ_BROOM,
	OBJ_DUST
};

#define BOY_SEQUENCE_STOPPED 0
#define BOY_SEQUENCE_THROW_BALL	1

#define DUST_SEQUENCE_STOPPED 0
#define DUST_SEQUENCE_MOVING 1

#define BROOM_SEQUENCE_STOPPED 0
#define BROOM_SEQUENCE_MOVING 1

#define BROOM_MOVE_TIME 1200



Page12::Page12() : Page(), broomAccTime( BROOM_MOVE_TIME ), broomInitialX( 0 ), broomTargetX( 0 ) {
	bkg = DrawableImagePtr( new DrawableImage( PATH + "bkg.png" ) );
	insertDrawable( bkg );

	stairs = DrawableImagePtr( new DrawableImage( PATH + "stairs.png" ) );
	insertDrawable( stairs );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	boy->addListener( OBJ_BOY, this );
	insertDrawable( boy );

	broom = SpritePtr( new Sprite( PATH + "broom" ) );
	broom->addListener( OBJ_BROOM, this );
	insertDrawable( broom );

	dust = SpritePtr( new Sprite( PATH + "dust" ) );
	dust->addListener( OBJ_DUST, this );
	insertDrawable( dust );

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page12::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( bkg, BKG_HEIGHT_PERCENT );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( size.x >> 1, size.y * 45 / 100 );
	bkg->setAnchor( ANCHOR_RIGHT );

	float stairsScale = bkg->getWidth() / ( float ) stairs->getWidth();
	stairs->setSize( bkg->getWidth(), ( int ) ( stairs->getHeight() * stairsScale ) );
	stairs->setAnchor( ANCHOR_BOTTOM_LEFT );
	stairs->setAnchorPosition( bkg->getAnchorPosition( ANCHOR_BOTTOM_LEFT ) );

	scaleDrawable( broom, scale );
	scaleDrawable( dust, scale );
	scaleDrawable( boy, scale );

	broom->setViewPort( Rectangle( bkg->getPosition(), bkg->getSize() ) );
	broom->setAnchor( ANCHOR_BOTTOM_LEFT );
	broom->setAnchorPosition( stairs->getAnchorPosition( ANCHOR_TOP_RIGHT ) );

	dust->setAnchor( ANCHOR_BOTTOM_RIGHT );
	dust->setViewPort( Rectangle( bkg->getPosition(), bkg->getSize() ) );

	boy->setPosition( stairs->getPosX() + ( int ) ( BOY_X * scale ), stairs->getPosY() + ( int ) ( BOY_Y * scale ) );
}


void Page12::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			boy->setSequenceIndex( BOY_SEQUENCE_THROW_BALL );
			broom->setSequenceIndex( BROOM_SEQUENCE_MOVING );
			dust->setSequenceIndex( DUST_SEQUENCE_STOPPED );

			broomAccTime = 0;
			broomTargetX = bkg->getRefPosX() + dust->getWidth();
			update( 5000 );

			textBox->setText( L"AMAVA AS CRIADAS\n         VARRENDO AS ESCADAS\n                         AMAVA AS GURIAS\n                                 DA RUA, VADIAS" );
		break;
		
		case STATE_SWEEPING:
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page12::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_BOY:
			if ( boy->getSequenceIndex() == BOY_SEQUENCE_STOPPED )
				boy->setSequenceIndex( BOY_SEQUENCE_THROW_BALL );
		break;

		case OBJ_STAIRS:
			broom->setSequenceIndex( BROOM_SEQUENCE_MOVING );
				
			broomInitialX = broom->getPosX();
			broomTargetX = bkg->getRefPosX() - broom->getWidth();
			broomAccTime = 0;
		break;
	}
}


void Page12::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}


void Page12::OnFrameChanged( int id, int frameIndex ) {
	switch ( id ) {
		case OBJ_BROOM:
			switch ( frameIndex % 4 ) {
				case 0:
				case 1:
					dust->setVisible( false );
				break;

				case 2:
				case 3:
					dust->setVisible( true );
					dust->setFrameIndex( frameIndex - 2 );
				break;
			}
		break;
	}
}


void Page12::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( id ) {
		case OBJ_DUST:
			dust->setSequenceIndex( DUST_SEQUENCE_STOPPED );
			dust->setVisible( false );
		break;

		case OBJ_BROOM:
			broomInitialX = broom->getPosX();
			broomTargetX = bkg->getRefPosX() + dust->getWidth();
			broomAccTime = 0;
		break;

		case OBJ_BOY:
			boy->setSequenceIndex( BOY_SEQUENCE_STOPPED );
		break;
	}
}


void Page12::update( uint32 delta ) {
	Page::update( delta );

	if ( broomAccTime < BROOM_MOVE_TIME ) {
		broomAccTime = CLAMP( broomAccTime + delta, 0, BROOM_MOVE_TIME );
		broom->setPosition( broomInitialX + ( ( broomTargetX - broomInitialX ) * broomAccTime / BROOM_MOVE_TIME ), broom->getPosY() );

		dust->setAnchorPosition( broom->getAnchorPosition( ANCHOR_BOTTOM_LEFT ) );
	}
}