#ifndef PAGE_18_H
#define PAGE_18_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page18 : public Page, public SpriteListener {

enum {
	STATE_SHOW_TEXT,
	STATE_BOY,
};

public:
	Page18();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	bool canAdvance() { 
		return state == STATE_BOY; 
	}

protected:

	PatternPtr bkg;

	SpritePtr boy;

private:

};
#endif