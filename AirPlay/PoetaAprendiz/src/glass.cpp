#include <src/glass.h>

#define PATH ( Constants::Path::IMAGES + "3/" )

#define GRAVITY ( GameManager::getDeviceScreenSize().y / 3.0f )

#define MIN_INITIAL_SPEED_Y ( -( GameManager::getDeviceScreenSize().y * 3 / 4 ) )
#define MAX_INITIAL_SPEED_Y ( -( GameManager::getDeviceScreenSize().y / 17 ) )

#define MIN_SPEED_X ( -( GameManager::getDeviceScreenSize().x >> 2 ) )
#define MAX_SPEED_X ( GameManager::getDeviceScreenSize().x >> 3 )

#define MIN_ANGULAR_SPEED -500
#define MAX_ANGULAR_SPEED 500


Glass::Glass( int index ) : Sprite( PATH + "glass" ) {
	setAnchor( ANCHOR_CENTER );

	speed.x = NanoMath::randomRange( MIN_SPEED_X, MAX_SPEED_X );
	speed.y = NanoMath::randomRange( MIN_INITIAL_SPEED_Y, MAX_INITIAL_SPEED_Y );
	angularSpeed = ( float ) NanoMath::randomRange( MIN_ANGULAR_SPEED, MAX_ANGULAR_SPEED );

	setFrameIndex( index % frames.size() );
}


void Glass::setInitialPosition( Point initialPosition ) {
	this->initialPosition = initialPosition;
	setPosition( initialPosition );
}


void Glass::reset() {
	setPosition( initialPosition );
	accTime = 0;
}


void Glass::update( uint32 delta ) {
	if ( getPosY() < GameManager::getDeviceScreenSize().y && getPosX() > -getWidth() && getPosX() < GameManager::getDeviceScreenSize().x ) {
		accTime += delta / 1000.0f;

		float yPos = initialPosition.y + speed.y * accTime + ( GRAVITY * 0.5f * accTime * accTime );
		setAnchorPosition( ( int ) ( initialPosition.x + ( accTime * speed.x ) ), ( int ) yPos );
		setAngle( ( ( int ) ( angularSpeed * accTime ) ) % 360 );
	} else {
		setVisible( false );
	}
}



