#ifndef PAGE_13_H
#define PAGE_13_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page13 : public Page, public SpriteListener {

enum {
	STATE_SHOW_TEXT,
	STATE_HEARTS,
};

public:
	Page13();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	bool canAdvance() { 
		return state == STATE_HEARTS; 
	}

protected:

	PatternPtr bkg;

	SpritePtr kids;

	SpritePtr heart;

	SoundPtr kiss;

private:

};
#endif