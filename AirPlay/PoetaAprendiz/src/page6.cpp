#include <src/page6.h>
#include <src/pagemanager.h>
#include <sstream>

#define PATH ( Constants::Path::IMAGES + "6/" )

#define BOY_HEIGHT_PERCENT 0.4f

#define BKG_HEIGHT_PERCENT 0.5f

#define BOY_OFFSET_Y -5

#define STARS 20

#define FRAMES_PER_SPRITE 18
#define BOY_SPRITES 3

#define BOY_SEQUENCE_STOPPED	0
#define BOY_SEQUENCE_APPEAR		1
#define BOY_SEQUENCE_JUMP		2
#define BOY_SEQUENCE_WALK_DOWN	3
#define BOY_SEQUENCE_WALK_UP	4

#define BOY_FRAME_TIME 100
#define BOY_TOTAL_FRAMES ( BOY_SPRITES * FRAMES_PER_SPRITE )
#define BOY_WALK_UP_TIME 2000
#define BOY_WALK_UP_TIME_WAIT ( BOY_WALK_UP_TIME + 500 )
#define BOY_WALK_UP_X 62

enum {
	OBJ_WALL,
	OBJ_BOY,
};


Page6::Page6() : Page(), accTime( 0 ), boyFrame( 0 ), boySequence( 0 ), accMoveTime( 0 ) {
	bkg = DrawableImagePtr( new DrawableImage( PATH + "bkg.png" ) );
	insertDrawable( bkg );

	for ( int i = 0; i < STARS; ++i ) {
		SpritePtr star = SpritePtr( new Sprite( PATH + "star" ) );
		star->setSequenceIndex( NanoMath::randomRange( 0, 7 ) );
		star->setAnchor( ANCHOR_CENTER );
		stars.append( star );
		insertDrawable( star );
	}

	wall = DrawableImagePtr( new DrawableImage( PATH + "wall.png" ) );
	insertDrawable( wall );

	for ( int i = 0; i < BOY_SPRITES; ++i ) {
		std::ostringstream morraCarai;
		morraCarai << ( PATH ) << "boy_" << i;

		SpritePtr s = SpritePtr( new Sprite( morraCarai.str() ) );
		s->setVisible( false );
		boy.append( s );
		insertDrawable( s );
	}

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page6::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	float scale = setHeightPercent( bkg, BKG_HEIGHT_PERCENT );
	float bkgScale = scale;
	scaleDrawable( wall, scale );

	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( width >> 1, height >> 1 );

	wall->setAnchor( ANCHOR_BOTTOM );
	wall->setAnchorPosition( bkg->getAnchorPosition( ANCHOR_BOTTOM ) );

	for ( int i = 0; i < STARS; ++i ) {
		stars[ i ]->setAnchorPosition( bkg->getPosX() + NanoMath::randomRange( 0, bkg->getWidth() ), 
									   bkg->getPosY() + NanoMath::randomRange( 0, wall->getPosY() - bkg->getPosY() ) );

		// cria um pouco de variação no tempo das estrelas
		stars[ i ]->setFrameIndex( NanoMath::randomRange( 0, 3 ) );
		stars[ i ]->update( NanoMath::randomRange( 0, 100 ) );
	}

	scale = ( bkg->getHeight() ) / ( float ) boy[ 0 ]->getHeight();
	Rectangle boyViewport = Rectangle( bkg->getPosition(), Point( bkg->getWidth(), wall->getAnchorPosition( ANCHOR_BOTTOM ).y - bkg->getPosY() ) );
	for ( int i = 0; i < BOY_SPRITES; ++i ) {
		SpritePtr s = boy[ i ];
		scaleDrawable( s, scale );
		s->setPosition( bkg->getPosX(), bkg->getPosY() + ( int ) ( bkgScale * BOY_OFFSET_Y ) );
		s->setViewPort( boyViewport );
	}
}


void Page6::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_JUMPING_WALL );
		break;
	}

	switch ( boySequence ) {
		case BOY_SEQUENCE_STOPPED:
		break;

		case BOY_SEQUENCE_WALK_UP:
		{
			if ( accMoveTime < BOY_WALK_UP_TIME ) {
				accTime += delta;
				if ( accTime >= BOY_FRAME_TIME ) {
					accTime %= BOY_FRAME_TIME;
					setBoyFrame( boyFrame == lastSequenceFrame ? lastSequenceFrame - 1 : lastSequenceFrame );
				}
			}

			accMoveTime += delta;
			int clampTime = NanoMath::min( accMoveTime, BOY_WALK_UP_TIME );
			setBoyPosition( bkg->getPosX() - boy[ 0 ]->getWidth() + ( ( boy[ 0 ]->getWidth() + BOY_WALK_UP_X ) * clampTime / BOY_WALK_UP_TIME ), boy[ 0 ]->getPosY() );
			if ( accMoveTime >= BOY_WALK_UP_TIME_WAIT ) {
				setBoySequence( BOY_SEQUENCE_APPEAR );
			}
		}
		break;

		default:
			accTime += delta;
			if ( accTime >= BOY_FRAME_TIME ) {
				accTime %= BOY_FRAME_TIME;
				nextBoyFrame();
			}
		break;
	}
}


void Page6::nextBoyFrame() {
	bool lastFrame = boyFrame == lastSequenceFrame;
	setBoyFrame( boyFrame + 1 );

	if ( lastFrame ) {
		switch ( boySequence ) {
			case BOY_SEQUENCE_APPEAR:
				setBoySequence( BOY_SEQUENCE_STOPPED );
			break;

			case BOY_SEQUENCE_JUMP:
				setBoySequence( BOY_SEQUENCE_WALK_DOWN );
			break;

			case BOY_SEQUENCE_WALK_DOWN:
				setBoySequence( BOY_SEQUENCE_WALK_UP );
			break;

			case BOY_SEQUENCE_WALK_UP:
				setBoySequence( BOY_SEQUENCE_APPEAR );
			break;
		}
	}
}


void Page6::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( L"PULAVA NO ESCURO\nNÃO IMPORTA QUE MURO" );
			setBoySequence( BOY_SEQUENCE_APPEAR );
		break;
		
		case STATE_BOY_JUMPING_WALL:
			/*setBoySequence( BOY_SEQUENCE_APPEAR );*/
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page6::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed && boySequence == BOY_SEQUENCE_STOPPED ) {
		setBoySequence( BOY_SEQUENCE_JUMP );
		accMoveTime = 0;
	}
}


void Page6::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}


void Page6::setBoySequence( int sequence ) {
	int firstFrame;
	switch ( sequence ) {
		case BOY_SEQUENCE_APPEAR:
			setBoyPosition( bkg->getPosX(), boy[ 0 ]->getPosY() );
			firstFrame = 52;
			lastSequenceFrame = 16;
		break;
	
		case BOY_SEQUENCE_JUMP:
			firstFrame = 17;
			lastSequenceFrame = 36;
		break;
	
		case BOY_SEQUENCE_WALK_DOWN:
			firstFrame = 37;
			lastSequenceFrame = 49;
		break;

		case BOY_SEQUENCE_WALK_UP:
			setBoyPosition( bkg->getPosX() - boy[ 0 ]->getWidth(), boy[ 0 ]->getPosY() );
			firstFrame = 50;
			lastSequenceFrame = 51;
		break;

		case BOY_SEQUENCE_STOPPED:
			boySequence = sequence;
		default:
			return;
	}

	boySequence = sequence;
	setBoyFrame( firstFrame );
}


void Page6::setBoyFrame( int frame) {
	boyFrame = frame % BOY_TOTAL_FRAMES;
	
	int spriteIndex = boyFrame / FRAMES_PER_SPRITE;
	int frameIndex = boyFrame % FRAMES_PER_SPRITE;
	for ( int i = 0; i < BOY_SPRITES; ++i ) {
		boy[ i ]->setVisible( i == spriteIndex );
		if ( i == spriteIndex )
			boy[ i ]->setFrameIndex( frameIndex );
	}
}


void Page6::setBoyPosition( int x, int y ) {
	for ( int i = 0; i < BOY_SPRITES; ++i ) {
		boy[ i ]->setPosition( x, y );
	}
}
