#ifndef PAGE_1_H
#define PAGE_1_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <src/doublelabel.h>


class Page1 : public Page, public SpriteListener {

enum {
	STATE_SPLASH,
	STATE_CREDITS,
	STATE_SHOW_TEXT,
	STATE_BOY_RUNNING,
};

public:
	Page1();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_BOY_RUNNING; 
	}

protected:

	TextBoxPtr textBox2;

	SpritePtr body;

	DrawableImagePtr shadow;

	DrawableImagePtr e;

	SpritePtr cape;

	SpritePtr creditsButton;
	LabelPtr creditsLabel;

	SpritePtr playButton;
	LabelPtr playLabel;

	SpritePtr backButton;
	LabelPtr backLabel;

	LabelPtr title;

	DoubleLabelPtr songByLabel;

	DoubleLabelPtr drawnByLabel;

	DoubleLabelPtr developedByLabel;

	DoubleLabelPtr animatedByLabel;

	DrawableImagePtr blueBkg;


private:

};
#endif