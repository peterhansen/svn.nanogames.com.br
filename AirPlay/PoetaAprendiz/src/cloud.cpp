#include <src/cloud.h>

#include <sstream>

#define PATH ( Constants::Path::IMAGES )

#define MIN_SPEEED 30

#define MAX_SPEED 100

#define CLOUD_TYPES 2


Cloud::Cloud( int type ) : DrawableImage( getPath( type ) ), speed( 0 ), multiplier( 1.0f ) {
	//setColor( 0xffffff77 );
	setAnchor( ANCHOR_CENTER );
	randomSpeed();
}


void Cloud::update( uint32 delta ) {
	float diff = remainder + ( speed * multiplier ) * delta / 1000.0f;
	int absDiff = ( int ) diff;
	remainder = diff - absDiff;
	move( absDiff, 0 );

	if ( getPosX() <= -getWidth() )
		reset();
}


void Cloud::setSpeedMultiplier( float multiplier ) {
	this->multiplier = multiplier;
}


void Cloud::reset() {
	randomSpeed();
	setAnchorPosition( GameManager::getDeviceScreenSize().x + ( getWidth() >> 1 ), NanoMath::randomRange( -getHeight() >> 1, GameManager::getDeviceScreenSize().y + ( getHeight() >> 1 ) ) );
}


void Cloud::randomSpeed() {
	speed = -5 - ( float ) NanoMath::randomRange( MIN_SPEEED, MAX_SPEED );
}


std::string Cloud::getPath( int type ) {
	std::stringstream s;
	s << PATH << "cloud_" << ( type % CLOUD_TYPES ) << ".png";

	return s.str();
}
