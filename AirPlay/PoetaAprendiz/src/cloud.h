#ifndef CLOUD_H
#define CLOUD_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <components/gamemanager.h>

#define CloudPtr	boost::shared_ptr< Cloud >

class Cloud : public DrawableImage {

public:
	Cloud( int type );

	void update( uint32 delta );

	void setSpeedMultiplier( float multiplier );

protected:

	float speed;

	float multiplier;

	float remainder;

private:

	std::string getPath( int type );

	void reset();

	void randomSpeed();

};
#endif