#ifndef CANVAS_H
#define CANVAS_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <components/gamemanager.h>

#define CLOUD_WIDTH 527
#define CLOUD_HEIGHT 237


#define CanvasPtr	boost::shared_ptr< Canvas >

class Canvas : public Drawable {

public:
	Canvas( std::string cloudPath );
	Canvas( std::string cloudPath, Point size );
	~Canvas();

	void paint();

	void draw( CIwArray< Point > points );

	void setCanvasSize( Point size, bool setDrawableSize = true );

	void setCanvasSize( int16 width, int16 height, bool setDrawableSize = true );

	void clear( uint32 argb = 0xffffffff );

	CIw2DSurface* getSurface();

	void setCloudColor( Color color );

	ImagePtr getCloud();

	void setRedrawMode( bool redraw );

protected:

	CIw2DSurface* surface;

	ImagePtr image;

	ImagePtr brush;

	ImagePtr cloud;

	Color cloudColor;

	bool redraw;


private:

	bool hitCloud( int x, int y );

	CIwImage cloudHitMap;

	CIwArray< Point > allPoints;

	bool hitMap[ CLOUD_WIDTH * CLOUD_HEIGHT ];

	void loadHitmap( std::string path );

};
#endif