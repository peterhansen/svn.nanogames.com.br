/** \file pageobject.h
	2011-10-15
*/

#ifndef PAGE_MANAGER_H
#define PAGE_MANAGER_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <src/loadscreen.h>

class Page;

//#define START_PAGE 6
//#define LAST_PAGE 20

#define PageManagerPtr	boost::shared_ptr<PageManager>

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class PageManager : public Screen {

public:

	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void devicePauseCB() {}
	void deviceUnpauseCB() {}

	void keyboardKeyCB( const s3eKeyboardEvent *ev ) {}
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}

	void setPageIndex( uint8 index );

	inline uint8 getPageIndex() { return currentPageIndex; }

	void advanceToChosenPage();

	//inline void advanceToNextPage() { setPageIndex( getPageIndex() + 1 ); }

	//inline void advanceToPreviousPage() { setPageIndex( getPageIndex() - 1 ); }

	inline void previousPage() { setPageIndex( getPageIndex() - 1 ); }

	static PageManagerPtr getInstance();

	static void unload();

	bool loadNextPage( LoadScreenPtr loadScreen );

	static void setCloseButtonVisible( bool visible );


protected:

	PagePtr currentPage;

	PagePtr nextPage;

	uint8 currentPageIndex;

	uint8 chosenPageIndex;

	void paint();

	void update( uint32 delta );

	bool checkNextPageButton( const s3ePointerEvent *ev );

	PagePtr loadPage( uint8 index );


private:

	PageManager();	

	static PageManagerPtr instance;

	CIwArray< PagePtr > loadedPages;

	CIwArray< Rectangle > currentViewports;
	CIwArray< Rectangle > nextViewports;

	float xTouchValue;
	float yTouchValue;
	float oldTouchX;
	float oldTouchY;

	uint8 flipState;

	bool moving;

	Point A;
	Point B;
	Point C;
	Point D;
	Point E;
	Point F;

	bool pressing;

	CIwArray< Point > path;

	void flipPage( float delta );

	void pointGenerate( float distance, int width, int height );
	
	void pointGenerate2( float xTouch, float yTouch, int width, int height );

	CIwArray< Point > pathOfTheMask();

	CIwArray< Point > pathOfFlippedPaper();

	float oldxF;
	float oldyF;

	float flipSpeedX;

	DrawableImagePtr paper;

	DrawableImagePtr closeButton;
	bool closeButtonClicked;
	LabelPtr closeLabelTitle;
	LabelPtr closeLabelYes;
	LabelPtr closeLabelNo;
	SpritePtr closeButtonYes;
	SpritePtr closeButtonNo;
	DrawableImagePtr closeBox;
	DrawableGroupPtr closeBoxGroup;

};

#endif