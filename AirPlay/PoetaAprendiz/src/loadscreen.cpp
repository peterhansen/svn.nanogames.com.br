#include <src/loadscreen.h>

#include <sstream>
#include <string>

#include <components/gamemanager.h>

#define PENCIL_HEIGHT_PERCENT 0.03f
#define PENCIL_Y_PERCENT 0.65f

#define BOOK_HEIGHT_PERCENT 0.13f

#define POINTS_INTERVAL 333

#define FONT_COLOR 0x5e99ff


LoadScreen::LoadScreen() : Screen(), points( 0 ) {
	pencilBkg = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "pencil.png" ) );
	pencilBkg->setColor( 0xffffff80 );
	insertDrawable( pencilBkg );

	pencilLoad = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "pencil.png" ) );
	insertDrawable( pencilLoad );

	book = SpritePtr( new Sprite( Constants::Path::IMAGES + "19/book" ) );
	book->setSequenceIndex( 2 );
	insertDrawable( book );

	FontPtr font = FontPtr( new Font( "fonts/TheGreatEscape.ttf", 10, 100 ) );
	label = LabelPtr( new Label( font, "Preparando a hist�ria..." ) );
	label->setColor( FONT_COLOR );
	label->setFormat( IW_GX_FONT_ALIGN_LEFT );
	insertDrawable( label );

	paper = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "paper.png" ) );
	paper->setColor( 0xffffff4f );
	insertDrawable( paper );

	setSize( GameManager::getDeviceScreenSize().x, GameManager::getDeviceScreenSize().y );
}


void LoadScreen::setProgress( float progress ) {
	Rectangle v = Rectangle( pencilLoad->getPosition(), Point( ( int ) ( pencilLoad->getWidth() * progress ), pencilLoad->getHeight() ) );
	pencilLoad->setViewPort( v );
}


void LoadScreen::setSize( int16 width, int16 height ) {
	Screen::setSize( width, height );

	setHeightPercent( pencilBkg, PENCIL_HEIGHT_PERCENT );
	pencilLoad->setSize( pencilBkg->getSize() );

	pencilBkg->setAnchor( ANCHOR_CENTER );
	pencilBkg->setAnchorPosition( width >> 1, ( int ) ( height * PENCIL_Y_PERCENT ) );

	pencilLoad->setPosition( pencilBkg->getPosition() );

	label->setText( "Preparando a hist�ria..." );
	label->autoSize();
	label->setAnchor( ANCHOR_TOP );
	label->setAnchorPosition( width >> 1, pencilBkg->getPosY() - label->getHeight() );

	setHeightPercent( book, BOOK_HEIGHT_PERCENT );
	book->setAnchor( ANCHOR_BOTTOM );
	book->setAnchorPosition( width >> 1, label->getPosY() - 15 );

	paper->setSize( width, height );
}


void LoadScreen::redraw() {
	Iw2DSurfaceClear(0xffffffff);
	draw();
	Iw2DSurfaceShow();
}


void LoadScreen::update( uint32 delta ) {
	Screen::update( delta );

	pointsAccTime += delta;
	if ( pointsAccTime >= POINTS_INTERVAL ) {
		pointsAccTime %= POINTS_INTERVAL;
		points = ( points + 1 ) % 4;
		std::string s = "Preparando a hist�ria";
		for ( int i = 0; i < points; ++i )
			s += '.';
		label->setText( s );
	}
}


float LoadScreen::setHeightPercent( DrawablePtr d, float percent ) {
	float scale = getHeight() * percent / d->getHeight();
	scaleDrawable( d, scale );

	return scale;
}


void LoadScreen::scaleDrawable( DrawablePtr d, float scale ) {
	d->setSize( ( int ) ( d->getWidth() * scale ), ( int ) ( d->getHeight() * scale ) );
}
