/** \file pageobject.h
	2011-10-15
*/

#ifndef PAGE_OBJECT_H
#define PAGE_OBJECT_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <components/pattern.h>
#include <src/constants.h>

#define PageObjectPtr	boost::shared_ptr<Drawable>

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class PageObject : public DrawableGroup {

public:
	PageObject();

	virtual void touchButtonCB( const s3ePointerTouchEvent *ev ) {}
	virtual void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	virtual void pointerButtonCB( const s3ePointerEvent *ev ) {}
	virtual void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}


protected:


private:

};
#endif