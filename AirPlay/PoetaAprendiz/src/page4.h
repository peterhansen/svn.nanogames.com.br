#ifndef PAGE_4_H
#define PAGE_4_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <components/tween.h>

class Page4 : public Page {

public:
	Page4();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void update( uint dt );

	void setSize( int16 width, int16 height );


protected:

	PatternPtr up;

	PatternPtr down;

	PatternPtr left;

	PatternPtr right;

	PatternPtr bkg;

	DrawableImagePtr eyes;

	SpritePtr boy;

	TweenPtr monsterTween;

	int8 draggedIndex;

	float accTime;

	Point lastPos;


private:

	CIwArray< DrawablePtr > objects;
	
	CIwArray< TextBoxPtr > labels;

	void moveEyes( int16 dx, int16 dy );

	void setEyesPosition( int16 x, int16 y );

	void setEyesVisibility( float level );

	void refreshEyes();

};
#endif