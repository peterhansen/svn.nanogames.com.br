#include <src/rock.h>

#define PATH ( Constants::Path::IMAGES + "3/" )

#define GRAVITY 10.0f

#define INITIAL_SPEED_Y -10.0f

#define MIN_PERCENT 0.2f

#define PERCENT_DIFF ( 1.0f - MIN_PERCENT )

#define SPEED ( GameManager::getDeviceScreenSize().x >> 1 )

#define MIN_ANGULAR_SPEED -300
#define MAX_ANGULAR_SPEED 300


Rock::Rock( Point initialPosition, Point finalPosition, Point finalSize ) : DrawableImage( PATH + "rock.png" ), active( true ) {
	this->initialPosition = initialPosition;
	setPosition( initialPosition );
	setSize( 32, 32 );
	this->finalSize = finalSize;

	speed = finalPosition - initialPosition;
	totalTime = speed.GetLength() / ( float ) SPEED;

	speed.Normalise();
	speed *= SPEED;

	angularSpeed = ( float ) NanoMath::randomRange( MIN_ANGULAR_SPEED, MAX_ANGULAR_SPEED );
}


void Rock::update( uint32 delta ) {
	if ( active ) {
		if ( getPosX() >= GameManager::getDeviceScreenSize().x ) {
			stop();
			return;
		}

		accTime += delta / 1000.0f;

		float percent = MIN_PERCENT + CLAMP( PERCENT_DIFF * ( accTime / totalTime ), 0, PERCENT_DIFF );
		setSize( ( int ) ( finalSize.x * percent ), ( int ) ( finalSize.y * percent ) );
		setAnchor( ANCHOR_CENTER );

		setAnchorPosition( ( int ) ( initialPosition.x + ( accTime * speed.x ) ), ( int ) ( initialPosition.y + ( accTime * speed.y ) ) );
		//float yPos = initialPosition.y + INITIAL_SPEED_Y * accTime + ( GRAVITY * 0.5f * accTime * accTime );
		//setAnchorPosition( initialPosition.x + ( accTime * speed ), yPos );
		setAngle( ( ( int ) ( angularSpeed * accTime ) ) % 360 );
	}
}


void Rock::stop() {
	active = false;
}


