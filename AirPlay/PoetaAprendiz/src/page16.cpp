#include <src/page16.h>

#define PATH ( Constants::Path::IMAGES + "16/" )

#define BOY_HEIGHT_PERCENT 0.33f

#define DESK_OFFSET_Y 197

#define BKG_COLOR 0x70a9df

#define BKG_WIDTH_PERCENT 0.2f
#define BKG_HEIGHT_PERCENT 0.6f



enum {
	STATE_SHOW_TEXT,
};

enum {
	OBJ_BOY,
};


Page16::Page16() : Page() {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	insertDrawable( bkg );

	desk = PatternPtr( new Pattern( ImagePtr( Iw2DCreateImage( ( "images/16/desk.png" ) ) ) ) );
	insertDrawable( desk );

	paper = DrawableImagePtr( new DrawableImage( PATH + "paper.png" ) );
	insertDrawable( paper );

	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	insertDrawable( boy );

	pageSounds = loadPageFlipSounds();

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page16::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( getAnchorPosition( ANCHOR_CENTER ) );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( width >> 1, height >> 1 );

	scaleDrawable( paper, scale );
	paper->setAnchor( ANCHOR_CENTER );
	paper->setAnchorPosition( boy->getPosX() + ( boy->getWidth() * 3 / 4 ), boy->getAnchorPosition( ANCHOR_BOTTOM ).y );

	desk->setPosition( bkg->getPosX(), boy->getPosY() + ( int ) ( scale * ( DESK_OFFSET_Y ) ) );
	desk->setSize( bkg->getWidth(), bkg->getAnchorPosition( ANCHOR_BOTTOM ).y - desk->getPosY() );
}


void Page16::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			textBox->setText( L"POR ISSO FAZIA\nSEU GRÃO DE POESIA" );
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page16::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 1 ) {
		lastPos = Point( ev->m_x, ev->m_y );
		DrawablePtr d = getObjectAt( lastPos );
		//if ( isPoster( d ) ) {
		//	dragged = d;

		//	// traz o poster carregado para a frente no desenho
		//	int boyIndex = getDrawableIndex( boy );
		//	setDrawableIndex( d, NanoMath::max( boyIndex - 1, 0 ) );
		//} else {
		//	dragged = DrawablePtr();

		//	if ( d == boy ) {
		//		// TODO garoto passa página da revista
		//		GameManager::playSound( pageSounds[ NanoMath::randomRange( 0, pageSounds.size() ) ] );
		//	}
		//}
	} else {
		dragged = DrawablePtr();
	}
}


void Page16::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	if ( dragged.get() != NULL ) {
		dragged->move( ev->m_x - lastPos.x, ev->m_y - lastPos.y );
		lastPos = Point( ev->m_x, ev->m_y );
	}
}


