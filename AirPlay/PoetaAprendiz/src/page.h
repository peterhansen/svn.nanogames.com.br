/** \file pageobject.h
	2011-10-15
*/

#ifndef PAGE_H
#define PAGE_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/textbox.h>
#include <src/constants.h>
#include <src/pageobject.h>
#include <components/sprite.h>
#include <components/sound.h>
#include <components/gamemanager.h>

#define PAGE_FLIPS_N_SOUNDS 3

class PageManager;

#define PagePtr	boost::shared_ptr<Page>

/** \class GameScreen
	\brief Tela principal do jogo./
*/
class Page : public DrawableGroup {

public:
	Page();
	Page( bool textBoxAutoSize );
	//virtual ~Page();

	virtual void pointerButtonCB( const s3ePointerEvent *ev );
	virtual void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	PageObjectPtr getObjectAt( Point p );

	bool checkNextPageButton( const s3ePointerEvent *ev );
	
	int8 getObjectIndexAt( Point p );

	inline int8 getObjectIndexAt( int16 x, int16 y ) { return getObjectIndexAt( Point( x, y ) ); }

	virtual void setState( uint8 state );

	inline uint8 getState() { return state; }

	void setSize( int16 width, int16 height );

protected:

	bool textBoxAutoSize;

	void createTextBox();

	TextBoxPtr textBox;

	uint8 state;

	virtual bool canAdvance();

	CIwArray< SoundPtr > loadPageFlipSounds();

	float setWidthPercent( DrawablePtr d, float percent );

	float setHeightPercent( DrawablePtr d, float percent );

	void scaleDrawable( DrawablePtr d, float scale );

private:


};
#endif