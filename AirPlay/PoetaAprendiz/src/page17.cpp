#include <src/page17.h>
#include <components/gamemanager.h>
#include <sstream>

#define PATH ( Constants::Path::IMAGES + "17/" )

#define BOY_START_X ( getWidth() >> 1 )

#define BOY_START_Y ( -boy->getHeight() )

#define MIN_TYPE_INTERVAL 50

#define MAX_TYPE_INTERVAL 420

#define TOTAL_SOUNDS 2

#define SOUND_PATH ( Constants::Path::SOUNDS )


Page17::Page17() : Page(), currentTextLength( 0 ), text( "E ACHAVA BONITA\nA PALAVRA ESCRITA" ) {
	createTextBox();
	FontPtr font = FontPtr( new Font( "fonts/Mom.ttf", 8, 100 ) );
	textBox->setFont( font );

	setState( STATE_TEXT_APPEARS );

	for ( int i = 0; i < TOTAL_SOUNDS; ++i ) {
		std::ostringstream morraCarai;
		morraCarai << SOUND_PATH << "type_" << i << ".wav";
		std::string morra = morraCarai.str();
		typeSounds.append( SoundPtr( new Sound( morra ) ) );
	}
}


void Page17::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	textBox->setText( text );
	textBox->autoSize();
	textBox->setAutoSizeMode( false );
	textBox->autoPosition();
	textBox->setText( "" );
}


void Page17::setState( uint8 state ) {
	switch ( state ) {
		case STATE_TEXT_APPEARS:
			textBox->setState( STATE_APPEARING );
			textBox->setText( "" );
			accTime = 0;
			currentTextLength = 0;
		break;

		case STATE_TEXT_SHOWN:
		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page17::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_TEXT_APPEARS:
			if ( textBox->getState() != STATE_VISIBLE )
				break;

			accTime += delta;
			if ( accTime >= nextLetterTime ) {
				++currentTextLength;
				GameManager::playSound( typeSounds[ NanoMath::randomRange( 0, TOTAL_SOUNDS ) ] );

				if ( ( uint ) currentTextLength < text.size() ) {
					randomizeNextLetter();
				} else {
					setState( STATE_TEXT_SHOWN );
				}
				textBox->setText( text.substr( 0, currentTextLength ) );
			}
		break;
	}
}


void Page17::randomizeNextLetter() {
	nextLetterTime = NanoMath::randomRange( MIN_TYPE_INTERVAL, MAX_TYPE_INTERVAL );
	accTime = 0;
}


void Page17::pointerButtonCB( const s3ePointerEvent *ev ) {
	switch ( state ) {
		case STATE_TEXT_APPEARS:
		break;

		case STATE_TEXT_SHOWN:

		break;
	}
}


void Page17::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
}
