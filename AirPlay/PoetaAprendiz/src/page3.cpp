#include <src/page3.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "3/" )

#define INITIAL_ROCK_POSITION Point( 300, 100 )

#define WINGS_SCALE 0.49f
#define WINGS_BOY_X 8
#define WINGS_BOY_Y 75

#define BOY_HEIGHT_PERCENT 0.3f

#define BOY_Y_PERCENT 0.2f

#define BKG_COLOR 0x70a9df

#define BKG_WIDTH_PERCENT 0.2f
#define BKG_HEIGHT_PERCENT 0.6f

#define PLOC_HEIGHT_PERCENT 0.1f

#define PLIC_OFFSET_X -70

#define PLIC_OFFSET_Y 30

#define PLOC_OFFSET Point( 250, 0 )

#define PLOCO_OFFSET Point( 140, 12 )

#define SHARDS 18

#define BOY_FRAME_THROW_STOPPED	4
#define BOY_FRAME_THROW_SHOT	10

#define ROCK_PERCENT_X ( 71.0f / 128.0f )
#define ROCK_PERCENT_Y ( 41.0f / 153.0f )


enum {
	INDEX_BKG,
	INDEX_BOY,
};

enum {
	SEQUENCE_BOY_STOPPED = 0,
	SEQUENCE_BOY_SHOOTING = 1,
	SEQUENCE_BOY_SHOT		= 2,
	SEQUENCE_BOY_RELOAD_AND_SHOOT = 3,
};

enum {
	STATE_SHOW_TEXT,
	STATE_PLIC_PLOC,
	STATE_BOY_SHOOTING,
};


Page3::Page3() : Page( false ) {
	bkg = PatternPtr( new Pattern( CreateColor( BKG_COLOR ) ) );
	
	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	boy->addListener( 0, this );

	wings = SpritePtr( new Sprite( PATH + "wings" ) );

	plic = DrawableImagePtr( new DrawableImage( PATH + "plic.png" ) );
	ploc = DrawableImagePtr( new DrawableImage( PATH + "ploc.png" ) );
	ploco = DrawableImagePtr( new DrawableImage( PATH + "ploco.png" ) );

	createTextBox();

	textBox2 = TextBoxPtr( new TextBox() );
	textBox2->setAnchor( ANCHOR_LEFT );

	for ( int i = 0; i < SHARDS; ++i ) {
		GlassPtr g = GlassPtr( new Glass( i ) );
		shards.append( g );
	}

	setState( STATE_SHOW_TEXT );
}


void Page3::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	bkg->setSize( ( int ) ( width * BKG_WIDTH_PERCENT ), ( int ) ( height * BKG_HEIGHT_PERCENT ) );
	bkg->setAnchor( ANCHOR_CENTER );
	bkg->setAnchorPosition( getAnchorPosition( ANCHOR_CENTER ) );

	float scale = setHeightPercent( boy, BOY_HEIGHT_PERCENT );
	boy->setAnchor( ANCHOR_TOP );
	boy->setAnchorPosition( width >> 1, ( int ) ( height * BOY_Y_PERCENT ) );

	scaleDrawable( wings, scale * WINGS_SCALE );
	wings->setPosition( boy->getPosX() + ( int ) ( scale * WINGS_BOY_X ), boy->getPosY() + ( int ) ( scale * WINGS_BOY_Y ) );

	textBox->autoPosition();
	textBox2->autoSize();

	TextBox temp = TextBox();
	temp.setText( "ERA " );

	plocScale = setHeightPercent( ploc, PLOC_HEIGHT_PERCENT );
	scaleDrawable( ploco, plocScale );
	scaleDrawable( plic, plocScale );

	int textBox2Y = textBox->getPosY() + textBox2->getHeight();

	plic->setPosition( textBox->getPosX() + temp.getTextWidth(), textBox2Y );

	temp.setText( " " );
	textBox2->setPosition( plic->getPosX() + plic->getWidth() + temp.getWidth(), textBox2Y );

	ploc->setPosition( textBox2->getPosX() + textBox2->getWidth() + temp.getWidth(), plic->getPosY() );

	ploco->setAnchor( ANCHOR_CENTER );
	ploco->setPosition( ploc->getPosition() + Point( ( int ) ( PLOCO_OFFSET.x * plocScale ), ( int ) ( PLOCO_OFFSET.y * plocScale ) ) );

	for ( int i = 0; i < SHARDS; ++i ) {
		shards[ i ]->setInitialPosition( ploco->getRefPosition() );
	}
}


void Page3::OnFrameChanged( int id, int frameIndex ) {
	switch ( boy->getSequenceIndex() ) {
		case SEQUENCE_BOY_SHOOTING:
			if ( frameIndex == BOY_FRAME_THROW_STOPPED ) {
				throwRock();
			}
		break;

		case SEQUENCE_BOY_RELOAD_AND_SHOOT:
			if ( frameIndex == BOY_FRAME_THROW_SHOT ) {
				throwRock();
			}
		break;
	}
	
}


void Page3::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case SEQUENCE_BOY_SHOOTING:
		case SEQUENCE_BOY_RELOAD_AND_SHOOT:
			boy->setSequenceIndex( SEQUENCE_BOY_SHOT );
		break;
	}
}


void Page3::setState( uint8 state ) {
	Page::setState( state );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			// reseta estado dos elementos
			boy->setSequenceIndex( SEQUENCE_BOY_STOPPED );
			rocks.clear();
			for ( int i = 0; i < SHARDS; ++i ) {
				shards[ i ]->reset();
			}
			ploco->setVisible( true );
			for ( uint i = 0; i < drawables.size(); ++i ) {
				DrawablePtr p = drawables[ i ];
			}
			
			removeAllDrawables();

			insertDrawable( bkg );
			insertDrawable( boy );
			insertDrawable( wings );
			insertDrawable( plic );
			insertDrawable( ploc );
			insertDrawable( ploco );
			insertDrawable( textBox );
			insertDrawable( textBox2 );

			textBox->setText( L"COM CHUMBO E BODOQUE\nERA" );
			textBox2->setText( "  E  " );

			refreshPlicPloc();
		break;

		case STATE_PLIC_PLOC:
			boy->setSequenceIndex( SEQUENCE_BOY_SHOOTING );
		break;
		
		case STATE_BOY_SHOOTING:
			ploco->setVisible( false );
			for ( int i = 0; i < SHARDS; ++i ) {
				insertDrawable( shards[ i ] );
			}
			update( 0 );
		break;
	}
}


void Page3::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT: {
			refreshPlicPloc();

			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_PLIC_PLOC );
		}
		break;

		case STATE_PLIC_PLOC:
			if ( rocks.size() > 0 ) {
				RockPtr r = rocks[ 0 ];
				if ( r->getRefPosY() >= ploco->getRefPosY() || r->getPosX() >= getWidth() ) {
					r->stop();
					r->setAnchorPosition( ploco->getRefPosition() );
					setState( STATE_BOY_SHOOTING );
				}
			}
		break;

		case STATE_BOY_SHOOTING:
			for ( uint i = 0; i < rocks.size(); ) {
				if ( rocks[ i ]->getPosX() >= getWidth() || rocks[ i ]->getPosY() >= getHeight() ) {
					removeDrawable( rocks[ i ] );
					rocks.erase( i );
				} else {
					++i;
				}
			}
		break;
	}
}


void Page3::pointerButtonCB( const s3ePointerEvent *ev ) {
	switch ( state ) {
		case STATE_BOY_SHOOTING:
			if ( ev->m_Pressed ) {
				int index = getObjectIndexAt( ev->m_x, ev->m_y );
				switch ( index ) {
					case INDEX_BOY:
						switch ( boy->getSequenceIndex() ) {
							case SEQUENCE_BOY_STOPPED:
								boy->setSequenceIndex( SEQUENCE_BOY_SHOOTING );
							break;

							case SEQUENCE_BOY_SHOT:
								boy->setSequenceIndex( SEQUENCE_BOY_RELOAD_AND_SHOOT );
							break;
						}
					break;
				}
			}
		break;
	}
}


void Page3::throwRock() {
	Point pos = Point( boy->getPosX() + ( int ) ( boy->getWidth() * ROCK_PERCENT_X ), 
					   boy->getPosY() + ( int ) ( boy->getHeight() * ROCK_PERCENT_Y ) );

	Point target = ( state == STATE_PLIC_PLOC ? ploco->getRefPosition() : ploco->getRefPosition() + Point( NanoMath::randomRange( 0, getWidth() >> 1 ), 0 ) );
	RockPtr rock = RockPtr( new Rock( pos, target, ploco->getSize() ) );
	rocks.append( rock );
	insertDrawable( rock );

	update( 0 );
}


void Page3::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}


void Page3::refreshPlicPloc() {
	float alpha = textBox->getTextAlpha();
	Color c = CreateColor( 0xffffffff );
	c.a = (uint8) ( 0xff * alpha );
	plic->setColor( c );
	ploc->setColor( c );
	ploco->setColor( c );
}
