/** \file TestScreen.h
	2011-10-15
*/

#ifndef TESTSCREEN_H
#define TESTSCREEN_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <components/pattern.h>
#include <src/constants.h>
#include <components/gamemanager.h>

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class TestScreen : public Screen {
public:
	TestScreen();

	void setSize( Point size )
	{
		DrawableGroup::setSize( size );
		label->setPosition( Point( 10, 10 ) );
		label->setSize( size );
	}

	void keyboardKeyCB( const s3eKeyboardEvent *ev ) {}
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}

	void touchButtonCB( const s3ePointerTouchEvent *ev ) {
	}
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	void pointerButtonCB( const s3ePointerEvent *ev ) {}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	virtual void devicePauseCB() {}
	virtual void deviceUnpauseCB() {}


protected:
	//DrawableImagePtr nanoLogo;
	FontPtr font;
	LabelPtr label;
	PatternPtr pattern;
	DrawableImagePtr image;
};
#endif