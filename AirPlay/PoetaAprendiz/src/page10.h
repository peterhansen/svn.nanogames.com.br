#ifndef PAGE_10_H
#define PAGE_10_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page10 : public Page, public SpriteListener {

enum {
	STATE_SHOW_TEXT,
	STATE_BOY_SLEEPING,
};

public:
	Page10();

	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_BOY_SLEEPING; 
	}

protected:

	SpritePtr boy;

	SoundPtr sound;

private:

};
#endif