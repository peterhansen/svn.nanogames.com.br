#include <src/Page10.h>
#include <src/pagemanager.h>
#include <components/gamemanager.h>

#define PATH ( Constants::Path::IMAGES + "10/" )


enum {
	OBJ_BOY,
};

enum {
	SEQUENCE_STOPPED,
	SEQUENCE_BREATHING,
};


Page10::Page10() : Page() {
	boy = SpritePtr( new Sprite( PATH + "boy" ) );
	boy->addListener( 0, this );
	insertDrawable( boy );

	//sound = SoundPtr( new Sound( Constants::Path::SOUNDS + "sea.raw" ) );

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page10::setSize( int16 width, int16 height ) {
	Page::setSize( width, height );

	boy->setAnchor( ANCHOR_CENTER );
	boy->setAnchorPosition( size.x >> 1, size.y >> 1 );
}


void Page10::OnFrameChanged( int id, int frameIndex ) {
}


void Page10::OnSequenceEnded( int id, int sequenceIndex ) {
	switch ( sequenceIndex ) {
		case SEQUENCE_BREATHING:
			boy->setSequenceIndex( SEQUENCE_STOPPED );
		break;
	}
}


void Page10::update( uint32 delta ) {
	Page::update( delta );

	switch ( state ) {
		case STATE_SHOW_TEXT:
			if ( textBox->getState() == STATE_VISIBLE )
				setState( STATE_BOY_SLEEPING );
		break;
	}
}


void Page10::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:
			boy->setSequenceIndex( SEQUENCE_STOPPED );
			textBox->setText( L"AMAVA ERA AMAR" );
		break;
		
		case STATE_BOY_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page10::touchButtonCB( const s3ePointerTouchEvent *ev ) {
}


void Page10::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page10::pointerButtonCB( const s3ePointerEvent *ev ) {
	int index = getObjectIndexAt( ev->m_x, ev->m_y );
	switch ( index ) {
		case OBJ_BOY:
			if ( boy->getSequenceIndex() == SEQUENCE_STOPPED )
				boy->setSequenceIndex( SEQUENCE_BREATHING );
			//GameManager::playSound( );
		break;
	}
}



void Page10::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
