#ifndef PAGE_5_H
#define PAGE_5_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page5 : public Page {

enum {
	STATE_SHOW_TEXT,
	STATE_BOY_RUNNING,
};

public:
	Page5();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_BOY_RUNNING; 
	}

protected:

	SpritePtr boy;

	DrawableImagePtr shadow;

	Point origin;

	Point direction;

	float totalTime;

	float accTime;

	Point boyOriginalSize;

	Point shadowOriginalSize;

private:

};
#endif