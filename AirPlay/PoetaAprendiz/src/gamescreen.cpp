#include <src/gamescreen.h>

void ButtonEventHandler::setScreen( GUIScreen *guiScreen )
{
	this->guiScreen = guiScreen;
}

bool ButtonEventHandler::FilterEvent( CIwEvent *pEvent )
{
	return pEvent->GetID() != IWUI_EVENT_CLICK;
}

bool ButtonEventHandler::HandleEvent( CIwEvent *pEvent )
{	
	CIwUIEventClick *pEventClick = IwSafeCast<CIwUIEventClick*>(pEvent);
	if( !pEventClick->GetPressed() )
		return false;

//#ifdef NANO_ONLINE_UNICODE_SUPPORT
//	const char * src = login->GetCaption();
//	int destSize = strlen(src);
//	mbstowcs( loginWStr, src, strlen(src) );
//	loginWStr[ destSize ] = 0;
//	guiScreen->customer.setNickname( loginWStr );
//#else
//	guiScreen->customer.setNickname( login->GetCaption() );
//#endif
//
//#ifdef NANO_ONLINE_UNICODE_SUPPORT
//	src = password->GetCaption();
//	mbstowcs( passwordWStr, src, strlen(src) );
//	destSize = strlen(src);
//	passwordWStr[ destSize ] = 0;
//	guiScreen->customer.setPassword( passwordWStr );
//#else
//	guiScreen->customer.setPassword( password->GetCaption() );
//#endif

	return true;
	//return NOCustomer::sendLoginRequest( &guiScreen->customer, guiScreen );
}

GUIScreen::GUIScreen()
{
	state = 1;
	IwGetResManager()->LoadGroup("UI.group");
	CIwResource* pResource = IwGetResManager()->GetResNamed( "testando", IW_UI_RESTYPE_ELEMENT );

	CIwUIElement* elmt =  (CIwUIElement*) pResource;
	IwGetUIView()->AddElement( elmt );
	IwGetUIView()->AddElementToLayout( elmt );
	IwGetUITextInput()->CreateSoftKeyboard();

	buttonEvHandler.setScreen( this );

	CIwUIButton *button = (CIwUIButton*) IwGetUIView()->GetChildNamed( "Button" );
	button->AddEventHandler( &buttonEvHandler );
}

GUIScreen::~GUIScreen()
{
	CIwUIButton *button = (CIwUIButton*) IwGetUIView()->GetChildNamed( "Button" );
	button->RemoveEventHandler( &buttonEvHandler );
}

