#ifndef PAGE_17_H
#define PAGE_17_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page17 : public Page {

enum {
	STATE_TEXT_APPEARS,
	STATE_TEXT_SHOWN,
};

public:
	Page17();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	bool canAdvance() { 
		return state == STATE_TEXT_SHOWN; 
	}

protected:

	int32 accTime;

	int32 nextLetterTime;

	int16 currentTextLength;

	void randomizeNextLetter();

	std::string text;

	CIwArray< SoundPtr > typeSounds;

private:


};
#endif