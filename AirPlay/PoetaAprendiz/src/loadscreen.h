/** \file pageobject.h
	2011-10-15
*/

#ifndef LOAD_SCREEN_H
#define LOAD_SCREEN_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/textbox.h>
#include <src/constants.h>
#include <src/pageobject.h>
#include <components/sprite.h>
#include <components/sound.h>
#include <components/gamemanager.h>

#define LoadScreenPtr	boost::shared_ptr<LoadScreen>

class LoadScreen: public Screen {

public:
	LoadScreen();

	void setProgress( float progress );

	void setSize( int16 width, int16 height );

	void redraw();

	void update( uint32 delta );

	void pointerButtonCB( const s3ePointerEvent *ev ) {}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}
	void touchButtonCB( const s3ePointerTouchEvent *ev ) {}
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}
	void keyboardKeyCB( const s3eKeyboardEvent *ev ) {}
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}
	void devicePauseCB() {}
	void deviceUnpauseCB() {}


protected:

	DrawableImagePtr paper;

	LabelPtr label;

	DrawableImagePtr pencilBkg;

	DrawableImagePtr pencilLoad;

	SpritePtr book;

	float setHeightPercent( DrawablePtr d, float percent );

	void scaleDrawable( DrawablePtr d, float scale );

	uint8 points;

	uint32 pointsAccTime;

};
#endif