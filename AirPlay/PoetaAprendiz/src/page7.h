#ifndef PAGE_7_H
#define PAGE_7_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page7 : public Page, public SpriteListener {

enum {
	STATE_SHOW_TEXT_1,
	STATE_BOY_FALLING,
	STATE_SHOW_TEXT_2,
	STATE_SPLASH,
};

public:
	Page7();

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void setSize( int16 width, int16 height );

	void update( uint32 delta );

	void OnFrameChanged( int id, int frameIndex );

	void OnSequenceEnded( int id, int sequenceIndex );

	bool canAdvance() { 
		return state == STATE_SPLASH; 
	}

protected:

	int accTime;

	PatternPtr bkg;

	DrawableImagePtr boy;

	SpritePtr splash;

	SpritePtr water;

private:

	void updateJump( int delta );

};
#endif