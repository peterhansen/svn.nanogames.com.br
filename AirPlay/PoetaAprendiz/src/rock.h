#ifndef ROCK_H
#define ROCK_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <components/gamemanager.h>

#define RockPtr	boost::shared_ptr< Rock >

class Rock : public DrawableImage {

public:
	Rock( Point initialPosition, Point finalPosition, Point finalSize );

	void update( uint32 delta );

	void stop();

protected:

	Point speed;

	bool active;

	float angularSpeed;

	Point initialPosition;

	float accTime;

	Point finalSize;

	float totalTime;

private:

};
#endif