/** \file sprite.h
	27/05/2011
*/
#include <string>

#include <Iw2D.h>
#include <IwGx.h>
#include <s3e.h>

#include <components\defines.h>
#include <components\drawable.h>

#ifndef SPRITE_H
#define SPRITE_H

#define currentSequence ( sequences[ sequenceIndex ] )
#define currentFrame ( frames[ currentSequence[ frameIndex ].x ] )

/** \struct Frame 
	\brief Frame da animação de uma sequência de um sprite. 
*/
struct Frame
{ 
	// TODO: fazer essa struct ser privada?
	Rectangle bounds;			//!< Retângulo envolvente de recorte dentro da spritesheet.
	Point offset;				//!<
};

/** \class Sprite 
	\brief Um sprite com diferentes sequências de animação. 
*/
class Sprite : public Drawable
{
public:
	/**
	Construtor da classe Sprite.
		@param image Imagem para ser usada como spritesheet.
		@param seqFile Arquivo .seq que descreve as sequencias do sprite. Para saber mais sobre o arquivo .ser,
		leia sobre o método readSequence.
		@see readSequence
	*/
	Sprite( ImagePtr image, std::string seqFile );
	
	Sprite( std::string imageFile, std::string seqFile );

	Sprite( std::string prefix );

	~Sprite() { sequences.clear_optimised(); }

	/** Incrementa o índice do frame da sequência atual. */
	void nextFrame() { setFrameIndex( frameIndex + 1 ); }

	/** Decrementa o índice do frame da sequência atual. */
	void previousFrame() { setFrameIndex( frameIndex - 1 ); }

	/** Atualiza o índice do frame da sequência atual, caso o índice seja válido. */
	void setFrameIndex( uint8 frameIndex );

	/** Atualiza o índice da sequência do sprite, caso o índice seja válido. Esse método
		zera o índice de frame da sequência automaticamente. */
	void setSequenceIndex( uint8 sequenceIndex );

	inline uint8 getFrameSequenceIndex() { return ( uint8 ) currentSequence[ frameIndex ].x; }

	inline uint8 getSequenceIndex() { return sequenceIndex; }

	void update( unsigned int dt );

protected:

	CIwArray<Frame> frames;
	CIwArray<Sequence> sequences;	//!< Sequencias de frames usadas por esse sprite.
	uint8 sequenceIndex;				//!< Indice da sequencia corrente. Caso esteja como -1, significa que esse sprite nao possui sequencias.

	int16 accTime;					//!< Tempo acumulado no frame atual.
	const ImagePtr image;			//!< Imagem que contem os frames da animacao.
	uint8 frameIndex;					//!< Indice corrente de acesso ao frame da sequencia (nao equivale ao frame da sequencia em si).
	
	bool paused;					//!< Indica se a animacao do sprite esta pausada no momento.

	void paint();

private:

	bool scaleToSize;				//!< Caso o sprite deva sofrer escala de forma que preencha seu tamanho.

	/** Número máximo de caracteres por linha na leitura de um arquivo de sequência (.seq) */
	static const int SEQUENCE_MAXCHARSLINE = 300;

	// TODO: explicar arquivo .seq melhor
	void readSequence( s3eFile* file );

	inline static int readNumber( char** line ) { 
		int value = strtol( *line, NULL, 10 ); 
		*line = strtok( NULL, " \r\n\t" ); 
		return value;
	}
};
#endif