/** \file listeners.h
	2011-06-03
*/

#ifndef LISTENERS_H
#define LISTENERS_H

#include <s3e.h>

#include <components\drawablegroup.h>

// TODO: por enquanto, esses listeners se encontram nesse arquivo
class PointerListener
{
public:
	virtual void pointerButtonCB( const s3ePointerEvent *ev ) = 0;
	virtual void pointerMotionCB( const s3ePointerMotionEvent *ev ) = 0;
};

class TouchListener
{
public:
	virtual void touchButtonCB( const s3ePointerTouchEvent *ev ) = 0;
	virtual void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) = 0;
};

// TODO: por enquanto, esses listeners se encontram nesse arquivo
class KeyboardListener
{
public:
	virtual void keyboardKeyCB( const s3eKeyboardEvent *ev ) = 0;
	virtual void keyboardCharCB( const s3eKeyboardCharEvent *ev ) = 0;
};

/** \class Screen
	\brief Representa uma tela do jogo, respons�vel por receber eventos de
	pointer e de teclado do GameManager e desenhar seu conte�do na tela.
	@see DrawableGroup
	@see GameManager
*/
class Screen : public DrawableGroup, public KeyboardListener, public PointerListener, public TouchListener
{
public:
	virtual void devicePauseCB() = 0;
	virtual void deviceUnpauseCB() = 0; 
};

#endif