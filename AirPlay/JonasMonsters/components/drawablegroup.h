/** \file
	drawableimage.h
	25/05/2011
*/
#include <components\defines.h>
#include <components\drawable.h>


#ifndef DRAWABLEGROUP_H
#define DRAWABLEGROUP_H

/** \class DrawableGroup
	\brief Classe de uma imagem desenhavel na tela.
*/
class DrawableGroup : public Drawable
{
public:
	DrawableGroup() : Drawable() {}
	~DrawableGroup() { removeAllDrawables(); }

	/** Retorna Drawable referente ao índice desejado. */
	DrawablePtr getDrawable( int index );

	/** Retorna Drawable o qual é incidido pelo ponto p. */
	DrawablePtr getDrawableAt( Point p );

	/** Insere Drawable e retorna seu indice */
	int insertDrawable( DrawablePtr drawable );

	/** Remove o Drawable através de ser DrawablePtr */
	void removeDrawable( const DrawablePtr drawable );

	/** Remove o Drawable alocado no índice desejado. */
	void removeDrawableAt( int index );

	/** Remove todos os drawables desse grupo. */
	void removeAllDrawables();

	void update( unsigned int dt );

protected:
	CIwArray<DrawablePtr> drawables;		//!< Lista dos drawables do grupo.
	void paint();
};
#endif