#include <connection.h>

//const unsigned char Connection::base64_table[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*char* Connection::base64_encode(const char *src, size_t len, size_t *out_len)
{
	// Essa fun��o foi retirada do exemplo do AirPlay chamado IwHttpExample.
	// Consulte-o para mais informa��es.
	char *out, *pos;
	const char *end, *in;
	size_t olen;

//#ifdef BASE64_ENCODE_ADD_NEWLINES
//			int line_len;
//#endif

	olen = len * 4 / 3 + 4; // 3-byte blocks to 4-byte
	olen += olen / 72;		// line feeds
	olen++;					// nul termination
	out = (char *)malloc(olen);
	if (out == NULL)
		return NULL;

	end = src + len;
	in = src;
	pos = out;

//#ifdef BASE64_ENCODE_ADD_NEWLINES
//		line_len = 0;
//#endif

	while (end - in >= 3) {
		*pos++ = base64_table[in[0] >> 2];
		*pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
		*pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
		*pos++ = base64_table[in[2] & 0x3f];
		in += 3;

//#ifdef BASE64_ENCODE_ADD_NEWLINES
//				line_len += 4;
//				if (line_len >= 72) {
//						*pos++ = '\n';
//						line_len = 0;
//				}
//#endif

	}

	if (end - in) {
		*pos++ = base64_table[in[0] >> 2];
		if (end - in == 1) {
				*pos++ = base64_table[(in[0] & 0x03) << 4];
				*pos++ = '=';
		} else {
				*pos++ = base64_table[((in[0] & 0x03) << 4) |
										(in[1] >> 4)];
				*pos++ = base64_table[(in[1] & 0x0f) << 2];
		}
		*pos++ = '=';
//#ifdef BASE64_ENCODE_ADD_NEWLINES
//			   line_len += 4;
//#endif
	}

//#ifdef BASE64_ENCODE_ADD_NEWLINES
//		if (line_len)
//			   *pos++ = '\n';
//#endif

	*pos = '\0';
	if (out_len)
		*out_len = pos - out;
	return out;
}*/

int32 HTTP_DATA_CB( void*, void* httpConnPtr)
{
    // This is the callback indicating that a ReadContent call has
    // completed.  Either we've finished, or a bigger buffer is
    // needed.  If the correct ammount of data was supplied initially,
    // then this will only be called once. However, it may well be
    // called several times when using chunked encoding.
	Connection *connection = (Connection*) httpConnPtr;
	connection->getData();

    // Firstly see if there's an error condition.
    /*if (httpConn->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = HTTP_ERROR;
    }
    else if (httpConn->ContentReceived() != httpConn->ContentLength())
    {
        // We have some data but not all of it. We need more space.
        uint32 oldLen = resultLength;

        // If iwhttp has a guess how big the next bit of data is (this
        // basically means chunked encoding is being used), allocate
        // that much space. Otherwise guess.
        if (resultLength < httpConn->ContentExpected())
            resultLength = httpConn->ContentExpected();
        else
            resultLength += 1024;

        // Allocate some more space and fetch the data.
        result = (char*)s3eRealloc(result, resultLength);
		httpConn->ReadContent( &result[oldLen], resultLength - oldLen,  );
    }
    else
    {
        // We've got all the data.
        status = HTTP_OK;
    }*/
    return 0;
}

int32 HTTP_HEADERS_CB(void*, void* httpConnPtr)
{
	Connection *connection = (Connection*) httpConnPtr;
	connection->getHeaders();
	//CIwHTTP *httpConn = connection->getIwConn();

   /* if (httpConn->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        connection->setStatus( HTTP_ERROR );
    }
    else
    {
        // Depending on how the server is communicating the content
        // length, we may actually know the length of the content, or
        // we may know the length of the first part of it, or we may
        // know nothing. ContentExpected always returns the smallest
        // possible size of the content, so allocate that much space
        // for now if it's non-zero. If it is of zero size, the server
        // has given no indication, so we need to guess. We'll guess at 1k.
        resultLength = httpConn->ContentExpected();
        if (!resultLength)
        {
            resultLength = 1024;
        }

        //char **resultBuffer = secureTest ? &secureResult: &result;

        s3eFree(result);
        result = (char*)s3eMalloc(resultLength + 1);
        (result)[resultLength] = 0;
        httpConn->ReadContent(result, resultLength, dataCB, NULL);
    }*/

    return 0;
}