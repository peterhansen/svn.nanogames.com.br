#include <src/textbox.h>

#include <components/pattern.h>
#include <components/gamemanager.h>

/** Dura��o da transi��o (estados STATE_APPEARING e STATE_HIDING). */
#define TRANSITION_TIME 600.0f

TextBox::TextBox() : DrawableGroup(),
	accTime( 0 ), state( STATE_NONE )
{
	box = DrawablePtr( new Pattern( CreateColor( 0xffdd0070 ) ) );
	insertDrawable( box );

	label = LabelPtr( new Label( FontPtr( new Font( "fonts/Serif.ttf", 20, 100 ) ), "Bla bla bla ble ble bli blo blu blu." ) );
	insertDrawable( label );

	setState( STATE_HIDDEN );
}


void TextBox::setSize( Point size ) {
	DrawableGroup::setSize( size );

	box->setSize( size );
	label->setSize( size );
}


void TextBox::update( uint delta ) {
	switch ( state ) {
		case STATE_APPEARING:
			accTime += delta;
			refresh();

			if ( accTime >= TRANSITION_TIME ) {
				setState( STATE_VISIBLE );
			}
		break;

		case STATE_HIDING:
			accTime += delta;
			refresh();

			if ( accTime >= TRANSITION_TIME ) {
				setState( STATE_HIDDEN );
			}
		break;

		case STATE_VISIBLE:
			DrawableGroup::update( delta );
		break;
	}
}


void TextBox::show() {
	switch ( state ) {
		case STATE_APPEARING:
		case STATE_VISIBLE:
		return;

		default:
			setState( STATE_APPEARING );
	}
}


void TextBox::hide() {
	switch ( state ) {
		case STATE_HIDDEN:
		case STATE_HIDING:
		return;

		default:
			setState( STATE_HIDING );
	}
}


void TextBox::setState( TextBoxState state ) {
	switch ( state ) {
		case STATE_HIDDEN:
			accTime = TRANSITION_TIME;
			setTween( GameManager::getDeviceScreenSize().y, GameManager::getDeviceScreenSize().y );
		break;

		case STATE_APPEARING:
			switch ( this->state ) {
				case STATE_VISIBLE:
				return;
			}

			setTween( getPosY(), GameManager::getDeviceScreenSize().y - getHeight() );
				
			accTime = 0;
		break;

		case STATE_HIDING:
			if ( this->state == STATE_HIDDEN )
				return;

			accTime = 0;
			setTween( getPosY(), GameManager::getDeviceScreenSize().y );
		break;
	}

	this->state = state;
	refresh();
}


void TextBox::setTween( int initialValue, int finalValue ) {
	tween.x = initialValue;
	tween.y = finalValue;
}


void TextBox::refresh() {
	accTime = NanoMath::clamp( accTime, 0, TRANSITION_TIME );
	setPosition( getPosX(), tween.x + ( ( tween.y - tween.x ) * ( accTime / TRANSITION_TIME ) ) );
}