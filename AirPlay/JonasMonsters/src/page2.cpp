#include <src/page2.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "2/" )

enum {
	OBJ_KITCHEN,
	OBJ_TOASTER,
};


Page2::Page2() : Page() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "cozinha.png" ) );
	insertDrawable( bkg );
	
	SpritePtr toaster = SpritePtr( new Sprite( PATH + "torradeira" ) );
	toaster->setPosition( 500, 630 );
	insertDrawable( toaster );

	createTextBox();
	
	// TODO setState( STATE_SHOW_TEXT );
}

void Page2::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page2::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page2::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed )
		PageManager::getInstance()->nextPage();
}


void Page2::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
