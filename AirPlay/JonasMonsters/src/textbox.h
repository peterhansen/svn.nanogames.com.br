/** \file pageobject.h
	2011-10-15
*/

#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>

#define TextBoxPtr	boost::shared_ptr<TextBox>

typedef enum {
	STATE_NONE,
	STATE_HIDDEN,
	STATE_APPEARING,
	STATE_VISIBLE,
	STATE_HIDING,
} TextBoxState;

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class TextBox : public DrawableGroup {

public:
	TextBox();

	virtual void touchButtonCB( const s3ePointerTouchEvent *ev ) {}
	virtual void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	void pointerButtonCB( const s3ePointerEvent *ev ) {}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	void setSize( Point size );

	void setState( TextBoxState state );

	void show();

	void hide();

	void update( uint delta );

protected:

	DrawablePtr box;
	LabelPtr label;

	TextBoxState state;

	uint32 accTime;

	Point tween;

private:

	void refresh();

	void setTween( int initialValue, int finalValue );

};
#endif