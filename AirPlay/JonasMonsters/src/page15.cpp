#include <src/page15.h>

#define PATH ( Constants::Path::IMAGES + "15/" )

enum {
	STATE_SHOW_TEXT,
	STATE_JONAS_SLEEPING,
};

enum {
	OBJ_ROOM,
	OBJ_COOL,
	OBJ_FIRE,
};


Page15::Page15() : Page() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "bkg.png" ) );
	insertDrawable( bkg );
	
	SpritePtr cool = SpritePtr( new Sprite( PATH + "cool" ) );
	cool->setPosition( 314, 70 );
	insertDrawable( cool );

	SpritePtr fire = SpritePtr( new Sprite( PATH + "fire" ) );
	fire->setPosition( 900, 500 );
	insertDrawable( fire );

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page15::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:

		break;
		
		case STATE_JONAS_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}



void Page15::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page15::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page15::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 0 ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();
			break;

			case OBJ_COOL:
			{
				Sprite *s = ( Sprite* ) getDrawable( index ).get();
				s->setSequenceIndex( s->getSequenceIndex() == 0 ? 1 : 0 );
				( ( TextBox* ) textBox.get() )->hide();
			}
			break;
		}
	}
	printf( "P (%d, %d) ->%d\n", getSize().x, getSize().y, getObjectIndexAt( Point( ev->m_x, ev->m_y ) ) );
}


void Page15::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
