#ifndef PAGE_4_H
#define PAGE_4_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <components/tween.h>

class Page4 : public Page {

public:
	Page4();

	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void update( uint dt );


protected:

	CIwArray<SpritePtr> socks;

	SpritePtr monster;

	TweenPtr monsterTween;

	int8 draggedIndex;

private:

	void resetPosition( uint8 index );

};
#endif