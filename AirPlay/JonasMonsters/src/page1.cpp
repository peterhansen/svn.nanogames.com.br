#include <src/page1.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "1/" )

enum {
	STATE_SHOW_TEXT,
	STATE_JONAS_SLEEPING,
};

enum {
	OBJ_ROOM,
	OBJ_GOO,
	OBJ_GAME,
	OBJ_BALL,
	OBJ_ABAJOUR,
	OBJ_NINJA,
	OBJ_ROBOT,
	OBJ_JONAS_SLEEP,
	OBJ_JONAS_WALK,
};

Page1::Page1() : Page() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "room.png" ) );
	insertDrawable( bkg );
	
	SpritePtr goo = SpritePtr( new Sprite( PATH + "geleca" ) );
	goo->setPosition( 314, 70 );
	insertDrawable( goo );

	SpritePtr game = SpritePtr( new Sprite( PATH + "game" ) );
	game->setPosition( 900, 500 );
	insertDrawable( game );

	SpritePtr ball = SpritePtr( new Sprite( PATH + "bola" ) );
	ball->setPosition( 18, 404 );
	insertDrawable( ball );
	
	SpritePtr abajour = SpritePtr( new Sprite( PATH + "abajour" ) );
	abajour->setPosition( 253, 262 );
	insertDrawable( abajour );

	ninja = SpritePtr( new Sprite( PATH + "ninja" ) );
	ninja->setPosition( 627, 20 );
	insertDrawable( ninja );
	
	SpritePtr robot = SpritePtr( new Sprite( PATH + "robo" ) );
	robot->setPosition( 348, 468 );
	insertDrawable( robot );
	
	jonasSleeping= DrawableImagePtr( new DrawableImage( PATH + "jonas_sleep.png" ) );
	jonasSleeping->setPosition( 618, 295 );
	insertDrawable( jonasSleeping );

	jonasWalking = DrawableImagePtr( new DrawableImage( PATH + "jonas_walking.png" ) );
	jonasWalking->setPosition( 900, 500 );
	jonasWalking->setVisible( false );
	insertDrawable( jonasWalking );

	createTextBox();
	
	setState( STATE_SHOW_TEXT );
}


void Page1::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:

		break;
		
		case STATE_JONAS_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}

void Page1::touchButtonCB( const s3ePointerTouchEvent *ev ) {
}


void Page1::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page1::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 0 ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();
			break;

			case OBJ_GAME:
			case OBJ_BALL:
			case OBJ_ABAJOUR:
			case OBJ_NINJA:
			case OBJ_ROBOT:
			case OBJ_GOO:
			{
				Sprite *s = ( Sprite* ) getDrawable( index ).get();
				s->setSequenceIndex( s->getSequenceIndex() == 0 ? 1 : 0 );
				( ( TextBox* ) textBox.get() )->hide();
			}
			break;

			case OBJ_JONAS_SLEEP:
				PageManager::getInstance()->nextPage();
			break;

			case OBJ_JONAS_WALK:
			break;
		}
	}
	printf( "P (%d, %d) ->%d\n", getSize().x, getSize().y, getObjectIndexAt( Point( ev->m_x, ev->m_y ) ) );
}


void Page1::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
