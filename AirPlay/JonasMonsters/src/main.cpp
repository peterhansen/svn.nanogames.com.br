
// include principal, que importa funcionalidade da API s3eFile
// e da API de imagens e fontes
#include <s3eFile.h>
#include <IwImage.h>
#include <IwGxFont.h>
#include <IwResManager.h>
#include <IwUI.h>

#include <components\defines.h>
#include <components\gamemanager.h>
#include <src/testscreen.h>
#include <src/pagemanager.h>

/**
* Chamada principal da aplicação.
*/
S3E_MAIN_DECL void IwMain() 
{
	// inicialização dos módulos necessários
	Iw2DInit();

	// TODO: caso IwGXFont seja usado...
	IwGxFontInit();

	// TODO: caso IwResourceManager seja usado...
	IwResManagerInit();

	// TODO: if IwUI is used...
	IwUIInit();
	
	{
		CIwUIView view;

		// inicializa Game Manager e carrega alguns recursos
		if( !GameManager::init() )
		{
			// TODO: avisar de erro na inicialização
			return;
		}

		// HACK
		Color bgColor =  CreateColor( 0, 0, 0, 0xff );
		//GameManager::setBackgroundColor( bgColor );
		Iw2DSurfaceClear( bgColor );
		Iw2DSurfaceShow();

		PageManagerPtr screen = PageManager::getInstance();
		screen->setPageIndex( 8 );
		GameManager::setCurrentScreen( screen );

		// principal loop da aplicação
		while( GameManager::update() )
			GameManager::draw();
	 //
		// executa funcoes necessarias do término da aplicacao
		GameManager::terminate();

		// if IwResourceManager for usado
		IwResManagerTerminate();

		PageManager::unload();
	}

	IwUITerminate();
	IwGxFontTerminate();
	Iw2DTerminate();
}