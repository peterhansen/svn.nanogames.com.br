
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <string>

namespace Constants
{
	namespace Path
	{
		const std::string IMAGES = "images/";
	};
};

#endif