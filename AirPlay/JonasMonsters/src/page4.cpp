#include <src/page4.h>
#include <src/pagemanager.h>
#include <components/tween.h>

#define PATH_1 ( Constants::Path::IMAGES + "1/" )
#define PATH ( Constants::Path::IMAGES + "4/" )

#define MONSTER_SPEED_APPEAR 300
#define MONSTER_SPEED_RUN 1000

#define MONSTER_INITIAL_X -100
#define MONSTER_FINAL_X 550

enum {
	STATE_SHOW_TEXT,
	STATE_COLLECT_SOCK_1,
	STATE_COLLECT_SOCK_2,
	STATE_COLLECT_SOCK_3,
	STATE_COLLECT_SOCK_4,
	STATE_COLLECT_SOCK_5,
	STATE_MONSTER_APPEARS,
	STATE_MONSTER_RUNS,
	STATE_MONSTER_COLLECTED,
};

enum {
	OBJ_ROOM,
	OBJ_GOO,
	OBJ_GAME,
	OBJ_BALL,
	OBJ_ABAJOUR,
	OBJ_NINJA,
	OBJ_ROBOT,
	OBJ_SOCK_1,
	OBJ_SOCK_2,
	OBJ_SOCK_3,
	OBJ_SOCK_4,
	OBJ_SOCK_5,
	OBJ_POT,
	OBJ_MONSTER,
	OBJ_JONAS_SLEEP,
	OBJ_JONAS_WALK,
};

#define TOTAL_SOCKS 5

#define SOCKS_POS { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 472, 389, 93, 391, 678, 9, 402, 389, 550, 524, 0, 0, 0, 0 }

#define POT_POSITION Point( 550, 440 )

#define MONSTER_POS_1 Point( -100, 440 )


Page4::Page4() : Page(),
	draggedIndex( -1 )
{
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH_1 + "room.png" ) );
	insertDrawable( bkg );
	
	SpritePtr goo = SpritePtr( new Sprite( PATH_1 + "geleca" ) );
	goo->setPosition( 314, 70 );
	insertDrawable( goo );

	SpritePtr game = SpritePtr( new Sprite( PATH_1 + "game" ) );
	game->setPosition( 900, 500 );
	insertDrawable( game );

	SpritePtr ball = SpritePtr( new Sprite( PATH_1 + "bola" ) );
	ball->setPosition( 18, 404 );
	insertDrawable( ball );
	
	SpritePtr abajour = SpritePtr( new Sprite( PATH_1 + "abajour" ) );
	abajour->setPosition( 253, 262 );
	insertDrawable( abajour );

	SpritePtr ninja = SpritePtr( new Sprite( PATH_1 + "ninja" ) );
	ninja->setPosition( 627, 20 );
	insertDrawable( ninja );
	
	SpritePtr robot = SpritePtr( new Sprite( PATH_1 + "robo" ) );
	robot->setPosition( 348, 468 );
	insertDrawable( robot );

	int bla[] = SOCKS_POS;

	for ( uint8 i = 0; i < TOTAL_SOCKS; ++i ) {
		SpritePtr socks = SpritePtr( new Sprite( PATH + "socks" ) );
		socks->setFrameIndex( i );
		insertDrawable( socks );
		resetPosition( OBJ_SOCK_1 + i );
	}

	DrawableImagePtr pot = DrawableImagePtr( new DrawableImage( PATH + "pot.png" ) );
	pot->setPosition( POT_POSITION );
	insertDrawable( pot );

	monster = SpritePtr( new Sprite( PATH + "monster" ) );
	monster->setPosition( MONSTER_POS_1 );
	insertDrawable( monster );

	createTextBox();
	
	// TODO setState( STATE_SHOW_TEXT );
	setState( STATE_COLLECT_SOCK_1 );
}

void Page4::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page4::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page4::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 1 ) {
		// pressionou a tela
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();
			break;

			case OBJ_GAME:
			case OBJ_BALL:
			case OBJ_ABAJOUR:
			case OBJ_NINJA:
			case OBJ_ROBOT:
			case OBJ_GOO:
			{
				Sprite *s = ( Sprite* ) getDrawable( index ).get();
				s->setSequenceIndex( s->getSequenceIndex() == 0 ? 1 : 0 );
				( ( TextBox* ) textBox.get() )->hide();
			}
			break;

			case OBJ_SOCK_1:
			case OBJ_SOCK_2:
			case OBJ_SOCK_3:
			case OBJ_SOCK_4:
			case OBJ_SOCK_5:
				switch ( getState() ) {
					case STATE_COLLECT_SOCK_1:
					case STATE_COLLECT_SOCK_2:
					case STATE_COLLECT_SOCK_3:
					case STATE_COLLECT_SOCK_4:
					case STATE_COLLECT_SOCK_5:
						draggedIndex = index;
						getDrawable( draggedIndex )->setColor( 0xffffff55 );
					break;
				}
			break;

			case OBJ_MONSTER:
				if ( getState() == STATE_MONSTER_APPEARS )
					setState( STATE_MONSTER_RUNS );
			break;

			case OBJ_JONAS_SLEEP:
				PageManager::getInstance()->nextPage();
			break;

			case OBJ_JONAS_WALK:
			break;
		}
	} else {
		// soltou o toque
		if ( draggedIndex >= 0 ) {
			int releasedIndex = getObjectIndexAt( ev->m_x, ev->m_y );
			if ( releasedIndex == OBJ_POT ) {
				getDrawable( draggedIndex )->setVisible( false );
				setState( getState() + 1 );
			} else {
				getDrawable( draggedIndex )->setColor( 0xffffffff );
				resetPosition( draggedIndex );
			}
			draggedIndex = -1;
		}
	}
}

void Page4::setState( uint8 state ) {
	switch ( state ) {
		case STATE_MONSTER_APPEARS:
			monster->setTransformation( IW_2D_IMAGE_TRANSFORM_NONE );
			monsterTween = TweenPtr( new Tween( ( MONSTER_FINAL_X - monster->getPosX() ) * 1000 / MONSTER_SPEED_APPEAR, MONSTER_INITIAL_X, MONSTER_FINAL_X ) );
		break;
		
		case STATE_MONSTER_RUNS:
			monster->setTransformation( IW_2D_IMAGE_TRANSFORM_FLIP_X );
			monsterTween = TweenPtr( new Tween( NanoMath::abs( MONSTER_INITIAL_X - monster->getPosX() ) * 1000 / MONSTER_SPEED_RUN, monster->getPosX(), MONSTER_INITIAL_X ) );
		break;

		case STATE_MONSTER_COLLECTED:
		break;
	}

	Page::setState( state );
}


void Page4::update( uint dt ) {
	Page::update( dt );

	switch ( state ) {
		case STATE_MONSTER_APPEARS:
			monster->setPosition( monsterTween->update( dt ), monster->getPosY() );
			if ( monsterTween->getPercent() >= 100 ) {
			//	setState( STATE_MONSTER_RUNS );
			}
		break;
		
		case STATE_MONSTER_RUNS:
			monster->setPosition( monsterTween->update( dt ), monster->getPosY() );
			if ( monsterTween->getPercent() >= 100 ) {
				setState( STATE_MONSTER_APPEARS );
			}
		break;
	}
}


void Page4::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	if ( draggedIndex >= 0 ) {
		getDrawable( draggedIndex )->setPosition( ev->m_x, ev->m_y );
	}
}


void Page4::resetPosition( uint8 index ) {
	int bla[] = SOCKS_POS;
	getDrawable( index )->setPosition( bla[ index << 1 ], bla[ ( index << 1 ) + 1 ] );
}
