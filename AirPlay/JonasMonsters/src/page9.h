#ifndef PAGE_9_H
#define PAGE_9_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page9 : public Page {

public:
	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );


protected:

private:

};
#endif