#ifndef PAGE_8_H
#define PAGE_8_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page8 : public Page {

public:
	Page8();

	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void setState( uint8 state );

	void update( uint dt );

protected:

	CIwArray<DrawableImagePtr> questions;

	CIwArray<int16> originalSizes;

	uint16 accTime;

	DrawablePtr dragged;

private:

};
#endif