#include <src/page3.h>
#include <src/pagemanager.h>

#define PATH ( Constants::Path::IMAGES + "3/" )


Page3::Page3() : Page() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "jonas.png" ) );
	insertDrawable( bkg );
	
	monster1 = DrawableImagePtr( new DrawableImage( PATH + "monstrinho.png" ) );
	monster1->setPosition( 500, 630 );
	insertDrawable( monster1 );

	monster2 = DrawableImagePtr( new DrawableImage( PATH + "monstrinho2.png" ) );
	monster2->setPosition( 200, 630 );
	insertDrawable( monster2 );

	createTextBox();
	
	// TODO setState( STATE_SHOW_TEXT );
}


void Page3::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page3::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page3::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed )
		PageManager::getInstance()->nextPage();
}


void Page3::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
