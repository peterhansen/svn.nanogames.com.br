#include <src/page.h>

#include <components/gamemanager.h>

Page::Page() : DrawableGroup() {
}

void Page::createTextBox() {
	textBox = TextBoxPtr( new TextBox() );
	insertDrawable( textBox );
}

DrawablePtr Page::getObjectAt( Point p ) {
	for ( int8 i = drawables.size() - 1; i >= 0; --i ) {
		if ( drawables[ i ]->contains( p ) )
			return drawables[ i ];
	}

	return DrawablePtr();
}


int8 Page::getObjectIndexAt( Point p ) {
	for ( int8 i = drawables.size() - 1; i >= 0; --i ) {
		if ( drawables[ i ]->contains( p ) )
			return i;
	}

	return -1;
}


void Page::pointerButtonCB( const s3ePointerEvent *ev ) {
	DrawablePtr obj = getObjectAt( Point( ev->m_x, ev->m_y ) );
	printf( "clicked on %d", obj );
}


void Page::setSize( Point size ) {
	DrawableGroup::setSize( size );
	
	if ( textBox ) {
		textBox->setSize( size.x, 100 );
		textBox->setPosition( 0, size.y - textBox->getHeight() );
	}
}

