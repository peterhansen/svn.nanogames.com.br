#include <src/page7.h>

#define PATH ( Constants::Path::IMAGES + "7/" )

enum {
	STATE_SHOW_TEXT,
	STATE_JONAS_SLEEPING,
};

enum {
	OBJ_ROOM,
	OBJ_GLOBE,
	OBJ_CUCKOO,
};


Page7::Page7() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "sala.png" ) );
	insertDrawable( bkg );
	
	SpritePtr globe = SpritePtr( new Sprite( PATH + "mundo" ) );
	globe->setPosition( 314, 70 );
	insertDrawable( globe );

	SpritePtr cuckoo = SpritePtr( new Sprite( PATH + "cuco" ) );
	cuckoo->setPosition( 900, 500 );
	insertDrawable( cuckoo );

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page7::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:

		break;
		
		case STATE_JONAS_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}



void Page7::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page7::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page7::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 0 ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();
			break;

			case OBJ_CUCKOO:
			{
				Sprite *s = ( Sprite* ) getDrawable( index ).get();
				s->setSequenceIndex( s->getSequenceIndex() == 0 ? 1 : 0 );
				( ( TextBox* ) textBox.get() )->hide();
			}
			break;
		}
	}
	printf( "P (%d, %d) ->%d\n", getSize().x, getSize().y, getObjectIndexAt( Point( ev->m_x, ev->m_y ) ) );
}


void Page7::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
