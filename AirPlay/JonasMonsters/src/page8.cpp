#include <src/page8.h>

#define PATH ( Constants::Path::IMAGES + "8/" )

#define QUESTIONS_TOTAL 3

#define TIME_GROW_FACTOR 0.5f

enum {
	STATE_SHOW_TEXT,
	STATE_JONAS_SLEEPING,
};

enum {
	OBJ_ROOM,
	OBJ_QUESTION_1,
	OBJ_QUESTION_2,
	OBJ_QUESTION_3,
};

#define QUESTIONS_POS { 200, 50, 400, 50, 600, 50 }


Page8::Page8() : Page(),
	dragged( DrawablePtr() )
{
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "jonas.png" ) );
	insertDrawable( bkg );

	int bla[] = QUESTIONS_POS;
	for ( uint8 i = 0; i < QUESTIONS_TOTAL; ++i ) {
		DrawableImagePtr question = DrawableImagePtr( new DrawableImage( PATH + "question_" + ( char ) ( '0' + i ) + ".png" ) );
		question->setPosition( bla[ i << 1 ] , bla[ ( i << 1 ) + 1 ] );
		question->setAnchor( ANCHOR_CENTER );
		insertDrawable( question );

		questions.push_back( question );
		originalSizes.push_back( question->getWidth() );
		originalSizes.push_back( question->getHeight() );
	}

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page8::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:

		break;
		
		case STATE_JONAS_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}


void Page8::update( uint dt ) {
	Page::update( dt );

	accTime += dt;

	float s = 0.2f + ABS( sin( accTime * TIME_GROW_FACTOR ) );

	for ( uint8 i = 0; i < questions.size(); ++i ) {
		questions[ i ]->setSize( ( int16 ) ( originalSizes[ i << 1 ] * s ), ( int16 ) ( originalSizes[ ( i << 1 ) + 1 ] * s ) );
	}
}



void Page8::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page8::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page8::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 1 ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_QUESTION_1:
			case OBJ_QUESTION_2:
			case OBJ_QUESTION_3:
			{
				dragged = getDrawable( index );
			}
			break;
			
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();

			default:
				dragged = DrawablePtr();
		}
	} else {
		dragged = DrawablePtr();
	}
	printf( "P (%d, %d) ->%d\n", getSize().x, getSize().y, getObjectIndexAt( Point( ev->m_x, ev->m_y ) ) );
}


void Page8::pointerMotionCB( const s3ePointerMotionEvent *ev ) {
	if ( dragged.get() ) {
		dragged->setAnchorPosition( ev->m_x, ev->m_y );
	}
}
