#include <src/page12.h>

#define PATH ( Constants::Path::IMAGES + "12/" )

enum {
	STATE_SHOW_TEXT,
	STATE_JONAS_SLEEPING,
};

enum {
	OBJ_ROOM,
	OBJ_COOL,
	OBJ_FIRE,
};


Page12::Page12() : Page() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "prato.png" ) );
	insertDrawable( bkg );
	
	DrawableImagePtr brocoli = DrawableImagePtr( new DrawableImage( PATH + "brocoli.png" ) );
	brocoli->setPosition( 314, 70 );
	insertDrawable( brocoli );
	
	DrawableImagePtr thing = DrawableImagePtr( new DrawableImage( PATH + "gosma.png" ) );
	thing->setPosition( 900, 500 );
	insertDrawable( thing );

	DrawableImagePtr hand = DrawableImagePtr( new DrawableImage( PATH + "mao.png" ) );
	hand->setPosition( 900, 500 );
	insertDrawable( hand );

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page12::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:

		break;
		
		case STATE_JONAS_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}



void Page12::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page12::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page12::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 0 ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();
			break;

			case OBJ_COOL:
			{
				Sprite *s = ( Sprite* ) getDrawable( index ).get();
				s->setSequenceIndex( s->getSequenceIndex() == 0 ? 1 : 0 );
				( ( TextBox* ) textBox.get() )->hide();
			}
			break;
		}
	}
	printf( "P (%d, %d) ->%d\n", getSize().x, getSize().y, getObjectIndexAt( Point( ev->m_x, ev->m_y ) ) );
}


void Page12::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
