#ifndef PAGE_6_H
#define PAGE_6_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>
#include <src/Box2D.h>

class Page6 : public Page {

public:
	Page6();
	~Page6();

	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev );

	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev );

	void update( uint dt );

	void draw();

protected:

	b2World* m_world;
	b2Body* body;

	SpritePtr monster;

private:

};
#endif