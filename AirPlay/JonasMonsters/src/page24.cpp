#include <src/page24.h>

#define PATH ( Constants::Path::IMAGES + "24/" )

#define MONSTERS_TOTAL 4

enum {
	STATE_SHOW_TEXT,
	STATE_JONAS_SLEEPING,
};

enum {
	OBJ_ROOM,
	OBJ_MONSTER_1,
	OBJ_MONSTER_2,
	OBJ_MONSTER_3,
	OBJ_MONSTER_4,
};

#define MONSTERS_POS { 0, 500, 200, 500, 400, 500, 600, 500 }


Page24::Page24() : Page() {
	DrawableImagePtr bkg = DrawableImagePtr( new DrawableImage( PATH + "bed.png" ) );
	insertDrawable( bkg );

	int bla[] = MONSTERS_POS;
	for ( uint8 i = 0; i < MONSTERS_TOTAL; ++i ) {
		SpritePtr monster = SpritePtr( new Sprite( PATH + "monster_" + ( char ) ( '0' + i ) ) );
		monster->setPosition( bla[ i << 1 ] , bla[ ( i << 1 ) + 1 ] );
		insertDrawable( monster );
	}

	createTextBox();
	setState( STATE_SHOW_TEXT );
}


void Page24::setState( uint8 state ) {
	switch ( state ) {
		case STATE_SHOW_TEXT:

		break;
		
		case STATE_JONAS_SLEEPING:

		break;
	}

	// atribui somente no final
	Page::setState( state );
}



void Page24::touchButtonCB( const s3ePointerTouchEvent *ev ) {

}


void Page24::touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {

}


void Page24::pointerButtonCB( const s3ePointerEvent *ev ) {
	if ( ev->m_Pressed == 0 ) {
		int index = getObjectIndexAt( ev->m_x, ev->m_y );
		switch ( index ) {
			case OBJ_ROOM:
				( ( TextBox* ) textBox.get() )->show();
			break;

			case OBJ_MONSTER_1:
			case OBJ_MONSTER_2:
			case OBJ_MONSTER_3:
			case OBJ_MONSTER_4:
			{
				Sprite *s = ( Sprite* ) getDrawable( index ).get();
				s->setSequenceIndex( s->getSequenceIndex() == 0 ? 1 : 0 );
				( ( TextBox* ) textBox.get() )->hide();
			}
			break;
		}
	}
	printf( "P (%d, %d) ->%d\n", getSize().x, getSize().y, getObjectIndexAt( Point( ev->m_x, ev->m_y ) ) );
}


void Page24::pointerMotionCB( const s3ePointerMotionEvent *ev ) {

}
