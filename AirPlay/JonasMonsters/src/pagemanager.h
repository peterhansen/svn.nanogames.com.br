/** \file pageobject.h
	2011-10-15
*/

#ifndef PAGE_MANAGER_H
#define PAGE_MANAGER_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/constants.h>
#include <src/page.h>

class Page;

#define PageManagerPtr	boost::shared_ptr<PageManager>

/** \class GameScreen
	\brief Tela principal do jogo.
*/
class PageManager : public Screen {

public:

	void touchButtonCB( const s3ePointerTouchEvent *ev ) {  }
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	void pointerButtonCB( const s3ePointerEvent *ev ) { if ( currentPage ) currentPage->pointerButtonCB( ev ); }
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) { if ( currentPage ) currentPage->pointerMotionCB( ev ); }

	void devicePauseCB() {}
	void deviceUnpauseCB() {}

	void keyboardKeyCB( const s3eKeyboardEvent *ev ) {}
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}

	void setPageIndex( uint8 index );

	inline uint8 getPageIndex() { return currentPageIndex; }

	inline void nextPage() { setPageIndex( getPageIndex() + 1 ); }

	inline void previousPage() { setPageIndex( getPageIndex() - 1 ); }

	static PageManagerPtr getInstance();

	static void unload();


protected:

	PagePtr currentPage;

	uint8 currentPageIndex;


private:

	PageManager();	

	static PageManagerPtr instance;

};

#endif