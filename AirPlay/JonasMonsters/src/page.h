/** \file pageobject.h
	2011-10-15
*/

#ifndef PAGE_H
#define PAGE_H

#include <components/defines.h>
#include <IwUI.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <src/textbox.h>
#include <src/constants.h>
#include <src/pageobject.h>
#include <components/sprite.h>


class PageManager;

#define PagePtr	boost::shared_ptr<Page>

/** \class GameScreen
	\brief Tela principal do jogo./
*/
class Page : public DrawableGroup {

public:
	Page();

	virtual void touchButtonCB( const s3ePointerTouchEvent *ev ) {}
	virtual void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	virtual void pointerButtonCB( const s3ePointerEvent *ev );
	virtual void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	PageObjectPtr getObjectAt( Point p );
	
	int8 getObjectIndexAt( Point p );

	inline int8 getObjectIndexAt( int16 x, int16 y ) { return getObjectIndexAt( Point( x, y ) ); }

	virtual void setState( uint8 state ) { 
		this->state = state; 
		IwTrace( MYAPP, ( "Page::setState( %d )", state ) );
	}

	inline uint8 getState() { return state; }

	void setSize( Point size );

protected:

	void createTextBox();

	DrawablePtr textBox;

	uint8 state;

private:

};
#endif