/** \file gamescreen.h
	2011-06-14
*/

#include <IwUI.h>

#include <components/defines.h>
#include <components/listeners.h>
#include <components/drawableimage.h>
#include <components/label.h>
#include <components/gamemanager.h>

#include <components/nanoonline/customer.h>

#ifndef GAMESCREEN_H
#define GAMESCREEN_H

class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1( this, "CController", CController, OnClickLaunchKeyboard, CIwUIElement* )
        IW_UI_CREATE_VIEW_SLOT1( this, "CController", CController, OnClickLaunchNumpad, CIwUIElement* )

        IW_UI_CREATE_VIEW_SLOT1( this, "CController", CController, OnClickShowQuitScreen, CIwUIElement* )
        IW_UI_CREATE_VIEW_SLOT1( this, "CController", CController, OnClickHideQuitScreen, CIwUIElement* )
        IW_UI_CREATE_VIEW_SLOT1( this, "CController", CController, OnClickQuit, CIwUIElement* )

        IW_UI_CREATE_VIEW_SLOT2( this, "CController", CController, OnRadioTextEntryMode, CIwUIElement* , int16 )
    }

private:
    void OnClickLaunchKeyboard( CIwUIElement* )
    {
        IwGetUITextInput()->SetEditorMode( CIwUITextInput::eSoftKeyboard );
        IwGetUITextInput()->DoTextEntry( NULL,"" );
    }

    void OnClickLaunchNumpad( CIwUIElement* )
    {
        IwGetUITextInput()->SetEditorMode( CIwUITextInput::eSoftNumpad );
        IwGetUITextInput()->DoTextEntry( NULL,"" );
    }

    void OnClickShowQuitScreen( CIwUIElement* )
    {
		GameManager::endGame();
    }

    void OnRadioTextEntryMode(CIwUIElement*, int16 mode)
    {
        IwGetUITextInput()->SetEditorMode((CIwUITextInput::EditorMode)mode);
    }

    void OnClickHideQuitScreen(CIwUIElement*)
    {
        IwGetUIView()->SetModal(NULL);
		GameManager::endGame();
    }

    void OnClickQuit(CIwUIElement*)
    {
        GameManager::endGame();
    }
};

// forward declaration
class GUIScreen;

/** \class Button event handler
*/
class ButtonEventHandler : public CIwUIElementEventHandler
{
public:
	GUIScreen *guiScreen;

	void setScreen( GUIScreen *guiScreen );
	virtual bool HandleEvent( CIwEvent *pEvent );
	virtual bool FilterEvent( CIwEvent *pEvent );
};

/** \class GUIScreen
	\brief Tela que utiliza os componentes de interface do m�dulo IwUI
*/
class GUIScreen : public Screen, public NOListener
{
public:

	NOCustomer customer;
	ButtonEventHandler buttonEvHandler;
	int state; // 0 - NOT LOGGED, 1 - LOGGED IN, 2 - PROFILE DOWNLOADED, -1 ERROR LOGGING, -2 ERROR DOWNLOADING PROFILE

	GUIScreen();
	~GUIScreen();

	virtual void update( unsigned int dt )
	{
		IwGetUIController()->Update();
		IwGetUIView()->Update( (int32) dt );
	}

	virtual void draw()
	{
		IwGetUIView()->Render();
	}

	void keyboardKeyCB( const s3eKeyboardEvent* ev ) {}
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}

	void touchButtonCB( const s3ePointerTouchEvent *ev ) 
	{
		//GameManager::endGame();
	}

	void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	void pointerButtonCB( const s3ePointerEvent *ev ) 
	{
		//GameManager::endGame();
	}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	virtual void devicePauseCB() {}
	virtual void deviceUnpauseCB() {}

	
	void onNORequestSent()
	{
		CIwUILabel *label = (CIwUILabel*) IwGetUIView()->GetChildNamed( "Label" );
		switch( state )
		{
		case 0:
			
			label->SetCaption(" logging in... ");
			break;

		case 1:
			label->SetCaption(" downloading profile... " );
			break;
		}
	}
	
	// Indica que uma requisi��o foi respondida e terminada com sucesso
	void onNOSuccessfulResponse()
	{
		CIwUILabel *label = (CIwUILabel*) IwGetUIView()->GetChildNamed( "Label" );

		switch( state )
		{
		case 0:
			state = 1;
			label->SetCaption(" logged in! ");
			break;

		case 1:
			NOString firstName, lastName;
			customer.getFirstName( firstName );
			customer.getLastName( lastName );
			firstName.append( L" " + lastName );

			char name[100];
			#ifdef NANO_ONLINE_UNICODE_SUPPORT
				wcstombs( name, firstName.data(), firstName.size() );
				name[ firstName.size() ] = 0;
			#else
				name = firstName.data();
				name[ firstName.size() ] = 0;
			#endif
			
			//LOG( "first name %ls", firstName );
			label->SetCaption( name );
			break;
		}
	}
	
	// Sinaliza erros ocorridos nas opera��es do NanoOnline
	void onNOError( NOString errorStr )
	{
		CIwUILabel *label = (CIwUILabel*) IwGetUIView()->GetChildNamed( "Label" );
		std::string errorMsg = convertToMBString( L"erro: " + errorStr );
		label->SetCaption( errorMsg.data() );
	}
};


/** \class GameScreen
	\brief Tela principal do jogo.
*/
class GameScreen : public Screen, public NOListener
{
public:

	NOCustomer customer;
	FontPtr font;
	LabelPtr label;

	GameScreen() 
	{
		font = FontPtr( new Font( "fonts/GenBasB.ttf", 12, 100 ) );
		label = LabelPtr( new Label( font, "ola!") );
		insertDrawable( label );

		customer = NOCustomer();
		customer.setPassword( L"senha123" );
		customer.setNickname( L"usuario" );

		NOCustomer::SendDownloadRequest( &customer, this );
	}

	virtual void onNORequestSent( void )
	{
		label->setText( "requisicao enviada" );
	}
	
	virtual void onNOSuccessfulResponse()
	{
		NOString firstName, lastName;
		customer.getFirstName( firstName );
		customer.getLastName( lastName );

		std::string fullName = convertToMBString( firstName + L" " + lastName );

		label->setText( "Nome do cliente: " + fullName );
	}
	
	// Sinaliza erros ocorridos nas opera��es do NanoOnline
	virtual void onNOError( NOString errorStr )
	{
		std::string errorMBStr = convertToMBString( L"erro: " + errorStr );
		label->setText( errorMBStr );
	}
	

	void setSize( Point size )
	{
		DrawableGroup::setSize( size );
		label->setPosition( Point( 10, 10 ) );
		label->setSize( size );
		//nanoLogo->setPosition( Point( size.x / 2, size.y / 2 ) );
	}

	void keyboardKeyCB( const s3eKeyboardEvent* ev )
	{
		GameManager::setCurrentScreen( ScreenPtr( new GUIScreen() ) );
	}

	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}

	void touchButtonCB( const s3ePointerTouchEvent *ev ) {}
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}

	void pointerButtonCB( const s3ePointerEvent *ev ) {}
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}

	virtual void devicePauseCB() {}
	virtual void deviceUnpauseCB() {}

protected:
	//DrawableImagePtr nanoLogo;
};
#endif