
// include principal, que importa funcionalidade da API s3eFile
// e da API de imagens e fontes
#include <s3eFile.h>
#include <IwImage.h>
#include <IwGxFont.h>
#include <IwResManager.h>
#include <IwUI.h>

#include <components\defines.h>
#include <components\gamemanager.h>
#include <src\gamescreen.h>

/**
* Chamada principal da aplicação.
*/
S3E_MAIN_DECL void IwMain() 
{
	// inicialização dos módulos necessários
	Iw2DInit();

	// TODO: caso IwGXFont seja usado...
	IwGxFontInit();

	// TODO: caso IwResourceManager seja usado...
	IwResManagerInit();

	// TODO: if IwUI is used...
	IwUIInit();
	
	{
		CIwUIView view;
		CController controller;
		CIwUITextInput textinput;


		// inicializa Game Manager e carrega alguns recursos
		if( !GameManager::init() )
		{
			// TODO: avisar de erro na inicialização
			return;
		}

		// HACK
		Color bgColor =  NanoColor::Create( 0, 255, 0, 255 );
		//GameManager::setBackgroundColor( bgColor );
		Iw2DSurfaceClear( bgColor );
		Iw2DSurfaceShow();

		GUIScreen *guiScreen = new GUIScreen();
		GameManager::setCurrentScreen( ScreenPtr( guiScreen ) );
		//GameScreen *gameScreen = new GameScreen();
		//GameManager::setCurrentScreen( ScreenPtr( gameScreen ) );

		bgColor =  NanoColor::Create( 0, 0, 255, 255 );
		//GameManager::setBackgroundColor( bgColor );
		Iw2DSurfaceClear( bgColor );
		Iw2DSurfaceShow();

		// principal loop da aplicação
		while( GameManager::update() )
			GameManager::draw();
	 
		// executa funcoes necessarias do término da aplicacao
		GameManager::terminate();

		// if IwResourceManager for usado
		IwResManagerTerminate();
	}

	IwUITerminate();
	IwGxFontTerminate();
	Iw2DTerminate();
}