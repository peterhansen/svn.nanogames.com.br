/** \file
	defines.h
	24/05/2011
	
	Arquivo para defines importantes do projeto.
*/
#include <ctime>
#include <string>
#include <map>

#include <Iw2D.h>
#include <IwColour.h>
#include <IwGxFont.h>

#include <components\boost\shared_ptr.hpp>

#ifndef DEFINES_H
#define DEFINES_H

// debug
#define DEBUG			IW_DEBUG
#define LOG( s, ... )	IwTrace( MYAPP, ( s, ##__VA_ARGS__) )
	
// deleting
#define DELETE( p )		delete p; p = NULL;

// basic types
#define Point			CIwSVec2
#define FloatCoord		CIwFVec2
#define Image			CIw2DImage
#define Color			CIwColour
#define Sequence		CIwArray<Frame>
#define Font			CIwGxFont
#define Transform		CIw2DImageTransform
#define FontHAlignment	IwGxFontAlignHor
#define FontVAlignment	IwGxFontAlignVer
#define FontFormatFlags int

// pointers
#define ImagePtr			boost::shared_ptr<Image>
#define RectanglePtr		boost::shared_ptr<Rectangle>
#define DrawablePtr			boost::shared_ptr<Drawable>
#define DrawableGroupPtr	boost::shared_ptr<DrawableGroup>
#define DrawableImagePtr	boost::shared_ptr<DrawableImage>
#define SpritePtr			boost::shared_ptr<Sprite>
#define PatternPtr			boost::shared_ptr<Pattern>
#define FontPtr				boost::shared_ptr<Font>
#define LabelPtr			boost::shared_ptr<Label>
#define SoundPtr			boost::shared_ptr<Sound>
#define ScreenPtr			boost::shared_ptr<Screen>

// state handling
#define COLOR_STATE						DEFINE_H_COLOR_STATE
#define saveColorState()				CIwColour COLOR_STATE = Iw2DGetColour()
#define setColorState( c )				Iw2DSetColour( c )
#define saveAndSetColorState( c )		saveColorState(); setColorState( c )
#define restoreColorState()				Iw2DSetColour(COLOR_STATE)
#define saveImageTransformState()		Transform DEFINE_H_IMAGE_TRANSFORM_STATE = Iw2DGetImageTransform()
#define restoreImageTransformState()	Iw2DSetImageTransform( DEFINE_H_IMAGE_TRANSFORM_STATE )
#define saveTransformState()			CIwMat2D DEFINE_H_TRANSFORM_STATE = Iw2DGetTransformMatrix()
#define restoreTransformState()			Iw2DSetTransformMatrix( DEFINE_H_TRANSFORM_STATE )
	
// languages
#define LANGUAGE_PT			0
#define LANGUAGE_EN			1

// files
#define MAX_FILENAME_LENGTH		60		//!< Tamanho m�ximo que o nome de um arquivo lido pode ter.

namespace NanoMath
{
	inline static int abs( int a )							{ return a < 0? -a: a; }
	inline static int absmod( int a, int q )				{ return a >= 0? a % q : ( q - abs( a ) % q ); }
	inline static int max( int a, int b )					{ return a > b? a: b; }
	inline static int min( int a, int b )					{ return a > b? b: a; }
	inline static int clamp( int x, int a, int b )			{ return min( max( x, a ), b ); }
	inline static int randomRange( int a, int b )			{ return IwRandMinMax( a, b ); }
	inline static iwfixed random()							{ return randomRange( 0, IW_GEOM_ONE ); }
	inline static int lerp( int a, int b, int x )			{ return a + ( ( b - a ) * x ); }
	inline static iwfixed lerpFixed( int a, int b, int x )	{ return a + ( ( ( b - a ) * x ) >> IW_GEOM_POINT ); }
};

namespace NanoColor
{
	/** M�todo f�brica de cores. As componentes de cor sofrem um clamp para o intervalo 0 ~ 255. */
	CIwColour Create( int r, int g, int b, int a );
	
	/** M�todo de composi��o multiplicativa de cores. */
	CIwColour Compose( CIwColour c1, CIwColour c2 );
};

namespace NanoUtils
{
	// Returns the value of time in seconds since 0 hours, 0 minutes, 0 seconds,
	// January 1, 1970, Coordinated Universal Time, without including leap seconds
	inline time_t GetTimeStamp( void )
	{
		return time( NULL );
	}
};
#endif