#include <Iw2D.h>
#include <IwGx.h>

#include <components\defines.h>
#include <components\sprite.h>

Sprite::Sprite( ImagePtr image, std::string seqFile ) : Drawable(),
	image( image ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( true ),
	frameIndex( 0 ),
	paused( false )
{
	sequences = readSequence( s3eFileOpen( seqFile.data(), "rb" ) );
	int seqSize = sequences.size();
}

void Sprite::setFrameIndex( int frameIndex )
{
	this->frameIndex = NanoMath::absmod( frameIndex, sequences[sequenceIndex].size() );
}

void Sprite::setSequenceIndex( int sequenceIndex )
{
	this->sequenceIndex = sequenceIndex; 
	frameIndex = 0;
}

void Sprite::update( unsigned int dt )
{
	// se o frame tiver duração 0 (zero), o controle da animação é feito externamente, através de chamadas explícitas
	// a nextFrame, previousFrame e etc.
	int frameTime = sequences[sequenceIndex][frameIndex].time;
	if ( frameTime > 0 && !paused )
	{
		accTime += dt;
		if ( accTime >= frameTime )
		{
			// trocou de frame
			accTime %= frameTime;
			nextFrame();
		}
	}
}

void Sprite::paint()
{
	Rectangle bounds = sequences[sequenceIndex][frameIndex].bounds;
	if( !scaleToSize )
		Iw2DDrawImageRegion( image.get(), clipStack->translate, bounds.getSize(), bounds.getPosition(), bounds.getSize() );
	else
		Iw2DDrawImageRegion( image.get(), clipStack->translate, size, bounds.getPosition(), bounds.getSize() );
	
}

CIwArray<Sequence> Sprite::readSequence( s3eFile* file )
{
	char line[SEQUENCE_MAXCHARSLINE];
	CIwArray<Sequence> seqs;
	while( s3eFileReadString(line, SEQUENCE_MAXCHARSLINE, file ) )
	{
		// reading sequence
		Sequence s = Sequence();
		char* charPtr;
		charPtr = strtok ( line, " " );
		while ( charPtr != NULL )
		{
			// reading frame
			int x = strtol( charPtr, NULL, 10 );
			charPtr = strtok ( NULL, " " );
			int y = strtol( charPtr, NULL, 10 );
			charPtr = strtok ( NULL, " " );
			int w = strtol(charPtr, NULL, 10 );
			charPtr = strtok ( NULL, " " );
			int h = strtol(charPtr, NULL, 10 );
			charPtr = strtok ( NULL, " " );
			int t = strtol(charPtr, NULL, 10 );
			charPtr = strtok ( NULL, " " );
			Frame f = Frame();
			f.bounds.set( Point( x , y ), Point( w, h ) );
			f.time = t;
			s.push_back( f );
		}
		seqs.push_back( s );
	}
	return seqs;
}