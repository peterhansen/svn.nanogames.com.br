
#include <components/nanoonline/customer.h>

#include <components/nanoonline/connection.h>
#include <components/nanoonline/NORequestHolder.h>

// Parâmetros de requisições ao NanoOnline
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME			0
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME			1
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME			2
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER		3
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL				4
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF					5
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD			6
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD_CONFIRM	7
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END			8
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENDER				9
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY			10
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID			11
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP			12
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_VALIDATE_BEFORE		13
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_VALIDATED_AT		14

#define NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME 0
#define NANO_ONLINE_CUSTOMER_PARAM_LOGIN_PASSWORD 1

// Códigos de retorno relativos à requisição de login
#define RC_ERROR_LOGIN_NICKNAME_NOT_FOUND	1
#define RC_ERROR_LOGIN_PASSWORD_INVALID		2

bool NOCustomer::sendLoginRequest( NOCustomer* customer, NOListener* listener )
{ 
	ConnectionPtr conn = customer->getConn();
	conn->setListener( listener );
	return conn->sendRequest( "/customers/login", MakeNORequestHolder( *customer, &NOCustomer::login, &NOCustomer::loginResponse ) );
}

bool NOCustomer::login( MemoryStream& stream )
{
	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME );

#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.writeUTF32String( nickname );
#else
	stream.writeUTF8String( nickname );
#endif

	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_PASSWORD );
	
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.writeUTF32String( password );
#else
	stream.writeUTF8String( password );
#endif
	
	return true;
}

bool NOCustomer::loginResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			{
				// O login é a única ação na qual o ID do usuário vem fora de NANO_ONLINE_ID_SPECIFIC_DATA
				if( table.find( NANO_ONLINE_ID_CUSTOMER_ID ) == table.end() )
				{
					GetNOText( NO_TXT_INVALID_RESPONSE, errorStr );
					return false;
				}
				else
				{
					// Armazena o profileId do usuário
					profileId = *static_cast< const int32* >( table[ NANO_ONLINE_ID_CUSTOMER_ID ].get() );
				}
			}
			return true;

		case RC_ERROR_LOGIN_PASSWORD_INVALID:
			GetNOText( NO_TXT_WRONG_PASSWORD, errorStr );
			return false;
			
		case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
			GetNOText( NO_TXT_BAD_NICKNAME, errorStr );
			return false;
			
		default:
			//#if DEBUG
			//	assert_n_log( false, "Unrecognized param return code sent to loginResponse in response to login request" );
			//#else
				GetNOText( NO_TXT_UNKNOWN_ERROR, errorStr );
			//#endif
			return false;
	}
}

bool NOCustomer::SendDownloadRequest( NOCustomer* customer, NOListener* listener )
{
	ConnectionPtr conn = customer->getConn();
	conn->setListener( listener );
	return conn->sendRequest( "/customers/import", MakeNORequestHolder( *customer, &NOCustomer::download, &NOCustomer::downloadResponse ) );
}

/*==============================================================================================

MÉTODO download
	Indica qual perfil deverá ser baixado.

===============================================================================================*/

bool NOCustomer::download( MemoryStream& stream )
{
	if( login( stream ) )
	{
		stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP );
		stream.writeInt64( timeStamp );
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO downloadResponse
	Trata a resposta do download do perfil.

===============================================================================================*/

bool NOCustomer::downloadResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			{
				std::vector< uint8 >* pSpecificData = static_cast< std::vector< uint8 >* >( table[ NANO_ONLINE_ID_SPECIFIC_DATA ].get() );
				
				MemoryStream dataStream;
				dataStream.swapBuffer( *pSpecificData );
				
				return readDownloadData( dataStream, errorStr );
			}
			break; // Nunca alcançará este break
					
		case RC_ERROR_LOGIN_PASSWORD_INVALID:
			GetNOText( NO_TXT_WRONG_PASSWORD, errorStr );
			return false;
			
		case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
			GetNOText( NO_TXT_BAD_NICKNAME, errorStr );
			return false;
			
		default:
			//#if DEBUG
			//	assert_n_log( false, "Unrecognized param return code sent to downloadResponse in response to download request" );
			//#else
				GetNOText( NO_TXT_UNKNOWN_ERROR, errorStr );
			//#endif
			return false;
	}
}

/*==============================================================================================

MÉTODO readDownloadData
	Lê os dados específicos do download de perfil.

===============================================================================================*/

bool NOCustomer::readDownloadData( MemoryStream& dataStream, NOString& errorStr )
{
	while( !dataStream.eos() )
	{
#if DEBUG
		int8 id = dataStream.readInt8();
		LOG( ">>> NOCustomer::readDownloadData => Read attrib id = %d\n", static_cast< int32 >( id ) );
		switch( id )
#else
		switch( dataStream.readInt8() )
#endif
		{
			case NANO_ONLINE_ID_CUSTOMER_ID:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID:
				profileId = dataStream.readInt32();
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( nickname );
				#else
					dataStream.readUTF8String( nickname );
				#endif
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( firstName );
				#else
					dataStream.readUTF8String( firstName );
				#endif
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( lastName );
				#else
					dataStream.readUTF8String( lastName );
				#endif
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( email );
				#else
					dataStream.readUTF8String( email );
				#endif
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENDER:
				{
					#ifdef NANO_ONLINE_UNICODE_SUPPORT
						NOString::value_type c = dataStream.readInt32();
						if( ( c == L'm' ) || ( c == L'M' ) )
							gender = GENDER_M;
						else if( ( c == L'f' ) || ( c == L'F' ) )
							gender = GENDER_F;
						else
							gender = GENDER_N;
					#else
						char c = dataStream.readInt8();
						if( ( c == 'm' ) || ( c == 'M' ) )
							gender = GENDER_M;
						else if( ( c == 'f' ) || ( c == 'F' ) )
							gender = GENDER_F;
						else
							gender = GENDER_N;
					#endif
				}	
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY:
				{
					int64 aux;
					time_t encodedBirthday = static_cast< time_t >( dataStream.readInt64( aux ) );
					
					if( encodedBirthday != -1 )
					{
						DecodeBirthday( encodedBirthday, &birthdayDay, &birthdayMonth, &birthdayYear );
					}
					else
					{
						birthdayDay = -1;
						birthdayMonth = -1;
						birthdayYear = -1;
					}
				}
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP:
				{
					int64 aux;
					timeStamp = static_cast< time_t >( dataStream.readInt64( aux ) );
				}
				break;
				
			// Ignora esses campos
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD_CONFIRM:
				{
					NOString dummy;
					
					#ifdef NANO_ONLINE_UNICODE_SUPPORT
						dataStream.readUTF32String( dummy );
					#else
						dataStream.readUTF8String( dummy );
					#endif
				}
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END:
				return true;

			// Não sabe ler o campo, então ignora
			default:
				//#if DEBUG
					//assert_n_log( false, ">>> NOCustomer::readDownloadData => Unknown parameter" );
				//#endif
				LOG( ">>> NOCustomer::readDownloadData => Unknown parameter" );
				return false;
				break;
		}
	}
	return true;
}

void NOCustomer::DecodeBirthday( time_t& encodedBirthday, int8* day, int8* month, int32* year )
{
	struct tm decodedBirthday;
	memset( &decodedBirthday, 0, sizeof( struct tm ));

	if( encodedBirthday != -1 )
		gmtime_r( &encodedBirthday, &decodedBirthday );
	else
		decodedBirthday.tm_mon = -1;
	
	if( day )
		*day = decodedBirthday.tm_mday;
	
	if( month )
		*month = decodedBirthday.tm_mon + 1; // Mês, nessa estrutura, vai de 0  a 11, mas dia vai de 1 a 31... Resolvi seguir o padrão do campo dia
	
	if( year )
		*year = decodedBirthday.tm_year + 1900;
}