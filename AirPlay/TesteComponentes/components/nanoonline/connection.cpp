#include <components/nanoonline/connection.h>

// TODO: levar para algum arquivo de defini��es?
typedef boost::shared_ptr< char > CStrPtr;
typedef boost::shared_ptr< NOString > StrPtr;
typedef boost::shared_ptr< uint8 > UInt8Ptr;
typedef boost::shared_ptr< int16 > Int16Ptr;
typedef boost::shared_ptr< int32 > Int32Ptr;
typedef boost::shared_ptr< int64 > Int64Ptr;
typedef boost::shared_ptr< std::vector< uint8 > > ByteVectPtr;

Connection::Connection( std::string hostUrl, std::string appVersion, std::string appShortName, int16 languageId )
	: hostUrl( hostUrl ),
	appVersion( appVersion ),
	appShortName( appShortName ),
	languageId( languageId )
{
	pCurrRequester = NULL;
	httpRequest = NULL;
	result = NULL;
	status = HTTP_READY;
	httpRequest = new CIwHTTP;
	resultLength = 0;
}

Connection::~Connection()
{
	if ( httpRequest )
		httpRequest->Cancel();
	DELETE( httpRequest );
	DELETE( pCurrRequester );
	s3eFree( result );
}

void Connection::setRequestHeader( std::string header, std::string value )
{ 
	httpRequest->SetRequestHeader( header.data(), value );
}

bool Connection::requestGet( const char *uri )
{
	bool ret = httpRequest->Get( uri, HTTP_HEADERS_CB, this ) == S3E_RESULT_SUCCESS;
	if( ret )
		status = HTTP_WAITING_RESPONSE;
	return ret;
}

bool Connection::requestPost( const char *uri, const char *body, int32 bodyLength )
{
	httpRequest->SetRequestHeader( "Content-Type", "application/octet-stream" );

	bool ret = httpRequest->Post( uri, body, bodyLength, HTTP_HEADERS_CB, this ) == S3E_RESULT_SUCCESS;
	if( ret )
		status = HTTP_WAITING_RESPONSE;
	return ret;
}

NOMap& Connection::ReadHeader( NOMap& table, MemoryStream& input )
{
	LOG( "NOConnection => Parsing response\n" );
	
	NOMap tempTable;

	bool ret = true;
	while( !input.eos() && ret )
	{
		int8 id = input.readInt8();
		LOG( "NOConnection => Leu ID: %d\n", static_cast< int32 >( id ) );

		switch( id )
		{
			case NANO_ONLINE_ID_APP:
			case NANO_ONLINE_ID_RETURN_CODE:
				{
					Int16Ptr pShort( new int16( input.readInt16() ) );
					if( pShort == NULL )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pShort ) );	
						LOG( "NOConnection => Read int16: %d\n\n", *pShort );
					}
				}
				break;

			case NANO_ONLINE_ID_CUSTOMER_ID:
				{
					Int32Ptr pInteger( new int32( input.readInt32() ) );
					if( pInteger == NULL )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pInteger ) );
						LOG( "NOConnection => Read int32: %d\n\n", *pInteger );
					}
				}
				break;

			case NANO_ONLINE_ID_LANGUAGE:
				{
					UInt8Ptr pByte( new uint8( static_cast< uint8 >( input.readInt8() ) ) );
					if( pByte == NULL )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pByte ) );
						LOG( "NOConnection => Read byte: %d\n\n", *pByte );
					}
				}
				break;

			case NANO_ONLINE_ID_APP_VERSION:
			case NANO_ONLINE_ID_NANO_ONLINE_VERSION:
			case NANO_ONLINE_ID_ERROR_MESSAGE:
				{
					#ifdef NANO_ONLINE_UNICODE_SUPPORT
						StrPtr pStr( new NOString() );
						if( ( pStr == NULL ) || ( input.readUTF32String( *pStr ).empty() ) )
					#else
						StrPtr pStr( new std::string() );
						if( ( pStr == NULL ) || ( input.readUTF8String( *pStr ).empty() ) )
					#endif
					{
						ret = false;
					}
					else
					{
						#if DEBUG
							#ifdef NANO_ONLINE_UNICODE_SUPPORT
								LOG( ">>> NOConnection => Read string: %ls\n\n", pStr->c_str() );
							#else
								LOG( ">>> NOConnection => Read string: %s\n\n", pStr->c_str() );
							#endif
						#endif
					}
				}
				break;

			case NANO_ONLINE_ID_SPECIFIC_DATA:
				{
					ByteVectPtr pSpecificData( new std::vector< uint8 >() );
					if( ( pSpecificData == NULL ) || ( input.readData( *pSpecificData ).size() == 0 ) )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pSpecificData ) );
						LOG( "NOConnection => Read byte array\n\n" );
					}
				}
				break;

		
			default:
				LOG( "%s\n", input.begin() );
				LOG( "NOConnection => ReadHeader( NOMap&, MemoryStream& ): Unknown field id in response header" );
				ret = false;
				break;
		}
	}
	
	if( ret )
		table = tempTable;
	else
		table.clear();
	
	return table;
}
void Connection::getHeaders()
{
	if (httpRequest->GetStatus() == S3E_RESULT_ERROR)
	{
		// Something has gone wrong
		status = HTTP_ERROR;
		NOString error;
		GetNOText( NO_TXT_HTTP_GET_HEADERS_ERROR, error ); 
		listener->onNOError( error );
	}
	else
	{
		// Depending on how the server is communicating the content
		// length, we may actually know the length of the content, or
		// we may know the length of the first part of it, or we may
		// know nothing. ContentExpected always returns the smallest
		// possible size of the content, so allocate that much space
		// for now if it's non-zero. If it is of zero size, the server
		// has given no indication, so we need to guess. We'll guess at 1k.
		resultLength = httpRequest->ContentExpected();
		if (!resultLength)
		{
			resultLength = 1024;
		}
		s3eFree( result );
		result = (char*) s3eMalloc( resultLength + 1 );
		(result)[resultLength] = 0;
		httpRequest->ReadContent( result, resultLength, HTTP_DATA_CB, this );
	}
}

void Connection::getData()
{
	if (httpRequest->GetStatus() == S3E_RESULT_ERROR)
	{
		// Something has gone wrong
		status = HTTP_ERROR;
		NOString errorStr;
		GetNOText( NO_TXT_HTTP_GET_DATA_ERROR, errorStr );
		listener->onNOError( errorStr );
	}
	else if (httpRequest->ContentReceived() != httpRequest->ContentLength())
	{
		// We have some data but not all of it. We need more space.
		uint32 oldLen = resultLength;

		// If iwhttp has a guess how big the next bit of data is (this
		// basically means chunked encoding is being used), allocate
		// that much space. Otherwise guess.
		if (resultLength < httpRequest->ContentExpected())
			resultLength = httpRequest->ContentExpected();
		else
			resultLength += 1024;

		// Allocate some more space and fetch the data.
		result = (char*)s3eRealloc(result, resultLength);
		httpRequest->ReadContent( &result[oldLen], resultLength - oldLen, HTTP_DATA_CB, this );
	}
	else
	{
		// We've got all the data.
		status = HTTP_OK;
		listener->onNOSuccessfulResponse();

		// parseando resultado!
		ByteVectPtr responseData = ByteVectPtr( new std::vector< uint8 >() ); 
		//LOG( "RESULTADO!!" );
		for( int i = 0; i < resultLength; i++ )
		//{
		//	LOG( "%d", result[i] );
			responseData->push_back( result[i] );
		
		onNetworkRequestCompleted( responseData.get() );
	}
}

void Connection::writeHeader( MemoryStream& output, int32 customerId )
{
	// Escreve a assinatura dos pacotes
	uint8 signature[ NANO_ONLINE_SIGNATURE_LEN ] = NANO_ONLINE_SIGNATURE;
	output.writeData( signature, NANO_ONLINE_SIGNATURE_LEN );

	// Escreve a vers�o do cliente
	output.writeInt8( NANO_ONLINE_ID_NANO_ONLINE_VERSION );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_NANO_ONLINE_VERSION: %d\n", NANO_ONLINE_ID_NANO_ONLINE_VERSION );
	
	std::string clientVersion;
	output.writeUTF8String( GetNanoOnlineClientVersion( &clientVersion )->c_str() );
	LOG( "Value: %s\n\n", clientVersion.c_str() );

	// Escreve o identificador da aplica��o
	output.writeInt8( NANO_ONLINE_ID_APP );

	output.writeUTF8String( GameManager::getAppShortName() );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_APP: %d\nValue: %s\n\n", NANO_ONLINE_ID_APP, appShortName.c_str() );

	// Escreve o identificador do usu�rio
	output.writeInt8( NANO_ONLINE_ID_CUSTOMER_ID );
	output.writeInt32( customerId );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_CUSTOMER_ID: %d\nValue: %d\n\n", NANO_ONLINE_ID_CUSTOMER_ID, customerId );

	// Escreve o identificador do idioma
	output.writeInt8( NANO_ONLINE_ID_LANGUAGE );
	output.writeInt8( GameManager::getLanguageId() );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_LANGUAGE: %d\nValue: %d\n\n", NANO_ONLINE_ID_LANGUAGE, languageId );

	// Escreve a vers�o da aplica��o
	output.writeInt8( NANO_ONLINE_ID_APP_VERSION );
	output.writeUTF8String( GameManager::getAppVersion() );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_APP_VERSION: %d\nValue: %s\n\n", NANO_ONLINE_ID_APP_VERSION, appVersion.c_str() );
	
	// Escreve o hor�rio local do cliente
	output.writeInt8( NANO_ONLINE_ID_CLIENT_LOCAL_TIME );
	time_t ts = NanoUtils::GetTimeStamp();
	output.writeInt64( static_cast< int64 >( ts ) );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_CLIENT_LOCAL_TIME: %d\nValue: %lld\n\n", NANO_ONLINE_ID_CLIENT_LOCAL_TIME, ts );

	// Escreve o identificador dos dados espec�ficos
	output.writeInt8( NANO_ONLINE_ID_SPECIFIC_DATA );
	LOG( "NOConnection => Wrote ID NANO_ONLINE_ID_SPECIFIC_DATA: %d\nValue: Empty\n\n", NANO_ONLINE_ID_SPECIFIC_DATA );
}


bool Connection::sendRequest( std::string commandURL, const NORequestHolderBase &requester )
{
	// mata um request holder anterior e armazena uma c�pia do novo
	DELETE( pCurrRequester );
	pCurrRequester = requester.clone();

	bool ret = false;
		
	if( hostUrl.empty() )
		return ret;
	
	std::string url = hostUrl + commandURL;
		
	// TODO: pegar o UserAgent??
	//CFStringRef pUserAgent = GetDeviceUserAgent();
	//if( !pUserAgent )
	//	{
	//		CFKILL( pRequest );
	//		goto End;
	//	}
	//setRequestHeader( "User-Agent", pUserAgent );
		
	// Determina o corpo da mensagem (geralmente para requisi��es POST)
	MemoryStream nanoOnlineData;
	writeHeader( nanoOnlineData );

	if( !pCurrRequester->onRequest( nanoOnlineData ) )
	{
		return false;
	}

	// TODO: lidar com mesmo problema do MAC?
	//nanoOnlineData.writeInt8( NANO_ONLINE_ID_MACOS_RAILS_BUG_FIX );

	LOG( "NanoOnline Data Size %d\n", nanoOnlineData.getSize() );

	int32 dataSize = nanoOnlineData.getSize();
	sentData = (char*) s3eMalloc( dataSize );
	nanoOnlineData.readCharData( sentData, dataSize );

	// J� criamos o corpo da mensagem, ent�o podemos esvaziar a stream
	nanoOnlineData.clear();

	ret = requestPost( url.data(), sentData, dataSize );
	if( ret )
	{
		listener->onNORequestSent();
	}
	else
	{
		NOString errorStr;
		GetNOText( NO_TXT_HTTP_POST_REQUEST_ERROR, errorStr );
		listener->onNOError( errorStr );
	}

	return ret;
}

void Connection::onNetworkRequestCompleted( std::vector< uint8 > *data )
{
	LOG( ">>> NOConnection => Request Completed\n" );

	// OBS: Esta chamada invalida os dados do par�metro 'data'. No entanto, otimizamos o consumo de mem�ria
	MemoryStream dataStream;
	dataStream.swapBuffer( *data );

	// Faz o parse dos dados
	NOMap table;
	ReadHeader( table, dataStream );
	dataStream.clear();
	
	if( table.size() == 0 )
	{
		NOString error;
		GetNOText( NO_TXT_INVALID_RESPONSE, error );
		listener->onNOError( error );
	}
	else if( table.find( NANO_ONLINE_ID_ERROR_MESSAGE ) != table.end() )
	{
		NOString error = *static_cast< const NOString* >( table[ NANO_ONLINE_ID_ERROR_MESSAGE ].get() );
		listener->onNOError( error );
	}
	else
	{
		int16 returnCode = NANO_ONLINE_RC_OK;
		if( table.find( NANO_ONLINE_ID_RETURN_CODE ) != table.end() )
			returnCode = *static_cast< const int16* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() );
		
		if( returnCode < NANO_ONLINE_RC_OK )
		{
			NOString error;
			switch( returnCode )
			{
				case NANO_ONLINE_RC_DEVICE_NOT_SUPPORTED:
					GetNOText( NO_TXT_DEVICE_UNSUPPORTED, error );
					break;

				case NANO_ONLINE_RC_APP_NOT_FOUND:
					GetNOText( NO_TXT_APP_UNSUPPORTED, error );
					break;
					
				case NANO_ONLINE_RC_SERVER_INTERNAL_ERROR:
				default:
					GetNOText( NO_TXT_SERVER_INTERNAL_ERROR, error );
					break;
			}
			listener->onNOError( error );
		}
		else
		{	
			NOString error;
			if( pCurrRequester->onResponse( table, error ) )
				listener->onNOSuccessfulResponse();
			else
				listener->onNOError( error );
		}
	}
}

int32 HTTP_DATA_CB( void*, void* httpConnPtr)
{
    // This is the callback indicating that a ReadContent call has
    // completed.  Either we've finished, or a bigger buffer is
    // needed.  If the correct ammount of data was supplied initially,
    // then this will only be called once. However, it may well be
    // called several times when using chunked encoding.
	Connection *connection = (Connection*) httpConnPtr;
	connection->getData();
    return 0;
}

int32 HTTP_HEADERS_CB(void*, void* httpConnPtr)
{
	Connection *connection = (Connection*) httpConnPtr;
	connection->freeSentData();
	connection->getHeaders();
    return 0;
}