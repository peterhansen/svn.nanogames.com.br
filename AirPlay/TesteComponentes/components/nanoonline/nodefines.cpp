#include <components\nanoonline\nodefines.h>

void GetNOText( uint16 textIndex, NOString& outText )
{
	//NOString::value_type buffer[ GET_TXT_BUFFER_SIZE ];
	//snprintf( buffer, GET_TXT_BUFFER_SIZE, "%d", static_cast< int32 >( textIndex ) );

	// TODO: fazer leitura vir de outro lugar
	std::string outString;
	switch( textIndex )
	{
		case NO_TXT_WRONG_PASSWORD:				outString = "Senha errada";									break;
		case NO_TXT_BAD_NICKNAME:				outString = "Nome de usuário inexistente";					break;
		case NO_TXT_INVALID_RESPONSE:			outString = "Resposta inválida";							break;
		case NO_TXT_DEVICE_UNSUPPORTED:			outString = "Não há suporte para esse aparelho";			break;
		case NO_TXT_APP_UNSUPPORTED:			outString = "Não há suporte para essa aplicação";			break;
		case NO_TXT_SERVER_INTERNAL_ERROR:		outString = "Erro interno do servidor";						break;
		case NO_TXT_HTTP_GET_HEADERS_ERROR:		outString = "Erro no processamento do cabeçalho da resposta HTTP";	break;
		case NO_TXT_HTTP_GET_DATA_ERROR:		outString = "Erro no processamento do corpo da resposta HTTP";		break;
		case NO_TXT_HTTP_POST_REQUEST_ERROR:	outString = "Não foi possível realizar requisição HTTP";			break;

		default:
		case NO_TXT_UNKNOWN_ERROR:				outString = "Erro desconhecido";				break;
	}

	outText = convertFromMBString( outString.data(), outString.size() );

//#ifdef NANO_ONLINE_UNICODE_SUPPORT
//	// TODO: fazer método de conversão melhor (que executa linhas abaixo)
//	// TODO: colocar em algum lugar tamanho mÃ¡ximo da mensagem de erro...
//	wchar_t wErrorStr[100];
//	int16 errorStrSize = outString.size();
//	mbstowcs( wErrorStr, outString.data(), errorStrSize );
//	wErrorStr[errorStrSize] = 0;
//	outText = wErrorStr;
//#else
//	outText = outString;
//#endif
}

#define NANO_ONLINE_MAX_MESSAGE_SIZE	256

std::string convertToMBString( NOString noString )
{
	char str[NANO_ONLINE_MAX_MESSAGE_SIZE];
	std::string out;
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	int16 size = noString.size();
	wcstombs( str, noString.c_str(), size );
	str[size] = 0;
	out = std::string( out );
#else
	str = noString;
#endif
	return str;
}

NOString convertFromMBString( const char *mbStr, int16 len )
{
	NOString out;
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	wchar_t str[NANO_ONLINE_MAX_MESSAGE_SIZE];
	mbstowcs( str, mbStr, len );
	str[len] = 0;
	out = NOString( str );
#else
	out = NOString( mbStr );
#endif
	return out;
}