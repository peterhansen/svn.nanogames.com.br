/** \file connection.h
	13-06-2011
*/
#include <vector>
#include <IwHTTP.h>

#include <components/defines.h>
#include <components/gamemanager.h>
#include <components/nanoonline/NOListener.h>
#include <components/nanoonline/MemoryStream.h>
#include <components/nanoonline/NORequestHolder.h>

#ifndef CONNECTION_H
#define CONNECTION_H

#define NANO_ONLINE_SIGNATURE_LEN			9
#define NANO_ONLINE_SIGNATURE				{ 137, 110, 97, 110, 111, 13, 10, 26, 10 }
#define NANO_ONLINE_ID_APP					-128		// Id de chave global: aplicativo/jogo. Tipo do valor: String
#define NANO_ONLINE_ID_APP_VERSION			-127		// Id de chave global: versão do aplicativo/jogo. Tipo do valor: string
#define NANO_ONLINE_ID_LANGUAGE				-126		// Id de chave global: idioma atual do aplicativo. Tipo do valor: byte
#define NANO_ONLINE_ID_NANO_ONLINE_VERSION	-125		// Id de chave global: versão do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string
#define NANO_ONLINE_ID_RETURN_CODE			-124		// Id de chave global: código de retorno. Tipo do valor: short
#define NANO_ONLINE_ID_CUSTOMER_ID			-123		// Id de chave global: usuário. Tipo do valor: int
#define NANO_ONLINE_ID_ERROR_MESSAGE		-122		// Id de chave global: mensagem de erro. Tipo do valor: string
#define NANO_ONLINE_ID_SPECIFIC_DATA		-121		// Id de chave global: início de dados específicos. Tipo do valor: byte[] (tamanho variável)
#define NANO_ONLINE_ID_FEEDER_DATA			-120		// Id de chave global: dados do servidor de notícias
#define NANO_ONLINE_ID_AD_SERVER_DATA		-119		// Id de chave global: dados do servidor de anúncios
#define NANO_ONLINE_ID_CLIENT_LOCAL_TIME	-118		// Id de chave global: horário local do cliente

#define NANO_ONLINE_ID_MACOS_RAILS_BUG_FIX	-100		// Id de chave global: Byte extra enviado nas requisições POST feitas via MacOS e iPhoneOS. Sem fazer isso, o Rails ignora o último byte do corpo de uma mensagem quando seu valor é 0, causando bugs estranhos

// Códigos de erro. Assim como as chaves, os valores de retorno globais possuem valores negativos para
// que não haja confusão com os valores utilizados internamente em cada serviço
#define NANO_ONLINE_RC_OK						 0
#define NANO_ONLINE_RC_SERVER_INTERNAL_ERROR	-1
#define NANO_ONLINE_RC_DEVICE_NOT_SUPPORTED		-2
#define NANO_ONLINE_RC_APP_NOT_FOUND			-3

/** \enum HTTPStatus
	\brief Descreve o estado de uma requisição HTTP.
	@see Connection
*/
enum HTTPStatus
{
	HTTP_READY,
	HTTP_WAITING_RESPONSE,
	HTTP_OK,
	HTTP_ERROR
};

// TODO: remover necessidade dessas forwards? Precisamos passar member function pointers... olhar MakeNORequestHolder
// forward declarations
int32 HTTP_HEADERS_CB( void *sysData, void *userData );
int32 HTTP_DATA_CB( void *sysData, void *userData );

/** \class Connection 
	\brief Classe que oferece serviços de requisição HTTP.
*/
class Connection
{
public:

	Connection( std::string hostUrl, std::string appVersion, std::string appShortName, int16 languageId );
	~Connection();

	// TODO: aumentar a privacidade desses membros
	void getHeaders();
	void getData();

	void inline setListener( NOListener *listener )
	{
		this->listener = listener;
	}
	
	void inline freeSentData()
	{
		s3eFree( sentData );
	}

	bool sendRequest( std::string commandURL, const NORequestHolderBase &requester );

protected:
	CIwHTTP *httpRequest;
	HTTPStatus status;
	char *result;
	int resultLength;
	NOListener *listener;

	// TODO: pegar isso de um arquivo de configuração do Marmelade?
	// Versão do cliente Nano Online
	#define NANO_ONLINE_CLIENT_VERSION "0.0.3"

	NORequestHolderBase* pCurrRequester;

	/** Identificação do usuário que está logado no NanoOnline. */
	int32 currCustomerId;
	
	/** Identificação do idioma utilizado no NanoOnline. */
	int16 languageId;

	/** Identificador da aplicação. */
	std::string appShortName;
	
	/** Versão da aplicação. */
	std::string appVersion;
	
	/** Path do host. */
	std::string hostUrl;

	char *sentData;

	void setRequestHeader( std::string header, std::string value );

	bool requestGet( const char *uri );

	bool requestPost( const char *uri, const char *body, int32 bodyLength );

	NOMap& ReadHeader( NOMap& table, MemoryStream& input );

	void onNetworkRequestCompleted( std::vector< uint8 > *data );

	//void clear()
	//{
		//hostUrl = "";

		//clean();
	//}

	//void clean()
	//{
	//	delete pCurrRequester;
	//	pCurrRequester = NULL;
	//}

	std::string* GetNanoOnlineClientVersion( std::string* pVersion )
	{
		pVersion->append( NANO_ONLINE_CLIENT_VERSION );
		return pVersion;
	}

	void writeHeader( MemoryStream& output, int32 customerId = CUSTOMER_PROFILE_ID_NONE );
};
#endif