/** \file nocustomer.h
	2011-28-06
*/
#include <string>

#include <IwHTTP.h>

#include <components\defines.h>
#include <components\nanoonline\connection.h>
#include <components\nanoonline\nodefines.h>

#ifndef NO_CUSTOMER_H
#define NO_CUSTOMER_H

// forward declaration
class NOListener;

// Sexo do usuário
enum Gender
{
	GENDER_N = 0,
	GENDER_M = 1,
	GENDER_F = 2
};

/** \class Customer
	\brief Implementação de um cliente simples, responsável por acessar os serviços
	disponibilizados pelos servidores Nano Online.
*/
class NOCustomer
{
public:

	NOCustomer() :
	  timeStamp( 0 )
	{
		conn =  ConnectionPtr( new Connection(
			NO_HOST_URL, 
			GameManager::getAppVersion(), 
			GameManager::getAppShortName(), 
			GameManager::getLanguageId() 
		) );
	}

	inline ConnectionPtr getConn()
	{
		return conn;
	}

	Gender getGender( void ) const;
	void getBirthday( int8* day, int8* month, int32* year ) const;

	// TODO: implementar método
	NOString& getEmail( NOString& out ) const;

	// TODO: implementar método
	NOString& getPassword( NOString& out ) const;

	// TODO: implementar método
	NOString& getNickname( NOString& out ) const;

	void getFirstName( NOString& out ) const
	{
		out = firstName;
	}

	void getLastName( NOString& out ) const
	{
		out = lastName;
	}

	inline void setNickname( NOString nickname )
	{
		this->nickname = nickname;
	}

	inline void setPassword( NOString password )
	{
		this->password = password;
	}

	/** Envia requisição de login para os servidores Nano Online. */
	static bool sendLoginRequest( NOCustomer* customer, NOListener* listener );
	
	/** Envia requisição de download de informações do perfil para Nano Online */
	static bool SendDownloadRequest( NOCustomer* pCustomer, NOListener* pListener );
	
protected:
	ConnectionPtr conn;			//!< Conexão através da qual o cliente realiza as requisições.
	NOProfileId profileId;		//!< Índice de cadastro do perfil do usuário.
	NOString nickname;
	NOString password;		

	// Dados pessoais do usuário
	NOString firstName;
	NOString lastName;
	NOString email;
	Gender gender;
	int8 birthdayDay;
	int8 birthdayMonth;
	int32 birthdayYear;
	time_t timeStamp;

	bool login( MemoryStream& stream );
	bool loginResponse( NOMap& table, NOString& errorStr );

	// Tratam o download de perfis do NanoOnline
	bool download( MemoryStream& stream );
	bool downloadResponse( NOMap& table, NOString& errorStr );
	bool readDownloadData( MemoryStream& dataStream, NOString& errorStr );

	void DecodeBirthday( time_t& encodedBirthday, int8* day, int8* month, int32* year );
};
#endif