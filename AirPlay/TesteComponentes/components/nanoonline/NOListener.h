#include <components\nanoonline\nodefines.h>

#ifndef NO_LISTENER_H
#define NO_LISTENER_H

class NOListener
{
	public:
		// Destrutor
		virtual ~NOListener( void ){};
	
		// Indica que uma requisição foi enviada para o NanoOnline
		virtual void onNORequestSent( void ) = 0;
	
		// Indica que a requisição foi cancelada pelo usuário
		//virtual void onNORequestCancelled( void ) = 0;
	
		// Indica que uma requisição foi respondida e terminada com sucesso
		virtual void onNOSuccessfulResponse() = 0;
	
		// Sinaliza erros ocorridos nas operações do NanoOnline
		virtual void onNOError( NOString errorStr ) = 0;
	
		// Indica o progresso da requisição atual
		//virtual void onNOProgressChanged( int32 currBytes, int32 totalBytes ) = 0;
};
#endif