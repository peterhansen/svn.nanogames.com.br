/*
 *  MemoryStream.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef MEMORY_STREAM_H
#define MEMORY_STREAM_H 1

// Components
#include "IStream.h"
//#include "NanoTypes.h"

// C++
#include <vector>
#include <string>

class MemoryStream
{
	public:
		// Construtores
		MemoryStream( void );
		MemoryStream( const uint8* pMemory, uint32 memSize );
	
		// Destrutor
		virtual ~MemoryStream( void ){};
	
		// Métodos de leitura
		bool readBool( void );

		int8 readInt8( void );
		int16 readInt16( void );
		int32 readInt32( void );
		int64& readInt64( int64& out );

		float readFloat( void );
		double& readDouble( double& out );

		std::string& readUTF8String( std::string& outStr );
		std::wstring& readUTF32String( std::wstring& outWStr );

		// YGOR
		char* readCharData( char *pOutBuffer, int32 bufferSize );
	
		uint8* readData( uint8* pOutBuffer, int32 bufferSize );
		std::vector< uint8 >& readData( std::vector< uint8 >& outBuffer, int32 dataLen = -1 );
	
		// Métodos de escrita
		void writeBool( bool b );

		void writeInt8( int8 n );
		void writeInt16( int16 n );
		void writeInt32( int32 n );
		void writeInt64( const int64& n );
	
		void writeFloat( float f );
		void writeDouble( const double& d );

		void writeUTF8String( const std::string& pStr );
		void writeUTF32String( const std::wstring& pWStr );
	
		void writeData( const uint8* pData, int32 dataLen );
	
		// Retorna se a stream terminou
		bool eos( void );
	
		// Métodos para manipular a localização da stream
		void seek( uint32 location );
	
		// Iteradores
		const uint8* begin( void ) const;
		const uint8* end( void ) const;
	
		// Retorna o tamanho atual da stream
		uint32 getSize( void ) const;
	
		// Limpa a stream de dados
		void clear( void );
	
		// Troca o conteúdo do buffer com o conteúdo da stream
		void swapBuffer( std::vector< uint8 >& buffer );

	private:
		// Stream
		std::vector< uint8 > stream;
};

// Implementação dos métodos inline

inline bool MemoryStream::eos( void )
{
	return stream.size() <= 0 ;
}

inline const uint8* MemoryStream::begin( void ) const
{
	return &( *stream.begin() );
}

inline const uint8* MemoryStream::end( void ) const
{
	return &( *stream.end() );
}

inline uint32 MemoryStream::getSize( void ) const
{
	return stream.size();
}

inline void MemoryStream::clear( void )
{
	stream.clear();
}

inline void MemoryStream::swapBuffer( std::vector< uint8 >& buffer )
{
	stream.swap( buffer );
}
#endif
