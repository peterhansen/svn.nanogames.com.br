/** \file sprite.h
	27/05/2011
*/
#include <string>

#include <Iw2D.h>
#include <IwGx.h>
#include <s3e.h>

#include <components\defines.h>
#include <components\drawable.h>

#ifndef SPRITE_H
#define SPRITE_H

/** \struct Frame 
	\brief Frame da animação de uma sequência de um sprite. 
*/
struct Frame
{ 
	// TODO: fazer essa struct ser privada?
	int time;					//!< Duração da execução desse Frame (dt).
	Rectangle bounds;			//!< Retângulo envolvente de recorte dentro da spritesheet.
};

/** \class Sprite 
	\brief Um sprite com diferentes sequências de animação. 
*/
class Sprite : public Drawable
{
public:
	/**
	Construtor da classe Sprite.
		@param image Imagem para ser usada como spritesheet.
		@param seqFile Arquivo .seq que descreve as sequencias do sprite. Para saber mais sobre o arquivo .ser,
		leia sobre o método readSequence.
		@see readSequence
	*/
	Sprite( ImagePtr image, std::string seqFile );
	~Sprite() { sequences.clear_optimised(); }

	/** Incrementa o índice do frame da sequência atual. */
	void nextFrame() { setFrameIndex( frameIndex + 1 ); }

	/** Decrementa o índice do frame da sequência atual. */
	void previousFrame() { setFrameIndex( frameIndex - 1 ); }

	/** Atualiza o índice do frame da sequência atual, caso o índice seja válido. */
	void setFrameIndex( int frameIndex );

	/** Atualiza o índice da sequência do sprite, caso o índice seja válido. Esse método
		zera o índice de frame da sequência automaticamente. */
	void setSequenceIndex( int sequenceIndex );

	void update( unsigned int dt );

protected:

	CIwArray<Sequence> sequences;	//!< Sequencias de frames usadas por esse sprite.
	int sequenceIndex;				//!< Indice da sequencia corrente. Caso esteja como -1, significa que esse sprite nao possui sequencias.

	short accTime;					//!< Tempo acumulado no frame atual.
	const ImagePtr image;			//!< Imagem que contem os frames da animacao.
	int frameIndex;					//!< Indice corrente de acesso ao frame da sequencia (nao equivale ao frame da sequencia em si).
	
	bool paused;					//!< Indica se a animacao do sprite esta pausada no momento.

	void paint();

private:

	bool scaleToSize;				//!< Caso o sprite deva sofrer escala de forma que preencha seu tamanho.

	/** Número máximo de caracteres por linha na leitura de um arquivo de sequência (.seq) */
	static const int SEQUENCE_MAXCHARSLINE = 300;

	// TODO: explicar arquivo .seq melhor
	static CIwArray<Sequence> readSequence( s3eFile* file );
};
#endif