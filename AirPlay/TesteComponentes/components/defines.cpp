#include <components\defines.h>

namespace boost
{
	// caso BOOST_NO_EXCEPTIONS esteja ligado, redirecionamos as excess�es
	// do boost para c�.
	#ifdef BOOST_NO_EXCEPTIONS
		void throw_exception( std::exception const & e )
		{
			LOG( e.what() );
		}
	#endif
}

CIwColour NanoColor::Create( int r, int g, int b, int a )
{
	r = NanoMath::clamp( r, 0, 255 );
	g = NanoMath::clamp( g, 0, 255 );
	b = NanoMath::clamp( b, 0, 255 );
	a = NanoMath::clamp( a, 0, 255 );
	CIwColour c = CIwColour();
	c.Set( r, g, b, a );
	return c;
}

CIwColour NanoColor::Compose( CIwColour c1, CIwColour c2 )
{
	return NanoColor::Create( (c1.r * c2.r) / 255, (c1.g * c2.g) / 255, (c1.b * c2.b) / 255, (c1.a * c2.a) / 255 );
}