/** \file sound.h
	31/05/2011
*/

#ifndef SOUND_H
#define SOUND_H

#include <string>

/** \struct Sound 
	\brief Retém um ponteiro para buffer para um som carregado na memória.
*/
struct Sound
{
	Sound( std::string fileName )
	{
		s3eFile* file = s3eFileOpen( fileName.data(), "rb" );
		size = s3eFileGetSize( file );
		buffer = ( int16* ) s3eMallocBase( size );
		memset( buffer, 0, size );
		s3eFileRead( buffer, size, 1, file );
		s3eFileClose( file );
	}
	
	int		size;
	int16*	buffer;
};
#endif