//--------------------------------------------------------------------------
// HelloWorld main file
//--------------------------------------------------------------------------
 
#include "s3e.h"
 
// Externs for functions which examples must implement
void ExampleInit();
void ExampleShutDown();
void ExampleRender();
bool ExampleUpdate( int timeElapsedMs );
void ExampleSwapBuffers();
void ExampleUpdateInput(  int timeElapsedMs  );
 
#include "Iw2D.h"

//--------------------------------------------------------------------------
// Main global function
//--------------------------------------------------------------------------
int main(int argc, char* argv[])
{
	// Initialisation of Airplay Studio modules
	ExampleInit();

	// Example main loop
	uint32 timer = (uint32)s3eTimerGetMs();
	while(1)
	{
		s3eDeviceYield(0);

		// Calculate the amount of time that's passed since last frame
		int delta = uint32( s3eTimerGetMs() ) - timer;
		timer += delta;

		// Make sure the delta-time value is safe
		if( delta < 0 )
			delta = 0;
		else if( delta > 100 )
			delta = 100;

		ExampleUpdateInput( delta );

		bool result = ExampleUpdate( delta );
		if(
			(result == false) ||
			(s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_DOWN) ||
			(s3eKeyboardGetState(s3eKeyLSK) & S3E_KEY_STATE_DOWN) ||
			(s3eDeviceCheckQuitRequest())
		)
			break;

		ExampleRender();
		ExampleSwapBuffers();
	}
	ExampleShutDown();
}