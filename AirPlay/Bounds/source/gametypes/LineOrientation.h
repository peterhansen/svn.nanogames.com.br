/*
 *  LineOrientation.h
 *  Bounds
 *
 *  Created by Max on 18/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_LINE_ORIENATION_H
#define BOUNDS_LINE_ORIENATION_H

namespace bounds
{
	enum LineOrientation
	{
		LINE_ORIENTATION_HOR,
		LINE_ORIENTATION_VER,
	};
}

#endif