/*
 *  CharacterExpression.h
 *  Bounds
 *
 *  Created by Max on 18/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_CHARACTER_EXPRESSION_H
#define BOUNDS_CHARACTER_EXPRESSION_H

namespace bounds
{
	enum CharacterExpression
	{
		NORMAL,
		GOOD,
		VERY_GOOD,
		BEST,
		BAD,
		VERY_BAD,
		WORST,
		WOUND_RECEIVED,
		WOUND_APPLIED
	};
}

#endif