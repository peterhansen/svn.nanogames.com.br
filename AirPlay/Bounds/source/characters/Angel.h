/*
 *  Angel.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_ANGEL_H
#define BOUNDS_ANGEL_H

#include "ICharacter.h"

namespace bounds
{
	class Angel : public ICharacter
	{
		public:
			Angel( CharacterId id );
			virtual ~Angel(){};

			///<summary>Retorna a imagem da cerca do personagem</summary>
			virtual void loadFenceImg( LineOrientation orientation, shared_ptr< Renderable > pOutFenceImg, shared_ptr< Renderable > pOutFenceShadowImg );

			///<summary>Retorna as imagens que podem ser aplicadas a uma zona 1x1 possu�da pelo personagem</summary>
			virtual vector< shared_ptr< Renderable > >& loadSmallZoneImgs( vector< shared_ptr< Renderable > >& rOutSmallZoneImgs );

			///<summary>Retorna as imagens que podem ser aplicadas a uma zona 2x2 possu�da pelo personagem</summary>
			virtual vector< shared_ptr< Renderable > >& loadBigZoneImgs( vector< shared_ptr< Renderable > >& rOutBigZoneImgs );

			///<summary>Retorna a imagem referente a uma express�o do personagem</summary>
			virtual shared_ptr< Renderable > loadCharacterExpressionImg( CharacterExpression expression );

			///<summary>Retorna a imagem que deve ser exibida na tela de informa��es sobre o personagem</summary>
			virtual shared_ptr< Renderable > loadAboutImg();

			///<summary>Retorna a imagem que deve ser exibida na tela de sele��o de personagens</summary>
			virtual shared_ptr< Renderable > loadSelectionImg();

			///<summary>Retorna a imagem que deve ser exibida quando o personagem ganha uma partida</summary>
			virtual shared_ptr< Renderable > loadWonImg();

			///<summary>Retorna o objeto respons�vel por carregar as imagens do cen�rio do personagem
			///<returns>Retorna o objeto respons�vel por carregar as imagens do cen�rio do personagem ou NULL em casos de erro</returns>
			///</summary>
			virtual shared_ptr< IStage > getStage();
	};
} // namespace bounds

#endif