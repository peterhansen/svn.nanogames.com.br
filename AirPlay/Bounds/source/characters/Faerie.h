/*
 *  Faerie.h
 *  Bounds
 *
 *  Created by Max on 02/01/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_FAERIE_H
#define BOUNDS_FAERIE_H

#include "ICharacter.h"

namespace bounds
{
	class Faerie : public ICharacter
	{
		public:
			Faerie( uint32 id );
			virtual ~Faerie();

			virtual Renderable* getLineImg( LineOrientation orientation );

			virtual Renderable* getZoneImg( uint8 zoneWidth, uint8 zoneHeight );

			virtual Renderable* getCharacterFace( CharacterExpression expression );

			virtual bool loadData();
			virtual void unloadData();

		private:
			// TODO
	};
} // namespace bounds

#endif