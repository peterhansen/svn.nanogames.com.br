/*
 *  CharacterFactory.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 02/10/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_CHARACTER_FACTORY_H
#define BOUNDS_CHARACTER_FACTORY_H

// C++
#include <vector>
#include "shared_ptr.hpp"

// Game
#include "ICharacter.h"

namespace bounds
{
	// Tipos utilizados pela classe
	// http://www.drdobbs.com/184401839;jsessionid=ZYRW35PNOCERPQE1GHPCKHWATMY32JVN
	typedef boost::shared_ptr< ICharacter > CharacterPointer;
	typedef std::vector< CharacterPointer > CharactersCollection;

	class CharacterFactory
	{
		public:
			///<sumary>Cria a �nica inst�ncia da classe</sumary>
			static void Create();

			///<sumary>Destr�i a �nica inst�ncia da classe</sumary>
			static void Destroy();

			///<sumary>Retorna a �nica inst�ncia desta classe</sumary>
			static const CharacterFactory* GetInstance();

			///<sumary>Retorna um array contendo todos os personagens dispon�veis no jogo</sumary>
			CharactersCollection& getCharacters( CharactersCollection& rOutCharactersVec ) const;

			///<sumary>Retorna o personagem cujo id foi passado como par�metro</sumary>
			ICharacter* getCharacter( CharacterId id ) const;

		private:
			// Singleton
			CharacterFactory();
			~CharacterFactory();

			///<sumary>ponteiro para a �nica inst�ncia da classe</sumary>
			// TODO: Usar smart pointer!!!!!!!!!!!! Ver implementa��o de singleton de Herb Sutter atrav�s de vari�veis est�ticas!!!
			static CharacterFactory* pSingleton;
	
			///<sumary>Array contendo todos os personagens dispon�veis no jogo</sumary>
			CharactersCollection characters;
	};
} // namespace bounds


#endif
