/*
 *  Angel.cpp
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#include "Angel.h"

namespace bounds
{

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

Angel::Angel( CharacterId id ) : ICharacter( id, "Narcisius" )
{
}

/*===============================================================================================================

METHOD loadFenceImg
	Retorna a imagem da cerca do personagem.

===============================================================================================================*/

void Angel::loadFenceImg( LineOrientation orientation, shared_ptr< Renderable > pOutFenceImg, shared_ptr< Renderable > pOutFenceShadowImg )
{
}

/*===============================================================================================================

METHOD loadSmallZoneImgs
	Retorna as imagens que podem ser aplicadas a uma zona 1x1 possu�da pelo personagem.

===============================================================================================================*/

vector< shared_ptr< Renderable > >& Angel::loadSmallZoneImgs( vector< shared_ptr< Renderable > >& rOutSmallZoneImgs )
{
}

/*===============================================================================================================

METHOD loadBigZoneImgs
	Retorna as imagens que podem ser aplicadas a uma zona 2x2 possu�da pelo personagem.

===============================================================================================================*/

vector< shared_ptr< Renderable > >& Angel::loadBigZoneImgs( vector< shared_ptr< Renderable > >& rOutBigZoneImgs )
{
}

/*===============================================================================================================

METHOD loadCharacterExpressionImg
	Retorna a imagem referente a uma express�o do personagem.

===============================================================================================================*/

shared_ptr< Renderable > Angel::loadCharacterExpressionImg( CharacterExpression expression )
{
}

/*===============================================================================================================

METHOD loadAboutImg
	Retorna a imagem que deve ser exibida na tela de informa��es sobre o personagem.

===============================================================================================================*/

shared_ptr< Renderable > Angel::loadAboutImg()
{
}

/*===============================================================================================================

METHOD loadSelectionImg
	Retorna a imagem que deve ser exibida na tela de sele��o de personagens.

===============================================================================================================*/

shared_ptr< Renderable > Angel::loadSelectionImg()
{
}

/*===============================================================================================================

METHOD loadWonImg
	Retorna a imagem que deve ser exibida quando o personagem ganha uma partida.

===============================================================================================================*/

shared_ptr< Renderable > Angel::loadWonImg()
{
}

/*===============================================================================================================

METHOD getStage
	Retorna o objeto respons�vel por carregar as imagens do cen�rio do personagem ou NULL em casos de erro.

===============================================================================================================*/

shared_ptr< IStage > Angel::getStage()
{
}

} // namespace bounds