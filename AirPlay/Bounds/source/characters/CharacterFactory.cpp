/*
 *  CharacterFactory.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 02/10/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

// Game
#include "CharacterFactory.h"

// Game - Available characters
#include "Oger.h"
#include "Faerie.h"

#include "Farmer.h"
#include "Engineer.h"

#include "Angel.h"
#include "Devil.h"

namespace bounds
{

/*===============================================================================================================

STATIC ATTRIBUTES

===============================================================================================================*/

CharacterFactory* CharacterFactory::pSingleton = NULL;

/*===============================================================================================================

STATIC METHOD Create
	Cria a única instância da classe.

===============================================================================================================*/

void CharacterFactory::Create()
{
	if( pSingleton == NULL )
	{
		pSingleton = new CharacterFactory();

		pSingleton->characters.push_back( CharacterPointer( new Angel() ) );
		pSingleton->characters.push_back( CharacterPointer( new Devil() ) );

		pSingleton->characters.push_back( CharacterPointer( new Farmer() ) );
		pSingleton->characters.push_back( CharacterPointer( new Engineer() ) );

		pSingleton->characters.push_back( CharacterPointer( new Oger() ) );
		pSingleton->characters.push_back( CharacterPointer( new Faerie() ) );
	}
}

/*===============================================================================================================

STATIC METHOD Destroy
	Destrói a única instância da classe.

===============================================================================================================*/

void CharacterFactory::Destroy()
{
	delete pSingleton;
}

/*===============================================================================================================

STATIC METHOD GetInstance
	Retorna a única instância desta classe.

===============================================================================================================*/

const CharacterFactory* CharacterFactory::GetInstance()
{
	return pSingleton;
}

/*===============================================================================================================

METHOD getCharacters
	Retorna um array contendo todos os personagens disponíveis no jogo.

===============================================================================================================*/

CharactersCollection& CharacterFactory::getCharacters( CharactersCollection& rOutCharactersVec ) const
{
	return rOutCharactersVec;
}

/*===============================================================================================================

METHOD getCharacter
	Retorna o personagem cujo id foi passado como parâmetro.

===============================================================================================================*/

ICharacter* CharacterFactory::getCharacter( CharacterId id ) const
{
	CharactersCollection::const_iterator end = characters.end();
	for( CharactersCollection::const_iterator it = characters.begin() ; it != end ; ++it )
	{
		if( it.id() == id )
			return it;
	}
	return NULL;
}

} // namespace bounds


