/*
 *  Devil.cpp
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#include "Devil.h"

// AirPlay
#include "IwResManager.h"

// Game
#include "DevilStage.h"

namespace bounds
{

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

Devil::Devil( CharacterId id ) : ICharacter( id )
{
	// Carrega o grupo de recursos deste personagem
	IwGetResManager()->LoadGroup( "devil.group" );
}

/*===============================================================================================================

DESTRUCTOR

===============================================================================================================*/

Devil::~Devil()
{
	// Carrega o grupo de recursos deste personagem
	IwGetResManager()->DestroyGroup( "devil.group" );
}

/*===============================================================================================================

METHOD loadFenceImg
	Retorna a imagem da cerca do personagem.

===============================================================================================================*/

void Devil::loadFenceImg( LineOrientation orientation, shared_ptr< Renderable > pOutFenceImg, shared_ptr< Renderable > pOutFenceShadowImg )
{
	// TODO
	//Iw2DCreateImageResource();
}

/*===============================================================================================================

METHOD loadSmallZoneImgs
	Retorna as imagens que podem ser aplicadas a uma zona 1x1 possu�da pelo personagem.

===============================================================================================================*/

vector< shared_ptr< Renderable > >& Devil::loadSmallZoneImgs( vector< shared_ptr< Renderable > >& rOutSmallZoneImgs )
{
}

/*===============================================================================================================

METHOD loadBigZoneImgs
	Retorna as imagens que podem ser aplicadas a uma zona 2x2 possu�da pelo personagem.

===============================================================================================================*/

vector< shared_ptr< Renderable > >& Devil::loadBigZoneImgs( vector< shared_ptr< Renderable > >& rOutBigZoneImgs )
{
}

/*===============================================================================================================

METHOD loadCharacterExpressionImg
	Retorna a imagem referente a uma express�o do personagem.

===============================================================================================================*/

shared_ptr< Renderable > Devil::loadCharacterExpressionImg( CharacterExpression expression )
{
}

/*===============================================================================================================

METHOD loadAboutImg
	Retorna a imagem que deve ser exibida na tela de informa��es sobre o personagem.

===============================================================================================================*/

shared_ptr< Renderable > Devil::loadAboutImg()
{
}

/*===============================================================================================================

METHOD loadSelectionImg
	Retorna a imagem que deve ser exibida na tela de sele��o de personagens.

===============================================================================================================*/

shared_ptr< Renderable > Devil::loadSelectionImg()
{
}

/*===============================================================================================================

METHOD loadWonImg
	Retorna a imagem que deve ser exibida quando o personagem ganha uma partida.

===============================================================================================================*/

shared_ptr< Renderable > Devil::loadWonImg()
{
}

/*===============================================================================================================

METHOD getStage
	Retorna o objeto respons�vel por carregar as imagens do cen�rio do personagem ou NULL em casos de erro.

===============================================================================================================*/

shared_ptr< IStage > Devil::getStage()
{
	return shared_ptr< IStage >( new DevilStage() );
}

/*===============================================================================================================

METHOD getName
	Retorna o nome do personagem.

===============================================================================================================*/

string& Devil::getName( string& rOutName ) const
{
	// TODO : Obter de um arquivo de textos!!!
	rOutName.assign( "Borr" );
	return rOutName;
}

/*===============================================================================================================

METHOD getAboutText
	Retorna o texto informativo sobre o personagem.

===============================================================================================================*/

string& Devil::getAboutText( string& rOutText ) const
{
	// TODO : Obter de um arquivo de textos!!!
	rOutText.assign( "Texto informativo sobre Borr" );
	return rOutText;
}

} // namespace bounds