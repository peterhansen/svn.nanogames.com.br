/*
 *  ICharacter.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_ICHARACTER_H
#define BOUNDS_ICHARACTER_H

// C++
#include <string>
#include <vector>
#include <inttypes.h>

// Boost (AirPlay ainda n�o aponta o header <memory> atualizado, o qual torna shared_ptr um recurso padr�o da linguagem)
#include "shared_ptr.hpp"

// Components
#include "Renderable.h"

// Bounds
#include "CharacterExpression.h"
#include "LineOrientation.h"
#include "IStage.h"

namespace bounds
{
	// Resolu��es de escopo para termos que escrever menos
	using std::string;
	using std::vector;
	using ngc::Renderable;
	using boost::shared_ptr;

	///<summary>Tipo do identificador �nico do personagem</summary>
	typedef uint8 CharacterId;

	class ICharacter
	{
		public:
			virtual ~ICharacter(){};

			///<summary>Retorna a imagem da cerca do personagem</summary>
			virtual void loadFenceImg( LineOrientation orientation, shared_ptr< Renderable > pOutFenceImg, shared_ptr< Renderable > pOutFenceShadowImg ) = 0;

			///<summary>Retorna as imagens que podem ser aplicadas a uma zona 1x1 possu�da pelo personagem</summary>
			virtual vector< shared_ptr< Renderable > >& loadSmallZoneImgs( vector< shared_ptr< Renderable > >& rOutSmallZoneImgs ) = 0;

			///<summary>Retorna as imagens que podem ser aplicadas a uma zona 2x2 possu�da pelo personagem</summary>
			virtual vector< shared_ptr< Renderable > >& loadBigZoneImgs( vector< shared_ptr< Renderable > >& rOutBigZoneImgs ) = 0;

			///<summary>Retorna a imagem referente a uma express�o do personagem</summary>
			virtual shared_ptr< Renderable > loadCharacterExpressionImg( CharacterExpression expression ) = 0;

			///<summary>Retorna a imagem que deve ser exibida na tela de informa��es sobre o personagem</summary>
			virtual shared_ptr< Renderable > loadAboutImg() = 0;

			///<summary>Retorna a imagem que deve ser exibida na tela de sele��o de personagens</summary>
			virtual shared_ptr< Renderable > loadSelectionImg() = 0;

			///<summary>Retorna a imagem que deve ser exibida quando o personagem ganha uma partida</summary>
			virtual shared_ptr< Renderable > loadWonImg() = 0;

			///<summary>Carrega as imagens referentes ao cen�rio do personagem. Estas imagens n�o s�o carregadas em loadData porque, no modo versus, o jogador
			///pode escolher jogar em um cen�rio que n�o pertence aos personagens envolvidos na partida. O programador fica respons�vel por deletar o ponteiro
			///retornado
			///<returns>Retorna o objeto que cont�m as informa��es do cen�rio do personagem ou NULL em casos de erro</returns>
			///</summary>
			virtual shared_ptr< IStage > getStage() = 0;

			///<summary>Retorna o nome do personagem</summary>
			virtual string& getName( string& rOutName ) const = 0;

			///<summary>Retorna o texto informativo sobre o personagem</summary>
			virtual string& getAboutText( string& rOutText ) const = 0;

			///<summary>Retorna a identifica��o �nica do personagem</summary>
			inline uint32 getId() const { return characterId; };

		protected:
			explicit ICharacter( CharacterId id ) : characterId( id ){};

		private:
			///<summary>Identifica��o �nica do personagem</summary>
			CharacterId characterId;
	};
} // namespace Bounds


#endif
