/*
 *  Engineer.h
 *  Bounds
 *
 *  Created by Max on 02/01/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_ENGINEER_H
#define BOUNDS_ENGINEER_H

#include "ICharacter.h"

namespace bounds
{
	class Engineer : public ICharacter
	{
		public:
			Engineer( uint32 id );
			virtual ~Engineer();

			virtual Renderable* getLineImg( LineOrientation orientation );

			virtual Renderable* getZoneImg( uint8 zoneWidth, uint8 zoneHeight );

			virtual Renderable* getCharacterFace( CharacterExpression expression );

			virtual bool loadData();
			virtual void unloadData();

		private:
			// TODO
	};
} // namespace bounds

#endif