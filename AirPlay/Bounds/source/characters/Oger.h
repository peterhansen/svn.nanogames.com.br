/*
 *  Oger.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_OGER_H
#define BOUNDS_OGER_H

#include "ICharacter.h"

namespace bounds
{
	class Oger : public ICharacter
	{
		public:
			Oger( uint32 id );
			virtual ~Oger();

			virtual Renderable* getLineImg( LineOrientation orientation );

			virtual Renderable* getZoneImg( uint8 zoneWidth, uint8 zoneHeight );

			virtual Renderable* getCharacterFace( CharacterExpression expression );

			virtual bool loadData();
			virtual void unloadData();

		private:
			// TODO
	};
} // namespace bounds

#endif