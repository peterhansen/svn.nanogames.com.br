#include "s3e.h"
#include "s3eSound.h"
#include "Iw2D.h"
#include "IwGx.h"
#include "IwResManager.h"
#include "IwSoundManager.h"
#include "IwSound.h"
#include "IwSoundInst.h"
#include "IwSoundSpec.h"

#include <memory.h>

// Definições
#define NANO_LOGO_WIDTH ( ( int16 )214 )
#define NANO_LOGO_HEIGHT ( ( int16 )197 )

// Globais
static CIw2DImage* pLogoImg = NULL;
static CIw2DFont* pLogoFont = NULL;

// Sons
enum
{
	// Menu sounds
	SFX_THEME,

	SFX_MAX,
};

CIwSoundSpec* g_SoundSpecs[SFX_MAX];

// Macro para iniciar a reprodução de um som
#define PLAY_SOUND(a)	g_SoundSpecs[SFX_##a]->Play()

//--------------------------------------------------------------------------
void ExampleInit()
{
	// Initialise modules
	Iw2DInit();
	IwResManagerInit();
	IwSoundInit();

	// Load resources
	IwGetResManager()->LoadGroup( "rsc.group" );

	// Logo
	pLogoImg = Iw2DCreateImageResource( "nanologo" );

	// Copyright
	pLogoFont = Iw2DCreateFontResource( "font" );
	Iw2DSetFont( pLogoFont );

	// The resource manager owns its handlers, so the user does not need to destroy them. 
	IwGetResManager()->AddHandler( new CIwResHandlerWAV() );
	CIwResGroup* pSndGroup = IwGetResManager()->LoadGroup( "snd.group" );

	// Music
	g_SoundSpecs[SFX_THEME] = ( CIwSoundSpec* )pSndGroup->GetResNamed( "music", IW_SOUND_RESTYPE_SPEC );
	PLAY_SOUND( THEME );

	// OLD
    //s3eFile *fileHandle = s3eFileOpen( "music.raw", "rb" );
    //int32 g_FileSize = s3eFileGetSize( fileHandle );
    //g_SoundBuffer = ( int16* )s3eMallocBase( g_FileSize );
    //memset( g_SoundBuffer, 0, g_FileSize );
    //s3eFileRead( g_SoundBuffer, g_FileSize, 1, fileHandle ); 
    //s3eFileClose( fileHandle );

    //// Finds a free channel that we can use to play our raw file on.
    //g_Channel = s3eSoundGetFreeChannel();

    //// Setting default frequency at which all channels will play at, in Hz.
    //s3eSoundSetInt( S3E_SOUND_DEFAULT_FREQ, 8000 );
}

//--------------------------------------------------------------------------
void ExampleShutDown()
{
	delete pLogoFont;
	delete pLogoImg;

	// destroy sound group
	IwGetResManager()->DestroyGroup( "snd" );
	IwGetResManager()->DestroyGroup( "rsc" );

	// OLD
	// s3eFreeBase( g_SoundBuffer );

	// Terminate system modules
	IwSoundTerminate();
	IwResManagerTerminate();
	Iw2DTerminate();
}

//--------------------------------------------------------------------------
bool ExampleUpdate( int timeElapsedMs )
{
	IwGetSoundManager()->Update();
	return true;
}

//--------------------------------------------------------------------------
void ExampleUpdateInput( int timeElapsedMs )
{
	s3ePointerUpdate();
	s3eKeyboardUpdate();
}

//--------------------------------------------------------------------------
void ExampleRender()
{
	Iw2DSurfaceClear( 0x00000000 );

	int displayWidth  = Iw2DGetSurfaceWidth();
	int displayHeight = Iw2DGetSurfaceHeight();

	// Draw title logo centered on screen
	int x = ( displayWidth - NANO_LOGO_WIDTH ) >> 1;
	int y = ( displayHeight - NANO_LOGO_HEIGHT ) >> 1;

	// Draw a sprite at the specified position, using the specified material.
	// The size of the sprite is taken from the size of the texture.
	Iw2DDrawImage( pLogoImg, CIwSVec2( x, y ), CIwSVec2( NANO_LOGO_WIDTH, NANO_LOGO_HEIGHT ) );

	// Render the text into a 100x100 region
	int fontHeight = pLogoFont->GetHeight();
	CIwSVec2 textRegion( displayWidth, fontHeight );
    
    // Centred on the centre of the surface
	CIwSVec2 topLeft( ( int16 )( ( displayWidth - textRegion.x ) >> 1 ) , ( int16 )( displayHeight - fontHeight ) );

    // Draw the string into the region
	//Iw2DDrawString( ( uint16* )( L"© Nano Games Ltda. All Rights Reserved." ), topLeft, textRegion, IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_CENTRE );
	Iw2DDrawString( ( uint16* )( L"Nano Games Ltda. All Rights Reserved." ), topLeft, textRegion, IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_CENTRE );
}

//--------------------------------------------------------------------------
void ExampleSwapBuffers()
{
	// Present the rendered surface to the screen
	Iw2DSurfaceShow();
}