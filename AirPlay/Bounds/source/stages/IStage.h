/*
 *  IStage.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_ISTAGE_H
#define BOUNDS_ISTAGE_H

// C++
#include <string>

// Boost (AirPlay ainda n�o aponta o header <memory> atualizado, o qual torna shared_ptr um recurso padr�o da linguagem)
#include "shared_ptr.hpp"

// AirPlay
#include "IwColour.h"

// Components
#include "Renderable.h"

// Game
#include "LineOrientation.h"

namespace bounds
{
	// Resolu��es de escopo para termos que escrever menos
	using std::string;
	using ngc::Renderable;
	using boost::shared_ptr;

	class IStage
	{
		public:
			virtual ~IStage(){};

			///<summary>Retorna a imagem de plano de fundo da fase</summary>
			virtual shared_ptr< Renderable > getBackgroundImg() = 0;

			///<summary>Retorna a imagem do ponto utilizado na fase</summary>
			virtual shared_ptr< Renderable > getDotImg() = 0;

			///<summary>Retorna a imagem da linha utilizada na fase</summary>
			virtual shared_ptr< Renderable > getLineImg( LineOrientation orientation ) = 0;

			///<summary>Retorna o nome da fase</summary>
			virtual string& getStageName( string& rOutName ) const = 0;

			///<summary>Retorna a cor utilizada para tingir os componentes de interface quando estamos utilizando este cen�rio</summary>
			inline CIwColour getTintColor() const { return tintColor; };

		protected:
			explicit IStage( CIwColour tintColor ) : tintColor( tintColor ){};

		private:
			///<summary>Cor utilizada para tingir os componentes de interface quando estamos utilizando este cen�rio</summary>
			CIwColour tintColor;
	};
}

#endif
