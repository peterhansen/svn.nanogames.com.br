/* 
 *  DevilStage.h
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef BOUNDS_DEVIL_STAGE_H
#define BOUNDS_DEVIL_STAGE_H

// Game
#include "IStage.h"

namespace bounds
{
	class DevilStage : public IStage
	{
		public:
			DevilStage();
			virtual ~DevilStage();

			///<summary>Retorna a imagem de plano de fundo da fase</summary>
			virtual shared_ptr< Renderable > getBackgroundImg();

			///<summary>Retorna a imagem do ponto utilizado na fase</summary>
			virtual shared_ptr< Renderable > getDotImg();

			///<summary>Retorna a imagem da linha utilizada na fase</summary>
			virtual shared_ptr< Renderable > getLineImg( LineOrientation orientation );

			///<summary>Retorna o nome da fase</summary>
			virtual string& getStageName( string& rOutName ) const;
	};
}

#endif