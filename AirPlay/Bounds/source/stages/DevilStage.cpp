/*
 *  DevilStage.cpp
 *  Bounds
 *
 *  Created by Daniel L. Alves on 18/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#include "DevilStage.h"

namespace bounds
{

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

DevilStage::DevilStage() : IStage( CIwColour( 0xF2A359 ) )
{
}

/*===============================================================================================================

DESTRUCTOR

===============================================================================================================*/

DevilStage::~DevilStage()
{
}

/*===============================================================================================================

METHOD getBackgroundImg
	Retorna a imagem de plano de fundo da fase.

===============================================================================================================*/

shared_ptr< Renderable > DevilStage::getBackgroundImg()
{
}

/*===============================================================================================================

METHOD getDotImg
	Retorna a imagem do ponto utilizado na fase.

===============================================================================================================*/

shared_ptr< Renderable > DevilStage::getDotImg()
{
}

/*===============================================================================================================

METHOD getLineImg
	Retorna a imagem da linha utilizada na fase.

===============================================================================================================*/

shared_ptr< Renderable > DevilStage::getLineImg( LineOrientation orientation )
{
}

/*===============================================================================================================

METHOD getStageName
	Retorna o nome da fase.

===============================================================================================================*/

string& DevilStage::getStageName( string& rOutName ) const
{
	// TODO : Obter de um arquivo de textos!!!
	rOutName.assign( "Borr" );
	return rOutName;
}

} // namespace bounds