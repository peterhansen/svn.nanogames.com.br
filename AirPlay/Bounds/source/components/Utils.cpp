/*
 *  Utils.cpp
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

namespace ngc
{

CIwMat2D& Utils::ScaleMatrix( const CIwFVec2& scale, CIwMat2D& rOutMtx )
{
}

CIwMat2D& Utils::ConvertCIwMatToCIwMat2D( const CIwMat& rInMtx, CIwMat2D& rOutMtx )
{
}

} // namespace ngc

//iwfixed  m [3][3] 
//CIwVec3  t 
//
//Detailed Description
//CIwMat - Combines a 3x3 rotation and 3-vector translation.
//
//Note:
//The transformation represented by m is normally a rotation, for this reason m will be referred to as a rotation within the following documentation.
//0  1  2  3   [0][0]  [0][1]  [0][2]  
//4  5  6  7  ==>  [1][0]  [1][1]  [1][2]  
//8  9  10  11   [2][0]  [2][1]  [2][2]  
//12  13  14  15   t.x  t.y  t.z 
//
//
//Detailed Description
//CIwMat2D - Combines a 2x2 rotation and 2-vector translation.
//
//Note:
//The transformation represented by m is normally a rotation, for this reason m will be referred to as a rotation within the following documentation.
//0  1  2  3   [0][0]  [0][1]  [0][2]  
//4  5  6  7  ==>  [1][0]  [1][1]  [1][2]  
//8  9  10  11   [2][0]  [2][1]  [2][2]  
//12  13  14  15   t.x  t.y  t.z  

