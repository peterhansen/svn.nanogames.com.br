/*
 *  Renderable.h
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2009 Nano Games. All rights reserved.
 */

#ifndef NGC_RENDERABLE_H
#define NGC_RENDERABLE_H

// AirPlay
#include <Iw2D.h>
#include <IwGeomVec2.h>
#include <IwUIRect.h>

// Components
#include "MatrixStack.h"

namespace ngc
{
	enum FlipOp
	{
		FLIP_OP_NONE = 0x00,
		FLIP_OP_HOR  = 0x01,
		FLIP_OP_VER  = 0x02,
		FLIP_OP_BOTH = ( FLIP_OP_HOR | FLIP_OP_VER )
	};

	class Renderable
	{
		public:
			virtual ~Renderable(){};

			///<summary>Determina a posi��o do objeto</summary>
			inline void setPosition( CIwSVec2 newPos ){ pos = newPos; };

			///<summary>Retorna a posi��o do objeto</summary>
			inline CIwSVec2 getPosition() const { return pos; };

			///<summary>Determina a coordenada de refer�ncia utilizada para o posicionamento do objeto</summary>
			inline void setAnchor( CIwSVec2 newAnchor ){ anchor = newAnchor; };

			///<summary>Retorna a coordenada de refer�ncia utilizada para o posicionamento do objeto</summary>
			inline CIwSVec2 getAnchor() const { return anchor; };

			///<summary>Determina a posi��o do objeto a partir da �ncora</summary>
			inline void setPositionByAnchor( CIwSVec2 newPos ){ setPosition( CIwSVec2( newPos.x - anchor.x, newPos.y - anchor.y ) ); };

			///<summary>Retorna a posi��o do objeto a partir da �ncora</summary>
			inline CIwSVec2 getPositionByAnchor() const { return getPosition() + anchor; };

			///<summary>Movimenta o objeto a partir de sua posi��o atual</summary>
			inline void move( CIwSVec2 movement ){ setPosition( pos + movement ); };

			///<summary>Determina o tamanho do objeto</summary>
			inline void setSize( CIwSVec2 newSize ){ size = newSize; };

			///<summary>Retorna o tamanho do objeto</summary>
			inline CIwSVec2 getSize() const { return size; };

			///<summary>Determina o tamanho do objeto</summary>
			inline void setScale( const CIwFVec2& newScale ){ scale = newScale; };

			///<summary>Retorna o tamanho do objeto</summary>
			inline CIwFVec2& getScale( CIwFVec2& rOutScale ) const { rOutScale = scale; return rOutScale; };
	
			///<summary>Determina se o objeto est� vis�vel. O valor default � true</summary>
			inline void setVisible( bool b ){ visible = b; };
	
			///<summary>Retorna se o objeto est� vis�vel</summary>
			inline bool isVisible() const { return visible; };

			///<summary>Determina o fator de transpar�ncia do objeto. O valor default � 255 (totalmente opaco)</summary>
			inline void setAlpha( uint8 a ){ tintColor.a = a; };

			///<summary>Retorna o fator de transpar�ncia do objeto</summary>
			inline uint8 getAlpha() const { return tintColor.a; };

			///<summary>Determina o modo de alpha a ser utilizado em opera��es de renderiza��o deste objeto. O valor default � IW_2D_ALPHA_NONE</summary>
			inline void setAlphaMode( CIw2DAlphaMode mode ){ alphaMode = mode; };

			///<summary>Retorna o modo de alpha a ser utilizado em opera��es de renderiza��o deste objeto</summary>
			inline CIw2DAlphaMode getAlphaMode() const { return alphaMode; };

			///<summary>Determina a cor a ser utilizada nas opera��es de modula��o. O valor default � branco opaco</summary>
			inline void setTintColor( CIwColour color ){ tintColor = color; };

			///<summary>Retorna a cor a ser utilizada nas opera��es de modula��o</summary>
			inline CIwColour getTintColor() const { return tintColor; };

			///<summary>Determina a �rea da tela na qual o objeto est� vis�vel. O valor default � a tela inteira</summary>
			inline void setViewport( const CIwUIRect& rNewViewport ){ viewport = rNewViewport; };
	
			///<summary>Retorna a �rea da tela na qual o objeto est� vis�vel</summary>
			inline CIwUIRect& getViewport( CIwUIRect& rOutViewport ) const { rOutViewport = viewport; return rOutViewport; };

			///<summary>Determina a rota��o, em graus, que deve ser aplicada ao objeto</summary>
			inline void setRotation( int16 degrees ){ rotDegrees = degrees; };

			///<summary>Retorna a rota��o, em graus, que est� sendo aplicada ao objeto</summary>
			inline int16 getRotation() const { return rotDegrees; };

			///<summary>Determina o eixo de rota��o do objeto</summary>
			inline void setRotationCenter( CIwSVec2 center ){ rotCenter = center; };

			///<summary>Retorna o eixo de rota��o do objeto</summary>
			inline CIwSVec2 getRotationCenter() const { return rotCenter; };

			///<summary>Determina as opera��es de espelhamento aplicadas ao objeto</summary>
			inline void setFlipOp( FlipOp op ){ flipOp = op; };

			///<summary>Determina as opera��es de espelhamento aplicadas ao objeto</summary>
			inline FlipOp getFlipOp() const { return flipOp; };

			///<summary>Renderiza o objeto</summary>
			virtual void render(){};

		protected:
			Renderable();

			///<summary>Retorna a matriz de transforma��o que deve ser aplicada ao objeto</summary>
			virtual MatrixStack::MatrixType& getTransformMatrix( MatrixStack::MatrixType& rOutMtx );

		private:
			///<summary>Posi��o do objeto no mundo</summary>
			CIwSVec2 pos;

			///<summary>Coordenada de refer�ncia utilizada para o posicionamento do objeto</summary>
			CIwSVec2 anchor;

			///<summary>Tamanho do objeto</summary>
			CIwSVec2 size;

			///<summary>Tamanho do objeto</summary>
			CIwSVec2 rotCenter;

			///<summary>Indica se o objeto est� vis�vel ou n�o</summary>
			bool visible;

			///<summary>Modo de alpha a ser utilizado em opera��es de renderiza��o deste objeto</summary>
			CIw2DAlphaMode alphaMode;

			///<summary>Tipo do espelhamento aplicado ao objeto</summary>
			FlipOp flipOp;

			///<summary>Rota��o, em graus, aplicada ao objeto</summary>
			int16 rotDegrees;

			///<summary>Cor a ser utilizada nas opera��es de modula��o</summary>
			CIwColour tintColor;

			///<summary>�rea da tela na qual o objeto est� vis�vel</summary>
			CIwUIRect viewport;

			///<summary>Fator de escala do objeto</summary>
			CIwFVec2 scale;
	};

} // namespace ngc

#endif