/*
 *  RenderableGroup.cpp
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#include "RenderableGroup.h"

// C++
#include <algorithm>
#include <iterator>
#include <stdexcept>

// AirPlay
#include "IwDebug.h"

// NGC
#include "MatrixStack.h"

// Defini��es �teis. Essas strings ir�o sumir na vers�o release. Logo, n�o precisamos nos preocuparmos com as repeti��es causadas pelas macros
#define ASSERT_CHILD_NOT_NULL( child ) IwAssertMsg( NGC, child != NULL, ( "Child cannot be null" ) )
#define ASSERT_INDEX_POSITIVE( index ) IwAssertMsg( NGC, index >= 0, ( "Invalid index %d: Indexes should be positive", index ) )
#define ASSERT_INDEX_LESSER_THAN_N( index ) IwAssertMsg( NGC, index < static_cast< ChildIndexType >( getNChildren() ), ( "Invalid index %d: There are only %d children in the group", index, getNChildren() ) )

namespace ngc
{

// Resolu��es de escopo para termos que escrever menos
using namespace std;

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

RenderableGroup::RenderableGroup() : Renderable(), children()
{
}

/*===============================================================================================================

DESTRUCTOR

===============================================================================================================*/

RenderableGroup::~RenderableGroup()
{
}

/*===============================================================================================================

METHOD addChild

===============================================================================================================*/

RenderableGroup::ChildIndexType RenderableGroup::addChild( ChildPtrType pChild )
{
	return addChild( pChild, getNChildren() );
}

/*===============================================================================================================

METHOD addChild

===============================================================================================================*/

RenderableGroup::ChildIndexType RenderableGroup::addChild( ChildPtrType pChild, ChildIndexType index )
{
	ASSERT_CHILD_NOT_NULL( pChild );
	ASSERT_INDEX_POSITIVE( index );

	ContainerType::iterator at;
	if( index < static_cast< ChildIndexType >( getNChildren() ) )
	{
		at = children.begin();
		advance( at, index );
	}
	else
	{
		index = getNChildren();
		at = children.end();
	}
	children.insert( at, pChild );

	return index;
}

/*===============================================================================================================

METHOD getChild

===============================================================================================================*/

RenderableGroup::ChildPtrType RenderableGroup::getChild( ChildIndexType index )
{
	ASSERT_INDEX_POSITIVE( index );
	ASSERT_INDEX_LESSER_THAN_N( index );

	ContainerType::iterator it = children.begin();
	advance( it, index );
	return *it;
}

/*===============================================================================================================

METHOD getChildIndex

===============================================================================================================*/

RenderableGroup::ChildIndexType RenderableGroup::getChildIndex( ChildPtrType pChild ) const
{
	ASSERT_CHILD_NOT_NULL( pChild );

	ChildIndexType index = -1, count = 0;
	ContainerType::const_iterator end = children.end();
	for( ContainerType::const_iterator it = children.begin() ; it != end ; ++it )
	{
		if( ( *it ) == pChild )
		{
			index = count;
			break;
		}
		++count;
	}
	return index;
}

/*===============================================================================================================

METHOD removeChildren

===============================================================================================================*/

void RenderableGroup::removeChildren()
{
	if( getNChildren() )
	{
		children.clear();

		// Garante que iremos desalocar toda a mem�ria alocada pelo vector
		ContainerType c;
		children.swap( c );
	}
}

/*===============================================================================================================

METHOD removeChild

===============================================================================================================*/

RenderableGroup::ChildPtrType RenderableGroup::removeChild( ChildPtrType pChild )
{
	ASSERT_CHILD_NOT_NULL( pChild );

	// Construtor padr�o de shared_ptr equivale a NULL
	RenderableGroup::ChildPtrType ret;

	ContainerType::iterator end = children.end();
	ContainerType::iterator it = find( children.begin(), end, pChild );
	if( it != end )
	{
		ret = *it;
		children.erase( it );
	}
	return ret;
}

/*===============================================================================================================

METHOD removeChild

===============================================================================================================*/

RenderableGroup::ChildPtrType RenderableGroup::removeChild( ChildIndexType index )
{
	ASSERT_INDEX_POSITIVE( index );
	ASSERT_INDEX_LESSER_THAN_N( index );

	// Construtor padr�o de shared_ptr equivale a NULL
	RenderableGroup::ChildPtrType ret;

	ContainerType::iterator it = children.begin();
	advance( it, index );

	ret = *it;
	children.erase( it );

	return ret;
}

/*===============================================================================================================

METHOD render

===============================================================================================================*/

void RenderableGroup::render( void )
{
	if( isVisible() && ( getAlpha() > 0 ) && ( getNChildren() > 0 ) )
	{
		// TODO
		//CIwUIRect vp;
		//getViewport( vp );
		//if( ( vp.w > 0 ) && ( vp.h > 0 ) && vp.Intersects( CIwUIRect( CIwRect32( 0, 0, Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight() ))) )
		{
			// Monta a matriz de transforma��o
			MatrixStack::MatrixType mtx;
			MatrixStack::PushMatrix( getTransformMatrix( mtx ) );

			// TODO : Cagado!!! N�o est� levando em conta o viewport do pai! Seria legal se tiv�ssemos um ViewportStack que vai dando intersect!!!
			CIwRect prevViewport = IwGxGetScissorScreenSpace();
			IwGxSetScissorScreenSpace( vp.x, vp.y, vp.w, vp.h );

			CIw2DAlphaMode prevAlphaMode = Iw2DGetAlphaMode();
			Iw2DSetAlphaMode( getAlphaMode() );

			// TODO : Cagado!!! N�o est� levando em conta a tintColor do pai!!!
			CIwColour prevTintColor = Iw2DGetColour();
			Iw2DSetColour( getTintColor() );	

			ContainerType::iterator end = children.end();
			for( ContainerType::iterator it = children.begin() ; it != end ; ++it )
				( *it )->render();

			// Reseta os estados de renderiza��o
			Iw2DSetColour( prevTintColor );
			Iw2DSetAlphaMode( prevAlphaMode );
			IwGxSetScissorScreenSpace( prevViewport.x, prevViewport.y, prevViewport.w, prevViewport.h );
			MatrixStack::PopMatrix();
		}
	}
}

} //namespace ngc