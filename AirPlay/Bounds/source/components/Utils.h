/*
 *  Utils.h
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef NGC_UTILS_H
#define NGC_UTILS_H

namespace ngc
{
	namespace Utils
	{
		CIwMat2D& ScaleMatrix( const CIwFVec2& scale, CIwMat2D& rOutMtx );

		CIwMat2D& ConvertCIwMatToCIwMat2D( const CIwMat& rInMtx, CIwMat2D& rOutMtx );
	}

} // namespace ngc

#endif