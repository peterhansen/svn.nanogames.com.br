/*
 *  Renderable.cpp
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

// Components
#include "Renderable.h"
#include "Utils.h"

namespace ngc
{

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

Renderable::Renderable() : pos(), anchor(), size(), scale( 1, 1 ), visible( true ),
						   alphaMode( IW_2D_ALPHA_NONE ), tintColor( IwGxGetColFixed( IW_GX_COLOUR_WHITE ) ),
						   viewport( CIwRect32( 0, 0, Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight() ) )
{
}

/*===============================================================================================================

METHOD getTransformMatrix
	Retorna a matriz de transformação que deve ser aplicada ao objeto.

===============================================================================================================*/

MatrixStack::MatrixType& Renderable::getTransformMatrix( MatrixStack::MatrixType& rOutMtx )
{
	// Inicializa a matriz
	rOutMtx.SetIdentity();

	// Translate
	// glTranslatef( position.x, position.y );
	rOutMtx.SetTrans( pos );

	// Rotate
	// glTranslatef( -rotCenter.x, -rotCenter.y );
	// glMultMatrixf( rotationMtx );
	// glTranslatef( rotCenter.x, rotCenter.y );
	rOutMtx.SetRot( IW_ANGLE_FROM_DEGREES( rotDegrees ), CIwVec2( rotCenter ) ); 

	// Scale
	// glScalef( size.x * scale.x, size.y * scale.y, size.z * scale.z );
	Utils::ScaleMatrix( scale, rOutMtx );

	// Mirror
	//if( isMirrored( MIRROR_HOR ) )
	//	glRotatef( 180.0f, 0.0f, 1.0f, 0.0f );
	//if( isMirrored( MIRROR_VER ) )
	//	glRotatef( 180.0f, 1.0f, 0.0f, 0.0f );

	// Dessa forma teríamos que fazer um push/ pop do flip state
	//CIw2DImageTransform currFlipOp = Iw2DGetImageTransform();
	//switch( flipOp )
	//{
	//	case FLIP_OP_BOTH:
	//		Iw2DImageTransformOp( currFlipOp, IW_2D_IMAGE_TRANSFORM_ROT180 );
	//		break;

	//	case FLIP_OP_HOR:
	//		Iw2DImageTransformOp( currFlipOp, IW_2D_IMAGE_TRANSFORM_FLIP_X );
	//		break;

	//	case FLIP_OP_VER:
	//		Iw2DImageTransformOp( currFlipOp, IW_2D_IMAGE_TRANSFORM_FLIP_Y );
	//		break;

	//	case FLIP_OP_NONE:
	//		break;
	//}

	CIwMat flipMtx;
	uint8 flipOps = static_cast< uint8 >( flipOp );
	if( flipOps & FLIP_OP_HOR )
		flipMtx.SetRotY( IW_ANGLE_FROM_DEGREES( 180 ) );
	if( flipOps & FLIP_OP_VER )
		flipMtx.SetRotX( IW_ANGLE_FROM_DEGREES( 180 ) );

	CIwMat2D temp;
	rOutMtx *= Utils::ConvertCIwMatToCIwMat2D( flipMtx, temp );

	return rOutMtx;
}

} // namespace ngc