/*
 *  RenderableImg.cpp
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#include "RenderableImg.h"

// Resolu��es de escopo para termos que escrever menos
using namespace boost;

namespace ngc
{

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

RenderableImg::RenderableImg( const std::string& imgName ) : pImg()
{
}

/*===============================================================================================================

DESTRUCTOR

===============================================================================================================*/

RenderableImg::~RenderableImg()
{
}

/*===============================================================================================================

METHOD render
	Renderiza o objeto.

===============================================================================================================*/

void RenderableImg::render( void )
{
	if( pImg && isVisible() && ( getAlpha() > 0 ) )
	{
		CIwUIRect vp;
		getViewport( vp );

		if( ( vp.w > 0 ) && ( vp.h > 0 ) && vp.Intersects( CIwUIRect( CIwRect32( 0, 0, Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight() ))) )
		{
			// TODO : Cagado!!! N�o est� dando push state! Al�m disso, deveria "somar" aos atributos determinados pelo pai!!!
			CIwRect prevViewport = IwGxGetScissorScreenSpace();
			IwGxSetScissorScreenSpace( vp.x, vp.y, vp.w, vp.h );

			CIw2DAlphaMode prevAlphaMode = Iw2DGetAlphaMode();
			Iw2DSetAlphaMode( getAlphaMode() );

			//CIwMat2D rot; 
			//rot.SetRot(IW_GEOM_ONE/8, CIwVec2(20, 20)); 
			Iw2DSetTransformMatrix(rot);

			// This 32 bit number is in ABGR_8888 format, i.e. 0xff000000 is black with full alpha.
			CIwColour prevTintColor = Iw2DGetColour();
			Iw2DSetColour( getTintColor() );			

			CIwSVec2 size( getSize() );
			CIwFVec2 scale;
			getScale( scale );
			Iw2DDrawImage( pImg.get(), getPosition(), CIwSVec2( size.x * scale.x, size.y * scale.y ) );

			Iw2DSetColour( prevTintColor );
			Iw2DSetAlphaMode( prevAlphaMode );
			IwGxSetScissorScreenSpace( prevViewport.x, prevViewport.y, prevViewport.w, prevViewport.h );
		}
	}

	//if( !Object::render() || NanoMath::feql( patternFactor.x, 0.0f ) || NanoMath::feql( patternFactor.y, 0.0f ) || NanoMath::feql( patternFactor.z, 0.0f ) || ( !pImg  ) )
	//	return false;
	//
	//bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	//if( customViewport )
	//{
	//	glEnable( GL_SCISSOR_TEST );
	//	viewport.apply();
	//}
	//
	//glMatrixMode( GL_MODELVIEW );
	//glPushMatrix();

	//float halfWidth = getWidth() * 0.5f;
	//float halfHeight = getHeight() * 0.5f;
	//glTranslatef( position.x + halfWidth, position.y + halfHeight, position.z );
	//
	//// TODOO : Provisório para o Blank!!!! Temos que deixar definir um centro de rotação!!!!!!!
	//glTranslatef( -halfWidth, -halfHeight, position.z );
	//
	//Matrix4x4 aux;
	//glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &aux ) ) );
	//
	//glTranslatef( halfWidth, halfHeight, position.z );

	//// Redimensiona o objeto
	//glScalef( size.x * scale.x, size.y * scale.y, size.z * scale.z );
	//
	//// Espelha o objeto
	//if( isMirrored( MIRROR_HOR ) )
	//	glRotatef( 180.0f, 0.0f, 1.0f, 0.0f );
	//
	//if( isMirrored( MIRROR_VER ) )
	//	glRotatef( 180.0f, 1.0f, 0.0f, 0.0f );

	//GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	//glEnable( GL_BLEND );
	//glBlendFunc( texBlendSrcFactor, texBlendDstFactor );
	//
	//// Carrega a textura
	//GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	//glEnable( GL_TEXTURE_2D );
	//pImg->load();
	//
	//// Determina o modo de texturização
	//glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, texEnvMode );
	//
	//if( NanoMath::fdif( scale.x, 1.0f ) || NanoMath::fdif( scale.y, 1.0f ) )
	//{
	//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	//}
	//else
	//{	
	//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	//}
	//
	//// Determina o shade model
	//GLint lastShadeModel;
	//glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	//glShadeModel( GL_FLAT );

	//// Renderiza o objeto
	//if( !( pVertexSet->getRenderStates() & RENDER_STATE_COLOR_ARRAY ) )
	//	glColor4ub( vertexSetColor.getUbR(), vertexSetColor.getUbG(), vertexSetColor.getUbB(), vertexSetColor.getUbA() );
	//
	//pVertexSet->render();
	//
	//if( !( pVertexSet->getRenderStates() & RENDER_STATE_COLOR_ARRAY ) )
	//	glColor4ub( 255, 255, 255, 255 );
	//
	//// Reseta os estados do OpenGL
	//pImg->unload();

	//if( tex2DWasEnabled == GL_FALSE )
	//	glDisable( GL_TEXTURE_2D );

	//glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
	//if( blendWasEnabled == GL_FALSE )
	//	glDisable( GL_BLEND );
	//
	//// Reseta o shade model
	//glShadeModel( lastShadeModel );
	//
	//glMatrixMode( GL_MODELVIEW );
	//glPopMatrix();

	//if( customViewport )
	//	glDisable( GL_SCISSOR_TEST );

	//return true;
}

} // namespace ngc