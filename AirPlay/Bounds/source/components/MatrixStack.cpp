/*
 *  MatrixStack.cpp
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#include "MatrixStack.h"

// AirPlay
#include <Iw2D.h>

// Defini��es �teis. Essas strings ir�o sumir na vers�o release. Logo, n�o precisamos nos preocuparmos com as repeti��es causadas pelas macros
#define ASSERT_INITIALIZED() IwAssertMsg( NGC, pSingleton != NULL, ( "MatrixStack not initialized" ) )

namespace ngc
{

/*===============================================================================================================

STATIC ATTRIBUTES

===============================================================================================================*/

MatrixStack* MatrixStack::pSingleton = NULL;

/*===============================================================================================================

CONSTRUCTOR

===============================================================================================================*/

MatrixStack::MatrixStack() : matrixes()
{
	matrixes.push( MatrixType::g_Identity );
}

/*===============================================================================================================

STATIC METHOD Create
	Cria a �nica inst�ncia da classe.

===============================================================================================================*/

void MatrixStack::Create()
{
	if( pSingleton == NULL )
		pSingleton = new MatrixStack();
}

/*===============================================================================================================

STATIC METHOD Destroy
	Destr�i a �nica inst�ncia da classe.

===============================================================================================================*/

void MatrixStack::Destroy()
{
	delete pSingleton;
	pSingleton = NULL;
}

/*===============================================================================================================

STATIC METHOD PushMatrix
	Adiciona uma matriz � pilha de matrizes de transforma��o, multiplicando-a pela matriz atual. Inicialmente, a
pilha de matrizes cont�m uma �nica matriz, uma matriz identidade

===============================================================================================================*/

void MatrixStack::PushMatrix( const MatrixType& mtx )
{
	ASSERT_INITIALIZED();

	MatrixType res( GetCurrentMatrix() * mtx );
	pSingleton->matrixes.push( res );

	// Determina a matriz de transforma��o a ser aplicada nas pr�ximas renderiza��es
	Iw2DSetTransformMatrix( res );
}

/*===============================================================================================================

STATIC METHOD GetCurrentMatrix
	Retorna uma refer�ncia para a matriz que est� no topo da pilha de matrizes de transforma��o.

===============================================================================================================*/

MatrixStack::MatrixType& MatrixStack::GetCurrentMatrix()
{
	ASSERT_INITIALIZED();
	return pSingleton->matrixes.top();
}

/*===============================================================================================================

STATIC METHOD PopMatrix
	Retira uma matriz da pilha de matrizes de transforma��o. A pilha nunca ficar� vazia, pois conter� sempre
uma matriz identidade como base.

===============================================================================================================*/

void MatrixStack::PopMatrix()
{
	ASSERT_INITIALIZED();

	// Nunca podemos retirar o �ltimo elemento da pilha!!!
	if( pSingleton->matrixes.size() > 1 )
	{
		pSingleton->matrixes.pop();

		// Determina a matriz de transforma��o a ser aplicada nas pr�ximas renderiza��es
		Iw2DSetTransformMatrix( GetCurrentMatrix() );
	}
}

} // namespace ngc