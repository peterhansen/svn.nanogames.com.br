/*
 *  RenderableImg.h
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef NGC_RENDERABLEIMG_H
#define NGC_RENDERABLEIMG_H

// C++
#include <string>

// Boost (AirPlay ainda n�o aponta o header <memory> atualizado, o qual torna shared_ptr um recurso padr�o da linguagem)
#include "shared_ptr.hpp"

// Components
#include "Renderable.h"

namespace ngc
{
	class RenderableImg : public Renderable
	{
		public:
			explicit RenderableImg( const std::string& imgName );
			virtual ~RenderableImg();

			///<summary>Renderiza o objeto</summary>
			virtual void render( void );

		private:
			///<summary>Imagem que ser� renderizada na tela</summary>
			boost::shared_ptr< CIw2DImage > pImg;
	};

} // namespace ngc

#endif