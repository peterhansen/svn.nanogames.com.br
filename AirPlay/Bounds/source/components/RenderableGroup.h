/*
 *  RenderableGroup.h
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef NGC_RENDERABLE_GROUP_H
#define NGC_RENDERABLE_GROUP_H

// C++
#include <vector>
#include <inttypes.h>

// Boost (AirPlay ainda n�o aponta o header <memory> atualizado, o qual torna shared_ptr um recurso padr�o da linguagem)
#include "shared_ptr.hpp"

// Components
#include "Renderable.h"

namespace ngc
{
	class RenderableGroup : public Renderable
	{
		public:
			typedef int16 ChildIndexType;
			typedef boost::shared_ptr< Renderable > ChildPtrType;
			typedef std::vector< ChildPtrType > ContainerType;
			typedef ContainerType::size_type SizeType;

			RenderableGroup();
			virtual ~RenderableGroup();

			///<summary>Adiciona um filho ao grupo</summary>
			///<returns>Retorna o �ndice no qual o filho foi inserido</returns>
			ChildIndexType addChild( ChildPtrType pChild );

			///<summary>Adiciona um filho ao grupo no �ndice especificado. Se o �ndice especiicado for maior que o n�mero total de filhos, o novo elemento ser�
			/// inserido no fim do grupo. Logo, o valor retornado pode ser diferente do passado como par�metro
			///</summary>
			///<returns>Retorna o �ndice no qual o filho foi inserido</returns>
			ChildIndexType addChild( ChildPtrType pChild, ChildIndexType index );

			///<summary>Retorna o filho contido no �ndice especificado</summary>
			ChildPtrType getChild( ChildIndexType index );

			///<summary>Retorna o �ndice do filho especificado</summary>
			ChildIndexType getChildIndex( ChildPtrType pChild ) const;

			///<summary>Remove todos os filhos do grupo</summary>
			void removeChildren();

			///<summary>Remove um filho do grupo</summary>
			///<returns>Retorna o filho que foi removido</returns>
			ChildPtrType removeChild( ChildPtrType pChild );

			///<summary>Remove um filho do grupo</summary>
			///<returns>Retorna o filho que foi removido</returns>
			ChildPtrType removeChild( ChildIndexType index );

			///<summary>Retorna o n�mero de filhos do grupo</summary>
			inline SizeType getNChildren() const { return children.size(); };

			///<summary>Renderiza o objeto</summary>
			virtual void render( void );

		private:
			///<summary>Array de objetos filhos do grupo</summary>
			ContainerType children;
	};

} // namespace ngc

#endif