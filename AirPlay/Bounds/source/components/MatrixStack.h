/*
 *  MatrixStack.h
 *  NGC
 *
 *  Created by Daniel L. Alves on 19/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 */

#ifndef NGC_MATRIX_STACK_H
#define NGC_MATRIX_STACK_H

// C++
#include <stack>

// AirPlay
#include <IwGeomMat2D.h> 

namespace ngc
{
	class MatrixStack
	{
		public:
			// TODO: Fazer com template
			typedef CIwMat2D MatrixType;

			///<sumary>Cria a �nica inst�ncia da classe</sumary>
			static void Create();

			///<sumary>Destr�i a �nica inst�ncia da classe</sumary>
			static void Destroy();

			///<summary>
			/// Adiciona uma matriz � pilha de matrizes de transforma��o, multiplicando-a pela matriz atual. Inicialmente, a
			/// pilha de matrizes cont�m uma �nica matriz, uma matriz identidade
			///</summary>
			static void PushMatrix( const MatrixType& mtx );

			///<summary>Retorna uma refer�ncia para a matriz que est� no topo da pilha de matrizes de transforma��o</summary>
			static MatrixType& GetCurrentMatrix();

			///<summary>
			/// Retira uma matriz da pilha de matrizes de transforma��o. A pilha nunca ficar� vazia, pois conter� sempre
			/// uma matriz identidade como base
			///</summary>
			static void PopMatrix();

		private:
			// Classe singleton: desabilitamos c�pias, new e delete para c�digo externo
			MatrixStack();
			MatrixStack( const MatrixStack& other );
			MatrixStack& operator=( const MatrixStack& other );
			~MatrixStack();

			///<summary>�nica inst�ncia da classe</summary>
			static MatrixStack* pSingleton;

			///<summary>Pilha de matrizes de transforma��o da aplica��o</summary>
			std::stack< MatrixType > matrixes;
	};
}

#endif