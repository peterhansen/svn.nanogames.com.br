#ifndef APP_STATE_H
#define APP_STATE_H

#include <components/defines.h>
#include <components/sprite.h>

#include "piece.h"

class MainScreen;

enum StateId
{
	STATE_ID_BRAND_SELECTION,
	//STATE_ID_FADING_ANIMATION,
	STATE_ID_CONVERTING_PIECES,
	STATE_ID_HIDING_PIECES,
	STATE_ID_HIGHLIGHTING_QUALITIES,
	STATE_ID_RESETTING
};

struct AppState
{
	StateId id;
	//unsigned int pieceTurningTime;
	unsigned int blendTime;
	Brand selectedBrand;
	int piecesConverted;
	int piecesWaitingForHiding;
	int qualitiesClicked;
};

class AppStateManager
{
public:

	AppStateManager( MainScreen* screen );

	void update( unsigned int dt );
	void reset();

	void setState( StateId state );

	void onSequenceEnded( Piece* const piece );
	bool onNextFrame( Piece* const piece, int nextFrameIndex );
	void onPointerPressed( const s3ePointerEvent* ev );

protected:	

	static const unsigned int TIME_FOR_A_NEW_PIECE_TO_TURN = 200;
	static const unsigned int TIME_FOR_PIECE_CONVERSION = 50;
	static const unsigned int BLEND_HIDE_TIME = 2000;
	static const unsigned int BLEND_SHOW_TIME = 1000;
	static const int MAX_SIMULTANEOUS_PIECES_TURNED = 3;
	static const int SET_GENERATOR_PRIME = 11;

	AppState state;
	MainScreen* screen;

	Brand nextBrandToTurn;
	int lastIndexToBeTurned;
	
	void selectBrand( Brand brand );

	//void updateMosaic( unsigned int dt );
	void updateBlendBackground(  unsigned int dt );
	
	void hidePiece( Piece* const piece );
	void showPiece( int pieceIndex );

	int getNextIndex( Brand brand, int i );
	int calculateBlendBackgroundOpacity();
};
#define AppStatePtr		boost::shared_ptr<AppStateManager>

#endif

