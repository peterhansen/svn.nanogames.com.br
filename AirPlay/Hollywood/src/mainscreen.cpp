#include "mainscreen.h"
#include <sstream>

MainScreen::MainScreen() :
	state( new AppStateManager( this ) ),
	mosaicPieces(),
	gridPatterns(),
	frontImage(),
	backgroundImages(),
	warningImages(),
	qualityImages(),
	qualities(),
	goldQualityPositions(),
	redQualityPositions()
{
	Piece::preLoad();
	BigSprite::preLoad();

	// loading background image dummy
	backgroundImage = PatternPtr( new Pattern( CreateColor( 255, 0, 255, 0 ) ) );
	insertInvisibleDrawable( backgroundImage );

	highlight = BigSpritePtr( new BigSprite() );
	insertDrawable( highlight );

	// loading qualities labels dummies
	for( int i = 0; i < NUMBER_OF_QUALITIES; i++ )
	{
		QualityPtr q = QualityPtr( new Quality( i + 1 ) );
		insertInvisibleDrawable( q );
		qualities.push_back( q );
	}

	// stablishing qualities positions
	goldQualityPositions.push_back( Point( 428, 52 ) );
	goldQualityPositions.push_back( Point( 455, 130 ) );
	goldQualityPositions.push_back( Point( 476, 205 ) );
	redQualityPositions.push_back( Point( 98, 52 ) );
	redQualityPositions.push_back( Point( 74, 130 ) );
	redQualityPositions.push_back( Point( 51, 205 ) );

	// loading background and warning images
	backgroundImages.push_back( DrawableImagePtr( new DrawableImage( getBrandBackgroundImageName( BRAND_GOLD ) ) ) );
	backgroundImages.push_back( DrawableImagePtr( new DrawableImage( getBrandBackgroundImageName( BRAND_RED ) ) ) );
	warningImages.push_back( DrawableImagePtr( new DrawableImage( getWarningImageName( BRAND_GOLD ) ) ) );
	warningImages.push_back( DrawableImagePtr( new DrawableImage( getWarningImageName( BRAND_RED ) ) ) );

	// preloading qualities images
	qualityImages.push_back( DrawableImagePtr( new DrawableImage( getQualityImageName( BRAND_GOLD, 0 ) ) ) );
	qualityImages.push_back( DrawableImagePtr( new DrawableImage( getQualityImageName( BRAND_GOLD, 1 ) ) ) );
	qualityImages.push_back( DrawableImagePtr( new DrawableImage( getQualityImageName( BRAND_GOLD, 2 ) ) ) );
	qualityImages.push_back( DrawableImagePtr( new DrawableImage( getQualityImageName( BRAND_RED, 0 ) ) ) );
	qualityImages.push_back( DrawableImagePtr( new DrawableImage( getQualityImageName( BRAND_RED, 1 ) ) ) );
	qualityImages.push_back( DrawableImagePtr( new DrawableImage( getQualityImageName( BRAND_RED, 2 ) ) ) );

	frontImage = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "BkgMosaico.png" ) );
	insertDrawable( frontImage );

	// inserting screen borders
	gridPatterns.push_back( PatternPtr( new Pattern( CreateColor( 0, 0, 0, 255 ) ) ) );
	gridPatterns.push_back( PatternPtr( new Pattern( CreateColor( 0, 0, 0, 255 ) ) ) );
	gridPatterns.push_back( PatternPtr( new Pattern( CreateColor( 0, 0, 0, 255 ) ) ) );
	gridPatterns.push_back( PatternPtr( new Pattern( CreateColor( 0, 0, 0, 255 ) ) ) );
	insertDrawable( gridPatterns[ 0 ] );
	insertDrawable( gridPatterns[ 1 ] );
	insertDrawable( gridPatterns[ 2 ] );
	insertDrawable( gridPatterns[ 3 ] );

	// inserting in between grid
	for( int i = 0; i < NUMBER_OF_MOSAIC_COLUMNS - 1; i++ )
	{
		PatternPtr p = PatternPtr( new Pattern( CreateColor( 0, 0, 0, 255 ) ) );
		gridPatterns.push_back( p );
		insertDrawable( p );
	}

	for( int i = 0; i < NUMBER_OF_MOSAIC_ROWS - 1; i++ )
	{
		PatternPtr p = PatternPtr( new Pattern( CreateColor( 0, 0, 0, 255 ) ) );
		gridPatterns.push_back( p );
		insertDrawable( p );
	}

	// TODO: remover grid
	//for( int i = 4, size = gridPatterns.size(); i < size; i++ )
	//{
	//	gridPatterns[i]->setVisible( false );
	//}

	for( int i = 0; i < 2; i++ )
	{
		Brand b = ( i == 0? BRAND_GOLD: BRAND_RED );
		for( int j = 0; j < Piece::VARIETY_OF_PIECES; j++ )
		{
			std::stringstream stillImageName;
			stillImageName << Constants::Path::IMAGES << "Sprite" << ( b == BRAND_GOLD? "Gold": "Red" ) << "0" << ( j % 4 ) + 1 << "Still.bmp";
			ImagePtr stillImage = ImagePtr( Iw2DCreateImage( stillImageName.str().data() ) );
			stillImages.push_back( stillImage );
		}
	}

	for( int i = 0; i < TOTAL_NUMBER_OF_PIECES; i++ )
	{
		Brand b = i < TOTAL_NUMBER_OF_PIECES/2 ? BRAND_GOLD : BRAND_RED;
		DrawableImagePtr stillPiece = DrawableImagePtr( new DrawableImage( stillImages[ (b == BRAND_GOLD? 0: 4) + i % 4 ] ) );
		insertDrawable( stillPiece );
		addNewPiece( b, i % 4, stillPiece );
	}

	//s3eFile* pFile = s3eFileOpen( (Constants::Path::IMAGES + "testando.bmp").data(), "rb");
	//CIwImage s_Image;
	//s_Image.ReadFile(pFile);

	//DrawableImagePtr di = DrawableImagePtr( new DrawableImage( ImagePtr( Iw2DCreateImage( s_Image ) ) ) );
	//insertDrawable( di );

	warning = getWarningImage( BRAND_GOLD );
	insertDrawable( warning );

	state->setState( STATE_ID_BRAND_SELECTION );
	setSize( Point( SCREEN_WIDHT, SCREEN_HEIGHT ) );
}

void MainScreen::terminate()
{
	Piece::terminate();
	BigSprite::terminate();
}

void MainScreen::setSize( Point size )
{
	Screen::setSize( size );

	frontImage->setPosition( 0, 0 );
	frontImage->setSize( size );
	backgroundImage->setPosition( 0, 0 );
	backgroundImage->setSize( size );

	gridPatterns[ 0 ]->setPosition( 0, 0 );
	gridPatterns[ 0 ]->setSize( 5, size.y );
	gridPatterns[ 1 ]->setPosition( 0, 0 );
	gridPatterns[ 1 ]->setSize( size.x, 5 );
	gridPatterns[ 2 ]->setPosition( 0, size.y - 8 );
	gridPatterns[ 2 ]->setSize( size.x, 8 );
	gridPatterns[ 3 ]->setPosition( size.x - 5, 0 );
	gridPatterns[ 3 ]->setSize( 6, size.y );

	for( int i = 0; i < TOTAL_NUMBER_OF_PIECES; i++ )
	{
		int xPos = ( i / NUMBER_OF_MOSAIC_ROWS ) * MOSAIC_PIECE_WIDTH + MOSAIC_PIECE_X_OFFSET;
		int yPos = ( i % NUMBER_OF_MOSAIC_ROWS ) * MOSAIC_PIECE_HEIGHT + MOSAIC_PIECE_Y_OFFSET;
		mosaicPieces[ i ]->setPosition( xPos, yPos );
	}

	for( int i = 0; i < NUMBER_OF_MOSAIC_COLUMNS - 1; i++ )
	{
		gridPatterns[ i + 4 ]->setPosition( MOSAIC_PIECE_X_OFFSET + MOSAIC_PIECE_WIDTH * ( 1 + i ) + 1, 0 );
		gridPatterns[ i + 4 ]->setSize( 2, size.y );
	}

	for( int i = 0; i < NUMBER_OF_MOSAIC_ROWS - 1; i++ )
	{
		gridPatterns[ i + 4 + NUMBER_OF_MOSAIC_COLUMNS - 1 ]->setPosition( 0, MOSAIC_PIECE_Y_OFFSET + MOSAIC_PIECE_HEIGHT * ( 1 + i ) + 7 );
		gridPatterns[ i + 4 + NUMBER_OF_MOSAIC_COLUMNS - 1 ]->setSize( size.x, 2 );
	}


	for( int i = 0; i < NUMBER_OF_QUALITIES; i++ )
	{
		qualities[ i ]->setPosition( 0, 0 );
		qualities[ i ]->setSize( size );
	}

	warning->setPosition( 0, size.y - warning->getHeight() );
}

void MainScreen::update( unsigned int dt )
{
	/*Screen::update( dt );*/
	for( int i = 0, nDrawables = drawables.size(); i < nDrawables; i++ )
		drawables[ i ]->update( dt );

	state->update( dt );
	s3eDeviceBacklightOn();
}

void MainScreen::onSequenceEnded( Sprite* const sprite, int sequenceIndex )
{
	state->onSequenceEnded( (Piece*)sprite );
}

bool MainScreen::onNextFrame( Sprite* const sprite, int nextFrameIndex )
{
	return state->onNextFrame( (Piece*)sprite, nextFrameIndex );
}

void MainScreen::addNewPiece( Brand brand, int pieceImageIndex, DrawableImagePtr stillPiece )
{
	PiecePtr piece = createPiece( brand, pieceImageIndex, stillPiece);
	insertDrawable( piece );
	mosaicPieces.push_back( piece );
}

PiecePtr MainScreen::createPiece( Brand brand, int imageIndex, DrawableImagePtr stillPiece )
{
	PiecePtr piece = PiecePtr( new Piece( brand, imageIndex, stillPiece ) );
	piece->setListener( this );
	return piece;
}

int MainScreen::getPieceIndex( Piece* piece )
{
	int pieceIndex = -1;
	for( int i = 0, size = mosaicPieces.size(); i < size; i++ )
	{
		if( mosaicPieces[ i ].get() == piece )
		{
			pieceIndex = i;
			break;
		}
	}
	return pieceIndex;
}

int MainScreen::getNumberOfPiecesVisible()
{
	int numberOfPiecesVisible = 0;
	for( int i = 0, size = mosaicPieces.size(); i < size; i++ )
		numberOfPiecesVisible += mosaicPieces[ i ]->isVisible() ? 1: 0;
	return numberOfPiecesVisible;
}

void MainScreen::turnPiece( int pieceIndex )
{
	mosaicPieces[ pieceIndex ]->turn();
}

void MainScreen::showPiece( int pieceIndex, Brand newBrand )
{
	PiecePtr piece = mosaicPieces[ pieceIndex ];
	piece->setVisible( true );
	convertPiece( piece.get(), newBrand );
	turnPiece( pieceIndex );
	piece->setFrameIndex( MOSAIC_PIECE_HIDING_FRAME_INDEX );
}

void MainScreen::convertPiece( Piece* piece, Brand brand )
{
	DrawableImagePtr newStillImage = DrawableImagePtr( new DrawableImage( stillImages[ ( brand == BRAND_GOLD? 0: 4) + piece->getImageIndex() ] ) );

	PiecePtr newPiece = createPiece( brand, piece->getImageIndex(), newStillImage );
	newPiece->setSequenceIndex( 1 );
	newPiece->setFrameIndex( MOSAIC_PIECE_HIDING_FRAME_INDEX );
	newPiece->setPosition( piece->getPosition().x, piece->getPosition().y );
	newPiece->setConverted( true );
	replacePiece( piece, newPiece, piece->getStillImage(), newStillImage );
}

void MainScreen::replacePiece( Piece* piece, PiecePtr newPiece, DrawableImagePtr stillImage, DrawableImagePtr newStillImage )
{
	for( int i = 0, size = drawables.size(); i < size; i++ )
	{
		if( drawables[ i ].get() == piece )
		{
			drawables[ i ] = newPiece;
			break;
		}
	}
	for( int i = 0, size = drawables.size(); i < size; i++ )
	{
		if( drawables[ i ].get() == stillImage.get() )
		{
			drawables[ i ] = newStillImage;
			break;
		}
	}
	mosaicPieces[ getPieceIndex( piece ) ] = newPiece;
}

void MainScreen::loadBrandFullScreen( Brand brand )
{
	// creating background image
	DrawableImagePtr newBackgroundImage = getBackgroundImage( brand ); /*new DrawableImage( getBrandBackgroundImageName( brand ) )*/
	
	// loading highlight bigsprite
	BigSpritePtr newHighlight = BigSpritePtr( new BigSprite( brand, 25, 100 ) );
	drawables[ 1 ] = newHighlight;
	drawables[ 1 ]->setPosition( brand == BRAND_GOLD? GOLD_HIGHLIGHT_X_POS: RED_HIGHLIGHT_X_POS, 0 );
	drawables[ 1 ]->setSize( 323, 361 );
	drawables[ 1 ]->setVisible( true );
	highlight = newHighlight;

	for( int i = 0; i < NUMBER_OF_QUALITIES; i++ )
	{
		QualityPtr q = qualities[ i ];
		q->setVisible( i != NUMBER_OF_QUALITIES - 1 );
			
		int signal = (brand == BRAND_RED)? -1: 1;

		DrawableImagePtr button = getQualityImage( brand, i );
		button->setPosition( brand == BRAND_GOLD? goldQualityPositions[ i ]: redQualityPositions[ i ] );//( SCREEN_WIDHT / 2 ) + signal * ( QUALITY_POSITION_X + ( i * QUALITY_OFFSET_X ) ), QUALITY_POSITION_Y + ( i * QUALITY_OFFSET_Y ) );

		PatternPtr highlight = PatternPtr( new Pattern( CreateColor( 0x00AA0aFF ) ) );

		q->setButtonAndHightlight( button, newHighlight );

		if( i != NUMBER_OF_QUALITIES - 1 )
			q->setState( QUALITY_STATE_FADING_IN );
	}
	
	newBackgroundImage->setPosition( backgroundImage->getPosition() );
	newBackgroundImage->setSize( backgroundImage->getSize() );
	drawables[ 0 ] = newBackgroundImage;
}

void MainScreen::blendGrid( int opacity, int warningOpacity )
{
	for( int i = 0; i < gridPatterns.size(); i++ )
		linearInterpolateAlpha( gridPatterns[ i ], -1, 255, opacity );

	linearInterpolateAlpha( warning, -1, 255, warningOpacity );
}

std::string MainScreen::getQualityImageName( Brand brand, int i )
{
	std::stringstream name;
	name << Constants::Path::IMAGES << "Txt";
	name << ( brand == BRAND_GOLD? "Gold" : "Red" );
	switch( i )
	{
	case 0:
		name << "Ousadia";
		break;
	case 1:
		name << "Inovacao";
		break;
	case 2:
		name << "Novo";
		break;
	}
	name << ".png";
	return name.str();
}

std::string MainScreen::getBrandBackgroundImageName( Brand brand )
{
	if( brand == BRAND_GOLD )
		return Constants::Path::IMAGES + "BkgGold.png";
	else
		return Constants::Path::IMAGES + "BkgRed.png";
}

std::string MainScreen::getWarningImageName( Brand brand )
{
	if( brand == BRAND_GOLD )
		return Constants::Path::IMAGES + "warningGold.png";
	else
		return Constants::Path::IMAGES + "warningRed.png";
}

DrawableImagePtr MainScreen::getBackgroundImage( Brand brand )
{
	return backgroundImages[ brand == BRAND_GOLD? 0: 1 ];
}

DrawableImagePtr MainScreen::getWarningImage( Brand brand )
{
	return warningImages[ brand == BRAND_GOLD? 0: 1 ];
}

DrawableImagePtr MainScreen::getQualityImage( Brand brand, int index )
{
	int arrayIndex = ( brand == BRAND_GOLD? 0: 3 ) + index;
	return qualityImages[ arrayIndex ];
}

void MainScreen::linearInterpolateAlpha( DrawablePtr drawable, int a, int b, int x )
{
	Color c = drawable->getColor();
	c.a = a * x + b;
	drawable->setColor( c );
}
	
void MainScreen::pointerButtonCB( const s3ePointerEvent *ev )
{
	state->onPointerPressed( ev );
}

void MainScreen::touchButtonCB( const s3ePointerTouchEvent *ev )
{
	s3ePointerEvent newEv;
	newEv.m_Pressed = ev->m_Pressed;
	newEv.m_x = ev->m_x;
	newEv.m_y = ev->m_y;
	pointerButtonCB( &newEv );
}