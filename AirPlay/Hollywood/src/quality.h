#ifndef QUALITY_H
#define QUALITY_H

#include <components\drawablegroup.h>
#include "bigsprite.h"

enum QualityState
{
	QUALITY_STATE_OFF,
	QUALITY_STATE_FADING_IN,
	QUALITY_STATE_WAITING_FOR_CLICK,
	QUALITY_STATE_TURNING_ON,
	QUALITY_STATE_ON
};

class Quality : public DrawableGroup
{
public:
	Quality( int id );

	void setState( QualityState state );
	void setButtonAndHightlight( DrawablePtr button, BigSpritePtr highlight );

	virtual void update( unsigned int dt );
	bool pressButton( Point point );
	//inline bool isClicked() const
	//{
	//	return state == QUALITY_STATE_ON || state == QUALITY_STATE_TURNING_ON; 
	//}

protected:
	static const int HIGHLIGHT_QUALITY_FADE_IN_RATIO = 5;
	static const unsigned int FADE_IN_TIME = 300;
	static const unsigned int TURNING_ON_TIME = 1600;
	static const unsigned int INITIAL_ALPHA = 95;

	int id;
	unsigned int time;
	QualityState state;
	BigSpritePtr highlight;
};
#define QualityPtr		boost::shared_ptr<Quality>
#endif

