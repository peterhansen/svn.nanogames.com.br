#ifndef PIECE_H
#define PIECE_H

#include <components/defines.h>
#include <components/drawableimage.h>
#include <components/listeners.h>
#include <components/sprite.h>

enum Brand
{
	BRAND_NONE,
	BRAND_RED,
	BRAND_GOLD
};

class Piece : public Sprite
{
public:

	static const int VARIETY_OF_PIECES = 4;

	Piece( Brand brand, int imageIndex, DrawableImagePtr stillPiece );
	
	static void preLoad();
	static void terminate();

	inline Brand getBrand() const
	{ 
		return brand;
	}

	inline int getImageIndex() const 
	{
		return imageIndex;
	}

	inline DrawableImagePtr getStillImage()
	{
		return stillPiece;
	}

	inline void setTimeForTurning( unsigned int timeForTurning )
	{
		timeBeforeTurningAgain = timeForTurning; 
	}
	
	inline void setConverted( bool converted )
	{
		this->converted = converted;
	}

	inline bool isConverted() const 
	{
		return converted;
	}

	inline bool isAvailableForTurning() const 
	{
		return !isTurning() && canTurnAgain(); 
	}

	inline bool isAvailableForShowing() const
	{
		return !isVisible() && !isTurning() && canTurnAgain();
	}

	inline bool isAvailableForConverting() const 
	{
		return isVisible() && !isTurning();
	}

	virtual void update( unsigned int dt )
	{
		if( firstTime < 5 )
		{
			dt = NanoMath::clamp( dt, 0, 100 );
			firstTime++;
		}

		Sprite::update( dt );

		if( canTurn && !isTurning() )
		{
			if( timeBeforeTurningAgain >= 0 )
			{
				timeBeforeTurningAgain -= dt;
			}
			else
			{
				turn();
			}
		}
	}

	void setSequenceIndex( int sequenceIndex );
	void setPosition( int x, int y );

	void turn();
	void setTurnable( bool canTurn );
	void hide();

protected:

	static const unsigned int TIME_WAITING_TO_TURN = 2000;

	//static ImagePtr redPieceImage1;
	//static ImagePtr redPieceImage2;
	static CIwArray<ImagePtr> redPieceImages;
	static CIwArray<ImagePtr> goldPieceImages;

	Brand brand;
	int firstTime;
	bool canTurn;
	bool converted;
	int imageIndex;
	int timeBeforeTurningAgain;

	DrawableImagePtr stillPiece;

	inline bool isTurning() const 
	{
		return getSequenceIndex() != 0;
	}

	inline bool canTurnAgain() const 
	{
		return timeBeforeTurningAgain <= 0 && canTurn;
	}

};
#define PiecePtr		boost::shared_ptr<Piece>

#endif
