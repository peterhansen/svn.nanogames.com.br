#ifndef BIG_SPRITE_H
#define BIG_SPRITE_H

#include <components/defines.h>
#include <components/drawable.h>
#include <components/drawableimage.h>

#include "piece.h"

enum BigSpriteState 
{
	BIG_SPRITE_STOPPED,
	BIG_SPRITE_FIRST_QUALITY_CLICKED,
	BIG_SPRITE_SECOND_QUALITY_CLICKED,
	BIG_SPRITE_BOTH_QUALITIES_CLICKED,
	BIG_SPRITE_BRAND_QUALITY_CLICKED
};

enum BigSpriteAnimation
{
	BIG_SPRITE_ANIMATION_STOPPED,
	BIG_SPRITE_ANIMATION_INOVACAO,
	BIG_SPRITE_ANIMATION_OUSADIA,
	BIG_SPRITE_ANIMATION_INOVACAO_OUSADIA,
	BIG_SPRITE_ANIMATION_OUSADIA_INOVACAO,
	BIG_SPRITE_ANIMATION_NOVO
};

class BigSprite : public Drawable
{
public:
	static void preLoad();
	static void terminate();

	BigSprite( Brand brand, int numberOfFrames, unsigned int frameTime );
	BigSprite();

	virtual void update( unsigned int dt );
	virtual void paint();

	void onQualityClicked( int qualityId );
	//inline void setPlaying( bool isPlaying ) 
	//{ 
	//	this->isPlaying = isPlaying; 
	//	currentFrame = 0; 
	//}

	inline bool getPlaying() const 
	{ 
		return this->isPlaying;
	}

protected:

	static const int NUMBER_OF_FRAMES_FINAL_ANIMATION = 13;
	static const int FADING_TIME = 600;

	BigSpriteState state;
	Brand brand;
	//int firstTime;
	bool isPlaying;
	int currentFrame;
	int numberOfFrames;
	unsigned int accTime;
	unsigned int frameTime;
	CIwArray<DrawableImagePtr> frames;

	static DrawableImagePtr goldStopped;
	static DrawableImagePtr redStopped;
	static CIwArray<DrawableImagePtr> goldAnimaInovacao;
	static CIwArray<DrawableImagePtr> goldAnimaOusadia;
	static CIwArray<DrawableImagePtr> goldAnimaInovacaoOusadia;
	static CIwArray<DrawableImagePtr> goldAnimaOusadiaInovacao;
	static CIwArray<DrawableImagePtr> goldAnimaNovo;
	static CIwArray<DrawableImagePtr> redAnimaInovacao;
	static CIwArray<DrawableImagePtr> redAnimaOusadia;
	static CIwArray<DrawableImagePtr> redAnimaInovacaoOusadia;
	static CIwArray<DrawableImagePtr> redAnimaOusadiaInovacao;
	static CIwArray<DrawableImagePtr> redAnimaNovo;

	//void setAnimation( std::string namePrefix, int numberOfFrames, unsigned int frameTime );
	void setAnimation( BigSpriteAnimation animation );
};
#define BigSpritePtr		boost::shared_ptr<BigSprite>

#endif