#include "bigsprite.h"

#include <sstream>
#include "constants.h"

BigSprite::BigSprite( Brand brand, int numberOfFrames, unsigned int frameTime ) :
	isPlaying( false ),
	state( BIG_SPRITE_STOPPED ),
	brand( brand ),
	frames()
{
	setAnimation( BIG_SPRITE_ANIMATION_STOPPED /*brand == BRAND_GOLD? "highlights/GoldAnimaStopped" : "highlights/RedAnimaStopped", 1, frameTime*/ );
}

BigSprite::BigSprite() :
	isPlaying( false ),
	state( BIG_SPRITE_STOPPED ),
	frames()
{
}

void BigSprite::preLoad()
{
	goldStopped = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "highlights/GoldAnimaStopped0001.png" ) );
	redStopped = DrawableImagePtr( new DrawableImage( Constants::Path::IMAGES + "highlights/RedAnimaStopped0001.png" ) );

	for( int i = 0; i < 1; i++ )
	{
		std::stringstream goldName, redName;
		goldName << Constants::Path::IMAGES << "highlights/GoldAnimaInovacao";
		redName << Constants::Path::IMAGES << "highlights/RedAnimaInovacao";
		int index = i*2 + 1;
		if( index < 10 )
		{
			goldName << "0";
			redName << "0";
		}
		goldName << "00" << index << ".png";
		redName << "00" << index << ".png";

		goldAnimaInovacao.push_back( DrawableImagePtr( new DrawableImage( goldName.str().data() ) ) );
		redAnimaInovacao.push_back( DrawableImagePtr( new DrawableImage( redName.str().data() ) ) );
	}

	for( int i = 0; i < 1; i++ )
	{
		std::stringstream goldName, redName;
		goldName << Constants::Path::IMAGES << "highlights/GoldAnimaOusadia";
		redName << Constants::Path::IMAGES << "highlights/RedAnimaOusadia";
		int index = i*2 + 1;
		if( index < 10 )
		{
			goldName << "0";
			redName << "0";
		}
		goldName << "00" << index << ".png";
		redName << "00" << index << ".png";

		goldAnimaOusadia.push_back( DrawableImagePtr( new DrawableImage( goldName.str().data() ) ) );
		redAnimaOusadia.push_back( DrawableImagePtr( new DrawableImage( redName.str().data() ) ) );
	}

	for( int i = 0; i < 1; i++ )
	{
		std::stringstream goldName, redName;
		goldName << Constants::Path::IMAGES << "highlights/GoldAnimaOusadiaInovacao";
		redName << Constants::Path::IMAGES << "highlights/RedAnimaOusadiaInovacao";
		int index = i*2 + 1;
		if( index < 10 )
		{
			goldName << "0";
			redName << "0";
		}
		goldName << "00" << index << ".png";
		redName << "00" << index << ".png";

		goldAnimaOusadiaInovacao.push_back( DrawableImagePtr( new DrawableImage( goldName.str().data() ) ) );
		redAnimaOusadiaInovacao.push_back( DrawableImagePtr( new DrawableImage( redName.str().data() ) ) );
	}

	for( int i = 0; i < 1; i++ )
	{
		std::stringstream goldName, redName;
		goldName << Constants::Path::IMAGES << "highlights/GoldAnimaInovacaoOusadia";
		redName << Constants::Path::IMAGES << "highlights/RedAnimaInovacaoOusadia";
		int index = i*2 + 1;
		if( index < 10 )
		{
			goldName << "0";
			redName << "0";
		}
		goldName << "00" << index << ".png";
		redName << "00" << index << ".png";

		goldAnimaInovacaoOusadia.push_back( DrawableImagePtr( new DrawableImage( goldName.str().data() ) ) );
		redAnimaInovacaoOusadia.push_back( DrawableImagePtr( new DrawableImage( redName.str().data() ) ) );
	}

	for( int i = 0; i < NUMBER_OF_FRAMES_FINAL_ANIMATION; i++ )
	{
		std::stringstream goldName, redName;
		goldName << Constants::Path::IMAGES << "highlights/GoldAnimaNovo";
		redName << Constants::Path::IMAGES << "highlights/RedAnimaNovo";
		if( i < 9 )
		{
			goldName << "0";
			redName << "0";
		}
		goldName << "00" << i + 1 << ".png";
		redName << "00" << i + 1 << ".png";

		goldAnimaNovo.push_back( DrawableImagePtr( new DrawableImage( goldName.str().data() ) ) );
		redAnimaNovo.push_back( DrawableImagePtr( new DrawableImage( redName.str().data() ) ) );
	}
}

void BigSprite::setAnimation( BigSpriteAnimation animation )
{
	frames.clear_optimised();
	currentFrame = 0;
	accTime = 0;
	this->frameTime = 100;

	//for( int i = 0; i < numberOfFrames; i++ )
	//{
	//	//std::stringstream name;
	//	//name << Constants::Path::IMAGES << namePrefix;
	//	//if( i < 9 )
	//	//{
	//	//	name << "0";
	//	//}
	//	//name << "00" << i + 1 << ".png";

	//	frames.push_back( DrawableImagePtr( new DrawableImage( name.str().data() ) ) );
	//}
	switch( animation )
	{
	case BIG_SPRITE_ANIMATION_STOPPED:
		this->numberOfFrames = 1;
		frames.push_back( brand == BRAND_GOLD? goldStopped: redStopped );
		break;

	case BIG_SPRITE_ANIMATION_INOVACAO:
		this->numberOfFrames = 2;
		frames.push_back( brand == BRAND_GOLD? goldStopped: redStopped );
		frames.push_back( ( brand == BRAND_GOLD? goldAnimaInovacaoOusadia[ 0 ]: redAnimaInovacaoOusadia[ 0 ] ) );
		isPlaying = true;
		break;

	case BIG_SPRITE_ANIMATION_OUSADIA:
		this->numberOfFrames = 2;
		frames.push_back( brand == BRAND_GOLD? goldStopped: redStopped );
		frames.push_back( ( brand == BRAND_GOLD? goldAnimaOusadiaInovacao[ 0 ]: redAnimaOusadiaInovacao[ 0 ] ) );
		isPlaying = true;
		break;

	case BIG_SPRITE_ANIMATION_INOVACAO_OUSADIA:
		this->numberOfFrames = 2;
		frames.push_back( ( brand == BRAND_GOLD? goldAnimaInovacaoOusadia[ 0 ]: redAnimaInovacaoOusadia[ 0 ] ) );
		frames.push_back( ( brand == BRAND_GOLD? goldAnimaNovo[ 0 ]: redAnimaNovo[ 0 ] ) );
		isPlaying = true;
		break;

	case BIG_SPRITE_ANIMATION_OUSADIA_INOVACAO:
		this->numberOfFrames = 2;
		frames.push_back( ( brand == BRAND_GOLD? goldAnimaOusadiaInovacao[ 0 ]: redAnimaOusadiaInovacao[ 0 ] ) );
		frames.push_back( ( brand == BRAND_GOLD? goldAnimaNovo[ 0 ]: redAnimaNovo[ 0 ] ) );
		isPlaying = true;
		break;

	case BIG_SPRITE_ANIMATION_NOVO:
		this->numberOfFrames = NUMBER_OF_FRAMES_FINAL_ANIMATION;
		for( int i = 0; i < this->numberOfFrames; i++ )
			frames.push_back( ( brand == BRAND_GOLD? goldAnimaNovo[ i ]: redAnimaNovo[ i ] ) );
		isPlaying = true;
		break;
		
	}
}

void BigSprite::onQualityClicked( int qualityId )
{
	std::stringstream animationName;
	switch( state )
	{
	case BIG_SPRITE_STOPPED:
		setAnimation( qualityId == 1? BIG_SPRITE_ANIMATION_OUSADIA: BIG_SPRITE_ANIMATION_INOVACAO );
		state = qualityId == 1? BIG_SPRITE_FIRST_QUALITY_CLICKED: BIG_SPRITE_SECOND_QUALITY_CLICKED;
		break;

	case BIG_SPRITE_FIRST_QUALITY_CLICKED:
		if( qualityId == 1 )
			return;

		setAnimation( BIG_SPRITE_ANIMATION_OUSADIA_INOVACAO );
		state = BIG_SPRITE_BOTH_QUALITIES_CLICKED;
		break;

	case BIG_SPRITE_SECOND_QUALITY_CLICKED:
		if( qualityId == 2 )
			return;

		setAnimation( BIG_SPRITE_ANIMATION_INOVACAO_OUSADIA );
		state = BIG_SPRITE_BOTH_QUALITIES_CLICKED;
		break;

	case BIG_SPRITE_BOTH_QUALITIES_CLICKED:
		if( qualityId != 3 )
			return;

		setAnimation( BIG_SPRITE_ANIMATION_NOVO );
		state = BIG_SPRITE_BRAND_QUALITY_CLICKED;
		break;
	}
}

void BigSprite::update( unsigned int dt )
{
	if( !isPlaying )
		return;

	switch( state )
	{
	case BIG_SPRITE_BRAND_QUALITY_CLICKED:
		accTime += dt;
		if( accTime > frameTime )
		{
			currentFrame++;
			accTime = 0;
			if( currentFrame >= numberOfFrames )
			{
				currentFrame = numberOfFrames - 1;
				isPlaying = false;
			}
		}
		break;

	case BIG_SPRITE_STOPPED:
		break;

	case BIG_SPRITE_FIRST_QUALITY_CLICKED:
	case BIG_SPRITE_SECOND_QUALITY_CLICKED:
		accTime += dt;
		frames[ 1 ]->setAlpha( NanoMath::clamp( ( accTime * 255 ) / FADING_TIME, 0, 255 ) );
		if( accTime > FADING_TIME )
		{
			accTime = 0;
			isPlaying = false;
		}
		break;

	case BIG_SPRITE_BOTH_QUALITIES_CLICKED:
	//case BIG_SPRITE_ANIMATION_OUSADIA_INOVACAO:
		accTime += dt;
		frames[ 1 ]->setAlpha( NanoMath::clamp( ( accTime * 255 ) / FADING_TIME, 0, 255 ) );
		frames[ 0 ]->setAlpha( NanoMath::clamp( 255 - frames[ 1 ]->getAlpha(), 0, 255 ) );
		if( accTime > FADING_TIME )
		{
			accTime = 0;
			isPlaying = false;
		}
		break;
	}

	
}

void BigSprite::paint()
{
	switch( state )
	{
	case BIG_SPRITE_BRAND_QUALITY_CLICKED:
		frames[ currentFrame ]->draw();
		break;

	case BIG_SPRITE_ANIMATION_STOPPED:
		frames[ 0 ]->draw();
		break;

	default:
		frames[ 0 ]->draw();
		frames[ 1 ]->draw();
		break;
	}
}

void BigSprite::terminate()
{
	goldStopped = DrawableImagePtr();
	redStopped = DrawableImagePtr();
	goldAnimaInovacao = CIwArray<DrawableImagePtr>();
	goldAnimaOusadia = CIwArray<DrawableImagePtr>();
	goldAnimaInovacaoOusadia = CIwArray<DrawableImagePtr>();
	goldAnimaOusadiaInovacao = CIwArray<DrawableImagePtr>();
	goldAnimaNovo = CIwArray<DrawableImagePtr>();
	redAnimaInovacao = CIwArray<DrawableImagePtr>();
	redAnimaOusadia = CIwArray<DrawableImagePtr>();
	redAnimaInovacaoOusadia = CIwArray<DrawableImagePtr>();
	redAnimaOusadiaInovacao = CIwArray<DrawableImagePtr>();
	redAnimaNovo = CIwArray<DrawableImagePtr>();
}

DrawableImagePtr BigSprite::goldStopped = DrawableImagePtr();
DrawableImagePtr BigSprite::redStopped = DrawableImagePtr();
CIwArray<DrawableImagePtr> BigSprite::goldAnimaInovacao = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::goldAnimaOusadia = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::goldAnimaInovacaoOusadia = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::goldAnimaOusadiaInovacao = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::goldAnimaNovo = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::redAnimaInovacao = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::redAnimaOusadia = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::redAnimaInovacaoOusadia = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::redAnimaOusadiaInovacao = CIwArray<DrawableImagePtr>();
CIwArray<DrawableImagePtr> BigSprite::redAnimaNovo = CIwArray<DrawableImagePtr>();