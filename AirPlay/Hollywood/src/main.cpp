// include principal, que importa funcionalidade da API s3eFile
// e da API de imagens e fontes
#include <s3eFile.h>
#include <IwImage.h>
//#include <IwGxFont.h>
//#include <IwResManager.h>
//#include <IwUI.h>

#include <components\defines.h>
#include <components\gamemanager.h>

#include "mainscreen.h"

S3E_MAIN_DECL void IwMain() 
{
	// inicialização dos módulos necessários
	Iw2DInit();
	Iw2DSetUseMipMapping( false );

	int32 surfaceInt = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_PIXEL_TYPE);
	IwTrace( MYAPP, ( "Surface Device Pixel Type: %d", surfaceInt ) );

	//s3eResult r = s3eSurfaceSetup( S3E_SURFACE_PIXEL_TYPE_RGB888, 0, NULL, S3E_SURFACE_BLIT_DIR_NATIVE );
	//if( r == S3E_RESULT_ERROR )
	//{
	//	s3eSurfaceError error = s3eSurfaceGetError();
	//	IwTrace( MYAPP, ( "IHHHHHHHHHHH... erro no s3eSurfaceSetup" ) );
	//}

	// TODO: caso IwGXFont seja usado...
	//IwGxFontInit();

	// TODO: caso IwResourceManager seja usado...
	//IwResManagerInit();

	// TODO: if IwUI is used...
	//IwUIInit();
	
	{
		//CIwUIView view;

		if( !GameManager::init() )
		{
			// TODO: avisar de erro na inicialização
			return;
		}

		MainScreenPtr screen = MainScreenPtr( new MainScreen() );
		GameManager::setCurrentScreen( screen );

		GameManager::setClampDt( false );

		while( GameManager::update() )
			GameManager::draw();
	 
		screen->terminate();

		// executa funcoes necessarias do término da aplicacao
		GameManager::terminate();

		// if IwResourceManager for usado
		//IwResManagerTerminate();
	}

	//IwUITerminate();
	//IwGxFontTerminate();
	Iw2DTerminate();
}

//void boost::throw_exception(std::exception const&)
//{
//	IwTrace( MYAPP, ( "Boost exception." ) );
//}