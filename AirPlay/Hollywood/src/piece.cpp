#include "piece.h"

#include <sstream>

#include "constants.h"

Piece::Piece( Brand brand, int imageIndex, DrawableImagePtr stillPiece ) : 
	Sprite( brand == BRAND_RED? Piece::redPieceImages[ imageIndex ] : Piece::goldPieceImages[ imageIndex ], Constants::Path::IMAGES + "mosaic.bin" ),
	brand( brand ),
	imageIndex( imageIndex ),
	canTurn( true ),
	converted( false ),
	timeBeforeTurningAgain( 0 ),
	stillPiece( stillPiece ),
	firstTime( 0 )
{
	/*stillPiece->setVisible( true );*/
	//setClipTest( false );
	//this->setScaleToSize( false );
	//this->setColor( brand == BRAND_RED? CreateColor( 255, 0, 0, 255 ): CreateColor( 0, 255, 0, 255 ) );
}

void Piece::preLoad()	
{
	for( int i = 0; i < VARIETY_OF_PIECES; i++ )
	{
		std::stringstream redName, goldName;

		goldName << Constants::Path::IMAGES << "SpriteGold0" << i + 1 << ".png";
		redName << Constants::Path::IMAGES << "SpriteRed0" << i + 1 << ".png";

		Piece::redPieceImages.push_back( ImagePtr( Iw2DCreateImage( redName.str().data() ) ) );
		Piece::goldPieceImages.push_back( ImagePtr( Iw2DCreateImage( goldName.str().data() ) ) );
	}	

	//for( int i = 0; i < VARIETY_OF_PIECES; i++ )
	//{
	//	CIwImage::Format f = Piece::goldPieceImages[ i ]->GetMaterial()->GetTexture()->GetFormatSW();
	//	Piece::goldPieceImages[ i ]->GetMaterial()->GetTexture()->SetFormatSW( CIwImage::ABGR_8888 );
	//}
}

void Piece::terminate()
{
	Piece::redPieceImages.clear_optimised();
	Piece::goldPieceImages.clear_optimised();
}

void Piece::turn()
{
	setVisible( true );
	stillPiece->setVisible( true );

	if( isAvailableForTurning() )
	{
		timeBeforeTurningAgain = TIME_WAITING_TO_TURN;
		setSequenceIndex( 1 );
	}
}

void Piece::setTurnable( bool canTurn )
{
	this->canTurn = canTurn;
}

void Piece::hide()
{
	setVisible( false );
	setSequenceIndex( 0 );
	stillPiece->setVisible( false );
	canTurn = false;
}

void Piece::setSequenceIndex( int sequenceIndex )
{
	Sprite::setSequenceIndex( sequenceIndex );

	setVisible( sequenceIndex == 1 );
	stillPiece->setVisible( !visible );
}

void Piece::setPosition( int x, int y )
{
	Sprite::setPosition( x, y );
	stillPiece->setPosition( x + 3, y + 9 );
}

CIwArray<ImagePtr> Piece::redPieceImages = CIwArray<ImagePtr>();
CIwArray<ImagePtr> Piece::goldPieceImages = CIwArray<ImagePtr>();