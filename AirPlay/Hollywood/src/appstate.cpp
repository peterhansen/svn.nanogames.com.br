#include "appstate.h"

#include "mainscreen.h"

AppStateManager::AppStateManager( MainScreen* screen ) :
	screen( screen ),
	nextBrandToTurn( BRAND_GOLD ),
	lastIndexToBeTurned( 0 )
{
	state.id = STATE_ID_BRAND_SELECTION;
	state.blendTime = 0;
	state.piecesConverted = 0;
	state.piecesWaitingForHiding = 0;
	state.selectedBrand = BRAND_NONE;
}

void AppStateManager::update( unsigned int dt )
{
	int piecesReady = 0;

	switch( state.id )
	{
	/*case STATE_ID_FADING_ANIMATION:*/
	case STATE_ID_CONVERTING_PIECES:
		
		for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES; i++ )
		{
			if( screen->mosaicPieces[ i ]->getBrand() == state.selectedBrand &&
				screen->mosaicPieces[ i ]->getFrameSequenceIndex() == 0 )
				piecesReady++;
		}

		if( piecesReady == screen->TOTAL_NUMBER_OF_PIECES )
			setState( STATE_ID_HIDING_PIECES );

	case STATE_ID_HIDING_PIECES:
	case STATE_ID_RESETTING:
		updateBlendBackground( dt );
		break;
	}
}

void AppStateManager::setState( StateId newState )
{
	state.id = newState;
	int turningTime = 0;

	switch( state.id )
	{
	case STATE_ID_RESETTING:
		{
			screen->warning->setVisible( true );
			state.blendTime = 0;
			//state.qualitiesClicked = 0;
			//state.selectedBrand = BRAND_NONE;
			for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES; i++ )
			{
				Brand brand = i < screen->TOTAL_NUMBER_OF_PIECES / 2? BRAND_GOLD : BRAND_RED;
				//int index = getNextIndex( brand, i );
				//unsigned int timeForTurning = ( brand == BRAND_RED? i * 1400 : ( ( i - ( screen->TOTAL_NUMBER_OF_PIECES / 2 ) ) * 1400 + 600 ) );
				
				screen->convertPiece( screen->mosaicPieces[ i ].get(), brand );
				screen->mosaicPieces[ i ]->setTurnable( true );
				screen->mosaicPieces[ i ]->setTimeForTurning( 100 );
			}
		}
		break;

	case STATE_ID_BRAND_SELECTION:
		screen->frontImage->setVisible( true );
		screen->backgroundImage->setVisible( false );
		screen->warning->setAlpha( 255 );
		state.qualitiesClicked = 0;
		state.piecesWaitingForHiding = 0;
		state.piecesConverted = 0;
		state.selectedBrand = BRAND_NONE;
		for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES; i++ )
		{
			Brand brand = i < screen->TOTAL_NUMBER_OF_PIECES / 2? BRAND_RED : BRAND_GOLD;
			int index = getNextIndex( brand, i );
			unsigned int timeForTurning = ( brand == BRAND_RED? i * 1400 : ( ( i - ( screen->TOTAL_NUMBER_OF_PIECES / 2 ) ) * 1400 + 600 ) );
			screen->mosaicPieces[ index ]->setTurnable( true );
			screen->mosaicPieces[ index ]->setTimeForTurning( timeForTurning );
			}
		break;

	case STATE_ID_CONVERTING_PIECES:
		for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES; i++ )
		{
			Brand brand = i < screen->TOTAL_NUMBER_OF_PIECES / 2? BRAND_RED : BRAND_GOLD;
			int index = getNextIndex( brand, i );
			PiecePtr p = screen->mosaicPieces[ index ];
			if( p->getBrand() == state.selectedBrand )
			{
				p->setTurnable( false );
			}
			else
			{
				turningTime += 120;
				p->setTimeForTurning( turningTime );
			}
		}
		break;

	case STATE_ID_HIDING_PIECES:
		state.blendTime = 0;
		screen->warning->setVisible( true );
		screen->frontImage->setVisible( false );
		screen->backgroundImage->setVisible( true );

		for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES; i++ )
		{
			int index = ( state.selectedBrand == BRAND_GOLD? screen->TOTAL_NUMBER_OF_PIECES - i - 1: i );
			PiecePtr p = screen->mosaicPieces[ index ];
			turningTime += 100;
			p->setTurnable( true );
			p->setSequenceIndex( 0 );
			p->setTimeForTurning( turningTime );
		}
		break;

	case STATE_ID_HIGHLIGHTING_QUALITIES:
		screen->warning->setVisible( false );
		break;
	}
}

void AppStateManager::onSequenceEnded( Piece* const piece )
{
	switch( state.id )
	{
	case STATE_ID_BRAND_SELECTION:
		piece->setSequenceIndex( 0 );
		break;

	case STATE_ID_RESETTING:
		piece->setSequenceIndex( 0 );
		piece->setTurnable( false );
		if( piece->isConverted() )
		{
			piece->setConverted( false );
			state.piecesConverted--;
			if( state.piecesConverted == 0 )
			{
				setState( STATE_ID_BRAND_SELECTION );
			}
		}
		break;

	case STATE_ID_CONVERTING_PIECES:
		if( piece->getBrand() == state.selectedBrand )
		{
			piece->setSequenceIndex( 0 );
			piece->setTurnable( false );

			if( piece->isConverted() )
			{
				piece->setConverted( false );
				state.piecesConverted++;
				IwTrace( MYAPP, ( "pieceConverted: %d", state.piecesConverted ) );
				//if( state.piecesConverted >= screen->TOTAL_NUMBER_OF_PIECES / 2 )
				//	setState( STATE_ID_HIDING_PIECES );
			}
		}

		//if( state.piecesConverted == screen->TOTAL_NUMBER_OF_PIECES / 2 && 
		//	state.piecesWaitingForHiding == screen->TOTAL_NUMBER_OF_PIECES / 2 )
		//		setState( STATE_ID_HIDING_PIECES );

		break;
	}
}

bool AppStateManager::onNextFrame( Piece* const piece, int nextFrameIndex )
{
	if( nextFrameIndex != screen->MOSAIC_PIECE_HIDING_FRAME_INDEX )
		return true;

	switch( state.id )
	{
	case STATE_ID_CONVERTING_PIECES:
		if( piece->getBrand() != state.selectedBrand )
		{
			screen->convertPiece( piece, state.selectedBrand );
		}
		break;

	case STATE_ID_HIDING_PIECES:
		hidePiece( piece );
		return false;
	}
	return true;
}

void AppStateManager::hidePiece( Piece* const piece )
{
	if( piece->isVisible() )
	{
		//screen->numberOfPiecesVisible--;
		piece->hide();

		if( screen->getNumberOfPiecesVisible() == 0 )
			setState( STATE_ID_HIGHLIGHTING_QUALITIES );
	}
}

void AppStateManager::showPiece( int pieceIndex )
{
	if( !screen->mosaicPieces[ pieceIndex ]->isVisible() )
	{
		screen->showPiece( pieceIndex, nextBrandToTurn );
		if( screen->getNumberOfPiecesVisible() == screen->TOTAL_NUMBER_OF_PIECES )
		{
			setState( STATE_ID_BRAND_SELECTION );
		}
	}
}


void AppStateManager::onPointerPressed( const s3ePointerEvent* ev )
{
	if( !ev->m_Pressed )
		return;

	Point clickPoint = Point( ev->m_x, ev->m_y );

	switch( state.id )
	{
	case STATE_ID_BRAND_SELECTION:
		for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES; i++ )
		{
			if( screen->mosaicPieces[ i ]->contains( clickPoint ) )
			{
				selectBrand( screen->mosaicPieces[ i ]->getBrand() );
			}
		}
		break;

	case STATE_ID_HIGHLIGHTING_QUALITIES:
		if( screen->highlight->getPlaying() )
			return;

		if( state.qualitiesClicked == screen->NUMBER_OF_QUALITIES )
		{
			setState( STATE_ID_RESETTING );
		}

		for( int i = 0, size = screen->qualities.size(); i < size; i++ )
		{
			if( screen->qualities[ i ]->pressButton( clickPoint ) )
			{
				state.qualitiesClicked++;
			}
		}

		if( state.qualitiesClicked == screen->NUMBER_OF_QUALITIES - 1 && !screen->qualities[ screen->NUMBER_OF_QUALITIES - 1 ]->isVisible() )
		{
			screen->qualities[ screen->NUMBER_OF_QUALITIES - 1 ]->setVisible( true );
			screen->qualities[ screen->NUMBER_OF_QUALITIES - 1 ]->setState( QUALITY_STATE_FADING_IN );
		}
		break;
	}
}

//void AppStateManager::updateMosaic( unsigned int dt )
//{
//	state.pieceTurningTime += dt;
//
//	//if( isTimeToTurnAnotherPiece() )
//	//{
//	//	state.pieceTurningTime = 0;
//	//	int nextPieceIndexToBeTurned = calculateNextPieceToTurnIndex();
//
//	//	lastIndexToBeTurned = nextPieceIndexToBeTurned;
//	//	switch( state.id )
//	//	{
//	//	case STATE_ID_RESETTING:
//	//		if( nextPieceIndexToBeTurned > -1 )
//	//		{
//	//			showPiece( nextPieceIndexToBeTurned );
//	//		}
//	//		nextBrandToTurn = ( nextBrandToTurn == BRAND_GOLD? BRAND_RED: BRAND_GOLD );
//	//		break;
//
//	//	//case STATE_ID_FADING_ANIMATION:
//	//	case STATE_ID_CONVERTING_PIECES:
//	//		if( nextPieceIndexToBeTurned > -1 )
//	//		{
//	//			screen->turnPiece( nextPieceIndexToBeTurned );
//	//		}
//	//		//if( state.piecesConverted >= screen->TOTAL_NUMBER_OF_PIECES / 2 )
//	//		//{
//	//		//	nextBrandToTurn = ( nextBrandToTurn == BRAND_GOLD? BRAND_RED: BRAND_GOLD );
//	//		//}
//	//		//else
//	//		//{
//	//			nextBrandToTurn = ( state.selectedBrand == BRAND_GOLD? BRAND_RED: BRAND_GOLD );
//	//		//}
//	//		break;
//
//	//	case STATE_ID_HIDING_PIECES:
//	//		//screen->turnPiece( nextPieceIndexToBeTurned );
//	//		//nextBrandToTurn = ( nextBrandToTurn == BRAND_GOLD? BRAND_RED: BRAND_GOLD );
//	//		//break;
//
//	//	case STATE_ID_BRAND_SELECTION:
//	//		if( nextPieceIndexToBeTurned > -1 )
//	//		{
//	//			screen->turnPiece( nextPieceIndexToBeTurned );
//	//		}
//	//		nextBrandToTurn = ( nextBrandToTurn == BRAND_GOLD? BRAND_RED: BRAND_GOLD );
//	//		break;
//	//	}			
//	//}
//}

void AppStateManager::updateBlendBackground( unsigned int dt )
{
	state.blendTime += dt;
	
	int blendBackgroundOpacity, warningBlendOpacity;
	switch( state.id )
	{
	case STATE_ID_CONVERTING_PIECES:
		return;

	case STATE_ID_HIDING_PIECES:
		blendBackgroundOpacity = NanoMath::clamp( ( 255 * state.blendTime ) / BLEND_HIDE_TIME, 0, 255 );
		warningBlendOpacity = NanoMath::clamp( ( 255 * state.blendTime ) / 3400, 0, 255 );
		screen->blendGrid( blendBackgroundOpacity, warningBlendOpacity );
		break;

	case STATE_ID_RESETTING:
		blendBackgroundOpacity = 255 - NanoMath::clamp( ( 255 * state.blendTime ) / BLEND_SHOW_TIME, 0, 255 );
		screen->blendGrid( blendBackgroundOpacity, blendBackgroundOpacity );
		break;
	}
}

//bool AppStateManager::isTimeToTurnAnotherPiece()
//{
//	switch( state.id )
//	{
//	case STATE_ID_BRAND_SELECTION:
//	case STATE_ID_RESETTING:
//		return state.pieceTurningTime > TIME_FOR_A_NEW_PIECE_TO_TURN;
//		break;
//
//	case STATE_ID_CONVERTING_PIECES:
//	case STATE_ID_HIDING_PIECES:
//		return state.pieceTurningTime > TIME_FOR_PIECE_CONVERSION;
//		break;
//	}
//}

void AppStateManager::selectBrand( Brand brand )
{
	state.selectedBrand = brand;
	setState( STATE_ID_CONVERTING_PIECES );
	screen->loadBrandFullScreen( brand );
}

//int AppStateManager::calculateNextPieceToTurnIndex()
//{
//	for( int i = 0; i < screen->TOTAL_NUMBER_OF_PIECES / 2; i++ )
//	{
//		int nextIndex = getNextIndex( i );
//
//		switch( state.id )
//		{
//		case STATE_ID_RESETTING:
//			if( screen->mosaicPieces[ nextIndex ]->isAvailableForShowing() )
//			{
//				return nextIndex;
//			}
//			break;
//
//		case STATE_ID_BRAND_SELECTION:
//		case STATE_ID_HIDING_PIECES:
//			if( screen->mosaicPieces[ nextIndex ]->isAvailableForTurning() )
//			{
//				return nextIndex;
//			}
//			break;
//
//		case STATE_ID_CONVERTING_PIECES:
//			if( screen->mosaicPieces[ nextIndex ]->isAvailableForConverting() && screen->mosaicPieces[ nextIndex ]->getBrand() != state.selectedBrand )
//			{	
//				IwTrace( MYAPP, ( "Next piece to convert: %d", nextIndex ) ); 
//				return nextIndex;
//			}
//			break;
//		}
///*
//		STATE_ID*/
//
//		//if( state.id == STATE_ID_RESETTING )
//		//{
//		//	if( screen->mosaicPieces[ nextIndex ]->isAvailableForShowing() )
//		//		break;
//		//}
//		//else
//		//{
//		//	if( screen->mosaicPieces[ nextIndex ]->isAvailableForTurning() )
//		//		break;
//		//}
//	}
//
//	// not found
//	return -1;
//}

int AppStateManager::getNextIndex( Brand brand, int i )
{
	int nextIndex = ( brand == BRAND_GOLD?  ( screen->TOTAL_NUMBER_OF_PIECES / 2 ) : 0 );
	nextIndex += ( i * SET_GENERATOR_PRIME ) % ( screen->TOTAL_NUMBER_OF_PIECES / 2 );
	return nextIndex;
}