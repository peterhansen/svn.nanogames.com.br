#include "quality.h"

#include <s3eVideo.h>

#include <components/pattern.h>

Quality::Quality( int id ) :
	DrawableGroup(),
	time( 0 ),
	id( id ),
	state( QUALITY_STATE_OFF )
{
	// dummy drawables
	insertDrawable( PatternPtr( new Pattern( CreateColor( 0, 0, 0, 0 ) ) ) );
}

void Quality::setState( QualityState state )
{
	this->state = state;
	time = 0;

	switch( this->state )
	{
	case QUALITY_STATE_FADING_IN:
		drawables[ 0 ]->setVisible( true );
		break;

	case QUALITY_STATE_TURNING_ON:
		highlight->onQualityClicked( id );
		break;
	}
}

void Quality::update( unsigned int dt )
{
	DrawableGroup::update( dt );

	switch( state )
	{
	case QUALITY_STATE_FADING_IN:
		time += dt;
		drawables[ 0 ]->setAlpha( ( time * INITIAL_ALPHA ) / FADE_IN_TIME );
		if( time > FADE_IN_TIME  )
		{
			setState( QUALITY_STATE_WAITING_FOR_CLICK );
		}
		break;

	case QUALITY_STATE_TURNING_ON:
		time += dt;
		drawables[ 0 ]->setAlpha( 127 + NanoMath::clamp( ( time * 4 * 127 ) / TURNING_ON_TIME, 0, 127 ) );
		if( time > TURNING_ON_TIME  )
		{
			setState( QUALITY_STATE_ON );
		}
		break;
	}
}

void Quality::setButtonAndHightlight( DrawablePtr button, BigSpritePtr highlight )
{
	drawables[ 0 ] = button;
	button->setVisible( false );
	button->setAlpha( 0 );
	this->highlight = highlight;
	time = 0;
}

bool Quality::pressButton( Point p )
{
	if( drawables[ 0 ]->contains( p ) && state == QUALITY_STATE_WAITING_FOR_CLICK )
	{
		setState( QUALITY_STATE_TURNING_ON );
		return true;
	}
	else
		return false;
}