#ifndef MAINSCREEN_H
#define MAINSCREEN_H

#include <components/defines.h>
#include <components/listeners.h>
#include <components/drawableimage.h>

// TODO: n�o ser�o usados
#include <components/pattern.h>
//#include <components/label.h>
// TODO: n�o ser�o usados

#include <components/spritelistener.h>
#include <components/gamemanager.h>

#include "constants.h"
#include "piece.h"
#include "quality.h"
#include "bigsprite.h"
#include "appstate.h"

class MainScreen : public Screen, public SpriteListener
{
	friend class AppStateManager;

public:

	void terminate();

	MainScreen();

	void setSize( Point size );
	void update( unsigned int dt );

	void onSequenceEnded( Sprite* const sprite, int sequenceIndex );
	bool onNextFrame( Sprite* const sprite, int nextFrameIndex );

	void keyboardKeyCB( const s3eKeyboardEvent* ev ) {}
	void keyboardCharCB( const s3eKeyboardCharEvent *ev ) {}
	void touchButtonCB( const s3ePointerTouchEvent *ev );
	void touchMotionCB( const s3ePointerTouchMotionEvent *ev ) {}
	void pointerButtonCB( const s3ePointerEvent *ev );
	void pointerMotionCB( const s3ePointerMotionEvent *ev ) {}
	void devicePauseCB() {}
	void deviceUnpauseCB() {}

protected:

	// TODO: nao usaremos!
	/*FontPtr font;*/

	static const int NUMBER_OF_MOSAIC_ROWS = 5;
	static const int NUMBER_OF_MOSAIC_COLUMNS = 6;
	static const int NUMBER_OF_QUALITIES = 3;
	static const int TOTAL_NUMBER_OF_PIECES = NUMBER_OF_MOSAIC_ROWS * NUMBER_OF_MOSAIC_COLUMNS;

	static const int MOSAIC_PIECE_X_OFFSET = 2;
	static const int MOSAIC_PIECE_Y_OFFSET = -5;
	static const int MOSAIC_PIECE_WIDTH = 132;
	static const int MOSAIC_PIECE_HEIGHT = 94;
	static const int MOSAIC_PIECE_HIDING_FRAME_INDEX = 11;

	//static const int QUALITY_POSITION_X = 90;
	//static const int QUALITY_OFFSET_X = 30;
	//static const int QUALITY_POSITION_Y = 60;
	//static const int QUALITY_OFFSET_Y = 75;

	static const int GOLD_HIGHLIGHT_X_POS = 53;
	static const int RED_HIGHLIGHT_X_POS = 427;

	CIwArray<Point> goldQualityPositions;
	CIwArray<Point> redQualityPositions;

	static const int SCREEN_WIDHT = 800;
	static const int SCREEN_HEIGHT = 480;

	AppStatePtr state;

	DrawableImagePtr frontImage;

	DrawablePtr warning;
	CIwArray<DrawableImagePtr> warningImages;

	PatternPtr backgroundImage;
	CIwArray<DrawableImagePtr> backgroundImages;

	CIwArray<QualityPtr> qualities;
	CIwArray<DrawableImagePtr> qualityImages;
	
	BigSpritePtr highlight;

	CIwArray<PiecePtr> mosaicPieces;
	CIwArray<PatternPtr> gridPatterns;
	CIwArray<ImagePtr> stillImages;

	void linearInterpolateAlpha( DrawablePtr drawable, int a, int b, int x );

	inline void insertInvisibleDrawable( DrawablePtr drawable )
	{
		drawable->setVisible( false );
		insertDrawable( drawable );
	}

	int getPieceIndex( Piece* piece );
	int getNumberOfPiecesVisible();

	void addNewPiece( Brand brand, int pieceImageIndex, DrawableImagePtr stillPiece );
	PiecePtr createPiece( Brand brand, int imageIndex, DrawableImagePtr stillPiece );

	void turnPiece( int pieceIndex );
	void showPiece( int pieceIndex, Brand newBrand );

	void convertPiece( Piece* piece, Brand brand );
	void replacePiece( Piece* piece, PiecePtr newPiece, DrawableImagePtr stillImage, DrawableImagePtr newStillImage );

	void loadBrandFullScreen( Brand brand );
	void blendGrid( int opacity, int warningOpacity );

	DrawableImagePtr getBackgroundImage( Brand brand );
	DrawableImagePtr getWarningImage( Brand brand );
	DrawableImagePtr getQualityImage( Brand brand, int index );

	std::string getQualityImageName( Brand brand, int index );
	std::string getBrandBackgroundImageName( Brand brand );
	std::string getWarningImageName( Brand brand );
};
#define MainScreenPtr		boost::shared_ptr<MainScreen>

#endif