[AppInfo]
AppVersion			Versão da aplicação, no formato "X.Y.Z". Utilizado pelo módulo Nano Online.
AppShortName		Nome código da aplicação, deve possuir quatro dígitos. Utilizado pelo módulo Nano Online. 

[Language]
DefaultLanguage		Variável de localização. Indica língua padrão utilizada para os textos da aplicação. Ver "defines.h" para os códigos das línguas.