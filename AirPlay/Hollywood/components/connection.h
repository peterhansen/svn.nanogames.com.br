/** \file network.h
	13-06-2011
*/
#include <string>

#include <IwHTTP.h>

#include <components\defines.h>

#ifndef CONNECTION_H
#define CONNECTION_H

/** \enum HTTPStatus
	\brief Descreve o estado de uma requisição HTTP.
	@see Connection
*/
enum HTTPStatus
{
	HTTP_READY,
	HTTP_WAITING_RESPONSE,
	HTTP_OK,
	HTTP_ERROR
};

// forward declarations
int32 HTTP_HEADERS_CB( void *sysData, void *userData );
int32 HTTP_DATA_CB( void *sysData, void *userData );

/** \struct Connection */
struct Connection
{
	CIwHTTP *httpRequest;
	HTTPStatus status;
	char* result;
	int resultLength;
	s3eCallback responseCB;

	Connection() :
		httpRequest( new CIwHTTP ),
		status( HTTP_READY ),
		result( NULL ),
		resultLength( -1 ),
		responseCB( NULL )
	{}

	~Connection()
	{
		if (httpRequest)
			httpRequest->Cancel();
		delete httpRequest;
		s3eFree(result);
	}

	inline void setRequestHeader( std::string header, std::string value ) 
	{ 
		httpRequest->SetRequestHeader( header.data(), value );
	}

	// TODO: levar p/ cpp
	void get( const char *uri, s3eCallback callback )
	{
		responseCB = callback;
		if( httpRequest->Get( uri, HTTP_HEADERS_CB, this ) )
			status = HTTP_WAITING_RESPONSE;
	}

	// TODO: levar p/ cpp
	void post( const char *uri, const char *body, int32 bodyLength, s3eCallback callback ) 
	{
		responseCB = callback;
		if( httpRequest->Post( uri, body, bodyLength, HTTP_HEADERS_CB, this ) )
			status = HTTP_WAITING_RESPONSE;
	}

	void getHeaders()
	{
		if (httpRequest->GetStatus() == S3E_RESULT_ERROR)
		{
			// Something has gone wrong
			status = HTTP_ERROR;
		}
		else
		{
			// Depending on how the server is communicating the content
			// length, we may actually know the length of the content, or
			// we may know the length of the first part of it, or we may
			// know nothing. ContentExpected always returns the smallest
			// possible size of the content, so allocate that much space
			// for now if it's non-zero. If it is of zero size, the server
			// has given no indication, so we need to guess. We'll guess at 1k.
			resultLength = httpRequest->ContentExpected();
			if (!resultLength)
			{
				resultLength = 1024;
			}

			//char **resultBuffer = secureTest ? &secureResult: &result;

			s3eFree(result);
			result = (char*)s3eMalloc(resultLength + 1);
			(result)[resultLength] = 0;
			httpRequest->ReadContent( result, resultLength, HTTP_DATA_CB, this );
		}
	}

	void getData()
	{
		if (httpRequest->GetStatus() == S3E_RESULT_ERROR)
		{
			// Something has gone wrong
			status = HTTP_ERROR;
		}
		else if (httpRequest->ContentReceived() != httpRequest->ContentLength())
		{
			// We have some data but not all of it. We need more space.
			uint32 oldLen = resultLength;

			// If iwhttp has a guess how big the next bit of data is (this
			// basically means chunked encoding is being used), allocate
			// that much space. Otherwise guess.
			if (resultLength < httpRequest->ContentExpected())
				resultLength = httpRequest->ContentExpected();
			else
				resultLength += 1024;

			// Allocate some more space and fetch the data.
			result = (char*)s3eRealloc(result, resultLength);
			httpRequest->ReadContent( &result[oldLen], resultLength - oldLen, HTTP_DATA_CB, this );
		}
		else
		{
			// We've got all the data.
			status = HTTP_OK;
			responseCB( NULL, result );
		}
	}
};

/** \class Connection
	\brief Encapsula uma conexão e gerencia requisições HTTP.
*/
/*class Connection
{
public:

	Connection() : httpConn( new CIwHTTP ) {}

	// TODO: levar p/ cpp
	~Connection() 
	{
		if ( httpConn )
			httpConn->Cancel();
		delete httpConn;
	}

	/** Retorna estado atual da conexão. 
	inline HTTPStatus getStatus() { return status; }

	/** Retorna objeto encapsulado CIwHTTP corrente. 
	//inline CIwHTTP* getIwConn()	{ return httpConn; }

	/** Adiciona cabeçalho de requisição. 
	// TODO: levar p/ cpp
	inline void setRequestHeader( std::string header, std::string value ) { httpConn->SetRequestHeader( header.data(), value ); }

	// TODO: levar p/ cpp
	bool get( const char *uri, s3eCallback callback ) 
	{
		cb = callback;
		if( httpConn->Get( uri, headersCB, this ) )
			status = HTTP_WAITING_RESPONSE;
	}

	// TODO: levar p/ cpp
	bool post( const char *uri, const char *body, int32 bodyLength, s3eCallback callback ) 
	{
		cb = callback;
		if( httpConn->Post( uri, body, bodyLength, headersCB, this ) )
			status = HTTP_WAITING_RESPONSE;
	}

protected:

	/** Tabela com caracteres utilizados na codificação do formato base64. */
	//static const unsigned char base64_table[65];

	/** base64_encode - Base64 encode. ATTENTION! Caller is responsible for freeing the returned buffer. 
		Returned buffer is null terminated to make it easier to use as a C string. The null terminator is
		not included in out_len.
		@param src Data to be encoded
		@param len Length of the data to be encoded
		@param out_len Pointer to output length variable, or NULL if not used
		@return Allocated buffer of out_len bytes of encoded data or NULL on failure.
	 */
	//static char* base64_encode( const char *src, size_t len, size_t *out_len );

	/** Callback para quando os dados de uma requisição foram recebidos. 
	int32 dataCB( void*, void* );
	//s3eCallback dataCB;

	/** Callback para quando os cabeçalhos de uma requisição foram recebidos 
	int32 headersCB( void*, void* );
	//eCallback headersCB;

	CIwHTTP *httpConn;		//!< Conexão IwHTTP encapsulada pela classe.

	char *result;			//!< Resultado da requisição, caso ela tenha retornado com sucesso. Pode ser nulo.
	int resultLength;		//!< Tamanho do resultado da requisição, em número de caracteres.

	HTTPStatus status;		//!< Estado corrente da conexão.
	s3eCallback cb;			//!< Callback estabelecida pelo cliente da classe para notificações de recebimento de dados.
};*/
#endif