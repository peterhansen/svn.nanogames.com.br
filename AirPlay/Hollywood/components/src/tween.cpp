#include <tween.h>

Tween::Tween( int t, int iv, int fv ) : accTime( 0 ), time( t ), initialValue( iv ), finalValue( fv ) {
}


int Tween::update( uint dt ) {
	accTime = NanoMath::min( accTime + dt, time );

	return initialValue + ( int ) ( ( finalValue - initialValue ) * accTime / ( float ) time );
}
