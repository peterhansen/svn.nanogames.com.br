#include <Iw2D.h>
#include <IwGx.h>

#include <components\defines.h>
#include <components\sprite.h>
#include <components\spritelistener.h>

/** Caracter utilizado para indicar para o leitor de descritores o início de uma sequência de frames com durações fixas (o primeiro valor lido é a duração dos frames, e em seguida os índices da sequência). */
#define FRAMESET_DESCRIPTOR_SEQUENCE_FIXED 'f'

/** Caracter utilizado para indicar para o leitor de descritores o início de uma sequência de frames com durações variáveis. Os valores presentes na sequência devem sempre obedecer a ordem índice - duração. */
#define FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE 'v'


typedef enum {
	READ_STATE_FRAMES_SIZE,
	READ_STATE_TOTAL_FRAMES,
	READ_STATE_FRAMES_INFO,
	READ_STATE_READ_SEQUENCE_TYPE,
	READ_STATE_FIXED_READ_TIME,
	READ_STATE_FIXED_READ_INDEXES,
	READ_STATE_VARIABLE_READ_TIME,
	READ_STATE_VARIABLE_READ_INDEX
} readState;


Sprite::Sprite( ImagePtr image, std::string seqFile ) : Drawable(),
	image( image ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( false ),
	frameIndex( 0 ),
	paused( false )
{
	readSequence( s3eFileOpen( seqFile.data(), "rb" ) );
}


Sprite::Sprite( std::string imageFile, std::string seqFile ) : Drawable(),
	image( ImagePtr( Iw2DCreateImage( imageFile.data() ) ) ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( true ),
	frameIndex( 0 ),
	paused( false )
{
	readSequence( s3eFileOpen( seqFile.data(), "rb" ) );
}


Sprite::Sprite( std::string prefix ) : Drawable(),
	image( ImagePtr( Iw2DCreateImage( ( prefix + ".png" ).data() ) ) ),
	sequenceIndex( 0 ),
	accTime( 0 ),
	scaleToSize( true ),
	frameIndex( 0 ),
	paused( false )
{
	readSequence( s3eFileOpen( ( prefix + ".bin" ).data(), "rb" ) );
}


int Sprite::getWarpedIndex( int frameIndex )
{
	return NanoMath::absmod( frameIndex, sequences[sequenceIndex].size() );
}

void Sprite::setSequenceIndex( uint8 sequenceIndex )
{
	this->sequenceIndex = sequenceIndex; 
	frameIndex = 0;
}

void Sprite::nextFrame()
{
	int nextFrameIndex = getWarpedIndex( frameIndex + 1 );
	if( listener )
	{
		if( !listener->onNextFrame( this, nextFrameIndex ) )
			return;
	}

	frameIndex = nextFrameIndex;

	if( frameIndex == 0 && listener )
	{
		listener->onSequenceEnded( this, sequenceIndex );
	}
}

void Sprite::previousFrame()
{
	frameIndex = getWarpedIndex( frameIndex - 1 );
}

void Sprite::update( unsigned int dt )
{
	// se o frame tiver duração 0 (zero), o controle da animação é feito externamente, através de chamadas explícitas
	// a nextFrame, previousFrame e etc.
	int frameTime = getCurrentSequence()[ frameIndex ].y;
	if ( frameTime > 0 && !paused )
	{
		accTime += dt;
		if ( accTime >= frameTime )
		{
			// trocou de frame
			accTime %= frameTime;
			nextFrame();
		}
	}
}

void Sprite::paint()
{
	Rectangle bounds = getCurrentFrame().bounds;
	if( !scaleToSize )
		Iw2DDrawImageRegion( image.get(), clipStack->translate, bounds.getSize(), bounds.getPosition(), bounds.getSize() );
		//Iw2DDrawImage( image.get(), clipStack->translate );
	else
		Iw2DDrawImageRegion( image.get(), clipStack->translate, size, bounds.getPosition(), bounds.getSize() );
		
}

void Sprite::readSequence( s3eFile* file ) {
	char line[ SEQUENCE_MAXCHARSLINE ];
	Point totalSize;
	int8 currentFrameRead = 0;
	int16 time = 0;
	char* charPtr;
	int8 totalFrames;
	Frame f;
	char* temp;
	Sequence s;

	readState state = READ_STATE_FRAMES_SIZE;
	bool continueLine = false;

	while ( continueLine || s3eFileReadString( line, SEQUENCE_MAXCHARSLINE, file ) ) {
		if ( !continueLine )
			charPtr = strtok( line, " \n\r\t" );

		continueLine = false;

		switch ( state ) {
			case READ_STATE_FRAMES_SIZE:
				totalSize.x = readNumber( &charPtr );
				totalSize.y = readNumber( &charPtr );
				setSize( totalSize );
				state = READ_STATE_TOTAL_FRAMES;
			break;

			case READ_STATE_TOTAL_FRAMES:
				totalFrames = readNumber( &charPtr );
				state = READ_STATE_FRAMES_INFO;
			break;

			case READ_STATE_FRAMES_INFO:
			{
				f = Frame();
				int x = readNumber( &charPtr );
				int y = readNumber( &charPtr );
				Point p = Point( x, y );

				int w = readNumber( &charPtr );
				int h = readNumber( &charPtr );
				Point s = Point( w, h );

				f.bounds.set( p, s );
				f.offset.x = readNumber( &charPtr );
				f.offset.y = readNumber( &charPtr );
				frames.push_back( f );

				++currentFrameRead;

				if ( currentFrameRead >= totalFrames ) {
					state = READ_STATE_READ_SEQUENCE_TYPE;
				}
			}
			break;

			case READ_STATE_READ_SEQUENCE_TYPE:
				temp = charPtr;//strtok( charPtr, " " );
				if ( temp ) {
					char c = temp[ 0 ];
					charPtr = strtok( NULL, " " );
					state = ( c == FRAMESET_DESCRIPTOR_SEQUENCE_FIXED ? READ_STATE_FIXED_READ_TIME : READ_STATE_VARIABLE_READ_TIME );
					continueLine = true;
				}
			break;

			case READ_STATE_FIXED_READ_TIME:
				time = readNumber( &charPtr );
				state = READ_STATE_FIXED_READ_INDEXES;
				continueLine = true;
			break;

			case READ_STATE_FIXED_READ_INDEXES:
				//charPtr = strtok ( charPtr, " " );

				while ( charPtr && ( &charPtr != NULL ) ) {
					s.push_back( Point( readNumber( &charPtr ), time ) );
					//charPtr = strtok( NULL, " " );
				}
				state = READ_STATE_READ_SEQUENCE_TYPE;
			break;

			case READ_STATE_VARIABLE_READ_TIME:
				time = readNumber( &charPtr );
				if ( time >= 0 ) {
					state = READ_STATE_VARIABLE_READ_INDEX;
					continueLine = true;
				}
				// não é necessário tratar o "else", pois o tratamento adequado já foi feito dentro de readNumber()
			break;

			case READ_STATE_VARIABLE_READ_INDEX:
				int frameIndex = readNumber( &charPtr );
				// Se for um índice válido, o insere no array, e lê a próxima informação (duração do próximo frame)
				// Caso contrário, o tratamento já terá sido feito dentro de readNumber.
				if ( frameIndex >= 0 ) {
					s.push_back( Point( readNumber( &charPtr ), time ) );
					state = READ_STATE_VARIABLE_READ_TIME;
					continueLine = true;
				}
			break;
		}

		switch ( state ) {
			case READ_STATE_FIXED_READ_INDEXES:
			case READ_STATE_VARIABLE_READ_INDEX:
				if ( s.size() > 0 )
					sequences.push_back( s );

				s = Sequence();
			break;
		}
	}

	if ( s.size() > 0 )
		sequences.push_back( s );

	printf( "SEQUENCES: %d\n", sequences.size() );

	s3eFileClose( file );
}

Frame& Sprite::getCurrentFrame()
{
	return frames[ getCurrentSequence()[ frameIndex ].x ];
}

Sequence& Sprite::getCurrentSequence()
{
	return sequences[ sequenceIndex ];
}