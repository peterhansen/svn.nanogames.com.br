#include <components\defines.h>
#include <components\pattern.h>

Pattern::Pattern( Color color ) : Drawable()
{
	this->color = color;
}

Pattern::Pattern( ImagePtr image ) : Drawable(), image(image)
{}

void Pattern::paint()
{
	if( image == NULL )
	{
		// caso no qual pattern se comporta como cor s�lida
		saveAndSetColorState( color );
		Iw2DFillRect( clipStack->translate, size );
		restoreColorState();
	}
	else
	{
		// caso pattern seja uma repeti��o stencil de uma imagem
		int imageW = image->GetWidth();
		int imageH = image->GetHeight();
		int totalI = 1 + size.x / imageW;
		int totalJ = 1 + size.y / imageH;
		for( int i = 0; i < totalI; i++ )
		{
			for( int j = 0; j < totalJ; j++ )
			{
				Point translation = Point( i * imageW , j * imageH );
				Iw2DDrawImage( image.get(), clipStack->translate + translation );
			}
		}
	}
}