#include <string>

#include <IwGxFont.h>

#include <components\defines.h>
#include <components\label.h>
#include <components\rectangle.h>
#include <IwGx.h>

Label::Label( FontPtr font, std::string text )
	: Drawable(),
	font(font),
	text(text),
	horAlign(IW_GX_FONT_ALIGN_LEFT), 
	verAlign(IW_GX_FONT_ALIGN_TOP), 
	formatFlags(IW_GX_FONT_DEFAULT_F)
{
	setColor( 0x000000ff );
}

void Label::setFormat(	FontHAlignment horAlign, FontVAlignment verAlign, FontFormatFlags formatFlags )
{
	this->horAlign = horAlign;
	this->verAlign = verAlign;
	this->formatFlags = formatFlags;
}

void Label::paint()
{
	// TODO: saveFontState
		CIwRect prevRect = IwGxFontGetRect();
		const Font* prevFont = IwGxFontGetFont();
		Color prevFontColor = IwGxFontGetCol();
		FontVAlignment prevVAlign = IwGxFontGetAlignmentVer();
		FontHAlignment prevHAlign = IwGxFontGetAlignmentHor();
		FontFormatFlags prevFlags = IwGxFontGetFlags();
	// TODO: saveFontState

	IwGxLightingOn();

	// TODO: ficar formatando a cada paint CERTAMENTE n�o � a melhor forma de se fazer.
	// Caso a marcha fique pesada, vamos usar um preparedData para desenho de textos, onde
	// se pr�-fomata o texto.
	IwGxFontSetFont( font.get() );
	IwGxFontSetRect( CIwRect( clipStack->translate.x, clipStack->translate.y, size.x, size.y ) );
	IwGxFontSetAlignmentHor( horAlign );
	IwGxFontSetAlignmentVer( verAlign );
	IwGxFontSetFlags( formatFlags );

	// TODO: para que essa linha funcione, precisamos ligar a componente emissiva do IwGx.
	// Tirando do manual: "Fonts are coloured using emissive lighting, 
	// so iwgx's emissive lighting must be enabled for colour to take effect."
	IwGxFontSetCol( color );
	 
	IwGxFontDrawText( text.data() );

	// TODO: restoreFontState
		IwGxFontSetRect( prevRect );
		IwGxFontSetCol( prevFontColor );
		IwGxFontSetAlignmentHor( prevHAlign );
		IwGxFontSetAlignmentVer( prevVAlign );
		IwGxFontSetFlags( prevFlags );
		if( prevFont )
			IwGxFontSetFont( prevFont );
	// TODO: restoreFontState
}