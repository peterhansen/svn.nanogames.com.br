/** \file
	Rectangle.h
	24-05-2011
*/
#include <components\defines.h>
#include <IwGxTypes.h>

#ifndef RECTANGLE_H
#define RECTANGLE_H

/** \class Rectangle
	\brief Representante de um retângulo 2D.
*/
class Rectangle
{
public:
	Rectangle() : box( CIwRect( 0, 0, 0, 0 ) ) {}
	Rectangle( Point topLeft, Point size ) : box ( CIwRect( topLeft.x, topLeft.y, size.x, size.y ) ) {}
	
	inline Point getPosition() { return Point(box.x, box.y); }
	inline Point getSize() { return Point(box.w, box.h); }
	
	/** Redefine coordenadas do retângulo. */
	inline void set( Point topLeft, Point size ) { box.x = topLeft.x; box.y = topLeft.y; box.w = size.x; box.h = size.y; }

	/** Verifica se ponto está contido no retângulo. */
	bool contains( Point p );

	/** Recalcula retângulo como interseção resultante. */
	Rectangle calcIntersection( Rectangle r );

private:
	CIwRect box;		//!< Caixa envolvente do retângulo.
};
#endif