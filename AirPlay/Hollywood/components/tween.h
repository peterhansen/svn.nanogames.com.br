#ifndef TWEEN_H
#define TWEEN_H

#include <components/defines.h>
//#include <IwUI.h>
#include <IwArray.h>


#define TweenPtr	boost::shared_ptr<Tween>

class Tween {

public:
	Tween( int time, int initialValue, int finalValue );

	int update( uint delta );

	inline int getAccTime() { return accTime; }

	inline int getPercent() { return accTime * 100 / time; }

protected:

	int time;

	int accTime;

	int initialValue;
	
	int finalValue;

private:

};
#endif