/** \file sprite.h
	27/05/2011
*/
#ifndef SPRITE_H
#define SPRITE_H

#include <string>

#include <Iw2D.h>
#include <IwGx.h>
#include <s3e.h>

#include <components\defines.h>
#include <components\drawable.h>

class SpriteListener;

/** \struct Frame 
	\brief Frame da animação de uma sequência de um sprite. 
*/
struct Frame
{ 
	// TODO: fazer essa struct ser privada?
	Rectangle bounds;			//!< Retângulo envolvente de recorte dentro da spritesheet.
	Point offset;				//!<
};

/** \class Sprite 
	\brief Um sprite com diferentes sequências de animação. 
*/
class Sprite : public Drawable
{
public:
	/**
	Construtor da classe Sprite.
		@param image Imagem para ser usada como spritesheet.
		@param seqFile Arquivo .seq que descreve as sequencias do sprite. Para saber mais sobre o arquivo .ser,
		leia sobre o método readSequence.
		@see readSequence
	*/
	Sprite( ImagePtr image, std::string seqFile );
	
	Sprite( std::string imageFile, std::string seqFile );

	Sprite( std::string prefix );

	~Sprite() { sequences.clear_optimised(); }

	/** Incrementa o índice do frame da sequência atual. */
	void nextFrame();

	/** Decrementa o índice do frame da sequência atual. */
	inline void previousFrame();

	/** Atualiza o índice da sequência do sprite, caso o índice seja válido. Esse método
		zera o índice de frame da sequência automaticamente. */
	void setSequenceIndex( uint8 sequenceIndex );

	inline void setFrameIndex( int frameIndex ) { this->frameIndex = getWarpedIndex( frameIndex ); }

	inline uint8 getFrameSequenceIndex() { return ( uint8 ) getCurrentSequence()[ frameIndex ].x; }

	inline uint8 getSequenceIndex() const { return sequenceIndex; }

	void update( unsigned int dt );

	inline void setListener( SpriteListener* listener ) { this->listener = listener; }

protected:

	CIwArray<Frame> frames;
	CIwArray<Sequence> sequences;	//!< Sequencias de frames usadas por esse sprite.
	uint8 sequenceIndex;			//!< Indice da sequencia corrente. Caso esteja como -1, significa que esse sprite nao possui sequencias.

	int16 accTime;					//!< Tempo acumulado no frame atual.
	const ImagePtr image;			//!< Imagem que contem os frames da animacao.
	uint8 frameIndex;				//!< Indice corrente de acesso ao frame da sequencia (nao equivale ao frame da sequencia em si).
	
	bool paused;					//!< Indica se a animacao do sprite esta pausada no momento.

	SpriteListener* listener;		//!< Listener do sprite, o qual recebe eventos específicos do sprite.

	void paint();
	Frame& getCurrentFrame();
	Sequence& getCurrentSequence();

	// TODO: remover essa função e fazer com que setar o frameIndex seja
	// possível de causar problemas? Talvez seja melhor, para não causar surpresas
	int getWarpedIndex( int frameIndex );

private:

	bool scaleToSize;				//!< Caso o sprite deva sofrer escala de forma que preencha seu tamanho.

	/** Número máximo de caracteres por linha na leitura de um arquivo de sequência (.seq) */
	static const int SEQUENCE_MAXCHARSLINE = 300;

	// TODO: explicar arquivo .seq melhor
	void readSequence( s3eFile* file );

	inline static int readNumber( char** line ) { 
		int value = strtol( *line, NULL, 10 ); 
		*line = strtok( NULL, " \r\n\t" ); 
		return value;
	}
};

#endif