/** \file listeners.h
	2011-06-03
*/
#ifndef SPRITE_LISTENER_H
#define SPRITE_LISTENER_H

#include <components/defines.h>
#include <components/sprite.h>

class SpriteListener
{
public:
	virtual void onSequenceEnded( Sprite* const sprite, int sequenceIndex ) = 0;
	virtual bool onNextFrame( Sprite* const sprite, int nextFrameIndex ) = 0;
};

#endif