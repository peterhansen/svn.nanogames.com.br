/*
 *  ZoneGroup.h
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef ZONE_GROUP_H
#define ZONE_GROUP_H 1

#include "ObjectGroup.h"
#include "DotGameInfo.h"
/*
 
 gerencia as zonas (nome horrível, eu sei...), que aparecem no jogo, ou seja, é ele quem aloca, desaloca, conta quantas estão disponíveis, etc.
 
 */

class ZoneGame;
class DotGame;
class GameBaseInfo;
class Player;

class ZoneGroup : public ObjectGroup {
	
public:
	
	//construtor
	ZoneGroup( GameBaseInfo& g );
	
	//destrutor
	~ZoneGroup( void ){};
	
	//indica se ainda há zona livre
	bool haveFreeZone( void );
	
	//insere zonas no grupo, para gerenciamento
	void insertZones( ZoneGame* z[ MAX_ZONES_COLUMNS ][ MAX_ZONES_ROWS ] );
	
	void reset( void );
	
	void setQteZones( uint8 h, uint8 v );
	
	bool isZoneOccupied( Point3f *p );
	
	bool isZoneCoupled( Point3f *p );
	
	uint8 zoneGetPlayerId( Point3f *p );

	void setZoneCoupled( Point3f *p, bool b );
	
	bool zoneGainedByPlayer( Point3f *p, Player *pl);
	
	void zoneLostedByPlayer( Point3f *p/*, Player*p*/ );
	
	void setZoneVisible( Point3f *p, bool b );
	
	static Point3f getZonePositionByXY( uint8 x, uint8 y );

	static Point3f* getZonePPositionByXY( uint8 x, uint8 y );
	
	static 	void getZoneXYByPosition( Point3f* p , uint8* x = NULL ,uint8* y = NULL );

	void BigZoneGainedByPLayer( Point3f *p, Player *pl );

	void BigZoneLostedByPLayer( Point3f *p );
	
	bool zoneHavePowerUp( Point3f *p );

	inline	uint8 getnZonesH( void );

	inline	uint8 getnZonesV( void );	
private:
	
	uint8 nZonesH, nZonesV;
	
};
uint8 ZoneGroup::getnZonesH( void ){ return nZonesH; }

uint8 ZoneGroup::getnZonesV( void ){ return nZonesV; }


#endif