/*
 *  ZoneGame.mm
 *  dotGame
 *
 *  Created by Max on 10/9/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include"PowerUp.h"
#include "ZoneGame.h"
#include "Player.h"
#include "LineGame.h"
#include "GenericDoll.h"
#include "DotGame.h"
#include "DotGameInfo.h"
#include "Exceptions.h"
#include "Macros.h"
#include "RenderableImage.h"
#include "Sprite.h"
#include "Random.h"
#include "PowerUpFactory.h"
#include "ZoneGroup.h"


#define MAX_ZONES_SPRITES 2//fundo normal + grande fundo

#define INDEX_BACKGROUND_SPRITE 0
#define INDEX_BACKGROUND_BIG_SPRITE 1

ZoneGame::ZoneGame():ObjectGroup( MAX_ZONES_SPRITES ),
playerId( 255 ),
occupied( false ), 
coupled( false ), 
power( NULL )//,
/*zCoupledLeft( NULL ), 
zCoupledRight( NULL )*/{
	setZoneGameState( ZONE_GAME_STATE_NONE );
#if DEBUG
	setName("zonas\n");
#endif
	//PowerUpFactory::Create();
	setSize(ZONE_SIDE, ZONE_SIDE);
		
}

/*============================================================================
 
 adiciona as imagens dos jogadores nas zones
 
 =============================================================================*/
void ZoneGame::setPlayerImage( Player *p ){
	//todoo: colocar para carregar as imagens e os sprites de todos os players aqui!!!
//	Sprite* s1;
//	s1 = new Sprite( p->boneco->getZoneSprt() );
	
	
	RenderableImage *r;
	r = new RenderableImage();
	r->setImage( p->boneco->getZoneSprt() );
	r->setScale( 1.0f, 1.0f, 1.0f );
	r->setSize( ZONE_SIDE, ZONE_SIDE, 1.0f );
	
#if DEBUG
	r->setName("zona imagem\n ");
	NSLog(@"imagem inserida");
#endif
		insertObject( r );
	setZoneGameState( ZONE_GAME_STATE_BLANK );
	
}
/*============================================================================
 

 ============================================================================*/
void ZoneGame::setZoneGameState ( ZoneGameState z ){
	state = z;

	updateImage();
}

/*============================================================================
 
 quando uma zona é ganhada pelo jogador
 
 ============================================================================*/

bool ZoneGame::gainedByPlayer( /* DotGame *dg*/ Player *pl ){
#if DEBUG
	uint8 x,y;
	Point3f tmpl = *getPosition();
	ZoneGroup::getZoneXYByPosition( &tmpl , &x ,&y );
	NSLog(@"zona: %d,%d", x, y);

#endif
if( occupied )
	return false;
	pl->score++;
	playerId = pl->getId();
	setPlayerImage( pl );
	
	setZoneGameState( ZONE_GAME_STATE_OCCUPIED );
	if( power ){
		pl->setPowerUp( power );
		power = NULL;
	}
#if DEBUG
	NSLog(@"\nzona( %3.2f,%3.2f ) ganhada pelo jogador %d\n", position.x, position.y, playerId );
#endif
	return true;
}

/*============================================================================
 
 atualiza a imagem de acordo com o estado
 
 ============================================================================*/

void ZoneGame::updateImage( void ){

	switch ( state ) {
		case ZONE_GAME_STATE_NONE :
			break;
			
		case ZONE_GAME_STATE_BLANK :
			//removeObject( 0 );
			//static_cast<RenderableImage*> ( getObject( 0 ) )->setVisible( false );	
			//static_cast<RenderableImage*> ( getObject( 0 ) )->setVertexSetColor( COLOR_WHITE );
			break;
			
		case ZONE_GAME_STATE_OCCUPIED :
			occupied = true;
//			NSLog(@" zona ocupada é a zona na posição x=%.f, y=%.f, pelo jogador %d ",position.x,position.y, playerId);
			static_cast<RenderableImage*> ( getObject( 0 ) )->setVisible( true );
//			static_cast<RenderableImage*> ( getObject( 0 ) )->setVertexSetColor( COLOR_PURPLE );
			break;
		default:
			break;
	}
}

bool ZoneGame::render( void ){
	if( power != NULL ){
		power->render();

	}
	return ObjectGroup::render();
}


/*============================================================================
 
 reseta
 
 ============================================================================*/

void ZoneGame::reset( void ){
	setZoneGameState( ZONE_GAME_STATE_BLANK );
}
/*============================================================================
 
 posiciona na tela
 
 ============================================================================*/

void ZoneGame::setPosition( uint8 x, uint8 y ){
	Point3f p;
	//p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
//		 LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
//		  1.0f);
	p = ZoneGroup::getZonePositionByXY( x, y );
	position.set( p );
}
/*============================================================================
 
 retorna a identificação do jogador que ocupou a zona
 
 ============================================================================*/
uint8 ZoneGame::getPlayerId( void ){
		return playerId;
}

ZoneGame::~ZoneGame(){
	SAFE_DELETE( power );

}
/* =================================================================================================

 lostByPlayer=> chamado qdo um jogador perder uma zona...
 
 =================================================================================================*/
void ZoneGame::lostByPlayer( void/*Player *p*/ ){
	
	if( !occupied )
		return;
		
#if DEBUG
	 NSLog(@"zona( %3.3f,%3.3f ) perdida pelo jogador %d",position.x,position.y, playerId);
#endif
	
	removeObject( INDEX_BACKGROUND_SPRITE );
	playerId = -1;
	occupied = false;
	//coupled = false ;
	reset();
}
/*===================================================================================

 adicona um PowerUp à zona 
 
===================================================================================*/
void ZoneGame::setPowerUp( PowerUp* p ){
	power = p;
	p->setZone( this );

}

/*===================================================================================
 
qdo uma das zonas se torna a responsavel por ter a imagem
 
 ===================================================================================*/

void ZoneGame::isMainBigZone( Player *p ){
//por enquanto, depois trocar para sprite!!
	RenderableImage *r;
	r = new RenderableImage();
	r->setImage( p->boneco->getBigZoneSprt() );
	getObject( INDEX_BACKGROUND_SPRITE )->setVisible( false );
	insertObject( r );
}

/*=====================================================================================
 
 
 
 =====================================================================================*/

void ZoneGame::notIsMainBigZone( void ){ 
	removeObject( INDEX_BACKGROUND_BIG_SPRITE );
	getObject( INDEX_BACKGROUND_SPRITE )->setVisible( true );
}