/*
 *  LineGame.h
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

/*
 mudanças posteriores: a classe vai receber um sprite, e vai exibir cada uma uma animação( cercas relativas à cada uma das personagens existentes ), por cima da imagem básica( em função do cenário ), podendo ser cada uma delas em fç da personagem

 o resultado final pode ser que cada um ods framaes de animação seja o equivalente à uma linha de cada personagem. ele deve ter uma ligação para saber qual é a personagem de quem ele vai exibir a animação.
 */

#ifndef LINE_GAME_H
#define LINE_GAME_H 1

#include "Sprite.h"
#include "DotGameInfo.h"
#include "ObjectGroup.h"
#include "BoardFactory.h"

//#define LINE_GAME_HEIGHT 64.0f//50.0
//#define LINE_GAME_WIDTH 8.0

enum LineGameState {
	
	LINE_GAME_STATE_NONE = -1,
	LINE_GAME_STATE_OCCUPIED = 0,
	LINE_GAME_STATE_SELECTED,
	LINE_GAME_STATE_ANIMATING,
	LINE_GAME_STATE_OBSERVING,
	LINE_GAME_STATE_BLANK	
}typedef LineGameState;

enum LineGameOrientation {
	LINE_GAME_ORIENTATION_HORIZONTAL = 0,
	LINE_GAME_ORIENTATION_VERTICAL
	
} typedef  LineGameOrientation;

class Player;
class GenericDoll;
class ZoneGame;
class Background;


class LineGame : public ObjectGroup/*, public SpriteListener*/ {

public:
	//construtor
	LineGame( LineGameOrientation l/*, idBackground Bg*/ );
	
	//destrutor
	~LineGame();
	
	static void loadImages( void );
	static void removeImages( void );
	
//	static Sprite* getCloudLineH( void );
//	static Sprite* getCloudLineV( void );	
//	static Sprite* getCloudZone_11( void );
//	static Sprite* getCloudZone_22( void );
	
	//associa um id de um jogador à uma linha
	void associate( uint8 id );
	
	void freeImages( void );
	
	void setLineGameState( LineGameState l );
	 
	inline	LineGameState getLineGameState( void );
	
//	bool update( float timeElapsed );

	//desnecessário??
	void setPlayerImage(/* idBackground Bg/*Player *p[ MAX_PLAYERS ]*/ void );
	
	//procura conseguir para si a figura do rosto do boneco
	void configure( GenericDoll *g );
	
	void setPosition( uint x, uint y ); 
	
	void reset( void );
	
	//virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) ;
	virtual bool checkCollision( const Point3f* pPoint, Player *p  ) ;

	void updateImage( void );

//	para ser chamado após qq turno
	void restart( void );
	
	//para ser selecionada diretamente pela cpu
	void cpuSelected( void );
	
	inline	bool isOccupied( void );
	
	inline	bool isLocked( void );
	
	inline LineGameOrientation getLineGameOrientation( void );
	
	inline void setPosition( float x, float y, float z );
	
	void setSelected( bool b );
	
	void setLocked( bool b );
	
	void getBackgroundImage( Background *bg );
	
	void adjustLinesToBackground( int8 index );

	void adjustLinesToPlayer( int8 index );
	
	void lockLine( void );
	
	void lineGameCpuGained( Player *p );
	
	virtual bool render( void );
	
	void lineToLastState( void );
	
	void lostByPlayer( void/*Player *p*/ );
	
	uint8 getPlayerId( void );
	
	//static void setSpriteListener( SpriteListener* s );

	//virtual void onAnimEnded( Sprite* pSprite );

protected:
	void gainedByPlayer( Player *p );
	
	// ela pega a textura de acordo com o jogador: 

	//identificação do jogador que ocupou o lugar (depois mudar para uma cor?)
		uint8 idPlayer;
	
		LineGameState state,lastState;
	
		LineGameOrientation orientation;
		
		bool occupied, selected, observed, locked;
	
		Color lColor;
		
		static Sprite *linhaBgH, *linhaBgV, *linhaPlV , *linhaPlH ;
//		static Sprite *cloudH, *cloudV;
};


inline uint8 LineGame::getPlayerId( void ){
	return idPlayer;
}


//métodos inline =>definição

inline bool LineGame::isOccupied( void ){
	return occupied;
}

inline bool LineGame::isLocked( void ){
	return locked;
}

inline LineGameOrientation LineGame::getLineGameOrientation( void ){
	return orientation;
}

inline	LineGameState LineGame::getLineGameState( void ){
	return state;
}

inline void LineGame::setPosition( float x, float y, float z ){
	ObjectGroup::setPosition( x,y,z );
}

inline void LineGame::setSelected( bool b ){
	selected = b;
}

inline void LineGame::cpuSelected( void ){
	
	if ( state == LINE_GAME_STATE_SELECTED && selected && !locked) {
	setLineGameState( LINE_GAME_STATE_OCCUPIED );
	}
}

inline void  LineGame::lineToLastState( void ){
	observed = false;
	setLineGameState( lastState );
	//this->setLineGameState( lastState ); 
}

#endif
