/*
 *  LineGame.mm
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "LineGame.h"
#include "Player.h"
#include "Utils.h"
#include "RenderableImage.h"
#include "ResourceManager.h"
#include "DotGameInfo.h"
#include "ZoneGame.h"
#include "ObjcMacros.h"
#include "Quad.h"
#include "BoardFactory.h"
#include "LineGroup.h"

#define INDEX_LINE_BACKGROUND 0
#define INDEX_LINE_PLAYERS 1
#define INDEX_LINE_CLOUD 2

#define OBJECTS_INSIDE_LINE_GAME 3


Sprite* LineGame::linhaBgH = NULL; 
Sprite* LineGame::linhaBgV = NULL;
Sprite* LineGame::linhaPlV = NULL;
Sprite* LineGame::linhaPlH = NULL;

//Sprite* LineGame::cloudH = NULL;
//Sprite* LineGame::cloudV = NULL;


LineGame::LineGame( LineGameOrientation l/*, idBackground Bg*/ ):ObjectGroup( OBJECTS_INSIDE_LINE_GAME ),
orientation( l ),
state( LINE_GAME_STATE_BLANK ),
occupied( false ),
selected( false ),
observed( false ),
lColor( COLOR_GREY ),
idPlayer( -1 )
{
	if( l == LINE_GAME_ORIENTATION_VERTICAL ){
		size.set( LINE_VERTICAL_WIDTH ,LINE_VERTICAL_HEIGHT ,1.0 );
	}else if( l == LINE_GAME_ORIENTATION_HORIZONTAL ) {
		size.set( LINE_HORIZONTAL_WIDTH , LINE_HORIZONTAL_HEIGHT, 1.0 );
	}
	
	setPlayerImage( /*Bg*/ );
#if DEBUG
	setName("Linha \n");
#endif
}

void LineGame::loadImages( void ){
		linhaBgH = new Sprite( "lh","lh" );
		linhaPlH = new Sprite( "ch","ch" );	
		linhaBgV = new Sprite( "lv","lv" );
		linhaPlV = new Sprite("cv","cv");
//		cloudH = new Sprite("fch","fch",ResourceManager::Get16BitTexture2D);
//		cloudV = new Sprite( "fcv","fcv",ResourceManager::Get16BitTexture2D );

#if DEBUG 
//	cloudH->setName("Nuvem Horizontal estática");
//	cloudV->setName("Nuvem Vertical estática");
	linhaBgH->setName("linha hor fundo estatica\n");
	linhaPlH->setName("linha hor jogador estatica\n");
	linhaBgV->setName("linha ver fundo estatica\n");
	linhaPlV->setName("linha ver jogador estatica\n");
	NSLog(@"imagens estáticas carregados\n");	
#endif
}


void LineGame::removeImages( void ){

	SAFE_DELETE( linhaBgH );
	SAFE_DELETE( linhaBgV );
	SAFE_DELETE( linhaPlV  );
	SAFE_DELETE( linhaPlH );
	//SAFE_DELETE( cloudH );
	//SAFE_DELETE( cloudV );
#if DEBUG
	NSLog(@"imagem estáticas descarregados \n");	
#endif
}

/*================================================================================
 
 puxa para si mesmo as imagens que estiverem armazenadas no array de jogadores  e no final aloca a imagem em branco...
 
 ================================================================================*/

void LineGame::setPlayerImage( /*idBackground Bg*/ ){
	
	Sprite* currSprite;

	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		currSprite = new Sprite( LineGame::linhaBgV );
		currSprite->setSize( LINE_VERTICAL_WIDTH ,LINE_VERTICAL_HEIGHT, 1.0f );
	}else 
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ) {
			currSprite = new Sprite( LineGame::linhaBgH );
			currSprite->setSize( LINE_HORIZONTAL_HEIGHT , LINE_HORIZONTAL_WIDTH, 1.0f );
		}
#if DEBUG
	currSprite->setName("linha de fundo\n");
#endif
	currSprite->setAnimSequence( BoardFactory::GetInstance()->setFrameByCharacter() );
	if( !insertObject( currSprite ) )
#if DEBUG
		NSLog(@" sprite fundo não inserido ");
#endif
//	else
//#if DEBUG
//		NSLog(@" sprite fundo inserido ");
//#endif
	

}


/*===================================================================
 
 determina o estado da linha e faz possíveis correções
 
 ====================================================================*/

void LineGame::setLineGameState( LineGameState l ){
	lastState = state;
	state = l;
	switch ( l ) {
		case LINE_GAME_STATE_NONE:
		case LINE_GAME_STATE_SELECTED:
			break;
		case LINE_GAME_STATE_OCCUPIED:
			if( lastState == LINE_GAME_STATE_ANIMATING )
				removeObject( 3 );
			occupied = true;
			break;
		case LINE_GAME_STATE_BLANK:
			selected = false;
			occupied = false;
			break;
		case LINE_GAME_STATE_OBSERVING:
			observed = true;
			break;
			
	}
	updateImage();
}

/*============================================================================
 
 checkCollision-> retorna true, se houve alguma colisão dentro dela

==============================================================================*/

bool LineGame::checkCollision( const Point3f* pPoint,Player *p ) {
	
	if( Utils::IsPointInsideRect( pPoint, this ) &&
	   getLineGameState() != LINE_GAME_STATE_NONE &&
	   isVisible() && !locked ){
#if DEBUG
		NSLog(@"colidiu na linha x = %.2f, y = %.2f",position.x,position.y);
#endif
		/*if ( state == LINE_GAME_STATE_SELECTED && selected ) {
			gainedByPlayer( p );	
			setLineGameState( LINE_GAME_STATE_OCCUPIED );

			return true;
		}*/
		
		if ( state == LINE_GAME_STATE_BLANK) {
			setLineGameState( LINE_GAME_STATE_SELECTED );//LINE_GAME_STATE_OCCUPIED );
			//gainedByPlayer( p );
			return true;
		}

	}
	return false;
}


/*========================================================================
 
 updateImage-> troca a figura da linha de acordo com o estado dela
 
 ==========================================================================*/

void LineGame::updateImage( void ){

	switch ( state ) {
		case LINE_GAME_STATE_BLANK:
			static_cast< Sprite* > (getObject( INDEX_LINE_BACKGROUND ))->setVertexSetColor( COLOR_WHITE );
			//getObject( INDEX_LINE_PLAYERS )->setVisible( false );
			break;

		case LINE_GAME_STATE_SELECTED:
			static_cast< Sprite* > ( getObject( INDEX_LINE_BACKGROUND ) )->setVertexSetColor( COLOR_GREY );
			selected = true;
			break;

		case LINE_GAME_STATE_OCCUPIED:
			static_cast< Sprite* > (getObject( INDEX_LINE_BACKGROUND ))->setVertexSetColor( COLOR_WHITE );
			getObject( INDEX_LINE_PLAYERS )->setVisible( true );
//			static_cast< Sprite* > ( getObject( INDEX_LINE_PLAYERS ) )->setVisible( true );
			break;
		case LINE_GAME_STATE_OBSERVING:
			break;

	}
	

}

/*======================================================================================
 
 posiciona a linha de acordo com a sua orientação
 
=========================================================================================*/
void LineGame::setPosition( uint x, uint y ){
	Point3f p;//,m;
//	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
//		
//		p.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
//			  LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
//			  1.0);
//
//		m.set( 0.0f,
//			  0.0f);
//	}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
//		
//		p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
//			  y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
//			  1.0);
//		m.set( ( FENCE_HORIZONTAL_WIDTH * 0.1f )	,
//			  0.0);
//	}
 p = LineGroup::getLineCoordenatesByXY( x, y ,  orientation == LINE_GAME_ORIENTATION_VERTICAL );
	position.set( p );	
	
	
	//getObject( INDEX_LINE_BACKGROUND )->setPosition( &m );
}


/*=============================================================================================
 
 restart-> para ser chamado depois de qq turno
 
 =====================================================================================================*/

void  LineGame::restart( void ){
	if( !occupied ){
		setLineGameState( LINE_GAME_STATE_BLANK );
		selected = false;
	}

}

/*=============================================================
 
 reset->deixa a linha no seu estado original
 
 =================================================================*/

void LineGame::reset( void ){
	setLineGameState( LINE_GAME_STATE_BLANK );
}

/*===========================================================================

 puxa a imagem da linha de acordo com o fundo
 
===========================================================================*/
void LineGame::getBackgroundImage( Background *bg ){

	
	/*if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		currSprite = bg->getBaseLineV();
	}else 
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ) {
			currSprite = bg->getBaseLineH();
		}*/
}

/*=================================================================================
 
 acerta a imagem da linha de acordo com o personagem do jogador
 
================================================================================*/
void LineGame::gainedByPlayer( Player *p ){
	//por enquanto, somente mudar as cores...!
	//	correto!	
	idPlayer = p->getId();
	
//	setLineGameState( LINE_GAME_STATE_ANIMATING );
	
	/*
	 depois mudar para a fç retornar um tipo objeto que vai ser inserido no grupo de objetos, deste jeito, independe de ser uma linha ou um quadro quaisquer
	 */
/*	Sprite *s2;
//	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
//		s2 = new Sprite( p->boneco->getLineVPic());
//	}else {
//		s2 = new Sprite( p->boneco->getLineHPic() );
//	}
//	Point3f f;
//	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
//		
//		f.set( 0.0,
//			  -( FENCE_VERTICAL_HEIGHT * 0.1 ),				  
//			  1.0f);
//	}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
//		
//		f.set( 0.0,
//			  - (FENCE_HORIZONTAL_HEIGHT * 0.75f),
//			  0.0f);
//	}
//	s2->setPosition( &f );
//  insertObject( s2 );
 lColor= *( p->boneco->getColor() );	
*/

//temporário	
	if( p->getId() == 0 )
		lColor.set( COLOR_BLUE );
	else 
		lColor.set( COLOR_GREEN );
//	Sprite *s1;

	Sprite* s;
	if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ){

	s = new Sprite( LineGame::linhaPlH );
	}
	else{
		
		s = new Sprite( LineGame::linhaPlV );
	}
#if DEBUG
	s->setName("linha jogador");
#endif
	
	Point3f f;
		if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		
			f.set( 0.0,
				  -( FENCE_VERTICAL_HEIGHT * 0.1 ),				  
				  1.0f);
		}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
		
			f.set( 0.0,
				  - (FENCE_HORIZONTAL_HEIGHT * 0.75f),
				  0.0f);
		}
	//s->setPosition( &f );
	s->setAnimSequence(CharacterFactory::GetInstance()->setFrameByCharacter( p->boneco->getIdDoll() ) );
	insertObject( s );
//	s1->setPosition( &f );
//	s->setAnimSequence( 0 );
//	insertObject( s1 );
	occupied = true;

}
	
void LineGame::lineGameCpuGained( Player *p ){
	if( occupied )
		return;
	gainedByPlayer( p );
	setLineGameState( LINE_GAME_STATE_OCCUPIED );
}

//destrutor
LineGame::~LineGame(){
}


bool LineGame::render( void ){
	if( !isVisible() )
		return false;
	if( state == LINE_GAME_STATE_ANIMATING ){
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ){
			
		}else {
			
		}
		
	}
	else if ( /*state == LINE_GAME_STATE_OBSERVING*/ observed ) {
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		glTranslatef( position.x, position.y, position.z );
		glColor4ub( lColor.getUbR(), lColor.getUbG(), lColor.getUbB(), lColor.getUbA());
		
		Quad q1;
		
		q1.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 10.0, 10.0f, 1.0f );
		q1.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( size.x-10.0, 10.0, 1.0f );
		q1.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( size.x-10.0, size.y-10.0, 1.0f);	
		q1.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 10.0f, size.y - 10.0, 1.0f );	
		
		q1.render();
		glPopMatrix();
		return true;
	}else
		return ObjectGroup::render();
}


/*============-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0=-=-=-==-=-==-==-=-======-=--===-=----=-=-=-=====
 
ajustLinesToBackground->acerta a figura da linha de acordo com o fundo
 
 ============-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0=-=-=-==-=-==-==-=-======-=--===-=----=-=-=-=====*/
void LineGame::adjustLinesToBackground( int8 index ){

	static_cast<Sprite*> (INDEX_LINE_BACKGROUND)->setAnimSequence( index );
}

/*===============================================================================================
 
 lostByPlayer=> fç chamada qdo o jogador perde a linha, 
 
 ==============================================================================================*/

void LineGame::lostByPlayer( /*Player *p*/ ){
	
	if( !occupied )
		return;
#if DEBUG
	NSLog(@"linha ( %3.3f , %3.3f ) perdida pelo jogador %d", position.x,position.y,idPlayer);
#endif
	
	lColor.set( COLOR_GREY );
	idPlayer = -1 ;
	removeObject( INDEX_LINE_PLAYERS );
	reset();
}


void LineGame::setLocked( bool b ){
	locked = b;
}

void LineGame::adjustLinesToPlayer( int8 index ){
	static_cast<Sprite*> ( getObject(INDEX_LINE_PLAYERS) )->setAnimSequence( index );
}



void LineGame::lockLine( void ){
	setLocked( true );
	lColor.set( COLOR_BLACK );
	//static_cast<Sprite*> ( getObject(INDEX_LINE_PLAYERS) )->setAnimSequence( BoardFactory::GetInstance()->getLockFrame() );
}	

//void LineGame::setSpriteListener( SpriteListener* s ){
//	cloudH->setListener( s );
//	cloudV->setListener( s );
//}


//void LineGame::onAnimEnded( Sprite* pSprite ){
//	setLineGameState( LINE_GAME_STATE_OCCUPIED );
//}
