/*
 *  Board.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Board.h"
#include "Marks.h"
#include "DotGame.h"
#include "ObjcMacros.h"
#include "LineGroup.h"
#include "ZoneGroup.h"
//
//	 Sprite* Background::baseLineH;
//	 Sprite* Background::baseLineV;
//

#define OBJECTS_INSIDE_BACKGROUND 2//4


Board::Board( idBackground _id , GameBaseInfo *g ):ObjectGroup( OBJECTS_INSIDE_BACKGROUND ){

	createMarks( g );
	//creatingBoard( g );
}

void Board::createMarks(  GameBaseInfo *g ){ 

Marks *pMarks = new Marks( g );
	//pMarks->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0);
	if( !insertObject( pMarks ) ) {
#if DEBUG
		LOG("erro criando os marcos");
#endif
	}
}

/*============================================================
 
 creatingBoard=>cria as linhas e as zonas

 ============================================================*/

//void Board::creatingBoard( GameBaseInfo *g ){ 
//	ZoneGroup* zoneManager = new ZoneGroup( *g );
//	if( !insertObject( zoneManager ) )
//		return ;//false;
//	
//	LineGroup* lineManager = new LineGroup( *g );
//	if(	!insertObject( lineManager ) )
//		return ;//false;
//	
//	uint8 x,y;
//	ZoneGame* z;
//	for ( x = 0; x < g->nHZones; x++) {
//		for ( y = 0; y< g->nVZones/* nVerticalZones*/ ; y++ ){
//			z = new ZoneGame();
//			z->setPosition( x, y );
//			zoneManager->insertObject( z );
//		}
//	}
//	LineGame* l;
//	for ( x = 0; x <  g->nHZones/*nHorizontalZones*/ ; x++ ) {
//		for ( y = 0; y < g->nVZones + 1/*nVerticalLines  */; y++) {
//			l = new LineGame( LINE_GAME_ORIENTATION_HORIZONTAL );
//			l->setPosition( x, y);
//			lineManager->insertObject( l );
//		}
//		
//	}
//	lineManager->setVisible(false);
//	zoneManager->setVisible(false);
//	for ( x = 0; x <  g->nHZones + 1/*nHorizontalLines*/  ; x++ ) {
//		for ( y = 0; y < g->nVZones/*nVerticalZones*/ ; y++) {
//			l = new LineGame( LINE_GAME_ORIENTATION_VERTICAL );
//			l->setPosition( x, y);
//			lineManager->insertObject( l );
//		}
//	}
//}
