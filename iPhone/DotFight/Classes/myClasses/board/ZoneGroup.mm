/*
 *  ZoneGroup.mm
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ZoneGroup.h"
#include "ZoneGame.h"
#include "DotGame.h"
#include "GameBaseInfo.h"
#include "Utils.h"
using namespace Utils;
ZoneGroup::ZoneGroup( GameBaseInfo& g ):ObjectGroup( g.nHZones * ( g.nVZones ) ), nZonesH( g.nHZones ), nZonesV( g.nVZones ) {}

/*=======================================================================================================
 
 insere zones=> insere as zonas em um grupo
 
======================================================================================================= */
void ZoneGroup::insertZones( ZoneGame* z[ MAX_ZONES_COLUMNS ][ MAX_ZONES_ROWS ] ){
	
	for (uint8 i=0; i< nZonesH ; i++) {
		for (uint8 j=0; j< nZonesV ; j++) {
			if(!insertObject( z[ i ][ j ] )){
#if DEBUG
				NSLog(@"zona não inserida");
#endif
				return;
			}
		}
	}
#if DEBUG
	NSLog(@"zonas inseridas");
#endif
}

/*=======================================================================================================

 setQteZones( uint8 h, uint8 v )-> seta a qte de zonas possíveis no jogo

 ======================================================================================================= */
void ZoneGroup::setQteZones( uint8 h, uint8 v ){

	nZonesH = h;
	nZonesV = v;
	
}

/*=======================================================================================================
 
 reset=> nome auto explicativo, reseta todas as zonas em jogo para o esta inicial
 
 =======================================================================================================*/
void ZoneGroup::reset( void ){
	for (uint8 c = 0; c < getNObjects(); c++) {
		static_cast< ZoneGame* > ( getObject( c ) )->reset();
	}
}


/*=======================================================================================================
 
haveFreeZone = verifica se há zonas livres em campo
 
=======================================================================================================*/
bool ZoneGroup::haveFreeZone( void ){
	for (uint16 c = 0; c < getNObjects(); c++) {
		if ( static_cast< ZoneGame* > ( getObject( c ) )->getZoneGameState() != ZONE_GAME_STATE_OCCUPIED ) {
			return true;
		}
	}
		return false;
}


//sim, tb presume que seja uma zona/quadrado válido!!
bool ZoneGroup::isZoneOccupied( Point3f *p ){

	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) )
			return static_cast< ZoneGame* > ( getObject( c ) )->isOccupied();
	
	return false;
}


bool ZoneGroup::zoneGainedByPlayer( Point3f *p, Player *pl ){
	 for (uint16 c = 0; c < getNObjects(); c++) {
		 if( IsPointInsideRect( p, getObject( c ) ) ){
			return static_cast<ZoneGame*>( getObject( c ) )->gainedByPlayer( pl );
			 //return true;
		 }

 }
#if DEBUG
	 NSLog(@"nenhuma zona selecionada");
#endif
return false;
}

/* =============================================================================================
 
 //sim, tb presume que seja uma zona/quadrado válido!!

 
 =============================================================================================*/

void ZoneGroup::zoneLostedByPlayer( Point3f *p ){
	for (uint16 c = 0; c < getNObjects(); c++) {
		if( IsPointInsideRect( p, getObject( c ) ) ){

			 static_cast<ZoneGame*>( getObject( c ) )->lostByPlayer();
			return;
		}
	}
}

/* =============================================================================================
 
 //sim, tb presume que seja uma zona/quadrado válido, se não, retorna false!
 
 =============================================================================================*/


bool ZoneGroup::isZoneCoupled( Point3f *p ){
	for (uint16 c = 0; c < getNObjects(); c++) 
		if( /**p == *getObject( c )->getPosition()*/IsPointInsideRect( p, getObject( c ) ) ){
			return	static_cast<ZoneGame*>( getObject( c ) )->isCoupled();
		}

#if DEBUG
	NSLog(@"zona nenhuma nestas coordenadas");
#endif
	return false;
}


/* =============================================================================================
 
 retorna o id do jogador de determinada zona
 
 =============================================================================================*/

uint8 ZoneGroup::zoneGetPlayerId( Point3f *p ){
	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) ){
#if DEBUG
			NSLog(@"zona( %3.4f,%3.4f )->playerId = %d\n",getObject( c )->getPosition()->x,getObject( c)->getPosition()->y,static_cast<ZoneGame*>( getObject( c ) )->getPlayerId());
#endif		
		return	static_cast<ZoneGame*>( getObject( c ) )->getPlayerId();

		}
	return -1;
#if DEBUG
	NSLog(@"zona nenhuma nestas coordenadas");
#endif

}

/* =============================================================================================
 
 acerta se determinada zona é casada ou não
 
 =============================================================================================*/

void ZoneGroup::setZoneCoupled( Point3f *p, bool b ){

	for (uint16 c = 0; c < getNObjects(); c++) 
		if( /**p == *getObject( c )->getPosition()*/IsPointInsideRect( p, getObject( c ) ) ){
			static_cast<ZoneGame*>( getObject( c ) )->setCoupled( b );
			return;
		}
#if DEBUG
	NSLog(@"zona nenhuma nestas coordenadas");
#endif
}

/*=============================================================================================
 
 determina se certa zona é visível ou não
 
 =============================================================================================*/
void ZoneGroup::setZoneVisible( Point3f *p, bool b ){
	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) ){
			getObject( c )->setVisible( b );
			return;
		}
}

/*============================================================================================
 
 cpnfigura qdo certas zonas é transformada em bigZona
 ( provavelmente vai deixar as outras invisíveis e tronar uma gde imagem por cima! )
 ============================================================================================*/

void ZoneGroup::BigZoneGainedByPLayer( Point3f *p, Player *pl ){

	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) ){
			static_cast<ZoneGame*>( getObject( c ) )->isMainBigZone( pl );		
			return;
		}
}

void ZoneGroup::BigZoneLostedByPLayer( Point3f *p ){

	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) ){
			static_cast<ZoneGame*>( getObject( c ) )->notIsMainBigZone();		
			return;
		}
}

/*=================================================================================
 retorna a posição real de uma zona/quadrado à partir de coordenadas x e y
==================================================================================*/
Point3f ZoneGroup::getZonePositionByXY( uint8 x, uint8 y ){
	Point3f p;
	p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
		 LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
		  1.0f);
	return p;

}


Point3f* ZoneGroup::getZonePPositionByXY( uint8 x, uint8 y ){
	
	Point3f *p,p1;
	p1.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
		  LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
		  1.0f);
	p = &p1;
	return p;

}

void ZoneGroup::getZoneXYByPosition( Point3f*p , uint8* x,uint8* y ){
	float i,j;
	i =  ( p->x - LINE_HORIZONTAL_HEIGHT ) / ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE );						
	j = ( p->y - LINE_VERTICAL_WIDTH ) / ( ZONE_SIDE + LINE_VERTICAL_WIDTH );
	
	i = floor( i );
	j = floor( j );	
#if DEBUG
uint8 m,n;
	
		m = ( p->x - LINE_HORIZONTAL_HEIGHT ) / ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE );	
		n = ( p->y - LINE_VERTICAL_WIDTH ) / ( ZONE_SIDE + LINE_VERTICAL_WIDTH );	
#endif
	
	if( x )
		*x = i;
	if( y )
		*y = j;
}

bool ZoneGroup::zoneHavePowerUp( Point3f *p ){
	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) ){
			return	static_cast<ZoneGame*>( getObject( c ) )->havePowerUp();
		}

}