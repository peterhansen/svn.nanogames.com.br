/*
 *  LineGroup.h
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef LINE_GROUP_H
#define LINE_GROUP_H 1

/*
 
 classe que gerenciaria as linhas, dizem se ela está ocupada, e se estiver,  por quem, etc.
 
talvez tenha um método para dizer qual linha foi pressionada
 
 */
#include "ObjectGroup.h"
#include "DotGameInfo.h"
#include "BoardFactory.h"

class LineGame;
class DotGame;
class GameBaseInfo;
class Point2i;
class Player;

class LineGroup : public ObjectGroup {
	
public:
	//construtor
	LineGroup( GameBaseInfo g );
	
	//destrutor
	~LineGroup( void ){
#if DEBUG
		NSLog(@"destruindo line group\n");
#endif
	};	

	
	//qdo uma linha é perdida pelo jogador
	void lineLostByPlayer( Point3f *p );

	//insere linhas horizontais
	void insertLinesH( 	LineGame* l[ MAX_ZONES_COLUMNS ][ MAX_LINES_ROWS ] );
	
	//insere linhas verticais
	void insertLinesV( LineGame* l[ MAX_LINES_COLUMNS ][ MAX_ZONES_ROWS ] );
	
	//reseta as linhas 
	void resetLines( void );

	//deseleciona as linhas
	void deselectLines( void );

	void setTotalLines( uint8 Lh, uint8 Lv , uint8 Zh, uint8 Zv );
	
	void setAllLinesObserving( void );
	
	void setAllLinesLastState( void );
	
	void resetLine( Point3f *p );
	
	bool haveFreeLine();

	void selectLine( Point3f *p , Player* pl );
	
	bool isLineGained( Point3f *p );
	
	bool isLineHorizontal( Point3f *p );
	
	void setLineVisible( Point3f *p, bool b );
	
//retorna a posição orignal da linha pressionada, desse jeito, posso simplesmente comparar a posição, ao invés de ter que ver se o ponto está dentro da linha
	const Point3f* lineBeginPosition( const Point3f *p );
	
	//retorna a posição de uma linha horizontal ou vertical a partir de 2 coordenadas
static	Point3f getLineCoordenatesByXY( uint8 x, uint8 y, bool vertical );
static	Point3f* getLinePCoordenatesByXY( uint8 x, uint8 y, bool vertical );
static	void getLineXYByPosition( Point3f *p, bool vertical, uint8* x = NULL , uint8* y = NULL  );
	
	//inútil??
	void  restartAllLines( void );
	
	//coloca a figura de acordo com o cenário
	void adjustAllLines( int8 i );

	//coloca a figura de acordo com o jogador
	void adjustLineToPlayers( Point3f *p, int8 i );
	
	//cria todas as linhas já com a posição
	void makeAllLines( void );
	
	void lockLine( Point3f *p );
	
	inline	uint8 getTotalLinesH( void ){ return totalLinesH; };

	inline	uint8 getTotalLinesV( void ){ return totalLinesV; };
	
	inline	uint8 getTotalZonesH( void ){ return totalZonesH; };
	
	inline	uint8 getTotalZonesV( void ){ return totalZonesV; };
	
	uint8 getLinePlayerId( Point3f *p );
	
private:
	
	uint8 totalLinesH, totalLinesV,  totalZonesH, totalZonesV;
	
};


#endif