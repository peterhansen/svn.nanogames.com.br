/*
 *  LineGroup.mm
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "LineGroup.h"
#include "LineGame.h"
#include "Player.h"
//#include "DotGame.h"
#include"Utils.h"
#include "GameBaseInfo.h"
LineGroup::LineGroup( GameBaseInfo g ):ObjectGroup( ( g.nHZones * ( g.nVZones + 1 ) ) * ( g.nVZones * ( g.nHZones + 1 ) ) ),
totalZonesH ( g.nHZones ),
totalZonesV ( g.nVZones ),
totalLinesH ( totalZonesH + 1 ),
totalLinesV ( totalZonesV + 1 ){
#if DEBUG
	setName("Grupo Linha");
#endif

}


/*=====================================================
 
 Método insertLinesH-> insere todas as linhas horizontais no grupo
 
 =======================================================*/

void LineGroup::insertLinesH( LineGame* l[ MAX_ZONES_COLUMNS ][ MAX_LINES_ROWS ] ){
	for (uint8 i=0; i< totalZonesH ; i++) {
		for (uint8 j=0; j< totalLinesV ; j++) {
			
			if( !insertObject( l[ i ][ j ] )){
#if DEBUG
				NSLog(@" objeto não incluido ");
#endif
			}
			
		}
	}
#if DEBUG
	NSLog(@" todas as linhas horizontais (%d) incluídas ",totalZonesH * totalLinesV );
#endif
}



/*=====================================================
 
 Método insertLinesV-> insere todas as linhas verticais no grupo
 
 =======================================================*/

void LineGroup::insertLinesV( LineGame* l[ MAX_LINES_COLUMNS ][ MAX_ZONES_ROWS ] ){
	for (uint8 i=0; i< totalLinesH  ; i++) {
		for (uint8 j=0; j< totalZonesV ; j++) {
			
			if( !insertObject( l[ i ][ j ] )){
#if DEBUG
				NSLog(@" objeto não incluido ");
#endif
			}
			
		}
	}
#if DEBUG
	NSLog(@" todas as linhas verticais (%d) incluídas ",totalZonesV * totalLinesH );
#endif
}


/*=====================================================
 
  Método resetLines-> reseta todas as linhas ao estado original

 =======================================================*/

void LineGroup::resetLines( void ){
	for (uint8 c=0; c< getNObjects(); c++ ) {
		static_cast <LineGame*> ( getObject( c ) )->reset();

	}
	
}

/*================================================================================================
 
 linePressed-> retorna a linha que foi pressionada
 
 CAGADO-> retirar esta merda
 
================================================================================================ */
/*
LineGame* LineGroup::LinePressed( const Point3f *p ){
/*	
	for ( uint i = 0 ; i < getNObjects(); i++) {
		if( static_cast<LineGame*> (getObject( i ) )->checkCollision( p ) ){
			return static_cast<LineGame*> (getObject( i ) );
		}
	}

}*/

/*==================================================================================================

deselectLines->deseleciona as linhas

================================================================================================== */

void LineGroup::deselectLines( void ){

	for (uint8 c = 0 ; c < getNObjects(); c++ ) {
		static_cast <LineGame*> ( getObject( c ) )->setLineGameState( LINE_GAME_STATE_BLANK );
		
	}
}


/*====================================================================================================
 
restartAllLines-> prepara todas as linhas para o próximo turno

====================================================================================================*/
void LineGroup::restartAllLines( void ){

	for (uint8 c = 0; c < getNObjects(); c++ ) {
		static_cast <LineGame*> ( getObject( c ) )->restart();
		
	}
}



/*==========================================================================================================

getTotalLines-> puxa a qte de linhas totais da partida
 
==========================================================================================================*/

void LineGroup::setTotalLines(  uint8 Lh, uint8 Lv , uint8 Zh, uint8 Zv  ){
	totalLinesH = Lh;
	totalLinesV = Lv;
	totalZonesH = Zh;
	totalZonesV = Zv;
}



void LineGroup::setAllLinesObserving( void ){
	for (uint8 c = 0; c < getNObjects(); c++ ) {
		static_cast <LineGame*> ( getObject( c ) )->setLineGameState( LINE_GAME_STATE_OBSERVING );
	}
}

void LineGroup::setAllLinesLastState( void ){
	for (uint8 c = 0; c < getNObjects(); c++ ) {
		static_cast <LineGame*> ( getObject( c ) )->lineToLastState();
	}
}

//coloca a figura de acordo com o cenário
void LineGroup::adjustAllLines( int8 i ){

	for (uint8 c = 0; c < getNObjects(); c++ ) {
		static_cast <LineGame*> ( getObject( c ) )->adjustLinesToBackground( i );
	}
	

}

//cria todas as linhas já com a posição
void LineGroup::makeAllLines( void ){
//	int32 i ;
	for ( int32 x = 0; x < getCapacity(); x++ ) {
		//LineGame* l = new LineGame();
	}

}


void LineGroup::resetLine( Point3f *p ){

	for( int32 i = 0; i < getNObjects() ;i++ ){
	
		if(  p == getObject( i )->getPosition()  ){
		
			static_cast< LineGame* > ( getObject( i ) )->reset() ;
			
		}
	}
	
}

//
void LineGroup::selectLine(  Point3f *p , Player* pl ){
#if DEBUG
	NSLog(@"linha que fora selecionada: x= %4.2f y=%4.2f",p->x,p->y);
#endif
	for( int32 i = 0; i < getNObjects() ;i++/*i+=2*/ ){
		
		if(  
		Utils::IsPointInsideRect( p ,getObject( i ) ) )
		{
#if DEBUG
			Point3f tmp;
			tmp = *getObject( i )->getPosition();
			NSLog(@"linha que fora marcada: x= %4.2f y=%4.2f",tmp.x,tmp.y);
#endif
			
				static_cast< LineGame* > ( getObject( i ) )->lineGameCpuGained( pl );
				return;
		}
	}
}

//sim, presume que a entrada é uma linha válida!!
bool LineGroup::isLineGained( Point3f *p ){
	
	for( int32 i = 0; i < getNObjects() ;i++ ){
		
		if( /*p == getObject( i )->getPosition()*/Utils::IsPointInsideRect( p ,getObject( i ) ) ){
#if DEBUG
			uint8 x,y;
			Point3f p;
			p = *getObject( i )->getPosition();
			getLineXYByPosition( &p , isLineHorizontal( &p ), &x, &y);
			NSLog(@"verificando linha: x= %d y=%d ",x,y);
#endif
			return static_cast< LineGame* > ( getObject( i ) )->isOccupied();
			
		}
	}
	
#if DEBUG
	NSLog(@"nenhuma linha encontrada!");
#endif
	
	return false;
}

//sim, presume que a entrada é uma linha válida!!
bool LineGroup::isLineHorizontal( Point3f *p ){
	
	for( int32 i = 0; i < getNObjects() ;i++ ){
		
		if(Utils::IsPointInsideRect( p ,getObject( i ) )  ){
			
			if( static_cast< LineGame* > ( getObject( i ) )->getLineGameOrientation() == LINE_GAME_ORIENTATION_HORIZONTAL ){
				return true;
			}else {
				return false;
			}
		}
	}
	return false;
}

//retorna a posição orignal da linha pressionada, desse jeito, posso simplesmente comparar a posição, ao invés de ter que ver se o ponto está dentro da linha
const Point3f* LineGroup::lineBeginPosition( const Point3f *p ){
	
	for( int32 i = 0; i < getNObjects() ;i++ ){
		
		if( Utils::IsPointInsideRect( p ,getObject( i ) ) ){
#if DEBUG
			Point3f tmp;
			tmp = *getObject(i)->getPosition();
			NSLog(@"posicao original x=%4.0f y=%4.0f", tmp.x,tmp.y );
#endif			
			return getObject( i )->getPosition() ;

		}
	}
	
	return NULL;
}


bool LineGroup::haveFreeLine(){
	for( int32 i = 0; i < getNObjects() ;i++ ){
		
	if( static_cast< LineGame* > ( getObject( i ) )->getLineGameState() == LINE_GAME_STATE_BLANK )
		return true;
	
	}
	
return false;	
}



void LineGroup::setLineVisible( Point3f *p, bool b){
	for( int32 i = 0; i < getNObjects() ;i++ )
		
		if( Utils::IsPointInsideRect( p ,getObject( i ) ) ){
		getObject( i )->setVisible( b );
#if DEBUG
			NSLog(@"a linha ( %3.3f, %3.3f ) está %c", getObject( i )->getPosition()->x ,getObject( i )->getPosition()->y, getObject( i )->isVisible() ? 'V':'I' );

#endif
			
			return;
		}
		
}

Point3f* LineGroup::getLinePCoordenatesByXY( uint8 x, uint8 y, bool vertical ){
	Point3f *p;
	Point3f aux;
	aux = getLineCoordenatesByXY( x, y, vertical );
	p = &aux;
	return p;
}

///transformar em estática?
Point3f LineGroup::getLineCoordenatesByXY( uint8 x, uint8 y, bool vertical ){
	Point3f p;
	if( vertical ){
		p.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
			  LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
			  1.0);
	}else {
		p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
			  y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
			  1.0);
		
	}
	return p;
}


//qdo uma linha é perdida pelo jogador
void LineGroup::lineLostByPlayer( Point3f *p ){
	
	for( int32 i = 0; i < getNObjects() ;i++ )
		if( Utils::IsPointInsideRect( p ,getObject( i ) ) ){		
			static_cast< LineGame* > ( getObject( i ) )->lostByPlayer();
			return;
		}
	
}


//coloca a figura de acordo com o jogador
void LineGroup::adjustLineToPlayers( Point3f *p, int8 x ){
	for( int32 i = 0; i < getNObjects() ;i++ )
		
		if( Utils::IsPointInsideRect( p ,getObject( i ) ) ){
			static_cast <LineGame*> ( getObject( i ) )->adjustLinesToPlayer( x );
			return;
		}
	
}



void LineGroup::getLineXYByPosition( Point3f *p, bool vertical, uint8* x , uint8* y ){
	float i,j;
	if( vertical ){

		i = floor( p->x / ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) );
		j = floor( ( p->y - LINE_VERTICAL_WIDTH ) / ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT ) );
	}else {
		i = floor( ( p->x - LINE_HORIZONTAL_HEIGHT ) / ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ));
		j = floor( p->y / (  LINE_HORIZONTAL_HEIGHT + ZONE_SIDE) ) ;
	}
	
	i = fabs( i );
	j = fabs( j );
#if DEBUG
uint8	 m,n;

	if( vertical ){
		
		m = ( p->x / ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) );
		n = ( ( p->y - LINE_VERTICAL_WIDTH ) / ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT ) );
	}else {
		m = ( ( p->x - LINE_HORIZONTAL_HEIGHT ) / ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ));
		n = ( p->y / (  LINE_HORIZONTAL_HEIGHT + ZONE_SIDE) ) ;
	}
	
#endif
	if( x )
		*x = i;
	if( y )
		*y = j;
}


void LineGroup::lockLine( Point3f *p ){
	for( int32 i = 0; i < getNObjects() ;i++ )
		
		if( Utils::IsPointInsideRect( p ,getObject( i ) ) ){
			static_cast <LineGame*> ( getObject( i ) )->lockLine();
#if DEBUG
			NSLog(@"linha travada( %4.4f, %4.4f )",getObject(i)->getPosition()->x ,getObject(i)->getPosition()->y );
#endif
			return;
		}
}

uint8 LineGroup::getLinePlayerId( Point3f *p ){

	for( int32 i = 0; i < getNObjects() ;i++ )
		
		if( Utils::IsPointInsideRect( p ,getObject( i ) ) ){
			return static_cast <LineGame*> ( getObject( i ) )->getPlayerId();

		}
	return 255;	
}
