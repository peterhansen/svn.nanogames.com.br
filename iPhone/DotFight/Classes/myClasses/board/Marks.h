//
//  Marks.h
//  dotGame
//
//  Created by Max on 1/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#ifndef MARKS_H
#define MARKS_H 1

#include "DotGameInfo.h"
#include"ObjectGroup.h"
#include "BoardFactory.h"

class GameBaseInfo;
class Sprite;

class Marks : public ObjectGroup {
	
public:
	
	Marks( GameBaseInfo *g );


	~Marks();
	void reconfigureMarks( /*idBackground idBg*/ int8 i );
	
	void disappearMark( const Point3f *p );
	
	void reappearMark( const Point3f *p );

	
private:
	
	bool  buildMarks( GameBaseInfo *g );
	
};


#endif