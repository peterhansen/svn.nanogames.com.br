/*
 *  ExecutiveBg.mm
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "ExecutiveBg.h"
#include "ObjcMacros.h"
#include "Marks.h"
#include "OptimizedTexPattern.h"


//#define INDEX_BOARD_BACKGROUND 3
//#define INDEX_BOARD_MARKS 0

ExecutiveBg::ExecutiveBg( GameBaseInfo* g ):Board( ID_BGD_EXECUTIVE, g ){

	generateBackground( g );
	configureMarks();
}

void ExecutiveBg::generateBackground(  GameBaseInfo* g  ){
	
	//trocar depois pela  imagem correta!
	char* nbg ="exeBg";
	Point3f p;
	p.set(  g->nHZones  *  ZONE_SIDE + ( ( g->nHZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
		  ( g->nVZones + 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE), 1.0);
	
	OptimizedTexPattern *t = new OptimizedTexPattern( nbg );
	
	p.x = ( p.x < SCREEN_WIDTH ? SCREEN_WIDTH : p.x /* * 0.60*/  ) ; 
	p.y = ( p.y < SCREEN_HEIGHT ? SCREEN_HEIGHT : p.y * 0.70  ) ; 	
	p *= 0.5f;
	
	//	
	//	t->setSize( SCREEN_WIDTH * 2.0f , SCREEN_HEIGHT * 3.0f ); //p.x, p.y  );//SCREEN_WIDTH * 2.0f , SCREEN_HEIGHT * 3.0f ); 
	//	t->setPosition( HALF_SCREEN_WIDTH - p.x , 0.0f/*HALF_SCREEN_WIDTH - p.y*/);
	
	t->setSize( SCREEN_WIDTH * 3.0f , SCREEN_HEIGHT * 3.0f ); 
	t->setPosition( HALF_SCREEN_WIDTH - p.x , HALF_SCREEN_WIDTH - p.y);
	if( !insertObject( t ) ) 
	{

#if DEBUG
		LOG("falha no fundo");
#endif	
		return ;
	}
	setObjectZOrder( INDEX_BOARD_BACKGROUND, 0);
}
void ExecutiveBg::configureLinesBkgd( void ){}
void ExecutiveBg::configureMarks( void ){
	static_cast<Marks*> ( getObject( INDEX_BOARD_MARKS ) )->reconfigureMarks( INDEX_MARK_EXECUTIVE ) ;
}