/*
 *  AngelBg.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef ANGEL_BG_H
#define ANGEL_BG_H 1

#include "Board.h"


class AngelBg : public Board {
	
public:
	AngelBg( GameBaseInfo* g );
	
	~AngelBg(){};
	
protected:	
	void generateBackground(  GameBaseInfo* g  ) ;
	
	void configureMarks( void ) ;
	
	void configureLinesBkgd( void );
	
};


#endif