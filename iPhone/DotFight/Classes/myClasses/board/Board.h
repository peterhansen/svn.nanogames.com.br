/*
 *  Board.h
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef BOARD_H
#define BOARD_H 1

#define INDEX_BOARD_BACKGROUND 1
#define INDEX_BOARD_MARKS 0


#include"Color.h"
#include "DotGameInfo.h"
#include "ObjectGroup.h"
#include "BoardFactory.h"
class Sprite;
class GameBaseInfo;



class Board : public ObjectGroup {

public:
	Board( idBackground _id, GameBaseInfo *g );
	
	~Board( void ){};
	
	idBackground getIdBackground( void );
	
	Color getCorBgd( void ); 
	
	void setMarkPosition( Point3f *p ){ getObject( INDEX_BOARD_MARKS )->setPosition( p ); };
	
protected:
	
	void createMarks( GameBaseInfo *g );

	virtual	void generateBackground(  GameBaseInfo* g  ) = 0;
	
	virtual	void configureMarks( void ) = 0;
	
	virtual void configureLinesBkgd( void ) = 0;
	
private:

	//void creatingBoard( GameBaseInfo *g );
	
	idBackground idBkgd;
	
	Color corBgd;
	
};
inline Color Board::getCorBgd( void ){
	return corBgd;
}
inline idBackground Board::getIdBackground( void ){
	return idBkgd;
}
#endif