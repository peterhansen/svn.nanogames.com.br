/*
 *  Background.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Background.h"
#include "DotGame.h"
#include "TexturePattern.h"
#include "Marks.h"
#include "ObjcMacros.h"

#define OBJECTS_INSIDE_BACKGROUND 2

	Sprite* Background::baseLineH;
	Sprite* Background::baseLineV;

Background::Background( idBackground _id,  DotGame *dg ):ObjectGroup( OBJECTS_INSIDE_BACKGROUND ){
 if ( !buildBackground( dg ) )
#if DEBUG
	 LOG("falha no build cenario");
#else
	return;
#endif
}

#define MANAGERS_POSITION_Y 63.0f
bool Background::buildBackground( DotGame *dg  ){
	GameBaseInfo d;
	//d = dg->GInfo;
	//descomentar para colocar de acordo com o cenário, tanto o fundo quanto os pontos!!!
	char* nbg;
	switch ( d.idBg ) {
		case ID_BGD_DEVIL :
			nbg="bg\0";
			break;
		case ID_BGD_EXECUTIVE :
			nbg="bg\0";
			break;
		case ID_BGD_FAIRY:
			nbg="bg\0";
			break;
		case ID_BGD_OGRE :
			nbg="bg\0";
			break;
		case ID_BGD_FARMER :
			nbg="bg\0"		;
			break;
		case ID_BGD_ANGEL:
			nbg="bg2\0"	;	
			break;
	}
	TextureFrame f( 0.0f, 0.0f, 512.0f, 512.0f, 0.0f, 0.0f );
	TexturePattern *t = new TexturePattern( nbg , &f  );
	t->setSize( SCREEN_WIDTH * 2.0f , SCREEN_HEIGHT * 2.0f );
	t->setPosition(( - t->getWidth()  ) * 0.5f,
				   ( - t->getHeight() ) * 0.25f);
	if( !insertObject( t ) ) 
	{
		return false;
#if DEBUG
		LOG("falha no fundo");
#endif
	}

	Point3f p;
	p.set(  d.nHZones *  ZONE_SIDE + ( ( d.nHZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
		  ( d.nVZones + 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
		  1.0);
	p *= 0.5f;
	
	Marks *pMarks = new Marks( &d );
	pMarks->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0);
	if( !insertObject( pMarks ) ) 
		return false;
	return true; 
}
#undef MANAGERS_POSITION_Y

	
void Background::removeAllImages( void ){}
	
	
bool Background::loadAllImages( void ){}


