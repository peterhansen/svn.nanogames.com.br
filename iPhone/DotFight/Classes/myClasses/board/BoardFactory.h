/*
 *  BoardFactory.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef BOARD_FACTORY_H
#define BOARD_FACTORY_H 1

//#include "DotGameInfo.h"
//#include "GameBaseInfo.h"

#define N_BOARDS 6

#define INDEX_LINE_ANGEL 0
#define INDEX_LINE_DEVIL 1
#define INDEX_LINE_EXECUTIVE 2
#define INDEX_LINE_FARMER 3
#define INDEX_LINE_FAIRY 4
#define INDEX_LINE_OGRE 5
#define INDEX_LINE_NEUTRAL 6

#define INDEX_LINE_BGD_OGRE 0
#define INDEX_LINE_BGD_ANGEL 1
#define INDEX_LINE_BGD_EXECUTIVE 2
#define INDEX_LINE_BGD_FAIRY 3
#define INDEX_LINE_BGD_FARMER 4
#define INDEX_LINE_BGD_DEVIL 5


#define INDEX_MARK_OGRE 0
#define INDEX_MARK_ANGEL 1
#define INDEX_MARK_EXECUTIVE 2
#define INDEX_MARK_FAIRY 3
#define INDEX_MARK_FARMER 4
#define INDEX_MARK_DEVIL 5



#include "CharacterFactory.h"

enum idBackground {
	ID_BGD_NONE = -1,
	ID_BGD_ANGEL = 0,
	ID_BGD_DEVIL,
	ID_BGD_FARMER,
	ID_BGD_EXECUTIVE,
	ID_BGD_FAIRY,
	ID_BGD_OGRE	
}typedef idBackground;

struct GameBaseInfo;
class Board;

class BoardFactory{
public:
	
	static bool Create( void );
	
	static void Destroy( void );

	static BoardFactory* GetInstance( void ) ;

	void setGameBaseInfo( GameBaseInfo* g2 ){ gInfo = g2; };
	
	Board* getNewBoard( /*idBackground _id*/ );
	
	int getNBoards( void );
	
	uint8 setFrameByCharacter( void );
	
	uint8 getLockFrame( void ){ return INDEX_LINE_NEUTRAL; };
	
	UIImage* getBackgroundByIndex( uint8 index );
	
private:

	BoardFactory( GameBaseInfo* g = NULL );
	~BoardFactory();

	static BoardFactory* pSingleton;
	
	GameBaseInfo* gInfo;
	int nBoards;
	
};


inline 	int BoardFactory::getNBoards( void ){
	return nBoards;
}

#endif