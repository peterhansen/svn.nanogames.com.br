/*
 *  Config.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H 1

#include "Ball_Defines.h"
#include "Tests.h"

/*=====================================
 
CONFIGURAÇÕES DOS COMPONENTES
 
======================================*/

// Define o número máximo de toques tratados pela aplicação
#define MAX_TOUCHES 2

// OBS: Ainda não é utilizado
// Duração padrão da vibração, em segundos
#define VIBRATION_TIME_DEFAULT 0.2f

// Define se utilizaremos um depth buffer na aplicação
#define USE_DEPTH_BUFFER 1

// Define se iremos utilizar um buffer para fazer renderizações fora da tela
#define USE_OFFSCREEN_BUFFER 0

// Intervalo padrão do timer responsçvel pelas atualizações da aplicação
#define DEFAULT_ANIMATION_INTERVAL ( 1.0f / 32.0f ) // 32 fps

// Tempo máximo (em segundos) que consideramos como transcorrido entre uma atualização e outra 
#define MAX_UPDATE_INTERVAL 0.125f

/*=====================================
 
CÂMERA DO JOGO
 
 ====================================*/

// Distância em metros à frente do jogador que a câmera aponta
#define CAMERA_SET_CONTROL_LOOK_AHEAD 5.0f

// Posição z mínima para a qual a câmera aponta
#define CAMERA_MIN_Z ( -5.0f )

// Velocidade com que a câmera se move quando controlada pelo jogador
#define CAMERA_MOVE_SPEED 5.0f
#define CAMERA_MOVE_PLAYER_TIME ( CAMERA_MOVE_DEFAULT_TIME / 3.0f )

// tempo padrão que a câmera leva para chegar à posição de destino
#define CAMERA_MOVE_DEFAULT_TIME 1.2f

// tempo padrão que a câmera leva para de fato chegar à posição do objeto sendo seguido
#define CAMERA_FOLLOW_DEFAULT_TIME 0.5f

// Área adjacente às bordas da tela na qual um toque faz com que a câmera movimente-se
// automaticamente (Ver GameScreen::moveCamIfBallNearBounds)
#define CAMERA_AUTO_MOVE_AREA 20.0f
#define CAMERA_AUTO_MOVE_MIN_SPEED  2.0f
#define CAMERA_AUTO_MOVE_MAX_SPEED 15.0f

// Tempo que a câmera demora para chegar até a bola na etapa de posicionamento do chute
#define CAMERA_FOCUS_ON_BALL_DELAY 0.5f

// Tempo que a câmera demora para ajustar o foco
#define CAMERA_ADJUST_TIME 1.5f

// Configuração do zoom
#if MAX_TOUCHES > 1

	#define ZOOM_IN_FACTOR 1.1f
	#define ZOOM_OUT_FACTOR ( 1.0f / ZOOM_IN_FACTOR )

	#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )
		#define ZOOM_MIN 0.0009f
		#define ZOOM_MAX 1.0f//1.8200f
	#else
		// Se for alterar este número, verificar o zoom out necessário para englobar a cena inteira nos chutes mais distantes
		// No caso de zoom max, idem, só que para os chutes mais próximos do gol
		// Daria para calcular esses números no construtor de GameScreen. Bastaria chamar adjustCamera para as faltas limites
		// e verificar os zooms calculados
		#define ZOOM_MIN 0.190f
		#define ZOOM_MAX 1.0f//1.740f
	#endif

	#define ZOOM_MIN_DIST_TO_CHANGE 10.0f

#endif

// Limites de movimentação da câmera

// ( ( SCREEN_WIDTH * ( 1.0f / ZOOM_MIN ) ) * 0.5f )
#define CAMERA_HALF_VIEW_VOLUME_WIDTH  835.0f //DM - OLD:1935f

// ( ( SCREEN_HEIGHT * ( 1.0f / ZOOM_MIN ) ) * 0.5f )
#define CAMERA_HALF_VIEW_VOLUME_HEIGHT 1490.0f //DM - OLD:1290f

// OLD
//#define CAMERA_LEFT_LIMIT		( -1916.0f - CAMERA_HALF_VIEW_VOLUME_WIDTH )
//#define CAMERA_RIGHT_LIMIT	(  1693.0f + CAMERA_HALF_VIEW_VOLUME_WIDTH )
//
//#define CAMERA_TOP_LIMIT		( -1136.0f - CAMERA_HALF_VIEW_VOLUME_HEIGHT )
//#define CAMERA_BOTTOM_LIMIT	(  1156.0f + CAMERA_HALF_VIEW_VOLUME_HEIGHT )

#define CAMERA_LEFT_LIMIT		( -1500.0f - CAMERA_HALF_VIEW_VOLUME_WIDTH ) //DM - OLD: -1750
#define CAMERA_RIGHT_LIMIT		(   400.0f + CAMERA_HALF_VIEW_VOLUME_WIDTH ) //DM - OLD: 400f

#define CAMERA_TOP_LIMIT		( - 600.0f - CAMERA_HALF_VIEW_VOLUME_HEIGHT )
#define CAMERA_BOTTOM_LIMIT		(   600.0f + CAMERA_HALF_VIEW_VOLUME_HEIGHT )

/*=====================================
 
ACELERÔMETROS
 
======================================*/

// Define qual o modo de interpretação dos valores gerados pelos acelerômetros
#define DEFAULT_ACCELEROMETER_CALC_MODE ACC_FORCE_MODE_AVERAGE

// Intervalo com que os acelerômetros reportam seus valores à aplicação
#define ACCELEROMETER_N_UPDATES_PER_SEC 60.0f
#define ACCELEROMETER_UPDATE_INTERVAL ( 1.0f / ACCELEROMETER_N_UPDATES_PER_SEC )

// Quantos valores acumulamos para calcular a posição de repouso do device
#define ACCEL_REST_POS_CALC_N_VALUES 32

// Liberdade de movimento (em graus) dada ao usuário
#define ACCEL_MOVEMENT_FREEDOM_X 40.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_X ( ACCEL_MOVEMENT_FREEDOM_X * 0.5f )

#define ACCEL_MOVEMENT_FREEDOM_Y 30.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_Y ( ACCEL_MOVEMENT_FREEDOM_Y * 0.5f )

// Define o intervalo aceito para o peso dos valores antigos utilzados no cálculo da média da
// aceleração reportada pelos acelerômetros
#define MIN_OLD_VALUES_WEIGHT 0.0f
#define MAX_OLD_VALUES_WEIGHT 0.9f

/*=====================================
 
RESOURCES
 
=====================================*/

// Número máximo de recordes salvos pela aplicação
#define APP_N_RECORDS 5

// Número de fontes suportado pela aplicação
#define APP_N_FONTS 1

// Índices das fontes no vetor de fontes da aplicação
#define APP_FONT_MAIN	0

// Número de textos da aplicação
#define APP_N_TEXTS 6

// Índices dos textos do jogo
#define TEXT_INDEX_HELP						0
#define TEXT_INDEX_GAME_MODE_TRAINING		1
#define TEXT_INDEX_GAME_MODE_RANKING		2
#define TEXT_INDEX_PLAYING_WITH_PROFILE		3
#define TEXT_INDEX_CHANGE_PROFILE			4
#define TEXT_INDEX_PLAYING_WITH_NO_PROFILE	5

// Número de sons da aplicação
#define APP_N_SOUNDS 25

// Índices dos sons do jogo
#define SOUND_INDEX_SPLASH				0
#define SOUND_INDEX_BT_CLICK			1

//TODOO: Burrice... O certo seria reaproveitar o som e não alocar um novo a cada instância, mas não houve tempo para isso
#define SOUND_INDEX_TAG_0_INCOMING		2
#define SOUND_INDEX_TAG_1_INCOMING		3
#define SOUND_INDEX_TAG_2_INCOMING		4
#define SOUND_INDEX_TAG_3_INCOMING		5
#define SOUND_INDEX_TAG_4_INCOMING		6

#define SOUND_INDEX_WHISTLE				7
#define SOUND_INDEX_TARGET_HIT			8
#define SOUND_INDEX_HAIR_MOVE			9
#define SOUND_INDEX_PAUSE_SAMBA			10
#define SOUND_INDEX_FX_AIM_CONFIRM		11
#define SOUND_INDEX_FX_BALL_GROW		12
#define SOUND_INDEX_KICK				13
#define SOUND_INDEX_POST				14
#define SOUND_INDEX_BARRIER_GEN			15
#define SOUND_INDEX_BARRIER_WOODY		16
#define SOUND_INDEX_BARRIER_FEARSOME	17
#define SOUND_INDEX_KEEPER				18
#define SOUND_INDEX_AMBIENT				19
#define SOUND_INDEX_CROWD_AMBIENT		20
#define SOUND_INDEX_CROWD_GOAL			21
#define SOUND_INDEX_TOO_BAD				22
#define SOUND_INDEX_GOAL				23
#define SOUND_INDEX_GAME_OVER			24

// "Nomes" dos sons do jogo
#define SOUND_NAME_SPLASH			0  // aac
#define SOUND_NAME_BT_CLICK			1  // wav
#define SOUND_NAME_TAG_INCOMING		2  // wav
#define SOUND_NAME_WHISTLE			3  // wav
#define SOUND_NAME_TARGET_HIT		4  // wav
#define SOUND_NAME_HAIR_MOVE		5  // wav
#define SOUND_NAME_PAUSE_SAMBA		6  // aac
#define SOUND_NAME_FX_AIM_CONFIRM	7  // wav
#define SOUND_NAME_FX_BALL_GROW		8  // wav
#define SOUND_NAME_KICK				9  // wav
#define SOUND_NAME_POST				10 // wav
#define SOUND_NAME_BARRIER_GEN		11 // wav
#define SOUND_NAME_BARRIER_WOODY	12 // wav
#define SOUND_NAME_BARRIER_FEARSOME	13 // wav
#define SOUND_NAME_KEEPER			14 // wav
#define SOUND_NAME_AMBIENT			15 // wav
#define SOUND_NAME_CROWD_AMBIENT	16 // aac
#define SOUND_NAME_CROWD_GOAL		17 // aac
#define SOUND_NAME_TOO_BAD			18 // wav
#define SOUND_NAME_GOAL				19 // wav
#define SOUND_NAME_GAME_OVER		20 // aac

// Vídeo

// Tempo total do vídeo
#define VIDEO_TOTAL_TIME 13.750f

// Tempo extra no final do vídeo no qual mantemos o último frame fixo, necessário para impedirmos o flickering pós-vídeo
#define VIDEO_EXTRA_TIME  0.550f

// Tempo efetivo do vídeo
#define VIDEO_EFFECTIVE_TIME ( VIDEO_TOTAL_TIME - VIDEO_EXTRA_TIME )

/*=================================================
 
REGRAS
 
=================================================*/

// Limites para cobrança de falta
#define FOUL_LEFT_LIMIT	 ( -REAL_FIELD_WIDTH * 0.5f + FOUL_MIN_DISTANCE_TO_LINE )
#define FOUL_RIGHT_LIMIT ( REAL_FIELD_WIDTH * 0.5f - FOUL_MIN_DISTANCE_TO_LINE )
#define FOUL_MAX_Z_LIMIT ( REAL_FIELD_DEPTH * 0.5f - FOUL_MIN_DISTANCE_TO_LINE )
#define FOUL_MIN_Z_LIMIT ( FOUL_MIN_DISTANCE_TO_LINE )

// Distância mínima da lateral do campo que a bola deve estar para uma falta ser cobrada
#define FOUL_MIN_DISTANCE_TO_LINE		6.0f

// Distância mínima da bola à linha de fundo no modo desafio de faltas
#define FOUL_MIN_Z_DISTANCE				REAL_PENALTY_TO_GOAL

// Distância z máxima que a bola pode estar para ser cobrada uma falta
#define FOUL_CHALLENGE_MAX_Z			20.0f ///DM- OLD:30.0f

// Número de tentativas que o jogador tem para acumular os pontos necessários para passar à próxima fase
#define FOUL_CHALLENGE_TRIES_PER_FOUL	3

// Limites laterais para cobrança de falta
#define FOUL_CHALLENGE_LEFT_LIMIT		( -( REAL_FIELD_WIDTH * 0.5f ) + 3.0f )
#define FOUL_CHALLENGE_RIGHT_LIMIT		(  -FOUL_CHALLENGE_LEFT_LIMIT )

// Tempo de duração da exibição do resultado
#define MATCH_STATE_WAITING_RESULT_DUR 5.0f

// Chance mínima que o jogador possui de bater de dentro da grande área
#define MATCH_MIN_CHANCE_BALL_POS_INSIDE_BIG_AREA 0.0f

// Chance máxima que o jogador possui de bater de dentro da grande área
#define MATCH_MAX_CHANCE_BALL_POS_INSIDE_BIG_AREA 50.0f

// Fase máxima na qual o joagador ainda pode bater de dentro da grande área
#define MATCH_MAX_LEVEL_BALL_POS_INSIDE_BIG_AREA 10

// Maior nível que o jogador pode alcançar
#define MAX_LEVEL 999

// Maior nível de dificuldade que o jogador pode alcançar
#define MAX_DIFFICULTY_LEVEL 50

// Fase a partir da qual a arquibancada fica completamente cheia
#define FIRST_LEVEL_WITH_FULL_CROWD 10

// Distância máxima x ao centro do campo, para efeitos de cálculo da posição do alvo
#define MAX_FOUL_X_DISTANCE ( ( REAL_FIELD_WIDTH / 2.0f ) - 5.0f )

// Porcentagem da área do gol válida para posicionamento do alvo
#define GOAL_AREA_PERCENT 0.4f

// Tempo que o jogador possui em cada etapa
#define MATCH_SETTING_DIR_N_PWR_MAX_TOTAL_TIME	45.0f
#define MATCH_SETTING_DIR_N_PWR_MIN_TOTAL_TIME	20.0f

#define MATCH_SETTING_EFFECT_MAX_TOTAL_TIME		 8.0f
#define MATCH_SETTING_EFFECT_MIN_TOTAL_TIME		 3.0f

// Distância máxima ao centro do alvo na qual força-se o goleiro a não defender a bola
#define BULLS_EYE_DISTANCE 0.65f

// tolerância da distância da bola à mira para facilitar a vida do jogador
#define BULLS_EYE_TOLERANCE ( BULLS_EYE_DISTANCE * 0.3f )

/*=====================================
 
PONTUAÇÃO
 
======================================*/

// Pontuação máxima que um jogador pode obter
#define SCORE_MAX 999999

// Máximo de pontos ganhos num chute
#define SCORE_MAX_POINTS_PER_KICK 250.0f

// Distância máxima do alvo que conta pontos
#define SCORE_MAX_DISTANCE 11.0f

// Mínimo de pontos ganhos num chute
#define SCORE_MIN_POINTS_PER_KICK 0.0f

// Número de pontos ganhos por cada bola restante ao passar de nível
#define SCORE_BONUS_PER_UNUSED_TRY ( SCORE_MAX_POINTS_PER_KICK * 1.5f )

// Pontos de "consolação" (pontos mínimos ganhos numa jogada)
#define SCORE_AFTER_BARRIER_POINTS ( SCORE_MAX_POINTS_PER_KICK * 0.00f )

// Pontos ganhos por marcar um gol
#define SCORE_GOAL_POINTS ( SCORE_MAX_POINTS_PER_KICK * 0.2f )

// Pontos ganhos por atingir a trave
// OBS: SCORE_POST_POINTS DEVE SER MENOR QUE SCORE_GOAL_POINTS !!!!!!!!!!!!!!!!!!!!
#define SCORE_POST_POINTS ( SCORE_MAX_POINTS_PER_KICK * 0.13f )

// Número máximo de pontos ganhos pela distância ao centro do alvo
#define SCORE_PERFECT_SHOOT_POINTS ( SCORE_MAX_POINTS_PER_KICK - SCORE_MIN_POINTS_PER_KICK - SCORE_AFTER_BARRIER_POINTS - SCORE_GOAL_POINTS )

// Porcentagem dos pontos ganhos quando a bola vai para fora
#define SCORE_OUT_FACTOR 0.7f

// Pontuação necessária na fase mais difícil do jogo
#define SCORE_MAX_ACCURACY 0.9f
#define SCORE_NEEDED_ON_MAX_DIFFICULTY_LEVEL ( ( FOUL_CHALLENGE_TRIES_PER_FOUL * SCORE_MAX_POINTS_PER_KICK ) * SCORE_MAX_ACCURACY )

// Expressão que calcula a dificuldade da fase
#define SCORE_MAGIC		57.79452984284f
#define SCORE_FUNC_EXP	2.0f
#define SCORE_FUNC_C	( SCORE_MAX_POINTS_PER_KICK * 0.5f )
#define SCORE_FUNC_A	(( SCORE_NEEDED_ON_MAX_DIFFICULTY_LEVEL - SCORE_FUNC_C ) / ( pow( MAX_DIFFICULTY_LEVEL, SCORE_FUNC_EXP ) + ( SCORE_MAGIC * MAX_DIFFICULTY_LEVEL ) ) )
#define SCORE_FUNC_B	( SCORE_FUNC_A * SCORE_MAGIC )


/*=================================================
 
 TRANSIÇÕES ENTRE JOGADAS
 
=================================================*/
 
#define TRANSITION1_NSLICES 20
#define TRANSITION1_DURATION 1.0f

/*================================================
 
 JOGADA
	Valores mínimos e máximos de cada controle
 
================================================*/

#define ISO_CURVE_MIN_ANG_SPEED   0.0f // Em graus
#define ISO_CURVE_MAX_ANG_SPEED 720.0f // Em graus

#define ISO_DIRECTION_MIN_VALUE ( -( REAL_GOAL_WIDTH + 0.8f ) )
#define ISO_DIRECTION_MAX_VALUE ( -ISO_DIRECTION_MIN_VALUE )

#define ISO_HEIGHT_MIN_VALUE	0.0f
#define ISO_HEIGHT_MAX_VALUE	( REAL_FLOOR_TO_BAR * 4.8f ) // OLD ( REAL_FLOOR_TO_BAR * 4.2f )

#define ISO_POWER_MIN_VALUE 	( REAL_PENALTY_TO_GOAL * 0.8f )
#define ISO_POWER_MAX_VALUE 	( REAL_PENALTY_TO_GOAL * 3.8f ) // OLD ( REAL_FLOOR_TO_BAR * 3.0f )

#define ISO_FX_FACTOR_MAX_VALUE 4.0f
#define ISO_FX_FACTOR_MIN_VALUE ( -ISO_FX_FACTOR_MAX_VALUE )

#define ISO_FX_MAX_DETOUR 8.0f
#define ISO_FX_MIN_DETOUR ( -ISO_FX_MAX_DETOUR )

// Ruído na precisão da mira
#define MAX_KICK_POWER_PERCENT_WITHOUT_NOISE 0.5f

// Em porcentagem
#define MIN_NOISE 0.10f
#define MAX_NOISE 0.25f

/*=====================================
 
VIEWS
 
======================================*/

// Índices das views da aplicação
#define VIEW_INDEX_SPLASH_NANO							0
#define VIEW_INDEX_SPLASH_GAME							1
#define VIEW_INDEX_LOAD_MENU							2
#define VIEW_INDEX_MAIN_MENU							3
#define VIEW_INDEX_PLAY_MENU							4
#define VIEW_INDEX_GAME									5
#define VIEW_INDEX_RANKING								6
#define VIEW_INDEX_HELP									7
#define VIEW_INDEX_CREDITS								8
#define VIEW_INDEX_NEW_GAME								9
#define VIEW_INDEX_LOAD_GAME							10
#define VIEW_INDEX_SELECT_MODE							11
#define VIEW_INDEX_SELECT_DOLL							12
#define VIEW_INDEX_CONFIG_MATCH							13
#define VIEW_INDEX_PAUSE_SCREEN							14
#define VIEW_INDEX_OPTIONS								( VIEW_INDEX_PAUSE_SCREEN + 1 )
#define VIEW_INDEX_PLAY_WITH_PROFILE					( VIEW_INDEX_OPTIONS + 1 )
#define VIEW_INDEX_NANO_ONLINE_FROM_MAIN_MENU			( VIEW_INDEX_PLAY_WITH_PROFILE + 1 )
#define VIEW_INDEX_NANO_ONLINE_FROM_PLAY_WITH_PROFILE	( VIEW_INDEX_NANO_ONLINE_FROM_MAIN_MENU + 1 )

// Opção do menu que está selecionada quando saímos da tela de jogo
#define DEFAULT_MENU_OPT MENU_OPTIONS_INDEX

// Duração padrão das animações de transição de views
#define DEFAULT_TRANSITION_DURATION 0.75f

// Duração do splash da nano (em segundos)
#define SPLASH_NANO_DURATION 2.0f

// Duração do splash do jogo (em segundos)
#define SPLASH_GAME_DURATION 2.0f

/*=====================================
 
CÓDIGOS DE ERRO
 
======================================*/

// Erros que podem terminar a aplicação
#define ERROR_ALLOCATING_DATA	ERROR_USER
#define ERROR_ALLOCATING_VIEWS	ERROR_USER + 1
#define ERROR_CHANGING_COLORS	ERROR_USER + 2
#define ERROR_NO_FRAME_BUFFER	ERROR_USER + 3

/*=====================================
 
VERSÃO DA APLICAÇÃO
 
======================================*/

// TODOO : Pegar a informação da versão do jogo no arquivo info.plist
#define APP_VERSION "1.0"
#define APP_SHORT_NAME "BRAZ"

/*=====================================
 
NANO ONLINE
 
======================================*/

// Número de entradas que ficarão visíveis no ranking online
#define NANO_ONLINE_RANKING_MAX_ENTRIES 30

#endif
