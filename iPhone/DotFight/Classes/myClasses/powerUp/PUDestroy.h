/*
 *  PUDestroy.h
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_DESTROY_H
#define P_U_DESTROY_H 1

#include "PowerUp.h"


class PUDestroy : public PowerUp {
public:
	PUDestroy( PowerUpListener* p );
	
	virtual ~PUDestroy(){}; 
	
	//o código do power up agindo
	virtual void powerUpDo( DotGame* dg );
	
	//atualiza o powerUp caso ele tenha algum  contador
	virtual void updatePowerUp( void ){};
	
	//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
	virtual void gainedByPlayer( Player* p );
	
	virtual void onRelease( void );

	
};



#endif