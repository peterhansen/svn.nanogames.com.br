/*
 *  PUMoveRandom.h
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_MOVE_RANDOM_H
#define P_U_MOVE_RANDOM_H 1

#include"PowerUp.h"


class PUMoveRandom : public PowerUp{

	public:
	
	PUMoveRandom( PowerUpListener *p );

	virtual ~PUMoveRandom( void ){};
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
	virtual void cpuDo( Player* p ){};

	
	private:
	uint8 nBonusLines;
};


#endif