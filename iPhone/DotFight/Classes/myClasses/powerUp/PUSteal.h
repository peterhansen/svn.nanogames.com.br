/*
 *  PUSteal.h
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_STEAL_H
#define P_U_STEAL_H 1

#include "PowerUp.h"

class PUSteal : public PowerUp {

	
public:
	
	PUSteal( PowerUpListener *p );
	
	virtual ~PUSteal( void ){};	
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
private:
//	figuras aqui!!
	
};



#endif