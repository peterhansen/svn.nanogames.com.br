/*
 *  PUComboBreak.mm
 *  dotGame
 *
 *  Created by Max on 2/4/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUComboBreak.h"
#include "DotGame.h"


PUComboBreak::PUComboBreak( PowerUpListener* p ):PowerUp( POWER_UP_TYPE_COMBO_BREAK, p ){}

//o código do power up agindo
void PUComboBreak::powerUpDo( DotGame* dg ){
//agindo
	
#if DEBUG
	NSLog(@"combobreak agindo-> diminui a contagem do combo feita pelo mané");
#endif
	dg->setComboValue( dg->getComboValue() / 2 );

}

//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
void PUComboBreak::gainedByPlayer( Player* p ){
#if DEBUG
	NSLog(@"combo break solto!!");
#endif

	onRelease();

}

void PUComboBreak::onRelease( void ){

	powerUpDo( pListener->getDotGameInstance() );
	

}
