/*
 *  PUComboBreak.h
 *  dotGame
 *
 *  Created by Max on 2/4/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_COMBO_BREAK_H
#define P_U_COMBO_BREAK_H 1

#include"PowerUp.h"

class PUComboBreak : public PowerUp {
	
public:
	
	PUComboBreak( PowerUpListener* p );
	
	virtual	~PUComboBreak(){};

		//o código do power up agindo
	virtual void powerUpDo( DotGame* dg );
	
	//atualiza o powerUp caso ele tenha algum  contador
	virtual void updatePowerUp( void ){};
	
	//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
	virtual void gainedByPlayer( Player* p );
	
	virtual void onRelease( void );

private:
	
	
	
};


#endif