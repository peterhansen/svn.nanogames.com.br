/*
 *  PowerUp.mm
 *  dotGame
 *
 *  Created by Max on 2/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUp.h"
#include "ZoneGame.h"
#include "Player.h"
#include "Random.h"
#include "ZoneGroup.h"
#include "LineGroup.h"


#define MAX_OBJECTS_INSIDE_POWER_UP 2

PowerUp::PowerUp( PowerUpType p , PowerUpListener *listener ):
ObjectGroup( MAX_OBJECTS_INSIDE_POWER_UP ),
pListener( listener ),
pwrUpType( p ),
disarmed( false ),
lineManager( NULL ),
zoneManager( NULL ),
//state( POWER_UP_STATE_NONE ),
zoneOf( NULL ){ 
	setVisible( true );
	RenderableImage* r;
	r = new RenderableImage("pause");
	r->setVisible(true);
	insertObject( r );
}

PowerUp::~PowerUp( void ){
	pListener = NULL;
	zoneOf = NULL;
	lineManager = NULL;
	zoneManager = NULL;
	pPlayer = NULL;
	
#if DEBUG
	NSLog(@" destruindo PowerUp ");
#endif
}
/* ======================================================================================================
 
 onrelease->fç chamada qdo o power up é lançado, chamando o seu listener para que ele faça algo de acordo com o tipo de power up.
 
 ======================================================================================================*/
/*void PowerUp::onRelease( void ){
	setVisible( false );
#if DEBUG
	NSLog(@" pwrup posicao x = %.2f,y = %.2f,z = %.2f\n ", position.x, position.y, position.z);
#endif
	//colocar  aqui código de animação ou qq coisa assim que aconteça neste momento;
	pListener->PowerUpEffect( this );
}*/
/* ======================================================================================================
 
 onrelease->fç chamada para associar um power up à ua zona do tabuleiro
 
 ======================================================================================================*/
void PowerUp::setZone( ZoneGame *z ){
	zoneOf = z;
	Point3f p;
	p = *z->getPosition();
	uint8 x,y;
	ZoneGroup::getZoneXYByPosition( &p , &x ,&y );
	p = ZoneGroup::getZonePositionByXY( x, y );
	//p += *z->getParent()->getPosition();
	position = p;
}
/* ======================================================================================================
 
 setListener->fç chamada para escolher o seu listener
 
 ======================================================================================================*/
void PowerUp::setListener( PowerUpListener *p ){
	pListener = p;
}
/* ======================================================================================================
 
 setPosition->usada para posicionar o powerUp
 
 ======================================================================================================*/
void PowerUp::setPosition(uint8 x, uint8 y ){
	Point3f p;
	p = ZoneGroup::getZonePositionByXY( x, y );
	position.set( p );
}


//diz o local onde o power up vai agir
void PowerUp::InputPosition( Point3f *p ){

	pPonto = p;
}
bool PowerUp::render(){
	
//#if DEBUG
//	NSLog(@"renderizando power up!");
//#endif
	return ObjectGroup::render();
}


Point3f* PowerUp::sortLineRandom( void ){
	uint8 x,y;
	bool horizontal;
	Point3f pLinha, *ret;
	for(;;){
		horizontal =  Random::GetInt( 0 , 1) == 0 ;
		x = Random::GetInt( 0, lineManager->getTotalLinesH() - 1);	
		y = Random::GetInt( 0, lineManager->getTotalLinesV() - 1);
		if ( lineManager->isLineGained( LineGroup::getLinePCoordenatesByXY( x, y, horizontal) ) ){
			
			pLinha =  LineGroup::getLineCoordenatesByXY( x, y, horizontal);
			break;	
		}
	}
	ret = &pLinha;
	return ret;
}

Point3f* PowerUp::sortZoneRandom( void ){
	uint8 x,y;
	Point3f pZona, *ret;
	for(;;){
		x = Random::GetInt( 0, zoneManager->getnZonesH() - 1);	
		y = Random::GetInt( 0, zoneManager->getnZonesV() - 1);
		if ( zoneManager->isZoneOccupied( ZoneGroup::getZonePPositionByXY( x, y ) ) ){
			
			pZona =  ZoneGroup::getZonePositionByXY( x, y );
			break;	
		}
	}
	ret = &pZona;
}