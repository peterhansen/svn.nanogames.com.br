/*
 *  PUBomb.h
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_BOMB_H
#define POWER_UP_BOMB_H 1

#include "PowerUp.h"

enum BombSize {
	BOMB_SIZE_SMALL = -1 ,
	BOMB_SIZE_MEDIUM = 0 ,
	BOMB_SIZE_BIG
}typedef BombSize;

class PowerUpListener;

class PUBomb : public PowerUp {
public:
	
	PUBomb( PowerUpListener *p );
	
 virtual ~PUBomb( void ){};
		
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void );
	
	virtual	void powerUpDo( DotGame* dg );
	
	
	//código para a cpu agir!!
	virtual void cpuDo( Player* p );
	
	
private:
	inline	uint8 getTimeToExplode( void );
	
	inline	void setTimeToExplode( uint8 t);
	//efeito na linha pressionada
	void effectInLine( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager, DotGame* dg);
	
	//efeito na zona pressionada
	void effectInZone( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager,DotGame* dg);
	
	bool buildBomb( void );
	
	uint8 timeToExplode;
		
	BombSize sizeBomb;
	bool timeOver;
	/*
	 provavelmente vai entrar aqui variáveis estáticas contendo as imagens de cada um dos estágios da bomba
	 */
};


uint8 PUBomb::getTimeToExplode( void ){ return timeToExplode; }
void PUBomb::setTimeToExplode( uint8 t){ timeToExplode = t; }

#endif