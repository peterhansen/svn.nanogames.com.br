/*
 *  PUMoveBonus.h
 *  dotGame
 *
 *  Created by Max on 3/15/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_MOVE_BONUS_H
#define POWER_UP_MOVE_BONUS_H 1

#include "PowerUp.h"


class PUMoveBonus : public PowerUp {
public:
	
	PUMoveBonus( PowerUpListener *p );
	
	virtual ~PUMoveBonus( void ){};
		
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );	
};

#endif