/*
 *  PUSteal.mm
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUSteal.h"
#include "Player.h"
#include "DotGame.h"

PUSteal::PUSteal( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_STEAL ,p ){


	
}


void  PUSteal::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );


}
	
void PUSteal::gainedByPlayer( Player* p ){
	onRelease();
}
	
void PUSteal::powerUpDo( DotGame* dg ){
	PowerUp *pow;
	Player *p, *p2;
	p = dg->getCurrPlayer();
	p2 = dg->getNextPLayer();
	pow = p2->getPowerUp();
	p->setPowerUp( pow );
	p2->destroyPowerUp();
}