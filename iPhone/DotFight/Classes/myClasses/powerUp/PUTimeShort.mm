/*
 *  PUTimeShort.mm
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUTimeShort.h"
#include "DotGame.h"


PUTimeShort::PUTimeShort( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_TIME_SHORT , p ){

}

void PUTimeShort::onRelease( void ){

	powerUpDo( pListener->getDotGameInstance() );
}

void PUTimeShort::gainedByPlayer( Player* p ){

	onRelease();
}



void PUTimeShort::powerUpDo( DotGame* dg ){

	dg->setDivisorValue( dg->getDivisorValue() * 2 );
}