/*
 *  PUTimeShort.h
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_TIME_SHORT_H
#define POWER_UP_TIME_SHORT_H 1

#include "PowerUp.h"

class PUTimeShort : public PowerUp {
	
public:
	
	PUTimeShort( PowerUpListener *p );
	
	virtual ~PUTimeShort(){};
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
protected:
	
};

#endif