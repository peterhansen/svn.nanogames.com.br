/*
 *  PUZoneDestroy.h
 *  dotGame
 *
 *  Created by Max on 3/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_ZONE_DESTROY_H
#define P_U_ZONE_DESTROY_H 1

#include "PowerUp.h"

class PUZoneDestroy : public PowerUp {

public:
	
	PUZoneDestroy( PowerUpListener *p );
	
	virtual ~PUZoneDestroy(){};	
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
private:
	void effectInLine( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager, DotGame* dg);

	void effectInZone( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager, DotGame* dg);
};


#endif