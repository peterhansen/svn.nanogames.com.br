/*
 *  PUDestroy.mm
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUDestroy.h"
#include "DotGame.h"
#include "Player.h"


PUDestroy::PUDestroy( PowerUpListener* p ):PowerUp( POWER_UP_TYPE_DESTROY, p ){}



//o código do power up agindo
void PUDestroy::powerUpDo( DotGame* dg ){
#if DEBUG
	NSLog(@"destruindo power up do adversário");
#endif
	
	Player *p;
	//como estou fazendo pensando para 2 jogadores, ele está destruindo o PUp do próximo jogador, caso tenha que se mudar para fazer de tal maneira que se tenha destruir o resto, mudar para getPlayerById, e entrar com o id do jogador.
	p = dg->getNextPLayer();
	p->destroyPowerUp();
}

//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
void PUDestroy::gainedByPlayer( Player* p ){
	
	onRelease();
}

void PUDestroy::onRelease( void ){

	powerUpDo( pListener->getDotGameInstance() );

}

