/*
 *  PowerUp.h
 *  dotGame
 *
 *  Created by Max on 2/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_H
#define POWER_UP_H 1

#include "RenderableImage.h"
#include "ObjectGroup.h"
#include "PowerUpFactory.h"

class ZoneGame;
class PowerUpListener;
class Player;
class LineGroup;
class ZoneGroup;
class DotGame;

//enum PowerUpState {
//	POWER_UP_STATE_NONE = 0,
//	POWER_UP_STATE_WAITING_INPUT,
////	POWER_UP_STATE_
//}typedef PowerUpState;

enum PowerUpTarget {
	POWER_UP_TARGET_PLAYER = -1,
	POWER_UP_TARGET_LINE = 0,
	POWER_UP_TARGET_ZONE,
//	POWER_UP_TARGET_PLAYER,
}typedef PowerUpTarget;

class PowerUp : ObjectGroup {
	
public:
	
	PowerUp( PowerUpType p , PowerUpListener *p = NULL );
	
	~PowerUp( void );

	bool isDisarmed( void );
	
	void setListener( PowerUpListener *p );
	
	PowerUpListener* getPowerUpListener( void );
	
	PowerUpType getPowerUpType( void );

	void setZone( ZoneGame *z );

	virtual bool render();
	
	//código para a cpu agir!!
	virtual void cpuDo( Player* p ){};
	
#if DEBUG
 void setName(const char* objName){
		ObjectGroup::setName( objName );};
#endif
	
	void setPosition( uint8 x, uint8 y );

	//diz o local onde o power up vai agir
	void InputPosition( Point3f *p );

	//o código do power up agindo
	virtual void powerUpDo( DotGame* dg ) = 0 ;
	
	//atualiza o powerUp caso ele tenha algum  contador
	virtual void updatePowerUp( void ) {}//= 0;
	
	//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
	virtual void gainedByPlayer( Player* p ) = 0;
	
	virtual void onRelease( void ) = 0;
	
	void setLineGroup( LineGroup *l ){ lineManager = l; };
	
	void setZoneGroup( ZoneGroup *z ){ zoneManager = z; };
	
protected:	
	
	Point3f* sortLineRandom( void );
	
	Point3f* sortZoneRandom( void );
	
	
	bool disarmed;
	ZoneGame* zoneOf;
	LineGroup* lineManager;
	ZoneGroup* zoneManager;
	PowerUpListener *pListener;
	PowerUpType pwrUpType;
//	PowerUpState state;
	Point3f *pPonto;
	PowerUpTarget target;
	Player *pPlayer;	
};


inline bool PowerUp::isDisarmed( void ){
	return disarmed;
}

inline	PowerUpType PowerUp::getPowerUpType( void ){
	return pwrUpType;
}

inline PowerUpListener* PowerUp::getPowerUpListener( void ){
	return pListener;
}

class PowerUpListener{
public:
	virtual ~PowerUpListener( void ){};
	virtual void PowerUpEffect( PowerUp* p ) = 0;
	virtual DotGame* getDotGameInstance( void ) = 0;
	
};
#endif