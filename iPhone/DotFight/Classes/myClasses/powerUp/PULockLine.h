/*
 *  PULockLine.h
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef PU_LOCK_LINE_H
#define PU_LOCK_LINE_H 1


#include "PowerUp.h"

class PULockLine : public PowerUp {
public:
	
	PULockLine( PowerUpListener *p );

	virtual ~PULockLine( void ){};
	
	virtual void onRelease( void );

	virtual void cpuDo( Player* p );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
private:
	//colocar figuras aqui e os seus getters and setters logo acima...
};


#endif