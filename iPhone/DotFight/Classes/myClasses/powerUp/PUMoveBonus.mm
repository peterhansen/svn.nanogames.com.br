/*
 *  PUMoveBonus.mm
 *  dotGame
 *
 *  Created by Max on 3/15/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUMoveBonus.h"
#include "DotGame.h"

PUMoveBonus::PUMoveBonus( PowerUpListener *p ):	PowerUp( POWER_UP_TYPE_MOVE_BONUS , p  ){}

void PUMoveBonus::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}

void PUMoveBonus::gainedByPlayer( Player* p ){
//depois mudar para ser qdo o jogador quiser
	onRelease();
}

void PUMoveBonus::powerUpDo( DotGame* dg ){
	dg->setLinesSelected( dg->getLinesSelected() - 2 );

}