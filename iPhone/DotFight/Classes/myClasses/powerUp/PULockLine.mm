/*
 *  PULockLine.mm
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PULockLine.h"
#include "DotGame.h"
#include "Random.h"

PULockLine::PULockLine( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_LOCK_LINE,p){}

void PULockLine::onRelease( void ){

	DotGame *dg;
	dg = pListener->getDotGameInstance();
	//dg->setDotGameState( DOT_GAME_STATE_WAITING_POWER_UP_INPUT );
	lineManager = dg->getLineGroup();
	if( pPlayer->getTypeUser() == PLAYER_USER_CPU ){
		uint8 x,y;
		bool vertical;
		for(;;){
			vertical =  Random::GetInt( 0 , 1) == 0 ;
			x = Random::GetInt( 0, lineManager->getTotalLinesH());	
			y = Random::GetInt( 0, lineManager->getTotalLinesV());
			if ( !lineManager->isLineGained( LineGroup::getLinePCoordenatesByXY( x, y, vertical) ) )
				break;		
		}
		lineManager->lockLine( LineGroup::getLinePCoordenatesByXY( x, y, vertical ) );
		return;
	}
	lineManager->lockLine( pPonto );
	
}

void PULockLine::gainedByPlayer( Player* p ){
	pPlayer = p;
	onRelease();
	//powerUpDo( pListener->getDotGameInstance() );
}

void PULockLine::cpuDo( Player* p ){

}
void PULockLine::powerUpDo( DotGame* dg ){
	LineGroup *l;
	l = dg->getLineGroup();
	l->lockLine( pPonto );
//	dg->setDotGameState();
}