/*
 *  PUZoneDestroy.mm
 *  dotGame
 *
 *  Created by Max on 3/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUZoneDestroy.h"
#include "ZoneGroup.h"
#include "Random.h"
#include "Player.h"
#include "DotGame.h"

PUZoneDestroy::PUZoneDestroy( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_DESTROY_ZONE ,p ){

}

void PUZoneDestroy::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}
	
void PUZoneDestroy::gainedByPlayer( Player* p ){
	
	onRelease();
	p->destroyPowerUp();

}
	
void PUZoneDestroy::powerUpDo( DotGame* dg ){
	
	uint8 x,y,i,j;
	ZoneGroup* z;
	Point3f p;
	
	z = dg->getZoneGroup();
	
	x = z->getnZonesH();
	y = z->getnZonesV();
	
	//vai sortear até sair uma zona ocupada
	for(;;){
		i = Random::GetInt( 0, x - 1 );
		j = Random::GetInt( 0, y - 1 );
		p = z->getZonePositionByXY( i, j );
		if( z->isZoneOccupied( &p ) )
			break;
	}
	
	
	effectInZone( &p, dg->getLineGroup(), z, dg);
}
// efeito na linha
void PUZoneDestroy::effectInLine( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager,  DotGame* dg){
	
	/*
	 como cada zona/quadrado só pode pertencer à uma e somente uma bigzona/quadradao, basta verificarmos quais das zonas seguintes pertence à bigzona/ quadradao, e depois desfazê-la.como funciona com uma linha, tem que se fazer com as duas zonas/quadrado adjacentes.
	 */
	
	if( !lineManager->isLineGained( p ) )
		return;
	
	Point3f p1;
	Point3f p2;
	
	uint8 x,y;
	bool horizontal;
		
	horizontal = lineManager->isLineHorizontal( p );
	
	if( horizontal ){
		lineManager->getLineXYByPosition( p, false, &x, &y);
		p2 = ZoneGroup::getZonePositionByXY( x , y - 1 );
	}else {
		lineManager->getLineXYByPosition( p, true, &x, &y);		
		p2 = ZoneGroup::getZonePositionByXY( x - 1, y );
	}
	
	
	p1 = ZoneGroup::getZonePositionByXY( x, y );
	
	lineManager->lineLostByPlayer( p );
	
#if DEBUG
	NSLog(@"agindo na linha :( %3.3f, %3.3f ) ", p->x,p->y );
#endif
	
	dg->unMakeBigZone( &p1 );
	dg->unMakeBigZone( &p2 );
	
}

//efeito na zona 
void PUZoneDestroy::effectInZone( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager, DotGame* dg){

	if( !ZoneManager->isZoneOccupied( p ) )
		return;
	
	uint8 x,y;
	ZoneManager->getZoneXYByPosition( p , &x, &y);
	
	Point3f p1,p2,p3,p4;
#if DEBUG
	NSLog(@"agindo na zona/quadrado :( %3.3f, %3.3f ), e nas suas linhas adjacentes ", p->x,p->y );
#endif

	p1 = LineGroup::getLineCoordenatesByXY( x , y ,true );
	p2 = LineGroup::getLineCoordenatesByXY( x + 1 , y ,true );
	p3 = LineGroup::getLineCoordenatesByXY( x , y ,false );
	p4 = LineGroup::getLineCoordenatesByXY( x , y + 1, false );
	
	effectInLine( &p1, lineManager, ZoneManager, dg );
	effectInLine( &p2, lineManager, ZoneManager, dg );
	effectInLine( &p3, lineManager, ZoneManager, dg );
	effectInLine( &p4, lineManager, ZoneManager, dg );
}
