/*
 *  PUMoveRandom.mm
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUMoveRandom.h"
#include "DotGame.h"
#include "Random.h"

#define MIN_BONUS_LINES 10


PUMoveRandom::PUMoveRandom( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_MOVE_RANDOM, p ),
nBonusLines( MIN_BONUS_LINES ){}

void PUMoveRandom::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}

void PUMoveRandom::gainedByPlayer( Player* p ){

	onRelease();
	
}

void PUMoveRandom::powerUpDo( DotGame* dg ){
	DotGame *dgame;
	dgame = pListener->getDotGameInstance();
	lineManager = dgame->getLineGroup();
	uint8 x,y;
	bool vertical;
	Point3f p;
	for ( uint8 i = 0 ; i < nBonusLines ; i++ ) {
		
		for(;;){
			vertical =  Random::GetInt( 0 , 1) == 0 ;
			
			if( vertical ){		
				x = Random::GetInt( 0, lineManager->getTotalLinesH() - 1);	
				y = Random::GetInt( 0, lineManager->getTotalZonesV() - 1);
				}else{	
				x = Random::GetInt( 0, lineManager->getTotalZonesH() - 1);		
				y = Random::GetInt( 0, lineManager->getTotalLinesV() - 1);

			}
			
			p = LineGroup::getLineCoordenatesByXY( x, y, vertical ); 
			if ( !lineManager->isLineGained( &p) )
				break;		
		}
		
	lineManager->selectLine( &p , dgame->getCurrPlayer() );
		if( vertical )
			dg->verifyZonesVertical( &p );
		else
			dg->verifyZonesHorizontal( &p );
	}
}
