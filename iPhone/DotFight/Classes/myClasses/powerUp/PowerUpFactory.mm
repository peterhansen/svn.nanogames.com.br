/*
 *  PowerUpFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/18/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUpFactory.h"
#include "ObjcMacros.h"
#include "PUComboBreak.h"
#include "PUBomb.h"
#include "PUZoneDestroy.h"
#include "PUSteal.h"
#include "PUDestroy.h"
#include "PULockLine.h"
#include "PUMoveBonus.h"
#include "PUTimeShort.h"
#include "PUMoveRandom.h"


PowerUpFactory* PowerUpFactory::pSingleton = NULL;

bool PowerUpFactory::Create( void ){
	if( !pSingleton )
		pSingleton = new PowerUpFactory();
	
	return pSingleton!= NULL;
}

void PowerUpFactory::Destroy( void ){
	SAFE_DELETE( pSingleton );
}
/*
 retorna um novo powerUp, caso seja de um tipo desconhecido, retorna NULL!
 */
PowerUp* PowerUpFactory::getNewPowerUp( PowerUpType t , PowerUpListener/*DotGame*/ *dg){
	if( !pSingleton )
		Create();

	PowerUp *p = NULL;
	
	switch ( t ) {
		case POWER_UP_TYPE_COMBO_BREAK:
			//confere
			p  = new PUComboBreak( dg );			
			break;

		case POWER_UP_TYPE_BOMB:
			p = new PUBomb( dg );
			break;
		
		case POWER_UP_TYPE_MOVE_BONUS:
			//confere
			p = new PUMoveBonus( dg );
			break;
		
		case POWER_UP_TYPE_MOVE_RANDOM:
			//confere
			p = new PUMoveRandom( dg );
			break;
		
		case POWER_UP_TYPE_TIME_SHORT:
			//confere
			p = new PUTimeShort( dg );
			break;
		
		case POWER_UP_TYPE_DESTROY_ZONE:
			p = new PUZoneDestroy( dg );
			break;
		
		case POWER_UP_TYPE_LOCK_LINE:
			//confere
			p = new PULockLine( dg );
			break;

		case POWER_UP_TYPE_STEAL:
			//confere
			p = new PUSteal( dg );
			break;

		case POWER_UP_TYPE_DESTROY:
			//confere
			p = new PUDestroy( dg );
			break;

	}
	
	return p;
	
}

PowerUpFactory::PowerUpFactory():nPowerTypes( N_POWER_UPS ){
#if DEBUG
	NSLog(@"powerUpFactory criada!");
#endif
}
	
PowerUpFactory::~PowerUpFactory(){
#if DEBUG
	NSLog(@"powerUpFactory destruída");
#endif
	
}
