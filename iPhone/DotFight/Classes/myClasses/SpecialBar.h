/*
 *  SpecialBar.h
 *  dotGame
 *
 *  Created by Max on 3/24/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SPECIAL_BAR_H
#define SPECIAL_BAR_H 1

//#include"RenderableImage.h"
#include "ObjectGroup.h"
#include "Color.h"
#include "BoardFactory.h"



enum SpecialBarState{
	SPECIAL_BAR_STATE_EMPTY = 0,
	SPECIAL_BAR_STATE_LOW,
	SPECIAL_BAR_STATE_MEDIUM,
	SPECIAL_BAR_STATE_FULL
}typedef SpecialBarState;

#include "RenderableImage.h"

/*
 colocar 2 figuras, uma para ser o fundo, e outra para ser a barra
 */
class SpecialBar : public ObjectGroup {
	
public:
	SpecialBar();
	
	virtual ~SpecialBar();
	
	void setSpecialBarState( SpecialBarState s );
	
	void updateBar( float f );
	
	inline	SpecialBarState getSpecialBarState( void );
	
	virtual bool render( void );
	
	void setColorBarBgd( Color c );

	void setColorBar( Color c );
	
private:
	
	void updateColor( void );

	void buildSpecialBar( void );
	
	Color colorBgd, colorBar;
	SpecialBarState state;
	float percentVisible;
//	RenderableImage bgd;
//	RenderableImage bar;
	
};

 inline SpecialBarState SpecialBar::getSpecialBarState( void ){
	 return state;
}


#endif