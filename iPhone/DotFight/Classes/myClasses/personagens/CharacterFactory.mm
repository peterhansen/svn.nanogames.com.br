/*
 *  CharacterFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "CharacterFactory.h"
#include "GenericDoll.h"
#include "GameBaseInfo.h"
#include "Diabinho.h"
#include "Ogro.h"
#include "Anjinho.h"
#include "Executivo.h"
#include "Fadinha.h"
#include "Fazendeiro.h"
#include "BoardFactory.h"

CharacterFactory* CharacterFactory::pSingleton = NULL ;

bool CharacterFactory::Create( void ){
if( !pSingleton )
	pSingleton = new CharacterFactory();
	
	return pSingleton != NULL;

}

void CharacterFactory::Destroy( void ){ 
	SAFE_DELETE( pSingleton );
}

CharacterFactory::~CharacterFactory(){
	//p
#if DEBUG
	NSLog(@"destruindo CharacterFactory");	
#endif
	
}


//seria melhor se fosse dinâmico...
CharacterFactory::CharacterFactory():nCharacters( N_DOLLS ){
#if DEBUG
	NSLog(@"criando CharacterFactory");	
#endif
	
}


//void CharacterFactory::LoadCharacterIndice( GenericDoll *g , int indice ){}

GenericDoll* CharacterFactory::LoadCharacterIndice( IdDoll id ){
	GenericDoll* g;
	
	switch ( id ) {
		case DOLL_ID_ANGEL :
			g = new Anjinho();
			break;
		case DOLL_ID_DEVIL :
			g = new Diabinho();
			break;
		case DOLL_ID_FAIRY :
			g = new Fadinha();
			break;
		case DOLL_ID_OGRE :
			g = new Ogro();
			break;
		case DOLL_ID_EXECUTIVE :
			g = new Executivo();
			break;
		case DOLL_ID_FARMER :
			g = new Fazendeiro();
			break;
			
	}
	return g;
}

void CharacterFactory::LoadAllImages( GameBaseInfo &g ){
	for ( int8 x = 0 ; x < g.nPlayers ; x++) {
		switch ( g.iddolls[ x ] ) {
			case DOLL_ID_ANGEL :
				Anjinho::LoadAllImages();
				break;
			case DOLL_ID_DEVIL :
				Diabinho::LoadAllImages();
				break;
			case DOLL_ID_FAIRY :
				Fadinha::LoadAllImages();
				break;
			case DOLL_ID_OGRE :
				Ogro::LoadAllImages();
				break;
			case DOLL_ID_EXECUTIVE :
				Executivo::LoadAllImages();
				break;
			case DOLL_ID_FARMER :
				Fazendeiro::LoadAllImages();
				break;
		}
	}
	
#if DEBUG
	NSLog(@"imagens dos jogadores carregadas");
#endif	
}

void CharacterFactory::RemoveAllImages( GameBaseInfo &g ){
	for ( int8 x = 0 ; x < g.nPlayers ; x++) {
		switch ( g.iddolls[ x ] ) {
			case DOLL_ID_ANGEL :
				Anjinho::RemoveAllImages();
				break;
			case DOLL_ID_DEVIL :
				Diabinho::RemoveAllImages();
				break;
			case DOLL_ID_FAIRY :
				Fadinha::RemoveAllImages();
				break;
			case DOLL_ID_OGRE :
				Ogro::RemoveAllImages();
				break;
			case DOLL_ID_EXECUTIVE :
				Executivo::RemoveAllImages();
				break;
			case DOLL_ID_FARMER :
				Fazendeiro::RemoveAllImages();
				break;
		}
	}
}


//retorna 255 qdo hover erro
uint8 CharacterFactory::setFrameByCharacter( IdDoll _id ){
//uint8 r
	switch ( _id ) {
		case DOLL_ID_ANGEL :
			return INDEX_MARK_ANGEL;
			break;
		case DOLL_ID_DEVIL :
			return INDEX_MARK_DEVIL;
			break;
		case DOLL_ID_FAIRY :
			return INDEX_MARK_FAIRY;
			break;
		case DOLL_ID_OGRE :
			return INDEX_MARK_OGRE;
			break;
		case DOLL_ID_EXECUTIVE :
			return INDEX_MARK_EXECUTIVE;
			break;
		case DOLL_ID_FARMER :
			return INDEX_MARK_FARMER;
			break;
	}
	return 255;
}

UIImage* CharacterFactory::getPicturePersonagen( uint8 index ){
	UIImage* tmp = NULL;
	/*switch ( index ) {
		case ID_PERSON_ANGEL:{}
			//colocar a imagem aqui, e depois retorná-las		
			break;
		case ID_PERSON_DEVIL:{}
			//colocar a imagem aqui, e depois retorná-las		
			break;
		case ID_PERSON_EXECUTIVE:{}
			//colocar a imagem aqui, e depois retorná-las		
			break;
		case ID_PERSON_FARMER:{}
			//colocar a imagem aqui, e depois retorná-las		
			break;
		case ID_PERSON_FAIRY:{}
			//colocar a imagem aqui, e depois retorná-las		
			break;
		case ID_PERSON_OGRE:{}
			//colocar a imagem aqui, e depois retorná-las		
			break;
	}*/	
	
	
	return tmp;
}



