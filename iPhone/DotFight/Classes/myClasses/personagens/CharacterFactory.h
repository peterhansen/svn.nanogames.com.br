/*
 *  CharacterFactory.h
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef CHARACTER_FACTORY_H
#define CHARACTER_FACTORY_H 1

//#include "DotGameInfo.h"

#define N_DOLLS 6
#include "NanoTypes.h"
//idem...
enum IdDoll {
	DOLL_ID_NONE = -1,
	DOLL_ID_ANGEL = 0,
	DOLL_ID_DEVIL = 1,
	DOLL_ID_FARMER,
	DOLL_ID_EXECUTIVE,
	DOLL_ID_FAIRY,
	DOLL_ID_OGRE	
} typedef IdDoll;

#define ID_PERSON_ANGEL 1;
#define ID_PERSON_DEVIL 2;
#define ID_PERSON_FARMER 3;
#define ID_PERSON_EXECUTIVE 4;
#define ID_PERSON_FAIRY 5;
#define ID_PERSON_OGRE 6;


class GenericDoll;
struct GameBaseInfo;
class CharacterFactory {
	
public:

	
//void LoadCharacterIndice( GenericDoll *g , int indice );
	
GenericDoll */*void*/ LoadCharacterIndice( /*GenericDoll *g ,*/ IdDoll id );
	
	static bool Create( void );
	
	static void Destroy( void );

	static CharacterFactory* GetInstance( void ) { return pSingleton; };
	
	void LoadAllImages( GameBaseInfo &g );
	
	void RemoveAllImages( GameBaseInfo &g );
	
	void setNCharacters( uint8 n  ){ nCharacters = n;  }
	
	uint8 getNCharacters( void );
	
	
	uint8 setFrameByCharacter( IdDoll _id );
	
	UIImage* getPicturePersonagen( uint8 index );

private:	
	CharacterFactory();

	~CharacterFactory();
	static CharacterFactory* pSingleton;
	
	uint8 nCharacters;
	
};

inline uint8 CharacterFactory::getNCharacters(){ return nCharacters; }
#endif
