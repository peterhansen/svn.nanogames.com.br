/*
 *  GenericDoll.h
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef GENERIC_DOLL_H
#define GENERIC_DOLL_H 1

#include "DotGameInfo.h"
#include "RenderableImage.h"
#include "Sprite.h"
#include "Color.h"
#include "DotGameInfo.h"
#include "CharacterFactory.h"


//enum DollExpressions {
//	
//	DOLL_EXPRESSION_NONE = -1,
//	DOLL_EXPRESSION_SAD = 0,
//	DOLL_EXPRESSION_HAPPY,
//}typedef DollExpressions;
//
////idem...
//enum IdDoll {
//	DOLL_ID_NONE = -1,
//	DOLL_ID_ANGEL = 0,
//	DOLL_ID_DEVIL = 1,
//	DOLL_ID_FARMER,
//	DOLL_ID_EXECUTIVE,
//	DOLL_ID_FAIRY,
//	DOLL_ID_OGRE	
//} typedef IdDoll;

class GenericDoll{
	
public:
	
	GenericDoll( IdDoll Idboneco );
		
	IdDoll getIdDoll( void );
	 
	void getColor( Color* pColor );
	
	//virtual Texture2DHandler getFacePic( void )=0;

	virtual Sprite* getLineHPic( void ) = 0;	

	virtual Sprite* getLineVPic( void ) = 0;
	
//	virtual Sprite* getZoneSprt( void ) = 0 ;
//	
//	virtual	Sprite* getBigZoneSprt( void ) = 0;
	
	virtual Texture2DHandler getZoneSprt( void ) = 0 ;
	
	virtual	Texture2DHandler getBigZoneSprt( void ) = 0;
	
protected:
	
	IdDoll iddoll ;
	
	DollExpressions face;
	
	friend class LineGame;
	friend class ZoneGame;
	Color bColor;
//private:
	//virtual void buildGenericDoll( void )=0;
	
};

inline /*Color**/ void GenericDoll::getColor( Color* pColor ) { pColor = &bColor; }

inline IdDoll GenericDoll::getIdDoll( void ){ return iddoll; }

//void GenericDoll::setColor( const Color& _color ) { color = _color;  }


#endif