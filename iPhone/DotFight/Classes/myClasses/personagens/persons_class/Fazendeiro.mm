/*
 *  Fazendeiro.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Fazendeiro.h"
#include "Random.h"
#include"Sprite.h"


//Sprite* Anjinho::bigZoneSpt = NULL;
//
//Sprite* Anjinho::ZoneSpt1= NULL;
//
//Sprite* Anjinho::ZoneSpt2= NULL;


Texture2DHandler Fazendeiro::bigZoneSpt = NULL;

Texture2DHandler Fazendeiro::ZoneSpt1= NULL;

Texture2DHandler Fazendeiro::ZoneSpt2= NULL;

Sprite* Fazendeiro::LineV = NULL;

Sprite* Fazendeiro::LineH = NULL;	


Fazendeiro::Fazendeiro():GenericDoll( DOLL_ID_FARMER ){}


void Fazendeiro::LoadAllImages( void ){
Texture2DHandler htex1, htex2, htex3;
	char name[] = {'z','n','2','\0'};
	char name2[] = {'z','n','2','\0'};
	char name3[] = {'b','g','z','o','n','a','\0'};
	
	htex1 = ResourceManager::GetTexture2D( name );

	htex2 = ResourceManager::GetTexture2D( name2 );
	
	htex3 = ResourceManager::GetTexture2D( name3 );
	
	bigZoneSpt = htex1;
	
	ZoneSpt1 = htex2;
	
	ZoneSpt2 = htex3;

#if DEBUG
	NSLog(@"imagens fazendeira carregadas");
#endif	
}

void Fazendeiro::RemoveAllImages( void ){
//temporário!
	RefCounter< Texture2D > deleter( NULL );
	bigZoneSpt = deleter;
	ZoneSpt2 = deleter;
	ZoneSpt1 = deleter;
	
}

Texture2DHandler Fazendeiro::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
	}

}
	
Texture2DHandler Fazendeiro::getBigZoneSprt( void ){

	return bigZoneSpt;

}


//
//Sprite* Fazendeiro::getZoneSprt( void ){
////	return ZoneSpt;
//}
//
// Sprite* Fazendeiro::getBigZoneSprt( void ){
////	return ZoneSpt;
//}


Sprite* Fazendeiro::getLineHPic( void ){}	

Sprite* Fazendeiro::getLineVPic( void ){}
