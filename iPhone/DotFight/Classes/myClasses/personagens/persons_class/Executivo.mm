/*
 *  Executivo.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Executivo.h"
#include "Sprite.h"
#include "Random.h"

//Sprite* Executivo::bigZoneSpt = NULL;
//
//Sprite* Executivo::ZoneSpt1= NULL;
//
//Sprite* Executivo::ZoneSpt2= NULL;

//Sprite* Executivo::LineV = NULL;
//
//Sprite* Executivo::LineH = NULL;	

Texture2DHandler Executivo::bigZoneSpt1 = NULL;

Texture2DHandler Executivo::bigZoneSpt2 = NULL;

Texture2DHandler Executivo::ZoneSpt1= NULL;

Texture2DHandler Executivo::ZoneSpt2= NULL;

Texture2DHandler Executivo::ZoneSpt3= NULL;

Sprite* Executivo::getLineHPic( void ){}	

Sprite* Executivo::getLineVPic( void ){}

Executivo::Executivo():GenericDoll( DOLL_ID_EXECUTIVE ){}


void Executivo::LoadAllImages( void ){
	Texture2DHandler htex1, htex2, htex3, htex4, htex5;
	char name[] = {'e','x','e','2','2','_','1','\0'};
	char name2[] = {'e','x','e','1','1','_','1','\0'};
	
	//	char* name = "dev22_1";
	//	char* name2 = "dev11_1";
	
	htex1 = ResourceManager::GetTexture2D( name );
	
	name[ 6 ] = '2';
	htex2 = ResourceManager::GetTexture2D( name );
	
	htex3 = ResourceManager::GetTexture2D( name2 );
	
	name2[ 6 ] = '2';
	htex4 = ResourceManager::GetTexture2D( name2 );
	
	name2[ 6 ] = '3';
	htex5 = ResourceManager::GetTexture2D( name2 );
	
	bigZoneSpt1 = htex1;
	
	bigZoneSpt2 = htex2;
	
	ZoneSpt1 = htex3;
	
	ZoneSpt2 = htex4;
	
	ZoneSpt3 = htex5;
	
#if DEBUG
	NSLog(@"imagens executivo carregadas");
#endif
}

void Executivo::RemoveAllImages( void ){
	//temporário!
	RefCounter< Texture2D > deleter( NULL );
	bigZoneSpt1 = deleter;
	bigZoneSpt2 = deleter;
	ZoneSpt2 = deleter;
	ZoneSpt1 = deleter;
	ZoneSpt3 = deleter;	
}


Texture2DHandler Executivo::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
			
		case 2:
			return ZoneSpt3;
			break;
	}
	

}
	
Texture2DHandler Executivo::getBigZoneSprt( void ){

	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
}


//
//Sprite* Executivo::getZoneSprt( void ){
////	return ZoneSpt;
//}
//
// Sprite* Executivo::getBigZoneSprt( void ){
////	return ZoneSpt;
//}
