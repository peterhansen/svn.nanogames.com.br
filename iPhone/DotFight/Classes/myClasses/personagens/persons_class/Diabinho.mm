/*
 *  diabinho.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Diabinho.h"

#include"Sprite.h"
#include "Random.h"

//Sprite* Anjinho::bigZoneSpt = NULL;
//
//Sprite* Anjinho::ZoneSpt1= NULL;
//
//Sprite* Anjinho::ZoneSpt2= NULL;


Texture2DHandler Diabinho::bigZoneSpt1 = NULL;

Texture2DHandler Diabinho::bigZoneSpt2 = NULL;

Texture2DHandler Diabinho::ZoneSpt1= NULL;

Texture2DHandler Diabinho::ZoneSpt2= NULL;

Texture2DHandler Diabinho::ZoneSpt3= NULL;

Sprite* Diabinho::getLineHPic( void ){}	

Sprite* Diabinho::getLineVPic( void ){}


Diabinho::Diabinho():GenericDoll( DOLL_ID_DEVIL ){}


void Diabinho::LoadAllImages( void ){
	Texture2DHandler htex1, htex2, htex3, htex4, htex5;
	char name[] = {'d','e','v','2','2','_','1','\0'};
	char name2[] = {'d','e','v','1','1','_','1','\0'};

//	char* name = "dev22_1";
//	char* name2 = "dev11_1";
	
	htex1 = ResourceManager::GetTexture2D( name );
	
	name[ 6 ] = '2';
	htex2 = ResourceManager::GetTexture2D( name );
	
	htex3 = ResourceManager::GetTexture2D( name2 );
	
	name2[ 6 ] = '2';
	htex4 = ResourceManager::GetTexture2D( name2 );
	
	name2[ 6 ] = '3';
	htex5 = ResourceManager::GetTexture2D( name2 );
	
	bigZoneSpt1 = htex1;

	bigZoneSpt2 = htex2;
	
	ZoneSpt1 = htex3;
	
	ZoneSpt2 = htex4;

	ZoneSpt3 = htex5;
	
#if DEBUG
	NSLog(@"imagens diabo carregadas");
#endif
}

void Diabinho::RemoveAllImages( void ){
	//temporário!
	RefCounter< Texture2D > deleter( NULL );
	bigZoneSpt1 = deleter;
	bigZoneSpt2 = deleter;
	ZoneSpt2 = deleter;
	ZoneSpt1 = deleter;
	ZoneSpt3 = deleter;	
}

Texture2DHandler Diabinho::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
			
		case 2:
			return ZoneSpt3;
			break;
	}
	
}

Texture2DHandler Diabinho::getBigZoneSprt( void ){
	
	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
}


//
//Sprite* Diabinho::getZoneSprt( void ){
////	return ZoneSpt;
//}
//
// Sprite* Diabinho::getBigZoneSprt( void ){
////	return ZoneSpt;
//}