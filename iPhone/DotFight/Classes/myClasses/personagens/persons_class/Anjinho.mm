/*
 *  Anjinho.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Anjinho.h"
#include"Sprite.h"
#include "Random.h"

//Sprite* Anjinho::bigZoneSpt = NULL;
//
//Sprite* Anjinho::ZoneSpt1= NULL;
//
//Sprite* Anjinho::ZoneSpt2= NULL;


Texture2DHandler Anjinho::bigZoneSpt = NULL;

Texture2DHandler Anjinho::ZoneSpt1= NULL;

Texture2DHandler Anjinho::ZoneSpt2= NULL;

Sprite* Anjinho::LineV = NULL;

Sprite* Anjinho::LineH = NULL;	


Sprite* Anjinho::getLineHPic( void ){}	

Sprite* Anjinho::getLineVPic( void ){}

Anjinho::Anjinho():GenericDoll( DOLL_ID_ANGEL ){}


void Anjinho::LoadAllImages( void ){

	Texture2DHandler htex1, htex2, htex3;
	char name[] = {'z','n','2','\0'};
	char name2[] = {'z','n','2','\0'};
	char name3[] = {'b','g','z','o','n','a','\0'};
	if ( !htex1 )
	htex1 = ResourceManager::GetTexture2D( name );

	htex2 = ResourceManager::GetTexture2D( name2 );
	
	htex3 = ResourceManager::GetTexture2D( name3 );
if ( !bigZoneSpt )	
	bigZoneSpt = htex1;
	
	ZoneSpt1 = htex2;
	
	ZoneSpt2 = htex3;
#if DEBUG
	NSLog(@"imagens anjo carregadas");
#endif	
}

void Anjinho::RemoveAllImages( void ){
//temporário!
	RefCounter< Texture2D > deleter( NULL );
	bigZoneSpt = deleter;
	ZoneSpt2 = deleter;
	ZoneSpt1 = deleter;
	
	
}


Texture2DHandler Anjinho::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
	}

}
	
Texture2DHandler Anjinho::getBigZoneSprt( void ){

	return bigZoneSpt;

}


//
//Sprite* Anjinho::getZoneSprt( void ){
////	return ZoneSpt;
//}
//
// Sprite* Anjinho::getBigZoneSprt( void ){
////	return ZoneSpt;
//}

