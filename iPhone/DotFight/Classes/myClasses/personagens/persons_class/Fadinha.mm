/*
 *  Fadinha.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Fadinha.h"
#include"Sprite.h"
#include "Random.h"


//Sprite* Fadinha::bigZoneSpt = NULL;
//
//Sprite* Fadinha::ZoneSpt1= NULL;
//
//Sprite* Fadinha::ZoneSpt2= NULL;


Texture2DHandler Fadinha::bigZoneSpt1 = NULL;

Texture2DHandler Fadinha::bigZoneSpt2 = NULL;

Texture2DHandler Fadinha::ZoneSpt1 = NULL;

Texture2DHandler Fadinha::ZoneSpt2 = NULL;

Texture2DHandler Fadinha::ZoneSpt3 = NULL ;

Sprite* Fadinha::LineV = NULL;

Sprite* Fadinha::LineH = NULL;	


Fadinha::Fadinha():GenericDoll( DOLL_ID_FAIRY ){}


void Fadinha::LoadAllImages( void ){
	
	Texture2DHandler htex1, htex2, htex3, htex4, htex5;
	
	char name[] = {'f','a','y','2','2','_','1','\0'};
	char name2[] = {'f','a','y','1','1','_','1','\0'};
	
	htex1 = ResourceManager::GetTexture2D( name );

	name[ 6 ] = '2';

	htex2 = ResourceManager::GetTexture2D( name );
	
	
	htex3 = ResourceManager::GetTexture2D( name2 );
	
	name[ 6 ] = '2';

	htex4 = ResourceManager::GetTexture2D( name2 );

	name[ 6 ] = '3';

	htex3 = ResourceManager::GetTexture2D( name2 );
	
	bigZoneSpt1 = htex1;
	
	bigZoneSpt2 = htex2;
	
	ZoneSpt1 = htex3;
	
	ZoneSpt2 = htex4;
	
	ZoneSpt3 = htex5;
#if DEBUG
	NSLog(@"imagens fada carregadas");
#endif
}

void Fadinha::RemoveAllImages( void ){
#if DEBUG
	NSLog(@"removendo imagens=> fada");
#endif
//temporário!
	RefCounter< Texture2D > deleter( NULL );
	bigZoneSpt1 = deleter;	
	bigZoneSpt2 = deleter;
	ZoneSpt2 = deleter;
	ZoneSpt1 = deleter;
	ZoneSpt3 = deleter;
}
Sprite* Fadinha::getLineHPic( void ){}	

Sprite* Fadinha::getLineVPic( void ){}


Texture2DHandler Fadinha::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
		case 2:
			return ZoneSpt3;
			break;
	}
	
}

Texture2DHandler Fadinha::getBigZoneSprt( void ){
		switch (Random::GetInt( 0 , 1 )) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
}


//
//Sprite* Fadinha::getZoneSprt( void ){
////	return ZoneSpt;
//}
//
// Sprite* Fadinha::getBigZoneSprt( void ){
////	return ZoneSpt;
//}
