/*
 *  Ogro.h
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef OGRE_H
#define OGRE_H 1

#include "GenericDoll.h"

/*
 cada classe de um boneco vai ter como dados as animações de zona normal e gde, assim como a imagem do rosto do boneco de modo estático. 
 
 */


class Sprite;

class Ogro : public GenericDoll {

public:
	
	Ogro();
	
	~Ogro(){
	
#if DEBUG
		NSLog(@"deletando ogro");
		
#endif
	};
	
	static void LoadAllImages( void );
	
	static void RemoveAllImages( void );
	
	//	virtual Sprite* getZoneSprt( void ) ;
	//	
	//	virtual	Sprite* getBigZoneSprt( void ) ;
	
	virtual Texture2DHandler getZoneSprt( void ) ;
	
	virtual	Texture2DHandler getBigZoneSprt( void );
	
	Sprite* getLineHPic( void );	

	Sprite* getLineVPic( void );	
	
	
protected:

	//
	//	static Sprite* bigZoneSpt;
	//
	//	static Sprite* ZoneSpt1;
	//	
	//	static Sprite* ZoneSpt2;
	
	static Texture2DHandler bigZoneSpt;
	
	static Texture2DHandler ZoneSpt1;
	
	static Texture2DHandler ZoneSpt2;
	
	static Sprite* LineV;
	
	static Sprite* LineH;	
	
};

#endif