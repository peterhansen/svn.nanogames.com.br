/*
 *  DGMainMenu.h
 *  dotGame
 *
 *  Created by Max on 12/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DG_MAIN_MENU
#define DG_MAIN_MENU 1


//#include"UpdatableView.h"
#include"GameBaseInfo.h"
//#include"GameBasicView.h"
#import "GameBasicView.h"

@interface DGMainMenu  : GameBasicView// GameBasicView */UpdatableView//UIView
{
@private
	
	IBOutlet UIButton* hPlay;
	IBOutlet UIButton* hOption;
	IBOutlet UIButton* hHelp;
	IBOutlet UIButton* hConfig;
	IBOutlet UIButton* hOnline;
	GameBaseInfo *ginfo;
	float timeCounter;
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void ) onBeforeTransition :( GameBaseInfo * )ginf;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

-( void )update:( float)timeElapsed;

@end




#endif