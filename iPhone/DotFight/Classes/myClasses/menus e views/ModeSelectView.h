/*
 *  ModeSelectView.h
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef MODE_SELECT_VIEW
#define MODE_SELECT_VIEW 1

#include "UpdatableView.h"
#include "GameBaseInfo.h"

enum ModeSelectViewMode {
	
	MODE_SELECT_VIEW_LOGGED,
	MODE_SELECT_VIEW_NOT_LOGGED
	
} typedef ModeSelectViewMode;

@interface  ModeSelectView : UpdatableView
{
	IBOutlet UIButton* hSolo;
	IBOutlet UIButton* hVersusPlayer;
	IBOutlet UIButton* hOk;
	IBOutlet UIButton* hBack;
	IBOutlet UIButton* hOnLine;
	IBOutlet UILabel* hres;
	IBOutlet UITextView* hTxt;
	GameBaseInfo* GBinfo;
	ModeSelectViewMode mode;
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo;


@end


#endif