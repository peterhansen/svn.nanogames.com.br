/*
 *  DGConfigView.h
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DG_CONFIG_VIEW 
#define DG_CONFIG_VIEW 1

#include "UpdatableView.h"
#include "GameBaseInfo.h"
#include <vector>
using namespace std;

@interface DGConfigView : UpdatableView
{
	IBOutlet UISlider* hHsize;
	IBOutlet UISlider* hVsize;
	IBOutlet UISlider* hLevel;
	IBOutlet UIButton* hOk ;
	IBOutlet UIButton* hBack ;
	IBOutlet UIButton* hFakeImg	;
	IBOutlet UILabel* hLevelInfo;
	IBOutlet UILabel* hHsizeInfo;
	IBOutlet UILabel* hVsizeInfo;
	IBOutlet UIImageView* hBgPreview;
	GameBaseInfo* GBinfo;
	vector<UIImage*>backgrounds;
	vector<idBackground>lista;
	uint8 indice;
//vector<UIImage*>backgrounds;
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo;

//método para dizer que o level, tamanho  horizontal e vertical foram mudados 
- ( IBAction )onSldPressed:( id )hSlider;

@end


#endif