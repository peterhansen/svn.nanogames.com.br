/*
 *  DGOptions.mm
 *  dotGame
 *
 *  Created by Max on 12/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "DGOptions.h"
#include "Config.h"
#include "Macros.h"
#include"ObjcMacros.h"

#include "FreeKickAppDelegate.h"

@implementation DGOptions


-( void ) awakeFromNib{
	[super awakeFromNib];
}



- ( IBAction ) onBtPressed:( id )hButton{
	if( hButton == hBack ){
		[ [ ApplicationManager GetInstance ] performTransitionToView: previousScreen ];
	}else if ( hButton == hVbBoxOn || hButton == hVbBoxOff ) {

		[ self setVibrationStatus:( hButton == hVbBoxOn ) ];
		
	}


}


- ( IBAction ) onSldPressed:( id )hSld{
	if ( hSld == hVolum ) {
		
		[( ( FreeKickAppDelegate* )APP_DELEGATE ) setVolume: [hVolum value]];	
		NSLog(@" volume %1.2f ",[hVolum value]);
	}
	
}

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition{


	[ hVolum setValue: [ ( FreeKickAppDelegate* )APP_DELEGATE getAudioVolume ]];
	
}


-( void )setVibrationStatus:( bool )VibrationOn{
	if( [ ( ( FreeKickAppDelegate* ) APP_DELEGATE ) hasVibration ] ){
	
		//colocar o código para demonstrar qual modo está ativado.
		
		[ ( ( FreeKickAppDelegate* ) APP_DELEGATE ) setVibrationStatus: VibrationOn];
		if( VibrationOn ){
			NSLog(@"vibraçao ligada");
		}else {
			NSLog(@"vibraçao desligada");		
		}

	}

}


-( void )setPreviousIndex:( int8 )prevIndex{

	previousScreen = prevIndex;
}

@end
