/*
 *  InfoBar.h
 *  dotGame
 *
 *  Created by Max on 12/1/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef INFO_BAR_H
#define INFO_BAR_H 1

#include "ObjectGroup.h"
#include "InterfaceControl.h"
#include "GameBaseInfo.h"
#include "TouchZone.h"
#include <vector>
using namespace std;

class RenderableImage;
class InterfaceControlListener;
//class Chronometer;
//class ChronometerListener;
class DotGame;

class InfoBar : public ObjectGroup, public InterfaceControl {
	
public:
	
	InfoBar( const GameBaseInfo &gInfo, InterfaceControlListener* pListener );	
	
	void updateScores( DotGame *dg  );
	
	void updatePowers( DotGame *dg );
	
	void configure( DotGame *dg );

	//verifica se há colisão no quadrado dos botões	
	bool checkCollision( const Point3f * p, DotGame* dg , bool active);
	
	virtual bool render( void );
	
	void onButtonPressed( uint8 i, DotGame* dg );
	
	// Atualiza o objeto
	virtual bool update( float timeElapsed );
	
	
	
private:
	friend class DotGame;
	
	void buildInfoBar( void );
	void clear();
	GameBaseInfo ginfo;
	
	//qte de botoes que existem na barra de informações
	uint8 nButtons;
	//vetor com as máscaras de collisão
	std::vector<TouchZone> maskCollisionVector;
};



#endif