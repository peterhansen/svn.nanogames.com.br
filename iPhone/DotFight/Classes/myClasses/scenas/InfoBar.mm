/*
 *  InfoBar.mm
 *  dotGame
 *
 *  Created by Max on 12/1/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "SpecialBar.h"

#include "InfoBar.h"
#include"ObjcMacros.h"

#include"GameBaseInfo.h"
#include"RenderableImage.h"
#include"InterfaceControlListener.h"
#include"Chronometer.h"
#include"ChronometerListener.h"
#include"DotGame.h"
#include"Label.h"
#include "FreeKickAppDelegate.h"


#include "Exceptions.h"
#include "Label.h"
#include "Quad.h"
#include "Sphere.h"
#include "RenderableImage.h"
#include "Utils.h"

#define INFO_BAR_BT_COLOR_PRESSED Color( static_cast< uint8 >( 127 ), 127, 127, 255 )
#define INFO_BAR_BT_COLOR_UNPRESSED Color( static_cast< uint8 >( 255 ), 255, 255, 255 )

// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
if( !pObject || !pGroup->insertObject( pObject ) )	\
{													\
delete pObject;									\
/*goto Error;*/										\
}


#define INFO_BAR_FONT_CHAR_SPACEMENT 1.0f

#define NAME_LABEL1_POSITION_X 80.0f
#define NAME_LABEL1_POSITION_Y 30.0f
#define SCORE_LABEL1_POSITION_X 80.0f
#define SCORE_LABEL1_POSITION_Y 20.f
#define FACE1_POSITION_X 20.0f
#define FACE1_POSITION_Y 0.0f

#define NAME_LABEL2_POSITION_X 380.0f
#define NAME_LABEL2_POSITION_Y 30.0f
#define SCORE_LABEL2_POSITION_X 380.0f
#define SCORE_LABEL2_POSITION_Y 20.0f

#define FACE2_POSITION_X 440.0f
#define FACE2_POSITION_Y 0.0f

#define FACE1_WIDTH 35.0f
#define FACE1_HEIGHT 35.0f
#define FACE2_WIDTH 35.0f
#define FACE2_HEIGHT 35.0f

#define LABELS_HEIGHT 22.0f
#define LABELS_WIDTH 45.0f

#define LABEL_SPECIAL_LEVEL_P1_POSITION_X 50
#define LABEL_SPECIAL_LEVEL_P1_POSITION_Y ( SCREEN_HEIGHT - 50 )


#define LABEL_SPECIAL_LEVEL_P2_POSITION_X ( SCREEN_WIDTH - ( LABELS_WIDTH + 50 )) 
#define LABEL_SPECIAL_LEVEL_P2_POSITION_Y ( SCREEN_HEIGHT - 50 )


#define BUTTON_POWER_UP_P1_INDICE 0
#define BUTTON_POWER_UP_P2_INDICE ( BUTTON_POWER_UP_P1_INDICE + 1 )
#define BUTTON_PAUSE_GAME ( BUTTON_POWER_UP_P2_INDICE + 1 )
#define LABEL_SPECIAL_P1_INDICE (  BUTTON_PAUSE_GAME + 1)
#define LABEL_SPECIAL_P2_INDICE ( LABEL_SPECIAL_P1_INDICE + 1 )
#define BAR_SPECIAL_P1_INDICE ( LABEL_SPECIAL_P2_INDICE + 1 )
#define BAR_SPECIAL_P2_INDICE ( BAR_SPECIAL_P1_INDICE + 1 )
#define LABEL_SCORE_P1_INDICE ( LABEL_SPECIAL_P2_INDICE + 1 )
#define LABEL_SCORE_P2_INDICE ( LABEL_SCORE_P1_INDICE + 1 )
#define LABEL_NAME_P1_INDICE ( LABEL_SCORE_P2_INDICE + 1 )
#define LABEL_NAME_P2_INDICE ( LABEL_NAME_P1_INDICE + 1 )

#define INFO_BAR_N_OBJECTS 14

InfoBar::InfoBar( const GameBaseInfo &gInfo, InterfaceControlListener* pListener ):ObjectGroup( INFO_BAR_N_OBJECTS ), InterfaceControl( pListener ),ginfo( gInfo ){
	buildInfoBar();
//	ginfo = gInfo;
}

void InfoBar::buildInfoBar( void ){
	//os botões serão criados primeiro e inseridos no grupo primeiro, visando ter assim uma melhor performance na hora de verificar se algum botão foi pressionado, mas depois a sua ordem Z será diferente, para poder ser renderizado na frente dos outros elementos!!
	
	RenderableImage* Pu1 = new RenderableImage();
	Texture2DHandler htexture3 = ResourceManager::GetTexture2D( "life" );
	Pu1->setImage( htexture3 );
	Pu1->setSize( 16, 16  );
	Pu1->setPosition( FACE1_POSITION_X-8.0, FACE1_POSITION_Y+28 );
	insertObject( Pu1 );
	TouchZone p1 = TouchZone( Pu1->getPosition()->x,Pu1->getPosition()->y,Pu1->getSize()->x,Pu1->getSize()->y );
	maskCollisionVector.push_back( p1 );
	
	RenderableImage* Pu2 = new RenderableImage();
	Texture2DHandler htexture4 = ResourceManager::GetTexture2D( "life" );
	Pu2->setVertexSetColor( COLOR_GREEN );
	Pu2->setImage( htexture4 );
	Pu2->setSize( 16, 16  );
	Pu2->setPosition( FACE2_POSITION_X - 8.0, FACE2_POSITION_Y + 28 );
	insertObject( Pu2 );
	TouchZone p2 = TouchZone( Pu2->getPosition()->x,Pu2->getPosition()->y,Pu2->getSize()->x,Pu2->getSize()->y );
	maskCollisionVector.push_back( p2 );
	
	
	RenderableImage* Ppause = new RenderableImage();
	Texture2DHandler htexture5 = ResourceManager::GetTexture2D( "Bpower" );
	Ppause->setImage( htexture5 );
	Ppause->setSize( 32, 32  );
	Ppause->setPosition( SCREEN_WIDTH - 64, SCREEN_HEIGHT - 32 );
	insertObject( Ppause );
	TouchZone p3 = TouchZone( Ppause->getPosition()->x,Ppause->getPosition()->y,Ppause->getSize()->x,Ppause->getSize()->y );
	maskCollisionVector.push_back( p3 );	
	
	
	Font* pInfoBarFont = [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_MAIN];
	
	if(!pInfoBarFont)
		NSLog(@"fonte não carregada!!");
	
	Label* pLbLevel1 = new Label( pInfoBarFont , false );
	
	if( !insertObject( pLbLevel1 ) )
		NSLog(@"falha na budega!!");
	pLbLevel1->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM );
	pLbLevel1->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbLevel1->setPosition( LABEL_SPECIAL_LEVEL_P1_POSITION_X,LABEL_SPECIAL_LEVEL_P1_POSITION_Y ,1.0f  );
	pLbLevel1->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
	pLbLevel1->setText("0",false, false);
	TouchZone p5 = TouchZone(pLbLevel1->getPosition()->x,pLbLevel1->getPosition()->y,pLbLevel1->getSize()->x,pLbLevel1->getSize()->y );
	maskCollisionVector.push_back( p5 );
	
	
	Label* pLbLevel2 = new Label( pInfoBarFont , false );
	if( !insertObject( pLbLevel2 ) )
		NSLog(@"falha na budega!!");
	pLbLevel2->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM );
	pLbLevel2->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbLevel2->setPosition( LABEL_SPECIAL_LEVEL_P2_POSITION_X,LABEL_SPECIAL_LEVEL_P2_POSITION_Y ,1.0f  );
	pLbLevel2->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
	pLbLevel2->setText("0",false, false);
	TouchZone p4 = TouchZone( pLbLevel2->getPosition()->x,pLbLevel2->getPosition()->y,pLbLevel2->getSize()->x,pLbLevel2->getSize()->y );
	maskCollisionVector.push_back( p4 );
	
	SpecialBar* p1SB = new SpecialBar();
	p1SB->setPosition( LABEL_SPECIAL_LEVEL_P1_POSITION_X + 10.0, LABEL_SPECIAL_LEVEL_P1_POSITION_Y + 10.0  );
	//insertObject( p1SB );
	p1SB->setColorBarBgd( COLOR_PURPLE );
	p1SB->updateBar( .1 );
	p1SB->setActive(false);
	p1SB->setVisible(false);
	TouchZone p6 = TouchZone( p1SB->getPosition()->x,p1SB->getPosition()->y,p1SB->getSize()->x,p1SB->getSize()->y );
	//maskCollisionVector.push_back( p6 );
	
//barra com o rosto do mané 01
	Color c = COLOR_PURPLE;
	c.setFloatA( 0.65f );
	TextureFrame f( 0, 0, 236 ,77,0,0 );
	RenderableImage* Face01 = new RenderableImage("visor");
	Face01->setImageFrame( f );
	Face01->setVertexSetColor( c );
	Face01->setPosition( 0.0, 0.0);
	insertObject( Face01 );
	setObjectZOrder(Face01, 0 );
	
//barra com o rosto do mané 02
	RenderableImage* Face02 = new RenderableImage( Face01 );
	Face02->mirror( MIRROR_HOR );
	Face02->setImageFrame( f );
	Face02->setVertexSetColor( c );
	Face02->setPosition( HALF_SCREEN_WIDTH /*-  0.0*/, 0.0);
	insertObject( Face02 );	
	setObjectZOrder(Face02, 1 );
	
	Label* pLbScoreP1 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbScoreP1 ))
		NSLog(@"label fail!!");
	pLbScoreP1->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
	pLbScoreP1->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbScoreP1->setPosition( SCORE_LABEL1_POSITION_X, SCORE_LABEL1_POSITION_Y ,1.0f  );
	pLbScoreP1->setSize( LABELS_WIDTH, LABELS_HEIGHT );
	pLbScoreP1->setText("0",false, false);

	Label* pLbScoreP2 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbScoreP2 ))
		NSLog(@"label fail!!");
	pLbScoreP2->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
	pLbScoreP2->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbScoreP2->setPosition( SCORE_LABEL2_POSITION_X, SCORE_LABEL2_POSITION_Y,1.0f  );
	pLbScoreP2->setSize( LABELS_WIDTH, LABELS_HEIGHT );	
	pLbScoreP2->setText("0",false, false);
	

	Label* pLbNameP1 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbNameP1 ))
		NSLog(@"label fail!!");
	pLbNameP1->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM);
	pLbNameP1->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbNameP1->setPosition( NAME_LABEL1_POSITION_X, NAME_LABEL1_POSITION_Y ,1.0f  );
	pLbNameP1->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
	pLbNameP1->setText("111",false, false);
	
	Label* pLbNameP2 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbNameP2 ))
		NSLog(@"label fail!!");
	pLbNameP2->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM );
	pLbNameP2->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbNameP2->setPosition( NAME_LABEL2_POSITION_X, NAME_LABEL2_POSITION_Y ,1.0f  );
	pLbNameP2->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
	pLbNameP2->setText("666",false, false);
	
	//colocar aqui depois recebendo as imagens de cada um dos jogadores...
	RenderableImage*r = new RenderableImage();
	Texture2DHandler htexture = ResourceManager::GetTexture2D( "tmpface" );
	r->setImage( htexture );
	r->setSize( FACE1_WIDTH, FACE1_HEIGHT  );
	r->setPosition( FACE1_POSITION_X, FACE1_POSITION_Y );
	insertObject( r );
	
	RenderableImage*r2 = new RenderableImage();
	Texture2DHandler htexture2 = ResourceManager::GetTexture2D( "tmpface2" );
	r2->setImage( htexture2 );
	r2->setSize( FACE2_WIDTH, FACE2_HEIGHT );
	r2->setPosition( FACE2_POSITION_X, FACE2_POSITION_Y );
	insertObject( r2 );
	
}

void InfoBar::clear(){
	maskCollisionVector.clear();

}

// Atualiza o objeto
bool InfoBar::update( float timeElapsed ){
	if( !ObjectGroup::update( timeElapsed ) )
		return false;
	
	return true;
}

/*==================================================================

 updateScores-> atualiza a pontuação
 
==================================================================*/
#define MAX_BUFFER_SCORE_LEN 3

void InfoBar::updateScores( DotGame *dg ){
	
	char buffer[ MAX_BUFFER_SCORE_LEN ];
	snprintf( buffer, MAX_BUFFER_SCORE_LEN, "%d",dg->players[ 0 ]->getScore() );
	static_cast< Label* >( getObject( 0 ) )->setText( buffer, false, false );
	
	char buffer2[ MAX_BUFFER_SCORE_LEN ];
	snprintf( buffer2, MAX_BUFFER_SCORE_LEN, "%d",dg->players[ 1 ]->getScore() );
	static_cast< Label* >( getObject( 1 ) )->setText( buffer2, false, false );
}
#undef MAX_BUFFER_SCORE_LEN
/*==================================================================
 
configure-> puxa dados do jogo, como nome, boneco, etc.
 
 ==================================================================*/
#define MAX_BUFFER_NAME_LEN 6
void InfoBar::configure( DotGame *dg ){

	char buffer[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer , MAX_BUFFER_NAME_LEN , "666" /*"dg->GInfo.nomes[0].c_str()" */);
	
	static_cast< Label* >( getObject( LABEL_NAME_P1_INDICE ) )->setText( buffer , false, false );
	
	char buffer2[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer2, MAX_BUFFER_NAME_LEN, "666"/*"dg->GInfo.nomes[1].c_str()" */);
	static_cast< Label* >( getObject( LABEL_NAME_P2_INDICE ) )->setText( buffer2, false, false );
	
}

#undef MAX_BUFFER_NAME_LEN
/*==================================================================
 
render->renderiza tudo e mais um pouco (dããããã)
 
 ==================================================================*/

bool InfoBar::render( void ){
	if( !isVisible() )
		return false;
	/*
//por enquanto...
	
//
//	glMatrixMode( GL_MODELVIEW );
//	glPushMatrix();
//	
//	glTranslatef(position.x, position.y, position.z);
//
//	
//	glColor4ub( 255, 0, 255, 255);
//	
//	Quad q1;
//	
//	q1.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 10.0f, 0.0f, 1.0f );
//	q1.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( SCREEN_WIDTH-10, 0.0f, 1.0f );
//	q1.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 10.0f, LABELS_WIDTH, 1.0f );
//	q1.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( SCREEN_WIDTH-10, LABELS_WIDTH, 1.0f );
//	
//	q1.render();
//	
//	
//	glColor4ub( 0, 0, 0, 255);
//	Quad q2;
//	
//	q2.quadVertexes[ QUAD_TOP_LEFT ].setPosition( HALF_SCREEN_WIDTH - 15.0, 0.0f, 1.0f );
//	q2.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( HALF_SCREEN_WIDTH + 22.0, 0.0f, 1.0f );
//	q2.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( HALF_SCREEN_WIDTH - 15.0, 45.0, 1.0f );
//	q2.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( HALF_SCREEN_WIDTH + 22.0f, 45.0, 1.0f );
//	
//	q2.render();
//	
#if DEBUG
	
//	glColor4ub( 255, 255, 255, 255);
//	
//	Quad q;
//	Point3f p,s;
//	p.set( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT);//FACE2_POSITION_X-8.0, FACE2_POSITION_Y+28 );
//	s.set( 16, 16  );
//	for( uint8 i = 0; i < maskCollisionVector.size() ;i++ ){
////		p = maskCollisionVector[ i ].getPosition();
////		s = maskCollisionVector[ i ].getSize();
//		
//		q.quadVertexes[ QUAD_TOP_LEFT ].setPosition( p.x , p.y + s.y, 1.0f );
//		q.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( p.x + s.x, p.y + s.y, 1.0f );
//		q.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( p.x, p.y, 1.0f );
//		q.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( p.x + s.x, p.y, 1.0f );
//		q.render();
//		p.x+=1;
//	}
//	
#endif	
	//glPopMatrix();
	 */
	return 	ObjectGroup::render();
}

void InfoBar::updatePowers( DotGame *dg ){
//	dg->currPlayer->pPower->

}

bool InfoBar::checkCollision( const Point3f * p, DotGame* dg , bool active){
//verificação de colisão com os botões aqui!!
	if( !active  /*|| ( dg->getDotGameState() == DOT_GAME_STATE_OBSERVING  )*/)
		return false; 
	RenderableImage *tmpR = NULL ;
	Label *tmpL = NULL;
	bool ret = false;
	bool isLabel;
	
	for ( uint8 i = 0 ; i < maskCollisionVector.size() ; i++) {
		if( i ==  LABEL_SPECIAL_P1_INDICE || i ==  LABEL_SPECIAL_P2_INDICE ){
			isLabel = true;
		tmpL = static_cast< Label *> ( getObject( i ) );}
		else {
			isLabel = false;
			tmpR = static_cast< RenderableImage *> ( getObject( i ) );
		}
		if( maskCollisionVector[i].verifyCollision( p ) && active ){
			if( isLabel ){
				if( tmpL->isVisible() ){
					ret = true;	
					tmpL->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					onButtonPressed( i, dg );
				}
			}else {
				if( tmpR->isVisible() ){
					ret = true;	
					tmpR->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					onButtonPressed( i, dg );
				}
			}
			
		}else {
			if( isLabel ){
				tmpL->setVertexSetColor( INFO_BAR_BT_COLOR_UNPRESSED );
			}else {
				tmpR->setVertexSetColor( INFO_BAR_BT_COLOR_UNPRESSED );
			}
		}
	}	
	return ret;
	
}

void InfoBar::onButtonPressed( uint8 i, DotGame* dg  ){
//identificar o botão pressionado de acordo com o índice e colocar aqui
	
#if DEBUG
	NSLog(@"botão da infoBar indice %d pressionado",i);
#endif
	
	Player *p1,*p2,*currP;
	currP = dg->currPlayer;
	p1 = dg->getPlayerByIndice( 0 );
	p2 = dg->getPlayerByIndice( 1 );
	switch ( i ) {
		case BUTTON_POWER_UP_P1_INDICE :{
			if( currP->getId() == p1->getId()  ){
				p1->doPowerUp( dg );
#if DEBUG
				NSLog(@"player 1 usando power up!");
#endif
			}
		}break;
		case LABEL_SPECIAL_P1_INDICE:{
			if( currP->getId() == p1->getId()  ){
				//p1->doPowerUp( dg );
#if DEBUG
				NSLog(@"player 1 usando especial!");
#endif
			}	
		}break;
		case BUTTON_POWER_UP_P2_INDICE :{
			if( currP->getId() == p2->getId()  ){
				p2->doPowerUp( dg );
#if DEBUG
				NSLog(@"player 2 usando power up!");
#endif
			}			
		}break;

		case LABEL_SPECIAL_P2_INDICE:{
			if( currP->getId() == p2->getId()  ){
#if DEBUG
				NSLog(@"player 2 usando especial!");
#endif
			}	
		}break;
		case BUTTON_PAUSE_GAME:{
#if DEBUG
			NSLog(@"botão de pause pressionado");
#endif
			dg->pause( !dg->paused );
		}break;
	}
}

