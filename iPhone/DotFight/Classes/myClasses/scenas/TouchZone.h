/*
 *  TouchZone.h
 *  dotGame
 *
 *  Created by Max on 3/22/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef TOUCH_ZONE_H
#define TOUCH_ZONE_H 1

//#include "Object.h"
#include "Point3f.h"
struct TouchZone {//: public Object {
	
public:
	TouchZone( float x,float y,float h,float w ){
		position.set( x, y );
		size.set( w, h );
	}
void setPosition( Point3f *p ){
		position.x = p->x;
	position.y = p->y;
	};
	
void setSize( Point3f *s ){
	size.y = size.y;
	size.x = size.x;
};
	Point3f getPosition( void ){ return position;};
	Point3f getSize( void ){ return size; };
	bool verifyCollision( const Point3f *p );
protected:
	Point3f position, size;
//	virtual bool render( void );
};



#endif