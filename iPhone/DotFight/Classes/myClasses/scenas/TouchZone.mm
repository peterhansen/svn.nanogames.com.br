/*
 *  TouchZone.mm
 *  dotGame
 *
 *  Created by Max on 3/22/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "TouchZone.h"
#include "Utils.h"
#include "Line.h"

bool TouchZone::verifyCollision( const Point3f *p ){

	return Utils::IsPointInsideRect( p, position.x, position.y, /*position.x +*/ size.x, /*position.y*/ + size.y );

}


//
//bool TouchZone::render( void ){
//
//if( !isVisible() )
//	return false;
//	
//	glMatrixMode( GL_MODELVIEW );
//	glPushMatrix();
//	glTranslatef( position.x, position.y, position.z );
//	gl
//
//}
