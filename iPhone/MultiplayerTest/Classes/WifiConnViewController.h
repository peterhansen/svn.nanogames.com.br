/*
 *  WifiConnViewController.h
 *  GKTank
 *
 *  Created by Daniel Lopes Alves on 3/8/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef WIFI_CONN_VIEW_CONTROLLER_H
#define WIFI_CONN_VIEW_CONTROLLER_H

// iPhoneOS API
#import <GameKit/GameKit.h>

// Components
#include "NanoTypes.h"
#include "UpdatableView.h"

// Forward Declarations
@class GKSession;
@class WifiConnViewController;

// Protocolo que declara as callbacks chamadas por WifiConnView
// A implementação do protocolo NSObject permite que chamemos respondsToSelector em objetos do tipo WifiConnViewDelegate sem
// que o compilador dispare o warning "respondsToSelector: not found in protocol(s)" quando testamos a implementação de métodos
// opcionais
@protocol WifiConnViewDelegate< NSObject >

	@required
		// Indica que a view precisa de uma sessão para iniciar a conexão. Caso o usuário
		// tenha passado uma sessão válida para o construtor, não será necessário
		// implementar este método
		-( GKSession* )wifiConnViewControllerNeedsGKSessionForMode:( GKSessionMode )sessionMode;

		// Indica que a conexão foi estabelecida com sucesso
		-( void )wifiConnViewControllerDidSucceed:( WifiConnViewController* )hWifiConnViewController;

		// Indica que o usuário apertou o botão de "Cancelar", cancelando a exibição da view
		-( void )wifiConnViewControllerDidCancel:( WifiConnViewController* )hWifiConnViewController;

		// Indica que conseguimos conectar com um determinado peer em uma determina sessão
		-( void )wifiConnViewController:( WifiConnViewController* )hWifiConnViewController didConnectPeer:( NSString* )peerID toSession:( GKSession* )session;

		// Indica que aconteceu um erro do qual não conseguiremos nos recuperar
		-( void )wifiConnViewController:( WifiConnViewController* )hWifiConnViewController didFailWithError:( NSString* )hError;

@end

// Forward Declarations
@class WifiConnView;

// Declaração da Classe
@interface WifiConnViewController : NSObject< UIAlertViewDelegate, GKSessionDelegate, UITableViewDataSource, UITableViewDelegate >
{
	@private
		// View controlada por este objeto
		WifiConnView *hView;
		
		// Objeto que irá receber os eventos do controlador
		id< WifiConnViewDelegate > delegate;
		
		// Lista dos nomes dos peers disponíveis
		NSMutableArray *hPeersIDList;

		// Armazena o real delegate da GKSession utilizada. Afinal, iremos
		// substituí-lo por nós mesmos enquanto estivermos procurando os
		// peers
		id< GKSessionDelegate > oldGKSessionDelegate;
		
		// Linha da tabela que está atualmente selecionada
		NSIndexPath *hCurrSelectedRow;
				
		// Peer ao qual desejamos conectar
		NSString *hWishedPeerId;

		// Timeout da conexão wifi criada
		float connectionTimeout;
	
		// Sessão multiplayer que deveremos configurar
		GKSession* hGKSession;
	
		// Controla o timeout das operações
		NSTimer *hNoPeersTimer;
}

@property( readwrite, nonatomic, assign ) id< WifiConnViewDelegate > delegate;
@property( readwrite, nonatomic, assign ) float connectionTimeout;

// Exibe a view e bloqueia a interação do usuário com as views pais
-( void )show;

// Esconde esta view e desbloqueia a interação do usuário com as views pais
-( void )dismiss;

@end


#endif
