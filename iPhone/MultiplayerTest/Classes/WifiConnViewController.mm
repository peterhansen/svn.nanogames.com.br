#include "WifiConnViewController.h"

// C++
#include <string>

// Components
#include "DeviceInterface.h"
#include "ObjcMacros.h"
#include "Utils.h"

// Ajuda
// http://starterstep.wordpress.com/2009/03/24/custom-uialertview-with-uitableview/
// http://starterstep.wordpress.com/2009/09/16/custom-uialertview-with-tableview-part-2/
// http://starterstep.files.wordpress.com/2009/04/top.png
// http://starterstep.files.wordpress.com/2009/04/bottom.png

// Tamanho do buffer inicial utilizado para conter os nomes dos peers disponíveis para uma conexão WiFi
#define DEFAULT_PEERS_LIST_LEN 5

// Identificações das subviews contidas nas células da tabela (um inteiro qualquer)
#define PEER_TABLE_CELL_BKG_TAG		322
#define PEER_TABLE_CELL_LABEL_TAG	545

// Altura mínima de cada linha da tabela
#define PEER_TABLE_DEFAULT_ROW_HEIGHT 38.0f

// Offset do label da célula para as bordas horizontais da tabela
#define PEER_TABLE_CELL_EXTRA_LABEL_OFFSET 10.0f

// Distância da tabela para as bordas da view pai
#define PEER_TABLE_DIST_FROM_BORDER 5.0f

// Identificador do modelo de célula da tabela
#define kPeerTableCellID @"peerCell"

// Tempo de timeout (em segundos) devido à conexão inativa
#define DEFAULT_CONNECTION_TIMEOUT 300.0f

// Largura padrão (e inalterável) de uma UIAlertView
#define DEFAULT_UIALERTVIEW_WIDTH 284.0f

// Valores importantes de uma UIAlertView
#define UIALERTVIEW_MSG_LABEL_X			 11.0f
#define UIALERTVIEW_MSG_LABEL_Y			 50.0f
#define UIALERTVIEW_MSG_LABEL_WIDTH		261.0f

// Altura da view em diferentes estados. Precisamos deste valor antes de a view ser exibida (enquanto a uialertview ainda não foi exibida, seu tamanho é 0, pois ela faz uma
// animação de aparecer na tela), por isto utilizamos estes defines ao invés de acessarmos a propriedade 'frame' ou a propriedade 'bounds'
#define WIFICONNVIEW_STATE_SEARCHING_PEERS_HEIGHT	 65.0f
#define WIFICONNVIEW_STATE_SHOWING_PEERS_HEIGHT		207.0f

// Declara um índice de botão que significará "transição de estados". Isto é necessário já que precisamos esconder a UIAlertView toda vez que mudamos seus elementos visuais, pois só
// assim ela conseguirá se reorganizar quando utilizarmos o próximo método 'show'.
#define WIFICONNVIEW_AUTO_DISMISS_BUTTON_INDEX 999

// Botões disponíveis nos diversos estados
#define WIFICONNVIEW_STATE_SHOWING_PEERS_CONNECT_BT	0
#define WIFICONNVIEW_STATE_SHOWING_PEERS_CANCEL_BT	1

#define WIFICONNVIEW_STATE_ASKING_SESSION_CLIENT_BT	0
#define WIFICONNVIEW_STATE_ASKING_SESSION_SERVER_BT	1

#pragma mark WifiConnView

// Estados da view
typedef enum
{
	kStateUndefined = 0,
	kStateCreated,
	kStateAskingSessionMode,
	kStateSearchingPeers,
	kStateShowingPeers,				// Somente para clientes
	kStateNoPeers,					// Somente para clientes
	kStateConnectingToPeer,
	kStateConnectionSuccessful,
	kStateNoSessionAvailable,
	kStateSessionError

} WifiConnViewState;

// Declaração da classe
@interface WifiConnView : UIAlertView
{
	@private
		// Elementos da interface
		UITableView *hPeersTable;
		UIActivityIndicatorView *hActivityIndicator;
		UIImageView *hTableTopBorder, *hTableBottomBorder;
	
		// Modo da sessão multiplayer
		GKSession *hGKSession;
	
		// Peer com o qual estamos tentando estabelecer a conexão
		NSString *hPeerIdToConnect;
	
		// Estado da view
		WifiConnViewState viewState;
}

@property( readonly, nonatomic, assign ) UITableView *hPeersTable;
@property( readwrite, nonatomic, assign ) WifiConnViewState viewState;
@property( readwrite, nonatomic, assign, setter = setSession, getter = session ) GKSession *hGKSession;
@property( readwrite, nonatomic, assign, setter = setPeerToConnect, getter = peerToConnect ) NSString *hPeerIdToConnect;

// Inicializa o objeto
-( bool )buildWifiConnView;

// Libera a memória alocada pelo objeto
-( void )cleanWifiConnView;

// Determina o estado da view
-( void )setViewState:( WifiConnViewState )newState;

// Esconde todos os elementos que acrescentamos à UIAlertView
-( void )hideAllExtraUIViews;

@end

// Início da implementação da classe
@implementation WifiConnView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hPeersTable, viewState, hGKSession, hPeerIdToConnect;

/*==============================================================================================

MENSAGEM init
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )init
{
	if( ( self = [super init] ) )
	{
		// Inicializa o objeto
		if( ![self buildWifiConnView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return nil;
}

/*==============================================================================================

MENSAGEM buildWifiConnView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildWifiConnView
{
	// Inicializa as variáveis da classe que não serão criadas via um .xib
	viewState = kStateUndefined;
	
	{ // Evita erros de compilação por causa dos gotos

		// Tabela de Peers
		CGSize parentSize = [self frame].size;
		hPeersTable = [[UITableView alloc] initWithFrame: CGRectMake( PEER_TABLE_DIST_FROM_BORDER, PEER_TABLE_DIST_FROM_BORDER, parentSize.width - PEER_TABLE_DIST_FROM_BORDER, parentSize.height - PEER_TABLE_DIST_FROM_BORDER ) style: UITableViewStylePlain];
		if( !hPeersTable )
			goto Error;
		
		[self addSubview: hPeersTable];
		[hPeersTable release];

		[hPeersTable setAllowsSelectionDuringEditing: NO];
		[hPeersTable setHidden: YES];
		
		// Bordas da tabela
		char buffer[ PATH_MAX ];
		NSString* hFilePath = CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "top", "png" ) );
		hTableTopBorder = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile: hFilePath]];
		if( !hTableTopBorder )
			goto Error;

		[self addSubview: hTableTopBorder];
		[hTableTopBorder release];
		[hTableTopBorder setHidden: YES];

		hFilePath = CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bottom", "png" ) );
		hTableBottomBorder = [[UIImageView alloc] initWithImage: [UIImage imageWithContentsOfFile: hFilePath]];
		if( !hTableBottomBorder )
			goto Error;
		
		[self addSubview: hTableBottomBorder];
		[hTableBottomBorder release];
		[hTableBottomBorder setHidden: YES];

		// Activity Indicator
		hActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
		if( !hActivityIndicator )
			goto Error;
		
		[self addSubview: hActivityIndicator];
		[hActivityIndicator release];
		[hActivityIndicator setHidden: YES];
		
		// Tudo OK
		[self setViewState: kStateCreated];

		return true;
		
	} // Evita erros de compilação por causa dos gotos

	Error:
		[self cleanWifiConnView];
		return false;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self cleanWifiConnView];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM cleanWifiConnView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanWifiConnView
{
	[self setViewState: kStateUndefined];
}

/*==============================================================================================

MENSAGEM setViewState:
	Determina o estado da view.

===============================================================================================*/

-( void )setViewState:( WifiConnViewState )newState
{
	switch( newState )
	{
		case kStateUndefined:
		case kStateCreated:
		case kStateSessionError:
		case kStateConnectionSuccessful:
			[self hideAllExtraUIViews];
			break;

		case kStateAskingSessionMode:
			[self hideAllExtraUIViews];

			[self setTitle: @"Select your Role"];
			[self addButtonWithTitle: @"Client"];
			[self addButtonWithTitle: @"Server"];
			
			// TODOO: Fazer os dois botões acima personalizados, assim como na UIPlayerPickerView
			//[self addButtonWithTitle: @"Cancel"];
			[self setCancelButtonIndex: -1];
			break;

		case kStateSearchingPeers:
			{
				[hPeersTable setHidden: YES];
				[hTableTopBorder setHidden: YES];
				[hTableBottomBorder setHidden: YES];
				
				if( [hGKSession sessionMode] == GKSessionModeClient )
					[self setTitle: @"Searching for Peers"];
				else
					[self setTitle: @"Waiting for Connections"];
				
				[self setMessage: @"\n\n\n"];
				[self addButtonWithTitle: @"Cancel"];
				[self setCancelButtonIndex: 0];

				[hActivityIndicator setHidden: NO];
				[hActivityIndicator setCenter: CGPointMake( DEFAULT_UIALERTVIEW_WIDTH * 0.5f, UIALERTVIEW_MSG_LABEL_Y + ( WIFICONNVIEW_STATE_SEARCHING_PEERS_HEIGHT * 0.5f ))];
				[hActivityIndicator startAnimating];			
			}
			break;
			
		case kStateShowingPeers:
			{
				[hActivityIndicator stopAnimating];
				[hActivityIndicator setHidden: YES];

				[self setTitle: @"Select a Peer"];
				[self setMessage: @"\n\n\n\n\n\n\n\n\n\n"];
				[self addButtonWithTitle: @"Connect"];
				[self addButtonWithTitle: @"Cancel"];
				[self setCancelButtonIndex: -1];

				[hPeersTable setFrame: CGRectMake( UIALERTVIEW_MSG_LABEL_X, UIALERTVIEW_MSG_LABEL_Y, UIALERTVIEW_MSG_LABEL_WIDTH, WIFICONNVIEW_STATE_SHOWING_PEERS_HEIGHT )];
				
				CGSize topBorderSize = [hTableTopBorder frame].size;
				[hTableTopBorder setFrame: CGRectMake( UIALERTVIEW_MSG_LABEL_X, UIALERTVIEW_MSG_LABEL_Y, topBorderSize.width, topBorderSize.height )];
				
				CGSize bottomBorderSize = [hTableBottomBorder frame].size;
				[hTableBottomBorder setFrame: CGRectMake( UIALERTVIEW_MSG_LABEL_X, UIALERTVIEW_MSG_LABEL_Y + WIFICONNVIEW_STATE_SHOWING_PEERS_HEIGHT - bottomBorderSize.height, bottomBorderSize.width, bottomBorderSize.height )];

				[hPeersTable setHidden: NO];
				[hTableTopBorder setHidden: NO];
				[hTableBottomBorder setHidden: NO];
			}
			break;
		
		case kStateNoPeers:
			[self hideAllExtraUIViews];
			
			[self setTitle: @"No peers available for connection"];
			[self addButtonWithTitle: @"Ok"];
			break;

		case kStateConnectingToPeer:
			[self hideAllExtraUIViews];

			[self setTitle: [NSString stringWithFormat: @"Connecting to peer %@", [hGKSession displayNameForPeer: hPeerIdToConnect]]];
			[self addButtonWithTitle: @"Cancel"];
			break;

		case kStateNoSessionAvailable:
			[self hideAllExtraUIViews];;
			
			[self setTitle: @"Coulnd't establish an online session"];
			[self addButtonWithTitle: @"Ok"];
			break;
	}
	
	viewState = newState;
}

/*==============================================================================================

MENSAGEM hideAllExtraUIViews
	Esconde todos os elementos que acrescentamos à UIAlertView.

===============================================================================================*/

-( void )hideAllExtraUIViews
{
	[hPeersTable setHidden: YES];
	[hTableTopBorder setHidden: YES];
	[hTableBottomBorder setHidden: YES];

	[hActivityIndicator stopAnimating];
	[hActivityIndicator setHidden: YES];
}

// Fim da implementação da classe
@end




#pragma mark WifiConnViewController

// Extensão da classe para declarar métodos "privados"
@interface WifiConnViewController( Private )

// Atualiza a lista de peers disponíveis para conexão
-( void )updatePeersList;

// Conecta o cliente a um determinado servidor
-( void )connectToPeer:( int32 )peerTableIndex;

// Métodos auxiliares na criação das células da tabela
-( UITableViewCell* )createTableCell;
-( UIColor* )getTxtColorForCell:( UITableViewCell* )hCell AtRow:( NSIndexPath* )hIndexPath;

// Inicializa o objeto
-( bool )buildWifiConnViewController;

// Libera a memória alocada pelo objeto
-( void )cleanWifiConnViewController;

// Recria a UIAlertView. Isto só é necessário porque não podemos retirar botões de uma UIAlertView
// depois de acrescentá-los
-( bool )resetAlertView;

// Modifica o estado da view controlada
-( void )changeWifiViewStateTo:( WifiConnViewState )newState;

// Retorna o controle para o chamador através de callbacks que indicam o resultado da operação
-( void )onEnd;

// Não conseguiu achar ou manter conexão com outros peers
-( void )searchingPeersTimeout;

@end

// Início da implementação da classe
@implementation WifiConnViewController

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize delegate, connectionTimeout;

/*==============================================================================================

MENSAGEM initWithGKSession:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )init
{
	if( ( self = [super init] ) )
	{
		// Inicializa o objeto
		if( ![self buildWifiConnViewController] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return nil;
}

/*==============================================================================================

MENSAGEM buildWifiConnViewController
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildWifiConnViewController
{
	// Inicializa as variáveis da classe que não serão criadas via um .xib
	delegate = nil;
	oldGKSessionDelegate = nil;
	hWishedPeerId = nil;
	connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
	hNoPeersTimer = nil;
	hGKSession = nil;

	{ // Evita erros de compilação por causa dos gotos

		hPeersIDList = [[NSMutableArray alloc] initWithCapacity: DEFAULT_PEERS_LIST_LEN];
		if( !hPeersIDList )
			goto Error;

		return [self resetAlertView];
		
	} // Evita erros de compilação por causa dos gotos

	Error:
		[self cleanWifiConnViewController];
		return false;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self cleanWifiConnViewController];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM cleanWifiConnViewController
	Libera a memória alocada pelo objeto.

==============================================================================================*/

-( void )cleanWifiConnViewController
{
	if( hGKSession )
		hGKSession.delegate = oldGKSessionDelegate;
	
	[hPeersIDList release];
	[hView release];
	
	if( hNoPeersTimer )
		[hNoPeersTimer invalidate];
}

/*==============================================================================================

MENSAGEM show
	Exibe a view controlada e bloqueia a interação do usuário com as views pais.

====================================================================================================*/

-( void )show
{
	if( !delegate )
	{
		#if DEBUG
			LOG( "WifiConnViewController::show called with no delegate set. Operation cancelled\n" );
		#endif
		return;
	}
	
	[self changeWifiViewStateTo: kStateAskingSessionMode];
}

/*==============================================================================================

MENSAGEM resetAlertView
	Recria a UIAlertView. Isto só é necessário porque não podemos retirar botões de uma UIAlertView
depois de acrescentá-los.

===============================================================================================*/

-( bool )resetAlertView
{
	if( hView )
	{
		[hView dismissWithClickedButtonIndex: WIFICONNVIEW_AUTO_DISMISS_BUTTON_INDEX animated: NO];
		[hView release];
	}
	
	hView = [[WifiConnView alloc] init];
	if( !hView )
		return false;

	[hView setDelegate: self];
	[hView setSession: hGKSession];
	[hView setPeerToConnect: hWishedPeerId];
	[[hView hPeersTable] setDelegate: self];
	[[hView hPeersTable] setDataSource: self];
	
	return true;
}

/*==============================================================================================

MENSAGEM changeWifiViewStateTo:
	Modifica o estado da view controlada.

===============================================================================================*/

-( void )changeWifiViewStateTo:( WifiConnViewState )newState
{
	if( newState == hView.viewState )
		return;

	// Para exibir os primeiros estados, não precisaremos recriar a view. Isto acontece porque ela ainda não terá sido
	// exibida, sem contar que não podemos voltar para estes estados. No estado kStateConnectionSuccessful não iremos mostrar
	// mais nada, então não recria a view
	if( ( newState != kStateCreated ) && ( newState != kStateUndefined ) && ( newState != kStateAskingSessionMode ) && ( newState != kStateConnectionSuccessful ) )
	{
		if( ![self resetAlertView] )
		{
			[delegate wifiConnViewController: self didFailWithError: @"Could not create WifiConnViewController view" ];
			return;
		}
	}

	[hView setViewState: newState];

	// Se aconteceu um erro de sessão, deixaremos que o delegate informe o erro para o usuário. Por isso destruímos a view,
	// a recriamos, mas não a exibimos
	// No estado kStateConnectionSuccessful não exibiremos mais nada, o feedback ficará a cargo do cliente
	if( ( newState != kStateSessionError ) && ( newState != kStateConnectionSuccessful ) )
		[hView show];
}

/*==============================================================================================

MENSAGEM dismiss
	Esconde a view controlada e desbloqueia a interação do usuário com as views pais.

===============================================================================================*/

-( void )dismiss
{
	[hView dismissWithClickedButtonIndex: WIFICONNVIEW_AUTO_DISMISS_BUTTON_INDEX animated: NO];
}

/*==============================================================================================

MENSAGEM updatePeersList
	Atualiza a lista de peers disponíveis para conexão.

===============================================================================================*/

-( void )updatePeersList
{
	if( hGKSession )
	{
		NSArray *hAvailablePeers = [hGKSession peersWithConnectionState: GKPeerStateAvailable];
		if( hAvailablePeers )
		{
			// Esvazia uma lista criada previamente
			[hPeersIDList removeAllObjects];

			// Preenche a nova lista
			NSUInteger nPeers = [hAvailablePeers count];
			for( NSUInteger i = 0 ; i < nPeers ; ++i )
			{
				// TODOO : Retirar!!!
				//#if TARGET_IPHONE_SIMULATOR
					LOG( "Peer %d - %s\n", i+1, NSSTRING_TO_CHAR_ARRAY( [hGKSession displayNameForPeer: [hAvailablePeers objectAtIndex: i]] ) );
				//#endif

				[hPeersIDList addObject: [hAvailablePeers objectAtIndex: i]];
			}
			
			// TODOO : Retirar!!!
			//#if TARGET_IPHONE_SIMULATOR
				LOG( "\n\n" );
			//#endif
			
			// Organiza a tabela
			[[hView hPeersTable] reloadData];
		}
	}
}

/*==============================================================================================

MENSAGEM connectToPeer:
	Conecta o cliente a um determinado servidor.

===============================================================================================*/

-( void )connectToPeer:( int32 )peerTableIndex
{
	hWishedPeerId = [hPeersIDList objectAtIndex: peerTableIndex];
	[hView setPeerToConnect: hWishedPeerId];	
	
	[hGKSession connectToPeer: hWishedPeerId withTimeout: connectionTimeout];
	
	[self changeWifiViewStateTo: kStateConnectingToPeer];
}

/*==============================================================================================

MENSAGEM onEnd
	Retorna o controle para o chamador através de callbacks que indicam o resultado da operação.

===============================================================================================*/

-( void )onEnd
{
	// Passa o controle para o chamador
	hGKSession.delegate = oldGKSessionDelegate;
	
	if( [hView viewState] == kStateConnectionSuccessful )
	{
		[delegate wifiConnViewControllerDidSucceed: self];
	}
	else
	{
		[self dismiss];
		[delegate wifiConnViewControllerDidCancel: self];
	}
}

/*==============================================================================================

MENSAGEM searchingPeersTimeout
	Não conseguiu achar ou manter conexão com outros peers.

===============================================================================================*/

-( void )searchingPeersTimeout
{
	[self changeWifiViewStateTo: kStateNoPeers];
}

/*==============================================================================================

PROTOCOL UIAlertViewDelegate

==============================================================================================*/

#pragma mark UIAlertViewDelegate Methods

-( void )alertView:( UIAlertView* )alertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	switch( [hView viewState] )
	{
		case kStateAskingSessionMode:
			// Pede uma sessão para o delegate
			hGKSession = [delegate wifiConnViewControllerNeedsGKSessionForMode: buttonIndex == WIFICONNVIEW_STATE_ASKING_SESSION_SERVER_BT ? GKSessionModeServer : GKSessionModeClient];
			if( hGKSession )
			{
				oldGKSessionDelegate = hGKSession.delegate;
				hGKSession.delegate = self;

				// Começa a procurar os peers
				[hView setSession: hGKSession];
				[hGKSession setAvailable: YES];
				
				[self changeWifiViewStateTo: kStateSearchingPeers];
			}
			else
			{
				[self changeWifiViewStateTo: kStateNoSessionAvailable];
			}
			break;

		case kStateShowingPeers:
			if( buttonIndex == WIFICONNVIEW_STATE_SHOWING_PEERS_CONNECT_BT )
				[self connectToPeer: [hCurrSelectedRow row]];
			else
				[self onEnd];
			break;
			
		case kStateConnectingToPeer:
			[self changeWifiViewStateTo: kStateShowingPeers];
			break;
			
		case kStateNoPeers:
			[self onEnd];
			break;
	}
}

-( void )alertViewCancel:( UIAlertView* )alertView
{
	// TODOO : Quando esse método é chamado???
}

/*==============================================================================================

PROTOCOLO UITableViewDelegate

===============================================================================================*/

#pragma mark UITableViewDelegate Methods

-( CGFloat )tableView:( UITableView* )hTableView heightForRowAtIndexPath:( NSIndexPath* )indexPath
{
	return PEER_TABLE_DEFAULT_ROW_HEIGHT;
}

-( void )tableView:( UITableView* )hTableView didSelectRowAtIndexPath:( NSIndexPath* )hIndexPath
{
	hCurrSelectedRow = hIndexPath;

	UITableViewCell* hCell = [hTableView cellForRowAtIndexPath: hIndexPath];
	if( [hCell selectionStyle] != UITableViewCellSelectionStyleNone )
	{
		UILabel *hLabel = ( UILabel* )[hCell viewWithTag: PEER_TABLE_CELL_LABEL_TAG];
		[hLabel setTextColor: [self getTxtColorForCell: hCell AtRow: hIndexPath]];
	}
}

-( void )tableView:( UITableView* )hTableView didDeselectRowAtIndexPath:( NSIndexPath* )hIndexPath
{
	hCurrSelectedRow = nil;

	UITableViewCell* hCell = [hTableView cellForRowAtIndexPath: hIndexPath];
	if( [hCell selectionStyle] != UITableViewCellSelectionStyleNone )
	{
		UILabel *hLabel = ( UILabel* )[hCell viewWithTag: PEER_TABLE_CELL_LABEL_TAG];
		[hLabel setTextColor: [self getTxtColorForCell: hCell AtRow: hIndexPath]];
	}
}

/*==============================================================================================

PROTOCOLO UITableViewDataSource

===============================================================================================*/

#pragma mark UITableViewDataSource Methods

-( UITableViewCell* )tableView:( UITableView* )hTableView cellForRowAtIndexPath:( NSIndexPath* )hIndexPath
{
	UITableViewCell *hCell = [hTableView dequeueReusableCellWithIdentifier: @"kPeerTableCellID"];
	if( hCell == nil )
	{
		hCell = [self createTableCell];
		if( !hCell )
			return nil;
	}

	// Trata células especiais
//	UIFont *hFont;
//	UIColor *hColorBkg;
//
//	int32 index = [hIndexPath row];
//	switch( index )
//	{
//		case 0:
//			hFont = [UIFont fontWithName: @"Courier-BoldOblique" size: 14.0f];
//			hColorBkg = [UIColor colorWithRed: 246.0f / 255.0f green: 219.0f / 255.0f blue: 35.0f / 255.0f alpha: 1.0f];
//			break;
//			
//		case 1:
//			hFont = [UIFont fontWithName: @"Courier-BoldOblique" size: 14.0f];
//			hColorBkg = [UIColor colorWithRed: 226.0f / 255.0f green: 227.0f / 255.0f blue: 228.0f / 255.0f alpha: 1.0f];
//			break;
//			
//		default:
//			// ...
//			break;
//	}
	
	UIFont *hFont = [UIFont fontWithName: @"Courier" size: 14.0f];
	UIColor *hColorBkg = [UIColor colorWithRed: 226.0f / 255.0f green: 227.0f / 255.0f blue: 228.0f / 255.0f alpha: 1.0f];
	
	if( ( hFont == nil ) || ( hColorBkg == nil ) )
		return nil;
	
	[[hCell viewWithTag: PEER_TABLE_CELL_BKG_TAG] setBackgroundColor: hColorBkg];

	// OLD : O label que já vem em uma UITableViewCell não é completamente configurável.
	// Por isso criamos e configuramos um label extra
	//UILabel *hLabel = [hCell textLabel];
	
	UILabel *hLabel = ( UILabel* )[hCell viewWithTag: PEER_TABLE_CELL_LABEL_TAG];
	[hLabel setFont: hFont];
	[hLabel setMinimumFontSize: 10.0f];
	[hLabel setAdjustsFontSizeToFitWidth: YES];
	[hLabel setText: [hGKSession displayNameForPeer: [hPeersIDList objectAtIndex: [hIndexPath row]]]];
	[hLabel setTextColor: [self getTxtColorForCell: hCell AtRow: hIndexPath]];
	
	return hCell; 
}

-( NSInteger )tableView:( UITableView* )tableView numberOfRowsInSection:( NSInteger )section
{
	return [hPeersIDList count];
}

-( NSInteger )numberOfSectionsInTableView:( UITableView* )tableView
{
	return 1;
}

#pragma mark Peer Table Aux Methods

/*==============================================================================================

MENSAGEM createTableCell
	Cria uma célula da tabela do NanoOnline.

===============================================================================================*/

-( UITableViewCell* )createTableCell
{
	UITableViewCell *hCell = [[[UITableViewCell alloc] initWithFrame: CGRectZero reuseIdentifier: kPeerTableCellID] autorelease];
	if( !hCell )
		return nil;

	[hCell setSelectionStyle:  UITableViewCellSelectionStyleBlue];
	
	[[[hCell subviews] objectAtIndex: 0] setTag: PEER_TABLE_CELL_BKG_TAG];
	[[hCell viewWithTag: PEER_TABLE_CELL_BKG_TAG] setBackgroundColor: [UIColor whiteColor]];

	// O label que já vem em uma UITableViewCell não é completamente configurável.
	// Por isso criamos e configuramos um label extra
	UILabel *hExtraLabelView = [[UILabel alloc] initWithFrame: CGRectMake( PEER_TABLE_CELL_EXTRA_LABEL_OFFSET, 0.0f, [hView hPeersTable].frame.size.width - PEER_TABLE_CELL_EXTRA_LABEL_OFFSET, PEER_TABLE_DEFAULT_ROW_HEIGHT )]; 
	if( !hExtraLabelView )
		return nil;
	
	[hCell addSubview: hExtraLabelView]; 
	[hExtraLabelView release]; 

	[hExtraLabelView setTag: PEER_TABLE_CELL_LABEL_TAG];
	[hExtraLabelView setBackgroundColor: [UIColor clearColor]];
	
	// Utiliza o label original para configurar algumas propriedades do label auxiliar
	std::string osVersion;
	DeviceInterface::GetDeviceSystemVersion( osVersion );

	if( Utils::CompareVersions( osVersion, "3.0" ) >= 0 )
	{
		UILabel *hTemplateLabel = [hCell textLabel];
		[hExtraLabelView setShadowColor: [hTemplateLabel shadowColor]];
		[hExtraLabelView setBaselineAdjustment: [hTemplateLabel baselineAdjustment]];
		[hExtraLabelView setTextAlignment: [hTemplateLabel textAlignment]];
		[hExtraLabelView setLineBreakMode: [hTemplateLabel lineBreakMode]];
	}
	else
	{
		[hExtraLabelView setBaselineAdjustment: UIBaselineAdjustmentAlignBaselines];
		[hExtraLabelView setTextAlignment: UITextAlignmentLeft];
		[hExtraLabelView setLineBreakMode: UILineBreakModeTailTruncation];
	}

	return hCell;
}

/*==============================================================================================

MENSAGEM getTxtColorForRow:
	Retorna a cor do texto a ser utilizada em uma determinada linha da tabela.

===============================================================================================*/

-( UIColor* )getTxtColorForCell:( UITableViewCell* )hCell AtRow:( NSIndexPath* )hIndexPath
{
	// Trata células especiais
//	UIColor *hColorText;
//
//	switch( [hIndexPath row] )
//	{
//		case 0:
//		case 1:
//			hColorText = [UIColor whiteColor];
//			break;
//			
//		default:
//			hColorText = [UIColor redColor];
//			break;
//	}
//	return hColorText;
	
	return [UIColor whiteColor];
}

/*==============================================================================================

PROTOCOLO GKSessionDelegate

===============================================================================================*/

#pragma mark GKSessionDelegate Methods

-( void )session:( GKSession* )session peer:( NSString* )peerID didChangeState:( GKPeerConnectionState )state
{
	// Trata o evento da conexão localmente
	if( ( [hView viewState] == kStateSearchingPeers ) || ( [hView viewState] == kStateShowingPeers ) || ( [hView viewState] == kStateConnectingToPeer ) )
	{
		switch( state )
		{
			case GKPeerStateAvailable:
				if( [hView viewState] == kStateSearchingPeers )
					[self changeWifiViewStateTo: kStateShowingPeers];
				
				// Sem break mesmo

			case GKPeerStateUnavailable:
				[self updatePeersList];
				[hNoPeersTimer invalidate];
				hNoPeersTimer = nil;
				
				if( [hPeersIDList count] == 0 )
				{
					hNoPeersTimer = [NSTimer scheduledTimerWithTimeInterval: connectionTimeout target: self selector: @selector( searchingPeersTimeout ) userInfo: nil repeats: NO];
					[self changeWifiViewStateTo: kStateSearchingPeers];
				}
				break;
				
			case GKPeerStateConnecting:
				if( [hGKSession sessionMode] == GKSessionModeServer )
				{
					hWishedPeerId = peerID;
					[hView setPeerToConnect: hWishedPeerId];
				}

				[self changeWifiViewStateTo: kStateConnectingToPeer];
				break;

			case GKPeerStateConnected:
				if( [peerID isEqualToString: hWishedPeerId] )
				{
					[delegate wifiConnViewController: self didConnectPeer: peerID toSession: session];
					[self changeWifiViewStateTo: kStateConnectionSuccessful];
					[self onEnd];
				}
				break;
			
			case GKPeerStateDisconnected:
			default:
				break;
		}
	}
	
	// Repassa o evento
	if( oldGKSessionDelegate && [oldGKSessionDelegate respondsToSelector: @selector( session:peer:didChangeState:) ] )
		[oldGKSessionDelegate session: session peer: peerID didChangeState: state];
}

-( void )session:( GKSession* )session connectionWithPeerFailed:( NSString* )peerID withError:( NSError* )error
{
	// Trata o evento da conexão localmente
	[self changeWifiViewStateTo: kStateSessionError];
	[delegate wifiConnViewController: self didFailWithError: [error localizedDescription]];

	// Repassa o evento
	if( oldGKSessionDelegate && [oldGKSessionDelegate respondsToSelector: @selector( session:connectionWithPeerFailed:withError:) ] )
		[oldGKSessionDelegate session: session connectionWithPeerFailed: peerID withError: error];
}

-( void )session:( GKSession* )session didFailWithError:( NSError* )error
{
	// Trata o evento da conexão localmente
	[self changeWifiViewStateTo: kStateSessionError];
	[delegate wifiConnViewController: self didFailWithError: [error localizedDescription] ];

	// Repassa o evento
	if( oldGKSessionDelegate && [oldGKSessionDelegate respondsToSelector: @selector( session:didFailWithError:) ] )
		[oldGKSessionDelegate session: session didFailWithError: error];
}

-( void )session:( GKSession* )session didReceiveConnectionRequestFromPeer:( NSString* )peerID
{
	// Trata o evento da conexão localmente
	if( true /* TODOO */ )
	{
		[hGKSession acceptConnectionFromPeer: peerID error: nil];
		[delegate wifiConnViewController: self didConnectPeer: peerID toSession: hGKSession];
	}
	else
	{
		[hGKSession denyConnectionFromPeer: peerID];
	}
	
	// Repassa o evento
	if( oldGKSessionDelegate && [oldGKSessionDelegate respondsToSelector: @selector( session:didReceiveConnectionRequestFromPeer:) ] )
		[oldGKSessionDelegate session: session didReceiveConnectionRequestFromPeer: peerID];
}

// Fim da implementação da classe
@end
