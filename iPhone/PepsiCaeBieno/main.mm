//
//  main.m
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

#if DEBUG
	#include "Exceptions.h"
	#include "Macros.h"
#endif

int main( int argc, char* argv[] )
{
	NSAutoreleasePool* hPool = [[NSAutoreleasePool alloc] init];

#if DEBUG
	@try
	{
		try
		{
#endif
			const int retVal = UIApplicationMain( argc, argv, NULL, NULL );
			[hPool release];
			return retVal;

#if DEBUG
		}
		catch( std::exception& ex )
		{
			LOG( CHAR_ARRAY_TO_NSSTRING( ex.what() ) );
			return 1;
		}
	}
	@catch( NSException* hException )
	{
		// TASK : De repente é bom manter este try-catch. Também poderíamos colocar o
		// "[hPool release]" dentro de um finally
		LOG( [hException reason] );
		[hPool release];
		return 1;
	}
#endif
}
