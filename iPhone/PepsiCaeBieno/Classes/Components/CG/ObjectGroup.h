/*
 *  ObjectGroup.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OBJECT_GROUP_H
#define OBJECT_GROUP_H

#include "Object.h"

class ObjectGroup : public Object
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException
		ObjectGroup( uint16 maxObjects );

		// Destrutor
		virtual ~ObjectGroup( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Retorna quantos objetos estão contidos no grupo
		inline uint16 getNObjects( void ) const { return nObjects; };
	
		// Retorna o número de objetos suportado pelo grupo
		inline uint16 getCapacity( void ) const { return maxObjects; };
	
		// Insere um novo objeto no grupo
		bool insertObject( Object* pObject, uint16 *pIndex = NULL );
	
		// Retira um objeto do grupo
		bool removeObject( uint16 index );
	
		// Esvazia o grupo
		void removeAllObjects( void );
	
		// Retorna o objeto contido no grupo no índice passado como parâmetro
		Object* getObject( uint16 index );
	
		// Determina a ordem Z do objeto de índice 'objIndex'
		void setObjectZOrder( uint16 objIndex, uint16 zOrder );
	
		// Retorna a ordem Z do objeto de índice 'objIndex'
		uint16 getObjectZOrder( uint16 objIndex );
	
	protected:
		// Inicializa o objeto
		bool build( void );
	
		// Determina o slot do grupo onde o objeto deve ser inserido
		bool setObject( uint16 index, Object* pObject );

	private:
		#if DEBUG
			// Renderiza um quad em volta da área do objeto
			void renderBorder( void );
		#endif

		// Número máximo de objetos suportado pelo grupo
		uint16 maxObjects;
	
		// Número de objetos contidos no grupo
		uint16 nObjects;
	
		// Objetos que formam o grupo
		Object** pObjects;
	
		// TODOO : Otimizar e generalizar com templates de tabelas de índices !!!
		// Vetor que 'ordena' os objetos pela sua ordem z
		Object*** pObjectsZOrder;
};

#endif
