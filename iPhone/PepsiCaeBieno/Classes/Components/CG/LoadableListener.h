#ifndef LOADABLE_LISTENER_H
#define LOADABLE_LISTENER_H

/*
 *  LoadableListener.h
 *
 *  Created by Daniel Lopes Alves on 9/29/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

/*==============================================================================================
 
 CLASSE LoadableListener
 
 ==============================================================================================*/

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include "NanoTypes.h"

// Tipos de operações suportadas por LoadableListener
typedef enum LoadableOp
{
	LOADABLE_OP_LOAD = 0,
	LOADABLE_OP_UNLOAD
}LoadableOp;

class LoadableListener
{
	public:
		// Método para receber os eventos do objeto loadable
		virtual void loadableHandleEvent( LoadableOp op, int32 loadableId, uint32 data ) = 0;
};

#endif