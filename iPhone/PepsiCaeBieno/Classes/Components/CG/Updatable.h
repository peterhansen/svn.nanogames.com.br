/*
 *  Updatable.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UPDATABLE_H
#define UPDATABLE_H

class Updatable
{
	public:
		// Construtor
		Updatable( bool active = true );

		// Destrutor
		virtual ~Updatable( void ){};
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed ){ return isActive(); };
	
		// Determina se o objeto deve ser atualizado
		virtual void setActive( bool b ){ active = b; };
	
		// Retorna se o objeto está sofrendo atualizações
		inline bool isActive( void ) const { return active; };
	
	private:
		// Indica se o objeto está sofrendo atualizações
		bool active;
};

#endif

