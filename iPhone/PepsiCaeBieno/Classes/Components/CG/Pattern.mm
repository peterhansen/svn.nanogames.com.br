#include "Pattern.h"

#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Pattern::Pattern( Object* pFill ) : Object(), pFill( pFill )
{
}
		
/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Pattern::~Pattern( void )
{
	SAFE_DELETE( pFill );
}

/*==============================================================================================

MÉTODO setFill
	Determina um objeto de preenchimento para o pattern.

==============================================================================================*/

void Pattern::setFill( Object* pPatternFill )
{
	// Apaga o objeto de preenchimento antigo
	SAFE_DELETE( pFill );
	
	// Armazena os parâmetros
	pFill = pPatternFill;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Pattern::render( void )
{
	if( !Object::render() || ( pFill == NULL ))
		return false;

	float xInc = pFill->getWidth() + offsetX;
	float yInc = pFill->getHeight() + offsetY;

	glEnable( GL_SCISSOR_TEST );
	glScissor( position.x, position.y, size.x, size.y );

	for( float y = 0; y < size.y ; y += yInc )
	{
		glPushMatrix();
		glTranslatef( position.x, position.y + y, 0.0f );

		for( float x = 0; x < size.x ; x += xInc )
		{
			pFill->render();
			glTranslatef( xInc, 0.0f, 0.0f );
		}

		glPopMatrix();
	}

	glDisable( GL_SCISSOR_TEST );

	return true;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool Pattern::update( float timeElapsed )
{
	if( !Object::update( timeElapsed ) )
		return false;

	pFill->update( timeElapsed );

	return true;
}

/*==============================================================================================

MÉTODO setOffset
	Determina um offset de desenho entre as repetições do pattern.

==============================================================================================*/

void Pattern::setOffset( float x, float y )
{
	offsetX = x;
	offsetY = y;
}
