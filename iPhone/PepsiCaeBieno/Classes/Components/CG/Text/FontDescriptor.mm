#include "FontDescriptor.h"

#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

FontDescriptor::FontDescriptor( void )
: TextureFrameDescriptor(), spaceWidth( 0 ), fontHeight( 0 ), hCharacterSet( NULL )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

FontDescriptor::~FontDescriptor( void )
{
	SAFE_RELEASE( hCharacterSet );
}

/*==============================================================================================

MÉTODO readFromScanner
	Obtém as informações da textura através do parser de arquivos.

==============================================================================================*/

bool FontDescriptor::readFromScanner( const NSScanner* hScanner )
{
	// Lê a string contendo os caracteres aceitos pela fonte
	[hScanner scanUpToCharactersFromSet: [NSCharacterSet newlineCharacterSet] intoString: &( hCharacterSet )];
	[hCharacterSet retain];

	// Lê a largura original do frame
	[hScanner scanInt:( int* )&( originalFrameWidth )];

	// Lê a altura original do frame
	[hScanner scanInt:( int* )&( originalFrameHeight )];
	
	// Lê a altura da fonte
	// TODOO : O certo é não ter originalFrameSize, e sim apenas fontHeight!!!
	fontHeight = ( uint16 )originalFrameHeight;
	
	// Lê a largura do caractere de espaçamento
	int32 aux;
	[hScanner scanInt:&aux];
	spaceWidth = ( uint8 )aux;

	// Lê o número de frames do sprite
	[hScanner scanInt:&aux];
	nFrames = ( uint16 )aux;

	// Aloca o array de frames
	pFrames = new TextureFrame[ nFrames ];
	if( !pFrames )
		return false;

	// Lê o offset deste frame em relação ao frame original
	for( uint16 framesCounter = 0; framesCounter < nFrames ; ++framesCounter )
	{
		[hScanner scanInt:( int* )&( pFrames[ framesCounter ].x )];
		[hScanner scanInt:( int* )&( pFrames[ framesCounter ].y )];
		[hScanner scanInt:( int* )&( pFrames[ framesCounter ].width )];
		[hScanner scanInt:( int* )&( pFrames[ framesCounter ].height )];
		[hScanner scanInt:( int* )&( pFrames[ framesCounter ].offsetX )];
		[hScanner scanInt:( int* )&( pFrames[ framesCounter ].offsetY )];
		
		// TODOO : Ler se o frame possui ou não transparência
	}
	return true;
}

