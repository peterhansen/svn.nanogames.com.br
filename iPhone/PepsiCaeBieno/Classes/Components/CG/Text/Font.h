/*
 *  Font.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FONT_H
#define FONT_H

#include "NanoRect.h"
#include "Point2i.h"
#include "TextAlignment.h"
#include "Texture2D.h"
#include "Vertex.h"

// TODOO : Transformar as fontes em recursos compartilhados com RefCounter!!!!!!!!!!!!!
class Font
{
	// Objetos da classe abaixo terão acesso a todos os membros da classe Font 
	friend class FontDescriptorReader;
	
	public:
		// Construtores
		// Exceções: InvalidArgumentException
		Font( const char* pFontImgsId, const char* pFontDescriptor, bool monoSpaced = false );
	
		// Destrutor
		virtual ~Font( void );
	
		// Retorna a altura da fonte (ou seja, a altura de seu caractere mais alto)
		float getFontHeight( void ) const;
	
		// Retorna o tamanho do caractere na fonte
		Point2i* getCharSize( Point2i* pOut, unichar c ) const;
	
		// Retorna qual a área necessária para se renderizar o texto
		Point2i* getTextSize( Point2i* pOut, const NSString* hText, float spacement ) const;
	
		// Renderiza o caractere na posição x, y
		void renderCharAt( unichar c, float x, float y ) const;
	
		// Renderiza o texto com o espaçamento de caracteres desejado
		void renderText( const NSString* hText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const;

	private:
		// Inicializa o objeto
		bool build( const char* pFontImgsId, const char* pFontDescriptor );
	
		// Libera os recursos alocados pelo objeto
		void clean( void );
	
		// Retorna o índice do frame correspondente ao caractere
		int32 getCharFrameIndex( unichar c ) const;
	
		// Indica se a fonte é mono-espaçada
		bool monoSpaced;
	
		// Tamanho do caractere de espaçamento da fonte
		uint8 spaceWidth;
	
		// Altura da fonte (ou seja, a altura de seu caractere mais alto)
		uint16 fontHeight;
	
		// Imagem contendo os caracateres da fonte
		Texture2D* pFontImage;
	
		// String contendo os caracteres suportados pela fonte
		NSString* hCharacterSet;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

/*==============================================================================================

MÉTODO getFontHeight
	Retorna a altura da fonte (ou seja, a altura de seu caractere mais alto).

==============================================================================================*/

inline float Font::getFontHeight( void ) const
{
	return static_cast< float >( fontHeight );
}

#endif

