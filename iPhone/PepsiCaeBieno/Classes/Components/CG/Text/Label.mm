#include "Label.h"
#include "Macros.h"
#include "Quad.h"
#include "Utils.h"

// TODOO : Retirar e criar classes NanoApplication e 3DAPIManager
#import "EAGLView.h"
#import "PepsiCaeBienoAppDelegate.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Label::Label( Font* pFont, bool manageFont )
	  : Object(), hCurrText( NULL ), pPreProcessedImg( NULL ), pCurrFont( pFont ), manageFont( manageFont )
{
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Label::Label( const char* pFontImgsId, const char* pFontDescriptor, bool manageFont )
	  : Object(), hCurrText( NULL ), pPreProcessedImg( NULL ),
		pCurrFont( new Font( pFontImgsId, pFontDescriptor ) ), manageFont( manageFont )
{
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Label::~Label( void )
{
	SAFE_RELEASE( hCurrText );
	SAFE_DELETE( pPreProcessedImg );
	
	if( manageFont )
		SAFE_DELETE( pCurrFont );
}

/*==============================================================================================

MÉTODO setFont
	Determina a fonte a ser utilizada na renderização do texto.

==============================================================================================*/

void Label::setFont( Font* pFont, bool manageFont )
{
	pCurrFont = pFont;
	this->manageFont = manageFont;
}

/*==============================================================================================

MÉTODO setText
	Determina o texto do label.

==============================================================================================*/

bool Label::setText( const char* pText, bool resize, bool preProcess )
{
	return setText( CHAR_ARRAY_TO_NSSTRING( pText ), resize, preProcess );
}

/*==============================================================================================

MÉTODO setText
	Determina o texto do label.

==============================================================================================*/

bool Label::setText( const NSString* hText, bool resize, bool preProcess )
{
	// Libera um possível texto anterior
	SAFE_RELEASE( hCurrText );
	
	// Armazena o texto passado como parâmetro
	hCurrText = hText;
	[hCurrText retain];
	
	// Redimensiona o label para comportar o texto
	if( resize )
	{
		Point2i textSize;
		pCurrFont->getTextSize( &textSize, hText, charSpacement );
		setSize( textSize.x, textSize.y, 1.0f );
	}
	
	// Verifica se deve fazer o pré-processamento da renderização do label
	return preProcess ? preProcessText() : true;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Label::render( void )
{
	if( !Object::render() )
		return false;

	if( pPreProcessedImg != NULL )
		renderPreprocessed();
	else if( pCurrFont != NULL )
		renderWithFont();
	else
		return false;

	return true;
}

/*==============================================================================================

MÉTODO preProcessText
	Renderiza o texto para uma textura visando otimizar renderizações futuras.

==============================================================================================*/

bool Label::preProcessText( void )
{
#if USE_OFFSCREEN_BUFFER
	// Libera uma possível memória alocada
	SAFE_DELETE( pPreProcessedImg );
	
	// Cria a textura que irá conter os dados da renderização
	pPreProcessedImg = new Texture2D();
	if( !pPreProcessedImg || !pPreProcessedImg->loadTextureEmpty( Utils::GetNextHighestPowerOf2( size.x ), Utils::GetNextHighestPowerOf2( size.y ) ))
		goto Error;
	
	{ //Evita problemas com o goto

		// Indica que iremos renderizar fora da tela
		EAGLView* p3DAPIManager = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) hGLView];
		[p3DAPIManager setOffscreenRender: true];
		
		Color clearColor( COLOR_TRANSPARENT );
		[p3DAPIManager clearOffscreenBuffer: &clearColor];
		
		// TODO : Colocar esse trecho de código em um lugar melhor
		// Prepara a cena
		glMatrixMode( GL_PROJECTION );
		glPushMatrix();
		glLoadIdentity();
		glOrthof( 0.0f, SCREEN_WIDTH, 0.0f, SCREEN_HEIGHT, -1.0f, 1.0f );
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		glLoadIdentity();

		// Executa a renderização
		NanoRect area( 0.0f, 0.0f, size.x, size.y );
		pCurrFont->renderText( hCurrText, charSpacement, &area, alignment );

		// TODOO : Colocar esse trecho de código em um lugar melhor
		glPopMatrix();
		glMatrixMode( GL_PROJECTION );
		glPopMatrix();
		
		// Obtém apenas o pedaço da imagem que desejamos
		glBindTexture( GL_TEXTURE_2D, pPreProcessedImg->getTextureId() );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
		glGetError();
		glCopyTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, 0, 0, size.x, size.y );
		GLenum err = glGetError();
		glBindTexture( GL_TEXTURE_2D, GL_NONE );
		
		// Coloca o buffer da tela novamente como o alvo de renderização
		[p3DAPIManager setOffscreenRender: false];
		
		if( err != GL_NO_ERROR )
			goto Error;
		
	} //Evita problemas com o goto
	
	return true;
	
	// Label de tratamento de erros
	Error:
		SAFE_DELETE( pPreProcessedImg );
		return false;
#else
	return false;
#endif
}

/*==============================================================================================

MÉTODO renderPreprocessed
	Renderiza a textura contendo o texto.

==============================================================================================*/

void Label::renderPreprocessed( void )
{
	// Executa as transformações sobre os vértices do sprite
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x + ( size.x * 0.5f ), position.y + ( size.y * 0.5f ), position.z );
	glScalef( getWidth(), getHeight(), 1.0f );

	Matrix4x4 rot;
	glMultMatrixf( ( float* )rotation.getTranspose( &rot ) );

	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pPreProcessedImg->load();
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );

	// Renderiza o label
	Quad aux;
	aux.render();
	
	// Reseta os estados do OpenGL
	pPreProcessedImg->unload();
	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );

	// Cancela as transformações do sprite
	glPopMatrix();
}

/*==============================================================================================

MÉTODO renderWithFont
	Renderiza o texto, caractere a caractere, utilizando as informações da fonte.

==============================================================================================*/

void Label::renderWithFont( void )
{
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	// Rotaciona o objeto
	Matrix4x4 aux;
	glMultMatrixf( ( float* )rotation.getTranspose( &aux ) );

	NanoRect area( position.x, position.y, size.x, size.y );
	pCurrFont->renderText( hCurrText, charSpacement, &area, alignment );
	
	glPopMatrix();
}

