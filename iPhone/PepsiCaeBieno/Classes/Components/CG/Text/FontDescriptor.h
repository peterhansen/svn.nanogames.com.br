/*
 *  FontDescriptor.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/28/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FONT_DESCRIPTOR_H
#define FONT_DESCRIPTOR_H

#include "TextureFrameDescriptor.h"

class FontDescriptor : public TextureFrameDescriptor
{
	public:
		// Construtor
		FontDescriptor( void );
	
		// Destrutor
		virtual ~FontDescriptor( void );
	
		// Obtém as informações da textura através do parser de arquivos
		virtual bool readFromScanner( const NSScanner* hScanner );
	
		// Retorna o tamanho do caractere de espaçamento
		inline uint8 getSpaceCharWidth( void ) const { return spaceWidth; };
	
		// Retorna a altura da fonte
		inline uint8 getFontHeight( void ) const { return fontHeight; };
	
		// Retorna uma string contendo os caracteres suportados pela fonte
		inline NSString* getCharSet( void ) const { return hCharacterSet; };

	private:
		// Tamanho do caractere de espaçamento da fonte
		uint8 spaceWidth;
	
		// Altura da fonte (ou seja, a altura de seu caractere mais alto)
		uint16 fontHeight;
	
		// String contendo os caracteres suportados pela fonte
		NSString* hCharacterSet;
};

#endif

