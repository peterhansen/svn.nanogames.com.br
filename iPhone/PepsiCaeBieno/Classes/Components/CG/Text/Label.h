/*
 *  Label.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef LABEL_H
#define LABEL_H 1

#include "Font.h"
#include "Object.h"
#include "Texture2D.h"

// TODOO : Transformar as fontes em recursos compartilhados com RefCounter!!!!!!!!!!!!! Aí
// poderíamos retirar o parâmetro manageFont dos construtores e o atributo correspondente

class Label : public Object
{
	public:
		// Construtores
		Label( Font* pFont = NULL, bool manageFont = true );
	
		// Exceções: InvalidArgumentException
		Label( const char* pFontImgsId, const char* pFontDescriptor, bool manageFont = true );

		// Destrutor
		virtual ~Label( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Determina a fonte a ser utilizada na renderização do texto
		void setFont( Font* pFont, bool manageFont = true );
	
		// Retorna a fonte que está sendo utilizada pelo label
		const Font* getFont( void ) const;

		// Determina o espaçamento entre os caracteres do label
		void setCharSpacement( float spacement );
	
		// Retorna o espaçamento de caracteres que está sendo utilizado pelo label
		float getCharSpacement( void ) const;
	
		// Determina o texto do label
		bool setText( const char* pText, bool resize = true, bool preProcess = false );
		bool setText( const NSString* hText, bool resize = true, bool preProcess = false );
	
		// Determina o alinhamento do texto
		void setTextAlignment( TextAlignment alignment );
	
		// Retorna o alinhamento do texto
		TextAlignment getTextAlignment( void ) const;
	
		// Obtém o texto do label
		const NSString* getText( void ) const;
	
	protected:
		// Renderiza o texto para uma textura visando otimizar renderizações futuras
		bool preProcessText( void );
	
		// Renderiza a textura contendo o texto
		void renderPreprocessed( void );
	
		// Renderiza o texto, caractere a caractere, utilizando as informações da fonte
		void renderWithFont( void );
	
		// Alinhamento do texto
		TextAlignment alignment;
	
		// Buffer contendo o texto do label
		NSString* hCurrText;
	
		// Textura contendo o texto do label já processado
		Texture2D* pPreProcessedImg;
	
		// Fonte utilizada para renderizarmos o texto do label
		Font *pCurrFont;
	
		// Espaçamento entre os caracteres do label
		float charSpacement;
	
		// Indica se o label deve destruir a fonte utilizada em seu construtor
		bool manageFont;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

// Retorna a fonte que está sendo utilizada pelo label
inline const Font* Label::getFont( void ) const
{
	return pCurrFont;
};

// Determina o espaçamento entre os caracteres do label
inline void Label::setCharSpacement( float spacement )
{
	charSpacement = spacement;
};
	
// Retorna o espaçamento de caracteres que está sendo utilizado pelo label
inline float Label::getCharSpacement( void ) const
{
	return charSpacement;
};

// Determina o alinhamento do texto
inline void Label::setTextAlignment( TextAlignment alignment )
{
	this->alignment = alignment;
}
	
// Retorna o alinhamento do texto
inline TextAlignment Label::getTextAlignment( void ) const
{
	return alignment;
}
	
// Obtém o texto do label
inline const NSString* Label::getText( void ) const
{
	return hCurrText;
};

#endif

