#include "VertexSet.h"

// Máscara utilizada para ativarmos todos os estados
#define ALL_STATES_MASK 0x000F

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

VertexSet::VertexSet( void ) : renderStates( RENDER_STATE_VERTEX_ARRAY | RENDER_STATE_TEXCOORD_ARRAY )
{
}

/*===========================================================================================
 
MÉTODO enableRenderStates
	Determina quais estados devemos habilitar para a renderização do objeto.

============================================================================================*/

void VertexSet::enableRenderStates( uint16 states )
{
	renderStates |= ( states & ALL_STATES_MASK );
}

/*===========================================================================================
 
MÉTODO disableRenderStates
	Determina quais estados devemos desabilitar para a renderização do objeto.

============================================================================================*/

void VertexSet::disableRenderStates( uint16 states )
{
	renderStates &= ~( states & ALL_STATES_MASK );
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/
	
bool VertexSet::render( void )
{	
	if( !Renderable::render() || (( renderStates & RENDER_STATE_VERTEX_ARRAY ) == 0 ))
		return false;
	
	float* pAux = ( float* )getVertexes();
	
	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( 3, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_POSITION_START ] );
	
	if( ( renderStates & RENDER_STATE_TEXCOORD_ARRAY ) != 0 )
	{
		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
		glTexCoordPointer( 2, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_TEXCOORD_START ] );
	}
	else
	{
		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	}
	
	if( ( renderStates & RENDER_STATE_COLOR_ARRAY ) != 0 )
	{
		glEnableClientState( GL_COLOR_ARRAY );
		glColorPointer( 4, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_COLOR_START ] );
	}
	else
	{
		glDisableClientState( GL_COLOR_ARRAY );
	}
	
	if( ( renderStates & RENDER_STATE_NORMAL_ARRAY ) != 0 )
	{
		glEnableClientState( GL_NORMAL_ARRAY );
		glNormalPointer( GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_NORMAL_START ] );
	}
	else
	{
		glDisableClientState( GL_NORMAL_ARRAY );
	}

	glDrawArrays( GL_TRIANGLE_STRIP, 0, getNVertexes() );

	return true;
}
