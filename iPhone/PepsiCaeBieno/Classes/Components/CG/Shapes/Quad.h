/*
 *  Quad.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/29/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef QUAD_H
#define QUAD_H

#include "VertexSet.h"

// Define o número de vértices contidos em um quad
#define QUAD_N_VERTEXES 4

// Define os índices dos vértices do quad
#define QUAD_BOTTOM_LEFT 0
#define QUAD_TOP_LEFT 1
#define QUAD_BOTTOM_RIGHT 2
#define QUAD_TOP_RIGHT 3

class Quad : public VertexSet
{
	public:
		// Construtores
		Quad( void );
		Quad( const Vertex* pVertexes );
		Quad( const Vertex* pV1, const Vertex* pV2, const Vertex* pV3, const Vertex* pV4 );
		Quad( const Vertex& pV1, const Vertex& pV2, const Vertex& pV3, const Vertex& pV4 );
	
		// Destrutor
		virtual ~Quad( void ){};
	
		// Vértices do quadrado
		Vertex quadVertexes[ QUAD_N_VERTEXES ];
	
		// Retorna o array de vértices que compõe a forma
		virtual const Vertex* getVertexes( void ) const;
	
		// Retorna quantos vértices que compõem a forma
		virtual uint16 getNVertexes( void ) const;
	
		// Mapeia as coordenadas de textura nos vértices do objeto. O parâmetro pTexCoords
		// deve apontar para um array de 8 floats, representando 4 pares (s,t) que definem
		// uma área quadrada na textura ativa. Os pares devem seguir a ordem: bottom-left,
		// top-left, bottom-right e top-right
		virtual void mapTexCoords( const float* pTexCoords );

		// Renderiza o objeto
		//virtual bool render( void );
};

// Implementação dos métodos inline
inline const Vertex* Quad::getVertexes( void ) const
{
	return quadVertexes;
}
	
// Retorna quantos vértices que compõem a forma
inline uint16 Quad::getNVertexes( void ) const
{
	return QUAD_N_VERTEXES;
}

#endif

