#include "Scene.h"
#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Scene::Scene( uint16 maxObjects ) : ObjectGroup( maxObjects ), clearBits( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ), clearDepth( 1.0f ), clearStencil( 0 ), pCurrCamera( NULL ), clearColor()
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Scene::~Scene( void )
{
	SAFE_DELETE( pCurrCamera );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Scene::render( void )
{
	glClearColor( clearColor.r, clearColor.g, clearColor.b, clearColor.a );
	glClearDepthf( clearDepth );
	
	// TODOO : Stencil ainda não é suportado pelo iphone. Ver se esse trecho de código vai dar problema
	glClearStencil( clearStencil );
	glClear( clearBits );
	
	pCurrCamera->place();
	return ObjectGroup::render();
}

/*==============================================================================================

MÉTODO setCurrentCamera

==============================================================================================*/

void Scene::setCurrentCamera( Camera* pCamera )
{
	// Deleta a câmera atual
	SAFE_DELETE( pCurrCamera );

	// Armazena a nova câmera
	pCurrCamera = pCamera;
}

