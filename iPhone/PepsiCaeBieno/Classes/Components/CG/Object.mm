#include "Object.h"

#include "Macros.h"
#include "Utils.h"

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

Object::Object( void )
	   : Renderable(), Updatable(),
		 pParent( NULL ), position(), speed(), acceleration(), size(), scale( 1.0f, 1.0f, 1.0f ), anchor(),
		 viewport( GET_DEFAULT_VIEWPORT() ), rotation()
{
	#if DEBUG
		memset( objName, 0, OBJECT_NAME_MAX_LENGTH );
	#endif
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/

bool Object::render( void )
{
	return Renderable::render()
		&& FDIF( size.x, 0.0f ) && FDIF( size.y, 0.0f ) && FDIF( size.z, 0.0f )
		&& FDIF( scale.x, 0.0f ) && FDIF( scale.y, 0.0f ) && FDIF( scale.z, 0.0f )
		&& ( viewport.width != 0 ) && ( viewport.height != 0 );
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool Object::update( float timeElapsed )
{
	if( !Updatable::update( timeElapsed ) )
		return false;
	
	// Calcula o deslocamento do objeto
	speed += acceleration * timeElapsed;
	position += ( speed * timeElapsed ) + ( acceleration * (( timeElapsed * timeElapsed ) * 0.5f ));
	
	return true;
}

/*===========================================================================================
 
MÉTODO setPosition
	Determina a posição do objeto.

============================================================================================*/

void Object::setPosition( float x, float y, float z )
{
	position.set( x, y, z ); 
}

/*===========================================================================================
 
MÉTODO setSpeed
	Determina a velocidade do objeto.

============================================================================================*/

void Object::setSpeed( float x, float y, float z )
{
	speed.set( x, y, z );
}

/*===========================================================================================
 
MÉTODO setAcceleration
	Determina a aceleração do objeto.

============================================================================================*/

void Object::setAcceleration( float x, float y, float z )
{
	acceleration.set( x, y, z );
}

/*===========================================================================================
 
MÉTODO setSize
	Determina o tamanho do objeto.

============================================================================================*/

void Object::setSize( float width, float height, float depth )
{
	size.set( width, height, depth );
}

/*==============================================================================================

MÉTODO setScale
	Determina a escala do objeto.

==============================================================================================*/

void Object::setScale( float scaleX, float scaleY, float scaleZ )
{
	scale.set( scaleX, scaleY, scaleZ );
}

/*==============================================================================================

MÉTODO setRotation
	Define a rotação atual do objeto.

==============================================================================================*/

void Object::setRotation( const Matrix4x4* pRotationMtx )
{
	rotation = *pRotationMtx;
}

/*==============================================================================================

MÉTODO setRotation
	Define a rotação atual do objeto.

==============================================================================================*/

void Object::setRotation( float angle, float axisX, float axisY, float axisZ )
{
	Matrix4x4 rotationMtx;
	Utils::GetRotationMatrix( &rotationMtx, angle, axisX, axisY, axisZ );
	setRotation( &rotationMtx );
}

/*==============================================================================================

MÉTODO rotate
	Imprime as sequências de animação do sprite.

==============================================================================================*/

void Object::rotate( const Matrix4x4* pRotationMtx )
{
	rotation *= *pRotationMtx;
}

/*==============================================================================================

MÉTODO rotate
	Imprime as sequências de animação do sprite.

==============================================================================================*/

void Object::rotate( float angle, float axisX, float axisY, float axisZ )
{
	Matrix4x4 rotationMtx;
	Utils::GetRotationMatrix( &rotationMtx, angle, axisX, axisY, axisZ );
	rotate( &rotationMtx );
}

/*==============================================================================================

MÉTODO setParent
	Determina o objeto pai deste objeto.

==============================================================================================*/

void Object::setParent( Object* pParentObject )
{
	pParent = pParentObject;
}

/*==============================================================================================

MÉTODO setViewport
	Determina o viewport deste objeto.

==============================================================================================*/

void Object::setViewport( const Viewport& v )
{
	viewport = v;
}

/*==============================================================================================

MÉTODO setViewport
	Determina o viewport deste objeto.

==============================================================================================*/

void Object::setViewport( int32 x, int32 y, int32 width, int32 height )
{
	viewport.set( x, y, width, height );
}

/*==============================================================================================

MÉTODO setAnchor
	Define uma âncora para ser utilizada no posicionamento do objeto.

==============================================================================================*/

void Object::setAnchor( uint16 achorCoordFlags )
{
	if( achorCoordFlags & ANCHOR_LEFT )
		anchor.x = 0.0f;
	else if( achorCoordFlags & ANCHOR_HCENTER )
		anchor.x = getWidth() * 0.5f;	
	else if( achorCoordFlags & ANCHOR_RIGHT )
		anchor.x = getWidth();

	if( achorCoordFlags & ANCHOR_TOP )
		anchor.y = 0.0f;
	else if( achorCoordFlags & ANCHOR_VCENTER )
		anchor.y = getHeight() * 0.5f;
	else if( achorCoordFlags & ANCHOR_BOTTOM )
		anchor.y = getHeight();

	if( achorCoordFlags & ANCHOR_FRONT )
		anchor.z = 0.0f;
	else if( achorCoordFlags & ANCHOR_DCENTER )
		anchor.z = getDepth() * 0.5f;
	else if( achorCoordFlags & ANCHOR_REAR )
		anchor.z = getDepth();
}

/*==============================================================================================

MÉTODO setAnchor
	Define uma âncora para ser utilizada no posicionamento do objeto.

==============================================================================================*/

void Object::setAnchor( float x, float y, float z )
{
	anchor.set( x, y, z );
}

/*==============================================================================================

MÉTODO setPositionByAnchor
	Determina a posição do objeto a partir da âncora.

==============================================================================================*/

void Object::setPositionByAnchor( const Point3f* pPos )
{
	setPosition( pPos->x - anchor.x, pPos->y - anchor.y, pPos->z - anchor.z );
}

/*==============================================================================================

MÉTODO setPositionByAnchor
	Determina a posição do objeto a partir da âncora.

==============================================================================================*/

void Object::setPositionByAnchor( float x, float y, float z )
{
	setPosition( x - anchor.x, y - anchor.y, z - anchor.z );
}

/*==============================================================================================

MÉTODO getPositionByAnchor
	Retorna a posição do objeto a partir da âncora.

==============================================================================================*/

Point3f* Object::getPositionByAnchor( Point3f* pPos ) const
{
	*pPos = *getPosition();
	*pPos += anchor;
	return pPos;
}

/*==============================================================================================

MÉTODO setName
	Determina um nome para o objeto.

==============================================================================================*/

#if DEBUG

void Object::setName( const char* pObjName )
{
	strncpy( objName, pObjName, OBJECT_NAME_MAX_LENGTH );
}

#endif
