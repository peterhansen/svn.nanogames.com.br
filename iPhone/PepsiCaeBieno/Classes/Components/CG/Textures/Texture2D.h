/*
 *  Texture2D.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/1/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef TEXTURE2D_H
#define TEXTURE2D_H 1

#include "NanoTypes.h"
#include "RefCounter.h"
#include "Texture.h"
#include "TextureFrameDescriptor.h"

class Texture2D : public Texture, public LoadableListener
{
	public:
		// Classe responsável pela alocação de texturas
		friend class ResourceManager;
	
		// Constrtutor
		// OBS: O USUÁRIO NÃO DEVE CHAMAR ESTE CONSTRUTOR SE DESEJA CARREGAR UM ARQUIVO DE RESOURCE. ESTE MÉTODO DEVE SER UTILIZADO
		// APENAS SE O USUÁRIO DESEJAR CARREGAR UMA TEXTURA VAZIA ATRAVÉS DE loadTextureEmpty( UINT32, UINT32 )
		// TODOO : Criar construtores e retirar os métodos loadTextureInFile, loadTextureWithData e loadTextureEmpty. Assim não precisaremos
		// nos preocupar com o OBS acima
		Texture2D( LoadableListener *pListener = NULL, int32 loadableId = -1 );

		// Destrtutor
		virtual ~Texture2D( void );

		// Carrega a textura de um arquivo
		bool loadTextureInFile( const char *pFileName, const TextureFrameDescriptor* pTexDesc = NULL );
	
		// Carrega a textura a partir dos dados recebidos como parâmetro
		bool loadTextureWithData( const NSData* pData, const TextureFrameDescriptor* pTexDesc = NULL );
	
		// Cria uma textura vazia com as dimensões desejadas
		bool loadTextureEmpty( uint32 texWidth, uint32 texHeight );

		// Obtém o array de bytes da textura
		virtual const uint8* getTextureData( void ) const;
	
		// Retorna a largura da textura
		inline uint32 getWidth( void ) const { return width; };
	
		// Retorna a altura da textura
		inline uint32 getHeight( void ) const { return height; };
	
		// Retorna o número de frames que a textura possui
		inline uint16 getNFrames( void ) const { return framesInfo.getNFrames(); };
	
		// Retorna o frame com o índice passado como parâmetro
		inline const TextureFrame* getFrame( uint16 frameIndex ) const { return &( framesInfo.getTextureFrames()[ frameIndex ] ); };
	
		// Retorna o array de frames da textura
		inline const TextureFrame* getFrames( void ) const { return framesInfo.getTextureFrames(); };
	
		// Altera um frame da textura
		void changeFrame( uint16 frameIndex, const TextureFrame* pTextureFrame ) { framesInfo.changeFrame( frameIndex, pTextureFrame ); };
	
		// Retorna a largura original dos frames da textura
		inline uint32 getOriginalFrameWidth( void ) const { return framesInfo.getOriginalFrameWidth(); };
	
		// Retorna a altura original dos frames da textura
		inline uint32 getOriginalFrameHeight( void ) const { return framesInfo.getOriginalFrameHeight(); };
	
		// Retorna os 4 pares de coordenadas de textura (s,t) correspondentes ao frame
		float* getFrameTexCoords( uint16 frame, float* pTexCoords );

	private:
		// Cria uma textura utilizando a imagem recebida
		bool loadTexture( CGImageRef pImage, const TextureFrameDescriptor* pFrameDescriptor );
	
		// Método para receber os eventos do objeto loadable
		virtual void loadableHandleEvent( LoadableOp op, int32 loadableId, uint32 data );
	
		// Largura e altura da imagem da textura
		uint32 width, height;

		// OLD
		// Ponteiro para o array de dados da textura
		//uint8* pTexData;
	
		// Listener que irá receber os eventos deste objeto
		LoadableListener* pUserListener;
	
		// Informações sobre a textura, como: altura, largura, frames e etc
		TextureFrameDescriptor framesInfo;
};

// Cria um apelido para os smart pointers utilizados
typedef RefCounter< Texture2D > Texture2DHandler;

#endif
