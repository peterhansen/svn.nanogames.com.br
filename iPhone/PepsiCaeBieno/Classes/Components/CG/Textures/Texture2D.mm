#include "Texture2D.h"
#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Texture2D::Texture2D( LoadableListener *pListener, int32 loadableId )
		  : Texture( NULL, loadableId ), width( 0 ), height( 0 ), /* OLD pTexData( NULL ),*/ pUserListener( pListener )
{
	setListener( this );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Texture2D::~Texture2D( void )
{
	// OLD
	//SAFE_DELETE_VEC( pTexData );
}

/*==============================================================================================

MÉTODO loadTextureInFile
	Carrega a textura bitmap de um arquivo.

==============================================================================================*/

bool Texture2D::loadTextureInFile( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	NSString* hFilePath = Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( pFileName ), @"png" );
	return loadTexture( [[UIImage imageWithContentsOfFile: hFilePath] CGImage], pTexDesc );
}

/*==============================================================================================

 MÉTODO loadTextureWithData
	Carrega a textura a partir dos dados recebidos como parâmetro.
 
==============================================================================================*/

bool Texture2D::loadTextureWithData( const NSData* pData, const TextureFrameDescriptor* pTexDesc )
{
	return loadTexture( [[UIImage imageWithData: pData ] CGImage], pTexDesc );
}

/*==============================================================================================

 MÉTODO loadTextureEmpty
	Cria uma textura vazia com as dimensões desejadas.
 
==============================================================================================*/

bool Texture2D::loadTextureEmpty( uint32 texWidth, uint32 texHeight )
{
	if( !IS_POW_OF_2( texWidth ) || !IS_POW_OF_2( texHeight ) )
		return false;
	
	// Limpa o código de erro
	glGetError();

	// Cria a textura
	glBindTexture( GL_TEXTURE_2D, texId );	
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL );	
	
	// Verifica se conseguiu criar a textura
	if( glGetError() != GL_NO_ERROR )
		return false;
	
	// Configura a textura
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	
	width = texWidth;
	height = texHeight;
	
	// Desfaz o vínculo com a textura criada
	glBindTexture( GL_TEXTURE_2D, GL_NONE );
	
	return true;
}

/*==============================================================================================

 MÉTODO getTextureData
	Retorna o array de cores da textura.
 
==============================================================================================*/

const uint8* Texture2D::getTextureData( void ) const
{
	// OLD
	//return pTexData;
	
	return NULL;
}

/*==============================================================================================

 MÉTODO getFrameTexCoords
	Retorna os 4 pares (s,t) de coordenadas de textura correspondentes ao frame.
 
==============================================================================================*/

float* Texture2D::getFrameTexCoords( uint16 frame, float* pTexCoords )
{
	const TextureFrame* pFrames = framesInfo.getTextureFrames();

	// Inferior esquerda
	pTexCoords[ 0 ] = ( float )( pFrames[ frame ].x ) / ( float )width;
	pTexCoords[ 1 ] = ( float )( pFrames[ frame ].y ) / ( float )height;
	
	// Superior esquerda
	pTexCoords[ 2 ] = pTexCoords[ 0 ];
	pTexCoords[ 3 ] = ( float )( pFrames[ frame ].y + pFrames[ frame ].height ) / ( float )height;

	// Inferior direita
	pTexCoords[ 4 ] = ( float )( pFrames[ frame ].x + pFrames[ frame ].width ) / ( float )width;
	pTexCoords[ 5 ] = pTexCoords[ 1 ];

	// Superior direita
	pTexCoords[ 6 ] = pTexCoords[ 4 ];
	pTexCoords[ 7 ] = pTexCoords[ 3 ];
	
	return pTexCoords;
}

/*==============================================================================================

 MÉTODO loadTexture
	Cria uma textura utilizando a imagem recebida.
 
==============================================================================================*/

bool Texture2D::loadTexture( CGImageRef pImage, const TextureFrameDescriptor* pTexDesc )
{
#if DEBUG
	if( !pImage )
		return false;
#endif

	uint8* pTexData = NULL;
	
	{ // Evita problemas com o goto

		// Verifica se as dimensões da textura são potências de 2
		width = CGImageGetWidth( pImage );
		height = CGImageGetHeight( pImage );
		
		if( !IS_POW_OF_2( width ) || !IS_POW_OF_2( height ) )
		{
			#if DEBUG
				LOG( @"As dimensões da textura não são potências de 2. Width = %d, Height = %d\n", width, height );
			#endif

			goto Error;
		}

		// Aloca a memória necessária para conter o bitmap da imagem
		pTexData = new uint8[ ( width * height ) << 2 ];
		if( pTexData == NULL )
			goto Error;

		// Preenche o bitmap
		CGContextRef texContext = CGBitmapContextCreate( pTexData, width, height, 8, width << 2, CGImageGetColorSpace( pImage ), kCGImageAlphaPremultipliedLast );
		CGContextSetBlendMode( texContext, kCGBlendModeCopy);
		CGContextDrawImage( texContext, CGRectMake( 0.0f, 0.0f, ( CGFloat )width, ( CGFloat )height ), pImage );
		CGContextRelease( texContext );

		// Especifica qual imagem utilizaremos para esta textura 2D
		glBindTexture( GL_TEXTURE_2D, texId );
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pTexData );
		glBindTexture( GL_TEXTURE_2D, GL_NONE );

		DESTROY_VEC( pTexData );
		
		// Armazena os dados dos frames contidos na textura
		if( pTexDesc )
		{
			if( !framesInfo.copy( *pTexDesc ) )
				goto Error;
		}
		else
		{
			TextureFrame frame;
			frame.set( 0, 0, width, height, 0, 0 );
			if( !framesInfo.setFrames( 1, &frame ) )
				goto Error;

			framesInfo.setOriginalFrameSize( width, height );
		}
		
		return true;
	
	} // Evita problemas com o goto
	
	// Label de tratamento de erros
	Error:
		CGImageRelease( pImage );
		DESTROY_VEC( pTexData );
		return false;
}

/*==============================================================================================

 MÉTODO loadableHandleEvent
	Método para receber os eventos do objeto loadable.
 
==============================================================================================*/

void Texture2D::loadableHandleEvent( LoadableOp op, int32 loadableId, uint32 data )
{
	if( op == LOADABLE_OP_LOAD )
		glBindTexture( GL_TEXTURE_2D, data );
	else
		glBindTexture( GL_TEXTURE_2D, GL_NONE );

	if( pUserListener )
		pUserListener->loadableHandleEvent( op, loadableId, data );
}
