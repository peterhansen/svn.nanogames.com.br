#ifndef PERSPECTIVE_CAMERA_H
#define PERSPECTIVE_CAMERA_H

/*==============================================================================================

CLASSE PerspectiveCamera

==============================================================================================*/

#include "Camera.h"
#include "Matrix4x4.h"

class PerspectiveCamera : public Camera
{
	public:
		// Construtores
		PerspectiveCamera( float fovy, float aspect, float zNear, float zFar, CameraType cameraType );
	
		PerspectiveCamera( float left = -1.0f, float right = 1.0f, float bottom = -1.0f, float top = 1.0f,
						   float zNear = 1.0, float zFar = 100.0f, CameraType cameraType = CAMERA_TYPE_AIR );

		// Destrutor
		virtual ~PerspectiveCamera( void ){};

		// Posiciona a câmera na cena
		virtual void place( void );

		// Determina a matriz de projeção utilizando gluPerspective
		void setPerspective( float fovy, float aspect, float zNear, float zFar );
	
		// Determina a matriz de projeção utilizando glFrustum
		void setFrustum( float left = -1.0f, float right = 1.0f, float bottom = -1.0f, float top = 1.0f, float zNear = 1.0f, float zFar = 100.0f );
	
		// Retorna o volume de visualização da câmera
		virtual Box* getViewVolume( Box* pViewVolume );
	
		// Obtém a matriz de projeção da câmera
		virtual Matrix4x4* getProjectionMatrix( Matrix4x4* pOut ) const;

	protected:
		// Matriz de projeção da câmera
		Matrix4x4 projection;
};

#endif