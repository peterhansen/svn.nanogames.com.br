/*
 *  Pattern.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PATTERN_H
#define PATTERN_H

#include "Object.h"

class Pattern : public Object
{
	public:
		// Construtores
		Pattern( Object* pFill = NULL );
		
		// Destrutor
		virtual ~Pattern( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

		// Determina um offset de desenho entre as repetições do pattern
		void setOffset( float x, float y );
	
		// Obtém o offset de desenho entre as repetições do pattern
		inline float getOffsetX( void ) const { return offsetX; };
		inline float getOffsetY( void ) const { return offsetY; };
	
		// Determina um objeto de preenchimento para o pattern
		void setFill( Object* pPatternFill );
	
		// Retorna o objeto utilizado como preenchimento do pattern
		inline Object* getFill( void ) { return pFill; };
	
	private:
		// Offset de desenho entre as repetições do pattern
		float offsetX, offsetY;
	
		// Objeto de preenchimento do pattern. Tem prioridade em relação a fillColor
		Object* pFill;
};

#endif
