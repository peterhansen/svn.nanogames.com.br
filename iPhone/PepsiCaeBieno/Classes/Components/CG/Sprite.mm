#include "Sprite.h"
#include "Exceptions.h"
#include "Macros.h"
#include "Quad.h"
#include "ResourceManager.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Sprite::Sprite( const Sprite* pSprite )
	   : Object(), pVertexSet( NULL ), currFrame( -1 ), frameTimeCounter( 0.0f ),
		 texBlendSrcFactor( GL_ONE ), texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ), texEnvMode( GL_MODULATE ), pFramesImg( NULL ), animDesc( pSprite->animDesc ),
		 mirrorOps( MIRROR_NONE ), currSequence( -1 ), currSequenceStep( -1 ),
		 loopingSequence( false ), pListener( NULL )
{
	build( pSprite->pFramesImg, pSprite->pVertexSet );
	
	position = pSprite->position;
	speed = pSprite->speed;
	acceleration = pSprite->acceleration;
	size = pSprite->size;
	scale = pSprite->scale;
	rotation = pSprite->rotation;
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Sprite::Sprite( const char* pImagesIdentifier, const char* pSpriteDescriptorName, CreateVertexSetFunction pVertexesCreator )
	   : Object(), pVertexSet( NULL ), currFrame( -1 ), frameTimeCounter( 0.0f ),
		 texBlendSrcFactor( GL_ONE ), texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ), texEnvMode( GL_MODULATE ), pFramesImg( NULL ),
		 mirrorOps( MIRROR_NONE ), currSequence( -1 ), currSequenceStep( -1 ),
		 loopingSequence( false ), pListener( NULL )
{
	build( pImagesIdentifier, pSpriteDescriptorName, pVertexesCreator() );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Sprite::~Sprite( void )
{
//	clean();
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

void Sprite::build( const char* pImagesIdentifier, const char* pSpriteDescriptorName, const VertexSetHandler& vertexes )
{
	// Obtém a path do arquivo descritor
	NSString* hSpriteDescriptorPath = Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( pSpriteDescriptorName ), @"bin" );
	NSString* hFileContents = [NSString stringWithContentsOfFile: hSpriteDescriptorPath encoding:NSUTF8StringEncoding error: NULL];
	if( hFileContents == NULL )
		goto Error;

	{ // Evita erros com os gotos
		
		NSScanner* hScanner = [NSScanner scannerWithString: hFileContents];
		TextureFrameDescriptor texDesc;
		if( !texDesc.readFromScanner( hScanner ) || !animDesc.readFromScanner( hScanner ) )
			goto Error;

		// Carrega as imagens que contém os frames do sprite
		build( ResourceManager::GetTexture2D( pImagesIdentifier, &texDesc ), vertexes );
	}
	return;
	
	// Label de tratamento de erros
	Error:
#if DEBUG
		throw ConstructorException( "Sprite::build( const char*, const char* ) : Unable to create object" );
#else
		throw ConstructorException();
#endif
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

void Sprite::build( const Texture2DHandler& image, const VertexSetHandler& vertexes )
{
	if( !image || !vertexes )
#if DEBUG
		throw ConstructorException( "Sprite::build( const Texture2DHandler&, const VertexSetHandler& ) : Unable to create object" );
#else
		throw ConstructorException();
#endif
		
	// Armazena o manipulador da textura
	pFramesImg = image;
	
	// Armazena aestrutura de vértices sobre a qual aplicaremos a textura dos frames
	pVertexSet = vertexes;
	
	// O tamanho inicial do sprite é igual ao tamanho original dos frames
	setSize( pFramesImg->getOriginalFrameWidth(), pFramesImg->getOriginalFrameHeight(), 1.0f );
	
	// O sprite começa na primeira etapa da primeira sequência
	setAnimSequence( 0 );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Sprite::render( void )
{	
	if( !Object::render() || ( animDesc.getNAnimSequences() == 0 ) || ( currSequence < 0 ) || ( currSequenceStep < 0 ) || ( currFrame < 0 ) )
		return false;
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	// Executa as transformações sobre os vértices do sprite
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	// Posiciona o sprite no mundo

	// OBS : Isso REALMENTE funciona...
	//	An optimum compromise that allows all primitives to be specified at integer
	//positions, while still ensuring predictable rasterization, is to translate x
	//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
	// away from the centers of pixels, while moving line vertices close enough to
	// the pixel centers
	const TextureFrame* pFrames = pFramesImg->getFrames();
	glTranslatef( position.x + pFrames[ currFrame ].offsetX + ( pFrames[ currFrame ].width * scale.x * 0.5f ) + 0.375f, position.y + pFrames[ currFrame ].offsetY + ( pFrames[ currFrame ].height * scale.y * 0.5f ) + 0.375f, position.z );

	// Redimensiona o sprite
	glScalef( pFrames[ currFrame ].width * scale.x, pFrames[ currFrame ].height * scale.y, scale.z );
	
	// Rotaciona o objeto
	if( isMirrored( MIRROR_HOR ) )
		glRotatef( 180.0f, 0.0f, 1.0f, 0.0f );
	
	if( isMirrored( MIRROR_VER ) )
		glRotatef( 180.0f, 1.0f, 0.0f, 0.0f );
	
	Matrix4x4 aux;
	glMultMatrixf( ( float* )rotation.getTranspose( &aux ) );
	
	// TODOO : Acrescentar um boolean à estrutura do frame para indicar se o frame possui
	// transparência ou não. Assim podemos otimizar a (des)ativação do blend
	// Habilita a transparência
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( texBlendSrcFactor, texBlendDstFactor );
	
	// Carrega a textura
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pFramesImg->load();
	
	// Determina o modo de texturização
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, texEnvMode );
	
	if( FDIF( scale.x, 1.0f ) || FDIF( scale.y, 1.0f ) )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	}
	else
	{		
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	}
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );

	// Renderiza o sprite
	pVertexSet->render();
	
	// Reseta os estados do OpenGL
	pFramesImg->unload();
	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );

	// Cancela as transformações do sprite
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	
	#if DEBUG
		//renderFrames();
	#endif
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );
	
	if( customViewport )
		glDisable( GL_SCISSOR_TEST );

	return true;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool Sprite::update( float timeElapsed )
{
	if( !Object::update( timeElapsed ) || !autoAnimation )
		return false;

	bool ret;
	int16 nextStepIfLooping;

	switch( handAnimUpdate( timeElapsed ) )
	{
		case 0:
			return false;
			
		case 1:
			ret = handAnimNextStep();
			nextStepIfLooping = 0;
			break;

		case -1:
			ret = handAnimPrevStep();
			nextStepIfLooping = getCurrAnimSequenceNSteps() - 1;
			break;
	}
	
	if( ret )
	{
		if( pListener )
			pListener->onAnimEnded( this );
		
		if( loopingSequence )
		{
			if( pListener )
				pListener->onAnimLooped( this );
			
			setCurrentAnimSequenceStep( nextStepIfLooping );
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO updateForward
	Executa a animação do sprite normalmente (ver updateRewind).

==============================================================================================*/

bool Sprite::updateForward( float timeElapsed )
{
	// Aqui podemos testar igualdade com 0.0f sem FEQL, pois é uma feature do script
	// 0.0 significar "sem atualização"
	float stepDuration = animDesc.getAnimSequence( currSequence )->getStepDuration( currSequenceStep );
	if( stepDuration <= 0.0f )
		return false;

	// Verifica se já ultrapassou o tempo do frame
	frameTimeCounter += timeElapsed;
	
	if( frameTimeCounter > stepDuration )
	{
		// Armazena o resíduo do tempo
		frameTimeCounter = fmod( frameTimeCounter, stepDuration );
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO updateRewind
	Faz a animação do sprite voltar no tempo.

==============================================================================================*/

bool Sprite::updateRewind( float timeElapsed )
{
	// Verifica se já ultrapassou o tempo do frame
	frameTimeCounter += timeElapsed;
	
	if( frameTimeCounter < 0.0 )
	{
		// Armazena o resíduo do tempo
		const AnimSequence* pCurrSequence = animDesc.getAnimSequence( currSequence );
		float stepDuration = pCurrSequence->getStepDuration( currSequenceStep );
		
		int16 prevStep = getCurrAnimSequenceStep() - 1;
		if( prevStep >= 0 )
			frameTimeCounter = pCurrSequence->getStepDuration( prevStep ) - fmod( fabs( frameTimeCounter ), stepDuration );
		
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO setAnimSequence
	Determina a sequência de animação a ser tocada.

==============================================================================================*/

void Sprite::setAnimSequence( int16 sequenceIndex, bool looping, bool autoAnim )
{
	currSequence = sequenceIndex;
	loopingSequence = looping;
	autoAnimation = autoAnim;
	
	if(( animDesc.getNAnimSequences() > 0 ) && ( currSequence >= 0 ))
		setCurrentAnimSequenceStep( 0 );
}

/*==============================================================================================

MÉTODO setCurrentAnimSequenceStep
	Determina qual a etapa da animação atual.

==============================================================================================*/

bool Sprite::setCurrentAnimSequenceStep( int16 step )
{
	// Atualiza a etapa atal
	currSequenceStep = step;
	
	// Zera o contador que controla a duração de cada etapa
	frameTimeCounter = 0.0f;
	
	// Modifica o frame
	if( ( currSequenceStep >= 0 ) && ( currSequenceStep < getCurrAnimSequenceNSteps() ) )
	{
		if( pListener )
			pListener->onAnimStepChanged( this );
		
		setCurrentFrame( animDesc.getAnimSequence( currSequence )->getStepFrameIndex( currSequenceStep ) );
		
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO mirror
	Determina as operações de espelhamento que devem ser aplicadas sobre o sprite.

==============================================================================================*/

void Sprite::mirror( MirrorOp ops )
{
	mirrorOps |= ops;
}

/*==============================================================================================

MÉTODO isMirrored
	Indica se uma determinada operação de espelhamento está aplicada sobre o sprite.

==============================================================================================*/

bool Sprite::isMirrored( MirrorOp op )
{
	return ( mirrorOps & op ) != 0;
}

/*==============================================================================================

MÉTODO unmirror
	Cancela operações de espelhamento.

==============================================================================================*/

void Sprite::unmirror( MirrorOp ops )
{
	mirrorOps &= ~ops;
}

/*==============================================================================================

MÉTODO print
	Imprime as sequências de animação do sprite.

==============================================================================================*/

#if DEBUG

void Sprite::print( void )
{
	LOG( @"Frames:" );
	for( int16 i=0 ; i < pFramesImg->getNFrames() ; ++i )
	{
		const TextureFrame* pFrame = pFramesImg->getFrame( i );
		LOG( @"Frame %d: X = %d, Y = %d, Width = %d, Height = %d, OffsetX = %d, OffsetY = %d", i, pFrame->x, pFrame->y, pFrame->width, pFrame->height, pFrame->offsetX, pFrame->offsetY );
	}
	LOG( @"   " );
	LOG( @"   " );
	
	LOG( @"Sequences" );
	for( int16 i=0 ; i < animDesc.getNAnimSequences() ; ++i )
	{
		LOG( @"Sequence %d:", i );
		const AnimSequence* pCurrSequence = animDesc.getAnimSequence( i );
		for( uint16 j=0 ; j < pCurrSequence->getNSteps() ; ++j )
			LOG( @"Frame %d - Duration %.4f",pCurrSequence->getStepFrameIndex( j ), pCurrSequence->getStepDuration( j ) );
		LOG( @"   " );
		LOG( @"   " );
	}
}

#endif

/*==============================================================================================

MÉTODO clean
	Libera os recursos alocados pelo objeto.

==============================================================================================*/

//void Sprite::clean( void )
//{
//	// Não são ponteiros, então não precisamos deletar
//	SAFE_DELETE( pVertexSet );
//	SAFE_DELETE( pFramesImg );
//	
//	// TODOOO : Poder suportar arrays de imagens?
////	if( pFramesImgs )
////	{
////		for( int16 i = 0 ; i < nFrames ; ++i )
////			SAFE_DELETE( pFramesImgs[i] );
////
////		DESTROY_VEC( pFramesImgs );
////	}
//}

/*==============================================================================================

MÉTODO loadFramesImgs
	Carrega as imagens que contém os frames do sprite.

==============================================================================================*/

//bool Sprite::loadFramesImgs( const char* pImagesIdentifier, const TextureFrameDescriptor* pDescriptor )
//{
//	pFramesImg = ResourceManager::GetTexture2D( pImagesIdentifier, pDescriptor );
//	return true;
//	
//	// TODOOO : Permitir várias texturas para o sprite
////	pFramesImgs = new Texture2D*[ nFrames ];
////	if( !pFramesImgs )
////		return false;
////
////	for( int16 i = 0 ; i < nFrames ; ++i )
////	{
////		pFramesImgs[ i ] = new Texture2D();
////		if( !pFramesImgs[ i ] || !pFramesImgs[ i ]->loadTextureInFile( pImagesIdentifier ) )
////			return false;
////	}
////	return true;
//}

/*==============================================================================================

MÉTODO setCurrentFrame
	Determina o frame atual do sprite.

==============================================================================================*/

void Sprite::setCurrentFrame( int16 frameIndex )
{
	// Armazena o frame atual
	currFrame = frameIndex;

	// Determina as novas coordenadas de textura
	if( frameIndex >= 0 )
		setFrameTexCoords();
}

/*==============================================================================================

MÉTODO CreateDefaultVertexSet
	Cria o conjunto de vértices padrão que será utilizado para renderizar os sprites.

==============================================================================================*/

VertexSetHandler Sprite::CreateDefaultVertexSet( void )
{
	// TODOO : Está cagado (mas funcionando), pode vazar memória!!!!!!!
	return VertexSetHandler( new Quad() );
}

/*===========================================================================================

MÉTODO setFrameTexCoords
	Preenche as coordenadas de textura dos vértices em relação ao frame atual.

============================================================================================*/

void Sprite::setFrameTexCoords( void )
{
	// TODOO : Fazer o espelhamento através das coordenadas de textura, e não através de operações
	// na matriz de textura. Assim iremos otimizar a renderização
	float texCoords[ QUAD_N_VERTEXES * 2 ];
	pVertexSet->mapTexCoords( pFramesImg->getFrameTexCoords( currFrame, texCoords ));
}

/*==============================================================================================

MÉTODO renderFrames
	Renderiza bordas para indicar as áreas do sprite e de sua imagem.

==============================================================================================*/

#if DEBUG

void Sprite::renderFrames( void )
{
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE );
	glEnableClientState( GL_VERTEX_ARRAY );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x + ( size.x * scale.x * 0.5f ), position.y + ( size.y * scale.y * 0.5f ), position.z );
	glScalef( scale.x, scale.y, scale.z );
	glScalef( size.x, size.y, size.z );
	
	Matrix4x4 aux;
	glMultMatrixf( ( float* )rotation.getTranspose( &aux ) ); // Matrizes OpenGL são column-major
	
	Point3f border[] =	{
							Point3f( -0.5f, -0.5f ),
							Point3f(  0.5f, -0.5f ),
							Point3f(  0.5f,  0.5f ),
							Point3f( -0.5f,  0.5f )
						};
	
	glColor4ub( 0, 255, 255, 255 );
	glVertexPointer( 3, GL_FLOAT, 0, border );
	glDrawArrays( GL_LINE_LOOP, 0, 4 );
	
	glPopMatrix();
}

#endif

/*==============================================================================================

MÉTODO setListener
	Determina um objeto que irá receber eventos do sprite.

==============================================================================================*/

void Sprite::setListener( SpriteListener* pListener )
{
	this->pListener = pListener;
}

/*==============================================================================================

MÉTODO handAnimUpdate
	Incrementa o contador de tempo da etapa de animação atual. Retorna:
		* -1 : Quando devemos retroceder uma etapa de animação
		*  0 : Quando não devemos fazer modificações
		*  1 : Quando devemos avançar uma etapa de animação

==============================================================================================*/

int8 Sprite::handAnimUpdate( float timeElapsed )
{
	if( ( animDesc.getNAnimSequences() == 0 )
	    || ( currSequence < 0 )
	    || ( currSequenceStep < 0 )
	    || ( currSequenceStep >= getCurrAnimSequenceNSteps() ) )
		return 0;

	if( timeElapsed < 0.0f )
		return updateRewind( timeElapsed ) ? -1 : 0;

	return updateForward( timeElapsed ) ? 1 : 0;
}

/*==============================================================================================

MÉTODO handAnimNextStep
	Avança uma etapa da animação. Retorna se a animação terminou.

==============================================================================================*/

bool Sprite::handAnimNextStep( void )
{
	int16 nextStep = getCurrAnimSequenceStep() + 1;
	setCurrentAnimSequenceStep( nextStep );
	
	return nextStep >= getCurrAnimSequenceNSteps();
}

/*==============================================================================================

MÉTODO handAnimPrevStep
	Volta uma etapa da animação. Retorna se a animação terminou.

==============================================================================================*/
	
bool Sprite::handAnimPrevStep( void )
{
	int16 prevStep = getCurrAnimSequenceStep() - 1;
	setCurrentAnimSequenceStep( prevStep );
	
	return prevStep < 0;
}
