/*
 *  Object.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OBJECT_H
#define OBJECT_H

#include "Matrix4x4.h"
#include "Point3f.h"
#include "Renderable.h"
#include "Updatable.h"
#include "Viewport.h"

#if DEBUG
	#define OBJECT_NAME_MAX_LENGTH 128
#endif

// Flags aceitas pelo método void setAnchor( uint16 )
#define ANCHOR_LEFT		0x0001
#define ANCHOR_HCENTER	0x0002
#define ANCHOR_RIGHT	0x0004

#define ANCHOR_TOP		0x0008
#define ANCHOR_VCENTER	0x0010
#define ANCHOR_BOTTOM	0x0020

#define ANCHOR_FRONT	0x0040
#define ANCHOR_DCENTER	0x0080
#define ANCHOR_REAR		0x0100

class Object : public Renderable, public Updatable
{
	public:
		// Construtor
		Object( void );

		// Destrutor
		virtual ~Object( void ){};
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Obtém a posição do objeto
		virtual inline const Point3f* getPosition( void ) const { return &position; };
		
		// Determina a posição do objeto
		inline void setPosition( const Point3f *pPosition ){ setPosition( pPosition->x, pPosition->y, pPosition->z ); };
		virtual void setPosition( float x = 0.0f, float y = 0.0f, float z = 0.0f );
	
		// Movimenta o objeto a partir de sua posição atual
		inline void move( const Point3f *pMovement ){ Point3f aux = position + *pMovement; setPosition( &aux ); };
		inline void move( float dx = 0.0f, float dy = 0.0f, float dz = 0.0f ){ Point3f aux( position.x + dx, position.y + dy, position.z + dz ); setPosition( &aux ); };

		// Obtém a velocidade do objeto
		inline const Point3f* const getSpeed( void ) const { return &speed; };
		
		// Determina a velocidade do objeto
		inline void setSpeed( const Point3f *pSpeed ){ setSpeed( pSpeed->x, pSpeed->y, pSpeed->z ); };
		virtual void setSpeed( float x = 0.0f, float y = 0.0f, float z = 0.0f );
	
		// Obtém a aceleração do objeto
		inline const Point3f* const getAcceleration( void ) const { return &acceleration; };
		
		// Determina a aceleração do objeto
		inline void setAcceleration( const Point3f *pAcceleration ){ setAcceleration( pAcceleration->x, pAcceleration->y, pAcceleration->z ); };
		virtual void setAcceleration( float x = 0.0f, float y = 0.0f, float z = 0.0f );
	
		// Obtém a largura do objeto
		inline float getWidth( void ) const { return getOriginalWidth() * scale.x; };
		inline float getOriginalWidth( void ) const { return size.x; };
	
		// Obtém a altura do objeto
		inline float getHeight( void ) const { return getOriginalHeight() * scale.y; };
		inline float getOriginalHeight( void ) const { return size.y; };
	
		// Obtém a profundidade do objeto
		inline float getDepth( void ) const { return getOriginalDepth() * scale.z; };
		inline float getOriginalDepth( void ) const { return size.z; };
	
		// Obtém o tamanho do objeto
		inline const Point3f* getSize( void ) const { return &size; };
		inline void getSize( float* pWidth, float* pHeight, float* pDepth ) const { if( pWidth ) *pWidth = getWidth(); if( pHeight ) *pHeight = getHeight(); if( pDepth ) *pDepth = getDepth(); };
	
		// Determina o tamanho do objeto
		inline void setSize( const Point3f* pSize ) { setSize( pSize->x, pSize->y, pSize->z ); };
		virtual void setSize( float width, float height, float depth = 1.0f );
	
		// Determina a largura do objeto
		inline void setWidth( float width ) { setSize( width, getHeight(), getDepth() ); };
	
		// Determina a altura do objeto
		inline void setHeight( float height ) { setSize( getWidth(), height, getDepth() ); };
	
		// Determina a profundidade do objeto
		inline void setDepth( float depth ) { setSize( getWidth(), getHeight(), depth ); };
	
		// Obtém a escala do objeto
		float getScaleX( void ) const;
		float getScaleY( void ) const;
		float getScaleZ( void ) const;
		const Point3f* getScale( void ) const;
		
		// Determina a escala do objeto
		void setScale( const Point3f* pScale );
		virtual void setScale( float scaleX = 1.0f, float scaleY = 1.0f, float scaleZ = 1.0f );
	
		// Define a rotação atual do objeto
		void setRotation( const Matrix4x4* pRotationMtx );
		void setRotation( float angle, const Point3f* pAxis );
		void setRotation( float angle, float axisX, float axisY, float axisZ );
	
		// Rotaciona o objeto a partir da rotação atual
		void rotate( const Matrix4x4* pRotationMtx );
		void rotate( float angle, const Point3f* pAxis );
		void rotate( float angle, float axisX, float axisY, float axisZ );
		
		// Obtém a rotação aplicada ao objeto
		const Matrix4x4* getRotation( void ) const;
	
		// Determina o objeto pai deste objeto
		void setParent( Object* pParentObject );
	
		// Retorna o objeto pai deste objeto
		Object* getParent( void ) const;
	
		// Determina o viewport deste objeto
		void setViewport( const Viewport& v );
		void setViewport( int32 x, int32 y, int32 width, int32 height );
	
		// Retorna o viewport deste objeto
		const Viewport& getViewport( void ) const;
	
		// Define uma âncora para ser utilizada no posicionamento do objeto
		void setAnchor( uint16 achorFlags );
		void setAnchor( float x, float y, float z );
	
		// Obtém as coordenadas âncora
		float getAnchorX( void ) const;
		float getAnchorY( void ) const;
		float getAnchorZ( void ) const;

		// Determina a posição do objeto a partir da âncora
		void setPositionByAnchor( const Point3f* pPos );
		void setPositionByAnchor( float x, float y, float z );

		// Retorna a posição do objeto a partir da âncora
		Point3f* getPositionByAnchor( Point3f* pPos ) const;
	
#if DEBUG
		// Determina um nome para o objeto
		void setName( const char* pObjName );
	
		// Retorna o nome do objeto
		const char* getName( void ) const;
#endif
	
	protected:
		// Objeto pai desse objeto
		Object* pParent;
		
		// Posição do objeto
		Point3f	position;
	
		// Velocidade do objeto
		Point3f speed;
	
		// Aceleração do objeto
		Point3f acceleration;
	
		// Tamanho do objeto
		Point3f size;
	
		// Fator de escala do objeto
		Point3f scale;
	
		// Coordenada de referência utilizada para o posicionamento do objeto
		Point3f anchor;
	
		// Viewport do objeto
		Viewport viewport;
	
		// Rotação aplicada ao objeto
		Matrix4x4 rotation;
	
		#if DEBUG
			// Nome do objeto
			char objName[ OBJECT_NAME_MAX_LENGTH ];
		#endif
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

/*==============================================================================================

MÉTODO getScaleX
	Obtém o fator de escala do objeto no eixo X.

==============================================================================================*/

inline float Object::getScaleX( void ) const
{
	return scale.x;
}

/*==============================================================================================

MÉTODO getScaleY
	Obtém o fator de escala do objeto no eixo Y.

==============================================================================================*/

inline float Object::getScaleY( void ) const
{
	return scale.y;
}

/*==============================================================================================

MÉTODO getScaleZ
	Obtém o fator de escala do objeto no eixo Z.

==============================================================================================*/

inline float Object::getScaleZ( void ) const
{
	return scale.z;
}

/*==============================================================================================

MÉTODO getScale
	Obtém o fator de escala do objeto.

==============================================================================================*/

inline const Point3f* Object::getScale( void ) const
{
	return &scale;
}

/*==============================================================================================

MÉTODO getScale
	Determina o fator de escala do objeto.

==============================================================================================*/

inline void Object::setScale( const Point3f* pScale )
{
	setScale( pScale->x, pScale->y, pScale->z );
};

/*==============================================================================================

MÉTODO getRotation
	Obtém a rotação aplicada ao objeto.

==============================================================================================*/

inline const Matrix4x4* Object::getRotation( void ) const
{
	return &rotation;
}

/*==============================================================================================

MÉTODO setRotation
	Define a rotação atual do objeto.

==============================================================================================*/

inline void Object::setRotation( float angle, const Point3f* pAxis )
{
	setRotation( angle, pAxis->x, pAxis->y, pAxis->z );
}

/*==============================================================================================

MÉTODO rotate
	Imprime as sequências de animação do sprite.

==============================================================================================*/

inline void Object::rotate( float angle, const Point3f* pAxis )
{
	rotate( angle, pAxis->x, pAxis->y, pAxis->z );
}

/*==============================================================================================

MÉTODO getParent
	Retorna o objeto pai deste objeto.

==============================================================================================*/

inline Object* Object::getParent( void ) const
{
	return pParent;
}

/*==============================================================================================

MÉTODO getViewport
	Retorna o viewport deste objeto.

==============================================================================================*/

inline const Viewport& Object::getViewport( void ) const
{
	return viewport;
}

/*==============================================================================================

MÉTODO getAnchorX
	Retorna a coordenada X da posição de âncora.

==============================================================================================*/

inline float Object::getAnchorX( void ) const
{
	return anchor.x;
}

/*==============================================================================================

MÉTODO getAnchorY
	Retorna a coordenada Y da posição de âncora.

==============================================================================================*/

inline float Object::getAnchorY( void ) const
{
	return anchor.y;
}

/*==============================================================================================

MÉTODO getAnchorZ
	Retorna a coordenada Z da posição de âncora.

==============================================================================================*/

inline float Object::getAnchorZ( void ) const
{
	return anchor.z;
}

/*==============================================================================================

MÉTODO getName
	Retorna o nome do objeto.

==============================================================================================*/

#if DEBUG

inline const char* Object::getName( void ) const
{
	return objName;
}

#endif

#endif

