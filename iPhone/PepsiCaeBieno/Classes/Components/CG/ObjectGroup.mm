#include "ObjectGroup.h"

#include "Exceptions.h"
#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

ObjectGroup::ObjectGroup( uint16 maxObjects )
			: Object(), maxObjects( maxObjects ), nObjects( 0 ), pObjects( NULL ),
			  pObjectsZOrder( NULL )
{
	if( !build() )
#if DEBUG
		throw OutOfMemoryException( "ObjectGroup::ObjectGroup( uint16 ): unable to create object" );
#else
		throw OutOfMemoryException();
#endif
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

ObjectGroup::~ObjectGroup( void )
{
	removeAllObjects();
		
	DESTROY_VEC( pObjects );
	DESTROY_VEC( pObjectsZOrder );
}

/*==============================================================================================

MÉTODO insertObject
	Insere um novo objeto no grupo.

==============================================================================================*/

bool ObjectGroup::insertObject( Object* pObject, uint16 *pIndex )
{
	// Verifica se o objeto passado é valido e se ainda possuímos espaço no grupo para armazená-lo
	if( ( pObject == NULL ) || ( nObjects >= maxObjects ) )
		return false;
	
	// Armazena o objeto no grupo
	pObjects[ nObjects ] = pObject;
	
	// Indica que este grupo é o pai do objeto
	pObjects[ nObjects ]->setParent( this );
	
	// Armazena no parâmetro de retorno o índice do grupo no qual o objeto foi inserido
	if( pIndex )
		*pIndex = nObjects;
	
	// Atualiza a variável que controla quantos elementos possuímos no grupo
	++nObjects;

	return true;
}

/*==============================================================================================

MÉTODO removeObject
	Retira um objeto do grupo.

==============================================================================================*/

bool ObjectGroup::removeObject( uint16 index )
{
	// Verifica se o índice está em um intervalo válido
	if( index >= nObjects )
		return false;
	
	// Apaga o objeto
	DESTROY( pObjects[ index ] );
	
	// Faz um "chega pra cá" nos elementos do grupo
	uint16 max = nObjects - 1;
	for( uint16 i = index ; i < max ; ++i )
		pObjects[i] = pObjects[i+1];
	
	pObjects[ max ] = NULL;
	
	// Atualiza a variável que controla quantos elementos possuímos no grupo
	--nObjects;
		
	return true;
}

/*==============================================================================================

MÉTODO removeAllObjects
	Esvazia o grupo.

==============================================================================================*/

void ObjectGroup::removeAllObjects( void )
{
	// Apaga os objetos do grupo
	for( uint16 i = 0 ; i < nObjects ; ++i )
		DESTROY( pObjects[i] );
	
	// Atualiza a variável que controla quantos elementos possuímos no grupo
	nObjects = 0;
}

/*==============================================================================================

MÉTODO getObject
	Retorna o objeto contido no grupo no índice passado como parâmetro.

==============================================================================================*/

Object* ObjectGroup::getObject( uint16 index )
{
	// Verifica se o índice está em um intervalo válido
	if( index >= nObjects )
		return NULL;
	return pObjects[ index ];
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool ObjectGroup::render( void )
{
	if( !Object::render() )
		return false;

	bool ret = false;
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x, position.y, position.z );
	glScalef( scale.x, scale.y, scale.z );
	
	Matrix4x4 aux;
	glMultMatrixf( ( float* )rotation.getTranspose( &aux ) ); // Matrizes OpenGL são column-major

	// TODOO : Renderizar de acordo com a ordenação de ordem Z
//	for( uint16 i = 0 ; i < nObjects ; ++i )
//		ret |= ( * pObjectsZOrder[i] )->render();
		
	for( uint16 i = 0 ; i < nObjects ; ++i )
		ret |= pObjects[i]->render();
	
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	
	if( customViewport )
		glDisable( GL_SCISSOR_TEST );
	
#if DEBUG	
	//renderBorder();
#endif

	return ret;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool ObjectGroup::update( float timeElapsed )
{
	if( !Object::update( timeElapsed ) )
		return false;

	bool ret = false;

	for( uint16 i = 0 ; i < nObjects ; ++i )
		ret |= pObjects[i]->update( timeElapsed );
	
	return ret;
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool ObjectGroup::build( void )
{
	pObjects = new Object*[ maxObjects ];
	if( pObjects == NULL )
		return false;
	
	pObjectsZOrder = new Object**[ maxObjects ];
	if( pObjectsZOrder == NULL )
	{
		DESTROY_VEC( pObjects );
		return false;
	}
	
	size.set( SCREEN_WIDTH, SCREEN_HEIGHT, 1.0f );

	return true;
}

/*==============================================================================================

MÉTODO setObjectZOrder
	Determina o slot do grupo onde o objeto deve ser inserido.

==============================================================================================*/

// TODOO : Este método está gambiarrento!!!!!!!!!!!!!!!!!!!!

bool ObjectGroup::setObject( uint16 index, Object* pObject )
{
	// Verifica se o índice está em um intervalo válido
	if( index >= nObjects )
		return false;
	
	delete pObjects[ index ];

	pObjects[ index ] = pObject;
	
	return true;
}

/*==============================================================================================

MÉTODO setObjectZOrder
	Determina a ordem Z do objeto de índice 'objIndex'.

==============================================================================================*/

void ObjectGroup::setObjectZOrder( uint16 objIndex, uint16 zOrder )
{
	// TODOO : Implementar !!!!
}

/*==============================================================================================

MÉTODO getObjectZOrder
	Retorna a ordem Z do objeto de índice 'objIndex'.

==============================================================================================*/

uint16 ObjectGroup::getObjectZOrder( uint16 objIndex )
{
	// TODOO : Implementar !!!!
	return 0;
}

/*==============================================================================================

MÉTODO renderBorder
	Renderiza um quad em volta da área do objeto.

==============================================================================================*/

#if DEBUG

void ObjectGroup::renderBorder( void )
{
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE );
	glEnableClientState( GL_VERTEX_ARRAY );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x + ( size.x * scale.x * 0.5f ), position.y + ( size.y * scale.y * 0.5f ), position.z );
	glScalef( scale.x, scale.y, scale.z );
	glScalef( size.x, size.y, size.z );
	
	Matrix4x4 aux;
	glMultMatrixf( ( float* )rotation.getTranspose( &aux ) ); // Matrizes OpenGL são column-major
	
	Point3f border[] =	{
							Point3f( -0.5f, -0.5f ),
							Point3f(  0.5f, -0.5f ),
							Point3f(  0.5f,  0.5f ),
							Point3f( -0.5f,  0.5f )
						};
	
	glColor4ub( 255, 0, 0, 255 );
	glVertexPointer( 3, GL_FLOAT, 0, border );
	glDrawArrays( GL_LINE_LOOP, 0, 4 );
	
	glPopMatrix();
}

#endif