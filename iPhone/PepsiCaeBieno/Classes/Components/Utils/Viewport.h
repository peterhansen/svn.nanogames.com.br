/*
 *  Viewport.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/5/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef VIEWPORT_H
#define VIEWPORT_H 1

#include "NanoTypes.h"

struct Viewport
{
	// Construtor
	Viewport( void ) : x( 0 ), y( 0 ), width( 0 ), height( 0 ) {};
	Viewport( int32 x, int32 y, int32 width, int32 height ) : x( x ), y( y ), width( width ), height( height ) {};
	
	// Aplica o viewport levando em consideração a orientação do device
	void apply( void );
	
	// Inicializa o objeto
	void set( int32 x, int32 y, int32 width, int32 height )
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	};
	
	// Operadores de comparação
	bool operator == ( const Viewport& other ) const
	{
		return ( x == other.x ) && ( y == other.y ) && ( width == other.width ) && ( height == other.height );
	};
	
	bool operator != ( const Viewport& other ) const
	{
		return ( x != other.x ) || ( y != other.y ) || ( width != other.width ) || ( height != other.height );
	};

	// Dados
	union
	{
		struct
		{
			int32 x, y, width, height;
		};
		int32 viewport[4];
	};
};

#endif
