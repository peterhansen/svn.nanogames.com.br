/*
 *  Macros.h
 *  OGLESTest
 *
 *  Created by Daniel Lopes Alves on 9/23/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MACROS_H
#define MACROS_H

#import <UIKit/UIKit.h>
#import <CoreGraphics/CGGeometry.h>

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include "ApplicationManager.h"
#include "Utils.h"

// Para utilizarmos em métodos OpenGL nos quais 0 representa o cancelamento da operação, como
// em glBindTexture( GL_TEXTURE_2D, 0 )
#ifndef GL_NONE
	#define GL_NONE 0
#endif

// Comparação de números em ponto flutuante
// Para uma comparação aprimorada, ver:
// - http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.htm

#define FLOAT_EPSILON 0.00001f
#define FCMP( f1, f2 ) ( fabs( ( f1 ) - ( f2 ) ) <= FLOAT_EPSILON ? 0 : ( ( f1 ) > ( f2 ) ? 1 : -1 ) )

// ==
#define FEQL( f1, f2 ) ( FCMP( f1, f2 ) == 0 )

// !=
#define FDIF( f1, f2 ) ( FCMP( f1, f2 ) != 0 )

// <
#define FLTN( f1, f2 ) ( FCMP( f1, f2 ) == -1 )

// >
#define FGTN( f1, f2 ) ( FCMP( f1, f2 ) == 1 )

// <=
#define FLEQ( f1, f2 ) ( FCMP( f1, f2 ) <= 0 )

// >=
#define FGEQ( f1, f2 ) ( FCMP( f1, f2 ) >= 0 )

// M_PI * 2
#define TWO_PI 6.283185307179587f

// Determina se um número inteiro é par
#define INT_IS_EVEN( x ) ( ( ( x ) & 0x1 ) == 0 )

// Determina se um inteiro é uma potência de 2
#define IS_POW_OF_2( v ) ( !( ( v ) & ( ( v ) - 1)) && ( v ) )

// Deleta um ponteiro e coloca seu valor como NULL
#define DESTROY( p ) { delete p; p = NULL; }
#define DESTROY_VEC( p ) { delete[] p; p = NULL; }

// Testa se um ponteiro é nulo antes de deletá-lo
#define SAFE_DELETE( p ) { if( p ) DESTROY( p ) }
#define SAFE_DELETE_VEC( p ) { if( p ) DESTROY_VEC( p ) }

// Converte graus em radianos
#define DEGREES_TO_RADIANS( degrees ) ( ( ( degrees ) * M_PI ) / 180.0f )

// Converte radianos em graus
#define RADIANS_TO_DEGREES( radians ) ( ( 180.0f * ( radians ) ) / M_PI )

// Restringe o número x ao intervalo [min, max]
#define CLAMP( x, min, max ) ( ( x ) <= ( min ) ? ( min ) : ( ( x ) >= ( max ) ? ( max ) : ( x ) ) )

// Interpolação linear
#define LERP( percent, x, y ) ( ( x ) + ( ( percent ) * ( ( y ) - ( x ) )))

// Obtém o valor absoluto do valor
#define NANO_ABS( x ) ( ( x ) < 0 ? -( x ) : ( x ) )


#ifdef __OBJC__

// Testa se um handler é nulo antes de chamar sua mensagem release
#define KILL( h ) { [h release]; h = NULL; }
#define SAFE_RELEASE( h ) { if( h )KILL( h ) }

// Converte um número inteiro para o tipo NSString
#define INT_TO_NSSTRING( v ) [NSString stringWithFormat:@"%d", ( int32 )( v )]

#define NSSTRING_TO_CHAR_ARRAY( string ) [string UTF8String]

// Converte um array de caracteres para o tipo NSString
#define CHAR_ARRAY_TO_NSSTRING( charArray ) [NSString stringWithUTF8String:charArray]

// Converte std::string para NSString
#define STD_STRING_TO_NSSTRING( string ) [NSString stringWithCString:string.c_str()]

// Obtém a largura da tela
#define SCREEN_WIDTH [(( ApplicationManager* )APP_DELEGATE) getScreenWidth]
#define HALF_SCREEN_WIDTH ( SCREEN_WIDTH * 0.5f )

// Obtém a altura da tela
#define SCREEN_HEIGHT [(( ApplicationManager* )APP_DELEGATE) getScreenHeight]
#define HALF_SCREEN_HEIGHT ( SCREEN_HEIGHT * 0.5f )

// Obtém o viewport default para a aplicação
#define GET_DEFAULT_VIEWPORT() [(( ApplicationManager* )APP_DELEGATE) getDefaultViewport]

// Obtém o delegate da aplicação
#define APP_DELEGATE [ApplicationManager GetInstance]

// Funções de log para as versões debug
// TASK : Passar a usar char array !!!
#define LOG( s, ... ) NSLog( s, ##__VA_ARGS__ )
#define GLLOG() Utils::glLog( __FILE__, __LINE__ )

#define LOG_FREE_MEM( string ) LOG( @"Free Mem %s: %.4f", string, Utils::GetFreeMemory() / 1048576.0f );

#endif // #ifdef __OBJC__

#endif // #ifndef MACROS_H
