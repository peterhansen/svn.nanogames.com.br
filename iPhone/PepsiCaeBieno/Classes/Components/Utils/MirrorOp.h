/*
 *  MirrorOp.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/5/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef MIRROR_OP_H
#define MIRROR_OP_H

// TODOO : Talvez seja melhor utilizar:
// typedef uint8 MirrorOp;
// #define MIRROR_NONE	0x00
// #define MIRROR_HOR	0x01
// #define MIRROR_VER	0x02
// #define MIRROR_BOTH	0x03
// Assim poderíamos utilizar coisas do tipo: MIRROR_HOR | MIRROR_VER para "somar" operações

// Possíveis operações de espelhamento
typedef enum MirrorOp
{
	MIRROR_NONE = 0x00,
	MIRROR_HOR  = 0x01,
	MIRROR_VER  = 0x02,
	MIRROR_BOTH = 0x03

}MirrorOp;

#endif
