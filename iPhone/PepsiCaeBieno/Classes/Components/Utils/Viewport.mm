#include "Viewport.h"

#include "ApplicationManager.h"
#include "Macros.h"

/*==============================================================================================

MÉTODO apply
	Aplica o viewport levando em consideração a orientação do device.

==============================================================================================*/

void Viewport::apply( void )
{
	// TODOO : Utilizar os parâmetros corretos para cada uma das 4 orientações
	if( [[ApplicationManager GetInstance] isOrientationPortrait] )
	{
		glScissor( x, y + height - 1, width, height );
	}
	else
	{
		// Antes de Simplificar :
		// glScissor( SCREEN_HEIGHT - ( y + height - 1 ) - 1, SCREEN_WIDTH - ( x + width - 1 ) - 1, height, width );
		glScissor( SCREEN_HEIGHT - y - height, SCREEN_WIDTH - x - width, height, width );
	}
}
