//
//  ViewManager.h
//  iBob
//
//  Created by Daniel Lopes Alves on 10/10/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef VIEW_MANAGER_H
#define VIEW_MANAGER_H

#import <UIKit/UIKit.h>
#include "NanoTypes.h"

// Pré-definição de ViewManager para que possamos utilizar seu nome da definição de TransitionDelegate
@class ViewManager;

// Protocolo que informa ao delegate os eventos de uma transição
@protocol TransitionDelegate <NSObject>

	@optional
		- ( void )transitionDidStart: ( ViewManager* )manager;
		- ( void )transitionDidFinish:( ViewManager* )manager;
		- ( void )transitionDidCancel:( ViewManager* )manager;

	@required
		- ( void )performTransitionToView:( uint8 )newIndex;
@end

// Definição completa de ViewManager
@interface ViewManager : UIView
{
	@private
		// Informa se ViewManager está realizando uma transição de telas
		bool transitioning;
	
		// Indica se a aplicação estava tratando interações do usuário antes de iniciar a transição
		bool interactionWasEnabled;
	
		// Objeto que irá receber informações sobre as transições de views
		id<TransitionDelegate> delegate;
}

// Métodos get e set para obter e determinar o objeto que irá receber informações sobre as transições de views
@property (nonatomic, assign) id<TransitionDelegate> delegate;

// Informa se ViewManager está realizando uma transição de telas
@property (readonly, getter=isTransitioning) bool transitioning;

// Informa que ViewManager deve realizar uma nova transição de views
- ( void )transitionFromSubview:( UIView* )oldView toSubview:( UIView* )newView transition:( NSString* )transition direction:( NSString* )direction duration:( NSTimeInterval )duration;

// Cancela a transição corrente (caso haja)
- ( void )cancelTransition;

@end

#endif

