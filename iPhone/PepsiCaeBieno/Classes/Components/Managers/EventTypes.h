/*
 *  EventTypes.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/12/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EVENT_TYPES_H
#define EVENT_TYPES_H

#include "NanoTypes.h"

// Tipos de eventos que podem ser recebidos pela aplicação
typedef uint8 EventTypes;

#define EVENT_NONE			0x00
#define EVENT_SYSTEM		0x01
#define EVENT_KEY			0x02
#define EVENT_TOUCH			0x04
#define EVENT_ACCELEROMETER 0x08

typedef uint8 EventTypesSpecific;

#define EVENT_TOUCH_BEGAN		0x00
#define EVENT_TOUCH_MOVED		0x01
#define EVENT_TOUCH_ENDED		0x02
#define EVENT_TOUCH_CANCELED	0x03

#endif
