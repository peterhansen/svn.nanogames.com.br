#include "ApplicationManager.h"

#include "Macros.h"

// Implementação da classe
@implementation ApplicationManager

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hWindow, hViewManager;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/

//- ( void )applicationDidFinishLaunching:( UIApplication* )application
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS : Descomentar se for necessário personalizar
//- ( void )dealloc
//{	
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM quit
	Termina a aplicação.

==============================================================================================*/

- ( void )quit:( uint32 )error
{
#if DEBUG
	LOG( @"ERROR: Quitting with error: %d\n", error );
#endif
	
	UIApplication *app = [UIApplication sharedApplication];
	SEL selector = @selector( terminate );

	if( [app respondsToSelector:selector] )
	{
		[app performSelector:selector];
	}
	else
	{
		[self applicationWillTerminate: app];
		exit( 0 );
	}
}

/*==============================================================================================

MENSAGEM startLoading
	Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos
da aplicação.

==============================================================================================*/

-( void ) startLoading
{
	// Cria a tela de loading
	UIActivityIndicatorView* hLoadingIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)]; 

	if( [[ApplicationManager GetInstance] isOrientationLandscape] )
		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH)];
	else
		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT)];
	
	[hLoadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge]; 
	[hViewManager addSubview:hLoadingIndicator];
	[hLoadingIndicator startAnimating];
	[hLoadingIndicator release];
	
	// Timer que irá carregar as views do jogo
	[NSTimer scheduledTimerWithTimeInterval:0.001f target:self selector:@selector(loadAll) userInfo:NULL repeats:NO];
}

/*==============================================================================================

MENSAGEM getScreenWidth
	Retorna a largura da tela.

==============================================================================================*/

- ( float )getScreenWidth
{
	return [self isOrientationLandscape] ? [UIScreen mainScreen].bounds.size.height : [UIScreen mainScreen].bounds.size.width;
}

/*==============================================================================================

MENSAGEM getScreenHeight
	Retorna a altura da tela.

==============================================================================================*/

- ( float )getScreenHeight
{
	return [self isOrientationLandscape] ? [UIScreen mainScreen].bounds.size.width : [UIScreen mainScreen].bounds.size.height;
}

/*==============================================================================================

MENSAGEM getDefaultViewport
	Retorna o viewport padrão da aplicação.

==============================================================================================*/

- ( Viewport )getDefaultViewport
{
	Viewport v( 0, 0, static_cast<int32>( [self getScreenWidth] ), static_cast<int32>( [self getScreenHeight] ) );
	return v;
}

/*==============================================================================================

MENSAGEM getOrientation
	Retorna a orientação da aplicação.

==============================================================================================*/

- ( UIInterfaceOrientation )getOrientation
{
	return [[UIApplication sharedApplication]statusBarOrientation];
}

/*==============================================================================================

MENSAGEM isOrientationLandscape
	Indica se a orientação da aplicação pertence a família landscape.

==============================================================================================*/

- ( bool )isOrientationLandscape
{
	return UIInterfaceOrientationIsLandscape( [self getOrientation] );
}

/*==============================================================================================

MENSAGEM isOrientationPortrait
	Indica se a orientação da aplicação pertence a família portrait.

==============================================================================================*/

- ( bool )isOrientationPortrait
{
	return UIInterfaceOrientationIsPortrait( [self getOrientation] );
}

/*==============================================================================================

MENSAGEM GetInstance
	Retorna o controlador da aplicação.

==============================================================================================*/

+ ( ApplicationManager* ) GetInstance
{
	return ( ApplicationManager* )[[UIApplication sharedApplication] delegate];
}

/*==============================================================================================

MÉTODO loadViewFromXib
	Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente, é
necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController à view
propriamente dita.

==============================================================================================*/

- ( UIView* ) loadViewFromXib:( const char* )pViewName
{
	UIViewController* hViewController = [[UIViewController alloc] initWithNibName: CHAR_ARRAY_TO_NSSTRING( pViewName ) bundle:NULL];
	
	// Carrega a view já na orientação correta
	if( [self isOrientationLandscape] )
	{
		CGAffineTransform transform = hViewController.view.transform;
		CGRect bounds = CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT );
		CGPoint center = CGPointMake( bounds.size.height * 0.5f, bounds.size.width * 0.5f );

		hViewController.view.center = center;
		
		if( [self getOrientation] == UIInterfaceOrientationLandscapeRight )
			transform = CGAffineTransformRotate( transform, M_PI_2 );
		else
			transform = CGAffineTransformRotate( transform, -M_PI_2 );
		
		hViewController.view.transform = transform;
	}

	UIView* hView = [hViewController view];
	[hView retain];
	[hViewController release];
	return hView;
}

/*==============================================================================================

MÉTODO loadViewControllerFromXib
	Carrega um viewcontroller e sua view a partir de um arquivo .xib.

==============================================================================================*/

- ( UIViewController* ) loadViewControllerFromXib:( const char* )pViewControllerName
{
	UIViewController* hViewController = [[UIViewController alloc] initWithNibName: CHAR_ARRAY_TO_NSSTRING( pViewControllerName ) bundle:NULL];
	
	// Carrega a view já na orientação correta
	if( [self isOrientationLandscape] )
	{
		CGAffineTransform transform = hViewController.view.transform;
		CGRect bounds = CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT );
		CGPoint center = CGPointMake( bounds.size.height * 0.5f, bounds.size.width * 0.5f );

		hViewController.view.center = center;
		
		if( [self getOrientation] == UIInterfaceOrientationLandscapeRight )
			transform = CGAffineTransformRotate( transform, M_PI_2 );
		else
			transform = CGAffineTransformRotate( transform, -M_PI_2 );
		
		hViewController.view.transform = transform;
	}

	[hViewController retain];
	return hViewController;
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

- ( void )transitionDidStart:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

// Executa a transição da view atual para a view de índice passado como parâmetro
- ( void )performTransitionToView:( uint8 )newIndex
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

// Fim da implementação da classe
@end
