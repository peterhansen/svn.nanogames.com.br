#include "ResourceManager.h"

/*==============================================================================================

 MÉTODO loadTextureEmpty
	Cria uma textura 2D. Reaproveita o handler caso um resource com o mesmo identificador
já tenha sido alocado.
 
==============================================================================================*/

Texture2DHandler ResourceManager::GetTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	// TODOO TASK : Verificar se a textura já foi alocada!!!!!!!!!!
	
	Texture2D* pTex = new Texture2D();
	if( !pTex || !pTex->loadTextureInFile( pFileName, pTexDesc ))
	{
		SAFE_DELETE( pTex );
		// TODOO : Disparar uma exceção aki!!!!
	}

	return Texture2DHandler( pTex );
}

