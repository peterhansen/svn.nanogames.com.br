/*
 *  ResourceManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/26/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include "Texture2D.h"
#include "VertexSet.h"

class ResourceManager
{
	public:
		// Cria uma textura 2D. Reaproveita o handler caso um resource com o mesmo
		// identificador já tenha sido alocado
		// Exceções: InvalidArgumentException
		static Texture2DHandler GetTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc = NULL );
};

#endif
