#include "AccelerometerListener.h"
#include "AccelerometerManager.h"
#include "Exceptions.h"
#include "Macros.h"

// Valor padrão do peso dos valores antigos (todos os valores, exceto o último reportado
// pelos acelerômetros) utilizado no cálculo de médias
#define DEFAULT_AVG_OLD_VALUES_WEIGHT 0.9f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

AccelerometerListener::AccelerometerListener( AccelerometerReportMode mode, float booster, float gravityFilter )
		 : reportMode( mode ), accManagerIndex( -1 ), booster( booster ), gravityFilter( gravityFilter ), averageOldValuesWeight( DEFAULT_AVG_OLD_VALUES_WEIGHT )
{
	accManagerIndex = AccelerometerManager::GetInstance()->insertListener( this );
	if( accManagerIndex < 0 )
#if DEBUG
		throw ConstructorException( "AccelerometerListener::AccelerometerListener(): Unable to create object" );
#else
		throw ConstructorException();
#endif
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

AccelerometerListener::~AccelerometerListener( void )
{
	AccelerometerManager::GetInstance()->removeListener( accManagerIndex );
}

/*===========================================================================================
 
MÉTODO setAverageOldValuesWeight
	Determina o peso dos valores antigos (todos os valores, exceto o último reportado pelos
acelerômetros) utilizado no cálculo de médias.
 
============================================================================================*/

void AccelerometerListener::setAverageOldValuesWeight( float weight )
{
	weight = fabs( weight );
	averageOldValuesWeight = CLAMP( weight, 0.0f, 1.0f );
};
