#include "AccelerometerInterface.h"
#include "Point3f.h"

@implementation AccelerometerInterface

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize pListener;

/*===========================================================================================
 
MENSAGEM setUpdateInterval
	Determina a frequência com que o usuário deseja receber os eventos do acelerômetro.
 
============================================================================================*/

- ( void ) setUpdateInterval:( float )secs
{
	[[UIAccelerometer sharedAccelerometer] setUpdateInterval: secs];
}

/*===========================================================================================
 
MENSAGEM startListening
	Começa a receber os eventos do acelerômetro.
 
============================================================================================*/

- ( void ) startListening
{
	[[UIAccelerometer sharedAccelerometer] setDelegate:self];
}

/*===========================================================================================
 
MENSAGEM stopListening
	Pára de receber os eventos do acelerômetro.
 
============================================================================================*/

- ( void ) stopListening
{
	[[UIAccelerometer sharedAccelerometer] setDelegate:NULL];
}

/*===========================================================================================
 
MENSAGEM accelerometer: didAccelerate:
	Implementação do protocolo UIAccelerometerDelegate.
 
============================================================================================*/		

- ( void ) accelerometer:( UIAccelerometer* )accelerometer didAccelerate:( UIAcceleration* )acceleration
{
	Point3f aux = Point3f( [acceleration x], [acceleration y], [acceleration z] );
	if( pListener )
		pListener->setAccelerometerEvent( &aux );
}

@end

