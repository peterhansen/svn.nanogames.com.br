#include "AccelerometerManager.h"
#include "Macros.h"
#include "Exceptions.h"
#include "Polynomial.h"

// Inicializa as variáveis estáticas da classe
AccelerometerManager* AccelerometerManager::pSingleton = NULL;

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

AccelerometerManager::AccelerometerManager( float updateInterval )
					 : updateInterval( updateInterval ), hInterface( NULL ),
					   restPosition()//, TODOO capturingRestPos( false )
{
	hInterface = [[AccelerometerInterface alloc]init];
	if( !hInterface )
#if DEBUG	// TODOO : É necessário?! Será que o [[alloc]init] já dispara a exceção?!?!?
		throw ConstructorException( "AccelerometerManager::AccelerometerManager : Unable to create object" );
#else
		throw ConstructorException();
#endif
	[hInterface setListener: this];

	memset( listeners, 0, sizeof( AccelerometerListener* ) * ACC_MANAGER_N_LISTENERS );

	setUpdateInterval( updateInterval );
}
	
/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

AccelerometerManager::~AccelerometerManager( void )
{
	suspend();

	KILL( hInterface );
	
	// É uma boa prática anular os ponteiros que não serão mais utilizados
	memset( listeners, 0, sizeof( AccelerometerListener* ) * ACC_MANAGER_N_LISTENERS );
}

/*==============================================================================================

MÉTODO Create
	 Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance().
No entanto, isso poderia resultar em exceções disparadas no meio do código da aplicação, o que
impactaria o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
chamado no início da aplicação, já que o usuário dos componentes parte do princípio que sempre
GetInstance() sempre retornará um valor válido.

==============================================================================================*/
 
bool AccelerometerManager::Create( float updateInterval )
{
	if( !pSingleton )
		pSingleton = new AccelerometerManager( updateInterval );
	return pSingleton != NULL;
}

/*==============================================================================================

MÉTODO Destroy
	Deleta o singleton.

==============================================================================================*/

void AccelerometerManager::Destroy( void )
{
	SAFE_DELETE( pSingleton );
}

/*===========================================================================================
 
MÉTODO resume
	Começa a receber os eventos do acelerômetro.

============================================================================================*/	

void AccelerometerManager::resume( void )
{
	[hInterface startListening];
}

/*===========================================================================================
 
MÉTODO suspend
	Pára de receber os eventos do acelerômetro.

============================================================================================*/

void AccelerometerManager::suspend( void )
{
	[hInterface stopListening];
}

/*===========================================================================================
 
MÉTODO startRestPositionCapture
	Inicia a captura de valores de aceleração que serão utilizados para determinar a posição
de repouso do device.
 
============================================================================================*/

// TODOO
//void AccelerometerManager::startRestPositionCapture( void )
//{
//	capturingRestPos = true;
//	restPosSampleCounter = 0;
//}

/*===========================================================================================
 
MÉTODO stopRestPositionCapture
	Pára a captura de valores de aceleração que serão utilizados para determinar a posição
de repouso do device.
 
============================================================================================*/

// TODOO
//void AccelerometerManager::stopRestPositionCapture( void )
//{
//	capturingRestPos = false;
//}

/*===========================================================================================
 
MÉTODO insertListener
	Determina que um objeto deve começar a receber os eventos do acelerômetro.
 
============================================================================================*/

int16 AccelerometerManager::insertListener( AccelerometerListener* pNewListener )
{
	// Procura um slot vazio
	int16 i = 0;
	for( ; i < ACC_MANAGER_N_LISTENERS ; ++i )
	{
		if( listeners[i] == NULL )
		{
			listeners[i] = pNewListener;
			acceleration[i].set();
			
			for( uint8 j = 0 ; j < ACCELEROMETER_HISTORY_LEN ; ++j )
			{
				accelHistory[i][0][j] = 0.0f;
				accelHistory[i][1][j] = 0.0f;
				accelHistory[i][2][j] = 0.0f;
			}
			break;
		}
	}

	return i >= ACC_MANAGER_N_LISTENERS ? -1 : i;
}

/*===========================================================================================
 
MÉTODO removeListener
	Indica que um objeto deve parar de receber os eventos do acelerômetro.
 
============================================================================================*/

bool AccelerometerManager::removeListener( int16 index )
{
	// Testa se o índice é válido
	if(( index < 0 ) && ( index >= ACC_MANAGER_N_LISTENERS ))
		return false;
	
	listeners[ index ] = NULL;
	return true;
}

/*===========================================================================================
 
MÉTODO setUpdateInterval
	Determina a frequência com que o usuário deseja receber os eventos do acelerômetro.
 
============================================================================================*/

void AccelerometerManager::setUpdateInterval( float secs )
{
	updateInterval = secs;
	[hInterface setUpdateInterval:fabs( secs )];
}

/*===========================================================================================
 
MÉTODO setAccelerometerEvent
	Recebe a aceleração reportada pelos acelerômetros.
 
============================================================================================*/

void AccelerometerManager::setAccelerometerEvent( const Point3f* pAcceleration )
{
	// TODOO : Seria legal se esta captura fosse uma thread a parte...
//	if( capturingRestPos )
//	{
//		restPositionSamples[ restPosSampleCounter ] = *pAcceleration;
//		
//		if( restPosSampleCounter + 1 >= = REST_POS_N_SAMPLES )
//			onRestPositionCaptured();
//		
//		restPosSampleCounter = ( restPosSampleCounter + 1 ) % REST_POS_N_SAMPLES;
//	}
	
	Point3f direction;
	for( int16 i = 0 ; i < ACC_MANAGER_N_LISTENERS ; ++i )
	{
		if( listeners[i] == NULL )
			continue;
		
		direction.set();

		switch( listeners[i]->getReportMode() )
		{
			// Obtém apenas a aceleração relacionada à gravidade
			case ACC_REPORT_MODE_GRAVITY:
				direction = (( *pAcceleration ) * listeners[i]->getGravityFilter() ) + ( direction * ( 1.0f - listeners[i]->getGravityFilter() ));
				direction *= listeners[i]->getBooster();
				break;
				
			case ACC_REPORT_MODE_GRAVITY_VEC:
				direction = ( *pAcceleration ) * listeners[i]->getGravityFilter();
				direction.normalize();
				direction *= listeners[i]->getBooster();
				break;
				
			case ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC:
				{
					direction = ( *pAcceleration ) * listeners[i]->getGravityFilter();
					direction.normalize();
					
					setHistory( i, &direction );
					
					// Calcula a média
					Point3f average;
					getHistoryAverage( &average, i );

					direction = average * listeners[i]->getBooster();
				}
				break;

			// Obtém somente a aceleração relacionanda aos movimentos
			case ACC_REPORT_MODE_MOVEMENT:
				acceleration[i] = (( *pAcceleration ) * listeners[i]->getGravityFilter() ) + ( acceleration[i] * ( 1.0f - listeners[i]->getGravityFilter() ));
				direction = ( *pAcceleration - acceleration[i] ) * listeners[i]->getBooster();
				break;

			case ACC_REPORT_MODE_AVERAGE:
				{
					// Retira a força da gravidade
					acceleration[i] = (( *pAcceleration ) * listeners[i]->getGravityFilter() ) + ( acceleration[i] * ( 1.0f - listeners[i]->getGravityFilter() ));
					Point3f lastValue = *pAcceleration - acceleration[i];
					
					setHistory( i, &lastValue );
					
					// Calcula a média
					Point3f average;
					getHistoryAverage( &average, i );

					direction = average * listeners[i]->getBooster();
				}
				break;

			case ACC_REPORT_MODE_POLINOMIAL_2:
			case ACC_REPORT_MODE_POLINOMIAL_3:
			case ACC_REPORT_MODE_POLINOMIAL_4:
				{
					// Retira a força da gravidade
					acceleration[i] = (( *pAcceleration ) * listeners[i]->getGravityFilter() ) + ( acceleration[i] * ( 1.0f - listeners[i]->getGravityFilter() ));
					Point3f aux = *pAcceleration - acceleration[i];
					
					setHistory( i, &aux );

					Polynomial p( listeners[i]->getReportMode() - ACC_REPORT_MODE_POLINOMIAL_2 + 2 );
					const uint8 degree = p.getDegree();

					for( uint8 j = 0 ; j < 3 ; ++j )
					{
						#warning Tratamento de exceção em código que é utilizado muitas vezes por segundo pode diminuir o desempenho da aplicação

						try
						{
							Polynomial::leastSquares( accelHistory[i][j], accelHistoryCounter, ACCELEROMETER_HISTORY_LEN, degree, &p );
							direction.p[j] = p.evaluate( ACCELEROMETER_HISTORY_LEN - 1 );
						}
						catch( IllegalMathOperation& ex )
						{
							// Opa! Provavelmente não foi possível fazer o cálculo da regressão polinomial
							// com os dados atuais
						}
					}
					
					direction *= listeners[i]->getBooster();
				}
				break;
				
			// Obtém a aceleração sem filtragem
			case ACC_REPORT_MODE_RAW:
			default:
				direction = ( *pAcceleration ) * listeners[i]->getBooster();
				break;
	
		}
		listeners[i]->onAccelerate( &restPosition, &direction );
	}
}

/*===========================================================================================
 
MÉTODO setHistory
	Armazena o hitórico dos valores reportados pelos acelerômetros.
 
============================================================================================*/

// TODOO : No início, quando ainda temos muitos valores (0.0f, 0.0f, 0.0f), o certo seria
// preeencher de accelHistoryCounter até ( ACCELEROMETER_HISTORY_LEN - 1) com pNewAccelData.
// Assim, quando só tivéssemos um valor válido, a média seria este mesmo valor.

void AccelerometerManager::setHistory( uint8 listenerIndex, const Point3f* pNewAccelData )
{	
	// Armazena a aceleração filtrada no histórico
	accelHistory[ listenerIndex ][0][ accelHistoryCounter ] = pNewAccelData->x;
	accelHistory[ listenerIndex ][1][ accelHistoryCounter ] = pNewAccelData->y;
	accelHistory[ listenerIndex ][2][ accelHistoryCounter ] = pNewAccelData->z;

	accelHistoryCounter = ( accelHistoryCounter + 1 ) % ACCELEROMETER_HISTORY_LEN;
}

/*===========================================================================================
 
MÉTODO getHistoryAverage
	Obtém a média histórica dos valores reportados pelos acelerômetros.
 
============================================================================================*/

Point3f* AccelerometerManager::getHistoryAverage( Point3f* pAverage, uint8 listenerIndex ) const
{
	// Faz o somatório dos valores que serão utilizados na média
	for( uint16 i = 0 ; i < ACCELEROMETER_HISTORY_LEN ; ++i )
	{
		pAverage->x += accelHistory[ listenerIndex ][0][i];
		pAverage->y += accelHistory[ listenerIndex ][1][i];
		pAverage->z += accelHistory[ listenerIndex ][2][i];
	}
	
	// Retira o último valor recebido (mais rápido desse jeito do que colocar um if dentro
	// do for. ATENÇÃO POIS O BUFFER É CIRCULAR, ENTÃO O ÚLTIMO VALOR NEM SEMPRE ESTARÁ
	// NA ÚLTIMA POSIÇÃO DO VETOR)
	uint16 lastValueIndex = ( accelHistoryCounter + ACCELEROMETER_HISTORY_LEN - 1 ) % ACCELEROMETER_HISTORY_LEN; 
	Point3f lastValue(	accelHistory[ listenerIndex ][0][ lastValueIndex ],
						accelHistory[ listenerIndex ][1][ lastValueIndex ],
						accelHistory[ listenerIndex ][2][ lastValueIndex ] );

	*pAverage -= lastValue;

	// Calcula a média aritmética dos valores antigos
	*pAverage /= ( ACCELEROMETER_HISTORY_LEN - 1 );
	
	// Fornece um peso maior aos valores antigos
	float weight = listeners[ listenerIndex ]->getAverageOldValuesWeight();
	*pAverage = (( *pAverage ) * weight ) + ( lastValue * ( 1.0f - weight ));

	return pAverage;
}

