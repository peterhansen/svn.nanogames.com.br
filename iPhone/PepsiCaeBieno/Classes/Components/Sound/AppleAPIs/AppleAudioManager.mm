#include "AppleAudioManager.h"

#include "Macros.h"
#include "Exceptions.h"

#define BUFFER_SIZE_IN_BYTES 0x10000

// Inicializa as variáveis estáticas da classe
AppleAudioManager* AppleAudioManager::pSingleton = NULL;

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AppleAudioManager::AppleAudioManager( uint8 nAppSounds )
				  : paused( false ), wasPlayingBeforeSuspend( false ), freeSlots( 0 ), nAppSounds( nAppSounds ), volume( 1.0f ), pCurrAudioData( NULL )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

AppleAudioManager::~AppleAudioManager( void )
{
	stopSound();
	
	delete pCurrAudioData;
	pCurrAudioData = NULL;
}

/*==============================================================================================

MÉTODO Create
	 Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance().
No entanto, isso poderia resultar em exceções disparadas no meio do código da aplicação, o que
impactaria o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
chamado no início da aplicação, já que o usuário dos componentes parte do princípio que sempre
GetInstance() sempre retornará um valor válido.

==============================================================================================*/
 
bool AppleAudioManager::Create( uint8 nAppSounds )
{
	if( !pSingleton )
		pSingleton = new AppleAudioManager( nAppSounds );
	return pSingleton != NULL;
}

/*==============================================================================================

MÉTODO Destroy
	Deleta o singleton.

==============================================================================================*/

void AppleAudioManager::Destroy( void )
{
	SAFE_DELETE( pSingleton );
}

/*==============================================================================================

MÉTODO suspend
	Suspende a execução dos devices existentes.

==============================================================================================*/

bool AppleAudioManager::suspend( void )
{
	if( ( pCurrAudioData != NULL ) && !paused )
	{
		if( isPlaying() )
		{
			wasPlayingBeforeSuspend = true;

			OSStatus err = AudioQueuePause( pCurrAudioData->pQueue );
			if( err == 0 )
				paused = true;
			else
				return false;
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO resume
	Resume a execução dos devices existentes.

==============================================================================================*/

bool AppleAudioManager::resume( void )
{
	if( ( pCurrAudioData != NULL ) && paused )
	{
		if( wasPlayingBeforeSuspend )
		{
			OSStatus err = AudioQueueStart( pCurrAudioData->pQueue, NULL );
			if( err == 0 )
				paused = false;
			else
				return false;
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO isPlaying
	Indica se algum som está sendo tocado.

==============================================================================================*/

bool AppleAudioManager::isPlaying( void ) const
{
	if( pCurrAudioData == NULL )
		return false;

	UInt32 playing;
	UInt32 dataSize = sizeof( playing );
	AudioQueueGetProperty( pCurrAudioData->pQueue, kAudioQueueProperty_IsRunning, &playing, &dataSize );
	return playing != 0;
}

/*==============================================================================================

MÉTODO AudioQueueBufferCallback
	Controla a execução do áudio.

==============================================================================================*/

void AppleAudioManager::AudioQueueBufferCallback( void* inUserData, AudioQueueRef inAudioQueue, AudioQueueBufferRef inCompleteAudioQueueBuffer )
{
	AppleAudioManager* pMe = ( AppleAudioManager* ) inUserData;
    if( ( pMe->pCurrAudioData == NULL ) || ( !pMe->pCurrAudioData->running ) )
		return;

    UInt32 numBytes;
    UInt32 nPackets = pMe->pCurrAudioData->numPacketsToRead;

    OSStatus result = AudioFileReadPackets( pMe->pCurrAudioData->audioFile, false, &numBytes, pMe->pCurrAudioData->pPacketDescs, pMe->pCurrAudioData->currentPacket, &nPackets, inCompleteAudioQueueBuffer->mAudioData );
    if( result )
	{
		#if DEBUG
			LOG( @"AudioFileReadPackets failed: %d", result );
		#endif

		pMe->stopSound();
		return;
	}

    if( nPackets > 0 )
	{
        UInt32 trimFramesAtStart = 0;
        UInt32 trimFramesAtEnd = 0;

        inCompleteAudioQueueBuffer->mAudioDataByteSize = numBytes; 
        inCompleteAudioQueueBuffer->mPacketDescriptionCount = nPackets;

        if( pMe->pCurrAudioData->currentPacket == 0 )
		{
            // at the beginning -- need to trim priming frames
            trimFramesAtStart = pMe->pCurrAudioData->mPrimingFrames;
        } 

        pMe->pCurrAudioData->currentPacket += nPackets;

        if( ( pMe->pCurrAudioData->currentPacket == pMe->pCurrAudioData->totalPackets ) && pMe->pCurrAudioData->looping )
		{
            // at the end -- need to trim remainder frames
            trimFramesAtEnd = pMe->pCurrAudioData->mRemainderFrames;

            // reset read from the beginning again
            pMe->pCurrAudioData->currentPacket = 0;
        }

		result = AudioQueueEnqueueBufferWithParameters( inAudioQueue, inCompleteAudioQueueBuffer, ( pMe->pCurrAudioData->pPacketDescs ? nPackets : 0 ), pMe->pCurrAudioData->pPacketDescs, trimFramesAtStart, trimFramesAtEnd, 0, NULL, NULL, NULL );

		if( result )
		{
			#if DEBUG
				LOG( @"AudioQueueBufferCallback( void*, AudioQueueRef, AudioQueueBufferRef ) - AudioQueueEnqueueBufferWithParameters failed: %d", result );
			#endif
			pMe->stopSound();
			return;
		}
    }
	else
	{
		pMe->stopSound();
	}
}

/*==============================================================================================

MÉTODO setVolume
	Determina o volume dos sons da aplicação.

==============================================================================================*/

void AppleAudioManager::setVolume( float volume )
{
	this->volume = volume;

	if( pCurrAudioData )
		AudioQueueSetParameter( pCurrAudioData->pQueue, kAudioQueueParam_Volume, volume );
}

/*==============================================================================================

MÉTODO insertAudio
	Cria um novo buffer contendo os dados do arquivo localizado em pAudioPath.

==============================================================================================*/

//bool AppleAudioManager::insertAudio( const char* pAudioPath, uint8 *pAudioIndex )
//{
//	if( freeSlots >= nAppSounds )
//		return false;
//	
//	// TODOO
//	// ...
//
//	return true;
//}

/*==============================================================================================

MÉTODO playSound
	Executa um som da aplicação.

==============================================================================================*/

void AppleAudioManager::playSound( uint8 soundIndex, bool looping, bool stopPrevious )
{
	if( pCurrAudioData != NULL )
	{
		if( !stopPrevious )
			return;
		
		stopSound();
		
		delete pCurrAudioData;
		pCurrAudioData = NULL;
	}
	
	// Abre o arquivo de som
	pCurrAudioData = new AudioQueueData( looping );
	
	char fileName[] = "snd0";
	fileName[3] = '0' + soundIndex;

	AudioFileOpenURL( ( CFURLRef )[NSURL fileURLWithPath:Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( fileName ), @"caf" )], 0x01/*fsRdPerm*/, 0/*inFileTypeHint*/, &( pCurrAudioData->audioFile ) );
	
	// Obtém o formato do arquivo
	UInt32 size = sizeof( pCurrAudioData->dataFormat );
	AudioFileGetProperty( pCurrAudioData->audioFile, kAudioFilePropertyDataFormat, &size, &( pCurrAudioData->dataFormat ) );
	
	// Agenda a reprodução do som
	AudioQueueNewOutput( &( pCurrAudioData->dataFormat ), AudioQueueBufferCallback, this, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &( pCurrAudioData->pQueue ) );

	// We have a couple of things to take care of now
	// (1) Setting up the conditions around VBR or a CBR format - affects how we will read from the file
	// If format is VBR we need to use a packet table
	UInt32 bufferSize = BUFFER_SIZE_IN_BYTES;
	if( pCurrAudioData->dataFormat.mBytesPerPacket == 0 || pCurrAudioData->dataFormat.mFramesPerPacket == 0 )
	{
		// first check to see what the max size of a packet is - if it is bigger
		// than our allocation default size, that needs to become larger
		UInt32 maxPacketSize;
		size = sizeof( maxPacketSize );
		AudioFileGetProperty( pCurrAudioData->audioFile, kAudioFilePropertyPacketSizeUpperBound, &size, &maxPacketSize );
		if( maxPacketSize > bufferSize ) 
			bufferSize = maxPacketSize;

		pCurrAudioData->numPacketsToRead = bufferSize / maxPacketSize;
		
		// We also need packet descpriptions for the file reading
		pCurrAudioData->pPacketDescs = new AudioStreamPacketDescription[ pCurrAudioData->numPacketsToRead ];
	}
	else
	{
		pCurrAudioData->numPacketsToRead = bufferSize / pCurrAudioData->dataFormat.mBytesPerPacket;
		pCurrAudioData->pPacketDescs = NULL;
	}

	size = sizeof( pCurrAudioData->totalPackets );
	AudioFileGetProperty( pCurrAudioData->audioFile, kAudioFileStreamProperty_AudioDataPacketCount, &size, &( pCurrAudioData->totalPackets ) );

	// (2) If the file has a cookie, we should get it and set it on the AudioQueue
	size = sizeof( UInt32 );
	OSStatus result = AudioFileGetPropertyInfo( pCurrAudioData->audioFile, kAudioFilePropertyMagicCookieData, &size, NULL );
	if( !result && size )
	{
		char* cookie = new char[ size ];
		AudioFileGetProperty( pCurrAudioData->audioFile, kAudioFilePropertyMagicCookieData, &size, cookie );
		AudioQueueSetProperty( pCurrAudioData->pQueue, kAudioQueueProperty_MagicCookie, cookie, size );
		delete[] cookie;
	}
	
	// Channel layout?
	OSStatus err = AudioFileGetPropertyInfo( pCurrAudioData->audioFile, kAudioFilePropertyChannelLayout, &size, NULL );
	if( err == noErr && size > 0 )
	{
		AudioChannelLayout* acl = ( AudioChannelLayout* )malloc( size );
		AudioFileGetProperty( pCurrAudioData->audioFile, kAudioFilePropertyChannelLayout, &size, acl );
		AudioQueueSetProperty( pCurrAudioData->pQueue, kAudioQueueProperty_ChannelLayout, acl, size );
		free( acl );
	}
	
	AudioFilePacketTableInfo packetTableInfo;
	size = sizeof( packetTableInfo );
	AudioFileGetProperty( pCurrAudioData->audioFile, kAudioFilePropertyPacketTableInfo, &size, &packetTableInfo );

	pCurrAudioData->mPrimingFrames = packetTableInfo.mPrimingFrames;
	pCurrAudioData->mRemainderFrames = packetTableInfo.mRemainderFrames;

	// Prime the queue with some data before starting
	AudioQueueAllocateBuffer( pCurrAudioData->pQueue, bufferSize, &( pCurrAudioData->pBuffer ) );
	
	// We need to enqueue a buffer after the queue has started
	pCurrAudioData->running = true;
	AudioQueueBufferCallback( this, pCurrAudioData->pQueue, pCurrAudioData->pBuffer );
	
	// Determina o volume do som que será tocado
	setVolume( volume );

	// lets start playing now - stop is called in the AQTestBufferCallback when there's
	// no more to read from the file
	AudioQueueStart( pCurrAudioData->pQueue, NULL );
	
//	// Espera a execução do som terminar
//	do
//	{
//		CFRunLoopRunInMode( kCFRunLoopDefaultMode, 0.25f, false );
//	}
//	while( pCurrAudioData->running );
//
//	// Garante que houve tempo para o shut down
//	CFRunLoopRunInMode( kCFRunLoopDefaultMode, 1.0f, false );
//	
//	delete pCurrAudioData;
//	pCurrAudioData = NULL;
}

/*==============================================================================================

MÉTODO stopSound
	Pára a execução de um som da aplicação.

==============================================================================================*/

// TODOO: Este método deve ser controlado com semáforos!!!!!!!!!! Afinal, ele pode ser chamado
// pela thread principal e pela thread que controla a reprodução de sons
void AppleAudioManager::stopSound( void )
{
	if( pCurrAudioData )
	{
		OSStatus result = AudioQueueStop( pCurrAudioData->pQueue, YES );
		if( result )
		{
			#if DEBUG
				LOG( @"AppleAudioManager::stopSound() stop failed: %d", ( int )result );
			#endif
		}
		
		pCurrAudioData->running = false;
	}
}

