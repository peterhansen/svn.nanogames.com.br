/*
 *  PlaybackAudioDevice.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/15/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef PLAYBACK_AUDIO_DEVICE_H
#define PLAYBACK_AUDIO_DEVICE_H

#include "AudioDevice.h"
#include "AudioContext.h"
#include "AudioListener.h"

// Número máximo de AudioContexts que podem ser manipulados por um AudioDevice
#define MAX_DEVICE_CONTEXTS 5

class PlaybackAudioDevice : public AudioDevice
{
	public:
		// Construtor
		PlaybackAudioDevice( void );
	
		// Destrutor
		virtual ~PlaybackAudioDevice( void );

		// Obtém o device que controla o dispositivo de áudio
		inline ALCdevice* getOpenALDevice( void ) const { return pDevice; };
	
		// Obtém um novo AudioContext para este AudioDevice
		AudioContext* newAudioContext( uint8* pIndex = NULL );
	
		// Obtém um AudioContext deste device 
		AudioContext* getAudioContext( uint8 index ) const;
	
		// Retorna o objeto que representa o receptor dos sons no mundo
		inline AudioListener* getAudioListener( void ) const { return pListener; };
	
		// Suspende a execução do device
		virtual bool suspend( void ) const;
	
		// Resume a execução dos device
		virtual bool resume( void ) const;
	
		// Determina o modelo matemático a ser usado na atenuação por distância. Seu valor padrão
		// é AL_INVERSE_DISTANCE_CLAMPED
		bool setDistanceModel( ALenum model ) const;
	
	private:
		// AudioContexts controlados por este AudioDevice
		AudioContext* contexts[ MAX_DEVICE_CONTEXTS ];
	
		// Objeto que representa o receptor dos sons no mundo
		AudioListener* pListener;
};

#endif