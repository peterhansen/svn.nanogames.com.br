/*
 *  AudioManager.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/2/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#include "CaptureAudioDevice.h"
#include "PlaybackAudioDevice.h"

class AudioManager
{
	public:
		// Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance(). No entanto, isso
		// poderia resultar em exceções disparadas no meio do código da aplicação, o que impactaria
		// o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
		// chamado no início da aplicação, já que o usuário dos componentes parte do princípio que
		// sempre GetInstance() sempre retornará um valor válido 
		// Exceções : OpenALException
		static bool Create( void );
	
		// Deleta o singleton
		static void Destroy( void );
	
		// Retorna a instância do objeto singleton
		static AudioManager* GetInstance( void ) { return pSingleton; };
	
		// Retorna o device de reprodução de sons
		PlaybackAudioDevice* getPlaybackDevice( void ) const { return pPlaybackDevice; };
	
		// Retorna o device de captura de sons
		CaptureAudioDevice* getCaptureDevice( void ) const { return pCaptureDevice; };

		// Indica se a plataforma suporta captura de áudio
		bool supportsCapture( void );

		// Indica se a plataforma suporta efeitos sonoros
		bool supportsReverbAndEffects( void );
	
		// Suspende a execução dos devices existentes
		bool suspend( void ) const;
	
		// Resume a execução dos devices existentes
		bool resume( void ) const;
	
	private:
		// Contrutor
		// Exceções : OpenALException
		AudioManager( void );
	
		// Destrutor
		~AudioManager( void );

		// Device de playback de sons
		PlaybackAudioDevice* pPlaybackDevice;
	
		// Device de captura de sons
		CaptureAudioDevice* pCaptureDevice;
	
		// Única instância da classe
		static AudioManager* pSingleton;
};

#endif