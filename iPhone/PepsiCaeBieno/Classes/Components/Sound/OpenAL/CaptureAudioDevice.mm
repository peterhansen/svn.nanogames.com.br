#include "CaptureAudioDevice.h"
#include "Exceptions.h"
#include "Macros.h"

// Frequência da amostragem da captura
#define	CAPTURE_SAMPLE_RATE	44100

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

CaptureAudioDevice::CaptureAudioDevice( uint8 maxCaptureTime ) : AudioDevice(), pCaptureDataBuffer( NULL )
{
	pDevice = alcCaptureOpenDevice( NULL, CAPTURE_SAMPLE_RATE, AL_FORMAT_MONO16, maxCaptureTime * CAPTURE_SAMPLE_RATE );
	if( !pDevice )
#if DEBUG
		throw OpenALException( "CaptureAudioDevice::CaptureAudioDevice()" );
#else
		throw OpenALException();
#endif

	// << 1 => Qualidade mono de 16 bits ( AL_FORMAT_MONO16 )
	pCaptureDataBuffer = new uint8[ maxCaptureTime * ( CAPTURE_SAMPLE_RATE << 1 ) ];
	
	if( !pCaptureDataBuffer )
	{
		// Libera o device. Este chamada é necessária, pois o destrutor desta classe não será chamado, já que a construção
		// do objeto ficará incompleta devido ao lançamento da exceção abaixo. Para maiores informações, leia o livro do
		// Bjarne Stroustroup
		alcCaptureCloseDevice( pDevice );
		
#if DEBUG
		throw OpenALException( "CaptureAudioDevice::CaptureAudioDevice()" );
#else
		throw OpenALException();
#endif
	}
}
	
/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

CaptureAudioDevice::~CaptureAudioDevice( void )
{
	SAFE_DELETE( pCaptureDataBuffer );
	alcCaptureCloseDevice( pDevice );
}

/*==============================================================================================

MÉTODO resume
	Inicia a captura de dados.

==============================================================================================*/

bool CaptureAudioDevice::resume( void ) const
{
	// Limpa o código de erro
	alcGetError( pDevice );
	
	alcCaptureStart( pDevice );
	
	return alcGetError( pDevice ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO suspend
	Pára a captura de dados.

==============================================================================================*/

bool CaptureAudioDevice::suspend( void ) const
{
	// Limpa o código de erro
	alcGetError( pDevice );
	
	alcCaptureStop( pDevice );
	
	return alcGetError( pDevice ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO getCapturedSamples
	Obtém os dados capturados. Retorna NULL em casos de erro.

==============================================================================================*/

uint8* CaptureAudioDevice::getCapturedSamples( int32* pNSamples )
{
	// Limpa o código de erro
	alcGetError( pDevice );

	// Obtém o número de amostras capturadas até o momento
	alcGetIntegerv( pDevice, ALC_CAPTURE_SAMPLES, 1, pNSamples );

	// Obtém os dados das amostras
	alcCaptureSamples( pDevice, pCaptureDataBuffer, *pNSamples );

	return alcGetError( pDevice ) == AL_NO_ERROR ? pCaptureDataBuffer : NULL;
}
 
