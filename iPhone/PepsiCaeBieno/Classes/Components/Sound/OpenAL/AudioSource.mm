#include "AudioSource.h"
#include "Exceptions.h"
#include "Macros.h"

// Valores a serem utilizados em AudioSource::ExecSourceFunction( int )
#define AL_COMMAND_PLAY		0
#define AL_COMMAND_PAUSE	1
#define AL_COMMAND_STOP		2
#define AL_COMMAND_REWIND	3

// TASK : Fazer métodos para determinar / obter estes atributos
// - AL_PITCH 
// - AL_CONE_OUTER_GAIN 
// - AL_CONE_INNER_ANGLE 
// - AL_CONE_OUTER_ANGLE
// - AL_SOURCE_TYPE
// - AL_BUFFERS_PROCESSED
// - AL_SEC_OFFSET
// - AL_SAMPLE_OFFSET
// - AL_BYTE_OFFSET

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

AudioSource::AudioSource( AudioContext* pContext ) : sourceId( 0 ), pParentContext( pContext )
{
	// Limpa o código de erro
	alGetError();
	
	// Obtém um id para a fonte de áudio
	alGenSources( 1, &sourceId );
	
	// Verifica se houve erros durante a operação
	if( alGetError() != AL_NO_ERROR )
#if DEBUG
		throw OpenALException( "AudioSource::AudioSource()" );
#else
		throw OpenALException();
#endif
	
	// Inicializa o vetor de buffers
	memset( buffers, NULL, sizeof( AudioSource* ) * MAX_AUDIO_BUFFERS_PER_SOURCE );
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

AudioSource::~AudioSource( void )
{
	// Chama alSourceStop automaticamente caso a fonte esteja tocando o áudio
	alDeleteSources( 1, &sourceId );
	
	for( uint8 i = 0 ; i < MAX_AUDIO_BUFFERS_PER_SOURCE ; ++i )
		SAFE_DELETE( buffers[i] );

	pParentContext = NULL;
}

/*==============================================================================================

MÉTODO getState
	Retorna o estado da fonte de aúdio.

==============================================================================================*/

ALint AudioSource::getState( void ) const
{
	ALint state;
	if( GetSourceI( AL_SOURCE_STATE, &state ) == AL_NO_ERROR )
		return state;
	return AL_NONE;
}

/*==============================================================================================

MÉTODO insertAudioBuffer
	Adiciona um buffer de áudio à fonte.

==============================================================================================*/

bool AudioSource::insertAudioBuffer( AudioBufferAL* pBuffer, uint8* pIndex )
{
	// Verifica se possui um índice livre
	uint8 i = 0;
	for( ; i < MAX_AUDIO_BUFFERS_PER_SOURCE ; ++i )
	{
		if( buffers[i] == NULL )
			break;
	}
	
	if( i >= MAX_AUDIO_BUFFERS_PER_SOURCE )
	{
		#if DEBUG
			LOG( @"AudioSource::insertAudioBuffer(): o numero de buffers ja chegou ao maximo permitido\n" );
		#endif
		return false;
	}

	// Limpa o código de erro
	alGetError();
	
	// Adiciona o buffer à fonte
	const ALuint aux = pBuffer->getBufferId();
	alSourceQueueBuffers( sourceId, 1, &aux );
	
	// Verifica se houve erros
#if DEBUG
	ALenum error = alGetError();
	if( error != AL_NO_ERROR )
		return false;
#else
	if( alGetError() != AL_NO_ERROR )
		return false;
#endif

	if( pIndex )
		*pIndex = i;

	buffers[ i ] = pBuffer;
	pBuffer->increaseRefCount();

	return true;
}

/*==============================================================================================

MÉTODO removeAudioBuffer
	Remove um buffer de áudio da fonte. Retorna o buffer se conseguir executar a remoção ou NULL
em caso de falhas.

==============================================================================================*/

AudioBufferAL* AudioSource::removeAudioBuffer( uint8 index )
{
#if DEBUG
	if( index >= MAX_AUDIO_BUFFERS_PER_SOURCE )
	{
		LOG( @"AudioSource::removeAudioBuffer - Indice nao existente\n" );
		return NULL;
	}
#endif
	
	// Limpa o código de erro
	alGetError();
	
	// Remove o buffer da fonte
	AudioBufferAL* pBuffer = buffers[ index ];
	ALuint aux = pBuffer->getBufferId();
	alSourceUnqueueBuffers( sourceId, 1, &aux );
	
	// Verifica se houve erros
	if( alGetError() != AL_NO_ERROR )
		return NULL;

	buffers[ index ] = NULL;

	pBuffer->decreaseRefCount();	
	return pBuffer;
}

/*==============================================================================================

MÉTODO play
	Inicia a execução da sequência de áudios ligados à source.

==============================================================================================*/

bool AudioSource::play( void ) const
{
	return ExecSourceFunction( AL_COMMAND_PLAY ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO pause
	Suspende a execução da sequência de aúdios ligados à source.

==============================================================================================*/

bool AudioSource::pause( void ) const
{
	return ExecSourceFunction( AL_COMMAND_PAUSE ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO stop
	Pára a execução da sequência de aúdios ligados à source.

==============================================================================================*/

bool AudioSource::stop( void ) const
{
	return ExecSourceFunction( AL_COMMAND_STOP ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO rewind
	Reinicia a execução da sequência de aúdios ligados à source.

==============================================================================================*/

bool AudioSource::rewind( void ) const
{
	return ExecSourceFunction( AL_COMMAND_REWIND ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setLooping
	Indica que a sequência de áudios deve ser executada em loop.

==============================================================================================*/

bool AudioSource::setLooping( bool loop ) const
{
	return SetSourceI( AL_LOOPING, loop ? AL_TRUE : AL_FALSE ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO isLooping
	Retorna se a sequência de áudios está sendo executada em loop.

==============================================================================================*/

bool AudioSource::isLooping( void ) const
{
	ALint looping;
	if( GetSourceI( AL_LOOPING, &looping ) == AL_NO_ERROR )
		return looping != AL_FALSE;
	return false;
}

/*==============================================================================================

MÉTODO setRelativeToListener
	Indica se os atributos "position", "speed", "cone" e "direction" devem ser interpretados
relativos à posição do listener.

==============================================================================================*/

bool AudioSource::setRelativeToListener( bool b ) const
{
	return SetSourceI( AL_SOURCE_RELATIVE, b ? AL_TRUE : AL_FALSE ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setPosition
	Determina a posição da fonte de áudio.

==============================================================================================*/

bool AudioSource::setPosition( const Point3f* pPos ) const
{
	return SetSourceFV( AL_POSITION, pPos->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setSpeed
	Determina a velocidade da fonte de áudio.

==============================================================================================*/

bool AudioSource::setSpeed( const Point3f* pVel ) const
{
	return SetSourceFV( AL_VELOCITY, pVel->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setDirection
	Determina a direção do áudio emitido pela fonte.

==============================================================================================*/

bool AudioSource::setDirection( const Point3f* pDir ) const
{
	return SetSourceFV( AL_DIRECTION, pDir->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setGain
	Determina o volume atual da fonte de áudio.

==============================================================================================*/

bool AudioSource::setGain( float gain ) const
{
	return SetSourceF( AL_GAIN, gain ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setMinGain
	Determina o volume mínimo da fonte de áudio.

==============================================================================================*/

bool AudioSource::setMinGain( float minGain ) const
{
	return SetSourceF( AL_MIN_GAIN, minGain ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setMaxGain
	 Determina o volume máximo da fonte de áudio.

==============================================================================================*/

bool AudioSource::setMaxGain( float maxGain ) const
{
	return SetSourceF( AL_MAX_GAIN, maxGain ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setMaxDistance
	Retorna o volume máximo da fonte de áudio.

==============================================================================================*/

bool AudioSource::getMaxGain( float* pMaxGain ) const
{
	return AudioSource::GetSourceF( AL_MAX_GAIN, pMaxGain ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setMaxDistance
	Determina qual a maior distância que a fonte pode ficar do listener.

==============================================================================================*/

bool AudioSource::setMaxDistance( float maxDistance ) const
{
	return SetSourceF( AL_MAX_DISTANCE, maxDistance ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setRefDistance
	Determina a distância de referência em relação ao listener.

==============================================================================================*/

bool AudioSource::setRefDistance( float referenceDistance ) const
{
	return SetSourceF( AL_REFERENCE_DISTANCE, referenceDistance ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setRollOffFactor
	Determina o fator de atenuação por distância. Seu valor padrão é 1.0f. 0.0f indica que não há
atenuação por distância.

==============================================================================================*/

bool AudioSource::setRollOffFactor( float rollOffFactor ) const
{
	return SetSourceF( AL_ROLLOFF_FACTOR, rollOffFactor ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO SetSourceI
	Executa o método alSourcei sobre esta fonte de áudio.

==============================================================================================*/

ALint AudioSource::SetSourceI( ALenum param, ALint value ) const
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alSourcei( sourceId, param, value );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO GetSourceI
	Executa o método alGetSourcei sobre esta fonte de áudio.

==============================================================================================*/

ALint AudioSource::GetSourceI( ALenum param, ALint* pValue ) const
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alGetSourcei( sourceId, param, pValue );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO SetSourceF
	Executa o método alSourcef sobre esta fonte de áudio.

==============================================================================================*/

ALint AudioSource::SetSourceF( ALenum param, ALfloat value ) const
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alSourcef( sourceId, param, value );
	
	// Retorna se houve erros
#if DEBUG
	ALenum error = alGetError();
	return error;
#else
	return alGetError();
#endif
}

/*==============================================================================================

MÉTODO GetSourceF
	Executa o método alSourcef sobre esta fonte de áudio.

==============================================================================================*/

ALint AudioSource::GetSourceF( ALenum param, ALfloat* pValue ) const
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alGetSourcef( sourceId, param, pValue );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO SetSourceFV
	Executa o método alSourcefv sobre esta fonte de áudio.

==============================================================================================*/

ALint AudioSource::SetSourceFV( ALenum param, const ALfloat* pValues ) const
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alSourcefv( sourceId, param, pValues );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO GetSourceFV
	Executa o método alGetSourcefv sobre esta fonte de áudio.

==============================================================================================*/

ALint AudioSource::GetSourceFV( ALenum param, ALfloat* pValues ) const
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alGetSourcefv( sourceId, param, pValues );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO ExecSourceFunction
	Executa uma das seguintes funções: alSourcePlay, alSourcePause, alSourceStop e alSourceRewind.

==============================================================================================*/

ALint AudioSource::ExecSourceFunction( uint8 code ) const
{
	// Limpa o código de erro
	alGetError();
	
	switch( code )
	{
		case AL_COMMAND_PLAY:
			alSourcePlay( sourceId );
			break;
			
		case AL_COMMAND_PAUSE:
			alSourcePause( sourceId );
			break;
			
		case AL_COMMAND_STOP:
			alSourceStop( sourceId );
			break;
			
		case AL_COMMAND_REWIND:
			alSourceRewind( sourceId );
			break;
	}
	
	// Retorna se houve erros
	return alGetError();
}
