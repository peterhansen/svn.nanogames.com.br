#include "Matrix.h"
#include "Macros.h"
#include "Utils.h"
#include "Exceptions.h"

#include <string.h>

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix::Matrix( uint8 rows, uint8 columns, const float *pElements ) : nRows( CLAMP( rows, 1, 254 ) ), nColumns( CLAMP( columns, 1, 254 ) )
{	
	build();
	setElements( pElements );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix::Matrix( uint8 rows, uint8 columns, float value ) : nRows( CLAMP( rows, 1, 254 ) ), nColumns( CLAMP( columns, 1, 254 ) )
{
	build();
	setElements( value );
}

/*==============================================================================================

CONSTRUTOR DE CÓPIA
 
==============================================================================================*/

Matrix::Matrix( const Matrix& m ) : nRows( m.nRows ), nColumns( m.nColumns )
{		
	build();
	setElements( m.pMtx );
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Matrix::~Matrix( void )
{
	SAFE_DELETE_VEC( pMtx );
}

/*==============================================================================================

MÉTODO set
	Determina os elementos da matriz.

==============================================================================================*/

inline void Matrix::setElements( const float* pElements )
{
	if( pElements )
		memcpy( pMtx, pElements, sizeof( float ) * ( nRows * nColumns ) );
}

/*==============================================================================================

MÉTODO set
	Inicializa todos os elementos da matriz com o valor recebido como parâmetro.

==============================================================================================*/

inline void Matrix::setElements( float value )
{
	memset( pMtx, value, sizeof( float ) * ( nRows * nColumns ) );
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

inline void Matrix::build( void )
{
	pMtx = new float[ nRows * nColumns ];
	if( !pMtx )
#if DEBUG	// TODO : É necessário?! Será que o new já dispara a exceção?!?!?
		throw OutOfMemoryException( "Não foi possível criar a matriz" );
#else
		throw OutOfMemoryException();
#endif
}

/*==============================================================================================

MÉTODO isIdentity
	Indica se esta matriz é uma matriz identidade.

==============================================================================================*/

bool Matrix::isIdentity( void ) const
{
	if( !isSquare() )
		return false;
	
	// TASK : Será que é mais rápido separar em 3 FORs?
	// 0 - Diagonal Principal
	// 1 - Triângulo superior
	// 2 - Triângulo inferior
	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )
		{
			const float d = ( *this )( row, column );
			if( row == column )
			{
				if( FDIF( d, 1.0 ) )
					return false;
			}
			else if( FDIF( d, 0.0 ) )
			{
				return false;
			}
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO setIdentity
	Transforma a matriz em uma matriz identidade.

==============================================================================================*/

void Matrix::setIdentity( void )
{
	if( !isSquare() )
#if DEBUG
		throw IllegalMathOperation( "A matriz não é quadrada, logo não pode ser transformada em uma matriz identidade" );
#else
		throw IllegalMathOperation();
#endif

	memset( pMtx, 0, sizeof( float ) * ( nRows * nColumns ) );
	
	for( uint8 row = 0 ; row < nRows ; ++row )
		( *this )( row, row ) = 1.0;
}

/*==============================================================================================

MÉTODO determinant
	Retorna o determinante da matriz.
	- http://people.richland.edu/james/lecture/m116/matrices/determinant.html

==============================================================================================*/

float Matrix::determinant( void ) const
{
	// Se a matriz não é quadrada, não possui determinante
	if( !isSquare() )
#if DEBUG
		throw IllegalMathOperation( "Não é possível calcular o determinante de uma matriz não-quadrada" );
#else
		throw IllegalMathOperation();
#endif
	
	// Chama a rotina recursiva
	return determinant( this );
}

/*==============================================================================================

MÉTODO determinant
	Rotina recursiva para cálculo do determinante. Parte do pricípio que a matriz recebida como
parâmetro é quadrada.
	Referência:
		- http://people.richland.edu/james/lecture/m116/matrices/determinant.html

==============================================================================================*/

float Matrix::determinant( const Matrix* pMatrix )
{
	const uint8 rows = pMatrix->getNRows();

	// Verifica se a matriz chegou em algum caso base
	// OBS: Já sabemos que é quadrada, então não precisamos checar se columns == rows
	switch( rows )
	{
		case 1:
			return ( *pMatrix )( 0, 0 );

		case 2:
			return ( ( *pMatrix )( 0, 0 ) * ( *pMatrix )( 1, 1 ) ) - ( ( *pMatrix )( 0, 1 ) * ( *pMatrix )( 1, 0 ) );
		
		case 3:
			#define A ( *pMatrix )( 0, 0 )
			#define B ( *pMatrix )( 0, 1 )
			#define C ( *pMatrix )( 0, 2 )
			#define D ( *pMatrix )( 1, 0 )
			#define E ( *pMatrix )( 1, 1 )
			#define F ( *pMatrix )( 1, 2 )
			#define G ( *pMatrix )( 2, 0 )
			#define H ( *pMatrix )( 2, 1 )
			#define I ( *pMatrix )( 2, 2 )

			return ( A * E * I ) + ( B * F * G ) + ( C * D * H ) - ( A * F * H ) - ( B * D * I ) - ( C * E * G );

			#undef A
			#undef B
			#undef C
			#undef D
			#undef E
			#undef F
			#undef G
			#undef H
			#undef I
	
		default:
			{
				const uint8 columns = pMatrix->getNColumns();
		
				// Cria uma matriz auxiliar com as dimensões reduzidas
				Matrix aux( rows - 1, columns - 1 );

				// Verifica que linha / coluna iremos retirar
				int16 betterChoice;
				float det = 0.0, mul;
				
				// OBS: Talvez só faça sentido fazer esta verificação se a matriz for maior que 4x4... 
				if( pMatrix->getBetterChoice( &betterChoice ) )
				{
					int8 sign = INT_IS_EVEN( betterChoice ) ? 1 : -1;

					for( uint8 column = 0 ; column < columns ; ++column )	
					{
						mul = ( *pMatrix )( betterChoice, column );
						if( FCMP( mul, 0.0 ) )
						{
							reduce( pMatrix, &aux, betterChoice, column );
							det += sign * mul * determinant( &aux );
						}
						sign *= -1;
					}
				}
				else
				{
					int8 sign = INT_IS_EVEN( betterChoice ) ? 1 : -1;

					for( uint8 row = 0 ; row < rows ; ++row )	
					{
						mul = ( *pMatrix )( row, betterChoice );
						if( FCMP( mul, 0.0 ) )
						{
							reduce( pMatrix, &aux, row, betterChoice );
							det += sign * mul * determinant( &aux );
						}
						sign *= -1;
					}

				}
				// Retorna o determinante
				return det;
			}
	} // switch( rows )
}

/*===========================================================================================

MÉTODO reduce
	Reduz a matriz pOrigin para pDest, retirando a linha indicada por skipRow e a coluna
indicada por skipColumn.  Parte do pricípio que a matriz recebida como parâmetro é quadrada.

============================================================================================*/

Matrix* Matrix::reduce( const Matrix* pOrigin, Matrix* pDest, uint8 skipRow, uint8 skipColumn )
{
	const uint8 originNRows = pOrigin->getNRows(), originNColumns = pOrigin->getNColumns();

#if DEBUG
	// Checa possíveis erros de passagem de parâmetro
	if( ( skipRow >= originNRows ) || ( skipColumn >= originNColumns ) )
		throw InvalidArgumentException( "Os parâmetros skipRow e skipColumn devem indicar valores válidos" );
#endif

	// Copia os elementos
	uint8 destRow = 0, destColumn = 0;

	for( uint8 row = 0 ; row < originNRows ; ++row )
	{
		if( row != skipRow )
		{
			for( uint8 column = 0 ; column < originNColumns ; ++column )	
			{
				if( column != skipColumn )
					( *pDest )( destRow, destColumn++ ) = ( *pOrigin )( row, column );
			}
			++destRow;
			destColumn = 0;
		}
	}
	return pDest;
}

/*===========================================================================================

MÉTODO getBetterChoice
	Retorna a linha / coluna que possui o maior número de zeros em pIndex. O valor de retorno
será true caso seja uma linha, ou false caso seja uma coluna.

============================================================================================*/

// OBS : Talvez seja possível fazer todo este método com apenas um for aninhado ao invés de 2
bool Matrix::getBetterChoice( int16* pIndex ) const
{
	// Descobre a linha que possui o maior número de zeros
	uint8 zeroCounter = 0;
	int16 betterRowCount = -1, rowIndex = -1;

	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )	
		{
			if( ( *this )( row, column ) == 0.0 )
				++zeroCounter;
		}

		if( zeroCounter > betterRowCount )
		{
			betterRowCount = zeroCounter;
			rowIndex = row;
		}		
		zeroCounter = 0;
	}

	// Descobre a coluna que possui o maior número de zeros
	int16 betterColumnCount = -1, columnIndex = -1;

	for( uint8 column = 0 ; column < nColumns ; ++column )
	{
		for( uint8 row = 0 ; row < nRows ; ++row )	
		{
			if( ( *this )( row, column ) == 0.0 )
				++zeroCounter;
		}

		if( zeroCounter > betterColumnCount )
		{
			betterColumnCount = zeroCounter;
			columnIndex = column;
		}		
		zeroCounter = 0;
	}

	// Verifica se a melhor opção é uma linha...
	if( betterRowCount >= betterColumnCount )
	{
		*pIndex = rowIndex;
		return true;
	}
	
	// Senão, é uma coluna
	*pIndex = columnIndex;
	return false;
}

/*===========================================================================================

MÉTODO inverse
	Retorna a matriz inversa desta matriz.

============================================================================================*/

Matrix* Matrix::inverse( Matrix* pInverse ) const
{
	// Se a matriz não é quadrada, não possui inversa
	if( !isSquare() )
#if DEBUG
		throw IllegalMathOperation( "Só matrizes quadradas possuem matrizes inversas" );
#else
		throw IllegalMathOperation();
#endif
	
	if( !pInverse )
#if DEBUG
		throw InvalidArgumentException( "Não é possível armazenar a matriz inversa na matriz recebida como parâmetro" );
#else
		throw InvalidArgumentException();
#endif
	
	Matrix aux( nRows, nColumns );

	if( nRows == 1 )
	{
		aux.pMtx[0] = 1.0 / pMtx[0];
	}
	else
	{
		const float det = determinant();
		if( FCMP( det, 0.0 ) == 0 )
			throw IllegalMathOperation( "A matriz não é inversível: determinante = 0" );
			
		*( adjoint( &aux ) ) /= det;
	}

	*pInverse = aux;
	return pInverse;
}

/*===========================================================================================

MÉTODO inverse
	Transforma esta matriz em sua matriz inversa.

============================================================================================*/

Matrix* Matrix::inverse( void )
{
	Matrix aux( nRows, nColumns );
	*this = *( inverse( &aux ) );
	return this;
}

/*===========================================================================================

MÉTODO adjoint
	Retorna a matriz adjunta desta matriz. Parte do princípio que a matriz é quadrada e maior
do que uma matriz 1x1.

============================================================================================*/

Matrix* Matrix::adjoint( Matrix* pAdjoint ) const
{
	// Cria uma matriz auxiliar para calcularmos as reduções
	Matrix aux( nRows - 1, nColumns - 1 );

	// Cria a matriz de cofatores
	int8 sign;
	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		sign = INT_IS_EVEN( row ) ? 1 : -1;
		for( uint8 column = 0 ; column < nColumns ; ++column )	
		{
			reduce( this, &aux, row, column );
			( *pAdjoint )( row, column ) = sign * determinant( &aux );
			
			sign *= -1;
		}
	}
	
	// Obtém a matriz transposta da matriz de cofatores, ou seja, a matriz adjunta
	return pAdjoint->transpose();
}

/*===========================================================================================

MÉTODO transpose
	Retorna a matriz transposta desta matriz.

============================================================================================*/

Matrix* Matrix::transpose( Matrix* pTranspose ) const
{
	if( !pTranspose )
#if DEBUG
		throw InvalidArgumentException( "Não é possível armazenar a matriz transposta na matriz recebida como parâmetro" );
#else
		throw InvalidArgumentException();
#endif
	
	Matrix aux( nColumns, nRows );

	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )
			aux( column, row ) = ( *this )( row, column );
	}

	*pTranspose = aux;
	return pTranspose;
}

/*===========================================================================================

MÉTODO transpose
	Transforma esta matriz em sua matriz transposta.

============================================================================================*/

Matrix* Matrix::transpose( void )
{
	if( isSquare() )
	{
		const uint8 totalRows = nRows - 1;
		for( uint8 row = 0 ; row < totalRows ; ++row )
		{
			for( uint8 column = row + 1; column < nColumns ; ++column )
			{
				const float aux = ( *this )( row, column );
				( *this )( row, column ) = ( *this )( column, row );
				( *this )( column, row ) = aux;
			}
		}
	}
	else
	{
		Matrix aux( nColumns, nRows );
		*this = *( transpose( &aux ) );
	}
	return this;
}

/*===========================================================================================

OPERADOR []
	Retorna um valor para ser usado como lvalue.
 
	Não é muito bom sobrecarregar o operador [] em casos de arrays multi-dimensionais. A
principal causa é o fato de ele só poder receber um parâmetro. A implementação que se faz
necessária e outros motivos para não usá-lo podem ser encontrados em:
	- http://www.parashift.com/c++-faq-lite/operator-overloading.html#faq-13.10

============================================================================================*/

//float& Matrix::operator[]( int32 index )
//{
//}

/*===========================================================================================

OPERADOR []
	Retorna um valor para ser usado como rvalue.
 
 	Não é muito bom sobrecarregar o operador [] em casos de arrays multi-dimensionais. A
principal causa é o fato de ele só poder receber um parâmetro. A implementação que se faz
necessária e outros motivos para não usá-lo podem ser encontrados em:
	- http://www.parashift.com/c++-faq-lite/operator-overloading.html#faq-13.10

============================================================================================*/

//float Matrix::operator[]( int32 index ) const
//{
//}

/*===========================================================================================

OPERADOR =

============================================================================================*/

Matrix& Matrix::operator=( const Matrix &m )
{
	// Evita auto-atribuição
	if( this != &m )
	{
		const uint16 dataSize = m.nRows * m.nColumns;
		if( dataSize != ( nRows * nColumns ) )
		{
			SAFE_DELETE_VEC( pMtx );
			pMtx = new float[ dataSize ];
		}

		nRows = m.nRows;
		nColumns = m.nColumns;
		
		setElements( m.pMtx );
	}
	return *this;
}

/*===========================================================================================

OPERADOR -

============================================================================================*/

Matrix Matrix::operator-( const Matrix& m ) const
{
	Matrix res( *this );
	res -= m;
	return res;
}

/*===========================================================================================

OPERADOR -=

============================================================================================*/

Matrix& Matrix::operator-=( const Matrix& m )
{
	if( ( nRows != m.nRows ) || ( nColumns != m.nColumns ) )
#if DEBUG
		throw IllegalMathOperation( "A operação de soma não está definida para matrizes de diferentes dimensões" );
#else
		throw IllegalMathOperation();
#endif
	
	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )
			( *this )( row, column ) -= m( row, column );
	}
	return *this;
}

/*==========================================================================================

OPERADOR +

============================================================================================*/

Matrix Matrix::operator+( const Matrix& m ) const
{
	Matrix res( *this );
	res += m;
	return res;
}

/*===========================================================================================

OPERADOR +=

============================================================================================*/

Matrix& Matrix::operator+=( const Matrix& m )
{
		if( ( nRows != m.nRows ) || ( nColumns != m.nColumns ) )
#if DEBUG
		throw IllegalMathOperation( "A operação de soma não está definida para matrizes de diferentes dimensões" );
#else
		throw IllegalMathOperation();
#endif
	
	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )
			( *this )( row, column ) += m( row, column );
	}
	return *this;
}

/*===========================================================================================

OPERADOR /

============================================================================================*/

Matrix Matrix::operator/( float mul ) const
{
	Matrix res( *this );
	res /= mul;
	return res;
}

/*===========================================================================================

OPERADOR *

============================================================================================*/

Matrix Matrix::operator*( float mul ) const
{
	Matrix res( *this );
	res *= mul;
	return res;
}

/*===========================================================================================

OPERADOR *

============================================================================================*/

Matrix Matrix::operator*( const Matrix& m ) const
{
	Matrix res( *this );
	res *= m;
	return res;
}

/*===========================================================================================

OPERADOR /=

============================================================================================*/

Matrix& Matrix::operator/=( float div )
{
	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )	
			( *this )( row, column ) /= div;
	}
	return *this;
}

/*===========================================================================================

OPERADOR *=

============================================================================================*/

Matrix& Matrix::operator*=( float mul )
{
	for( uint8 row = 0 ; row < nRows ; ++row )
	{
		for( uint8 column = 0 ; column < nColumns ; ++column )	
			( *this )( row, column ) *= mul;
	}
	return *this;
}

/*===========================================================================================

OPERADOR *=

============================================================================================*/

Matrix& Matrix::operator*=( const Matrix& m )
{
	if( nColumns != m.nRows )
#if DEBUG
		throw IllegalMathOperation( "Não é possível realizar a multiplicação das matrizes: o número de linhas e colunas é incompatível" );
#else
		throw IllegalMathOperation();
#endif

	Matrix aux( nRows, m.nColumns );

	for( uint8 originRow = 0 ; originRow < nRows ; ++originRow )
	{
		for( uint8 destColumn = 0 ; destColumn < m.nColumns ; ++destColumn )
		{
			float sum = 0.0;
			for( uint8 originColumn = 0 ; originColumn < nColumns ; ++originColumn )
				sum += ( *this )( originRow, originColumn ) * m( originColumn, destColumn );

			aux( originRow, destColumn ) = sum;
		}
	}
	*this = aux;
	return *this;
}

/*===========================================================================================

OPERADOR ()
	Retorna um valor para ser usado como rvalue.

============================================================================================*/

float Matrix::operator()( uint8 row, uint8 column ) const
{
	return pMtx[ ( row * nColumns ) + column ];
}

/*===========================================================================================

OPERADOR ()
	 Retorna um valor para ser usado como lvalue.

============================================================================================*/

float& Matrix::operator()( uint8 row, uint8 column )
{
	return pMtx[ ( row * nColumns ) + column ];
}
