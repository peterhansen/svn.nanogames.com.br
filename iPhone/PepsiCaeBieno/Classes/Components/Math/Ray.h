/*
 *  Ray.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef RAY_H
#define RAY_H

#include "Point3f.h"

class Ray
{
	public:
		// Origem do raio
		Point3f origin;

		// Direção do raio
		Point3f direction;
	
		// Construtores
		Ray( void ) {};
		Ray( const Point3f* pOrigin, const Point3f* pDirection );
	
		// Inicializa o objeto
		inline void set( const Point3f* pOrigin = NULL, const Point3f* pDirection = NULL ){ if( pOrigin ) origin = *pOrigin; else origin.set(); if( pDirection ) direction = *pDirection; else direction.set(); };
	
		// Operadores de comparação
		bool operator == ( const Ray& point ) const;
};

#endif
