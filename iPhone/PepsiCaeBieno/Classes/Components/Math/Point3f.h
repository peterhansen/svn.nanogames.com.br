#ifndef POINT3F_H
#define POINT3F_H

#include "NanoTypes.h"

// Pré-definição de Point2i para que possamos utilizar seu nome da definição de Point3f. Assim
// evitamos #includes recursivos
class Point2i;

class Point3f
{
	public:
		// Assim podemos acessar os dados do ponto através de seus valores individuais ou como
		// um array
		union
		{
			struct
			{
				float x, y, z;
			};
			float p[3];
		};
	
		// XCode ainda não suporta o padrão N2210
		// http://www.google.com.br/search?hl=pt-BR&client=firefox-a&rls=org.mozilla%3Apt-BR%3Aofficial&q=C%2B%2B+n2210+&btnG=Pesquisar&meta=
		// Ressucita o construtor padrão
		// Point3f() = default;

		// Construtores
		explicit Point3f( float x = 0.0f, float y = 0.0f, float z = 0.0f );
		explicit Point3f( const Point2i* pPoint );

		// Inicializa o objeto
		inline void set( Point3f& point ){ *this = point; };
		inline void set( float x = 0.0f, float y = 0.0f, float z = 0.0f ){ this->x = x; this->y = y; this->z = z; };
	
		void set( Point2i& point ); // Não é inline pois aconteceria um include cíclico

		// Retorna o módulo do vetor descrito pelo ponto
		float getModule( void ) const;
	
		// Retorna o ângulo entre este vetor e o vetor passado como parâmetro
		float getAngle( const Point3f* pPoint ) const;
		float getAngle( float x, float y, float z ) const;

		// Normaliza o vetor descrito pelo ponto
		void normalize( void );
	
		// Retorna normalizado o vetor descrito pelo ponto
		Point3f getNormalized( void ) const;
	
		// Operadores de conversão ( casting )
//		operator float* ();
//		operator const float* () const;
	
		// Operadores unários
		Point3f operator + () const;
		Point3f operator - () const;

		// Operadores binários
		Point3f operator + ( const Point3f& point ) const;
		Point3f operator - ( const Point3f& point ) const;
		Point3f operator * ( float f ) const;
		Point3f operator / ( float f ) const;
		Point3f operator % ( const Point3f& point ) const; // Produto Vetorial - Cross Product
		float	operator * ( const Point3f& point ) const; // Produto Interno - Dot Product
	
		// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
		friend Point3f operator * ( float f, const Point3f& point );

		// Operadores de atribuição
		//Point3f& operator = ( const Point2i& point );
		Point3f& operator += ( const Point3f& point );
		Point3f& operator -= ( const Point3f& point );
		Point3f& operator *= ( float f );
		Point3f& operator /= ( float f );
		Point3f& operator %= ( const Point3f& point );

		// Operadores lógicos
		bool operator == ( const Point3f& point ) const;
		bool operator != ( const Point3f& point ) const;
};

#endif
