#include "PepsiCaeBieno.h"
#include "Config.h"

#include "AccelerometerManager.h"
#include "AudioManager.h"
#include "AudioSource.h"
#include "Exceptions.h"
#include "Font.h"
#include "Label.h"
#include "Random.h"
#include "RenderableImage.h"
#include "ResourceManager.h"

#include "GameUtils.h"
#include "LevelInfo.h"
#include "OptionsTag.h"
#include "PauseTag.h"
#include "PepsiCaeBienoAppDelegate.h"
#include "PieceGroup.h"
#include "ScoreLabel.h"
#include "WordsGroup.h"

#if DEBUG
	#include "Tests.h"
#endif

// Determina o número de objetos na cena
#define SCENE_N_OBJECTS ( 19 + SCORE_LABELS_MAX )

// Índices dos objetos contidos no grupo
#define OBJ_INDEX_BKG_PT				0
#define OBJ_INDEX_BKG_ES				1
#define OBJ_INDEX_PAUSE_BT				2
#define OBJ_INDEX_OPT_BT_PT				3
#define OBJ_INDEX_OPT_BT_ES				4
#define OBJ_INDEX_LEFT_BT				5
#define OBJ_INDEX_RIGHT_BT				6
#define OBJ_INDEX_PIECES_GROUP			7
#define OBJ_INDEX_SCORE_LABEL_0			8
#define OBJ_INDEX_SCORE_LABEL_LAST		( OBJ_INDEX_SCORE_LABEL_0 + SCORE_LABELS_MAX - 1 )
#define OBJ_INDEX_LEVEL_INFO			( OBJ_INDEX_SCORE_LABEL_LAST +  1 )
#define OBJ_INDEX_NEXT_PIECE			( OBJ_INDEX_SCORE_LABEL_LAST +  2 )
#define OBJ_INDEX_WORDS_GROUP			( OBJ_INDEX_SCORE_LABEL_LAST +  3 )
#define OBJ_INDEX_LEVEL_MSG_0			( OBJ_INDEX_SCORE_LABEL_LAST +  4 )
#define OBJ_INDEX_LEVEL_MSG_1			( OBJ_INDEX_SCORE_LABEL_LAST +  5 )
#define OBJ_INDEX_LEVEL_MSG_2			( OBJ_INDEX_SCORE_LABEL_LAST +  6 )
#define OBJ_INDEX_LEVEL_MSG_3			( OBJ_INDEX_SCORE_LABEL_LAST +  7 )
#define OBJ_INDEX_LEVEL_MSG_4			( OBJ_INDEX_SCORE_LABEL_LAST +  8 )
#define OBJ_INDEX_GAME_OVER_MSG_PT		( OBJ_INDEX_SCORE_LABEL_LAST +  9 )
#define OBJ_INDEX_GAME_OVER_MSG_ES		( OBJ_INDEX_SCORE_LABEL_LAST + 10 )
#define OBJ_INDEX_PAUSE_MSG				( OBJ_INDEX_SCORE_LABEL_LAST + 11 )

// Macro para reduzir a digitação em build()
#define PEPSI_CAE_BIEN_CHECKED_INSERT( pObj )	if( !pObj || !insertObject( pObj ) )	\
												{										\
													delete pObj;						\
													goto Error;							\
												}

// Velocidade vertical mínima da peça, em pixels por segundo
#define SPEED_MOVE_MIN_EASY PIECE_HEIGHT // OLD ( PIECE_HEIGHT * 2.0f )

// Velocidade vertical máxima inicial durante a movimentação peça, em pixels por segundo
#define SPEED_MOVE_MAX_EASY ( PIECE_HEIGHT * 6.0f )

// Velocidade vertical mínima durante a movimentação peça, em pixels por segundo
#define SPEED_MOVE_MIN_HARD SPEED_MOVE_MAX_EASY // OLD ( PIECE_HEIGHT * 4.0f )

// Velocidade vertical máxima durante a movimentação peça, em pixels por segundo
#define SPEED_MOVE_MAX_HARD SPEED_MOVE_MAX_EASY

// Velocidades horizontais máxima e mínima
#if INPUT_SCHEME_COLUMN_BY_HOR_SPEED
	#define PIECE_MAX_HOR_SPEED ( PIECE_WIDTH * 40.0f )
	#define PIECE_MIN_HOR_SPEED -PIECE_MAX_HOR_SPEED
#endif

// Aceleração da movimentação horizontal da peça no modo de input CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS
#define PIECE_HOR_ACCEL ( PIECE_WIDTH * 50.0f ) // OLD ( PIECE_WIDTH * 40.0f )

// Duração da transição de mostrar/esconder palavras, em milisegundos
#define TIME_TRANSITION 0.47f

// Pontuação máxima mostrada
#define SCORE_SHOWN_MAX 999999

#define WORD_INVALID 0
#define WORD_REGULAR 1
#define WORD_REVERSE 2

#define TIME_MESSAGE		 2.0f
#define TIME_GAME_OVER		 3.0f
#define TIME_GAME_FINISHED	10.0f

// Tamanho mínimo de uma palavra que pode ser formada
#define WORD_MIN_LENGTH 2

// Configurações do botão de pause
#define GAME_BT_PAUSE_POS_X 361.0f
#define GAME_BT_PAUSE_POS_Y_ACCEL_ONLY		264.0f
#define GAME_BT_PAUSE_POS_Y_ACCEL_N_TOUCH	231.0f
#define GAME_BT_PAUSE_WIDTH 128.0f
#define GAME_BT_PAUSE_HEIGHT 64.0f

// Configurações dos botões de movimentar as peças para a esquerda e para a direita
#define GAME_BT_LEFT_DIST_FROM_LEFT		 9.0f
#define GAME_BT_LEFT_DIST_FROM_BOTTOM	 5.0f
#define GAME_BT_LEFT_WIDTH				63.0f
#define GAME_BT_LEFT_HEIGHT				51.0f

#define GAME_BT_RIGHT_DIST_FROM_RIGHT	GAME_BT_LEFT_DIST_FROM_LEFT
#define GAME_BT_RIGHT_DIST_FROM_BOTTOM	GAME_BT_LEFT_DIST_FROM_BOTTOM
#define GAME_BT_RIGHT_WIDTH				GAME_BT_LEFT_WIDTH
#define GAME_BT_RIGHT_HEIGHT			GAME_BT_LEFT_HEIGHT

// Configurações do botão "Opções"
#define GAME_BT_OPT_POS_X_OFFSET		  2.0f
#define GAME_BT_OPT_POS_Y_ACCEL_ONLY	GAME_BT_PAUSE_POS_Y_ACCEL_ONLY
#define GAME_BT_OPT_POS_Y_ACCEL_N_TOUCH GAME_BT_PAUSE_POS_Y_ACCEL_N_TOUCH
#define GAME_BT_OPT_WIDTH 118.0f
#define GAME_BT_OPT_HEIGHT 64.0f

// Posicionamento do tabuleiro das peças
#define PIECES_GROUP_POS_X	120.0f
#define PIECES_GROUP_POS_Y	 73.0f
#define PIECES_GROUP_WIDTH	240.0f
#define PIECES_GROUP_HEIGHT	240.0f

// Posicionamento do componente que exibe as informações da fase
#define LEVEL_INFO_POS_X 362.0f
#define LEVEL_INFO_POS_Y_ACCEL_ONLY		75.0f
#define LEVEL_INFO_POS_Y_ACCEL_N_TOUCH  37.0f

// Posicionamento do componente que exibe as palavras a serem formadas
#define WORDS_GROUP_POS_X  0.0f
#define WORDS_GROUP_POS_Y_ACCEL_ONLY	78.0f
#define WORDS_GROUP_POS_Y_ACCEL_N_TOUCH 49.0f

// Configurações do objeto que indica a próxima peça a cair
#define NEXT_PIECE_POS_X   408.0f
#define NEXT_PIECE_POS_Y_ACCEL_ONLY   211.0f
#define NEXT_PIECE_POS_Y_ACCEL_N_TOUCH   172.0f
#define NEXT_PIECE_WIDTH	48.0f
#define NEXT_PIECE_HEIGHT	48.0f

// Determina as dimensões das mensagens que exibem o nível atual no início de cada fase
#define GAME_LV_MSG_WIDTH 130.0f
#define GAME_LV_MSG_HEIGHT 64.0f

// Determina as dimensões das mensagens que exibem a mensagem de fim de jogo
#define GAME_END_MSG_WIDTH 207.0f
#define GAME_END_MSG_HEIGHT 70.0f

// Determina as dimensões da mensagem de pausa
#define GAME_PAUSE_MSG_WIDTH	130.0f
#define GAME_PAUSE_MSG_HEIGHT	 62.0f

// Definições das cores multiplicadas pelos botões em seus estados
#define GAME_SCREEN_BT_PRESSED_COLOR	( uint8 )127, ( uint8 )127, ( uint8 )127, ( uint8 )255
#define GAME_SCREEN_BT_UNPRESSED_COLOR	( uint8 )255, ( uint8 )255, ( uint8 )255, ( uint8 )255

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

PepsiCaeBieno::PepsiCaeBieno( const ControlType& controlType )
		  :	Scene( SCENE_N_OBJECTS ),
			EventListener( EVENT_TOUCH ),
			AccelerometerListener( ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC ),
			nActiveTouches( 0 ),
			settingRestPos( 0 ),
			initDistBetweenTouches( 0.0f ),
			pPiecesGroup( NULL ),
			pieceVerticalSpeed( 0.0f ),

#if INPUT_SCHEME_COLUMN_BY_HOR_SPEED
			pieceHorizontalSpeed( 0.0f ),
			accHorizontalMovement( ( TOTAL_COLUMNS * PIECE_WIDTH ) * 0.5f ),
#endif
			pMovingPiece( NULL ),
			pNextPiece( NULL ),
			movingPieceColumn( -1 ),
			fallSpeedPercent( 0.0f ),
			pWordsGroup( NULL ),
			level( 0 ),
			timeToNextState( 0.0f ),
			timeLeft( 0.0f ),
			timeToNextPiece( 0.0f ),
			nextCharsCount( 0 ),
			wordsDraggingTouch( -1 ),
			pauseDraggingTouch( -1 ),
			leftDraggingTouch( -1 ),
			rightDraggingTouch( -1 ),
			optionsDraggingTouch( -1 ),
			score( 0 ),
			scoreShown( 0.0f ),
			currSoundIndex( 0 ),
			controlType( controlType )
{
	// Inicializa os arrays da classe
	memset( words, 0, sizeof( char* ) * ( TOTAL_WORDS << 1 ) );
	memset( pieces, 0, sizeof( Piece* ) * TOTAL_COLUMNS * TOTAL_ROWS );
	memset( scoreLabels, 0, sizeof( ScoreLabel* ) * SCORE_LABELS_MAX );

	// Inicializa os componentes do da cena
	if( !build() )
		#if DEBUG
			throw ConstructorException( "PepsiCaeBieno::PepsiCaeBieno: Unable to create object" );
		#else
			throw ConstructorException();
		#endif
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

PepsiCaeBieno::~PepsiCaeBieno( void )
{
	saveScoreIfRecord();
}

/*==============================================================================================

MÉTODO saveScoreIfRecord
	Salva a pontuação atual caso esta seja um recorde.

==============================================================================================*/

void PepsiCaeBieno::saveScoreIfRecord( void )
{
	if( gameState != GAME_STATE_GAME_OVER )
		[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) saveScoreIfRecord: score];
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool PepsiCaeBieno::build( void )
{
	{ // Evita erros com os gotos

		// Cria a câmera que irá renderrizar a cena do jogo
		OrthoCamera* pCamera = new OrthoCamera();
		if( !pCamera )
			return false;

		setCurrentCamera( pCamera );
		pCamera->setLandscapeMode( true );

		// Aloca os backgrounds
		char bkgPath[] = "bkgpt";
		for( uint8 i = 0 ; i < APP_N_LANGUAGES ; ++i )
		{
			RenderableImage* pBkg = new RenderableImage( bkgPath );
			PEPSI_CAE_BIEN_CHECKED_INSERT( pBkg );

			TextureFrame bkgFrame( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0 );
			pBkg->setImageFrame( bkgFrame );
			
			bkgPath[3] = 'e';
			bkgPath[4] = 's';
		}
		
		// Cria o botão de pause
		PauseTag* pPauseButton = new PauseTag();
		PEPSI_CAE_BIEN_CHECKED_INSERT( pPauseButton );

		TextureFrame imgFrame( 0, 0, GAME_BT_PAUSE_WIDTH, GAME_BT_PAUSE_HEIGHT, 0, 0 );
		pPauseButton->setImageFrame( imgFrame );
		pPauseButton->setPosition( GAME_BT_PAUSE_POS_X, GAME_BT_PAUSE_POS_Y_ACCEL_ONLY );
		pPauseButton->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		
		// Cria o botão "Opções" (português e espanhol)
		OptionsTag* pOptBtPt = new OptionsTag( "tagOptPt" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pOptBtPt );
		
		imgFrame.set( 0, 0, GAME_BT_OPT_WIDTH, GAME_BT_OPT_HEIGHT, 0, 0 );
		pOptBtPt->setImageFrame( imgFrame );
		pOptBtPt->setPosition( PIECES_GROUP_POS_X - pOptBtPt->getWidth() - GAME_BT_OPT_POS_X_OFFSET, GAME_BT_OPT_POS_Y_ACCEL_N_TOUCH );
		pOptBtPt->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		
		OptionsTag* pOptBtEs = new OptionsTag( "tagOptEs" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pOptBtEs );

		pOptBtEs->setImageFrame( imgFrame );
		pOptBtEs->setPosition( pOptBtPt->getPosition() );
		pOptBtEs->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		
		// Cria o botão de movimentar as peças para a esquerda
		RenderableImage* pLeftButton = new RenderableImage( "seta" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pLeftButton );

		imgFrame.set( 0, 0, GAME_BT_LEFT_WIDTH, GAME_BT_LEFT_HEIGHT, 0, 0 );
		pLeftButton->setImageFrame( imgFrame );
		pLeftButton->setPosition( GAME_BT_LEFT_DIST_FROM_LEFT, SCREEN_HEIGHT - GAME_BT_LEFT_HEIGHT - GAME_BT_LEFT_DIST_FROM_BOTTOM );
		
		// Cria o botão de movimentar as peças para a direita
		RenderableImage* pRightButton = new RenderableImage( pLeftButton );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pRightButton );

		pRightButton->setPosition( SCREEN_WIDTH - GAME_BT_LEFT_WIDTH - GAME_BT_RIGHT_DIST_FROM_RIGHT, SCREEN_HEIGHT - GAME_BT_LEFT_HEIGHT - GAME_BT_RIGHT_DIST_FROM_BOTTOM );
		pRightButton->mirror( MIRROR_HOR );

		// Peça que está caindo
		pMovingPiece = new Piece();
		if( !pMovingPiece )
			goto Error;

		pMovingPiece->setState( PIECE_STATE_MOVING );
		
		// Tabuleiro
		pPiecesGroup = new PieceGroup( pMovingPiece );
		if( !pPiecesGroup || !insertObject( pPiecesGroup ) )
		{
			delete pPiecesGroup;
			delete pMovingPiece;
			goto Error;
		}

		pPiecesGroup->setPosition( PIECES_GROUP_POS_X, PIECES_GROUP_POS_Y );
		pPiecesGroup->setViewport( PIECES_GROUP_POS_X, PIECES_GROUP_POS_Y, PIECES_GROUP_WIDTH, PIECES_GROUP_HEIGHT );
		
		// Labels que informam a pontuação de cada jogada
		for( uint8 i = 0 ; i < SCORE_LABELS_MAX ; ++i )
		{
			scoreLabels[ i ] = new ScoreLabel( this );
			PEPSI_CAE_BIEN_CHECKED_INSERT( scoreLabels[ i ] );
		}
		
		// Informações da fase
		LevelInfo* pLevelInfo = new LevelInfo();
		PEPSI_CAE_BIEN_CHECKED_INSERT( pLevelInfo );

		pLevelInfo->setPosition( LEVEL_INFO_POS_X, LEVEL_INFO_POS_Y_ACCEL_ONLY );
		
		updateScoreLabel();
		
		// Indicador da próxima peça
		pNextPiece = new Piece();
		PEPSI_CAE_BIEN_CHECKED_INSERT( pNextPiece );

		pNextPiece->setViewport( GET_DEFAULT_VIEWPORT() );
		pNextPiece->setPosition( NEXT_PIECE_POS_X, NEXT_PIECE_POS_Y_ACCEL_ONLY );
		pNextPiece->setState( PIECE_STATE_IDLE );
		pNextPiece->setColor( PIECE_COLOR_ORANGE );
		
		// Cria o componente que irá exibir as palavras que devem ser formadas
		pWordsGroup = new WordsGroup();
		PEPSI_CAE_BIEN_CHECKED_INSERT( pWordsGroup );

		pWordsGroup->setPosition( WORDS_GROUP_POS_X, WORDS_GROUP_POS_Y_ACCEL_ONLY );
		
		// Aloca as mensagens que exibem o nível atual para o jogador
		char msgPath[] = "lv ";
		for( uint8 i = 0 ; i < MAX_LEVEL ; ++i )
		{
			msgPath[2] = '0' + i;

			RenderableImage* pLevelMsg = new RenderableImage( msgPath );
			PEPSI_CAE_BIEN_CHECKED_INSERT( pLevelMsg );
			
			TextureFrame msgFrame( 0, 0, GAME_LV_MSG_WIDTH, GAME_LV_MSG_HEIGHT, 0, 0 );
			pLevelMsg->setImageFrame( msgFrame );

			pLevelMsg->setPosition( PIECES_GROUP_POS_X + (( PIECES_GROUP_WIDTH - pLevelMsg->getWidth() ) * 0.5f ), PIECES_GROUP_POS_Y + (( PIECES_GROUP_HEIGHT - pLevelMsg->getHeight() ) * 0.5f ) );
		}
		
		// Aloca as mensagens que informam o fim de jogo
		char endImgPath[] = "endpt";
		for( uint8 i = 0 ; i < APP_N_LANGUAGES ; ++i )
		{
			RenderableImage* pEndMsg = new RenderableImage( endImgPath );
			PEPSI_CAE_BIEN_CHECKED_INSERT( pEndMsg );
			
			TextureFrame endMsgFrame( 0, 0, GAME_END_MSG_WIDTH, GAME_END_MSG_HEIGHT, 0, 0 );
			pEndMsg->setImageFrame( endMsgFrame );
			
			pEndMsg->setPosition( PIECES_GROUP_POS_X + (( PIECES_GROUP_WIDTH - pEndMsg->getWidth() ) * 0.5f ), PIECES_GROUP_POS_Y + (( PIECES_GROUP_HEIGHT - pEndMsg->getHeight() ) * 0.5f ) );
			
			endImgPath[3] = 'e';
			endImgPath[4] = 's';
		}
		
		// Aloca a mensagem de pausa
		RenderableImage* pPauseMsg = new RenderableImage( "msgp" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pPauseMsg );
			
		TextureFrame endMsgFrame( 0, 0, GAME_PAUSE_MSG_WIDTH, GAME_PAUSE_MSG_HEIGHT, 0, 0 );
		pPauseMsg->setImageFrame( endMsgFrame );
			
		pPauseMsg->setPosition( PIECES_GROUP_POS_X + (( PIECES_GROUP_WIDTH - pPauseMsg->getWidth() ) * 0.5f ), PIECES_GROUP_POS_Y + (( PIECES_GROUP_HEIGHT - pPauseMsg->getHeight() ) * 0.5f ) );

		return true;

	} // Evita erros com os gotos
	
	// Label de tratamento de erros
	Error:
		return false;
}

/*==============================================================================================

MÉTODO reset
	Reinicia a tela de jogo.

==============================================================================================*/

void PepsiCaeBieno::reset( uint8 languageIndex )
{
	// Sorteia a primeira música ambiente
	int32 rand = Random::GetInt( 0, 128 );
	if( rand < 43 )
	{
		currSoundIndex = SOUND_INDEX_AMBIENT_1;
	}
	else if( rand < 86 )
	{
		currSoundIndex = SOUND_INDEX_AMBIENT_2;
	}
	else
	{
		currSoundIndex = SOUND_INDEX_AMBIENT_3;
	}
	
	// Inicializa as variáveis da classe
	nActiveTouches = 0;
	settingRestPos = 0;
	initDistBetweenTouches = 0;
	pieceVerticalSpeed = 0.0f;
	
	#if	INPUT_SCHEME_COLUMN_BY_HOR_SPEED
		pieceHorizontalSpeed = 0.0f;
		accHorizontalMovement = ( TOTAL_COLUMNS * PIECE_WIDTH ) * 0.5f;
	#endif
						
	movingPieceColumn = -1;
	fallSpeedPercent = 0.0f;
	level = 0;
	timeToNextState = 0.0f;
	timeLeft = 0.0f;
	timeToNextPiece = 0.0f;
	nextCharsCount = 0;

	score = 0;
	scoreShown = 0.0f;
	updateScoreLabel();
	
	memset( nextChars, 0, sizeof( char ) * ( WORD_MAX_LENGTH + 1 ) * WORDS_TO_MIX );
	
	// Nenhum botão está pressionado
	resetButtons();
	
	// Modifica os componentes que dependem de idioma
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;

	getObject( OBJ_INDEX_BKG_PT )->setVisible( isPortuguese );
	getObject( OBJ_INDEX_OPT_BT_PT )->setVisible( isPortuguese );
	
	getObject( OBJ_INDEX_BKG_ES )->setVisible( !isPortuguese );
	getObject( OBJ_INDEX_OPT_BT_ES )->setVisible( !isPortuguese );
	
	// Modifica o posicionamento dos elementos da interface de acordo com o modo de input
	if( controlType == CONTROL_TYPE_ACCELEROMETERS_ONLY )
	{
		pWordsGroup->setPosition( WORDS_GROUP_POS_X, WORDS_GROUP_POS_Y_ACCEL_ONLY );

		getObject( OBJ_INDEX_LEFT_BT )->setVisible( false );
		getObject( OBJ_INDEX_RIGHT_BT )->setVisible( false );
		
		getObject( OBJ_INDEX_OPT_BT_PT )->setPosition( getObject( OBJ_INDEX_OPT_BT_PT )->getPosition()->x, GAME_BT_OPT_POS_Y_ACCEL_ONLY );
		getObject( OBJ_INDEX_OPT_BT_ES )->setPosition( getObject( OBJ_INDEX_OPT_BT_ES )->getPosition()->x, GAME_BT_OPT_POS_Y_ACCEL_ONLY );
		getObject( OBJ_INDEX_LEVEL_INFO )->setPosition( getObject( OBJ_INDEX_LEVEL_INFO )->getPosition()->x, LEVEL_INFO_POS_Y_ACCEL_ONLY );
		
		getObject( OBJ_INDEX_PAUSE_BT )->setPosition( getObject( OBJ_INDEX_PAUSE_BT )->getPosition()->x, GAME_BT_PAUSE_POS_Y_ACCEL_ONLY );
		
		pNextPiece->setPosition( pNextPiece->getPosition()->x, NEXT_PIECE_POS_Y_ACCEL_ONLY );
	}
	else
	{
		pWordsGroup->setPosition( WORDS_GROUP_POS_X, WORDS_GROUP_POS_Y_ACCEL_N_TOUCH );

		getObject( OBJ_INDEX_LEFT_BT )->setVisible( true );
		getObject( OBJ_INDEX_RIGHT_BT )->setVisible( true );
		
		getObject( OBJ_INDEX_OPT_BT_PT )->setPosition( getObject( OBJ_INDEX_OPT_BT_PT )->getPosition()->x, GAME_BT_OPT_POS_Y_ACCEL_N_TOUCH );
		getObject( OBJ_INDEX_OPT_BT_ES )->setPosition( getObject( OBJ_INDEX_OPT_BT_ES )->getPosition()->x, GAME_BT_OPT_POS_Y_ACCEL_N_TOUCH );
		getObject( OBJ_INDEX_LEVEL_INFO )->setPosition( getObject( OBJ_INDEX_LEVEL_INFO )->getPosition()->x, LEVEL_INFO_POS_Y_ACCEL_N_TOUCH );
		
		getObject( OBJ_INDEX_PAUSE_BT )->setPosition( getObject( OBJ_INDEX_PAUSE_BT )->getPosition()->x, GAME_BT_PAUSE_POS_Y_ACCEL_N_TOUCH );
		
		pNextPiece->setPosition( pNextPiece->getPosition()->x, NEXT_PIECE_POS_Y_ACCEL_N_TOUCH );
	}

	// OBS: Essas duas mensagens devem ficar invisíveis até o término do jogo. Logo, não
	// alteramos suas visibilidades aqui
//	getObject( OBJ_INDEX_GAME_OVER_MSG_PT )->setVisible( isPortuguese );
//	getObject( OBJ_INDEX_GAME_OVER_MSG_ES )->setVisible( !isPortuguese );
	
	pWordsGroup->reset( languageIndex );
	static_cast< LevelInfo* >( getObject( OBJ_INDEX_LEVEL_INFO ) )->reset( languageIndex );

	// Carrega as palavras que podem ser formadas, já no idioma correto
	loadWords();

	// Carrega a 1a fase
	loadNextLevel();
}

/*==============================================================================================

MÉTODO resetButtons
	Coloca todos os botões em seus estados iniciais.

==============================================================================================*/

void PepsiCaeBieno::resetButtons( void )
{
	cancelTrackedTouches();

	wordsDraggingTouch = -1;
	pauseDraggingTouch = -1;
	leftDraggingTouch = -1;
	rightDraggingTouch = -1;
	optionsDraggingTouch = -1;
	
	Color btUnPressedColor( GAME_SCREEN_BT_UNPRESSED_COLOR );
	static_cast< PauseTag* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( btUnPressedColor );
	static_cast< RenderableImage* >( getObject( OBJ_INDEX_LEFT_BT ) )->setVertexSetColor( btUnPressedColor );
	static_cast< RenderableImage* >( getObject( OBJ_INDEX_RIGHT_BT ) )->setVertexSetColor( btUnPressedColor );
	static_cast< OptionsTag* >( getObject( OBJ_INDEX_OPT_BT_PT ) )->setVertexSetColor( btUnPressedColor );
	static_cast< OptionsTag* >( getObject( OBJ_INDEX_OPT_BT_ES ) )->setVertexSetColor( btUnPressedColor );
}

/*==============================================================================================

MÉTODO loadWords
	Carrega as palavras que podem ser formadas já no idioma correto.

==============================================================================================*/

void PepsiCaeBieno::loadWords( void )
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;

	for( int8 i = 0, j = 0 ; i < TOTAL_WORDS ; ++i, j += 2 )
	{
		const char* pCurrWord = [hApplication getText: TEXT_INDEX_FIRST_WORD + i];
	
		snprintf( words[j], WORD_MAX_LENGTH + 1, "%s", pCurrWord );
		
		#if DEBUG
			LOG( @"%d - %s", j, words[j] );
		#endif
		
		int8 currWordLen = strlen( pCurrWord );
		for( int8 k = currWordLen - 1 ; k >= 0 ; --k )
			words[j + 1][ currWordLen - k - 1 ] = words[j][ k ];
		
		for( int8 k = currWordLen ; k < WORD_MAX_LENGTH + 1 ; ++k )
			words[j + 1][k] = '\0';
		
		#if DEBUG
			LOG( @"%d - %s", j + 1, words[j + 1] );
		#endif
	}
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool PepsiCaeBieno::update( float timeElapsed )
{
	if( !isActive() )
		return false;

	switch( gameState )
	{
		case GAME_STATE_PAUSED:
		case GAME_STATE_OPTIONS:
			break;

		case GAME_STATE_PLAYING:
			// OLD: Assim o jogador era penalizado pela animação da peça sumindo, já que, enquanto animamos, não deixamos outras peças cairem
			//updateTime( timeElapsed );
			
			if( timeToNextPiece > 0.0f )
			{
				timeToNextPiece -= timeElapsed;

				if ( timeToNextPiece <= 0 )
				{
					timeToNextPiece = 0.0f;
					insertNewPiece();
				}
			}
			else
			{
				updateTime( timeElapsed );
				
				if( controlType == CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS )
				{
					if( leftDraggingTouch != -1 )
					{
						if( pieceHorizontalSpeed > 0.0f )
							setPieceHorSpeed( 0.0f );

						setPieceHorSpeed( pieceHorizontalSpeed - ( PIECE_HOR_ACCEL * timeElapsed ) );
					}
					else if( rightDraggingTouch != -1 )
					{
						if( pieceHorizontalSpeed < 0.0f )
							setPieceHorSpeed( 0.0f );

						setPieceHorSpeed( pieceHorizontalSpeed + ( PIECE_HOR_ACCEL * timeElapsed ) );
					}
				}
				
				if( canMove() )
				{
					#if INPUT_SCHEME_COLUMN_BY_HOR_SPEED
						// OLD
//						if( pMovingPiece->getPosition()->y > 0.0f )
//						{
							float currX = pMovingPiece->getPosition()->x;
							uint8 currColumn = CLAMP( currX, 0.0f, pPiecesGroup->getWidth() ) / PIECE_WIDTH;
							
							accHorizontalMovement += ( pieceHorizontalSpeed * timeElapsed );
							accHorizontalMovement = CLAMP( accHorizontalMovement, 0.0f, pPiecesGroup->getWidth() - PIECE_WIDTH );

							uint8 goalColumn = CLAMP( accHorizontalMovement / PIECE_WIDTH, 0, TOTAL_COLUMNS - 1 );
							
							#if DEBUG
								//LOG( @"Speed = %.3f ------ GoalX = %.3f", pieceHorizontalSpeed, accHorizontalMovement );
							#endif

							while( currColumn != goalColumn )
							{
								if( !setMovingPieceColumn( currColumn > goalColumn ? --currColumn : ++currColumn ) )
									break;
							}
//						}
					#endif

					pMovingPiece->move( 0.0f, pieceVerticalSpeed * timeElapsed );

					Point3f movingPiecePos = *( pMovingPiece->getPosition() );
					if( movingPiecePos.y >= pMovingPiece->getYLimit() )
					{
						pMovingPiece->setPosition( movingPiecePos.x, pMovingPiece->getYLimit() );

						int8 currentRow = getAvailableRowsAtColumn( movingPieceColumn );
						if( currentRow >= 0 )
						{
							Piece* pInsertedPiece = pPiecesGroup->insertPiece( pMovingPiece );
							
							// Não era para acontecer, mas por precaução...
							if( pInsertedPiece == NULL )
							{
								// fim de jogo
								setState( GAME_STATE_GAME_OVER );
							}
							else
							{
								pieces[ movingPieceColumn ][ currentRow ] = pInsertedPiece;

								checkWords( movingPieceColumn, currentRow );

								insertNewPiece();
							}
						}
						else
						{
							// fim de jogo
							setState( GAME_STATE_GAME_OVER );
						}
					}
				}
			}

		default:
			Scene::update( timeElapsed );
	}
	
	// Caso o estado atual tenha uma duração máxima definida, atualiza o contador
	if( timeToNextState > 0.0f )
	{
		timeToNextState -= timeElapsed;
			
		if( timeToNextState <= 0.0f )
			onStateEnded();
	}

	// Atualiza a pontuação
	updateScore( timeElapsed, false );

	return true;
}

/*===========================================================================================
 
MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

============================================================================================*/

bool PepsiCaeBieno::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	// Obtém a informação do toque
	const NSArray* pTouchesArray = ( NSArray* )pParam;
	
	// OBS : Acho que isso nunca acontece, mas...
	uint8 nTouches = [pTouchesArray count];
	if( nTouches == 0 )
		return false;
		
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
			{
				int8 touchIndex = -1;
				for( uint8 i = 0 ; ( i < nTouches ) && ( nActiveTouches < MAX_TOUCHES ) ; ++i )
				{
					if( ( touchIndex = startTrackingTouch( [pTouchesArray objectAtIndex:i] ) ) >= 0 )
						onNewTouch( touchIndex, &trackedTouches[i].startPos );
				}
			}
			return true;

		case EVENT_TOUCH_MOVED:
			// Se possuímos mais de um toque (no caso deste jogo, > 1 serç sempre 2), estamos recebendo uma operação
			// de zoom
			if( nActiveTouches > 1 )
			{
				// Fazer quando o usuário move apenas um dedo causa bugs estranhos
				if( nTouches == 1 )
					return false;

				// OBS: Não vamos ter zoom
//				int8 touchIndex = -1;
//				Point3f aux[ MAX_TOUCHES ] = { trackedTouches[0].startPos, trackedTouches[1].startPos };
//
//				for( uint8 i = 0 ; i < nTouches ; ++i )
//				{
//					UITouch* hTouch = [pTouchesArray objectAtIndex:i];
//					if( ( touchIndex = isTrackingTouch( hTouch ) ) >= 0 )
//					{
//						CGPoint currTouchPos = [hTouch locationInView: NULL];
//						aux[ touchIndex ].set( currTouchPos.x, currTouchPos.y );
//					}
//				}
//
//				// Se os toques estão se aproximando, é um zoom out
//				float currZoom = pCurrCamera->getZoomFactor();
//				float newDistBetweenTouches = ( aux[0] - aux[1] ).getModule();
//				float movement = fabs( newDistBetweenTouches - initDistBetweenTouches );
//				if( movement < ZOOM_MIN_DIST_TO_CHANGE )
//					return false;
//
//				if( FLTN( newDistBetweenTouches, initDistBetweenTouches ) )
//				{
//					float newZoom = currZoom * powf( ZOOM_OUT_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
//
//					if( newZoom < ZOOM_MIN )
//						pCurrCamera->zoom( ZOOM_MIN );
//					else
//						pCurrCamera->zoom( newZoom );
//				}
//				// Se os toques estão se afastando, é um zoom in
//				else if( FGTN( newDistBetweenTouches, initDistBetweenTouches ) )
//				{
//					float newZoom = currZoom * powf( ZOOM_IN_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
//
//					if( newZoom > ZOOM_MAX )
//						pCurrCamera->zoom( ZOOM_MAX );
//					else
//						pCurrCamera->zoom( newZoom );
//				}
//
//				initDistBetweenTouches = newDistBetweenTouches;
			}
			// Movimentação de toque único
			else
			{
				UITouch* hTouch = [pTouchesArray objectAtIndex:0];
				int8 touchIndex = isTrackingTouch( hTouch );
				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					CGPoint lastPos = [hTouch previousLocationInView:NULL];
					
					Point3f touchCurrPos( currPos.x, currPos.y );
					Point3f touchLastPos( lastPos.x, lastPos.y );

					onSingleTouchMoved( touchIndex, &touchCurrPos, &touchLastPos );
				}
			}
			return true;
		
		case EVENT_TOUCH_CANCELED:
		case EVENT_TOUCH_ENDED:
			for( uint8 i = 0 ; i < nTouches ; ++i )
			{
				UITouch* hTouch = [pTouchesArray objectAtIndex:i];
				int8 touchIndex = isTrackingTouch( hTouch );

				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					currPos = Utils::MapPointByOrientation( &currPos );

					Point3f touchPos( currPos.x, currPos.y );
					onTouchEnded( touchIndex, &touchPos );
					
					// Cancela o rastreamento do toque
					stopTrackingTouch( touchIndex );
				}
			}
			return true;
	}
	return false;
}

/*===========================================================================================
 
MÉTODO onNewTouch
	Trata eventos de início de toque.

============================================================================================*/

void PepsiCaeBieno::onNewTouch( int8 touchIndex, const Point3f* pTouchPos )
{
	switch( gameState )
	{
		case GAME_STATE_PLAYING:
			
		// OLD
//		case GAME_STATE_BEGIN_LEVEL:
//		case GAME_STATE_LEVEL_CLEARED:
//		case GAME_STATE_GAME_OVER:
//		case GAME_STATE_NEW_RECORD:
//		case GAME_STATE_SHOW_SCORE:
			{
				bool pressedLeftButtton;
				Color btPressedColor( GAME_SCREEN_BT_PRESSED_COLOR );
				
				PauseTag* pPauseButton = static_cast< PauseTag* >( getObject( OBJ_INDEX_PAUSE_BT ) );
				
				RenderableImage* pLeftButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_LEFT_BT ) );
				RenderableImage* pRightButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_RIGHT_BT ) );

				OptionsTag* pOptionsButton = static_cast< OptionsTag* >( getObject( OBJ_INDEX_OPT_BT_PT ) );
				if( !pOptionsButton->isVisible() )
					pOptionsButton = static_cast< OptionsTag* >( getObject( OBJ_INDEX_OPT_BT_ES ) );

				// Testa colisão com a barra de palavras
				if( pWordsGroup->checkCollision( pTouchPos ) )
				{
					wordsDraggingTouch = touchIndex;
				}
				else if( ( controlType == CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS ) &&
						 ( ( pressedLeftButtton = Utils::IsPointInsideRect( pTouchPos, pLeftButton ) ) || Utils::IsPointInsideRect( pTouchPos, pRightButton ) ) )
				{
					if( pressedLeftButtton )
					{
						leftDraggingTouch = touchIndex;
						pLeftButton->setVertexSetColor( btPressedColor );
						
						setMovingPieceColumn( movingPieceColumn - 1 );
						
						accHorizontalMovement = ( movingPieceColumn * PIECE_WIDTH ) + ( PIECE_WIDTH * 0.5f );
					}
					else
					{
						rightDraggingTouch = touchIndex;
						pRightButton->setVertexSetColor( btPressedColor );
						
						setMovingPieceColumn( movingPieceColumn + 1 );
						
						accHorizontalMovement = ( movingPieceColumn * PIECE_WIDTH ) + ( PIECE_WIDTH * 0.5f );
					}
				}
				// Testa colsisão com o botão de pause
				else if( pPauseButton->checkCollision( pTouchPos ) )
				{
					pauseDraggingTouch = touchIndex;
					pPauseButton->setVertexSetColor( btPressedColor );
				}
				// Testa colisão com o botão de opções
				else if( pOptionsButton->checkCollision( pTouchPos ) )
				{
					optionsDraggingTouch = touchIndex;
					pOptionsButton->setVertexSetColor( btPressedColor );
				}
			}
			break;
			
		case GAME_STATE_PAUSED:
			{
				PauseTag* pPauseButton = static_cast< PauseTag* >( getObject( OBJ_INDEX_PAUSE_BT ) );
				
				RenderableImage* pLeftButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_LEFT_BT ) );
				RenderableImage* pRightButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_RIGHT_BT ) );

				// Testa colisão com a barra de palavras
				if( pWordsGroup->checkCollision( pTouchPos ) )
				{
					wordsDraggingTouch = touchIndex;
				}
				// OBS: Testamos colisão com estes botões simplesmente para evitarmos de acionar um botão que fica por trás. Por exemplo,
				// o usuário poderia clicar no botão de mover a peça para a esquerda no estado de pause e acabar despausando o jogo.
				else if( ( controlType == CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS ) &&
						 ( ( Utils::IsPointInsideRect( pTouchPos, pLeftButton ) ) || Utils::IsPointInsideRect( pTouchPos, pRightButton ) ) )
				{
					// Não faz nada
				}
				// Testa colsisão com o botão de pause
				else if( pPauseButton->checkCollision( pTouchPos ) )
				{
					pauseDraggingTouch = touchIndex;
					
					Color btPressedColor( GAME_SCREEN_BT_PRESSED_COLOR );
					pPauseButton->setVertexSetColor( btPressedColor );
				}
			}
			break;
	}
}

/*===========================================================================================
 
MÉTODO getPickingRay
	Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
jogo.

============================================================================================*/

void PepsiCaeBieno::getPickingRay( Ray& ray, const Point3f& p ) const
{
	Utils::GetPickingRay( &ray, pCurrCamera, p.x, p.y );

	// Corrige a origem do raio, jç que esta serç sempre a posição do observador (centro da tela)
	// OBS: Esta abordagem estç PORCA!!!!! O certo seria utilizarmos:
	// Utils::Unproject( &p, pCurrCamera, &trackedTouches[ touchIndex ].unmappedStartPos );
	// No entanto, a saída de Utils::Unproject não estç produzindo valores corretos com a projeção ortogonal

	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	Point3f screenCenter( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	ray.origin += ( Utils::MapPointByOrientation( &p ) - screenCenter ) * invZoom;
}

/*===========================================================================================
 
MÉTODO onTouchEnded
	Trata eventos de fim de toque.

============================================================================================*/

void PepsiCaeBieno::onTouchEnded( int8 touchIndex, const Point3f* pTouchPos )
{	
	switch( gameState )
	{
		case GAME_STATE_PLAYING:
		// OLD
//		case GAME_STATE_LEVEL_CLEARED:
//		case GAME_STATE_GAME_OVER:
//		case GAME_STATE_NEW_RECORD:
//		case GAME_STATE_SHOW_SCORE:
			{
				Color btUnPressedColor( GAME_SCREEN_BT_UNPRESSED_COLOR );

				if( touchIndex == wordsDraggingTouch )
				{
					wordsDraggingTouch = -1;
				}
				else if( touchIndex == pauseDraggingTouch )
				{
					pauseDraggingTouch = -1;
					static_cast< RenderableImage* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( btUnPressedColor );
					
					setState( GAME_STATE_PAUSED );
				}
				else if( touchIndex == optionsDraggingTouch )
				{
					optionsDraggingTouch = -1;
					
					RenderableImage* pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_PT ) );
					if( !pOptionsButton->isVisible() )
						pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_ES ) );
					
					pOptionsButton->setVertexSetColor( btUnPressedColor );
					
					setState( GAME_STATE_OPTIONS );
				}
				else if( controlType == CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS )
				{
					if( touchIndex == leftDraggingTouch )
					{
						leftDraggingTouch = -1;
						static_cast< RenderableImage* >( getObject( OBJ_INDEX_LEFT_BT ) )->setVertexSetColor( btUnPressedColor );
						
						setPieceHorSpeed( 0.0f );
					}
					else if( touchIndex == rightDraggingTouch )
					{
						rightDraggingTouch = -1;
						static_cast< RenderableImage* >( getObject( OBJ_INDEX_RIGHT_BT ) )->setVertexSetColor( btUnPressedColor );
						
						setPieceHorSpeed( 0.0f );
					}
				}
			}
			break;
			
		case GAME_STATE_PAUSED:
			if( touchIndex == wordsDraggingTouch )
			{
				wordsDraggingTouch = -1;
			}
			else if( touchIndex == pauseDraggingTouch )
			{
				pauseDraggingTouch = -1;
				
				Color btUnPressedColor( GAME_SCREEN_BT_UNPRESSED_COLOR );
				static_cast< RenderableImage* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( btUnPressedColor );
				
				resume();
			}
			break;
	}
}

/*===========================================================================================
 
MÉTODO onSingleTouchMoved
	Trata eventos de movimentação de toques.

============================================================================================*/

void PepsiCaeBieno::onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos )
{
	switch( gameState )
	{
		case GAME_STATE_BEGIN_LEVEL:
		case GAME_STATE_PLAYING:
		case GAME_STATE_LEVEL_CLEARED:
		case GAME_STATE_GAME_OVER:
		case GAME_STATE_NEW_RECORD:
		case GAME_STATE_SHOW_SCORE:
		case GAME_STATE_PAUSED:
		{
			Color btUnPressedColor( GAME_SCREEN_BT_UNPRESSED_COLOR );
			Point3f currPos = Utils::MapPointByOrientation( pCurrPos );

			if( touchIndex == wordsDraggingTouch )
			{
				// Verifica se ainda está colidindo
				if( pWordsGroup->checkCollision( &currPos ) )
				{	
					float invZoom = 1.0f / pCurrCamera->getZoomFactor();
					
					Point3f lastPos = Utils::MapPointByOrientation( pLastPos );
					pWordsGroup->slide( ( lastPos.y - currPos.y ) * invZoom );
				}
				else
				{
					wordsDraggingTouch = -1;
				}
			}
			else if( touchIndex == pauseDraggingTouch )
			{
				PauseTag* pPauseButton = static_cast< PauseTag* >( getObject( OBJ_INDEX_PAUSE_BT ) );

				// Verifica se ainda está colidindo
				if( !pPauseButton->checkCollision( &currPos ) )
				{
					pauseDraggingTouch = -1;
					pPauseButton->setVertexSetColor( btUnPressedColor );
				}
			}
			else if( touchIndex == optionsDraggingTouch )
			{
				OptionsTag* pOptionsButton = static_cast< OptionsTag* >( getObject( OBJ_INDEX_OPT_BT_PT ) );
				if( !pOptionsButton->isVisible() )
					pOptionsButton = static_cast< OptionsTag* >( getObject( OBJ_INDEX_OPT_BT_ES ) );

				// Verifica se ainda está colidindo
				if( !pOptionsButton->checkCollision( &currPos ) )
				{
					optionsDraggingTouch = -1;
					pOptionsButton->setVertexSetColor( btUnPressedColor );
				}
			}
			else if( controlType == CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS )
			{
				RenderableImage* pLeftButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_LEFT_BT ) );
				RenderableImage* pRightButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_RIGHT_BT ) );

				if( touchIndex == leftDraggingTouch )
				{
					// Verifica se ainda está colidindo
					if( !Utils::IsPointInsideRect( &currPos, pLeftButton ) )
					{
						leftDraggingTouch = -1;
						pLeftButton->setVertexSetColor( btUnPressedColor );
						
						setPieceHorSpeed( 0.0f );
					}
				}
				else if( touchIndex == rightDraggingTouch )
				{
					// Verifica se ainda está colidindo
					if( !Utils::IsPointInsideRect( &currPos, pRightButton ) )
					{
						rightDraggingTouch = -1;
						pRightButton->setVertexSetColor( btUnPressedColor );
						
						setPieceHorSpeed( 0.0f );
					}
				}
			}
		}
		break;
	}
}

/*===========================================================================================
 
MÉTODO isTrackingTouch
	Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja sendo
acompanhado.

============================================================================================*/

int8 PepsiCaeBieno::isTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == ( int32 )hTouch )
			return i;
	}
	return -1;
}

/*===========================================================================================
 
MÉTODO startTrackingTouch
	Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
fazê-lo.

============================================================================================*/

int8 PepsiCaeBieno::startTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == 0 )
		{
			++nActiveTouches;

			trackedTouches[i].Id = ( int32 )hTouch;
			
			CGPoint aux = [hTouch locationInView: NULL];
			trackedTouches[i].unmappedStartPos.set( aux.x, aux.y );

			aux = Utils::MapPointByOrientation( &aux );
			trackedTouches[i].startPos.set( aux.x, aux.y );
			
			if( nActiveTouches == 2 )
				initDistBetweenTouches = ( trackedTouches[0].startPos - trackedTouches[1].startPos ).getModule();

			return i;
		}
	}
	return -1;
}
	
/*===========================================================================================
 
MÉTODO stopTrackingTouch
	Cancela o rastreamento do toque recebido como parâmetro.

============================================================================================*/

void PepsiCaeBieno::stopTrackingTouch( int8 touchIndex )
{
	if( touchIndex < 0 )
		return;

	if( trackedTouches[ touchIndex ].Id != 0 )
	{
		--nActiveTouches;
		trackedTouches[ touchIndex ].Id = 0;
		initDistBetweenTouches = 0.0f;
	}
}

/*===========================================================================================
 
MÉTODO cancelTrackedTouches
	Pára de rastrear os toques atuais.

============================================================================================*/

void PepsiCaeBieno::cancelTrackedTouches( void )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
		trackedTouches[ i ].Id = 0;

	nActiveTouches = 0;
	initDistBetweenTouches = 0.0f;
}

/*===========================================================================================
 
MÉTODO onAccelerate
	Recebe os eventos do acelerômetro.

============================================================================================*/

void PepsiCaeBieno::onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration )
{
	if( !isActive() )
		return;

	switch( gameState )
	{
		case GAME_STATE_PLAYING:
			{
				// Obtém a angulação do aparelho em relação ao eixo de repouso
				float angleX, angleY;
				GameUtils::GetDeviceAngles( &angleX, &angleY, pRestPosition, pAcceleration );

				if( controlType == CONTROL_TYPE_ACCELEROMETERS_ONLY )
				{
					#if INPUT_SCHEME_COLUMN_BY_Y_ANGLE

						// Modifica a coluna da peça que está caindo
						float yPercent = ( angleY + ACCEL_HALF_MOVEMENT_FREEDOM_Y ) / ACCEL_MOVEMENT_FREEDOM_Y;
						yPercent = CLAMP( yPercent, 0.0f, 1.0f );
						
						#if DEBUG
							//LOG( @"Accelerometer yPercent = %0.3f", yPercent );
						#endif

						uint8 goalColumn = ( TOTAL_COLUMNS - 1 ) * yPercent;
						uint8 currColumn = CLAMP( pMovingPiece->getPosition()->x, 0.0f, pPiecesGroup->getWidth() ) / PIECE_WIDTH;
					
						bool ret = true;
						while( ( currColumn != goalColumn ) && ret )
							ret = setMovingPieceColumn( currColumn > goalColumn ? --currColumn : ++currColumn );
					
					#elif INPUT_SCHEME_COLUMN_BY_HOR_SPEED
					
						// Modifica a velocidade horizontal da peça que está caindo
	//					if( pMovingPiece->getPosition()->y > 0.0f )
	//					{
							if( ( angleY >= -ACCEL_NO_MOVE_Y_INTERVAL ) && ( angleY <= ACCEL_NO_MOVE_Y_INTERVAL ) )
							{
								setPieceHorSpeed( 0.0f );
							}
							else
							{
								float yPercent = ( fabsf( angleY ) - ACCEL_NO_MOVE_Y_INTERVAL ) / ( ACCEL_HALF_MOVEMENT_FREEDOM_Y - ACCEL_NO_MOVE_Y_INTERVAL );
								
								if( angleY < 0.0f )
									yPercent = LERP( yPercent, 0.5f, 0.0f );
								else
									yPercent = LERP( yPercent, 0.5f, 1.0f );

								yPercent = CLAMP( yPercent, 0.0f, 1.0f );

								setPieceHorSpeed( LERP( yPercent, PIECE_MIN_HOR_SPEED, PIECE_MAX_HOR_SPEED ) );
							
							#if DEBUG
								//LOG( @"Angle = %.3f, Percent = %.3f, Speed = %.3f", angleY, yPercent, pieceHorizontalSpeed );
							#endif
						}

					#else
						#error É necessário definir no preprocessador umas das opções de input do acelerômetro: INPUT_SCHEME_COLUMN_BY_Y_ANGLE / INPUT_SCHEME_COLUMN_BY_HOR_SPEED
					#endif
				}

				if( ( controlType == CONTROL_TYPE_ACCELEROMETERS_ONLY ) || ( controlType == CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS ) )
				{
					// Modifica a velocidade vertical da peça que está caindo
					float xPercent = ( angleX + ACCEL_HALF_MOVEMENT_FREEDOM_X ) / ACCEL_MOVEMENT_FREEDOM_X;
					xPercent = CLAMP( xPercent, 0.0f, 1.0f );
					
					#if DEBUG
						//LOG( @"Accelerometer xPercent = %0.3f", xPercent );
					#endif
					
					setFallSpeedPercent( 1.0f - xPercent );
				}
			}
			break;

		// OLD
//		case GAME_STATE_BEGIN_LEVEL:
//		case GAME_STATE_SETTING_REST_POS:
//			// Parece estranho, então segue a explicação: AccelerometerManager armazena os valores dos acelerômetros
//			// em um histórico antes de chamar onAccelerate. Logo, estamos deixando que ele acumule valores suficientes
//			// para que a média informada ( este objeto está no mode ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC ) por pAcceleration
//			// seja considerada como posição de repouso
//			if( settingRestPos > 0 )
//			{
//				--settingRestPos;
//			}
//			else
//			{
//				#if DEBUG
//					//LOG( @"RestPos: X = %.3f, Y = %.3f, Z = %.3f", pAcceleration->x, pAcceleration->y, pAcceleration->z );
//				#endif
//
//				AccelerometerManager::GetInstance()->setRestPosition( pAcceleration );
//
//				onStateEnded();
//			}
//			break;
	}
}

/*====================================================================================

MÉTODO getDifficultyLevel
	Obtém qual a fase em termos de dificuldade (o jogador pode atingir aa última fase
em termos de dificuldade mas continuar jogando).

======================================================================================*/

int16 PepsiCaeBieno::getDifficultyLevel( int16 level ) const
{
	return level > MAX_DIFFICULTY_LEVEL ? MAX_DIFFICULTY_LEVEL : level;
}

/*====================================================================================

MÉTODO setMovingPieceColumn
	Determina a coluna da peça que está caindo.

======================================================================================*/

bool PepsiCaeBieno::setMovingPieceColumn( int32 column )
{
	if( !canMove() )
		return false;

	if( column < 0 || column >= TOTAL_COLUMNS )
	{
		// Peça não pode ser movida pois já está no limite (esquerdo ou direito)
		// OLD Utils::Vibrate( VIBRATION_TIME_DEFAULT );
	}
	else
	{
		int16 newYLimit = ( int16 ) ( getAvailableRowsAtColumn( column ) * PIECE_HEIGHT );
		float movingPiecePosY = pMovingPiece->getPosition()->y;
		if( newYLimit >= movingPiecePosY )
		{
			// Peça pode ser movida para o lado
			pMovingPiece->setYLimit( newYLimit );
			movingPieceColumn = ( int8 ) column;
			pMovingPiece->setPosition( column * PIECE_WIDTH, movingPiecePosY );

			return true;
		}
		// OLD
//		else
//		{
//			// Peça não pode ser movida pois há outra peça impedindo a movimentação horizontal
//			Utils::Vibrate( VIBRATION_TIME_DEFAULT );
//		}
	}
	return false;
}

/*====================================================================================

MÉTODO canMove
	Indica se a peça que está caindo pode se movimentar.

======================================================================================*/

bool PepsiCaeBieno::canMove( void ) const
{
	return pMovingPiece->getState() != PIECE_STATE_NONE;
}

/*====================================================================================

MÉTODO getAvailableRowsAtColumn
	Obtém o número de linhas livres em determinada coluna.
 
	@param column índice da coluna a ser testada.
	@return quantidade de linhas livres na coluna recebida, ou -1 caso todas estejam
ocupadas (fim de jogo).

======================================================================================*/

int8 PepsiCaeBieno::getAvailableRowsAtColumn( int32 column ) const
{ 
	int8 index = TOTAL_ROWS - 1;
	while( index >= 0 && pieces[ column ][ index ] != NULL )
		--index;

	return index;
}

/*====================================================================================

MÉTODO setPieceHorSpeed

======================================================================================*/

void PepsiCaeBieno::setPieceHorSpeed( float speed )
{
	pieceHorizontalSpeed = speed;

	if( pieceHorizontalSpeed < PIECE_MIN_HOR_SPEED )
		pieceHorizontalSpeed = PIECE_MIN_HOR_SPEED;
	else if( pieceHorizontalSpeed > PIECE_MAX_HOR_SPEED )
		pieceHorizontalSpeed = PIECE_MAX_HOR_SPEED;
	
	#if DEBUG
		//LOG( @"Velocidade Horizontal: %.3f", pieceHorizontalSpeed );
	#endif
}

/*====================================================================================

MÉTODO setFallSpeedPercent

======================================================================================*/

void PepsiCaeBieno::setFallSpeedPercent( float percent )
{
	fallSpeedPercent = CLAMP( percent, 0.0f, 1.0f );
	pieceVerticalSpeed = LERP( fallSpeedPercent, currentMinSpeed, currentMaxSpeed );
	
	#if DEBUG
		//LOG( @"Piece Vertical Speed = %.3f", pieceVerticalSpeed );
	#endif
}

/*====================================================================================

MÉTODO getNextScoreLabel

======================================================================================*/

ScoreLabel* PepsiCaeBieno::getNextScoreLabel( void ) const
{
	int8 candidate = 0;
	for( int8 i = 0; i < SCORE_LABELS_MAX ; ++i )
	{
		switch( scoreLabels[ i ]->getState() )
		{
			case SCORE_LABEL_STATE_IDLE:
				return scoreLabels[ i ];

			case SCORE_LABEL_STATE_VANISHING:
				candidate = i;
				break;
		}
	}
	return scoreLabels[ candidate ];
}

/*====================================================================================

MÉTODO setState
	Determina o estado da tela de jogo.

======================================================================================*/

void PepsiCaeBieno::setState( GameState newState )
{
	gameLastState = gameState;
	gameState = newState;
	
	timeToNextState = 0.0f;

	// Torna as mensagens invisíveis
	for( uint8 i = OBJ_INDEX_LEVEL_MSG_0 ; i <= OBJ_INDEX_PAUSE_MSG ; ++i )
		getObject( i )->setVisible( false );
	
	// Torna os botões de pause e de opções desabilitados
	RenderableImage* pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_PT ) );
	if( !pOptionsButton->isVisible() )
		pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_ES ) );
	pOptionsButton->setVertexSetColor( Color( ( uint8 )255, 255, 255, 128 ) );

	static_cast< RenderableImage* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( Color( ( uint8 )255, 255, 255, 128 ) );

	switch( gameState )
	{
			case GAME_STATE_BEGIN_LEVEL:
				{
					// OLD
					//showMessage( "%s %d %s %d %s", [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_LEVEL_CAPS], level, [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_GOAL], getLevelScoreNeeded( level ), [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_POINTS] );
					
					getObject( OBJ_INDEX_LEVEL_MSG_0 + level - 1 )->setVisible( true );

					// OLD
//					#if TARGET_IPHONE_SIMULATOR
//						timeToNextState = TIME_MESSAGE;
//					#else
//						settingRestPos = ACCEL_REST_POS_CALC_N_VALUES;
//					#endif
					
					// sin 45 = 0.707106781186548
					Point3f aux( -0.70710678f, 0.0f, -0.70710678f );
					AccelerometerManager::GetInstance()->setRestPosition( &aux );
					
					timeToNextState = TIME_MESSAGE;
				}
				break;
			
			case GAME_STATE_GAME_OVER:
				// OLD
				//showMessage( "%s", [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_GAME_OVER] );
			
				resetButtons();
				getObject( OBJ_INDEX_GAME_OVER_MSG_PT + [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getLanguage] )->setVisible( true );

				[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_GAME_OVER];
			
				timeToNextState = TIME_GAME_OVER;
				break;
			
			case GAME_STATE_LEVEL_CLEARED:
				{
					// OLD
					//showMessage( "%s %d %s", [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_LEVEL_CAPS], level, [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_COMPLETE] );
					
					resetButtons();

					timeToNextState = TIME_MESSAGE;

					updateScore( 0, true );

					[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_LEVEL_COMPLETE];
				}
				break;
			
			case GAME_STATE_GAME_FINISHED:
				timeToNextState = TIME_GAME_FINISHED;
			
				resetButtons();
				getObject( OBJ_INDEX_GAME_OVER_MSG_PT + [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getLanguage] )->setVisible( true );
			
				updateScore( 0, true );
				[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_ENDING];
				break;

			case GAME_STATE_PAUSED:
				pause( true );
				break;

			case GAME_STATE_OPTIONS:
				[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_PAUSE_SCREEN];
				break;
			
			case GAME_STATE_NEW_RECORD:
				// OLD
				//showMessage( "%s", [( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_NEW_RECORD] );
				timeToNextState = DEFAULT_TRANSITION_DURATION;
				break;

			case GAME_STATE_SHOW_SCORE:
				// OLD
				//showMessage( "%s %d", [( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getText: TEXT_INDEX_SCORE], score );
				timeToNextState = DEFAULT_TRANSITION_DURATION;
				break;
			
			case GAME_STATE_PLAYING:
				// Habilita o botão de pause e o botão de opções
				static_cast< RenderableImage* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( Color( COLOR_WHITE ) );
				pOptionsButton->setVertexSetColor( Color( COLOR_WHITE ) );
				break;
		}
}

/*======================================================================================

MÉTODO onStateEnded

======================================================================================*/

void PepsiCaeBieno::onStateEnded( void )
{
	switch( gameState )
	{
		case GAME_STATE_LEVEL_CLEARED:
			loadNextLevel();
			break;
		
		case GAME_STATE_BEGIN_LEVEL:
			setState( GAME_STATE_PLAYING );
			break;
		
		case GAME_STATE_GAME_FINISHED:
		case GAME_STATE_GAME_OVER:
			// OLD
//			setState( GAME_STATE_SHOW_SCORE );
			
			resetButtons();

			[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) onGameOver: score];
			break;
			
		case GAME_STATE_NEW_RECORD:
			// OLD
			//[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) onGameOver: score];
			break;
			
		case GAME_STATE_SHOW_SCORE:
			// OLD
//			if( [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) isHighScore: score] >= 0 )
//				setState( GAME_STATE_NEW_RECORD );
//			else
//				[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) onGameOver: score];
			break;
	}
}

/*======================================================================================

MÉTODO resume
	Retoma o processamento da tela de jogo.

======================================================================================*/

void PepsiCaeBieno::resume( void )
{	
	if( gameState == GAME_STATE_PAUSED )
	{
		gameState = GAME_STATE_PLAYING;
		gameLastState = GAME_STATE_PAUSED;

		pause( false );
	}
	else if( ( gameState == GAME_STATE_OPTIONS ) || ( gameState == GAME_STATE_PLAYING ) )
	{
		gameState = GAME_STATE_PLAYING;
		gameLastState = GAME_STATE_OPTIONS;
		
		// Habilita o botão de pause e o botão de opções
		RenderableImage* pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_PT ) );
		if( !pOptionsButton->isVisible() )
			pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_ES ) );
		
		pOptionsButton->setVertexSetColor( Color( COLOR_WHITE ) );
		
		static_cast< RenderableImage* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( Color( COLOR_WHITE ) );
	}
}

/*======================================================================================

MÉTODO pause
	Coloca ou retira o jogo do estado de GAME_STATE_PAUSED.

======================================================================================*/

void PepsiCaeBieno::pause( bool paused )
{
	static_cast< RenderableImage* >( getObject( OBJ_INDEX_PAUSE_BT ) )->setVertexSetColor( Color( COLOR_WHITE ) );
	
	RenderableImage* pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_PT ) );
	if( !pOptionsButton->isVisible() )
		pOptionsButton = static_cast< RenderableImage* >( getObject( OBJ_INDEX_OPT_BT_ES ) );

	if( paused )
		pOptionsButton->setVertexSetColor( Color( ( uint8 )255, 255, 255, 128 ) );
	else
		pOptionsButton->setVertexSetColor( Color( COLOR_WHITE ) );

	getObject( OBJ_INDEX_PAUSE_MSG )->setVisible( paused );
	pMovingPiece->setVisible( !paused );
	pPiecesGroup->setVisible( !paused );

	for( uint8 i = 0 ; i < SCORE_LABELS_MAX ; ++i )
	{
		if( scoreLabels[i]->getState() != SCORE_LABEL_STATE_IDLE )
			scoreLabels[i]->setVisible( !paused );
	}
}

/*======================================================================================

MÉTODO playGameBkgMusic
	Inicia a reprodução de uma música do jogo.

======================================================================================*/

void PepsiCaeBieno::playGameBkgMusic( void )
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: currSoundIndex];
	
	++currSoundIndex;
	if( currSoundIndex > SOUND_INDEX_AMBIENT_3 )
		currSoundIndex = SOUND_INDEX_AMBIENT_1;
}

/*======================================================================================

MÉTODO loadNextLevel
	Carrega a próxima fase do jogo.

======================================================================================*/

void PepsiCaeBieno::loadNextLevel( void )
{
	loadLevel( level + 1 );
}

/*======================================================================================

MÉTODO getLevel
	Retorna a fase atual do jogo.

======================================================================================*/

int16 PepsiCaeBieno::getLevel( void ) const
{
	return level;
}

/*======================================================================================

MÉTODO getLevelScoreNeeded
	Retorna a pontuação necessária para passar da fase.

======================================================================================*/

int16 PepsiCaeBieno::getLevelScoreNeeded( int16 lv ) const
{
	return lv * SCORE_GOAL_PER_LEVEL;
}

/*======================================================================================

MÉTODO loadLevel
	Carrega uma fase do jogo.

======================================================================================*/

void PepsiCaeBieno::loadLevel( int16 lv )
{
	// Atualiza a fase atual
	level = lv;
	if( level > MAX_LEVEL )
		level = MAX_LEVEL;
	
	updateLevelLabel();

	// Atualiza a pontuação
	updateScore( 0.0f, true );
	
	// Nenhum  botão está pressionado
	resetButtons();

	// Modifica a velocidade com que as peças caem
	float levelPercent = level / MAX_LEVEL;
	currentMinSpeed = LERP( levelPercent, SPEED_MOVE_MIN_EASY, SPEED_MOVE_MAX_EASY );
	currentMaxSpeed = LERP( levelPercent, SPEED_MOVE_MIN_HARD, SPEED_MOVE_MAX_HARD );
	setFallSpeedPercent( 0.5f );
	
	#if	INPUT_SCHEME_COLUMN_BY_HOR_SPEED
		pieceHorizontalSpeed = 0.0f;
		accHorizontalMovement = ( TOTAL_COLUMNS * PIECE_WIDTH ) * 0.5f;
	#endif

	// Reseta o tempo restante
	timeLeft = LEVEL_TIME;
	updateTime( 0.0f );
	
	// Reseta o tabuleiro
	for( int8 i = 0; i < TOTAL_COLUMNS; ++i )
	{
		for( int8 j = 0; j < TOTAL_ROWS ; ++j )
			pieces[ i ][ j ] = NULL;
	}
	pPiecesGroup->reset();
	
	#if DEBUG
		#if IS_CURR_TEST( TEST_FULL_BOARD )
			// Preenche o tabuleiro completamente
			for( int8 i = 0; i < TOTAL_COLUMNS ; ++i )
			{
				for( int8 j = 0; j < TOTAL_ROWS ; ++j )
				{
					if( ( i == ( TOTAL_COLUMNS >> 1 ) ) && ( j == 0 ) )
					{
						pieces[ i ][ j ] = NULL;
						continue;
					}

					pMovingPiece->setCharIndex( 0 );
					pMovingPiece->setPosition( i * PIECE_WIDTH, j * PIECE_HEIGHT );
					
					Piece* pInsertedPiece = pPiecesGroup->insertPiece( pMovingPiece );
					pieces[ i ][ j ] = pInsertedPiece;
				}
			}
		#endif
	#endif
	
	nextCharsCount = 0;
	sortNextPieces();
	updateNextPiece();

	timeToNextPiece = 0.0f;
	insertNewPiece();
	
	// Inicia a fase
	playGameBkgMusic();
	setState( GAME_STATE_BEGIN_LEVEL );
}

/*======================================================================================

MÉTODO updateLevelLabel
	Atualiza o texto do label que exibe a fase atual.

======================================================================================*/

#define LEVEL_LABEL_BUFFER_LEN 16

void PepsiCaeBieno::updateLevelLabel( void )
{
	char buffer[ LEVEL_LABEL_BUFFER_LEN ];
	snprintf( buffer, LEVEL_LABEL_BUFFER_LEN, "%d", level );

	static_cast< LevelInfo* >( getObject( OBJ_INDEX_LEVEL_INFO ) )->updateLevelLabel( buffer );
}

#undef LEVEL_LABEL_BUFFER_LEN

/*======================================================================================

MÉTODO insertNewPiece
	Insere uma nova peça no tabuleiro.

======================================================================================*/

void PepsiCaeBieno::insertNewPiece( void )
{
	// Se o tabuleiro está lotado, é gameOver
	if( !hasEmptySpaceForNewPiece() )
	{
		setState( GAME_STATE_GAME_OVER );
		return;
	}

	// OLD
	//pMovingPiece->setPosition( 0.0f, -( ( PIECE_HEIGHT * 3.0f ) * 0.5f ) );
	
	pMovingPiece->setPosition( 0.0f, -( ( PIECE_HEIGHT * 2.0f ) + ( PIECE_HEIGHT / 3.0f ) ) );
	
	#if INPUT_SCHEME_COLUMN_BY_HOR_SPEED
		pieceHorizontalSpeed = 0.0f;
		accHorizontalMovement = ( TOTAL_COLUMNS * PIECE_WIDTH ) * 0.5f;
	#endif
		
	if( timeToNextPiece <= 0.0f )
	{
		pMovingPiece->setState( PIECE_STATE_MOVING );

		pMovingPiece->setCharIndex( pNextPiece->getCharIndex() );
		setMovingPieceColumn( TOTAL_COLUMNS >> 1 );
		
		if( nextCharsCount <= 0 )
			sortNextPieces();

		updateNextPiece();
	}
	else
	{
		pMovingPiece->setState( PIECE_STATE_NONE );
	}
}

/*======================================================================================

MÉTODO hasEmptySpaceForNewPiece
	Indica se ainda há espaço livre no tabuleiro para a peça que está caindo.

======================================================================================*/

bool PepsiCaeBieno::hasEmptySpaceForNewPiece( void ) const
{
	for( uint8 i = 0 ; i < TOTAL_COLUMNS ; ++i )
	{
		if( getAvailableRowsAtColumn( i ) >= 0 )
			return true;
	}
	return false;
}

/*======================================================================================

MÉTODO updateNextPiece
	Atualiza o componente que exibe a próxima peça.

======================================================================================*/

void PepsiCaeBieno::updateNextPiece( void )
{
	pNextPiece->setChar( nextChars[ --nextCharsCount ] );
	
	pNextPiece->setSize( 1.0f, 1.0f, 1.0f );
	pNextPiece->setScale( NEXT_PIECE_WIDTH, NEXT_PIECE_HEIGHT, 1.0f );
}

/*======================================================================================

MÉTODO updateScore
	Atualiza a pontuação que deve ser exibida pelo respectivo label.

	@param equalize indica se a pontuação exibida deve ser automaticamente igualada à
pontuação real.

======================================================================================*/

void PepsiCaeBieno::updateScore( float timeElapsed, bool equalize )
{
	if( static_cast< int32 >( scoreShown ) < score )
	{
		if( equalize )
		{
			scoreShown = score;
		}
		else
		{
			scoreShown += scoreSpeed * timeElapsed;
			
			int32 aux = static_cast< int32 >( scoreShown );
			if( aux >= score )
			{
				if( aux > SCORE_SHOWN_MAX )
					scoreShown = SCORE_SHOWN_MAX;
				
				scoreShown = score;
			}
		}
		updateScoreLabel();
	}
}

/*======================================================================================

MÉTODO updateScoreLabel
	Atualiza o texto do label que exibe a pontuação do jogador.

======================================================================================*/

#define UPDATE_SCORE_LABEL_BUFFER_LEN 128

void PepsiCaeBieno::updateScoreLabel( void )
{	
	// É meio cagado fazer este cálcula a cada vez que chamamos updateScoreLabel(), mas enfim...
	char buffer[ UPDATE_SCORE_LABEL_BUFFER_LEN ];
	snprintf( buffer, UPDATE_SCORE_LABEL_BUFFER_LEN, "%d", MAX_POINTS );
	uint8 maxPointsLen = strlen( buffer );
	
	snprintf( buffer, UPDATE_SCORE_LABEL_BUFFER_LEN, "%0*d", maxPointsLen, static_cast< int32 >( scoreShown ) );
	static_cast< LevelInfo* >( getObject( OBJ_INDEX_LEVEL_INFO ) )->updatePointsLabel( buffer );
}

#undef UPDATE_SCORE_LABEL_BUFFER_LEN

/*======================================================================================

MÉTODO updateTime
	Atualiza a variável que controla o tempo restante da fase e o label que o exibe.

======================================================================================*/	

#define UPDATE_TIME_BUFFER_LEN 16

void PepsiCaeBieno::updateTime( float timeElapsed )
{
	if( timeLeft > 0.0f )
	{
		timeLeft -= timeElapsed;

		if( timeLeft <= 0.0f )
		{
			timeLeft = 0.0f;

			// OLD
			//setState( GAME_STATE_GAME_OVER );

			if( level == MAX_LEVEL )
				setState( GAME_STATE_GAME_FINISHED );
			else
				setState( GAME_STATE_LEVEL_CLEARED );
		}
	}

	int32 minutes = static_cast< int32 >( timeLeft / 60.0f );
	int32 seconds = static_cast< int32 >( timeLeft - ( minutes * 60.0f ) );
	
	char buffer[ UPDATE_TIME_BUFFER_LEN ];
	snprintf( buffer, UPDATE_TIME_BUFFER_LEN, "%d:%02d", minutes, seconds );
	
	static_cast< LevelInfo* >( getObject( OBJ_INDEX_LEVEL_INFO ) )->updateTimeRemainingLabel( buffer );
}

#undef UPDATE_TIME_BUFFER_LEN

/*======================================================================================

MÉTODO changeScore
	Incrementa a pontuação e atualiza as variáveis e labels correspondentes.

	@param diff variação na pontuação (positiva ou negativa).

======================================================================================*/	

void PepsiCaeBieno::changeScore( int32 diff )
{
	score += diff;
	scoreSpeed = ( score - scoreShown ) * 0.5f;
}

/*======================================================================================

MÉTODO checkWords
	Verifica se o jogador conseguiu formar uma nova palavra na coluna 'column' ou na
linha 'row'.

======================================================================================*/

void PepsiCaeBieno::checkWords( int32 column, int32 row )
{
	// verifica sequências na horizontal
	int32 start = column;
	int32 end = column;

	while( start > 0 && pieces[ start - 1 ][ row ] != NULL )
		--start;

	while( end < ( TOTAL_COLUMNS - 1 ) && pieces[ end + 1 ][ row ] != NULL )
		++end;

	int32 maxLength = end - start + 1;
	int32 currentLength = maxLength;
	
	{ // Fornece um escopo para a string maxHorString. Assim ela será destruída antes de alocarmos maxVerString, economizando memória

		std::string maxHorString( maxLength, ' ' );
		
		while( currentLength >= WORD_MIN_LENGTH )
		{
			int32 positions = maxLength - currentLength;

			// testa as possíveis palavras
			for( int8 i = 0; i <= positions; ++i )
			{
				// verifica somente as combinações que incluam a nova peça inserida
				if( ( start + i ) <= column && column < ( start + i + currentLength ) )
				{
					// monta a string na ordem normal (esquerda para a direita) - o teste já verifica a sequência inversa
					for( int8 j = i; j < i + currentLength; ++j )
						maxHorString[ j - i ] = pieces[ start + j ][ row ]->getChar();

					std::string aux( maxHorString, 0, currentLength );
					int8 wordType = hasWord( aux.c_str() );

					if( wordType != WORD_INVALID )
					{
						int16 startColumn = start + i;
						int16 endColumn = startColumn + currentLength;

						wordMade( currentLength, pieces[ startColumn ][ row ], pieces[ endColumn - 1 ][ row ] );

						// OLD
//						for( int8 j = i; j < i + currentLength; ++j )
//						{
//							int32 c = start + j;
//							pieces[ c ][ row ]->vanish( wordType == WORD_REGULAR ? j - i : i + currentLength - 1 - j );
//
//							// faz as peças acima caírem
//							for( int32 k = row - 1; k >= 0; --k )
//							{
//								if( pieces[ c ][ k ] != NULL )
//									pieces[ c ][ k ]->fall( 1 );
//
//								pieces[ c ][ k + 1 ] = pieces[ c ][ k ];
//							}
//							pieces[ c ][ 0 ] = NULL;
//						}

						for( int8 c = 0; c < TOTAL_COLUMNS; ++c )
						{
							if( pieces[ c ][ row ] != NULL )
							{
								int32 steps = 0;
								if( c >= startColumn && c < endColumn )
								{
									steps = wordType == WORD_REGULAR ? c - startColumn : endColumn - 1 - c;
									pieces[ c ][ row ]->vanish( true, steps );
								}
								else
								{
									steps = currentLength;
									pieces[ c ][ row ]->vanish( false, currentLength );
								}

								// Faz as peças acima caírem
								for( int32 k = row - 1 ; k >= 0 ; --k )
								{
									if( pieces[ c ][ k ] != NULL )
										pieces[ c ][ k ]->fall( 1, steps );

									pieces[ c ][ k + 1 ] = pieces[ c ][ k ];
								}
								pieces[ c ][ 0 ] = NULL;
							}
						}


						#if DEBUG
							printPieces( "DEPOIS HORIZONTAL");
						#endif
						return;
					}
				}
			}
			--currentLength;
		}
	}

	// verifica sequências na vertical
	start = row;
	end = row;

	while( start > 0 && pieces[ column ][ start - 1 ] != NULL )
		--start;

	while( end < ( TOTAL_ROWS - 1 ) && pieces[ column ][ end + 1 ] != NULL )
		++end;

	maxLength = end - start + 1;
	currentLength = maxLength;
	
	std::string maxVerString( maxLength, ' ' );

	while( currentLength >= WORD_MIN_LENGTH )
	{
		int32 positions = maxLength - currentLength;

		// testa as possíveis palavras
		for( int8 i = 0; i <= positions; ++i )
		{
			// verifica somente as combinaçıes que incluam a nova peça inserida
			if( ( start + i ) <= row && row < ( start + i + currentLength ) )
			{
				// monta a string na ordem normal (cima para baixo) - o teste jç verifica a sequência inversa
				for( int8 j = i; j < i + currentLength; ++j )
					maxVerString[ j - i ] = pieces[ column ][ start + j ]->getChar();

				std::string aux( maxVerString, 0, currentLength );
				int8 wordType = hasWord( aux.c_str() );

				if( wordType != WORD_INVALID )
				{
					wordMade( currentLength, pieces[ column ][ start ], pieces[ column ][ start + currentLength - 1 ] );

					// OLD
//					for( int8 j = i; j < i + currentLength; ++j )
//						pieces[ column ][ start + j ]->vanish( wordType == WORD_REGULAR ? j - i : i + currentLength - 1 - j );
//
//					// Faz as peças acima caírem
//					for( int32 k = row - 1; k >= 0; --k )
//					{
//						if( pieces[ column ][ k ] != NULL )
//							pieces[ column ][ k ]->fall( currentLength );
//
//						pieces[ column ][ k + currentLength ] = pieces[ column ][ k ];
//					}
//
//					pieces[ column ][ 0 ] = NULL;
					
					for( int8 j = i; j < i + currentLength; ++j )
					{
						pieces[ column ][ start + j ]->vanish( true, wordType == WORD_REGULAR ? j - i : i + currentLength - 1 - j );
						pieces[ column ][ start + j ] = NULL;
					}
					
					for( int8 j = start + i + currentLength; j < TOTAL_ROWS; ++j )
					{
						pieces[ column ][ j ]->vanish( false, currentLength );
						pieces[ column ][ j ] = NULL;
					}

					return;
				}
			}
		}
		--currentLength;
	}
}

/*======================================================================================

MÉTODO printPieces
	Imprime as pelas que estão no tabuleiro.

======================================================================================*/

#if DEBUG

void PepsiCaeBieno::printPieces( const char* pTitle ) const
{
	LOG( @"%s", pTitle );

	char pieceChar[3];
	for( int8 r = 0; r < TOTAL_ROWS; ++r )
	{
		for( int8 c = 0; c < TOTAL_COLUMNS; ++c )
		{
			if( pieces[ c ][ r ] != NULL )
			{
				pieceChar[0] = pieces[ c ][ r ]->getChar();
				pieceChar[1] = ' ';
				pieceChar[2] = NULL;
			
				LOG( @"%s",  pieceChar );
			}
			else
			{
				LOG( @"- " );
			}
		}

		LOG( @"" );
	}
}

#endif

/*======================================================================================

MÉTODO wordMade
	Indica que o jogador conseguiu formar uma palavra.

======================================================================================*/

void PepsiCaeBieno::wordMade( int32 length, const Piece* pStart, const Piece* pEnd )
{
	// Calcula a posição onde o label deverá aparecer
	Point3f pos( PIECES_GROUP_POS_X + ( ( pStart->getPosition()->x + ( pEnd->getPosition()->x + pEnd->getWidth() ) ) * 0.5f ),
				 PIECES_GROUP_POS_Y + pStart->getPosition()->y );

	// Pontuação dada por cada tamanho de palavra (começando por WORD_MIN_LENGTH)
	int16 scorePerWord[] = { 25, 50, 100, 150, 200, 300, 375, 500, 1000 };
	
	// Indica a pontuação que o label deverá exibir
	getNextScoreLabel()->setScore( pos, scorePerWord[ length - WORD_MIN_LENGTH ] );

	// Garante um tempo para que a animação seja executada antes de outra peça cair
	timeToNextPiece = length * VANISH_TIME;
}

/*======================================================================================

MÉTODO hasWord
	Verifica se a palavra formada é existente.

======================================================================================*/

int8 PepsiCaeBieno::hasWord( const char* pWord ) const
{
	for( int8 i = 0; i < ( TOTAL_WORDS << 1 ) ; ++i )
	{
		if( strcmp( words[ i ], pWord ) == 0 )
		{
			// Neste componente possuímos o dobro das palavras, já que contamos as palavras invertidas. No entanto,
			// WordsGroup possui apenas as palavras originais. Por isso dividimos i por 2
			pWordsGroup->addWord( i >> 1 );

			// Se i é ímpar, é uma palvra invertida
			return ( ( i & 1 ) != 0 ? WORD_REVERSE : WORD_REGULAR );
		}
	}
	return WORD_INVALID;
}

/*======================================================================================

MÉTODO showMessage

======================================================================================*/

// OLD
//#define SHOW_MESSAGE_BUFFER_LEN 256
//
//void PepsiCaeBieno::showMessage( const char* pFormatStr, ... )
//{
//	va_list params;
//	va_start( params, pFormatStr );
//
//	char buffer[ SHOW_MESSAGE_BUFFER_LEN ];
//	vsnprintf( buffer, SHOW_MESSAGE_BUFFER_LEN, pFormatStr, params );
//	
//	va_end( params );
//
//	pLabelMessage->setAlignment( ALIGN_HOR_CENTER );
//	pLabelMessage->setText( buffer );
//	pLabelMessage->setSize( SCREEN_WIDTH, pLabelMessage->getTextTotalHeight() );
//	pLabelMessage->setPosition( offsetX, size.y - ( pLabelMessage->getHeight() ) >> 1 );
//	pLabelMessage->setVisible( true );
//}
//
//#undef SHOW_MESSAGE_BUFFER_LEN

/*======================================================================================

MÉTODO getScore
	Retorna a pontuação atual do jogador.

======================================================================================*/

int32 PepsiCaeBieno::getScore( void ) const
{
	return score;
}

/*======================================================================================

MÉTODO sortNextPieces
	Embaralha palavras para gerarmos as letras que cairão na tela.

======================================================================================*/

void PepsiCaeBieno::sortNextPieces( void )
{	
	for( uint8 i = 0; i < WORDS_TO_MIX ; ++i )
	{
		int32 rand = Random::GetInt( 0, ( TOTAL_WORDS << 1 ) - 1 );

		#if DEBUG
			LOG( @"Random Word = %d", rand );
		#endif

		char* pChars = words[ rand ];
		uint16 nChars = strlen( pChars );

		strncpy( &nextChars[ nextCharsCount ], pChars, nChars );
		
		nextCharsCount += nChars;

		#if DEBUG
			LOG( @"Sorted Word %d: %s", i, pChars );
		#endif
	}

	// Troca a posição das letras
	int32 limit = nextCharsCount << 2;
	for( uint8 i = 0; i < limit; ++i )
	{
		int32 index1 = Random::GetInt( 0, nextCharsCount - 1 );
		int32 index2 = Random::GetInt( 0, nextCharsCount - 1 );

		char temp = nextChars[ index1 ];
		nextChars[ index1 ] = nextChars[ index2 ];
		nextChars[ index2 ] = temp;
	}

	#if DEBUG
		// OBS : Não tem '\0' no fim, então não podemos mandar imprimir uma string !!!
		for( uint8 i = 0 ; i < nextCharsCount ; ++i )
			LOG( @"Next Char %d: %c", i, nextChars[ i ] );
	#endif
}
