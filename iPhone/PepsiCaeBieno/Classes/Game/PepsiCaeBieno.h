/*
 *  PepsiCaeBieno.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 2/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PEPSI_CAE_BIENO_H
#define PEPSI_CAE_BIENO_H 1

#include "AccelerometerListener.h"
#include "EventListener.h"
#include "OrthoCamera.h"
#include "Scene.h"
#include "Touch.h"

#include "Config.h"
#include "ControlType.h"
#include "Piece.h"

// Forward Declarations
class AudioSource;
class Label;
class ScoreLabel;
class WordsGroup;

// Número máximo de labels 'dinâmicos' que exibem a pontuação de cada jogada no próprio tabuleiro
#define SCORE_LABELS_MAX 5

class PepsiCaeBieno : public Scene, public EventListener, public AccelerometerListener
{
	public:
		// Construtor
		PepsiCaeBieno( const ControlType& controlType );
	
		// Destrutor
		virtual ~PepsiCaeBieno( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
	
		// Recebe os eventos do acelerômetro
		virtual void onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration );
	
		// Incrementa a pontuação e atualiza as variáveis e labels correspondentes
		void changeScore( int32 diff );
	
		// Retorna a palavra contida no índice 'index' do array de palavras que devem ser completadas
		const char* getWord( uint16 index ) const;
	
		// Retoma o processamento da tela de jogo
		void resume( void );
	
		// Reinicia a tela de jogo
		void reset( uint8 languageIndex );
	
		// Inicia a reprodução de uma música do jogo
		void playGameBkgMusic( void );
	
		// Salva a pontuação atual caso esta seja um recorde
		void saveScoreIfRecord( void );

	private:
		// Estados da partida
		enum GameState
		{
			GAME_STATE_NONE				=   0,
			GAME_STATE_BEGIN_LEVEL		=   1,
			GAME_STATE_PLAYING			=   2,
			GAME_STATE_LEVEL_CLEARED	=   3,
			GAME_STATE_GAME_OVER		=   4,
			GAME_STATE_PAUSED			=   5,
			GAME_STATE_OPTIONS			=	6,
			GAME_STATE_NEW_RECORD		=   7,
			GAME_STATE_SHOW_SCORE		=   8,
			GAME_STATE_SETTING_REST_POS	=   9,
			GAME_STATE_GAME_FINISHED	=  10
		};
	
		// Eventos de input do jogo
		enum GameEvent
		{
			GAME_EVENT_SET_PIECE_ACC = 0,
			GAME_EVENT_MOVE_PIECE_LEFT,
			GAME_EVENT_MOVE_PIECE_RIGHT,
			GAME_EVENT_PAUSE
		};
	
		// Inicializa o objeto
		bool build( void );
	
		// Coloca todos os botões em seus estados iniciais
		void resetButtons( void );
	
		// Carrega as palavras que podem ser formadas já no idioma correto
		void loadWords( void );
	
		// Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja
		// sendo acompanhado
		int8 isTrackingTouch( const UITouch* hTouch );
		
		// Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
		// fazê-lo
		int8 startTrackingTouch( const UITouch* hTouch );
	
		// Cancela o rastreamento do toque recebido como parâmetro
		void stopTrackingTouch( int8 touchIndex );
	
		// Pára de rastrear os toques atuais
		void cancelTrackedTouches( void );
	
		// Modifica o estado da partida
		void setState( GameState newState );
		void onStateEnded( void );

		// Trata eventos de início de toque
		void onNewTouch( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de fim de toque
		void onTouchEnded( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de movimentação de toques
		void onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos );
	
		// Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
		// jogo
		void getPickingRay( Ray& ray, const Point3f& p ) const;
	
		// Carrega uma fase
		void loadLevel( int16 lv );
	
		// Carrega a próxima fase
		void loadNextLevel( void );

		// Obtém o nível atual
		int16 getLevel( void ) const;
	
		// Calcula a pontuação mínima necessçria para uma determinada fase através de uma
		// equação quadrçtica
		int32 getLevelScoreTarget( int16 level ) const;
	
		// Obtém qual a fase em termos de dificuldade (o jogador pode atingir aa última
		// fase em termos de dificuldade mas continuar jogando)
		int16 getDifficultyLevel( int16 level ) const;
	
		// Insere uma nova peça no tabuleiro
		void insertNewPiece( void );
	
		// Indica se ainda há espaço livre no tabuleiro para a peça que está caindo
		bool hasEmptySpaceForNewPiece( void ) const;
	
		// Atualiza o componente que exibe a próxima peça
		void updateNextPiece( void );
	
		// Indica que o jogador conseguiu formar uma palavra
		void wordMade( int32 length, const Piece* pStart, const Piece* pEnd );

		// Verifica se a palavra formada é existente
		int8 hasWord( const char* pWord ) const;

		// Retorna a pontuação do jogador
		int32 getScore( void ) const;
	
		void setPieceHorSpeed( float speed );
		void setFallSpeedPercent( float percent );
	
		ScoreLabel* getNextScoreLabel( void ) const;
	
		// Determina a coluna da peça que está caindo
		bool setMovingPieceColumn( int32 column );
	
		// OLD
		// Exibe uma mensagem centralizada na tela
		//void showMessage( const char* pFormatStr, ... );
	
		// Verifica se o jogador conseguiu formar uma nova palavra na coluna 'column' ou na linha 'row'
		void checkWords( int32 column, int32 row );
	
		// Atualiza a pontuação que deve ser exibida pelo respectivo label
		void updateScore( float timeElapsed, bool equalize );

		// Atualiza o texto do label que exibe a pontuação do jogador
		void updateScoreLabel( void );
	
		// Atualiza a variável que controla o tempo restante da fase e o label que o exibe
		void updateTime( float timeElapsed );
	
		// ObtÈm o n˙mero de colunas livres em determinada coluna.
		int8 getAvailableRowsAtColumn( int32 column ) const;
	
		// Embaralha palavras para gerarmos as letras que cairão na tela
		void sortNextPieces( void );
	
		// Atualiza o texto do label que exibe a fase atual
		void updateLevelLabel( void );
	
		// Retorna a pontuação necessária para passar da fase
		int16 getLevelScoreNeeded( int16 lv ) const;
	
		// Indica se a peça que está caindo pode se movimentar
		bool canMove( void ) const;

		// Coloca ou retira o jogo do estado de GAME_STATE_PAUSED
		void pause( bool paused );

		#if DEBUG	
			// Imprime as pelas que estão no tabuleiro
			void printPieces( const char* pTitle ) const;
		#endif
	
		// Estado do jogo
		GameState gameState, gameLastState;
	
		// Variçveis utilizadas para o controle dos gestos realizados no touchscreen
		uint8 nActiveTouches;
		float initDistBetweenTouches;
		Touch trackedTouches[ MAX_TOUCHES ];
	
		// Índices do toque que está fazendo o drag'n'drop da barra de palavras
		// OBS: Isso está TRISTE!!!!!!!!!!! Seria MTO BOM generalizar...
		int8 wordsDraggingTouch, pauseDraggingTouch, leftDraggingTouch, rightDraggingTouch, optionsDraggingTouch;
	
		// Indica se estamos determinando a posição de repouso do device dinamicamente
		int8 settingRestPos; 
	
		// Fase atual
		int16 level;
	
		// Velocidades mínima e máxima da fase atual
		float currentMinSpeed, currentMaxSpeed;
	
		// Controlador de estados de jogo que possuem duração
		float timeToNextState;
	
		// Tempo restante da fase
		float timeLeft;
	
		// Tempo de espera até inserir a nova peça (evita problemas com peças caindo durante animações)
		float timeToNextPiece;
	
		// Label que exibe o tempo restante da fase
		Label* pTimeLabel;

		// Pontuação do jogador
		int32 score;

		// Pontuação exibida atualmente
		float scoreShown;
	
		// Velocidade de atualização da pontuação
		float scoreSpeed;

		// Label que indica a pontuação e o multiplicador de pontos
		Label* pScoreLabel;

		// Palavras válidas (o tamanho È duplicado pois as palavras podem ser testadas na ordem inversa).
		// Obs: não foi possÌvel utilizar um Hashtable pois, apesar dos conte˙dos das chaves (strings) serem
		//iguais, os objetos são diferentes, logo uma chave usada para busca posterior nunca seria encontrada
		char words[ TOTAL_WORDS << 1 ][ WORD_MAX_LENGTH + 1 ];
	
		// Próximos caracteres a aparecer na tela	
		char nextChars[ ( WORD_MAX_LENGTH + 1 ) * WORDS_TO_MIX ];
	
		// Número de caracteres contidos no array nextChars
		int16 nextCharsCount;

		// Array de peças já encaixadas
		Piece* pieces[ TOTAL_COLUMNS ][ TOTAL_ROWS ];

		// Conjunto das peças que estão no tabuleiro
		PieceGroup* pPiecesGroup;

		// Labels que aparecem no tabuleiro para exibir as pontuações feitas por cada palavra terminada
		ScoreLabel* scoreLabels[ SCORE_LABELS_MAX ];

		// Velocidade vertical da peça
		float pieceVerticalSpeed;
	
		#if INPUT_SCHEME_COLUMN_BY_HOR_SPEED
			// Velocidade horizontal da peça
			float pieceHorizontalSpeed;
	
			// Acumulador de movimentação em x
			float accHorizontalMovement;
		#endif

		// Peça que o jogador movimenta
		Piece* pMovingPiece;

		// Indicador da próxima peça
		Piece* pNextPiece;

		// Índice da coluna atual da peça que está caindo
		int8 movingPieceColumn;

		// Porcentagem atual da velocidade de queda da peça atual
		float fallSpeedPercent;

		// Grupo das palavras
		WordsGroup* pWordsGroup;
	
		// Música ambiente que está sendo tocada
		uint8 currSoundIndex;
	
		// Tipo de input atual
		const ControlType& controlType;
};

// Implementação dos métodos inline

// Retorna a palavra 'index' contida no array de palavras aceitas pelo jogo
inline const char* PepsiCaeBieno::getWord( uint16 index ) const
{
	return words[ index ];
};

#endif
