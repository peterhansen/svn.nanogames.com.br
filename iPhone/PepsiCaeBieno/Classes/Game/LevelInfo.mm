#include "LevelInfo.h"

#include "PepsiCaeBienoAppDelegate.h"

#include "Exceptions.h"
#include "Label.h"
#include "RenderableImage.h"

// Número de objetos no grupo
#define LEVEL_INFO_N_OBJS 5

// Índices dos objetos no grupo
#define LEVEL_INFO_OBJ_INDEX_BKG_PT			0
#define LEVEL_INFO_OBJ_INDEX_BKG_ES			1
#define LEVEL_INFO_OBJ_INDEX_LABEL_TIME		2
#define LEVEL_INFO_OBJ_INDEX_LABEL_LEVEL	3
#define LEVEL_INFO_OBJ_INDEX_LABEL_POINTS	4

// Macro para reduzir a digitação em build()
#define LEVEL_INFO_CHECKED_INSERT( pObj )	if( !pObj || !insertObject( pObj ) )	\
											{										\
												delete pObj;						\
												goto Error;							\
											}

// Configurações dos elementos do grupo
#define LEVEL_INFO_LABEL_TIME_POS_X		58.0f
#define LEVEL_INFO_LABEL_TIME_POS_Y		18.0f
#define LEVEL_INFO_LABEL_TIME_WIDTH		49.0f
#define LEVEL_INFO_LABEL_TIME_HEIGHT	16.0f

#define LEVEL_INFO_LABEL_LEVEL_POS_X	91.0f
#define LEVEL_INFO_LABEL_LEVEL_POS_Y	41.0f
#define LEVEL_INFO_LABEL_LEVEL_WIDTH	16.0f
#define LEVEL_INFO_LABEL_LEVEL_HEIGHT	18.0f

#define LEVEL_INFO_LABEL_POINTS_POS_X	55.0f
#define LEVEL_INFO_LABEL_POINTS_POS_Y	76.0f
#define LEVEL_INFO_LABEL_POINTS_WIDTH	52.0f
#define LEVEL_INFO_LABEL_POINTS_HEIGHT	14.0f

// Espaçamento dos caracteres dos labels
#define LEVEL_INFO_FONTS_CHAR_SPACEMENT 0.0f

/*======================================================================================

CONSTRUTOR

======================================================================================*/

LevelInfo::LevelInfo( void )
		  : ObjectGroup( LEVEL_INFO_N_OBJS )
{
	build();
}

/*======================================================================================

MÉTODO updateTimeRemainingLabel
	Atualiza o label que exibe o tempo restante da fase.

======================================================================================*/

void LevelInfo::updateTimeRemainingLabel( const char* pText )
{
	static_cast< Label* >( getObject( LEVEL_INFO_OBJ_INDEX_LABEL_TIME ) )->setText( pText, false, false );
}

/*======================================================================================

MÉTODO updatePointsLabel
	Atualiza o label que exibe os pontos do jogador.

======================================================================================*/

void LevelInfo::updatePointsLabel( const char* pText )
{
	static_cast< Label* >( getObject( LEVEL_INFO_OBJ_INDEX_LABEL_POINTS ) )->setText( pText, false, false );
}

/*======================================================================================

MÉTODO updateLevelLabel
	Atualiza o label que exibe a fase atual.

======================================================================================*/

void LevelInfo::updateLevelLabel( const char* pText )
{
	static_cast< Label* >( getObject( LEVEL_INFO_OBJ_INDEX_LABEL_LEVEL ) )->setText( pText, false, false );
}

/*======================================================================================

MÉTODO build
	Inicializa o objeto.

======================================================================================*/

void LevelInfo::build( void )
{
	{ // Evita erros de compilação por causa dos gotos incluídos na macro LEVEL_INFO_CHECKED_INSERT

		char bkgPath[] = "infoPT";
		for( uint8 i = 0 ; i < APP_N_LANGUAGES ; ++i )
		{
			RenderableImage* pLevelInfoBkg = new RenderableImage( bkgPath );
			LEVEL_INFO_CHECKED_INSERT( pLevelInfoBkg );
			
			bkgPath[4] = 'E';
			bkgPath[5] = 'S';
		}

		// Cria os labels que irão exibir os dados da fase
		#if FONTS_TRANSPARENT

			Font* pFontNumbersBig = [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_BIG];

			Label* pLbTime = new Label( pFontNumbersBig, false );
			LEVEL_INFO_CHECKED_INSERT( pLbTime );
			
			Label* pLbLevel = new Label( pFontNumbersBig, false );
			LEVEL_INFO_CHECKED_INSERT( pLbLevel )
			
			Label* pLbPoints = new Label( pFontNumbersBig, false );
			LEVEL_INFO_CHECKED_INSERT( pLbPoints );
		
		#elif FONTS_OPAQUE
		
			Font* pFontNumbersBigBlue = [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_BIG_BLUE];
			Font* pFontNumbersBigRed = [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_BIG_RED];

			Label* pLbTime = new Label( pFontNumbersBigRed, false );
			LEVEL_INFO_CHECKED_INSERT( pLbTime );
			
			Label* pLbLevel = new Label( pFontNumbersBigRed, false );
			LEVEL_INFO_CHECKED_INSERT( pLbLevel )
			
			Label* pLbPoints = new Label( pFontNumbersBigBlue, false );
			LEVEL_INFO_CHECKED_INSERT( pLbPoints );

		#endif

		// Configura os labels do grupo
		pLbTime->setCharSpacement( LEVEL_INFO_FONTS_CHAR_SPACEMENT );
		pLbTime->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pLbTime->setSize( LEVEL_INFO_LABEL_TIME_WIDTH, LEVEL_INFO_LABEL_TIME_HEIGHT );
		pLbTime->setPosition( LEVEL_INFO_LABEL_TIME_POS_X, LEVEL_INFO_LABEL_TIME_POS_Y );
		
		pLbLevel->setCharSpacement( LEVEL_INFO_FONTS_CHAR_SPACEMENT );
		pLbLevel->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pLbLevel->setSize( LEVEL_INFO_LABEL_LEVEL_WIDTH, LEVEL_INFO_LABEL_LEVEL_HEIGHT );
		pLbLevel->setPosition( LEVEL_INFO_LABEL_LEVEL_POS_X, LEVEL_INFO_LABEL_LEVEL_POS_Y );
		
		pLbPoints->setCharSpacement( LEVEL_INFO_FONTS_CHAR_SPACEMENT );
		pLbPoints->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pLbPoints->setSize( LEVEL_INFO_LABEL_POINTS_WIDTH, LEVEL_INFO_LABEL_POINTS_HEIGHT );
		pLbPoints->setPosition( LEVEL_INFO_LABEL_POINTS_POS_X, LEVEL_INFO_LABEL_POINTS_POS_Y );
		
		return;
		
	} // Evita erros de compilação por causa dos gotos incluídos na macro LEVEL_INFO_CHECKED_INSERT
	
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "LevelInfo::build(): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*======================================================================================

MÉTODO reset
	Reinicia o componente.

======================================================================================*/

void LevelInfo::reset( uint8 languageIndex )
{
	// Utiliza a interface correta para o idioma
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;
	getObject( LEVEL_INFO_OBJ_INDEX_BKG_PT )->setVisible( isPortuguese );
	getObject( LEVEL_INFO_OBJ_INDEX_BKG_ES )->setVisible( !isPortuguese );
}
