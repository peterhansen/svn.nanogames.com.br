#include "WordsGroup.h"

#include "PepsiCaeBieno.h"
#include "PepsiCaeBienoAppDelegate.h"

#include "Exceptions.h"
#include "Label.h"

// Número de objetos no grupo
#define WORDS_GROUP_N_OBJECTS 4

// Índices dos objetos no grupo
#define WORDS_GROUP_OBJ_INDEX_BKG						0
#define WORDS_GROUP_OBJ_INDEX_FRONT_PT					1
#define WORDS_GROUP_OBJ_INDEX_FRONT_ES					2
#define WORDS_GROUP_OBJ_INDEX_TOTAL_WORDS_MADE_LABEL	3

// Índices dos objetos no grupo do background
#define WORDS_GROUP_BKG_OBJ_INDEX_FIRST_TIMES_LABEL 1
#define WORDS_GROUP_BKG_OBJ_INDEX_FIRST_WORDS_LABEL ( WORDS_GROUP_BKG_OBJ_INDEX_FIRST_TIMES_LABEL + TOTAL_WORDS )

// Movimentação do background permitida no eixo Y
#define WORDS_GROUP_BKG_MAX_Y 31.0f // OLD 20.0f

// Posicionamento do background no eixo X
#define WORDS_GROUP_BKG_POS_X 6.0f // OLD 5.0f

// Configurações das texturas dos objetos do grupo
#define WORDS_GROUP_BKG_TEXTURE_FRAME_WIDTH		 93.0f
#define WORDS_GROUP_BKG_TEXTURE_FRAME_HEIGHT	220.0f // OLD 330.0f ( Com 30 palavras )

#define WORDS_GROUP_FRONT_TEXTURE_FRAME_WIDTH  118.0f // OLD 117.0f
#define WORDS_GROUP_FRONT_TEXTURE_FRAME_HEIGHT 176.0f // OLD 241.0f

// Viewport do background
#define WORDS_GROUP_BKG_VIEWPORT_POS_X  ( WORDS_GROUP_BKG_POS_X + 1 )
#define WORDS_GROUP_BKG_VIEWPORT_POS_Y	WORDS_GROUP_BKG_MAX_Y
#define WORDS_GROUP_BKG_VIEWPORT_WIDTH    91.0f
#define WORDS_GROUP_BKG_VIEWPORT_HEIGHT  132.0f // OLD 197.0f

// Configurações dos labels que exibem quantas vezes uma determinada palavra já foi formada
#define WORDS_GROUP_TIMES_LABELS_POS_X		-1.0f
#define WORDS_GROUP_TIMES_LABELS_WIDTH		16.0f
#define WORDS_GROUP_TIMES_LABELS_HEIGHT		11.0f

// Configurações dos labels que exibem as palavras que podem ser formadas
#define WORDS_GROUP_WORDS_LABELS_POS_X		16.0f
#define WORDS_GROUP_WORDS_LABELS_WIDTH		70.0f
#define WORDS_GROUP_WORDS_LABELS_HEIGHT		11.0f

// Espaço existente entre uma palavra e outra na lista de palavras
#define WORDS_GROUP_DY_BETWEEN_WORDS 0.0f

// Configurações do label que exibe o total de palavras formadas
#define WORDS_GROUP_TOTAL_WORDS_LABEL_POS_X		69.0f // OLD 70.0f
#define WORDS_GROUP_TOTAL_WORDS_LABEL_POS_Y		 4.0f

#define WORDS_GROUP_TOTAL_WORDS_LABEL_WIDTH		27.0f
#define WORDS_GROUP_TOTAL_WORDS_LABEL_HEIGHT	13.0f

/*======================================================================================

CONSTRUTOR

======================================================================================*/

WordsGroup::WordsGroup( void ) : ObjectGroup( WORDS_GROUP_N_OBJECTS )
{
	{  // Evita problemas com os gotos

		ObjectGroup* pBackground = createBackground();
		if( !pBackground || !insertObject( pBackground ) )
		{
			delete pBackground;
			goto Error;
		}
		
		// Interface
		char interfacePath[] = "wordsPT";
		for( uint8 i = 0 ; i < APP_N_LANGUAGES ; ++i )
		{
			RenderableImage* pInterface = new RenderableImage( interfacePath );
			if( !pInterface || !insertObject( pInterface ) )
			{
				delete pInterface;
				goto Error;
			}
			
			TextureFrame frame( 0, 0, WORDS_GROUP_FRONT_TEXTURE_FRAME_WIDTH, WORDS_GROUP_FRONT_TEXTURE_FRAME_HEIGHT, 0, 0 );
			pInterface->setImageFrame( frame );
			
			interfacePath[5] = 'E';
			interfacePath[6] = 'S';
		}
		
		#if FONTS_TRANSPARENT
			Label* pTotalWords = new Label( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL ], false );
		#elif FONTS_OPAQUE
			Label* pTotalWords = new Label( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL_MED_RED ], false );
		#endif
		if( !pTotalWords || !insertObject( pTotalWords ) )
		{
			delete pTotalWords;
			goto Error;
		}
		pTotalWords->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pTotalWords->setCharSpacement( 1.0f );
		pTotalWords->setSize( WORDS_GROUP_TOTAL_WORDS_LABEL_WIDTH, WORDS_GROUP_TOTAL_WORDS_LABEL_HEIGHT );
		pTotalWords->setPosition( WORDS_GROUP_TOTAL_WORDS_LABEL_POS_X, WORDS_GROUP_TOTAL_WORDS_LABEL_POS_Y );
		
		updateLabel( pTotalWords, 0, MAX_TOTAL_WORDS_COMPLETION );
		
		// Determina o tamanho do grupo
		setSize( WORDS_GROUP_FRONT_TEXTURE_FRAME_WIDTH, WORDS_GROUP_FRONT_TEXTURE_FRAME_HEIGHT );
		
		// Centraliza o background no grupo
		pBackground->setPosition( WORDS_GROUP_BKG_POS_X, WORDS_GROUP_BKG_MAX_Y );
		
		// Inicializa os arrays da classe
		memset( wordsDone, 0, sizeof( int8 ) * TOTAL_WORDS );
		
		return;

	} // Evita problemas com os gotos
	
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "WordsGroup::WordsGroup( PepsiCaeBieno* ): Unable To create object" );
	#else
		throw ConstructorException();
	#endif
}

/*======================================================================================

MÉTODO createBackground
	Cria a parte deslizante do objeto.

======================================================================================*/

ObjectGroup* WordsGroup::createBackground( void )
{
	ObjectGroup* pBackground = new ObjectGroup( ( TOTAL_WORDS << 1 ) + 1 );
	if( !pBackground )
		return NULL;
	
	{ // Evita problemas com os gotos

		RenderableImage* pBkgImg = new RenderableImage( "wordsBkg" );
		if( !pBkgImg || !pBackground->insertObject( pBkgImg ) )
		{
			delete pBkgImg;
			goto Error;
		}

		TextureFrame frame( 0, 0, WORDS_GROUP_BKG_TEXTURE_FRAME_WIDTH, WORDS_GROUP_BKG_TEXTURE_FRAME_HEIGHT, 0, 0 );
		pBkgImg->setImageFrame( frame );
		
		// Determina o tamanho do grupo
		pBackground->setSize( WORDS_GROUP_BKG_TEXTURE_FRAME_WIDTH, WORDS_GROUP_BKG_TEXTURE_FRAME_HEIGHT );

		// Aloca os labels para exibir quantas vezes uma determinada palavra já foi formada
		#if FONTS_TRANSPARENT
			Font* pNTimesFont = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL];
		#elif FONTS_OPAQUE
			Font* pNTimesFontLightRed = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL_LIGHT_RED];
			Font* pNTimesFontDarkRed = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL_DARK_RED];
		#endif

		for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
		{
			#if FONTS_TRANSPARENT
				Label* pLabel = new Label( pNTimesFont, false );
			#elif FONTS_OPAQUE
				Label* pLabel;
				if( ( i & 1 ) == 0 )
					pLabel = new Label( pNTimesFontDarkRed, false );
				else
					pLabel = new Label( pNTimesFontLightRed, false );
			#endif

			if( !pLabel || !pBackground->insertObject( pLabel ) )
			{
				delete pLabel;
				goto Error;
			}
			pLabel->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
			pLabel->setCharSpacement( 1.0f );
			pLabel->setSize( WORDS_GROUP_TIMES_LABELS_WIDTH, WORDS_GROUP_TIMES_LABELS_HEIGHT );
			pLabel->setPosition( WORDS_GROUP_TIMES_LABELS_POS_X, i * ( WORDS_GROUP_TIMES_LABELS_HEIGHT + WORDS_GROUP_DY_BETWEEN_WORDS ) );
			
			updateLabel( pLabel, 0, WORDS_COMPLETION_MAX_TIMES );
		}
		
		// Aloca os labels para conter as palavras
		#if FONTS_TRANSPARENT
			Font* pWordsFont = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_WORDS];
		#elif FONTS_OPAQUE
			Font* pWordsFontLightRed = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_WORDS_LIGHT_RED];
			Font* pWordsFontDarkRed = [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_WORDS_DARK_RED];
		#endif

		for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
		{
			#if FONTS_TRANSPARENT
				Label* pLabel = new Label( pWordsFont, false );
			#elif FONTS_OPAQUE
				Label* pLabel = NULL;
				if( ( i & 1 ) == 0 )
					pLabel = new Label( pWordsFontDarkRed, false );
				else
					pLabel = new Label( pWordsFontLightRed, false );
			#endif

			if( !pLabel || !pBackground->insertObject( pLabel ) )
			{
				delete pLabel;
				goto Error;
			}
			pLabel->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_CENTER );
			pLabel->setCharSpacement( 1.0f );
			pLabel->setSize( WORDS_GROUP_WORDS_LABELS_WIDTH, WORDS_GROUP_WORDS_LABELS_HEIGHT );
			pLabel->setPosition( WORDS_GROUP_WORDS_LABELS_POS_X, i * ( WORDS_GROUP_WORDS_LABELS_HEIGHT + WORDS_GROUP_DY_BETWEEN_WORDS ) );
		}

	} // Evita problemas com os gotos
	
	return pBackground;
		
	// Label de tratamento de erros
	Error:
		delete pBackground;
		return NULL;
}

/*======================================================================================

MÉTODO reset
	Reinicia o componente.

======================================================================================*/

void WordsGroup::reset( uint8 languageIndex )
{
	// Inicializa as variáveis da classe
	memset( wordsDone, 0, sizeof( int8 ) * TOTAL_WORDS );
	
	// Carrega as palavras que podem ser formadas, já no idioma correto
	loadWords();
	
	// Reposiciona o fundo
	Object* pBackground = getObject( WORDS_GROUP_OBJ_INDEX_BKG );
	pBackground->setPosition( pBackground->getPosition()->x, WORDS_GROUP_BKG_MAX_Y );
	
	// Utiliza a interface correta para o idioma
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;
	getObject( WORDS_GROUP_OBJ_INDEX_FRONT_PT )->setVisible( isPortuguese );
	getObject( WORDS_GROUP_OBJ_INDEX_FRONT_ES )->setVisible( !isPortuguese );
	
	// Atualiza os labels
	updateLabel( static_cast< Label* >( getObject( WORDS_GROUP_OBJ_INDEX_TOTAL_WORDS_MADE_LABEL ) ), 0, MAX_TOTAL_WORDS_COMPLETION );
	
	for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
		updateLabel( static_cast< Label* >( static_cast< ObjectGroup* >( getObject( WORDS_GROUP_OBJ_INDEX_BKG ) )->getObject( WORDS_GROUP_BKG_OBJ_INDEX_FIRST_TIMES_LABEL + i ) ), 0, WORDS_COMPLETION_MAX_TIMES );
}

/*======================================================================================

MÉTODO addWord
	Indica que uma palavra foi completada.

======================================================================================*/

void WordsGroup::addWord( int32 index )
{
	++wordsDone[ index ];
	if( wordsDone[ index ] > WORDS_COMPLETION_MAX_TIMES )
		wordsDone[ index ] = WORDS_COMPLETION_MAX_TIMES;

	updateLabel( static_cast< Label* >( static_cast< ObjectGroup* >( getObject( WORDS_GROUP_OBJ_INDEX_BKG ) )->getObject( WORDS_GROUP_BKG_OBJ_INDEX_FIRST_TIMES_LABEL + index ) ), wordsDone[ index ], WORDS_COMPLETION_MAX_TIMES );
	
	int16 total = 0;
	
	for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
		total += wordsDone[ i ];
	
	updateLabel( static_cast< Label* >( getObject( WORDS_GROUP_OBJ_INDEX_TOTAL_WORDS_MADE_LABEL ) ), total, MAX_TOTAL_WORDS_COMPLETION );
}

/*======================================================================================

MÉTODO updateLabel
	Atualiza o label que exibe uma dada palavra.

======================================================================================*/

#define WORDS_GROUP_UPDATE_LABEL_BUFFER_LEN 16

void WordsGroup::updateLabel( Label* pLabel, int16 nTimes, int16 maxNumber )
{
	char buffer[ WORDS_GROUP_UPDATE_LABEL_BUFFER_LEN ];
	snprintf( buffer, WORDS_GROUP_UPDATE_LABEL_BUFFER_LEN, "%d", maxNumber );
	uint8 completionMaxTimesLen = strlen( buffer );
	
	snprintf( buffer, WORDS_GROUP_UPDATE_LABEL_BUFFER_LEN, "%0*d", completionMaxTimesLen, nTimes );

	pLabel->setText( buffer, false );
}

#undef WORDS_GROUP_UPDATE_LABEL_BUFFER_LEN

/*======================================================================================

MÉTODO setPosition
	Determina a posição do objeto.

======================================================================================*/

void WordsGroup::setPosition( float x, float y, float z )
{
	ObjectGroup::setPosition( x, y, z );
	getObject( WORDS_GROUP_OBJ_INDEX_BKG )->setViewport( x + WORDS_GROUP_BKG_VIEWPORT_POS_X, y + WORDS_GROUP_BKG_VIEWPORT_POS_Y, WORDS_GROUP_BKG_VIEWPORT_WIDTH, WORDS_GROUP_BKG_VIEWPORT_HEIGHT );
}

/*======================================================================================

MÉTODO checkCollision
	Checa colisão com um toque.

======================================================================================*/

bool WordsGroup::checkCollision( const Point3f* pTouchPos ) const
{
	Point3f myPos = *( getPosition() );
	return Utils::IsPointInsideRect( pTouchPos, myPos.x + WORDS_GROUP_BKG_VIEWPORT_POS_X, myPos.y + WORDS_GROUP_BKG_VIEWPORT_POS_Y, WORDS_GROUP_BKG_VIEWPORT_WIDTH, WORDS_GROUP_BKG_VIEWPORT_HEIGHT );
}

/*======================================================================================

MÉTODO slide
	Faz a rolagem da barra de palavras.

======================================================================================*/

void WordsGroup::slide( float dy )
{
	Object* pBackground = getObject( WORDS_GROUP_OBJ_INDEX_BKG );
	pBackground->move( 0.0f, dy );
	
	Point3f bkgPos = *( pBackground->getPosition() );
	float minY = WORDS_GROUP_BKG_VIEWPORT_POS_Y - ( pBackground->getHeight() - WORDS_GROUP_BKG_VIEWPORT_HEIGHT );
	if( bkgPos.y < minY )
	{
		pBackground->setPosition( bkgPos.x, minY );
	}
	else if( bkgPos.y > WORDS_GROUP_BKG_MAX_Y )
	{
		pBackground->setPosition( bkgPos.x, WORDS_GROUP_BKG_MAX_Y );
	}
}

/*======================================================================================

MÉTODO loadWords
	Carrega as palavras que devem ser exibidas.

======================================================================================*/

void WordsGroup::loadWords( void )
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* ) APP_DELEGATE;

	for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
	{
		#if DEBUG
			LOG( @"WordsGroup::loadWords - Word %d = %s", i, [hApplication getText: TEXT_INDEX_FIRST_WORD + i] );
		#endif
		
		static_cast< Label* >( static_cast< ObjectGroup* >( getObject( WORDS_GROUP_OBJ_INDEX_BKG ) )->getObject( WORDS_GROUP_BKG_OBJ_INDEX_FIRST_WORDS_LABEL + i ) )->setText( [hApplication getText: TEXT_INDEX_FIRST_WORD + i], false );
	}
}
