/*
 *  WordsGroup.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef WORDS_GROUP_H
#define WORDS_GROUP_H

#include "Config.h"
#include "ObjectGroup.h"

// Forward Declarations
class Label;
class PepsiCaeBieno;

class WordsGroup : public ObjectGroup
{
	public:
		// Construtor
		WordsGroup( void );

		// Destrutor
		virtual ~WordsGroup( void ){};

		// Indica que uma palavra foi completada
		void addWord( int32 index );
	
		// Determina a posição do objeto
		virtual void setPosition( float x = 0.0f, float y = 0.0f, float z = 0.0f );
	
		// Checa colisão com um toque
		bool checkCollision( const Point3f* pTouchPos ) const;
	
		// Faz a rolagem da barra de palavras
		void slide( float dy );
	
		// Reinicia o componente
		void reset( uint8 languageIndex );
	
	private:
		// Cria a parte deslizante do objeto
		ObjectGroup* createBackground( void );
	
		// Atualiza o label que exibe uma dada palavra
		void updateLabel( Label* pLabel, int16 nTimes, int16 maxNumber );
	
		// Carrega as palavras que devem ser exibidas
		void loadWords( void );

		// Índices das palavras que já foram completadas
		int8 wordsDone[ TOTAL_WORDS ];
};

#endif
