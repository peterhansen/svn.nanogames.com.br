/*
 *  Piece.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/22/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PIECE_H
#define PIECE_H

#include "RenderableImage.h"

#include "Config.h"

typedef enum PieceState
{
	PIECE_STATE_NONE		= -1,
	PIECE_STATE_IDLE		=  0,
	PIECE_STATE_MOVING		=  1,
	PIECE_STATE_VANISHING	=  2,
	PIECE_STATE_VANISHING_2	=  3,
	PIECE_STATE_FALLING		=  4
}PieceState;

typedef enum PieceColor
{
	PIECE_COLOR_NONE	= -1,
	PIECE_COLOR_BLUE	=  0,
	PIECE_COLOR_PINK	=  1,
	PIECE_COLOR_GREEN	=  2,
	PIECE_COLOR_ORANGE	=  3
}PieceColor;

// Número de cores das peças
#define PIECE_N_COLORS 4

// Número de letras utilizadas pelo jogo
#define N_LETTERS 26

// Forward declaration para PieceGroup
class PieceGroup;

class Piece : public RenderableImage
{
	public:
		// Construtor
		Piece( void );
	
		// Destrutor
		virtual ~Piece( void ){};
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Inicia a animação de fazer a peça desaparecer
		void vanish( bool green, int32 steps );
	
		// Faz a peça se cair por um espaço correspondente a PIECE_HEIGHT * pieces
		void fall( int32 pieces, int32 steps );	

		// Getters e Setters dos atributos classe
		PieceState getState( void ) const;
		void setState( PieceState state );
	
		float getYLimit( void ) const;
		void setYLimit( float yLimit );

		char getChar( void ) const;
		void setChar( char c );
	
		int8 getCharIndex( void ) const;
		void setCharIndex( int8 charIndex );

		PieceColor getColor( void ) const;
		void setColor( PieceColor pieceColor );
	
		// Aloca as imagens estáticas da classe
		static bool LoadImages( void );
	
		// Desaloca as imagens estáticas da classe
		static bool ReleaseImages( void );

	private:
		// Obtém a velocidade de queda da peça
		float getFallSpeed( void ) const;

		// Determina a velocidade de queda da peça
		void setFallSpeed( float speed );
	
		// Atualiza a imagem da peça de acordo com o seu estado
		void updateImage( void );
	
		// Retorna a posição da peça na tela independentemente se está contida dentro de 
		// algum grupo
		Point3f* getScreenPos( Point3f* pOut ) const;

		/** Índice do caracter (varia de zero a TOTAL_CHARS - 1 ) */
		int8 charIndex;

		/** Cor atual da peça */
		PieceColor color;
		
		/** Estado da peça */
		PieceState state;

		/** Limite de queda da peça. */
		float yLimit;
		
		/** Auxiliar de animação */
		float accTime;

		/** Velocidade de queda da peça */
		float fallSpeed;
	
		// Imagens das peças
		static Texture2DHandler images[ PIECE_N_COLORS ][ N_LETTERS ];
};

#endif
