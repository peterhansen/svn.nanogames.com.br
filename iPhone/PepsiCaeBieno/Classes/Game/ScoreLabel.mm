#include "ScoreLabel.h"

#include "Config.h"
#include "PepsiCaeBieno.h"
#include "PepsiCaeBienoAppDelegate.h"

#include "Random.h"

// Tempo em milisegundos da animação de aparição / sumiço
#define SCORE_LABEL_APPEARING_TIME 0.21f

#define SPEED_MIN -( SCREEN_HEIGHT / 16.0f )
#define SPEED_MAX_DIFF ( SCREEN_HEIGHT / 8.0f )

#define LIFE_MIN		1.0f
#define LIFE_MAX_DIFF	1.7f

/*======================================================================================

CONSTRUTOR

======================================================================================*/

ScoreLabel::ScoreLabel( PepsiCaeBieno* pPlayScreen )
#if FONTS_TRANSPARENT
		   : Label( [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL ], false ),
#elif FONTS_OPAQUE
	       : Label( [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL_TRANSP ], false ),
#endif
			 state( SCORE_LABEL_STATE_UNDEFINDED ),
			 lifeTime( 0.0f ),
			 scoreSpeed( 0.0f ),
			 pPlayScreen( pPlayScreen )
{
	setViewport( 0, 0, 0, 0 );
	setState( SCORE_LABEL_STATE_IDLE );
}

/*======================================================================================

MÉTODO update
	Atualiza o objeto.

======================================================================================*/

bool ScoreLabel::update( float timeElapsed )
{
	if( !Label::update( timeElapsed ) )
		return false;

	switch( state )
	{
		case SCORE_LABEL_STATE_APPEARING:
			lifeTime += timeElapsed;
			
			if( lifeTime < SCORE_LABEL_APPEARING_TIME )
			{
				float currentWidth = getWidth() * lifeTime / SCORE_LABEL_APPEARING_TIME;
				float currentHeight = getHeight() * lifeTime / SCORE_LABEL_APPEARING_TIME;

				Point3f posByAnchor;
				getPositionByAnchor( &posByAnchor );
				setViewport( posByAnchor.x - ( currentWidth * 0.5f ), posByAnchor.y - ( currentHeight * 0.5f ), currentWidth, currentHeight );
			}
			else
			{
				setState( SCORE_LABEL_STATE_VISIBLE );
			}
			break;

		case SCORE_LABEL_STATE_VISIBLE:
			lifeTime -= timeElapsed;

			if( lifeTime > 0.0f )
				move( 0.0f, scoreSpeed * timeElapsed );
			else
				setState( SCORE_LABEL_STATE_VANISHING );
			break;

		case SCORE_LABEL_STATE_VANISHING:
			lifeTime -= timeElapsed;

			if( lifeTime > 0.0f )
			{
				float currentWidth = getWidth() * lifeTime / SCORE_LABEL_APPEARING_TIME;
				float currentHeight = getHeight() * lifeTime / SCORE_LABEL_APPEARING_TIME;
				
				Point3f posByAnchor;
				getPositionByAnchor( &posByAnchor );
				setViewport( posByAnchor.x - ( currentWidth* 0.5f ), posByAnchor.y - ( currentHeight * 0.5f ), currentWidth, currentHeight );
			}
			else
			{
				setState( SCORE_LABEL_STATE_IDLE );
			}
			break;
	}
	return true;
}


/*======================================================================================

MÉTODO setScore
	Determina a pontuação que deverá ser exibida.

======================================================================================*/

#define SCORE_LABEL_BUFFER_LEN 32

void ScoreLabel::setScore( const Point3f& pos, int32 score )
{
	char buffer[ SCORE_LABEL_BUFFER_LEN ];
	snprintf( buffer, SCORE_LABEL_BUFFER_LEN, "%d", score );
	setText( buffer );
	
	setAnchor( ANCHOR_HCENTER | ANCHOR_VCENTER );

	pPlayScreen->changeScore( score );
	
	setPositionByAnchor( &pos );
	setState( SCORE_LABEL_STATE_APPEARING );
}

#undef SCORE_LABEL_BUFFER_LEN

/*======================================================================================

MÉTODO setState
	Determina o estado do objeto.

======================================================================================*/

void ScoreLabel::setState( ScoreLabelState s )
{
	state = s;

	switch( state )
	{
		case SCORE_LABEL_STATE_IDLE:
			setVisible( false );
			break;

		case SCORE_LABEL_STATE_APPEARING:
			setVisible( true );
			scoreSpeed = SPEED_MIN - Random::GetFloat( SPEED_MAX_DIFF );

			lifeTime = 0.0f;
			setViewport( 0, 0, 0, 0 );
			break;

		case SCORE_LABEL_STATE_VISIBLE:
			lifeTime = LIFE_MIN + Random::GetFloat( LIFE_MAX_DIFF );
			setViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
			break;

		case SCORE_LABEL_STATE_VANISHING:
			lifeTime = SCORE_LABEL_APPEARING_TIME;
			break;
	}
}

