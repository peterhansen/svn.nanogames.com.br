/*
 *  OptionsTag.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 7/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OPTIONS_TAG_H
#define OPTIONS_TAG_H 1

#include "Collidable.h"

#include "RenderableImage.h"

class OptionsTag : public RenderableImage, public Collidable
{
	public:
		// Construtor
		OptionsTag( const char* pFileName );
	
		// Destrutor
		virtual ~OptionsTag( void ){};

		// Checa colisão do objeto com o ponto
		virtual bool checkCollision( const Point3f* pPoint );
};

#endif
