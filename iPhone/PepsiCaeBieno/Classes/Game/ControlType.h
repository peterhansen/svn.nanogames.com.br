/*
 *  ControlType.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 7/14/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CONTROL_TYPE_H
#define CONTROL_TYPE_H 1

enum ControlType
{
	CONTROL_TYPE_ACCELEROMETERS_ONLY = 0,
	CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS
};

#endif
