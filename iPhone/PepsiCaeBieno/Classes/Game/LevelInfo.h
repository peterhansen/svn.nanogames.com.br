/*
 *  LevelInfo.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 5/1/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef LEVEL_INFO_H
#define LEVEL_INFO_H 1

#include "ObjectGroup.h"

class LevelInfo : public ObjectGroup
{
	public:
		// Construtor
		// Exceções: ConstructorException
		LevelInfo( void );
	
		// Destrutor
		virtual ~LevelInfo( void ){};
	
		// Atualiza o label que exibe o tempo restante da fase
		void updateTimeRemainingLabel( const char* pText );
	
		// Atualiza o label que exibe os pontos do jogador
		void updatePointsLabel( const char* pText );
	
		// Atualiza o label que exibe a fase atual
		void updateLevelLabel( const char* pText );
	
		// Reinicia o componente
		void reset( uint8 languageIndex );

	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );
};

#endif
