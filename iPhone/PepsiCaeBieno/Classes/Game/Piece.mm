#include "Piece.h"

#include "Config.h"
#include "PieceGroup.h"
#include "ResourceManager.h"

#include "Macros.h"

// Velocidade de queda da peça
#define SPEED_FALL ( PIECE_HEIGHT * 1000.0f / 600.0f )

// Tamanho do frame das peças utilizadas no tabuleiro
#define PIECE_BOARD_FRAME_WIDTH		32.0f
#define PIECE_BOARD_FRAME_HEIGHT	32.0f

// Tamanho do frame das peças que indicam a próxima letra a cair
#define PIECE_ORANGE_FRAME_WIDTH	46.0f
#define PIECE_ORANGE_FRAME_HEIGHT	46.0f

// Inicializa as variáveis estáticas da classe
Texture2DHandler Piece::images[ PIECE_N_COLORS ][ N_LETTERS ];

/*======================================================================================

CONSTRUTOR

======================================================================================*/

Piece::Piece( void )
	  : RenderableImage(),
		charIndex( -1 ),
		color( PIECE_COLOR_NONE ),
		state( PIECE_STATE_NONE ),
		yLimit( 0.0f ),
		accTime( 0.0f ),
		fallSpeed( 0.0f )
{
	setAnchor( ANCHOR_HCENTER | ANCHOR_BOTTOM );

	setColor( PIECE_COLOR_BLUE );
	setState( PIECE_STATE_NONE );
}	

/*======================================================================================

MÉTODO setChar

======================================================================================*/

void Piece::setChar( char c )
{
	setCharIndex( ( int8 ) ( c - 'A' ) );
}
	
/*======================================================================================

MÉTODO setCharIndex

======================================================================================*/

void Piece::setCharIndex( int8 charIndex )
{
	this->charIndex = charIndex;
	updateImage();
}

/*======================================================================================

MÉTODO getColor

======================================================================================*/

PieceColor Piece::getColor( void ) const
{
	return color;
}

/*======================================================================================

MÉTODO setColor

======================================================================================*/

void Piece::setColor( PieceColor pieceColor )
{
	color = pieceColor;
	updateImage();
}

/*======================================================================================

MÉTODO getCharIndex

======================================================================================*/

int8 Piece::getCharIndex( void ) const
{
	return charIndex;
}

/*======================================================================================

MÉTODO getChar

======================================================================================*/

char Piece::getChar( void ) const
{
	return ( char ) ( 'A' + charIndex );
}

/*======================================================================================

MÉTODO updateImage

======================================================================================*/

void Piece::updateImage( void )
{
	#if DEBUG
		int8 bla = static_cast< int8 >( color );
		int8 ble = charIndex;
	#endif

	if( ( color != PIECE_COLOR_NONE ) && ( charIndex >= 0 ) )
		setImage( Piece::images[ static_cast< int8 >( color ) ][ charIndex ] );

	setSize( 1.0f, 1.0f, 1.0f );
	setScale( PIECE_WIDTH, PIECE_HEIGHT, 1.0f );
}

/*======================================================================================

MÉTODO getScreenPos
	Retorna a posição da peça na tela independentemente se está contida dentro de 
algum grupo.

======================================================================================*/

Point3f* Piece::getScreenPos( Point3f* pOut ) const
{
	*pOut = *getPosition();
	
	Object* pParent = getParent();
	while( pParent != NULL )
	{
		*pOut += *( pParent->getPosition() );
		
		pParent = pParent->getParent();
	}

	return pOut;
}

/*======================================================================================

MÉTODO getState

======================================================================================*/

PieceState Piece::getState( void ) const
{
	return state;
}

/*======================================================================================

MÉTODO setState

======================================================================================*/

void Piece::setState( PieceState state )
{	
	this->state = state;
	setViewport( GET_DEFAULT_VIEWPORT() );

	setVisible( state != PIECE_STATE_NONE );

	switch( state )
	{
		case PIECE_STATE_IDLE:
			setColor( PIECE_COLOR_BLUE );
			break;

		case PIECE_STATE_MOVING:
			setColor( PIECE_COLOR_PINK );
			break;

		case PIECE_STATE_VANISHING:
			setColor( PIECE_COLOR_GREEN );

		case PIECE_STATE_VANISHING_2:
			{
				Point3f p;
				getScreenPos( &p );

				Viewport v( p.x, p.y, getWidth(), getHeight() );
				setViewport( v );
			}
			break;

		case PIECE_STATE_FALLING:
			setFallSpeed( SPEED_FALL );
			break;
	}
}

/*======================================================================================

MÉTODO update

======================================================================================*/

bool Piece::update( float timeElapsed )
{
	if( !RenderableImage::update( timeElapsed ) )
		return false;

	switch( state )
	{
		case PIECE_STATE_MOVING:
			break;

		case PIECE_STATE_VANISHING:
		case PIECE_STATE_VANISHING_2:
			accTime -= timeElapsed;

			if( accTime <= VANISH_TIME )
			{
				if( accTime > 0.0f )
				{
					Point3f p;
					getScreenPos( &p );
					
					viewport.width = getWidth() * accTime / VANISH_TIME;
					viewport.x = p.x + ( ( getWidth() - viewport.width ) * 0.5f );
					
					viewport.height = getHeight() * accTime / VANISH_TIME;
					viewport.y = p.y + getHeight() - ( getHeight() * accTime / VANISH_TIME );
				}
				else
				{
					setState( PIECE_STATE_NONE );
				}
			}
			break;

		case PIECE_STATE_FALLING:
			if( accTime > 0.0f )
			{
				accTime -= timeElapsed;
			}
			else
			{
				move( 0, getFallSpeed() * timeElapsed );
				
				const Point3f* pPos = getPosition();
				if( pPos->y >= yLimit )
				{
					setPosition( pPos->x, yLimit );
					setState( PIECE_STATE_IDLE );
				}
			}
			break;
	}
	return true;
}

/*======================================================================================

MÉTODO setYLimit

======================================================================================*/

void Piece::setYLimit( float yLimit )
{
	this->yLimit = yLimit;
}

/*======================================================================================

MÉTODO getYLimit

======================================================================================*/

float Piece::getYLimit( void ) const
{
	return yLimit;
}

/*======================================================================================

MÉTODO fall

======================================================================================*/

void Piece::fall( int32 pieces, int32 steps )
{
	accTime = ( VANISH_TIME * steps );
	
	setYLimit( getPosition()->y + ( pieces * PIECE_HEIGHT ) );
	setState( PIECE_STATE_FALLING );
}

/*======================================================================================

MÉTODO vanish

======================================================================================*/

void Piece::vanish( bool green, int32 steps )
{
	accTime = VANISH_TIME * ( steps + 1 );
	setState( green ? PIECE_STATE_VANISHING : PIECE_STATE_VANISHING_2 );
}

/*======================================================================================

MÉTODO getFallSpeed
	Obtém a velocidade de queda da peça.

======================================================================================*/

float Piece::getFallSpeed( void ) const
{
	return fallSpeed;
}

/*======================================================================================

MÉTODO setFallSpeed
	Determina a velocidade de queda da peça.

======================================================================================*/

void Piece::setFallSpeed( float speed )
{
	fallSpeed = speed;
}

/*======================================================================================

MÉTODO LoadImages
	Aloca as imagens estáticas da classe.

======================================================================================*/

bool Piece::LoadImages( void )
{
	char letterPath[] = { ' ', ' ', '\0' };
	for( uint8 i = 0 ; i < N_LETTERS ; ++i )
	{
		letterPath[0] = 'A' + i;

		letterPath[1] = 'b';
		Texture2DHandler hTextureB = ResourceManager::GetTexture2D( letterPath );
		if( !hTextureB )
			goto Error;
		
		images[0][i] = hTextureB;

		letterPath[1] = 'p';
		Texture2DHandler hTextureP = ResourceManager::GetTexture2D( letterPath );
		if( !hTextureP )
			goto Error;
		
		images[1][i] = hTextureP;
		
		letterPath[1] = 'g';
		Texture2DHandler hTextureG = ResourceManager::GetTexture2D( letterPath );
		if( !hTextureG )
			goto Error;
		
		images[2][i] = hTextureG;
		
		letterPath[1] = 'o';
		Texture2DHandler hTextureO = ResourceManager::GetTexture2D( letterPath );
		if( !hTextureO )
			goto Error;
		
		images[3][i] = hTextureO;
		
		TextureFrame frame( 0, 0, PIECE_ORANGE_FRAME_WIDTH, PIECE_ORANGE_FRAME_HEIGHT, 0, 0 );
		hTextureO->changeFrame( 0, &frame );
	}

	return true;
	
	// Label de tratamento de erros
	Error:
		ReleaseImages();
		return false;
}

/*======================================================================================

MÉTODO ReleaseImages
	Desaloca as imagens estáticas da classe.

======================================================================================*/
 
bool Piece::ReleaseImages( void )
{
	RefCounter< Texture2D > deleter( NULL );
	for( uint8 i = 0 ; i < N_LETTERS ; ++i )
	{
		images[0][i] = deleter;
		images[1][i] = deleter;
		images[2][i] = deleter;
		images[3][i] = deleter;
	}
	return true;
}

