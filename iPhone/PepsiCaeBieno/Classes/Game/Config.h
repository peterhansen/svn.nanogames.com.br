/*
 *  Config.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H 1

/*=====================================
 
INFORMAÇÔES DO JOGO
 
=====================================*/

// Maior nível que pode ser alcançado
#define MAX_LEVEL 5

// Nível a partir do qual a maior dificuldade do jogo é alcançada
#define MAX_DIFFICULTY_LEVEL 5

// Pontuação máxima que um jogador consegue atingir
#define MAX_POINTS 99999

// Quantidade total de palavras válidas
#define TOTAL_WORDS 20 // OLD 30

// Tempo total de cada nível: 3 minutos
#define LEVEL_TIME ( 3.0f * 60.0f )

// Alteração na porcentagem de velocidade de queda a cada tecla pressionada (cima ou baixo)
#define FALL_SPEED_CHANGE_STEP 17

// Duração da animação de desaparecimento da peça
#define VANISH_TIME 0.25f

// Número de colunas no tabuleiro
#define TOTAL_COLUMNS 10

// Número de linhas no tabuleiro
#define TOTAL_ROWS 10

// Número total de peças no tabuleiro
#define TOTAL_PIECES ( TOTAL_COLUMNS * TOTAL_ROWS )

// Pontuação necessária para passar de nível
#define SCORE_GOAL_PER_LEVEL 2000

// Tempo que o botão de pausa fica visível antes de esmaecer
#define SOFT_KEY_VISIBLE_TIME 3.8f

// Largura das peças do tabuleiro
#define PIECE_WIDTH 24.0f

// Altura das peças ddo tabuleiro
#define PIECE_HEIGHT 24.0f

/** Largura em pixels do grupo das palavras na tela de jogo. */
#define WORDS_GROUP_WIDTH 150

/** Dist‚ncia em pixels do grupo das palavras ao tabuleiro. */
#define WORDS_GROUP_BOARD_OFFSET 30

/** Número máximo de vezes que uma palavra pode ser completada */
#define WORDS_COMPLETION_MAX_TIMES 99

// Contagem máxima do número de palavras formadas durante uma partida
#define MAX_TOTAL_WORDS_COMPLETION 999

// Número de caracteres da menor palavra
#define WORD_MIN_LENGTH 2

// Número de caracteres da maior palavra
#define WORD_MAX_LENGTH 9

// Número de palavras a serem embaralhadas antes de colocarmos suas letras caindo na tela
#define WORDS_TO_MIX 1

/** Espessura da borda do tabuleiro. */
#define BORDER_THICKNESS 4

/** Quantidade total de blocos usados na tela de splash. */
#define TOTAL_BLOCKS 8

/** Offset na posição horizontal do block "bien" em relação ao bloco "cae" na tela de splash. */
#define BLOCK_BIEN_OFFSET_X 61

/** Offset na posição horizontal do block "Pepsi" em relação ao bloco "cae" na tela de splash. */
#define BLOCK_PEPSI_OFFSET_X 138

/** Offset na posição vertical do block "Pepsi" em relação ao bloco "cae" na tela de splash. */
#define BLOCK_PEPSI_OFFSET_Y 19

/** Espessura da borda do tabuleiro. */
#define BORDER_HALF_THICKNESS ( BORDER_THICKNESS >> 1 )

// Índices das views da aplicação
#define VIEW_INDEX_SELECT_LANG	 0
#define VIEW_INDEX_LOADING		 1
#define VIEW_INDEX_SPLASH_NANO	 2
#define VIEW_INDEX_SPLASH_GAME	 3
#define VIEW_INDEX_MAIN_MENU	 4
#define VIEW_INDEX_GAME			 5
#define VIEW_INDEX_RECORDS		 6
#define VIEW_INDEX_OPTIONS		 7
#define VIEW_INDEX_HELP			 8
#define VIEW_INDEX_CREDITS		 9
#define VIEW_INDEX_PAUSE_SCREEN	10
#define VIEW_INDEX_CONFIRM_SCREEN	11

// Duração padrão das animações de transição de tela
#define DEFAULT_TRANSITION_DURATION 0.75f

// Duração do splash da nano (em segundos)
#define SPLASH_NANO_DURATION 2.0f

/*=====================================
 
CÓDIGOS DE ERRO
 
======================================*/

// Erros que podem terminar a aplicação
#define ERROR_ALLOCATING_DATA	ERROR_USER
#define ERROR_ALLOCATING_VIEWS	ERROR_USER + 1
#define ERROR_CHANGING_COLORS	ERROR_USER + 2
#define ERROR_NO_FRAME_BUFFER	ERROR_USER + 3
#define ERROR_UNSUPPORTED_OS	ERROR_USER + 4

/*=====================================
 
ACELERÔMETRO
 
======================================*/

// Define qual o modo de interpretação dos valores gerados pelos acelerômetros
#define DEFAULT_ACCELEROMETER_CALC_MODE ACC_FORCE_MODE_AVERAGE

// Intervalo com que os acelerômetros reportam seus valores à aplicação
#define ACCELEROMETER_N_UPDATES_PER_SEC 60.0f // OLD 30.0f 
#define ACCELEROMETER_UPDATE_INTERVAL ( 1.0f / ACCELEROMETER_N_UPDATES_PER_SEC )

// Quantos valores acumulamos para calcular a posição de repouso do device
#define ACCEL_REST_POS_CALC_N_VALUES 32

// Liberdade de movimento (em graus) dada ao usuário
#define ACCEL_MOVEMENT_FREEDOM_X 20.0f// OLD 40.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_X ( ACCEL_MOVEMENT_FREEDOM_X * 0.5f )

#define ACCEL_MOVEMENT_FREEDOM_Y 70.0f // OLD 60.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_Y ( ACCEL_MOVEMENT_FREEDOM_Y * 0.5f )

// Intervalos nos quais ignoramos a inclinação do device:
// [ -ACCEL_IGNORE_MOVE_INTERVAL_Y, ACCEL_IGNORE_MOVE_INTERVAL_Y ]
#define ACCEL_NO_MOVE_Y_INTERVAL 10.0f// OLD 4.0f

// Define o intervalo aceito para o peso dos valores antigos utilzados no cçlculo da média da
// aceleração reportada pelos acelerômetros
#define MIN_OLD_VALUES_WEIGHT 0.0f
#define MAX_OLD_VALUES_WEIGHT 0.9f

/*=====================================
 
CONFIGURAÇÕES DOS COMPONENTES
 
======================================*/

// Define o número máximo de toques tratados pela aplicação
#define MAX_TOUCHES 1

// Duração padrão da vibração, em segundos
#define VIBRATION_TIME_DEFAULT 0.2f

// Define se utilizaremos um depth buffer na aplicação
#define USE_DEPTH_BUFFER 1

// Define se iremos utilizar um buffer para fazer renderizações fora da tela
#define USE_OFFSCREEN_BUFFER 0

// Intervalo padrão do timer responsável pelas atualizações da aplicação
#define DEFAULT_ANIMATION_INTERVAL ( 1.0f / 32.0f ) // 32 fps

// Tempo máximo (em segundos) que consideramos como transcorrido entre uma atualização e outra 
#define MAX_UPDATE_INTERVAL 0.5f

// Configuração do zoom
#define ZOOM_IN_FACTOR 1.1f
#define ZOOM_OUT_FACTOR ( 1.0f / ZOOM_IN_FACTOR )

#define ZOOM_MIN 0.09f
#define ZOOM_MAX 6.00f

#define ZOOM_MIN_DIST_TO_CHANGE 10.0f

/*=====================================
 
RESOURCES
 
=====================================*/

// Número máximo de recordes salvos pela aplicação
#define APP_N_RECORDS 5

// Número de fontes suportado pela aplicação
#if FONTS_TRANSPARENT
	#define APP_N_FONTS 3
#elif FONTS_OPAQUE
	#define APP_N_FONTS 8
#endif

// Índices das fontes no vetor de fontes da aplicação
#if FONTS_TRANSPARENT
	#define APP_FONT_NUMBERS_SMALL		0
	#define APP_FONT_NUMBERS_BIG		1
	#define APP_FONT_WORDS				2
#elif FONTS_OPAQUE
	#define APP_FONT_WORDS_LIGHT_RED			0
	#define APP_FONT_WORDS_DARK_RED				1
	#define APP_FONT_NUMBERS_SMALL_MED_RED		2
	#define APP_FONT_NUMBERS_SMALL_DARK_RED		3
	#define APP_FONT_NUMBERS_SMALL_LIGHT_RED	4
	#define APP_FONT_NUMBERS_SMALL_TRANSP		5
	#define APP_FONT_NUMBERS_BIG_BLUE			6
	#define APP_FONT_NUMBERS_BIG_RED			7
#endif

// Número de idiomas suportados pela aplicação
#define APP_N_LANGUAGES 2

// Índices dos idiomas
#define LANGUAGE_INDEX_PORTUGUESE	0
#define LANGUAGE_INDEX_SPANISH		1

// Número de textos da aplicação
#define APP_N_TEXTS ( 8 + TOTAL_WORDS )

// Índices dos textos do jogo
#define TEXT_INDEX_POPUP_TITLE				0
#define TEXT_INDEX_POPUP_MSG_GOTOURL		1
#define TEXT_INDEX_POPUP_MSG_NO_OS_SUPPORT	2
#define TEXT_INDEX_POPUP_YES				3
#define TEXT_INDEX_POPUP_NO					4
#define TEXT_INDEX_POPUP_EXIT				5
#define TEXT_INDEX_HELP						6
#define TEXT_INDEX_LEAVE_GAME				7
#define TEXT_INDEX_FIRST_WORD				8
#define TEXT_INDEX_LAST_WORD				( TEXT_FIRST_WORD + TOTAL_WORDS - 1 )

// Número de sons da aplicação
#define APP_N_SOUNDS 7

// Índices dos sons do jogo
#define SOUND_INDEX_SPLASH			0
#define SOUND_INDEX_ENDING			1
#define SOUND_INDEX_AMBIENT_1		2
#define SOUND_INDEX_AMBIENT_2		3
#define SOUND_INDEX_AMBIENT_3		4
#define SOUND_INDEX_LEVEL_COMPLETE	5
#define SOUND_INDEX_GAME_OVER		6

#endif
