#include "PieceGroup.h"

#include "Config.h"
#include "Piece.h"

#include "Exceptions.h"

#define PIECE_GROUP_MOVING_PIECE_INDEX		0

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

PieceGroup::PieceGroup( Piece* pMovingPiece ) : ObjectGroup( TOTAL_PIECES + 1 )
{
	if( !insertObject( pMovingPiece ) )
		goto Error;

	for( int8 i = 0; i < TOTAL_PIECES; ++i )
	{
		Piece* pPiece = new Piece();
		if( !pPiece || !insertObject( pPiece ) )
		{
			delete pPiece;
			goto Error;
		}
	}

	setSize( TOTAL_COLUMNS * PIECE_WIDTH, TOTAL_ROWS * PIECE_HEIGHT );
	setAnchor( ANCHOR_HCENTER | ANCHOR_VCENTER );
	
	return;
	
	// Label de tratamento de erros
	Error:
		// Já que houve um erro no construtor, a responsabilidade de deletar pMovingPiece é do chamador
		setObject( PIECE_GROUP_MOVING_PIECE_INDEX, NULL );
	
	#if DEBUG
		throw ConstructorException( "PieceGroup::PieceGroup( Piece* ): Unable To create object" );
	#else
		throw ConstructorException();
	#endif
}

/*==============================================================================================

MÉTODO reset

==============================================================================================*/

void PieceGroup::reset( void )
{
	for( int8 i = 1; i < getNObjects() ; ++i )
		static_cast< Piece* >( getObject( i ) )->setState( PIECE_STATE_NONE );
}

/*==============================================================================================

MÉTODO insertPiece

==============================================================================================*/

Piece* PieceGroup::insertPiece( const Piece* pPiece )
{	
	// Procura um espaço
	for( int8 i = 0 ; i < TOTAL_PIECES ; ++i )
	{
		Piece* pEmptyPiece = static_cast< Piece* >( getObject( i ) );

		if( pEmptyPiece->getState() == PIECE_STATE_NONE )
		{
			pEmptyPiece->setCharIndex( pPiece->getCharIndex() );
			pEmptyPiece->setState( PIECE_STATE_IDLE );
			pEmptyPiece->setPosition( pPiece->getPosition() );
			return pEmptyPiece;
		}
	}
	
	// Não é possível inserir mais peças no tabuleiro
	return NULL;
}
