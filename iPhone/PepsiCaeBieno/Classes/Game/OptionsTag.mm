#include "OptionsTag.h"
	
#define OPTIONS_TAG_COLLISION_QUAD_HEIGHT 27.0f

/*======================================================================================

CONSTRUTOR

======================================================================================*/

OptionsTag::OptionsTag( const char* pFileName ) : RenderableImage( pFileName ), Collidable()
{
}

/*======================================================================================

MÉTODO checkCollision
	Checa colisão do objeto com o ponto.

======================================================================================*/

bool OptionsTag::checkCollision( const Point3f* pPoint )
{
	return Utils::IsPointInsideRect( pPoint, 0.0f, getPosition()->y, getWidth(), OPTIONS_TAG_COLLISION_QUAD_HEIGHT );
}
