#include "OptionsView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"

#define BT_LANG_ALPHA_SELECTED		1.0f
#define BT_LANG_ALPHA_UNSELECTED	0.5f

// Implementação da classe
@implementation OptionsView

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// OLD
	// Modifica a aparência do slider de som
//	UIImage *hThumbImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"optThumb", @"png" ) ];
//	[hSliderSound setThumbImage: hThumbImg forState: UIControlStateNormal];
//	[hSliderSound setThumbImage: hThumbImg forState: UIControlStateHighlighted];
//	[hSliderSound setThumbImage: hThumbImg forState: UIControlStateDisabled];
//	[hSliderSound setThumbImage: hThumbImg forState: UIControlStateSelected];
//	
//	UIImage *hMaxImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"optMinTrk", @"png" ) ];
//	[hSliderSound setMaximumTrackImage: hMaxImg forState: UIControlStateNormal];
//	[hSliderSound setMaximumTrackImage: hMaxImg forState: UIControlStateHighlighted];
//	[hSliderSound setMaximumTrackImage: hMaxImg forState: UIControlStateDisabled];
//	[hSliderSound setMaximumTrackImage: hMaxImg forState: UIControlStateSelected];
//	
//	UIImage *hMinImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"optMaxTrk", @"png" ) ];
//	[hSliderSound setMinimumTrackImage: hMinImg forState: UIControlStateNormal];
//	[hSliderSound setMinimumTrackImage: hMinImg forState: UIControlStateHighlighted];
//	[hSliderSound setMinimumTrackImage: hMinImg forState: UIControlStateDisabled];
//	[hSliderSound setMinimumTrackImage: hMinImg forState: UIControlStateSelected];
}

/*==============================================================================================

MENSAGEM setLanguage: AndControlType:
	Determina o idioma da interface.

==============================================================================================*/

- ( void ) setLanguage: ( uint8 )languageIndex
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;
	
	hBkgPT.hidden = !isPortuguese;
	hBackImgPT.hidden = !isPortuguese;
	hImgTxtAccelPT.hidden = !isPortuguese;
	hImgFlagControlPT.hidden = !isPortuguese;

	hBkgES.hidden = isPortuguese;
	hBackImgES.hidden = isPortuguese;
	hImgTxtAccelES.hidden = isPortuguese;
	hImgFlagControlES.hidden = isPortuguese;
	
	if( isPortuguese )
	{
		// OLD
//		hBtPortuguese.selected = YES
		
		[hBtPortuguese setAlpha: BT_LANG_ALPHA_SELECTED];
		[hBtSpanish setAlpha: BT_LANG_ALPHA_UNSELECTED];
	}
	else
	{
		// OLD
//		hBtSpanish.selected = YES

		[hBtPortuguese setAlpha: BT_LANG_ALPHA_UNSELECTED];
		[hBtSpanish setAlpha: BT_LANG_ALPHA_SELECTED];
	}
	
	if( *pControlType == CONTROL_TYPE_ACCELEROMETERS_ONLY )
		hBtCkbAccel.selected = YES;
	else
		hBtCkbTouchScreen.selected = YES;
	
	// Atualiza a barra de volume, já que ela pode ter sido modificada em outra tela
	[hVolumeSlider setValue: [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getAudioVolume] ];
}

/*==============================================================================================

MENSAGEM setControlType
	Passa um ponteiro que controla o tipo de input no jogo.

==============================================================================================*/

- ( void ) setControlType:( ControlType* ) pType
{
	pControlType = pType;
}

/*==============================================================================================

MENSAGEM onVolumeChanged
	Indica que o usuário alterou o volume dos sons da aplicação.

==============================================================================================*/

- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) setVolume: [hVolumeSlider value]];
}

/*==============================================================================================

MENSAGEM onChangeLanguage
	Chamado quando o usuário modifica o idioma da aplicação.

==============================================================================================*/

- ( IBAction )onChangeLanguage: ( UIButton* )hButton
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;
	
	if( hButton == hBtPortuguese )
	{
		[hApplication setLanguage: LANGUAGE_INDEX_PORTUGUESE];
		
		// OLD
//		hBtPortuguese.selected = YES;
//		hBtSpanish.selected = NO;
	}
	else
	{
		[hApplication setLanguage: LANGUAGE_INDEX_SPANISH];
		
		// OLD
//		hBtPortuguese.selected = NO;
//		hBtSpanish.selected = YES;
	}
	
	[self setLanguage: [hApplication getLanguage] ];
}

/*==============================================================================================

MENSAGEM onChangeControl
	Chamado quando o usuário modifica o tipo de controle da aplicação.

==============================================================================================*/

- ( IBAction )onChangeControl: ( UIButton* )hButton
{
	if( hButton == hBtCkbTouchScreen )
	{
		hBtCkbAccel.selected = NO;
		hBtCkbTouchScreen.selected = YES;
		
		*pControlType = CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS;
	}
	else if( hButton == hBtCkbAccel )
	{
		hBtCkbAccel.selected = YES;
		hBtCkbTouchScreen.selected = NO;
		
		*pControlType = CONTROL_TYPE_ACCELEROMETERS_ONLY;
	}
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onExit
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

// Fim da implementação da classe
@end
