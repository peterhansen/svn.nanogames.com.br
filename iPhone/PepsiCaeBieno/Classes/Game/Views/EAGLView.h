//
//  EAGLView.h
//  iBob
//
//  Created by Daniel Lopes Alves on 10/3/08.
//  Copyright Nano Games 2008. All rights reserved.
//

#ifndef OPENGL_VIEW_H
#define OPENGL_VIEW_H

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <UIKit/UIKit.h>

#include "AudioManager.h"
#include "Color.h"
#include "Scene.h"
#include "Texture2D.h"

@interface EAGLView : UIView
{	
	@private
		// Largura e altura da tela e do back buffer
		int32 backingWidth;
		int32 backingHeight;

		// Contexto de renderização do OpenGL
		EAGLContext* hContext;
		
		// Nome do OpenGL para os buffers utilizados na renderização
		uint32 viewRenderbuffer, viewFramebuffer, offscreenFrameBuffer;
		
		// Nome do OpenGL para os buffers de profundidade
		uint32 depthRenderbuffer, offscreenDepthBuffer;
	
		// Textura que representa o offscreen renderbuffer
		Texture2D* pOffscreenRenderBuffer;
	
		// Indica se estamos renderizando fora de tela
		bool usingOffscreenBuffer;
		
		// Controlam a atualização da tela
		NSTimer* hAnimationTimer;
		NSTimeInterval animationInterval;
	
		// Ponteiro para a cena atual
		Scene* pCurrScene;
	
		// Armazena a última vez que chamamos update
		float lastUpdateTime;

		// Indica se devemos exibir o conteúdo do offscreen buffer na tela
		bool showOffscreenBuffer;
}

@property ( nonatomic, assign, setter = setCurrScene, getter = getCurrScene ) Scene* pCurrScene;
@property ( nonatomic, assign, setter = setAnimationInterval ) NSTimeInterval animationInterval;

// Inicia o loop de renderização
- ( void ) startAnimation;

// Pçra o loop de renderização
- ( void ) stopAnimation;

// Atualiza e renderiza a cena do jogo
- ( void ) drawView;

// Renderiza a cena do jogo
- ( void ) render;

// Pára o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL
- ( void ) suspend;

// Reinicia o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL
- ( void ) resume;

// Determina se devemos renderizar o offscreen buffer ao invés da cena atual
- ( void ) showOffscreenBuffer: ( bool )s;

// Indica se o usuçrio deseja executar as próximas rederizações fora da tela
- ( bool ) setOffscreenRender: ( bool )s;

// Limpa o offscreen buffer com a cor desejada
- ( void ) clearOffscreenBuffer: ( Color* ) pColor;

// Inicializa a operação de renderização
- ( void ) prepareRender;

// Finaliza a operação de renderização
- ( void ) finishRender;

// Indica se esta view deve receber múltiplos eventos de toque
- ( void ) enableMultipleTouch: ( bool )s;

// Indica que esta view não deve repassar os eventos de toque para outras views, se tornando
// a única tratadora de eventos da aplicação
- ( void ) enableExclusiveTouch: ( bool )s;

// Reseta o jogo
- ( void ) resetGame: ( int8 )currLanguage;

@end

#endif

