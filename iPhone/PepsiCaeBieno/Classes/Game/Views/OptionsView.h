/*
 *  OptionsView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OPTIONS_VIEW_H
#define OPTIONS_VIEW_H 1

#import <UIKit/UIKit.h>

#include "ControlType.h"
#include "NanoTypes.h" //uint8

@interface OptionsView : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;
	
		// Botões de seleção de idioma
		IBOutlet UIButton *hBtPortuguese, *hBtSpanish;
	
		// Botões de seleção do tipo de controle
		IBOutlet UIButton *hBtCkbTouchScreen, *hBtCkbAccel;
	
		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hBackImgPT, *hBackImgES;
		IBOutlet UIImageView *hImgTxtAccelPT, *hImgTxtAccelES;
		IBOutlet UIImageView *hImgFlagControlPT, *hImgFlagControlES;
	
		// Barra que controla os volume dos sons
		IBOutlet UISlider* hVolumeSlider;
	
		// Indica o tipo de controle utilizado pelo jogo
		ControlType *pControlType;
}

// Passa um ponteiro que controla o tipo de input no jogo
- ( void ) setControlType:( ControlType* ) pType;

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Indica que o usuário alterou o volume dos sons da aplicação
- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;

// Chamado quando o usuário modifica o idioma da aplicação
- ( IBAction )onChangeLanguage: ( UIButton* )hButton;

// Chamado quando o usuário modifica o tipo de controle da aplicação
- ( IBAction )onChangeControl: ( UIButton* )hButton;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;

@end

#endif
