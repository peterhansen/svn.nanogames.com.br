#import "SplashNano.h"

#include "ApplicationManager.h"
#include "Config.h"

// Início da implementação da classe
@implementation SplashNano

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM setLanguage
	Determina o idioma da interface.

==============================================================================================*/

- ( void ) setLanguage: ( uint8 )languageIndex
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;

	hCopyrightPT.hidden = !isPortuguese;
	hCopyrightES.hidden = isPortuguese;
}

/*==============================================================================================

MENSAGEM onEnd
	Chama a próxima tela do jogo.

==============================================================================================*/

- ( void ) onEnd
{
	[[ApplicationManager GetInstance] performTransitionToView: VIEW_INDEX_SPLASH_GAME];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	[NSTimer scheduledTimerWithTimeInterval:SPLASH_NANO_DURATION target:self selector:@selector(onEnd) userInfo:NULL repeats:NO];
}

// Fim da implementação da classe
@end

