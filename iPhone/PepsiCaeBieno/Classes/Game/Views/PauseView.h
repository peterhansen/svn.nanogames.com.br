/*
 *  PauseView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/30/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PAUSE_VIEW_H
#define PAUSE_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface PauseView : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;

		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hBackToMenuPT, *hBackToMenuES;
		IBOutlet UIImageView *hBackImgPT, *hBackImgES;
	
		// Barra que controla os volume dos sons
		IBOutlet UISlider* hVolumeSlider;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Indica que o usuário alterou o volume dos sons da aplicação
- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;

// Chamado quando o usuário deseja voltar para a tela de jogo
- ( IBAction )onBackToGame;

// Chamado quando o usuário deseja voltar para o menu principal
- ( IBAction )onExitGame;

@end

#endif

