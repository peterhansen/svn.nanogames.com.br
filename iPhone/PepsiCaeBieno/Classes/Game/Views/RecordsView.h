/*
 *  RecordsView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef RECORDS_VIEW_H
#define RECORDS_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface RecordsView : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;
	
		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hBackImgPT, *hBackImgES;
		IBOutlet UIImageView *hFlagRecPT, *hFlagRecES;
	
		// Label que exibe o 1o recorde
		IBOutlet UILabel *hFirstPointsLabel;
	
		// Controlam a atualização da tela
		NSTimer* hAnimationTimer;
		NSTimeInterval animationInterval;
	
		// Recorde que está piscando
		int8 blinkingRecord;
	
		// Controla a animação do recorde que pisca
		float blinkingController;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Indica os recordes que devem ser exibidos
- ( void ) setRecords: ( uint32* )pRecords;

// Determina o recorde que deve piscar
- ( void ) setBlinkingRecord: ( int8 )index;

// Suspende o processamento da view
- ( void ) suspend;

// Reinicia o processamento da view
- ( void ) resume;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;

@end

#endif
