#include "ConfirmScreen.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"
#include "PepsiCaeBieno.h";

// Implementação da classe
@implementation ConfirmScreen

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM setLanguage
	Determina o idioma da interface.

==============================================================================================*/

- ( void ) setLanguage: ( uint8 )languageIndex;
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;
	
	hBkgPT.hidden = !isPortuguese;
	hQuestionPT.hidden = !isPortuguese;;
	
	hBkgES.hidden = isPortuguese;
	hQuestionES.hidden = isPortuguese;
	
	// getText() retornará o texto já no idioma correto
	hTitle.text = CHAR_ARRAY_TO_NSSTRING( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE) getText: TEXT_INDEX_LEAVE_GAME] );
}

/*==============================================================================================

MENSAGEM onBackToPauseMenu
	Chamado quando o usuário deseja voltar para a tela de pause.

==============================================================================================*/

- ( IBAction )onBackToPauseMenu
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_PAUSE_SCREEN];
}

/*==============================================================================================

MENSAGEM onExitGame
	Chamado quando o usuário deseja voltar para o menu principal.

==============================================================================================*/

- ( IBAction )onExitGame
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;
	(( PepsiCaeBieno* )[[hApplication hGLView] getCurrScene] )->saveScoreIfRecord();
	
	[hApplication performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

// Fim da implementação da classe
@end

