/*
 *  HelpView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef HELP_VIEW_H
#define HELP_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface HelpView : UIView
{
	@private
		// Caixa do texto de ajuda
		IBOutlet UITextView* hHelpText;
	
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;
	
		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hBackImgPT, *hBackImgES;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;

@end

#endif
