//
//  CreditsView.h
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 12/19/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef CREDITS_VIEW_H
#define CREDITS_VIEW_H

#import <UIKit/UIKit.h>
#import <UIKit/UIAlert.h>

#include "NanoTypes.h" //uint8

@interface CreditsView : UIView <UIAlertViewDelegate>
{
	@private
		// Links para os sites
		IBOutlet UILabel* hLinkNanoGames;
		IBOutlet UILabel* hLinkPepsi;
	
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;

		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UILabel *hDevelopedByPT, *hDevelopedByES;
		IBOutlet UILabel *hAllRightsPT, *hAllRightsES;
		IBOutlet UIImageView *hBackImgPT, *hBackImgES;
	
		// Indica qual botão de weblink foi clicado antes de exibirmos o popup
		int8 webButton;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Abandona a aplicação e abre o navegador no site da nanogames
- ( IBAction ) onGoToNanoGamesURL;

// Abandona a aplicação e abre o navegador no site da pepsi
- ( IBAction ) onGoToPepsiURL;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;

@end

#endif

