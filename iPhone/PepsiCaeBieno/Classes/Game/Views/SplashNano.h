//
//  SplashNano.h
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 12/18/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef SPLASH_NANO_H
#define SPLASH_NANO_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface SplashNano : UIView
{
	@private
		// Label que exibe o copyright da marca
		IBOutlet UILabel *hCopyrightPT, *hCopyrightES;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Chama a próxima tela do jogo
- ( void ) onEnd;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
