#include "PauseView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"

// Início da implementção da classe
@implementation PauseView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM setLanguage
	Determina o idioma da interface.

==============================================================================================*/

- ( void ) setLanguage: ( uint8 )languageIndex
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;

	hBkgPT.hidden = !isPortuguese;
	hBackImgPT.hidden = !isPortuguese;
	hBackToMenuPT.hidden = !isPortuguese;
	
	hBkgES.hidden = isPortuguese;
	hBackImgES.hidden = isPortuguese;
	hBackToMenuES.hidden = isPortuguese;
	
	// Atualiza a barra de volume, já que ela pode ter sido modificada em outra tela
	[hVolumeSlider setValue: [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) getAudioVolume] ];
}

/*==============================================================================================

MENSAGEM onVolumeChanged
	Indica que o usuário alterou o volume dos sons da aplicação.

==============================================================================================*/

- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) setVolume: [hVolumeSlider value]];
}

/*==============================================================================================

MENSAGEM onBackToGame
	Chamado quando o usuário deseja voltar para a tela de jogo.

==============================================================================================*/

- ( IBAction )onBackToGame
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_GAME];
}

/*==============================================================================================

MENSAGEM onExitGame
	Chamado quando o usuário deseja voltar para o menu principal.

==============================================================================================*/

- ( IBAction )onExitGame
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_CONFIRM_SCREEN];
}

// Fim da implementção da classe
@end

