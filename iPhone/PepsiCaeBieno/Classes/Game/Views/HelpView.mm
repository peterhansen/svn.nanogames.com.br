#include "HelpView.h"

#include "Config.h"
#include "Macros.h"
#include "PepsiCaeBienoAppDelegate.h"

#if DEBUG
	#include "Tests.h"
#endif

// Início da implementção da classe
@implementation HelpView

/*==============================================================================================

MENSAGEM setLanguage
	Determina o idioma da interface.

==============================================================================================*/

- ( void ) setLanguage: ( uint8 )languageIndex
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;
	hBkgPT.hidden = !isPortuguese;
	hBackImgPT.hidden = !isPortuguese;
	
	hBkgES.hidden = isPortuguese;
	hBackImgES.hidden = isPortuguese;
	
	// getText() retornará o texto já no idioma correto
	hHelpText.text = CHAR_ARRAY_TO_NSSTRING( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE) getText: TEXT_INDEX_HELP] );
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onExit
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

// Fim da implementação da classe
@end

