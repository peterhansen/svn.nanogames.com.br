//
//  MainMenuView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface MainMenuView : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;
	
		// Imagens que representam os botões
		IBOutlet UIImageView *hPlayPT, *hPlayES;
		IBOutlet UIImageView *hHelpPT, *hHelpES;
		IBOutlet UIImageView *hRecordsPT, *hRecordsES;
		IBOutlet UIImageView *hOptionsPT, *hOptionsES;
		IBOutlet UIImageView *hCreditsPT, *hCreditsES;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Mensagem chamada quando o jogador seleciona a opção "Play"
-( IBAction )onPlay;

// Mensagem chamada quando o jogador seleciona a opção "Ranking"
-( IBAction )onRanking;

// Mensagem chamada quando o jogador seleciona a opção "Options"
-( IBAction )onOptions;

// Mensagem chamada quando o jogador seleciona a opção "Help"
-( IBAction )onHelp;

// Mensagem chamada quando o jogador seleciona a opção "Credits"
-( IBAction )onCredits;

@end
