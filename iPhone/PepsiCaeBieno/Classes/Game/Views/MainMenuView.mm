//
//  MainMenuView.mm
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import "MainMenuView.h"

#include "PepsiCaeBienoAppDelegate.h"
#include "Config.h"

#if TARGET_OS_IPHONE

@interface MainMenuView ()

// Extensão da classe para declarar métodos privados
-( void )playTheme;

@end

#endif

// Início da implementação da classe
@implementation MainMenuView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM setLanguage
	Determina o idioma da interface.

==============================================================================================*/

- ( void ) setLanguage: ( uint8 )languageIndex
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;

	hBkgPT.hidden = !isPortuguese;
	hPlayPT.hidden = !isPortuguese;
	hHelpPT.hidden = !isPortuguese;
	hRecordsPT.hidden = !isPortuguese;
	hOptionsPT.hidden = !isPortuguese;
	hCreditsPT.hidden = !isPortuguese;
	
	hBkgES.hidden = isPortuguese;
	hPlayES.hidden = isPortuguese;
	hHelpES.hidden = isPortuguese;
	hRecordsES.hidden = isPortuguese;
	hOptionsES.hidden = isPortuguese;
	hCreditsES.hidden = isPortuguese;
	
#if TARGET_OS_IPHONE
	if( ![(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) isPlayingAudio] )
		[NSTimer scheduledTimerWithTimeInterval: 0.050f target:self selector:@selector( playTheme ) userInfo:NULL repeats:NO];
#endif
}

/*==============================================================================================

MENSAGEM onPlay
	Mensagem chamada quando o jogador seleciona a opção "Play".

==============================================================================================*/

-( IBAction )onPlay
{
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_GAME];
}

/*==============================================================================================

MENSAGEM onRanking
	Mensagem chamada quando o jogador seleciona a opção "Ranking".

==============================================================================================*/

-( IBAction )onRanking
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )[ApplicationManager GetInstance];
	
	[[hApplication hRecordsView] setBlinkingRecord: -1];

	[hApplication performTransitionToView: VIEW_INDEX_RECORDS];
}

/*==============================================================================================

MENSAGEM onOptions
	Mensagem chamada quando o jogador seleciona a opção "Options".

==============================================================================================*/

-( IBAction )onOptions
{
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_OPTIONS];
}

/*==============================================================================================

MENSAGEM onHelp
	Mensagem chamada quando o jogador seleciona a opção "Help".

==============================================================================================*/

-( IBAction )onHelp
{
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_HELP];
}

/*==============================================================================================

MENSAGEM onCredits
	Mensagem chamada quando o jogador seleciona a opção "Credits".

==============================================================================================*/

-( IBAction )onCredits
{
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_CREDITS];
}

/*==============================================================================================

MENSAGEM playTheme
	Inicia a reprodução da música tema do jogo.

==============================================================================================*/

#if TARGET_OS_IPHONE

-( void )playTheme
{
	PepsiCaeBienoAppDelegate* hAppDelegate = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;
	
	if( [hAppDelegate isOlderOSVersion] )
		[hAppDelegate playAudio: SOUND_INDEX_SPLASH];
}

#endif

// Fim da implementação da classe
@end
