/*
 *  ConfirmScreen.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/30/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIRM_SCREEN_VIEW_H
#define CONFIRM_SCREEN_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface ConfirmScreen : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkgPT, *hBkgES;
	
		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hQuestionPT, *hQuestionES;

		// Pergunta feita ao usuário
		IBOutlet UILabel* hTitle;
}

// Determina o idioma da interface
- ( void ) setLanguage: ( uint8 )languageIndex;

// Chamado quando o usuário deseja voltar para a tela de pause
- ( IBAction )onBackToPauseMenu;

// Chamado quando o usuário deseja voltar para o menu principal
- ( IBAction )onExitGame;

@end

#endif

