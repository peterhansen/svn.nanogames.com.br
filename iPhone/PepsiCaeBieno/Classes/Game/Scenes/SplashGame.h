//
//  SplashGame.h
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#ifndef SPLASH_GAME_H
#define SPLASH_GAME_H

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

#include "NanoTypes.h" //uint8

@interface SplashGame : UIImageView
{
	@private
		// Tocador de vídeos
		MPMoviePlayerController* hMoviePlayer;
}

// Construtor chamado quando carregamos a view via código
- ( id )initWithFrame:( CGRect )frame AndLanguage:( uint8 )languageIndex;

// Chama a próxima tela do jogo
- ( void ) onEnd;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
