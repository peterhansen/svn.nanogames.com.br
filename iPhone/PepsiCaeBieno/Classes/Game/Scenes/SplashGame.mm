#import "SplashGame.h"

#include "ApplicationManager.h"
#include "Config.h"
#include "Macros.h"
#include "Utils.h"

// Extensão da classe para declarar métodos privados
@interface SplashGame ()

	// Inicializa o objeto
	- ( bool ) build: ( uint8 )languageIndex;

	// Desaloca o componente reprodutor de vídeos
	- ( void ) deallocMoviePlayer;

	// Tenta impedir o flickering no final da exibição do vídeo
	- ( void ) gambiarra;

@end

// Início da implementação da classe
@implementation SplashGame

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame AndLanguage:( uint8 )languageIndex
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self build: languageIndex] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

- ( bool ) build: ( uint8 )languageIndex
{
	bool isPortuguese = languageIndex == LANGUAGE_INDEX_PORTUGUESE;

	// Background para impedir o fade final do vídeo
	UIImage* hImg;

	NSString* hImgExt = @"png";
	if( isPortuguese )
		hImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"bkgpt", hImgExt ) ];
	else
		hImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"bkges", hImgExt ) ];
		
	if( hImg == nil )
		return false;
	
	UIImageView* hBkg = [[UIImageView alloc]initWithImage: hImg];
	if( hBkg == NULL )
		return false;
	
	CGPoint center = CGPointMake( HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH );
	hBkg.center = center;

	CGAffineTransform transform = [hBkg transform];
	transform = CGAffineTransformRotate( transform, ( M_PI * 0.5f ) );
	hBkg.transform = transform;

	[self addSubview: hBkg];
	hBkg.hidden = YES;
	
	/*
		Because it takes time to load movie files into memory, MPMoviePlayerController
		automatically begins loading your movie file shortly after you initialize a new 
		instance. When it is done preloading the movie file, it sends the
		MPMoviePlayerContentPreloadDidFinishNotification notification to any registered 
		observers. If an error occurred during loading, the userInfo dictionary of the 
		notification object contains the error information. If you call the play method 
		before preloading is complete, no notification is sent and your movie begins 
		playing as soon as it is loaded into memory
	*/

	// Registra-se para receber a mensagem indicando que o vídeo está pronto para reprodução
//	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( moviePreloadDidFinish: ) name: MPMoviePlayerContentPreloadDidFinishNotification object: nil ];
	
    // Carrega o vídeo
	NSURL* hMovieURL;
	NSString* hMovieExt = @"m4v";

	if( isPortuguese )
		hMovieURL = Utils::GetURLForResource( @"pt", hMovieExt );
	else
		hMovieURL = Utils::GetURLForResource( @"es", hMovieExt );
		
	hMoviePlayer = [[ MPMoviePlayerController alloc ] initWithContentURL: hMovieURL];
	if( hMoviePlayer == NULL )
		return false;

    // Registra-se para obter os eventos da reprodução do vídeo
//    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( moviePlayBackDidFinish: ) name: MPMoviePlayerPlaybackDidFinishNotification object: nil];
//    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( movieScalingModeDidChange: ) name: MPMoviePlayerScalingModeDidChangeNotification object: nil];

	// Poderíamos usar MPMovieScalingModeNone, pois já fazemos os videos na resolução de tela do iPhone. No entanto, utilizei
	// MPMovieScalingModeFill caso desejemos economizar espaço
	//hMoviePlayer.scalingMode = MPMovieScalingModeNone;
	hMoviePlayer.scalingMode = MPMovieScalingModeFill;
    
    // Não deixa o usuário ter controle sobre a execução do vídeo
	hMoviePlayer.movieControlMode = MPMovieControlModeHidden;

    // A cor de background do vídeo poder ser qualquer UIColor
    hMoviePlayer.backgroundColor = [UIColor clearColor];

	return true;
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self deallocMoviePlayer];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM deallocMoviePlayer
	Desaloca o componente reprodutor de vídeos.

==============================================================================================*/

- ( void )deallocMoviePlayer
{
	if( hMoviePlayer != NULL )
	{
		// Cancela os eventos da exibição de vídeo
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerContentPreloadDidFinishNotification object:nil];
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object: hMoviePlayer];
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerScalingModeDidChangeNotification object: hMoviePlayer];

		// Deleta o moviePlayer
		[hMoviePlayer release];
		hMoviePlayer = NULL;
	}
}

/*==============================================================================================

MENSAGEM onEnd
	Chama a próxima tela do jogo.

==============================================================================================*/

- ( void ) onEnd
{
	[[ApplicationManager GetInstance] performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	 /*                       
		 As soon as you call the play: method, the player initiates a transition that fades 
		 the screen from your current window content to the designated background 
		 color of the player. If the movie cannot begin playing immediately, the player 
		 object continues displaying the background color and may also display a progress 
		 indicator to let the user know the movie is loading. When playback finishes, the 
		 player uses another fade effect to transition back to your window content.
     */
     
	[hMoviePlayer play];
	[NSTimer scheduledTimerWithTimeInterval:5.5f target:self selector:@selector(gambiarra) userInfo:NULL repeats:NO];
}

/*==============================================================================================

MENSAGEM moviePreloadDidFinish
	Mensagem chamada quando o vídeo acaba de ser carregado.

==============================================================================================*/

- ( void )moviePreloadDidFinish:( NSNotification* )hNotification
{
	// MPMoviePlayerController* hMoviePlayer = [ hNotification object ];
}

/*==============================================================================================

MENSAGEM gambiarra
	Tenta impedir o flickering no final da exibição do vídeo.

==============================================================================================*/

-( void ) gambiarra
{
	// Impede o fade no final do vídeo
	UIImageView* hBkg = ( UIImageView* )[[self subviews] objectAtIndex: 0];
	hBkg.hidden = NO;

	[self bringSubviewToFront: hBkg];
	[hBkg setNeedsDisplay];
	
	[hMoviePlayer stop];
	[self deallocMoviePlayer];
	[self onEnd];
}

/*==============================================================================================

MENSAGEM moviePlayBackDidFinish
	Mensagem chamada quando o video terminar de ser exibido.

==============================================================================================*/

- ( void )moviePlayBackDidFinish:( NSNotification* )hNotification
{
	// Impede o fade no final do vídeo
//	UIImageView* hBkg = ( UIImageView* )[[self subviews] objectAtIndex: 0];
//	hBkg.hidden = NO;
//	
//	CGRect screenSize = [[UIScreen mainScreen] bounds];
//	[hBkg drawRect: screenSize];
//	[hBkg setNeedsDisplay];
//	
//	[hMoviePlayer stop];
//	[self deallocMoviePlayer];
//
//	[hBkg drawRect: screenSize];
//	[hBkg setNeedsDisplay];

//	[self onEnd];
}

/*==============================================================================================

MENSAGEM movieScalingModeDidChange
	Mensagem chamada quando a escala do video é alterada.

==============================================================================================*/

- ( void )movieScalingModeDidChange:( NSNotification* )hNotification
{
	// MPMoviePlayerController* hMoviePlayer = [ hNotification object ];
}

// Fim da implementação da classe
@end
