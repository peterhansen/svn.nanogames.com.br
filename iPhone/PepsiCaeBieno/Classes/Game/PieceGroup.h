/*
 *  PieceGroup.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/22/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PIECE_GROUP_H
#define PIECE_GROUP_H

#include "ObjectGroup.h"

// Forward Declaration para a classe Piece
class Piece;

class PieceGroup : public ObjectGroup
{
	public:
		// Construtor
		// Exceções : ConstructorException
		PieceGroup( Piece* pMovingPiece );
	
		// Destrutor
		virtual ~PieceGroup( void ){};

		// Reinicializa o objeto
		void reset( void );

		// Insere uma nova peça no conjunto de peças
		Piece* insertPiece( const Piece* pPiece );

	private:
		// Inicializa o objeto
		void build( void );
};

#endif
