/*
 *  PauseTag.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 7/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PAUSE_TAG_H
#define PAUSE_TAG_H 1

#include "Collidable.h"

#include "RenderableImage.h"

class PauseTag : public RenderableImage, public Collidable
{
	public:
		// Construtor
		PauseTag( void );
	
		// Destrutor
		virtual ~PauseTag( void ){};

		// Checa colisão do objeto com o ponto
		virtual bool checkCollision( const Point3f* pPoint );
};

#endif
