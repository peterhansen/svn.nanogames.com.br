//
//  PepsiCaeBienoAppDelegate.h
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#ifndef GAME_APP_DELEGATE_H
#define GAME_APP_DELEGATE_H 1

#include "ApplicationManager.h"

#include "Config.h"
#include "ControlType.h"
#include "ConfirmScreen.h"
#include "CreditsView.h"
#include "EAGLView.h"
#include "HelpView.h"
#include "LanguageView.h"
#include "MainMenuView.h"
#include "OptionsView.h"
#include "PauseView.h"
#include "RecordsView.h"
#include "SplashGame.h"
#include "SplashNano.h"

#include "AudioSource.h"

// Forward Declarations
class Font;

@interface PepsiCaeBienoAppDelegate : ApplicationManager <UITabBarControllerDelegate>
{
	@private
	
		/************* TELAS DE JOGO *************/

		// View de seleção de idioma
		LanguageView* hLanguageView;

		// Views dos menus
		MainMenuView* hMainMenu;
	
		HelpView* hHelpView;
		PauseView* hPauseView;
		ConfirmScreen* hConfirmScreen;
		CreditsView* hCreditsView;
		OptionsView* hOptionsView;
		RecordsView* hRecordsView;

		// View do jogo
		EAGLView* hGLView;
		
		// View do splash da nano
		SplashNano* hSplashNano;
		
		// View do splash do jogo
		SplashGame* hSplashGame;
	
		/************* DADOS DA APLICAÇÃO *************/
		
		// Idioma atual do jogo
		uint8 currLanguage;
	
		// Tipo do modo de input
		ControlType controlType;
	
		// Sons do jogo
		AudioSource* appSounds[ APP_N_SOUNDS ];

		// Textos do jogo
		char* texts[ APP_N_LANGUAGES ][ APP_N_TEXTS ];
	
		// Fontes da aplicação
		Font* appFonts[ APP_N_FONTS ];
	
		// Recordes salvos
		uint32 records[ APP_N_RECORDS ];
}

// Gera os getters e setters para as propriedades a seguir
@property ( nonatomic, readonly ) EAGLView* hGLView;
@property ( nonatomic, readonly ) RecordsView* hRecordsView;
@property ( nonatomic, setter = setLanguage, getter = getLanguage ) uint8 currLanguage;

// Retorna um texto da aplicação
- ( char* ) getText: ( uint16 ) textIndex;
	
// Retorna uma fonte da aplicação
- ( Font* ) getFont:( uint8 )fontIndex;

// Inicia a reprodução de um som da aplicação
- ( void ) playAudio: ( uint16 ) audioIndex;

// Indica se a aplicação está tocando algum som
- ( bool )isPlayingAudio;

// Retorna o percentual do volume máximo utilizado na reprodução de sons
- ( float )getAudioVolume;

// Determina o volume dos sons da aplicação
- ( void )setVolume:( float )volume;

// Exibe a tela de fim de jogo
- ( void ) onGameOver: ( int32 )score;

// Retorna o lugar da pontuação na tabela de recordes ou um número negativo caso a jogador
// não tenha alcançado um valor que supere o menor recorde
- ( int8 ) isHighScore: ( int32 )score;

// Armazena a pontuação caso esta seja um recorde
- ( int8 ) setHighScore: ( int32 )score;

// Salva a pontuação atual caso esta seja um recorde
-( bool ) saveScoreIfRecord: ( int32 )score;

// Verifica se suportamos o sistema operacional
- ( bool )isSupportedOS;

// Verifica se suportamos a versão do sistema operacional
- ( bool )isSupportedOSVersion;

// Retorna se a versão do sistema operacional é anterior à 3.0
- ( bool )isOlderOSVersion;

@end

#endif


