#include "PepsiCaeBienoAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

#include "Config.h"
#include "Macros.h"
#include "PepsiCaeBieno.h"

#include "AccelerometerManager.h"
#include "EventManager.h"
#include "Font.h"

#if DEBUG
	#include "Tests.h"
#endif

#if SOUND_WITH_OPEN_AL
	#include "AudioManager.h"
#else
	#include "AppleAudioManager.h"
#endif

// Extensão da classe para declarar métodos privados
@interface PepsiCaeBienoAppDelegate ()

// Carrega as views do jogo
-( void )loadAll;

// Carrega os sons da aplicação
- ( bool ) initSounds;
	
// Carrega os textos da aplicação
- ( bool ) initTexts;

// Carrega as fontes da aplicação
-( bool ) initFonts;

// Libera os textos alocados
- ( void ) cleanTexts;

// Libera os sons alocados
- ( void ) cleanSounds;

// Libera a memória alocada pelas fontes da aplicação
- ( void ) cleanFonts;

// Adiciona um texto ao array de textos da aplicação
-( bool ) addText:( const char* )pText ToLanguage:( uint8 )languageIndex AtIndex:( uint8 )textIndex;

// Retorna o nome do arquivo onde devemos salvar os dados do jogo
-( NSString* )getSaveFileName;

// Carrega possíveis dados salvos
-( bool )loadRecords;

// Salva os dados do jogo
-( bool )saveRecords;

@end

// Implementação da classe
@implementation PepsiCaeBienoAppDelegate

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hGLView, hRecordsView, currLanguage;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/

- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	// OBS: Apesar de essa informação estar em info.plist, precisamos reseta-la aqui para que as UIAlertViews sejam exibidas corretamente
	// TODOO : Pegar a orientação de Info.plist!!!!!!!!!!!!!!!!!!!!!
	[application setStatusBarOrientation: UIInterfaceOrientationLandscapeRight animated:NO];
	
	// Inicializa as variáveis da classe
	hLanguageView = NULL;
	hMainMenu = NULL;
	hHelpView = NULL;
	hCreditsView = NULL;
	hOptionsView = NULL;
	hRecordsView = NULL;
	hPauseView = NULL;
	hConfirmScreen = NULL;
	hGLView = NULL;
	hSplashNano = NULL;
	hSplashGame = NULL;

	currLanguage = LANGUAGE_INDEX_PORTUGUESE;
	controlType = CONTROL_TYPE_TOUCH_SCREEN_N_ACCELEROMETERS;

	// Inicializa os arrays da classe
	memset( appSounds, 0, sizeof( AudioSource* ) * APP_N_SOUNDS );
	memset( texts, 0, sizeof( char* ) * APP_N_LANGUAGES * APP_N_TEXTS );
	memset( appFonts, 0, sizeof( Font* ) * APP_N_FONTS );
	
	// Salva os recordes
	if( ![self loadRecords] )
		memset( records, 0, sizeof( uint32 ) * APP_N_RECORDS );

	// Cria o controlador de sons da aplicação
	#if SOUND_WITH_OPEN_AL
		if( !AudioManager::Create() )
			goto Error;
		AudioManager::GetInstance()->suspend();
	#else
		if( !AppleAudioManager::Create( APP_N_SOUNDS ) )
			goto Error;
		AppleAudioManager::GetInstance()->suspend();
	#endif
	
	// Cria os singletons que irão controlar os eventos de toque e do acelerômetro
	if( !EventManager::Create() )
		goto Error;
	
	if( !AccelerometerManager::Create( ACCELEROMETER_UPDATE_INTERVAL ) )
		goto Error;
	
	if( ![self initTexts] )
		goto Error;
	
	// Indica que este objeto irá receber os eventos do controlador de telas
	// OBS: hViewManager foi criado quando carregamos MainWindow.xib, o que ocorre logo antes de applicationDidFinishLaunching ser chamada
	[hViewManager setDelegate: self];
	
	// Cria a tela de seleção de idioma
	hLanguageView = ( LanguageView* )[self loadViewFromXib: "LanguageView" ];
	if( hLanguageView == NULL )
	{
		#if DEBUG
			LOG( @"PepsiCaeBienoAppDelegate::applicationDidFinishLaunching : Unable to create view" );
		#endif

		goto Error;
	}

	// Exibe a tela de seleção de idioma
	[self performTransitionToView: VIEW_INDEX_SELECT_LANG];
	
	// Impede o device de entrar no modo sleep durante o jogo
	[[UIApplication sharedApplication] setIdleTimerDisabled: YES];

	return;
	
	// Label de tratamento de erros
	Error:
		[self quit: ERROR_ALLOCATING_VIEWS];
		return;
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

// TODOO : O certo seria chamar respondsToSelector( suspend ) no início do método para view
// atual !!!! No final do método, chamaríamos respondsToSelector( resume )

- ( void )performTransitionToView:( uint8 )newIndex;
{
	// Não interrompe uma transição
	if( [hViewManager isTransitioning] )
		return;

    // TASK : Fazer através de controlers e não diretamente das views
	UIView *hNextView;
	NSString *hTransition = nil, *hDirection = nil;
	
	bool keepCurrView = true, forceDraw = false;

	switch( newIndex )
	{
		case VIEW_INDEX_SELECT_LANG:
			hNextView = hLanguageView;
			break;

		case VIEW_INDEX_LOADING:
			[hLanguageView removeFromSuperview];
			[hLanguageView release];
			hLanguageView = NULL;

			[self startLoading];

			currViewIndex = newIndex;
			return;

		case VIEW_INDEX_SPLASH_NANO:
			{
				// Obtém o UIActivityIndicatorView e o deixa invisível
				UIActivityIndicatorView* hLoading = [[hViewManager subviews] objectAtIndex:0];
				[hLoading setHidden: YES];
				
				// Exibe o Splash da Nano Games. A primeira view exibida não realiza transições
				[hViewManager addSubview: hSplashNano];
				[hSplashNano onBecomeCurrentScreen];
				[hSplashNano release];
				
				// Destrói o UIActivityIndicatorView
				[hLoading removeFromSuperview];
				
				currViewIndex = newIndex;
			}
			return;
			
		case VIEW_INDEX_SPLASH_GAME:
			hNextView = hSplashGame;
			hTransition = kCATransitionFade;
			
			#if TARGET_IPHONE_SIMULATOR
				[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_SPLASH];
			#else
				if( ![self isOlderOSVersion] )
					[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_SPLASH];
			#endif
			break;
			
		case VIEW_INDEX_MAIN_MENU:
			
			switch( currViewIndex )
			{
				case VIEW_INDEX_SPLASH_GAME:
					hSplashGame = NULL;
					keepCurrView = false;
					forceDraw = true;
					break;
					
				case VIEW_INDEX_RECORDS:
					[hRecordsView suspend];
					break;
					
				case VIEW_INDEX_GAME:
					[hGLView suspend];
					
				case VIEW_INDEX_CONFIRM_SCREEN:
					[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_SPLASH];
					break;
			}

			hNextView = hMainMenu;
			break;

		case VIEW_INDEX_PAUSE_SCREEN:
			// Suspende o processamento da tela de jogo
			[hGLView suspend];

			// Exibe a tela de pause
			hNextView = hPauseView;
			break;
			
		case VIEW_INDEX_RECORDS:
			if( currViewIndex == VIEW_INDEX_GAME )
			{
				[hGLView suspend];
				[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_SPLASH];
			}
			
			hNextView = hRecordsView;

			[hRecordsView setRecords: records];
			[hRecordsView resume];

			break;

		case VIEW_INDEX_OPTIONS:
			hNextView = hOptionsView;
			break;
			
		case VIEW_INDEX_CONFIRM_SCREEN:
			hNextView = hConfirmScreen;
			break;
			
		case VIEW_INDEX_HELP:
			hNextView = hHelpView;
			break;
			
		case VIEW_INDEX_CREDITS:
			hNextView = hCreditsView;
			break;

		case VIEW_INDEX_GAME:
			{
				hNextView = hGLView;

				if( currViewIndex == VIEW_INDEX_MAIN_MENU )
				{
					// Realiza as modificações decorrentes da seleção de idioma
					[hGLView resetGame: currLanguage];
				}

				// Renderiza a view do jogo sem o jogador ver, já utilizando as novas opções do menu. Assim
				// garantimos que ele não verá a imagem anterior durante a transição
				[hGLView render];
				
				// Reinicia o processamento da tela de jogo
				[hGLView resume];
			}
			break;
			
		default:
			#if DEBUG
				LOG( @"AppDelegate::performTransitionToView() - Invalid index" );
			#endif
			return;
	}

	if( hTransition != nil )
	{
		// Executa a transição de views
		[hViewManager transitionFromSubview: [[hViewManager subviews] objectAtIndex: 0] toSubview: hNextView transition: hTransition direction: hDirection duration: DEFAULT_TRANSITION_DURATION ];
	}
	else
	{
		if( [hNextView respondsToSelector:@selector( setLanguage: ) ] )
			[hNextView setLanguage: currLanguage ];

		[hViewManager addSubview: hNextView];
		
		if( forceDraw )
		{
			[hViewManager bringSubviewToFront: hNextView];
			[hNextView drawRect: [[UIScreen mainScreen] bounds]];
			[hNextView setNeedsDisplay];
		}
		
		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			
			if( hCurrView != hNextView )
			{
				if( keepCurrView )
					[hCurrView retain];

				[hCurrView removeFromSuperview];
			}
		}
	}

	// Atualiza o índice da view que está sendo exibida
	currViewIndex = newIndex;
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

//- ( void )transitionDidStart:( ViewManager* )manager
//{
//}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	switch( currViewIndex )
	{
		case VIEW_INDEX_SPLASH_GAME:
			[hSplashGame onBecomeCurrentScreen];

			// Já que o splash da nano não está mais visível e não o utilizaremos novamente,
			// retira-o da memória
			SAFE_RELEASE( hSplashNano );
			break;
			
		case VIEW_INDEX_MAIN_MENU:
			// Já que o splash do jogo não está mais visível e não o utilizaremos novamente,
			// retira-o da memória
			SAFE_RELEASE( hSplashGame );
			break;
			
		case VIEW_INDEX_GAME:			
			// Reinicia os componentes do jogo
			[hGLView resume];
			break;
	}
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// TODO : Confirmar se a condição do if é testada desse jeito
	// Se estávamos saindo da tela de jogo, retoma o processamento dos componentes
	if( currViewIndex == VIEW_INDEX_MAIN_MENU )
		[hGLView resume];
}

/*==============================================================================================

MENSAGEM loadAll
	Carrega as views do jogo.

==============================================================================================*/

-( void )loadAll
{	
	// Carrega os sons do jogo
	if( ![self initSounds] )
	{
		// Termina a aplicação
		[self quit: ERROR_ALLOCATING_DATA];

		return;
	}

	// Cria as views do jogo
	CGRect screenSize = [[UIScreen mainScreen] bounds];
	
	// Carrega o splash da nano
	PepsiCaeBienoAppDelegate* hAppManager = ( PepsiCaeBienoAppDelegate* )[ApplicationManager GetInstance];
	hSplashNano = ( SplashNano* )[hAppManager loadViewFromXib: "SplashNano" ];

	// Carrega o splash do jogo
	hSplashGame = [[SplashGame alloc] initWithFrame: screenSize AndLanguage: [hAppManager getLanguage] ];
	
	// Cria o menu principal
	hMainMenu = ( MainMenuView* )[hAppManager loadViewFromXib: "MainMenu" ];
	hHelpView = ( HelpView* )[hAppManager loadViewFromXib: "HelpView" ];
	hCreditsView = ( CreditsView* )[hAppManager loadViewFromXib: "CreditsView" ];
	
	hOptionsView = ( OptionsView* )[hAppManager loadViewFromXib: "OptionsView" ];
	[hOptionsView setControlType: &controlType];
	
	hRecordsView = ( RecordsView* )[hAppManager loadViewFromXib: "RecordsView" ];
	hConfirmScreen = ( ConfirmScreen* )[hAppManager loadViewFromXib: "ConfirmScreen" ];
	
	// Cria a tela de pause
	hPauseView = ( PauseView* )[hAppManager loadViewFromXib: "PauseView" ];
	
	// Cria a view do jogo (view OpenGL)
	hGLView = [[EAGLView alloc] initWithFrame:screenSize];
	
	// Carrega as imagens das peças e as fontes do jogo
	// OBS: Estes métodos devem ser chamados obrigatoriamente após a inicialização do OpenGL
	if( !Piece::LoadImages() || ![self initFonts] )
	{	
		// Termina a aplicação
		[self quit: ERROR_ALLOCATING_DATA];

		return;
	}
	
	PepsiCaeBieno* pCurrScene = new PepsiCaeBieno( controlType );

	// Verifica se conseguiu alocar todas as views necessárias
	if( ( hSplashNano == NULL ) || ( hSplashGame == NULL ) || ( hMainMenu == NULL )
		|| ( hHelpView == NULL ) || ( hCreditsView == NULL ) || ( hOptionsView == NULL )
	    || ( hRecordsView == NULL ) || ( hConfirmScreen == NULL ) || ( hPauseView == NULL )
		|| ( hGLView == NULL ) || ( pCurrScene == NULL ) )
	{
		SAFE_DELETE( pCurrScene );

		#if DEBUG
			LOG( @"PepsiCaeBienoAppDelegate::loadAll : Unable to create data" );
		#endif

		// Termina a aplicação
		[self quit: ERROR_ALLOCATING_VIEWS];

		return;
	}
	
	// Associa a primeira cena do jogo ao controlador da API gráfica
	[hGLView setCurrScene: pCurrScene];

	// Exibe a primeira view da aplicação
	[self performTransitionToView: VIEW_INDEX_SPLASH_NANO];
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS : Descomentar se for necessário personalizar
//- ( void )dealloc
//{	
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM applicationWillResignActive
	Mensagem chamada quando a aplicação vai ser suspensa.

==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	#if SOUND_WITH_OPEN_AL
		AudioManager::GetInstance()->resume();
	#else
		AppleAudioManager::GetInstance()->suspend();
	#endif

	// TODOO : O certo seria chamar respondsToSelector( suspend ) para view atual !!!!
	if( currViewIndex == VIEW_INDEX_GAME )
		[hGLView suspend];
	else if( currViewIndex == VIEW_INDEX_RECORDS )
		[hRecordsView suspend];
}

/*==============================================================================================

MENSAGEM applicationDidBecomeActive
	Mensagem chamada quando a aplicação é reiniciada.

==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	#if SOUND_WITH_OPEN_AL
		AudioManager::GetInstance()->resume();
	#else
		AppleAudioManager::GetInstance()->resume();
	#endif
	
	// TODOO : O certo seria chamar respondsToSelector( resume ) para view atual !!!!
	if( currViewIndex == VIEW_INDEX_GAME )
		[hGLView resume];
	else if( currViewIndex == VIEW_INDEX_RECORDS )
		[hRecordsView resume];
}

/*==============================================================================================

MENSAGEM applicationWillTerminate
	Tells the delegate when the application is about to terminate. This method is optional. This
method is the ideal place for the delegate to perform clean-up tasks, such as freeing allocated
memory, invalidating timers, and storing application state.

==============================================================================================*/

- ( void )applicationWillTerminate:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationWillTerminate" );
#endif
	
	// Reabilita o modo sleep
	[[UIApplication sharedApplication] setIdleTimerDisabled: NO];;

	// Suspende o processamento da tela atual
	[self applicationWillResignActive: application];
	
	// Salva os recordes
	[self saveRecords];
	
	// Desaloca os textos, os sons e as fontes da aplicação
	[self cleanTexts];
	[self cleanSounds];
	[self cleanFonts];
	Piece::ReleaseImages();
	
	// Impede de desalocarmos duplamente a tela atual
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView* hCurrView = [hSubviews objectAtIndex: 0];
		[hCurrView retain];
	}
	
	// Desaloca as telas da aplicação
	SAFE_RELEASE( hLanguageView );
	SAFE_RELEASE( hSplashNano );
	SAFE_RELEASE( hSplashGame );
	
	SAFE_RELEASE( hPauseView );
	SAFE_RELEASE( hGLView );
	SAFE_RELEASE( hHelpView );
	SAFE_RELEASE( hOptionsView );
	SAFE_RELEASE( hCreditsView );
	SAFE_RELEASE( hRecordsView );
	SAFE_RELEASE( hConfirmScreen );

	SAFE_RELEASE( hMainMenu );

	// OLD : hViewManager será desalocado automaticamente ao quando abandonarmos a aplicação,  já que o criamos quando carregamos MainWindow.xib
//	// Desaloca o controlador de telas
//	// OBS: Não utiliza SAFE_RELEASE pois obrigatoriamente teremos hViewManager, já que este é criado quando carregamos MainWindow.xib
//	KILL( hViewManager );
	
	// Desaloca os singletons
	AccelerometerManager::Destroy();
	
	#if SOUND_WITH_OPEN_AL
		AudioManager::Destroy();
	#else
		AppleAudioManager::Destroy();
	#endif
	
	EventManager::Destroy();

	// OLD : hWindow será desalocado automaticamente ao quando abandonarmos a aplicação, já que o criamos quando carregamos MainWindow.xib
//	// Desaloca a janela da aplicação
//	// OBS: Não utiliza SAFE_RELEASE pois obrigatoriamente teremos hWindow, já que este é criado quando carregamos MainWindow.xib
//	KILL( hWindow );
}

/*==============================================================================================

MENSAGEM applicationDidReceiveMemoryWarning
	Tells the delegate when the application receives a memory warning from the system. This
method is optional. In this method, the delegate tries to free up as much memory as possible.
After the method returns (and the delegate then returns from applicationWillTerminate), the
application is terminated.

==============================================================================================*/

- ( void )applicationDidReceiveMemoryWarning:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationDidReceiveMemoryWarning" );
#endif
	[self quit: ERROR_MEMORY_WARNING];
}

/*==============================================================================================

MENSAGEM applicationSignificantTimeChange
	Tells the delegate when there is a significant change in the time. This method is optional.
Examples of significant time changes include the arrival of midnight, an update of the time by
a carrier, and the change to daylight savings time. The delegate can implement this method to
adjust any object of the application displays time or is sensitive to time changes.

==============================================================================================*/

- ( void )applicationSignificantTimeChange:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationSignificantTimeChange" );
#endif
}

/*==============================================================================================

MENSAGEM getText
	Retorna um texto da aplicação.

==============================================================================================*/

- ( char* ) getText: ( uint16 ) textIndex
{
	return texts[ currLanguage ][ textIndex ];
}

/*==============================================================================================

MENSAGEM getFont
	Retorna uma fonte da aplicação.

==============================================================================================*/

- ( Font* ) getFont:( uint8 )fontIndex
{
	return appFonts[ fontIndex ];
}

/*==============================================================================================

MENSAGEM playAudio
	Inicia a reprodução de um som da aplicação.

==============================================================================================*/

- ( void ) playAudio: ( uint16 ) audioIndex
{
	#if SOUND_WITH_OPEN_AL
		appSounds[ audioIndex ]->play();
	#else
		// OBS: Feito na correria
		bool looping = ( audioIndex == SOUND_INDEX_SPLASH ) || (( audioIndex >= SOUND_INDEX_AMBIENT_1 ) && ( audioIndex <= SOUND_INDEX_AMBIENT_3 ));
		AppleAudioManager::GetInstance()->playSound( audioIndex, looping, true );
	#endif
}

/*==============================================================================================

MENSAGEM initSounds
	Carrega os sons da aplicação.

==============================================================================================*/

- ( bool ) initSounds
{
#if SOUND_WITH_OPEN_AL

	memset( appSounds, 0, sizeof( AudioSource* ) * APP_N_SOUNDS );
	
	// Obtém o device de reprodução de sons
	PlaybackAudioDevice* pDevice = AudioManager::GetInstance()->getPlaybackDevice();
	pDevice->setDistanceModel( AL_LINEAR_DISTANCE );
	
	// Configura a posição do listener
	Point3f listenerPos;
	AudioListener* pListener = pDevice->getAudioListener();
	pListener->setPosition( &listenerPos );
	pListener->setGain( 1.0f );

	// Cria um novo contexto
	AudioContext* pCurrContext = pDevice->newAudioContext();
	if( !pCurrContext )
		return false;

	pCurrContext->makeCurrentContext();

	// Aloca os sons
#if APP_N_SOUNDS > 9
	#error Se APP_N_SOUNDS for maior que 9, o looping abaixo deve ser modificado
#endif

	char soundPath[] = { 's', 'n', 'd', ' ', '\0' };
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
	{
		soundPath[3] = '0' + i;
		appSounds[i] = Utils::GetAudioSource( pCurrContext, soundPath, "caf" );
		
		if( appSounds[i] == NULL )
		{
			[self cleanSounds];
			return false;
		}
		appSounds[i]->setPosition( &listenerPos );
		appSounds[i]->setMinGain( 1.0f );
		appSounds[i]->setMaxGain( 1.0f );
	}
	
	appSounds[ SOUND_INDEX_SPLASH ]->setLooping( true );
	appSounds[ SOUND_INDEX_AMBIENT_1 ]->setLooping( true );
	appSounds[ SOUND_INDEX_AMBIENT_2 ]->setLooping( true );
	appSounds[ SOUND_INDEX_AMBIENT_3 ]->setLooping( true );

#endif

	return true;
}

/*==============================================================================================

MÉTODO initFonts
	Carrega as fontes da aplicação.

==============================================================================================*/

-( bool ) initFonts
{
	memset( appFonts, 0, sizeof( Font* ) * APP_N_FONTS );
	
	#if FONTS_TRANSPARENT

		char fontNames[] = "fnt  ";
		char suffixs[ APP_N_FONTS ][3] = { "ns", "nb", "w\0" };
		bool monoSpaced[ APP_N_FONTS ] = { true, true, false };

		for( uint8 i = 0 ; i < APP_N_FONTS ; ++i )
		{
			// Monta o nome da fonte
			fontNames[3] = suffixs[i][0];
			fontNames[4] = suffixs[i][1];
			
			// Cria a fonte
			appFonts[ i ] = new Font( fontNames, fontNames, monoSpaced[i] );
			if( !appFonts[ i ] )
			{
				[self cleanFonts];
				return false;
			}
		}
		return true;
	
	#elif FONTS_OPAQUE
	
		char fontNames[] = "fnt ";
		bool monoSpaced[ APP_N_FONTS ] = { false, false, true, true, true, true, true, true };

		for( uint8 i = 0 ; i < APP_N_FONTS ; ++i )
		{
			// Monta o nome da fonte
			fontNames[3] = '0' + i;
			
			// Cria a fonte
			appFonts[ i ] = new Font( fontNames, fontNames, monoSpaced[i] );
			if( !appFonts[ i ] )
			{
				[self cleanFonts];
				return false;
			}
		}
		return true;
	
	#else
		#error É necessário definir no preprocessador umas das opções de fonte: FONTS_TRANSPARENT / FONTS_OPAQUE
	#endif
}

/*==============================================================================================

MENSAGEM isPlayingAudio
	Indica se a aplicação está tocando algum som.

==============================================================================================*/

- ( bool )isPlayingAudio
{
	#if SOUND_WITH_OPEN_AL
		for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
		{
			if( appSounds[i]->getState() == AL_PLAYING )
				return true;
		}
		return false;
	#else
		return AppleAudioManager::GetInstance()->isPlaying();
	#endif
}

/*==============================================================================================

MENSAGEM getAudioVolume
	Retorna o percentual do volume máximo utilizado na reprodução de sons.

==============================================================================================*/

- ( float )getAudioVolume
{
	#if SOUND_WITH_OPEN_AL
		float volume = 1.0f;
		AudioManager::GetInstance()->getPlaybackDevice()->getAudioListener()->getGain( &volume );
		return volume;
	#else
		return AppleAudioManager::GetInstance()->getVolume();
	#endif
}

/*==============================================================================================

MENSAGEM setVolume
	Determina o volume dos sons da aplicação.

==============================================================================================*/

- ( void )setVolume:( float )volume
{
	#if SOUND_WITH_OPEN_AL
		AudioManager::GetInstance()->getPlaybackDevice()->getAudioListener()->setGain( volume );
	#else
		AppleAudioManager::GetInstance()->setVolume( volume );
	#endif
}

/*==============================================================================================

MENSAGEM initTexts
	Carrega os textos da aplicação.

==============================================================================================*/

- ( bool ) initTexts
{	
	{ // Evita problemas com os gotos

		// Português
		if( ![self addText: "Alerta" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_TITLE] )
			goto Error;
		
		if( ![self addText: "Este botão termina o jogo e inicia o navegador da internet. Deseja sair do jogo?" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_MSG_GOTOURL] )
			goto Error;
		
		if( ![self addText: "Esta versão do jogo não suporta o iPhone OS 3.0 ou superiores. Aguarde uma possível atualização." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_MSG_NO_OS_SUPPORT] )
			goto Error;
		
		if( ![self addText: "Sim" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_YES] )
			goto Error;
		
		if( ![self addText: "Não" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_NO] )
			goto Error;
		
		if( ![self addText: "Sair" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_EXIT] )
			goto Error;

		// OLD
//		if( ![self addText: "Olá! Prepare-se para jogar o Pepsi Cai Bem. Neste jogo, as letras caem e você tem que formar palavras positivas com elas para ganhar pontos. Ao formar as palavras, as letras da mesma linha ou coluna somem da tela.\n\nAs palavras válidas estão no lado esquerdo da tela e os pontos aumentam de acordo com a quantidade de letras de cada palavra:\n\n2 LETRAS = 25 PONTOS\n3 LETRAS = 50 PONTOS\n4 LETRAS = 100 PONTOS\n5 LETRAS = 150 PONTOS\n6 LETRAS = 200 PONTOS\n7 LETRAS = 300 PONTOS\n8 LETRAS = 375 PONTOS\n9 LETRAS = 500 PONTOS\n\nVocê pode formar palavras em qualquer sentido horizontal ou vertical.\n\nO jogo tem 5 níveis, cada um com duração de 3 minutos. Se você deixar as peças chegarem ao topo da tela, é fim de jogo para você!\n\nControles:\n\nMover a peça: incline seu iPhone para a esquerda ou direita.\n\nAcelerar ou frear a queda: incline seu iPhone para frente ou para trás.\n\nEstá preparado?\nBoa Sorte!" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_HELP] )
//			goto Error;
		
		if( ![self addText: "Olá! Prepare-se para jogar o Pepsi Cai Bem. Neste jogo, as letras caem e você tem que formar palavras positivas com elas para ganhar pontos. Ao formar as palavras, as letras da mesma linha ou coluna somem da tela.\n\nAs palavras válidas estão no lado esquerdo da tela e os pontos aumentam de acordo com a quantidade de letras de cada palavra:\n\n2 LETRAS = 25 PONTOS\n3 LETRAS = 50 PONTOS\n4 LETRAS = 100 PONTOS\n5 LETRAS = 150 PONTOS\n6 LETRAS = 200 PONTOS\n7 LETRAS = 300 PONTOS\n\nVocê pode formar palavras em qualquer sentido horizontal ou vertical.\n\nO jogo tem 5 níveis, cada um com duração de 3 minutos. Se você deixar as peças chegarem ao topo da tela, é fim de jogo para você!\n\nControles:\n\nMover a peça: incline seu iPhone para a esquerda ou direita.\n\nAcelerar ou frear a queda: incline seu iPhone para frente ou para trás.\n\nEstá preparado?\nBoa Sorte!" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_HELP] )
			goto Error;

		if( ![self addText: "Deseja sair do jogo?" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_LEAVE_GAME] )
			goto Error;
		
		char ptWords[ TOTAL_WORDS ][ WORD_MAX_LENGTH + 1 ] =	{
																	// OLD
//																	"OK\0\0\0\0\0\0\0",
//																	"BOM\0\0\0\0\0\0",
//																	"LOL\0\0\0\0\0\0",
//																	"OLA\0\0\0\0\0\0",
//																	"SOL\0\0\0\0\0\0",
//																	"WOW\0\0\0\0\0\0",
//																	"AMOR\0\0\0\0\0",
//																	"COOL\0\0\0\0\0",
//																	"HOJE\0\0\0\0\0",
//																	"XOXO\0\0\0\0\0",
//																	"AGORA\0\0\0\0",
//																	"ANIMO\0\0\0\0",
//																	"BRAVO\0\0\0\0",
//																	"GOOOL\0\0\0\0",
//																	"SABOR\0\0\0\0",
//																	"VAMOS\0\0\0\0",
//																	"AMIGOS\0\0\0",
//																	"BEIJOS\0\0\0",
//																	"JUNTOS\0\0\0",
//																	"ABRACOS\0\0",
//																	"CARINHO\0\0",
//																	"CONECTE\0\0",
//																	"SORRISO\0\0",
//																	"DIVERSAO\0",
//																	"OTIMISMO\0",
//																	"POSITIVO\0",
//																	"BONISSIMO",
//																	"DIVERTIDO",
//																	"APROVEITO",
//																	"AUTENTICO"

																	"OK\0\0\0\0\0\0\0",
																	"BOM\0\0\0\0\0\0",
																	"OBA\0\0\0\0\0\0",
																	"OLA\0\0\0\0\0\0",
																	"GOL\0\0\0\0\0\0",
																	"AMOR\0\0\0\0\0",
																	"NOVO\0\0\0\0\0",
																	"HOJE\0\0\0\0\0",
																	"SHOW\0\0\0\0\0",
																	"AGORA\0\0\0\0",
																	"BEIJO\0\0\0\0",
																	"SABOR\0\0\0\0",
																	"VAMOS\0\0\0\0",
																	"CERTO\0\0\0\0",
																	"TODOS\0\0\0\0",
																	"AMIGOS\0\0\0",
																	"JUNTOS\0\0\0",
																	"FUTURO\0\0\0",
																	"SORRIA\0\0\0",
																	"RENOVAR\0\0"
																};

		for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
		{
			if( ![self addText: ptWords[i] ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_FIRST_WORD + i ] )
				goto Error;
		}

		// Espanhol
		if( ![self addText: "Alerta" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_POPUP_TITLE] )
			goto Error;
		
		if( ![self addText: "Este botón termina el juego y inicia el navegador de la internet.¿Desea salir del juego?" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_POPUP_MSG_GOTOURL] )
			goto Error;
		
		if( ![self addText: "Esta versión del juego no suporta el iPhone OS 3.0 o superiors. Aguarde una posible actualización." ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_POPUP_MSG_NO_OS_SUPPORT] )
			goto Error;
		
		if( ![self addText: "Sí" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_POPUP_YES] )
			goto Error;
		
		if( ![self addText: "No" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_POPUP_NO] )
			goto Error;
		
		if( ![self addText: "Salir" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_POPUP_EXIT] )
			goto Error;

		// OLD
//		if( ![self addText: "¡Hola! Preparate para jugar el Cae Bien. En este juego las letras caen y tu tienes que formar palabras positivas para ganar puntos. Además las letras convertidas en palabras desaparecen de la pantalla.\n\nLas palabras que suman puntos están en el lado izquierdo de la pantalla y los puntos aumentan de acuerdo con la cantidad de letras en cada palabra:\n\n2 LETRAS = 25 PUNTOS\n3 LETRAS = 50 PUNTOS\n4 LETRAS = 100 PUNTOS\n5 LETRAS = 150 PUNTOS\n6 LETRAS = 200 PUNTOS\n7 LETRAS = 300 PUNTOS\n8 LETRAS = 375 PUNTOS\n9 LETRAS = 500 PUNTOS\n\nDebes formar palabras situadas de arriba abajo o de abajo arriba, de izquierda a derecha o de derecha a izquierda, controlando la caída de las letras. Incline su iPhone para mover las letras para derecha, izquierda o acelerar y refrenar la caída.\n\nEl juego tiene 5 niveles y tú tienes 3 minutos en cada nivel. Pero si se te llena la pantalla de piezas, has perdido la partida.\n\nEn el último nivel, pierdes si se te llena la pantalla o ganas si lo juegas por los 3 minutos.\n\n¿Estás preparado?\n¡Buena Suerte!" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_HELP] )
//			goto Error;
		
		if( ![self addText: "¡Hola! Preparate para jugar el Cae Bien. En este juego las letras caen y tu tienes que formar palabras positivas para ganar puntos. Además las letras convertidas en palabras desaparecen de la pantalla.\n\nLas palabras que suman puntos están en el lado izquierdo de la pantalla y los puntos aumentan de acuerdo con la cantidad de letras en cada palabra:\n\n2 LETRAS = 25 PUNTOS\n3 LETRAS = 50 PUNTOS\n4 LETRAS = 100 PUNTOS\n5 LETRAS = 150 PUNTOS\n6 LETRAS = 200 PUNTOS\n7 LETRAS = 300 PUNTOS\n\nDebes formar palabras situadas de arriba abajo o de abajo arriba, de izquierda a derecha o de derecha a izquierda, controlando la caída de las letras. Incline su iPhone para mover las letras para derecha, izquierda o acelerar y refrenar la caída.\n\nEl juego tiene 5 niveles y tú tienes 3 minutos en cada nivel. Pero si se te llena la pantalla de piezas, has perdido la partida.\n\nEn el último nivel, pierdes si se te llena la pantalla o ganas si lo juegas por los 3 minutos.\n\n¿Estás preparado?\n¡Buena Suerte!" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_HELP] )
			goto Error;
		
		if( ![self addText: "¿Desea salir del juego?" ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_LEAVE_GAME] )
			goto Error;
		
		char esWords[ TOTAL_WORDS ][ WORD_MAX_LENGTH + 1 ] =	{
																	// OLD
//																	"OK\0\0\0\0\0\0\0",
//																	"HOY\0\0\0\0\0\0",
//																	"LOL\0\0\0\0\0\0",
//																	"SOL\0\0\0\0\0\0",
//																	"WOW\0\0\0\0\0\0",
//																	"AMOR\0\0\0\0\0",
//																	"COOL\0\0\0\0\0",
//																	"HOLA\0\0\0\0\0",
//																	"XOXO\0\0\0\0\0",
//																	"ANIMO\0\0\0\0",
//																	"AHORA\0\0\0\0",
//																	"BESOS\0\0\0\0",
//																	"BRAVO\0\0\0\0",
//																	"BUENO\0\0\0\0",
//																	"GOOOL\0\0\0\0",
//																	"SABOR\0\0\0\0",
//																	"VAMOS\0\0\0\0",
//																	"AMIGOS\0\0\0",
//																	"JUNTOS\0\0\0",
//																	"SONRIO\0\0\0",
//																	"ABRAZOS\0\0",
//																	"COMPARTE\0",
//																	"DISFRUTO\0",
//																	"POSITIVO\0",
//																	"AUTENTICO",
//																	"BUENISIMO",
//																	"CONECTATE",
//																	"DIVERSION",
//																	"DIVERTIDO",
//																	"OPTIMISMO"
			
																	"OK\0\0\0\0\0\0\0",
																	"HOY\0\0\0\0\0\0",
																	"LOL\0\0\0\0\0\0",
																	"GOL\0\0\0\0\0\0",
																	"WOW\0\0\0\0\0\0",
																	"AMOR\0\0\0\0\0",
																	"COOL\0\0\0\0\0",
																	"HOLA\0\0\0\0\0",
																	"XOXO\0\0\0\0\0",
																	"ANIMO\0\0\0\0",
																	"AHORA\0\0\0\0",
																	"BESOS\0\0\0\0",
																	"BRAVO\0\0\0\0",
																	"BUENO\0\0\0\0",
																	"SABOR\0\0\0\0",
																	"VAMOS\0\0\0\0",
																	"AMIGOS\0\0\0",
																	"JUNTOS\0\0\0",
																	"SONRIE\0\0\0",
																	"ABRAZOS\0\0"
																};

		for( uint8 i = 0 ; i < TOTAL_WORDS ; ++i )
		{
			if( ![self addText: esWords[i] ToLanguage: LANGUAGE_INDEX_SPANISH AtIndex: TEXT_INDEX_FIRST_WORD + i ] )
				goto Error;
		}
		
		return true;

	} // Evita problemas com os gotos

	// Label de tratamento de erros
	Error:
		[self cleanTexts];
		return false;
}

/*==============================================================================================

MENSAGEM addText
	Adiciona um texto ao array de textos da aplicação.

==============================================================================================*/

#define MAX_TEXT_LEN 2048

-( bool ) addText:( const char* )pText ToLanguage:( uint8 )languageIndex AtIndex:( uint8 )textIndex
{	
	char buffer[ MAX_TEXT_LEN ];
	snprintf( buffer, MAX_TEXT_LEN, "%s", pText );
	
	// +1 = Espaço extra para conter o '\0' que termina a string
	int32 len = strlen( buffer ) + 1;
	
	texts[ languageIndex ][ textIndex ] = new  char[ len ];
	if( !texts[ languageIndex ][ textIndex ] )
		return false;
	
	snprintf( texts[ languageIndex ][ textIndex ], len, "%s", buffer );
	
	#if DEBUG
		LOG( @"Language %d, TextIndex %d = %s", languageIndex, textIndex, texts[ languageIndex ][ textIndex ] );
	#endif
	
	return true;
}

#undef MAX_TEXT_LEN

/*==============================================================================================

MENSAGEM cleanTexts
	Libera os textos alocados.

==============================================================================================*/

- ( void ) cleanTexts
{
	for( uint8 i = 0 ; i < APP_N_LANGUAGES ; ++i )
	{
		for( uint8 j = 0 ; j < APP_N_TEXTS ; ++j )
		{
			delete texts[ i ][ j ];
			texts[ i ][ j ] = NULL;
		}
	}
}

/*==============================================================================================

MENSAGEM cleanSounds
	Libera os sons alocados.

==============================================================================================*/

- ( void ) cleanSounds
{
	// OBS: Esse trabalho é feito automaticamente por AudioManager
//	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
//	{
//		delete appSounds[ i ];
//		appSounds[ i ] = NULL;
//	}
}

/*==============================================================================================

MÉTODO cleanFonts
	Libera a memória alocada pelas fontes da aplicação.

==============================================================================================*/

- ( void ) cleanFonts
{
	for( uint8 i = 0 ; i < APP_N_FONTS ; ++i )
	{
		delete appFonts[ i ];
		appFonts[ i ] = NULL;
	}
}

/*==============================================================================================

MENSAGEM onGameOver
	Exibe a tela de fim de jogo.

==============================================================================================*/

- ( void ) onGameOver: ( int32 )score
{
	if( [self saveScoreIfRecord: score] )
		[self performTransitionToView: VIEW_INDEX_RECORDS];
	else
		[self performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM saveScoreIfRecord
	Salva a pontuação atual caso esta seja um recorde.

==============================================================================================*/

-( bool ) saveScoreIfRecord: ( int32 )score
{
	int8 highScoreIndex;
	if( ( highScoreIndex = [self setHighScore: score] ) >= 0 )
	{
		[hRecordsView setBlinkingRecord: highScoreIndex];
		return true;
	}
	return false;
}

/*==============================================================================================

MENSAGEM isHighScore
	Retorna o lugar da pontuação na tabela de recordes ou um número negativo caso a jogador
não tenha alcançado um valor que supere o menor recorde.

==============================================================================================*/

- ( int8 ) isHighScore: ( int32 )score
{
	for( int32 i = 0; i < APP_N_RECORDS ; ++i )
	{
		if( score > records[ i ] )
			return i;
	}	
	return -1;		
}

/*==============================================================================================

MENSAGEM setHighScore
	Armazena a pontuação caso esta seja um recorde.

==============================================================================================*/

-( int8 )setHighScore: ( int32 )score
{
	for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		if( score > records[ i ] )
		{
			for( uint8 j = APP_N_RECORDS - 1 ; j > i ; --j )
				records[ j ] = records[ j - 1 ];
			
			records[ i ] = score;

			[self saveRecords];
			
			return i;
		}
	}
	return -1;
}

/*==============================================================================================

MENSAGEM getSaveFileName
	Retorna o nome do arquivo onde devemos salvar os dados do jogo.

==============================================================================================*/

-( NSString* )getSaveFileName
{
	return @"sv0.sv";
}

/*==============================================================================================

MENSAGEM loadRecords
	Carrega possíveis dados salvos.

==============================================================================================*/

-( bool )loadRecords
{
	// Obtém o nome do arquivo de save
	NSString* hFileName = [self getSaveFileName];

	// Verifica se possuímos um diretório onde podemos criar arquivos
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
		#endif

		return false;
	}

	// Verifica se já salvamos recordes 
	NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
	if( ![[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
		return false;
	
	// Carrega os recordes salvos
	FILE *pFile = fopen( [hFilePath UTF8String], "r" );
	if( pFile == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not open file %s for reading", [hFilePath UTF8String] );
		#endif

		return false;
	}
	
	bool ret = true;
	uint32 nRecordsLoaded = 0;

	if( ( nRecordsLoaded = fread( records, sizeof( uint32 ), APP_N_RECORDS, pFile ) ) < APP_N_RECORDS )
	{
		#if DEBUG
			LOG( @"ERROR: Could load only the first %d records", nRecordsLoaded );
		#endif
		
		ret = false;
	}

	fclose( pFile );

	return ret;
}

/*==============================================================================================

MENSAGEM saveRecords
	Salva os dados do jogo.

==============================================================================================*/

-( bool )saveRecords
{
	// Obtém o nome do arquivo de save
	NSString* hFileName = [self getSaveFileName];
	
	// Verifica se possuímos um diretório onde podemos criar arquivos
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
		#endif

		return false;
	}

	// TODOO : O ideal seria verificar se os recordes atuais são diferentes dos recordes existentes no início desta execução. Para
	// isso, teríamos que criar um array uint32 recordsAtLastRun[ APP_N_RECORDS ] e copiar nele os dados de 'records' em applicationDidFinishLaunching

	// Verifica se existe algum recorde diferente de 0
	uint8 i;
	for( i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		if( records[i] != 0 )
			break;
	}

	if( i >= APP_N_RECORDS )
		return true;
	
	// Monta o nome do arquivo onde iremos salvar os recordes
	NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
	
	// Abre / cria o arquivo
	FILE* pFile = fopen( [hFilePath UTF8String], "w" );
	if( pFile == NULL )
	{
		#if DEBUG
			if( [[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
				LOG( @"ERROR: Could not open file %s for writing", [hFilePath UTF8String] );
			else
				LOG( @"ERROR: Could not create file %s", [hFilePath UTF8String] );
		#endif

		return false;
	}
	
	bool ret = true;
	uint32 nRecordsSaved = 0;

	if( ( nRecordsSaved = fwrite( records, sizeof( uint32 ), APP_N_RECORDS, pFile ) ) < APP_N_RECORDS )
	{
		#if DEBUG
			LOG( @"ERROR: Could save only the first %d records", nRecordsSaved );
		#endif
		
		ret = false;
	}

	// Fecha o arquivo
	fclose( pFile );

	return ret;
}

/*==============================================================================================

MENSAGEM isSupportedOS
	Verifica se suportamos o sistema operacional.

==============================================================================================*/

- ( bool )isSupportedOS
{
	return true;

	// OLD
//	NSString *hOSName = [[UIDevice currentDevice] systemName];
//	return [[hOSName stringByReplacingOccurrencesOfString: @" " withString: @""] caseInsensitiveCompare: @"iPhoneOS"] == NSOrderedSame;
}

/*==============================================================================================

MENSAGEM isSupportedOSVersion
	Verifica se suportamos a versão do sistema operacional.

==============================================================================================*/

#define N_SUPPORTED_OS_VERSIONS 4

- ( bool )isSupportedOSVersion
{
	return true;

	// OLD
//	NSString *hVersion = [[UIDevice currentDevice] systemVersion];
//	NSString *hVersions[ N_SUPPORTED_OS_VERSIONS ] = { @"2.0", @"2.1", @"2.2", @"2.2.1" };
//	
//	for( uint8 i = 0 ; i < N_SUPPORTED_OS_VERSIONS ; ++i )
//	{
//		if( [hVersion compare: hVersions[i] ] == NSOrderedSame )
//			return true;
//	}
//	return false;
}

#undef N_SUPPORTED_OS_VERSIONS

/*==============================================================================================

MENSAGEM isOlderOSVersion
	Retorna se a versão do sistema operacional é anterior à 3.0.

==============================================================================================*/
				   
- ( bool )isOlderOSVersion
{
	return true;
	
	// OLD
//	unichar zero = '0';
//	return ( [[[UIDevice currentDevice] systemVersion] characterAtIndex:0] - zero ) < 3;
}				   

// Fim da implementação da classe
@end
