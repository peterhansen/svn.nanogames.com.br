#include "PauseTag.h"

#define PAUSE_TAG_COLLISION_QUAD_HEIGHT 27.0f

/*======================================================================================

CONSTRUTOR

======================================================================================*/

PauseTag::PauseTag( void ) : RenderableImage( "pause" ), Collidable()
{
}

/*======================================================================================

MÉTODO checkCollision
	Checa colisão do objeto com o ponto.

======================================================================================*/

bool PauseTag::checkCollision( const Point3f* pPoint )
{
	return Utils::IsPointInsideRect( pPoint, getPosition()->x, getPosition()->y, getWidth(), PAUSE_TAG_COLLISION_QUAD_HEIGHT );
}
