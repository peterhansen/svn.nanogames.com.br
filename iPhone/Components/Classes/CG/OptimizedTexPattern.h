/*
 *  OptimizedTexPattern.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef OPTIMIZED_TEX_PATTERN_H
#define OPTIMIZED_TEX_PATTERN_H 1

// Components
#include "Object.h"
#include "Quad.h"
#include "ResourceManager.h"
#include "Texture2D.h"

// OpenGL
#include "GLHeaders.h"

class OptimizedTexPattern : public Object
{
	public:
		// Construtores
		OptimizedTexPattern( const char* pImg, LoadTextureFunction pLoadTextureFunction = &( ResourceManager::GetTexture2D ) );
		OptimizedTexPattern( const OptimizedTexPattern* pOther );
	
		// Destrutor
		virtual ~OptimizedTexPattern( void ){};
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Determina a cor do conjunto de vértices a ser utilizada na renderização
		void setVertexSetColor( const Color& color );
	
		// Retorna a cor do conjunto de vértices utilizado na renderização
		Color* getVertexSetColor( Color* pColor ) const;
	
		// TODOO: Pegar o valor padrão de acordo com a forma de carregar a imagem utilizada do disco!!! Ou seja, criar uma nova informação em Texture2D
		// Determina os fatores de blending que determinam como as componentes R, G, B e A são calculadas.
		// Os valores aceitos para srcFactor são:
		// - GL_ZERO
		// - GL_ONE
		// - GL_DST_COLOR
		// - GL_ONE_MINUS_DST_COLOR
		// - GL_SRC_ALPHA
		// - GL_ONE_MINUS_SRC_ALPHA
		// - GL_DST_ALPHA
		// - GL_ONE_MINUS_DST_ALPHA
		// - GL_SRC_ALPHA_SATURATE
		// O valor padrão é GL_ONE.
		//
		// Os valores aceitos para dstFactor são:
		// - GL_ZERO
		// - GL_ONE
		// - GL_SRC_COLOR
		// - GL_ONE_MINUS_SRC_COLOR
		// - GL_SRC_ALPHA
		// - GL_ONE_MINUS_SRC_ALPHA
		// - GL_DST_ALPHA
		// - GL_ONE_MINUS_DST_ALPHA
		// O valor padrão é GL_ONE_MINUS_SRC_ALPHA.
		void setBlenFunc( GLenum srcFactor, GLenum dstFactor );
	
		// Determina o tamanho do objeto
		virtual void setSize( float width, float height, float depth = 1.0f );
	
	private:
		// Inicializa o objeto
		// Exceções : Constructor Exception
		void build( Texture2DHandler pImg );
	
		// Fatores que indicam como as componentes R, G, B e A são calculadas no blending. Ver glBlendFunc
		GLenum texBlendSrcFactor, texBlendDstFactor;

		// Modo de texturização. Ver glTexEnv
		GLenum texEnvMode;
	
		// Cor do conjunto de vértices a ser utilizada na renderização
		Color vertexSetColor;
	
		// Conjunto de vértices utilizado para desenharmos a textura
		Quad vertexes;
	
		// Textura contendo a imagem do objeto
		Texture2DHandler pTex;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

inline Color* OptimizedTexPattern::getVertexSetColor( Color* pColor ) const
{
	*pColor = vertexSetColor;
	return pColor;
}

inline void OptimizedTexPattern::setBlenFunc( GLenum srcFactor, GLenum dstFactor )
{
	texBlendSrcFactor = srcFactor;
	texBlendDstFactor = dstFactor;
}

#endif
