#include "Label.h"

// Components
#include "Bitwise.h"
#include "ObjcMacros.h"
#include "Quad.h"
#include "Utils.h"
#include "GLHeaders.h"
// TODOO : Retirar e criar classes NanoApplication e 3DAPIManager
//#include "EAGLView.h"
//#include "FreeKickAppDelegate.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Label::Label( Font* pFont, bool manageFont )
	  : Object(),
		alignment( ALIGN_HOR_LEFT | ALIGN_VER_TOP ),
		// OLD hCurrText( NULL ),
		pCurrText( NULL ),
		pPreProcessedImg( NULL ),
		pCurrFont( pFont ),
		charSpacement( 0.0f ),
		manageFont( manageFont ),
		vertexSetColor( COLOR_WHITE )
{
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Label::Label( const char* pFontImgsId, const char* pFontDescriptor, bool manageFont )
	  : Object(),
		alignment( ALIGN_HOR_LEFT | ALIGN_VER_TOP ),
		// OLD hCurrText( NULL ),
		pCurrText( NULL ),
		pPreProcessedImg( NULL ),
		pCurrFont( new Font( pFontImgsId, pFontDescriptor ) ),
		charSpacement( 0.0f ),
		manageFont( manageFont ),
		vertexSetColor( COLOR_WHITE )
{
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Label::~Label( void )
{
	// OLD SAFE_RELEASE( hCurrText );
	DELETE_VEC( pCurrText );

	DELETE( pPreProcessedImg );
	
	if( manageFont )
	{
		DELETE( pCurrFont );
	}
	else
	{
		pCurrFont = NULL;
	}
}

/*==============================================================================================

MÉTODO setFont
	Determina a fonte a ser utilizada na renderização do texto.

==============================================================================================*/

void Label::setFont( Font* pFont, bool manageFont )
{
	pCurrFont = pFont;
	this->manageFont = manageFont;
}

/*==============================================================================================

MÉTODO setText
	Determina o texto do label.

==============================================================================================*/

// OLD
//bool Label::setText( const char* pText, bool resize, bool preProcess )
//{
//	return setText( CHAR_ARRAY_TO_NSSTRING( pText ), resize, preProcess );
//}

bool Label::setText( const char* pText, bool resize, bool preProcess )
{
	// Libera um possível texto anterior
	DELETE_VEC( pCurrText );
	
	// Armazena o texto passado como parâmetro
	int32 len = strlen( pText );
	pCurrText = new char[ len + 1 ];
	if( !pCurrText )
		return false;
	
	strcpy( pCurrText, pText );
	
	// Redimensiona o label para comportar o texto
	if( resize )
	{
		Point2i textSize;
		pCurrFont->getTextSize( &textSize, pCurrText, charSpacement );
		setSize( textSize.x, textSize.y, 1.0f );
	}
	
	// Verifica se deve fazer o pré-processamento da renderização do label
	return preProcess ? preProcessText() : true;

}

/*==============================================================================================

MÉTODO setText
	Determina o texto do label.

==============================================================================================*/

// OLD
//bool Label::setText( const NSString* hText, bool resize, bool preProcess )
//{
//	// Libera um possível texto anterior
//	SAFE_RELEASE( hCurrText );
//	
//	// Armazena o texto passado como parâmetro
//	hCurrText = hText;
//	[hCurrText retain];
//	
//	// Redimensiona o label para comportar o texto
//	if( resize )
//	{
//		Point2i textSize;
//		pCurrFont->getTextSize( &textSize, hText, charSpacement );
//		setSize( textSize.x, textSize.y, 1.0f );
//	}
//	
//	// Verifica se deve fazer o pré-processamento da renderização do label
//	return preProcess ? preProcessText() : true;
//}

// OLD
//bool Label::setText( const NSString* hText, bool resize, bool preProcess )
//{
//	return setText( NSSTRING_TO_CHAR_ARRAY( hText ), resize, preProcess );
//}

/*==============================================================================================

MÉTODO setVertexSetColor
	Determina a cor do conjunto de vértices a ser utilizada na renderização.

==============================================================================================*/

void Label::setVertexSetColor( const Color& color )
{
	vertexSetColor = color;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Label::render( void )
{
	if( !Object::render() )
		return false;

	if( pPreProcessedImg != NULL )
		renderPreprocessed();
	else if( pCurrFont != NULL )
		renderWithFont();
	else
		return false;

	return true;
}

/*==============================================================================================

MÉTODO preProcessText
	Renderiza o texto para uma textura visando otimizar renderizações futuras.

==============================================================================================*/
/*
bool Label::preProcessText( void )
{
	// Libera uma possível memória alocada
	DELETE( pPreProcessedImg );
	
	// Cria a textura que irá conter os dados da renderização
	pPreProcessedImg = new Texture2D();
	if( !pPreProcessedImg || !pPreProcessedImg->loadTextureEmpty( Bitwise::getNextPowerOf2( size.x ), Bitwise::getNextPowerOf2( size.y ) ))
		goto Error;
	
	{ //Evita problemas com o goto

		// Indica que iremos renderizar fora da tela
		EAGLView* p3DAPIManager = [(( FreeKickAppDelegate* ) APP_DELEGATE ) hGLView];
		[p3DAPIManager setOffscreenRender: true];
		
		Color clearColor( COLOR_TRANSPARENT );
		[p3DAPIManager clearOffscreenBuffer: &clearColor];
		
		// TODO : Colocar esse trecho de código em um lugar melhor
		// Prepara a cena
		glMatrixMode( GL_PROJECTION );
		glPushMatrix();
		glLoadIdentity();
		glOrthof( 0.0f, SCREEN_WIDTH, 0.0f, SCREEN_HEIGHT, -1.0f, 1.0f );
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		glLoadIdentity();

		// Executa a renderização
		NanoRect area( 0.0f, 0.0f, size.x, size.y );
		
		// OLD
//		pCurrFont->renderText( hCurrText, charSpacement, &area, alignment );
		
		pCurrFont->renderText( pCurrText, charSpacement, &area, alignment );

		// TODOO : Colocar esse trecho de código em um lugar melhor
		glPopMatrix();
		glMatrixMode( GL_PROJECTION );
		glPopMatrix();
		
		// Obtém apenas o pedaço da imagem que desejamos
		glBindTexture( GL_TEXTURE_2D, pPreProcessedImg->getTextureId() );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
		glGetError();
		glCopyTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, 0, 0, size.x, size.y );
		GLenum err = glGetError();
		glBindTexture( GL_TEXTURE_2D, GL_NONE );
		
		// Coloca o buffer da tela novamente como o alvo de renderização
		[p3DAPIManager setOffscreenRender: false];
		
		if( err != GL_NO_ERROR )
			goto Error;
		
	} //Evita problemas com o goto
	
	return true;
	
	// Label de tratamento de erros
	Error:
		DELETE( pPreProcessedImg );
		return false;
}
*/
/*==============================================================================================

MÉTODO renderPreprocessed
	Renderiza a textura contendo o texto.

==============================================================================================*/
/*
void Label::renderPreprocessed( void )
{
	// Executa as transformações sobre os vértices do sprite
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x + ( size.x * 0.5f ), position.y + ( size.y * 0.5f ), position.z );
	glScalef( getWidth(), getHeight(), 1.0f );

	Matrix4x4 rot;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &rot ) ) );

	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pPreProcessedImg->load();
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );

	// Renderiza o label
	Quad aux;
	aux.render();
	
	// Reseta os estados do OpenGL
	pPreProcessedImg->unload();
	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );

	// Cancela as transformações do sprite
	glPopMatrix();
}
*/
/*==============================================================================================

MÉTODO renderWithFont
	Renderiza o texto, caractere a caractere, utilizando as informações da fonte.

==============================================================================================*/

void Label::renderWithFont( void )
{
	glColor4ub( vertexSetColor.getUbR(), vertexSetColor.getUbG(), vertexSetColor.getUbB(), vertexSetColor.getUbA() );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	// Rotaciona o objeto
	Matrix4x4 aux;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &aux ) ) );

	NanoRect area( position.x, position.y, size.x, size.y );
	
	// OLD
//	pCurrFont->renderText( hCurrText, charSpacement, &area, alignment );
	
	pCurrFont->renderText( pCurrText, charSpacement, &area, alignment );
	
	glPopMatrix();
	
	glColor4ub( 255, 255, 255, 255 );
}

