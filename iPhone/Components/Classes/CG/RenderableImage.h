/*
 *  RenderableImage.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/10/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef RENDERABLE_IMAGE_H
#define RENDERABLE_IMAGE_H 1

// Components
#include "MirrorOp.h"
#include "Object.h"
#include "ResourceManager.h"
#include "Texture2D.h"
#include "VertexSet.h"

// OpenGL
#include "GLHeaders.h"

class RenderableImage : public Object
{
	public:
		// Construtores
		RenderableImage( void );
		//RenderableImage( const RenderableImage& other );
		RenderableImage( const RenderableImage* pOther );
		RenderableImage( const char* pImagePath, CreateVertexSetFunction pVertexesCreator = &( RenderableImage::CreateDefaultVertexSet ), LoadTextureFunction pLoadTextureFunction = &( ResourceManager::GetTexture2D ) );
	
		// Destrutor
		virtual ~RenderableImage( void );
	
		// Renderiza o objeto
		virtual bool render( void );

		// Determina o fator de repetição da imagem
		inline void setPatternFactor( Point3f* pPatternFactor ) { setPatternFactor( pPatternFactor->x, pPatternFactor->y, pPatternFactor->z ); };
		void setPatternFactor( float x = 1.0f, float y = 1.0f, float z = 1.0f );
	
		// Retorna o fator de repetição da imagem
		inline const Point3f* getPatternFactor( void ) const { return &patternFactor; };
	
		// Determina as operações de espelhamento que devem ser aplicadas sobre o objeto
		void mirror( MirrorOp ops );
	
		// Indica se uma determinada operação de espelhamento está aplicada sobre o objeto
		bool isMirrored( MirrorOp ops );
	
		// Cancela operações de espelhamento aplicadas ao objeto
		void unmirror( MirrorOp ops );
	
		// Determina um frame da imagem para ser utilizado na texturização do objeto. Por default, esse frame é a imagem inteira
		void setImageFrame( const TextureFrame& frame );
	
		// Determina a imagem a ser utilizada pelo objeto
		void setImage( Texture2DHandler pImage );
	
		// Determina a cor do conjunto de vértices a ser utilizada na renderização
		void setVertexSetColor( const Color& color );
	
		// Retorna a cor do conjunto de vértices utilizado na renderização
		Color* getVertexSetColor( Color* pColor ) const;

		// Cria o conjunto de vértices que será utilizado para renderizar o objeto
		// Exceções: InvalidArgumentException
		static VertexSetHandler CreateDefaultVertexSet( void );
	
		// TODOO: Pegar o valor padrão de acordo com a forma de carregar a imagem utilizada do disco!!! Ou seja, criar uma nova informação em Texture2D
		// Determina os fatores de blending que determinam como as componentes R, G, B e A são calculadas.
		// Os valores aceitos para srcFactor são:
		// - GL_ZERO
		// - GL_ONE
		// - GL_DST_COLOR
		// - GL_ONE_MINUS_DST_COLOR
		// - GL_SRC_ALPHA
		// - GL_ONE_MINUS_SRC_ALPHA
		// - GL_DST_ALPHA
		// - GL_ONE_MINUS_DST_ALPHA
		// - GL_SRC_ALPHA_SATURATE
		// O valor padrão é GL_ONE.
		//
		// Os valores aceitos para dstFactor são:
		// - GL_ZERO
		// - GL_ONE
		// - GL_SRC_COLOR
		// - GL_ONE_MINUS_SRC_COLOR
		// - GL_SRC_ALPHA
		// - GL_ONE_MINUS_SRC_ALPHA
		// - GL_DST_ALPHA
		// - GL_ONE_MINUS_DST_ALPHA
		// O valor padrão é GL_ONE_MINUS_SRC_ALPHA.
		void setBlenFunc( GLenum srcFactor, GLenum dstFactor );

	protected:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( const Texture2DHandler& image, const VertexSetHandler& vertexes );

		// Libera os recursos alocados pelo objeto
		//void clean( void );
	
		// Operações de espelhamento aplicadas ao objeto
		uint8 mirrorOps;
	
		// Fatores que indicam como as componentes R, G, B e A são calculadas no blending. Ver glBlendFunc
		GLenum texBlendSrcFactor, texBlendDstFactor;

		// Modo de texturização. Ver glTexEnv
		GLenum texEnvMode;

		// Vértices do objeto
		VertexSetHandler pVertexSet;
	
		// Textura contendo a imagem do objeto
		Texture2DHandler pImg;
	
		// Fator de repetição da imagem do objeto
		Point3f patternFactor;
	
		// Cor do conjunto de vértices a ser utilizada na renderização
		Color vertexSetColor;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

inline Color* RenderableImage::getVertexSetColor( Color* pColor ) const
{
	*pColor = vertexSetColor;
	return pColor;
}

inline void RenderableImage::setBlenFunc( GLenum srcFactor, GLenum dstFactor )
{
	texBlendSrcFactor = srcFactor;
	texBlendDstFactor = dstFactor;
}

#endif
