#include "OptimizedTexPattern.h"

// Components
#include "Camera.h"
#include "Exceptions.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"
#include "Quad.h"
#include "Scene.h"
#include "TextureFrame.h"

// TODOO
#warning OptimizedTexPattern TODOS
#warning Esta classe deve derivar de RenderableImage!!!!!!!!!!!

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

OptimizedTexPattern::OptimizedTexPattern( const char* pImg, LoadTextureFunction pLoadTextureFunction )
					: Object(), texBlendSrcFactor( GL_ONE ), texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ), texEnvMode( GL_MODULATE ),
					  vertexSetColor( COLOR_WHITE ), vertexes()

{
	build( pLoadTextureFunction( pImg, NULL ) );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

OptimizedTexPattern::OptimizedTexPattern( const OptimizedTexPattern* pOther )
					: Object(), texBlendSrcFactor( GL_ONE ), texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ), texEnvMode( GL_MODULATE ),
					  vertexSetColor( COLOR_WHITE ), vertexes()
{
	build( pOther->pTex );
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

void OptimizedTexPattern::build( Texture2DHandler pImg )
{
	pTex = pImg;
	if( !pTex )
#if DEBUG
		throw ConstructorException( "OptimizedTexPattern::build( Texture2DHandler ): No texture" );
#else
		throw ConstructorException();
#endif
	
	float texCoords[ QUAD_N_VERTEXES * 2 ];
	vertexes.mapTexCoords( pTex->getFrameTexCoords( 0, texCoords ) );

	// Queremos obrigar o usuário a chamar setSize. Caso ele não o faça, nada aparecerá na tela
	Object::setSize( 0.0f, 0.0f, 1.0f );
}

/*==============================================================================================

MÉTODO setVertexSetColor
	Determina a cor do conjunto de vértices a ser utilizada na renderização.

==============================================================================================*/

void OptimizedTexPattern::setVertexSetColor( const Color& color )
{
	vertexSetColor = color;
}

/*==============================================================================================

MÉTODO setVertexSetColor
	Determina o tamanho do objeto.
 	Mapeamento de textura OpenGL:
 
	(  S  ,  T   )   (  S  ,  T   )
	( 0.0f, 1.0f ) - ( 1.0f, 1.0f )
	-----------------------------
	|							|
	|							|
	|							|
	|							|
	-----------------------------
	( 0.0f, 0.0f ) - ( 1.0f, 0.0f )

==============================================================================================*/

void OptimizedTexPattern::setSize( float width, float height, float depth )
{	
	// Faz o cast
	float textWidth = pTex->getWidth();
	float textHeight = pTex->getHeight();

	// Preenche as coordenadas de textura
	float texCoords[ QUAD_N_VERTEXES * 2 ];
	
	// Inferior esquerda sempre será s = 0.0f, t = 0.0f
	texCoords[ 0 ] = 0.0f;
	texCoords[ 1 ] = 0.0f;
	
	// Superior esquerda sempre será s = 0.0f
	texCoords[ 2 ] = 0.0f;
	texCoords[ 3 ] = height / textHeight;

	// Inferior direita sempre será t = 0.0f
	texCoords[ 4 ] = width / textWidth;
	texCoords[ 5 ] = 0.0f;
	
	// Superior direita
	texCoords[ 6 ] = texCoords[ 4 ];
	texCoords[ 7 ] = texCoords[ 3 ];
	
	// Mapeia as novas coordenadas de textura
	vertexes.mapTexCoords( texCoords );
	
	// Indica para o objeto seu novo tamanho
	Object::setSize( width, height, depth );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool OptimizedTexPattern::render( void )
{
	if( !Object::render() )
		return false;
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	// OBS : Isso REALMENTE funciona...
	//	An optimum compromise that allows all primitives to be specified at integer
	//positions, while still ensuring predictable rasterization, is to translate x
	//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
	// away from the centers of pixels, while moving line vertices close enough to
	// the pixel centers
	
	// Posiciona o objeto
	float halfWidth = getWidth() * 0.5f;
	float halfHeight = getHeight() * 0.5f;
	glTranslatef( position.x + halfWidth + 0.375f, position.y + halfHeight + 0.375f, position.z );
	
	// TODOO : Provisório!!!! Temos que deixar definir um centro de rotação!!!!!!!
	glTranslatef( -halfWidth, -halfHeight, position.z );
	
	Matrix4x4 aux;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &aux ) ) );
	
	glTranslatef( halfWidth, halfHeight, position.z );

	// Redimensiona o objeto
	glScalef( size.x * scale.x, size.y * scale.y, size.z * scale.z );

	// Ativa o blend
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( texBlendSrcFactor, texBlendDstFactor );
	
	// Carrega a textura
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pTex->load();
	
	// Determina o modo de texturização
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, texEnvMode );
	
	if( NanoMath::fdif( scale.x, 1.0f ) || NanoMath::fdif( scale.y, 1.0f ) )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	}
	else
	{	
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	}
	
	// Ativa o "pattern"
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );

	// Renderiza o objeto
	if( !( vertexes.getRenderStates() & RENDER_STATE_COLOR_ARRAY ) )
		glColor4ub( vertexSetColor.getUbR(), vertexSetColor.getUbG(), vertexSetColor.getUbB(), vertexSetColor.getUbA() );
	
	vertexes.render();
	
	if( !( vertexes.getRenderStates() & RENDER_STATE_COLOR_ARRAY ) )
		glColor4ub( 255, 255, 255, 255 );

	// Reseta os estados do OpenGL
	pTex->unload();

	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );
	
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

	if( customViewport )
		glDisable( GL_SCISSOR_TEST );

	return true;
}


