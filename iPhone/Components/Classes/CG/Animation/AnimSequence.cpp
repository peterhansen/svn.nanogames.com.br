#include "AnimSequence.h"

// Components
#include "Macros.h"

// C++
#include <cstring>

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AnimSequence::AnimSequence( void ) : nSteps( 0 ), pDurations( NULL ), pIndexes( NULL )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AnimSequence::AnimSequence( const AnimSequence& ref ) : nSteps( ref.nSteps ), pDurations( new float[ ref.nSteps ] ), pIndexes( new uint16[ ref.nSteps ] )
{
	// Copia os dados
	for( uint16 i = 0 ; i < nSteps ; ++i )
	{
		pDurations[i] = ref.pDurations[i];
		pIndexes[i] = ref.pIndexes[i];
	}
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

AnimSequence::~AnimSequence( void )
{
	clean();
}

/*==============================================================================================

MÉTODO set
	Inicializa a sequência de animação do sprite.

==============================================================================================*/

bool AnimSequence::set( uint16 nSteps, const float* pFramesDurations, const uint16* pFramesIndexes )
{
	// Limpa os possíveis dados já existentes
	clean();
	
	this->nSteps = nSteps;
	
	pDurations = new float[ nSteps ];
	if( !pDurations )
		return false;
	
	memcpy( pDurations, pFramesDurations, sizeof( float ) * nSteps );
	
	pIndexes = new uint16[ nSteps ];
	if( !pIndexes )
		return false;
	
	memcpy( pIndexes, pFramesIndexes, sizeof( uint16 ) * nSteps );
	
	return true;
}

/*==============================================================================================

MÉTODO set
	Inicializa a sequência de animação do sprite.

==============================================================================================*/

bool AnimSequence::set( uint16 nSteps, float framesDurations, const uint16* pFramesIndexes )
{
	// Limpa os possíveis dados já existentes
	clean();
	
	this->nSteps = nSteps;
	
	pDurations = new float[ nSteps ];
	if( !pDurations )
		return false;

	for( uint16 i = 0 ; i < nSteps ; ++i )
		pDurations[i] = framesDurations;
	
	pIndexes = new uint16[ nSteps ];
	if( !pIndexes )
		return false;
	
	memcpy( pIndexes, pFramesIndexes, sizeof( uint16 ) * nSteps );
	
	return true;
}

/*==============================================================================================

MÉTODO clean
	Deleta os arrays do objeto.

==============================================================================================*/

void AnimSequence::clean( void )
{
	DELETE_VEC( pDurations );
	DELETE_VEC( pIndexes );
}

/*==============================================================================================

OPERADOR =

==============================================================================================*/

AnimSequence& AnimSequence::operator=( const AnimSequence& ref )
{
	// Evita auto atribuição
	if( this == &ref )
		return *this;
	
	// Evita vazamento de memória
	clean();
	
	// Copia o número de etapas de animação
	nSteps = ref.nSteps;

	// Cria os arrays contendo as informações sobre as etapas da animação
	pDurations = new float[ nSteps ];
	pIndexes = new uint16[ nSteps ];
	
	// Copia os dados
	for( uint16 i = 0 ; i < nSteps ; ++i )
	{
		pDurations[i] = ref.pDurations[i];
		pIndexes[i] = ref.pIndexes[i];
	}

	return *this;
}

