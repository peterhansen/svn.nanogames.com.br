#include "Quad.h"

#define DEFAULT_SIZE 1.0f
#define HALF_SIZE ( DEFAULT_SIZE * 0.5f )

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Quad::Quad( void ) : VertexSet()
{
	quadVertexes[0].set( -HALF_SIZE, -HALF_SIZE, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f );
	quadVertexes[1].set( -HALF_SIZE,  HALF_SIZE, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f );
	quadVertexes[2].set(  HALF_SIZE, -HALF_SIZE, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f );
	quadVertexes[3].set(  HALF_SIZE,  HALF_SIZE, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Quad::Quad( const Vertex* pVertexes ) : VertexSet()
{
	quadVertexes[0] = pVertexes[0];
	quadVertexes[1] = pVertexes[1];
	quadVertexes[2] = pVertexes[2];
	quadVertexes[3] = pVertexes[3];
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Quad::Quad( const Vertex* pV1, const Vertex* pV2, const Vertex* pV3, const Vertex* pV4 ) : VertexSet()
{
	quadVertexes[0] = *pV1;
	quadVertexes[1] = *pV2;
	quadVertexes[2] = *pV3;
	quadVertexes[3] = *pV4;
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Quad::Quad( const Vertex& pV1, const Vertex& pV2, const Vertex& pV3, const Vertex& pV4 ) : VertexSet()
{
	quadVertexes[0] = pV1;
	quadVertexes[1] = pV2;
	quadVertexes[2] = pV3;
	quadVertexes[3] = pV4;
}

/*===========================================================================================
 
MÉTODO mapTexCoords
	Mapeia as coordenadas de textura nos vértices do objeto. O parâmetro pTexCoords deve
apontar para um array de 8 floats, representando 4 pares (s,t) que definem uma área quadrada
na textura ativa. Os pares devem seguir a ordem: bottom-left, top-left, bottom-right e top-right
 
	OBS : Se utilizarmos as coordenadas

			0.0f, 0.0f
			0.0f, 1.0f
			1.0f, 0.0f
			1.0f, 1.0f

	A imagem ficará espelhada verticalmente. Este é um "feature" normal do OpenGL. Logo,
utilizamos as coordenadas de textura já invertidas no eixo y.

============================================================================================*/

void Quad::mapTexCoords( const float* pTexCoords )
{
	quadVertexes[0].setTexCoords( pTexCoords[0], pTexCoords[1] );
	quadVertexes[1].setTexCoords( pTexCoords[2], pTexCoords[3] );
	quadVertexes[2].setTexCoords( pTexCoords[4], pTexCoords[5] );
	quadVertexes[3].setTexCoords( pTexCoords[6], pTexCoords[7] );
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/
	
//bool Quad::render( void )
//{	
//	if( !VertexSet::render() || (( renderStates & RENDER_STATE_VERTEX_ARRAY ) == 0 ))
//		return false;
//	
//	float* pAux = ( float* )quadVertexes;
//	
//	glEnableClientState( GL_VERTEX_ARRAY );
//	glVertexPointer( 3, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_POSITION_START ] );
//	
//	if( ( renderStates & RENDER_STATE_TEXCOORD_ARRAY ) != 0 )
//	{
//		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
//		glTexCoordPointer( 2, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_TEXCOORD_START ] );
//	}
//	else
//	{
//		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
//	}
//	
//	if( ( renderStates & RENDER_STATE_COLOR_ARRAY ) != 0 )
//	{
//		glEnableClientState( GL_COLOR_ARRAY );
//		glColorPointer( 4, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_COLOR_START ] );
//	}
//	else
//	{
//		glDisableClientState( GL_COLOR_ARRAY );
//	}
//	
//	if( ( renderStates & RENDER_STATE_NORMAL_ARRAY ) != 0 )
//	{
//		glEnableClientState( GL_NORMAL_ARRAY );
//		glNormalPointer( GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_NORMAL_START ] );
//	}
//	else
//	{
//		glDisableClientState( GL_NORMAL_ARRAY );
//	}
//
//	glDrawArrays( GL_TRIANGLE_STRIP, 0, QUAD_N_VERTEXES );
//
//	return true;
//}


