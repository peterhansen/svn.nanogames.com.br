#include "Sphere.h"

#include "MathFuncs.h"
#include "Exceptions.h"

#include <math.h>

// Radius deve ser sempre 0.5f (diâmetro de 1.0f) na hora de gerar a mesh. glScalef() cuidará
// de gerar esferas maiores
#define DEFAULT_RADIUS 0.5f

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Sphere::Sphere( int32 lod )
       : VertexSet(), lod( lod ), nSphereVertices( 0 ), pSphereVertices( NULL )
{
	// Inicializa o array de vértices
	build();
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Sphere::~Sphere( void )
{
	DELETE_VEC( pSphereVertices );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

//bool Sphere::render( void )
//{
//	if( !VertexSet::render() || (( renderStates & RENDER_STATE_VERTEX_ARRAY ) == 0 ))
//		return false;
//	
//	float* pAux = ( float* )getVertexes();
//	
//	glEnableClientState( GL_VERTEX_ARRAY );
//	glVertexPointer( 3, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_POSITION_START ] );
//	
//	if( ( renderStates & RENDER_STATE_TEXCOORD_ARRAY ) != 0 )
//	{
//		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
//		glTexCoordPointer( 2, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_TEXCOORD_START ] );
//	}
//	else
//	{
//		glDisableClientState( GL_TEXTURE_COORD_ARRAY );
//	}
//	
//	if( ( renderStates & RENDER_STATE_COLOR_ARRAY ) != 0 )
//	{
//		glEnableClientState( GL_COLOR_ARRAY );
//		glColorPointer( 4, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_COLOR_START ] );
//	}
//	else
//	{
//		glDisableClientState( GL_COLOR_ARRAY );
//	}
//	
//	if( ( renderStates & RENDER_STATE_NORMAL_ARRAY ) != 0 )
//	{
//		glEnableClientState( GL_NORMAL_ARRAY );
//		glNormalPointer( GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_NORMAL_START ] );
//	}
//	else
//	{
//		glDisableClientState( GL_NORMAL_ARRAY );
//	}
//
//	glDrawArrays( GL_TRIANGLE_STRIP, 0, getNVertexes() );
//
//	return true;
//}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.
	Baseado na função escrita por Paul Bourke. Referência:
		- http://astronomy.swin.edu.au/~pbourke/opengl/sphere/

==============================================================================================*/

void Sphere::build( void )
{
	// Radius deve ser sempre 0.5f (diâmetro de 1.0f) na hora de gerar a mesh. glScalef() cuidará
	// de gerar esferas maiores
    // Não permite raios negativos
    //if( radius < 0 )
	//	radius = -radius;

    // Não permite um nível de precisão menor que 4
    if( lod < 4 ) 
        lod = 4;
	
	// Calcula o número de vértices necessários
	// OBS: nSphereVertices = ( lod / 2 ) * ( ( lod + 1 ) * 2 )  =  lodˆ2 + lod
	nSphereVertices = ( lod * lod ) + lod;
	
	// Cria o vetor de vértices
	// OBS: Se não conseguir alocar a memória, irá disparar uma exceção
	pSphereVertices = new Vertex[ nSphereVertices ];
	if( !pSphereVertices )
#if DEBUG
		throw ConstructorException( "Sphere::build( void ): Unable to create object" );
#else
		throw ConstructorException();
#endif

    int32 k = 0;

	const int32 halfLOD = lod >> 1;
	const float TWO_PI_DIV_LOD = TWO_PI / lod, floatCastedLOD = static_cast< float >( lod );

	float ex, ez, py0, py1, texU, texV0, texV1;
	float theta1, theta2, theta3, sinTheta1, cosTheta1, sinTheta2, cosTheta2, sinTheta3, cosTheta3;
	
    for( int32 i = 0; i < halfLOD ; ++i )
    {
        theta1 = static_cast< float >( ( i * TWO_PI_DIV_LOD ) - M_PI_2 );
        theta2 = static_cast< float >( (( i + 1 ) * TWO_PI_DIV_LOD ) - M_PI_2 );
		
		sinTheta1 = sinf( theta1 );
		cosTheta1 = cosf( theta1 );
		sinTheta2 = sinf( theta2 );
		cosTheta2 = cosf( theta2 );

		py0 = DEFAULT_RADIUS * sinTheta2;
		py1 = DEFAULT_RADIUS * sinTheta1;
		
		texV0 = ( ( i+1 ) << 1 ) / floatCastedLOD;
		texV1 = ( i << 1 ) / floatCastedLOD;

		for( int32 j = 0; j <= lod ; ++j )
		{
			theta3 = j * TWO_PI_DIV_LOD;

			sinTheta3 = sinf( theta3 );
			cosTheta3 = cosf( theta3 );

			ex = cosTheta2 * cosTheta3;
			ez = cosTheta2 * sinTheta3;
			texU = -( j / floatCastedLOD );
			pSphereVertices[ k ].setTexCoords( texU, texV0 );
			pSphereVertices[ k ].setNormal( ex, sinTheta2, ez );
			pSphereVertices[ k ].setPosition( DEFAULT_RADIUS * ex, py0, DEFAULT_RADIUS * ez );
			++k;

			ex = cosTheta1 * cosTheta3;
			ez = cosTheta1 * sinTheta3;
			
			pSphereVertices[ k ].setTexCoords( texU, texV1 );
			pSphereVertices[ k ].setNormal( ex, sinTheta1, ez );
			pSphereVertices[ k ].setPosition( DEFAULT_RADIUS * ex, py1, DEFAULT_RADIUS * ez );
			++k;
		}
    }
}

/*===========================================================================================
 
MÉTODO mapTexCoords
	Mapeia as coordenadas de textura nos vértices do objeto. O parâmetro pTexCoords deve
apontar para um array de getNVertexes() * 2 floats, representando os pares (s,t) que definem
as coordenadas de textura a serem utilizadas.
 
============================================================================================*/

void Sphere::mapTexCoords( const float* pTexCoords )
{
#if DEBUG
	LOG( "Sphere::mapTexCoords( const float* ): Method not implemented" );
#endif

	// Por enquanto, não permitimos mudança nas coordenadas de textura da esfera
	// TASK
}
