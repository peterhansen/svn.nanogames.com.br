/*
 *  TextureFrameDescriptor.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/11/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEXTURE_FRAMES_DESCRIPTOR_H
#define TEXTURE_FRAMES_DESCRIPTOR_H 1

#include "TextureFrame.h"

class TextureFrameDescriptor
{
	public:
		// Construtor
		TextureFrameDescriptor( void );
	
		// Destrutor
		virtual ~TextureFrameDescriptor( void );
	
		// Obtém as informações da textura através da leitura de um arquivo descritor
		bool readFromFile( const char* pFileName );

		// Obtém as informações da textura através do parser de arquivos
		virtual bool readFromScanner( const NSScanner* hScanner );
	
		// Getters e Setters para os atributos da classe
		void setOriginalFrameSize( uint32 width, uint32 height );
		
		inline uint32 getOriginalFrameWidth( void ) const { return originalFrameWidth; };
		inline uint32 getOriginalFrameHeight( void ) const { return originalFrameHeight; };
		
		bool setFrames( uint16 n, const TextureFrame* pTexFrames );
	
		void changeFrame( uint16 frameIndex, const TextureFrame* pTexFrame );
	
		inline uint32 getNFrames( void ) const { return nFrames; };
		inline const TextureFrame* getTextureFrames( void ) const { return pFrames; };
	
		// Retorna o frame com o índice passado como parâmetro
		inline const TextureFrame* getFrame( uint16 frameIndex ) const { return &( getTextureFrames()[ frameIndex ] ); };
	
		// Não utiliza operador de atribuição para não termos que disparar uma exceção no
		// meio do código
		bool copy( const TextureFrameDescriptor& desc );
	
	protected:
		// Tamanho original dos frames da textura
		int32 originalFrameWidth, originalFrameHeight;

		// Número de frames contidos na textura
		uint16 nFrames;

		// Offsets dos frames em relação ao seu tamanho original
		TextureFrame* pFrames;
};

#endif
