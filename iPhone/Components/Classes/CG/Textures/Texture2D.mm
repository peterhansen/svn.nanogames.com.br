#include "Texture2D.h"

// Components
#include "Bitmap.h"
#include "Bitwise.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"

// OpenGL
#include "GLHeaders.h"

// TODOO
#warning Vai dar problema se chamarmos loadTextureWithData, loadTextureInFile, loadTextureEmpty ou loadPVRCTexInFile para uma textura que já tenha sido carregada!!! O certo é verificar se texID != 0. Se for, temos que chamar glDeleteTextures

// Estruturas e definições necessárias para carregarmos texturas PVRCT:
#define PVR_TEXTURE_FLAG_TYPE_MASK	0xff

enum
{
	kPVRTextureFlagTypePVRCT_2 = 24,
	kPVRTextureFlagTypePVRCT_4
};

typedef struct PVRTexHeader
{
	uint32 headerLength;	// Size of this structure
	uint32 height;			// Height of surface to be created
	uint32 width;			// Width of input surface
	uint32 numMipmaps;		// Number of mip-map levels
	uint32 flags;			// Pixel format flags
	uint32 dataLength;		// Total size in bytes
	uint32 bpp;				// Number of bits per pixel
	uint32 bitmaskRed;		// Mask for red bits
	uint32 bitmaskGreen;	// Mask for green bits
	uint32 bitmaskBlue;		// Mask for blue bits
	uint32 bitmaskAlpha;	// Mask for alpha bits
	uint32 pvrTag;			// Number identifying pvr file
	uint32 numSurfs;		// Number of surfaces present in the pvr
	
} PVRTexHeader;

/*==============================================================================================

INICIALIZAÇÃO DE VARIÁVEIS ESTÁTICAS

==============================================================================================*/

const uint8 Texture2D::lerps_0_15[] =
{
	//	0	1	2	3	4	5	6	7	8	9
/* 0 */	0,	0,	0,	0,	1,	1,	1,	1,	1,	1,
/* 1 */	1,	2,	2,	2,	2,	2,	2,	3,	3,	3,
/* 2 */	3,	3,	3,	3,	4,	4,	4,	4,	4,	4,
/* 3 */	4,	5,	5,	5,	5,	5,	5,	6,	6,	6,
/* 4 */	6,	6,	6,	6,	7,	7,	7,	7,	7,	7,
/* 5 */	7,	8,	8,	8,	8,	8,	8,	9,	9,	9,
/* 6 */	9,  9,  9,  9, 10, 10, 10, 10, 10, 10,
/* 7 */ 10, 11, 11, 11, 11, 11, 11, 12, 12, 12,
/* 8 */ 12, 12, 12, 12, 13, 13, 13, 13, 13, 13,
/* 9 */ 13, 14, 14, 14, 14, 14, 14, 15, 15, 15,
/*10 */ 15
};

const uint8 Texture2D::lerps_0_31[] =
{
	//	0	1	2	3	4	5	6	7	8	9
/* 0 */ 0,  0,  1,  1,  1,  2,  2,  2,  2,  3,
/* 1 */ 3,  3,  4,  4,  4,  5,  5,  5,  6,  6,
/* 2 */ 6,  7,  7,  7,  7,  8,  8,  8,  9,  9,
/* 3 */ 9, 10, 10, 10, 11, 11, 11, 11, 12, 12,
/* 4 */12, 13, 13, 13, 14, 14, 14, 15, 15, 15,
/* 5 */16, 16, 16, 16, 17, 17, 17, 18, 18, 18,
/* 6 */19, 19, 19, 20, 20, 20, 20, 21, 21, 21,
/* 7 */22, 22, 22, 23, 23, 23, 24, 24, 24, 24,
/* 8 */25, 25, 25, 26, 26, 26, 27, 27, 27, 28,
/* 9 */28, 28, 29, 29, 29, 29, 30, 30, 30, 31,
/*10 */31
};

const uint8 Texture2D::lerps_0_63[] =
{
	//	0	1	2	3	4	5	6	7	8	9
/* 0 */	0,	1,	1,	2,	3,	3,	4,	4,	5,	6,
/* 1 */	6,	7,	8,	8,	9,	9, 10, 11, 11, 12,
/* 2 */13, 13, 14, 14, 15, 16, 16, 17, 18, 18,
/* 3 */19, 20, 20, 21, 21, 22, 23, 23, 24, 25,
/* 4 */25, 26, 26, 27, 28, 28, 29, 30, 30, 31,
/* 5 */32, 32, 33, 33, 34, 35, 35, 36, 37, 37,
/* 6 */38, 38, 39, 40, 40, 41, 42, 42, 43, 43,
/* 7 */44, 45, 45, 46, 47, 47, 48, 49, 49, 50,
/* 8 */50, 51, 52, 52, 53, 54, 54, 55, 55, 56,
/* 9 */57, 57, 58, 59, 59, 60, 60, 61, 62, 62,
/*10 */63
};

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Texture2D::Texture2D( LoadableListener *pListener, int32 loadableId )
		  : Texture( NULL, loadableId ), width( 0 ), height( 0 ), pUserListener( pListener ), framesInfo()
{
	setListener( this );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Texture2D::~Texture2D( void )
{
}

/*==============================================================================================

MÉTODO loadTextureInFile
	Carrega a textura bitmap de um arquivo.

==============================================================================================*/

bool Texture2D::loadTextureInFile( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	char buffer[ PATH_MAX ];
	NSString* hFilePath = CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, pFileName, "png" ) );
	return loadTexture( [[UIImage imageWithContentsOfFile: hFilePath] CGImage], pTexDesc );
}

/*==============================================================================================

 MÉTODO loadTextureWithData
	Carrega a textura a partir dos dados recebidos como parâmetro.
 
==============================================================================================*/

bool Texture2D::loadTextureWithData( const NSData* pData, const TextureFrameDescriptor* pTexDesc )
{
	return loadTexture( [[UIImage imageWithData: pData ] CGImage], pTexDesc );
}

/*==============================================================================================

 MÉTODO loadTextureEmpty
	Cria uma textura vazia com as dimensões desejadas.
 
==============================================================================================*/

bool Texture2D::loadTextureEmpty( uint32 texWidth, uint32 texHeight )
{
	if( !Bitwise::isPowOf2( texWidth ) || !Bitwise::isPowOf2( texHeight ) )
		return false;
	
	// Limpa o código de erro
	glGetError();

	// Cria a textura
	glBindTexture( GL_TEXTURE_2D, texId );	
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, texWidth, texHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL );	
	
	// Verifica se conseguiu criar a textura
	if( glGetError() != GL_NO_ERROR )
		return false;

	// OLD : Essas operações passaram a ser feitas em cada objeto que utiliza a textura
//	// Configura a textura
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
//	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	
	width = texWidth;
	height = texHeight;
	
	// Desfaz o vínculo com a textura criada
	glBindTexture( GL_TEXTURE_2D, GL_NONE );
	
	return true;
}

/*==============================================================================================

 MÉTODO getTextureData
	Retorna o array de cores da textura.
 
==============================================================================================*/

const uint8* Texture2D::getTextureData( void ) const
{
#warning const uint8* Texture2D::getTextureData( void ) const: Método não implementado !!!!
	return NULL;
}

/*==============================================================================================

 MÉTODO getFrameTexCoords
	Retorna os 4 pares (s,t) de coordenadas de textura correspondentes ao frame.
 
==============================================================================================*/

float* Texture2D::getFrameTexCoords( uint16 frame, float* pTexCoords )
{
	const TextureFrame* pFrames = framesInfo.getTextureFrames();

	// Inferior esquerda
	pTexCoords[ 0 ] = static_cast< float >( pFrames[ frame ].x ) / static_cast< float >( width );
	pTexCoords[ 1 ] = static_cast< float >( pFrames[ frame ].y ) / static_cast< float >( height );
	
	// Superior esquerda
	pTexCoords[ 2 ] = pTexCoords[ 0 ];
	pTexCoords[ 3 ] = static_cast< float >( pFrames[ frame ].y + pFrames[ frame ].height ) / static_cast< float >( height );

	// Inferior direita
	pTexCoords[ 4 ] = static_cast< float >( pFrames[ frame ].x + pFrames[ frame ].width ) / static_cast< float >( width );
	pTexCoords[ 5 ] = pTexCoords[ 1 ];

	// Superior direita
	pTexCoords[ 6 ] = pTexCoords[ 4 ];
	pTexCoords[ 7 ] = pTexCoords[ 3 ];
	
	return pTexCoords;
}

/*==============================================================================================

 MÉTODO loadPVRCTexInFile
	Carrega a textura comprimida contida no arquivo passado como parâmetro.
 
==============================================================================================*/

bool Texture2D::loadPVRCTexInFile( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	char buffer[ PATH_MAX ];
	NSString* hFilePath = CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, pFileName, "pvr" ) );
	return loadPVRCTexture( reinterpret_cast< const uint8* >( [[NSData dataWithContentsOfFile: hFilePath] bytes] ), pTexDesc );
}

/*==============================================================================================

 MÉTODO loadPVRCTexture
	Carrega a textura comprimida a partir do array de bytes.
 
==============================================================================================*/

bool Texture2D::loadPVRCTexture( const uint8* pData, const TextureFrameDescriptor* pTexDesc )
{	
	const PVRTexHeader* pHeader = reinterpret_cast< const PVRTexHeader* >( pData );
	
	// Verifica se o arquivo é realmente uma imagem PVRCT
	uint32 pvrTag = CFSwapInt32LittleToHost( pHeader->pvrTag );

	unsigned char PVRTexIdentifier[4] = { 'P', 'V', 'R', '!' };
	if( PVRTexIdentifier[0] != (( pvrTag >>  0 ) & 0xff ) ||
		PVRTexIdentifier[1] != (( pvrTag >>  8 ) & 0xff ) ||
		PVRTexIdentifier[2] != (( pvrTag >> 16 ) & 0xff ) ||
		PVRTexIdentifier[3] != (( pvrTag >> 24 ) & 0xff ))
	{
		return false;
	}
	
	uint32 flags = CFSwapInt32LittleToHost( pHeader->flags );
	uint32 formatFlags = flags & PVR_TEXTURE_FLAG_TYPE_MASK;
	
	// Verifica se o arquivo está em um dos formatos que sabemos tratar
	if(( formatFlags != kPVRTextureFlagTypePVRCT_4 ) && ( formatFlags != kPVRTextureFlagTypePVRCT_2 ))
		return false;
	
	// Verifica se a imagem é quadrada (limitação imposta pelo hardware do iPhone para texturas PVRCT)
	width = CFSwapInt32LittleToHost( pHeader->width );
	height = CFSwapInt32LittleToHost( pHeader->height );
	
	if( width != height )
		return false;
	
	// Verifica se as dimensões da textura são potências de 2 (limitação imposta pela versão do OpenGLes e pelo hardware)
	if( !Bitwise::isPowOf2( width ) || !Bitwise::isPowOf2( height ) )
		return false;
	
	// Armazena os dados dos frames contidos na textura
	if( pTexDesc )
	{
		if( !framesInfo.copy( *pTexDesc ) )
			return false;
	}
	else
	{
		TextureFrame frame;
		frame.set( 0, 0, width, height, 0, 0 );
		if( !framesInfo.setFrames( 1, &frame ) )
			return false;

		framesInfo.setOriginalFrameSize( width, height );
	}
	
	// Determina o formato da textura para o OpenGL
	GLenum format;
	uint32 bpp, blockSize;
	bool hasAlpha = CFSwapInt32LittleToHost( pHeader->bitmaskAlpha ) != 0;

	if( formatFlags == kPVRTextureFlagTypePVRCT_4 )
	{
		blockSize = 4 * 4; // Pixel by pixel block size for 4bpp
		bpp = 4;
		
		format = hasAlpha ? GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG : GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
	}
	else // if( formatFlags == kPVRTextureFlagTypePVRCT_2 )
	{
		blockSize = 8 * 4; // Pixel by pixel block size for 2bpp
		bpp = 2;
		
		format = hasAlpha ? GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG : GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
	}
	
	// Especifica qual imagem utilizaremos para esta textura 2D
	glBindTexture( GL_TEXTURE_2D, texId );

	// Calculate the data size for each texture level and respect the minimum number of blocks
	uint32 dataOffset = 0;
	uint32 dataLength = CFSwapInt32LittleToHost( pHeader->dataLength );
	int32 auxWidth = width, auxHeight = height;
	const uint8* pPVRCTDatabytes = ( reinterpret_cast< const uint8* >( pHeader ) + sizeof( PVRTexHeader ) );
	
	for( uint8 level = 0 ; dataOffset < dataLength ; ++level )
	{
		uint32 widthBlocks = formatFlags == kPVRTextureFlagTypePVRCT_4 ? auxWidth >> 2 : auxWidth >> 3;
		uint32 heightBlocks = auxHeight >> 2;
		
		// Clamp to minimum number of blocks
		if( widthBlocks < 2 )
			widthBlocks = 2;

		if( heightBlocks < 2 )
			heightBlocks = 2;

		uint32 dataSize = widthBlocks * heightBlocks * ( ( blockSize  * bpp ) >> 3 );
		glCompressedTexImage2D( GL_TEXTURE_2D, level, format, auxWidth, auxHeight, 0, dataSize, pPVRCTDatabytes + dataOffset );
		
		dataOffset += dataSize;
		
		auxWidth = MAX( auxWidth >> 1, 1 );
		auxHeight = MAX( auxHeight >> 1, 1 );
	}

	// Desvincula a textura
	glBindTexture( GL_TEXTURE_2D, GL_NONE );

	return true;
}

/*==============================================================================================

 MÉTODO loadTexture
	Cria uma textura utilizando a imagem recebida.
 
==============================================================================================*/

bool Texture2D::loadTexture( CGImageRef pImage, const TextureFrameDescriptor* pTexDesc )
{
#if DEBUG
	if( !pImage )
		return false;
#endif

	uint8* pTexData = NULL;
	
	{ // Evita problemas com o goto

		// Verifica se as dimensões da textura são potências de 2
		width = CGImageGetWidth( pImage );
		height = CGImageGetHeight( pImage );
		
		if( !Bitwise::isPowOf2( width ) || !Bitwise::isPowOf2( height ) )
		{
			#if DEBUG
				LOG( "As dimensões da textura não são potências de 2. Width = %d, Height = %d\n", width, height );
			#endif

			goto Error;
		}

		// Aloca a memória necessária para conter o bitmap da imagem
		pTexData = new uint8[ ( width * height ) << 2 ];
		if( pTexData == NULL )
			goto Error;

		// Preenche o bitmap
		CGContextRef texContext = CGBitmapContextCreate( pTexData, width, height, 8, width << 2, CGImageGetColorSpace( pImage ), kCGImageAlphaPremultipliedLast );
		CGContextSetBlendMode( texContext, kCGBlendModeCopy);
		CGContextDrawImage( texContext, CGRectMake( 0.0f, 0.0f, static_cast< float >( width ), static_cast< float >( height ) ), pImage );
		CGContextRelease( texContext );

		// Especifica qual imagem utilizaremos para esta textura 2D
		glBindTexture( GL_TEXTURE_2D, texId );
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pTexData );
		glBindTexture( GL_TEXTURE_2D, GL_NONE );
		
		// Armazena os dados dos frames contidos na textura
		if( pTexDesc )
		{
			if( !framesInfo.copy( *pTexDesc ) )
				goto Error;
		}
		else
		{
			TextureFrame frame;
			frame.set( 0, 0, width, height, 0, 0 );
			if( !framesInfo.setFrames( 1, &frame ) )
				goto Error;

			framesInfo.setOriginalFrameSize( width, height );
		}

		DELETE_VEC( pTexData );
		return true;
	
	} // Evita problemas com o goto
	
	// Label de tratamento de erros
	Error:
		CGImageRelease( pImage );
		DELETE_VEC( pTexData );
		return false;
}

/*==============================================================================================

 MÉTODO load16bitTexture
	Carrega a textura de 16 bits contida no arquivo passado como parâmetro.
 
==============================================================================================*/

#if LOAD_TEXTURES_WITH_QUARTZ

bool Texture2D::load16BitTexInFile( const char *pFileName, const TextureFrameDescriptor* pTexDesc, TexPixelFormat destFormat )
{
	char buffer[ PATH_MAX ];
	NSString* hFilePath = CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, pFileName, "png" ) );
	return load16bitTexture( [[UIImage imageWithContentsOfFile: hFilePath] CGImage], pTexDesc, destFormat );
}

#else

bool Texture2D::load16BitTexInFile( const char *pFileName, const TextureFrameDescriptor* pTexDesc, TexPixelFormat destFormat )
{
	// TODOO
//	NSData* hFileData = [NSData dataWithContentsOfFile: Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( pFileName ), @"png" )];
//	
//	PNG texImage( ( const uint8* )[hFileData bytes] );
//	
//	[hFileData release];
	
	char buffer[ PATH_MAX ];
	PNG texImage( Utils::GetPathForResource( buffer, PATH_MAX, pFileName, "png" ) );
	return load16bitTexture( &texImage, pTexDesc, destFormat );
}

#endif

/*==============================================================================================

 MÉTODO load16bitTexture
	Carrega uma textura de 16bits.
 
==============================================================================================*/

#if LOAD_TEXTURES_WITH_QUARTZ

bool Texture2D::load16bitTexture( CGImageRef pImage, const TextureFrameDescriptor* pTexDesc, TexPixelFormat destFormat )
{
#if DEBUG
	if( !pImage )
		return false;
#endif

	uint8 *pInputTexData = NULL;
	uint8 *p16BitTexData = NULL;
	
	{ // Evita problemas com o goto

		// Verifica se as dimensões da textura são potências de 2
		width = CGImageGetWidth( pImage );
		height = CGImageGetHeight( pImage );
		
		if( !Bitwise::isPowOf2( width ) || !Bitwise::isPowOf2( height ) )
		{
			#if DEBUG
				LOG( "As dimensões da textura não são potências de 2. Width = %d, Height = %d\n", width, height );
			#endif

			goto Error;
		}
		
		pInputTexData = new uint8[ ( width * height ) << 2 ];
		if( pInputTexData == NULL )
			goto Error;

		// Preenche o bitmap
		CGContextRef texContext = CGBitmapContextCreate( pInputTexData, width, height, 8, width << 2, CGImageGetColorSpace( pImage ), kCGImageAlphaPremultipliedLast );
		
		// CGContextSetBlendMode( texContext, kCGBlendModeCopy );
		CGContextSetBlendMode( texContext, kCGBlendModeNormal );
		
		CGContextDrawImage( texContext, CGRectMake( 0.0f, 0.0f, static_cast< float >( width ), static_cast< float >( height ) ), pImage );
		CGContextRelease( texContext );

		// Converte de 32bits para 16 bits
		uint32* p32BitTexData = reinterpret_cast< uint32* >( pInputTexData );
		
		uint32 nPixels = width * height;
		p16BitTexData = new uint8[ nPixels << 1 ];
		if( p16BitTexData == NULL )
			goto Error;

		GLenum format = GL_RGBA;
		GLenum formatType = GL_UNSIGNED_SHORT_4_4_4_4;

		switch( destFormat )
		{
			case TEX_FORMAT_RGBA_4444:
				convert32bppTo16bpp4444( reinterpret_cast< uint16* >( p16BitTexData ), p32BitTexData, nPixels );
				
				format = GL_RGBA;
				formatType = GL_UNSIGNED_SHORT_4_4_4_4;
				break;
			
			case TEX_FORMAT_RGBA_5551:
				convert32bppTo16bpp5551( reinterpret_cast< uint16* >( p16BitTexData ), p32BitTexData, nPixels );
				
				format = GL_RGBA;
				formatType = GL_UNSIGNED_SHORT_5_5_5_1;
				break;

			case TEX_FORMAT_RGB_565:
				convert32bppTo16bpp565( reinterpret_cast< uint16* >( p16BitTexData ), p32BitTexData, nPixels );
				
				format = GL_RGB;
				formatType = GL_UNSIGNED_SHORT_5_6_5;
				break;
		}

		// OLD
//		for( uint32 currPixel = 0 ; currPixel < nPixels ; ++currPixel )
//		{
//			p16BitTexData[ currPixel ] = 
//
//			// R
//			( lerps_0_15[ (( p32BitTexData[ currPixel ] & 0xFF ) * 100 ) >> 8 ] << 12 ) |
//			
//			// G
//			( lerps_0_15[ ((( p32BitTexData[ currPixel ] >>  8 ) & 0xFF ) * 100 ) >> 8 ] <<  8 ) |
//								 
//			// B
//			( lerps_0_15[ ((( p32BitTexData[ currPixel ] >> 16 ) & 0xFF ) * 100 ) >> 8 ] <<  4 ) |
//								 
//			// A
//			( lerps_0_15[ ((( p32BitTexData[ currPixel ] >> 24 ) & 0xFF ) * 100 ) >> 8 ] );
//		}

		DELETE_VEC( pInputTexData );

		// Especifica qual imagem utilizaremos para esta textura 2D
		glBindTexture( GL_TEXTURE_2D, texId );
		glTexImage2D( GL_TEXTURE_2D, 0, format, width, height, 0, format, formatType, p16BitTexData );
		
		#if DEBUG
			GLenum error = glGetError();
			if( error != GL_NO_ERROR )
				LOG( "Error loading texture: %x", error );
		#endif
		
		glBindTexture( GL_TEXTURE_2D, GL_NONE );
		
		DELETE_VEC( p16BitTexData );
		
		// Armazena os dados dos frames contidos na textura
		if( pTexDesc )
		{
			if( !framesInfo.copy( *pTexDesc ) )
				goto Error;
		}
		else
		{
			TextureFrame frame;
			frame.set( 0, 0, width, height, 0, 0 );
			if( !framesInfo.setFrames( 1, &frame ) )
				goto Error;

			framesInfo.setOriginalFrameSize( width, height );
		}
		return true;
	
	} // Evita problemas com o goto
	
	// Label de tratamento de erros
	Error:
		CGImageRelease( pImage );
		DELETE_VEC( p16BitTexData );
		DELETE_VEC( pInputTexData );
		return false;
}

#else

bool Texture2D::load16bitTexture( PNG* pImage, const TextureFrameDescriptor* pTexDesc, TexPixelFormat destFormat )
{
#if DEBUG
	if( !pImage )
		return false;
#endif

	uint8 *p16BitTexData = NULL;
	
	{ // Evita problemas com o goto

		// Verifica se as dimensões da textura são potências de 2
		width = pImage->getWidth();
		height = pImage->getHeight();
		
		if( !Bitwise::isPowOf2( width ) || !Bitwise::isPowOf2( height ) )
		{
			#if DEBUG
				LOG( "As dimensões da textura não são potências de 2. Width = %d, Height = %d\n", width, height );
			#endif

			goto Error;
		}
		
		uint32* p32BitTexData = ( uint32* ) pImage->getPixels();
		if( p32BitTexData == NULL )
			goto Error;

		// Converte de 32bits para 16 bits
		uint32 nPixels = width * height;
		p16BitTexData = new uint8[ nPixels << 1 ];
		if( p16BitTexData == NULL )
			goto Error;
		
		GLenum format = GL_RGBA;
		GLenum formatType = GL_UNSIGNED_SHORT_4_4_4_4;

		switch( destFormat )
		{
			case TEX_FORMAT_RGBA_4444:
				convert32bppTo16bpp4444( reinterpret_cast< uint16* >( p16BitTexData ), p32BitTexData, nPixels );
				
				format = GL_RGBA;
				formatType = GL_UNSIGNED_SHORT_4_4_4_4;
				break;
			
			case TEX_FORMAT_RGBA_5551:
				convert32bppTo16bpp5551( reinterpret_cast< uint16* >( p16BitTexData ), p32BitTexData, nPixels );
				
				format = GL_RGBA;
				formatType = GL_UNSIGNED_SHORT_5_5_5_1;
				break;

			case TEX_FORMAT_RGB_565:
				convert32bppTo16bpp565( reinterpret_cast< uint16* >( p16BitTexData ), p32BitTexData, nPixels );
				
				format = GL_RGB;
				formatType = GL_UNSIGNED_SHORT_5_6_5;
				break;
		}

		// Libera a memória alocada pela imagem
		pImage->clear();

		// Especifica qual imagem utilizaremos para esta textura 2D
		glBindTexture( GL_TEXTURE_2D, texId );
		glTexImage2D( GL_TEXTURE_2D, 0, format, width, height, 0, format, formatType, p16BitTexData );
		
		#if DEBUG
			GLenum error = glGetError();
			if( error != GL_NO_ERROR )
				LOG( "Error loading texture: %x", error );
		#endif
		
		glBindTexture( GL_TEXTURE_2D, GL_NONE );
		
		DELETE_VEC( p16BitTexData );
		
		// Armazena os dados dos frames contidos na textura
		if( pTexDesc )
		{
			if( !framesInfo.copy( *pTexDesc ) )
				goto Error;
		}
		else
		{
			TextureFrame frame;
			frame.set( 0, 0, width, height, 0, 0 );
			if( !framesInfo.setFrames( 1, &frame ) )
				goto Error;

			framesInfo.setOriginalFrameSize( width, height );
		}
		return true;
	
	} // Evita problemas com o goto
	
	// Label de tratamento de erros
	Error:
		DELETE_VEC( p16BitTexData );
		return false;
}

#endif

/*==============================================================================================

 MÉTODO convert32bppTo16bpp4444
	Converte uma textura de 32 bpp para uma textura de 16 bpp no formato RGBA 4444.
 
==============================================================================================*/

uint16* Texture2D::convert32bppTo16bpp4444( uint16* p16BitTexData, const uint32* p32BitTexData, int32 texDatalen )
{
	for( int32 currPixel = 0 ; currPixel < texDatalen ; ++currPixel )
	{
		p16BitTexData[ currPixel ] = 

		// R
		( lerps_0_15[ (( p32BitTexData[ currPixel ] & 0xFF ) * 100 ) >> 8 ] << 12 ) |
		
		// G
		( lerps_0_15[ ((( p32BitTexData[ currPixel ] >>  8 ) & 0xFF ) * 100 ) >> 8 ] <<  8 ) |
							 
		// B
		( lerps_0_15[ ((( p32BitTexData[ currPixel ] >> 16 ) & 0xFF ) * 100 ) >> 8 ] <<  4 ) |
							 
		// A
		( lerps_0_15[ ((( p32BitTexData[ currPixel ] >> 24 ) & 0xFF ) * 100 ) >> 8 ] );
	}
	
	return p16BitTexData;
}

/*==============================================================================================

 MÉTODO convert32bppTo16bpp5551
	Converte uma textura de 32 bpp para uma textura de 16 bpp no formato RGBA 5551.
 
==============================================================================================*/

uint16* Texture2D::convert32bppTo16bpp5551( uint16* p16BitTexData, const uint32* p32BitTexData, int32 texDatalen )
{
	for( int32 currPixel = 0 ; currPixel < texDatalen ; ++currPixel )
	{
		p16BitTexData[ currPixel ] = 

		// R
		( lerps_0_31[ (( p32BitTexData[ currPixel ] & 0xFF ) * 100 ) >> 8 ] << 11 ) |
		
		// G
		( lerps_0_31[ ((( p32BitTexData[ currPixel ] >>  8 ) & 0xFF ) * 100 ) >> 8 ] <<  6 ) |
							 
		// B
		( lerps_0_31[ ((( p32BitTexData[ currPixel ] >> 16 ) & 0xFF ) * 100 ) >> 8 ] <<  1 ) |
		 
		// A
		( ((( p32BitTexData[ currPixel ] >> 24 ) & 0xFF ) + 1 ) >> 8 ); // TODOO : Está porco deste jeito
	}
	
	return p16BitTexData;
}

/*==============================================================================================

 MÉTODO convert32bppTo16bpp565
	Converte uma textura de 32 bpp para uma textura de 16 bpp no formato RGB 565.
 
==============================================================================================*/

uint16* Texture2D::convert32bppTo16bpp565( uint16* p16BitTexData, const uint32* p32BitTexData, int32 texDatalen )
{
	for( int32 currPixel = 0 ; currPixel < texDatalen ; ++currPixel )
	{
		p16BitTexData[ currPixel ] = 

		// R
		( lerps_0_31[ (( p32BitTexData[ currPixel ] & 0xFF ) * 100 ) >> 8 ] << 11 ) |
		
		// G
		( lerps_0_63[ ((( p32BitTexData[ currPixel ] >>  8 ) & 0xFF ) * 100 ) >> 8 ] <<  5 ) |
							 
		// B
		( lerps_0_31[ ((( p32BitTexData[ currPixel ] >> 16 ) & 0xFF ) * 100 ) >> 8 ] );
	}
	
	return p16BitTexData;
}

/*==============================================================================================

 MÉTODO loadableHandleEvent
	Método para receber os eventos do objeto loadable.
 
==============================================================================================*/

void Texture2D::loadableHandleEvent( LoadableOp op, int32 loadableId, uint32 data )
{
	if( op == LOADABLE_OP_LOAD )
		glBindTexture( GL_TEXTURE_2D, data );
	else
		glBindTexture( GL_TEXTURE_2D, GL_NONE );

	if( pUserListener )
		pUserListener->loadableHandleEvent( op, loadableId, data );
}
