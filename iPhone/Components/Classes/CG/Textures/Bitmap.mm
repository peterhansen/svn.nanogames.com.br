#include "Bitmap.h"

#include <string.h>

#include "Macros.h"

struct BmpFileHeader
{
	int16 signature; // ( 0x42 0x4D - Hex code points for 'B' and 'M' )
	int32 bmpSize;
	int16 reserved1;
	int16 reserved2;
	int32 bmpDataOffset;
};

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

BitmapParser::BitmapParser( void )
{
	memset( &infoHeader, 0, sizeof( BmpInfoHeader ) );
}

/*==============================================================================================

MÉTODO parse
	Preenche as estruturas do bitmap com os dados obtidos de pData.

===============================================================================================*/

bool BitmapParser::parse( const uint8* pData )
{
	// Confere a assinatura do arquivo
	if(( pData[0] != 'B' ) || ( pData[1] != 'M' ))
		return false;
	
	// Pula o FileHeader
	// OBS : Não utiliza sizeof( BmpFileHeader ) por causa do alinhamento do sistema
	BmpFileHeader fileHeader;
	uint32 fileHeaderSize = sizeof( fileHeader.signature ) + sizeof( fileHeader.bmpSize ) + sizeof( fileHeader.reserved1 ) + sizeof( fileHeader.reserved2 ) + sizeof( fileHeader.bmpDataOffset );

	// Armazena os dados de InfoHeader
	uint8* pBmpInfoHeader = ( uint8* )( pData + fileHeaderSize );
	
	infoHeader.headerSize = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.width = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.height = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.numColorPlanes = *( ( int16* )pBmpInfoHeader );
	pBmpInfoHeader += 2;

	infoHeader.bpp = *( ( int16* )pBmpInfoHeader );
	pBmpInfoHeader += 2;

	infoHeader.compressionType = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.rawDataSize = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.horResolution = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.verResolution = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.paletteNColors = *( ( int32* )pBmpInfoHeader );
	pBmpInfoHeader += 4;
	
	infoHeader.nImportantColors = *( ( int32* )pBmpInfoHeader );

	//dataSize = ( paletteNColors * paletteColorSize ) + ( width * height * ( bpp / 8 ) )
	
	// Armazena um ponteiro para a paleta de cores e outro para os pixels da imagem
	uint8* pRawData = ( uint8* )( pData + fileHeaderSize + infoHeader.headerSize );
	if( infoHeader.paletteNColors > 0 )
	{
		pPalette = pRawData;
		
		#if DEBUG
			printPalette();
		#endif

		pPixels = pRawData + ( infoHeader.paletteNColors * 4 );
	}
	else
	{
		pPalette = NULL;
		pPixels = pRawData;
	}
	return true;
}

/*==============================================================================================

MÉTODO printPalette
	Imprime a paleta da imagem na tela.

===============================================================================================*/

#if DEBUG

void BitmapParser::printPalette( void )
{
	LOG( "Palette Begin" );

	for( int16 i = 0 ; i < infoHeader.paletteNColors ; ++i )
	{
		uint8 b = pPalette[ ( i * 4 ) + 0 ];
		uint8 g = pPalette[ ( i * 4 ) + 1 ];
		uint8 r = pPalette[ ( i * 4 ) + 2 ];
		
		LOG( "Entry %d => R:%d, G:%d, B:%d", i, r, g, b );
	}

	LOG( "Palette End" );
}

#endif
