//
//  main.mm
//  Components
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "Exceptions.h"

#if DEBUG
	#include "ObjcMacros.h"
#endif

void myTerminate( void )
{
	#if DEBUG
		assert_n_log( false, ">>> Called std::terminate(). Aborting app\n" );
	#endif

	// TODOO : Terminar com exit() não dispara SIGABRT. Isso não deixa explícito para o programador que o programa terminou com erro
	// (apesar de passarmos EXIT_FAILURE como parâmetro e fazermos o log acima no modo DEBUG). Por isso mantemos abort().
	// As dúvidas são:
	// - 1) Será que vale a pena tornarmos hPool global para chamarmos [hPool release] aqui? Não é preciosismo demais, já que o sistema garante
	//		que todos os recursos serão liberados quando a aplicação é terminada / abortada?
	//
	// - 2) Será que vale a pena chamarmos [APP_DELEGATE quit: ERROR_CODE_STD_TERMINATE] aqui, visando o término "mais correto" da aplicação?
//	exit( EXIT_FAILURE );
	
	abort();
}

void myUnexpected( void )
{
	#if DEBUG
		LOG( ">>> EXCEPTION => Unexpected C++ exception thrown. Terminating app\n" );
	#endif

	std::terminate();
}

int main( int argc, char* argv[] )
{
	int retVal = 0;
	NSAutoreleasePool* hPool = [[NSAutoreleasePool alloc] init];

	@try
	{
		try
		{
			// Fornece alguns handlers de C++ que nos permitirão fazer logs adicionais em casos de erro
			std::set_terminate( myTerminate );
			std::set_unexpected( myUnexpected );

			// Inicia a execução da aplicação
			retVal = UIApplicationMain( argc, argv, NULL, NULL );
		}
		catch( std::exception& ex )
		{
			#if DEBUG
				LOG( ">>> EXCEPTION => C++ exception caught in main(): %s\n", ex.what() );
			#endif
			retVal = 1;
		}
		catch( ... )
		{
			#if DEBUG
				LOG( ">>> EXCEPTION => Undefined C++ object caught in main()\n" );
			#endif
			retVal = 1;
		}
	}
	@catch( NSException* hException )
	{
		#if DEBUG
			LOG( ">>> EXCEPTION => Objc exception caught in main(): %s\n", NSSTRING_TO_CHAR_ARRAY( [hException reason] ) );
		#endif
		retVal = 1;
	}
	@finally
	{
		[hPool release];
	}

	return retVal;
}
	