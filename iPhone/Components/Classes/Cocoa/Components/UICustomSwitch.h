/*
 *  UICustomSwitch.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/25/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UI_CUSTOM_SWITCH_H
#define UI_CUSTOM_SWITCH_H

#import <UIKit/UIKit.h>

// UICustomSwitch allows you to set the left and right label text 
@interface UICustomSwitch : UISwitch
{
	@private
		UILabel *hLeftLabel, *hRightLabel;
}

-( void )setLeftLabelText:( NSString* )labelText; 
-( void )setRightLabelText:( NSString* )labelText; 

@end

#endif
