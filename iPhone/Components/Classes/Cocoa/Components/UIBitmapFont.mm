#include "UIBitmapFont.h"

// Components
#include "ObjcMacros.h"
#include "Utils.h"

// Extensão da classe para declarar métodos "privados"
@interface UIBitmapFont( Private )

// Inicializa o objeto
-( bool )buildUIBitmapFontWithImgs:( NSString* )hFontImgsId andDescriptor:( NSString* )hFontDescriptor monoSpaced:( bool )isMonoSpaced;

// Libera a memória alocada pelo objeto
-( void )cleanUIBitmapFont;

// Retorna o índice do frame correspondente ao caractere
-( NSInteger )getCharFrameIndex:( unichar )c;

@end

/*==============================================================================================

IMPLEMENTAÇÃO DA CLASSE

==============================================================================================*/

// Início da implementação da classe
@implementation UIBitmapFont

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize monoSpaced, spaceWidth, fontHeight;

/*==============================================================================================

MENSAGEM initWithImgsId:fontDescriptor:andSpacementType:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithImgsId:( NSString* )hFontImgsId fontDescriptor:( NSString* )hFontDescriptor andSpacementType:( bool )isMonoSpaced
{
    if( ( self = [super init] ) )
	{
		if( ![self buildUIBitmapFontWithImgs: hFontImgsId andDescriptor: hFontDescriptor monoSpaced: isMonoSpaced] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self cleanUIBitmapFont];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM buildUIBitmapFontWithImgs:andDescriptor:
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildUIBitmapFontWithImgs:( NSString* )hFontImgsId andDescriptor:( NSString* )hFontDescriptor monoSpaced:( bool )isMonoSpaced
{
	// Inicializa as variáveis da classe
	monoSpaced = isMonoSpaced;
	spaceWidth = 0;
	fontHeight = 0;
	hFontImage = nil;
	hCharacterSet = nil;
	
	{
		// Lê o descritor da fonte
		if( !fontDesc.readFromFile( NSSTRING_TO_CHAR_ARRAY( hFontDescriptor ) ))
			goto Error;
		
		// Carrega a imagem da fonte
		char buffer[ PATH_MAX ];
		NSString* hFilePath = CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, NSSTRING_TO_CHAR_ARRAY( hFontImgsId ), "png" ) );
		hFontImage = [UIImage imageWithContentsOfFile: hFilePath];
		if( hFontImage == nil )
			goto Error;
		
		// Armazena outros atributos da fonte
		spaceWidth = fontDesc.getSpaceCharWidth();
		fontHeight = fontDesc.getFontHeight();
		hCharacterSet = fontDesc.getCharSet();
		[hCharacterSet retain];

		return true;
		
	}
	
	// Label de tratamento de erros
	Error:
		[self cleanUIBitmapFont];
		return false;
}

/*==============================================================================================

MENSAGEM cleanUIBitmapFont
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanUIBitmapFont
{
	[hFontImage release];
	[hCharacterSet release];
}

/*==============================================================================================

MENSAGEM getCharFrameIndex:
	Retorna o índice do frame correspondente ao caractere.

===============================================================================================*/

-( NSInteger )getCharFrameIndex:( unichar )c
{
	NSUInteger nChars = [hCharacterSet length];
	for( NSUInteger i = 0 ; i < nChars ; ++i )
	{
		if( [hCharacterSet characterAtIndex: i] == c )
			return i;
	}
	return -1;
}

/*==============================================================================================

MENSAGEM getCharSize:
	Retorna o tamanho do caractere na fonte.

===============================================================================================*/

-( CGSize )getCharSize:( unichar )c
{
	CGSize charSize = CGSizeMake( 0.0f, 0.0f );

	if( monoSpaced )
	{
		charSize.width = fontDesc.getOriginalFrameWidth();
		charSize.height = fontHeight;
	}
	else
	{
		if( c == ' ' )
		{
			charSize.width = spaceWidth;
			charSize.height = fontHeight;
		}
		else
		{
			int32 charFrameIndex = [self getCharFrameIndex: c];
			if( charFrameIndex >= 0 )
			{
				const TextureFrame* pCharFrame = fontDesc.getFrame( charFrameIndex );
				charSize.width = pCharFrame->width;
				charSize.height = pCharFrame->height;
			}
		}
	}
	return charSize;
}

/*==============================================================================================

MENSAGEM getTextSize:withSpacement:
	Retorna qual a área necessária para se renderizar o texto.

===============================================================================================*/

-( CGSize )getTextSize:( NSString* )hText withCharSpacement:( float )spacement
{
	CGSize textSize = CGSizeMake( 0.0f, 0.0f );

	NSUInteger nChars = hText.length;
	for( NSUInteger i = 0 ; i < nChars ; ++i )
	{
		CGSize charSize = [self getCharSize: [hText characterAtIndex: i]];
		textSize.width += charSize.width + spacement;
		
		// Poderíamos retornar sempre fontHeight, mas assim fica mais genérico
		if( charSize.height > textSize.height )
			textSize.height = charSize.height;
	}

	// O último espaçamento deve ser retirado
	textSize.width -= spacement;

	return textSize;
}

/*==============================================================================================

MENSAGEM getCharImage:
	Retorna uma view contendo a imagem do caractere.

===============================================================================================*/

-( CGImageRef )getCharImage:( unichar )c
{
	const TextureFrame* pCharFrame = fontDesc.getFrame( [self getCharFrameIndex: c] );
	return CGImageCreateWithImageInRect( [hFontImage CGImage], CGRectMake( pCharFrame->x, pCharFrame->y, pCharFrame->width, pCharFrame->height ));
}

/*==============================================================================================

MENSAGEM getCharImage:
	Retorna o offset do caractere em relação ao frame padrão dos caracteres da fonte.

===============================================================================================*/

-( CGSize )getCharOffset:( unichar )c
{
	const TextureFrame* pCharFrame = fontDesc.getFrame( [self getCharFrameIndex: c] );
	return CGSizeMake( pCharFrame->offsetX, pCharFrame->offsetY );
}

// Fim da implementação da classe
@end

