/*
 *  UICustomScrollView.h
 *  NetworkTest
 *
 *  Created by Daniel Lopes Alves on 9/28/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UI_CUSTOM_SCROLL_VIEW_H
#define UI_CUSTOM_SCROLL_VIEW_H

// Apple
#import <UIKit/UIKit.h>

@interface UICustomScrollView : UIScrollView
{
	@private
		NSArray *hCancelTouchesViews;
}

// Determina as subviews que devem tomar conta do toque, impedindo que UICustomScrollView faça o scroll da tela
-( void )setCancelTouchesViews:( NSArray* )hViewsArray;

@end

#endif
