/*
 *  UIBitmapLabel.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 5/6/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef UIBITMAP_LABEL_H
#define UIBITMAP_LABEL_H

#include "TextAlignment.h"
#include "UIBitmapFont.h"

@interface UIBitmapLabel : UIView
{
	@private
		// Fonte utilizada pelo label
		UIBitmapFont *font;
	
		// Texto do label
		NSString *text;
	
		// Alinhamento do texto
		TextAlignment alignment;
	
		// Espaçamento entre os caracteres do label
		float charSpacement;
}

@property( readwrite, nonatomic, retain ) NSString *text;
@property( readwrite, nonatomic, retain ) UIBitmapFont *font;
@property( readwrite, nonatomic, assign ) TextAlignment alignment;
@property( readwrite, nonatomic, assign ) float charSpacement;

// Redimensiona o label para comportar o texto
-( void )adjustFrameToText;

@end

#endif
