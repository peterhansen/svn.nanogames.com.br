#include "NOCallOutView.h"

// Components
#include "ObjcMacros.h"
#include "Utils.h"

// Definições auxiliares
#define NO_CALLOUT_USEFUL_HEIGHT		38.0f
#define NO_CALLOUT_DIST_FROM_BORDER		 5.0f
#define NO_CALLOUT_WIDTH_TILL_CALLOUT	30.0f

#define NO_CALLOUT_FINAL_ALPHA 0.8f

#define NO_CALLOUT_CHANGE_TITLE_DUR	1.0f
#define NO_CALLOUT_ANIM_ID @"NOCOUTVCL"

@interface NOCallOutView( Private )

	-( bool )buildNOCallOutView;
	-( void )changeTitle;

@end

@implementation NOCallOutView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOCallOutView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOCallOutView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOCallOutView
	Inicializa a view.

==============================================================================================*/

-( bool )buildNOCallOutView
{
	// Inicializa as variáveis da classe
	hImgView = nil;
	
	hTempTitleLabel = nil;
	hTitleLabel = nil;
	
	refPoint = CGPointZero;
	
	hTimer = nil;
	
	{ // Evita erros de compilação por causa dos gotos

		// Aloca os elementos da view
		
		// Aloca a imagem da view
		char imgsType[] = "png";
		char buffer[ PATH_MAX ];
		UIImage *hImg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "callout", imgsType ))];
		if( !hImg )
			goto Error;
		
		hImgView = [[UIImageView alloc] initWithImage: hImg];
		if( !hImgView )
		{
			// OBS: Não precisamos chamar release em hImg aqui, já que ela é autorelease
			//[hImg release];

			goto Error;
		}

		[self addSubview: hImgView];
		
		// Determina o tamanho da view
		CGSize imgSize = [hImg size];
		CGRect currFrame = [self frame];
		[self setFrame: CGRectMake( currFrame.origin.x, currFrame.origin.y, imgSize.width, imgSize.height )];
		
		// Aloca os labels que irão exibir as mensagens
		hTitleLabel = [[UILabel alloc] initWithFrame: CGRectMake( NO_CALLOUT_DIST_FROM_BORDER, NO_CALLOUT_DIST_FROM_BORDER, imgSize.width - NO_CALLOUT_DIST_FROM_BORDER, NO_CALLOUT_USEFUL_HEIGHT - NO_CALLOUT_DIST_FROM_BORDER )];
		if( !hTitleLabel )
			goto Error;

		[self addSubview: hTitleLabel];
		[hTitleLabel release];
		
		hTempTitleLabel = [[UILabel alloc] initWithFrame: [hTitleLabel frame]];
		if( !hTempTitleLabel )
			goto Error;

		[self addSubview: hTempTitleLabel];
		[hTempTitleLabel release];
		
		// Configura a view para o seu estado inicial
		[hImgView setAlpha: 0.0f];
		[self setHidden: YES];
		
		UIFont *hFont = [UIFont boldSystemFontOfSize: 17.0f];
		if( !hFont )
			goto Error;

		[hTitleLabel setAdjustsFontSizeToFitWidth: YES];
		[hTitleLabel setTextAlignment: UITextAlignmentCenter];
		[hTitleLabel setTextColor: [UIColor whiteColor]];
		[hTitleLabel setBackgroundColor: [UIColor clearColor]];
		[hTitleLabel setFont: hFont];
		
		[hTempTitleLabel setAdjustsFontSizeToFitWidth: YES];
		[hTempTitleLabel setTextAlignment: UITextAlignmentCenter];
		[hTempTitleLabel setTextColor: [UIColor whiteColor]];
		[hTempTitleLabel setBackgroundColor: [UIColor clearColor]];
		[hTempTitleLabel setFont: hFont];

		return true;
	
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
		//[self cleanNOHelpView];
		return false;
}

-( void )setTempTitle:( NSString* )hTempTitle
{
	[hTempTitleLabel setText: hTempTitle];
}

// Retorna o título temporário da callout
-( NSString* )tempTitle
{
	return [hTempTitleLabel text];
}

-( void )setTitle:( NSString* )hTitle
{
	[hTitleLabel setText: hTitle];
}

// Retorna o título principal da callout
-( NSString* )title
{
	return [hTitleLabel text];
}

-( void )setRefPoint:( CGPoint )p
{
	refPoint = p;
	CGRect currFrame = [self frame];
	[self setFrame: CGRectMake( refPoint.x - NO_CALLOUT_WIDTH_TILL_CALLOUT, refPoint.y - [hImgView frame].size.height, currFrame.size.width, currFrame.size.height )];
}

-( void )showCallout
{
	if( hTimer )
	{
		[hTimer invalidate];
		hTimer = nil;
	}

	[hImgView setAlpha: NO_CALLOUT_FINAL_ALPHA];
	[hTitleLabel setAlpha: 0.0f];
	[hTempTitleLabel setAlpha: 1.0f];
	
	[self setAlpha: 1.0f];
	[self setHidden: NO];
	
#if DEBUG
	LOG( "NOCallOut\n" );
	LOG( "TempTitleLabelAlpha = %.3f\n", [hTempTitleLabel alpha] );
	LOG( "TempTitleLabelText = %s\n", NSSTRING_TO_CHAR_ARRAY( [hTempTitleLabel text] ) );
	LOG( "TitleLabelAlpha = %.3f\n", [hTitleLabel alpha] );
	LOG( "TitleLabelText = %s\n", NSSTRING_TO_CHAR_ARRAY( [hTitleLabel text] ) );
	LOG( "\n\n" );
#endif
	
	// TODOO: Só não está fazendo a animação de "bounce" ao aparecer. Fora isso, está bem parecido com a original
	if( hTempTitleLabel != nil )
		hTimer = [NSTimer scheduledTimerWithTimeInterval: 2.0f target: self selector: @selector( changeTitle ) userInfo: nil repeats: NO];
	else
		[hTitleLabel setAlpha: 1.0f];
}

-( void )changeTitle
{
	hTimer = nil;

	[UIView beginAnimations: NO_CALLOUT_ANIM_ID context: nil];
	[UIView setAnimationDuration: NO_CALLOUT_CHANGE_TITLE_DUR];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut];
	[hTempTitleLabel setAlpha: 0.0f];
	[hTitleLabel setAlpha: 1.0f];
	[UIView commitAnimations];
}

@end
