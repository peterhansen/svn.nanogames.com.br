#include "UIBitmapLabel.h"

// Extensão da classe para declarar métodos "privados"
@interface UIBitmapLabel( Private )

// Inicializa o objeto
-( bool )buildUIBitmapLabel;

// Libera a memória alocada pelo objeto
-( void )cleanUIBitmapLabel;

// Apaga todas as subviews atuais
-( void )cleanSubviews;

// Aloca as views necessárias para conter os caracteres do label
-( void )processText;

@end

/*==============================================================================================

IMPLEMENTAÇÃO DA CLASSE

==============================================================================================*/

// Início da implementação da classe
@implementation UIBitmapLabel

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize font, alignment, charSpacement, text;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame: frame] ) )
	{
		if( ![self buildUIBitmapLabel] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

-( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder: decoder] ) )
	{
		if( ![self buildUIBitmapLabel] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self cleanUIBitmapLabel];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM buildUIBitmapLabel
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildUIBitmapLabel
{
	// Inicializa as variáveis do objeto
	alignment = ALIGN_HOR_LEFT | ALIGN_VER_TOP;
	text = nil;
	font = nil;
	charSpacement = 0.0f;
	
	[self setClipsToBounds: YES];
	
	return true;
}

/*==============================================================================================

MENSAGEM cleanUIBitmapLabel
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanUIBitmapLabel
{
	[font release];
}

/*==============================================================================================

MÉTODO setFont:
	Determina a fonte do label.

==============================================================================================*/

-( void )setFont:( UIBitmapFont* )hFont
{
	if( font != hFont )
	{
		self.font = hFont;
		
		// Aloca as views necessárias para conter os caracteres do label
		[self processText];
	}
}

/*==============================================================================================

MÉTODO adjustFrameToText
	Redimensiona o label para comportar o texto.

==============================================================================================*/

-( void )adjustFrameToText
{
	if( ( font != nil ) && ( text != nil ) )
	{
		CGSize textSize = [font getTextSize: text withCharSpacement: charSpacement];

		CGPoint currOrigin = [self frame].origin;
		[self setFrame: CGRectMake( currOrigin.x, currOrigin.y, textSize.width, textSize.height )];
	}
}

/*==============================================================================================

MÉTODO setText:
	Determina o texto do label.

==============================================================================================*/

-( void )setText:( NSString* )hNewText
{
	if( [hNewText compare: text] !=  NSOrderedSame )
	{
		// Armazena o texto passado como parâmetro
		text = hNewText;
		
		if( font != nil )
		{
			// Aloca as views necessárias para conter os caracteres do label
			[self processText];
		}
	}
}

/*==============================================================================================

MÉTODO cleanSubviews
	Apaga todas as subviews atuais.

==============================================================================================*/

-( void )cleanSubviews
{
	NSArray *hSubviews = [self subviews];
	if( hSubviews != nil )
	{
		for( UIView* hSubview in hSubviews )
			[hSubview removeFromSuperview];
	}
}

/*==============================================================================================

MÉTODO processText
	Aloca as views necessárias para conter os caracteres do label.

==============================================================================================*/

-( void )processText
{
	// Apaga todas as subviews atuais
	[self cleanSubviews];
	
	// Obtém o tamanho atual da view
	CGSize currViewSize = [self frame].size;
	
	// Checa o alinhamento horizontal. O default é ALIGN_HOR_LEFT
	float x = 0.0f;
	if( ( alignment & ALIGN_HOR_RIGHT ) != 0 )
	{
		x =  currViewSize.width - [font getTextSize: text withCharSpacement: charSpacement].width;
	}
	else if( ( alignment & ALIGN_HOR_CENTER ) != 0 )
	{
		x = ( currViewSize.width - [font getTextSize: text withCharSpacement: charSpacement].width ) * 0.5f;
	}
	// OBS: Não é necessária a implementação
//	else if( ( alignment & ALIGN_HOR_LEFT ) != 0 )
//	{
//	}

	// Checa o alinhamento vertical. O default é ALIGN_VER_TOP
	float y = 0.0f;
	if( ( alignment & ALIGN_VER_BOTTOM ) != 0 )
	{
		y = currViewSize.height - [font fontHeight];
	}
	else if( ( alignment & ALIGN_VER_CENTER ) != 0 )
	{
		y = ( currViewSize.height - [font fontHeight] ) * 0.5f;
	}
	// OBS: Não é necessária a implementação
//	else if( ( alignment & ALIGN_VER_TOP ) != 0 )
//	{
//	}

	float currX = x;
	uint8 spaceWidth = [font spaceWidth];

	for( NSUInteger i = 0 ; i < text.length ; ++i )
	{
		unichar c = [text characterAtIndex: i];
		if( c != ' ' )
		{
			// Aloca a view para conter o caractere
			UIImage *hImg = [UIImage imageWithCGImage: [font getCharImage: c]];
			UIImageView *hCharView = [[UIImageView alloc] initWithImage: hImg];
			if( hCharView != nil )
			{
				[self addSubview: hCharView];
				[hCharView release];
				
				CGSize charSize = [hCharView frame].size;
				
				// Configura a view
				[hCharView setClipsToBounds: YES];
				[hCharView setContentMode: UIViewContentModeTopLeft];

				CGSize charOffset = [font getCharOffset: c];
				[hCharView setFrame: CGRectMake( currX + charOffset.width, y + charOffset.height, charSize.width, charSize.height )];
				
				// Incrementa o contador da posição x para a renderização do próximo caractere
				currX += charSize.width + charSpacement;
			}
			#if DEBUG
			else
			{
				// Não temos este caractere na fonte
				// TODOO: Renderiza um quadrado vermelho para indicar ao programador este problema
			}
			#endif
		}
		// Se o tamanho do caractere espaço for 0, considera que esse caractere não existe na fonte
		else if( spaceWidth > 0 )
		{
			// Incrementa o contador da posição x para a renderização do próximo caractere
			x += spaceWidth + charSpacement;
		}
	}
}

// Fim da implementação da classe
@end
