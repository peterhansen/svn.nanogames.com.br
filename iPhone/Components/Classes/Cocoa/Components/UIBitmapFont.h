/*
 *  UIBitmapFont.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 5/6/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef UIBITMAP_FONT_H
#define UIBITMAP_FONT_H

// Components
#include "FontDescriptor.h"
#include "NanoTypes.h"

@interface UIBitmapFont : NSObject
{
	@private
		// Indica se a fonte é mono-espaçada
		bool monoSpaced;
	
		// Tamanho do caractere de espaçamento da fonte
		uint8 spaceWidth;
	
		// Altura da fonte (ou seja, a altura de seu caractere mais alto)
		uint16 fontHeight;
	
		// Descritor da fonte
		FontDescriptor fontDesc;
	
		// Imagem contendo os caracateres da fonte
		UIImage *hFontImage;
	
		// String contendo os caracteres suportados pela fonte
		NSString *hCharacterSet;
}

@property( readonly, nonatomic, assign ) bool monoSpaced;
@property( readonly, nonatomic, assign ) uint8 spaceWidth;
@property( readonly, nonatomic, assign ) uint16 fontHeight;

// Construtor
-( id )initWithImgsId:( NSString* )hFontImgsId fontDescriptor:( NSString* )hFontDescriptor andSpacementType:( bool )isMonoSpaced;

// Retorna o tamanho do caractere na fonte
-( CGSize )getCharSize:( unichar )c;

// Retorna qual a área necessária para se renderizar o texto
-( CGSize )getTextSize:( NSString* )hText withCharSpacement:( float )spacement;

// Retorna uma view contendo a imagem do caractere
-( CGImageRef )getCharImage:( unichar )c;

// Retorna o offset do caractere em relação ao frame padrão dos caracteres da fonte
-( CGSize )getCharOffset:( unichar )c;

@end

#endif
