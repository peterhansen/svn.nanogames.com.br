//
//  UpdatableView.h
//  Components
//
//  Created by Daniel Lopes Alves on 6/17/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef UPDATABLE_VIEW_H
#define UPDATABLE_VIEW_H 1

#include "BasicView.h"

@interface UpdatableView : BasicView
{
	@private
		// Controlam a atualização da view
		NSTimer* hUpdateTimer;
		NSTimeInterval updateInterval;
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Pára o processamento da view
- ( void ) suspend;

// Reinicia o processamento da view
- ( void ) resume;

@end

#endif
