/*
 *  FBConnectTypes.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/19/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef FB_CONNECT_TYPES_H
#define FB_CONNECT_TYPES_H

#if ALLOW_FBCONNECT_CUSTOMIZATION

typedef unsigned long long FBUID;
typedef unsigned long long FBID;

#endif

#endif
