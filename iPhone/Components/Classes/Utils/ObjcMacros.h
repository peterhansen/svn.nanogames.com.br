/*
 *  ObjcMacros.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OBJC_MACROS_H
#define OBJC_MACROS_H 1

#include "Macros.h"

#ifdef __OBJC__

// Apple
#import <UIKit/UIKit.h>
#import <CoreGraphics/CGGeometry.h>

// Components
#include "ApplicationManager.h"

// C++
#include <string>

// Testa se um handler é nulo antes de chamar sua mensagem release
#define RELEASE_HANDLER( h ) { [h release]; h = NULL; }
#define SAFE_RELEASE( h ) { if( h )RELEASE_HANDLER( h ) }

// Converte um número inteiro para o tipo NSString
#define INT_TO_NSSTRING( v ) [NSString stringWithFormat:@"%d", static_cast< int32 >( v )]

// Converte de NSString para char*
#define NSSTRING_TO_CHAR_ARRAY( str ) [str UTF8String]

// Converte de NSString para std::string
#define NSSTRING_TO_STD_STRING( str ) std::string( NSSTRING_TO_CHAR_ARRAY( str ) )

// Converte de char* para NSString
#define CHAR_ARRAY_TO_NSSTRING( charArray ) [NSString stringWithUTF8String:charArray]

// Converte de std::string para NSString
#define STD_STRING_TO_NSSTRING( str ) [NSString stringWithCString:str.c_str()]

// Obtém a largura da tela
#define SCREEN_WIDTH [(( ApplicationManager* )APP_DELEGATE) getScreenWidth]
#define HALF_SCREEN_WIDTH ( SCREEN_WIDTH * 0.5f )

// Obtém a altura da tela
#define SCREEN_HEIGHT [(( ApplicationManager* )APP_DELEGATE) getScreenHeight]
#define HALF_SCREEN_HEIGHT ( SCREEN_HEIGHT * 0.5f )

// Obtém o viewport default para a aplicação
#define GET_DEFAULT_VIEWPORT() [(( ApplicationManager* )APP_DELEGATE) getDefaultViewport]

// Obtém o delegate da aplicação
#define APP_DELEGATE [ApplicationManager GetInstance]

#endif // #ifdef __OBJC__

#endif // #ifndef OBJC_MACROS_H
