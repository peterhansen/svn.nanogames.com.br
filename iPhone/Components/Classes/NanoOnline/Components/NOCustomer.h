/*
 *  NOCustomer.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/10/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_CUSTOMER_H
#define NANO_ONLINE_CUSTOMER_H 1

// Components
#include "ISerializable.h"
#include "NOMap.h"
#include "NOString.h"

// Facebook
#include "FBConnectTypes.h"

// Tipo do id do usuário
// TODOO : Transformar numa struct !!!!
typedef int32 NOProfileId;

// Id do usuário "dummy", ou seja, id utilizado quando ainda não há um usuário registrado
// OBS: Mantemos o CUSTOMER_PROFILE_ID_NONE como -1 para manter a compatibilidade com o
// código JAVA. Mas o mais correto, e menos propenso a erros, seria NOCustomerId ser do
// tipo unsigned e CUSTOMER_PROFILE_ID_NONE ser igual a 0. Afinal, todo id negativo é
// inválido
#define CUSTOMER_PROFILE_ID_NONE -1

// Sexo
enum Gender
{
	GENDER_N = 0,
	GENDER_M = 1,
	GENDER_F = 2
};

// Declarações adiadas
class MemoryStream;
class INOListener;

class NOCustomer : public ISerializable
{
	public:
		// Construtor
		NOCustomer( void );
	
		// Destrutor
		virtual ~NOCustomer( void );
	
		// Indica se algum usuário está registrado
		bool isRegistered( void ) const;
	
		// Métodos Get
		NOProfileId getProfileId( void ) const;

		bool isRememberingMe( void ) const;
		bool isRememberingPassword( void ) const;

		Gender getGender( void ) const;
		void getBirthday( int8* day, int8* month, int32* year ) const;

		NOString& getEmail( NOString& out ) const;
		NOString& getPassword( NOString& out ) const;

		NOString& getNickname( NOString& out ) const;
		NOString& getFirstName( NOString& out ) const;
		NOString& getLastName( NOString& out ) const;

		void getFBData( FBUID& userUID, NOString& sessionKey, NOString& sessionSecret ) const;
	
		// Métodos Set
		void setRememberMe( bool b );
		void setRememberPassword( bool b );

		void setGender( Gender g );
	
		// O parâmetro ignoreValidation é apenas para uso interno!!!!!!!!!!! O usuário desta classe NUNCA deve passar o valor true neste parâmetro!!!
		bool setBirthday( int8 day, int8 month, int32 year, NOString& outError, bool ignoreValidation = false );

		bool setNickname( const NOString& customerNickname, NOString& outError );
		bool setFirstName( const NOString& customerFirstName, NOString& outError );
		bool setLastName( const NOString& customerLastName, NOString& outError );
	
		bool setEmail( const NOString& customerEmail, NOString& outError );
		bool setPassword( const NOString& customerPassword, NOString& outError );
	
		void setFBData( const FBUID& userUID, const NOString& sessionKey, const NOString& sessionSecret );
	
		// Lê o objeto de uma stream
		virtual void serialize( MemoryStream& stream ) const;

		// Escre o objeto em uma stream 
		virtual void unserialize( MemoryStream& stream );

		// Requisições
		static bool SendCreateRequest( NOCustomer* pCustomer, INOListener* pListener );
		static bool SendEditRequest( NOCustomer* pCustomer, INOListener* pListener );
		static bool SendDownloadRequest( NOCustomer* pCustomer, INOListener* pListener );
		static bool SendLoginRequest( NOCustomer* pCustomer, INOListener* pListener );
		static bool SendFbDataDownloadRequest( NOCustomer* pCustomer, INOListener* pListener );
	
		// Retorna o tamnanho de string (em caracteres) suportado para um campo de texto
		static void GetNicknameSupportedLen( uint32& min, uint32& max );
		static void GetFirstNameSupportedLen( uint32& min, uint32& max );
		static void GetLastNameSupportedLen( uint32& min, uint32& max );
		static void GetPasswordSupportedLen( uint32& min, uint32& max );
		static void GetEmailSupportedLen( uint32& min, uint32& max );
	
		// Operadores
		bool operator ==( const NOCustomer& rho ) const;
		bool operator !=( const NOCustomer& rho ) const;

	private:	
		// Tratam o login do usuário no NanoOnline
		bool login( MemoryStream& stream );
		bool loginResponse( NOMap& table, NOString& errorStr );
	
		// Tratam o cadastro de perfis no NanoOnline
		bool createProfile( MemoryStream& stream );
		bool createProfileResponse( NOMap& table, NOString& errorStr );
		bool readCreateProfileData( MemoryStream& dataStream, NOString& errorStr );
	
		// Atualiza os dados do perfil do usuário
		bool updateProfile( MemoryStream& stream );
	
		// Tratam o download de perfis do NanoOnline
		bool download( MemoryStream& stream );
		bool downloadResponse( NOMap& table, NOString& errorStr );
		bool readDownloadData( MemoryStream& dataStream, NOString& errorStr );

		// Tratam o download de informações do Facebook
		bool fillNOProfileWithFbData( MemoryStream& stream );
		bool fillNOProfileWithFbDataResponse( NOMap& table, NOString& errorStr );
		bool readFbData( MemoryStream& dataStream, NOString& errorStr );
	
		// Id do usuário que está utilizando o NanoOnline
		NOProfileId profileId;
	
		// Indica se devemos armazenar este usuário localmente
		bool rememberMe;
		
		// Indica se devemos armazenar a senha deste usuário
		bool rememberPassword;

		// Dados pessoais do usuário
		Gender gender;
		int8 birthdayDay;
		int8 birthdayMonth;
		int32 birthdayYear;
		time_t timeStamp;
	
		// TODOO: NarrowStr...
		NOString email;
		NOString password;

		// TODOO: WideStr...
		NOString nickname;
		NOString firstName;
		NOString lastName;
	
		// Integração com Facebook
		FBUID fbUID;
		NOString fBSessionKey;
		NOString fBSessionSecret;
};

// Implementação dos métodos inline

inline NOProfileId NOCustomer::getProfileId( void ) const
{
	return profileId;
}

inline bool NOCustomer::isRememberingMe( void ) const
{
	return rememberMe;
}

inline bool NOCustomer::isRememberingPassword( void ) const
{
	return rememberPassword;
}

inline Gender NOCustomer::getGender( void ) const
{
	return gender;
}

inline NOString& NOCustomer::getEmail( NOString& out ) const
{
	out = email;
	return out;
}

inline NOString& NOCustomer::getPassword( NOString& out ) const
{
	out = password;
	return out;
}

inline NOString& NOCustomer::getNickname( NOString& out ) const
{
	out = nickname;
	return out;
}

inline NOString& NOCustomer::getFirstName( NOString& out ) const
{
	out = firstName;
	return out;
}

inline NOString& NOCustomer::getLastName( NOString& out ) const
{
	out = lastName;
	return out;
}

#endif
