/*
 *  NOFacebookConstants.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 2/19/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef NO_FACEBOOK_CONSTANTS_H
#define NO_FACEBOOK_CONSTANTS_H

// Parâmetros permitidos para o controlador do Facebook
#define NANO_ONLINE_PARAM_FACEBOOK_INFO_END				0
#define NANO_ONLINE_PARAM_FACEBOOK_UID					1
#define NANO_ONLINE_PARAM_FACEBOOK_SESSION_KEY			2
#define NANO_ONLINE_PARAM_FACEBOOK_SESSION_SECRET		3
#define NANO_ONLINE_PARAM_FACEBOOK_FEED_TYPE			4
#define NANO_ONLINE_PARAM_FACEBOOK_FEED_PARAMS			5

// Códigos de retorno relacionados ao Facebook

// Não foi possível obter os dados do usuário via Facebook, mesmo ele já
// estando logado
#define RC_ERROR_FACEBOOK_COULD_NOT_RETRIEVE_DATA	1

// O feed solicitado pela aplicação não possui registro no BD
#define RC_ERROR_FACEBOOK_INVALID_FEED_TYPE			2

// O usuário não forneceu a permissão necessária para a realização da ação
#define RC_ERROR_FACEBOOK_NO_PERMISSION				3

// Não foi possível achar as informações do facebook relativas à aplicação cliente
#define RC_ERROR_FACEBOOK_INVALID_APP				4

// Erro desconhecido ao tentar publicar o feed
#define	RC_ERROR_FACEBOOK_COULDNT_PUBLISH_FEED		5

// É usuário do Facebook, mas não é usuário do NanoOnline. Isto acontece quando
// não conseguimos, a partir de um UID, chegar a um CustomerId utilizando as
// tabelas Customers e FacebookProfiles
#define RC_ERROR_FACEBOOK_NOT_NANO_ONLINE_USER		6

// Tipos de parâmetros que podem ser passados para o preenchimento dos campos
// 'caption' e 'description' de um feed
#define FB_FEED_PARAM_TYPE_END		0
#define FB_FEED_PARAM_TYPE_INT8		1
#define FB_FEED_PARAM_TYPE_INT16	2
#define FB_FEED_PARAM_TYPE_INT32	3
#define FB_FEED_PARAM_TYPE_INT64	4
#define FB_FEED_PARAM_TYPE_STRING	5
#define FB_FEED_PARAM_TYPE_FLOAT	6
#define FB_FEED_PARAM_TYPE_DOUBLE	7

#endif
