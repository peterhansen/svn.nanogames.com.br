/*
 *  NOConf.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 11/25/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_CONFIG_H
#define NO_CONFIG_H

#include "NOCustomer.h"

struct NOConf
{
	NOConf( void ) : lastLoggedInProfile( CUSTOMER_PROFILE_ID_NONE )
	{
	}
	
	NOProfileId lastLoggedInProfile;
	
	// No futuro, poderemos acrescentar informações como skin
	// e outras mais
};

#endif
