#include "NOFacebookFeed.h"

// Components
#include "Macros.h"
#include "MemoryStream.h"

// NanoOnline
#include "NOConnection.h"
#include "NOConstants.h"
#include "NOCustomer.h"
#include "NOFacebookConstants.h"
#include "NOGlobalData.h"
#include "NORequestHolder.h"
#include "NOTextsIndexes.h"

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

NOFacebookFeed::NOFacebookFeed( FeedType feedType, const NOCustomer *pFeedTarget )
			   : pFeedTarget( pFeedTarget ),
				 feedType( feedType )
{
}

/*==============================================================================================

DESTRUTOR

===============================================================================================*/

NOFacebookFeed::~NOFacebookFeed( void )
{
}

/*==============================================================================================

MÉTODO fbWallPublish
	Indica que desejamos publicar um feed na wall do usuário.

===============================================================================================*/

bool NOFacebookFeed::fbWallPublish( MemoryStream& stream )
{
	if( pFeedTarget == NULL )
		return false;
	
	FBUID userUID;
	NOString sessionKey, sessionSecret;
	pFeedTarget->getFBData( userUID, sessionKey, sessionSecret );

	stream.writeInt8( NANO_ONLINE_PARAM_FACEBOOK_UID );
	stream.writeInt64( userUID );

    stream.writeInt8( NANO_ONLINE_PARAM_FACEBOOK_SESSION_KEY );
	stream.writeUTF32String( sessionKey );
	
	stream.writeInt8( NANO_ONLINE_PARAM_FACEBOOK_SESSION_SECRET );
	stream.writeUTF32String( sessionSecret );
	
	stream.writeInt8( NANO_ONLINE_PARAM_FACEBOOK_FEED_TYPE );
	stream.writeInt16( feedType );
	
	stream.writeInt8( NANO_ONLINE_PARAM_FACEBOOK_FEED_PARAMS );
	writeFeedParams( stream );
	
	return true;
}

/*==============================================================================================

MÉTODO fbWallPublishResponse
	Verifica se conseguimos publicar um feed na wall do usuário.

===============================================================================================*/

bool NOFacebookFeed::fbWallPublishResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			return true;
			
		case RC_ERROR_FACEBOOK_INVALID_APP:
			// TODOO GetNOText( NO_TXT_FB_BAD_UID, errorStr );
			return false;

		case RC_ERROR_FACEBOOK_NO_PERMISSION:
			// TODOO GetNOText( NO_TXT_FB_BAD_UID, errorStr );
			return false;

		case RC_ERROR_FACEBOOK_COULDNT_PUBLISH_FEED:
			// TODOO GetNOText( NO_TXT_FB_BAD_UID, errorStr );
			return false;
			
		case RC_ERROR_FACEBOOK_INVALID_FEED_TYPE:
			// TODOO GetNOText( NO_TXT_FB_BAD_UID, errorStr );
			return false;
			
		case RC_ERROR_FACEBOOK_NOT_NANO_ONLINE_USER:
			// TODOO GetNOText( NO_TXT_FB_BAD_UID, errorStr );
			return false;
			
		default:
			#if DEBUG
				assert_n_log( false, "Unrecognized param return code sent to fbWallPublish in response to fbWallPublishResponse request" );
			#else
				GetNOText( NO_TXT_UNKNOWN_ERROR, errorStr );
			#endif
			return false;
	}
}

/*==============================================================================================

MÉTODO SendFbWallPublishRequest
	Envia uma requisição de fbWallPublish ao NanoOnline.

===============================================================================================*/

bool NOFacebookFeed::SendFbWallPublishRequest( NOFacebookFeed* pFeed, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/facebook/publish_feed", MakeNORequestHolder( *pFeed, &NOFacebookFeed::fbWallPublish, &NOFacebookFeed::fbWallPublishResponse ) );
}
