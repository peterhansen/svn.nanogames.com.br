/*
 *  NOFacebookFeed.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/19/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef NO_FACEBOOK_FEED_H
#define NO_FACEBOOK_FEED_H

#include "NanoTypes.h"
#include "NOMap.h"
#include "NOString.h"

// Declarações adiadas
class INOListener;
class MemoryStream;
class NOCustomer;

class NOFacebookFeed
{
	public:
		// Tipos da classe
		typedef uint16 FeedType;

		// Construtor e Destrutor
		NOFacebookFeed( FeedType feedType, const NOCustomer *pFeedTarget );
		virtual ~NOFacebookFeed( void );

		// Determina o usuário do Facebook que receberá o feed em sua wall
		void setFeedTarget( const NOCustomer *pFeedTarget );
	
		// Retorna o usuário do Facebook que receberá o feed em sua wall
		const NOCustomer* getFeedTarget( void ) const;
	
		// Determina o tipo do feed que iremos publicar
		void setFeedType( FeedType type );
	
		// Retorna o tipo do feed que iremos publicar
		FeedType getFeedType( void ) const;
	
		// Requisições
		static bool SendFbWallPublishRequest( NOFacebookFeed* pFeed, INOListener* pListener );
	
	private:
		// Escreve os parâmetros específicos do feed
		virtual void writeFeedParams( MemoryStream& stream ) = 0;
	
		// Publica conteúdo na wall do usuário do Facebook
		bool fbWallPublish( MemoryStream& stream );
		bool fbWallPublishResponse( NOMap& table, NOString& errorStr );
	
		// Usuário do Facebook que receberá o feed em sua wall
		const NOCustomer *pFeedTarget;
	
		// Tipo do feed que iremos publicar. Diretamente relacionado com o banco de dados
		FeedType feedType;
};

// Declaração dos métodos inline

inline void NOFacebookFeed::setFeedTarget( const NOCustomer *pTarget )
{
	this->pFeedTarget = pTarget;
}

inline const NOCustomer* NOFacebookFeed::getFeedTarget( void ) const
{
	return pFeedTarget;
}

inline void NOFacebookFeed::setFeedType( NOFacebookFeed::FeedType type )
{
	feedType = type;
}

inline NOFacebookFeed::FeedType NOFacebookFeed::getFeedType( void ) const
{
	return feedType;
}

#endif
