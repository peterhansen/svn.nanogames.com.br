#include "NOProfileImportFacebookView.h"

// Components
#include "ObjcMacros.h"

// NanoOnline
#include "NOTextsIndexes.h"
#include "NOViewsIndexes.h"

// This code will not work until you enter your Facebook application's API key here:
static NSString* kApiKey = @"540f196e74004270cda15d85f497ff89"; // @"<YOUR API KEY>";

// Enter either your API secret or a callback URL (as described in documentation):
static NSString* kApiSecret = nil;//*/ @"3d043a478e1c7954a05f11a5729f85e0"; // @"<YOUR SECRET KEY>";
static NSString* kGetSessionProxy = @"http://localhost:3000/facebook/login";// */nil; // @"<YOUR SESSION CALLBACK)>";

// Macros auxiliares
#define ON_ERROR( wstdString ) [[NOControllerView sharedInstance] showError: wstdString]

// Extensão da classe para declarar métodos privados
@interface NOProfileImportFacebookView( Private )

// Inicializa a view
-( bool )buildNOProfileImportFacebookView;

// Libera a memória alocada pelo objeto
-( void )cleanNOProfileImportFacebookView;

@end

// Início da implementação da classe
@implementation NOProfileImportFacebookView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOProfileImportFacebookView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOProfileImportFacebookView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOProfileImportFacebookView
	Inicializa a view.

==============================================================================================*/

- ( bool )buildNOProfileImportFacebookView
{
	// Inicializa as variáveis da classe
	hFbSession = nil;

	// Determina o título da view
	[self setScreenTitle: [NOControllerView GetText: NO_TXT_FB_CONNECT]];
	
	{ // Evita erros de compilação por causa dos gotos
		
		if( [FBSession session] == nil )
		{
			// Aloca os elementos da view que não vão ser criados através do InterfaceBuilder
			if( kGetSessionProxy )
				hFbSession = [[FBSession sessionForApplication: kApiKey getSessionProxy: kGetSessionProxy delegate: self] retain];
			else
				hFbSession = [[FBSession sessionForApplication: kApiKey secret: kApiSecret delegate: self] retain];
			
			if( !hFbSession )
				goto Error;
			
			// TODOO : TESTE
			downloadedCustomer = NOCustomer(); 
			
			viewState = FBVIEW_STATE_CREATING_PROFILE;
		}
		else
		{
			hFbSession = [FBSession session];
			[[NOControllerView sharedInstance] getTempProfile: &downloadedCustomer];
			
			viewState == FBVIEW_STATE_PUBLISHING_FEED;
		}

		noListener.setCocoaListener( self );
//		feed = BrazookaRankingFeed( 10000LL, &downloadedCustomer );

		return true;
	
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
		[self cleanNOProfileImportFacebookView];
		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

-( void )awakeFromNib
{
	[super awakeFromNib];

	// Configura a imagem do botão "fshare". Ainda não descobrimos como acessar a imagem de um bundle
	// a partir do Interface Builder...
	UIImage *hImg = [UIImage imageNamed: @"FBConnect.bundle/images/share.png"];
	if( hImg )
	{
		[hShareButton setImage: hImg forState: UIControlStateNormal];
		
		// Se determinarmos as outras imagens, o cocoa pára de fazer os ajustes automáticos (como escurecer
		// a imagem quando pressionado)
//		[hShareButton setImage: hImg forState: UIControlStateDisabled];
//		[hShareButton setImage: hImg forState: UIControlStateSelected];
//		[hShareButton setImage: hImg forState: UIControlStateHighlighted];
	}
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//-( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self cleanNOProfileImportFacebookView];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM cleanNOProfileImportFacebookView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanNOProfileImportFacebookView
{
	// Acho que o autorelease de FBSession não está funcionando. Então, quando criamos
	// um objeto deste tipo através de sessionForApplication:secret:delegate:, ou de 
	// sessionForApplication:getSessionProxy:delegate: (como fazemos em buildNOProfileImportFacebookView),
	// a contagem de referências fica em 1. Como ainda utilizamos retain, o q seria correto
	// se o autorelease estivesse funcionando, a contagem de referências vai para 2. Assim
	// o método dealloc de FBSession nunca seria chamado, o que causaria vazamento de memória e outros bugs
	// de reutilização de objeto, já que FBSession é um singleton. Por isso utilizamos o for abaixo
	// para garantir esta desalocação
//	int32 total = [hFbSession retainCount];
//	for( int32 i = 0 ; i < total ; ++i )
//		[hFbSession release];
//
//	hFbSession = nil;
}

/*==============================================================================================

MENSAGEM askExtendedPermission:
	Exibe o diálogo que pede permissão extendida. Assim podemos acessar mais dados do usuário e
publicar feeds na sua wall.

	Para uma explicação mais detalhada do porquê deste método, acesse o link:
		- http://wiki.developers.facebook.com/index.php/Extended_permissions
	Lá também há uma lista com todos os possíveis valores de 'permission'.

===============================================================================================*/

-( IBAction )askExtendedPermission:( UIButton* )hButton
{
	FBPermissionDialog* hDialog = [[[FBPermissionDialog alloc] init] autorelease];
	hDialog.delegate = self;
	hDialog.permission = @"publish_stream";
	
	// TODOO: Desabilitar a interface da view atual toda vez que exibirmos um diálogo do facebook!!!!!!!!!
	[hDialog show];
}

/*==============================================================================================

MENSAGEM publishFeed:
	Publica um conteúdo na wall do usuário.

===============================================================================================*/

-( IBAction )publishFeed:( UIButton* )hButton
{
	if( ![hFbSession isConnected] )
		return;
/*
	if( !NOFacebookFeed::SendFbWallPublishRequest( &feed, &noListener ) )
	{
		NOString temp;
		[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_COULDNT_CREATE_REQUEST] toSTDString: temp];
		ON_ERROR( temp );
		return;
	}
*/		
	// Tudo foi OK, então apaga quaisquer indicações de erros e mostra a mensagem de "Por favor aguarde"
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	[hNOController hideError];
	[hNOController showWaitViewWithText: nil];
	
	// OLD : Agora fazemos tudo via servidor
//	if( true /* Usuário já forneceu permissão extendida */ )
//	{
//		FBStreamDialog* hDialog = [[[FBStreamDialog alloc] init] autorelease];
//		hDialog.delegate = self;
//		hDialog.userMessagePrompt = @"Say something to your friends";
//
//		// TODOO : Teste!!!!!!!!!!!
//		int64 points = 7000LL;
//		int32 rankPos = 22;
//
//		NSString *hName = @"Brazooka Soccer for iPhone";
//		NSString *hHRef = @"http://www.nanogames.com.br/brazooka_soccer.php";
//		NSString *hCaption = [NSString stringWithFormat: @"{*actor*} has made %lld points on Brazooka Soccer!!! Can you do better?", points, nil];
//
//		NOCustomer tempProfile;
//		[[NOControllerView sharedInstance] getTempProfile: &tempProfile];
//		
//		NSString *hGenderPronoum;
//		if( tempProfile.getGender() == GENDER_M )
//			hGenderPronoum = @"he";
//		else
//			hGenderPronoum = @"she";
//		NSString *hDescription = [NSString stringWithFormat: @"Now %@ is number %d on Brazooka Soccer worldwide online ranking", hGenderPronoum, rankPos, nil];
//		
//		NSString *hMediaLink = @"http://www.nanogames.com.br/imagens/brazooka/57x57.png";
//
//		hDialog.attachment = [NSString stringWithFormat: @"{\"name\":\"%s\",\"href\":\"%s\",\"caption\":\"%s\",\"description\":\"%s\",\"media\":[{\"type\":\"image\",\"src\":\"%s\",\"href\":\"%s\"}]}", NSSTRING_TO_CHAR_ARRAY( hName ), NSSTRING_TO_CHAR_ARRAY( hHRef ), NSSTRING_TO_CHAR_ARRAY( hCaption ), NSSTRING_TO_CHAR_ARRAY( hDescription ), NSSTRING_TO_CHAR_ARRAY( hMediaLink ), NSSTRING_TO_CHAR_ARRAY( hMediaLink ), nil];
//		hDialog.targetId = [NSString stringWithFormat: @"%lld", [hFbSession uid], nil];
//		
//		NSString *hActionLinkText = @"Download Brazooka Soccer";
//		NSString *hActionLinkHRef = @"http://itunes.apple.com/us/app/brazooka-soccer-br/id329184705?mt=8";
//		hDialog.actionLinks = [NSString stringWithFormat: @"[{\"text\":\"%s\",\"href\":\"%s\"}]", NSSTRING_TO_CHAR_ARRAY( hActionLinkText ), NSSTRING_TO_CHAR_ARRAY( hActionLinkHRef ), nil];
//		
//		LOG( ">>>>>>>>>>>> Attatchment %s\n", NSSTRING_TO_CHAR_ARRAY( hDialog.attachment ) );
//		LOG( ">>>>>>>>>>>> Action Link %s\n", NSSTRING_TO_CHAR_ARRAY( hDialog.actionLinks ) );
//		
//		// TODOO: Desabilitar a interface da view atual toda vez que exibirmos um diálogo do facebook!!!!!!!!!
//		[hDialog show];
//	}
//	else
//	{
//		[self askExtendedPermission: hButton];
//	}
}

/*==============================================================================================

FBDialogDelegate

===============================================================================================*/

/*==============================================================================================

MENSAGEM dialog:didFailWithError:

===============================================================================================*/

-( void )dialog:( FBDialog* )hDialog didFailWithError:( NSError* )error
{
	//hLabel.text = [NSString stringWithFormat: @"Error(%d) %@", error.code, error.localizedDescription];
	
	// Mostra um popup
	// TODOO
}

/*==============================================================================================

FBSessionDelegate

===============================================================================================*/

/*==============================================================================================

MENSAGEM session:didLogin:

	Obtém do Facebook os dados do usuário relevantes para o NanoOnline.
	 
	A relação completa dos campos que podem ser obtidos da tabela Users pode ser encontrada em:
		- http://wiki.developers.facebook.com/index.php/User_(FQL)
	 
	A lista completa de tabelas está em:
		- http://wiki.developers.facebook.com/index.php/FQL_Tables
	 
	Os campos atualmente relevantes são:

		- uid				int64		The user ID of the user being queried.
		- first_name		string		The first name of the user being queried. 	
		- last_name			string		The last name of the user being queried. 	
		- name				string		The full name of the user being queried. 
		- username			string		The username of the user being queried. 
		- birthday_date		string		The birthday of the user being queried, rendered as a machine-readable string. The format of this date never changes. 
		- sex				string		The sex of the user being queried. Starting February 7, 2010, this field will only return results in English.
 
===============================================================================================*/

-( void )session:( FBSession* )hSession didLogin:( FBUID )uid
{
	NOString sessionKey, sessionSecret;
	[NOControllerView ConvertNSString: [hSession sessionKey] toSTDString: sessionKey];
	[NOControllerView ConvertNSString: [hSession sessionSecret] toSTDString: sessionSecret];
	downloadedCustomer.setFBData( uid, sessionKey, sessionSecret );

	if( !NOCustomer::SendFbDataDownloadRequest( &downloadedCustomer, &noListener ) )
	{
		NOString temp;
		[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_COULDNT_CREATE_REQUEST] toSTDString: temp];
		ON_ERROR( temp );
		return;
	}
		
	// Tudo foi OK, então apaga quaisquer indicações de erros e mostra a mensagem de "Por favor aguarde"
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	[hNOController hideError];
	[hNOController showWaitViewWithText: nil];
}

-( void )sessionDidNotLogin:( FBSession* )hSession
{
	// Mostra um popup
	// TODOO
}

-( void )sessionDidLogout:( FBSession* )hSession
{
	if( [hFbSession isConnected] )
	{
		// Mostra um popup
		// TODOO
	}
}

/*==============================================================================================

FBRequestDelegate

===============================================================================================*/

-( void )request:( FBRequest* )request didLoad:( id )result
{
	// OLD : Agora fazemos tudo via servidor
//	[[NOControllerView sharedInstance] hideWaitView];
//	
//	if( [request.method isEqualToString: @"facebook.fql.query"] )
//	{
//		NSArray* users = result;
//		NSDictionary* user = [users objectAtIndex:0];
//		
//#if DEBUG
//		int32 count = [user count];
//		LOG( ">>>> Keys Retrieved = %d", count );
//
//		NSArray *hKeys = [user allKeys];
//		for( NSString *hKey in hKeys )
//			LOG( ">>>> %s\n", NSSTRING_TO_CHAR_ARRAY( hKey ) );
//#endif
//
//		// Separa os dados recebidos
//		NSString* firstName = [user objectForKey:@"first_name"];
//		NSString* lastName = [user objectForKey:@"last_name"];
//		NSString* name = [user objectForKey:@"name"];
//		NSString* username = [user objectForKey:@"username"];
//		NSString* birthday = [user objectForKey:@"birthday_date"];
//		NSString* sex = [user objectForKey:@"sex"];
//
//		// Preenche o perfil com os dados obtidos do facebook
//		NOString aux, formatError;
//		NOCustomer tempProfile;
//		uint32 min, max;
//		
//		bool hasFirstname = false;
//		
//		if( (( NSNull* )firstName ) != [NSNull null] )
//		{
//			[NOControllerView ConvertNSString: firstName toSTDString: aux];
//			
//			if( !aux.empty() )
//			{
//				hasFirstname = true;
//
//				NOCustomer::GetFirstNameSupportedLen( min, max );
//				NOString cropped = aux.substr( 0, max );
//				tempProfile.setFirstName( cropped, formatError );
//			}
//		}
//		
//		if( (( NSNull* )lastName ) != [NSNull null] )
//		{
//			[NOControllerView ConvertNSString: lastName toSTDString: aux];
//			
//			if( !aux.empty() )
//			{
//				NOCustomer::GetLastNameSupportedLen( min, max );
//				NOString cropped = aux.substr( 0, max );
//				tempProfile.setLastName( cropped, formatError );
//			}
//		}
//		
//		if( (( NSNull* )username ) != [NSNull null] )
//		{
//			[NOControllerView ConvertNSString: username toSTDString: aux];
//		}
//		else
//		{
//			if( (( NSNull* )name ) != [NSNull null] )
//				[NOControllerView ConvertNSString: name toSTDString: aux];
//			else if( hasFirstname )
//				[NOControllerView ConvertNSString: firstName toSTDString: aux];
//		}
//		if( !aux.empty() )
//		{
//			// Cropa nos limites do NanoOnline
//			NOCustomer::GetNicknameSupportedLen( min, max );
//			NOString cropped = aux.substr( 0, max );
//			tempProfile.setNickname( cropped, formatError );
//		}
//
//		if( [sex compare: @"male"] == NSOrderedSame )
//			tempProfile.setGender( GENDER_M );
//		else
//			tempProfile.setGender( GENDER_F );
//		
//		if( (( NSNull* )birthday ) != [NSNull null] )
//		{
//			NSArray *hDataComponents = [birthday componentsSeparatedByString: @"/"];
//			if( [hDataComponents count] > 1 ) // Pode vir só dia e mês...
//			{
//				int32 year;
//				int8 month = [(( NSString* )[hDataComponents objectAtIndex: 0]) intValue];
//				int8 day = [(( NSString* )[hDataComponents objectAtIndex: 1]) intValue];
//				
//				if( [hDataComponents count] > 2 )
//					year = [(( NSString* )[hDataComponents objectAtIndex: 2]) intValue];
//
//				tempProfile.setBirthday( day, month, year, formatError, true );
//			}
//		}
//		
//		NOString stdSessionKey, stdSessionSecret;
//		[NOControllerView ConvertNSString: hFbSession.sessionKey toSTDString: stdSessionKey];
//		[NOControllerView ConvertNSString: hFbSession.sessionSecret toSTDString: stdSessionSecret];
//		tempProfile.setFBData( hFbSession.uid, stdSessionKey, stdSessionSecret );
//
//		NOControllerView *hController = [NOControllerView sharedInstance];
//		[hController setTempProfile: &tempProfile];
//
//		// TODOO : Cancelar realmente???
////		if( [hFbSession isConnected] )
////			[hFbSession logout];
//
//		// Muda de tela
//		[hController performTransitionToView: NO_VIEW_INDEX_PROFILE_CREATE_WITH_FB];
//	}
//	else
//	{
//		LOG( "Request method = %s\n", NSSTRING_TO_CHAR_ARRAY( request.method ) );
//	}
}

-( void )request:( FBRequest* )request didFailWithError:( NSError* )error
{
	//hLabel.text = [NSString stringWithFormat: @"Error(%d) %@", error.code, error.localizedDescription];
	
	// Mostra um popup
	// TODOO
}

/*==============================================================================================

MENSAGEM onNOSuccessfulResponse
	Indica que uma requisição foi respondida e terminada com sucesso.

===============================================================================================*/

- ( void )onNOSuccessfulResponse
{
	[[NOControllerView sharedInstance] hideWaitView];

	// TODOO : Cancelar realmente???
//	// Cancela a sessão
//	if( [hFbSession isConnected] )
//		[hFbSession logout];
	
	// Muda de tela
	if( viewState == FBVIEW_STATE_CREATING_PROFILE )
	{
		NOControllerView *hController = [NOControllerView sharedInstance];
		[hController setTempProfile: &downloadedCustomer];
		
		[hController performTransitionToView: NO_VIEW_INDEX_PROFILE_CREATE_WITH_FB];
	}
}

/*==============================================================================================

MENSAGEM onNOError:WithErrorDesc:
	Sinaliza erros ocorridos nas operações do NanoOnline.

===============================================================================================*/

-( void ) onNOError:( NOErrors )errorCode WithErrorDesc:( NSString* )hErrorDesc
{
	// TODOO : Cancelar realmente???
	// Cancela a sessão
//	if( [hFbSession isConnected] )
//		[hFbSession logout];
	
	[[NOControllerView sharedInstance] hideWaitView];

	NOString aux;
	[NOControllerView ConvertNSString: hErrorDesc toSTDString: aux];

	ON_ERROR( aux );
}

/*==============================================================================================

MENSAGEM onNORequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

===============================================================================================*/

-( void )onNORequestCancelled
{
	// TODOO : Cancelar realmente???
	// Cancela a sessão
//	if( [hFbSession isConnected] )
//		[hFbSession logout];
	
	[[NOControllerView sharedInstance] hideWaitView];
}

/*==============================================================================================

MENSAGEM onNOProgressChangedTo:ofTotal:
	Indica o progresso da requisição atual.

===============================================================================================*/

-( void )onNOProgressChangedTo:( int32 )currBytes ofTotal:( int32 )totalBytes
{
	[[NOControllerView sharedInstance] setProgress: static_cast< float >( currBytes ) / totalBytes];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	// TODOO
	switch( viewState )
	{
		case FBVIEW_STATE_UNDEFINED:
			return;
			
		case FBVIEW_STATE_CREATING_PROFILE:
			break;
			
		case FBVIEW_STATE_PUBLISHING_FEED:
			break;
	}
}
	
// Fim da implementação da classe
@end
