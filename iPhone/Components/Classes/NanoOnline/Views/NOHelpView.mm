#include "NOHelpView.h"

// NanoOnline
#include "NOTextsIndexes.h"

// Extensão da classe para declarar métodos privados
@interface NOHelpView ( Private )

// Inicializa a view
-( bool )buildNOHelpView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOHelpView;

@end

// Início da implementação da classe
@implementation NOHelpView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOHelpView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOHelpView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOHelpView
	Inicializa a view.

==============================================================================================*/

- ( bool )buildNOHelpView
{
	// Determina o título da tela
	[self setScreenTitle: [NOControllerView GetText: NO_TXT_HELP]];
	return true;

//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view
//		// ...
//
//		return true;
//	
//	} // Evita erros de compilação por causa dos gotos
//	
//	// Label de tratamento de erros
//	Error:
//		[self cleanNOHelpView];
//		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	[hTxtHelp setText: [NOControllerView GetText: NO_TXT_NANO_ONLINE_HELP]];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self cleanNOHelpView];
//  [super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNOHelpView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOHelpView
//{
//}

// Fim da implementação da classe
@end
