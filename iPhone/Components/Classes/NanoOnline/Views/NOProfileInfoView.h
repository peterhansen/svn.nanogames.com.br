/*
 *  NOProfileInfoView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/25/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_PROFILE_CREATE_VIEW_H
#define NO_PROFILE_CREATE_VIEW_H

// Componentes
#include "NOCustomer.h"
#include "NOBaseView.h"
#include "NOListenerCocoaBridge.h"

// Declarações adiadas
@class UICustomSwitch;
@class UILimitedTextField;

enum NOProfileInfoViewMode
{
	PROFILE_INFO_VIEW_MODE_UNDEFINED	= 0,
	PROFILE_INFO_VIEW_MODE_CREATING		= 1,
	PROFILE_INFO_VIEW_MODE_EDITING		= 2
};

@interface NOProfileInfoView : NOBaseView <NOCocoaListener>
{
	@private
		NOCustomer currCustomer;
		NOListenerCocoaBridge noListener;

		// Modo de visualização da tela
		NOProfileInfoViewMode viewMode;
	
		// Estado do modo de visualização da tela
		int8 viewState;

		// Informações da data de aniversário
		int8 inputDay;
		int8 inputMonth;
		int32 inputYear;

		// Elementos de interface contidos na view
		IBOutlet UILimitedTextField *hTbNickname;
		IBOutlet UILimitedTextField *hTbEmail;
		IBOutlet UILimitedTextField *hTbPassword;
		IBOutlet UILimitedTextField *hTbPasswordConfirm;
		IBOutlet UILimitedTextField *hTbLastName;
		IBOutlet UILimitedTextField *hTbFirstName;
	
		IBOutlet UICustomSwitch *hSwGender;
	
		IBOutlet UILimitedTextField *hTbBirthdayDay;
		IBOutlet UILimitedTextField *hTbBirthdayMonth;
		IBOutlet UILimitedTextField *hTbBirthdayYear;
	
		IBOutlet UIImageView *hImgAvatar;

		IBOutlet UIButton *hBtOk;
	
		// Para conseguirmos traduzir os textos sem termos que criar
		// outros xibs
		IBOutlet UILabel *hLbNickname;
		IBOutlet UILabel *hLbGender;
		IBOutlet UILabel *hLbPassword;
		IBOutlet UILabel *hLbPasswordConfirm;
		IBOutlet UILabel *hLbEmail;
		IBOutlet UILabel *hLbFirstName;
		IBOutlet UILabel *hLbLastName;
		IBOutlet UILabel *hLbBirthday;
}

@property ( readwrite, nonatomic, assign, getter = getViewMode, setter = setViewMode )NOProfileInfoViewMode viewMode;

// Este método deve ser chamado antes de a view ser exibida. Ele prrenche os campos do formulário
// com os dados do perfil passado como parâmetro
-( void )fillFormFieldsWithProfile:( const NOCustomer* )pProfileToEdit;

// Método chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

// Método chamado quando o usuário altera o valor da switch de sexo
-( IBAction )onSwValueChanged;

// Verifica se os números informados na data de nascimento são válidos
-( IBAction )onBirthdayChanged:( UILimitedTextField* )hTxtField;

@end

#endif
