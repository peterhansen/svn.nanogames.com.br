/*
 *  NORankingView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_RANKING_VIEW_H
#define NANO_ONLINE_RANKING_VIEW_H

// Components
#include "NOBaseView.h"
#include "NOListenerCocoaBridge.h"
#include "NOMap.h"
#include "NORanking.h"

enum NORankingViewMode
{
	RANKING_VIEW_MODE_UNDEFINED	= 0,
	RANKING_VIEW_MODE_LOCAL		= 1,
	RANKING_VIEW_MODE_GLOBAL	= 2
};

@interface NORankingView : NOBaseView <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate, NOCocoaListener>
{
	@private
		NOListenerCocoaBridge noListener;

		// Tabela que exibe os recordes do ranking
		IBOutlet UIView *hTableBorder;
		IBOutlet UITableView* hRankingsTable;

		// Outros elementos da interface
		IBOutlet UIImageView *hGoldMedal, *hSilverMedal, *hBronzeMedal;
		IBOutlet UITextField *h1stNick, *h1stPoints; 
		IBOutlet UITextField *h2ndNick, *h2ndPoints;
		IBOutlet UITextField *h3rdNick, *h3rdPoints;

		// Botão que ativa a ação desta tela
		IBOutlet UIButton *hBtAction;
	
		// Botões que funcionam apenas no modo RANKING_VIEW_MODE_LOCAL
		IBOutlet UIButton *hBtDelete, *hBtToggleChecks;
	
		// Indica, no modo RANKING_VIEW_MODE_LOCAL, o estado de cada checkbox
		NSMutableDictionary *hCkbStateDict;
	
		// Imagens da checkbox
		UIImage *hImgChecked, *hImgUnChecked;

		// OLD
//		// Configurações das seções da tabela
//		struct TableSection
//		{
//			TableSection( void ) : nRows( 0 ), headerStr(), footerStr() {}
//			TableSection( NSInteger nRows, const std::string& headerStr, const std::string& footerStr = std::string() ) : nRows( nRows ), headerStr( headerStr ), footerStr( footerStr ) {}
//			
//			void set( NSInteger nRows, const std::string& headerStr, const std::string& footerStr = std::string() )
//			{
//				this->nRows = nRows;
//				this->headerStr = headerStr;
//				this->footerStr = footerStr;
//			}
//			
//			NSInteger nRows;
//			std::string headerStr;
//			std::string footerStr;
//		};
//	
//		std::map< NSInteger, TableSection > tableSections;
	
		// Dados do ranking
		NORanking* pRanking;
	
		// Ranking auxiliar que contém apenas as pontuações que serão submetidas
		NORanking auxRanking;
	
		// Auxiliares de formatação
		uint32 nickMaxLen;
		uint8 maxPointsLen;
		uint8 nDecimalSeparators;
		uint8 nRecordsLen;
	
		// Controla a máquina de estados da view
		int8 viewState;
	
		// Indica qual o modo de interação da view que está sendo utilizado
		NORankingViewMode viewMode;
}

// Inicializa a view com os dados do ranking
-( void )initViewInMode:( NORankingViewMode )viewMode forRanking:( NORanking* )pRnk;

// Método chamado quando o botão de ação deste modo de visualização é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

@end

#endif
