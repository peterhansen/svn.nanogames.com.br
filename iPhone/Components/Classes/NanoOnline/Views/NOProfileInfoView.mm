#include "NOProfileInfoView.h"

// Components
#include "ObjcMacros.h"
#include "UICustomSwitch.h"
#include "UILimitedTextField.h"

// Components - NanoOnline
#include "NOTextsIndexes.h"
#include "NOViewsIndexes.h"

// Definições dos botões do popup
#define POPUP_YES_BT_INDEX	0
#define POPUP_NO_BT_INDEX	1

#define POPUP_OK_BT_INDEX	2

// Estados da view
#define NOPROFVIEW_STATE_UNDEFINED			-1
#define NOPROFVIEW_CREATE_STATE_IDLE		 0
#define NOPROFVIEW_EDIT_STATE_IDLE			 1
#define NOPROFVIEW_EDIT_STATE_SYNCH			 2
#define NOPROFVIEW_EDIT_STATE_AFTER_SYNCH	 3

// Macros auxiliares
#define ON_ERROR( wstdString ) [[NOControllerView sharedInstance] showError: wstdString]

#define RETURN_IF_ERROR( wstdString )		\
		if( !error.empty() )				\
		{									\
			ON_ERROR( wstdString );			\
			return;							\
		}

// Extensão da classe para declarar métodos privados
@interface NOProfileInfoView ( Private )

// Inicializa o objeto
-( bool )buildNOProfileInfoView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOProfileInfoView;

@end

// Início da implementação da classe
@implementation NOProfileInfoView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize viewMode;

-( void )setViewMode:( NOProfileInfoViewMode )mode
{
	viewMode = mode;

	NSString* hBtTitle;
	switch( viewMode )
	{
		case PROFILE_INFO_VIEW_MODE_CREATING:
			hBtTitle = [NOControllerView GetText: NO_TXT_CREATE];
			break;
			
		case PROFILE_INFO_VIEW_MODE_EDITING:
			hBtTitle = [NOControllerView GetText: NO_TXT_EDIT];
			break;
	}
	
	[self setScreenTitle: hBtTitle];

	[hBtOk setTitle: hBtTitle forState: UIControlStateNormal];
	[hBtOk setTitle: hBtTitle forState: UIControlStateHighlighted];
	[hBtOk setTitle: hBtTitle forState: UIControlStateDisabled];
	[hBtOk setTitle: hBtTitle forState: UIControlStateSelected];
}

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOProfileInfoView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOProfileInfoView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOProfileInfoView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNOProfileInfoView
{
	// Inicializa as variáveis da classe
	hTbNickname = nil;
	hTbEmail = nil;
	hTbPassword = nil;
	hTbLastName = nil;
	hTbFirstName = nil;
	hSwGender = nil;
	hImgAvatar = nil;
	hBtOk = nil;
	
	inputDay = -1;
	inputMonth = -1;
	inputYear = -1;

	currCustomer = NOCustomer();
	noListener.setCocoaListener( self );
	
	viewMode = PROFILE_INFO_VIEW_MODE_UNDEFINED;
	viewState = NOPROFVIEW_STATE_UNDEFINED;
	
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Configura os elementos da view que foram criados pelo InterfaceBuilder

	// Determina os valores da switch
	// OBS: Os caracteres de espaço servem para centralizarmos as letras 'M' e 'F' na imagem da switch
	[hSwGender setLeftLabelText: [NOControllerView GetText: NO_TXT_GENDER_M]];
	[hSwGender setRightLabelText: [NOControllerView GetText: NO_TXT_GENDER_F] ];

	// Determina os limites dos campos de texto
	uint32 minTextFieldLen, maxTextFieldLen;
	NOCustomer::GetNicknameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbNickname setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];
	
	NOCustomer::GetFirstNameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbFirstName setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetLastNameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbLastName setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetPasswordSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbPassword setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetEmailSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbEmail setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	[hTbBirthdayDay setMinLimit: 2 AndMaxLimit: 2];
	[hTbBirthdayMonth setMinLimit: 2 AndMaxLimit: 2];
	[hTbBirthdayYear setMinLimit: 4 AndMaxLimit: 4];
	
	[hLbNickname setText: [NOControllerView GetText: NO_TXT_NICKNAME]];
	[hLbGender setText: [NOControllerView GetText: NO_TXT_GENDER]];
	[hLbPassword setText: [NOControllerView GetText: NO_TXT_PASSWORD]];
	[hLbPasswordConfirm setText: [NOControllerView GetText: NO_TXT_PASSWORD_CONFIRM]];
	[hLbEmail setText: [NOControllerView GetText: NO_TXT_EMAIL]];
	[hLbFirstName setText: [NOControllerView GetText: NO_TXT_FIRST_NAME]];
	[hLbLastName setText: [NOControllerView GetText: NO_TXT_LAST_NAME]];
	[hLbBirthday setText: [NOControllerView GetText: NO_TXT_BIRTHDAY]];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self cleanNOProfileInfoView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNOProfileInfoView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOProfileInfoView
//{
//}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	if( viewMode == PROFILE_INFO_VIEW_MODE_EDITING )
	{
		viewState = NOPROFVIEW_EDIT_STATE_SYNCH;

		if( NOCustomer::SendDownloadRequest( &currCustomer, &noListener ) )
		{
			[[NOControllerView sharedInstance] showWaitViewWithText: [NOControllerView GetText: NO_TXT_SYNCH_W_SERVER]];
		}
		else
		{
			// OLD
			//[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server. Changes made to your profile on other devices won't apply here. Continue anyway?" CancelBtIndex: POPUP_NO_BT_INDEX AndBts: @"Yes", @"No", nil];
			
			[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_COULDNT_SYNCH] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];			
		}
	}
	else
	{
		viewState = NOPROFVIEW_CREATE_STATE_IDLE;
	}
}

/*==============================================================================================

MENSAGEM fillFormFieldsWithProfile:
	Este método deve ser chamado antes de a view ser exibida. Ele prrenche os campos do formulário
com os dados do perfil passado como parâmetro.
 
===============================================================================================*/

-( void )fillFormFieldsWithProfile:( const NOCustomer* )pProfileToEdit
{
	if( pProfileToEdit )
	{
		currCustomer = *pProfileToEdit;

		NSString *hAux;

		NOString aux;
		currCustomer.getNickname( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbNickname setText: hAux];
		
		currCustomer.getFirstName( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbFirstName setText: hAux];

		currCustomer.getLastName( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbLastName setText: hAux];

		currCustomer.getEmail( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbEmail setText: hAux];
		
		currCustomer.getPassword( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbPassword setText: hAux];
		[hTbPasswordConfirm setText: [hTbPassword text]];
		
		if( currCustomer.getGender() == GENDER_F )
			[hSwGender setOn: NO];
		else
			[hSwGender setOn: YES];
		
		[self onSwValueChanged];
		
		currCustomer.getBirthday( &inputDay, &inputMonth, &inputYear );
		
		if( ( inputDay != -1 ) && ( inputMonth != -1 ) && ( inputYear != -1 ) )
		{
			#define PROFILE_EDIT_BUF_SIZE 8

			char buffer[ PROFILE_EDIT_BUF_SIZE ];
			snprintf( buffer, PROFILE_EDIT_BUF_SIZE, "%02d", static_cast< int32 >( inputDay ) );
			[hTbBirthdayDay setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
			
			snprintf( buffer, PROFILE_EDIT_BUF_SIZE, "%02d", static_cast< int32 >( inputMonth ) );
			[hTbBirthdayMonth setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
			
			snprintf( buffer, PROFILE_EDIT_BUF_SIZE, "%4d", inputYear );
			[hTbBirthdayYear setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
			
			[self onBirthdayChanged: hTbBirthdayYear];

			#undef PROFILE_EDIT_BUF_SIZE
		}
	}
}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.
 
===============================================================================================*/
		
-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( hButton == hBtOk )
	{
//		#if DEBUG
//		
//				NOString error;
//				currCustomer.setNickname( L"aマリオ", error );
//		
//				currCustomer.setFirstName( L"Super", error );
//				currCustomer.setLastName( L"Mario", error );
//				currCustomer.setPassword( L"mario123", error );
//				currCustomer.setEmail( L"mario@nanogames.com.br", error );
//				currCustomer.setGender( GENDER_M );
//		#else
				NOString error, aux;
				
				[NOControllerView ConvertNSString: [hTbNickname text] toSTDString: aux];
				currCustomer.setNickname( aux, error );
				RETURN_IF_ERROR( error );

				[NOControllerView ConvertNSString: [hTbFirstName text] toSTDString: aux];
				currCustomer.setFirstName( aux, error );
				RETURN_IF_ERROR( error );

				[NOControllerView ConvertNSString: [hTbLastName text] toSTDString: aux];
				currCustomer.setLastName( aux, error );
				RETURN_IF_ERROR( error );
				
				[NOControllerView ConvertNSString: [hTbEmail text] toSTDString: aux];
				currCustomer.setEmail( aux, error );
				RETURN_IF_ERROR( error );
				
				[NOControllerView ConvertNSString: [hTbPassword text] toSTDString: aux];
				currCustomer.setPassword( aux, error );
				RETURN_IF_ERROR( error );
				
				if( [[hTbPassword text] compare: [hTbPasswordConfirm text]] != NSOrderedSame )
				{
					[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_PASSWORD_DIFFS_FROM_CONFIRM] toSTDString: error];
					RETURN_IF_ERROR( error );
				}
	
				if( ( inputDay == -1 ) || ( inputMonth == -1 ) || ( inputYear == -1 ) )
				{
					[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_BIRTHDAY_MANDATORY] toSTDString: error];
					RETURN_IF_ERROR( error );
				}
					
				if( ( inputDay != -1 ) && ( inputMonth != -1 ) && ( inputYear != -1 ) )
				{
					currCustomer.setBirthday( inputDay, inputMonth, inputYear, error );
					RETURN_IF_ERROR( error );
				}
				
				currCustomer.setGender( [hSwGender isOn] == YES ? GENDER_M : GENDER_F );
//		#endif

		bool ret = false;
		if( viewMode == PROFILE_INFO_VIEW_MODE_CREATING )
			ret = NOCustomer::SendCreateRequest( &currCustomer, &noListener );
		else if( viewMode == PROFILE_INFO_VIEW_MODE_EDITING )
			ret = NOCustomer::SendEditRequest( &currCustomer, &noListener );

		if( !ret )
		{
			NOString temp;
			[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_COULDNT_CREATE_REQUEST] toSTDString: temp];
			ON_ERROR( temp );
			return;
		}
		
		// Tudo foi OK, então apaga quaisquer indicações de erros e mostra a mensagem de "Por favor aguarde"
		NOControllerView *hNOController = [NOControllerView sharedInstance];
		[hNOController hideError];
		[hNOController showWaitViewWithText: nil];
	}
}

/*==============================================================================================

MENSAGEM onSwValueChanged
	Método chamado quando o usuário altera o valor da switch de sexo.

===============================================================================================*/

-( IBAction )onSwValueChanged
{
	[hImgAvatar setImage: [NOControllerView getDefaultAvatarForGender: ( [hSwGender isOn] ? GENDER_M : GENDER_F ) ]];
}

/*==============================================================================================

MENSAGEM onBirthdayChanged
	Verifica se os números informados na data de nascimento são válidos.

===============================================================================================*/

-( IBAction )onBirthdayChanged:( UILimitedTextField* )hTxtField
{
	inputDay = static_cast< int8 >( strtol( NSSTRING_TO_CHAR_ARRAY( [hTbBirthdayDay text] ), static_cast< char** >( NULL ), 10 ) );
	inputMonth = static_cast< int8 >( strtol( NSSTRING_TO_CHAR_ARRAY( [hTbBirthdayMonth text] ), static_cast< char** >( NULL ), 10 ) );
	inputYear = static_cast< int32 >( strtol( NSSTRING_TO_CHAR_ARRAY( [hTbBirthdayYear text] ), static_cast< char** >( NULL ), 10 ) );
	
	NOString error;
	if( !currCustomer.setBirthday( inputDay, inputMonth, inputYear, error ))
	{
		[hTbBirthdayDay setTextColor: [UIColor redColor]];
		[hTbBirthdayMonth setTextColor: [UIColor redColor]];
		[hTbBirthdayYear setTextColor: [UIColor redColor]];
		
		// Iremos informar o erro explicitamente apenas na hora da submissão
		//ON_ERROR( error );
	}
	else
	{
		[hTbBirthdayDay setTextColor: [UIColor blackColor]];
		[hTbBirthdayMonth setTextColor: [UIColor blackColor]];
		[hTbBirthdayYear setTextColor: [UIColor blackColor]];
	}
}

/*==============================================================================================

MENSAGEM onNOSuccessfulResponse
	Indica que uma requisição foi respondida e terminada com sucesso.

===============================================================================================*/

- ( void )onNOSuccessfulResponse
{
	[[NOControllerView sharedInstance] hideWaitView];

	switch( viewMode )
	{
		case PROFILE_INFO_VIEW_MODE_CREATING:
			// Salva o perfil localmente
			if( [NOControllerView insertProfile: currCustomer] == FS_OK )
			{
				// Atualiza o perfil que está sendo manipulado
				[[NOControllerView sharedInstance] setTempProfile: &currCustomer];
				
				// Mostra o popup de feedback
				[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_PROFILE_CREATED] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
			}
			else
			{
				// Opa! Não conseguiu salvar localmente... Melhor abortar...
				NOString error;
				[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_COULDNT_SAVE_PROFILE] toSTDString: error];
				ON_ERROR( error );
			}
			break;
			
		case PROFILE_INFO_VIEW_MODE_EDITING:
			// Edita o perfil localmente
			if( [NOControllerView updateProfile: currCustomer] == FS_OK )
			{
				// Atualiza o perfil que está sendo manipulado
				[[NOControllerView sharedInstance] setTempProfile: &currCustomer];
				
				if( viewState == NOPROFVIEW_EDIT_STATE_IDLE )
				{
					[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_PROFILE_EDITTED] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
				}
				else
				{
					viewState = NOPROFVIEW_EDIT_STATE_AFTER_SYNCH;
					[self fillFormFieldsWithProfile: &currCustomer];
					[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_PROFILE_SYNCHED] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];	
				}
			}
			else
			{
				if( viewState == NOPROFVIEW_EDIT_STATE_SYNCH )
				{
					// OLD
					//[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server. Changes made to your profile on other devices won't apply here. Continue anyway?" CancelBtIndex: POPUP_NO_BT_INDEX AndBts: @"Yes", @"No", nil];
					
					[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_COULDNT_SYNCH] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
				}
				else
				{
					// Opa! Não conseguiu editar localmente... Melhor abortar...
					NOString error;
					[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_COULDNT_SAVE_PROFILE] toSTDString: error];
					ON_ERROR( error );
				}
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário pressiona um dos botões do popup.

================================================================================================*/

-( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	switch( viewMode )
	{
		case PROFILE_INFO_VIEW_MODE_CREATING:
			[hNOController setHistoryAsShortestWayToView:NO_VIEW_INDEX_PROFILE_SELECT];
			[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_SELECT];
			break;

		case PROFILE_INFO_VIEW_MODE_EDITING:
			switch( viewState )
			{
				case NOPROFVIEW_EDIT_STATE_AFTER_SYNCH:
					viewState = NOPROFVIEW_EDIT_STATE_IDLE;
					break;

				case NOPROFVIEW_EDIT_STATE_SYNCH:
					// OLD
//					if( buttonIndex == POPUP_YES_BT_INDEX )
//					{
//						viewState = NOPROFVIEW_EDIT_STATE_IDLE;
//						return;
//					}
					
					// Sem break mesmo
					
				case NOPROFVIEW_EDIT_STATE_IDLE:
					[hNOController setHistoryAsShortestWayToView:NO_VIEW_INDEX_PROFILE_SELECT];
					[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_SELECT];
					break;
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM onNOError:WithErrorDesc:
	Sinaliza erros ocorridos nas operações do NanoOnline.

===============================================================================================*/

-( void ) onNOError:( NOErrors )errorCode WithErrorDesc:( NSString* )hErrorDesc
{
	[[NOControllerView sharedInstance] hideWaitView];

	if( ( viewMode == PROFILE_INFO_VIEW_MODE_EDITING ) && ( viewState == NOPROFVIEW_EDIT_STATE_SYNCH ) )
	{
		// OLD
		//[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server. Changes made to your profile on other devices won't apply here. Continue anyway?" CancelBtIndex: POPUP_NO_BT_INDEX AndBts: @"Yes", @"No", nil];
		
		[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_COULDNT_SYNCH] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
	}
	else
	{
		NOString aux;
		[NOControllerView ConvertNSString: hErrorDesc toSTDString: aux];

		ON_ERROR( aux );
	}
}

/*==============================================================================================

MENSAGEM onNORequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

===============================================================================================*/

-( void )onNORequestCancelled
{
	[[NOControllerView sharedInstance] hideWaitView];
	
	if( ( viewMode == PROFILE_INFO_VIEW_MODE_EDITING ) && ( viewState == NOPROFVIEW_EDIT_STATE_SYNCH ) )
	{
		// Sai da tela
		
		// OLD
		//[self alertView: nil clickedButtonAtIndex: POPUP_NO_BT_INDEX];
		
		[self alertView: nil clickedButtonAtIndex: POPUP_OK_BT_INDEX];
	}
}

/*==============================================================================================

MENSAGEM onNOProgressChangedTo:ofTotal:
	Indica o progresso da requisição atual.

===============================================================================================*/

-( void )onNOProgressChangedTo:( int32 )currBytes ofTotal:( int32 )totalBytes
{
	[[NOControllerView sharedInstance] setProgress: static_cast< float >( currBytes ) / totalBytes];
}

// Fim da implementação da classe
@end

