/*
 *  NOProfileImportFacebookView.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/18/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_IMPORT_FROM_FB_VIEW_H
#define NANO_ONLINE_IMPORT_FROM_FB_VIEW_H

// Components
#include "NOBaseView.h"
#include "NOListenerCocoaBridge.h"

// Facebook API
#import "FBConnect/FBConnect.h"

// Game
//#include "BrazookaRankingFeed.h"

enum FacebookViewState
{
	FBVIEW_STATE_UNDEFINED,
	FBVIEW_STATE_CREATING_PROFILE,
	FBVIEW_STATE_PUBLISHING_FEED
};

// Forward Declarations
@class FBSession;

@interface NOProfileImportFacebookView : NOBaseView < FBDialogDelegate, FBSessionDelegate, FBRequestDelegate, NOCocoaListener >
{
	@private
		NOCustomer downloadedCustomer;
		NOListenerCocoaBridge noListener;
//		BrazookaRankingFeed feed;
	
		// Estado da view
		FacebookViewState viewState;
	
		// Conexão com o Facebook
		FBSession *hFbSession;
	
		// Elementos da interface
		IBOutlet FBLoginButton *hLoginButton;
		IBOutlet UIButton *hShareButton;
		IBOutlet UIButton *hPublishFeed;
}

// Exibe o diálogo que pede permissão extendida. Assim podemos acessar mais dados do usuário e publicar feeds na sua wall
-( IBAction )askExtendedPermission:( UIButton* )hButton;

// Publica um conteúdo na wall do usuário
-( IBAction )publishFeed:( UIButton* )hButton;

@end

#endif
