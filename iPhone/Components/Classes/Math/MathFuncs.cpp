#include "MathFuncs.h"

/*==============================================================================================

MÉTODO nextMulOf
	Retorna o primeiro múltiplo de m no intervalo [n, MAX_INT].

==============================================================================================*/

int32 NanoMath::nextMulOf( int32 n, int32 m )
{
	int32 aux = n % m;
	return aux == 0 ? n : n + ( m - aux );
}
