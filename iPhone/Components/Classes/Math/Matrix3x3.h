/*
 *  Matrix3x3.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef MATRIX3X3_H
#define MATRIX3X3_H 1

#include "NanoTypes.h"

struct Matrix3x3
{
	// Assim podemos acessar os dados do ponto através de seus valores individuais ou como
	// um array
	union
	{
        struct
		{
            float _00, _01, _02;
            float _10, _11, _12;
            float _20, _21, _22;
        };
        float m[3][3];
    };

	// Construtores
    Matrix3x3( void );
	explicit Matrix3x3( float value );
    explicit Matrix3x3( const float* pElements );
    Matrix3x3( float _00, float _01, float _02,
			   float _10, float _11, float _12,
               float _20, float _21, float _22 );
	
	// Inicializa o objeto
	void set( float value );
	void set( const float* pElements );
	void set( float _00, float _01, float _02,
			  float _10, float _11, float _12,
			  float _20, float _21, float _22 );

    // Operadores de acesso
    float& operator () ( uint8 row, uint8 column );
    float  operator () ( uint8 row, uint8 column ) const;

    // Operadores de conversão ( casting )
    operator float* ();
    operator const float* () const;

    // Operadores de atribuição
    Matrix3x3& operator *= ( const Matrix3x3& mtx );
    Matrix3x3& operator += ( const Matrix3x3& mtx );
    Matrix3x3& operator -= ( const Matrix3x3& mtx );
    Matrix3x3& operator *= ( float f );
    Matrix3x3& operator /= ( float f );

    // Operadores unários
    Matrix3x3 operator + () const;
    Matrix3x3 operator - () const;

    // Operadores binários
    Matrix3x3 operator * ( const Matrix3x3& mtx ) const;
    Matrix3x3 operator + ( const Matrix3x3& mtx ) const;
    Matrix3x3 operator - ( const Matrix3x3& mtx ) const;
    Matrix3x3 operator * ( float f ) const;
    Matrix3x3 operator / ( float f ) const;

	// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
    friend Matrix3x3 operator * ( float f, const Matrix3x3& mtx );

	// Operadores de comparação
    bool operator == ( const Matrix3x3& mtx ) const;
    bool operator != ( const Matrix3x3& mtx ) const;
	
	// Transforma esta matriz em sua matriz transposta
	Matrix3x3& transpose( void );
	
	// Obtém a matriz transposta desta matriz
	inline Matrix3x3* getTranspose( Matrix3x3* pOut ) const { *pOut = *this; pOut->transpose(); return pOut; };
	
	// Transforma esta matriz em sua matriz inversa
	bool inverse( void );
	
	// Obtém a matriz inversa desta matriz
	inline Matrix3x3* getInverse( Matrix3x3* pOut ) const { *pOut = *this; pOut->inverse(); return pOut; };
	
	// Transforma esta matriz em uma matriz identidade
	void identity( void );
	
	// Indica se esta matriz é uma matriz identidade
	bool isIdentity( void ) const;
	
	// Matrizes OpenGL são ordenadas da forma column-major
	inline Matrix3x3* getOpenGLMatrix( Matrix3x3* pOut ) const { return getTranspose( pOut ); }; 
};

// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
Matrix3x3 operator * ( float f, const Matrix3x3& mtx );

#endif

