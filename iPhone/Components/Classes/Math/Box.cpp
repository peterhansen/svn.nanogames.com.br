#include "Box.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Box::Box( void ) : x( 0.0f ), y( 0.0f ), z( 0.0f ), width( 0.0f ), height( 0.0f ), depth( 0.0f )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Box::Box( float x, float y, float z, float width, float height, float depth ) : x( x ), y( y ), z( z ), width( width ), height( height ), depth( depth )
{
}

/*===========================================================================================

MÉTODO set
	Inicializa o objeto.

============================================================================================*/

void Box::set( float x, float y, float z, float width, float height, float depth )
{
	this->x = x;
	this->y = y;
	this->z = z;
	
	this->width = width;
	this->height = height;
	this->depth = depth;
}
