//
//  main.m
//  Burp
//
//  Created by Daniel L. Alves on 4/4/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int retVal = UIApplicationMain(argc, argv, nil, nil);
	[pool release];
	return retVal;
}
