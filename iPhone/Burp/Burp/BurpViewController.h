//
//  BurpViewController.h
//  Burp
//
//  Created by Daniel L. Alves on 4/4/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <UIKit/UIKit.h>

// HT
#import "AudioController.h"
#import "SmartConnection.h"
#import "BurpVocodingThreadViewController.h"

// TODO : Enviar o arquivo como MP3 e mostrar progresso de envio!!!

@interface BurpViewController : UIViewController< AudioControllerDelegate, BurpVocodingThreadViewControllerDelegate >
{
	@private
		AudioController* audioManager;

		NSString* lastFilter;
	
		SmartConnection* conn;
	
		IBOutlet UIButton* btRec;
		IBOutlet UIButton* btPlay;
	
		IBOutlet UIButton* btVocode;
		IBOutlet UIButton* btPlayFilter;
		IBOutlet UIButton* btPlayVocoded;
	
		IBOutlet UIButton* btSend;
	
		IBOutlet UITextField* tbBandCount;
		IBOutlet UITextField* tbWindowLength;
		IBOutlet UITextField* tbWindowOverlap;
	
		IBOutlet BurpVocodingThreadViewController* vocoderViewController;
}

-( IBAction )onBtRecClick:( UIButton* )sender;
-( IBAction )onBtPlayClick:( UIButton* )sender;

-( IBAction )onBtFilterBurp01Click:( UIButton* )sender;
-( IBAction )onBtFilterBurp02Click:( UIButton* )sender;
-( IBAction )onBtFilterBurp03Click:( UIButton* )sender;
-( IBAction )onBtFilterBurp04Click:( UIButton* )sender;
-( IBAction )onBtFilterBurp05Click:( UIButton* )sender;
-( IBAction )onBtFilterBurp06Click:( UIButton* )sender;
-( IBAction )onBtFilterCreature01Click:( UIButton* )sender;
-( IBAction )onBtFilterCreature02Click:( UIButton* )sender;
-( IBAction )onBtFilterFrogClick:( UIButton* )sender;
-( IBAction )onBtFilterLaserBeamClick:( UIButton* )sender;
-( IBAction )onBtFilterLionClick:( UIButton* )sender;
-( IBAction )onBtFilterSynthClick:( UIButton* )sender;
-( IBAction )onBtFilterWaterClick:( UIButton* )sender;
-( IBAction )onBtFilterWhiteNoiseClick:( UIButton* )sender;
-( IBAction )onBtFilterZombieClick:( UIButton* )sender;

-( IBAction )onBandCountChanged:( UITextField* )sender;
-( IBAction )onWindowLengthChanged:( UITextField* )sender;
-( IBAction )onWindowOverlapChanged:( UITextField* )sender;


-( IBAction )onBtPlayFilterClick:( UIButton* )sender;
-( IBAction )onBtPlayVocodedClick:( UIButton* )sender;

-( IBAction )onBtSendClick:( UIButton* )sender;

@end
