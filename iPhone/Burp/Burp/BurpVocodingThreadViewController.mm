//
//  BurpVocodingThreadViewController.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/24/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "BurpVocodingThreadViewController.h"

// HT
#import "BurpVocodingThread.h"
#import "Logger.h"


#pragma mark - Defines

// Intervalo entre as atualizações de tela
#define INTERFACE_REFRESH_INTERVAL ( 1.0f / 32.0f )


#pragma mark - Private Interface

@interface BurpVocodingThreadViewController( Private )

//-( BOOL )buildBurpVocodingThreadViewController;

-( BOOL )createVocodingThread;

-( void )onVocodingFinished;
-( void )cancelViewUpdate;
-( void )updateView;

@end


#pragma mark - Implementation

@implementation BurpVocodingThreadViewController


#pragma mark - Acessors

@synthesize titleLabel, bandCount, windowLength, windowOverlap, carrierSoundPath, modulatorSoundPath, delegate;

-( UILabel* )titleLabel
{
	return lbTitle;
}


#pragma mark - Ctor & Dtor

//-( id )init
//{
//	if( ( self = [super init] ) )
//	{
//		if( ![self buildBurpVocodingThreadViewController] )
//			goto Error;
//    }
//    return self;
//	
//Error:
//	[self release];
//	return NULL;
//}

//-( id )initWithNibName:( NSString* )nibNameOrNil bundle:( NSBundle* )nibBundleOrNil
//{
//    if( ( self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil] ) )
//	{
//		if( ![self buildBurpVocodingThreadViewController] )
//			goto Error;
//    }
//    return self;
//	
//Error:
//	[self release];
//	return NULL;
//}

//-( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildBurpVocodingThreadViewController] )
//			goto Error;
//    }
//    return self;
//	
//Error:
//	[self release];
//	return NULL;
//}

//-( BOOL )buildBurpVocodingThreadViewController
//{
//	return YES;
//}

//-( void )dealloc
//{
//    [super dealloc];
//}


#pragma mark - View lifecycle

//-( void )didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

-( void )viewDidLoad
{
    [super viewDidLoad];
	
	[self.view setHidden: YES];
}

//-( void )viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

-( BOOL )shouldAutorotateToInterfaceOrientation:( UIInterfaceOrientation )interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}


#pragma mark - Public Methods

-( BOOL )start
{
	if( [self createVocodingThread] )
	{
		BurpVocodingThread* vocodingThread = ( BurpVocodingThread* )self.workerThread;
		vocodingThread.delegate = self;
		[vocodingThread setModulatorFileName: modulatorSoundPath];
		[vocodingThread setCarrierFileName: carrierSoundPath];
		[vocodingThread setBandCount: bandCount];
		[vocodingThread setWindowLength: windowLength];
		[vocodingThread setWindowOverlap: windowOverlap];
		
		nFramesToProcess = 0;
		
		[self onBurpVocodingThread: ( BurpVocodingThread* )self.workerThread processedFrameNumber: 0];
		[self.view setHidden: NO];
		[loadingIcon startAnimating];
		
		[self cancelViewUpdate];
		interfaceUpdater = [NSTimer scheduledTimerWithTimeInterval: INTERFACE_REFRESH_INTERVAL target: self selector:@selector( updateView ) userInfo: NULL repeats: YES];
		
		return [super start];
	}
	return NO;
}

-( NSData* )vocodedData
{
	return [(( BurpVocodingThread* )self.workerThread ) vocodedData];
}


#pragma mark - BurpVocodingThreadDelegate

-( void )onBurpVocodingThread:( BurpVocodingThread* )thread error:( NSError* )error
{
	LOGI( @"Vocoder error: %@", [error localizedDescription] );
	
	[self onVocodingFinished];
	
	if( [delegate respondsToSelector: @selector( onBurpVocodingThreadViewController:error: )] )
		[delegate onBurpVocodingThreadViewController: self error: error];
}

-( void )onBurpVocodingThread:( BurpVocodingThread* )thread willStartWithNFrames:( NSInteger )nFrames
{
	LOGI( @"Vocoder will start with %d frames", nFrames );
	
	nFramesToProcess = nFrames;
}

-( void )onBurpVocodingThread:( BurpVocodingThread* )thread processedFrameNumber:( NSInteger )frame
{
	LOGI( @"Vocoder processed frame %d", frame );

	self.workProgress = ( float )frame / ( float )nFramesToProcess;
}

-( void )onBurpVocodingThreadFinished:( BurpVocodingThread* )thread;
{
	LOGI( @"Vocoder finished" );
	
	[self onVocodingFinished];
	
	if( [delegate respondsToSelector: @selector( onBurpVocodingThreadViewControllerFinished: )] )
		[delegate onBurpVocodingThreadViewControllerFinished: self];
}


#pragma mark - Utils

-( BOOL )createVocodingThread
{
	BurpVocodingThread* vocodingThread = [[BurpVocodingThread alloc] init];
	if( !vocodingThread )
		return NO;
	
	self.workerThread = vocodingThread;
	[vocodingThread release];

	return YES;
}

-( void )onVocodingFinished
{
	[self cancelViewUpdate];
	[loadingIcon stopAnimating];
	[self.view setHidden: YES];
}

-( void )cancelViewUpdate
{
	[interfaceUpdater invalidate];
	interfaceUpdater = nil;
}

-( void )updateView
{
	NSString* progressText = [NSString stringWithFormat: @"%d%%", ( int )( self.workProgress * 100.0f )];
	[lbProgress setText: progressText];
}

@end
