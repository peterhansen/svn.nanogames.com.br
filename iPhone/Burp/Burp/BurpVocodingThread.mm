//
//  BurpVocodingThread.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/24/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "BurpVocodingThread.h"

// HT
#import "Logger.h"
#import "HTNSStringUtils.h"


#pragma mark - Defines

#define VOCODING_OUPUT_FULL_FILENAME @"final.wav"


#pragma mark - Private Interface

@interface BurpVocodingThread()

@property( readwrite, retain )NSData* vocoded;

@end


@interface BurpVocodingThread( Private )

-( BOOL )buildBurpVocodingThread;

@end


#pragma mark - Implementation

@implementation BurpVocodingThread


#pragma mark - Acessors

@synthesize delegate, vocoded;


#pragma mark - Ctor & Dtor

-( id )init
{
	if( ( self = [super init] ) )
	{
		if( ![self buildBurpVocodingThread] )
			goto Error;
		
		return self;
	}
	
Error:
	[self release];
	return nil;
}

-( id )initWithTarget:( id )target selector:( SEL )selector object:( id )argument
{
	if( ( self = [super initWithTarget: target selector: selector object: argument] ) )
	{
		if( ![self buildBurpVocodingThread] )
			goto Error;
		
		return self;
	}
	
Error:
	[self release];
	return nil;
}

-( BOOL )buildBurpVocodingThread
{
	vocoder = [[VocoderWrapper alloc] init];
	if( vocoder == nil )
		return NO;
	
	vocoder.outputPath = [NSString getFullPathForFileName: VOCODING_OUPUT_FULL_FILENAME];
	vocoder.delegate = self;
	
	return YES;
}

-( void )dealloc
{
	self.vocoded = nil;
	
	[vocoder release];
	
	[super dealloc];
}


#pragma mark - Worker Thread Overriding

-( void )run
{
	self.vocoded = [vocoder run];
}


#pragma mark - Public Methods

-( void )setModulatorFileName:( NSString* )modulator
{
	vocoder.modulatorSoundPath = [NSString getFullPathForFileName: modulator];
}

-( void )setCarrierFileName:( NSString* )carrier
{
	vocoder.carrierSoundPath = [[NSBundle mainBundle] pathForResource: carrier ofType: @"wav"];
}

-( void )setBandCount:( NSInteger )bandCount;
{
	vocoder.bandCount = bandCount;
}

-( void )setWindowLength:( NSUInteger )windowLength
{
	vocoder.windowLength = windowLength;
}

-( void )setWindowOverlap:( NSUInteger )windowOverlap
{
	vocoder.windowOverlap = windowOverlap;
}

-( NSData* )vocodedData
{
	if( !self.vocoded )
		return nil;
	return [NSData dataWithData: self.vocoded];
}


#pragma mark - VocoderWrapperDelegate

-( void )onVocoder:( VocoderWrapper* )vocoder error:( NSError* )error
{
	LOGI( @"Vocoder error: %@", [error localizedDescription] );
	
	if( [delegate respondsToSelector: @selector( onBurpVocodingThread:error: )] )
		[delegate onBurpVocodingThread: self error: error];
}

-( void )onVocoder:( VocoderWrapper* )vocoder startWithNFrames:( NSInteger )nFrames
{
	LOGI( @"Vocoder will start with %d frames", nFrames );
	
	if( [delegate respondsToSelector: @selector( onBurpVocodingThread:willStartWithNFrames: )] )
		[delegate onBurpVocodingThread: self willStartWithNFrames: nFrames];
}

-( void )onVocoder:( VocoderWrapper* )vocoder updateToFrameNumber:( NSInteger )frame
{
	LOGI( @"Vocoder processed frame %d", frame );
	
	if( [delegate respondsToSelector: @selector( onBurpVocodingThread:processedFrameNumber: )] )
		[delegate onBurpVocodingThread: self processedFrameNumber: frame];
}

-( void )onVocoderFinish:( VocoderWrapper* )vocoder;
{
	LOGI( @"Vocoder finished" );
	
	if( [delegate respondsToSelector: @selector( onBurpVocodingThreadFinished: )] )
		[delegate onBurpVocodingThreadFinished: self];
}

@end
