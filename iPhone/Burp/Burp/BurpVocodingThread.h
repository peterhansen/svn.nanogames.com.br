//
//  BurpVocodingThread.h
//  Burp
//
//  Created by Daniel L. Alves on 4/24/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <Foundation/Foundation.h>

// HT
#import "VocoderWrapper.h"
#import "WorkerThread.h"


@class BurpVocodingThread;

@protocol BurpVocodingThreadDelegate< NSObject >

	@optional
		-( void )onBurpVocodingThread:( BurpVocodingThread* )thread error:( NSError* )nFrames;
		-( void )onBurpVocodingThread:( BurpVocodingThread* )thread willStartWithNFrames:( NSInteger )nFrames;
		-( void )onBurpVocodingThread:( BurpVocodingThread* )thread processedFrameNumber:( NSInteger )nFrames;
		-( void )onBurpVocodingThreadFinished:( BurpVocodingThread* )thread;
@end


@interface BurpVocodingThread : WorkerThread< VocoderWrapperDelegate >
{
	@private
		VocoderWrapper* vocoder;
	
		NSData* vocoded;
	
		id< BurpVocodingThreadDelegate > delegate;
}
@property( readwrite, assign )id< BurpVocodingThreadDelegate > delegate;

-( void )setModulatorFileName:( NSString* )modulator;
-( void )setCarrierFileName:( NSString* )carrier;

-( void )setBandCount:( NSInteger )bandCount;
-( void )setWindowLength:( NSUInteger )windowLength;
-( void )setWindowOverlap:( NSUInteger )windowOverlap;

-( NSData* )vocodedData;

@end
