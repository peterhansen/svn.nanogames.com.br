//
//  BurpAppDelegate.h
//  Burp
//
//  Created by Daniel L. Alves on 4/4/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BurpViewController;

@interface BurpAppDelegate : NSObject< UIApplicationDelegate >
{
}
@property( nonatomic, retain )IBOutlet UIWindow* window;
@property( nonatomic, retain )IBOutlet BurpViewController* viewController;

@end
