//
//  BurpVocodingThreadViewController.h
//  Burp
//
//  Created by Daniel L. Alves on 4/24/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <UIKit/UIKit.h>

// HT
#import "BurpVocodingThread.h"
#import "WorkerThreadViewController.h"


@class BurpVocodingThreadViewController;

@protocol BurpVocodingThreadViewControllerDelegate< NSObject >

	@optional
		-( void )onBurpVocodingThreadViewController:( BurpVocodingThreadViewController* )controller error:( NSError* )error;
		-( void )onBurpVocodingThreadViewControllerFinished:( BurpVocodingThreadViewController* )controller;
@end


@interface BurpVocodingThreadViewController : WorkerThreadViewController< BurpVocodingThreadDelegate >
{
	@private
		IBOutlet UILabel* lbTitle;
		IBOutlet UILabel* lbProgress;
		IBOutlet UIActivityIndicatorView* loadingIcon;
	
		NSInteger bandCount;
		NSUInteger windowLength;
		NSUInteger windowOverlap;
		NSString* carrierSoundPath;
		NSString* modulatorSoundPath;
	
		NSInteger nFramesToProcess;
		NSTimer* interfaceUpdater;
	
		id< BurpVocodingThreadViewControllerDelegate > delegate;
}
@property( nonatomic, readonly )UILabel* titleLabel;

@property( nonatomic, readwrite, assign )NSInteger bandCount;
@property( nonatomic, readwrite, assign )NSUInteger windowLength;
@property( nonatomic, readwrite, assign )NSUInteger windowOverlap;

@property( nonatomic, readwrite, retain )NSString* carrierSoundPath;
@property( nonatomic, readwrite, retain )NSString* modulatorSoundPath;

@property( nonatomic, readwrite, assign )id< BurpVocodingThreadViewControllerDelegate > delegate;

-( NSData* )vocodedData;

@end
