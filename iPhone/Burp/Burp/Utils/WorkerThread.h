/*
 *  WorkerThread.h
 *  Burp
 *
 *  Created by Daniel Lopes Alves on 4/24/11.
 *  Copyright 2011 Hermit Tommy. All rights reserved.
 *
 */

#ifndef WORKER_THREAD_H
#define WORKER_THREAD_H

#import <UIKit/UIKit.h>

@interface WorkerThread : NSThread
{
}

// Método que deve ser sobrescrito pelas subclasses. Assim que a thread iniciar sua execução, ele será chamado
-( void )run;

@end

#endif
