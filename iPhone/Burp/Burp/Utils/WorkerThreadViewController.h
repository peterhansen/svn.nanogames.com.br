//
//  WorkerThreadViewController.h
//  Burp
//
//  Created by Daniel Lopes Alves on 4/24/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef WORKER_THREAD_VIEW_CONTROLLER_H
#define WORKER_THREAD_VIEW_CONTROLLER_H

#import <UIKit/UIKit.h>

// HT
#include "WorkerThread.h"

@interface WorkerThreadViewController : UIViewController
{
	@private
		float workProgress;

		WorkerThread* workerThread;
}
// FIXME : Isto deveria ser thread safe !!! No entanto, veja o arquivo .mm correspondente para enteder melhor o problema
@property( nonatomic, readwrite, assign )float workProgress;

// FIXME : Isto deveria ser thread safe !!! No entanto, veja o arquivo .mm correspondente para enteder melhor o problema
@property( nonatomic, readwrite, retain )WorkerThread* workerThread;

-( BOOL )start;

@end

#endif
