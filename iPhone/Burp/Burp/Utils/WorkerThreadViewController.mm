//
//  WorkerThreadViewController.mm
//  Burp
//
//  Created by Daniel Lopes Alves on 4/24/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#include "WorkerThreadViewController.h"

#pragma mark - Defines

#define BUSY_WAIT_SLEEP_TIME_SECS 0.050


#pragma mark - Private Interface

@interface WorkerThreadViewController( Private )

//-( BOOL )buildWorkerThreadViewController;

-( void )cancelThread;

@end


#pragma mark - Implementation

@implementation WorkerThreadViewController


#pragma mark - Acessors

@synthesize workProgress, workerThread;

// FIXME : Isto deveria ser thread safe !!! No entanto, se declaramos a propriedade como atômica, não conseguimos (talvez
// por falta de conhecimento) sobrescrever sua implementação, e precisamos fazer isto para conseguir fazer o clamp [0.0, 1.0]
-( void )setWorkProgress:( float )percent
{
	workProgress = percent;
	
	if( workProgress < 0.0f )
		workProgress = 0.0f;
	else if( workProgress > 1.0f )
		workProgress = 1.0f;
}

// FIXME : Isto deveria ser thread safe !!! No entanto, se declaramos a propriedade como atômica, não conseguimos (talvez
// por falta de conhecimento) sobrescrever sua implementação, e precisamos fazer isto para conseguir chamar cancelThread 
// quando necessário
-( void )setWorkerThread:( WorkerThread* )thread
{
	if( workerThread != thread )
	{
		[self cancelThread];
		
		workerThread = thread;
		
		if( workerThread )
			[workerThread retain];
	}	
}


#pragma mark - Ctor & Dtor

//-( id )init
//{
//	if( ( self = [super init] ) )
//	{
//		if( ![self buildWorkerThreadViewController] )
//			goto Error;
//    }
//    return self;
//	
//	Error:
//		[self release];
//		return NULL;
//}

//-( id )initWithNibName:( NSString* )nibNameOrNil bundle:( NSBundle* )nibBundleOrNil
//{
//    if( ( self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil] ) )
//	{
//		if( ![self buildWorkerThreadViewController] )
//			goto Error;
//    }
//    return self;
//
//	Error:
//		[self release];
//		return NULL;
//}

//-( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildWorkerThreadViewController] )
//			goto Error;
//    }
//    return self;
//
//	Error:
//		[self release];
//		return NULL;
//}

//-( BOOL )buildWorkerThreadViewController
//{
//	return YES;
//}

-( void )dealloc
{
	[self cancelThread];
	
    [super dealloc];
}


#pragma mark - View Lifecycle

-( BOOL )start
{
	self.workProgress = 0.0f;

	[workerThread start];
	
	return YES;
}


#pragma mark - Utils

-( void )cancelThread
{
	if( workerThread )
	{
		if( [workerThread isExecuting] )
			[workerThread cancel];
		
		while( ![workerThread isFinished] )
			[NSThread sleepForTimeInterval: BUSY_WAIT_SLEEP_TIME_SECS];
		
		[workerThread release];
		workerThread = nil;
	}
}

@end
