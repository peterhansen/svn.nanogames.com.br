//
//  SmartConnection.mm
//  Burp
//
//  Created by Daniel L. Alves on 3/31/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "SmartConnection.h"

// GLB
#import "Logger.h"
#import "HTNSErrorUtils.h"

#pragma mark - Defines

// Métodos HTTP suportados
#define HTTPMETHOD_GET @"GET"
#define HTTPMETHOD_POST @"POST"

// Valor default do timeout da conexão
#if IOSGLOBOAPI_DEBUG
	#define DEFAULT_CONN_TIMEOUT_SECS 30
#else
	#define DEFAULT_CONN_TIMEOUT_SECS 3
#endif

// Domain dos erros informados pelos objetos desta classe
NSString *const SmartConnectionErrorDomain = @"SmartConnectionErrorDomain";

// Respostas HTTP
#define HTTP_STATUS_OK 200

#pragma mark - Private Interface

@interface SmartConnection()

@property( nonatomic, readwrite, retain )NSURLConnection* conn;
@property( nonatomic, readwrite, retain )NSMutableData* connData;

@end


@interface SmartConnection( Private )

-( void )downloadRequest:( NSMutableURLRequest* )request;

-( void )startTimeoutController;
-( void )cancelTimeoutController;
-( void )onTimeout;

-( BOOL )connectionSucceeded;

@end

#pragma mark - Implementation

@implementation SmartConnection

#pragma mark - Acessors

@synthesize httpStatusCode, timeoutSecs, delegate, conn, connData;

#pragma mark - Ctor & Dtor

-( id )init
{
	if( ( self = [super init] ) )
	{
		timeoutSecs = DEFAULT_CONN_TIMEOUT_SECS;		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( void )dealloc
{
	[self cancel];
	
	self.delegate = nil;
	
	[super dealloc];
}

#pragma mark - Public Methods

-( void )cancel
{
	[self.conn cancel];
	[self cancelTimeoutController];
	
	requestAlive = NO;

	self.conn = nil;
	self.connData = nil;

	httpStatusCode = 0;
}

-( BOOL )isRequestAlive
{
	return requestAlive;
}

-( void )downloadRequest:( NSMutableURLRequest* )request
{
	[self cancel];
	
	if( request )
	{
        NSURLConnection* connection = [[NSURLConnection alloc] initWithRequest: request delegate: self];
		self.conn = connection;
        [connection release];
		if( !self.conn )
		{
			LOGE( @"Could not create NSURLConnection. Download will not be done." );
			return;
		}
		
		self.connData = [NSMutableData dataWithCapacity: 10];
		if( !self.connData )
		{
			LOGE( @"Could not create download data storage. Download will not be done." );
			return;
		}
		
		if( delegate && [delegate respondsToSelector: @selector( onSmartConnectionStarted: )] )
			[delegate onSmartConnectionStarted: self];
		
		[self.conn start];
		[self startTimeoutController];
		
		LOGI( @"Started download request" );

		requestAlive = YES;
	}
}

-( void )startRequest:( NSMutableURLRequest* )request
{
	[self downloadRequest: request];
}

-( void )startRequest:( NSString* )urlStr withParams:( NSString* )params usingGetInsteadOfPost:( BOOL )usingGet
{
	NSMutableURLRequest *request;
	if( usingGet )
	{
		if( params )
			urlStr = [urlStr stringByAppendingFormat: @"?%@", params];
		
		request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString: urlStr]];
		[request setHTTPMethod: HTTPMETHOD_GET];
	}
	else
	{
		request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString: urlStr]];
		[request setHTTPMethod: HTTPMETHOD_POST];
		
		if( params )
		{
			NSData* bodyData = [params dataUsingEncoding: NSUTF8StringEncoding];
			[request setHTTPBody: bodyData];
			[request setValue: [NSString stringWithFormat: @"%d", [bodyData length]] forHTTPHeaderField: @"Content-Length" ];
		}
	}
	
	[self startRequest: request];
}

#pragma mark - Timeout Control

-( void )startTimeoutController
{
	timeoutController = [NSTimer timerWithTimeInterval: timeoutSecs target: self selector: @selector( onTimeout ) userInfo: nil repeats: NO];
	if( timeoutController )
	{
		[[NSRunLoop currentRunLoop] addTimer: timeoutController forMode: NSRunLoopCommonModes];
	}
	else
	{
		LOGE( @"Could not create timeout controller. Timeout control won't be used." );
	}
}

-( void )cancelTimeoutController
{
	[timeoutController invalidate];
	timeoutController = nil;
}

-( void )onTimeout
{
	NSString* errorMsg =  NSLocalizedString( @"Connection timeout", nil );
	
	LOGI( @"%@", errorMsg );
	
	NSError* error = [NSError errorWithCode: SmartConnectionErrorConnectionTimeout 
									 domain: SmartConnectionErrorDomain 
					andLocalizedDescription: errorMsg];
	
	[self connection: conn didFailWithError: error];
}

#pragma mark - NSURLConnection delegate methods

-( void )connectionDidFinishLoading:( NSURLConnection* )connection
{
	if( ![self connectionSucceeded] )
	{
		NSString* errorMsg = [NSString stringWithFormat: NSLocalizedString( @"Connection failed with status %d", nil ), httpStatusCode];
		
		LOGI( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: SmartConnectionErrorBadStatus 
										 domain: SmartConnectionErrorDomain 
						andLocalizedDescription:  errorMsg];
		
		[self connection: connection didFailWithError: error];
	}
	else
	{
		if( delegate && [delegate respondsToSelector: @selector( onSmartConnection:finishedReceivingData: )] )
			[delegate onSmartConnection: self finishedReceivingData: [NSData dataWithData: connData]];
		
		[self cancel];
	}
}

-( void )connection:( NSURLConnection* )connection didReceiveData:( NSData* )data
{
	[self.connData appendData: data];
}

-( void )connection:( NSURLConnection* )connection didReceiveResponse:( NSURLResponse* )response
{
	NSHTTPURLResponse* httpResponse = ( NSHTTPURLResponse* )response;
	httpStatusCode = httpResponse.statusCode;
}

-( void )connection:( NSURLConnection* )connection didFailWithError:( NSError* )error
{
	[self cancel];
	
	if( delegate && [delegate respondsToSelector: @selector( onSmartConnection:error: )] )
	   [delegate onSmartConnection: self error: error];
}

#pragma mark - Utils

-( BOOL )connectionSucceeded
{
	return httpStatusCode == HTTP_STATUS_OK;
}

@end
