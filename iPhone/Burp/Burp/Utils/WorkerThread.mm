/*
 *  WorkerThread.mm
 *  Burp
 *
 *  Created by Daniel Lopes Alves on 4/24/11.
 *  Copyright 2011 Hermit Tommy. All rights reserved.
 *
 */

#include "WorkerThread.h"

// HT
#import "Logger.h"


#pragma mark - Implementation

@implementation WorkerThread

-( void )run
{
	// A implementação padrão é vazia mesmo
}

-( void )main
{
	NSAutoreleasePool* hPool = [[NSAutoreleasePool alloc] init];
	
	@try
	{
		[self run];
	}
	@catch( NSException* ex )
	{
		LOGE( @"Exception caught in thread %@: %@", [self name], [ex reason] );
	}		

	[hPool release];
}

@end
