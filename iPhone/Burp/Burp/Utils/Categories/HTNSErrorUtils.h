//
//  HTNSErrorUtils.h
//  Burp
//
//  Created by Daniel L. Alves on 4/19/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSError( HTNSErrorUtils )

+( NSError* )errorWithCode:( NSInteger )code domain:( NSString* )domain andLocalizedDescription:( NSString* )description;

@end
