//
//  Logger.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Logger.h"


@implementation NSString( Logger )

+( void )log:( NSString* )format, ...
{
	va_list params;
	va_start( params, format );

	NSLogv( format, params );
	
	va_end( params );
}

@end
