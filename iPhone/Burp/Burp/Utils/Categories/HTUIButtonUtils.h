//
//  HTUIButtonUtils.h
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIButton( HTUIButtonUtils )

-( void )setTitleForAllStates:( NSString* )newTitle;

@end
