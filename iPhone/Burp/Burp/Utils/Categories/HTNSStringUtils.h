//
//  HTNSStringUtils.h
//  Burp
//
//  Created by Daniel L. Alves on 4/16/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString( HTNSStringUtils )

+( NSString* )getFullPathForFileName:( NSString* )filename;

@end
