//
//  HTNSURLUtils.h
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSURL( HTNSURLUtils )

+( NSURL* )fileURLForFileName:( NSString* )filename;

@end
