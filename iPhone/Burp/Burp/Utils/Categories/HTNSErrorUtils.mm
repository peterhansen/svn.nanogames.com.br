//
//  HTNSErrorUtils.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/19/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "HTNSErrorUtils.h"


@implementation NSError( HTNSErrorUtils )

+( NSError* )errorWithCode:( NSInteger )code domain:( NSString* )domain andLocalizedDescription:( NSString* )description
{	
	NSArray *objArray = [NSArray arrayWithObjects: description, nil];
	
	NSArray *keyArray = [NSArray arrayWithObjects: NSLocalizedDescriptionKey, nil];
	
	NSDictionary *dict = [NSDictionary dictionaryWithObjects: objArray forKeys: keyArray];
	
	return [NSError errorWithDomain: domain code: code userInfo: dict];
}

@end
