//
//  HTNSStringUtils.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/16/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "HTNSStringUtils.h"


@implementation NSString( HTNSStringUtils )

+( NSString* )getFullPathForFileName:( NSString* )filename
{
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == nil )
		return nil;
	return [hDocumentsDirectory stringByAppendingPathComponent: filename];
}

@end
