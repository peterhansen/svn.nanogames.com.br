//
//  HTUIButtonUtils.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "HTUIButtonUtils.h"


@implementation UIButton( HTUIButtonUtils )

-( void )setTitleForAllStates:( NSString* )newTitle
{
	[self setTitle: newTitle forState:UIControlStateNormal];
	[self setTitle: newTitle forState:UIControlStateHighlighted];
	[self setTitle: newTitle forState:UIControlStateDisabled];
	[self setTitle: newTitle forState:UIControlStateSelected];
}

@end
