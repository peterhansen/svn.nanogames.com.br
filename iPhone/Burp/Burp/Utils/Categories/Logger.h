//
//  Logger.h
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef LOGGER_H
#define LOGGER_H

#import <Foundation/Foundation.h>

@interface NSString( Logger )

+( void )log:( NSString* )format, ...;

@end

#if HT_DEBUG
	#define LOG( format, ... ) [NSString log: @"%s:\n\t"format"\n", __PRETTY_FUNCTION__, ##__VA_ARGS__ ]
#else
	#define LOG( format, ... )
#endif

#define LOGI( format, ... ) LOG( @"INFO: "format, ##__VA_ARGS__ )
#define LOGW( format, ... ) LOG( @"WARNING: "format, ##__VA_ARGS__ )
#define LOGE( format, ... ) LOG( @"ERROR: "format, ##__VA_ARGS__ )

#endif
