//
//  HTNSURLUtils.m
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "HTNSURLUtils.h"

// HT
#import "Logger.h"
#import "HTNSStringUtils.h"

@implementation NSURL( HTNSURLUtils )

+( NSURL* )fileURLForFileName:( NSString* )filename
{
	NSString* temp = [NSString getFullPathForFileName: filename];
	
	LOGI( @"Full path for file %@ is: %@", filename, temp );
	
	return [NSURL fileURLWithPath: temp];
}

@end
