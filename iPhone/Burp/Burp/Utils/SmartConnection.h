//
//  SmartConnection.h
//  Burp
//
//  Created by Daniel L. Alves on 3/31/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef SMART_CONNECTION_H
#define SMART_CONNECTION_H

#import <Foundation/Foundation.h>

// Domain dos erros informados pelos objetos desta classe
FOUNDATION_EXPORT NSString *const SmartConnectionErrorDomain;

// Códigos de erro
enum
{
	SmartConnectionErrorConnectionTimeout,
	SmartConnectionErrorBadStatus
};


@class SmartConnection;

@protocol SmartConnectionDelegate< NSObject >

	@optional
		-( void )onSmartConnectionStarted:( SmartConnection* )conn;
		-( void )onSmartConnection:( SmartConnection* )conn finishedReceivingData:( NSData* )data;
		-( void )onSmartConnection:( SmartConnection* )conn error:( NSError* )error;

@end


@interface SmartConnection : NSObject
{
	@private
		BOOL requestAlive;

		NSURLConnection* conn;
		NSMutableData* connData;
		NSInteger httpStatusCode;
	
		NSTimer* timeoutController;
		NSTimeInterval timeoutSecs;
	
		id< SmartConnectionDelegate > delegate;
}
@property( nonatomic, readonly )NSInteger httpStatusCode;
@property( nonatomic, readonly, assign )NSTimeInterval timeoutSecs;
@property( nonatomic, readwrite, assign )id< SmartConnectionDelegate > delegate;

-( void )cancel;
-( BOOL )isRequestAlive;
-( void )startRequest:( NSMutableURLRequest* )request;
-( void )startRequest:( NSString* )urlStr withParams:( NSString* )params usingGetInsteadOfPost:( BOOL )usingGet;

@end

#endif
