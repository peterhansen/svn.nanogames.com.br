//
//  AudioPlayerWrapper.h
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef AUDIO_PLAYER_WRAPPER_H
#define AUDIO_PLAYER_WRAPPER_H

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>

// Domain dos erros informados por este componente
FOUNDATION_EXPORT NSString *const AudioPlayerErrorDomain;

// Códigos de Erro
enum
{
	AudioPlayerErrorCouldNotCreatePlayer = 1,
	AudioPlayerErrorCouldNotStartPlaying = 2
};


@class AudioPlayerWrapper;

@protocol AudioPlayerWrapperDelegate< NSObject >

	@required
		-( void )onAudioPlayerWrapper:( AudioPlayerWrapper* )player error:( NSError* )error;

	@optional
		-( void )audioPlayerWrapperBeginInterruption:( AudioPlayerWrapper* )player;
		-( void )audioPlayerWrapperEndInterruption:( AudioPlayerWrapper* )player;
		-( void )audioPlayerWrapperDidFinishPlaying:( AudioPlayerWrapper* )player successfully:( BOOL )flag;
@end


@interface AudioPlayerWrapper : NSObject< AVAudioPlayerDelegate >
{
    @private		
		BOOL playing;
		BOOL interruptedOnPlayback;
	
		AVAudioPlayer* audioPlayer;
		
		AVAudioSession* session;
	
		id< AudioPlayerWrapperDelegate > delegate;
}
@property( nonatomic, readwrite, assign )id< AudioPlayerWrapperDelegate > delegate;

-( id )initWithSession:( AVAudioSession* )audioSession;

-( BOOL )startPlayingData:( NSData* )data;
-( void )stopPlaying;
-( void )pausePlaying;
-( BOOL )isPlaying;

@end

#endif
