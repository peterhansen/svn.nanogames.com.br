//
//  AudioController.h
//  Burp
//
//  Created by Daniel L. Alves on 4/4/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioSession.h>

// HT
#import "AudioRecorderWrapper.h"
#import "AudioPlayerWrapper.h"


// Domain dos erros informados por este componente
FOUNDATION_EXPORT NSString *const AudioControllerErrorDomain;

// Códigos de Erro
enum
{
	AudioControllerErrorCouldNotSetCategory = 1,
	AudioControllerErrorCouldNotActivateSession = 2
};


@class AudioController;

@protocol AudioControllerDelegate< NSObject >

	@required
		-( void )onAudioController:( AudioController* )controller error:( NSError* )error;

	@optional
		-( void )playerBeginInterruption:( AudioController* )controller;
		-( void )playerEndInterruption:( AudioController* )controller;
		-( void )playerDidFinishPlaying:( AudioController* )controller successfully:( BOOL )flag;

		-( void )recorderBeginInterruption:( AudioController* )controller;
		-( void )recorderEndInterruption:( AudioController* )controller;
		-( void )recorderDidFinishRecording:( AudioController* )controller successfully:( BOOL )flag;
@end


typedef enum AudioWrapperState
{
	AUDIO_WRAPPER_STATE_UNDEFINED,
	AUDIO_WRAPPER_STATE_PLAYBACK,
	AUDIO_WRAPPER_STATE_RECORD

}AudioWrapperState;


@interface AudioController : NSObject< AVAudioSessionDelegate, AudioPlayerWrapperDelegate, AudioRecorderWrapperDelegate >
{
	@private
		AudioPlayerWrapper* audioPlayer;
		AudioRecorderWrapper* audioRecorder;
	
		AudioWrapperState state;
		AVAudioSession* session;
	
		NSString* recordFilename;
		NSTimeInterval recordDuration;
	
		id< AudioControllerDelegate > delegate;
}
@property( nonatomic, readwrite, assign )id< AudioControllerDelegate > delegate;

// Recording
-( void )setRecordingFilename:( NSString* )filename;
-( NSString* )getRecordingFilename;

-( void )setRecordingDurationSecs:( NSTimeInterval )duration;
-( NSTimeInterval )getRecordingDurationSecs;

-( BOOL )startRecording;
-( void )stopRecording;
-( BOOL )isRecording;
-( BOOL )isRecordingAvailable;
-( NSData* )getRecordedData;
-( BOOL )hasRecordedData;

// Playback
-( BOOL )startPlaying:( NSData* )data;
-( void )stopPlaying;
-( void )pausePlaying;
-( BOOL )isPlaying;

@end
