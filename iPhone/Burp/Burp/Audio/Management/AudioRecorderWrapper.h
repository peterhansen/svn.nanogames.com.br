//
//  AudioRecorderWrapper.h
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef AUDIO_RECORDER_WRAPPER_H
#define AUDIO_RECORDER_WRAPPER_H

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioRecorder.h>
#import <AVFoundation/AVAudioSession.h>

// Domain dos erros informados por este componente
FOUNDATION_EXPORT NSString *const AudioRecorderErrorDomain;

// Códigos de Erro
enum
{
	AudioRecorderErrorCouldNotCreateRecorder = 1,
	AudioRecorderErrorCouldNotStartRecording = 2
};


@class AudioRecorderWrapper;

@protocol AudioRecorderWrapperDelegate< NSObject >

	@required
		-( void )onAudioRecorderWrapper:( AudioRecorderWrapper* )recorder error:( NSError* )error;

	@optional
		-( void )audioRecorderWrapperBeginInterruption:( AudioRecorderWrapper* )recorder;
		-( void )audioRecorderWrapperEndInterruption:( AudioRecorderWrapper* )recorder;
		-( void )audioRecorderWrapperDidFinishRecording:( AudioRecorderWrapper* )recorder successfully:( BOOL )flag;
@end


@interface AudioRecorderWrapper : NSObject< AVAudioRecorderDelegate >
{
	@private
		BOOL recording;
		BOOL interruptedOnRecording;
		
		AVAudioRecorder* audioRecorder;
		
		AVAudioSession* session;
	
		NSString* recordFilename;
		NSTimeInterval recordDuration;
	
		id< AudioRecorderWrapperDelegate > delegate;
}
@property( nonatomic, readwrite, retain )NSString* recordFilename;
@property( nonatomic, readwrite, assign )NSTimeInterval recordDuration;

@property( nonatomic, readwrite, assign )id< AudioRecorderWrapperDelegate > delegate;

-( id )initWithSession:( AVAudioSession* )audioSession;

-( BOOL )startRecording;
-( void )stopRecording;
-( BOOL )isRecording;
-( BOOL )isRecordingAvailable;

-( NSData* )getRecordedData;

@end

#endif
