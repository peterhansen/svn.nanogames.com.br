//
//  AudioController.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/4/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "AudioController.h"

// HT
#import "Logger.h"
#import "HTNSErrorUtils.h"

#pragma mark - Defines

// Domain dos erros informados por este componente
NSString *const AudioControllerErrorDomain = @"AudioControllerErrorDomain";


#pragma mark - Private Interface

@interface AudioController( Private )

-( void )setActive:( BOOL )active;
-( BOOL )setState:( AudioWrapperState )newState;

@end


#pragma mark - Implementation

@implementation AudioController


#pragma mark - Acessors

@synthesize delegate;

// Sobrescreve o setState gerado por @synthesize
-( BOOL )setState:( AudioWrapperState )newState
{
	NSString* category;
	switch( newState )
	{
		case AUDIO_WRAPPER_STATE_PLAYBACK:
			// Não queremos a categoria AVAudioSessionCategoryPlayback, pois queremos permitir
			// que o usuário escute músicas ao mesmo tempo que ouve o seu arroto. Além do mais,
			// interromper o áudio não irá piorar a experiência do usuário. Ver discussões em:
			// - http://developer.apple.com/library/ios/#documentation/UserExperience/Conceptual/MobileHIG/TechnologyUsage/TechnologyUsage.html#//apple_ref/doc/uid/TP40006556-CH18-SW3
			// - http://developer.apple.com/library/ios/#documentation/Audio/Conceptual/AudioSessionProgrammingGuide/Configuration/Configuration.html#//apple_ref/doc/uid/TP40007875-CH3-SW3
			category = AVAudioSessionCategoryAmbient;
			break;
			
		case AUDIO_WRAPPER_STATE_RECORD:
			category = AVAudioSessionCategoryRecord;
			break;
			
		case AUDIO_WRAPPER_STATE_UNDEFINED:
		default:
			// Em casos de "erro", vamos utilizar a categoria AVAudioSessionCategoryAmbient, já que é a
			// categoria default do iOS, além de ser a menos invasiva
			category = AVAudioSessionCategoryAmbient;
			break;
	}
	
	NSError* setCategoryError = nil;
	[session setCategory: category error: &setCategoryError];

	BOOL ret = NO;
	if( setCategoryError )
	{
		NSString* errorMsg = [NSString stringWithFormat: @"%@ %@: %@", 
														 NSLocalizedString( @"Could no set category", nil ),
														 category,
														 [setCategoryError localizedDescription]];
		LOGE( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: AudioControllerErrorCouldNotSetCategory 
										 domain: AudioControllerErrorDomain
						andLocalizedDescription: errorMsg];
		
		[delegate onAudioController: self error: error];
	}
	else
	{
		LOGI( @"Switched to category %@", category );

		state = newState;
		ret = YES;
	}
	
	return ret;
}


#pragma mark - CTor & DTor

-( id )init
{
	if( ( self = [super init] ) )
	{
		session = [AVAudioSession sharedInstance];
		session.delegate = self;
		
		[self setState: AUDIO_WRAPPER_STATE_UNDEFINED];
		
		// The system activates your audio session on application launch. Even so, Apple recommends that you explicitly activate
		// your session—typically as part of your application’s viewDidLoad method. This gives you an opportunity to test whether
		// or not activation succeeded.
		[self setActive: YES];
		
		audioPlayer = [[AudioPlayerWrapper alloc] initWithSession: session];
		if( !audioPlayer )
			goto Error;
		
		audioPlayer.delegate = self;
		
		audioRecorder = [[AudioRecorderWrapper alloc] initWithSession: session];
		if( !audioRecorder )
			goto Error;
		
		audioRecorder.delegate = self;

		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( void )dealloc
{
	[audioPlayer release];
	[audioRecorder release];
	
	[super dealloc];
}


#pragma mark - AVAudioSessionDelegate

-( void )beginInterruption
{
	// Most applications never need to explicitly deactivate their audio session. Important exceptions include 
	// VoIP (Voice over Internet Protocol) applications and, in some cases, recording applications.
}

-( void )endInterruption
{
	// The system deactivates your audio session for a Clock or Calendar alarm or incoming phone call. When the user dismisses
	// the alarm, or chooses to ignore a phone call, the system allows your session to become active again. Do reactivate it,
	// upon interruption end, to ensure that your audio works.
	// In the specific case of playing or recording audio with an AVAudioPlayer or AVAudioRecorder object, the system takes care
	// of audio session reactivation upon interruption end. Nonetheless, Apple recommends that you implement the
	// AVAudioSessionDelegate protocol’s interruption delegate methods to explicitly reactivate your audio session. Doing so gives
	// you the opportunity to ensure that reactivation succeeded, and to update your application’s state and user interface.
	[self setActive: YES];
}

-( void )inputIsAvailableChanged:( BOOL )isInputAvailable
{
	LOGI( @"inputIsAvailableChanged: %@", ( isInputAvailable ? @"True" : @"False" ) );
	
	// TODO : Implementar ???
}


#pragma mark - Recording Methods

-( void )setRecordingFilename:( NSString* )filename
{
	audioRecorder.recordFilename = filename;
}

-( NSString* )getRecordingFilename
{
	return audioRecorder.recordFilename;
}

-( void )setRecordingDurationSecs:( NSTimeInterval )duration
{
	audioRecorder.recordDuration = duration;
}

-( NSTimeInterval )getRecordingDurationSecs
{
	return audioRecorder.recordDuration;
}

-( BOOL )startRecording
{
	if( state != AUDIO_WRAPPER_STATE_RECORD )
	{
		if( state == AUDIO_WRAPPER_STATE_PLAYBACK )
			[self stopPlaying];
		
		if( ![self setState: AUDIO_WRAPPER_STATE_RECORD] )
			return NO;
	}
	return [audioRecorder startRecording];
}

-( void )stopRecording
{
	if( state == AUDIO_WRAPPER_STATE_RECORD )
	{
		[audioRecorder stopRecording];

		// An app using the “recording” category should ensure that its audio session is active only while recording.
		// Before recording starts and when recording stops, ensure your session is inactive to allow other sounds, such as 
		// incoming message alerts, to play. Alternatively, if your recording app also provides playback, you could switch to
		// a playback category when not recording. This also allows alert sounds to play. 
		[self setState: AUDIO_WRAPPER_STATE_UNDEFINED];
	}
}

-( BOOL )isRecording
{
	return ( state == AUDIO_WRAPPER_STATE_RECORD ) && [audioRecorder isRecording];
}

-( BOOL )isRecordingAvailable
{
	return [audioRecorder isRecordingAvailable];
}

-( NSData* )getRecordedData
{
	return [audioRecorder getRecordedData];
}

-( BOOL )hasRecordedData
{
	return [[audioRecorder getRecordedData] length] > 0;
}


#pragma mark - AudioRecorderWrapperDelegate

-( void )onAudioRecorderWrapper:( AudioRecorderWrapper* )recorder error:( NSError* )error
{
	// Garante que estaremos na categoria correta
	[self stopRecording];
	
	[delegate onAudioController: self error: error];
}

-( void )audioRecorderWrapperBeginInterruption:( AudioRecorderWrapper* )recorder
{
	if( [delegate respondsToSelector: @selector( recorderBeginInterruption: )] )
		[delegate recorderBeginInterruption: self];
}

-( void )audioRecorderWrapperEndInterruption:( AudioRecorderWrapper* )recorder
{
	if( [delegate respondsToSelector: @selector( recorderEndInterruption: )] )
		[delegate recorderEndInterruption: self];
}

-( void )audioRecorderWrapperDidFinishRecording:( AudioRecorderWrapper* )recorder successfully:( BOOL )flag
{
	// Garante que estaremos na categoria correta
	[self stopRecording];
	
	if( [delegate respondsToSelector: @selector( recorderDidFinishRecording:successfully: )] )
		[delegate recorderDidFinishRecording: self successfully: flag];
}


#pragma mark - Playback Methods

-( BOOL )startPlaying:( NSData* )data
{
	if( state != AUDIO_WRAPPER_STATE_PLAYBACK )
	{
		if( state == AUDIO_WRAPPER_STATE_RECORD )
			[self stopRecording];
	
		if( ![self setState: AUDIO_WRAPPER_STATE_PLAYBACK] )
			return NO;
	}
	return [audioPlayer startPlayingData: data];
}

-( void )stopPlaying;
{
	if( state == AUDIO_WRAPPER_STATE_PLAYBACK )
	{
		[audioPlayer stopPlaying]; 
		[self setState: AUDIO_WRAPPER_STATE_UNDEFINED];
	}
}

-( void )pausePlaying
{
	if( state == AUDIO_WRAPPER_STATE_PLAYBACK )
		[audioPlayer pausePlaying];
}

-( BOOL )isPlaying
{
	return ( state == AUDIO_WRAPPER_STATE_PLAYBACK ) && [audioPlayer isPlaying];
}


#pragma mark - AudioPlayerWrapperDelegate

-( void )onAudioPlayerWrapper:( AudioPlayerWrapper* )player error:( NSError* )error
{
	// Garante que estaremos na categoria correta
	[self stopPlaying];
	
	[delegate onAudioController: self error: error];
}

-( void )audioPlayerWrapperBeginInterruption:( AudioPlayerWrapper* )player
{
	if( [delegate respondsToSelector: @selector( playerBeginInterruption: )] )
		[delegate playerBeginInterruption: self];
}

-( void )audioPlayerWrapperEndInterruption:( AudioPlayerWrapper* )player
{
	if( [delegate respondsToSelector: @selector( playerEndInterruption: )] )
		[delegate playerEndInterruption: self];
}

-( void )audioPlayerWrapperDidFinishPlaying:( AudioPlayerWrapper* )player successfully:( BOOL )flag
{
	if( [delegate respondsToSelector: @selector( playerDidFinishPlaying:successfully: )] )
		[delegate playerDidFinishPlaying: self successfully: flag];
	
	// Garante que estaremos na categoria correta
	[self stopPlaying];
}


#pragma mark - Utils

-( void )setActive:( BOOL )active
{
	NSError* activationError = nil;
	[session setActive: active error: &activationError];
	
	// When changing your audio session’s active/inactive state, check to ensure that the call is successful. Write your code
	// to gracefully handle the system refusing to activate your session.
	if( activationError )
	{
		NSString* errorMsg = [NSString stringWithFormat: @"%@: %@",
														 NSLocalizedString( @"Could not activate session", nil ),
														 [activationError localizedDescription]];
		LOGE( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: AudioControllerErrorCouldNotActivateSession 
										 domain: AudioControllerErrorDomain
						andLocalizedDescription: errorMsg];
		
		[delegate onAudioController: self error: error];
	}
}
		 
@end
