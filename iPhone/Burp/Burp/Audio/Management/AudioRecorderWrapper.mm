//
//  AudioRecorderWrapper.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "AudioRecorderWrapper.h"

// Foundation
#import <AudioToolbox/AudioServices.h>
#import <CoreAudio/CoreAudioTypes.h>

// HT
#import "Logger.h"
#import "HTNSErrorUtils.h"
#import "HTNSURLUtils.h"


#pragma mark - Defines

// Configurações default do arquivo que vamos gravar
#define RECORDING_FILENAME @"rec_temp"
#define RECORDING_DURATION_SEC 10.0

// Domain dos erros informados por este componente
NSString *const AudioRecorderErrorDomain = @"AudioRecorderErrorDomain";


#pragma mark - Private Interface

@interface AudioRecorderWrapper( Private )

-( BOOL )resetRecorder;

+( NSDictionary* )getSettings;

@end


#pragma mark - Implementation

@implementation AudioRecorderWrapper


#pragma mark - Acessors

@synthesize recordFilename, recordDuration, delegate;

-( void )setRecordFilename:( NSString* )str
{
	if( recordFilename != str )
	{
		[recordFilename release];

		recordFilename = [NSString stringWithFormat: @"%@.wav", ( str ? str : RECORDING_FILENAME )];

		[recordFilename retain];
		
		[self resetRecorder];
	}
}


#pragma mark - CTor & DTor

-( id )initWithSession:( AVAudioSession* )audioSession
{
	if( ( self = [super init] ) )
	{
		session = audioSession;
		
		recording = NO;
		interruptedOnRecording = NO;
		
		self.recordFilename = RECORDING_FILENAME;
		self.recordDuration = RECORDING_DURATION_SEC;
		
		// O setter de recordFilename criar o objeto audioRecorder
		if( !audioRecorder )
			goto Error;
		
		// Habilita input via devices bluetooth
		UInt32 allowBluetoothInput = 1;
		AudioSessionSetProperty( kAudioSessionProperty_OverrideCategoryEnableBluetoothInput,
								sizeof( allowBluetoothInput ),
								&allowBluetoothInput );		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( void )dealloc
{
	delegate = nil;
	session = nil;
	
	self.recordFilename = nil;
	[audioRecorder release];
	
	[super dealloc];
}


#pragma mark - Wrapper Methods

-( BOOL )startRecording
{
	BOOL ret = YES;
	if( [audioRecorder prepareToRecord] && [audioRecorder recordForDuration: self.recordDuration] )
	{
		recording = YES;
		interruptedOnRecording = NO;
	}
	else
	{
		ret = NO;
		
		NSString* errorMsg = NSLocalizedString( @"Could not start recording", nil );
		
		LOGE( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: AudioRecorderErrorCouldNotStartRecording
										 domain: AudioRecorderErrorDomain
						andLocalizedDescription: errorMsg];
		
		[delegate onAudioRecorderWrapper: self error: error];
	}
	return ret;
}

-( void )stopRecording
{
	if( [self isRecording] )
		[audioRecorder stop];
	
	recording = NO;
	interruptedOnRecording = NO;
}

-( BOOL )isRecording
{
	// TODO : Testar se esta é a melhor abordagem
	
	// recording é necessária já que, se sofrermos uma interrupção, audioRecorder.recording continuará YES, mas
	// não estaremos gravando de fato
	return audioRecorder.recording && recording;
}

-( BOOL )isRecordingAvailable
{
	return [session inputIsAvailable];
}


#pragma mark - AVAudioRecorderDelegate

-( void )audioRecorderDidFinishRecording:( AVAudioRecorder* )recorder successfully:( BOOL )flag
{
	// TODO : Verificar se este método é chamado quando recebemos um stop
	
	[self stopRecording];
	
	if( [delegate respondsToSelector: @selector( audioRecorderWrapperDidFinishRecording:successfully: )] )
		[delegate audioRecorderWrapperDidFinishRecording: self successfully: flag];
}

-( void )audioRecorderBeginInterruption:( AVAudioRecorder* )recorder
{
	if( recording )
	{
        recording = NO;
        interruptedOnRecording = YES;
		
		if( [delegate respondsToSelector: @selector( audioRecorderWrapperBeginInterruption: )] )
			[delegate audioRecorderWrapperBeginInterruption: self];
    }
}

-( void )audioRecorderEndInterruption:( AVAudioRecorder* )recorder
{
	if( interruptedOnRecording )
	{
        [self startRecording];
	
		if( [delegate respondsToSelector: @selector( audioRecorderWrapperEndInterruption: )] )
			[delegate audioRecorderWrapperEndInterruption: self];
	}
}

#pragma mark - Utils

-( BOOL )resetRecorder
{
	// Apaga o antigo (se existir)
	[audioRecorder release];
	audioRecorder = nil;
	
	// Cria o novo
	NSError* audioRecorderError = nil;
	NSURL* tempAudioFile = [NSURL fileURLForFileName: self.recordFilename];
	audioRecorder = [[AVAudioRecorder alloc] initWithURL: tempAudioFile settings: [AudioRecorderWrapper getSettings] error: &audioRecorderError];
	if( !audioRecorder )
	{
		NSString* errorMsg = [NSString stringWithFormat: @"%@: %@",
														 NSLocalizedString( @"Could not create AVAudioRecorder", nil ),
														 [audioRecorderError localizedDescription]];
		
		LOGE( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: AudioRecorderErrorCouldNotCreateRecorder
										 domain: AudioRecorderErrorDomain
						andLocalizedDescription: errorMsg];
		
		[delegate onAudioRecorderWrapper: self error: error];

		return NO;
	}
	
	audioRecorder.delegate = self;
	[audioRecorder prepareToRecord];
	
	return YES;
}

+( NSDictionary* )getSettings
{
	NSDictionary* settings = [NSDictionary  dictionaryWithObjectsAndKeys:
							  
							  // Retirado da documentação do vocoder:
							  // "The input sound files must be mono, 8- or 16-bit linear, uncompressed
							  // AIFF or WAVE files."
							  // Além disso, não faz sentido ser stereo porque o iphone só possui um microfone
							  [NSNumber numberWithInt: kAudioFormatLinearPCM], AVFormatIDKey, 
							  [NSNumber numberWithInt: 16], AVEncoderBitRateKey,
							  [NSNumber numberWithInt: 1], AVNumberOfChannelsKey,
							  
							  // Qualidade de CD
							  [NSNumber numberWithFloat: 44100.0], AVSampleRateKey,
							  
							  // Algumas flags
							  [NSNumber numberWithBool: NO], AVLinearPCMIsBigEndianKey,
							  [NSNumber numberWithBool: NO], AVLinearPCMIsFloatKey,
							  nil];
	
	return settings;
}

-( NSData* )getRecordedData
{
	NSURL* tempAudioFile = [NSURL fileURLForFileName: self.recordFilename];
	return [NSData dataWithContentsOfURL: tempAudioFile];
}

@end
