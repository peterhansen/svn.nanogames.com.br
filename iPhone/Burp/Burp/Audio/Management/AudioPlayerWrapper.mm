//
//  AudioPlayerWrapper.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/9/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "AudioPlayerWrapper.h"

// HT
#import "Logger.h"
#import "HTNSErrorUtils.h"

#pragma mark - Defines

// Domain dos erros informados por este componente
NSString *const AudioPlayerErrorDomain = @"AudioPlayerErrorDomain";


#pragma mark - Private Interface

@interface AudioPlayerWrapper( Private )

-( BOOL )startPlaying;

@end


#pragma mark - Implementation

@implementation AudioPlayerWrapper


#pragma mark - Acessors

@synthesize delegate;


#pragma mark - CTor & DTor

-( id )initWithSession:( AVAudioSession* )audioSession
{
	if( ( self = [super init] ) )
	{
		session = audioSession;

		playing = NO;
		interruptedOnPlayback = NO;
		
		// Enquanto não temos nada para tocar, o player ficará inválido, já que não é possível inicializá-lo
		// sem determinar o que ele irá tocar
		audioPlayer = nil;
		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( void )dealloc
{
	delegate = nil;

	session = nil;
	[audioPlayer release];

	[super dealloc];
}


#pragma mark - Wrapper Methods

-( BOOL )startPlaying
{
	BOOL ret = YES;
	if( [audioPlayer play] )
	{
		playing = YES;
		interruptedOnPlayback = NO;
	}
	else
	{
		ret = NO;
		
		NSString* errorMsg = NSLocalizedString( @"Could not start playing", nil );
		
		LOGE( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: AudioPlayerErrorCouldNotStartPlaying
										 domain: AudioPlayerErrorDomain
						andLocalizedDescription: errorMsg];

		[delegate onAudioPlayerWrapper: self error: error];
	}
	return ret;
}

-( BOOL )startPlayingData:( NSData* )data
{
	if( audioPlayer )
	{
		[audioPlayer release];
		audioPlayer = nil;
	}
	
	NSError* audioPlayerError = nil;
	audioPlayer = [[AVAudioPlayer alloc] initWithData: data error: &audioPlayerError];
	if( !audioPlayer )
	{
		NSString* errorMsg = NSLocalizedString( @"Could not create AVAudioPlayer", nil );
		
		LOGE( @"%@", errorMsg );
		
		NSError* error = [NSError errorWithCode: AudioPlayerErrorCouldNotCreatePlayer
										 domain: AudioPlayerErrorDomain
										 andLocalizedDescription:errorMsg];
		
		[delegate onAudioPlayerWrapper: self error: error];
		return NO;
	}
	audioPlayer.delegate = self;
	
	return [self startPlaying];
}

-( void )stopPlaying;
{
	if( audioPlayer && [self isPlaying] )
		[audioPlayer stop];
	
	playing = NO;
	interruptedOnPlayback = NO;
}

-( void )pausePlaying
{
	if( audioPlayer )
		[audioPlayer pause];
}

-( BOOL )isPlaying
{
	if( !audioPlayer )
		return NO;
	
	// TODO : Testar se esta é a melhor abordagem
	
	// playing é necessária já que, se sofrermos uma interrupção, audioPlayer.playing continuará YES, mas
	// não estaremos tocando algum som de fato
	return audioPlayer.playing && playing;
}


#pragma mark - AVAudioPlayerDelegate

-( void )audioPlayerDidFinishPlaying:( AVAudioPlayer* )player successfully:( BOOL )flag
{
	// TODO : Verificar se este método é chamado quando recebemos um stop
	
	[self stopPlaying];
	
	if( [delegate respondsToSelector: @selector( audioPlayerWrapperDidFinishPlaying:successfully: )] )
		[delegate audioPlayerWrapperDidFinishPlaying: self successfully: flag];
}

-( void )audioPlayerBeginInterruption:( AVAudioPlayer* )player
{	
    if( playing )
	{
        playing = NO;
        interruptedOnPlayback = YES;
		
		if( [delegate respondsToSelector: @selector( audioPlayerWrapperBeginInterruption: )] )
			[delegate audioPlayerWrapperBeginInterruption: self];
    }
}

-( void )audioPlayerEndInterruption:( AVAudioPlayer* )player
{	
    if( interruptedOnPlayback )
	{
        [self startPlaying];
	
		if( [delegate respondsToSelector: @selector( audioPlayerWrapperEndInterruption: )] )
			[delegate audioPlayerWrapperEndInterruption: self];
	}
}

@end
