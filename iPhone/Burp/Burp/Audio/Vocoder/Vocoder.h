//
//  Vocoder.h
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef VOCODER_H
#define VOCODER_H

// C++
#include <string>

// Vocoder
#include "VocoderConfig.h"
#include "wave.h"
#include "fft.h"

class VocoderListener
{
	public:
		virtual void onVocoderError( const std::string& error ) = 0;
		virtual void onVocoderStart( VINT numFrames ) = 0;
		virtual void onVocoderUpdate( VINT frameNum ) = 0;
		virtual void onVocoderFinish() = 0;
};

class Vocoder
{
	public:
		Vocoder( const std::string& modulator, const std::string& carrier, const std::string& output );
		~Vocoder();
	
		void setVolume( VREAL volume );
		inline VREAL getVolume() const { return vocode_volume; };
	
		void setBandCount( VINT bandCount );
		inline VINT getBandCount() const { return vocode_band_count; };
	
		void setNormalize( VBOOL b );
		inline VBOOL isNormalized() const { return vocode_normalize; };
		
		void setWindowLength( size_t windowLength );
		inline size_t getWindowLength() const { return vocode_window_length; };
		
		void setWindowOverlap( size_t windowOverlap );
		inline size_t getWindowOverlap() const { return vocode_window_overlap; };
	
		void setListener( VocoderListener* listener );
		inline VocoderListener* getListener() const { return listener; };
	
		bool run( std::string& error );
	
		static bool GetDefaultWindowLengthAndOverlap( const std::string& filename, size_t& windowLength,
													  size_t& windowOverlap, std::string& error );

	private:
		// TODO : Mudar para uma struct
		// Forma bizarra de falar "ponteiro para um array VREAL[2] deve se chamar COMPLEX_ARRAY"
		typedef VREAL ( COMPLEX_NUMBER )[2];
		typedef VREAL ( *COMPLEX_ARRAY )[2];
	
		// Memory Management
		void cleanup();
		static void freeIfNotNull( void** pp );
		void freeMemory();
		void allocateMemory();
		static void fftCreateArrays( REAL** c, REAL** s, int** rev, int n );
	
		// Settings Check
		void generateDefaultWindowLengthAndOverlap();
		bool areSettingsOk( std::string& error ) const;
	
		// File System
		bool openFiles( std::string& error );
		static void waveCloseIfOpen( WAVE_FILE** pp );
		void closeFiles();

		// Main loop
		void vocode();
		void vocode_window( VREAL* modulator, COMPLEX_ARRAY carrier, COMPLEX_ARRAY output );
		void vocoder();
		void loop( WAVE_FILE* source, SAMPLE* dest, size_t length );

		// Convertions
		size_t read_zero( WAVE_FILE* source, SAMPLE* dest, size_t length );
		void sample_to_complex_array( SAMPLE* sample_array, COMPLEX_ARRAY complex_array, size_t length, SAMPLE max_magnitude );	
		void sample_to_real_array( SAMPLE* sample_array, VREAL* real_array, size_t length, SAMPLE max_magnitude );
		void complex_to_sample_array( COMPLEX_ARRAY complex_array, SAMPLE* sample_array, size_t length, SAMPLE max_magnitude, VREAL vocode_volume );
	
		// Settings
		VBOOL got_window_length;
		VBOOL got_window_overlap;
		VBOOL vocode_normalize;
		VREAL vocode_volume;
		VINT vocode_band_count;

		size_t vocode_window_length;
		size_t vocode_window_overlap;
	
		std::string vocode_modulator_filename;
		std::string vocode_carrier_filename;
		std::string vocode_output_filename;
	
		// Carrier File
		WAVE_FILE* carrier_file;
		SAMPLE carrier_max_magnitude;
		
		// Modulator File
		WAVE_FILE* modulator_file;
		SAMPLE modulator_max_magnitude;
		VINT modulator_length;
		VINT vocode_modulator_rate;
		
		// Output File
		WAVE_FILE* output_file;
		SAMPLE output_max_magnitude;
	
		// Buffers
		SAMPLE* modulator_sample_buffer;
		SAMPLE* carrier_sample_buffer;
		SAMPLE* output_sample_buffer1; 
		SAMPLE* output_sample_buffer2;
		
		VREAL* modulator;
	
		COMPLEX_ARRAY looped_carrier; 
		COMPLEX_ARRAY output;
	
		REAL* fft_c;
		REAL* fft_s;
		int* fft_rev;
	
		// Listener
		VocoderListener* listener;
};

#endif
