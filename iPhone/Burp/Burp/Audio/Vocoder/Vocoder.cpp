//
//  Vocoder.cpp
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#include "Vocoder.h"

// C++
#include <cmath>
#include <new>

#pragma mark - Macros

#define DELETE_N_NULLIFY( p ) { delete p; p = NULL; }
#define DELETE_ARRAY_N_NULLIFY( p ) { delete[] p; p = NULL; }

#pragma mark - Defines

#define DEFAULT_WINDOW_TIME 15 // 1/15th of a second

#pragma mark - CTor & DTor

Vocoder::Vocoder( const std::string& modulator, const std::string& carrier, const std::string& output ) 
		:	got_window_length( FALSE ),
			got_window_overlap( FALSE ),
			vocode_normalize( TRUE ),
			vocode_volume( 1.0 ),
			vocode_band_count( 16 ),
			// Não podemos determinar um valor default para os próximos dois atributos, pois dependemos dos arquivos envolvidos
			// na operação para gerar tais valores e, nesse ponto, ainda não os abrimos e parseamos
			vocode_window_length( 0 ),
			vocode_window_overlap( 0 ),
			vocode_modulator_filename( modulator ),
			vocode_carrier_filename( carrier ),
			vocode_output_filename( output ),
			carrier_file( NULL ),
			carrier_max_magnitude( 0 ),
			modulator_file( NULL ),
			modulator_max_magnitude( 0 ),
			modulator_length( 0 ),
			vocode_modulator_rate( 0 ),
			output_file( NULL ),
			output_max_magnitude( 0 ),
			modulator_sample_buffer( NULL ),
			carrier_sample_buffer( NULL ),
			output_sample_buffer1( NULL ),
			output_sample_buffer2( NULL ),
			modulator( NULL ),
			looped_carrier( NULL ),
			output( NULL ),
			fft_c( NULL ),
			fft_s( NULL ),
			fft_rev( NULL ),
			listener( NULL )
{
}

Vocoder::~Vocoder()
{
	cleanup();
	listener = NULL;
}

#pragma mark - Memory Management

void Vocoder::cleanup()
{
	freeMemory();
	closeFiles();
}

void Vocoder::freeMemory()
{
	// TODO : Modificar para utilizar smart pointers e/ ou vectors
	DELETE_ARRAY_N_NULLIFY( output );
	DELETE_ARRAY_N_NULLIFY( looped_carrier );
	DELETE_ARRAY_N_NULLIFY( modulator );
	DELETE_ARRAY_N_NULLIFY( output_sample_buffer1 );
	DELETE_ARRAY_N_NULLIFY( output_sample_buffer2 );
	DELETE_ARRAY_N_NULLIFY( carrier_sample_buffer );
	DELETE_ARRAY_N_NULLIFY( modulator_sample_buffer );
	
	DELETE_ARRAY_N_NULLIFY( fft_c );
	DELETE_ARRAY_N_NULLIFY( fft_s );
	DELETE_ARRAY_N_NULLIFY( fft_rev );
}

void Vocoder::allocateMemory()
{
	// TODO : Modificar para utilizar smart pointers e/ ou vectors
	
	// Se alguém não conseguir alocar, vamos disparar uma exceção std::bad_alloc
	modulator_sample_buffer = new SAMPLE[ vocode_window_length ];
	carrier_sample_buffer = new SAMPLE[ vocode_window_length ];
	output_sample_buffer1 = new SAMPLE[ vocode_window_length ];
	output_sample_buffer2 = new SAMPLE[ vocode_window_length ];
	modulator = new VREAL[ vocode_window_length ];
	
	// TODO : Quando mudar COMPLEX_ARRAY para um array de structs, retirar estes casts grosseiros
	looped_carrier = ( COMPLEX_ARRAY )new VREAL[ 2 * vocode_window_length ];
	output = ( COMPLEX_ARRAY )new VREAL[ 2 * vocode_window_length ];
}

void Vocoder::fftCreateArrays( REAL** c, REAL** s, int** rev, int n )
{
    // Compute temp array of sins and cosines
    *c = new REAL[ n ];
    *s = new REAL[ n ];
    *rev = new int[ n ];
 
	int nu = ilog2( n );
    for( int i = 0 ; i < n ; i++ )
	{
        REAL arg = 2 * M_PI * i/n;
        (*c)[i] = cos(arg);
        (*s)[i] = sin(arg);
        (*rev)[i] = bitrev(i, nu);
    }
}

#pragma mark - Acessors

void Vocoder::setVolume( VREAL volume )
{
	vocode_volume = volume;
}

void Vocoder::setBandCount( VINT bandCount )
{
	vocode_band_count = bandCount;
}

void Vocoder::setNormalize( VBOOL b )
{
	vocode_normalize = b;
}

void Vocoder::setWindowLength( size_t windowLength )
{
	vocode_window_length = windowLength;
	got_window_length = TRUE;
}

void Vocoder::setWindowOverlap( size_t windowOverlap )
{
	vocode_window_overlap = windowOverlap;
	got_window_overlap = TRUE;
}

void Vocoder::setListener( VocoderListener *listener )
{
	this->listener = listener;
}

#pragma mark - Main Loop

bool Vocoder::run( std::string& error )
{
	// Verifica se os arquivos de fato existem
	// TODO
	
	// Abre os arquivos
	if( !openFiles( error ) )
		return false;
	
	// Checa as configurações
	generateDefaultWindowLengthAndOverlap();
	
	if( !areSettingsOk( error ) )
		return false;
	
	// Aplica os filtros
	bool ret = true;
	try
	{
		vocode();
	}
	catch( std::bad_alloc& ex )
	{
		ret = false;
	}
	
	// Limpa a memória
	cleanup();
	
	return ret;
}

void Vocoder::vocode()
{
	allocateMemory();
	
	// OLD : Essa porra tosca nunca deleta a memória alocada em fft_c, fft_s e fft_rev!!!!
	// Acrescentamos estas variáveis a nossa classe e vamos deletá-las em freeMemory. Também
	// emulamos o método original aqui para termos melhor controle sobre a (des)alocação de memória
	//fft_create_arrays( &fft_c, &fft_s, &fft_rev, vocode_window_length );
	fftCreateArrays( &fft_c, &fft_s, &fft_rev, vocode_window_length );
	
	vocoder();
	
	cleanup();
}

void Vocoder::vocode_window( VREAL *modulator, COMPLEX_ARRAY carrier, COMPLEX_ARRAY output )
{
	int band_no, band_length, extra_band_length;
	
	band_length = vocode_window_length / (vocode_band_count * 2);
	extra_band_length = vocode_window_length / 2 - band_length * (vocode_band_count - 1);
	
	realfftmag(modulator, vocode_window_length);
	fft(carrier, vocode_window_length, fft_c, fft_s, fft_rev);
	normalize_fft(carrier, vocode_window_length);
	
	for (band_no = 0; band_no < vocode_band_count; band_no++) {
		int i, j, k, l;
		VREAL m, c;
		
		l = (band_no == vocode_band_count - 1) ? extra_band_length : band_length;
		
		m = 0; c = 0;
		for (i = 0, j = band_no * band_length, k = vocode_window_length - j - 1;
			 i < l; i++, j++, k--)
		{
			if (vocode_normalize) {
				VREAL c1 = carrier[j][0]*carrier[j][0] + carrier[j][1]*carrier[j][1],
				c2 = carrier[k][0]*carrier[k][0] + carrier[k][1]*carrier[k][1];
				c += sqrt(c1) + sqrt(c2);
			}
			m += modulator[j];
		}
		
		if (!vocode_normalize) c = 1.0;
		if (c == 0) c = 0.0001;
		
		for (i = 0, j = band_no * band_length, k = vocode_window_length - j - 1;
			 i < l; i++, j++, k--) {
			output[j][0] = carrier[j][0] * m / c;
			output[j][1] = carrier[j][1] * m / c;
			output[k][0] = carrier[k][0] * m / c;
			output[k][1] = carrier[k][1] * m / c;
		}
	}
	
	invfft (output, vocode_window_length, fft_c, fft_s, fft_rev);
}

void Vocoder::vocoder()
{
	size_t i;
	SAMPLE *output_old = output_sample_buffer1,
	*output_new = output_sample_buffer2, *output_temp;
	VINT num_frames, frame_no;
	
	num_frames = (modulator_length - vocode_window_overlap) /
	(vocode_window_length - vocode_window_overlap);
	frame_no = 0;
	
	read_zero(modulator_file, modulator_sample_buffer, vocode_window_length);
	loop(carrier_file, carrier_sample_buffer, vocode_window_length);
	
	sample_to_real_array(modulator_sample_buffer, modulator, vocode_window_length,
						 modulator_max_magnitude);
	sample_to_complex_array(carrier_sample_buffer, looped_carrier,
							vocode_window_length, carrier_max_magnitude);
	
	vocode_window(modulator, looped_carrier, output);
	
	complex_to_sample_array(output, output_old, vocode_window_length,
							output_max_magnitude, vocode_volume);
	wave_write(output_file, output_old, vocode_window_length - vocode_window_overlap);
	
	for (i = 0; i < vocode_window_overlap; ++i)
    {
		modulator_sample_buffer[i] =
        modulator_sample_buffer[vocode_window_length - vocode_window_overlap + i];
		carrier_sample_buffer[i] =
        carrier_sample_buffer[vocode_window_length - vocode_window_overlap + i];
    }
	
	if( listener )
		listener->onVocoderStart( num_frames );
	
	while (read_zero(modulator_file, modulator_sample_buffer +
					 vocode_window_overlap, vocode_window_length - vocode_window_overlap))
    {
		if( listener )
			listener->onVocoderUpdate( frame_no );
		
		loop(carrier_file, carrier_sample_buffer + vocode_window_overlap,
			 vocode_window_length - vocode_window_overlap);
		
		sample_to_real_array(modulator_sample_buffer, modulator, vocode_window_length,
							 modulator_max_magnitude);
		sample_to_complex_array(carrier_sample_buffer, looped_carrier,
								vocode_window_length, carrier_max_magnitude);
		
		vocode_window(modulator, looped_carrier, output);
		
		complex_to_sample_array(output, output_new, vocode_window_length,
								output_max_magnitude, vocode_volume);
		
		for (i = 0; i < vocode_window_overlap; ++i)
        { 
			output_new[i] = (SAMPLE)((output_new[i] * (i / (double)vocode_window_overlap)) +
									 (output_old[vocode_window_length - vocode_window_overlap + i] *
									  ((vocode_window_overlap - i) / (double)vocode_window_overlap)));
        }
		
		wave_write(output_file, output_new, vocode_window_length - vocode_window_overlap);
		
		for (i = 0; i < vocode_window_overlap; ++i)
        {
			modulator_sample_buffer[i] =
            modulator_sample_buffer[vocode_window_length - vocode_window_overlap + i];
			carrier_sample_buffer[i] =
            carrier_sample_buffer[vocode_window_length - vocode_window_overlap + i];
        }
		
		output_temp = output_new;
		output_new = output_old;
		output_old = output_temp;
		
		++frame_no;
    }
	
	wave_write( output_file, output_old + vocode_window_length - vocode_window_overlap, vocode_window_overlap );
	
	if( listener )
	{
		listener->onVocoderUpdate( frame_no /* TODO Retirar - 1 */ );
		listener->onVocoderFinish();
	}
}

void Vocoder::loop( WAVE_FILE* source, SAMPLE* dest, size_t length )
{
	while( length > 0 )
    {
		size_t n = wave_read( source, dest, length );
		if( n < length ) 
			wave_seek( source, 0 );
		
		dest += n;
		length -= n;
    }
}

#pragma mark - File System

bool Vocoder::openFiles( std::string& error )
{
	WAVE_INFO wave_info;
	carrier_file = wave_open( vocode_carrier_filename.c_str(), &wave_info );
	if( wave_info.channels != 1 )
	{
		error.append( "carrier must be mono (1 channel)" );
		return false;
	}
	carrier_max_magnitude = (1 << (wave_info.bits - 1)) - 1;
	
	modulator_file = wave_open( vocode_modulator_filename.c_str(), &wave_info );
	if( wave_info.channels != 1 )
	{
		error.append( "modulator must be mono (1 channel)" );
		return false;
	}
	modulator_max_magnitude = ( 1 << ( wave_info.bits - 1 )) - 1;
	modulator_length = wave_info.length;
	vocode_modulator_rate = wave_info.rate;
	
	output_file = wave_create( vocode_output_filename.c_str(), &wave_info );
	output_max_magnitude = ( 1 << ( wave_info.bits - 1 )) - 1;
	
	return true;
}

void Vocoder::waveCloseIfOpen( WAVE_FILE** pp )
{
	if( *pp != NULL )
	{
		wave_close( *pp );
		*pp = NULL;
	}
}

void Vocoder::closeFiles()
{
	waveCloseIfOpen( &output_file );
	waveCloseIfOpen( &modulator_file );
	waveCloseIfOpen( &carrier_file );
}


#pragma mark - Static methods

bool Vocoder::GetDefaultWindowLengthAndOverlap( const std::string& filename, size_t& windowLength,
												size_t& windowOverlap, std::string& error )
{
	WAVE_INFO waveInfo;
	WAVE_FILE* temp = wave_open( filename.c_str(), &waveInfo );
	
	if( temp == NULL )
	{
		error.append( "Could not open file" );
		return false;
	}
	
	if( waveInfo.channels != 1 )
	{
		error.append( "Sound must be mono (1 channel)" );
		return false;
	}
	
	windowLength = ipow( 2, ilog2( waveInfo.rate / DEFAULT_WINDOW_TIME ));
	windowOverlap = windowLength >> 1;
	
	return true;
}


#pragma mark - Settings Check

void Vocoder::generateDefaultWindowLengthAndOverlap()
{
	if( !got_window_length )
		vocode_window_length = ipow( 2, ilog2( vocode_modulator_rate / DEFAULT_WINDOW_TIME ));
	
	if( !got_window_overlap )
		vocode_window_overlap = vocode_window_length >> 1;
}

bool Vocoder::areSettingsOk( std::string& error ) const
{
#define MAX_BUFFER_SIZE 128
	
	char buffer[ MAX_BUFFER_SIZE ];
	
	if(( vocode_window_length < 2 ) || (( size_t )ipow( 2, ilog2( vocode_window_length )) != vocode_window_length ))
	{
		snprintf( buffer,
				 MAX_BUFFER_SIZE,
				 "window length must be > 1 and a power of two (the closest power of two to the number you entered is %d)",
				 ipow( 2, ilog2( vocode_window_length )) );

		error.append( buffer );
		return false;
	}
	
	if( vocode_window_overlap > ( vocode_window_length >> 1 ))
	{
		snprintf( buffer,
				 MAX_BUFFER_SIZE,
				 "window overlap must be <= window length/2 (which is %zu)",
				 ( vocode_window_length >> 1 ));
		
		error.append( buffer );
		return false;
	}
	
	if(( vocode_band_count < 1 ) || (( size_t )vocode_band_count > ( vocode_window_length >> 1 )))
	{
		snprintf( buffer,
				 MAX_BUFFER_SIZE,
				 "band count must be > 0 and <= window length/2 (which is %zu)",
				 ( vocode_window_length >> 1 ));
		
		error.append( buffer );
		return false;
	}
	
#undef MAX_BUFFER_SIZE
	
	return true;
}

#pragma mark - Convertions

size_t Vocoder::read_zero( WAVE_FILE* source, SAMPLE* dest, size_t length )
{
	size_t n = wave_read( source, dest, length );
	for( size_t i = n ; i < length ; ++i )
		dest[i] = 0;
	return n;
}

void Vocoder::sample_to_complex_array( SAMPLE* sample_array, COMPLEX_ARRAY complex_array, size_t length, SAMPLE max_magnitude )
{
	for( size_t i = 0; i < length; ++i )
    {
		complex_array[i][0] = sample_array[i] / (VREAL)max_magnitude;
		complex_array[i][1] = 0;
    }
}

void Vocoder::sample_to_real_array( SAMPLE* sample_array, VREAL* real_array, size_t length, SAMPLE max_magnitude )
{
	for( size_t i = 0; i < length; ++i )
		*real_array++ = *sample_array++ / ( VREAL )max_magnitude;
}

void Vocoder::complex_to_sample_array( COMPLEX_ARRAY complex_array, SAMPLE* sample_array, size_t length,
									   SAMPLE max_magnitude, VREAL vocode_volume )
{
	for( size_t i = 0; i < length; ++i )
    {
		VREAL sample = complex_array[i][0] * vocode_volume;
		
		if( sample < -1.0 )
			sample = -1.0;
		else if( sample > 1.0 )
			sample = 1.0;
		
		sample_array[i] = ( SAMPLE )( sample * max_magnitude );
    }
}
