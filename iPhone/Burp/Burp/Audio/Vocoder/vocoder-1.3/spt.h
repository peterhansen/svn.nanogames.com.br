/* $Id: spt.h,v 1.4 2002/09/20 02:30:51 emanuel Exp $ */

#ifndef _SPT_
#define _SPT_

#include <stdlib.h>

#ifndef FALSE
#define FALSE	(1==2)
#endif

#ifndef TRUE
#define TRUE		(1==1)
#endif

#ifndef sqr
#define sqr(x)		((x)*(x))
#endif

#endif /* _SPT */
