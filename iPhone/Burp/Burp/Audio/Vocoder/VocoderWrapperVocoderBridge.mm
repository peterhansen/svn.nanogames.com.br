//
//  VocoderWrapperVocoderBridge.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/16/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#include "VocoderWrapperVocoderBridge.h"

// HT
#import "HTNSErrorUtils.h"
#import "VocoderWrapper.h"

void VocoderWrapperVocoderBridge::onVocoderError( const std::string& error )
{
	if( bridgeTarget )
	{
		id< VocoderWrapperDelegate > delegate = bridgeTarget.delegate;
		if( delegate != nil )
		{
			NSString* errMsg = [NSString stringWithFormat: @"%s", error.c_str()];
			
			NSError* error = [NSError errorWithCode: VocoderWrapperErrorInternal
											 domain: VocoderWrapperErrorDomain
							andLocalizedDescription: errMsg];
			
			[delegate onVocoder: bridgeTarget error: error];
		}
	}
}

void VocoderWrapperVocoderBridge::onVocoderStart( VINT numFrames )
{
	if( bridgeTarget )
	{
		id< VocoderWrapperDelegate > delegate = bridgeTarget.delegate;
		if( delegate != nil )
			[delegate onVocoder: bridgeTarget startWithNFrames:( NSInteger )numFrames];
	}
}

void VocoderWrapperVocoderBridge::onVocoderUpdate( VINT frameNum )
{
	if( bridgeTarget )
	{
		id< VocoderWrapperDelegate > delegate = bridgeTarget.delegate;
		if( delegate != nil )
			[delegate onVocoder: bridgeTarget updateToFrameNumber:( NSInteger )frameNum];
	}
}

void VocoderWrapperVocoderBridge::onVocoderFinish()
{
	if( bridgeTarget )
	{
		id< VocoderWrapperDelegate > delegate = bridgeTarget.delegate;
		if( delegate != nil )
			[delegate onVocoderFinish: bridgeTarget];
	}
}

void VocoderWrapperVocoderBridge::setBridgeTarget( VocoderWrapper* target )
{
	bridgeTarget = target;
}
