//
//  VocoderConfig.h
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef VOCODER_CONFIG_H
#define VOCODER_CONFIG_H

#include <inttypes.h>

typedef int8_t VBOOL;
#define FALSE 0
#define TRUE 1

typedef uint32_t U32;
typedef int32_t S32;
typedef uint16_t U16;
typedef int16_t S16;
typedef uint8_t U8;
typedef int8_t S8;

typedef S32 VINT;
typedef S16 VSHORT;
typedef S8  VBYTE;
typedef U32 VUINT;
typedef U16 VUSHORT;
typedef U8  VUBYTE;

typedef double VREAL;

#endif
