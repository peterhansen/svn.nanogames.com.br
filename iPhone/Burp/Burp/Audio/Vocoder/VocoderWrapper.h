//
//  VocoderWrapper.h
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef VOCODER_WRAPPER_h
#define VOCODER_WRAPPER_h

#import <Foundation/Foundation.h>

// Domain dos erros informados por objetos desta classe
FOUNDATION_EXPORT NSString *const VocoderWrapperErrorDomain;

// Códigos de erro
enum
{
	VocoderWrapperErrorInvalidSettings = 1,
	VocoderWrapperErrorInternal = 2
};


@class VocoderWrapper;

@protocol VocoderWrapperDelegate< NSObject >

	@required
		-( void )onVocoder:( VocoderWrapper* )vocoder error:( NSError* )error;
		-( void )onVocoder:( VocoderWrapper* )vocoder startWithNFrames:( NSInteger )nFrames;
		-( void )onVocoder:( VocoderWrapper* )vocoder updateToFrameNumber:( NSInteger )frame;
		-( void )onVocoderFinish:( VocoderWrapper* )vocoder;

@end


@interface VocoderWrapper : NSObject 
{
	@private
		NSData* modulator;
		NSData* carrier;
		NSData* vocoded;
	
		NSString* carrierSoundPath;
		NSString* modulatorSoundPath;
		NSString* outputPath;
	
		NSInteger bandCount;
		NSUInteger windowLength;
		NSUInteger windowOverlap;
	
		id< VocoderWrapperDelegate > delegate;
}
@property( nonatomic, readwrite, assign )id< VocoderWrapperDelegate > delegate;

// TODO : Verificar se é melhor deixar um objeto vocoder criado para repassarmos diretamente as configurações,
// ao invés de armazenarmos para posterior inicialização
@property( nonatomic, readwrite, assign )NSInteger bandCount;
@property( nonatomic, readwrite, assign )NSUInteger windowLength;
@property( nonatomic, readwrite, assign )NSUInteger windowOverlap;

@property( nonatomic, readwrite, retain )NSString* carrierSoundPath;
@property( nonatomic, readwrite, retain )NSString* modulatorSoundPath;
@property( nonatomic, readwrite, retain )NSString* outputPath;

-( NSData* )run;

+( NSError* )getDefaultWndLength:( NSUInteger* )windowLength andWndOverlap:( NSUInteger* )windowOverlap forFile:( NSString* )filename;

@end

#endif

