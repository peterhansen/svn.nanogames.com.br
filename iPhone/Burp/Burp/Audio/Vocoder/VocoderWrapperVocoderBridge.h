//
//  VocoderWrapperVocoderBridge.h
//  Burp
//
//  Created by Daniel L. Alves on 4/16/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#ifndef VOCODER_WRAPPER_VOCODER_BRIDGE_H
#define VOCODER_WRAPPER_VOCODER_BRIDGE_H

#include "Vocoder.h"
#include "VocoderWrapper.h"

class VocoderWrapperVocoderBridge : public VocoderListener
{
	public:
		virtual void onVocoderError( const std::string& error );
		virtual void onVocoderStart( VINT numFrames );
		virtual void onVocoderUpdate( VINT frameNum );
		virtual void onVocoderFinish();
	
		void setBridgeTarget( VocoderWrapper* target );
		inline VocoderWrapper* getBridgeTarget() const { return bridgeTarget; };
	
	private:
		VocoderWrapper* bridgeTarget;
};

#endif
