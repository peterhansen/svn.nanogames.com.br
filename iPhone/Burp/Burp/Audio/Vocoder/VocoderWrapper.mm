//
//  VocoderWrapper.mm
//  Burp
//
//  Created by Daniel L. Alves on 4/12/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "VocoderWrapper.h"

// HT
#import "Logger.h"
#import "HTNSErrorUtils.h"
#import "HTNSURLUtils.h"

#include "Vocoder.h"
#include "VocoderWrapperVocoderBridge.h"

#pragma mark - Defines

#define DEFAULT_BAND_COUNT 120

// Domain dos erros informados por objetos desta classe
NSString *const VocoderWrapperErrorDomain = @"VocoderWrapperErrorDomain";


#pragma mark - Private Interface

@interface VocoderWrapper()

@property( nonatomic, readwrite, retain )NSData* modulator;
@property( nonatomic, readwrite, retain )NSData* carrier;
@property( nonatomic, readwrite, retain )NSData* vocoded;

@end

@interface VocoderWrapper( Private )

+(  NSData* )loadSound:( NSString* )soundPath;

-( BOOL )checkSettings;

@end


#pragma mark - Implementation

@implementation VocoderWrapper


#pragma mark - Acessors

@synthesize delegate, bandCount, windowLength, windowOverlap, carrierSoundPath, modulatorSoundPath,
			outputPath, modulator, carrier, vocoded;


#pragma mark - CTor & DTor

-( id )init
{
	if( ( self = [super init] ) )
	{
		carrierSoundPath = nil;
		modulatorSoundPath = nil;
		outputPath = nil;
		
		modulator = nil;
		carrier = nil;
		vocoded = nil;
		
		bandCount = DEFAULT_BAND_COUNT;
		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( void )dealloc
{
	self.carrierSoundPath = nil;
	self.modulatorSoundPath = nil;
	self.outputPath = nil;
	
	self.modulator = nil;
	self.carrier = nil;
	self.vocoded = nil;
	
	[super dealloc];
}


#pragma mark - Sound Loading

+( NSData* )loadSound:( NSString* )soundPath
{
	return [NSData dataWithContentsOfURL: [NSURL fileURLWithPath: soundPath]];
}


#pragma mark - Settings

-( BOOL )checkSettings
{
	NSString* errorMsg = nil;
	
	if( !carrierSoundPath )
	{
		errorMsg = NSLocalizedString( @"Carrier sound path not set. Aborting.", nil );
		goto Error;
	}
	
	if( !modulatorSoundPath )
	{
		errorMsg = NSLocalizedString( @"Modulator sound path not set. Aborting.", nil );
		goto Error;
	}
	
	if( !outputPath )
	{
		errorMsg = NSLocalizedString( @"Output sound path not set. Aborting.", nil );
		goto Error;
	}
	
	return YES;
	
	Error:
		LOGE( @"%@", errorMsg );

		[delegate onVocoder: self error: [NSError errorWithCode: VocoderWrapperErrorInvalidSettings 
														 domain: VocoderWrapperErrorDomain 
										andLocalizedDescription: errorMsg]];
		return NO;
}


#pragma mark - Main Loop

-( NSData* )run
{
	if( ![self checkSettings] )
		return nil;
	
	Vocoder vocoder( [modulatorSoundPath UTF8String],
					 [carrierSoundPath UTF8String],
					 [outputPath UTF8String] );
	
	vocoder.setBandCount( bandCount );
	vocoder.setWindowLength( windowLength );
	vocoder.setWindowOverlap( windowOverlap );
	
	VocoderWrapperVocoderBridge delegateBridge;
	delegateBridge.setBridgeTarget( self );
	vocoder.setListener( &delegateBridge );

	std::string error;
	if( !vocoder.run( error ) )
	{
		NSString* errMsg = [NSString stringWithFormat: @"%@: %s", 
													   NSLocalizedString( @"Vocoder did not succeeded", nil ),
													   error.c_str()];
		LOGE( @"%@", errMsg );
		
		[delegate onVocoder: self error: [NSError errorWithCode: VocoderWrapperErrorInvalidSettings 
														 domain: VocoderWrapperErrorDomain 
										andLocalizedDescription: errMsg]];
		return nil;
	}

	return [VocoderWrapper loadSound: outputPath];
}


#pragma mark - Static Methods

+( NSError* )getDefaultWndLength:( NSUInteger* )windowLength andWndOverlap:( NSUInteger* )windowOverlap forFile:( NSString* )filename
{
	std::string errorDesc;
	
	size_t length, overlap;
	bool ret = Vocoder::GetDefaultWindowLengthAndOverlap( [filename UTF8String], length, overlap, errorDesc );
	
	if( ret )
	{
		if( windowLength )
			*windowLength = ( NSUInteger )length;
		
		if( windowOverlap )
			*windowOverlap = ( NSUInteger )overlap;
			
		return nil;
	}
	
	return [NSError errorWithCode: VocoderWrapperErrorInvalidSettings
						   domain: VocoderWrapperErrorDomain
		  andLocalizedDescription: [NSString stringWithFormat: @"%s", errorDesc.c_str()]];
}

@end
