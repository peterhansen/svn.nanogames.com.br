//
//  BurpViewController.m
//  Burp
//
//  Created by Daniel L. Alves on 4/4/11.
//  Copyright 2011 Hermit Tommy. All rights reserved.
//

#import "BurpViewController.h"

// HT
#import "HTUIButtonUtils.h"
#import "HTNSErrorUtils.h"
#import "HTNSStringUtils.h"
#import "Logger.h"

#pragma mark - Defines

// T�tulos dos bot�es
#define BUTTON_REC_TITLE_REC	@"Rec"
#define BUTTON_REC_TITLE_STOP	@"Stop"

#define BUTTON_PLAY_TITLE_PLAY	@"Play"
#define BUTTON_PLAY_TITLE_PAUSE	@"Pause"

// Configura��es do arquivo tempor�rio
#define RECORDING_FILENAME @"burp_temp"
#define RECORDING_DURATION_SEC 10.0

// Nome do arquivo final gerado
#define VOCODED_FULL_FILENAME @"final.wav"

// Nomes dos arquivos dos filtro de Vocode
#define VOCODER_FILTER_BURP01		@"burp01"
#define VOCODER_FILTER_BURP02		@"burp02"
#define VOCODER_FILTER_BURP03		@"burp03"
#define VOCODER_FILTER_BURP04		@"burp04"
#define VOCODER_FILTER_BURP05		@"burp05"
#define VOCODER_FILTER_BURP06		@"burp06"
#define VOCODER_FILTER_CREATURE01	@"creature01"
#define VOCODER_FILTER_CREATURE02	@"creature02"
#define VOCODER_FILTER_FROG			@"frog"
#define VOCODER_FILTER_LASER_BEAM	@"laser_beam"
#define VOCODER_FILTER_LION			@"lion"
#define VOCODER_FILTER_SYNTH		@"synth"
#define VOCODER_FILTER_WATER		@"water"
#define VOCODER_FILTER_WHITE_NOISE	@"white_noise"
#define VOCODER_FILTER_ZOMBIE		@"zombie"

#if DEBUG
	#define SERVER_URL @"http://staging.nanogames.com.br/burps/new"
#else
	#define SERVER_URL // TODO : Determinar url de produ��o
#endif

#define CONTENT_TYPE_WAV @"audio/wav"
#define CONTENT_TYPE_MP3 @"audio/mpeg3"


#pragma mark - Private Interface

@interface BurpViewController()

@property( nonatomic, readwrite, retain )NSString* lastFilter;

@end

@interface BurpViewController( Private )

-( BOOL )buildBurpViewController;
-( void )runVocoderWithFilter:( NSString* )filterName;

-( void )startVocoder;
-( void )finishVocoder;

+( void )showAlertWithMsg:( NSString* )msg andTitle:( NSString* )title;

@end

#pragma mark - Implementation

@implementation BurpViewController

#pragma mark - Acessors

@synthesize lastFilter;

#pragma mark - CTor & Dtor

-( id )init
{
	if( ( self = [super init] ) )
	{
		if( ![self buildBurpViewController] )
			goto Error;
		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( id )initWithNibName:( NSString* )nibNameOrNil bundle:( NSBundle* )nibBundleOrNil
{
	if( ( self = [super initWithNibName: nibNameOrNil bundle: nibBundleOrNil] ) )
	{
		if( ![self buildBurpViewController] )
			goto Error;
		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( id )initWithCoder:( NSCoder* )coder
{
	if( ( self = [super initWithCoder: coder] ) )
	{
		if( ![self buildBurpViewController] )
			goto Error;
		
		return self;
	}
	
	Error:
		[self release];
		return nil;
}

-( BOOL )buildBurpViewController
{
	{
		audioManager = [[AudioController alloc] init];
		if( !audioManager )
			goto Error;
		
		[audioManager setRecordingFilename: RECORDING_FILENAME];
		[audioManager setRecordingDurationSecs: RECORDING_DURATION_SEC];
		audioManager.delegate = self;
		
		conn = [[SmartConnection alloc] init];
		
		return true;
	}
	
	Error:
		return false;
}

-( void )dealloc
{
	self.lastFilter = nil;

	[audioManager release];
	
	[conn release];
	
    [super dealloc];
}

#pragma mark - View lifecycle

//-( void )didReceiveMemoryWarning
//{
//    // Releases the view if it doesn't have a superview.
//    [super didReceiveMemoryWarning];
//    
//    // Release any cached data, images, etc that aren't in use.
//}

-( void )viewDidLoad
{
    [super viewDidLoad];

	vocoderViewController.delegate = self;
	vocoderViewController.modulatorSoundPath = [audioManager getRecordingFilename];
}

//-( void )viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}

-( BOOL )shouldAutorotateToInterfaceOrientation:( UIInterfaceOrientation )interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

#pragma mark - Audio Capture

-( IBAction )onBtRecClick:( UIButton* )sender
{
	NSString* btTitle = btRec.titleLabel.text;
	if( [btTitle isEqualToString: BUTTON_REC_TITLE_STOP] )
	{
		[audioManager stopRecording];
		btTitle = BUTTON_REC_TITLE_REC;
	}
	else
	{
		[audioManager startRecording];
		btTitle = BUTTON_REC_TITLE_STOP;
	}
	[btRec setTitleForAllStates: btTitle];
}

-( IBAction )onBtPlayClick:( UIButton* )sender
{
	NSString* btTitle = btPlay.titleLabel.text;
	if( [btTitle isEqualToString: BUTTON_PLAY_TITLE_PAUSE] )
	{
		[audioManager pausePlaying];
		btTitle = BUTTON_PLAY_TITLE_PLAY;
	}
	else
	{
		[audioManager startPlaying: [audioManager getRecordedData]];
		btTitle = BUTTON_PLAY_TITLE_PAUSE;
	}
	[btPlay setTitleForAllStates: btTitle];
}

#pragma mark - AudioController Delegate( Player )

-( void )onAudioController:( AudioController* )controller error:( NSError* )error
{
	NSString* domain = nil;
	
	if( [[error domain] isEqualToString: AudioRecorderErrorDomain] )
	{
		domain = @"Recording";
	}
	else if( [[error domain] isEqualToString: AudioRecorderErrorDomain] )
	{
		domain = @"Playing";
	}
	else
	{
		domain = @"Setting Up";
	}
	
	NSString* title = [NSString stringWithFormat: @"Error While %@: ", domain];

	LOGE( @"%@: %@", title, [error localizedDescription] );
	
	[BurpViewController showAlertWithMsg: [error localizedDescription] andTitle: title];
}

-( void )playerBeginInterruption:( AudioController* )controller
{
	NSString* msg = @"Received player interruption";
	LOGI( @"%@", msg );
	
	[BurpViewController showAlertWithMsg: msg andTitle: @"Interruption"];
}

-( void )playerEndInterruption:( AudioController* )controller
{
	NSString* msg = @"Received player interruption end";
	LOGI( @"%@", msg );
	
	[BurpViewController showAlertWithMsg: msg andTitle: @"Interruption"];
}

-( void )playerDidFinishPlaying:( AudioController* )controller successfully:( BOOL )flag
{
	[btPlay setTitleForAllStates: BUTTON_PLAY_TITLE_PLAY];
	
	NSString* info = [NSString stringWithFormat: @"Did finish playing successfully? %@", ( flag ? @"True" : @"False" )];

	LOGI( @"%@", info );
	
	[BurpViewController showAlertWithMsg: info andTitle: @"Player Status"];
}

#pragma mark - AudioController Delegate( Recorder )

-( void )recorderBeginInterruption:( AudioController* )controller
{
	NSString* msg = @"Received recorder interruption";
	LOGI( @"%@", msg );
	
	[BurpViewController showAlertWithMsg: msg andTitle: @"Interruption"];
}

-( void )recorderEndInterruption:( AudioController* )controller
{
	NSString* msg = @"Received recorder interruption end";
	LOGI( @"%@", msg );
	
	[BurpViewController showAlertWithMsg: msg andTitle: @"Interruption"];
}

-( void )recorderDidFinishRecording:( AudioController* )controller successfully:( BOOL )flag
{
	if( flag )
	{
		[btPlay setEnabled: [audioManager hasRecordedData]];
		
		NSUInteger windowLength, windowOverlap; 
		NSError* error = [VocoderWrapper getDefaultWndLength: &windowLength 
											   andWndOverlap: &windowOverlap 
													 forFile: [NSString getFullPathForFileName: [audioManager getRecordingFilename]]];
		
		if( error )
		{
			NSString* errorMsg = [NSString stringWithFormat:  @"Could not calculate default window length and overlap: %@", [error localizedDescription]];
			LOGE( @"%@", errorMsg );
			[BurpViewController showAlertWithMsg: errorMsg andTitle: @"Recorder Status"];
		}
		else
		{
			[tbWindowLength setText: [NSString stringWithFormat: @"%d", windowLength]];
			[tbWindowOverlap setText: [NSString stringWithFormat: @"%d", windowOverlap]];
		}
	}
	
	[btRec setTitleForAllStates: BUTTON_REC_TITLE_REC];
	
	NSString* info = [NSString stringWithFormat: @"Did finish recording successfully? %@", ( flag ? @"True" : @"False" )];
	LOGI( @"%@", info );
	[BurpViewController showAlertWithMsg: info andTitle: @"Recorder Status"];
}

#pragma mark - Vocoder

-( void )runVocoderWithFilter:( NSString* )filterName
{
	self.lastFilter = filterName;
	
	if( [audioManager hasRecordedData] )
	{
		vocoderViewController.carrierSoundPath = filterName;
		[self startVocoder];
	}
	else
	{
		NSString* info = @"No recorded data to vocode";
		LOGI( @"%@", info );
		[BurpViewController showAlertWithMsg: info andTitle: @"Vocoder Status"];
	}
}

-( IBAction )onBtFilterBurp01Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_BURP01];
}

-( IBAction )onBtFilterBurp02Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_BURP02];
}

-( IBAction )onBtFilterBurp03Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_BURP03];
}

-( IBAction )onBtFilterBurp04Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_BURP04];
}

-( IBAction )onBtFilterBurp05Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_BURP05];
}

-( IBAction )onBtFilterBurp06Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_BURP06];
}

-( IBAction )onBtFilterCreature01Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_CREATURE01];
}

-( IBAction )onBtFilterCreature02Click:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_CREATURE02];
}

-( IBAction )onBtFilterFrogClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_FROG];
}

-( IBAction )onBtFilterLaserBeamClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_LASER_BEAM];
}

-( IBAction )onBtFilterLionClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_LION];
}

-( IBAction )onBtFilterSynthClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_SYNTH];
}

-( IBAction )onBtFilterWaterClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_WATER];
}

-( IBAction )onBtFilterWhiteNoiseClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_WHITE_NOISE];
}

-( IBAction )onBtFilterZombieClick:( UIButton* )sender
{
	[self runVocoderWithFilter: VOCODER_FILTER_ZOMBIE];
}

-( IBAction )onBandCountChanged:( UITextField* )sender
{
	NSInteger bandCount = [[sender text] integerValue];
	LOGI( @"Band count now is %d", bandCount );
	[vocoderViewController setBandCount: bandCount];
}

-( IBAction )onWindowLengthChanged:( UITextField* )sender
{
	NSUInteger windowLength = [[sender text] integerValue];
	LOGI( @"Window length now is %d", windowLength );
	[vocoderViewController setWindowLength: windowLength];
}

-( IBAction )onWindowOverlapChanged:( UITextField* )sender
{
	NSUInteger windowOverlap = [[sender text] integerValue];
	LOGI( @"Window overlap now is %d", windowOverlap );
	[vocoderViewController setWindowOverlap: windowOverlap];
}

-( IBAction )onBtPlayFilterClick:( UIButton* )sender
{
	if( self.lastFilter )
	{
		NSString* filterPath = [[NSBundle mainBundle] pathForResource: lastFilter ofType: @"wav"];
		[audioManager startPlaying: [NSData dataWithContentsOfFile: filterPath]];
	}
	else
	{
		NSString* info = @"No filter used until now";
		LOGI( @"%@", info );
		[BurpViewController showAlertWithMsg: info andTitle: @"Vocoder Status"];
	}
}

-( IBAction )onBtPlayVocodedClick:( UIButton* )sender
{
	NSData* vocodedData = vocoderViewController.vocodedData;
	
	if( vocodedData )
	{
		[audioManager startPlaying: vocodedData];
	}
	else
	{
		NSString* info = @"No vocoded data to play";
		LOGI( @"%@", info );
		[BurpViewController showAlertWithMsg: info andTitle: @"Vocoder Status"];
	}
}

#pragma mark - Audio Submission

-( IBAction )onBtSendClick:( UIButton* )sender
{
	NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString: SERVER_URL]];
	[request setHTTPMethod: @"POST"];
	
	NSString* contentType = @"multipart/form-data, boundary=AaB03x";
	[request setValue: contentType forHTTPHeaderField: @"Content-type"];
	
	NSData* boundary = [@"\r\n--AaB03x\r\n" dataUsingEncoding: NSUTF8StringEncoding];
	
	NSMutableData* postBody = [NSMutableData data];
	[postBody appendData: boundary];
	
	NSString* contentDisposition = [NSString stringWithFormat: @"Content-Disposition: form-data; name=\"burp\"; filename=\"%@\"", VOCODED_FULL_FILENAME];
	[postBody appendData: [contentDisposition dataUsingEncoding: NSUTF8StringEncoding]];
	
	NSString* soundContentType = [NSString stringWithFormat: @"Content-Type: %@\r\n\r\n", CONTENT_TYPE_WAV]; 
	[postBody appendData: [soundContentType dataUsingEncoding: NSUTF8StringEncoding]];

	[postBody appendData: vocoderViewController.vocodedData];
	
	[postBody appendData: boundary];
	
	[request setHTTPBody: postBody];
	[request setValue: [NSString stringWithFormat: @"%d", [postBody length]] forHTTPHeaderField: @"Content-Length" ];
	
	[conn startRequest: request];
}

#pragma mark - Utils

+( void )showAlertWithMsg:( NSString* )msg andTitle:( NSString* )title
{
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: title 
													message: msg delegate:nil 
										  cancelButtonTitle: NSLocalizedString( @"OK", nil )
										  otherButtonTitles: nil];
    [alert show];
    [alert release];
}

#pragma mark - BurpVocodingThreadViewControllerDelegate

-( void )startVocoder
{
	[self onBandCountChanged: tbBandCount];
	[self onWindowLengthChanged: tbWindowLength];
	[self onWindowOverlapChanged: tbWindowOverlap];
	 
	UIView* mainView = self.view;
	[mainView setUserInteractionEnabled: NO];
	[mainView addSubview: vocoderViewController.view];
	
	if( ![vocoderViewController start] )
	{
		NSString* errorMsg = NSLocalizedString( @"Could not start vocoder controller", nil );
		NSError* error = [NSError errorWithCode: VocoderWrapperErrorInternal domain: VocoderWrapperErrorDomain andLocalizedDescription: errorMsg];
		[self onBurpVocodingThreadViewController: vocoderViewController error: error];
	}
}

-( void )finishVocoder
{
	[vocoderViewController.view removeFromSuperview];
	
	[self.view setUserInteractionEnabled: YES];
}

-( void )onBurpVocodingThreadViewController:( BurpVocodingThreadViewController* )controller error:( NSError* )error
{
	[self finishVocoder];

	[BurpViewController showAlertWithMsg: [error localizedDescription] andTitle: @"Vocoder Error"];
}

-( void )onBurpVocodingThreadViewControllerFinished:( BurpVocodingThreadViewController* )controller
{
	[self finishVocoder];
	
	[BurpViewController showAlertWithMsg: @"Vocoder Succeeded" andTitle: @"Vocoder Status"];
}

@end
