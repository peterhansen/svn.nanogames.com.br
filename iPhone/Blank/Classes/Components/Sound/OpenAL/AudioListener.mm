#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include "AudioListener.h"

/*==============================================================================================

MÉTODO setGain
	Determina o "ganho" do listener. O valor passado deve ser positivo.

==============================================================================================*/

bool AudioListener::setGain( float gain )
{
	return AudioListener::SetListenerF( AL_GAIN, gain ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO getGain
	Obtém o "ganho" do listener. O valor retornado será sempre positivo.

==============================================================================================*/

bool AudioListener::getGain( float* pGain )
{
	return AudioListener::GetListenerF( AL_GAIN, pGain ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setPosition
	Determina a posição do listener.

==============================================================================================*/

bool AudioListener::setPosition( const Point3f* pPos )
{
	return AudioListener::SetListenerFV( AL_POSITION, pPos->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO getPosition
	Obtém a posição do listener.

==============================================================================================*/

bool AudioListener::getPosition( Point3f* pPos )
{
	return AudioListener::GetListenerFV( AL_POSITION, pPos->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setSpeed
	Determina a velocidade do listener.

==============================================================================================*/

bool AudioListener::setSpeed( const Point3f* pSpeed )
{
	return AudioListener::SetListenerFV( AL_VELOCITY, pSpeed->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO getSpeed
	Obtém a velocidade do listener.

==============================================================================================*/

bool AudioListener::getSpeed( Point3f* pSpeed )
{
	return AudioListener::GetListenerFV( AL_VELOCITY, pSpeed->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO setOrientation
	Determina a orientação do listener.

==============================================================================================*/

bool AudioListener::setOrientation( const Point3f* pOrientation )
{
	return AudioListener::SetListenerFV( AL_ORIENTATION, pOrientation->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO getOrientation
	Obtém a orientação do listener.

==============================================================================================*/

bool AudioListener::getOrientation( Point3f* pOrientation )
{
	return AudioListener::GetListenerFV( AL_ORIENTATION, pOrientation->p ) == AL_NO_ERROR;
}

/*==============================================================================================

MÉTODO SetListenerFV
	Executa o método alListenerfv sobre este objeto.

==============================================================================================*/

ALint AudioListener::SetListenerFV( ALenum param, const ALfloat* pValues )
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alListenerfv( param, pValues );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO GetListenerFV
	Executa o método alGetListenerfv sobre este objeto.

==============================================================================================*/

ALint AudioListener::GetListenerFV( ALenum param, ALfloat* pValues )
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alGetListenerfv( param, pValues );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO SetListenerFV
	Executa o método alListenerf sobre este objeto.

==============================================================================================*/

ALint AudioListener::SetListenerF( ALenum param, ALfloat value )
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alListenerf( param, value );
	
	// Retorna se houve erros
	return alGetError();
}

/*==============================================================================================

MÉTODO GetListenerFV
	Executa o método alGetListenerf sobre este objeto.

==============================================================================================*/

ALint AudioListener::GetListenerF( ALenum param, ALfloat* pValue )
{
	// Limpa o código de erro
	alGetError();
	
	// Executa a operação
	alGetListenerf( param, pValue );
	
	// Retorna se houve erros
	return alGetError();
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Orientation
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setListenerOrientation: (float) angle : (float*) outX : (float*) outZ
//{
//	mAngle = angle;
//	ALenum  error = AL_NO_ERROR;
//	float	rads = DEG2RAD(mAngle);
//	float	orientation[6] = {	0.0, 0.0, -1.0,    // direction
//								0.0, 1.0, 0.0	}; //up	
//								 
//	orientation[0] = cos(rads);
//	orientation[1] = sin(rads);		// No Change to the Z vector
//	gListenerDirection = RAD2DEG(atan2(orientation[1], orientation[0]));
//	
//	// Change OpenAL Listener's Orientation
//	orientation[0] = sin(rads);
//	orientation[1] = 0.0;			// No Change to the Y vector
//	orientation[2] = -cos(rads);	
//
//	alListenerfv(AL_ORIENTATION, orientation);
//	if((error = alGetError()) != AL_NO_ERROR)
//		printf("Error Setting Listener Orientation");
//		
//	// set the listener velocity as well, in this app we are always syncing the velocity to the direction that the listener is facing
//	[self setListenerVelocity: mVelocityScaler : outX : outZ];
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Listener Velocity Gain
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setListenerVelocity:(float) inVelocity : (float*) outX : (float*) outZ
//{
//	mVelocityScaler = inVelocity;
//	
//	ALenum  error = AL_NO_ERROR;
//	float	rads = DEG2RAD(mAngle);
//	float	velocity[3] = {	0.0, 0.0, 0.0}; //up	
//								 	
//	// Change OpenAL Listener's Orientation
//	velocity[0] = sin(rads) * mVelocityScaler;
//	velocity[1] = 0.0;			// No Change to the Y vector
//	velocity[2] = -cos(rads) * mVelocityScaler;
//	
//	if(outX) *outX = velocity[0];	
//	if(outZ) *outZ = velocity[2];	
//
//	alListenerfv(AL_VELOCITY, velocity);
//	if((error = alGetError()) != AL_NO_ERROR)
//		printf("Error Setting Listener Velocity");
//
//}

#endif COMPONENTS_CONFIG_ENABLE_OPENAL
