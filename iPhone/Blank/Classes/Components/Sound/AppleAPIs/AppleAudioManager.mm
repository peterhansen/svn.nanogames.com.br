#if COMPONENTS_CONFIG_ENABLE_APPLE_SOUND_API

#include "AppleAudioManager.h"

#include "Macros.h"
#include "MathFuncs.h"
#include "Exceptions.h"

#define BUFFER_SIZE_IN_BYTES 0x10000

// Inicializa as variáveis estáticas da classe
AppleAudioManager* AppleAudioManager::pSingleton = NULL;

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AppleAudioManager::AppleAudioManager( uint8 nAppSounds )
				  : volume( 1.0f ), paused( false ), nAppSounds( nAppSounds ), pSounds( NULL )
{
	// OBS: Esta classe poderia ser um template cujos parâmetros seriam
	// AppleAudioManager< maxSoundsType, maxSounds >
	// Assim pSounds não teria que ser alocado dinamicamente. Aliás, 
	// poderíamos até declará-lo como um boost:Array< maxSounds >
	pSounds = new AudioQueueData*[ nAppSounds ];
	if( pSounds )
		memset( pSounds, NULL, sizeof( AudioQueueData* ) * nAppSounds );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

AppleAudioManager::~AppleAudioManager( void )
{
	stopAllSounds();
	
	for( uint8 i = 0 ; i < nAppSounds ; ++i )
		unloadSound( i );

	DESTROY_VEC( pSounds );
}

/*==============================================================================================

MÉTODO Create
	 Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance().
No entanto, isso poderia resultar em exceções disparadas no meio do código da aplicação, o que
impactaria o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
chamado no início da aplicação, já que o usuário dos componentes parte do princípio que sempre
GetInstance() sempre retornará um valor válido.

==============================================================================================*/
 
bool AppleAudioManager::Create( uint8 nAppSounds )
{
	if( !pSingleton )
		pSingleton = new AppleAudioManager( nAppSounds );
	return pSingleton != NULL;
}

/*==============================================================================================

MÉTODO Destroy
	Deleta o singleton.

==============================================================================================*/

void AppleAudioManager::Destroy( void )
{
	DESTROY( pSingleton );
}

/*==============================================================================================

MÉTODO suspend
	Suspende a execução dos devices existentes.

==============================================================================================*/

bool AppleAudioManager::suspend( void )
{
	if( paused )
		return true;

	for( uint8 i = 0 ; i < nAppSounds ; ++i )
	{
		if( pSounds[i] != NULL )
		{
			if( isPlaying( i ) )
			{
				pSounds[i]->wasPlayingBeforeSuspend = true;

				OSStatus err = AudioQueuePause( pSounds[i]->pQueue );
				if( err != 0 )
				{
					// OBS: Talvez algum som já tenha tido sua execução suspensa...
					return false;
				}
			}
			else
			{
				pSounds[i]->wasPlayingBeforeSuspend = false;
			}
		}
	}

	paused = true;
	return true;
}

/*==============================================================================================

MÉTODO resume
	Resume a execução dos devices existentes.

==============================================================================================*/

bool AppleAudioManager::resume( void )
{
	if( !paused )
		return true;

	for( uint8 i = 0 ; i < nAppSounds ; ++i )
	{
		if( ( pSounds[i] != NULL ) && pSounds[i]->wasPlayingBeforeSuspend )
		{
			OSStatus err = AudioQueueStart( pSounds[i]->pQueue, NULL );
			if( err != 0 )
			{
				// OBS: Talvez algum som já tenha recomeçado a tocar...
				return false;
			}
		}
	}

	paused = false;
	return true;
}

/*==============================================================================================

MÉTODO isPlaying
	Indica se algum som está sendo tocado.

==============================================================================================*/

bool AppleAudioManager::isPlaying( uint8 soundIndex ) const
{
	if( pSounds[ soundIndex ] == NULL )
		return false;
	
	// OBS: Apenas retornar pSounds[ soundIndex ]->running não é muito seguro...
	UInt32 playing;
	UInt32 dataSize = sizeof( playing );
	AudioQueueGetProperty( pSounds[ soundIndex ]->pQueue, kAudioQueueProperty_IsRunning, &playing, &dataSize );
	return playing != 0;
}

/*==============================================================================================

MÉTODO AudioQueueBufferCallback
	Controla a execução do áudio.

==============================================================================================*/

void AppleAudioManager::AudioQueueBufferCallback( void* inUserData, AudioQueueRef inAudioQueue, AudioQueueBufferRef inCompleteAudioQueueBuffer )
{
	AudioQueueData* pSound = static_cast< AudioQueueData* >( inUserData );
	AppleAudioManager* pAudioManager = static_cast< AppleAudioManager* >( pSound->pAudioManager );

	// Se o reprodutor de sons está suspenso ou o som está pausado, retorna
    if( pAudioManager->paused || !pSound->running )
		return;

    UInt32 numBytes;
    UInt32 nPackets = pSound->numPacketsToRead;

    OSStatus result = AudioFileReadPackets( pSound->audioFile, false, &numBytes, pSound->pPacketDescs, pSound->currentPacket, &nPackets, inCompleteAudioQueueBuffer->mAudioData );
    if( result )
	{
		#if DEBUG
			LOG( @"AudioFileReadPackets failed: %d", result );
		#endif

		pAudioManager->stopSound( pSound->soundIndex );
		return;
	}

    if( nPackets > 0 )
	{
        UInt32 trimFramesAtStart = 0;
        UInt32 trimFramesAtEnd = 0;

        inCompleteAudioQueueBuffer->mAudioDataByteSize = numBytes; 
        inCompleteAudioQueueBuffer->mPacketDescriptionCount = nPackets;

        if( pSound->currentPacket == 0 )
		{
            // at the beginning -- need to trim priming frames
            trimFramesAtStart = pSound->mPrimingFrames;
        } 

        pSound->currentPacket += nPackets;

        if( ( pSound->currentPacket == pSound->totalPackets ) && pSound->looping )
		{
            // at the end -- need to trim remainder frames
            trimFramesAtEnd = pSound->mRemainderFrames;

            // reset read from the beginning again
            pSound->currentPacket = 0;
        }

		result = AudioQueueEnqueueBufferWithParameters( inAudioQueue, inCompleteAudioQueueBuffer, ( pSound->pPacketDescs ? nPackets : 0 ), pSound->pPacketDescs, trimFramesAtStart, trimFramesAtEnd, 0, NULL, NULL, NULL );

		if( result )
		{
			#if DEBUG
				LOG( @"AudioQueueBufferCallback( void*, AudioQueueRef, AudioQueueBufferRef ) - AudioQueueEnqueueBufferWithParameters failed: %d", result );
			#endif
			pAudioManager->stopSound( pSound->soundIndex );
			return;
		}
    }
	else
	{
		pAudioManager->stopSound( pSound->soundIndex );
	}
}

/*==============================================================================================

MÉTODO setVolume
	Determina o volume dos sons da aplicação.

==============================================================================================*/

void AppleAudioManager::setVolume( float volume )
{
	this->volume = NanoMath::clamp( volume, 0.0f, 1.0f );

	for( uint8 i = 0 ; i < nAppSounds ; ++i )
	{
		if( pSounds[i] != NULL )
			AudioQueueSetParameter( pSounds[i]->pQueue, kAudioQueueParam_Volume, this->volume );
	}
}

/*==============================================================================================

MÉTODO insertAudio
	Retira um som da memória.

==============================================================================================*/

bool AppleAudioManager::unloadSound( uint8 soundIndex )
{
	stopSound( soundIndex );
	DESTROY( pSounds[ soundIndex ] );
	return true;
}

/*==============================================================================================

MÉTODO loadSound
	Carrega um som para ser reproduzido posteriormente.

==============================================================================================*/

#define PLAY_SOUND_AUDIO_FILE_PATH_BUFFER_LEN 4

bool AppleAudioManager::loadSound( uint8 soundName, uint8 soundIndex )
{
	if( pSounds[ soundIndex ] != NULL )
		return true;

	// Aloca a estrutura que irá controlar a reprodução de sons
	pSounds[ soundIndex ] = new AudioQueueData( this, soundIndex );
	if( !pSounds[ soundIndex ] )
		return false;
	
	{ // Evita erros de compilação por causa dos gotos
		
		char fileName[ PLAY_SOUND_AUDIO_FILE_PATH_BUFFER_LEN ];
		snprintf( fileName, sizeof( char ) * PLAY_SOUND_AUDIO_FILE_PATH_BUFFER_LEN, "%d", static_cast< int32 >( soundName ) );
		AudioFileOpenURL( ( CFURLRef )[NSURL fileURLWithPath:Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( fileName ), @"caf" )], 0x01/*fsRdPerm*/, 0/*inFileTypeHint*/, &( pSounds[ soundIndex ]->audioFile ) );
		
		// Obtém o formato do arquivo
		UInt32 size = sizeof( pSounds[ soundIndex ]->dataFormat );
		AudioFileGetProperty( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyDataFormat, &size, &( pSounds[ soundIndex ]->dataFormat ) );
		
		// Agenda a reprodução do som
		AudioQueueNewOutput( &( pSounds[ soundIndex ]->dataFormat ), AudioQueueBufferCallback, pSounds[ soundIndex ], CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &( pSounds[ soundIndex ]->pQueue ) );

		// We have a couple of things to take care of now
		// (1) Setting up the conditions around VBR or a CBR format - affects how we will read from the file
		// If format is VBR we need to use a packet table
		UInt32 bufferSize = BUFFER_SIZE_IN_BYTES;
		if( ( pSounds[ soundIndex ]->dataFormat.mBytesPerPacket == 0 ) || ( pSounds[ soundIndex ]->dataFormat.mFramesPerPacket == 0 ) )
		{
			// first check to see what the max size of a packet is - if it is bigger
			// than our allocation default size, that needs to become larger
			UInt32 maxPacketSize;
			size = sizeof( maxPacketSize );
			AudioFileGetProperty( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyPacketSizeUpperBound, &size, &maxPacketSize );
			if( maxPacketSize > bufferSize ) 
				bufferSize = maxPacketSize;

			pSounds[ soundIndex ]->numPacketsToRead = bufferSize / maxPacketSize;
			
			// We also need packet descpriptions for the file reading
			pSounds[ soundIndex ]->pPacketDescs = new AudioStreamPacketDescription[ pSounds[ soundIndex ]->numPacketsToRead ];
			if( !pSounds[ soundIndex ]->pPacketDescs )
				goto Error;
		}
		else
		{
			pSounds[ soundIndex ]->numPacketsToRead = bufferSize / pSounds[ soundIndex ]->dataFormat.mBytesPerPacket;
			pSounds[ soundIndex ]->pPacketDescs = NULL;
		}

		size = sizeof( pSounds[ soundIndex ]->totalPackets );
		AudioFileGetProperty( pSounds[ soundIndex ]->audioFile, kAudioFileStreamProperty_AudioDataPacketCount, &size, &( pSounds[ soundIndex ]->totalPackets ) );

		// (2) If the file has a cookie, we should get it and set it on the AudioQueue
		size = sizeof( UInt32 );
		OSStatus result = AudioFileGetPropertyInfo( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyMagicCookieData, &size, NULL );
		if( !result && size )
		{
			char* pCookie = new char[ size ];
			if( !pCookie )
				goto Error;

			AudioFileGetProperty( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyMagicCookieData, &size, pCookie );
			AudioQueueSetProperty( pSounds[ soundIndex ]->pQueue, kAudioQueueProperty_MagicCookie, pCookie, size );

			DESTROY_VEC( pCookie );
		}
		
		// Channel layout?
		OSStatus err = AudioFileGetPropertyInfo( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyChannelLayout, &size, NULL );
		if( err == noErr && size > 0 )
		{
			AudioChannelLayout* acl = ( AudioChannelLayout* )malloc( size );
			if( acl == NULL )
				goto Error;

			AudioFileGetProperty( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyChannelLayout, &size, acl );
			AudioQueueSetProperty( pSounds[ soundIndex ]->pQueue, kAudioQueueProperty_ChannelLayout, acl, size );

			free( acl );
			acl = NULL;
		}
		
		AudioFilePacketTableInfo packetTableInfo;
		size = sizeof( packetTableInfo );
		AudioFileGetProperty( pSounds[ soundIndex ]->audioFile, kAudioFilePropertyPacketTableInfo, &size, &packetTableInfo );

		pSounds[ soundIndex ]->mPrimingFrames = packetTableInfo.mPrimingFrames;
		pSounds[ soundIndex ]->mRemainderFrames = packetTableInfo.mRemainderFrames;
		
		// Prime the queue with some data before starting
		AudioQueueAllocateBuffer( pSounds[ soundIndex ]->pQueue, bufferSize, &( pSounds[ soundIndex ]->pBuffer ) );
		
		return true;
	
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
		DESTROY( pSounds[ soundIndex ] );
		return false;
}

#undef PLAY_SOUND_AUDIO_FILE_PATH_BUFFER_LEN

/*==============================================================================================

MÉTODO playSound
	Executa um som da aplicação.

==============================================================================================*/

void AppleAudioManager::playSound( uint8 soundName, uint8 soundIndex, bool looping, bool playAgainIfAlreadyPlaying )
{
	if( !loadSound( soundName, soundIndex ) )
	{
		#if DEBUG
			LOG( @"Não foi possível iniciar a reprodução do som %d", soundName );
		#endif
		return;
	}
	
	if( isPlaying( soundIndex ) && playAgainIfAlreadyPlaying )
		stopSound( soundIndex );

	pSounds[ soundIndex ]->running = true;
	pSounds[ soundIndex ]->looping = looping;

	// Precisamos colocar alguma coisa no buffer, mesmo quando pausados
	bool wasPaused = paused;
	paused = false;

	AudioQueueBufferCallback( pSounds[ soundIndex ], pSounds[ soundIndex ]->pQueue, pSounds[ soundIndex ]->pBuffer );

	paused = wasPaused;

	// Determina o volume do som que será tocado
	setVolume( volume );

	// lets start playing now - stop is called in the AQTestBufferCallback when there's
	// no more to read from the file
	AudioQueueStart( pSounds[ soundIndex ]->pQueue, NULL );
	
	// Trata o caso de receber um playSound enquanto a aplicação está suspensa
	pSounds[ soundIndex ]->wasPlayingBeforeSuspend = true;

	if( paused )
		AudioQueuePause( pSounds[ soundIndex ]->pQueue );
}

/*==============================================================================================

MÉTODO stopSound
	Pára a execução de um som da aplicação.

==============================================================================================*/

// TODOO: Este método deve ser controlado com semáforos!!!!!!!!!! Afinal, ele pode ser chamado
// pela thread principal e pela thread que controla a reprodução de sons
void AppleAudioManager::stopSound( uint8 soundIndex )
{
	if( pSounds[ soundIndex ] != NULL )
	{
		OSStatus result = AudioQueueStop( pSounds[ soundIndex ]->pQueue, YES );
		if( result )
		{
			#if DEBUG
				LOG( @"AppleAudioManager::stopSound( uint8 ) stop failed. Sound: %d; Error: %d", soundIndex, ( int )result );
			#endif
		}

		pSounds[ soundIndex ]->running = false;
		pSounds[ soundIndex ]->currentPacket = 0;
		
		// Trata o caso de receber um stopSound enquanto a aplicação está suspensa
		pSounds[ soundIndex ]->wasPlayingBeforeSuspend = false;
	}
}

/*==============================================================================================

MÉTODO stopAllSounds
	Pára a execução de todos os sons da aplicação.

==============================================================================================*/

void AppleAudioManager::stopAllSounds( void )
{
	for( uint8 i = 0 ; i < nAppSounds ; ++i )
		stopSound( i );
}

/*==============================================================================================

MÉTODO pauseSound
	Pára / retoma a execução do som indicado.

==============================================================================================*/

void AppleAudioManager::pauseSound( uint8 soundIndex, bool paused )
{
	if( pSounds[ soundIndex ] && pSounds[ soundIndex ]->running )
	{
		if( paused )
			AudioQueuePause( pSounds[ soundIndex ]->pQueue );
		else
			AudioQueueStart( pSounds[ soundIndex ]->pQueue, NULL );
	}
}

/*==============================================================================================

MÉTODO pauseAllSounds
	Pára / retoma a execução de todos os sons da aplicação.

==============================================================================================*/

void AppleAudioManager::pauseAllSounds( bool paused )
{
	for( uint8 i = 0 ; i < nAppSounds ; ++i )
		pauseSound( i, paused );
}


#endif COMPONENTS_CONFIG_ENABLE_APPLE_SOUND_API
