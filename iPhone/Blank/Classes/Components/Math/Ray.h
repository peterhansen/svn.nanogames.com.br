/*
 *  Ray.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef RAY_H
#define RAY_H 1

#include <cstdio> // Definição de NULL

#include "Point3f.h"

class Ray
{
	public:
		// Origem do raio
		Point3f origin;

		// Direção do raio
		Point3f direction;
	
		// Construtores
		Ray( void );
		Ray( const Point3f* pOrigin, const Point3f* pDirection );
	
		// Inicializa o objeto
		void set( const Point3f* pOrigin = NULL, const Point3f* pDirection = NULL );
	
		// Operadores de comparação
		bool operator == ( const Ray& point ) const;
};

#endif
