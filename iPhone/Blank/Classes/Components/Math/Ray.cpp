#include "Ray.h"

Ray::Ray( void ) : origin(), direction()
{
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Ray::Ray( const Point3f* pOrigin, const Point3f* pDirection ) : origin( pOrigin ? *pOrigin : Point3f() ), direction( pDirection ? *pDirection : Point3f() )
{
	set( pOrigin, pDirection );
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Ray::set( const Point3f* pOrigin, const Point3f* pDirection )
{
	if( pOrigin )
		origin = *pOrigin;
	else
		origin.set();

	if( pDirection )
		direction = *pDirection;
	else
		direction.set();
}

/*==============================================================================================

 OPERADOR ==
 
==============================================================================================*/

bool Ray::operator == ( const Ray& ray ) const
{
	return ( origin == ray.origin ) && ( direction == ray.direction ); 
}
