#include "Bezier.h"

#include "MathFuncs.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

BezierCurve::BezierCurve( const Point3f* pOrigin, const Point3f* pControl1, const Point3f* pControl2, const Point3f* pDestination )
			: origin( pOrigin ? *pOrigin : Point3f() ),
			  control1( pControl1 ? *pControl1 : Point3f() ),
			  control2( pControl2 ? *pControl2 : Point3f() ),
			  destination( pDestination ? *pDestination : Point3f() )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void BezierCurve::set( const Point3f* pOrigin, const Point3f* pControl1, const Point3f* pControl2, const Point3f* pDestination )
{
	if( pOrigin )
		origin = *pOrigin;
	else
		origin.set();
	
	if( pControl1 )
		control1 = *pControl1;
	else
		control1.set();
	
	if( pControl2 )
		control2 = *pControl2;
	else
		control2.set();
	
	if( pDestination )
		destination = *pDestination;
	else
		destination.set();
}

/*==============================================================================================

MÉTODO getPointAt
	Retorna o ponto da curva referente a determinada porcentagem do movimento.

==============================================================================================*/

Point3f* BezierCurve::getPointAt( Point3f* pOut, float percent ) const
{
	percent = NanoMath::clamp( percent, 0.0f, 1.0f );
	pOut->set(	evaluate( origin.x, control1.x, control2.x, destination.x, percent ),
				evaluate( origin.y, control1.y, control2.y, destination.y, percent ),
				evaluate( origin.z, control1.z, control2.z, destination.z, percent ) );
	return pOut;
}

/*==============================================================================================

MÉTODO getXAt
	Retorna a coordenada x do ponto da curva referente a determinada porcentagem do movimento.

==============================================================================================*/

float BezierCurve::getXAt( float percent ) const
{
	return evaluate( origin.x, control1.x, control2.x, destination.x, NanoMath::clamp( percent, 0.0f, 1.0f ) );
}

/*==============================================================================================

MÉTODO getYAt
	Retorna a coordenada y do ponto da curva referente a determinada porcentagem do movimento.

==============================================================================================*/

float BezierCurve::getYAt( float percent ) const
{
	return evaluate( origin.y, control1.y, control2.y, destination.y, NanoMath::clamp( percent, 0.0f, 1.0f ) );
}

/*==============================================================================================

MÉTODO getZAt
	Retorna a coordenada z do ponto da curva referente a determinada porcentagem do movimento.

==============================================================================================*/

float BezierCurve::getZAt( float percent ) const
{
	return evaluate( origin.z, control1.z, control2.z, destination.z, NanoMath::clamp( percent, 0.0f, 1.0f ) );
}

/*==============================================================================================

MÉTODO evaluate
	Retorna o valor da componente em um dado momento da curva.

==============================================================================================*/

float BezierCurve::evaluate( float orig, float ctrl1, float ctrl2, float dest, float percent ) const
{
	float c = 3.0f * ( ctrl1 - orig );
	float b = 3.0f * ( ctrl2 - ctrl1 ) - c;
	float a = dest - orig - c - b;

	return ( a * ( percent * percent * percent ) ) + ( b * ( percent * percent ) ) + ( c * percent ) + orig;	
}
