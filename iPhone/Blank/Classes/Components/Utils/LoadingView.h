//
//  LoadingView.h
//  Components
//
//  Created by Daniel Lopes Alves on 8/4/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef LOADING_VIEW_H
#define LOADING_VIEW_H 1

#import <UIKit/UIKit.h>

#include "LoadingThread.h"
#include "NanoTypes.h"

@interface LoadingView : UIView
{
	@private
		// Ponteiro para a função de carregamento do usuário
		id loadingObj;
		SEL loadingFunc;
		SEL onLoadEndedFunc;
	
		// Indica se houve algum erro de processamento na thread
		int16 threadErrorCode;
	
		// Porcentagem de carregamento completa
		float loadingPercent;
	
		// Thread responsável por executar a rotina de carregamento
		LoadingThread* hThread;
	
		// Trata a animação do logo da Nano Games
		NSTimer *hLogoAnimController;
		IBOutlet UIImageView *hNanoGamesLogo;
	
		// Fornecem para o usuário um feedback do carregamento
		NSTimer *hInterfaceUpdater;
		IBOutlet UILabel *hProgressLabel;
		IBOutlet UIProgressView *hProgressBar;
	
		// Indica se já exibimos 100% na tela
		bool showed100Feedback;
}

// Determina um ponteiro para a função de loading
-( void )setLoadingTarget:( id )target AndSelector:( SEL )selector CallingAfterLoad:( SEL )onLoadEndedSelector;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

// Pára o loop de renderização
- ( void ) resume;

// Pára o loop de renderização
- ( void ) suspend;

// Determina a porcentagem atual da barra de progresso, atualizando sua imagem e o label de
// porcentagem do loading
- ( void ) setProgress:( float )percent;

// Modifica a porcentagem atual da barra de progresso em 'percent', atualizando sua imagem e
// o label de porcentagem do loading
- ( void ) changeProgress:( float )percent;

// Indica que houve algum erro de processamento na thread
-( void ) setError:( int16 )errorCode;

// Retorna se houve algum erro de processamento na thread
-( int16 ) getError;

@end

#endif
