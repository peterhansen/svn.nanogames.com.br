/*
 *  ResourceManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/26/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H 1

#include "Texture2D.h"

class ResourceManager
{
	public:
		// Cria uma textura 2D. Reaproveita o handler caso um resource com o mesmo
		// identificador já tenha sido alocado
		static Texture2DHandler GetTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc = NULL );
	
		// Cria uma textura 2D com compressão. Reaproveita o handler caso um resource com o mesmo identificador
		// já tenha sido alocado.
		static Texture2DHandler GetPVRCTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc = NULL );
	
		// Cria uma textura 2D de 16 bits sem compressão
		static Texture2DHandler Get16BitTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc = NULL );
};

#endif
