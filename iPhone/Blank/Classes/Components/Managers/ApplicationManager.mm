#include "ApplicationManager.h"

#include "LoadingView.h"
#include <AudioToolbox/AudioToolbox.h>

// TODOO : Não era para este arquivo incluir arquivos que não pertecem aos componentes !!!
#include "Macros.h"

// Implementação da classe
@implementation ApplicationManager

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hWindow, hViewManager;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/

- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	currViewIndex = -1;
	lastViewIndex = -1;
	vibrationOn = false;
	hasVibrationFunc = [[[UIDevice currentDevice] model] compare: @"iPod touch" options: NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch | NSWidthInsensitiveSearch ] != NSOrderedSame;

	appState = APPLICATION_STATE_RUNNING;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS : Descomentar se for necessário personalizar
//- ( void )dealloc
//{	
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM applicationWillResignActive
	Mensagem chamada quando a aplicação vai ser suspensa.

==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	appState = APPLICATION_STATE_SUSPENDED;
}

/*==============================================================================================

MENSAGEM applicationDidBecomeActive
	Mensagem chamada quando a aplicação é reiniciada.

==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	appState = APPLICATION_STATE_RUNNING;
}

/*==============================================================================================

MENSAGEM getApplicationState
	Retorna o estado da aplicação.

==============================================================================================*/

- ( ApplicationState )getApplicationState
{
	return appState;
}

/*==============================================================================================

MENSAGEM quit
	Termina a aplicação.

==============================================================================================*/

- ( void )quit:( uint32 )error
{
#if DEBUG
	LOG( @"ERROR: Quitting with error: %d\n", error );
#endif
	
	UIApplication *app = [UIApplication sharedApplication];
	SEL selector = @selector( terminate );

	if( [app respondsToSelector:selector] )
	{
		[app performSelector:selector];
	}
	else
	{
		[self applicationWillTerminate: app];
		exit( 0 );
	}
}

/*==============================================================================================

MENSAGEM startLoadingWithTarget: AndSelector:
	Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos
da aplicação.

==============================================================================================*/

-( void ) startLoadingWithTarget:( id )target AndSelector:( SEL )aSelector CallingAfterLoad:( SEL )onLoadEndedSelector
{
	// OLD
//	// Cria a tela de loading
//	UIActivityIndicatorView* hLoadingIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)]; 
//
//	if( [[ApplicationManager GetInstance] isOrientationLandscape] )
//		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH)];
//	else
//		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT)];
//	
//	[hLoadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge]; 
//	[hViewManager addSubview:hLoadingIndicator];
//	[hLoadingIndicator startAnimating];
//	[hLoadingIndicator release];
//
//	// Timer que irá carregar as views do jogo
//	[NSTimer scheduledTimerWithTimeInterval:0.001f target:self selector:aSelector userInfo:NULL repeats:NO];
	
	LoadingView* hLoadingView = ( LoadingView* )[[ApplicationManager GetInstance] loadViewFromXib: "LoadingView" ];
	if( !hLoadingView )
		[self quit: ERROR_CREATING_LOADING_VIEW];
	
	[hLoadingView setLoadingTarget: target AndSelector: aSelector CallingAfterLoad: onLoadEndedSelector];
	
	[hViewManager addSubview: hLoadingView];

	[hLoadingView onBecomeCurrentScreen];
	[hLoadingView release];
}

/*==============================================================================================

MENSAGEM getScreenWidth
	Retorna a largura da tela.

==============================================================================================*/

- ( float )getScreenWidth
{
	return [self isOrientationLandscape] ? [UIScreen mainScreen].bounds.size.height : [UIScreen mainScreen].bounds.size.width;
}

/*==============================================================================================

MENSAGEM getScreenHeight
	Retorna a altura da tela.

==============================================================================================*/

- ( float )getScreenHeight
{
	return [self isOrientationLandscape] ? [UIScreen mainScreen].bounds.size.width : [UIScreen mainScreen].bounds.size.height;
}

/*==============================================================================================

MENSAGEM getDefaultViewport
	Retorna o viewport padrão da aplicação.

==============================================================================================*/

- ( Viewport )getDefaultViewport
{
	Viewport v( 0, 0, static_cast<int32>( [self getScreenWidth] ), static_cast<int32>( [self getScreenHeight] ) );
	return v;
}

/*==============================================================================================

MENSAGEM getOrientation
	Retorna a orientação da aplicação.

==============================================================================================*/

- ( UIInterfaceOrientation )getOrientation
{
	return [[UIApplication sharedApplication]statusBarOrientation];
}

/*==============================================================================================

MENSAGEM isOrientationLandscape
	Indica se a orientação da aplicação pertence a família landscape.

==============================================================================================*/

- ( bool )isOrientationLandscape
{
	return UIInterfaceOrientationIsLandscape( [self getOrientation] );
}

/*==============================================================================================

MENSAGEM isOrientationPortrait
	Indica se a orientação da aplicação pertence a família portrait.

==============================================================================================*/

- ( bool )isOrientationPortrait
{
	return UIInterfaceOrientationIsPortrait( [self getOrientation] );
}

/*==============================================================================================

MENSAGEM GetInstance
	Retorna o controlador da aplicação.

==============================================================================================*/

+ ( ApplicationManager* ) GetInstance
{
	return ( ApplicationManager* )[[UIApplication sharedApplication] delegate];
}

/*==============================================================================================

MÉTODO loadViewFromXib
	Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente, é
necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController à view
propriamente dita.

==============================================================================================*/

- ( UIView* ) loadViewFromXib:( const char* )pViewName
{
	UIView *hView = NULL;
	UIViewController *hViewController = [[UIViewController alloc] initWithNibName: CHAR_ARRAY_TO_NSSTRING( pViewName ) bundle:NULL];
	
	if( hViewController )
	{
		// Carrega a view já na orientação correta
		if( [self isOrientationLandscape] )
		{
			CGAffineTransform transform = hViewController.view.transform;
			CGRect bounds = CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT );
			CGPoint center = CGPointMake( bounds.size.height * 0.5f, bounds.size.width * 0.5f );

			hViewController.view.center = center;
			
			if( [self getOrientation] == UIInterfaceOrientationLandscapeRight )
				transform = CGAffineTransformRotate( transform, static_cast< float >( M_PI_2 ) );
			else
				transform = CGAffineTransformRotate( transform, static_cast< float >( -M_PI_2 ) );
			
			hViewController.view.transform = transform;
		}

		hView = [hViewController view];
		[hView retain];
		[hViewController release];
	}

	return hView;
}

/*==============================================================================================

MÉTODO loadViewControllerFromXib
	Carrega um viewcontroller e sua view a partir de um arquivo .xib.

==============================================================================================*/

- ( UIViewController* ) loadViewControllerFromXib:( const char* )pViewControllerName
{
	UIViewController* hViewController = [[UIViewController alloc] initWithNibName: CHAR_ARRAY_TO_NSSTRING( pViewControllerName ) bundle:NULL];
	
	// Carrega a view já na orientação correta
	if( [self isOrientationLandscape] )
	{
		CGAffineTransform transform = hViewController.view.transform;
		CGRect bounds = CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT );
		CGPoint center = CGPointMake( bounds.size.height * 0.5f, bounds.size.width * 0.5f );

		hViewController.view.center = center;
		
		if( [self getOrientation] == UIInterfaceOrientationLandscapeRight )
			transform = CGAffineTransformRotate( transform, static_cast< float >( M_PI_2 ) );
		else
			transform = CGAffineTransformRotate( transform, static_cast< float >( -M_PI_2 ) );
		
		hViewController.view.transform = transform;
	}

	[hViewController retain];
	return hViewController;
}

/*==============================================================================================

MENSAGEM hasVibration:
	Indica se o device possui a funcionalidade de vibração.

==============================================================================================*/

- ( bool ) hasVibration
{
	return hasVibrationFunc;
}

/*==============================================================================================

MENSAGEM setVibrationStatus:
	Determina se a vibração está ligada ou desligada.

==============================================================================================*/
	
- ( void )setVibrationStatus:( bool ) on
{
	if( [self hasVibration] )
		vibrationOn = on;
}

/*==============================================================================================

MENSAGEM isVibrationOn
	Indica se a vibração está ligada / desligada.

==============================================================================================*/
	
- ( bool )isVibrationOn
{
	return [self hasVibration] ? vibrationOn : false;
}

/*==============================================================================================

MENSAGEM vibrate:
	Faz o device vibrar.

==============================================================================================*/

- ( void )vibrate:( float )time;
{
	if( [self hasVibration] && [self isVibrationOn] )
	{
		// TASK: Como fazer para determinar o tempo de vibração ?????
		AudioServicesPlaySystemSound( kSystemSoundID_Vibrate );

//		// TODOO Rodar em uma thread separada....
//		for( int32 i = 0 ; i < nTimes ; ++i )
//		{
//			AudioServicesPlaySystemSound( kSystemSoundID_Vibrate );
//			sleep (0.7f );
//		}
	}
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

- ( void )transitionDidStart:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

- ( void )performTransitionToView:( uint8 )newIndex
{
	[self performTransitionToView: newIndex WithLoadingView: NULL];
}

- ( void )performTransitionToView:( uint8 )newIndex WithLoadingView:( const LoadingView* )hLoadindView
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

// Fim da implementação da classe
@end
