#import <QuartzCore/QuartzCore.h>

#include "ViewManager.h"
#include "NanoTypes.h"

// Identificador da animação realizada pela transição de views
#define ANIMATION_KEY @"VMA"

// Implementação da classe
@implementation ViewManager

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize transitioning, delegate;

/*==============================================================================================

MENSAGEM transitionFromSubview
	Informa que ViewManager deve realizar uma nova transição de views.

==============================================================================================*/

- ( void )transitionFromSubview:( UIView* )oldView toSubview:( UIView* )newView keepOldView:( bool )retainOldView freeNewView:( bool )releaseNewView transition:( NSString* )transition direction:( NSString* )direction duration:( NSTimeInterval )duration
{	
	// Se já estamos realizando alguma transição, ignoramos esta chamada
	if( transitioning )
		return;
	
	// Obtém as subviews de ViewManager
	NSArray *subViews = [self subviews];
	NSUInteger index;
	
	if( [oldView superview] == self)
	{
		// Obtém o índice de oldView para que possamos inserir newView em seu lugar
		for( index = 0; [subViews objectAtIndex:index] != oldView ; ++index );

		if( retainOldView )
			[oldView retain];

		[oldView removeFromSuperview];
	}
	
	// Se realmente estamos indo para uma nova view e esta ainda não possui uma superview...
	if( newView && ( [newView superview] == NULL ) )
	{
		// Insere-a no antigo índice de oldView
		[self insertSubview:newView atIndex:index];
		
		if( releaseNewView )
			[newView release];
	}

	// Set up the animation
	CATransition *animation = [CATransition animation];
	[animation setDelegate:self];
	
	// Set the type and if appropriate direction of the transition
	[animation setType:transition];
	if( transition != kCATransitionFade )
		[animation setSubtype:direction];
	
	// Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
	[animation setDuration:duration];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[self layer] addAnimation:animation forKey: ANIMATION_KEY ];
}

/*==============================================================================================

MENSAGEM cancelTransition
	Cancela a transição corrente (caso haja).

==============================================================================================*/

- ( void )cancelTransition
{
	// Cancela a animação. O resto do tratamento será feito em animationDidStop:finished:
	if( transitioning )
		[[self layer] removeAnimationForKey: ANIMATION_KEY ];
}

/*==============================================================================================

MENSAGEM animationDidStart
	Método implementado para recebermos eventos de CAAnimation.

==============================================================================================*/

- ( void )animationDidStart:( CAAnimation* )animation
{	
	transitioning = YES;
    
	// Armazena o valor atual de userInteractionEnabled para que possamos restaurá-lo em animationDidStop:finished:
	interactionWasEnabled = self.userInteractionEnabled;
	
	// Se a interação do usuário já não está desabilitada, desabilita-a durante a transição
	if( interactionWasEnabled )
		self.userInteractionEnabled = NO;
    
	// Informamos ao listener que a transição foi iniciada (caso este implemente o método transitionDidStart)
	if( [delegate respondsToSelector:@selector( transitionDidStart: )] )
		[delegate transitionDidStart:self];
}

/*==============================================================================================

MENSAGEM animationDidStop
	Método implementado para recebermos eventos de CAAnimation.

==============================================================================================*/

- ( void )animationDidStop:( CAAnimation* )animation finished:( BOOL )finished
{	
	transitioning = NO;
	
	// Volta ao valor original de userInteractionEnabled
	if( interactionWasEnabled )
		self.userInteractionEnabled = YES;
    
	// Informamos ao listener que a transição terminou
	if( finished )
	{
		if( [ delegate respondsToSelector:@selector( transitionDidFinish: ) ] )
			[ delegate transitionDidFinish:self ];
	}
	else
	{
		if( [ delegate respondsToSelector:@selector( transitionDidCancel: ) ] )
			[ delegate transitionDidCancel:self ];
	}
}

// Fim da implementação da classe
@end
