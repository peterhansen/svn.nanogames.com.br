/*
 *  ApplicationManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef APPLICATION_MANAGER_H
#define APPLICATION_MANAGER_H 1

#import <UIKit/UIKit.h>

#include "NanoTypes.h"
#include "ViewManager.h"
#include "Viewport.h"

// Erros padrões
#define ERROR_NONE					0x0000
#define ERROR_MEMORY_WARNING		0x0001
#define ERROR_CREATING_LOADING_VIEW	0x0002
#define ERROR_USER					0x0100

enum ApplicationState
{
	APPLICATION_STATE_RUNNING =		0,
	APPLICATION_STATE_SUSPENDED =	1
};

@interface ApplicationManager : NSObject <UIApplicationDelegate, TransitionDelegate>
{
	@protected
		// Janela da aplicação
		IBOutlet UIWindow* hWindow;
		
		// Responsável por realiza a transição entre as views da aplicação
		IBOutlet ViewManager* hViewManager;
	
		// Índice da view atual
		int8 currViewIndex;
	
		// Índice da última view
		int8 lastViewIndex;
	
		// Indica se o device possui vibração
		bool hasVibrationFunc;
	
		// Indica se a vibração está ligada / desligada
		bool vibrationOn;
	
	@private
		// Estado atual da aplicação
		ApplicationState appState;
}

// Cria os setters e getters para estas propriedades
@property ( nonatomic, assign ) UIWindow* hWindow;
@property ( nonatomic, assign ) ViewManager* hViewManager;

// Retorna o estado da aplicação
- ( ApplicationState )getApplicationState;

// Termina a aplicação
- ( void )quit:( uint32 )error;

// Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos da aplicação
-( void ) startLoadingWithTarget:( id )target AndSelector:( SEL )aSelector CallingAfterLoad:( SEL )onLoadEndedSelector;

// Retorna a largura da tela
- ( float )getScreenWidth;

// Retorna a altura da tela
- ( float )getScreenHeight;

// Retorna o viewport padrão da aplicação
- ( Viewport )getDefaultViewport;

// Retorna a orientação da aplicação
- ( UIInterfaceOrientation )getOrientation;

// Indica se a orientação da aplicação pertence a família landscape
- ( bool )isOrientationLandscape;

// Indica se a orientação da aplicação pertence a família portrait
- ( bool )isOrientationPortrait;

// Retorna o controlador da aplicação
+ ( ApplicationManager* ) GetInstance;

// Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente,
// é necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController
// à view propriamente dita
- ( UIView* ) loadViewFromXib:( const char* )pViewName;

// Carrega um viewcontroller e sua view a partir de um arquivo .xib
- ( UIViewController* ) loadViewControllerFromXib:( const char* )pViewControllerName;

// Indica se o device possui a funcionalidade de vibração
- ( bool ) hasVibration;

// Determina se a vibração está ligada ou desligada
- ( void ) setVibrationStatus:( bool ) on;

// Indica se a vibração está ligada / desligada
- ( bool ) isVibrationOn;

// Faz o device vibrar
- ( void ) vibrate:( float )time;

@end

#endif
