#include "Texture.h"

/*==============================================================================================
 
CONSTRUTOR
 
==============================================================================================*/

Texture::Texture( LoadableListener *pListener, int32 loadableId ) : Loadable( pListener, loadableId ), texId( 0 )
{
	// Utiliza o OpenGL para gerar um identificador para a textura
	glGenTextures( 1, &texId );
	
	setListenerData( texId );
}

/*==============================================================================================
 
DESTRUTOR
 
==============================================================================================*/

Texture::~Texture( void )
{
	// Libera o identificador para posterior utilização pelo OpenGL
	if( texId != 0 )
	{
		glDeleteTextures( 1, &texId );
		texId = 0;
	}
}
