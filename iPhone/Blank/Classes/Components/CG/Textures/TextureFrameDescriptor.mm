#include "TextureFrameDescriptor.h"

#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

TextureFrameDescriptor::TextureFrameDescriptor( void ) : originalFrameWidth( 0 ), originalFrameHeight( 0 ), nFrames( 0 ), pFrames( NULL )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

TextureFrameDescriptor::~TextureFrameDescriptor( void )
{
	SAFE_DELETE_VEC( pFrames );
}

/*==============================================================================================

MÉTODO readFromFile
	Obtém as informações da textura através da leitura de um arquivo descritor.

==============================================================================================*/

bool TextureFrameDescriptor::readFromFile( const char* pFileName )
{
	// Obtém um interpretador para o arquivo descritor
	NSString* hSpriteDescriptorPath = Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( pFileName ), @"bin" );
	NSString* hFileContents = [NSString stringWithContentsOfFile: hSpriteDescriptorPath encoding:NSUTF8StringEncoding error: NULL];
	if( hFileContents == NULL )
		return false;

	return readFromScanner( [NSScanner scannerWithString: hFileContents] );
}

/*==============================================================================================

MÉTODO readFromScanner
	Obtém as informações da textura através do parser de arquivos.

==============================================================================================*/

bool TextureFrameDescriptor::readFromScanner( const NSScanner* hScanner )
{
	// Lê a largura original do frame
	[hScanner scanInt:static_cast< int32* >( &( originalFrameWidth ) )];

	// Lê a altura original do frame
	[hScanner scanInt:static_cast< int32* >( &( originalFrameHeight ) )];

	// Lê o número de frames do sprite
	int32 aux;
	[hScanner scanInt:&aux];
	nFrames = ( uint16 )aux;

	// Aloca o array de frames
	pFrames = new TextureFrame[ nFrames ];
	if( !pFrames )
		return false;

	// Lê o offset deste frame em relação ao frame original
	for( uint16 framesCounter = 0; framesCounter < nFrames ; ++framesCounter )
	{
		[hScanner scanInt:static_cast< int32* >( &( pFrames[ framesCounter ].x ))];
		[hScanner scanInt:static_cast< int32* >( &( pFrames[ framesCounter ].y ))];
		[hScanner scanInt:static_cast< int32* >( &( pFrames[ framesCounter ].width ))];
		[hScanner scanInt:static_cast< int32* >( &( pFrames[ framesCounter ].height ))];
		[hScanner scanInt:static_cast< int32* >( &( pFrames[ framesCounter ].offsetX ))];
		[hScanner scanInt:static_cast< int32* >( &( pFrames[ framesCounter ].offsetY ))];
		
		// TODOO : Ler se o frame possui ou não transparência
	}

	return true;
}

/*==============================================================================================

MÉTODO setOriginalFrameSize
	Determina.

==============================================================================================*/

void TextureFrameDescriptor::setOriginalFrameSize( uint32 width, uint32 height )
{
	originalFrameWidth = width;
	originalFrameHeight = height;
}

/*==============================================================================================

MÉTODO setFrames
	Obtém as informações da textura através do parser de arquivos.

==============================================================================================*/

bool TextureFrameDescriptor::setFrames( uint16 n, const TextureFrame* pTexFrames )
{
	// Deleta possíveis informações anteriores
	SAFE_DELETE_VEC( pFrames );
	
	// Copia as informações recebidas como parâmetro
	nFrames = n;
	
	if(( nFrames > 0 ) && ( pTexFrames != NULL ))
	{
		pFrames = new TextureFrame[ nFrames ];
		if( !pFrames )
			return false;
		
		for( uint16 i = 0 ; i < nFrames ; ++i )
			pFrames[i] = pTexFrames[i];
	}
	return true;
}

/*==============================================================================================

MÉTODO changeFrame
	Altera um frame da textura.

==============================================================================================*/

void TextureFrameDescriptor::changeFrame( uint16 frameIndex, const TextureFrame* pTexFrame )
{
	pFrames[ frameIndex ] = *pTexFrame;
}

/*==============================================================================================

MÉTODO copy
	Não utiliza operador de atribuição para não termos que disparar uma exceção no meio do
código.

==============================================================================================*/

bool TextureFrameDescriptor::copy( const TextureFrameDescriptor& desc )
{
	originalFrameWidth = desc.originalFrameWidth;
	originalFrameHeight = desc.originalFrameHeight;

	return setFrames( desc.nFrames, desc.pFrames );
}

