/*
 *  Scene.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/20/08.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SCENE_H
#define SCENE_H 1

#include "Camera.h"
#include "Color.h"
#include "ObjectGroup.h"
#include "Texture2D.h"
#include "VertexSet.h"

class Scene : public ObjectGroup
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException
		Scene( uint16 maxObjects );

		// Destrutor
		virtual ~Scene( void );
	
		// Renderiza o objeto
		virtual bool render( void );

		// Determina a câmera atual
		void setCurrentCamera( Camera* camera );
	
		// Obtém a câmera atual
		inline Camera* getCurrentCamera( void ) const { return pCurrCamera; };
	
		// Determina a cor utilizada para limpar o buffer de cor
		inline void setClearColor( const Color* pColor ){ clearColor = *pColor; };
	
		// Determina o valor utilizado para limpar o buffer de profundidade. O intervalo válido
		// é [0.0f, 1.0f]. Valores fora desse intervalo serão cropados
		inline void setClearDepth( float value ){ clearDepth = value; };
	
		// Determina o índice utilizado para limpar o stencil buffer. O intervalo aceito é de 2ˆm-1,
		// onde m é o número de bits do buffer
		inline void setClearStencil( int32 value ){ clearStencil = value; };
	
		// Determina quais buffers devem ser limpos antes da renderização. Valores válidos são:
		// - GL_COLOR_BUFFER_BIT
		// - GL_DEPTH_BUFFER_BIT
		// - GL_STENCIL_BUFFER_BIT
		inline void setClearBits( uint32 clearMask ){ clearBits = clearMask; };
	
	protected:
		// Clear bits
		uint32 clearBits;
	
		// Valor utilizado para limpar o buffer de profundidade
		float clearDepth;
	
		// Valor utilizado para limpar o stencil buffer
		int32 clearStencil;
	
		// Câmera que está sendo utilizada para renderizar a cena
		Camera* pCurrCamera;
	
		// Cor utilizada para limpar o buffer de cor
		Color clearColor;
};

#endif