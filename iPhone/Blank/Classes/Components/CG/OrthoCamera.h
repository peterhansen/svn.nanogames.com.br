/*
 *  OrthoCamera.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/26/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ORTHO_CAMERA_H
#define ORTHO_CAMERA_H 1

#include "Camera.h"

class OrthoCamera : public Camera
{
	public:
		// Construtores
		OrthoCamera( CameraType cameraType = CAMERA_TYPE_AIR, float zNear = -1.0f, float zFar = 1.0f );
	
		// Destrutor
		virtual ~OrthoCamera( void ){};

		// Configura os atributos da câmera na API gráfica
		virtual void place( void );
	
		// Determina se a câmera deve renderizar em modo landscape
		inline void setLandscapeMode( bool s ){ landscapeModeOn = s; };
	
		// Indica se a câmera está renderizando em modo landscape
		inline bool isLandscapeMode( void ) const { return landscapeModeOn; };
	
		// Retorna o volume de visualização da câmera
		virtual Box* getViewVolume( Box* pViewVolume );
	
		// Obtém a matriz de projeção da câmera
		virtual Matrix4x4* getProjectionMatrix( Matrix4x4* pOut ) const;
	
	protected:
		// Indica se a câmera está renderizando em modo landscape
		bool landscapeModeOn;
	
	private:
		// Atributos da projeção da câmera
		float zNear, zFar;
};

#endif
