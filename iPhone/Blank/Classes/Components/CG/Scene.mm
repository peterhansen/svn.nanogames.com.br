#include "Scene.h"
#include "Macros.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Scene::Scene( uint16 maxObjects ) : ObjectGroup( maxObjects ), clearBits( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ), clearDepth( 1.0f ), clearStencil( 0 ), pCurrCamera( NULL ), clearColor()
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Scene::~Scene( void )
{
	DESTROY( pCurrCamera );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Scene::render( void )
{
	glClearColor( clearColor.r, clearColor.g, clearColor.b, clearColor.a );
	glClearDepthf( clearDepth );
	// glClearStencil( clearStencil ); // TODOO : Stencil ainda não é suportado pelo iphone. Ver se esse trecho de código vai dar problema

	glClear( clearBits );
	
	pCurrCamera->place();
	
	return ObjectGroup::render();
}

/*==============================================================================================

MÉTODO setCurrentCamera

==============================================================================================*/

void Scene::setCurrentCamera( Camera* pCamera )
{
	// Deleta a câmera atual
	SAFE_DELETE( pCurrCamera );

	// Armazena a nova câmera
	pCurrCamera = pCamera;
}

