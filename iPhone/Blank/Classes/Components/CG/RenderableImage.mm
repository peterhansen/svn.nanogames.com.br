#include "RenderableImage.h"

#include "Exceptions.h"
#include "MathFuncs.h"
#include "Quad.h"
#include "ResourceManager.h"

// TODOO !!!
#warning RenderableImage TODOS:
#warning Utilizar os atributos setados por setPatternFactor() em render. Será alguma coisa do tipo glMatrixMode( GL_TEXTURE ); glScalef( patternFactor.x, patternFactor.y, patternFactor.z );
#warning Criar um método setOffset( float x, float y ). Para implementá-lo, basta modificarmos as coordenadas de textura utilizando GL_REPEAT

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

RenderableImage::RenderableImage( void )
				: Object(), mirrorOps( MIRROR_NONE ), texBlendSrcFactor( GL_ONE ), texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ), texEnvMode( GL_MODULATE ),
				  pVertexSet( NULL ), pImg( NULL ), patternFactor( 1.0f, 1.0f, 1.0f ), vertexSetColor( COLOR_WHITE )
{
	build( NULL, RenderableImage::CreateDefaultVertexSet() );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

//RenderableImage::RenderableImage( const RenderableImage& other )
//				: Object(), mirrorOps( MIRROR_NONE ),
//				  pVertexSet( NULL ), pImg( NULL ), patternFactor( other.patternFactor ), vertexSetColor( COLOR_WHITE )
//{
//	build( other.pImg, other.pVertexSet );
//
//	position = other.position;
//	speed = other.speed;
//	acceleration = other.acceleration;
//	size = other.size;
//	scale = other.scale;
//	rotation = other.rotation;
//}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

RenderableImage::RenderableImage( const RenderableImage* pOther )
				: Object(), mirrorOps( MIRROR_NONE ), texBlendSrcFactor( GL_ONE ), texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ), texEnvMode( GL_MODULATE ),
				  pVertexSet( NULL ), pImg( NULL ), patternFactor( pOther->patternFactor ), vertexSetColor( COLOR_WHITE )
{
	build( pOther->pImg, RenderableImage::CreateDefaultVertexSet() /* TODOO Criar este método obrigatório em VertexSet pOther->pVertexSet->getCopy() */ );

	position = pOther->position;
	speed = pOther->speed;
	acceleration = pOther->acceleration;
	size = pOther->size;
	scale = pOther->scale;
	rotation = pOther->rotation;
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

RenderableImage::RenderableImage( const char* pImagePath, CreateVertexSetFunction pVertexesCreator, LoadTextureFunction pLoadTextureFunction )
				: Object(),
				  mirrorOps( MIRROR_NONE ),
				  texBlendSrcFactor( GL_ONE ),
				  texBlendDstFactor( GL_ONE_MINUS_SRC_ALPHA ),
				  texEnvMode( GL_MODULATE ), 
				  pVertexSet( NULL ),
				  pImg( NULL ),
				  patternFactor( 1.0f, 1.0f, 1.0f ),
				  vertexSetColor( COLOR_WHITE )
{
	Texture2DHandler image = pLoadTextureFunction( pImagePath, NULL );
	if( !image )
#if DEBUG
		throw ConstructorException( "RenderableImage::RenderableImage( const char*, CreateVertexSetFunction ): Unable to load image" );
#else
		throw ConstructorException();
#endif
	
	build( image, pVertexesCreator() );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

RenderableImage::~RenderableImage( void )
{
	#if DEBUG
		LOG( @"Destruindo RenderableImage de %s", getName() );
	#endif

	//clean();
}

/*==============================================================================================

MÉTODO setPatternFactor
	Determina o fator de repetição da imagem.

==============================================================================================*/

void RenderableImage::setPatternFactor( float x, float y, float z )
{
	patternFactor.set( x, y, z );
}

/*==============================================================================================

MÉTODO mirror
	Determina as operações de espelhamento que devem ser aplicadas sobre o objeto.

==============================================================================================*/

void RenderableImage::mirror( MirrorOp ops )
{
	mirrorOps |= ops;
}

/*==============================================================================================

MÉTODO isMirrored
	Indica se uma determinada operação de espelhamento está aplicada sobre o objeto.

==============================================================================================*/

bool RenderableImage::isMirrored( MirrorOp op )
{
	return ( mirrorOps & op ) != 0;
}

/*==============================================================================================

MÉTODO unmirror
	Cancela operações de espelhamento aplicadas ao objeto.

==============================================================================================*/

void RenderableImage::unmirror( MirrorOp ops )
{
	mirrorOps &= ~ops;
}

/*==============================================================================================

MÉTODO setImageFrame
	Determina um frame da imagem para ser utilizado na texturização do objeto. Por default, esse
frame é a imagem inteira.

==============================================================================================*/

void RenderableImage::setImageFrame( const TextureFrame& frame )
{
	if( pImg )
	{
		pImg->changeFrame( 0, &frame );
		
		float texCoords[ QUAD_N_VERTEXES * 2 ];
		pVertexSet->mapTexCoords( pImg->getFrameTexCoords( 0, texCoords ));

		RenderableImage::setSize( frame.width, frame.height );
	}
}

/*==============================================================================================

MÉTODO setImage
	Determina a imagem a ser utilizada pelo objeto.

==============================================================================================*/

void RenderableImage::setImage( Texture2DHandler pImage )
{
	pImg = pImage;
	if( pImg )
	{
		float texCoords[ QUAD_N_VERTEXES * 2 ];
		pVertexSet->mapTexCoords( pImg->getFrameTexCoords( 0, texCoords ));

		const TextureFrame* pFrame = pImg->getFrame( 0 );
		RenderableImage::setSize( pFrame->width, pFrame->height );
	}
}

/*==============================================================================================

MÉTODO setVertexSetColor
	Determina a cor do conjunto de vértices a ser utilizada na renderização.

==============================================================================================*/

void RenderableImage::setVertexSetColor( const Color& color )
{
	vertexSetColor = color;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool RenderableImage::render( void )
{
	if( !Object::render() || NanoMath::feql( patternFactor.x, 0.0f ) || NanoMath::feql( patternFactor.y, 0.0f ) || NanoMath::feql( patternFactor.z, 0.0f ) || ( !pImg  ) )
		return false;
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	// OBS : Isso REALMENTE funciona...
	//	An optimum compromise that allows all primitives to be specified at integer
	//positions, while still ensuring predictable rasterization, is to translate x
	//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
	// away from the centers of pixels, while moving line vertices close enough to
	// the pixel centers
	
	// Posiciona o objeto
	float halfWidth = getWidth() * 0.5f;
	float halfHeight = getHeight() * 0.5f;
	glTranslatef( position.x + halfWidth + 0.375f, position.y + halfHeight + 0.375f, position.z );
	
	// TODOO : Provisório para o Blank!!!! Temos que deixar definir um centro de rotação!!!!!!!
	glTranslatef( -halfWidth, -halfHeight, position.z );
	
	Matrix4x4 aux;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &aux ) ) );
	
	glTranslatef( halfWidth, halfHeight, position.z );

	// Redimensiona o objeto
	glScalef( size.x * scale.x, size.y * scale.y, size.z * scale.z );
	
	// Espelha o objeto
	if( isMirrored( MIRROR_HOR ) )
		glRotatef( 180.0f, 0.0f, 1.0f, 0.0f );
	
	if( isMirrored( MIRROR_VER ) )
		glRotatef( 180.0f, 1.0f, 0.0f, 0.0f );

	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( texBlendSrcFactor, texBlendDstFactor );
	
	// Carrega a textura
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pImg->load();
	
	// Determina o modo de texturização
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, texEnvMode );
	
	if( NanoMath::fdif( scale.x, 1.0f ) || NanoMath::fdif( scale.y, 1.0f ) )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	}
	else
	{	
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	}

	// TODOO
//	glMatrixMode( GL_TEXTURE );
//	glScalef( patternFactor.x, patternFactor.y, patternFactor.z );
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );

	// Renderiza o objeto
	if( !( pVertexSet->getRenderStates() & RENDER_STATE_COLOR_ARRAY ) )
		glColor4ub( vertexSetColor.getByteR(), vertexSetColor.getByteG(), vertexSetColor.getByteB(), vertexSetColor.getByteA() );
	
	pVertexSet->render();
	
	if( !( pVertexSet->getRenderStates() & RENDER_STATE_COLOR_ARRAY ) )
		glColor4ub( 255, 255, 255, 255 );
	
	// Reseta os estados do OpenGL
	pImg->unload();

	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );
	
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

	if( customViewport )
		glDisable( GL_SCISSOR_TEST );

	return true;
}

/*==============================================================================================

MÉTODO CreateDefaultVertexSet
	Cria o conjunto de vértices que será utilizado para renderizar o objeto.

==============================================================================================*/

VertexSetHandler RenderableImage::CreateDefaultVertexSet( void )
{
	// TODOO : Está cagado (mas funcionando), pode vazar memória!!!!!!!
	return VertexSetHandler( new Quad() );
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

void RenderableImage::build( const Texture2DHandler& image, const VertexSetHandler& vertexes )
{
	if( /*!image ||*/ !vertexes )
#if DEBUG
		throw ConstructorException( "RenderableImage::build( const Texture2DHandler&, const VertexSetHandler& ): Unable to create object" );
#else
		throw ConstructorException();
#endif

	pVertexSet = vertexes;
	
	if( image )
		setImage( image );
}

/*==============================================================================================

MÉTODO clean
	Libera os recursos alocados pelo objeto.

==============================================================================================*/

//void RenderableImage::clean( void )
//{
//	// Não são ponteiros, logo não precisamos deletar
//	SAFE_DELETE( pVertexSet );
//	SAFE_DELETE( pImg );
//}

