/*
 *  Tests.h
 *  Blank
 *
 *  Created by Daniel Lopes Alves on 4/6/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEST_H
#define TEST_H 1

// Testes
#define TEST_NONE 0

#if DEBUG

#define CURR_TEST	TEST_NONE
#define IS_CURR_TEST( x ) ( CURR_TEST == ( x ) )

#else

#define IS_CURR_TEST( x ) 0

#endif

#endif
