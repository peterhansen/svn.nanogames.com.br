#include "GameScreen.h"

#include "AccelerometerManager.h"
#include "Exceptions.h"
#include "Macros.h"
#include "MathFuncs.h"
#include "OrthoCamera.h"

#include "BlankAppDelegate.h"
#include "Tests.h"

// TODOO: Determina o número de objetos na cena
#define SCENE_N_OBJECTS 1

// TODOO: Índices dos objetos contidos na cena
#define OBJ_INDEX		0

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

GameScreen::GameScreen( LoadingView* pLoadingView )
		  :	Scene( SCENE_N_OBJECTS ),
			EventListener( EVENT_TOUCH ),
			AccelerometerListener( ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC ),
			gameScreenState( GAME_SCREEN_STATE_UNDEFINED ),
			gameScreenLastState( GAME_SCREEN_STATE_UNDEFINED ),
			nActiveTouches( 0 ),
			initDistBetweenTouches( 0.0f ),
			stateTimeCounter( 0.0f ),
			stateDuration( 0.0f ),
			pInterfaceCamera( NULL )
{
	// Inicializa os vetores da classe
	#if DEBUG
		setName( "GameScreen" );
	#endif
	
	build( pLoadingView );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

GameScreen::~GameScreen( void )
{
	// TODOO : Descomentar se necessário
//	// Garante que iremos salvar a pontuação do jogo mesmo em eventos de quit e abort
//	if( ( gameScreenState != MATCH_STATE_UNDEFINED ) && ( gameInfo.points > 0 ) )
//	{
//		[(( BlankAppDelegate* )APP_DELEGATE ) saveScoreIfRecord: gameInfo.points];
//			
//		// Impede que por alguma razão salvemos a pontuação duas vezes
//		gameInfo.points = 0;
//	}
	
	clean();
}

/*==============================================================================================

MÉTODO clean
	Libera os recursos alocados pelo objeto.

==============================================================================================*/

void GameScreen::clean( void )
{
	DESTROY( pInterfaceCamera );

	// TODOO : Destrói os outros objetos que foram alocados no heap
	// mas que não foram incluídos no grupo da cena
	//...
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool GameScreen::build( LoadingView* hLoadindView )
{
	{ // Evita erros com os gotos

		// Cria os controles que devem ser renderizados sobre os elementos do jogo e não devem ser
		// transformados pela câmera
		if( !createInterface( hLoadindView ) )
			goto Error;
		
		// TODOO: Aloca os elementos do jogo
		//...
		

		// TODOO: Coloca a partida em seu estado inicial
		//...

		return true;
		
	} // Evita erros com os gotos
	
	Error:
		clean();
	
		#if DEBUG
			throw ConstructorException( "GameScreen::GameScreen: Unable to create object" );
		#else
			throw ConstructorException();
		#endif
	
		// Só para ser educado...
		return false;
}

/*===========================================================================================
 
MÉTODO createControls
	Cria os controles que devem ser renderizados sobre os elementos do jogo e não devem ser
transformados pelo zoom da câmera.

============================================================================================*/

bool GameScreen::createInterface( LoadingView* hLoadindView )
{
	// Câmera que irá renderizar os controles
	pInterfaceCamera = new OrthoCamera();
	if( !pInterfaceCamera )
		return false;

	// Indica o estado de visualização inicial do jogo
	pInterfaceCamera->setLandscapeMode( true );
	
	// TODOO : Indica o quanto já progredimos no carregamento inicial do jogo
//	[hLoadindView changeProgress: 0.5f];

	return true;
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/

bool GameScreen::render( void )
{
	if( Scene::render() )
	{
		return renderInterface();
	}
	return false;
}

/*===========================================================================================
 
MÉTODO hideInterface
	Esconde os controles da interface.

============================================================================================*/

void GameScreen::hideInterface( void )
{
}

/*===========================================================================================
 
MÉTODO renderInterface
	Renderiza os controles da interface.

============================================================================================*/

bool GameScreen::renderInterface( void )
{
	return false;
}

/*===========================================================================================
 
MÉTODO updateInterface
	Atualiza os controles da interface.

============================================================================================*/

bool GameScreen::updateInterface( float timeElapsed )
{
	return false;
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool GameScreen::update( float timeElapsed )
{
	if( !isActive() )
		return false;

	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
			break;

		case GAME_SCREEN_STATE_LOADING:
			break;

		case GAME_SCREEN_STATE_RUNNING:
			break;

		case GAME_SCREEN_STATE_PAUSED:
			break;

		case GAME_SCREEN_STATE_GAME_OVER:
			break;
	}
	
	// Atualiza os controles da interface
	updateInterface( timeElapsed );

	return true;
}

/*===========================================================================================
 
MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

============================================================================================*/

bool GameScreen::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	// Obtém a informação do toque
	const NSArray* hTouchesArray = static_cast< const NSArray* >( pParam );
	
	// OBS : Acho que isso nunca acontece, mas...
	uint8 nTouches = [hTouchesArray count];
	if( nTouches == 0 )
		return false;
		
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
			{
				int8 touchIndex = -1;
				for( uint8 i = 0 ; ( i < nTouches ) && ( nActiveTouches < MAX_TOUCHES ) ; ++i )	
				{
					if( ( touchIndex = startTrackingTouch( [hTouchesArray objectAtIndex:i] ) ) >= 0 )
						onNewTouch( touchIndex, &trackedTouches[i].startPos );
				}
			}
			return true;

		case EVENT_TOUCH_MOVED:
			// Se possuímos mais de um toque, estamos recebendo uma operação de zoom
			#if MAX_TOUCHES > 1
				if( nActiveTouches > 1 )
				{
					// Fazer zoom quando o usuário move apenas um dedo está causando bugs estranhos
					if( nTouches == 1 )
						return false;

					int8 touchIndex = -1;
					Point3f aux[ MAX_TOUCHES ] = { trackedTouches[0].startPos, trackedTouches[1].startPos };

					for( uint8 i = 0 ; i < nTouches ; ++i )
					{
						UITouch* hTouch = [hTouchesArray objectAtIndex:i];
						if( ( touchIndex = isTrackingTouch( hTouch ) ) >= 0 )
						{
							CGPoint currTouchPos = [hTouch locationInView: NULL];
							aux[ touchIndex ].set( currTouchPos.x, currTouchPos.y );
						}
					}

					// Se os toques estão se aproximando, é um zoom out
					float currZoom = pCurrCamera->getZoomFactor();
					float newDistBetweenTouches = ( aux[0] - aux[1] ).getModule();
					float movement = fabsf( newDistBetweenTouches - initDistBetweenTouches );
					if( movement < ZOOM_MIN_DIST_TO_CHANGE )
						return false;

					if( NanoMath::fltn( newDistBetweenTouches, initDistBetweenTouches ) )
					{
						float newZoom = currZoom * powf( ZOOM_OUT_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );

						if( newZoom < ZOOM_MIN )
							pCurrCamera->zoom( ZOOM_MIN );
						else
							pCurrCamera->zoom( newZoom );
					}
					// Se os toques estão se afastando, é um zoom in
					else if( NanoMath::fgtn( newDistBetweenTouches, initDistBetweenTouches ) )
					{
						float newZoom = currZoom * powf( ZOOM_IN_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );

						if( newZoom > ZOOM_MAX )
							pCurrCamera->zoom( ZOOM_MAX );
						else
							pCurrCamera->zoom( newZoom );
					}

					initDistBetweenTouches = newDistBetweenTouches;
				}
				// Movimentação de toque único
				else
			#endif
				{
					UITouch* hTouch = [hTouchesArray objectAtIndex:0];
					int8 touchIndex = isTrackingTouch( hTouch );
					if( touchIndex >= 0 )
					{
						CGPoint currPos = [hTouch locationInView:NULL];
						CGPoint lastPos = [hTouch previousLocationInView:NULL];
						
						Point3f touchCurrPos( currPos.x, currPos.y );
						Point3f touchLastPos( lastPos.x, lastPos.y );

						onSingleTouchMoved( touchIndex, &touchCurrPos, &touchLastPos );
					}
				}
			return true;
		
		case EVENT_TOUCH_CANCELED:
		case EVENT_TOUCH_ENDED:
			for( uint8 i = 0 ; i < nTouches ; ++i )
			{
				UITouch* hTouch = [hTouchesArray objectAtIndex:i];
				int8 touchIndex = isTrackingTouch( hTouch );

				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					currPos = Utils::MapPointByOrientation( &currPos );

					Point3f touchPos( currPos.x, currPos.y );

					// Verifica se houve um toque duplo
					if( [hTouch tapCount] == 2 )
						onDoubleClick( touchIndex );

					onTouchEnded( touchIndex, &touchPos );
					
					// Cancela o rastreamento do toque
					stopTrackingTouch( touchIndex );
				}
			}
			return true;
	}
	return false;
}

/*===========================================================================================
 
MÉTODO onNewTouch
	Trata eventos de início de toque.

============================================================================================*/

void GameScreen::onNewTouch( int8 touchIndex, const Point3f* pTouchPos )
{
	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
		case GAME_SCREEN_STATE_LOADING:
		case GAME_SCREEN_STATE_PAUSED:
		case GAME_SCREEN_STATE_GAME_OVER:
			return;

		case GAME_SCREEN_STATE_RUNNING:
			break;
	}
}

/*===========================================================================================
 
MÉTODO onTouchEnded
	Trata eventos de fim de toque.

============================================================================================*/

void GameScreen::onTouchEnded( int8 touchIndex, const Point3f* pTouchPos )
{	
	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
		case GAME_SCREEN_STATE_LOADING:
		case GAME_SCREEN_STATE_PAUSED:
		case GAME_SCREEN_STATE_GAME_OVER:
			return;

		case GAME_SCREEN_STATE_RUNNING:
			break;
	}
}

/*===========================================================================================
 
MÉTODO onDoubleClick
	Trata eventos de clique duplo.

============================================================================================*/

void GameScreen::onDoubleClick( int8 touchIndex )
{
	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
		case GAME_SCREEN_STATE_LOADING:
		case GAME_SCREEN_STATE_PAUSED:
		case GAME_SCREEN_STATE_GAME_OVER:
			return;

		case GAME_SCREEN_STATE_RUNNING:
			break;
	}
}

/*===========================================================================================
 
MÉTODO onSingleTouchMoved
	Trata eventos de movimentação de toques.

============================================================================================*/

void GameScreen::onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos )
{
	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
		case GAME_SCREEN_STATE_LOADING:
		case GAME_SCREEN_STATE_PAUSED:
		case GAME_SCREEN_STATE_GAME_OVER:
			return;

		case GAME_SCREEN_STATE_RUNNING:
			break;
	}
}

/*===========================================================================================
 
MÉTODO isTrackingTouch
	Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja sendo
acompanhado.

============================================================================================*/

int8 GameScreen::isTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		// Isto funciona porque o objeto UITouch é criado no início do toque e
		// deletado apenas no seu término. Logo, o seu endereço é o identificador
		// ideal
		if( trackedTouches[i].Id == reinterpret_cast< int32 >( hTouch ) )
			return i;
	}
	return -1;
}

/*===========================================================================================
 
MÉTODO startTrackingTouch
	Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
fazê-lo.

============================================================================================*/

int8 GameScreen::startTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == 0 )
		{
			++nActiveTouches;

			// Armazena o endereço do objeto. Isto funciona porque o objeto UITouch é criado
			// no início do toque e deletado apenas no seu término. Logo, o seu endereço é o
			// identificador ideal
			trackedTouches[i].Id = reinterpret_cast< int32 >( hTouch );
			
			CGPoint aux = [hTouch locationInView: NULL];
			trackedTouches[i].unmappedStartPos.set( aux.x, aux.y );

			aux = Utils::MapPointByOrientation( &aux );
			trackedTouches[i].startPos.set( aux.x, aux.y );
			
			if( nActiveTouches == 2 )
				initDistBetweenTouches = ( trackedTouches[0].startPos - trackedTouches[1].startPos ).getModule();

			return i;
		}
	}
	return -1;
}
	
/*===========================================================================================
 
MÉTODO stopTrackingTouch
	Cancela o rastreamento do toque recebido como parâmetro.

============================================================================================*/

void GameScreen::stopTrackingTouch( int8 touchIndex )
{
	if( touchIndex < 0 )
		return;

	if( trackedTouches[ touchIndex ].Id != 0 )
	{
		--nActiveTouches;
		trackedTouches[ touchIndex ].Id = 0;
		initDistBetweenTouches = 0.0f;
	}
}

/*===========================================================================================
 
MÉTODO onAccelerate
	Recebe os eventos do acelerômetro.

============================================================================================*/

void GameScreen::onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration )
{
	if( !isActive() )
		return;
	
	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
		case GAME_SCREEN_STATE_LOADING:
		case GAME_SCREEN_STATE_PAUSED:
		case GAME_SCREEN_STATE_GAME_OVER:
			return;

		case GAME_SCREEN_STATE_RUNNING:
			break;
	}
}

/*===============================================================================

MÉTODO setState
   Modifica o estado da partida.

================================================================================*/

void GameScreen::setState( GameScreenState newState )
{
	// Pára de receber os eventos dos acelerômetros. O estado que precisar pode ativá-los novamente
	AccelerometerManager::GetInstance()->stopAccelerometers();

	switch( gameScreenState )
	{
		case GAME_SCREEN_STATE_UNDEFINED:
		case GAME_SCREEN_STATE_LOADING:			
			return;

		case GAME_SCREEN_STATE_PAUSED:
			pause();
			break;

		case GAME_SCREEN_STATE_GAME_OVER:
			break;

		case GAME_SCREEN_STATE_RUNNING:
			break;
	}
	
	gameScreenLastState = gameScreenState;
	gameScreenState = newState;
}

/*===============================================================================

MÉTODO onGameOver
   Inicia a transição para a tela de jogo adequada.

================================================================================*/

void GameScreen::onGameOver( void )
{
	// TODOO : Submite a pontuação para o ranking
//	[(( BlankAppDelegate* )APP_DELEGATE ) onGameOver: gameInfo.points];
}

/*===============================================================================

MÉTODO pause
   Suspende o processamento dos elementos do jogo e exibe a tela de pause.

================================================================================*/

void GameScreen::pause( void )
{
	if( gameScreenState != GAME_SCREEN_STATE_PAUSED )
	{
		// Pára de receber os eventos do acelerômetro
		AccelerometerManager::GetInstance()->stopAccelerometers();
		
		// Pausa todos os sons atuais
		BlankAppDelegate* hApplication = ( BlankAppDelegate* ) APP_DELEGATE;
		[hApplication pauseAllSounds: true];
		
		// TODOO: Toca o som de pause
//		[hApplication playAudioNamed: SOUND_NAME_WHISTLE AtIndex: SOUND_INDEX_WHISTLE Looping: false];
		
		// TODOO: Exibe a tela de pause
//		[hApplication performTransitionToView: VIEW_INDEX_PAUSE_SCREEN];
	}
}

/*===============================================================================

MÉTODO unpause
   Retorna para o estado anterior ao estado MATCH_STATE_PAUSED.

================================================================================*/

void GameScreen::unpause( void )
{
	if( gameScreenState == GAME_SCREEN_STATE_PAUSED )
	{
		// OBS: Aqui não utiliza setState( matchLastState ) pois iríamos reiniciar o estado, e o que
		// queremos é voltar do exato momento quando o estado foi suspenso
		gameScreenState = gameScreenLastState;
		
		// Volta a executar todos os sons
		[(( BlankAppDelegate* ) APP_DELEGATE ) pauseAllSounds: false];

		// Volta a receber os eventos do acelerômetro
		AccelerometerManager::GetInstance()->startAccelerometers();
	}
}

/*===============================================================================

MÉTODO resetLevel
	Reseta a fase atual.

================================================================================*/

void GameScreen::resetLevel( void )
{
}

/*===============================================================================

MÉTODO onLoadNewLevel
	Carrega uma nova fase.

================================================================================*/

void GameScreen::loadLevel( int16 level )
{
}
