/*
 *  GameScreen.h
 *  Blank
 *
 *  Created by Daniel Lopes Alves on 2/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GAME_SCREEN_H
#define GAME_SCREEN_H 1

#include "AccelerometerListener.h"
#include "EventListener.h"
#include "Scene.h"
#include "Touch.h"

#include "Config.h"
#include "Tests.h"

// Declarações Adiadas
class OrthoCamera;
//...

class GameScreen :	public Scene, public EventListener, public AccelerometerListener
{
	public:
		// Construtor
		GameScreen( LoadingView* hLoadindView );
	
		// Destrutor
		virtual ~GameScreen( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
	
		// Recebe os eventos do acelerômetro
		virtual void onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration );
	
		// Indica se o jogo acabou
		inline bool isGameOver( void ) const { return gameScreenState == GAME_SCREEN_STATE_UNDEFINED; };
	
		// Inicia a transição para a tela de jogo adequada
		void onGameOver( void );
	
		// Retorna para o estado anterior ao estado GAME_SCREEN_STATE_PAUSED
		void unpause( void );

	private:
		// Estados da partida
		enum GameScreenState
		{
			GAME_SCREEN_STATE_UNDEFINED = -2,
			GAME_SCREEN_STATE_LOADING = -1,
			GAME_SCREEN_STATE_RUNNING = 0,
			GAME_SCREEN_STATE_PAUSED,
			GAME_SCREEN_STATE_GAME_OVER
		};
	
		// Libera os recursos alocados pelo objeto
		void clean( void );
	
		// Inicializa o objeto
		bool build( LoadingView* hLoadindView );
	
		// Esconde todos os controles
		void hideInterface( void );
	
		// Renderiza os controles da interface
		bool renderInterface( void );
	
		// Atualiza os controles da interface
		bool updateInterface( float timeElapsed );
	
		// Cria os controles que devem ser renderizados sobre os elementos do jogo e não devem ser
		// transformados pelo zoom da câmera
		bool createInterface( LoadingView* hLoadindView );
	
		// Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja
		// sendo acompanhado
		int8 isTrackingTouch( const UITouch* hTouch );
		
		// Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
		// fazê-lo
		int8 startTrackingTouch( const UITouch* hTouch );
	
		// Cancela o rastreamento do toque recebido como parâmetro
		void stopTrackingTouch( int8 touchIndex );

		// Trata eventos de início de toque
		void onNewTouch( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de fim de toque
		void onTouchEnded( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de clique duplo
		void onDoubleClick( int8 touchIndex );
	
		// Trata eventos de movimentação de toques
		void onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos );
	
		// Carrega uma nova fase
		void loadLevel( int16 level );
	
		// Reseta a fase atual
		void resetLevel( void );
	
		// Modifica o estado da partida
		void setState( GameScreenState newState );
	
		// Suspende o processamento dos elementos do jogo e exibe a tela de pause.
		void pause( void );

		// Estado da partida
		GameScreenState gameScreenState, gameScreenLastState;
	
		// Variáveis utilizadas para o controle dos gestos realizados no touchscreen
		uint8 nActiveTouches;
		float initDistBetweenTouches;
		Touch trackedTouches[ MAX_TOUCHES ];
	
		// Controlam a duração de estados de transição
		float stateTimeCounter, stateDuration;
	
		// Câmera utilizada para renderizar os controles da cena
		OrthoCamera* pInterfaceCamera;
};

#endif
