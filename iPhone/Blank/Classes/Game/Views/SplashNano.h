//
//  SplashNano.h
//  FeeKick
//
//  Created by Daniel Lopes Alves on 12/18/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef SPLASH_NANO_H
#define SPLASH_NANO_H 1

#import <UIKit/UIKit.h>

@interface SplashNano : UIView
{
}

// Chama a próxima tela do jogo
- ( void ) onEnd;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
