//
//  Event.mm
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 NanoStudio. All rights reserved.
//

#import "SBJson.h"

#import "Event.h"
#import "TicketCostumer.h"
#import "ServerProxy.h"


@implementation Event


- ( NSString *) getLocation {
    return location;
}

- ( NSString *) getTime {
    return time;
}

- ( NSString *) getDate {
    return date;
    
}

- ( NSString *) getName {
    return name;
}



- ( void ) setLocation: ( NSString *) str {
    location = str;
}

- ( void ) setTime: ( NSString * ) str {
    time = str;
}

- ( void ) setDate: ( NSString * ) str {
    date = str;
    
}

- ( void ) setName: ( NSString * ) str {
    name = str;
}

- ( BOOL ) isAlreadyIn: ( NSString * ) code {
    TicketCostumer *tc;
    
    for ( int c = 0; c < [ attendingCostumers count ]; ++c  ) {
        
        tc = [ attendingCostumers objectAtIndex: c ];
        
        if ( [ [ tc getQRCode ] compare: code ] == NSOrderedSame ) {            
            return YES;            
        }
    }
    return NO; 
}

- ( BOOL ) notAttending: ( NSString * ) code {
    
    TicketCostumer *tc;
    
    for ( int c = 0; c < [ attendingCostumers count ]; ++c  ) {
        
        tc = [ attendingCostumers objectAtIndex: c ];
        
        if ( [ [ tc getQRCode ] compare: code ] == NSOrderedSame ) {        
            
            if ( [ [ tc getStatus ] compare:@"false" ] == NSOrderedSame ) {
                NSLog(@"erro de consistência! ticket ja validado sendo novamente validado!");
            }
            
            [ tc setStatus: @"false" ];
            [ attendingCostumers removeObject: tc ];
            [ expectedCostumers addObject: tc ];
            return YES;
            
        }
    }
    return NO;
}


- ( BOOL ) attended: ( NSString * ) code {
    
    TicketCostumer *tc;
    
    for ( int c = 0; c < [ expectedCostumers count ]; ++c  ) {
        
        tc = [ expectedCostumers objectAtIndex: c ];
        
        if ( [ [ tc getQRCode ] compare: code ] == NSOrderedSame ) {        
            
            if ( [ [ tc getStatus ] compare:@"true" ] == NSOrderedSame ) {
                NSLog(@"erro de consistência! ticket ja validado sendo novamente validado!");
            }
            [ tc setStatus: @"true" ];
            [ expectedCostumers removeObject: tc ];
            [ attendingCostumers addObject: tc ];
            return YES;
            
        }
    }
    return NO;
}

- ( Event * ) initWithEventId: ( Event_Id ) event_Id {
    [ super init ];
    eventId = event_Id;
    expectedCostumers = [ [ NSMutableArray alloc ] init ];
    attendingCostumers = [ [ NSMutableArray alloc ] init ];    
    filters = [ [ NSMutableArray alloc ] init ];
    
    [ self getServerData ];
    return self;
}


- ( int ) getAttendingCostumersCount {
    return [ attendingCostumers count ];
}

- ( int ) getExpectedCostumersCount {
    return [ expectedCostumers count ];    
}


- ( NSMutableArray * ) getAllTicketsList {
    
    int total;
    NSMutableArray *tmp = [ [ NSMutableArray alloc ] init ];
    TicketCostumer *tc;
    
    total = [ self getAttendingCostumersCount ];
    for ( int c = 0; c < total; ++c ) {
        tc = [ attendingCostumers objectAtIndex: c ];
        [ tmp addObject: tc ];
    }
    
    total = [ self getExpectedCostumersCount ];
    for ( int c = 0; c < total; ++c ) {
        tc = [ expectedCostumers objectAtIndex: c ];       
        [ tmp addObject: tc ]; 
    }
    
    return tmp;    
}

- ( NSArray * ) getAllTicketsListCopy {
    int total;
    NSMutableArray *tmp = [ [ NSMutableArray alloc ] init ];
    TicketCostumer *tc;
    

    
    total = [ self getAttendingCostumersCount ];
    for ( int c = 0; c < total; ++c ) {
        tc = [ attendingCostumers objectAtIndex: c ];
        [ tmp addObject: [ tc makeCopy ] ];
    }
    
    total = [ self getExpectedCostumersCount ];
    for ( int c = 0; c < total; ++c ) {
        tc = [ expectedCostumers objectAtIndex: c ];       
        [ tmp addObject: [ tc makeCopy ] ]; 
    }
    
    return tmp;
}

+ ( NSString * ) TAG_EVENTID {
    return @"eventId";
}

+ ( NSString * ) TAG_FILTERS {
    return @"filters";
}

/**
 
 **/
- ( void ) getServerData {
    
    if ( [ self loadFromCache ] )
        return;
    
    NSString *JSONContent = nil;
    NSMutableString *url = nil;
    NSArray *buyers = nil;
    NSDictionary *buyerData = nil;
    TicketCostumer *tc = nil;

    url = [ [ NSMutableString alloc ] initWithString: @"" ];
    [ url appendString: [ ServerProxy BASE_URL ]  ];
    [ url appendString: [ ServerProxy EVENT_DATA_REQUEST ]  ];
    [ url appendString: [ ServerProxy PARAMS_START_MARKER ]  ];
    [ url appendString: [ Event TAG_EVENTID ]  ];
    [ url appendString: [ ServerProxy VALUE_START ]  ];
    [ url appendString: [ NSString stringWithFormat: @"%d", eventId ]  ];    
    [ url appendString: [ ServerProxy PARAM_SEPARATOR ]  ];    
    [ url appendString: [ Event TAG_FILTERS ]  ];
    [ url appendString: [ ServerProxy VALUE_START ]  ];

    
    NSMutableString *filtersStr = [ [ NSMutableString alloc ] init ];
    
    for ( int c = 0; c < [ filters count ]; ++c ) {

        if ( c != 0 )
            [ filtersStr appendString: @"," ];
        
        [ filtersStr appendString: [ filters objectAtIndex: c ] ];
    }
    
    [ url appendString: filtersStr ];    
    
    [ url appendString: [ ServerProxy PARAMS_END ]  ];
    
    JSONContent = [ ServerProxy JSONFromUrl: url ];
    
    buyers = [ JSONContent JSONValue ];
    
    for ( int c = 0; c < [ buyers count ]; ++c ) {
        
        buyerData =  ( ( NSDictionary * )[ buyers objectAtIndex: c ] );         
        
        tc = [ self ticketWithCode: [ buyerData valueForKey: [ TicketCostumer TAG_QRCODE ] ] ];
        
        if ( tc == nil ) {
            tc = [ [ TicketCostumer alloc ] initWithDictionary: buyerData ];            
        } else {
            if ( [ self isAlreadyIn: [ tc getQRCode ] ] == YES ) {
                [ attendingCostumers removeObject: tc ];
            } else {
                [ expectedCostumers removeObject: tc ];                
            }
        }
        
        [ tc updateWithDictionary: buyerData ];
        
        if ( [ [ tc getStatus ] compare:@"true" ] == NSOrderedSame )
            [ attendingCostumers addObject: tc ];
        else
            [ expectedCostumers addObject: tc ];
    }
}


- ( void ) dataFromSummary: ( NSDictionary * ) dic {    
      
     /*
     carrega:
     NSString *location;
     NSString *time;
     NSString *date;
     NSString *name;
     NSMutableArray *filters;
     */
    NSString *tmp;
    
    tmp = [ dic valueForKey: @"event-place" ];    
    location = [ [ NSString alloc ] initWithString: tmp ];

    tmp = [ dic valueForKey: @"event-time" ];
    time = [ [ NSString alloc ] initWithString: tmp ]; 
    
    tmp = [ dic valueForKey: @"event-date" ];
    date = [ [ NSString alloc ] initWithString: tmp ]; 
    
    tmp = [ dic valueForKey: @"event-name" ];
    name = [ [ NSString alloc ] initWithString: tmp ]; 
    
    filters = [ [ NSArray alloc ] initWithArray: [ dic valueForKey: @"categories" ] ];
}

- ( Event * ) initWithEventId: ( Event_Id ) event_Id andDictionaryData: ( NSDictionary * ) dic {
    
    [ super init ];
    eventId = event_Id;
    expectedCostumers = [ [ NSMutableArray alloc ] init ];
    attendingCostumers = [ [ NSMutableArray alloc ] init ];    
    filters = [ [ NSMutableArray alloc ] init ];
    
    [ self dataFromSummary: dic ];    
    
    [ self getServerData ];
    
    return self;
}

- ( NSArray * )getFilters {
    return filters;
}

- ( TicketCostumer * ) ticketWithCode: ( NSString * ) code {

    TicketCostumer *tc;
    
    for ( int c = 0; c < [ expectedCostumers count ]; ++c  ) {
        
        tc = [ expectedCostumers objectAtIndex: c ];
        
        if ( [ [ tc getQRCode ] compare: code ] == NSOrderedSame ) {
            
            return tc;
            
        }
    }    
    
    for ( int c = 0; c < [ attendingCostumers count ]; ++c  ) {
        
        tc = [ attendingCostumers objectAtIndex: c ];
        
        if ( [ [ tc getQRCode ] compare: code ] == NSOrderedSame ) {
            
            return tc;
            
        }
    }    
    
    
    return nil;
}

/**
 
 
 **/

- ( BOOL ) loadFromCache {
    return NO;
}
@end
