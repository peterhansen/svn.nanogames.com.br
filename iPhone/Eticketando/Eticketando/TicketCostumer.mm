//
//  TicketCostumer.mm
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import "TicketCostumer.h"


@implementation TicketCostumer

- ( void ) updateWithDictionary: ( NSDictionary * ) dictionary {
    name = nil;
    [ self setName: [ dictionary objectForKey:@"buyer-name" ] ];
    
    QRcode = nil;
    [ self setQRCode: [ dictionary objectForKey:@"QRcode" ] ];
    
    category = nil;
    [ self setCategory: [ dictionary objectForKey:@"buyer-category" ] ];
    
    userId = nil;
    [ self setId: [ dictionary objectForKey:@"id" ] ];    
    
    status = nil;
    [ self setStatus: [ dictionary objectForKey:@"status" ] ];    
    
    email = nil;
    [ self setStatus: [ dictionary objectForKey:@"email" ] ];    

    picture = nil;
    [ self setStatus: [ dictionary objectForKey:@"picture" ] ];    
    
    
}
    

- ( TicketCostumer * ) initWithDictionary: ( NSDictionary * ) dictionary {
    [ super init ];
    
    [ self updateWithDictionary: dictionary ];
    
    return self;
}

- ( TicketCostumer * ) makeCopy {
    
    TicketCostumer * myCopy = [ [ TicketCostumer alloc ]  init ];
    
    [ myCopy setName: [ self getName ] ];
    [ myCopy setCategory: [ self getCategory ] ];
    [ myCopy setId: [ self getId ] ];
    [ myCopy setStatus: [ self getStatus ] ];
    [ myCopy setQRCode: [ self getQRCode ] ];    
    [ myCopy setPicture: [ self getPicture ] ];
    [ myCopy setEmail: [ self getEmail ] ];
    
    return myCopy;
}

- ( void ) setEmail: ( NSString * ) str {
    email = str;
}

- ( void ) setName: ( NSString * ) str {
    [ name release ];
    name = [ [ NSString alloc ] initWithString: str ];   
}

- ( void ) setQRCode: ( NSString * ) str {
    [ QRcode release ];
    QRcode = [ [ NSString alloc ] initWithString: str ];   
}

- ( void ) setCategory: ( NSString * ) str {
    [ category release ];
    category = [ [ NSString alloc ] initWithString: str ];   
}

- ( void ) setStatus: ( NSString * ) str {
    [ status release ];
    status = [ [ NSString alloc ] initWithString: str ];   
    [ self setUpdatedOnThisPush: NO ];                
}

- ( void ) setId: ( NSString * ) str {
    [ userId release ];
    userId = [ [ NSString alloc ] initWithString: str ];   
}

- ( void ) setUpdatedOnThisPush: ( BOOL ) state {
    changedSinceLastPush = state;   
}

- ( BOOL ) isUpdatedSinceLastPush {
    return changedSinceLastPush;
}

+ ( NSString * ) TAG_QRCODE {
    return @"QRcode";
}

- ( NSMutableDictionary * ) dataDictionary {
    
    NSMutableDictionary *dict = [ [ NSMutableDictionary alloc ] init ];
    
    [ dict setValue: [ TicketCostumer TAG_QRCODE ] forKey: QRcode ];    
    [ dict setValue: @"buyer-name" forKey: name ];
    [ dict setValue: @"id" forKey: userId ];
    [ dict setValue: @"status" forKey: status ];    
    [ dict setValue: @"buyer-category" forKey: category ];    
    [ dict setValue: @"email" forKey: email ];
    [ dict setValue: @"picture" forKey: picture ];
    return dict;
}

- ( NSComparisonResult ) compare: ( TicketCostumer *) other {
    return [ name caseInsensitiveCompare: [ other getName ] ];
}

- ( NSString * ) getEmail {
    return email;
}

- ( NSString * ) getId {
    return userId;
}

- ( NSString * ) getName {
    return name;
}

- ( NSString * ) getQRCode {
    return QRcode;
}

- ( NSString * ) getCategory {
    return category;
}

- ( NSString * ) getStatus {
    return status;
}

- ( NSString * ) toString {
    NSMutableString *tmp = [ [ NSMutableString alloc ] initWithString: @"" ];
    NSString *toReturn;
    
    [ tmp appendString: name ];
    [ tmp appendString: @"\n" ];    
    [ tmp appendString: category ];
    
    toReturn = [ NSString stringWithString: tmp ];
    return toReturn;
} 

@end
