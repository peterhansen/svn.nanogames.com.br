//
//  ServerProxy.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ServerProxy : NSObject {
    
}
+ ( NSString *) BASE_URL;
+ ( NSString *) EVENT_DATA_REQUEST;
+ ( NSString *) PARAMS_END;
+ ( NSString *) PARAMS_START_MARKER;
+ ( NSString *) VALUE_START;
+ ( NSString *) PARAM_SEPARATOR;
+ ( NSString *) NEW_LINE;
+ ( NSString *) LOGIN;
+ ( NSString * ) EVENT_LIST_DATA_REQUEST;
+ ( NSString * ) JSONFromUrl: ( NSString * ) url;
+ ( NSString * ) TAG_PASSWDHASH;
+ ( NSString * ) TAG_USERID;

@end
