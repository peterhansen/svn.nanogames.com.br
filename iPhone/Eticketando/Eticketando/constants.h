//
//  constants.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//
#ifndef CONSTANTS_H
#define CONSTANTS_H

#define VIEWID_LOGIN 0
#define VIEWID_CHECKIN 1
#define VIEWID_MANUALVALIDATION 2
#define VIEWID_TICKETSLIST 3
#define CELL_HEIGHT_NORMAL 60

#define CONFIGKEY_FLASH @"use_flash"

#endif