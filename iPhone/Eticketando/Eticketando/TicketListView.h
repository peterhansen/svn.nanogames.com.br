//
//  TicketListView.h
//  Eticketando
//
//  Created by Daniel Monteiro on 9/1/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "ClickCheckinDelegate.h"

@interface TicketListView : UIView< UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, ClickCheckinDelegate > {
    IBOutlet UITableView *ticketList;    
    IBOutlet UISearchBar *searchBar;
    BOOL searchIsOn;
    Event *currentEvent;
    NSString *currentFilter;
//    NSMutableArray *filteredSet;
    NSMutableArray *sections;
    NSMutableArray *sectionTitles;
}

//conjunto básico de operações...bom candidato a classe básica
- ( void ) initData;
- ( void ) updateUIMainThread;
- ( void ) updateUI;
- ( IBAction ) logout;
- ( IBAction ) back;
- ( UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
- ( void ) buildFilteredSet;
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView;
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString * )title atIndex:(NSInteger)index;
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
@property (nonatomic, retain) UISearchBar *searchBar; 
@property (nonatomic, assign) BOOL searchIsOn;
@end
