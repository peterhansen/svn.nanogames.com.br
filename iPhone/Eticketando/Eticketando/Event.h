//
//  Event.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TicketCostumer.h"
typedef int Event_Id;

@interface Event : NSObject {
    NSMutableArray *attendingCostumers;
    NSMutableArray *expectedCostumers;
    Event_Id eventId;
    NSString *location;
    NSString *time;
    NSString *date;
    NSString *name;
    NSArray *filters;
}

- ( BOOL ) isAlreadyIn: ( NSString * ) code;
- ( BOOL ) attended: ( NSString * ) code;
- ( Event * ) initWithEventId: ( Event_Id ) event_Id;
- ( Event * ) initWithEventId: ( Event_Id ) event_Id andDictionaryData: ( NSDictionary * ) dic;
- ( void ) dataFromSummary: ( NSDictionary *) dic;
- ( int ) getAttendingCostumersCount;
- ( int ) getExpectedCostumersCount;
- ( NSMutableArray * ) getAllTicketsList;
- ( NSMutableArray * ) getAllTicketsListCopy;
- ( void ) getServerData;
- ( BOOL ) loadFromCache;
+ ( NSString * ) TAG_EVENTID;
- ( NSArray * )getFilters;
- ( NSString *) getLocation;
- ( NSString *) getTime;
- ( NSString *) getDate;
- ( NSString *) getName;
- ( void ) setLocation: ( NSString *) str ;
- ( void ) setTime: ( NSString * ) str;
- ( void ) setDate: ( NSString * ) str;
- ( void ) setName: ( NSString * ) str;
+ ( NSString * ) TAG_FILTERS;
- ( TicketCostumer * ) ticketWithCode: ( NSString * ) code;
- ( BOOL ) notAttending: ( NSString * ) code;

@end
