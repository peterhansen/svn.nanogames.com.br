//
//  TicketCellView.h
//  Eticketando
//
//  Created by Daniel Monteiro on 9/1/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TicketCostumer.h"
#import "ClickCheckinDelegate.h"

@interface TicketCellView : UITableViewCell {
    TicketCostumer *costumerObject;
    IBOutlet UILabel *buyerName;
    IBOutlet UILabel *buyerCategory;
    IBOutlet UILabel *buyerEmail;    
    IBOutlet UIImageView *profileImageView;
    IBOutlet UIButton *checkinButton;
    id < ClickCheckinDelegate > delegate;
}

- ( void ) initData;
- ( void ) updateUIMainThread;
- ( void ) updateUI;
//- ( void ) addIndexTitle;
- ( void ) setCostumer: ( TicketCostumer * ) costumer;
- ( TicketCostumer * ) getCostumer;
- ( IBAction ) triggerCheckin;

@property ( nonatomic, retain ) id < ClickCheckinDelegate > delegate;
@property ( nonatomic, retain ) IBOutlet UIButton *checkinButton;
@end
