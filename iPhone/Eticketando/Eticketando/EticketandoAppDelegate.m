//
//  EticketandoAppDelegate.m
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#include "constants.h"
#import "EticketandoAppDelegate.h"
#import "Event.h"
#import "ServerProxy.h"
#import "CheckinView.h"
#import "SBJson.h"
#import "SBJsonParser.h"

@implementation EticketandoAppDelegate


@synthesize window=_window;


- ( void )setCurrentCode: ( NSString * ) code {
    currentCode = [ [ NSString alloc ] initWithString: code ];
}

- ( NSString * ) getCurrentCode {
    return currentCode;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{


}

- (void)applicationDidBecomeActive:(UIApplication *)application {    
    [ self initData ];
}

- ( void ) initData {
    
    NSArray *viewsFromXib; 

    //tmp
    userId = @"0";
    
    //inicializa dados gerais
    instance = self;
    views = [ [ NSMutableArray alloc ] initWithCapacity: 9 ];
    configs = [ [ NSMutableDictionary alloc ] init ];
    
    [ self loadEventsData ];    

    ///inicializa dados de configuração
    [ self setConfigFor: CONFIGKEY_FLASH with: @"true" ];
    

    

    ///carrega as views
    viewsFromXib = [ [ NSBundle mainBundle ] loadNibNamed:@"LoginView" owner: self options: nil ];
    [ views addObject: [ viewsFromXib objectAtIndex: 0 ] ];    
    
    viewsFromXib = [ [ NSBundle mainBundle ] loadNibNamed:@"CheckinView" owner: self options: nil ];
    [ views addObject: [ viewsFromXib objectAtIndex: 0 ] ];
    
    viewsFromXib = [ [ NSBundle mainBundle ] loadNibNamed:@"ManualValidationView" owner: self options: nil ];
    [ views addObject: [ viewsFromXib objectAtIndex: 0 ] ];
    
    viewsFromXib = [ [ NSBundle mainBundle ] loadNibNamed:@"TicketListView" owner: self options: nil ];
    [ views addObject: [ viewsFromXib objectAtIndex: 0 ] ];
    
    
    //tmp
    [ EticketandoAppDelegate setViewFromId: VIEWID_LOGIN ];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
    [_window release];
    [super dealloc];
}


+ ( void ) setView:(UIView *)view {
    instance.window.rootViewController.view = view;
    
    if ( [ view respondsToSelector:@selector( updateUI ) ] ) 
        [ view performSelector: @selector( updateUI ) ];
    
    [ instance.window  setNeedsLayout ];
    [ instance.window  setNeedsDisplay ];    
}

+ ( void ) setViewFromId:(int)view {
    [ EticketandoAppDelegate setView: [ instance viewWithId: view ] ];
}

- ( UIView *) viewWithId: ( int ) viewId {
    return [ views objectAtIndex: viewId ];
}

+ ( EticketandoAppDelegate * ) instance {
    return instance;
}

- ( void ) restoreViewController {
    self.window.rootViewController = baseViewController;
}


- ( void ) setConfigFor: ( NSString * ) key with: ( NSString *) value {
    [ configs removeObjectForKey: key ];
    [ configs setValue: value forKey: key ];
}

- ( NSString * ) getConfigFor: ( NSString * ) key {
    return [ configs valueForKey: key ];
}

- ( void ) loadEventsData {
    NSMutableString *url;
    NSString *JSONContent;
    NSDictionary *eventJSON;
    NSArray *items;
    currentEventId = @"0";
    url = [ [ NSMutableString alloc ] initWithString: @"" ];
    
    [ url appendString: [ ServerProxy BASE_URL ]  ];
    [ url appendString: [ ServerProxy EVENT_LIST_DATA_REQUEST ]  ];
    [ url appendString: [ ServerProxy PARAMS_START_MARKER ]  ];
    [ url appendString: [ EticketandoAppDelegate TAG_USERID ]  ];
    [ url appendString: [ ServerProxy VALUE_START ]  ];
    [ url appendString: userId ];    
    [ url appendString: [ ServerProxy PARAMS_END ]  ];
    
    JSONContent = [ ServerProxy JSONFromUrl: url ];
    items = [ JSONContent JSONValue ];
    
    eventJSON = [ items objectAtIndex: 0 ];    
    Event *event = [ [ Event alloc ] initWithEventId: 0 andDictionaryData: eventJSON ];
    
    eventSummary = [ [ NSMutableArray alloc ] initWithCapacity: 1 ];
    [ eventSummary addObject: event ];

}

- ( void ) pokeServer {
    NSMutableArray *toSend = [ [ NSMutableArray alloc ] init ];
    Event *event = [ self eventWithId: currentEventId ];
    NSArray *ticketList = [ event getAllTicketsList ];
    TicketCostumer *tc;
    
    for ( int c = 0; c < [ ticketList count ] ; ++c ) {
        tc = [ ticketList objectAtIndex: c ];
        
        if ( [ tc isUpdatedSinceLastPush ] == NO )
            [ toSend addObject: [ tc dataDictionary ] ];
        
    }
     
    SBJsonWriter *jparser = [ [ SBJsonWriter new ] init ];
    NSString *ArrayjsonItems = [ jparser stringWithObject: toSend ];
    
    NSLog( @"JSON: %@" , ArrayjsonItems );
    
    ///TODO - lista de parametros para postar
    
    NSString *post = @"";
    NSData *postData = [ post dataUsingEncoding: NSASCIIStringEncoding allowLossyConversion: YES ];
    
    NSString *postLength = [ NSString stringWithFormat:@"%d", [ ArrayjsonItems length ] ];
    
    NSMutableURLRequest *request = [ [ [ NSMutableURLRequest alloc ] init ] autorelease ];
    
    //TODO - preparar a URL
    [ request setURL: [ NSURL URLWithString: @"" ] ];
    [ request setHTTPMethod: @"POST" ];
    [ request setValue: postLength forHTTPHeaderField: @"Content-Length" ];
    [ request setValue: @"application/x-www-form-urlencoded" forHTTPHeaderField: @"Content-Type" ];
    [ request setHTTPBody: postData ]; 
    
    //verifica o caso de erros...cuidado com loops infinitos!
    [ self updateFromServer ];
    
    if ( NO == YES ) {
        for ( int c = 0; c < [ ticketList count ] ; ++c ) {

            tc = [ ticketList objectAtIndex: c ];
            
            [ tc setUpdatedOnThisPush: YES ];
            
        }        
    } else {
        UIAlertView* alertView = [ [ UIAlertView alloc ] initWithTitle: @"Falha" message:@"Conexão com servidor falhou." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles: nil ];
        [alertView show];
        [alertView release];
    }

}

- ( void ) updateFromServer {
    lastUpdate = [ [ NSDate alloc ] init ];
    Event *event = [ self eventWithId: currentEventId ];
    [ event getServerData ];
    
    if ( [ self.window.rootViewController.view respondsToSelector:@selector( initData ) ] )
        [ self.window.rootViewController.view initData ];    
}


+ ( NSString * ) TAG_USERID {
    return @"userId";
}

- ( Event * ) eventWithId: ( NSString * ) eventId {
    int intEventId = [ eventId intValue ];
    return [ eventSummary objectAtIndex: intEventId ];    
}


- ( Event * ) eventWithIdInt: ( int ) eventId {
    return [ eventSummary objectAtIndex: eventId ];    
}

@end
