//
//  ManualValidationView.m
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import "constants.h"
#import "ManualValidationView.h"
#import "EticketandoAppDelegate.h"

@implementation ManualValidationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [ self initData ];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [ self initData ];
    }
    return self;
}

- ( void ) touchesBegan: ( NSSet * ) touches withEvent: ( UIEvent * ) event {
    [ codeLabel resignFirstResponder ];
}

- ( void ) initData {
    codeLabel.delegate = self;
    
    currentEvent = [ [ EticketandoAppDelegate instance ] eventWithId: 0 ];
    [ self updateUI ];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

- ( IBAction ) back {
    [ EticketandoAppDelegate setViewFromId: VIEWID_CHECKIN ];       
}

- ( IBAction ) logout {
    [ EticketandoAppDelegate setViewFromId: VIEWID_LOGIN ];   
}

- ( IBAction ) validate {    
    [ ticketdataLabel setText: @"Validando..." ];
    [ codeLabel resignFirstResponder ];
    if ( [ currentEvent attended: [ codeLabel text ] ] == YES ) {
        [ ticketdataLabel setText: [ [ currentEvent ticketWithCode: [ codeLabel text ] ] toString ] ];
    } else {
        if ( [ currentEvent isAlreadyIn: [ codeLabel text ] ] == YES ) {
            
            [ ticketdataLabel setText: [ NSString stringWithFormat: @"%@\nTicket já validado!", [ [ currentEvent ticketWithCode: [ codeLabel text ] ] toString ] ] ];
        } else {
            [ ticketdataLabel setText: @"Ticket não encontrado" ];
        }
    }
    [ [ EticketandoAppDelegate instance ] pokeServer ]; 
    [ codeLabel.rightView setNeedsDisplay ];
}

- ( void ) updateUI {
    [ self performSelectorOnMainThread: @selector( updateUIMainThread ) withObject: self waitUntilDone: YES ];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
	[textField resignFirstResponder];
    
	return YES;
    
}

- ( void ) updateUIMainThread {
    NSString *tmp;
    
    tmp = [ currentEvent getDate ];
    [ timeLabel setText: tmp ];
    
    tmp = [ currentEvent getDate ];
    [ dateLabel setText: tmp ];
    
    tmp = [ currentEvent getName ];    
    [ nameLabel setText: tmp ];
    
    tmp = [ currentEvent getLocation ];
    [ locationLabel setText: tmp ];

    NSString *code = [ [ EticketandoAppDelegate instance ] getCurrentCode ];
    if ( [ code compare: @"-" ] == NSOrderedSame ) {
        [ [ EticketandoAppDelegate instance ] setCurrentCode: @"" ];
        [ ticketdataLabel setText: @"Não foi possível ler o QRCode." ];
    } else {
        [ codeLabel setText: code ];        
    }

    if ( [ code compare: @"" ] == NSOrderedSame )
        [ ticketdataLabel setText: @"" ];
    
    [ self setNeedsDisplay ];
}
@end
