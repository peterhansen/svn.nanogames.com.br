//
//  EticketandoAppDelegate.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface EticketandoAppDelegate : NSObject <UIApplicationDelegate> {
    NSMutableArray *views;
    IBOutlet UIViewController *baseViewController;
    NSMutableDictionary *configs;
    NSMutableArray *events;
    NSString *userId;
    NSMutableArray *eventSummary;
    NSString *currentCode;
    NSString *currentEventId;
    NSDate *lastUpdate;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
    - ( NSString * ) getConfigFor: ( NSString * ) key;
    - ( void ) setConfigFor: ( NSString * ) key with: ( NSString *) value;
    - ( UIView *) viewWithId: ( int ) viewId;
    + ( void ) setView: ( UIView * ) view;
    + ( void ) setViewFromId: ( int ) view;
    + ( EticketandoAppDelegate * ) instance;
    - ( void ) restoreViewController;
    - ( void ) loadEventsData;
    - ( void ) initData;
    + ( NSString * ) TAG_USERID;
    - ( void )setCurrentCode: ( NSString * ) code;
    - ( NSString * ) getCurrentCode;
    - ( void ) pokeServer;
    - ( void ) updateFromServer;
    - ( Event * ) eventWithId: ( NSString * ) eventId;
    - ( Event * ) eventWithIdInt: ( int ) eventId;
@end

static EticketandoAppDelegate *instance;