//
//  ManualValidationView.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface ManualValidationView : UIView< UITextFieldDelegate > {
    IBOutlet UITextField *codeLabel;
    IBOutlet UILabel *dateLabel;
    IBOutlet UILabel *timeLabel;    
    IBOutlet UILabel *locationLabel;
    IBOutlet UILabel *nameLabel;    
    IBOutlet UILabel *ticketdataLabel;    
    Event *currentEvent;
}
- ( void ) initData;
- ( IBAction ) validate;
- ( IBAction ) back;
- ( IBAction ) logout;
- ( void ) updateUI;
- ( void ) updateUIMainThread;
@end
