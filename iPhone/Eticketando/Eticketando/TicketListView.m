//
//  TicketListView.m
//  Eticketando
//
//  Created by Daniel Monteiro on 9/1/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import "TicketListView.h"
#import "TicketCellView.h"
#import "EticketandoAppDelegate.h"
#import "constants.h"

@implementation TicketListView

@synthesize searchBar;
@synthesize searchIsOn;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [ self initData ];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [ self initData ];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- ( void ) updateUI {
    [ self performSelectorOnMainThread: @selector( updateUIMainThread ) withObject: self waitUntilDone: YES ];
}

- ( void ) updateUIMainThread {
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [ tableView reloadData ];
    return indexPath;       
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ( ( TicketCellView *) [ tableView cellForRowAtIndexPath: indexPath ] ).checkinButton.hidden = NO;
    [ ( ( TicketCellView *) [ tableView cellForRowAtIndexPath: indexPath ] ) updateUI ];
}



- ( void ) peformCheckinFor: ( TicketCostumer * ) buyer onView: ( UIView * ) view {
    
    TicketCellView * cell = ( TicketCellView * ) view;
    TicketCostumer * tc = buyer;
    NSString *status = [ tc getStatus ];
    
    if ( [ status compare:@"false" ] == NSOrderedSame ) {    
        if ( [ currentEvent attended: [ tc getQRCode ] ] == YES ) {
//            [ [ EticketandoAppDelegate instance ] pokeServer ]; 
        } else {
            
        }
        
    } else {
        if ( [ currentEvent notAttending: [ tc getQRCode ] ] == YES ) {
//            [ [ EticketandoAppDelegate instance ] pokeServer ];             
        } else {
            
        }
    }

    [ cell updateUI ];    
}


//- (void)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    ( ( TicketCellView *) [ tableView cellForRowAtIndexPath: indexPath ] ).checkinButton.hidden = YES;
//}


- ( void ) buildFilteredSet {
    NSMutableArray *filteredSet;
    filteredSet = [ currentEvent getAllTicketsList ];
    [ filteredSet sortUsingSelector: @selector( compare: ) ];
    [ ticketList reloadData ];    

    
    sections = [ [ NSMutableArray alloc ] init ];
    sectionTitles = [ [ NSMutableArray alloc ] init ];
    NSMutableArray *section;
    NSString *sectionTitle;
    TicketCostumer *tc;
    int sectionIndex;
    
    for ( int c = 0; c < [ filteredSet count ]; ++c ) {
        tc = [ filteredSet objectAtIndex: c ];
        
        sectionTitle = [ [ tc getName ] substringToIndex: 1 ];
        //busca a section do ticket:
        sectionIndex = [ sectionTitles indexOfObject: sectionTitle ];
        
        if ( sectionIndex == NSNotFound ) {
            section = [ [ NSMutableArray alloc ] init ];            
            [ sections addObject: section ];
            [ sectionTitles addObject: sectionTitle ];
        } else {
            section = [ sections objectAtIndex: sectionIndex ];
        }
        

        [ section addObject: tc ];        
    }
    [ filteredSet release ];
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [ ( ( TicketCellView * ) cell ) updateUI ];
}

- ( UITableViewCell * )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        NSString *MyIdentifier = @"TicketCellView";
        
        TicketCellView *cell = (TicketCellView *)[ tableView dequeueReusableCellWithIdentifier: MyIdentifier ];
        
        if (cell == nil) { 
            
            NSArray * topLevelObjects = [ [ NSBundle mainBundle ] loadNibNamed:@"TicketCellView" owner: nil options: nil ];
            
            for ( id currentObject in topLevelObjects ) {
                if ( [ currentObject isKindOfClass: [ TicketCellView class ] ] ) {
                    cell = ( TicketCellView *) currentObject;
                    break;
                }
            }
        }    
    
    NSString *this;
    NSString *prev;
    
//    if ( indexPath.row != 0 ) {
//        this = [ [ filteredSet objectAtIndex: ( indexPath.row - 1 ) ] getName ];
//        prev = [ [ filteredSet objectAtIndex: ( indexPath.row ) ] getName ];        
//    }
    NSMutableArray *section = [ sections objectAtIndex: indexPath.section ];
    
    [ cell setCostumer: ( TicketCostumer * )[ section objectAtIndex: indexPath.row ] ];
    cell.checkinButton.hidden = YES;
    cell.delegate = self;
    
    if ( indexPath.row != 0 ) {
        if ( ( prev != nil && this != nil ) )  {

//            NSLog(@"------------------");
//            NSLog(@"this: %@ ", this );
//            NSLog(@"prev: %@ ", prev );
//            NSLog(@"this[0]: %@ ", [ this substringToIndex: 1 ] );
//            NSLog(@"prev[0]: %@ ", [ prev substringToIndex: 1 ] );    
            
            
//            if ( [[ this substringToIndex: 1 ] caseInsensitiveCompare: [ prev substringToIndex: 1 ] ] != NSOrderedSame ) {
//                [ cell addIndexTitle ];
//            }
        }        
    } else {
//        [ cell addIndexTitle ];        
    }
    
    [ cell updateUI ];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if ( indexPath.row > 0 ) {
//        NSString *this = [ [ filteredSet objectAtIndex: ( indexPath.row - 1 ) ] getName ];
//        NSString *prev = [ [ filteredSet objectAtIndex: ( indexPath.row ) ] getName ];
//
//        if ( prev != nil && this != nil )  {
//            if ( [[ this substringToIndex: 1 ] caseInsensitiveCompare: [ prev substringToIndex: 1 ] ] == NSOrderedSame ) {
//                return CELL_HEIGHT_NORMAL;
//            } else {
//                return CELL_HEIGHT_INDEX;            
//            }            
//        } else {
//            return CELL_HEIGHT_NORMAL;
//        }
//    } else {
//        return CELL_HEIGHT_NORMAL;
//    }    
    return CELL_HEIGHT_NORMAL;
}


//personaliza o título da section
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
//    if (sectionTitle == nil) {
//        return nil;
//    }
//    
//    // Create label with section title
//    UILabel *label = [[[UILabel alloc] init] autorelease];
//    label.frame = CGRectMake(20, 6, 300, 30);
//    label.backgroundColor = [UIColor clearColor];
//    label.textColor = [UIColor colorWithHue:(136.0/360.0)  // Slightly bluish green
//                                 saturation:1.0
//                                 brightness:0.60
//                                      alpha:1.0];
//    label.shadowColor = [UIColor whiteColor];
//    label.shadowOffset = CGSizeMake(0.0, 1.0);
//    label.font = [UIFont boldSystemFontOfSize:16];
//    label.text = sectionTitle;
//    
//    // Create header view and add label as a subview
//    UIView *view = [ [ UIView alloc ] initWithFrame: CGRectMake(0, 0, 320, 50 ) ];
//    [view autorelease];
//    [view addSubview:label];
//    
//    return view;
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ( sections != nil )
        return [ sections count ];
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( sections != nil )
        return [ [ sections objectAtIndex: section ] count ];
    else
        return 0;
}

- ( void ) initData {
    currentFilter = @"";
    sections = nil;
    sectionTitles = nil;
    currentEvent = [ [ EticketandoAppDelegate instance ] eventWithId: 0 ];
    ticketList.separatorStyle = UITableViewCellSelectionStyleGray;
    ticketList.separatorColor = [ UIColor redColor];
    
    [ self buildFilteredSet ];    
    [ ticketList reloadData ];
    [ self updateUI ];
}

- (void)dealloc
{
    [super dealloc];
}

- ( IBAction ) back {
//    [ [ EticketandoAppDelegate instance ] pokeServer ];    
    [ [ EticketandoAppDelegate instance ] updateFromServer ];    
//    [ EticketandoAppDelegate setViewFromId: VIEWID_CHECKIN ];       
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [ sectionTitles objectAtIndex: section ];
}

- ( IBAction ) logout {
    [ EticketandoAppDelegate setViewFromId: VIEWID_LOGIN ];   
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    if ( sectionTitles != nil )
        return sectionTitles;
    else
        return [ NSArray arrayWithObjects: nil ];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString * )title atIndex:(NSInteger)index {
//    NSIndexPath *indPath = [ NSIndexPath indexPathForRow: index inSection: 0 ];
//    [ tableView scrollToRowAtIndexPath: indPath atScrollPosition: UITableViewScrollPositionMiddle animated:YES ];

    return [ sectionTitles indexOfObject: [ title substringToIndex: 1 ] ];
}


- ( IBAction ) textFieldDoneEditing: ( id ) sender {
    [sender resignFirstResponder];
}

- ( void ) searchBarSearchButtonClicked: ( UISearchBar * ) theSearchBar {
    
    int len = [ [ searchBar.text stringByTrimmingCharactersInSet: [ NSCharacterSet whitespaceAndNewlineCharacterSet ] ] length ];
    
    if (len > 2) { 
        [ self searchBar: searchBar textDidChange: searchBar.text ];
    }
    
    [ searchBar resignFirstResponder ];    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
//    TicketCostumer *tc;
//    
//    if ( [ currentFilter length ] > [ searchBar.text length ] ) 
//        [ self buildFilteredSet ];
//    
//    currentFilter = searchBar.text;    
//        for ( int c = 0; c < [ filteredSet count ]; ++c ) {
//            
//            tc = [ filteredSet objectAtIndex: c ];
//            
//            if ( [ [ [ tc getName ] substringToIndex: [ currentFilter length ] ] caseInsensitiveCompare: currentFilter ] != NSOrderedSame ) {
//                [ filteredSet removeObject: tc ];
//            }
//        }

    
    [ ticketList reloadData ];
}
@end
