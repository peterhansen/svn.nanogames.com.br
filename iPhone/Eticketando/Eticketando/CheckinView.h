//
//  Checkin.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@interface CheckinView : UIView< ZBarReaderDelegate > {
    Event* currentEvent;
    IBOutlet UILabel *filtersLabel;
    IBOutlet UILabel *dateLabel;
    IBOutlet UILabel *timeLabel;    
    IBOutlet UILabel *locationLabel;
    IBOutlet UILabel *nameLabel;    
    IBOutlet UIButton *lampButton;
}

- ( void ) initData;
- ( void ) updateUIMainThread;
- ( void ) updateUI;

- ( IBAction ) scan;
- ( IBAction ) validateManually;
- ( IBAction ) info;
- ( IBAction ) toggleLamp;
- ( IBAction ) back;
- ( IBAction ) logout;
- ( IBAction ) gotoTickets;

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info;
@end
