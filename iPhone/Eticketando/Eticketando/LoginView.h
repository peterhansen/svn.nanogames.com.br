//
//  LoginView.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LoginView : UIView<UITextFieldDelegate> {
    IBOutlet UITextField *usernameTF;
    IBOutlet UITextField *passwordTF;
}
- ( IBAction ) attemptLogin;
- ( IBAction ) textFieldDoneEditing: ( id ) sender;
@end
