//
//  TicketCostumer.h
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TicketCostumer : NSObject {
    NSString *QRcode;
    NSString *name;
    NSString *category;
    NSString *status;
    NSString *userId;
    NSString *email;
    NSString *picture;    
    BOOL changedSinceLastPush;
}
- ( void ) updateWithDictionary: ( NSDictionary * ) dictionary;
- ( void ) setPicture: ( NSString * ) str;
- ( void ) setName: ( NSString * ) str;
- ( void ) setQRCode: ( NSString * ) str;
- ( void ) setCategory: ( NSString * ) str;
- ( void ) setStatus: ( NSString * ) str;
- ( void ) setId: ( NSString * ) str;
- ( void ) setEmail: ( NSString * ) str;
- ( void ) setUpdatedOnThisPush: ( BOOL ) state;
- ( BOOL ) isUpdatedSinceLastPush;
+ ( NSString * ) TAG_QRCODE;
- ( NSString * ) toString;
- ( NSString * ) getPicture;
- ( NSString * ) getId;
- ( NSString * ) getName;
- ( NSString * ) getQRCode;
- ( NSString * ) getCategory;
- ( NSString * ) getStatus;
- ( NSString * ) getEmail;
- ( TicketCostumer * ) makeCopy;
- ( NSMutableDictionary * ) dataDictionary;
- ( NSComparisonResult ) compare: ( TicketCostumer *) other;
- ( TicketCostumer * ) initWithDictionary: ( NSDictionary * ) dictionary;
@end
