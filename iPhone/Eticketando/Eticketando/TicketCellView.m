//
//  TicketCellView.m
//  Eticketando
//
//  Created by Daniel Monteiro on 9/1/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import "TicketCellView.h"
#import "constants.h"
#import <QuartzCore/QuartzCore.h>

@implementation TicketCellView

@synthesize delegate;
@synthesize checkinButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

- ( void ) initData {
    delegate = nil;
    costumerObject = nil;
}

- ( IBAction ) triggerCheckin {
    if ( delegate != nil && costumerObject != nil ) {
        [ delegate peformCheckinFor: costumerObject onView: self ];             
    }
}

//- ( void ) addIndexTitle {
//    UIImageView * rect = [ [ UIImageView alloc ] initWithFrame: CGRectMake( 0 , 0, [ self bounds ].size.width, ( CELL_HEIGHT_INDEX - CELL_HEIGHT_NORMAL ) ) ];
//    
//    UILabel *lbl = [ [ UILabel alloc ] initWithFrame: CGRectMake( 0 , 0, [ self bounds ].size.width, ( CELL_HEIGHT_INDEX - CELL_HEIGHT_NORMAL ) ) ];
//    
//    [ lbl setText: [ [ costumerObject getName ] substringToIndex: 1 ] ];
//    [ lbl setTextColor: [ UIColor whiteColor ] ];
//    
//    [ rect setImage: nil ];
//    [ rect setBackgroundColor: [ UIColor orangeColor ] ];
//    [ self addSubview: rect ];    
////    [ self addSubview: lbl ];
//
//    CAGradientLayer *theGradient = [ CAGradientLayer layer ];
//    theGradient.frame = rect.bounds;
//    theGradient.colors = [NSArray arrayWithObjects:
//                          (id) [ [ UIColor colorWithRed:1.0f green:1.0f blue:1.0f
//                                               alpha:0.2f ] CGColor ],
//                          (id) [ [ UIColor colorWithRed:1.0f green:1.0f blue:1.0f
//                                                  alpha:0.6f ] CGColor ], nil ];
//    
//    // add gradient to reflection image
//    [ rect.layer insertSublayer:theGradient atIndex:0 ];    
//    
//    CGPoint center = [ buyerName center ];
//    center.y += ( CELL_HEIGHT_INDEX - CELL_HEIGHT_NORMAL ) /*- ( [ buyerName bounds].size.height  / 4 )*/;
//    [ buyerName setCenter: center ];
//    
//    center = [ presense center ];
//    center.y += ( CELL_HEIGHT_INDEX - CELL_HEIGHT_NORMAL ) /*- ( [ buyerName bounds].size.height / 4 )*/;
//    [ presense setCenter: center ];
//    
//    center = [ checkinButton center ];
//    center.y += ( CELL_HEIGHT_INDEX - CELL_HEIGHT_NORMAL ) /*- ( [ buyerName bounds].size.height / 4 )*/;
//    [ checkinButton setCenter: center ];
//    
//    
//    [ self updateUI ];
//    
//}


- ( void ) setCostumer: ( TicketCostumer * ) costumer {
    costumerObject = costumer;
    [ buyerName setText: [ costumerObject getName ] ];
    [ buyerCategory setText: [ costumerObject getCategory ] ];
    [ buyerEmail setText: [ costumerObject getEmail ] ];    
}

- ( TicketCostumer * ) getCostumer {
    return costumerObject;
}

- ( void ) updateUI {
    [ self performSelectorOnMainThread: @selector( updateUIMainThread ) withObject: self waitUntilDone: YES ];
}

- ( void ) updateUIMainThread {
    NSString *status = [ costumerObject getStatus ];
    
    if ( [ status compare:@"true" ] == NSOrderedSame ) {
//        [ presense setImage: [ UIImage imageNamed: @"check" ] ];
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
//        [ presense setImage: nil ];
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    
    [ self setNeedsDisplay ];
}


@end
