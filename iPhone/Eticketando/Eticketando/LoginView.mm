//
//  LoginView.mm
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import "LoginView.h"
#import "constants.h"
#import "EticketandoAppDelegate.h"
#import "ServerProxy.h"
#import "SBJson.h"

@implementation LoginView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
	[ usernameTF resignFirstResponder];
	[ passwordTF resignFirstResponder];    
    
	return YES;
    
}

- ( IBAction ) textFieldDoneEditing: ( id ) sender {
    [sender resignFirstResponder];
}

- ( IBAction ) attemptLogin {
    NSMutableString *url = [ [ NSMutableString alloc ] initWithString: @"" ]; 
    NSString *JSONContent;
    
    url = [ [ NSMutableString alloc ] initWithString: @"" ];
    
    [ url appendString: [ ServerProxy BASE_URL ]  ];
    [ url appendString: [ ServerProxy LOGIN ]  ];
    [ url appendString: [ ServerProxy PARAMS_START_MARKER ]  ];
    [ url appendString: [ ServerProxy TAG_USERID ]  ];
    [ url appendString: [ ServerProxy VALUE_START ]  ];
    [ url appendString: [ usernameTF text ] ];    
    [ url appendString: [ ServerProxy PARAM_SEPARATOR ]  ];    
    [ url appendString: [ ServerProxy TAG_PASSWDHASH ]  ];
    [ url appendString: [ ServerProxy VALUE_START ]  ];
    [ url appendString: [ passwordTF text ] ];    
    [ url appendString: [ ServerProxy PARAMS_END ]  ];
    
    JSONContent = [ ServerProxy JSONFromUrl: url ];
    
    NSDictionary *result = [ JSONContent JSONValue ];
    
    if ( [ [ result valueForKey:@"login" ] compare: @"true" ] == NSOrderedSame ) {
        [ EticketandoAppDelegate setViewFromId: VIEWID_CHECKIN ];
    } else {
        //feedback negativo
        UIAlertView* alertView = [ [ UIAlertView alloc ] initWithTitle: @"Falha" message:@"Login inválido." delegate: nil cancelButtonTitle:@"OK" otherButtonTitles: nil ];
        [alertView show];
        [alertView release];
        
    }
    
    [ passwordTF setText: @"" ];
}
@end
