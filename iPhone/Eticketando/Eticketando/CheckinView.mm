//
//  Checkin.mm
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//
#include "constants.h"
#import "Event.h"
#import "CheckinView.h"
#import "EticketandoAppDelegate.h"

@implementation CheckinView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [ self initData ];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [ self initData ];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



/**
 
 **/
- (void)dealloc
{
    [super dealloc];
}

- ( void ) initData {    
    currentEvent = [ [ EticketandoAppDelegate instance ] eventWithId: 0 ];
    [ self updateUI ];
}

- ( void ) scan {
  
    ZBarReaderViewController *reader = [ ZBarReaderViewController new ];

    [ reader.scanner setSymbology: ZBAR_NONE
                           config: ZBAR_CFG_ENABLE
                               to: 0];

    
    [ reader.scanner setSymbology: ZBAR_QRCODE
                   config: ZBAR_CFG_ENABLE
                       to: 1];
    
    reader.readerDelegate = self;
    
    BOOL flash = ( [ [ [ EticketandoAppDelegate instance ] getConfigFor: CONFIGKEY_FLASH ] compare: @"true" ] == NSOrderedSame ) ? YES : NO;

    reader.cameraFlashMode = ( flash == YES )? UIImagePickerControllerCameraFlashModeOn : UIImagePickerControllerCameraFlashModeOff;
    
        
    reader.readerView.zoom = 1.0;
        
    [ EticketandoAppDelegate instance ].window.rootViewController = reader;
}

- (void) imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info {
    
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    
    for ( ZBarSymbol *item in results ) {
        NSLog( @"codigo: %@", [ item data ] );
        [ [ EticketandoAppDelegate instance ] setCurrentCode: [ item data ] ];
    }
    [reader dismissModalViewControllerAnimated: YES];
    [ [ EticketandoAppDelegate instance ] restoreViewController ];

    [ self validateManually ];
}

- ( void ) validateManually {
    [ EticketandoAppDelegate setViewFromId: VIEWID_MANUALVALIDATION ];
}

- ( void ) info {
    
}

- ( void ) toggleLamp {
    BOOL currentState;
    NSString *value;

    value = [ [ EticketandoAppDelegate instance ] getConfigFor: CONFIGKEY_FLASH ];
    
    currentState = ( [ value compare: @"true" ] == NSOrderedSame ) ? YES : NO;
    
    if ( currentState == YES )
        value = @"false";
    else
        value = @"true";
        
        
    [ [ EticketandoAppDelegate instance ] setConfigFor: CONFIGKEY_FLASH with: value  ];
    
    [ self updateUI ];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController*)picker {
    [ [ EticketandoAppDelegate instance ] restoreViewController ];    
}

- (void) readerControllerDidFailToRead:(ZBarReaderController*)reader withRetry:(BOOL)retry {
    [reader dismissModalViewControllerAnimated: YES];
    [ [ EticketandoAppDelegate instance ] restoreViewController ];
    [ [ EticketandoAppDelegate instance ] setCurrentCode: @"-" ];
    [ EticketandoAppDelegate setViewFromId: VIEWID_MANUALVALIDATION ];   
}

- ( IBAction ) back {
    [ EticketandoAppDelegate setViewFromId: VIEWID_LOGIN ];   
}

- ( IBAction ) gotoTickets {
    [ EticketandoAppDelegate setViewFromId: VIEWID_TICKETSLIST ];      
}


- ( IBAction ) logout {
    [ EticketandoAppDelegate setViewFromId: VIEWID_LOGIN ];   
}

- ( void ) updateUI {
    [ self performSelectorOnMainThread: @selector( updateUIMainThread ) withObject: self waitUntilDone: YES ];
}

- ( void ) updateUIMainThread {

    NSString *value;
    
    value = [ [ EticketandoAppDelegate instance ] getConfigFor: CONFIGKEY_FLASH ];
    
    if ( [ value compare: @"true" ] == NSOrderedSame ) {
        [ lampButton setImage: [ UIImage imageNamed:@"bt_lamp_on" ] forState: UIControlStateNormal ];
    } else {
        [ lampButton setImage: [ UIImage imageNamed:@"bt_lamp_off" ] forState: UIControlStateNormal ];        
    }
    
    NSString *tmp;
    
    tmp = [ currentEvent getDate ];
    [ timeLabel setText: tmp ];
    
    tmp = [ currentEvent getDate ];
    [ dateLabel setText: tmp ];
    
    tmp = [ currentEvent getName ];    
    [ nameLabel setText: tmp ];
    
    tmp = [ currentEvent getLocation ];
    [ locationLabel setText: tmp ];
    
    NSMutableString *filtersStr = [ [ NSMutableString alloc ] init ];
    NSArray *filters = [ currentEvent getFilters ];
    
    for ( int c = 0; c < [ filters count ]; ++c ) {
        
        if ( c != 0 )
            [ filtersStr appendString: @", " ];            
        
        [ filtersStr appendString: [ filters objectAtIndex: c ] ];
    }
    
    [ filtersLabel setText: [ NSString stringWithString: filtersStr ] ];
    
    [ filtersStr release ];
    
    [ self setNeedsDisplay ];
}

@end
