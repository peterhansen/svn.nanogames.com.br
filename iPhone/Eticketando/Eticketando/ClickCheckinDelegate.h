//
//  ClickCheckinDelegate.h
//  Eticketando
//
//  Created by Daniel Monteiro on 9/5/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ClickCheckinDelegate <NSObject>
- ( void ) peformCheckinFor: ( TicketCostumer * ) buyer onView: ( UIView * ) view ;
@end
