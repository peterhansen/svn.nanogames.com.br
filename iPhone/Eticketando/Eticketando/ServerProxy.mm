//
//  ServerProxy.mm
//  Eticketando
//
//  Created by Daniel Monteiro on 8/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ServerProxy.h"


@implementation ServerProxy



+ ( NSString *) BASE_URL {
    return @"http://jad.nanogames.com.br/eticketando/";
}

+ ( NSString *) LOGIN {
    return @"login.json";
}


+ ( NSString *) EVENT_DATA_REQUEST {
    return @"buyers.json";
}

+ ( NSString * ) TAG_PASSWDHASH {
    return @"pass";
}

+ ( NSString * ) TAG_USERID {
    return @"userid";
}


+ ( NSString * ) EVENT_LIST_DATA_REQUEST {
    return @"events.json";
}

/**
 * Indica fim de parametros
 */
+ ( NSString *) PARAMS_END {
    return @";";
}
/**
 * Indica comeo dos parametros
 */
+ ( NSString *) PARAMS_START_MARKER {
    return @"?";
}
/**
 * Indica o comeo do valor do parametro
 */
+ ( NSString *) VALUE_START {
    return @"=";	
}
/**
 * Indica separador de parametros
 */
+ ( NSString *) PARAM_SEPARATOR {
    return @"&";
}

/**
 * Caracter de fim de linha
 * */
+ ( NSString *) NEW_LINE {
    return @"\n";
}

+ ( NSString * ) JSONFromUrl: ( NSString * ) url {
    
    NSString *toReturn = nil;
    NSURL *urlObj;
    
    urlObj = [ NSURL URLWithString: url ];
    toReturn = [ NSString stringWithContentsOfURL: urlObj encoding: NSUTF8StringEncoding error: nil ];
    
    return toReturn;
}

@end
