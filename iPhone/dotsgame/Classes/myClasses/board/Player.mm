/*
 *  Player.mm
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "Player.h"
#include "GameBaseInfo.h"
#include "PowerUp.h"
#include "CharacterFactory.h"

/*
 construtor
 */
Player::Player():Ident( 0 ),
nome( "Player" ),
score( 0 ),
boneco( NULL ),
pPower( NULL ),
state( PLAYER_MATCH_STATE_NONE ),
specialLevel( 0 ),
//powerUpCurrentBarValue( 0.0f ),
typeUser( PLAYER_USER_USER ){}


/*=============================================================
 
carrega as informacões de acordo com gamebaseinfo

 =============================================================*/
 void Player::LoadInfo( GameBaseInfo g, uint8 index ){

	setNome( g.nomes[ index ] );
	loadDoll( g.iddolls[ index ] );
	setId( index );
	//setTypeUser( g.pUserType[ index ] );
}


/*=======================================================================================
 
 carrega a personagem do jogador, deve ser chamada na hora de carregar a tela

 ========================================================================================*/
void Player::loadDoll( IdDoll _id ){
	boneco = CharacterFactory::GetInstance()->LoadCharacterIndice( _id );
}

//destrutor
Player::~Player(){
	SAFE_DELETE( boneco );
	destroyPowerUp();
}
/*==========================================================================================
 
updatePowerUp-> atualiza o powerUp, se necessário
 
 ==========================================================================================*/
void Player::updatePowerUp( /*DotGame* dg*/ ){
if( pPower!= NULL )
	pPower->updatePowerUp();
}
/*==========================================================================================
 
 releasePowerUp-> solta o powerUp, 
 
 ==========================================================================================*/
void Player::releasePowerUp( void ){
 	if( pPower != NULL ){
		pPower->onRelease();
	//	pPower = NULL;
		destroyPowerUp();
	}
}


void Player::inputPowerUp( Point3f *p ){
	if( pPower != NULL )
		pPower->InputPosition( p );
	
}

void Player::doPowerUp( DotGame* dg ){
	if( pPower != NULL ){
		pPower->powerUpDo( dg );
		destroyPowerUp();
	}
}


//void Player::setPowerUp( PowerUp* p ){
//	pPower = p;
//	pPower->gainedByPlayer( this );
//}
void Player::setPowerUp( PowerUp* p ){
	if( pPower )
		destroyPowerUp();
	pPower = p;
	if( pPower ){
		pPower->setVisible( false );
		pPower->gainedByPlayer( this );
	}
}

void Player::destroyPowerUp( void ){
	//SAFE_DELETE( pPower );
	pPower = NULL;
}

//isso é da barra de especial, sim ela é escalável
void Player::setSpecialLevel( uint8 u ){
	specialLevel = ( u >= MAX_SPECIAL_LEVEL ?  MAX_SPECIAL_LEVEL : u );//u;
}
