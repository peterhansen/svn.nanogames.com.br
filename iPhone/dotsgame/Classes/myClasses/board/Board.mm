/*
 *  Board.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Board.h"
#include "Marks.h"
#include "DotGame.h"
#include "ObjcMacros.h"
#include "OptimizedTexPattern.h"
//#include "LineGroup.h"
//#include "ZoneGroup.h"

#define OBJECTS_INSIDE_BACKGROUND 2//4
//TODOO: COLOCAR as linhas e os quadrados/zonas aqui!!!

Board::Board( idBackground _id , GameBaseInfo *g ):ObjectGroup( OBJECTS_INSIDE_BACKGROUND ),nameBgMusic(0),alt( NULL ){
	alt = new OptimizedTexPattern( "texmapa" );
#if DEBUG
	alt->setName("fundo modo mapa \n");
#endif 
	createMarks( g );
	//creatingBoard( g );
}

void Board::createMarks(  GameBaseInfo *g ){ 

Marks *pMarks = new Marks( g );
	//pMarks->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0);
	if( !insertObject( pMarks ) ) {
#if DEBUG
		LOG("erro criando os marcos");
#endif
	}
}

/*============================================================
 
 creatingBoard=>cria as linhas e as zonas

 ============================================================*/

//void Board::creatingBoard( GameBaseInfo *g ){ 
//	ZoneGroup* zoneManager = new ZoneGroup( *g );
//	if( !insertObject( zoneManager ) )
//		return ;//false;
//	
//	LineGroup* lineManager = new LineGroup( *g );
//	if(	!insertObject( lineManager ) )
//		return ;//false;
//	
//	uint8 x,y;
//	ZoneGame* z;
//	for ( x = 0; x < g->nHZones; x++) {
//		for ( y = 0; y< g->nVZones/* nVerticalZones*/ ; y++ ){
//			z = new ZoneGame();
//			z->setPosition( x, y );
//			zoneManager->insertObject( z );
//		}
//	}
//	LineGame* l;
//	for ( x = 0; x <  g->nHZones/*nHorizontalZones*/ ; x++ ) {
//		for ( y = 0; y < g->nVZones + 1/*nVerticalLines  */; y++) {
//			l = new LineGame( LINE_GAME_ORIENTATION_HORIZONTAL );
//			l->setPosition( x, y);
//			lineManager->insertObject( l );
//		}
//		
//	}
//	lineManager->setVisible(false);
//	zoneManager->setVisible(false);
//	for ( x = 0; x <  g->nHZones + 1/*nHorizontalLines*/  ; x++ ) {
//		for ( y = 0; y < g->nVZones/*nVerticalZones*/ ; y++) {
//			l = new LineGame( LINE_GAME_ORIENTATION_VERTICAL );
//			l->setPosition( x, y);
//			lineManager->insertObject( l );
//		}
//	}
//}

void Board::setObserving( bool b ){
	observing = b;

	if( getObject(INDEX_BOARD_BACKGROUND) ){
		getObject(INDEX_BOARD_BACKGROUND)->setVisible(!observing);
		alt->setPosition( getObject(INDEX_BOARD_BACKGROUND)->getPosition() );
		alt->setSize( getObject(INDEX_BOARD_BACKGROUND)->getSize()->x,getObject(INDEX_BOARD_BACKGROUND)->getSize()->y );
		alt->setVisible(observing);
	}
	static_cast<Marks*> ( getObject( INDEX_BOARD_MARKS) )->setObserved( b );
}

Board::~Board( void ){
	pZoneGroup = NULL;
	pLineGroup = NULL;
	SAFE_DELETE( alt );
}


bool Board::render( void ){
//pode ser melhorado, eu sei, mas a pressa está falando mais alto...
//todoo: caso queiram elaborar mais a ordem de renderização dos componentes, é só colocar aqui, junto dos elementos do cenários( cercas, quad/zonas, linhas, imagem de fundo, pontos )é só colocar aqui.
	if( !isVisible() )
		return false;
	bool ret = false;
	//glTranslatef( position.x,position.y,position.z );

	if( !isObserving() ){
		ret |= ObjectGroup::render();

		
		glPushMatrix();
		glTranslatef( position.x,position.y,position.z );
		ret |= pLineGroup->renderLines();
		glPopMatrix();
		
		glPushMatrix();
		ret |= pZoneGroup->render();
		glPopMatrix();
		
		glPushMatrix();
		glTranslatef( position.x,position.y,position.z );
		ret |= pLineGroup->renderFences();
		glPopMatrix();
	}else {	
		glPushMatrix();
		glTranslatef( position.x,position.y,position.z );
		ret |=	alt->render();
		ret |= getObject( INDEX_BOARD_MARKS )->render();
		ret |= pLineGroup->renderLines();
		glPopMatrix();
		
		glPushMatrix();
		ret |= pZoneGroup->render();
		glPopMatrix();
	}
return ret;
}