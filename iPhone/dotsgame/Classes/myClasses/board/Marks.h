//
//  Marks.h
//  dotGame
//
//  Created by Max on 1/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#ifndef MARKS_H
#define MARKS_H 1

#include "DotGameInfo.h"
#include"ObjectGroup.h"
#include "BoardFactory.h"
#include"Color.h"
#include "VertexSet.h"
#include "GLHeaders.h"


class GameBaseInfo;
class Sprite;

class Marks : public ObjectGroup {
	
public:
	
	Marks( GameBaseInfo *g );


	~Marks();
	void reconfigureMarks( /*idBackground idBg*/ int8 i );
	
	void disappearMark( const Point3f *p );
	
	void reappearMark( const Point3f *p );

	void setObserved( bool b ){ observed = b; };
	
	bool isObserved( void );
	
	virtual bool render();
	
private:
	
	// Cria o conjunto de vértices que será utilizado para renderizar o objeto
	bool  buildMarks( GameBaseInfo *g );
	//essa é a cor do modo mapa, se depois quiserem mudar para outras cores, é só mudar a cor...
	Color c;
	bool observed;
	
};
inline bool Marks::isObserved( void ){
	return observed;
}

#endif