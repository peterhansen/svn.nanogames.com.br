/*
 *  BoardFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "BoardFactory.h"
#include "Board.h"
#include "GameBaseInfo.h"


#include "AngelBg.h"
#include "DevilBg.h"
#include "ExecutiveBg.h"
#include "FairyBg.h"
#include "FarmerBg.h"
#include "OgreBg.h"

BoardFactory* BoardFactory::pSingleton = NULL;



bool BoardFactory::Create( void ){
if( !pSingleton )
	pSingleton = new BoardFactory();
#if DEBUG
	NSLog(@"boardfactory Criada!");
#endif
	return pSingleton!= NULL;
}

void BoardFactory::Destroy( void ){
	SAFE_DELETE( pSingleton );
}

BoardFactory::BoardFactory( GameBaseInfo* g ):gInfo( g ),nBoards( N_BOARDS ) {
#if DEBUG
	NSLog(@"criando BoardFactory");	
#endif
}

BoardFactory::~BoardFactory(){

	gInfo = NULL;

#if DEBUG
	NSLog(@"destruindo BoardFactory");
#endif
}

Board* BoardFactory::getNewBoard( /*idBackground _id*/ ){

	if( pSingleton == NULL )
		Create();
	
	Board* b;
	
	switch ( gInfo->idBg /*_id*/ ) {
		case ID_BGD_ANGEL :
			b = new AngelBg( gInfo );
			break;
		case ID_BGD_DEVIL :
			b = new DevilBg( gInfo );
			break;
		case ID_BGD_FAIRY :
			b = new FairyBg( gInfo );
			break;
		case ID_BGD_OGRE :
			b = new OgreBg( gInfo );
			break;
		case ID_BGD_ENGINNER :
			b = new ExecutiveBg( gInfo );
			break;
		case ID_BGD_FARMER :
			b = new FarmerBg( gInfo );
			break;
	}
	
	return b;	
}


BoardFactory* BoardFactory::GetInstance( void ) {
	if( !pSingleton )
		Create();
	return pSingleton; 
}


UIImage* BoardFactory::getBackgroundByIndex( uint8 index ){
	UIImage* u;
	
switch ( index ) {
	case 0:
	//	[ UIImage];
		break;
	default:
		break;
}
}

uint8 BoardFactory::setFrameByCharacter( void ){
	uint8 res;
	switch ( gInfo->idBg ) {
			
		case ID_BGD_ANGEL :
			res = INDEX_LINE_BGD_ANGEL;
			break;
		case ID_BGD_DEVIL :
			res = INDEX_LINE_BGD_DEVIL;
			break;
		case ID_BGD_ENGINNER :
			res = INDEX_LINE_BGD_ENGINNER;
			break;
		case ID_BGD_FARMER :
			res = INDEX_LINE_BGD_FARMER;
			break;
		case ID_BGD_FAIRY :
			res = INDEX_LINE_BGD_FAIRY;
			break;
		case ID_BGD_OGRE :
			res = INDEX_LINE_BGD_OGRE;
			break;
	}			
	return res;
	
}


uint8 BoardFactory::setFrameLineByCharacter( IdDoll _id ){
	switch ( _id ) {
		case DOLL_ID_ANGEL :
			return INDEX_LINE_ANGEL;
			break;
		case DOLL_ID_DEVIL :
			return INDEX_LINE_DEVIL;
			break;
		case DOLL_ID_FAIRY :
			return INDEX_LINE_FAIRY;
			break;
		case DOLL_ID_OGRE :
			return INDEX_LINE_OGRE;
			break;
		case DOLL_ID_ENGINNER :
			return INDEX_LINE_ENGINNER;
			break;
		case DOLL_ID_FARMER :
			return INDEX_LINE_FARMER;
			break;
	}
	return 255;
	
}




/*
 retorna o nome da música de fundo de acordo com o bg, depois tem que acertar
 */
uint8 BoardFactory::getBgMusicName( void ){
	
	uint8 ret = 255;
	
	switch ( gInfo->idBg ) {
		case ID_BGD_OGRE:
			ret = 64;
			break;

		case ID_BGD_FAIRY:
			ret = 65;
			break;
		case ID_BGD_FARMER:
			ret = 66;
			break;
		case ID_BGD_ENGINNER:
			ret = 6;
			break;
		case ID_BGD_DEVIL:
			ret = 6;
			break;
		case ID_BGD_ANGEL:
			ret = 6;
			break;
		
	
	}
	return ret;
}

