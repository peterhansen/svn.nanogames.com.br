/*
 *  LineGame.mm
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "LineGame.h"
#include "Player.h"
#include "Utils.h"
#include "RenderableImage.h"
#include "ResourceManager.h"
#include "DotGameInfo.h"
#include "ZoneGame.h"
#include "ObjcMacros.h"
#include "Quad.h"
#include "BoardFactory.h"
#include "LineGroup.h"
#include "FreeKickAppDelegate.h"
#include "GenericDoll.h"


//#define INDEX_LINE_CLOUD 2

#define OBJECTS_INSIDE_LINE_GAME 2//3

#define OFFSET_LINE_ADJUST_POSITION_LINE_HORIZONTAL -20.0f
#define OFFSET_LINE_ADJUST_POSITION_LINE_VCERTICAL -10.0f

#define BONUS_SIZE_LINE_IN_X 5.0f
#define BONUS_SIZE_LINE_IN_Y 5.0f
Sprite* LineGame::linhaBgH = NULL; 
Sprite* LineGame::linhaBgV = NULL;
RenderableImage* LineGame::linhaAltH = NULL;
RenderableImage* LineGame::linhaAltV = NULL;


LineGame::LineGame( LineGameOrientation l/*, idBackground Bg*/ ):ObjectGroup( OBJECTS_INSIDE_LINE_GAME ),
orientation( l ),
state( LINE_GAME_STATE_BLANK ),
occupied( false ),
selected( false ),
observed( false ),
lColor( COLOR_TRANSPARENT ),
alt( NULL ),
keepInvisible( false ),
locked( false ),
idPlayer( -1 )
{
	if( l == LINE_GAME_ORIENTATION_VERTICAL ){
		size.set( LINE_VERTICAL_WIDTH ,LINE_VERTICAL_HEIGHT ,1.0 );
	}else if( l == LINE_GAME_ORIENTATION_HORIZONTAL ) {
		size.set( LINE_HORIZONTAL_WIDTH , LINE_HORIZONTAL_HEIGHT, 1.0 );
	}
	
	setPlayerImage( /*Bg*/ );
#if DEBUG
	setName("Linha \n");
#endif
}

void LineGame::loadImages( void ){
		linhaBgH = new Sprite( "lh","lh",ResourceManager::Get16BitTexture2D );
		linhaBgV = new Sprite( "lv","lv",ResourceManager::Get16BitTexture2D );
		linhaAltH = new RenderableImage("mapa_line_h");
		linhaAltV = new RenderableImage("mapa_line_v");
#if DEBUG 
	linhaAltH->setName("imagem alternativa estatica h \n");
	linhaAltV->setName("imagem alternativa estatica v \n");
	linhaBgH->setName("linha hor fundo estatica\n");
	linhaBgV->setName("linha ver fundo estatica\n");
	NSLog(@"imagens estáticas carregados\n");	
#endif
}


void LineGame::removeImages( void ){

	SAFE_DELETE( linhaBgH );
	SAFE_DELETE( linhaBgV );
	SAFE_DELETE( linhaAltH );
	SAFE_DELETE( linhaAltV );
#if DEBUG
	NSLog(@"imagem estáticas descarregados \n");	
#endif
}

/*================================================================================
 
 puxa para si mesmo as imagens que estiverem armazenadas no array de jogadores  e no final aloca a imagem em branco...
 
 ================================================================================*/

void LineGame::setPlayerImage( /*idBackground Bg*/ ){
	
	Sprite* currSprite;

	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		currSprite = new Sprite( LineGame::linhaBgV );
		currSprite->setSize( LINE_VERTICAL_WIDTH ,LINE_VERTICAL_HEIGHT, 1.0f );
		
	}else 
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ) {
			currSprite = new Sprite( LineGame::linhaBgH );
			currSprite->setSize( LINE_HORIZONTAL_HEIGHT , LINE_HORIZONTAL_WIDTH, 1.0f );
		}
#if DEBUG
	currSprite->setName("linha de fundo\n");
#endif
	currSprite->setAnimSequence( BoardFactory::GetInstance()->setFrameByCharacter() );

	if( !insertObject( currSprite ) )
#if DEBUG
		NSLog(@" sprite fundo não inserido ");
#endif


}


/*===================================================================
 
 determina o estado da linha e faz possíveis correções
 
 ====================================================================*/

void LineGame::setLineGameState( LineGameState l ){
	lastState = state;
	state = l;
	switch ( l ) {
		case LINE_GAME_STATE_NONE:
		case LINE_GAME_STATE_SELECTED:
			lColor = COLOR_YELLOW;
			break;
		case LINE_GAME_STATE_OCCUPIED:
			if( lastState == LINE_GAME_STATE_ANIMATING )
				removeObject( 3 );
			occupied = true;
			break;
		case LINE_GAME_STATE_BLANK:
			lColor = COLOR_TRANSPARENT;
			selected = false;
			occupied = false;
			break;
		case LINE_GAME_STATE_OBSERVING:
			keepInvisible = isVisible();
			if( !occupied )
				setVisible( false );
			if( locked )
			setVisible( true );
			observed = true;
			break;
			
	}
	if( lastState == LINE_GAME_STATE_OBSERVING ){
		if( observed ){
			observed = false;
			}
		setVisible( keepInvisible );
		SAFE_DELETE( alt );
	}
	updateImage();
}

/*============================================================================
 
 checkCollision-> retorna true, se houve alguma colisão dentro dela

==============================================================================*/

bool LineGame::checkCollision( const Point3f* pPoint,Player *p ) {
	
	if( Utils::IsPointInsideRect( pPoint, position.x  , position.y ,size.x + BONUS_SIZE_LINE_IN_X ,size.y + BONUS_SIZE_LINE_IN_Y ) &&
	   getLineGameState() != LINE_GAME_STATE_NONE &&
	   isVisible() && !locked ){
#if DEBUG
		//NSLog(@"colidiu na linha x = %.2f, y = %.2f",position.x,position.y);
#endif
		/*if ( state == LINE_GAME_STATE_SELECTED && selected ) {
			gainedByPlayer( p );	
			setLineGameState( LINE_GAME_STATE_OCCUPIED );

			return true;
		}*/
		
		if ( state == LINE_GAME_STATE_BLANK) {
			setLineGameState( LINE_GAME_STATE_SELECTED );

			return true;
		}

	}
	return false;
}


/*========================================================================
 
 updateImage-> troca a figura da linha de acordo com o estado dela
 
 ==========================================================================*/

void LineGame::updateImage( void ){

	switch ( state ) {
		case LINE_GAME_STATE_BLANK:
			static_cast< Sprite* > (getObject( INDEX_LINE_BACKGROUND ))->setVertexSetColor( COLOR_WHITE );
			//getObject( INDEX_LINE_PLAYERS )->setVisible( false );
			break;

		case LINE_GAME_STATE_SELECTED:
			static_cast< Sprite* > ( getObject( INDEX_LINE_BACKGROUND ) )->setVertexSetColor( COLOR_GREY );
			selected = true;
			break;

		case LINE_GAME_STATE_OCCUPIED:
			static_cast< Sprite* > (getObject( INDEX_LINE_BACKGROUND ))->setVertexSetColor( COLOR_WHITE );
			getObject( INDEX_LINE_PLAYERS )->setVisible( true );
//			static_cast< Sprite* > ( getObject( INDEX_LINE_PLAYERS ) )->setVisible( true );
			break;
		case LINE_GAME_STATE_OBSERVING:
			if ( locked || occupied ){
				//return;
				if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ){
					alt = new RenderableImage( linhaAltH );
					alt->setPosition( position.x + 10.0, position.y );
				}else {
					alt = new RenderableImage( linhaAltV );
					alt->setPosition( position.x , position.y + 10.0);
				}
				
				alt->setVertexSetColor( lColor );
#if DEBUG
				alt->setName("imagem alternativa linha\n");
#endif
			}
			break;

	}
	

}

/*======================================================================================
 
 posiciona a linha dget
 =====================================================*/
void LineGame::setPosition( uint x, uint y ){
	Point3f p;//,m;
//	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
//		
//		p.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
//			  LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
//			  1.0);
//
//		m.set( 0.0f,
//			  0.0f);
//	}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
//		
//		p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
//			  y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
//			  1.0);
//		m.set( ( FENCE_HORIZONTAL_WIDTH * 0.1f )	,
//			  0.0);
//	}
 p = LineGroup::getLineCoordenatesByXY( x, y ,  orientation == LINE_GAME_ORIENTATION_VERTICAL );
	position.set( p );	
	
	
	//getObject( INDEX_LINE_BACKGROUND )->setPosition( &m );
}


/*=============================================================================================
 
 restart-> para ser chamado depois de qq turno
 
 =====================================================================================================*/

void  LineGame::restart( void ){
	if( !occupied ){
		setLineGameState( LINE_GAME_STATE_BLANK );
		selected = false;
	}

}

/*=============================================================
 
 reset->deixa a linha no seu estado original
 
 =================================================================*/

void LineGame::reset( void ){
	setLineGameState( LINE_GAME_STATE_BLANK );
}

/*===========================================================================

 puxa a imagem da linha de acordo com o fundo
 
===========================================================================*/
void LineGame::getBackgroundImage( Background *bg ){

	
	/*if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		currSprite = bg->getBaseLineV();
	}else 
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ) {
			currSprite = bg->getBaseLineH();
		}*/
}

/*=================================================================================
 
 acerta a imagem da linha de acordo com o personagem do jogador
 
================================================================================*/
void LineGame::gainedByPlayer( Player *p ){
	//por enquanto, somente mudar as cores...!
	//	correto!	
	idPlayer = p->getId();
	
//	setLineGameState( LINE_GAME_STATE_ANIMATING );
	
	/*
	 depois mudar para a fç retornar um tipo objeto que vai ser inserido no grupo de objetos, deste jeito, independe de ser uma linha ou um quadro quaisquer
	 */

//temporário	
	if( p->getId() == 0 )
		lColor.set( COLOR_BLUE );
	else 
		lColor.set( COLOR_GREEN );
	
	Sprite* s = NULL;
	if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ){
		s = new Sprite(( Sprite* ) p->boneco->getLineHPic() );
	}
	else{
		
		s = new Sprite( ( Sprite* ) p->boneco->getLineVPic());
	}
#if DEBUG
	s->setName("linha jogador");
#endif
	
	Point3f f;
		if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		
			f.set( 0.0, OFFSET_LINE_ADJUST_POSITION_LINE_HORIZONTAL ,1.0f);
		}else {
		
			f.set( 0.0,OFFSET_LINE_ADJUST_POSITION_LINE_VCERTICAL, 0.0f);
		}
	s->setPosition( &f );
	s->setAnimSequence(BoardFactory::GetInstance()->setFrameLineByCharacter( p->boneco->getIdDoll() ) );
	insertObject( s );
	occupied = true;

}
	
void LineGame::lineGameCpuGained( Player *p ){
	if( occupied )
		return;
	gainedByPlayer( p );
	setLineGameState( LINE_GAME_STATE_OCCUPIED );
}

//destrutor
LineGame::~LineGame(){
	SAFE_DELETE( alt );
}


bool LineGame::render( void ){
	if( !isVisible() )
		return false;
	/*if( state == LINE_GAME_STATE_ANIMATING ){
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ){
			
		}else {
			
		}
		
	}*/
	else if ( state == LINE_GAME_STATE_OBSERVING /*&& ( occupied || locked ) /*observed*/ ) {
		return alt->render();
	}//else
		return ObjectGroup::render();
}


/*============-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0=-=-=-==-=-==-==-=-======-=--===-=----=-=-=-=====
 
ajustLinesToBackground->acerta a figura da linha de acordo com o fundo
 
 ============-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0=-=-=-==-=-==-==-=-======-=--===-=----=-=-=-=====*/
void LineGame::adjustLinesToBackground( int8 index ){

	static_cast<Sprite*> (INDEX_LINE_BACKGROUND)->setAnimSequence( index );
}

/*===============================================================================================
 
 lostByPlayer=> fç chamada qdo o jogador perde a linha, 
 
 ==============================================================================================*/

void LineGame::lostByPlayer( /*Player *p*/ ){
	
	if( !occupied )
		return;
#if DEBUG
	//NSLog(@"linha ( %3.3f , %3.3f ) perdida pelo jogador %d", position.x,position.y,idPlayer);
#endif
	
	lColor.set( COLOR_GREY );
	idPlayer = -1 ;
	removeObject( INDEX_LINE_PLAYERS );
	reset();
}


void LineGame::setLocked( bool b ){
	locked = b;
}

void LineGame::adjustLinesToPlayer( int8 index ){
	static_cast<Sprite*> ( getObject(INDEX_LINE_PLAYERS) )->setAnimSequence( index );
}



void LineGame::lockLine( void ){
	setLocked( true );
	lColor.set( COLOR_BLACK );
	Sprite* s = NULL;
	if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ){
		s = new Sprite(( Sprite* ) GenericDoll::linhaPlH );
	}
	else{
		
		s = new Sprite( ( Sprite* )GenericDoll::linhaPlV );
	}
#if DEBUG
	s->setName("linha jogador");
#endif
	
	Point3f f;
	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		
		f.set( 0.0, OFFSET_LINE_ADJUST_POSITION_LINE_HORIZONTAL ,1.0f);
	}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
		
		f.set( 0.0,OFFSET_LINE_ADJUST_POSITION_LINE_VCERTICAL, 0.0f);
	}
	s->setPosition( &f );
	s->setAnimSequence(BoardFactory::GetInstance()->getLockFrame() );
	insertObject( s );
	
	//static_cast<Sprite*> ( getObject(INDEX_LINE_PLAYERS) )->setAnimSequence( BoardFactory::GetInstance()->getLockFrame() );
}	



//renderiza a linha
bool LineGame::renderLine( void ){
	if( !isVisible() )
		return false;
	else if ( state == LINE_GAME_STATE_OBSERVING /*&& ( occupied || locked ) /*observed*/ ) 
		return alt->render();
	glPushMatrix();
	glTranslatef(position.x , position.y, position.z);
	bool ret = getObject(INDEX_LINE_BACKGROUND)->render();
	glPopMatrix();
	return ret ;
}
//renderiza a cerca
bool LineGame::renderFence( void ){
	if( !isVisible() || state != LINE_GAME_STATE_OCCUPIED )
		return false;
	glPushMatrix();
	glTranslatef(position.x, position.y, position.z );
	bool ret = getObject(INDEX_LINE_PLAYERS)->render();
	glPopMatrix();
	return ret ;
	
}

#if DEBUG
void LineGame::renderQuads( void ){
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE );
	glEnableClientState( GL_VERTEX_ARRAY );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	glTranslatef( position.x + ( size.x * 0.5f ), position.y + ( size.y * 0.5f ), position.z );
	glScalef( size.x, size.y, size.z );
	
	
	
	Point3f border[] =	{
		Point3f( -0.5f, -0.5f ),
		Point3f(  0.5f, -0.5f ),
		Point3f(  0.5f,  0.5f ),
		Point3f( -0.5f,  0.5f )
	};
	
	glColor4ub( 255, 0, 255, 125 );
	glVertexPointer( 3, GL_FLOAT, 0, border );
	glDrawArrays(GL_TRIANGLE_FAN,0,4 );//GL_LINE_LOOP, 0, 4 );
	
	glPopMatrix();
}
#endif
