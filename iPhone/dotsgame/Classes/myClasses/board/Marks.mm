//
//  Marks.mm
//  dotGame
//
//  Created by Max on 1/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#include "Marks.h"
#include "GameBaseInfo.h"
#include "Sprite.h"
#include "Sphere.h"
#include "Exceptions.h"
#include "Utils.h"
using namespace Utils;

#define MARK_SPHERE_LOD 8//10

Marks::Marks( GameBaseInfo *g ):ObjectGroup( ( g->nVZones + 1 ) * ( g->nHZones + 1 ) ),c( COLOR_BLACK), observed( false ){
if	( !buildMarks( g ) )
	 return;
}

bool Marks::buildMarks( GameBaseInfo *g ){
	//como é um sprite com todos os ptos, ele  carrega daqui mesmo, e cria várias cópias. como tem aquele problema esquisito com sprites estáticos, é melhor carregar uma instância aqui mesmo e criar por cópia. depois mudar para que todas as chamadas de texturas sejam colapsadas nesta classe, no render!!
	Sprite *s = new Sprite( "pts","pts"/*, ResourceManager::Get16BitTexture2D*/ );
	Point3f p;
	for ( uint8 x = 0; x < ( g->nHZones + 1 ) ; x++ ) {
		for ( uint8 y = 0; y < ( g->nVZones + 1 ); y++) {
			Sprite *s2 = new Sprite( s );
#if DEBUG
			char name[ 23 ];
			snprintf( name , 23 , "marca: x= %d, y=%d \n" , x , y );
			s2->setName( name );
#endif
			p.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT),
				  ( y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE) ),
				  1.0);
			s2->setPosition( &p );
			if( !insertObject( s2 ) ){
				return false;
				break;
			}
		}
	}
	return true;
}


//mudar isto para que receba um numero e com esse numero fosse para a linha correspondente da pesonagem
void Marks::reconfigureMarks( /*idBackground idBg*/ int8 i ){
	Sprite *s2;
	for( uint32 x = 0; x < getNObjects(); x++ ){
		s2 = dynamic_cast< Sprite* > ( getObject( x ) ) ;
		s2->setAnimSequence( i );
	}
}
/* ++++++++++++++++++++===============================================
 
disappearMark => faz uma marca desaparecer.
 
 ++++++++++++++++++++===============================================*/
void Marks::disappearMark( const Point3f *p ){
	
	for( int16 i= 0; i< getNObjects(); i++ ){
	
		if(  IsPointInsideRect( p , getObject( i ) ) ){
			getObject(i)->setVisible( false );	
		}
	}
}

/* ++++++++++++++++++++===============================================
 
 reappearMark => faz uma marca aparecer.
 
 ++++++++++++++++++++===============================================*/
void Marks::reappearMark( const Point3f *p ){

	for( int16 i= 0; i< getNObjects(); i++ ){
		
		if(  IsPointInsideRect( p , getObject( i ) ) ){
			getObject(i)->setVisible( true );	
		}
	}

}


Marks::~Marks(){}
/* ++++++++++++++++++++===============================================
 
 render => faz a renderização dos marcos
 
 ++++++++++++++++++++===============================================*/
bool Marks::render(){
	
	if( !Object::render() )
		return false;
	
	if( !observed ){
		return ObjectGroup::render();
	}
	
	
	glDisable( GL_DEPTH_TEST );
	
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	// Desabilita a texturização
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glDisable( GL_TEXTURE_2D );
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );
	glColor4ub( c.getUbR(), c.getUbG(), c.getUbR(), c.getUbA() );
	glMatrixMode( GL_MODELVIEW );

	Point3f mSize, mPosition, mScale;
	glTranslatef( position.x, position.y, position.z );
	glScalef( scale.x, scale.y, scale.z );
	
	for( int16 i = 0; i < getNObjects(); ++i  ){

		if( !getObject( i )->isVisible() )
			continue;
		
		glPushMatrix();
		Sphere s( MARK_SPHERE_LOD );
		mPosition = *getObject( i )->getPosition();
		mSize = *getObject( i )->getSize();
		mScale = *getObject( i )->getScale();

		glTranslatef( mPosition.x +( ( mSize.x * mScale.x ) * 0.5 ), mPosition.y +( ( mSize.y * mScale.y ) * 0.5 ) ,1.0f );
		glScalef(mScale.x * mSize.x,mScale.y * mSize.y, 1.0f );

		s.render(); 
		glPopMatrix();

	}

	
	glColor4ub( 255, 255, 255, 255 );
	
	if( tex2DWasEnabled == GL_TRUE )
		glEnable( GL_TEXTURE_2D );
	
	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );
	
	return true;
}

//VertexSetHandler Marks::CreateVertexSet( void )
//{
//	Sphere* pSphere = new Sphere( MARK_SPHERE_LOD );
//	if( !pSphere )
//#if TARGET_IPHONE_SIMULATOR
//		throw ConstructorException( "Marks::CreateVertexSet( void ): Unable to create vertex set" );
//#else
//		throw ConstructorException( "" );
//#endif
//
//	//pSphere->disableRenderStates( RENDER_STATE_COLOR_ARRAY | RENDER_STATE_NORMAL_ARRAY );
//	return VertexSetHandler( pSphere );
//}