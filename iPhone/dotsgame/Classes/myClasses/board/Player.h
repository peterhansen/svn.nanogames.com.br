/*
 *  Player.h
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef PLAYER_H
#define PLAYER_H 1

#include "Macros.h"
//#include "ObjectGroup.h"
#include "GenericDoll.h"
#include <string>
using std::string;

//meio inútil, mas pode ser útil a posteriore
enum PlayerMatchState{
	PLAYER_MATCH_STATE_NONE = -1,
	PLAYER_MATCH_STATE_WAITING = 0,
	PLAYER_MATCH_STATE_CHOOSING,
}typedef PlayerMatchState;

enum PlayerUser {
	PLAYER_USER_NONE = -1,
	PLAYER_USER_USER =0,
	PLAYER_USER_CPU = 1,
	//necessário, pq como ele muda o estado de jogo em fç do jogador, qdo for jogador que não seja local, ele espera o input.
	PLAYER_USER_USER_ON_LINE
}typedef PlayerUser ;


class LineGame;
class GameBaseInfo;
class PowerUp;
class DotGame;
//jogador
class Player {
public:
	
	//construtor
	Player();
	
	//destrutor
	~Player();
	
	//carrega as informacões de acordo com gamebaseinfo
	void LoadInfo( GameBaseInfo g, uint8 index );
	
	//carrega o boneco que o jogador escolhe
	void loadDoll( IdDoll _id );
	
	void updatePowerUp( /*DotGame* dg*/void );
	
	/*  Getters & Setters  */	
	
	inline	uint8 getId( void );
	
	inline	uint8 getScore( void );
	
	inline	string getNome( void );
	
	inline	void setNome( string s ); 
	
	//inline	void setNome( NSString s ); 
	
	inline	void setScore( uint8 s );
	
	inline	void setId( uint8 _id );
	
	inline void setTypeUser( PlayerUser p );
	
	inline PlayerUser getTypeUser( void );

	//jogador libera o power up
	void releasePowerUp( void );
	
	void inputPowerUp( Point3f *p );
	//jogador usa o power up	
	void doPowerUp( DotGame* dg );
	
	inline PowerUp* getPowerUp( void );
	
	 void setPowerUp( PowerUp* p );
	
	inline uint8 getSpecialLevel( void );
	
	void setSpecialLevel( uint8 u );
	
	void destroyPowerUp( void );
	
	inline bool havePowerUp( void );
		
 inline	GenericDoll* getDoll( void ){ return boneco; };
	
	
protected:
	friend class LineGame;	
	friend class ZoneGame;	
	
	// para multiplayer, identificar o jogador
	uint8 Ident;
	
	string nome;
	
	uint8 score;
	
	GenericDoll* boneco;
	
	PlayerMatchState state;
	
	PlayerUser typeUser;
	
	PowerUp* pPower;
	
	uint8 specialLevel;
};


uint8 Player::getId( void ){
	return Ident;
}

uint8 Player::getScore( void ){return score;}

string Player::getNome( void ){ return nome;}

void Player::setNome( string s ){ nome = s; }  

void Player::setScore( uint8 s ){ score = s; }

void Player::setId( uint8 _id ){ Ident = _id; }

void Player::setTypeUser( PlayerUser p ){ typeUser = p; }

PlayerUser Player::getTypeUser( void ){ return typeUser; }

PowerUp* Player::getPowerUp( void ){ return pPower; }


uint8 Player::getSpecialLevel( void ){
	return specialLevel;
}

bool Player::havePowerUp( void ){
	return pPower != NULL;
}

#endif