/*
 *  Board.h
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef BOARD_H
#define BOARD_H 1

#define INDEX_BOARD_BACKGROUND 1
#define INDEX_BOARD_MARKS 0


#include"Color.h"
#include "DotGameInfo.h"
#include "ObjectGroup.h"
#include "BoardFactory.h"

#include "ZoneGroup.h"
#include "LineGroup.h"

class Sprite;
class GameBaseInfo;
class OptimizedTexPattern;
//tabuleiro do cenário, depois seria bom se passassem todos as funções com linhas ( ponto, marcação, zona, etc. para cá! )

class Board : public ObjectGroup {

public:
	Board( idBackground _id, GameBaseInfo *g );
	
	virtual ~Board( void );
	
	idBackground getIdBackground( void );
	
	Color getCorBgd( void ); 
	
	void setMarkPosition( Point3f *p ){ getObject( INDEX_BOARD_MARKS )->setPosition( p ); };
	
	bool isObserving( void );
	
	void setObserving( bool b );
	//o nome da música está aqui mesmo...
	uint8 getNameBgMusic ( void );
	
	virtual bool render( void );
	
	void setZoneGroup( ZoneGroup *z ){ pZoneGroup = z; };
	
	void setLineGroup( LineGroup *l ){ pLineGroup = l; };
	
protected:
	
	void createMarks( GameBaseInfo *g );
	
	//void set
	
	virtual	void generateBackground(  GameBaseInfo* g  ) = 0;
	
	virtual	void configureMarks( void ) = 0;
	
	virtual void configureLinesBkgd( void ) = 0;
	
private:
	OptimizedTexPattern *alt;
	idBackground idBkgd;
	Color corBgd;
	bool observing;
	uint8 nameBgMusic;
	ZoneGroup *pZoneGroup;
	LineGroup *pLineGroup;
};

inline bool Board::isObserving( void ){
	return observing;
}

inline Color Board::getCorBgd( void ){
	return corBgd;
}
inline idBackground Board::getIdBackground( void ){
	return idBkgd;
}

inline uint8 Board::getNameBgMusic ( void ){
	return nameBgMusic;
}

#endif