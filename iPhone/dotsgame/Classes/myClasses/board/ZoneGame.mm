/*
 *  ZoneGame.mm
 *  dotGame
 *
 *  Created by Max on 10/9/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include"PowerUp.h"
#include "ZoneGame.h"
#include "Player.h"
#include "LineGame.h"
#include "GenericDoll.h"
#include "DotGame.h"
#include "DotGameInfo.h"
#include "Exceptions.h"
#include "Macros.h"
#include "RenderableImage.h"
#include "Sprite.h"
#include "Random.h"
#include "PowerUpFactory.h"
#include "ZoneGroup.h"


#define MAX_ZONES_SPRITES 2//fundo normal + grande fundo

#define INDEX_BACKGROUND_SPRITE 0
#define INDEX_BACKGROUND_BIG_SPRITE 1

#define OFFSET_ZONE_IMAGE_X 2.0
#define OFFSET_ZONE_IMAGE_Y OFFSET_ZONE_IMAGE_X

RenderableImage* ZoneGame::alternative = NULL;
RenderableImage* ZoneGame::alternativeBig = NULL;
RenderableImage* ZoneGame::lockedZone = NULL;

ZoneGame::ZoneGame():Object(),//:ObjectGroup( MAX_ZONES_SPRITES ),
playerId( 255 ),
occupied( false ), 
coupled( false ),
locked( false ),
currPic( NULL ),
observed( false ),
c( COLOR_TRANSPARENT ),
imageSpecial( NULL ),
imageBigSpecial( NULL ),
power( NULL )//,
/*zCoupledLeft( NULL ), 
zCoupledRight( NULL )*/{
	setZoneGameState( ZONE_GAME_STATE_NONE );
#if DEBUG
	setName("zonas\n");
#endif
	//PowerUpFactory::Create();
	setSize(ZONE_SIDE, ZONE_SIDE);
		
}

/*============================================================================
 
 adiciona as imagens dos jogadores nas zones
 
 =============================================================================*/
void ZoneGame::setPlayerImage( Player *p ){
	imageSpecial = new RenderableImage( ( RenderableImage* )p->boneco->getZoneSprt() );

	if( imageSpecial == NULL ){
#if DEBUG
	NSLog(@"imagem não inserida");
	throw ConstructorException("fail while seting quad/zone 1x1 image");
#endif
	throw ConstructorException();
	}
	imageSpecial->setPosition( position.x + OFFSET_ZONE_IMAGE_X , position.y + OFFSET_ZONE_IMAGE_Y);
#if DEBUG
	imageSpecial->setName("zona imagem\n ");
#endif
	
//	setZoneGameState( ZONE_GAME_STATE_BLANK );
	
}
/*============================================================================
 

 ============================================================================*/
void ZoneGame::setZoneGameState ( ZoneGameState z ){
	state = z;

	updateImage();
}

/*============================================================================
 
 quando uma zona é ganhada pelo jogador
 
 ============================================================================*/

bool ZoneGame::gainedByPlayer( /* DotGame *dg*/ Player *pl ){
	if( occupied || locked ){
#if DEBUG
//NSLog(@"\n\t\tzona: %d,%d já ocupada pelo jogador %d\n", x, y,playerId );
#endif
		return false;

	}
	pl->score++;
	playerId = pl->getId();
	if( playerId == 0 ){
		c = COLOR_BLUE;
	}else {
		c = COLOR_GREEN;
	}

	setPlayerImage( pl );
	setVisible(true);
	setZoneGameState( ZONE_GAME_STATE_OCCUPIED );
	if( power ){
		pl->setPowerUp( power );
		power = NULL;
	}
#if DEBUG
	
//	NSLog(@"\n\t\tzona( %3.2f,%3.2f ) ganhada pelo jogador %d\n\t\tzona: %d,%d\n", position.x, position.y, playerId,  x, y );
#endif
	return true;
}

/*============================================================================
 
 atualiza a imagem de acordo com o estado
 
 ============================================================================*/

void ZoneGame::updateImage( void ){

	switch ( state ) {
		case ZONE_GAME_STATE_NONE :
			//break;
			
		case ZONE_GAME_STATE_BLANK :
			break;
			
		case ZONE_GAME_STATE_OCCUPIED :
			occupied = true;
			break;
		default:
			break;
	}
}

bool ZoneGame::render( void ){
	if( !isVisible() )
		return false;

	bool ret = false;
	if( !observed ){
		if( imageSpecial )
			ret |= imageSpecial->render();
		if( imageBigSpecial )
			ret |= imageBigSpecial->render();
		//ret |= ObjectGroup::render();
	}else {
		if( !currPic )
			return true;
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		ret |=	currPic->render();
		glPopMatrix();
	}
	
	return ret;
}


/*============================================================================
 
 reseta
 
 ============================================================================*/

void ZoneGame::reset( void ){
	setZoneGameState( ZONE_GAME_STATE_BLANK );
}
/*============================================================================
 
 posiciona na tela
 
 ============================================================================*/

void ZoneGame::setPosition( uint8 x, uint8 y ){
	Point3f p;
//p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
//LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ), 1.0f);
	p = ZoneGroup::getZonePositionByXY( x, y );
	position.set( p );
}
/*============================================================================
 
 retorna a identificação do jogador que ocupou a zona
 
 ============================================================================*/
uint8 ZoneGame::getPlayerId( void ){
		return playerId;
}

ZoneGame::~ZoneGame(){
	SAFE_DELETE( currPic );
	SAFE_DELETE( imageSpecial );
	SAFE_DELETE( imageBigSpecial );
	power = NULL;
}
/* =================================================================================================

 lostByPlayer=> chamado qdo um jogador perder uma zona...
 
 =================================================================================================*/
void ZoneGame::lostByPlayer( void/*Player *p*/ ){
	
	if( !occupied )
		return;
		
#if DEBUG
	 NSLog(@"zona( %3.3f,%3.3f ) perdida pelo jogador %d",position.x,position.y, playerId);
#endif
	SAFE_DELETE( imageSpecial );
	//removeObject( INDEX_BACKGROUND_SPRITE );
	playerId = -1;
	occupied = false;
	//coupled = false ;
	reset();
}
/*===================================================================================

 adicona um PowerUp à zona 
 
===================================================================================*/
void ZoneGame::setPowerUp( PowerUp* p ){
	power = p;
	p->setZone( this );

}

/*===================================================================================
 
qdo uma das zonas se torna a responsavel por ter a imagem
 
 ===================================================================================*/

void ZoneGame::isMainBigZone( Player *p ){
	//old
//	RenderableImage *r;
//	r = new RenderableImage( p->boneco->getBigZoneSprt() );
	//r->setImage( p->boneco->getBigZoneSprt() );
	//getObject( INDEX_BACKGROUND_SPRITE )->setVisible( false );
	//insertObject( ( Object* ) p->boneco->getBigZoneSprt() );
	imageSpecial->setVisible( false );
	imageBigSpecial = new RenderableImage( ( RenderableImage *)  p->boneco->getBigZoneSprt() );
	if( !imageBigSpecial ){
#if DEBUG
	imageBigSpecial->setName("imagem especial 2x2 \n");
	NSLog(@"imagem não inserida");
	throw ConstructorException("fail while seting quad/zone 2x2 image");
#endif
	throw ConstructorException();

	}
	imageBigSpecial->setPosition( position.x, position.y );
}

/*=====================================================================================
 
 
 
 =====================================================================================*/

void ZoneGame::notIsMainBigZone( void ){ 
	//old
	//removeObject( INDEX_BACKGROUND_BIG_SPRITE );
//	getObject( INDEX_BACKGROUND_SPRITE )->setVisible( true );
	imageSpecial->setVisible( true );
	SAFE_DELETE( imageBigSpecial );

}

void ZoneGame::changePlayer( Player *p ){
	
		playerId = p->getId();

	if( imageBigSpecial ){
		SAFE_DELETE( imageBigSpecial );
		isMainBigZone( p );
	}
	if( imageSpecial ){
		SAFE_DELETE( imageSpecial );
		setPlayerImage( p );
	}
	/*old
	 if( getObject( INDEX_BACKGROUND_BIG_SPRITE ) ){
		removeObject( INDEX_BACKGROUND_BIG_SPRITE );
		isMainBigZone( p );
	}else if( getObject( INDEX_BACKGROUND_SPRITE ) ) {
		removeObject( INDEX_BACKGROUND_SPRITE );
		setPlayerImage( p );
	}*/

}

void ZoneGame::setPlayerId( uint8 i ){
	playerId = i;
}


void ZoneGame::setLocked( bool _l ){
	locked = _l;
	if( locked ){
		SAFE_DELETE( imageSpecial );
		imageSpecial = new RenderableImage( lockedZone );
		imageSpecial->setPosition(&position);
	}
}



bool ZoneGame::update( float timeElapsed ){

	return isActive();
}


PowerUp* ZoneGame::getPowerUp( void ){
	if( power )
		return power;
	return NULL;
}	
#define ADJUST_IMAGE_ALT_BIG_ZONA 25.0
void ZoneGame::setObserved( bool o ){

	observed = o;
	if( observed ){
		if( occupied && !locked ){	
			Point3f p = position;
			if( imageSpecial->isVisible() ){
				currPic = new RenderableImage( alternative );
				p.set( p.x + ( currPic->getSize()->x * 0.5 ) , p.y + ( currPic->getSize()->y * 0.5 ));
			}else if ( imageBigSpecial ) {
				currPic = new RenderableImage( alternativeBig );
				p.set( p.x +ADJUST_IMAGE_ALT_BIG_ZONA ,p.y + ADJUST_IMAGE_ALT_BIG_ZONA,0.0 );
			}
			
			currPic->setVertexSetColor( c );
			currPic->setPosition( &p );
#if DEBUG
			currPic->setName("figura corrente do quadrado\n");
#endif
		}
	}else {
		SAFE_DELETE( currPic );
	}
}
#undef ADJUST_IMAGE_ALT_BIG_ZONA


bool ZoneGame::loadImage( void ){ 
	if( !alternative ) {
		alternative = new RenderableImage("mapa_quad", RenderableImage::CreateDefaultVertexSet );
#if DEBUG
		alternative->setName("imagens estática alternativa dos 1x1\n");
#endif
	}
	if( !alternativeBig ){
		alternativeBig = new RenderableImage("mapa_quad_22", RenderableImage::CreateDefaultVertexSet );
#if DEBUG
		alternativeBig->setName("imagens estática alternativa dos 2x2\n");
#endif
	}
	
	if( !lockedZone ){
		lockedZone = new RenderableImage("zNull", RenderableImage::CreateDefaultVertexSet );
#if DEBUG
		lockedZone->setName("big Hole\n");
#endif
	}
	if( alternative && lockedZone && alternativeBig )
		return true;
#if DEBUG
	throw ConstructorException( "não foi possível carregar as imagens.." );
#else
	throw ConstructorException();
#endif
	return false;
}

void ZoneGame::removeImage( void ){
	SAFE_DELETE( alternative )
	SAFE_DELETE( alternativeBig )
	SAFE_DELETE( lockedZone )
}




#if DEBUG
void ZoneGame::renderQuads( void ){
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE );
	glEnableClientState( GL_VERTEX_ARRAY );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	glTranslatef( position.x + ( size.x * 0.5f ), position.y + ( size.y * 0.5f ), position.z );
	glScalef( size.x, size.y, size.z );
	
	
	
	Point3f border[] =	{
		Point3f( -0.5f, -0.5f ),
		Point3f(  0.5f, -0.5f ),
		Point3f(  0.5f,  0.5f ),
		Point3f( -0.5f,  0.5f )
	};
	
	glColor4ub( 255, 255, 255, 125 );
	glVertexPointer( 3, GL_FLOAT, 0, border );
	glDrawArrays( GL_LINE_LOOP, 0, 4 );
	
	glPopMatrix();
}
#endif