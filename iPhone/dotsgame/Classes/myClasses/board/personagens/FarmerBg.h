/*
 *  FarmerBg.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef FARMER_BG_H
#define FARMER_BG_H 1

#include "Board.h"


class FarmerBg : public Board {
	
public:
	FarmerBg( GameBaseInfo* g );
	
	virtual ~FarmerBg(){};
	
protected:	
	void generateBackground(  GameBaseInfo* g  ) ;
	
	void configureMarks( void ) ;
	
	void configureLinesBkgd( void );
	
	
};


#endif