/*
 *  ZoneGame.h
 *  dotGame
 *
 *  Created by Max on 10/9/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef ZONE_GAME_H
#define ZONE_GAME_H 1


/*
 classe para definir as zonas que estão sendo ocupadas ou não pelos jogadores, a primeiro momento, eu as estou considerando como uma imagem estática, e caso ela venha a ser uma animação, mudar para sprite.
 os símbolos nela serão em função do jogador ( friend? ), que determina qual imagem deve ficar aparecendo
 
 caso seja uma bigzona, ele carrega a imagem em um, que fica com imagebigspecial != null, ele carrega sempre na zona à superior esquerda, para facilitar os cálculos
 */

#include "Sprite.h"
#include "ObjectGroup.h"
#include "DotgameInfo.h"
//#include"RenderableImage.h"

enum ZoneGameState {
	ZONE_GAME_STATE_NONE = -1,
	ZONE_GAME_STATE_BLANK = 0,	
	ZONE_GAME_STATE_OCCUPIED,
	ZONE_GAME_STATE_COUPLED
}typedef ZoneGameState;

class Player;
class DotGame;
class PowerUp;
class RenderableImage;
class ZoneGame : public Object{//ObjectGroup {
	
public:
	
	ZoneGame();
	
	virtual ~ZoneGame();
	
	void associateImage( Player *p );

	void setZoneGameState ( ZoneGameState z );
	
	inline  ZoneGameState getZoneGameState ( void );
	
	bool gainedByPlayer(/* DotGame *dg*/ Player *pl);
	
	void updateImage( void );
	
	void reset( void );
	
	void setPosition( uint8 x, uint8 y );
	
	void setPlayerImage( Player *p/*[ MAX_PLAYERS  /*nPlayers* ]*/ );

	void isMainBigZone( Player *p );
	
	void notIsMainBigZone( void );
	
	bool isOccupied( void );
	
	uint8 getPlayerId( void );
	
	bool isCoupled( void );
	
	void setCoupled( bool _b );
	
	bool isLocked( void );
	
	void setLocked( bool _l );
	
	bool isObserved( void );
	
	void setObserved( bool o );
	
	virtual bool update( float timeElapsed );

	void setPowerUp( PowerUp* p );
	PowerUp* getPowerUp( void );

	virtual bool render( void );
	
	bool havePowerUp( void );
	
#if DEBUG
	void renderQuads( void );
#endif
	
	void lostByPlayer( void/*Player *p*/ );
	void setPlayerId( uint8 i );
	void changePlayer( Player *p );
	
	static bool loadImage( void );
	static void removeImage( void );	
protected:
	
	friend class LineGame;
	friend class Player;
	//friend class GenericDoll;
	friend class DotGame;
	
	uint8 playerId;
	
	ZoneGameState state;
	bool occupied, locked, observed;

	
	//indica se a zona faz parte de uma BigZone
	bool coupled;
	
	PowerUp* power;
	RenderableImage *currPic, *imageSpecial, *imageBigSpecial;

	static	RenderableImage* alternative, *alternativeBig,*lockedZone;

	Color c;

};

inline bool ZoneGame:: isOccupied( void ){

	return ( occupied );//|| locked );
}

inline  ZoneGameState ZoneGame::getZoneGameState ( void ){
	return state;
}

inline bool ZoneGame::isCoupled( void ){ 
	return coupled;
}

inline void ZoneGame::setCoupled( bool _b ){ 
	coupled = _b;
}

inline bool ZoneGame::havePowerUp( void ){
	return power !=NULL;
}

inline bool ZoneGame::isLocked( void ){
	return locked;
}
inline bool ZoneGame::isObserved( void ){
	return observed;
}
#endif
