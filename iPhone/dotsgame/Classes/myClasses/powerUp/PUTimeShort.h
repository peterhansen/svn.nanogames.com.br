/*
 *  PUTimeShort.h
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_TIME_SHORT_H
#define POWER_UP_TIME_SHORT_H 1

#include "PowerUp.h"
enum  {
	POWER_UP_TIME_SHORT_POSITIVE = 0,
	POWER_UP_TIME_SHORT_NEGATIVE
}typedef PowerUpTimeType;
//reduz/aumenta o seu tempo total em 50%-> é tb escalável facilmente...
class PUTimeShort : public PowerUp {
	
public:
	
	PUTimeShort( PowerUpListener *p );
	
	virtual ~PUTimeShort(){};
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
	virtual Renderable* getImageToBar( void );
	
	static bool loadImageToBar( void );
		
	static void unLoadImageToBar( void );
	
	virtual Sprite* getAnimation( void ){ return NULL; };

protected:
	static RenderableImage* imageBarPositivo;
	static RenderableImage* imageBarNegativo;
	PowerUpTimeType tipo;
};

#endif