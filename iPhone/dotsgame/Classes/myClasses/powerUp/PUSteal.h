/*
 *  PUSteal.h
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_STEAL_H
#define P_U_STEAL_H 1

#include "PowerUp.h"
//rouba power up do alheio... que coisa mais feia...
class PUSteal : public PowerUp {

	
public:
	
	PUSteal( PowerUpListener *p );
	
	virtual ~PUSteal( void ){};	
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
	virtual Renderable* getImageToBar( void );
	
	static bool loadImageToBar( void );
		
	static void unLoadImageToBar( void );
	virtual Sprite* getAnimation( void ){ return NULL; }

private:
//	figuras aqui!!
	static RenderableImage* imageBar;

};



#endif