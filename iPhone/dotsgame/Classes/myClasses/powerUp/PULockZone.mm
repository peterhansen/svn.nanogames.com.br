/*
 *  PULockZone.mm
 *  dotGame
 *
 *  Created by Max on 4/14/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PULockZone.h"
#include "ZoneGroup.h"
#include "DotGame.h"
RenderableImage* PULockZone::imageBar = NULL ;
Sprite* PULockZone::animation = NULL;


PULockZone::PULockZone( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_DESTROY_ZONE , p ){
	animed = true;
}

void PULockZone::powerUpDo( DotGame* dg ){
	zoneManager = dg->getZoneGroup();
	if( pPlayer->getTypeUser() == PLAYER_USER_CPU ){
		Point3f *pZone;
		sortZoneRandom(pZone );
		zoneManager->setZoneLocked(pZone, true);
	}
}

//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
void PULockZone::gainedByPlayer( Player* p ){
	pPlayer = p;
	onRelease();
}

void PULockZone::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}


Renderable* PULockZone::getImageToBar( void ){
	return imageBar;
}
	
bool PULockZone::loadImageToBar( void ){
if( !imageBar )
	imageBar = new RenderableImage("nuke");
	
	if( !animation )
		animation = new Sprite("tnulo","tnulo");
	
	if( !imageBar || !animation)
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup lockZones Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading lockZones images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PULockZone::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );
	SAFE_DELETE( animation );
}

