/*
 *  PULockLine.mm
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PULockLine.h"
#include "DotGame.h"
#include "Random.h"


RenderableImage* PULockLine::imageBar = NULL ;
Sprite* PULockLine::animation = NULL ;


PULockLine::PULockLine( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_LOCK_LINE,p){
	setPowerUpTarget( POWER_UP_TARGET_LINE );
}

void PULockLine::onRelease( void ){

	DotGame *dg;
	dg = pListener->getDotGameInstance();
	dg->setDotGameState( DOT_GAME_STATE_WAITING_POWER_UP_TARGET_LINE_INPUT );
	lineManager = dg->getLineGroup();
	Point3f *aux;
	if( pPlayer->getTypeUser() == PLAYER_USER_CPU ){
		sortLineFreeRandom( aux );
		lineManager->lockLine( /*LineGroup::getLinePCoordenatesByXY( x, y, vertical )*/ aux  );
		return;
	}
	//lineManager->lockLine( pPonto );
	
}

void PULockLine::gainedByPlayer( Player* p ){
	pPlayer = p;
	//onRelease();
	//powerUpDo( pListener->getDotGameInstance() );
}


void PULockLine::powerUpDo( DotGame* dg ){
	
	lineManager  = dg->getLineGroup();
	Point3f aux;
	sortLineFreeRandom(&aux);
	lineManager->lockLine( &aux );//pPonto );
//	dg->setDotGameState();
}

Renderable* PULockLine::getImageToBar( void ){
	return imageBar;
}
	
bool PULockLine::loadImageToBar( void ){
if( !imageBar )
	imageBar = new RenderableImage("lockLine");
	//if( !animation )
//		animation = new Sprite( LineGame::linhaBgH )
	if( !imageBar || !animation )
		
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup lockLines Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading lockLines images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PULockLine::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );
	SAFE_DELETE( animation );
}

Sprite*  PULockLine::getAnimation( void ){
	return animation;
}
