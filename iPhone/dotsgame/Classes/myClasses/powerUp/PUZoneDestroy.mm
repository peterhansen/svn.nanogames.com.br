/*
 *  PUZoneDestroy.mm
 *  dotGame
 *
 *  Created by Max on 3/12/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUZoneDestroy.h"
#include "ZoneGroup.h"
#include "Random.h"
#include "Player.h"
#include "DotGame.h"

RenderableImage* PUZoneDestroy::imageBar = NULL ;
Sprite* PUZoneDestroy::animation = NULL ;


PUZoneDestroy::PUZoneDestroy( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_DESTROY_ZONE ,p ){
	setPowerUpTarget( POWER_UP_TARGET_ZONE );
}

void PUZoneDestroy::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}
	
void PUZoneDestroy::gainedByPlayer( Player* p ){
	
	//onRelease();
//	p->destroyPowerUp();

}
/*
falta verificar as bigzonas direito, está dando problema...	 
*/
void PUZoneDestroy::powerUpDo( DotGame* dg ){
	
	//uint8 x,y,i,j;
	Point3f p;
	
	zoneManager = dg->getZoneGroup();
	lineManager = dg->getLineGroup();
	sortZoneRandom(&p);
	effectInZone( &p,lineManager, zoneManager, dg);
	//if( zoneManager->isZoneLocked( &p ) )
//		effectInZone(&p, dg );
//	zoneManager->setZoneLocked(&p, true);
}
// efeito na linha
void PUZoneDestroy::effectInLine( Point3f *p /*,LineGroup *lineManager, ZoneGroup *ZoneManager*/,  DotGame* dg){
	
	/*
	 como cada zona/quadrado só pode pertencer à uma e somente uma bigzona/quadradao, basta verificarmos quais das zonas seguintes pertence à bigzona/ quadradao, e depois desfazê-la.como funciona com uma linha, tem que se fazer com as duas zonas/quadrado adjacentes.
	 */
	
	if( !lineManager->isLineGained( p ) )
		return;
	
	Point3f p1;
	Point3f p2;
	
	uint8 x,y;
	bool horizontal;
		
	horizontal = lineManager->isLineHorizontal( p );
	dg->getLineXyByPosition( p , &y, &x);
	if( horizontal ){
		p2 = ZoneGroup::getZonePositionByXY( x , y - 1 );
	}else {
		p2 = ZoneGroup::getZonePositionByXY( x - 1, y );
	}
	
	
	p1 = ZoneGroup::getZonePositionByXY( x, y );
	
	lineManager->lineLostByPlayer( p );
	
#if DEBUG
	NSLog(@"\n agindo na linha :( %3.3f, %3.3f )%d %d %c \n ", p->x,p->y,x,y ,( lineManager->isLineHorizontal( p )?'H':'V' ));
#endif
	
	dg->unMakeBigZone( &p1 );
	dg->unMakeBigZone( &p2 );
	
}

//efeito na zona 
void PUZoneDestroy::effectInZone( Point3f *p ,/*LineGroup *lineManager, ZoneGroup *ZoneManager,*/ DotGame* dg){

	if( !zoneManager->isZoneOccupied( p ) )
		return;
	
	uint8 x,y;
	dg->getZoneXYByPosition( p , &x, &y);
	
	Point3f p1,p2,p3,p4;
#if DEBUG
	NSLog(@"agindo na zona/quadrado :( %3.3f, %3.3f ) ( %d %d), e nas suas linhas adjacentes ", p->x,p->y,x,y );
#endif

	p1 = LineGroup::getLineCoordenatesByXY( x , y ,true );
	p2 = LineGroup::getLineCoordenatesByXY( x + 1 , y ,true );
	p3 = LineGroup::getLineCoordenatesByXY( x , y ,false );
	p4 = LineGroup::getLineCoordenatesByXY( x , y + 1, false );
	
	effectInLine( &p1/*, lineManager, ZoneManager*/, dg );
	effectInLine( &p2/*, lineManager, ZoneManager*/, dg );
	effectInLine( &p3/*, lineManager, ZoneManager*/, dg );
	effectInLine( &p4/*, lineManager, ZoneManager*/, dg );
}

Renderable* PUZoneDestroy::getImageToBar( void ){
	return imageBar;
}
	
bool PUZoneDestroy::loadImageToBar( void ){
	if( !imageBar )
		imageBar = new RenderableImage("nuke");
	
	if( !animation )
		animation = new Sprite("tnulo","tnulo");
	if( !imageBar || !animation )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup ZoneDestroy Carregadas");
#endif
	return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading ZoneDestroy images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUZoneDestroy::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );
	SAFE_DELETE( animation );
}

Sprite* PUZoneDestroy::getAnimation( void ){
	return animation;
}
