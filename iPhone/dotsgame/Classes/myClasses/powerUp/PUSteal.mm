/*
 *  PUSteal.mm
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUSteal.h"
#include "Player.h"
#include "DotGame.h"
RenderableImage* PUSteal::imageBar = NULL ;

PUSteal::PUSteal( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_STEAL ,p ){


	
}


void  PUSteal::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );


}
	
void PUSteal::gainedByPlayer( Player* p ){
	//onRelease();
}
	
void PUSteal::powerUpDo( DotGame* dg ){
	PowerUp *pow;
	Player *p, *p2;
	p = dg->getCurrPlayer();
	p2 = dg->getNextPLayer();
	pow = p2->getPowerUp();
	p->setPowerUp( pow );
	p2->destroyPowerUp();
}



Renderable* PUSteal::getImageToBar( void ){
	return imageBar;
}
	
bool PUSteal::loadImageToBar( void ){
if( !imageBar )
	imageBar = new RenderableImage("life", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	if( !imageBar )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pupsteal Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading Pupsteal images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUSteal::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );

}

