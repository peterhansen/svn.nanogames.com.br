/*
 *  PULockZone.h
 *  dotGame
 *
 *  Created by Max on 4/14/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef PU_LOCK_ZONE_H
#define PU_LOCK_ZONE_H 1

#include"PowerUp.h"
//deixa ele "travado" com um buraco...
class PULockZone : public PowerUp {
	
public:
	PULockZone( PowerUpListener *p );
	
	virtual ~PULockZone( void ){};
	
	//o código do power up agindo
	virtual void powerUpDo( DotGame* dg );
	
	//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
	virtual void gainedByPlayer( Player* p );
	
	virtual void onRelease( void );
	
	virtual Renderable* getImageToBar( void );
	
	static bool loadImageToBar( void );
		
	static void unLoadImageToBar( void );
	
private:
	static RenderableImage* imageBar;
	static	Sprite *animation;
	
};

#endif