/*
 *  PUReverse.h
 *  dotGame
 *
 *  Created by Max on 4/14/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_REVERSE_H
#define P_U_REVERSE_H 1

#include "PowerUp.h"
//troca todos os elementos de um jogador pelo outro
class PUReverse : public PowerUp {
public:
	PUReverse( PowerUpListener *p );
	
	virtual ~PUReverse(){};	
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
	virtual Renderable* getImageToBar( void );
	
	static bool loadImageToBar( void );
		
	static void unLoadImageToBar( void );
	virtual Sprite* getAnimation( void ){ return NULL; };

	
private:
	static RenderableImage* imageBar;

};


#endif