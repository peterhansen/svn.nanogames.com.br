/*
 *  PUDestroy.mm
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUDestroy.h"
#include "DotGame.h"
#include "Player.h"

RenderableImage* PUDestroy::imageBar = NULL ;


PUDestroy::PUDestroy( PowerUpListener* p ):PowerUp( POWER_UP_TYPE_DESTROY, p ){}


//o código do power up agindo
void PUDestroy::powerUpDo( DotGame* dg ){
#if DEBUG
	NSLog(@"destruindo o seu próprio power up ");
#endif
	
	//devido às mudanças, adaptei este powerUp para fazer com que ele perca o seu próprio powerUp, ficando com nenhum, mantive o código anterior aqui para caso seja feito algum power up deste tipo, já tem o código pronto...
	//Player *p;
	//como estou fazendo pensando para 2 jogadores, ele está destruindo o PUp do próximo jogador, caso tenha que se mudar para fazer de tal maneira que se tenha destruir o resto, mudar para getPlayerById, e entrar com o id do jogador.
	//p = dg->getNextPLayer();
	//p->destroyPowerUp();
}

//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
void PUDestroy::gainedByPlayer( Player* p ){
	
	onRelease();
}

void PUDestroy::onRelease( void ){

	powerUpDo( pListener->getDotGameInstance() );

}


Renderable* PUDestroy::getImageToBar( void ){
	return imageBar;
}
	
bool PUDestroy::loadImageToBar( void ){
if( !imageBar )
	imageBar = new RenderableImage("life");
	if( !imageBar )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pupdestroy Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading Pupdestroy images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUDestroy::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );

}
