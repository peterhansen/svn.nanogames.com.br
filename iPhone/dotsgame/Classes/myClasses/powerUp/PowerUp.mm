/*
 *  PowerUp.mm
 *  dotGame
 *
 *  Created by Max on 2/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUp.h"
#include "ZoneGame.h"
#include "Player.h"
#include "Random.h"
#include "ZoneGroup.h"
#include "LineGroup.h"
#include "PowerUpGroup.h"


RenderableImage* PowerUp::hidden = NULL;
RenderableImage* PowerUp::alternative = NULL;
Sprite* PowerUp::shine = NULL ;


//#define MAX_OBJECTS_INSIDE_POWER_UP 2

PowerUp::PowerUp( PowerUpType p , PowerUpListener *listener ):
Sprite( shine/*"PU_shine" ,"PU_shine"*/  ),
//animation( NULL ),
//ObjectGroup( MAX_OBJECTS_INSIDE_POWER_UP ),
pListener( listener ),
currPic( NULL ),
pwrUpType( p ),
disarmed( false ),
lineManager( NULL ),
zoneManager( NULL ),
observing( false ),
//state( POWER_UP_STATE_NONE ),
zoneOf( NULL ){ 
	setVisible( true );
	setAnimSequence( 0 ,true, true );
	
#if DEBUG
//	currPic.setName("imagem power up alternativa\n");
#endif
}
/*
 destrutor
 */
PowerUp::~PowerUp( void ){
	pListener = NULL;
	zoneOf = NULL;
	lineManager = NULL;
	zoneManager = NULL;
	pPlayer = NULL;
	pGroup = NULL;
	SAFE_DELETE( currPic );
#if DEBUG
	NSLog(@" destruindo PowerUp ");
#endif
}
/* ======================================================================================================
 
 onrelease->fç chamada qdo o power up é lançado, chamando o seu listener para que ele faça algo de acordo com o tipo de power up.
 
 ======================================================================================================*/
/*void PowerUp::onRelease( void ){
	setVisible( false );
#if DEBUG
	NSLog(@" pwrup posicao x = %.2f,y = %.2f,z = %.2f\n ", position.x, position.y, position.z);
#endif
	//colocar  aqui código de animação ou qq coisa assim que aconteça neste momento;
	pListener->PowerUpEffect( this );
}*/
/* ======================================================================================================
 
 onrelease->fç chamada para associar um power up à ua zona do tabuleiro
 
 ======================================================================================================*/
void PowerUp::setZone( ZoneGame *z ){
	zoneOf = z;
	//Point3f p;
//	p = *z->getPosition();
//	p *=0.5;
	//setAnimSequence( 0,true );

	//position = p;
}
/* ======================================================================================================
 
 setListener->fç chamada para escolher o seu listener
 
 ======================================================================================================*/
void PowerUp::setListener( PowerUpListener *p ){
	pListener = p;
}
/* ======================================================================================================
 
 setPosition->usada para posicionar o powerUp
 
 ======================================================================================================*/
void PowerUp::setPosition(uint8 x, uint8 y ){
	Point3f p;
	p = ZoneGroup::getZonePositionByXY( x, y );
	position.set( p );
	//currPic.setPosition( &p );
}


//diz o local onde o power up vai agir
void PowerUp::InputPosition( Point3f *p ){

	pPonto = p;
}
bool PowerUp::render(){
	if( !isVisible() )
		return false;
	bool ret = false;
	if( !observing ){
		//
		//glTranslatef( -position.x, -position.y, -position.z);
		ret |= Sprite::render();
	}else {
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		currPic->render();
		glPopMatrix();
		ret = true;
	}
	return ret;
}

void PowerUp::setPowerUpTarget(PowerUpTarget t ){
	target = t;
}

void PowerUp::sortLineRandom( Point3f* p ){
	uint8 x,y;
	bool horizontal;
	Point3f pLinha;
	for(;;){
		horizontal =  Random::GetInt( 0 , 1) == 0 ;
		x = Random::GetInt( 0, lineManager->getTotalLinesH() - 1);	
		y = Random::GetInt( 0, lineManager->getTotalLinesV() - 1);
		pLinha =  LineGroup::getLineCoordenatesByXY( x, y, horizontal);
		if ( lineManager->isLineGained( &pLinha ) && lineManager->isLineLocked( &pLinha ) )
			break;	
	}
	p->set( pLinha.x,pLinha.y,pLinha.z );
//	ret = &pLinha;
	//return ret;
}

void PowerUp::sortZoneRandom( Point3f* p ){
	uint8 x,y;
	Point3f pZona;
	for(;;){
		x = Random::GetInt( 0, zoneManager->getnZonesH() - 1);	
		y = Random::GetInt( 0, zoneManager->getnZonesV() - 1);
		pZona =  ZoneGroup::getZonePositionByXY( x, y );
		if ( zoneManager->isZoneOccupied( &pZona ) && !zoneManager->isZoneLocked( &pZona )){
			
			break;	
		}
	}
	p->set( pZona.x,pZona.y,pZona.z );
	//ret = &pZona;
	//return ret;
}

void PowerUp::sortLineFreeRandom( Point3f* p ){
	uint8 x,y;
	bool horizontal;
	Point3f pLinha;
	for(;;){
		horizontal =  Random::GetInt( 0 , 1) == 0 ;
		x = Random::GetInt( 0, lineManager->getTotalLinesH() - 1);	
		y = Random::GetInt( 0, lineManager->getTotalLinesV() - 1);
		pLinha =  LineGroup::getLineCoordenatesByXY( x, y, horizontal);

		if ( !lineManager->isLineGained( &pLinha) && !lineManager->isLineLocked( &pLinha ) ){
			
			break;	
		}
	}
	p->set( pLinha.x,pLinha.y,pLinha.z );
//	ret = pLinha;
	//return ret;
}

void PowerUp::sortZoneFreeRandom( Point3f* z ){
uint8 x,y;
	Point3f pZona;//, *ret;
	for(;;){
		x = Random::GetInt( 0, zoneManager->getnZonesH() - 1);	
		y = Random::GetInt( 0, zoneManager->getnZonesV() - 1);
		pZona =  ZoneGroup::getZonePositionByXY( x, y );
		if ( !zoneManager->isZoneOccupied( &pZona ) && !zoneManager->isZoneLocked( &pZona )){
			
			break;	
		}
	}
	z->set( pZona.x,pZona.y,pZona.z );

	//*ret = pZona;
	//return ret;


}

bool PowerUp::update( float timeElapsed ){
	if( !isActive() )
		return false;
	return Sprite::update( timeElapsed );
}

void PowerUp::setObserved( bool b ){
	observing = b;
	if( observing ){
		currPic = new RenderableImage( alternative );
		currPic->setPosition( &position );
#if DEBUG
		currPic->setName("imagem PUp mapa\n");
#endif
	}else {
		SAFE_DELETE( currPic );
	}

}

Renderable* PowerUp::getHiddenImage( void ){
	return hidden;
}

bool PowerUp::loadImages( void ){
	//acertar!!
	if( !alternative )
		alternative = new RenderableImage( "puGen" );
	if( !hidden )
		hidden = new RenderableImage( "presente" );
	if( !shine )
	shine = new Sprite("PU_shine" ,"PU_shine");
	if( alternative && hidden && shine){
#if DEBUG
		alternative->setName("imagem tabuleiro pUp Generico\n");
		hidden->setName("imagem pUp escondido\n");
		shine->setName("imagem pUp taburleiro oficial\n");
		NSLog(@"imagens PowerUp generic loaded!");
#endif
		return true;
		
	}else{
#if DEBUG
		throw ConstructorException("fail while loading PowerUp images");
#else
		throw ConstructorException();
#endif 	
		return false;	
	}
		
}
	
void PowerUp::removeImages( void ){
	SAFE_DELETE( alternative );
	SAFE_DELETE( hidden );
	SAFE_DELETE( shine );
}

void PowerUp::setPowerUpGroup( PowerUpGroup *p ){
	pGroup = p;
}