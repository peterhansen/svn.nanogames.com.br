/*
 *  PUMoveRandom.mm
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUMoveRandom.h"
#include "DotGame.h"
#include "Marks.h"
#include "Random.h"

#define MIN_BONUS_LINES 3//10

RenderableImage* PUMoveRandom::imageBar = NULL ;

PUMoveRandom::PUMoveRandom( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_MOVE_RANDOM, p ),
nBonusLines( MIN_BONUS_LINES ){}

void PUMoveRandom::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}

void PUMoveRandom::gainedByPlayer( Player* p ){

	//onRelease();
	
}

//é eu sei está horrível, simplesmente copiei e colei os códigos da cena, por isso eu melembro de colocar o próprio tabuleiro com o conjunto das linhas, quadrados/zonas, cercas para ser responsável pela renderização das budegas, mas no momento a pressa fala mais alto.
void PUMoveRandom::powerUpDo( DotGame* dg ){
	lineManager = dg->getLineGroup();
	zoneManager = dg->getZoneGroup();
	Point3f p;
	for ( uint8 i = 0 ; i <= nBonusLines ; i++ ) {
		sortLineFreeRandom( &p );		
		lineManager->selectLine( &p , dg->getCurrPlayer() );
		if( !lineManager->isLineHorizontal( &p ) )
			verifyZonesVertical( &p );
		else
			verifyZonesHorizontal( &p );
	}

}

Renderable* PUMoveRandom::getImageToBar( void ){
	return imageBar;
}

bool PUMoveRandom::loadImageToBar( void ){
	if( !imageBar )
		imageBar = new RenderableImage("MvRandom");
	if( !imageBar )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup MvRandom Carregadas");
#endif
	return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading MvRandom images");
#else
	throw ConstructorException();
#endif 	
	return false;
}

void PUMoveRandom::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );
	
}

bool PUMoveRandom::verifyZonesHorizontal( Point3f *p ){

	uint8 x,y;
	bool ret = false ;
	
	//LineGroup::getLineXYByPosition( p, false, &x, &y);
	pListener->getDotGameInstance()->getLineXyByPosition( p, &x, &y );
	Point3f p1;
	p1 = ZoneGroup::getZonePositionByXY( x, y );
#if DEBUG
	NSLog(@"zona a ser verificada %d  %d ",x ,y);
#endif	
	if( verifyLines(  x, y ) ){
		ret |= true;
	}

		
	if( y >= 1 ){
		p1 = ZoneGroup::getZonePositionByXY( x , y - 1 );
#if DEBUG
		NSLog(@"zona a ser verificada %d  %d ",x ,y- 1);
#endif
		if( verifyLines( /*&p1*/x, y - 1 ) ){
			ret |= true;
		}
	}
	return ret;
}

bool PUMoveRandom::verifyZonesVertical( Point3f *p ){
	uint8 x,y;
	bool ret = false ;
	Point3f p1;
	//LineGroup::getLineXYByPosition( p, true, &x, &y);
	pListener->getDotGameInstance()->getLineXyByPosition( p, &x, &y );
	
	p1 = ZoneGroup::getZonePositionByXY( x, y );
#if DEBUG
	//NSLog(@"zona a ser verificada %d  %d ",x,y);
#endif
	if( verifyLines( /*&p1*/x, y ) ){
		ret = true;
	}

	if( x >= 1 ){
		p1 = ZoneGroup::getZonePositionByXY( x - 1, y );
#if DEBUG
	//	NSLog(@"zona a ser verificada %d  %d ",x - 1,y);
#endif
		if( verifyLines( /*&p1*/x - 1, y ) ){
			ret = true;
		}
	}
	return ret;
}

bool PUMoveRandom::verifyLines( uint8 x, uint8 y ){
Point3f p1, p2, p3, p4,pZone;
//	uint8 x,y;
	bool ret = false;
	
	pZone = ZoneGroup::getZonePositionByXY( x, y); //ZoneGroup::getZoneXYByPosition( pZone, &x, &y);
#if DEBUG
	//NSLog(@" verificando zona %d  %d ",x,y);
#endif
	p1 = LineGroup::getLineCoordenatesByXY( x, y, true );
	p2 = LineGroup::getLineCoordenatesByXY( x + 1, y, true );
	p3 = LineGroup::getLineCoordenatesByXY( x, y, false );
	p4 = LineGroup::getLineCoordenatesByXY( x , y + 1, false );
	
	if( lineManager->isLineGained( &p1 )  &&
	   lineManager->isLineGained( &p2 ) &&
	   lineManager->isLineGained( &p3 ) &&
	   lineManager->isLineGained( &p4 ) ){
		ret |= zoneManager->zoneGainedByPlayer( /*&p4*/&pZone, pListener->getDotGameInstance()->getCurrPlayer() );
				if( !makeBigZone( &pZone ) ){
			if( ret ){	}
		}


#if DEBUG
	//	NSLog(@"lines selected %d ",linesSelected);
	//	NSLog(@"combo = %d ",combo);
#endif

	}
	return ret;
}

bool PUMoveRandom::makeBigZone( Point3f *p ){
#if DEBUG
	//NSLog(@" verificando big zonas ");
#endif
	uint8 x,y;
	//ZoneGroup::getZoneXYByPosition( p, &x, &y);
	pListener->getDotGameInstance()->getZoneXYByPosition( p, &x , &y );
	bool ret = false;
	
	Point3f p2,p3,p4,markPos;
	
	//verificando zona/quadrado superior à esquerda
	p2 = ZoneGroup::getZonePositionByXY( x, y - 1 );
	p3 = ZoneGroup::getZonePositionByXY( x - 1, y - 1 );
	p4 = ZoneGroup::getZonePositionByXY( x - 1, y );
	
	if( zoneManager->isZoneOccupied( p ) &&
		zoneManager->isZoneOccupied( &p2 ) &&
		zoneManager->isZoneOccupied( &p3 ) &&
		zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		    !zoneManager->isZoneCoupled( &p2 ) &&
		    !zoneManager->isZoneCoupled( &p3 ) &&
		    !zoneManager->isZoneCoupled( &p4 ) )
			if(   ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
				
				(zoneManager->zoneGetPlayerId( &p3 ) ==
				 zoneManager->zoneGetPlayerId( &p4 ) ) &&
				
				(zoneManager->zoneGetPlayerId( p ) ==
				 zoneManager->zoneGetPlayerId( &p4 ) )){
				
			markPos.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
						( y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ));
			//	pListener->getDotGameInstance()->getBoard()->getObject(INDEX_BOARD_MARKS )
				static_cast<Marks*> ( pListener->getDotGameInstance()->getBoard()->getObject(INDEX_BOARD_MARKS ) )->disappearMark( &markPos );
				zoneManager->setZoneCoupled( p , true);
				zoneManager->setZoneCoupled( &p2 , true);
				zoneManager->setZoneCoupled( &p3 , true);
				zoneManager->setZoneCoupled( &p4 , true);
				
				zoneManager->setZoneVisible( &p2 , false );
				zoneManager->setZoneVisible( p , false );
				zoneManager->setZoneVisible( &p4 , false );
				zoneManager->BigZoneGainedByPLayer( &p3, pListener->getDotGameInstance()->getCurrPlayer());
								
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y - 1, true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y , true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y, false), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x - 1 , y, false), false );

#if DEBUG
//			NSLog(@"aparecer animação 2x2 aqui");
//			NSLog(@"bigzona feita!");
#endif
				ret= true;
				
				
			}
	
	//zona/quadrado inferior à esquerda
	p2 = ZoneGroup::getZonePositionByXY( x, y + 1 );
	p3 = ZoneGroup::getZonePositionByXY( x - 1, y + 1 );
	p4 = ZoneGroup::getZonePositionByXY( x - 1, y );
	
	if( zoneManager->isZoneOccupied( p ) &&
	   zoneManager->isZoneOccupied( &p2 ) &&
	   zoneManager->isZoneOccupied( &p3 ) &&
	   zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		  !zoneManager->isZoneCoupled( &p2 ) &&
		   !zoneManager->isZoneCoupled( &p3 ) &&
		   !zoneManager->isZoneCoupled( &p4 ) )
			if(   ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) )
			{
				
				markPos.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							( ( y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> (pListener->getDotGameInstance()->getBoard()->getObject(INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				//colocar em 1 fç à parte?
				zoneManager->setZoneCoupled( p , true);
				zoneManager->setZoneCoupled( &p2 , true);
				zoneManager->setZoneCoupled( &p3 , true);
				zoneManager->setZoneCoupled( &p4 , true);
				
				zoneManager->setZoneVisible( &p2 , false );
				zoneManager->setZoneVisible( &p3 , false );
				zoneManager->setZoneVisible( p , false );
				
				zoneManager->BigZoneGainedByPLayer( &p4, pListener->getDotGameInstance()->getCurrPlayer());
				
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y + 1, true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y , true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y + 1, false), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x - 1 , y + 1, false), false );
				
				
#if DEBUG
		//		NSLog(@"aparecer animação 2x2 aqui");
		//		NSLog(@"bigzona feita!");
#endif
				ret = true;
			}
	
	
//zona supeiror à direita
	p2 = ZoneGroup::getZonePositionByXY( x, y - 1 );
	p3 = ZoneGroup::getZonePositionByXY( x + 1, y - 1 );
	p4 = ZoneGroup::getZonePositionByXY( x + 1, y );

	if( zoneManager->isZoneOccupied( p ) &&
	   zoneManager->isZoneOccupied( &p2 ) &&
	   zoneManager->isZoneOccupied( &p3 ) &&
	   zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		   !zoneManager->isZoneCoupled( &p2 ) &&
		   !zoneManager->isZoneCoupled( &p3 ) &&
		   !zoneManager->isZoneCoupled( &p4 ) )
			if(   ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) )
			   ){
				
				markPos.set( ( x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							( y * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> (pListener->getDotGameInstance()->getBoard()->getObject(INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				
				zoneManager->setZoneCoupled( p , true);
				zoneManager->setZoneCoupled( &p2 , true);
				zoneManager->setZoneCoupled( &p3 , true);
				zoneManager->setZoneCoupled( &p4 , true);
				
				zoneManager->setZoneVisible( &p4 , false );
				zoneManager->setZoneVisible( &p3 , false );
				zoneManager->setZoneVisible( p , false );
				zoneManager->BigZoneGainedByPLayer( &p2, pListener->getDotGameInstance()->getCurrPlayer());
				
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x + 1 , y - 1, true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x + 1, y , true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y , false), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x + 1 , y, false), false );
			#if DEBUG
			//	NSLog(@"aparecer animação 2x2 aqui");
			//	NSLog(@"bigzona feita!");
#endif

				ret = true;
			}
	
	
//zona inferior à direita	

	p2 = ZoneGroup::getZonePositionByXY( x, y + 1 );
	p3 = ZoneGroup::getZonePositionByXY( x + 1, y + 1 );
	p4 = ZoneGroup::getZonePositionByXY( x + 1, y );

	if( zoneManager->isZoneOccupied( p ) &&
		zoneManager->isZoneOccupied( &p2 ) &&
		zoneManager->isZoneOccupied( &p3 ) &&
		zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		    !zoneManager->isZoneCoupled( &p2 ) &&
		    !zoneManager->isZoneCoupled( &p3 ) &&
		    !zoneManager->isZoneCoupled( &p4 ) )
			if(  ( zoneManager->zoneGetPlayerId( p ) ==
			    zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
				
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				  zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) )
				){
						
				markPos.set( ( x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),( ( y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( pListener->getDotGameInstance()->getBoard()->getObject(INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				
						zoneManager->setZoneCoupled( p , true);
						zoneManager->setZoneCoupled( &p2 , true);
						zoneManager->setZoneCoupled( &p3 , true);
						zoneManager->setZoneCoupled( &p4 , true);

				zoneManager->setZoneVisible( &p2 , false );
				zoneManager->setZoneVisible( &p3 , false );
				zoneManager->setZoneVisible( &p4 , false );
				zoneManager->BigZoneGainedByPLayer( p, pListener->getDotGameInstance()->getCurrPlayer());
				
				ret = true;
				
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x + 1 , y + 1, true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x + 1 , y , true), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x , y + 1, false), false );
				lineManager->setLineVisible( LineGroup::getLinePCoordenatesByXY( x + 1 , y + 1, false), false );
#if DEBUG
			//	NSLog(@"aparecer animação 2x2 aqui");
			//	NSLog(@"bigzona feita!");
#endif						
		
					}
	
	return ret;
}

//Sprite*  PUMoveRandom::getAnimation( void ){ return NULL;}
