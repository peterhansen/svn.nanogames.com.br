/*
 *  PULockLine.h
 *  dotGame
 *
 *  Created by Max on 3/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef PU_LOCK_LINE_H
#define PU_LOCK_LINE_H 1


#include "PowerUp.h"
//trava a linha( muro de preda, eu disse PREEEEEEDA! )
class PULockLine : public PowerUp {
public:
	
	PULockLine( PowerUpListener *p );

	virtual ~PULockLine( void ){};
	
	virtual void onRelease( void );

	//virtual void cpuDo( Player* p );
	
	virtual Renderable* getImageToBar( void );
	
	static bool loadImageToBar( void );
		
	static void unLoadImageToBar( void );
	
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	virtual Sprite* getAnimation( void );

private:
	static RenderableImage* imageBar;
	static Sprite* animation;
	//colocar figuras aqui e os seus getters and setters logo acima...
};


#endif