/*
 *  PUMoveRandom.h
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef P_U_MOVE_RANDOM_H
#define P_U_MOVE_RANDOM_H 1

#include"PowerUp.h"

/*
 marca linhas aleatoriamente
 */
class PUMoveRandom : public PowerUp{

	public:
	
	PUMoveRandom( PowerUpListener *p );

	virtual ~PUMoveRandom( void ){};
	
	virtual void onRelease( void );
	
	virtual void gainedByPlayer( Player* p );
	
	virtual	void updatePowerUp( void ){};
	
	virtual	void powerUpDo( DotGame* dg );
	
	//virtual void cpuDo( Player* p ){};

	virtual Renderable* getImageToBar( void );
	
	static bool loadImageToBar( void );
		
	static void unLoadImageToBar( void );

	virtual Sprite* getAnimation( void ){ return NULL; };

	private:
	//está horrível, eu sei, só botei estas fçs aqui por pura pressa mesmo...
	bool verifyZonesHorizontal( Point3f *p );
		
	bool verifyZonesVertical( Point3f *p );
	
	bool verifyLines( uint8 x, uint8 y );

	bool makeBigZone( Point3f *p );

	uint8 nBonusLines;
	static RenderableImage* imageBar;

};


#endif