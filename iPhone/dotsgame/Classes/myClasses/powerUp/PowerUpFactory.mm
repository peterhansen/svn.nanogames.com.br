/*
 *  PowerUpFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/18/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUpFactory.h"
#include "ObjcMacros.h"
#include "PUComboBreak.h"
#include "PUBomb.h"
#include "PUZoneDestroy.h"
#include "PUSteal.h"
#include "PUDestroy.h"
#include "PULockLine.h"
#include "PUMoveBonus.h"
#include "PUTimeShort.h"
#include "PUMoveRandom.h"
#include "PUReverse.h"
#include"Exceptions.h"
#include "FreeKickAppDelegate.h"


PowerUpFactory* PowerUpFactory::pSingleton = NULL;

bool PowerUpFactory::Create( void ){
	if( !pSingleton )
		pSingleton = new PowerUpFactory();
	
	return pSingleton!= NULL;
}

void PowerUpFactory::Destroy( void ){
	SAFE_DELETE( pSingleton );
}
/*
 retorna um novo powerUp, caso seja de um tipo desconhecido, retorna NULL!
 */
PowerUp* PowerUpFactory::getNewPowerUp( PowerUpType t , PowerUpListener/*DotGame*/ *dg){
	if( !pSingleton )
		Create();

	PowerUp *p = NULL;
	bool ret = false;
	switch ( t ) {
		case POWER_UP_TYPE_COMBO_BREAK:
			//confere
			ret|=	PUComboBreak::loadImageToBar();
			p  = new PUComboBreak( dg );			
			break;
			
		case POWER_UP_TYPE_BOMB:
			ret|=	PUBomb::loadImageToBar();
			p = new PUBomb( dg );
			break;
			
		case POWER_UP_TYPE_MOVE_BONUS:
			//confere
			ret|=	PUMoveBonus::loadImageToBar();
			p = new PUMoveBonus( dg );
			break;
			
		case POWER_UP_TYPE_MOVE_RANDOM:
			//confere
			ret|=	PUMoveRandom::loadImageToBar();
			p = new PUMoveRandom( dg );
			break;
			
		case POWER_UP_TYPE_TIME_SHORT:
			//confere
			ret|=	PUTimeShort::loadImageToBar();
			p = new PUTimeShort( dg );
			break;
			
		case POWER_UP_TYPE_DESTROY_ZONE:
			ret|=	PUZoneDestroy::loadImageToBar();
			p = new PUZoneDestroy( dg );
			break;
			
		case POWER_UP_TYPE_LOCK_LINE:
			//confere
			ret|=	PULockLine::loadImageToBar();
			p = new PULockLine( dg );
			break;
			
		case POWER_UP_TYPE_STEAL:
			//confere
			ret|=	PUSteal::loadImageToBar();
			p = new PUSteal( dg );
			break;
			
		case POWER_UP_TYPE_DESTROY:
			//confere
			ret|=	PUDestroy::loadImageToBar();
			p = new PUDestroy( dg );
			break;
		case POWER_UP_TYPE_REVERSE_BOARD:
			ret|=	PUReverse::loadImageToBar();
			p = new PUReverse( dg );
			break;
	}
	
	if( !ret || !p ){
	if( !ret )
#if DEBUG
		NSLog(@"encerrando programa por causa de imagem não carregada\n");
#endif
	}
		
	return p;
	
}

PowerUpFactory::PowerUpFactory():nPowerTypes( N_POWER_UPS ){
#if DEBUG
	NSLog(@"powerUpFactory criada!");
#endif
}
	
PowerUpFactory::~PowerUpFactory(){
#if DEBUG
	NSLog(@"powerUpFactory destruída");
#endif
	
}


bool PowerUpFactory::loadAllImages( void ){
	//carregar imagens aqui!!
	bool ret = false;
	ret |=	PowerUp::loadImages();
	
	if( ret ){
#if DEBUG
		NSLog(@"imagens PowerUp carregadas");
#endif
	}else{
#if DEBUG
		throw ConstructorException("fail while loading PU images");
#else
		throw ConstructorException();
#endif 	
		[( FreeKickAppDelegate* )APP_DELEGATE quit: ERROR_ALLOCATING_DATA];
		
		return false;
	}
	return ret;
}
void PowerUpFactory::unLoadAllImages( void ){
#if DEBUG
	printf("\n desalocando imagens dos power ups!\n");
#endif
	PUComboBreak::unLoadImageToBar();
	PUBomb::unLoadImageToBar();
	PUMoveBonus::unLoadImageToBar();
	PUMoveRandom::unLoadImageToBar();
	PUTimeShort::unLoadImageToBar();
	PUZoneDestroy::unLoadImageToBar();
	PULockLine::unLoadImageToBar();
	PUSteal::unLoadImageToBar();
	PUDestroy::unLoadImageToBar();
	PUReverse::unLoadImageToBar();
	PowerUp::removeImages();
}

