/*
 *  PUBomb.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUBomb.h"
#include "Random.h"
#include "DotGame.h"

#define MINIMAL_TIME_TO_EXPLODE 0
#define MAXIMAL_TIME_TO_EXPLODE 6//5

RenderableImage* PUBomb::imageBarSmall = NULL;
RenderableImage* PUBomb::imageBarMedium = NULL;

PUBomb::PUBomb( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_BOMB,p),
timeToExplode( -1 ),timeOver( false )
{
	if( !buildBomb() ){
#if DEBUG
		NSLog(@"power up não criado");
#endif
	}
	
}

void PUBomb::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
//	pListener->PowerUpEffect( this );
}


void PUBomb::gainedByPlayer( Player* p ){
#if DEBUG
	NSLog(@"bomba ganha pelo jogador");
#endif
	pPlayer = p;
	timeToExplode = Random::GetInt( MINIMAL_TIME_TO_EXPLODE, MAXIMAL_TIME_TO_EXPLODE );
	//onRelease();
}

bool PUBomb::buildBomb( void ){
#if DEBUG
	setName("pwr up bomba");
#endif
	//timeToExplode = Random::GetInt( MINIMAL_TIME_TO_EXPLODE, MAXIMAL_TIME_TO_EXPLODE );
	sizeBomb = BOMB_SIZE_MEDIUM;
	/*switch ( Random::GetInt( 0, 2 ) ) {
		case 0:
			sizeBomb = BOMB_SIZE_SMALL;
			break;
		case 1:
			sizeBomb = BOMB_SIZE_MEDIUM;
			break;
		case 2:
			sizeBomb =	BOMB_SIZE_BIG;		
			break;
			
	}*/
	return true;
	
}

//código para a cpu agir!!
//void PUBomb::cpuDo( Player* p ){
//
//
//
//};

void PUBomb::powerUpDo( DotGame* dg ){
	if( pPlayer->getTypeUser() == PLAYER_USER_CPU ){
		//cpuDo( pPlayer );
		return;
	}
	Point3f p1,p2;
	lineManager = dg->getLineGroup();
	zoneManager = dg->getZoneGroup();


	switch ( sizeBomb ) {
			
			//detona uma linha!
		case BOMB_SIZE_SMALL :
#if DEBUG
			NSLog(@"bomba pequena agindo");
#endif
			effectInLine( pPonto, lineManager, zoneManager, dg);
			break;
			
		//detona um quadrado/zona
		case BOMB_SIZE_MEDIUM :
#if DEBUG
			NSLog(@"bomba media agindo");
#endif
			effectInZone( pPonto ,lineManager, zoneManager, dg);
			break;
			//detona 9 quadrados/zonas!!!
	//	case BOMB_SIZE_BIG :
//#if DEBUG
//			NSLog(@"bomba grande agindo");
//#endif
//			uint8 x,y;
//			zoneManager->getZoneXYByPosition( pPonto, &x ,&y );
//			
//			//zona/quad na linha de cima			
//			effectInZone( ZoneGroup::getZonePPositionByXY( x, y - 1 ) , lineManager ,zoneManager, dg);
//			effectInZone( ZoneGroup::getZonePPositionByXY( x - 1, y - 1 ) , lineManager ,zoneManager, dg);
//			effectInZone( ZoneGroup::getZonePPositionByXY( x + 1, y - 1 ) , lineManager ,zoneManager, dg);
//			
//			//zona/quad na mesma linha
//			effectInZone( ZoneGroup::getZonePPositionByXY( x, y ) , lineManager ,zoneManager, dg);
//			effectInZone( ZoneGroup::getZonePPositionByXY( x - 1, y ) , lineManager ,zoneManager, dg);
//			effectInZone( ZoneGroup::getZonePPositionByXY( x + 1, y ) , lineManager ,zoneManager, dg);
//			
//			//zona/quad na linha de baixo
//			effectInZone( ZoneGroup::getZonePPositionByXY( x, y + 1 ) , lineManager ,zoneManager , dg);
//			effectInZone( ZoneGroup::getZonePPositionByXY( x - 1, y + 1 ) , lineManager ,zoneManager , dg);
//			effectInZone( ZoneGroup::getZonePPositionByXY( x + 1, y + 1 ) , lineManager ,zoneManager , dg);
//			
//			break;
	}
	
}

void PUBomb::updatePowerUp( void ){
	if( timeToExplode <= 0 ){
		timeOver = true;
		onRelease();
		return;
	}else {
		timeToExplode--;
	}
}

// efeito na linha
void PUBomb::effectInLine( Point3f *p ,LineGroup *lineManager, ZoneGroup *ZoneManager,  DotGame* dg){
	
	/*
	 como cada zona/quadrado só pode pertencer à uma e somente uma bigzona/quadradao, basta verificarmos quais das zonas seguintes pertence à bigzona/ quadradao, e depois desfazê-la.como funciona com uma linha, tem que se fazer com as duas zonas/quadrado adjacentes.
	 */

	uint8 x,y;
	bool horizontal;
	Point3f *pLinha;
	if( pPlayer->getTypeUser()== PLAYER_USER_CPU )
		for(;;){
			pLinha = sortLineRandom();
			if( lineManager->getLinePlayerId( pLinha ) != pPlayer->getId() && lineManager->getLinePlayerId( pLinha ) != 255  )
				break;
		}
	Point3f p1;
	Point3f p2;
#if DEBUG
	NSLog(@"agindo na linha :( %3.3f, %3.3f ) ", pLinha->x, pLinha->y );
#endif	
	horizontal = lineManager->isLineHorizontal( pLinha );
	dg->getLineXyByPosition(pLinha, &x, &y);//lineManager->getLineXYByPosition( pLinha, !horizontal, &x, &y);
	
	if( horizontal ){
		p2 = ZoneGroup::getZonePositionByXY( x , y - 1 );
	}else {
		p2 = ZoneGroup::getZonePositionByXY( x - 1, y );
	}
	
	
	p1 = ZoneGroup::getZonePositionByXY( x, y );
	dg->unMakeBigZone( &p1 );
	dg->unMakeBigZone( &p2 );
	
	lineManager->lineLostByPlayer( pLinha );
}

//efeito na zona pressionada
void PUBomb::effectInZone( Point3f *p ,LineGroup *lineManager, ZoneGroup *zoneManager, DotGame* dg){

//	if( !ZoneManager->isZoneOccupied( p ) )
//		return;
	uint8 x,y;
	
	Point3f* pZona;
	
	if( Random::GetInt( 0 , 1 ) == 0 )
		pZona = sortZoneRandom();
	else {
		pZona = sortZoneFreeRandom();
	}

	
	dg->getZoneXYByPosition( pZona, &x, &y );

	Point3f p1,p2,p3,p4;
#if DEBUG
	NSLog(@"agindo na zona/quadrado :( %3.3f, %3.3f ), e nas suas linhas adjacentes ", pZona->x,pZona->y );
#endif

	p1 = LineGroup::getLineCoordenatesByXY( x , y ,true );
	p2 = LineGroup::getLineCoordenatesByXY( x + 1 , y ,true );
	p3 = LineGroup::getLineCoordenatesByXY( x , y ,false );
	p4 = LineGroup::getLineCoordenatesByXY( x , y + 1, false );
	
	effectInLine( &p1, lineManager, zoneManager, dg );
	effectInLine( &p2, lineManager, zoneManager, dg );
	effectInLine( &p3, lineManager, zoneManager, dg );
	effectInLine( &p4, lineManager, zoneManager, dg );
	
	zoneManager->zoneLostedByPlayer( pZona );//setZoneLocked(pZona, true);
}

bool PUBomb::loadImageToBar( void ){
	if( !imageBarSmall )
		imageBarSmall = new RenderableImage("BSmall", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);

	if( !imageBarMedium )
		imageBarMedium = new RenderableImage("BMedium", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);

	if( !imageBarSmall || !imageBarMedium )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup Bombs Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading Bombs images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUBomb::unLoadImageToBar( void ){
	SAFE_DELETE( imageBarSmall );
	SAFE_DELETE( imageBarMedium );
//	static RenderableImage* imageBarSmall;
//	static RenderableImage* imageBarMedium;
}

Renderable* PUBomb::getImageToBar( void ){
	if( sizeBomb == BOMB_SIZE_SMALL )
		return imageBarSmall;
	if( sizeBomb == BOMB_SIZE_MEDIUM )
		return imageBarMedium;
	return NULL;
}