/*
 *  PowerUp.h
 *  dotGame
 *
 *  Created by Max on 2/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_H
#define POWER_UP_H 1

//#include "RenderableImage.h"
#include "Sprite.h"
#include "PowerUpFactory.h"
#include "RenderableImage.h"
#include "Exceptions.h"

class ZoneGame;
class PowerUpListener;
class Player;
class LineGroup;
class ZoneGroup;
class DotGame;
class PowerUpGroup;


enum PowerUpTarget {
	POWER_UP_TARGET_PLAYER = -1,
	POWER_UP_TARGET_LINE = 0,
	POWER_UP_TARGET_ZONE,
//	POWER_UP_TARGET_PLAYER,
}typedef PowerUpTarget;

class PowerUp :public Sprite  {
	
	friend class PowerUpGroup;
public:
	
	PowerUp( PowerUpType p , PowerUpListener *p = NULL );
	
	virtual ~PowerUp( void );

	bool isDisarmed( void );
	
	void setListener( PowerUpListener *p );
	
	PowerUpListener* getPowerUpListener( void );
	
	PowerUpType getPowerUpType( void );

	void setZone( ZoneGame *z );

	virtual bool render();
	
	virtual bool update( float timeElapsed );

	void setPowerUpGroup( PowerUpGroup *p );
	
//	Texture2DHandler
	
	//código para a cpu agir!!
	//virtual void cpuDo( Player* p ){};
	
#if DEBUG
 void setName(const char* objName){
		Sprite::setName( objName );
 };
#endif
	
	void setPosition( uint8 x, uint8 y );

	//diz o local onde o power up vai agir
	void InputPosition( Point3f *p );

	//o código do power up agindo
	virtual void powerUpDo( DotGame* dg ) = 0 ;
	
	//atualiza o powerUp caso ele tenha algum  contador
	virtual void updatePowerUp( void ) {}//= 0;
	
	//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
	virtual void gainedByPlayer( Player* p ) = 0;

	//utilizado qdo o powerUp é liberado
	virtual void onRelease( void ) = 0;

	//imagem a ser exibida na barra dos personagens
	virtual Renderable* getImageToBar( void ) = 0;
	
	//retorna a animação do power up
	virtual Sprite* getAnimation( void )=0;
	
	void setVisible( bool b ){ Sprite::setVisible( b ); };
	
	void setLineGroup( LineGroup *l ){ lineManager = l; };
	
	void setZoneGroup( ZoneGroup *z ){ zoneManager = z; };
	
	PowerUpTarget getPowerUpTarget( void );	

	void setPowerUpTarget(PowerUpTarget t );
	
	bool isObserved( void );
	
	void setObserved( bool b );
	
	static bool loadImages( void );
	
	static void removeImages( void );
	
	Renderable* getHiddenImage( void );

	bool isAnimed( void );
	
protected:	
	void sortLineRandom( Point3f* p );
	
	void sortLineFreeRandom( Point3f* p );
	
	void sortZoneRandom( Point3f* p );

	void sortZoneFreeRandom( Point3f* p );	
	
	bool disarmed;

	
	//Sprite* giftAnim;
	
	ZoneGame* zoneOf;
	LineGroup* lineManager;
	ZoneGroup* zoneManager;
	PowerUpListener *pListener;
	PowerUpType pwrUpType;
	Point3f *pPonto;
	PowerUpTarget target;
	Player *pPlayer;
	bool observing,animed;
	RenderableImage *currPic;
	PowerUpGroup *pGroup;

	static	RenderableImage* alternative;
	static  RenderableImage* hidden;
	static Sprite* shine;

};

inline PowerUpTarget PowerUp::getPowerUpTarget( void ){ 
	return target;
}

inline bool PowerUp::isDisarmed( void ){
	return disarmed;
}

inline bool PowerUp::isAnimed( void ){
	return animed;
}

inline	PowerUpType PowerUp::getPowerUpType( void ){
	return pwrUpType;
}

inline PowerUpListener* PowerUp::getPowerUpListener( void ){
	return pListener;
}
inline bool PowerUp::isObserved( void ){
	return observing;
}



class PowerUpListener{
public:
	virtual ~PowerUpListener( void ){};
	virtual void PowerUpEffect( PowerUp* p ) = 0;
	virtual DotGame* getDotGameInstance( void ){};
	
};
#endif