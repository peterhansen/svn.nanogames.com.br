/*
 *  PUMoveBonus.mm
 *  dotGame
 *
 *  Created by Max on 3/15/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUMoveBonus.h"
#include "DotGame.h"

RenderableImage* PUMoveBonus::imageBar = NULL ;

PUMoveBonus::PUMoveBonus( PowerUpListener *p ):	PowerUp( POWER_UP_TYPE_MOVE_BONUS , p  ){}

void PUMoveBonus::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}

void PUMoveBonus::gainedByPlayer( Player* p ){
//depois mudar para ser qdo o jogador quiser
	//onRelease();
}

void PUMoveBonus::powerUpDo( DotGame* dg ){
	dg->setLinesSelected( dg->getLinesSelected() - 2 );

}


Renderable* PUMoveBonus::getImageToBar( void ){
	return imageBar;
}
	
bool PUMoveBonus::loadImageToBar( void ){
	if( !imageBar )
		imageBar = new RenderableImage("life", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	if( !imageBar )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup MoveBonus Carregadas");
#endif
	return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading MoveBonus images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUMoveBonus::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );
}
//Sprite* PUMoveBonus::getAnimation( void ){
//	return NULL;
//}