/*
 *  PowerUpGroup.h
 *  dotGame
 *
 *  Created by Max on 4/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_GROUP_H
#define POWER_UP_GROUP_H 1

#include"Object.h"
#include<vector>
#include "Sprite.h"
class PowerUp;
//classe responsável pelos power ups( renderizar, desalocar power ups livres, animação, etc. )
class PowerUpGroup : public Object{//, public PowerUpListener{//,public SpriteListener{//,public PowerUpListener {
	
public:

 	PowerUpGroup();

	virtual ~PowerUpGroup();
	
	void adPowerUp( PowerUp *p );
	
//	// Chamado quando o sprite muda a etapa de animação
//	virtual void onAnimStepChanged( Sprite* pSprite ){};
//	
//	// Chamado quando o sprite termina uma sequência de animação
//	virtual void onAnimEnded( Sprite* pSprite ){};
	
	bool renderAnimPowerUp( void );
	
	virtual bool update( float timeElapsed );

	virtual bool render( void );

	void setPowerUpObserving( bool b );

	void setCurrAnim( PowerUp *p );

	PowerUp* getCurrPowerUp( void ){ return currPower ;};
	void setCurrPowerUp( PowerUp* p ){ currPower = p; };
	//começa a animação do powerUp
	
	//remove os power Ups que estão livres ( que não pertencem a jogador ou a zona/quad nenhum )
	void removeFreePowerUps( void );
protected:

	std::vector< PowerUp* >PowerUpList;	
	Sprite *currAnim;
	//pówer Up corrente
	PowerUp* currPower;
};


#endif