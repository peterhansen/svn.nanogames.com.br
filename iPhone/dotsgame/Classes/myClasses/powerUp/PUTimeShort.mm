/*
 *  PUTimeShort.mm
 *  dotGame
 *
 *  Created by Max on 3/16/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUTimeShort.h"
#include "DotGame.h"
#include "Random.h"
#define MAX_TO_DIVIDE_TIME_TO 2

RenderableImage* PUTimeShort::imageBarPositivo = NULL;
RenderableImage* PUTimeShort::imageBarNegativo = NULL;

PUTimeShort::PUTimeShort( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_TIME_SHORT , p ){
	if( Random::GetInt( 0, 1 ) == 1 )
		tipo = POWER_UP_TIME_SHORT_POSITIVE;
	else
		tipo = POWER_UP_TIME_SHORT_NEGATIVE;

}

void PUTimeShort::onRelease( void ){

	powerUpDo( pListener->getDotGameInstance() );
}

void PUTimeShort::gainedByPlayer( Player* p ){
if( tipo == POWER_UP_TIME_SHORT_NEGATIVE )
	onRelease();
	//onRelease();
}



void PUTimeShort::powerUpDo( DotGame* dg ){
	if( tipo == POWER_UP_TIME_SHORT_NEGATIVE )
	//dg->pChronometer->setCurrTime()
		dg->getChronometer()->setCurrTime( dg->getChronometer()->getCurrTime() * MAX_TO_DIVIDE_TIME_TO );
	else if( tipo == POWER_UP_TIME_SHORT_POSITIVE )
		dg->setDivisorValue( dg->getDivisorValue() * MAX_TO_DIVIDE_TIME_TO );
}

bool PUTimeShort::loadImageToBar( void ){
if( !imageBarNegativo )
	imageBarNegativo = new RenderableImage("TSNegativo", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	if( !imageBarPositivo )
		imageBarPositivo = new RenderableImage("TSPositivo", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	if( !imageBarNegativo || !imageBarPositivo)
		goto Error;
#if DEBUG
	NSLog(@"imagens PupTimeShort Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading PupTimeShort images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUTimeShort::unLoadImageToBar( void ){
	SAFE_DELETE( imageBarPositivo );
	SAFE_DELETE( imageBarNegativo );

}

Renderable* PUTimeShort::getImageToBar( void ){
    if( tipo == POWER_UP_TIME_SHORT_POSITIVE )
		return imageBarPositivo;
	return imageBarNegativo;
	}
//Sprite* PUTimeShort::getAnimation( void ){return NULL;}
