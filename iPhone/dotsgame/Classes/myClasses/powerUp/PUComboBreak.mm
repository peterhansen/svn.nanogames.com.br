/*
 *  PUComboBreak.mm
 *  dotGame
 *
 *  Created by Max on 2/4/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUComboBreak.h"
#include "DotGame.h"

RenderableImage* PUComboBreak::imageBar = NULL ;

PUComboBreak::PUComboBreak( PowerUpListener* p ):PowerUp( POWER_UP_TYPE_COMBO_BREAK, p ){

animed = false;}

//o código do power up agindo
void PUComboBreak::powerUpDo( DotGame* dg ){
//agindo
	
#if DEBUG
	NSLog(@"combobreak agindo-> diminui a qte de especial do mané");
#endif
	//com o trecho de código abaixo, consiguimos fazer com que o contador de combos do karinha caia à metade, como o combo não está sendo em consideração no momento, resolvi adaptar este código para diminuir o valor da barra de especial, mas caso venhamos a considerar o valor do combo, e quisermos fazer um power down para isso, o código está abaixo:
//	dg->setComboValue( dg->getComboValue() / 2 ); sim isso mesmo...
//	pPlayer->setPowerUpMetter( pPlayer->getPowerUpMetter() - 1);
	pPlayer->setSpecialLevel( pPlayer->getSpecialLevel() - 1 );
}

//quando ele é ganho por algum jogador ( obviamente, pode ser que nem todos tenham executem este método )
void PUComboBreak::gainedByPlayer( Player* p ){
#if DEBUG
	NSLog(@"combo break solto!!");
#endif

	onRelease();

}

void PUComboBreak::onRelease( void ){

	powerUpDo( pListener->getDotGameInstance() );
	

}



Renderable* PUComboBreak::getImageToBar( void ){
	return imageBar;
}
	
bool PUComboBreak::loadImageToBar( void ){
if( !imageBar )
	imageBar = new RenderableImage("Cbreak", RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	if( !imageBar )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup lockLines Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading lockLines images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
Sprite* PUComboBreak::getAnimation( void ){
	return NULL;
}

void PUComboBreak::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );
	//SAFE_DELETE( animation );
}
