/*
 *  PowerUpFactory.h
 *  dotGame
 *
 *  Created by Max on 2/18/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_FACTORY_H 
#define POWER_UP_FACTORY_H 1

#define N_POWER_UPS 9//10
//colocar mais depois...
enum PowerUpType{
	POWER_UP_TYPE_NONE = -1,
	POWER_UP_TYPE_COMBO_BREAK = 0,
	POWER_UP_TYPE_MOVE_BONUS,
	//falta
	POWER_UP_TYPE_MOVE_RANDOM,
	POWER_UP_TYPE_TIME_SHORT,
	POWER_UP_TYPE_BOMB,
	POWER_UP_TYPE_LOCK_LINE,
	POWER_UP_TYPE_DESTROY,
	POWER_UP_TYPE_STEAL,
	POWER_UP_TYPE_DESTROY_ZONE,
	POWER_UP_TYPE_REVERSE_BOARD,
}typedef PowerUpType;

class PowerUp;
class DotGame;
class PowerUpListener;
//falta fazer um gerador aleatório...
class PowerUpFactory {

public:
	static bool Create( void );
	
	static void Destroy( void );
	
	static PowerUpFactory* GetInstance( void ){ return pSingleton; }
	
	static bool loadAllImages( void );
	
	static void unLoadAllImages( void );
	
	PowerUp* getNewPowerUp( PowerUpType t, PowerUpListener *dg );

	int getNPowerTypes( void );
	
private:
	PowerUpFactory();
	
	~PowerUpFactory();
	
	static PowerUpFactory* pSingleton;
	
	int nPowerTypes;
	
};

inline int PowerUpFactory::getNPowerTypes( void ){
	return nPowerTypes;
}

#endif