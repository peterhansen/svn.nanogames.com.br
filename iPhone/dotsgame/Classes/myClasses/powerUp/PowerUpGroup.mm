/*
 *  PowerUpGroup.mm
 *  dotGame
 *
 *  Created by Max on 4/19/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUpGroup.h"
#include "PowerUp.h"
#include "PowerUpFactory.h"
#include "ObjcMacros.h"


PowerUpGroup::PowerUpGroup():currAnim( NULL ),currPower( NULL ){}

PowerUpGroup::~PowerUpGroup(){
//é eu sei, o melhor seria mexer no template, para que nesse caso ele fizesse isso ...
	for( int16 i = 0 ; i < PowerUpList.size() ;i++ )
		SAFE_DELETE( PowerUpList[ i ] );
	PowerUpList.clear();
	currAnim = NULL;
}
	
void PowerUpGroup::adPowerUp( PowerUp *p ){
	p->pGroup = this;
	PowerUpList.push_back( p );
	PowerUpList.resize( ( int )PowerUpList.size() );
}
	
bool PowerUpGroup::update( float timeElapsed ){
	
	if( !isActive() || PowerUpList.empty())//size() == 0 )
		return false;
	bool ret = false;	
	if( currAnim )
		ret |= currAnim->update( timeElapsed );
	for( int16 i = 0 ; i < PowerUpList.size() ;i++ )
		ret |= PowerUpList[ i ]->update( timeElapsed );
	return ret;
}


/*=-=--==-=-==-=--=-----=-=-==-=--=-=-=======================================================================
 renderiza os PowerUps
 
 isso nao é mais necessário, visto que tive que alterar o método de renderização
acessando  http://en.wikipedia.org/wiki/Virtual_function#C.2B.2B explica como fazer um std::vector com classes virtuais acessando métodos virtuais, vcs podem até saber disso, só para ver que não é algo tão alienígena assim...
 
=-=--==-=-==-=--=-----=-=-==-=--=-=-=======================================================================*/
bool PowerUpGroup::render( void ){
	
	if( !isVisible() || PowerUpList.size() == 0 )
		return false;
	
	
	bool ret = false;
	
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x, position.y, position.z );
	glScalef( scale.x, scale.y, scale.z );
	
	Matrix4x4 aux;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &aux ) ) );

	for( int16 i = 0 ; i < PowerUpList.size() ;i++ )
		ret |= PowerUpList[ i ]->render();
	//OLD, mudei pq eu tive que alterar o método de renderização, logo tornando o trecho de código a seguir desnecessário, mas fica a nota para quem quiser ver...
//	for( std::vector< PowerUp* >::const_iterator it = PowerUpList.begin()  ; it != PowerUpList.end() ; ++it ){
//		ret |=	( *it )->render();
//	}
	
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	
	if( customViewport )
		glDisable( GL_SCISSOR_TEST );
	ret |= renderAnimPowerUp();
	return ret;
}
/*
 chamar depois do uso do power up! libera os powerups que não estão associados a nenhuma zona/quad, ou player
 */
void PowerUpGroup::removeFreePowerUps( void ){
	std::vector< PowerUp* > aux;
	for( int16 i = 0 ; i < PowerUpList.size() ;i++ ){
		std::vector< PowerUp* >::const_iterator it = PowerUpList.begin();
		if( PowerUpList[ i ]->zoneOf == NULL && PowerUpList[ i ]->pPlayer == NULL ){
			SAFE_DELETE( PowerUpList[ i ] );
			continue;
			//PowerUpList.erase( it );
		}
		aux[ i ] = PowerUpList[ i ];
		++it;
	}
	
	//conferir depois
	PowerUpList.swap( aux );
	//PowerUpList.copy( aux );
	//for( int16 i = 0 ; i < PowerUpList.size() ;i++ ){
//	for( std::vector< PowerUp* >::const_iterator it = PowerUpList.begin()  ; it != PowerUpList.end() ; ++it ){
//		if( /*PowerUpList[ i ]*/ ( *it )->zoneOf == NULL && /*PowerUpList[ i ]*/ ( *it )->pPlayer == NULL ){
//		
//			//SAFE_DELETE( ( *it )/* PowerUpList[ i ]*/);
//			//PowerUpList.erase( /*PowerUpList[ i ]*/ it  );
//		
//		}
//	}

}

void PowerUpGroup::setPowerUpObserving( bool b ){
	for( int16 i = 0 ; i < PowerUpList.size() ;i++ ){
		PowerUpList[ i ]->setObserved( b );
	}
}

bool PowerUpGroup::renderAnimPowerUp( void ){
	if( currAnim != NULL )
		return false;
	return currAnim->render();
}

void PowerUpGroup::setCurrAnim( PowerUp *p ){
	if( !p->isAnimed() )
		return;
	currAnim = p->getAnimation();
	currAnim->setAnimSequence( p->getCurrAnimSequence() );
}