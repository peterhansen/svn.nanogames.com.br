/*
 *  PUReverse.mm
 *  dotGame
 *
 *  Created by Max on 4/14/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUReverse.h"
#include "DotGame.h"
#include "ZoneGroup.h"
#include "Player.h"
RenderableImage* PUReverse::imageBar = NULL ;

PUReverse::PUReverse( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_REVERSE_BOARD ,p ){}

void PUReverse::onRelease( void ){
	powerUpDo( pListener->getDotGameInstance() );
}

void PUReverse::gainedByPlayer( Player* p ){
	pPlayer = p;
	//onRelease();
}


void PUReverse::powerUpDo( DotGame* dg ){

//	ZoneGroup *z;
	Player* p;

	zoneManager = dg->getZoneGroup();
	p = dg->getPlayerByIndice( pPlayer->getId() + 1 );
	ZoneGame *tmpZ;	
	for( int16 i = 0; i < zoneManager->getNObjects() ; i++ ){
		tmpZ = static_cast< ZoneGame* > ( zoneManager->getObject( i ) );
		if( tmpZ->isOccupied() && !tmpZ->isLocked() )
			
#if DEBUG
		{
			uint8 x, y;
			Point3f p;
			p = *tmpZ->getPosition();
		dg->getZoneXYByPosition( &p ,&x ,&y );
		NSLog(@"agindo na zona/quad %d,%d",x,y  );
		}
#endif
			
			if(  tmpZ->getPlayerId() == pPlayer->getId() ){
				tmpZ->changePlayer( p );
			}else {
				tmpZ->changePlayer( pPlayer );
			}
	}
	uint8 tmp = dg->getPlayerByIndice( 0 )->getScore();
	dg->getPlayerByIndice( 0 )->setScore( dg->getPlayerByIndice( 1 )->getScore() );
	dg->getPlayerByIndice( 1 )->setScore( tmp );
	
	dg->getInfoBar()->getLifeBar()->swapSlices(); //updateScores( );
	dg->getInfoBar()->updateScores( dg );
}

Renderable* PUReverse::getImageToBar( void ){
	return imageBar;
}
	
bool PUReverse::loadImageToBar( void ){
if( !imageBar )
	imageBar = new RenderableImage("reverse");
	if( !imageBar )
		goto Error;
#if DEBUG
	NSLog(@"imagens Pup reverse Carregadas");
#endif
	return true;
Error:
	#if DEBUG
	throw ConstructorException("fail while loading reverse images");
#else
	throw ConstructorException();
#endif 	
	return false;
}
	
void PUReverse::unLoadImageToBar( void ){
	SAFE_DELETE( imageBar );

}
