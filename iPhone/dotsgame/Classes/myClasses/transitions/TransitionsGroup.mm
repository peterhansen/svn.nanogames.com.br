/*
 *  TransitionsGroup.mm
 *  dotGame
 *
 *  Created by Max on 11/4/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

//#include "TransitionsGroup.h"
//#include "OrthoCamera.h"
//#include "Exceptions.h"
//#include "Macros.h"
//#include"ObjcMacros.h"
//
//TransitionsGroup::TransitionsGroup( uint16 maxTransitions, OrthoCamera* pCamera )
//:OGLTransition( pListener ),maxTransitions( maxTransitions ), nTransitions( 0 ),
//pCamera( pCamera ), pOGLTrans( NULL ), activeTransition( 0 ), pCurrTrans( NULL )
//{
//	if( !buildTransitionsGrupo() )
//#if DEBUG
//		throw OutOfMemoryException( "TransitionsGroup::TransitionsGroup( uint16 ): unable to create object" );
//#else
//	throw OutOfMemoryException();
//#endif
//}
//
//
///*=======================================================================================
// destrutor
// =======================================================================================*/
//TransitionsGroup::~TransitionsGroup(){
//	removeAllTransitions();
//	
//	DELETE_VEC( pOGLTrans );
//}
//
//
////renderiza transitions
//bool TransitionsGroup::render( void ){
//	
//	
//	pCamera->place();
//	//bool ret = false;
////	for ( uint16 x ; x </*=*/ nTransitions ;x++ ) {
////		ret |= pOGLTrans[ x ]->render();
////	}
////	return ret;
//	if( pCurrTrans )
//	pCurrTrans->render();
//}
//
////insere transitions
//bool TransitionsGroup::insertTransitions( OGLTransition* o, uint16 *pIndex  ){
//	if( ( o == NULL ) || ( nTransitions >= maxTransitions ) )
//		return false;
//	pOGLTrans[ nTransitions ] = o;
//	
//	pOGLTrans[ nTransitions ]->setActive( false );
//	
//	if( pIndex )
//		*pIndex = nTransitions;
//	
//	++nTransitions;
//	return true;
//}
//
////esvazia o grupo
//void TransitionsGroup::removeAllTransitions( void ){
//	
//	// Apaga os objetos do grupo
//	for( uint16 i = 0 ; i < nTransitions ; ++i )
//	{
//		DELETE( pOGLTrans[i] );
//	}
//	
//	// Atualiza a variável que controla quantos elementos possuímos no grupo
//	nTransitions = 0;
//	
//}
//
////retorna a transitions contida no index 
//OGLTransition* TransitionsGroup::getTransitions( uint16 index ){
//	
//	if( index >= nTransitions )
//		return NULL;
//	return pOGLTrans[ index ];
//}
//
//
////constrói o grupo
//bool TransitionsGroup::buildTransitionsGrupo( void ){
//	
//	pOGLTrans = new OGLTransition* [ maxTransitions ];
//	if( pOGLTrans == NULL )
//		return false;
//	return true;
//}
//
//
////atualiza o grupo
//bool TransitionsGroup::update( float timeElapsed ){
//	//bool ret = false;
////	for ( uint16 x ; x </*=*/ nTransitions ;x++ ) {
////		ret |= pOGLTrans[ x ]->update( timeElapsed );
////	}
////	return ret;
//	return pCurrTrans->update( timeElapsed );
//	
//}
//
//
////reseta todas as transitions
//
//void TransitionsGroup::reset( void ) {
//	for ( uint16 x ; x </*=*/ nTransitions ;x++ ) {
//		pOGLTrans[ x ]->reset();
//	}
//}
//
//
//
////escolhe a transitions para ser exibida de acordo com o índice
//bool TransitionsGroup::setTransition( uint16 index ){
//if( index >= nTransitions )
//	return false;
//	for ( uint16 x ; x </*=*/ nTransitions ;x++ ) {
//		pOGLTrans[ x ]->setActive( false );
//	}
//	pOGLTrans[ index ]->setActive( true );
//	pCurrTrans = pOGLTrans[ index ];
//	return true;
//
//}
