/*
 *  CurtainLeftTransitions.mm
 *  dotGame
 *
 *  Created by Max on 10/16/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

//#include "CurtainLeftTransition.h"

#include "Macros.h"
#include "Quad.h"
#include "ObjcMacros.h"
#include"GLHeaders.h"

/*
CurtainLeftTransition::CurtainLeftTransition ( float duration, OGLTransitionListener* pListener, const Color& color ):OGLTransition( pListener ),
color( color ),
maxDoorWidth( SCREEN_WIDTH * 1.5 ), 
widthCounter1( maxDoorWidth ),
widthCounter2( maxDoorWidth ),
transitionSpeed( maxDoorWidth / duration ){
}


bool CurtainLeftTransition::render( void ){
	
	if( !OGLTransition::render() || ( color.getFloatA() == 0 ) )
		return false;
	
	glColor4f( color.getFloatR(), color.getFloatG(), color.getFloatB(), color.getFloatA() );
	glMatrixMode( GL_MODELVIEW );
	
	Quad q1,q2;
	q1.disableRenderStates( RENDER_STATE_TEXCOORD_ARRAY | RENDER_STATE_COLOR_ARRAY | RENDER_STATE_NORMAL_ARRAY );
	q2.disableRenderStates( RENDER_STATE_TEXCOORD_ARRAY | RENDER_STATE_COLOR_ARRAY | RENDER_STATE_NORMAL_ARRAY );
	
	
	glPushMatrix();	
	
		glTranslatef( widthCounter2 * 0.5 , HALF_SCREEN_HEIGHT, 0.0f );
		glScalef( widthCounter2, SCREEN_HEIGHT, 1.0f );
		q1.render();
	
	glPopMatrix();
	return true;
	
}

// Atualiza o objeto
bool CurtainLeftTransition::update( float timeElapsed ){
	
	if( !OGLTransition::update( timeElapsed ) )
		return false;
	
	if( widthCounter1 > maxDoorWidth && widthCounter2 <0 )
		pListener->onOGLTransitionEnd();
	else{
		widthCounter1 += transitionSpeed * timeElapsed;
		widthCounter2 -= transitionSpeed * timeElapsed;
	}
	
	return true;
}

// Reinicializa o objeto
void CurtainLeftTransition::reset( void ){
	widthCounter1 = SCREEN_WIDTH;
	widthCounter2 = SCREEN_WIDTH;
}
*/