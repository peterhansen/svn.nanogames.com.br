/*
 *  TransitionsGroup.h
 *  dotGame
 *
 *  Created by Max on 11/4/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
//
//#ifndef TRANSITIONS_GROUP_H
//#define TRANSITIONS_GROUP_H 1
//
//#include"OGLTransition.h"
//
//class OrthoCamera;
//
//
//
//class TransitionsGroup: OGLTransition {
//	
//	
//public:
//	
//	// Construtor
//	// Exceções: pode disparar OutOfMemoryException
//	explicit TransitionsGroup( uint16 maxTransitions, OrthoCamera *pCamera );
//	
//    virtual ~TransitionsGroup();
//	
//	//renderiza transitions
//	virtual bool render( void );
//	
//	//insere transitions
//	bool insertTransitions( OGLTransition* o, uint16 *pIndex = NULL ); 
//	
//	//esvazia o grupo
//	void removeAllTransitions( void );
//	
//	//retorna o número de transitions contidos no grupo
//	inline uint16 getnTransitions( void ) const { return nTransitions; }
//	
//	//retorna o número máximo de transitions contidos no grupo
//	inline uint16 getMaxTransitions( void ) const { return maxTransitions; }
//	
//	//retorna a transitions contida no index 
//	OGLTransition* getTransitions( uint16 index );
//
//	//atualiza o objeto
//	bool update( float timeElapsed );
//	
//	//escolhe a transitions para ser exibida de acordo com o índice
//	bool setTransition( uint16 index );
//	
//	void reset( void );
//	
//protected:
//
//	//inicializa o objeto
//	bool buildTransitionsGrupo( void );
//	
//
//	
//private:
//	//câmera própria
//	OrthoCamera * pCamera;
//	
//	OGLTransition* pCurrTrans;
//	// Objetos que formam o grupo
//	OGLTransition** pOGLTrans ;
//	
//	//quantas transições existem no grupo
//	uint16 nTransitions;
//	
//	//máximo de transições que existem no grupo
//	uint16 maxTransitions;
//	
//	//transition ativa
//	uint16 activeTransition;
//};
//
//
//#endif