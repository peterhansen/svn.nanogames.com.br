#include "GoodGirlFont.h"

#include "GLHeaders.h"
#include "Quad.h"

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

GoodGirlFont::GoodGirlFont( void ) : Font( "gg", "gg", false )
{
}

/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

GoodGirlFont::~GoodGirlFont( void )
{
}

/*===========================================================================================
 
MÉTODO getCharSize
	Retorna o tamanho do caractere na fonte.

============================================================================================*/

Point2i* GoodGirlFont::getCharSize( Point2i* pOut, unichar c ) const
{
	if( c >= '0' && c <= '9' )
	{
		pOut->x = pFontImage->getOriginalFrameWidth();
		pOut->y = fontHeight;
	}
	else
	{
		if( c == ' ' )
		{
			pOut->x = spaceWidth;
			pOut->y = fontHeight;
		}
		else
		{
			int32 charFrameIndex = getCharFrameIndex( c );
			if( charFrameIndex >= 0 )
			{
				const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );

				pOut->x = pCharFrame->width;
				pOut->y = pCharFrame->height;
			}
			else
			{
				pOut->set();
			}
		}
	}
	return pOut;
}

/*===========================================================================================
 
MÉTODO renderText
	Renderiza o texto com o espaçamento de caracteres desejado.

============================================================================================*/

void GoodGirlFont::renderText( const char* pText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const
{
	// Habilita o blending para utilizarmos a transparência
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	// Habilita a textura da fonte
	glEnable( GL_TEXTURE_2D );
	pFontImage->load();
	
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	// OBS : Receber o scale do label
//	if( FDIF( scale.x, 1.0f ) || FDIF( scale.y, 1.0f ) )
//	{
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
//	}
//	else
//	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
//	}
	
	// Indica que estamos no sistema local
	glMatrixMode( GL_MODELVIEW );
	
	// Checa o alinhamento horizontal. O default é ALIGN_HOR_LEFT
	float x = pArea->x;
	if( ( alignment & ALIGN_HOR_RIGHT ) != 0 )
	{
		Point2i aux;
		x = pArea->x + pArea->width - getTextSize( &aux, pText, spacement )->x;
	}
	else if( ( alignment & ALIGN_HOR_CENTER ) != 0 )
	{
		Point2i aux;
		x = pArea->x + (( pArea->width - getTextSize( &aux, pText, spacement )->x ) * 0.5f );
	}
	// OBS: Não é necessária a implementação
//	else if( ( alignment & ALIGN_HOR_LEFT ) != 0 )
//	{
//	}
	
	// Checa o alinhamento vertical. O default é ALIGN_VER_TOP
	float y = pArea->y;
	if( ( alignment & ALIGN_VER_BOTTOM ) != 0 )
	{
		y = pArea->y + pArea->height - fontHeight;
	}
	else if( ( alignment & ALIGN_VER_CENTER ) != 0 )
	{
		y = pArea->y + (( pArea->height - fontHeight ) * 0.5f );
	}
	// OBS: Não é necessária a implementação
//	else if( ( alignment & ALIGN_VER_TOP ) != 0 )
//	{
//	}

	// Percorre a string
	float texCoords[8];
	uint32 nChars = strlen( pText );
	
	for( uint32 i = 0 ; i < nChars ; ++i )
	{
		// Verifica se possuímos o caractere da string na fonte
		unichar c = pText[i];
		if( c != ' ' )
		{
			int32 charFrameIndex = getCharFrameIndex( c );
			if( charFrameIndex >= 0  )
			{				
				// Obtém as coordenadas de texura desse caractere
				const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );
				pFontImage->getFrameTexCoords( charFrameIndex, texCoords );
				
				// Renderiza o caractere
				Quad q;
				q.quadVertexes[0].setTexCoords( texCoords[0], texCoords[1] );
				q.quadVertexes[1].setTexCoords( texCoords[2], texCoords[3] );
				q.quadVertexes[2].setTexCoords( texCoords[4], texCoords[5] );
				q.quadVertexes[3].setTexCoords( texCoords[6], texCoords[7] );

				glPushMatrix();
		
				// OBS : Isso REALMENTE funciona...
				//	An optimum compromise that allows all primitives to be specified at integer
				//positions, while still ensuring predictable rasterization, is to translate x
				//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
				// away from the centers of pixels, while moving line vertices close enough to
				// the pixel centers
				glTranslatef( x + pCharFrame->offsetX + ( pCharFrame->width * 0.5f ) + 0.375f, y + pCharFrame->offsetY + ( pCharFrame->height * 0.5f ) + 0.375f, 0.0f );
				glScalef( pCharFrame->width, pCharFrame->height, 1.0f );
				
				q.render();
				
				glPopMatrix();
				
				// Incrementa o contador da posição x para a renderização do próximo caractere
				if( c >= '0' && c <= '9' )
					x += pFontImage->getOriginalFrameWidth() + spacement;
				else
					x += pCharFrame->width + spacement;
			}
		}
		// Se o tamanho do caractere espaço for 0, considera que esse caractere não existe na fonte
		else if( spaceWidth > 0 )
		{
			// Incrementa o contador da posição x para a renderização do próximo caractere
			x += spaceWidth + spacement;
		}
	}
	
	// Desabilita a textura da fonte
	pFontImage->unload();
}

// OLD
//void GoodGirlFont::renderText( const NSString* hText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const
//{
//	// Habilita o blending para utilizarmos a transparência
//	glEnable( GL_BLEND );
//	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
//
//	// Habilita a textura da fonte
//	glEnable( GL_TEXTURE_2D );
//	pFontImage->load();
//	
//	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
//
//	// OBS : Receber o scale do label
////	if( FDIF( scale.x, 1.0f ) || FDIF( scale.y, 1.0f ) )
////	{
////		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
////		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
////	}
////	else
////	{
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
////	}
//	
//	// Indica que estamos no sistema local
//	glMatrixMode( GL_MODELVIEW );
//	
//	// Checa o alinhamento horizontal. O default é ALIGN_HOR_LEFT
//	float x = pArea->x;
//	if( ( alignment & ALIGN_HOR_RIGHT ) != 0 )
//	{
//		Point2i aux;
//		x = pArea->x + pArea->width - getTextSize( &aux, hText, spacement )->x;
//	}
//	else if( ( alignment & ALIGN_HOR_CENTER ) != 0 )
//	{
//		Point2i aux;
//		x = pArea->x + (( pArea->width - getTextSize( &aux, hText, spacement )->x ) * 0.5f );
//	}
//	// OBS: Não é necessária a implementação
////	else if( ( alignment & ALIGN_HOR_LEFT ) != 0 )
////	{
////	}
//	
//	// Checa o alinhamento vertical. O default é ALIGN_VER_TOP
//	float y = pArea->y;
//	if( ( alignment & ALIGN_VER_BOTTOM ) != 0 )
//	{
//		y = pArea->y + pArea->height - fontHeight;
//	}
//	else if( ( alignment & ALIGN_VER_CENTER ) != 0 )
//	{
//		y = pArea->y + (( pArea->height - fontHeight ) * 0.5f );
//	}
//	// OBS: Não é necessária a implementação
////	else if( ( alignment & ALIGN_VER_TOP ) != 0 )
////	{
////	}
//
//	// Percorre a string
//	float texCoords[8];
//	uint32 nChars = [hText length];
//	
//	for( uint32 i = 0 ; i < nChars ; ++i )
//	{
//		// Verifica se possuímos o caractere da string na fonte
//		unichar c = [hText characterAtIndex:i];
//		if( c != ' ' )
//		{
//			int32 charFrameIndex = getCharFrameIndex( c );
//			if( charFrameIndex >= 0  )
//			{				
//				// Obtém as coordenadas de texura desse caractere
//				const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );
//				pFontImage->getFrameTexCoords( charFrameIndex, texCoords );
//				
//				// Renderiza o caractere
//				Quad q;
//				q.quadVertexes[0].setTexCoords( texCoords[0], texCoords[1] );
//				q.quadVertexes[1].setTexCoords( texCoords[2], texCoords[3] );
//				q.quadVertexes[2].setTexCoords( texCoords[4], texCoords[5] );
//				q.quadVertexes[3].setTexCoords( texCoords[6], texCoords[7] );
//
//				glPushMatrix();
//		
//				// OBS : Isso REALMENTE funciona...
//				//	An optimum compromise that allows all primitives to be specified at integer
//				//positions, while still ensuring predictable rasterization, is to translate x
//				//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
//				// away from the centers of pixels, while moving line vertices close enough to
//				// the pixel centers
//				glTranslatef( x + pCharFrame->offsetX + ( pCharFrame->width * 0.5f ) + 0.375f, y + pCharFrame->offsetY + ( pCharFrame->height * 0.5f ) + 0.375f, 0.0f );
//				glScalef( pCharFrame->width, pCharFrame->height, 1.0f );
//				
//				q.render();
//				
//				glPopMatrix();
//				
//				// Incrementa o contador da posição x para a renderização do próximo caractere
//				if( c >= '0' && c <= '9' )
//					x += pFontImage->getOriginalFrameWidth() + spacement;
//				else
//					x += pCharFrame->width + spacement;
//			}
//		}
//		// Se o tamanho do caractere espaço for 0, considera que esse caractere não existe na fonte
//		else if( spaceWidth > 0 )
//		{
//			// Incrementa o contador da posição x para a renderização do próximo caractere
//			x += spaceWidth + spacement;
//		}
//	}
//	
//	// Desabilita a textura da fonte
//	pFontImage->unload();
//}
