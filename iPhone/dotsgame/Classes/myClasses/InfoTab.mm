/*#include "InfoTab.h"

#include "Exceptions.h"
#include "Label.h"
#include "Quad.h"
#include "RenderableImage.h"
#include "Utils.h"

#include "FreeKickAppDelegate.h"
#include "Iso_Defines.h"
#include "GameInfo.h"
#include "GameUtils.h"
#include "ObjcMacros.h"

// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
		if( !pObject || !pGroup->insertObject( pObject ) )	\
		{													\
			delete pObject;									\
			goto Error;										\
		}

// Define o número de objetos no grupo
#define INFO_TAB_N_OBJS ( 7 + ( FOUL_CHALLENGE_TRIES_PER_FOUL * 2 ))

// Índices dos objetos no grupo
#define INFO_TAB_OBJ_INDEX_BACK					0
#define INFO_TAB_OBJ_INDEX_INFO_TAG				1
#define INFO_TAB_OBJ_INDEX_CLOSE_TAG			2
#define INFO_TAB_OBJ_INDEX_SCORE				3
#define INFO_TAB_OBJ_INDEX_LB_TOTAL_POINTS		4
#define INFO_TAB_OBJ_INDEX_LB_CURR_LEVEL		5
#define INFO_TAB_OBJ_INDEX_LB_SCORE_GOAL		6

// Configurações da imagem de fundo
#define INFO_TAB_BACK_IMG_FRAME_WIDTH	207.0f // OLD 314.0f 
#define INFO_TAB_BACK_QUAD_WIDTH		107.0f
#define INFO_TAB_BACK_IMG_FRAME_HEIGHT	320.0f

// Configurações da imagem da tag "Info"
#define INFO_TAB_INFO_TAG_IMG_FRAME_WIDTH	77.0f
#define INFO_TAB_INFO_TAG_IMG_FRAME_HEIGHT	44.0f
#define INFO_TAB_INFO_TAG_DIST_FROM_LEFT	71.0f
#define INFO_TAB_INFO_TAG_DIST_FROM_TOP		14.0f

// Configurações da imagem da tag "Fechar"
#define INFO_TAB_CLOSE_TAG_IMG_FRAME_WIDTH	118.0f
#define INFO_TAB_CLOSE_TAG_IMG_FRAME_HEIGHT	 32.0f
#define INFO_TAB_CLOSE_TAG_DIST_FROM_BOTTOM	 19.0f
#define INFO_TAB_CLOSE_TAG_DIST_FROM_RIGHT	  8.0f

// Configurações da imagem do placar
#define INFO_TAB_SCORE_IMG_FRAME_WIDTH	233.0f
#define INFO_TAB_SCORE_IMG_FRAME_HEIGHT	210.0f
#define INFO_TAB_SCORE_DIST_FROM_TOP	 33.0f

// Duração da animação de exebição do controle
#define INFO_TAB_ANIM_DURATION 0.100f

// Quantidade de pixels que a imagem passa do ponto final antes de voltar
#define INFO_TAB_ANIM_EXTRA_X 5.0f

// Configurações dos labels
#define INFO_TAB_FONT_CHAR_SPACEMENT 1.0f

#define INFO_TAB_LB_TOTAL_POINTS_POS_X	 38.0f // OLD 77.0f
#define INFO_TAB_LB_TOTAL_POINTS_POS_Y	 39.0f // OLD 39.0f
#define INFO_TAB_LB_TOTAL_POINTS_WIDTH	151.0f // OLD 99.0f
#define INFO_TAB_LB_TOTAL_POINTS_HEIGHT	 19.0f // OLD 20.0f

#define INFO_TAB_LB_CURR_LEVEL_POS_X	 29.0f // OLD 35.0f
#define INFO_TAB_LB_CURR_LEVEL_POS_Y	 95.0f // OLD 97.0f
#define INFO_TAB_LB_CURR_LEVEL_WIDTH	 45.0f // OLD 38.0f
#define INFO_TAB_LB_CURR_LEVEL_HEIGHT	 22.0f // OLD 20.0f

#define INFO_TAB_LB_SCORE_GOAL_POS_X	 91.0f // OLD 99.0f
#define INFO_TAB_LB_SCORE_GOAL_POS_Y	 93.0f // OLD 94.0f
#define INFO_TAB_LB_SCORE_GOAL_WIDTH	103.0f // OLD 86.0f
#define INFO_TAB_LB_SCORE_GOAL_HEIGHT	 25.0f // OLD 23.0f

// Configurações das imagens que exibem o resultado de cada tentativa
#define INFO_TAB_FIRST_TRY_ICON_POS_X  43.0f
#define INFO_TAB_FIRST_TRY_ICON_POS_Y 155.0f

#define INFO_TAB_TRIES_ICONS_WIDTH  32.0f
#define INFO_TAB_TRIES_ICONS_DIST_X 24.0f
		
#define INFO_TAB_TRY_RESULT_V_OFFSET_X  10.0f
#define INFO_TAB_TRY_RESULT_V_OFFSET_Y  -5.0f

#define INFO_TAB_TRY_RESULT_X_OFFSET_X  3.0f
#define INFO_TAB_TRY_RESULT_X_OFFSET_Y  3.0f

// Tamanho do buffer para formatação dos textos dos labels
#define INFO_TAB_LABELS_BUFFER_LEN 32

// Velocidade da rolagem dos pontos (em pontos/segundo)
#define INFO_TAB_SCORE_ANIM_MAX_DUR 1.000f
#define INFO_TAB_SCORE_ANIM_MIN_SPEED 0.1f // No mínimo, um ponto a cada 100 milésimos

// Cor da fonte do label "Objetivo de Pontos" quando o usuário alcançou / ultrapassou o objetivo da fase
#define COLOR_SCORE_TARGET_HIT Color( static_cast< uint8 >( 255 ), static_cast< uint8 >( 192 ), static_cast< uint8 >( 0 ), static_cast< uint8 >( 255 ) )

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/
/*
InfoTab::InfoTab( const GameInfo* pGameInfo, InterfaceControlListener* pListener )
		: ObjectGroup( INFO_TAB_N_OBJS ), InterfaceControl( pListener ), closeButtonPressed( false ),
		  goingLeft( false ), pGameInfo( pGameInfo ), scoreShown( 0 ), scoreDiff( 0.0f ), targetShown( 0 ),
		  targetDiff( 0.0f ), doingUpdateAnimation( false )
{
	// Inicializa os vetores do objeto
	memset( tryResultV, 0, sizeof( RenderableImage* ) * FOUL_CHALLENGE_TRIES_PER_FOUL );
	memset( tryResultX, 0, sizeof( RenderableImage* ) * FOUL_CHALLENGE_TRIES_PER_FOUL );
	
	build();
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/
/*
InfoTab::~InfoTab( void )
{
	clear();
}

/*===========================================================================================
 
MÉTODO clear
	Limpa as variáveis do objeto.

============================================================================================*/
/*
void InfoTab::clear( void )
{
	memset( tryResultV, 0, sizeof( RenderableImage* ) * FOUL_CHALLENGE_TRIES_PER_FOUL );
	memset( tryResultX, 0, sizeof( RenderableImage* ) * FOUL_CHALLENGE_TRIES_PER_FOUL );
}

/*===========================================================================================
 
MÉTODO checkCloseButtonCollision
	Indica se houve colisão entre o toque e o botão de "Fechar".

============================================================================================*/
/*
bool InfoTab::checkCloseButtonCollision( const Point3f* pTouchPos )
{
	RenderableImage* pCloseTag = static_cast< RenderableImage* >( getObject( INFO_TAB_OBJ_INDEX_CLOSE_TAG ) );
	
	Point3f screenPos = *( getPosition()) + *( pCloseTag->getPosition() );
	return Utils::IsPointInsideRect( pTouchPos,screenPos.x, screenPos.y, pCloseTag->getWidth(), pCloseTag->getHeight() );
}

/*===========================================================================================
 
MÉTODO setCloseButtonState
	Indica qual o estado do botão de "Fechar".

============================================================================================*/
/*
void InfoTab::setCloseButtonState( bool pressed, const Color& color )
{
	static_cast< RenderableImage* >( getObject( INFO_TAB_OBJ_INDEX_CLOSE_TAG ) )->setVertexSetColor( color );
	closeButtonPressed = pressed;
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exebição do controle.

============================================================================================*/
/*
void InfoTab::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		setActive( true );
		setVisible( true );
	}
	else
	{
		setPosition( SCREEN_WIDTH, getPosition()->y );
		
		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/
/*
void InfoTab::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
	{
		// Garante que o controle está escondido
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
		
		// Inicia a animação de entrar na tela
		goingLeft = true;
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
		
		[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING Looping: false];
	}
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/
/*
void InfoTab::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}

/*===========================================================================================
 
MÉTODO startUpdateAnimation
	Faz a animação de rolagem dos números.

============================================================================================*/
/*
void InfoTab::startUpdateAnimation( void )
{
	doingUpdateAnimation = true;

	updateScore( false );
	updateTries();
}
/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/
/*
bool InfoTab::render( void )
{	
	if( !isVisible() )
		return false;
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	// TEST: Sem imagens. Fica serrilhado pois não há anti-aliasing
//	glTranslatef( position.x, position.y, position.z );
//	
//	glEnable( GL_BLEND );
//	glColor4ub( 0, 0, 0, 179 );
//	
//	Quad aux;
//	aux.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 0.0f, 0.0f, 0.0f );
//	aux.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( 73.0f, 0.0f, 0.0f );
//	aux.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 138.0f, 320.0f, 0.0f );
//	aux.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( 209.0f, 320.0f, 0.0f );
//	aux.render();
//	
//	glDisable( GL_BLEND );
//	
//	glColor4ub( 1, 100, 75, 255 );
//	
//	aux.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 70.0f, 0.0f, 0.0f );
//	aux.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( 314.0f, 0.0f, 0.0f );
//	aux.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 206.0f, 320.0f, 0.0f );
//	aux.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( 314.0f, 320.0f, 0.0f );
//	aux.render();

	// TEST: Imagem orignal + 1 quad. Fica perfeito, mas ocupa bastante memória para o que é
	glTranslatef( position.x + INFO_TAB_BACK_IMG_FRAME_WIDTH, position.y, position.z );
	glColor4ub( 1, 100, 75, 255 );

	Quad aux;
	aux.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 0.0f, 0.0f, 0.0f );
	aux.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( INFO_TAB_BACK_QUAD_WIDTH, 0.0f, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 0.0f, INFO_TAB_BACK_IMG_FRAME_HEIGHT, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( INFO_TAB_BACK_QUAD_WIDTH, INFO_TAB_BACK_IMG_FRAME_HEIGHT, 0.0f );
	aux.render();
	
	glPopMatrix();

	ObjectGroup::render();
	
	return true;
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/
/*
bool InfoTab::update( float timeElapsed )
{
	if( !ObjectGroup::update( timeElapsed ) )
		return false;

	float movement = ( ( timeElapsed * ( getWidth() + INFO_TAB_ANIM_EXTRA_X ) ) / INFO_TAB_ANIM_DURATION );

	switch( getInterfaceControlState() )
	{
		case INTERFACE_CONTROL_STATE_SHOWING:
			{
				float nextPosX = getPosition()->x - movement;
				float finalPosX = SCREEN_WIDTH - getWidth();
			
				if( nextPosX <= finalPosX )
				{
					nextPosX = finalPosX;
					
					if( goingLeft )
					{
						goingLeft = false;
					}
					else
					{
						nextPosX += INFO_TAB_ANIM_EXTRA_X;
						setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
					}
				}
				setPosition( nextPosX, getPosition()->y );
			}
			break;
			
		case INTERFACE_CONTROL_STATE_HIDING:
			{
				float nextPosX = getPosition()->x + movement;
				float finalPosX = SCREEN_WIDTH;
				
				if( nextPosX >= finalPosX )
				{
					nextPosX = finalPosX;
					setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
				}
				setPosition( nextPosX, getPosition()->y );
			}
			break;
			
		case INTERFACE_CONTROL_STATE_SHOWN:
			#if !( DEBUG && IS_CURR_TEST( TEST_POINTS_ANIM ) )
			if( doingUpdateAnimation )
			#endif
				updateScoreLabel( timeElapsed );
			break;
	}
	
	return true;
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/
/*
void InfoTab::build( void )
{
	{  // Evita erros de compilação por causa dos gotos

		// Aloca os objetos do grupo
		char path[] = { 'c', 't', 'i', ' ', '\0' };
		
		path[3] = 'b';
		
		#if CONFIG_TEXTURES_16_BIT
			RenderableImage* pBack = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
		#else
			RenderableImage* pBack = new RenderableImage( path );
		#endif

		CHECKED_GROUP_INSERT( this, pBack );
		
		path[3] = 't';
		#if CONFIG_TEXTURES_16_BIT
			RenderableImage* pInfoTag = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
		#else
			RenderableImage* pInfoTag = new RenderableImage( path );
		#endif

		CHECKED_GROUP_INSERT( this, pInfoTag );
		
		path[3] = 'c';
		#if CONFIG_TEXTURES_16_BIT
			RenderableImage* pCloseTag = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
		#else
			RenderableImage* pCloseTag = new RenderableImage( path );
		#endif

		CHECKED_GROUP_INSERT( this, pCloseTag );
		
		path[3] = 'f';
		#if CONFIG_TEXTURES_16_BIT
			RenderableImage* pScore = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
		#else
			RenderableImage* pScore = new RenderableImage( path );
		#endif

		CHECKED_GROUP_INSERT( this, pScore );
		
		// Configura as imagens
		
		// OLD
//		TextureFrame frame( 0.0f, 0.0f, INFO_TAB_BACK_IMG_FRAME_WIDTH, INFO_TAB_BACK_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
//		pBack->setImageFrame( frame );

		// TEST: Utiliza uma imagem pequena e dá resize para ficar do tamanho correto. GL_LINEAR está cagando a textura...
		TextureFrame frame( 1.0f, 1.0f, 127.0f, 196.0f, 0.0f, 0.0f );
		pBack->setImageFrame( frame );
		pBack->setScale( INFO_TAB_BACK_IMG_FRAME_WIDTH / 127.0f, INFO_TAB_BACK_IMG_FRAME_HEIGHT / 196.0f );
		
		frame.set( 0.0f, 0.0f, INFO_TAB_INFO_TAG_IMG_FRAME_WIDTH, INFO_TAB_INFO_TAG_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pInfoTag->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, INFO_TAB_CLOSE_TAG_IMG_FRAME_WIDTH, INFO_TAB_CLOSE_TAG_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pCloseTag->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, INFO_TAB_SCORE_IMG_FRAME_WIDTH, INFO_TAB_SCORE_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pScore->setImageFrame( frame );
		
		// Determina o tamanho do grupo
		
		// OLD
		//setSize( INFO_TAB_BACK_IMG_FRAME_WIDTH, INFO_TAB_BACK_IMG_FRAME_HEIGHT );
		
		setSize( INFO_TAB_BACK_IMG_FRAME_WIDTH + INFO_TAB_BACK_QUAD_WIDTH, INFO_TAB_BACK_IMG_FRAME_HEIGHT );
		
		// Posiciona as imagens no grupo
		//pBack->setPosition( 0.0f, 0.0f );
		pInfoTag->setPosition( INFO_TAB_INFO_TAG_DIST_FROM_LEFT, INFO_TAB_INFO_TAG_DIST_FROM_TOP );
		pCloseTag->setPosition( getWidth() - pCloseTag->getWidth() + INFO_TAB_CLOSE_TAG_DIST_FROM_RIGHT, getHeight() - pCloseTag->getHeight() - INFO_TAB_CLOSE_TAG_DIST_FROM_BOTTOM );
		pScore->setPosition( getWidth() - pScore->getWidth() + 25.0f, INFO_TAB_SCORE_DIST_FROM_TOP );

		// Labels
		Font* pInfoTabFont = [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_MAIN];
		Point3f scorePos = *( pScore->getPosition() );

		Label* pLbTotalPoints = new Label( pInfoTabFont, false );
		CHECKED_GROUP_INSERT( this, pLbTotalPoints );

		pLbTotalPoints->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pLbTotalPoints->setCharSpacement( INFO_TAB_FONT_CHAR_SPACEMENT );
		pLbTotalPoints->setPosition( scorePos.x + INFO_TAB_LB_TOTAL_POINTS_POS_X, scorePos.y + INFO_TAB_LB_TOTAL_POINTS_POS_Y );
		pLbTotalPoints->setSize( INFO_TAB_LB_TOTAL_POINTS_WIDTH, INFO_TAB_LB_TOTAL_POINTS_HEIGHT );
		
		Label* pLbCurrLevel = new Label( pInfoTabFont, false );
		CHECKED_GROUP_INSERT( this, pLbCurrLevel );
		
		pLbCurrLevel->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pLbTotalPoints->setCharSpacement( INFO_TAB_FONT_CHAR_SPACEMENT );
		pLbCurrLevel->setPosition( scorePos.x + INFO_TAB_LB_CURR_LEVEL_POS_X, scorePos.y + INFO_TAB_LB_CURR_LEVEL_POS_Y );
		pLbCurrLevel->setSize( INFO_TAB_LB_CURR_LEVEL_WIDTH, INFO_TAB_LB_CURR_LEVEL_HEIGHT );
		
		Label* pLbScoreGoal = new Label( pInfoTabFont, false );
		CHECKED_GROUP_INSERT( this, pLbScoreGoal );

		pLbScoreGoal->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pLbTotalPoints->setCharSpacement( INFO_TAB_FONT_CHAR_SPACEMENT );
		pLbScoreGoal->setPosition( scorePos.x + INFO_TAB_LB_SCORE_GOAL_POS_X, scorePos.y + INFO_TAB_LB_SCORE_GOAL_POS_Y );
		pLbScoreGoal->setSize( INFO_TAB_LB_SCORE_GOAL_WIDTH, INFO_TAB_LB_SCORE_GOAL_HEIGHT );
		
		// Aloca e configura as imagen que exibem o resultado de cada tentativa
		#if CONFIG_TEXTURES_16_BIT
			// Reduzir estas duas imagens para 16 bits estava quebrando-as
			path[3] = 'v';
			tryResultV[ 0 ] = new RenderableImage( path/*, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D*/ //);
	/*		CHECKED_GROUP_INSERT( this, tryResultV[ 0 ] );

			path[3] = 'x';
			tryResultX[ 0 ] = new RenderableImage( path/*, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D*/ //);
	/*		CHECKED_GROUP_INSERT( this, tryResultX[ 0 ] );
		#else
			path[3] = 'v';
			tryResultV[ 0 ] = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, tryResultV[ 0 ] );

			path[3] = 'x';
			tryResultX[ 0 ] = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, tryResultX[ 0 ] );
		#endif

		tryResultV[ 0 ]->setPosition( scorePos.x + INFO_TAB_FIRST_TRY_ICON_POS_X + INFO_TAB_TRY_RESULT_V_OFFSET_X, scorePos.y + INFO_TAB_FIRST_TRY_ICON_POS_Y + INFO_TAB_TRY_RESULT_V_OFFSET_Y );
		tryResultX[ 0 ]->setPosition( scorePos.x + INFO_TAB_FIRST_TRY_ICON_POS_X + INFO_TAB_TRY_RESULT_X_OFFSET_X, scorePos.y + INFO_TAB_FIRST_TRY_ICON_POS_Y + INFO_TAB_TRY_RESULT_X_OFFSET_Y );

		for( uint8 i = 1 ; i < FOUL_CHALLENGE_TRIES_PER_FOUL ; ++i )
		{
			tryResultV[ i ] = new RenderableImage( tryResultV[ 0 ] );
			CHECKED_GROUP_INSERT( this, tryResultV[ i ] );
			tryResultV[ i ]->setPosition( tryResultV[ 0 ]->getPosition() );
			tryResultV[ i ]->move( i * ( INFO_TAB_TRIES_ICONS_WIDTH + INFO_TAB_TRIES_ICONS_DIST_X ), 0.0f );

			tryResultX[ i ] = new RenderableImage( tryResultX[ 0 ] );
			CHECKED_GROUP_INSERT( this, tryResultX[ i ] );
			tryResultX[ i ]->setPosition( tryResultX[ 0 ]->getPosition() );
			tryResultX[ i ]->move( i * ( INFO_TAB_TRIES_ICONS_DIST_X + INFO_TAB_TRIES_ICONS_WIDTH ), 0.0f );
		}
		
		// Atualiza o texto dos labels
		updateScoreGoal();
		updateTries();
		updateTotalPoints();
		updateLevel();

		return;
	
	}  // Evita erros de compilação por causa dos gotos

	// Label de tratamento de erros
	Error:
		clear();

	#if DEBUG
		throw ConstructorException( "InfoTab::build( void ): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*===========================================================================================
 
MÉTODO updateLevel
	Atualiza a informação "Fase Atual".

============================================================================================*/
/*
void InfoTab::updateLevel( void )
{
	char buffer[ INFO_TAB_LABELS_BUFFER_LEN ];
	
	snprintf( buffer, INFO_TAB_LABELS_BUFFER_LEN, "%d", static_cast< int32 >( MAX_LEVEL ) );
	int32 maxLevelLen = strlen( buffer );
	
	// 0* => Garante que, no mínimo, pGameInfo->currLevel será impresso com 'maxLevelLen' dígitos, preenchendo números menores com 0s à esquerda
	int32 printableLevel = pGameInfo->currLevel + 1;
	snprintf( buffer, INFO_TAB_LABELS_BUFFER_LEN, "%0*d", maxLevelLen, printableLevel > static_cast< int32 >( MAX_LEVEL ) ? static_cast< int32 >( MAX_LEVEL ) : printableLevel );

	static_cast< Label* >( getObject( INFO_TAB_OBJ_INDEX_LB_CURR_LEVEL ) )->setText( buffer, false, false );
}

/*===========================================================================================
 
MÉTODO updateTotalPoints
	Atualiza a informação "Total de Pontos".

============================================================================================*/
/*
void InfoTab::updateTotalPoints( void )
{
	char aux[ INFO_TAB_LABELS_BUFFER_LEN ];
	char text[ INFO_TAB_LABELS_BUFFER_LEN ];

	snprintf( aux, INFO_TAB_LABELS_BUFFER_LEN, "%d", static_cast< int32 >( SCORE_MAX ) );
	int32 maxPointsLen = strlen( aux );

	snprintf( aux, INFO_TAB_LABELS_BUFFER_LEN, "%0*d", maxPointsLen, static_cast< uint32 >( scoreShown > SCORE_MAX ? SCORE_MAX : scoreShown ) );
	
	int8 currPointsLen = strlen( aux );
	int8 finalLen = currPointsLen + ( currPointsLen <= 0 ? 0 : ( currPointsLen - 1 ) / 3 );
	
	for( int8 i = currPointsLen - 1, currChar = 0 ; i >= 0 ; ++currChar )
	{
		// &3 == %4
		text[ finalLen - currChar - 1 ] = (( currChar + 1 ) & 3 ) == 0 ? '.' : aux[ i-- ];
	}
	text[ finalLen ] = '\0';
	
	static_cast< Label* >( getObject( INFO_TAB_OBJ_INDEX_LB_TOTAL_POINTS ) )->setText( text, false, false );
}

/*===========================================================================================
 
MÉTODO updateTries
	Atualiza o resultado das tentativas.

============================================================================================*/
/*
void InfoTab::updateTries( void )
{
	for( uint8 i = 0 ; i < FOUL_CHALLENGE_TRIES_PER_FOUL ; ++i )
	{
		switch( pGameInfo->tries[ i ] )
		{
			case TRY_RESULT_NOT_USED:
				tryResultV[ i ]->setVisible( false );
				tryResultX[ i ]->setVisible( false );
				break;

			case TRY_RESULT_GOAL:
				tryResultV[ i ]->setVisible( true );
				tryResultX[ i ]->setVisible( false );
				break;

			case TRY_RESULT_OUT:
				tryResultV[ i ]->setVisible( false );
				tryResultX[ i ]->setVisible( true );
				break;
		}
	}
}

/*===========================================================================================
 
MÉTODO updateScoreGoal
	Atualiza a informação pontuação ganha / pontuação necessária.

============================================================================================*/
/*
void InfoTab::updateScoreGoal( void )
{
	char buffer[ INFO_TAB_LABELS_BUFFER_LEN ];
	
	int32 currLevelTarget = GameUtils::GetLevelScoreTarget( pGameInfo->currLevel );
	snprintf( buffer, INFO_TAB_LABELS_BUFFER_LEN, "%d", currLevelTarget );
	
	int32 currLevelTargetLen = strlen( buffer );
	int32 targetShownCasted = static_cast< int32 >( targetShown );

	snprintf( buffer, INFO_TAB_LABELS_BUFFER_LEN, "%0*d / %d", currLevelTargetLen, targetShownCasted, currLevelTarget );
	
	Label* pLabel = static_cast< Label* >( getObject( INFO_TAB_OBJ_INDEX_LB_SCORE_GOAL ) );
	pLabel->setText( buffer, false, false );
	
	if( ( targetShownCasted > 0 ) && ( currLevelTarget > 0 ) && ( targetShownCasted >= currLevelTarget ) )
	{
		pLabel->setVertexSetColor( COLOR_SCORE_TARGET_HIT );
	}
	else
	{
		pLabel->setVertexSetColor( Color ( COLOR_WHITE ) );
	}
}

/*===========================================================================================
 
MÉTODO updateScore
	Modifica a pontuação exibida pela barra de pontos.

============================================================================================*/
/*
void InfoTab::updateScore( bool immediately )
{
	if( immediately )
	{
		scoreShown = pGameInfo->points;
		updateTotalPoints();

		targetShown = pGameInfo->levelScore;
		updateScoreGoal();
		
		scoreDiff = 0.0f;
		targetDiff = 0.0f;
	}
	else
	{
		scoreDiff = ( pGameInfo->points - scoreShown );
		targetDiff = ( pGameInfo->levelScore - targetShown );
	}
}

/*===========================================================================================
 
MÉTODO updateScoreLabel
	Faz a animação de rolagem dos pontos.

============================================================================================*/
/*
bool InfoTab::updateScoreLabel( float timeElapsed )
{
	bool ret = false;

	if( scoreDiff && ( static_cast< uint32 >( scoreShown ) < pGameInfo->points ) )
	{
		ret = true;
		
		// Se for demorar mais do que INFO_TAB_SCORE_MIN_SPEED, aumenta a velocidade
		float currSpeed = INFO_TAB_SCORE_ANIM_MAX_DUR / scoreDiff;

		if( currSpeed > INFO_TAB_SCORE_ANIM_MIN_SPEED )
			scoreShown += timeElapsed / INFO_TAB_SCORE_ANIM_MIN_SPEED;
		else
			scoreShown += ( scoreDiff * timeElapsed ) / INFO_TAB_SCORE_ANIM_MAX_DUR;

		if( scoreShown >= pGameInfo->points )
		{
			scoreShown = pGameInfo->points;
			scoreDiff = 0.0f;
		}

		updateTotalPoints();
	}

	if( targetDiff && ( static_cast< int32 >( targetShown ) < pGameInfo->levelScore ) )
	{
		ret = true;
		
		// Se for demorar mais do que INFO_TAB_SCORE_MIN_SPEED, aumenta a velocidade
		float currSpeed = INFO_TAB_SCORE_ANIM_MAX_DUR / targetDiff;
		
		if( currSpeed > INFO_TAB_SCORE_ANIM_MIN_SPEED )
			targetShown += timeElapsed / INFO_TAB_SCORE_ANIM_MIN_SPEED;
		else
			targetShown += ( targetDiff * timeElapsed ) / INFO_TAB_SCORE_ANIM_MAX_DUR;

		if( targetShown >= pGameInfo->levelScore )
		{
			targetShown = pGameInfo->levelScore;
			targetDiff = 0.0f;
		}
		
		updateScoreGoal();
	}
	
	doingUpdateAnimation = ret;

	return ret;
}
*/