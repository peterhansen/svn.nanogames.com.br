/*
 *  LifeBar.h
 *  dotGame
 *
 *  Created by Max on 5/13/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef LIFE_BAR_H
#define LIFE_BAR_H 1

#include "RenderableImage.h"
#include "GameBaseInfo.h"	

class Color;

class LifeBar : public RenderableImage {
public:
	LifeBar( GameBaseInfo *gInfo, const char* pImagePath );
	LifeBar( GameBaseInfo *gInfo, const RenderableImage *r);	
	
	virtual ~LifeBar();
	
//	virtual	bool update( float timeElapsed );
	
	virtual	bool render();
	
	//atualiza o tamanho da barra
	void updateSizeBar( Player *pl );
	//retorna o percentil do jogador que esta ganhando
	float getPercentil( uint8 *idPlayerWinnig = NULL );
	
	float getPercentilByPlayer( uint8 idPlayer );

	void swapSlices( void );

	
private:
	bool buildLifeBar( GameBaseInfo *gInfo/*, const char* pImagePath */);
	Color plColors[ MAX_PLAYERS ];
	uint16 halfSlices, totalSlices, plSlices[ MAX_PLAYERS ];//, p2Slices;
	float sliceSize;
	
};

#endif