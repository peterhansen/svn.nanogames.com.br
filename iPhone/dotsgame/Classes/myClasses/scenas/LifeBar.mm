/*
 *  LifeBar.mm
 *  dotGame
 *
 *  Created by Max on 5/13/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "LifeBar.h"
#include "CharacterFactory.h"
#include "Exceptions.h"
#include "Quad.h"
#include "Player.h"

#define LIFE_BAR_FRAME_WIDTH 222.0f
#define LIFE_BAR_FRAME_HEIGTH 9.0f

LifeBar::LifeBar( GameBaseInfo *gInfo, const char* pImagePath ):RenderableImage( pImagePath ){
	if( !buildLifeBar( gInfo )  ){
#if DEBUG
		throw ConstructorException( "LifeBar não construida" );
#else
		throw ConstructorException();
#endif
	}
}


LifeBar::LifeBar( GameBaseInfo *gInfo, const RenderableImage *r ):RenderableImage( r ){

	if( !buildLifeBar( gInfo )  ){
#if DEBUG
		throw ConstructorException( "LifeBar não construida" );
#else
		throw ConstructorException();
#endif
	}

}
	
LifeBar::~LifeBar(){
#if DEBUG
	LOG( "\t\t\tdestruindo life bar\n" );
#endif
	//SAFE_DELETE( image );
} 
	
//bool LifeBar::update( float timeElapsed ){}

bool LifeBar::render(){
	if( !isVisible() )
		return false;
	
	bool ret = false;
	
	ret |= RenderableImage::render();
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glTranslatef( position.x, position.y, position.z );

	Quad q[ MAX_PLAYERS ];
	//player 1	
	q[ 0 ].quadVertexes[ QUAD_TOP_LEFT ].setPosition( 0.0, 0.0, 0.0);
	q[ 0 ].quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 3.0, LIFE_BAR_FRAME_HEIGTH, 0.0 );
	
	q[ 0 ].quadVertexes[ QUAD_TOP_RIGHT ].setPosition( plSlices[ 0 ] * sliceSize, 0.0, 0.0 );
	q[ 0 ].quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( plSlices[ 0 ] * sliceSize, LIFE_BAR_FRAME_HEIGTH, 0.0 );	

	//player 2													 
	q[ 1 ].quadVertexes[ QUAD_TOP_LEFT ].setPosition( plSlices[ 0 ] * sliceSize, 0.0, 0.0 );
	q[ 1 ].quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( plSlices[ 0 ] * sliceSize, LIFE_BAR_FRAME_HEIGTH , 0.0 );

	q[ 1 ].quadVertexes[ QUAD_TOP_RIGHT ].setPosition( LIFE_BAR_FRAME_WIDTH , 0.0, 0.0 );
	q[ 1 ].quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( LIFE_BAR_FRAME_WIDTH ,LIFE_BAR_FRAME_HEIGTH, 0.0 );

	for( uint8 i = 0 ; i < MAX_PLAYERS ;i++ ){
		glPushMatrix();
		glColor4ub(plColors[i].getUbR(), plColors[i].getUbG(), plColors[i].getUbB(), 125);
		q[ i ].render();		
		glPopMatrix();	
	}
	
	glPopMatrix();
	return ret;
}

bool LifeBar::buildLifeBar( GameBaseInfo *gInfo/*, const char* pImagePath */){
	bool ret = false;
	halfSlices = gInfo->nHZones * gInfo->nVZones;
	
	Color c;
	
	for (uint8 i = 0 ; i < MAX_PLAYERS; i++) {
		CharacterFactory::GetInstance()->getColorByCharacter( gInfo->iddolls[i], &c );
		plColors[ i ] = c;
		plSlices[ i ] = halfSlices;
	}
	totalSlices = halfSlices * 2;
	
	sliceSize = /*size.x*/LIFE_BAR_FRAME_WIDTH / totalSlices;
	TextureFrame t( 0.0, 0.0, LIFE_BAR_FRAME_WIDTH , LIFE_BAR_FRAME_HEIGTH , 0.0, 0.0 );

	RenderableImage::setImageFrame( t );
	ret = true;
		
	return ret;
}
/*===============================================================
 atualiza o tamanho da barra->onde pl é o jogador que marcou um quad/zona
 
=====================================================================*/
void LifeBar::updateSizeBar( Player *pl ){
	//sim eu sei, não está para Nplayers, mas a própria interface em si meio que não permite isso...
	//uint8 another = ( pl->getId() + 1 >= MAX_PLAYERS? 0 : pl->getId() + 1 );
	
//	plSlices[ pl->getId() ] = halfSlices + pl->getScore();
//
//	plSlices[ another ] = totalSlices - plSlices[ pl->getId() ];
//	plSlices[ another ] -= plSlices[ pl->getId() ];// pl->getScore();//plSlices[ pl->getId() ];
	
	if( pl->getId() == 0 ){
		plSlices[ 0 ] += 1;
		plSlices[ 1 ] -= 1;
	}else {
		plSlices[ 1 ] += 1;
		plSlices[ 0 ] -= 1;		
	}
	
#if DEBUG
	LOG( "fatias jogador 0: %d, jogador 1: %d, nfatias:%d \n", plSlices[0] ,plSlices[1],totalSlices );
#endif
}

float LifeBar::getPercentil( uint8 *idPlayerWinnig ){

	if( plSlices[ 0 ] > plSlices[ 1 ] ){
		if( idPlayerWinnig )
			*idPlayerWinnig = 0;
		return ( plSlices[ 0 ] / totalSlices  ) * 100;
		
	}else if( plSlices[ 0 ] < plSlices[ 1 ] ){
		if( idPlayerWinnig )
			*idPlayerWinnig = 1;
		return ( plSlices[ 1 ] / totalSlices  ) * 100;		
	}
	
	//se chegou até aqui, é pq o jogo está empatado, logo, não há um jogador vencedor e o percentil é igual
	if( idPlayerWinnig )
		*idPlayerWinnig = -1;
	
	return 50.0f;
}

float LifeBar::getPercentilByPlayer( uint8 idPlayer ){
	float f = ( ( float )plSlices[ idPlayer ] / ( float )totalSlices ) * 100.0f;
#if DEBUG
	printf("%f percentil do player %d\n",f,idPlayer);
#endif
	return f;

}
void LifeBar::swapSlices( void ){
	uint16 aux;
	aux = plSlices[0];
	plSlices[0] = plSlices[1];
	plSlices[1] = aux;
}