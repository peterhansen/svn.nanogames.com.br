/*
 *  FeedbackTxtMsg.mm
 *  dotGame
 *
 *  Created by Max on 5/31/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "FeedbackTxtMsg.h" 
#include "ObjcMacros.h"
#include "GLHeaders.h"

// Durações das animações
#define FEEDBACK_MSG_ANIM_VIEWPORT_DUR	0.500f
#define FEEDBACK_MSG_ANIM_BOUNCE_DUR	0.400f
#define FEEDBACK_MSG_ANIM_FADE_DUR		0.200f

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

FeedbackTxtMsg::FeedbackTxtMsg(InterfaceControlListener* pListener, const char* pFontImgsId, const char* pFontDescriptor, bool manageFont ):
				Label(pFontImgsId, pFontDescriptor, manageFont),
			  InterfaceControl( pListener ),
			  currAnimType( FEEDBACK_TXT_ANIM_NONE ),
			  speedY( 0.0f ),
			  acumWidth( 0.0f )
{
	//setImageFrame( *pImgFrame );
}
FeedbackTxtMsg::FeedbackTxtMsg(InterfaceControlListener* pListener , Font* pFont, bool manageFont ):	Label(pFont, manageFont),
InterfaceControl( pListener ),
currAnimType( FEEDBACK_TXT_ANIM_NONE ),
speedY( 0.0f ),
acumWidth( 0.0f )

{
	Label::setTextAlignment(  ALIGN_HOR_CENTER | ALIGN_VER_CENTER  );
	//setImageFrame( *pImgFrame );
}

/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

FeedbackTxtMsg::~FeedbackTxtMsg( void )
{
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/

void FeedbackTxtMsg::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
	{
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
		
		switch( currAnimType )
		{
		/*	case FEEDBACK_MSG_ANIM_FADE:
				{
					Color transparentWhite( 1.0f, 1.0f, 1.0f, 0.0f );
					setVertexSetColor( transparentWhite );
				}
				break;*/
				
			case FEEDBACK_TXT_ANIM_VIEWPORT:
				{
					acumWidth = 0.0f;

					Point3f currPos = *( getPosition() );
					setViewport( currPos.x, currPos.y, 0.0f, getHeight() );
				}
				break;
				
			case FEEDBACK_TXT_ANIM_BOUNCE:
				setPosition( getPosition()->x, -getHeight() );
				break;
		}
	}
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/

void FeedbackTxtMsg::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}
	
/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool FeedbackTxtMsg::update( float timeElapsed )
{
//	if( Label::update( timeElapsed ) );//RenderableImage::update( timeElapsed ) )
//		return false;
	
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) )
		return false;

	switch( currAnimType )
	{
		case FEEDBACK_TXT_ANIM_VIEWPORT:
			animViewport( timeElapsed );
			break;
			
		case FEEDBACK_TXT_ANIM_BOUNCE:
			animBounce( timeElapsed );
			break;
			
		/*case FEEDBACK_TXT_ANIM_FADE:
			animFade( timeElapsed );
			break;*/
			
		case FEEDBACK_TXT_ANIM_NONE:
			setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
			break;
	}
	return true;
}

/*===========================================================================================
 
MÉTODO animViewport
	Determina o tipo de animações utilizado pelo controle.

============================================================================================*/

void FeedbackTxtMsg::setCurrAnimType( FeedbackTxtAnimType animType )
{
	currAnimType = animType;
	
	/*if( animType == FEEDBACK_TXT_ANIM_FADE )
		setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );*/
}

/*===========================================================================================
 
MÉTODO animViewport
	Faz as animações de esconder / exibir a través do viewport.

============================================================================================*/

void FeedbackTxtMsg::animViewport( float timeElapsed )
{
	float width = getWidth();
	Viewport viewport = getViewport();
	
	if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWING )
	{
		acumWidth += ( timeElapsed * width ) / FEEDBACK_MSG_ANIM_VIEWPORT_DUR;

		viewport.width = static_cast< int32 >( acumWidth );
		
		if( viewport.width >= width )
		{
			viewport.width = width;
			setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
		}
	}
	else if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDING )
	{
		viewport.width -= ( timeElapsed * width ) / FEEDBACK_MSG_ANIM_VIEWPORT_DUR;
		
		if( viewport.width <= 0.0f )
		{
			viewport.width = 0.0f;
			setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
		}
	}

	setViewport( viewport );
}

/*===========================================================================================
 
MÉTODO animBounce
	Faz as animações de entrar na tela quicando.

============================================================================================*/

// TODOO : No update, fazer quicar de fato
void FeedbackTxtMsg::animBounce( float timeElapsed )
{
	Point3f currPos = *( getPosition() );

	if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWING )
	{
		float finalY = ( SCREEN_HEIGHT - getHeight() ) * 0.5f;
		
		currPos.y += ( timeElapsed * ( finalY + getHeight() ) ) / FEEDBACK_MSG_ANIM_BOUNCE_DUR;

		if( currPos.y >= finalY )
		{
			currPos.y = finalY;
			setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
		}
	}
	else if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDING )
	{
		float finalY = -getHeight();
		
		currPos.y -= ( timeElapsed * ( ( ( SCREEN_HEIGHT - getHeight() ) * 0.5f ) + getHeight() ) ) / FEEDBACK_MSG_ANIM_BOUNCE_DUR;
		
		if( currPos.y <= finalY )
		{
			currPos.y = finalY;
			setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
		}
	}
	
	setPosition( &currPos );
}

/*===========================================================================================
 
MÉTODO animFade
	Faz as animações de fade in / fade out.

============================================================================================*/

void FeedbackTxtMsg::animFade( float timeElapsed )
{
	Color c;
	getVertexSetColor( &c );
	
	if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWING )
	{
		c.setFloatA( c.getFloatA() + timeElapsed / FEEDBACK_MSG_ANIM_FADE_DUR );
		
		if( c.getFloatA() >= 1.0f )
		{
			c.setFloatA( 1.0f );
			setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
		}
	}
	else if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDING )
	{
		c.setFloatA( c.getFloatA() - timeElapsed / FEEDBACK_MSG_ANIM_FADE_DUR );//		c.a -= timeElapsed / FEEDBACK_MSG_ANIM_FADE_DUR;
		
		if( c.getFloatA() <= 0.0f )
			setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
	}

	setVertexSetColor( c );
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exibição do controle.

============================================================================================*/

void FeedbackTxtMsg::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		setActive( true );
		setVisible( true );
	}
	else
	{
		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}
