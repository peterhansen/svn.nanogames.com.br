/*
 *  ButtonsGroup.h
 *  dotGame
 *
 *  Created by Max on 10/22/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef BUTTONS_GROUP_H
#define BUTTONS_GROUP_H 1
#include "ObjectGroup.h"
#include "OrthoCamera.h"
#include "Color.h"
/*
 função da classe é mais gerenciar os botões de controle, facilitando a desalocação, renderização, update, blá,blá,blá, mas depois acabou ficando com a fç de renderizar tudo que possua uma câmera diferente da principal
 */


class ButtonsGroup : public ObjectGroup {
	
public:
	
	ButtonsGroup( OrthoCamera *p );
	
	virtual bool render( void );
	
	//retorna o botão pressionado, sendo que ele não tem haver com a barra de informação
	bool idButtonPressed( const Point3f *p, uint8 *indiceB  = NULL);
	
private:
	OrthoCamera * pCamera;
	// Clear bits
	//uint32 clearBits;
	
	// Valor utilizado para limpar o buffer de profundidade
	//float clearDepth;
	
	// Valor utilizado para limpar o stencil buffer
	//int32 clearStencil;
	
	// Cor utilizada para limpar o buffer de cor
	//Color clearColor;
	
};



#endif