/*
 *  Button.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/12/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BUTTON_H
#define BUTTON_H 1

#include "RenderableImage.h"
#include "InterfaceControl.h"
//#include "TouchZone.h"
// Forward Declarations
class InterfaceControlListener;

class Button : public RenderableImage, public InterfaceControl
{
public:
	
		Button( const char* pPath, InterfaceControlListener* pListener );
	
		// Destrutor
		virtual ~Button( void );
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
//<MN>
	
//	TouchZone collisonMask;
//	</MN>
};

#endif
