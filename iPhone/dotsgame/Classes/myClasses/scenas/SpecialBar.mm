/*
 *  SpecialBar.mm
 *  dotGame
 *
 *  Created by Max on 5/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "SpecialBar.h"
#include "FreeKickAppDelegate.h"
RenderableImage* SpecialBar::baseImage = NULL;

#define SIZE_FRAME_SPECIAL_BAR_X 94.0F
#define SIZE_FRAME_SPECIAL_BAR_Y 12.0F
//#define SIZE_FRAME_SPECIAL_BAR_ADJUST_X 80.0F


SpecialBar::SpecialBar( bool orient, uint8 _idPlayer, uint8 _nSlices ):RenderableImage( baseImage ), idPlayer( _idPlayer ), currLevel( 0 ),nSlices( _nSlices ), sliceSize( SIZE_FRAME_SPECIAL_BAR_X / nSlices ),isPlayer2( orient ) {
	
	baseFrame.set( 0.0,0.0,SIZE_FRAME_SPECIAL_BAR_X,SIZE_FRAME_SPECIAL_BAR_Y,0.0,0.0 );
	if( orient ){
		mirror( MIRROR_HOR );
		setViewport( baseFrame.width, position.y, size.x , size.y  );
		position.x -= SIZE_FRAME_SPECIAL_BAR_ADJUST_X;
	}else 
		setViewport( position.x, position.y, 0 , size.y );
	

}
	
bool SpecialBar::updateSize( ){
	bool ret = false;
	currLevel =( ( currLevel + 1 ) <= nSlices ? ++currLevel : 0  );
	if( currLevel == 0 )
		ret = true;
	float viewWidth ; 
	if( isPlayer2 ){
		viewWidth =size.x;
		viewWidth = ( position.x + baseFrame.width ) - ( currLevel * sliceSize );
		setViewport( viewWidth, position.y, size.x , size.y );
	}else{
		viewWidth = 0.0;	
		viewWidth = currLevel * sliceSize;
		setViewport( position.x, position.y, viewWidth , size.y );
	}
	return ret;
}
	
void SpecialBar::unLoadImages( void ){
	SAFE_DELETE( baseImage );
}
	
bool SpecialBar::loadImages( void ){

	baseImage = new RenderableImage("bPow"/*, &t*/);
	return baseImage != NULL;
}
uint8 SpecialBar::getIdPlayer( void ){
	return idPlayer;
}

SpecialBar::~SpecialBar(){
	//SAFE_DELETE( levelText );
}

