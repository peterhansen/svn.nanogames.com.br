#include "Button.h"

// Duração das animações de fade in e fade out
#define FADE_ANIM_DUR 0.200f

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

Button::Button( const char* pPath, InterfaceControlListener* pListener )

#if CONFIG_TEXTURES_16_BIT
	   : RenderableImage( pPath ),//, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D ),
#else
	   : RenderableImage( pPath ),
#endif
		 InterfaceControl( pListener )
{
	setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}
	
/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

Button::~Button( void )
{
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/

void Button::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/

void Button::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool Button::update( float timeElapsed )
{
	if( !RenderableImage::update( timeElapsed ) )
		return false;

	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) )
		return false;
	
	Color c;
	getVertexSetColor( &c );
	
	if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWING )
	{
		c.setFloatA( c.getFloatA() + ( timeElapsed / FADE_ANIM_DUR ) );
		
		if( c.getFloatA() >= 1.0f )
		{
			c.setFloatA( 1.0f );
			setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
		}
	}
	else if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDING )
	{
		c.setFloatA( c.getFloatA() - ( timeElapsed / FADE_ANIM_DUR ) );
		
		if( c.getFloatA() <= 0.0f )
		{
			c.setFloatA( 0.0f );
			setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
		}
	}

	setVertexSetColor( c );
	
	return true;
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exebição do controle.

============================================================================================*/

void Button::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		if( state == INTERFACE_CONTROL_STATE_SHOWN )
		{
			Color c;
			getVertexSetColor( &c );
			c.setFloatA( 1.0f );
			setVertexSetColor( c );
		}
		setActive( true );
		setVisible( true );
	}
	else
	{
		Color c;
		getVertexSetColor( &c );
		c.setFloatA( 0.0f );
		setVertexSetColor( c );

		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}
