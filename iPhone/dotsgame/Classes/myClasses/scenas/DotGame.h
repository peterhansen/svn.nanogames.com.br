/*
 *  DotGame.h
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DOT_GAME_H
#define DOT_GAME_H 1
#include "Point2i.h"

#include "Scene.h"
//#include "OGLTransition.h"
#include "Sprite.h"
#include "InterfaceControl.h"
#include "Chronometer.h"

#include "AccelerometerListener.h"
#include "EventListener.h"
#include "Scene.h"
#include "Touch.h"

#include "AnimatedCameraListener.h"
#include "ChronometerListener.h"
#include "InterfaceControlListener.h"
//#include "OGLTransition.h"
//#include "CurtainTransition.h"
//#include "SliceTransition.h"
#include "OrthoCamera.h"
#include "CurtainLeftTransition.h"
//#include "ClosingDoorTransition.h"


#include "Config.h"
#include "ZoneGame.h"
#include "LineGame.h"
#include "PowerUp.h"

#include "Player.h"

#include "DotGameInfo.h"

#include "ZoneGroup.h"
#include "LineGroup.h"
#include "PowerUpGroup.h"

#include "GameBaseInfo.h"
#include "ButtonsGroup.h"
#include "InfoBar.h"
#include "LoadingView.h"
//#include "Background.h"
#include "Board.h"

#include <vector>

enum DotGameState {

	DOT_GAME_STATE_NONE = -1,
	DOT_GAME_STATE_PLAYER_WAITING = 0,
	DOT_GAME_STATE_AFTER_CPU_MOVE,
	DOT_GAME_STATE_AFTER_PLAYER_MOVE,
	DOT_GAME_STATE_ANIMATING_VICTORY,
	DOT_GAME_STATE_PAUSED,
	DOT_GAME_STATE_SHOWING_INFO_PLAYER,
	DOT_GAME_STATE_GAME_OVER,
	DOT_GAME_STATE_OBSERVING,
	DOT_GAME_STATE_ANIMATING_TRANSITION_1,
	DOT_GAME_STATE_ANIMATING_TRANSITION_2,
	DOT_GAME_STATE_CPU_PLAYING,
	DOT_GAME_STATE_FOCUSING_LINE,
	DOT_GAME_STATE_SHOWING_MSG_GAME_OVER,
	DOT_GAME_STATE_HIDING_MSG_GAME_OVER,
	DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER,
	DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER,
	DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN,
	DOT_GAME_STATE_SHOWING_MSG_CPU_WIN,
	DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN,
	DOT_GAME_STATE_HIDING_MSG_CPU_WIN,	
	DOT_GAME_STATE_ANIMATING_LINE,
	DOT_GAME_STATE_ANIMATING_ZONE,
	DOT_GAME_STATE_ANIMATING_BIG_ZONE,
	DOT_GAME_STATE_WAITING_POWER_UP_TARGET_LINE_INPUT,
	DOT_GAME_STATE_WAITING_POWER_UP_TARGET_ZONE_INPUT,
	DOT_GAME_STATE_ANIMATING_POWER_UP
/**NECESSÁRIO MAIS?	*/
	
}typedef DotGameState;

//base do jogo...
class DotGame : public Scene/*, public OGLTransitionListener*/, public SpriteListener, public InterfaceControlListener, public ChronometerListener, public AnimatedCameraListener,public EventListener, public PowerUpListener /*???*/ {

public:
	
	DotGame( GameBaseInfo *g, LoadingView* pLoadingView );


	virtual	~DotGame();
	void unpause(void);
	
	void pause( /*bool b*/ void );
	
	//retona o jogador atual
	Player* getCurrPlayer( void ){ return currPlayer; };
	
	//retorna o próximo jogador
	Player* getNextPLayer( void ){ return players[ ( indicePlayer + 1 >= nPlayers ? 0 : indicePlayer + 1 ) ]; };
	
	// Renderiza o objeto
	virtual bool render( void );
	
	// Atualiza o objeto
	virtual bool update( float timeElapsed );
	
	// Método que trata os eventos enviados pelo sistema
	virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
	
	//passa infoBar
	inline	InfoBar* getInfoBar( void ){ return pInfoBar; };
	
	// Chamado quando o sprite muda de etapa de animação
	virtual void onAnimStepChanged( Sprite* pSprite );
	
	// Chamado quando o sprite termina um sequência de animação
	virtual void onAnimEnded( Sprite* pSprite );
	
	//qdo o power up é liberado, este método é chamado
	virtual void PowerUpEffect( PowerUp* p );

	//retorna uma instância de dotgame para o powerUp
	virtual DotGame* getDotGameInstance( void );

	//set dotgame state
	void setDotGameState( DotGameState d );
	DotGameState getDotGameState( void );

	inline uint8 getLevel( void );
	
	//para saída da tela
	//indica se  o jogo está correndo
	bool isGoodbye( void );
	//método para sair do jogo
	void auRevoir();
	
	inline ZoneGroup* getZoneGroup( void ){ return zoneManager; };
	inline LineGroup* getLineGroup( void ){ return lineManager; };
	
	inline int8 getDispMoves( void ){ return dispMoves; };
	inline void setDispMoves( int8 d ){ dispMoves = d; };
	
	inline int8 getLinesSelected( void ){ return linesSelected; };
	inline void setLinesSelected( int8 d ){ linesSelected = d; };
	//retorna os jogador pelo índice, pode ser qq índice, 
	Player* getPlayerByIndice( uint8 i );
	
	void clearMoves( void );
	
	void setComboValue( uint8 c ){ combo = c; };

	inline uint8 getComboValue( void ){ return combo; };
	
	void setDivisorValue( float c );
	
	float getDivisorValue( void );
	
	//procura se há alguma zona grande
	bool makeBigZone( Point3f *p );
	bool unMakeBigZone( Point3f *p );
//verifica se fechou um quadrado ou não
	bool verifyZonesHorizontal( Point3f *p );
	bool verifyZonesVertical( Point3f *p );
	bool verifyLines( uint8 x, uint8 y );

	//introduz os movimentos dentro do vetor de buffer de movimentos
	void inputMoves( const Point3f *p );
	
	inline Chronometer* getChronometer( void ){ return pChronometer; };
	
	//tirei isso da lineGroup e de zonegroup pq estava cagado, deste jeito funciona, sim isso é uma gambiarra muita da feia...
	void getLineXyByPosition( Point3f *pLine , uint8 *x = NULL, uint8 *y = NULL );
	
	void getZoneXYByPosition( Point3f *pZone , uint8 *x = NULL, uint8 *y = NULL  );
	
	
	void moveCamera(const Point3f *p ){ moveCameraTo( p , 1.0 ); };
	Board* getBoard( void ){ return bg; }
	
private:
//começa outra partida em um nível de dificuldade maior
	void nextLevel( void );
	
//reseta o tabuleiro na mesma partida
	void resetTable( void );
	
	// Método chamado para indicar que a animação do controle foi finalizada
	virtual void onInterfaceControlAnimCompleted( InterfaceControl* pControl );
	
	// Método chamado quando o cronômetro termina sua contagem
	 void onStopTimeReached( void );
	
	// Método chamado quando a transição termina
	//virtual void onOGLTransitionEnd( void );	
	
	// Indica que a animação da câmera terminou
	virtual void onCameraAnimEnded( void );

	// Trata eventos de fim de toque
	void onTouchEnded( int8 touchIndex, const Point3f* pTouchPos );

//método build->carrega os sprites das nuvens
	bool buildDotGame( LoadingView* hLoadindView/*, bool Continuing*/ );
	//carrega as nuvens
	bool loadCloudSprites( void );
	//faz os botões
	bool buildButtons( void );
	
	//constrói o tabuleiro
	bool makeTable( void );
	
	//constrói o fundo
	bool buildBg( void );
	
	//carrega os players
	bool loadPlayers( void );
	
	int8 startTrackingTouch( const UITouch* hTouch );
	void stopTrackingTouch( int8 touchIndex );
	void onNewTouch( int8 touchIndex, const Point3f* pTouchPos );
	int8 isTrackingTouch( const UITouch* hTouch );
	void onMoveCamera( float dx, float dy );
	// Trata eventos de movimentação de toques
	void onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos );

	
	// Move a câmera para a posição 'position' real em 'time' segundos
	void moveCameraTo( const Point3f* pRealPosition, float time );
	void moveCameraTo( const Point3f* pRealPosition, float finalZoom, float time );
	

	// Indica que a aplicação recebeu um evento de suspend. Quando a aplicação
	// chama este método da cena, significa que a cena já parou de ser atualizada.
	// Os eventos de update serão retomados após a próxima chamada a resume()
	virtual void suspend( void );
	
	// Indica que a aplicação irá resumir sua execução. Quando a aplicação
	// chama este método da cena, significa que a cena voltará ser atualizada
	// através de eventos de update
	virtual void resume( void );
	
	// Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
	// jogo
	void getPickingRay( Ray& ray, const Point3f& p ) const;

	//obtém o toque relativo ao deslocamento da câmera
	Point3f getRelativePosition( const Point3f* p);
	
	//retorna a linha que foi pressionada
	bool linePressed( const Point3f *p );
	
	// Trata eventos de clique duplo
	void onDoubleClick( int8 touchIndex );

	//troca os personagens de acordo com a sua vez de jogar
	void changePlayer( void );

	//renderiza a transition
	bool renderTransitions( void );
	
	//deleta tudo
	void clear( void );
	
	//retorna se tem zonas livres em jogo
	bool haveFreeZone( void );

	//retorna se tem Linhas livres em jogo
	bool haveFreeLine( void );

	
	//função que retorna o zoom máximo possível de acordo com o tamanho do cenário
	float adjustMinZoom( void/*float f*/ );
	
	//reseta o zoom da camera qdo sai do modo de observação
	void resetCam( void );
	
	//as funções a seguir são para o caso em que o jogador esteja contra a máquina.
	
	//função que coordena as ações da cpu  
	void cpuMove( void );

	//fç que serve para tomar os quadrados que tiverem 3 linhas
	bool takeAll3( void ); 

	// verifica se a zona está com as linhas anteriores ocupadas
	bool verifyZones( uint8 x, uint8 y );
	
	//escolhe uma linha aleatoriamente
	bool chooseLineRand( void );

	
	//aponta qual jogador ganhou
	uint8 indicePlayerWinner( void );
	
	// Auxiliares da câmera
//	bool movingCamera;
//	Point3f cameraMovement;	

	uint8 idPlayerOrder[ MAX_PLAYERS ];
	
	ZoneGame* zones[ MAX_ZONES_COLUMNS ][ MAX_ZONES_ROWS ];
	
	LineGame* lineH [ MAX_ZONES_COLUMNS ][ MAX_LINES_ROWS ];
	
	LineGame* lineV [ MAX_LINES_COLUMNS ][ MAX_ZONES_ROWS ];
	
	//qte de linhas verticais e horizontais, zonas/quad 
	uint8 nHorizontalZones, nVerticalZones,nVerticalLines,nHorizontalLines;
	
	//o ideal seria se feosse uma lista encadeada cíclica, mas...
	Player* players[ MAX_PLAYERS ];
	//jogador corrente
	Player* currPlayer;
	//modo de jogo
	DotGameMode mode;
	// estado,último estado
	DotGameState state, lastState;
	
	uint8 horizontalSize, verticalSize, combo;
	//contador de linhas
	int8 linesSelected;
	
	ZoneGroup *zoneManager;
	
	LineGroup *lineManager;
	
	PowerUpGroup *pPowerUpGroup;
	
	ButtonsGroup *buttonsManager;
	
	bool animCamera;
	uint8 level, indicePlayer, nPlayers;
	
	//para cálculo da IA ( provavelmente vai morrer!! )
	uint8 limit;

	//informações de configuração do jogo ( pode ser usado para save com algumas adaptações!! )	
	GameBaseInfo *GInfo;

	// Responsáveis pela renderização das transições de fases	
	//OGLTransition *pCurrTransition;
//	SliceTransition *pSliceTransition;
	//CurtainTransition *pCurtainTransition;
	//CurtainLeftTransition	*pCurtainLeftTransition;
	
	// Câmera utilizada para renderizar os controles da cena
	OrthoCamera* pControlsCamera;
	
	//mostra as informações sobre os jogadores!
	InfoBar* pInfoBar;
	
	//cronometro,
	Chronometer* pChronometer;

	// Controlam a duração de estados de transição
	float stateTimeCounter, stateDuration;
	
	// Variáveis utilizadas para o controle dos gestos realizados no touchscreen
	uint8 nActiveTouches;
	float initDistBetweenTouches;
	Touch trackedTouches[ MAX_TOUCHES ];
	//tabuleiro
	Board* bg;
	//limites da tela
	Point3f lim, min;
	//para indicar se o jogo continua
	bool goodBye, persist;	

	//variáveis usadas para controlar o modo de rodadas no modo exibição
	uint8 movesMax, movesCount;

	
	//variáveis e funções usadas para controle de movimentos, gerando assim um buffer de comandos a serem executados
	std::vector<Point3f> movesMake;
	uint8 dispMoves;
	
	//variável com o tempo de jogo de cada player
	float maxTime,divisor;
	
	//indica se o jogo está pausado ou não
	bool paused;
		
	//controle dos sprites que se animam!?
	bool animLine,animZone;

							//para multiplayer
	//identifica o jogador local
	//uint8 idLocalPlayer;
	
	//o correto seria que apenas as funções fossem, mas como eu não sei afirmar direito quais fçs seriam, resolvicolcar a classe toda, fica pro TODOO definir quais fçs seriam
	friend class ZoneGame;
	friend class InfoBar;
	//friend class Background;
};


inline uint8 DotGame::getLevel( void ){ return level; }

inline bool  DotGame::isGoodbye( void ){ return goodBye; }

inline DotGameState DotGame::getDotGameState( void ){ return state; }


//void DotGame::setDivisorValue( float c ){ divisor = c; }
//
//float DotGame::getDivisorValue( void ){ return divisor; }

#endif
