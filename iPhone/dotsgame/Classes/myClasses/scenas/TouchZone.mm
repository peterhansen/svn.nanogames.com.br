/*
 *  TouchZone.mm
 *  dotGame
 *
 *  Created by Max on 3/22/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "TouchZone.h"
#include "Utils.h"
#include "Line.h"
#if DEBUG
// OpenGL
#include "GLHeaders.h"
#endif
bool TouchZone::verifyCollision( const Point3f *p ){

	return Utils::IsPointInsideRect( p, position.x, position.y, /*position.x +*/ size.x, /*position.y*/ + size.y );

}

#if DEBUG
bool TouchZone::render(){
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE );
	glEnableClientState( GL_VERTEX_ARRAY );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	glTranslatef( position.x + ( size.x * 0.5f ), position.y + ( size.y * 0.5f ), position.z );
	glScalef( size.x, size.y, size.z );
	

	
	Point3f border[] =	{
		Point3f( -0.5f, -0.5f ),
		Point3f(  0.5f, -0.5f ),
		Point3f(  0.5f,  0.5f ),
		Point3f( -0.5f,  0.5f )
	};
	
	glColor4ub( 125, 125, 255, 255 );
	glVertexPointer( 3, GL_FLOAT, 0, border );
	glDrawArrays( GL_LINE_LOOP, 0, 4 );
	
	glPopMatrix();
return true;
}
#endif
//
//bool TouchZone::render( void ){
//
//if( !isVisible() )
//	return false;
//	
//	glMatrixMode( GL_MODELVIEW );
//	glPushMatrix();
//	glTranslatef( position.x, position.y, position.z );
//	gl
//
//}
