/*
 *  InfoBar.h
 *  dotGame
 *
 *  Created by Max on 12/1/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef INFO_BAR_H
#define INFO_BAR_H 1

#include "ObjectGroup.h"
#include "InterfaceControl.h"
#include "GameBaseInfo.h"

#include "LifeBar.h"
#include "SpecialBar.h"

#include "TouchZone.h"
#include <vector>
using namespace std;

class RenderableImage;
class InterfaceControlListener;
class Label;
//class ChronometerListener;
class DotGame;
//possui todo o display do jogo, menos o chronometro, 
class InfoBar : public ObjectGroup, public InterfaceControl {
	
public:
	
	InfoBar( const GameBaseInfo &gInfo, InterfaceControlListener* pListener );	
	
	virtual ~InfoBar(); 
	//atualiza a pontuação
	void updateScores( DotGame *dg );
	//atualiza a barra de powerUp
	bool updatePowers( DotGame *dg );
	//acerta de acordo com a partida( pega cor de fundo, imagens de personagens, etc. )
	void configure( DotGame *dg );

	//verifica se há colisão no quadrado dos botões	
	bool checkCollision( const Point3f * p, DotGame* dg , bool active);
	
	virtual bool render( void );
	//chamado qdo um botão é pressionado
	void onButtonPressed( uint8 i, DotGame* dg );
	
	// Atualiza o objeto
	virtual bool update( float timeElapsed );
	
	inline LifeBar* getLifeBar(){ return pLifeBar; };
	
private:
	friend class DotGame;
	
	bool buildInfoBar( void );
	void clear();
	GameBaseInfo ginfo;

	//obs: as imagens em array se referem às do jogadores em sua posição, ou seja, player0 == PUpPlayer[0]

	//powerUpsFiguras
	RenderableImage *PUpPlayer[ MAX_PLAYERS ], *sqPower[ MAX_PLAYERS ];
	
//Faces dos jogadores
	//RenderableImage *FacePlayer[ MAX_PLAYERS ];	

//barra de especiais	
	SpecialBar *spPlayer[ MAX_PLAYERS ];

//Labels dos especiais
	Label *spLabel[ MAX_PLAYERS ];
	
//barra de vidas
	LifeBar *pLifeBar;
	
//sprites dos rostos dos jogadores
	Sprite* pFacesPlayer[ MAX_PLAYERS ];
	
//qte de botoes que existem na barra de informações
	uint8 nButtons;
	
//vetor com as máscaras de collisão
	std::vector<TouchZone> maskCollisionVector;
};



#endif