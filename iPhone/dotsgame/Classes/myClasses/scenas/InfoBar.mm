/*
 *  InfoBar.mm
 *  dotGame
 *
 *  Created by Max on 12/1/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#include "SpecialBar.h"

#include "InfoBar.h"
#include"ObjcMacros.h"

#include"GameBaseInfo.h"
#include"RenderableImage.h"
#include"InterfaceControlListener.h"
#include"Chronometer.h"
#include"ChronometerListener.h"
#include"DotGame.h"
#include"Label.h"
#include "FreeKickAppDelegate.h"


#include "Exceptions.h"
#include "Label.h"
#include "RenderableImage.h"
#include "Utils.h"

#define INFO_BAR_BT_COLOR_PRESSED Color( static_cast< uint8 >( 127 ), 127, 127, 255 )
#define INFO_BAR_BT_COLOR_UNPRESSED Color( static_cast< uint8 >( 255 ), 255, 255, 255 )

#define SPECIAL_BAR_PLAYER_1_POSITION_X 83.0f
#define SPECIAL_BAR_PLAYER_1_POSITION_Y 40.5f

#define SPECIAL_BAR_PLAYER_2_POSITION_X 267.0F 
#define SPECIAL_BAR_PLAYER_2_POSITION_Y SPECIAL_BAR_PLAYER_1_POSITION_Y

#define INFO_BAR_FONT_CHAR_SPACEMENT 1.0f

#define NAME_LABEL1_POSITION_X 130.0f
#define NAME_LABEL1_POSITION_Y 50.0f
#define SCORE_LABEL1_POSITION_X 80.0f
#define SCORE_LABEL1_POSITION_Y 20.f

#define GLASSES_LEFT_POSITION_X 1.0f
#define GLASSES_LEFT_POSITION_Y 17.0
#define GLASSES_RIGHT_POSITION_X  ( HALF_SCREEN_WIDTH -  2.5 )
#define GLASSES_RIGHT_POSITION_Y GLASSES_LEFT_POSITION_Y


#define FACE1_POSITION_X 18.0
#define FACE1_POSITION_Y 6.0f

#define FACE2_POSITION_X 360.0f
#define FACE2_POSITION_Y FACE1_POSITION_Y

#define POWER_UP_PLAYER_1_POSITION_X 5.0f
#define POWER_UP_PLAYER_1_POSITION_Y 14.0f

#define POWER_UP_PLAYER_2_POSITION_X 442.0
#define POWER_UP_PLAYER_2_POSITION_Y POWER_UP_PLAYER_1_POSITION_Y

#define NAME_LABEL2_POSITION_X 272.0f
#define NAME_LABEL2_POSITION_Y 50.0f
#define SCORE_LABEL2_POSITION_X 380.0f
#define SCORE_LABEL2_POSITION_Y 20.0f

#define FACE1_WIDTH 102.0f
#define FACE1_HEIGHT 66.0f

#define FACE2_WIDTH 102.0f
#define FACE2_HEIGHT 66.0f

#define LABELS_HEIGHT 22.0f
#define LABELS_WIDTH  85.0f//65.0f//45.0f

#define LABEL_SPECIAL_LEVEL_P1_POSITION_X 129.0f
#define LABEL_SPECIAL_LEVEL_P1_POSITION_Y 39.0f

#define LABEL_SPECIAL_LEVEL_P2_POSITION_X 333.0f
#define LABEL_SPECIAL_LEVEL_P2_POSITION_Y LABEL_SPECIAL_LEVEL_P1_POSITION_Y

#define BUTTON_POWER_UP_P1_INDICE 128
#define BUTTON_POWER_UP_P2_INDICE ( BUTTON_POWER_UP_P1_INDICE + 1 )

#define LABEL_NAME_P1_INDICE 2//( LABEL_SCORE_P2_INDICE + 1 )
#define LABEL_NAME_P2_INDICE ( LABEL_NAME_P1_INDICE + 1 )

#define LIFE_BAR_POSITION_X 130.0
#define LIFE_BAR_POSITION_Y 25.0//( 10.0 + 15.0 )

#define MASK_COLLISION_POWER_UP_INDICE_PLAYER_1 0
#define MASK_COLLISION_POWER_UP_INDICE_PLAYER_2 1
#define MASK_COLLISION_SPECIAL_INDICE_PLAYER_1 2
#define MASK_COLLISION_SPECIAL_INDICE_PLAYER_2 3


#define INFO_BAR_N_OBJECTS 10//14

InfoBar::InfoBar( const GameBaseInfo &gInfo, InterfaceControlListener* pListener ):ObjectGroup( INFO_BAR_N_OBJECTS ), InterfaceControl( pListener ), ginfo( gInfo ){
	
	memset( PUpPlayer, NULL , sizeof( RenderableImage* ) * MAX_PLAYERS );
	memset( sqPower, NULL , sizeof( RenderableImage* ) * MAX_PLAYERS );
	memset( pFacesPlayer, NULL , sizeof( Sprite* ) * MAX_PLAYERS );
	memset( spPlayer, NULL , sizeof( SpecialBar* ) * MAX_PLAYERS );
	memset( spLabel, NULL , sizeof( Label* ) * MAX_PLAYERS );

	if( !buildInfoBar() ){
#if DEBUG
		NSLog(@"falha no buil->InfoBar");
		throw ConstructorException( "Falha no infoBar->Build" );
#else
		throw ConstructorException();
#endif
		
	}
}




bool InfoBar::buildInfoBar( void ){
	
//power up player 1
	TouchZone p1 = TouchZone(  POWER_UP_PLAYER_1_POSITION_X ,POWER_UP_PLAYER_1_POSITION_Y,32,32);
	maskCollisionVector.push_back( p1 );
	
//power up player 2
	TouchZone p2 = TouchZone( POWER_UP_PLAYER_2_POSITION_X ,POWER_UP_PLAYER_2_POSITION_Y,32,32 );
	maskCollisionVector.push_back( p2 );
	
//especial player 1
	TouchZone p3 = TouchZone(LABEL_SPECIAL_LEVEL_P1_POSITION_X,LABEL_SPECIAL_LEVEL_P1_POSITION_Y,LABELS_WIDTH,LABELS_HEIGHT );
	maskCollisionVector.push_back( p3 );
	
//especial player 2
	TouchZone p4 = TouchZone( LABEL_SPECIAL_LEVEL_P2_POSITION_X - LABELS_WIDTH + 12.0,LABEL_SPECIAL_LEVEL_P2_POSITION_Y,LABELS_WIDTH,LABELS_HEIGHT  );
	maskCollisionVector.push_back( p4 );

	//Font* pInfoBarFont = [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_MAIN];
	
	Font* pInfoBarFont = [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NAME];
	Font* pInfoBarSpecialFont =  [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_SPECIAL];
//new Font("fapelido","fapelido",false);
	
	if(!pInfoBarFont){
#if DEBUG
		NSLog(@"fonte não carregada!!");
#endif
		return false;
	}


	
//barra com o rosto do mané 01
	Color c = COLOR_PURPLE;
	c.setFloatA( 0.85f );
	TextureFrame f( 0, 0, 240 ,80,0,0 );
	RenderableImage* Face01 = new RenderableImage("interface");
	Face01->setImageFrame( f );
	Face01->setVertexSetColor( c );
	Face01->setPosition( GLASSES_LEFT_POSITION_X, GLASSES_LEFT_POSITION_Y);
	if(!Face01 || !insertObject( Face01 ) ){
#if DEBUG
		NSLog(@"óculos esquerdo não carregada!!");
#endif
		return false;
	
	}
		
	setObjectZOrder(Face01, 0 );
	
//barra com o rosto do mané 02
	RenderableImage* Face02 = new RenderableImage( Face01 );
	Face02->mirror( MIRROR_HOR );
	Face02->setImageFrame( f );
	Color c2 = COLOR_YELLOW;
	c2.setFloatA( 0.15f );
	Face02->setVertexSetColor( c );
	Face02->setPosition( GLASSES_RIGHT_POSITION_X,GLASSES_RIGHT_POSITION_Y);
	if( !Face02 || !insertObject( Face02 ) ){
#if DEBUG
		NSLog(@"óculos direito não carregada!!");
#endif
		return false;
		
	}
	setObjectZOrder(Face02, 1 );

	Label* pLbNameP1 = new Label( pInfoBarFont , false );
	if(!pLbNameP1 || !insertObject( pLbNameP1 )){
#if DEBUG
		NSLog(@"label nome p1 fail!!");
#endif
		return false;
	}
	pLbNameP1->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM);
	pLbNameP1->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbNameP1->setPosition( NAME_LABEL1_POSITION_X, NAME_LABEL1_POSITION_Y ,1.0f  );
	pLbNameP1->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
//	pLbNameP1->setText("111",false, false);
	
	Label* pLbNameP2 = new Label( pInfoBarFont , false );
	if(!pLbNameP2 || !insertObject( pLbNameP2 )){
#if DEBUG
		NSLog(@"label nome p2 fail!!");
#endif
		return false;
	}
	pLbNameP2->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM );
	pLbNameP2->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbNameP2->setPosition( NAME_LABEL2_POSITION_X, NAME_LABEL2_POSITION_Y ,1.0f  );
	pLbNameP2->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
//	pLbNameP2->setText("666",false, false);
	
	
	float positionsLabelX[ MAX_PLAYERS ] = { LABEL_SPECIAL_LEVEL_P1_POSITION_X, LABEL_SPECIAL_LEVEL_P2_POSITION_X } ;
	float positionsLabelY[ MAX_PLAYERS ] = { LABEL_SPECIAL_LEVEL_P1_POSITION_Y, LABEL_SPECIAL_LEVEL_P2_POSITION_Y } ;
	
	float positionsX[ MAX_PLAYERS ] = { POWER_UP_PLAYER_1_POSITION_X - 5.0f , POWER_UP_PLAYER_2_POSITION_X - 4.0f  };
	float positionsY[ MAX_PLAYERS ] = { POWER_UP_PLAYER_1_POSITION_Y - 4.0f, POWER_UP_PLAYER_2_POSITION_Y - 4.0f};

	
	float positionsBarX[ MAX_PLAYERS ] = { SPECIAL_BAR_PLAYER_1_POSITION_X, SPECIAL_BAR_PLAYER_2_POSITION_X } ;
	float positionsBarY[ MAX_PLAYERS ] = { SPECIAL_BAR_PLAYER_1_POSITION_Y, SPECIAL_BAR_PLAYER_2_POSITION_Y } ;
	
	bool b = true;
	c.setFloatA( 1.0 );
	RenderableImage aux = RenderableImage("sqPow");
	for( uint8 i = 0; i < MAX_PLAYERS; i++ ){
		//mudar para receber a face vinda do jogador indice tal...
		spPlayer[ i ] = new SpecialBar( b, i );
		spLabel[ i ] = new Label( pInfoBarSpecialFont , false  );
		
		sqPower[ i ] = new RenderableImage( &aux );
		
		if( !spPlayer || !spLabel || !sqPower )
			return false;
		b = false;
		
		spPlayer[ i ]->setPosition(positionsBarX[i], positionsBarY[i]);
		
		spLabel[ i ]->setTextAlignment(ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM);
		spLabel[ i ]->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
		spLabel[ i ]->setSize(LABELS_WIDTH, LABELS_HEIGHT);
		spLabel[ i ]->setPosition(positionsLabelX[i], positionsLabelY[i] );
		spLabel[ i ]->setText( "0",false,false );
		
		sqPower[ i ]->setPosition( positionsX[i], positionsY[i]   );
		sqPower[ i ]->setVertexSetColor( c );
			
	}	

	pLifeBar = new LifeBar( &ginfo, "lifeBar" );
	if( !pLifeBar || !insertObject(pLifeBar) )
		return false;
	pLifeBar->setPosition( LIFE_BAR_POSITION_X, LIFE_BAR_POSITION_Y );
	
	
	return true;
}

void InfoBar::clear(){
	for( uint8 i = 0 ; i < MAX_PLAYERS ; ++i ){
		SAFE_DELETE( PUpPlayer[ i ] );
		SAFE_DELETE( spPlayer [ i ] );
		SAFE_DELETE( spLabel[ i ] );
		SAFE_DELETE( pFacesPlayer[ i ] );
		SAFE_DELETE( sqPower[ i ] );
	}
	pLifeBar = NULL;
	maskCollisionVector.clear();
}

// Atualiza o objeto
bool InfoBar::update( float timeElapsed ){
	if( !ObjectGroup::update( timeElapsed ) )
		return false;
	
	return true;
}

/*==================================================================

 updateScores-> atualiza a pontuação
 
==================================================================*/
#define MAX_BUFFER_SCORE_LEN 3

void InfoBar::updateScores( DotGame *dg /*Player* pl*/  ){
	Player *pl = dg->getCurrPlayer();
	uint8 idCurPl = pl->getId();
	
	if ( spPlayer[ idCurPl ]->updateSize() ){
		pl->setSpecialLevel( pl->getSpecialLevel() + 1 );
	}
	pLifeBar->updateSizeBar( pl );

	char buffer[ MAX_BUFFER_SCORE_LEN ];
	snprintf( buffer, MAX_BUFFER_SCORE_LEN, "%d", pl->getSpecialLevel() );
	spLabel[ pl->getId() ]->setText( buffer, false, false );

	for (int8 i = 0; i < MAX_PLAYERS; i++) {
		dg->getPlayerByIndice( i )->getDoll()->setExpressionByPercetinl( pLifeBar->getPercentilByPlayer( i ) );
		pFacesPlayer[ i ]->setAnimSequence( dg->getPlayerByIndice( i )->getDoll()->getIndiceByExpression() , true ,false );
	
	}
}


/*==================================================================
 
configure-> puxa dados do jogo, como nome, boneco, etc.
 
 ==================================================================*/
#define MAX_BUFFER_NAME_LEN 12
void InfoBar::configure( DotGame *dg ){
	
	float positionsFaceX[ MAX_PLAYERS ] = { FACE1_POSITION_X, FACE2_POSITION_X } ;
	float positionsFaceY[ MAX_PLAYERS ] = { FACE1_POSITION_Y, FACE2_POSITION_Y } ;
	
	for( int8 i = 0; i < MAX_PLAYERS; i++ ){
		pFacesPlayer[ i ] = new Sprite( ( Sprite* ) dg->getPlayerByIndice( i )->getDoll()->getFacePic() );
		pFacesPlayer[ i ]->setPosition( positionsFaceX[i], positionsFaceY[i] );
		pFacesPlayer[ i ]->setAnimSequence( dg->getPlayerByIndice( i )->getDoll()->getIndiceByExpression() , true ,false );
	}
	pFacesPlayer[ 1 ]->mirror( MIRROR_HOR );
	
	char buffer[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer , MAX_BUFFER_NAME_LEN, dg->GInfo->nomes[ 0 ].c_str() );
	static_cast< Label* >( getObject( LABEL_NAME_P1_INDICE ) )->setText( buffer , false, false );
	
	char buffer2[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer2, MAX_BUFFER_NAME_LEN, dg->GInfo->nomes[ 1 ].c_str() );
	static_cast< Label* >( getObject( LABEL_NAME_P2_INDICE ) )->setText( buffer2, false, false );
	
}
#undef MAX_BUFFER_NAME_LEN

/*==================================================================
 
render->renderiza tudo e mais um pouco (dããããã)
 
 ==================================================================*/

bool InfoBar::render( void ){
	if( !isVisible() )
		return false;
	bool ret = false;

	ret |= ObjectGroup::render();
	glPushMatrix();

	
	for( uint8 i = 0 ; i < MAX_PLAYERS ;i++){
		pFacesPlayer[ i ]->render();
		
		sqPower[ i ]->render();
		if( PUpPlayer[ i ]  )
			ret |= PUpPlayer[ i ]->render();
		
		
		ret |= spPlayer[ i ]->render();	
		ret |= spLabel[ i ]->render();

}
	glPopMatrix();
#if DEBUG
	for ( int8 i = 0; i<maskCollisionVector.size();i++ ){
		maskCollisionVector[i].render();
	}

#endif
	return ret;
}

/*
 atualiza os powerUps da barra de acordo com o tipo e com o jogador corrente
 */

bool InfoBar::updatePowers( DotGame *dg ){
	Player *p;

	Renderable* r = NULL;
	float positionsX[ MAX_PLAYERS ] = { POWER_UP_PLAYER_1_POSITION_X, POWER_UP_PLAYER_2_POSITION_X } ;
	float positionsY[ MAX_PLAYERS ] = { POWER_UP_PLAYER_1_POSITION_Y, POWER_UP_PLAYER_2_POSITION_Y } ;
	for( uint8 i = 0 ; i< MAX_PLAYERS ; i++ ){
		SAFE_DELETE ( PUpPlayer[ i ] ) ;
		p = dg->getPlayerByIndice( i );
		if( p->havePowerUp() ){
			if( p->getId() == dg->getCurrPlayer()->getId() ){
				r = p->getPowerUp()->getImageToBar(); 	
			}else {
				r = p->getPowerUp()->getHiddenImage();
			}
			
			if( r!= NULL ){
				PUpPlayer[ i ] = new RenderableImage( ( RenderableImage* ) r );
				if ( PUpPlayer[ i ]  != NULL ){
					PUpPlayer[ i ]->setPosition( positionsX[i], positionsY[i] );
				}else {
					return false;
				}
				
			}else {
				return false;
			}
		}
	}
	
	return true;

}

bool InfoBar::checkCollision( const Point3f * p, DotGame* dg , bool active){
//verificação de colisão com os botões aqui!!
	if( !active )
		return false; 
	bool ret = false;
	for ( uint8 i = 0 ; i < maskCollisionVector.size() ; i++) {
		if( maskCollisionVector[i].verifyCollision( p )  ){
			switch ( i ) {
				case MASK_COLLISION_SPECIAL_INDICE_PLAYER_1:
					spLabel[ 0 ]->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					spPlayer[ 0 ]->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					ret = true;
					onButtonPressed(i , dg);
					break;
				case MASK_COLLISION_SPECIAL_INDICE_PLAYER_2:
					spLabel[ 1 ]->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					spPlayer[ 1 ]->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					ret = true;
					onButtonPressed(i , dg);
					break;
				case MASK_COLLISION_POWER_UP_INDICE_PLAYER_1:
					if( PUpPlayer[ 0 ] )
						PUpPlayer[ 0 ]->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					ret = true;
					onButtonPressed(i , dg);
					break;
				case MASK_COLLISION_POWER_UP_INDICE_PLAYER_2:
					if( PUpPlayer[ 1 ] )
						PUpPlayer[ 1 ]->setVertexSetColor(INFO_BAR_BT_COLOR_PRESSED);
					ret = true;
					onButtonPressed(i , dg);
					break;
			}
			
		}
		for( uint8 i = 0; i < MAX_PLAYERS; i++ ){	
			spPlayer[ i ]->setVertexSetColor( INFO_BAR_BT_COLOR_UNPRESSED );
			spLabel[ i ]->setVertexSetColor( INFO_BAR_BT_COLOR_UNPRESSED );
			//sqPower[ i ]->setVertexSetColor( INFO_BAR_BT_COLOR_UNPRESSED );
		}
	}	
	return ret;
}

void InfoBar::onButtonPressed( uint8 i, DotGame* dg  ){
//identificar o botão pressionado de acordo com o índice e colocar aqui
	
#if DEBUG
	NSLog(@"botão da infoBar indice %d pressionado",i);
#endif
	
	Player *p1,*p2,*currP;
	currP = dg->currPlayer;
	p1 = dg->getPlayerByIndice( 0 );
	p2 = dg->getPlayerByIndice( 1 );
	switch ( i ) {
		case MASK_COLLISION_POWER_UP_INDICE_PLAYER_1 :{
			if( currP->getId() == p1->getId() && p1->havePowerUp() ){
				p1->doPowerUp( dg );
#if DEBUG
				NSLog(@"player 1 usando power up!");
#endif
				p1->setPowerUp( NULL );
				updateScores( dg );
				updatePowers( dg );
			}
		}break;

		case MASK_COLLISION_POWER_UP_INDICE_PLAYER_2 :{
			if( currP->getId() == p2->getId() && p2->havePowerUp()  ){
				p2->doPowerUp( dg );
#if DEBUG
				NSLog(@"player 2 usando power up!");
#endif
				p2->setPowerUp( NULL );
				updateScores( dg );
				updatePowers( dg );
			}			
		}break;
		case MASK_COLLISION_SPECIAL_INDICE_PLAYER_1:{
			if( currP->getId() == p1->getId() && currP->getSpecialLevel() > 0 ){
				dg->setLinesSelected( dg->getLinesSelected() - 1 );
				currP->setSpecialLevel( currP->getSpecialLevel() - 1 );
				char buffer[ MAX_BUFFER_SCORE_LEN ];
				snprintf( buffer, MAX_BUFFER_SCORE_LEN, "%d", currP->getSpecialLevel() );
				spLabel[ currP->getId() ]->setText( buffer, false, false );
#if DEBUG
				NSLog(@"player 1 usando especial!");
#endif
			}	
		}break;
		case MASK_COLLISION_SPECIAL_INDICE_PLAYER_2:{
			if( currP->getId() == p2->getId()  && currP->getSpecialLevel() > 0 ){
				dg->setLinesSelected( dg->getLinesSelected() - 1 );
				currP->setSpecialLevel( currP->getSpecialLevel() - 1 );
				char buffer[ MAX_BUFFER_SCORE_LEN ];
				snprintf( buffer, MAX_BUFFER_SCORE_LEN, "%d", currP->getSpecialLevel() );
				spLabel[ currP->getId() ]->setText( buffer, false, false );
#if DEBUG
				NSLog(@"player 2 usando especial!");
#endif
			}	
		}break;
	}

}
#undef MAX_BUFFER_SCORE_LEN
InfoBar::~InfoBar(){
	clear();
}
