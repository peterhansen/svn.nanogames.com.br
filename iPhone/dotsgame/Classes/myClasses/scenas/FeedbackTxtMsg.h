/*
 *  FeedbackTxtMsg.h
 *  dotGame
 *
 *  Created by Max on 5/31/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef FEEDBACK_TXT_MSG_H
#define FEEDBACK_TXT_MSG_H 1

#include"Label.h"
#include"InterfaceControl.h"

// Forward Declarations
class InterfaceControlListener;

enum FeedbackTxtAnimType
{
	FEEDBACK_TXT_ANIM_NONE = 0,
	FEEDBACK_TXT_ANIM_VIEWPORT = 1,
	FEEDBACK_TXT_ANIM_BOUNCE = 2,
};

/*
 perfeito para internacionalização, basta trocar os textos, já que é apenas uma string, além do mais, é como o
 */

class FeedbackTxtMsg : public Label ,public InterfaceControl {
	
	public:
		// Construtor
		// Exceções: ConstructorException
		FeedbackTxtMsg(InterfaceControlListener* pListener , const char* pFontImgsId , const char* pFontDescriptor, bool manageFont = true );
	
		FeedbackTxtMsg(InterfaceControlListener* pListener , Font* pFont = NULL, bool manageFont = true );
	
		// Destrutor
		virtual ~FeedbackTxtMsg( void );
	
		// Determina o tipo de animações utilizado pelo controle
		void setCurrAnimType( FeedbackTxtAnimType animType );
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Determina o estado de exibição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

	private:
		// Métodos que realizam as animações
		void animViewport( float timeElapsed );
		void animBounce( float timeElapsed );
		void animFade( float timeElapsed );

		// Tipo de animação do controle
		FeedbackTxtAnimType currAnimType;
	
		// Auxiliares das animações
		float speedY;
		float acumWidth;

};


#endif