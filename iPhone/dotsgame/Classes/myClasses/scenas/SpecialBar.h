/*
 *  SpecialBar.h
 *  dotGame
 *
 *  Created by Max on 5/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SPECIAL_BAR_H
#define SPECIAL_BAR_H 1
#define SIZE_FRAME_SPECIAL_BAR_ADJUST_X 16.0f//61.0F

#include"RenderableImage.h"
#include "Color.h"
#include "Label.h"
//class Player;
/*
 barra de especial, serve para medir o especial( dããããã ), ela mede através da qte de fatias ( slices ), que ele possui. ela funciona por viewport
 */
class SpecialBar : public RenderableImage {
	
public:
	
	SpecialBar( bool orient, uint8 _idPlayer, uint8 _nSlices = 5 );
	
	virtual ~SpecialBar();
	
	uint8 getIdPlayer( void );
	
	//void updateLabel( void );
	
	bool updateSize(/*uint8 _size*/  );
	
	static void unLoadImages( void );	
	
	static bool loadImages( void );
	
	//virtual bool render();
	
private:
	
	static RenderableImage* baseImage;
	TextureFrame baseFrame;
	//Label *levelText;
	bool isPlayer2;	
	uint8 nSlices, currLevel;
	float sliceSize;
	uint8 idPlayer;
		
};


#endif