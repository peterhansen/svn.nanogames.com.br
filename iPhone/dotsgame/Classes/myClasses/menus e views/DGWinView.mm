/*
 *  DGWinView.mm
 *  dotGame
 *
 *  Created by Max on 6/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "DGWinView.h"

#include "ModeSelectView.h"
#include "FreeKickAppDelegate.h"
#include "Config.h"
#include "Macros.h"
#include "DotGame.h"
#include "CharacterFactory.h"

@interface DGWinView( Private )
	
	
@end

@implementation DGWinView

-( IBAction ) onBtPressed:( id )hButton{
	if( hButton == hPlayAgain){
		[[ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_LOAD_GAME ];
	}else if( hButton == hMenu )  {
		[[ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_MAIN_MENU ];

	}
	
}

-( void ) onBeforeTransition: ( GameBaseInfo* )GInfo{
	
	IdDoll winId = GInfo->iddolls[ GInfo->idWinner ];
	[ winnerImage setImage: CharacterFactory::GetInstance()->getWinCharImages( winId ) ];
	[ winnerBackground setImage: CharacterFactory::GetInstance()->getWinBgImages ( winId ) ];
	
	//[ winnerImage setCenter:CGPointMake( winnerImage.center.x ,( ( winnerImage.frame.origin.x * 0.5 ) - HALF_SCREEN_HEIGHT ) ) ];
	 
	

}

-( void )update:( float ) timeElapsed{
	[ super update:timeElapsed ];

}


-( void ) awakeFromNib{
	[super awakeFromNib];
	
}

-( void )dealloc{
	[ super dealloc ];
	[ winnerImage release ];
	[ winnerBackground release ];
}

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen{
	[ self resume ];
}

@end


