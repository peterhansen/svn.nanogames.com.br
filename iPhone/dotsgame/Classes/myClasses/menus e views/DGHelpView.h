/*
 *  DGHelpView.h
 *  dotGame
 *
 *  Created by Max on 12/10/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef DG_HELP_VIEW
#define DG_HELP_VIEW 1

#include "UpdatableView.h"

@interface DGHelpView : UpdatableView
{
	IBOutlet UIButton* hBack;
	IBOutlet UITextView* Hhelp;
	
}

- ( IBAction ) onBtPressed:( id )hButton;
// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

@end


#endif