/*
 *  DGOptions.h
 *  dotGame
 *
 *  Created by Max on 12/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DG_OPTIONS_VIEW
#define DG_OPTIONS_VIEW 1

#include "UpdatableView.h"
#include "NanoTypes.h"

enum {
	DG_OPTIONS_STATE_NONE = -1,
	DG_OPTIONS_STATE_SHOWING = 0,
	DG_OPTIONS_STATE_SHOW,
	DG_OPTIONS_STATE_HIDDING
} typedef dgOptionsState;


enum {
	DG_OPTIONS_STYLE_NONE = -1,
	DG_OPTIONS_STYLE_FROM_GAME = 0,
	DG_OPTIONS_STYLE_FROM_MENU
}typedef dgOptionsStyle; ;

enum {
	DG_OPTIONS_MODE_NONE = -1,
	DG_OPTIONS_MODE_HELP = 0,
	DG_OPTIONS_MODE_SETTINGS,
	DG_OPTIONS_MODE_QUIT
//	DG_OPTIONS_MODE_
}typedef dgOptionsMode;

@interface DGOptions : UpdatableView
{
	//settings mode
	IBOutlet UIButton* hSettings;
	IBOutlet UILabel* hVolume;
	IBOutlet UILabel* hVibra;
	IBOutlet UISlider* hVolum;
	IBOutlet UIButton *hVbBoxOn, *hVbBoxOff;
	
	IBOutlet UIButton *hBack;

	// somente acessível qdo vem da tela de jogo
	//quit mode
	//IBOutlet UIButton *hQuit;
	IBOutlet UIButton *hYes;
	IBOutlet UIButton *hNo;
	
	//para voltar à tela de jogo
	IBOutlet UIButton *hContinue;
	
	//help mode
	IBOutlet UIButton *hHelpButton;
	IBOutlet UITextView *hHelpText;
	
	
	int8 previousScreen;
	dgOptionsStyle style;
	dgOptionsState currState, lastState;
	dgOptionsMode mode,lastMode;
}

- ( IBAction ) onBtPressed:( id )hButton;


- ( IBAction ) onSldPressed:( id )hSld;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

-( void )setVibrationStatus:( bool )VibrationOn;

-( void )setPreviousIndex:( int8 )prevIndex;

-( void )setDgOptionsMode:( dgOptionsMode) nOption;

-( void )setDgOptionsStyle:( dgOptionsStyle ) nStyle;

-( void )setDgOptionsState:( dgOptionsState ) nState;

-( void )update:( float ) timeElapsed;

@end
#endif