/*
 *  UIPattern.h
 *  dotGame
 *
 *  Created by Max on 5/26/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef UI_PATTERN_H 
#define UI_PATTERN_H 1
//fazer um pattern é fácil, basta ver que na imageview tem uma fç para isso
@interface UIPattern : UIView
{

@private
	
	
	float offsetX, offsetY;

//imagem que servirá de Pattern
	UIImage *image;
}

//@property( readwrite, nonatomic,copy ) float offsetX;
//@property( readwrite, nonatomic,copy ) float offsetY;
//@property( readwrite, nonatomic,copy ) float offsetX;

@end


#endif