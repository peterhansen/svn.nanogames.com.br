/*
 *  UIPattern.mm
 *  dotGame
 *
 *  Created by Max on 5/26/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "UIPattern.h"

@interface UIPattern( Private )

//inicializa o objeto
-( bool )buildUIPattern;

//libera a memória alocada
-( void )cleanUIPattern;

//apaga as subviews atuais
-( void )cleanSubViews;

@end


@implementation UIPattern
/*
 construtor via código
 */
-( id )initWithFrame:( CGRect )frame{
	
	if( ( self = [ super initWithFrame:frame ] ) ){
		
		if( ![ self buildUIPattern ] )
			goto Error;
	}
	return self;
	
Error:
	[ self release ];
	return NULL;
}
/*
 construtor via nib/xib
 */
-( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder: decoder] ) )
	{
		if( ![self buildUIPattern] )
			goto Error;
    }
    return self;
	
	Error:
		[self release];
		return NULL;
}
/*
destrutor
 */
-( void )dealloc{
	[ self cleanUIPattern ];
	[ super dealloc ];
}
/*
 limpa as subviews alocadas para a imagem
 */
-( void )cleanSubviews
{
	NSArray *hSubviews = [self subviews];
	if( hSubviews != nil )
	{
		for( UIView* hSubview in hSubviews )
			[hSubview removeFromSuperview];
	}
}



@end


