/*
 *  LoadingVersusView.h
 *  dotGame
 *
 *  Created by Max on 12/16/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef LOADING_VERSUS_VIEW_H
#define LOADING_VERSUS_VIEW_H 1

#import <UIKit/UIKit.h>

#include "LoadingThread.h"
#include "NanoTypes.h"
#include "GameBaseInfo.h"

@interface LoadingVersusView : UIView
{	
@private
	// Ponteiro para a função de carregamento do usuário
	id loadingObj;
	SEL loadingFunc;
	SEL onLoadEndedFunc;
	
	// Indica se houve algum erro de processamento na thread
	int16 threadErrorCode;
	
	// Porcentagem de carregamento completa
	float loadingPercent;
	
	// Thread responsável por executar a rotina de carregamento
	LoadingThread* hThread;
	
	IBOutlet UIImageView *hP1;
	IBOutlet UIImageView *hP2;
	IBOutlet UIImageView *hVersus;	
	
	// Fornecem para o usuário um feedback do carregamento
	NSTimer *hInterfaceUpdater;
	IBOutlet UILabel *hProgressLabel;
	IBOutlet UIProgressView *hProgressBar;
	
	// Indica se já exibimos 100% na tela
	bool showed100Feedback;
	
	GameBaseInfo* GBinfo;
}

// Determina um ponteiro para a função de loading
-( void )setLoadingTarget:( id )target AndSelector:( SEL )selector CallingAfterLoad:( SEL )onLoadEndedSelector;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

// Pára o loop de renderização
- ( void ) resume;

// Pára o loop de renderização
- ( void ) suspend;

// Determina a porcentagem atual da barra de progresso, atualizando sua imagem e o label de
// porcentagem do loading
- ( void ) setProgress:( float )percent;

// Modifica a porcentagem atual da barra de progresso em 'percent', atualizando sua imagem e
// o label de porcentagem do loading
- ( void ) changeProgress:( float )percent;

// Indica que houve algum erro de processamento na thread
-( void ) setError:( int16 )errorCode;

// Retorna se houve algum erro de processamento na thread
-( int16 ) getError;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo;

@end
#endif