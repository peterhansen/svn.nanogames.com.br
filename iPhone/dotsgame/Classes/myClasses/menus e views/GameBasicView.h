/*
 *  GameBasicView.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 12/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */


#ifndef GAME_BASIC_VIEW_H
#define GAME_BASIC_VIEW_H 1

#include "UpdatableView.h"

@interface GameBasicView : UpdatableView
{
}

// Indica que o usuário está pressionando uma opção do menu.
- ( IBAction ) onBtClick:( id )hButton;

@end

#endif
