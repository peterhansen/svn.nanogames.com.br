/*
 *  DotGame.mm
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "DotGame.h"
#include "AnimatedCamera.h"
#include "Button.h"
#include "MathFuncs.h"
//#include "GameUtils.h"
#include "BoardFactory.h"
#include "PowerUpFactory.h"


#include "Utils.h"
#include"Random.h"
#include "FeedbackTxtMsg.h"
#include "TexturePattern.h"
#include "FeedbackMsg.h"
#include "ObjcMacros.h"
#include "Marks.h"
#include "FreeKickAppDelegate.h"
#include "Button.h"
#include "MathFuncs.h"
using namespace NanoMath;


#define POSITION_BUTTON_LINE_X 16.0f
#define POSITION_BUTTON_LINE_Y 272.0f

#define POSITION_BUTTON_PAUSE_X 220.0f
#define POSITION_BUTTON_PAUSE_Y 39.0f



//indíces das nuvens dentro do vetor
#define INDEX_CLOUD_ZONE_11_1 3//4//2//4
#define INDEX_CLOUD_ZONE_11_2 ( INDEX_CLOUD_ZONE_11_1 + 1 )
#define INDEX_CLOUD_ZONE_22_1 ( INDEX_CLOUD_ZONE_11_2 + 1 )
#define INDEX_CLOUD_ZONE_22_2 ( INDEX_CLOUD_ZONE_22_1 + 1 )
#define INDEX_CLOUD_LINE_H ( INDEX_CLOUD_ZONE_22_2 + 1 )
#define INDEX_CLOUD_LINE_V ( INDEX_CLOUD_LINE_H + 1 )

//COORDENADAS PARA AJUSTE DAS ANIMAÇÕES DAS NUVENS
#define ADJUST_CLOUD_LINE_HORIZONTAL_POSITION_X 12.0f
#define ADJUST_CLOUD_LINE_HORIZONTAL_POSITION_Y 20.0f
#define ADJUST_CLOUD_LINE_VERTICAL_POSITION_X 12.0f
#define ADJUST_CLOUD_LINE_VERTICAL_POSITION_Y 10.0f

#define ADJUST_CLOUD_ZONE_1X1_POSITION_X 38.0f
#define ADJUST_CLOUD_ZONE_1X1_POSITION_Y 45.0f

#define ADJUST_CLOUD_ZONE_2X2_POSITION_X 38.0f
#define ADJUST_CLOUD_ZONE_2X2_POSITION_Y 45.0f

//MENSAGENS DO JOGO
#define INDEX_MSG_INTERFACE 2
//#define INDEX_MSG_GAME_OVER 2 //( INDEX_CLOUD_LINE_V + 1 )
//#define INDEX_MSG_NEXT_PLAYER 3 //( INDEX_MSG_GAME_OVER + 1 )
//#define INDEX_MSG_NEXT_CPU 4 //( INDEX_MSG_NEXT_PLAYER + 1 )
//#define INDEX_MSG_CPU_WIN 5 //( INDEX_CLOUD_LINE_H + 1 )
//#define INDEX_MSG_PLAYER_WIN 6 //( INDEX_CLOUD_LINE_H + 1 )

 //chance da cpu usar powerUp=> basicamente tudo que faz é pegar o nível da partida e dividir por dez e somar 1, logo n nível 1 == 1%, e no máximo 85%
#define CHANCE_CPU_USE_POWER_UP  clamp( ( level/15 )* 100 , 0, 85 )//( ( level/50 ) + 1 ) * 100 


//tempo de certas atividades
#define TIME_FOCUS_LINE 0.5f
#define DURATION_TIME_MSG_GAME_OVER 0.5f
#define MAX_TIME_TO_PLAYER_MAKE_MOVE 30.0f//15.0F
#define TIME_TO_CPU_PLAY 27.0f//3.0f//13.0F




//identificação dos botões no jogo
#define ID_BUTTON_YES 0
#define ID_BUTTON_NO 1
#define ID_BUTTON_LINE_VIEW 2
#define ID_BUTTON_CONFIRM_MOVE 3
#define ID_BUTTON_PAUSE 4

//BOTOES DO JOGO dentro do grupo
#define INDEX_BUTTON_YES  3//7//ID_BUTTON_YES + 7
#define INDEX_BUTTON_NO  4//8//ID_BUTTON_NO + 7
#define INDEX_BUTTON_LINE_VIEW 5//9//ID_BUTTON_LINE_VIEW + 7
#define INDEX_BUTTON_CONFIRM_MOVE 6//10//ID_BUTTON_LINE_VIEW + 7
#define INDEX_BUTTON_PAUSE 7//11//ID_BUTTON_LINE_VIEW + 7



//posição dos butões
#define MANAGERS_POSITION_Y 63.0f
#define POSITION_BACKGROUND_Y 252.0f
#define POSITION_BACKGROUND_X 250.0f


//mínimo de movimentos permitidos
#define MIN_MOVES_AVAILABLE	3//1

//qte de objetos na cena
#define SCENE_N_OBJECTS 15//14//0

DotGame::DotGame( GameBaseInfo *g, LoadingView* pLoadingView ):
Scene( SCENE_N_OBJECTS ),
//OGLTransitionListener(),
SpriteListener(),
InterfaceControlListener(),
ChronometerListener(),
EventListener( EVENT_TOUCH ),
currPlayer( NULL ),
buttonsManager( NULL ),
lineManager( NULL ),
zoneManager( NULL ),
pChronometer( NULL ),
pInfoBar( NULL ),
lastState( DOT_GAME_STATE_NONE ),
state( DOT_GAME_STATE_NONE ),
level( g->level),
limit( level - 1 ),
indicePlayer( 255 ),
//GInfo( g ),
maxTime( MAX_TIME_TO_PLAYER_MAKE_MOVE ),
combo( 0 ),
stateTimeCounter( 0.0f ),
linesSelected( 0 ),
divisor( 1.0f ),
bg( NULL ),
animCamera( false ),
nActiveTouches( 0 ),
goodBye( false ),
dispMoves( MIN_MOVES_AVAILABLE ),//3 ), //MIN_MOVES_AVAILABLE ),nHorizontalZones( g.nHZones ),
nVerticalZones( g->nVZones ),
nHorizontalZones( g->nHZones ),
nHorizontalLines( nHorizontalZones + 1 ),
nVerticalLines( nVerticalZones + 1  ),
initDistBetweenTouches( 0.0f ),
mode( g->mode ),
movesMax( 200),//( nHorizontalZones + nVerticalZones ) / 2 ),
movesCount( 0 ),
nPlayers( MAX_PLAYERS/*g.nPlayers*/ ),
paused( false )
{
#if DEBUG
	LOG_FREE_MEM( "\n\nqte de mem. livre antes de carregar as coisas" );
#endif

	GInfo = g;
	memset( zones, NULL, sizeof( ZoneGame* ) * (nHorizontalZones * nVerticalZones ) );
	memset( lineH, NULL, sizeof( LineGame* ) * ( nHorizontalLines * nVerticalZones  ) );	
	memset( lineV, NULL, sizeof( LineGame* ) * (nHorizontalZones * nVerticalLines  ) );	
	memset( players, NULL, sizeof( Player* ) * nPlayers );	
	
	[ pLoadingView setProgress: 0.1 ];
	
	if(	!buildDotGame( pLoadingView ) )
	{
	
	#if DEBUG
		NSLog(@" falha no build!! ");
	#endif
	}
	
	FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* ) APP_DELEGATE;
	[hApplication pauseAllSounds: true];
		
//	[ hApplication  playAudioNamed: SOUND_INDEX_SPLASH AtIndex: SOUND_NAME_SPLASH Looping: true ];
	#if DEBUG
	LOG_FREE_MEM( "\n\nqte de mem. livre depois de carregar as coisas" );
#endif
}

/*==============================================================================================
 
 método buildDotGame => constroi os elementos do dotgame
 
 ==============================================================================================*/
bool DotGame::buildDotGame( LoadingView* pLoadingView ){
#if DEBUG
	LOG("método build chamado!!");
#endif
	// Cria os objetos que irão sofrer as transformações da câmera
	AnimatedCamera* pCurrCamera = new AnimatedCamera( this, CAMERA_TYPE_AIR, -10.0f, 10.0f );
	if( !pCurrCamera )
		goto Error;
	
	
	setCurrentCamera( pCurrCamera );
	pCurrCamera->setLandscapeMode( true );
	pCurrCamera->setPosition(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT,1.0);
	
	if( !loadPlayers() ) 
		goto Error;//NSLog(@"falha ao carregar jogadores");
#if DEBUG
	LOG_FREE_MEM("jogadores prontos");
#endif
	[ pLoadingView setProgress: 0.1 ];
	
	if( !buildBg() )
		goto Error;
#if DEBUG
	LOG_FREE_MEM("fundo pronto");
#endif
	[ pLoadingView setProgress: 0.3 ];		
	
	if( !makeTable() ){
#if DEBUG		
		NSLog(@"falha ao carregar tabuleiro");
#endif
		goto Error;
	}
	[ pLoadingView setProgress: 0.4 ];
	if( !loadCloudSprites() ){
#if DEBUG
		NSLog( @"falha ao carregar os sprites!" );
#endif
		goto Error;
	}
	
#if DEBUG	
	LOG_FREE_MEM("tabuleiro pronto");
#endif
	if( !buildButtons() ){
#if DEBUG
		LOG_FREE_MEM("falha ao carregar os botões");
#endif
		goto Error;
	}	
	[ pLoadingView setProgress: 0.45 ];
	changePlayer();	
	
	return true;
	
Error:
	clear();
	return false;
}


/*===========================================================
 
 buildButtons=> método usado para construir os elementos de interface que são fixos.
 
 =-=-------------------------------------------------------------*/
bool DotGame::buildButtons( void ){
#if DEBUG
	NSLog(@"construindo botoes");
#endif
	
	pControlsCamera = new OrthoCamera();
	if ( !pControlsCamera )
		return false;
	
	pControlsCamera->setLandscapeMode( true );
	
	buttonsManager = new ButtonsGroup( pControlsCamera );
	
	if( !buttonsManager|| ! insertObject( buttonsManager ) )
		return false;
	
	//incluir os outros botões deste ponto em diante!!
	
	//pSliceTransition = new SliceTransition( 20, 1.0, this );
	
	//pCurtainTransition = new CurtainTransition( 1.0, this, COLOR_BLACK ); 
	
	//pClosingDoorTransition = new ClosingDoorTransition( 1.0 , this, COLOR_BLACK);
	
	//pCurtainLeftTransition = new CurtainLeftTransition( 1.0 , this, COLOR_BLACK);
	
	//pCurrTransition = pSliceTransition;
	
	pInfoBar = new InfoBar( /*this->*/*GInfo ,this );
#if DEBUG
	pInfoBar->setName("infobar\n");
#endif
	if ( !pInfoBar || !buttonsManager->insertObject( pInfoBar ) )
		return false;
	
	pInfoBar->startShowAnimation();
	pInfoBar->setVisible( true );
	pInfoBar->configure( this );
	
	pChronometer = new Chronometer( this, this );
	if( !pChronometer || !buttonsManager->insertObject( pChronometer ))
		return false;
#if DEBUG
	pChronometer->setName("cronometro\n");
#endif	
	//pChronometer->setPosition(POSITION_BUTTON_PAUSE_X,POSITION_BUTTON_PAUSE_Y );
	//( (HALF_SCREEN_WIDTH - pChronometer->getWidth()  * 0.5f) ) , 40.0f );
	//pChronometer->setSize( PCHRONOMETER_WIDTH, PCHRONOMETER_HEIGHT );

	
	pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0);
	pChronometer->pause(false);
	//pChronometer->startShowAnimation();
	pChronometer->setInterfaceControlState(INTERFACE_CONTROL_STATE_SHOWN, false);
	
	//	depois fazer o mesmo com as outras msgs, não esquecer que tb tem outras 
	//	msg de game over
	Font *fonte = new Font( "ftitulos","ftitulos" );
	FeedbackTxtMsg *pGameOver = new FeedbackTxtMsg(this, fonte , false);//FeedbackMsg( "msggo\0", &frame, this );
	pGameOver->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
	pGameOver->setPosition(HALF_SCREEN_WIDTH , HALF_SCREEN_HEIGHT,0.0f);
	pGameOver->setCurrAnimType( FEEDBACK_TXT_ANIM_VIEWPORT );
	pGameOver->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	if( !pGameOver || !buttonsManager->insertObject( pGameOver ) )
		return false;
#if DEBUG
	pGameOver->setName("Msg View\n");
#endif		

	//botão de sim
	Button *pSim = new Button("sim", this );
	if( !pSim || !buttonsManager->insertObject( pSim ) )
		return false;
	pSim->setPosition( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	pSim->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
#if DEBUG
	pSim->setName("botao sim \n");
#endif
	
	
	//botao de não
	Button* pNao = new Button( "nao",this );
	if( !pNao || !buttonsManager->insertObject( pNao ) )
		return false;
	pNao->setPosition( HALF_SCREEN_WIDTH + 80, HALF_SCREEN_HEIGHT + 80 );
	pNao->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
#if DEBUG
	pNao->setName("botao nao \n");
#endif
	
	TextureFrame frame( 0.0f, 0.0f, 38.0f, 31.0f, 0.0f, 0.0f );
	Button* pLines = new Button( "mapa", this );
	pLines->setImageFrame( frame );
	if( !pLines || !buttonsManager->insertObject( pLines ) )
		return false;
	pLines->setPosition( POSITION_BUTTON_LINE_X, POSITION_BUTTON_LINE_Y);//HALF_SCREEN_WIDTH - 64.0f , SCREEN_HEIGHT - 32.0f);
	
	pLines->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
#if DEBUG
	pLines->setName("botao Linhas \n");
#endif
	
	//botão de confirmar movimento
	
	Button* pOk = new Button( "ok",this );
	if( !pOk || !buttonsManager->insertObject( pOk ) )
		return false;
	pOk->setPosition( SCREEN_WIDTH - 164.0f , SCREEN_HEIGHT - 32.0f);
	pOk->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
#if DEBUG
	pOk->setName("botao ok \n");
#endif
	
	frame.set( 0 ,0 ,36.0 ,39.0 ,0.0 ,0.0);
	Button* pPause = new Button( "pause",this );
	if( !pPause || !buttonsManager->insertObject( pPause ) )
		return false;
	//pPause->setImageFrame( frame );
	pPause->setPosition( SCREEN_WIDTH - 36.0f , SCREEN_HEIGHT - 36.0f);
	pPause->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
#if DEBUG
	pPause->setName("botao pause \n");
#endif
	return true;
}


/*======================================================================================
 
 constrói o background
 
 ==================================================================================== */
bool DotGame::buildBg( void ){

	bg = BoardFactory::GetInstance()->getNewBoard(); 
	lim.set(  nHorizontalZones *  ZONE_SIDE + ( ( nHorizontalLines ) * LINE_HORIZONTAL_HEIGHT ) ,
		  ( nVerticalLines ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE));
	min = *bg->getObject( 1 )->getPosition();
	
#if DEBUG
	NSLog(@"fundo com certeza criado");
#endif
	return insertObject( bg );

}

/**==========================================================================
 
 constrói o tabuleiro, com os pontos e zonas necessárias. 
 
 ==========================================================================*/

bool DotGame::makeTable( void ){
	
	zoneManager = new ZoneGroup( *GInfo );
	zoneManager->setQteZones( nHorizontalZones, nVerticalZones);
	if( !zoneManager )//|| !insertObject( zoneManager ) )
		return false;
	
	lineManager = new LineGroup( *GInfo );
	lineManager->setTotalLines( nHorizontalLines , nVerticalLines, nHorizontalZones , nVerticalZones );
	if( !lineManager ||	!insertObject( lineManager ) )
		return false;
	
	bg->setLineGroup(lineManager);
	bg->setZoneGroup(zoneManager);
	pPowerUpGroup = new PowerUpGroup();
	if( !pPowerUpGroup || !insertObject( pPowerUpGroup ) )
		return false;

	uint8 x,y;
	PowerUp *pu;
	for ( x = 0; x < nHorizontalZones; x++) {
		for ( y = 0; y< nVerticalZones ; y++ ){
			zones[ x ][ y ] = new ZoneGame();
			zones[ x ][ y ]->setPosition( x, y );
			
			if( ( x >= nHorizontalZones /2  ) && ( y == nVerticalZones - 1  ) ){
				pu = PowerUpFactory::GetInstance()->getNewPowerUp( POWER_UP_TYPE_DESTROY_ZONE , this) ;
					zones[ x ][ y ]->setPowerUp( pu );
				pu->setPosition(x, y);
				pPowerUpGroup->adPowerUp( zones[ x ][ y ]->getPowerUp() );
					//PowerList.push_back( z[ i ][ j ]->getPowerUp() );

				}
			
			}
	}
	
	for ( x = 0; x < nHorizontalZones ; x++ ) {
		for ( y = 0; y < nVerticalLines  ; y++) {
			lineH[ x ][ y ] = new LineGame( LINE_GAME_ORIENTATION_HORIZONTAL/*, GInfo.idBg*/ );
			lineH[ x ][ y ]->setPosition( x , y );
		}
		
	}
	
	for ( x = 0; x < nHorizontalLines  ; x++ ) {
		for ( y = 0; y < nVerticalZones ; y++) {
			lineV[ x ][ y ] = new LineGame( LINE_GAME_ORIENTATION_VERTICAL/*, GInfo.idBg*/ );
			lineV[ x ][ y ]->setPosition( x , y );
		}
	}
	

	//lineManager->adjustAllLines( BoardFactory::GetInstance()-> );
//	lineManager->setVisible(false);
//	zoneManager->setVisible(false);
//	pPowerUpGroup->setVisible(false);
//	bg->setVisible(false);
	
	
	Point3f p;
	p.set(  nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
		  nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
		  1.0);
	p *= 0.5f;
	min.set(  nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
			nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
			1.0);
	zoneManager->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0 );
	lineManager->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0 );
	bg->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0 );
	pPowerUpGroup->setPosition(HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0);
	lineManager->insertLinesH( lineH );
	lineManager->insertLinesV( lineV );
	zoneManager->insertZones( zones , this);
	return true;
	
}


/*===============================================================================
 
 Atualiza o objeto
 
 ===============================================================================*/
bool DotGame::update( float timeElapsed ){
	if( animCamera )	{
		AnimatedCamera* pCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );
		pCamera->update( timeElapsed );
	}
#if DEBUG
	/*static uint16 counter1 = 0, counter2 = 0;
	static float total1 = 0.0f, total2 = 0.0f;
	float t1,t2, t3, t4;
	t1 = Utils::GetTimeStamp();
	*/
#endif
	switch ( state ) {
		case DOT_GAME_STATE_NONE:
		case DOT_GAME_STATE_PLAYER_WAITING :
		case DOT_GAME_STATE_PAUSED:
			
			break;
			
		case DOT_GAME_STATE_AFTER_CPU_MOVE :
			
		/*	if( !haveFreeZone() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
				else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
				}
				
				//	setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER);//DOT_GAME_STATE_GAME_OVER );
				//	setDotGameState(DOT_GAME_STATE_ANIMATING_TRANSITION_1);
			}
			*/
			break;
			
		case DOT_GAME_STATE_CPU_PLAYING:
			
			if( ( fleq(pChronometer->getCurrTime() , TIME_TO_CPU_PLAY )&& !pChronometer->isPaused())) {
				cpuMove();
				
			}
			
			break;
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:		
			//pCurrTransition->update( timeElapsed );
			break;
			
		case DOT_GAME_STATE_FOCUSING_LINE:
		{
			//			AnimatedCamera* pCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );
			//			pCamera->update( timeElapsed );
		}
			break;
		case DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_CPU_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER:
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER:{
			
			if( stateTimeCounter >= 0.0f ){
				stateTimeCounter += timeElapsed;
				if( stateTimeCounter >= stateDuration ){
					stateTimeCounter = 0.0;
					stateDuration = -1.0f;
					
					if( state == DOT_GAME_STATE_SHOWING_MSG_GAME_OVER ){
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_GAME_OVER );
						//return;
					}else if( state == DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER ){
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER );
						
					}else if( state == DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN ){
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN );
						
					}else if( state == DOT_GAME_STATE_SHOWING_MSG_CPU_WIN )
						
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_CPU_WIN );
				}
			}
		}
			
			break;
			
		default:
			break;
	}
#if DEBUG
//	t3 = t2 = Utils::GetTimeStamp();
/*	
	if( ++counter1 <= 500 ){
		t3 = t2 = Utils::GetTimeStamp();
		total1 += ( t2 - t1 );
	}else {
		printf("\n\n media do tempo resultante : %3.10f \n\n",total1/500.0);
		counter1= total1 = 0;
	}
	*/
	bool ret = Scene::update( timeElapsed );
#else	

	return Scene::update( timeElapsed );
#endif
	
#if DEBUG
	
/*	if( ++counter2 <= 500 ){
		t4 = Utils::GetTimeStamp();
		total2 += ( t4 - t3 );
	}else {
		printf("\n\n media do tempo resultante 2: %3.10f \n\n",total2/500);
		counter2 = total2 = 0;
	}
	*/
	
//	printf("\n\n tempo resultante 2 : %3.10f -%3.10f = %3.10f \n\n",t4,t3,t4 - t3);
	return ret;
#endif
}

/*===========================================================================================
 
 MÉTODO handleEvent
 Método que trata os eventos enviados pelo sistema.
 
 ============================================================================================*/

bool DotGame::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ){
	
	if( evtType != EVENT_TOUCH )
		return false;
	// Obtém a informação do toque
	const NSArray* hTouchesArray = ( NSArray* )pParam;
	
	// OBS : Acho que isso nunca acontece, mas...
	uint8 nTouches = [hTouchesArray count];
	if( nTouches == 0 )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
		{
			int8 touchIndex = -1;
			for( uint8 i = 0 ; ( i < nTouches ) && ( nActiveTouches < MAX_TOUCHES ) ; ++i )	
			{
				if( ( touchIndex = startTrackingTouch( [hTouchesArray objectAtIndex:i] ) ) >= 0 )
					onNewTouch( touchIndex, &trackedTouches[i].startPos );
			}
		}
			return true;
			
		case EVENT_TOUCH_MOVED:
			// Se possuímos mais de um toque, estamos recebendo uma operação de zoom
#if MAX_TOUCHES > 1
			if( nActiveTouches > 1 ) // No caso deste jogo, > 1 será sempre == 2 
			{
				if( ( nTouches == 1 || state != DOT_GAME_STATE_OBSERVING /*|| state!= DOT_GAME_STATE_PLAYER_WAITING*/) )
					return false;
				
				
				int8 touchIndex = -1;
				Point3f aux[ MAX_TOUCHES ] = { trackedTouches[0].startPos, trackedTouches[1].startPos };
				
				for( uint8 i = 0 ; i < nTouches ; ++i )
				{
					UITouch* hTouch = [hTouchesArray objectAtIndex:i];
					if( ( touchIndex = isTrackingTouch( hTouch ) ) >= 0 )
					{
						CGPoint currTouchPos = [hTouch locationInView: NULL];
						aux[ touchIndex ].set( currTouchPos.x, currTouchPos.y );
					}
				}
				
				// Se os toques estão se aproximando, é um zoom out
				float currZoom = pCurrCamera->getZoomFactor();
				float newDistBetweenTouches = ( aux[0] - aux[1] ).getModule();
				float movement = fabsf( newDistBetweenTouches - initDistBetweenTouches );
				if( movement < ZOOM_MIN_DIST_TO_CHANGE )
					return false;
#if DEBUG
//				NSLog(@"currCameraZoom %1.3f",pCurrCamera->getZoomFactor());		
#endif			
				if( NanoMath::fltn( newDistBetweenTouches, initDistBetweenTouches ) )
				{

					float zoomMin = adjustMinZoom();					
					float newZoom = currZoom * powf( ZOOM_OUT_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
					
					if( newZoom < zoomMin )//ZOOM_MIN )
						pCurrCamera->zoom( zoomMin );//ZOOM_MIN );
					else
						pCurrCamera->zoom( newZoom );
				}
				// Se os toques estão se afastando, é um zoom in
				else if( NanoMath::fgtn( newDistBetweenTouches, initDistBetweenTouches ) )
				{
					float newZoom = currZoom * powf( ZOOM_IN_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );

					if( newZoom > 1.0f/*ZOOM_MAX*/ )
						pCurrCamera->zoom( 1.0f/*ZOOM_MAX*/ );
					else
						pCurrCamera->zoom( newZoom );
				}
				
				// Garante que o zoom não fez a câmera extrapolar os limites
//#if ! CONFIG_FREE_CAM_MOVEMENT
				onMoveCamera( 0.0f, 0.0f );
//#endif
				
#if DEBUG
				
				LOG( "Curr Camera Zoom: %.3f\n", pCurrCamera->getZoomFactor() );
				
#endif
				
				initDistBetweenTouches = newDistBetweenTouches;
			}
			
			else
			{
#endif
				UITouch* hTouch = [hTouchesArray objectAtIndex:0];
				int8 touchIndex = isTrackingTouch( hTouch );
				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					CGPoint lastPos = [hTouch previousLocationInView:NULL];
					
					Point3f touchCurrPos( currPos.x, currPos.y );
					Point3f touchLastPos( lastPos.x, lastPos.y );
					
					onSingleTouchMoved( touchIndex, &touchCurrPos, &touchLastPos );
				}
#if MAX_TOUCHES > 1
			}
#endif
			return true;
			
			
		case EVENT_TOUCH_CANCELED:
		case EVENT_TOUCH_ENDED:
			for( uint8 i = 0 ; i < nTouches ; ++i )
			{
				UITouch* hTouch = [hTouchesArray objectAtIndex:i];
				int8 touchIndex = isTrackingTouch( hTouch );
				
				if( touchIndex >= 0 )
				{
					CGPoint tmp = [hTouch locationInView:NULL];
					Point3f currPos;
					currPos.set( tmp.x, tmp.y );
					currPos = Utils::MapPointByOrientation( &currPos );
					
					Point3f touchPos( currPos.x, currPos.y );
					
//#if DEBUG
					// Verifica se houve um toque duplo
					if( [hTouch tapCount] == 2 )
					{
						// OBS: Não é utilizado nesse jogo
						// Envia evento de toque duplo
						onDoubleClick( touchIndex );
					}
//#endif
					
					onTouchEnded( touchIndex, &touchPos );
					
					// Cancela o rastreamento do toque
					stopTrackingTouch( touchIndex );
				}
			}
			return true;
	}
	return false;
}

// Chamado quando o sprite muda de etapa de animação
void DotGame::onAnimStepChanged( Sprite* pSprite ){
	
	switch ( state ) {
		case DOT_GAME_STATE_ANIMATING_LINE :{
			
			
			if( pSprite->getCurrAnimSequenceStep() == 4 ){
				[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
				Point3f p;
				//linha pressionada 
				//if( movesMake.empty() )
					
				p = movesMake.back();					
				lineManager->selectLine( &p, currPlayer );
				
				
			//começo das verificações das zonas e bigzonas aqui
#if DEBUG
		//		NSLog(@"testando as zonas no passo 4 da animação!");
#endif
				
				if ( lineManager->isLineHorizontal( &p ) ){
//					res = 
					if( verifyZonesHorizontal( &p ) )
						--linesSelected;
				}else /*linha vertical*/{
//					res = verifyZonesVertical( &p );
					if( verifyZonesVertical( &p ) )
						--linesSelected;
				}
			}
		}
			break;
		case DOT_GAME_STATE_ANIMATING_ZONE :{
			if( pSprite->getCurrAnimSequenceStep() == 4 ){
//				Point3f p;
//				p = *pSprite->getPosition();
//				p.set( p.x + ADJUST_CLOUD_ZONE_1X1_POSITION_X,
//					  p.y + ADJUST_CLOUD_ZONE_1X1_POSITION_Y );
//				zoneManager->zoneGainedByPlayer(&p, currPlayer );
				//p = movesMake.back();					
				//lineManager->selectLine( &movesMake.back(), currPlayer );
				
//			verificação das zonas aqui
				Point3f p;
				p = *pSprite->getPosition();
				p.set( p.x +  ADJUST_CLOUD_ZONE_2X2_POSITION_X, 
					  p.y + ADJUST_CLOUD_ZONE_2X2_POSITION_X);
				zoneManager->setZoneVisible(&p, true);
			}
		}
			break;
		case DOT_GAME_STATE_ANIMATING_BIG_ZONE :{
		
		}		
			break;
			case DOT_GAME_STATE_ANIMATING_POWER_UP:
			if( pSprite->getCurrAnimSequenceStep() == 4 ){
				pPowerUpGroup->getCurrPowerUp()->powerUpDo( this );
			}
				break;

			
	}
	
}

// Chamado quando o sprite termina um sequência de animação
void DotGame::onAnimEnded( Sprite* pSprite ){
	
	pSprite->setVisible( false );
	switch ( state ) {
		case DOT_GAME_STATE_ANIMATING_LINE :{
#if DEBUG
//			NSLog(@"testando as zonas no fim da animação das linhas");
#endif	
			if( !lineManager->isLineGained( &movesMake.back() ) ){
				lineManager->selectLine( &movesMake.back(), currPlayer );
				if ( lineManager->isLineHorizontal( &movesMake.back() ) ){
					if( verifyZonesHorizontal( &movesMake.back() ) )
						--linesSelected;
				}else /*linha vertical*/{
					if( verifyZonesVertical( &movesMake.back() ) )
						--linesSelected;
				}
			}
			movesMake.pop_back();

			setDotGameState( ( currPlayer->getTypeUser() == PLAYER_USER_CPU ? DOT_GAME_STATE_AFTER_CPU_MOVE : DOT_GAME_STATE_AFTER_PLAYER_MOVE ) );
#if DEBUG
			//NSLog(@"fim da animação da linha");
#endif	
		}
			break;
		case DOT_GAME_STATE_ANIMATING_ZONE :{
			
			if( !lineManager->isLineGained( &movesMake.back() ) ){
			//	lineManager->selectLine( &movesMake.back(), currPlayer );
				if ( lineManager->isLineHorizontal( &movesMake.back() ) ){
					if( verifyZonesHorizontal( &movesMake.back() ) )
						--linesSelected;
				}else /*linha vertical*/{
					if( verifyZonesVertical( &movesMake.back() ) )
						--linesSelected;
				}
			}
			//}
			movesMake.pop_back();
			/*Point3f p;
			p = *pSprite->getPosition();
			p.set( p.x + ADJUST_CLOUD_ZONE_1X1_POSITION_X,
				  p.y + ADJUST_CLOUD_ZONE_1X1_POSITION_Y );
			//if(){
			zoneManager->zoneGainedByPlayer(&p, currPlayer );*/
			setDotGameState( ( currPlayer->getTypeUser() == PLAYER_USER_CPU ? DOT_GAME_STATE_AFTER_CPU_MOVE : DOT_GAME_STATE_AFTER_PLAYER_MOVE ) );
#if DEBUG
		//	NSLog(@"fim da animação da zona");
#endif	
			}
			break;
		case DOT_GAME_STATE_ANIMATING_BIG_ZONE :{
			movesMake.pop_back();
			
			setDotGameState( ( currPlayer->getTypeUser() == PLAYER_USER_CPU ? DOT_GAME_STATE_AFTER_CPU_MOVE : DOT_GAME_STATE_AFTER_PLAYER_MOVE ) );
#if DEBUG
		//	NSLog(@"fim da animação da big zona");
#endif	
					
		}		
			break;
			
	}
	
}


//chamado qdo o power up é liberado!!!
void DotGame::PowerUpEffect( PowerUp* p ){
#if DEBUG
//	NSLog(@"PowerUpAtivado");
#endif
	if( p->isAnimed() ){
		p->getAnimation()->setListener( this );
		pPowerUpGroup->setCurrAnim( p );
		pPowerUpGroup->setCurrPowerUp( p );
	}
//	if( state == DOT_GAME_STATE_WAITING_POWER_UP_INPUT ){
//		return;
//	}
//	setDotGameState( DOT_GAME_STATE_WAITING_POWER_UP_INPUT );

}
// Método chamado para indicar que a animação do controle foi finalizada
void DotGame::onInterfaceControlAnimCompleted( InterfaceControl* pControl ){
	
	switch ( state ) {
		case DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_HIDING_MSG_CPU_WIN:
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
			break;
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER :

			break;
		case DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER:
			changePlayer();
			break;
			
		case DOT_GAME_STATE_HIDING_MSG_GAME_OVER:{
			setDotGameState( DOT_GAME_STATE_GAME_OVER );
		}
			break;
			
		default:
			break;
	}
}
/*========================================-==-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-==-=-=-
 
 onStopTimeReached->Método chamado quando o cronômetro termina sua contagem
 
 ========================================-==-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-==-=-=-*/
void DotGame::onStopTimeReached( void ){
	
	switch ( state ) {
		case DOT_GAME_STATE_CPU_PLAYING :
		case DOT_GAME_STATE_PLAYER_WAITING :
			setDotGameState(DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER);
			//changePlayer();
			//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
			break;
		case DOT_GAME_STATE_AFTER_CPU_MOVE:
		case DOT_GAME_STATE_AFTER_PLAYER_MOVE:
		case DOT_GAME_STATE_ANIMATING_LINE:
		case DOT_GAME_STATE_ANIMATING_VICTORY:
		case DOT_GAME_STATE_PAUSED:
		case DOT_GAME_STATE_SHOWING_INFO_PLAYER:
		case DOT_GAME_STATE_GAME_OVER:
		case DOT_GAME_STATE_OBSERVING:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
		case DOT_GAME_STATE_FOCUSING_LINE:
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER:
		case DOT_GAME_STATE_HIDING_MSG_GAME_OVER:
			break;
	}
	
}


/*===================================================================================
 
 Método chamado quando a transição termina
 
 ===================================================================================*/

/*void DotGame::onOGLTransitionEnd( void ){
	//colocar todos os estados possíveis aqui e a reação à cada um deles...
	switch ( state ) {
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1 :
			pChronometer->setTimeInterval(  maxTime / divisor, 0.0f);
			pChronometer->pause( true );
			pCurrCamera->setPosition( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT, 1.0f );
			setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_2 );
			break;
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
			//changePlayer();		
			pChronometer->pause( false );
			//setDotGameState( DOT_GAME_STATE_PLAYER_WAITING );
			break;
			
		default:
			break;
	}
	
}*/
/*==========================================================================================
 
 Indica que a animação da câmera terminou
 
 =========================================================================================*/
void DotGame::onCameraAnimEnded( void ){
	animCamera = false;
	
	switch ( state ) {
		case DOT_GAME_STATE_FOCUSING_LINE:
		{
				
#if DEBUG
				//NSLog(@"verificar as zonas");
				Point3f tmp;
				tmp = movesMake.back() ;
			//	NSLog(@"movimento: x=%4.0f y=%4.0f", tmp.x, tmp.y );
			//NSLog(@"%d", linesSelected);
				
#endif
				bool res = false;
				if( !movesMake.empty() ){
//					Point3f p;
					//linha pressionada 
//					p = movesMake.back();					
//					lineManager->selectLine( &p, currPlayer );
					
//					if ( lineManager->isLineHorizontal( &p ) ){
//						res = verifyZonesHorizontal( &p );
//						if( res )
//							--linesSelected;
//					}else /*linha vertical*/{
//						res = verifyZonesVertical( &p );
//						if( res )
//							--linesSelected;
//					}
//					movesMake.pop_back();

					setDotGameState( DOT_GAME_STATE_ANIMATING_LINE );
				}else {
					if( !res ){
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );}
					else {
						//linesSelected = 0;
					}

				}
			}
			break;
		case DOT_GAME_STATE_CPU_PLAYING:
			break;
		default:
			break;
	}
	
	
}


/*===========================================================================================
 carrega os jogadores, e os seus respectivos bonecos...
 
 ===========================================================================================*/
bool DotGame::loadPlayers( void ){

	for (uint8 c = 0 ; c < nPlayers ; c++) {
		players[ c ] = new Player();
		players [ c ]->LoadInfo( *GInfo, c );
		if( !players[ c ] ){
#if DEBUG
	//		NSLog( @"falha ao carregar o players" );
#endif
			return false;
		}
		players [ c ]->setTypeUser( PLAYER_USER_CPU );	
	}
	
	currPlayer = players[ 0 ];	
	//players [ 1 ]->setTypeUser( PLAYER_USER_USER );	
	
	
	//acerta os jogadores de acordo com o modo de jogo, o ideal seria se fosse em LoadInfo
	switch ( mode ) {
		case DOT_GAME_MODE_VERSUS_PLAYER_LOCAL :
			players[ 0 ]->setTypeUser( PLAYER_USER_USER );
			//break;
		case DOT_GAME_MODE_VERSUS_CPU :
			players[ 1 ]->setTypeUser( PLAYER_USER_USER );
			break;
	}
	return true;
}


/*===========================================================================================
 
 MÉTODO startTrackingTouch
 Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
 fazê-lo.
 
 ============================================================================================*/
int8 DotGame::startTrackingTouch( const UITouch* hTouch ){
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == 0 )
		{
			++nActiveTouches;
			
			trackedTouches[i].Id = ( int32 )hTouch;
			
			CGPoint aux = [hTouch locationInView: NULL];
			trackedTouches[i].unmappedStartPos.set( aux.x, aux.y );
			Point3f p;
			p.set( aux.x, aux.y );
			
			//aux = Utils::MapPointByOrientation( &aux );
			p = Utils::MapPointByOrientation( &p );
			trackedTouches[i].startPos.set( p.x, p.y );
			
			if( nActiveTouches == 2 )
				initDistBetweenTouches = ( trackedTouches[0].startPos - trackedTouches[1].startPos ).getModule();
			
			return i;
		}
}
	return -1;	
}


/*===============================================================================
 
 MÉTODO stopTrackingTouch
 Cancela o rastreamento do toque recebido como parâmetro. 
 
 ===============================================================================*/
void DotGame::stopTrackingTouch( int8 touchIndex ){
	if( touchIndex < 0 )
		return;
	
	if( trackedTouches[ touchIndex ].Id != 0 )
	{
		--nActiveTouches;
		trackedTouches[ touchIndex ].Id = 0;
		initDistBetweenTouches = 0.0f;
	}
}

/*===========================================================================================
 
 MÉTODO onSingleTouchMoved
 Trata eventos de movimentação de toques.
 
 ============================================================================================*/
void DotGame::onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos ){

	Point3f currPos = Utils::MapPointByOrientation( pCurrPos );
	Point3f lastPos = Utils::MapPointByOrientation( pLastPos );

	switch ( state ) {
	//case DOT_GAME_STATE_PLAYER_WAITING:
			//break;
		case DOT_GAME_STATE_PLAYER_WAITING:
			pInfoBar->checkCollision( &currPos , this, false);
		case  DOT_GAME_STATE_OBSERVING :
		{
			Ray r;
			getPickingRay( r, *pCurrPos );
			
		//	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
		//	Point3f currPos = Utils::MapPointByOrientation( pCurrPos );
			
			onMoveCamera( ( currPos.x - lastPos.x ) /** invZoom*/, ( currPos.y - lastPos.y ) /** invZoom*/ );			
		}	
			break;
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1 :
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2 :				
			break;


		case DOT_GAME_STATE_GAME_OVER:{

			buttonsManager->idButtonPressed( &currPos );
		}
			break;

	}
	
}

/*===========================================================================================
 
 MÉTODO isTrackingTouch
 Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja sendo
 acompanhado.
 
 ============================================================================================*/

int8 DotGame::isTrackingTouch( const UITouch* hTouch ){
	
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == ( int32 )hTouch )
			return i;
	}
	return -1;
	
}

/*===========================================================================================
 
 MÉTODO onMoveCamera
 Movimenta a câmera respeitando as restrições de posição.
 
 ============================================================================================*/

void DotGame::onMoveCamera( float dx, float dy ){

	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	float halfCamViewVolumeWidth = ( SCREEN_WIDTH * invZoom ) * 0.5f;
	float halfCamViewVolumeHeight = ( SCREEN_HEIGHT * invZoom ) * 0.5f;

	Point3f camPos = *( pCurrCamera->getPosition() );
	
	camPos.x += dx;
	camPos.y += dy ;

	Point3f lim2, min2;

	min2 = *bg->getObject( 1 )->getPosition();	
	min2 += *lineManager->getPosition();
	lim2 = LineGroup::getLineCoordenatesByXY( nHorizontalLines + 1, nVerticalLines + 1 , false );
	if( lim2.x >= SCREEN_WIDTH ){
		
		if( camPos.x  + halfCamViewVolumeWidth > lim2.x ){ 
			
			camPos.x -=  ( camPos.x + halfCamViewVolumeWidth ) -lim2.x;
		} else	if( camPos.x - halfCamViewVolumeWidth < min2.x  ){
			
			camPos.x += min2.x - ( camPos.x - halfCamViewVolumeWidth );
		}
	}else 
		if( camPos.x > lim2.x ){
			camPos.x = lim2.x;
		}else if( camPos.x < ( min2.x + lineManager->getPosition()->x  )) {
			camPos.x = (min2.x + lineManager->getPosition()->x  );
		}

	if( lim2.y > SCREEN_HEIGHT ){
		if( camPos.y - halfCamViewVolumeHeight < ( min2.y ) ){
			
			camPos.y += ( min2.y  ) - ( camPos.y - halfCamViewVolumeHeight ) ;
		}else
			if( camPos.y + halfCamViewVolumeHeight > lim2.y ) {	
			camPos.y  -= ( camPos.y + halfCamViewVolumeHeight ) - lim2.y  ;
			
		}
	}else 
		if( camPos.y > lim2.y ){
			camPos.y = lim2.y ;
		}else if( camPos.y   < (min2.y + lineManager->getPosition()->y )) {
			camPos.y = (min2.y + lineManager->getPosition()->y );
		}
	
	pCurrCamera->setPosition(camPos.x, camPos.y, 0.0f ); 	
	
#if DEBUG 
	 Point3f currCamPos = *( pCurrCamera->getPosition() );
	currCamPos.set(currCamPos.x + halfCamViewVolumeWidth, currCamPos.y + halfCamViewVolumeHeight );
	
	//LOG( "Cam Pos: x = %.3f, y = %.3f -> %.3f, %.3f -> %.3f, %.3f\n", currCamPos.x, currCamPos.y, min2.x, min2.y, lim2.x, lim2.y );
//	 LOG( "Cam Pos: x = %.3f, y = %.3f -> %.3f, %.3f -> %.3f, %.3f\n", currCamPos.x, currCamPos.y/*, min2.x, min2.y, lim2.x, lim2.y */);
#endif
 }
/*===========================================================================================
 
 MÉTODO getPickingRay
 Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
 jogo.
 
 ============================================================================================*/

void DotGame::getPickingRay( Ray& ray, const Point3f& p ) const{
	
	
	Utils::GetPickingRay( &ray, pCurrCamera, p.x, p.y );
	
	// Corrige a origem do raio, já que esta será sempre a posição do observador (centro da tela)
	// OBS: Esta abordagem está PORCA!!!!! O certo seria utilizarmos:
	// Utils::Unproject( &p, pCurrCamera, &trackedTouches[ touchIndex ].unmappedStartPos );
	// No entanto, a saída de Utils::Unproject não está produzindo valores corretos com a projeção ortogonal
	
	float invZoom = 1.0f;// / pCurrCamera->getZoomFactor();
	Point3f screenCenter( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	ray.origin += ( Utils::MapPointByOrientation( &p ) - screenCenter ) * invZoom;
	
	
}


/*======================================================================
 
 Trata eventos de início de toque
 
 ======================================================================*/
void DotGame::onNewTouch( int8 touchIndex, const Point3f* pTouchPos ){
	/*
	 todoo: colocar todos os estados possíveis aqui e a reações possíveis em cada um desses estados!!!
	 */
	
	// if( mode == DOT_GAME_MODE_EXIBITHION )
	//	goodBye = true;
	
	switch ( state ) {
		case DOT_GAME_STATE_PLAYER_WAITING:
			
			/*			if( nActiveTouches > 1 )
			 {
			 break;
			 }
			 
			 
			 break;*/
		case DOT_GAME_STATE_CPU_PLAYING:
			pInfoBar->checkCollision( pTouchPos, this ,false);
		case DOT_GAME_STATE_GAME_OVER:
			buttonsManager->idButtonPressed( pTouchPos );
			break;
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			break;
		
			
		default:
			break;
	}
	
}

/*======================================================================
 
 Trata eventos de fim de toque
 
 ======================================================================*/

void DotGame::onTouchEnded( int8 touchIndex, const Point3f* pTouchPos ){
	/*
	 todoo: colocar todos os estados possíveis aqui e a reações possíveis em cada um desses estados!!!
	 */
	switch ( state ) {
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			break;
		case DOT_GAME_STATE_WAITING_POWER_UP_TARGET_ZONE_INPUT:
		case DOT_GAME_STATE_WAITING_POWER_UP_TARGET_LINE_INPUT :{
			
			Point3f p;
			p = getRelativePosition( pTouchPos );
			//*pTouchPos;
			
			if( state == DOT_GAME_STATE_WAITING_POWER_UP_TARGET_LINE_INPUT ){
				if( lineManager->isLineGained( &p ) ){
					currPlayer->inputPowerUp( &p );
					//currPlayer->doPowerUp( this );
				}
			}else if( state == DOT_GAME_STATE_WAITING_POWER_UP_TARGET_ZONE_INPUT ) {
				if( zoneManager->isZoneOccupied( &p ) ){
					currPlayer->inputPowerUp( &p );
					//currPlayer->doPowerUp( this );
				}
			}
		}//break;

		case DOT_GAME_STATE_PLAYER_WAITING :
	
		case DOT_GAME_STATE_CPU_PLAYING:{
		 if( pInfoBar->checkCollision( pTouchPos, this , true) )
			 break;	}
		case DOT_GAME_STATE_GAME_OVER:
		case DOT_GAME_STATE_OBSERVING:{

			uint8 b;
			if( buttonsManager->idButtonPressed(pTouchPos, &b) ){
				switch ( b ) {
					case ID_BUTTON_LINE_VIEW:
						switch ( state ) {
							case DOT_GAME_STATE_OBSERVING :
								pChronometer->pause( false );
								lineManager->setAllLinesLastState();
								bg->setObserving( false );
								zoneManager->setAllZonesObserved( false );
								pPowerUpGroup->setPowerUpObserving( false );
								setDotGameState( lastState );
								break;
							case DOT_GAME_STATE_PLAYER_WAITING:
							case DOT_GAME_STATE_CPU_PLAYING :
								setDotGameState( DOT_GAME_STATE_OBSERVING );
								break;
						}
						break;
					
					case ID_BUTTON_YES:
						switch ( state ) {
							case DOT_GAME_STATE_GAME_OVER:
								resetTable();
								break;
								
						}
						break;
						
					case ID_BUTTON_NO:
						switch ( state ) {
							case DOT_GAME_STATE_GAME_OVER:
								goodBye = true;
								break;
						}
						break;
					case ID_BUTTON_CONFIRM_MOVE:{
						if( state == DOT_GAME_STATE_PLAYER_WAITING ){
							setDotGameState( DOT_GAME_STATE_AFTER_PLAYER_MOVE );
						}else if( state == DOT_GAME_STATE_WAITING_POWER_UP_TARGET_LINE_INPUT ||
								  state == DOT_GAME_STATE_WAITING_POWER_UP_TARGET_ZONE_INPUT )  {
								currPlayer->doPowerUp( this );
						}

					
					}
						break;
						case ID_BUTTON_PAUSE:
							if ( state == DOT_GAME_STATE_PLAYER_WAITING ) {
								setDotGameState( DOT_GAME_STATE_PAUSED ); 
								
							}
							break;

				}
				break; 
#if DEBUG
				//NSLog(@"botao indice %d",b);
				//NSLog(@" nome botao %s\n", buttonsManager->getObject( 7 + b)->getName() );
#endif
			}
		
			if( state == DOT_GAME_STATE_PLAYER_WAITING ){
				if( currPlayer->getTypeUser() == PLAYER_USER_USER ){
					linePressed( pTouchPos );
				}
				
			}
		}break;
	}
}
//}

/*-================================================================
 
 Move a câmera para a posição 'position' real em 'time' segundos
 
 ===================================================================*/
void DotGame::moveCameraTo( const Point3f* pRealPosition, float time ){
	moveCameraTo( pRealPosition, /*pCurrCamera->getZoomFactor()*/1.0f, time );
	
}

void DotGame::moveCameraTo( const Point3f* pRealPosition, float finalZoom, float time ){
	//	if( !movingCamera )
	//		movingCamera = true;
	//	else {
	//		return;
	//	}
	
	Point3f lim2, min2;
//	lim2 = *bg->getObject( 0 )->getSize();
//	min2 = *bg->getObject( 0 )->getPosition();
//	Point3f sc(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
//	lim2.x -= SCREEN_WIDTH * 1.2;
//	min2 += sc;
	
	Point3f d = *pRealPosition;
	d += *lineManager->getPosition();
	
	//Point3f d = getRelativePosition( pRealPosition );
	
	min2 = *bg->getObject( 1 )->getPosition();	
	min2 += *lineManager->getPosition();
	min2.set( min2.x + HALF_SCREEN_WIDTH, min2.y + HALF_SCREEN_HEIGHT );

	lim2 = LineGroup::getLineCoordenatesByXY( nHorizontalLines + 1, nVerticalLines + 1 , false );

	if( d.x <= min2.x ){
		d.x = min2.x;
	}else if( d.x >= lim2.x) {
		d.x = lim2.x ;
	} 
	
	if( d.y <= min2.y ){
		d.y = min2.y;
	}else if( d.y > lim2.y ){ 
		d.y = lim2.y;
	}

//NSLog(@"destino: ( %3.2f, %3.2f, %3.2f )", d.x, d.y, d.z);
	AnimatedCamera *pSceneCamera = dynamic_cast<AnimatedCamera*> ( pCurrCamera );
	pSceneCamera->setAnim(&d, finalZoom, time);
	pSceneCamera->startAnim();
	animCamera = true;
	if ( ( time <= 0.0f ) || ( ( d == *( pSceneCamera->getPosition() ) ) && ( pSceneCamera->getZoomFactor() == finalZoom ) ) ) {
		pSceneCamera->endAnim();
	}
	
}


/*=============================================================================================================
 
 retorna a linha que foi pressionada
 
 =============================================================================================================*/
bool DotGame::linePressed( const Point3f *p ){
	
#if DEBUG
	//NSLog(@"linha pressionada %d",movesMake.size() );
	//NSLog(@"posicao do toque( %3.2f, %3.2f)",p->x,p->y);
#endif
	
	bool /*r1,r2,*/r3;
	r3 = false;
	Point3f ret =  getRelativePosition( p );
	
//	if ( lineManager->linePressed( &ret , currPlayer) ){
//		r3 = true;
//		inputMoves( lineManager->getMiddleLine( &ret ) );
//	}
	
	for ( uint8 x = 0; x < nHorizontalZones ; x++ ) 
		for ( uint8 y = 0; y < nVerticalLines; y++) {
			
			if(	lineH[ x ][ y ]->checkCollision( &ret , currPlayer)){

				inputMoves(/* &ret */lineH[ x ][ y ]->getPosition() );

			}else {
			//este trecho de código serve para resetar as linhas que não forem selecionadas, sim eu sei, meio porco, mas nada melhor me passa pela cabeça no momento...
				for( uint8 i = 0; i < movesMake.size() ;i++ ){
				 
					if( /*ret*/*lineH[ x ] [ y ]->getPosition()!= movesMake[ i ] ){
						
						lineH[ x ] [ y ]->restart();
					
					}//*else
						//break;
				}
			}
		}	
	

	
	for ( uint8 x = 0; x < nHorizontalLines ; x++ ) 
		for ( uint8 y = 0; y < nVerticalZones ; y++) {
			
			if(	lineV[ x ][ y ]->checkCollision( &ret , currPlayer)){
			
				inputMoves(/* &ret */lineV[ x ][ y ]->getPosition()  );
			}else {
		//este trecho de código serve para resetar as linhas que não forem selecionadas, sim eu sei, meio porco, mas nada melhor me passa pela cabeça no momento...
				for( uint8 i = 0; i < movesMake.size() ;i++ ){
					
					if( /*ret*/*lineV[ x ] [ y ]->getPosition() != movesMake[ i ] ){
						
						lineV[ x ] [ y ]->restart();
						
					}					
				}
				
			}
			
		}
	
	
	return r3;	
}


/*=======================================================================================================
 
 // Trata eventos de clique duplo
 onDoleClick->eventos de toque duplo :-p 
 
 ==========================================================================================================*/
void DotGame::onDoubleClick( int8 touchIndex ){
	
	
	switch ( state ) {
		case DOT_GAME_STATE_CPU_PLAYING:


			break;
		case DOT_GAME_STATE_PLAYER_WAITING :
//	if( currPlayer->getTypeUser() == PLAYER_USER_USER ){
//		Point3f p ;
//		p = Utils::MapPointByOrientation( &trackedTouches[ touchIndex ].unmappedStartPos );//&p );
//				if( linePressed( &p ) ){
//					NSLog(@"jogador %d possui %d pontos",indicePlayer , currPlayer->getScore() );
//					//linesSelected = -1;
//					pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0f);
//					
//				}else {
//					if( linesSelected >= 1 ){
//						NSLog(@" trocar de player ");
//						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
//						//changePlayer();
//						//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
//					}
//				}
//			}
//			if( !haveFreeZone() ){
//				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
//					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
//				else {
//					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
//				}
//				
//				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
//			}
			
		//	pChronometer->pause( true );
			break;
			
		case DOT_GAME_STATE_OBSERVING :
		//	resetCam();
			//setDotGameState( lastState );
			//NSLog(@"saindo em modo de observação");
			//pChronometer->pause( false );
			break;
			
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			break;
			
			
		default:
			break;
	}
	
}

/*=======================================================================================================
 
 changePlayer-> troca os jogadores de acordo com a sua vez de jogar
 
 =======================================================================================================*/
void DotGame::changePlayer( void ){
	//NSLog(@"cpu no. %d fez %d pontos ",indicePlayer, currPlayer->getScore() );
	//NSLog(@"jogador trocado!!");
	if( !haveFreeZone() ){
	//	NSLog(@"acabou as zonas...");
		if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
		else {
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
		}
		
		return;
	}
	
	//sim seria melhor uma lista circular, mas...	
	indicePlayer = ( ( ++indicePlayer ) >= nPlayers? 0:indicePlayer );
	
	linesSelected = 0;
	combo = 0;
	currPlayer = players[ indicePlayer ];
	dispMoves = MIN_MOVES_AVAILABLE;
	stateTimeCounter = 0.0f;
	lineManager->restartAllLines();
	movesMake.clear();
	pChronometer->setTimeInterval( maxTime / divisor, 0.0f );
	divisor = 1.0;
	pInfoBar->updatePowers( this );
	if( mode == DOT_GAME_MODE_EXIBITHION && ++movesCount > movesMax )
		goodBye = true;
	currPlayer->updatePowerUp();
	
	if( currPlayer->getTypeUser() == PLAYER_USER_USER ){
		setDotGameState( DOT_GAME_STATE_PLAYER_WAITING );
	}else{
		setDotGameState( DOT_GAME_STATE_CPU_PLAYING );
	}
}

/*==========================================================================================
 
 setDotGameState-> muda o estado da partida e faz os ajustes necessários
 
 ========================================================================================== */
void DotGame::setDotGameState( DotGameState d ){
	if ( lastState == DOT_GAME_STATE_OBSERVING ){
		moveCameraTo( pCurrCamera->getPosition(), 1, 0.1 );
	}
	if( d == DOT_GAME_STATE_PAUSED ) {
		
		pause( /*true*/ );
		//[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_OPTIONS];
		lastState = state;
		state = d;
		return;
	}

	lastState = state;
	state = d;

	//FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* )APP_DELEGATE;

	switch ( d ) {
			
		case DOT_GAME_STATE_AFTER_PLAYER_MOVE:{
			//if( dispMoves <= 0 )
			if( !haveFreeZone() || !haveFreeLine() )
				goodBye = true;
			/*if( !haveFreeZone() || !haveFreeLine() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU ){
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
					return;
				}else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
					return;
				}
			}	*/
			if( !movesMake.empty() ){
				
				moveCameraTo( &movesMake.back(), TIME_FOCUS_LINE ); 
				setDotGameState( DOT_GAME_STATE_FOCUSING_LINE );
				return;
			}else {
				if( linesSelected < dispMoves ){
					setDotGameState( DOT_GAME_STATE_PLAYER_WAITING );
					return;
				}
			}
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
		}
			break;
			
		case DOT_GAME_STATE_AFTER_CPU_MOVE:{
#if DEBUG
				NSLog(@"linesSelected=%d, dispMoves=%d",linesSelected,dispMoves);
#endif		
			//movesMake.pop_back();
			if( !haveFreeZone() || !haveFreeLine() ){
				
				GInfo->idWinner = indicePlayerWinner();
				goodBye = true;
				/*if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU ){
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
					return;
				}else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
					return;
				}*/
			}	
			if( linesSelected < dispMoves ){
				setDotGameState( DOT_GAME_STATE_CPU_PLAYING );
				return;
			}
		
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
			//if( linesSelected >= dispMoves /*linesSelected >= 1*/ ){
//				//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
//				setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
//				return;
//			}else {
//				if( !haveFreeZone() ){
//					if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU ){
//						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
//						return;
//					} else {
//						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
//						return;
//					}
//				}				
//				
//				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
//				//linesSelected = 0;
//				setDotGameState( DOT_GAME_STATE_CPU_PLAYING );
//			}
			pChronometer->pause( false );
		}
			break;
		case DOT_GAME_STATE_ANIMATING_LINE:{
		
			Point3f pLinha;
			Sprite *tmp;
			pLinha = movesMake.back();
			//uint8 x,y;
			if( lineManager->isLineHorizontal( &pLinha ) ){
				tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_LINE_H ) );
				pLinha.set( pLinha.x - ADJUST_CLOUD_LINE_HORIZONTAL_POSITION_X,
						   pLinha.y - ADJUST_CLOUD_LINE_HORIZONTAL_POSITION_Y,  0.0);
			}else {
				tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_LINE_V ) );
				pLinha.set( pLinha.x - ADJUST_CLOUD_LINE_VERTICAL_POSITION_X,
						   pLinha.y - ADJUST_CLOUD_LINE_VERTICAL_POSITION_Y, 0.0);
			}
			pLinha += *lineManager->getPosition();
			tmp->setPosition( &pLinha );
			tmp->setVisible( true );
			tmp->setAnimSequence( 0, false, true);
#if DEBUG
			NSLog(@"posicao da linha que foi selecionada: ( %4.4f,%4.4f)", movesMake.back().x,movesMake.back().y );
			NSLog(@"posicao da fumaca  ( %4.4f,%4.4f)", tmp->getPosition()->x,tmp->getPosition()->y );
#endif

		}
			break;
		case DOT_GAME_STATE_ANIMATING_BIG_ZONE:	
		case DOT_GAME_STATE_ANIMATING_ZONE:{
		}
		break;
			
		case DOT_GAME_STATE_PLAYER_WAITING:
		{
			//só para garantir, caso o último movimento o mané ganhe um quadrado, o jogo dá game over mesmo assim...
			if( !haveFreeZone() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
				else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
				}
			}
		}
			//break;
		case DOT_GAME_STATE_CPU_PLAYING:
			pChronometer->pause( false );
			//pChronometer->setTimeInterval( maxTime / divisor, 1.0);
			break;
		case DOT_GAME_STATE_FOCUSING_LINE :
			pChronometer->pause( true );
			break;
			
//			break;
		
		case  DOT_GAME_STATE_WAITING_POWER_UP_TARGET_LINE_INPUT :{
			static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_CONFIRM_MOVE ) )->setVertexSetColor( COLOR_RED );
		}break;
		case  DOT_GAME_STATE_WAITING_POWER_UP_TARGET_ZONE_INPUT:{
			static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_CONFIRM_MOVE ) )->setVertexSetColor( COLOR_GREEN );			
		}break;
			
		//case  DOT_GAME_STATE_ANIMATING_TRANSITION_1 :
//			pCurrTransition = pSliceTransition;
//			pCurrTransition->reset();
//			break;
//			
//		case  DOT_GAME_STATE_ANIMATING_TRANSITION_2 :
//			switch ( Random::GetInt( 0 ,2 ) ) {
//				case 0:
//					//pCurrTransition = pClosingDoorTransition;
//					break;
//				case 1:
//					pCurrTransition = pCurtainLeftTransition;
//					break;
//				case 2:
//					pCurrTransition = pCurtainTransition;
//					break;
//				default:
//					break;
//					
//			}
//			
//			pCurrTransition->reset();
//			break;	
			
		case DOT_GAME_STATE_GAME_OVER:{
			static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_YES ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
			static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_NO ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
			buttonsManager->getObject( INDEX_BUTTON_LINE_VIEW )->setVisible( false );
		}
			break;
		case DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER:
		{
			if( !haveFreeZone() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
				else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
				}
				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
			}
		}
		case DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_CPU_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER:
		{
#define BUFFER_SIZE_MSG 15
			char buffer[ BUFFER_SIZE_MSG ];
			pChronometer->pause(true);
			FeedbackTxtMsg *tmp;
			tmp = static_cast< FeedbackTxtMsg* > ( buttonsManager->getObject( INDEX_MSG_INTERFACE ) );
			if( state == DOT_GAME_STATE_SHOWING_MSG_GAME_OVER ){
				stateDuration = DURATION_TIME_MSG_GAME_OVER;
				snprintf( buffer , BUFFER_SIZE_MSG,"GAME OVER");
				tmp->setText( buffer );
			}else if( state == DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER ){
				stateDuration = 0.5;
				snprintf( buffer , BUFFER_SIZE_MSG,"PLAYER %0d",indicePlayer+1);
				tmp->setText(buffer);
				
			}else if( state == DOT_GAME_STATE_SHOWING_MSG_CPU_WIN )
			{
				stateDuration = 0.5;
				snprintf( buffer , BUFFER_SIZE_MSG,"CPU VENCEU");
				tmp->setText( buffer );
			}
			else if( state == DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN )
			{
				stateDuration = 0.5;	
				snprintf( buffer , BUFFER_SIZE_MSG,"JOGADOR VENCEU");
				tmp->setText( buffer );
			}
			
			
			tmp->startShowAnimation();
#undef BUFFER_SIZE_MSG
		}
			break;
		case DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_HIDING_MSG_CPU_WIN:
		case DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER:
		case DOT_GAME_STATE_HIDING_MSG_GAME_OVER:
		{
			//FeedbackMsg *tmp;
			//InterfaceControl *tmp;
			FeedbackTxtMsg *tmp;
			tmp = static_cast< FeedbackTxtMsg* > ( buttonsManager->getObject( INDEX_MSG_INTERFACE ) );
		/*	if( state == DOT_GAME_STATE_HIDING_MSG_GAME_OVER )
				//tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_GAME_OVER ) );
			else
				if( state == DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER )
				{
					if( players [ ( ( indicePlayer  ) >= nPlayers-1 ) ? 0 : indicePlayer + 1 ]->getTypeUser() == PLAYER_USER_USER )
						tmp = static_cast< FeedbackTxtMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER ) );
					else {
						tmp =  static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_CPU ) );
					}
				}else 
					if( state == DOT_GAME_STATE_HIDING_MSG_CPU_WIN )
						tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_CPU_WIN ) );
					else 
						if( state == DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN )
							tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_PLAYER_WIN ) );*/
			tmp->startHideAnimation();			
		}
			break;
		case DOT_GAME_STATE_OBSERVING:
		{
			bg->setObserving( true );
			pChronometer->pause( true );
			lineManager->setAllLinesObserving();
			zoneManager->setAllZonesObserved( true );
			pPowerUpGroup->setPowerUpObserving( true );
		}
		default:
			break;
	}
}

/*==============================================================================
 
 renderiza tudo...( dãããããã )
 
 ==============================================================================*/
bool DotGame::render( void ){
#if DEBUG
//	static uint16 counter = 0;
//	static float total = 0.0f;
//	long t1,t2;
//	t1 = Utils::GetTimeStamp();
	
#endif
	bool ret = Scene::render();
#if DEBUG
//	if( ++counter <= 500 ){
//		t2 = Utils::GetTimeStamp();
//		total += ( t2 - t1 );
//	}else {
//		printf("\n\n media do tempo resultante : %d \n\n",total/*/55.0*/);
//		counter = total = 0;
//	}
#endif		
		return ret;
}

/*================================================================================
 
 renderTransitions->renderiza a transition, e talvez alguma mensagem que apareça na tela...
 
 ================================================================================*/
bool DotGame::renderTransitions( void ){
	pControlsCamera->place();
	
	bool ret = false;
	
	//ret |= pCurrTransition->render();
	ret = true;
//	ret = zoneManager->renderPowerUp();

	return ret;
}

/*=========================================================================
 
 getRelativePosition->obtém o toque relativo ao deslocamento da câmera
 
 =========================================================================*/

Point3f DotGame::getRelativePosition( const Point3f *p ){
	
	Point3f p1, p3;
	Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );

	p1 = *pCurrCamera->getPosition();
	p1 -= sc;
	p1 -= *lineManager->getPosition();
	//sc += *lineManager->getPosition();
	//p2 = p1-sc;
	p3 = *p + p1;
	return p3 ;
}

/*========================================================================================
 
 cpuMove-> faz com que a cpu jogue
 
 ==============================================================================================*/


void DotGame::cpuMove( void ){

	
	if( currPlayer->getTypeUser() == PLAYER_USER_CPU && currPlayer->havePowerUp() ){
		
		if( Random::GetInt( 0, 100 ) <= CHANCE_CPU_USE_POWER_UP ){ 
			currPlayer->releasePowerUp();
		}
	
	
	}
	
	if( combo <= limit )
			if ( takeAll3() ) {
				setDotGameState( DOT_GAME_STATE_FOCUSING_LINE );
				return;
			}
		
		for(;;){	
			if( !lineManager->haveFreeLine() )
				break;
			
			if( chooseLineRand() ){
				setDotGameState( DOT_GAME_STATE_FOCUSING_LINE );
				return;
			}
		}
	//}
	if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
		setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
	else {
		setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
	}
	
	//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
	//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
	//changePlayer();
}


/* =====================================================================================
 
 escolhe uma linha aleatoriamente
 
 =====================================================================================*/
bool DotGame::chooseLineRand( void ){
	
	uint8 x,y;
	//uint8 t3;
	bool retB = false;
	Point3f p;

	if( Random::GetInt( 0 , 1) ){
		x = Random::GetInt( 0, nHorizontalLines-1 );
		y = Random::GetInt( 0, nVerticalZones-1 );
		
		p.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
			  LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
			  1.0);
	}else{
		x = Random::GetInt( 0, nHorizontalZones-1 );
		y = Random::GetInt( 0, nVerticalLines-1 );
		
		p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
			  y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
			  1.0);
	}
	if( !lineManager->isLineGained( &p ) ){
		inputMoves( &p );
		moveCameraTo( &p , TIME_FOCUS_LINE );
		retB = true;		
	}
	
	return retB;
}

/*=================================-=-=-=-=-=-=-====-=--===-=-=-==-=-=-==-=-=-
 
 verifica se há zonas livres no jogo
 
 =================================-=-=-=-=-=-=-====-=--===-=-=-==-=-=-==-=-=-*/

bool DotGame::haveFreeZone( void ){
	
	bool r = false;
	for (uint8 x = 0; x < nHorizontalZones; x++) {
		for (uint8 y = 0; y< nVerticalZones ; y++ ){
			if ( zones[ x ][ y ]->getZoneGameState() != ZONE_GAME_STATE_OCCUPIED && !zones[ x ][ y ]->isLocked() ) {
				
				return true;
				break;
			}
		}
	}
	return r;
}


/*-====================================================================================-=-
 
 destrutor
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=*/
DotGame::~DotGame(){
	clear();
}

/* -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=
 
 takeAll3 ->fç que serve para tomar os quadrados que tiverem 3 linhas
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=*/
bool DotGame::takeAll3( void ){
	bool ret = false;
	for (uint8 x = 0; x < nHorizontalZones; x++ ) {
		for (uint8 y = 0 ; y < nVerticalZones ; y++ ) {
			
			if( verifyZones( x , y ) ){
				ret = true; 
				return ret;
			}
		}
	}	
	return ret;
}

/* -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=
 
 verifica se a zona está com as linhas anteriores ocupadas
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-= */
bool DotGame::verifyZones( uint8 x, uint8 y ){
	uint8 count = 0;
	if( zones[ x ][ y ]->getZoneGameState() != ZONE_GAME_STATE_OCCUPIED ){
		
		if( lineH[ x ][ y ]->isOccupied() )
			count ++;
		
		if( lineV[ x ][ y ]->isOccupied() )
			count ++;
		
		if( lineH[ x ][ y + 1  ]->isOccupied() )
			count ++;
		
		if( lineV[ x + 1 ][ y  ]->isOccupied() )
			count ++;
		
		if( count == 3 ){
			
			if ( !lineV[ x ][ y ]->isOccupied() ) {
				inputMoves( lineV[ x ][ y ]->getPosition() );
				moveCameraTo(  lineV[ x ][ y ]->getPosition() , TIME_FOCUS_LINE);
				return true;
			}
			
			if( !lineV[ x + 1 ][ y ]->isOccupied() ){
				inputMoves( lineV[ x + 1 ][ y ]->getPosition() );
				moveCameraTo(  lineV[ x +  1][ y ]->getPosition(), TIME_FOCUS_LINE);
				return true;				
			}
			
			if( !lineH[ x ][ y ]->isOccupied() ){
				inputMoves( lineH[ x ][ y ]->getPosition() );
				moveCameraTo( lineH[ x ][ y ]->getPosition(), TIME_FOCUS_LINE);
				return true;				
			}
			
			if( !lineH[ x ][ y + 1 ]->isOccupied() ){
				inputMoves( lineH[ x ][ y + 1 ]->getPosition() );
				moveCameraTo(  lineH[ x ][ y + 1 ]->getPosition() , TIME_FOCUS_LINE);	
				return true;				
			}
		}
		
	}
	return false;
}


/* -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-= 
 
 haveFreeLine->retorna se tem Linhas livres em jogo
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-= */

bool DotGame::haveFreeLine( void ){
	bool ret = false;
	for ( uint8 x = 0; x < nHorizontalZones ; x++ ) {
		for ( uint8 y = 0; y < nVerticalLines  ; y++) {
			if ( !lineH[ x ][ y ]->isOccupied() && !lineH [ x ][ y ]->isLocked() ){
				ret = true;
				break;
			}
		}
	}
	
	for ( uint8 x = 0; x < nHorizontalLines  ; x++ ) {
		for ( uint8 y = 0; y < nVerticalZones ; y++) {
			if ( !lineV[ x ][ y ]->isOccupied() && !lineV [ x ][ y ]->isLocked() ){
				ret = true;
				break;
			}			
		}
	}
	return ret;
}
/*==============================================================================
 
 clear->deleta tudo
 
 ==============================================================================*/
void DotGame::clear( void ){

//	DELETE( pSliceTransition );
//	DELETE( pCurtainTransition);
	pControlsCamera = NULL;
//	DELETE( pCurtainLeftTransition );
	pInfoBar = NULL;
	pChronometer = NULL ;
	//pCurrTransition = NULL;
	buttonsManager = NULL;
	uint8 x,y;
	//SAFE_DELETE( zoneManager );
	//SAFE_DELETE( lineManager );
	for( x = 0; x < nPlayers ;x++ )
		DELETE( players[ x ] );

	currPlayer = NULL;

	for ( x = 0; x < nHorizontalZones; x++) 
		for ( y = 0; y< nVerticalZones ; y++ )
			zones[ x ][ y ] = NULL;
	
	for ( x = 0; x < nHorizontalZones ; x++ ) 
		for ( y = 0; y < nVerticalLines  ; y++) 
			lineH[ x ][ y ] = NULL;
	
	for ( x = 0; x < nHorizontalLines  ; x++ ) 
		for (  y = 0; y < nVerticalZones ; y++) 
			lineV[ x ][ y ] = NULL;
		
	
}

/* ===============================================================================================

 adjustMinZoom ==> função que retorna o zoom máximo possível de acordo com o tamanho do cenário
 
 ===============================================================================================*/
float DotGame::adjustMinZoom( void/*float f*/ ){

	//float max = nHorizontalLines  *  ZONE_SIDE + ( ( nHorizontalLines + 1) * LINE_HORIZONTAL_HEIGHT );
//	max += lineManager->getPosition()->x;
	
	Point3f max, min;
	float ret;
	
	min= *getObject( 0 )->getPosition() ;
	max.set( nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
			nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE) );
	
	if( max.x <= SCREEN_WIDTH && max.y <= SCREEN_HEIGHT )
		return 1.0f;
	
	
	float widthNeeded = ( max.x - min.x );
	float heightNeeded = ( max.y - min.y );
	
	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	float currWidth = ( SCREEN_WIDTH * invZoom );
	float currHeight = ( SCREEN_HEIGHT * invZoom );
	
	float zoomNeededInX = widthNeeded / currWidth;
	float zoomNeededInY = heightNeeded / currHeight;	
	float finalZoom =  (invZoom * ( zoomNeededInX > zoomNeededInY ? zoomNeededInX : zoomNeededInY ));
	
	ret = 1/finalZoom;
	//ret = ( ret > 1 ? 1 : ret );
	
	return  ret;
	
	/*	Point3f max, min;
	 
	 min = *getObject( 0 )->getPosition();
	 
	 Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT);
	 
	 min += *lineManager->getPosition() + (sc * 1.25) ;
	 
	 max.set( nHorizontalLines  *  ZONE_SIDE + ( ( nHorizontalLines + 1) * LINE_HORIZONTAL_HEIGHT ) , 
	 LINE_VERTICAL_WIDTH + ( nVerticalLines * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT ))
	 );
	 
	 max += *lineManager->getPosition();
	 
	 // Calcula o zoom
	 float widthNeeded = ( max.x - min.x );
	 float heightNeeded = ( max.y - min.y );
	 
	 float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	 float currWidth = ( SCREEN_WIDTH * invZoom );
	 float currHeight = ( SCREEN_HEIGHT * invZoom );
	 
	 float zoomNeededInX = widthNeeded / currWidth;
	 float zoomNeededInY = heightNeeded / currHeight;	
	 float finalZoom =  (invZoom * ( zoomNeededInX > zoomNeededInY ? zoomNeededInX : zoomNeededInY ));*/

}

/* ===============================================================================================

 resetCam->reseta o zoom da camera qdo sai do modo de observação

===============================================================================================*/
 void DotGame::resetCam( void ){
	 moveCameraTo(pCurrCamera->getPosition(), 1.0f, TIME_FOCUS_LINE);
}
/*=====================================================================================
 
 auRevoir->sai da tela de jogo--> adie...
 
 =====================================================================================*/

void DotGame::auRevoir(){
	/*if( !persist )
		[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_MAIN_MENU];
	else
		[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_LOAD_GAME];*/
	GInfo->idWinner = indicePlayerWinner();
	[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_WIN];
}
/*=====================================================================================
 
nextLevel->começa outra partida em um nível de dificuldade maior
 
=====================================================================================*/
void DotGame::nextLevel( void ){

}
/*======================================================================================

 resetTable->reseta o tabuleiro na mesma partida

======================================================================================*/
void DotGame::resetTable( void ){
//	lineManager->resetLines();
//	zoneManager->reset();
		uint8 x,y;
	for ( x = 0; x < nHorizontalZones; x++) {
		for ( y = 0; y< nVerticalZones ; y++ ){
			zones[ x ][ y ]->reset();
		}
	}
	
	for ( x = 0; x < nHorizontalZones ; x++ ) {
		for ( y = 0; y < nVerticalLines  ; y++) {
			lineH[ x ][ y ]->reset();
		}
		
	}
	
	
	for ( x = 0; x < nHorizontalLines  ; x++ ) {
		for ( y = 0; y < nVerticalZones ; y++) {
			lineV[ x ][ y ]->reset();
		}
	}
	
	static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_YES ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_NO ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );

	
	for( uint8 x = 0; x < nPlayers; x++ )
		players[ x ]->setScore( 0 );
	pInfoBar->configure(this);//updateScores( this );

	Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	pCurrCamera->setPosition( &sc );
/*	moveCameraTo( &sc, 0.1);*/
	indicePlayer = 255;
	changePlayer();
}

/*====================================================================================================

 indicePlayerWinner-> aponta qual jogador ganhou
 
====================================================================================================*/
uint8 DotGame::indicePlayerWinner( void ){
//feito para 2 jogadores, sendo que para mais jogadores, teria-se que colocar uma ordenação aqui...
	if ( players[ 0 ]->getScore() > players[ 1 ]->getScore() ) {
		return 0;
	}else {
		return 1;
	}	
}
/*========================================================
 
 inputMoves=> controla a qte de movimentos permitidos pelo jogador
 
 -=-=-=-=-=0=-0=-0=-=9=--0=-0=-=-=-=-==--===-=-=-=-=-=-==.-.=-.=.-=-*/
void DotGame::inputMoves( const Point3f *p ){
	
	linesSelected = ( ++linesSelected >= dispMoves ? dispMoves: linesSelected );
	movesMake.push_back( *p );
	if( movesMake.size() > dispMoves )
		movesMake.erase( movesMake.begin() );
#if DEBUG
//	NSLog(@"p=> x=%3.2f, y=%3.2f", p->x , p->y );
//	NSLog(@"lines selected %d ",linesSelected);
#endif
}

/*=================================================================================
 
 MakeBigZone-> verifica se há 4 zonas em série ( recebe um pto e verifica )
  
 =================================================================================*/	
bool DotGame::makeBigZone( Point3f *p ){
#if DEBUG
	//NSLog(@" verificando big zonas ");
#endif
	uint8 x,y;
	//ZoneGroup::getZoneXYByPosition( p, &x, &y);
	getZoneXYByPosition( p, &x , &y );
	bool ret = false;
	
	Point3f p2,p3,p4,markPos,pLinha;
	
	//verificando zona/quadrado superior à esquerda
	p2 = ZoneGroup::getZonePositionByXY( x, y - 1 );
	p3 = ZoneGroup::getZonePositionByXY( x - 1, y - 1 );
	p4 = ZoneGroup::getZonePositionByXY( x - 1, y );
	
	if( zoneManager->isZoneOccupied( p ) &&
		zoneManager->isZoneOccupied( &p2 ) &&
		zoneManager->isZoneOccupied( &p3 ) &&
		zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		    !zoneManager->isZoneCoupled( &p2 ) &&
		    !zoneManager->isZoneCoupled( &p3 ) &&
		    !zoneManager->isZoneCoupled( &p4 ) )
			if(   ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
				
				(zoneManager->zoneGetPlayerId( &p3 ) ==
				 zoneManager->zoneGetPlayerId( &p4 ) ) &&
				
				(zoneManager->zoneGetPlayerId( p ) ==
				 zoneManager->zoneGetPlayerId( &p4 ) )){
				
			markPos.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
						( y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				zoneManager->setZoneCoupled( p , true);
				zoneManager->setZoneCoupled( &p2 , true);
				zoneManager->setZoneCoupled( &p3 , true);
				zoneManager->setZoneCoupled( &p4 , true);
				
				zoneManager->setZoneVisible( &p2 , false );
				zoneManager->setZoneVisible( p , false );
				zoneManager->setZoneVisible( &p4 , false );
				zoneManager->BigZoneGainedByPLayer( &p3, currPlayer);
				
				pLinha = LineGroup::getLineCoordenatesByXY( x , y - 1, true);
				lineManager->setLineVisible(&pLinha , false );
				
				pLinha = LineGroup::getLineCoordenatesByXY( x , y , true);			
				lineManager->setLineVisible(&pLinha , false );
				
				pLinha =LineGroup::getLineCoordenatesByXY( x , y, false);
				lineManager->setLineVisible(&pLinha , false );
				
				pLinha =LineGroup::getLineCoordenatesByXY( x - 1 , y, false);
				lineManager->setLineVisible(&pLinha , false);
				
			Point3f tmpZone;
			Sprite *tmp;
			if( !getObject( INDEX_CLOUD_ZONE_22_1)->isVisible() ){
				tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_1 ) );
			}else {
				tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_2 ) );
			}
				getObject( INDEX_CLOUD_ZONE_11_1 )->setVisible( false );
				getObject( INDEX_CLOUD_ZONE_11_2 )->setVisible( false );
			tmpZone = p3;
			tmpZone.set(tmpZone.x -ADJUST_CLOUD_ZONE_2X2_POSITION_X,
					  tmpZone.y- ADJUST_CLOUD_ZONE_2X2_POSITION_Y,
					  0.0);
 			tmpZone += *zoneManager->getPosition();
			tmp->setPosition( &tmpZone );
			tmp->setVisible( true );
			tmp->setAnimSequence( 0, false, true);
			setDotGameState( DOT_GAME_STATE_ANIMATING_BIG_ZONE );
#if DEBUG
//			NSLog(@"aparecer animação 2x2 aqui");
//			NSLog(@"bigzona feita!");
#endif
				ret= true;
				
				
			}
	
	//zona/quadrado inferior à esquerda
	p2 = ZoneGroup::getZonePositionByXY( x, y + 1 );
	p3 = ZoneGroup::getZonePositionByXY( x - 1, y + 1 );
	p4 = ZoneGroup::getZonePositionByXY( x - 1, y );
	
	if( zoneManager->isZoneOccupied( p ) &&
	   zoneManager->isZoneOccupied( &p2 ) &&
	   zoneManager->isZoneOccupied( &p3 ) &&
	   zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		  !zoneManager->isZoneCoupled( &p2 ) &&
		   !zoneManager->isZoneCoupled( &p3 ) &&
		   !zoneManager->isZoneCoupled( &p4 ) )
			if(   ( zoneManager->zoneGetPlayerId( p ) ==   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) )
			{
				
				markPos.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							( ( y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				//colocar em 1 fç à parte?
				zoneManager->setZoneCoupled( p , true);
				zoneManager->setZoneCoupled( &p2 , true);
				zoneManager->setZoneCoupled( &p3 , true);
				zoneManager->setZoneCoupled( &p4 , true);
				
				zoneManager->setZoneVisible( &p2 , false );
				zoneManager->setZoneVisible( &p3 , false );
				zoneManager->setZoneVisible( p , false );
				
				zoneManager->BigZoneGainedByPLayer( &p4, currPlayer);
	
				pLinha = LineGroup::getLineCoordenatesByXY( x , y + 1, true);
				lineManager->setLineVisible(&pLinha , false );
		
				pLinha = LineGroup::getLineCoordenatesByXY( x , y , true);	
				lineManager->setLineVisible(&pLinha , false );
				
				pLinha =LineGroup::getLineCoordenatesByXY( x , y + 1, false);
				lineManager->setLineVisible(&pLinha , false );
				
				pLinha =LineGroup::getLineCoordenatesByXY( x - 1 , y + 1, false);
				lineManager->setLineVisible(&pLinha , false);			
				
				Point3f tmpZone;
				Sprite *tmp;
				if( !getObject( INDEX_CLOUD_ZONE_22_1)->isVisible() ){
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_1 ) );
				}else {
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_2 ) );
				}
				tmpZone = p4;
				tmpZone.set( tmpZone.x - ADJUST_CLOUD_ZONE_2X2_POSITION_X,
							tmpZone.y- ADJUST_CLOUD_ZONE_2X2_POSITION_Y,
							0.0);
				tmpZone += *zoneManager->getPosition();
				tmp->setPosition( &tmpZone );
				tmp->setVisible( true );
				tmp->setAnimSequence( 0, false, true);
				getObject( INDEX_CLOUD_ZONE_11_1 )->setVisible( false );
				getObject( INDEX_CLOUD_ZONE_11_2 )->setVisible( false );
				setDotGameState( DOT_GAME_STATE_ANIMATING_BIG_ZONE );

#if DEBUG
		//		NSLog(@"aparecer animação 2x2 aqui");
		//		NSLog(@"bigzona feita!");
#endif
				ret = true;
			}
	
	
//zona supeiror à direita
	p2 = ZoneGroup::getZonePositionByXY( x, y - 1 );
	p3 = ZoneGroup::getZonePositionByXY( x + 1, y - 1 );
	p4 = ZoneGroup::getZonePositionByXY( x + 1, y );

	if( zoneManager->isZoneOccupied( p ) &&
	   zoneManager->isZoneOccupied( &p2 ) &&
	   zoneManager->isZoneOccupied( &p3 ) &&
	   zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		   !zoneManager->isZoneCoupled( &p2 ) &&
		   !zoneManager->isZoneCoupled( &p3 ) &&
		   !zoneManager->isZoneCoupled( &p4 ) )
			if(   ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) )
			   ){
				
				markPos.set( ( x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							( y * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				
				zoneManager->setZoneCoupled( p , true);
				zoneManager->setZoneCoupled( &p2 , true);
				zoneManager->setZoneCoupled( &p3 , true);
				zoneManager->setZoneCoupled( &p4 , true);
				
				zoneManager->setZoneVisible( &p4 , false );
				zoneManager->setZoneVisible( &p3 , false );
				zoneManager->setZoneVisible( p , false );
				zoneManager->BigZoneGainedByPLayer( &p2, currPlayer);
				
				pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y - 1, true);
				lineManager->setLineVisible(&pLinha , false );
			
				pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y , true)	;			
				lineManager->setLineVisible(&pLinha , false );
			
				pLinha =LineGroup::getLineCoordenatesByXY( x , y, false);
				lineManager->setLineVisible(&pLinha , false );
			
				pLinha =LineGroup::getLineCoordenatesByXY( x + 1 , y, false);
				lineManager->setLineVisible(&pLinha , false);
				
				
				Point3f tmpZone;
				Sprite *tmp;
				if( !getObject( INDEX_CLOUD_ZONE_22_1)->isVisible() ){
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_1 ) );
				}else {
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_2 ) );
				}
				getObject( INDEX_CLOUD_ZONE_11_1 )->setVisible( false );
				getObject( INDEX_CLOUD_ZONE_11_2 )->setVisible( false );
				
				tmpZone = p2;
				tmpZone.set( tmpZone.x - ADJUST_CLOUD_ZONE_2X2_POSITION_X,
							tmpZone.y- ADJUST_CLOUD_ZONE_2X2_POSITION_Y,
							0.0);
				tmpZone += *zoneManager->getPosition();
				tmp->setPosition( &tmpZone );
				tmp->setVisible( true );
				tmp->setAnimSequence( 0, false, true);
				setDotGameState( DOT_GAME_STATE_ANIMATING_BIG_ZONE );
#if DEBUG
			//	NSLog(@"aparecer animação 2x2 aqui");
			//	NSLog(@"bigzona feita!");
#endif

				ret = true;
			}
	
	
//zona inferior à direita	

	p2 = ZoneGroup::getZonePositionByXY( x, y + 1 );
	p3 = ZoneGroup::getZonePositionByXY( x + 1, y + 1 );
	p4 = ZoneGroup::getZonePositionByXY( x + 1, y );

	if( zoneManager->isZoneOccupied( p ) &&
		zoneManager->isZoneOccupied( &p2 ) &&
		zoneManager->isZoneOccupied( &p3 ) &&
		zoneManager->isZoneOccupied( &p4 ) )
		if( !zoneManager->isZoneCoupled( p ) &&
		    !zoneManager->isZoneCoupled( &p2 ) &&
		    !zoneManager->isZoneCoupled( &p3 ) &&
		    !zoneManager->isZoneCoupled( &p4 ) )
			if(  ( zoneManager->zoneGetPlayerId( p ) ==
			    zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
				
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				  zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) )
				){
						
				markPos.set( ( x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),( ( y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->disappearMark( &markPos );
				
						zoneManager->setZoneCoupled( p , true);
						zoneManager->setZoneCoupled( &p2 , true);
						zoneManager->setZoneCoupled( &p3 , true);
						zoneManager->setZoneCoupled( &p4 , true);

				zoneManager->setZoneVisible( &p2 , false );
				zoneManager->setZoneVisible( &p3 , false );
				zoneManager->setZoneVisible( &p4 , false );
				zoneManager->BigZoneGainedByPLayer( p, currPlayer);
				
				ret = true;
						
				pLinha = LineGroup::getLineCoordenatesByXY( x + 1, y + 1, true);
				lineManager->setLineVisible(&pLinha , false );
			
				pLinha = LineGroup::getLineCoordenatesByXY( x + 1, y , true)		;		
				lineManager->setLineVisible(&pLinha , false );
			
				pLinha = LineGroup::getLineCoordenatesByXY( x , y + 1, false);
				lineManager->setLineVisible(&pLinha , false );
			
				pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y + 1, false);
				lineManager->setLineVisible(&pLinha , false);
				
				getObject( INDEX_CLOUD_ZONE_11_1 )->setVisible( false );
				getObject( INDEX_CLOUD_ZONE_11_2 )->setVisible( false );
				Point3f tmpZone;
				Sprite *tmp;
				if( !getObject( INDEX_CLOUD_ZONE_22_1)->isVisible() ){
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_1 ) );
				}else {
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_22_2 ) );
				}
				tmpZone = *p;
				tmpZone.set( tmpZone.x - ADJUST_CLOUD_ZONE_2X2_POSITION_X,
							tmpZone.y- ADJUST_CLOUD_ZONE_2X2_POSITION_Y,
							0.0);
				tmpZone += *zoneManager->getPosition();
				tmp->setPosition( &tmpZone );
				tmp->setVisible( true );
				tmp->setAnimSequence( 0, false, true);
				setDotGameState( DOT_GAME_STATE_ANIMATING_BIG_ZONE );
#if DEBUG
			//	NSLog(@"aparecer animação 2x2 aqui");
			//	NSLog(@"bigzona feita!");
#endif						
		
					}
	
	return ret;
}

/*=================================================================================
 
 desfaz os quadrados/zonas grandes

 =================================================================================*/
bool DotGame::unMakeBigZone( Point3f *p ){
#if DEBUG
	//NSLog(@"desfazendo bigZonas");
#endif
	uint8 x,y;
	//ZoneGroup::getZoneXYByPosition( p, &x, &y);
	getZoneXYByPosition( p, &x, &y);
	bool ret = false;
	
	Point3f p2,p3,p4,markPos,pLinha;
	
	//verificando zonas/quadrados superior à esquerda
	p2 = ZoneGroup::getZonePositionByXY( x, y - 1 );
	p3 = ZoneGroup::getZonePositionByXY( x - 1, y - 1 );
	p4 = ZoneGroup::getZonePositionByXY( x - 1, y );
	
	
	if( ( ( ( zoneManager->isZoneCoupled( p ) && zoneManager->isZoneCoupled( &p2 ) )
		   && zoneManager->isZoneCoupled( &p3 ) ) && 
		 zoneManager->isZoneCoupled( &p4 )
	   ))
			
			if( ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ))
			if( zoneManager->zoneGetPlayerId( &p4 )!= 255 ){
				
				markPos.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							y * (  LINE_HORIZONTAL_HEIGHT  + ZONE_SIDE ) );
				
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->reappearMark( &markPos );
				zoneManager->setZoneCoupled( p , false);
				zoneManager->setZoneCoupled( &p2 , false);
				zoneManager->setZoneCoupled( &p3 , false);
				zoneManager->setZoneCoupled( &p4 , false);

				pLinha = LineGroup::getLineCoordenatesByXY( x , y - 1, true);
//				zoneManager->zoneLostedByPlayer( p );
				lineManager->setLineVisible( &pLinha , true );
				
				pLinha = LineGroup::getLineCoordenatesByXY( x , y , true);
				lineManager->setLineVisible( &pLinha , true );
				
				pLinha = LineGroup::getLineCoordenatesByXY( x , y, false);
				lineManager->setLineVisible( &pLinha , true );
				
				pLinha = LineGroup::getLineCoordenatesByXY( x - 1 , y, false);
				lineManager->setLineVisible( &pLinha , true );
				
				zoneManager->setZoneVisible( &p2 , true );
				zoneManager->setZoneVisible( p , true );
				zoneManager->setZoneVisible( &p4 , true );
				zoneManager->BigZoneLostedByPLayer( &p3 );
				
#if DEBUG
				NSLog(@"bigzona desfeita!");
#endif
				ret= true;
				
				
			}
	
	//zona/quadrado inferior à esquerda
	p2 = ZoneGroup::getZonePositionByXY( x, y + 1 );
	p3 = ZoneGroup::getZonePositionByXY( x - 1, y + 1 );
	p4 = ZoneGroup::getZonePositionByXY( x - 1, y );
	
	if(( ( ( zoneManager->isZoneCoupled( p ) && zoneManager->isZoneCoupled( &p2 ) )
		  && zoneManager->isZoneCoupled( &p3 ) ) && 
		zoneManager->isZoneCoupled( &p4 )
		) )
		if(   ( zoneManager->zoneGetPlayerId( p ) ==
			   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
		   
		   (zoneManager->zoneGetPlayerId( &p3 ) ==
			zoneManager->zoneGetPlayerId( &p4 ) ) &&
		   
		   (zoneManager->zoneGetPlayerId( p ) ==
			zoneManager->zoneGetPlayerId( &p4 ) ))
				if( zoneManager->zoneGetPlayerId( &p4 )!= 255 )
			   {
				markPos.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							( ( y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->reappearMark( &markPos );
				//colocar em 1 fç à parte?
				zoneManager->setZoneCoupled( p , false);
				zoneManager->setZoneCoupled( &p2 , false);
				zoneManager->setZoneCoupled( &p3 , false);
				zoneManager->setZoneCoupled( &p4 , false);

				   pLinha = LineGroup::getLineCoordenatesByXY( x , y + 1, true);
				   lineManager->setLineVisible(&pLinha, true );
				   pLinha = LineGroup::getLineCoordenatesByXY( x , y , true);
				   lineManager->setLineVisible(&pLinha , true );
				   pLinha = LineGroup::getLineCoordenatesByXY( x , y + 1, false);
				   lineManager->setLineVisible(&pLinha, true );
				   pLinha = LineGroup::getLineCoordenatesByXY( x - 1 , y + 1, false);
				   lineManager->setLineVisible(&pLinha, true );
				
				   zoneManager->setZoneVisible( &p2 , true );
				zoneManager->setZoneVisible( &p3 , true );
				zoneManager->setZoneVisible( p , true );
				
				zoneManager->BigZoneLostedByPLayer( &p4 );
				
#if DEBUG
				   NSLog(@"bigzona desfeita!");
#endif
				
				ret = true;
			}
	
	
	//zona superior à direita
	p2 = ZoneGroup::getZonePositionByXY( x, y - 1 );
	p3 = ZoneGroup::getZonePositionByXY( x + 1, y - 1 );
	p4 = ZoneGroup::getZonePositionByXY( x + 1, y );
	
	if(( ( ( zoneManager->isZoneCoupled( p ) && zoneManager->isZoneCoupled( &p2 ) )
		  && zoneManager->isZoneCoupled( &p3 ) ) && 
		zoneManager->isZoneCoupled( &p4 )
		))
			if(   ( zoneManager->zoneGetPlayerId( p ) ==
				   zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) )
			   )
			
						if( zoneManager->zoneGetPlayerId( &p4 )!= 255 ){
				
				markPos.set( ( x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
							( y * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->reappearMark( &markPos );
				
				zoneManager->setZoneCoupled( p , false);
				zoneManager->setZoneCoupled( &p2 , false);
				zoneManager->setZoneCoupled( &p3 , false);
				zoneManager->setZoneCoupled( &p4 , false);
							
							pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y - 1, true);
							lineManager->setLineVisible( &pLinha, true );
							pLinha = LineGroup::getLineCoordenatesByXY( x + 1, y , true);
							lineManager->setLineVisible(&pLinha , true );
							pLinha = LineGroup::getLineCoordenatesByXY( x , y , false);
							lineManager->setLineVisible(&pLinha , true );
							pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y, false);
							lineManager->setLineVisible(&pLinha , true );
							
							
				zoneManager->setZoneVisible( &p4 , true );
				zoneManager->setZoneVisible( &p3 , true );
				zoneManager->setZoneVisible( p , true );
				zoneManager->BigZoneLostedByPLayer( &p2 );

#if DEBUG
				NSLog(@"bigzona desfeita!");
#endif
				
				ret = true;
			}
	
	
	//zona inferior à direita	
	
	p2 = ZoneGroup::getZonePositionByXY( x, y + 1 );
	p3 = ZoneGroup::getZonePositionByXY( x + 1, y + 1 );
	p4 = ZoneGroup::getZonePositionByXY( x + 1, y );
	
	if(( ( ( zoneManager->isZoneCoupled( p ) && zoneManager->isZoneCoupled( &p2 ) )
		  && zoneManager->isZoneCoupled( &p3 ) ) && 
		zoneManager->isZoneCoupled( &p4 )
		))
																		
			if(  ( zoneManager->zoneGetPlayerId( p ) ==
				  zoneManager->zoneGetPlayerId( &p2 ) ) 	 &&
			   
			   (zoneManager->zoneGetPlayerId( &p3 ) ==
				zoneManager->zoneGetPlayerId( &p4 ) ) &&
			   
			   (zoneManager->zoneGetPlayerId( p ) ==
				zoneManager->zoneGetPlayerId( &p4 ) )
			   )
						if( zoneManager->zoneGetPlayerId( &p4 )!= 255 ){
				
				markPos.set( ( x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),( ( y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ));
				static_cast<Marks*> ( bg->getObject( INDEX_BOARD_MARKS ))->reappearMark( &markPos );
				
				zoneManager->setZoneCoupled( p , false);
				zoneManager->setZoneCoupled( &p2 , false);
				zoneManager->setZoneCoupled( &p3 , false);
				zoneManager->setZoneCoupled( &p4 , false);
							
							pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y + 1, true);
							lineManager->setLineVisible(&pLinha , true );
							pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y , true);
							lineManager->setLineVisible(&pLinha , true );
							pLinha = LineGroup::getLineCoordenatesByXY( x , y + 1, false);
							lineManager->setLineVisible(&pLinha , true );
							pLinha = LineGroup::getLineCoordenatesByXY( x + 1 , y + 1, false);
							lineManager->setLineVisible(&pLinha , true );

				zoneManager->setZoneVisible( &p2 , true );
				zoneManager->setZoneVisible( &p3 , true );
				zoneManager->setZoneVisible( &p4 , true );
				zoneManager->BigZoneLostedByPLayer( p );
				ret = true;
#if DEBUG
				NSLog(@"bigzona desfeita!");
#endif
				
			}
	zoneManager->zoneLostedByPlayer( p );
	return ret;
}

DotGame* DotGame::getDotGameInstance( void ){
	return this;
}

Player*  DotGame::getPlayerByIndice( uint8 i ){
	return players[ ( i % nPlayers ) /*( i >= nPlayers ? i % nPlayers : i )*/ ];
}

void DotGame::clearMoves( void ){ 
	movesMake.clear();
	linesSelected = dispMoves;
	setDotGameState( ( currPlayer->getTypeUser() == PLAYER_USER_CPU ? DOT_GAME_STATE_AFTER_CPU_MOVE: DOT_GAME_STATE_AFTER_PLAYER_MOVE ) );
}

void DotGame::setDivisorValue( float c ){ divisor = c; }

float DotGame::getDivisorValue( void ){ return divisor; }

bool DotGame::verifyZonesHorizontal( Point3f *p ){

	uint8 x,y;
	bool ret = false ;
	
	//LineGroup::getLineXYByPosition( p, false, &x, &y);
	getLineXyByPosition( p, &x, &y );
	Point3f p1;
	p1 = ZoneGroup::getZonePositionByXY( x, y );
#if DEBUG
	NSLog(@"zona a ser verificada %d  %d ",x ,y);
#endif	
	if( verifyLines(  x, y ) ){
		ret |= true;
	}

		
	if( y >= 1 ){
		p1 = ZoneGroup::getZonePositionByXY( x , y - 1 );
#if DEBUG
		NSLog(@"zona a ser verificada %d  %d ",x ,y- 1);
#endif
		if( verifyLines( /*&p1*/x, y - 1 ) ){
			ret |= true;
		}
	}
	return ret;
}

bool DotGame::verifyZonesVertical( Point3f *p ){
	uint8 x,y;
	bool ret = false ;
	Point3f p1;
	//LineGroup::getLineXYByPosition( p, true, &x, &y);
	getLineXyByPosition( p, &x, &y );
	
	p1 = ZoneGroup::getZonePositionByXY( x, y );
#if DEBUG
	//NSLog(@"zona a ser verificada %d  %d ",x,y);
#endif
	if( verifyLines( /*&p1*/x, y ) ){
		ret = true;
	}

	if( x >= 1 ){
		p1 = ZoneGroup::getZonePositionByXY( x - 1, y );
#if DEBUG
	//	NSLog(@"zona a ser verificada %d  %d ",x - 1,y);
#endif
		if( verifyLines( /*&p1*/x - 1, y ) ){
			ret = true;
		}
	}
	return ret;
}


void DotGame::pause( /*bool b*/ ){
	lineManager->setVisible( false );
	zoneManager->setVisible( false );
	pChronometer->pause( true );
	[(( FreeKickAppDelegate* )APP_DELEGATE ) pauseAllSounds: true];
	[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_PAUSE_SCREEN];
}

bool DotGame::loadCloudSprites( void ){
	Sprite *s1, *s2, *s3, *s4, *s5, *s6;
#if DEBUG
	
	LOG_FREE_MEM("antes de carregar as nuvens");
#endif
	Sprite tmp( "f11","f11",ResourceManager::Get16BitTexture2D  );
	s1 = new Sprite( tmp );//"f11","f11");
	s1->setListener( this );
	//s1->setPosition( HALF_SCREEN_WIDTH - 32.0, HALF_SCREEN_HEIGHT - 32.0 );
	//s1->setAnimSequence(0, true, true);
	s1->setVisible( false );
	if( !insertObject( s1 ) )
		return false;
	
	s2 = new Sprite( tmp );//"f11","f11",ResourceManager::Get16BitTexture2D );
	s2->setListener( this );
	s2->setVisible( false );
	if( !insertObject( s2 ) )
		return false;
	
	Sprite tmp2( "f22","f22",ResourceManager::Get16BitTexture2D  );

	s3 = new Sprite( tmp2 );//"f22","f22",ResourceManager::Get16BitTexture2D );
	s3->setListener( this );
	//s3->setPosition( HALF_SCREEN_WIDTH - 32.0, HALF_SCREEN_HEIGHT - 32.0 );
	//s3->setAnimSequence(0, true, true);
	s3->setVisible( false );
	if( !insertObject( s3 ) )
		return false;
	
	s4 = new Sprite( tmp2 );//"f22","f22",ResourceManager::Get16BitTexture2D );
	s4->setListener( this );
	s4->setVisible( false );
	if( !insertObject( s4 ) )
		return false;
	
	s5 = new Sprite( "fch","fch",ResourceManager::Get16BitTexture2D );
	s5->setListener( this );
	s5->setVisible( false );
	if( !insertObject( s5 ) )
		return false;
	
	s6 = new Sprite( "fcv","fcv",ResourceManager::Get16BitTexture2D );
	s6->setListener( this );
	s6->setVisible( false );
	if( !insertObject( s6 ) )
		return false;
#if DEBUG
	s1->setName("sprite anim zona 1x1->1\n");
	s2->setName("sprite anim zona 1x1->2\n");
	s3->setName("sprite anim zona 2x2->1\n");
	s4->setName("sprite anim zona 2x2->2\n");
	s5->setName("sprite anim linha hor\n");
	s6->setName("sprite anim linha ver\n");
	LOG_FREE_MEM("depois de carregar as nuvens");
#endif

	return true;
}


bool DotGame::verifyLines( uint8 x, uint8 y ){
Point3f p1, p2, p3, p4,pZone;
//	uint8 x,y;
	bool ret = false;
	
	pZone = ZoneGroup::getZonePositionByXY( x, y); //ZoneGroup::getZoneXYByPosition( pZone, &x, &y);
#if DEBUG
	//NSLog(@" verificando zona %d  %d ",x,y);
#endif
	p1 = LineGroup::getLineCoordenatesByXY( x, y, true );
	p2 = LineGroup::getLineCoordenatesByXY( x + 1, y, true );
	p3 = LineGroup::getLineCoordenatesByXY( x, y, false );
	p4 = LineGroup::getLineCoordenatesByXY( x , y + 1, false );
	
	if( lineManager->isLineGained( &p1 )  &&
	   lineManager->isLineGained( &p2 ) &&
	   lineManager->isLineGained( &p3 ) &&
	   lineManager->isLineGained( &p4 ) ){
		ret |= zoneManager->zoneGainedByPlayer( /*&p4*/&pZone, currPlayer);
		if( ret )
			pInfoBar->updatePowers( this );
		if( !makeBigZone( &pZone ) ){
			if( ret ){
				//colocar aqui o código para aparecer a animação de 1x1!!
				//zoneManager->setZoneVisible( pZone, false );
				Point3f tmpZone;
				Sprite *tmp;
				if( !getObject( INDEX_CLOUD_ZONE_11_1 )->isVisible() ){
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_11_1 ) );
				}else {
					tmp = static_cast< Sprite* > ( getObject( INDEX_CLOUD_ZONE_11_2 ) );
				}
				tmpZone = pZone;
				tmpZone.set( tmpZone.x - ADJUST_CLOUD_ZONE_1X1_POSITION_X,
							tmpZone.y - ADJUST_CLOUD_ZONE_1X1_POSITION_Y,
							0.0);
				tmpZone += *zoneManager->getPosition();
				tmp->setPosition( &tmpZone );
				tmp->setVisible( true );
				tmp->setAnimSequence( 0, false, true);
				
#if DEBUG
//	//			NSLog(@"posicao da zona que foi selecionada: ( %4.2f,%4.2f)", pZone.x,pZone.y );
//	//			NSLog(@"posicao da fumaca  ( %4.2f,%4.2f)", tmp->getPosition()->x,tmp->getPosition()->y );
//				uint8 x,y;
//				/*ZoneGroup::*/getZoneXYByPosition( &pZone , &x ,&y );
//	//			NSLog(@"zona: %d,%d", x, y);
#endif		
				setDotGameState( DOT_GAME_STATE_ANIMATING_ZONE );
			}
		}

		combo++;
		pInfoBar->updateScores( this ); //currPlayer );//this );
#if DEBUG
	//	NSLog(@"lines selected %d ",linesSelected);
	//	NSLog(@"combo = %d ",combo);
#endif

	}
	return ret;


}


void  DotGame::getLineXyByPosition( Point3f *pLine , uint8 *x, uint8 *y ){
	for ( uint8 i = 0; i < nHorizontalZones ; i++ ) 
		for ( uint8 j = 0; j < nVerticalLines; j++) {
			if( Utils::IsPointInsideRect( pLine ,lineH[ i ][ j ] )){
				if( x )
					*x = i;
				
				if( y )
					*y = j;
				
				return;
			}
				}		
	
	for ( uint8 i = 0; i < nHorizontalLines ; i++ ) 
		for ( uint8 j = 0; j < nVerticalZones ; j++) 
			if( Utils::IsPointInsideRect( pLine ,lineV[ i ][ j ] ) ){
				if( x )
					*x = i;
				if( y )
					*y = j;
				return;
			}
#if DEBUG
//	NSLog(@"nenhuma linha nestas coordenadas");
#endif
}

void DotGame::getZoneXYByPosition( Point3f *pZone , uint8 *x, uint8 *y){
	for ( uint8 i = 0; i < nHorizontalZones; i++) 
		for ( uint8 j = 0; j< nVerticalZones ; j++ )
			if( Utils::IsPointInsideRect( pZone, zones[ i ][ j ] ) ){
				if( x )
					*x = i;
				if( y )
					*y = j;
				return;
			}
#if DEBUG
//	NSLog(@"nenhuma zona/quad nestas coordenadas");
#endif			
		
	
}

void DotGame::suspend( void ){


}

void DotGame::resume( void ){
	unpause();
}


void DotGame::unpause(void){ 
	if( state == DOT_GAME_STATE_PAUSED ){
		lineManager->setVisible( true );
		zoneManager->setVisible( true );
		pChronometer->pause( false );
		state = lastState;
		[(( FreeKickAppDelegate* ) APP_DELEGATE ) pauseAllSounds: false];
	
	}
		
}
