/*
 *  ModeSelectView.mm
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ModeSelectView.h"
#include "FreeKickAppDelegate.h"
#include "Config.h"
#include "Macros.h"

// NanoOnline
#include "NOControllerView.h"
#include "NOCustomer.h"
#include "NOGlobalData.h"
#include "NOString.h"


@interface ModeSelectView()

-( void )updateTxt;

@end


@implementation ModeSelectView
- ( void )awakeFromNib
{
	
	//colocar modificações aqui!!!!
	[super awakeFromNib];
	
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
}



// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton{
	[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
	if ( hButton == hSolo ) {
		
		[ hres setText:@"solo" ];
		GBinfo->mode = DOT_GAME_MODE_VERSUS_CPU ;
		
	}else if ( hButton == hVersusPlayer ) {

		[ hres setText:@"versus player" ];		
		GBinfo->mode = DOT_GAME_MODE_VERSUS_PLAYER_LOCAL ;
	}else if ( hButton == hOk ) {
		[ [ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_SELECT_DOLL ];
	}else if( hButton == hBack )  {
		[ [ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_MAIN_MENU ];
	}else if( hButton == hOnLine ){
		[ [ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_NANO_ONLINE_FROM_PLAY_WITH_PROFILE ];

	}
	[ self updateTxt ];
}

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo{
	GBinfo = GBInfo;
	
	if( NOGlobalData::GetActiveProfile().getProfileId() >= 0 ){
		mode = MODE_SELECT_VIEW_LOGGED;
		[ hOnLine setAlpha:0.0f ];
		//NOString nickname;
		//NOGlobalData::GetActiveProfile().getNickname( nickname );
	}else {
		mode = MODE_SELECT_VIEW_NOT_LOGGED;
		[ hTxt setText:@"usuário não logado!! \n se vc quiser logar com um perfil, clique no botão OnLine para entrar com um login" ];
	}
	
	if( mode == MODE_SELECT_VIEW_NOT_LOGGED )
	{
		//[ hOnLine setAlpha:0.0f ];
	}
	
	[ self updateTxt ];
}


-( void )updateTxt{

	switch ( GBinfo->mode ) {
		case DOT_GAME_MODE_VERSUS_CPU:
			[ hres setText:@"solo" ];
			[ hTxt setText:@"jogue contra a máquina" ];
			break;
		case DOT_GAME_MODE_VERSUS_PLAYER_LOCAL:
			[ hres setText:@"versus" ];
			[ hTxt setText:@"jogue contra alguém" ];
			break;
		case DOT_GAME_MODE_NONE:
			[ hres setText:@"modo" ];
			break;
		
			}
	if( mode == MODE_SELECT_VIEW_LOGGED ){
				[ hOnLine setAlpha:0.0f ];
				//NOString nickname;
				//NOGlobalData::GetActiveProfile().getNickname( nickname );
			}else {
				//[ hTxt setText:@"usuário não logado!! \n se vc quiser logar com um perfil, clique no botão OnLine para entrar com um login" ];
			}	

}


@end
