/*
 *  SelectView.h
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SELECT_VIEW
#define SELECT_VIEW 1

//#include"UpdatableView.h"
#include"GameBasicView.h"
#include"GameBaseInfo.h"
#include "CharacterFactory.h"

#include<vector>

enum  {
	SELECT_VIEW_STATE_NONE = -1,
	SELECT_VIEW_STATE_CHOOSING = 0,
	SELECT_VIEW_STATE_ANIMATING_1,//mudando de quadro
	SELECT_VIEW_STATE_ANIMATING_2,//mudando de tela
	SELECT_VIEW_STATE_CONFIRMING
}typedef SelectViewState;

@interface SelectView : GameBasicView//UpdatableView
{
//fada ogro fazendeiro executivo anjinho diabinho
//	IBOutlet UIButton* hAnjinho;
//	IBOutlet UIButton* hDiabinho;
//	IBOutlet UIButton* hFada;
//	IBOutlet UIButton* hFazendeiro;
//	IBOutlet UIButton* hExecutivo;
//	IBOutlet UIButton* hOgro;
@private
		//persistentens
	IBOutlet UILabel* hIdPlayer;
	IBOutlet UIButton* hOK;
	IBOutlet UIButton* hback;

		//somente na tela de seleção de personagem
	IBOutlet UIButton* hNextChar;
	IBOutlet UIButton* hPrevChar;
	IBOutlet UILabel* hPlayerId;
	
	IBOutlet UIImageView* h1;
	
	IBOutlet UIImageView* h2;

	IBOutlet UIImageView* h3;
	
	IBOutlet UIImageView* h4;

		//somente no estado de confirmação
	IBOutlet UIImageView* hBig;
	IBOutlet UITextView* hBio;
	//IBOutlet UIButton* hVoltar;
	
	//identificador de quantos jogadores faltam
	uint8 iPlayer;
	uint8 idAtual;
	
	//estado da view
	SelectViewState state,lastState;
	
	GameBaseInfo* GBinfo;
	
	int8 indice;
	std::vector< IdDoll > listCharacters;
	
	UIImageView* imageList[ N_PERSONS ];
	
	//CGRect positionList[ N_PERSONS ];
	CGPoint positionList[ N_PERSONS ];
	IdDoll currPer;
	float offsetX;
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo;

//método para ser chamado qdo o teclado sai de tela
//-( BOOL )textFieldShouldReturn:( UITextField * )textField ;

//método chamado qdo o segmentcontrol muda de valor
//- (void)segmentAction:(id)sender;

- ( void )update:( float )timeElapsed;

-( void )setSelectViewState:( SelectViewState )nState;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end

#endif