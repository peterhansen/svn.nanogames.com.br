/*
 *  DGOptions.mm
 *  dotGame
 *
 *  Created by Max on 12/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "DGOptions.h"
#include "Config.h"
#include "Macros.h"
#include"ObjcMacros.h"

#include "FreeKickAppDelegate.h"

@implementation DGOptions


-( void ) awakeFromNib{
	[super awakeFromNib];
}

- ( IBAction ) onBtPressed:( id )hButton{
	if( currState != DG_OPTIONS_STATE_SHOW )
		return;
	

		[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
	
	if( hButton == hBack ){
		[ self setDgOptionsMode:DG_OPTIONS_MODE_QUIT ];
		//[ [ ApplicationManager GetInstance ] performTransitionToView: previousScreen ];
	}else if ( hButton == hVbBoxOn || hButton == hVbBoxOff ) {
		[ self setVibrationStatus:( hButton == hVbBoxOn ) ];
	}else if( hButton == hHelpButton ) {
		[ self setDgOptionsMode: DG_OPTIONS_MODE_HELP];
	}else if( hButton == hSettings ) {
		[ self setDgOptionsMode: DG_OPTIONS_MODE_SETTINGS ];
	}else if( hButton == hContinue ) {
		[ [ ApplicationManager GetInstance ] performTransitionToView: previousScreen ];
	}else if( hButton == hYes ) {
		[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_MAIN_MENU];
	}else if( hButton == hNo ) {
		[ self setDgOptionsMode: DG_OPTIONS_MODE_HELP ];
	}
}


- ( IBAction ) onSldPressed:( id )hSld{
	if( currState != DG_OPTIONS_STATE_SHOW )
		return;
	if ( hSld == hVolum ) {
		
		[( ( FreeKickAppDelegate* )APP_DELEGATE ) setVolume: [hVolum value]];	
		NSLog(@" volume %1.2f ",[hVolum value]);
	}
	
}

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition{
	lastMode = mode = DG_OPTIONS_MODE_NONE;
	[ hVolum setValue: [ ( FreeKickAppDelegate* )APP_DELEGATE getAudioVolume ]];
	[ hYes setHidden:true];//setAlpha:0.0f ];
	[ hNo setHidden:true];
	[ hVolume setHidden:true];
	[ hVibra  setHidden:true];
	[ hVolum  setHidden:true];
	[ hVbBoxOn  setHidden:true];
	[ hVbBoxOff  setHidden:true];
	[ hHelpText  setHidden:true];
	[ hContinue  setHidden:true];
	[ self setDgOptionsMode:DG_OPTIONS_MODE_HELP];
	[ self setDgOptionsState:DG_OPTIONS_STATE_SHOW ];
}

-( void )setVibrationStatus:( bool )VibrationOn{
	if( [ ( ( FreeKickAppDelegate* ) APP_DELEGATE ) hasVibration ] ){
	
		//colocar o código para demonstrar qual modo está ativado.
		
		[ ( ( FreeKickAppDelegate* ) APP_DELEGATE ) setVibrationStatus: VibrationOn];
		if( VibrationOn ){
			NSLog(@"vibraçao ligada");
		}else {
			NSLog(@"vibraçao desligada");		
		}

	}

}


-( void )setPreviousIndex:( int8 )prevIndex{

	previousScreen = prevIndex;
}

-( void )setDgOptionsMode:( dgOptionsMode) nOption{
	lastMode = mode;
	mode = nOption;
	if( mode == lastMode )
		return;
	switch ( mode ) {
		case DG_OPTIONS_MODE_HELP:
			[hHelpText setHidden:false ];
			break;
		case DG_OPTIONS_MODE_QUIT:
			[ hYes  setHidden:false];
			[hNo  setHidden:false];
			break;
		case DG_OPTIONS_MODE_SETTINGS :
			[ hVolume  setHidden:false];
			[ hVibra  setHidden:false];
			[ hVolum  setHidden:false];
			[ hVbBoxOn  setHidden:false];
			[ hVbBoxOff  setHidden:false];
			break;
	}
	switch ( lastMode ) {
		case DG_OPTIONS_MODE_HELP:
			[ hHelpText  setHidden:true];
			break;
		case DG_OPTIONS_MODE_QUIT:
			[ hYes  setHidden:true];
			[ hNo  setHidden:true];
			break;
		case DG_OPTIONS_MODE_SETTINGS :
			[ hVolume  setHidden:true];
			[ hVibra  setHidden:true];
			[ hVolum setHidden:true];
			[ hVbBoxOn  setHidden:true];
			[ hVbBoxOff  setHidden:true];
			break;
	}
}

-( void )setDgOptionsStyle:( dgOptionsStyle ) nStyle{

	style = nStyle;
	if( style == DG_OPTIONS_STYLE_FROM_GAME ){
		[ hContinue  setHidden:false];
		
	}else if( style == DG_OPTIONS_STYLE_FROM_MENU ){
		[ hContinue  setHidden:true];
	//	[ hBack /*setCurrentTitle:@"back"*/ ];
		
	}
	
}

-( void )setDgOptionsState:( dgOptionsState ) nState{
	currState = nState;
}

-( void )update:( float ) timeElapsed{
	switch ( currState ) {
		case DG_OPTIONS_STATE_SHOWING :{
			switch ( mode ) {
				case DG_OPTIONS_MODE_SETTINGS :
					
					break;
				case DG_OPTIONS_MODE_HELP :
					
					break;
				case DG_OPTIONS_MODE_QUIT :
					
					break;
			}
		}
			break;
		case DG_OPTIONS_STATE_HIDDING :{
			switch ( lastMode ) {
				case DG_OPTIONS_MODE_SETTINGS :
					
					break;
				case DG_OPTIONS_MODE_HELP :
					
					break;
				case DG_OPTIONS_MODE_QUIT :
					
					break;
			}
			
		}
			break;
	}
}

@end
