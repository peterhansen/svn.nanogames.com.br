/*
 *  DGWinView.h
 *  dotGame
 *
 *  Created by Max on 6/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DG_WIN_VIEW_H
#define DG_WIN_VIEW_H 1

#include "GameBasicView.h"
#include "GameBaseInfo.h"

@interface DGWinView : GameBasicView{
	@private
	IBOutlet UIImageView* winnerImage;
	IBOutlet UIImageView* winnerBackground;	
	IBOutlet UIImageView* messageWinner;	
	IBOutlet UIButton* hMenu;
	IBOutlet UIButton* hPlayAgain;
	
}

-( IBAction ) onBtPressed:( id )hButton;

-( void ) onBeforeTransition: ( GameBaseInfo* )GInfo;

-( void )update:( float ) timeElapsed;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end



#endif