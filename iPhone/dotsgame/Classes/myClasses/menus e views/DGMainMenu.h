/*
 *  DGMainMenu.h
 *  dotGame
 *
 *  Created by Max on 12/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DG_MAIN_MENU
#define DG_MAIN_MENU 1

#include"GameBaseInfo.h"
#import "GameBasicView.h"
#import "UIBitmapLabel.h"


enum  {
	DG_MAIN_MENU_STATE_NONE = - 1,
	DG_MAIN_MENU_STATE_SHOWING = 0,
	DG_MAIN_MENU_STATE_WAITING_INPUT,
	DG_MAIN_MENU_STATE_HIDING
}typedef DGMainMenuState;

@interface DGMainMenu  : GameBasicView{
@private
	
	UIBitmapLabel* hTxtPlay;
	IBOutlet UIButton* hBtPlay;
	
	IBOutlet UIButton* hBtOption;
	UIBitmapLabel* hTxtOption;

	IBOutlet UIButton* hBtHelp;
	UIBitmapLabel* hTxtHelp;

	IBOutlet UIButton* hBtConfig;
	UIBitmapLabel* hTxtConfig;

	IBOutlet UIButton* hBtOnline;
	UIBitmapLabel* hTxtOnline;
	
	GameBaseInfo *ginfo;
	DGMainMenuState state;
	float timeCounter;
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void ) onBeforeTransition :( GameBaseInfo * )ginf;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

-( void )update:( float )timeElapsed;

@end




#endif