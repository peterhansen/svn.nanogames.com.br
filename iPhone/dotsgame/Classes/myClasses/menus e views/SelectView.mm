/*
 *  SelectView.mm
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "SelectView.h"
#include "FreeKickAppDelegate.h"
#include "Config.h"
#include "ObjcMacros.h"
#include "CharacterFactory.h"
#include "Random.h"

// NanoOnline
#include "NOControllerView.h"
#include "NOCustomer.h"
#include "NOGlobalData.h"
#include "NOString.h"

#define SPEED_ANIMATION_TIME 0.4f

#define ADJUST_LINE_MIN_POSITION 200.0f//269.0
#define OFFSET_IMAGE_POSITION_Y 80.0f

@interface SelectView( Private )
	
	-( bool )haveAnother: ( IdDoll )_test;

@end


@implementation SelectView

- ( void )awakeFromNib
{
	[super awakeFromNib];
	iPlayer = 0;
	state = SELECT_VIEW_STATE_NONE;
	[ hIdPlayer setText:@"player 1" ];
	iPlayer = 0;
	/*float*/
	offsetX = 10.0;
	listCharacters.assign( N_PERSONS, DOLL_ID_NONE );
	listCharacters.resize( listCharacters.size() );
	currPer = DOLL_ID_NONE;
	uint8 c = 0 ;
	//tem que ser desse jeito para produzir uma ordem aleatória, talvez o ideal seja passar isso para uma função...
	for( ; ; ){
		IdDoll _aux;
		_aux = CharacterFactory::GetInstance()->getIdDollByIndice( Random::GetInt( 0, CharacterFactory::GetInstance()->getNCharacters() - 1 ) );
		
		for (uint8 j = 0 ; j < listCharacters.size(); j++) {
			if ( [self haveAnother:_aux  ] ) {
				break;
			}else {
				listCharacters.at( c ) = _aux;
				imageList[ c ] = [ [ UIImageView alloc] initWithImage:CharacterFactory::GetInstance()->getPicturePersonagen( _aux )  ];
				positionList[ c ] = imageList[c].bounds.origin;
				positionList[ c ].x += offsetX ;//+imageList[ c ].center.x;//size.width ;
				positionList[ c ].y += OFFSET_IMAGE_POSITION_Y ;
				//+ positionList[ c ].size.width; 
				//positionList[ c ].origin.y = positionList[ c ].size.height - ADJUST_LINE_MIN_POSITION ;
				++c; 
			}
		}
		if( c >= CharacterFactory::GetInstance()->getNCharacters() )
			break;
	}
	
	indice = 0;
	[ h1 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ ( indice - 1 < 0 ? CharacterFactory::GetInstance()->getNCharacters() - 1 : indice - 1 ) ] )];
	[ h2 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ indice ] )];
	[ h3 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ ( indice + 1 ) >= CharacterFactory::GetInstance()->getNCharacters() ? 0 : indice + 1] ) ];
	
	currPer = listCharacters[ indice ];
	//float next;
//	for( uint8 i = 0 ; i < N_PERSONS ; i++ ){}
	[ self setSelectViewState:SELECT_VIEW_STATE_CHOOSING ];	
	
}

#define MAX_BUFFER_DOLL_NAME 15
// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton{
	//[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
	
	if( hButton == hOK ){
		if( state == SELECT_VIEW_STATE_CONFIRMING ){
			
			GBinfo->iddolls[ iPlayer ] = currPer;
			GBinfo->iddolls[ iPlayer + 1 ] =  listCharacters[ Random::GetInt( 0 , N_PERSONS - 1  ) ] ;
			if( ++iPlayer >= GBinfo->nPlayers ){
				[ [ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_CONFIG_MATCH ];
			}else {
				[ self setSelectViewState:SELECT_VIEW_STATE_CHOOSING ];
			}
						
		}else if( state == SELECT_VIEW_STATE_CHOOSING ) {
			
			[ self setSelectViewState:SELECT_VIEW_STATE_CONFIRMING ];
		}

	}else if(hButton == hPrevChar /*hNextChar*/ ) {
		//CharacterFactory::GetInstance()->getNCharacters();

		indice = ( ++indice >= CharacterFactory::GetInstance()->getNCharacters() ? 0 : indice );
		[ h1 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ ( indice - 1 < 0 ? CharacterFactory::GetInstance()->getNCharacters() - 1 : indice - 1 ) ] )];
		[ h2 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ indice ] )];
		[ h3 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ ( indice + 1 ) >= CharacterFactory::GetInstance()->getNCharacters() ? 0 : indice + 1] ) ];
		currPer = listCharacters[ indice ];
	}
	else if(hButton == hback ){
		if ( state == SELECT_VIEW_STATE_CHOOSING ){
			[[ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_SELECT_MODE ];
		}else if( state == SELECT_VIEW_STATE_CONFIRMING ) {
			[ self setSelectViewState:SELECT_VIEW_STATE_CHOOSING ];
		}
	}else if( hButton == hNextChar/* hPrevChar*/ ) {

		indice = ( --indice < 0 ? CharacterFactory::GetInstance()->getNCharacters() - 1 : indice );
		CGPoint aux;
		
		[ h1 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[( (indice - 1) < 0 ? CharacterFactory::GetInstance()->getNCharacters() - 1 : indice - 1 ) ] )];
		[ h2 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ indice  ] )]; 
		[ h3 setImage:CharacterFactory::GetInstance()->getPicturePersonagen( listCharacters[ ( indice + 1 ) >= CharacterFactory::GetInstance()->getNCharacters() ? 0 : indice + 1] )];
		
		float sizeMin,x;
	
		[ h2 setCenter:CGPointMake( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT )];
		 
		aux = positionList[ ( indice - 1 < 0 ? CharacterFactory::GetInstance()->getNCharacters() - 1 : indice - 1 ) ];
		x = h2.center.x;
		x -= ( h1.image.size.width + h2.image.size.width ) * 0.5;
		x= ( x < h1.image.size.width?  -h1.image.size.width * 0.5 : x );
		[ h1 setCenter:CGPointMake( aux.x - x, HALF_SCREEN_HEIGHT /*- ( h1.frame.size.height  * 0.5 )+30.0*/ ) ];
			
		sizeMin = - h1.frame.size.width ;		
		aux = positionList[ indice ];
		sizeMin = SCREEN_WIDTH - h3.frame.size.width;
		aux = positionList[ ( indice + 1 ) >= CharacterFactory::GetInstance()->getNCharacters() ? 0 : indice + 1  ];
		[ h3 setCenter:CGPointMake( SCREEN_WIDTH - 20.0/*h3.center.x*/, h3.center.y ) ];
	
		currPer = listCharacters[ indice ];
	}
}


// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo{
	GBinfo = GBInfo;

	//acertar para um tratamento melhor, provavelmente vai ser transeferido para outra tela...
	if( NOGlobalData::GetActiveProfile().getProfileId() >= 0 ){
		NOString nickname;
		NOGlobalData::GetActiveProfile().getNickname( nickname );
	}else {
		LOG("ninguém logado");
	}
	
	
}
#undef MAX_BUFFER_DOLL_NAME

///*===================================================================================================================================
// 
//método chamado qdo o usuário aperta voltar no teclado, ele somente utiliza os 5 primeiros caracteres, e depois atualiza o campo nome.
//
// ====================================================================================================================================*/
//
//#define MAX_BUFFER_NAME_LEN 6
//
//- (BOOL)textFieldShouldReturn:(UITextField *)textField 
//{
//	char buffer[ MAX_BUFFER_NAME_LEN ];
//	//snprintf( buffer , MAX_BUFFER_NAME_LEN , NSSTRING_TO_CHAR_ARRAY (hNome.text) );
//	
//	//[hNome setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];
//
//	GBinfo->nomes[ iPlayer ] = buffer ;
//	
//	[textField resignFirstResponder]; 
//	return YES; 
//}
//
//#undef MAX_BUFFER_NAME_LEN


- ( void )update:( float )timeElapsed{
	switch ( state ) {
		case SELECT_VIEW_STATE_NONE:
			
			break;
		case SELECT_VIEW_STATE_CONFIRMING:
			
			break;
		case SELECT_VIEW_STATE_CHOOSING:
 
			break;
		case SELECT_VIEW_STATE_ANIMATING_1:{
			CGRect quad;
			UIImageView* tmpvec[] = { h1 , h2, h3 }; 
			for( uint8 aux = 0; aux < 3 ; aux++ ){
				quad = tmpvec[ aux ].frame;
			//	[ tmpvec[ aux ] setFrame:CGRectMake(quad.origin.x - ( destiny / timeElapsed ) + offsetX, quad.origin.y, quad.size.width, quad.size.height) ]; 
			}
			
		}break;
	}
	
}

-( void )setSelectViewState:( SelectViewState )nState{
	lastState = state;
	state = nState;
	
	switch ( state ) {
		case SELECT_VIEW_STATE_NONE:
			break;
			
		case SELECT_VIEW_STATE_CONFIRMING:
			[ hNextChar setHidden:true ];
			[ hPrevChar setHidden:true ];
			[ h2 setHidden:true ];
			[ h3 setHidden:true ];
			[ h1 setHidden:true ];
			[ hBig setHidden:false ];
			[ hBio setHidden:false ];
			[ hBig setImage:CharacterFactory::GetInstance()->getPicturePersonagen( currPer )  ];
			break;

		case SELECT_VIEW_STATE_CHOOSING:
			[ hBig setHidden:true ];
			[ hBio setHidden:true ];
			[ hNextChar setHidden:false ];
			[ hPrevChar setHidden:false ];
			[ h2 setHidden:false ];
			[ h3 setHidden:false ];
			[ h1 setHidden:false ];
			break;
	
		case SELECT_VIEW_STATE_ANIMATING_1:
			break;
	}
	

}

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen{
	[ self resume ];
}

-( bool )haveAnother: ( IdDoll )_test{
	for( uint8 x = 0; x < listCharacters.size() ;x++ )
		if ( listCharacters[ x ] == _test )
			return true;
	return false;
}

//-( void )drawRect:( CGRect )rect{
//	printf("renderizando\n");
//}

-( void )dealloc{
	[ super dealloc ];
	for ( uint8 i = 0 ; i < N_PERSONS ; i++ ) {
		[ imageList[ i ] release ];
	}
}

@end
