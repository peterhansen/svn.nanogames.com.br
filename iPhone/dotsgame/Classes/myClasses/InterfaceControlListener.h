/*
 *  InterfaceControlListener.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef INTERFACE_CONTROL_LISTENER_H
#define INTERFACE_CONTROL_LISTENER_H 1

#include "InterfaceControlListener.h"

// Forward Declarations
class InterfaceControl;

class InterfaceControlListener
{
	public:
		// Destrutor
		virtual ~InterfaceControlListener( void ){};

		// Método chamado para indicar que a animação do controle foi finalizada
		virtual void onInterfaceControlAnimCompleted( InterfaceControl* pControl ) = 0;
};

#endif
