#include "InterfaceControl.h"
#include "InterfaceControlListener.h"

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

InterfaceControl::InterfaceControl( InterfaceControlListener* pListener ) : pListener( pListener ), interfaceControlState( INTERFACE_CONTROL_STATE_UNDEFINED )
{
}

/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

InterfaceControl::~InterfaceControl( void )
{
	pListener = NULL;
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exibição do controle.

============================================================================================*/

void InterfaceControl::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	interfaceControlState = state;
	
	if( notifyListener )
	{
		if( interfaceControlState == INTERFACE_CONTROL_STATE_SHOWN )
		{
			// OLD pListener->onControlShown( this );
			pListener->onInterfaceControlAnimCompleted( this );
		}
		else if( interfaceControlState == INTERFACE_CONTROL_STATE_HIDDEN )
		{
			// OLD pListener->onControlHidden( this );
			pListener->onInterfaceControlAnimCompleted( this );
		}
	}
}
