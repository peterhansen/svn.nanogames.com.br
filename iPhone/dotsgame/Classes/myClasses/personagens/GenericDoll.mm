/*
 *  GenericDoll.mm
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "GenericDoll.h"
#include "MathFuncs.h"
#include "Exceptions.h"
Sprite* GenericDoll::linhaPlV = NULL;
Sprite* GenericDoll::linhaPlH = NULL;


GenericDoll::GenericDoll( IdDoll Idboneco ):iddoll( Idboneco ),
face( DOLL_EXPRESSION_SERIOUS ), bColor( NULL ){

}

bool GenericDoll::loadAllImages( void ){
	if( !linhaPlH )
		linhaPlH = new Sprite( "ch","ch",ResourceManager::Get16BitTexture2D );
	if( !linhaPlV )
		linhaPlV = new Sprite( "cv","cv",ResourceManager::Get16BitTexture2D );
#if DEBUG 
	//	cloudH->setName("Nuvem Horizontal estática");
	//	cloudV->setName("Nuvem Vertical estática");
	linhaPlH->setName("linha hor jogador estatica\n");
	linhaPlV->setName("linha ver jogador estatica\n");
	NSLog(@"imagens estáticas carregados\n");	
#endif
	
if( !linhaPlH || !linhaPlV  )
	goto Error;
	
	return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading Dolls Lines images");
#else
	throw ConstructorException();
#endif 	
	return false;
	
}

Renderable* GenericDoll::getLineHPic( ){
	return linhaPlH;
}	

Renderable* GenericDoll::getLineVPic( ){
	return linhaPlV;
}


bool GenericDoll::unloadAllImages( void ){
	SAFE_DELETE(linhaPlH);
	SAFE_DELETE(linhaPlV);

	return !linhaPlH && !linhaPlV ;
}

int8 GenericDoll::getIndiceByExpression( /*DollExpressions _d*/ ){
	int8 ret = -1;
	switch ( face ) {
		case DOLL_EXPRESSION_SERIOUS:
			ret = INDEX_EXPRESSION_SERIOS;
			break;
		case DOLL_EXPRESSION_SAD:
			ret = INDEX_EXPRESSION_SAD;
			break;
		case DOLL_EXPRESSION_HAPPY:
			ret = INDEX_EXPRESSION_HAPPY;
			break;
		case DOLL_EXPRESSION_VERY_SAD:
			ret = INDEX_EXPRESSION_FURIOSLY;
			break;
		case DOLL_EXPRESSION_VERY_HAPPY:
			ret = INDEX_EXPRESSION_VERY_HAPPY;
			break;
			
	}
	return ret;
}

void GenericDoll::setExpressionByPercetinl( float percent ){
//como é um float, tem que ser if(){}else{} mesmo
	if( NanoMath::fleq( percent, 20.0f ) ){
		face = DOLL_EXPRESSION_VERY_SAD;
	}else if ( NanoMath::fleq( percent, 40.0f )  ){
		face = DOLL_EXPRESSION_SAD;		
	}else if ( NanoMath::fleq( percent, 60.0f )  ){
		face = DOLL_EXPRESSION_SERIOUS;
	}else if ( NanoMath::fleq( percent, 80.0f )  ){
		face = DOLL_EXPRESSION_HAPPY;
	}else if (NanoMath::fleq( percent, 100.0f ) ){
		face = DOLL_EXPRESSION_VERY_HAPPY;
	}
}
