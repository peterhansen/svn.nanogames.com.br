/*
 *  Fazendeiro.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Fazendeiro.h"
#include "Random.h"
#include"Sprite.h"
#include "Exceptions.h"

RenderableImage* Fazendeiro::bigZoneSpt1 = NULL;

RenderableImage* Fazendeiro::bigZoneSpt2 = NULL;

RenderableImage* Fazendeiro::ZoneSpt1= NULL;

RenderableImage* Fazendeiro::ZoneSpt2= NULL;

RenderableImage* Fazendeiro::ZoneSpt3= NULL;

Sprite* Fazendeiro::FacePic = NULL;

Fazendeiro::Fazendeiro():GenericDoll( DOLL_ID_FARMER ){}


bool Fazendeiro::LoadAllImages( void ){
	
	
Texture2DHandler htex1, htex2, htex3,htex4,htex5;
	char name[] = {'f','a','r','m','2','2','_','1','\0'};
	
	if( !bigZoneSpt1 )
		bigZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	name[ 7 ] = '2';
	if( !bigZoneSpt2 )
		bigZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );	
	name[ 4 ] = '1';
	name[ 5 ] = '1';
	name[ 7 ] = '1';
	if( !ZoneSpt1 )
		ZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );

	name[ 7 ] = '2';
	if( !ZoneSpt2 )
		ZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	name[ 7 ] = '3';	
	if( !ZoneSpt3 )
		ZoneSpt3 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		if( !FacePic )
		FacePic = new Sprite( "farmFace","farmFace" );
	if( !bigZoneSpt1 || !bigZoneSpt2 || !ZoneSpt1 || !ZoneSpt2 || !ZoneSpt3 || !FacePic )
		goto Error;
#if DEBUG
	NSLog(@"imagens fazendeira carregadas");
#endif	
	return true;

	Error:
#if DEBUG
	throw ConstructorException("fail while loading farmer images");
#else
	throw ConstructorException();
#endif 	
	return false;
}

void Fazendeiro::RemoveAllImages( void ){
	SAFE_DELETE( bigZoneSpt1 );
	SAFE_DELETE( bigZoneSpt2 );
	SAFE_DELETE( ZoneSpt1 );
	SAFE_DELETE( ZoneSpt2 );
	SAFE_DELETE( ZoneSpt3 );
	SAFE_DELETE( FacePic );
}

Renderable* Fazendeiro::getZoneSprt( void ){
	switch ( Random::GetInt( 0 ,2  ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
		case 2:
			return ZoneSpt3;
			break;
	}
	return NULL;
}
	
Renderable* Fazendeiro::getBigZoneSprt( void ){

	switch ( Random::GetInt( 0 ,1  ) ) {
		case 0:
			return bigZoneSpt1;
			break;
		case 1:
			return bigZoneSpt2;
			break;
	}
	return NULL;
}

