/*
 *  Anjinho.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Anjinho.h"
#include"Sprite.h"
#include "Random.h"
#include "Exceptions.h"
#include "LineGame.h"


RenderableImage* Anjinho::bigZoneSpt1 = NULL;

RenderableImage* Anjinho::bigZoneSpt2 = NULL;

RenderableImage* Anjinho::ZoneSpt1 = NULL;

RenderableImage* Anjinho::ZoneSpt2 = NULL;

RenderableImage* Anjinho::ZoneSpt3 = NULL;

Sprite* Anjinho::FacePic = NULL;


Anjinho::Anjinho():GenericDoll( DOLL_ID_ANGEL ){}


bool Anjinho::LoadAllImages( void ){

	//	Texture2DHandler htex1, htex2, htex3;
	char name[] = {'a','n','g','2','2','_','1','\0'};
	
	if ( !bigZoneSpt1 )	
		bigZoneSpt1 = new RenderableImage( name );
	name[ 6 ] = '2';
	
	if( !bigZoneSpt2 )
		bigZoneSpt2 = new RenderableImage( name );
	name[ 3 ] = '1';
	name[ 4 ] = '1';
	name[ 6 ] = '1';
	if( !ZoneSpt1 )
		ZoneSpt1 = new RenderableImage( name );
	name[ 6 ] = '2';
	if( !ZoneSpt2 )
		ZoneSpt2 = new RenderableImage( name );
	name[ 6 ] = '3';
	
	if( !ZoneSpt3 )
		ZoneSpt3 = new RenderableImage( name );
	
	if( !FacePic )
		FacePic = new Sprite( "angFace","angFace" );
	
	if( !bigZoneSpt1 || !bigZoneSpt2 || !ZoneSpt1 || !ZoneSpt2 || !ZoneSpt3 || !FacePic )
		goto Error;

#if DEBUG
	NSLog(@"imagens anjo carregadas");
#endif	
	return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading Angel images");
#else
	throw ConstructorException();
#endif 	
	return false;
}

void Anjinho::RemoveAllImages( void ){
	SAFE_DELETE( bigZoneSpt1 );
	SAFE_DELETE( bigZoneSpt2 );
	SAFE_DELETE( ZoneSpt1 );
	SAFE_DELETE( ZoneSpt2 );
	SAFE_DELETE( ZoneSpt3 );
	SAFE_DELETE( FacePic );
}

Renderable* Anjinho::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
			
		case 2:
			return ZoneSpt3;
			break;
	}
	return NULL;
}
	
Renderable* Anjinho::getBigZoneSprt( void ){

	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
	return NULL;
}
