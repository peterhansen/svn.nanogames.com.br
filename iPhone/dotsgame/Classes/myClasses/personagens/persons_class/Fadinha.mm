/*
 *  Fadinha.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Fadinha.h"
#include"Sprite.h"
#include "Random.h"
#include "Exceptions.h"

RenderableImage* Fadinha::bigZoneSpt1 = NULL;

RenderableImage* Fadinha::bigZoneSpt2 = NULL;

RenderableImage* Fadinha::ZoneSpt1 = NULL;

RenderableImage* Fadinha::ZoneSpt2 = NULL;

RenderableImage* Fadinha::ZoneSpt3 = NULL ;

Sprite* Fadinha::FacePic = NULL;


Fadinha::Fadinha():GenericDoll( DOLL_ID_FAIRY ){}


bool Fadinha::LoadAllImages( void ){
	char name[] = {'f','a','y','2','2','_','1','\0'};
	if( !bigZoneSpt1 )
		bigZoneSpt1 = new RenderableImage( name);// , RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	
	name[ 6 ] = '2';
	
	if( !bigZoneSpt2 )
		bigZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	name[ 3 ] = '1';
	name[ 4 ] = '1';
	name[ 6 ] = '1';
	if( !ZoneSpt1 )
		ZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	name[ 6 ] = '2';
	
	if( !ZoneSpt2 )
		ZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	name[ 6 ] = '3';
	
	if( !ZoneSpt3 )
		ZoneSpt3 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	if( !FacePic )
		FacePic = new Sprite( "fayFace","fayFace" );

		if( !bigZoneSpt1 || !bigZoneSpt2 || !ZoneSpt1 || !ZoneSpt2 || !ZoneSpt3 || !FacePic  )
		goto Error;
#if DEBUG
	NSLog(@"imagens fada carregadas");
#endif
	return true;
	Error:
#if DEBUG
	throw ConstructorException("fail while loading Fairy images");
#else
	throw ConstructorException();
#endif 	
	return false;
	
}

void Fadinha::RemoveAllImages( void ){
#if DEBUG
	NSLog(@"removendo imagens=> fada");
#endif
	SAFE_DELETE( bigZoneSpt1 );
	SAFE_DELETE( bigZoneSpt2 );
	SAFE_DELETE( ZoneSpt1 );
	SAFE_DELETE( ZoneSpt2 );
	SAFE_DELETE( ZoneSpt3 );
	SAFE_DELETE( FacePic );
}

Renderable*  Fadinha::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
		case 2:
			return ZoneSpt3;
			break;
	}
	return NULL;
}

Renderable*  Fadinha::getBigZoneSprt( void ){
		switch (Random::GetInt( 0 , 1 )) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
	return NULL;
}

