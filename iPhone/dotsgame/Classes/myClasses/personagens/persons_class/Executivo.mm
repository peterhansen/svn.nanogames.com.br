/*
 *  Executivo.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Executivo.h"
#include "Sprite.h"
#include "Random.h"
#include "Exceptions.h"

//Sprite* Executivo::bigZoneSpt = NULL;
//
//Sprite* Executivo::ZoneSpt1= NULL;
//
//Sprite* Executivo::ZoneSpt2= NULL;

//Sprite* Executivo::LineV = NULL;
//
//Sprite* Executivo::LineH = NULL;	

RenderableImage* Executivo::bigZoneSpt1 = NULL;

RenderableImage* Executivo::bigZoneSpt2 = NULL;

RenderableImage* Executivo::ZoneSpt1= NULL;

RenderableImage* Executivo::ZoneSpt2= NULL;

RenderableImage* Executivo::ZoneSpt3= NULL;

Sprite* Executivo::FacePic = NULL;


Executivo::Executivo():GenericDoll( DOLL_ID_ENGINNER ){}


bool Executivo::LoadAllImages( void ){
	Texture2DHandler htex1, htex2, htex3, htex4, htex5;
	char name[] = {'e','x','e','2','2','_','1','\0'};
		if( !bigZoneSpt1 )
			bigZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		
		name[ 6 ] = '2';
		
		if( !bigZoneSpt2 )
			bigZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		
		name[ 3 ] = '1';
		name[ 4 ] = '1';
		name[ 6 ] = '1';
		if( !ZoneSpt1 )
			ZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		name[ 6 ] = '2';

		if( !ZoneSpt2 )
			ZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		name[ 6 ] = '3';

		if( !ZoneSpt3 )
			ZoneSpt3 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
	
	if( !FacePic )
		FacePic = new Sprite( "engFace","engFace" );

	if( !bigZoneSpt1 || !bigZoneSpt2 || !ZoneSpt1 || !ZoneSpt2 || !ZoneSpt3 || !FacePic)
		goto Error;
#if DEBUG
	NSLog(@"imagens civil enginner carregadas");
#endif
	return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading civil enginner images");
#else
	throw ConstructorException();
#endif 	
	return false;

}

void Executivo::RemoveAllImages( void ){
	SAFE_DELETE( bigZoneSpt1 );
	SAFE_DELETE( bigZoneSpt2 );
	SAFE_DELETE( ZoneSpt1 );
	SAFE_DELETE( ZoneSpt2 );
	SAFE_DELETE( ZoneSpt3 );
	SAFE_DELETE( FacePic );
}


Renderable* Executivo::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
			
		case 2:
			return ZoneSpt3;
			break;
	}
	return NULL;
}
	
Renderable* Executivo::getBigZoneSprt( void ){

	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
	return NULL;
}


//
//Sprite* Executivo::getZoneSprt( void ){
////	return ZoneSpt;
//}
//
// Sprite* Executivo::getBigZoneSprt( void ){
////	return ZoneSpt;
//}
