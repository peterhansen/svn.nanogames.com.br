/*
 *  diabinho.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Diabinho.h"
#include "Exceptions.h"
#include"Sprite.h"
#include "Random.h"
RenderableImage* Diabinho::bigZoneSpt1 = NULL;

RenderableImage* Diabinho::bigZoneSpt2 = NULL;

RenderableImage* Diabinho::ZoneSpt1= NULL;

RenderableImage* Diabinho::ZoneSpt2= NULL;

RenderableImage* Diabinho::ZoneSpt3= NULL;

Sprite* Diabinho::FacePic = NULL;



Diabinho::Diabinho():GenericDoll( DOLL_ID_DEVIL ){}

//botar para carregar o sprite da cerca aqui!!
bool Diabinho::LoadAllImages( void ){
	{
		
		
		char name[] = {'d','e','v','2','2','_','1','\0'};
		
		if( !bigZoneSpt1 )
			bigZoneSpt1 = new RenderableImage( name );//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
		
		name[ 6 ] = '2';
		
		if( !bigZoneSpt2 )
			bigZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
		
		name[ 3 ] = '1';
		name[ 4 ] = '1';
		name[ 6 ] = '1';
		if( !ZoneSpt1 )
			ZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		name[ 6 ] = '2';

		if( !ZoneSpt2 )
			ZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
		name[ 6 ] = '3';

		if( !ZoneSpt3 )
			ZoneSpt3 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	if( !FacePic )
		FacePic = new Sprite( "devilFace","devilFace" );
		
	if( !bigZoneSpt1 || !bigZoneSpt2 || !ZoneSpt1 || !ZoneSpt2 || !ZoneSpt3 || !FacePic  )
		goto Error;
#if DEBUG
	NSLog(@"imagens diabo carregadas");
#endif
	return true;
	}
Error:
#if DEBUG
	throw ConstructorException("fail while loading devil images");
#else
	throw ConstructorException();
#endif 	
	return false;
}

void Diabinho::RemoveAllImages( void ){
	//temporário!
	SAFE_DELETE( bigZoneSpt1 );
	SAFE_DELETE( bigZoneSpt2 );
	SAFE_DELETE( ZoneSpt1 );
	SAFE_DELETE( ZoneSpt2 );
	SAFE_DELETE( ZoneSpt3 );
	SAFE_DELETE( FacePic );
} 

Renderable* Diabinho::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
			
		case 2:
			return ZoneSpt3;
			break;
	}
	return NULL;
}

Renderable* Diabinho::getBigZoneSprt( void ){
	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return bigZoneSpt2;
			break;
		case 1:
			return bigZoneSpt1;
			break;
	}
	return NULL;
}