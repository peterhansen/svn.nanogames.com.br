/*
 *  Fadinha.h
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef FAIRY_H
#define FAIRY_H 1

#include "GenericDoll.h"

/*
 cada classe de um boneco vai ter como dados as animações de zona normal e gde, assim como a imagem do rosto do boneco de modo estático. 
 
 */


class Sprite;

class Fadinha : public GenericDoll {

public:
	
	Fadinha();
	
	~Fadinha(){
#if DEBUG
		NSLog(@"deletando fada");
		
#endif
		
	};
	
	static bool LoadAllImages( void );
	
	static void RemoveAllImages( void );
	
	virtual Renderable* getZoneSprt( void );
	
	virtual	Renderable* getBigZoneSprt( void );
	
	virtual Renderable* getFacePic( void ){ return FacePic; };

protected:
	static RenderableImage* bigZoneSpt1;
	
	static RenderableImage* bigZoneSpt2;
	
	static RenderableImage* ZoneSpt1;
	
	static RenderableImage* ZoneSpt2;
	
	static RenderableImage* ZoneSpt3;

	static Sprite* FacePic;

};

#endif