/*
 *  diabinho.h
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef DEVIL_H
#define DEVIL_H 1

#include "GenericDoll.h"

/*
 cada classe de um boneco vai ter como dados as animações de zona normal e gde, assim como a imagem do rosto do boneco de modo estático. 
 
 */


class Sprite;

class Diabinho : public GenericDoll {

public:
	
	Diabinho();
	
	~Diabinho(){
#if DEBUG
		NSLog(@"deletando diabinho");
		
#endif
		
	};
	
//	
//	virtual bool getLineHPic(  Renderable* r );	
//	
//	virtual bool getLineVPic( Renderable* r );	
	
	//Renderable* getLineHPic( void );	
	
	//Renderable* getLineVPic( void );	

	static bool LoadAllImages( void );
	
	static void RemoveAllImages( void );
	
	virtual Renderable* getFacePic( void ){ return FacePic; };

	virtual Renderable* getZoneSprt( void );
	
	virtual	Renderable* getBigZoneSprt( void );
	
	
protected:
	static RenderableImage* bigZoneSpt1;
	
	static RenderableImage* bigZoneSpt2;
	
	static RenderableImage* ZoneSpt1;
	
	static RenderableImage* ZoneSpt2;
	
	static RenderableImage* ZoneSpt3;

	static Sprite* FacePic;

	//static Sprite* LineV;
	//static Sprite* LineH;	
	
};


#endif