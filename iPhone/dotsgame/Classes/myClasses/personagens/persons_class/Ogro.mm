/*
 *  Ogro.mm
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "Ogro.h"
#include "Random.h"
#include"Sprite.h"
#include "Exceptions.h"

RenderableImage* Ogro::bigZoneSpt1 = NULL;

RenderableImage* Ogro::bigZoneSpt2 = NULL;

RenderableImage* Ogro::ZoneSpt1 = NULL;

RenderableImage* Ogro::ZoneSpt2 = NULL;

RenderableImage* Ogro::ZoneSpt3 = NULL;

Sprite* Ogro::FacePic = NULL;

Ogro::Ogro():GenericDoll( DOLL_ID_OGRE ){}

bool Ogro::LoadAllImages( void ){
	
	Texture2DHandler htex1, htex2, htex3,htex4,htex5;
	
	char name[] = {'o','g','r','e','2','2','_','1','\0'};
	
	if( !bigZoneSpt1 )
		bigZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	name[ 7 ] = '2';
	
	if( !bigZoneSpt2 )
		bigZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	name[ 4 ] = '1';
	name[ 5 ] = '1';
	name[ 7 ] = '1';
	
	if( !ZoneSpt1 )
		ZoneSpt1 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	name[ 7 ] = '2';
	
	if( !ZoneSpt2 )
		ZoneSpt2 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	name[ 7 ] = '3';	
	if( !ZoneSpt3 )
		ZoneSpt3 = new RenderableImage( name);//, RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D );
	
	if( !FacePic )
		FacePic = new Sprite( "ogroFace","ogroFace" );
	
	//TODOO depois trocar por um AND
	if( !bigZoneSpt1 || !bigZoneSpt2 || !ZoneSpt1 || !ZoneSpt2 || !ZoneSpt3 || !FacePic )
		goto Error;
#if DEBUG
	NSLog(@"imagens ogro carregadas");
#endif	
		return true;
Error:
#if DEBUG
	throw ConstructorException("fail while loading ogre images");
#else
	throw ConstructorException();
#endif 	
	return false;
	
}

void Ogro::RemoveAllImages( void ){
	SAFE_DELETE( bigZoneSpt1 );
	SAFE_DELETE( bigZoneSpt2 );
	SAFE_DELETE( ZoneSpt1 );
	SAFE_DELETE( ZoneSpt2 );
	SAFE_DELETE( ZoneSpt3 );
	SAFE_DELETE( FacePic );
}
Renderable* Ogro::getZoneSprt( void ){
	switch ( Random::GetInt( 0 , 2 ) ) {
		case 0:
			return ZoneSpt1;
			break;
		case 1:
			return ZoneSpt2;
			break;
		case 2:
			return ZoneSpt3;
			break;
	}
	return NULL;
}
	
Renderable* Ogro::getBigZoneSprt( void ){

	switch ( Random::GetInt( 0 , 1 ) ) {
		case 0:
			return bigZoneSpt1;
			break;
		case 1:
			return bigZoneSpt2;
			break;
	}
	return NULL;
}
