/*
 *  CharacterFactory.h
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef CHARACTER_FACTORY_H
#define CHARACTER_FACTORY_H 1

//#include "DotGameInfo.h"
#include "Color.h"

#define N_PERSONS 6
#include "NanoTypes.h"
#include <vector>
using namespace std;
//idem...
enum IdDoll {
	DOLL_ID_NONE = -1,
	DOLL_ID_ANGEL = 0,
	DOLL_ID_DEVIL = 1,
	DOLL_ID_FARMER,
	DOLL_ID_ENGINNER,
	DOLL_ID_FAIRY,
	DOLL_ID_OGRE	
} typedef IdDoll;

#define ID_PERSON_ANGEL 0;
#define ID_PERSON_DEVIL 1;
#define ID_PERSON_FARMER 2;
#define ID_PERSON_ENGINNER 3;
#define ID_PERSON_FAIRY 4;
#define ID_PERSON_OGRE 5;


class GenericDoll;
struct GameBaseInfo;
class CharacterFactory {
	
public:

	
//void LoadCharacterIndice( GenericDoll *g , int indice );
	
GenericDoll */*void*/ LoadCharacterIndice( /*GenericDoll *g ,*/ IdDoll id );
	
	static bool Create( void );
	
	static void Destroy( void );

	static CharacterFactory* GetInstance( void ) ;//{ return pSingleton; };
	
	void LoadAllImages( GameBaseInfo &g );
	
	void RemoveAllImages( GameBaseInfo &g );
	
	void setNCharacters( uint8 n  ){ nCharacters = n;  }
	
	uint8 getNCharacters( void );
		
	uint8 setFrameByCharacter( IdDoll _id );
	
	//void getWinImages( IdDoll _id , UIImage* bg, UIImage* character );
	
	UIImage* getWinCharImages( IdDoll _id );
	
	UIImage* getWinBgImages( IdDoll _id );
	
	void getColorByCharacter( IdDoll _index, Color *pColor );
	
	UIImage* getPicturePersonagen( uint8 index );///*uint8 index*/ IdDoll index );

    IdDoll	getIdDollByIndice( uint8 index );

	UIImage* getPicturePersonagen( IdDoll index );

private:	
	CharacterFactory();

	~CharacterFactory();
	static CharacterFactory* pSingleton;
	
	uint8 nCharacters;
	
};


#endif
