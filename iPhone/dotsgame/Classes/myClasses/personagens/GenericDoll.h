/*
 *  GenericDoll.h
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef GENERIC_DOLL_H
#define GENERIC_DOLL_H 1

#include "DotGameInfo.h"
#include "RenderableImage.h"
#include "Sprite.h"
#include "Color.h"
#include "DotGameInfo.h"
#include "CharacterFactory.h"
#include "ObjcMacros.h"

#define INDEX_EXPRESSION_SERIOS 0
#define INDEX_EXPRESSION_HAPPY 1
#define INDEX_EXPRESSION_VERY_HAPPY 2
#define INDEX_EXPRESSION_SAD 3
#define INDEX_EXPRESSION_FURIOSLY 4


//enum DollExpressions {
//	
//	DOLL_EXPRESSION_NONE = -1,
//	DOLL_EXPRESSION_SAD = 0,
//	DOLL_EXPRESSION_HAPPY,
//}typedef DollExpressions;
//
////idem...
//enum IdDoll {
//	DOLL_ID_NONE = -1,
//	DOLL_ID_ANGEL = 0,
//	DOLL_ID_DEVIL = 1,
//	DOLL_ID_FARMER,
//	DOLL_ID_ENGINNER,
//	DOLL_ID_FAIRY,
//	DOLL_ID_OGRE	
//} typedef IdDoll;


class GenericDoll{
	
public:
	
	GenericDoll( IdDoll Idboneco );
		
	IdDoll getIdDoll( void );
	 
	void getColor( Color* pColor );
	
	//virtual Texture2DHandler getFacePic( void )=0;
	
	virtual Renderable* getLineHPic();	
	
	virtual Renderable* getLineVPic();

	DollExpressions getDollExpression( void ){ return face; };
	
	void setDollExpression( DollExpressions _d ){ face = _d; };
	
	int8 getIndiceByExpression( /*DollExpressions _d*/ );
	
//	virtual bool getLineHPic(  Renderable* r ) = 0;	
//	virtual bool getLineVPic( Renderable* r ) = 0;
	
//	virtual Sprite* getZoneSprt( void ) = 0 ;
//	
//	virtual	Sprite* getBigZoneSprt( void ) = 0;
	
//	virtual RenderableImage* getZoneSprt( void ) = 0 ;
//	
//	virtual	RenderableImage* getBigZoneSprt( void ) = 0;

	virtual Renderable* getZoneSprt( void ) = 0 ;
	
	virtual	Renderable* getBigZoneSprt( void ) = 0;
	
	virtual Renderable* getFacePic( void ) = 0;
	
	static bool loadAllImages( void );
	static bool unloadAllImages( void );

	void setExpressionByPercetinl( float percent );

protected:
	
	IdDoll iddoll ;
	
	DollExpressions face;
	
	friend class LineGame;
	friend class ZoneGame;
	Color bColor;
	static Sprite *linhaPlV , *linhaPlH ;
//private:
	//virtual void buildGenericDoll( void )=0;
	
};

inline /*Color**/ void GenericDoll::getColor( Color* pColor ) { pColor = &bColor; }

inline IdDoll GenericDoll::getIdDoll( void ){ return iddoll; }

//void GenericDoll::setColor( const Color& _color ) { color = _color;  }


#endif