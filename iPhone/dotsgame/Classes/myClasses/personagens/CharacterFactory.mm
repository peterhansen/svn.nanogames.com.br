/*
 *  CharacterFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "CharacterFactory.h"
#include "GenericDoll.h"
#include "GameBaseInfo.h"
#include "Diabinho.h"
#include "Ogro.h"
#include "Anjinho.h"
#include "Executivo.h"
#include "Fadinha.h"
#include "Fazendeiro.h"
#include "BoardFactory.h"
#include "Exceptions.h"
#include "FreeKickAppDelegate.h"
#include "ObjcMacros.h"

CharacterFactory* CharacterFactory::pSingleton = NULL ;

bool CharacterFactory::Create( void ){
	if( !pSingleton ){
		pSingleton = new CharacterFactory();
#if DEBUG
		//	if( pSingleton )
		NSLog(@"CharacterFactory criado");
#endif
	}
	return pSingleton != NULL;

}

void CharacterFactory::Destroy( void ){ 
	SAFE_DELETE( pSingleton );
}

CharacterFactory::~CharacterFactory(){
	//p
#if DEBUG
	NSLog(@"destruindo CharacterFactory");	
#endif
	
}


//seria melhor se fosse dinâmico...
CharacterFactory::CharacterFactory():nCharacters( N_PERSONS ){
#if DEBUG
	NSLog(@"criando CharacterFactory");	
#endif
	
}


//void CharacterFactory::LoadCharacterIndice( GenericDoll *g , int indice ){}

GenericDoll* CharacterFactory::LoadCharacterIndice( IdDoll id ){
	GenericDoll* g;
	
	switch ( id ) {
		case DOLL_ID_ANGEL :
			g = new Anjinho();
			break;
		case DOLL_ID_DEVIL :
			g = new Diabinho();
			break;
		case DOLL_ID_FAIRY :
			g = new Fadinha();
			break;
		case DOLL_ID_OGRE :
			g = new Ogro();
			break;
		case DOLL_ID_ENGINNER :
			g = new Executivo();
			break;
		case DOLL_ID_FARMER :
			g = new Fazendeiro();
			break;
			
	}
	return g;
}

void CharacterFactory::LoadAllImages( GameBaseInfo &g ){
	GenericDoll::loadAllImages();
	bool ret = false ;
	for ( int8 x = 0 ; x < g.nPlayers ; x++) {
		switch ( g.iddolls[ x ] ) {
			case DOLL_ID_ANGEL :
				ret |= Anjinho::LoadAllImages();
				break;
			case DOLL_ID_DEVIL :
				ret |= Diabinho::LoadAllImages();
				break;
			case DOLL_ID_FAIRY :
				ret |= Fadinha::LoadAllImages();
				break;
			case DOLL_ID_OGRE :
				ret |= Ogro::LoadAllImages();
				break;
			case DOLL_ID_ENGINNER :
				ret |= Executivo::LoadAllImages();
				break;
			case DOLL_ID_FARMER :
				ret |= Fazendeiro::LoadAllImages();
				break;
		}
	}
	if( ret ){
#if DEBUG
		NSLog(@"imagens dos jogadores carregadas");
#endif	
	}else {
#if DEBUG
		NSLog(@"imagens dos jogadores não carregadas");
#endif
		[( FreeKickAppDelegate* )APP_DELEGATE quit: ERROR_ALLOCATING_DATA];

	}
	
}

void CharacterFactory::RemoveAllImages( GameBaseInfo &g ){
	GenericDoll::unloadAllImages();
	for ( int8 x = 0 ; x < g.nPlayers ; x++) {
		switch ( g.iddolls[ x ] ) {
			case DOLL_ID_ANGEL :
				Anjinho::RemoveAllImages();
				break;
			case DOLL_ID_DEVIL :
				Diabinho::RemoveAllImages();
				break;
			case DOLL_ID_FAIRY :
				Fadinha::RemoveAllImages();
				break;
			case DOLL_ID_OGRE :
				Ogro::RemoveAllImages();
				break;
			case DOLL_ID_ENGINNER :
				Executivo::RemoveAllImages();
				break;
			case DOLL_ID_FARMER :
				Fazendeiro::RemoveAllImages();
				break;
		}
	}
}


//retorna 255 qdo hover erro
uint8 CharacterFactory::setFrameByCharacter( IdDoll _id ){
//uint8 r
	switch ( _id ) {
		case DOLL_ID_ANGEL :
			return INDEX_MARK_ANGEL;
			break;
		case DOLL_ID_DEVIL :
			return INDEX_MARK_DEVIL;
			break;
		case DOLL_ID_FAIRY :
			return INDEX_MARK_FAIRY;
			break;
		case DOLL_ID_OGRE :
			return INDEX_MARK_OGRE;
			break;
		case DOLL_ID_ENGINNER :
			return INDEX_MARK_ENGINNER;
			break;
		case DOLL_ID_FARMER :
			return INDEX_MARK_FARMER;
			break;
	}
	return 255;
}

UIImage* CharacterFactory::getPicturePersonagen( uint8 index ){
	return getPicturePersonagen( getIdDollByIndice( index ) );
}


//eu sei, um horror...
IdDoll	CharacterFactory::getIdDollByIndice( uint8 index ){
	IdDoll aux;
	if( index == 0 )
		aux = DOLL_ID_ANGEL;
	else if( index == 1 ) {
		aux = DOLL_ID_DEVIL;
	}else if( index == 2 ) {
		aux = DOLL_ID_FARMER;
	}else if( index == 3 ) {
		aux = DOLL_ID_ENGINNER;
	}else if( index == 4 ) {
		aux = DOLL_ID_FAIRY;
	}else if( index == 5 ) {
		aux = DOLL_ID_OGRE;
	}else {
		aux = DOLL_ID_NONE;
	}
	return aux;
}

CharacterFactory* CharacterFactory::GetInstance( void ) { 
	Create();
return pSingleton; }

uint8 CharacterFactory::getNCharacters(){
return nCharacters; }


UIImage* CharacterFactory::getPicturePersonagen( IdDoll index ){
	UIImage* ret = NULL;
	char buffer[ PATH_MAX ];
	
	switch ( index ) {
		case DOLL_ID_ANGEL :
			ret = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "SMangel", "png" ))];

			break;
		case DOLL_ID_DEVIL :
			ret = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "SMdevil", "png" ))];
			break;
		case DOLL_ID_FAIRY :
			ret = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "SMenginner", "png" ))];
			break;
		case DOLL_ID_OGRE :
		ret = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "SMfairy", "png" ))];
			break;
		case DOLL_ID_ENGINNER :
			ret = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "SMfarmer", "png" ))];
			break;
		case DOLL_ID_FARMER :
			ret = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "SMogro", "png" ))];
			
			break;
	}
	
	if(!ret){
#if DEBUG
		NSLog(@"falha na imagem=>bonecos");
#endif
	}
	return ret;
}


void CharacterFactory::getColorByCharacter( IdDoll _index , Color *pColor){
	switch ( _index ) {
		case DOLL_ID_ANGEL :
			*pColor = COLOR_BLUE;
			break;
		case DOLL_ID_DEVIL :
			*pColor = COLOR_RED;
			break;
		case DOLL_ID_FAIRY :
			*pColor = COLOR_PURPLE;
			break;
		case DOLL_ID_OGRE :
			*pColor = COLOR_GREEN;
			break;
		case DOLL_ID_ENGINNER :
			*pColor = COLOR_GREY;
			break;
		case DOLL_ID_FARMER :
			*pColor = COLOR_BROWN;
			break;
	}

}


UIImage* CharacterFactory::getWinCharImages( IdDoll _id ){
	char buffer[ PATH_MAX ];
		UIImage* character = NULL;
	switch ( _id ) {
			
		case DOLL_ID_ANGEL :{
			character = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "dufw", "png" ))];
		}
			break;
			
		case DOLL_ID_DEVIL :{
			character = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "BORR", "png" ))];
		}
			
			break;
			
		case DOLL_ID_FAIRY :{
			character = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "violet", "png" ))];
		}
			break;
			
		case DOLL_ID_OGRE :{
			character = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "duh", "png" ))];
		}	
			break;
			
		case DOLL_ID_ENGINNER :{
			character = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "gomez", "png" ))];
		}
			break;
			
		case DOLL_ID_FARMER :{
			character = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "mary", "png" ))];
		}
			break;
	}
	if(!character){
#if DEBUG
		NSLog(@"falha na imagem=>bonecos");
#endif
	}
		
	return character;
}

UIImage* CharacterFactory::getWinBgImages( IdDoll _id ){
	char buffer[ PATH_MAX ];
	UIImage* bg = NULL;

	
	switch ( _id ) {
			
		case DOLL_ID_ANGEL :{
			bg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bgwinang", "png" ))];
			}
			break;
			
		case DOLL_ID_DEVIL :{
			bg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bgwindev", "png" ))];
			}
			
			break;
			
		case DOLL_ID_FAIRY :{
			bg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bgwinfada", "png" ))];
			}
			break;
			
		case DOLL_ID_OGRE :{
			bg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bgwinogro", "png" ))];
			}	
			break;
			
		case DOLL_ID_ENGINNER :{
			bg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bgwineng", "png" ))];
			}
			break;
			
		case DOLL_ID_FARMER :{
			bg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "bgwinfarmer", "png" ))];
		}
			break;
	}
	if(!bg){
#if DEBUG
		NSLog(@"falha na imagem=>bonecos");
#endif
	}
	
	return bg;
}
