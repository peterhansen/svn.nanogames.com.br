/*
 *  SpecialBar.mm
 *  dotGame
 *
 *  Created by Max on 3/24/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "SpecialBar.h"
#include "Color.h"

#define MAX_OBJECTS_INSIDE 2
#define INDEX_BGD_BAR 0
#define INDEX_BAR 1

SpecialBar::SpecialBar():ObjectGroup( MAX_OBJECTS_INSIDE ),colorBar( COLOR_WHITE ), colorBgd( COLOR_WHITE ), percentVisible( 0.0f ) {

	buildSpecialBar();
}

void SpecialBar::setSpecialBarState( SpecialBarState s ){
	state = s;
	updateColor();
}

void SpecialBar::buildSpecialBar( void ){
	RenderableImage *bgd;
	RenderableImage *bar;
	bgd = new RenderableImage("bPower");	
	bar = new RenderableImage("bPM");
	
	bgd->setVertexSetColor( colorBgd );
	insertObject( bgd );

	bar->setVertexSetColor( colorBar );
	bar->setSize( bgd->getSize()->x * 0.7 , bgd->getSize()->y * 0.7 );
	bar->setPosition( bgd->getPosition()->x * 0.2 , bgd->getPosition()->y * 0.2 );	
	Viewport v = bar->getViewport();
	v.width *= percentVisible; 
	bar->setViewport( v );	
	insertObject( bar );

}

void SpecialBar::setColorBar( Color c ){
	colorBar = c;
	static_cast< RenderableImage* > ( getObject( INDEX_BAR ) )->setVertexSetColor( c );
//	bar->setVertexSetColor( c );
}

bool SpecialBar::render( void ){
	
return	ObjectGroup::render();
//	bool ret;
//	ret |= bgd.render();
//	ret |= bar.render();
//	return ret;
}

void SpecialBar::updateColor( void ){
	Color c;
	switch ( state ) {
		case SPECIAL_BAR_STATE_LOW:
			c = COLOR_BLUE;
			break;
			
		case SPECIAL_BAR_STATE_MEDIUM:
			c = COLOR_YELLOW;			
			break;
			
		case SPECIAL_BAR_STATE_FULL:
			c = COLOR_RED;
			break;
	}
	static_cast< RenderableImage* > ( getObject( INDEX_BAR ) )->setVertexSetColor( c );

	//bar.setVertexSetColor( c );
}

void SpecialBar::setColorBarBgd( Color c ){
	colorBgd = c;
	static_cast< RenderableImage* > ( getObject( INDEX_BGD_BAR ) )->setVertexSetColor( c );

//	bgd.setVertexSetColor( c );
}

void SpecialBar::updateBar( float f ){

	percentVisible += f;
	if( percentVisible >= 1.0f /*&& state != SPECIAL_BAR_STATE_FULL*/ ){
		switch ( state ) {
			case SPECIAL_BAR_STATE_LOW:
				setSpecialBarState( SPECIAL_BAR_STATE_MEDIUM );
				break;
			case SPECIAL_BAR_STATE_MEDIUM:
				setSpecialBarState( SPECIAL_BAR_STATE_FULL );
				break;
				//	case SPECIAL_BAR_STATE_FULL:
				//		setSpecialBarState( SPECIAL_BAR_STATE_MEDIUM );
				//		break;
		}
		percentVisible -= 1.0f;
	}
	if( state == SPECIAL_BAR_STATE_FULL )
		return;
	Viewport v = getObject( INDEX_BAR )->getViewport();
	v.width = percentVisible * getObject( INDEX_BAR )->getSize()->x; 
	getObject( INDEX_BAR )->setViewport( v );
	
}
SpecialBar::~SpecialBar(){}
