/*
 *  MMLCollection.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

#include <string> 
#include <iostream>
#include <fstream>
#include <sstream> 
#include <vector>
#include <map>


#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/**************************************************************************************************
* Constructor 
***************************************************************************************************/

CMMLCategory::CMMLCategory():myId(-1)
{

}

/**************************************************************************************************
 * Destructor 
 ***************************************************************************************************/

CMMLCategory::~CMMLCategory()
{
  for (int c=0;c<getTotalItems();c++)
    delete entities[c];
}


/**************************************************************************************************
 * getter for the category of the collection
 ***************************************************************************************************/

const std::string CMMLCategory::getCategory()
{
	return category;
}

/**************************************************************************************************
 * get entity by its ID;
 ***************************************************************************************************/

CMMLEntity* CMMLCategory::getEntityById(CMMLEntity::EntityIdType id)
{
	if (id < 0 || id > getTotalItems())
		throw new MMLInvalidEntityId();
		
	return entities[id];
}

/**************************************************************************************************
 * set the collection's category
 ***************************************************************************************************/
void CMMLCategory::setCategory(const std::string &Category)
{
	category=Category;
	////HACK/////
	////TODOO: change this mess
	std::string cmd="mkdir -p ";
	cmd+=CMMLDatabase::getInstance()->getBasePath();
	cmd+="/";
	cmd+=category;
#ifdef DEBUG
	std::cerr << "creating category with:"<<cmd<<std::endl;
#endif
	system(cmd.c_str());
}


/**************************************************************************************************
 * Add an entity to this category
 ***************************************************************************************************/
EntityIdType CMMLCategory::addEntity( CMMLEntity* entity)
{
	if (entity==NULL)
		throw new MMLNonExistantEntity();
	
	if (entity->getId()!= -1)	
		throw new MMLEntityAlreadyCategorized();
	
  if (entity->getTotalFields()==1)
	{
	bool duplicate=false;
	try 
		{
		  CMMLEntity::MMLFieldConstIterator it = entity->getConstIterator();
		  std::string key=it->first;
		  std::string value=it->second;
		  getEntityByKeyValue(key,value);
  		  duplicate=true;
		}
	catch (MMLNonExistantEntity *e)
		{
 		  duplicate=false;
		}
  	if (duplicate)
  		throw new MMLDuplicateTrivialEntity();
	}
  entity->setCategory(myId);
  entity->setId(getTotalItems());
  entities.push_back(entity);
  return entity->getId();
}


/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/
int CMMLCategory::getTotalItems()
{
  return entities.size();
}

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/
void CMMLCategory::internalize()
{
  struct dirent *de=NULL;
  DIR *d=NULL;
  std::string fullpath="";
  std::string filename="";
  CMMLEntity::MMLEntityReference entity;
  std::ifstream *stream;

  fullpath+=CMMLDatabase::getInstance()->getBasePath();
  fullpath+="/";
  fullpath+=getCategory();


  d=opendir(fullpath.c_str());
  if(d == NULL)
    perror("Couldn't open directory");

  // Loop while not NULL
  de = readdir(d);
  while(de = readdir(d))
    {
      if (!strcmp(de->d_name,".") || !strcmp(de->d_name,".."))
	continue;
      entity=new CMMLEntity();
      filename=fullpath;
      filename+="/";
      filename+=de->d_name;

#ifdef DEBUG
      std::cout << "internalizing "<<filename << std::endl;
#endif      
      stream=new std::ifstream(filename.c_str());
      entity->initFromStream(*stream);
      addEntity(entity);
      delete stream;
    }
  closedir(d);
}



/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/
const EntityIdType CMMLCategory::getEntityIdByKeyValue(const std::string key,const std::string value)
{
  int totalitems=getTotalItems();
  for (int c=0;c<totalitems;c++)
    {
      try
      	{
		if (((CMMLEntity*)entities[c])->getValueByKey(key)==value)
		  return c;
	    }
    
    	catch (MMLNonExistantFieldQueried exception)
		{
			//nada
		}
    }
  throw  new MMLNonExistantEntity();
}


/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/
CMMLEntity* CMMLCategory::getEntityByKeyValue(const std::string key,const std::string value)
{
  return getEntityById(getEntityIdByKeyValue(key,value));
}

/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/

CMMLCategory::CategoryIdType CMMLCategory::getId()
{
  return myId;
}

/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/

void CMMLCategory::setId(CategoryIdType id)
{
  myId=id;
}


/**************************************************************************************************
 * 
 ***************************************************************************************************/

void CMMLCategory::commit()
{
  std::string fullpath="";
  std::string filename="";
  CMMLEntity::MMLEntityReference entity;
  CMMLEntity::MMLFieldConstIterator it;
  std::ofstream *stream;

  fullpath+=CMMLDatabase::getInstance()->getBasePath();
  fullpath+="/";
  fullpath+=getCategory();

  for (int c=0;c<getTotalItems();++c)
    {
      entity=getEntityById(c);

      filename=fullpath;
      filename+="/";
      ///eh mais seguro nao confiar que o Id interno ainda corresponda ao Id dentro da colecao.
      filename+=ItoA(entity->getId());
      std::cout << "commiting "<<filename << std::endl;
      stream=new std::ofstream(filename.c_str());
      
      it = entity->getConstIterator();
      while (it!=entity->getConstEndIterator())
      {      
		*stream << it->first << "=" << it->second<<";"<<std::endl;
		it++;
      }
      *stream<<"~";
      stream->close();
      delete stream;      
    }
  
  
}
