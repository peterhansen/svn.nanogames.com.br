/*
 *  Constants.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef CONSTANTS_H
#define CONSTANTS_H

///general parsing constants
#define ENTITY_ATTRIB_MARKER '='
#define ENTITY_EOL_MARKER ';'
#define ENTITY_EOF_MARKER '~'

#endif