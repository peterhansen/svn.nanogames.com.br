/*
 *  MMLEntity.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef MMLENTITY_H
#define MMLENTITY_H


class CMMLEntity
{
private:
	typedef std::map<std::string,std::string> Fields;
public:
	///typedefs:	
	typedef int EntityIdType;	
	typedef CMMLEntity* MMLEntityReference;
	typedef Fields::const_iterator MMLFieldConstIterator;
	///object lifecycle:
	CMMLEntity();
	~CMMLEntity();
	///get methods:
	int getTotalFields();
	const std::string getValueByKey(const std::string &key);
	EntityIdType getId();
	bool isFieldValid(const std::string &fieldindex);
	CMMLCategory::CategoryIdType getCategory();
	/// set methods:
	void setCategory(CMMLCategory::CategoryIdType id);
	void setValueForKey(const std::string &key,const std::string &value);
	void setId(EntityIdType id);
	///misc methods:
	std::string dump();
	void initFromStream(std::istream &i_stream);	
	MMLFieldConstIterator getConstIterator();
	MMLFieldConstIterator getConstEndIterator();	
	bool equal(MMLEntityReference other);
private:
	///fields:
	EntityIdType Id;
	CMMLCategory::CategoryIdType CategoryId;
	Fields fields;	
};
#endif

