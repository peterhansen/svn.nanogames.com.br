#ifndef MMUTILS_H
#define MMUTILS_H
//----------------------------------------------------------------------------------------
typedef int EntityIdType;
typedef int CategoryIdType;
//----------------------------------------------------------------------------------------
/*============================================================================================== 
* AtoI
* @author Daniel Monteiro
* @brief converte de string C para inteiro (a versao padrao e' muito chata de usar)
* @param const char *input a string para ser convertida
* @returns int correspondente 
 ================================================================================================*/

static int AtoI(const char *input)
{
	int temp;
	std::stringstream ss (std::stringstream::in | std::stringstream::out);
	ss << input;
	ss >> temp;
	return temp;
}
/*==============================================================================================
* ItoA
* @author Daniel Monteiro
* @brief converte de inteiro para String C++
* @param int o numero a ser convertido
* @returns std::string correspondente
 ================================================================================================*/

static std::string ItoA(int num)
{
	std::string temp;
	std::stringstream ss (std::stringstream::in | std::stringstream::out);
	ss << num;
	ss >> temp;
	return temp;
}
//----------------------------------------------------------------------------------------
class MMLBaseException: public std::exception
{
private:
	
	std::string reason;
	std::string otherComments;
public: 
	
	MMLBaseException(std::string Reason,std::string OtherComments="")  : std::exception()
	{		
		reason=Reason;
		otherComments=OtherComments;
	}	
	
	virtual ~MMLBaseException() throw() 
	{
	}
	
	virtual const char *what() 
	{ 
		std::string toReturn;
		toReturn+="<MMLBaseException reason=\'";
		toReturn+=reason;
		if (otherComments=="")
			toReturn+="\'/>";
		else
		{
			toReturn+="\'>\n";
			toReturn+=otherComments;
			toReturn+="\n</MMLBaseException>";
		}
		return toReturn.c_str();
	}
};
//----------------------------------------------------------------------------------------
class MMLNonExistantFieldQueried : public MMLBaseException
	{
	public:
		
		MMLNonExistantFieldQueried() : MMLBaseException("campo nao existente consultado")
		{		
		}
	};
//----------------------------------------------------------------------------------------
class MMLBadStreamForInput : public MMLBaseException
	{
	public:
		MMLBadStreamForInput() : MMLBaseException("stream problematico usado para entrada")
		{		
		}
		
	};
//----------------------------------------------------------------------------------------
class MMLInvalidAttrib : public MMLBaseException
	{
	public:
		MMLInvalidAttrib() : MMLBaseException("atributo invalido")
		{		
		}
	};
//----------------------------------------------------------------------------------------
class MMLEmptyKeyName : public MMLBaseException
	{
	public:
		MMLEmptyKeyName() : MMLBaseException("nome de chave vazia")
		{		
		}
	};
//----------------------------------------------------------------------------------------
class MMLInvalidEntityId : public MMLBaseException
	{
	public:
		MMLInvalidEntityId() : MMLBaseException("id de entidade invalido")
		{
		}
	};
//----------------------------------------------------------------------------------------
class MMLNonExistantCategory : public MMLBaseException
	{
	public:
		MMLNonExistantCategory() : MMLBaseException("categoria nao existente") 
		{
		}
	};
//----------------------------------------------------------------------------------------
class MMLNonExistantEntity : public MMLBaseException
	{
	public:
		MMLNonExistantEntity() : MMLBaseException("entidade nao existente consultada")
		{
		}
	};
//----------------------------------------------------------------------------------------
class MMLDuplicateTrivialEntity : public MMLBaseException
	{
	public:
		MMLDuplicateTrivialEntity() : MMLBaseException("insercao de entidade com chave primaria duplicada")
		{
		}	
	};
//----------------------------------------------------------------------------------------
class MMLEntityAlreadyCategorized : public MMLBaseException
	{
	public:
		MMLEntityAlreadyCategorized() : MMLBaseException("entidade ja categorizada")
		{
		}	
	};
//----------------------------------------------------------------------------------------
class MMLCategoryAlreadyManaged : public MMLBaseException
	{
	public:
		MMLCategoryAlreadyManaged() : MMLBaseException("categoria ja gerenciada")
		{
		}	
	};
//----------------------------------------------------------------------------------------

#endif
