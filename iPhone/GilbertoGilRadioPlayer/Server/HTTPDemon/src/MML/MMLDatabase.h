/*
 *  MMLDatabase.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef MMLDATABASE_H
#define MMLDATABASE_H


class CMMLDatabase
	{
	public:
		///typedef:
		typedef CMMLDatabase* MMLDatabaseReference;
		///object lifecycle:
		CMMLDatabase();		
		~CMMLDatabase();
		///object singleton:
		static  MMLDatabaseReference getInstance();
		///set methods:
		void setBasePath(const std::string &basepath);
		void setCategoryById(CategoryIdType id, CMMLCategory::MMLCategoryReference Category);
		///get methods:
		const std::string getBasePath();
		CMMLCategory::MMLCategoryReference getCategoryById(CategoryIdType id);
		CategoryIdType getCategoryIdByName(std::string name);
		CMMLCategory::MMLCategoryReference getCategoryByName(std::string name);
		int getTotalItems();
		///misc methods:
		void addCategory(CMMLCategory::MMLCategoryReference ref);
		void cleanUp();
	private:

		///fields:
		static MMLDatabaseReference instance;
		std::string basePath;
		std::vector<CMMLCategory::MMLCategoryReference> category;
	};
#endif