
///C & Unix
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <sys/wait.h> 
#include <signal.h> 

/// C++
#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>


/// SockLib
#include "tcpsock.h"

/// MML
#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/// HTTPDemon
#include "httpspawn.h"
#include "httpdemon.h"

/// AudioServer
#include "audioserver.h"
#include "audiorelay.h"


void sigchld_handler(int s) 
{ 
    while(waitpid(-1, NULL, WNOHANG) > 0); 
} 

void quitHandler(int sig) {
	exit(0);
}


/************************************************************************ 
 * showCopyright - mostra a copyright em console
 * @author Daniel Monteiro
 * @see showUsage
 *************************************************************************/
void showCopyright()
{
  std::cout << "nano audio relay v 0.1" << std::endl;
  std::cout << "copyright 2010 Nano Games ltda. All rights reserved. http://www.nanogames.com.br" << std::endl;
}

/************************************************************************ 
 * showUsage - mostra os possiveis uso da aplicacao
 * @author Daniel Monteiro
 * @see showUsage
 *************************************************************************/
void showUsage()
{
  using namespace std;
  showCopyright();
  cout << "usage:" << endl;
  cout << "server [dispatcher_first_port=PORT] [dispatcher_last_port=PORT] [audio_first_port=PORT] [audio_last_port=PORT] :" << endl;
  cout << "   controls the port usage on the server system." << endl;
  cout << "server [bitrate=BPS]" << endl;
  cout << "   sets the current bitrate to use - this typically implies in more CPU usage, but uses less bandwidth." << endl;
  cout << "server [maxclients=NUM]" << endl;
  cout << "   sets maximum number of allowed clients connected to audio servers. There is no maximum number of dispatchers" << endl;
  cout << "   but if the system hits the maximum number of audio clients, new clients will be put on hold and be allowed to drop." << endl;
}

/************************************************************************ 
 * main - ponto de entrada do sistema
 * @return        inteiro - codigo de erro UNIX
 * aceita       : 
 * @param argc    numero de parametros (incluindo a primeira entrada, que contem o arquivo binario)
 * @param argv    vetor de strings (sendo que argv[0] contem o nome do binario).
 * @author Daniel Monteiro
 *************************************************************************/

int main (int argc, char *argv[])
{
  
  signal(SIGINT,quitHandler);
  struct sigaction sa; 
  sa.sa_handler = sigchld_handler; // reap all dead processes 
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = SA_RESTART; 
  if (sigaction(SIGCHLD, &sa, NULL) == -1) 
    { 
      perror("sigaction"); 
      exit(1); 
    } 
  
  if (argc>1)
    {
      HTTPDemon httpd(argv[1]);
      try
	{
	  httpd.start();	
	}
      catch (SockLibException &e)
	{
	  std::cout << e.what() << std::endl;
	}
    }
  return 0;
}
