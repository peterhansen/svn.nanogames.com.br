/*
 * @author Daniel Monteiro
 * @brief atende a uma requisicao de audio.
 * @see HTTPSpawn
*/

#ifndef AUDIOSERVER_H
#define AUDIOSERVER_H 1

class AudioServer: public HTTPSpawn
{
 public:
  ///definicoes
  typedef FILE* pipe_t;
  
  ///funcoes gerais
  pipe_t stream(const std::string &Filename, const std::string &Port, const std::string &TelnetPort);
  virtual void commit();
  virtual void takeRequest(const std::string &Msg);
  virtual ~AudioServer();
  AudioServer(const TCPSock* peer);
 private:
  pipe_t audioThreadPipe;  
};

const int BLANKPIPE=NULL;

#endif
