
/// C++
#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>


/// SockLib
#include "tcpsock.h"

/// MML
#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/// HTTPDemon
#include "httpspawn.h"
#include "httpdemon.h"

/// AudioServer
#include "audioserver.h"


void AudioServer::takeRequest(const std::string &Msg)
{

  ///pega o cookie
  //HTTPSpawn::takeRequest(Msg);

  int connectors=0;
  int baseport=0;
  int telnetport=0;
  int servletport=0;
  int limit=0;
  //audioThreadPipe=-1;

  CMMLEntity::MMLEntityReference genconf;
//  CMMLEntity::MMLEntityReference session;
  CMMLEntity::MMLEntityReference stats;
  CMMLDatabase::MMLDatabaseReference db=CMMLDatabase::getInstance();
  CMMLCategory::MMLCategoryReference sessions=db->getCategoryByName("sessions");
  CMMLCategory::MMLCategoryReference confs=db->getCategoryByName("configs");

  genconf=confs->getEntityByKeyValue("section-name","HTTPDemon-general-configurations");
  stats=confs->getEntityByKeyValue("section-name","HTTPDemon-general-stats");

  std::string tmp;
  std::string File;
  std::string Url;
  
  if (Msg.find_first_of('/')==std::string::npos)
    {
      setBody(getErrorPage(HTTP_STATUS_NOT_FOUND));
      return;
    }	  
  
  File=Msg.substr(Msg.find_first_of('/')+1);
  File=File.substr(0,File.find_first_of(' '));
      

  ///options eh enviado caso eu use XMLHttpRequest do Javascript.
  if (Msg.substr(0,Msg.find_first_of(' '))=="STREAM" || Msg.substr(0,Msg.find_first_of(' '))=="OPTIONS")
    {

      Url=genconf->getValueByKey("audioserver-base-url");
      connectors=sessions->getTotalItems();
      baseport=AtoI(genconf->getValueByKey("base-port").c_str());
      limit=AtoI(genconf->getValueByKey("client-limit").c_str());
      telnetport=AtoI(genconf->getValueByKey("telnet-port").c_str());
      servletport=AtoI(genconf->getValueByKey("servlet-port").c_str());
      connectors=connectors % limit;
      audioThreadPipe=stream(File.c_str(),ItoA(baseport+connectors),ItoA(telnetport+connectors)); 

      {
	//	std::cout << "--------------------got STREAM---------------"<<std::endl;
	std::string toReturn="";
	std::string tmp="";
	toReturn+="<?xml version='1.0' encoding='UTF-8'?>\n";
	toReturn+="<stream-info>\n";
	toReturn+="<ports>\n";
	toReturn+="<port name='audio'>\n";
	tmp=ItoA(baseport+connectors);
	toReturn+=Url+":"+tmp+"\n";   
	getMMLEntity()->setValueForKey("audio-port",tmp);
	toReturn+="</port>\n";
	toReturn+="<port name='telnet'>\n";
	tmp=ItoA(telnetport+connectors);
	toReturn+=Url+":"+tmp+"\n"; 
	getMMLEntity()->setValueForKey("telnet-port",tmp);
	toReturn+="</port>\n";
	toReturn+="<port name='control'>\n";
	tmp=ItoA(servletport+connectors);
	toReturn+=Url+":"+tmp+"\n";   
	getMMLEntity()->setValueForKey("servlet-port",tmp);
	toReturn+="</port>\n";
	toReturn+="</ports>\n";
	toReturn+="<session-id>\n";
	toReturn+=getMMLEntity()->getValueByKey("session-id");
	toReturn+="\n</session-id>\n</stream-info>";
	setBody(toReturn);
	//std::cout << "***********************"<<std::endl<<"body:" << std::endl<<toReturn << std::endl;
	getMMLEntity()->setValueForKey("state","opening");
	getMMLEntity()->setValueForKey("clientset","no");
      }
    }
  
  else
    {
      //      std::cout << "--------------------- got KILL-STREAM --------------------" << std::endl;
      getMMLEntity()->setValueForKey("session-id","-1"); //marca como invalida  TODOO: achar solucao melhor (nao posso apagar, pois se a sessao nao for encontrada, a colecao verificara  uma entidade invalida)      
      setMMLEntity(sessions->getEntityByKeyValue("session-id",File));
      getMMLEntity()->setValueForKey("state","closing");
      getMMLEntity()->setValueForKey("clientset","yes");
      //      std::cout << "cookie:" << File << std::endl;
    }
  
}



void AudioServer::commit()
{
  HTTPSpawn::commit();

  if (getMMLEntity()->getValueByKey("state")=="closing")
	if (!fork())
	{
      std::string cmd;
      std::string tmp;
      cmd="./dropvlc.sh ";
      cmd+=getMMLEntity()->getValueByKey("telnet-port");
      system(cmd.c_str());
      std::cout << "saindo: " <<cmd << std::endl;
      getMMLEntity()->setValueForKey("state","closed");
      getMMLEntity()->setValueForKey("clientset","true");
      setBody("kthxbye!\r\n\r\n\r\n");	
    	exit(0);
	}


}

AudioServer::AudioServer(const TCPSock *peer):HTTPSpawn(peer),  audioThreadPipe(BLANKPIPE)
{
  std::cout << getMMLEntity()->dump() << std::endl; 
}


AudioServer::~AudioServer()
{
  /*
    ????
  if (audioThreadPipe!=BLANKPIPE)
    close (audioThreadPipe);
  */
}



AudioServer::pipe_t AudioServer::stream(const std::string &filename, const std::string &AudioPort, const std::string &TelnetPort)
{

  CMMLEntity::MMLEntityReference genconf;
//  CMMLEntity::MMLEntityReference session;
  CMMLEntity::MMLEntityReference stats;
  std::string cmd="";
  std::string marker="";
  CMMLDatabase::MMLDatabaseReference db=CMMLDatabase::getInstance();
//  CMMLCategory::MMLCategoryReference sessions=db->getCategoryByName("sessions");
  CMMLCategory::MMLCategoryReference confs=db->getCategoryByName("configs");

  genconf=confs->getEntityByKeyValue("section-name","HTTPDemon-general-configurations");
  stats=confs->getEntityByKeyValue("section-name","HTTPDemon-general-stats");
  

//  CMMLEntity::MMLEntityReference entity=getMMLEntity();   

  cmd+=genconf->getValueByKey("audioserver-stream-app-name");
  cmd+=genconf->getValueByKey("audioserver-stream-options");
  {

    
    marker=genconf->getValueByKey("audioserver-filename-marker");
    if (cmd.find(marker)!=std::string::npos)
      cmd=cmd.replace(cmd.find(marker),marker.length(),genconf->getValueByKey("audioserver-audiofiles-url")+filename);

    marker=genconf->getValueByKey("audioserver-telnetport-marker");
    if (cmd.find(marker)!=std::string::npos)
      cmd=cmd.replace(cmd.find(marker),marker.length(),TelnetPort);

    marker=genconf->getValueByKey("audioserver-streamport-marker");
    if (cmd.find(marker)!=std::string::npos)
      cmd=cmd.replace(cmd.find(marker),marker.length(),AudioPort);
  }
  std::cout << "executando:"<< cmd << std::endl;
  return popen(cmd.c_str(),"w");
}
