
/// C++
#include <string> 
#include <iostream>
#include <sstream>
#include <fstream> 
#include <vector>
#include <map>


/// SockLib
#include "tcpsock.h"

/// MML
#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/// HTTPDemon
#include "httpspawn.h"


const std::string HTTPSpawn::getStatusPage()
{
  std::string toReturn="";
  CMMLDatabase::MMLDatabaseReference db=CMMLDatabase::getInstance();
  CMMLCategory::MMLCategoryReference confs=db->getCategoryByName("configs");
//  CMMLCategory::MMLCategoryReference sessions=db->getCategoryByName("sessions");
  CMMLEntity::MMLEntityReference stats=confs->getEntityByKeyValue("section-name","HTTPDemon-general-stats");
  

  toReturn+="<html>\n";
  toReturn+="<body bgcolor=\'#000000\'><font color=\'#00FF00\'> \n";
  toReturn+=stats->dump();
  /*
  for (int d=0;d<entity->getTotalFields();++d)	
    toReturn+= stats->getKeyNameByIndex(d) + "=" + stats->getValueByIndex(d)+";<br/>";
    */
  toReturn+="<br/>server status page<br/>\n</font>\n</body>\n</html>";

  return toReturn.c_str();
  
}



/************************************************************************ 
 * construtor - trata uma requisicao HTTP
 * @author Daniel Monteiro
 * @see ~HTTPDemon
 *************************************************************************/
HTTPSpawn::HTTPSpawn(const TCPSock* Peer):peer((TCPSock*)Peer),status(HTTP_STATUS_OK)
{
  entity=new CMMLEntity();
  entity->setValueForKey("session-id","-1");
  entity->setValueForKey("clientset","no");
  entity->setValueForKey("primary-cookie","session-id");
}

/************************************************************************ 
 * destrutor - limpa a sujeira deixada pelo tratador
 * @author Daniel Monteiro
 * @see ~HTTPDemon
 *************************************************************************/
HTTPSpawn::~HTTPSpawn()
{
  peer->disconnect();
  delete peer;
   //nao posso matar a MMLEntity, porque ela agora pode pertencer a alguma colecao...
  ///TODOO: adicionar contador de referencias no MMLEntity? ou o CategoryId resolve?

}


/************************************************************************ 
 * setBody - define o corpo da resposta a requisicao
 * @author Daniel Monteiro
 * @see takeRequest
 *************************************************************************/
void HTTPSpawn::setBody(const std::string &Body)
{
  body=Body;
}


/************************************************************************ 
 * setStatus - define o status do servidor HTTP em relacao a requisicao
 * @author Daniel Monteiro
 * @see takeRequest
 *************************************************************************/
void HTTPSpawn::setStatus(HTTPStatus Status)
{
  status=Status;
}


/************************************************************************ 
 * getBody - obtem o corpo da resposta a requisicao
 * @author Daniel Monteiro
 * @see takeRequest
 *************************************************************************/

const std::string HTTPSpawn::getBody()
{
  return body.c_str();
}

/************************************************************************ 
 * getStatus - obtem o status do servidor em relacao a requisicao
 * @author Daniel Monteiro
 * @see takeRequest
 *************************************************************************/

HTTPSpawn::HTTPStatus HTTPSpawn::getStatus()
{
  return status;
}


/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @throw TCP_SOCKET_NOT_CONNECTED se nao estiver conectado
 * @see setBody setStatus
 *************************************************************************/

void HTTPSpawn::takeRequest()
{  
  if (peer!=NULL && peer->getState()==TCPSock::CONNECTED)
    takeRequest(peer->receive(true));
  else
    throw TCP_SOCKET_NOT_CONNECTED;
}


/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

void HTTPSpawn::takeRequest(const std::string &Msg)
{  
  std::string toBody="";
  //  std::cout << "request:" << std::endl;
  //  std::cout << Msg << std::endl;  
  
  ///pega apenas o primeiro par chave=valor;
  {
    std::string CookieStr=std::string("Cookie");
    size_t pos=Msg.find(CookieStr);
    if (pos!=std::string::npos)
      {
	std::string parcial=Msg.substr(pos); //corta tudo ate a linha de Cookie
	parcial=parcial.substr(0,parcial.find_first_of('\n')); // fica somente com a  linha desejada
	parcial=parcial.substr(parcial.find_first_of(':')+1); //elimina "Cookie:"
	parcial=parcial.substr(0,parcial.find_first_of(';')); //fica apenas com a primeira definicao"
	parcial=parcial.substr(parcial.find_last_of(' ')+1); //corta os espacos antes do cookie
	entity->setValueForKey(parcial.substr(0,parcial.find_last_of('=')),parcial.substr(parcial.find_last_of('=')+1));
	entity->setValueForKey("clientset","yes");
	//	std::cout << "---------------->>>> COOKIE!!!! " << parcial.substr(0,parcial.find_last_of('=')) <<"="<<parcial.substr(parcial.find_last_of('=')+1) << std::endl;


      }
  }



  int cmdId=getValueForCommand(Msg.substr(0,Msg.find_first_of(' ')));

    switch (cmdId)
      {
      case HTTP_CMD_GET:
      {
	std::string file;
	file=Msg.substr(5);
	file=file.substr(0,file.find_first_of(' '));
	if (file=="")
	  file="./index.html";
	else 
	  file="./"+file;
	
	///<temp>
	if (file=="./server-status.html")
	  {
	    setBody(getStatusPage());
	    return;
	  }
	///</temp>
	//	std::cout << "file:"<<file<<std::endl;
	std::ifstream filestream(file.c_str());
	if (!filestream.good())
	  {
	    setBody(getErrorPage(HTTP_STATUS_NOT_FOUND));
	    return;
	    }	  
	
	while (filestream.good())
	  toBody+=filestream.get();      
	
	///nao sei porque, mas preciso retirar o ultimo byte. EOF?
	toBody=toBody.substr(0,toBody.length()-1);
	
	setBody(toBody);
      }      
      break;
      
      case HTTP_CMD_POST:
	{
	}
	break;
	
      case HTTP_CMD_HEAD:
	{
	  
	}
	break;
	
      case HTTP_CMD_NOT_SUPPORTED:
	{
	  setBody(getErrorPage(HTTP_STATUS_NOT_SUPPORTED));
	}
	break;      
      }
    //////
}

/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

void HTTPSpawn::commit()
{ 
	std::cout << "commiting..." << std::endl;
  std::string toSend;
  ////////
  toSend+="HTTP/1.0 ";
  toSend+=getStringForStatus(getStatus());
  toSend+="\n";
  toSend+="Connection: close\n";
  toSend+="Content-Length: "+ItoA(getBody().length() *sizeof(char))+"\n";
  toSend+="Server: Nano HTTP Server (CentOS5)\n";
  
  if (getMMLEntity()->getValueByKey("clientset")=="no" && getMMLEntity()->getValueByKey("primary-cookie")!="" && entity->getValueByKey(getMMLEntity()->getValueByKey("primary-cookie"))!="")
    {
      toSend+="Set-Cookie: "+getMMLEntity()->getValueByKey("primary-cookie")+"="+entity->getValueByKey(getMMLEntity()->getValueByKey("primary-cookie"))+"\n";
      getMMLEntity()->setValueForKey("clientset","yes");
    }
  
  toSend+="\r\n";

    std::cout << "enviando:"<<std::endl;
  	  std::cout << toSend << std::endl;

  toSend+=getBody();
  ////////
  peer->send(toSend);
	std::cout << "commited." << std::endl;
  
}

/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

void HTTPSpawn::setCookie(const std::string &Cookie)
{ 
  if (getMMLEntity()!=NULL && getMMLEntity()->getValueByKey("primary-cookie")!="")
    return getMMLEntity()->setValueForKey(getMMLEntity()->getValueByKey("primart-cookie"),Cookie);
  // else
  ///throw ?
}

/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

const std::string HTTPSpawn::getCookie()
{ 
  if (getMMLEntity()!=NULL && getMMLEntity()->getValueByKey("primary-cookie")!="")
    return getMMLEntity()->getValueByKey(getMMLEntity()->getValueByKey("primart-cookie")).c_str();
  else
    return "";
}



/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

void HTTPSpawn::setMMLEntity(const CMMLEntity::MMLEntityReference Entity)
{ 
  entity=Entity;
}


/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

CMMLEntity::MMLEntityReference HTTPSpawn::getMMLEntity()
{ 
  return entity;
}





/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

void HTTPSpawn::setCmd(HTTPCommand Cmd)
{ 
  cmd=Cmd;
}


/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

HTTPSpawn::HTTPCommand HTTPSpawn::getCmd()
{ 
  return cmd;
}
