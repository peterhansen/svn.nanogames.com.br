/*
 * @author Daniel Monteiro
 * @brief atende a uma requisicao HTTP
 * @see HTTPDemon
*/

#ifndef HTTPSPAWN_H
#define HTTPSPAWN_H 1

class HTTPSpawn
{
 public:
  ///definicoes:
  typedef int HTTPStatus; 
  enum HTTPCommand {HTTP_CMD_GET, HTTP_CMD_POST, HTTP_CMD_HEAD, HTTP_CMD_NOT_SUPPORTED};
  ///construtores e destrutores:
  HTTPSpawn(const TCPSock* Peer);
  ~HTTPSpawn();
  
  ///metodos de acesso:
  void setBody(const std::string &Body);
  void setStatus(HTTPStatus Status);
  void setCookie(const std::string &Cookie);
  
  HTTPCommand getCmd();
  void setCmd(HTTPCommand Cmd);
  
  const std::string getCookie();
  const std::string getBody();
  HTTPStatus getStatus();
  CMMLEntity::MMLEntityReference getMMLEntity();
  void setMMLEntity(const CMMLEntity::MMLEntityReference Entity);
  const std::string getStatusPage();
  ///metodos gerais
  virtual void takeRequest(const std::string &Msg);
  void takeRequest();
  virtual void commit();
  ///metodos estaticos
  inline static const std::string getStringForStatus(HTTPStatus Status);
  inline static HTTPCommand getValueForCommand(const std::string &Cmd);
  inline static const std::string getErrorPage(HTTPStatus Status);

  
 private:
  /// metodos privados:
  

  ////campos da classe:
  TCPSock *peer; 
  int status;
  HTTPCommand cmd;
  std::string body;
  CMMLEntity::MMLEntityReference entity;
};

////////////////// constantes ///////////////////////////////////////////////

const HTTPSpawn::HTTPStatus HTTP_STATUS_OK = 200;
const HTTPSpawn::HTTPStatus HTTP_STATUS_NOT_FOUND = 404;
const HTTPSpawn::HTTPStatus HTTP_STATUS_NOT_SUPPORTED = 500;

////////////////// metodos estaticos e inline //////////////////////////////


/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

const std::string HTTPSpawn::getStringForStatus(HTTPStatus Status)
{  
  switch(Status)
    {
    case HTTP_STATUS_OK:
      return "OK";
    case HTTP_STATUS_NOT_FOUND:
      return "NOT FOUND";
    case HTTP_STATUS_NOT_SUPPORTED:
      return "NOT SUPPORTED";

    }
 return "NOT SUPPORTED";    
}

/************************************************************************ 
 * takeRequest - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

const std::string HTTPSpawn::getErrorPage(HTTPStatus Status)
{  
  std::string toReturn="";
  toReturn+="<html>\n";
  toReturn+="<body bgcolor=\'#000000\'><font color=\'#00FF00\'> \n";
  toReturn+=ItoA(Status)+" - "+getStringForStatus(Status);
  toReturn+="<br/> Pelo visto, esse servidor comeu um acaraj� estragado la no pel�...<br/>\n</font>\n</body>\n</html>";
  return toReturn.c_str();  
}



/************************************************************************ 
 * getValueForCommand - pega a string de requisicao do cliente e monta uma resposta
 * @author Daniel Monteiro
 * @see setBody setStatus
 *************************************************************************/

HTTPSpawn::HTTPCommand HTTPSpawn::getValueForCommand(const std::string &Cmd)
{  
  if (Cmd=="GET") return HTTP_CMD_GET;
  if (Cmd=="POST") return HTTP_CMD_POST;
  if (Cmd=="HEAD") return HTTP_CMD_HEAD;
  return HTTP_CMD_NOT_SUPPORTED;
}


#endif
