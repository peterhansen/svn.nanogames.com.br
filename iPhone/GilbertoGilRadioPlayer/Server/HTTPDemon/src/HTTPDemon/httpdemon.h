/*
 * @brief implementacao simples de HTTP - 
 * @author Daniel Monteiro
 * @see SockLib
 */


#ifndef HTTPDEMON_H
#define HTTPDEMON_H 1

///sim, eu sei. Brincadeira com HTTPD (httpdaemon)
class HTTPDemon
{
 private:
  ////////////
  class Range
  {
  public:
    int start;
    int finish;

    Range(int Start,int Finish)
      {
	start=Start;
	finish=Finish;
      }
  };
  ///////////
 public:
  ///constutores e destrutores:
  ////pelo amor de deus, chega de constantes hard-coded
  HTTPDemon(const std::string &Port, int FirstPort=9000, int LastPort=10000);
  virtual ~HTTPDemon();

  ///metodos de acesso:
  void setLocalHostName(const std::string &LocalHostName);  
  const char* getLocalHostName();  

  ///metodos de uso geral
  void start();
  const char* getNextPort();
  ///campos da classe:
 private:
  void setCountCookie(CMMLEntity::MMLEntityReference session);
  std::string localHostName;
  TCPSock listener;
  Range availablePorts;
};

#endif
