
/// C++
#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>


/// SockLib
#include "tcpsock.h"

/// MML
#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/// HTTPDemon
#include "httpspawn.h"
#include "httpdemon.h"

/// AudioServer
#include "audioserver.h"

/************************************************************************ 
 * construtor - cria e prepara o servico de HTTP
 * @author Daniel Monteiro
 * @see start ~HTTPDemon
 *************************************************************************/

HTTPDemon::HTTPDemon(const std::string &Port, int FirstPort, int LastPort):availablePorts(FirstPort,LastPort)
{
  ///posso precisar da porta para montar alguma
  listener.setPort(Port);
}

HTTPDemon::~HTTPDemon()
{
  ///TODOO:matar qualquer spawn que estiver vivo
}



void HTTPDemon::setCountCookie(CMMLEntity::MMLEntityReference session)
{
  ///nao preciso me preocupar muito, pois essa funcao soh eh chamada quando o sistema ja esta inicializado
  CMMLDatabase::MMLDatabaseReference db=CMMLDatabase::getInstance();
  CMMLCategory::MMLCategoryReference confs;
  CMMLEntity::MMLEntityReference stats;

  confs=db->getCategoryByName("configs");
  stats=confs->getEntityByKeyValue("section-name","HTTPDemon-general-stats");
  std::cout << "livestats:"<<std::endl<<stats->dump() << std::endl;
  int uniqueusers= AtoI(stats->getValueByKey("unique-users-live").c_str());  
  uniqueusers++;
  stats->setValueForKey("unique-users-live",ItoA(uniqueusers));
  session->setValueForKey("session-id",ItoA(uniqueusers));
  session->setValueForKey("clientset","no");

}

void HTTPDemon::start()
{  
    std::cout << "<HTTPDemon::init>" << std::endl;


  HTTPSpawn *spawn;
  TCPSock *client;
  bool IsMultithreadedListener;
  bool IsSessionsPersistent;
  bool IsVerbose;
  int spawnPid;
  ///inicializa a base de dados MML
  CMMLDatabase::MMLDatabaseReference db=CMMLDatabase::getInstance();

  db->setBasePath("./server_rs");

  CMMLCategory::MMLCategoryReference confs;
  CMMLCategory::MMLCategoryReference sessions;
  CMMLEntity::MMLEntityReference genconf;
  CMMLEntity::MMLEntityReference session;
  CMMLEntity::MMLEntityReference stats;


  

  {
    try
      {
	confs=db->getCategoryByName("configs");
      }
    catch(MMLNonExistantCategory *e)
      {
	    confs=new CMMLCategory();
	    confs->setCategory("configs");
	    db->addCategory(confs);	
      }
  }



  {
    try
      {
	sessions=db->getCategoryByName("sessions");
      }
    catch(MMLNonExistantCategory *e)
      {
	    sessions=new CMMLCategory();
	    sessions->setCategory("sessions");
	    db->addCategory(sessions);	
      }
  }

  confs->internalize();
  sessions->internalize();
  
  {
    try
      {
	genconf=confs->getEntityByKeyValue("section-name","HTTPDemon-general-configurations");
      }
    catch(MMLNonExistantEntity *e)
      {
	    genconf=new CMMLEntity();	    
	    genconf->setValueForKey("section-name","HTTPDemon-general-configurations");
	    genconf->setValueForKey("multithreaded-listener","true");
	    genconf->setValueForKey("sessions-persistent","false");
	    genconf->setValueForKey("verbose","false");
	    confs->addEntity(genconf);
      }
  }




  {
    try
      {
	stats=confs->getEntityByKeyValue("section-name","HTTPDemon-general-stats");
      }
    catch(MMLNonExistantEntity *e)
      {
	    stats=new CMMLEntity();
	    stats->setValueForKey("section-name","HTTPDemon-general-stats");
	    confs->addEntity(stats);
      }
  }


  stats->setValueForKey("next-available-port","0");
  stats->setValueForKey("unique-users-live","0");
  genconf->setValueForKey("base-port","9000");
  genconf->setValueForKey("telnet-port","10000");
  genconf->setValueForKey("servlet-port","11000");
  genconf->setValueForKey("client-limit","10");
  //genconf->setValueForKey("audioserver-base-url","http://staging.nanogames.com.br");
  genconf->setValueForKey("audioserver-base-url","http://192.168.1.178");


  genconf->setValueForKey("audioserver-stream-app-name","../vlc/vlc");
  genconf->setValueForKey("audioserver-audiofiles-url","http://www.gilbertogil.com.br/upload/discografia_1/audio/");
  genconf->setValueForKey("audioserver-stream-options"," -vvv %filename --play-and-exit -I telnet --telnet-port %telnetport  --daemon  --sout '#transcode{acodec=mp3,ab=128}:std{access=http,mux=mp4,dst=:%streamport}'");
  genconf->setValueForKey("audioserver-filename-marker","%filename");
  genconf->setValueForKey("audioserver-telnetport-marker","%telnetport");
  genconf->setValueForKey("audioserver-streamport-marker","%streamport");
  

  IsMultithreadedListener=  genconf->getValueByKey("multithreaded-listener") == "true";
  IsSessionsPersistent=  genconf->getValueByKey("sessions-persistent") == "true";
  IsVerbose=  genconf->getValueByKey("verbose") == "true";

	IsVerbose=true;
  if (IsVerbose)
    std::cout << "iniciando HTTP" << std::endl;

  std::string Msg;
  while (true)
    {
      client=listener.waitForConnectionBlock();

      if (IsVerbose)
	std::cout << "cliente aceito" << std::endl;      

      if (IsMultithreadedListener)
	spawnPid=fork();

      if (!IsMultithreadedListener || !spawnPid)
	{
	  Msg=client->receive(true);	 

	  if (IsVerbose)
	    {
	      std::cout << "-------------------request-----------------" << std::endl;
	      std::cout << "MSG:"<<std::endl<<Msg<<std::endl;
	      std::cout << "-------------------end of request-----------------" << std::endl;
	    }

	  if (Msg.substr(0,Msg.find_first_of(' '))=="STREAM" ||   Msg.substr(0,Msg.find_first_of(' '))=="KILL-STREAM")
	      spawn=new AudioServer(client);
	  else
	      spawn=new HTTPSpawn(client);
	  
	  session=spawn->getMMLEntity();
	  sessions->addEntity(session);	


	  if (Msg.substr(0,Msg.find_first_of(' '))=="STREAM")
	    setCountCookie(session);

	  spawn->takeRequest(Msg);



	  if (IsVerbose)
	    std::cout << "sessions agora tem "<<sessions->getTotalItems() << " items"<< std::endl;

	  if (session->getValueByKey("session-id")=="")	    
	    setCountCookie(session);

	  spawn->commit();
	  if (IsSessionsPersistent)
	    sessions->commit();
	  delete spawn;	  
	}  
      confs->commit();
	  
    }
    std::cout << "</HTTPDemon::init>" << std::endl;      
}
