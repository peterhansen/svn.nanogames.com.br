/*
 * @brief classe socket TCP 
 * @author Daniel Monteiro
 * @see HTTPDemon TCPSockListen
 */


#ifndef TCPSOCK_H
#define TCPSOCK_H 1


const int BLANKTCPSOCKET=-1;
const int TCPSOCKETBUFFERSIZE=255;
//---------------------------------------------------------------------------
class SockLibException : public std::exception 
{
private:
		std::string cause;
public:	
	SockLibException() 
		{
		}
	SockLibException(std::string Cause) 
		{ 
			cause=Cause;
		}
	virtual ~SockLibException() throw() 
		{
		}
	virtual const char *what() 
		{ 
			return cause.c_str(); 
		}
};
//---------------------------------------------------------------------------
///temporariamente, sao constantes numericas. logo serao classes derivadas de std::exception
#define TCP_SOCKET_NOT_CONNECTED  SockLibException("not connected")
#define TCP_SOCKET_UNKNOWN_HOSTNAME SockLibException("unknown hostname")
#define TCP_SOCKET_UNKNOWN_PORT SockLibException("unknown port")
#define TCP_SOCKET_CONNECTION_ERROR SockLibException("connection error")
#define TCP_SOCKET_ALREADY_CONNECTED SockLibException("already connected")
#define TCP_SOCKET_SEND_FAILED SockLibException("send failed")
#define TCP_SOCKET_RECV_FAILED SockLibException("recv failed")
#define TCP_SOCKET_INITIALIZATION_ERROR SockLibException("init error")
#define TCP_SOCKET_INVALID_STATE SockLibException("invalid state")


class TCPSock
{
 public:  
  ///definicoes padrao:
  typedef int UnixHandler;
  enum TCPSockState {DISCONNECTED, LISTENING , CONNECTING , CONNECTED};
  ///ciclo de vida do objeto:
  TCPSock(int SockFD=BLANKTCPSOCKET);
  ~TCPSock();
  ///metodos de acesso a membros de dados:
  TCPSockState getState();
  UnixHandler  getSockFD()  ;
  std::string  getHostName() ;
  std::string  getPort() ;
  void setState(TCPSockState State);
  void setSockFD(UnixHandler SockFD);
  void setHostName(const std::string& HostName);
  void setPort(const std::string& Port);
  ///metodos gerais:
  void connect();
  void connect(const std::string &HostName,const std::string &Port);
  void disconnect();
  ///TODOO: hard-codding is a no no...shame on you, DM...
  TCPSock* waitForConnectionBlock(const std::string &Port="", int MaxQueue=20);
  void send(const std::string &Data);
  const std::string receive(bool StopOnCRLF=false);
  //  bool isDataReady();  
  
  ///membros privados;  
 private:
  UnixHandler sockFD;  
  std::string hostName;
  std::string port;
  std::string peerPort;
  //uso interno, mantem o estado da conexao.
  TCPSockState state;
  //callback?

};

#endif
