///C & Unix
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <sys/wait.h> 
#include <signal.h> 

///C++
#include <iostream>
#include <string>


#include "tcpsock.h"

/************************************************************************ 
 * TCPSock - construtor
 * @author Daniel Monteiro
 * @see ~TCPSock
 *************************************************************************/
TCPSock::TCPSock(UnixHandler SockFD):sockFD(SockFD),hostName(""),state(DISCONNECTED)
{
 ///se nao recebe um sockfd valido, entao esta desconectado
  if (SockFD!=BLANKTCPSOCKET)    
    {
      state=CONNECTED;
    }
}


/************************************************************************ 
 * TCPSock - destrutor
 * @author Daniel Monteiro
 * @see TCPSock
 *************************************************************************/
TCPSock::~TCPSock()
{
  ///TODOO: verificar se existem tratamentos especificos para os outros casos de estados de um socket.
  if (state!=DISCONNECTED)
    {
      close(sockFD);
    }
}

/************************************************************************ 
 * getHostName - obtem o nome (ou endereco) da outra ponta da conexao.
 * @author Daniel Monteiro
 * @return pointeiro const char contendo nome da maquina na outra ponta.
 * @throw TCP_SOCKET_NOT_CONNECTED caso o socket nao esteja conectado.
 * @see setHostName
 *************************************************************************/
std::string TCPSock::getHostName()
{
  ///TODOO: verificar se mesmo que a conexao esteja em handshake, ja se deve ter um hostname
  if (getState()==DISCONNECTED)
    throw TCP_SOCKET_NOT_CONNECTED;
  else
    return hostName.c_str();
}

/************************************************************************ 
 * getHostName - obtem o descritor do Socket. Deve ser usado com cuidado
 * @author Daniel Monteiro
 * @return inteiro que server de handler de sistema para o ponteiro TCP. -1 caso o socket esteja desconectado
 * @see setSockFD
 *************************************************************************/
TCPSock::UnixHandler TCPSock::getSockFD()
{
  ///aqui, mesmo que se retornar BlankTCPSocket, "nao eh problema meu". Talvez interesse a outra classe obter o valor de BlankTCPSocket.
  return sockFD;
}

/************************************************************************ 
 * setHostName - determina o nome do host remoto.
 * @author Daniel Monteiro
 * @param HostName ponteiro const char contendo o nome do host remoto. Este nome nao eh conferido com o valor associado ao socket e sera sobre-escrito em caso de nova conexao.
 * @see getHostName
 *************************************************************************/
void TCPSock::setHostName(const std::string &HostName)
{
  ///como hostName, ele ja cuida de fazer a copia do ponteiro char, eliminando problemas de ownership
  hostName=HostName;
}




/************************************************************************ 
 * getPort - obtem o numero da porta atribuido ao socket.
 * @author Daniel Monteiro
 * @return ponteiro para const char contendo o numero da porta
 * @see setPort
 *************************************************************************/
std::string TCPSock::getPort()
{
  return  port;
}

/************************************************************************ 
 * setPort - atribui um numero de porta ao socket.
 * @author Daniel Monteiro
 * @param Port ponteiro const para char, contendo o numero da porta
 * @see getHostName
 *************************************************************************/
void TCPSock::setPort(const std::string &Port)
{
  ///como hostName, ele ja cuida de fazer a copia do ponteiro char, eliminando problemas de ownership
  port=Port;
}



/************************************************************************ 
 * setHostName - internaliza um novo descritor de socket. Funciona como um mecanismo de copia de sockets.
 * @author Daniel Monteiro
 * @param SockFD descritor de socket a ser internalizado.
 * @see getSockFD
 *************************************************************************/
void TCPSock::setSockFD(TCPSock::UnixHandler SockFD)
{
  ///para evitar problemas de ownership, eu nao fecho a conexao do socket antigo.
  sockFD=SockFD;
}


/************************************************************************ 
 * connect - conecta a um hostname previamente definido.
 * @author Daniel Monteiro
 * @throw TCP_SOCKET_UNKNOWN_HOSTNAME caso nao haja hostname pre-definido
 * @throw TCP_SOCKET_UNKNOWN_PORT caso nao haja porta pre-definida
 * @see connect (const char *hostname)
 *************************************************************************/
void TCPSock::connect()
{
  if (hostName=="")
    throw TCP_SOCKET_UNKNOWN_HOSTNAME;

  if (port=="")
    throw TCP_SOCKET_UNKNOWN_PORT;

  connect(hostName,port);
}


/************************************************************************ 
 * connect - conecta a um hostname
 * @author Daniel Monteiro
 * @param HostName ponteiro const char contendo o nome do host remoto a ser contactado
 * @param Port ponteiro const char contendo o numero da porta aberta no host remoto.
 * @throw TCP_SOCK_UNKNOWN_HOSTNAME caso o hostname nao seja valido.
 * @throw TCP_SOCKET_UNKNOWN_PORT caso nao haja porta pre-definida
 * @throw TCP_SOCK_INITIALIZATION_ERROR caso a inicializacao do socket falhe
 * @throw TCP_SOCK_CONNECTION_ERROR caso a conexao com o servidor falhe
 * @see connect ()
 *************************************************************************/
void TCPSock::connect(const std::string &HostName,const std::string &Port)
{

 if (HostName=="")
    throw TCP_SOCKET_UNKNOWN_HOSTNAME;

  if (Port=="")
    throw TCP_SOCKET_UNKNOWN_PORT;


  ///declaracoes uteis para a funcao
  struct addrinfo hints, *servinfo; 
  int sockfd;
  
  ///estamos no processo de conexao:
  state=CONNECTING;
  
  ///obtem informacoes sobre o host remoto
  {

    ////inicializa a estrutura
    memset(&hints, 0, sizeof hints); 
    hints.ai_family = AF_UNSPEC;  
    hints.ai_socktype = SOCK_STREAM;
    
    if (getaddrinfo(HostName.c_str(), Port.c_str(), &hints, &servinfo) != 0)
      throw TCP_SOCKET_UNKNOWN_HOSTNAME;
  }
  ///inicializa o socket
  {
    if ((sockfd = socket(servinfo->ai_family, servinfo->ai_socktype,0)) == -1) 
      throw TCP_SOCKET_INITIALIZATION_ERROR;
  }
  ///e finalmente conecta ao host remoto.
  {
    if (::connect(sockfd, servinfo->ai_addr, servinfo->ai_addrlen)==-1)
      throw TCP_SOCKET_CONNECTION_ERROR;
  }
  ///limpeza:
  freeaddrinfo(servinfo);
  
  ///ajustes dos campos da classe:
  state=CONNECTED;
  setSockFD(sockfd);
  setHostName(HostName);
  setPort(Port);
}


/************************************************************************ 
 * disconnect - desconecta o socket
 * @author Daniel Monteiro
 * @throw TCP_SOCK_NOT_CONNECTED caso o socket nao esteja conectado
 * @see connect (const char *hostname)
 *************************************************************************/
void TCPSock::disconnect()
{
  if (state==DISCONNECTED)
    throw TCP_SOCKET_NOT_CONNECTED;
  
  setState(DISCONNECTED);
  shutdown(getSockFD(),2);
  close(getSockFD());
  
  ///Pode ser que eu queira ter a chance de reconectar...
  setHostName("");
  setPort("");
  setSockFD(BLANKTCPSOCKET);
}


/************************************************************************ 
 * waitForConnectionBlock - espera por conexao na porta indicada
 * @author Daniel Monteiro
 * @throw TCP_SOCK_UNKNOWN_PORT caso nao haja porta pre-definida e a porta passada como parametro seja invalida
 * @throw TCP_SOCK_ALREADY_CONNECTED caso o socket ja esteja conectado.
 * @param Port ref. para string const contendo o numero da porta.Este nao precisa ser passado caso a porta tenha sido previamente definida.
 * @see connect (const char *hostname)
 *************************************************************************/
TCPSock* TCPSock::waitForConnectionBlock(const std::string &Port, int MaxQueue)
{
  ///declaracoes uteis para a funcao
  int sockfd,tmpfd;  
  
  struct sockaddr_storage their_addr; 
  socklen_t addr_size; 
  struct addrinfo hints, *res;  
  

  if (Port=="" && getPort()=="" )
    throw TCP_SOCKET_UNKNOWN_PORT;

  if (state==CONNECTED && Port!="")
    throw TCP_SOCKET_ALREADY_CONNECTED;


      ///define como sera nossa escuta
      {
	///prenche a estrutura res (addrinfo)
	memset(&hints, 0, sizeof hints); 
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM; 
	hints.ai_flags = AI_PASSIVE;
	getaddrinfo(NULL, getPort().c_str(), &hints, &res); 
      }

  if (state==DISCONNECTED)
    {
      ///a porta passada como parametro tem prioridade
      if (Port!="")
	setPort(Port);
      
      
      ///TODOO: verificar como incluir esse handler 
      ///  signal(SIGINT,quitHandler);
      
      /// estamos aceitando conexoes:
      state=CONNECTING;
      
      
      
      ///abre o socket
      tmpfd  = socket(res->ai_family, res->ai_socktype, 0); 
      setSockFD(tmpfd);
      ///liga com o endereco
      bind(tmpfd, res->ai_addr, res->ai_addrlen); 
      
      ///elimina problemas em re-aproveitar portas e sockets
      {
	int yes=1;
	if (setsockopt(tmpfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) 
	  throw TCP_SOCKET_INITIALIZATION_ERROR;
      }
      
      
      ///escuta por novas conexoes
      listen(tmpfd, MaxQueue);  
    }
  else
    tmpfd=getSockFD();
  ///obtem a primeira conexao de entrada esperando por conexao
  addr_size = sizeof their_addr; 
  sockfd = accept(tmpfd, (struct sockaddr *)&their_addr, &addr_size);

  if (sockfd<0)  
      throw TCP_SOCKET_INITIALIZATION_ERROR;

  state=CONNECTED;
  ///elimina problemas em re-aproveitar portas e sockets
 
  /*
  ///obtem o nome da outra ponta:
  {
    //feio, mas necessario...
    void *in_addr;
    
    if (((struct sockaddr *)&their_addr)->sa_family == AF_INET)       
      in_addr= &(((struct sockaddr_in*)((struct sockaddr *)&their_addr))->sin_addr);      
    else
      in_addr= &(((struct sockaddr_in6*)((struct sockaddr *)&their_addr))->sin6_addr);
    
    std::string addr;
    char s[INET6_ADDRSTRLEN];
    addr=inet_ntop(their_addr.ss_family, in_addr , s, sizeof s);
    setHostName(addr);
  }
  
  freeaddrinfo(res);
  */
  return new TCPSock(sockfd);
}



/************************************************************************ 
 * send - envia dados para a outra ponta
 * @author Daniel Monteiro
 * @throw TCP_SOCK_NOT_CONNECTED caso o socket nao esteja conectado
 * @throw TCP_SOCK_SEND_FAILED caso o envio falhe
 * @param Data referencia const para string contendo os dados
 * @see receive
 *************************************************************************/
void TCPSock::send(const std::string &Data)
{
  if (state==DISCONNECTED)
    throw TCP_SOCKET_NOT_CONNECTED;

  ///+1 para o \0
  if (::send(getSockFD(),Data.c_str(),Data.length()+1,0)==-1)
    throw TCP_SOCKET_SEND_FAILED;
}


/************************************************************************ 
 * send - envia dados para a outra ponta
 * @author Daniel Monteiro
 * @throw TCP_SOCK_NOT_CONNECTED caso o socket nao esteja conectado
 * @throw TCP_SOCK_RECV_FAILED caso o envio falhe
 * @return retorna uma std::string constante contendo os dados
 * @see send
 *************************************************************************/
const std::string TCPSock::receive(bool StopOnCRLF)
{
  if (state==DISCONNECTED)
    throw TCP_SOCKET_NOT_CONNECTED;

  char buffer[TCPSOCKETBUFFERSIZE];
  int numbytes=0;
  std::string toReturn="";
  //  do
    {
      ///+1 para o \0
      std::cout << "waiting for data" << std::endl;
      if ((numbytes=recv(getSockFD(),buffer,TCPSOCKETBUFFERSIZE,0))==-1)
	throw TCP_SOCKET_RECV_FAILED;
      
      buffer[numbytes]='\0';
      toReturn+=buffer;
      std::cout <<"so far:" << toReturn << std::endl;

    }
  //while (StopOnCRLF  && (toReturn.length()>2 && toReturn.substr(toReturn.length()-2,2)!="\r\n"));
  
  return toReturn;
}


/************************************************************************ 
 * setState - seta o estado do socket
 * @author Daniel Monteiro
 * @throw TCP_SOCK_INVALID_STATE caso o estado proposto seja invalido
 * @param State o novo estado
 * @see getState
 *************************************************************************/
void TCPSock::setState(TCPSock::TCPSockState State)
{
  if (State>CONNECTED || State < DISCONNECTED)
    throw TCP_SOCKET_INVALID_STATE;

  state=State;
}


/************************************************************************ 
 * getState - obtem o estado do socket
 * @author Daniel Monteiro
 * @return o estado do socket
 * @see setState
 *************************************************************************/
TCPSock::TCPSockState TCPSock::getState()
{
  return state;
}
