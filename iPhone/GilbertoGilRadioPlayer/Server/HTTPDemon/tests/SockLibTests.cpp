#include <limits.h>
#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>


#include <gtest/gtest.h>
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MML/MMLCategory.h"
#include "MML/MMLEntity.h"
#include "MML/MMLDatabase.h"

//so' para limpar a tela
TEST(SockLibDoesNothing,ButClearsTheScreen)
{
	system("clear");
	std::cout << "==========================================================" << std::endl;
	std::cout << "* E X E C U T A N D O   T E S T E S . . .                *" << std::endl;	
	std::cout << "==========================================================" << std::endl;
	std::cout << "TESTES: SockLib" << std::endl;		
}