#include <limits.h>
#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>


#include <gtest/gtest.h>
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MML/MMLCategory.h"
#include "MML/MMLEntity.h"
#include "MML/MMLDatabase.h"

//so' para limpar a tela
TEST(MMLDoesNothing,ButClearsTheScreen)
{
	system("clear");
	std::cout << "==========================================================" << std::endl;
	std::cout << "* E X E C U T A N D O   T E S T E S . . .                *" << std::endl;	
	std::cout << "==========================================================" << std::endl;
	std::cout << "TESTES: MML" << std::endl;			
}

//====================================================================================
///<testes de entidade>
///<cobertura>
/*
- ajustes de chave-valor:
-- se o valor setado e' retornado
-- busca chave nao existente
-- strings vazias tanto na chave quanto no valor

- comparacao entre entidades:
-- a outra entidade e' nula
-- uma das entidades e' vazia
-- a outra entidade e' vazia
-- ambas as entidades sao vazias
-- ambas entidades sao iguais

- testes de IO:
-- stream vazio
-- stream mal formado (chave vazia)
-- stream mal formado (valor vazio)
-- stream mal formado (ambos vazios)
-- stream mal formado (sem marcacao de fim de arquivo)
*/
///</cobertura>
//---------------------------------------------------------------------------
// testes de chave, valor

//---------------------------------------------------------------------------
TEST(EntityTest, TestKeyValueDoesntExist) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	EXPECT_EQ(entity->getValueByKey("capital"),"");	
	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestKeyValueSetted) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->setValueForKey("nome-pais","Brasil");
	entity->setValueForKey("capital","Brasilia");
	EXPECT_EQ(entity->getValueByKey("capital"),"Brasilia");
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEmptyStrings) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->setValueForKey("","Brasil");
	entity->setValueForKey(" ","Brasilia");
	entity->setValueForKey("Brasil"," ");
	entity->setValueForKey("Brasilia","");
	EXPECT_EQ(entity->getValueByKey(""),"Brasil");
	EXPECT_EQ(entity->getValueByKey(" "),"Brasilia");
	EXPECT_EQ(entity->getValueByKey("Brasil")," ");
	EXPECT_EQ(entity->getValueByKey("Brasilia"),"");			
}
//---------------------------------------------------------------------------
// testes de igualdade entre entidades

TEST(EntityTest, TestEqualEntities) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");
	entity2->setValueForKey("nome-pais","Brasil");
	entity2->setValueForKey("capital","Brasilia");	
	EXPECT_EQ(entity1->equal(entity2),true);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesDiferentOther) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");
	entity2->setValueForKey("capital","Brasilia");
	entity2->setValueForKey("nome-pais","Brasil");		
	EXPECT_EQ(entity1->equal(entity2),true);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesIncompleteOther) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");
	entity2->setValueForKey("capital","Brasilia");
	EXPECT_EQ(entity1->equal(entity2),false);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesDiferentValueOther) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");
	entity2->setValueForKey("capital","Buenos Aires");
	EXPECT_EQ(entity1->equal(entity2),false);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesDiferentKeyOther) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");
	entity2->setValueForKey("cidade","Brasilia");
	EXPECT_EQ(entity1->equal(entity2),false);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesNULLOther) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=NULL;	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");	
	try 
		{
			entity1->equal(entity2);
		}
	catch (MMLNonExistantEntity *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}		
		
	FAIL();	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesEmptyOther) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity1->setValueForKey("nome-pais","Brasil");
	entity1->setValueForKey("capital","Brasilia");
	EXPECT_EQ(entity1->equal(entity2),false);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesEmptyFirst) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	entity2->setValueForKey("capital","Brasilia");
	EXPECT_EQ(entity1->equal(entity2),false);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEqualEntitiesEmptyBoth) 
{
	CMMLEntity::MMLEntityReference entity1=new CMMLEntity();
	CMMLEntity::MMLEntityReference entity2=new CMMLEntity();	
	EXPECT_EQ(entity1->equal(entity2),true);
}

//---------------------------------------------------------------------------
// testes de IO

TEST(EntityTest, TestInternalizeEntityStrict) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	EXPECT_EQ(entity->getValueByKey("programador"),"Daniel");	
	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityAlreadyFilled) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->setValueForKey("teste","123");
	entity->initFromStream(str);
	EXPECT_EQ(entity->getValueByKey("teste"),"123");	
	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEmptyStream) 
{
	std::stringstream str;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	EXPECT_EQ(entity->getTotalFields(),0);	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityEmptyValue) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	
	try
		{
		entity->initFromStream(str);
		}
	catch (MMLInvalidAttrib *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();
			return;
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	EXPECT_EQ(entity->getValueByKey("programador"),"");	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityNoAttrib) 
{
	std::stringstream str;
	str<< "programador"<< std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	try
		{
		entity->initFromStream(str);
		}
	catch (MMLInvalidAttrib *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();
			return;
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				

	FAIL();		
}

//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityDuplicateEOL) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Monteiro"<<ENTITY_EOL_MARKER <<"Daniel"<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	std::string expected;
	expected+="Monteiro";
	expected+=ENTITY_EOL_MARKER;
	expected+="Daniel";
	EXPECT_EQ(entity->getValueByKey("programador"),expected);	
	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityDuplicateAttrib) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<< ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	std::string expected;
	expected+=ENTITY_ATTRIB_MARKER;
	expected+="Daniel";
	EXPECT_EQ(entity->getValueByKey("programador"),expected);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityInnerAttrib) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Nome"<< ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	std::string expected;
	expected+="Nome";	
	expected+=ENTITY_ATTRIB_MARKER;
	expected+="Daniel";
	EXPECT_EQ(entity->getValueByKey("programador"),expected);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityInnerAttribComposite) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Nome"<< ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER<<"Monteiro"<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	std::string expected;
	expected+="Nome";	
	expected+=ENTITY_ATTRIB_MARKER;
	expected+="Daniel";
	expected+=ENTITY_EOL_MARKER;
	expected+="Monteiro";
	EXPECT_EQ(entity->getValueByKey("programador"),expected);
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityInnerAttribCompositeDoubleEOL) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Nome"<< ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER<<"Monteiro"<<ENTITY_EOL_MARKER<<ENTITY_EOL_MARKER << std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	std::string expected;
	expected+="Nome";	
	expected+=ENTITY_ATTRIB_MARKER;
	expected+="Daniel";
	expected+=ENTITY_EOL_MARKER;
	expected+="Monteiro";
	expected+=ENTITY_EOL_MARKER;	
	EXPECT_EQ(entity->getValueByKey("programador"),expected);
}

//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityNoEOFMarker) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Daniel"<<ENTITY_EOL_MARKER << std::endl;
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	entity->initFromStream(str);
	EXPECT_EQ(entity->getValueByKey("programador"),"Daniel");	
	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestInternalizeEntityNoEOLMarker) 
{
	std::stringstream str;
	str<< "programador" << ENTITY_ATTRIB_MARKER<<"Daniel"<< std::endl;
	str<< ENTITY_EOF_MARKER << std::endl;	
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
		try 
		{
		entity->initFromStream(str);
		}
	catch (MMLInvalidAttrib *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what(); 	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	FAIL();	
}
///</testes de entidade>
//====================================================================================
///<testes de categoria>
///<cobertura>
/*
- ajustes de chave-valor e ordem de adicao:
-- adicionar a uma categoria muda o id
-- setar uma chave-valor depois de adicionar a uma categoria
*/
///</cobertura>
TEST(CategoryTest, TestAddingAlreadyCategorizedEntity) 
{
	CMMLEntity::MMLEntityReference entity;
	CMMLCategory::MMLCategoryReference category1;
	CMMLCategory::MMLCategoryReference category2;
	CMMLDatabase::MMLDatabaseReference db;
	//esperado sucesso
	try 
		{
		entity=new CMMLEntity();	
		category1=new CMMLCategory();
		category2=new CMMLCategory();	
			
			
		db=CMMLDatabase::getInstance();			
		db->cleanUp();
		category1->setCategory("a1");
		db->addCategory(category1);	
		category2->setCategory("a2");	
		db->addCategory(category2);						
		entity->setValueForKey("resultado-esperado","sucesso");		
		category1->addEntity(entity);
		}
	catch (MMLBaseException *e)	
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	//esperado erro
	try 
		{
		category2->addEntity(entity);
		}
	catch (MMLEntityAlreadyCategorized *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)	
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	FAIL();	
}

TEST(CategoryTest, TestIdChangesAfterAdd) 
{
	EntityIdType beforeAdd;
	EntityIdType afterAdd;	
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	beforeAdd=entity->getId();
	category->addEntity(entity);
	afterAdd=entity->getId();	
	EXPECT_NE(beforeAdd,afterAdd);	
}


TEST(CategoryTest, TestGetEntityByKeyValueExistsSetAfter) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	category->addEntity(entity);
	entity->setValueForKey("test framework","gtest");	
	EXPECT_EQ(entity,category->getEntityByKeyValue("test framework","gtest"));	
}
//---------------------------------------------------------------------------
TEST(CategoryTest, TestGetEntityByKeyValueExistsSetBefore) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	entity->setValueForKey("test framework","gtest");	
	category->addEntity(entity);	
	EXPECT_EQ(entity,category->getEntityByKeyValue("test framework","gtest"));	
}
//---------------------------------------------------------------------------
TEST(CategoryTest, TestSetKeyValueAfterAdd) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	category->addEntity(entity);
	entity->setValueForKey("test framework","gtest");	
	EXPECT_EQ(entity->getValueByKey("test framework"),"gtest");	
}
//---------------------------------------------------------------------------
TEST(CategoryTest, TestSetKeyValueBeforeAdd) 
{
	CMMLEntity::MMLEntityReference entity=new CMMLEntity();
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	entity->setValueForKey("test framework","gtest");	
	category->addEntity(entity);	
	EXPECT_EQ(entity->getValueByKey("test framework"),"gtest");	
}
//---------------------------------------------------------------------------
TEST(CategoryTest, TestAddNULLEntity) 
{
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	try 
		{
			category->addEntity(NULL);	
		}
	catch (MMLNonExistantEntity *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what() ;	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	FAIL();
}

//---------------------------------------------------------------------------
TEST(CategoryTest, TestReplicatedEntitiesAdd) 
{
	CMMLEntity::MMLEntityReference entity1;
	CMMLEntity::MMLEntityReference entity2;	
	CMMLCategory::MMLCategoryReference category;
	
	///<teste>
	//esperado sucesso
	try 
		{
			category=new CMMLCategory();	
			entity1=new CMMLEntity();	
			entity1->setValueForKey("test-framework","gtest");		
			entity2=new CMMLEntity();	
			entity2->setValueForKey("test-framework","gtest");			
			category->addEntity(entity1);	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	//esperado erro
	try 
		{
			category->addEntity(entity2);	
		}
	catch (MMLDuplicateTrivialEntity *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{ 
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	FAIL();		
	///</teste>
	
	
}
//---------------------------------------------------------------------------
TEST(EntityTest, TestEntityDoesntExist) 
{
	CMMLCategory::MMLCategoryReference category=new CMMLCategory();
	try 
		{
			CMMLEntity::MMLEntityReference entity=category->getEntityByKeyValue("a","b");
		}
	catch (MMLNonExistantEntity *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}			
	FAIL();
}
///</testes de categoria>
//====================================================================================
///<testes de base de dados>
//---------------------------------------------------------------------------
TEST(CategoryTest, TestAddNULLCategory) 
{
	////<declaracoes>
	CMMLDatabase::MMLDatabaseReference db;	
	///</declaracoes>		
	try 
		{
			db=CMMLDatabase::getInstance();				
			db->addCategory(NULL);	
		}
	catch (MMLNonExistantCategory *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	FAIL();
}
//---------------------------------------------------------------------------
TEST(CategoryTest, TestAddAnonymousCategory) 
{
	////<declaracoes>
	CMMLDatabase::MMLDatabaseReference db;	
	CMMLCategory::MMLCategoryReference collection;	
	///</declaracoes>		
	
	try 
		{
			db=CMMLDatabase::getInstance();				
			collection = new CMMLCategory();			
			db->addCategory(collection);	
		}
	catch (MMLNonExistantCategory *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();	
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	FAIL();
}

//---------------------------------------------------------------------------
TEST(DatabaseTest, TestSingletonConsistency) 
{
	////<declaracoes>
	CMMLDatabase::MMLDatabaseReference db;	
	CMMLDatabase::MMLDatabaseReference db_mirror;		
	CMMLCategory::MMLCategoryReference collection;
	CMMLCategory::MMLCategoryReference collection_mirror;
	CMMLEntity::MMLEntityReference entity;	
	CMMLEntity::MMLEntityReference entity_mirror;
	///</declaracoes>		
	
	////<incializando o primeiro conjunto de valores>
	db=CMMLDatabase::getInstance();	
	collection=new CMMLCategory();
	collection->setCategory("paises");
	db->addCategory(collection);
	
	entity=new CMMLEntity();
	entity->setValueForKey("nome-pais","Brasil");
	entity->setValueForKey("capital","Brasilia");
	collection->addEntity(entity);
	///</incializando o primeiro conjunto de valores>
		
	////<obtendo o conjunto espelho...>
	db_mirror=CMMLDatabase::getInstance();	
	///</obtendo o conjunto espelho...>	
	
	///<teste do singleton em si>
	EXPECT_EQ(db,db_mirror);
	///</teste do singleton em si>	
		
	///<obtendo o conjunto espelho...resto>
	collection_mirror=db_mirror->getCategoryByName("paises");
	entity_mirror=collection_mirror->getEntityByKeyValue("nome-pais","Brasil");	
	///</obtendo o conjunto espelho...resto>	
	
	////<tests>
	EXPECT_EQ(entity->getValueByKey("capital"), entity_mirror->getValueByKey("capital"));
	///</tests>
}
//---------------------------------------------------------------------------
TEST(DatabaseTest, TestDuplicateCategory) 
{
	////<declaracoes>
	CMMLDatabase::MMLDatabaseReference db;	
	CMMLCategory::MMLCategoryReference collection1;
	CMMLCategory::MMLCategoryReference collection2;
	///</declaracoes>		
	
	///<teste>
	//esperado sucesso
	try 
		{
		////<incializando o primeiro conjunto de valores>
		db=CMMLDatabase::getInstance();	
		collection1=new CMMLCategory();
		collection1->setCategory("nova-categoria");
		collection2=new CMMLCategory();
		collection2->setCategory("nova-categoria");		
		///</incializando o primeiro conjunto de valores>			
		db->addCategory(collection1);
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
			return;
		}
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
		
	//esperado erro
	try 
		{
			db->addCategory(collection2);	
		}
	catch (MMLCategoryAlreadyManaged *e)
		{
			SUCCEED();
			return;	
		}
	catch (MMLBaseException *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (MMLBaseException &e)
		{
			FAIL() << e.what();
		}		
	catch (std::exception *e)
		{
			FAIL() << e->what();	
			return;
		}		
	catch (std::exception &e)
		{
			FAIL() << e.what();	
			return;
		}		
	catch (int e)
		{
			FAIL() <<"int e="<< e;	
			return;
		}				
	///nao lancou excessao, entao esta errado.
	FAIL();		
	///</teste>
}
//---------------------------------------------------------------------------
///</testes de base de dados>
//====================================================================================