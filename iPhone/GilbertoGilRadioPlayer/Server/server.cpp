#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <string>
#include <iostream>
#include <sys/wait.h> 
#include <signal.h> 
#include <sstream>

#define MAXDATASIZE 100
#define PORTA_ENTRADA "11475"
#define PORTA 11498

typedef FILE pipe_t;
typedef int socket_t;

socket_t sockfd, new_fd;
int connectors;
char isbuff[100];


void sigchld_handler(int s) 
{ 
    while(waitpid(-1, NULL, WNOHANG) > 0); 
} 

//lembrar de alocar antes de usa
void IntToStr(int num,std::string &str)
{
std::stringstream ss;
ss << num;
str=ss.str();
}

void closesocks()
{
	std::cout << "closing sockets" << std::endl;
	close(new_fd);
	close(sockfd);
}

void quitHandler(int sig) {
	printf("Quitting because of ctrl-c....\n");
	closesocks();
	system("killall vlc");	
	exit(0);
}


pipe_t* Stream(const char* filename)
{
	std::cout << "streaming "<<filename << " at 90";
	std::string cmd="vlc -vvv ";
	std::string tmp;

	IntToStr(connectors,tmp);

	if (connectors<100)
	  tmp="0"+tmp;

	if (connectors<10)
	  tmp="0"+tmp;

	std::cout << "telnet na porta 10"<<tmp<< std::endl;

	cmd+=filename;
	cmd+=" --play-and-exit -I telnet --telnet-port 10";
	cmd+=tmp;
	cmd+=" --daemon  --sout '#transcode{acodec=mp3,ab=128}:std{access=http,mux=mp4,dst=:9";
	std::cout << tmp <<std::endl;
	cmd+=tmp;
	cmd+="}'";
	std::cout << cmd <<std::endl;
	//	if (!fork())  
	{
	  //	int ret = system(cmd.c_str());
	  return popen(cmd.c_str(),"w");
	}

//	return cmd;
}

void receive(int sockfd, char **buf)
{
	int numbytes=0;
	char _buf[MAXDATASIZE];
	if ((numbytes = recv(sockfd, _buf, MAXDATASIZE-1, 0)) == -1) 
    { 
		perror("recv");
		exit(1);
    }
	(_buf)[numbytes] = '\0';
	memcpy(buf,_buf,(numbytes+1));
}

std::string getnext(std::string &cmd_queue)
{
	std::string cmd=cmd_queue.substr(0,cmd_queue.find_first_of('#'));
	cmd_queue=cmd_queue.substr(cmd_queue.find_first_of('#')+1);
	return cmd;
}


int audio_server_main(int porta) 
{ 
	signal(SIGINT,quitHandler);
	int audiofd,controlfd;
	
	struct sockaddr_storage their_addr; 
	socklen_t addr_size; 
	struct addrinfo hints, *res; 
	char buf[MAXDATASIZE];
	pipe_t *VLCPipe;
	
	///prenche a estrutura res (addrinfo)
	memset(&hints, 0, sizeof hints); 
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM; 
	hints.ai_flags = AI_PASSIVE;
	std::string porta_str;
	IntToStr(porta,porta_str);
	getaddrinfo(NULL, porta_str.c_str(), &hints, &res); 
	////////////////////////////////////////
	
	//abre o socket
         controlfd  = socket(res->ai_family, res->ai_socktype, 0); 
	///liga com o endereco
	bind(controlfd, res->ai_addr, res->ai_addrlen); 
	///escuta por novas conexoes
	listen(controlfd, 0xFFFF); 
	
	
	/////obtem a primeira conexao de entrada esperando por conexao
	addr_size = sizeof their_addr; 
	audiofd = accept(controlfd, (struct sockaddr *)&their_addr, &addr_size);
	close(controlfd);
	/////////////////////////////////////////////////
	
	
	//////////////////aqui se faz o parsing dos comandos de streaming
	bool quit=false;
	std::string cmd;
	std::string cmd_queue;
	std::string tmp;
	std::string Url;
//	atexit(closesocks);
	while (!quit)
    {
		cmd="#"; //torna a fila de comandos vazia.
		receive(audiofd,(char**)(&buf));	
		cmd=buf;
		cmd_queue+=cmd;

		///////////// filtra e executa os comandos recebidos
		while(!quit && cmd_queue.find_first_of('#')!=-1)
		{
			std::cout << "fila:"<<cmd_queue<<std::endl;
			cmd=getnext(cmd_queue);
			
			std::cout << "comando atual:"<<cmd << " - fila: "<< cmd_queue << std::endl;
			if (cmd=="quit")
			{

				//	quit=true;
				//	system("killall vlc");
				cmd="./dropvlc.sh ";
				IntToStr(porta-PORTA+10000,tmp);
				cmd+=tmp;
				system(cmd.c_str());
				std::cout << "saindo: " <<cmd << std::endl;
				fwrite("\3",1,1,VLCPipe);
				exit(0);
			}
			
			if (cmd=="stream")
				{
				Url="http://staging.nanogames.com.br:9";
				tmp="";
				IntToStr(connectors,tmp);
				if (connectors<100)
				  Url+="0";
				if (connectors<10)
				  Url+="0";
				Url+=tmp;		
				receive(audiofd,(char**)(&buf));	
				cmd=buf;
				cmd_queue+=cmd;			
				VLCPipe=Stream(getnext(cmd_queue).c_str());
				send(audiofd,Url.c_str(),Url.length(),0);
				std::cout << "stream started:"<<Url << std::endl;
				}
		}
		//////////////////////////////////////////////
    }
    
	//////////////////////fecha o socket
	close(audiofd); 
    
	return 0; 
} 



int main(int argc, char *argv[]) 
{ 
	signal(SIGINT,quitHandler);
	struct sigaction sa; 
	sa.sa_handler = sigchld_handler; // reap all dead processes 
	sigemptyset(&sa.sa_mask); 
	sa.sa_flags = SA_RESTART; 
	if (sigaction(SIGCHLD, &sa, NULL) == -1) { 
	  perror("sigaction"); 
	  exit(1); 
	} 


	connectors=0; //esquema ingenuo - TODO: Mudar
	
	struct sockaddr_storage their_addr; 
	std::string porta_str;
	socklen_t addr_size; 
	struct addrinfo hints, *res; 
	char buf[MAXDATASIZE];
//	while (true)
//	{
	///prenche a estrutura res (addrinfo)
	memset(&hints, 0, sizeof hints); 
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM; 
	hints.ai_flags = AI_PASSIVE;
	getaddrinfo(NULL, PORTA_ENTRADA, &hints, &res); 
	////////////////////////////////////////
	
	//abre o socket
	sockfd = socket(res->ai_family, res->ai_socktype, 0); 
	///liga com o endereco
	bind(sockfd, res->ai_addr, res->ai_addrlen); 
	///escuta por novas conexoes
	int yes=1; 
	//char yes='1'; // Solaris people use this 
	// lose the pesky "Address already in use" error message
	//Beej's Guide to Network Programming 20 
	if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) { 
	perror("setsockopt"); 
        exit(1); 
        } 
        	
	std::cout << "escutando" << std::endl;
	listen(sockfd, 25); 
	
	
	/////obtem a primeira conexao de entrada esperando por conexao
	addr_size = sizeof their_addr; 
	while (true)
	{
	new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);
	connectors++;
//	close(sockfd);
	receive(new_fd,(char**)(&buf));	
	std::string cmd=buf;
	if (!strcmp(buf,"nanogames-ggplayer"))
	  {
	    
	    cmd="staging.nanogames.com.br#";
	    if (send(new_fd,cmd.c_str(),cmd.length(),0)==-1)
	    perror("send1");
	    porta_str="";
	    IntToStr(PORTA+connectors,porta_str);
	    if (send(new_fd,porta_str.c_str(),porta_str.length(),0)==-1)
	     perror("send2");
	    std::cout << "sent "<<cmd<<" "<<porta_str <<"="<<PORTA<<"+"<<connectors<< std::endl;
	    close(new_fd);

	
	    if (!fork())  audio_server_main(PORTA+connectors);

 
	  }	
	  
	  //close(new_fd);
	  //close(sockfd);
	  }
	  
	close(new_fd);
	close(sockfd);
	return 0;
} 



