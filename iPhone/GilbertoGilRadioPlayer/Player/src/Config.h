/*
 *  Config.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/19/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H 


///odeio incluir essas coisas aqui, mas UpdatableView usa isso...
#include <string>
//----------------------------MACROS------------------------------------------------------
#define DEFAULT_ANIMATION_INTERVAL 1.0f/32.0f
//----------------------------------------------------------------------------------------

//----------------------------ENUMS-----------------------------------------------------
////telas
enum {screen_UNDEFINED=-1,screen_INTRO,screen_MENU,screen_DISCO,screen_RSS,screen_PHOTOS,screen_RADIO,screen_TWITTER,screen_BIO, screen_TEXT, screen_ABOUT, screen_PLAYLISTS, screen_AGENDA };
///estados de player de audio
enum PlayStateEnum {PlayState_INVALID=-1,PlayState_PREVIOUS, PlayState_STOP,PlayState_PLAY,PlayState_PAUSE,PlayState_NEXT,PlayState_STOPPED,PlayState_FORCESTOP};
///modos de execucao de musica
enum PlayerModeEnum { PlayerMode_NORMAL, PlayerMode_SHUFFLE, PlayerMode_REPEAT_SONG, PlayerMode_REPEAT_LIST }; 
///razoes para a saida do sistema
enum QuitReasons{NORMAL_EXIT,ERROR_ALLOCATING_VIEWS,SOUND_SYSTEM_INIT_ERROR};
//----------------------------------------------------------------------------------------

//----------------------------TYPEDEFS----------------------------------------------------
///tipo de identificador de visao do sistema
typedef int ViewId;
//----------------------------------------------------------------------------------------

//----------------------------CONSTANTES----------------------------------------------------
///usa servidor de audio dedicado?
const bool CONSTusingDedicatedAudioServer=false;

const float CONSTradioUpdateRate = 1.0f;
///<nomes das colecoes>
const std::string CONSTalbumsCollection="discs";
const std::string CONSTphotosCollection="photos";
const std::string CONSTworksCollection="works";
const std::string CONSTcrewMembersCollection="crewmembers";
const std::string CONSTcrewPartiesCollection="crewparties";
const std::string CONSTtextsCollection="texts";
const std::string CONSTtextGroupsCollection="textgroups";
const std::string CONSTsealsCollection="seals";
const std::string CONSTpublishersCollection="publishers";
const std::string CONSTmusicsCollection="musics";
const std::string CONSTplaylistsCollection="playlists";
const std::string CONSTconfsCollection="settings";
///</nomes das colecoes>


///nome do diretorio de informacoes em cache
const std::string CONSTcachedFilesDir="cached";
///nomes dos diretorios de fotos gerais
const std::string CONSTgeneralPhotos="generalphotos";
///nomes dos diretorios de fotos de capas de discos
const std::string CONSTdiscoPhotos="discophotos";
///nomes da playlist padrao
const std::string CONSTdefaultPlaylist="default-playlist";


///URL servidor de audio
//const std::string CONSTaudioServerURL="http://www.gilbertogil.com.br/upload/discografia_1/audio/";
//const std::string CONSTaudioServerURL="http://192.168.1.178:8080/";  ///maquina local (macintosh)
//const std::string CONSTaudioServerURL="http://staging.nanogames.com.br:8080/"; ///staging
//const std::string CONSTaudioServerURL="http://66.7.217.220:8090/";  ///maquina producao
const std::string CONSTaudioServerURL="http://66.7.217.220/";  ///maquina producao

///"senha" identificadora do cliente para o servidor de audio
const std::string CONSTdedicatedAudioServerPassphrare="NANOGAMES";
///comando HTTP de stream
const std::string CONSTdedicatedAudioServerStreamCmd="STREAM";
///twitter RSS URL:
///(twitter do gilberto gil)
const std::string CONSTtwitterAccountRSSURL="http://twitter.com/statuses/user_timeline/14907774.rss";

///URL do XML de biografia
const std::string CONSTbiographyURL="http://gilbertogil.com.br/app_gil/info_lista_biografia.php";

///URL do RSS de noticias
const std::string CONSTnewsfeed="http://gilbertogil.com.br/app_gil/info_lista_noticias.php";

//URL da agenda
const std::string CONSTagendaURL = "http://gilbertogil.com.br/app_gil/info_agenda.php";

const std::string CONSTupdatingTwitterText="Atualizando...";
/*
 200.142.114.170 - producao
 //[requestUrl appendString:@"http://staging.nanogames.com.br:8090/"]; ///staging
 //[requestUrl appendString:@"http://www.gilbertogil.com.br/upload/discografia_1/audio/"]; ///refazenda sem servidor de audio dedicado
 //[requestUrl appendString:@"http://192.168.1.178:8080/"];  ///maquina local (macintosh)
 */

//----------------------------------------------------------------------------------------
#endif CONFIG_H