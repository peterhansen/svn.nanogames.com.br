//
//  AgendaView.h
//  Player
//
//  Created by Daniel Monteiro on 7/28/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"
#import "SingleTapEnabledScrollView.h"


@interface AgendaView : BaseGilbertoGilView < UIWebViewDelegate > {
	BOOL updated;
	///<InterfaceBuilder>
	/// saida do RSS
	GDataXMLDocument *doc;
    IBOutlet UIWebView *output;
}


///</InterfaceBuilder>


///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildAgendaView;
///</ciclo de vida>

- (void) updateContent;
- ( void ) updateUI;

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)webViewDidStartLoad:(UIWebView *)webView;

@end
