/*
 *  Text.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include <string>

#ifndef TEXT_H
#define TEXT_H

class CText
	{
	public:
		///typdefs:
		typedef int TextIdType;
		///object lifecycle:
		CText();
		~CText();
		///get methods:
		std::string getName();
		std::string getBody();
		std::string getAuthor();
		///misc methods:
		///query methods:
	private:
		///fields:
		std::string Name;
		std::string Body;
		std::string Author;
	};
#endif
