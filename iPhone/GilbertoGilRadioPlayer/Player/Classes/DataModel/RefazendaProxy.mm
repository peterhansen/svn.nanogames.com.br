#warning  pegar o ano do disco: http://gilbertogil.com.br/app_gil/info_disco.php?id=1

/// C++
#include <iostream>
#include <sstream>
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#include "Config.h"
#import "HTTPBridge.h"
#import "PlayerAppDelegate.h"

/// Proxy
#include "Tweet.h"
#include "RefazendaProxy.h"

#import "TwitterView.h"

CRefazendaProxy::RefazendaProxyReference CRefazendaProxy::Instance=NULL;


/**************************************************************************************************
 * getter for the category of the collection
 ***************************************************************************************************/
CRefazendaProxy::RefazendaProxyReference  CRefazendaProxy::getInstance(std::string BasePath)
{
	
	
	if (!Instance)
		return Instance=new CRefazendaProxy(BasePath);
	else
		return Instance;				
}		

CRefazendaProxy::~CRefazendaProxy()
{
	delete CMMLDatabase::getInstance();
    [ doc autorelease ];
    this->items.clear();
}



void CRefazendaProxy::setCurrentAlbum(CMMLEntity *album)
{
	currentSong=NULL;
	currentAlbum=album;
}

CMMLEntity *CRefazendaProxy::getCurrentAlbum()
{
	return currentAlbum;
}


void CRefazendaProxy::setCurrentSong(CMMLEntity *song)
{
	currentSong=song;
	currentAlbum=NULL;
}

CMMLEntity *CRefazendaProxy::getCurrentSong()
{
	return currentSong;
}

/**************************************************************************************************
 * getter for the category of the collection
 ***************************************************************************************************/
CRefazendaProxy::CRefazendaProxy(std::string BasePath)
{
    ///assume que sim, a principio
	connectedToInternet = true;
	///<declaracoes>
	///MML
	CMMLCategory::MMLCategoryReference clref;
	CMMLEntity::MMLEntityReference playlist;
	///</declaracoes>
	
	
	///<configura o diretorio base para as colecoes>
#ifdef DEBUG    
	std::cout <<"base path:"<<BasePath <<std::endl;
#endif    
	CMMLDatabase::getInstance()->setBasePath(BasePath);
	///</configura o diretorio base para as colecoes>
	CSS = nil;
    doc = nil;
	currentAlbum=NULL;
	currentSong=NULL;
	///<configura as colecoes>
	
#ifdef DEBUG
	std::cout << "criando a colecao de fotos" << std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTphotosCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout << "criando a colecao de musicas" << std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTmusicsCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout <<"criando a colecao de tecnicos" << std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTcrewMembersCollection);
	addCategory(clref);	
	clref->internalize();
	
#ifdef DEBUG
	std::cout <<"criando a colecao de equipe tecnica"<<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTcrewPartiesCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout << "criando a colecao de playlists" << std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTplaylistsCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout <<"criando a colecao de textos" << std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTtextsCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout <<"criando a colecao de grupos de textos"<<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTtextGroupsCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout <<"criando a colecao de editoras"<<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTsealsCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout << "criando a colecao de publicadoras" <<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTpublishersCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout << "criando a colecao de obras" <<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTworksCollection);
	addCategory(clref);	
	clref->internalize();
	
	
#ifdef DEBUG
	std::cout <<"criando a colecao de albums" <<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTalbumsCollection);
	addCategory(clref);	
	clref->internalize();
	
    
#ifdef DEBUG
	std::cout <<"criando a colecao de configuracoes" <<std::endl;
#endif
	
	clref=new CMMLCategory();
	clref->setCategory(CONSTconfsCollection);
	addCategory(clref);	
	clref->internalize();
	
	///</configura as colecoes>
	
	///<inicializa a playlist>
	clref=getCategoryByName(CONSTplaylistsCollection);
	try
	{
		playlist=clref->getEntityByKeyValue("nome",CONSTdefaultPlaylist);	
	}
	catch (MMLNonExistantEntity *ex)
	{		
#ifdef DEBUG        
		NSLog(@"criando playlyst...");
#endif        
		playlist=new CMMLEntity();
		playlist->setValueForKey("nome", CONSTdefaultPlaylist);
		playlist->setValueForKey("size", "0");
		clref->addEntity(playlist);
	}
	catch (std::exception ex)
	{
		throw ex;
	}
	///</incializa a playlist>
	
	lastRequestParam="";
	///<liberacoes>
	clref=NULL;
	playlist=NULL;
	///</liberacoes>    
}

/**************************************************************************************************
 * Indica que ocorreu um erro na requisição
 ***************************************************************************************************/
void CRefazendaProxy::onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr )
{
	
}

/**************************************************************************************************
 * Indica o progresso atual da requisição
 ***************************************************************************************************/
void CRefazendaProxy::onNetworkProgressChanged( int32 currBytes, int32 totalBytes )
{
	
}

/**************************************************************************************************
 * Indica que a requisição terminou com sucesso
 ***************************************************************************************************/
void CRefazendaProxy::onNetworkRequestCompleted( std::vector< uint8 >& data )
{
#ifdef DEBUG    
	NSLog(@"dados obtidos...decodificando");
#endif    
	///<declaracao de variaveis>
	///objc
	GDataXMLDocument *doc;
	NSString *tmp;		
	///cpp
	std::string Data;
	uint8 *buffer;
	MemoryStream dataStream;
	///</declaracao de variaveis>
	
	
	///obtem os dados brutos
	dataStream.swapBuffer( data );
	buffer=(uint8*)malloc(dataStream.getSize());
	dataStream.readData(buffer, dataStream.getSize());
	Data=(char*)buffer;
	Data=Data.substr(0,Data.find_last_of('>')+1);
	/////////////////////////////////////////////

	///monta a colecao de registros	
	///<inicializacoes>
	doc=[GDataXMLDocument alloc];	
	tmp=[NSString stringWithCString:(char*) Data.c_str() encoding:  /*NSISOLatin1StringEncoding*/NSWindowsCP1252StringEncoding ];	
	///</inicializacoes>		
	///preenche a arvore DOM com o XML vindo do servidor da Refazenda
	[doc initWithXMLString:tmp  options:0 error: nil ];	
	tmp=[[doc rootElement] name];
	
	{
		if ([ tmp compare:@"lista_album"  ]== NSOrderedSame)
			parseAlbumList([doc rootElement]);
		else
			if ([ tmp compare:@"lista_fotos"  ]== NSOrderedSame)
				parsePhotoList([doc rootElement]);
	}
	
	lastRequestParam="";	
	///<liberacoes>
	
	//[tmp autorelease];
	[doc autorelease];
	free(buffer);	
	
	
	///</liberacoes>
}

/**************************************************************************************************
 * Indica que a requisição foi cancelada pelo usuário
 ***************************************************************************************************/
void CRefazendaProxy::onNetworkRequestCancelled( void )
{
}


/**************************************************************************************************
 * Indica que a requisição terminou com sucesso
 ***************************************************************************************************/
void CRefazendaProxy::parseAlbumList(GDataXMLNode *Root)
{
	///objc
	NSString *nomeAlbum;
	NSString *idTrilha;
	NSString *nomeObra;	
	NSString *idObra;
	NSString *bonus;
    NSString *tempo;
	NSString *numero;
    NSString *idAlbum;
	NSString *obs;
	
    NSMutableString *filter;
	GDataXMLNode *obraField;
	NSArray *obraFields;
  	NSArray *personalFields;
   	NSArray *personFields;
	
	
	int track;
	///MML
	CMMLCategory::MMLCategoryReference playlists;
	CMMLEntity *playlist;
	CMMLCategory::MMLCategoryReference albums;
	CMMLEntity *album;	
	CMMLCategory::MMLCategoryReference musics;
	CMMLEntity *music;		
	CMMLCategory::MMLCategoryReference works;
	CMMLEntity *work;		
	CMMLCategory::MMLCategoryReference crewMembers;
	CMMLEntity *crewMember;
	CMMLCategory::MMLCategoryReference crewParties;
	CMMLEntity *crewParty;
    
	
	
	
	playlists=getCategoryByName(CONSTplaylistsCollection);
	playlist= playlists->getEntityByKeyValue("nome", CONSTdefaultPlaylist);		
	albums=getCategoryByName(CONSTalbumsCollection);	
	musics=getCategoryByName(CONSTmusicsCollection);
	works=getCategoryByName(CONSTworksCollection);
    crewMembers = getCategoryByName( CONSTcrewMembersCollection );
    crewParties = getCategoryByName( CONSTcrewPartiesCollection );
	
	///pequisa na arvore DOM com uma requisicao XPath que busca todos os nos nome, conforme diz a string.
	///para cada no "nome" dentro os nos retornados...	
	NSArray *albumNodes = [Root nodesForXPath:@"/lista_album/album/nome" error: nil ];			
    NSArray *faixas;
    
	for (GDataXMLElement *albumNode in albumNodes) 
	{
        //		NSLog(@"buscando nome %@", [[albumNode stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ]);
		track=0;
		try 
		{
			album=albums->getEntityByKeyValue("nome", [[[albumNode stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSWindowsCP1252StringEncoding]);
		}
		catch (MMLNonExistantEntity *ex)
		{
            //			NSLog(@"criando album");
			///obtem seu valor (que é o nome, que surpresa...)				
			nomeAlbum=[[albumNode stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
			///voltando na arvore vamos para /lista_album/album/trilha_audio
			faixas = [albumNode nodesForXPath:@"../faixas_do_album/faixa" error: nil ];	
			///criando uma entrada MML para o disco
			album=new CMMLEntity();
			album->setValueForKey("nome", [nomeAlbum cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSWindowsCP1252StringEncoding ]);
			///e finalmente adiciona o disco ao sistema de registros
			albums->addEntity(album);
            
        }
        obraFields=[albumNode nodesForXPath:@"../id_album" error: nil ];	
        obraField= [obraFields objectAtIndex:0];
        idAlbum = [ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
        
        album->setValueForKey("refazenda-id", [ idAlbum  cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );
        
        obraFields=[albumNode nodesForXPath:@"../gravadora" error: nil ];	
        obraField= [obraFields objectAtIndex:0];
        
        album->setValueForKey("gravadora",[ [ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSWindowsCP1252StringEncoding ]);            
        
        
        //			NSLog(@"gravadora:%s",album->getValueByKey("gravadora").c_str());
        
        obraFields=[albumNode nodesForXPath:@"../ano" error: nil ];	
        obraField= [obraFields objectAtIndex:0];
        
        album->setValueForKey("ano",[ [ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ]);            
        
        //			NSLog(@"ano:%s",album->getValueByKey("ano").c_str());
        
        
        obraFields=[albumNode nodesForXPath:@"../tem_texto" error: nil ];	
        obraField= [obraFields objectAtIndex:0];
        
        album->setValueForKey("texto",[ [ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ]);            
        
        //            album->setValueForKey("texto","true");            
        
        
        //			NSLog(@"texto:%s",album->getValueByKey("texto").c_str());
        
        
        ///para cada faixa no album...
        for (GDataXMLElement *trilha_node in faixas) 
        {
            ////////////////////////////trilha_audio/////////////////////////////////
            obraFields=[trilha_node nodesForXPath:@"trilha_audio" error: nil ];					
            obraField=[obraFields objectAtIndex:0];
            idTrilha=[ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            if ([idTrilha length]!=0)
            {
                ///define uma tupla ("numdafaixa","nomedomp3") na entidade disco.
                album->setValueForKey(ItoA(track).c_str(), [idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);				
            }
            //////////////////////////////////////////////////////////////////
            
            
            ////////////////////////////numero/////////////////////////////////
            obraFields=[trilha_node nodesForXPath:@"numero" error: nil ];					
            
            obraField=[obraFields objectAtIndex:0];
            numero=[ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            if ([idTrilha length]!=0)
            {
                ///define uma tupla ("numdafaixa","nomedomp3") na entidade disco.
                album->setValueForKey([numero cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding], [idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);				
            }
            //////////////////////////////////////////////////////////////////
            
            ////////////////////////////numero/////////////////////////////////
            obraFields=[trilha_node nodesForXPath:@"tempo" error: nil ];					
            
            obraField=[obraFields objectAtIndex:0];
            tempo=[ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            if ([idTrilha length]!=0)
            {
                ///define uma tupla ("numdafaixa","nomedomp3") na entidade disco.
                album->setValueForKey([numero cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding], [idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);				
            }
            //////////////////////////////////////////////////////////////////                
            
            ////////////////////////////nome da obra/////////////////////////////////
            obraFields=[trilha_node nodesForXPath:@"bonus" error: nil ];					
            
            obraField=[obraFields objectAtIndex:0];
            bonus=[ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            ///por enquanto, ignora
            
            ////////////////////////////nome da obra/////////////////////////////////
            obraFields=[trilha_node nodesForXPath:@"obra/nome_obra" error: nil ];					
            obraField=[obraFields objectAtIndex:0];
            nomeObra=[ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
            
            
            //////////////////////////////obs//////////////////////////////////
            obraFields = [ trilha_node nodesForXPath:@"obra/obs" error: nil ];				
            obraField = [ obraFields objectAtIndex: 0 ];
            
            obs = [ [ obraField stringValue ] stringByTrimmingCharactersInSet:
                   [ NSCharacterSet whitespaceAndNewlineCharacterSet ] ];	
            
            filter = [ NSMutableString stringWithString: obs ];
            
            //////////////////////////////id da obra//////////////////////////////////
            obraFields=[trilha_node nodesForXPath:@"obra/id_obra" error: nil ];					
            obraField=[obraFields objectAtIndex:0];
            idObra=[ [obraField stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];			
            
            
            ///////////////////////////////cria obra, caso nao exista.////////////////////
            try 
            {
                work=works->getEntityByKeyValue("id-obra", [ idObra cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );
            }			
            catch (MMLBaseException *ex)
            {				
                work=new CMMLEntity();
                work->setValueForKey("id-obra", [idObra cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);		
                works->addEntity(work);
                
            }	
            
            
            work->setValueForKey("nome", [nomeObra cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ]);                                
            work->setValueForKey( "obs", [ filter cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );		
            
            ///////////////////////////////cria trilha, caso nao exista////////////////////
            try 
            {
                music=musics->getEntityByKeyValue("id-trilha", [idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);
            }			
            catch (MMLBaseException *ex)
            {				
                music=new CMMLEntity();
                music->setValueForKey("id-trilha", [idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);			
                music->setValueForKey("tempo", [ tempo cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );
                music->setValueForKey("id-album", [ idAlbum cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );
                music->setValueForKey("id-obra", [idObra cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);				                    
                musics->addEntity(music);
                
                
                ////pessoal da faixa////
                
                try {
                    crewParties->getEntityIdByKeyValue( "id-trilha", [ idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );                
                    
                } catch ( MMLNonExistantEntity *e ) {
                    
                    personalFields = [ trilha_node nodesForXPath:@"pessoas_info/pessoa" error: nil ];
                    
                    if ( [ personalFields count ] > 0 ) {
                        
                        crewParty = new CMMLEntity();
                        crewParties->addEntity( crewParty );
                        
                        
                        for ( GDataXMLElement *pessoa_node in personalFields ) {                        
                            
                            crewMember = new CMMLEntity();
                            
                            personFields = [ pessoa_node nodesForXPath:@"pessoa_nome" error: nil ];
                            
                            if ( [ personFields count ] > 0 )                                
                                crewMember->setValueForKey( "nome", [ [ [ ( ( GDataXMLElement* ) [ personFields objectAtIndex: 0 ] ) stringValue ] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );
                            
                            
                            personFields = [ pessoa_node nodesForXPath: @"funcao" error: nil ];
                            
                            if ( [ personFields  count ] > 0 )
                                crewMember->setValueForKey( "funcao", [ [ [ ( ( GDataXMLElement* ) [ personFields objectAtIndex: 0 ] ) stringValue ] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );
                            
                            
                            
                            crewMembers->addEntity( crewMember );
                            crewMember->setValueForKey( "id-worker", ItoA( crewMembers->getTotalItems() - 1 ) );
                            crewParty->setValueForKey( ItoA( crewParty->getTotalFields() ), crewMember->getValueByKey( "id-worker" ) );
                            
                        }                    
                        
                        
                        crewParty->setValueForKey("id-trilha", [ idTrilha cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding ] );                }
                    
                    
                }                    
            }
            track++;
        }
        //////
        {
            ///as musicas vem com indice comecando em 1...mas o resto do sistema foi feito
            ///com 0.
            int tmp= AtoI([numero cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);
            tmp--;
            numero=[NSString stringWithUTF8String:ItoA(tmp).c_str()];
        }
        album->setValueForKey("size",[numero cStringUsingEncoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding]);
        
	}
	works->commit();
	musics->commit();
	albums->commit();
    crewParties->commit();
    crewMembers->commit();
    
	playlists=NULL;
	playlist=NULL;
	album=NULL;		
    
#ifdef DEBUG    
	NSLog(@"tudo pronto com os discos!", albums->getTotalItems());
#endif    
}	

/**************************************************************************************************
 * Indica que a requisição terminou com sucesso
 ***************************************************************************************************/
void CRefazendaProxy::parsePhotoList(GDataXMLNode *Root)
{
	///objc
	NSString *tmp;
	//C++
	///MML
	CMMLCategory::MMLCategoryReference photoalbum;
	CMMLEntity *photo;		
	
	photoalbum=getCategoryByName(CONSTphotosCollection);		
	
	///pequisa na arvore DOM com uma requisicao XPath que busca todos os nos nome, conforme diz a string.
	///para cada no "nome" dentro os nos retornados...	
	NSArray *photonodearray = [Root nodesForXPath:@"/lista_fotos/foto" error: nil ];			
	NSArray *field;
	for (GDataXMLElement *photonode in photonodearray) 
	{		
		field=[photonode nodesForXPath:@"./id" error: nil ];	
		tmp=[[field objectAtIndex:0] stringValue];					
		tmp=[tmp stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];		
		tmp=[tmp stringByReplacingOccurrencesOfString:@"&quot;" withString:@"'" ];				
		if ([tmp length] > 250)
			tmp=[tmp substringToIndex:250];				
		
		try 
		{			
			photoalbum->getEntityIdByKeyValue("refazenda-id",[tmp UTF8String]);
			
		}
		catch (MMLNonExistantEntity *ex) 
		{
			///prepara o ponteiro, para que eu saiba se houver alguma falha
			photo=NULL;
			
			try 
			{
				photo=new CMMLEntity();
				photoalbum->addEntity(photo);
				
				//----------------------
				photo->setValueForKey("refazenda-id", [tmp UTF8String]);
				//----------------------				
				field=[photonode nodesForXPath:@"./ext_g" error: nil ];	
				tmp=[[field objectAtIndex:0] stringValue];				
				tmp=[tmp stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];		
				tmp=[tmp stringByReplacingOccurrencesOfString:@"&quot;" withString:@"'" ];							
				if ([tmp length] > 250)
					tmp=[tmp substringToIndex:250];				
				
				photo->setValueForKey("ext_g", [tmp UTF8String]);
				//----------------------				
				field=[photonode nodesForXPath:@"./ext_p" error: nil ];	
				tmp=[[field objectAtIndex:0] stringValue];				
				tmp=[tmp stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];		
				tmp=[tmp stringByReplacingOccurrencesOfString:@"&quot;" withString:@"'" ];							
				if ([tmp length] > 250)
					tmp=[tmp substringToIndex:250];				
				
				photo->setValueForKey("ext_p", [tmp UTF8String]);
				//----------------------	
				
				
			}
			catch (MMLBaseException *ex) 
			{
				///qualquer falha aqui é intolerável
				if (photo!=NULL)
					delete photo;
				throw;
			}
			
			///falhas toleráveis
			try 
			{
				field=[photonode nodesForXPath:@"./fotografo" error: nil ];	
				tmp=[[field objectAtIndex:0] stringValue];				
				tmp=[tmp stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
				tmp=[tmp stringByReplacingOccurrencesOfString:@"&quot;" withString:@"'" ];							
				
				if ([tmp length] > 250)
					tmp=[tmp substringToIndex:250];				
				
				photo->setValueForKey("fotografo", [tmp UTF8String]);
				
				field=[photonode nodesForXPath:@"./legenda" error: nil ];	
				tmp=[[field objectAtIndex:0] stringValue];				
				tmp=[tmp stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];		
				tmp=[tmp stringByReplacingOccurrencesOfString:@"&quot;" withString:@"'" ];							
				if ([tmp length] > 220)
				{
					NSString *tmp1=@"(erro ao obter legenda)";
					NSString *tmp2=[tmp substringToIndex:200];
					tmp=tmp1;
					tmp=[tmp stringByAppendingString:tmp2];
				}
				
				photo->setValueForKey("legenda", [tmp UTF8String]);
                //	NSLog(@"%d legenda %@",photo->getId(),tmp);
				
			}
			catch (MMLInvalidAttrib *ex) 
			{
				NSLog(@"excessao: %s",ex->what());
				continue;
			}
			catch (MMLBaseException *ex) 
			{
				///pra outra falha, nao se pode ser tolerante
				if (photo!=NULL)
					delete photo;
				throw;
				
			}
		}
	}	
	
	photoalbum->commit();
	photoalbum=NULL;
	photo=NULL;		
#ifdef DEBUG    
	NSLog(@"tudo pronto com as fotos!");	
#endif    
}


std::vector<Tweet * > &CRefazendaProxy::getTweets() {
    return items;
}

void CRefazendaProxy::updateTwitterContent() {
    GDataXMLDocument *doc;
	GDataXMLNode *root;
	Tweet *t;
	NSString *urlContent;
	NSString *rawContent;	
	NSError *error;
	NSString *fullPathToFile;
	NSURL *url;
	NSArray *nodeItems;
    
    for ( int c = 0; c < items.size(); ++c ) {
        delete items[ c ];
    }
    
    items.clear();
    
	
	fullPathToFile=[ NSString stringWithFormat:@"%@cached/%s", [ PlayerAppDelegate getDataDir ], "twitter" ];
	
	
    
	
	rawContent = [NSString stringWithContentsOfFile:fullPathToFile encoding:/*NSISOLatin1StringEncoding*/NSUTF8StringEncoding error:&error];
	
	
	url=[NSURL URLWithString:[NSString stringWithUTF8String:CONSTtwitterAccountRSSURL.c_str()]];
	urlContent= [NSString stringWithContentsOfURL:url  encoding:/*NSISOLatin1StringEncoding*/NSUTF8StringEncoding error: &error];
    
	if (urlContent!=nil && [urlContent length] != 0)
	{
		rawContent=urlContent;
		[rawContent writeToFile:fullPathToFile atomically:NO encoding:/*NSISOLatin1StringEncoding*/NSUTF8StringEncoding error: nil ];
	}
	
	if (rawContent!=nil && [rawContent length] != 0)
	{
		doc=[GDataXMLDocument alloc];	
		[doc initWithXMLString:rawContent  options:0 error: nil ];	
		
		root=[doc rootElement];
		nodeItems = [root nodesForXPath:@"/rss/channel/item" error: nil ];			
		NSString *filteredText;
		for (GDataXMLElement *item in nodeItems) 
		{
            filteredText = [ TwitterView filterText:
                            [ ( ( GDataXMLElement * ) [ [ item nodesForXPath:@"description" error: nil ] objectAtIndex:0 ] ) stringValue ] ];
            t = new Tweet("gilbertogil", 
                          "Gilberto Gil", 
                          [ 
                           filteredText
                           cStringUsingEncoding:NSUTF8StringEncoding ], 
                          [ [ ( ( GDataXMLElement * ) [ [ item nodesForXPath:@"pubDate" error: nil ] objectAtIndex:0 ] ) stringValue ] cStringUsingEncoding:NSUTF8StringEncoding ]
                          );
            
            items.push_back( t );
            [ filteredText autorelease ];
		}
		
		[doc autorelease];
	}
	else
	{
        //[finalContent setString: @"conteudo indisponivel no momento"];		
	}
	
	if (urlContent==nil)
	{
		connectedToInternet = false;
	}
}


void CRefazendaProxy::updateRSSContent() {
    NSString *fullPathToFile=[ NSString stringWithFormat:@"%@cached/%s", [PlayerAppDelegate getDataDir], "noticias" ];
	GDataXMLNode *root;	
    GDataXMLDocument *myDoc;
	NSError *error;
	NSURL *url;	
	NSString *urlContent;
	NSString *rawContent;	
	NSArray *items;
    
	rawContent= [NSString stringWithContentsOfFile:fullPathToFile encoding:/*NSISOLatin1StringEncoding*/NSUTF8StringEncoding error:&error];
	
	url=[NSURL URLWithString:[NSString stringWithUTF8String:CONSTnewsfeed.c_str()]];		
	urlContent=[NSString stringWithContentsOfURL:url encoding: /*NSISOLatin1StringEncoding*/NSUTF8StringEncoding error:&error];	
	
	if (urlContent!=nil && [urlContent length ] != 0 )
	{
		rawContent=urlContent;
		[rawContent writeToFile: fullPathToFile atomically:NO encoding:/*NSISOLatin1StringEncoding*/NSUTF8StringEncoding error:&error];
	}
	
	if (rawContent != nil && [rawContent length ] != 0 ) 
	{
		myDoc = [GDataXMLDocument alloc];	
		[ myDoc initWithXMLString: rawContent options:0 error: nil  ];	
        doc = myDoc;     
	} else  {
        connectedToInternet = false;
    }
}

bool CRefazendaProxy::isConnectedtoInternet() {
    return connectedToInternet;
}

NSString *CRefazendaProxy::getStyle() {
    
    // ta quebrando, não sei porque...
    //    if ( CSS == nil ) {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)    
        CSS = [ NSString stringWithContentsOfFile: [ [ NSBundle mainBundle ] pathForResource: @"style_ipad" ofType: @"html" ] encoding: NSUTF8StringEncoding error: nil ];           
    else
        CSS = [ NSString stringWithContentsOfFile: [ [ NSBundle mainBundle ] pathForResource: @"style" ofType: @"html" ] encoding: NSUTF8StringEncoding error: nil ];       
    //    }
    
    return CSS;
}

GDataXMLDocument *CRefazendaProxy::getNewsDoc() {
    return doc;    
}