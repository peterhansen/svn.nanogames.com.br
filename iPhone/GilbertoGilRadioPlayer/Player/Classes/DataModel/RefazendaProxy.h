enum DefaultCollections{MUSIC,PHOTOS,TEXTS,CREWMEMBERS,PLAYLISTS,ALBUMS};

#include "MMLDatabase.h"
#include "INetworkListener.h"
#include "Tweet.h"

#ifndef REFAZENDAPROXY_H
#define REFAZENDAPROXY_H

class CRefazendaProxy:public CMMLDatabase, public INetworkListener
{
public:
	typedef CRefazendaProxy* RefazendaProxyReference;
	///class lifecycle
	CRefazendaProxy(std::string BasePath="./Documents/");
	~CRefazendaProxy();
	///sigleton
	static RefazendaProxyReference getInstance(std::string BasePath="./Documents/");
	
	// Indica que ocorreu um erro na requisição
	virtual	void onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr );
	
	// Indica o progresso atual da requisição
	virtual void onNetworkProgressChanged( int32 currBytes, int32 totalBytes );
	
	// Indica que a requisição terminou com sucesso
	virtual void onNetworkRequestCompleted( std::vector< uint8 >& data );
	
	// Indica que a requisição foi cancelada pelo usuário
	virtual void onNetworkRequestCancelled( void );
	
	/// operações de parsing dos dados vindos do servidor da refazenda
	
	// parseia a lista de discos
	void parseAlbumList(GDataXMLNode *Root);
	
	// parseia a lista de fotos
	void parsePhotoList(GDataXMLNode *Root);
	
	// album atual sendo visto
	void setCurrentAlbum(CMMLEntity *album);
	CMMLEntity *getCurrentAlbum();

	// musica atual sendo vista
	void setCurrentSong(CMMLEntity *song);
	CMMLEntity *getCurrentSong();
    
    NSString *getStyle();
	
    std::vector<Tweet * > &getTweets();
    void updateTwitterContent();	
    void updateRSSContent();	
    GDataXMLDocument *getNewsDoc();
    bool isConnectedtoInternet();
private:
    GDataXMLDocument *doc;
    bool connectedToInternet;
	static RefazendaProxyReference Instance;
	std::string lastRequestParam;
	CMMLEntity *currentAlbum;
	CMMLEntity *currentSong;	
    std::vector<Tweet * > items;
    NSString *CSS;
};





#endif
