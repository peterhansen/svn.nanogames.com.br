/*
 *  MMLCollection.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

#include <string> 
#include <iostream>
#include <fstream>
#include <sstream> 
#include <vector>
#include <map>


#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/**************************************************************************************************
* Constructor 
***************************************************************************************************/

CMMLCategory::CMMLCategory():myId(-1)
{

}

/**************************************************************************************************
 * Destructor 
 ***************************************************************************************************/

CMMLCategory::~CMMLCategory()
{
  for (int c=0;c<getTotalItems();c++)
    delete entities[c];
}


/**************************************************************************************************
 * getter for the category of the collection
 ***************************************************************************************************/

const std::string CMMLCategory::getCategory()
{
	return category;
}

/**************************************************************************************************
 * get entity by its ID;
 ***************************************************************************************************/

CMMLEntity* CMMLCategory::getEntityById(CMMLEntity::EntityIdType id)
{
	if (getTotalItems()==0)
		throw new MMLNonExistantEntity();
	
	if (id < 0 || id >= getTotalItems())
		throw new MMLInvalidEntityId();
		
	return entities[id];
}

/**************************************************************************************************
 * set the collection's category
 ***************************************************************************************************/
void CMMLCategory::setCategory(const std::string &Category)
{
	category=Category;
#ifdef DEBUG
	std::cerr << "creating category:"<<category<<std::endl;
#endif
}


/**************************************************************************************************
 * Add an entity to this category
 ***************************************************************************************************/
EntityIdType CMMLCategory::addEntity( CMMLEntity* entity)
{
	if (entity==NULL)
		throw new MMLNonExistantEntity();
	
	if (entity->getId()!= -1)	
		throw new MMLEntityAlreadyCategorized();
	
  if (entity->getTotalFields()==1)
	{
	bool duplicate=false;
	try 
		{
		  CMMLEntity::MMLFieldConstIterator it = entity->getConstIterator();
		  std::string key=it->first;
		  std::string value=it->second;
		  getEntityByKeyValue(key,value);
  		  duplicate=true;
		}
	catch (MMLNonExistantEntity *e)
		{
 		  duplicate=false;
		}
  	if (duplicate)
  		throw new MMLDuplicateTrivialEntity();
	}
  entity->setCategory(myId);
  entity->setId(getTotalItems());
  entities.push_back(entity);
  return entity->getId();
}


/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/
int CMMLCategory::getTotalItems()
{
  return entities.size();
}

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/
void CMMLCategory::internalize()
{
	NSFileManager *filemgr;
	NSArray *filelist;
	NSArray *dirPaths;
	NSString *docsDir;
	NSString *catDir;
	NSString *filename;
	int count;
	std::ifstream *stream;
	std::string fullpath;
	CMMLEntity::MMLEntityReference entity;	
	NSError *error=[[NSError alloc] init];	
	filemgr =[NSFileManager defaultManager];	
	dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);	
	docsDir = [dirPaths objectAtIndex:0];	
	docsDir=[docsDir stringByAppendingString:@"/"];
	catDir = [docsDir stringByAppendingPathComponent:[NSString stringWithUTF8String:getCategory().c_str()]];
 	NSArray *dirs=[filemgr directoryContentsAtPath:docsDir];
	
	if ([dirs indexOfObject:[NSString stringWithUTF8String:getCategory().c_str()]] == NSNotFound)
	{
		if ([filemgr createDirectoryAtPath:catDir withIntermediateDirectories:YES attributes:nil error:&error] == NO)
		{
			NSLog(@"falhou pra criar o diretorio");
			NSLog([error localizedDescription]);
			exit(0);
		}
	}
	
	
	filelist = [filemgr directoryContentsAtPath:[catDir stringByAppendingString:@"/"]];
	
	
    count = [filelist count];
#ifdef DEBUG    
	NSLog([NSString stringWithFormat:@"total in:%d",count]);
#endif    
	for (int  i = 0; i < count; ++i)
	{
		filename=[catDir stringByAppendingString:@"/"];
//		filename=[filename stringByAppendingString:[NSString stringWithFormat:@"%d" ,i]];
		filename=[filename stringByAppendingString:[filelist objectAtIndex:i ]];		
//		NSLog(filename);
		entity=new CMMLEntity();		
		stream=new std::ifstream([filename UTF8String]);
		if (stream->good())
		{
			try 
			{
				entity->initFromStream(*stream);
				addEntity(entity);		
				delete stream;						
			}
			catch (MMLBadStreamForInput *ex) 
			{
				delete stream;
				delete entity;
//#ifdef DEBUG
				std::cerr << " MMLBadStreamForInput exception:"<<ex->what()<<" with "<< category <<"::"<<i<<std::endl;
//#endif
				
				continue;
			}
//#ifdef DEBUG			
			catch (MMLBaseException *ex) 
			{

				std::cerr << " general exception:"<<ex->what()<<" with "<< category <<"::"<<i<<std::endl;				
			}
//#endif			
		}
	}
	
	[ error autorelease ];
    [filemgr release];
}



/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/
const EntityIdType CMMLCategory::getEntityIdByKeyValue(const std::string key,const std::string value)
{

  int totalitems=getTotalItems();
  for (int c=0;c<totalitems;c++)
    {
      try
      	{
		if (((CMMLEntity*)entities[c])->getValueByKey(key)==value)
		  return c;
	    }
    
    	catch (MMLNonExistantFieldQueried exception)
		{
			//nada
		}
    }
  throw  new MMLNonExistantEntity();
}


/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/
CMMLEntity* CMMLCategory::getEntityByKeyValue(const std::string key,const std::string value)
{
  return getEntityById(getEntityIdByKeyValue(key,value));
}

/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/

CMMLCategory::CategoryIdType CMMLCategory::getId()
{
  return myId;
}

/**************************************************************************************************
 * get the count of items in this category
 ***************************************************************************************************/

void CMMLCategory::setId(CategoryIdType id)
{
  myId=id;
}

void CMMLCategory::removeEntity( CMMLEntity *deleteEntity ) {    
    std::vector<CMMLEntity *>::iterator it;
	CMMLEntity::MMLEntityReference entity = NULL;
    
    it = entities.begin();
    
    while ( it != entities.end() ) {

        if ( *it == deleteEntity) {
            entity = *it;
            break;
        }
            
        
        ++it;
    }
    
    if ( entity == NULL )
        return;
    
   
    entities.erase( it );    
      
   
}

/**************************************************************************************************
 * 
 ***************************************************************************************************/

void CMMLCategory::commit()
{
	std::string fullpath="";
	std::string filename="";
	CMMLEntity::MMLEntityReference entity;
	CMMLEntity::MMLFieldConstIterator it;
	std::ofstream *stream;
	
	//	fullpath+=CMMLDatabase::getInstance()->getBasePath();
	//	fullpath+="/";
	
	NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);	
	NSString *docsDir = [dirPaths objectAtIndex:0];	
	docsDir=[docsDir stringByAppendingString:@"/"];
	fullpath=[docsDir UTF8String];
	
	fullpath+=getCategory();
	int namelen;
    
    
    NSArray *content = [ [ NSFileManager defaultManager ] contentsOfDirectoryAtPath: [ NSString stringWithUTF8String: fullpath.c_str() ]  error: nil ];
    
    for ( NSString *file in content ) {
        
//        NSLog(@"delete: %@", [ NSString stringWithFormat:@"%s/%@", fullpath.c_str(), file ] );
        
        if ( [ [ NSFileManager defaultManager ] removeItemAtPath: [ NSString stringWithFormat:@"%s/%@", fullpath.c_str(), file ] error: nil ] == NO ) {
            NSLog(@"no good!");
        }
        
    }
#ifdef DEBUG    
    NSLog(@"size depois: %d filename: %s ", ((int) entities.size()), filename.c_str() );
#endif    
    
	for (int c=0;c<getTotalItems();++c)
    {
		entity=getEntityById(c);		
		filename=fullpath;
		filename+="/";
		///eh mais seguro nao confiar que o Id interno ainda corresponda ao Id dentro da colecao.
		namelen=ItoA(entity->getId()).length();
		for (int d=0;d<(TRAILINGZEROES-namelen);d++)
			filename+="0";	
		
		filename+=ItoA(entity->getId());
//		std::cout << "commiting "<<filename << std::endl;
		stream=new std::ofstream(filename.c_str());
		if (stream->bad())
		{
			NSLog(@"bad stream for output");
			throw new MMLBadStreamForInput();
		}
		
		it = entity->getConstIterator();
		while (it!=entity->getConstEndIterator())
		{      
			*stream << it->first << "=" << it->second<<";"<<std::endl;
			it++;
		}
		*stream<<"~";
//		std::cout << "~"<< std::endl;
		stream->flush();
		stream->close();
		delete stream;     
        

  
	}
}
