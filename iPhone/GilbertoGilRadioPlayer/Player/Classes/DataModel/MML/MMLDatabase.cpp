/*
 *  MMLDatabase.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */


/*
 void setBasePath(std::string basepath);
 std::string getBasePath();
 */

#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>

#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLDatabase.h"

CMMLDatabase::MMLDatabaseReference CMMLDatabase::instance=NULL;	


/**************************************************************************************************
 * getter for the category of the Category
 ***************************************************************************************************/
CMMLDatabase::MMLDatabaseReference  CMMLDatabase::getInstance()
{
	if (!instance)
		return instance=new CMMLDatabase();
	else
		return instance;				
}		


/**************************************************************************************************
 * Destructor
 ***************************************************************************************************/

CMMLDatabase::~CMMLDatabase()
{

}

/**************************************************************************************************
 * Constructor
 ***************************************************************************************************/

CMMLDatabase::CMMLDatabase()
{
	setBasePath("./");
}

/**************************************************************************************************
 * setBasePath - set the root for all the categories
 ***************************************************************************************************/

void CMMLDatabase::setBasePath(const std::string &path)
{
	basePath=path;
}

/**************************************************************************************************
 * getBasePath - get the root for all the categories
 ***************************************************************************************************/

const std::string CMMLDatabase::getBasePath()
{
	return basePath;
}



/**************************************************************************************************
 * getBasePath - get the root for all the categories
 ***************************************************************************************************/

int CMMLDatabase::getTotalItems()
{
  return category.size();
}


/**************************************************************************************************
 * getBasePath - get the root for all the categories
 ***************************************************************************************************/

CMMLCategory::MMLCategoryReference CMMLDatabase::getCategoryById(CMMLCategory::CategoryIdType id)
{
  if (id <0 || id > getTotalItems())
	throw new MMLNonExistantCategory();


  return category[id];
}

/**************************************************************************************************
 * getBasePath - get the root for all the categories
 ***************************************************************************************************/

CMMLCategory::MMLCategoryReference CMMLDatabase::getCategoryByName(const std::string name)
{
  return getCategoryById(getCategoryIdByName(name));
}

/**************************************************************************************************
 * getBasePath - get the root for all the categories
 ***************************************************************************************************/

CMMLCategory::CategoryIdType CMMLDatabase::getCategoryIdByName(const std::string name)
{
  int totalitems=getTotalItems();
  for (int c=0;c<totalitems;c++)
    if (category[c]->getCategory()==name)
      return c;

    throw  new MMLNonExistantCategory();
}


/**************************************************************************************************
 * getBasePath - get the root for all the categories
 ***************************************************************************************************/

void CMMLDatabase::addCategory(CMMLCategory::MMLCategoryReference ref)
{
	if (ref==NULL)
		throw new MMLNonExistantCategory();
		
    if (ref->getId()!=-1)
  		throw new MMLCategoryAlreadyManaged();  
  		
    if (ref->getCategory()=="" || ref->getCategory()==" " )
		throw new MMLNonExistantCategory();
  		
    try 
	    {
    	getCategoryIdByName(ref->getCategory());
    	}
    catch (MMLNonExistantCategory *ex)
    	{
    		//significa que esta tudo bem.
			ref->setId(category.size());
			category.push_back(ref);    
			return;	
    	}
    throw new MMLCategoryAlreadyManaged();  
}


void CMMLDatabase::cleanUp()
		{
		category.clear();			
		}

