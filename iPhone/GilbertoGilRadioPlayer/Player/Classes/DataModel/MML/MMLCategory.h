/*
 *  MMLCollection.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */


#ifndef MMLCATEGORY_H
#define MMLCATEGORY_H

class CMMLEntity;

class CMMLCategory
	{
	public:
		///typedefs:
		typedef int CategoryIdType;		
		typedef CMMLCategory* MMLCategoryReference;
		//object lifecycle:
		CMMLCategory();
		~CMMLCategory();
		
		///get methods:
		int getTotalItems();
		const std::string getCategory();
		CMMLEntity* getEntityById(EntityIdType id);
		CMMLEntity* getEntityByKeyValue(const std::string key,const std::string value);
		const EntityIdType getEntityIdByKeyValue(const std::string key,const std::string value);
		CategoryIdType getId();
		///set methods:
		void setId(CategoryIdType id);
		void setCategory(const std::string &category);		
		///misc methods:
		EntityIdType addEntity(CMMLEntity *entity);
		void internalize();
		void commit();
        void removeEntity( CMMLEntity *entity );
	private:
		CategoryIdType myId;
		std::string category;
		std::vector<CMMLEntity *> entities;
	};
#endif
