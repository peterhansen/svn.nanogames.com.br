/*
 *  MMLEntity.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
 
#include <string> 
#include <iostream>
#include <sstream> 
#include <vector>
#include <map>
 
#include "MMLConstants.h"
#include "MMLUtils.h"
#include "MMLCategory.h"
#include "MMLEntity.h"

/**************************************************************************
 * Generic constructor. Just zero-init everything. All the initialization takes place after reading from disk
 *
 ******************************************************************************/

CMMLEntity::CMMLEntity():Id(-1), CategoryId(-1)
{

}

/********************************************************************************
 * Default destructor. Destroy everything and zero-set it.
 * Destroy also the objects from the collec#include <MMLCollection.h>tion, since it would be orphaned
 *********************************************************************************/

CMMLEntity::~CMMLEntity()
{
	CategoryId=0;
}


/******************************************************************************
* getValueByKey - get the value of a field based on the fields name
**********************************************************************************/

const std::string CMMLEntity::getValueByKey(const std::string &key)
{
	return fields[key];
}


/***************************************************************************
 * getFieldIndexByName - get the field index by its name
 *************************************************************************/
bool CMMLEntity::isFieldValid(const std::string &fieldname)
{	
	Fields::iterator it= fields.find(fieldname);
	if (it==fields.end())
		return false;
		
	return true;
}


/***************************************************************************
 * getTotalFields - get the total number of stored fields
 *************************************************************************/
CMMLEntity::MMLFieldConstIterator CMMLEntity::getConstIterator()
{
	return fields.begin();
}

CMMLEntity::MMLFieldConstIterator CMMLEntity::getConstEndIterator()
{
	return fields.end();
}


/***************************************************************************
 * getTotalFields - get the total number of stored fields
 *************************************************************************/
int CMMLEntity::getTotalFields()
{
	return fields.size();
}

/***************************************************************************
 * setValueForKey - set the current value of a field, by its key
 *************************************************************************/
void CMMLEntity::setValueForKey(const std::string &key,const std::string &value)
{
	fields[ key ]=value;
}


/***************************************************************************
 * setValueForIndex - set the current value of a field, by its index
 *************************************************************************/
void CMMLEntity::initFromStream(std::istream &i_stream)
{
	///markers for the end of the tokens, actually
	int fieldnameindex=-1;
	int valueindex=-1;
	std::string current="dummy";
	char *buffer=(char*)malloc(255*sizeof(char));
	std::string key;
	std::string value;
	
	
	if (!i_stream.good())
	  {
#ifdef DEBUG
	    std::cout << "ERROR: bad stream for input"<< std::endl;
#endif
		throw new MMLBadStreamForInput();
	  }
	
	while (i_stream.good() && ( current.length()>0 &&  current[0]!=ENTITY_EOF_MARKER))
		{
			
		  i_stream.getline(buffer,255);		  
		  current=buffer;

		  if (current.length()==0 || current[0]==ENTITY_EOF_MARKER)
		    continue;
			
			fieldnameindex= current.find_first_of(ENTITY_ATTRIB_MARKER);
			valueindex= current.find_last_of(ENTITY_EOL_MARKER);
			
			if (fieldnameindex<=0 || fieldnameindex > (int) current.length() ||
				valueindex <=0 || valueindex > (int)current.length())
			{
			
//#ifdef DEBUG
					std::cout << "ERROR: invalid lvalue=rvalue fieldnameindex="<< fieldnameindex <<", valueindex="<<valueindex<<", current="<<current<<", current.length()="<<current.length()<<", buffer='" << buffer <<"'" << std::endl;
//#endif
              		free(buffer);				
					throw new MMLInvalidAttrib();
			}
			

			key=current.substr(0,fieldnameindex);
		
			if (key=="")
			{
				free(buffer);			       
#ifdef DEBUG
				
				std::cout << "ERROR: key name not found"<< std::endl;
#endif
				throw new MMLEmptyKeyName();
			}
			
			value=current.substr(fieldnameindex+1,(valueindex-fieldnameindex-1));
			
#ifdef DEBUG
			std::cout << "pair:"<<key<<"="<<value<<std::endl;
#endif
			fields[key]=value;
		}	
	free(buffer);
}

/***************************************************************************
 * setValueForKey - set the current value of a field, by its key
 *************************************************************************/
void CMMLEntity::setId(EntityIdType id)
{
  Id=id;
}


/***************************************************************************
 * setValueForKey - set the current value of a field, by its key
 *************************************************************************/
CMMLEntity::EntityIdType CMMLEntity::getId()
{
  return Id;
}

/***************************************************************************
 * setValueForKey - set the current value of a field, by its key
 *************************************************************************/

CMMLCategory::CategoryIdType CMMLEntity::getCategory()
{
  return CategoryId;
}

/***************************************************************************
 * setValueForKey - set the current value of a field, by its key
 *************************************************************************/

void CMMLEntity::setCategory(CMMLCategory::CategoryIdType id)
{
  CategoryId=id;
}


bool CMMLEntity::equal(MMLEntityReference other)
{
	///se o outro e' nulo
	if (other==NULL)
		throw new MMLNonExistantEntity();
		
	///se nao tenho campos
	if (getTotalFields()==0)
		{
			///mas o outro tambem nao tem, entao somos iguais
			if (other->getTotalFields()==0)	
				return true;
			else
			///senao, somos diferentes
				return false;
		}
		
	///confere campo a campo	
	Fields::iterator it=fields.begin();
	while (it!=fields.end())
	{
#ifdef DEBUG		
		std::cout << it->first << "=" << it->second << std::endl;
#endif		
		try 
			{
			if (getValueByKey(it->first)!=other->getValueByKey(it->first))
				return false;
			}
		catch (MMLNonExistantFieldQueried e)
			{
				return false;
			}
		it++;	
	}
	///sao iguais
	return true;	
}

std::string CMMLEntity::dump()
{
	std::string toReturn="";
	toReturn+="object:"+ItoA(Id)+"\n";
	toReturn+="category:"+ItoA(CategoryId)+"\n";	
	toReturn+="fields:";
	toReturn+="\n";
	Fields::iterator it=fields.begin();
	do
	{
		toReturn += it->first;
		toReturn += "=";
		toReturn += it->second;
		toReturn += "\n";
		it++;	
	}
	while (it!=fields.end());
	
	toReturn+="~";
	
	return toReturn;
}
