/*
 *  CrewMember.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include <string>

#ifndef CREWMEMBER_H
#define CREWMEMBER_H

class CCrewMember
	{
	public:
		///typdefs:
		typedef int CrewIdType;
		///object lifecycle:
		CCrewMember();
		~CCrewMember();
		///get methods:
		const std::string getName();
		const std::string getFunction();
		///set methods:
		void setName(const std::string name);
		void setFunction(const std::string func);
	private:
		///fields:
		std::string Name;
		std::string Function;		
	};

#endif