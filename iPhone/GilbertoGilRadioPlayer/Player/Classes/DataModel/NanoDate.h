/*
 *  NanoDate.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

//#include "NanoTypes.h"

#ifndef NANODATE_H
#define NANODATE_H

///TODOO: this is a draft - please add all utility functions here.
///but lets not make this a new Java's GregorianCalleandar...Its PITA.
class NanoDate
	{
	public:
		///typedefs:
		typedef long BasicDateType;
		///object lifecycle:
		NanoDate();
		~NanoDate();
		///set methods:
		void setDateWithUnixDate(BasicDateType unixdate);		
		///get methods:
		BasicDateType getUnixDate();		
		///misc methods:
		const NanoDate Sum(const NanoDate &withthis);
		const NanoDate Subtract(const NanoDate &withthis);
		
		///query methods:
		bool sameYear(const NanoDate &ofthis);		
		bool sameMonth(const NanoDate &ofthis);
///	TODOO:	It would be nice to implement this
///		bool sameWeek(const NanoDate &ofthis);
		bool sameDay(const NanoDate &ofthis);
		///static methods:
		///TODOO: whats the best return type here?
		static const NanoDate Now();
	private:
		///fields:
		BasicDateType Date;
	};

#endif
