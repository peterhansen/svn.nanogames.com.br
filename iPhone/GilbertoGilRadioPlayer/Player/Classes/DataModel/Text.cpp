/*
 *  Text.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include "Text.h"

std::string CText::getName()
{
  return Name;
}


std::string CText::getBody()
{
  return Body;
}

std::string CText::getAuthor()
{
  return Author;
}
