/*
 *  Playlist.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include <iostream>
#include "MMLUtils.h"
#include "MMLEntity.h"
#include "MMLCollection.h"
#include "RefazendaProxy.h"
#include "Playlist.h"
#include "Music.h"

CPlaylist::CPlaylist()
{
  Name="";
}

CPlaylist::~CPlaylist()
{

}

void CPlaylist::internalize()
{
  CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
  CMMLCollection::MMLCollectionReference col=ref->getCollectionByName("playlists");

  CMMLEntity::MMLEntityReference en=col->getEntityByKeyValue("nome",getName());
  int count=AtoI(en->getValueByKey("count").c_str());


  for (int c=0;c<count;c++)    
      addMusicId(AtoI(en->getValueByKey(ItoA(c)).c_str()));
}


int CPlaylist::getTotalItems()
{
  return Music.size();
}


void CPlaylist::addMusicId(CMusic::MusicIdType id)
{
  return Music.push_back(id);
}

void CPlaylist::setName(const std::string &name)
{
  Name=name;
}

const std::string CPlaylist::getName()
{
  return Name;
}



#ifdef DEBUG
void CPlaylist::dump()
{

  
  CMusic *music;
  CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
  CMMLCollection::MMLCollectionReference col=ref->getCollectionByName("musics");

  for (int c=0;c<getTotalItems();c++)
    {
      music=new CMusic();
      music->internalize(Music[c]);
      std::cout << music->getTrack()<<"-"<<music->getName() << "-" << music->getLength() ;
      if (music->isBonus())
	std::cout << "*";
      std::cout << std::endl;
    }
}
#endif
