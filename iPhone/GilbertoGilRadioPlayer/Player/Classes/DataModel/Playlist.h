/*
 *  Playlist.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include <vector>
#include "Music.h"

#ifndef PLAYLIST_H
#define PLAYLIST_H

class CPlaylist
	{
	public:
		///typedefs:
		typedef int PlaylistIdType;
		///object lifecycle:
		CPlaylist();
		~CPlaylist();		
		///get methods:
		const std::string getName();		
		CMusic::MusicIdType getMusicIdByPosition(int pos);
		int getTotalItems();
		///set methods:
		void setName(const std::string &name);
		///misc. methods:
		void addMusicId(CMusic::MusicIdType id);
		void internalize();

#ifdef DEBUG
		void dump();
#endif
		
	private:
		///fields:
		std::vector<CMusic::MusicIdType> Music;
		std::string Name;
	
	};

#endif
