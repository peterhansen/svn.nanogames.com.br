/*
 *  Music.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <string>

#ifndef MUSIC_H
#define MUSIC_H

class CMusic
	{
	public:
		///typedefs:
		typedef int MusicIdType;
		///object lifecycle
		CMusic();
		~CMusic();		
		///get methods:		
		///TODOO: retorna uma copia da referencia (ergo, aponta para o mesmo buffer) ou retorna uma nova copia de buffer?
		const std::string getName();
		const std::string getPublisher();
		int getTrack();
		int getLength();				
		MusicIdType getId();
		///set methods:
		void setName(const std::string &name);
		void setPublisher(const std::string &publisher);
		void setTrack(int track);
		void setLength(int length);		
		void setId(MusicIdType id);
		///query methods:
		bool isBonus();
		///misc methods:
		void internalize(MusicIdType id);
	private:
		///fields:
		std::string Name;
		std::string Publisher;
		int Track;
		int Length;
		MusicIdType Id;
		bool IsBonus;
	};

#endif
