/*
 *  Album.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <dirent.h>
#include <iostream>
#include <fstream>

#include "MMLUtils.h"
#include "MMLEntity.h"
#include "MMLCollection.h"
#include "RefazendaProxy.h"
#include "Playlist.h"
#include "MusicAlbum.h"



CMusicAlbum::CMusicAlbum()
{
}


CMusicAlbum* CMusicAlbum::CreateNew(const std::string &Name)
{
	CMusicAlbum *ptr=new CMusicAlbum();	
	std::string fullpath="";
	int nextId=0;
	
	CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
	CMMLCollection::MMLCollectionReference col=ref->getCollectionByName("discs");
	nextId=col->getTotalItems();
	
	fullpath+=CMMLDatabase::getInstance()->getBasePath();
	fullpath+=col->getCategory();
	fullpath+=ItoA(nextId);
	
	{
		std::ofstream createfile(fullpath.c_str());		
		{
			createfile << "nome="<<Name<<';'<<std::endl;
			createfile << "~" << std::endl;
		}
		createfile.close();
	}
	ptr->setName(Name.c_str());
	col->addEntity((CMMLEntity*)ptr);
	return ptr;
}

void CMusicAlbum::internalize()
{
  CPlaylist::internalize();
  CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
  CMMLCollection::MMLCollectionReference col=ref->getCollectionByName("discs");
  CMMLEntity::MMLEntityReference en=col->getEntityByKeyValue("nome",getName());
  Seal=ref->getCollectionByName("seals")->getEntityById(AtoI(en->getValueByKey("selo").c_str()))->getValueByKey("nome");
  Publisher=ref->getCollectionByName("publishers")->getEntityById(AtoI(en->getValueByKey("editora").c_str()))->getValueByKey("nome");
  Year=en->getValueByKey("ano");
}


const std::string CMusicAlbum::getSeal()
{
  return Seal;
}

const std::string CMusicAlbum::getPublisher()
{
  return Publisher;
}


const std::string CMusicAlbum::getCrewString()
{
  std::string toReturn="";
  CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
  CMMLCollection::MMLCollectionReference col_party=ref->getCollectionByName("crewparties");
  CMMLCollection::MMLCollectionReference col_crew=ref->getCollectionByName("crewmembers");
  CMMLCollection::MMLCollectionReference col_album=ref->getCollectionByName("discs");
  CMMLEntity::MMLEntityReference en_album=col_album->getEntityByKeyValue("nome",getName());
  CMMLEntity::MMLEntityReference en_party=col_party->getEntityById(AtoI(en_album->getValueByKey("equipe").c_str()));

  for (int c=0;c<en_party->getTotalFields();c++)
    {
      toReturn+=en_party->getKeyNameByIndex(c);
      toReturn+=":";
      toReturn+=col_crew->getEntityById(AtoI(en_party->getValueByIndex(c).c_str()))->getValueByKey("nome");
      toReturn+="\n";
    }
  
  return toReturn;
}


const std::string CMusicAlbum::getTextsString()
{
  std::string toReturn="";
  CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
  CMMLCollection::MMLCollectionReference col_textsgroups=ref->getCollectionByName("textsgroups");
  CMMLCollection::MMLCollectionReference col_texts=ref->getCollectionByName("texts");
  CMMLCollection::MMLCollectionReference col_album=ref->getCollectionByName("discs");
  CMMLEntity::MMLEntityReference en_album=col_album->getEntityByKeyValue("nome",getName());
  CMMLEntity::MMLEntityReference en_textsgroup=col_textsgroups->getEntityById(AtoI(en_album->getValueByKey("textos").c_str()));
  CMMLEntity::MMLEntityReference en_text;

  for (int c=0;c<en_textsgroup->getTotalFields();c++)
    {
      en_text=col_texts->getEntityById(AtoI(en_textsgroup->getValueByIndex(c).c_str()));
      for (int d=0;d<en_text->getTotalFields();d++)
	{
	toReturn+=en_text->getValueByIndex(d);
	toReturn+="\n";
	}
      toReturn+="\n";
    }
  
  return toReturn;
}
