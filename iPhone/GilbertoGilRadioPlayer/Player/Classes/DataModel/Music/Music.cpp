/*
 *  Music.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include "MMLUtils.h"
#include "MMLEntity.h"
#include "MMLCollection.h"
#include "RefazendaProxy.h"
#include "Music.h"

CMusic::CMusic()
{
  Track=0;
  IsBonus=0;
  Length=0;
  Publisher="";
  Name="";
}


void CMusic::internalize(CMusic::MusicIdType id)
{

  ///nao ha tratamento porque nao obrigacao dos campos existirem.

  CRefazendaProxy::RefazendaProxyReference ref=CRefazendaProxy::getInstance();
  CMMLCollection::MMLCollectionReference col=ref->getCollectionByName("musics");
  CMMLEntity::MMLEntityReference en=col->getEntityById(id);

  Id=id;

  try 
    {
      Name=en->getValueByKey("nome");
    }
  ///este erro e' inadmissivel
  catch (int e) 
    {
      throw e;
    }

  try
    {
  Length=AtoI(en->getValueByKey("duracao").c_str());
    }
  catch (int e) 
    {
      Length=0;
    }

  try
    {
  Track=AtoI(en->getValueByKey("faixa").c_str());
    }
  catch (int e) 
    {
      Track=0;
    }

  try
    {
  IsBonus=AtoI(en->getValueByKey("bonus").c_str());
    }
  catch (int e) 
    {
      IsBonus=false;
    }
  
  try
    {
  Publisher=en->getValueByKey("editora"); 
    }
  catch (int e) 
    {
      Publisher="";
    }

}


int CMusic::getLength()
{
  return Length;
}


int CMusic::getTrack()
{
  return Track;
}

const std::string CMusic::getName()
{
  return Name;
}


bool CMusic::isBonus()
{
  return IsBonus;
}
