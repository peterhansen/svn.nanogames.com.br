/*
 *  AlbumCollection.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <iostream>
#include "MMLCollection.h"
#include "RefazendaProxy.h"
#include "AlbumCollection.h"


/**************************************************************************************************
 * getter for the category of the collection
 ***************************************************************************************************/
CMusicAlbumCollection::CMusicAlbumCollection()
{
  AlbumCollection=CRefazendaProxy::getInstance()->getCollectionByName("discs");
#ifdef DEBUG
  std::cout << "CMusicAlbumCollection::CMusicAlbumCollection(): category aquired with:"<<AlbumCollection->getTotalItems() << " item(s)"<< std::endl;
#endif
}

