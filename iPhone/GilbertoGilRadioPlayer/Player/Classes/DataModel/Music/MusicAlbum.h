/*
 *  Album.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */


#include "Playlist.h"
#include "NanoDate.h"

#ifndef MUSICALBUM_H
#define MUSICALBUM_H

class CMusicAlbum: public CPlaylist
	{
	public: 
		///typedefs:
		typedef int MusicAlbumIdType;
		///object lifecycle		
		CMusicAlbum();
		~CMusicAlbum();
		static CMusicAlbum* CreateNew(const std::string &Name);
		///get methods
		const std::string getPublisher();
		const std::string getSeal();
		const std::string getYear();
		MusicAlbumIdType getId();
		NanoDate getDate;
		///set methods
		
		void setPublisher(const std::string &publisher);
		void setSeal (const std::string &seal);
		void setDate(const NanoDate &date);		
		void setId(MusicAlbumIdType id);
		void setYear(const std::string &year);
		///misc methods
		void internalize();
		const std::string getCrewString();
		const std::string getTextsString();
	private:
		///fields:
		std::string Publisher;
		std::string Seal;
		MusicAlbumIdType Id;		
		NanoDate DataLancamento;
		std::string Year;
	};

#endif
