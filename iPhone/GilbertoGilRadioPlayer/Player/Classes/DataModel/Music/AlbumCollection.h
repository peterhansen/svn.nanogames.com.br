/*
 *  AlbumCollection.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include "NanoDate.h"
#include "MusicAlbum.h"

#ifndef ALBUMCOLLECTION_H
#define ALBUMCOLLECTION_H

class CMusicAlbumCollection
	{
	public:
		///object lifecycle
		CMusicAlbumCollection();
		~CMusicAlbumCollection();		
		///set methods
		void setLastUpdate (const NanoDate &date);
		///get methods
		NanoDate getLastUpdate();
		const CMusicAlbum::MusicAlbumIdType getAlbumByIndex(int index);
		///query methods
		int getTotalAlbums();
		///misc. methods
		void addAlbum(const CMusicAlbum::MusicAlbumIdType album);
		void clearAlbumCollection();
	private:
		///fields:
		std::vector<CMusicAlbum::MusicAlbumIdType> Album;
		CMMLCollection::MMLCollectionReference AlbumCollection;
	};

#endif
