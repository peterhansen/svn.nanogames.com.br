/*
 *  Photo.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <string>

#ifndef PHOTO_H
#define PHOTO_H

class CPhoto
	{
	public:
		///typdefs:
		typedef int PhotoIdType;
		typedef CPhoto* PhotoReference;
		///object lifecycke:
		CPhoto();
		~CPhoto();
		///get methods:
		const std::string getURL();
		const std::string getDescription();
		///set methods:
		void setURL(std::string url);
		void setDescription(std::string description);
		///misc. methods:
		///query methods:		
	private:
		///fields:
		std::string URL;
		std::string Description;
	};

#endif