/*
 *  PhotoAlbum.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include "Photo.h"
#include <vector>

#ifndef PHOTOALBUM_H
#define PHOTOALBUM_H

class CPhotoAlbum
	{
		public:
		///object lifecycle:
		CPhotoAlbum();
		~CPhotoAlbum();		
		///get methods:
		///TODOO: think of a good reason for this method to be here...
		const CPhoto::PhotoReference getPhotoById(CPhoto::PhotoIdType id);
		const CPhoto::PhotoReference getPhotoByIndex(int index);
		int getTotalPhotos();
		///set methods:		
		///misc methods:
		void addPhoto(CPhoto::PhotoIdType id);
		private:
		///fields:
		std::vector <CPhoto::PhotoIdType> Photo;
		
	};

#endif