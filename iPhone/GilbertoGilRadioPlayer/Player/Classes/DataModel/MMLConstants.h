/*
 *  Constants.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/26/10.
 *  Copyright 2010 Daniel Monteiro. All rights reserved.
 *
 */


#ifndef MMLCONSTANTS_H
#define MMLCONSTANTS_H



/// numero de zeros antes do id de uma entidade, para evitar problemas com a ordenacao do finder
#define TRAILINGZEROES 6


/// constantes de parsing
#define ENTITY_ATTRIB_MARKER '='
#define ENTITY_EOL_MARKER ';'
#define ENTITY_EOF_MARKER '~'

#endif

