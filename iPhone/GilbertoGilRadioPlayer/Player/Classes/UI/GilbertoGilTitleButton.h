//
//  GilbertoGilTitleButton.h
//  Player
//
//  Created by Daniel Monteiro on 5/26/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NUM_LOGOS 7

@interface GilbertoGilTitleButton : UIImageView {
}

- ( void ) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- ( void ) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;
- ( void ) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event;
- ( void ) forceLogoColor: (int) logo;
@end
