//
//  BioView.m
//  Player
//
//  Created by Daniel Monteiro on 9/3/10.
//  Copyright 2010 Nano Games. All rights reserved.
//
/// C++
#include <iostream>
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "HTTPBridge.h"
#import "PlayerAppDelegate.h"
#import "Config.h"
#include "RefazendaProxy.h"

/// View
#import "BioView.h"

@implementation BioView

@synthesize yearList;


/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildBioView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildBioView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM buildBioView 
 
 ================================================================================================*/

-( bool )buildBioView
{
	[self setHidden: YES];	
	updated=NO;
    doc=[GDataXMLDocument alloc];		
	if (updated == NO) {
        [ NSThread detachNewThreadSelector:@selector( updateOnTimer ) toTarget: self withObject: self ];        
    }
	return true;
	
}

/*==============================================================================================
 
 MENSAGEM updateOnTimer
 
 ================================================================================================*/

- (void) updateOnTimer
{
	[self updateContent];	
}

/*==============================================================================================
 
 MENSAGEM forceUpdate
 
 ================================================================================================*/

- (IBAction) forceUpdate
{
	[self updateContent];
}


/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 
 ================================================================================================*/

- (void) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];

    index = -1;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)            
        [ yearOut loadHTMLString: 
         [ NSString stringWithContentsOfFile: [ [NSBundle mainBundle] pathForResource:@"geral_ipad" ofType:@"html" ]  encoding:NSUTF8StringEncoding error: nil ]
                         baseURL: nil ];
    else
        [ yearOut loadHTMLString: 
       [ NSString stringWithContentsOfFile: [ [NSBundle mainBundle] pathForResource:@"geral" ofType:@"html" ]  encoding:NSUTF8StringEncoding error: nil ]
                     baseURL: nil ];
}



- ( void ) updateUI {
    UILabel *yearLabel;
    GDataXMLElement *item;    
    GDataXMLNode *root;
    root = [doc rootElement];
    NSArray *items;
    
    items = [root nodesForXPath:@"/lista_biografia/biografia/ano" error: nil ];		
    UIFont * font;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
        font = [ UIFont fontWithName: @"Verdana-Bold" size: 22 ];
    else
        font = [ UIFont fontWithName: @"Verdana-Bold" size: 14 ];
    
    for ( int c = [ items count ] - 1; c >= 0; --c )
    {
        item = [ items objectAtIndex: c ];
        ///ipad: 122x66 - 8 pixels de espaçamento
        //ipod: 50x31 - 3 pixels de espaçamento
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            yearLabel = [ [ UILabel alloc ] initWithFrame:CGRectMake( ( [ [ yearList subviews] count ] - 1 )  * 130 , 0, 122, 66 ) ];
            
        } else {
            yearLabel = [ [ UILabel alloc ] initWithFrame:CGRectMake( ( [ [ yearList subviews] count ] - 1 )  * 53 , 0, 50, 31 ) ];
        }
        
        
        
        [ yearLabel setText: [item stringValue] ];
        [ yearLabel setTextAlignment: UITextAlignmentCenter ];
        [ yearLabel setBackgroundColor: [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ] ];
        [ yearLabel setTextColor: [ UIColor colorWithRed: ( 34.0f/255.0f) green: (72.0f/255.0f) blue: ( 101.0f/255.0f) alpha:1.0f] ];
        [ yearLabel setFont: font ];
        [ yearList addSubview: yearLabel ];        
        [ yearLabel autorelease ];
    }
}

/*==============================================================================================
 
 MENSAGEM updateContent
 
 ================================================================================================*/

- (void) updateContent {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; 
    
    
	NSString *fullPathToFile=[ NSString stringWithFormat:@"%@cached/%s", [PlayerAppDelegate getDataDir], "biografia" ];
	NSMutableString *content;
	NSString *contentRaw;
	NSString *contentRawTmp;	
	NSURL *url=[NSURL URLWithString: [NSString stringWithUTF8String:CONSTbiographyURL.c_str()]];
	
	content = [[NSMutableString alloc] init];
	
	contentRaw = [NSString stringWithContentsOfFile:fullPathToFile encoding:NSWindowsCP1252StringEncoding error: nil ];
	
#ifdef DEBUG
	NSLog(@"from cache :%@",contentRaw);
#endif		

	contentRawTmp=[NSString stringWithContentsOfURL:url encoding: NSWindowsCP1252StringEncoding error: nil ];
	
	if (contentRawTmp!=nil && [contentRawTmp length]!=0)
	{
		contentRaw=contentRawTmp;
		[contentRaw writeToFile:fullPathToFile atomically:NO encoding:NSWindowsCP1252StringEncoding error: nil ];
	}
	
	if (contentRaw!=nil && [contentRaw length]!=0)
	{
		[doc initWithXMLString:contentRaw  options:0 error: nil ];		
        [ self performSelectorOnMainThread:@selector( updateUI ) withObject: self waitUntilDone: YES ];
		updated=YES;	

#ifdef DEBUG
		NSLog(@"to cache :%@",content);
#endif		
	}

	[content autorelease];	
    
    [ yearList setClipsToBounds:YES];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        [ yearList setContentSize:CGSizeMake( ( [ [ yearList subviews] count ] - 1 ) * 130, 66)];
    else
        [ yearList setContentSize:CGSizeMake( ( [ [ yearList subviews] count ] - 1 ) * 53, 31)];
         
    [ yearList setScrollEnabled:YES];
    
    
    [ pool release ];    
}

/*==============================================================================================
 
 MENSAGEM updateFor
 
 ================================================================================================*/


- (void) updateFor: ( int ) year 
{
    NSArray *items;
    NSArray *views = [ yearList subviews ];
    GDataXMLNode *root;
    root = [doc rootElement];
    NSString *path = [ NSString stringWithFormat: @"/lista_biografia/biografia[%d]/texto" , ( [ yearList.subviews count ] - ( year  + 1 ) ) ];
    NSMutableString *tmpContent  = [ [ NSMutableString alloc ] init ];
 
    if ( root != nil ) {        
        items = [ root nodesForXPath: path error: nil ];		
        for ( GDataXMLElement *item in items ) {
            [ tmpContent appendString:@"<p>" ];    
            [ tmpContent appendString:[ item stringValue ] ];            
            [ tmpContent appendString:@"</p>" ];                       
        }

    } else {
#ifdef DEBUG        
        NSLog(@" erro na arvore ");
#endif
        return;
    }
    [ tmpContent appendString:@"</body></html>" ];
    
    
    [ tmpContent replaceOccurrencesOfString:@"\n" withString: @"<br/>" options:NSCaseInsensitiveSearch range: NSMakeRange( 0, [ tmpContent length ] ) ];

    [ tmpContent insertString: @"</h1>" atIndex:0 ];    
    [ tmpContent insertString: [ [ views objectAtIndex: year ] text ] atIndex:0 ];        
    [ tmpContent insertString: @"<h1>" atIndex:0 ];

    [ tmpContent insertString: @"</head<body>" atIndex:0 ];
    [ tmpContent insertString: CRefazendaProxy::getInstance()->getStyle() atIndex:0 ];    
    [ tmpContent insertString: @"<html><head>" atIndex:0 ];
    
    [ yearOut loadHTMLString: tmpContent  baseURL:nil ];
    
    [ tmpContent autorelease ];
}


/*==============================================================================================
 
 MENSAGEM touchesEnded
 
 ================================================================================================*/

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{
    NSArray *views = [ yearList subviews ];
    UIColor *color;
    for ( UILabel *view in  views ) {
        if ( CGRectContainsPoint(view.frame, [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: yearList ] ) ) {
            color = [ [ UIColor alloc ] initWithRed: ( 98.0f /  255.0f ) green: ( 135.0f / 255.0f ) blue: ( 163.0f / 255.0f ) alpha: 1.0f ];            
            [ view setBackgroundColor: color ];
            [ color autorelease ];            
            index = [ views indexOfObject: view ];
            [ self updateFor: index ];           
            
        }
        
        else {
            
            color = [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ];
            [ view setBackgroundColor: color ];
            [ color autorelease ];             
        }
    }   
}

- ( void ) gotoMenu {
    if ( index == -1)
        [ super gotoMenu ];
    else
        [ self onBecomeCurrentScreen ];
}

/*==============================================================================================
 
 MENSAGEM dealloc
 
 ================================================================================================*/

- (void) dealloc {
    [super dealloc];
    [yearList dealloc];
}
@end
