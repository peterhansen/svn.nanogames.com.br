//
//  HeadlineViewCell.mm
//  Player
//
//  Created by Daniel Monteiro on 6/2/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "HeadlineViewCell.h"


@implementation HeadlineViewCell

@synthesize headline;
@synthesize sourceAndDate;
@synthesize webView;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
