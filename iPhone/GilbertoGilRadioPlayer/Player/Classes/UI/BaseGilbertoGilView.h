//
//  BaseGilbertoGilView.h
//  Player
//
//  Created by Daniel Monteiro on 9/8/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Nano Components
#import "UpdatableView.h"
#import "ObjcMacros.h"
/// Aplicacao
#import "Config.h"


@interface BaseGilbertoGilView : UpdatableView 
{
	/// id da view
	ViewId myID;
	
	/// a view se encontra bloqueada de interacao com o usuario?
	BOOL locked;
}
///<InterfaceBuilder>
/// vai ao menu principa;
- (IBAction)gotoMenu;

/// Indica que o usuário selecionou uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;
///</InterfaceBuilder>

/// atualiza de fato a view
- (void) refresh;

/// Atualiza a view
- ( void ) update:( float )timeElapsed;

/// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

/// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

/// "construtor" apartir do XIB
- ( void )awakeFromNib;

/// notifica o fim do carregamento
- (void) notifyFinishedLoading;

/// interacao com o sistema bloqueada (setter)
- (void) setLocked: (BOOL) state;

/// interacao com o sistema bloqueada (getter)
- (BOOL) isLocked;


///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildBaseGilbertoGilViewView;

///</ciclo de vida>

@end
