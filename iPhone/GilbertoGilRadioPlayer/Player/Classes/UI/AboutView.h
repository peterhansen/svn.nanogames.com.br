//
//  AboutView.h
//  Player
//
//  Created by Daniel Monteiro on 5/23/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"

@interface AboutView : BaseGilbertoGilView<UIWebViewDelegate> {
	IBOutlet UIWebView *aboutDisplay;
	}
	///<ciclo de vida>
	/// construtor de primeiro passo, apartir de [[alloc] init ]
	-( id )initWithFrame:( CGRect )frame;
	
	/// construtor de primeiro passo, apartir do XIB
	-( id )initWithCoder:( NSCoder* )decoder;
	
	/// costrutor de fato
	-( bool )buildAboutView;
	///</ciclo de vida>

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType; 
@end