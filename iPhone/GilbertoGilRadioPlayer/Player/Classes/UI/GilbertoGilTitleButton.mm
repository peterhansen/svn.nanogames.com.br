//
//  GilbertoGilTitleButton.m
//  Player
//
//  Created by Daniel Monteiro on 5/26/11.
//  Copyright 2011 Nano Games. All rights reserved.
//
/// C++
#include <sstream> 
#include <map>
#include <vector>

/// 3rd Party
#import "GDataXMLNode.h"

/// Aplicacao
#import "Config.h"
#import "BaseGilbertoGilView.h"
#import "PlayerAppDelegate.h"
#import "GilbertoGilTitleButton.h"


@implementation GilbertoGilTitleButton

- (id)init {
    id mySelf = [ super init ];
    self.alpha = 0.1;
    return mySelf;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    id mySelf = [ super initWithCoder: aDecoder ];
    self.alpha = 0.1;    
    return mySelf;
}


-(id)initWithImage:(UIImage *)image {
    id mySelf = [ super initWithImage: image ];
    self.alpha = 0.1;
    return mySelf;
}


- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage {
    id mySelf = [ super initWithImage: image highlightedImage: highlightedImage ];
    self.alpha = 0.1;
    return mySelf;
}



- (void) forceLogoColor: (int) logo 
{
    char** logos;
    char *logosiPad[] = { "logo_black_ipad","logo_black_ipad","logo_red_ipad","logo_green_ipad","logo_blue_ipad","logo_pink_ipad","logo_orange_ipad"};
    char *logosiPod[]  = { "logo_black","logo_black","logo_red","logo_green","logo_blue","logo_pink","logo_orange"};
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        logos = logosiPad;
    }
    else
    {
        logos = logosiPod;
    }

    

    
    UIImage * img;
    
    img = [ UIImage imageWithContentsOfFile: [ [ NSBundle mainBundle ] pathForResource:[ NSString stringWithCString:logos[ logo ] encoding: NSUTF8StringEncoding ] ofType:@"png"] ];
    
    [ self setImage: img ];
    [ self setNeedsDisplay ];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{	
    int logo = rand();
    
    if ( logo < 0 )
        logo = -logo;
    
    logo = logo % NUM_LOGOS - 1;
    
    [ self forceLogoColor: logo  + 1 ];
    self.alpha = 1.0;    
}

- ( void ) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [ self forceLogoColor: 0 ];
    self.alpha = 0.1;
}



- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{	
    [ self forceLogoColor: 0 ];
    self.alpha = 0.1;    

   if ( CGRectContainsPoint( self.frame, [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: self ] ) ) {
       PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
       
       if ( [ self.superview isKindOfClass: [ BaseGilbertoGilView class ]   ] )
           [ self.superview onBeforeTransition ];
       
       [instance performTransitionToView:screen_MENU ];
    }            
}
@end
