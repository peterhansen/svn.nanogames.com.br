//
//  TweetEntryTableCellView.h
//  Player
//
//  Created by Daniel Monteiro on 5/23/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TweetEntryTableCellView : UITableViewCell {
	NSMutableArray *columns;
	NSString *tweet;
	NSString *user;
	NSString *realName;
	NSString *date;
}
- (void)addColumn:(CGFloat)position;

- (void)setContent: (NSString *) tweetText :(NSString *)userName  :(NSString *) personName :( NSString *)tweetDate;

@end
