//
//  SingleTapEnabledScrollView.h
//  Player
//
//  Created by Daniel Monteiro on 5/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SingleTapEnabledScrollView : UIScrollView {
    
}
- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event;
@end
