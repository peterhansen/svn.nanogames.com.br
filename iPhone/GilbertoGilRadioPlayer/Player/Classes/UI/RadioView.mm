//
//  RadioView.m
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// C++
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"

/// View
#import "PlaylistSongViewCell.h"
#import "OnDemandLoadImageView.h"
#import "DiscoView.h"
#import "RadioView.h"
#import "AlertPrompt.h"


@implementation RadioView
///depende de uma lock melhor


- (CMMLEntity::MMLEntityReference) currentSongEntity
{
	///C++	
	std::string nome;
	///MML
	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference musics=proxy->getCategoryByName(CONSTmusicsCollection);
	CMMLCategory::MMLCategoryReference works=proxy->getCategoryByName(CONSTworksCollection);
	///ObjC
	NSString *idTrilha;	
	///</init>	
	idTrilha=[MP3List objectAtIndex:song];
	return works->getEntityByKeyValue("id-obra", musics->getEntityByKeyValue("id-trilha",[ idTrilha UTF8String ] )->getValueByKey("id-obra"));
}


- (CMMLEntity::MMLEntityReference) currentAlbumEntity
{
	///C++	
	std::string nome;
	///MML
	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference albums=proxy->getCategoryByName(CONSTalbumsCollection);    
	CMMLCategory::MMLCategoryReference musics=proxy->getCategoryByName(CONSTmusicsCollection);
	///ObjC
	NSString *idTrilha;	
	///</init>	
	idTrilha=[MP3List objectAtIndex:song];

    CMMLEntity::MMLEntityReference album;
    CMMLEntity::MMLEntityReference music;    
    std::string idAlbum;
    
    music = musics->getEntityByKeyValue("id-trilha",[ idTrilha UTF8String ] );
    idAlbum = music->getValueByKey("id-album");
    album = albums->getEntityByKeyValue("refazenda-id", idAlbum );
     
    return album;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/
-( id )initWithFrame:( CGRect )frame {
    if ( ( self = [ super initWithFrame:frame ] ) ) {
		if( ![ self buildRadioView ] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[ self release ];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/
- ( id )initWithCoder:( NSCoder* )decoder {
	if( ( self = [super initWithCoder:decoder] ) ) 	{
		if( ![self buildRadioView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
-( bool )buildRadioView {
	[self setHidden: YES];
	audioController=NULL;
	/*playerModeController.selectedSegmentIndex = */playerMode = PlayerMode_NORMAL;
	MP3List=[ [ NSMutableArray alloc ] init ];	
	playlistTable.dataSource = self;
	[ self playlistIsEmpty ];	
    playlistTable.allowsSelectionDuringEditing = NO;
	return true;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
    
    
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (void) FetchMusicList {	
	char *tmp;
	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference playlists=proxy->getCategoryByName(CONSTplaylistsCollection);
	CMMLEntity::MMLEntityReference playlist;
	
	playlist=playlists->getEntityByKeyValue("nome",CONSTdefaultPlaylist);		
	int howmany=AtoI((playlist->getValueByKey("size")).c_str());
	for (int c=0;c<howmany;++c)
	{
		tmp=(char*)playlist->getValueByKey(ItoA(c)).c_str();
		[MP3List addObject:[NSString stringWithUTF8String: tmp  ]];
	}
	playlist->setValueForKey("size", "0");
    [ playlistTable setEditing: YES animated: NO ];
}


- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    [ self swap: fromIndexPath.row withIndex: toIndexPath.row ];
    [ tableView reloadData ];
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    return proposedDestinationIndexPath;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;  
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (void)dealloc {
    [super dealloc];
    [ MP3List autorelease ];
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (IBAction) play {
	
//    if ( [ playlistTable isEditing ] == YES )
//        return;
    
    
	if (audioController!=NULL && audioController->getPlayerState()==PlayState_PLAY) {
        [ self pause ];
        return;	
    }
		
	
	if (audioController!=NULL && audioController->getPlayerState()==PlayState_PAUSE) {
		[self pause ];
	}
	
	if (audioController!=NULL && audioController->getPlayerState()!=PlayState_STOP) 
		return;	
	
	[self InitRadio];
	[self updatePlaylistPanel];	
	[playlistTable reloadData];	
	[self scrollToSong];
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (void) InitRadio {
	
	///verifica se o player ja esta tocando ou se a lista de musicas esta vazia. Se estiver, saia.
	[self updatePlaylistPanel];	
	if (audioController!=NULL && audioController->getPlayerState()==PlayState_PLAY) 
		return;	
	if ([MP3List count]==0) return;	
	
	///<declaracoes de variaveis locais>
	///objc
	NSString *mp3file;
	///</declaracoes de variaveis locais>
	
	///<mandando a requisicao de musica>	
	mp3file=(NSString*)[MP3List objectAtIndex:song];	
	[ playlistTable reloadData ];
	///apaga qualquer requisicao antiga.	
	//delete audioController;
	try {
		
		audioController=new HTTPAudioStreamController();
		audioController->startStream([mp3file stringByAppendingString:@".mp3" ]); 	
        

        CMMLEntity::MMLEntityReference songEntity = [ self currentSongEntity ];
        CMMLCategory::MMLCategoryReference musics = CRefazendaProxy::getInstance()->getCategoryByName( CONSTmusicsCollection );
        
        NSString * idTrilha = [ MP3List objectAtIndex: song ];
        [ songLength setText: [ NSString stringWithUTF8String: musics->getEntityByKeyValue("id-trilha",[ idTrilha UTF8String ] )->getValueByKey("tempo").c_str() ] ];

		[self initTimer];
		
	} catch (AudioStreamUnavailableException *ex) {
		//dar uma mensagem explicando o problema.
	}
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) terminateTimer
{
	if (updateTicker != nil)
		[updateTicker invalidate];
	
	updateTicker = nil;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) initTimer {
	updateTicker= [ NSTimer scheduledTimerWithTimeInterval: CONSTradioUpdateRate
												   target: self
												 selector: @selector( selfUpdateTicker: )
												 userInfo: nil
												  repeats: YES
				   ];	
	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) scrollToSong {
 	NSIndexPath *indPath = [ NSIndexPath indexPathForRow:song inSection: 0 ];
	[ playlistTable scrollToRowAtIndexPath:indPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES ];
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (IBAction) stop {
	
	if (audioController!=NULL)
		audioController->requestPlayerState(PlayState_FORCESTOP);
	
    cover.image = nil;
    CGRect frame = songProgressBox.frame;
    frame.size.width = 0;
    songProgress.frame = frame;

    
	delete audioController;
	audioController=NULL;
	
	[self updatePlaylistPanel];	
	[self terminateTimer];
	[playlistTable reloadData];	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (IBAction) pause {
	
	[self FetchMusicList]; ///pode ser que a lista esteja vazia, mas que haja musicas na fila pra entrar pra lista.	
	
	if ([MP3List count]==0) 
		return;
	
	if (audioController!=NULL) {
		
		if (audioController->getPlayerState()==PlayState_PLAY)
			audioController->requestPlayerState(PlayState_PAUSE);
		else
			audioController->requestPlayerState(PlayState_PLAY); 
	}
	
	[self updatePlaylistPanel];	
	[playlistTable reloadData];	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (IBAction) clear {
	
	[self stop];
	[MP3List removeAllObjects  ];
	[self updatePlaylistPanel];	
	
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)                    
        [ playBtn setImage:[ UIImage imageNamed:@"play_ipad.png" ] forState: UIControlStateNormal ];                
    else
        [ playBtn setImage:[ UIImage imageNamed:@"play.png" ] forState: UIControlStateNormal ];
    
    [ playBtn setNeedsDisplay ];

	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference playlists=proxy->getCategoryByName(CONSTplaylistsCollection);
	CMMLEntity::MMLEntityReference playlist;
	
	playlist=playlists->getEntityByKeyValue("nome",CONSTdefaultPlaylist);		
	playlist->setValueForKey("size", "0");
	song=0;
    
	[playlistTable reloadData];	
	[self playlistIsEmpty];
    
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (IBAction) next {
	
	[self FetchMusicList];
	[self updatePlaylistPanel];	
	cover.image = nil;
    
	if ([MP3List count]==0) 
		return;
	
    
	if (song == [MP3List count] -1 && playerMode == PlayerMode_NORMAL ) {
		[self stop];
		return;
	}
	
	
	int prevstate = PlayState_INVALID;
	
	if (audioController!=NULL) {		
		
		prevstate=audioController->getPlayerState();		
		[self stop];	
	}
	
	switch (playerMode) {
			
		case PlayerMode_NORMAL:
			
		case PlayerMode_REPEAT_LIST: {
			song++;		
		}			
		break;
			
		case PlayerMode_SHUFFLE: {
			int nextsong=song;
			
			if ([MP3List count]!=0 && [MP3List count]!=1) {
				do 
					nextsong=rand() % [MP3List count];					
				while (song==nextsong);
			}			
			
			song=nextsong;
		}
		break;			
			
		default:
			break;
	}
	
	
	
	if (song>=[MP3List count]) {
		
		if ( playerMode==PlayerMode_REPEAT_LIST )
			song=0;
		else
			song=[MP3List count]-1;	
	}
	
	
	if (prevstate==PlayState_PLAY)
		[self play];
	
	
	[self updatePlaylistPanel];	
	[playlistTable reloadData];	
	[self scrollToSong];
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (IBAction) previous
{
  	cover.image = nil;

	if (playerMode!=PlayerMode_REPEAT_LIST && song == 0 || playerMode == PlayerMode_REPEAT_SONG) 
		return;
        

	[self FetchMusicList]; ///pode ser que a lista esteja vazia, mas que haja musicas na fila pra entrar pra lista.	

    if ([MP3List count]== 0 ) {
		return;        
    }
	

	
	if (playerMode==PlayerMode_SHUFFLE)
		[ self next ];
	
    
	int prevstate = PlayState_INVALID;
	 
	if ( audioController != NULL ) {
		
		prevstate=audioController->getPlayerState();		
		[self stop];		
	}
	
	song--;
	
	if ( song < 0 ) {
		
		if ( playerMode == PlayerMode_REPEAT_LIST )
			song = [MP3List count] -1;
		else
			song = 0;
	}
	
	if (prevstate == PlayState_PLAY)
		[self play];	
	
	[self updatePlaylistPanel];	
	[playlistTable reloadData];	
	[self scrollToSong];	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
/*
 
 body { background-color:#d4e2e7; text-align:left;}
 p { font-family:Verdana; font-size:12px; color:#103e5e;}
 span { margin: 0 auto;}
 img { max-height:353px; max-width:274px;}
 h1 { font-family:Verdana; font-size:12px; color:#103e5e; font-weight: bold;  }    
 h2 { font-family:Verdana; font-size:11px; color:#103e5e; font-weight: normal;}
 h3 { font-family:Verdana; font-size:11px; color:#646464; font-weight: normal;}
 h4 { font-family:Verdana; font-size:10px; color:#646464; font-weight: normal;}
 h5 { font-family:Verdana; font-size:10px; color:#103e5e; font-weight: normal;}

 */
- (void) updatePlaylistPanel {
	if ( [ MP3List count ] == 0 ) {
        [ self playlistIsEmpty ];
        [ songPosition setText: @"00:00" ];
        [ songLength setText: @"00:00" ];    
        [ cover setImage: [ UIImage imageNamed:@"icDisco.png"] ];
        [ cover setHidden: YES ];
        [ cover setNeedsLayout ];
        [ cover setNeedsDisplay ];
        [ self setNeedsLayout ];
        [ self setNeedsDisplay ];
        [ saveBtn setEnabled: NO ];
        [ songInfo loadHTMLString:@"<html><body><br/></body></html>" baseURL: nil ];
    } else {
		[ self playlistIsNotEmpty ];
        
        NSString *musica = [ NSString stringWithUTF8String: [ self currentSongEntity ]->getValueByKey("nome").c_str() ];
        NSString *disco = [ NSString stringWithUTF8String: [ self currentAlbumEntity ]->getValueByKey("nome").c_str() ];

        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)            
            [ songInfo loadHTMLString:[ NSString stringWithFormat:@"<html><body><p style = 'font-family:Verdana; font-size:18px; color:#103e5e; line-height: 22px'>%@<br/><span style = 'font-family:Verdana; font-size:15px; color:#646464; font-weight: normal;'>%@</span></p></body></html>",musica, disco ] baseURL: nil ];
        else
            [ songInfo loadHTMLString:[ NSString stringWithFormat:@"<html><body><p style = 'font-family:Verdana; font-size:12px; color:#103e5e; line-height: 18px'>%@<br/><span style = 'font-family:Verdana; font-size:10px; color:#646464; font-weight: normal;'>%@</span></p></body></html>",musica, disco ] baseURL: nil ];
            
            
        
//        [ songInfo loadHTMLString:[ NSString stringWithFormat:@"<html><body><p>%@</p>%@</body></html>",musica, disco ] baseURL: nil ];
        
        
        [ cover setHidden: NO ];        
        
        if ( cover.image == nil ) {
            
            //botão de texto
           	CMMLCategory::MMLCategoryReference musics=CRefazendaProxy::getInstance()->getCategoryByName(CONSTmusicsCollection);
            CMMLCategory::MMLCategoryReference works=CRefazendaProxy::getInstance()->getCategoryByName(CONSTworksCollection);
            std::string idTrilha;	
            idTrilha = [ ((NSString * )[MP3List objectAtIndex:song]) UTF8String ];
            std::string idObra = musics->getEntityByKeyValue( "id-trilha", idTrilha )->getValueByKey( "id-obra" );
            readTextBtn.enabled =
            ( works->getEntityByKeyValue( "id-obra", idObra )->getValueByKey("obs") == "true"  ) ? YES : NO;
            
            //capa do disco
            [ cover  scheduleImageLoad: [ NSString stringWithCString: [ self currentAlbumEntity ]->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                          forDirectory: @"cached/discophotos"
                               withURL: [ DiscoView getPictureUrl: AtoI( [ self currentAlbumEntity ]->getValueByKey("refazenda-id").c_str() ) ] 
                            withBorder: 5.0
                           AndWithAlternativeImage:@"icDisco.png"             
             ];            
        }
        
        if ( audioController == NULL ) {
            [ songPosition setText: @"00:00" ];
            [ songLength setText: @"00:00" ];
        }        
    }
    [ cover setNeedsDisplay ];
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/



- (void) selfUpdateTicker : ( NSTimer * ) timer {
	[ self FetchMusicList ];
	[ self updatePlaylistPanel ];
	bool notset = true;
    
    if ( audioController!= NULL ) {
        int len = audioController->getSongLengthInSeconds();
        int pos = audioController->getSongPositionInSeconds();
        
        if ( len != -1 && pos != -1 ) {
            std::string posSecs = ItoA( pos % 60 );
            std::string posMins = ItoA( pos / 60 );
            
            if ( posSecs.length() == 1)
                posSecs = "0" + posSecs;
            
            if ( posMins.length() == 1)
                posMins = "0" + posMins;
            
            notset = false;
            
            [ songPosition setText: [ NSString stringWithFormat:@"%s:%s", posMins.c_str(), posSecs.c_str() ] ];
            
            CGRect frame = songProgressBox.frame;
            frame.size.width = ( ( pos * ( frame.size.width ) ) / len );
            songProgress.frame = frame;
        }        
        
        if ( audioController->checkForSongEnding() )					
            [ self next ];        
    } 
    
    if ( notset ) {
        [ songPosition setText: @"00:00" ];
        [ songLength setText: @"00:00" ];

    }
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (void) onBecomeCurrentScreen {
	[super onBecomeCurrentScreen];	
    cover.image = nil;    
	[self FetchMusicList];	
	[playlistTable reloadData];	
	[self updatePlaylistPanel];	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
- (void) onBeforeTransition {
	[super onBeforeTransition];
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/



- (IBAction) onPlayerModeChanged {
	
//	NSInteger value=[ playerModeController selectedSegmentIndex ] ;
//	playerMode=( PlayerModeEnum ) value;
	
	if (playerMode == PlayerMode_SHUFFLE)
		srand ( time( NULL ) );
	
	[ self updatePlaylistPanel ];
}




/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ MP3List count ];
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	///C++	
	std::string nome;
	///MML
	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference musics=proxy->getCategoryByName(CONSTmusicsCollection);
	CMMLCategory::MMLCategoryReference works=proxy->getCategoryByName(CONSTworksCollection);
	///ObjC
	NSString *idTrilha;	
	
    static NSString *CellIdentifier = @"PlaylistSongViewCell";
	
    PlaylistSongViewCell *cell = ( PlaylistSongViewCell *)[ tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    if (cell == nil) 
	{
        NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:CellIdentifier owner:nil options:nil ];
        
        
        for ( id currentObject in topLevelObjects ) {
            if ( [ currentObject isKindOfClass:[ PlaylistSongViewCell class ] ] ) {
                cell = ( PlaylistSongViewCell *) currentObject;
                break;
            }
        }
        
    }
    
    


	
	idTrilha=[MP3List objectAtIndex:indexPath.row];
	nome = works->getEntityByKeyValue("id-obra", musics->getEntityByKeyValue("id-trilha",[ idTrilha UTF8String ] )->getValueByKey("id-obra"))->getValueByKey("nome");
	cell.bounds.size.width=[tableView bounds].size.width - 20;


    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.playlistListener = self;
    cell.trackNum = indexPath.row;
	cell.table = tableView;
    cell.songName.text = 
    [ NSString stringWithFormat:@" %d. %s  %@", ( indexPath.row + 1), musics->getEntityByKeyValue("id-trilha",[ idTrilha UTF8String ] )->getValueByKey("tempo").c_str(),  [ NSString stringWithUTF8String:nome.c_str() ] ];


    
	if (indexPath.row==song)
	{
        [ cell setPlaying: YES ];        
	}
	else			
        [ cell setPlaying: NO ];
    
	cell.bounds.size.width=20;    
    cell.manager = self;
    [ cell setNeedsDisplay ];
    [ tableView setNeedsDisplay ];
	return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}


- ( void ) swap: ( int ) index withIndex: ( int ) otherIndex {
    
    int size = [ MP3List count ];
    int selected = index;
    
    if ( index < 0 || otherIndex < 0 || index >= size || otherIndex >= size )
        return;

    NSString *tmp;
    
    if ( song == index ) {
        song = otherIndex;
    }
    
    if ( index < otherIndex ) {
        
        if ( index < song && song < otherIndex ) {
            --song;
        }
        
        tmp = [ MP3List objectAtIndex: selected ];
        
        [ tmp retain ];
        
        for ( int c = index; c < otherIndex; ++c ) {
            if ( c >= 0 && ( c + 1 ) < size ) {
                
                [MP3List replaceObjectAtIndex: c withObject: [ MP3List objectAtIndex: c + 1]];                


            }

        }
        
        [ MP3List replaceObjectAtIndex: otherIndex withObject: tmp ];        
    } 
    else {
        
        if ( otherIndex < song && song < index ) {
            ++song;
        }
        
        tmp = [ MP3List objectAtIndex: index ];
        
        [ tmp retain ];
        
        for ( int c = index; c >= otherIndex; --c ) {
            if ( ( c - 1 ) >= 0 && ( c < size ) ) {
                [MP3List replaceObjectAtIndex: c withObject: [ MP3List objectAtIndex: c - 1]];
 
            }
                
        }
        
        [ MP3List replaceObjectAtIndex: otherIndex withObject: tmp ];
    }
    
#ifdef DEBUG    
    NSLog( @" replacing %d with %d", index, otherIndex );
#endif    
}

- ( void ) removeTrack: ( int ) trackNum {   
    
    bool decrementar=false;
    PlayStateEnum previousState= PlayState_STOP;
    
    if (audioController!=NULL)
        previousState= audioController->getPlayerState();
    
    if (trackNum == song)
        [self stop];
    
    
    [ MP3List removeObjectAtIndex: trackNum ];
    
    if ( trackNum < song)
        decrementar=true;
    
    if (song >= [MP3List count])
        decrementar=true;
    
    if (decrementar)
        song--;
    
    
    if ( ( decrementar && trackNum - 1 == song) || ( trackNum == song))
        if (previousState == PlayState_PLAY)
            [self play];
    
    
    [playlistTable reloadData];
    

    
    
    if ( [ MP3List count ] == 0 )    
        [self playlistIsEmpty];
     else 
        [self scrollToSong];
    
    [ playlistTable reloadData ];
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	PlayStateEnum state = PlayState_STOP;
    
	if (audioController != NULL)
		state= audioController->getPlayerState();
    
	[self stop];
	song = indexPath.row;
    [ playlistTable reloadData ];
	[self play];
	

	
	return nil;
	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (void) playlistIsEmpty
{
    [ saveBtn setEnabled: NO ];	    
	[clearBtn setEnabled:NO];
	[playBtn setEnabled: NO ];
	[ffBtn setEnabled:NO];
	[rewBtn setEnabled:NO];		
	[textBtn setEnabled:NO];
	[self updateUIControls];	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) playlistIsNotEmpty
{
	
	
	PlayStateEnum	state;
	
	if (audioController!=NULL)
		state=audioController->getPlayerState();
	else
		state=PlayState_STOP;		
	
    [ saveBtn setEnabled: YES ];
	
	switch (state) 
	{
		case PlayState_PLAY:
		{
//			[playBtn setEnabled:NO];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)                    
                [ playBtn setImage:[ UIImage imageNamed:@"pause_ipad.png" ] forState: UIControlStateNormal ];
            else
                [ playBtn setImage:[ UIImage imageNamed:@"pause.png" ] forState: UIControlStateNormal ];
            
//			[pauseBtn setEnabled:YES];
//			[stopBtn setEnabled:YES];
			[ffBtn setEnabled:YES];
			[rewBtn setEnabled:YES];
		}
			break;			
		case PlayState_PAUSE:
		{
//			[playBtn setEnabled:YES];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)                    
                [ playBtn setImage:[ UIImage imageNamed:@"play_ipad.png" ] forState: UIControlStateNormal ];                
            else
                [ playBtn setImage:[ UIImage imageNamed:@"play.png" ] forState: UIControlStateNormal ];
            
//			[pauseBtn setEnabled:NO];
//			[stopBtn setEnabled:YES];
			[ffBtn setEnabled:YES];
			[rewBtn setEnabled:YES];			
		}			
			break;
			
		case PlayState_STOP:			
		default:
		{
			[playBtn setEnabled:YES];
//			[pauseBtn setEnabled:NO];
//			[stopBtn setEnabled:NO];
			[ffBtn setEnabled:YES];
			[rewBtn setEnabled:YES];			
		}			
			break;
	}	 
	[clearBtn setEnabled:YES];	
	
	if (playerMode == PlayerMode_NORMAL && song == [MP3List count]-1)
		[ffBtn setEnabled:NO];
	
	if (playerMode == PlayerMode_SHUFFLE)
		[rewBtn setEnabled:NO];
	
	[textBtn setEnabled:YES];
	
	[self updateUIControls];
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) updateUIControls
{
//	if (playBtn.enabled == YES)
//		playBtn.alpha=1.0;
//	else
//		playBtn.alpha=0.5;
	
	
//	if (pauseBtn.enabled == YES)
//		pauseBtn.alpha=1.0;
//	else
//		pauseBtn.alpha=0.5;
//	
//	if (stopBtn.enabled == YES)
//		stopBtn.alpha=1.0;
//	else
//		stopBtn.alpha=0.5;
//	
	
//	if (ffBtn.enabled == YES)
//		ffBtn.alpha=1.0;
//	else
//		ffBtn.alpha=0.5;
//	
//	
//	if (rewBtn.enabled == YES)
//		rewBtn.alpha=1.0;
//	else
//		rewBtn.alpha=0.5;
//	
//	if (clearBtn.enabled == YES)
//		clearBtn.alpha=1.0;
//	else
//		clearBtn.alpha=0.5;
//	
//	
//	if (textBtn.enabled == YES)
//		textBtn.alpha=1.0;
//	else
//		textBtn.alpha=0.5;
	
	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/




/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (IBAction) readText
{
	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference musics = proxy->getCategoryByName(CONSTmusicsCollection);
	CMMLCategory::MMLCategoryReference works = proxy->getCategoryByName(CONSTworksCollection);
	///ObjC
	NSString *idTrilha;	
	///</init>	
	idTrilha=[MP3List objectAtIndex:song];
	proxy->setCurrentSong( musics->getEntityByKeyValue("id-trilha",[ idTrilha UTF8String ] ));
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ ApplicationManager GetInstance];
	[instance performTransitionToView:screen_TEXT ];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
        return 59;
    else
        return 28;
}

- (IBAction) gotoPlaylists {
    PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
	[instance performTransitionToView:screen_PLAYLISTS ];    
}


- (IBAction) onRepeatBtn {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if ( playerMode == PlayerMode_NORMAL || playerMode == PlayerMode_SHUFFLE ) {
            playerMode = PlayerMode_REPEAT_LIST;
            [ repeatBtn setImage: [ UIImage imageNamed:@"btRepeatOn_ipad.png" ]  forState: UIControlStateNormal];
            
        } else if ( playerMode == PlayerMode_REPEAT_LIST ) {
            playerMode = PlayerMode_REPEAT_SONG;
            [ repeatBtn setImage: [ UIImage imageNamed:@"btRepeatOneOn_ipad.png" ]  forState: UIControlStateNormal];
        } else if ( playerMode == PlayerMode_REPEAT_SONG ) {
            playerMode = PlayerMode_NORMAL;
            [ repeatBtn setImage: [ UIImage imageNamed:@"btRepeat_ipad.png" ]  forState: UIControlStateNormal];
        }  
        
    } else {
        if ( playerMode == PlayerMode_NORMAL || playerMode == PlayerMode_SHUFFLE ) {
            playerMode = PlayerMode_REPEAT_LIST;
            [ repeatBtn setImage: [ UIImage imageNamed:@"btRepeatOn.png" ]  forState: UIControlStateNormal];
            
        } else if ( playerMode == PlayerMode_REPEAT_LIST ) {
            playerMode = PlayerMode_REPEAT_SONG;
            [ repeatBtn setImage: [ UIImage imageNamed:@"btRepeatOneOn.png" ]  forState: UIControlStateNormal];
        } else if ( playerMode == PlayerMode_REPEAT_SONG ) {
            playerMode = PlayerMode_NORMAL;
            [ repeatBtn setImage: [ UIImage imageNamed:@"btRepeat.png" ]  forState: UIControlStateNormal];
        }  
        
    }
    
    
}

- (IBAction) onShuffleBtn {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if ( playerMode != PlayerMode_SHUFFLE ) {
            playerMode = PlayerMode_SHUFFLE;
            [ shuffleBtn setImage: [ UIImage imageNamed:@"btShuffleOn_ipad.png" ]  forState: UIControlStateNormal];
        } else {
            playerMode = PlayerMode_NORMAL;
            [ shuffleBtn setImage: [ UIImage imageNamed:@"btShuffle_ipad.png" ]  forState: UIControlStateNormal];
        }

    } else {
        if ( playerMode != PlayerMode_SHUFFLE ) {
            playerMode = PlayerMode_SHUFFLE;
            [ shuffleBtn setImage: [ UIImage imageNamed:@"btShuffleOn.png" ]  forState: UIControlStateNormal];
        } else {
            playerMode = PlayerMode_NORMAL;
            [ shuffleBtn setImage: [ UIImage imageNamed:@"btShuffle.png" ]  forState: UIControlStateNormal];
        }
        
    }
}

- (IBAction) savePlaylist {
    
    if ( [ MP3List count ] == 0 )
        return;
    
//    prompt = [AlertPrompt alloc];
//    prompt = [prompt initWithTitle: NSLocalizedString( @"SAVE_LIST_POPUP_TITLE", nil ) message:@"Digite um nome para sua lista" delegate:self cancelButtonTitle:@"Cancelar" okButtonTitle:@"Ok"];
//    [prompt show];
    
    prompt = [ AlertPrompt alloc ];
    prompt = [ prompt initWithTitle: NSLocalizedString( @"SAVE_LIST_POPUP_TITLE", nil ) message: NSLocalizedString( @"SAVE_LIST_POPUP_TEXT", nil )  delegate: self cancelButtonTitle: NSLocalizedString( @"CANCEL", nil ) okButtonTitle:@"Ok" ];
    [ prompt show ];


}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != [alertView cancelButtonIndex])
    {
        NSString *entered = [(AlertPrompt *)alertView enteredText];
        char * tmpchar;
        CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
        CMMLCategory::MMLCategoryReference playlists=proxy->getCategoryByName(CONSTplaylistsCollection);
        CMMLEntity::MMLEntityReference playlist = new CMMLEntity();
        
        playlist->setValueForKey( "nome", [ entered UTF8String ] );
        
        int howmany= [ MP3List count ];
        
        for (int c = 0; c < howmany; ++c )
        {
            tmpchar = (char*)[ ((NSString *)[ MP3List objectAtIndex: c ]) UTF8String ];
            playlist->setValueForKey( ItoA(c) , tmpchar );
        }
        
        playlist->setValueForKey("size", ItoA( howmany ) );
        playlists->addEntity( playlist );
//        NSLog( @"dump: \n%s", playlist->dump().c_str() );
        playlists->commit();
        PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
        [instance performTransitionToView:screen_PLAYLISTS ];    
    }
    [prompt release];    
}


- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (((UIView *)[cell.subviews lastObject]).frame.size.width == 1.0)
    {
        ((UIView *)[cell.subviews lastObject]).backgroundColor =
        [UIColor clearColor];
    }
}
@end
