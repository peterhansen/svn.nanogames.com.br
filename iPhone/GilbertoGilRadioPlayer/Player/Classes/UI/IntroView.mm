//
//  IntroView.m
//  Player
//
//  Created by Daniel Monteiro on 4/16/10.
//  Copyright 2010 Nano Games. All rights reserved.
//


/// C++
#include <sstream> 
#include <map>

/// 3rd Party
#include "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#import "HTTPBridge.h"
/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#include "RefazendaProxy.h"
#import "PlayerAppDelegate.h"

/// View
#import "IntroView.h"

@implementation IntroView


- ( void ) onBecomeCurrentScreen {
    [ self setHidden: NO ];
    [ self onEntrar ];
}

- (void) startLoading
{
	PlayerAppDelegate* appController=(PlayerAppDelegate*)[ApplicationManager GetInstance];	
	[appController initDataWithSelector: @selector(notifyFinishedLoading) onObject:self];	
}

- (void) notifyFinishedLoading
{
	PlayerAppDelegate* appController=(PlayerAppDelegate*)[ApplicationManager GetInstance];		
	[appController setBusy:NO withListener:nil onObject:nil];		
	[self gotoMenu];
}
 
/*==============================================================================================
 
 MENSAGEM onEntrarBtnPressed:
 Entra no sistema
 
 ================================================================================================*/

- (IBAction) onEntrarBtnPressed
{
    [ self onEntrar ];
}

- ( void ) onEntrar
{
	if ([self isLocked])
		return;
	
	PlayerAppDelegate* appController=(PlayerAppDelegate*)[ApplicationManager GetInstance];	
	[appController setBusy:YES withListener: @selector(startLoading) onObject:self];
}

/*==============================================================================================
 
 MENSAGEM dealloc:
 Destrutor
 
 ================================================================================================*/

- (void)dealloc 
{
    [super dealloc];
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildIntroView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildIntroView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( bool )buildIntroView
{
	[self setHidden: NO ];
	
	return true;
}

@end
