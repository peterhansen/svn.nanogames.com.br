//
//  AlbumSongViewCell.h
//  Player
//
//  Created by Daniel Monteiro on 5/31/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <string>

@interface AlbumSongViewCell : UITableViewCell {
    IBOutlet UILabel* songName;    
    IBOutlet UIButton *addButton;
    IBOutlet UIButton *textButton;
    NSString *trackId;
}

- ( IBAction ) addToPlaylist;
- ( IBAction ) seeText;
- ( void ) resetAddButton;
@property (nonatomic, retain) IBOutlet NSString* trackId;
@property (nonatomic, retain) IBOutlet UIButton *textButton;
@property (nonatomic, retain) IBOutlet UILabel* songName;


+ ( UIColor *) getSelectorColor;

+ ( UIColor *) getUnselectorColor;

@end
