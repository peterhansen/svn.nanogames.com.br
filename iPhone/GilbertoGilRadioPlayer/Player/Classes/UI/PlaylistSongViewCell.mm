//
//  PlaylistSongViewCell.mm
//  Player
//
//  Created by Daniel Monteiro on 6/1/11.
//  Copyright 2011 Nano Games. All rights reserved.
//
#import "Config.h"
#include "HTTPAudioStreamController.h"
#include "RadioView.h"


#import "PlaylistSongViewCell.h"
#import "AlbumSongViewCell.h"

@implementation PlaylistSongViewCell

@synthesize songName;
@synthesize trackNum;
@synthesize playlistListener;
@synthesize table;
@synthesize manager;
@synthesize relocateSign;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}


- (IBAction)edit:(id)sender
{
    [table setEditing:!table.editing animated:YES];
    
    for (UITableViewCell *cell in [table visibleCells])
    {
        if (((UIView *)[cell.subviews lastObject]).frame.size.width == 1.0)
        {
            ((UIView *)[cell.subviews lastObject]).backgroundColor =
            [UIColor clearColor];
        }
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    for (UIControl *control in self.subviews) {
        if (control.frame.size.width == 1.0f) {
            control.backgroundColor = [UIColor clearColor];
        }                
    }
}

- ( IBAction ) removeFromPlaylist {
    [ playlistListener removeTrack: trackNum ];
}

- ( void ) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
//    if ( CGRectContainsPoint( relocateSign.frame, [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: self ] ) ) {
//        [ table setScrollEnabled: NO ];
//    } else {
//        CGPoint touch = [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: self ];
//
//        [ table setScrollEnabled: YES ];
//        
//        if ( [ super hitTest: touch  withEvent: event ] != self )
            [ super touchesBegan: touches withEvent: event ];
//    }
}


- ( void ) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    [ table setScrollEnabled: YES ];    
//    
    [ super touchesEnded: touches withEvent: event ];

        
}

- ( void ) setPlaying:(BOOL)selected {

    UIColor *color;

    if ( selected == YES )
        color = [ AlbumSongViewCell getSelectorColor ];
    else
        color = [ AlbumSongViewCell getUnselectorColor ];
    

//    [ bg setBackgroundColor: color ];
    [ deleteBtn setBackgroundColor: color ];
    [ relocateSign setBackgroundColor: color ];
    [ songName setBackgroundColor: color ];
}

- ( void ) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {

//    CGPoint touch = [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: self ];
//    CGPoint previous = [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) previousLocationInView: self  ];
//    
//        if ( CGRectContainsPoint( relocateSign.frame, [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: self ] ) ) {
//           
//            if ( touch.y - previous.y > 0 ) {
//                [ manager swap: trackNum withIndex: trackNum + 1 ];
//            } else if ( touch.y - previous.y < 0 ) {                
//                [ manager swap: trackNum withIndex: trackNum - 1 ];                
//            } 
//            
//            [ table reloadData ];
//            [ table setNeedsLayout ];
//            [ table setNeedsDisplay ];
//            [ table setScrollEnabled: NO ];            
//
//        }  else {
//            [ table setScrollEnabled: YES ];
//            
//            if ( [ super hitTest: touch  withEvent: event ] != self )
                [ super touchesMoved: touches withEvent: event ];
//        }
}


@end
