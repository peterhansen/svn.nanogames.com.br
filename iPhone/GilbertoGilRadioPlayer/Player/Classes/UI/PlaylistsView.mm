/// C++
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"
#import "PlaylistViewCell.h"
#import "PlaylistsView.h"


@implementation PlaylistsView

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildPlaylistsView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildPlaylistsView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}


- (void)dealloc {
    [super dealloc];
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

-( bool )buildPlaylistsView
{
	[self setHidden: YES];	
	return true;
}


- (void) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];
    [ table reloadData ];
}

- ( void ) gotoMenu {
    PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
	[instance performTransitionToView:screen_RADIO ];
}





- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
        return 58;
    else
        return 29;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	CRefazendaProxy *proxy = CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference playlists = proxy->getCategoryByName(CONSTplaylistsCollection);
#ifdef DEBUG    
    NSLog(@"total playlists: %d" , ( playlists->getTotalItems() - 1 ) );
#endif    
    return playlists->getTotalItems() - 1;
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	CRefazendaProxy *proxy = CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference playlists = proxy->getCategoryByName(CONSTplaylistsCollection);
	
	
    static NSString *CellIdentifier = @"PlaylistViewCell";
	
    PlaylistViewCell *cell = (  PlaylistViewCell *) [ tableView dequeueReusableCellWithIdentifier: CellIdentifier ];
    
    if (cell == nil) 
	{
        
        NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed: CellIdentifier owner:nil options: nil ];
        
        
        for ( id currentObject in topLevelObjects ) {
            if ( [ currentObject isKindOfClass:[  PlaylistViewCell class ] ] ) {
                cell = (  PlaylistViewCell *) currentObject;
                break;
            }
        }
        
    }
    cell.container = tableView;
    cell.listNameLabel.text = [ NSString stringWithCString: playlists->getEntityById( indexPath.row + 1 )->getValueByKey("nome").c_str() encoding: NSUTF8StringEncoding ];
    
	return cell;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    std::string nome = std::string(
    [ [ [ (( PlaylistViewCell *) [ tableView cellForRowAtIndexPath: indexPath ]) listNameLabel ] text ] UTF8String ]);
     CMMLDatabase::MMLDatabaseReference proxy = CRefazendaProxy::getInstance();
    CMMLCategory::MMLCategoryReference playlists = proxy->getCategoryByName( CONSTplaylistsCollection );
    CMMLEntity::MMLEntityReference mainPlaylist = playlists->getEntityByKeyValue( "nome", CONSTdefaultPlaylist );
    CMMLEntity::MMLEntityReference myPlaylist = playlists->getEntityByKeyValue( "nome", nome );
    
//	NSLog( @"dump: \n%s", myPlaylist->dump().c_str() );
    
    int size = AtoI( myPlaylist->getValueByKey("size").c_str() );
    int mainSize = AtoI( mainPlaylist->getValueByKey("size").c_str() ); 
    
    for ( int c = 0; c < size; ++c ) {
        mainPlaylist->setValueForKey(ItoA( mainSize + c ),myPlaylist->getValueByKey(ItoA(c)));
    }
    
    mainPlaylist->setValueForKey("size", ItoA( size + mainSize ) );
    PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
	[instance performTransitionToView:screen_RADIO ];

	
	return nil;
	
}


@end