//
//  AlbumSongViewCell.mm
//  Player
//
//  Created by Daniel Monteiro on 5/31/11.
//  Copyright 2011 Nano Games. All rights reserved.
//
/// C++
#include <sstream> 
#include <map>

/// 3rd party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "Config.h"
#include "RefazendaProxy.h"
#import "PlayerAppDelegate.h"
/// View 
#import "AlbumSongViewCell.h"


@implementation AlbumSongViewCell

@synthesize textButton;
@synthesize songName;
@synthesize trackId;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [ self resetAddButton ];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

- ( void ) resetAddButton {
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        [ addButton setImage: [ UIImage imageNamed:@"add_ipad.png" ] forState: UIControlStateNormal ]; 
    else
        [ addButton setImage: [ UIImage imageNamed:@"add.png" ] forState: UIControlStateNormal ];
    
    UIColor *color;
    
    color = [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ];
    [ songName setBackgroundColor: color ];
    [ color autorelease ];
     

    color = [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ];
    [ addButton setBackgroundColor: color ];
    [ color autorelease ];

    
    color = [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ];
    [ textButton setBackgroundColor: color ];
    [ color autorelease ];
}

- ( IBAction ) addToPlaylist {
    
    CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference playlists;
	CMMLEntity::MMLEntityReference playlist;	
	playlists=proxy->getCategoryByName(CONSTplaylistsCollection);
	playlist=playlists->getEntityByKeyValue("nome",CONSTdefaultPlaylist);
	int playlistsize = AtoI( playlist->getValueByKey("size").c_str() );	
	
    int count = AtoI( playlist->getValueByKey( "size" ).c_str() );

    
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
        [ addButton setImage: [ UIImage imageNamed:@"add_2_ipad.png" ] forState: UIControlStateNormal ];
    else
        [ addButton setImage: [ UIImage imageNamed:@"add2.png" ] forState: UIControlStateNormal ];
        
    
    
    [ self setNeedsLayout ];
    [ self setNeedsDisplay ];
        
    try	{
        playlist->setValueForKey(ItoA( playlistsize ), [ trackId UTF8String ] );	        
		playlist->setValueForKey("size", ItoA( playlistsize + 1 ) );
	} catch (MMLNonExistantEntity *e) {
		
	}    

    
    UIColor *color;
    
    color = [ [ UIColor alloc ] initWithRed: ( 230.0f /  255.0f ) green: ( 233.0f / 255.0f ) blue: ( 234.0f / 255.0f ) alpha: 1.0f ];
    [ songName setBackgroundColor: color ];
    [ color autorelease ];
    
    color = [ [ UIColor alloc ] initWithRed: ( 230.0f /  255.0f ) green: ( 233.0f / 255.0f ) blue: ( 234.0f / 255.0f ) alpha: 1.0f ];
    [ addButton setBackgroundColor: color ];
    [ color autorelease ];
    
    color = [ [ UIColor alloc ] initWithRed: ( 230.0f /  255.0f ) green: ( 233.0f / 255.0f ) blue: ( 234.0f / 255.0f ) alpha: 1.0f ];
    [ textButton setBackgroundColor: color ];
    [ color autorelease ];
}

+ ( UIColor *) getSelectorColor {
    return [ UIColor colorWithRed: ( 230.0f /  255.0f ) green: ( 233.0f / 255.0f ) blue: ( 234.0f / 255.0f ) alpha: 1.0f  ];
}

+ ( UIColor *) getUnselectorColor {
    return [ UIColor colorWithRed: ( 195.0f /  255.0f ) green: ( 210.0f / 255.0f ) blue: ( 215.0f / 255.0f ) alpha: 1.0f ];
}

- ( IBAction ) seeText {
    CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference musics = proxy->getCategoryByName(CONSTmusicsCollection);
	///ObjC
	///</init>	
	proxy->setCurrentSong( musics->getEntityByKeyValue("id-trilha",[ trackId UTF8String ]));
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ ApplicationManager GetInstance];
	[instance performTransitionToView:screen_TEXT ];
}

@end
