//
//  BioView.h
//  Player
//
//  Created by Daniel Monteiro on 9/3/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"
#import "SingleTapEnabledScrollView.h"

@interface BioView :  BaseGilbertoGilView<UIScrollViewDelegate>
{
	BOOL updated;
	///<InterfaceBuilder>
	/// saida do RSS
    IBOutlet UIWebView *yearOut;
    IBOutlet SingleTapEnabledScrollView *yearList;
    NSInteger index;
	GDataXMLDocument *doc;
}

- (IBAction) forceUpdate;
///</InterfaceBuilder>


///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildBioView;
///</ciclo de vida>

- (void) updateContent;
- (void) updateFor: ( int ) year;
- ( void ) updateUI;
@property ( nonatomic, retain ) IBOutlet SingleTapEnabledScrollView *yearList;
@end
