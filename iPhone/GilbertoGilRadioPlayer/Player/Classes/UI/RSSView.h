//
//  RSSView.h
//  Player
//
//  Created by Daniel Monteiro on 4/16/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"

#define MODE_HEADLINES 0
#define MODE_DETAILS 1

@interface RSSView : BaseGilbertoGilView<UIScrollViewDelegate,UITableViewDataSource, UIWebViewDelegate >
{
	BOOL updated;
    int mode;
    int lastSelection;
    NSString *htmlToLoad;
	///<InterfaceBuilder>
    IBOutlet UITableView *headlines;
    IBOutlet UIWebView *webView;
}
- (IBAction) forceUpdate;
///</InterfaceBuilder>

///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildRSSView;
///</ciclo de vida>
- (void) loadHTML;
- (void) updateContent;
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)webViewDidStartLoad:(UIWebView *)webView;
@end 
