//
//  AboutView.mm
//  Player
//
//  Created by Daniel Monteiro on 5/23/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "AboutView.h"


@implementation AboutView

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildAboutView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildAboutView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}


- (void)dealloc {
    [super dealloc];
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

-( bool )buildAboutView
{
	[self setHidden: YES];	
    [ aboutDisplay setDelegate: self ];
	return true;
}


- ( void ) onBeforeTransition {
    [ aboutDisplay loadHTMLString:@"<html><body></body></html>" baseURL: nil ];
}

- (void) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    [ aboutDisplay loadHTMLString: 
         [ NSString stringWithContentsOfFile: [ [NSBundle mainBundle] pathForResource: ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? @"about_ipad" : @"about" ofType:@"html" ]  encoding:NSUTF8StringEncoding error: nil ]
                          baseURL: baseURL  ];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:request.URL];
        return false;
    }
    return true;
}

@end
