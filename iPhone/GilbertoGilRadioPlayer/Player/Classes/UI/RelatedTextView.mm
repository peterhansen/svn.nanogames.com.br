//
//  RelatedTextView.mm
//  Player
//
//  Created by Daniel Monteiro on 9/23/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// C++
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"
/// View
#import "RelatedTextView.h"


@implementation RelatedTextView


/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildRelatedTextView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildRelatedTextView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM buildRelatedTextView
 Inicializa a view.
 
 ================================================================================================*/
-( bool )buildRelatedTextView
{
	[self setHidden: YES];	
    contentRaw = [ [ NSMutableString alloc ] init ];
	return true;
	
}
/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 Inicializa a view.
 
 ================================================================================================*/

- (void) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];
    [ relatedText loadHTMLString: @"<html><body style = 'background-color:#d4e2e7; text-align:left;' ></body></html>" baseURL: nil ];
    [ NSThread detachNewThreadSelector:@selector( updateContent ) toTarget: self withObject: self ];
//	tickerTimer= [NSTimer scheduledTimerWithTimeInterval: 0.0
//														   target: self
//														 selector: @selector(updateContent:)
//														 userInfo: nil
//														  repeats: NO];	 	
}
/*==============================================================================================
 
 MENSAGEM onBeforeTransition
 Inicializa a view.
 
 ================================================================================================*/

- (void) onBeforeTransition
{
	[super onBeforeTransition];
    [ relatedText loadHTMLString:@"<html><body></body></html>" baseURL: nil ];
}
/*==============================================================================================
 
 MENSAGEM getFileName:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/


- ( std::string ) getFileName
{
	std::string filename;	
	
    if ( CRefazendaProxy::getInstance()->getCurrentSong()!=NULL) {
        filename="song";                
		filename+=CRefazendaProxy::getInstance()->getCurrentSong()->getValueByKey("id-trilha");
    }
    

	
	if (CRefazendaProxy::getInstance()->getCurrentAlbum()!=NULL) {
        filename="album";                
		filename+=CRefazendaProxy::getInstance()->getCurrentAlbum()->getValueByKey("refazenda-id");
        
    }
	
	return filename;
}
/*==============================================================================================
 
 MENSAGEM getUrl:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

- (const char*) getUrl
{
	std::string url;
	
	if (CRefazendaProxy::getInstance()->getCurrentSong()!=NULL)
	{
		url="http://gilbertogil.com.br/app_gil/info_lista_texto_faixa.php?id=";
		url+=CRefazendaProxy::getInstance()->getCurrentSong()->getValueByKey("id-trilha");
	}
	
	if (CRefazendaProxy::getInstance()->getCurrentAlbum()!=NULL)
	{
		url="http://gilbertogil.com.br/app_gil/info_texto_teste.php?id=";
		url+=CRefazendaProxy::getInstance()->getCurrentAlbum()->getValueByKey("refazenda-id");
	}
	
	return url.c_str();
}
/*==============================================================================================
 
 MENSAGEM updateContent:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/



- (void) updateContent
{

    NSAutoreleasePool *pool = [ [ NSAutoreleasePool alloc ] init ]; 
	try 
	{		
        
        if (CRefazendaProxy::getInstance()->getCurrentAlbum()==NULL && CRefazendaProxy::getInstance()->getCurrentSong()==NULL )
            return;
        
        
        NSString *fullPathToFile =[ NSString stringWithFormat:@"%@cached/%s", [PlayerAppDelegate getDataDir], [self getFileName].c_str() ];
        NSString *contentRawTmp;	
        
        NSURL *url=[ NSURL URLWithString:[ NSString stringWithUTF8String: [ self getUrl ] ] ];
        
        contentRawTmp = [NSString stringWithContentsOfFile:fullPathToFile encoding:NSISOLatin1StringEncoding error: nil ];

        if ( contentRawTmp != nil )
            [ contentRaw setString: contentRawTmp ];
        
#ifdef DEBUG
        NSLog(@"from cache :%@",contentRaw);
#endif		
        
        
        ///obtem o conteúdo da URL
        contentRawTmp = [ NSString stringWithContentsOfURL: url encoding: NSISOLatin1StringEncoding error: nil ];    
        
        
        ///limpa eventuais artefatos de formatação espurios
        if (contentRawTmp!=nil && [contentRawTmp length]!=0)
        {
            [ contentRaw setString: contentRawTmp ];
            [ contentRaw writeToFile:fullPathToFile atomically:NO encoding:NSISOLatin1StringEncoding error: nil ];
        }
        
         
        [ self performSelectorOnMainThread: @selector( doUpdateContent ) withObject: self waitUntilDone: YES ];
        
	}
	catch (Exception *ex) 
	{
		
	}
	
    [ pool release ];
}

///*==============================================================================================
// 
// MENSAGEM goBack:
// Construtor chamado quando carregamos a view via código.
// 
// ================================================================================================*/


- (IBAction) gotoMenu
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
	
//	if (CRefazendaProxy::getInstance()->getCurrentAlbum()!=NULL) {
//        CRefazendaProxy::getInstance()->setCurrentSong( NULL );
//        CRefazendaProxy::getInstance()->setCurrentAlbum( NULL );
//        [instance performTransitionToView:screen_DISCO ];
//    }
//		
//	
//	if (CRefazendaProxy::getInstance()->getCurrentSong()!=NULL) {
//        CRefazendaProxy::getInstance()->setCurrentSong( NULL );
//        CRefazendaProxy::getInstance()->setCurrentAlbum( NULL );        
//        [instance performTransitionToView:screen_RADIO ];	
//    }
		
    [ instance back ];
	
}

/*==============================================================================================
 
 MENSAGEM doUpdateContent:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

- (void) doUpdateContent
{
    NSMutableString *content;
    content = [[NSMutableString alloc] init];        
    
    [ content appendString: @"<body>"];	    
    
    
    ///o texto é referente a um album
    if (contentRaw!=nil && [contentRaw length]!=0 && CRefazendaProxy::getInstance()->getCurrentAlbum()!=NULL)
    {
        GDataXMLDocument *doc;
        doc=[GDataXMLDocument alloc];		
        [doc initWithXMLString:contentRaw  options:0 error: nil ];		
        GDataXMLNode *root;
        root=[doc rootElement];
        
        if (root == nil) {
            NSLog(@"conteudo do XML:\n%@",contentRaw);
            throw new Exception("XML mal-formado");
        }
        
        NSArray *items;
        NSArray *titulos;		
        NSArray *autores;		        
        
        items = [root nodesForXPath:@"/textos/info/texto" error: nil ];			
        titulos = [root nodesForXPath:@"/textos/info/titulo" error: nil ];
        autores = [root nodesForXPath:@"/textos/info/autor" error: nil ];        
        
        for (GDataXMLElement *item in items) 
        {
            
            //titulo
            [ content appendString:@"<h1>" ];
            
            [content appendString: [[titulos objectAtIndex:[items indexOfObject:item]] stringValue ]];
            [ content appendString:@"</h1>" ];
            [ content appendString:@"<h4>" ];
            [content appendString: [[autores objectAtIndex:[items indexOfObject:item]] stringValue ]];
            [ content appendString:@"</h4>" ];
            
            //corpo
            [ content appendString:@"<p>" ];
            [content appendString:[item stringValue]];
            [ content appendString:@"</p>" ];            
            [ content replaceOccurrencesOfString:@"\n" withString: @"" options:NSCaseInsensitiveSearch range: NSMakeRange( 0,[ content length ] ) ];                                                                                                                               
        }
        
        if ([items count] ==0)
            [content setString: NSLocalizedString( @"TEXT_ERROR", nil )  ];
        [ content appendString: @"</body>"];       
        [ content insertString: [ NSString stringWithFormat:@"<html><head>%@</head>", CRefazendaProxy::getInstance()->getStyle() ] atIndex: 0];        
        
        [ relatedText loadHTMLString: [ content stringByReplacingOccurrencesOfString:@"\\" withString:@"'" ] baseURL: nil ];
        
        
#ifdef DEBUG
        NSLog(@"to cache :%@",content);
#endif		
        [doc autorelease];
    }
    
    
    
    ///o texto é referente a uma música
    if (contentRaw!=nil && [contentRaw length]!=0 && CRefazendaProxy::getInstance()->getCurrentSong()!=NULL)
    {
        GDataXMLDocument *doc;
        doc=[GDataXMLDocument alloc];        
        
        CMMLCategory::MMLCategoryReference crewMembers = CRefazendaProxy::getInstance()->getCategoryByName(CONSTcrewMembersCollection);
        CMMLEntity::MMLEntityReference crewMember;
        
        CMMLCategory::MMLCategoryReference crewParties = CRefazendaProxy::getInstance()->getCategoryByName(CONSTcrewPartiesCollection);        
        CMMLEntity::MMLEntityReference crewParty;        

        CMMLCategory::MMLCategoryReference works = CRefazendaProxy::getInstance()->getCategoryByName(CONSTworksCollection);
        CMMLEntity::MMLEntityReference work;
        std::string idObra;
        std::string idTrilha;        
        
        [doc initWithXMLString:contentRaw  options:0 error: nil ];		
        GDataXMLNode *root;
        root=[doc rootElement];
        if (root == nil)
        {
            NSLog(@"conteudo do XML:\n%@",contentRaw);
            throw new Exception("XML mal-formado");
        }
        
        NSArray *items1;
        NSArray *items2;		
        
        [ content appendString:@"<h1>"];
        idObra = CRefazendaProxy::getInstance()->getCurrentSong()->getValueByKey("id-obra");
        idTrilha = CRefazendaProxy::getInstance()->getCurrentSong()->getValueByKey("id-trilha");        
        crewParty = crewParties->getEntityByKeyValue( "id-trilha", idTrilha.c_str() );
                
        work = works->getEntityByKeyValue("id-obra", idObra );
        
        [ content appendString: [ NSString stringWithUTF8String: work->getValueByKey("nome").c_str() ] ];
        
        [ content appendString:@"</h1>"];        
        
        
        
        items1 = [ root nodesForXPath:@"/lista_textos_faixas/faixa/pessoas/pessoa" error: nil ];
        
        [ content appendString:@"<p>"];            
        std::string idWorker;
        
        for ( GDataXMLElement *pessoa_node in items1 ) {                        
            
            items2 = [ pessoa_node nodesForXPath:@"nome" error: nil ];
            
            if ( [ items2 count ] > 0 )                                
                [ content appendString: [ [ items2 objectAtIndex: 0 ] stringValue ] ];

            [ content appendString: @"<br/>" ];            
        }
        
        [ content appendString:@"</p>"]; 
        
        items1= [root nodesForXPath:@"/lista_textos_faixas/faixa/letra" error: nil ];			
        
        for (GDataXMLElement *item in items1) 
        {
            [ content appendString:@"<p>"];            
            [content appendString:[item stringValue]];
            [ content appendString:@"</p>"];            
        }
        
        
        items1 = [ root nodesForXPath:@"/lista_textos_faixas/faixa/pessoas/pessoa" error: nil ];
        
        [ content appendString:@"<p>"];            
                
        for ( GDataXMLElement *pessoa_node in items1 ) {                        
            
            items2 = [ pessoa_node nodesForXPath: @"editora" error: nil ];
            
            if ( [ items2  count ] > 0 )
                [ content appendString: [ [ items2 objectAtIndex: 0 ] stringValue ] ];
            
            [ content appendString: @"<br/>" ];            
        }
        
        [ content appendString:@"</p>"]; 
        
        
        items2= [root nodesForXPath:@"/lista_textos_faixas/faixa/obs" error: nil ];			
        
        for (GDataXMLElement *item in items2) 
        {   
            [ content appendString:@"<p>"];
            [content appendString:[item stringValue]];
            [ content appendString:@"</p>"];                		            
        }
        
        if ([items1 count] ==0 && [items2 count]==0)
            [content setString: NSLocalizedString( @"TEXT_ERROR", nil ) ];		
        
        
        [ content replaceOccurrencesOfString:@"\n" withString: @"<br/>" options:NSCaseInsensitiveSearch range: NSMakeRange( 0, [ content length ] ) ];
        

        [ content appendString:@"<p>"];            
        [ content appendString: NSLocalizedString( @"PERSONEL_INFO", nil )];            

        for ( int personIndex = 0; personIndex < crewParty->getTotalFields() - 1; ++personIndex ) {
            idWorker = crewParty->getValueByKey( ItoA( personIndex ) ); 
            crewMember = crewMembers->getEntityByKeyValue( "id-worker", idWorker.c_str() );

            [ content appendString: [ NSString stringWithFormat:@"%@ - %@<br/>" ,
                                     [ NSString stringWithCString: crewMember->getValueByKey("nome") .c_str()  encoding: NSUTF8StringEncoding ]                                      
                                     ,[ NSString stringWithCString: crewMember->getValueByKey("funcao") .c_str() encoding: NSUTF8StringEncoding
                                       ] 
                                     ]
             ];            
        }
        [ content appendString:@"</p>"]; 
        [ content appendString: @"</body></html>"];

        [ content insertString: [ NSString stringWithFormat:@"<html><head>%@</head>", CRefazendaProxy::getInstance()->getStyle() ] atIndex: 0];        
        
        [ relatedText loadHTMLString: content baseURL: nil ];       
        
#ifdef DEBUG
        NSLog(@"to cache :%@",content);
#endif		
        [doc autorelease];
    }
    
    
    [content autorelease];   
	
}


@end
