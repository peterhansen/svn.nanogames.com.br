//
//  HeadlineViewCell.h
//  Player
//
//  Created by Daniel Monteiro on 6/2/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HeadlineViewCell : UITableViewCell {
    IBOutlet UILabel *headline;
    IBOutlet UILabel *sourceAndDate;
    IBOutlet UIWebView *webView;
}

@property (nonatomic, retain) IBOutlet UILabel *headline;
@property (nonatomic, retain) IBOutlet UILabel* sourceAndDate;
@property (nonatomic, retain) IBOutlet UIWebView *webView;

@end
