//
//  PhotosView.h
//  Player
//
//  Created by Daniel Monteiro on 4/16/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"
#import "SingleTapEnabledScrollView.h"
#import "GilbertoGilTitleButton.h"

enum ViewMode {THUMBS,FULLPHOTO};



@interface PhotosView : BaseGilbertoGilView< UIScrollViewDelegate, UITableViewDelegate,UITableViewDataSource >
{
	///Foto atual sendo vista
	int CurrentPhoto;
	ViewMode currentViewMode;
	CGRect imageOriginalFrame;
///<InterfaceBuilder>
	IBOutlet UIImageView *ImageView;
    IBOutlet UIWebView *PhotoLabel;
    IBOutlet SingleTapEnabledScrollView *photoSlider;
    IBOutlet UITableView *photographers;
    IBOutlet GilbertoGilTitleButton *logoBtn;
    int currentPhotographer;
    NSMutableArray *photographerList;
    bool lockedTouches;
//    UIScrollView *scrollImageFrame;
    int imageWidth;
    int imageHeight;
    int imageVerticalSpacing;
    int imageHorizontalSpacing;
}
- (IBAction) refresh;
- (IBAction) next;
- (IBAction) previous;
- (IBAction) gotoAgenda;
///<InterfaceBuilder>

///obtem a url da foto desejada
+ (NSString*) getPictureUrl: (int) index withMode: ( int ) mode;
+ ( std::string ) getPhotoFileName: ( int ) index withMode: ( int ) mode ;
- (NSString*) getPictureUrl;

///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;
- ( void ) refreshTable;
/// costrutor de fato
-( bool )buildPhotosView;
///</ciclo de vida>
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event ;
-( std::string )getCurrentFileName;
- ( void ) buildPhotographerList;
- ( void ) dispathBuildAlbumScrollViewWithAuthor: ( NSString * ) author;
- ( void ) setPhotoText;
- ( int ) totalPhotosForPhotographer: ( NSString * ) name;
@end
