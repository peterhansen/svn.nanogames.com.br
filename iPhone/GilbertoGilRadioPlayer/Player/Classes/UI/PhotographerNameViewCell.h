//
//  PhotographerNameViewCell.h
//  Player
//
//  Created by Daniel Monteiro on 6/6/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PhotographerNameViewCell : UITableViewCell { 
    IBOutlet UILabel *nameLabel;
    IBOutlet UIImageView *bg;    
    bool filtered;
}
- ( void ) updateBG;
- ( void ) setFiltered:(bool) makeFiltered;
@property (nonatomic, retain) IBOutlet UILabel *nameLabel;

@end
