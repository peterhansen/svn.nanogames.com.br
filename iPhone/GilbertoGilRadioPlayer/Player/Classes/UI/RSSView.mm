//
//  RSSView.m
//  Player
//
//  Created by Daniel Monteiro on 4/16/10.
//  Copyright 2010 Nano Games. All rights reserved.
//
/// C++
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// Aplicacao
#import "HTTPBridge.h"
#import "Config.h"
#import "PlayerAppDelegate.h"

/// View
#import "RSSView.h"
#import "HeadlineViewCell.h"

/// C++
#include <sstream> 
#include <map>
#include <vector>

/// 3rd Party
#import "GDataXMLNode.h"

/// Aplicacao
#import "Config.h"
#import "PlayerAppDelegate.h"
#include "Tweet.h"


/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"
@implementation RSSView



- ( void ) onBeforeTransition {
    [ super onBeforeTransition ];
    [ webView loadHTMLString:@"<html><body></body></html>" baseURL: nil ];
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildRSSView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildRSSView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

-( bool )buildRSSView
{
    mode = MODE_HEADLINES;
	[self setHidden: YES];
	updated=NO;
    htmlToLoad = nil;
	return true;
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- ( void ) becameCurrentScreen {
    //	if (updated == NO)
    //		NSTimer *tickerTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
    // 												 			    target:self
    //											 				  selector:@selector(updateOnTimer:)
    //															  userInfo:nil
    //															   repeats:NO];	 	
    
    mode = MODE_HEADLINES;
    [ webView setHidden: YES ];
    [ headlines setHidden: NO ];
    [ headlines reloadData ];    
    [ self bringSubviewToFront: headlines ];
    
    //        [ self setNeedsLayout ];
    //        [ self setNeedsDisplay ];
    
}

- (void) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: YES withListener:@selector( becameCurrentScreen ) onObject: self ];    
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (void) updateOnTimer: (NSTimer*) timer
{
	[timer invalidate];
	[self updateContent];	
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (IBAction) forceUpdate
{
	[self updateContent];
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) updateContent
{
	
    CRefazendaProxy::getInstance()->updateRSSContent();
	updated=YES;	
	[ self setNeedsDisplay ];
    lastSelection = -1;
    [ headlines reloadData ];    

//	[finalContent autorelease];
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void)dealloc 
{
    [super dealloc];
}

- ( void ) gotoMenu {
    if ( mode == MODE_DETAILS ) {
        mode = MODE_HEADLINES;
        [ webView setHidden: YES ];
        [ headlines setHidden: NO ];
        [ headlines reloadData ];
        [ self bringSubviewToFront: headlines ];
        [ self setNeedsLayout ];
        [ self setNeedsDisplay ];
    } else 
        [ super gotoMenu ];
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    mode = MODE_DETAILS;
    
    [ headlines reloadData ];
    [ headlines setHidden: YES ];
    [ webView setHidden: NO ];
    [ self bringSubviewToFront: webView ];
    
    NSRange range;
    NSMutableString *tmpContent;    
    GDataXMLElement *item;
    
    if ( CRefazendaProxy::getInstance()->getNewsDoc() == nil || [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] == nil )
        return nil;
    
    
    NSArray *items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:@"/lista_noticias/noticia" error: nil ];			
    
    if ( mode == MODE_HEADLINES || lastSelection < 0 || lastSelection >= [ items count ] )
        item = [ items objectAtIndex: indexPath.row ];
    else
        item = [ items objectAtIndex: lastSelection ];
    
    lastSelection = indexPath.row;
    
    //prepara o corpo do texto da notícia
    tmpContent = [ NSMutableString stringWithString: @"<html>" ];

    [ tmpContent appendString:@"<head>"];
    [ tmpContent appendString: CRefazendaProxy::getInstance()->getStyle() ];    
    [ tmpContent appendString:@"</head>"];    
    
    [ tmpContent appendString:@"<body>"];

    //título
    items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:[ NSString stringWithFormat:@"/lista_noticias/noticia[%d]/titulo", lastSelection + 1 ] error: nil ];    

    [ tmpContent appendString: @"<h1>" ];
    [ tmpContent appendString: [((GDataXMLElement*)[ items objectAtIndex:0 ]) stringValue ] ];    
    [ tmpContent appendString: @"</h1>" ];
    
    
    //autor
    items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:[ NSString stringWithFormat:@"/lista_noticias/noticia[%d]/veiculo", lastSelection + 1 ] error: nil ];    
    
    [ tmpContent appendString: @"<h4>" ];
    [ tmpContent appendString: [((GDataXMLElement*)[ items objectAtIndex:0 ]) stringValue ] ];    

    
    
    //data
    items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:[ NSString stringWithFormat:@"/lista_noticias/noticia[%d]/data", lastSelection + 1 ] error: nil ];    

    [ tmpContent appendString: @" - " ];
    [ tmpContent appendString: [((GDataXMLElement*)[ items objectAtIndex:0 ]) stringValue ] ];    
    [ tmpContent appendString: @"</h4>" ];

    
    //conteúdo
    [ tmpContent appendString: @"<p>" ];    
    items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:[ NSString stringWithFormat:@"/lista_noticias/noticia[%d]/texto", lastSelection + 1 ] error: nil ];    
    
    [ tmpContent appendString: [((GDataXMLElement*)[ items objectAtIndex:0 ]) stringValue ] ];
    [ tmpContent appendString: @"</p>" ];    
    
    
    //substituir caracteres e tags que não são usadas.
    range=NSMakeRange(0, [tmpContent length]);
    
    [tmpContent replaceOccurrencesOfString:@"style=\"" withString:@"_oldstyle=\"" options:NSCaseInsensitiveSearch range: range];
    
    
    items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:[ NSString stringWithFormat:@"/lista_noticias/noticia[%d]/imagem", lastSelection + 1 ] error: nil ];
    
    
    if ( [ items count ] > 0 ) {
        [ tmpContent appendString: [ NSString stringWithFormat:@"<img src = '%@'/>" , [ ((GDataXMLElement*)[ items objectAtIndex:0 ]) stringValue  ] ] ];        
    }        
//    else
//        NSLog(@"sem imagens");
    
    
    [ tmpContent appendString: @"</body></html>" ];
    htmlToLoad = tmpContent;    
    [ tmpContent retain ];
//    [ webView loadHTMLString: tmpContent baseURL: nil ];
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: YES withListener:@selector( loadHTML ) onObject: self ];
//    [ self loadHTML ];
    
    
    [ webView setHidden: NO ];
    [ self setNeedsLayout ];
    [ self setNeedsDisplay ];
    
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
        return 60;        
    else
        return 48;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{ 
    UITableViewCell *baseCell;
    
//    if ( mode == MODE_HEADLINES ) {    
    
	NSString *MyIdentifier = @"HeadlineViewCell";
	
    baseCell = [ tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if ( baseCell == nil /*|| [ baseCell isKindOfClass: [ NewsDetailViewCell class ] ]*/ ) { 
        
        NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:MyIdentifier owner:nil options:nil ];
        
        for ( id currentObject in topLevelObjects ) {
            if ( [ currentObject isKindOfClass:[ HeadlineViewCell class ] ] ) {
                baseCell = currentObject;
                break;
            }
        }
        
    } else {
        NSLog( @"célula ja existe " );
    }
    HeadlineViewCell * cell = (HeadlineViewCell *) baseCell;
   
    GDataXMLElement *subitem;
    NSRange range;
    NSMutableString *tmpContent;    
    GDataXMLElement *item;
    
    if ( CRefazendaProxy::getInstance()->getNewsDoc() == nil || [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] == nil )
        return cell;

    
    NSArray *items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:@"/lista_noticias/noticia" error: nil ];			
    
    if ( mode == MODE_HEADLINES || lastSelection < 0 || lastSelection >= [ items count ] )
        item = [ items objectAtIndex: indexPath.row ];
    else
        item = [ items objectAtIndex: lastSelection ];
    
    subitem = [ [ item nodesForXPath:@"titulo" error: nil ] objectAtIndex:0 ];
    tmpContent = [ NSMutableString stringWithString: [ subitem stringValue ] ];
    range=NSMakeRange(0, [tmpContent length]);
    [tmpContent replaceOccurrencesOfString:@"\n" withString:@"" options:NSCaseInsensitiveSearch range: range];
    cell.headline.text = tmpContent;
//    [ tmpContent autorelease ];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    subitem = [ [ item nodesForXPath:@"veiculo" error: nil ] objectAtIndex:0 ];
    tmpContent = [ NSMutableString stringWithString: [ subitem stringValue ] ];
    range=NSMakeRange(0, [tmpContent length]);
    [tmpContent replaceOccurrencesOfString:@"\n" withString:@"" options:NSCaseInsensitiveSearch range: range];
    cell.sourceAndDate.text = tmpContent;
//    [ tmpContent autorelease ];
    
    subitem = [ [ item nodesForXPath:@"data" error: nil ] objectAtIndex:0 ];
    tmpContent = [ NSMutableString stringWithString: [ subitem stringValue ] ];
    range=NSMakeRange(0, [tmpContent length]);
    [tmpContent replaceOccurrencesOfString:@"\n" withString:@"" options:NSCaseInsensitiveSearch range: range];
    cell.sourceAndDate.text = [ cell.sourceAndDate.text stringByAppendingString: @" - " ];    
    cell.sourceAndDate.text = [ cell.sourceAndDate.text stringByAppendingString: tmpContent ];

    return baseCell;    
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ( CRefazendaProxy::getInstance()->getNewsDoc() != NULL );
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ( mode == MODE_HEADLINES ) {
        if ( CRefazendaProxy::getInstance()->getNewsDoc() != nil ) {
            NSArray *items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:@"/lista_noticias/noticia" error:nil ];			
            int count = [ items count];
            return ( ( count > 20 ) ? 20 : count );
        } else
            return 0;
    } else {
        return 0;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: NO withListener: nil onObject: nil ];   
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
    
}

- (void) loadHTML {
   
    [ webView loadHTMLString: htmlToLoad baseURL: nil ];

}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: NO withListener: nil onObject: nil ];}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: YES withListener: nil onObject: nil ];
}

@end
