//
//  PlaylistsView.h
//  Player
//
//  Created by Daniel Monteiro on 6/3/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <Foundation/Foundation.h>
/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"

@interface PlaylistsView : BaseGilbertoGilView < UITableViewDelegate, UITableViewDataSource > {
    IBOutlet UITableView *table;
}
///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildPlaylistsView;
///</ciclo de vida>
@end
