//
//  TweetCellView.m
//  Player
//
//  Created by Daniel Monteiro on 5/24/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "TweetCellView.h"


@implementation TweetCellView

@synthesize tweetLabel;
@synthesize userLabel;
@synthesize realNameLabel;
@synthesize dateLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [ tweetLabel setEditable: NO ];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
