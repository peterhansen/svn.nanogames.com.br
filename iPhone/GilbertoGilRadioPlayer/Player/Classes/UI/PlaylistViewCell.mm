//
//  PlaylistViewCell.mm
//  Player
//
//  Created by Daniel Monteiro on 6/6/11.
//  Copyright 2011 Nano Games. All rights reserved.
//
/// C++
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"
#import "PlaylistViewCell.h"


@implementation PlaylistViewCell

@synthesize listNameLabel;
@synthesize container;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

-( IBAction ) deleteList {
    std::string nome = std::string( [ [ listNameLabel  text ] UTF8String ] );
    
    CMMLDatabase::MMLDatabaseReference proxy = CRefazendaProxy::getInstance();
    CMMLCategory::MMLCategoryReference playlists = proxy->getCategoryByName( CONSTplaylistsCollection );
    playlists->removeEntity( playlists->getEntityByKeyValue( "nome", nome ) );
    playlists->commit();
    [ container reloadData ];
}

@end
