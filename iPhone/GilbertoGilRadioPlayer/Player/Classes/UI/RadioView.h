//
//  RadioView.h
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"
#import "Config.h"
#import "OnDemandLoadImageView.h"
#import "AlertPrompt.h"

@interface RadioView :  BaseGilbertoGilView <UITableViewDataSource,UITableViewDataSource,UIAlertViewDelegate>
{
	///C
	PlayerModeEnum playerMode;
	int Volume;
	int Progress;
	int song;

	///ObjC
	NSMutableArray   *MP3List;	
	NSTimer *updateTicker;
	
	///C++
	HTTPAudioStreamController *audioController;
	
	///<InterfaceBuilder>
	IBOutlet UIButton *shuffleBtn;	
	IBOutlet UIButton *repeatBtn;		
    IBOutlet UIButton *readTextBtn;
	IBOutlet UIButton *clearBtn;	
	IBOutlet UIButton *textBtn;		
	IBOutlet UIButton *playBtn;
	IBOutlet UIButton *ffBtn;
	IBOutlet UIButton *rewBtn;
    IBOutlet UIButton *saveBtn;
	IBOutlet UITableView *playlistTable;
    IBOutlet UILabel *songPosition;
    IBOutlet UILabel *songLength;
    IBOutlet UIImageView *songProgress;
    IBOutlet UIImageView *songProgressBox;
    IBOutlet UIWebView *songInfo;
    IBOutlet OnDemandLoadImageView *cover;
    AlertPrompt *prompt;
}
- (IBAction) play;
- (IBAction) stop;
- (IBAction) pause;
- (IBAction) next;
- (IBAction) previous;
- (IBAction) clear;
- (IBAction) onPlayerModeChanged;
- (IBAction) gotoPlaylists;
- (IBAction) savePlaylist;
- (IBAction) onRepeatBtn;
- (IBAction) onShuffleBtn;
///</InterfaceBuilder>


- (void) InitRadio;
- (void) updatePlaylistPanel;

///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildRadioView;
///</ciclo de vida>

- (void) terminateTimer;

- (void) initTimer;

- (IBAction) readText;

- (void) playlistIsEmpty;

- (void) playlistIsNotEmpty;

- (void) updateUIControls;

- (void) scrollToSong;

- ( void ) removeTrack: ( int ) trackNum;

- ( void ) swap: ( int ) index withIndex: ( int ) otherIndex;
@end
