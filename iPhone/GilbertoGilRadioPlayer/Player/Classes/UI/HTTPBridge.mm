/*
 *  CHTTPBridge.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 4/30/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <iostream>
#include "HTTPBridge.h"


CHTTPBridge* CHTTPBridge::Instance = NULL;

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

CHTTPBridge::CHTTPBridge():
NetMan( this ) , Data(""),listener(NULL),Status(READY)
{
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

CFStringRef CHTTPBridge::GetDeviceUserAgent( void )
{ 
	std::string userAgentBuffer;
	
	DeviceInterface::GetDeviceModel( userAgentBuffer, false );
	userAgentBuffer.append( " ## " );
	
	DeviceInterface::GetDeviceSystemName( userAgentBuffer, true );
	userAgentBuffer.append( " " );
	
	DeviceInterface::GetDeviceSystemVersion( userAgentBuffer, true );
	userAgentBuffer.append( " ## " );
	
	DeviceInterface::GetDeviceUID( userAgentBuffer, true );
	
#if DEBUG
	LOG( ">>> NanoOnline - User Agent: %s\n", userAgentBuffer.c_str() );
#endif
	
	return CFStringCreateWithCString( kCFAllocatorDefault, userAgentBuffer.c_str(), kCFStringEncodingUTF8 );
}


/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

CHTTPBridge * CHTTPBridge::getInstance()
{
	if (!Instance)
		Instance=new CHTTPBridge();
	
	return Instance;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

void CHTTPBridge::Destroy()
{
	delete Instance;
	Instance=NULL;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/


CHTTPBridge::CHTTPBridgeStatus CHTTPBridge::getStatus()
{
	return Status;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/


void CHTTPBridge::sendPost(const char* url, const char* body,const INetworkListener *Listener)
{
	request("POST",url,body,Listener);
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

void CHTTPBridge::request(const char*cmd,const char* url, const char* body,const INetworkListener *Listener)
{
	Status=BUSY;
	NSLog([NSString stringWithFormat:@"sending %s to %s with %s",cmd,url,body]);
	bool ret = false;	
	listener=(INetworkListener*)Listener;
	CFURLRef UrlRef = NULL;
	CFDataRef BodyDataRef = NULL;
	CFHTTPMessageRef RequestRef = NULL;
	MemoryStream data;
	CFStringRef UrlStrAuxRef=CFStringCreateWithCString(kCFAllocatorDefault,url,kCFStringEncodingUTF8);
	
	if( !UrlStrAuxRef )	
	{		
		NSLog(@"unable to create http request string (1)");
		throw UNABLE_TO_CREATE_HTTP_REQUEST_STRING;
	}
	
	UrlRef = CFURLCreateWithString( kCFAllocatorDefault, UrlStrAuxRef, NULL );
	
	
	if( !UrlRef )
	{
		NSLog(@"unable to create http request string (2)");
		throw UNABLE_TO_CREATE_HTTP_REQUEST_STRING;	
	}

	
	RequestRef = CFHTTPMessageCreateRequest(kCFAllocatorDefault,CFStringCreateWithCString( NULL, cmd, kCFStringEncodingUTF8 ),UrlRef, kCFHTTPVersion1_1);
	if( !RequestRef )
	{
		NSLog(@"unable to create http message");
		throw UNABLE_TO_CREATE_HTTP_MSG;
	}
	
	
	CFStringRef UserAgentRef = GetDeviceUserAgent();
	
	if( !UserAgentRef )
	{
		CFKILL( RequestRef);
		NSLog(@"unable to obtain user agent");
		throw UNABLE_TO_OBTAIN_USER_AGENT;
	}
	
	CFHTTPMessageSetHeaderFieldValue( RequestRef, CFSTR( "User-Agent" ), UserAgentRef );
	


	data.writeUTF8String(body);
	BodyDataRef=CFDataCreate( kCFAllocatorDefault, data.begin(), data.getSize() );
	if( !BodyDataRef)
	{
		CFKILL( RequestRef);
		NSLog(@"unable to create data buffer (1)");
		throw CANT_CREATE_DATA_BUFFER;
	}
	
	
	
	CFHTTPMessageSetBody(RequestRef,BodyDataRef);

	
	// Envia a requisição
	ret = NetMan.sendRequest(RequestRef );
	if( !ret )
	{
		CFKILL( RequestRef );
		NSLog(@"unable to create data buffer (2)");
		throw CANT_CREATE_DATA_BUFFER;
		
	}	
	
	data.clear();
	CFKILL( BodyDataRef );
	CFKILL( UserAgentRef );
	CFKILL( UrlStrAuxRef );	
	CFKILL( UrlRef );	
	
	
	std::cout << "sending request..." << std::endl;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

void CHTTPBridge::onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr )
{
		NSLog([NSString stringWithFormat:@"network error!%d %s",errorCode, errorStr.c_str() ] );
		Status=ERROR;
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

void CHTTPBridge::onNetworkProgressChanged( int32 currBytes, int32 totalBytes )
{
	
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

void CHTTPBridge::onNetworkRequestCancelled( void )
{
		NSLog(@"request cancelled");
	Status=READY;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/


void CHTTPBridge::onNetworkRequestCompleted( std::vector< uint8 >& data )
{
	std::cout << "onNetworkRequestCompleted" << std::endl;
	if (listener!=NULL)		
		listener->onNetworkRequestCompleted(data);
	else
	{
 
		MemoryStream dataStream;
		dataStream.swapBuffer( data );
		uint8 *buffer=(uint8*)malloc(dataStream.getSize());
		dataStream.readData(buffer, dataStream.getSize());
		Data=(char*)buffer;
		free(buffer);
	
	}
	listener=NULL;	 
	Status=READY;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

const std::string CHTTPBridge::getData()
{
	return Data;
}
