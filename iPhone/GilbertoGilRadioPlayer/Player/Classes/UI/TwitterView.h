//
//  TwitterView.h
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"

@interface TwitterView : BaseGilbertoGilView <UITableViewDataSource,UITableViewDataSource>
{
	//timer para a atualizacao dos dados (padrao: 10 segundos)
	NSTimer *tickerTimer;
	
	///<InterfaceBuilder>
	/// saida do RSS
	IBOutlet UISlider *timerSlider;	
	IBOutlet UILabel *updateLabel;
	IBOutlet UITableView *tweetsTable;
    IBOutlet UILabel *TwitterLabel;
    IBOutlet UIButton *refreshBtn;
    IBOutlet UIButton *back;
	BOOL connectedToInternet;
}
- (IBAction) forceUpdate;
- (IBAction) sliderChanged;
///</InterfaceBuilder>

///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildTwitterView;
///</ciclo de vida>

- (void) updateContent;

- (void) setUpdateRate: (int) interval;
- (void) terminateTimer;
- (void) initTimerForMinutes: (int) minutes;
+ (NSString *) filterText: ( NSString * ) text;
@end
