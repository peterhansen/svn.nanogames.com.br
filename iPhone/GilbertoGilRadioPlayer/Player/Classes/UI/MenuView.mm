//
//  MenuView.m
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//


/// C++
#include <string>
#include <vector>
#include <sstream> 
#include <map>
#import <QuartzCore/QuartzCore.h>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"
/// View
#include "Tweet.h"
#import "MenuView.h"
#import "HeadlineViewCell.h"
#import "TweetCellView.h"
#import "PhotosView.h"
#import "BriefTweetViewCell.h"
#import "BriefHeadline.h"
#import "DiscoView.h"

@implementation MenuView

/*==============================================================================================
 
 MENSAGEM dealloc:
 Destrutor
 
 ================================================================================================*/

- (void)dealloc 
{
    [super dealloc];
}

/*==============================================================================================
 
 MENSAGEM gotoRadio:
 Vai para a visao de radio
 
 ================================================================================================*/

- (IBAction) gotoRadio
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_RADIO ];
}
/*==============================================================================================
 
 MENSAGEM gotoDisco:
 Vai para a visao de discografia
 
 ================================================================================================*/

- (IBAction) gotoDisco
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_DISCO];
}
/*==============================================================================================
 
 MENSAGEM gotoRss:
 Vai para a visao de noticias
 
 ================================================================================================*/

- (IBAction) gotoRss
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_RSS ];
}
/*==============================================================================================
 
 MENSAGEM gotoPhotos:
 Vai para a visao de fotos
 
 ================================================================================================*/

- (IBAction) gotoPhotos
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_PHOTOS ];	
}
/*==============================================================================================
 
 MENSAGEM gotoTwitter:
 Vai para a visao de Twitter
 
 ================================================================================================*/

- (IBAction) gotoTwitter
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];
	[instance performTransitionToView:screen_TWITTER ];
}

/*==============================================================================================
 
 MENSAGEM gotoBio:
 Vai para a visao de biografia
 
 ================================================================================================*/

- (IBAction) gotoBio
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_BIO ];
}

/*==============================================================================================
 
 MENSAGEM gotoAbout:
 Vai para a tela de créditos
 
 ================================================================================================*/

- (IBAction) gotoAbout
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_ABOUT ];
}


- (IBAction) gotoAgenda {
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_AGENDA ];    
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildMenuView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildMenuView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

- ( void ) downloadRSS {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];     
    CRefazendaProxy *proxy = CRefazendaProxy::getInstance();
    proxy->updateRSSContent();
    proxy->updateTwitterContent();
    
    
    PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];
    
    if ( [ instance getCurrentView ] == screen_MENU ) {
        [ self performSelectorOnMainThread:@selector( updateRSSData ) withObject: self waitUntilDone: YES ];
    }        
    [ pool release ];
}

- ( void ) updateRSSData {
    [ twitterPreview reloadData ];
    [ newsClipping reloadData ];
    
    [ twitterPreview setNeedsLayout ];
    [ twitterPreview setNeedsDisplay ];    
    [ newsClipping setNeedsLayout ];
    [ newsClipping setNeedsDisplay ];
}

/*==============================================================================================
 
 MENSAGEM buildMenuView:
  Inicializa a view.
 
 ================================================================================================*/

-( bool )buildMenuView
{
	[self setHidden: YES];    
    
	return true;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {    
    return ( CRefazendaProxy::getInstance()->getCategoryByName( CONSTalbumsCollection )->getTotalItems() >=1 ) ? 1 : 0;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    ( CRefazendaProxy::getInstance()->getCategoryByName( CONSTalbumsCollection )->getTotalItems() >=1 ) ? 1 : 0;
    
    return tableView == twitterPreview? ( ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 2 : 1 ) : ( ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ? 4 : 3 );
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if ( tableView == twitterPreview ) {
        NSString *MyIdentifier = @"BriefTweetViewCell";
        
        BriefTweetViewCell *cell = (BriefTweetViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) { 
            
            
            NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:@"BriefTweetViewCell" owner:nil options:nil ];
            
            for ( id currentObject in topLevelObjects ) {
                if ( [ currentObject isKindOfClass:[ BriefTweetViewCell class ] ] ) {
                    cell = ( BriefTweetViewCell *) currentObject;
                    break;
                }
            }
        }    
        
        if ( CRefazendaProxy::getInstance()->getTweets().size() > 0 )  {
            Tweet *t = CRefazendaProxy::getInstance()->getTweets()[ indexPath.row ];
            
            
            cell.dateLabel.text = [ NSString stringWithCString: t->getPubDate().c_str() encoding: NSUTF8StringEncoding ];
            cell.tweetLabel.text = [ NSString stringWithCString: t->getTweetText().c_str() encoding: NSUTF8StringEncoding ];
        } else {
            if ( indexPath.row > 0)
                cell.tweetLabel.text = @""; 
                cell.dateLabel.text = @"";
        }
        
        
        [ cell setFrame:CGRectMake(0, 0, 284, 84 ) ];
        return cell;        
    } else {
        BriefHeadline *baseCell;
        
        //    if ( mode == MODE_HEADLINES ) {    
        
        NSString *MyIdentifier = @"BriefHeadline";
        
        baseCell = [ tableView dequeueReusableCellWithIdentifier: MyIdentifier ];
        
        if ( baseCell == nil /*|| [ baseCell isKindOfClass: [ NewsDetailViewCell class ] ]*/ ) { 
            
            NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:MyIdentifier owner:nil options:nil ];
            
            for ( id currentObject in topLevelObjects ) {
                if ( [ currentObject isKindOfClass:[ BriefHeadline class ] ] ) {
                    baseCell = currentObject;
                    break;
                }
            }
            
        } 
        BriefHeadline * cell = (BriefHeadline *) baseCell;
        
        GDataXMLElement *subitem;
        NSRange range;
        NSMutableString *tmpContent;    
        GDataXMLElement *item;
        
        cell.hidden = YES;
        
        if ( CRefazendaProxy::getInstance()->getNewsDoc() == nil || [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] == nil )
            return cell;
        
        cell.hidden = NO;        
        
        NSArray *items = [ [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] nodesForXPath:@"/lista_noticias/noticia" error: nil ];			
        
        item = [ items objectAtIndex: indexPath.row ];
        
        subitem = [ [ item nodesForXPath:@"titulo" error: nil ] objectAtIndex:0 ];
        tmpContent = [ NSMutableString stringWithString: [ subitem stringValue ] ];
        range=NSMakeRange(0, [tmpContent length]);
        [tmpContent replaceOccurrencesOfString:@"\n" withString:@"" options:NSCaseInsensitiveSearch range: range];
        cell.headline.text = tmpContent;
        
        return baseCell;  
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)    
        return tableView == twitterPreview? 75 : 36;
    else
        return tableView == twitterPreview? 129 : 16;        
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];
    if ( tableView == twitterPreview ) {
        [instance performTransitionToView:screen_TWITTER ];        
    } else if ( tableView == newsClipping ) {
        [instance performTransitionToView:screen_RSS ];      
    }
	return nil;
}

- ( void ) onBecomeCurrentScreen {
    [ super onBecomeCurrentScreen ];
    
//TODO: tenta carregar previamente o conteúdo do twitter e das notícias
    [ twitterPreview reloadData ];
    [ newsClipping reloadData ];  
    if ( CRefazendaProxy::getInstance()->getNewsDoc() == nil || [ CRefazendaProxy::getInstance()->getNewsDoc() rootElement ] == nil )
    [ NSThread detachNewThreadSelector:@selector( downloadRSS ) toTarget: self withObject: self ];
    
    
    ///C++	
    std::string nome;
    ///MML
    CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
    CMMLCategory::MMLCategoryReference albums=proxy->getCategoryByName(CONSTalbumsCollection);
    
    CMMLCategory::MMLCategoryReference photos=proxy->getCategoryByName(CONSTphotosCollection);    
    
    ///</init>	
    
    CMMLEntity::MMLEntityReference album;
    
    int total;
    int randed;
    bool found;
    int tries;
    total = albums->getTotalItems();    
    std::vector< int > selected;
    srand ( time(NULL) );
    found = false; 
    tries = 0;
    do {
        randed = rand() % total;   
        
        for ( int c = 0; c < selected.size(); ++c ) {
            if ( selected[ c ] == randed ) {
                found = true;
                break;
            }                
        }
        ++tries;
    } while ( found && tries < albums->getTotalItems() );
    selected.push_back( randed );
    
    //se não tem, lamento muito...
    if ( albums->getTotalItems() == 0 )
        return;
    
    try {        
        
        album = albums->getEntityById( randed );
        
        [ disc0  scheduleImageLoad: [ NSString stringWithCString: album->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                      forDirectory: @"cached/discophotos"
                           withURL: [ DiscoView getPictureUrl: AtoI( album->getValueByKey("refazenda-id").c_str() ) ] 
                        withBorder: 1.0
                        AndWithAlternativeImage:@"icDisco.png"         
         ];        
        
        [ disc0.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );
        
        album = albums->getEntityById( randed );
        
        [ disc1  scheduleImageLoad: [ NSString stringWithCString: album->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                      forDirectory: @"cached/discophotos"
                           withURL: [ DiscoView getPictureUrl: AtoI( album->getValueByKey("refazenda-id").c_str() ) ] 
                        withBorder: 1.0     
                        AndWithAlternativeImage:@"icDisco.png"                  
         ];        
        [ disc1.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );
        
        album = albums->getEntityById( randed );
        
        [ disc2  scheduleImageLoad: [ NSString stringWithCString: album->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                      forDirectory: @"cached/discophotos"
                           withURL: [ DiscoView getPictureUrl: AtoI( album->getValueByKey("refazenda-id").c_str() ) ] 
                        withBorder: 1.0    
                        AndWithAlternativeImage:@"icDisco.png"                  
         ];
        [ disc2.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );  
        
        album = albums->getEntityById( randed );
        
        [ disc3  scheduleImageLoad: [ NSString stringWithCString: album->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                      forDirectory: @"cached/discophotos"
                           withURL: [ DiscoView getPictureUrl: AtoI( album->getValueByKey("refazenda-id").c_str() ) ] 
                        withBorder: 1.0     
                        AndWithAlternativeImage:@"icDisco.png"                  
         ];
        [ disc3.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );  
        
        album = albums->getEntityById( randed );
        
        [ disc4  scheduleImageLoad: [ NSString stringWithCString: album->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                      forDirectory: @"cached/discophotos"
                           withURL: [ DiscoView getPictureUrl: AtoI( album->getValueByKey("refazenda-id").c_str() ) ] 
                        withBorder: 1.0     
                        AndWithAlternativeImage:@"icDisco.png"          
         ];
        [ disc4.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );
        
        album = albums->getEntityById( randed );
        
        [ disc5  scheduleImageLoad: [ NSString stringWithCString: album->getValueByKey("refazenda-id").c_str() encoding: NSUTF8StringEncoding ] 
                      forDirectory: @"cached/discophotos"
                           withURL: [ DiscoView getPictureUrl: AtoI( album->getValueByKey("refazenda-id").c_str() ) ] 
                        withBorder: 1.0     
                        AndWithAlternativeImage:@"icDisco.png"                  
         ];
        [ disc5.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        
        
        selected.clear();
        
        total = photos->getTotalItems();       
        
        
        int pic;
        
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );
        
        pic = randed;
        
        [ photo0  scheduleImageLoad: [ NSString stringWithCString: [ PhotosView getPhotoFileName: pic withMode: THUMBS ].c_str() encoding: NSUTF8StringEncoding ]
                       forDirectory: @"cached/generalphotos"
                            withURL:     [ PhotosView getPictureUrl: pic withMode: THUMBS ] 
                         withBorder: 1.0     
                        AndWithAlternativeImage:@"icPhoto.png"
         ];
        
        [ photo0.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );
        
        pic = randed;
        [ photo1  scheduleImageLoad: [ NSString stringWithCString: [ PhotosView getPhotoFileName: pic withMode: THUMBS ].c_str() encoding: NSUTF8StringEncoding ]
                       forDirectory: @"cached/generalphotos"
                            withURL:     [ PhotosView getPictureUrl: pic withMode: THUMBS ] 
                         withBorder: 1.0     
                         AndWithAlternativeImage:@"icPhoto.png"         
         ];
        
        [ photo1.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        found = false; 
        tries = 0;
        do {
            randed = rand() % total;   
            
            for ( int c = 0; c < selected.size(); ++c ) {
                if ( selected[ c ] == randed ) {
                    found = true;
                    break;
                }                
            }
            ++tries;
        } while ( found && tries < albums->getTotalItems() );
        
        selected.push_back( randed );
        
        pic = randed;
        [ photo2  scheduleImageLoad: [ NSString stringWithCString: [ PhotosView getPhotoFileName: pic withMode: THUMBS ].c_str() encoding: NSUTF8StringEncoding ]
                       forDirectory: @"cached/generalphotos"
                            withURL:     [ PhotosView getPictureUrl: pic withMode: THUMBS ] 
                         withBorder: 1.0    
                        AndWithAlternativeImage:@"icPhoto.png"         
         ];
        [ photo2.layer setBorderColor: [[UIColor whiteColor] CGColor]];    
    } catch ( Exception e ) {
        //Se não funcionou nada aqui, lamento muito...
    }
}

@end
