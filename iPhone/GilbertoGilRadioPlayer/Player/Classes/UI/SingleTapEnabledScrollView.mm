//
//  SingleTapEnabledScrollView.mm
//  Player
//
//  Created by Daniel Monteiro on 5/25/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "SingleTapEnabledScrollView.h"


@implementation SingleTapEnabledScrollView

- (id)initWithFrame:(CGRect)frame 
{
    return [super initWithFrame:frame];
}

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{	
    // If not dragging, send event to next responder
    if (!self.dragging) 
        [self.nextResponder touchesEnded: touches withEvent:event]; 
    else
        [super touchesEnded: touches withEvent: event];
}

@end
