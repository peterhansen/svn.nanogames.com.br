//
//  PhotosView.m
//  Player
//
//  Created by Daniel Monteiro on 4/16/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

///<TODO>
/*
 - ver quais includes sao realmente necessarios
 - as actions podem ser estaticas?
 */
///</TODO>

/// C++
#include <iostream>
#include <string>
#include <sstream> 
#include <map>
#import <QuartzCore/QuartzCore.h>
/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "HTTPBridge.h"
#include "RefazendaProxy.h"
#import "PlayerAppDelegate.h"
#import "Config.h"
#import "OnDemandLoadImageView.h"
#import "PhotographerNameViewCell.h"

/// View
#import "PhotosView.h"

#define IMAGE_W_2_SPACING ( imageWidth + ( imageHorizontalSpacing * 2 ) )
#define ALL_PHOTOS NSLocalizedString( @"ALL", nil )

@implementation PhotosView


- ( void ) gotoMenu {
    [ super gotoMenu ];
    [ self onBeforeTransition ];
}

- ( void ) onBeforeTransition {
    ImageView.frame=imageOriginalFrame;
//    scrollImageFrame.frame = imageOriginalFrame;
    currentViewMode=THUMBS;
}

- ( void ) scrollTo: ( OnDemandLoadImageView * ) item  {
    if ( [ photoSlider.subviews indexOfObject: item ] != 0 ) {
        [ photoSlider scrollRectToVisible: CGRectMake( ( [ photoSlider.subviews indexOfObject: item ] * IMAGE_W_2_SPACING ) - ( IMAGE_W_2_SPACING >>1 ), 0, imageHeight + IMAGE_W_2_SPACING, imageHeight ) animated: YES ];
    } else {
        [ photoSlider scrollRectToVisible: CGRectMake( - IMAGE_W_2_SPACING >> 1, 0, IMAGE_W_2_SPACING, imageHeight )  animated: YES ];        
    }
    
    [ item.layer setBorderColor: [ [ UIColor colorWithRed: ( 98.0f/255.0f) green: ( 135.0f/255.0f) blue: ( 163.0f/255.0f) alpha: 1.0f ] CGColor ] ];
    
}



+ ( std::string ) getPhotoFileName: ( int ) index withMode: ( int ) mode {
	CMMLCategory::MMLCategoryReference photos;
	photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);
	
	std::string filename="";
	filename=photos->getEntityById(index)->getValueByKey("refazenda-id");	
	
	if ( mode == THUMBS)	
	{
		filename+="_p";
		filename+=photos->getEntityById(index)->getValueByKey("ext_p");
	}
	else 
	{
		filename+="_g";		
		filename+=photos->getEntityById(index)->getValueByKey("ext_g");
	}
	return filename;	
}

- ( std::string ) getCurrentFileName
{
	CMMLCategory::MMLCategoryReference photos;
	photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);
	
	std::string filename="";
	filename=photos->getEntityById(CurrentPhoto)->getValueByKey("refazenda-id");	
	
	if (currentViewMode==THUMBS)	
	{
		filename+="_p";
		filename+=photos->getEntityById(CurrentPhoto)->getValueByKey("ext_p");
	}
	else 
	{
		filename+="_g";		
		filename+=photos->getEntityById(CurrentPhoto)->getValueByKey("ext_g");
	}
	return filename;	
}


/*==============================================================================================
 
 MENSAGEM getPictureUrl:
 Obtem a url da foto desejada
 
 ================================================================================================*/
///<TODO>
/*
 - melhorar o tratamento de excessoes C++
 - precisa de tratamento de excessoes ObjC?
 - passar as URLs para constantes
 */
///</TODO>


 - (NSString*) getPictureUrl
 {
     return [ PhotosView getPictureUrl: CurrentPhoto  withMode: currentViewMode ];
// ///url base
// NSMutableString *url=[NSMutableString stringWithUTF8String:"http://gilbertogil.com.br/upload/fotos_2/"];
// 
// ///<Monta a URL em si>
// //	[url appendString:[NSString stringWithUTF8String:[self getCurrentFileName]]];
// 
// ///</Monta a URL em si>
// 
// #ifdef DEBUG	
// NSLog(@"url a ser buscada: %@",url);
// #endif
// 
// 
// std::string receiver=[self getCurrentFileName];
// return [NSString stringWithFormat:@"http://gilbertogil.com.br/upload/fotos_2/%s",receiver.c_str()];
// return url;
 }
 
 
+ (NSString*) getPictureUrl: (int) index withMode: ( int ) mode
{
//	NSLog(@"url: http://gilbertogil.com.br/upload/fotos_2/%s", [ PhotosView getPhotoFileName: index withMode: mode ].c_str() );
	return [NSString stringWithFormat:@"http://gilbertogil.com.br/upload/fotos_2/%s", [ PhotosView getPhotoFileName: index withMode: mode ].c_str() ];
}

/*==============================================================================================
 
 MENSAGEM dealloc:
 Destrutor
 
 ================================================================================================*/

- (void)dealloc 
{
    [super dealloc];
}

/*==============================================================================================
 
 MENSAGEM refresh:
 Atualiza a imagem sendo exibida
 
 ================================================================================================*/
///<TODO>
/*
 - Esta funcao precisa existir?
 - Usar ou nao uma thread separada?
 */
///</TODO>
- (IBAction)refresh
{
	[NSThread detachNewThreadSelector:@selector(fetchCurrentImage)  toTarget:self withObject:nil];
}



- (void) onDataFetched: (NSMutableData*) data
{
	if (data==nil)	{
		NSBundle *mainBundle = [NSBundle mainBundle];		
		data= [ NSMutableData dataWithContentsOfFile: [ mainBundle pathForResource:@"album" ofType:@"jpg" ] ];
	}
	
	UIImage *img= [ [ UIImage alloc ] initWithData:data ];
	
	///se se foi possivel reconstruir a imagem corretamente.
	if (img!=nil) {
		if ((currentViewMode == FULLPHOTO) && ([img size].width > [img size].height) ) {
			ImageView.transform = CGAffineTransformMakeRotation(3.14159265/2);
//            scrollImageFrame.transform = CGAffineTransformMakeRotation(3.14159265/2);
		} else {
			ImageView.transform = CGAffineTransformIdentity;
//            scrollImageFrame.transform = CGAffineTransformIdentity;
		}
		[ ImageView setBackgroundColor: [ UIColor blackColor ] ];
		[ ImageView setImage: img ];
		[ ImageView setNeedsDisplay ];        
//        [ scrollImageFrame setNeedsDisplay ];

	} else {
#ifdef DEBUG
		NSLog(@"img==nil in DiscoView");	
#endif        
	}
    [ img autorelease ];		
}


- (void) fetchServerImage
{
	CMMLCategory::MMLCategoryReference photos;
	std::string filename;
	photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);
	
	if (CurrentPhoto > photos->getTotalItems())
		throw new MMLNonExistantEntity();
	
	filename=[self getCurrentFileName];
    
#ifdef DEBUG    
	NSLog(@"obtendo foto: %s",filename.c_str());	
#endif    
	
	////////////////////////	
	
	NSString *photoDir;
	photoDir = [[PlayerAppDelegate getDataDir] stringByAppendingPathComponent:@"cached/generalphotos"];
	photoDir=[photoDir stringByAppendingString:@"/"];
	NSMutableData *data;
	//////////////////////	
	
	NSString *fullPathToFile=[photoDir stringByAppendingString:[NSString stringWithUTF8String:filename.c_str()]];	
	
	NSURL *url=[NSURL URLWithString: [self getPictureUrl]  ];
    
#ifdef DEBUG    
	NSLog(@"fetching:%@",[ self getPictureUrl ]);
#endif    
	
	if (url==nil)
	{
#ifdef DEBUG		
		NSLog(@"url==nil");
#endif		
	}
	data=[NSMutableData dataWithContentsOfURL:url];
	if (data!=nil)
		[data writeToFile:fullPathToFile atomically: YES ];		

	
}

- ( void ) refreshSlider {
    [ self scrollViewDidScroll: photoSlider ];
    [ self setNeedsLayout ];
    [ self setNeedsDisplay ];
}

- ( void ) dispathBuildAlbumScrollViewWithAuthor: ( NSString * ) author {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; 
    int count = 0;
    int menor = -1;
    int visiveis = 0;
    
    for ( OnDemandLoadImageView *view in photoSlider.subviews ) {
        [ view removeFromSuperview ];
    }

    
    CMMLCategory::MMLCategoryReference photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);			
    
    count = (photos->getTotalItems());
    

        try 
        {
            UIColor *color;
            OnDemandLoadImageView *photo;
            std::string filename;
            
            //130, 145
            for ( int c = 0; c < count; ++c ) {
                
                photo = [ [ OnDemandLoadImageView alloc ] initWithFrame:CGRectMake( ( visiveis * IMAGE_W_2_SPACING ) , imageVerticalSpacing >> 1, imageWidth + imageHorizontalSpacing, imageHeight ) ];
                
                if ( author != ALL_PHOTOS && photos->getEntityById( c )->getValueByKey("fotografo") != [ author UTF8String ] )
                    continue;
                
                if ( menor == -1 )
                    menor = c;
                
                filename = [ PhotosView getPhotoFileName: c  withMode: THUMBS ];
                
                [ photo  scheduleImageLoadDelayed:[ NSString stringWithCString: filename.c_str() encoding: NSUTF8StringEncoding ] 
                              forDirectory: @"cached/generalphotos"
                                   withURL: [ PhotosView getPictureUrl: c  withMode: THUMBS ] 
                 withBorder: 5.0
                 AndWithAlternativeImage:@"icPhoto.png"
                 ];
                
                if ( c < 5 ) {
                    [ photo doLoad ];                    
                    [ self performSelectorOnMainThread:@selector( setNeedsDisplay ) withObject: photo waitUntilDone: YES ];
                }

                
                photo.itemId = c;
                color = [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ];
                [ photo setBackgroundColor: color ];
                
                [ photoSlider addSubview: photo ];   
                [ photo autorelease ];
                [ color autorelease ];                
                ++visiveis;
            }
            [ photoSlider setClipsToBounds: YES ];
            [ photoSlider setContentSize:CGSizeMake( visiveis * IMAGE_W_2_SPACING, imageHeight ) ];
            [ photoSlider setScrollEnabled: YES ];        
        }
        catch (MMLNonExistantEntity *ex) 
        {//
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)                        
                [ PhotoLabel loadHTMLString: 
                 NSLocalizedString( @"ERROR_PHOTO_HTML_IPAD", nil ) baseURL: nil ] ;        
            else
                [ PhotoLabel loadHTMLString: 
                 NSLocalizedString( @"ERROR_PHOTO_HTML_IPOD", nil ) baseURL: nil ] ;        
 
        }
    
    [ pool release ];
    CurrentPhoto = menor;
    [ self setPhotoText ];
    [ self fetchServerImage ];
    [ self scrollTo: [ photoSlider.subviews objectAtIndex: 0 ] ];
//    if ( callback != nil && obj != nil )
//        [obj performSelector:callback];
//    [ photoSlider setNeedsLayout ];
    [ self performSelectorOnMainThread:@selector( refreshSlider ) withObject: self waitUntilDone: YES ];
    
    [ self performSelectorOnMainThread:@selector( setNeedsDisplay ) withObject: self waitUntilDone: YES ];

    
//    [ self performSelectorOnMainThread:@selector( refreshTable ) withObject: self waitUntilDone: YES ];
//    [ self refreshTable ];
    
}






/*==============================================================================================
 
 MENSAGEM fetchCurrentImage:
 Obtem a imagem atual, buscando primeiro no cache
 
 ================================================================================================*/
///<TODO>
/*
 - testar se o tratamento de erros e' e efetivo e suficiente
 - imagino que essa AutoRelease Pool nao seja necessaria, a nao ser que essa requisicao nao seja feita
 em uma thread separada
 */
///</TODO>

- (void) fetchCurrentImage
{
	try {
		
		CMMLCategory::MMLCategoryReference photos;
		std::string filename;
		
		photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);
		
		if (CurrentPhoto > photos->getTotalItems())
			throw new MMLNonExistantEntity();
		
		filename=[self getCurrentFileName];

#ifdef DEBUG        
		NSLog(@"buscando foto: %s",filename.c_str());
#endif		
		
		////////////////////////	
		NSFileManager *filemgr;
		NSString *photoDir;
		filemgr =[NSFileManager defaultManager];	
		photoDir = [[PlayerAppDelegate getDataDir] stringByAppendingPathComponent:@"cached/generalphotos"];
		photoDir=[photoDir stringByAppendingString:@"/"];
		NSMutableData *data;
		//////////////////////	
		
		NSString *fullPathToFile=[photoDir stringByAppendingString:[NSString stringWithUTF8String:filename.c_str()]];
		
		if ([[filemgr contentsOfDirectoryAtPath:photoDir error: nil ] indexOfObject:[NSString stringWithUTF8String:filename.c_str()]]==NSNotFound)
		{
            [ self fetchServerImage ];
		}

		data = [ NSMutableData dataWithContentsOfFile: fullPathToFile ];
		[ self onDataFetched: data];
		[ ImageView setNeedsDisplay ];
		
	}
	catch (MMLInvalidEntityId *ex)  
	{
//		[PhotoText setText:[NSString stringWithFormat:@"foto nao disponivel no momento: %s" , ex->what()]];
	}
	catch (MMLBaseException *ex) 
	{
//		[PhotoText setText:[NSString stringWithFormat:@"erro desconhecido: %s" , ex->what()]];
	}
	
	
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

- (void) setPhotoText
{
	try
	{

		CMMLCategory::MMLCategoryReference photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);	

        //lamento muito...
        if ( photos->getTotalItems() == 0 )
            return;
        
		if (CurrentPhoto > photos->getTotalItems())
			throw new MMLNonExistantEntity();
		
		CMMLEntity::MMLEntityReference photo=photos->getEntityById(CurrentPhoto);

        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)           
            [ PhotoLabel loadHTMLString: 
             [ NSString stringWithFormat:@"<html>%@<body><span style = 'font-family:Verdana; font-size:14px; color:#103e5e; font-weight: normal;' >%@</span><br/><span style = 'font-family:Verdana; font-size:12px; color:#646464; font-weight: normal;'>&copy; %@</span></body></html>", 
                         CRefazendaProxy::getInstance()->getStyle(),
              [ NSString stringWithUTF8String: photo->getValueByKey("legenda").c_str() ], 
              [ NSString stringWithUTF8String: photo->getValueByKey("fotografo").c_str() ] 
              ] baseURL: nil ] ;
        else
            [ PhotoLabel loadHTMLString: 
             [ NSString stringWithFormat:@"<html>%@<body><span style = 'font-family:Verdana; font-size:11px; color:#103e5e; font-weight: normal;' >%@</span><br/><span style = 'font-family:Verdana; font-size:10px; color:#646464; font-weight: normal;'>&copy; %@</span></body></html>", 
                         CRefazendaProxy::getInstance()->getStyle(),
              [ NSString stringWithUTF8String: photo->getValueByKey("legenda").c_str() ], 
              [ NSString stringWithUTF8String: photo->getValueByKey("fotografo").c_str() ] 
              ] baseURL: nil ] ;
	}
	
	catch (MMLNonExistantEntity *ex)
	{
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
            [ PhotoLabel loadHTMLString: 
             @"<html><body><p style = 'font-family:Verdana; font-size:14px; color:#103e5e;'>foto/legenda indisponivel no momento</p></body></html>" baseURL: nil ] ;
        else
            [ PhotoLabel loadHTMLString: 
             @"<html><body><p style = 'font-family:Verdana; font-size:12px; color:#103e5e;'>foto/legenda indisponivel no momento</p></body></html>" baseURL: nil ] ;
            

	}
	catch (MMLNonExistantCategory *ex)
	{
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
            [ PhotoLabel loadHTMLString: 
             NSLocalizedString( @"ERROR_PHOTO_SUBTITLE_HTML_IPAD", nil ) baseURL: nil ] ;        
        else
            [ PhotoLabel loadHTMLString: 
             NSLocalizedString( @"ERROR_PHOTO_SUBTITLE_HTML_IPOD", nil ) baseURL: nil ] ;            
	}
	
	
//	[PhotoIndexLabel setText:[NSString stringWithFormat:@"%d", CurrentPhoto+1]];		
	
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

- (void) onBecomeCurrentScreen
{
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: YES withListener: nil onObject: nil ];
    lockedTouches  = false;
//    imageView.frame = CGMakeRect
	currentViewMode=THUMBS;
	[super onBecomeCurrentScreen];	
	[self fetchCurrentImage];
	[self setPhotoText];	
    
    if ( currentPhotographer == nil && CRefazendaProxy::getInstance()->getCategoryByName( CONSTphotosCollection )->getTotalItems() > 0 ) {
        currentPhotographer = 0;
//        [ NSThread detachNewThreadSelector: @selector( initData )  toTarget:self withObject: self ];
        [ self initData ];
    }
    
    
        //152 164 170
    [ photographers setSeparatorColor: [ UIColor colorWithRed: (152.0f/ 255.0f)  green: (164.0f/255.0f) blue: (170.0f/255.0f) alpha: 1.0f] ];
    [ photographers setNeedsLayout ];
    [ photographers setNeedsDisplay ];
    [ photoSlider setNeedsLayout ];
    [ photoSlider setNeedsDisplay ];
    [ ( ( PlayerAppDelegate *) [ PlayerAppDelegate GetInstance ] ) setBusy: NO withListener: nil onObject: nil ];    
}

- ( void ) initData {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];      
    [ self buildPhotographerList ];
    [ self dispathBuildAlbumScrollViewWithAuthor: [ photographerList objectAtIndex: currentPhotographer ] ];
    [ pool release ];    
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

- (IBAction) next
{
	currentViewMode=THUMBS;
	try 
	{
		CMMLCategory::MMLCategoryReference photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);			
		CurrentPhoto++;	
        
		if (CurrentPhoto >= photos->getTotalItems()) 
            CurrentPhoto = 0;
        
		[self fetchCurrentImage];	
		[self setPhotoText];
		
	}
	catch (MMLNonExistantCategory *ex) 
	{
//		[PhotoText setText:@"indisponível"];
	}
	catch (MMLNonExistantEntity *ex) 
	{
//		[PhotoText setText:@"indisponível"];		
	}	
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/
- (IBAction) previous
{
	currentViewMode=THUMBS;
	try 
	{
		CMMLCategory::MMLCategoryReference photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);	
		CurrentPhoto--;
		if (CurrentPhoto < 0) CurrentPhoto = photos->getTotalItems()-1;
		[self fetchCurrentImage];
		[self setPhotoText];		
	}
	catch (MMLNonExistantCategory *ex) 
	{
//		[PhotoText setText:@"indisponível"];
	}
	catch (MMLNonExistantEntity *ex) 
	{
//		[PhotoText setText:@"indisponível"];		
	}
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildPhotosView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildPhotosView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}
/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/


-( bool )buildPhotosView
{
	[self setHidden: YES];
	CurrentPhoto=0;
    currentPhotographer = 0;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {

        imageWidth = 320;
        imageHeight = 277;
        imageHorizontalSpacing = 25;
        imageVerticalSpacing = 27;       
         
    } else {
        imageWidth = 145;
        imageHeight = 130;
        imageHorizontalSpacing = 15;
        imageVerticalSpacing = 10;        
        
    }
    
//    scrollImageFrame = [ [ UIScrollView alloc ] initWithFrame: [ UIScreen mainScreen ].applicationFrame ];
    //98 135 163
	return true;
}


- ( void ) startExpanding {

    [ UIView beginAnimations:@"PhotoAnimation" context:nil];
    [ UIView setAnimationDuration:0.6];
    [ UIView setAnimationDelegate:self];
    [ UIView setAnimationDidStopSelector:@selector (animationDidStop:finished:context:) ];	 
    
    imageOriginalFrame = [ ImageView frame ];                                          
    ImageView.frame = [ UIScreen mainScreen ].applicationFrame;
    
    
    [self bringSubviewToFront: ImageView ];
    //                        [self bringSubviewToFront: scrollImageFrame ];                        
    currentViewMode=FULLPHOTO; 
    [ logoBtn setUserInteractionEnabled: NO ];
    [UIView commitAnimations];
   
}

- ( void ) displayFullscren {
    currentViewMode=FULLPHOTO; 
    [ self fetchCurrentImage ];
    [ self startExpanding ];

}

/*==============================================================================================
 
 MENSAGEM touchesEnded
 
 ================================================================================================*/

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{
    if ( lockedTouches )
        return;
    
    UITouch *touch = [touches anyObject];
    
    [ logoBtn setUserInteractionEnabled: YES ];    
    
    if ( [ touch view ] == photoSlider )
    {

        if (currentViewMode==THUMBS)
    	{
            
            NSArray *views = [ photoSlider subviews ];
            for ( UIView *view in  views ) {
                
                [ view.layer setBorderColor: [[UIColor whiteColor] CGColor]];
                
                if ( CGRectContainsPoint(view.frame, [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: photoSlider ] ) ) {
                    
                    
                    if ( CurrentPhoto == ((OnDemandLoadImageView*) view ).itemId ) {
                        [(PlayerAppDelegate*)[ApplicationManager GetInstance] setBusy:YES withListener:@selector( displayFullscren ) onObject: self ];            
                        
                        [ view.layer setBorderColor: [ [ UIColor colorWithRed: ( 98.0f/255.0f) green: ( 135.0f/255.0f) blue: ( 163.0f/255.0f) alpha: 1.0f ] CGColor ] ];

                    } else {
                        CurrentPhoto = ((OnDemandLoadImageView*) view ).itemId;
                        [self setPhotoText];
                        [ self scrollTo: view ];
                        [ view.layer setBorderColor: [[UIColor colorWithRed: ( 98.0f/255.0f) green: ( 135.0f/255.0f) blue: ( 163.0f/255.0f) alpha: 1.0f ] CGColor]];
                    }
                }            
            }  
        }
        else
        {
            [ UIView beginAnimations:@"PhotoAnimation" context:nil];
            [ UIView setAnimationDuration:0.6];
            [ UIView setAnimationDelegate:self];
            [ UIView setAnimationDidStopSelector:@selector (animationDidStop:finished:context:) ];	            
    		ImageView.frame=imageOriginalFrame;
//            scrollImageFrame.frame = imageOriginalFrame;
            currentViewMode=THUMBS;
            
            [UIView commitAnimations];
            [self setPhotoText];
            
        }
    } else if ( currentViewMode == FULLPHOTO /*&& [ touch view ] == ImageView */) {
        [ UIView beginAnimations:@"PhotoAnimation" context:nil];
        [ UIView setAnimationDuration:0.6];
        [ UIView setAnimationDelegate:self];
        [ UIView setAnimationDidStopSelector:@selector (animationDidStop:finished:context:) ];	
        
        ImageView.frame=imageOriginalFrame;
//        scrollImageFrame.frame = imageOriginalFrame;
        currentViewMode=THUMBS;
        
        [UIView commitAnimations];        
        [self setPhotoText];
    }
}


- (void)animationWillStart:(NSString*)animationID context:(void*)context
{
    lockedTouches  = true;
	if (currentViewMode==FULLPHOTO)	
	{
		[self fetchCurrentImage];
//		ImageView.contentMode = UIViewContentModeScaleAspectFit;	
        ImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleWidth; 
        CGRect rect = [ UIScreen mainScreen ].applicationFrame;        
        ImageView.frame = rect;    
//        scrollImageFrame.frame = rect;
        
	} else {
//        ImageView.contentMode = UIViewContentModeScaleAspectFit;
        ImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleWidth; 
        
    }
		
}





- (void) animationDidStop:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    lockedTouches = false;
	
	if (currentViewMode==THUMBS)	
	{
		[self fetchCurrentImage];
		ImageView.contentMode = UIViewContentModeScaleAspectFit;	
//        scrollImageFrame.contentMode = UIViewContentModeScaleAspectFit;
//        [ ImageView removeFromSuperview ];
//        [ self addSubview: ImageView ];
//        [ scrollImageFrame removeFromSuperview ];
//        [ scrollImageFrame setHidden: YES ];
//        [ scrollImageFrame setMultipleTouchEnabled: NO ];
//        [ scrollImageFrame setScrollEnabled: NO ];
//        [ scrollImageFrame setUserInteractionEnabled: NO ];

	} else {
        ImageView.contentMode = UIViewContentModeScaleAspectFit;	
        [ ImageView setUserInteractionEnabled: YES ];
        [ ImageView setMultipleTouchEnabled: YES ];

//        scrollImageFrame.contentMode = UIViewContentModeScaleAspectFill;
//        [ ImageView removeFromSuperview ];
//        [ self addSubview: scrollImageFrame ];
//        [ scrollImageFrame addSubview: ImageView ];
//        [ scrollImageFrame setHidden: NO ];
//        [ self bringSubviewToFront: scrollImageFrame ];
//        [ scrollImageFrame setMultipleTouchEnabled: YES ];
//        [ scrollImageFrame setScrollEnabled: YES ];
//        [ scrollImageFrame setContentSize: [ ImageView frame ].size ];
//        [ scrollImageFrame setUserInteractionEnabled: YES ];

    }
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ( CRefazendaProxy::getInstance()->getCategoryByName( CONSTphotosCollection )->getTotalItems() > 0 ) ? 1 : 0;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    try {
        if ( [ photographerList count ] == 0 )
            [ self buildPhotographerList ];
        
        return [ photographerList count ];
    } catch ( Exception e ) {
        return 0;
    }
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{ 
	static NSString *MyIdentifier = @"PhotographerNameViewCell";
    
    PhotographerNameViewCell *cell = (PhotographerNameViewCell*)[ tableView dequeueReusableCellWithIdentifier: MyIdentifier ];
    
    if (cell == nil) { 
        NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:MyIdentifier owner:nil options:nil ];
        
        for ( id currentObject in topLevelObjects ) {
            if ( [ currentObject isKindOfClass:[ PhotographerNameViewCell class ] ] ) {
                cell = ( PhotographerNameViewCell *) currentObject;
                break;
            }
        }
    } 
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [ cell.nameLabel setText: [ photographerList objectAtIndex: indexPath.row ] ];
    [ cell setFiltered: ( indexPath.row == currentPhotographer ) ];
    
    return cell;
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/



- ( void ) rebuildSlider {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];     
    [ self dispathBuildAlbumScrollViewWithAuthor: [ photographerList objectAtIndex: currentPhotographer ] ];
    [ pool release ];
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ( currentViewMode == FULLPHOTO /*&& [ touch view ] == ImageView */) {
        [ UIView beginAnimations:@"PhotoAnimation" context:nil];
        [ UIView setAnimationDuration:0.6];
        [ UIView setAnimationDelegate:self];
        [ UIView setAnimationDidStopSelector:@selector (animationDidStop:finished:context:) ];	
        
        ImageView.frame=imageOriginalFrame;
        currentViewMode=THUMBS;
        
        [UIView commitAnimations];        
        [self setPhotoText];
        [ self scrollTo: [ photoSlider.subviews objectAtIndex: CurrentPhoto ] ];
        return nil;
    } else {    
        currentPhotographer = indexPath.row;
#ifdef DEBUG        
        NSLog( @"autor: %s ", [ [ photographerList objectAtIndex: currentPhotographer ] UTF8String ] );
#endif        
        [( PlayerAppDelegate * ) [ ApplicationManager GetInstance ] setBusy: YES withListener:@selector( rebuildSlider ) onObject: self  ];  
        
        

        
        [ photographers reloadData ];
        [ ( ( PhotographerNameViewCell* ) [ photographers cellForRowAtIndexPath: indexPath ] )   setFiltered: YES ];
        
        return nil;        
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)            
        return 59;        
    else
        return 33;
}


- ( int ) totalPhotosForPhotographer: ( NSString * ) name {
    
    int count = 0;
    int total = 0;
    
    CMMLCategory::MMLCategoryReference photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);			
    CMMLEntity::MMLEntityReference photo;
    
    count = (photos->getTotalItems());
    std::string desired = [ name UTF8String ];    

    for ( int c = 0; c < count; ++c ) {
                
        photo = photos->getEntityById( c );
        
        if ( [ name isEqualToString: ALL_PHOTOS ] || ( photo->getValueByKey("fotografo") == desired ) )
            ++total;
    }
    
    return total;
}

- ( void ) buildPhotographerList {
    int count = 0;
    photographerList = [ [ NSMutableArray alloc ] init ];    
    CMMLCategory::MMLCategoryReference photos=CRefazendaProxy::getInstance()->getCategoryByName(CONSTphotosCollection);			
    CMMLEntity::MMLEntityReference photo;
    NSString *photoAuthor;
    count = (photos->getTotalItems());
    
    [ photographerList addObject: ALL_PHOTOS ];
    
    for ( int c = 0; c < count; ++c ) {
        
        photo = photos->getEntityById( c );
        photoAuthor = [ NSString stringWithUTF8String: photo->getValueByKey("fotografo").c_str() ];
        
        if ( [ photographerList indexOfObject: photoAuthor ] == NSNotFound ) {
            [ photographerList addObject: photoAuthor ];
        }
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int i0 = (( ((int)scrollView.contentOffset.x) - ( IMAGE_W_2_SPACING >> 1 ) ) / IMAGE_W_2_SPACING ); 
    int i1 = (( ((int)scrollView.contentOffset.x) + ( IMAGE_W_2_SPACING << 1 ) ) / IMAGE_W_2_SPACING ) + 2;
    UIView  *view;
    for ( int c = i0; c < i1; ++c ) {
        
        if ( c < 0 || c >= [ scrollView.subviews count ] )
            continue;
        
        view = [ scrollView.subviews objectAtIndex: c ];
        if ( [ view isKindOfClass: [ OnDemandLoadImageView class ]  ] ) {
            [ (( OnDemandLoadImageView *) view ) doLoad ];
        }
        
    }
}


//WTF?!
- (IBAction) gotoAgenda {
    PlayerAppDelegate *instance=(PlayerAppDelegate*)[PlayerAppDelegate GetInstance];//[[UIApplication sharedApplication] delegate];
	[instance performTransitionToView:screen_AGENDA ];    

}
@end
