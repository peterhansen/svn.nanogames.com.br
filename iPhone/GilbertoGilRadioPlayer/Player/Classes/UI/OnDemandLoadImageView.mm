//
//  OnDemandLoadImageView.m
//  Player
//
//  Created by Daniel Monteiro on 5/30/11.
//  Copyright 2011 NanoGames. All rights reserved.
//
/// C++
#include <sstream> 
#include <map>


#import <QuartzCore/QuartzCore.h>

#import "OnDemandLoadImageView.h"
#import "PlayerAppDelegate.h"

@implementation OnDemandLoadImageView

@synthesize imageFilename;
@synthesize imageUrl;
@synthesize directory;
@synthesize itemId;



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        loadThread = nil;
        imageFilename = nil;
        directory = nil;
        imageUrl = nil;
        [ self.layer setBorderColor: [[UIColor blackColor] CGColor]];
        [ self.layer setBorderWidth: 5.0];
        
        // A thin border.
        self.layer.borderColor = [UIColor whiteColor].CGColor;

        self.clipsToBounds = YES;

        
        imageFilename = nil;
        directory = nil;
        imageUrl = nil;
        itemId = -1;
        loadThread = nil;  
        self.image = nil;
    }
    return self;
}

- ( void ) scheduleImageLoad: ( NSString * ) filename forDirectory: ( NSString * ) dir withURL: ( NSString * ) url  withBorder: ( float ) size AndWithAlternativeImage: ( NSString* ) alt {
    [ self scheduleImageLoadDelayed: filename forDirectory: dir withURL: url withBorder: size AndWithAlternativeImage: alt ];
    [ self doLoad ];
}

- ( void ) scheduleImageLoadDelayed:(NSString *)filename forDirectory:(NSString *)dir withURL:(NSString *)url  withBorder: ( float ) size AndWithAlternativeImage: ( NSString* ) alt {
    
    [ self.layer setBorderWidth: size ];
    imageFilename = filename;
    directory = dir;
    imageUrl = url;
    loadThread = nil;
    self.image = [ UIImage imageNamed: alt ];
    [ imageFilename retain ];
    [ directory retain ];
    [ imageUrl retain ];
}


- ( void ) doLoad {

//    NSLog(@"on demand load: %@ \n%@ \n%@ \n%d \n%@", imageFilename, directory, imageUrl, itemId, loadThread );
    
    if ( imageFilename != nil && directory != nil && imageUrl != nil && loadThread == nil )
        [ NSThread detachNewThreadSelector: @selector( assyncLoadImage )  toTarget:self withObject: self ];    
}

- ( void ) assyncLoadImage {
    loadThread = [ NSThread currentThread ]; 
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];  
    
    
//    NSLog(@"thread started: ( %@, %@, %@ free mem: %d bytes )", self.imageFilename, self.directory, self.imageUrl, [ PlayerAppDelegate get_free_memory ] );
    

    NSMutableData *data;	
    NSFileManager *filemgr;
    NSString *photoDir;    
    UIImage *img = nil;
    
    
    ////////////////////////	
    NSError *error = [ [NSError alloc] init];	
    
    filemgr =[NSFileManager defaultManager];	
    
    photoDir = [ [ PlayerAppDelegate getDataDir ] stringByAppendingPathComponent:directory ];
    photoDir = [ photoDir stringByAppendingString:@"/" ];
    //////////////////////
    NSString *fullPathToFile=[ photoDir stringByAppendingString: imageFilename ];
    NSURL* urlPtr = [ NSURL URLWithString: imageUrl ];

	
    if ( [ [ filemgr contentsOfDirectoryAtPath: photoDir error:&error ] indexOfObject:imageFilename ] == NSNotFound )
    {
        data = [ NSMutableData dataWithContentsOfURL: urlPtr ];
        
        if (data!=nil)
            [data writeToFile:fullPathToFile atomically: NO ];	
        else {
            //#ifdef DEBUG
            NSLog(@"error");            
            //#endif
        }

    } else {
        data = [ NSMutableData dataWithContentsOfFile:fullPathToFile ];
    }
    
    img=[[UIImage alloc ] initWithData: data ];    
    
    [ self setNeedsLayout ];
    [ self setNeedsDisplay ];

    [self performSelectorOnMainThread:@selector( displayImage: ) withObject: img waitUntilDone:NO];
    [ img autorelease ];
    


    [ error autorelease ];
    [ pool release ];
}

- (void)displayImage:(UIImage *)image {
    [ self setImage: image ]; //UIImageView
    
    //não me pertencem!!!!
//    [ imageFilename autorelease ];
//    [ directory autorelease ];
//    [ imageUrl autorelease ];

}

- (void)dealloc
{
    [super dealloc];    
}

@end
