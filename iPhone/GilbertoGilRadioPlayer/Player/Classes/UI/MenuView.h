//
//  MenuView.h
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"
#import "OnDemandLoadImageView.h"

@interface MenuView :  BaseGilbertoGilView <UITableViewDataSource,UITableViewDataSource>
{
    IBOutlet UITableView *twitterPreview;
    IBOutlet UITableView *newsClipping;
    
    IBOutlet OnDemandLoadImageView *disc0;
    IBOutlet OnDemandLoadImageView *disc1;
    IBOutlet OnDemandLoadImageView *disc2;
    IBOutlet OnDemandLoadImageView *disc3;
    IBOutlet OnDemandLoadImageView *disc4;
    IBOutlet OnDemandLoadImageView *disc5;    
    
    IBOutlet OnDemandLoadImageView *photo0;
    IBOutlet OnDemandLoadImageView *photo1;
    IBOutlet OnDemandLoadImageView *photo2;
    
}
///<InterfaceBuilder
- (IBAction) gotoRadio;
- (IBAction) gotoDisco;
- (IBAction) gotoRss;
- (IBAction) gotoPhotos;
- (IBAction) gotoTwitter;
- (IBAction) gotoBio;
- (IBAction) gotoAbout;
- (IBAction) gotoAgenda;
///</InteraceBuilder>
- ( void ) updateRSSData;
///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildMenuView;
///<ciclo de vida>

@end
