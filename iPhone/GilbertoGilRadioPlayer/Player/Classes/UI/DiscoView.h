//
//  DiscoView.h
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"
#import "SingleTapEnabledScrollView.h"

@interface DiscoView : BaseGilbertoGilView<UIScrollViewDelegate,UITableViewDataSource>
{
	///qual foto estamos vendo
	int CurrentPhoto;
	
///<InterfaceBuilder>
	IBOutlet UIButton *listenBtn;	
	IBOutlet UIButton *textBtn;		
    IBOutlet SingleTapEnabledScrollView *albumListView;
    IBOutlet UIWebView *albumTitle;
    IBOutlet UITableView *trackList;
    IBOutlet UIButton *readTextButton;
    
    int imageWidth;
    int imageHeight;
    int imageVerticalSpacing;
    int imageHorizontalSpacing;
    
}
- (IBAction) listen;
///<InterfaceBuilder>

///obtem a url da foto buscada
+ /**static*/ (NSString*) getPictureUrl: (int) index;

///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildDiscoView;
///</ciclo de vida>
- (IBAction) readText;
+ (NSString*) getPictureUrl: (int) index;
+ ( NSString * ) getPictureFilename:(int)index;
@property ( nonatomic, retain ) IBOutlet SingleTapEnabledScrollView *albumListView;
@end
