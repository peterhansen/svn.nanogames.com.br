//
//  PlaylistViewCell.h
//  Player
//
//  Created by Daniel Monteiro on 6/6/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PlaylistViewCell : UITableViewCell {
    IBOutlet UILabel* listNameLabel;    
    UITableView *container;
}

-( IBAction ) deleteList;

@property (nonatomic, retain) IBOutlet UILabel* listNameLabel;
@property (nonatomic, retain) UITableView *container;

@end
