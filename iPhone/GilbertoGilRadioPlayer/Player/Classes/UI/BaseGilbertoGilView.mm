//
//  BaseGilbertoGilView.m
//  Player
//
//  Created by Daniel Monteiro on 9/8/10.
//  Copyright 2010 Nano Games. All rights reserved.
//
/// C++
#include <sstream> 
#include <map>

/// Aplicacao
#import "PlayerAppDelegate.h"
/// View 
#import "BaseGilbertoGilView.h"

@implementation BaseGilbertoGilView

- ( void )awakeFromNib
{
	[super awakeFromNib];
}	

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildBaseGilbertoGilViewView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildBaseGilbertoGilViewView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}


-( bool )buildBaseGilbertoGilViewView
{
	[self setHidden: YES];
	return true;
}



- (void)dealloc {
    [super dealloc];
}

- (IBAction) gotoMenu
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];
	[instance performTransitionToView:screen_MENU ];
}





- (void) refresh
{
}


// Atualiza a view
- ( void ) update:( float )timeElapsed
{

}

// Indica que o usuário selecionou uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton
{

}


// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition
{
	[self setHidden:YES];	
}

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen
{
	[self setHidden:NO];
}




- (void) notifyFinishedLoading
{

}

- (void) setLocked: (BOOL) state
{

}

- (BOOL) isLocked
{
	return false;
}



@end
