//
//  BriefTweetViewCell.h
//  Player
//
//  Created by Daniel Monteiro on 6/9/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BriefTweetViewCell : UITableViewCell {
    
    IBOutlet UITextView* tweetLabel;
    IBOutlet UILabel* dateLabel;
    
}

@property (nonatomic, retain) IBOutlet UITextView *tweetLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateLabel;
@end
