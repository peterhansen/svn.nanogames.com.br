//
//  TwitterView.m
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//



/// C++
#include <sstream> 
#include <map>
#include <vector>
#include <math.h>   

/// 3rd Party
#import "GDataXMLNode.h"

/// Aplicacao
#import "Config.h"
#import "PlayerAppDelegate.h"
#include "Tweet.h"


/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#include "ObjcMacros.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "HTTPAudioStreamController.h"
#import "PlayerAppDelegate.h"
#include "RefazendaProxy.h"

/// View
#import "TwitterView.h"
#import "TweetCellView.h"

@implementation TwitterView


/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildTwitterView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildTwitterView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/
-( bool )buildTwitterView
{
	tickerTimer=nil;
	connectedToInternet=YES;
	 
	[ self setHidden: YES ];	
	[ self setUpdateRate:0 ];


	return true;
	
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];		
	[self setUpdateRate:1];

//	if (tickerTimer!=nil)
//		[tickerTimer fire];

//    NSArray *array = [ UIFont familyNames ];
//    for ( NSString *font in array  ) {
//        NSLog( @" font:%@", font );
//    }
    
    
//    UIFont *myFont = [ UIFont fontWithName:@"Florida" size:12 ] ;    
//    NSLog( @"font: %@", myFont );
//    [ [ refreshBtn titleLabel ] setFont: myFont ];
//    [ [ back titleLabel ] setFont: myFont ];
//    [ TwitterLabel setFont: myFont ];
    
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) gotoMenu
{
	[super gotoMenu];
	[self terminateTimer];
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) updateOnTimer: (NSTimer*) timer
{
	[self updateContent];	
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (IBAction) forceUpdate
{
	[self updateContent];
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (IBAction) sliderChanged
{	
	[self setUpdateRate:(int) [timerSlider value] ];
}


+ (NSString *) filterText: ( NSString * ) text 
{
    NSMutableString *tmp = [ [ NSMutableString alloc ] initWithString: text ];
    NSRange range; 
    
    range = NSMakeRange(0, [ tmp length]);
    
    [ tmp replaceOccurrencesOfString:@"&gt;" withString:@">" options:NSCaseInsensitiveSearch range: range ];
    
    range = NSMakeRange(0, [ tmp length]);
    
    [ tmp replaceOccurrencesOfString:@"&lt;" withString:@"<" options:NSCaseInsensitiveSearch range: range ];
    
    range = NSMakeRange(0, [ tmp length]);

    [ tmp replaceOccurrencesOfString:@"&quot;" withString:@"\"" options:NSCaseInsensitiveSearch range: range ];

    range = NSMakeRange(0, [ tmp length]);
    
    [ tmp replaceOccurrencesOfString:@"&amp;" withString:@"&" options:NSCaseInsensitiveSearch range: range ];    
    
    range = NSMakeRange(0, [ tmp length]);
    
    [ tmp replaceOccurrencesOfString:@"&circ;" withString:@"ˆ" options:NSCaseInsensitiveSearch range: range ];

    range = NSMakeRange(0, [ tmp length]);
    
    [ tmp replaceOccurrencesOfString:@"&tilde;" withString:@"˜" options:NSCaseInsensitiveSearch range: range ];

    range = NSMakeRange(0, [ tmp length]);
    
    [ tmp replaceOccurrencesOfString:@"&euro;" withString:@"€" options:NSCaseInsensitiveSearch range: range ];    
    
    
    NSString *toReturn = [ [ NSString alloc  ] initWithString: tmp ];
    [ tmp autorelease ];
    
    return toReturn;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- ( void ) finishedUpdatingContent {
    CRefazendaProxy::getInstance()->updateTwitterContent();
    [ tweetsTable reloadData ];    
}

- (void) updateContent
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ApplicationManager GetInstance];    
    [ instance setBusy: YES  withListener: @selector( finishedUpdatingContent ) onObject: self ];
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) terminateTimer
{
	if (tickerTimer != nil)
		[tickerTimer invalidate];
	tickerTimer = nil;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) initTimerForMinutes: (int) minutes
{
	tickerTimer= [NSTimer scheduledTimerWithTimeInterval: minutes * 60
												  target: self
												selector: @selector(updateOnTimer:)
												userInfo: nil
												 repeats: YES
				  ];	
	
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (void) setUpdateRate: (int) interval
{
    	[self terminateTimer];

        [timerSlider setValue: interval ];
    [updateLabel setText: [ NSString stringWithFormat: NSLocalizedString( @"UPDATE_RATE", nil ), interval, ( NSLocalizedString( interval == 1 ? @"MINUTE" : @"MINUTES", nil ) ) ] ];
    	
    	if ( ((int) [timerSlider value]) != 0 && connectedToInternet == YES)
    		[self initTimerForMinutes:interval];
    	else 
    		[updateLabel setText: NSLocalizedString( @"UPDATE_OFF", nil ) ];
    	
    	
    	if (connectedToInternet == NO) {
    		NSLog(@"twitter:`sem conexao`");		
    		[updateLabel setText: NSLocalizedString( @"NO_LINK", nil ) ];
    		[timerSlider setValue: 0.0f ];
    		[self terminateTimer];
    	}
    
    
//	[self terminateTimer];
//	
//	[timerSlider setValue: interval ];
//	[updateLabel setText: [NSString stringWithFormat:@" Atualizações a cada %d %s", interval, ( interval == 1 ? "minuto": "minutos" )]];
//	
//	if ( ((int) [timerSlider value]) != 0 && connectedToInternet == YES)
//		[self initTimerForMinutes:interval];
//	else 
//		[updateLabel setText: @"Atualizações desligadas"];
//	
//	
//	if (connectedToInternet == NO)
//	{
//		NSLog(@"twitter:`sem conexao`");		
//		[updateLabel setText: @"sem conexao"];
//		[timerSlider setValue: 0.0f ];
//		[self terminateTimer];
//	}
}
/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (void)dealloc {
    [super dealloc];
}


///------------------------------------------------------------------------------------------

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ( CRefazendaProxy::getInstance()->getTweets().size() > 0 );
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int size = CRefazendaProxy::getInstance()->getTweets().size();
    
    return ( size >= 20 ? 20 : size );
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
	NSString *MyIdentifier = @"TweetCellView";
	
    TweetCellView *cell = (TweetCellView *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    if (cell == nil) { 
		
        
        NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:@"TweetCellView" owner:nil options:nil ];
        
        for ( id currentObject in topLevelObjects ) {
            if ( [ currentObject isKindOfClass:[ TweetCellView class ] ] ) {
                cell = ( TweetCellView *) currentObject;
                break;
            }
        }
        
        
        Tweet *t =   CRefazendaProxy::getInstance()->getTweets()[ indexPath.row ];
        
        cell.dateLabel.text = [ NSString stringWithCString: t->getPubDate().c_str() encoding: NSUTF8StringEncoding ];
        cell.userLabel.text = [ NSString stringWithCString: t->getUserName().c_str() encoding: NSUTF8StringEncoding ];
        cell.realNameLabel.text = [ NSString stringWithCString: t->getRealName().c_str() encoding: NSUTF8StringEncoding ];
        cell.tweetLabel.text = [ NSString stringWithCString: t->getTweetText().c_str() encoding: NSUTF8StringEncoding ];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [ cell setFrame:CGRectMake(0, 0, 284, 84 ) ];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	return nil;
}


@end
