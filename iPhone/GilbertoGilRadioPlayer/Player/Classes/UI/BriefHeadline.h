//
//  BriefHeadline.h
//  Player
//
//  Created by Daniel Monteiro on 6/9/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BriefHeadline : UITableViewCell {
    IBOutlet UILabel *headline;    
}
@property (nonatomic, retain) IBOutlet UILabel *headline;    
@end
