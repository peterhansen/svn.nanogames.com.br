//
//  DiscoView.m
//  Player
//
//  Created by Daniel Monteiro on 4/19/10.
//  Copyright 2010 Nano Games. All rights reserved.
//



/// C++
#include <sstream> 
#include <map>
#import <QuartzCore/QuartzCore.h>
/// 3rd party
#include "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "Config.h"
#import "HTTPBridge.h"
#include "Config.h"
#include "RefazendaProxy.h"
#import "PlayerAppDelegate.h"
#import "OnDemandLoadImageView.h"
/// View 
#import "DiscoView.h"
#import "AlbumSongViewCell.h"


#define IMAGE_W_2_SPACING ( imageWidth + ( imageHorizontalSpacing * 2 ) )
#define IMAGE_W_SPACING ( imageWidth + imageHorizontalSpacing )

@implementation DiscoView

@synthesize albumListView;


- ( void ) viewDidLoad {
    NSLog(@"init");
    
    
}

+ ( NSString * ) getPictureFilename:(int)index {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)    
        return [ NSString stringWithFormat:@"%d_g.jpeg", index ];
    else
        return [ NSString stringWithFormat:@"%d_p.jpeg", index ];
}


/*==============================================================================================
 
 MENSAGEM getPictureUrl
 Obtem a URL da foto buscada
 
 ================================================================================================*/

+ /**static*/ (NSString*) getPictureUrl: (int) index
{
	NSMutableString *url=[NSMutableString stringWithCString:"http://gilbertogil.com.br/upload/discografia_1/"];
	[url appendString:[NSString stringWithFormat: @"%d", index]];

    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) 
        [url appendString:@"_g.jpeg" ];
    else
        [url appendString:@"_p.jpeg" ];
    
	return url;
}

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildDiscoView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildDiscoView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

-( bool )buildDiscoView
{
	[self setHidden: YES];    
	CurrentPhoto=0;    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        imageWidth = 285;
        imageHeight = 285;
        imageHorizontalSpacing = 15;
        imageVerticalSpacing = 15;       
        
    } else {
        imageWidth = 130;
        imageHeight = 130;
        imageHorizontalSpacing = 15;
        imageVerticalSpacing = 15;        
        
    }    
	return true;
}

/*==============================================================================================
 
 MENSAGEM dealloc
 destrutor
 
 ================================================================================================*/

- (void)dealloc 
{
    [super dealloc];
    [ albumListView release ];
}


/*==============================================================================================
 
 MENSAGEM setPhotoText
 define o texto a ser exibido na tela (referente a foto sendo vista no momento)
 
 ================================================================================================*/

- (void) setPhotoText
{
	try
	{
		CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
		CMMLCategory::MMLCategoryReference albums=proxy->getCategoryByName(CONSTalbumsCollection);		
		proxy->setCurrentAlbum(albums->getEntityById(CurrentPhoto));
        
        CMMLEntity::MMLEntityReference album = albums->getEntityById(CurrentPhoto);

        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)                    
            [ albumTitle loadHTMLString:              
             [ NSString stringWithFormat: NSLocalizedString( @"DISC_LABEL_IPAD", nil ), ( CurrentPhoto + 1 ),
                [ NSString stringWithUTF8String: album->getValueByKey("nome").c_str() ],
                [ NSString stringWithUTF8String: album->getValueByKey("gravadora").c_str() ],          
                [ NSString stringWithUTF8String: album->getValueByKey("ano").c_str() ]
                ] baseURL: nil ];
        else
            [ albumTitle loadHTMLString:              
             [ NSString stringWithFormat: NSLocalizedString( @"DISC_LABEL_IPOD", nil ), ( CurrentPhoto + 1 ),
              [ NSString stringWithUTF8String: album->getValueByKey("nome").c_str() ],
              [ NSString stringWithUTF8String: album->getValueByKey("gravadora").c_str() ],          
              [ NSString stringWithUTF8String: album->getValueByKey("ano").c_str() ]
              ] baseURL: nil ];
            
        
        if ( album->getValueByKey("texto") == "true" )
            [ readTextButton setEnabled: YES ];
        else
            [ readTextButton setEnabled: NO ];
        
	}
	catch (MMLNonExistantEntity *ex)
	{
        [ albumTitle loadHTMLString: NSLocalizedString( @"DISC_NAME_ERROR", nil )  baseURL: nil ];
	}
	catch (MMLNonExistantCategory *ex)
	{
        [ albumTitle loadHTMLString: NSLocalizedString( @"DISC_ERROR", nil )  baseURL: nil ];
	}
}



/*==============================================================================================
 
 MENSAGEM listen
 indica que o usuario deseja ouvir o disco sendo visualizado
 
 ================================================================================================*/
///<TODO>
/*
 - tratamento de excessoes precisa cobrir as outras operacoes
 - tratamento de excessoes ObjC?
 - trocar o tipo da excessao C++ 
 */
///</TODO>


- (IBAction) listen
{
	CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
	CMMLCategory::MMLCategoryReference playlists;
	CMMLEntity::MMLEntityReference playlist;
	CMMLCategory::MMLCategoryReference albums;
	
	playlists=proxy->getCategoryByName(CONSTplaylistsCollection);			
	
	try 
	{
		albums=proxy->getCategoryByName(CONSTalbumsCollection);		
	}
	catch (MMLNonExistantEntity *ex) 
	{
        [ albumTitle loadHTMLString: NSLocalizedString( @"SONGS_ERROR", nil )  baseURL: nil ];
		return;
	}

	playlist=playlists->getEntityByKeyValue("nome",CONSTdefaultPlaylist);	
	CMMLEntity::MMLEntityReference album=albums->getEntityById(CurrentPhoto);
	int count=AtoI(album->getValueByKey("size").c_str());	
	int playlistsize=AtoI(playlist->getValueByKey("size").c_str());	
	
	///obtem os trilha_audio`s
	for (int c=0;c<count;++c)	
	{
		playlist->setValueForKey(ItoA(c+playlistsize), album->getValueByKey(ItoA(c)));	
	}	
	
	try
	{
		playlist->setValueForKey("size", ItoA(count+playlistsize));
	}
	catch (MMLNonExistantEntity *e)
	{
        [ albumTitle loadHTMLString: NSLocalizedString( @"SONGS_ERROR", nil )  baseURL: nil ];
		return;
	}
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)            
        [ listenBtn setImage: [ UIImage imageNamed:@"add_2_ipad.png"]  forState: UIControlStateNormal ];
    else
        [ listenBtn setImage: [ UIImage imageNamed:@"add2.png"]  forState: UIControlStateNormal ];
//	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ ApplicationManager GetInstance];
//	[instance performTransitionToView:screen_RADIO ];
    
}


- ( void ) dispathBuildAlbumScrollView : (SEL)callback onObject:(id) obj {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; 
    int count = 0;
    
    CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
    CMMLCategory::MMLCategoryReference albums;
    albums=proxy->getCategoryByName(CONSTalbumsCollection);		
    CMMLEntity::MMLEntityReference album;
    count = albums->getTotalItems();
    
    
    if ( [ [ albumListView subviews ] count ] < count   )
        try 
    {
        OnDemandLoadImageView *discoPhoto;
        UIColor *color;
        std::string filename;
        
        
        for ( int c = 0; c < count; ++c ) {
            
            album = albums->getEntityById( c );       
            discoPhoto = [ [ OnDemandLoadImageView alloc ] initWithFrame:CGRectMake( c * ( IMAGE_W_SPACING ) , 7, imageWidth, imageHeight ) ];
            filename = album->getValueByKey("refazenda-id");
            [ discoPhoto  scheduleImageLoadDelayed: [ NSString stringWithCString: filename.c_str() encoding: NSUTF8StringEncoding ] 
                               forDirectory: @"cached/discophotos"
                                    withURL: [ DiscoView getPictureUrl: AtoI( filename.c_str() ) ] 
                withBorder: 0.0
                AndWithAlternativeImage:@"icDisco.png"
             ];
            
            if ( c < 5 )
                [ discoPhoto doLoad ];
            
            color = [ [ UIColor alloc ] initWithRed: ( 208.0f /  255.0f ) green: ( 224.0f / 255.0f ) blue: ( 229.0f / 255.0f ) alpha: 1.0f ];
            [ discoPhoto setBackgroundColor: color ];
            [ albumListView addSubview: discoPhoto ];
            [ color autorelease ];            
            [ discoPhoto autorelease ];
        }
        [ albumListView setClipsToBounds: YES ];
        [ albumListView setContentSize:CGSizeMake( count * ( IMAGE_W_SPACING ), imageHeight + imageVerticalSpacing )];
        [ albumListView setScrollEnabled: YES ];        
    }
    catch (MMLNonExistantEntity *ex) 
    {
        [ albumTitle loadHTMLString: NSLocalizedString( @"DISC_ERROR", nil ) baseURL: nil ];
    }
    
    
    [ pool release ];
    
    if ( callback != nil && obj != nil )
        [obj performSelector:callback];
}

- (void) startLoading
{
	PlayerAppDelegate* appController=(PlayerAppDelegate*)[ApplicationManager GetInstance];	
	[ self dispathBuildAlbumScrollView: @selector(notifyFinishedLoading) onObject:self];	
}

- (void) notifyFinishedLoading
{
	PlayerAppDelegate* appController=(PlayerAppDelegate*)[ApplicationManager GetInstance];		
	[appController setBusy:NO withListener:nil onObject:nil];		
}

/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 A view ganhou foco
 
 ================================================================================================*/
- (void) onBecomeCurrentScreen
{

	[super onBecomeCurrentScreen];	
	[self setPhotoText];
    
   	PlayerAppDelegate* appController=(PlayerAppDelegate*)[ApplicationManager GetInstance];
	[ appController setBusy:YES withListener: @selector( startLoading ) onObject: self ];
//    [ self dispathBuildAlbumScrollView: nil onObject: nil ];
    [ trackList reloadData ];
}

/*==============================================================================================
 
 MENSAGEM touchesEnded
 
 ================================================================================================*/

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event 
{
 
    /*
     CurrentPhoto--;
     if (CurrentPhoto < 0) CurrentPhoto = CurrentPhoto=count-1;
     [self setPhotoText];
     [self fetchCurrentImage];

     */
    
    
    
    NSArray *views = [ albumListView subviews ];
    for ( OnDemandLoadImageView *view in  views ) {
        [ view.layer setBorderColor: [[UIColor whiteColor] CGColor]];
        [ view setNeedsLayout ];
        [ view setNeedsDisplay ];
        [ view.layer setBorderWidth: 0.0 ];                    
        if ( CGRectContainsPoint(view.frame, [ ((UITouch*)[ [ touches allObjects ] objectAtIndex: 0 ]) locationInView: albumListView ] ) ) {
            
            CurrentPhoto = [ views indexOfObject: view ];
            
            [self setPhotoText];
            [ trackList reloadData ];
            NSIndexPath *indPath = [ NSIndexPath indexPathForRow: 0 inSection: 0 ];
            [ trackList scrollToRowAtIndexPath: indPath atScrollPosition: UITableViewScrollPositionMiddle animated:YES ];
            [ albumListView scrollRectToVisible: CGRectMake( ( CurrentPhoto * IMAGE_W_SPACING ) - ( IMAGE_W_SPACING / 2 ), 0, imageWidth + ( IMAGE_W_SPACING ), imageHeight ) animated: YES ];
            
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)            
                [ listenBtn setImage: [ UIImage imageNamed:@"add_ipad.png"]  forState: UIControlStateNormal ];                
            else
                [ listenBtn setImage: [ UIImage imageNamed:@"add.png"]  forState: UIControlStateNormal ];
            
            [ view.layer setBorderColor: [[UIColor colorWithRed: ( 98.0f/255.0f) green: ( 135.0f/255.0f) blue: ( 163.0f/255.0f) alpha: 1.0f ] CGColor]];
            [ view.layer setBorderWidth: 5.0 ];         
        }            
    }   
}


- (IBAction) readText
{
	PlayerAppDelegate *instance=(PlayerAppDelegate*)[ ApplicationManager GetInstance];
	[instance performTransitionToView:screen_TEXT ];
}


///------------------------------------------------------------------------------------------

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return ( ( CRefazendaProxy::getInstance()->getCategoryByName( CONSTalbumsCollection )->getTotalItems() > 0 ) ? 1 : 0 );
}

/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    try {
        CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
        CMMLCategory::MMLCategoryReference albums;
        albums=proxy->getCategoryByName(CONSTalbumsCollection);		    
        
        CMMLEntity::MMLEntityReference album = albums->getEntityById( CurrentPhoto );
        
        
        return AtoI(album->getValueByKey("size").c_str()) + 1;
        
    } catch ( Exception e ) {
        return 0;
    }
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{ 
    CRefazendaProxy *proxy=CRefazendaProxy::getInstance();
    CMMLCategory::MMLCategoryReference albums;
    albums=proxy->getCategoryByName(CONSTalbumsCollection);		
    CMMLEntity::MMLEntityReference album = albums->getEntityById( CurrentPhoto );
	CMMLCategory::MMLCategoryReference works=proxy->getCategoryByName(CONSTworksCollection);
    CMMLCategory::MMLCategoryReference musics=proxy->getCategoryByName(CONSTmusicsCollection);

    std::string idObra;
    std::string idTrilha;
    std::string tempo;
 
	NSString *MyIdentifier = @"AlbumSongViewCell";
	
    AlbumSongViewCell *cell = (AlbumSongViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) { 
        
        NSArray *topLevelObjects = [ [ NSBundle mainBundle] loadNibNamed:MyIdentifier owner:nil options:nil ];
        
        for ( id currentObject in topLevelObjects ) {
            if ( [ currentObject isKindOfClass:[ AlbumSongViewCell class ] ] ) {
                cell = ( AlbumSongViewCell *) currentObject;
                break;
            }
        }
        
    }
    
    idTrilha = album->getValueByKey( ItoA( indexPath.row ) );
    idObra = musics->getEntityByKeyValue( "id-trilha", idTrilha )->getValueByKey( "id-obra" );
    tempo = musics->getEntityByKeyValue( "id-trilha", idTrilha )->getValueByKey( "tempo");
    
    cell.songName.text = 
    [ NSString stringWithFormat:@"  %d. %s  %@", ( indexPath.row + 1), tempo.c_str(),  [ NSString stringWithUTF8String: works->getEntityByKeyValue( "id-obra", idObra )->getValueByKey("nome").c_str() ] ];  
    
    cell.trackId = [ NSString stringWithCString: idTrilha.c_str() encoding: NSUTF8StringEncoding ];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    try {
        
        if ( works->getEntityByKeyValue( "id-obra", idObra )->getValueByKey("obs").length() == 0 || works->getEntityByKeyValue( "id-obra", idObra )->getValueByKey("obs") == "false" ) {
            cell.textButton.imageView.hidden = YES;        
            cell.textButton.enabled = NO;                        
        } else {
            cell.textButton.imageView.hidden = NO;
            cell.textButton.enabled = YES;                        
        }        
    } catch ( MMLBaseException e ) {
        //#ifdef DEBUG
        NSLog(@"deu erro na célula de música:");
        //#endif
        cell.textButton.imageView.hidden = YES;        
        cell.textButton.enabled = NO;         
    }
    
	CMMLCategory::MMLCategoryReference playlists;
	CMMLEntity::MMLEntityReference playlist;	
	playlists=proxy->getCategoryByName(CONSTplaylistsCollection);
	playlist=playlists->getEntityByKeyValue("nome",CONSTdefaultPlaylist);	
	
    int count = AtoI( playlist->getValueByKey( "size" ).c_str() );
    bool found = false;
    
    for ( int c = 0; c < count && !found; ++c )
        if ( playlist->getValueByKey( ItoA( c ) ) == idTrilha )
            found = true;
    
    if ( !found )
        [ cell resetAddButton ];    

    return cell;
}


/*==============================================================================================
 
 MENSAGEM buildConfirmLoadView
 Inicializa a view.
 
 ================================================================================================*/

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)        
        return 58;
    else
        return 29;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int i0 = (( ((int)scrollView.contentOffset.x) - ( imageWidth / 2 ) ) / imageWidth); 
    int i1 = (( ((int)scrollView.contentOffset.x) + ( imageHeight * 2 ) ) / imageHeight ) + 2;
    UIView  *view;
    for ( int c = i0; c < i1; ++c ) {
        
        if ( c < 0 || c >= [ scrollView.subviews count ] )
            continue;
        
        view = [ scrollView.subviews objectAtIndex: c ];
        if ( [ view isKindOfClass: [ OnDemandLoadImageView class ]  ] ) {
            [ (( OnDemandLoadImageView *) view ) doLoad ];
        }
        
    }
}
@end
