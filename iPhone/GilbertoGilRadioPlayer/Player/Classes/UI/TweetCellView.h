//
//  TweetCellView.h
//  Player
//
//  Created by Daniel Monteiro on 5/24/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TweetCellView : UITableViewCell {
    
    IBOutlet UITextView* tweetLabel;
    IBOutlet UILabel* userLabel;    
    IBOutlet UILabel* realNameLabel;
    IBOutlet UILabel* dateLabel;
    
}

@property (nonatomic, retain) IBOutlet UITextView *tweetLabel;
@property (nonatomic, retain) IBOutlet UILabel *userLabel;
@property (nonatomic, retain) IBOutlet UILabel *realNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateLabel;

@end
