//
//  OnDemandLoadImageView.h
//  Player
//
//  Created by Daniel Monteiro on 5/30/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OnDemandLoadImageView : UIImageView {
    NSString *imageFilename;
    NSString *directory;
    NSString *imageUrl;
    NSInteger itemId;
    NSThread *loadThread;
}

- ( void ) scheduleImageLoad: ( NSString * ) filename forDirectory: ( NSString * ) dir withURL: ( NSString * ) url  withBorder: ( float ) size AndWithAlternativeImage: ( NSString* ) alt;

- ( void ) scheduleImageLoadDelayed:(NSString *)filename forDirectory:(NSString *)dir withURL:(NSString *)url  withBorder: ( float ) size AndWithAlternativeImage: ( NSString* ) alt;
- ( void ) doLoad;
- ( void ) assyncLoadImage;
- ( void ) unloadImg;
@property ( nonatomic, assign ) NSInteger itemId;
@property ( nonatomic, retain ) NSString *imageFilename;
@property ( nonatomic, retain ) NSString *directory;
@property ( nonatomic, retain ) NSString *imageUrl;
@end
