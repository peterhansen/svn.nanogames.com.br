//
//  PlaylistSongViewCell.h
//  Player
//
//  Created by Daniel Monteiro on 6/1/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PlaylistSongViewCell : UITableViewCell {
    
    IBOutlet UILabel* songName;    
    IBOutlet UIImageView* relocateSign;
    IBOutlet UIButton* deleteBtn;
    IBOutlet UIImageView* bg;
    
    NSInteger trackNum;
	RadioView *playlistListener;	
    UITableView *table;
    RadioView *manager;
}

- ( IBAction ) removeFromPlaylist;
- ( void ) setPlaying:(BOOL)selected;
@property (nonatomic, retain) IBOutlet UILabel* songName;
@property (readwrite,assign) NSInteger trackNum;
@property (nonatomic, retain) RadioView *playlistListener;
@property (nonatomic, retain) UITableView *table;
@property (nonatomic, retain) RadioView *manager;
@property (nonatomic, retain) IBOutlet UIImageView* relocateSign;

@end