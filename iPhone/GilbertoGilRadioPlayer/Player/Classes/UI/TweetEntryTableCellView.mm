//
//  TweetEntryTableCellView.mm
//  Player
//
//  Created by Daniel Monteiro on 5/23/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "TweetEntryTableCellView.h"


@implementation TweetEntryTableCellView

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {

    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
    }
	

	
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [super dealloc];
}

- (void)addColumn:(CGFloat)position {
    [columns addObject:[NSNumber numberWithFloat:position]];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    // Use the same color and width as the default cell separator for now
    CGContextSetRGBStrokeColor(ctx, 1.0, 0.5, 0.5, 1.0);
    CGContextSetLineWidth(ctx, 0.25);
	
    for (int i = 0; i < [columns count]; i++) {
        CGFloat f = [((NSNumber*) [columns objectAtIndex:i]) floatValue];
        CGContextMoveToPoint(ctx, f, 0);
        CGContextAddLineToPoint(ctx, f, self.bounds.size.height);
    }
	
    CGContextStrokePath(ctx);
	
    [super drawRect:rect];
}

- (void) setContent:(NSString *) tweetText :(NSString *)userName  :(NSString *) personName :( NSString *)tweetDate 
{
	
	UILabel *label;
	
	user = userName;	
	label =  [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0, 140.0, 20.0)] autorelease];
	[self addColumn:140];	
	label.tag = 0;
	label.text = user;	
	label.font = [UIFont systemFontOfSize:12.0];
	label.textAlignment = UITextAlignmentRight;
	label.textColor = [UIColor blueColor];
	label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
	UIViewAutoresizingFlexibleHeight;
	[ self.contentView addSubview:label];	
	realName = personName;	
	label = [[[UILabel alloc] initWithFrame:CGRectMake(140.0, 0, 140.0,20.0)] autorelease];
	[ self addColumn:140];
	label.tag = 1;
	label.text = personName;
	label.font = [UIFont systemFontOfSize:12.0];
	label.textAlignment = UITextAlignmentRight;
	label.textColor = [UIColor blueColor];
	label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
	UIViewAutoresizingFlexibleHeight;
	[self.contentView addSubview:label]; 
	tweet = tweetText;
	realName = personName;	
	label = [[[UILabel alloc] initWithFrame:CGRectMake(0.0, 20, 280.0,20.0)] autorelease];
	[ self addColumn:280];
	label.tag = 2;
	label.text = tweet;
	label.font = [UIFont systemFontOfSize:12.0];
	label.textAlignment = UITextAlignmentRight;
	label.textColor = [UIColor blueColor];
	label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
	UIViewAutoresizingFlexibleHeight;
	[self.contentView addSubview:label]; 	
	date = tweetDate;
	realName = personName;	
	label = [[[UILabel alloc] initWithFrame:CGRectMake( 0.0, 40, 280.0,20.0)] autorelease];
	[ self addColumn:280];
	label.tag = 3;
	label.text = date;
	label.font = [UIFont systemFontOfSize:12.0];
	label.textAlignment = UITextAlignmentRight;
	label.textColor = [UIColor blueColor];
	label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin |
	UIViewAutoresizingFlexibleHeight;
	[self.contentView addSubview:label]; 
	


}
@end
