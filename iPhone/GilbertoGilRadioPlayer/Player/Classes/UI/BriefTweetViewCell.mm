//
//  BriefTweetViewCell.mm
//  Player
//
//  Created by Daniel Monteiro on 6/9/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "BriefTweetViewCell.h"


@implementation BriefTweetViewCell

@synthesize tweetLabel;
@synthesize dateLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [super dealloc];
}

@end
