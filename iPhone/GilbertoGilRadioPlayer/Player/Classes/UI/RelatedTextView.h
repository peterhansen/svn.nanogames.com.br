//
//  RelatedTextView.h
//  Player
//
//  Created by Daniel Monteiro on 9/23/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"

@interface RelatedTextView : BaseGilbertoGilView {
	///link para o conteudo da view.
	IBOutlet UIWebView *relatedText;
	///timer de atualização de conteudo
    NSMutableString *contentRaw;
}

///<ciclo de vida>
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id ) initWithFrame:( CGRect )frame;
/// construtor de primeiro passo, apartir do XIB
-( id ) initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildRelatedTextView;
///</ciclo de vida>
- (void) doUpdateContent;
- (IBAction) gotoMenu;
- (void) updateContent;
- (void) doUpdateContent;
@end