//
//  PhotographerNameViewCell.mm
//  Player
//
//  Created by Daniel Monteiro on 6/6/11.
//  Copyright 2011 Nano Games. All rights reserved.
//

#import "PhotographerNameViewCell.h"
#import "AlbumSongViewCell.h"

@implementation PhotographerNameViewCell
@synthesize nameLabel;

- (void)dealloc
{
    [super dealloc];
}

- ( void ) updateBG {
    
    UIColor *color;
    
    if ( filtered )
        color = [ UIColor colorWithRed: (161.0f/255.0f) green: (178.0f/255.0f) blue:(191.0f/255.0f) alpha: 1.0f ];        
    else
        color = [ UIColor colorWithRed:(213.0f/255.0f) green:(218.0f/255.0f) blue:(220.0f/255.0f) alpha:1.0f];        
    
    
    [ nameLabel setBackgroundColor: color ];
    [ bg setBackgroundColor: color ];
    [ self setBackgroundColor: color ];
}

- ( void ) setFiltered:(bool) makeFiltered {
    filtered = makeFiltered;
    [ self updateBG ];
}

@end
