//
//  IntroView.h
//  Player
//
//  Created by Daniel Monteiro on 4/16/10.
//  Copyright 2010 Nano Games. All rights reserved.
//

/// ObjC
#import <UIKit/UIKit.h>

/// Aplicacao
#import "BaseGilbertoGilView.h"

@interface IntroView :  BaseGilbertoGilView
{
///<InterfaceBuilder>	
	IBOutlet UIButton *entrarBtn;
}
- (IBAction) onEntrarBtnPressed;
///</InterfaceBuilder>

///<ciclo de vida do objeto >
/// construtor de primeiro passo, apartir de [[alloc] init ]
-( id )initWithFrame:( CGRect )frame;

/// construtor de primeiro passo, apartir do XIB
-( id )initWithCoder:( NSCoder* )decoder;

/// costrutor de fato
-( bool )buildIntroView;

- ( void ) onEntrar;
///</ciclo de vida do objeto>
@end