//
//  PlayerAppDelegate.m
//  Player
//
//  Created by Daniel Monteiro on 3/24/10.
//  Copyright Nano Games 2010. All rights reserved.
//

/// C++
#include <iostream>
#include <sstream>
#include <map>

/// 3rd Party
#include "bass.h"
#include "GDataXMLNode.h"

/// Nano Components
#import "ObjcMacros.h"
#include "EventManager.h"
#include "INetworkListener.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"


/// Aplicacao
#include "Config.h"
#include "RefazendaProxy.h"
#include "HTTPAudioStreamController.h"
#include "Tweet.h"

/// <Views>
#import "RadioView.h"
#import "BioView.h"
#import "RSSView.h"
#import "PhotosView.h"
#import "IntroView.h"
#import "MenuView.h"
#import "FacebookView.h"
#import "TwitterView.h"
#import "DiscoView.h"
#import "AboutView.h"
#import "RelatedTextView.h"
#import "PlaylistsView.h"
#import "AgendaView.h"

#import <mach/mach.h>
#import <mach/mach_host.h>

/// </Views>

/// Delegate
#import "PlayerAppDelegate.h"

@implementation PlayerAppDelegate

@synthesize window;

+ ( void ) print_free_memory {
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);        
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
        NSLog(@"Failed to fetch vm statistics");
    
    /* Stats in bytes */ 
    natural_t mem_used = (vm_stat.active_count +
                          vm_stat.inactive_count +
                          vm_stat.wire_count) * pagesize;
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_total = mem_used + mem_free;
    NSLog(@"used: %u free: %u total: %u", mem_used, mem_free, mem_total);
}

+(natural_t) get_free_memory {
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    vm_statistics_data_t vm_stat;
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        NSLog(@"Failed to fetch vm statistics");
        return 0;
    }
    /* Stats in bytes */
    natural_t mem_free = vm_stat.free_count * pagesize;
    return mem_free;
}

/*==============================================================================================
 
 MENSAGEM getDataDir:
 Obtem o diretorio de dados da aplicacao
 
 ================================================================================================*/

- (void) setBusy: (BOOL) state withListener:(SEL) callback onObject:(UpdatableView*) context 
{
	if ( state == YES )	{		
		///estamos entrando no modo ocupado
		BaseGilbertoGilView *currView=[[hViewManager subviews] objectAtIndex:0];		
		[currView setLocked: state];		
        [ hActivityIndicator setBackgroundColor: [ UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.5f ] ];
        [ hActivityIndicator setFrame: [ currView frame ] ];
        [ hActivityIndicator setContentMode: UIViewContentModeTopRight ];
		[hViewManager addSubview: hActivityIndicator];	
		[hActivityIndicator setHidden:NO];
		[hActivityIndicator setCenter:CGPointMake(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT)];	
		[hActivityIndicator startAnimating];
		
		tickerListener=context;
		tickerCallback=callback;
        
        
		if (tickerCallback != nil && tickerListener != nil)
			activityTicker= [NSTimer scheduledTimerWithTimeInterval: 0.05
															 target: self
														   selector: @selector(tickerDummyMethod:)
														   userInfo: nil
															repeats: NO]; 		
	} else {
		///estamos liberando o sistema
		[activityTicker invalidate];	
		activityTicker=nil;
		[hActivityIndicator setHidden:YES];
		[hActivityIndicator stopAnimating];
		[hActivityIndicator removeFromSuperview];
		BaseGilbertoGilView *currView=[[hViewManager subviews] objectAtIndex:0];		
		[currView setLocked: state];	
		[currView setAlpha: 1.0 ];		
	}	
}

/*==============================================================================================
 
 MENSAGEM tickerDummyMethod:
 Realiza a tarefa de segundo plano, quando o sistema deve sinalizar que esta ocupado
 
 ================================================================================================*/

- (void) tickerDummyMethod: (NSTimer *)timer
{
	[tickerListener performSelector:tickerCallback];
	[self setBusy:NO withListener:nil onObject:nil];
}

/*==============================================================================================
 
 MENSAGEM getDataDir:
 Obtem o diretorio de dados da aplicacao
 
 ================================================================================================*/

+ /**static*/ (NSString *) getDataDir
{
	NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);	
	NSString *docsDir = [dirPaths objectAtIndex:0];	
	docsDir=[docsDir stringByAppendingString:@"/"];
	return docsDir;
}


/*==============================================================================================
 
 MENSAGEM applicationDidFinishLaunching:
 Aplicacao acabou de ser carregada
 
 ================================================================================================*/

- (void)applicationDidFinishLaunching:(UIApplication *)application 
{
	// Inicializa os membros da superclasse
	[super applicationDidFinishLaunching: application];
	
	if( !EventManager::Create() )
		goto Error;
	[[UIApplication sharedApplication] setStatusBarHidden:YES animated:YES];
	[self initSoundSystem];
	[self initUI];
	[self performTransitionToView:screen_INTRO];
	
#if DEBUG
	LOG_FREE_MEM( "Inicio da app" );
#endif
	
	return;	
	
Error:
	[self quit: ERROR_ALLOCATING_VIEWS];
	return;
}

/*==============================================================================================
 
 MENSAGEM initSoundSystem:
 inicializa o sistema de som.
 
 ================================================================================================*/

- (void)initSoundSystem
{	
	if (!BASS_Init(-1,44100,0,NULL,NULL)) 		
	{
		///TODOO: tratamento de erros adequado
		[self quit:SOUND_SYSTEM_INIT_ERROR];
	}	
}

/*==============================================================================================
 
 MENSAGEM dealloc:
 Desaloca todos os recursos alocados
 
 ================================================================================================*/

- (void)dealloc 
{	
	
    [radioView onBeforeTransition];	
	[radioView release];
	[bioView onBeforeTransition];
	[bioView release];	
    [discoView onBeforeTransition];
	[discoView release];	
	[introView onBeforeTransition];
	[introView release];	
	[rssView onBeforeTransition];	
	[rssView release];	
	[photosView onBeforeTransition];
	[photosView release];	
	[menuView onBeforeTransition];		
	[menuView release];	
	[twitterView onBeforeTransition];		
	[twitterView release];	
	[hActivityIndicator release];	
	[relatedTextView onBeforeTransition];
	[relatedTextView release];	
	[aboutView onBeforeTransition];
	[aboutView release];
    [playlistsView onBeforeTransition];
	[playlistsView release];
    [ agendaView onBeforeTransition ];
    [ agendaView release ];

	
	[self releaseMenuViews];
	
	
	
	
    [window release];
    [super dealloc];
}

/*==============================================================================================
 
 MENSAGEM initData:
 inicializa os dados
 
 ================================================================================================*/

- (void) initDataWithSelector: (SEL)callback onObject:(id) obj
{
	///<TODO>:
	/*
	 * desalocar: 
	 - error
	 - doc
 	 * tratamento de excessoes:
	 - dados nao vieram
	 - dados vieram corrompidos
	 - nao ha conexao
	 - dados presentes no aparelho estao corrompidos
	 */
	///</TODO>
    BOOL hasFile = NO;
	NSString *str;	    
	bool retrieveData=true;
	
	////////<inicializa diretorios>////////////////		
	NSString *docsDir = [PlayerAppDelegate getDataDir];
	NSString *cacheDir = [docsDir stringByAppendingPathComponent:@"cached"];
	cacheDir=[cacheDir stringByAppendingString:@"/"];	
	CRefazendaProxy *proxy=	CRefazendaProxy::getInstance((char*)[docsDir UTF8String]);
	////////</inicializa diretorios>////////////////
	
	///<inicializa o diretorio de cache>
	NSFileManager *filemgr =[NSFileManager defaultManager];		
	
	if ([[filemgr contentsOfDirectoryAtPath:docsDir error: nil] indexOfObject:@"cached"] == NSNotFound)
		[filemgr createDirectoryAtPath:cacheDir withIntermediateDirectories:YES attributes:nil error: nil ];
	
	if ([[filemgr contentsOfDirectoryAtPath:cacheDir error: nil ] indexOfObject:@"discophotos"] == NSNotFound)
		[filemgr createDirectoryAtPath:[cacheDir stringByAppendingString:@"discophotos"] withIntermediateDirectories:YES attributes:nil error: nil ];
	
	if ([[filemgr contentsOfDirectoryAtPath:cacheDir error: nil] indexOfObject:@"generalphotos"] == NSNotFound)
		[filemgr createDirectoryAtPath:[cacheDir stringByAppendingString:@"generalphotos"] withIntermediateDirectories:YES attributes:nil error: nil ];
	
	///</inicializa o diretorio de cache>
	
	
	///<decide se pega ou nao os dados na web>
    
    NSArray *content = [ [ NSFileManager defaultManager ] contentsOfDirectoryAtPath: cacheDir  error: nil ];
    
    hasFile = ( ( [ content indexOfObject: @"albums" ] != NSNotFound ) ? YES : NO );
    
	int today=time(NULL);
	CMMLCategory::MMLCategoryReference confs=proxy->getCategoryByName(CONSTconfsCollection);
	CMMLEntity::MMLEntityReference stats;
	try 
	{
		stats=confs->getEntityByKeyValue("nome", "application-status");
		int lastupdate = 0;
        const char *lastUpdated = stats->getValueByKey("last-updated").c_str();
        lastupdate = AtoI( lastUpdated );		
		int aDayInTheLife = 60 /*segundos*/ * 60 /*minutos*/ * 24 /*horas = 1 dia*/;
#ifdef DEBUG
		NSLog(@"last update %d , day of the last update %d , now %d , today %d", lastupdate , lastupdate / aDayInTheLife , today, today/ aDayInTheLife);
#endif        
		retrieveData=(lastupdate / aDayInTheLife) != (today/ aDayInTheLife);
	}
	catch (MMLNonExistantEntity *ex) 
	{
#ifdef DEBUG        
		NSLog(@"nunca atualizado");
#endif        
		stats=new CMMLEntity();
		stats->setValueForKey("nome", "application-status");
		confs->addEntity(stats);
        [ self downloadData ];  
        
        content = [ [ NSFileManager defaultManager ] contentsOfDirectoryAtPath: cacheDir  error: nil ];        
        hasFile = ( ( [ content indexOfObject: @"albums" ] != NSNotFound ) ? YES : NO );
        
		retrieveData = !( hasFile == YES );

	}
	catch (MMLBaseException *ex) 
	{
		NSLog(@"excessao:%s", ex->what());
		throw;
	}
	
	
	///</decide se pega ou nao os dados na web>
	
    
    
	///<busca os dados>	
    
    
	
    
    
    if ( hasFile == YES ) {
        stats->setValueForKey("last-updated",ItoA(today));
        confs->commit();
#ifdef DEBUG
        NSLog( @" carregando arquivo: %@ ", [ cacheDir stringByAppendingString:@"albums" ] );
#endif        
        str = [ NSString stringWithContentsOfFile: [ cacheDir stringByAppendingString:@"albums" ] encoding: NSISOLatin1StringEncoding error: nil ];

		if (str!=nil)
		{
#ifdef DEBUG            
			NSLog(@"chegaram os discos");
#endif            
			GDataXMLDocument *doc;
			doc=[GDataXMLDocument alloc];	
			[doc initWithXMLString:str  options:0 error: nil ];		
			GDataXMLNode *node=[doc rootElement];		
			CRefazendaProxy::getInstance()->parseAlbumList(node);
			[doc release];
            
            if ( [ [ NSFileManager defaultManager ] removeItemAtPath: [ cacheDir stringByAppendingString:@"albums" ] error: nil ] == YES ) {
#ifdef DEBUG            
                NSLog(@"arquivo apagado!");
#endif            
            } else {
                NSLog(@"arquivo NÃO apagado!");
            }
            
		}
#ifdef DEBUG		
		NSLog(@"mandando o pedido para as fotos");				
#endif        
        
        str = [ NSString stringWithContentsOfFile: [ cacheDir stringByAppendingString:@"photos" ] encoding: NSISOLatin1StringEncoding error: nil ];
        
		if ( str!=nil ) {
#ifdef DEBUG            
			NSLog(@"chegaram as fotos");			
#endif            
			GDataXMLDocument *doc;
			doc=[GDataXMLDocument alloc];	
			[doc initWithXMLString:str  options:0 error: nil ];		
			GDataXMLNode *node=[doc rootElement];		
			CRefazendaProxy::getInstance()->parsePhotoList(node);
			[doc release];			
            
            if ( [ [ NSFileManager defaultManager ] removeItemAtPath: [ cacheDir stringByAppendingString:@"photos" ] error: nil ] == YES ) {
#ifdef DEBUG            
                NSLog(@"arquivo apagado!");
#endif            
            } else {
#ifdef DEBUG            
                NSLog(@"arquivo NÃO apagado!");
#endif            
            }            
		}
	}
	///</busca os dados>	
	///<liberacoes>
	//[error release];
	proxy=NULL;
	///</liberacoes>	
	[obj performSelector:callback];
    
    if ( retrieveData )
        [ NSThread detachNewThreadSelector:@selector( downloadData ) toTarget: self withObject: self ];
}

- ( void ) downloadData {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; 
    NSString *str;
    NSString *cacheDir = [ [ PlayerAppDelegate getDataDir ] stringByAppendingString:@"cached/" ];

	{
#ifdef DEBUG
		NSLog(@"mandando o pedido para os discos: %@", [ cacheDir stringByAppendingString: @"albums" ] );		
#endif
        
		str=[NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://gilbertogil.com.br/app_gil/info_lista_album.php"] encoding:NSISOLatin1StringEncoding error: nil ];
        
        if ( str != nil ) {
            [ str writeToFile: [ cacheDir stringByAppendingString: @"albums" ] atomically: NO encoding: NSISOLatin1StringEncoding error: nil ];
#ifdef DEBUG            
            NSLog( @" gravando arquivo " );            
#endif            
        } 
        
        str=[NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://gilbertogil.com.br/app_gil/info_lista_fotos.php"] encoding:NSISOLatin1StringEncoding error: nil ];
        
        if ( str != nil ) {
            [ str writeToFile: [ cacheDir stringByAppendingString:@"photos" ] atomically: NO encoding: NSISOLatin1StringEncoding error: nil ];
#ifdef DEBUG            
            NSLog( @" gravando arquivo " );            
#endif            
        } 
    }
    [ pool release ];
}
/*==============================================================================================
 
 MENSAGEM initUI:
 inicializa a interface grafica
 
 ================================================================================================*/
///<TODO>
/*
 - passar esses nomes de XIB para constantes
 */
///</TODO>
- (void) initUI
{
	//view atual de sistema e' nula
	currentView=screen_UNDEFINED;
	///<configura as views>
	ApplicationManager* hAppManager = [ApplicationManager GetInstance];
	introView = ( IntroView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "IntroView" ]];	
	menuView = ( MenuView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "MenuView" ]];	
	discoView = ( DiscoView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "DiscoView" ]];	
	rssView = ( RSSView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "RSSView" ]];	
	photosView = ( PhotosView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "PhotosView" ]];	
	radioView = ( RadioView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "RadioView" ]];	
	twitterView = ( TwitterView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "TwitterView" ]];	
	bioView = ( BioView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "BioView" ]];		
	relatedTextView = ( RelatedTextView* )[hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "RelatedTextView" ]];
    playlistsView = (PlaylistsView*)[ hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "PlaylistsView" ]];		
	aboutView = (AboutView*)[ hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "AboutView" ] ];
    agendaView = ( AgendaView * )[ hAppManager loadViewFromXib: [ PlayerAppDelegate getXIBName: "AgendaView" ] ];
	///</configura as views>	
	
	///<configura o indicador de ocupado>
	hActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    
	if( hActivityIndicator == nil )
		[self quit:ERROR_ALLOCATING_VIEWS];
	
	[hActivityIndicator setHidesWhenStopped: YES];
    
	///</configura o indicador de ocupado>
	
#ifdef DEBUG		
	NSLog(@"views carregadas");
#endif
}

/*==============================================================================================
 
 MENSAGEM performTransitionToView:
 muda a visao atual
 
 ================================================================================================*/

- ( void )performTransitionToView:( int8 )newIndex 
{


	if( [hViewManager isTransitioning] )
		return;
	
//	
//	if (newIndex == screen_TEXT && CRefazendaProxy::getInstance()->getCurrentAlbum()==NULL && CRefazendaProxy::getInstance()->getCurrentSong()==NULL )
//		return;
		
    lastScreen = currentView;
	
#ifdef DEBUG	
	NSLog(@"setview - from %d to %d",currentView,newIndex);
#endif
	
	BaseGilbertoGilView *views[]={introView,menuView,discoView,rssView,photosView,radioView,twitterView,bioView, relatedTextView, aboutView, playlistsView, agendaView };
	UpdatableView *hNextView=nil;
	
	bool keepCurrView;
	
	///<tmp>
	keepCurrView=true;
	///</tmp>
	
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView *hCurrView = [hSubviews objectAtIndex:0];
		
		if( hCurrView != hNextView )
		{
			if( keepCurrView )
				[hCurrView retain];
			
			[hCurrView removeFromSuperview];
		}
	}
	
	hNextView = views[newIndex];	
	
	[hViewManager addSubview: hNextView];
	
	if( [hNextView respondsToSelector: @selector( onBecomeCurrentScreen )] )
		[hNextView onBecomeCurrentScreen];
	
	
	[hNextView release];	
	
	[self syncNewViewWithApplicationState];
	currentView=newIndex;
	if (![self isStatusBarHidden])
	{
		CGRect statusBarSize = [[UIApplication sharedApplication] statusBarFrame];
		[hNextView setFrame:CGRectMake(0, statusBarSize.origin.y+statusBarSize.size.height, SCREEN_WIDTH, SCREEN_HEIGHT-(statusBarSize.origin.y+statusBarSize.size.height))];
	}
}

/*==============================================================================================
 
 MENSAGEM releaseMenuViews
 Desaloca a memória alocada pelas telas dos menus.
 
 ==============================================================================================*/

- ( void ) releaseMenuViews
{
	int32 nSubviews = [[hViewManager subviews] count];
	while( nSubviews )
	{
		UIView* hView = [[hViewManager subviews] objectAtIndex: 0];
		[hView removeFromSuperview];
		--nSubviews;
	}
}

- ( ViewId ) getCurrentView {
    return currentView;
}

/*==============================================================================================
 
 MENSAGEM transitionDidFinish
 Chamada quando uma transição de views acabou de terminar.
 
 ==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )hManager
{
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		BaseGilbertoGilView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( onBecomeCurrentScreen )] )
			[hCurrView onBecomeCurrentScreen];
	}	
	[self syncNewViewWithApplicationState];
}

/*==============================================================================================
 
 MENSAGEM syncNewViewWithApplicationState
 Sincrozina o estado da view atual com o estado da aplicação. Este método serve para contornar
 a possibilidade de recebermos um evento de suspend enquanto estamos transitando entre views.
 
 ==============================================================================================*/

- ( void ) syncNewViewWithApplicationState
{
	if( [self getApplicationState] == APPLICATION_STATE_SUSPENDED )
	{
		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			BaseGilbertoGilView *hCurrView = [hSubviews objectAtIndex:0];
			if( [hCurrView respondsToSelector: @selector( suspend )] )
				[hCurrView suspend];
		}
	}
}

/*==============================================================================================
 
 MENSAGEM transitionDidCancel:
 Chamada quando uma transição de views é cancelada.
 
 ==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )hManager
{
}



/*==============================================================================================
 
 MENSAGEM applicationWillResignActive
 Mensagem chamada quando a aplicação vai ser suspensa.
 
 ==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	///<TODO>
	/*
	 - parar os sons
	 - interromper qualquer download de forma graciosa
	 - verificar se existe alguma thread ativa
	 - deixar o sistema num estado que quando voltar, ele seja capaz de voltar a ele de forma
	 consistente
	 */
	///</TODO>
	
	[super applicationWillResignActive: application];
	
	// Pára de renderizar e atualizar as views e scenes do jogo
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		BaseGilbertoGilView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( suspend )] )
			[hCurrView suspend];
	}
}

/*==============================================================================================
 
 MENSAGEM applicationDidBecomeActive
 Mensagem chamada quando a aplicação é reiniciada.
 
 ==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	[super applicationDidBecomeActive: application];
	
	// Volta a executar os sons
	///<TODO>
	/*
	 - reiniciar os sons, se estava tocando algum
	 - reiniciar qualquer download necessario
	 - reinciiar qualquer thread que estivesse sendo executada
	 */
	///</TODO>
	
	// Reinicia a renderização e a atualização das views e scenes do jogo
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		BaseGilbertoGilView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( resume )] )
			[hCurrView resume];
	}
}

/*==============================================================================================
 
 MENSAGEM applicationWillTerminate
 Tells the delegate when the application is about to terminate. This method is optional. This
 method is the ideal place for the delegate to perform clean-up tasks, such as freeing allocated
 memory, invalidating timers, and storing application state.
 
 ==============================================================================================*/

- ( void )applicationWillTerminate:( UIApplication* )application
{
#if DEBUG
	LOG( "applicationWillTerminate\n" );
#endif
	
	
	// Reabilita o modo sleep
	[[UIApplication sharedApplication] setIdleTimerDisabled: NO];
	
	// Suspende o processamento da tela atual
	[self applicationWillResignActive: application];
	
	EventManager::Destroy();
    [ hActivityIndicator autorelease ];
}

/*==============================================================================================
 
 MENSAGEM applicationDidReceiveMemoryWarning
 Tells the delegate when the application receives a memory warning from the system. This
 method is optional. In this method, the delegate tries to free up as much memory as possible.
 After the method returns (and the delegate then returns from applicationWillTerminate), the
 application is terminated.
 
 ==============================================================================================*/

- ( void )applicationDidReceiveMemoryWarning:( UIApplication* )application
{
#if DEBUG
	LOG( "applicationDidReceiveMemoryWarning\n" );
#endif
	
	// OBS: Não vamos abortar a aplicação pois recebemos MemoryWarnings mesmo quando ainda temos uma
	// quantidade razoável de memória
	//[self quit: ERROR_MEMORY_WARNING];
}

/*==============================================================================================
 
 MENSAGEM applicationSignificantTimeChange
 Tells the delegate when there is a significant change in the time. This method is optional.
 Examples of significant time changes include the arrival of midnight, an update of the time by
 a carrier, and the change to daylight savings time. The delegate can implement this method to
 adjust any object of the application displays time or is sensitive to time changes.
 
 ==============================================================================================*/

-( void )applicationSignificantTimeChange:( UIApplication* )application
{
#if DEBUG
	LOG( "applicationSignificantTimeChange\n" );
#endif
}

- ( void ) back {
    [ self performTransitionToView: lastScreen ];    
}


+ ( char *) getXIBName: (char *)xibName {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return  (( char *) [ [ NSString stringWithFormat: @"%s%s", xibName, "~ipad" ] UTF8String ]);
    } else {
        return xibName;
    }    
}
@end
