/*
 *  HTTPAudioStreamController.cpp
 *  Player
 *
 *  Created by Daniel Monteiro on 6/21/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#include "MMLConstants.h"
#include "Config.h"


#include "HTTPAudioStreamController.h"

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

HTTPAudioStreamController::HTTPAudioStreamController():
sessionId("-1"),playerState(PlayState_STOP)
{
}	

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::startStream(NSString *Url)
{
	///<declaracoes de variaveis locais>
	///objc
	NSMutableString *requestUrl;
	///cpp
	myOwnHTTPSender=new CHTTPBridge();
	///</declaracoes de variaveis locais>	
	
	requestUrl=[[NSMutableString alloc] init];
	[requestUrl appendString:[NSString stringWithUTF8String:CONSTaudioServerURL.c_str()]];

	[requestUrl appendString:Url];	
	
	if (CONSTusingDedicatedAudioServer)
		myOwnHTTPSender->request(CONSTdedicatedAudioServerStreamCmd.c_str(), [requestUrl UTF8String],CONSTdedicatedAudioServerPassphrare.c_str(),this);				
	else	
		streamAt([requestUrl UTF8String]);
	
	[ requestUrl autorelease ];
#ifdef DEBUG    
	NSLog(@"mandando pedido de stream");
#endif    
}

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

HTTPAudioStreamController::~HTTPAudioStreamController()
{
	///precisa settar o cookie de session-id
	//	myOwnHTTPSender->request("KILL-STREAM", [requestUrl UTF8String],"NANOGAMES",this);		
}

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr )
{
#ifdef DEBUG    
	NSLog(@"network error!%d %s",errorCode, errorStr.c_str());
#endif	
}

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::onNetworkProgressChanged( int32 currBytes, int32 totalBytes )
{
	
}

/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::onNetworkRequestCompleted( std::vector< uint8 >& data )
{
	///precisa pegar o cookie de session-id
#ifdef DEBUG    
	NSLog(@"HTTPAudioStreamController iniciando streaming!");
#endif    
	/////////////////////////////////////////////////////////////////////////////////////////	
	///<declaracoes de variaveis locais>
	///objc
	GDataXMLDocument *doc;
	NSString *audiourl;		
	NSError *error;
	///cpp
	std::string Data;
	///</declaracoes de variaveis locais>	
	
	doc=[GDataXMLDocument alloc];			
	error=[NSError alloc];	
	
	{
		
		MemoryStream dataStream;
		dataStream.swapBuffer( data );
		uint8 *buffer=(uint8*)malloc(dataStream.getSize());
		dataStream.readData(buffer, dataStream.getSize());
		Data=(char*)buffer;
		free(buffer);
		
	}
	
	/////////////////////////////////////////////////////////////////////////////
	{
		NSMutableString *responseStr;
		NSString *httpHeaders;
		NSRange range;
		NSArray *urls; 
		GDataXMLElement *node;
		///obtendo as urls
		responseStr=[NSString stringWithUTF8String:(char*) Data.c_str() ];		
		
		///<? e' do comeco da tag <?xml version='1.0' encoding='UTF-8'?> , para pular o header do HTTP
		///ate' onde vai os headers do HTTP? ate logo imediatamente o CRLF...ou seja, o comeco do XML
		range=NSMakeRange(0, [responseStr rangeOfString:@"<?"].location);
		///copio essa string...
		httpHeaders=[responseStr substringWithRange:range ];
		///para elimina-la ao alimentar o GData
		[doc initWithXMLString:[responseStr stringByReplacingOccurrencesOfString: httpHeaders withString:@""]  options:0 error:&error ]; 		
		//----------
		////basicamente, estou pesquisando a arvore DOM com uma requisicao XPath que busca os filhos de ports, que sejam port e que tenham um campo chamado
		///name e com valor=="audio"
		urls = [doc nodesForXPath:@"/stream-info/ports/port[@name='audio']" error:&error];		
		///so' deve ter um elemento, mas mesmo que tenha, por enquanto, queremos apenas o primeiro
		node= [urls objectAtIndex:0];
		/// esse stringByReplacingOccurrencesOfString e' para eliminar qualquer quebra de linha. O GData nao gosta.
		audiourl= [[node stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ];
		
		
		urls = [doc nodesForXPath:@"/stream-info/session-id" error:&error];		
		node= [urls objectAtIndex:0];
		sessionId= [[[node stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ] UTF8String ];
		NSLog(@"session-id: %s" , sessionId.c_str());
		//----------
		urls = [doc nodesForXPath:@"/stream-info/ports/port[@name='control']" error:&error];			
		node=[urls objectAtIndex:0 ];	
//		NSLog([[node stringValue] stringByReplacingOccurrencesOfString:@"\n" withString:@"" ]);	
	}
#ifdef DEBUG    
	NSLog(@" streaming %@",audiourl);
#endif    
	streamAt([audiourl UTF8String]);
	//////////////////////////////////////////////////////////////////////////
	[ error autorelease ];
	[doc autorelease];
}


/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::streamAt(const char *url)
{
#ifdef DEBUG	
	NSLog(@"iniciando streaming");
#endif
	
	///TODOO: adicionar checagem de erro nas configuracoes
	
	///<inicializando a BASS>
	BASS_Start();
	BASS_SetConfig(BASS_CONFIG_NET_PREBUF,100);
	BASS_SetConfig(BASS_CONFIG_NET_TIMEOUT,5000);
	BASS_SetConfig(BASS_CONFIG_BUFFER,5000);
	BASS_SetConfig(BASS_CONFIG_NET_PLAYLIST,2);
	BASS_SetConfig(BASS_CONFIG_UPDATEPERIOD,50);	
	BASS_SetConfigPtr(BASS_CONFIG_NET_PROXY,NULL);
	BASS_StreamFree(chan);
	///</inicializando a BASS>		

	///<toca o stream em si - isso aqui precisa melhorar...Apesar de rodar em outro thread, o controle de buffer, nao deveria estar aqui>
	chan=BASS_StreamCreateURL(url,0,BASS_SAMPLE_FLOAT|BASS_STREAM_AUTOFREE,0,0);
	if (chan)
	{	
		playerState=PlayState_PLAY;
		BASS_ChannelPlay(chan,FALSE);		
        long len=BASS_ChannelGetLength(chan, BASS_POS_BYTE); // the length in bytes
        double time=BASS_ChannelBytes2Seconds(chan, len); // the length in seconds
#ifdef DEBUG        
        NSLog(@"tempo: %d", ( ( int ) time ) );
#endif      
	}
	
#ifdef DEBUG	
	else
	{
		NSLog(@"erro:%d",BASS_ErrorGetCode());
		playerState=PlayState_STOP;
		throw new AudioStreamUnavailableException();
	}
	
	NSLog(@"Stream finalizado");
#endif	
}

int HTTPAudioStreamController::getSongLengthInSeconds() {
    long len=BASS_ChannelGetLength(chan, BASS_POS_BYTE); // the length in bytes
    double time=BASS_ChannelBytes2Seconds(chan, len); // the length in seconds
    return ( int )time;
}

int HTTPAudioStreamController::getSongPositionInSeconds() {
    long len=BASS_ChannelGetPosition(chan, BASS_POS_BYTE); // the length in bytes
    double time=BASS_ChannelBytes2Seconds(chan, len); // the length in seconds
    return ( int ) time;
}


/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::onNetworkRequestCancelled()
{
#ifdef DEBUG
	NSLog(@"request cancelled");
#endif    
}



/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

PlayStateEnum HTTPAudioStreamController::getPlayerState()
{
	return playerState;
}


/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

bool HTTPAudioStreamController::checkForSongEnding()
{	
	
	if (BASS_ChannelIsActive(chan)!=BASS_ACTIVE_PLAYING && playerState!=PlayState_PLAY)
		return false;
	
	
	if (BASS_ChannelIsActive(chan)!=BASS_ACTIVE_PLAYING)
		return true;
	return false;
}



/**************************************************************************************************
 * read all entities from disk
 ***************************************************************************************************/

void HTTPAudioStreamController::requestPlayerState(PlayStateEnum PlayerState)
{
	switch (PlayerState) 
	{
		case PlayState_PLAY:		
			BASS_Start();
			playerState=PlayerState;
			break;
		case PlayState_PAUSE:
			BASS_Pause();			
			playerState=PlayerState;			
			break;
		case PlayState_FORCESTOP:
		{
			/*
			if (getPlayerState()==PlayState_PLAY)
			{
				NSMutableString *requestUrl;
				myOwnHTTPSender=new CHTTPBridge();
				requestUrl=[[NSMutableString alloc] init];
				[requestUrl appendString:@"http://staging.nanogames.com.br:8080/"];
				//[requestUrl appendString:@"http://192.168.1.178:8080/"];
				[requestUrl appendString:[NSString stringWithUTF8String:sessionId.c_str()]];	
				NSLog(requestUrl);			
				myOwnHTTPSender->request("KILL-STREAM", [requestUrl UTF8String],"",NULL);				
			}
			*/
			
		}
		case PlayState_STOP:
			BASS_Stop();	
			BASS_StreamFree(chan);
			playerState=PlayerState;
			break;
		default:
			break;
	}	
}