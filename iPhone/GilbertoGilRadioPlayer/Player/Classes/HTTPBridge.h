/*
 *  CHTTPBridge.h
 *  Player
 *
 *  Created by Daniel Monteiro on 4/30/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

// Foundation
#include <CFNetwork/CFHTTPAuthentication.h>
#include <CFNetwork/CFHTTPMessage.h>
#include <CFNetwork/CFHTTPStream.h>
#include <CFNetwork/CFNetworkErrors.h>
// C++
#include <string>
#include <bits/stl_pair.h>
#include <vector>

///componentes
#include "MemoryStream.h"
#include "DeviceInterface.h"
#include "NetworkManager.h"
#include "INetworkListener.h"
#include "Macros.h"
#include "Utils.h"

/// NanoOnline
#include "NanoTypes.h"
#include "NetworkManagerErrors.h"
#include "Exception.h"

///MML
#include "MMLConstants.h"

#warning deprecado: trocar por mecanismo mais apropriado
///<TODO>
/*
 trocar por algo menos nojento
 */
///</TODO>
#define UNABLE_TO_CREATE_HTTP_REQUEST_STRING new Exception("unable to create http request string")
#define UNABLE_TO_CREATE_HTTP_MSG new Exception("unable to create http message")
#define UNABLE_TO_OBTAIN_USER_AGENT new Exception("unable to obtain user agent")
#define CANT_CREATE_DATA_BUFFER new Exception("unable to create data buffer")



// Macros de desalocação de objetos CF*
#define CFKILL( p ) { CFRelease( p ); p = NULL; }
#define SAFE_CFKILL( p ) { if( p != NULL )CFKILL( p ); }

// Tamanho do buffer de leitura que irá armazenar as respostas das requisições HTTP
#define NETWORK_MANAGER_READ_BUFFER_LEN 1024

// Códigos de erro HTTP
// http://en.wikipedia.org/wiki/List_of_HTTP_status_codes

#define HTTP_STATUS_OK 200

// Indica se o código de erro indica redirecionamento
#define IS_REDIRECTION_HTTP_STATUS_CODE( errorCode ) (( errorCode >= 300 ) && ( errorCode <= 307 ))




#ifndef HTTPBRIDGE_H
#define HTTPBRIDGE_H 1




class CHTTPBridge:private INetworkListener
	{
	public:
		enum CHTTPBridgeStatus {READY,BUSY,ERROR};
//////////////vindo de INetworkListener /////////////////////////////////////////////////		
		
		// Indica que ocorreu um erro na requisição
		virtual void onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr );
		
		// Indica o progresso atual da requisição
		virtual void onNetworkProgressChanged( int32 currBytes, int32 totalBytes );
		
		// Indica que a requisição terminou com sucesso
		virtual void onNetworkRequestCompleted( std::vector< uint8 >& data );
		
		// Indica que a requisição foi cancelada pelo usuário
		virtual void onNetworkRequestCancelled( void );	
/////////////// Cuida do resto ///////////////////////////////////////////////////////////
		static CHTTPBridge*getInstance();
		void Destroy();
		CFStringRef GetDeviceUserAgent();
		void request(const char*cmd,const char* url,const char* body,const INetworkListener *Listener);
		void sendPost(const char* url,const char* body,const INetworkListener *Listener);
		const std::string getData();
		std::string Data;
		CHTTPBridge();
		CHTTPBridgeStatus getStatus();
//-------------------------------------------------------------------------------------------		
	private:
		INetworkListener *listener;
		CHTTPBridgeStatus Status;
		static CHTTPBridge* Instance;
		NetworkManager NetMan;
	};

#endif
