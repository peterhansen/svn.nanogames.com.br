//
//  Tweet.cpp
//  Player
//
//  Created by Daniel Monteiro on 5/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <string>
#include "Tweet.h"

Tweet::Tweet( std::string user, std::string name, std::string tweet, std::string pubdate ) {        
    timeStamp = ( pubdate );
    realName = ( name );
    userName = ( user ); 
    tweetText = ( tweet );
}    

const std::string Tweet::getTweetText() const {
    return tweetText;
}

const std::string Tweet::getPubDate() const {
    return timeStamp;
}


const std::string Tweet::getUserName() const {
    return userName;
}


const std::string Tweet::getRealName() const {
    return realName;
}  