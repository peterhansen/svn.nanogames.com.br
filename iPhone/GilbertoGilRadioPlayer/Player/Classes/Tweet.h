//
//  Tweet.h
//  Player
//
//  Created by Daniel Monteiro on 5/24/11.
//  Copyright 2011 Nano Games. All rights reserved.
//
#ifndef TWEET_H
#define TWEET_H

class Tweet {
    
private:
    
    std::string timeStamp;
    std::string realName;
    std::string userName;
    std::string tweetText;
    
public:
    
    Tweet( std::string user, std::string name, std::string tweet, std::string pubdate );    
    const std::string getTweetText() const;    
    const std::string getPubDate() const;    
    const std::string getUserName() const;    
    const std::string getRealName() const;
    
};

#endif