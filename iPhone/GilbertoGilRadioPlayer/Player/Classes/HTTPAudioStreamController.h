/*
 *  HTTPAudioStreamController.h
 *  Player
 *
 *  Created by Daniel Monteiro on 6/21/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */
#include <dirent.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"
#import "HTTPBridge.h"
#import "GDataXMLNode.h"
#include <string.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <errno.h> 
#include <string.h>
#include <string>
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <sys/wait.h> 
#include <signal.h> 
#include "bass.h"

class HTTPAudioStreamController: public INetworkListener
	{
	private:
		
		CHTTPBridge *myOwnHTTPSender;
		///BASS channel handler
		HSTREAM chan;
		PlayStateEnum playerState;
		void streamAt(const char *url);
		std::string sessionId;
	public:
		bool checkForSongEnding();
		
        int getSongLengthInSeconds();
        
        int getSongPositionInSeconds();
        
		PlayStateEnum getPlayerState();
		
		///peça "por favor"!
		void requestPlayerState(PlayStateEnum PlayerState);
		
		/// construtor:
		HTTPAudioStreamController();		
		
		// Destrutor
		virtual ~HTTPAudioStreamController( void );
		
		void startStream(NSString *Url);
		
		// Indica que ocorreu um erro na requisição
		virtual	void onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr );
		
		// Indica o progresso atual da requisição
		virtual void onNetworkProgressChanged( int32 currBytes, int32 totalBytes );
		
		// Indica que a requisição terminou com sucesso
		virtual void onNetworkRequestCompleted( std::vector< uint8 >& data );
		
		// Indica que a requisição foi cancelada pelo usuário
		virtual void onNetworkRequestCancelled( void );
	};


