/*
 *  Expection.h
 *  Player
 *
 *  Created by Daniel Monteiro on 6/7/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef EXCEPTION_H
#define EXCEPTION_H

//----------------------------------------------------------------------------------------------------
/**
 * @author Daniel Monteiro
 * @brief Excessao basica usada pelo sistema
 */
//----------------------------------------------------------------------------------------------------
class Exception: public std::exception
{	
	/// --------------------------PUBLICOS----------------------------------------------------------------
public:
	/*==============================================================================================	 
	 MENSAGEM what:
	 @brief retorna uma mensagem humana a respeito da excessao
	 @returns const char* contendo a mensagem humana	 
	 ================================================================================================*/
	
	virtual const char* what() const throw()
	{
		return desc.c_str();
	}
	
	
	/*==============================================================================================
	 
	 MENSAGEM Exception
	 @brief Destrutor
	 @param std::string Desc	descricao humana da excessao	 
	 ================================================================================================*/
	
	Exception(std::string Desc)
	{
		desc=Desc;
	}
	
	/*==============================================================================================	 
	 MENSAGEM ~Exception
	 @brief Destrutor	 
	 ================================================================================================*/
	
	virtual ~Exception() throw()  
	{
		
	}
	
	/// --------------------------PRIVADOS----------------------------------------------------------------	
private:
	
	/// descricao da excessao
	std::string desc;
	
};
//*----------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------
class AudioStreamUnavailableException : public Exception
{
public:
	
	AudioStreamUnavailableException() : Exception("Stream de audio não pôde ser inciializado")
	{		
	}
};
#endif