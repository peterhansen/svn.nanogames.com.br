//
//  PlayerAppDelegate.h
//  Player
//
//  Created by Daniel Monteiro on 3/24/10.
//  Copyright Nano Games 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicationManager.h"
#import "Config.h"
#import "UpdatableView.h"

///declaração forward das views.
@class IntroView;
@class MenuView;
@class PhotosView;
@class BioView;
@class TwitterView;
@class RSSView;
@class DiscoView;
@class RadioView;
@class RelatedTextView;
@class AboutView;
@class PlaylistsView;
@class AgendaView;

@interface PlayerAppDelegate : ApplicationManager 
{
	///Janela da aplicacao
    UIWindow *window;
	
	///<view>
	///view de biografia
	BioView *bioView;
	///view de noticias
	RSSView *rssView;
	///view de abertura
	IntroView *introView;
	///view de fotos gerais
	PhotosView *photosView;
	///view de menu principal
	MenuView *menuView;
	///view de twitter
	TwitterView *twitterView;
	///view de discografia
	DiscoView *discoView;
	///view de radio (playlist)
	RadioView *radioView;
	///view de créditos
	AboutView *aboutView;
    
    AgendaView *agendaView;
    
    PlaylistsView *playlistsView; 
	
	///view de textos
	RelatedTextView *relatedTextView;

	///</views>
	
	///id da vida atual
	ViewId currentView;
	
	///indicador de ocupado
	UIActivityIndicatorView *hActivityIndicator;
	///timer para realizar tarefas em segundo plano (enquanto mostra ao usuario que esta ocupado)
	NSTimer *activityTicker;
	///qual view pediu para aparecer ocupada
	UpdatableView *tickerListener;
	///callback da tarefa a ser realizada
	SEL tickerCallback;
    ViewId lastScreen;	
}

///<DM> retain? </DM>
@property (nonatomic, retain) IBOutlet UIWindow *window;

+ /**static*/ (NSString *) getDataDir;

///<ciclo de vida>
- (void) initDataWithSelector: (SEL)callback onObject:(id) obj;
- (void) initUI;
- (void)initSoundSystem;
- ( void ) releaseMenuViews;
///</ciclo de vida>
- ( ViewId ) getCurrentView;
- ( void )performTransitionToView:( int8 )newIndex;
- (void) setBusy: (BOOL) state withListener:(SEL) callback onObject:(UpdatableView*)context;
+ ( void ) print_free_memory;
- ( void ) downloadData;
+(natural_t) get_free_memory;
- ( void ) back;
+ ( char * ) getXIBName: ( char *) xibName;
@end

