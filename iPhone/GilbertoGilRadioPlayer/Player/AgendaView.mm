//
//  AgendaView.mm
//  Player
//
//  Created by Daniel Monteiro on 7/28/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


//
//  BioView.m
//  Player
//
//  Created by Daniel Monteiro on 9/3/10.
//  Copyright 2010 Nano Games. All rights reserved.
//
/// C++
#include <iostream>
#include <sstream> 
#include <map>

/// 3rd Party
#import "GDataXMLNode.h"

/// Nano Components
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#import "NetworkManager.h"

/// MML
#include "MMLUtils.h"
#include "MMLConstants.h"
#include "MMLCategory.h"
#include "MMLEntity.h"
#include "MMLDatabase.h"

/// Aplicacao
#import "HTTPBridge.h"
#import "PlayerAppDelegate.h"
#import "Config.h"
#include "RefazendaProxy.h"

/// View
#import "AgendaView.h"


@implementation AgendaView

/*==============================================================================================
 
 MENSAGEM initWithFrame:
 Construtor chamado quando carregamos a view via código.
 
 ================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildAgendaView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM initWithCoder:
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildAgendaView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

-( bool )buildAgendaView {
    [self setHidden: YES];	
	updated=NO;
    doc=[GDataXMLDocument alloc];	
	[ NSThread detachNewThreadSelector:@selector( updateContent ) toTarget: self withObject: self ];
	return true;
}

- (void) updateContent {
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init]; 
    
    
	NSString *fullPathToFile=[ NSString stringWithFormat:@"%@cached/%s", [PlayerAppDelegate getDataDir], "agenda" ];
	NSMutableString *content;
	NSString *contentRaw;
	NSString *contentRawTmp;	
	NSURL *url=[NSURL URLWithString: [NSString stringWithUTF8String:CONSTagendaURL.c_str()]];
	
	content = [[NSMutableString alloc] init];
	
	contentRaw = [NSString stringWithContentsOfFile:fullPathToFile encoding: NSUTF8StringEncoding error: nil ];
	
#ifdef DEBUG
	NSLog(@"from cache :%@",contentRaw);
#endif		
    
	contentRawTmp=[NSString stringWithContentsOfURL:url encoding: NSUTF8StringEncoding error: nil ];
	
	if (contentRawTmp!=nil && [contentRawTmp length]!=0)
	{
		contentRaw=contentRawTmp;
		[contentRaw writeToFile:fullPathToFile atomically:NO encoding: NSUTF8StringEncoding error: nil ];
	}
	
	if (contentRaw!=nil && [contentRaw length]!=0)
	{
		[doc initWithXMLString:contentRaw  options:0 error: nil ];		
        [ self performSelectorOnMainThread:@selector( updateUI ) withObject: self waitUntilDone: YES ];
		updated=YES;	
        
#ifdef DEBUG
		NSLog(@"to cache :%@",content);
#endif		
	}
    
	[content autorelease];	
    [ pool release ];
}
    

- (NSString*)getMonthName:(int)month
{
	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	return [[formatter monthSymbols] objectAtIndex:(month - 1)];
}

- ( void ) updateUI {
    
    NSArray *items;
    NSArray *propertyHolder;
    NSMutableString *contentHTML = [ [ NSMutableString alloc ] init ];
    
    GDataXMLElement *root = [ doc rootElement ];
    
    items = [ root nodesForXPath:@"/agenda/evento" error: nil ];        
    [ contentHTML appendString:@"<html><head>" ];
    [ contentHTML appendString: CRefazendaProxy::getInstance()->getStyle() ];
    [ contentHTML appendString:@"</head><body>" ];

    NSString *date = @"";    
    NSString *month = @"";
    NSString *day = @"";
    NSString *tmp = @"";
    
        
    for (GDataXMLElement *item in items) {
        
    

        propertyHolder = [ item nodesForXPath:@"data" error: nil ];
        
        if ( propertyHolder != nil && [ propertyHolder count ] > 0 && [ [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] length ] > 0  ) {
            date = [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ];
            
            day = [ date substringToIndex: 2 ];
            
            tmp = [ [ date substringFromIndex: 3 ] substringToIndex: 2 ];
            
            if ( [ tmp compare: month ] != NSOrderedSame ) {
                month = tmp;                
                [ contentHTML appendString:@"<h1 style = 'font-size:16px;color:#6287a3;' >" ];                        
                [ contentHTML appendString: [ self getMonthName: AtoI( [ month UTF8String ] ) ] ];                                    
                [ contentHTML appendString:@"</h1>" ];                                                    
            }
           [ contentHTML appendString:@"<p>" ];                
           [ contentHTML appendString:@"<b>" ];
           [ contentHTML appendString: day ];
           [ contentHTML appendString:@"</b>" ];
           [ contentHTML appendString:@" &gt; " ];
            
        }

        propertyHolder = [ item nodesForXPath:@"cidade" error: nil ];
        
        if ( propertyHolder != nil && [ propertyHolder count ] > 0 && [ [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] length ] > 0  ) {
            [ contentHTML appendString: [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] ];
           [ contentHTML appendString:@" / " ];
        }
        
        
        propertyHolder = [ item nodesForXPath:@"estado" error: nil ];
        
        if ( propertyHolder != nil && [ propertyHolder count ] > 0 && [ [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] length ] > 0  ) {
            [ contentHTML appendString: [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] ];           
            [ contentHTML appendString:@"<br/>" ];
        }        
        
        propertyHolder = [ item nodesForXPath:@"endereco" error: nil ];
        
        if ( propertyHolder != nil && [ propertyHolder count ] > 0 && [ [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] length ] > 0  ) {
            [ contentHTML appendString:@"endereço:" ];        
            [ contentHTML appendString: [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] ];

            [ contentHTML appendString:@": " ];             
        }        
        
        propertyHolder = [ item nodesForXPath:@"nome" error: nil ];
        
        if ( propertyHolder != nil && [ propertyHolder count ] > 0 && [ [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] length ] > 0 ) {
            [ contentHTML appendString: [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] ];
            [ contentHTML appendString:@"<br/>" ];            
        }
        propertyHolder = [ item nodesForXPath:@"horario" error: nil ];
        
        if ( propertyHolder != nil && [ propertyHolder count ] > 0 && [ [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] length ] > 0  ) {
    
            [ contentHTML appendString: [ ( ( GDataXMLElement * ) [ propertyHolder objectAtIndex: 0 ] ) stringValue ] ];
            [ contentHTML appendString:@"<br/>" ];            
        }
        [ contentHTML appendString:@"</p>" ];        
        [ contentHTML appendString:@"<br/>" ];   
            
    }
    [ contentHTML appendString:@"</body></html>" ];
    [ output loadHTMLString: contentHTML baseURL: nil ];
    [ contentHTML autorelease ];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [ webView  setMultipleTouchEnabled: NO ];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    [ webView  setMultipleTouchEnabled: NO ];
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ webView  setMultipleTouchEnabled: NO ];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [ webView  setMultipleTouchEnabled: NO ];
}

@end
