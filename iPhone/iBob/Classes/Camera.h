#ifndef CAMERA_H
#define CAMERA_H

#include "Point3f.h"

// Tipos de câmera
enum CameraType
{
	CAMERA_TYPE_LAND = 0,
	CAMERA_TYPE_AIR
};

class Camera
{
	public:
		// Construtores
		Camera( CameraType cameraType = CAMERA_TYPE_AIR );
	
		// Destrutor
		virtual ~Camera( void ){};

		// Configura os atributos da câmera na API gráfica
		virtual void place( void );
	
		// Equivalente a gluLookAt
		// - http://www.opengl.org/documentation/specs/man_pages/hardcopy/GL/html/glu/lookat.html
		static void lookAt( const Point3f* pEye, const Point3f* pCenter, const Point3f* pUp );
	
		// Determina o tipo de câmera a ser utilizado
		inline void setCameraType( CameraType cameraType ){ type = cameraType; };
	
		// Obtém o tipo de câmera utilizado
		inline CameraType getCameraType( void ) const { return type; };
	
		// Obtém a posição da câmera
		inline Point3f* getPosition( Point3f* pOut ) const { *pOut = pos; return pOut; };

		// Determina a posição da câmera
		inline void setPosition( const Point3f* pPosition ) { pos = *pPosition; };
	
		// Rotaciona a câmera ao redor do eixo right
		void pitch( float angle );
	
		// Rotaciona a câmera ao redor do eixo up
		void yaw( float angle );
	
		// Rotaciona a câmera ao redor do eixo look
		void roll( float angle );
	
		// Movimenta a câmera ao longo do eixo look
		void walk( float units );
	
		// Movimenta a câmera ao longo do eixo right
		void strafe( float units );
	
		// Movimenta a câmera ao longo do eixo up
		void fly( float units );
	
		// Direciona o eixo look
		void lookAt( const Point3f* pSpot );
	
		// Obtém o eixo up
		inline Point3f* getUp( Point3f* pOut ) const { *pOut = up; return pOut; };
	
		// Obtém o eixo look
		inline Point3f* getLook( Point3f* pOut ) const { *pOut = look; return pOut; };
	
		// Obtém o eixo right
		inline Point3f* getRight( Point3f* pOut ) const { *pOut = right; return pOut; };

	private:
		// Garante que os eixos right, up e look são ortogonais entre si
		void makeAxesOrthogonal( void );

		// Tipo da câmera
		CameraType type;
	
		// Atributos da câmera
		Point3f	pos, right /*X*/, up/*Y*/, look /*Z*/;
};

#endif