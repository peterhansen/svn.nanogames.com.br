#import "CreditsView.h"

// Início da implementção da classe
@implementation CreditsView

/*==============================================================================================

MENSAGEM onGoToURL
	Abandona a aplicação e abre o navegador no site da nanogames.

==============================================================================================*/

- ( IBAction ) onGoToURL
{
	NSURL* url = [NSURL URLWithString: @"http://www.nanogames.com.br"];
	[[UIApplication sharedApplication] openURL:url];
}

// Fim da implementção da classe
@end
