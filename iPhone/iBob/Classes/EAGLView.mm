#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>

#import "EAGLView.h"
#import "iBobAppDelegate.h"

#include "AccManager.h"
#include "EventManager.h"
#include "Macros.h"
#include "Config.h"

#ifdef RENDER_TEST_SCENE
	#include "TestScene.h"
#else
	#include "BobFullScene.h"
#endif

// Extensão da classe para declarar métodos privados
@interface EAGLView ()

- ( bool ) createFramebuffer;
- ( void ) destroyFramebuffer;
- ( bool ) setupView;

@end

// Implementção da classe
@implementation EAGLView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize animationInterval, pAudioManager;

/*==============================================================================================

MENSAGEM layerClass
	Indica que classe utilizaremos para construir a layer de renderização da view. Sua implementação
é obrigatória para criarmos uma layer OpenGL.

==============================================================================================*/

+ ( Class ) layerClass
{
	return [CAEAGLLayer class];
}

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor.

==============================================================================================*/

- ( id ) initWithFrame:( CGRect )frame
{
	if( ( self = [super initWithFrame : frame] ) )
	{
		pAudioManager = new AudioManager();
		
		if( !pAudioManager )
			goto error;
		pAudioManager->suspend();
		
		// Cria os singletons que irão controlar os eventos de toque e do acelerômetro
		if( !EventManager::GetInstance() )
			goto error;
		
		AccManager* pAccManager = AccManager::GetInstance();
		if( !pAccManager )
			goto error;

		pAccManager->setUpdateInterval( 1.0f / 100.0f );

		// Determina o intervalo máximo de atualização de tela
		animationInterval = DEFAULT_ANIMATION_INTERVAL;
		
		// Get the layer
		CAEAGLLayer* pEaglLayer = ( CAEAGLLayer* )self.layer;
		
		pEaglLayer.opaque = YES;
		pEaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, NULL];
		
		hContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
		
		if( !hContext || ![EAGLContext setCurrentContext : hContext]|| ![self createFramebuffer] || ![self setupView] )
			goto error;
		
		// Indica que iremos suportar mais de um toque
		[self enableMultipleTouch: true];
	}
	return self;

	// Tratamento de erros durante a inicialização
	error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void ) dealloc
{
	[self stopAnimation];

	SAFE_DELETE( pAudioManager );
	SAFE_DELETE( pCurrScene );
	
	[self destroyFramebuffer];
	
	if( [EAGLContext currentContext] == hContext )
		[EAGLContext setCurrentContext : NULL];
	
	KILL( hContext );

	[super dealloc];
}

/*==============================================================================================

MENSAGEM setupView
	Cria a primeira cena do jogo.

==============================================================================================*/

- ( bool ) setupView
{
	if( !pCurrScene )
	{
#ifdef RENDER_TEST_SCENE
		pCurrScene = new TestScene();
#else
		pCurrScene = BobFullScene::CreateInstance( self );
#endif
		return pCurrScene != NULL;
	}
	return true;
}

/*==============================================================================================

MENSAGEM drawView
	Atualiza e renderiza a cena do jogo.

==============================================================================================*/

- ( void ) drawView
{
	if( !pCurrScene )
		return;

	// TASK : animationInterval pode não ser constante... Utilizar clock() para pegar o tempo
	// transcorrido real
	//float timeElapsed = clock() / CLOCKS_PER_SEC;
	
	// Atualiza os elementos da cena
	pCurrScene->update( animationInterval );
	
	// Renderiza a cena do jogo
	[self render];
}

/*==============================================================================================

MENSAGEM render
	Renderiza a cena do jogo.

==============================================================================================*/

- ( void )render
{
	// Renderiza a cena
	[EAGLContext setCurrentContext : hContext];

	glBindFramebufferOES( GL_FRAMEBUFFER_OES, viewFramebuffer );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	pCurrScene->render();

	glBindRenderbufferOES( GL_RENDERBUFFER_OES, viewRenderbuffer );
	[hContext presentRenderbuffer : GL_RENDERBUFFER_OES];
}

/*==============================================================================================

MENSAGEM layoutSubviews
	Indica que a view foi tornada ativa e que tela deve ser exibida.

==============================================================================================*/

- ( void ) layoutSubviews
{
	[EAGLContext setCurrentContext : hContext];

	[self destroyFramebuffer];

	if( ![self createFramebuffer] )
	{
		// Termina a aplicação
		#if TARGET_IPHONE_SIMULATOR
			[( iBobAppDelegate* )APP_DELEGATE quit: ERROR_NO_FRAME_BUFFER];
		#else
			[( iBobAppDelegate* )APP_DELEGATE quit];
		#endif
		return;
	}

	[self render];
}

/*==============================================================================================

MENSAGEM createFramebuffer
	Aloca os buffers necessários à renderização OpenGL.

==============================================================================================*/

- ( bool ) createFramebuffer
{
	glGenFramebuffersOES( 1, &viewFramebuffer );
	glGenRenderbuffersOES( 1, &viewRenderbuffer );

	glBindFramebufferOES( GL_FRAMEBUFFER_OES, viewFramebuffer );	
	glBindRenderbufferOES( GL_RENDERBUFFER_OES, viewRenderbuffer );

	[hContext renderbufferStorage : GL_RENDERBUFFER_OES fromDrawable : ( CAEAGLLayer* )self.layer];

	glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer );	
	glGetRenderbufferParameterivOES( GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth );
	glGetRenderbufferParameterivOES( GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight );

	// Aloca o buffer de profundidade
	if( USE_DEPTH_BUFFER )
	{
		glGenRenderbuffersOES( 1, &depthRenderbuffer );
		glBindRenderbufferOES( GL_RENDERBUFFER_OES, depthRenderbuffer );
		glRenderbufferStorageOES( GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight );
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer );
	}

	if( glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) != GL_FRAMEBUFFER_COMPLETE_OES )
	{
#if TARGET_IPHONE_SIMULATOR
		LOG( @"failed to make complete framebuffer object %x\n", glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) );
#endif
		return false;
	}
	return true;
}

/*==============================================================================================

MENSAGEM destroyFramebuffer
	Libera a memória utilizada pelos buffers do OpenGL.

==============================================================================================*/

- ( void ) destroyFramebuffer
{
	glDeleteFramebuffersOES( 1, &viewFramebuffer );
	viewFramebuffer = 0;

	glDeleteRenderbuffersOES( 1, &viewRenderbuffer );
	viewRenderbuffer = 0;
	
	if( depthRenderbuffer )
	{
		glDeleteRenderbuffersOES( 1, &depthRenderbuffer );
		depthRenderbuffer = 0;
	}
}

/*==============================================================================================

MENSAGEM startAnimation
	Inicia o loop de renderização.

==============================================================================================*/

- ( void ) startAnimation
{
	[self stopAnimation];
	hAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector(drawView) userInfo:NULL repeats:YES];
}

/*==============================================================================================

MENSAGEM stopAnimation
	Pára o loop de renderização.

==============================================================================================*/

- ( void ) stopAnimation
{
	if( hAnimationTimer )
	{
		[hAnimationTimer invalidate];
		
		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hAnimationTimer invalidate] chama
		// [hAnimationTimer release] automaticamente
		hAnimationTimer = NULL;
	}
}

/*==============================================================================================

MENSAGEM touchesCanceled
	Chamada quando um evento do sistema, como um aviso de pouca memória, cancela o evento.

==============================================================================================*/

- ( void ) touchesCancel
{
	#if TARGET_IPHONE_SIMULATOR
	@try
	{
	#endif
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_CANCELED, NULL );
	#if TARGET_IPHONE_SIMULATOR
	}
	@catch( NSException* hException )
	{
		LOG( @"touchesCancel" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM touchesEnded
	Chamada quando uma sequência de um toque termina.

==============================================================================================*/

- ( void )touchesEnded: ( NSSet* )touches withEvent: ( UIEvent* )event
{
	#if TARGET_IPHONE_SIMULATOR
	@try
	{
	#endif
		const NSArray* pTouchesArray = [touches allObjects];
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_ENDED, pTouchesArray );
	#if TARGET_IPHONE_SIMULATOR
	}
	@catch( NSException* hException )
	{
		LOG( @"touchesEnded" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM touchesMoved
	Chamada quando um toque já iniciado é movimentado.

==============================================================================================*/

- ( void )touchesMoved: ( NSSet* )touches withEvent: ( UIEvent* )event
{
	#if TARGET_IPHONE_SIMULATOR
	@try
	{
	#endif
		const NSArray* pTouchesArray = [touches allObjects];
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_MOVED, pTouchesArray );
	#if TARGET_IPHONE_SIMULATOR
	}
	@catch( NSException* hException )
	{
		LOG( @"touchesMoved" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM touchesBegan
	Chamada quando uma nova sequência de toque começa.

==============================================================================================*/

- ( void )touchesBegan: ( NSSet* )touches withEvent: ( UIEvent* )event
{
	#if TARGET_IPHONE_SIMULATOR
	@try
	{
	#endif
		const NSArray* pTouchesArray = [touches allObjects];
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_BEGAN, pTouchesArray );
	#if TARGET_IPHONE_SIMULATOR
	}
	@catch( NSException* hException )
	{
		LOG( @"touchesBegan" );
	}
	#endif
}


/*==============================================================================================

MENSAGEM setOpts
	Aplica as opções do menu no jogo.

==============================================================================================*/

#if BLOB_MODE
	#if TARGET_IPHONE_SIMULATOR
		- ( void ) setOpts: ( bool )reset blobColor:( Color* )pColor hardness:( float )hardness stickness:( float )stickness accelForceLevel:( float )accelForceLevel movCalcMode:( AccForceCalcMode )movCalcMode volume:( float )volume gravity:( float )gravity;
	#else
		- ( void ) setOpts: ( bool )reset blobColor:( Color* )pColor hardness:( float )hardness stickness:( float )stickness volume:( float )volume gravity:( float )gravity;
	#endif
#else
	#if TARGET_IPHONE_SIMULATOR
		- ( void ) setOpts: ( bool )reset hardness:( float )hardness stickness:( float )stickness accelForceLevel:( float )accelForceLevel movCalcMode:( AccForceCalcMode )movCalcMode volume:( float )volume gravity:( float )gravity;
	#else
		- ( void ) setOpts: ( bool )reset hardness:( float )hardness stickness:( float )stickness volume:( float )volume gravity:( float )gravity;
	#endif
#endif
{
#ifndef RENDER_TEST_SCENE

	// Modifica o volume da aplicação
	PlaybackAudioDevice* pDevice = pAudioManager->getPlaybackDevice();
	AudioListener* pListener = pDevice->getAudioListener();
	pListener->setGain( volume );
	
	// Indica se devemos colocar o personagem em sua posição inicial
	BobFullScene* pScene = ( BobFullScene* ) pCurrScene;
	if( reset )
		pScene->iBobReset();
	
	// Modifica a força da gravidade
	pScene->setGravity( gravity );

	// Modifica as propriedades das molas que constituem o iBob
	pScene->iBobSetHardness( hardness );
	
	// Modifica o coeficiente de aderência do iBob
	pScene->iBobSetStickness( stickness );
	
	#if BLOB_MODE
		// Determina a nova cor do personagem
		pScene->iBlobSetColor( pColor );
	#endif
	
	#if TARGET_IPHONE_SIMULATOR
		// Mofifica o nível das forças que podem ser geradas pelo acelerômetro
		AccForce* pForce = pScene->getAccForce();
		pForce->setAccelerometerBooster( accelForceLevel );

		// Modifica o tipo de cálculo utilizado para induzirmos a tendência de movimento
		// OLD pForce->setAccelerometerCalcMode( movCalcMode );
	#endif
#endif
}

/*==============================================================================================

MENSAGEM suspend
	Pára o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL.

==============================================================================================*/

- ( void ) suspend
{
	[self stopAnimation];
	AccManager::GetInstance()->suspend();
	pAudioManager->suspend();
}

/*==============================================================================================

MENSAGEM resume
	Reinicia o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL.

==============================================================================================*/

- ( void ) resume
{
	[self startAnimation];
	AccManager::GetInstance()->resume();
	pAudioManager->resume();
}

/*==============================================================================================

MENSAGEM enableMultipleTouch
	Indica se esta view deve receber múltiplos eventos de toque.

==============================================================================================*/

- ( void ) enableMultipleTouch: ( bool )s
{
	[self setMultipleTouchEnabled: s ? YES : NO];
}

/*==============================================================================================

MENSAGEM enableExclusiveTouch
	Indica que esta view não deve repassar os eventos de toque para outras views, se tornando
a única tratadora de eventos da aplicação.

==============================================================================================*/

- ( void ) enableExclusiveTouch: ( bool )s
{
	[self setExclusiveTouch: s ? YES : NO];
}

// TASK : Permitir que o usuário desta classe utilize estes métodos
//- beginGeneratingDeviceOrientationNotifications
//- endGeneratingDeviceOrientationNotifications


// Fim da implementção da classe
@end

