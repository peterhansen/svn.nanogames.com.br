#include "Polynomial.h"
#include "Matrix.h"
#include "Matrix3x3.h"
#include "Macros.h"
#include <math.h>

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Polynomial::Polynomial( uint8 degree, const float* pCoeficients ) : nTerms( degree + 1 ), pTerms( NULL )
{
	pTerms = new float[ nTerms ];
	if( !pTerms )
#if TARGET_IPHONE_SIMULATOR	// TODO : É necessário?! Será que o new já dispara a exceção?!?!?	
		throw OutOfMemoryException( "Não foi possível criar o polinômio" );
#else
		throw OutOfMemoryException();
#endif

	setTerms( pCoeficients );
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Polynomial::~Polynomial( void )
{
	SAFE_DELETE_VEC( pTerms );
}

/*==============================================================================================

MÉTODO build
	Determina os coeficientes dos termos do polinômio.

==============================================================================================*/

inline void Polynomial::setTerms( const float* pCoeficients )
{
	if( pCoeficients )
		memcpy( pTerms, pCoeficients, sizeof( float ) * nTerms );
}

/*==============================================================================================

MÉTODO evaluate
	Calcula qual será o o valor de y para o valor de x recebido.

==============================================================================================*/

float Polynomial::evaluate( float x )
{
	float y = 0.0f;
	for( uint16 i = 0 ; i < nTerms ; ++i )
		y += pTerms[i] * pow( x, i );
	return y;
}

/*===========================================================================================

MÉTODO leastSquares
	Regressão Polinomial Linear através do Método dos Mínimos Quadrados.
	Referências:
		- http://en.wikipedia.org/wiki/Least_squares
		- http://www.scottsarra.org/math/courses/na/nc/polyRegression.html
		- http://pessoal.sercomtel.com.br/matematica/superior/algebra/mmq/mmq.htm

============================================================================================*/

Polynomial* Polynomial::leastSquares( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p )
{
	switch( degree )
	{
		case 2:
			return optimizedLeastSquaresDegree2( pYs, firstIndex, nValues, degree, p );

		case 3:
			return optimizedLeastSquaresDegree3( pYs, firstIndex, nValues, degree, p );
			
		default:
			return nonOptimizedLeastSquares( pYs, firstIndex, nValues, degree, p );
	}
}

/*===========================================================================================

MÉTODO nonOptimizedLeastSquares
	Versão não-otimizada, mas genérica, do método para calcular as mínimas raízes quadradas.

============================================================================================*/

Polynomial* Polynomial::nonOptimizedLeastSquares( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p )
{
	// Calcula os somatórios ∑ yi, ∑ yixi, ∑ yixi², ... , ∑ yixiⁿ
	
	const uint16 nCoeficients = degree + 1;
	Matrix sumsYiXi( nCoeficients, 1, 0.0 );
	
	for( uint16 j = 0 ; j < nCoeficients ; ++j )
	{
		// OBS: 0ˆ0 == 1
		for( uint16 i = 0 ; i < nValues ; ++i )
			sumsYiXi( j, 0 ) += pYs[ ( firstIndex + i ) % nValues ] * pow( ( i + 1 ), j );
	}
	
	// Calcula os somatórios ∑ xi, ∑ xi², ∑ xi³, ... , ∑ xiⁿ

	const uint16 nSumsXi = degree << 1;
	float* pSumsXi = new float[ nSumsXi ];
	memset( pSumsXi, 0, sizeof( float ) * nSumsXi );

	for( uint16 j = 0 ; j < nSumsXi ; ++j )
	{
		for( uint16 i = 0 ; i < nValues ; ++i )
			pSumsXi[j] += pow( ( i + 1 ), j + 1 );
	}

	// Cria a matriz jacobiana. O primeiro elemento é sempre nValues
	Matrix jacobian( nCoeficients, nCoeficients );
	jacobian( 0, 0 ) = nValues;
	
	// Preenche o resto da matriz
	// |   m      ∑ xi     ∑ xi²    ...   ∑ xiⁿ   |
	// |  ∑ xi    ∑ xi²    ∑ xi³    ...   ∑ xiⁿ⁺¹ |
	// |  ∑ xi²   ∑ xi³    ∑ xi⁴    ...    ...    |
	// |   ...     ...      ...     ...    ...    |
	// |  ∑ xiⁿ   ∑ xiⁿ⁺¹  ∑ xiⁿ⁺²  ...   ∑ xi²ⁿ  |
	uint8 columnStart = 1;
	for( int32 row = 0 ; row < nCoeficients ; ++row )
	{
		for( int32 column = columnStart ; column < nCoeficients ; ++column )
			jacobian( row, column ) = pSumsXi[ column + ( row - 1 ) ];

		columnStart = 0;
	}

	DESTROY_VEC( pSumsXi );

	// Calcula a matriz jacobiana inversa
	jacobian.inverse();
	
	// Faz a multiplicação de matrizes para achar os coeficientes da resposta
	// J * β = Y
	// J⁻¹ * J * β = J⁻¹ * Y
	// I * β = J⁻¹ * Y
	// β = J⁻¹ * Y
	const Matrix res = jacobian * sumsYiXi;
	p->setTerms( res.getElements() );
	return p;
}

/*===========================================================================================

MÉTODO optimizedLeastSquaresDegree2
	Utiliza a classe Matrix3x3 para otimizar o cálculo das mínimas raízes quadradas.

============================================================================================*/

Polynomial* Polynomial::optimizedLeastSquaresDegree2( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p )
{
#define N_COEFICIENTS 3

	// Calcula os somatórios ∑ yi, ∑ yixi, ∑ yixi², ... , ∑ yixiⁿ
	float sumsYiXi[ N_COEFICIENTS ] = { 0.0f, 0.0f, 0.0f };
	
	for( uint8 j = 0 ; j < N_COEFICIENTS ; ++j )
	{
		// OBS: 0ˆ0 == 1
		for( uint8 i = 0 ; i < nValues ; ++i )
			sumsYiXi[ j ] += pYs[ ( firstIndex + i ) % nValues ] * pow( ( i + 1 ), j );
	}
	
	// Calcula os somatórios ∑ xi, ∑ xi², ∑ xi³, ... , ∑ xiⁿ
#define N_SUMS_XI 4

	float sumsXi[ N_SUMS_XI ] = { 0.0f, 0.0f, 0.0f, 0.0f };

	for( uint8 j = 0 ; j < N_SUMS_XI ; ++j )
	{
		for( uint8 i = 0 ; i < nValues ; ++i )
			sumsXi[j] += pow( ( i + 1 ), j + 1 );
	}

#undef N_SUMS_XI

	// Cria a matriz jacobiana. O primeiro elemento é sempre nValues
	Matrix3x3 jacobian;
	jacobian( 0, 0 ) = nValues;
	
	// Preenche o resto da matriz
	// |   m      ∑ xi     ∑ xi²    ...   ∑ xiⁿ   |
	// |  ∑ xi    ∑ xi²    ∑ xi³    ...   ∑ xiⁿ⁺¹ |
	// |  ∑ xi²   ∑ xi³    ∑ xi⁴    ...    ...    |
	// |   ...     ...      ...     ...    ...    |
	// |  ∑ xiⁿ   ∑ xiⁿ⁺¹  ∑ xiⁿ⁺²  ...   ∑ xi²ⁿ  |
	uint8 columnStart = 1;
	for( uint8 row = 0 ; row < N_COEFICIENTS ; ++row )
	{
		for( uint8 column = columnStart ; column < N_COEFICIENTS ; ++column )
			jacobian( row, column ) = sumsXi[ column + ( row - 1 ) ];

		columnStart = 0;
	}

	// Calcula a matriz jacobiana inversa
	if( jacobian.inverse() )
	{	
		// Faz a multiplicação de matrizes para achar os coeficientes da resposta
		// J * β = Y
		// J⁻¹ * J * β = J⁻¹ * Y
		// I * β = J⁻¹ * Y
		// β = J⁻¹ * Y
		const float res[ N_COEFICIENTS ] =	{
												( jacobian._00 * sumsYiXi[0] ) + ( jacobian._01 * sumsYiXi[1] ) + ( jacobian._02 * sumsYiXi[2] ),
												( jacobian._10 * sumsYiXi[0] ) + ( jacobian._11 * sumsYiXi[1] ) + ( jacobian._12 * sumsYiXi[2] ),
												( jacobian._20 * sumsYiXi[0] ) + ( jacobian._21 * sumsYiXi[1] ) + ( jacobian._22 * sumsYiXi[2] )
											};
		p->setTerms( res );
	}
	else
	{
		const float res[ N_COEFICIENTS ] = { 0.0f, 0.0f, 0.0f };
		p->setTerms( res );
	}
	return p;
	
#undef N_COEFICIENTS
}
/*===========================================================================================

MÉTODO optimizedLeastSquaresDegree3
	Utiliza a classe Matrix4x4 para otimizar o cálculo das mínimas raízes quadradas.

============================================================================================*/

Polynomial* Polynomial::optimizedLeastSquaresDegree3( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p )
{
#define N_COEFICIENTS 4

	// Calcula os somatórios ∑ yi, ∑ yixi, ∑ yixi², ... , ∑ yixiⁿ
	float sumsYiXi[ N_COEFICIENTS ] = { 0.0f, 0.0f, 0.0f, 0.0f };

	// TASK : Melhora?!?!?!?
//	for( uint8 i = 0 ; i < nValues ; ++i )
//		sumsYiXi[ 0 ] += pYs[ ( firstIndex + i ) % nValues ];
//		
//	for( uint8 i = 0 ; i < nValues ; ++i )
//		sumsYiXi[ 1 ] += pYs[ ( firstIndex + i ) % nValues ] * ( i + 1 );
//
//	for( uint8 j = 2 ; j < N_COEFICIENTS ; ++j )
//	{
//		// OBS: 0ˆ0 == 1
//		for( uint8 i = 0 ; i < nValues ; ++i )
//			sumsYiXi[ j ] += pYs[ ( firstIndex + i ) % nValues ] * pow( ( i + 1 ), j );
//	}
	
	for( uint8 j = 0 ; j < N_COEFICIENTS ; ++j )
	{
		// OBS: 0ˆ0 == 1
		for( uint8 i = 0 ; i < nValues ; ++i )
			sumsYiXi[ j ] += pYs[ ( firstIndex + i ) % nValues ] * pow( ( i + 1 ), j );
	}
	
	// Calcula os somatórios ∑ xi, ∑ xi², ∑ xi³, ... , ∑ xiⁿ
#define N_SUMS_XI 6

	float sumsXi[ N_SUMS_XI ] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

	// TASK : Melhora?!?!?!?
//	for( uint8 i = 0 ; i < nValues ; ++i )
//		sumsXi[ 0 ] += ( i + 1 );
//
//	for( uint8 j = 1 ; j < N_SUMS_XI ; ++j )
//	{
//		for( uint8 i = 0 ; i < nValues ; ++i )
//			sumsXi[j] += pow( ( i + 1 ), j + 1 );
//	}
	
	for( uint8 j = 0 ; j < N_SUMS_XI ; ++j )
	{
		for( uint8 i = 0 ; i < nValues ; ++i )
			sumsXi[j] += pow( ( i + 1 ), j + 1 );
	}

#undef N_SUMS_XI

	// Cria a matriz jacobiana. O primeiro elemento é sempre nValues:
	// |   m      ∑ xi     ∑ xi²    ...   ∑ xiⁿ   |
	// |  ∑ xi    ∑ xi²    ∑ xi³    ...   ∑ xiⁿ⁺¹ |
	// |  ∑ xi²   ∑ xi³    ∑ xi⁴    ...    ...    |
	// |   ...     ...      ...     ...    ...    |
	// |  ∑ xiⁿ   ∑ xiⁿ⁺¹  ∑ xiⁿ⁺²  ...   ∑ xi²ⁿ  |
	Matrix4x4 jacobian(     nValues, sumsXi[ 0 ], sumsXi[ 1 ], sumsXi[ 2 ],
						sumsXi[ 0 ], sumsXi[ 1 ], sumsXi[ 2 ], sumsXi[ 3 ],
						sumsXi[ 1 ], sumsXi[ 2 ], sumsXi[ 3 ], sumsXi[ 4 ],
						sumsXi[ 2 ], sumsXi[ 3 ], sumsXi[ 4 ], sumsXi[ 5 ] );

	// Calcula a matriz jacobiana inversa
	if( jacobian.inverse() )
	{	
		// Faz a multiplicação de matrizes para achar os coeficientes da resposta
		// J * β = Y
		// J⁻¹ * J * β = J⁻¹ * Y
		// I * β = J⁻¹ * Y
		// β = J⁻¹ * Y
		const float res[ N_COEFICIENTS ] =	{
												( jacobian._00 * sumsYiXi[0] ) + ( jacobian._01 * sumsYiXi[1] ) + ( jacobian._02 * sumsYiXi[2] ) + ( jacobian._03 * sumsYiXi[3] ),
												( jacobian._10 * sumsYiXi[0] ) + ( jacobian._11 * sumsYiXi[1] ) + ( jacobian._12 * sumsYiXi[2] ) + ( jacobian._13 * sumsYiXi[3] ),
												( jacobian._20 * sumsYiXi[0] ) + ( jacobian._21 * sumsYiXi[1] ) + ( jacobian._22 * sumsYiXi[2] ) + ( jacobian._23 * sumsYiXi[3] ),
												( jacobian._30 * sumsYiXi[0] ) + ( jacobian._31 * sumsYiXi[1] ) + ( jacobian._32 * sumsYiXi[2] ) + ( jacobian._33 * sumsYiXi[3] )
											};
		p->setTerms( res );
	}
	else
	{
		const float res[ N_COEFICIENTS ] = { 0.0f, 0.0f, 0.0f, 0.0f };
		p->setTerms( res );
	}
	return p;
	
#undef N_COEFICIENTS
}
