/*
 *  ShapePoint.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/1/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef SHAPE_POINT_H
#define SHAPE_POINT_H

#include "Shape.h"

class ShapePoint : public Shape
{
	public:
		// Construtor
		ShapePoint( const Color *color = NULL, float size = DEFAULT_SIZE );
	
		// Destrutor
		virtual ~ShapePoint( void ){};

		// Renderiza o objeto
		virtual void render( void );

		// Obtém uma cópia do objeto
		virtual Shape* getCopy( void );
};

#endif
