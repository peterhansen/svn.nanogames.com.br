#ifndef POINT3F
#define POINT3F

/*==============================================================================================

 CLASSE Point3f
 
==============================================================================================*/

// TASK : Fazer uma classe Point que contenha:
// - texCoord
// - normal
// - color
// Classe base ter byte descritor dos campos que são implementados. Este campo será preenchido pelas subclasses. Essa idéia vem de DirectX
class Point3f
{
	public:
		// Assim podemos acessar os dados do ponto através de seus valores individuais ou como
		// um array
		union
		{
			struct
			{
				float x, y, z;
			};
			float p[3];
		};
	
		// XCode ainda não suporta o padrão N2210
		// http://www.google.com.br/search?hl=pt-BR&client=firefox-a&rls=org.mozilla%3Apt-BR%3Aofficial&q=C%2B%2B+n2210+&btnG=Pesquisar&meta=
		// Ressucita o construtor padrão
		// Point3f() = default;

		// Construtor
		Point3f( float x = 0.0f, float y = 0.0f, float z = 0.0f );

		// Inicializa o objeto
		inline void set( float x = 0.0f, float y = 0.0f, float z = 0.0f ){ this->x = x; this->y = y; this->z = z; }

		// Retorna o módulo do vetor descrito pelo ponto
		float getModule( void ) const;

		// Retorna normalizado o vetor descrito pelo ponto
		void normalize( void );
	
		// Operadores de conversão ( casting )
		operator float* ();
		operator const float* () const;
	
		// Operadores unários
		Point3f operator + () const;
		Point3f operator - () const;

		// Operadores binários
		Point3f operator + ( const Point3f& point ) const;
		Point3f operator - ( const Point3f& point ) const;
		Point3f operator * ( float f ) const;
		Point3f operator / ( float f ) const;
		Point3f operator % ( const Point3f& point ) const; // Produto Vetorial - Cross Product
		float	operator * ( const Point3f& point ) const; // Produto Interno - Dot Product
	
		// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
		friend Point3f operator * ( float f, const Point3f& point );

		// Operadores de atribuição
		Point3f& operator += ( const Point3f& point );
		Point3f& operator -= ( const Point3f& point );
		Point3f& operator *= ( float f );
		Point3f& operator /= ( float f );

		// Operadores lógicos
		bool operator == ( const Point3f& point ) const;
		bool operator == ( float f ) const;
	
		bool operator != ( const Point3f& point ) const;
		bool operator != ( float f ) const;
};

#endif
