#include "Particle.h"
#include "Force.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Particle::Particle( const Point3f* pPosition, const Point3f* pSpeed, float life, float size, float mass, float angle, bool active, const Point3f* pAxis, Shape* pShape )
		: Node( pPosition, pSpeed ), life( life ), size( size ), mass( mass ), angle( angle ), active( active ), visible( true ),
		  immortal( false ), immune( false ), pShape( pShape ), pLimitPlanes( NULL )
{
	setAxis( pAxis );
	force.set();
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

Particle::~Particle( void )
{
	SAFE_DELETE( pShape );
	SAFE_DELETE( pLimitPlanes );
}

/*===========================================================================================

MÉTODO update

============================================================================================*/

bool Particle::update( double timeElapsed, const DynamicVector* pForces, Plane* pCollisionPlane )
{
	if( active )
	{
		if( !immortal )
		{
			life -= timeElapsed;
			Plane* p = isOutsidePlanes();
			
			if( pCollisionPlane )
				*pCollisionPlane = *p;
				
			if( ( life < 0.0f ) || p )
			{
				// Partícula morreu
				active = false;
				visible = false;

				return false;
			}
		}

		position += speed * timeElapsed;

		// Se partícula não é imune a forças, sofre a ação das forças recebidas como parâmetro
		if( !immune && pForces )
		{
			// Percorre o vetor de forças
			const float aux = timeElapsed * timeElapsed;
			const uint16 length = pForces->getUsedLength();
			for( uint16 i = 0; i < length ; ++i )
			{ 
				const Force* f = ( Force* )pForces->getData(i);
				if( f && f->isActive() )
					speed += *( f->getDirection() ) * aux;
			}
		}

		const Point3f zero;
		if( speed != zero )
			angleAxis = speed / speed.getModule();

		return true;
	}
	return false;
}

/*===========================================================================================

MÉTODO render

============================================================================================*/

void Particle::render( void )
{
	if( visible && pShape )
	{
		loadTransform();

		glPushMatrix();
		glTranslatef( position.x, position.y, position.z );

		if( FDIF( angle, 0.0f ) )
			glRotatef( angle, angleAxis.x, angleAxis.y, angleAxis.z );

		glScalef( size, size, size );

		// Armazena o tamanho atual de GL_POINT
		int32 currPointSize;
		glGetIntegerv( GL_POINT_SIZE, &currPointSize );
		
		// Verifica se devemos habilitar a transparência
		const bool blendIsEnabled = glIsEnabled( GL_BLEND );
		const bool hasTransparency = FLTN( pShape->getColor()->a, 1.0f );
		if( hasTransparency )
		{
			if( !blendIsEnabled )
				glEnable( GL_BLEND );
		}
		else
		{
			glDisable( GL_BLEND );
		}
		
		pShape->render();
		
		// Restaura o estado de GL_BLEND
		if( blendIsEnabled )
			glEnable( GL_BLEND );
		else
			glDisable( GL_BLEND );

		// Restaura o tamanho de GL_POINT
		glPointSize( currPointSize );

		glPopMatrix();

		unloadTransform();
	}
}

/*===========================================================================================

MÉTODO setActive

============================================================================================*/

void Particle::setActive( bool active )
{
	this->active = active;
}

/*===========================================================================================

MÉTODO setShape

============================================================================================*/

void Particle::setShape( Shape* pShape )
{
	this->pShape = pShape;
}

/*===========================================================================================

MÉTODO setBasedOn

============================================================================================*/

bool Particle::setBasedOn( Particle* const pBaseElement )
{
	if( !pBaseElement )
		return false;

	setLife( Random::NextFloat( pBaseElement->getLife() ) );
	mass = Random::NextFloat( 1.0f, pBaseElement->getMass() );
	size = Random::NextFloat( 1.0f, pBaseElement->getSize() );

	position = *( pBaseElement->getPosition() );

	const Point3f* pBaseSpeed = pBaseElement->getSpeed();
	speed = Point3f( Random::NextFloat( -pBaseSpeed->x, pBaseSpeed->x ), Random::NextFloat( pBaseSpeed->y ), Random::NextFloat( -pBaseSpeed->z, pBaseSpeed->z ) );

	active = true;
	visible = true;

	return true;
}

/*===========================================================================================

MÉTODO setLife

============================================================================================*/

void Particle::setLife( float newLife )
{
	life = newLife;
	immortal = FLTN( newLife, 0.0f );
}

/*===========================================================================================

MÉTODO setSize

============================================================================================*/

void Particle::setSize( float newSize )
{
	size = newSize;
}

/*===========================================================================================

MÉTODO setMass

============================================================================================*/

void Particle::setMass( float newMass )
{
	mass = newMass;
}

/*===========================================================================================

MÉTODO setAngle

============================================================================================*/

void Particle::setAngle( float angle )
{
	this->angle = angle;
}

/*===========================================================================================

MÉTODO setAxis

============================================================================================*/

void Particle::setAxis( const Point3f *pAxis )
{
	 if( pAxis )
		 angleAxis = *pAxis;
	 else
		 angleAxis = Point3f( 1.0 );
}

/*===========================================================================================

MÉTODO setImmune

============================================================================================*/

void Particle::setImmune ( bool myImmune )
{
	immune = myImmune;
}

/*===========================================================================================

MÉTODO setImmortal

============================================================================================*/

void Particle::setImmortal ( bool myImmortal )
{
	immortal = myImmortal;
}

/*===========================================================================================

MÉTODO getCopy

============================================================================================*/

Particle* Particle::getCopy( void )
{
	Particle* p = new Particle( &position, &speed, life, size, mass, angle, active, &angleAxis );
	if( p )
	{
		if( pShape )
		{
			// TASK : Por enquanto vai funcionar, já que os emitters não deletam as partículas
			// depois que essas morrem...
//			Shape* s = pShape->getCopy();
//			if( !s )
//			{
//				DESTROY( p );
//				return NULL;
//			}
			// Mas o certo é fazer assim !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//pShape->increaseRefCount();
			p->setShape( pShape );
		}
		p->setImmune( immune );
		p->setLimitPlanes( pLimitPlanes );
	}
	return p;
}

/*===========================================================================================

MÉTODO setVisible

============================================================================================*/

void Particle::setVisible( bool isVisible )
{
	visible = isVisible;
}

/*===========================================================================================

MÉTODO getLimitPlaneAt

============================================================================================*/

const Plane* Particle::getLimitPlaneAt( uint16 index )
{
	if( pLimitPlanes )
		return ( Plane* ) pLimitPlanes->getData( index );
	return NULL;
}

/*===========================================================================================

MÉTODO insertLimitPlane

============================================================================================*/

int16 Particle::insertLimitPlane( const Plane* pLimitPlane )
{
	if( !pLimitPlanes )
	{
		pLimitPlanes = new DynamicVector();
		if( !pLimitPlanes )
			return -1;
	}

	Plane* pAux = new Plane();
	if( pAux )
	{
		*pAux = *pLimitPlane;
		return pLimitPlanes->insert( pAux );
	}
	return -1;
}

/*===========================================================================================

MÉTODO removeLimitPlaneAt

============================================================================================*/

bool Particle::removeLimitPlaneAt( uint16 index )
{
	if( pLimitPlanes )
		return pLimitPlanes->remove( index );
	return false;
}

/*===========================================================================================

MÉTODO setLimitPlanes

============================================================================================*/

void Particle::setLimitPlanes( DynamicVector* pLimitPlanes )
{
	this->pLimitPlanes = pLimitPlanes;
}

/*===========================================================================================

MÉTODO isOutsidePlanes

============================================================================================*/

Plane* Particle::isOutsidePlanes( void )
{
	if( pLimitPlanes )
	{
		const uint16 length = pLimitPlanes->getUsedLength();

		for( uint16 i = 0; i < length; ++i )
		{
			Plane* p = ( Plane* ) pLimitPlanes->getData( i );
			if( p && (( position * p->normal ) < p->d ) )
				return p;
		}
	}
	return NULL;
}

