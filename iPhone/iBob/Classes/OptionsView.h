//
//  OptionsView.h
//  iBob
//
//  Created by Daniel Lopes Alves on 12/11/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef OPTIONS_VIEW_H
#define OPTIONS_VIEW_H

#import <UIKit/UIKit.h>

#if TARGET_IPHONE_SIMULATOR
	#include "AccForce.h"
#endif

@interface OptionsView : UIView
{
	@private
		// Barra para a configuração do volume da aplicação
		IBOutlet UISlider* hSliderSound;
	
		// Barra para a configuração da consistência do iBlob
		IBOutlet UISlider* hSliderHardness;
	
		// Barra para a configuração da aderência do iBlob
		IBOutlet UISlider* hSliderStickness;
	
		// Barra para a configuração da força da gravidade
		IBOutlet UISlider* hSliderGravity;
	
		// Botão para reinicializar o estado do personagem
		IBOutlet UIButton* hResetButton;
	
		// Indica se devemos reinicializar o estado do personagem quando voltarmos para a tela de jogo
		bool reset;
}

// Getter e setter do atributo reset
@property ( nonatomic, assign, setter=setReset, getter= mustReset) bool reset;

#if TARGET_IPHONE_SIMULATOR
	// Retorna o multiplicador que devemos utilizar sobre as forças geradas pelos acelerômetros
	- ( float )getForceLevel;

	// Retorna o modo de cálculo a ser utilizado na interpretação dos dados dos acelerômetros
	- ( AccForceCalcMode )getMovementCalcMode;
#endif

// Retorna o coeficiente de consistência a ser usado pelo personagem
- ( float )getHardness;

// Retorna o coeficiente de aderência a ser usado pelo personagem
- ( float )getStickness;

// Retorna o volume do som
- ( float )getSoundVolume;

// Retorna a força da gravidade
- ( float )getGravity;

// Evento recebido quando o jogador deseja reinicializar o estado do personagem
- ( IBAction )onReset;

@end

#endif