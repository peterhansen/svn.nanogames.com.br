#include "BobFullScene.h"
#include "EAGLView.h"
#include "EventManager.h"
#include "Emitter.h"
#include "PerspectiveCamera.h"
#include "Texture2D.h"
#include "Floor.h"
#include "iBobAppDelegate.h"
#import <UIKit/UIKit.h>

#if BLOB_MODE
	#include "ShapePoint.h"
	#include "Sphere.h"

	// Configurações dos emissores de partículas
	#define N_BLOB_EMITTERS 10
	#define MIN_BLOB_PARTICLES 1.0f
	#define MAX_BLOB_PARTICLES 12.0f
	#define MIN_BLOB_SPEED 200.0f
	#define MAX_BLOB_SPEED 1200.0f
#endif

// Índices dos loadables da cena
#define LOADABLE_MAIN_SCENE	0
#define LOADABLE_TEX_GROUND	1
#define LOADABLE_TEX_WALL	2
#define LOADABLE_TEX_BOB	3

// Distância da câmera para o mundo
#define CAMERA_DIST_FROM_WORLD 250.0f

// Tamanho do mundo da cena
#define WORLD_DIMENSION 500.0f

#define PLANE_COLLISION_OFFSET 10.0f
#define COLLISION_PLANES_D -(( WORLD_DIMENSION / 2.0f ) - PLANE_COLLISION_OFFSET )

#define DEFAULT_TIGHTNESS 850
#define DEFAULT_DAMPING 32 // OLD 25 era o original ...

#define CHARACTER_SIZE 200.0f
#define CHARACTER_SPONGE_N_POINTS_BY_EDGE 4

#define MAX_TOUCH_FORCE 10000.0f

// Índice do ambiente da cena
#define SCENE_ENVIRONMENT_INDEX 0

// Índice do ambiente das partículas
#define PARTICLES_ENVIRONMENT_INDEX 1

// Índice da força da gravidade no ambiente da cena
#define SCENE_GRAVITY_FORCE_INDEX 1

// Índice da força da gravidade no ambiente das partículas
#define PARTICLES_GRAVITY_FORCE_INDEX 0

// Índices das texturas a serem aplicadas ao background no vetor auxiliar
#define N_BKG_SPRITES 5
#define BKG_SPRITE_LEFT_INDEX 0
#define BKG_SPRITE_RIGHT_INDEX 1
#define BKG_SPRITE_FLOOR_INDEX 2
#define BKG_SPRITE_CEILING_INDEX 3
#define BKG_SPRITE_REAR_INDEX 4

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

BobFullScene::BobFullScene( EAGLView* hParentView )
			 :Scene( NULL, LOADABLE_MAIN_SCENE ),
			  backToMenuBtReductionCounter( 0.0f ), pIBob( NULL ), hParentView( hParentView ),
			  pBackToMenuButton( NULL )
#if BLOB_MODE
, pBlobEmitters( NULL )
#endif
{
	memset( touchForces, 0, sizeof( TouchForce* ) * MAX_TOUCHES );
	setListener( this );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

BobFullScene::~BobFullScene( void )
{
	// Cancela alguns ponteiros
	pIBob = NULL;
	memset( touchForces, 0, sizeof( TouchForce* ) * MAX_TOUCHES );
	
	// Deleta os componentes que estão fora da estrutura de Scene
	SAFE_DELETE( pBackToMenuButton );

	#if BLOB_MODE
		SAFE_DELETE_VEC( pBlobEmitters );
	#endif
}

/*==============================================================================================

MÉTODO CreateInstance

==============================================================================================*/

BobFullScene* BobFullScene::CreateInstance( EAGLView* hParentView )
{
	BobFullScene *pObj = new BobFullScene( hParentView );
	if( pObj )
	{
		if( !pObj->build() )
			DESTROY( pObj );
	}
	return pObj;
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool BobFullScene::build( void )
{
	if( !createBackToMenuButton() )
		return false;

	// Aloca a câmera da cena	
	PerspectiveCamera *pCamera = new PerspectiveCamera( 90.0f, ( float )( SCREEN_WIDTH / SCREEN_HEIGHT ), 1.0f, WORLD_DIMENSION + CAMERA_DIST_FROM_WORLD, CAMERA_TYPE_AIR );
	if( !pCamera )
		return false;

	setCurrentCamera( pCamera );
	
	const Point3f cameraPos( 0.0f, 0.0f, ( WORLD_DIMENSION * 0.5f ) + CAMERA_DIST_FROM_WORLD );
	pCamera->setPosition( &cameraPos );
	
	// Cria o mundo para conter os ambientes físicos
	World *pWorld = new World();
	if( !pWorld || ( insertWorld( pWorld ) < 0 ) )
	{
		SAFE_DELETE( pWorld );
		return false;
	}
	
	// Cria o ambiente físico da cena
	Environment* pEnvironmentPlayer = new Environment();
	if( !pEnvironmentPlayer || ( pWorld->insertEnvironment( pEnvironmentPlayer ) < 0 ) )
	{
		SAFE_DELETE( pEnvironmentPlayer );
		return false;
	}

	// Cria uma força que irá corresponder ao acelerômetro
	AccForce *pAccForce = new AccForce( DEFAULT_ACCELEROMETER_CALC_MODE, DEFAULT_ACC_BOOSTER );
	if( !pAccForce || ( pEnvironmentPlayer->insertForce( pAccForce ) < 0 ) )
	{
		SAFE_DELETE( pAccForce );
		return false;
	}

	AccForce *pGravityForce = new AccForce( ACC_FORCE_MODE_GRAVITY_VEC, DEFAULT_GRAVITY_BOOSTER );
	if( !pGravityForce || ( pEnvironmentPlayer->insertForce( pGravityForce ) < 0 ) )
	{
		SAFE_DELETE( pGravityForce );
		return false;
	}
	
	// Cria as forças de toque
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		touchForces[i] = new TouchForce();
		if( !touchForces[i] || ( pEnvironmentPlayer->insertForce( touchForces[i] ) < 0 ) )
		{
			SAFE_DELETE( touchForces[i] );
			return false;
		}
		touchForces[i]->setActive( false );
	}
	
	// Cria as paredes do mundo
	Emitter* pTopEmitter = new Emitter();
	if( !pTopEmitter || ( pEnvironmentPlayer->insertParticle( pTopEmitter ) < 0 ) )
	{
		SAFE_DELETE( pTopEmitter );
		return false;
	}
#if TARGET_IPHONE_SIMULATOR
	pTopEmitter->setParticleName( "Top Emitter" );
#endif
	pTopEmitter->setSize( WORLD_DIMENSION );
	pTopEmitter->setImmune( true );
	pTopEmitter->setAngle( 90.0f );
	Point3f pos = Point3f( 0.0f, WORLD_DIMENSION / 2, 0.0f );
	pTopEmitter->setPosition( &pos );
	
	Emitter* pRightEmitter = new Emitter();
	if( !pRightEmitter || ( pEnvironmentPlayer->insertParticle( pRightEmitter ) < 0 ) )
	{
		SAFE_DELETE( pRightEmitter );
		return false;
	}
#if TARGET_IPHONE_SIMULATOR
	pRightEmitter->setParticleName( "Right Emitter" );
#endif
	pRightEmitter->setSize( WORLD_DIMENSION );
	pRightEmitter->setImmune( true );
	Point3f axis = Point3f( 0.0f, 1.0f, 0.0f );
	pRightEmitter->setAxis( &axis );
	pRightEmitter->setAngle( 90.0f );
	pos = Point3f( WORLD_DIMENSION / 2.0f, 0.0f, 0.0f );
	pRightEmitter->setPosition( &pos );

	Emitter* pLeftEmitter = new Emitter();
	if( !pLeftEmitter || ( pEnvironmentPlayer->insertParticle( pLeftEmitter ) < 0 ) )
	{
		SAFE_DELETE( pLeftEmitter );
		return false;
	}
#if TARGET_IPHONE_SIMULATOR
	pLeftEmitter->setParticleName( "Left Emitter" );
#endif
	pLeftEmitter->setSize( WORLD_DIMENSION );
	pLeftEmitter->setImmune( true );
	axis = Point3f( 0.0f, 1.0f, 0.0f );
	pLeftEmitter->setAxis( &axis );
	pLeftEmitter->setAngle( 90.0f );
	pos = Point3f( -WORLD_DIMENSION / 2.0f, 0.0f, 0.0f );
	pLeftEmitter->setPosition( &pos );
	
	Emitter* pRearEmitter = new Emitter();
	if( !pRearEmitter || ( pEnvironmentPlayer->insertParticle( pRearEmitter ) < 0 ) )
	{
		SAFE_DELETE( pRearEmitter );
		return false;
	}
#if TARGET_IPHONE_SIMULATOR
	pRearEmitter->setParticleName( "Rear Emitter" );
#endif
	pRearEmitter->setSize( WORLD_DIMENSION );
	pRearEmitter->setImmune( true );
	pos = Point3f( 0.0f, 0.0f, -WORLD_DIMENSION / 2 );
	pRearEmitter->setPosition( &pos );

	// Chão
	Emitter* pGroundEmitter = new Emitter();
	if( !pGroundEmitter || ( pEnvironmentPlayer->insertParticle( pGroundEmitter ) < 0 ) )
	{
		SAFE_DELETE( pGroundEmitter );
		return false;
	}
#if TARGET_IPHONE_SIMULATOR
	pGroundEmitter->setParticleName( "Ground Emitter" );
#endif
	pGroundEmitter->setSize( WORLD_DIMENSION );
	pGroundEmitter->setImmune( true );
	pGroundEmitter->setAngle( 90.0f );
	pos = Point3f( 0.0f, -WORLD_DIMENSION / 2, 0.0f );
	pGroundEmitter->setPosition( &pos );
	
	Sprite* bkgSprites[ N_BKG_SPRITES ];
	if( !getBkgSprites( bkgSprites ) )
		return false;
	
	pLeftEmitter->setShape( bkgSprites[ BKG_SPRITE_LEFT_INDEX ] );
	pRightEmitter->setShape( bkgSprites[ BKG_SPRITE_RIGHT_INDEX ] );
	pGroundEmitter->setShape( bkgSprites[ BKG_SPRITE_FLOOR_INDEX ] );
	pTopEmitter->setShape( bkgSprites[ BKG_SPRITE_CEILING_INDEX ] );
	pRearEmitter->setShape( bkgSprites[ BKG_SPRITE_REAR_INDEX ] );

	// Planos limite das partículas
	Plane collisionPlanes[ N_COLLISION_PLANES ];

	Point3f planeUp = Point3f( 0.0f, 0.0f, -1.0f );
	Point3f planeRight = Point3f( 1.0f, 0.0f, 0.0f );
	collisionPlanes[0].set( &planeUp, &planeRight, COLLISION_PLANES_D );
	
	planeUp = Point3f( 0.0f, 0.0f, 1.0f );
	planeRight = Point3f( 1.0f, 0.0f, 0.0f );
	collisionPlanes[1].set( &planeUp, &planeRight, COLLISION_PLANES_D );

	planeUp = Point3f( 0.0f, 0.0f, 1.0f );
	planeRight = Point3f( 0.0f, 1.0f, 0.0f );
	collisionPlanes[2].set( &planeUp, &planeRight, COLLISION_PLANES_D );

	planeUp = Point3f( 0.0f, 0.0f, -1.0f );
	planeRight = Point3f( 0.0f, 1.0f, 0.0f );
	collisionPlanes[3].set( &planeUp, &planeRight, COLLISION_PLANES_D );

	planeUp = Point3f( 1.0f, 0.0f, 0.0f );
	planeRight = Point3f( 0.0f, 1.0f, 0.0f );
	collisionPlanes[4].set( &planeUp, &planeRight, COLLISION_PLANES_D );

	planeUp = Point3f( -1.0f, 0.0f, 0.0f );
	planeRight = Point3f( 0.0f, 1.0f, 0.0f );
	collisionPlanes[5].set( &planeUp, &planeRight, COLLISION_PLANES_D );

	// Aloca o Bob Esponja
	const Point3f cubePosition = Point3f( 0.0f, CHARACTER_SIZE );
	pIBob = new iBob( &cubePosition );
	if( !pIBob
#if DRAG_N_DROP_USES_FORCES
		|| !pIBob->build( NULL, CHARACTER_SIZE, CHARACTER_SPONGE_N_POINTS_BY_EDGE, DEFAULT_TIGHTNESS, DEFAULT_DAMPING, 0.0f )
#else
	    || !pIBob->build( CHARACTER_SIZE, CHARACTER_SPONGE_N_POINTS_BY_EDGE, DEFAULT_TIGHTNESS, DEFAULT_DAMPING, 0.0f )
#endif
	    || ( pEnvironmentPlayer->insertParticle( pIBob ) < 0 ) )
	{
		SAFE_DELETE( pIBob );
		return false;
	}
	
	for( uint8 i = 0 ; i < N_COLLISION_PLANES ; ++i )
	{
		if( pIBob->insertLimitPlane( &collisionPlanes[i] ) < 0 )
			return false;
	}
	
#if BLOB_MODE
	// Cria os emissores que serão utilizados quando o cubo colidir com os planos do mundo
	if( !createBlobEmitters( pWorld, collisionPlanes ) )
		return false;
	pIBob->setCollisionListener( this );
#else
	// Determina a textura do Bob
	Texture2D *pBobTexture = new Texture2D( NULL, LOADABLE_TEX_BOB );
	if( !pBobTexture || !pBobTexture->loadTextureFile( @"bob.png" ) )
	{
		SAFE_DELETE( pBobTexture );
		return false;
	}
	
	Sprite* pBobSprite = Sprite::CreateInstance( pBobTexture, 1.0f, 1.0f );
	if( !pBobSprite )
	{
		DESTROY( pBobTexture );
		return false;
	}

	pIBob->setShape( pBobSprite );
	
#endif
	
	// Inicializa o áudio da aplicação
	if( !initAudio( [hParentView getAudioManager] ) )
		return false;

	#if TARGET_IPHONE_SIMULATOR
		//pIBob->drawTempCoils = true;

		//pIBob->setDrawMode( DRAW_MODE_WIRED );
		pIBob->setDrawMode( DRAW_MODE_QUADS );
	#endif
	
	// Coloca o personagem no meio da cena
	Point3f aux( 0.0f, -WORLD_DIMENSION * 0.5f, 0.0f );
	pIBob->move( &aux );
	
	// Indica que a cena irá receber e tratar eventos de toque
	EventManager::GetInstance()->insertListener( this );
	startListeningTo( EVENT_TOUCH );

	return true;
}

/*==============================================================================================

MÉTODO loadableHandleEvent
	Método para receber os eventos do objeto loadable.

==============================================================================================*/

void BobFullScene::loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data )
{
	switch( loadableId )
	{
		case LOADABLE_MAIN_SCENE:
			if( op == LOADABLE_OP_LOAD )
			{
				glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
				glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
				glEnable( GL_DEPTH_TEST );
				glEnableClientState( GL_VERTEX_ARRAY );
				glEnableClientState( GL_TEXTURE_COORD_ARRAY );

				glShadeModel( GL_FLAT );
			}
			break;
	}
}

/*==============================================================================================

MÉTODO iBobSetHardness
	Modifica as propriedades das molas que constituem o iBob.

==============================================================================================*/

void BobFullScene::iBobSetHardness( float hardness )
{
	pIBob->setTightness( LERP( hardness, MIN_TIGHTNESS, MAX_TIGHTNESS ) );
	pIBob->setDampingFactor( LERP( hardness, MIN_DAMPING, MAX_DAMPING ) );
}

/*==============================================================================================

MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

==============================================================================================*/

bool BobFullScene::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	#if TARGET_IPHONE_SIMULATOR
	try
	{
	#endif
		if( evtType == EVENT_TOUCH )
		{
			const NSArray* pTouchesArray = ( NSArray* )pParam;
			
			switch( evtSpecific )
			{
				case EVENT_TOUCH_BEGAN:
					{
						int8 touchForceIndex = -1;
						const uint8 touchesCount = [pTouchesArray count];
						
						for( uint8 i = 0 ; ( i < touchesCount ) && ( ( touchForceIndex = getUnusedTouchForce() ) >= 0 ) ; ++i )
						{
							// Obtém a informação do toque
							const UITouch* pTouch = [pTouchesArray objectAtIndex: i];
							const CGPoint point = [pTouch locationInView : NULL];
						
							// Testa colisão com o botão de voltar para o menu
							if( pBackToMenuButton->checkCollision( point.x, SCREEN_HEIGHT - point.y - 1.0f ) )
							{
								backToMenuBtVisibleCounter = 0.0f;
								backToMenuBtReductionCounter = 0.0f;
								
								// Se o botão estáinvisível, exibe-o novamente
								if( pBackToMenuButton->getColor()->a == 0.0f )
								{
									pBackToMenuButton->getColor()->a = 1.0f;
								}
								else
								{
									// Cancela todos os toques existentes
									for( uint8 forceIndex = 0 ; forceIndex < MAX_TOUCHES ; ++forceIndex )
										cancelTouchForce( forceIndex );
									
									// Coloca a opacidade do botão no máximo para repetirmos a animação de
									// fade out na próxima vez que entrarmos na tela de jogo
									Color* pAux = pBackToMenuButton->getColor();
									pAux->a = 1.0f;
									
									// Volta para o menu
									[((iBobAppDelegate* )APP_DELEGATE) performTransitionToView: VIEW_INDEX_MAIN_MENU];
								}
								return true;
							}
							
							// Configura a força
							touchForces[ touchForceIndex ]->setTouchId( ( int32 )pTouch );							
							touchForces[ touchForceIndex ]->setDirection();
							touchForces[ touchForceIndex ]->setActive( true );

							// Verifica se houve colisão do toque com o personagem
							Ray ray;
							Utils::GetPickingRay( point.x, SCREEN_HEIGHT - point.y - 1.0f, &ray );

							int16 particleIndex;
							if( ( particleIndex = pIBob->checkCollision( &ray ) ) >= 0  )
							{
								#if DRAG_N_DROP_USES_FORCES
									pIBob->distributeForce( particleIndex, touchForceIndex );
								#else
									iBobParticle* pParticle = pIBob->getParticle( particleIndex );
									pIBob->selectParticle( pParticle );
									touchForces[ touchForceIndex ]->setSelectedParticle( pParticle );
								#endif
							}
							else
							{
								touchForcesMoved( pTouchesArray );
							}

						} // fim for( uint8 i = 0 ; ( i < touchesCount ) && ( nActiveTouchForces < MAX_TOUCHES ) ; ++i )
					}
					break;

				case EVENT_TOUCH_MOVED:
					return touchForcesMoved( pTouchesArray );

				case EVENT_TOUCH_ENDED:
				case EVENT_TOUCH_CANCELED:
					return cancelTouchForces( pTouchesArray );

				default:
					return false;
					
			} // fim switch( evtSpecific )

		} // fim ( evtType == EVENT_TOUCH )

	#if TARGET_IPHONE_SIMULATOR
	}
	catch( ... )
	{
		LOG( @"Entrei no catch de handle event!!!!!!!!" );
	}
	#endif
	return false;
}

/*==============================================================================================

MÉTODO getUnusedTouchForce
	Retorna o índice da primeira força de toque que não está sendo utilizada. Retornará -1 caso
todas as forças já estejam em uso.

==============================================================================================*/

int8 BobFullScene::getUnusedTouchForce( void )
{
	for( int8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( touchForces[i]->getTouchId() == 0 )
			return i;
	}
	return -1;
}

/*==============================================================================================

MÉTODO cancelTouchForces
	Cancela as forças identificadas pelos toques contidos em pTouchesArray.

==============================================================================================*/

bool BobFullScene::cancelTouchForces( NSArray* pTouchesArray )
{
	bool ret = false;
	const uint8 count = [pTouchesArray count];

	for( uint8 i = 0 ; i < count ; ++i )
	{
		// Obtém a informação do toque
		const UITouch* pTouch = [pTouchesArray objectAtIndex:i];
		
		// Verifica qual a força que está tratando este toque
		for( uint j = 0 ; j < MAX_TOUCHES ; ++j )
		{
			if( touchForces[j]->getTouchId() == ( int32 )pTouch )
			{
				cancelTouchForce( j );
				ret = true;
				break;
			}
		}
	}
	return ret;
}

/*==============================================================================================

MÉTODO cancelTouchForce
	Cancela a força de toque com o índice recebido como parâmetro.

==============================================================================================*/

void BobFullScene::cancelTouchForce( uint8 index )
{
	#if ! DRAG_N_DROP_USES_FORCES
		iBobParticle* pSelectedParticle = touchForces[ index ]->getSelectedParticle();

		if( pSelectedParticle != NULL )
		{
			pIBob->unselectParticle( pSelectedParticle );
			touchForces[ index ]->setSelectedParticle( NULL );
		}
	#endif

	touchForces[ index ]->setActive( false );
	touchForces[ index ]->setDirection( NULL );
	touchForces[ index ]->setTouchId( NULL );
}

/*==============================================================================================

MÉTODO touchForcesMoved
	Trata a movimentação das forças de toque.

==============================================================================================*/

bool BobFullScene::touchForcesMoved( NSArray* pTouchesArray )
{
	bool ret = false;

	// Obtém a posição z do personagem em coordenadas da tela
	Point3f iBobProjPos;
	const Point3f iBobPos = *pIBob->getPosition();
	Utils::Project( &iBobProjPos, &iBobPos );
	
	const uint8 count = [pTouchesArray count];
	for( uint8 i = 0 ; i < count ; ++i )
	{
		// Obtém a informação do toque
		const UITouch* pTouch = [pTouchesArray objectAtIndex:i];
		
		// Verifica qual a força que está tratando este toque
		for( uint j = 0 ; j < MAX_TOUCHES ; ++j )
		{
			if( touchForces[j]->getTouchId() == ( int32 )pTouch )
			{
				const CGPoint currPoint = [pTouch locationInView : NULL];
				Point3f currPos( currPoint.x, SCREEN_HEIGHT - currPoint.y - 1.0f, iBobProjPos.z );
				Utils::Unproject( &currPos, &currPos );

				#if ! DRAG_N_DROP_USES_FORCES
					iBobParticle* pSelectedParticle = touchForces[j]->getSelectedParticle();

					if( pSelectedParticle != NULL )
					{
						const CGPoint prevPoint = [pTouch previousLocationInView : NULL];
						Point3f prevPos( prevPoint.x, SCREEN_HEIGHT - prevPoint.y - 1, iBobProjPos.z );
						Utils::Unproject( &prevPos, &prevPos );
						
						Point3f movement = currPos - prevPos;
						pSelectedParticle->move( &movement );
					}
					else
					{
				#endif
						// * 50.0f => Faz com que a força gerada tenha impacto no sistema físico
						Point3f direction = ( currPos - iBobPos ) * 50.0f;
						direction.z = 0.0f;
					
						// TASK : Utilizar a velocidade de movimentação do toque na geração da força
						//direction *= ( 60)[pTouch NSTimeInterval]  100.0f;
					
						touchForces[j]->setDirection( &direction );

				#if ! DRAG_N_DROP_USES_FORCES
					}
				#endif

				ret = true;
				break;
			}
		}
	}
	return ret;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void BobFullScene::render( void )
{
	// Renderiza a cena 3D
	Scene::render();
	
	// Renderiza o botão que leva o usuário da tela de jogo ao menu
	pBackToMenuButton->render();
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

void BobFullScene::update( double timeElapsed )
{
	// Atualiza os objetos da cena
	Scene::update( timeElapsed );

	// Verifica se deve atualizar a opacidade do botão de "voltar ao menu"
	if( backToMenuBtVisibleCounter >= BACK_TO_MENU_BT_VISIBLE_TIME )
	{
		backToMenuBtReductionCounter += timeElapsed;
		if( backToMenuBtReductionCounter >= BACK_TO_MENU_BT_ALPHA_REDUCTION_SPEED )
		{
			backToMenuBtReductionCounter = 0.0f;

			Color* pAux = pBackToMenuButton->getColor();
			if( pAux->a > MIN_BACK_TO_MENU_BT_ALPHA )
			{
				pAux->a -= BACK_TO_MENU_BT_ALPHA_REDUCTION;
				if( pAux->a < MIN_BACK_TO_MENU_BT_ALPHA )
					pAux->a = MIN_BACK_TO_MENU_BT_ALPHA;
			}
		}
	}
	else
	{
		backToMenuBtVisibleCounter += timeElapsed;
	}
}

/*==============================================================================================

MÉTODO setGravity
	Determina a força da gravidade utilizada na cena.

==============================================================================================*/

void BobFullScene::setGravity( float gravity )
{
	World* pWorld = getWorldAt( 0 );	
	(( AccForce* )pWorld->getEnvironmentAt( SCENE_ENVIRONMENT_INDEX )->getForceAt( SCENE_GRAVITY_FORCE_INDEX ))->setAccelerometerBooster( gravity );	
	(( AccForce* )pWorld->getEnvironmentAt( PARTICLES_ENVIRONMENT_INDEX )->getForceAt( PARTICLES_GRAVITY_FORCE_INDEX ))->setAccelerometerBooster( gravity * PARTICLES_GRAVITY_BOOSTER );
}

/*==============================================================================================

MÉTODO iBlobSetColor
	Determina a nova cor do personagem.

==============================================================================================*/

#if BLOB_MODE

void BobFullScene::iBlobSetColor( const Color* pColor )
{
	pIBob->setBlobColor( pColor );
	
	// Altera a cor das partículas emitidas pelo personagem
	Shape* pShape = (( Particle* )pBlobEmitters[0]->getBaseElement())->getShape();
	pShape->setColor( pColor );
}

#endif

/*==============================================================================================

MÉTODO onCollision
	Indica que a partícula passada como parâmetro sofreu colisão.

==============================================================================================*/

#if BLOB_MODE

void BobFullScene::onCollision( const iBobParticle* pParticle )
{
	const float speedModule = pParticle->getSpeedBeforeCollision()->getModule();
	if( speedModule > MIN_BLOB_SPEED )
	{
		const int16 emitterIndex = getUnusedBlobEmitter();
		if( emitterIndex >= 0 )
		{
			pBlobEmitters[ emitterIndex ]->setCurrentActiveElements( 0 );
			pBlobEmitters[ emitterIndex ]->setLife( 0.2f );
			
			const float nParticles = LERP( ( speedModule - MIN_BLOB_SPEED ) / ( MAX_BLOB_SPEED - MIN_BLOB_SPEED ), MIN_BLOB_PARTICLES, MAX_BLOB_PARTICLES ) / pBlobEmitters[ emitterIndex ]->getLife();
			pBlobEmitters[ emitterIndex ]->setEmissionRate( nParticles );
			pBlobEmitters[ emitterIndex ]->setPosition( pParticle->getPosition() );
			
			Point3f speed = *( pParticle->getSpeedBeforeCollision() );
			speed = -speed;			
			pBlobEmitters[ emitterIndex ]->getBaseElement()->setSpeed( &speed );
			
			pBlobEmitters[ emitterIndex ]->setVisible( true );
			pBlobEmitters[ emitterIndex ]->setActive( true );
		}	
	}
}

#endif

/*==============================================================================================

MÉTODO getUnusedBlobEmitter
	Obtém o índice de um emissor que não está sendo utilizado.

==============================================================================================*/

#if BLOB_MODE

int16 BobFullScene::getUnusedBlobEmitter( void )
{
	const uint16 nEmitters = N_BLOB_EMITTERS;
	for( uint16 i = 0 ; i < nEmitters ; ++i )
	{
		if( !pBlobEmitters[i]->isActive() )
			return i;
	}
	return -1;
}

#endif

/*==============================================================================================

MÉTODO createBlobEmitters
	Cria os emissores que serão utilizados quando o cubo colidir com os planos do mundo.

==============================================================================================*/

#if BLOB_MODE

bool BobFullScene::createBlobEmitters( World* pWorld, const Plane* pCollisionPlanes )
{
	// Cria o ambiente físico das partículas
	Environment* pEnvironment = new Environment();
	if( !pEnvironment || ( pWorld->insertEnvironment( pEnvironment ) < 0 ) )
	{
		SAFE_DELETE( pEnvironment )
		return false;
	}

	// Cria a gravidade do ambiente
	AccForce *pGravity = new AccForce( ACC_FORCE_MODE_GRAVITY_VEC, DEFAULT_GRAVITY_BOOSTER * PARTICLES_GRAVITY_BOOSTER );
	if( !pGravity || ( pEnvironment->insertForce( pGravity ) < 0 ) )
	{
		SAFE_DELETE( pGravity );
		return false;
	}
	
	// Cria o array de emissores
	const uint16 nEmitters = N_BLOB_EMITTERS;
	pBlobEmitters = new Emitter*[ nEmitters ];
	if( !pBlobEmitters )
		return false;
	
	// Cria o modelo das partículas que serão emitidas
	Color particleColor = *( pIBob->getBlobColor() );
	particleColor.a = 1.0f;
	Sphere* pShape = new Sphere( 1.0f, 6, &particleColor );
	if( !pShape )
		return false;

	Particle* pBase = new Particle();
	if( !pBase )
	{
		DESTROY( pShape );
		return false;
	}
	Point3f speed( 80.0f, 80.0f, 80.0f );
	pBase->setSpeed( &speed );
	pBase->setShape( pShape );
	pBase->setLife( 200.0f );
	pBase->setSize( 10.0f );
	
	for( uint8 j = 0 ; j < N_COLLISION_PLANES ; ++j )
	{
		if( pBase->insertLimitPlane( &pCollisionPlanes[j] ) < 0 )
		{
			DESTROY( pBase );
			return false;
		}
	}

	// Configura os emissores
	for( uint16 i = 0 ; i < nEmitters ; ++i )
	{
		pBlobEmitters[i] = new Emitter();
		if( !pBlobEmitters[i] || !pBlobEmitters[i]->setBaseElement( pBase )
		    || ( pEnvironment->insertParticle( pBlobEmitters[i]) < 0 ) )
		{
			SAFE_DELETE( pBlobEmitters[i] );
			DESTROY( pBase );
			return false;
		}
		pBlobEmitters[i]->setImmune( true );
		pBlobEmitters[i]->setVisible( false );
		pBlobEmitters[i]->setActive( false );
	}
	return true;
}

#endif

/*==============================================================================================

MÉTODO createBackToMenuButton
	Cria o botão que leva o usuário da tela de jogo ao menu.

==============================================================================================*/

bool BobFullScene::createBackToMenuButton( void )
{
	Texture2D* pTexture = new Texture2D( NULL, 0 );
	if( !pTexture || !pTexture->loadTextureInFile( @"backButton.png" ) ) 
	{
		SAFE_DELETE( pTexture );
		return false;
	}

	pBackToMenuButton = new BackButton( pTexture );
	return pBackToMenuButton != NULL;
}

/*==============================================================================================

MÉTODO initAudio
	Inicializa o áudio da aplicação.

==============================================================================================*/

bool BobFullScene::initAudio( AudioManager* pAudioManager )
{
	PlaybackAudioDevice* pDevice = pAudioManager->getPlaybackDevice();
	pDevice->setDistanceModel( AL_LINEAR_DISTANCE );

	AudioContext* pCurrContext = pDevice->newAudioContext();
	if( !pCurrContext )
		return false;
	
	// Torna este contexto o atual
	pCurrContext->makeCurrentContext();

	// Aloca os sons do personagem
	AudioSource* pScream = Utils::GetAudioSource( pCurrContext, "scream", "wav" );
	if( !pScream )
		return false;
	
	AudioSource* pCollision = Utils::GetAudioSource( pCurrContext, "collision", "wav" );
	if( !pCollision )
		return false;
	
	// Configura a posição do listener
	Point3f listenerPos( 0.0f, 0.0f, ( WORLD_DIMENSION * 0.5f ) - PLANE_COLLISION_OFFSET );
	AudioListener* pListener = pDevice->getAudioListener();
	pListener->setPosition( &listenerPos );
	pListener->setGain( 1.0f );
	
	// Indica que os sons não ficarão em loop
	pScream->setLooping( false );
	pCollision->setLooping( false );
	
	// Determina qual a menor porcentagem do volume do som original que podemos alcançar com o 
	// modelo de distância
	pScream->setMinGain( SCREAM_MIN_GAIN );
	pScream->setMaxGain( SCREAM_MAX_GAIN );

	pCollision->setMinGain( COLLISION_SOUND_MIN_GAIN );
	pCollision->setMaxGain( COLLISION_SOUND_MAX_GAIN );

	// Fator de atenuação do som
	pScream->setRollOffFactor( SCREAM_ROLLOFF_FACTOR );
	pCollision->setRollOffFactor( COLLISION_SOUND_ROLLOFF_FACTOR );
	
	// Maior distância que a fonte pode ficar do listener
	pScream->setMaxDistance( WORLD_DIMENSION - ( PLANE_COLLISION_OFFSET * 2.0f ) );
	pCollision->setMaxDistance( WORLD_DIMENSION - ( PLANE_COLLISION_OFFSET * 2.0f ) );

	// Passa para o personagem os sons alocados
	pIBob->setSound( IBOB_SOUND_INDEX_SCREAM, pScream );
	pIBob->setSound( IBOB_SOUND_INDEX_COLLISION, pCollision );
	
	return true;
}

/*==============================================================================================

MÉTODO getBkgSprites
	Aloca as texturas a serem aplicadas no background da cena.

==============================================================================================*/

bool BobFullScene::getBkgSprites( Sprite** bkgSprites )
{
	NSString* paths[ N_BKG_SPRITES ] = { @"left", @"right", @"floor", @"ceiling", @"rear" };
	
	uint8 i = 0;
	for( ; i < N_BKG_SPRITES ; ++i )
	{
		Texture2D* pBkgTexture = new Texture2D( NULL, LOADABLE_TEX_WALL );
		if( !pBkgTexture || !pBkgTexture->loadTextureInFile( [paths[i] stringByAppendingString: @".png" ]) )
		{
			SAFE_DELETE( pBkgTexture );
			goto Error;
		}
		
		if( i == BKG_SPRITE_FLOOR_INDEX )
			bkgSprites[i] = Floor::CreateInstance( pBkgTexture, 1.0f, 1.0f );
		else
			bkgSprites[i] = Sprite::CreateInstance( pBkgTexture, 1.0f, 1.0f );

		if( !bkgSprites[i] )
		{
			DESTROY( pBkgTexture );
			goto Error;
		}
	}
	return true;
	
	// Label de tratamento de erros
	Error:
		for( uint8 j = 0 ; j < i ; ++j )
			DESTROY( bkgSprites[j] );

		return false;
}


