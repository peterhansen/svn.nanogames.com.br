/*
 *  Polynomial.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/23/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include "NanoTypes.h"

// Exemplos de representação dos termos dos polinômios:

// y = 5 -3x + 4xˆ2
// pTerms = [ 5.0f, -3.0, 4.0f ]

// y = 23 + 9xˆ2
// pTerms = [ 23.0f, 0.0f, 9.0f ]

// y = xˆ2
// pTerms = [ 0.0f, 0.0f, 1.0f ]

class Polynomial
{
	public:
		// Construtores
		Polynomial( uint8 degree, const float* pCoeficients = NULL );

		// Destrutor
		virtual ~Polynomial( void );
	
		// Determina os coeficientes dos termos do polinômio
		inline void setTerms( const float* pCoeficients );
	
		// Obtém o grau do polinômo
		inline uint8 getDegree( void ) const { return ( uint8 )( nTerms - 1 ); };
	
		// Obtém os coeficientes dos termos do polinômio
		inline const float* getCoeficients( void ) const { return pTerms; };
	
		// Calcula qual será o o valor de y para o valor de x recebido
		float evaluate( float x );

		// Regressão Polinomial Linear através do Método dos Mínimos Quadrados
		// http://en.wikipedia.org/wiki/Least_squares
		// http://www.scottsarra.org/math/courses/na/nc/polyRegression.html
		// http://pessoal.sercomtel.com.br/matematica/superior/algebra/mmq/mmq.htm
		static Polynomial* leastSquares( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p );
	
	private:
		// Versão não-otimizada, mas genérica, do método para calcular as mínimas raízes quadradas
		static Polynomial* nonOptimizedLeastSquares( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p );
												 
		// Utiliza a classe Matrix3x3 para otimizar o cálculo das mínimas raízes quadradas.
		static Polynomial* optimizedLeastSquaresDegree2( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p );
	
		// Utiliza a classe Matrix4x4 para otimizar o cálculo das mínimas raízes quadradas
		static Polynomial* optimizedLeastSquaresDegree3( const float* pYs, uint8 firstIndex, uint8 nValues, uint8 degree, Polynomial* p );
	
		// Número de termos do polinômio
		uint16 nTerms;
	
		// Termos do polinômio
		float* pTerms;
};

#endif