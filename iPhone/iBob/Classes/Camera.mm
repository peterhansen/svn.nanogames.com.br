#include "Camera.h"
#include "Utils.h"

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Camera::Camera( CameraType cameraType ) : type( cameraType )
{
	pos.set(   0.0f, 0.0f,  0.0f );
	right.set( 1.0f, 0.0f,  0.0f );
	up.set(    0.0f, 1.0f,  0.0f );
	look.set(  0.0f, 0.0f, -1.0f );
}

/*==============================================================================================

MÉTODO place
	Configura os atributos da câmera na API gráfica.

==============================================================================================*/

void Camera::place( void )
{
	// Mantém os eixos da câmera ortogonais entre si
	makeAxesOrthogonal();
	
	// Constrói a matriz
	// OBS: Matrizes OpenGL são ordenadas da forma column-major
	Matrix4x4 modelView( right.x, right.y, right.z, 0.0f,
						    up.x,    up.y,    up.z, 0.0f,
						 -look.x, -look.y, -look.z, 0.0f,
							0.0f,    0.0f,    0.0f, 1.0f );
	
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glMultMatrixf( ( float* )&modelView );
	glTranslatef( -( right * pos ), -( up * pos ), ( look * pos ) );
}

/*==============================================================================================

MÉTODO lookAt
	Equivalente a gluLookAt
	- http://www.opengl.org/documentation/specs/man_pages/hardcopy/GL/html/glu/lookat.html

==============================================================================================*/

void Camera::lookAt( const Point3f* pEye, const Point3f* pCenter, const Point3f* pUp )
{
	Point3f f = Point3f( pCenter->x - pEye->x, pCenter->y - pEye->y, pCenter->z - pEye->z );
	f.normalize();
	
	Point3f up = *pUp;
	up.normalize();
	
	const Point3f s = f % up;
	const Point3f u = s % f;
	
	const float m[] = {	 s.x,  s.y,  s.z, 0,
						 u.x,  u.y,  u.z, 0,
						-f.x, -f.y, -f.z, 0,
						   0,    0,    0, 1 };

	glMultMatrixf( m );
	glTranslatef( -pEye->x, -pEye->y, -pEye->z );
}

/*==============================================================================================

MÉTODO pitch
	Rotaciona a câmera ao redor do eixo right.

==============================================================================================*/

void Camera::pitch( float angle )
{
	Matrix4x4 m;
	Utils::GetRotationMatrix( &m, &right, angle );
	
	Utils::TransformCoord( &up, &up, &m );
	Utils::TransformCoord( &look, &look, &m );
}

/*==============================================================================================

MÉTODO yaw
	Rotaciona a câmera ao redor do eixo up.

==============================================================================================*/

void Camera::yaw( float angle )
{
	Matrix4x4 m;

	switch( type )
	{
		case CAMERA_TYPE_LAND:
			{
				Point3f worldUp( 0.0f, 1.0f, 0.0f );
				Utils::GetRotationMatrix( &m, &worldUp, angle );
			}
			break;
			
		case CAMERA_TYPE_AIR:
			Utils::GetRotationMatrix( &m, &up, angle );
			break;
	}

	Utils::TransformCoord( &right, &right, &m );
	Utils::TransformCoord( &look, &look, &m );
}

/*==============================================================================================

MÉTODO roll
	Rotaciona a câmera ao redor do eixo look.

==============================================================================================*/

void Camera::roll( float angle )
{
	if( type == CAMERA_TYPE_AIR )
	{
		Matrix4x4 m;
		Utils::GetRotationMatrix( &m, &look, angle );
		
		Utils::TransformCoord( &right, &right, &m );
		Utils::TransformCoord( &up, &up, &m );
	}
}

/*==============================================================================================

MÉTODO walk
	Movimenta a câmera ao longo do eixo look.

==============================================================================================*/

void Camera::walk( float units )
{
	switch( type )
	{
		case CAMERA_TYPE_LAND:
			pos += Point3f( look.x, 0.0f, look.z ) * units;
			break;
			
		case CAMERA_TYPE_AIR:
			pos += look * units;
			break;
	}
}

/*==============================================================================================

MÉTODO strafe
	Movimenta a câmera ao longo do eixo right.

==============================================================================================*/

void Camera::strafe( float units )
{
	switch( type )
	{
		case CAMERA_TYPE_LAND:
			pos += Point3f( right.x, 0.0f, right.z ) * units;
			break;
			
		case CAMERA_TYPE_AIR:
			pos += right * units;
			break;
	}
}

/*==============================================================================================

MÉTODO fly
	Movimenta a câmera ao longo do eixo up.

==============================================================================================*/

void Camera::fly( float units )
{
	if( type == CAMERA_TYPE_AIR )
		pos += up * units;
}

/*==============================================================================================

MÉTODO lookAt
	Direciona o eixo look.

==============================================================================================*/

void Camera::lookAt( const Point3f* pSpot )
{
	look.set( pSpot->x - pos.x, pSpot->y - pos.y, pSpot->z - pos.z );
	makeAxesOrthogonal();
}

/*==============================================================================================

MÉTODO makeAxesOrthogonal
	Garante que os eixos right, up e look são ortogonais entre si.

==============================================================================================*/

void Camera::makeAxesOrthogonal( void )
{
	look.normalize();
	up.normalize();
	
	right = look % up;
	up = right % look;
}

