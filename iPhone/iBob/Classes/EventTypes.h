/*
 *  EventTypes.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/12/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EVENT_TYPES_H
#define EVENT_TYPES_H

// Tipos de eventos que podem ser recebidos pela aplicação
typedef enum EventTypes
{
	EVENT_SYSTEM		= 0x0001,
	EVENT_KEY			= 0x0010,
	EVENT_TOUCH			= 0x0100,
	EVENT_ACCELEROMETER = 0x1000
}EventTypes;

typedef enum EventTypesSpecific
{
	EVENT_TOUCH_BEGAN,
	EVENT_TOUCH_MOVED,
	EVENT_TOUCH_ENDED,
	EVENT_TOUCH_CANCELED
}EventTypesSpecific;

#endif
