#include "Sphere.h"
#include "Macros.h"
#include "Point3f.h"
#include <math.h>

// Radius deve ser sempre 0.5f (diâmetro de 1.0f) na hora de gerar a mesh. glScalef() cuidará
// de gerar esferas maiores
#define DEFAULT_RADIUS 0.5f

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Sphere::Sphere( float radius, int lod, const Color* pColor )
       : Shape( pColor, radius * 2.0f ), lod( lod ), nSphereVertices( 0 ), pSphereVertices( NULL ), pSphereTexCoords( NULL )
{
	// Inicializa o array de vértices
	build();
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Sphere::~Sphere( void )
{
	// Apaga o conteúdo anterior
	SAFE_DELETE_VEC( pSphereVertices );
	SAFE_DELETE_VEC( pSphereTexCoords );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void Sphere::render( void )
{
#if TARGET_IPHONE_SIMULATOR
	if( !pSphereVertices )
	{
		LOG( @"Não foi possivel desenhar a esfera, pois seu array de vertices nao foi inicializado" );
	}
	else
	{
#endif
		glScalef( size, size, size );
		
		glColor4f( color.r, color.g, color.b, color.a );
		
		if( pTexture )
		{
			pTexture->load();
			glEnable( GL_TEXTURE_2D );
			glTexCoordPointer( 2, GL_FLOAT, 0, pSphereTexCoords );
		}
		else
		{
			glDisable( GL_TEXTURE_2D );
		}

		glVertexPointer( 3, GL_FLOAT, 0, pSphereVertices );
		glDrawArrays( GL_TRIANGLE_STRIP, 0, nSphereVertices );
		
		if( pTexture )
		{
			pTexture->unload();
			glDisable( GL_TEXTURE_2D );
		}
		
		const float invSize = 1.0f / size;
		glScalef( invSize, invSize, invSize );

#if TARGET_IPHONE_SIMULATOR
	}
#endif
}

/*==============================================================================================

MÉTODO getCopy
	Obtém uma cópia do objeto.

==============================================================================================*/

Shape* Sphere::getCopy( void )
{
	return new Sphere( getRadius(), lod, getColor() );
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.
	Baseado na função escrita por Paul Bourke. Referência:
		- http://astronomy.swin.edu.au/~pbourke/opengl/sphere/

==============================================================================================*/

void Sphere::build( void )
{
	// Radius deve ser sempre 0.5f (diâmetro de 1.0f) na hora de gerar a mesh. glScalef() cuidará
	// de gerar esferas maiores
    // Não permite raios negativos
    //if( radius < 0 )
	//	radius = -radius;

    // Não permite um nível de precisão menor que 4
    if( lod < 4 ) 
        lod = 4;
	
	// Calcula o número de vértices necessários
	// OBS: nSphereVertices = ( lod / 2 ) * ( ( lod + 1 ) * 2 )  =  lodˆ2 + lod
	nSphereVertices = ( lod * lod ) + lod;
	
	// Cria o vetor de vértices
	// OBS: Se não conseguir alocar a memória, irá disparar uma exceção
	pSphereVertices = new Point3f[ nSphereVertices ];
	memset( pSphereVertices, 0, sizeof( Point3f ) * nSphereVertices );
	
	pSphereTexCoords = new float[ nSphereVertices ][2];
	memset( pSphereTexCoords, 0, sizeof( float ) * nSphereVertices << 1 );

    int k = 0;

	const int halfLOD = lod >> 1;
	const float TWO_PI_DIV_LOD = TWO_PI / lod, floatCastedLOD = ( float )lod;

	float ex, ez, py0, py1, texU, texV0, texV1;
	float theta1, theta2, theta3, sinTheta1, cosTheta1, sinTheta2, cosTheta2, sinTheta3, cosTheta3;
	
    for( int i = 0; i < halfLOD ; ++i )
    {
        theta1 = ( i * TWO_PI_DIV_LOD ) - M_PI_2;
        theta2 = (( i + 1 ) * TWO_PI_DIV_LOD ) - M_PI_2;
		
		sinTheta1 = sinf( theta1 );
		cosTheta1 = cosf( theta1 );
		sinTheta2 = sinf( theta2 );
		cosTheta2 = cosf( theta2 );

		py0 = DEFAULT_RADIUS * sinTheta2;
		py1 = DEFAULT_RADIUS * sinTheta1;
		
		texV0 = ( ( i+1 ) << 1 ) / floatCastedLOD;
		texV1 = ( i << 1 ) / floatCastedLOD;

		for( int j = 0; j <= lod ; ++j )
		{
			theta3 = j * TWO_PI_DIV_LOD;

			sinTheta3 = sinf( theta3 );
			cosTheta3 = cosf( theta3 );

			ex = cosTheta2 * cosTheta3;
			ez = cosTheta2 * sinTheta3;
			texU = -( j / floatCastedLOD );
			//pSphereVertices[ k ].setNormal( ex, sinTheta2, ez );
			pSphereTexCoords[ k ][0] = texU;
			pSphereTexCoords[ k ][1] = texV0;
			pSphereVertices[ k ].set( DEFAULT_RADIUS * ex, py0, DEFAULT_RADIUS * ez );
			++k;

			ex = cosTheta1 * cosTheta3;
			ez = cosTheta1 * sinTheta3;
			
			//pSphereVertices[ k ].setNormal( ex, sinTheta1, ez );
			pSphereTexCoords[ k ][0] = texU;
			pSphereTexCoords[ k ][1] = texV1;
			pSphereVertices[ k ].set( DEFAULT_RADIUS * ex, py1, DEFAULT_RADIUS * ez );
			++k;
		}
    }
}
