#include "SpringCoil.h"

// Erro aceito na precisão de float
#define SPRINGCOIL_FLOAT_EPSILON 0.005f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

SpringCoil::SpringCoil( float elasticity, bool breacheable, Particle* particle1, Particle* particle2, bool maintainDirection, float tightness, float dampingFactor, float myLife )
: maxLength( maxLength ), breacheable( breacheable ), p1( particle1 ), p2( particle2 ), tightness( tightness ), dampingFactor( dampingFactor ), maintainDirection( maintainDirection )
{
	life = myLife;
	immortal = life < 0.0f;
	set( particle1, particle2, tightness, dampingFactor, elasticity );
}

/*===========================================================================================
 
MÉTODO update
 
============================================================================================*/

bool SpringCoil::update( double timeElapsed )
{
#if TARGET_IPHONE_SIMULATOR
	if( !p1 || !p2 )
	{
		LOG( @"Mola invalida\n" );
		return false;
	}
#endif

	life -= timeElapsed;
	if( immortal || ( life > 0.0f ) )
	{
		Point3f distance = ( *p1->getPosition() ) - ( *p2->getPosition() );
		Point3f relativeSpeed = ( *p1->getSpeed() ) - ( *p2->getSpeed() );

		float distanceModule = distance.getModule();
		Point3f distanceUnit = distance / distanceModule;

		// Garante o comprimento mínimo da mola
		const float minLength = restLength * 0.2f;
		const bool tooLong = FGEQ( distanceModule, maxLength );
		if( tooLong )
		{
			if( breacheable )
			{
				// Se o comprimento atual for maior que o maior comprimento suportado pela mola,
				// esta arrebentou
				return true;
			}
			
			Point3f offset = distanceUnit * ( ( maxLength - distanceModule ) * 0.5f );

			Point3f temp = ( *p1->getPosition() ) + offset;
			p1->setPosition( &temp );
			
			temp = ( *p2->getPosition() ) - offset;
			p2->setPosition( &temp );

			distance = ( *p1->getPosition() ) - ( *p2->getPosition() );

			// Conserta o erro de precisão do float
			distanceModule = maxLength;
			distanceUnit = distance / distanceModule;
		}
		else if( distanceModule < minLength )
		{
			Point3f offset = distanceUnit * ( ( minLength - distanceModule ) * 0.5f );

			Point3f temp = ( *p1->getPosition() ) + offset;
			p1->setPosition( &temp );
			
			temp = ( *p2->getPosition() ) - offset;
			p2->setPosition( &temp );

			distance = ( *p1->getPosition() ) - ( *p2->getPosition() );

			// Conserta o erro de precisão do float
			distanceModule = minLength;
			distanceUnit = distance / distanceModule;
		}

		// TASK : Faz sentido manter este if ???
//		if( FGEQ( distanceModule, 1.0f ) )
//		{
			// F = -k(|x|-d)(x/|x|) - bv
			// k = constante da mola (tightness)
			// x = vetor distância atual entre as partÌculas
			// d = comprimento de repouso da mola (restLength)
			// v = vetor da velocidade relativa entre as partÌculas ligadas pela mola
			// b = fator de amortecimento (dampingFactor)
			const float d = distanceModule - restLength;

			// Para manter o formato do cubo, a fórmula original de molas foi alterada para poder levar em
			// conta a direção original das molas
			Point3f force = ( ( distanceUnit * ( -tightness * d ) ) - ( relativeSpeed * dampingFactor ) );

			if( maintainDirection )
				force = ( force + ( ( originalDirection * ( -tightness * d ) ) - ( relativeSpeed * dampingFactor ) ) ) * 0.5f;

			p1->force += force;
			p2->force -= force;
//		}
		return false;
	}

	// Mola não é imortal e seu tempo de vida é menor ou igual a zero
	return true;
}

/*===========================================================================================
 
MÉTODO set
 
============================================================================================*/

void SpringCoil::set( Particle* particle1, Particle* particle2, float tightness, float dampingFactor, float elasticity )
{
	// Temos que manter este if por causa do construtor padrão!!!
	if( particle1 && particle2 )
	{
		originalDirection = ( *particle1->getPosition() ) - ( *particle2->getPosition() );
		restLength = originalDirection.getModule();
		maxLength = restLength * elasticity;
		
		if( restLength > 0.0f )
			originalDirection.normalize();

		this->p1 = particle1;
		this->p2 = particle2;
	}
	this->tightness = tightness;
	this->dampingFactor = dampingFactor;
}
