#include "Texture.h"

/*==============================================================================================
 
CONSTRUTOR
 
==============================================================================================*/

Texture::Texture( LoadableListener *pListener, uint32 loadableId ) : Loadable( pListener, loadableId )
{
	// Utiliza o OpenGL para gerar um identificador para a textura
	glGenTextures( 1, &texId );
	
	setListenerData( texId );
}

/*==============================================================================================
 
DESTRUTOR
 
==============================================================================================*/

Texture::~Texture( void )
{
	// Libera o identificador para posterior utilização pelo OpenGL
	glDeleteTextures( 1, &texId );
}
