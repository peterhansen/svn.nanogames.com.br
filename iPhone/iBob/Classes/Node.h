#ifndef NODE_H
#define NODE_H

/*==============================================================================================

CLASSE Node

==============================================================================================*/

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include <stddef.h>		// NULL

#include "Point3f.h"

// TASK : Absorver alguns atributos de particle
class Node
{
	public:
		// Construtor
		Node( const Point3f* pPosition = NULL, const Point3f* pSpeed = NULL );
	
		// Destrutor
		virtual ~Node( void );

		// Obtém a posição do objeto
		virtual inline const Point3f* getPosition( void ) const { return &position; };
		
		// Determina a posição do objeto
		virtual void setPosition( const Point3f *pPosition );
	
		// Movimenta o objeto a partir de sua posição atual
		inline void move( const Point3f *pMovement ){ const Point3f aux = position + *pMovement; setPosition( &aux ); };

		// Obtém a velocidade do objeto
		inline const Point3f* const getSpeed( void ) const { return &speed; };
		
		// Determina a velocidade do objeto
		virtual void setSpeed( const Point3f *pSpeed );

		// Determina o nó pivot
		virtual void attach( Node *pPivot );
	
		// Cancela a referência ao nó pivot
		virtual void detach( void );

		// Determina o pivot do nó
		inline const Node* getPivot( void ) const { return pPivot; };

	protected:
		// Carrega as transformações do nó pivot
		void loadTransform( void );
	
		// Descarrega as transformações do nó pivot
		void unloadTransform( void );

		// Posição do nó
		Point3f	position;
	
		// Velocidade do nó
		Point3f speed;
	
		// Nó pivot
		Node *pPivot;
};

#endif