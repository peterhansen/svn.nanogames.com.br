#include "AccForce.h"
#include "Polynomial.h"
#include "Exceptions.h"
#import "AccManager.h"

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

AccForce::AccForce( AccForceCalcMode mode, float booster, float gravityFilteringFactor )
		 :Force(), mode( mode ), accelHistoryCounter( 0 ), filteringFactor( gravityFilteringFactor ), booster( booster )
{
	if( !AccManager::GetInstance()->insertListener( this, &myAccManagerListenerIndex ) )
#if TARGET_IPHONE_SIMULATOR
		throw InvalidOperationException( "AccForce::AccForce() - Não foi possível acrescentar um listener a AccManager" );
#else
		throw InvalidOperationException();
#endif

	memset( acceleration, 0, 3 * sizeof( float ) );
	memset( accelHistory, 0, 3 * sizeof( float ) * ACCEL_HISTORY_LEN );	
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

AccForce::~AccForce( void )
{
	AccManager::GetInstance()->removeListener( myAccManagerListenerIndex );
}

/*===========================================================================================
 
MÉTODO OnAccelerate
	Recebe os eventos do acelerômetro.
 
============================================================================================*/

void AccForce::OnAccelerate( const AccManager* pAccelerometer, const Point3f* pAcceleration )
{
	switch( mode )
	{
		// Obtém apenas a aceleração relacionada à gravidade
		case ACC_FORCE_MODE_GRAVITY:
			direction.x = ( pAcceleration->x * filteringFactor ) + ( direction.x * ( 1.0f - filteringFactor ) );
			direction.y = ( pAcceleration->y * filteringFactor ) + ( direction.y * ( 1.0f - filteringFactor ) );
			direction.z = ( pAcceleration->z * filteringFactor ) + ( direction.z * ( 1.0f - filteringFactor ) );
			direction *= booster;
			break;
			
		case ACC_FORCE_MODE_GRAVITY_VEC:
			direction.x = ( pAcceleration->x * filteringFactor );
			direction.y = ( pAcceleration->y * filteringFactor );
			direction.z = ( pAcceleration->z * filteringFactor );
			direction.normalize();
			direction *= booster;
			break;

		// Obtém somente a aceleração relacionanda aos movimentos
		case ACC_FORCE_MODE_MOVEMENT:
			acceleration[0] = ( pAcceleration->x * filteringFactor ) + ( acceleration[0] * ( 1.0f - filteringFactor ));
			direction.x = ( pAcceleration->x - acceleration[0]  ) * booster;
			
			acceleration[1] = ( pAcceleration->y * filteringFactor ) + ( acceleration[1] * ( 1.0f - filteringFactor ));
			direction.y = ( pAcceleration->y - acceleration[1]  ) * booster;
			
			acceleration[2] = ( pAcceleration->z * filteringFactor ) + ( acceleration[2] * ( 1.0f - filteringFactor ));
			direction.z = ( pAcceleration->z - acceleration[2]  ) * booster;
			break;

		case ACC_FORCE_MODE_AVERAGE:
			{
				Point3f lastValue;
				setHistory( pAcceleration, &lastValue );
				
				// Calcula a média aritmética dos valores antigos
				Point3f average;
				
				for( int32 i = 0 ; i < ACCEL_HISTORY_LEN ; ++i )
				{
					average.p[0] += accelHistory[0][ i ];
					average.p[1] += accelHistory[1][ i ];
					average.p[2] += accelHistory[2][ i ];
				}
				
				// Retira o último valor recebido (mais rápido desse jeito do que colocar um if dentro
				// do for)
				average -= lastValue;
				average /= ( ACCEL_HISTORY_LEN - 1 );
				
				// Fornece um peso maior aos valores antigos
				const float oldValues = 0.9f; // [0.0f, 1.0f]
				average = ( average * oldValues ) + ( lastValue * ( 1.0f - oldValues ) );

				direction = average;
			}
			break;

		case ACC_FORCE_MODE_POLINOMIAL_2:
		case ACC_FORCE_MODE_POLINOMIAL_3:
		case ACC_FORCE_MODE_POLINOMIAL_4:
			{
				setHistory( pAcceleration );

				Polynomial p( mode - ACC_FORCE_MODE_POLINOMIAL_2 + 2 );
				const uint8 degree = p.getDegree();

				for( uint8 i= 0 ; i < 3 ; ++i )
				{
					try
					{
						Polynomial::leastSquares( accelHistory[i], accelHistoryCounter, ACCEL_HISTORY_LEN, degree, &p );
						direction.p[i] = p.evaluate( ACCEL_HISTORY_LEN - 1 );
					}
					catch( IllegalMathOperation& ex )
					{
						// Opa! Provavelmente não foi possível fazer o cálculo da regressão polinomial
						// com os dados atuais
					}
				}
			}
			break;
			
		// Obtém a aceleração sem filtragem
		case ACC_FORCE_MODE_RAW:
		default:
			direction = ( *pAcceleration ) * booster;
			break;
	}
}

/*===========================================================================================
 
MÉTODO setHistory
	Armazena o hitórico da aceleração.
 
============================================================================================*/

void AccForce::setHistory( const Point3f* pNewAccelData, Point3f* pLastValue )
{
	// Retira a força da gravidade
	float x, y, z;
	acceleration[0] = ( pNewAccelData->x * filteringFactor ) + ( acceleration[0] * ( 1.0f - filteringFactor ));
	x = ( pNewAccelData->x - acceleration[0]  ) * booster;
	
	acceleration[1] = ( pNewAccelData->y * filteringFactor ) + ( acceleration[1] * ( 1.0f - filteringFactor ));
	y = ( pNewAccelData->y - acceleration[1]  ) * booster;
	
	acceleration[2] = ( pNewAccelData->z * filteringFactor ) + ( acceleration[2] * ( 1.0f - filteringFactor ));
	z = ( pNewAccelData->z - acceleration[2]  ) * booster;
	
	// Armazena a aceleração filtrada no histórico
	accelHistory[0][ accelHistoryCounter ] = x;
	accelHistory[1][ accelHistoryCounter ] = y;
	accelHistory[2][ accelHistoryCounter ] = z;
	
	if( pLastValue )
		pLastValue->set( x, y, z );

	accelHistoryCounter = ( accelHistoryCounter + 1 ) % ACCEL_HISTORY_LEN;
}
