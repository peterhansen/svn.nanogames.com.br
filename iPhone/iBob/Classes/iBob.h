#ifndef IBOB_H
#define IBOB_H

/*==============================================================================================

CLASSE iBob

==============================================================================================*/

#include "AudioSource.h"
#include "Emitter.h"
#include "iBobParticle.h"
#include "iBobSpringCoil.h"

#if BLOB_MODE
	#include "CollisionListener.h"
#endif

#if TARGET_IPHONE_SIMULATOR
	// Modos de desenho do cubo
	#define DRAW_MODE_WIRED 0
	#define DRAW_MODE_QUADS 1
#endif

// Define quantos sons o personagem pode emitir
#define IBOB_N_SOUNDS 2

// Define os índices dos sons do personagem
#define IBOB_SOUND_INDEX_SCREAM 0
#define IBOB_SOUND_INDEX_COLLISION 1

class iBob : public iBobParticle
{
	public:
		// Construtor
		iBob( const Point3f* pPosition = NULL, const Point3f* pSpeed = NULL );
	
		// Destrutor
		virtual ~iBob( void );

		// Inicializa o componente
		#if DRAG_N_DROP_USES_FORCES
			bool build( TouchForce* touchForces[], float dimension, uint8 nPointsByEdge, float tightness, float damping, float stickingFactor );
		#else
			bool build( float dimension, uint8 nPointsByEdge, float tightness, float damping, float stickingFactor );
		#endif

		// Atualiza o objeto
		virtual bool update( double timeElapsed, const DynamicVector* pForces, Plane* pCollisionPlane = NULL );
	
		// Renderiza o objeto
		virtual void render( void );

		// Insere um novo plano limite no vetor de planos
		virtual int16 insertLimitPlane( const Plane* pLimitPlane );

		#if TARGET_IPHONE_SIMULATOR
			// Especifica qual o modo de renderização do iBob
			void setDrawMode( uint8 mode );
		
			// Obtém o modo de renderização do iBob
			inline uint8 getDrawMode( void ) const { return drawMode; };
		#endif

		// Determina / obtém a aderência do cubo à superfícies externas( valores serão limitados entre 0.0f e 1.0f )
		void setStickingFactor( float factor );
		float getStickingFactor( void );

		void setTightness( float tightness );
		inline float getTightness( void ) const { return pCoils ? pCoils[0].tightness: 0.0f; };
	
		void setDampingFactor( float factor );
		inline float getDampingFactor( void ) const { return pCoils ? pCoils[0].dampingFactor: 0.0f; };

		// Determina a velocidade do objeto
		virtual void setSpeed( const Point3f* pSpeed );
	
		// Determina a posição do objeto
		virtual void setPosition( const Point3f *pPosition );

		// Reinicializa o objeto
		void reset( void );
	
		// Verifica se o iBob sofreu colisão com o raio. Retorna o índice da partícula que sofreu
		// a colisão ou um número negativo caso não haja interseção
		int16 checkCollision( const Ray* pRay ) const;
	
		#if DRAG_N_DROP_USES_FORCES
			// Distribui uma força pelas partículas do cubo, dissipando-a conforme as partículas vão se
			// afastando do ponto inicial
			void distributeForce( uint8 firstParticleIndex, uint8 forceIndex );
		#else
			// Informa que a partícula passada como parâmetro está selecionada e deve parar de sofrer
			// a atuação das forças físicas
			void selectParticle( iBobParticle* pParticle );
	
			// Informa que a partícula passada como parâmetro não está mais selecionada e deve voltar
			// a sofrer a atuação das forças físicas.
			void unselectParticle( iBobParticle* pParticle );
	
			// Retorna a partícula relativa ao índice recebido como parâmetro
			inline iBobParticle* getParticle( uint8 particleIndex ) const { return ( iBobParticle* )pCubeVertices->getData( particleIndex ); };
		#endif

		#if TARGET_IPHONE_SIMULATOR
			// Indica se devemos desenhar as molas temporárias
			bool drawTempCoils;
		#endif
	
		#if BLOB_MODE
			// Retorna o número de partículas utilizadas nas 6 faces do cubo
			inline uint16 getNFaceParticles( void ) const { return facesParticlesLength; };
	
			// Determina um objeto que irá receber notificações de colisões
			inline void setCollisionListener( CollisionListener* pListener ) { pCollisionListener = pListener; };
	
			// Retorna a cor do personagem
			inline const Color* getBlobColor( void ) const { return &blobColor; };
	
			// Determina a cor do personagem
			inline void setBlobColor( const Color* pColor ) { blobColor = *pColor; };
		#endif
	
		#if TARGET_IPHONE_SIMULATOR
			inline const Point3f* getParticlePos( uint8 particleIndex ) const { return (( iBobParticle* )pCubeVertices->getData( particleIndex ))->getPosition(); }; 
		#endif
	
		// Determina o som a ser utilizado pelo personagem no índice index
		inline void setSound( uint8 index, AudioSource* pSound ) { sounds[ index ] = pSound; };

	private:
		// Cria molas temporárias entre os nós do cubo e pParticle
		bool createTemporaryCoils( iBobParticle* pParticle );
	
		// Retorna qual vértice do triângulo está mais próximo do ponto recebido como parâmetro.
		static uint8 getNearestVertex( const Triangle* pTriangle, const Point3f* pPoint );

		// Recalcula a posição e a velocidade do cubo de acordo com a posição de suas partículas
		void recalcPosAndSpeed( void );
	
		// Determina a posição das fontes de som
		void updateAudioSources( void );
	
		// Método chamado quando uma das partículas que constitui o personagem sofre colisão com um
		// dos planos da cena.
		// TASK : void onCollision( void );

		#if DRAG_N_DROP_USES_FORCES
			// Percorre o grafo em profunidade
			static void graphIterator( const iBobParticle* pParticle, uint8 touchForceIndex, float weight );
		#endif
	
		#if CONTROLLING_ORIENTATION
			// Calcula a orientação do iBob no mundo 3D
			void setOrientation( void );
		#endif
	
		#ifdef TARGET_IPHONE_SIMULATOR
			// Indica o modo de desenho
			uint8 drawMode;
		#endif
	
		// Quantos pontos por aresta nosso cubo terá
		uint8 nPointsByEdge;
	
		#if COLLISION_SOUND
			// Auxiliares que indicam se alguma partícula do personagem sofreu colisão na atualização anterior
			int8 colliding;
			bool alreadyPlayedSound;
			uint8 noCollisionCounter;
			Plane lastCollisionPlane;
		#endif

		uint16 nParticles, numberOfCoils, facesParticlesLength, dimension;
	
		// Orientação do iBob no mundo 3D
		Point3f orientation;

		// Vértices que formam as faces do cubo
		DynamicVector	*pCubeVertices,
						*pTempCoils,
						*pCubeOriginalPositions; // Posições originais dos nós para podermos reiniciar o cubo

		// Partículas que vão causar a aderência do cubo
		iBobParticle** pFacesParticles;

		// Array de molas do cubo (não inclui as molas temporárias)
		iBobSpringCoil* pCoils;
	
		// Array de sons do personagem
		AudioSource* sounds[ IBOB_N_SOUNDS ];
	
		#if BLOB_MODE
			// Cria os olhos da gosma
			bool setEyes( uint16 nFaceVertices );
	
			// TASK : SHRINK
			//void shrinkCoils( void );
	
			// Cor do personagem
			Color blobColor;

			// Olhos da gosma
			iBobParticle* pEyes[2];
	
			// Objeto que irá receber notificações de colisões
			CollisionListener* pCollisionListener;
		#endif
};

#endif