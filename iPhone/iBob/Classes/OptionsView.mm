#import "OptionsView.h"
#include "Config.h"
#include "iBobAppDelegate.h"

// Extensão da classe para declarar métodos privados
@interface OptionsView()

- ( void ) backToGame;

@end

// Implementação da classe
@implementation OptionsView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize reset;

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( id )initWithFrame:( CGRect )frame
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//		reset = false;
//
//	return self;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
		reset = false;

    return self;
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( void )drawRect:( CGRect )rect
//{
//    // Drawing code
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM getForceLevel
	Retorna o multiplicador que devemos utilizar sobre as forças geradas pelos acelerômetros.

==============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

- ( float )getForceLevel
{
	// TODO : A implementação correta é esta
	//return LERP( [hForceLevel value], MIN_ACC_BOOSTER, MAX_ACC_BOOSTER );

	return DEFAULT_ACC_BOOSTER;
}

#endif

/*==============================================================================================

MENSAGEM getMovementCalcMode
	Retorna o modo de cálculo a ser utilizado na interpretação dos dados dos acelerômetros.

==============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

- ( AccForceCalcMode )getMovementCalcMode
{
	// TODO : A implementação correta é esta
	//return ( AccForceCalcMode )( ACC_FORCE_MODE_AVERAGE + ( uint8 )[hMovementCalcMode selectedSegmentIndex] );
	return ACC_FORCE_MODE_AVERAGE;
}

#endif

/*==============================================================================================

MENSAGEM getHardness
	Retorna o coeficiente de consistência a ser usado pelo personagem.

==============================================================================================*/

- ( float )getHardness
{
	return [hSliderHardness value];
}

/*==============================================================================================

MENSAGEM getStickness
	Retorna o coeficiente de aderência a ser usado pelo personagem.

==============================================================================================*/

- ( float )getStickness
{
	return [hSliderStickness value];
}

/*==============================================================================================

MENSAGEM getSoundVolume
	Retorna o volume do som.

==============================================================================================*/

- ( float )getSoundVolume
{
	return [hSliderSound value];
}

/*==============================================================================================

MENSAGEM getGravity
	Retorna a força da gravidade.

==============================================================================================*/

- ( float )getGravity
{
	return LERP( [hSliderGravity value], MIN_GRAVITY_BOOSTER, MAX_GRAVITY_BOOSTER );
}

/*==============================================================================================

MENSAGEM onReset
	Evento recebido quando o jogador deseja reinicializar o estado do personagem.

==============================================================================================*/

- ( IBAction )onReset
{
	// Não reseta o volume
	//[hSliderSound setValue: DEFAULT_SOUND_VOLUME animated: YES];

	[hSliderHardness setValue: DEFAULT_HARDNESS animated: YES];
	[hSliderStickness setValue: DEFAULT_STICKNESS animated: YES];
	[hSliderGravity setValue: DEFAULT_GRAVITY animated: YES];
	
	reset = true;
	
	// Espera 1 segundo antes de chamar a tela de jogo. Assim as animações das UISliders são exibidas para o jogador
	[NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(backToGame) userInfo:NULL repeats:NO];
}

/*==============================================================================================

MENSAGEM backToGame
	Retorna para a tela de jogo.

==============================================================================================*/

-( void )backToGame
{
	[((iBobAppDelegate* )APP_DELEGATE) performTransitionToView: VIEW_INDEX_GAME];
}

// Fim da implementação da classe
@end
