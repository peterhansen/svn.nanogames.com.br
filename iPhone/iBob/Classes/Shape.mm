#include "Shape.h"
#include "Macros.h"
#include "Point3f.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Shape::Shape( const Color* pColor, float size ) : size( size ), pTexture( NULL )
{
	setColor( pColor );
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

Shape::~Shape( void )
{
	SAFE_DELETE( pTexture );
}

/*===========================================================================================

MÉTODO setSize
	Determina o tamanho do objeto.

============================================================================================*/

void Shape::setSize( float size )
{
	this->size = size;
}

/*===========================================================================================

MÉTODO setColor
	Determina a cor do objeto.

============================================================================================*/

void Shape::setColor( const Color *pColor )
{
	if( pColor )
		color = *pColor;
	else
		color = Color( COLOR_WHITE );
}

/*===========================================================================================

MÉTODO setTexture
	Determina a textura do objeto.

============================================================================================*/

void Shape::setTexture( Texture* pTexture )
{
	this->pTexture = pTexture;
}
