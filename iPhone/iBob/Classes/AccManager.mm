#include "AccManager.h"
#include "Macros.h"
#include "Exceptions.h"

// Frequência de atualização padrão do acelerômetro
#define DEFAULT_ACC_FREQ 50.0 // Hz
#define DEFAULT_UPDATE_INTERVAL ( 1.0f / DEFAULT_ACC_FREQ )

// Inicializa as variáveis estáticas da classe
AccManager* AccManager::pSingleton = NULL;

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

AccManager::AccManager( void ) : updateInterval( DEFAULT_UPDATE_INTERVAL ), hInterface( NULL )
{
	hInterface = [[AccelerometerInterface alloc]init];
	if( !hInterface )
#if TARGET_IPHONE_SIMULATOR	// TODO : É necessário?! Será que o [[alloc]init] já dispara a exceção?!?!?
		throw OutOfMemoryException( "Não foi possível criar AccelerometerInterface" );
#else
		throw OutOfMemoryException();
#endif

	memset( listeners, 0, sizeof( AccListener* ) * ACC_MANAGER_N_LISTENERS );
}
	
/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

AccManager::~AccManager( void )
{
	KILL( hInterface );
	
	// É sempre uma boa política anular os ponteiros
	memset( listeners, 0, sizeof( AccListener* ) * ACC_MANAGER_N_LISTENERS );
}

/*===========================================================================================
 
MÉTODO GetInstance
	Retorna a única instância da classe.

============================================================================================*/

AccManager* AccManager::GetInstance( void )
{
	if( !pSingleton )
	{
		pSingleton = new AccManager();
		[pSingleton->hInterface setListener: pSingleton];
	}
	return pSingleton;
}

/*===========================================================================================
 
MÉTODO release
	Libera a memória alocada pela única instância da classe.

============================================================================================*/	

void AccManager::release( void )
{
	if( pSingleton )
	{
		pSingleton->suspend();
		DESTROY( pSingleton );
	}
}

/*===========================================================================================
 
MÉTODO resume
	Começa a receber os eventos do acelerômetro.

============================================================================================*/	

void AccManager::resume( void )
{
	[hInterface startListening];
}

/*===========================================================================================
 
MÉTODO suspend
	Pára de receber os eventos do acelerômetro.

============================================================================================*/

void AccManager::suspend( void )
{
	[hInterface stopListening];
}

/*===========================================================================================
 
MÉTODO setListener
	Determina que um objeto deve começar a receber os eventos do acelerômetro.
 
============================================================================================*/

bool AccManager::insertListener( AccListener* pNewListener, uint8* pIndex )
{
	// Procura pelo primeiro índice livre
	for( uint8 i = 0 ; i < ACC_MANAGER_N_LISTENERS ; ++i )
	{
		if( listeners[ i ] == NULL )
		{
			if( pIndex )
				*pIndex = i;
			
			listeners[ i ] = pNewListener;
			
			return true;
		}
	}
	
	#if TARGET_IPHONE_SIMULATOR
		LOG( @"AccManager::insertListener(): o numero de listeners ja chegou ao maximo permitido\n" );
	#endif

	return false;
}

/*===========================================================================================
 
MÉTODO setListener
	Indica que um objeto deve parar de receber os eventos do acelerômetro.
 
============================================================================================*/

bool AccManager::removeListener( uint8 index )
{
	#if TARGET_IPHONE_SIMULATOR
	
		if( index >= ACC_MANAGER_N_LISTENERS )
		{
			LOG( @"AccManager::removeListener(): index não está contido no vetor de listeners\n" );
			return false;
		}
	
	#endif
	
	listeners[ index ] = NULL;
	
	return true;
}

/*===========================================================================================
 
MÉTODO setUpdateInterval
	Determina a frequência com que o usuário deseja receber os eventos do acelerômetro.
 
============================================================================================*/

void AccManager::setUpdateInterval( float secs )
{
	if( secs <= 0.0f )
		[hInterface setUpdateInterval:DEFAULT_UPDATE_INTERVAL];
	else
		[hInterface setUpdateInterval:secs];
}

/*===========================================================================================
 
MÉTODO setUpdateInterval
	Recebe os eventos do acelerômetro.
 
============================================================================================*/

void AccManager::OnAccelerate( const AccManager* pAccelerometer, const Point3f* pAcceleration )
{
	for( uint8 i = 0 ; i < ACC_MANAGER_N_LISTENERS ; ++i )
	{
		if( listeners[i] )
			listeners[i]->OnAccelerate( pAccelerometer, pAcceleration );
	}
}

