/*
 *  Viewport.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/5/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef VIEWPORT_H
#define VIEWPORT_H

struct Viewport
{
	union
	{
		struct
		{
			int32 x, y, width, height;
		};
		int32 viewport[4];
	};
};

#endif
