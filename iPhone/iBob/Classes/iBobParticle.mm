#include "iBobParticle.h"

#if ! DRAG_N_DROP_USES_FORCES
	#include "Force.h"
#endif

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

#if DRAG_N_DROP_USES_FORCES
iBobParticle::iBobParticle( TouchForce* touchForces[], const Point3f *pPosition, const Point3f *pSpeed, float stickingFactor )
#else
iBobParticle::iBobParticle( const Point3f *pPosition, const Point3f *pSpeed, float stickingFactor )
#endif
			 : Particle( pPosition, pSpeed ), faceParticle( false ), 
#if DRAG_N_DROP_USES_FORCES
			   visited( false ),
#else
			   selected( false ),
#endif
			   nTempCoils( 0 ), stickingFactor( stickingFactor )
{
	#if DRAG_N_DROP_USES_FORCES

		if( touchForces )
			memcpy( this->touchForces, touchForces, sizeof( TouchForce* ) * MAX_TOUCHES );
		else
			memset( this->touchForces, 0, sizeof( TouchForce* ) * MAX_TOUCHES );
		
		memset( touchForcesWeights, 0, sizeof( float ) * MAX_TOUCHES );

	#endif

	setMass( 10.0f );
	setImmortal( true );
	setSize( 1.0f );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

iBobParticle::~iBobParticle( void )
{
	SAFE_DELETE( pNeighbors );
}

/*==============================================================================================

MÉTODO build

==============================================================================================*/

bool iBobParticle::build( void )
{
	const uint8 MIN_NEIGHBORS = 6;
	pNeighbors = new DynamicVector( MIN_NEIGHBORS );
	return ( pNeighbors != NULL );
}

/*==============================================================================================

MÉTODO update

==============================================================================================*/

bool iBobParticle::update( double timeElapsed, const DynamicVector *pExternForces, Plane* pCollisionPlane )
{	
	if( !active )
		return false;

	bool collided = false;
	
#if ! DRAG_N_DROP_USES_FORCES
	if( !selected )
#endif
		position += speed * timeElapsed;

	if( !immune )
	{
		const float aux = timeElapsed / mass;

		if( pExternForces )
		{
			// Se partícula não é imune a forças, sofre a ação das forças recebidas como parâmetro
			
			const uint16 length = pExternForces->getUsedLength();
			for( uint16 i = 0; i < length; ++i )
			{
				Force *f = ( Force* ) pExternForces->getData( i );
				if( f && f->isActive() )
					speed += *( f->getDirection() ) * aux;
			}
		}

		#if DRAG_N_DROP_USES_FORCES

			// Aplica as forças de toque
			for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
			{
				if( touchForces[i] && touchForces[i]->isActive() && FDIF( touchForcesWeights[i], 0.0f ) )
					speed += *( touchForces[i]->getDirection() ) * touchForcesWeights[i] * aux;
			}

		#endif
		
		speed += force * aux;
	}
	
	#if BLOB_MODE
		speedBeforeCollision = speed;
	#endif

	// Verifica se passou de algum plano limite
	Plane *p = isOutsidePlanes();
	if( p )
	{
		if( pCollisionPlane )
			*pCollisionPlane = *p;

		position -= p->normal * ( ( position * p->normal ) - p->d );
		speed.set();

		#if ! DRAG_N_DROP_USES_FORCES
			if( !selected )
		#endif
				collided = true;
	}

	// Zera a força resultante
	force.set();

	return collided;
}

/*==============================================================================================

MÉTODO insertNeighbor

==============================================================================================*/

bool iBobParticle::insertNeighbor( iBobParticle* pParticle )
{
	if( pNeighbors )
		return pNeighbors->insert( pParticle );
	return false;
}

/*==============================================================================================

MÉTODO removeNeighbor

==============================================================================================*/

bool iBobParticle::removeNeighbor( uint16 index )
{
	if( pNeighbors )
		return pNeighbors->remove( index );
	return false;
}

/*==============================================================================================

MÉTODO getNeighborAt

==============================================================================================*/

iBobParticle* iBobParticle::getNeighborAt( uint16 index ) const
{
	if( pNeighbors )
		return ( iBobParticle* )pNeighbors->getData( index );
	return NULL;
}

/*==============================================================================================

MÉTODO decreaseTempCoils

==============================================================================================*/

void iBobParticle::decreaseTempCoils( void )
{
	// Se o número de molas temporárias chegou a 0, volta a considerar as forças aplicadas
	if( nTempCoils >= 1 )
	{
		--nTempCoils;
		if( nTempCoils == 0 )
			setImmune( false );
	}
}

/*==============================================================================================

MÉTODO destroyTempCoils
	Cancela todas as molas temporárias ligas à partícula.

==============================================================================================*/

void iBobParticle::destroyTempCoils( void )
{
	nTempCoils = 0;
	setImmune( false );
}

/*==============================================================================================

MÉTODO setStickingFactor

==============================================================================================*/

void iBobParticle::setStickingFactor( float factor )
{
	// Limita os valores de factor entre 0.0f e 1.0f
	factor = CLAMP( factor, 0.0f, 1.0f );
	stickingFactor = factor;
}

/*===========================================================================================

MÉTODO setTouchForceWeight
	Determina o quanto a força de toque "index" irá influenciar esta partícula.

============================================================================================*/

#if DRAG_N_DROP_USES_FORCES

void iBobParticle::setTouchForceWeight( uint8 touchForceIndex, float weight )
{
	touchForcesWeights[ touchForceIndex ] = weight;
}

#endif