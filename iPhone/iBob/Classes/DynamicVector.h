#ifndef DYNAMICVECTOR_H
#define DYNAMICVECTOR_H

/*===========================================================================================

CLASSE DynamicVector
	Implementa um vetor dinâmico genérico não ordenado. Só é permitido inserir novos
elementos no fim do vetor.

============================================================================================*/

#include <sys/types.h>	// uint32, ubyte, ...
#include <stddef.h>		// NULL
#include <limits.h>		// USHRT_MAX
#include <string.h>		// memset
#include <stdlib.h>		// realloc

#include "Macros.h"

#define DEFAULT_GROW_TAX 2.0f

class DynamicVector
{
	public:
		// Construtor
		DynamicVector( uint16 initialSlots = 1, float growTax = DEFAULT_GROW_TAX );
	
		// Destrutor
		virtual ~DynamicVector( void );

		// Insere os dados passados no último slot do vetor
		bool insert( void* const pData );

		// Remove os dados do slot com o índice passado como parâmetro
		bool remove( uint16 index );

		// Troca os slots do vetor
		bool switchSlots( uint16 slot1, uint16 slot2 );

		// Retorna os dados do slot com o índice passado como parâmetro
		const void* getData( uint16 index ) const;

		// Retorna o número de slots do vetor
		inline uint16 getLength( void ) const { return nSlots; };

		// Retorna o número de slots utilizados do vetor
		inline uint16 getUsedLength( void ) const { return nUsedSlots; };

	protected:

		// Ponteiro para o vetor din‚mico
		void **pVector;

		// Número de slots usados do vetor
		uint16 nUsedSlots;

		// Número total de slots do vetor
		uint16 nSlots;

		// Indica a taxa de crescimento dos slots
		const float growTax;
};

#endif