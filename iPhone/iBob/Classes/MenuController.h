//
//  MenuController.h
//  iBob
//
//  Created by Daniel Lopes Alves on 12/12/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef MENU_CONTROLLER_H
#define MENU_CONTROLLER_H

#import <UIKit/UIKit.h>

#include "Color.h"

#if TARGET_IPHONE_SIMULATOR
	#include "AccForce.h"
#endif

@interface MenuController : UITabBarController
{
	@private
		#if BLOB_MODE
			// Controlador da view onde é determinada a cor do personagem
			IBOutlet UIViewController* hColorsViewController;
		#endif

		// Controlador da view onde são determinadas as opções do jogo
		IBOutlet UIViewController* hOptionsViewController;
}

#if BLOB_MODE

// Getter e Setter para obter / determinar o controlador da view onde é determinada a cor do personagem
@property (nonatomic, assign ) UIViewController* hColorsViewController;

#endif

// Getter e Setter para obter / determinar o controlador da view onde são determinadas as opções do jogo
@property (nonatomic, assign ) UIViewController* hOptionsViewController;

#if TARGET_IPHONE_SIMULATOR
	// Retorna o multiplicador que devemos utilizar sobre as forças geradas pelos acelerômetros
	- ( float )getForceLevel;

	// Retorna o modo de cálculo a ser utilizado na interpretação dos dados dos acelerômetros
	- ( AccForceCalcMode )getMovementCalcMode;
#endif

#if BLOB_MODE
	// Retorna a cor a ser usada pelo personagem
	- ( Color* )getColor:( Color* )pColor;
#endif

// Retorna o coeficiente de consistência a ser usado pelo personagem
- ( float )getHardness;

// Retorna o coeficiente de aderência a ser usado pelo personagem
- ( float )getStickness;

// Retorna o volume do som
- ( float )getSoundVolume;

// Retorna a força da gravidade
- ( float )getGravity;

// Retorna se devemos reinicializar o estado do personagem
- ( bool )mustReset;

// Indica o estado do botão reset
- ( void )setResetValue:( bool ) value;

@end

#endif

