/*
 *  AccListener.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/9/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACC_LISTENER_H
#define ACC_LISTENER_H

#include "Point3f.h"

// Evita recursão de includes
class AccManager;

class AccListener
{
	public:
		// Recebe os eventos do acelerômetro
		virtual void OnAccelerate( const AccManager* pAccelerometer, const Point3f* pAcceleration ) = 0;
};

#endif
