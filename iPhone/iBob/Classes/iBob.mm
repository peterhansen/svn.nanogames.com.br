#include "iBob.h"
#include "Config.h"
#include "BobFullScene.h"

#if BLOB_MODE
	#include "BlobEye.h"
	#include "Texture2D.h"
#endif

#if DRAG_N_DROP_USES_FORCES
	#define DISSIPATION_FACTOR 0.50f
#endif

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

iBob::iBob( const Point3f *pPosition, const Point3f *pSpeed )
#if DRAG_N_DROP_USES_FORCES
		 : iBobParticle( NULL, pPosition, pSpeed ),
#else
		 : iBobParticle( pPosition, pSpeed ),
#endif

#if TARGET_IPHONE_SIMULATOR
		  drawTempCoils( false ), drawMode( DRAW_MODE_QUADS ),
#endif

#if COLLISION_SOUND
		  colliding( -1 ), alreadyPlayedSound( false ), noCollisionCounter( 0 ),
#endif
		  numberOfCoils( 0 ), orientation( 0.0f, 1.0f, 0.0f ), pCubeVertices( NULL ),
		  pTempCoils( NULL ), pCubeOriginalPositions( NULL ), pFacesParticles( NULL ), pCoils( NULL )
#if BLOB_MODE
		  , blobColor( BLOB_COLOR_DEFAULT_R, BLOB_COLOR_DEFAULT_G, BLOB_COLOR_DEFAULT_B, BLOB_COLOR_MIN_ALPHA )
#endif
{
	setImmune( true );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

iBob::~iBob( void )
{
	memset( sounds, 0, sizeof( AudioSource* ) * IBOB_N_SOUNDS );

	if( pFacesParticles )
	{
		for( int32 i = 0 ; i < facesParticlesLength ; ++i)
			SAFE_DELETE( pFacesParticles[i] );
			
		DESTROY_VEC( pFacesParticles );
	}

	SAFE_DELETE_VEC( pCoils );
	SAFE_DELETE( pCubeVertices );
	SAFE_DELETE( pCubeOriginalPositions );
	SAFE_DELETE( pTempCoils );
}

/*==============================================================================================

MÉTODO build

==============================================================================================*/

#if DRAG_N_DROP_USES_FORCES
bool iBob::build( TouchForce* touchForces[], float myDimension, uint8 myPointsByEdge, float tightness, float damping, float stickingFactor )
#else
bool iBob::build( float myDimension, uint8 myPointsByEdge, float tightness, float damping, float stickingFactor )
#endif
{
	if( !iBobParticle::build() )
		return false;

	dimension = myDimension;
	if( dimension <= 0.0f )
		dimension = 50.0f;

	nPointsByEdge = myPointsByEdge;

	const float halfSize = dimension / 2.0f;
	const float spaceBetweenPoints = dimension / (float) ( nPointsByEdge - 1 ); 

	// Anti-erro
	if( nPointsByEdge < 2 )
		nPointsByEdge = 2;

	// Aloca um vetor para armazenar os vértices que formarão o cubo
	const uint16 nFaceVertices = nPointsByEdge * nPointsByEdge;
	nParticles = nFaceVertices * nPointsByEdge;
	pCubeVertices = new DynamicVector( nParticles );
	if( !pCubeVertices )
		return false;

	pCubeOriginalPositions = new DynamicVector( nParticles );
	if( !pCubeOriginalPositions )
		return false;

	// Aloca um vetor para armazenar as molas temporárias criadas para fazer o grude com os planos de colisão
	pTempCoils = new DynamicVector( nParticles );
	if( !pTempCoils )
		return false;

	// Aloca um vetor para armazenar referências para os nós das faces do cubo
	uint16 facesVerticesCounter = 0;
	facesParticlesLength = 2*( ( nFaceVertices  ) + ( nPointsByEdge*( nPointsByEdge-2 ) ) + ( ( nPointsByEdge-2 )*( nPointsByEdge-2 ) ) );
	pFacesParticles = new iBobParticle*[ facesParticlesLength ];
	if( !pFacesParticles )
		return false;

	// Gera as partículas do cubo
	Point3f *pPos;
	iBobParticle *pParticle;
	for( uint16 z = 0 ; z < nPointsByEdge ; ++z )
	{
		for( uint16 y = 0 ; y < nPointsByEdge ; ++y )
		{
			for( uint16 x = 0 ; x < nPointsByEdge ; ++x )
			{
				pPos = new Point3f( position.x -halfSize+( x*spaceBetweenPoints ), position.y + halfSize - ( y*spaceBetweenPoints ), position.z - halfSize + ( z*spaceBetweenPoints ) );
				if( pPos )
				{
					if( !pCubeOriginalPositions->insert( pPos ) )
					{
						DESTROY( pPos );
						return false;
					}

					#if DRAG_N_DROP_USES_FORCES
						pParticle = new iBobParticle( touchForces, pPos, NULL, stickingFactor );
					#else
						pParticle = new iBobParticle( pPos, NULL, stickingFactor );
					#endif

					if( !pParticle || !pParticle->build() || !pCubeVertices->insert( pParticle ) )
					{
						SAFE_DELETE( pParticle );
						return false;
					}
					else
					{	
						if( ( z == 0 ||  z == nPointsByEdge-1 )
							|| ( y == 0 ||  y == nPointsByEdge-1 )
							|| ( x == 0 ||  x == nPointsByEdge-1 ) )
						{
							pParticle->setFaceParticle( true );
							pFacesParticles[ facesVerticesCounter++ ] = pParticle;
						}
					}
				}
				else
				{
					return false;
				}
			}
		}
	}

	// Aloca um vetor para armazenar as molas	
	#if USE_CENTER_PARTICLE
		numberOfCoils = ( ( nPointsByEdge-1 )*( nPointsByEdge )*( nPointsByEdge )*3 )+( 6*( nPointsByEdge-1 )*( nPointsByEdge-1 )*( nPointsByEdge ) ) + 8;
	#else
		numberOfCoils = ( ( nPointsByEdge-1 )*( nPointsByEdge )*( nPointsByEdge )*3 )+( 6*( nPointsByEdge-1 )*( nPointsByEdge-1 )*( nPointsByEdge ) );
	#endif
	pCoils = new iBobSpringCoil[ numberOfCoils ];
	if( !pCoils )
		return false;

	// Coloca as molas entre os vértices adjacentes
	uint16 aux, coilCounter = 0;
	iBobParticle *p1, *p2;

	// Eixo x
	for( uint16 z=0 ; z < nPointsByEdge ; z++ )
	{
		for( uint16 y=0 ; y < nPointsByEdge ; y++ )
		{
			for( uint16 x=0 ; x < nPointsByEdge-1 ; x++ )
			{
				aux = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

				p1 = ( iBobParticle* )pCubeVertices->getData( aux );
				p2 = ( iBobParticle* )pCubeVertices->getData( aux+1 );
				
				#if CONTROLLING_ORIENTATION
					pCoils[coilCounter].setOrientation( &orientation );
				#endif
				pCoils[coilCounter++].set( p1, p2, tightness, damping );

				if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
					return false;

				if ( y < nPointsByEdge - 1 )
				{
					p1 = ( iBobParticle* )pCubeVertices->getData( aux );
					p2 = ( iBobParticle* )pCubeVertices->getData( aux+1+nPointsByEdge );
					
					#if CONTROLLING_ORIENTATION
						pCoils[coilCounter].setOrientation( &orientation );
					#endif
					
					pCoils[coilCounter++].set( p1, p2, tightness, damping );
					
					if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
						return false;

					p1 = ( iBobParticle* )pCubeVertices->getData( aux+nPointsByEdge );
					p2 = ( iBobParticle* )pCubeVertices->getData( aux+1 );
					
					#if CONTROLLING_ORIENTATION
						pCoils[coilCounter].setOrientation( &orientation );
					#endif
					
					pCoils[coilCounter++].set( p1, p2, tightness, damping );
					
					if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
						return false;
				}	
			}
		}
	}
	
	// Eixo y
	for( uint16 z=0 ; z < nPointsByEdge ; z++ )
	{
		for( uint16 y=0 ; y < nPointsByEdge-1 ; y++ )
		{
			for( uint16 x=0 ; x < nPointsByEdge ; x++ )
			{
				aux = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

				p1 = ( iBobParticle* )pCubeVertices->getData( aux );
				p2 = ( iBobParticle* )pCubeVertices->getData( aux+nPointsByEdge );
					
				#if CONTROLLING_ORIENTATION
					pCoils[coilCounter].setOrientation( &orientation );
				#endif
					
				pCoils[coilCounter++].set( p1, p2, tightness, damping );
				
				if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
					return false;

				if ( z < nPointsByEdge - 1 )
				{
					p1 = ( iBobParticle* )pCubeVertices->getData( aux );
					p2 = ( iBobParticle* )pCubeVertices->getData( aux+nPointsByEdge+nFaceVertices );
					
					#if CONTROLLING_ORIENTATION
						pCoils[coilCounter].setOrientation( &orientation );
					#endif
					
					pCoils[coilCounter++].set( p1, p2, tightness, damping );
					
					if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
						return false;

					p1 = ( iBobParticle* )pCubeVertices->getData( aux+nPointsByEdge );
					p2 = ( iBobParticle* )pCubeVertices->getData( aux+nFaceVertices );
					
					#if CONTROLLING_ORIENTATION
						pCoils[coilCounter].setOrientation( &orientation );
					#endif
					
					pCoils[coilCounter++].set( p1, p2, tightness, damping );
					
					if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
						return false;
				}
			}
		}
	}

	// Eixo z
	for( uint16 z=0 ; z < nPointsByEdge-1 ; z++ )
	{
		for( uint16 y=0 ; y < nPointsByEdge ; y++ )
		{
			for( uint16 x=0 ; x < nPointsByEdge ; x++ )
			{
				aux = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

				p1 = ( iBobParticle* )pCubeVertices->getData( aux );
				p2 = ( iBobParticle* )pCubeVertices->getData( aux+nFaceVertices );
					
				#if CONTROLLING_ORIENTATION
					pCoils[coilCounter].setOrientation( &orientation );
				#endif
					
				pCoils[coilCounter++].set( p1, p2, tightness, damping );
				
				if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
					return false;

				if ( x < nPointsByEdge - 1 )
				{
					p1 = ( iBobParticle* )pCubeVertices->getData( aux );
					p2 = ( iBobParticle* )pCubeVertices->getData( aux+nFaceVertices+1 );
					
					#if CONTROLLING_ORIENTATION
						pCoils[coilCounter].setOrientation( &orientation );
					#endif
					
					pCoils[coilCounter++].set( p1, p2, tightness, damping );
					
					if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
						return false;

					p1 = ( iBobParticle* )pCubeVertices->getData( aux+nFaceVertices );
					p2 = ( iBobParticle* )pCubeVertices->getData( aux+1 );
					
					#if CONTROLLING_ORIENTATION
						pCoils[coilCounter].setOrientation( &orientation );
					#endif
					
					pCoils[coilCounter++].set( p1, p2, tightness, damping );
					
					if( !p1->insertNeighbor( p2 ) || !p2->insertNeighbor( p1 ) )
						return false;
				}

			}
		}
	}

#if USE_CENTER_PARTICLE
	// Cria molas entre as partículas mais internas do cuno e a partícula que irá determinar sua
	// posição
	const uint16 halfWay = ( nPointsByEdge >> 1 ) - 1;
	const uint16 firstZ = halfWay * nFaceVertices, lastZ = firstZ + nFaceVertices + 1;
	const uint16 firstY = halfWay * nPointsByEdge, lastY = firstY + nPointsByEdge + 1;
	const uint16 firstX = halfWay, lastX = halfWay + 2;
	
	for( uint16 z = firstZ ; z < lastZ ; z += nFaceVertices )
	{
		for( uint16 y = firstY ; y < lastY ; y += nPointsByEdge )
		{
			for( uint16 x = firstX ; x < lastX ; ++x )
			{
				p1 = ( iBobParticle* )pCubeVertices->getData( z + y + x );
	
				#if CONTROLLING_ORIENTATION
					pCoils[coilCounter].setOrientation( &orientation );
				#endif
					
				pCoils[coilCounter++].set( p1, this, tightness, damping );
				
				if( !p1->insertNeighbor( this ) || !insertNeighbor( p1 ) )
					return false;
			}
		}
	}
#endif
	
#if BLOB_MODE
	return setEyes( nFaceVertices );
#else
	return true;
#endif
}

/*==============================================================================================

MÉTODO setEyes
	Cria os olhos da gosma.

==============================================================================================*/

#if BLOB_MODE

bool iBob::setEyes( uint16 nFaceVertices )
{	
	// Cria os olhos
	BlobEye *pEye = new BlobEye();
	if( !pEye )
		return false;
	
	const uint16 index = ( ( nPointsByEdge-1 ) * nFaceVertices ) + 1;
	pEyes[0] = ( iBobParticle* )pCubeVertices->getData( index );
	pEyes[0]->setShape( pEye );
	
	pEyes[1] = ( iBobParticle* )pCubeVertices->getData( index + 1 );
	pEyes[1]->setShape( pEye );
	
	// Cria a textura dos olhos
	Texture2D *pEyesTexture = new Texture2D( NULL, 0 );
#ifdef TESTING_SOCCER_BALL
	if( !pEyesTexture || !pEyesTexture->loadTextureInFile( @"bt6.png" ) )
#else
	if( !pEyesTexture || !pEyesTexture->loadTextureInFile( @"eye.png" ) )
#endif
	{
		SAFE_DELETE( pEyesTexture );
		return false;
	}
	pEye->setTexture( pEyesTexture );
	
	return true;
}

#endif

/*==============================================================================================

MÉTODO render

==============================================================================================*/

void iBob::render( void )
{

#if TARGET_IPHONE_SIMULATOR
	switch( drawMode )
	{
		case DRAW_MODE_WIRED:
		{
			glDisable( GL_TEXTURE_2D );

			// Desenha as partículas
			glPointSize( 5 );
			glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
			
			float aux[6];
			const int32 length = pCubeVertices->getUsedLength();

			for( int32 i = 0; i < length; ++i )
			{
				const Point3f* pPos = ( ( Particle* ) pCubeVertices->getData( i ) )->getPosition();

				aux[0] = pPos->x;
				aux[1] = pPos->y;
				aux[2] = pPos->z;
				
				glVertexPointer( 3, GL_FLOAT, 0, aux );
				glDrawArrays( GL_POINTS, 0, 1 );
			}

			// Desenha as molas
			glLineWidth( 1 );
			glColor4f( 1.0f, 0.0f, 0.0f, 1.0f );
			
			for( int32 i = 0; i < numberOfCoils; ++i )
			{
				const Point3f* pPos = pCoils[i].p1->getPosition();
				
				aux[0] = pPos->x;
				aux[1] = pPos->y;
				aux[2] = pPos->z;
				
				pPos = pCoils[i].p2->getPosition();
				
				aux[3] = pPos->x;
				aux[4] = pPos->y;
				aux[5] = pPos->z;

				glVertexPointer( 3, GL_FLOAT, 0, aux );
				glDrawArrays( GL_LINES, 0, 2 );
			}
		}
		break;

		case DRAW_MODE_QUADS:
		{
#endif
			const Point3f *pVertice;
			uint16 temp;
			const uint16 nFaceVertices = nPointsByEdge*nPointsByEdge;
			const float xOffset = 0.25f / ( nPointsByEdge - 1 ),
						yOffset = 1.0f / ( nPointsByEdge - 1 );
			float offset = 0.5f, texCoord[8], vertices[12];
			
			#if BLOB_MODE
				glEnable( GL_BLEND );
				glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
			
				// Renderiza as shapes por fora, pois iremos renderizar os dados dos vértices das
				// partículas, não chamando o método render() das mesmas
				pEyes[0]->render();
				pEyes[1]->render();
			
				glDisable( GL_TEXTURE_2D );
				glEnableClientState( GL_COLOR_ARRAY );
				glShadeModel( GL_SMOOTH );
			
				Color colors[4] = { blobColor, blobColor, blobColor, blobColor };
				float percentAxis0, percentAxis1;
				float maxDist = ( nPointsByEdge - 1 ) * 0.5f;
			#else
				glEnable( GL_TEXTURE_2D );
				pShape->getTexture()->load();
				glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
			#endif

			for( uint16 z = 0 ; z < nPointsByEdge ; z+= nPointsByEdge-1 )
			{
				for( uint16 y = 0 ; y < nPointsByEdge-1 ; ++y )
				{
					for( uint16 x = 0 ; x < nPointsByEdge-1 ; ++x )
					{
						temp = ( z*nFaceVertices )+( y*nPointsByEdge )+x;
						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )y - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )x - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[0].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[0].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif

						texCoord[0] = offset + ( x * xOffset );
						texCoord[1] = y * yOffset;
						
						vertices[0] = pVertice->x;
						vertices[1] = pVertice->y;
						vertices[2] = pVertice->z;
						
						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nPointsByEdge ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )( y + 1 ) - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )x - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[1].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[1].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						texCoord[2] = texCoord[0];
						texCoord[3] = ( y + 1 ) * yOffset;

						vertices[3] = pVertice->x;
						vertices[4] = pVertice->y;
						vertices[5] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+1 ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )y - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )( x + 1 ) - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[2].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[2].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif

						texCoord[4] = offset + ( ( x + 1 ) * xOffset );
						texCoord[5] = texCoord[1];
						
						vertices[6] = pVertice->x;
						vertices[7] = pVertice->y;
						vertices[8] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nPointsByEdge+1 ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )( y + 1 ) - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )( x + 1 ) - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[3].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[3].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						texCoord[6] = texCoord[4];
						texCoord[7] = texCoord[3];

						vertices[ 9] = pVertice->x;
						vertices[10] = pVertice->y;
						vertices[11] = pVertice->z;
						
						#if BLOB_MODE
							glColorPointer( 4, GL_FLOAT, 0, colors );
						#endif
						glVertexPointer( 3, GL_FLOAT, 0, vertices );
						glTexCoordPointer( 2, GL_FLOAT, 0, texCoord );
						glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
					}
				}
				offset -= 0.5f;
			}

			#if ! BLOB_MODE
				glDisable( GL_TEXTURE_2D );
			#endif

			for( uint16 y=0 ; y < nPointsByEdge ; y+= nPointsByEdge-1 )
			{
				#if ! BLOB_MODE
					if( y == 0 )
						glColor4f( 1.0f, 0.95f, 0.32f, 1.0f );
					else
						glColor4f( 0.82f, 0.56f, 0.12f, 1.0f );
				#endif

				for( uint16 z=0 ; z < nPointsByEdge-1 ; ++z )
				{
					for( uint16 x=0 ; x < nPointsByEdge-1 ; ++x )
					{
						temp = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )z - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )x - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[0].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[0].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						vertices[0] = pVertice->x;
						vertices[1] = pVertice->y;
						vertices[2] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nFaceVertices ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )( z + 1 ) - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )x - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[1].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[1].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						vertices[3] = pVertice->x;
						vertices[4] = pVertice->y;
						vertices[5] = pVertice->z;
						
						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+1 ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )z - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )( x + 1 ) - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[2].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[2].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						vertices[6] = pVertice->x;
						vertices[7] = pVertice->y;
						vertices[8] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nFaceVertices+1 ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )( z + 1 ) - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )( x + 1 ) - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[3].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[3].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						vertices[ 9] = pVertice->x;
						vertices[10] = pVertice->y;
						vertices[11] = pVertice->z;

						#if BLOB_MODE
							glColorPointer( 4, GL_FLOAT, 0, colors );
						#endif
						glVertexPointer( 3, GL_FLOAT, 0, vertices );
						glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
					}
				}
			}

			#if ! BLOB_MODE
				glEnable( GL_TEXTURE_2D );
				glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );
			#endif
			
			offset = 0.25f;

			for( uint16 x=0 ; x < nPointsByEdge ; x+= nPointsByEdge-1 )
			{
				for( uint16 z=0 ; z < nPointsByEdge-1 ; ++z )
				{
					for( uint16 y=0 ; y < nPointsByEdge-1 ; ++y )
					{
						temp = ( z*nFaceVertices )+( y*nPointsByEdge )+x;
						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )y - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )z - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[0].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[0].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						texCoord[0] = offset + ( z * xOffset );
						texCoord[1] = y * yOffset;
						
						vertices[0] = pVertice->x;
						vertices[1] = pVertice->y;
						vertices[2] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nPointsByEdge ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )( y + 1 ) - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )z - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[1].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[1].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						texCoord[2] = texCoord[0];
						texCoord[3] = ( y + 1 ) * yOffset;
						
						vertices[3] = pVertice->x;
						vertices[4] = pVertice->y;
						vertices[5] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nFaceVertices ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )y - maxDist ) / maxDist;
							percentAxis1 = fabs( ( float )( z + 1 ) - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[2].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[2].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						texCoord[4] = offset + ( ( z + 1 ) * xOffset );
						texCoord[5] = texCoord[1];
						
						vertices[6] = pVertice->x;
						vertices[7] = pVertice->y;
						vertices[8] = pVertice->z;

						pVertice = ( ( iBobParticle* )pCubeVertices->getData( temp+nFaceVertices+nPointsByEdge ) )->getPosition();
						
						#if BLOB_MODE
							percentAxis0 = fabs( ( float )( y + 1 ) - maxDist ) / maxDist;;
							percentAxis1 = fabs( ( float )( z + 1 ) - maxDist ) / maxDist;
							#if INSIDE_OUT
								colors[3].a = BLOB_COLOR_TOTAL_ALPHA - LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#else
								colors[3].a = LERP( ( percentAxis0 + percentAxis1 ) * 0.5f, BLOB_COLOR_MIN_ALPHA, BLOB_COLOR_MAX_ALPHA );
							#endif
						#endif
						
						texCoord[6] = texCoord[4];
						texCoord[7] = texCoord[3];
						
						vertices[ 9] = pVertice->x;
						vertices[10] = pVertice->y;
						vertices[11] = pVertice->z;
						
						#if BLOB_MODE
							glColorPointer( 4, GL_FLOAT, 0, colors );
						#endif
						glVertexPointer( 3, GL_FLOAT, 0, vertices );
						glTexCoordPointer( 2, GL_FLOAT, 0, texCoord );
						glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
					}
				}
				offset += 0.5f;
			}

			#if BLOB_MODE
				glDisableClientState( GL_COLOR_ARRAY );			
			#else
				pShape->getTexture()->unload();
			#endif

#if TARGET_IPHONE_SIMULATOR
		}
		break;
	}

	// Desenha as molas temporárias de aderência
	if( drawTempCoils )
	{
		glLineWidth( 5 );
		glColor4f( 0.0f, 1.0f, 0.0f, 1.0f );
		
		float tempCoilsPos[6];
		const uint16 nTempCoils = pTempCoils->getUsedLength();
		for( int32 i = 0; i < nTempCoils; ++i )
		{
			SpringCoil *pTempCoil = ( SpringCoil* ) pTempCoils->getData( i );
			if( pTempCoil )
			{
				const Point3f *pP1 =  pTempCoil->p1->getPosition();
				const Point3f *pP2 =  pTempCoil->p2->getPosition();

				tempCoilsPos[0] = pP1->x;
				tempCoilsPos[1] = pP1->y;
				tempCoilsPos[2] = pP1->z;
				tempCoilsPos[3] = pP2->x;
				tempCoilsPos[4] = pP2->y;
				tempCoilsPos[5] = pP2->z;
				
				glVertexPointer( 3, GL_FLOAT, 0, tempCoilsPos );
				glDrawArrays( GL_LINES, 0, 2 );
			}
		}
		glLineWidth( 1 );
	}
#endif
}
	
/*==============================================================================================

MÉTODO update

==============================================================================================*/

bool iBob::update( double timeElapsed, const DynamicVector *pExternForces, Plane* pCollisionPlane )
{
	// Atualiza as molas do cubo
	for( uint16 i = 0; i < numberOfCoils; ++i )
		pCoils[i].update( timeElapsed );

	// Atualiza as molas temporárias que causam a aderência do cubo
	uint16 nCoils = pTempCoils->getUsedLength();
	for( uint16 i = 0; i < nCoils ; )
	{
		SpringCoil *pCoil = ( SpringCoil* ) pTempCoils->getData( i );

		// Se a mola arrebentou...
		if( pCoil && pCoil->update( timeElapsed ) )
		{
			// Decrementa o número de molas ligadas às partículas
			( ( iBobParticle* )pCoil->p1 )->decreaseTempCoils();
			( ( iBobParticle* )pCoil->p2 )->decreaseTempCoils();

			// Retira a mola do vetor de molas temporárias
			pTempCoils->remove( i );
			--nCoils;
		}
		else
		{
			++i;
		}
	}

	// Verifica se deve criar novas molas temporárias. Testa apenas as partículas das faces do cubo
	#if COLLISION_SOUND
		uint16 nCollidingParticles = 0;
		uint16 planeCounter[ N_COLLISION_PLANES ];
		memset( planeCounter, 0, sizeof( uint16 ) * N_COLLISION_PLANES );
	#endif
	
	const uint16 length = pCubeVertices->getUsedLength();
	for( uint16 i = 0; i < length; ++i )
	{
		iBobParticle *p = ( iBobParticle* ) pCubeVertices->getData( i );

		// Se colidiu...
		#if COLLISION_SOUND
		Plane plane;
		if( p->update( timeElapsed, pExternForces, &plane ) )
		#else
		if( p->update( timeElapsed, pExternForces ) )
		#endif
		{
			#if COLLISION_SOUND
				// Incrementa o contador do número de colisões no plano
				for( uint8 i = 0 ; i < N_COLLISION_PLANES ; ++i )
				{
					if( *(( Plane* )pLimitPlanes->getData( i )) == plane )
					{
						++planeCounter[i];
						break;
					}
				}
				
				// Incrementa o contador do número de partículas que estão colidindo
				++nCollidingParticles;
			#endif
			
			if( p->isFaceParticle() )
			{
				#if BLOB_MODE
					// Aciona um emissor de partículas no lugar da colisão
					pCollisionListener->onCollision( p );
				#endif

				// Cria as molas temporárias que irão causar a aderência do cubo
				createTemporaryCoils( p );
			}
		}
	}
	
	#if COLLISION_SOUND
		// Procura o plano que sofreu o maior número de colisões
		uint16 maxCollisionsPerPlane = 0;
		Plane currCollisionPlane;
		for( uint8 i = 0 ; i < N_COLLISION_PLANES ; ++i )
		{
			if( planeCounter[i] > maxCollisionsPerPlane )
			{
				maxCollisionsPerPlane = planeCounter[i];
				currCollisionPlane = *( ( Plane* )pLimitPlanes->getData( i ) );
			}
		}
		
		// Evita erros de precisão da colisão
		if( nCollidingParticles < MIN_COLLIDING_PARTICLES )
		{
			if( noCollisionCounter < MIN_NO_COLLISION_COUNTER )
			{
				++noCollisionCounter;
			}
			else
			{
				colliding = 0;
				alreadyPlayedSound = false;
			}
		}
		else
		{
			// Se mudou de plano de colisão, terá q tocar o som novamente
			if( currCollisionPlane != lastCollisionPlane )
			{
				alreadyPlayedSound = false;
				lastCollisionPlane = currCollisionPlane;
			}
			
			noCollisionCounter = 0;
			colliding = 1;
		}
	#endif

	// Atualiza a posição do cubo
	recalcPosAndSpeed();
	
	// Determina a posição das fontes de som
	updateAudioSources();
	
	// Verifica se devemos tocar o som do berro
	const float speedModule = speed.getModule();
	if(( speedModule > SCREAM_MIN_SPEED_MODULE ) && ( sounds[ IBOB_SOUND_INDEX_SCREAM ]->getState() != AL_PLAYING ) )
	{
		Point3f aux( speed.x, speed.y, CLAMP( speed.z, SCREAM_Z_MIN_SPEED, SCREAM_Z_MAX_SPEED ) );
		sounds[ IBOB_SOUND_INDEX_SCREAM ]->setSpeed( &aux );
		sounds[ IBOB_SOUND_INDEX_SCREAM ]->play();
	}
	
	#if COLLISION_SOUND
		// Verifica se devemos tocar o som de colisão
		if( ( colliding == 1 )
		   && ( !alreadyPlayedSound )
		   && ( speedModule >= COLLISION_SOUND_MIN_SPEED )
		   && ( sounds[ IBOB_SOUND_INDEX_COLLISION ]->getState() != AL_PLAYING ) )
		{
			alreadyPlayedSound = true;
			const float speedPercent = (( speedModule > COLLISION_SOUND_MAX_SPEED ? COLLISION_SOUND_MAX_SPEED : speedModule ) - COLLISION_SOUND_MIN_SPEED )/ ( COLLISION_SOUND_MAX_SPEED - COLLISION_SOUND_MIN_SPEED );
			sounds[ IBOB_SOUND_INDEX_COLLISION ]->setGain( LERP( speedPercent, COLLISION_SOUND_MIN_GAIN, COLLISION_SOUND_MAX_GAIN ) );
			sounds[ IBOB_SOUND_INDEX_COLLISION ]->play();
		}
	#endif
						
	#if CONTROLLING_ORIENTATION			
		// Calcula a nova orientação do iBob no mundo 3D
		setOrientation();
	#endif

	return true;
}

/*==============================================================================================

MÉTODO insertLimitPlane

==============================================================================================*/

int16 iBob::insertLimitPlane( const Plane* pLimitPlane )
{
	const uint16 length = pCubeVertices->getUsedLength();

	for( uint16 i = 0; i < length; ++i )
	{
		if( ( ( Particle* ) pCubeVertices->getData( i ) )->insertLimitPlane( pLimitPlane ) < 0 )
			return -1;
	}

	return Particle::insertLimitPlane( pLimitPlane );
}

/*==============================================================================================

MÉTODO setDrawMode

==============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

void iBob::setDrawMode( uint8 mode )
{
	drawMode = mode;
}

#endif

/*==============================================================================================

MÉTODO setStickingFactor

==============================================================================================*/

void iBob::setStickingFactor( float factor )
{
	const uint16 length = pCubeVertices->getUsedLength();
	for( uint16 i = 0; i < length; ++i )
		(( iBobParticle* )pCubeVertices->getData( i ) )->setStickingFactor( factor );
}

/*==============================================================================================

MÉTODO getStickingFactor

==============================================================================================*/

float iBob::getStickingFactor( void )
{
	return ( ( iBobParticle* )pCubeVertices->getData( 0 ) )->getStickingFactor();
}

/*==============================================================================================

MÉTODO setTightness

==============================================================================================*/

void iBob::setTightness( float tightness )
{
	for( int32 i = 0; i < numberOfCoils; ++i )
		pCoils[i].tightness = tightness;
}

/*==============================================================================================

MÉTODO setDampingFactor

==============================================================================================*/

void iBob::setDampingFactor( float factor )
{
	for( int32 i = 0; i < numberOfCoils; ++i )
		pCoils[i].dampingFactor = factor;
}

/*==============================================================================================

MÉTODO createTemporaryCoils

==============================================================================================*/

bool iBob::createTemporaryCoils( iBobParticle* pParticle )
{
	uint16 totalTempCoils = pTempCoils->getUsedLength();
	if( totalTempCoils >= MAX_TEMP_COILS )
		return true;
	
	uint16 nTempCoils = pParticle->getNTempCoils();
	const uint16 nNeighbors = pParticle->getNNeighbors();
	for( uint16 i=0 ; i < nNeighbors && nTempCoils < MAX_TEMP_COILS_PER_PARTICLE ; ++i )
	{
		const float speed = ( *( pParticle->getSpeed() ) ).getModule();
		
		const float stickness = speed * pParticle->getStickingFactor();
		SpringCoil *pNewCoil = new SpringCoil( 1.5, true, pParticle, pParticle->getNeighborAt( i ), false, stickness, stickness, TEMP_COILS_LIFE_BOOSTER * pParticle->getStickingFactor() );
		if( !pNewCoil || !pTempCoils->insert( pNewCoil ) )
		{
			SAFE_DELETE( pNewCoil );
			return false;
		}
		else
		{
			pParticle->increaseTempCoils();
			nTempCoils = pParticle->getNTempCoils();
		}
	}
	// Não tem problema se não entrarmos no for e deixarmos a partícula imune, pois não entrar no for significa
	// que já existem molas temporárias, logo a partícula está imune
	pParticle->setImmune( true );

	return true;
}

/*==============================================================================================

MÉTODO setSpeed

==============================================================================================*/

void iBob::setSpeed( const Point3f* pSpeed )
{
	Particle::setSpeed( pSpeed );
	
	const uint16 length = pCubeVertices->getUsedLength();
	for ( uint16 i = 0; i < length; ++i )
		( ( iBobParticle* )pCubeVertices->getData( i ) )->setSpeed( pSpeed );
}

/*==============================================================================================

MÉTODO recalcPosAndSpeed
	Recalcula a posição e a velocidade do cubo de acordo com a posição de suas partículas.

==============================================================================================*/

void iBob::recalcPosAndSpeed( void )
{
	Point3f pos, spd;
	const uint16 length = pCubeVertices->getUsedLength();
	for( uint16 i = 0 ; i < length ; ++i )
	{
		pos += *((( iBobParticle* )pCubeVertices->getData( i ))->getPosition());
		spd += *((( iBobParticle* )pCubeVertices->getData( i ))->getSpeed());
	}

	pos /= length;
	spd /= length;

	iBobParticle::setPosition( &pos );
	iBobParticle::setSpeed( &spd );
}

/*==============================================================================================

MÉTODO setPosition

==============================================================================================*/

void iBob::setPosition( const Point3f *pPosition )
{
	Point3f aux;
	if( pPosition )
		aux = *pPosition;
		
	// Movimenta todas as partículas do cubo
	const Point3f movement = aux - position;
	const uint16 length = pCubeVertices->getUsedLength();

	for( uint16 i = 0; i < length; ++i )
	{
		iBobParticle *p = ( iBobParticle* ) pCubeVertices->getData( i );
		p->move( &movement );
		p->setSpeed( NULL );
	}

	// Recalcula a posição global do objeto
	recalcPosAndSpeed();
	
	// Determina a posição das fontes de som
	updateAudioSources();
	
	// Zera a velocidade do objeto
	setSpeed( NULL );
}

/*==============================================================================================

MÉTODO reset

==============================================================================================*/

void iBob::reset( void )
{
	force.set();
	setSpeed( NULL );
	setPosition( NULL );

	while( pTempCoils->getUsedLength() > 0 )
		pTempCoils->remove( 0 );

	const uint16 length = pCubeVertices->getUsedLength();
	for( uint16 i = 0; i < length; ++i )
	{
		iBobParticle *p = ( iBobParticle* ) pCubeVertices->getData( i );
		p->force.set();
		p->setSpeed( NULL );
		p->destroyTempCoils();
	}
}

/*==============================================================================================

MÉTODO checkCollision
	Verifica se o iBob sofreu colisão com o raio. Retorna o índice da partícula que sofreu a
colisão ou um número negativo caso não haja interseção.

==============================================================================================*/

int16 iBob::checkCollision( const Ray* pRay ) const
{
	// Checa colisão apenas com os triângulos das faces do cubo 
	const uint8 aux = nPointsByEdge - 1;
	const uint16 nFaceVertices = nPointsByEdge * nPointsByEdge;
	
	int16 particleIndex = -1, temp[3];
	float shortestDist = FLT_MAX, distToCamera;
	
	Triangle tri;
	Point3f collisionPoint;

	// Faces frontal e traseira
	for( uint16 z = 0 ; z < nPointsByEdge ; z += aux )
	{
		for( uint16 y = 0 ; y < aux ; ++y )
		{
			for( uint16 x = 0 ; x < aux ; ++x )
			{
				temp[0] = ( z * nFaceVertices ) + ( y * nPointsByEdge ) + x;
				temp[1] = temp[0] + nPointsByEdge;
				temp[2] = temp[0] + 1;

				tri.vertexes[0] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[0] ) )->getPosition() );
				tri.vertexes[1] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[1] ) )->getPosition() );
				tri.vertexes[2] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[2] ) )->getPosition() );

				if( Utils::IntersectRayTriangle( false, pRay, &tri, &distToCamera, &collisionPoint.x, &collisionPoint.y ) )
				{
					if( distToCamera < shortestDist )
					{
						shortestDist = distToCamera;
						particleIndex = temp[ getNearestVertex( &tri, tri.getWithBarycentricCoords( &collisionPoint, collisionPoint.x, collisionPoint.y ) ) ];
					}
				}
				
				temp[0] = temp[1] + 1;
				tri.vertexes[0] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[0] ) )->getPosition() );
				
				if( Utils::IntersectRayTriangle( false, pRay, &tri, &distToCamera, &collisionPoint.x, &collisionPoint.y ) )
				{
					if( distToCamera < shortestDist )
					{
						shortestDist = distToCamera;
						particleIndex = temp[ getNearestVertex( &tri, tri.getWithBarycentricCoords( &collisionPoint, collisionPoint.x, collisionPoint.y ) ) ];
					}
				}
				
			}
		}
	}
	
	// Faces superior e inferior
	for( uint16 y=0 ; y < nPointsByEdge ; y += aux )
	{
		for( uint16 z=0 ; z < aux ; ++z )
		{
			for( uint16 x=0 ; x < aux ; ++x )
			{
				temp[0] = ( z * nFaceVertices ) + ( y * nPointsByEdge ) + x;
				temp[1] = temp[0] + nFaceVertices;
				temp[2] = temp[0] + 1;

				tri.vertexes[0] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[0] ) )->getPosition() );
				tri.vertexes[1] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[1] ) )->getPosition() );
				tri.vertexes[2] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[2] ) )->getPosition() );
				
				if( Utils::IntersectRayTriangle( false, pRay, &tri, &distToCamera, &collisionPoint.x, &collisionPoint.y ) )
				{
					if( distToCamera < shortestDist )
					{
						shortestDist = distToCamera;
						particleIndex = temp[ getNearestVertex( &tri, tri.getWithBarycentricCoords( &collisionPoint, collisionPoint.x, collisionPoint.y ) ) ];
					}
				}
				
				temp[0] = temp[1] + 1;
				tri.vertexes[0] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[0] ) )->getPosition() );
				
				if( Utils::IntersectRayTriangle( false, pRay, &tri, &distToCamera, &collisionPoint.x, &collisionPoint.y ) )
				{
					if( distToCamera < shortestDist )
					{
						shortestDist = distToCamera;
						particleIndex = temp[ getNearestVertex( &tri, tri.getWithBarycentricCoords( &collisionPoint, collisionPoint.x, collisionPoint.y ) ) ];
					}
				}
			}
		}
	}

	// Faces esquerda e direita
	for( uint16 x=0 ; x < nPointsByEdge ; x += aux )
	{
		for( uint16 z=0 ; z < aux ; ++z )
		{
			for( uint16 y=0 ; y < aux ; ++y )
			{
				temp[0] = ( z * nFaceVertices ) + ( y * nPointsByEdge ) + x;
				temp[1] = temp[0] + nPointsByEdge;
				temp[2] = temp[0] + nFaceVertices;

				tri.vertexes[0] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[0] ) )->getPosition() );
				tri.vertexes[1] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[1] ) )->getPosition() );
				tri.vertexes[2] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[2] ) )->getPosition() );
				
				if( Utils::IntersectRayTriangle( false, pRay, &tri, &distToCamera, &collisionPoint.x, &collisionPoint.y ) )
				{
					
					if( distToCamera < shortestDist )
					{
						shortestDist = distToCamera;
						particleIndex = temp[ getNearestVertex( &tri, tri.getWithBarycentricCoords( &collisionPoint, collisionPoint.x, collisionPoint.y ) ) ];
					}
				}
				
				temp[0] = temp[1] + nFaceVertices;
				tri.vertexes[0] = *( ( ( iBobParticle* )pCubeVertices->getData( temp[0] ) )->getPosition() );
				
				if( Utils::IntersectRayTriangle( false, pRay, &tri, &distToCamera, &collisionPoint.x, &collisionPoint.y ) )
				{
					if( distToCamera < shortestDist )
					{
						shortestDist = distToCamera;
						particleIndex = temp[ getNearestVertex( &tri, tri.getWithBarycentricCoords( &collisionPoint, collisionPoint.x, collisionPoint.y ) ) ];
					}
				}
			}
		}
	}
	return particleIndex;
}

/*==============================================================================================

MÉTODO getNearestVertex
	Retorna qual vértice do triângulo está mais próximo do ponto recebido como parâmetro.

==============================================================================================*/

uint8 iBob::getNearestVertex( const Triangle* pTriangle, const Point3f* pPoint )
{
	const float dist0 = ( pTriangle->vertexes[0] - *pPoint ).getModule();
	const float dist1 = ( pTriangle->vertexes[1] - *pPoint ).getModule();
	const float dist2 = ( pTriangle->vertexes[2] - *pPoint ).getModule();
	
	if( dist0 < dist1 )
		return dist0 < dist2 ? 0 : 2;
	else
		return dist1 < dist2 ? 1 : 2;
}

/*==============================================================================================

MÉTODO distributeForce
	Distribui uma força pelas partículas do cubo, dissipando-a conforme as partículas vão se
afastando do ponto inicial.

==============================================================================================*/

#if DRAG_N_DROP_USES_FORCES

void iBob::distributeForce( uint8 firstParticleIndex, uint8 forceIndex )
{
	// Reseta o estado dos nós do grafo
	const uint16 length = pCubeVertices->getUsedLength();
	for( uint16 i = 0; i < length; ++i )
		(( iBobParticle* ) pCubeVertices->getData( i ))->setVisited( false );
		
	// Visita o "primeiro" nó do grafo
	iBobParticle* pFirstParticle = ( iBobParticle* )pCubeVertices->getData( firstParticleIndex );

	// Indica que a patícula foi visitada
	pFirstParticle->setVisited( true );
		
	// Insere a força no array de forças da partícula
	pFirstParticle->setTouchForceWeight( forceIndex, 1.0f );
	
	// Percorre o restante do grafo em profunidade
	graphIterator( pFirstParticle, forceIndex, 1.0f );
}

#endif

/*==============================================================================================

MÉTODO graphIterator
	Percorre o grafo em profunidade.

==============================================================================================*/

#if DRAG_N_DROP_USES_FORCES

void iBob::graphIterator( const iBobParticle* pParticle, uint8 touchForceIndex, float weight )
{
	// Aplica o fator de dissipação na força recebida
	const float halvedWeight = weight * DISSIPATION_FACTOR;
	
	// Percorre as partículas adjacentes a esta partícula
	bool visitedSomeone = false;
	const uint16 nNeighbors = pParticle->getNNeighbors();
	for( uint16 i= 0 ; i < nNeighbors ; ++i )
	{
		iBobParticle* pNeighbor = pParticle->getNeighborAt( i );

		if( !pNeighbor->wasVisited() )
		{
			visitedSomeone = true;
			pNeighbor->setVisited( true );
			pNeighbor->setTouchForceWeight( touchForceIndex, halvedWeight );
		}
	}
	// Recursão
	if( visitedSomeone )
		graphIterator( pParticle->getNeighborAt( 0 ), touchForceIndex, halvedWeight );
}

#endif

/*==============================================================================================

MÉTODO selectParticle
	Informa que a partícula passada como parâmetro está selecionada e deve parar de sofrer
a atuação das forças físicas.

==============================================================================================*/

#if ! DRAG_N_DROP_USES_FORCES

void iBob::selectParticle( iBobParticle* pParticle )
{
	// Retira as molas temporárias que causam a aderência da partícula
	uint16 nCoils = pTempCoils->getUsedLength();
	for( uint16 i = 0; i < nCoils ; )
	{
		SpringCoil *pCoil = ( SpringCoil* ) pTempCoils->getData( i );

		// Se a partícula faz parte da mola...
		if( ( pCoil->p1 == pParticle ) || ( pCoil->p2 == pParticle ))
		{
			// Decrementa o número de molas ligadas às partículas
			( ( iBobParticle* )pCoil->p1 )->decreaseTempCoils();
			( ( iBobParticle* )pCoil->p2 )->decreaseTempCoils();

			// Retira a mola do vetor de molas temporárias
			pTempCoils->remove( i );
			--nCoils;
		}
		else
		{
			++i;
		}
	}
	
	// Indica que a partícula não deve sofrer a ação de forças físicas
	pParticle->setImmune( true );
	
	// Indica que a partícula está selecionada
	pParticle->setSelected( true );
}

#endif

/*==============================================================================================

MÉTODO unselectParticle
	Informa que a partícula passada como parâmetro não está mais selecionada e deve voltar a
sofrer a atuação das forças físicas.

==============================================================================================*/

#if ! DRAG_N_DROP_USES_FORCES

void iBob::unselectParticle( iBobParticle* pParticle )
{
	if( pParticle )
	{
		pParticle->setImmune( false );
		pParticle->setSelected( false );
	}
}

#endif

/*==============================================================================================

MÉTODO setOrientation
	Calcula a orientação do iBob no mundo 3D.

==============================================================================================*/

#if CONTROLLING_ORIENTATION

void iBob::setOrientation( void )
{
	const uint8 nFaceVertices = nPointsByEdge * nPointsByEdge;
	const uint8 particleAxisIndex = ( nPointsByEdge >> 1 ) - ( ~nPointsByEdge & 0x1 );
	const uint8 particleIndex = particleAxisIndex * ( nFaceVertices + nPointsByEdge + 1 );

	const Point3f p0 = *(( iBobParticle* )pCubeVertices->getData( particleIndex ))->getPosition();
	const Point3f p1 = *(( iBobParticle* )pCubeVertices->getData( particleIndex + 1 ))->getPosition();
	const Point3f p2 = *(( iBobParticle* )pCubeVertices->getData( particleIndex + nFaceVertices ))->getPosition();
	
	orientation = ( p1 - p0 ) % ( p2 - p0 );
	orientation.normalize();
}

#endif

/*==============================================================================================

MÉTODO updateAudioSources
	Determina a posição das fontes de som.

==============================================================================================*/

void iBob::updateAudioSources( void )
{
	// Iremos nos importar apenas com a profundidade do personagem na cena
	Point3f aux = *( getPosition() );
	aux.x = 0.0f;
	aux.y = 0.0f;

	for( uint8 i = 0 ; i < IBOB_N_SOUNDS ; ++i )
		sounds[i]->setPosition( &aux );
}

/*==============================================================================================

MÉTODO shrinkCoils
	Diminui o comprimento mínimo das molas que constituem o personagem. Assim fazemos com que
ele diminua de tamanho.

==============================================================================================*/

// TASK : SHRINK
//#if BLOB_MODE
//
//void iBob::shrinkCoils( void )
//{
//	for( uint16 i = 0 ; i < numberOfCoils ; ++i )
//		pCoils[i].restLength = SHRINK_FACTOR * pCoils[i].restLength;
//}
//
//#endif

/*==============================================================================================

MÉTODO onCollision
	Método chamado quando uma das partículas que constitui o personagem sofre colisão com um dos
planos da cena.

==============================================================================================*/

//void iBob::onCollision( void )
//{
//#if BLOB_MODE
	// TASK : SHRINK
	//shrinkCoils();
//#endif
//}

