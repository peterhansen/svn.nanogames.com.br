#ifndef NANO_RANDOM
#define NANO_RANDOM

#include "NanoTypes.h"

class Random
{
	public:
		// Determina o inicializador do gerador de números randômicos
		static void SetSeed( uint32 s );
	
		// Obtém um número randômico real 
		static float NextFloat( void );
	
		// Obtém um número randômico real no intervalo [0.0f, maxValue]
		// Equivale a chamar NextDouble( 0.0f, maxValue )
		static float NextFloat( float maxValue );
	
		// Obtém um número randômico real no intervalo definido 
		static float NextFloat( float minValue, float maxValue );
	
	private:
		// Indica se um inicializador já foi passado para o gerador de números randômicos
		static bool seeded;
};

#endif