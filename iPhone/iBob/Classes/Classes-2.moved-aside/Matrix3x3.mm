#include "Matrix3x3.h"
#include "Macros.h"

#define A m[0][0]
#define B m[0][1]
#define C m[0][2]
#define D m[1][0]
#define E m[1][1]
#define F m[1][2]
#define G m[2][0]
#define H m[2][1]
#define I m[2][2]

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Matrix3x3::Matrix3x3( const double* pValues )
{
	set( pValues );
}

/*===========================================================================================

MÉTODO set
	Preenche a matriz com os valores recebidos como parâmetro.

============================================================================================*/

void Matrix3x3::set( const double* pValues )
{
	memcpy( m, pValues, sizeof( double ) * 9 );
}

/*===========================================================================================

MÉTODO inverse
	Retorna a matriz inversa da matriz recebida como parâmetro.

============================================================================================*/

Matrix3x3* Matrix3x3::inverse( Matrix3x3* pInv )
{
	if( !pInv )
		return NULL;

	adjunta( pInv );
	
	( *pInv ) /= determinant();
	return pInv;
}

/*===========================================================================================

MÉTODO determinant
	Retorna o determinante da matriz.

============================================================================================*/

double Matrix3x3::determinant( void )
{
	return ( A * E * I ) + ( B * F * G ) + ( C * D * H ) - ( A * F * H ) - ( B * D * I ) - ( C * E * G );
}

/*===========================================================================================

MÉTODO adjunta
	Retorna a matriz adjunta.

============================================================================================*/

Matrix3x3* Matrix3x3::adjunta( Matrix3x3* pAdj )
{
	const double aux[] = {	  (( E * I ) - ( F * H ) ), -(( B * I ) - ( C * H )),  (( B * F ) - ( C * E )),
							 -(( D * I ) - ( F * G ) ),  (( A * I ) - ( C * G )), -(( A * F ) - ( C * D )),
							  (( D * H ) - ( E * G ) ), -(( A * H ) - ( B * G )),  (( A * E ) - ( B * D )) };
	pAdj->set( aux );
	return pAdj;
}

/*===========================================================================================

OPERADOR /=

============================================================================================*/

Matrix3x3& Matrix3x3::operator/=( double div )
{
	double *p = ( double* )&m;
	for( uint8 i = 0 ; i < 9 ; ++i )
		p[i] /= div;
	return ( *this );
}
