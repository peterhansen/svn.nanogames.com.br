/*
 *  Matrix.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/24/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MATRIX3X3_H
#define MATRIX3X3_H

class Matrix3x3
{
	public:
		// Construtor
		Matrix3x3( const double* pValues );
	
		// Destrutor
		virtual ~Matrix3x3( void ){};
	
		// Preenche a matriz com os valores recebidos como parâmetro.
		void set( const double* pValues );
	
		// Retorna a matriz inversa da matriz recebida como parâmetro
		Matrix3x3* inverse( Matrix3x3* pInv );
	
		// Retorna o determinante da matriz
		double determinant( void );
	
		// Retorna a matriz adjunta
		Matrix3x3* adjunta( Matrix3x3* pAdj );
	
		// OPERADORES
	
		Matrix3x3& operator/=( double div );
	
	private:	
		// Matriz
		double m[3][3];
};

#endif