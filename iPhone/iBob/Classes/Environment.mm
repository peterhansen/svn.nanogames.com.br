#include "Environment.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Environment::Environment( void ) : pForces( NULL ), pParticles( NULL )
{
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

Environment::~Environment( void )
{
	SAFE_DELETE( pForces );
	SAFE_DELETE( pParticles );
}

/*===========================================================================================

MÉTODO insertParticle

============================================================================================*/

int16 Environment::insertParticle( Particle* pParticle )
{
	if( !pParticles )
	{
		pParticles = new DynamicVector();
		if( !pParticles )
			return -1;
	}
	pParticles->insert( pParticle );
	return pParticles->getUsedLength() - 1;
}

/*===========================================================================================

MÉTODO removeParticleAt

============================================================================================*/

bool Environment::removeParticleAt( uint16 index )
{
	if( pParticles )
		return pParticles->remove( index );
	return false;
}

/*===========================================================================================

MÉTODO removeForceAt

============================================================================================*/

int16 Environment::insertForce( Force* pForce )
{
	if( !pForces )
	{
		pForces = new DynamicVector();
		if( !pForces )
			return -1;
	}
	pForces->insert( pForce );
	return pForces->getUsedLength() - 1;
}

/*===========================================================================================

MÉTODO removeForceAt

============================================================================================*/

bool Environment::removeForceAt( uint16 index )
{
	if( pForces )
		return pForces->remove( index );
	return false;
}

/*===========================================================================================

MÉTODO update

============================================================================================*/

void Environment::update( double timeElapsed )
{
	// Atualiza as forças e calcula a força resultante
	if( pForces )
	{
		const uint16 pForcesLength = pForces->getUsedLength();
		for( uint16 i = 0; i < pForcesLength; ++i )
		{
			Force *pForce = ( Force* )pForces->getData( i );
			if( pForce )
				pForce->update( timeElapsed );
		}
	}

	// Atualiza as partículas
	if( pParticles )
	{
		const uint16 pParticlesLength = pParticles->getUsedLength();
		uint16 activeParticles = pParticlesLength;
		for( uint16 i = 0; i < pParticlesLength; ++i )
		{
			Particle* p = ( Particle* ) pParticles->getData(i);
			if( p && !p->update( timeElapsed, pForces ) )
			{
				pParticles->switchSlots( i, activeParticles-1 );
				--activeParticles;
			}
		}
	}
}

/*===========================================================================================

MÉTODO render

============================================================================================*/

void Environment::render( void )
{
	if( pParticles )
	{
		const uint16 pParticlesLength = pParticles->getUsedLength();

		for( uint16 i = 0; i < pParticlesLength; ++i )
		{
			Particle* p = ( Particle* ) pParticles->getData( i );
#if TARGET_IPHONE_SIMULATOR
			const char* pParticleName = p->getParticleName();
#endif
			if( p )
				p->render();
		}
	}
}
