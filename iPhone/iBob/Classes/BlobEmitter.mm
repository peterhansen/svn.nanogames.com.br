#if BLOB_MODE

#include "BlobEmitter.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

BlobEmitter::BlobEmitter( void ) : Emitter()
{
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/	

bool BlobEmitter::update( double timeElapsed, const DynamicVector* pExternForces )
{
	if( isActive() )
	{
		// Se o emissor morreu...
		if( !Emitter::update( timeElapsed, pExternForces ) )
		{
//			// Fica invisível
//			setVisible( false );
//			
//			// E é desativado
//			setActive( false );
			
			return false;
		}
	}
	return true;
}

#endif
