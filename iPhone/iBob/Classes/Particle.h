#ifndef PARTICLE_H
#define PARTICLE_H

/*==============================================================================================

CLASSE Particle

==============================================================================================*/

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include "Shape.h"
#include "DynamicVector.h"
#import "Random.h"
#include "Plane.h"
#include "Node.h"
#include "Macros.h"

// Tempo de vida padrão de uma partícula
#define MAX_DEFAULT_LIFE 30000.0f

// Massa padrão de uma partícula
#define MAX_DEFAULT_MASS 80.0f

// Tamanho padrão de uma partícula
#define MAX_DEFAULT_SIZE 6.0f

#if TARGET_IPHONE_SIMULATOR
	#define PARTICLE_NAME_LENGTH 128 
#endif

class Particle : public Node
{
	public:
		// Força resultante
		Point3f force;

		// Construtor
		Particle( const Point3f* pPosition = NULL, const Point3f* pSpeed = NULL, float life = MAX_DEFAULT_LIFE, float size = MAX_DEFAULT_SIZE, float mass = MAX_DEFAULT_MASS, float angle = 0.0, bool active = true, const Point3f* pAxis = NULL, Shape* pShape = NULL );
	
		// Destrutor
		virtual ~Particle( void );

		// Atualiza o objeto
		virtual bool update( double timeElapsed, const DynamicVector* pForces, Plane* pCollisionPlane = NULL );
	
		// Renderiza o objeto
		virtual void render( void );

		// Determina se a partícula está ativa
		virtual void setActive( bool active );
	
		// Retorna se a partícula está ativa
		inline bool isActive( void ) const { return active; };

		// Determina a forma da partícula
		virtual void setShape( Shape* pShape );
	
		// Retorna a forma da partícula
		inline Shape* getShape( void ) const { return pShape; };

		// Determina o tempo de vida da partícula
		virtual void setLife( float life );
	
		// Retorna o tempo de vida da partícula
		inline float getLife( void ) const { return life; };
	
		// Retorna o tamanho da partícula
		virtual void setSize( float size );
	
		// Determina o tamanho da partícula
		inline float getSize( void ) const { return size; };
	
		// Determina a massa da partícula
		virtual void setMass( float mass );
	
		// Retorna a massa da partícula
		inline float getMass( void ) const { return mass; };
	
		// Determina o ângulo da partícula
		virtual void setAngle( float angle );
	
		// Retorna o ângulo da partícula
		inline float getAngle( void ) const { return angle; };

		// Determina o eixo da partícula
		virtual void setAxis( const Point3f* axis );
	
		// Retorna o eixo da partícula
		inline const Point3f* getAxis( void ) const { return &angleAxis; };

		// Determina se a partícula é imune a forças
		virtual void setImmune ( bool immune );
	
		// Retorna se a partícula é imune a forças
		inline bool isImmune( void ) const { return immune; };

		// Determina se a partícula é imortal
		virtual void setImmortal ( bool immortal );
	
		// Retorna se a partícula é imortal
		inline bool isImmortal( void ) const { return immortal; };

		// Determina se a partícula está visível
		virtual void setVisible( bool visible );
	
		// Retorna se a partícula está visível
		inline bool isVisible( void ) const { return visible; };

		// Determina uma partícula para esta copiar
		virtual bool setBasedOn( Particle* const baseParticle );
	
		// Obtém uma cópia desta partícula
		virtual Particle* getCopy( void );

		// Retorna o plano limite com o índice recebido 
		virtual const Plane* getLimitPlaneAt( uint16 index );
	
		// Remove o plano limite com o índice recebido
		virtual bool removeLimitPlaneAt( uint16 index );
	
		// Insere um novo plano limite no vetor de planos
		virtual int16 insertLimitPlane( const Plane* pLimitPlane );
	
		// Determina os planos limites da partícula
		virtual void setLimitPlanes( DynamicVector* limitPlanes );
	
#if TARGET_IPHONE_SIMULATOR
		// Determina o nome da partícula para fins de depuração
		inline void setParticleName( const char* pParticleName ){ memcpy( particleName, pParticleName, sizeof( char ) * PARTICLE_NAME_LENGTH ); };
	
		// Retorna o nome da partícula
		inline const char* getParticleName( void ) const { return particleName; };
#endif
	
	protected:
		// Tempo de vida da partícula
		float life;		
	
		// Tamanho da partícula
		float size;
	
		// Massa da partícula
		float mass;
	
		// Ângulo de rotação
		float angle;
	
		// Indica se a partícula está ativa
		bool active;
	
		// Indica se a partícula está visível
		bool visible;
	
		// Indica se a partícula é imortal
		bool immortal;
	
		// Indica se a partícula é imune a forças
		bool immune;

		// Eixo de rotação da partícula
		Point3f angleAxis;
	
		// Forma da partícula
		Shape* pShape;
	
		// Determinam uma área 3D onde a partícula é válida
		DynamicVector* pLimitPlanes;
		
		// Verifica se a partícula está dentro ou fora de seus planos limites
		virtual Plane* isOutsidePlanes( void );

#if TARGET_IPHONE_SIMULATOR
	private:
		// Nome da partícula para fins de depuração
		char particleName[ PARTICLE_NAME_LENGTH ];
#endif
};

#endif