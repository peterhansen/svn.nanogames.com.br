#include "Mesh.h"
#include "Macros.h"
#include "Exceptions.h"

#include <string.h>

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Mesh::Mesh( uint32 meshSize, const Triangle* pMeshTriangles ) : meshSize( meshSize ), pMesh( NULL )
{
	if( !pMeshTriangles )
#if TARGET_IPHONE_SIMULATOR
		throw InvalidArgumentException( "Não é possível criar uma malha de triângulos vazia. pMeshTriangles == NULL" );
#else
		throw InvalidArgumentException();
#endif
	
	pMesh = new Triangle[ meshSize ];
	if( !pMesh )
#if TARGET_IPHONE_SIMULATOR	// TODO : É necessário?! Será que o new já dispara a exceção?!?!?
		throw OutOfMemoryException( "Não foi possível criar a malha de triângulos" );
#else
		throw OutOfMemoryException();
#endif
		
	memcpy( pMesh, pMeshTriangles, sizeof( Triangle ) * meshSize );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

/* TASK Mesh::Mesh( const char* pFilename, MeshFormat fileFormat ) : meshSize( meshSize ), pMesh( NULL )
{
	loadMesh( pFilename, fileFormat );
}*/

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

Mesh::~Mesh( void )
{
	SAFE_DELETE_VEC( pMesh );
}

/*==============================================================================================

MÉTODO loadMesh
	Carrega a malha de um arquivo.

==============================================================================================*/

/*bool Mesh::loadMesh( const char* pFilename, MeshFormat fileFormat )
{
}*/
