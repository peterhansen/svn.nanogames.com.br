/*
 *  iBobSpringCoil.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/19/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef IBOB_SPRING_COIL_H
#define IBOB_SPRING_COIL_H

#include "SpringCoil.h"

class iBobSpringCoil : public SpringCoil
{
	public:	
		// Construtor
		iBobSpringCoil( void );
	
#if CONTROLLING_ORIENTATION

		// Atualiza o objeto retornando se a mola arrebentou
		virtual bool update( double time );
	
		// Determina a orientação que irá influenciar na direção original da mola
		inline void setOrientation( Point3f* pOrientation ){ this->pOrientation = pOrientation; };
	
	private:
		// Orientação que irá influenciar na direção original da mola
		Point3f* pOrientation;

#endif
};

#endif
