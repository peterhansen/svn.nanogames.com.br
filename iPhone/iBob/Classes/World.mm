#include "World.h"

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

World::World( void ) : pEnvironments( NULL )
{
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

World::~World( void )
{
	SAFE_DELETE( pEnvironments );
}

/*===========================================================================================
 
MÉTODO insertEnvironment
	Cria um novo ambiente para este mundo. Retorna o número de ambientes atualizados ou -1 em
casos de erro.
 
============================================================================================*/

int16 World::insertEnvironment( Environment* pEnvironment )
{
	if( !pEnvironments )
	{
		pEnvironments = new DynamicVector();
		if( !pEnvironments )
			return -1;
	}
	pEnvironments->insert( pEnvironment );

	return pEnvironments->getUsedLength() - 1;
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.
 
============================================================================================*/

void World::update( double timeElapsed )
{
	if( pEnvironments )
	{
		const uint16 length = pEnvironments->getUsedLength();
		for( uint16 i = 0; i < length; ++i )
			( ( Environment* ) pEnvironments->getData(i) )->update( timeElapsed );
	}
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.
 
============================================================================================*/

void World::render( void )
{
	if( pEnvironments )
	{
		const uint16 length = pEnvironments->getUsedLength();
		for( uint16 i = 0; i < length; ++i )
			( ( Environment* ) pEnvironments->getData(i) )->render();
	}
}
