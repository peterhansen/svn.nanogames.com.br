/*
 *  Mesh.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MESH_H
#define MESH_H

#include "NanoTypes.h"
#include "Triangle.h"

class Mesh
{
	public:
		// Construtores
		Mesh( uint32 meshSize, const Triangle* pMeshTriangles );
		// TASK Mesh( const char* pFilename, MeshFormat fileFormat );
	
		// Destrutor
		virtual ~Mesh( void );
	
		// Carrega a malha de um arquivo
		// TASK bool loadMesh( const char* pFilename, MeshFormat fileFormat );
	
	private:
		// Triângulos da malha
		uint32 meshSize;
		Triangle* pMesh;
};

#endif
