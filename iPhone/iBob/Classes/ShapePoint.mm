#include "ShapePoint.h"
#include "Point3f.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

ShapePoint::ShapePoint( const Color *pColor, float size ) : Shape( pColor, size )
{
}

/*==============================================================================================

MÉTODO getCopy
	Renderiza o objeto.

==============================================================================================*/

void ShapePoint::render( void )
{
	// Configura o tamanho de GL_POINT
	glPointSize( size );

	// Renderiza o ponto
	Point3f aux;
	glColor4f( color.r, color.g, color.b, color.a );
	glVertexPointer( 3, GL_FLOAT, 0, &aux );
	glDrawArrays( GL_POINTS, 0, 1 );
}

/*==============================================================================================

MÉTODO getCopy
	Obtém uma cópia do objeto.

==============================================================================================*/

Shape* ShapePoint::getCopy( void )
{
	return new ShapePoint( getColor(), getSize() );
}
