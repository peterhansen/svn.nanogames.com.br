/*
 *  Matrix4x4.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/5/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MATRIX4X4_H
#define MATRIX4X4_H

#include "NanoTypes.h"

struct Matrix4x4
{
	// Assim podemos acessar os dados do ponto através de seus valores individuais ou como
	// um array
	union
	{
        struct
		{
            float _00, _01, _02, _03;
            float _10, _11, _12, _13;
            float _20, _21, _22, _23;
            float _30, _31, _32, _33;
        };
        float m[4][4];
    };

	// Construtores
    Matrix4x4( void ) {};
	Matrix4x4( float value );
    Matrix4x4( const float* pElements );
    Matrix4x4( float _00, float _01, float _02, float _03,
			   float _10, float _11, float _12, float _13,
               float _20, float _21, float _22, float _23,
               float _30, float _31, float _32, float _33 );
	
	// Inicializa o objeto
	void set( float value );
	void set( const float* pElements );

    // Operadores de acesso
    float& operator () ( uint8 row, uint8 column );
    float  operator () ( uint8 row, uint8 column ) const;

    // Operadores de conversão ( casting )
    operator float* ();
    operator const float* () const;

    // Operadores de atribuição
    Matrix4x4& operator *= ( const Matrix4x4& mtx );
    Matrix4x4& operator += ( const Matrix4x4& mtx );
    Matrix4x4& operator -= ( const Matrix4x4& mtx );
    Matrix4x4& operator *= ( float f );
    Matrix4x4& operator /= ( float f );

    // Operadores unários
    Matrix4x4 operator + () const;
    Matrix4x4 operator - () const;

    // Operadores binários
    Matrix4x4 operator * ( const Matrix4x4& mtx ) const;
    Matrix4x4 operator + ( const Matrix4x4& mtx ) const;
    Matrix4x4 operator - ( const Matrix4x4& mtx ) const;
    Matrix4x4 operator * ( float f ) const;
    Matrix4x4 operator / ( float f ) const;

	// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
    friend Matrix4x4 operator * ( float f, const Matrix4x4& mtx );

	// Operadores de comparação
    bool operator == ( const Matrix4x4& mtx ) const;
    bool operator != ( const Matrix4x4& mtx ) const;
	
	// Transforma esta matriz em sua matriz transposta
	void transpose( void );
	
	// Obtém a matriz transposta desta matriz
	inline Matrix4x4* getTranspose( Matrix4x4* pOut ) const { *pOut = *this; pOut->transpose(); return pOut; };
	
	// Transforma esta matriz em sua matriz inversa
	bool inverse( void );
	
	// Obtém a matriz inversa desta matriz
	inline Matrix4x4* getInverse( Matrix4x4* pOut ) const { *pOut = *this; pOut->inverse(); return pOut; };
	
	// Transforma esta matriz em uma matriz identidade
	void identity( void );
	
	// Indica se esta matriz é uma matriz identidade
	bool isIdentity( void ) const;
	
	// Matrizes OpenGL são ordenadas da forma column-major
	inline Matrix4x4* getOpenGLMatrix( Matrix4x4* pOut ) const { return getTranspose( pOut ); }; 
};

// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
Matrix4x4 operator * ( float f, const Matrix4x4& mtx );

#endif

