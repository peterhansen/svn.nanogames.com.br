//
//  SplashNano.h
//  iBob
//
//  Created by Daniel Lopes Alves on 12/18/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef SPLASH_NANO_H
#define SPLASH_NANO_H

#import <UIKit/UIKit.h>

#include "Config.h"

@interface SplashNano : UIView
{
}

// Chama a próxima tela do jogo
- ( void ) onEnd;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif