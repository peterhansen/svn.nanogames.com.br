#include "PCM.h"
#include "Macros.h"

/*===========================================================================================

MÉTODO Print
	Imprime os valores dos samples do arquivo PCM.

============================================================================================*/

void PCM::Print( const uint8* pData, uint32 dataSize )
{
	int16* pData16 = ( int16* ) pData;
	for( uint32 i = 0 ; i < dataSize ; i += 2, ++pData16 )
		LOG( @"%d", *pData16 );
}

/*===========================================================================================

MÉTODO IsWhiteNoise
	Indica se o PCM descreve um ruído branco. Definições:
 
		- White noise is a random signal (or process) with a flat power spectral density. In 
		other words, the signal contains equal power within a fixed bandwidth at any center 
		frequency.
 
		- Bandwidth is the difference between the upper and lower cutoff frequencies of, for 
		example, a filter, a communication channel, or a signal spectrum, and is typically 
		measured in hertz. In case of a baseband channel or signal, the bandwidth is equal to 
		its upper cutoff frequency.
 
		- In statistical signal processing and physics, the spectral density, power spectral 
		density (PSD), or energy spectral density (ESD), is a positive real function of a 
		frequency variable associated with a stationary random process, or a deterministic 
		function of time, which has dimensions of power per Hz, or energy per Hz. It is often 
		called simply the spectrum of the signal. Intuitively, the spectral density captures the 
		frequency content of a stochastic process and helps identify periodicities.
 
 - http://en.wikipedia.org/wiki/Frequency_spectrum
 - http://en.wikipedia.org/wiki/Spectrum_analyzer
 - http://cnx.org/content/m0040/latest/
 
 - http://www.arachnoid.com/signal_processing/fft.html
 - http://www.fftw.org/fftw3_doc/index.html#Top
 
 
 The Fourier transform of a random (aka stochastic) waveform (aka noise) is also random.
 Some kind of averaging is required in order to create a clear picture of the underlying 
 frequency content (aka frequency distribution). Typically, the data is divided into
 time-segments of a chosen duration, and transforms are performed on each one. Then the
 magnitude or (usually) squared-magnitude components of the transforms are summed into an
 average transform. This is a very common operation performed on digitized (aka sampled)
 time-data, using the discrete Fourier transform (see Welch method). When the result is flat,
 as we have said, it is commonly referred to as white noise.

============================================================================================*/

bool PCM::IsWhiteNoise( const uint8* pData, uint32 dataSize )
{
	// TODO
	// power(s)= a0ˆ2 + 0.5f * SUM( akˆ2 + bkˆ2 );
	return false;
}
