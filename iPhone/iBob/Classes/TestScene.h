/*
 *  TestScene.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/26/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef TEST_SCENE_H
#define TEST_SCENE_H

#if RENDER_TEST_SCENE

#include "Scene.h"
#include "LoadableListener.h"

class TestScene : public Scene, public LoadableListener
{
	public:
		TestScene( void );
		virtual ~TestScene( void );
	
		virtual void loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data );

	private:
		bool build( void );
		Particle *pParticle;
};

#endif

#endif