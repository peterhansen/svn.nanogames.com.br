//
//  iBobAppDelegate.h
//  iBob
//
//  Created by Daniel Lopes Alves on 10/3/08.
//  Copyright Nano Games 2008. All rights reserved.
//

#ifndef GAME_APP_DELEGATE_H
#define GAME_APP_DELEGATE_H

#import <UIKit/UIKit.h>

#include "EAGLView.h"
#include "SplashGame.h"
#include "SplashNano.h"
#include "ViewManager.h"
#include "MenuController.h"

@interface iBobAppDelegate : NSObject <UIApplicationDelegate, UITabBarControllerDelegate, TransitionDelegate>
{
	// Janela da aplicação
	IBOutlet UIWindow* hWindow;
	
	// Responsável por realiza a transição entre as views da aplicação
	IBOutlet ViewManager* hViewManager;

	// View do menu
    IBOutlet MenuController* hTabBarController;
	
	// View do jogo
	EAGLView* hGLView;
	
	// View do splash da nano
	SplashNano* hSplashNano;
	
	// View do splash do jogo
	SplashGame* hSplashGame;
	
	// Índice da view atual
	uint8 currViewIndex;
}

// Cria os setters e getters para estas propriedades
@property ( nonatomic, assign ) UIWindow* hWindow;
@property ( nonatomic, assign ) ViewManager* hViewManager;
@property ( nonatomic, assign ) MenuController* hTabBarController;

// Executa a transição da view atual para a view de índice passado como parâmetro
- ( void )performTransitionToView:( uint8 )newIndex;

// Termina a aplicação
#if TARGET_IPHONE_SIMULATOR
- ( void )quit:( uint32 )error;
#else
- ( void )quit;
#endif

@end

#endif

