#if BLOB_MODE

#import "ColorsView.h"

#include "Config.h"
#include "Macros.h"
#include "iBobAppDelegate.h"

// Implementação da classe
@implementation ColorsView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Configura os sliders
	[hSliderR setValue: BLOB_COLOR_DEFAULT_R];
	[hSliderG setValue: BLOB_COLOR_DEFAULT_G];
	[hSliderB setValue: BLOB_COLOR_DEFAULT_B];
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM encodeWithCoder
	Implementação padrão do protocolo NSCoding.

==============================================================================================*/

- ( void )encodeWithCoder:( NSCoder* )encoder
{
	[super encodeWithCoder:encoder];
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( void )drawRect:( CGRect )rect
//{
//    // Drawing code
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	DESTROY( piBlobImageData );
    [super dealloc];
}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

- ( bool )build
{
	// Obtém um ponteiro para o array de dados da imagem a ser alterada
	piBlobImageData = PNG::load( @"goo.png" );
	if( piBlobImageData == NULL )
	{
		#if TARGET_IPHONE_SIMULATOR
			LOG( @"ColorsView: Nao foi possivel carregar a imagem\n" );
		#endif
		return false;
	}

	// Obtém o índice da cor que iremos modificar 
	Color defaultColor( BLOB_COLOR_DEFAULT_R, BLOB_COLOR_DEFAULT_G, BLOB_COLOR_DEFAULT_B, 1.0f );
	const int16 aux = piBlobImageData->getPaletteColorIndex( &defaultColor );
	if( aux < 0 )
	{
		#if TARGET_IPHONE_SIMULATOR
			LOG( @"ColorsView: A imagem não possui paleta de cores\n" );
		#endif
		return false;
	}

	defaultColorIndex = ( uint8 )aux;
	
	return true;
}

/*==============================================================================================

MENSAGEM onBackToDefault
	Configura a cor padrão.

==============================================================================================*/

- ( IBAction )onBackToDefault
{
	[hSliderR setValue: BLOB_COLOR_DEFAULT_R animated: YES];
	[hSliderG setValue: BLOB_COLOR_DEFAULT_G animated: YES];
	[hSliderB setValue: BLOB_COLOR_DEFAULT_B animated: YES];
	
	// TASK : Deixar configurar o intervalo de alpha???
	//[hSliderA setValue: animated: YES];
	
	[self onColorChanged];
}

/*==============================================================================================

MENSAGEM onColorChanged
	Evento chamado toda vez que o usuário faz modificações na configuração de cor do iBlob.

==============================================================================================*/

- ( IBAction )onColorChanged
{
	// Obtém os valores das componentes da cor
	Color currColor( [hSliderR value], [hSliderG value], [hSliderB value] );
	
	// Modifica a cor da imagem de feedback
	piBlobImageData->changePalette( defaultColorIndex, &currColor );
	
	UIImage* pImage = piBlobImageData->getImageCopy();
	if( pImage != NULL )
	{
		[hiBlobImageView setImage: pImage];
	}
	else
	{
		// Termina a aplicação
		#if TARGET_IPHONE_SIMULATOR
			[( iBobAppDelegate* )APP_DELEGATE quit: ERROR_CHANGING_COLORS];
		#else
			[( iBobAppDelegate* )APP_DELEGATE quit];
		#endif
	}
}

/*==============================================================================================

MENSAGEM getColor
	Retorna a cor configurada pelo jogador.

==============================================================================================*/

- ( void )getColor:( Color* )pColor
{
	// TASK : Deixar configurar o intervalo de alpha???
	//pColor->set( [hSliderR value], [hSliderG value], [hSliderB value], [hSliderA value] );

	pColor->set( [hSliderR value], [hSliderG value], [hSliderB value], 1.0f );
}

// Fim da implementação da classe
@end

#endif