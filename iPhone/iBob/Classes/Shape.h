#ifndef SHAPE_H
#define SHAPE_H

/*==============================================================================================

CLASSE Shape

==============================================================================================*/

#include "Texture.h"
#include "Color.h"

#define DEFAULT_SIZE 3.0

class Shape
{
	public:
		// Construtor
		Shape( const Color* pColor = NULL, float size = DEFAULT_SIZE );
	
		// Destrutor
		virtual ~Shape( void );

		// Renderiza o objeto
		virtual void render( void ) = 0;

		// Obtém uma cópia do objeto
		virtual Shape* getCopy( void ) = 0;

		// Determina a cor do objeto
		virtual void setColor( const Color* pColor = NULL );
	
		// Obtém a cor do objeto
		virtual Color* getColor( void ) { return &color; };
		
		// Determina o tamanho do objeto
		virtual void setSize( float size );
	
		// Obtém o tamanho do objeto
		inline float getSize( void ) const { return size; };
	
		// Determina a textura do objeto
		virtual void setTexture( Texture* pTexture );
	
		// Obtém a textura do objeto
		inline Texture* getTexture( void ) const { return pTexture; };
	
	protected:
		// Cor da forma
		Color color;
	
		// Tamanho da forma
		float size;
	
		// Textura da forma
		Texture *pTexture;
};

#endif