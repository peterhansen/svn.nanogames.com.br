//
//  SplashGame.h
//  iBob
//
//  Created by Daniel Lopes Alves on 12/18/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef SPLASH_GAME_H
#define SPLASH_GAME_H

#import <UIKit/UIKit.h>

@interface SplashGame : UIImageView
{
}

// Inicializa o objeto
- ( bool ) build;

// Chama a próxima tela do jogo
- ( void ) onEnd;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
