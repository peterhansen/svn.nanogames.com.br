#include "Force.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Force::Force( const Point3f *pDirection ) : Particle()
{
	setDirection( pDirection );
}

/*===========================================================================================

MÉTODO setDirection
	Determina a direção do vetor da força.

============================================================================================*/

void Force::setDirection( const Point3f *pDirection )
{
	if( pDirection )
		direction = *pDirection;
	else
		direction.set();
}

/*===========================================================================================

MÉTODO getCopy
	Retorna uma cópia deste objeto.

============================================================================================*/

Force* Force::getCopy( void ) const
{
	return new Force( &direction );
}
