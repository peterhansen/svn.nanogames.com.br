#include "Floor.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Floor::Floor( Texture *pTexture, float spriteWidth, float spriteHeight, GLenum textureEnvMode ) : Sprite( pTexture, spriteWidth, spriteHeight, textureEnvMode )
{
}

/*==============================================================================================

MÉTODO CreateInstance
	Cria uma nova instância da classe.

==============================================================================================*/

Floor* Floor::CreateInstance( Texture *pTexture, float spriteWidth, float spriteHeight, GLenum textureEnvMode )
{
	return new Floor( pTexture, spriteWidth, spriteHeight, textureEnvMode );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void Floor::render( void )
{
	float scaleFactor = 5.0f;
	
	glMatrixMode( GL_TEXTURE );
	glScalef( scaleFactor, scaleFactor, 0.0f );
	glMatrixMode( GL_MODELVIEW );
	
	Sprite::render();
	
	glMatrixMode( GL_TEXTURE );
	glLoadIdentity();
	glMatrixMode( GL_MODELVIEW );
}

