#include "PNG.h"
#include "Macros.h"

// Tamanho da tabela de CRCs
#define CRC_TABLE_LENGTH 256

// Inicializa as variáveis estáticas da classe
uint32* PNG::pCRCTable = NULL;

// Tamanhos (em bytes) fixos dos chunks e suas partes
#define PNG_SIGNATURE_LENGTH 8

#define CHUNK_SIZE_LENGTH	4
#define CHUNK_TYPE_LENGTH	4
#define CHUNK_CRC_LENGTH	4
#define CHUNK_MIN_LENGTH ( CHUNK_SIZE_LENGTH + CHUNK_TYPE_LENGTH + CHUNK_CRC_LENGTH )

// Chunk IHDR
#define CHUNK_IHDR_DATA_LENGTH 13
#define CHUNK_IHDR_LENGTH ( CHUNK_MIN_LENGTH + CHUNK_IHDR_DATA_LENGTH )

// Primeiro byte do chunk da paleta de cores (PLTE)
#define CHUNK_PLTE_FIRST_BYTE ( PNG_SIGNATURE_LENGTH + CHUNK_IHDR_LENGTH )

// Primeiro byte da paleta de cores propeiamente dita
#define PALETTE_START ( CHUNK_PLTE_FIRST_BYTE + CHUNK_SIZE_LENGTH + CHUNK_TYPE_LENGTH );

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

PNG::PNG( void ) : pData( NULL ), pPaletteChunk( NULL ), paletteLength( 0 )
{
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

PNG::~PNG( void )
{
	pPaletteChunk = NULL;
	SAFE_RELEASE( pData );
}

/*===========================================================================================

MÉTODO load
	Carrega a imagem.

============================================================================================*/

PNG* PNG::load( const char* pFileName )
{
	return load( CHAR_ARRAY_TO_NSSTRING( pFileName ) );
}

/*===========================================================================================

MÉTODO load
	Carrega a imagem.

============================================================================================*/

PNG* PNG::load( const NSString* pFileName )
{
	if( ( pCRCTable == NULL ) && !CreateCRCTable() )
		return NULL;
		
	PNG* pObj = new PNG();
	if( pObj )
	{
		// Exclui a extensão do arquivo
		NSRange aux = [pFileName rangeOfString: @"." options: NSBackwardsSearch];
		
		NSString* pFilePath;
		if( aux.location != NSNotFound )
			pFilePath = [pFileName substringToIndex: aux.location ];
		else
			pFilePath = pFileName;
		
		// Abre o arquivo
		pObj->pData = [NSData dataWithContentsOfFile: [[NSBundle mainBundle] pathForResource: pFilePath ofType: @"png" ] ];
		if( pObj->pData == NULL )
		{
			DESTROY( pObj );
		}
		else
		{
			// É necessário!!! Se não fizermos isso, pData será desalocado no fim desta mensagem
			[pObj->pData retain];
			
			#if TARGET_IPHONE_SIMULATOR
			// TASK : Verificar se é realmente um PNG
			// Assinatura do arquivo PNG: 137 80 78 71 13 10 26 10
			#endif
			
			// Procura o chunk da paleta de cores
			pObj->searchPLTEChunk();
		}
	}
	return pObj;
}

/*===========================================================================================

MÉTODO changePalette
	Modifica a cor from para a cor to na palera de cores da imagem.

============================================================================================*/

bool PNG::changePalette( const Color* pFrom, const Color* pTo )
{
	// Verifica se a imagem possui paleta de cores
	if( pPaletteChunk == NULL )
		return false;
	
	// Obtém um ponteiro para a paleta de cores
	const uint8* palettePointer = getPalettePointer();

	// Percorre as cores da paleta
	const uint16 nColors = getPaletteNColors();
	const uint8 r = pFrom->getByteR(), g = pFrom->getByteG(), b = pFrom->getByteB();
	for( uint16 i = 0, currByte =  0; i < nColors ; ++i, currByte += 3 )
	{
		// Verifica se esta cor é a cor que estamos procurando
		if( ( palettePointer[ currByte ] == r )
			&& ( palettePointer[ currByte + 1 ] == g )
			&& ( palettePointer[ currByte + 2 ] == b ) )
		{
			// Modifica a cor
			return changePalette( i, pTo );
		}
	}
	return false;
}

/*===========================================================================================

MÉTODO changePalette
	Faz com que a cor de índice colorPaletteIndex na paleta de cores da imagem passe a ser
igual a newColor.

============================================================================================*/

bool PNG::changePalette( uint8 colorPaletteIndex, const Color* pNewColor )
{
	// Verifica se a imagem possui paleta de cores
	if( pPaletteChunk == NULL )
		return false;

	// Obtém um ponteiro para a paleta de cores
	uint8* palettePointer = getPalettePointer();
	
	// Modifica a cor da paleta
	uint8* pColorTochange = palettePointer + ( colorPaletteIndex * 3 );
	pColorTochange[ 0 ] = pNewColor->getByteR();
	pColorTochange[ 1 ] = pNewColor->getByteG();
	pColorTochange[ 2 ] = pNewColor->getByteB();
	
	// Recalcula o CRC do chunk
	// OBS : palettePointer + CHUNK_SIZE_LENGTH => O tamanho do chunk não entra no cálculo do CRC
	uint32* pCRC = ( uint32* )( palettePointer + paletteLength );
	*pCRC = GetCRC( pPaletteChunk + CHUNK_SIZE_LENGTH, CHUNK_TYPE_LENGTH + paletteLength );
	
	return true;
}

/*===========================================================================================

MÉTODO getPaletteColorIndex
	Retorna o índice da cor recebida na paleta de cores.

============================================================================================*/

int16 PNG::getPaletteColorIndex( const Color* pColor ) const
{
	if( pPaletteChunk != NULL )
	{
		// Obtém um ponteiro para a paleta de cores
		const uint8* palettePointer = getPalettePointer();

		// Percorre as cores da paleta
		const uint16 nColors = getPaletteNColors();
		const uint8 r = pColor->getByteR(), g = pColor->getByteG(), b = pColor->getByteB();
		for( uint16 i = 0, currByte =  0; i < nColors ; ++i, currByte += 3 )
		{
			// Verifica se esta cor é a cor que estamos procurando
			if( ( palettePointer[ currByte ] == r )
				&& ( palettePointer[ currByte + 1 ] == g )
				&& ( palettePointer[ currByte + 2 ] == b ) )
			{
				// Retorna o índice da cor
				return i;
			}
		}
	}
	return -1;
}

/*===========================================================================================

MÉTODO CreateCRCTable
	Obtém uma cópia da paleta de cores da imagem. O usuário é responsável por desalocar o vetor
retornado.
	TASK : Preencher os valores de alpha corretamente!!!!!!

============================================================================================*/

Color* PNG::getPalette( void )
{
	// Verifica se a imagem possui paleta de cores
	if( pPaletteChunk == NULL )
		return NULL;
	
	// Aloca um vetor para conter as cores da paleta
	Color* pRet = new Color[ getPaletteNColors() ];
	if( !pRet )
		return NULL;
	
	// Obtém um ponteiro para a paleta de cores
	const uint8* palettePointer = getPalettePointer();
	
	// Preenche o vetor cópia
	for( uint16 i = 0 ; i < paletteLength ; i += 3 )
		pRet[i/3].set( palettePointer[i], palettePointer[i+1], palettePointer[i+2], 255 );
	
	return pRet;
}

/*===========================================================================================

MÉTODO setPalette
	Determina uma nova paleta de cores para a imagem. O parâmetro nColors é colocado no
intervalo [0, getPaletteNColors() ].
 
	TASK : Levar em conta o alpha!!!!!!

============================================================================================*/
 
void PNG::setPalette( Color* pNewPalette, uint16 nColors )
{
	// Verifica se a imagem possui paleta de cores e os parâmetros do método
	if( ( pPaletteChunk == NULL ) || ( pNewPalette == NULL ) || ( nColors == 0 ) )
		return;
	
	// Coloca nColors no intervalo aceito
	nColors = CLAMP( nColors, 0, getPaletteNColors() );
	
	// Obtém um ponteiro para a paleta de cores
	uint8* palettePointer = getPalettePointer();
	
	// Copia as cores recebidas na paleta da imagem
	uint32 currIndex;
	for( uint16 i = 0 ; i < nColors ; ++i )
	{
		currIndex = i * 3;
		palettePointer[ currIndex     ] = pNewPalette[i].getByteR();
		palettePointer[ currIndex + 1 ] = pNewPalette[i].getByteG();
		palettePointer[ currIndex + 2 ] = pNewPalette[i].getByteB();
	}
	
	// Recalcula o CRC do chunk
	// OBS : palettePointer + CHUNK_SIZE_LENGTH => O tamanho do chunk não entra no cálculo do CRC
	uint32* pCRC = ( uint32* )( palettePointer + paletteLength );
	*pCRC = GetCRC( pPaletteChunk + CHUNK_SIZE_LENGTH, CHUNK_TYPE_LENGTH + paletteLength );
}

/*===========================================================================================

MÉTODO CreateCRCTable
	Retorna uma imagem gerada a partir do arquivo PNG. O usuário é responsável por desalocar
a imagem retornada.

============================================================================================*/

UIImage* PNG::getImageCopy( void ) const
{
	return [UIImage imageWithData: pData];
}

/*===========================================================================================

MÉTODO CreateCRCTable
	Pré-calcula a tabela de CRC para otimizar cálculos futuros.

============================================================================================*/

bool PNG::CreateCRCTable( void )
{
	pCRCTable = new uint32[ CRC_TABLE_LENGTH ];
	if( !pCRCTable )
		return false;

	uint32 c;
	for( uint16 i = 0 ; i < CRC_TABLE_LENGTH ; ++i )
	{
		c = i;
		for( uint8 j = 0 ; j < 8 ; ++j )
		{
			if( ( c & 1 ) != 0 )
				c = 0xedb88320 ^ ( c >> 1 );
			else
				c >>= 1;
		}
		pCRCTable[ i ] = c;
	}
	return true;
}

/*===========================================================================================

MÉTODO GetCRC
	Calcula o CRC dos dados contidos em pChunkData.

============================================================================================*/

uint32 PNG::GetCRC( const uint8* pChunkData, uint32 length )
{
	uint32 crc = 0xFFFFFFFF;
	for( uint32 i = 0 ; i < length ; ++i )
		crc = pCRCTable[ ( crc ^ pChunkData[ i ] ) & 0xFF ] ^ ( crc >> 8 );

	return htonl( crc ^ 0xFFFFFFFF );
}

/*===========================================================================================

MÉTODO getPalettePointer
	Obtém um ponteiro para o início da paleta de cores da imagem.

============================================================================================*/

uint8* PNG::getPalettePointer( void ) const
{
	return pPaletteChunk == NULL ? NULL : pPaletteChunk + CHUNK_SIZE_LENGTH + CHUNK_TYPE_LENGTH;
}

/*===========================================================================================

MÉTODO searchPLTEChunk
	Procura o chunk da paleta de cores.

============================================================================================*/

void PNG::searchPLTEChunk( void )
{
	uint8 *pFirstByte = ( uint8* ) [pData bytes];

	// Pula a assinatura PNG e o chunk IHDR, que obrigatoriamente é o 1o
	uint8* pCurrByte = pFirstByte + PNG_SIGNATURE_LENGTH + CHUNK_IHDR_LENGTH;
	
	const uint32 pngSize = [pData length];
	while( ( pCurrByte - pFirstByte ) < pngSize )
	{
		// Obtém o tamanho do chunk
		uint32 chunkDataLength = ntohl( *( ( uint32* )pCurrByte ) );
		pCurrByte += CHUNK_SIZE_LENGTH;
		
		// Verifica se é o chunk que estamos procurando
		if(( pCurrByte[0] == 'P' ) && ( pCurrByte[1] == 'L' ) && ( pCurrByte[2] == 'T' ) && ( pCurrByte[3] == 'E' ))
		{
			// Armazena o ponteiro para a paleta de cores
			pPaletteChunk = pCurrByte - CHUNK_SIZE_LENGTH;
			
			// Obtém o tamanho do chunk da paleta de cores (PLTE)
			paletteLength = chunkDataLength;
			
			return;
		}
		
		// Senão, vai para o próximo chunk
		pCurrByte += CHUNK_TYPE_LENGTH + chunkDataLength + CHUNK_CRC_LENGTH;
	}
}

