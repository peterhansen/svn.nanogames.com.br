#include "Sprite.h"
#include "Macros.h"

// Coordenadas de textura dos sprites

// OBS : Se utilarmos as coordenadas abaixo, a imagem ficará espelhada verticalmente. Este
// é um "feature" normal do OpenGL. Logo, utilizamos as coordenadas de textura já invertidas
// no eixo y
//const float Sprite::texCoords[ N_SPRITE_TEX_COORDS ] = {	0.0f, 0.0f,
//															0.0f, 1.0f,
//															1.0f, 0.0f,
//															1.0f, 1.0f };

const float Sprite::texCoords[ N_SPRITE_TEX_COORDS ] = {	0.0f, 1.0f,
															0.0f, 0.0f,
															1.0f, 1.0f,
															1.0f, 0.0f };
										
// Vértices do sprite
const float Sprite::vertexes[12] = {	-0.5f, -0.5f, 0.0f,
										-0.5f,  0.5f, 0.0f,
										 0.5f, -0.5f, 0.0f,
										 0.5f,  0.5f, 0.0f };

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Sprite::Sprite( Texture *pTexture, float spriteWidth, float spriteHeight, GLenum textureEnvMode ) : Shape(), texEnvMode( textureEnvMode ), usingOwnTexCoords( false ), width( spriteWidth ), height( spriteHeight )
{
	setTexture( pTexture );
}

/*==============================================================================================

MÉTODO CreateInstance
	Cria uma nova instância da classe.

==============================================================================================*/

Sprite* Sprite::CreateInstance( Texture *pTexture, float spriteWidth, float spriteHeight, GLenum textureEnvMode )
{
	return new Sprite( pTexture, spriteWidth, spriteHeight, textureEnvMode );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void Sprite::render( void )
{
	if( !width && !height )
		return;

	// Determina o shade model
	int32 shadeModelPrevValue;
	glGetIntegerv( GL_SHADE_MODEL, &shadeModelPrevValue );
	glShadeModel( GL_FLAT );
	
	Color currOglColor;
	glGetFloatv( GL_CURRENT_COLOR, ( float* )&currOglColor );

	glColor4f( color.r, color.g, color.b, color.a );

	if( pTexture )
	{
		glEnable( GL_TEXTURE_2D );
		
		if( usingOwnTexCoords )
			glTexCoordPointer( 2, GL_FLOAT, 0, myTexCoords );
		else
			glTexCoordPointer( 2, GL_FLOAT, 0, Sprite::texCoords );

 		pTexture->load();
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, texEnvMode );
	}

	glScalef( width, height, 1.0f );

	glVertexPointer( 3, GL_FLOAT, 0, Sprite::vertexes );
	glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );

	// Reseta o shade model e a cor padrão
	glShadeModel( shadeModelPrevValue );
	glColor4f( currOglColor.r, currOglColor.g, currOglColor.b, currOglColor.a );

	if( pTexture )
		pTexture->unload();
}

/*==============================================================================================

MÉTODO setWidth
	Determina a largura do sprite.

==============================================================================================*/

void Sprite::setWidth( float width )
{
	this->width = width;
}

/*==============================================================================================

MÉTODO setHeight
	Determina a altura do sprite.

==============================================================================================*/

void Sprite::setHeight( float height )
{
	this->height = height;
}

/*==============================================================================================

MÉTODO getCopy
	Obtém uma cópia do objeto.

==============================================================================================*/

Shape* Sprite::getCopy( void )
{
	Sprite *pCopy = CreateInstance( pTexture, width, height );

	if( pCopy )
	{
		pCopy->setColor( &color );
		pCopy->setSize( size );
	}
	return pCopy;
}

/*==============================================================================================

MÉTODO setTexCoords
	Determina coordenadas de textura específicas para este objeto. Se desejar utilizar as
coordenadas de textura padrão fornecidas pela classe, basta passar NULL.

==============================================================================================*/

void Sprite::setTexCoords( float* pTexCoords )
{
	if( pTexCoords == NULL )
	{
		usingOwnTexCoords = false;
	}
	else
	{
		usingOwnTexCoords = true;
		memcpy( myTexCoords, pTexCoords, sizeof( float ) * N_SPRITE_TEX_COORDS );
	}
}

