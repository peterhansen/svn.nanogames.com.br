#ifndef BOB_SIMPLE_SCENE_H
#define BOB_SIMPLE_SCENE_H

/*
 *  BobSimpleCube.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#import <UIKit/UIKit.h>

#include "Scene.h"
#include "Texture2D.h"
#include "LoadableListener.h"
#include "PerspectiveCamera.h"

class BobSimpleScene : public Scene, LoadableListener
{
	public:
		// Retorna uma instância da classe
		static BobSimpleScene* CreateInstance( void );
	
		// Destrutor
		virtual ~BobSimpleScene( void );
	
		// Renderiza o objeto
		virtual void render( void );
	
		// Atualiza o objeto
		virtual void update( double timeElapsed );
	
		// Método para receber os eventos do objeto loadable
		virtual void loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data );
	
	private:
		// Construtor
		BobSimpleScene( void );
	
		// Iniciliza o objeto
		bool build( void );
	
		// Textura utilizada no cubo do Bob Esponja
		Texture2D* pBobTexture;
		
		// Acumula o ângulo de rotação do cubo texturizado 
		float angle;
};

#endif