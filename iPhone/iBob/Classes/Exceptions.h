/*
 *  Exceptions.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/27/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <stdexcept>
#include <string.h>

// Tamanho máximo da string de descrição da exceção
#define MAX_EXCEPTION_MSG_LEN 256

using namespace std;

// TASK : Otimizar!!!!!!!!!!

class NanoBaseException : public exception
{
	public:
		// Construtor
		NanoBaseException( const char* pMsg = NULL )
		{
			if( pMsg )
				strncpy( msg, pMsg, MAX_EXCEPTION_MSG_LEN );
			else
				strcpy( msg, "NanoBaseException" );
		};
	
		// Obtém a mensagem da exceção
		/* final */virtual const char* what( void ) const throw() { return msg; };

	private:
		// Erro informado pela exceção
		char msg[ MAX_EXCEPTION_MSG_LEN ];
};

class InvalidArgumentException : public NanoBaseException
{
	public:
		// Construtor
		InvalidArgumentException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
};

class InvalidOperationException : public NanoBaseException
{
	public:
		// Construtor
		InvalidOperationException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
};

class OutOfMemoryException : public NanoBaseException
{
	public:
		// Construtor
		OutOfMemoryException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
};

class IllegalMathOperation : public NanoBaseException
{
	public:
		// Construtor
		IllegalMathOperation( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
};

class OpenALException : public NanoBaseException
{
	public:
		// Construtor
		OpenALException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
};

#endif