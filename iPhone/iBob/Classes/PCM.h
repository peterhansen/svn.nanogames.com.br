/*
 *  PCM.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 1/5/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_PCM_H
#define NANO_PCM_H

#include "NanoTypes.h"

class PCM
{
	public:
		// Imprime os valores dos samples do arquivo PCM
		static void Print( const uint8* pData, uint32 dataSize );
	
		// Indica se o PCM descreve um ruído branco
		static bool IsWhiteNoise( const uint8* pData, uint32 dataSize );

	private:
};

#endif