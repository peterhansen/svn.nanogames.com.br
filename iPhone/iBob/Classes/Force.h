#ifndef FORCE_H
#define FORCE_H

/*===========================================================================================

CLASSE Force

============================================================================================*/

#include "Particle.h"

// TASK : Precisa derivar de Particle? Para podermos ter um emissor de particles é legal...
// TASK : Uma força deve ter uma área ( width, height, depth ) de atuação que se aplica a partir de
// sua posição
class Force : public Particle
{
	public:
		// Construtor
		Force( const Point3f *pDirection = NULL );
		
		// Destrutor
		virtual ~Force( void ){};

		// Atualiza o objeto
		virtual void update( double timeElapsed ){};

		// Determina a direção do vetor da força
		virtual void setDirection( const Point3f *pDirection = NULL );

		// Obtém a direção do vetor da força
		inline const Point3f* getDirection( void ) const { return &direction; };

		// Retorna uma cópia deste objeto
		virtual Force* getCopy( void ) const;

	protected:
		// Direção do vetor da força
		Point3f	direction;
};

#endif