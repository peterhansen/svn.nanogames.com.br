/*
 *  Utils.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/16/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef UTILS_H
#define UTILS_H

#include "NanoTypes.h"
#include "Triangle.h"
#include "Ray.h"
#include "Matrix4x4.h"
#include "Viewport.h"
#include "PerspectiveCamera.h"
#include "AudioContext.h"

class Utils
{
	public:
		// Salva a string no arquivo de log padrão
		static bool Log( NSString* hFormat, ... );

#if TARGET_IPHONE_SIMULATOR
		// Loga o erro do openGL na janela console
		static void glLog( const char* pFileName, const unsigned long line );
#endif

		// Retorna se o raio colide com o triângulo. Retorna as coordenadas baricêntricas em u, v e a distância para a origem do raio em t 
		static bool IntersectRayTriangle( bool testCull, const Ray* pRay, const Triangle* pTriangle, float* t, float* u, float* v );
	
		// Cria um raio a partir das coordenadas da tela recebidas
		static Ray* GetPickingRay( int32 x, int32 y, Ray* pRay );
	
		// Leva as coordenadas do espaço da tela para o espaço local
		static bool Unproject( Point3f* pUnprojected, const Point3f* pPoint );
	
		// Leva as coordenadas do espaço local para o espaço da tela
		static bool Project( Point3f* pProjected, const Point3f* pPoint );
	
		// Transforma as coordenadas do raio utilizando a matriz 4x4 apontada por pTransformMtx
		static void TransformRay( Ray* pRay, const Matrix4x4* pTransformMtx );
	
		// Transforma o ponto, pPoint(x, y, z, 1), pela matriz, pMtx, projetando o resultado de volta ao
		// plano w = 1
		static Point3f* TransformCoord( Point3f* pOut, const Point3f* pPoint, const Matrix4x4* pMtx );
	
		// Transforma o vetor, pNormal(x, y, z, 0), pela matriz pMtx
		static Point3f* TransformNormal( Point3f* pOut, const Point3f* pNormal, const Matrix4x4* pMtx );
	
		// Retorna a matriz de projeção que está sendo utilizada pela API gráfica
		static Matrix4x4* GetProjectionMatrix( Matrix4x4* pOut );
	
		// Retorna a matriz de modelo/visão que está sendo utilizada pela API gráfica
		static Matrix4x4* GetModelViewMatrix( Matrix4x4* pOut );
	
		// Retorna o viewport que está sendo utilizado pela API gráfica
		static Viewport* GetViewport( Viewport* pOut );
	
		// Obtém uma matriz de rotação utilizando as funcionalidades da API gráfica
		static Matrix4x4* GetRotationMatrix( Matrix4x4* pOut, const Point3f* pAxis, float angle );
	
		// Obtém o ângulo e o eixo de rotação entre dois vetores
		static float FindAngleBetweenVectors( Point3f* pOut, const Point3f* pV1, const Point3f* pV2 );
	
		// Carrega um arquivo de áudio
		static void* GetAudioFile( const char* pFileName, const char* pFileType, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq );
		static void* GetAudioFile( const NSString* pFileName, const NSString* pFileType, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq );
	
		// Carrega uma fonte de áudio contendo o arquivo passado como parâmetro
		static AudioSource* GetAudioSource( AudioContext* pContext, const char* pFileName, const char* pFileType );
	
		// Vibra o device
		static void Vibrate( void );
	
		// Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente,
		// é necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController
		// à view propriamente dita
		static UIView* LoadViewFromXib( const char* pViewName );
		static UIView* LoadViewFromXib( const NSString* hViewName );
};

#endif
