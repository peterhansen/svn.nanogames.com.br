//
//  WebButton.m
//  iBob
//
//  Created by Daniel Lopes Alves on 1/7/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import "WebButton.h"


@implementation WebButton


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)dealloc {
    [super dealloc];
}


@end
