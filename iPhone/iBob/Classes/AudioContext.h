/*
 *  AudioContext.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/15/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_CONTEXT_H
#define AUDIO_CONTEXT_H

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

#include "AudioSource.h"

// Número máximo de AudioSources que podem ser manipulados por um AudioContext
#define MAX_CONTEXT_SOURCES 128

// Pré-definição da classe AudioDevice para evitar #includes recursivos
class PlaybackAudioDevice;

class AudioContext
{
	public:
		// Destrutor
		~AudioContext( void );
	
		// Indica se este device é o device atual
		inline bool isCurrentContext( void ) const { return alcGetCurrentContext() == pContext; };

		// Torna este AudioContext o atual
		bool makeCurrentContext( void );
	
		// Retorna o AudioDevice que controla este AudioContext
		inline const PlaybackAudioDevice* getParentDevice( void ) const { return pParentDevice; };
	
		// Suspende a execução deste AudioContext. Mudanças de estado no OpenAL serão aceitas, mas
		// não processadas
		bool suspend( void );
	
		// Processa todas as mudanças de estado pendentes e retoma a execução do device
		bool resume( void );
	
		// Obtém um novo AudioSource para este AudioContext
		AudioSource* newAudioSource( uint8* pIndex = NULL );
	
		// Obtém um AudioSource deste AudioContext 
		AudioSource* getAudioSource( uint8 index ) const;
	
	protected:
		// Objetos da classe AudioDevice terão acesso aos métodos e atributos protected de AudioContext
		friend class PlaybackAudioDevice;
	
		// Construtor
		// Exceções : OpenALException
		AudioContext( const PlaybackAudioDevice* pParent );
	
	private:
		// Indica quais fontes estavam executando antes de o componente ser suspenso
		// TASK : Transformar em array de bits!!!!!!!!
		bool wasPlaying[ MAX_CONTEXT_SOURCES ];
	
		// Ponteiro para a inerface OpenAL
		ALCcontext* pContext;
	
		// Audio device que controla este AudioContext
		const PlaybackAudioDevice* pParentDevice;
	
		// AudioSources controlados por este AudioContexts
		AudioSource* sources[ MAX_CONTEXT_SOURCES ];
};

#endif

