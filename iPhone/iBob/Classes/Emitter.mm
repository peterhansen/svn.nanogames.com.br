#include "Emitter.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Emitter::Emitter( const Point3f* pPosition, const Point3f* pSpeed, float life, float size, float mass, float angle, bool active, const Point3f* pAxis, Shape* pShape, float emissionRate ) 
				: Particle( pPosition, pSpeed, life, size, mass, angle, active, pAxis, pShape ),
				emissionRate( emissionRate ), pElements( NULL ), pBaseElement( NULL ), currentActiveElements( 0 ),
				elementsToEmit( 0.0f )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Emitter::~Emitter( void )
{
	SAFE_DELETE( pElements );
	SAFE_DELETE( pBaseElement );
}

/*==============================================================================================

MÉTODO emit

==============================================================================================*/

bool Emitter::emit( uint16 quantity )
{
	if( pBaseElement )
	{
		for( uint16 i = 0 ; i < quantity ; i++ )
		{
			if( currentActiveElements + 1 >= pElements->getUsedLength() )
			{
				// Número de partículas ativas é maior do que o tamanho máximo atual do vetor
				Particle* pCopy = pBaseElement->getCopy();
				if( !pCopy || !pElements->insert( pCopy ) )
				{
					SAFE_DELETE( pCopy );
					return false;
				}	
			}
			Particle* pParticle = ( Particle* )pElements->getData( currentActiveElements );
			if( pParticle )
			{
				if( !pParticle->setBasedOn( pBaseElement ) )
					return false;

				pParticle->setPosition( &position );
				
				++currentActiveElements;
			}
		}

		elementsToEmit -= quantity;
		if( elementsToEmit < 0.0f )
			elementsToEmit = 0.0f;

		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO update

==============================================================================================*/

bool Emitter::update( double timeElapsed, const DynamicVector* pExternForces, Plane* pCollisionPlane )
{
	if( !active )
		return true;

	if( !Particle::update( timeElapsed, pExternForces, pCollisionPlane ) )
	{
		// Emissor pára de emitir novas partículas
		elementsToEmit = 0.0f;
		emissionRate = 0.0f;

		// Se todas suas partículas filhas morrerem, emissor morre
		if( currentActiveElements == 0 )
			return false;
		
		active = true;
		visible = true;
	}

	if( pBaseElement && pElements )
	{
		for( int32 i = 0; i < currentActiveElements; /* Sem ++i */ )
		{
			Particle *e = ( Particle* ) pElements->getData(i);
			if( e && !e->update( timeElapsed, pExternForces, NULL ) )
			{
				// Elemento morreu, então reorganiza lista
				pElements->switchSlots( i, currentActiveElements-1 );
				--currentActiveElements;
			}
			else
			{
				++i;
			}
		}
		
		elementsToEmit += timeElapsed * emissionRate;
		if( elementsToEmit >= 1.0f && !emit( ( uint16 ) elementsToEmit ) )
			return false;
	}
	return true;
}

/*==============================================================================================

MÉTODO render

==============================================================================================*/

void Emitter::render( void )
{
	if( isVisible() )
	{
		Particle::render();

		for( uint16 i = 0; i < currentActiveElements; ++i )
		{
			Particle* p = ( Particle* )pElements->getData(i);
			if( p )
				p->render();
		}
	}
}

/*==============================================================================================

MÉTODO setBaseElement

==============================================================================================*/

bool Emitter::setBaseElement( Particle* pNewBaseElement )
{
	if( pNewBaseElement )
	{
		// Apaga o array de elementos anterior
		SAFE_DELETE( pElements );
		
		// Cria um novo array de elementos
		pElements = new DynamicVector();
		if( !pElements || !pElements->insert( pNewBaseElement->getCopy() ) )
		{
			SAFE_DELETE( pElements );
			return false;
		}
		pBaseElement = pNewBaseElement;
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO setEmissionRate

==============================================================================================*/

void Emitter::setEmissionRate( float newEmissionRate )
{
	emissionRate = newEmissionRate;
}

/*==============================================================================================

MÉTODO getCopy

==============================================================================================*/

Particle* Emitter::getCopy( void ) 
{
	Emitter* pCopy = new Emitter( &position, &speed, getLife(), size, mass, angle, active, &angleAxis, NULL, emissionRate );
	if( pCopy )
	{
		if( !pCopy->setBaseElement( pBaseElement ) )
		{
			DESTROY( pCopy );
			return NULL;
		}
		pCopy->setShape( pShape->getCopy() );
	}
	return pCopy;
}

/*==============================================================================================

MÉTODO setBasedOn

==============================================================================================*/

bool Emitter::setBasedOn( Particle* const pBaseParticle )
{
	if( !Particle::setBasedOn( pBaseParticle ) )
		return false;

	emissionRate = ( ( Emitter* )pBaseParticle )->getEmissionRate();
	elementsToEmit = 0.0f;
	currentActiveElements = 0;

	return true;
}
