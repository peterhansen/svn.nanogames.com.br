//
//  ColorsView.h
//  iBob
//
//  Created by Daniel Lopes Alves on 12/11/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef COLORS_VIEW_H
#define COLORS_VIEW_H

#if BLOB_MODE

#import <UIKit/UIKit.h>

#include "PNG.h"

@interface ColorsView : UIView <NSCoding>
{
	@private
		// Barras para a configuração das componentes R, G, B da cor do iBlob
		IBOutlet UISlider* hSliderR;
		IBOutlet UISlider* hSliderG;
		IBOutlet UISlider* hSliderB;
	
		// TASK : Deixar configurar o intervalo de alpha???
		//IBOutlet UISlider* hSliderA;

		// Botão que reseta a cor atual de volta para a cor padrão
		IBOutlet UIButton* hBackToDefault;
	
		// Imagem de feedback
		IBOutlet UIImageView* hiBlobImageView;
	
		// Índice da cor padrão na paleta de cores da imagem do iBlob
		uint8 defaultColorIndex;
	
		// Imagem que iremos modificar
		PNG* piBlobImageData;
}

// Inicializa o objeto
- ( bool )build;

// Configura a cor padrão
- ( IBAction )onBackToDefault;

// Evento chamado toda vez que o usuário faz modificações na configuração de cor do iBlob
- ( IBAction )onColorChanged;

// Retorna a cor configurada pelo jogador
- ( void )getColor:( Color* )pColor;

@end

#endif

#endif