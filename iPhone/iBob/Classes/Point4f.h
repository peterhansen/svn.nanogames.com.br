/*
 *  Point4f.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/5/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef POINT4F_H
#define POINT4F_H

#include "Point3f.h"

struct Point4f
{
	// Assim podemos acessar os dados do ponto através de seus valores individuais ou como
	// um array
	union
	{
		struct
		{
			float x, y, z, w;
		};
		float p[4];
	};

	// Construtores
    Point4f( const float* xyzw );
	Point4f( const Point3f& xyz, float w = 1.0f );
    Point4f( float x = 0.0f, float y = 0.0f, float z = 0.0f, float w  = 1.0f );

    // Operadores de conversão ( casting )
    operator float* ();
    operator const float* () const;

    // Operadores de atribuição
    Point4f& operator += ( const Point4f& p );
    Point4f& operator -= ( const Point4f& p );
    Point4f& operator *= ( float f );
    Point4f& operator /= ( float f );

    // Operadores unários
    Point4f operator + () const;
    Point4f operator - () const;

    // Operadores binários
    Point4f operator + ( const Point4f& p ) const;
    Point4f operator - ( const Point4f& p ) const;
    Point4f operator * ( float f ) const;
    Point4f operator / ( float f ) const;

	// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
    friend Point4f operator * ( float, const Point4f& p );

	// Operadores de comparação
    BOOL operator == ( const Point4f& p ) const;
    BOOL operator != ( const Point4f& p ) const;
	
	// Inicializa um ponto
	inline void set( float x = 0.0f, float y = 0.0f, float z = 0.0f, float w = 0.0f ){ this->x = x; this->y = y; this->z = z; this->w = w; };
};

// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
Point4f operator * ( float f, const Point4f& p );

#endif
