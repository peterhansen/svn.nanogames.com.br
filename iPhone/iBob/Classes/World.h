#ifndef WORLD_H
#define WORLD_H

#include "Environment.h"
#include "DynamicVector.h"

class World
{
	public:
		// Construtor
		World( void );
	
		// Destrutor
		virtual ~World( void );

		// Cria um novo ambiente para este mundo. Retorna o número de ambientes atualizados ou -1 em casos de erro
		int16 insertEnvironment( Environment *pEnvironment );
	
		// Remove o ambiente de índice recebido como parâmetro
		inline bool removeEnvironmentAt( uint16 index ) const { return pEnvironments ? pEnvironments->remove( index ) : false; };
	
		// Retorna o ambiente de índice recebido como parâmetro
		inline Environment* getEnvironmentAt( uint16 index ) const { return pEnvironments ? ( Environment* )pEnvironments->getData( index ) : NULL; };
	
		// Renderiza o objeto
		virtual void render( void );
		
		// Atualiza o objeto
		virtual void update( double timeElapsed );

	private:
		// Array de ambientes deste mundo
		DynamicVector *pEnvironments;
};

#endif