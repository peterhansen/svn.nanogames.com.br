#ifndef EMITTER_H
#define EMITTER_H

/*==============================================================================================

CLASSE Emitter

==============================================================================================*/

#include "Particle.h"
#include "DynamicVector.h"
#include "Force.h"

class Emitter : public Particle
{
	public:
		// Construtor
		Emitter( const Point3f* pPosition = NULL, const Point3f* pSpeed = NULL, float life = MAX_DEFAULT_LIFE, float size = MAX_DEFAULT_SIZE, float mass = MAX_DEFAULT_MASS, float angle = 0.0, bool active = true, const Point3f* pAxis = NULL, Shape* pShape = NULL, float emissionRate = 1.0f );
	
		// Destrutor
		virtual ~Emitter( void );

		// Renderiza o objeto
		virtual void render( void );
	
		// Atualiza o objeto
		virtual bool update( double timeElapsed, const DynamicVector* pExternForces, Plane* pCollisionPlane = NULL );
	
		// Emite "quantity" elementos baseados em "baseElement"
		virtual bool emit( uint16 quantity );

		virtual bool setBaseElement( Particle* baseElement );
		inline Particle* getBaseElement( void ) const { return pBaseElement; };

		virtual void setEmissionRate( float emissionRate );
		inline float getEmissionRate( void ) const { return emissionRate; };

		virtual bool setBasedOn( Particle* const pBaseElement );
		virtual Particle* getCopy( void );

		inline uint16 getCurrentActiveElements( void ) const { return currentActiveElements; };
		inline void setCurrentActiveElements( uint16 n ) { currentActiveElements = n; };
	
	protected:
		// Taxa de emissão por segundo
		float emissionRate;
	
		// Quantidade atual de elementos a ser emitida
		float elementsToEmit;
	
		// Lista de elementos
		DynamicVector *pElements;
	
		// Número de elementos ativos atualmente
		uint16 currentActiveElements;
	
		// Elemento base (modelo) para os elementos emitidos
		Particle *pBaseElement;
};

#endif