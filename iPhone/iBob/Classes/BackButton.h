/*
 *  BackButton.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/8/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef BACK_BUTTON_H
#define BACK_BUTTON_H

#include "Point3f.h"
#include "Sprite.h"
#include "Texture2D.h"

class BackButton : public Sprite
{
	public:
		// Construtor
		BackButton( Texture2D *pTexture );
	
		// Testa colisão com o botão que leva o usuário da tela de jogo ao menu
		bool checkCollision( float x, float y );
	
		// Renderiza o objeto
		virtual void render( void );

		// Retorna a posição do objeto
		inline const Point3f* getPosition( void ) const { return &position; };

	private:
		// Posição do objeto
		Point3f position;
};

#endif