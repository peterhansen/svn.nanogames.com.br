/*
 *  Clock.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#if TARGET_IPHONE_SIMULATOR

#ifndef CLOCK_H
#define CLOCK_H

#include <time.h> 

class Clock
{
	public:
		// Marca o tempo atual como o início da contagem de tempo
		static void MarkTime( void );
	
		// Retorna o tempo decorrido desde a última chamada a MarkTime()
		static float GetElapsedTime( void );
	
	private:
		// Contador do tempo decorrido
		static clock_t t;
};

#endif

#endif