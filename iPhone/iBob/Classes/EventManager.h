/*
 *  EventManager.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/12/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include "EventListener.h"
#include "EventTypes.h"
#include "NanoTypes.h"

// Tamanho do array de listeners
#define EVENTMANAGER_LISTENERS_ARRAY_LEN 32

class EventManager
{
	public:
		// Destrutor
		virtual ~EventManager( void );
	
		// Retorna a instância do objeto singleton
		static EventManager* GetInstance( void );
	
		// Acrescenta um objeto ao array de listeners
		bool insertListener( EventListener* pListener );
	
		// Remove o listener recebido do array de listeners
		void removeListener( EventListener* pListener );
	
		// Limpa o array de listeners
		inline void cleanListenerArray( void ) { nListeners = 0; };
	
		// Indica que um evento deve ser tratado pela aplicação
		bool setEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) const;

	protected:
		// Construtor
		EventManager( void );

	private:
		// Ponteiro para a única instância da classe
		static EventManager* pSingleton;
	
		// Número de slots ocupados em eventListeners
		uint8 nListeners;

		// Array de objetos tratadores de eventos
		EventListener* eventListeners[ EVENTMANAGER_LISTENERS_ARRAY_LEN ];
};

#endif
