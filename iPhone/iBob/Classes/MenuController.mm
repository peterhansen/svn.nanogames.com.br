#import "MenuController.h"

#if BLOB_MODE
	#include "ColorsView.h"
#endif

#include "OptionsView.h"

// Início da implementção da classe
@implementation MenuController

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

#if BLOB_MODE
	@synthesize hColorsViewController;
#endif

@synthesize hOptionsViewController;

/*==============================================================================================

MENSAGEM getForceLevel
	Retorna o multiplicador que devemos utilizar sobre as forças geradas pelos acelerômetros.

==============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

- ( float )getForceLevel
{
	return [ ( OptionsView* )[hOptionsViewController view] getForceLevel];
}

#endif

/*==============================================================================================

MENSAGEM getMovementCalcMode
	Retorna o modo de cálculo a ser utilizado na interpretação dos dados dos acelerômetros.

==============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

- ( AccForceCalcMode )getMovementCalcMode
{
	return [( OptionsView* )[hOptionsViewController view] getMovementCalcMode];
}

#endif

/*==============================================================================================

MENSAGEM getColor
	Retorna a cor a ser usada pelo personagem.

==============================================================================================*/

#if BLOB_MODE

- ( Color* )getColor:( Color* )pColor
{
	[( ColorsView* )[hColorsViewController view] getColor:pColor];
	return pColor;
}

#endif

/*==============================================================================================

MENSAGEM getHardness
	Retorna o coeficiente de consistência a ser usado pelo personagem.

==============================================================================================*/

- ( float )getHardness
{
	return [( OptionsView* )[hOptionsViewController view] getHardness];
}

/*==============================================================================================

MENSAGEM getStickness
	Retorna o coeficiente de aderência a ser usado pelo personagem.

==============================================================================================*/

- ( float )getStickness
{
	return [( OptionsView* )[hOptionsViewController view] getStickness];
}

/*==============================================================================================

MENSAGEM getSoundVolume
	Retorna o volume do som.

==============================================================================================*/

- ( float )getSoundVolume
{
	return [( OptionsView* )[hOptionsViewController view] getSoundVolume];
}

/*==============================================================================================

MENSAGEM getSoundVolume
	Retorna a força da gravidade.

==============================================================================================*/

- ( float )getGravity
{
	return [( OptionsView* )[hOptionsViewController view] getGravity];
}

/*==============================================================================================

MENSAGEM mustReset
	Retorna se devemos reinicializar o estado do personagem.

==============================================================================================*/

- ( bool )mustReset
{
	return [( OptionsView* )[hOptionsViewController view] mustReset];
}

/*==============================================================================================

MENSAGEM mustReset
	Indica o estado do botão reset.

==============================================================================================*/

- ( void )setResetValue:( bool ) value
{
	[( OptionsView* )[hOptionsViewController view] setReset: value];
}

// Fim da implementção da classe
@end
