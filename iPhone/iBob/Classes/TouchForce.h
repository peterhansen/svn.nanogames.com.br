/*
 *  TouchForce.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/14/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef TOUCH_FORCE_H
#define TOUCH_FORCE_H

#include "Force.h"

#if ! DRAG_N_DROP_USES_FORCES
	#include "iBobParticle.h"
#endif

class TouchForce : public Force
{
	public:
		// Construtor
		TouchForce( const Point3f *pDirection = NULL );
	
		// Destrutor
		virtual ~TouchForce( void );
	
		// Determina o ID do toque que irá gerar a força.
		// OBS: Objetos UITouch são persistentes durante as etapas de um evento de toque, como
		// touchBegan e touchEnded. Logo, o ID do toque deve ser o endereço do objeto UITouch
		inline void setTouchId( int32 touchId ){ this->touchId = touchId; };
	
		// Obtém o ID do toque que está gerando a força
		inline int32 getTouchId( void ) const { return touchId; };
	
		// Retorna uma cópia deste objeto
		virtual Force* getCopy( void ) const;
	
		#if ! DRAG_N_DROP_USES_FORCES
			// Determina a partícula selecionada pelo toque
			inline void setSelectedParticle( iBobParticle* pParticle ) { pSelectedParticle = pParticle; };
	
			// Indica qual a partícula que está selecionada pelo toque
			inline iBobParticle* getSelectedParticle( void ) const { return pSelectedParticle; };
		#endif
	
	private:
		// ID do toque
		int32 touchId;
	
		#if ! DRAG_N_DROP_USES_FORCES
			// Partícula selecionada pelo toque ou NULL em caso do toque estar gerando uma força
			// para o modo INPUT_MODE_CATCH_MY_FINGER
			iBobParticle* pSelectedParticle;
		#endif
	
};

#endif
