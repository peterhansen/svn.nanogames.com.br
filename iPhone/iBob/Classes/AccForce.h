/*
 *  AccForce.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/9/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACC_FORCE_H
#define ACC_FORCE_H

#include "Force.h"
#include "AccListener.h"

// Tempo de duração da força do acelerômetro (em segundos)
#define ACCEL_HISTORY_LEN 10

typedef enum
{
	ACC_FORCE_MODE_RAW = 0,
	ACC_FORCE_MODE_GRAVITY,
	ACC_FORCE_MODE_GRAVITY_VEC,
	ACC_FORCE_MODE_MOVEMENT,
	ACC_FORCE_MODE_AVERAGE,
	ACC_FORCE_MODE_POLINOMIAL_2,
	ACC_FORCE_MODE_POLINOMIAL_3,
	ACC_FORCE_MODE_POLINOMIAL_4
}AccForceCalcMode;

class AccForce : public Force, public AccListener
{
	public:
		// Construtor
		// Exceptions: InvalidOperationException
		AccForce( AccForceCalcMode mode, float booster, float gravityFilteringFactor = 0.1f );
	
		// Destrutor
		virtual ~AccForce( void );
	
		// Determina a porcentagem da aceleração que corresponde ao efeito da gravidade
		inline void setGravityFilteringFactor( float factor ) { filteringFactor = factor; };
	
		// Obtém a porcentagem da aceleração que corresponde ao efeito da gravidade
		inline float getGravityFilteringFactor( void ) const { return filteringFactor; };
	
		// Determina o multiplicador que será aplicado na força reportada pelos acelerômetros
		inline void setAccelerometerBooster( float booster ){ this->booster = booster; };
	
		// Obtém o multiplicador que está sendo aplicado na força reportada pelos acelerômetros
		inline float getAccelerometerBooster( void ) const { return booster; };
	
		// Determina o modo de cálculo da força
		inline void setAccelerometerCalcMode( AccForceCalcMode mode ) { acceleration[0] = 0.0f; acceleration[1] = 0.0f; acceleration[2] = 0.0f; this->mode = mode; }
	
		// Obtém o modo de cálculo da força
		inline AccForceCalcMode getAccelerometerCalcMode( void ) const { return mode; };
	
		// Recebe os eventos do acelerômetro
		virtual void OnAccelerate( const AccManager* pAccelerometer, const Point3f* pAcceleration );

	private:
		// Armazena o hitórico da aceleração
		void setHistory( const Point3f* pNewAccelData, Point3f* pLastValue = NULL );
	
		// Modo de cálculo da força baseada no acelerômetro
		AccForceCalcMode mode;
	
		// Índice deste objeto no vetor de listener de AccManager
		uint8 myAccManagerListenerIndex;
	
		// Armazena os últimos valores recebidos
		uint8 accelHistoryCounter;
		float accelHistory[3][ ACCEL_HISTORY_LEN ];

		// Porcentagem da aceleração que corresponde ao efeito da gravidade
		float filteringFactor;
	
		// Multiplicador que será aplicado na força reportada pelos acelerômetros 
		float booster;

		// Aceleração nos 3 eixos
		float acceleration[3];
};

#endif
