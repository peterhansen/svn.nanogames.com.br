#include "Texture2D.h"
#include "Macros.h"

// Inicializa a variável estática da classe
int16 Texture2D::counter = 0;

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Texture2D::Texture2D( LoadableListener *pListener, uint32 loadableId ) : Texture( NULL, loadableId ), pTexData( NULL ), pUserListener( pListener ), width( 0 ), height( 0 )
{
	if( counter <= 0 )
	{
		counter = 1;
		glEnable( GL_TEXTURE_2D );
	}
	else
	{
		++counter;
	}
	setListener( this );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Texture2D::~Texture2D( void )
{
	SAFE_DELETE_VEC( pTexData );

	--counter;

	if( counter <= 0  )
		glDisable( GL_TEXTURE_2D );
}

/*==============================================================================================

MÉTODO loadTextureInFile
	Carrega a textura bitmap de um arquivo.

==============================================================================================*/

bool Texture2D::loadTextureInFile( const NSString *pFileName )
{
	// Obtém a extensão do arquivo
	NSRange aux = [pFileName rangeOfString: @"." options: NSBackwardsSearch];
	
	NSString* pFilePath;
	NSString* pFileType;
	if( aux.location != NSNotFound )
	{
		pFilePath = [pFileName substringToIndex: aux.location ];
		pFileType = [pFileName substringFromIndex: aux.location + 1 ];
	}
	else
	{
		// Assume PNG
		pFilePath = pFileName;
		pFileType = @"png";
	}
	
	return loadTexture( [[UIImage imageWithContentsOfFile: [[NSBundle mainBundle] pathForResource: pFilePath ofType: pFileType ] ] CGImage] );
}

/*==============================================================================================

 MÉTODO loadTextureWithData
	Carrega a textura a partir dos dados recebidos como parâmetro.
 
==============================================================================================*/

bool Texture2D::loadTextureWithData( const NSData* pData )
{
	return loadTexture( [[UIImage imageWithData: pData ] CGImage] );
}

/*==============================================================================================

 MÉTODO getTextureData
	Retorna o array de cores da textura.
 
==============================================================================================*/

const uint8* Texture2D::getTextureData( void ) const
{
	return pTexData;
}

/*==============================================================================================

 MÉTODO loadTexture
	Cria uma textura utilizando a imagem recebida.
 
==============================================================================================*/

bool Texture2D::loadTexture( CGImageRef pImage )
{
	if( pImage )
	{		
		// Verifica se as dimensões da textura são potências de 2
		width = CGImageGetWidth( pImage );
		height = CGImageGetHeight( pImage );
		
		if( !IS_POW_OF_2( width ) || !IS_POW_OF_2( height ) )
		{
#if TARGET_IPHONE_SIMULATOR
			LOG( @"As dimensões da textura não são potências de 2. Width = %d, Height = %d\n", width, height );
#endif
			goto error;
		}
		
		{ // Evita problemas com o goto

			// Aloca a memória necessária para conter o bitmap da imagem
			pTexData = new uint8[ ( width * height ) << 2 ];

			// Preenche o bitmap
			CGContextRef texContext = CGBitmapContextCreate( pTexData, width, height, 8, width << 2, CGImageGetColorSpace( pImage ), kCGImageAlphaPremultipliedLast );
			CGContextDrawImage( texContext, CGRectMake( 0.0f, 0.0f, ( CGFloat )width, ( CGFloat )height ), pImage );
			CGContextRelease( texContext );

			// Especifica a imagem da textura 2D, informando o endereço dos dados na memória através do ponteiro texData
			glBindTexture( GL_TEXTURE_2D, texId );
			
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
			
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL );
			
			// Impede que a cor da textura seja multiplicada com a cor de fundo
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pTexData );

			return true;
		
		} // Evita problemas com o goto
		
		error:
			CGImageRelease( pImage );
			SAFE_DELETE( pTexData );
			return false;
	}
	return false;
}

/*==============================================================================================

 MÉTODO loadableHandleEvent
	Método para receber os eventos do objeto loadable.
 
==============================================================================================*/

void Texture2D::loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data )
{
	if( op == LOADABLE_OP_LOAD )
		glBindTexture( GL_TEXTURE_2D, data );

	if( pUserListener )
		pUserListener->loadableHandleEvent( op, loadableId, data );
}
