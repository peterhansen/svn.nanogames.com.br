#ifndef SCENE_H
#define SCENE_H

#include "World.h"
#include "DynamicVector.h"
#include "Macros.h"
#include "Camera.h"

#include "Loadable.h"

class Scene : public Loadable
{
	public:
		// Construtor
		Scene( LoadableListener* pListener, uint32 id );

		// Destrutor
		virtual ~Scene( void );

		// Insere um novo mundo físico na cena
		int16 insertWorld( World *pWorld );
	
		// Remove da cena o mundo físico que possui o índice passado como parâmetro
		bool removeWorldAt( uint16 index );
	
		// Obtém o mundo físico com o índice passado como parâmetro
		World* getWorldAt( uint16 index );

		// Renderiza o objeto
		virtual void render( void );
	
		// Atualiza o objeto
		virtual void update( double timeElapsed );

		// Determina a câmera atual
		void setCurrentCamera( Camera* camera );
	
		// Obtém a câmera atual
		Camera* getCurrentCamera( void );
	
	protected:
		DynamicVector* pWorlds;
	
		// Câmera que está sendo utilizada para renderizar a cena
		Camera* pCurrCamera;
};

#endif