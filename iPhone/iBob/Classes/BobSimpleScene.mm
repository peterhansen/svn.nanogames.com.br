/*
 *  BobSimpleCube.cpp
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#include "EAGLView.h"
#include "BobSimpleScene.h"

#define LOADABLE_SCENE_IND	0
#define LOADABLE_BOB_IND	1

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

BobSimpleScene::BobSimpleScene( void ) : Scene( NULL, LOADABLE_SCENE_IND ), pBobTexture( NULL ), angle( 0.0 )
{
	setListener( this );
}
	
/*==============================================================================================

DESTRUTOR

==============================================================================================*/

BobSimpleScene::~BobSimpleScene( void )
{
	SAFE_DELETE( pBobTexture );
}

/*==============================================================================================

MÉTODO CreateInstance
	Retorna uma instância da classe.

==============================================================================================*/

BobSimpleScene* BobSimpleScene::CreateInstance( void )
{
	BobSimpleScene* pObj = new BobSimpleScene();
	if( pObj )
	{
		if( !pObj->build() )
			DESTROY( pObj );
	}
	return pObj;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void BobSimpleScene::render( void )
{
	load();
	pBobTexture->load();

	Camera* pMyCamera = getCurrentCamera();
	if( pMyCamera )
		pMyCamera->place();

	glRotatef( angle, 1.0f, 0.0f, 0.0f );
	glRotatef( angle, 0.0f, 1.0f, 0.0f );
	
	angle += 1.0;

	const GLfloat mySize = 1.0f;
	const GLfloat halfSize = ( mySize / 2 );
	
	const GLfloat cubeVertices[] =
	{
		 halfSize,  halfSize, -halfSize, // 0 - Cima trás dir
		-halfSize,  halfSize, -halfSize, // 1 - Cima trás esq
		 halfSize, -halfSize, -halfSize, // 2 - Baixo trás dir
		-halfSize, -halfSize, -halfSize, // 3 - Baixo trás esq
		 halfSize,  halfSize,  halfSize, // 4 - Cima frente dir
		-halfSize,  halfSize,  halfSize, // 5 - Cima frente esq
		-halfSize, -halfSize,  halfSize, // 6 - Baixo frente esq
		 halfSize, -halfSize,  halfSize, // 7 - Baixo frente dir
		-halfSize,  halfSize,  halfSize, // 8 - Cópia de 5: Workaround para o mapeamento de textura
		-halfSize, -halfSize,  halfSize, // 9 - Cópia de 6: Workaround para o mapeamento de textura
		 halfSize, -halfSize, -halfSize, //10 - Cópia de 2: Workaround para o mapeamento de textura
		-halfSize, -halfSize, -halfSize, //11 - Cópia de 3: Workaround para o mapeamento de textura
		 halfSize,  halfSize,  halfSize, //12 - Cópia de 4: Workaround para o mapeamento de textura
		 halfSize,  halfSize, -halfSize, //13 - Cópia de 0: Workaround para o mapeamento de textura
		-halfSize,  halfSize, -halfSize, //14 - Cópia de 1: Workaround para o mapeamento de textura
		 halfSize, -halfSize,  halfSize  //15 - Cópia de 7: Workaround para o mapeamento de textura
	};

	const GLfloat cubeTexCoords[] =
	{
		0.50f, 1.0f, // Cima trás dir
		0.75f, 1.0f, // Cima trás esq
		0.50f, 0.0f, // Baixo trás dir
		0.75f, 0.0f, // Baixo trás esq
		0.25f, 1.0f, // Cima frente dir
		0.00f, 1.0f, // Cima frente esq
		0.00f, 0.0f, // Baixo frente esq
		0.25f, 0.0f, // Baixo frente dir
		1.00f, 1.0f, // Fecha o ciclo
		1.00f, 0.0f  // Fecha o ciclo
	};
	
	glVertexPointer( 3, GL_FLOAT, 0, cubeVertices );
	
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	glTexCoordPointer( 2, GL_FLOAT, 0, cubeTexCoords );
	
	const uint8 front[] = { 6, 7, 5, 4 };
	glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, front );
	
	const uint8 back[] = { 1, 0, 3, 2 };
	glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, back );
	
	const uint8 right[] = { 2, 0, 7, 4 };
	glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, right );
	
	const uint8 left[] = { 1, 3, 8, 9 };
	glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, left );
	
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	
	const uint8 bottom[] = { 11, 10, 9, 15 };
	glColor4ub( 201, 127, 28, 255 );
	glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, bottom );
	
	const uint8 top[] = { 8, 12, 14, 13 };
	glColor4ub( 255, 242, 69, 255 );
	glDrawElements( GL_TRIANGLE_STRIP, 4, GL_UNSIGNED_BYTE, top );
	
	pBobTexture->unload();
	unload();
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

void BobSimpleScene::update( double timeElapsed )
{
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool BobSimpleScene::build( void )
{
//	const Point3f center = Point3f(), eye = Point3f( 0.0f, 0.0f, 5.0f ), up = Point3f( 0.0f, 1.0f, 0.0f );
	PerspectiveCamera *pCamera = new PerspectiveCamera();
	if( !pCamera )
		return false;

	setCurrentCamera( pCamera );
	
	const Point3f cameraPos( 0.0f, 0.0f, 5.0f );
	pCamera->setPosition( &cameraPos );
	
	pBobTexture = new Texture2D( this, LOADABLE_BOB_IND );
	if( !pBobTexture || !pBobTexture->loadTextureFile( @"bob.png" ) )
		return false;
	return true;
}
		
/*==============================================================================================

MÉTODO loadableHandleEvent
	Método para receber os eventos do objeto loadable.

==============================================================================================*/

void BobSimpleScene::loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data )
{
	switch ( loadableId )
	{
		case LOADABLE_SCENE_IND:
			if( op == LOADABLE_OP_LOAD )
			{
				glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
				glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
				glEnable( GL_DEPTH_TEST );
				glEnableClientState( GL_VERTEX_ARRAY );
				glEnableClientState( GL_TEXTURE_COORD_ARRAY );

				glDisable( GL_LIGHTING );
				glDisable( GL_BLEND );
				
				glShadeModel( GL_FLAT );
				
				// Configura as texturas uniformemente
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
				
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
				glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
				
				// Impede que a cor da textura seja multiplicada com a cor de fundo
				glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL );
			}
			else
			{
			}
			break;
			
		case LOADABLE_BOB_IND:
			if( op == LOADABLE_OP_LOAD )
			{
			}
			else
			{
			}
			break;
	}
}

