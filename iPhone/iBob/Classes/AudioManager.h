/*
 *  AudioManager.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/2/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H

#include "CaptureAudioDevice.h"
#include "PlaybackAudioDevice.h"

class AudioManager
{
	public:
		// TASK : Virar Singleton
		// Destrutor do singleton
		//static void destroy( void );
	
		// Contrutor
		// Exceções : OpenALException
		AudioManager( void );
	
		// Destrutor
		~AudioManager( void );
	
		// Retorna o device de reprodução de sons
		PlaybackAudioDevice* getPlaybackDevice( void ) const { return pPlaybackDevice; };
	
		// Retorna o device de captura de sons
		CaptureAudioDevice* getCaptureDevice( void ) const { return pCaptureDevice; };

		// Indica se a plataforma suporta captura de áudio
		bool supportsCapture( void );

		// Indica se a plataforma suporta efeitos sonoros
		bool supportsReverbAndEffects( void );
	
		// Suspende a execução dos devices existentes
		bool suspend( void ) const;
	
		// Resume a execução dos devices existentes
		bool resume( void ) const;
	
	private:
		// TASK : Virar Singleton
		// Construtor
		//AudioManager( void );

		// Device de playback de sons
		PlaybackAudioDevice* pPlaybackDevice;
	
		// Device de captura de sons
		CaptureAudioDevice* pCaptureDevice;
};

#endif