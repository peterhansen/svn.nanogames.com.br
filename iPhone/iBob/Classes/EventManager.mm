#include "EventManager.h"

// Inicializa as variáveis estáticas da classe
EventManager* EventManager::pSingleton = NULL;

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

EventManager::EventManager( void ) : nListeners( 0 )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

EventManager::~EventManager( void )
{
	nListeners = 0;
}

/*==============================================================================================

MÉTODO GetInstance
	Retorna a instância do objeto singleton.

==============================================================================================*/

EventManager* EventManager::GetInstance( void )
{
	if( !pSingleton )
		pSingleton = new EventManager();

	return pSingleton;
}

/*==============================================================================================

MÉTODO insertListener
	Acrescenta um objeto ao array de listeners.

==============================================================================================*/

bool EventManager::insertListener( EventListener* pHandler )
{
	if( nListeners < EVENTMANAGER_LISTENERS_ARRAY_LEN )
	{
		eventListeners[ nListeners++ ] = pHandler;
		return true;
	}
	return false;
}
/*==============================================================================================

MÉTODO removeListener
	Remove o listener recebido do array de listeners.

==============================================================================================*/

void EventManager::removeListener( EventListener* pListener )
{
	uint8 index = 0;
	for( ; index < nListeners ; ++index )
	{
		if( eventListeners[index] == pListener )
		{
			--nListeners;
			break;
		}
	}
	
	// "Chega pra cá"
	while( index < nListeners )
		eventListeners[index] = eventListeners[ ++index ];
}

/*==============================================================================================

MÉTODO setEvent
	Indica que um evento deve ser tratado pela aplicação.

==============================================================================================*/

bool EventManager::setEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) const
{
	bool ret = false;

	for( uint8 i = 0 ; i < nListeners ; ++i )
	{
		if( eventListeners[i]->isListeningTo( evtType ) )
			ret |= eventListeners[i]->handleEvent( evtType, evtSpecific, pParam );
	}

	return ret;
}
