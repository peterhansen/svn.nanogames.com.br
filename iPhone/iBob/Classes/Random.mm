#include "Random.h"
#include "SFMT.h"
#include "Macros.h"

#define DEFAULT_SEED [[NSDate date] timeIntervalSince1970]

// Inicializa a variável estática da classe
bool Random::seeded = false;

/*===========================================================================================

MÉTODO SetSeed
	Determina o inicializador do gerador de números randômicos.

============================================================================================*/

void Random::SetSeed( uint32 s )
{
	init_gen_rand( s );
	seeded = true;
}

/*===========================================================================================

MÉTODO NextDouble
	Obtém um número randômico real.

============================================================================================*/

float Random::NextFloat( void )
{
	if( !seeded )
		SetSeed( DEFAULT_SEED );

	return ( float )gen_rand32();
}

/*===========================================================================================

MÉTODO NextDouble
	Obtém um número randômico real no intervalo [0.0f, maxValue]
	Equivale a chamar NextDouble( 0.0f, maxValue )

============================================================================================*/

float Random::NextFloat( float maxValue )
{
	return NextFloat( 0.0f, maxValue );
}

/*===========================================================================================

MÉTODO NextDouble
	Obtém um número randômico real no intervalo definido.

============================================================================================*/

float Random::NextFloat( float minValue, float maxValue )
{
	if( !seeded )
		SetSeed( DEFAULT_SEED );

	// [ 0.0f, 1.0f ]
	const float v = ( float )genrand_real1();
	return LERP( v, minValue, maxValue );
}
