//
//  MenuView.m
//  iBob
//
//  Created by Daniel Lopes Alves on 10/10/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#import "MenuView.h"
#import "iBobAppDelegate.h"

@implementation MenuView

- ( void )dealloc
{
	[hSensibility release];
	[hForceLevel release];
	[hSoftness release];
	[hStickness release];
	[hMovementCalcMode release];
	[super dealloc];
}

- (IBAction)onBtClicked
{
	[((iBobAppDelegate* )APP_DELEGATE ) performTransition];
}

- ( float )getSensibility
{
	return [hSensibility value];
}

- ( float )getForceLevel
{
	return [hForceLevel value];
}

- ( float )getStickness
{
	return [hStickness value];
}

- ( float )getSoftness
{
	return [hSoftness value];
}

- ( AccForceCalcMode )getMovementCalcMode
{
	return ( AccForceCalcMode )( ACC_FORCE_MODE_AVERAGE + ( uint8 )[hMovementCalcMode selectedSegmentIndex] );
}

@end
