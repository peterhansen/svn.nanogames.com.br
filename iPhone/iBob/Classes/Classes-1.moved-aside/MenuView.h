//
//  MenuView.h
//  iBob
//
//  Created by Daniel Lopes Alves on 10/10/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "NanoTypes.h"
#include "AccForce.h"

@interface MenuView : UIView
{
	@private
		IBOutlet UISlider* hSoftness;
		IBOutlet UISlider* hStickness;
		IBOutlet UISlider* hSensibility;
		IBOutlet UISlider* hForceLevel;
		IBOutlet UISegmentedControl* hMovementCalcMode;
}

- ( IBAction )onBtClicked;

- ( float )getStickness;
- ( float )getSoftness;
- ( float )getSensibility;
- ( float )getForceLevel;
- ( AccForceCalcMode )getMovementCalcMode;

@end
