#import "iBobAppDelegate.h"
#include "AccManager.h"
#include "Config.h"

#import <QuartzCore/QuartzCore.h>

// Extensão da classe para declarar métodos privados
@interface iBobAppDelegate ()

// Carrega as views do jogo
-( void )loadAll;

@end

// Implementação da classe
@implementation iBobAppDelegate

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hWindow, hViewManager, hTabBarController;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/

- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	// Indica que este objeto irá receber os eventos da aplicação
	[hViewManager setDelegate:self];

	// Cria a tela de loading
	UIActivityIndicatorView* hLoadingIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)]; 
	[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT)]; 
	[hLoadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge]; 
	[hViewManager addSubview:hLoadingIndicator];
	[hLoadingIndicator startAnimating];
	[hLoadingIndicator release];
	
	// Timer que irá carregar as views do jogo
	[NSTimer scheduledTimerWithTimeInterval:0.001f target:self selector:@selector(loadAll) userInfo:NULL repeats:NO];
}

/*==============================================================================================

MENSAGEM loadAll
	Carrega as views do jogo.

==============================================================================================*/

-( void )loadAll
{
	// OBS: O Menu é criado pelo arquivo MainWndowFinal.xib !!!

	// Cria as views dos splashs
	CGRect screenSize = [[UIScreen mainScreen] bounds];
	hSplashNano = ( SplashNano* )Utils::LoadViewFromXib( @"SplashNano" );
	hSplashGame = [[SplashGame alloc] initWithFrame: screenSize];
	
	// Cria a view do jogo (view OpenGL)
	hGLView = [[EAGLView alloc] initWithFrame:screenSize]; 
	
	// Verifica se conseguiu alocar todas as views necessárias
	if( ( hGLView == NULL ) || ( hSplashNano == NULL ) || ( hSplashGame == NULL ) )
	{
		#if TARGET_IPHONE_SIMULATOR
			LOG( @"Nao conseguiu alocar todas as views do jogo\n" );
		#endif

		// Termina a aplicação
		#if TARGET_IPHONE_SIMULATOR
			[self quit: ERROR_ALLOCATING_VIEWS];
		#else
			[self quit];
		#endif
		return;
	}
	
	// Exibe a primeira view da aplicação
	[self performTransitionToView: VIEW_INDEX_SPLASH_NANO];
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS : Descomentar se for necessário personalizar
//- ( void )dealloc
//{	
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM applicationWillResignActive
	Mensagem chamada quando a aplicação vai ser suspensa.

==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	// Se estamos na view OpenGL e a aplicação vai ser suspensa, pára a animação
	if( currViewIndex == VIEW_INDEX_GAME )
		[hGLView suspend];
}

/*==============================================================================================

MENSAGEM applicationDidBecomeActive
	Mensagem chamada quando a aplicação é reiniciada.

==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	if( currViewIndex == VIEW_INDEX_GAME )
		[hGLView resume];
}

/*==============================================================================================

MENSAGEM applicationWillTerminate
	Tells the delegate when the application is about to terminate. This method is optional. This
method is the ideal place for the delegate to perform clean-up tasks, such as freeing allocated
memory, invalidating timers, and storing application state.

==============================================================================================*/

- ( void )applicationWillTerminate:( UIApplication* )application
{
#if TARGET_IPHONE_SIMULATOR
	LOG( @"applicationWillTerminate" );
#endif
	
	// NÃO MUDAR A ORDEM DAS DECLARAÇÕES!!!!!!!!!!!!!!!!!!!

	// Suspende o processamento da tela atual
	[self applicationWillResignActive: application];
	
	// Desaloca a tela de joga
	KILL( hGLView );

	// Desaloca o controlador de telas
	KILL( hViewManager );
	
	// Desaloca as telas da aplicação
	SAFE_RELEASE( hSplashNano );
	SAFE_RELEASE( hSplashGame );

	KILL( hTabBarController );
	
	// Desaloca os singletons
	AccManager::release();
	
	// Desaloca a janela da aplicação
	KILL( hWindow );
}

/*==============================================================================================

MENSAGEM applicationDidReceiveMemoryWarning
	Tells the delegate when the application receives a memory warning from the system. This
method is optional. In this method, the delegate tries to free up as much memory as possible.
After the method returns (and the delegate then returns from applicationWillTerminate), the
application is terminated.

==============================================================================================*/

- ( void )applicationDidReceiveMemoryWarning:( UIApplication* )application
{
#if TARGET_IPHONE_SIMULATOR
	LOG( @"applicationDidReceiveMemoryWarning" );
	
	[self quit: ERROR_MEMORY_WARNING];
#else
	[self quit];
#endif
}

/*==============================================================================================

MENSAGEM applicationSignificantTimeChange
	Tells the delegate when there is a significant change in the time. This method is optional.
Examples of significant time changes include the arrival of midnight, an update of the time by
a carrier, and the change to daylight savings time. The delegate can implement this method to
adjust any object of the application displays time or is sensitive to time changes.

==============================================================================================*/

- ( void )applicationSignificantTimeChange:( UIApplication* )application
{
#if TARGET_IPHONE_SIMULATOR
	LOG( @"applicationSignificantTimeChange" );
#endif
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

// Executa a transição da view atual para a view de índice passado como parâmetro
- ( void )performTransitionToView:( uint8 )newIndex;
{
	// Não interrompe uma transição
	if( [hViewManager isTransitioning] )
		return;

    // TASK : Fazer através de controlers e não diretamente das views
	UIView* hNextView;
	NSString *hTransition = nil, *hDirection = nil;
	switch( newIndex )
	{
		case VIEW_INDEX_SPLASH_NANO:
			{
				// Obtém o UIActivityIndicatorView e o deixa invisível
				UIActivityIndicatorView* hLoading = [[hViewManager subviews] objectAtIndex:0];
				[hLoading setHidden: YES];
				
				// Exibe o Splash da Nano Games. A primeira view exibida não realiza transições
				[hViewManager addSubview: hSplashNano];
				[hSplashNano onBecomeCurrentScreen];
				[hSplashNano release];
				
				// Destrói o UIActivityIndicatorView
				[hLoading removeFromSuperview];
				
				currViewIndex = newIndex;
			}
			return;
			
		case VIEW_INDEX_SPLASH_GAME:
			hNextView = hSplashGame;
			hTransition = kCATransitionFade;
			break;
			
		case VIEW_INDEX_MAIN_MENU:
			{
				hNextView = [hTabBarController view];

				[hGLView suspend];

				// Indica qual item do menu começará selecionado
				[hTabBarController setSelectedIndex: DEFAULT_MENU_OPT];
				
				// Reinicializa o estado do botão reset
				[hTabBarController setResetValue: false];
				
				if( currViewIndex == VIEW_INDEX_SPLASH_GAME )
				{
					hTransition = @"pageCurl";
				}
				else // if( currViewIndex == VIEW_INDEX_GAME )
				{
					hTransition = kCATransitionPush;
					hDirection = kCATransitionFromLeft;
				}
			}
			break;
			
		case VIEW_INDEX_GAME:
			{
				hNextView = hGLView;

				#if BLOB_MODE
					Color aux;
					#if TARGET_IPHONE_SIMULATOR
						[hGLView setOpts: [hTabBarController mustReset] blobColor:[hTabBarController getColor: &aux] hardness:[hTabBarController getHardness] stickness:[hTabBarController getStickness] accelForceLevel:[hTabBarController getForceLevel] movCalcMode:[hTabBarController getMovementCalcMode] volume:[hTabBarController getSoundVolume] gravity:[hTabBarController getGravity]];
					#else
						[hGLView setOpts: [hTabBarController mustReset] blobColor:[hTabBarController getColor: &aux] hardness:[hTabBarController getHardness] stickness:[hTabBarController getStickness] volume:[hTabBarController getSoundVolume] gravity:[hTabBarController getGravity]];
					#endif
				#else
					#if TARGET_IPHONE_SIMULATOR
						[hGLView setOpts: [hTabBarController mustReset] hardness:[hTabBarController getHardness] stickness:[hTabBarController getStickness] accelForceLevel:[hTabBarController getForceLevel] movCalcMode:[hTabBarController getMovementCalcMode] volume:[hTabBarController getSoundVolume] gravity:[hTabBarController getGravity]];
					#else
						[hGLView setOpts: [hTabBarController mustReset] hardness:[hTabBarController getHardness] stickness:[hTabBarController getStickness] volume:[hTabBarController getSoundVolume] gravity:[hTabBarController getGravity]];
					#endif
				#endif

				// Renderiza a view do jogo sem o jogador ver, já utilizando as novas opções do menu. Assim
				// garantimos que ele não verá a imagem anterior durante a transição
				[hGLView render];
				
				hTransition = kCATransitionPush;
				hDirection = kCATransitionFromRight;
			}
			break;
			
		default:
			#if TARGET_IPHONE_SIMULATOR
				LOG( @"AppDelegate::performTransitionToView() - Indice recebido eh inexistente\n" );
			#endif
			return;
	}

	// Executa a transição de views
	[hViewManager transitionFromSubview: [[hViewManager subviews] objectAtIndex: 0] toSubview: hNextView transition: hTransition direction: hDirection duration: DEFAULT_TRANSITION_DURATION ];
	
	// Atualiza o índice da view que está sendo exibida
	currViewIndex = newIndex;
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

- ( void )transitionDidStart:( ViewManager* )manager
{
}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	switch( currViewIndex )
	{
		case VIEW_INDEX_SPLASH_GAME:
			[hSplashGame onBecomeCurrentScreen];

			// Já que o splash da nano não está mais visível e não o utilizaremos novamente,
			// retira-o da memória
			SAFE_RELEASE( hSplashNano );
			break;
			
		case VIEW_INDEX_MAIN_MENU:
			// Já que o splash do jogo não está mais visível e não o utilizaremos novamente,
			// retira-o da memória
			SAFE_RELEASE( hSplashGame );
			break;
			
		case VIEW_INDEX_GAME:			
			// Reinicia os componentes do jogo
			[hGLView resume];
			break;
	}
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// TODO : Confirmar se a condição do if é testada desse jeito
	// Se estávamos saindo da tela de jogo, retoma o processamento dos componentes
	if( currViewIndex == VIEW_INDEX_MAIN_MENU )
		[hGLView resume];
}

/*==============================================================================================

MENSAGEM tabBarControllerDidSelectViewController
	Evento chamado quando o usuário seleciona um item da barra de tabs.

==============================================================================================*/

- ( void )tabBarController:( UITabBarController* )tabBarController didSelectViewController:( UIViewController* )viewController
{
	// Se estamos indo para o jogo...
	if( [tabBarController selectedIndex] == MENU_GAME_INDEX )
		[self performTransitionToView: VIEW_INDEX_GAME];
}

/*==============================================================================================

MENSAGEM quit
	Termina a aplicação.

==============================================================================================*/

#if TARGET_IPHONE_SIMULATOR
- ( void )quit:( uint32 )error
#else
- ( void )quit
#endif
{
#if TARGET_IPHONE_SIMULATOR
	LOG( @"ERROR: Quiting! Reason: %d\n", error );
#endif
	
	UIApplication *app = [UIApplication sharedApplication];
	SEL selector = @selector( terminate );

	if( [app respondsToSelector:selector] )
	{
		[app performSelector:selector];
	}
	else
	{
		[self applicationWillTerminate: app];
		exit(0);
	}
}

// Fim da implementação da classe
@end
