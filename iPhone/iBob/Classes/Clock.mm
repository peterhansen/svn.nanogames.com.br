#if TARGET_IPHONE_SIMULATOR

#include "Clock.h"

// Inicializa a variável estática da classe
clock_t Clock::t = 0;

/*===========================================================================================

MÉTODO MarkTime
	Marca o tempo atual como o início da contagem de tempo.

============================================================================================*/

void Clock::MarkTime( void )
{
	t = clock();
}

/*===========================================================================================

MÉTODO GetElapsedTime
	Retorna o tempo decorrido desde a última chamada a MarkTime().

============================================================================================*/

float Clock::GetElapsedTime( void )
{
	return ( float )( clock() - t ) / CLOCKS_PER_SEC;
}

#endif

