#if RENDER_TEST_SCENE

#include "TestScene.h"
#include "Macros.h"
#include "Camera.h"
#include "World.h"
#include "Environment.h"
#include "Sphere.h"

// Tamanho do mundo da cena
#define WORLD_DIMENSION 500.0f

TestScene::TestScene( void ) : Scene( NULL, 0 )
{
	build();
	setListener( this );
}

TestScene::~TestScene( void )
{
	pParticle = NULL;
}

bool TestScene::build( void )
{
	// Aloca a câmera da cena	
	PerspectiveCamera *pCamera = new PerspectiveCamera( 90.0f, ( float )( SCREEN_WIDTH / SCREEN_HEIGHT ), 1.0f, WORLD_DIMENSION + 2000.0f, CAMERA_TYPE_AIR );
	if( !pCamera )
		return false;

	setCurrentCamera( pCamera );
	
	const Point3f cameraPos( 0.0f, 0.0f, 500.0f );
	pCamera->setPosition( &cameraPos );	
	
	// Cria o mundo para conter os ambientes físicos
	World *pWorld = new World();
	if( !pWorld || ( insertWorld( pWorld ) < 0 ) )
	{
		SAFE_DELETE( pWorld );
		return false;
	}
	
	// Cria o ambiente físico da cena
	Environment* pEnvironment = new Environment();
	if( !pEnvironment || ( pWorld->insertEnvironment( pEnvironment ) < 0 ) )
	{
		SAFE_DELETE( pEnvironment );
		return false;
	}
	
	pParticle = new Particle();
	if( !pParticle || ( pEnvironment->insertParticle( pParticle ) < 0 ) )
	{
		SAFE_DELETE( pParticle );
		return false;
	}
	
	Color c( COLOR_ORANGE );
	Sphere *pSphere = new Sphere( 10.0f, 30, &c );
	if( !pSphere )
		return false;
	
	pParticle->setShape( pSphere );
	pParticle->setImmortal( true );
	pParticle->setSize( 1.0f );
	
	return true;
}

void TestScene::loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data )
{
	switch( loadableId )
	{
		case 0:
			if( op == LOADABLE_OP_LOAD )
			{
				glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
				glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
				glEnable( GL_DEPTH_TEST );
				glEnableClientState( GL_VERTEX_ARRAY );
				glEnableClientState( GL_TEXTURE_COORD_ARRAY );

				glDisable( GL_LIGHTING );
				glDisable( GL_BLEND );
				
				glShadeModel( GL_FLAT );
			}
			break;
	}
}

#endif
