/*
 *  EventListener.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/12/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EVENT_LISTENER_H
#define EVENT_LISTENER_H

#include "EventTypes.h"
#include "NanoTypes.h"

class EventListener
{
	public:
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) = 0;

		// Indica que o listener deve começar a receber eventos dos tipos "eventTypes"
		inline void startListeningTo( EventTypes eventTypes ) { listeningTo |= eventTypes; };

		// Indica que o listener deve parar de receber eventos dos tipos "eventTypes"
		inline void stopListeningTo( EventTypes eventTypes ) { listeningTo &= ~eventTypes; };

		// Indica se o listener está esperando eventos dos tipos "eventTypes"
		inline bool isListeningTo( EventTypes eventTypes ) const { return ( listeningTo & eventTypes ) == eventTypes; };

	private:
		// Indica por quais tipos de eventos o listener está esperando
		// listeningTo é declarada como int32 ao invés de EventTypes para evitar o problema de conversão de int para enum
		int32 listeningTo;
};

#endif
