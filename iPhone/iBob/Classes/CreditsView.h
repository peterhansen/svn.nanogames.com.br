//
//  CreditsView.h
//  iBob
//
//  Created by Daniel Lopes Alves on 12/19/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef CREDITS_VIEW_H
#define CREDITS_VIEW_H

#import <UIKit/UIKit.h>


@interface CreditsView : UIView
{
}

// Abandona a aplicação e abre o navegador no site da nanogames
- ( IBAction ) onGoToURL;

@end

#endif

