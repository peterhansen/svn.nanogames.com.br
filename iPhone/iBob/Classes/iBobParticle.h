#ifndef IBOB_PARTICLE
#define IBOB_PARTICLE

/*==============================================================================================

CLASSE iBobParticle

==============================================================================================*/

#include "Config.h"
#include "Particle.h"

#if DRAG_N_DROP_USES_FORCES
	#include "TouchForce.h"
#endif

class iBobParticle : public Particle
{
	public:
		// Construtor
		#if DRAG_N_DROP_USES_FORCES
			iBobParticle( TouchForce* touchForces[], const Point3f *pPosition = NULL, const Point3f *pSpeed = NULL, float stickingFactor = 0.0f );
		#else
			iBobParticle( const Point3f *pPosition = NULL, const Point3f *pSpeed = NULL, float stickingFactor = 0.0f );
		#endif
	
		// Destrutor
		virtual ~iBobParticle( void );

		// Inicializa o objeto
		bool build( void );
	
		// Atualiza o objeto
		virtual bool update( double timeElapsed, const DynamicVector *pExternForces, Plane* pCollisionPlane = NULL );
		
		// Retorna o número de partículas vizinhas
		inline uint16 getNNeighbors( void ) const { return pNeighbors ? pNeighbors->getUsedLength() : 0; } ;
	
		bool insertNeighbor( iBobParticle* pParticle );
		bool removeNeighbor( uint16 index );
		iBobParticle* getNeighborAt( uint16 index ) const;

		// Incrementa o número de molas temporárias desta partícula
		inline void increaseTempCoils( void ) { if( nTempCoils < MAX_TEMP_COILS_PER_PARTICLE )++nTempCoils; };
	
		// Decrementa o número de molas temporárias desta partícula
		void decreaseTempCoils( void );
	
		// Cancela todas as molas temporárias ligas à partícula
		void destroyTempCoils( void );
	
		// Retorna o número de molas temporárias desta partícula
		inline uint8 getNTempCoils( void ) const { return nTempCoils; };

		// Determina o fator de aderência desta partícula
		void setStickingFactor( float factor );
	
		// Obtém o fator de aderência desta partícula
		inline float getStickingFactor( void ) const { return stickingFactor; };
	
		#if DRAG_N_DROP_USES_FORCES
			// Determina o quanto a força de toque "index" irá influenciar esta partícula
			void setTouchForceWeight( uint8 touchForceIndex, float weight );
	
			// Indica se a partícula já foi visitada
			inline bool wasVisited( void ) const { return visited; };
		
			// Determina o estado da partícula na varredura do grafo
			inline void setVisited( bool b ) { visited = b; };
		#else
			// Indica se a partícula está selecionada pelo usuário
			inline bool isSelected( void ) const { return selected; };
		
			// Determina se partícula está selecionada pelo usuário
			inline void setSelected( bool b ) { selected = b; };
		#endif
	
		// Indica se a partícula está localizada em uma das faces do iBob
		inline bool isFaceParticle( void ) const { return faceParticle; };
	
		// Determina se a partícula está localizada em uma das faces do iBob
		inline void setFaceParticle( bool b ){ faceParticle = b; };
	
		#if BLOB_MODE
			// Indica a velocidade da partícula antes de essa sofrer colisão. Armazenamos este valor
			// já que após uma colisão a velocidade da partícula é zerada
			inline const Point3f* getSpeedBeforeCollision( void ) const { return &speedBeforeCollision; };
		#endif
	
	protected:
		// Indica se a partícula está localizada em uma das faces do iBob
		bool faceParticle;
	
		#if DRAG_N_DROP_USES_FORCES
			// Auxiliar para a varredura do grafo
			bool visited;
		#else
			// Indica se a partícula está selecionada pelo usuário
			bool selected;
		#endif
	
		// Número de molas temporárias
		uint8 nTempCoils;
	
		// Fator de aderência da partícula
		float stickingFactor;
	
		// Vetor com as partículas vizinhas
		DynamicVector *pNeighbors;
	
		#if BLOB_MODE
			// Indica a velocidade da partícula antes de essa sofrer colisão. Armazenamos este valor
			// já que após uma colisão a velocidade da partícula é zerada
			Point3f speedBeforeCollision;
		#endif

		#if DRAG_N_DROP_USES_FORCES
			// Forças aplicadas na partícula
			const TouchForce* touchForces[ MAX_TOUCHES ];
		
			// Peso das forças aplicadas na partícula
			float touchForcesWeights[ MAX_TOUCHES ];
		#endif
};

#endif