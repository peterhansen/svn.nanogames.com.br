#ifndef SPRITE_H
#define SPRITE_H

/*==============================================================================================

CLASSE Sprite

==============================================================================================*/

#include "Shape.h"
#include "Texture.h"

// Número de coordenas de textura utilizadas pelo sprite (4 pares (s,t))
#define N_SPRITE_TEX_COORDS 8

class Sprite : public Shape
{
	public:
		// Cria uma nova instância da classe
		static Sprite* CreateInstance( Texture *pTexture, float width, float height, GLenum textureEnvMode = GL_DECAL );
	
		// Destrutor
		virtual ~Sprite( void ){};
	
		// Renderiza o objeto
		virtual void render( void );

		// Obtém uma cópia do objeto
		virtual Shape* getCopy( void );

		// Determina a largura do sprite
		virtual void setWidth( float width );
	
		// Obtém a largura do sprite
		inline float getWidth( void ) const { return width; };
	
		// Determina a altura do sprite
		virtual void setHeight( float height );

		// Obtém a altura do sprite
		inline float getHeight( void ) const { return height; };
	
		// Determina coordenadas de textura específicas para este objeto. Se desejar utilizar as
		// coordenadas de textura padrão fornecidas pela classe, basta passar NULL
		void setTexCoords( float* pTexCoords );
	
	protected:
		// Construtor
		Sprite( Texture *pTexture, float spriteWidth, float spriteHeight, GLenum textureEnvMode = GL_DECAL );

	private:
		// Coordenadas de textura dos sprites
		static const float texCoords[ N_SPRITE_TEX_COORDS ] ;
	
		// Vértices do sprite
		static const float vertexes[12];
	
		// Modo de texturização. Ver glTexEnv
		GLenum texEnvMode;
	
		// Indica se estamos usando as coordenadas de textura da classe ou do objeto
		bool usingOwnTexCoords;
	
		// Coordenadas de textura específicas desse sprite, caso o usuário não deseje utilizar
		// as coordenadas padrão já fornecidas pela classe
		float myTexCoords[ N_SPRITE_TEX_COORDS ];
	
		// Dimensões do sprite
		float width, height;
};

#endif