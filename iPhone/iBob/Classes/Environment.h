#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

/*===========================================================================================

CLASSE Environment

============================================================================================*/

#include "DynamicVector.h"
#include "Force.h"

class Environment
{
	public:
		// Construtor
		Environment( void );
		
		// Destrutor
		virtual ~Environment( void );
		
		int16 insertParticle( Particle *pParticle );
		bool removeParticleAt( uint16 index );
	
		int16 insertForce( Force *pForce );
		bool removeForceAt( uint16 index );
		inline Force* getForceAt( uint16 index ){ return pForces ? ( Force* )pForces->getData( index ) : NULL; }
	
		// Atualiza o objeto
		void update( double timeElapsed );
	
		// Renderiza o objeto
		void render( void );
	
	protected:
		DynamicVector *pForces;
		DynamicVector *pParticles;
};

#endif