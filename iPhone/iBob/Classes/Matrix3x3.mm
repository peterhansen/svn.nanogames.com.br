#include "Matrix3x3.h"
#include "Macros.h"

// Define o número de linhas e colunas da matriz
#define N_ROWS 3
#define N_COLUMNS 3

// Define o número de elementos existentes na matriz
#define N_ELEMENTS ( N_ROWS * N_COLUMNS )

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix3x3::Matrix3x3( float value )
{
	set( value );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix3x3::Matrix3x3( const float* pElements )
{
	set( pElements );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix3x3::Matrix3x3(	float _00, float _01, float _02,
						float _10, float _11, float _12,
						float _20, float _21, float _22 )
					:	_00( _00 ), _01( _01 ), _02( _02 ),
						_10( _10 ), _11( _11 ), _12( _12 ),
						_20( _20 ), _21( _21 ), _22( _22 )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix3x3::set( float value )
{
	memset( m, value, sizeof( float ) * N_ELEMENTS );
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix3x3::set( const float* pElements )
{
	memcpy( m, pElements, sizeof( float ) * N_ELEMENTS );
}

/*==============================================================================================

OPERADOR () lvalue
 
==============================================================================================*/

float& Matrix3x3::operator () ( uint8 row, uint8 column )
{
	return m[ row ][ column ];
}

/*==============================================================================================

OPERADOR () rvalue
 
==============================================================================================*/

float Matrix3x3::operator () ( uint8 row, uint8 column ) const
{
	return m[ row ][ column ];
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

Matrix3x3::operator float* ()
{
	return ( float* )&m;
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

Matrix3x3::operator const float* () const
{
	return ( float* )&m;
}

/*==============================================================================================

OPERADOR *=
 
==============================================================================================*/

Matrix3x3& Matrix3x3::operator *= ( const Matrix3x3& mtx )
{
	Matrix3x3 res;
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			res.m[ row ][ column ]  =	m[ row ][ 0 ] * mtx.m[ 0 ][ column ] +
										m[ row ][ 1 ] * mtx.m[ 1 ][ column ] +
										m[ row ][ 2 ] * mtx.m[ 2 ][ column ];
		}
	}
	*this = res;
	return *this;
}

/*==============================================================================================

OPERADOR +=
 
==============================================================================================*/

Matrix3x3& Matrix3x3::operator += ( const Matrix3x3& mtx )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][column ] += mtx.m[ row ][ column ];
	}
	return *this;
}

/*==============================================================================================

OPERADOR -=
 
==============================================================================================*/

Matrix3x3& Matrix3x3::operator -= ( const Matrix3x3& mtx )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] -= mtx.m[ row ][ column ];
	}
	return *this;
}

/*==============================================================================================

OPERADOR *=
 
==============================================================================================*/

Matrix3x3& Matrix3x3::operator *= ( float f )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] *= f;
	}
	return *this;
}

/*==============================================================================================

OPERADOR /=
 
==============================================================================================*/

Matrix3x3& Matrix3x3::operator /= ( float f )
{
	// É mais rápido multiplicar do que dividir, por isso invertemos o determinante e depois o
	// multiplicamos pelos elementos da matriz 
	f = 1.0f / f;
	return ( *this ) *= f;
}

/*==============================================================================================

OPERADOR + unário
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator + () const
{
	return *this;
}

/*==============================================================================================

OPERADOR - unário
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator - () const
{
	Matrix3x3 res = *this;
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			res.m[ row ][ column ] = -m[ row ][ column ];
	}
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator * ( const Matrix3x3& mtx ) const
{
	Matrix3x3 res = *this;
	res *= mtx;
	return res;
}

/*==============================================================================================

OPERADOR +
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator + ( const Matrix3x3& mtx ) const
{
	Matrix3x3 res = *this;
	res += mtx;
	return res;
}

/*==============================================================================================

OPERADOR -
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator - ( const Matrix3x3& mtx ) const
{
	Matrix3x3 res = *this;
	res -= mtx;
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator * ( float f ) const
{
	Matrix3x3 res = *this;
	res *= f;
	return res;
}

/*==============================================================================================

OPERADOR /
 
==============================================================================================*/

Matrix3x3 Matrix3x3::operator / ( float f ) const
{
	Matrix3x3 res = *this;
	res /= f;
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix3x3 operator * ( float f, const Matrix3x3& mtx )
{
	return mtx * f;
}

/*==============================================================================================

OPERADOR ==
 
==============================================================================================*/

bool Matrix3x3::operator == ( const Matrix3x3& mtx ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( FCMP( m[ row ][column ], mtx.m[ row ][ column ] ) != 0 )
				return false;
		}
	}
	return true;
}

/*==============================================================================================

OPERADOR !=
 
==============================================================================================*/
	
bool Matrix3x3::operator != ( const Matrix3x3& mtx ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( FCMP( m[ row ][column ], mtx.m[ row ][ column ] ) == 0 )
				return true;
		}
	}
	return false;
}

/*===========================================================================================

MÉTODO transpose
	Transforma esta matriz em sua matriz transposta.

============================================================================================*/

void Matrix3x3::transpose( void )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = row + 1 ; column < N_COLUMNS ; ++column )
		{
			const float aux = m[ row ][ column ];
			m[ row ][ column ] = m[ column ][ row ];
			m[ column ][ row ] = aux;
		}
	}
}

/*===========================================================================================

MÉTODO inverse
	Transforma esta matriz em sua matriz inversa.

============================================================================================*/

bool Matrix3x3::inverse( void )
{
	// Acha o determinante da matriz 3x3 utilizando o método prático (este método só existe 
	// para matrizes 3x3!!!)
    float det =   ( _00 * _11 * _22 ) + ( _01 * _12 * _20 ) + ( _02 * _10 * _21 ) 
				- ( _00 * _12 * _21 ) - ( _01 * _10 * _22 ) - ( _02 * _11 * _20 );
	
	// Se o determinante for 0, a matriz não é inversível
    if( FCMP( det, 0.0f ) == 0 )
		return false;

	// É mais rápido multiplicar do que dividir, por isso invertemos o determinante e depois o
	// multiplicamos pelos elementos da matriz adjunta
    det = 1.0f / det;

	// Cria a matriz inversa
    for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] *= det;
	}

	return true;
}

/*===========================================================================================

MÉTODO identity
	Transforma esta matriz em uma matriz identidade.

============================================================================================*/

void Matrix3x3::identity( void )
{
	memset( m, 0, sizeof( float ) * N_ELEMENTS );
	m[0][0] = 1.0f;
	m[1][1] = 1.0f;
	m[2][2] = 1.0f;
}

/*===========================================================================================

MÉTODO isIdentity
	Indica se esta matriz é uma matriz identidade.

============================================================================================*/

bool Matrix3x3::isIdentity( void ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( row != column )
			{
				if( FCMP( m[ row ][ column ], 0.0f ) != 0 )
					return false;
			}
			else
			{
				if( FCMP( m[ row ][ column ], 1.0f ) != 0 )
					return false;
			}
		}
	}
	return true;
}

