/*
 *  BlobEye.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/2/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef BLOB_EYE_H
#define BLOB_EYE_H

#if BLOB_MODE

#include "Sphere.h"

class BlobEye : public Sphere
{
	public:
		// Construtor
		BlobEye( void );
		
		// Destrutor
		virtual ~BlobEye( void ){};
	
		// Renderiza o objeto
		virtual void render( void );

		// Obtém uma cópia do objeto.
		virtual Shape* getCopy( void ) const;

#ifdef TESTING_SOCCER_BALL
	private:
		Point3f rot;
#endif
};

#endif

#endif