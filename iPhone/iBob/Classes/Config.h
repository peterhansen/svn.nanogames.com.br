/*
 *  Config.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/17/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

// Existem as seguintes opções de pré-compilação (não são mutuamente exclusivas):
// CONTROLLING_ORIENTATION
// DRAG_N_DROP_USES_FORCES
// BLOB_MODE
// NANO_DEBUG
// RENDER_TEST_SCENE
// USE_CENTER_PARTICLE
// Estas opções devem ser determinadas nas configurações do projeto, e não nesse arquivo

// Número de planos de colisão existentes na cena
#define N_COLLISION_PLANES 6

// Define qual o modo de interpretação dos valores gerados pelos acelerômetros
#define DEFAULT_ACCELEROMETER_CALC_MODE ACC_FORCE_MODE_AVERAGE

// Duração do splash da nano (em segundos)
#define SPLASH_NANO_DURATION 2

// Duração do splash do jogo (em segundos)
#define SPLASH_GAME_DURATION 2

// Define se utilizaremos um depth buffer na aplicação
#define USE_DEPTH_BUFFER 1

// Intervalo padrão do timer responsável pelas atualizações da aplicação
#define DEFAULT_ANIMATION_INTERVAL ( 1.0f / 32.0f ) // 32 fps

// Número máximo de toques tratados ao mesmo tempo
#define MAX_TOUCHES 5

// Número máximo de molas temporárias criadas por partícula do iBob
#define MAX_TEMP_COILS_PER_PARTICLE 1 // OLD : Original era 3

// Número máximo de molas temporárias que podem ser criadas
#define MAX_TEMP_COILS 12

// Número mínimo de partículas que devem sofrer colisão para que toquemos o respectivo som
#define MIN_COLLIDING_PARTICLES 1

// Número mínimo de iterações que devemos ficar sem colisões para considerar que o todas as
// partículas do personagem realmente pararam de colidir com os planos da cena
#define MIN_NO_COLLISION_COUNTER 2

// Velocidade mínima necessária para se ativar o som de colisão
#define COLLISION_SOUND_MIN_SPEED 250.0f

// Velocidade máxima considerada no cálculo do volume do som de colisão
#define COLLISION_SOUND_MAX_SPEED 2000.0f

// Ganhos (porcentagem do volume original) mínimo e máximo que o som de colisão pode atingir
// de acordo com a força do impacto
#define COLLISION_SOUND_MIN_GAIN 0.05f
#define COLLISION_SOUND_MAX_GAIN 0.15f

// Define o fator de atenuação do som de colisão que é aplicado de acordo com a posição da fonte de som
#define COLLISION_SOUND_ROLLOFF_FACTOR 0.01

// Velocidade mínima necessária para se ativar o berro do personagem
#define SCREAM_MIN_SPEED_MODULE 1000.0f

// Velocidades extremas permitidas no eixo z para o cálculo do efeito doppler
#define SCREAM_Z_MIN_SPEED -400.0f
#define SCREAM_Z_MAX_SPEED  150.0f

// Ganhos (porcentagem do volume original) mínimo e máximo que o berro do personagem pode
// atingir de acordo com a velocidade do mesmo
#define SCREAM_MIN_GAIN 0.5f
#define SCREAM_MAX_GAIN 1.0f

// Define o fator de atenuação do grito que é aplicado de acordo com a posição da fonte de som
#define SCREAM_ROLLOFF_FACTOR 0.05f

// Define um multiplicador para a vida das molas temporárias. A vida total será dada por:
// stickingFactor * TEMP_COILS_LIFE_BOOSTER
// Onde stickingFactor pode ser configurado pelo usuário final da aplicação
#define TEMP_COILS_LIFE_BOOSTER 2.0f // OLD: Original era 5.0f

// Índices das views da aplicação
#define VIEW_INDEX_SPLASH_NANO	0
#define VIEW_INDEX_SPLASH_GAME	1
#define VIEW_INDEX_MAIN_MENU	2
#define VIEW_INDEX_GAME			3

// Índices das opções do menu
#define MENU_GAME_INDEX		0
#define MENU_COLORS_INDEX	1
#define MENU_OPTIONS_INDEX	2
#define MENU_CREDITS_INDEX	3

// Opção do menu que está selecionada quando saímos da tela de jogo
#define DEFAULT_MENU_OPT MENU_COLORS_INDEX

// Duração padrão das animações de transição de tela
#define DEFAULT_TRANSITION_DURATION 0.75f

// Valor padrão utilizado para multiplar a aceleração reportada pelos acelerômetros
#define DEFAULT_ACC_BOOSTER 50000.0f

#if TARGET_IPHONE_SIMULATOR
	// Valor mínimo utilizado para multiplar a aceleração reportada pelos acelerômetros
	#define MIN_ACC_BOOSTER 20000.0f

	// Valor máximo utilizado para multiplar a aceleração reportada pelos acelerômetros
	#define MAX_ACC_BOOSTER 80000.0f
#endif

// Configurações extremas das molas que constituem o personagem
#define MIN_TIGHTNESS 200
#define MAX_TIGHTNESS 500// OLD Original era 1500...

#define MIN_DAMPING 10
#define MAX_DAMPING 20 // OLD Original era 40...

// Intervalo permitido para o multiplicador da força da gravidade
#define MIN_GRAVITY_BOOSTER 0.0f
#define MAX_GRAVITY_BOOSTER 6000.0f

// Força da gravidade padrão2
#define DEFAULT_GRAVITY_BOOSTER 3000.0F

// Multiplicador utilizado para aplicar uma gravidade maior às partículas emitidas pelo personagem. Assim
// fazemos com que elas saiam de cena rapidamente
#define PARTICLES_GRAVITY_BOOSTER 17.0f

// Menor opacidade atingida pelo botão de "voltar ao menu" com o passar do tempo
#define MIN_BACK_TO_MENU_BT_ALPHA 0.0f

// O quanto reduzimos da opacidade do botão de "voltar ao menu" a cada BACK_TO_MENU_BT_ALPHA_REDUCTION_SPEED
#define BACK_TO_MENU_BT_ALPHA_REDUCTION 0.05f

// Velocidade com a qual a opacidade do botão de "voltar ao menu" é reduzida
#define BACK_TO_MENU_BT_ALPHA_REDUCTION_SPEED 0.1f

// Define o tempo que o botão de "voltar ao menu" fica completamente opaco antes de começar
// a animação de fade out
#define BACK_TO_MENU_BT_VISIBLE_TIME 3.0f

#if BLOB_MODE
	// Intervalo de alpha permitido na cor do iBlob
	#define BLOB_COLOR_MIN_ALPHA 0.25f
	#define BLOB_COLOR_MAX_ALPHA 0.80f

	#define BLOB_COLOR_TOTAL_ALPHA ( BLOB_COLOR_MIN_ALPHA + BLOB_COLOR_MAX_ALPHA )

	// Cor padrão do iBlob
	#define BLOB_COLOR_DEFAULT_R 0.126f
	#define BLOB_COLOR_DEFAULT_G 1.0f
	#define BLOB_COLOR_DEFAULT_B 0.0f

	// Indica se o gradiente crescente de alpha deve ser feito de dentro para fora (bordas mais
	// transparentes) ou de fora para dentro
	// 0 => false ; != 0 => true 
	#define INSIDE_OUT 1
#endif

// Valores padrões das configurações existentes na tela de opções
#define DEFAULT_SOUND_VOLUME 1.0f
#define DEFAULT_HARDNESS 1.0f
#define DEFAULT_STICKNESS 0.0f
#define DEFAULT_GRAVITY 0.5f

// Erros que podem terminar a aplicação
#define NO_ERROR				0x0
#define ERROR_ALLOCATING_VIEWS	0x1
#define ERROR_CHANGING_COLORS	0x2
#define ERROR_NO_FRAME_BUFFER	0x3
#define ERROR_MEMORY_WARNING	0x4

#endif
