#ifndef SPRING_COIL_H
#define SPRING_COIL_H

/*===========================================================================================
 
CLASSE SpringCoil
 
============================================================================================*/

#include "Particle.h"

// Comprimento máximo padrão ( em porcentagem )
#define DEFAULT_ELASTICITY 4.0f

class SpringCoil
{
	public:
		// Partículas que fazem parte da mola
		Particle *p1, *p2;
	
		bool immortal, maintainDirection;
		float tightness, dampingFactor, life;
	
		// Comprimento da mola quando está em repouso
		float restLength;
	
		// Indica se a mola pode arrebentar
		bool breacheable;

		// Direção original entre as partículas da mola
		Point3f originalDirection;

		// Construtor
		SpringCoil( float elasticity = DEFAULT_ELASTICITY, bool breacheable = false, Particle* p1 = NULL, Particle* p2 = NULL, bool maintainDirection = true, float tightness = 0.0f, float dampingFactor = 0.0f, float life = -1.0f );

		// Atualiza o objeto retornando se a mola arrebentou
		virtual bool update( double time );
	
		// Inicializa o objeto
		void set( Particle* particle1, Particle* particle2, float tightness, float dampingFactor, float elasticity = DEFAULT_ELASTICITY );
	
	private:
		// Comprimento máximo da mola ( arrebenta caso o ultrapasse )
		float maxLength;
};

#endif