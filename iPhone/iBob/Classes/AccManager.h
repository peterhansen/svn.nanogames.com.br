/*
 *  AccManager.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/9/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACC_MANAGER_H
#define ACC_MANAGER_H

#include "AccelerometerInterface.h"
#include "AccListener.h"
#include "NanoTypes.h"

// Número máximo de listeners que AccManager pode ter
#define ACC_MANAGER_N_LISTENERS 16

class AccManager : public AccListener
{
	public:
		// Retorna a única instância da classe
		static AccManager* GetInstance( void );
	
		// Libera a memória alocada pela única instância da classe
		static void release( void );

		// Determina que um objeto deve começar a receber os eventos do acelerômetro
		bool insertListener( AccListener* pNewListener, uint8* pIndex = NULL );
	
		// Indica que um objeto deve parar de receber os eventos do acelerômetro
		bool removeListener( uint8 index );

		// Determina a frequência com que o usuário deseja receber os eventos do acelerômetro
		void setUpdateInterval( float secs );

		// Começa a receber os eventos do acelerômetro
		void resume( void );

		// Pára de receber os eventos do acelerômetro
		void suspend( void );
	
	private:
		// Construtor
		AccManager( void );
	
		// Destrutor
		virtual ~AccManager( void );
	
		// Recebe os eventos do acelerômetro
		virtual void OnAccelerate( const AccManager* pAccelerometer, const Point3f* pAcceleration );
	
		// Objeto que irá receber os eventos do acelerômetro
		AccListener* listeners[ ACC_MANAGER_N_LISTENERS ];
	
		// Intervalo de atualização do acelerômetro
		float updateInterval;

		// TODO : Inicializar o singleton dessa classe na classe da view openGL e removê-lo da memória no
		// término da aplicação

		// Interface da API do acelerômetro
		AccelerometerInterface* hInterface;
	
		// Única instância da classe
		static AccManager* pSingleton;
};

#endif

