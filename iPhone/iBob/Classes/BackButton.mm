#include "BackButton.h"
#include "Macros.h"

// Define o quanto o botão que leva ao menu fica separado da tela
#define BACK_TO_MENU_BT_DIST_FROM_SCREEN 5.0f

// Tenta evitar distorções na imagem
#define TRY_EXACT_PIXELIZATION 0.375f

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

BackButton::BackButton( Texture2D *pTexture )
		   : Sprite( pTexture, pTexture->getWidth() * 0.5f, pTexture->getHeight() * 0.5f, GL_MODULATE ),
			 // OLD position( SCREEN_WIDTH - ( getWidth() * 0.5f ) - BACK_TO_MENU_BT_DIST_FROM_SCREEN + TRY_EXACT_PIXELIZATION, ( getHeight() * 0.5f ) + BACK_TO_MENU_BT_DIST_FROM_SCREEN - TRY_EXACT_PIXELIZATION, 0.0f )
			 position( ( getWidth() * 0.5f ) + BACK_TO_MENU_BT_DIST_FROM_SCREEN - TRY_EXACT_PIXELIZATION, ( getHeight() * 0.5f ) + BACK_TO_MENU_BT_DIST_FROM_SCREEN - TRY_EXACT_PIXELIZATION, 0.0f )
{
	Color c( 1.0f, 1.0f, 1.0f, 1.0f );
	setColor( &c );
}

/*==============================================================================================

MÉTODO checkCollision
	Testa colisão com o botão que leva o usuário da tela de jogo ao menu.

==============================================================================================*/

bool BackButton::checkCollision( float x, float y )
{
	const float top = position.y + ( getHeight() * 0.5f );
	const float left = position.x - ( getWidth() * 0.5f );
	return FGEQ( x, left )
		   && FLEQ( x, left + getWidth() )
		   && FGEQ( y, top - getHeight() )
           && FLEQ( y, top );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void BackButton::render( void )
{
	glDisable( GL_DEPTH_TEST );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glOrthof( 0.0f, SCREEN_WIDTH, 0.0f, SCREEN_HEIGHT, -1.0f, 1.0f );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glTranslatef( position.x, position.y, position.z );
	
	Sprite::render();
	
	glPopMatrix();
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );	
}
