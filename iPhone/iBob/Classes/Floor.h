/*
 *  Floor.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 1/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FLOOR_H
#define FLOOR_H

#include "Sprite.h"

class Floor : public Sprite
{
	public:
		// Cria uma nova instância da classe
		static Floor* CreateInstance( Texture *pTexture, float width, float height, GLenum textureEnvMode = GL_DECAL );
	
		// Renderiza o objeto
		virtual void render( void );
	
	protected:
		// Construtor
		Floor( Texture *pTexture, float spriteWidth, float spriteHeight, GLenum textureEnvMode = GL_DECAL );
};

#endif

