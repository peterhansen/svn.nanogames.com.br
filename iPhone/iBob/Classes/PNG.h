/*
 *  PNG.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/9/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_PNG_H
#define NANO_PNG_H

#include "Color.h"

// Maiores informações sobre formato PNG podem ser encontradas nos sites:
// - http://www.libpng.org/pub/png/spec/
// - http://www.libpng.org/pub/png/libpng.html
// - http://www.zlib.net/
class PNG
{
	public:
		// Carrega a imagem
		static PNG* load( const char* pFileName );
		static PNG* load( const NSString* pFileName );
	
		// Modifica a cor from para a cor to na palera de cores da imagem
		bool changePalette( const Color* pFrom, const Color* pTo );
	
		// Faz com que a cor de índice colorPaletteIndex na paleta de cores da imagem passe a ser
		// igual a newColor
		bool changePalette( uint8 colorPaletteIndex, const Color* pNewColor );
	
		// TASK : Será que é bom termos este método?
		// Retorna os dados da imagem
		//inline const NSData* getData( void ) const { return pData; };
	
		// Retorna o índice da cor recebida na paleta de cores
		int16 getPaletteColorIndex( const Color* pColor ) const;
	
		// Retorna o número de cores existentes na paleta da imagem
		inline uint16 getPaletteNColors( void ) const { return paletteLength / 3; };
	
		// Obtém uma cópia da paleta de cores da imagem. O usuário é responsável por desalocar o
		// vetor retornado
		// TASK : Preencher os valores de alpha corretamente!!!!!!
		Color* getPalette( void );
	
		// Determina uma nova paleta de cores para a imagem. O parâmetro nColors é colocado no
		// intervalo [0, getPaletteNColors() ]
		void setPalette( Color* pNewPalette, uint16 nColors );
	
		// Retorna uma imagem gerada a partir do arquivo PNG. O usuário é responsável por desalocar
		// a imagem retornada
		UIImage* getImageCopy( void ) const;
	
		// Destrutor
		~PNG( void );
	
	private:
		// Construtor
		PNG( void );
	
		// Pré-calcula a tabela de CRC para otimizar cálculos futuros
		static bool CreateCRCTable( void );
	
		// Calcula o CRC dos dados contidos em pChunkData
		static uint32 GetCRC( const uint8* pChunkData, uint32 length );
	
		// Obtém um ponteiro para o início da paleta de cores da imagem
		uint8* getPalettePointer( void ) const;
	
		// Procura o chunk da paleta de cores
		void searchPLTEChunk( void );

		// Raw data da imagem
		NSData* pData;
	
		// Ponteiro para o chunk da paleta de cores
		uint8* pPaletteChunk;
	
		// Tamanho paleta de cores da imagem
		uint16 paletteLength;
	
		// Tabela utilizada no cálculo dos CRCs dos chunks
		static uint32* pCRCTable;
};

#endif