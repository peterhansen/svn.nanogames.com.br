/*
 *  Sphere.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/25/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_SPHERE_H
#define NANO_SPHERE_H

#include "Shape.h"
#include "Point3f.h"

class Sphere : public Shape
{
	public:
		// Construtor
		Sphere( float radius, int lod, const Color* pColor = NULL );

		// Destrutor
		virtual ~Sphere( void );
	
		// Renderiza o objeto
		virtual void render( void );
	
		// Obtém uma cópia do objeto
		virtual Shape* getCopy( void );
	
		// Retorna o raio da esfera
		// OBS: Equivale a getSize() * 0.5f
		inline float getRadius( void ) const { return getSize() * 0.5f; };
	
		// Determina o raio da esfera
		// OBS: Equivale a setSize( radius * 2.0f );
		inline void setRadius( float radius ){ setSize( radius * 2.0f ); };
	
	private:
		// Inicializa o objeto
		void build( void );

		// Nível de detalhamento da esfera
		int lod;

		// Array de vértices que descrevem a esfera
		int nSphereVertices;
		Point3f* pSphereVertices;
		float (*pSphereTexCoords)[2];
};

#endif