/*
 *  CollisionListener.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/1/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef COLLISION_LISTENER_H
#define COLLISION_LISTENER_H

#if BLOB_MODE

class CollisionListener
{
	public:
		// Indica que a partícula passada como parâmetro sofreu colisão
		virtual void onCollision( const iBobParticle* pParticle ) = 0;
};

#endif

#endif