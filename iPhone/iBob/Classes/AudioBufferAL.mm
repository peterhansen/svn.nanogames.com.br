#include "AudioBufferAL.h"
#include "Exceptions.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

AudioBufferAL::AudioBufferAL( void ) : refCount( 0 ), bufferId( 0 )
{
	// Limpa o código de erro
	alGetError();
 
	// Cria o buffer
	alGenBuffers( 1, &bufferId ); 
	
	// Verifica se houve erros durante a operação
	if( alGetError() != AL_NO_ERROR )
#if TARGET_IPHONE_SIMULATOR
		throw OpenALException( "AudioBufferAL::AudioBufferAL()" );
#else
		throw OpenALException();
#endif
}
	
/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

AudioBufferAL::~AudioBufferAL( void )
{
	if( decreaseRefCount() )
		alDeleteBuffers( 1, &bufferId );
}

/*==============================================================================================

MÉTODO fillBuffer
	Preenche este buffer com os dados contidos em pData.
 
==============================================================================================*/

bool AudioBufferAL::fillBuffer( const ALvoid* pData, ALsizei size, ALenum format, ALsizei freq )
{
	// Limpa o código de erro
	alGetError();

	// Preenche o buffer com os dados recebidos
	alBufferData( bufferId, format, pData, size, freq );
	
	// Verifica se houve erros durante a operação
#if TARGET_IPHONE_SIMULATOR
	ALenum error = alGetError();
	return error == AL_NO_ERROR;
#else
	return alGetError() == AL_NO_ERROR;
#endif
}

// TASK : Verificar se há ganho de desempenho com StaticBuffers

// Destrutor
//if (gStaticBufferData)
//	free(gStaticBufferData);

// Construtor
// use the static buffer data API once for testing
//			gStaticBufferData = (ALvoid*)malloc(size);
//			memcpy(gStaticBufferData, data, size);
//			alBufferDataStaticProc(gBuffer[i], format, gStaticBufferData, size, freq);

//Métodos
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef ALvoid	AL_APIENTRY	(*alBufferDataStaticProcPtr) (const ALint bid, ALenum format, ALvoid* data, ALsizei size, ALsizei freq);
//ALvoid  alBufferDataStaticProc(const ALint bid, ALenum format, ALvoid* data, ALsizei size, ALsizei freq)
//{
//	static	alBufferDataStaticProcPtr	proc = NULL;
//    
//    if (proc == NULL) {
//        proc = (alBufferDataStaticProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alBufferDataStatic");
//    }
//    
//    if (proc)
//        proc(bid, format, data, size, freq);
//
//    return;
//}