/*
 *  BobFullScene.h
 *
 *  Created by Daniel Lopes Alves on 9/29/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef BOB_FULL_SCENE_H
#define BOB_FULL_SCENE_H

#include "Scene.h"
#include "BackButton.h"
#include "iBob.h"
#include "AccForce.h"
#include "TouchForce.h"
#include "EventListener.h"
#include "Config.h"
#include "EAGLView.h"

#if BLOB_MODE
	#include "CollisionListener.h"
#endif

class BobFullScene : public Scene, public LoadableListener, public EventListener, public CollisionListener
{
	public:
		// Destrutor
		virtual ~BobFullScene( void );
	
		// Cria um novo objeto da classe
		static BobFullScene* CreateInstance( EAGLView* hParentView );
	
		// Método para receber os eventos do objeto loadable
		virtual void loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data );
	
		// Métodos para configuração da cena e do personagem
		void setGravity( float gravity );
		void iBobSetHardness( float hardness );
		inline void iBobReset( void ) { pIBob->reset(); };
		inline void iBobSetStickness( float stickness ) { pIBob->setStickingFactor( stickness ); };
	
		#if TARGET_IPHONE_SIMULATOR
			// Retorna a força responsável por controlar as informações do acelerômetro
			inline AccForce* getAccForce( void ) { return ( AccForce* ) getWorldAt( 0 )->getEnvironmentAt( 0 )->getForceAt( 0 ); };
		#endif
	
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );

		// Renderiza o objeto
		virtual void render( void );
	
		// Atualiza o objeto
		void update( double timeElapsed );
	
		#if BLOB_MODE
			// Determina a nova cor do personagem
			void iBlobSetColor( const Color* pColor );

			// Indica que a partícula passada como parâmetro sofreu colisão
			virtual void onCollision( const iBobParticle* pParticle );
		#endif
	
	private:
		// Construtor
		BobFullScene( EAGLView* hParentView );
	
		// Inicializa o objeto
		bool build( void );
	
		// Retorna o índice da primeira força de toque que não está sendo utilizada. Retornará -1
		// caso todas as forças já estejam em uso
		int8 getUnusedTouchForce( void );
	
		// Cancela as forças identificadas pelos toques contidos em pTouchesArray
		bool cancelTouchForces( NSArray* pTouchesArray );
	
		// Cancela a força de toque com o índice recebido como parâmetro
		void cancelTouchForce( uint8 index );
	
		// Trata a movimentação das forças de toque
		bool touchForcesMoved( NSArray* pTouchesArray );
	
		// Cria o botão que leva o usuário da tela de jogo ao menu
		bool createBackToMenuButton( void );
	
		// Inicializa o áudio da aplicação
		bool initAudio( AudioManager* pAudioManager );
	
		// Aloca as texturas a serem aplicadas no background da cena
		bool getBkgSprites( Sprite** bkgSprites );

		// Contadores que controlam a atualização da opacidade do botão de "voltar ao menu"
		float backToMenuBtVisibleCounter, backToMenuBtReductionCounter;
	
		// Indica os toques ativos sobre o iBob
		TouchForce* touchForces[ MAX_TOUCHES ];
	
		// Ponteiro para o iBob
		iBob* pIBob;
	
		// Ponteiro para a view que contém a cena
		EAGLView* hParentView;
	
		// Botão que leva o usuário da tela de jogo ao menu
		BackButton* pBackToMenuButton;
	
		#if BLOB_MODE
			// Cria os emissores que serão utilizados quando o cubo colidir com os planos do mundo
			bool createBlobEmitters( World* pWorld, const Plane* pCollisionPlanes );
	
			// Cria um emissor de partículas utilizando os dados da partícula recebida como parâmetro
			void setBlobEmitter( const iBobParticle* pParticle );
	
			// Obtém o índice de um emissor que não está sendo utilizado
			int16 getUnusedBlobEmitter( void );
	
			// Emissores ativados quando as partículas do cubo colidem com os planos da cena
			Emitter** pBlobEmitters;
		#endif
};

#endif

