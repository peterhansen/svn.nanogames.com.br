#ifndef PLANE_H
#define PLANE_H

#include "Point3f.h"

class Plane
{
	public:
		// Vetor apontando para a "direita" do plano
		Point3f	right;	
	
		// Vetor apontando para o "topo" do plano
		Point3f	top;
	
		// Normal do plano
		Point3f	normal;

		// Deslocamento
		float d;

		// Construtores
		Plane( void ){};
		Plane( const Point3f* pTop, const Point3f* pRight, float d = 0.0f );
	
		// Inicializa o objeto
		inline void set( const Point3f* pTop = NULL, const Point3f* pRight = NULL, float d = 0.0f ){ if( pTop ) top = *pTop; else top.set( 0.0f, 1.0f, 0.0f ); if( pRight ) right = *pRight; else right.set( 1.0f ); this->d = d; normal = right % top; normal.normalize(); };
	
		// Operadores de comparação
		bool operator == ( const Plane& p ) const;
		bool operator != ( const Plane& p ) const;
};

#endif
