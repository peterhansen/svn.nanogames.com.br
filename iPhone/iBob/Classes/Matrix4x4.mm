#include "Matrix4x4.h"
#include "Macros.h"

// Define o número de linhas e colunas da matriz
#define N_ROWS 4
#define N_COLUMNS 4

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4( float value )
{
	set( value );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4( const float* pElements )
{
	set( pElements );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4(	float _00, float _01, float _02, float _03,
						float _10, float _11, float _12, float _13,
						float _20, float _21, float _22, float _23,
						float _30, float _31, float _32, float _33 )
					:	_00( _00 ), _01( _01 ), _02( _02 ), _03( _03 ),
						_10( _10 ), _11( _11 ), _12( _12 ), _13( _13 ),
						_20( _20 ), _21( _21 ), _22( _22 ), _23( _23 ),
						_30( _30 ), _31( _31 ), _32( _32 ), _33( _33 )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix4x4::set( float value )
{
	memset( m, value, sizeof( float ) << 4 );
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix4x4::set( const float* pElements )
{
	memcpy( m, pElements, sizeof( float ) << 4 );
}

/*==============================================================================================

OPERADOR () lvalue
 
==============================================================================================*/

float& Matrix4x4::operator () ( uint8 row, uint8 column )
{
	return m[ row ][ column ];
}

/*==============================================================================================

OPERADOR () rvalue
 
==============================================================================================*/

float Matrix4x4::operator () ( uint8 row, uint8 column ) const
{
	return m[ row ][ column ];
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

Matrix4x4::operator float* ()
{
	return ( float* )&m;
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

Matrix4x4::operator const float* () const
{
	return ( float* )&m;
}

/*==============================================================================================

OPERADOR *=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator *= ( const Matrix4x4& mtx )
{
// TODO : Necessário ???
//	// Evita as imprecisões numéricas
//	if( isIdentity() )
//	{
//		*this = mtx;
//	}
//	else if( !mtx.isIdentity() )
//	{
		Matrix4x4 res;
		for( uint8 row = 0 ; row < N_ROWS ; ++row )
		{
			for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			{
				res.m[ row ][ column ]  =	m[ row ][ 0 ] * mtx.m[ 0 ][ column ] +
											m[ row ][ 1 ] * mtx.m[ 1 ][ column ] +
											m[ row ][ 2 ] * mtx.m[ 2 ][ column ] +
											m[ row ][ 3 ] * mtx.m[ 3 ][ column ];
			}
		}
		*this = res;
//	}
	return *this;
}

/*==============================================================================================

OPERADOR +=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator += ( const Matrix4x4& mtx )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][column ] += mtx.m[ row ][ column ];
	}
	return *this;
}

/*==============================================================================================

OPERADOR -=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator -= ( const Matrix4x4& mtx )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] -= mtx.m[ row ][ column ];
	}
	return *this;
}

/*==============================================================================================

OPERADOR *=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator *= ( float f )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] *= f;
	}
	return *this;
}

/*==============================================================================================

OPERADOR /=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator /= ( float f )
{
	// É mais rápido multiplicar do que dividir, por isso invertemos o determinante e depois o
	// multiplicamos pelos elementos da matriz 
	f = 1.0f / f;
	return ( *this ) *= f;
}

/*==============================================================================================

OPERADOR + unário
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator + () const
{
	return *this;
}

/*==============================================================================================

OPERADOR - unário
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator - () const
{
	Matrix4x4 res = *this;
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			res.m[ row ][ column ] = -m[ row ][ column ];
	}
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator * ( const Matrix4x4& mtx ) const
{
	Matrix4x4 res = *this;
	res *= mtx;
	return res;
}

/*==============================================================================================

OPERADOR +
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator + ( const Matrix4x4& mtx ) const
{
	Matrix4x4 res = *this;
	res += mtx;
	return res;
}

/*==============================================================================================

OPERADOR -
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator - ( const Matrix4x4& mtx ) const
{
	Matrix4x4 res = *this;
	res -= mtx;
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator * ( float f ) const
{
	Matrix4x4 res = *this;
	res *= f;
	return res;
}

/*==============================================================================================

OPERADOR /
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator / ( float f ) const
{
	Matrix4x4 res = *this;
	res /= f;
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix4x4 operator * ( float f, const Matrix4x4& mtx )
{
	return mtx * f;
}

/*==============================================================================================

OPERADOR ==
 
==============================================================================================*/

bool Matrix4x4::operator == ( const Matrix4x4& mtx ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( FCMP( m[ row ][column ], mtx.m[ row ][ column ] ) != 0 )
				return false;
		}
	}
	return true;
}

/*==============================================================================================

OPERADOR !=
 
==============================================================================================*/
	
bool Matrix4x4::operator != ( const Matrix4x4& mtx ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( FCMP( m[ row ][column ], mtx.m[ row ][ column ] ) == 0 )
				return true;
		}
	}
	return false;
}

/*===========================================================================================

MÉTODO transpose
	Transforma esta matriz em sua matriz transposta.

============================================================================================*/

void Matrix4x4::transpose( void )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = row + 1 ; column < N_COLUMNS ; ++column )
		{
			const float aux = m[ row ][ column ];
			m[ row ][ column ] = m[ column ][ row ];
			m[ column ][ row ] = aux;
		}
	}
}

/*===========================================================================================

MÉTODO inverse
	Transforma esta matriz em sua matriz inversa.

============================================================================================*/

bool Matrix4x4::inverse( void )
{
	// Executa sequencialmente as reduções necessárias para se criar a matriz adjunta. Já coloca
	// seus fatores na forma transposta, além de realizar as inversões de sinal antecipadamente,
	// para otimizar a rotina (ver o método "Matrix* adjoint( Matrix* )" da classe Matrix para
	// maiores informações sobre o algoritmo genérico para uma matrix NxN)
	Matrix4x4 inv;
	
	// Coluna 0


	// Acha o elemento [0][0] retirando a linha 0 e a coluna 0
    inv.m[0][0] = + ( _11 * _22 * _33 ) + ( _12 * _23 * _31 ) + ( _21 * _13 * _32 )
				  - ( _11 * _23 * _32 ) - ( _12 * _21 * _33 ) - ( _13 * _22 * _31 );
	
	// Acha o elemento [1][0] ([0][1] já transposto) retirando a linha 0 e a coluna 1
    inv.m[1][0] = - ( _10 * _22 * _33 ) - ( _12 * _23 * _30 ) - ( _13 * _20 * _32 )
				  + ( _10 * _23 * _32 ) + ( _12 * _20 * _33 ) + ( _13 * _22 * _30 );
	
	// Acha o elemento [2][0] ([0][2] já transposto) retirando a linha 0 e a coluna 2
    inv.m[2][0] = + ( _10 * _21 * _33 ) + ( _11 * _23 * _30 ) + ( _13 * _20 * _31 )
				  - ( _10 * _23 * _31 ) - ( _11 * _20 * _33 ) - ( _13 * _21 * _30 );
	
	// Acha o elemento [3][0] ([0][3] já transposto) retirando a linha 0 e a coluna 3
    inv.m[3][0] = - ( _10 * _21 * _32 ) - ( _11 * _22 * _30 ) - ( _20 * _12 * _31 )
				  + ( _10 * _22 * _31 ) + ( _11 * _20 * _32 ) + ( _12 * _21 * _30 );


	// Coluna 1


	// Acha o elemento [0][1] ([1][0] já transposto) retirando a linha 1 e a coluna 0
    inv.m[0][1] = - ( _01 * _22 * _33 ) - ( _02 * _23 * _31 ) - ( _03 * _21 * _32 )
				  + ( _01 * _23 * _32 ) + ( _02 * _21 * _33 ) + ( _03 * _22 * _31 );
	
	// Acha o elemento [1][1] retirando a linha 1 e a coluna 1
    inv.m[1][1] = + ( _00 * _22 * _33 ) + ( _02 * _23 * _30 ) + ( _03 * _20 * _32 ) 
				  - ( _00 * _23 * _32 ) - ( _02 * _20 * _33 ) - ( _03 * _22 * _30 );
	
	// Acha o elemento [2][1] ([1][2] já transposto) retirando a linha 1 e a coluna 2
    inv.m[2][1] = - ( _00 * _21 * _33 ) - ( _01 * _23 * _30 ) - ( _03 * _20 * _31 )
				  + ( _00 * _23 * _31 ) + ( _01 * _20 * _33 ) + ( _03 * _21 * _30 );

	// Acha o elemento [3][1] ([1][3] já transposto) retirando a linha 1 e a coluna 3
    inv.m[3][1] = + ( _00 * _21 * _32 ) + ( _01 * _22 * _30 ) + ( _02 * _20 * _31 )
				  - ( _00 * _22 * _31 ) - ( _01 * _20 * _32 ) - ( _02 * _21 * _30 );


	// Coluna 2


	// Acha o elemento [0][2] ([2][0] já transposto) retirando a linha 2 e a coluna 0
    inv.m[0][2] = + ( _01 * _12 * _33 ) + ( _02 * _13 * _31 ) + ( _03 * _11 * _32 )
				  - ( _01 * _13 * _32 ) - ( _02 * _11 * _33 ) - ( _03 * _12 * _31 );
	
	// Acha o elemento [1][2] ([2][1] já transposto) retirando a linha 2 e a coluna 1
    inv.m[1][2] = - ( _00 * _12 * _33 ) - ( _02 * _13 * _30 ) - ( _03 * _10 * _32 ) 
				  + ( _00 * _13 * _32 ) + ( _02 * _10 * _33 ) + ( _03 * _12 * _30 );
	
	// Acha o elemento [2][2] retirando a linha 2 e a coluna 2
    inv.m[2][2] = + ( _00 * _11 * _33 ) + ( _01 * _13 * _30 ) + ( _03 * _10 * _31 )
				  - ( _00 * _13 * _31 ) - ( _01 * _10 * _33 ) - ( _03 * _11 * _30 );
	
	// Acha o elemento [3][2] ([2][3] já transposto) retirando a linha 2 e a coluna 3
    inv.m[3][2] = - ( _00 * _11 * _32 ) - ( _30 * _01 * _12 ) - ( _02 * _10 * _31 )
				  + ( _00 * _12 * _31 ) + ( _01 * _10 * _32 ) + ( _02 * _11 * _30 );

	
	// Coluna 3
	

	// Acha o elemento [0][3] ([3][0] já transposto) retirando a linha 3 e a coluna 0
    inv.m[0][3] = - ( _01 * _12 * _23 ) - ( _02 * _13 * _21 ) - ( _03 * _11 * _22 )
				  + ( _01 * _13 * _22 ) + ( _02 * _11 * _23 ) + ( _03 * _12 * _21 );
	
	// Acha o elemento [1][3] ([3][1] já transposto) retirando a linha 3 e a coluna 1
    inv.m[1][3] = + ( _00 * _12 * _23 ) + ( _02 * _13 * _20 ) + ( _03 * _10 * _22 )
				  - ( _00 * _13 * _22 ) - ( _02 * _10 * _23 ) - ( _03 * _12 * _20 );
	
	// Acha o elemento [2][3] ([3][2] já transposto) retirando a linha 3 e a coluna 2
    inv.m[2][3] = - ( _00 * _11 * _23 ) - ( _01 * _13 * _20 ) - ( _03 * _10 * _21 )
				  + ( _00 * _13 * _21 ) + ( _01 * _10 * _23 ) + ( _03 * _11 * _20 );
	
	// Acha o elemento [3][3] retirando a linha 3 e a coluna 3
    inv.m[3][3] = + ( _00 * _11 * _22 ) + ( _01 * _12 * _20 ) + ( _02 * _10 * _21 )
				  - ( _00 * _12 * _21 ) - ( _01 * _10 * _22 ) - ( _02 * _11 * _20 );

	// Acha o determinante da matriz original (ver o método "float determinant( const Matrix* )"
	// da classe Matrix para maiores informações sobre o algoritmo genérico para uma matrix NxN)
    float det = _00 * inv.m[0][0] + _01 * inv.m[1][0] + _02 * inv.m[2][0] + _03 * inv.m[3][0];
	
	// Se o determinante for 0, a matriz não é inversível
    if( FCMP( det, 0.0f ) == 0 )
		return false;

	// É mais rápido multiplicar do que dividir, por isso invertemos o determinante e depois o
	// multiplicamos pelos elementos da matriz adjunta
    det = 1.0f / det;

	// Cria a matriz inversa
    for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] = inv.m[ row ][ column ] * det;
	}

	return true;
}

/*===========================================================================================

MÉTODO identity
	Transforma esta matriz em uma matriz identidade.

============================================================================================*/

void Matrix4x4::identity( void )
{
	memset( m, 0, sizeof( float ) << 4 );
	m[0][0] = 1.0f;
	m[1][1] = 1.0f;
	m[2][2] = 1.0f;
	m[3][3] = 1.0f;
}

/*===========================================================================================

MÉTODO isIdentity
	Indica se esta matriz é uma matriz identidade.

============================================================================================*/

bool Matrix4x4::isIdentity( void ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( row != column )
			{
				if( FCMP( m[ row ][ column ], 0.0f ) != 0 )
					return false;
			}
			else
			{
				if( FCMP( m[ row ][ column ], 1.0f ) != 0 )
					return false;
			}
		}
	}
	return true;
}

