#if BLOB_MODE

#include "BlobEye.h"

// Define o tamanho do raio do olho
#define EYE_RADIUS 15.0f

// Nível de detalhamento da esfera do olho
#define EYE_LOD 30

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

BlobEye::BlobEye( void )
		: Sphere( EYE_RADIUS, EYE_LOD, NULL )
#ifdef TESTING_SOCCER_BALL
		, rot()
#endif
{
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

void BlobEye::render( void )
{
#ifdef TESTING_SOCCER_BALL
	glRotatef( rot.x, 1.0f, 0.0f, 0.0f );
	glRotatef( rot.y, 0.0f, 1.0f, 0.0f );
	glRotatef( rot.z, 0.0f, 0.0f, 1.0f );
	glScalef( 3.0f, 3.0f, 3.0f );
	
	++rot.x;
	//++rot.y;
	//++rot.z;
#else
	glRotatef( 90.0f, 0.0f, 1.0f, 0.0f );
	glScalef( 1.0f, 2.0f, 1.0f );
#endif
	
	Sphere::render();

#ifdef TESTING_SOCCER_BALL
	glScalef( 1.0f / 3.0f, 1.0f / 3.0f, 1.0f / 3.0f );
	glRotatef( -rot.z, 0.0f, 0.0f, 1.0f );
	glRotatef( -rot.y, 0.0f, 1.0f, 0.0f );
	glRotatef( -rot.x, 1.0f, 0.0f, 0.0f );
#else
	glScalef( 1.0f, 0.5f, 1.0f );
	glRotatef( -90.0f, 0.0f, 1.0f, 0.0f );
#endif
}

/*==============================================================================================

MÉTODO getCopy
	Obtém uma cópia do objeto.

==============================================================================================*/

Shape* BlobEye::getCopy( void ) const
{
	return new BlobEye();
}

#endif
