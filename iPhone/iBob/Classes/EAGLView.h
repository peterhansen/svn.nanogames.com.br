//
//  EAGLView.h
//  iBob
//
//  Created by Daniel Lopes Alves on 10/3/08.
//  Copyright Nano Games 2008. All rights reserved.
//

#ifndef OPENGL_VIEW_H
#define OPENGL_VIEW_H

#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include "Scene.h"
#include "AudioManager.h"

#if TARGET_IPHONE_SIMULATOR
	#include "AccForce.h"
#endif

@interface EAGLView : UIView
{	
	@private
		// Largura e altura da tela e do back buffer
		int32 backingWidth;
		int32 backingHeight;

		// Contexto de renderização do OpenGL
		EAGLContext* hContext;
		
		// Nome do OpenGL para o buffer de renderização e o buffer de atualização utilizados para exibir os gráficos da aplicação
		uint32 viewRenderbuffer, viewFramebuffer;
		
		// Nome do OpenGL para o buffer de profundidade associado a viewFramebuffer, se existir ( 0 se não existir )
		uint32 depthRenderbuffer;
		
		// Controlam a atualização da tela
		NSTimer* hAnimationTimer;
		NSTimeInterval animationInterval;
	
		// Ponteiro para a cena atual
		Scene* pCurrScene;
	
		// Controlador de áudio
		AudioManager* pAudioManager;
}

@property ( nonatomic, assign, setter = setAnimationInterval ) NSTimeInterval animationInterval;
@property ( nonatomic, assign, getter = getAudioManager ) AudioManager* pAudioManager;

- ( void ) startAnimation;
- ( void ) stopAnimation;
- ( void ) drawView;
- ( void ) render;
- ( void ) suspend;
- ( void ) resume;

// Aplica as opções do menu no jogo
#if BLOB_MODE
	#if TARGET_IPHONE_SIMULATOR
		- ( void ) setOpts: ( bool )reset blobColor:( Color* )pColor hardness:( float )hardness stickness:( float )stickness accelForceLevel:( float )accelForceLevel movCalcMode:( AccForceCalcMode )movCalcMode volume:( float )volume gravity:( float )gravity;
	#else
		- ( void ) setOpts: ( bool )reset blobColor:( Color* )pColor hardness:( float )hardness stickness:( float )stickness volume:( float )volume gravity:( float )gravity;
	#endif
#else
	#if TARGET_IPHONE_SIMULATOR
		- ( void ) setOpts: ( bool )reset hardness:( float )hardness stickness:( float )stickness accelForceLevel:( float )accelForceLevel movCalcMode:( AccForceCalcMode )movCalcMode volume:( float )volume gravity:( float )gravity;
	#else
		- ( void ) setOpts: ( bool )reset hardness:( float )hardness stickness:( float )stickness volume:( float )volume gravity:( float )gravity;
	#endif
#endif

@end

#endif

