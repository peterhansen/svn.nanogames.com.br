/*
 *  AccelerometerInterface.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/15/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACCELEROMETER_INTERFACE_H
#define ACCELEROMETER_INTERFACE_H

#import <UIKit/UIKit.h>
#include "AccListener.h"

@interface AccelerometerInterface : NSObject <UIAccelerometerDelegate>
{
	@private
		// TASK : Queremos armazenar???
		// Valores do acelerômetro
		//UIAccelerationValue accel[3];
	
		// Objeto que irá receber os eventos do acelerômetro
		AccListener* pListener;
}

@property (nonatomic, assign, setter=setListener) AccListener* pListener;

// Determina a frequência com que o usuário deseja receber os eventos do acelerômetro
- ( void ) setUpdateInterval:( float )secs;

// Começa a receber os eventos do acelerômetro
- ( void ) startListening;

// Pára de receber os eventos do acelerômetro
- ( void ) stopListening;

@end

#endif

