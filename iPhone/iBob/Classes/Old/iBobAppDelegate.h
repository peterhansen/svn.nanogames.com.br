//
//  iBobAppDelegate.h
//  iBob
//
//  Created by Daniel Lopes Alves on 10/3/08.
//  Copyright Nano Games 2008. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "EAGLView.h";
#import "MenuView.h";
#import "AppView.h";
#import "ViewManager.h";

@interface iBobAppDelegate : NSObject <UIApplicationDelegate, UITabBarDelegate, TransitionDelegate>
{
	IBOutlet UIWindow* hWindow;
	IBOutlet ViewManager* hViewManager;

	EAGLView* hGLView;
	MenuView* hMenuView;
}

-( void )performTransition;

@property ( nonatomic, retain ) EAGLView* hGLView;
@property ( nonatomic, retain ) MenuView* hMenuView;

@end

