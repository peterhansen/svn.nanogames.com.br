#import "iBobAppDelegate.h"
#include "AccManager.h"

// TODO : Retirar!!!
#include "AudioManager.h"

// Implementação da classe

@implementation iBobAppDelegate

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hGLView, hMenuView;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/

// TODO : Retirar!!!
//#include "AudioManager.h"

- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	// TODO : Retirar!!!
	//AudioManager::teste2();
	//UITabBarItem
	UISlideBar
	UITabBar
	
    // Define uma cor de fundo para a única janela da aplicação
    [hWindow setBackgroundColor:[UIColor blackColor]];
    
    // Cria as duas views que utilizaremos no jogo: GAME e MENU
	CGRect screenSize = [[UIScreen mainScreen] bounds];
	hGLView = [[EAGLView alloc] initWithFrame:screenSize];
	if( hGLView == nil )
	{
		// TODO: Tá, faz o q se der erro? Existe algum exit() / abort()?
	}

	// Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente,
	// é necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController
	// à view propriamente dita 
	UIViewController *hMenuViewController = [[UIViewController alloc] initWithNibName:@"MenuView" bundle:nil];
	hMenuView = ( MenuView* )[hMenuViewController view];
	[hMenuView retain]; // Impede que a desalocação do UIViewController desaloque a view
	[hMenuViewController release];
	
	// Determina qual será a primeira janela da aplicação
	[hViewManager addSubview:hMenuView];
	[hViewManager setDelegate:self];
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	// TODO Passar todo esse destrutor para o applicationWillTerminate, onde podemos debugar!!!!

	// TODO : Testa se vai vazar memória
//	int32 windowCount = [hWindow retainCount];
//	int32 viewManagerCount = [hWindow retainCount];
//	int32 glViewCount = [hWindow retainCount];
//	int32 menuViewCount = [hWindow retainCount];
	
	// TODO : Retirar!!!
	AudioManager::destroy();
	
	AccManager::release();

	[hWindow release];
	[hViewManager release];
	[hGLView release];
	[hMenuView release];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM applicationWillResignActive
	Mensagem chamada quando a aplicação vai ser suspensa.

==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	// Se estamos na view OpenGL e a aplicação vai ser suspensa, pára a animação
	if( [hGLView superview] )
	{
		[hGLView stopAnimation];
		AccManager::GetInstance()->stopListening();
	}
}

/*==============================================================================================

MENSAGEM applicationDidBecomeActive
	Mensagem chamada quando a aplicação é reiniciada.

==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	// Se estamos na view OpenGL e a aplicação está sendo reiniciada, recomeça a animação
	if( [hGLView superview] )
	{
		[hGLView startAnimation];
		AccManager::GetInstance()->startListening();
	}
}

/*==============================================================================================

MENSAGEM applicationWillTerminate
	Tells the delegate when the application is about to terminate. This method is optional. This
method is the ideal place for the delegate to perform clean-up tasks, such as freeing allocated
memory, invalidating timers, and storing application state.

==============================================================================================*/

- ( void )applicationWillTerminate:( UIApplication* )application
{
#ifdef TARGET_IPHONE_SIMULATOR
	LOG( @"applicationWillTerminate" );
#endif
}

/*==============================================================================================

MENSAGEM applicationDidReceiveMemoryWarning
	Tells the delegate when the application receives a memory warning from the system. This
method is optional. In this method, the delegate tries to free up as much memory as possible.
After the method returns (and the delegate then returns from applicationWillTerminate), the
application is terminated.

==============================================================================================*/

- ( void )applicationDidReceiveMemoryWarning:( UIApplication* )application
{
#ifdef TARGET_IPHONE_SIMULATOR
	LOG( @"applicationDidReceiveMemoryWarning" );
#endif
}

/*==============================================================================================

MENSAGEM applicationSignificantTimeChange
	Tells the delegate when there is a significant change in the time. This method is optional.
Examples of significant time changes include the arrival of midnight, an update of the time by
a carrier, and the change to daylight savings time. The delegate can implement this method to
adjust any object of the application displays time or is sensitive to time changes.

==============================================================================================*/

- ( void )applicationSignificantTimeChange:( UIApplication* )application
{
#ifdef TARGET_IPHONE_SIMULATOR
	LOG( @"applicationSignificantTimeChange" );
#endif
}

/*==============================================================================================

MENSAGEM performTransition
	Executa a transição da view atual para a segunda view existente.

==============================================================================================*/

- ( void )performTransition
{
	// Não interrompe uma transição
	if( [hViewManager isTransitioning] )
		return;
	
	// TODO : Escolher um tipo e retirar a aleatoriedade !!!

	// Escolhe randomicamente um tipo de transição e uma direção a partir dos arrays de transições e direções suportadas
	NSArray *transitions = [NSArray arrayWithObjects:kCATransitionMoveIn, kCATransitionPush, kCATransitionReveal, kCATransitionFade, nil];
	NSUInteger transitionsIndex = random() % [transitions count];
	NSString *transition = [transitions objectAtIndex:transitionsIndex];
	
	NSArray *directions = [NSArray arrayWithObjects:kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom, nil];
	NSUInteger directionsIndex = random() % [directions count];
	NSString *direction = [directions objectAtIndex:directionsIndex];
    
	// Se a view 1 já é uma subview de ViewManager, troca-a de lugar com a view 2 e vice-versa
	if( [hGLView superview] )
	{
		[hViewManager replaceSubview:hGLView withSubview:hMenuView transition:transition direction:direction duration:0.75];
	}
	else
	{
		[hGLView setOpts: true softness:[hMenuView getSoftness] stickness:[hMenuView getStickness] accelSensibility:[hMenuView getSensibility] accelForceLevel:[hMenuView getForceLevel] movCalcMode:[hMenuView getMovementCalcMode]];
		[hViewManager replaceSubview:hMenuView withSubview:hGLView transition:transition direction:direction duration:0.75];
	}
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

- ( void )transitionDidStart:( ViewManager* )manager
{
	// Se estamos saindo da view OpenGL, pára o loop de renderização
	if( [hGLView superview] == nil )
	{
		[hGLView stopAnimation];
		AccManager::GetInstance()->stopListening();
	}
}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	// Se a view OpenGL é a que está sendo exibida, inicia o loop de renderização
	if( [hGLView superview] )
	{
		[hGLView startAnimation];
		AccManager::GetInstance()->startListening();
	}
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// Temos q voltar ao estado anterior !!!
	// TODO
}

/*==============================================================================================

MENSAGEM tabBarDidSelectItem
	Evento chamado quando o usuário seleciona um item da barra de tabs.

==============================================================================================*/

- ( void )tabBar:( UITabBar* )tabBar didSelectItem:( UITabBarItem* )item
{
}

@end
