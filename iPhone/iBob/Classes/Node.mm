#include "Node.h"

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

Node::Node( const Point3f* pPosition, const Point3f* pSpeed ) : pPivot( NULL )
{
	setPosition( pPosition );
	setSpeed( pSpeed );
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

Node::~Node( void )
{
	// pPivot É APAGADO POR FORA!!! A LINHA ABAIXO NÃO DEVE SER EXECUTADA!!!
	//SAFE_DELETE( pPivot );
}

/*===========================================================================================
 
MÉTODO setPosition
	Determina a posição do objeto.

============================================================================================*/

void Node::setPosition( const Point3f *pPosition )
{
	if( pPosition )
		position = *pPosition;
	else
		position.set(); 
}

/*===========================================================================================
 
MÉTODO setSpeed
	Determina a velocidade do objeto.

============================================================================================*/

void Node::setSpeed( const Point3f *pSpeed )
{
	if( pSpeed )
		speed = *pSpeed;
	else
		speed.set();
}

/*===========================================================================================

MÉTODO attach
	Determina o nó pivot.

============================================================================================*/

void Node::attach( Node *pPivot )
{
	this->pPivot = pPivot;
}

/*===========================================================================================
 
MÉTODO detach
	Cancela a referência ao nó pivot.

============================================================================================*/

void Node::detach( void )
{
	pPivot = NULL;
}

/*===========================================================================================
 
MÉTODO loadTransform
	Carrega as transformações do nó pivot.

============================================================================================*/

void Node::loadTransform( void )
{
	if( pPivot )
	{
		glPushMatrix();
		const Point3f* pivotPosition = pPivot->getPosition();
		glTranslatef( pivotPosition->x, pivotPosition->y, pivotPosition->z );
	}
}

/*===========================================================================================
 
MÉTODO unloadTransform
	Descarrega as transformações do nó pivot.

============================================================================================*/

void Node::unloadTransform( void )
{
	if( pPivot )
		glPopMatrix();
}
