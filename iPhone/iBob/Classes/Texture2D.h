#ifndef TEXTURE2D_H
#define TEXTURE2D_H

/*==============================================================================================

CLASSE Texture2D

==============================================================================================*/

#include "Texture.h"
#include "Point3f.h"
#include "NanoTypes.h"

class Texture2D : public Texture, LoadableListener
{
	public:
		// Constrtutor
		Texture2D( LoadableListener *pListener, uint32 loadableId );
	
		// Destrtutor
		virtual ~Texture2D( void );

		// Carrega a textura de um arquivo
		bool loadTextureInFile( const NSString *pFilename );
	
		// Carrega a textura a partir dos dados recebidos como parâmetro
		bool loadTextureWithData( const NSData* pData );

		// Obtém o array de bytes da textura
		virtual const uint8* getTextureData( void ) const;
	
		// Retorna a largura da textura
		inline int32 getWidth( void ) const { return width; };
	
		// Retorna a altura da textura
		inline int32 getHeight( void ) const { return height; };

	private:
		// Cria uma textura utilizando a imagem recebida
		bool loadTexture( CGImageRef pImage );
	
		// Método para receber os eventos do objeto loadable
		virtual void loadableHandleEvent( LoadableOp op, uint32 loadableId, uint32 data );
	
		// Variável de controle de texturas 2D
		static int16 counter;

		// Ponteiro para o array de dados da textura
		uint8 *pTexData;
	
		// Listener que irá receber os eventos deste objeto
		LoadableListener* pUserListener;
	
		// Largura e altura da textura
		int32 width, height;
};

#endif