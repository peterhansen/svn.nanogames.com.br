#include "Scene.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Scene::Scene( LoadableListener* pListener, uint32 id ) : Loadable( pListener, id ), pWorlds( NULL ), pCurrCamera( NULL )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Scene::~Scene( void )
{
	SAFE_DELETE( pWorlds );
	SAFE_DELETE( pCurrCamera );
}

/*==============================================================================================

MÉTODO insertWorld

==============================================================================================*/

int16 Scene::insertWorld( World *pWorld )
{
	if( !pWorlds )
	{
		pWorlds = new DynamicVector();
		if( !pWorlds )
			return -1;
	}
	pWorlds->insert( pWorld );
	return pWorlds->getUsedLength() - 1;
}

/*==============================================================================================

MÉTODO removeWorldAt

==============================================================================================*/

bool Scene::removeWorldAt( uint16 index )
{
	if( pWorlds )
		return pWorlds->remove( index );
	return false;
}

/*==============================================================================================

MÉTODO getWorldAt

==============================================================================================*/

World* Scene::getWorldAt( uint16 index )
{
	if( pWorlds )
		return ( World* ) pWorlds->getData( index );
	return false;
}

/*==============================================================================================

MÉTODO render

==============================================================================================*/

void Scene::render( void )
{
	if( pWorlds )
	{
		load();

		if( pCurrCamera )
			pCurrCamera->place();

		const uint16 worldsLength = pWorlds->getUsedLength();

		for( uint16 i = 0; i < worldsLength; ++i )
			( ( World* ) ( pWorlds->getData(i) ) )->render();

		unload();
	}
}

/*==============================================================================================

MÉTODO setCurrentCamera

==============================================================================================*/

void Scene::setCurrentCamera( Camera* pCamera )
{
	pCurrCamera = pCamera;
}

/*==============================================================================================

MÉTODO getCurrentCamera

==============================================================================================*/

Camera* Scene::getCurrentCamera( void )
{
	return pCurrCamera;
}

/*==============================================================================================

MÉTODO update

==============================================================================================*/

void Scene::update( double timeElapsed )
{
	if( timeElapsed > 0.0f && pWorlds )
	{
		const uint16 length = pWorlds->getUsedLength();
		for( uint16 i = 0 ; i < length ; ++i )
			( ( World* ) pWorlds->getData(i) )->update( timeElapsed );
	}
}
