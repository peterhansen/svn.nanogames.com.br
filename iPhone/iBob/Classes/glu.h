/*
 *  glu.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/4/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_GLU_H
#define NANO_GLU_H

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

GLint gluProject( GLfloat objx, GLfloat objy, GLfloat objz, const GLfloat modelMatrix[16], 
				  const GLfloat projMatrix[16], const GLint viewport[4], GLfloat *winx,
				  GLfloat *winy, GLfloat *winz );

GLint gluUnProject( GLfloat winx, GLfloat winy, GLfloat winz, const GLfloat modelMatrix[16], 
					const GLfloat projMatrix[16], const GLint viewport[4], GLfloat *objx,
				    GLfloat *objy, GLfloat *objz );

void gluPerspective( GLfloat fovy, GLfloat aspect, GLfloat zNear, GLfloat zFar );

void gluLookAt( GLfloat eyex, GLfloat eyey, GLfloat eyez, GLfloat centerx, GLfloat centery,
			    GLfloat centerz, GLfloat upx, GLfloat upy, GLfloat upz );

void __gluMultMatricesd( const GLfloat a[16], const GLfloat b[16], GLfloat r[16] );

void __gluMultMatrixVecd( const GLfloat matrix[16], const GLfloat in[4], GLfloat out[4] );

int __gluInvertMatrix( const GLfloat m[16], GLfloat invOut[16] );

#endif
