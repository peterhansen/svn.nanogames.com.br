#include "DynamicVector.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

DynamicVector::DynamicVector( uint16 initialSlots, float growTax ) 
: pVector( NULL ), nUsedSlots( 0 ), nSlots( initialSlots ), growTax( growTax )
{
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

DynamicVector::~DynamicVector( void )
{
	if( pVector )
	{
		for( uint16 i=0 ; i < nUsedSlots ; ++i )
			SAFE_DELETE( pVector[i] );

		DESTROY_VEC( pVector );
	}
}

/*===========================================================================================

MÉTODO insert
	Insere os dados passados no slot com o Ìndice passado como par‚metro. deleteIfUsed indica
se os dados devem ser deletados caso o slot j· esteja ocupado ou se o n˙mero de slots do vetor
deve ser aumentado.

============================================================================================*/

bool DynamicVector::insert( void* const pData )
{
	if( !pData )
		return false;

	// Se é a primeira inserção...
	if( !pVector )
	{
		// Aloca o vetor com o número inicial de slots
		pVector = new void*[ nSlots ];
		if( !pVector )
			return false;

		// Inicializa o vetor
		memset( pVector, NULL, sizeof( void* ) * nSlots );
	}

	if( nUsedSlots >= nSlots )
	{
		// Correção no cálculo no caso de o vetor possuir apenas 1 elemento e growTax < 2.0f
		int32 newSlotNumber = (( int32 )( nSlots * growTax ) ) == nSlots ? nSlots + 1 : ( int32 ) ( nSlots * growTax );
		if( newSlotNumber > USHRT_MAX )
		{
			if( nSlots == USHRT_MAX )
				return false;

			newSlotNumber = USHRT_MAX;
		}

		void **pTemp = ( void** ) realloc( pVector, sizeof( void* ) * newSlotNumber );
		if( !pTemp )
			return false;

		pVector = pTemp;
		nSlots = newSlotNumber;
	}
	
	// Insere os dados no vetor
	pVector[nUsedSlots] = pData;

	// Incrementa o número de slots utilizados
	++nUsedSlots;

	return true;
}

/*===========================================================================================

MÉTODO remove
	Remove os dados do slot com o índice passado como parâmetro.

============================================================================================*/

bool DynamicVector::remove( uint16 index )
{
	if( !pVector || index >= nUsedSlots )
		return false;

	// Esvazia o slot
	SAFE_DELETE( pVector[ index ] );

	// Copia o último elemento no slot removido
	pVector[ index ] = pVector[ nUsedSlots - 1 ];
	pVector[ nUsedSlots - 1 ] = NULL;

	// Atualiza o número de slots usados do vetor
	--nUsedSlots;

	return true;
}

/*===========================================================================================

MÉTODO switchSlots
	Troca os slots do vetor.

============================================================================================*/

bool DynamicVector::switchSlots( uint16 slot1, uint16 slot2 )
{
	if( slot1 >= nUsedSlots || slot2 >= nUsedSlots )
		return false;

	if( slot1 != slot2 )
	{
		void *pTemp = pVector[ slot1 ];
		pVector[ slot1 ] = pVector[ slot2 ];
		pVector[ slot2 ] = pTemp;
	}
	return true;
}

/*===========================================================================================

MÉTODO getData
	Retorna os dados do slot com o índice passado como parâmetro.

============================================================================================*/

const void* DynamicVector::getData( uint16 index ) const
{
	if( !pVector || index >= nUsedSlots )
		return NULL;
	return pVector[index];
}