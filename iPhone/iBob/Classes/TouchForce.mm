#include "TouchForce.h"

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

TouchForce::TouchForce( const Point3f *pDirection ) : Force( pDirection ), touchId( 0 )
#if ! DRAG_N_DROP_USES_FORCES
, pSelectedParticle( NULL )
#endif
{
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

TouchForce::~TouchForce( void )
{
	touchId = 0;

	#if ! DRAG_N_DROP_USES_FORCES
		pSelectedParticle = NULL;
	#endif
}

/*===========================================================================================

MÉTODO getCopy
	Retorna uma cópia deste objeto.

============================================================================================*/

Force* TouchForce::getCopy( void ) const
{
	return new TouchForce( &direction );
}

