/*
 *  BlobEmitter.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 12/1/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef BLOB_EMTTER_H
#define BLOB_EMTTER_H

#if BLOB_MODE

#include "Emitter.h"

class BlobEmitter : public Emitter
{
	public:
		// Construtor
		BlobEmitter( void );

		// Destrutor
		virtual ~BlobEmitter( void ){}
	
		// Atualiza o objeto
		virtual bool update( double timeElapsed, const DynamicVector* pExternForces );
};

#endif

#endif