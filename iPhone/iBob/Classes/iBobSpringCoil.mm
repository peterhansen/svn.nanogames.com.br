#include "iBobSpringCoil.h"
#include "Utils.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

iBobSpringCoil::iBobSpringCoil( void )
: SpringCoil()
#if CONTROLLING_ORIENTATION
, pOrientation( NULL )
#endif
{
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto retornando se a mola arrebentou.

============================================================================================*/

#if CONTROLLING_ORIENTATION

bool iBobSpringCoil::update( double time )
{
	if( pOrientation )
	{
		// Recalcula a orientação da mola
		Point3f defaultOrientation( 0.0f, 1.0f, 0.0f );
		
		Point3f rotationAxis;
		const float angle  = Utils::FindAngleBetweenVectors( &rotationAxis, &defaultOrientation, pOrientation );
		
		Matrix4x4 mtx;
		Utils::TransformNormal( &originalDirection, &originalDirection, Utils::GetRotationMatrix( &mtx, &rotationAxis, angle ) );
		originalDirection.normalize();
	}
	return SpringCoil::update( time );
}

#endif