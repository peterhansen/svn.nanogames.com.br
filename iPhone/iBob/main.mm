//
//  main.m
//  iBob
//
//  Created by Daniel Lopes Alves on 10/3/08.
//  Copyright Nano Games 2008. All rights reserved.
//

#import <UIKit/UIKit.h>

#if TARGET_IPHONE_SIMULATOR
	#include "Macros.h"
#endif

int main( int argc, char* argv[] )
{
	NSAutoreleasePool* hPool = [[NSAutoreleasePool alloc] init];

#if TARGET_IPHONE_SIMULATOR
	@try
	{
#endif
		const int retVal = UIApplicationMain( argc, argv, NULL, NULL );
		[hPool release];
		return retVal;
#if TARGET_IPHONE_SIMULATOR
	}
	@catch( NSException* hException )
	{
		// TASK : De repente é bom manter este try-catch. Também poderíamos colocar o
		// "[hPool release]" dentro de um finally
		LOG( [hException reason] );
		[hPool release];
		return 1;
	}
#endif
}
