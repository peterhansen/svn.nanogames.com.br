#include "SetEffectGroup.h"

#include "Config.h"
#include "Exceptions.h"
#include "ObjcMacros.h"
#include "MathFuncs.h"
#include "Random.h"

#include "Aim.h"
#include "EffectSoccerBall.h"
#include "Fader.h"
#include "GameUtils.h"

#include "FreeKickAppDelegate.h"

// Define o número de objetos contidos no grupo
#define SET_EFFECT_GROUP_N_OBJS 3

// Define as propriedades do fader que escure a tela antes de o jogador determinar o efeito
// a ser aplicado na bola
#define FADER_START_ALPHA 0.00f
#define FADER_FINAL_ALPHA 0.60f
#define FADERTRANSITION_TIME 1.0f

// Configurações das animações
#define SET_EFFECT_GROUP_ANIM_DUR_FADE_IN		0.200f
#define SET_EFFECT_GROUP_ANIM_DUR_FADE_SCALE	0.200f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

SetEffectGroup::SetEffectGroup( SetEffectGroupListener* pEffectListener, InterfaceControlListener* pInterfaceListener )
			   : ObjectGroup( SET_EFFECT_GROUP_N_OBJS ),
				 #if TARGET_IPHONE_SIMULATOR
					EventListener( EVENT_TOUCH ),
				 #endif
				 AccelerometerListener( ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC ),
				 SpriteListener(),
				 InterfaceControl( pInterfaceListener ),
				 pFader( NULL ),
				 pAim( NULL ),
				 pTarget( NULL ),
				 pEffectListener( pEffectListener ),
				 kickPwrPercent( 0.0f ),
				 targetFinalScaleX( 0.0f ),
				 targetFinalScaleY( 0.0f )
{
	build();
}

/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

SetEffectGroup::~SetEffectGroup( void )
{
	clear();
}

/*===========================================================================================
 
MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

bool SetEffectGroup::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
		case EVENT_TOUCH_MOVED:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				// Obtém a informação do toque
				const NSArray* pTouchesArray = ( NSArray* )pParam;
				const UITouch* pTouch = [pTouchesArray objectAtIndex: 0];
				CGPoint temp = [pTouch locationInView : NULL];

				Point3f point( temp.x, temp.y );
				point = Utils::MapPointByOrientation( &point );
				setAimPosition( point.x, point.y );

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;

		case EVENT_TOUCH_ENDED:
		case EVENT_TOUCH_CANCELED:
			break;
	}
	return true;
}

#endif

/*===========================================================================================
 
MÉTODO OnAccelerate
	Recebe os eventos do acelerômetro.

============================================================================================*/

void SetEffectGroup::onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration )
{
	if( !isActive() || ( pAim->getState() == AIM_STATE_SHOT ) )
		return;

	float angleX, angleY;
	GameUtils::GetDeviceAngles( &angleX, &angleY, pRestPosition, pAcceleration );
	
	// Acha onde devemos posicionar a mira na bola. Para a movimentação em X, utilizamos
	// a rotação em Y. Para a movimentação em Y, utilizamos a rotação em Z.
	const Point3f* pPos = pTarget->getPosition();
	float maxX = pPos->x + pTarget->getWidth();
	float maxY = pPos->y + pTarget->getHeight();
	
	float xPos = NanoMath::lerp( ( angleY + ACCEL_HALF_MOVEMENT_FREEDOM_Y ) / ACCEL_MOVEMENT_FREEDOM_Y, pPos->x, maxX );
	float yPos = NanoMath::lerp( ( angleX + ACCEL_HALF_MOVEMENT_FREEDOM_X ) / ACCEL_MOVEMENT_FREEDOM_X, pPos->y, maxY );
	
	// OBS: A inversão do valor gerado por z depende da orientação do device
	yPos = pPos->y + ( maxY - yPos ); 

	// Modifica a posição da mira, aplicando uma perturbação de acordo com a força do chute
	float xNoise = 0.0f, yNoise = 0.0f;
	
	// TODOO : Verificar se vai precisar desta noise. Além disso, maneirar, pq está com MTA noise
//	if( kickPwrPercent > MAX_KICK_POWER_PERCENT_WITHOUT_NOISE )
//	{
//		float noisePercent = NanoMath::lerp( ( kickPwrPercent - MAX_KICK_POWER_PERCENT_WITHOUT_NOISE ) / ( 1.0f - MAX_KICK_POWER_PERCENT_WITHOUT_NOISE ), MIN_NOISE, MAX_NOISE );
//		float maxNoiseX = pTarget->getWidth() * noisePercent;
//		float maxNoiseY = pTarget->getHeight() * noisePercent;
//
//		xNoise = Random::GetFloat( -maxNoiseX, maxNoiseX );
//		yNoise = Random::GetFloat( -maxNoiseY, maxNoiseY );
//	}

	setAimPosition( xPos + xNoise, yPos + yNoise );
}

/*===========================================================================================
 
MÉTODO reset
	Coloca o objeto em seu estado inicial.

============================================================================================*/

void SetEffectGroup::reset( float difficultyPercent, float kickPowerPercent )
{
	// Determina o tempo que o jogador terá para determinar o efeito da bola
	difficultyPercent = NanoMath::clamp( difficultyPercent, 0.0f, 1.0f );
	
	// Determina a precisão da mira. Esta varia de acordo com a força do chute: quanto mais forte o chute, menos precisa será a mira
	kickPowerPercent = NanoMath::clamp( kickPowerPercent, 0.0f, 1.0f );
	setAverageOldValuesWeight( NanoMath::lerp( kickPowerPercent, MAX_OLD_VALUES_WEIGHT, MIN_OLD_VALUES_WEIGHT ) );
	kickPwrPercent = kickPowerPercent;
	
	// Coloca a mira no centro do alvo
	Point3f targetPos = *( pTarget->getPosition() );
	setAimPosition( targetPos.x + ( pTarget->getWidth() * 0.5f ), targetPos.y + ( pTarget->getHeight() * 0.5f ) );
	
	// Faz a mira piscar
	pAim->setState( AIM_STATE_BLINKING );
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/

void SetEffectGroup::build( void )
{
	Load();
}

/*===========================================================================================
 
 MÉTODO Load
 Inicializa o objeto.
 
 ============================================================================================*/

void SetEffectGroup::Load()
{
	
	{ // Evita erros com os gotos

	// Cria o "escurecedor" de tela
	Color black( COLOR_BLACK );
	pFader = new Fader( black, FADER_START_ALPHA, FADER_FINAL_ALPHA, FADERTRANSITION_TIME );
	if( !pFader || !insertObject( pFader ) )
	{
		SAFE_DELETE( pFader );
		goto Error;
	}
		
	// OBS0: Alguém poderia dizer: "Caraca, que doidera! Fader escurece automaticamente, pq cancelar tal animação utilizando pFader->setActive( false ) e refazer
	// tudo na mão?!". Eu responderia como Wolfgang Krauser: "LEG TOMAHAWK!!!!!!!"
	pFader->setActive( false );
	
	// Cria o alvo
	pTarget = new EffectSoccerBall();
	if( !pTarget || !insertObject( pTarget ) )
	{
		SAFE_DELETE( pTarget );
		goto Error;
	}
	pTarget->setPosition(( SCREEN_WIDTH - pTarget->getWidth() ) * 0.5f, ( SCREEN_HEIGHT - pTarget->getHeight() ) * 0.5f );
		
	targetFinalScaleX = pTarget->getScale()->x;
	targetFinalScaleY = pTarget->getScale()->y;
	
	// Cria a mira
	pAim = new Aim();
	if( !pAim || !insertObject( pAim ) )
	{
		SAFE_DELETE( pAim );
		goto Error;
	}
	pAim->setPosition(( SCREEN_WIDTH - pAim->getWidth() ) * 0.5f, ( SCREEN_HEIGHT - pAim->getHeight() ) * 0.5f );
	pAim->setListener( this );

	return;

	} // Evita erros com os gotos
	
	// Label de tratamento de erros
	Error:
		clear();

#if TARGET_IPHONE_SIMULATOR
		throw ConstructorException( "SetEffectGroup::Load(): unable to create object" );
#else
		throw ConstructorException();
#endif
}

/*===========================================================================================
 
 MÉTODO Unload
Descarrega os assets da memória.
 
 ============================================================================================*/
void SetEffectGroup::Unload( void )
{
	removeAllObjects();
}

/*===========================================================================================
 
MÉTODO clear
	Limpa as variáveis do objeto.

============================================================================================*/

void SetEffectGroup::clear( void )
{
	pFader = NULL; 
	pAim = NULL;
	pTarget = NULL;
	pEffectListener = NULL;
}

/*===========================================================================================
 
MÉTODO setAimPosition
	Movimenta a mira dentro da bola de efeito.

============================================================================================*/

void SetEffectGroup::setAimPosition( float posX, float posY )
{
	// Modifica a posição atual da mira
	pAim->setCenterPosition( posX, posY );
	Point3f aimCenterPos = pAim->getCenterPosition();
	
	// Verifica se o centro da mira saiu do alvo
	Point3f targetCenterPos = pTarget->getCenterPosition();
	Point3f diff = aimCenterPos - targetCenterPos;

	if( diff.getModule() > pTarget->getRadius() )
	{
		// Calcula o quanto a mira deve retroceder para voltar a ter o seu centro no
		// interior do alvo
		diff -= ( diff.getNormalized() * pTarget->getRadius() );

		// Coloca a mira numa posição válida
		pAim->setCenterPosition( aimCenterPos.x - diff.x, aimCenterPos.y - diff.y );
	}
}

/*===========================================================================================
 
MÉTODO setEffect
	Confirma o efeito atual.

============================================================================================*/

void SetEffectGroup::setEffect( void )
{
	onEffectSet();
}

/*===========================================================================================
 
MÉTODO onEffectSet
	Chama o listener informando o efeito aplicado à bola.

============================================================================================*/

void SetEffectGroup::onEffectSet( void )
{
	if( pAim->getState() != AIM_STATE_SHOT )
		pAim->setState( AIM_STATE_SHOT );
}

/*===========================================================================================
 
MÉTODO getEffectHeight
	Retorna a altura gerada pelo efeito.

============================================================================================*/

float SetEffectGroup::getEffectHeight( void ) const
{
	return NanoMath::lerp( ( pAim->getCenterPosition().y - pTarget->getPosition()->y ) / pTarget->getHeight(), ISO_HEIGHT_MIN_VALUE, ISO_HEIGHT_MAX_VALUE );
}

/*===========================================================================================
 
MÉTODO getEffectFactor
	Retorna o fator de efeito do chute.

============================================================================================*/

float SetEffectGroup::getEffectFactor( void ) const
{
	return NanoMath::lerp( ( pAim->getCenterPosition().x - pTarget->getPosition()->x ) / pTarget->getWidth(), ISO_FX_FACTOR_MIN_VALUE, ISO_FX_FACTOR_MAX_VALUE );
}

/*===========================================================================================
 
MÉTODO getEffectAngularSpeed
	Retorna a velocidade e o eixo de rotação da bola.

============================================================================================*/

void SetEffectGroup::getEffectAngularSpeed( float& angularSpeed, Point3f& angularSpeedAxis, float powerUsed ) const
{
	Point3f aimCenterPos = pAim->getCenterPosition();
	Point3f targetCenterPos = pTarget->getCenterPosition();
	
	// OLD : Não ficava tão bom, pois na imagem de um círculo, não conseguimos obter os valores extremos de x, y, exceto pelos nos casos de 0, 90, 180 e 270 graus
//	angularSpeed.x = 0.0f;
//	angularSpeed.y = NanoMath::lerp( ( aimCenterPos.x - targetCenterPos.x ) / ( pTarget->getWidth() * 0.5f ), ISO_CURVE_MIN_ANG_SPEED, ISO_CURVE_MAX_ANG_SPEED );
//	angularSpeed.z = - NanoMath::lerp( ( aimCenterPos.y - targetCenterPos.y ) / ( pTarget->getHeight() * 0.5f ), ISO_CURVE_MIN_ANG_SPEED, ISO_CURVE_MAX_ANG_SPEED );
	
	Point3f diff = aimCenterPos - targetCenterPos;
	float percent = diff.getModule() / pTarget->getRadius();
	
	angularSpeed = NanoMath::lerp( percent, ISO_CURVE_MIN_ANG_SPEED, ISO_CURVE_MAX_ANG_SPEED );
	
	// Obtém o vetor ortogonal
	diff.set( diff.y, -diff.x, 0.0f );
	diff.normalize();

	// OBS: Não rotaciona a bola em torno do eixo x, já que o chute do jogador nunca acerta a bola vindo do eixo z 
	angularSpeedAxis.set( 0.0f, -diff.y, -diff.x );	

	// TODOOO
//	Point3f xAxis( 1.0f, 0.0f, 0.0f );
//	Point3f xRealWorld = GameUtils::isoGlobalToPixels( xAxis );
//	
//	Point3f aux;
//	float angle = Utils::FindRotationBetweenVectors( &aux, &xAxis, &xRealWorld );
//	
//	Matrix4x4 rotMtx;
//	Utils::TransformCoord( &angularSpeedAxis, &angularSpeedAxis, Utils::GetRotationMatrix( &rotMtx, angle, &aux ) );
	
	// TODOOO
//	diff.set( 0.0f, 0.0f, 1.0f );
//
//	diff = -diff;
//	diff = GameUtils::isoGlobalToPixels( diff.x, 0.0f, diff.y ) + GameUtils::isoGlobalToPixels( 0.0f, diff.x, diff.y );
//	angularSpeedAxis.set( -diff.y, -diff.x );

	// Multiplica a velocidade angular pelo percentual da força máxima aplicada
	powerUsed = NanoMath::clamp( ( powerUsed - ISO_POWER_MIN_VALUE ), 0.0f, ( ISO_POWER_MAX_VALUE - ISO_POWER_MIN_VALUE ) );
	angularSpeed *= powerUsed / ( ISO_POWER_MAX_VALUE - ISO_POWER_MIN_VALUE );
}

/*===========================================================================================
 
MÉTODO onAnimEnded
	Chamado quando o sprite termina um sequência de animação.

============================================================================================*/

void SetEffectGroup::onAnimEnded( Sprite* pSprite )
{
	pAim->setVisible( false );

	if( pEffectListener != NULL )
		pEffectListener->onEffectSet();
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool SetEffectGroup::update( float timeElapsed )
{
	if( !ObjectGroup::update( timeElapsed ) )
		return false;

	switch( getInterfaceControlState() )
	{
		case INTERFACE_CONTROL_STATE_SHOWING:
			{
				Color faderColor = *( pFader->getColor() );
				if( faderColor.getFloatA() < FADER_FINAL_ALPHA )
				{
					faderColor.setFloatA( faderColor.getFloatA() + (( timeElapsed * ( FADER_FINAL_ALPHA - FADER_START_ALPHA ) ) / SET_EFFECT_GROUP_ANIM_DUR_FADE_IN ) );

					if( faderColor.getFloatA() > FADER_FINAL_ALPHA )
					{
						faderColor.setFloatA( FADER_FINAL_ALPHA );
						
						[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_FX_BALL_GROW AtIndex: SOUND_INDEX_FX_BALL_GROW Looping: false];
					}

					pFader->setColor( &faderColor );
				}
				else
				{
					float scaleFactorX = pTarget->getScale()->x + ( ( timeElapsed * targetFinalScaleX ) / SET_EFFECT_GROUP_ANIM_DUR_FADE_SCALE );
					float scaleFactorY = pTarget->getScale()->y + ( ( timeElapsed * targetFinalScaleY ) / SET_EFFECT_GROUP_ANIM_DUR_FADE_SCALE );
					
					if( scaleFactorX > targetFinalScaleX )
						scaleFactorX = targetFinalScaleX;
					
					if( scaleFactorY > targetFinalScaleY )
						scaleFactorY = targetFinalScaleY;
					
					if( ( scaleFactorX == targetFinalScaleX ) && ( scaleFactorY == targetFinalScaleY ) )
					{
						pAim->setVisible( true );
						setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
					}

					pTarget->setScale( scaleFactorX, scaleFactorY );
					pTarget->setPosition( ( SCREEN_WIDTH - pTarget->getWidth() ) * 0.5f, ( SCREEN_HEIGHT - pTarget->getHeight() ) * 0.5f );
				}
			}
			break;
			
		case INTERFACE_CONTROL_STATE_HIDING:
			{
				float scaleFactorX = pTarget->getScale()->x;
				float scaleFactorY = pTarget->getScale()->y;

				if( ( scaleFactorX > 0.0f ) || ( scaleFactorY >  0.0f ) )
				{
					scaleFactorX -= ( ( timeElapsed * targetFinalScaleX ) / SET_EFFECT_GROUP_ANIM_DUR_FADE_SCALE );
					scaleFactorY -= ( ( timeElapsed * targetFinalScaleY ) / SET_EFFECT_GROUP_ANIM_DUR_FADE_SCALE );
					
					if( scaleFactorX <  0.0f )
						scaleFactorX =  0.0f;
					
					if( scaleFactorY <  0.0f )
						scaleFactorY =  0.0f;

					pTarget->setScale( scaleFactorX, scaleFactorY );
					pTarget->setPosition( ( SCREEN_WIDTH - pTarget->getWidth() ) * 0.5f, ( SCREEN_HEIGHT - pTarget->getHeight() ) * 0.5f );
				}
				else
				{
					Color faderColor = *( pFader->getColor() );
					
					faderColor.setFloatA( faderColor.getFloatA() - (( timeElapsed * ( FADER_FINAL_ALPHA - FADER_START_ALPHA ) ) / SET_EFFECT_GROUP_ANIM_DUR_FADE_IN ));

					if( faderColor.getFloatA() < FADER_START_ALPHA )
					{
						faderColor.setFloatA( FADER_START_ALPHA );
						setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
					}

					pFader->setColor( &faderColor );
				}
			}
			break;

		case INTERFACE_CONTROL_STATE_SHOWN:
			break;
	}

	return true;
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exebição do controle.

============================================================================================*/

void SetEffectGroup::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		setActive( true );
		setVisible( true );
	}
	else
	{
		Color c( 0.0f, 0.0f, 0.0f, FADER_START_ALPHA );
		pFader->setColor( &c );

		pTarget->setScale( 0.0f, 0.0f );
		pTarget->setPosition( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
		
		pAim->setVisible( false );

		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/

void SetEffectGroup::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/

void SetEffectGroup::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}
