#include "ParkingLot.h"

#include "Exceptions.h"
#include "GLHeaders.h"
#include "TextureFrame.h"
#include "TexturePattern.h"

// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
		if( !pObject || !pGroup->insertObject( pObject ) )	\
		{													\
			delete pObject;									\
			goto Error;										\
		}

// Número de objetos no grupo
#define PARKING_LOT_N_OBJS	0 // OLD 4

// Índices dos sprites contidos no grupo
#define PARKING_LOT_TOP		0
#define PARKING_LOT_LEFT	1
#define PARKING_LOT_RIGHT	2
#define PARKING_LOT_BOTTOM	3

// OLD
//#define PARKING_LOT_VOLUME_WIDTH ( CAMERA_RIGHT_LIMIT - CAMERA_LEFT_LIMIT )
//#define PARKING_LOT_VOLUME_HEIGHT ( CAMERA_BOTTOM_LIMIT - CAMERA_TOP_LIMIT )

#define PARKING_LOT_VOLUME_WIDTH 7479
#define PARKING_LOT_VOLUME_HEIGHT 4872

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

ParkingLot::ParkingLot( void )
		   : ObjectGroup( PARKING_LOT_N_OBJS ), viewVolumeWidth( PARKING_LOT_VOLUME_WIDTH ), viewVolumeHeight( PARKING_LOT_VOLUME_HEIGHT )
{
	build();
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

ParkingLot::~ParkingLot( void )
{
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/

void ParkingLot::build( void )
{
	{ // Evita erros de compilação por causa dos gotos

//		TextureFrame imgFrame( 0.0f, 0.0f, 64.0f, 64.0f, 0.0f, 0.0f );
//
//		#if CONFIG_TEXTURES_16_BIT
//			TexturePattern *pTop = new TexturePattern( "pl", &imgFrame, ResourceManager::GetTexture2D /* ResourceManager::Get16BitTexture2D */ );
//		#else
//			TexturePattern *pTop = new TexturePattern( "pl" );
//		#endif
//
//		CHECKED_GROUP_INSERT( this, pTop );
//		
//		TexturePattern *pLeft = new TexturePattern( pTop );
//		CHECKED_GROUP_INSERT( this, pLeft );
//		
//		TexturePattern *pRight = new TexturePattern( pTop );
//		CHECKED_GROUP_INSERT( this, pRight );
//		
//		TexturePattern *pBottom = new TexturePattern( pTop );
//		CHECKED_GROUP_INSERT( this, pBottom );
//
//		// Determina o tamanho dos patterns
//		float height = pFieldSize->y * 2.0f;
//		float width = pFieldSize->x * 2.0f;
//
//		pTop->setSize( width, imgFrame.height );
//		pLeft->setSize( imgFrame.width, height );
//		pRight->setSize( imgFrame.width, height );
//		pBottom->setSize( width, imgFrame.height );
//		
//		// Posiciona os patterns
//		pTop->setPosition( pFieldPos->x, pFieldPos->y );
//		pLeft->setPosition( pTop->getPosition()->x, pTop->getPosition()->y + pTop->getHeight() );
//		pRight->setPosition( pTop->getPosition()->x + pTop->getWidth() - pRight->getWidth(), pLeft->getPosition()->y );
//		pBottom->setPosition( pLeft->getPosition()->x, pLeft->getPosition()->y + pLeft->getHeight() );
//		
//		// Determina o tamanho do grupo
//		setSize( pTop->getWidth(), ( pTop->getHeight() * 2.0f ) + pLeft->getHeight() );
		
		return;
		
	} // Evita erros de compilação por causa dos gotos

	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "void ParkingLot::build( void ): Unable to create object" );
	#else
		throw ConstructorException( "" );
	#endif
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/

// OLD
//#define PARKING_LOT_TOP_HEIGHT  850.0f
//
//#define PARKING_LOT_MID_WIDTH	 2200.0f
//#define PARKING_LOT_MID_HEIGHT  850.0f
//
//#define PARKING_LOT_BOT_WIDTH	 2800.0f
//#define PARKING_LOT_BOT_HEIGHT  800.0f

#define PARKING_LOT_TOP_HEIGHT	  700.0f

#define PARKING_LOT_MID_WIDTH	 2250.0f
#define PARKING_LOT_MID_HEIGHT	  950.0f

#define PARKING_LOT_BOT_WIDTH	 2900.0f
#define PARKING_LOT_BOT_HEIGHT	 1700.0f

bool ParkingLot::render( void )
{
	if( !isVisible() )
		return false;
	
	Quad aux;
	glColor4ub( 0, 0, 0, 255 );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x, position.y, position.z );

	aux.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 0.0f, 0.0f, 0.0f );
	aux.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( viewVolumeWidth, 0.0f, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 0.0f, PARKING_LOT_TOP_HEIGHT, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( viewVolumeWidth, PARKING_LOT_TOP_HEIGHT, 0.0f );
	aux.render();
	
	glPopMatrix();
	
	glPushMatrix();
	
	glTranslatef( position.x, position.y, position.z );
	
	float startX = viewVolumeWidth - PARKING_LOT_MID_WIDTH;
	float startY = PARKING_LOT_TOP_HEIGHT;
	aux.quadVertexes[ QUAD_TOP_LEFT ].setPosition( startX, startY, 0.0f );
	aux.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( startX + PARKING_LOT_MID_WIDTH, startY, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( startX, startY + PARKING_LOT_MID_HEIGHT, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( startX + PARKING_LOT_MID_WIDTH, startY + PARKING_LOT_MID_HEIGHT, 0.0f );
	aux.render();

	glPopMatrix();
	
	glPushMatrix();
	
	glTranslatef( position.x, position.y, position.z );
	
	startX = viewVolumeWidth - PARKING_LOT_BOT_WIDTH;
	startY = viewVolumeHeight - PARKING_LOT_BOT_HEIGHT;
	aux.quadVertexes[ QUAD_TOP_LEFT ].setPosition( startX, startY, 0.0f );
	aux.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( startX + PARKING_LOT_BOT_WIDTH, startY, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( startX, startY + PARKING_LOT_BOT_HEIGHT, 0.0f );
	aux.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( startX + PARKING_LOT_BOT_WIDTH, startY + PARKING_LOT_BOT_HEIGHT, 0.0f );
	aux.render();

	glPopMatrix();

	return true;
}

