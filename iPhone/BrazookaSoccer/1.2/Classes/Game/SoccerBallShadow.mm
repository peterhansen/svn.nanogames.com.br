#include "SoccerBallShadow.h"

#include "GameUtils.h"
#include "Iso_Defines.h"
#include "SoccerBall.h"

#include "MathFuncs.h"

// Define a altura máxima da bola para efeitos do cálculo da sombra
#define BALL_SHADOW_BALL_MAX_HEIGHT 5.0f

// Intervalo de scale aplicado à sombra dependendo da altura da bola
#define BALL_SHADOW_MIN_SCALE 0.30f
#define BALL_SHADOW_MAX_SCALE 1.05f

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

SoccerBallShadow::SoccerBallShadow( const SoccerBall* pBall ) : Object(), pBall( pBall )
{
}

/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

SoccerBallShadow::~SoccerBallShadow( void )
{
	pBall = NULL;
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/

bool SoccerBallShadow::render( void )
{
	if( !isVisible() )
		return false;
	
	glDisable( GL_DEPTH_TEST );
	
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	// Desabilita a texturização
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glDisable( GL_TEXTURE_2D );
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );
	
	glColor4ub( 0, 0, 0, 85 );
		
	float width = pBall->getWidth() * scale.x;
	float height = pBall->getHeight() * scale.y;

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	// Posiciona o objeto
	glTranslatef( position.x + ( width * 0.5f ), position.y + ( height * 0.5f ), 0.0f );

	// Redimensiona o objeto
	glScalef( width, height, 1.0f );

	// Renderiza a sombra
	pBall->getBallVertexSet()->render();
	
	glPopMatrix();
	
	glColor4ub( 255, 255, 255, 255 );

	if( tex2DWasEnabled == GL_TRUE )
		glEnable( GL_TEXTURE_2D );

	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );

	return true;
}

/*===========================================================================================
 
MÉTODO updatePosition
	Atualiza a posição do objeto na tela.

============================================================================================*/

void SoccerBallShadow::updatePosition( void )
{
	// Garante que a sombra está no chão
	realPosition.y = 0.0f;
	
	float currScale = NanoMath::lerp( NanoMath::clamp( pBall->getRealPosition()->y, 0.0f, BALL_SHADOW_BALL_MAX_HEIGHT ) / BALL_SHADOW_BALL_MAX_HEIGHT, BALL_SHADOW_MAX_SCALE, BALL_SHADOW_MIN_SCALE );
	setScale( currScale, currScale, 1.0f );
	
	float width = pBall->getWidth() * currScale;
	float height = pBall->getHeight() * currScale;

	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPosition( p.x + ISO_BALL_X_OFFSET - ( width * 0.5f ), p.y + ISO_BALL_Y_OFFSET - ( height * 0.5f ) + 1.0f, 0.0f );
}
