#include "EffectSoccerBall.h"

// Define o tamanho da bola
#define BALL_DIAMETER 180.0f
#define BALL_RADIUS ( BALL_DIAMETER * 0.5f )

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

EffectSoccerBall::EffectSoccerBall( void )
				#if CONFIG_TEXTURES_16_BIT
					: RenderableImage( "bfx", RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D  )
				#else
					: RenderableImage( "bfx" )
				#endif
					, radius( BALL_RADIUS )
{
	setScale( BALL_DIAMETER / getWidth(), BALL_DIAMETER / getHeight() );
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

EffectSoccerBall::~EffectSoccerBall( void )
{
}

/*===========================================================================================
 
MÉTODO getCenterPosition
	Retorna a posição do centro da bola.

============================================================================================*/

Point3f EffectSoccerBall::getCenterPosition( void ) const
{
	return Point3f( position.x + ( getWidth() * 0.5f ), position.y + ( getHeight() * 0.5f ), 0.0f );
}
