/*
 *  InfoTab.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef INFO_TAB_H
#define INFO_TAB_H 1

#include "Config.h"

#include "Color.h"
#include "InterfaceControl.h"
#include "ObjectGroup.h"

// Forward Declarations
class GameInfo;
class RenderableImage;
class InterfaceControlListener;

class InfoTab : public ObjectGroup, public InterfaceControl
{
	public:
		// Construtor
		// Exceções: ConstructorException
		InfoTab( const GameInfo* pGameInfo, InterfaceControlListener* pListener );
	
		// Destrutor
		virtual ~InfoTab( void );
	
		// Indica se houve colisão entre o toque e o botão de "Fechar"
		bool checkCloseButtonCollision( const Point3f* touchPos );
	
		// Indica qual o estado do botão de "Fechar"
		void setCloseButtonState( bool pressed, const Color& color );

		// Retorna se o botão de "Fechar" está pressionado
		bool isCloseButtonPressed( void ) const;
	
		// Retorna se está executando a animação de rolagem dos números
		bool isDoingUpdateAnimation( void ) const;
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz a animação de rolagem dos números
		void startUpdateAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Métodos que atualizam as informações exibidas
		void updateScore( bool immediately );
		void updateTries( void );
		void updateLevel( void );

	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );
	
		// Limpa as variáveis do objeto
		void clear( void );
	
		// Faz a animação de rolagem dos pontos
		bool updateScoreLabel( float timeElapsed );
		void updateScoreGoal( void );
		void updateTotalPoints( void );
	
		// Indica se estamos fazendo a animação de rolagem dos números
		bool doingUpdateAnimation;
	
		// Indica se o botão de "Fechar" está pressionado
		bool closeButtonPressed;
	
		// Indica se o objeto está se deslocando para a esquerda na animação de entrar na tela
		bool goingLeft;
	
		// Auxiliares na animação dos números
		float scoreShown;
		float scoreDiff;
	
		float targetShown;
		float targetDiff;
	
		// Informações do jogo atual
		const GameInfo* pGameInfo;
	
		// Imagens que indicam o resultado de cada tentativa
		RenderableImage* tryResultV[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
		RenderableImage* tryResultX[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
};

// Retorna se o botão de "Fechar" está pressionado
inline bool InfoTab::isCloseButtonPressed( void ) const
{
	return closeButtonPressed;
}

// Retorna se está executando a animação de rolagem dos números
inline bool InfoTab::isDoingUpdateAnimation( void ) const
{
	return doingUpdateAnimation;
}

#endif
