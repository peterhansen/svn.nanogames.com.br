#include "MeterBar.h"

#include "MathFuncs.h"

#if TARGET_IPHONE_SIMULATOR
	#include "Utils.h"
#endif

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

MeterBar::MeterBar( float minValue, float maxValue, float initValue )
			:
			#if TARGET_IPHONE_SIMULATOR
				EventListener( EVENT_TOUCH ),
			#endif
				minValue( minValue ),
				maxValue( maxValue ),
				currValue( initValue )
{
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

MeterBar::~MeterBar( void )
{
}

/*===========================================================================================
 
MÉTODO setCurrValue
	Determina o valor atual do medidor (absoluto).

============================================================================================*/

void MeterBar::setCurrValue( float value )
{
	setCurrValuePercent( ( NanoMath::clamp( value, minValue, maxValue ) - minValue ) / ( maxValue - minValue ) );
}

/*===========================================================================================
 
MÉTODO setCurrValuePercent
	Determina o valor atual do medidor (porcentagem).

============================================================================================*/

void MeterBar::setCurrValuePercent( float percent )
{
	currValue = NanoMath::lerp( NanoMath::clamp( percent, 0.0f, 1.0f ), minValue, maxValue );
}

/*===========================================================================================
 
MÉTODO setMeterInterval
	Determina o intervalo de valores aceito pelo medidor.

============================================================================================*/

void MeterBar::setMeterInterval( float min, float max )
{
	minValue = min;
	maxValue = max;
}
