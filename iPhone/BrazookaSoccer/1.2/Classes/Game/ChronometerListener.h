/*
 *  ChronometerListener.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/19/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CHRONOMETER_LISTENER_H
#define CHRONOMETER_LISTENER_H 1

class ChronometerListener
{
	public:
		// Destrutor
		virtual ~ChronometerListener( void ){};

		// Método chamado quando o cronômetro termina sua contagem
		virtual void onStopTimeReached( void ) = 0;
};

#endif
