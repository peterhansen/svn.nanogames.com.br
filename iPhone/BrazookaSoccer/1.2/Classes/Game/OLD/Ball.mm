#include "Ball.h"
#include "Ball_Defines.h"

#include "Macros.h"
#include "MathFuncs.h"
#include "Random.h"

// Define o número de objetos no grupo (imagem da bola e sua sombra)
#define BALL_N_OBJECTS 2

// Define o número de objetos no grupo (imagem da bola e sua sombra)
#define BALL_OBJ_INDEX_AURA		0
#define BALL_OBJ_INDEX_BALL		1

// "fator de quicada" da bola (qual fração da velocidade ela retoma ao quicar)
#define BALL_BOUNCE_FACTOR 0.7f

// "fator de elasticidade" da bola ao colidir com uma trave, ou seja, quantos
// % da velocidade máxima que ela pode alcançar ao ser rebatida por uma trave.
#define REAL_ELASTIC_FACTOR ( -0.55f )

// velocidade vertical mínima da bola - valor usado para evitar que bola quique no chão indefinidamente, ou seja, que ela possa rolar
#define REAL_MIN_Y_SPEED 0.7f

// distância exata em que a bola toca a trave
#define REAL_BALL_POST_CONTACT_DISTANCE ( REAL_BALL_RADIUS + REAL_POST_WIDTH )

// fator de desaceleração da bola após ela colidir com algo (valor multiplicado pela velocidade a cada segundo)
#define BALL_SLOW_DOWN_FACTOR 0.81f

// velocidade máxima (positiva ou negativa) da bola após colisão com goleiro ou barreira
#define AFTER_COLLISION_MIN_Y -9
#define AFTER_COLLISION_MAX_Y 22

/*===============================================================================

CONSTRUTOR

================================================================================*/

Ball::Ball( void )
	 :	RealPosOwner(),
		ObjectGroup( BALL_N_OBJECTS ),
		realInitialPosition(),
		realSpeed(),
		realLastGoalPosition(),
		realInitialYSpeed( 0.0f ),
		accTime( 0.0f ),
		accTimeY( 0.0f ),
		ballState( BALL_STATE_UNDEFINED ),
		timeToGoal( 0.0f ),
		fxActive( true ),
		checkedGoalCollision( false ),
		lastCollisionTime( 0.0f ),
		lastPlayResult( BALL_STATE_UNDEFINED ),
		lastCollisionDirection(),
		isReplay( false ),
		movFunc()
{
}

/*===============================================================================

DESTRUTOR

================================================================================*/

Ball::~Ball( void )
{
	ballState = BALL_STATE_UNDEFINED;
	lastPlayResult = BALL_STATE_UNDEFINED;
}

/*===============================================================================

MÉTODO reset

================================================================================*/

void Ball::reset( const Point3f* pRealPosition )
{
	setState( BALL_STATE_STOPPED );

	realSpeed.set();
	
	movFunc.set();
	
	accTime = 0.0f;
	accTimeY = 0.0f;
	
	fxActive = true;
	checkedGoalCollision = false;

	if( pRealPosition )
		setRealPosition( pRealPosition->x, pRealPosition->y, pRealPosition->z );
	else
		setRealPosition( REAL_PENALTY_POSITION );
	
	setVisible( true );
}

/*======================================================================================

MÉTODO kick
   Recebe um ponteiro para uma jogada e define os novos valores da velocidade da bola. O
valor retornado refere-se à posição estimada da bola, quando esta cruza a linha de
fundo. Para o cálculo da posição da bola, não são considerados possíveis obstáculos
no caminho, como a barreira, goleiro ou jogadores.

========================================================================================*/

Point3f* Ball::kick( Point3f* pOut, Play* pPlay, bool replay )
{
	if( pPlay )
	{
		isReplay = replay;
		if( !replay )
		{
			lastCollisionTime = 0.0f;
			lastCollisionDirection.set();
		}

		///<DM> Garante que o efeito de chute não se adultera no replay (sem isso, a cada "play" do replay
		/// o efeito aumentaria).
		
		if (isReplay)
		{
			pPlay->direction = iBackupDirection;
			pPlay->fx=iBackupFX;
		}
		else
		{
			iBackupDirection=pPlay->direction;			
			iBackupFX=pPlay->fx;			
		}
		///</DM>

		fxActive = true;
		checkedGoalCollision = false;
		realInitialPosition.set( realPosition );
		
		// Faz com que o efeito influencie o destino final da bola
		pPlay->direction.x += NanoMath::lerp( ( pPlay->fx - ISO_FX_FACTOR_MIN_VALUE ) / ( ISO_FX_FACTOR_MAX_VALUE - ISO_FX_FACTOR_MIN_VALUE ), ISO_FX_MAX_DETOUR, ISO_FX_MIN_DETOUR );
		
		// Acrescenta a componente Y à direção do chute
		Point3f y( 0.0f, 1.0f * pPlay->height, 0.0f );
		Point3f finalDir = ( pPlay->direction + y ).getNormalized() * pPlay->power;

		timeToGoal = static_cast< float >( fabs( realPosition.z / finalDir.z ) );
		realInitialYSpeed = finalDir.y;
		realSpeed = finalDir;

		setState( BALL_STATE_MOVING );
		accTime = 0.0f;
		accTimeY = 0.0f;

		Point3f destination( realInitialPosition.x + ( realSpeed.x * timeToGoal ),
							 0.0f,
							 realInitialPosition.z + ( realSpeed.z * timeToGoal ) );

		float time = 0.0f, totalTime = 0.0f;
		float initialY = realInitialPosition.y;
		float initialYSpeed = realInitialYSpeed;

		do
		{
			time = timeToGoal - totalTime;
			destination.y = initialY + ( initialYSpeed * time ) + ( GRAVITY_ACCELERATION * 0.5f * time * time );
			if( destination.y < 0.0f )
			{
				totalTime -= initialYSpeed / GRAVITY_ACCELERATION;
				initialYSpeed *= BALL_BOUNCE_FACTOR;

				// Teste para evitar excesso de loops quando a bola é chutada rasteira
				if( initialYSpeed < ( REAL_MIN_Y_SPEED * 10.0f ) )
				{
					destination.y = 0.0f;
					break;
				}
			}
		}
		while( ( time > 0.0f ) && ( destination.y < 0.0f ) );
		
		// Determina a função de movimentação da bola no eixo x
		Point3f origAux( realInitialPosition.x, 0.0f, realInitialPosition.z );
		Point3f destAux( destination.x, 0.0f, destination.z );
		Point3f dirAux( finalDir.x, 0.0f, finalDir.z );

		Point3f step = dirAux.getNormalized() * ( ( destAux - origAux ).getModule() / 3.0f );
		
		Point3f yVector( 0.0f, 1.0f, 0.0f );
		Point3f fxBezierVector = ( dirAux % yVector ).normalize() * pPlay->fx;

		Point3f xControl1( origAux + ( 1.0f * step ) + fxBezierVector );
		Point3f xControl2( origAux + ( 2.0f * step ) + fxBezierVector );

		movFunc.set( &origAux, &xControl1, &xControl2, &destAux );
		
		*pOut = destination;
		return pOut;
	}
	return NULL;
}

/*===============================================================================

MÉTODO update
	Atualiza o objeto
   
=============================================================================== */

bool Ball::update( float timeElapsed )
{
	if( !ObjectGroup::update( timeElapsed ) || ( ballState == BALL_STATE_STOPPED ) )
		return false;

	if( isReplay && !checkedGoalCollision )
	{
		if( accTime + timeElapsed > lastCollisionTime )		
			timeElapsed = lastCollisionTime - accTime;
	}

	accTime += timeElapsed;
	accTimeY += timeElapsed;

	// Garante que teste de colisão seja feito exatamente na linha do gol
	if(!checkedGoalCollision)
	{
		if( realPosition.z <= 0.0f )
		{
			lastCollisionTime = accTime;
			checkedGoalCollision = true;
		}
		else if( accTime >= timeToGoal )
		{
			accTime = timeToGoal;
			checkedGoalCollision = true;
		}
	}
	
	// Modifica a direção da bola de acordo com o efeito aplicado
	float currCurvePercent = accTime / timeToGoal;
	if( fxActive && ( ballState == BALL_STATE_MOVING ) && ( currCurvePercent <= 1.0f ) )
	{
		if( currCurvePercent == 1.0f )
			fxActive = false;
		
		Point3f lastPos, currPos;
		movFunc.getPointAt( &lastPos, currCurvePercent - 0.01f );
		movFunc.getPointAt( &currPos, currCurvePercent );
	
		if( currPos != lastPos )
			realSpeed = ( currPos - lastPos ).normalize() * realSpeed.getModule();
		
		realPosition.x = currPos.x;
		realPosition.z = currPos.z;
	}
	else
	{
		///<DM>
		///TODOO: trocar cálculo incremental por amostral
		
		// Movimenta a bola no eixo x
		realPosition.x += realSpeed.x * timeElapsed;
		
		// Movimenta a bola no eixo z
		realPosition.z += realSpeed.z * timeElapsed;
		
		/*
		Point3f lastPos, currPos;
		movFunc.getPointAt( &lastPos, 0.99f );
		movFunc.getPointAt( &currPos, 1.0f );		
		Point3f realSpeedAfterFx = Point3f( ( currPos - lastPos ).normalize() * realSpeed.getModule() );	
		realPosition= lastPos + ( realSpeed * ( accTime - timeToGoal ) );
		 */
		///</DM>
	}

	// Verifica se bola quicou no chão
	///<DM> preserva a trajetória da bola ao passar pela linha de fundo
	//	realSpeed.y = realInitialYSpeed + GRAVITY_ACCELERATION * accTimeY;
	///</DM>
	if(( realSpeed.y < 0.0f ) && ( realPosition.y <= 0.0f ) )
	{
		accTimeY = 0.0f;		
		realInitialPosition.y = 0.0f;
		realPosition.y = 0.0f;
		realInitialYSpeed = static_cast< float >( fabs( realInitialYSpeed ) * BALL_BOUNCE_FACTOR );
		realSpeed.y = static_cast< float >( fabs( realSpeed.y ) * BALL_BOUNCE_FACTOR );
	}
	
	// Movimenta a bola no eixo y
	// y = y0 + ( v0 * t ) + ( a / 2 * t * t )
	realPosition.y = realInitialPosition.y + ( realInitialYSpeed * accTimeY ) + ( GRAVITY_ACCELERATION * 0.5f * accTimeY * accTimeY );

	// Impede que a bola atravesse o chão
	if( realPosition.y < 0.0f )
		realPosition.y = 0.0f;

	
		
	switch( ballState )
	{
		case BALL_STATE_MOVING:
		
			if( realPosition.z <= 0.0f || accTime == timeToGoal )
			{
				// Bola chegou na linha de fundo
				setState( checkGoalCollision() );
				realLastGoalPosition = realPosition;
			}
			break;

		case BALL_STATE_GOAL:
		case BALL_STATE_POST_GOAL:
			reduceBallSpeed( timeElapsed );
			keepBallInsideGoal();
			break;

		case BALL_STATE_OUT:
		case BALL_STATE_POST_OUT:
			reduceBallSpeed( timeElapsed );
			handleBallOut();
			break;

		case BALL_STATE_POST_BACK:
		case BALL_STATE_REJECTED_KEEPER:
		case BALL_STATE_REJECTED_BARRIER:
			// A bola desacelera após ter colidido com alguma coisa ou ser chutada para fora
			reduceBallSpeed( timeElapsed );
			break;
	}

	// Converte as coordenadas reais da bola para uma posição da tela
	updatePosition();

   return true;
}

/*===============================================================================

MÉTODO checkGoalCollision

   Realiza os testes de colisão com as traves e retorna o novo estado da
   bola, que no fundo é o resultado da jogada (gol, fora, trave, etc).

=============================================================================== */

BallState Ball::checkGoalCollision( void )
{
	// caso seja replay e o instante de tempo atual seja inferior ao instante da
	// definição da última jogada, não colide

	///<DM>
	///a salvaguarda em relaçaõ a STATE_OUT é para evitar distorções.
	if( isReplay && lastPlayResult!=BALL_STATE_OUT)
	///</DM>
	{
		if( accTime < lastCollisionTime )
		{
			return ballState;
		}
		
		else
		{
			realInitialPosition = realPosition = realLastGoalPosition;
			fxActive = false;
			realSpeed = lastCollisionDirection;
			accTime = accTimeY = 0.0f;			
			realInitialYSpeed = realSpeed.y;
			return lastPlayResult;

		}
 
	}
	 

	// testa se a bola entrou no gol ou colidiu com alguma trave
    if( realPosition.z <= 2.0f && realPosition.y <= REAL_BAR_TOP && realPosition.x >= REAL_LEFT_POST_END && realPosition.x <= REAL_RIGHT_POST_END )
	{
        if( realPosition.y >= REAL_BAR_BOTTOM )
		{
			// bateu no travessão
			lastPlayResult = handlePostCollision( POST_BAR );
        }
		else
		{
			// bola abaixo do travessão. testa colisão com traves.

            if( realPosition.x <= REAL_LEFT_POST_START )
			{ 
				// bateu na trave esquerda
				lastPlayResult = handlePostCollision( POST_LEFT );
            }
			else if( realPosition.x >= REAL_RIGHT_POST_START )
			{ 
				// bateu na trave direita
				lastPlayResult = handlePostCollision( POST_RIGHT );
            }
			else
			{
				
				realInitialPosition.x = realPosition.x;
				realInitialPosition.z = realPosition.z;
										
				realSpeed.y = realInitialYSpeed + GRAVITY_ACCELERATION * ( accTimeY * 2.0f );

				lastCollisionDirection = realSpeed;

				lastCollisionTime = accTime;
				accTime = 0.0f; 
				lastPlayResult = BALL_STATE_GOAL;
				
				fxActive = false;
				checkedGoalCollision = true;
            }
        }
    }
	else
	{
		///<DM>
		///evita que a bola chape no chão após passar pela linha de fundo
		realInitialPosition.x = realPosition.x;
		realInitialPosition.z = realPosition.z;
//		realInitialPosition = realPosition;
		///</DM>
		realSpeed.y = realInitialYSpeed + GRAVITY_ACCELERATION * ( accTimeY * 2.0f );

		///<DM>
		///para quando a bola sai pela linha de fundo e não é replay. Evita sobrescrita do backup
		if (!isReplay)
		{
		lastCollisionDirection = realSpeed;
		lastCollisionTime = accTime;
		}
		///</DM>
		fxActive = false;
		

		accTime = 0.0f;

		lastPlayResult = BALL_STATE_OUT;
    }
	return lastPlayResult;
}

/*===============================================================================

MÉTODO checkPlayerCollision

=============================================================================== */

uint8 Ball::checkPlayerCollision( GenericPlayer *pPlayer, uint8 type )
{
	if( pPlayer && pPlayer->isVisible() && pPlayer->getBallCollisionTest() )
	{
		
		///<DM> Acabou sendo mais interessante seguir um fluxo mais próximo do real.
		
		// caso seja replay e o instante de tempo atual seja inferior ao instante da
		// definição da última jogada, não colide
		/*
		if (isReplay )
		{
			if( accTime < lastCollisionTime )
			{
				return ballState;
			}		
			
			else
			{				
				realSpeed = lastCollisionDirection;			 
				realInitialPosition = realPosition;
				realLastGoalPosition = realPosition;				
				accTime = 0.0f;			
				accTimeY = 0.0f;
				realInitialYSpeed = iAuxCollisionDirection.y;
				setState( lastPlayResult );					
				return lastPlayResult;				 		
			}
		 
		}
		 */
		///</DM>
		
		CollisionQuad playerArea = *( pPlayer->getCollisionArea() );
		float distanceToPlayer = playerArea.distanceTo( &realPosition );

		if( distanceToPlayer <= REAL_BALL_RADIUS )
		{
			// colidiu com o plano do quad do jogador
			pPlayer->setBallCollisionTest( false );

			Point3f collisionPosition;
			rewindToPlayerCollisionQuad( &collisionPosition, pPlayer, 0.0f, timeToGoal );

			if( playerArea.isInsideQuad( &collisionPosition, REAL_BALL_RADIUS ) )
			{
				// colidiu de fato com o jogador
				realPosition = collisionPosition;
				realInitialPosition = realPosition;
				realLastGoalPosition = realPosition;
//////////////////////////////////////////////////////////////////////				
				// cosTheta = ( a * b ) / ( modA * modB )
				// como modB == modNormal == 1:
				float cosTheta = realSpeed.getNormalized() * playerArea.normal;

				// ajuste: quando jogador bate falta pelo lado esquerdo da tela, a colisão com a
				// barreira fazia a bola passar pela mesma
				if( collisionPosition.x <= REAL_BIG_AREA_WIDTH * 0.5f )
					cosTheta = -cosTheta;

				realSpeed.z *= REAL_ELASTIC_FACTOR * ( 1.3f - fabsf( cosTheta ) );
				if( realSpeed.z < 0.0f )
					realSpeed.z *= -1.0f;
				
				uint16 rnd;
				if( type == COLLISION_BARRIER )
				{
					realSpeed.x *= REAL_ELASTIC_FACTOR * cosTheta;
				}
				else
				{
					// caso tenha colidido com goleiro, velocidade x é semi-aleatória
					// (há mais possibilidade da bola manter a direção atual)
					
					rnd = static_cast< uint16 >( Random::GetInt() );
					rnd %= 230;
					realSpeed.x *= fabsf( REAL_ELASTIC_FACTOR * cosTheta ) * ( rnd - 100 ) / 100.0f;
					
				}
//////////////////////////////////////////////////////////////////////////
///<DM>
				if (isReplay)
					realSpeed=iAuxCollisionDirection;
				else
					iAuxCollisionDirection=realSpeed;
///</DM>				
				// obs.: esse teste é necessário devido a um bug onde o teste de colisão com o 
				// goleiro era executado mais de uma vez, e com isso no replay o tempo até o gol era 0.0f
				if( accTime > 0.0f )
					lastCollisionTime = accTime;

				// direção vertical da bola é semi-aleatória após impacto
				accTime = 0.0f;
				accTimeY = 0.0f;


				rnd = static_cast< uint16 >( Random::GetInt() );
				realSpeed.y = AFTER_COLLISION_MIN_Y + ( rnd % AFTER_COLLISION_MAX_Y );
///<DM>				
				if (isReplay)
					realSpeed=lastCollisionDirection;
				else
					lastCollisionDirection = realSpeed;
///</DM>				
				realInitialYSpeed = realSpeed.y;		
				if( type == COLLISION_BARRIER )
					lastPlayResult = BALL_STATE_REJECTED_BARRIER;
				else
					lastPlayResult = BALL_STATE_REJECTED_KEEPER;

				setState( lastPlayResult );
				
				fxActive = false;
				checkedGoalCollision = true;
			}
		}
	}

	// Se não for passado um jogador para testar colisão com a bola, apenas retorna seu estado atual
	return ballState;
}

/*===============================================================================

MÉTODO getRealPosAtTime
	Obtém a posição da bola no momento indicado.

================================================================================*/

#warning A atual implementação deste método permite que ele seja utilizado apenas enquanto a bola ainda não colidiu e ainda está no estado BALL_STATE_MOVING!!!!
#warning O ideal seria implementá-lo corretamente e utilizá-lo no método update e em todos os outros métodos de checagem de colisão com a bola

// TODOO
// Point3f* Ball::getRealPosAndSpeedAtTime( Point3f* pOutPos, Point3f* pOutSpeed, float time )

Point3f* Ball::getRealPosAtTime( Point3f* pOutPos, float time )
{
	if( time > timeToGoal )
	{
		#if DEBUG
			LOG( "A atual implementação deste método permite que ele seja utilizado apenas enquanto a bola ainda não colidiu e ainda está no estado BALL_STATE_MOVING!!!! O ideal seria implementá-lo corretamente e utilizá-lo no método update e em todos os outros métodos de checagem de colisão com a bola\n" );
		#endif

		time = timeToGoal;	
	}

	// Verifica as coordenadas x e z da bola de acordo com a função de efeito
	Point3f pos;
	movFunc.getPointAt( &pos, time / timeToGoal );

	// Verifica a coordenada y da bola de acordo com a equação de movimento
	float timeCounter = 0.0f, totalTime = 0.0f;
	float initialY = realInitialPosition.y;
	float initialYSpeed = realInitialYSpeed;

	do
	{
		timeCounter = time - totalTime;
		pos.y = initialY + ( initialYSpeed * timeCounter ) + ( GRAVITY_ACCELERATION * 0.5f * timeCounter * timeCounter );
		if( pos.y < 0.0f )
		{
			totalTime -= initialYSpeed / GRAVITY_ACCELERATION;
			initialYSpeed *= BALL_BOUNCE_FACTOR;

			// Teste para evitar excesso de loops quando a bola é chutada rasteira
			if( initialYSpeed < ( REAL_MIN_Y_SPEED * 10.0f ) )
			{
				pos.y = 0.0f;
				break;
			}
		}
	}
	while( ( timeCounter > 0.0f ) && ( pos.y < 0.0f ) );

	// Impede que a bola atravesse o chão
	if( pos.y < 0.0f )
		pos.y = 0.0f;
	
	*pOutPos = pos;
	return pOutPos;
}

/*===============================================================================

MÉTODO rewindToPlayerCollisionQuad
	Obtém a posição da bola no momento em que ela colidiu com o jogador.

================================================================================*/

Point3f* Ball::rewindToPlayerCollisionQuad( Point3f* pCollisionPos, const GenericPlayer *pPlayer, float minTime, float maxTime )
{
	float testTime = ( maxTime + minTime ) * 0.5f;
	
	#if DEBUG
		static uint32 count = 0;
		LOG( "Rewind %d: min = %.3f, max = %.3f, test = %.3f\n", count++, minTime, maxTime, testTime );
	#endif

	float currDistToPlayer = pPlayer->getCollisionArea()->distanceTo( getRealPosAtTime( pCollisionPos, testTime ) );
	
	if( ( static_cast< float >( fabs( currDistToPlayer ) ) <= REAL_BALL_RADIUS ) || NanoMath::feql( static_cast< float >( fabs( minTime - maxTime ) ), 0.0f ) )
	{
		return pCollisionPos;
	}
	else
	{
		if( currDistToPlayer > 0.0f )
		{
			return rewindToPlayerCollisionQuad( pCollisionPos, pPlayer, testTime, maxTime );
		}
		else
		{
			return rewindToPlayerCollisionQuad( pCollisionPos, pPlayer, minTime, testTime );
		}
	}
}

/*===============================================================================

MÉTODO handlePostCollision
	Trata a colisão com as traves.

================================================================================*/

BallState Ball::handlePostCollision( uint8 postIndex )
{
	// Armazena a velocidade antes da colisão
	Point3f beforeCollisionSpeed = realSpeed;

	// Define a posição global da trave com a qual a bola colidiu
	Point3f postPosition;
	switch( postIndex )
	{
		case POST_LEFT:
			postPosition.set( REAL_LEFT_POST_MIDDLE, realPosition.y );
			break;

		case POST_RIGHT:
			postPosition.set( REAL_RIGHT_POST_MIDDLE, realPosition.y );
			break;

		case POST_BAR:
			postPosition.set( realPosition.x, REAL_BAR_MIDDLE );
			break;
	}

	// Obtém a velocidade normalizada
	Point3f normalSpeed = realSpeed.getNormalized();

	// 1. POSICIONA BOLA NO PONTO DE CONTATO
	// ADAPTADO DE: http://www.gamasutra.com/features/20020118/vandenhuevel_02.htm

	// Obtém a distância inicial do centro da bola ao centro da trave
	Point3f distance = postPosition - realPosition;
	float distanceModule = distance.getModule();
	Point3f normalDistance = distance.getNormalized();
	
	// Cálculo da projeção de um vetor sobre outro: proj( B,A ) = ( (A.B) / |A|ˆ2 ) * A
	Point3f projectionV_D = realSpeed * ( ( realSpeed * distance ) / ( realSpeed.getModule() * realSpeed.getModule() ) );
	float projectionModule = projectionV_D.getModule();

	// Não há necessidade de se extrair a raiz de F, pois seu valor só é utilizado ao quadrado:
	// distanceModuleˆ2 = projectionModuleˆ2 + Fˆ2
	// Logo:
	float F_squared = ( distanceModule * distanceModule ) - ( projectionModule * projectionModule );

	// O módulo da hipotenusa do triângulo retângulo final, formado entre os pontos central da bola e da trave
	// e o ponto ortogonal ao centro da trave no vetor velocidade, é a soma dos raios da trave e da bola. Seu
	// valor só é utilizado ao quadrado, logo o quadrado é calculado de uma vez
	float sumRadiiSquared = ( REAL_BALL_RADIUS + ( REAL_POST_WIDTH * 0.5f ) ) * ( REAL_BALL_RADIUS + ( REAL_POST_WIDTH * 0.5f ) );
	
	// Encontra o módulo da distância que a bola deve retroceder no vetor da velocidade, até chegar ao
	// ponto exato de contato
	float T = sumRadiiSquared - F_squared;
	float backDistance = projectionModule - static_cast< float >( sqrt( T ) );

	// Finalmente, posiciona a bola no ponto correto de contato
	realPosition += normalSpeed * backDistance;

	// 2. CALCULA DIREÇÃO E VELOCIDADE DA BOLA APÓS A COLISÃO

	// Recalcula a projeção do vetor velocidade sobre o vetor distância entre os centros da bola
	// e da trave. Aqui os vetores são normalizados, pois o importante é o ângulo formado entre
	// eles. O quadrado da projeção é usado para descobrir o ângulo formado entre esses dois vetores:
	// 0.0f:	ortogonal (90 graus)
	// 0.5f:	diagonal  (45 graus)
	// 1.0f: paralelo  ( 0 graus)
	normalDistance = distance.getNormalized();
	projectionV_D = normalSpeed * ( ( normalSpeed * normalDistance ) / ( normalSpeed.getModule() * normalSpeed.getModule() ) );	
	projectionModule = projectionV_D.getModule();
	projectionModule *= projectionModule;

	realSpeed.y = GRAVITY_ACCELERATION * accTimeY; // v = v0 + at;

	BallState returnValue = BALL_STATE_POST_GOAL;
	float realSpeedModule = realSpeed.getModule();

	// Reajusta a quantidade de movimento após a colisão. O cálculo é feito com base no fato de que,
	// quanto mais frontal à trave for o chute, maior a perda de energia. Ou seja, bolas que apenas
	// "raspam" a trave perdem pouca energia
	realSpeedModule -= projectionModule * ( 1.0f - static_cast< float >( fabs( REAL_ELASTIC_FACTOR ) ) ) * realSpeedModule;
	realSpeed = realSpeed.getNormalized() * realSpeedModule;
	
	// O cálculo da variação em z é igual para todas as traves. O que muda é o cálculo no outro eixo
	// (x no caso das traves e y no caso do travessão)
	realSpeed.z += ( 2.0f * projectionModule ) * realSpeedModule;

	Point3f oldSpeed = realSpeed;
	switch( postIndex )
	{
		case POST_LEFT:
			if( realPosition.x > postPosition.x )
				realSpeed.x = static_cast< float >( fabs( realSpeed.x + ( 1.0f - ( 2.0f * fabs( 0.5f - projectionModule ) ) ) * realSpeedModule ) );
			else
				realSpeed.x = -static_cast< float >( fabs( realSpeed.x - ( 1.0f - ( 2.0f * fabs( 0.5f - projectionModule ) ) ) * realSpeedModule ) );

			if( realSpeed.z > 0.0f )
			{
				returnValue = BALL_STATE_POST_BACK;
			}
			else
			{
				// Velocidade z menor ou igual a zero. A bola foi para fora ou para o gol
				if( realPosition.x > postPosition.x )
					returnValue = BALL_STATE_POST_GOAL;
				else
					returnValue = BALL_STATE_POST_OUT;
			}
			break;

		case POST_RIGHT:
			if( realPosition.x < postPosition.x )
				realSpeed.x = -static_cast< float >( fabs( realSpeed.x - ( 1.0f - ( 2.0f * fabs( 0.5f - projectionModule ) ) ) * realSpeedModule ) );
			else
				realSpeed.x = static_cast< float >( fabs( realSpeed.x + ( 1.0f - ( 2.0f * fabs( 0.5f - projectionModule ) ) ) * realSpeedModule ) );

			if( realSpeed.z > 0.0f )
			{
				returnValue = BALL_STATE_POST_BACK;
			}
			else
			{
				// velocidade z menor ou igual a zero. A bola foi para fora ou para o gol
				if( realPosition.x < postPosition.x )
					returnValue = BALL_STATE_POST_GOAL;
				else
					returnValue = BALL_STATE_POST_OUT;
			}
			break;

		case POST_BAR:
		default:
			if( realPosition.y < postPosition.y )
				realSpeed.y = -static_cast< float >( fabs( realSpeed.y - ( 1.0f - ( 2.0f * fabs( 0.5f - projectionModule ) ) ) * realSpeedModule * 0.7f ) );
			else
				realSpeed.y = static_cast< float >( fabs( realSpeed.y + ( 1.0f - ( 2.0f * fabs( 0.5f - projectionModule ) ) ) * realSpeedModule * 0.7f ) );

			if( realSpeed.z > 0.0f )
			{
				returnValue = BALL_STATE_POST_BACK;
			}
			else
			{
				// Velocidade z menor ou igual a zero; bola foi para fora ou para o gol
				if( realPosition.y < postPosition.y )
					returnValue = BALL_STATE_POST_GOAL;
				else
					returnValue = BALL_STATE_POST_OUT;
			}		
			break;
	}

	// Evita que a bola ganhe velocidade apóss a colisão
	float beforeCollisionModule = beforeCollisionSpeed.getModule();
	if( realSpeed.getModule() > beforeCollisionModule )
	{
		realSpeed.normalize();
		realSpeed *= beforeCollisionModule;

		// Pega o maior "culpado" pelo módulo da velocidade e o reduz
		float *pHighest;
		if( fabs( realSpeed.x ) > fabs( realSpeed.z ) )
			pHighest = &( realSpeed.x );
		else
			pHighest = &( realSpeed.z );

		if( fabs( realSpeed.y ) > fabs( *pHighest ) )
			pHighest = &( realSpeed.y );

		*pHighest *= static_cast< float >( fabs( REAL_ELASTIC_FACTOR ) );
	}

	fxActive = false;
	checkedGoalCollision = true;

	lastCollisionDirection = realSpeed;
	lastCollisionTime = accTime;
	accTime = 0.0f;
	accTimeY = 0.0f;

	realInitialYSpeed = realSpeed.y;

	// Posição inicial para cálculo da posição da bola passa a ser o ponto de colisão com a trave
	realInitialPosition = realPosition;

	return returnValue;
}

/*===============================================================================

MÉTODO reduceBallSpeed
	Desacelera a bola.

=============================================================================== */

void Ball::reduceBallSpeed( float timeElapsed )
{
	realInitialPosition.x = realPosition.x;
	realInitialPosition.z = realPosition.z;	 
	accTime = 0.0f;
	realSpeed -= realSpeed * BALL_SLOW_DOWN_FACTOR * timeElapsed;
}

/*===============================================================================

MÉTODO setCollisionWithQuad
	Determina que a bola colidiu com o quad passado como parâmetro.

=============================================================================== */

void Ball::setCollisionWithQuad( const CollisionQuad* pQuad )
{
	// Muda a direção da bola
	///<DM>	 replay para quando bate na torcida
	if (isReplay )
	{
		realSpeed = iAuxCollisionDirection;			 
		
		realInitialYSpeed = realSpeed.y;
		realInitialPosition = realPosition;	
		
		if( accTime > 0.0f )				lastCollisionTime = accTime;		
		
		accTime = 0.0f;
		accTimeY = 0.0f;		
		fxActive = false;
		checkedGoalCollision = true;
		
		return;
	}
	///</DM>
/////////////////////////////////////////////	
	// cosTheta = ( a * b ) / ( modA * modB )
	// como modB == modNormal == 1:
	realSpeed.y = 0.0f;
	float cosTheta = realSpeed.getNormalized() * pQuad->normal;

	if( realPosition.x <= REAL_BIG_AREA_WIDTH * 0.5f )
		cosTheta = -cosTheta;

	// OBS: Coloca uma velocidade muito pequena para impedir que a bola volte muito e acabe "furando" outro objeto que deveria sofrer colisão
	realSpeed.z *= ( REAL_ELASTIC_FACTOR * ( 1.3f - fabsf( cosTheta ) ) ) * 0.20f;
	if( realSpeed.z < 0.0f )
		realSpeed.z *= -1.0f;

	realSpeed.x *= REAL_ELASTIC_FACTOR * cosTheta;

	uint16 rnd = static_cast< uint16 >( Random::GetInt() );
	realSpeed.y = AFTER_COLLISION_MIN_Y + ( rnd % AFTER_COLLISION_MAX_Y );
/////////////////////////////////////////////////
	///<DM>
	//	lastCollisionDirection = realSpeed;
	iAuxCollisionDirection = realSpeed;
	///</DM>
	
	realInitialYSpeed = realSpeed.y;
	realInitialPosition = realPosition;
	
	if( accTime > 0.0f )				lastCollisionTime = accTime;

	
	accTime = 0.0f;
	accTimeY = 0.0f;	
	fxActive = false;
	checkedGoalCollision = true;
}

