#include "PlayerBarrier.h"

#include "Ball_Defines.h"
#include "GameUtils.h"
#include "Iso_Defines.h"

#include "Exceptions.h"
#include "Macros.h"

// Número de frames da imagem do jogador parado
#define N_PLAYER_FRAMES 5

/*===============================================================================

CONSTRUTOR

=============================================================================== */

PlayerBarrier::PlayerBarrier( void ) // TODOO : Barrier( BARRIER_MAX_N_PLAYERS + 2 )
{
	build();
}

/*===============================================================================

DESTRUTOR

=============================================================================== */

//PlayerBarrier::~PlayerBarrier( void )
//{
//}

/*===============================================================================

MÉTODO build
	Inicializa o objeto.

=============================================================================== */

bool PlayerBarrier::build( void )
{
	// Cria os jogadores que podem ser utilizados na barreira
//	Sprite *pPlayer;
//	for( uint8 i = 0 ; i < BARRIER_MAX_N_PLAYERS ; ++i )
//	{
//		pPlayer = new Sprite();
//		if( !pPlayer || !insertObject( pPlayer ) )
//		{
//			SAFE_DELETE( pPlayer );
//			goto Error;
//		}
//		pPlayer->setVisible( false );	
//	}
//	
//	pBarrierPlayers = ( Sprite* )getObject( 0 );

	return true;

	// Label de tratamento de erros
	Error:
		return false;
}

/*===============================================================================

MÉTODO prepare
	Posiciona a barreira em relação à posição da bola. O número de jogadores na
barreira é definido de acordo com a distância da falta ao gol.

================================================================================*/

void PlayerBarrier::prepare( const Point3f* pBallPosition )
{
	// Obtém a distância da bola ao centro do gol
	float distance = pBallPosition->getModule();
	if( distance < DISTANCE_MAX_PLAYERS )
		distance = DISTANCE_MAX_PLAYERS;
	else if( distance > DISTANCE_MIN_PLAYERS )
		distance = DISTANCE_MIN_PLAYERS;

	numberOfPlayers = ( uint8 ) ( BARRIER_MAX_N_PLAYERS - 
			( ( distance - DISTANCE_MAX_PLAYERS ) / ( DISTANCE_MIN_PLAYERS - DISTANCE_MAX_PLAYERS ) )
			* ( BARRIER_MAX_N_PLAYERS - BARRIER_MIN_N_PLAYERS ) );

	Point3f currentPosition;
	Point3f postPosition;
	Point3f p;

	if( pBallPosition->x < 0.0f )
	{
		// bola está mais próxima da trave esquerda
		postPosition.set( -REAL_GOAL_WIDTH / 2.0 );
	}
	else
	{
		// bola está mais próxima da trave direita
		postPosition.set( REAL_GOAL_WIDTH / 2.0 );
	}

	// vetor no eixo y
	Point3f upVector( 0.0f, 1.0f, 0.0f );
	Point3f postToBallVector = postPosition - *pBallPosition;

	// vetor ortogonal à linha da bola (normalizado)
	Point3f ortho = ( upVector % postToBallVector ).getNormalized();

	// direção de inserção dos jogadores
	Point3f insertDirection = ortho * BARRIER_PLAYERS_DISTANCE;

	// define a posição do jogador-base da barreira (do jogador mais à esquerda)
	Point3f basePosition = *pBallPosition + 
					   ( postToBallVector.getNormalized() * BARRIER_DISTANCE_TO_BALL ) - 
					   // desloca a barreira de forma a proteger um pouco mais a trave
					   // mais próxima da bola
					   insertDirection * numberOfPlayers * 0.1f;
	
	if( pBallPosition->x < 0.0f )
	{
		// define o quad de colisão
		collisionArea.set( ( basePosition + insertDirection * numberOfPlayers * 0.1f ),
						   upVector, ortho * -1.0f, ORIENT_BOTTOM_LEFT,
						   numberOfPlayers * BARRIER_PLAYERS_DISTANCE, REAL_BARRIER_HEIGHT );

		insertDirection *= -1.0f;
	}
	else
	{
		// define o quad de colisão
		collisionArea.set( basePosition + insertDirection * numberOfPlayers * 0.9f, 
						   basePosition + insertDirection * numberOfPlayers * 0.9f + ( upVector * REAL_BARRIER_HEIGHT ), 
						   basePosition );
	}

	// define a posição geral da barreira como a posição do seu jogador central, para que
	// após o lookAt() todos estejam virados para a bola.
	realPosition = basePosition;
	float higherZ = realPosition.z; // armazena o maior z da barreira, para que o cálculo da troca de ordem z com a bola seja correto

	Point3f collectionPos = GameUtils::isoGlobalToPixels( realPosition );
	setPosition( collectionPos.x, collectionPos.y );
	uint8 i;
	float* zOrder = new float[ numberOfPlayers ];
	if( zOrder == NULL )
		return;

	Sprite *pPlayer;
	for( i = 0; i < numberOfPlayers; ++i )
	{
		pPlayer = ( Sprite* )getObject( i );

		if( pPlayer )
		{
			currentPosition = basePosition + insertDirection * i;
			zOrder[ i ] = currentPosition.z;

			if( currentPosition.z > higherZ )
				higherZ = currentPosition.z;

			if( i == 0 )
			{
				zOrder[ 0 ] = currentPosition.z;
			}
			else
			{
				for( int j = 0; j < i; ++j )
				{
					if( currentPosition.z < zOrder[ j ] )
					{
#warning Implementar isso de algum modo...........
						//setDrawableZOrder( j, i );

						float zTemp = zOrder[ j ];
						zOrder[ j ] = zOrder[ i ];
						zOrder[ i ] = zTemp;
					}
				}
			}

			p = GameUtils::isoGlobalToPixels( currentPosition );
			pPlayer->setPosition( -collectionPos.x + p.x + ISO_PLAYER_STOPPED_X_OFFSET, -collectionPos.y + p.y + ISO_PLAYER_STOPPED_Y_OFFSET );
			pPlayer->setVisible( true );
		}
	}

	realPosition.z = higherZ;

	DESTROY_VEC( zOrder );

	// Marca os jogadores que não estarão na barreira como invisíveis
	for( ; i < BARRIER_MAX_N_PLAYERS; ++i )
	{
		pPlayer = static_cast< Sprite* >( getObject( i ) );
		
		if( pPlayer )
			pPlayer->setVisible( false );
	}

	// Força a troca de frame para evitar que alguns jogadores que não estavam presentes na última
	// falta virem para o lado errado (isso ocorria quando oldDirection era igual a direction em
	// GenericIsoPlayer::lookAt(), e o número de jogadores aumentava)
	lookAt( pBallPosition, true );

	setBallCollisionTest( true );
	setVisible( true );
}

/*===============================================================================

MÉTODO updateFrame
	Atualiza o frame atual dos jogadores, de acordo com a direção para a qual
estão virados.

================================================================================*/

void PlayerBarrier::updateFrame( void )
{
	uint8 frameIndex;
	MirrorOp mirror;

	switch( direction )
	{
		case DIRECTION_UP:
		case DIRECTION_UP_RIGHT:
		case DIRECTION_RIGHT:
			frameIndex = ( uint8 )direction - ( ( direction - 4 ) << 1 );
			mirror = MIRROR_NONE;
			break;

		case DIRECTION_DOWN_RIGHT:
		case DIRECTION_UP_LEFT:
			frameIndex = direction;
			mirror = MIRROR_NONE;
			break;

		case DIRECTION_DOWN:
		case DIRECTION_DOWN_LEFT:
		case DIRECTION_LEFT:
			frameIndex = direction;
			mirror = MIRROR_HOR;
			break;
	}

	for( uint8 i = 0; i < numberOfPlayers; ++i )
		static_cast< Sprite* >( getObject( i ) )->setCurrentAnimSequenceStep( frameIndex );

	static_cast< Sprite* >( getObject( 0 ) )->mirror( mirror );
}

