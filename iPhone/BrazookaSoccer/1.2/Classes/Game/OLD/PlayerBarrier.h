/*
 *  PlayerBarrier.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/5/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PLAYER_BARRIER_H
#define PLAYER_BARRIER_H

#include "Barrier.h"

#include "Sprite.h"

#define N_BARRIER_PLAYERS 4

class PlayerBarrier : public Barrier
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException e NanoBaseException
		PlayerBarrier( void );
	
		// Destrutor
		virtual ~PlayerBarrier( void ){};

		// Posiciona a barreira em relação à posição da bola
		virtual void prepare( const Point3f* pBallPosition );
	
		// Atualiza o frame atual dos jogadores, de acordo com a direção para a qual estão virados
		virtual void updateFrame( void );

	protected:
		// Jogadores que podem ser utilizados na barreira da barreira
		Texture2DHandler barrierPlayersTexs[ N_BARRIER_PLAYERS ];
	
	private:
		// Inicializa o objeto
		bool build( void );
};

#endif
