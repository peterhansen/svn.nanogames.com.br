#ifndef ISO_DEFINES_H
#define ISO_DEFINES_H 1

// Valor próximo do máximo de um int16, usado para evitar erros no redesenho do pattern
#define MAX_SIZE 16000

// Define quantos persongens diferentes possuímos para formar a barreira
#define BARRIER_N_DIFFERENT_CHARACTERS 5

// Número máximo e mínimo de jogadores na barreira
#define BARRIER_MAX_N_PLAYERS 4
#define BARRIER_MIN_N_PLAYERS 1

// Distâncias que correspondem ao mínimo e máximo de jogadores presentes na barreira
#define BARRIER_MIN_DISTANCE 15.0f
#define BARRIER_MAX_DISTANCE 35.0f

// Distância oficial da barreira em metros
#define BARRIER_DISTANCE_TO_BALL 7.8f //old: 9.15

// Altura em pixels da trave
#define ISO_FLOOR_TO_BAR 182.0f

// Número de pixels em x e y percorridos para cada 1 metro real percorrido no eixo x
#define ISO_XREAL_X_OFFSET 50.0000f
#define ISO_XREAL_Y_OFFSET 27.813789063083003f // OLD 27.0492f 

// Constante para ajustar as dimensões do campo. Apesar de fugir do tamanho real de um campo, torna melhor a visualização do jogo
#define ISO_ADJUSTMENT_CONST 0.8f

// Número de pixels em x e y percorridos para cada 1 metro real percorrido no eixo z
#define ISO_ZREAL_X_OFFSET ( -76.6441f * ISO_ADJUSTMENT_CONST )
#define ISO_ZREAL_Y_OFFSET ( 33.576984846698118f * ISO_ADJUSTMENT_CONST ) // OLD ( 34.9005f * ISO_ADJUSTMENT_CONST )

// Offset na posição de desenho do círculo central
#define ISO_CENTER_CIRCLE_OFFSET_X ( -29 )
#define ISO_CENTER_CIRCLE_OFFSET_Y ( -23 )

// Offset na posição da bola para que seja desenhada no centro (ou seja, metade das dimensões de cada frame)
#define ISO_BALL_X_OFFSET  1.0f
#define ISO_BALL_Y_OFFSET -4.0f

// posição z das placas de publicidade que ficam atrás do gol
#define ISO_BACK_BOARDS_Z			-7.0

// altura real das placas de publicidade
#define ISO_BACK_BOARDS_REAL_HEIGHT 1.2

// offset na posição de desenho das placas de publicidade
#define ISO_BACK_BOARDS_OFFSET_X	7
#define ISO_BACK_BOARDS_OFFSET_Y	-12

// offset na posição da marca do pÍnalti para que ela seja desenhada centralizada
#define ISO_PENALTY_MARK_X_OFFSET	-2
#define ISO_PENALTY_MARK_Y_OFFSET	-1

// offset na posição da meia-lua para que ela seja desenhada centralizada
#define ISO_MEIA_LUA_X_OFFSET		-39
#define ISO_MEIA_LUA_Y_OFFSET		-18

// Offset na posição de desenho das bandeiras de escanteio
#define ISO_CORNER_LEFT_FLAG_OFFSET_X	 0.0f
#define ISO_CORNER_LEFT_FLAG_OFFSET_Y	 0.0f
#define ISO_CORNER_RIGHT_FLAG_OFFSET_X  -5.0f
#define ISO_CORNER_RIGHT_FLAG_OFFSET_Y	-1.0f

// offsets no desenho da mira, no modo desafio de faltas
#define CROSSHAIR_OFFSET_X 1
#define CROSSHAIR_OFFSET_Y -3

// offsets do jogador no modo isométrico
#define ISO_PLAYER_STOPPED_X_OFFSET  ( -4 )
#define ISO_PLAYER_STOPPED_Y_OFFSET  ( -20 )
#define ISO_PLAYER_SHOOTING_X_OFFSET ( -8 )
#define ISO_PLAYER_SHOOTING_Y_OFFSET ( -19 )
#define ISO_PLAYER_RUNNING_X_OFFSET  ( -5 )
#define ISO_PLAYER_RUNNING_Y_OFFSET  ( -20 )

// Garante que a mira sempre ficará entre o goleiro e as partes do gol
#define ISO_BALL_TARGET_REAL_Z	0.01f
#define ISO_KEEPER_REAL_Z		0.02f

#endif
