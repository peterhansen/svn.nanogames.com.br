#include "GenericPlayer.h"

#if DEBUG
	#include "Macros.h"
#endif

/*===============================================================================

CONSTRUTOR

=============================================================================== */

GenericPlayer::GenericPlayer( uint16 maxObjects )
			  : RealPosOwner(),
				ObjectGroup( maxObjects ),
				collisionArea(),
				checkBallCollision( false )
{
}

/*===============================================================================

DESTRUTOR

=============================================================================== */

GenericPlayer::~GenericPlayer( void )
{
}
