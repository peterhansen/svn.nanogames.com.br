/*
 *  GenericIsoPlayer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GENERIC_ISO_PLAYER_H
#define GENERIC_ISO_PLAYER_H 1

#include "GenericPlayer.h"
#include "IsoDirection.h"

class GenericIsoPlayer : public GenericPlayer
{
	public:
		// Destrutor
		virtual ~GenericIsoPlayer( void );

		// Reinicializa o objeto
		virtual void reset( const Point3f* pPosition = NULL ) = 0;

		// Faz o jogador virar de frente para a posição "pPosition"
		virtual void lookAt( const Point3f* pPosition = NULL, bool forceFrameUpdate = false );

	protected:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException
		explicit GenericIsoPlayer( uint16 maxObjects );

		// Atualiza o frame do jogador de acordo com a direção para a qual está virado
		virtual void updateFrame( void ) = 0;

		// Direção para a qual o jogador está virado
		IsoDirection direction;
};

#endif

