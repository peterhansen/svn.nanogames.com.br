/*
 *  CornerFlag.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CORNER_FLAG_H
#define CORNER_FLAG_H 1

#include "Sprite.h"

#include "RealPosOwner.h"

class CornerFlag : public RealPosOwner, public Sprite
{
	public:
		// Construtor
		CornerFlag( float offsetX, float offsetY );
		CornerFlag( float offsetX, float offsetY, const CornerFlag* pBase );

		// Destrutor
		virtual ~CornerFlag( void );

		static float GetCornerFlagAnchorX( void );
		static float GetCornerFlagAnchorY( void );
	
	private:
		// Atualiza a posição do objeto na tela
		virtual void updatePosition( void );
	
		// Offset de posicionamento
		float offsetX, offsetY;
};

#endif
