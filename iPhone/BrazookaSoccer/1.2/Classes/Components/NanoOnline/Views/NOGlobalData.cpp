#include "NOGlobalData.h"

// NanoOnline
#include "NOConf.h"
#include "NOCustomer.h"
#include "NORanking.h"

/*==============================================================================================

MÉTODO ESTÁTICO GetActiveProfile
	Retorna uma referência para o perfil atualmente logado no device.

===============================================================================================*/

NOCustomer& NOGlobalData::GetActiveProfile( void )
{
	// Perfil atualmente logado no device
	// Essa variável vai ser criada e inicializada com o construtor padrão na 1a
	// vez que o método NOGlobalData::GetActiveProfile for chamado. Será destruída
	// apenas quando a aplicação terminar. Para maiores informações, ver a política
	// de destruição de variáveis estáticas de C++ e a documentação de "atexit()"
	static NOCustomer activeProfile;
	return activeProfile;
}

/*==============================================================================================

MÉTODO ESTÁTICO GetRanking
	Retorna uma referência para o ranking contendo as melhores pontuações ainda não submetidas.

===============================================================================================*/

NORanking& NOGlobalData::GetRanking( void )
{
	// Ranking contendo as melhores pontuações ainda não submetidas
	// e os dados do ranking online
	// Essa variável vai ser criada e inicializada com o construtor padrão na 1a
	// vez que o método NOGlobalData::GetRanking for chamado. Será destruída
	// apenas quando a aplicação terminar. Para maiores informações, ver a política
	// de destruição de variáveis estáticas de C++ e a documentação de "atexit()"
	static NORanking ranking;
	return ranking;
}

/*==============================================================================================

MÉTODO ESTÁTICO GetConfig
	Retorna a configuração do NanoOnline.

===============================================================================================*/

NOConf& NOGlobalData::GetConfig( void )
{
	// Configuração do NanoOnline
	// Essa variável vai ser criada e inicializada com o construtor padrão na 1a
	// vez que o método NOGlobalData::GetConfig for chamado. Será destruída
	// apenas quando a aplicação terminar. Para maiores informações, ver a política
	// de destruição de variáveis estáticas de C++ e a documentação de "atexit()"
	static NOConf configuration;
	return configuration;
}
