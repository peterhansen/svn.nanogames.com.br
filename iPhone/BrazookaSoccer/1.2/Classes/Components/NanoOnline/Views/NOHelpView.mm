#include "NOHelpView.h"

// Extensão da classe para declarar métodos privados
@interface NOHelpView ( Private )

// Inicializa a view
-( bool )buildNOHelpView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOHelpView;

@end

// Início da implementação da classe
@implementation NOHelpView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOHelpView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOHelpView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOHelpView
	Inicializa a view.

==============================================================================================*/

- ( bool )buildNOHelpView
{
	// Determina o título da tela
	[self setScreenTitle: @"Help"];
	return true;

//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view
//		// ...
//
//		return true;
//	
//	} // Evita erros de compilação por causa dos gotos
//	
//	// Label de tratamento de erros
//	Error:
//		[self cleanNOHelpView];
//		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	[hTxtHelp setText: @"Hi, welcome to Nano Online. This is a free service that allows you to create a unique profile across all our games, storing your high scores and interacting with players all over the world!\n\nYou can check the high scores directly from your phone or on our web site.\n\nIf this is your first access to the system, the first thing you should do is create a profile. You must enter simple personal data such as your nickname, genre, birthday, first name and last name. Your e-mail is required to send you a new password in case you lose your old one.\n\nIf you've already created a profile on our website or in one of our games, you can just select the \"download profile\" option and enter your nickname and password.\n\nSUBMITTING SCORES\n\nEach profile's high score is stored on the phone. When you access the \"Submit Records\" list you have the option to submit these scores. From this point on, everybody can check your score from the phone or on our web site!\n\nCOSTS\n\nNano Games doesn't charge you anything for using the service. Depending on your carrier and connection mode (Wi-Fi or carrier network), you may be charged by the data traffic. Contact your carrier to know more about the costs applied.\n\nUsually, the charge depends on the amount of data transferred, not by the time taken on the connection.\n\nTo bring you a quicker and cheaper service, all actions, such as creating a profile, sending and updating high scores exchange small amounts of data.\n\nCreating a profile, for example, transfer about 250 bytes, which means it's necessary to create 4 profiles to transfer 1 kilobyte, or 4,000 profiles to reach 1 megabyte! Sending your score to the online ranking usually don't transfer more than 200 bytes.\n\nFor instance: if your carrier charges you US$ 10.00 per megabyte, it means you'll pay US$ 0.01 (1 cent) each 5 submissions! It's not so expensive, right? If you have any more doubts, access www.nanogames.com.br to know more."];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self cleanNOHelpView];
//  [super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNOHelpView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOHelpView
//{
//}

// Fim da implementação da classe
@end
