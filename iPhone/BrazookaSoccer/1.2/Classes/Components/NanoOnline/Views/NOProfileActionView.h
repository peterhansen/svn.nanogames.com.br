/*
 *  NOProfileActionView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 10/1/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_PROFILE_ACTION_VIEW_H
#define NO_PROFILE_ACTION_VIEW_H

// Componentes
#include "NOBaseView.h"

@interface NOProfileActionView : NOBaseView
{
	@private
		// Elementos de interface contidos na view	
		IBOutlet UIButton *hBtSelect;
		IBOutlet UIButton *hBtDownload;
		IBOutlet UIButton *hBtCreate;
}

// Método chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

@end

#endif
