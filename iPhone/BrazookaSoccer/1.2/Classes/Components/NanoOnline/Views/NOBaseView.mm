#include "NOBaseView.h"

// Extensão da classe para declarar métodos privados
@interface NOBaseView ( Private )

// Inicializa o objeto
-( bool )buildNOBaseView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOBaseView;

// OLD: Agora NOControllerView é um singleton
// Atribui hNOController como delegate de todos os campos de texto contidos nesta view
//-( void )setViewTxtFieldsDelegate:( UIView* )hView;

@end

// Início da implementação da classe
@implementation NOBaseView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

// OLD: Agora NOControllerView é um singleton
//@synthesize hNOController, hScreenTitle;

@synthesize hScreenTitle;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOBaseView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOBaseView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	// [self cleanNOBaseView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM buildNOBaseView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNOBaseView
{
	// Inicializa as variáveis da classe
	
	// OLD: Agora NOControllerView é um singleton
//	hNOController = nil;

	hScreenTitle = @"";
	return true;
}

/*==============================================================================================

MENSAGEM cleanNOBaseView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOBaseView
//{
//}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes que iniciarmos a transição para esta view.
 
===============================================================================================*/

//-( void ) onBeforeTransition
//{
//}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

//-( void ) onBecomeCurrentScreen
//{
//}

/*==============================================================================================

MENSAGEM setNOController:
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

// OLD: Agora NOControllerView é um singleton
//-( void )setNOController:( NOControllerView* )hView
//{
//	if( hView != nil )
//	{
//		hNOController = hView;
//		[self setViewTxtFieldsDelegate: self];
//		[self setHidden: NO];
//	}
//	else
//	{
//		[self setHidden: YES];
//	}
//}

/*==============================================================================================

MENSAGEM setViewTxtFieldsDelegate
	Atribui hNOController como delegate de todos os campos de texto contidos nesta view.
 
===============================================================================================*/

// OLD: Agora NOControllerView é um singleton e este método foi passado para lá
//-( void )setViewTxtFieldsDelegate:( UIView* )hView
//{
//	if( [hView isKindOfClass: [UITextField class]] )
//		[(( UITextField* )hView ) setDelegate: [self getNOController]];
//	
//	NSArray* hSubviews = [hView subviews];
//	if( hSubviews != nil )
//	{
//		for( UIView *hSubview in hSubviews )
//			[self setViewTxtFieldsDelegate: hSubview];
//	}
//}

/*==============================================================================================

MENSAGEM showPopUpWithTitle:Msg:CancelBtIndex:AndBts:
	Exibe um popup para o usuário.
 
===============================================================================================*/

-( void )showPopUpWithTitle:( NSString* )hTitle Msg:( NSString* )hMsg CancelBtIndex:( int8 )cancelIndex AndBts:( NSString* )firstBtTitle, ...
{
	if( firstBtTitle != nil )
	{
		UIAlertView *hPopUp = [[UIAlertView alloc] initWithTitle: hTitle message: hMsg delegate: self cancelButtonTitle: nil otherButtonTitles: nil];
		if( !hPopUp )
			return;

		if( firstBtTitle )
		{
			[hPopUp addButtonWithTitle: firstBtTitle];

			// Insere os outros botões
			id btTitle;
			va_list argumentList;
			va_start( argumentList, firstBtTitle );

			while( ( btTitle = va_arg( argumentList, id ) ) )
				[hPopUp addButtonWithTitle: ( NSString* )btTitle];

			va_end( argumentList );

			[hPopUp setCancelButtonIndex: cancelIndex];
			[hPopUp show];
			[hPopUp release];
		}
	}
}

/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário pressiona um dos botões do popup.

================================================================================================*/

-( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	// A implementação padrão é vazia
}

// Fim da implementação da classe
@end
