#include "NOProfileAuthenticateView.h"

// Components
#include "NOCustomer.h"
#include "NOViewsIndexes.h"

#include "ObjcMacros.h"
#include "UICustomSwitch.h"
#include "UILimitedTextField.h"

// Macros auxiliares
#define ON_ERROR( str ) [[NOControllerView sharedInstance] showError: str]

#define RETURN_IF_ERROR( stdString )		\
		if( !error.empty() )				\
		{									\
			ON_ERROR( stdString );			\
			return;							\
		}

// Extensão da classe para declarar métodos privados
@interface NOProfileAuthenticateView ( Private )

// Inicializa o objeto
-( bool )buildNOProfileDownloadView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOProfileDownloadView;

// Inicia a animação de scroll do campo que acompanha a movimentação do teclado
- ( void ) startAnimation:( SEL )hSelector WithUserInfo:( NSDictionary* )hDictionary;

// Pára a animação de scroll do campo que acompanha a movimentação do teclado
- ( void ) stopAnimation;

@end

// Início da implementação da classe
@implementation NOProfileAuthenticateView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize viewMode;

-( void )setViewMode:( NOProfileAuthenticateViewMode )mode
{
	viewMode = mode;

	NSString* hBtTitle;
	switch( viewMode )
	{
		case PROFILE_AUTHENTICATE_VIEW_MODE_LOGIN:
			hBtTitle = @"Login";
			break;
			
		case PROFILE_AUTHENTICATE_VIEW_MODE_DOWNLOAD:
			hBtTitle = @"Download";
			break;
	}
	
	[self setScreenTitle: hBtTitle];

	[hBtOk setTitle: hBtTitle forState: UIControlStateNormal];
	[hBtOk setTitle: hBtTitle forState: UIControlStateHighlighted];
	[hBtOk setTitle: hBtTitle forState: UIControlStateDisabled];
	[hBtOk setTitle: hBtTitle forState: UIControlStateSelected];
}

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOProfileDownloadView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOProfileDownloadView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Configura os elementos da view que foram criados pelo InterfaceBuilder
	
	// Determina os valores da switch
	// OBS: Os caracteres de espaço servem para centralizarmos as strings na imagem da switch
	[hSwRememberMe setLeftLabelText: @"YES" ];
	[hSwRememberMe setRightLabelText: @" NO" ];
	
	[hSwRememberPassword setLeftLabelText: @"YES" ];
	[hSwRememberPassword setRightLabelText: @" NO" ];
	
	// Determina os limites dos campos de texto
	uint32 minTextFieldLen, maxTextFieldLen;
	NOCustomer::GetNicknameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbNickname setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetPasswordSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbPassword setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
//	[self cleanNOProfileDownloadView];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM buildNOProfileDownloadView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNOProfileDownloadView
{
	// Inicializa as variáveis da classe
	noListener.setCocoaListener( self );
	return true;
}

/*==============================================================================================

MENSAGEM cleanNOProfileDownloadView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOProfileDownloadView
//{
//}

/*==============================================================================================

MENSAGEM setProfileToAuthenticate:
	Este método deve ser chamado antes de a view ser exibida. Ele prrenche os campos do formulário
com os dados do perfil passado como parâmetro.

===============================================================================================*/

-( void )setProfileToAuthenticate:( const NOCustomer* )pProfileToLogin
{
	if( pProfileToLogin )
	{
		currCustomer = *pProfileToLogin;

		NOString nickname;
		currCustomer.getNickname( nickname );
		[hTbNickname setText: [NOControllerView ConvertSTDStringToNSString: nickname]];

		NOString auxOtherData;
		currCustomer.getPassword( auxOtherData );
		[hTbPassword setText: [NOControllerView ConvertSTDStringToNSString: auxOtherData ]];
	}
}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.

===============================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( hButton == hBtOk )
	{
		NOString aux, error;

		[NOControllerView ConvertNSString: [hTbNickname text] toSTDString: aux];
		currCustomer.setNickname( aux, error );
		RETURN_IF_ERROR( error );

		[NOControllerView ConvertNSString: [hTbPassword text] toSTDString: aux];		
		currCustomer.setPassword( aux, error );
		RETURN_IF_ERROR( error );
		
		currCustomer.setRememberMe( [hSwRememberMe isOn] );
		currCustomer.setRememberPassword( [hSwRememberPassword isOn] );
		
		switch( viewMode )
		{
			case PROFILE_AUTHENTICATE_VIEW_MODE_UNDEFINED:
				return;

			case PROFILE_AUTHENTICATE_VIEW_MODE_LOGIN:
				if( !NOCustomer::SendLoginRequest( &currCustomer, &noListener ) )
				{
					ON_ERROR( L"Could not create request" );
					return;
				}
				break;

			case PROFILE_AUTHENTICATE_VIEW_MODE_DOWNLOAD:
				if( !NOCustomer::SendDownloadRequest( &currCustomer, &noListener ) )
				{
					ON_ERROR( L"Could not create request" );
					return;
				}
				break;
		}
		
		// Tudo foi OK, então apaga quaisquer indicações de erros
		NOControllerView *hNOController = [NOControllerView sharedInstance];

		[hNOController hideError];
		[hNOController showWaitViewWithText: nil];
	}
}

/*==============================================================================================

MENSAGEM onNOSuccessfulResponse
	Indica que uma requisição foi respondida e terminada com sucesso.

===============================================================================================*/

- ( void )onNOSuccessfulResponse
{
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	[hNOController hideWaitView];

	// Por enquanto, rememberMe ainda não é utilizado
	if( [hSwRememberMe isOn] )
	{		
		// Salva o perfil localmente
		if( [NOControllerView insertProfile: currCustomer] != FS_OK )
		{
			ON_ERROR( L"Could not save profile" );
			return;
		}
	}

	// Mostra o popup de feedback
	NSString *hFeedback;
	switch( viewMode )
	{
		case PROFILE_AUTHENTICATE_VIEW_MODE_LOGIN:
			hFeedback = @"Logged in";
			
			// Salva o último perfil a fazer login
			[hNOController setLastLoggedInProfile: currCustomer.getProfileId()];
			
			// O perfil logado é o perfil ativo
			[hNOController setActiveProfile: &currCustomer];
			
			break;

		case PROFILE_AUTHENTICATE_VIEW_MODE_DOWNLOAD:
			hFeedback = @"Profile downloaded";
			
			[hNOController setTempProfile: &currCustomer];
			break;
	}

	[self showPopUpWithTitle: @"" Msg: hFeedback CancelBtIndex: 0 AndBts: @"Ok", nil];
}

/*==============================================================================================

MENSAGEM onNOError:WithErrorDesc:
	Sinaliza erros ocorridos nas operações do NanoOnline.

===============================================================================================*/

-( void ) onNOError:( NOErrors )errorCode WithErrorDesc:( NSString* )hErrorDesc
{
	[[NOControllerView sharedInstance] hideWaitView];

	NOString aux;
	[NOControllerView ConvertNSString: hErrorDesc toSTDString: aux];

	ON_ERROR( aux );
}

/*==============================================================================================

MENSAGEM onNORequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

===============================================================================================*/

-( void )onNORequestCancelled
{
	[[NOControllerView sharedInstance] hideWaitView];
}

/*==============================================================================================

MENSAGEM onNOProgressChangedTo:ofTotal:
	Indica o progresso da requisição atual.

===============================================================================================*/

-( void )onNOProgressChangedTo:( int32 )currBytes ofTotal:( int32 )totalBytes
{
	[[NOControllerView sharedInstance] setProgress: static_cast< float >( currBytes ) / totalBytes];
}

/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário pressiona um dos botões do popup.

================================================================================================*/

-( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	switch( viewMode )
	{
		case PROFILE_AUTHENTICATE_VIEW_MODE_UNDEFINED:
			return;

		case PROFILE_AUTHENTICATE_VIEW_MODE_LOGIN:
		case PROFILE_AUTHENTICATE_VIEW_MODE_DOWNLOAD:
			[hNOController setHistoryAsShortestWayToView:NO_VIEW_INDEX_PROFILE_SELECT];
			[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_SELECT];
			break;
	}
}

// Fim da implementação da classe
@end
