#include "NORankingEntry.h"

// Components
#include "Utils.h"

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

NORankingEntry::NORankingEntry( void )
			   : profileId( 0 ), rankingPos( -1 ), score( 0 ), timeStamp( Utils::GetTimeStamp() ), nickname()
{
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

NORankingEntry::NORankingEntry( NOProfileId profileId, const int64& score, const NOString& nickname, int32 rankingPos )
			   : profileId( profileId ), rankingPos( rankingPos ), score( score ), timeStamp( Utils::GetTimeStamp() ),
				 nickname( nickname )
{
}

/*==============================================================================================

MÉTODO setProfileId
	Determina o id do usuário que fez a pontuação.

===============================================================================================*/

void NORankingEntry::setProfileId( NOProfileId newProfileId )
{
	profileId = newProfileId;
}

/*==============================================================================================

MÉTODO setScore
	Determina a pontuação do usuário descrito por profileId.

===============================================================================================*/

void NORankingEntry::setScore( const int64& newScore )
{
	score = newScore;
}

/*==============================================================================================

MÉTODO setNickname
	Determina o apelido do usuário descrito por profileId.

===============================================================================================*/

void NORankingEntry::setNickname( const NOString& newNickname )
{
	nickname = newNickname;
}

/*==============================================================================================

MÉTODO setSubmitted
	Lê o objeto de uma stream.

===============================================================================================*/

void NORankingEntry::serialize( MemoryStream& stream ) const
{
	stream.writeInt32( profileId );
	stream.writeInt32( rankingPos );
	stream.writeInt64( score );
	stream.writeInt64( timeStamp );
	
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.writeUTF32String( nickname );
#else
	stream.writeUTF8String( nickname );
#endif
}

/*==============================================================================================

MÉTODO setSubmitted
	Escre o objeto em uma stream.

===============================================================================================*/

void NORankingEntry::unserialize( MemoryStream& stream )
{
	profileId = stream.readInt32();
	rankingPos = stream.readInt32();
	
	stream.readInt64( score );
	stream.readInt64( timeStamp );
	
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.readUTF32String( nickname );
#else
	stream.readUTF8String( nickname );
#endif
}
