/*
 *  Loadable.h
 *
 *  Created by Daniel Lopes Alves on 9/23/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef LOADABLE_H
#define LOADABLE_H 1

// Components
#include "LoadableListener.h"

// C++
#include <cstdio> // Definição de NULL

class Loadable
{
	public:
		// Construtor
		Loadable( LoadableListener* pListener = NULL, int32 id = -1, uint32 data = 0 );
	
		// Destrutor
		virtual ~Loadable( void );

		// Ativa os estados do OpenGL
		virtual void load( void );
	
		// Desativa os estados do OpenGL
		virtual void unload( void );
	
		// Determina um listener que irá receber os eventos desse objeto
		void setListener( LoadableListener* pListener );
	
		// Determina dados extras para serem enviados ao listener
		void setListenerData( uint32 data );
	
	protected:
		// Obtém o ponteiro para o listener que irá receber os eventos desse objeto
		inline LoadableListener* getListener() const { return pListener; };
	
		// Retorna o id deste objeto
		inline int32 getId() const { return myId; };
	
	private:
		// Listener que irá receber os eventos deste objeto
		LoadableListener* pListener;
	
		// Identificação do loadable para o usuário
		const int32 myId;
	
		// Dados extras que o listener irá receber
		uint32 data;
};

#endif