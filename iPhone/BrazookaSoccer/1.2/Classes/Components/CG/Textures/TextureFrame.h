/*
 *  TextureFrame.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/22/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEXTURE_FRAME_H
#define TEXTURE_FRAME_H 1

#include "NanoTypes.h"

// TODOO : Acrescentar um boolean à estrutura para indicar se o frame possui transparência
// ou não. Assim podemos otimizar a (des)ativação do blend
typedef struct TextureFrame
{
	int32 x, y;
	int32 width, height;
	int32 offsetX, offsetY;
	
	TextureFrame( void ) : x( 0 ), y( 0 ), width( 0 ), height( 0 ), offsetX( 0 ), offsetY( 0 ) {};

	TextureFrame( uint32 x, uint32 y, uint32 width, uint32 height, uint32 offsetX, uint32 offsetY ) : x( x ), y( y ), width( width ), height( height ), offsetX( offsetX ), offsetY( offsetY ) {};
	
	void set( uint32 x, uint32 y, uint32 width, uint32 height, uint32 offsetX, uint32 offsetY )
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
		this->offsetX = offsetX;
		this->offsetY = offsetY;
	};
	
} TextureFrame;

#endif

