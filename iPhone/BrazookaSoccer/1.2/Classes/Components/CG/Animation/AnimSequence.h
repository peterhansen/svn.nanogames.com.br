/*
 *  AnimSequence.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/19/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ANIM_SEQUENCE_H
#define ANIM_SEQUENCE_H 1

#include "NanoTypes.h"

class AnimSequence
{
	public:
		// Construtores
		AnimSequence( void );
		AnimSequence( const AnimSequence& ref );
	
		// Destrutor
		~AnimSequence( void );
	
		// Inicializa a sequência de animação do sprite
		bool set( uint16 nSteps, const float* pFramesDurations, const uint16* pFramesIndexes );
		bool set( uint16 nSteps, float framesDurations, const uint16* pFramesIndexes );

		// Retorna o número de etapas nesta sequência
		inline uint16 getNSteps( void ) const { return nSteps; };
	
		// Retorna o índice do frame utilizado na etapa da sequência
		inline int16 getStepFrameIndex( uint16 step ) const { return step < nSteps ? pIndexes[ step ] : -1; };
	
		// Retorna a duração da etapa na sequência
		inline float getStepDuration( int16 step ) const { return ( step >= 0 ) && ( step < nSteps ) ? pDurations[ step ] : -1.0f; };
	
		// Operador de atribuição
		AnimSequence& operator=( const AnimSequence& ref );
	
	private:
		// Deleta os arrays do objeto
		void clean( void );

		// Número de elementos nos arrays de durações e de índices
		uint16 nSteps;
	
		// Array contendo a duração de cada frame nesta sequência
		float* pDurations;
	
		// Índices dos frames da sequência
		uint16* pIndexes;
};

#endif

