#include "UICustomSwitch.h"

// A hierarquia das views de um controle pode ser determinada assim:
//
//#define outstring(anObject) [[anObject description] UTF8String] 
//
//-( void )explode:( id )aView level:( int )aLevel 
//{ 
//	// Indent to show the current level 
//	for( int i = 0;  i < aLevel; i++ )
//		printf( "—" );
//
//	// Show the class and superclass for the current object 
//	printf( "%s:%s\n", outstring( [aView class] ), outstring( [aView superclass] ) ); 
//
//	// Recurse for all subviews 
//	for( UIView *subview in [aView subviews] ) 
//		[self explode: subview level: ( aLevel + 1 )]; 
//}

// Add the undocumented alternate colors 
@interface UISwitch ( Extended )
	- ( void )setAlternateColors:( BOOL ) boolean; 
@end 

// Expose the _UISwitchSlider class 
@interface _UISwitchSlider : UIView 
@end 

@implementation UICustomSwitch

-( _UISwitchSlider* )slider
{ 
	return [[self subviews] lastObject]; 
}

-( UIView* )textHolder
{ 
	return [[[self slider] subviews] objectAtIndex:2]; 
}

-( UILabel* )leftLabel
{ 
	return [[[self textHolder] subviews] objectAtIndex:0]; 
} 
-( UILabel* )rightLabel
{ 
	return [[[self textHolder] subviews] objectAtIndex:1]; 
}

-( void )setLeftLabelText:( NSString* )labelText
{
	[[self leftLabel] setText:labelText];
}

-( void )setRightLabelText:( NSString* )labelText
{
	[[self rightLabel] setText:labelText];
}

@end 
