#include "UICustomScrollView.h"

// Components
#include "ObjcMacros.h"

// Extensão da classe para declarar métodos privados
@interface UICustomScrollView ( Private )

// Inicializa a view
-( bool )build;

// Libera a memória alocada pelo objeto
-( void )clean;

// Procura uma view em uma hierarquia de views
-( bool )isView:( UIView* )hWanted InViewTreeOf:( UIView *)hView;

@end

// Início da implementação da classe
@implementation UICustomScrollView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
	// Passa um frame qualquer. A inicialização correta será feita em buildWithBkgImg:AndFillImg:LeftCapWidth:TopCapHeight:
    if( ( self = [super initWithFrame: frame] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

- ( bool )build
{
	hCancelTouchesViews = nil;
	[self setDelaysContentTouches: NO];
	return true;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self clean];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM clean
	Libera a memória alocada pelo objeto.

==============================================================================================*/

-( void )clean
{
	RELEASE_HANDLER( hCancelTouchesViews );
}

/*==============================================================================================

MENSAGEM touchesShouldBegin:withEvent:inContentView:
	Overridden by subclasses to customize the default behavior when a finger touches down in
displayed content.

================================================================================================*/

//-( BOOL )touchesShouldBegin:( NSSet* )touches withEvent:( UIEvent* )event inContentView:( UIView* )hView
//{
//	return YES;
//}

/*==============================================================================================

MENSAGEM touchesShouldCancelInContentView:
	Returns whether to cancel touches related to the content subview and start dragging.

================================================================================================*/

-( BOOL )touchesShouldCancelInContentView:( UIView* )hView
{
	if( hCancelTouchesViews == nil )
		return NO;
	
	bool found = false;
	int32 max = [hCancelTouchesViews count];
	for( int32 i = 0 ; ( i < max ) && !found; ++i )
		found = [self isView: hView InViewTreeOf: [hCancelTouchesViews objectAtIndex: i]];

	return found ? NO : YES;
}

/*==============================================================================================

MENSAGEM setCancelTouchesViews:
	Determina as subviews que devem tomar conta do toque, impedindo que UICustomScrollView faça
o scroll da tela.

================================================================================================*/

-( void )setCancelTouchesViews:( NSArray* )hViewsArray
{
	[self clean];
	hCancelTouchesViews = [[NSArray alloc] initWithArray: hViewsArray];
}

/*==============================================================================================

MENSAGEM isView:InViewTreeOf:
	Procura uma view em uma hierarquia de views.

================================================================================================*/

-( bool )isView:( UIView* )hWanted InViewTreeOf:( UIView *)hView
{
	if( hWanted == hView )
		return true;
	
	NSArray *hSubviews = [hView subviews];
	if( hSubviews )
	{
		int32 max = [hSubviews count];
		for( int32 i = 0 ; i < max ; ++i )
		{
			if( [self isView: hWanted InViewTreeOf: [hSubviews objectAtIndex:i]] )
				return true;
		}
	}
	return false;
}

// Fim da implementação da classe
@end
