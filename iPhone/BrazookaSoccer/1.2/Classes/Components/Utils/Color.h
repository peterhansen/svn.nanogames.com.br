/*
 *  NanoOnlineCustomer.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/30/08.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef COLOR_H
#define COLOR_H 1

#include "NanoTypes.h"

// Cores pré-definidas
typedef enum PreDefinedColors
{
	COLOR_BLACK = 0,
	COLOR_GREY,
	COLOR_WHITE,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_BLUE,
	COLOR_YELLOW,
	COLOR_MAGENTA,
	COLOR_CYAN,
	COLOR_PURPLE,
	COLOR_BROWN,
	COLOR_ORANGE,
	COLOR_TRANSPARENT
}PreDefinedColors;

class Color
{
	public:		
		// Construtores
		Color( void );
		Color( float red, float green, float blue, float alpha );
		Color( uint8 red, uint8 green, uint8 blue, uint8 alpha );
	
		explicit Color( uint32 color );
		Color( PreDefinedColors color );
	
		// Sets
	
		// Obtém a representação em byte do valor da componente R da cor
		inline void setUbR( uint8 red ) { r = red / 255.0f; };
	
		// Obtém a representação em byte do valor da componente G da cor
		inline void setUbG( uint8 green ) { g = green / 255.0f; };
	
		// Obtém a representação em byte do valor da componente B da cor
		inline void setUbB( uint8 blue ) { b = blue / 255.0f; };
	
		// Obtém a representação em byte do valor da componente A da cor
		inline void setUbA( uint8 alpha ) { a = alpha / 255.0f; };
	
		// Obtém a representação em byte do valor da componente R da cor
		inline void setFloatR( float red ) { r = red; };
	
		// Obtém a representação em byte do valor da componente G da cor
		inline void setFloatG( float green ) { g = green; };
	
		// Obtém a representação em byte do valor da componente B da cor
		inline void setFloatB( float blue ) { b = blue; };
	
		// Obtém a representação em byte do valor da componente A da cor
		inline void setFloatA( float alpha ) { a = alpha; };
	
		inline void set4ub( uint8 red = 0, uint8 green = 0, uint8 blue = 0, uint8 alpha = 255 )
		{
			set4f( red / 255.0f, green / 255.0f, blue / 255.0f, alpha / 255.0f );
		};

		inline void set4f( float red = 0.0f, float green = 0.0f, float blue = 0.0f, float alpha = 1.0f )
		{
			r = red;
			g = green;
			b = blue;
			a = alpha;			
		};
		
		inline void set( uint32 color )
		{
			set4ub( color >> 24, ( color >> 16 ) & 0xFF, ( color >> 8 ) & 0xFF, color & 0xFF );
		};

		void set( PreDefinedColors color );

		// Gets
	
		// Obtém a representação em byte do valor da componente R da cor
		inline uint8 getUbR( void ) const { return static_cast< uint8 >( r * 255.0f ); };
	
		// Obtém a representação em byte do valor da componente G da cor
		inline uint8 getUbG( void ) const { return static_cast< uint8 >( g * 255.0f ); };
	
		// Obtém a representação em byte do valor da componente B da cor
		inline uint8 getUbB( void ) const { return static_cast< uint8 >( b * 255.0f ); };
	
		// Obtém a representação em byte do valor da componente A da cor
		inline uint8 getUbA( void ) const { return static_cast< uint8 >( a * 255.0f ); };

		// Obtém a representação em byte do valor da componente R da cor
		inline float getFloatR( void ) const { return r; };
	
		// Obtém a representação em byte do valor da componente G da cor
		inline float getFloatG( void ) const { return g; };
	
		// Obtém a representação em byte do valor da componente B da cor
		inline float getFloatB( void ) const { return b; };
	
		// Obtém a representação em byte do valor da componente A da cor
		inline float getFloatA( void ) const { return a; };

		// Operadores	
		bool operator==( const Color& rho ) const
		{
			// OLD: Seria assim se as componentes fossem armazenadas como uint8
			//return ( this->r == rho.r ) && ( this->g == rho.g ) && ( this->b == rho.b ) && ( this->a == rho.a );
			
			return ( this->getUbR() == rho.getUbR() ) && ( this->getUbG() == rho.getUbG() ) && ( this->getUbB() == rho.getUbB() ) && ( this->getUbA() == rho.getUbA() );
		};
	
		bool operator!=( const Color& rho ) const
		{
			return !( *this == rho );
		}
	
	private:
		// Componentes da cor
		float r, g, b, a;
};

#endif
