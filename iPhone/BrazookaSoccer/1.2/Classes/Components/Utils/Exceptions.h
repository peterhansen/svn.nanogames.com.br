/*
 *  Exceptions.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/27/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H 1

#include <exception>	// C++ exceptions
#include <new>			// C++ exceptions
#include <cstring>		// strncpy

#pragma GCC visibility push(default)

extern "C++"
{
	// TASK : Otimizar!!!!!!!!!!
	
	// Tamanho máximo da string de descrição da exceção
	#define MAX_EXCEPTION_MSG_LEN 256

	class NanoBaseException : public std::exception
	{
		public:
			// Construtor
			explicit NanoBaseException( const char* pMsg = NULL )
			{
				if( pMsg )
				{
					strncpy( msg, pMsg, MAX_EXCEPTION_MSG_LEN - 1 );
					msg[ MAX_EXCEPTION_MSG_LEN - 1 ] = '\0';
				}
				else
				{
					strcpy( msg, "NanoBaseException" );
				}
			};
		
			// Obtém a mensagem da exceção
			/* final */virtual const char* what( void ) const throw() { return msg; };

		private:
			// Erro informado pela exceção
			char msg[ MAX_EXCEPTION_MSG_LEN ];
	};

	class ConstructorException : public NanoBaseException
	{
		public:
			// Construtor
			explicit ConstructorException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
	};

	class InvalidArgumentException : public NanoBaseException
	{
		public:
			// Construtor
			explicit InvalidArgumentException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
	};

	class InvalidOperationException : public NanoBaseException
	{
		public:
			// Construtor
			explicit InvalidOperationException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
	};

	class OutOfMemoryException : public NanoBaseException
	{
		public:
			// Construtor
			explicit OutOfMemoryException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
	};

	class IllegalMathOperation : public NanoBaseException
	{
		public:
			// Construtor
			explicit IllegalMathOperation( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
	};

	class OpenALException : public NanoBaseException
	{
		public:
			// Construtor
			explicit OpenALException( const char* pMsg = NULL ) : NanoBaseException( pMsg ){};
	};

} // extern "C++"

#pragma GCC visibility pop

#endif
