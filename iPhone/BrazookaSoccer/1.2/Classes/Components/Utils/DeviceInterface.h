/*
 *  DeviceInterface.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 8/31/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef DEVICE_INTERFACE_H
#define DEVICE_INTERFACE_H 1

#include <string>

class DeviceInterface
{
	public:
		// A string unique to each device based on various hardware details
		static std::string& GetDeviceUID( std::string& str, bool append = false );

		// The name identifying the device
		static std::string& GetDeviceName( std::string& str, bool append = false );

		// The name of the operating system running on the device represented by the receiver
		static std::string& GetDeviceSystemName( std::string& str, bool append = false );

		// The current version of the operating system
		static std::string& GetDeviceSystemVersion( std::string& str, bool append = false );

		// The model of the device
		static std::string& GetDeviceModel( std::string& str, bool append = false );

		// The model of the device as a localized string
		static std::string& GetDeviceLocalizedModel( std::string& str, bool append = false );
};

#endif
