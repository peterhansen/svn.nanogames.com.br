//
//  main.m
//  FreeKick
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

#if DEBUG
	#include "Exceptions.h"
	#include "ObjcMacros.h"
#endif

int main( int argc, char* argv[] )
{
	NSAutoreleasePool* hPool = [[NSAutoreleasePool alloc] init];

#if DEBUG
	@try
	{
		try
		{
#endif
			const int retVal = UIApplicationMain( argc, argv, NULL, NULL );
			[hPool release];
			return retVal;

#if DEBUG
		}
		catch( std::exception& ex )
		{
			LOG( ex.what() );
			LOG( "\n" );
			[hPool release];
			return 1;
		}
	}
	@catch( NSException* hException )
	{
		// TASK : De repente é bom manter este try-catch. Também poderíamos colocar o
		// "[hPool release]" dentro de um finally
		LOG( NSSTRING_TO_CHAR_ARRAY( [hException reason] ) );
		LOG( "\n" );
		[hPool release];
		return 1;
	}
#endif
}
