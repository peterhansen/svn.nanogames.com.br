/*
 *  Background.h
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef BACKGROUND
#define BACKGROUND 1

#include"Texture2D.h"
#include"Sprite.h"
#include "ObjectGroup.h"
#include"DotGameInfo.h"

/*
 seria o fundo do  cenário, como seria as animações, os elementos, etc, tem que se definir....
 mas dentro desta  variável ficaria os elementos. provavelmente deve ter um listener ou qq coisa, para se comportar de acordo com a partida...
 
 */

//posições de objetos dentro do grupo
#define MARKS_INDICE_INSIDE_GROUP 0


#include "BoardFactory.h"

class GameBaseInfo;
class DotGame;
class Marks;

/*enum idBackground {
 ID_BGD_NONE = -1,
 ID_BGD_ANGEL = 0,
 ID_BGD_DEVIL,
 ID_BGD_FARMER,
 ID_BGD_EXECUTIVE,
 ID_BGD_FAIRY,
 ID_BGD_OGRE	
 }typedef idBackground ;
 */
class Background:public ObjectGroup {
	
public:
	
	//construtor
	Background( idBackground _id,  DotGame *dg );
	
	//destrutor
	~Background( ){};
	
	//Texture2DHandler getFundo( void );
	
	Sprite*/*Texture2DHandler*/ getBaseLineH( void ){};
	
	Sprite*/*Texture2DHandler*/ getBaseLineV( void ){};
	
	void removeAllImages( void );
	
	idBackground getIdBackground( void );
	
	bool loadAllImages( void );
	
	bool buildBackground( DotGame *dg  );
	
protected:
	idBackground idBack;
	
	///static /*sprite*/Texture2DHandler fundo;
	//	static Sprite* /*Texture2DHandler*/ mark;
	static Sprite* /*Texture2DHandler*/ baseLineH;
	static Sprite* /*Texture2DHandler*/ baseLineV;
};

inline idBackground Background::getIdBackground( void ){
	return idBack;
}
#endif