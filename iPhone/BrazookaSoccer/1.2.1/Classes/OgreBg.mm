/*
 *  OgreBg.mm
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "OgreBg.h"
#include "TextureFrame.h"
#include "TexturePattern.h"
#include "ObjcMacros.h"
#include "Marks.h"


//#define INDEX_BOARD_BACKGROUND 3
//#define INDEX_BOARD_MARKS 0

OgreBg::OgreBg( GameBaseInfo* g ):Board( ID_BGD_OGRE, g ){

	generateBackground();
	configureMarks();
}

void OgreBg::generateBackground( void ){
	char* nbg ="bg2\0";
	
	TextureFrame f( 0.0f, 0.0f, 512.0f, 512.0f, 0.0f, 0.0f );
	TexturePattern *t = new TexturePattern( nbg , &f  );
	t->setSize( SCREEN_WIDTH * 2.0f , SCREEN_HEIGHT * 2.0f );
	t->setPosition(( - t->getWidth()  ) * 0.5f,
				   ( - t->getHeight() ) * 0.25f);
	if( !insertObject( t ) ) 
	{

#if DEBUG
		LOG("falha no fundo");
#endif	
		return ;
	}
	setObjectZOrder( INDEX_BOARD_BACKGROUND, 0);
}
void OgreBg::configureLinesBkgd( void ){

}

void OgreBg::configureMarks( void ){
	static_cast<Marks*> ( getObject( INDEX_BOARD_MARKS ) )->reconfigureMarks( INDEX_MARK_OGRE ) ;
}