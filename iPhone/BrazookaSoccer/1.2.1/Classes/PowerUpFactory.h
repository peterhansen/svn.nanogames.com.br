/*
 *  PowerUpFactory.h
 *  dotGame
 *
 *  Created by Max on 2/18/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_FACTORY_H 
#define POWER_UP_FACTORY_H 1

#define N_POWER_UPS 10
//colocar mais depois...
enum PowerUpType{
	POWER_UP_TYPE_NONE = -1,
	POWER_UP_TYPE_COMBO_BREAK = 0,
	POWER_UP_TYPE_MOVE_CHOOSE,
	POWER_UP_TYPE_MOVE_RANDOM,
	POWER_UP_TYPE_TIME_SHORT,
	POWER_UP_TYPE_BOMB
}typedef PowerUpType;

class PowerUp;
class DotGame;
class PowerUpListener;

class PowerUpFactory {

public:
	static bool Create( void );
	
	static void Destroy( void );
	
	static PowerUpFactory* GetInstance( void ){ return pSingleton; }
	
	PowerUp* getNewPowerUp( PowerUpType t, PowerUpListener *dg );

	int getNPowerTypes( void );
	
private:
	PowerUpFactory();
	
	~PowerUpFactory();
	
	static PowerUpFactory* pSingleton;
	
	int nPowerTypes;
	
};

inline int PowerUpFactory::getNPowerTypes( void ){
	return nPowerTypes;
}

#endif