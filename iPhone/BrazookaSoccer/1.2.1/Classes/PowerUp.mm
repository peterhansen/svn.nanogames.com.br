/*
 *  PowerUp.mm
 *  dotGame
 *
 *  Created by Max on 2/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUp.h"
#include "ZoneGame.h"
#include "Player.h"
PowerUp::PowerUp( PowerUpType p , const char* pImagePath ,PowerUpListener *listener ):
RenderableImage( pImagePath ),
pListener( listener ),
pwrUpType( p ),
disarmed( false ),
zoneOf( NULL ){ 
//	setVisible( true );
}

PowerUp::~PowerUp( void ){
	pListener = NULL;
	zoneOf = NULL;
	
#if DEBUG
	NSLog(@" destruindo PowerUp ");
#endif
}
/* ======================================================================================================
 
 onrelease->fç chamada qdo o power up é lançado, chamando o seu listener para que ele faça algo de acordo com o tipo de power up.
 
 ======================================================================================================*/
/*void PowerUp::onRelease( void ){
	setVisible( false );
#if DEBUG
	NSLog(@" pwrup posicao x = %.2f,y = %.2f,z = %.2f\n ", position.x, position.y, position.z);
#endif
	//colocar  aqui código de animação ou qq coisa assim que aconteça neste momento;
	pListener->PowerUpEffect( this );
}*/
/* ======================================================================================================
 
 onrelease->fç chamada para associar um power up à ua zona do tabuleiro
 
 ======================================================================================================*/
void PowerUp::setZone( ZoneGame *z ){
	this->zoneOf = z;
}
/* ======================================================================================================
 
 setListener->fç chamada para escolher o seu listener
 
 ======================================================================================================*/
void PowerUp::setListener( PowerUpListener *p ){
	this->pListener = p;
}
/* ======================================================================================================
 
 setPosition->usada para posicionar o powerUp
 
 ======================================================================================================*/
void PowerUp::setPosition(uint8 x, uint8 y ){
	Point3f p;
	p.set( x  * ZONE_SIDE + ( ( x + 1 ) * LINE_WIDTH ),
		   y  * ZONE_SIDE + ( ( y + 1 ) * LINE_WIDTH ),
		   1.0f);
	position.set( p );
}