/*
 *  CharacterFactory.h
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef CHARACTER_FACTORY_H
#define CHARACTER_FACTORY_H 1

//#include "DotGameInfo.h"

#define N_DOLLS 6
#include "NanoTypes.h"
//idem...
enum IdDoll {
	DOLL_ID_NONE = -1,
	DOLL_ID_ANGEL = 0,
	DOLL_ID_DEVIL = 1,
	DOLL_ID_FARMER,
	DOLL_ID_EXECUTIVE,
	DOLL_ID_FAIRY,
	DOLL_ID_OGRE	
} typedef IdDoll;

class GenericDoll;
class CharacterFactory {
	
public:

	
//void LoadCharacterIndice( GenericDoll *g , int indice );
	
GenericDoll */*void*/ LoadCharacterIndice( /*GenericDoll *g ,*/ IdDoll id );
	
	static bool Create( void );
	
	static void Destroy( void );

	static CharacterFactory* GetInstance( void ) { return pSingleton; };
	
	uint8 getNCharacters( void ){ return nCharacters;  }
	
	void setNCharacters( uint8 n  ){ nCharacters = n;  }
	
private:	
	CharacterFactory();

	~CharacterFactory();
	static CharacterFactory* pSingleton;
	
	uint8 nCharacters;
	
};


#endif
