/*
 *  CharacterFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "CharacterFactory.h"
#include "GenericDoll.h"

#include "Diabinho.h"
#include "Ogro.h"
#include "Anjinho.h"
#include "Executivo.h"
#include "Fadinha.h"
#include "Fazendeiro.h"

CharacterFactory* CharacterFactory::pSingleton = NULL ;

bool CharacterFactory::Create( void ){
if( !pSingleton )
	pSingleton = new CharacterFactory();
	
	return pSingleton != NULL;

}

void CharacterFactory::Destroy( void ){ 
	SAFE_DELETE( pSingleton );
}

CharacterFactory::~CharacterFactory(){
#if DEBUG
	NSLog(@"destruindo CharacterFactory");	
#endif
	
}


//seria melhor se fosse dinâmico...
CharacterFactory::CharacterFactory():nCharacters( N_DOLLS ){
#if DEBUG
	NSLog(@"criando CharacterFactory");	

#endif
	
}


//void CharacterFactory::LoadCharacterIndice( GenericDoll *g , int indice ){}

GenericDoll* CharacterFactory::LoadCharacterIndice( IdDoll id ){
	GenericDoll* g;
	
	switch ( id ) {
		case DOLL_ID_ANGEL :
			g = new Anjinho();
			break;
		case DOLL_ID_DEVIL :
			g = new Diabinho();
			break;
		case DOLL_ID_FAIRY :
			g = new Fadinha();
			break;
		case DOLL_ID_OGRE :
			g = new Ogro();
			break;
		case DOLL_ID_EXECUTIVE :
			g = new Executivo();
			break;
		case DOLL_ID_FARMER :
			g = new Fazendeiro();
			break;
			
	}
	return g;
}

