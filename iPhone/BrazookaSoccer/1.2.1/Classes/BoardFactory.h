/*
 *  BoardFactory.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef BOARD_FACTORY_H
#define BOARD_FACTORY_H 1

//#include "DotGameInfo.h"
//#include "GameBaseInfo.h"

#define N_BOARDS 6

#define INDEX_MARK_ANGEL 0
#define INDEX_MARK_OGRE 1
#define INDEX_MARK_DEVIL 2
#define INDEX_MARK_FAIRY 3
#define INDEX_MARK_FARMER 4
#define INDEX_MARK_EXECUTIVE 5


enum idBackground {
	ID_BGD_NONE = -1,
	ID_BGD_ANGEL = 0,
	ID_BGD_DEVIL,
	ID_BGD_FARMER,
	ID_BGD_EXECUTIVE,
	ID_BGD_FAIRY,
	ID_BGD_OGRE	
}typedef idBackground;

struct GameBaseInfo;
class Board;
class BoardFactory{
public:
	
	static bool Create( void );
	
	static void Destroy( void );

	static BoardFactory* GetInstance( void ) { return pSingleton; };

	void setGameBaseInfo( GameBaseInfo* g2 ){ gInfo = g2; };
	
	Board* getNewBoard( idBackground _id );
	
	int getNBoards( void );
	
private:

	BoardFactory( GameBaseInfo* g = NULL );
	~BoardFactory();

	static BoardFactory* pSingleton;
	
	GameBaseInfo* gInfo;
	int nBoards;
	
};


inline 	int BoardFactory::getNBoards( void ){
	return nBoards;
}

#endif