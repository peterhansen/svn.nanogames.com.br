/*
 *  ExecutiveBg.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef EXECUTIVE_BG_H
#define EXECUTIVE_BG_H 1

#include "Board.h"


class ExecutiveBg : public Board {
	
public:
	ExecutiveBg( GameBaseInfo* g );
	
	~ExecutiveBg(){};
	
	
protected:	
	void generateBackground( void ) ;
	
	void configureMarks( void ) ;
	
	void configureLinesBkgd( void );
	
	
};


#endif