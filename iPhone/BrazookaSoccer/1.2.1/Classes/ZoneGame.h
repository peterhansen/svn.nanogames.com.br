/*
 *  ZoneGame.h
 *  dotGame
 *
 *  Created by Max on 10/9/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef ZONE_GAME_H
#define ZONE_GAME_H 1


/*
 classe para definir as zonas que estão sendo ocupadas ou não pelos jogadores, a primeiro momento, eu as estou considerando como uma imagem estática, e caso ela venha a ser uma animação, mudar para sprite.
 os símbolos nela serão em função do jogador ( friend? ), que determina qual imagem deve ficar aparecendo
 */

#include "Sprite.h"
#include "ObjectGroup.h"
#include "DotGameInfo.h"

enum ZoneGameState {
	ZONE_GAME_STATE_NONE = -1,
	ZONE_GAME_STATE_BLANK = 0,	
	ZONE_GAME_STATE_OCCUPIED
}typedef ZoneGameState;

class Player;
class DotGame;


class ZoneGame : public ObjectGroup {
	
public:
	
	ZoneGame();
	
	~ZoneGame(){};
	
	void associateImage( Player *p );

	void setZoneGameState ( ZoneGameState z );
	
	inline  ZoneGameState getZoneGameState ( void );
	
	void gainedByPlayer( DotGame *dg);
	
	void updateImage( void );
	
	void reset( void );
	
	void setPosition( uint8 x, uint8 y );
	
	void setPlayerImage( Player *p[ /*nPlayers*/ MAX_PLAYERS ] );

	bool isOccupied( void );
	
	uint8 getPlayerId( void );
	
	bool isCoupled( void );
	
	void setCoupled( bool _b );
	
	void lostByPlayer( Player* p );
	
protected:
	
	friend class LineGame;
	friend class Player;
	//friend class GenericDoll;
	friend class DotGame;
	
	uint8 playerId;
	
	ZoneGameState state;
	bool occupied;
	
	//indica se a zona faz parte de uma BigZone
	bool coupled;
	
	//sprite atual
	Sprite* pCurrSprite;

};

inline bool ZoneGame:: isOccupied( void ){
	return occupied;
}

inline  ZoneGameState ZoneGame::getZoneGameState ( void ){
	return state;
}

inline bool ZoneGame::isCoupled( void ){ 
	return coupled;
}

inline void ZoneGame::setCoupled( bool _b ){ 
	coupled = _b;
}


#endif
