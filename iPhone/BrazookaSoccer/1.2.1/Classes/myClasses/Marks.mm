//
//  Marks.mm
//  dotGame
//
//  Created by Max on 1/25/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#include "Marks.h"
#include "GameBaseInfo.h"
#include "Sprite.h"

#include "Utils.h"
using namespace Utils;


Marks::Marks( GameBaseInfo *g ):ObjectGroup( ( g->nVZones + 1 ) * ( g->nHZones + 1 ) ){
if	( !buildMarks( g ) )
	 return;
}

bool Marks::buildMarks( GameBaseInfo *g ){
	//como é um sprite com todos os ptos, ele  carrega daqui mesmo, e cria várias cópias. como tem aquele problema esquisito com sprites estáticos, é melhor carregar uma instância aqui mesmo e criar por cópia.
	Sprite *s = new Sprite( "pts","pts" );
	Point3f p;
	for ( uint8 x = 0; x < ( g->nHZones + 1 ) ; x++ ) {
		for ( uint8 y = 0; y < ( g->nVZones + 1 ); y++) {
			Sprite *s2 = new Sprite( s );
#if DEBUG
			char name[ 23 ];
			snprintf( name , 23 , "marca: x= %d, y=%d \n" , x , y );
			s2->setName( name );
#endif
			p.set( x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
				  ( y * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ),
				  1.0);
			s2->setPosition( &p );
			if( !insertObject( s2 ) ){
				return false;
				break;
			}
			
		}
	}
	return true;
}


//mudar isto para que receba um numero e com esse numero fosse para a linha correspondente da pesonagem
void Marks::reconfigureMarks( /*idBackground idBg*/ int8 i ){
	Sprite *s2;
	for( uint32 x = 0; x < getNObjects(); x++ ){
		s2 = dynamic_cast< Sprite* > ( getObject( x ) ) ;
		s2->setAnimSequence( i );
	}
}
/* ++++++++++++++++++++===============================================
 
disappearMark => faz uma marca desaparecer.
 
 ++++++++++++++++++++===============================================*/
void Marks::disappearMark( const Point3f *p ){
	
	for( int16 i; i< getNObjects(); i++ ){
	
		if(  IsPointInsideRect( p , getObject( i ) ) ){
			getObject(i)->setVisible( false );	
		}
	}
}

/* ++++++++++++++++++++===============================================
 
 reappearMark => faz uma marca aparecer.
 
 ++++++++++++++++++++===============================================*/
void Marks::reappearMark( const Point3f *p ){

	for( int16 i; i< getNObjects(); i++ ){
		
		if(  IsPointInsideRect( p , getObject( i ) ) ){
			getObject(i)->setVisible( true );	
		}
	}

}


Marks::~Marks(){}