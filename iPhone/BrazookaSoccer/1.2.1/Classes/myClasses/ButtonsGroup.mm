/*
 *  ButtonsGroup.mm
 *  dotGame
 *
 *  Created by Max on 10/22/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ButtonsGroup.h"
#include "Macros.h"
#include "GLHeaders.h"
#include "Utils.h"
#include "Button.h"
#include "Color.h"

#define MAX_OBJECTS_INSIDE 16
#define BUTTONS_INDICE_START 7
#define N_BUTTONS_INSIDE_GROUP 4

#define GAME_SCREEN_BT_COLOR_PRESSED Color( static_cast< uint8 >( 127 ), 127, 127, 255 )
#define GAME_SCREEN_BT_COLOR_UNPRESSED Color( static_cast< uint8 >( 255 ), 255, 255, 255 )

ButtonsGroup::ButtonsGroup( OrthoCamera *p ):ObjectGroup( MAX_OBJECTS_INSIDE ),
pCamera( p )//,
/*clearBits( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ), 
clearDepth( 1.0f ),
clearStencil( 0 ),
clearColor()*/{

#if DEBUG
	setName("grupo dus butão\n");
#endif
}

// eu sobrescrevi este método para que ele possa usar uma câmera diferente da cena...
bool ButtonsGroup::render( void ){
	//glClearColor( clearColor.getFloatR(), clearColor.getFloatG(), clearColor.getFloatB(), clearColor.getFloatA());
	pCamera->place();
	bool ret = false;
	for( uint16 i = 0 ; i < getNObjects() ; ++i )
		ret |= (getObject( i ))->render();
	return ret;
}

//retorna true se algum botão for pressionado, e indiceB é o número de identificação do botão pressionado pelo usuário
//todoo: está meio burro ainda, visto que ele mesmo deveria contar qtos botoes tem dentro, e não ser passado para ele manualmente...
bool ButtonsGroup::idButtonPressed( const Point3f *p, uint8 *indiceB ){

	uint8 c = 0;
	Button* tmp;
	bool ret = false ;
	for ( uint8 i = BUTTONS_INDICE_START;  i <= ( BUTTONS_INDICE_START + ( N_BUTTONS_INSIDE_GROUP - 1 ) ); i++) {
		tmp = static_cast< Button *> ( getObject( i ) );
		tmp->setVertexSetColor(GAME_SCREEN_BT_COLOR_UNPRESSED);
		if( tmp->isVisible() && ( tmp->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) && Utils::IsPointInsideRect( p, tmp )){
			tmp->setVertexSetColor( GAME_SCREEN_BT_COLOR_PRESSED );
			if( indiceB )
				*indiceB = c;
			ret = true;
		}
		c++;

	}
	return ret; 
}

