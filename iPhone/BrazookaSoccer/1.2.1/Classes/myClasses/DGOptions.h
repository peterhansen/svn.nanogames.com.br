/*
 *  DGOptions.h
 *  dotGame
 *
 *  Created by Max on 12/11/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DG_OPTIONS_VIEW
#define DG_OPTIONS_VIEW 1

#include "UpdatableView.h"
#include "NanoTypes.h"

@interface DGOptions : UpdatableView
{
	IBOutlet UILabel* hVolume;
	IBOutlet UILabel* hVibra;
	
	IBOutlet UISlider* hVolum;
	IBOutlet UIButton *hVbBoxOn, *hVbBoxOff;
	IBOutlet UIButton *hBack;
	
	int8 previousScreen;
}

- ( IBAction ) onBtPressed:( id )hButton;


- ( IBAction ) onSldPressed:( id )hSld;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

-( void )setVibrationStatus:( bool )VibrationOn;

-( void )setPreviousIndex:( int8 )prevIndex;

@end
#endif