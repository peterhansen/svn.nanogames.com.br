/*
 *  DGMainMenu.mm
 *  dotGame
 *
 *  Created by Max on 12/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "DGMainMenu.h"
#include "Macros.h"
#include "FreeKickAppDelegate.h"
#include"Config.h"
#include"ObjcMacros.h"
#include"MathFuncs.h"

// NanoOnline
#include "NOControllerView.h"
#include "NOCustomer.h"
#include "NOGlobalData.h"
#include "NOString.h"

#define MAX_TIME_TO_EXIBITHION 3.5//10.0
@implementation DGMainMenu


- ( void )awakeFromNib
{
	[super awakeFromNib];
	//[self resume];	
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
	//[hTxtHelp setFont: [(( FreeKickAppDelegate* )APP_DELEGATE ) getiPhoneFont]];
	
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton{

	if( hButton == hPlay ){
		[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_SELECT_MODE ];
		[ self suspend ];
	}else if(hButton == hOption  ) {
		[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_OPTIONS];
		[ self suspend ];
	NSLog(@"tela de opções");			
	}else if( hButton == hHelp ){
[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_HELP ];
	}else if(hButton == hConfig  ) {
		NSLog(@"tela de ajuda");
//	[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_OPTIONS];
	}else if( hButton == hOnline ) {
		[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_NANO_ONLINE_FROM_MAIN_MENU ];
		[ self suspend ];

	}
}


// Método chamado antes de iniciarmos uma transição para esta view
- ( void ) onBeforeTransition :( GameBaseInfo * )ginf{
	
	ginfo = ginf;
	ginf->reset();
}

-( void )update: ( float ) timeElapsed{
//	timeCounter += timeElapsed;
	if ( NanoMath::fgeq( timeCounter, MAX_TIME_TO_EXIBITHION ) ) {
		timeCounter = 0.0;
		ginfo->iddolls[ 0 ] = DOLL_ID_OGRE;
		ginfo->iddolls[ 1 ] = DOLL_ID_FAIRY;
		ginfo->mode = DOT_GAME_MODE_EXIBITHION;
		ginfo->nVZones = 3;
		ginfo->nHZones = 3;
		ginfo->idBg = ID_BGD_FAIRY;
		[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_LOAD_GAME ];
		[ self suspend ];
		//NSLog(@"chamando a tela de jogo aqui!!");
	}
}


// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen{
	[ self resume ];
}

@end
