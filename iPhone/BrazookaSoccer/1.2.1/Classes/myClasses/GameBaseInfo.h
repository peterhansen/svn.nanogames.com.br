/*
 *  GameBaseInfo.h
 *  dotGame
 *
 *  Created by Max on 10/23/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef Game_BASE_INFO
#define Game_BASE_INFO 1

#include <string>
using std::string;
#include "DotGameInfo.h"
#include "Player.h"
#include "CharacterFactory.h"
#include "BoardFactory.h"

struct GameBaseInfo {
	GameBaseInfo( void );
	string nomes[ MAX_PLAYERS ];
	IdDoll iddolls[ MAX_PLAYERS ];
	PlayerUser pUserType[ MAX_PLAYERS ];
	DotGameMode mode;
	uint8 nPlayers, nVZones, nHZones, level  ;
	idBackground idBg;
	uint8 scores[ MAX_PLAYERS ];	
	void reset();
};
#endif

