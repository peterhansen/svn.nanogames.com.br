/*
 *  InfoBar.mm
 *  dotGame
 *
 *  Created by Max on 12/1/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "InfoBar.h"
#include"ObjcMacros.h"

#include"GameBaseInfo.h"
#include"RenderableImage.h"
#include"InterfaceControlListener.h"
#include"Chronometer.h"
#include"ChronometerListener.h"
#include"DotGame.h"
#include"Label.h"
#include "FreeKickAppDelegate.h"

#include "Exceptions.h"
#include "Label.h"
#include "Quad.h"
#include "Sphere.h"
#include "RenderableImage.h"
#include "Utils.h"


// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
if( !pObject || !pGroup->insertObject( pObject ) )	\
{													\
delete pObject;									\
/*goto Error;*/										\
}


#define INFO_BAR_FONT_CHAR_SPACEMENT 1.0f

#define NAME_LABEL1_POSITION_X 80.0f
#define NAME_LABEL1_POSITION_Y 0.0f
#define SCORE_LABEL1_POSITION_X 80.0f
#define SCORE_LABEL1_POSITION_Y 20.f
#define FACE1_POSITION_X 20.0f
#define FACE1_POSITION_Y 0.0f

#define NAME_LABEL2_POSITION_X 380.0f
#define NAME_LABEL2_POSITION_Y 0.0f
#define SCORE_LABEL2_POSITION_X 380.0f
#define SCORE_LABEL2_POSITION_Y 20.0f

#define FACE2_POSITION_X 440.0f
#define FACE2_POSITION_Y 0.0f

#define FACE1_WIDTH 35.0f
#define FACE1_HEIGHT 35.0f
#define FACE2_WIDTH 35.0f
#define FACE2_HEIGHT 35.0f

#define LABELS_HEIGHT 22.0f
#define LABELS_WIDTH 45.0f


#define LABEL_SCORE_P1_INDICE 0
#define LABEL_SCORE_P2_INDICE 1
#define LABEL_NAME_P1_INDICE 2
#define LABEL_NAME_P2_INDICE 3
#define INFO_BAR_N_OBJECTS 6

InfoBar::InfoBar( const GameBaseInfo &gInfo, InterfaceControlListener* pListener ):ObjectGroup( INFO_BAR_N_OBJECTS ), InterfaceControl( pListener )
 {

	buildInfoBar();
//	ginfo = gInfo;

}


void InfoBar::buildInfoBar( void ){
	
	Font* pInfoBarFont = [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_MAIN];

	if(!pInfoBarFont)
		NSLog(@"fonte não carregada!!");
	
	Label* pLbScoreP1 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbScoreP1 ))
		NSLog(@"label fail!!");
	pLbScoreP1->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
	pLbScoreP1->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbScoreP1->setPosition( SCORE_LABEL1_POSITION_X, SCORE_LABEL1_POSITION_Y ,1.0f  );
	pLbScoreP1->setSize( LABELS_WIDTH, LABELS_HEIGHT );
	pLbScoreP1->setText("0",false, false);

	Label* pLbScoreP2 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbScoreP2 ))
		NSLog(@"label fail!!");
	pLbScoreP2->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
	pLbScoreP2->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbScoreP2->setPosition( SCORE_LABEL2_POSITION_X, SCORE_LABEL2_POSITION_Y,1.0f  );
	pLbScoreP2->setSize( LABELS_WIDTH, LABELS_HEIGHT );	
	pLbScoreP2->setText("0",false, false);
	

	Label* pLbNameP1 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbNameP1 ))
		NSLog(@"label fail!!");
	pLbNameP1->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM);
	pLbNameP1->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbNameP1->setPosition( NAME_LABEL1_POSITION_X, NAME_LABEL1_POSITION_Y ,1.0f  );
	pLbNameP1->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
	pLbNameP1->setText("111",false, false);
	
	Label* pLbNameP2 = new Label( pInfoBarFont , false );
	if(	!insertObject( pLbNameP2 ))
		NSLog(@"label fail!!");
	pLbNameP2->setTextAlignment( ALIGN_HOR_LEFT | ALIGN_VER_BOTTOM );
	pLbNameP2->setCharSpacement( INFO_BAR_FONT_CHAR_SPACEMENT );
	pLbNameP2->setPosition( NAME_LABEL2_POSITION_X, NAME_LABEL2_POSITION_Y ,1.0f  );
	pLbNameP2->setSize(LABELS_WIDTH, LABELS_HEIGHT);	
	pLbNameP2->setText("666",false, false);
	
	//colocar aqui depois recebendo as imagens de cada um dos jogadores...
	RenderableImage*r = new RenderableImage();
	Texture2DHandler htexture = ResourceManager::GetTexture2D( "tmpface" );
	r->setImage( htexture );
	r->setSize( FACE1_WIDTH, FACE1_HEIGHT  );
	r->setPosition( FACE1_POSITION_X, FACE1_POSITION_Y );
	insertObject( r );
	
	RenderableImage*r2 = new RenderableImage();
	Texture2DHandler htexture2 = ResourceManager::GetTexture2D( "tmpface2" );
	r2->setImage( htexture2 );
	r2->setSize( FACE2_WIDTH, FACE2_HEIGHT );
	r2->setPosition( FACE2_POSITION_X, FACE2_POSITION_Y );
	insertObject( r2 );
	
	
}

void InfoBar::clear(){

}

// Atualiza o objeto
bool InfoBar::update( float timeElapsed ){
	if( !ObjectGroup::update( timeElapsed ) )
		return false;
	
	return true;
}

/*==================================================================

 updateScores-> atualiza a pontuação
 
==================================================================*/
#define MAX_BUFFER_SCORE_LEN 3

void InfoBar::updateScores( DotGame *dg ){
	
	char buffer[ MAX_BUFFER_SCORE_LEN ];
	snprintf( buffer, MAX_BUFFER_SCORE_LEN, "%d",dg->players[ 0 ]->getScore() );
	static_cast< Label* >( getObject( 0 ) )->setText( buffer, false, false );
	
	char buffer2[ MAX_BUFFER_SCORE_LEN ];
	snprintf( buffer2, MAX_BUFFER_SCORE_LEN, "%d",dg->players[ 1 ]->getScore() );
	static_cast< Label* >( getObject( 1 ) )->setText( buffer2, false, false );
}
#undef MAX_BUFFER_SCORE_LEN
/*==================================================================
 
configure-> puxa dados do jogo, como nome, boneco, etc.
 
 ==================================================================*/
#define MAX_BUFFER_NAME_LEN 6
void InfoBar::configure( DotGame *dg ){

	char buffer[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer , MAX_BUFFER_NAME_LEN , dg->GInfo.nomes[0].c_str() );
	
	static_cast< Label* >( getObject( 2 ) )->setText( buffer , false, false );
	
	char buffer2[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer2, MAX_BUFFER_NAME_LEN, dg->GInfo.nomes[1].c_str() );
	static_cast< Label* >( getObject( 3 ) )->setText( buffer2, false, false );
	
}

#undef MAX_BUFFER_NAME_LEN
/*==================================================================
 
render->renderiza tudo e mais um pouco (dããããã)
 
 ==================================================================*/

bool InfoBar::render( void ){
	if( !isVisible() )
		return false;
//por enquanto...
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	glTranslatef(position.x, position.y, position.z);
	glColor4ub( 255, 0, 255, 255);
	
	Quad q1;
	
	q1.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 10.0f, 0.0f, 1.0f );
	q1.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( SCREEN_WIDTH-10, 0.0f, 1.0f );
	q1.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 10.0f, LABELS_WIDTH, 1.0f );
	q1.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( SCREEN_WIDTH-10, LABELS_WIDTH, 1.0f );
	
	q1.render();
	
	
	glColor4ub( 0, 0, 0, 255);
	Quad q2;
	
	q2.quadVertexes[ QUAD_TOP_LEFT ].setPosition( HALF_SCREEN_WIDTH - 15.0, 0.0f, 1.0f );
	q2.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( HALF_SCREEN_WIDTH + 22.0, 0.0f, 1.0f );
	q2.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( HALF_SCREEN_WIDTH - 15.0, 45.0, 1.0f );
	q2.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( HALF_SCREEN_WIDTH + 22.0f, 45.0, 1.0f );
	
	q2.render();


	glPopMatrix();
	ObjectGroup::render();
	return true;	
}

