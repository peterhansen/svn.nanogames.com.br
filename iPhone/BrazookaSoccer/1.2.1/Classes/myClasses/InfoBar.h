/*
 *  InfoBar.h
 *  dotGame
 *
 *  Created by Max on 12/1/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef INFO_BAR_H
#define INFO_BAR_H 1

#include "ObjectGroup.h"
#include "InterfaceControl.h"
#include "GameBaseInfo.h"

class RenderableImage;
class InterfaceControlListener;
//class Chronometer;
//class ChronometerListener;
class DotGame;

class InfoBar : public ObjectGroup, public InterfaceControl {
	
public:
	
	InfoBar( const GameBaseInfo &gInfo, InterfaceControlListener* pListener );//, ChronometerListener *cListener );
	
	
	void updateScores( DotGame *dg  );
	void configure( DotGame *dg );
	
	
	virtual bool render( void );
	
	
	// Atualiza o objeto
	virtual bool update( float timeElapsed );
	
	
	
private:
	friend class DotGame;
	
	void buildInfoBar( void );
	void clear();
	GameBaseInfo ginfo;
};



#endif