#include "FreeKickAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

#include "AccelerometerManager.h"

#if SOUND_WITH_OPEN_AL
	#include "AudioManager.h"
	#include "AudioSource.h"
#else
	#include "AppleAudioManager.h"
#endif

#include "EventManager.h"
#include "Font.h"
#include "Macros.h"

#include "CreditsView.h"
#include "GameScreen.h"
#include "HelpView.h"
#include "MainMenuView.h"
#include "OptionsView.h"
#include "PlayMenuView.h"
#include "RankingView.h"

#include "GoodGirlFont.h"

#include "Tests.h"
#include "DotGame.h"
#include "DotGameMainMenuView.h"

// Extensão da classe para declarar métodos privados
@interface FreeKickAppDelegate()

// Sincrozina o estado da view atual com o estado da aplicação. Este método serve para contornar
// a possibilidade de recebermos um evento de suspend enquanto estamos transitando entre views
- ( void ) syncNewViewWithApplicationState;

// Carrega as views do menu e os splashs
- ( void )loadMenu:( LoadingView* )hLoadingView;

// Carrega a view do jogo e a tela de pause
- ( void )loadGame:( LoadingView* )hLoadingView;

// Chamado quando uma thread de loading termina
- ( void )onLoadEnded:( LoadingView* )hLoadingView;

// Desaloca a memória alocada pela tela de jogo
- ( void )releaseGameView;

// Desaloca a memória alocada pelas telas dos menus
- ( void )releaseMenuViews;

// Retorna o nome do arquivo onde devemos salvar os dados do jogo
- ( NSString* )getSaveFileName;

//<DM>
- ( NSString* )getStateFileName;
- (void) scheduleLoadState;
//</DM>

#if DEBUG
	// Libera as views que foram alocadas em loadAll
- ( void )releasePreAllocatedViews;
#endif

#if APP_N_SOUNDS > 0

	// Carrega os sons da aplicação
	- ( bool ) initSounds;

	// Retira da memória os sons já carregados
	- ( void ) cleanSounds;

	// Retira da memória todos os sons utilizados apenas na tela de pause
	- ( void ) unloadPauseViewSounds;

#endif
	
#if APP_N_FONTS > 0

	// Carrega as fontes da aplicação
	- ( bool ) initFonts;

	// Libera a memória alocada pelas fontes da aplicação
	- ( void ) cleanFonts;

#endif

#if APP_N_TEXTS > 0

	// Carrega os textos da aplicação
	- ( bool ) initTexts;

	// Libera os textos alocados
	- ( void ) cleanTexts;

	// Adiciona um texto ao array de textos da aplicação
	- ( bool ) addText:( const char* )pText ToLanguage:( uint8 )languageIndex AtIndex:( uint8 )textIndex;

#endif

#if APP_N_RECORDS > 0

	// Carrega possíveis dados salvos
	- ( bool )loadRecords;

	// Salva os dados do jogo
	- ( bool )saveRecords;

#endif
//<DM>
// salva status de jogo (fase)
- ( bool ) saveGameState;
// carrega o status de jogo , se houver
- ( bool ) loadGameState;

- (bool) fileExists:(NSString *)fileName;

- (void) removeStateFile;
//</DM>
@end

// Implementação da classe
@implementation FreeKickAppDelegate

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hGLView, currLanguage;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/



- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	{ // Evita erros de compilação por causa dos gotos
		
#if DEBUG
		LOG_FREE_MEM( "Inicio da app" );
#endif
		
		// Inicializa os membros da superclasse
		[super applicationDidFinishLaunching: application];
		
		// Inicializa as variáveis da classe
		hGLView = NULL;
		hMainMenu = NULL;
		hSplashNano = NULL;
		hSplashGame = NULL;
		
		highScoreIndex = -1;
		currLanguage = LANGUAGE_INDEX_ENGLISH;
		
		// Inicializa os arrays da classe
#if ( APP_N_SOUNDS > 0 ) && SOUND_WITH_OPEN_AL
		memset( appSounds, 0, sizeof( AudioSource* ) * APP_N_SOUNDS );
#endif
#if ( APP_N_LANGUAGES > 0 ) && ( APP_N_TEXTS > 0 )
		memset( texts, 0, sizeof( char* ) * APP_N_LANGUAGES * APP_N_TEXTS );
#endif
#if APP_N_FONTS > 0
		memset( appFonts, 0, sizeof( Font* ) * APP_N_FONTS );
#endif
		// Carrega os recordes
#if APP_N_RECORDS > 0
		if( ![self loadRecords] )
			memset( records, 0, sizeof( uint32 ) * APP_N_RECORDS );
#endif
		
		// Cria o controlador de sons da aplicação
#if SOUND_WITH_OPEN_AL
		if( !AudioManager::Create() )
			goto Error;
		AudioManager::GetInstance()->suspend();
#else
		if( !AppleAudioManager::Create( APP_N_SOUNDS ) )
			goto Error;
		AppleAudioManager::GetInstance()->suspend();
#endif
		// Cria os singletons que irão controlar os eventos de toque e do acelerômetro
		if( !EventManager::Create() )
			goto Error;
	//	if( !AccelerometerManager::Create( ACCELEROMETER_UPDATE_INTERVAL ) )
//			goto Error;
//		// Define a posição de repouso do device
//		// sin 45 = 0.707106781186548
//		Point3f aux( -0.70710678f, 0.0f, -0.70710678f );
//		AccelerometerManager::GetInstance()->setRestPosition( &aux );
//		AccelerometerManager::GetInstance()->stopAccelerometers();
		
		// OLD
		// Cria a tela de seleção de idioma
		//    hLanguageView = ( LanguageView* )[self loadViewFromXib: "LanguageView" ];
		//    if( hLanguageView == NULL )
		//    {
		//        #if DEBUG
		//            LOG( @"FreeKickAppDelegate::applicationDidFinishLaunching : Unable to create view" );
		//        #endif
		//
		//        goto Error;
		//    }
		//
		//    // Exibe a tela de seleção de idioma
		//    [self performTransitionToView: VIEW_INDEX_SELECT_LANG];
#if DEBUG && ( IS_CURR_TEST( TEST_BARRIER_SHADOWS ) || IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_BALL_SHADOW ) || IS_CURR_TEST( TEST_BALL_TARGET_HIT )  )
		gameInfo.gameMode = GAME_MODE_TRAINING;
		[self performTransitionToView: VIEW_INDEX_LOAD_GAME];
#else
		[self performTransitionToView: VIEW_INDEX_LOAD_MENU];
#endif
		//<DM>
		[self setVolume: 0]; //desabilitar na hora de lancar!!!!
		LoadGameState=false;
		//</DM>
		return;
	} // Evita erros de compilação por causa dos gotos
	// Label de tratamento de erros
Error:
	[self quit: ERROR_ALLOCATING_VIEWS];
	return;
} 

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

- ( void )performTransitionToView:( uint8 )newIndex WithLoadingView:( const LoadingView* )hLoadindView
{
	// Não interrompe uma transição
	if( [hViewManager isTransitioning] )
		return;
	
	lastViewIndex = currViewIndex;

    // TASK : Fazer através de controlers e não diretamente das views
	UIView *hNextView;
	NSString *hTransition = nil, *hDirection = nil;
	
	bool keepCurrView = false, forceDraw = false, nullTransition = false;

	switch( newIndex )
	{
		// OLD
//		case VIEW_INDEX_SELECT_LANG:
//			hNextView = hLanguageView;
//			break;

		case VIEW_INDEX_LOAD_MENU:
			// OLD
//			[hLanguageView removeFromSuperview];
//			[hLanguageView release];
//			hLanguageView = NULL;

			[self startLoadingWithTarget: self AndSelector: @selector( loadMenu: ) CallingAfterLoad: @selector( onLoadEnded: )];
			currViewIndex = newIndex;
			return;

		case VIEW_INDEX_SPLASH_NANO:
			hNextView = hSplashNano;
			hSplashNano = NULL;

			hTransition = kCATransitionFade;
			break;
			
		case VIEW_INDEX_SPLASH_GAME:
			/*
			#if 0 && DEBUG && TARGET_IPHONE_SIMULATOR

				newIndex = VIEW_INDEX_MAIN_MENU;
				currViewIndex = VIEW_INDEX_SPLASH_GAME;
				SAFE_RELEASE( hSplashGame );

			#else
				hNextView = hSplashGame;
				hTransition = kCATransitionFade;
				break;

			#endif
			 */
		case VIEW_INDEX_MAIN_MENU:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				switch( currViewIndex )
				{
					case VIEW_INDEX_SPLASH_GAME:
						hSplashGame = NULL;
						forceDraw = true;
						
						// OBS: NÃO PODE TOCAR O SOM ANTES DE FAZER A TRANSIÇÃO QUE REMOVERÁ O VÍDEO DA TELA!!!! FAZÊ-LO CAUSA FLICKERING DE VÍDEO EM
						// VERSÕES DO IPHONEOS INFERIORES À 3.0!!!!
						// [hMainMenu onBecomeCurrentScreen] ficou responsável por tocar o som do menu
						//[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
						break;
						
					case VIEW_INDEX_RANKING:
						[( ( RankingView* )[[hViewManager subviews] objectAtIndex: 0] ) suspend];
						break;
						
					case VIEW_INDEX_OPTIONS:
						break;

					case VIEW_INDEX_GAME:
					case VIEW_INDEX_PAUSE_SCREEN:
						[self releaseGameView];
						// OBS: NÃO PODE TOCAR O SOM ANTES DE FAZER A TRANSIÇÃO QUE REMOVERÁ O VÍDEO DA TELA!!!! FAZÊ-LO CAUSA FLICKERING DE VÍDEO EM
						// VERSÕES DO IPHONEOS INFERIORES À 3.0!!!!
						// [hMainMenu onBecomeCurrentScreen] ficou responsável por tocar o som do menu
						//[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
						break;
				}

				if( hMainMenu == NULL )
					hMainMenu = ( MainMenuView* )[[ApplicationManager GetInstance] loadViewFromXib: "MainMenu" ];

				[hMainMenu onBeforeTransition];

				hNextView = ( UIView* ) hMainMenu;
				
				hMainMenu = NULL;

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;
			
		case VIEW_INDEX_PLAY_MENU:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				PlayMenuView* hPlayMenu = ( PlayMenuView* )[[ApplicationManager GetInstance] loadViewFromXib: "PlayMenu" ];
				[hPlayMenu onBeforeTransition: &gameInfo];

				hNextView = hPlayMenu;

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;

		case VIEW_INDEX_RANKING:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				if( currViewIndex == VIEW_INDEX_GAME )
				{
					[self releaseGameView];
					[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
				}

				RankingView* hRanking = ( RankingView* )[[ApplicationManager GetInstance] loadViewFromXib: "Ranking" ];
				[hRanking onBeforeTransition: highScoreIndex setRecords: records];
				
				hNextView = hRanking;

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;

		case VIEW_INDEX_PAUSE_SCREEN:
			#if DEBUG
				LOG_FREE_MEM( "Entrando na tela de pausa" );
			#endif

			// Suspende o processamento da tela de jogo, mantendo-a na memória
			[hGLView suspend];
			
			// Vamos manter a tela de jogo na memória
			keepCurrView = true;
			
			hTransition = kCATransitionMoveIn;
			hDirection = kCATransitionFromRight;
			
		case VIEW_INDEX_OPTIONS:
			{
//				OptionsView* hOptionsMenu = ( OptionsView* )[[ApplicationManager GetInstance] loadViewFromXib: "OptionsView" ];
//				[hOptionsMenu setStyle: static_cast< OptionsViewStyle >( newIndex - VIEW_INDEX_PAUSE_SCREEN )];
//				[hOptionsMenu setPrevScreen: currViewIndex];
//				[hOptionsMenu onBeforeTransition];
//
//				hNextView = hOptionsMenu;
			
				DGMainMenu* hdgteste = ( DGMainMenu* ) [[ ApplicationManager GetInstance ] loadViewFromXib:"DGMainMenu" ];
				hNextView = hdgteste;
			
			}
			break;
			
		case VIEW_INDEX_HELP:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				HelpView* hHelp = ( HelpView* )[[ApplicationManager GetInstance] loadViewFromXib: "HelpView" ];
				[hHelp onBeforeTransition];

				hNextView = hHelp;

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;
			
		case VIEW_INDEX_CREDITS:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				CreditsView* hCredits = ( CreditsView* )[[ApplicationManager GetInstance] loadViewFromXib: "CreditsView" ];
				[hCredits onBeforeTransition];

				hNextView = hCredits;

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;

		case VIEW_INDEX_LOAD_GAME:
			#if DEBUG
				if( ( currViewIndex == -1 ) || ( currViewIndex == VIEW_INDEX_SPLASH_NANO ) )
				{
					[self releaseMenuViews];
					[self startLoadingWithTarget: self AndSelector: @selector( loadGame: ) CallingAfterLoad: @selector( onLoadEnded: )];
					
					[self stopAudio];
					
					gameInfo.gameMode = GAME_MODE_TRAINING;

					highScoreIndex = -1;
					currViewIndex = newIndex;
					
					return;
				}
				else
			#endif
				{
					highScoreIndex = -1;

					hTransition = kCATransitionFade;
					hNextView = NULL;
					nullTransition = true;
				}
				break;

		case VIEW_INDEX_GAME:
			{
				#if DEBUG
					LOG_FREE_MEM( "Entrando na tela de jogo" );
				#endif
				
				#if DEBUG
				if( ( currViewIndex == VIEW_INDEX_LOAD_GAME ) || ( currViewIndex == VIEW_INDEX_PLAY_MENU ) )
				{
					[self releasePreAllocatedViews];
				#else
				if( currViewIndex == VIEW_INDEX_LOAD_GAME )
				{
				#endif
					
					hTransition = kCATransitionFade;
				}
				else if( currViewIndex == VIEW_INDEX_PAUSE_SCREEN )
				{
					// Retira da memória todos os sons utilizados apenas na tela de pause 
					[self unloadPauseViewSounds];
					
					// Renderiza a view do jogo sem o jogador ver, já utilizando as novas opções do menu. Assim
					// garantimos que ele não verá a imagem anterior durante a transição
					[hGLView render];
					
					hTransition = kCATransitionMoveIn;
					hDirection = kCATransitionFromLeft;
				}

				hNextView = hGLView;
			}
			break;
			
		default:
			#if DEBUG
				LOG( @"AppDelegate::performTransitionToView() - Invalid index" );
			#endif
			return;
	}
	
	// Verifica se conseguimos alocar a nova view
	if( ( hNextView == NULL ) && ( !nullTransition ) )
	{
		[self quit: ERROR_ALLOCATING_VIEWS];
		return;
	}

	if( hTransition != nil )
	{
		// Executa a transição de views		
		[hViewManager transitionFromSubview: [[hViewManager subviews] objectAtIndex: 0] toSubview: hNextView keepOldView: keepCurrView freeNewView: true transition: hTransition direction: hDirection duration: DEFAULT_TRANSITION_DURATION ];
	}
	else
	{
		#if APP_N_LANGUAGES > 1
			if( [hNextView respondsToSelector:@selector( setLanguage: ) ] && ( newIndex != VIEW_INDEX_GAME ) )
				[hNextView setLanguage: currLanguage ];
		#endif

		[hViewManager addSubview: hNextView];
		
		if( [hNextView respondsToSelector: @selector( onBecomeCurrentScreen )] )
			[hNextView onBecomeCurrentScreen];
		
		if( forceDraw )
		{
			[hViewManager bringSubviewToFront: hNextView];
			[hNextView drawRect: [[UIScreen mainScreen] bounds]];
			[hNextView setNeedsDisplay];
		}

		[hNextView release];

		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			
			if( hCurrView != hNextView )
			{
				if( keepCurrView )
					[hCurrView retain];

				[hCurrView removeFromSuperview];
			}
		}
		
		[self syncNewViewWithApplicationState];
	}

	// Atualiza o índice da view que está sendo exibida
	currViewIndex = newIndex;
}

/*==============================================================================================

MENSAGEM releasePreAllocatedViews
	Libera as views que foram alocadas em loadAll.

==============================================================================================*/

#if DEBUG

- ( void ) releasePreAllocatedViews
{
	[hSplashGame release];
	hSplashGame = NULL;

	[hSplashNano release];
	hSplashNano = NULL;
	
	[hMainMenu release];
	hMainMenu = NULL;
}

#endif

/*==============================================================================================

MENSAGEM releaseMenuViews
	Desaloca a memória alocada pelas telas dos menus.

==============================================================================================*/

- ( void ) releaseMenuViews
{
	int32 nSubviews = [[hViewManager subviews] count];
	while( nSubviews )
	{
		UIView* hView = [[hViewManager subviews] objectAtIndex: 0];
		[hView removeFromSuperview];
		--nSubviews;
	}
	
	// Retira todos os sons da memória. O jogo alocará o que precisar
	[self unloadSounds];

	hSplashNano = NULL;
	hSplashGame = NULL;
	hMainMenu = NULL;
}
	
/*==============================================================================================

MENSAGEM releaseGameView
	Desaloca a memória alocada pela tela de jogo.

==============================================================================================*/

- ( void ) releaseGameView
{
	if( hGLView != NULL )
	{
		[hGLView suspend];
		
		[self cleanFonts];
		
		#if DEBUG
			uint32 glViewRefCount = [hGLView retainCount];
		#endif
		
		if( [hGLView superview] != NULL )
			[hGLView removeFromSuperview];
		else 
			[hGLView release];

		hGLView = NULL;
	}
	
	// Retira todos os sons da memória. O menu alocará o que precisar
	[self unloadSounds];
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

//- ( void )transitionDidStart:( ViewManager* )hManager
//{
//}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )hManager
{
	switch( currViewIndex )
	{
		case VIEW_INDEX_SPLASH_GAME:
			[hSplashGame onBecomeCurrentScreen];

			// Já que o splash da nano não está mais visível e não o utilizaremos novamente,
			// retira-o da memória
			SAFE_RELEASE( hSplashNano );
			break;
			
		case VIEW_INDEX_LOAD_GAME:
			[self releaseMenuViews];
			
			[self stopAudio];
			
			[self startLoadingWithTarget: self AndSelector: @selector( loadGame: ) CallingAfterLoad: @selector( onLoadEnded: )];

			break;
			
		case VIEW_INDEX_GAME:
			#if DEBUG
			{
				NSArray* hSubviews = [hViewManager subviews];
				if( hSubviews )
					LOG( @"NSubviews: %d", [hSubviews count] );
			}
			#endif
	
			// Reinicia o processamento dos componentes do jogo
			[hGLView resume];
			break;
			
		case VIEW_INDEX_MAIN_MENU:
			// Já que o splash do jogo não está mais visível e não o utilizaremos novamente,
			// retira-o da memória
			SAFE_RELEASE( hSplashGame );

		case VIEW_INDEX_SPLASH_NANO:
		case VIEW_INDEX_PLAY_MENU:
		case VIEW_INDEX_RANKING:
		case VIEW_INDEX_PAUSE_SCREEN:
		case VIEW_INDEX_OPTIONS:
		case VIEW_INDEX_HELP:
		case VIEW_INDEX_CREDITS:
			{
				NSArray* hSubviews = [hViewManager subviews];
				if( [hSubviews count] > 0 )
				{
					UIView *hCurrView = [hSubviews objectAtIndex:0];
					if( [hCurrView respondsToSelector: @selector( onBecomeCurrentScreen )] )
						[hCurrView onBecomeCurrentScreen];
				}
			}
			break;
	}
	
	[self syncNewViewWithApplicationState];
}

/*==============================================================================================

MENSAGEM syncNewViewWithApplicationState
	Sincrozina o estado da view atual com o estado da aplicação. Este método serve para contornar
a possibilidade de recebermos um evento de suspend enquanto estamos transitando entre views.

==============================================================================================*/
	
- ( void ) syncNewViewWithApplicationState
{
	if( [self getApplicationState] == APPLICATION_STATE_SUSPENDED )
	{
		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			if( [hCurrView respondsToSelector: @selector( suspend )] )
				[hCurrView suspend];
		}
	}
}

/*==============================================================================================

MENSAGEM transitionDidCancel:
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )hManager
{
	// TODOO : Confirmar se a condição do if é testada desse jeito
	// Se estávamos saindo da tela de jogo, retoma o processamento dos componentes
	if( ( currViewIndex == VIEW_INDEX_MAIN_MENU ) && ( hGLView != NULL ) )
		[hGLView resume];
}

/*==============================================================================================

MENSAGEM loadGame:
	Carrega a view do jogo e a tela de pause.

==============================================================================================*/

- ( void )loadGame:( LoadingView* )hLoadingView
{
	// OBS: Antigamente deixava a view de Pause carregada, o que correspondia a 6,92% do loading
	[hLoadingView changeProgress: 0.0692f];

	// Cria a view do jogo (view OpenGL)
	hGLView = [[EAGLView alloc] initWithFrame: [[UIScreen mainScreen] bounds]];

	if( hGLView == NULL )
	{
		[hLoadingView setError: ERROR_ALLOCATING_VIEWS];
		return;
	}

	// Só podemos alocar as fontes quando o OpenGL está inicializado
	#if ( APP_N_FONTS > 0 ) 
		if( ![self initFonts] )
		{
			[self releaseGameView];
			[hLoadingView setError: ERROR_ALLOCATING_DATA];
			return;
		}
	#endif
	///<DM>
	if (LoadGameState)
	{
		[self loadGameState];	
	}
	///</DM>
	//GameScreen* pCurrScene = new GameScreen( gameInfo, hLoadingView,LoadGameState );
	///<DM>
	LoadGameState=false;
	///</DM>*/
	
	//descomentar para carregar a minha cena
	GInfo.nPlayers = 2;
	GInfo.nomes[0] = "seu";
	GInfo.nomes[1] = "sua";
	GInfo.mode = DOT_GAME_MODE_SOLO;
	GInfo.idBg = ID_BGD_FAZENDEIRO;
	GInfo.nHZones = 6; 
	GInfo.nVZones = 5;
	DotGame* pCurrScene = new DotGame( GInfo, hLoadingView );
	
		
	if( pCurrScene == NULL )
	{
		[self releaseGameView];
		[hLoadingView setError: ERROR_ALLOCATING_VIEWS];
		return;
	}

	[hGLView setCurrScene: pCurrScene];

	#if APP_N_LANGUAGES > 1	
		if( currViewIndex == VIEW_INDEX_MAIN_MENU )
		{
			// Realiza as possíveis modificações decorrentes da seleção de idioma
			[hGLView setLanguage: currLanguage];
		}
	#endif

	[hLoadingView setProgress: 1.00f];
}

/*==============================================================================================

MENSAGEM loadMenu:
	Carrega as views do menu e os splashs.

==============================================================================================*/

-( void )loadMenu:( LoadingView* )hLoadingView
{
	// Carrega os textos, os sons e as fontes do jogo
	#if ( APP_N_TEXTS > 0 ) 
		if( ![self initTexts] )
		{
			[hLoadingView setError: ERROR_ALLOCATING_DATA];
			return;
		}
	#endif
	
	#if ( APP_N_SOUNDS > 0 ) 
		if( ![self initSounds] )
		{
			[hLoadingView setError: ERROR_ALLOCATING_DATA];
			return;
		}
	#endif
	
	[hLoadingView setProgress: 0.2f];
	
	// Cria as views dos splashs
	ApplicationManager* hAppManager = [ApplicationManager GetInstance];
	hSplashNano = ( SplashNano* )[hAppManager loadViewFromXib: "SplashNano" ];
	if( hSplashNano == NULL )
	{
		[hLoadingView setError: ERROR_ALLOCATING_VIEWS];
		return;
	}	
	[hLoadingView setProgress: 0.2f];
	
	//hMainMenu = (DotGameMainMenuView*)[[ApplicationManager GetInstance] loadViewFromXib:""];
	hMainMenu = ( MainMenuView* )[[ApplicationManager GetInstance] loadViewFromXib: "MainMenu" ];
	if( hMainMenu == NULL )
	{
		[hLoadingView setError: ERROR_ALLOCATING_VIEWS];
		return;
	}
	[hLoadingView setProgress: 0.9f];

	hSplashGame = [[SplashGame alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
	if( hSplashGame == NULL )
	{
		[hLoadingView setError: ERROR_ALLOCATING_VIEWS];
		return;
	}
	[hLoadingView setProgress: 1.0f];
}

/*==============================================================================================

MENSAGEM onLoadEnded:
	Chamado quando uma thread de loading termina.

==============================================================================================*/
	
-( void )onLoadEnded:( LoadingView* )hLoadingView
{
	int16 errorCode;
	if( ( errorCode = [hLoadingView getError] ) != ERROR_NONE )
	{
		[self quit: errorCode];
		return;
	}

	switch( currViewIndex )
	{
		case VIEW_INDEX_LOAD_MENU:
			[self performTransitionToView: VIEW_INDEX_SPLASH_NANO];
			break;

		case VIEW_INDEX_LOAD_GAME:
			[self performTransitionToView: VIEW_INDEX_GAME];
			break;
	}
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{	
	[super dealloc];
}

/*==============================================================================================

MENSAGEM applicationWillResignActive
	Mensagem chamada quando a aplicação vai ser suspensa.

==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	[super applicationWillResignActive: application];

	// Pára de tocar sons
	#if SOUND_WITH_OPEN_AL
		AudioManager::GetInstance()->suspend();
	#else
		AppleAudioManager::GetInstance()->suspend();
	#endif
	
	// Pára os acelerômetros
	AccelerometerManager::GetInstance()->suspend();

	// Pára de renderizar e atualizar as views e scenes do jogo
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( suspend )] )
			[hCurrView suspend];
	}
}

/*==============================================================================================

MENSAGEM applicationDidBecomeActive
	Mensagem chamada quando a aplicação é reiniciada.

==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	[super applicationDidBecomeActive: application];

	// Volta a executar os sons
	#if SOUND_WITH_OPEN_AL
		AudioManager::GetInstance()->resume();
	#else
		AppleAudioManager::GetInstance()->resume();
	#endif
	
	// Reinicia os acelerômetros
	AccelerometerManager::GetInstance()->resume();

	// Reinicia a renderização e a atualização das views e scenes do jogo
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( resume )] )
			[hCurrView resume];
	}
}

/*==============================================================================================

MENSAGEM applicationWillTerminate
	Tells the delegate when the application is about to terminate. This method is optional. This
method is the ideal place for the delegate to perform clean-up tasks, such as freeing allocated
memory, invalidating timers, and storing application state.

==============================================================================================*/

- ( void )applicationWillTerminate:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationWillTerminate" );
#endif

	
	// Reabilita o modo sleep
	[[UIApplication sharedApplication] setIdleTimerDisabled: NO];

	// Suspende o processamento da tela atual
	[self applicationWillResignActive: application];
	
	// Salva os recordes
	#if APP_N_RECORDS > 0
		[self saveRecords];
	#endif

	// Desaloca as telas da aplicação
	if( currViewIndex != VIEW_INDEX_SPLASH_NANO )
	{
		SAFE_RELEASE( hSplashNano );
	}
	else if( currViewIndex != VIEW_INDEX_SPLASH_GAME )
	{
		SAFE_RELEASE( hSplashGame );
	}
	else if( currViewIndex != VIEW_INDEX_MAIN_MENU )
	{
		SAFE_RELEASE( hMainMenu );
	}
	else if( currViewIndex != VIEW_INDEX_GAME )
	{
		SAFE_RELEASE( hGLView );
	}
	
	if( currViewIndex == VIEW_INDEX_GAME )
	{
		[hGLView setCurrScene: NULL];
	}

	// OLD : hViewManager será desalocado automaticamente ao quando abandonarmos a aplicação, já que o criamos quando carregamos MainWindow.xib
//	// Desaloca o controlador de telas
//	// OBS: Não utiliza SAFE_RELEASE pois obrigatoriamente teremos hViewManager, já que este é criado quando carregamos MainWindow.xib
//	KILL( hViewManager );
	
	// Desaloca os singletons
	AccelerometerManager::Destroy();

	#if SOUND_WITH_OPEN_AL
		AudioManager::Destroy();
	#else
		AppleAudioManager::Destroy();
	#endif
	
	EventManager::Destroy();
	
	// Desaloca os textos, sons e fontes da aplicação
	#if APP_N_TEXTS > 0
		[self cleanTexts];
	#endif
	
	#if APP_N_SOUNDS > 0
		[self cleanSounds];
	#endif
	
	#if APP_N_FONTS > 0
		[self cleanFonts];
	#endif

	// OLD : hWindow será desalocado automaticamente ao quando abandonarmos a aplicação, já que o criamos quando carregamos MainWindow.xib
//	// Desaloca a janela da aplicação
//	// OBS: Não utiliza SAFE_RELEASE pois obrigatoriamente teremos hWindow, já que este é criado quando carregamos MainWindow.xib
//	KILL( hWindow );
}

/*==============================================================================================

MENSAGEM applicationDidReceiveMemoryWarning
	Tells the delegate when the application receives a memory warning from the system. This
method is optional. In this method, the delegate tries to free up as much memory as possible.
After the method returns (and the delegate then returns from applicationWillTerminate), the
application is terminated.

==============================================================================================*/

- ( void )applicationDidReceiveMemoryWarning:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationDidReceiveMemoryWarning" );
#endif
	
	// OBS: Não vamos abortar a aplicação pois recebemos MemoryWarnings mesmo quando ainda temos uma
	// quantidade razoável de memória
	//[self quit: ERROR_MEMORY_WARNING];
}

/*==============================================================================================

MENSAGEM applicationSignificantTimeChange
	Tells the delegate when there is a significant change in the time. This method is optional.
Examples of significant time changes include the arrival of midnight, an update of the time by
a carrier, and the change to daylight savings time. The delegate can implement this method to
adjust any object of the application displays time or is sensitive to time changes.

==============================================================================================*/

- ( void )applicationSignificantTimeChange:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationSignificantTimeChange" );
#endif
}

/*==============================================================================================

MENSAGEM getText
	Retorna um texto da aplicação.

==============================================================================================*/

- ( char* ) getText: ( uint16 ) textIndex
{
	#if APP_N_TEXTS > 0
		return texts[ currLanguage ][ textIndex ];
	#else
		return NULL;
	#endif
}

/*==============================================================================================

MENSAGEM getFont
	Retorna uma fonte da aplicação.

==============================================================================================*/

- ( Font* ) getFont:( uint8 )fontIndex
{
	#if APP_N_FONTS > 0
		return appFonts[ fontIndex ];
	#else
		return NULL;
	#endif
}

/*==============================================================================================

MENSAGEM getiPhoneFont
	Retorna a fonte embutida que mais se aproxima da fonte utilizada na aplicação.

==============================================================================================*/

- ( UIFont* ) getiPhoneFont
{
	return [UIFont fontWithName: @"Marker Felt" size: 24.0f];
}

/*==============================================================================================

MENSAGEM playAudioNamed:AtIndex:Looping:
	Inicia a reprodução de um som da aplicação.

==============================================================================================*/
	
- ( void ) playAudioNamed:( uint8 )audioName AtIndex:( uint8 ) audioIndex Looping:( bool )looping
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL
			// Ignora audioName pois os sons foram carregados em initSounds
			appSounds[ audioIndex ]->setLooping( looping );
			appSounds[ audioIndex ]->play();
		#else
			AppleAudioManager::GetInstance()->playSound( audioName, audioIndex, looping, true );
		#endif
	#endif
}

/*==============================================================================================

MENSAGEM stopAudio
	Pára de reproduzir todos os sons.

==============================================================================================*/
					
- ( void ) stopAudio
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL
			for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
			{
				if( appSounds[i]->getState() == AL_PLAYING )
					appSounds[i]->stop();
			}
		#else
			AppleAudioManager::GetInstance()->stopAllSounds();
		#endif
	#endif
}
	
/*==============================================================================================

MENSAGEM stopAudioAtIndex
	Pára de reproduzir o som indicado.

==============================================================================================*/
					
- ( void ) stopAudioAtIndex: ( uint8 )index
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL
			if( appSounds[ index ]->getState() == AL_PLAYING )
				appSounds[ index ]->stop();
		#else
			AppleAudioManager::GetInstance()->stopSound( index );
		#endif
	#endif
}

/*==============================================================================================

MENSAGEM pauseAllSounds:
	(Des)Pausa todos os sons que estão sendo executados no momento.

==============================================================================================*/

- ( void ) pauseAllSounds: ( bool )pause
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL
			for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
				appSounds[i]->pause();
		#else
			AppleAudioManager::GetInstance()->pauseAllSounds( pause );
		#endif
	#endif
}

/*==============================================================================================

MENSAGEM isPlayingAudioWithIndex:
	Indica se está tocando um som específico.

==============================================================================================*/

- ( bool )isPlayingAudioWithIndex:( uint8 )index
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL

			return appSounds[ index ]->getState() == AL_PLAYING;

		#else

			return AppleAudioManager::GetInstance()->isPlaying( index );

		#endif
	#else
		return false;
	#endif
}

/*==============================================================================================

MENSAGEM isPlayingAudio
	Indica se a aplicação está tocando algum som.

==============================================================================================*/

- ( bool )isPlayingAudio
{
	#if APP_N_SOUNDS > 0
		for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
		{
			if( [self isPlayingAudioWithIndex: i] )
				return true;
		}
		return false;
	#else
		return false;
	#endif
}

/*==============================================================================================

MENSAGEM getAudioVolume
	Retorna o percentual do volume máximo utilizado na reprodução de sons.

==============================================================================================*/

- ( float )getAudioVolume
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL
			float volume = 1.0f;
			AudioManager::GetInstance()->getPlaybackDevice()->getAudioListener()->getGain( &volume );
			return volume;
		#else
			return AppleAudioManager::GetInstance()->getVolume();
		#endif
	#else
		return -1.0f;
	#endif
}

/*==============================================================================================

MENSAGEM setVolume
	Determina o volume dos sons da aplicação.

==============================================================================================*/

- ( void )setVolume:( float )volume
{
	#if APP_N_SOUNDS > 0
		#if SOUND_WITH_OPEN_AL
			AudioManager::GetInstance()->getPlaybackDevice()->getAudioListener()->setGain( volume );
		#else
			AppleAudioManager::GetInstance()->setVolume( volume );
		#endif
	#endif
}

/*==============================================================================================

MENSAGEM unloadSounds
	Retira da memória os sons já carregados.

==============================================================================================*/

#if APP_N_SOUNDS > 0

-( void ) unloadSounds
{
	#if SOUND_WITH_OPEN_AL
		// OBS: Este método ainda não está implementado para OpenAl
		assert( 0 );
	#else
		for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
			[self unloadSoundAtIndex :i];
	#endif
}
	
///<DM>
	-( void ) unloadSoundAtIndex: (int)index
	{
#if SOUND_WITH_OPEN_AL
		// OBS: Este método ainda não está implementado para OpenAl
		assert( 0 );
#else
		AppleAudioManager::GetInstance()->unloadSound( index );
#endif
	}
	
#endif
///</DM>	


	
	
/*==============================================================================================

MENSAGEM unloadPauseViewSounds
	Retira da memória todos os sons utilizados apenas na tela de pause.

==============================================================================================*/
	
#if APP_N_SOUNDS > 0
	
- ( void ) unloadPauseViewSounds
{
	#if SOUND_WITH_OPEN_AL
		// OBS: Este método ainda não está implementado para OpenAl
		assert( 0 );
	#else

		#define PAUSE_EXCLUSIVE_SOUNDS 4
		
			uint8 soundsToUnload[ PAUSE_EXCLUSIVE_SOUNDS ] = {
															   SOUND_INDEX_TAG_3_INCOMING,
															   SOUND_INDEX_TAG_4_INCOMING,
															   SOUND_INDEX_HAIR_MOVE,
															   SOUND_INDEX_PAUSE_SAMBA
															 };

			for( uint8 i = 0 ; i < PAUSE_EXCLUSIVE_SOUNDS ; ++i )
				AppleAudioManager::GetInstance()->unloadSound( soundsToUnload[i] );
		
		#undef PAUSE_EXCLUSIVE_SOUNDS

	#endif
}
	
#endif

/*==============================================================================================

MENSAGEM initSounds
	Carrega os sons da aplicação.

==============================================================================================*/

#if APP_N_SOUNDS > 0

- ( bool ) initSounds
{
#if SOUND_WITH_OPEN_AL

	memset( appSounds, 0, sizeof( AudioSource* ) * APP_N_SOUNDS );
	
	// Obtém o device de reprodução de sons
	PlaybackAudioDevice* pDevice = AudioManager::GetInstance()->getPlaybackDevice();
	pDevice->setDistanceModel( AL_LINEAR_DISTANCE );
	
	// Configura a posição do listener
	Point3f listenerPos;
	AudioListener* pListener = pDevice->getAudioListener();
	pListener->setPosition( &listenerPos );
	pListener->setGain( 1.0f );

	// Cria um novo contexto
	AudioContext* pCurrContext = pDevice->newAudioContext();
	if( !pCurrContext )
		return false;

	pCurrContext->makeCurrentContext();

	// Aloca os sons
#if APP_N_SOUNDS > 9
	#error Se APP_N_SOUNDS for maior que 9, o looping abaixo deve ser modificado
#endif

	char soundPath[] = { ' ', '\0' };
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
	{
		soundPath[0] = '0' + i;
		appSounds[i] = Utils::GetAudioSource( pCurrContext, soundPath, "caf" );
		
		if( appSounds[i] == NULL )
		{
			[self cleanSounds];
			return false;
		}
		appSounds[i]->setPosition( &listenerPos );
		appSounds[i]->setMinGain( 1.0f );
		appSounds[i]->setMaxGain( 1.0f );
	}
	
	appSounds[ SOUND_INDEX_SPLASH ]->setLooping( true );

#endif

	return true;
}

#endif

/*==============================================================================================

MÉTODO initFonts
	Carrega as fontes da aplicação.

==============================================================================================*/

#if APP_N_FONTS > 0

-( bool ) initFonts
{
	memset( appFonts, 0, sizeof( Font* ) * APP_N_FONTS );

	appFonts[ 0 ] = new GoodGirlFont();
	if( !appFonts[ 0 ] )
	{
		[self cleanFonts];
		return false;
	}
	
	return true;
}

#endif


	
/*==============================================================================================

MENSAGEM initTexts
	Carrega os textos da aplicação.

==============================================================================================*/

#if APP_N_TEXTS > 0

- ( bool ) initTexts
{
	{ // Evita problemas com os gotos

		// Inglês
		
		// OLD
//		if( ![self addText: "HELP\n\nBrazooka Soccer is a freekick challenge game where levels become more difficult as you move forward. In each challenge you'll have 3 chances to meet the level score, being chances points cumulative and variable depending on the kick result. Possibilities are:\n\n- : 100 pts\n- : 200 pts\n- : 300 pts\n\nUnused chances give you the max punctuation for a shot. If you don't meet the score needed, it's game over.\n\nA kick is divided into 3 steps:\n\n- 1st Step: Before getting into action, you can freely observe the current challenge moving and zooming the camera. This way you can calculate the best kick settings for the next two steps without time limits.\n\n- 2nd Step: Here you have to set the kick direction and power. Direction mesures how your kick destination will get farther from the center of the goal. Power defines the ball speed, affecting also the distance it will travel. Beware of the time limit.\n\n- 3rd Step: In this step you choose where your kick will hit the ball, defining the ball trajectory and distance from the ground. The lower you aim, the higher the ball gets. The more to the left or to the right, the more the ball trajectory will change from straight to curve. Again, beware of the time limit.\n\nControls:\n\n- Pause Button:\n\nPauses the game and shows the pause menu.\n\n- Kick Button:\n\nConfirms your kick step settings.\n\n- Info Button:\n\nShows your play info: total points, current level, score needed for this level and remaining tries.\n\n- Go to Ball Button:\n\nFocus the camera at the ball.\n\n- Before pressing kick for the 1st time:\n\nYou can navigate through the soccer field with swipe gestures and zoom in / zoom out your view with pincer gestures. In training mode you can also drag and drop the soccer ball and the goal target.\n\n- Setting Power and Direction:\n\nTilt your iPhone or iPodTouch up or down to set the kick power and left or right to set the kick direction.\n\n- Setting Effect and Height:\n\nTilt your iPhone or iPodTouch to aim the kick at the ball.\n\nVersion 1.0" ToLanguage: LANGUAGE_INDEX_ENGLISH AtIndex: TEXT_INDEX_HELP] )
//			goto Error;
		
		if( ![self addText: "HELP\n\nObjective:\n\nBrazooka Soccer is a free kick challenge game, where you must show power and skills to hit the target indicated at the goal. In each challenge you have 3 shots to meet the level score. The closer to the target your shot gets, the more points you score. You also receive points if you score a goal or hit the post, even when not hitting the target. On the other hand, if your shot goes out of bounds or hits the barrier, your shot score is reduced. The maximum score per shot is 250 points.\n\nIf you achieve the score target with less than 3 shots, you receive a bonus for each unused shot. If you can't achieve the score target after the 3 shots, it's game over for you!\n\nShooting:\n\nA kick is divided into 3 steps:\n\n- 1st Step: Before getting into action, you can freely observe the current challenge moving and zooming the camera. This way you can calculate the best kick settings for the next two steps without time limits.\n\n- 2nd Step: Here you have to set the kick direction and power. Direction measures how your kick destination will get farther from the center of the goal. Power defines the ball speed, affecting also the distance it will travel. Beware of the time limit.\n\n- 3rd Step: In this step you choose where your kick will hit the ball, defining the ball trajectory and distance from the ground. The lower you aim, the higher the ball gets. The more to the left or to the right, the more the ball trajectory will change from straight to curve. Again, beware of the time limit.\n\nOther controls:\n\n- Pause Button:\n\nPauses the game and shows the pause menu.\n\n- Kick Button:\n\nConfirms your kick step settings.\n\n- Info Button:\n\nShows your play info: total points, current level, score needed for this level and remaining tries.\n\n- Go to Ball Button:\n\nFocus the camera at the ball.\n\n- Moving the camera:\n\nBefore you begin a new shot, you can navigate through the soccer field with swipe gestures and zoom in / out your view with pincer gestures. In training mode you can also drag and drop the soccer ball and the goal target.\n\n- Setting Power and Direction:\n\nTilt your iPhone or iPod Touch up or down to set the kick power and left or right to set the kick direction.\n\n- Setting Effect and Height:\n\nTilt your iPhone or iPod Touch to aim the kick at the ball.\n\nVersion 1.0" ToLanguage: LANGUAGE_INDEX_ENGLISH AtIndex: TEXT_INDEX_HELP] )
			goto Error;
		
		if( ![self addText: "TRAINING MODE\n\nTrain before you try the long run. In this mode you can set your freekick and goal target positions, so you can practice the hardest shots." ToLanguage: LANGUAGE_INDEX_ENGLISH AtIndex: TEXT_INDEX_GAME_MODE_TRAINING] )
			goto Error;

		if( ![self addText: "ENDLESS MODE\n\nThis is the real thing. In each freekick challenge you'll have 3 chances to meet a certain score. If so, you keep moving forward, otherwise, it's game over. This isn't everything: every new level becomes harder. How many points can you get?" ToLanguage: LANGUAGE_INDEX_ENGLISH AtIndex: TEXT_INDEX_GAME_MODE_RANKING] )
			goto Error;
		
		return true;

	} // Evita problemas com os gotos

	// Label de tratamento de erros
	Error:
		[self cleanTexts];
		return false;
}

#endif

/*==============================================================================================

MENSAGEM addText
	Adiciona um texto ao array de textos da aplicação.

==============================================================================================*/

#if APP_N_TEXTS > 0

#define MAX_TEXT_LEN 2500

-( bool ) addText:( const char* )pText ToLanguage:( uint8 )languageIndex AtIndex:( uint8 )textIndex
{	
	char buffer[ MAX_TEXT_LEN ];
	snprintf( buffer, MAX_TEXT_LEN, "%s", pText );
	
	// +1 = Espaço extra para conter o '\0' que termina a string
	int32 len = strlen( buffer ) + 1;
	
	texts[ languageIndex ][ textIndex ] = new  char[ len ];
	if( !texts[ languageIndex ][ textIndex ] )
		return false;

	snprintf( texts[ languageIndex ][ textIndex ], len, "%s", buffer );
	
//	#if DEBUG
//		LOG( @"Language %d, TextIndex %d = %s", languageIndex, textIndex, texts[ languageIndex ][ textIndex ] );
//	#endif
	
	return true;
}

#undef MAX_TEXT_LEN

#endif

/*==============================================================================================

MENSAGEM cleanTexts
	Libera os textos alocados.

==============================================================================================*/

#if APP_N_TEXTS > 0

- ( void ) cleanTexts
{
	for( uint8 i = 0 ; i < APP_N_LANGUAGES ; ++i )
	{
		for( uint8 j = 0 ; j < APP_N_TEXTS ; ++j )
		{
			delete[] texts[ i ][ j ];
			texts[ i ][ j ] = NULL;
		}
	}
}

#endif

/*==============================================================================================

MENSAGEM cleanSounds
	Libera os sons alocados.

==============================================================================================*/

#if APP_N_SOUNDS > 0

- ( void ) cleanSounds
{
	// OBS: Esse trabalho é feito automaticamente por AudioManager
//	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
//	{
//		delete appSounds[ i ];
//		appSounds[ i ] = NULL;
//	}
}

#endif

/*==============================================================================================

MÉTODO cleanFonts
	Libera a memória alocada pelas fontes da aplicação.

==============================================================================================*/

#if APP_N_FONTS > 0

- ( void ) cleanFonts
{
	for( uint8 i = 0 ; i < APP_N_FONTS ; ++i )
	{
		delete appFonts[ i ];
		appFonts[ i ] = NULL;
	}
}

#endif

/*==============================================================================================

MENSAGEM onGameOver
	Exibe a tela de fim de jogo.

==============================================================================================*/

- ( void ) onGameOver: ( int32 )score
{
	if( [self saveScoreIfRecord: score] )
		[self performTransitionToView: VIEW_INDEX_RANKING];
	else
		[self performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM saveScoreIfRecord
	Salva a pontuação atual caso esta seja um recorde.

==============================================================================================*/

-( bool ) saveScoreIfRecord: ( int32 )score
{
	#if APP_N_RECORDS > 0
	
		highScoreIndex = [self setHighScore: score];
		return highScoreIndex >= 0;
	
	#else
	
		return false;

	#endif
}

/*==============================================================================================

MENSAGEM isHighScore
	Retorna o lugar da pontuação na tabela de recordes ou um número negativo caso a jogador
não tenha alcançado um valor que supere o menor recorde.

==============================================================================================*/

- ( int8 ) isHighScore: ( int32 )score
{
	#if APP_N_RECORDS > 0
	
		if( score > 0 )
		{
			uint32 aux = static_cast< uint32 >( score );

			for( int32 i = 0; i < APP_N_RECORDS ; ++i )
			{
				if( aux > records[ i ] )
					return i;
			}
		}
		return -1;		
	
	#else
		return -1;
	#endif
}

/*==============================================================================================

MENSAGEM setHighScore
	Armazena a pontuação caso esta seja um recorde.

==============================================================================================*/

-( int8 )setHighScore: ( int32 )score
{
	#if APP_N_RECORDS > 0

		if( score > 0 )
		{
			uint32 aux = static_cast< uint32 >( score );

			for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
			{
				if( aux > records[ i ] )
				{
					for( uint8 j = APP_N_RECORDS - 1 ; j > i ; --j )
						records[ j ] = records[ j - 1 ];
					
					records[ i ] = aux;

					[self saveRecords];
					
					return i;
				}
			}
		}
		return -1;

	#else
		return -1;
	#endif
}

/*==============================================================================================

MENSAGEM getSaveFileName
	Retorna o nome do arquivo onde devemos salvar os dados do jogo.

==============================================================================================*/

-( NSString* )getSaveFileName
{
	return @"sv0.sv";
}

//<DM>
/*==============================================================================================
 
 MENSAGEM getStateFileName
 Retorna o nome do arquivo onde esta o estado da ultima partida.
 
 ==============================================================================================*/

-( NSString* )getStateFileName
{
	return @"st0.st";
}	

	- (bool) fileExists: (NSString *)aFileName
	{
//		LOG(@"filename to check:%s",NSSTRING_TO_CHAR_ARRAY(aFileName));
		NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
		if( hDocumentsDirectory == NULL )	
		return false;
	//	LOG(@"filename to check:%s",NSSTRING_TO_CHAR_ARRAY(hDocumentsDirectory));
		NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: aFileName];
	//	LOG(@"filename to check:%s",NSSTRING_TO_CHAR_ARRAY(hFilePath));		
		
		if( ![[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
		{
	//	LOG(@"file doesnt exists");
		return false;	
		}

	//	LOG(@"file exists");
		return true;
	}	
//</DM>
	
/*==============================================================================================

MENSAGEM loadRecords
	Carrega possíveis dados salvos.

==============================================================================================*/

#if APP_N_RECORDS > 0

-( bool )loadRecords
{
	// Obtém o nome do arquivo de save
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	NSString* hFileName = [self getSaveFileName];
	NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
	if (![self fileExists:hFileName]) 
		return false;
	
	// Carrega os recordes salvos
	FILE *pFile = fopen( [hFilePath UTF8String], "r" );
	if( pFile == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not open file %s for reading", [hFilePath UTF8String] );
		#endif

		return false;
	}
	
	bool ret = true;
	uint32 nRecordsLoaded = 0;

	if( ( nRecordsLoaded = fread( records, sizeof( uint32 ), APP_N_RECORDS, pFile ) ) < APP_N_RECORDS )
	{
		#if DEBUG
			LOG( @"ERROR: Could load only the first %d records", nRecordsLoaded );
		#endif
		
		ret = false;
	}

	fclose( pFile );

	return ret;
}

#endif

/*==============================================================================================

MENSAGEM saveRecords
	Salva os dados do jogo.

==============================================================================================*/

#if APP_N_RECORDS > 0

-( bool )saveRecords
{
	// Obtém o nome do arquivo de save
	NSString* hFileName = [self getSaveFileName];
	
	// Verifica se possuímos um diretório onde podemos criar arquivos
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
		#endif

		return false;
	}

	// OBS : O ideal seria verificar se os recordes atuais são diferentes dos recordes existentes no início desta execução. Para
	// isso, teríamos que criar um array uint32 recordsAtLastRun[ APP_N_RECORDS ] e copiar nele os dados de 'records' em applicationDidFinishLaunching

	// Verifica se existe algum recorde diferente de 0
	uint8 i;
	for( i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		if( records[i] != 0 )
			break;
	}

	if( i >= APP_N_RECORDS )
		return true;
	
	// Monta o nome do arquivo onde iremos salvar os recordes
	NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
	
	// Abre / cria o arquivo
	FILE* pFile = fopen( [hFilePath UTF8String], "w" );
	if( pFile == NULL )
	{
		#if DEBUG
			if( [[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
				LOG( @"ERROR: Could not open file %s for writing", [hFilePath UTF8String] );
			else
				LOG( @"ERROR: Could not create file %s", [hFilePath UTF8String] );
		#endif

		return false;
	}
	
	bool ret = true;
	uint32 nRecordsSaved = 0;

	if( ( nRecordsSaved = fwrite( records, sizeof( uint32 ), APP_N_RECORDS, pFile ) ) < APP_N_RECORDS )
	{
		#if DEBUG
			LOG( @"ERROR: Could save only the first %d records", nRecordsSaved );
		#endif
		
		ret = false;
	}

	// Fecha o arquivo
	fclose( pFile );

	return ret;
}

#endif
//<DM>
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////NAO CONFUNDA!!!!/////////////////////////////////
- (void ) scheduleLoadState
	{
		LOG(@"load game schedulled");
		LoadGameState=true;
	}
	
- (bool ) loadGameState
	{	
		int NumBarrierPlayers=0;
		int tmpPlayer;
		
	//	LOG( @"loadGameState" );	
		//dados a se obter do arquivo para posterior decodificacao.
		// Obtém o nome do arquivo de save
		NSString* hFileName = [self getStateFileName];
		
		// Verifica se possuímos um diretório onde podemos criar arquivos
		NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
		if( hDocumentsDirectory == NULL )
		{
#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
#endif
			
			return false;
		}
		
		// Verifica se já salvamos recordes 
		NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
		if( ![[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
			return false;
		
		// Carrega os recordes salvos
		FILE *pFile = fopen( [hFilePath UTF8String], "r" );
		if( pFile == NULL )
		{

//			LOG( @"WARNING: Invalid state file - OK, maybe there is no state file and the user is running is for the first time ", [hFilePath UTF8String] );
			return false;
		}
		
		uint32 nRecordsLoaded = 0;

		if( ( nRecordsLoaded = fread( &gameInfo, sizeof( gameInfo ), 1 , pFile ) ) < 1 )
		{
#if DEBUG
			LOG( @"ERROR: Could load only the first %d state information", nRecordsLoaded );
#endif
			return false;
			
		}

		

//		LOG(@"Got the game state: level %d try %d ",gameInfo.currLevel,gameInfo.currTry);

		fclose( pFile );		
		return true;
	}
	
- (bool ) saveGameState
	{
		
	//	LOG( @"saveGameState" );	
		//dados a se gravar no arquivo para posterior decodificacao.
		// Obtém o nome do arquivo de save
		NSString* hFileName = [self getStateFileName];
		
		// Verifica se possuímos um diretório onde podemos criar arquivos
		NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
		if( hDocumentsDirectory == NULL )
		{
#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
#endif
			
			return false;
		}
		
		NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
		
		// Abre / cria o arquivo
		FILE* pFile = fopen( [hFilePath UTF8String], "w" );
		if( pFile == NULL )
		{
#if DEBUG
			if( [[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
				LOG( @"ERROR: Could not open file %s for writing", [hFilePath UTF8String] );
			else
				LOG( @"ERROR: Could not create file %s", [hFilePath UTF8String] );
#endif
			
			return false;
		}
		
		uint32 nRecordsSaved = 0;		

		if( ( nRecordsSaved = fwrite( &gameInfo, sizeof( gameInfo ), 1, pFile ) ) < 1 )
		{
#if DEBUG
			LOG( @"ERROR: Could save only the first %d state information", nRecordsSaved );
#endif
			return false;
		}
		

		
		
		// Fecha o arquivo
		fclose( pFile );
		
		return true;		
	}
	
	
	- (void)removeStateFile
	{
		LOG(@"remove state file");
		NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
		if( hDocumentsDirectory == NULL )	
			return;

		NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: [self getStateFileName]];

		
		if( ![[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
			return		;

			[[NSFileManager defaultManager] removeItemAtPath:hFilePath error: NULL ];
	}
//</DM>	
// Fim da implementação da classe
@end
