/*
 *  SelectView.h
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef SELECT_VIEW
#define SELECT_VIEW 1

#include"UpdatableView.h"
#include"GameBaseInfo.h"

@interface SelectView : UpdatableView
{
//fada ogro fazendeiro executivo anjinho diabinho
	IBOutlet UIButton* hAnjinho;
	IBOutlet UIButton* hDiabinho;
	IBOutlet UIButton* hFada;
	IBOutlet UIButton* hFazendeiro;
	IBOutlet UIButton* hExecutivo;
	IBOutlet UIButton* hOgro;
	IBOutlet UILabel* hres;
	IBOutlet UILabel* htype;
	IBOutlet UIButton* hOK;
	IBOutlet UIButton* hback;
	IBOutlet UITextField* hNome;
	IBOutlet UIImageView* img;
	IBOutlet UISegmentedControl* hPlayer;
	uint8 iPlayer;
	GameBaseInfo* GBinfo;
}

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo;

//método para ser chamado qdo o teclado sai de tela
-( BOOL )textFieldShouldReturn:( UITextField * )textField ;

//método chamado qdo o segmentcontrol muda de valor
- (void)segmentAction:(id)sender;



@end



#endif