/*
 *  SelectView.mm
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "SelectView.h"
#include "FreeKickAppDelegate.h"
#include "Config.h"
#include "ObjcMacros.h"

// NanoOnline
#include "NOControllerView.h"
#include "NOCustomer.h"
#include "NOGlobalData.h"
#include "NOString.h"
@implementation SelectView

- ( void )awakeFromNib
{
	[super awakeFromNib];
	iPlayer = 0;
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
	//[hTxtHelp setFont: [(( FreeKickAppDelegate* )APP_DELEGATE ) getiPhoneFont]];
	
}

#define MAX_BUFFER_DOLL_NAME 15
// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton{

	char buffer[ MAX_BUFFER_DOLL_NAME ];
	
	if ( hButton == hAnjinho ) {
		GBinfo->iddolls[ iPlayer ] = DOLL_ID_ANGEL ;

		sprintf( buffer,"anjinho" );
	}else if ( hButton == hDiabinho ) {
		GBinfo->iddolls[ iPlayer ] = DOLL_ID_DEVIL ;
		sprintf( buffer, "diabinho" );
	}else if ( hButton == hFada ) {
		GBinfo->iddolls[ iPlayer ] = DOLL_ID_FAIRY ;
		sprintf( buffer, "fada" );
	}else if ( hButton == hFazendeiro ) {
		GBinfo->iddolls[ iPlayer ] = DOLL_ID_FARMER ;
		sprintf( buffer, "fazendeiro" );
	}else if ( hButton == hExecutivo ) {
		GBinfo->iddolls[ iPlayer ] = DOLL_ID_EXECUTIVE ;
		sprintf( buffer, "executivo" );
	}else if ( hButton == hOgro ) {
		GBinfo->iddolls[ iPlayer ] = DOLL_ID_OGRE ;		
		sprintf( buffer, "ogro" );
	}else if ( hButton == hOK ) {
		[ [ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_CONFIG_MATCH ];
	}else if (hButton == hback) {
		[[ ApplicationManager GetInstance ] performTransitionToView:VIEW_INDEX_SELECT_MODE ];
	}



[hres setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];
}

//chamado qdo o segmented button muda de valor
- (void)segmentAction:(id)sender
{
	iPlayer = [ hPlayer selectedSegmentIndex ];
	
	[ hNome setText: STD_STRING_TO_NSSTRING( GBinfo->nomes[ iPlayer ] ) ];
	
	char buffer[ MAX_BUFFER_DOLL_NAME ];

	switch ( GBinfo->iddolls[ iPlayer ] ) {
		case DOLL_ID_FAIRY:
			sprintf( buffer, "fadinha" );	
			break;
		case DOLL_ID_ANGEL :
			sprintf( buffer, "anjinho" );
			break;
		case DOLL_ID_EXECUTIVE:
			sprintf( buffer, "executivo" );
			break;
		case DOLL_ID_FARMER:
			sprintf( buffer, "fazendeiro" );			
			break;
		case DOLL_ID_OGRE:
			sprintf( buffer, "ogro" );			
			break;
		case DOLL_ID_DEVIL:
			sprintf( buffer, "diabinho" );			
			break;
		case DOLL_ID_NONE:
			sprintf( buffer, "?????????" );
			break;
	}
	
	[hres setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];

}

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo{
	GBinfo = GBInfo;
	if( NOGlobalData::GetActiveProfile().getProfileId() >= 0 ){
		NOString nickname;
		NOGlobalData::GetActiveProfile().getNickname( nickname );
	}else {
		LOG("ninguém logado");
	}

	[ hNome setText: STD_STRING_TO_NSSTRING( GBinfo->nomes[ iPlayer ] ) ];
	
	char buffer[ MAX_BUFFER_DOLL_NAME ];
	
	switch ( GBinfo->iddolls[ iPlayer ] ) {
		case DOLL_ID_FAIRY:
			sprintf( buffer, "fadinha" );	
			break;
		case DOLL_ID_ANGEL :
			sprintf( buffer, "anjinho" );
			break;
		case DOLL_ID_EXECUTIVE:
			sprintf( buffer, "executivo" );
			break;
		case DOLL_ID_FARMER:
			sprintf( buffer, "fazendeiro" );			
			break;
		case DOLL_ID_OGRE:
			sprintf( buffer, "ogro" );			
			break;
		case DOLL_ID_DEVIL:
			sprintf( buffer, "diabinho" );			
			break;
		case DOLL_ID_NONE:
			sprintf( buffer, "?????????" );
			break;
	}

	[hres setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];	
	
}
#undef MAX_BUFFER_DOLL_NAME

/*===================================================================================================================================
 
método chamado qdo o usuário aperta voltar no teclado, ele somente utiliza os 5 primeiros caracteres, e depois atualiza o campo nome.

 ====================================================================================================================================*/

#define MAX_BUFFER_NAME_LEN 6

- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
	char buffer[ MAX_BUFFER_NAME_LEN ];
	snprintf( buffer , MAX_BUFFER_NAME_LEN , NSSTRING_TO_CHAR_ARRAY (hNome.text) );
	
	[hNome setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];

	GBinfo->nomes[ iPlayer ] = buffer ;
	
	[textField resignFirstResponder]; 
	return YES; 
}

#undef MAX_BUFFER_NAME_LEN




@end
