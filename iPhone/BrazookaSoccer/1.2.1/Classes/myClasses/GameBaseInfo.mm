/*
 *  GameBaseInfo.mm
 *  dotGame
 *
 *  Created by Max on 10/23/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "GameBaseInfo.h"

GameBaseInfo::GameBaseInfo( void ):mode( DOT_GAME_MODE_NONE ),nVZones( 3 ),nHZones( 3 ),idBg( ID_BGD_NONE ),nPlayers( 2 ),level( 1 ) {
	memset( iddolls, DOLL_ID_NONE, sizeof( IdDoll ) * nPlayers );
	memset( pUserType , PLAYER_USER_NONE, sizeof ( PlayerUser ) * nPlayers );
	memset( scores ,0, sizeof ( uint8 ) * MAX_PLAYERS );

	for (uint8 i = 0; i< MAX_PLAYERS; i++ ) {
		nomes[ i ] = "player";
	}
}


void GameBaseInfo::reset(){
	mode = DOT_GAME_MODE_NONE;
	nVZones = 3;
	nHZones = 3;
	idBg = ID_BGD_NONE;
	nPlayers = 2;
	level = 1;
	for (uint8 i = 0; i< MAX_PLAYERS; i++ ) {
		iddolls[ i ] = DOLL_ID_NONE;
		pUserType[ i ] = PLAYER_USER_NONE;
		nomes[ i ] = "player";
		scores[ i ] = 0;
	}


}