/*
 *  DotGameInfo.h
 *  dotGame
 *
 *  Created by Max on 10/9/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef DOT_GAME_INFO_H
#define DOT_GAME_INFO_H 1

#define  TOTAL_DOLLS_EXPRESSIONS 5


enum DotGameMode {
	DOT_GAME_MODE_NONE,
	DOT_GAME_MODE_VERSUS_PLAYER,
	DOT_GAME_MODE_VERSUS_CPU,
	DOT_GAME_MODE_MULTIPLAY_SOLO,
	DOT_GAME_MODE_MULTIPLAY_ONLINE,
	DOT_GAME_MODE_EXIBITHION
}typedef DotGameMode;

enum DollExpressions {
	
	DOLL_EXPRESSION_NONE = -1,
	DOLL_EXPRESSION_SAD = 0,
	DOLL_EXPRESSION_HAPPY,
}typedef DollExpressions;

#define MAX_PLAYERS 2
#define MAX_ZONES_COLUMNS 10
#define MAX_ZONES_ROWS 10
#define MAX_LINES_COLUMNS ( MAX_ZONES_COLUMNS +1 ) 
#define MAX_LINES_ROWS ( MAX_ZONES_ROWS + 1) 


/**=========================
 
 DEFINIÇÕES DAS MEDIDAS das zonas, linhas, etc.
 
 ==========================*/

#define ZONE_SIDE 64.0f//50.0f//64.0f//50.0F
#define LINE_HEIGHT ZONE_SIDE
#define LINE_WIDTH 8.0f//10.0F

#define LINE_HORIZONTAL_HEIGHT 7.8f//10.0f
#define LINE_HORIZONTAL_WIDTH ZONE_SIDE

#define LINE_VERTICAL_HEIGHT ZONE_SIDE
#define LINE_VERTICAL_WIDTH 8.0f//10.0f

#define FENCE_HORIZONTAL_HEIGHT 17.0f
#define FENCE_HORIZONTAL_WIDTH ZONE_SIDE

#define FENCE_VERTICAL_HEIGHT ZONE_SIDE
#define FENCE_VERTICAL_WIDTH 17.0f


#endif