/*
 *  LoadingVersusView.mm
 *  dotGame
 *
 *  Created by Max on 12/16/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "LoadingVersusView.h"

// Intervalo entre as atualizações de tela
#define LOADING_VIEW_REFRESH_INTERVAL ( 1.0f / 32.0f )

#include "Utils.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"

//métodos privados

@interface LoadingVersusView()

// Inicializa o objeto
- ( bool )build;

// Atualiza a tela
- ( void )updateView;

@end




//inicio implementação
@implementation LoadingVersusView


/*==============================================================================================
 
 MENSAGEM initWithFrame: AndSelector:
 Construtor chamado quando carregamos a view via código.
 
 ==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}


/*==============================================================================================
 
 MENSAGEM initWithCoder
 Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
 
 ==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
Error:
	[self release];
	return NULL;
}

/*==============================================================================================
 
 MENSAGEM build
 Inicializa a view.
 
 ==============================================================================================*/

- ( bool )build
{
	hThread = [[LoadingThread alloc] init];
	if( !hThread )
		return false;
	
	loadingObj = NULL;
	loadingFunc = NULL;
	onLoadEndedFunc = NULL;
	showed100Feedback = false;
	
	return true;
}

/*==============================================================================================
 
 MENSAGEM dealloc
 Destrutor.
 
 ==============================================================================================*/

- ( void )dealloc
{
	if( [hThread isExecuting] )
		[hThread cancel];
	
	while( ![hThread isFinished] );
	
	[hThread release];
	hThread = NULL;
	
	[self suspend];
	
    [super dealloc];
}

/*==============================================================================================
 
 MENSAGEM setLoadingTarget: AndSelector:
 Determina um ponteiro para a função de loading.
 
 ==============================================================================================*/

-( void )setLoadingTarget:( id )target AndSelector:( SEL )selector CallingAfterLoad:( SEL )onLoadEndedSelector
{
	loadingObj = target;
	loadingFunc = selector;
	onLoadEndedFunc = onLoadEndedSelector;
}

/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 Método chamado quando a view se torna a view principal da aplicação.
 
 ==============================================================================================*/

- ( void ) onBecomeCurrentScreen
{
	if( ( loadingObj != NULL ) && ( loadingFunc != NULL ) && ( onLoadEndedFunc != NULL ) )
	{
		loadingPercent = 0.0f;
		threadErrorCode = ERROR_NONE;
		showed100Feedback = false;
		
		// Inicia a animação do logo da nano
		//[hNanoGamesLogo startAnimating];
		[self resume];
		
		// Inicia o carregamento
		[hThread loadWithObj:loadingObj Method:loadingFunc AndParam: self];
		[hThread start];
	}
}


/*==============================================================================================
 
 MENSAGEM resume
 Pára o loop de renderização.
 
 ==============================================================================================*/

- ( void ) resume
{
	[self suspend];

	hInterfaceUpdater = [NSTimer scheduledTimerWithTimeInterval: LOADING_VIEW_REFRESH_INTERVAL target:self selector:@selector( updateView ) userInfo:NULL repeats:YES];
}


/*==============================================================================================
 
 MENSAGEM setProgress:
 Determina a porcentagem atual da barra de progresso, atualizando sua imagem e o label de
 porcentagem do loading.
 
 ==============================================================================================*/

- ( void ) setProgress:( float )percent
{
	loadingPercent = NanoMath::clamp( percent, 0.0f, 1.0f );
	
}

/*==============================================================================================
 
 MENSAGEM changeProgress:
 Modifica a porcentagem atual da barra de progresso em 'percent', atualizando sua imagem e
 o label de porcentagem do loading.
 
 ==============================================================================================*/

- ( void ) changeProgress:( float )percent
{
	[self setProgress: loadingPercent + percent];
}


/*==============================================================================================
 
 MENSAGEM setError:
 Indica que houve algum erro de processamento na thread.
 
 ==============================================================================================*/

-( void ) setError:( int16 )errorCode
{
	threadErrorCode = errorCode;
}

/*==============================================================================================
 
 MENSAGEM getError
 Retorna se houve algum erro de processamento na thread.
 
 ==============================================================================================*/

-( int16 ) getError
{
	return threadErrorCode;
}

/*==============================================================================================
 
 MENSAGEM updateView
 Atualiza a tela.
 
 ==============================================================================================*/

-( void )updateView
{
	if( !showed100Feedback )
	{
		[hProgressLabel setText: [NSString stringWithFormat: @"%d%%", static_cast< int32 >( loadingPercent * 100.0f )]];
		[hProgressBar setProgress: loadingPercent];
		
		if( [hThread isFinished] )
		{
			[hProgressBar setProgress: 1.0f];
			showed100Feedback = true;
		}
	}
	else
	{		
		[self suspend];
		[loadingObj performSelector: onLoadEndedFunc withObject: self];
	}
}


// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo{
	
	GBinfo = GBInfo;
	
	char* res;	
	switch (GBInfo->iddolls[ 0 ]) {
			
		case DOLL_ID_ANGEL :
			res = "anjo2";
			break;
			
		case DOLL_ID_DEVIL :
			res = "devil";
			break;
			
		case DOLL_ID_FARMER :
			res = "Caip1";
			break;
			
		case DOLL_ID_EXECUTIVE :
			res = "exec";
			break;
			
		case DOLL_ID_FAIRY :
			res = "fada";
			break;
			
		case DOLL_ID_OGRE :
			res = "ogro";
			break;
	}

	//[ hP1 setImage: [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( res, "png" ) )] ];
	
	switch (GBInfo->iddolls[ 1 ]) {
			
		case DOLL_ID_ANGEL :
			res = "anjo2";
			break;
			
		case DOLL_ID_DEVIL :
			res = "devil";
			break;
			
		case DOLL_ID_FARMER :
			res = "Caip1";
			break;
			
		case DOLL_ID_EXECUTIVE :
			res = "exec";
			break;
			
		case DOLL_ID_FAIRY :
			res = "fada";
			break;
			
		case DOLL_ID_OGRE :
			res = "ogro";
			break;
	}
	//[ hP2 setImage: [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( res, "png" ) )] ];
}


/*=========================================================================
 
 para o loop de renderização
 
 ======================================================================*/
- ( void ) suspend{
	if( hInterfaceUpdater )
	{
		[hInterfaceUpdater invalidate];
		hInterfaceUpdater = NULL;
	}

}

@end
