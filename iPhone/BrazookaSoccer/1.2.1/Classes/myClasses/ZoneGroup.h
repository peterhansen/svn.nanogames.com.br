/*
 *  ZoneGroup.h
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef ZONE_GROUP_H
#define ZONE_GROUP_H 1

#include "ObjectGroup.h"
#include "DotGameInfo.h"
/*
 
 gerencia as zonas (nome horrível, eu sei...), que aparecem no jogo, ou seja, é ele quem aloca, desaloca, conta quantas estão disponíveis, etc.
 
 */

class ZoneGame;
class DotGame;
class GameBaseInfo;
class Player;

class ZoneGroup : public ObjectGroup {
	
public:
	
	//construtor
	ZoneGroup( GameBaseInfo& g );
	
	//destrutor
	~ZoneGroup( void ){};
	
	//indica se ainda há zona livre
	bool haveFreeZone( void );
	
	//insere zonas no grupo, para gerenciamento
	void insertZones( ZoneGame* z[ MAX_ZONES_COLUMNS ][ MAX_ZONES_ROWS ] );
	
	void reset( void );
	
	void setQteZones( uint8 h, uint8 v );
	
	bool isZoneOccupied( Point3f *p );
	
	bool zoneGainedByPlayer( Point3f *p, Player *pl);
	
	void zoneLostedByPlayer( Point3f *p/*, Player*p*/ );

	
private:
	
	uint8 nZonesH, nZonesV;
	
};

#endif