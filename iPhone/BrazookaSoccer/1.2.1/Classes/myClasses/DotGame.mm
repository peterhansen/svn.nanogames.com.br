/*fada ogro fazendeiro executivo anjinho diabinho
 *  DotGame.mm
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "DotGame.h"
#include "AnimatedCamera.h"
#include "Button.h"
#include "MathFuncs.h"
#include "GameUtils.h"
#include "BoardFactory.h"


#include "Utils.h"
#include "Random.h"
#include "Background.h"
#include "TexturePattern.h"
#include "FeedbackMsg.h"
#include "ObjcMacros.h"
#include "Marks.h"
#include "FreeKickAppDelegate.h"
#include "Button.h"
#include "MathFuncs.h"
using namespace NanoMath;

//tempo de certas atividades

#define TIME_FOCUS_LINE 0.5f
#define DURATION_TIME_MSG_GAME_OVER 0.5f
#define MAX_TIME_TO_PLAYER_MAKE_MOVE 30.0f//15.0F
#define TIME_TO_CPU_PLAY 27.0f//3.0f//13.0F


//MENSAGENS DO JOGO
#define INDEX_MSG_GAME_OVER 2
#define INDEX_MSG_NEXT_PLAYER 3
#define INDEX_MSG_NEXT_CPU 4
#define INDEX_MSG_CPU_WIN 5
#define INDEX_MSG_PLAYER_WIN 6

//identificação dos botões no jogo
#define ID_BUTTON_YES 0
#define ID_BUTTON_NO 1
#define ID_BUTTON_LINE_VIEW 2
#define ID_BUTTON_CONFIRM_MOVE 3

//BOTOES DO JOGO dentro do grupo
#define INDEX_BUTTON_YES  7//ID_BUTTON_YES + 7
#define INDEX_BUTTON_NO  8//ID_BUTTON_NO + 7
#define INDEX_BUTTON_LINE_VIEW 9//ID_BUTTON_LINE_VIEW + 7


//posição dos butões
#define MANAGERS_POSITION_Y 63.0f
#define POSITION_BACKGROUND_Y 252.0f
#define POSITION_BACKGROUND_X 250.0f




//qte de objetos na cena
#define SCENE_N_OBJECTS 6

DotGame::DotGame( GameBaseInfo &g, LoadingView* pLoadingView ):
Scene( SCENE_N_OBJECTS ),
OGLTransitionListener(),
SpriteListener(),
InterfaceControlListener(),
ChronometerListener(),
EventListener( EVENT_TOUCH ),
currPlayer( NULL ),
buttonsManager( NULL ),
lineManager( NULL ),
zoneManager( NULL ),
pChronometer( NULL ),
pInfoBar( NULL ),
lastState( DOT_GAME_STATE_NONE ),
state( DOT_GAME_STATE_NONE ),
level( g.level),
limit( level - 1 ),
indicePlayer( 255 ),
GInfo( g ),
combo( 0 ),
stateTimeCounter( 0.0f ),
linesSelected( 0 ),
animCamera( false ),
nActiveTouches( 0 ),
goodBye( false ),
dispMoves( 3 ),// 1 ),
nHorizontalZones( g.nHZones ),
nVerticalZones( g.nVZones ),
nHorizontalLines( nHorizontalZones + 1 ),
nVerticalLines( nVerticalZones + 1  ),
//nActiveTouches( 0 ),
initDistBetweenTouches( 0.0f ),
//stateTimeCounter( 0.0f ),
mode( g.mode ),
movesMax( 200),//( nHorizontalZones + nVerticalZones ) / 2 ),
movesCount( 0 ),
nPlayers( MAX_PLAYERS/*g.nPlayers*/ )
{
#if DEBUG
	LOG_FREE_MEM( "\n\nqte de mem. livre antes de carregar as coisas" );
#endif
	memset( zones, NULL, sizeof( ZoneGame* ) * (nHorizontalZones * nVerticalZones ) );
	memset( lineH, NULL, sizeof( LineGame* ) * ( nHorizontalLines * nVerticalZones  ) );	
	memset( lineV, NULL, sizeof( LineGame* ) * (nHorizontalZones * nVerticalLines  ) );	
	memset( players, NULL, sizeof( Player* ) * nPlayers );	
	
	[ pLoadingView setProgress: 0.1 ];
	
	if(	!buildDotGame( pLoadingView ) )
	
	#if DEBUG
		NSLog(@" falha no build!! ");
	#endif
	#if DEBUG
	LOG_FREE_MEM( "\n\nqte de mem. livre depois de carregar as coisas" );
#endif
}

/*==============================================================================================
 
 método buildDotGame => constroi os elementos do dotgame
 
 ==============================================================================================*/
bool DotGame::buildDotGame( LoadingView* pLoadingView ){
#if DEBUG
	LOG("método build chamado!!");
#endif
	// Cria os objetos que irão sofrer as transformações da câmera
	AnimatedCamera* pCurrCamera = new AnimatedCamera( this, CAMERA_TYPE_AIR, -10.0f, 10.0f );
	if( !pCurrCamera )
		goto Error;
	
	
	setCurrentCamera( pCurrCamera );
	pCurrCamera->setLandscapeMode( true );
	pCurrCamera->setPosition(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT,1.0);
	
	if( !loadPlayers() ) 
		goto Error;//NSLog(@"falha ao carregar jogadores");
#if DEBUG
	LOG_FREE_MEM("jogadores prontos");
#endif
	[ pLoadingView setProgress: 0.1 ];
	
	if( !buildBg() )
		goto Error;
#if DEBUG
	LOG_FREE_MEM("fundo pronto");
#endif
	[ pLoadingView setProgress: 0.5 ];		
	
	if( !makeTable() ){
#if DEBUG		
		NSLog(@"falha ao carregar tabuleiro");
#endif
		goto Error;
	}
	[ pLoadingView setProgress: 0.3 ];
#if DEBUG	
	LOG_FREE_MEM("tabuleiro pronto");
#endif
	if( !buildButtons() ){
#if DEBUG
		LOG_FREE_MEM("falha ao carregar os botões");
#endif
		goto Error;
	}	
	[ pLoadingView setProgress: 0.45 ];


	changePlayer();	
	return true;
	
Error:
	clear();
	return false;
}


/*===========================================================
 
 buildButtons=> método usado para construir os elementos de interface que são fixos.
 
 =-=-------------------------------------------------------------*/
bool DotGame::buildButtons( void ){
	
	pControlsCamera = new OrthoCamera();
	if ( !pControlsCamera )
		return false;
	
	pControlsCamera->setLandscapeMode( true );
	
	buttonsManager = new ButtonsGroup( pControlsCamera );
	
	if( !buttonsManager|| ! insertObject( buttonsManager ) )
		return false;
	
	//incluir os outros botões deste ponto em diante!!
	
	pSliceTransition = new SliceTransition( 20, 1.0, this );
	
	pCurtainTransition = new CurtainTransition( 1.0, this, COLOR_BLACK ); 
	
	//pClosingDoorTransition = new ClosingDoorTransition( 1.0 , this, COLOR_BLACK);
	
	pCurtainLeftTransition = new CurtainLeftTransition( 1.0 , this, COLOR_BLACK);
	
	pCurrTransition = pSliceTransition;
	
	pInfoBar = new InfoBar( this->GInfo ,this );
#if DEBUG
	pInfoBar->setName("infobar\n");
#endif
	if ( !pInfoBar || !buttonsManager->insertObject( pInfoBar ) )
		return false;
	
	pInfoBar->startShowAnimation();
	pInfoBar->setVisible( true );
	pInfoBar->configure( this );
	pInfoBar->setPosition( 0.0f,7.5f );
	
	pChronometer = new Chronometer( this, this );
	if( !pChronometer )
		return false;
#if DEBUG
	pChronometer->setName("cronometro\n");
#endif	
	pChronometer->setPosition( ( ( SCREEN_WIDTH - pChronometer->getWidth() ) * 0.5f ) , 40.0f );
		
	if( !pChronometer || !buttonsManager->insertObject( pChronometer ))
		return false;
	
	pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0);
	pChronometer->pause(false);
	pChronometer->startShowAnimation();
	
	TextureFrame frame( 0.0f, 0.0f, 254.0f, 49.0f, 0.0f, 0.0f );
	
	//	depois fazer o mesmo com as outras msgs, não esquecer que tb tem outras 
	//	msg de game over
	FeedbackMsg *pGameOver = new FeedbackMsg( "msggo\0", &frame, this );
	
	pGameOver->setPosition(HALF_SCREEN_WIDTH/2, HALF_SCREEN_HEIGHT,0.0f);
	pGameOver->setCurrAnimType( FEEDBACK_MSG_ANIM_FADE );
	pGameOver->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	if( !pGameOver || !buttonsManager->insertObject( pGameOver ) )
		return false;
#if DEBUG
	pGameOver->setName("game over msg\n");
#endif		
	
	//msg de aviso de troca de jogador!!
	frame.set( 0.0f, 0.0f, 256.0f, 64.0f, 0.0f, 0.0f );
	FeedbackMsg *pSuaVez = new FeedbackMsg( "CPTMPVIEW", &frame ,this );
	pSuaVez->setPosition(HALF_SCREEN_WIDTH/2, HALF_SCREEN_HEIGHT,0.0f);
	pSuaVez->setCurrAnimType( FEEDBACK_MSG_ANIM_VIEWPORT );
	pSuaVez->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	//buttonsManager->insertObject( pSuaVez );
#if DEBUG
	pSuaVez->setName("sua vez msg\n");
#endif	
	
	if( !pSuaVez || !buttonsManager->insertObject( pSuaVez ) )
		return false;
	//aviso que é vez da cpu
	frame.set( 0.0f, 0.0f, 256.0f, 64.0f, 0.0f, 0.0f );
	FeedbackMsg *pCpuVez = new FeedbackMsg( "cpuvz" , &frame ,this );
	pCpuVez->setCurrAnimType( FEEDBACK_MSG_ANIM_VIEWPORT );
	pCpuVez->setPosition(HALF_SCREEN_WIDTH/2, HALF_SCREEN_HEIGHT,0.0f);
	pCpuVez->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	
#if DEBUG
	pCpuVez->setName("cpu vez msg\n");
#endif
	if( !pCpuVez || !buttonsManager->insertObject( pCpuVez ) )
		return false;
	
	//avisa que a cpu ganhou
	frame.set( 0.0f, 0.0f, 256.0f, 256.0f, 0.0f, 0.0f );
	FeedbackMsg *pCpuWin = new FeedbackMsg( "cpwin", &frame ,this );
	pCpuWin->setCurrAnimType( FEEDBACK_MSG_ANIM_VIEWPORT );
	pCpuWin->setPosition(HALF_SCREEN_WIDTH/2, HALF_SCREEN_HEIGHT,0.0f);
	pCpuWin->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	
#if DEBUG
	pCpuWin->setName("cpu venceu msg\n");
#endif
	if( !pCpuWin || !buttonsManager->insertObject( pCpuWin ) )
		return false;
	
	//avisa que o player ganhou
	frame.set( 0.0f, 0.0f, 256.0f, 256.0f, 0.0f, 0.0f );
	FeedbackMsg *pPlrWin = new FeedbackMsg( "plrwin", &frame ,this );
	pPlrWin->setCurrAnimType( FEEDBACK_MSG_ANIM_VIEWPORT );
	pPlrWin->setPosition(HALF_SCREEN_WIDTH/2, HALF_SCREEN_HEIGHT,0.0f);
	pPlrWin->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	
#if DEBUG
	pPlrWin->setName("player venceu msg\n");
#endif
	if( !pPlrWin || !buttonsManager->insertObject( pPlrWin ) )
		return false;
	//botão de sim
	Button *pSim = new Button("sim", this );
	if( !pSim || !buttonsManager->insertObject( pSim ) )
		return false;
	pSim->setPosition( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	pSim->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
#if DEBUG
	pSim->setName("botao sim \n");
#endif
	
	
	//botao de não
	Button* pNao = new Button( "nao",this );
	if( !pNao || !buttonsManager->insertObject( pNao ) )
		return false;
	pNao->setPosition( HALF_SCREEN_WIDTH + 80, HALF_SCREEN_HEIGHT + 80 );
	pNao->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
#if DEBUG
	pNao->setName("botao nao \n");
#endif
	
	//frame.set( 0.0f, 0.0f, 64.0f, 64.0f, 0.0f, 0.0f );

	Button* pLines = new Button( "lines",this );
	if( !pLines || !buttonsManager->insertObject( pLines ) )
		return false;
	pLines->setPosition( 0.0 , SCREEN_HEIGHT - 64.0f);
	pLines->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
#if DEBUG
	pLines->setName("botao Linhas \n");
#endif
	
	//botão de confirmar movimento
	
	Button* pOk = new Button( "bok",this );
	if( !pOk || !buttonsManager->insertObject( pOk ) )
		return false;
	pOk->setPosition( SCREEN_WIDTH - 64.0f , SCREEN_HEIGHT - 64.0f);
	pOk->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
#if DEBUG
	pOk->setName("botao ok \n");
#endif
	
	
	return true;
}


/*======================================================================================
 
 constrói o background
 
 ==================================================================================== */
bool DotGame::buildBg( void ){
//#include "Background.h"
	bg = BoardFactory::GetInstance()->getNewBoard( GInfo.idBg ); 
	lim.set(  nHorizontalZones *  ZONE_SIDE + ( ( nHorizontalLines ) * LINE_HORIZONTAL_HEIGHT ) ,
		  ( nVerticalLines ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE)	
	); //= *bg->getObject( 0 )->getSize();
	min = *bg->getObject( 1 )->getPosition();
	return insertObject( bg );

}

/**==========================================================================
 
 constrói o tabuleiro, com os pontos e zonas necessárias. 
 
 ==========================================================================*/

bool DotGame::makeTable( void ){
	zoneManager = new ZoneGroup( GInfo );
	zoneManager->setQteZones( nHorizontalZones, nVerticalZones);
	if( !insertObject( zoneManager ) )
		return false;
	
	lineManager = new LineGroup( GInfo );
	lineManager->setTotalLines( nHorizontalLines , nVerticalLines, nHorizontalZones , nVerticalZones );
	if(	!insertObject( lineManager ) )
		return false;
	
	uint8 x,y;
	for ( x = 0; x < nHorizontalZones; x++) {
		for ( y = 0; y< nVerticalZones ; y++ ){
			zones[ x ][ y ] = new ZoneGame();
			zones[ x ][ y ]->setPosition( x, y );
		}
	}
	
	for ( x = 0; x < nHorizontalZones ; x++ ) {
		for ( y = 0; y < nVerticalLines  ; y++) {
			lineH[ x ][ y ] = new LineGame( LINE_GAME_ORIENTATION_HORIZONTAL/*, GInfo.idBg*/ );
			lineH[ x ][ y ]->setPosition( x , y );
		}
		
	}
	
	for ( x = 0; x < nHorizontalLines  ; x++ ) {
		for ( y = 0; y < nVerticalZones ; y++) {
			lineV[ x ][ y ] = new LineGame( LINE_GAME_ORIENTATION_VERTICAL/*, GInfo.idBg*/ );
			lineV[ x ][ y ]->setPosition( x , y );
		}
	}
	
	lineManager->insertLinesH( lineH );
	lineManager->insertLinesV( lineV );
	zoneManager->insertZones( zones );
	
	//lineManager->setVisible(false);
	//zoneManager->setVisible(false);
	
	
	Point3f p;
	p.set(  nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
		  nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
		  1.0);
	p *= 0.5f;
	min.set(  nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
			nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
			1.0);
	zoneManager->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0 );
	lineManager->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0 );
	bg->setPosition( HALF_SCREEN_WIDTH - p.x, MANAGERS_POSITION_Y, 1.0 );

	return true;
	
}

/*===============================================================================
 
 ZoneHorizontalSuperiorOccupied-> verifica se a zona que está a em cima da linha horizontal foi ocupada pelo último movimento
 
 ===============================================================================*/
bool DotGame::ZoneHorizontalSuperiorOccupied( uint8 x,uint8 y ){
	//ok!
	if ( x >= 0  && ( y > 0 ) ) {
		
		if( lineH[ x ][ y ]->isOccupied() && 
		   lineH[ x ][ y - 1 ]->isOccupied() &&
		   lineV[ x ][ y - 1 ]->isOccupied() &&
		   lineV[ x + 1 ][ y - 1 ]->isOccupied())
		{
			cZone.set( x , y - 1 );
			zones[ x ][ y - 1 ]->gainedByPlayer( currPlayer );
			//NSLog(@" zona ocupada é a zona na posição x=%d, y=%d, pelo jogador %d ",x,y-1, zones[ x ][ y-1 ]->getPlayerId());
			linesSelected =0;
			combo++;
			pInfoBar->updateScores( this );
			makeBigZone();
			return true;
		}
		
	}
	return false;
}


/*===============================================================================
 
 ZoneHorizontalInferiorOccupied-> verifica se a zona que está embaixo da linha horizontal foi ocupada pelo último movimento
 
 ===============================================================================*/

bool DotGame::ZoneHorizontalInferiorOccupied( uint8 x, uint8 y )
{
	//OK!!
	if( x < nHorizontalZones && ( y < ( nVerticalZones )) ) {
		
		if( lineH[ x ][ y ]->isOccupied() && 
		   lineH[ x ][ y + 1 ]->isOccupied() &&
		   lineV[ x ][ y ]->isOccupied() &&
		   lineV[ x + 1 ][ y ]->isOccupied())
		{
			cZone.set( x , y );
			linesSelected =0;
			combo++;
			zones[ x ][ y ]->gainedByPlayer( currPlayer );
			//NSLog(@" zona ocupada é a zona na posição x=%d, y=%d, pelo jogador %d ",x,y, zones[ x ][ y ]->getPlayerId());
			makeBigZone();
			pInfoBar->updateScores( this );
			return true;
		}
		
	}
	return false;
}


/*===============================================================================
 
 ZoneVerticalLeftOccupied-> verifica se a zona que está a esquerda da linha vertical foi ocupada pelo último movimento
 
 ===============================================================================*/

bool DotGame::ZoneVerticalLeftOccupied( uint8 x, uint8 y ){
	//ok
	if( ( y >= 0 ) && ( x >  0  ) ){
		
		if( lineH[ x - 1 ][ y ]->isOccupied() && 
		   lineH[ x - 1 ][ y + 1 ]->isOccupied() &&
		   lineV[ x ][ y ]->isOccupied() &&
		   lineV[ x - 1 ][ y ]->isOccupied())
		{
			cZone.set( x - 1 , y );
			zones[ x - 1 ][ y ]->gainedByPlayer( currPlayer );
			combo++;
			linesSelected =0;
			//NSLog(@" zona ocupada é a zona na posição x=%d, y=%d, pelo jogador %d ",x,y, zones[ x-1 ][ y ]->getPlayerId());
			makeBigZone();
			pInfoBar->updateScores( this );
			return true;
		}
	}
	return false;
}

/*===============================================================================
 
 ZoneVerticalRightOccupied-> verifica se a zona que está a direita da linha vertical foi ocupada pelo último movimento
 
 ===============================================================================*/

bool DotGame::ZoneVerticalRightOccupied( uint8 x, uint8 y ){
	//ok!!
	if( ( y < nVerticalZones ) && ( x < (nHorizontalZones ) ) ){
		
		if( lineH[ x ][ y ]->isOccupied() && 
		   lineH[ x ][ y + 1 ]->isOccupied() &&
		   lineV[ x ][ y ]->isOccupied() &&
		   lineV[ x + 1 ][ y ]->isOccupied())
		{
			cZone.set( x , y );
			zones[ x ][ y ]->gainedByPlayer( currPlayer );
		//	NSLog(@" zona ocupada é a zona na posição x=%d, y=%d, pelo jogador %d ",x,y, zones[ x ][ y ]->getPlayerId());
			combo++;
			linesSelected =0;
			makeBigZone();
			pInfoBar->updateScores( this );
			return true;
		}
	}
	return false;
	
}

/*===============================================================================
 
 Atualiza o objeto
 
 ===============================================================================*/
bool DotGame::update( float timeElapsed ){
	if( animCamera )	{
		AnimatedCamera* pCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );
		pCamera->update( timeElapsed );
	}
	switch ( state ) {
		case DOT_GAME_STATE_NONE:
		case DOT_GAME_STATE_PLAYER_WAITING :
			break;
			
		case DOT_GAME_STATE_AFTER_CPU_MOVE :
			
			if( !haveFreeZone() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
				else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
				}
				
				//	setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER);//DOT_GAME_STATE_GAME_OVER );
				//	setDotGameState(DOT_GAME_STATE_ANIMATING_TRANSITION_1);
			}
			
			break;
			
		case DOT_GAME_STATE_CPU_PLAYING:
			
			if( ( fleq(pChronometer->getCurrTime() , TIME_TO_CPU_PLAY )&& !pChronometer->isPaused())) {
				cpuMove();
				
			}
			
			break;
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:		
			pCurrTransition->update( timeElapsed );
			break;
			
		case DOT_GAME_STATE_FOCUSING_LINE:
		{
			//			AnimatedCamera* pCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );
			//			pCamera->update( timeElapsed );
		}
			break;
		case DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_CPU_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER:
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER:{
			
			if( stateTimeCounter >= 0.0f ){
				stateTimeCounter += timeElapsed;
				if( stateTimeCounter >= stateDuration ){
					stateTimeCounter = 0.0;
					stateDuration = -1.0f;
					
					if( state == DOT_GAME_STATE_SHOWING_MSG_GAME_OVER ){
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_GAME_OVER );
						//return;
					}else if( state == DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER ){
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER );
						
					}else if( state == DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN ){
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN );
						
					}else if( state == DOT_GAME_STATE_SHOWING_MSG_CPU_WIN )
						
						setDotGameState( DOT_GAME_STATE_HIDING_MSG_CPU_WIN );
				}
			}
		}
			
			break;
			
		default:
			break;
	}
	
	
	return Scene::update( timeElapsed );
}

/*===========================================================================================
 
 MÉTODO handleEvent
 Método que trata os eventos enviados pelo sistema.
 
 ============================================================================================*/

bool DotGame::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ){
	
	if( evtType != EVENT_TOUCH )
		return false;
	// Obtém a informação do toque
	const NSArray* hTouchesArray = ( NSArray* )pParam;
	
	// OBS : Acho que isso nunca acontece, mas...
	uint8 nTouches = [hTouchesArray count];
	if( nTouches == 0 )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
		{
			int8 touchIndex = -1;
			for( uint8 i = 0 ; ( i < nTouches ) && ( nActiveTouches < MAX_TOUCHES ) ; ++i )	
			{
				if( ( touchIndex = startTrackingTouch( [hTouchesArray objectAtIndex:i] ) ) >= 0 )
					onNewTouch( touchIndex, &trackedTouches[i].startPos );
			}
		}
			return true;
			
		case EVENT_TOUCH_MOVED:
			// Se possuímos mais de um toque, estamos recebendo uma operação de zoom
#if MAX_TOUCHES > 1
			if( nActiveTouches > 1 ) // No caso deste jogo, > 1 será sempre == 2 
			{
				if( ( nTouches == 1 /*|| state != DOT_GAME_STATE_OBSERVING || state!= DOT_GAME_STATE_PLAYER_WAITING*/) )
					return false;
				
				
				int8 touchIndex = -1;
				Point3f aux[ MAX_TOUCHES ] = { trackedTouches[0].startPos, trackedTouches[1].startPos };
				
				for( uint8 i = 0 ; i < nTouches ; ++i )
				{
					UITouch* hTouch = [hTouchesArray objectAtIndex:i];
					if( ( touchIndex = isTrackingTouch( hTouch ) ) >= 0 )
					{
						CGPoint currTouchPos = [hTouch locationInView: NULL];
						aux[ touchIndex ].set( currTouchPos.x, currTouchPos.y );
					}
				}
				
				// Se os toques estão se aproximando, é um zoom out
				float currZoom = pCurrCamera->getZoomFactor();
				float newDistBetweenTouches = ( aux[0] - aux[1] ).getModule();
				float movement = fabsf( newDistBetweenTouches - initDistBetweenTouches );
				if( movement < ZOOM_MIN_DIST_TO_CHANGE )
					return false;
				
				if( NanoMath::fltn( newDistBetweenTouches, initDistBetweenTouches ) )
				{

					float zoomMin = adjustMinZoom();					
					float newZoom = currZoom * powf( ZOOM_OUT_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
					
					if( newZoom < zoomMin )//ZOOM_MIN )
						pCurrCamera->zoom( zoomMin );//ZOOM_MIN );
					else
						pCurrCamera->zoom( newZoom );
				}
				// Se os toques estão se afastando, é um zoom in
				else if( NanoMath::fgtn( newDistBetweenTouches, initDistBetweenTouches ) )
				{
					float newZoom = currZoom * powf( ZOOM_IN_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
					
					if( newZoom > ZOOM_MAX )
						pCurrCamera->zoom( ZOOM_MAX );
					else
						pCurrCamera->zoom( newZoom );
				}
				
				// Garante que o zoom não fez a câmera extrapolar os limites
#if ! CONFIG_FREE_CAM_MOVEMENT
				onMoveCamera( 0.0f, 0.0f );
#endif
				
#if DEBUG
				
				LOG( "Curr Camera Zoom: %.3f\n", pCurrCamera->getZoomFactor() );
				
#endif
				
				initDistBetweenTouches = newDistBetweenTouches;
			}
			
			else
			{
#endif
				UITouch* hTouch = [hTouchesArray objectAtIndex:0];
				int8 touchIndex = isTrackingTouch( hTouch );
				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					CGPoint lastPos = [hTouch previousLocationInView:NULL];
					
					Point3f touchCurrPos( currPos.x, currPos.y );
					Point3f touchLastPos( lastPos.x, lastPos.y );
					
					onSingleTouchMoved( touchIndex, &touchCurrPos, &touchLastPos );
				}
#if MAX_TOUCHES > 1
			}
#endif
			return true;
			
			
		case EVENT_TOUCH_CANCELED:
		case EVENT_TOUCH_ENDED:
			for( uint8 i = 0 ; i < nTouches ; ++i )
			{
				UITouch* hTouch = [hTouchesArray objectAtIndex:i];
				int8 touchIndex = isTrackingTouch( hTouch );
				
				if( touchIndex >= 0 )
				{
					CGPoint tmp = [hTouch locationInView:NULL];
					Point3f currPos;
					currPos.set( tmp.x, tmp.y );
					currPos = Utils::MapPointByOrientation( &currPos );
					
					Point3f touchPos( currPos.x, currPos.y );
					
//#if DEBUG
					// Verifica se houve um toque duplo
					if( [hTouch tapCount] == 2 )
					{
						// OBS: Não é utilizado nesse jogo
						// Envia evento de toque duplo
						onDoubleClick( touchIndex );
					}
//#endif
					
					onTouchEnded( touchIndex, &touchPos );
					
					// Cancela o rastreamento do toque
					stopTrackingTouch( touchIndex );
				}
			}
			return true;
	}
	return false;
}

// Chamado quando o sprite muda de etapa de animação
void DotGame::onAnimStepChanged( Sprite* pSprite ){
}

// Chamado quando o sprite termina um sequência de animação
void DotGame::onAnimEnded( Sprite* pSprite ){
}


//chamado qdo o power up é liberado!!!
void DotGame::PowerUpEffect( PowerUp* p ){

	switch ( p->getPowerUpType() ) {
			
		case POWER_UP_TYPE_TIME_SHORT :
			NSLog(@"efeito de reduzir o tempo");
			break;
			
		case POWER_UP_TYPE_MOVE_RANDOM:
			NSLog(@"escolhe lihas aleatoriamente");
			break;
			
		case POWER_UP_TYPE_MOVE_CHOOSE:
			NSLog(@"movimento extra");
			break;
			
		case POWER_UP_TYPE_COMBO_BREAK:
			NSLog(@"muda de jogador automaticamente");
			break;
			
	}
	
	
}
// Método chamado para indicar que a animação do controle foi finalizada
void DotGame::onInterfaceControlAnimCompleted( InterfaceControl* pControl ){
	
	switch ( state ) {
		case DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_HIDING_MSG_CPU_WIN:
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
			break;
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER :
			//			setDotGameState( DOT_GAME_STATE_GAME_OVER );
			//setDotGameState( DOT_GAME_STATE_HIDING_MSG_GAME_OVER );
			break;
		case DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER:
			changePlayer();
			//			setDotGameState( DOT_GAME_STATE_ );
			break;
			
		case DOT_GAME_STATE_HIDING_MSG_GAME_OVER:{
			setDotGameState( DOT_GAME_STATE_GAME_OVER );
		}
			break;
			
		default:
			break;
	}
}
/*========================================-==-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-==-=-=-
 
 onStopTimeReached->Método chamado quando o cronômetro termina sua contagem
 
 ========================================-==-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-==-=-=-*/
void DotGame::onStopTimeReached( void ){
	
	switch ( state ) {
		case DOT_GAME_STATE_CPU_PLAYING :
		case DOT_GAME_STATE_PLAYER_WAITING :
			setDotGameState(DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER);
			//changePlayer();
			//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
			break;
		case DOT_GAME_STATE_AFTER_CPU_MOVE:
		case DOT_GAME_STATE_AFTER_PLAYER_MOVE:
		case DOT_GAME_STATE_ANIMATING_LINE:
		case DOT_GAME_STATE_ANIMATING_VICTORY:
		case DOT_GAME_STATE_PAUSED:
		case DOT_GAME_STATE_SHOWING_INFO_PLAYER:
		case DOT_GAME_STATE_GAME_OVER:
		case DOT_GAME_STATE_OBSERVING:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
		case DOT_GAME_STATE_FOCUSING_LINE:
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER:
		case DOT_GAME_STATE_HIDING_MSG_GAME_OVER:
			break;
	}
	
}


/*===================================================================================
 
 Método chamado quando a transição termina
 
 ===================================================================================*/

void DotGame::onOGLTransitionEnd( void ){
	//colocar todos os estados possíveis aqui e a reação à cada um deles...
	switch ( state ) {
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1 :
			pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0f);
			pChronometer->pause( true );
			pCurrCamera->setPosition( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT, 1.0f );
			setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_2 );
			break;
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
			//changePlayer();		
			pChronometer->pause( false );
			//setDotGameState( DOT_GAME_STATE_PLAYER_WAITING );
			break;
			
		default:
			break;
	}
	
}	
/*==========================================================================================
 
 Indica que a animação da câmera terminou
 
 =========================================================================================*/
void DotGame::onCameraAnimEnded( void ){
	animCamera = false;
	
	switch ( state ) {
		case DOT_GAME_STATE_FOCUSING_LINE:
			if( currPlayer->getTypeUser() == PLAYER_USER_CPU ){
			if( vertical ){
				lineV[ choice.x ][ choice.y ]->lineGameCpuGained( currPlayer );
				linesSelected++;
				ZoneVerticalLeftOccupied( choice.x , choice.y );
				ZoneVerticalRightOccupied( choice.x , choice.y );		
				//	NSLog(@"ocupada linha vertical no. %d %d ", choice.x, choice.y);				
			}else {
				
				lineH[choice.x ][ choice.y]->lineGameCpuGained( currPlayer );
				linesSelected++;
				ZoneHorizontalSuperiorOccupied( choice.x , choice.y ) ;
				ZoneHorizontalInferiorOccupied( choice.x , choice.y ) ;		
				//	NSLog(@"ocupada linha horizontal no. %d %d ", choice.x , choice.y );
				
			}
			
			setDotGameState( lastState == DOT_GAME_STATE_CPU_PLAYING ?  DOT_GAME_STATE_AFTER_CPU_MOVE : DOT_GAME_STATE_PLAYER_WAITING );
			}else{
				
#if DEBUG
				NSLog(@"verificar as zonas");
				Point3f tmp;
				tmp = movesMake.back() ;
				NSLog(@"movimento: x=%4.0f y=%4.0f", tmp.x, tmp.y );
				
#endif
				bool res = false;
				if( !movesMake.empty() ){
					
					//são 5 ptos pq são as 4 linhas + zonas
					Point3f p, p1, p2, p3, p4;
					uint8 x,y;
					//linha pressionada 
					p = movesMake.back();					
					lineManager->selectLine( &p, currPlayer );
			
	//calcular a variação da zona e das outras 3 linhas para calcular se a zona está ocupada ou não. com isso, pode-se calcular as variações sem depender da tela de jogo. depois fazer o mesmo para a outra zona( direita ) deselecionar e o mesmo para os PWups
					
					if ( lineManager->isLineHorizontal( &p ) ){
						//uint8 x,y;
						//colocar no vertical!!!!! lembrar de mudar as variáveis
						//x = p.x / (  LINE_HORIZONTAL_HEIGHT + ZONE_SIDE)  ;
						//y =  ( p.y - LINE_HORIZONTAL_HEIGHT )/( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE );
						
						x =  ( p.x - LINE_HORIZONTAL_HEIGHT ) / ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE );						
						y = p.y / (  LINE_HORIZONTAL_HEIGHT + ZONE_SIDE);
						
						//linha horizontal superior
						p1.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
							   ( y - 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
							   1.0);
						//linha vertical esquerda superior
						p2.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
							   LINE_VERTICAL_WIDTH + ( ( y - 1 ) * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
							   1.0);
						//linha vertical direita superior
						p3.set(( x + 1 ) * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
							   LINE_VERTICAL_WIDTH + ( ( y - 1 ) * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
							   1.0);
						//zona/quad selecionado
						p4.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
							   LINE_VERTICAL_WIDTH + ( ( y - 1 ) * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
							   1.0f); 
						

#if DEBUG
						bool t1,t2,t3,t4;
						t1 = lineManager->isLineGained( &p )  ;
						t2 = lineManager->isLineGained( &p1 ) ;
						t3 = lineManager->isLineGained( &p2 ) ;
						t4 = lineManager->isLineGained( &p3 );
#endif
						/*
						 colocar o cálculo para TODOS os ptos 
						 */
						
						if( lineManager->isLineGained( &p )  &&
						   lineManager->isLineGained( &p1 ) &&
						   lineManager->isLineGained( &p2 ) &&
						   lineManager->isLineGained( &p3 ) ){
							
							res |= zoneManager->zoneGainedByPlayer( &p4, currPlayer);
							linesSelected--;
						}
						
						p1.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
							   ( y + 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
							   1.0);
						//linha vertical esquerda superior
						p2.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
							   LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
							   1.0);
						//linha vertical direita superior
						p3.set(( x + 1 ) * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
							   LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
							   1.0);
						//zona/quad selecionado
						p4.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
							   LINE_VERTICAL_WIDTH + ( y  * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
							   1.0f); 
						
						if( lineManager->isLineGained( &p )  &&
						   lineManager->isLineGained( &p1 ) &&
						   lineManager->isLineGained( &p2 ) &&
						   lineManager->isLineGained( &p3 ) ){
							
							res |= zoneManager->zoneGainedByPlayer( &p4, currPlayer);
							linesSelected--;
						}
						
					}
					else /*linha vertical*/{
						
						//ZoneVerticalLeftOccupied
						//colocar no vertical!!!!! lembrar de mudar as variáveis
						x = p.x / (  LINE_HORIZONTAL_HEIGHT + ZONE_SIDE)  ;
						y = ( p.y - LINE_HORIZONTAL_HEIGHT )/( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE );

						
						//linha horizontal superior
						p1.set( LINE_HORIZONTAL_HEIGHT + ( ( x - 1) * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
							   ( y ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
							   1.0);
						//linha horizontal esquerda superior
						p2.set(  LINE_HORIZONTAL_HEIGHT + ( ( x - 1) * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
							   ( y + 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
							   1.0);
						//linha vertical direita superior
						p3.set(( x - 1 ) * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
							   LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
							   1.0);
						//zona/quad selecionado
						p4.set( LINE_HORIZONTAL_HEIGHT + ( ( x - 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),							   LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
							   1.0f); 
								
						if( lineManager->isLineGained( &p )  &&
						   lineManager->isLineGained( &p1 ) &&
						   lineManager->isLineGained( &p2 ) &&
						   lineManager->isLineGained( &p3 ) ){
							
							res |= zoneManager->zoneGainedByPlayer( &p4, currPlayer);
							linesSelected--;
						}
						//ZoneVerticalRightOccupied
						//linha horizontal superior
						p1.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
							   ( y ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
							   1.0);
						//linha horizontal esquerda superior
						p2.set(LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
							   ( y + 1 ) * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
							   1.0);
						//linha vertical direita superior
						p3.set(( x + 1 ) * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
							   LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
							   1.0);
						//zona/quad selecionado
						p4.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
							   LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
							   1.0f); 
						
						if( lineManager->isLineGained( &p )  &&
						   lineManager->isLineGained( &p1 ) &&
						   lineManager->isLineGained( &p2 ) &&
						   lineManager->isLineGained( &p3 ) ){
							
							res |= zoneManager->zoneGainedByPlayer( &p4, currPlayer);
							linesSelected--;
						}
						
					}
					
					
					movesMake.pop_back();
					pInfoBar->updateScores( this );

					setDotGameState( DOT_GAME_STATE_AFTER_PLAYER_MOVE );
				}else {
					if( !res ){
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );}
					else {
						linesSelected = 0;
					}

				}
			}
			
			break;
		case DOT_GAME_STATE_CPU_PLAYING:
			break;
		default:
			break;
	}
	
	
}


/*===========================================================================================
 carrega os jogadores, e os seus respectivos bonecos...
 
 ===========================================================================================*/
bool DotGame::loadPlayers( void ){
	//Anjinho::loadAllImages();
	
	for (uint8 c = 0 ; c < nPlayers ; c++) {
		players[ c ] = new Player();
		players [ c ]->LoadInfo( GInfo, c );
		if( !players[ c ] ){
#if DEBUG
			NSLog( @"falha ao carregar o players" );
#endif
			return false;
		}
		players [ c ]->setTypeUser( PLAYER_USER_CPU );	
	}
	
	currPlayer = players[ 0 ];	
	//players [ 1 ]->setTypeUser( PLAYER_USER_USER );	
	
	
	//acerta os jogadores de acordo com o modo de jogo, o ideal seria se fosse em LoadInfo
	switch ( mode ) {
		case DOT_GAME_MODE_VERSUS_PLAYER :
			players[ 0 ]->setTypeUser( PLAYER_USER_USER );
			//break;
		case DOT_GAME_MODE_VERSUS_CPU :
			players[ 1 ]->setTypeUser( PLAYER_USER_USER );
			break;
	}
	return true;
}


/*===========================================================================================
 
 MÉTODO startTrackingTouch
 Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
 fazê-lo.
 
 ============================================================================================*/
int8 DotGame::startTrackingTouch( const UITouch* hTouch ){
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == 0 )
		{
			++nActiveTouches;
			
			trackedTouches[i].Id = ( int32 )hTouch;
			
			CGPoint aux = [hTouch locationInView: NULL];
			trackedTouches[i].unmappedStartPos.set( aux.x, aux.y );
			Point3f p;
			p.set( aux.x, aux.y );
			
			//aux = Utils::MapPointByOrientation( &aux );
			p = Utils::MapPointByOrientation( &p );
			trackedTouches[i].startPos.set( p.x, p.y );
			
			if( nActiveTouches == 2 )
				initDistBetweenTouches = ( trackedTouches[0].startPos - trackedTouches[1].startPos ).getModule();
			
			return i;
		}
}
	return -1;	
}


/*===============================================================================
 
 MÉTODO stopTrackingTouch
 Cancela o rastreamento do toque recebido como parâmetro. 
 
 ===============================================================================*/
void DotGame::stopTrackingTouch( int8 touchIndex ){
	if( touchIndex < 0 )
		return;
	
	if( trackedTouches[ touchIndex ].Id != 0 )
	{
		--nActiveTouches;
		trackedTouches[ touchIndex ].Id = 0;
		initDistBetweenTouches = 0.0f;
	}
}

/*===========================================================================================
 
 MÉTODO onSingleTouchMoved
 Trata eventos de movimentação de toques.
 
 ============================================================================================*/
void DotGame::onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos ){
	/*
	 
	 todoo: colocar todos os estados possíveis aqui e a reações possíveis em cada um desses estados!!!
	 
	 */
	
	switch ( state ) {
	//	case DOT_GAME_STATE_PLAYER_WAITING:
			//break;
		case  DOT_GAME_STATE_OBSERVING :
		{
			Ray r;
			getPickingRay( r, *pCurrPos );
			
		//	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
			Point3f currPos = Utils::MapPointByOrientation( pCurrPos );
			Point3f lastPos = Utils::MapPointByOrientation( pLastPos );
			
			onMoveCamera( ( currPos.x - lastPos.x ) /** invZoom*/, ( currPos.y - lastPos.y ) /** invZoom*/ );			
		}	
			break;
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1 :
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2 :				
			break;
		case DOT_GAME_STATE_PLAYER_WAITING:
	
		case DOT_GAME_STATE_GAME_OVER:{
			Point3f currPos = Utils::MapPointByOrientation( pCurrPos );

			buttonsManager->idButtonPressed( &currPos );
		}
			break;

	}
	
}

/*===========================================================================================
 
 MÉTODO isTrackingTouch
 Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja sendo
 acompanhado.
 
 ============================================================================================*/

int8 DotGame::isTrackingTouch( const UITouch* hTouch ){
	
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == ( int32 )hTouch )
			return i;
	}
	return -1;
	
}

/*===========================================================================================
 
 MÉTODO onMoveCamera
 Movimenta a câmera respeitando as restrições de posição.
 
 ============================================================================================*/

void DotGame::onMoveCamera( float dx, float dy ){
	/*Point3f p;
	p.set(  nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
		  nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
		  1.0);
	/*p *= 0.5f;*/
	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	float halfCamViewVolumeWidth = ( SCREEN_WIDTH * invZoom ) * 0.5f;
	float halfCamViewVolumeHeight = ( SCREEN_HEIGHT * invZoom ) * 0.5f;

	Point3f camPos = *( pCurrCamera->getPosition() );
	
	camPos.x += dx;
	camPos.y += dy ;
//	
//	Point3f lim2, min2;
//
//	min2 = *bg->getObject( 0 )->getPosition();//getPosition();
//	
//	Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT);
//	
//	min2 += *lineManager->getPosition() + (sc * 1.25) ;
//	
//	lim2.set( ( nHorizontalLines + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
//				  ( ( nVerticalLines + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ),
//				  1.0);
//	
////	lim2 += *lineManager->getPosition();
//	
//	
//	if( lim2.x >= SCREEN_WIDTH /*&& max.y <= SCREEN_HEIGHT*/ ){
//		
//		if( camPos.x  + halfCamViewVolumeWidth > lim2.x ){ 
//			
//			camPos.x -=  ( camPos.x + halfCamViewVolumeWidth ) -lim2.x;
//		} else	if( camPos.x - halfCamViewVolumeWidth < min2.x  ){
//			
//			camPos.x += min2.x - ( camPos.x - halfCamViewVolumeWidth );
//		}
//	}else 
//		if( camPos.x > lim2.x ){
//			camPos.x = lim2.x;
//		}else if( camPos.x   < (min2.x + lineManager->getPosition()->x  )) {
//			camPos.x = (min2.x + lineManager->getPosition()->x  );
//		}
//		
//	
//	
//	if( lim2.y > SCREEN_HEIGHT /*&& max.y <= SCREEN_HEIGHT*/ ){
//		if( camPos.y - halfCamViewVolumeHeight < ( min2.y + 50.0 ) ){
//			
//			camPos.y += ( min2.y + 50.0 ) - ( camPos.y - halfCamViewVolumeHeight ) ;
//		}else	if( camPos.y + halfCamViewVolumeHeight > lim2.y + 50 ) {	
//			camPos.y  -= ( camPos.y + halfCamViewVolumeHeight ) -  ( lim2.y + 50 ) ;
//			
//		}
//	}else 
//		if( camPos.y > lim2.y ){
//			camPos.y = lim2.y;
//		}else if( camPos.y   < (min2.y + lineManager->getPosition()->y )) {
//			camPos.y = (min2.y + lineManager->getPosition()->y );
//		}
	
	pCurrCamera->setPosition(camPos.x, camPos.y, 0.0f ); 	
	
#if DEBUG 
	 Point3f currCamPos = *( pCurrCamera->getPosition() );
	// LOG( "Cam Pos: x = %.3f, y = %.3f -> %.3f, %.3f -> %.3f, %.3f\n", currCamPos.x, currCamPos.y/*, min2.x, min2.y, lim2.x, lim2.y */);
#endif
 }
/*===========================================================================================
 
 MÉTODO getPickingRay
 Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
 jogo.
 
 ============================================================================================*/

void DotGame::getPickingRay( Ray& ray, const Point3f& p ) const{
	
	
	Utils::GetPickingRay( &ray, pCurrCamera, p.x, p.y );
	
	// Corrige a origem do raio, já que esta será sempre a posição do observador (centro da tela)
	// OBS: Esta abordagem está PORCA!!!!! O certo seria utilizarmos:
	// Utils::Unproject( &p, pCurrCamera, &trackedTouches[ touchIndex ].unmappedStartPos );
	// No entanto, a saída de Utils::Unproject não está produzindo valores corretos com a projeção ortogonal
	
	float invZoom = 1.0f;// / pCurrCamera->getZoomFactor();
	Point3f screenCenter( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	ray.origin += ( Utils::MapPointByOrientation( &p ) - screenCenter ) * invZoom;
	
	
}


/*======================================================================
 
 Trata eventos de início de toque
 
 ======================================================================*/
void DotGame::onNewTouch( int8 touchIndex, const Point3f* pTouchPos ){
	/*
	 todoo: colocar todos os estados possíveis aqui e a reações possíveis em cada um desses estados!!!
	 */
	
	// if( mode == DOT_GAME_MODE_EXIBITHION )
	//	goodBye = true;
	
	switch ( state ) {
		case DOT_GAME_STATE_PLAYER_WAITING:
			
			/*			if( nActiveTouches > 1 )
			 {
			 break;
			 }
			 
			 
			 break;*/
		case DOT_GAME_STATE_CPU_PLAYING:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			break;
		case DOT_GAME_STATE_GAME_OVER:
			buttonsManager->idButtonPressed( pTouchPos );
			break;
			
		default:
			break;
	}
	
}

/*======================================================================
 
 Trata eventos de fim de toque
 
 ======================================================================*/

void DotGame::onTouchEnded( int8 touchIndex, const Point3f* pTouchPos ){
	/*
	 todoo: colocar todos os estados possíveis aqui e a reações possíveis em cada um desses estados!!!
	 */
	switch ( state ) {
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			break;


		case DOT_GAME_STATE_PLAYER_WAITING :
		{
			//if( dispMoves >= 1 ){
			if( currPlayer->getTypeUser() == PLAYER_USER_USER ){
				linePressed( pTouchPos );
				/*if( linePressed( &p ) ){
				 NSLog(@"jogador %d possui %d pontos",indicePlayer , currPlayer->getScore() );
				 //linesSelected = -1;
				 pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0f);
				 
				 }else {
				 //	if( linesSelected >= 1 ){
				 //						NSLog(@" trocar de player ");
				 //						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
				 //					}
				 }
				 }*/
			}
			//}
		}	
		case DOT_GAME_STATE_GAME_OVER:
		case DOT_GAME_STATE_OBSERVING:
		case DOT_GAME_STATE_CPU_PLAYING:{
			uint8 b;
			if( buttonsManager->idButtonPressed(pTouchPos, &b) ){
				switch ( b ) {
					case ID_BUTTON_LINE_VIEW:
						switch ( state ) {
							case DOT_GAME_STATE_OBSERVING :
								pChronometer->pause( false );
								lineManager->setAllLinesLastState();
								setDotGameState( lastState );
								break;
							case DOT_GAME_STATE_PLAYER_WAITING:
							case DOT_GAME_STATE_CPU_PLAYING :
								setDotGameState( DOT_GAME_STATE_OBSERVING );
								/*
								 //	pChronometer->pause( !pChronometer->isPaused() );
								 //					if( pChronometer->isPaused() ){
								 //						lineManager->setAllLinesObserving();
								 //						return;
								 //					}else {
								 //						lineManager->setAllLinesLastState();
								 //						return;
								 //					}
								 */
								break;
						}
						break;
					
					case ID_BUTTON_YES:
						switch ( state ) {
							case DOT_GAME_STATE_GAME_OVER:
								resetTable();
								break;
								
						}
						break;
						
					case ID_BUTTON_NO:
						switch ( state ) {
							case DOT_GAME_STATE_GAME_OVER:
								goodBye = true;
								break;
						}
						break;
					case ID_BUTTON_CONFIRM_MOVE:
						if( state == DOT_GAME_STATE_PLAYER_WAITING ){
							setDotGameState( DOT_GAME_STATE_AFTER_PLAYER_MOVE );
						}
						break;
				}
#if DEBUG
				NSLog(@"botao indice %d",b);
				NSLog(@" nome botao %s\n", buttonsManager->getObject( 7 + b)->getName() );
#endif
			}
		}break;
			
			
		default:
			break;
	}
}
//}

/*-================================================================
 
 Move a câmera para a posição 'position' real em 'time' segundos
 
 ===================================================================*/
void DotGame::moveCameraTo( const Point3f* pRealPosition, float time ){
	moveCameraTo( pRealPosition, /*pCurrCamera->getZoomFactor()*/1.0f, time );
	
}

void DotGame::moveCameraTo( const Point3f* pRealPosition, float finalZoom, float time ){
	//	if( !movingCamera )
	//		movingCamera = true;
	//	else {
	//		return;
	//	}
	
	Point3f lim2, min2;
//	lim2 = lim;
//	min2 = min;
	lim2 = *bg->getObject( 0 )->getSize();
	min2 = *bg->getObject( 0 )->getPosition();
	Point3f sc(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
// SCREEN_WIDTH, SCREEN_HEIGHT); //HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	lim2.x -= SCREEN_WIDTH * 1.2;
	min2 += sc;
	Point3f d = *pRealPosition;
	d += *lineManager->getPosition();
	
	if( d.x <= min2.x ){
		d.x = min2.x;
	}else if( d.x >= lim2.x) {
		d.x = lim2.x ;
	} 
	
	if( d.y <= min2.y ){
		d.y = min2.y;
	}else if( d.y > lim2.y ){ 
		d.y = lim2.y;
	}

	d = *pRealPosition;

	//NSLog(@"destino: ( %3.2f, %3.2f, %3.2f )", d.x, d.y, d.z);
	AnimatedCamera *pSceneCamera = dynamic_cast<AnimatedCamera*> ( pCurrCamera );
	pSceneCamera->setAnim(&d, finalZoom, time);
	pSceneCamera->startAnim();
	animCamera = true;
	if ( ( time <= 0.0f ) || ( ( d == *( pSceneCamera->getPosition() ) ) && ( pSceneCamera->getZoomFactor() == finalZoom ) ) ) {
		pSceneCamera->endAnim();
	}
	
}


/*=============================================================================================================
 
 retorna a linha que foi pressionada
 
 =============================================================================================================*/
bool DotGame::linePressed( const Point3f *p ){
	
#if DEBUG
	NSLog(@"linha pressionada %d",movesMake.size() );

#endif
	
	bool /*r1,r2,*/r3;
	r3 = false;
	Point3f ret =  getRelativePosition( p );
	for ( uint8 x = 0; x < nHorizontalZones ; x++ ) 
		for ( uint8 y = 0; y < nVerticalLines; y++) {
			
			if(	lineH[ x ][ y ]->checkCollision( &ret , currPlayer)){
			/*	if(	lineH[ x ][ y ]->getLineGameState() == LINE_GAME_STATE_OCCUPIED ){
					r1 = ZoneHorizontalSuperiorOccupied( x , y);
					r2 = ZoneHorizontalInferiorOccupied( x , y);
					linesSelected++;
					choice.set( x, y );
					r3 =  r1 || r2 ;*/
				choice.set( x, y );
				inputMoves(/* &ret */lineH[ x ][ y ]->getPosition() );
		//	dispMoves -= 1;
				//	movesMake.push_back( choice );
				vertical = false;
			}else {
			//este trecho de código serve para resetar as linhas que não forem selecionadas, sim eu sei, meio porco, mas nada melhor me passa pela cabeça no momento...
				for( uint8 i = 0; i < movesMake.size() ;i++ ){
				 
					if( /*ret*/*lineH[ x ] [ y ]->getPosition()!= movesMake[ i ] ){
						
						lineH[ x ] [ y ]->restart();
					
					}else
						break;
				}
			}
		}	
	

	
	for ( uint8 x = 0; x < nHorizontalLines ; x++ ) 
		for ( uint8 y = 0; y < nVerticalZones ; y++) {
			
			if(	lineV[ x ][ y ]->checkCollision( &ret , currPlayer)){
			/*	if( lineV[ x ][ y ]->getLineGameState() == LINE_GAME_STATE_OCCUPIED ){
					r1 = ZoneVerticalLeftOccupied( x , y);
					r2 = ZoneVerticalRightOccupied( x , y );
					linesSelected++;
					r3 = r1 || r2;
					choice.set( x, y );
				}*/
				vertical = true;
				choice.set( x, y );
				
				inputMoves(/* &ret */lineV[ x ][ y ]->getPosition()  );
			//	dispMoves -= 1;
			}else {
		//este trecho de código serve para resetar as linhas que não forem selecionadas, sim eu sei, meio porco, mas nada melhor me passa pela cabeça no momento...
				for( uint8 i = 0; i < movesMake.size() ;i++ ){
					
					if( /*ret*/*lineV[ x ] [ y ]->getPosition() != movesMake[ i ] ){
						
						lineV[ x ] [ y ]->restart();
						
					}else {
						break;
					}
					
				}
				
			}
			
		}
	
	
	return r3;	
}


/*=======================================================================================================
 
 // Trata eventos de clique duplo
 onDoleClick->eventos de toque duplo :-p 
 
 ==========================================================================================================*/
void DotGame::onDoubleClick( int8 touchIndex ){
	
	
	switch ( state ) {
		case DOT_GAME_STATE_CPU_PLAYING:


			break;
		case DOT_GAME_STATE_PLAYER_WAITING :
//	if( currPlayer->getTypeUser() == PLAYER_USER_USER ){
//		Point3f p ;
//		p = Utils::MapPointByOrientation( &trackedTouches[ touchIndex ].unmappedStartPos );//&p );
//				if( linePressed( &p ) ){
//					NSLog(@"jogador %d possui %d pontos",indicePlayer , currPlayer->getScore() );
//					//linesSelected = -1;
//					pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0f);
//					
//				}else {
//					if( linesSelected >= 1 ){
//						NSLog(@" trocar de player ");
//						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
//						//changePlayer();
//						//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
//					}
//				}
//			}
//			if( !haveFreeZone() ){
//				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
//					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
//				else {
//					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
//				}
//				
//				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
//			}
			
		//	pChronometer->pause( true );
			break;
			
		case DOT_GAME_STATE_OBSERVING :
		//	resetCam();
			//setDotGameState( lastState );
			//NSLog(@"saindo em modo de observação");
			//pChronometer->pause( false );
			break;
			
			
		case DOT_GAME_STATE_ANIMATING_TRANSITION_1:
		case DOT_GAME_STATE_ANIMATING_TRANSITION_2:
			break;
			
			
		default:
			break;
	}
	
}

/*=======================================================================================================
 
 changePlayer-> troca os jogadores de acordo com a sua vez de jogar
 
 =======================================================================================================*/
void DotGame::changePlayer( void ){
	//NSLog(@"cpu no. %d fez %d pontos ",indicePlayer, currPlayer->getScore() );
	//NSLog(@"jogador trocado!!");
	if( !haveFreeZone() ){
	//	NSLog(@"acabou as zonas...");
		if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
		else {
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
		}
		
		return;
	}
	
	//sim seria melhor uma lista circular, mas...	
	if ( ( ++indicePlayer ) >= nPlayers ) {
		indicePlayer = 0;
	}
	
	linesSelected = 0;
	combo = 0;
	currPlayer = players[ indicePlayer ];
	dispMoves = 3;//1;
	choice.set( 0, 0 );
	cZone.set( 0, 0 );
	stateTimeCounter = 0.0f;
	lineManager->restartAllLines();
	movesMake.clear();
	pChronometer->setTimeInterval( MAX_TIME_TO_PLAYER_MAKE_MOVE, 0.0f );
	
	if( mode == DOT_GAME_MODE_EXIBITHION && ++movesCount > movesMax )
		goodBye = true;
	currPlayer->updatePowerUp();
	
	if( currPlayer->getTypeUser() == PLAYER_USER_USER ){
		setDotGameState( DOT_GAME_STATE_PLAYER_WAITING );
	}else{
		setDotGameState( DOT_GAME_STATE_CPU_PLAYING );
	}
}

/*==========================================================================================
 
 setDotGameState-> muda o estado da partida e faz os ajustes necessários
 
 ========================================================================================== */
void DotGame::setDotGameState( DotGameState d ){
	lastState = state;
	state = d;
	switch ( d ) {
			
		case DOT_GAME_STATE_AFTER_PLAYER_MOVE:{
			if( !movesMake.empty() ){
				moveCameraTo( &movesMake.back(), TIME_FOCUS_LINE ); 
				setDotGameState( DOT_GAME_STATE_FOCUSING_LINE );
				return;
			}
			//if( dispMoves <= 0 )
			if( !haveFreeZone() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU ){
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
					return;
				}else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
					return;
				}
			}	
			setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
			//else {
//					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
//				}
				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
			}
		
			break;
		
		case DOT_GAME_STATE_AFTER_CPU_MOVE:
			if( linesSelected >= 1 )
				//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
				//changePlayer();
				setDotGameState( DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER );
			else {
				if( !haveFreeZone() ){
					if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
					else {
						setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
					}
				}				
				
				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
				setDotGameState( DOT_GAME_STATE_CPU_PLAYING );
			}
			pChronometer->pause( false );
			break;
		case DOT_GAME_STATE_ANIMATING_LINE:break;
		case DOT_GAME_STATE_PLAYER_WAITING:
			//break;
		case DOT_GAME_STATE_CPU_PLAYING:
			pChronometer->pause( false );
			pChronometer->setTimeInterval(MAX_TIME_TO_PLAYER_MAKE_MOVE, 1.0);
			break;
		case DOT_GAME_STATE_FOCUSING_LINE :
			pChronometer->pause( true );
			break;
			
			break;
			
		case  DOT_GAME_STATE_ANIMATING_TRANSITION_1 :
			pCurrTransition = pSliceTransition;
			pCurrTransition->reset();
			break;
			
		case  DOT_GAME_STATE_ANIMATING_TRANSITION_2 :
			switch ( Random::GetInt( 0 ,2 ) ) {
				case 0:
					//pCurrTransition = pClosingDoorTransition;
					break;
				case 1:
					pCurrTransition = pCurtainLeftTransition;
					break;
				case 2:
					pCurrTransition = pCurtainTransition;
					break;
				default:
					break;
					
			}
			
			pCurrTransition->reset();
			break;	
			
		case DOT_GAME_STATE_GAME_OVER:
			static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_YES ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
			static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_NO ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN );
			buttonsManager->getObject( INDEX_BUTTON_LINE_VIEW )->setVisible( false );
			//nextLevel();
			//resetTable();
			/*
			 NSLog(@" fim de jogo ");
			 for (uint8 i = 0; i < nPlayers; i++) {
			 NSLog(@" jogador no. %d fez %d pontos ", i, players[ i ]->getScore() );
			 }*/
			
			//pChronometer->pause(true);
			//setDotGameState( DOT_GAME_STATE_OBSERVING );
			break;
		case DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER:
			if( !haveFreeZone() ){
				if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
				else {
					setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
				}
				
				//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
			
			}
		case DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_CPU_WIN:
		case DOT_GAME_STATE_SHOWING_MSG_GAME_OVER:
		{
			
			pChronometer->pause(true);
			FeedbackMsg *tmp;
			if( state == DOT_GAME_STATE_SHOWING_MSG_GAME_OVER ){
				stateDuration = DURATION_TIME_MSG_GAME_OVER;
				tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_GAME_OVER ) );
			}else if( state == DOT_GAME_STATE_SHOWING_MSG_NEXT_PLAYER ){
				stateDuration = 0.5;
				if( players [ ( ( indicePlayer  ) >= nPlayers-1 ) ? 0 : indicePlayer + 1 ]->getTypeUser() == PLAYER_USER_USER )
					tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER ) );
				else {
					tmp =  static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER + 1 ) );
				}
				
			}else if( state == DOT_GAME_STATE_SHOWING_MSG_CPU_WIN )
			{
				stateDuration = 0.5;
				tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER + 2 ) );
			}
			else if( state == DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN )
			{
				stateDuration = 0.5;
				tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER + 3 ) );
			}
			
			
			tmp->startShowAnimation();
		}
			break;
		case DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN:
		case DOT_GAME_STATE_HIDING_MSG_CPU_WIN:
		case DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER:
		case DOT_GAME_STATE_HIDING_MSG_GAME_OVER:
		{
			FeedbackMsg *tmp;
			if( state == DOT_GAME_STATE_HIDING_MSG_GAME_OVER )
				tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_GAME_OVER ) );
			else
				if( state == DOT_GAME_STATE_HIDING_MSG_NEXT_PLAYER )
				{
					if( players [ ( ( indicePlayer  ) >= nPlayers-1 ) ? 0 : indicePlayer + 1 ]->getTypeUser() == PLAYER_USER_USER )
						tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER ) );
					else {
						tmp =  static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER + 1 ) );
					}
				}else 
					if( state == DOT_GAME_STATE_HIDING_MSG_CPU_WIN )
						tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER + 2 ) );
					else 
						if( state == DOT_GAME_STATE_HIDING_MSG_PLAYER_WIN )
							tmp = static_cast< FeedbackMsg* > ( buttonsManager->getObject( INDEX_MSG_NEXT_PLAYER + 3 ) );
			
			tmp->startHideAnimation();			
		}
			break;
		case DOT_GAME_STATE_OBSERVING:
			pChronometer->pause( true );
			lineManager->setAllLinesObserving();
		default:
			break;
	}
}

/*==============================================================================
 
 renderiza tudo...( dãããããã )
 
 ==============================================================================*/
bool DotGame::render( void ){
	if(	Scene::render() )
		return renderTransitions();
	
	return false;
}

/*================================================================================
 
 renderTransitions->renderiza a transition, e talvez alguma mensagem que apareça na tela...
 
 ================================================================================*/
bool DotGame::renderTransitions( void ){
	pControlsCamera->place();
	
	bool ret = false;
	
	ret |= pCurrTransition->render();
	return ret;
}

/*=========================================================================
 
 getRelativePosition->obtém o toque relativo ao deslocamento da câmera
 
 =========================================================================*/

Point3f DotGame::getRelativePosition( const Point3f *p ){
	
	Point3f p1,p2, p3;
	
	p1 = *pCurrCamera->getPosition();
	
	Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	sc += *lineManager->getPosition();
	p2 = p1-sc;
	p3 = *p + p2;
	return p3 ;
}

/*========================================================================================
 
 cpuMove-> faz com que a cpu jogue
 
 ==============================================================================================*/


void DotGame::cpuMove( void ){
	
	if( combo <= limit )
		if ( takeAll3() ) {
			setDotGameState( DOT_GAME_STATE_FOCUSING_LINE );
			return;
		}
	
	for(;;){	
		if( !haveFreeLine() )
		
			break;
		
		if( chooseLineRand() ){
			setDotGameState( DOT_GAME_STATE_FOCUSING_LINE );
			return;
		}
		if( dispMoves <= 0)
			break;
	}
	if( players[ indicePlayerWinner() ]->getTypeUser() == PLAYER_USER_CPU )
		setDotGameState( DOT_GAME_STATE_SHOWING_MSG_CPU_WIN );
	else {
		setDotGameState( DOT_GAME_STATE_SHOWING_MSG_PLAYER_WIN );
	}
	
	//setDotGameState( DOT_GAME_STATE_SHOWING_MSG_GAME_OVER );
	//setDotGameState( DOT_GAME_STATE_ANIMATING_TRANSITION_1 );
	//changePlayer();
}


/* =====================================================================================
 
 escolhe uma linha aleatoriamente
 
 =====================================================================================*/
bool DotGame::chooseLineRand( void ){
	
	uint8 x,y;
	uint8 t3;
	bool retB = false;
	
	t3 = Random::GetInt( 0 , 1) ;
	if( t3 == 0 ){
		vertical = true;
		//vertical
		x = Random::GetInt( 0, nHorizontalLines-1 );
		y = Random::GetInt( 0, nVerticalZones-1 );
		
		if( !lineV[ x ][ y ]->isOccupied() ){
			choice.set( x, y );
			retB = true;
			moveCameraTo( lineV[ x ][ y ]->getPosition() , TIME_FOCUS_LINE );
		}
		
	}else {
		vertical = false;
		//horizontal
		x = Random::GetInt( 0, nHorizontalZones-1 );
		y = Random::GetInt( 0, nVerticalLines-1 );
		
		if( !lineH[ x ][ y ]->isOccupied() ){
			choice.set( x, y );
			retB = true;
			moveCameraTo( lineH[ x ][ y ]->getPosition() , TIME_FOCUS_LINE );
		}
		
	}
	return retB;
}

/*=================================-=-=-=-=-=-=-====-=--===-=-=-==-=-=-==-=-=-
 
 verifica se há zonas livres no jogo
 
 =================================-=-=-=-=-=-=-====-=--===-=-=-==-=-=-==-=-=-*/

bool DotGame::haveFreeZone( void ){
	
	bool r = false;
	for (uint8 x = 0; x < nHorizontalZones; x++) {
		for (uint8 y = 0; y< nVerticalZones ; y++ ){
			if ( zones[ x ][ y ]->getZoneGameState() != ZONE_GAME_STATE_OCCUPIED ) {
				
				return true;
				break;
			}
		}
	}
	return r;
}


/*-====================================================================================-=-
 
 destrutor
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=*/
DotGame::~DotGame(){
	clear();
}

/* -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=
 
 takeAll3 ->fç que serve para tomar os quadrados que tiverem 3 linhas
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=*/
bool DotGame::takeAll3( void ){
	bool ret = false;
	for (uint8 x = 0; x < nHorizontalZones; x++ ) {
		for (uint8 y = 0 ; y < nVerticalZones ; y++ ) {
			
			if( verifyZones( x , y ) ){
				ret = true; 
				//combo++;
				return ret;
			}
		}
	}	
	return ret;
}

/* -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-=
 
 verifica se a zona está com as linhas anteriores ocupadas
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-= */
bool DotGame::verifyZones( uint8 x, uint8 y ){
	uint8 count = 0;
	if( zones[ x ][ y ]->getZoneGameState() != ZONE_GAME_STATE_OCCUPIED ){
		
		if( lineH[ x ][ y ]->isOccupied() )
			count ++;
		
		if( lineV[ x ][ y ]->isOccupied() )
			count ++;
		
		if( lineH[ x ][ y + 1  ]->isOccupied() )
			count ++;
		
		if( lineV[ x + 1 ][ y  ]->isOccupied() )
			count ++;
		
		if( count == 3 ){
			
			if ( !lineV[ x ][ y ]->isOccupied() ) {
				//	&p = getRealPosition( lineV[ x ][ y ]->getPosition())
				moveCameraTo(  lineV[ x ][ y ]->getPosition() , TIME_FOCUS_LINE);
				choice.set( x, y );
				vertical = true;
				linesSelected++;
				return true;
			}
			
			if( !lineV[ x + 1 ][ y ]->isOccupied() ){
				moveCameraTo(  lineV[ x +  1][ y ]->getPosition(), TIME_FOCUS_LINE);
				choice.set( x + 1, y );
				vertical = true;
				linesSelected++;
				return true;				
			}
			
			if( !lineH[ x ][ y ]->isOccupied() ){
				moveCameraTo(  lineH[ x ][ y ]->getPosition(), TIME_FOCUS_LINE);
				choice.set( x, y );
				vertical = false;
				linesSelected++;
				return true;				
			}
			
			if( !lineH[ x ][ y + 1 ]->isOccupied() ){
				moveCameraTo(  lineH[ x ][ y + 1 ]->getPosition() , TIME_FOCUS_LINE);	
				choice.set( x, y +1 );
				vertical = false;
				linesSelected++;
				return true;				
			}
		}
		
	}
	return false;
}


/* -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-= 
 
 haveFreeLine->retorna se tem Linhas livres em jogo
 
 -=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-================================================-= */

bool DotGame::haveFreeLine( void ){
	bool ret = false;
	for ( uint8 x = 0; x < nHorizontalZones ; x++ ) {
		for ( uint8 y = 0; y < nVerticalLines  ; y++) {
			if ( !lineH[ x ][ y ]->isOccupied() ){
				ret = true;
				break;
			}
		}
	}
	
	for ( uint8 x = 0; x < nHorizontalLines  ; x++ ) {
		for ( uint8 y = 0; y < nVerticalZones ; y++) {
			if ( !lineV[ x ][ y ]->isOccupied() ){
				ret = true;
				break;
			}			
		}
	}
	return ret;
}
/*==============================================================================
 
 clear->deleta tudo
 
 ==============================================================================*/
void DotGame::clear( void ){

	DELETE( pSliceTransition );
	DELETE( pCurtainTransition);
	pControlsCamera = NULL;
	DELETE( pCurtainLeftTransition );
	pInfoBar = NULL;
	pChronometer = NULL ;
	pCurrTransition = NULL;
	buttonsManager = NULL;
	uint8 x,y;
	
	for( x = 0; x < nPlayers ;x++ )
		DELETE( players[ x ] );

	currPlayer = NULL;

	for ( x = 0; x < nHorizontalZones; x++) 
		for ( y = 0; y< nVerticalZones ; y++ )
			zones[ x ][ y ] = NULL;
	
	for ( x = 0; x < nHorizontalZones ; x++ ) 
		for ( y = 0; y < nVerticalLines  ; y++) 
			lineH[ x ][ y ] = NULL;
	
	for ( x = 0; x < nHorizontalLines  ; x++ ) 
		for (  y = 0; y < nVerticalZones ; y++) 
			lineV[ x ][ y ] = NULL;
		
	
}

/*=================================================================================
 
 MakeBigZone-> verifica se há 4 zonas em série
 
 =================================================================================*/	

bool DotGame::makeBigZone( void ){
	
	bool ret = false;
	Point3f p;
		if( verifyZoneSLeft() ){
		
		lineH[ cZone.x ][ cZone.y ]->setVisible( false );
		lineH[ cZone.x - 1 ][ cZone.y ]->setVisible( false );
		lineV[ cZone.x ][ cZone.y ]->setVisible( false );
		lineV[ cZone.x ][ cZone.y - 1 ]->setVisible( false );
		
		p.set( cZone.x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
			  ( cZone.y * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ),
			  1.0);
		static_cast<Marks*> ( bg->getObject( MARKS_INDICE_INSIDE_GROUP ))->disappearMark(&p);
		//colocar aqui o tratamento em que isso ocorre com as zonas!!
		if( currPlayer->getId() == 0 ){
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);			
		}else {
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);		
		}
	}
	
	if( verifyZoneILeft() ){
		
		lineH[ cZone.x ][ cZone.y + 1 ]->setVisible( false );
		lineH[ cZone.x - 1 ][ cZone.y + 1  ]->setVisible( false );
		lineV[ cZone.x ][ cZone.y  ]->setVisible( false );
		lineV[ cZone.x ][ cZone.y + 1 ]->setVisible( false );
		
		
		p.set( cZone.x * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
			 ( ( cZone.y + 1) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ),
			  1.0);
		static_cast<Marks*> ( bg->getObject( MARKS_INDICE_INSIDE_GROUP ))->disappearMark(&p);
		//colocar aqui o tratamento em que isso ocorre com as zonas!!	
		if( currPlayer->getId() == 0 ){
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);		
		}else {
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x - 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
		}
		
	}
	
	if( verifyZoneIRight() ){
		
		lineH[ cZone.x ][ cZone.y + 1 ]->setVisible( false );
		lineH[ cZone.x + 1 ][ cZone.y + 1  ]->setVisible( false );
		lineV[ cZone.x + 1 ][ cZone.y  ]->setVisible( false );
		lineV[ cZone.x + 1 ][ cZone.y + 1 ]->setVisible( false );
		
		p.set( ( cZone.x + 1 ) * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
			 ( ( cZone.y + 1 ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ),
			  1.0);
		static_cast<Marks*> ( bg->getObject( MARKS_INDICE_INSIDE_GROUP ))->disappearMark(&p);
		//colocar aqui o tratamento em que isso ocorre com as zonas!!
		if( currPlayer->getId() == 0 ){
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
		}else{
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y + 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
		}
		
	}	
	
	if( verifyZoneSRight() ){
		
		lineH[ cZone.x ][ cZone.y ]->setVisible( false );
		lineH[ cZone.x + 1 ][ cZone.y ]->setVisible( false );
		lineV[ cZone.x + 1 ][ cZone.y ]->setVisible( false );
		lineV[ cZone.x + 1 ][ cZone.y - 1 ]->setVisible( false );
		
		p.set( ( cZone.x + 1 )  * ( ZONE_SIDE + LINE_HORIZONTAL_HEIGHT ),
			  ( cZone.y  ) * ( ( LINE_HORIZONTAL_HEIGHT  ) + ZONE_SIDE ) ,
			  1.0);
		static_cast<Marks*> ( bg->getObject( MARKS_INDICE_INSIDE_GROUP ))->disappearMark(&p);
		//colocar aqui o tratamento em que isso ocorre com as zonas!!		
		if( currPlayer->getId() == 0 ){
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y]->getObject(0) )->setVertexSetColor(COLOR_YELLOW);
		}else {
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x + 1 ][ cZone.y  ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x  ][ cZone.y - 1 ]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
			static_cast< RenderableImage * > (zones[ cZone.x ][ cZone.y]->getObject(0) )->setVertexSetColor(COLOR_GREEN);
		}
		
	}
	return ret;
}


/*=================================================================================
 
 conjunto de funções para verificar as zonas laterais para fazer as BigZones, para colocar mais opções, basta retirar a trava de limites de zonas por quadrados, assim fazendo com que a qte máxima de "bigzonas" que um quadrado possa pertencer seja maior.
 
 ================================================================================= */

bool DotGame::verifyZoneSLeft( void ){
	
	
	if( ( cZone.x - 1  > 0 ) && ( cZone.y - 1 > 0 ) ){
		
		if( zones[ cZone.x - 1 ][ cZone.y - 1 ]->isOccupied() &&
		   zones[ cZone.x - 1 ][ cZone.y ]->isOccupied() &&
		   zones[ cZone.x ][ cZone.y - 1 ]->isOccupied() &&		
		   zones[ cZone.x ][ cZone.y ]->isOccupied() ){
			
			if( !zones[ cZone.x - 1 ][ cZone.y - 1 ]->isCoupled() &&
			   !zones[ cZone.x - 1 ][ cZone.y ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y - 1 ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y ]->isCoupled() ){
				
				if( zones[ cZone.x - 1 ][ cZone.y - 1 ]->getPlayerId() ==
				   zones[ cZone.x - 1 ][ cZone.y ]->getPlayerId() ==
				   zones[ cZone.x ][ cZone.y - 1 ]->getPlayerId() ==
				   zones[ cZone.x ][ cZone.y ]->getPlayerId() ){
					
					zones[ cZone.x - 1 ][ cZone.y - 1 ]->setCoupled( true );
					zones[ cZone.x - 1 ][ cZone.y ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y - 1 ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y ]->setCoupled( true );
					return true ;
				}
			}
		}
	}
	return false;
	
}

bool DotGame::verifyZoneILeft( void ){
	
	if( ( ( cZone.x - 1 )  > 0 ) && ( ( cZone.y + 1 ) <= nVerticalZones-1  ) ){
		if( zones[ cZone.x - 1 ][ cZone.y + 1 ]->isOccupied() &&
		   zones[ cZone.x - 1 ][ cZone.y ]->isOccupied() &&
		   zones[ cZone.x ][ cZone.y + 1 ]->isOccupied() &&		
		   zones[ cZone.x ][ cZone.y ]->isOccupied() ){
			
			if( !zones[ cZone.x - 1 ][ cZone.y + 1 ]->isCoupled() &&
			   !zones[ cZone.x - 1 ][ cZone.y ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y + 1 ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y ]->isCoupled() ){
				if(
				   zones[ cZone.x - 1 ][ cZone.y + 1 ]->getPlayerId() ==
				   zones[ cZone.x - 1 ][ cZone.y ]->getPlayerId() ==
				   zones[ cZone.x ][ cZone.y + 1 ]->getPlayerId() ==	
				   zones[ cZone.x ][ cZone.y ]->getPlayerId() ){
					
					zones[ cZone.x - 1 ][ cZone.y + 1 ]->setCoupled( true );
					zones[ cZone.x - 1 ][ cZone.y ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y + 1 ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y ]->setCoupled( true ); 
					return true ;
				}
			}
		}
	}
	return false;	
	
}

bool DotGame::verifyZoneSRight( void ){
	
	if( ( ( cZone.x + 1) <= nHorizontalZones -1 ) &&
	   ( ( cZone.y - 1 ) <= nVerticalZones - 1) &&
	   cZone.x > 0 && 
	   cZone.y > 0){
		if( zones[ cZone.x + 1 ][ cZone.y - 1 ]->isOccupied() &&
		   zones[ cZone.x + 1 ][ cZone.y ]->isOccupied() &&
		   zones[ cZone.x ][ cZone.y - 1 ]->isOccupied() &&		
		   zones[ cZone.x ][ cZone.y ]->isOccupied() )	{
			
			if( !zones[ cZone.x + 1 ][ cZone.y - 1 ]->isCoupled() &&
			   !zones[ cZone.x + 1 ][ cZone.y ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y - 1 ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y ]->isCoupled() ){
				
				if( zones[ cZone.x + 1 ][ cZone.y - 1 ]->getPlayerId() ==
				   zones[ cZone.x + 1 ][ cZone.y ]->getPlayerId() ==
				   zones[ cZone.x ][ cZone.y - 1 ]->getPlayerId() ==	
				   zones[ cZone.x ][ cZone.y ]->getPlayerId() ){
					
					zones[ cZone.x + 1 ][ cZone.y - 1 ]->setCoupled( true );
					zones[ cZone.x + 1 ][ cZone.y ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y - 1 ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y ]->setCoupled( true );
					
					return true ;
				}
			}
		}
	}
	return false;
}

bool DotGame::verifyZoneIRight( void ){
	if( ( cZone.x + 1 <= nHorizontalZones- 1 )  &&  ( cZone.y + 1 <= nVerticalZones - 1) ){
		
		
		if( zones[ cZone.x + 1 ][ cZone.y + 1 ]->isOccupied() &&
		   zones[ cZone.x + 1 ][ cZone.y ]->isOccupied() &&
		   zones[ cZone.x ][ cZone.y + 1 ]->isOccupied() &&		
		   zones[ cZone.x ][ cZone.y ]->isOccupied() )	{
			
			if(	!zones[ cZone.x + 1 ][ cZone.y + 1 ]->isCoupled() &&
			   !zones[ cZone.x + 1 ][ cZone.y ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y + 1 ]->isCoupled() &&
			   !zones[ cZone.x ][ cZone.y ]->isCoupled()){
				
				if(
				   zones[ cZone.x + 1 ][ cZone.y + 1 ]->getPlayerId() ==
				   zones[ cZone.x + 1 ][ cZone.y ]->getPlayerId() ==
				   zones[ cZone.x ][ cZone.y + 1 ]->getPlayerId() ==	
				   zones[ cZone.x ][ cZone.y ]->getPlayerId() ){
					
					zones[ cZone.x + 1 ][ cZone.y + 1 ]->setCoupled( true );
					zones[ cZone.x + 1 ][ cZone.y ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y + 1 ]->setCoupled( true );
					zones[ cZone.x ][ cZone.y ]->setCoupled( true ); 
					return true ;
				}
			}	
		}
	}
	return false;
}
/* ===============================================================================================

 adjustMinZoom ==> função que retorna o zoom máximo possível de acordo com o tamanho do cenário
 
 ===============================================================================================*/
float DotGame::adjustMinZoom( void/*float f*/ ){

	//float max = nHorizontalLines  *  ZONE_SIDE + ( ( nHorizontalLines + 1) * LINE_HORIZONTAL_HEIGHT );
//	max += lineManager->getPosition()->x;
	
	Point3f max, min;
	
	min= *getObject( 0 )->getPosition() ;
	max.set( nHorizontalZones  *  ZONE_SIDE + ( ( nHorizontalZones + 1) * LINE_HORIZONTAL_HEIGHT ) ,
			nVerticalLines * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE) );
	
	if( max.x <= SCREEN_WIDTH && max.y <= SCREEN_HEIGHT )
		return 1.0f;
	
	
	float widthNeeded = ( max.x - min.x );
	float heightNeeded = ( max.y - min.y );
	
	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	float currWidth = ( SCREEN_WIDTH * invZoom );
	float currHeight = ( SCREEN_HEIGHT * invZoom );
	
	float zoomNeededInX = widthNeeded / currWidth;
	float zoomNeededInY = heightNeeded / currHeight;	
	float finalZoom =  (invZoom * ( zoomNeededInX > zoomNeededInY ? zoomNeededInX : zoomNeededInY ));
	
	return 1/finalZoom; 
	
	/*	Point3f max, min;
	 
	 min = *getObject( 0 )->getPosition();
	 
	 Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT);
	 
	 min += *lineManager->getPosition() + (sc * 1.25) ;
	 
	 max.set( nHorizontalLines  *  ZONE_SIDE + ( ( nHorizontalLines + 1) * LINE_HORIZONTAL_HEIGHT ) , 
	 LINE_VERTICAL_WIDTH + ( nVerticalLines * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT ))
	 );
	 
	 max += *lineManager->getPosition();
	 
	 // Calcula o zoom
	 float widthNeeded = ( max.x - min.x );
	 float heightNeeded = ( max.y - min.y );
	 
	 float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	 float currWidth = ( SCREEN_WIDTH * invZoom );
	 float currHeight = ( SCREEN_HEIGHT * invZoom );
	 
	 float zoomNeededInX = widthNeeded / currWidth;
	 float zoomNeededInY = heightNeeded / currHeight;	
	 float finalZoom =  (invZoom * ( zoomNeededInX > zoomNeededInY ? zoomNeededInX : zoomNeededInY ));*/


}

/* ===============================================================================================

 resetCam->reseta o zoom da camera qdo sai do modo de observação

===============================================================================================*/
 void DotGame::resetCam( void ){

	 moveCameraTo(pCurrCamera->getPosition(), 1.0f, TIME_FOCUS_LINE);

}
/*=====================================================================================
 
 auRevoir->sai da tela de jogo=> adie...
 
 =====================================================================================*/

void DotGame::auRevoir(){
	if( !persist )
		[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_MAIN_MENU];
	else
		[(( FreeKickAppDelegate* )APP_DELEGATE ) performTransitionToView:VIEW_INDEX_LOAD_GAME];
}
/*=====================================================================================
 
nextLevel->começa outra partida em um nível de dificuldade maior
 
=====================================================================================*/
void DotGame::nextLevel( void ){

}
/*======================================================================================

 resetTable->reseta o tabuleiro na mesma partida

======================================================================================*/
void DotGame::resetTable( void ){
//	lineManager->resetLines();
//	zoneManager->reset();
		uint8 x,y;
	for ( x = 0; x < nHorizontalZones; x++) {
		for ( y = 0; y< nVerticalZones ; y++ ){
			zones[ x ][ y ]->reset();
		}
	}
	
	for ( x = 0; x < nHorizontalZones ; x++ ) {
		for ( y = 0; y < nVerticalLines  ; y++) {
			lineH[ x ][ y ]->reset();
		}
		
	}
	
	
	for ( x = 0; x < nHorizontalLines  ; x++ ) {
		for ( y = 0; y < nVerticalZones ; y++) {
			lineV[ x ][ y ]->reset();
		}
	}
	
	static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_YES ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	static_cast< Button* > ( buttonsManager->getObject( INDEX_BUTTON_NO ) )->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );

	
	for( uint8 x = 0; x < nPlayers; x++ )
		players[ x ]->setScore( 0 );
	pInfoBar->updateScores( this );

	Point3f sc( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	pCurrCamera->setPosition( &sc );
/*	moveCameraTo( &sc, 0.1);*/
	indicePlayer = 255;
	changePlayer();
}

/*====================================================================================================

 indicePlayerWinner-> aponta qual jogador ganhou
 
====================================================================================================*/
uint8 DotGame::indicePlayerWinner( void ){
//feito para 2 jogadores, sendo que para mais jogadores, teria-se que colocar uma ordenação aqui...
	if ( players[ 0 ]->getScore() > players[ 1 ]->getScore() ) {
		return 0;
	}else {
		return 1;
	}	
}

bool DotGame::ZoneHorizontalSuperiorDesoccupied( uint8 x, uint8 y ){

}

bool DotGame::ZoneHorizontalInferiorDesoccupied( uint8 x, uint8 y ){
	//OK!!
	if( x < nHorizontalZones && ( y < ( nVerticalZones )) ) {
		
		if( lineH[ x ][ y ]->isOccupied() && 
		   lineH[ x ][ y + 1 ]->isOccupied() &&
		   lineV[ x ][ y ]->isOccupied() &&
		   lineV[ x + 1 ][ y ]->isOccupied())
		{
			zones[ x ][ y ]->lostByPlayer();
			//NSLog(@" zona ocupada é a zona na posição x=%d, y=%d, pelo jogador %d ",x,y, zones[ x ][ y ]->getPlayerId());

			return true;
		}
		
	}
	return false;
}

bool DotGame::ZoneVerticalLeftDesoccupied( uint8 x, uint8 y ){

}

bool DotGame::ZoneVerticalRightDesoccupied( uint8 x, uint8 y ){

}

/*========================================================
 
 inputMoves=> controla a qte de movimentos permitidos pelo jogador
 
 -=-=-=-=-=0=-0=-0=-=9=--0=-0=-=-=-=-==--===-=-=-=-=-=-==.-.=-.=.-=-*/
void DotGame::inputMoves( const Point3f *p ){
#if DEBUG
	NSLog(@"p=> x=%3.2f, y=%3.2f", p->x , p->y );
#endif
//	linesSelected++;
	linesSelected = ( ++linesSelected >= dispMoves ? dispMoves: linesSelected );
	movesMake.push_back( *p/*ret*/ );
	if( movesMake.size() > dispMoves )
		movesMake.erase( movesMake.begin() );

}

