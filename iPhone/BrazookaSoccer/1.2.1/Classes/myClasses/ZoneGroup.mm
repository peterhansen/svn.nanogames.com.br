/*
 *  ZoneGroup.mm
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "ZoneGroup.h"
#include "ZoneGame.h"
#include "DotGame.h"
#include "GameBaseInfo.h"
#include "Utils.h"
using namespace Utils;
ZoneGroup::ZoneGroup( GameBaseInfo& g ):ObjectGroup( g.nHZones * ( g.nVZones + 1 ) ), nZonesH( 0 ), nZonesV( 0 ) {}

/*=======================================================================================================
 
 insere zones=> insere as zonas em um grupo
 
======================================================================================================= */
void ZoneGroup::insertZones( ZoneGame* z[ MAX_ZONES_COLUMNS ][ MAX_ZONES_ROWS ] ){
	
	for (uint8 i=0; i< nZonesH ; i++) {
		for (uint8 j=0; j< nZonesV ; j++) {
			insertObject( z[ i ][ j ] );
		}
	}
}

/*=======================================================================================================

 setQteZones( uint8 h, uint8 v )-> seta a qte de zonas possíveis no jogo

 ======================================================================================================= */
void ZoneGroup::setQteZones( uint8 h, uint8 v ){

	nZonesH = h;
	nZonesV = v;
	
}

/*=======================================================================================================
 
 reset=> nome auto explicativo, reseta todas as zonas em jogo para o esta inicial
 
 =======================================================================================================*/
void ZoneGroup::reset( void ){
	for (uint8 c = 0; c < getNObjects(); c++) {
		static_cast< ZoneGame* > ( getObject( c ) )->reset();
	}
}


/*=======================================================================================================
 
haveFreeZone = verifica se há zonas livres em campo
 
=======================================================================================================*/
bool ZoneGroup::haveFreeZone( void ){
	for (uint16 c = 0; c < getNObjects(); c++) {
		if ( static_cast< ZoneGame* > ( getObject( c ) )->getZoneGameState() != ZONE_GAME_STATE_OCCUPIED ) {
			return true;
		}
	}
		return false;
}



bool ZoneGroup::isZoneOccupied( Point3f *p ){

	for (uint16 c = 0; c < getNObjects(); c++) 
		if( IsPointInsideRect( p, getObject( c ) ) )
			return static_cast< ZoneGame* > ( getObject( c ) )->isOccupied();
	
	return false;
}


 bool ZoneGroup::zoneGainedByPlayer( Point3f *p, Player *pl ){
	 for (uint16 c = 0; c < getNObjects(); c++) {
		 if( *p == *getObject( c )->getPosition() ){
			 static_cast<ZoneGame*>( getObject( c) )->gainedByPlayer( pl );
			 return true;
		 }
		 return false;
 }
#if DEBUG
	 NSLog(@"nenhuma zona selecionada");
#endif
}

void ZoneGroup::zoneLostedByPlayer( Point3f *p/*, Player*p*/ ){
	for (uint16 c = 0; c < getNObjects(); c++) {
			 static_cast<ZoneGame*>( getObject( c) )->lostByPlayer();
		}
}