/*
 *  Player.mm
 *  dotGame
 *
 *  Created by Max on 10/7/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "Player.h"
#include "GameBaseInfo.h"
#include "PowerUp.h"
#include "CharacterFactory.h"

/*
 construtor
 */
Player::Player():Ident(0),
nome( "Player" ),
score( 0 ),
boneco( NULL ),
pPower( NULL ),
state( PLAYER_MATCH_STATE_NONE ),
typeUser( PLAYER_USER_USER ){}


/*=============================================================
 
carrega as informacões de acordo com gamebaseinfo

 =============================================================*/
 void Player::LoadInfo( GameBaseInfo g, uint8 index ){

	setNome( g.nomes[ index ] );
	loadDoll( g.iddolls[ index ] );
	setId( index );
	//setTypeUser( g.pUserType[ index ] );
}


/*=======================================================================================
 
 carrega a personagem do jogador, deve ser chamada na hora de carregar a tela

 ========================================================================================*/
void Player::loadDoll( IdDoll _id ){
	boneco = CharacterFactory::GetInstance()->LoadCharacterIndice( _id );
}

//destrutor
Player::~Player(){
	SAFE_DELETE( boneco );
	pPower = NULL;
}
/*==========================================================================================
 
updatePowerUp-> atualiza o powerUp, se necessário
 
 ==========================================================================================*/
void Player::updatePowerUp( void ){
if( pPower!= NULL )
	pPower->updatePowerUp();
}
/*==========================================================================================
 
 releasePowerUp-> solta o powerUp, 
 
 ==========================================================================================*/
void Player::releasePowerUp( void ){
	pPower->onRelease();
	pPower = NULL;
}