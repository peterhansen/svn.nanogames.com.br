/*
 *  DGConfigView.mm
 *  dotGame
 *
 *  Created by Max on 12/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "DGConfigView.h"
#include "FreeKickAppDelegate.h"
#include "Macros.h"
#include"Config.h"
#include"ObjcMacros.h"

#include<string>
using namespace std;



@implementation DGConfigView

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
	//[hTxtHelp setFont: [(( FreeKickAppDelegate* )APP_DELEGATE ) getiPhoneFont]];
	
}
/*=========================================================
 
 Indica que o usuário está pressionando uma opção do menu

 =========================================================*/
- ( IBAction ) onBtPressed:( id )hButton{
	
	if( hButton == hOk){
		
		[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_LOAD_GAME ];
	}else if (hButton == hBack) {
		[ [ ApplicationManager GetInstance ] performTransitionToView: VIEW_INDEX_SELECT_DOLL ];
	}else if ( hButton == hFakeImg ) {
		if( ++indice > 5 )
			indice = 0;
		GBinfo->idBg = lista[ indice ];
#if DEBUG
		switch ( GBinfo->idBg ) {
			case  ID_BGD_ANGEL:
				NSLog(@"anjo bgd");
				break;
			case  ID_BGD_FAIRY:
				NSLog(@"fada bgd");
				break;
			case  ID_BGD_DEVIL:
				NSLog(@"devil bgd");
				break;
			case  ID_BGD_FARMER:
				NSLog(@"farmer bgd");
				break;
			case  ID_BGD_EXECUTIVE:
				
				NSLog(@"executivo bgd");
				break;
			case  ID_BGD_OGRE:
				
				NSLog(@"ogro bgd");
				break;
				
		}
		
#endif
	}
}
#define MAX_SIZE_BUFFER_NUM_LEN 4

/*===================================================================
 
 Método chamado antes de iniciarmos uma transição para esta view

 =======================================================================*/
- ( void )onBeforeTransition: ( GameBaseInfo* )GBInfo{
	GBinfo = GBInfo;
	
	[ hHsize setValue:( float ) GBinfo->nHZones  ];
	[ hVsize setValue:( float ) GBinfo->nVZones ];
	[ hLevel setValue:( float ) GBinfo->level];
	
	char buffer[ MAX_SIZE_BUFFER_NUM_LEN ];
	
	sprintf( buffer,"%2.0f",[ hHsize value ] );
	[ hHsizeInfo setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];
	
	sprintf( buffer,"%2.0f",[ hVsize value ] );		
	[hVsizeInfo setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];
	
	sprintf( buffer,"%2.0f",[ hLevel value ] );
	[hLevelInfo setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];	

	indice = GBinfo->idBg;
	lista[ 0 ]=  ID_BGD_ANGEL;
	lista[ 1 ]=  ID_BGD_DEVIL;
	lista[ 2 ]=  ID_BGD_FARMER;
	lista[ 3 ]=  ID_BGD_EXECUTIVE;
	lista[ 4 ]=  ID_BGD_FAIRY;
	lista[ 5 ]=  ID_BGD_OGRE;
} 



/*=======================================================================

método para dizer que o level, tamanho  horizontal e vertical foram mudados 
 
=======================================================================*/
- ( IBAction )onSldPressed:( id )hSlider{
	
	char buffer[ MAX_SIZE_BUFFER_NUM_LEN ];
	if( hSlider == hHsize ){
		
		sprintf( buffer,"%2.0f",[ hHsize value ] );
		[ hHsizeInfo setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];
		GBinfo->nHZones = ( uint8 )round([ hHsize value ]);
		
	}else if( hSlider == hVsize ) {
				
		sprintf( buffer,"%2.0f",[ hVsize value ] );		
		[hVsizeInfo setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];
		GBinfo->nVZones = ( uint8 )round([ hVsize value ]);
		
	}else if( hSlider == hLevel ) {
				
		sprintf( buffer,"%2.0f",[ hLevel value ] );
		[hLevelInfo setText:CHAR_ARRAY_TO_NSSTRING( buffer ) ];		
		GBinfo->level = ( uint8 )round([ hLevel value ]);
	}
	
	
}

#undef MAX_SIZE_BUFFER_NUM_LEN

@end

