/*
 *  LineGame.mm
 *  dotGame
 *
 *  Created by Max on 10/8/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "LineGame.h"
#include "Player.h"
#include "Utils.h"
#include "RenderableImage.h"
#include "ResourceManager.h"
#include "DotGameInfo.h"
#include "ZoneGame.h"
#include "ObjcMacros.h"
#include "Quad.h"
//#include "Anjinho.h"
//#include "Background.h"


#define INDEX_LINE_PLAYERS 1

#define INDEX_LINE_BACKGROUND 0

#define INDEX_LINE_ANGEL 0
#define INDEX_LINE_DEVIL 1
#define INDEX_LINE_EXECUTIVE 2
#define INDEX_LINE_FARMER 3
#define INDEX_LINE_FAIRY 4
#define INDEX_LINE_OGRE 5


#define OBJECTS_INSIDE_LINE_GAME 2 


Sprite* LineGame::linhaBgH; 
Sprite* LineGame::linhaBgV;
Sprite* LineGame::linhaPlV;
Sprite* LineGame::linhaPlH;


LineGame::LineGame( LineGameOrientation l/*, idBackground Bg*/ ):ObjectGroup( OBJECTS_INSIDE_LINE_GAME ),
orientation( l ),
state( LINE_GAME_STATE_BLANK ),
occupied( false ),
selected( false ),
observed( false ),
lColor( COLOR_GREY ),
idPlayer( -1 )
{
	if( l == LINE_GAME_ORIENTATION_VERTICAL ){
		size.set( LINE_VERTICAL_WIDTH ,LINE_VERTICAL_HEIGHT ,1.0 );
	}else if( l == LINE_GAME_ORIENTATION_HORIZONTAL ) {
		size.set( LINE_HORIZONTAL_WIDTH , LINE_HORIZONTAL_HEIGHT, 1.0 );
	}
	
	
	
	setPlayerImage( /*Bg*/ );
#if DEBUG
	setName("Linha \n");
#endif
}

void LineGame::loadImages( void ){
		linhaBgH = new Sprite( "lh","lh" );
		linhaPlH = new Sprite( "ch","ch" );	
		linhaBgV = new Sprite( "lv","lv" );
		linhaPlV = new Sprite("cv","cv");
#if DEBUG 
	linhaBgH->setName("linha hor fundo estatica\n");
	linhaPlH->setName("linha hor jogador estatica\n");
	linhaBgV->setName("linha ver fundo estatica\n");
	linhaPlV->setName("linha ver jogador estatica\n");
	NSLog(@"imagens estáticas carregados\n");	
#endif
}


void LineGame::removeImages( void ){

	SAFE_DELETE( linhaBgH );
	SAFE_DELETE( linhaBgV );
	SAFE_DELETE( linhaPlV  );
	SAFE_DELETE( linhaPlH );

#if DEBUG
	NSLog(@"imagen estáticas descarregados \n");	
#endif
}

/*================================================================================
 
 puxa para si mesmo as imagens que estiverem armazenadas no array de jogadores  e no final aloca a imagem em branco...
 
 ================================================================================*/

void LineGame::setPlayerImage( /*idBackground Bg*/ ){
	
	Sprite* currSprite;

	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		currSprite = new Sprite( LineGame::linhaBgV );
		currSprite->setSize( LINE_VERTICAL_WIDTH ,LINE_VERTICAL_HEIGHT, 1.0f );
	}else 
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ) {
			currSprite = new Sprite( LineGame::linhaBgH );
			currSprite->setSize( LINE_HORIZONTAL_HEIGHT , LINE_HORIZONTAL_WIDTH, 1.0f );
		}
	
	/*switch ( Bg ) {
		case DOLL_ID_ANGEL:
			
			currSprite->setAnimSequence( INDEX_LINE_ANGEL );
			currSprite->setVertexSetColor( COLOR_BLUE );
			break;
		case DOLL_ID_OGRE:
			
			currSprite->setAnimSequence( INDEX_LINE_OGRE );
			currSprite->setVertexSetColor( COLOR_GREEN );			
			break;
		case DOLL_ID_DEVIL:
			
			currSprite->setAnimSequence( INDEX_LINE_DEVIL );
			currSprite->setVertexSetColor( COLOR_RED );			
			break;
		case DOLL_ID_FARMER:
			
			currSprite->setAnimSequence( INDEX_LINE_FARMER );
			currSprite->setVertexSetColor( COLOR_BLACK );			
			break;
		case DOLL_ID_EXECUTIVE:
			
			currSprite->setAnimSequence( INDEX_LINE_EXECUTIVE );
			currSprite->setVertexSetColor( COLOR_GREY );			
			break;
		case DOLL_ID_FAIRY:
			
			currSprite->setAnimSequence( INDEX_LINE_FAIRY );
			currSprite->setVertexSetColor( COLOR_PURPLE );
			break;
	}*/
	
	
	
#if DEBUG
	currSprite->setName("linha de fundo\n");
#endif
	if( !insertObject( currSprite ) )NSLog(@" sprite fundo não inserido ");
}


/*===================================================================
 
 determina o estado da linha e faz possíveis correções
 
 ====================================================================*/

void LineGame::setLineGameState( LineGameState l ){
	lastState = state;
	state = l;
	switch ( l ) {
		case LINE_GAME_STATE_NONE:
		case LINE_GAME_STATE_SELECTED:
			break;
		case LINE_GAME_STATE_OCCUPIED:
			occupied = true;
			break;
		case LINE_GAME_STATE_BLANK:
			selected = false;
			occupied = false;
			break;
		case LINE_GAME_STATE_OBSERVING:
			observed = true;
			break;
			
	}
	updateImage();
}

/*============================================================================
 
 checkCollision-> retorna true, se houve alguma colisão dentro dela

==============================================================================*/

bool LineGame::checkCollision( const Point3f* pPoint,Player *p ) {
	
	if( Utils::IsPointInsideRect( pPoint, this ) &&
	   getLineGameState() != LINE_GAME_STATE_NONE &&
	   isVisible() ){
#if DEBUG
		NSLog(@"colidiu na linha x = %.2f, y = %.2f",position.x,position.y);
#endif
		/*if ( state == LINE_GAME_STATE_SELECTED && selected ) {
			gainedByPlayer( p );	
			setLineGameState( LINE_GAME_STATE_OCCUPIED );

			return true;
		}*/
		
		if ( state == LINE_GAME_STATE_BLANK) {
			setLineGameState( LINE_GAME_STATE_SELECTED );//LINE_GAME_STATE_OCCUPIED );
			//gainedByPlayer( p );
			return true;
		}

	}
	return false;
}


/*========================================================================
 
 updateImage-> troca a figura da linha de acordo com o estado dela
 
 ==========================================================================*/

void LineGame::updateImage( void ){

	switch ( state ) {
		case LINE_GAME_STATE_BLANK:
			static_cast< Sprite* > (getObject( INDEX_LINE_BACKGROUND ))->setVertexSetColor( COLOR_WHITE );
			//getObject( INDEX_LINE_PLAYERS )->setVisible( false );
			break;

		case LINE_GAME_STATE_SELECTED:
			static_cast< Sprite* > ( getObject( INDEX_LINE_BACKGROUND ) )->setVertexSetColor( COLOR_GREY );
			selected = true;
			break;

		case LINE_GAME_STATE_OCCUPIED:
			static_cast< Sprite* > (getObject( INDEX_LINE_BACKGROUND ))->setVertexSetColor( COLOR_WHITE );
			getObject( INDEX_LINE_PLAYERS )->setVisible( true );
//			static_cast< Sprite* > ( getObject( INDEX_LINE_PLAYERS ) )->setVisible( true );
			break;
		case LINE_GAME_STATE_OBSERVING:
			break;

	}
	

}

/*======================================================================================
 
 posiciona a linha de acordo com a sua orientação
 
=========================================================================================*/
void LineGame::setPosition( uint x, uint y ){
	Point3f p,m;
	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		
		p.set( x * ( LINE_VERTICAL_WIDTH + ZONE_SIDE ) ,
			  LINE_VERTICAL_WIDTH + ( y * ( LINE_VERTICAL_WIDTH + LINE_VERTICAL_HEIGHT )),
			  1.0);

		m.set( 0.0f,
			  0.0f);
	}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
		
		p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + LINE_HORIZONTAL_WIDTH ) ), 
			  y * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE),
			  1.0);
		m.set( ( FENCE_HORIZONTAL_WIDTH * 0.1f )	,
			  0.0);
	}
	
	position.set( p );	
	
	
	getObject( INDEX_LINE_BACKGROUND )->setPosition( &m );
}


/*=============================================================================================
 
 restart-> para ser chamado depois de qq turno
 
 =====================================================================================================*/

void  LineGame::restart( void ){
	if( !occupied ){
		setLineGameState( LINE_GAME_STATE_BLANK );
		selected = false;
	}

}

/*=============================================================
 
 reset->deixa a linha no seu estado original
 
 =================================================================*/

void LineGame::reset( void ){
	setLineGameState( LINE_GAME_STATE_BLANK );
}

/*===========================================================================

 puxa a imagem da linha de acordo com o fundo
 
===========================================================================*/
void LineGame::getBackgroundImage( Background *bg ){

	
	/*if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		currSprite = bg->getBaseLineV();
	}else 
		if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL ) {
			currSprite = bg->getBaseLineH();
		}*/
}

/*=================================================================================
 
 acerta a imagem da linha de acordo com o personagem do jogador
 
================================================================================*/
void LineGame::gainedByPlayer( Player *p ){
	//por enquanto, somente mudar as cores...!
	//	correto!	
	idPlayer = p->getId();
/*	Sprite *s2;
//	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
//		s2 = new Sprite( p->boneco->getLineVPic());
//	}else {
//		s2 = new Sprite( p->boneco->getLineHPic() );
//	}
//	Point3f f;
//	if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
//		
//		f.set( 0.0,
//			  -( FENCE_VERTICAL_HEIGHT * 0.1 ),				  
//			  1.0f);
//	}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
//		
//		f.set( 0.0,
//			  - (FENCE_HORIZONTAL_HEIGHT * 0.75f),
//			  0.0f);
//	}
//	s2->setPosition( &f );
//  insertObject( s2 );
 lColor= *( p->boneco->getColor() );	
*/

//temporário	
	if( p->getId() == 0 )
		lColor.set( COLOR_BLUE );
	else 
		lColor.set( COLOR_GREEN );

	

		
	
	Sprite* s;
	if( orientation == LINE_GAME_ORIENTATION_HORIZONTAL )
		s = new Sprite( LineGame::linhaPlH );
	else
		s = new Sprite( LineGame::linhaPlV );
#if DEBUG
	s->setName("linha jogador");
#endif
	
	Point3f f;
		if( orientation == LINE_GAME_ORIENTATION_VERTICAL ){
		
			f.set( 0.0,
				  -( FENCE_VERTICAL_HEIGHT * 0.1 ),				  
				  1.0f);
		}else {//LINE_GAME_ORIENTATION_HORIZONTAL :
		
			f.set( 0.0,
				  - (FENCE_HORIZONTAL_HEIGHT * 0.75f),
				  0.0f);
		}
	s->setPosition( &f );
	
	insertObject( s );
	switch ( p->boneco->getIdDoll() ) {
			
		case DOLL_ID_ANGEL:
			
			s->setAnimSequence( INDEX_LINE_ANGEL );
			s->setVertexSetColor( COLOR_BLUE );
			return;
			break;
		case DOLL_ID_OGRE:
			
			s->setAnimSequence( INDEX_LINE_OGRE );
			s->setVertexSetColor( COLOR_GREEN );			
			return;
			break;
		case DOLL_ID_DEVIL:
			
			s->setAnimSequence( INDEX_LINE_DEVIL );
			s->setVertexSetColor( COLOR_RED );			
			return;
			break;
		case DOLL_ID_FARMER:
			
			s->setAnimSequence( INDEX_LINE_FARMER );
			s->setVertexSetColor( COLOR_BLACK );			
			return;
			break;
		case DOLL_ID_EXECUTIVE:
			
			s->setAnimSequence( INDEX_LINE_EXECUTIVE );
			s->setVertexSetColor( COLOR_GREY );			
			return;
			break;
		case DOLL_ID_FAIRY:
			
			s->setAnimSequence( INDEX_LINE_FAIRY );
			s->setVertexSetColor( COLOR_PURPLE );
			return;
			break;
		}

}
	
void LineGame::lineGameCpuGained( Player *p ){
	
	gainedByPlayer( p );
setLineGameState( LINE_GAME_STATE_OCCUPIED );
}

//destrutor
LineGame::~LineGame(){
}


bool LineGame::render( void ){
	if( !isVisible() )
		return false;

	if ( /*state == LINE_GAME_STATE_OBSERVING*/ observed ) {
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		glTranslatef( position.x, position.y, position.z );
		glColor4ub( lColor.getUbR(), lColor.getUbG(), lColor.getUbB(), lColor.getUbA());
		
		Quad q1;
		
		q1.quadVertexes[ QUAD_TOP_LEFT ].setPosition( 0.0, 0.0f, 1.0f );
		q1.quadVertexes[ QUAD_TOP_RIGHT ].setPosition( size.x, 0.0, 1.0f );
		q1.quadVertexes[ QUAD_BOTTOM_RIGHT ].setPosition( size.x, size.y, 1.0f);	
		q1.quadVertexes[ QUAD_BOTTOM_LEFT ].setPosition( 0.0f, size.y, 1.0f );	
		
		q1.render();
		glPopMatrix();
		return true;
	}else
		return ObjectGroup::render();
}


/*============-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0=-=-=-==-=-==-==-=-======-=--===-=----=-=-=-=====
 
ajustLinesToBackground->acerta a figura da linha de acordo com o fundo
 
 ============-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-0=-=-=-==-=-==-==-=-======-=--===-=----=-=-=-=====*/
void LineGame::adjustLinesToBackground( int8 index ){

	static_cast<Sprite*> (INDEX_LINE_BACKGROUND)->setAnimSequence( index );
}

/*===============================================================================================
 
 lostByPlayer=> fç chamada qdo o jogador perde a linha, 
 
 ==============================================================================================*/

void LineGame::lostByPlayer( /*Player *p*/ ){
	lColor.set( COLOR_GREY );
	idPlayer = -1 ;
	removeObject( INDEX_LINE_PLAYERS );
	reset();
}

