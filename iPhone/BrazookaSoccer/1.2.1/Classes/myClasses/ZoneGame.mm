/*
 *  ZoneGame.mm
 *  dotGame
 *
 *  Created by Max on 10/9/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include"PowerUp.h"
#include "ZoneGame.h"
#include "Player.h"
#include "LineGame.h"
#include "GenericDoll.h"
#include "DotGame.h"
#include "DotGameInfo.h"

#include "Exceptions.h"
#include "Macros.h"

#include "RenderableImage.h"
//#include "Button.h"
#include "Random.h"

#include "PowerUpFactory.h"


#define MAX_ZONES_SPRITES MAX_PLAYERS + 1

ZoneGame::ZoneGame():ObjectGroup( MAX_ZONES_SPRITES ),playerId( -1 ), occupied( false ), coupled( false ), power( NULL ) {
	setZoneGameState( ZONE_GAME_STATE_NONE );
#if DEBUG
	setName("zonas\n");
#endif
	PowerUpFactory::Create();
		
}

/*============================================================================
 
 adiciona as imagens dos jogadores nas zones
 
 =============================================================================*/
void ZoneGame::setPlayerImage( Player *p ){
	//todoo: colocar para carregar as imagens e os sprites de todos os players aqui!!!
	//por enquanto, vai isso aqui mesmo...
	char name2[]={'z','n','2','\0'};
	Texture2DHandler hTexture2 = ResourceManager::GetTexture2D( name2 );
	RenderableImage *r;
	r = new RenderableImage();
	r->setImage( hTexture2 );
	r->setScale(ZONE_SIDE, ZONE_SIDE, 1.0f );
	r->setSize( 1.0f, 1.0f, 1.0f );
	
#if DEBUG
	r->setName("zona imagem\n ");
#endif
		insertObject( r );
	setZoneGameState( ZONE_GAME_STATE_BLANK );
	
}
/*============================================================================
 

 ============================================================================*/
void ZoneGame::setZoneGameState ( ZoneGameState z ){
	state = z;

	updateImage();
}

/*============================================================================
 
 quando uma zona é ganhada pelo jogador
 
 ============================================================================*/

void ZoneGame::gainedByPlayer( /* DotGame *dg*/ Player *pl ){
//	dg->currPlayer->score++;
	pl->score++;
//	power = PowerUpFactory::GetInstance()->getNewPowerUp( POWER_UP_TYPE_BOMB, dg );
//	dg->currPlayer->setPowerUp( power );
	playerId = pl->getId();
	setPlayerImage( pl );
#if DEBUG
	NSLog(@"zona( %3.2f,%3.2f ) ganhada pelo jogador %d\n", position.x, position.y, playerId );
#endif
	setZoneGameState( ZONE_GAME_STATE_OCCUPIED );
}

/*============================================================================
 
 atualiza a imagem de acordo com o estado
 
 ============================================================================*/

void ZoneGame::updateImage( void ){

	switch ( state ) {
		case ZONE_GAME_STATE_NONE :
			break;
			
		case ZONE_GAME_STATE_BLANK :
			//removeObject( 0 );
			static_cast<RenderableImage*> ( getObject( 0 ) )->setVisible( false );	
			static_cast<RenderableImage*> ( getObject( 0 ) )->setVertexSetColor( COLOR_WHITE );
			break;
			
		case ZONE_GAME_STATE_OCCUPIED :
			occupied = true;
//			NSLog(@" zona ocupada é a zona na posição x=%.f, y=%.f, pelo jogador %d ",position.x,position.y, playerId);
			static_cast<RenderableImage*> ( getObject( 0 ) )->setVisible( true );
			static_cast<RenderableImage*> ( getObject( 0 ) )->setVertexSetColor( COLOR_PURPLE );
			break;
		default:
			break;
	}
}

bool ZoneGame::render( void ){
	if( power != NULL )
	power->render();
	return ObjectGroup::render();
}


/*============================================================================
 
 reseta
 
 ============================================================================*/

void ZoneGame::reset( void ){
	setZoneGameState( ZONE_GAME_STATE_BLANK );
}
/*============================================================================
 
 posiciona na tela
 
 ============================================================================*/

void ZoneGame::setPosition( uint8 x, uint8 y ){
	Point3f p;
	p.set( LINE_HORIZONTAL_HEIGHT + ( x * ( LINE_HORIZONTAL_HEIGHT + ZONE_SIDE ) ),	
		 LINE_VERTICAL_WIDTH + ( y * ( ZONE_SIDE + LINE_VERTICAL_WIDTH ) ),
		  1.0f);
position.set( p );
}
/*============================================================================
 
 retorna a identificação do jogador que ocupou a zona
 
 ============================================================================*/
uint8 ZoneGame::getPlayerId( void ){
		return playerId;
}

ZoneGame::~ZoneGame(){
	SAFE_DELETE( power );

}
/* =================================================================================================

 lostByPlayer=> chamado qdo um jogador perder uma zona...
 
 =================================================================================================*/
void ZoneGame::lostByPlayer( void/*Player *p*/ ){
	removeObject(0);
	playerId = -1;
	occupied = false;
	coupled = false ;
	reset();
}

void ZoneGame::setPowerUp( PowerUp* p ){
	this->power = p;

}