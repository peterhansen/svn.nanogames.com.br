/*
 *  OgreBg.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef OGRE_BG_H
#define OGRE_BG_H 1

#include "Board.h"


class OgreBg : public Board {
	
public:
	OgreBg( GameBaseInfo* g );
	
	~OgreBg(){};
	
	
protected:	
	void generateBackground( void ) ;
	
	void configureMarks( void ) ;
	
	void configureLinesBkgd( void );
	
	
};


#endif