/*
 *  FairyBg.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef FAIRY_BG_H
#define FAIRY_BG_H 1

#include "Board.h"


class FairyBg : public Board {
	
public:
	FairyBg( GameBaseInfo* g );
	
	~FairyBg(){};
	
	
protected:	
	void generateBackground( void ) ;
	
	void configureMarks( void ) ;
	
	void configureLinesBkgd( void );
	
	
};


#endif