/*
 *  PowerUp.h
 *  dotGame
 *
 *  Created by Max on 2/3/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef POWER_UP_H
#define POWER_UP_H 1

#include "RenderableImage.h"
#include "PowerUpFactory.h"

class ZoneGame;
class PowerUpListener;
class Player;

//class LineGroup;
//class ZoneGroup;
//class Board;

//enum PowerUpType{
//	POWER_UP_TYPE_NONE = -1,
//	POWER_UP_TYPE_ONUS_COMBO_BREAK = 0,
//	POWER_UP_TYPE_BONUS_MOVE_CHOOSE,
//	POWER_UP_TYPE_BONUS_MOVE_RANDOM,
//	POWER_UP_TYPE_ONUS_TIME_SHORT,
//	POWER_UP_TYPE_BOMB
//}typedef PowerUpType;

class PowerUp : public RenderableImage  {
	
public:
	
	PowerUp( PowerUpType p , const char* pImagePath  = NULL ,PowerUpListener *p = NULL );
	
	~PowerUp( void );

	bool isDisarmed( void );
	
	void setListener( PowerUpListener *p );
	
	PowerUpListener* getPowerUpListener( void );
	
	PowerUpType getPowerUpType( void );

	void setZone( ZoneGame *z );

	void setPosition( uint8 x, uint8 y );
	
	//atualiza o powerUp caso ele tenha algum  contador
	virtual void updatePowerUp( void ) = 0;
	
	//quando ele é ganho por algum jogador ( obivamente, pode ser que nem todos tenham executem este método )
	virtual void gainedByPlayer( Player* p ) = 0;
	
	virtual void onRelease( void ) = 0;
	
private:
	bool disarmed;
	PowerUpListener *pListener;
	PowerUpType pwrUpType;
	ZoneGame* zoneOf;
	
};


class PowerUpListener{
public:
	virtual ~PowerUpListener( void ){};
	virtual void PowerUpEffect( PowerUp* p ) = 0;
	
};

inline bool PowerUp::isDisarmed( void ){
	return disarmed;
}

inline	PowerUpType PowerUp::getPowerUpType( void ){
	return pwrUpType;
}

inline PowerUpListener* PowerUp::getPowerUpListener( void ){
	return pListener;
}

#endif