/*
 *  PowerUpFactory.mm
 *  dotGame
 *
 *  Created by Max on 2/18/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PowerUpFactory.h"
#include "ObjcMacros.h"
#include "PUComboBreak.h"
#include "PUBomb.h"

PowerUpFactory* PowerUpFactory::pSingleton = NULL;

bool PowerUpFactory::Create( void ){
	if( !pSingleton )
		pSingleton = new PowerUpFactory();
	
	return pSingleton!= NULL;
}

void PowerUpFactory::Destroy( void ){
	SAFE_DELETE( pSingleton );
}

PowerUp* PowerUpFactory::getNewPowerUp( PowerUpType t , PowerUpListener/*DotGame*/ *dg){
	if( !pSingleton )
		Create();

	PowerUp *p;
	
	switch ( t ) {
		case POWER_UP_TYPE_COMBO_BREAK:
			
			break;
		case POWER_UP_TYPE_BOMB:
			p = new PUBomb( dg );
			break;
			
		case POWER_UP_TYPE_MOVE_CHOOSE:
			
			break;
		case POWER_UP_TYPE_MOVE_RANDOM:
			
			break;
		case POWER_UP_TYPE_TIME_SHORT:
			
			break;
	}
	
	return p;
	
}

PowerUpFactory::PowerUpFactory():nPowerTypes( N_POWER_UPS ){
#if DEBUG
	NSLog(@"powerUpFactory criada!");
#endif
}
	
PowerUpFactory::~PowerUpFactory(){
#if DEBUG
	NSLog(@"powerUpFactory destruída");
#endif
	
}
