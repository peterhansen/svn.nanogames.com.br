#include "Chronometer.h"

#include "Exceptions.h"
#include "Label.h"
#include "ObjcMacros.h"
#include "RenderableImage.h"

#include "ChronometerListener.h"
#include "FreeKickAppDelegate.h"

// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
		if( !pObject || !pGroup->insertObject( pObject ) )	\
		{													\
			delete pObject;									\
			goto Error;										\
		}

// Número de objetos contidos no grupo
#define CHRONOMETER_N_OBJS 1//2

// Índices dos objetos no grupo
#define CHRONOMETER_OBJ_INDEX_BKG	0
#define CHRONOMETER_OBJ_INDEX_LABEL	0//1

// Configurações do background
#define CHRONOMETER_BKG_IMG_FRAME_WIDTH		107.0f // OLD 97.0f
#define CHRONOMETER_BKG_IMG_FRAME_HEIGHT	43.0f

// Configurações do label
#define CHRONOMETER_FONT_CHAR_SPACEMENT	1.0f
#define CHRONOMETER_LB_POS_X	15.0f
#define CHRONOMETER_LB_POS_Y	 7.0f
#define CHRONOMETER_LB_WIDTH	83.0f
#define CHRONOMETER_LB_HEIGHT	20.0f

// Configurações das animações
#define CHRONOMETER_ANIM_MOVE_DUR 0.100f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

Chronometer::Chronometer( InterfaceControlListener* pControlListener, ChronometerListener* pChronometerListener )
			: ObjectGroup( CHRONOMETER_N_OBJS ), InterfaceControl( pControlListener ), paused( false ), soundFlag( false ),
			  pListener( pChronometerListener ), currTime( 0.0f ), startTime( 0.0f ), stopTime( 0.0f )
{
	build();
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

Chronometer::~Chronometer( void )
{
	pListener = NULL;
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/

void Chronometer::build( void )
{
	{ // Evita erros de compilação por causa dos gotos
		
	/*	#if CONFIG_TEXTURES_16_BIT
			RenderableImage *pBkg = new RenderableImage( "ctc", RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
		#else
			RenderableImage *pBkg = new RenderableImage( "ctc" );
		#endif

		CHECKED_GROUP_INSERT( this, pBkg );
		
		TextureFrame frame( 0.0f, 0.0f, CHRONOMETER_BKG_IMG_FRAME_WIDTH, CHRONOMETER_BKG_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pBkg->setImageFrame( frame );*/
		
		Label *pTimeLabel = new Label( [(( FreeKickAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_MAIN], false );
		CHECKED_GROUP_INSERT( this, pTimeLabel );
		
		pTimeLabel->setTextAlignment( ALIGN_HOR_CENTER | ALIGN_VER_CENTER );
		pTimeLabel->setCharSpacement( CHRONOMETER_FONT_CHAR_SPACEMENT );
		pTimeLabel->setSize( CHRONOMETER_LB_WIDTH, CHRONOMETER_LB_HEIGHT );
		pTimeLabel->setPosition( CHRONOMETER_LB_POS_X, CHRONOMETER_LB_POS_Y );
		
	//	setSize( pBkg->getWidth(), pBkg->getHeight() );
	
		updateTimeLabel();
		
		return;

	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
#if TARGET_IPHONE_SIMULATOR
		throw ConstructorException( "Chronometer::build(): Unable to create object" );
#else
		throw ConstructorException();
#endif
}

/*===========================================================================================
 
MÉTODO setTimeInterval
	Determina em que valor de tempo o cronômetro deve para de contar.

============================================================================================*/

void Chronometer::setTimeInterval( float startAt, float stopAt )
{
	startTime = static_cast< float >( fabs( startAt ) );
	stopTime = static_cast< float >( fabs( stopAt ) );
	
	currTime = startTime;
	
	updateTimeLabel();
}

/*===========================================================================================
 
MÉTODO getTimeInterval
	Retorna em que valor de tempo o cronômetro deve para de contar.

============================================================================================*/

void Chronometer::getTimeInterval( float* pStartAt, float* pStopAt ) const
{
	if( pStartAt )
		*pStartAt = startTime;

	if( pStopAt )
		*pStopAt = stopTime;
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool Chronometer::update( float timeElapsed )
{
	if( !ObjectGroup::update( timeElapsed ) )
		return false;
	
	switch( getInterfaceControlState() )
	{
		case INTERFACE_CONTROL_STATE_SHOWING:
			{
				float nextY = getPosition()->y + ( ( timeElapsed * getHeight() ) / CHRONOMETER_ANIM_MOVE_DUR );
				
				if( ( nextY + getHeight() > 0.0f ) && !soundFlag )
				{
					soundFlag = true;
					[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed:SOUND_NAME_TAG_INCOMING AtIndex:SOUND_INDEX_TAG_2_INCOMING Looping: false];
				}
				
				if( nextY > 0.0f )
				{
					nextY = 0.0f;
					setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
				}
				
				setPosition( getPosition()->x, nextY );
			}
			break;

		case INTERFACE_CONTROL_STATE_HIDING:
			{
				float startY = -getHeight();
				float nextY = getPosition()->y - ( ( timeElapsed * getHeight() ) / CHRONOMETER_ANIM_MOVE_DUR );
				
				if( nextY < startY )
				{
					nextY = startY;
					setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
				}
				
				setPosition( getPosition()->x, nextY );
			}
			break;

		case INTERFACE_CONTROL_STATE_SHOWN:
			if( !paused )
			{
				if( stopTime < startTime )
				{
					currTime -= timeElapsed;
					if( currTime < stopTime )
					{
						currTime = stopTime;
						pListener->onStopTimeReached();
					}
				}
				else
				{
					currTime += timeElapsed;
					if( currTime > stopTime )
					{
						currTime = stopTime;
						pListener->onStopTimeReached();
					}
				}
				updateTimeLabel();
			}
			break;
	}
	return true;
}

/*===========================================================================================
 
MÉTODO updateTimeLabel
	Atualiza o texto exibido pelo cronômetro.

============================================================================================*/

#define CHRONOMETER_UPDATE_TIME_LABEL_BUFFER_LEN 16

void Chronometer::updateTimeLabel( void )
{
	int32 seconds = int32( currTime );
	//int32 miliseconds = ( currTime - seconds ) * 1000.0f;
	
	char buffer[ CHRONOMETER_UPDATE_TIME_LABEL_BUFFER_LEN ];
	//snprintf( buffer, CHRONOMETER_UPDATE_TIME_LABEL_BUFFER_LEN, "%02d:%03d", seconds, miliseconds );
	snprintf( buffer, CHRONOMETER_UPDATE_TIME_LABEL_BUFFER_LEN, "%02d", seconds );

	static_cast< Label* >( getObject( CHRONOMETER_OBJ_INDEX_LABEL ) )->setText( buffer, false, false );

	// OLD
//	static_cast< Label* >( getObject( CHRONOMETER_OBJ_INDEX_LABEL ) )->setText( [NSString stringWithFormat: @"%02d:%03d", seconds, miliseconds], false, false );
}

#undef CHRONOMETER_UPDATE_TIME_LABEL_BUFFER_LEN

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exebição do controle.

============================================================================================*/

void Chronometer::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		setActive( true );
		setVisible( true );
	}
	else
	{
		setPosition( getPosition()->x, -getHeight() );

		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/

void Chronometer::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
	{
		soundFlag = false;
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
	}
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/

void Chronometer::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}
