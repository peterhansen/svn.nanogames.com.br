#include "CurtainTransition.h"

#include "GLHeaders.h"
#include "ObjcMacros.h"
#include "Quad.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

CurtainTransition::CurtainTransition( float duration, OGLTransitionListener* pListener, const Color& color )
		   : OGLTransition( pListener ), ended( false ), heightCounter( SCREEN_HEIGHT ),
			 transitionSpeed( heightCounter / duration ),
			 color( color )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

CurtainTransition::~CurtainTransition( void )
{
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool CurtainTransition::render( void )
{
	if( !OGLTransition::render() || ( color.getUbA() == 0 ) )
		return false;

	glColor4ub( color.getUbR(), color.getUbG(), color.getUbB(), color.getUbA() );

	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glTranslatef( HALF_SCREEN_WIDTH, heightCounter * 0.5f, 0.0f );	
	glScalef( SCREEN_WIDTH, heightCounter, 1.0f );
	
	Quad q;
	q.disableRenderStates( RENDER_STATE_TEXCOORD_ARRAY );
	q.render();
	
	glPopMatrix();
	
	// Coloca a cor padrão de volta
	glColor4ub( 255, 255, 255, 255 );

	return true;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool CurtainTransition::update( float timeElapsed )
{
	if( !OGLTransition::update( timeElapsed ) || ended )
		return false;
	
	if( heightCounter < 0.0f )
	{
		ended = true;
		pListener->onOGLTransitionEnd();
	}
	else
	{
		heightCounter -= transitionSpeed * timeElapsed;
	}
	return true;
}

/*==============================================================================================

MÉTODO update
	Reinicializa o objeto.

==============================================================================================*/

void CurtainTransition::reset( void )
{
	ended = false;
	heightCounter = SCREEN_HEIGHT;
}

