/*
 *  SetEffectGroup.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/13/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SET_EFFECT_SCENE_H
#define SET_EFFECT_SCENE_H 1

#include "AccelerometerListener.h"

#if TARGET_IPHONE_SIMULATOR
	#include "EventListener.h"
#endif

#include "ObjectGroup.h"
#include "Sprite.h"

#include "InterfaceControl.h"

// Forward Declarations
class Aim;
class EffectSoccerBall;
class Fader;

class SetEffectGroupListener
{
	public:
		// Destrutor
		virtual ~SetEffectGroupListener( void ){};

		// Informa ao listener o efeito empregado pelo chute
		virtual void onEffectSet( void ) = 0;
};

#if TARGET_IPHONE_SIMULATOR
class SetEffectGroup : public ObjectGroup, public EventListener, public AccelerometerListener, public SpriteListener, public InterfaceControl
#else
class SetEffectGroup : public ObjectGroup, public AccelerometerListener, public SpriteListener, public InterfaceControl
#endif
{
	public:
		///<DM>
		///descarrega da memória os assets
		void Unload();
		///carrega os assets da memória
		void Load();
		///</DM>
	
	
		// Construtor
		// Exceções: ConstructorException
		SetEffectGroup( SetEffectGroupListener* pEffectListener, InterfaceControlListener* pInterfaceListener );
	
		// Destrutor
		virtual ~SetEffectGroup( void );

		// Coloca o objeto em seu estado inicial
		void reset( float difficultyPercent, float kickPowerPercent );
	
		// Retorna a altura gerada pelo efeito
		float getEffectHeight( void ) const;
	
		// Retorna o fator de efeito do chute
		float getEffectFactor( void ) const;
	
		// Retorna a velocidade e o eixo de rotação da bola
		void getEffectAngularSpeed( float& angularSpeed, Point3f& angularSpeedAxis, float powerUsed ) const;
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
		// Confirma o efeito atual
		void setEffect( void );

	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );
	
		// Limpa as variáveis do objeto
		void clear( void );
	
		// Movimenta a mira dentro da bola de efeito
		void setAimPosition( float posX, float posY );
	
		// Chama o listener informando o efeito aplicado à bola
		void onEffectSet( void );
	
		#if TARGET_IPHONE_SIMULATOR
			// Método que trata os eventos enviados pelo sistema
			virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
		#endif
	
		// Recebe os eventos do acelerômetro
		virtual void onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration );
		
		// Chamado quando o sprite termina um sequência de animação
		virtual void onAnimEnded( Sprite* pSprite );
	
		// Quad utilizado para escurecer a cena atual
		Fader* pFader;
	
		// Mira do chute
		Aim* pAim;
	
		// Bola que servirá como alvo para a mira do chute
		EffectSoccerBall* pTarget;
	
		// Objeto que irá receber o efeito aplicado à bola
		SetEffectGroupListener* pEffectListener;
	
		// Porcentagem de força utilizada no chute
		float kickPwrPercent;
	
		// Auxiliares das animações
		float targetFinalScaleX, targetFinalScaleY;
};

#endif
