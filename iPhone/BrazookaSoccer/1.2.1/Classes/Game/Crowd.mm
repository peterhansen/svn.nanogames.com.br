#include "Crowd.h"

#include "Exceptions.h"
#include "RenderableImage.h"

#include "Ball_Defines.h"
#include "GameUtils.h"

// Número de "direções" (lado esquerdo, lado direito, cima, ...) da torcida
#define CROWD_N_DIRECTIONS 6

// Define quantas vezes devemos prolongar as partes laterais da torcida
#define CROWD_LEFT_N_EXTRAS		2
#define CROWD_RIGHT_N_EXTRAS	2
#define CROWD_TOP_N_EXTRAS		1

// Número de objetos no grupo
#define CROWD_N_OBJS ( CROWD_N_DIRECTIONS + CROWD_LEFT_N_EXTRAS + CROWD_RIGHT_N_EXTRAS + CROWD_TOP_N_EXTRAS )

// Índices das "direções" da torcida
#define CROWD_LEFT_INDEX				0
#define CROWD_TOP_LEFT_INDEX			1
#define CROWD_TOP_INDEX					2
#define CROWD_TOP_RIGHT_INDEX			3
#define CROWD_TOP_RIGHT_FIX_INDEX		4
#define CROWD_RIGHT_INDEX				5
#define CROWD_LEFT_1ST_EXTRA_INDEX		6
#define CROWD_LEFT_LAST_EXTRA_INDEX		( CROWD_LEFT_1ST_EXTRA_INDEX + CROWD_LEFT_N_EXTRAS - 1 )
#define CROWD_RIGHT_1ST_EXTRA_INDEX		( CROWD_LEFT_LAST_EXTRA_INDEX + 1 )
#define CROWD_RIGHT_LAST_EXTRA_INDEX	( CROWD_RIGHT_1ST_EXTRA_INDEX + CROWD_RIGHT_N_EXTRAS - 1 )
#define CROWD_TOP_1ST_EXTRA_INDEX		( CROWD_RIGHT_LAST_EXTRA_INDEX + 1 )
#define CROWD_TOP_LAST_EXTRA_INDEX		( CROWD_TOP_1ST_EXTRA_INDEX + CROWD_TOP_N_EXTRAS - 1 )

// Indica qual resolução de torcida estamos utilizando
#define CROWD_RESOLUTION 1024
//#define CROWD_RESOLUTION 512

// Offset de posicionamento em relação à realPosition
#if ( CROWD_RESOLUTION == 1024 ) || ( CROWD_RESOLUTION == 512 )
	#define CROWD_POS_OFFSET_X -3000.0f
	#define CROWD_POS_OFFSET_Y -2200.0f
#endif

// Define a escala padrão na qual utilizaremos a torcida
#if ( CROWD_RESOLUTION == 1024 )
	#define CROWD_DEFAULT_SCALE_X	3.0f
	#define CROWD_DEFAULT_SCALE_Y	3.0f
#elif ( CROWD_RESOLUTION == 512 )
	#define CROWD_DEFAULT_SCALE_X	6.0f
	#define CROWD_DEFAULT_SCALE_Y	6.0f
#else
	#error Defina uma resolução existente para a torcida
#endif

// Offset de posicionamento das imagens da torcida
#if ( CROWD_RESOLUTION == 1024 )

	#define CROWD_OFFSET_X_LEFT_LEFT ( CROWD_DEFAULT_SCALE_X *    0.0f )
	#define CROWD_OFFSET_Y_LEFT_LEFT ( CROWD_DEFAULT_SCALE_Y * -532.0f )

	#define CROWD_OFFSET_X_LEFT_TOPLEFT ( CROWD_DEFAULT_SCALE_X *   0.0f )
	#define CROWD_OFFSET_Y_LEFT_TOPLEFT ( CROWD_DEFAULT_SCALE_Y * -23.0f )

	#define CROWD_OFFSET_X_TOPLEFT_TOP ( CROWD_DEFAULT_SCALE_X *  0.0f )
	#define CROWD_OFFSET_Y_TOPLEFT_TOP ( CROWD_DEFAULT_SCALE_Y * 75.0f )

	#define CROWD_OFFSET_X_TOP_TOP ( CROWD_DEFAULT_SCALE_X *    0.0f )
	#define CROWD_OFFSET_Y_TOP_TOP ( CROWD_DEFAULT_SCALE_Y * -533.0f )

	#define CROWD_OFFSET_X_TOP_TOPRIGHT ( CROWD_DEFAULT_SCALE_X *    0.0f )
	#define CROWD_OFFSET_Y_TOP_TOPRIGHT ( CROWD_DEFAULT_SCALE_Y *  189.0f )

	#define CROWD_OFFSET_X_TOPRIGHT_TOPRIGHTFIX ( CROWD_DEFAULT_SCALE_X *    0.0f )
	#define CROWD_OFFSET_Y_TOPRIGHT_TOPRIGHTFIX ( CROWD_DEFAULT_SCALE_Y * -428.0f )

	#define CROWD_OFFSET_X_TOPRIGHTFIX_RIGHT ( CROWD_DEFAULT_SCALE_X *    0.0f )
	#define CROWD_OFFSET_Y_TOPRIGHTFIX_RIGHT ( CROWD_DEFAULT_SCALE_Y * -431.0f )

	#define CROWD_OFFSET_X_RIGHT_RIGHT ( CROWD_DEFAULT_SCALE_X *    0.0f )
	#define CROWD_OFFSET_Y_RIGHT_RIGHT ( CROWD_DEFAULT_SCALE_Y * -432.0f )

#elif ( CROWD_RESOLUTION == 512 )

	#define CROWD_OFFSET_X_LEFT_LEFT ( CROWD_DEFAULT_SCALE_X *   0.0f * 0.5f )
	#define CROWD_OFFSET_Y_LEFT_LEFT ( CROWD_DEFAULT_SCALE_Y * -532.0f * 0.5f )

	#define CROWD_OFFSET_X_LEFT_TOPLEFT ( CROWD_DEFAULT_SCALE_X *   0.0f * 0.5f )
	#define CROWD_OFFSET_Y_LEFT_TOPLEFT ( CROWD_DEFAULT_SCALE_Y * -23.0f * 0.5f )

	#define CROWD_OFFSET_X_TOPLEFT_TOP ( CROWD_DEFAULT_SCALE_X *  0.0f * 0.5f )
	#define CROWD_OFFSET_Y_TOPLEFT_TOP ( CROWD_DEFAULT_SCALE_Y * 75.0f * 0.5f )

	#define CROWD_OFFSET_X_TOP_TOP ( CROWD_DEFAULT_SCALE_X *    0.0f * 0.5f )
	#define CROWD_OFFSET_Y_TOP_TOP ( CROWD_DEFAULT_SCALE_Y * -533.0f * 0.5f )

	#define CROWD_OFFSET_X_TOP_TOPRIGHT ( CROWD_DEFAULT_SCALE_X *    0.0f * 0.5f )
	#define CROWD_OFFSET_Y_TOP_TOPRIGHT ( CROWD_DEFAULT_SCALE_Y *  189.0f * 0.5f )

	#define CROWD_OFFSET_X_TOPRIGHT_TOPRIGHTFIX ( CROWD_DEFAULT_SCALE_X *    0.0f * 0.5f )
	#define CROWD_OFFSET_Y_TOPRIGHT_TOPRIGHTFIX ( CROWD_DEFAULT_SCALE_Y * -428.0f * 0.5f )

	#define CROWD_OFFSET_X_TOPRIGHTFIX_RIGHT ( CROWD_DEFAULT_SCALE_X *    0.0f * 0.5f )
	#define CROWD_OFFSET_Y_TOPRIGHTFIX_RIGHT ( CROWD_DEFAULT_SCALE_Y * -430.0f * 0.5f )

	#define CROWD_OFFSET_X_RIGHT_RIGHT ( CROWD_DEFAULT_SCALE_X *    0.0f * 0.5f )
	#define CROWD_OFFSET_Y_RIGHT_RIGHT ( CROWD_DEFAULT_SCALE_Y * -432.0f * 0.5f )

#endif

// Tamanho dos frames
#if ( CROWD_RESOLUTION == 1024 )

	#define CROWD_FRAMES_OFFSET_X 2

	#define CROWD_FRAME_LEFT_WIDTH 339.0f
	#define CROWD_FRAME_LEFT_HEIGHT 680.0f

	#define CROWD_FRAME_TOP_LEFT_WIDTH 872.0f
	#define CROWD_FRAME_TOP_LEFT_HEIGHT 607.0f

	#define CROWD_FRAME_TOP_WIDTH 340.0f
	#define CROWD_FRAME_TOP_HEIGHT 722.0f

	#define CROWD_FRAME_TOP_RIGHT_WIDTH 381.0f
	#define CROWD_FRAME_TOP_RIGHT_HEIGHT 928.0f

	#define CROWD_FRAME_TOP_RIGHT_FIX_WIDTH 95.0f
	#define CROWD_FRAME_TOP_RIGHT_FIX_HEIGHT 473.0f

	#define CROWD_FRAME_RIGHT_WIDTH 405.0f
	#define CROWD_FRAME_RIGHT_HEIGHT 619.0f

#elif ( CROWD_RESOLUTION == 512 )

	#define CROWD_FRAMES_OFFSET_X 3

	#define CROWD_FRAME_LEFT_WIDTH 168.0f
	#define CROWD_FRAME_LEFT_HEIGHT 340.0f

	#define CROWD_FRAME_TOP_LEFT_WIDTH 434.0f
	#define CROWD_FRAME_TOP_LEFT_HEIGHT 304.0f

	#define CROWD_FRAME_TOP_WIDTH 168.0f
	#define CROWD_FRAME_TOP_HEIGHT 361.0f

	#define CROWD_FRAME_TOP_RIGHT_WIDTH 190.0f
	#define CROWD_FRAME_TOP_RIGHT_HEIGHT 464.0f

	#define CROWD_FRAME_TOP_RIGHT_FIX_WIDTH 45.0f
	#define CROWD_FRAME_TOP_RIGHT_FIX_HEIGHT 237.0f

	#define CROWD_FRAME_RIGHT_WIDTH 200.0f
	#define CROWD_FRAME_RIGHT_HEIGHT 310.0f

#endif

// Índices dos quads de colisão da torcida
#define CROWD_COLLISION_QUAD_INDEX_LEFT		0
#define CROWD_COLLISION_QUAD_INDEX_RIGHT	1
#define CROWD_COLLISION_QUAD_INDEX_TOP		2
#define CROWD_COLLISION_QUAD_INDEX_BOTTOM	3

// Altura dos quads de colisão da torcida. Define alguma coisa BEM alta para garantir que a bola não vai passar por cima
#define SOMETHING_REALLY_HIGH 100.0f

// Largura extra do campo após as marcações
#define CROWD_OUTFIELD_WIDTH 9.0f

// Profundidade extra do campo após as marcações
#define CROWD_OUTFIELD_DEPTH 8.0f

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/	

Crowd::Crowd( void ) : ObjectGroup( CROWD_N_OBJS ), RealPosOwner(), crowdState(	CROWD_STATE_UNDEFINED ), lastCollisionQuad( -1 )
{
	// Declara os quads de colisão
	uint8 currQuad = 0;
	
	Point3f pos( -( ( REAL_FIELD_WIDTH * 0.5f ) + CROWD_OUTFIELD_WIDTH ), 0.0f, REAL_FIELD_DEPTH * 0.5f ),
			top( 0.0f, 1.0f, 0.0f ),
			right( 0.0f, 0.0f, -1.0f );

	collisionQuads[ currQuad++ ].set( pos, top, right, ORIENT_BOTTOM, REAL_FIELD_DEPTH + ( CROWD_OUTFIELD_DEPTH * 2.0f ), SOMETHING_REALLY_HIGH );
	
	pos.x = -pos.x - 4.0f;
	right.z = 1.0f;
	collisionQuads[ currQuad++ ].set( pos, top, right, ORIENT_BOTTOM, REAL_FIELD_DEPTH + ( CROWD_OUTFIELD_DEPTH * 2.0f ) + 4.0f, SOMETHING_REALLY_HIGH );
	
	pos.set( -1.5f, 0.0f, -CROWD_OUTFIELD_DEPTH );
	right.set( 1.0f, 0.0f, 0.0f );
	collisionQuads[ currQuad++ ].set( pos, top, right, ORIENT_BOTTOM, REAL_FIELD_WIDTH + ( CROWD_OUTFIELD_WIDTH * 2.0f ), SOMETHING_REALLY_HIGH );
	
	pos.z = REAL_FIELD_DEPTH - pos.z;
	right.x = -1.0f;
	collisionQuads[ currQuad++ ].set( pos, top, right, ORIENT_BOTTOM, REAL_FIELD_WIDTH + ( CROWD_OUTFIELD_WIDTH * 2.0f ), SOMETHING_REALLY_HIGH );
	
#if DEBUG
	if( currQuad < CROWD_N_COLLISION_QUADS )
		throw NanoBaseException( "Não configurou todos os quads de colisão de torcida" );
#endif
}

/*===========================================================================================
 
DESTRUTOR

============================================================================================*/	

Crowd::~Crowd( void )
{
	crowdState = CROWD_STATE_UNDEFINED;
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/	

void Crowd::build( CrowdState state )
{
	{ // Evita erros de compilação por causa dos gotos

		char path[] = { 'c', 'r', 'w', ' ', ( '0' + state ), '\0' };
		
		for( uint8 i = 0 ; i < CROWD_N_DIRECTIONS ; ++i )
		{
			path[ 3 ] = '0' + i;

			#if CONFIG_TEXTURES_16_BIT
				RenderableImage* pImg = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			#else
				RenderableImage* pImg = new RenderableImage( path );
			#endif
			if( !pImg || !insertObject( pImg ) )
			{
				delete pImg;
				goto Error;
			}
			pImg->setScale( CROWD_DEFAULT_SCALE_X, CROWD_DEFAULT_SCALE_Y );
		}
		
		// Aloca as repetições da esquerda
		for( uint8 i = 0 ; i < CROWD_LEFT_N_EXTRAS ; ++i )
		{
			RenderableImage* pImg = new RenderableImage( static_cast< RenderableImage* >( getObject( CROWD_LEFT_INDEX ) ) );
			if( !pImg || !insertObject( pImg ) )
			{
				delete pImg;
				goto Error;
			}
			pImg->setScale( CROWD_DEFAULT_SCALE_X, CROWD_DEFAULT_SCALE_Y );
		}
		
		// Aloca as repetições da direita
		for( uint8 i = 0 ; i < CROWD_RIGHT_N_EXTRAS ; ++i )
		{
			RenderableImage* pImg = new RenderableImage( static_cast< RenderableImage* >( getObject( CROWD_RIGHT_INDEX ) ) );
			if( !pImg || !insertObject( pImg ) )
			{
				delete pImg;
				goto Error;
			}
			pImg->setScale( CROWD_DEFAULT_SCALE_X, CROWD_DEFAULT_SCALE_Y );
		}
		
		// Aloca as repetições da parte superior
		for( uint8 i = 0 ; i < CROWD_TOP_N_EXTRAS ; ++i )
		{
			RenderableImage* pImg = new RenderableImage( static_cast< RenderableImage* >( getObject( CROWD_TOP_INDEX ) ) );
			if( !pImg || !insertObject( pImg ) )
			{
				delete pImg;
				goto Error;
			}
			pImg->setScale( CROWD_DEFAULT_SCALE_X, CROWD_DEFAULT_SCALE_Y );
		}
		
		return;
		
	} // Evita erros de compilação por causa dos gotos

	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "void Crowd::build( void ): Unable to create object" );
	#else
		throw ConstructorException( "" );
	#endif
}

/*============================================================================================

MÉTODO updatePosition
	Atualiza a posição do objeto na tela.

=============================================================================================*/

void Crowd::updatePosition( void )
{
	Point3f myPos = GameUtils::isoGlobalToPixels( realPosition );
	setPosition( myPos.x + CROWD_POS_OFFSET_X, myPos.y + CROWD_POS_OFFSET_Y );
}

/*============================================================================================

MÉTODO setCrowdState
	Determina o estado do objeto.

=============================================================================================*/

void Crowd::setCrowdState( CrowdState state )
{
	lastCollisionQuad = -1;
	
	if( state == crowdState )
		return;

	// Deleta todas as imagens da torcida
	crowdState = CROWD_STATE_UNDEFINED;
	removeAllObjects();
	
	// Aloca as imagens do novo estado
	// Exceções: ConstructorException
	build( state );
	
	// Armazena o novo estado da torcida
	crowdState = state;

	// Posiciona os objetos dentro do grupo
	RenderableImage* pLeft = static_cast< RenderableImage* >( getObject( CROWD_LEFT_INDEX ) );
	
	TextureFrame imgFrame;
	imgFrame.x = CROWD_FRAMES_OFFSET_X;
	imgFrame.width = CROWD_FRAME_LEFT_WIDTH;
	imgFrame.height = CROWD_FRAME_LEFT_HEIGHT;
	pLeft->setImageFrame( imgFrame );
	pLeft->setPosition( 0.0f, 0.0f );
	
	for( uint8 i = 0 ; i < CROWD_LEFT_N_EXTRAS ; ++i )
	{
		RenderableImage* pImg = static_cast< RenderableImage* >( getObject( CROWD_LEFT_1ST_EXTRA_INDEX + i ) );
		pImg->setImageFrame( imgFrame );
		pImg->setPosition( pLeft->getPosition()->x - (( pLeft->getWidth() + CROWD_OFFSET_X_LEFT_LEFT ) * ( i + 1 ) ), pLeft->getPosition()->y + (( pLeft->getHeight() + CROWD_OFFSET_Y_LEFT_LEFT ) * ( i + 1 ) ) );
	}

	RenderableImage* pTopLeft = static_cast< RenderableImage* >( getObject( CROWD_TOP_LEFT_INDEX ) );
	imgFrame.width = CROWD_FRAME_TOP_LEFT_WIDTH;
	imgFrame.height = CROWD_FRAME_TOP_LEFT_HEIGHT;
	pTopLeft->setImageFrame( imgFrame );
	pTopLeft->setPosition( pLeft->getPosition()->x + pLeft->getWidth() + CROWD_OFFSET_X_LEFT_TOPLEFT, CROWD_OFFSET_Y_LEFT_TOPLEFT );

	RenderableImage* pTop = static_cast< RenderableImage* >( getObject( CROWD_TOP_INDEX ) );
	imgFrame.width = CROWD_FRAME_TOP_WIDTH;
	imgFrame.height = CROWD_FRAME_TOP_HEIGHT;
	pTop->setImageFrame( imgFrame );
	pTop->setPosition( pTopLeft->getPosition()->x + pTopLeft->getWidth() + CROWD_OFFSET_X_TOPLEFT_TOP, pTopLeft->getPosition()->y + CROWD_OFFSET_Y_TOPLEFT_TOP );
	
	for( uint8 i = 0 ; i < CROWD_TOP_N_EXTRAS ; ++i )
	{
		RenderableImage* pImg = static_cast< RenderableImage* >( getObject( CROWD_TOP_1ST_EXTRA_INDEX + i ) );
		pImg->setImageFrame( imgFrame );
		pImg->setPosition( pTop->getPosition()->x + (( pTop->getWidth() + CROWD_OFFSET_X_TOP_TOP ) * ( i + 1 ) ), pTop->getPosition()->y + (( pTop->getHeight() + CROWD_OFFSET_Y_TOP_TOP ) * ( i + 1 ) ) );
	}

	RenderableImage* pTopRight = static_cast< RenderableImage* >( getObject( CROWD_TOP_RIGHT_INDEX ) );
	RenderableImage* pTopLast = static_cast< RenderableImage* >( getObject( CROWD_TOP_LAST_EXTRA_INDEX ) );
	imgFrame.width = CROWD_FRAME_TOP_RIGHT_WIDTH;
	imgFrame.height = CROWD_FRAME_TOP_RIGHT_HEIGHT;
	pTopRight->setImageFrame( imgFrame );
	pTopRight->setPosition( pTopLast->getPosition()->x + pTopLast->getWidth() + CROWD_OFFSET_X_TOP_TOPRIGHT, pTopLast->getPosition()->y + CROWD_OFFSET_Y_TOP_TOPRIGHT );

	RenderableImage* pTopRightFix = static_cast< RenderableImage* >( getObject( CROWD_TOP_RIGHT_FIX_INDEX ) );
	imgFrame.width = CROWD_FRAME_TOP_RIGHT_FIX_WIDTH;
	imgFrame.height = CROWD_FRAME_TOP_RIGHT_FIX_HEIGHT;
	pTopRightFix->setImageFrame( imgFrame );
	pTopRightFix->setPosition( pTopRight->getPosition()->x - pTopRightFix->getWidth() + CROWD_OFFSET_X_TOPRIGHT_TOPRIGHTFIX, pTopRight->getPosition()->y + pTopRight->getHeight() + CROWD_OFFSET_Y_TOPRIGHT_TOPRIGHTFIX );
	
	RenderableImage* pRight = static_cast< RenderableImage* >( getObject( CROWD_RIGHT_INDEX ) );
	imgFrame.width = CROWD_FRAME_RIGHT_WIDTH;
	imgFrame.height = CROWD_FRAME_RIGHT_HEIGHT;
	pRight->setImageFrame( imgFrame );
	pRight->setPosition( pTopRightFix->getPosition()->x - pRight->getWidth() + CROWD_OFFSET_X_TOPRIGHTFIX_RIGHT, pTopRightFix->getPosition()->y + pTopRightFix->getHeight() + CROWD_OFFSET_Y_TOPRIGHTFIX_RIGHT );
	
	for( uint8 i = 0 ; i < CROWD_RIGHT_N_EXTRAS ; ++i )
	{
		RenderableImage* pImg = static_cast< RenderableImage* >( getObject( CROWD_RIGHT_1ST_EXTRA_INDEX + i ) );
		pImg->setImageFrame( imgFrame );
		pImg->setPosition( pRight->getPosition()->x - (( pRight->getWidth() + CROWD_OFFSET_X_RIGHT_RIGHT ) * ( i + 1 ) ), pRight->getPosition()->y + (( pRight->getHeight() + CROWD_OFFSET_Y_RIGHT_RIGHT ) * ( i + 1 ) ) );
	}
	
	// Conserta o ordenamento z
	switchObjectsZOrder( CROWD_TOP_RIGHT_FIX_INDEX, CROWD_TOP_LAST_EXTRA_INDEX );
	
	// Determina o tamanho do grupo
	setSize( pTop->getPosition()->x + pTop->getWidth(), pRight->getPosition()->y + pRight->getHeight() );
}

/*============================================================================================

MÉTODO render
	Renderiza o objeto.

=============================================================================================*/

#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )

bool Crowd::render( void )
{
	if( ObjectGroup::render() )
	{
		// Renderiza os quads de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		for( uint8 i = 0 ; i < 4 ; ++i )
		{
			Point3f pos = GameUtils::isoGlobalToPixels( collisionQuads[i].position );
			Point3f dx = GameUtils::isoGlobalToPixels( collisionQuads[i].right * collisionQuads[i].width );
			Point3f dy = GameUtils::isoGlobalToPixels( collisionQuads[i].top * collisionQuads[i].height );

			dy = -dy;
			
			Point3f top[ 2 ] = {
									pos,
									pos + dy,
								};
			
			glColor4ub( 255, 0, 0, 255 );
			glVertexPointer( 3, GL_FLOAT, 0, top );
			glDrawArrays( GL_LINES, 0, 2 );
			
			Point3f right[ 2 ] = {
									pos,
									pos + dx,
								};
			
			glColor4ub( 0, 255, 0, 255 );
			glVertexPointer( 3, GL_FLOAT, 0, right );
			glDrawArrays( GL_LINES, 0, 2 );
			
			Point3f others[ 4 ] = {
									pos + dy,
									pos + dy + dx,
									pos + dy + dx,
									pos + dx
								};

			glColor4ub( 255, 255, 0, 255 );
			glVertexPointer( 3, GL_FLOAT, 0, others );
			glDrawArrays( GL_LINES, 0, 4 );

			// Renderiza a normal do quad
			if( i >= 2 )
			{			
				if( collisionQuads[i].normal.z > 0.0f )
					glColor4ub( 255, 0, 255, 255 );
				else
					glColor4ub( 255, 183, 15, 255 );
			}
			else
			{
				if( collisionQuads[i].normal.x > 0.0f )
					glColor4ub( 255, 0, 255, 255 );
				else
					glColor4ub( 255, 183, 15, 255 );
			}
			
			Point3f normalPos = pos + ( dx * 0.5f ) + ( dy * 0.5f );
			glVertexPointer( 3, GL_FLOAT, 0, &normalPos );
			glDrawArrays( GL_POINTS, 0, 1 );
		}

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		glColor4ub( 255, 255, 255, 255 );

		return true;
	}
	return false;
}

#endif

/*============================================================================================

MÉTODO getCollisionTopZ
	Retorna a menor coordenada z a partir da qual temos torcida.

=============================================================================================*/

float Crowd::getCollisionTopZ( void ) const
{
	return collisionQuads[ CROWD_COLLISION_QUAD_INDEX_TOP ].position.z;
}

/*============================================================================================

MÉTODO getCollisionLeftX
	Retorna a menor coordenada x a partir da qual temos torcida.

=============================================================================================*/

float Crowd::getCollisionLeftX( void ) const
{
	return collisionQuads[ CROWD_COLLISION_QUAD_INDEX_LEFT ].position.x;
}

/*============================================================================================

MÉTODO getCollisionRightX
	Retorna a maior coordenada x a partir da qual temos torcida.

=============================================================================================*/

float Crowd::getCollisionRightX( void ) const
{
	return collisionQuads[ CROWD_COLLISION_QUAD_INDEX_RIGHT ].position.x;
}

