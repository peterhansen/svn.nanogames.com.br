#include "BlondBarrierPlayer.h"

#include "Exceptions.h"

#include "GameUtils.h"

// Número de objetos contidos no grupo
#define BLOND_N_OBJS 2

// Índices dos objetos contidos no grupo
#define BLOND_OBJ_INDEX_SHADOW 0
#define BLOND_OBJ_INDEX_PLAYER 1

// Altura e largura deste personagem
#define BLOND_WIDTH		0.60f
#define BLOND_HEIGHT	1.77f

// Offset do pixel de referência do personagem
#define BLOND_FEET_OFFSET_Y 18.0f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

BlondBarrierPlayer::BlondBarrierPlayer( void ) : BarrierPlayer( BLOND_N_OBJS )
{
	// Aloca o personagem
	char aux[] = "engs";
	
	{ // Evita erros de compilação por causa dos gotos
	
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pShadow = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pShadow = new Sprite( aux, aux );
		#endif
		if( !pShadow || !insertObject( pShadow ) )
		{
			delete pShadow;
			goto Error;
		}
		
		aux[3] = '\0';
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pBlond = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pBlond = new Sprite( aux, aux );
		#endif
		
		if( !pBlond || !insertObject( pBlond ) )
		{
			delete pBlond;
			goto Error;
		}
		pBlond->setListener( this );

		// Determina o tamanho do grupo
		setSize( pBlond->getWidth(), pBlond->getHeight() );
		
		float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, BLOND_HEIGHT, 0.0f ).y ) / pBlond->getStepFrameHeight() );
		setScale( scaleFactor, scaleFactor );
		
		#if DEBUG
			setName( "ingles" );
		#endif

		return;
		
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "BlondBarrierPlayer::BlondBarrierPlayer( void ): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

BlondBarrierPlayer::~BlondBarrierPlayer( void )
{
}

/*===========================================================================================
 
MÉTODO getRealWidth
	Retorna a largura real do jogador da barreira (em metros).

============================================================================================*/

float BlondBarrierPlayer::getRealWidth( void ) const
{
	return BLOND_WIDTH - 0.15f;
}

/*===========================================================================================
 
MÉTODO prepare
	Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente.

============================================================================================*/

void BlondBarrierPlayer::prepare( const Point3f& ballRealPos )
{
	setActive( false );

	Sprite* pBlond = static_cast< Sprite* >( getObject( BLOND_OBJ_INDEX_PLAYER ) );

	switch( direction )
	{
		case DIRECTION_DOWN:
			pBlond->setAnimSequence( 1, false );
			
			if( pBlond->isMirrored( MIRROR_HOR ) )
				pBlond->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_LEFT:
		case DIRECTION_DOWN_LEFT:
			pBlond->setAnimSequence( 2, false );

			if( pBlond->isMirrored( MIRROR_HOR ) )
				pBlond->unmirror( MIRROR_HOR );

			break;
	
		case DIRECTION_RIGHT:
		case DIRECTION_DOWN_RIGHT:
			pBlond->setAnimSequence( 2, false );
			
			if( !pBlond->isMirrored( MIRROR_HOR ) )
				pBlond->mirror( MIRROR_HOR );

			break;
			
		case DIRECTION_UP_LEFT:
			pBlond->setAnimSequence( 0, false );
			
			if( pBlond->isMirrored( MIRROR_HOR ) )
				pBlond->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_RIGHT:
			pBlond->setAnimSequence( 0, false );
			
			if( !pBlond->isMirrored( MIRROR_HOR ) )
				pBlond->mirror( MIRROR_HOR );

			break;
			
		// Nunca acontecerá esta possibilidade
//		case DIRECTION_UP:
//			break;
	}
	
	TextureFrame currFrameInfo;
	pBlond->getStepFrameInfo( &currFrameInfo, pBlond->getCurrAnimSequence(), pBlond->getCurrAnimSequenceStep() );
	setAnchor( ( currFrameInfo.offsetX * getScale()->x ) + ( currFrameInfo.width * getScale()->x * 0.5f ),
			   ( currFrameInfo.offsetY * getScale()->y ) + (( currFrameInfo.height - BLOND_FEET_OFFSET_Y ) * getScale()->y ), 0.0f );
}

/*===============================================================================

MÉTODO onAnimStepChanged
	Chamado quando o sprite muda de etapa de animação.

================================================================================*/

void BlondBarrierPlayer::onAnimStepChanged( Sprite* pSprite )
{
	Sprite* pBlond = static_cast< Sprite* >( getObject( BLOND_OBJ_INDEX_PLAYER ) );
	Sprite* pShadow = static_cast< Sprite* >( getObject( BLOND_OBJ_INDEX_SHADOW ) );
	
	int16 currSeq = pBlond->getCurrAnimSequence();
	if( ( currSeq == 2 ) && pBlond->isMirrored( MIRROR_HOR ) )
		pShadow->setAnimSequence( 3, false, false );
	else
		pShadow->setAnimSequence( currSeq, false, false );

	pShadow->setCurrentAnimSequenceStep( pBlond->getCurrAnimSequenceStep() );
	
	// Garante que o posicionamento dos elementos do grupo estará correto
	updatePosition();
}

/*===========================================================================================
 
MÉTODO updateFrame
	Atualiza o frame do jogador de acordo com a direção para a qual está virado.

============================================================================================*/

void BlondBarrierPlayer::updateFrame( void )
{
	float width = BLOND_WIDTH, height = BLOND_HEIGHT;
	Point3f top( VECTOR_UP_90 ), right, offset;
	
	getOrtho( &right );

	Sprite* pBlond = static_cast< Sprite* >( getObject( BLOND_OBJ_INDEX_PLAYER ) );
	switch( pBlond->getCurrAnimSequence() )
	{
		case 0:
			{
				int16 step = pBlond->getCurrAnimSequenceStep();
				if( step == 1 )
				{
					top.set( VECTOR_UP_80 );
					offset.x += 0.15f;
					width += 0.15f;
				}
				else if( step == 2 )
				{
					height += 0.10f;
				}
			}
			break;

		case 1:
			{
				int16 step = pBlond->getCurrAnimSequenceStep();
				if( step == 1 )
				{
					width += 0.10f;
					height -= 0.10f;
				}
				else if( step == 2 )
				{
					width += 0.2f;
				}
			}
			break;
			
		case 2:
			{
				int16 step = pBlond->getCurrAnimSequenceStep();
				if( step == 1 )
				{
					offset.x += 0.10f;
					height -= 0.10f;
					width += 0.05f;
				}
				else if( step == 2 )
				{
					offset.x += 0.05f;
					width += 0.05f;
				}
				else if( step == 3 )
				{
					width += 0.15f;
					height += 0.10f;
				}
			}
			break;
	}

	// OLD
//	TextureFrame currFrameInfo;
//	pBlond->getStepFrameInfo( &currFrameInfo, pBlond->getCurrAnimSequence(), pBlond->getCurrAnimSequenceStep() );
//
//	Point3f aux = GameUtils::isoGlobalToPixels( realPosition.x + offset.x, realPosition.y + offset.y, realPosition.z + offset.z );
//	aux.x += pBlond->getPosition()->x + ( currFrameInfo.offsetX * getScale()->x );
//	aux.y += pBlond->getPosition()->y + ( currFrameInfo.offsetY * getScale()->y );
//	Point3f realPos = GameUtils::isoPixelsToGlobalY0( aux.x, aux.y );
//
//	collisionArea.set( realPos, top, right, ORIENT_TOP_LEFT, width, height );

	collisionArea.set( realPosition + offset, top, right, ORIENT_BOTTOM, width, height );
}

/*===============================================================================

MÉTODO updatePosition
	Atualiza a posição da bola na tela (não altera a posição real).
   
=============================================================================== */

void BlondBarrierPlayer::updatePosition( void )
{	
	// Atualiza a orientação e o retângulo de colisão go goleiro
	updateFrame();
	
	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( p.x, p.y, 0.0f );
}

/*===============================================================================

MÉTODO onBallGettingClose
	Faz com que o jogador exiba alguma reação devido à aproximação da bola.

================================================================================*/

void BlondBarrierPlayer::onBallGettingClose( void )
{
	if( !didReactToBall() )
	{
		BarrierPlayer::onBallGettingClose();
		setActive( true );
	}
}

/*===============================================================================

MÉTODO render
	Renderiza o objeto.

================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )

bool BlondBarrierPlayer::render( void )
{
	if( ObjectGroup::render() )
	{
		// Renderiza o quad de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		Point3f pos = GameUtils::isoGlobalToPixels( collisionArea.position );
		Point3f dx = GameUtils::isoGlobalToPixels( collisionArea.right * collisionArea.width );
		Point3f dy = GameUtils::isoGlobalToPixels( collisionArea.top * collisionArea.height );

		dy = -dy;
		
		Point3f top[ 2 ] = {
								pos,
								pos + dy,
							};
		
		glColor4ub( 255, 0, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f right[ 2 ] = {
								pos,
								pos + dx,
							};
		
		glColor4ub( 0, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f others[ 4 ] = {
								pos + dy,
								pos + dy + dx,
								pos + dy + dx,
								pos + dx
							};

		glColor4ub( 255, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, others );
		glDrawArrays( GL_LINES, 0, 4 );

		Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
		if( collisionArea.normal.z > 0.0f )
			glColor4ub( 179, 179, 179, 255 );
		else
			glColor4ub( 255, 183, 15, 255 );

		glVertexPointer( 3, GL_FLOAT, 0, &nomal );
		glDrawArrays( GL_POINTS, 0, 1 );

//		// Renderiza a moldura do sprite
//		Sprite* pBlond = static_cast< Sprite* >( getObject( BLOND_OBJ_INDEX_PLAYER ) );
//		TextureFrame currFrameInfo;
//		pBlond->getStepFrameInfo( &currFrameInfo, pBlond->getCurrAnimSequence(), pBlond->getCurrAnimSequenceStep() );
//		
//		float spritePosX = getPosition()->x + pBlond->getPosition()->x + currFrameInfo.offsetX + 0.375f;
//		float spritePosY = getPosition()->y + pBlond->getPosition()->y + currFrameInfo.offsetY + 0.375f;
//		
//		float spriteWidth = currFrameInfo.width * getScale()->x;
//		float spriteHeight = currFrameInfo.height * getScale()->y;
//		
//		Point3f border[ 4 ] = {
//								Point3f( spritePosX, spritePosY ),
//								Point3f( spritePosX, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY )
//							  };
//		
//		glColor4ub( 0, 255, 255, 255 );
//
//		glVertexPointer( 3, GL_FLOAT, 0, border );
//		glDrawArrays( GL_LINE_LOOP, 0, 4 );

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		
		glColor4ub( 255, 255, 255, 255 );
		
		return true;
	}
	return false;
}

#endif
