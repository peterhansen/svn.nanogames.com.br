#include "GameScreen.h"

// Components
#include "AccelerometerManager.h"
#include "Exceptions.h"
#include "Line.h"
#include "LoadingView.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"

#include "TexturePattern.h"

#include "Random.h"
#include "RenderableImage.h"
#include "ResourceManager.h"

// Game
#include "AnimatedCamera.h"
#include "Ball_Defines.h"
#include "BallTarget.h"
#include "Barrier.h"
#include "Button.h"
#include "Chronometer.h"
#include "CornerFlag.h"
#include "Crowd.h"
#include "CurtainTransition.h"
#include "DirectionMeter.h"
#include "FeedbackMsg.h"
#include "FieldMarks.h"
#include "FreeKickAppDelegate.h"
#include "GameUtils.h"

// TODOO : Descomentar no update
//#include "GLButton.h"

#include "Goal.h"
#include "InfoTab.h"
#include "InterfaceControl.h"
#include "IsoBall.h"
#include "IsoKeeper.h"
#include "IsoPlayer.h"
#include "ParkingLot.h"
#include "PowerMeter.h"
#include "SliceTransition.h"

#include "BlondBarrierPlayer.h"
#include "FearsomeBarrierPlayer.h"
#include "JapaneseBarrierPlayer.h"
#include "SleepyBarrierPlayer.h"
#include "WoodBarrierPlayer.h"

#include "Tests.h"

// Determina o número de objetos na cena
#if IS_CURR_TEST( TEST_PVR )
#define SCENE_N_OBJECTS ( 19 + BARRIER_MAX_N_PLAYERS )
#elif IS_CURR_TEST( TEST_PALETTE )
#define SCENE_N_OBJECTS ( 19 + BARRIER_MAX_N_PLAYERS )
#else
#define SCENE_N_OBJECTS ( 18 + BARRIER_MAX_N_PLAYERS + REPLAY_N_BUTTONS )
#endif

// Índices dos objetos contidos no grupo
#define OBJ_INDEX_FIELD					  0
#define OBJ_INDEX_PARKING_LOT			  1
#define OBJ_INDEX_FIELD_MARKS			  2
#define OBJ_INDEX_FIELD_CROWD			  3
#define OBJ_INDEX_LEFT_FLAG_SHADOW		  4
#define OBJ_INDEX_RIGHT_FLAG_SHADOW		  5
#define OBJ_INDEX_GOAL_BOTTOM			  6
#define OBJ_INDEX_BALL					  7
#define OBJ_INDEX_PLAYER				  8
#define OBJ_INDEX_GOAL_TOP				  9
#define OBJ_INDEX_KEEPER				 10
#define OBJ_INDEX_BALL_TARGET			 11
#define OBJ_INDEX_LEFT_FLAG				 12
#define OBJ_INDEX_RIGHT_FLAG			 13
#define OBJ_INDEX_BARRIER_FEARSOME		 14
#define OBJ_INDEX_BARRIER_BLOND			 15
#define OBJ_INDEX_BARRIER_JAPANESE		 16
#define OBJ_INDEX_BARRIER_SLEEPY		 17
#define OBJ_INDEX_BARRIER_WOOD_FIRST	 18
#define OBJ_INDEX_BARRIER_WOOD_LAST		 ( OBJ_INDEX_BARRIER_WOOD_FIRST + BARRIER_MAX_N_PLAYERS - 1 )

#if IS_CURR_TEST( TEST_PVR )
#define OBJ_INDEX_PVR_RENDERABLE	 ( OBJ_INDEX_BARRIER_WOOD_LAST + 1 )
#elif IS_CURR_TEST( TEST_PALETTE )
#define OBJ_INDEX_PALETTE_RENDERABLE ( OBJ_INDEX_BARRIER_WOOD_LAST + 1 )
#endif

// Intervalo de objetos no qual deveremos controlar e alterar a ordem Z
#define FIRST_REAL_POS_OWNER OBJ_INDEX_GOAL_BOTTOM
#define LAST_REAL_POS_OWNER OBJ_INDEX_BARRIER_WOOD_LAST

// Índices do array de botões de controle do replay
#define REPLAY_BT_REWIND	0
#define REPLAY_BT_SLOWDOWN	1
#define REPLAY_BT_PAUSEPLAY	2
#define REPLAY_BT_FASTER	3
#define REPLAY_BT_CAMTOGGLE	4
#define REPLAY_BT_STOP		5
#define REPLAY_BT_PLAY		6
#define REPLAY_BT_HIDE		7
#define REPLAY_BT_SHOW		8

// Configurações do botão de "chutar a bola"
#define GAME_SCREEN_BT_KICK_DIST_FROM_LEFT 22.0f
#define GAME_SCREEN_BT_KICK_DIST_FROM_BOTTOM 21.0f
#define GAME_SCREEN_BT_KICK_WIDTH 64.0f
#define GAME_SCREEN_BT_KICK_HEIGHT 64.0f

// Configurações do botão de "mostrar informações"
#define GAME_SCREEN_BT_INFO_DIST_FROM_RIGHT 23.0f
#define GAME_SCREEN_BT_INFO_DIST_FROM_TOP 18.0f
#define GAME_SCREEN_BT_INFO_WIDTH 61.0f
#define GAME_SCREEN_BT_INFO_HEIGHT 64.0f

// Configurações do botão de "pausar o jogo"
#define GAME_SCREEN_BT_PAUSE_DIST_FROM_RIGHT 22.0f
#define GAME_SCREEN_BT_PAUSE_DIST_FROM_BOTTOM 18.0f
#define GAME_SCREEN_BT_PAUSE_WIDTH 62.0f
#define GAME_SCREEN_BT_PAUSE_HEIGHT 57.0f

// Configurações do botão de "focar a bola"
#define GAME_SCREEN_BT_FOCUS_DIST_FROM_RIGHT 22.0f
#define GAME_SCREEN_BT_FOCUS_DIST_FROM_TOP 18.0f
#define GAME_SCREEN_BT_FOCUS_WIDTH	54.0f
#define GAME_SCREEN_BT_FOCUS_HEIGHT	55.0f

// Configurações do medidor de força
#define GAME_SCREEN_POWER_METER_DIST_FROM_TOP 15.0f
#define GAME_SCREEN_POWER_METER_DIST_FROM_RIGHT 14.0f

// Configurações do medidor de direção
#define GAME_SCREEN_DIRECTION_METER_DIST_FROM_BOTTOM 10.0f

// Cores dos quads sob as imagens dos botões
#define GAME_SCREEN_BT_COLOR_PRESSED Color( static_cast< uint8 >( 127 ), 127, 127, 255 )
#define GAME_SCREEN_BT_COLOR_UNPRESSED Color( static_cast< uint8 >( 255 ), 255, 255, 255 )

// Configurações das mensagens de resultado da jogada
#define GAME_SCREEN_FEEDBACK_MSG_GOAL		0
#define GAME_SCREEN_FEEDBACK_MSG_GAME_OVER	1
#define GAME_SCREEN_FEEDBACK_MSG_TOO_BAD	2
#define GAME_SCREEN_FEEDBACK_MSG_NEXT_LV	3

#define GAME_SCREEN_MSG_GOAL_WIDTH	252.0f
#define GAME_SCREEN_MSG_GOAL_HEIGHT	 97.0f

#define GAME_SCREEN_MSG_GAME_OVER_WIDTH		254.0f
#define GAME_SCREEN_MSG_GAME_OVER_HEIGHT	 49.0f

#define GAME_SCREEN_MSG_TOO_BAD_WIDTH	213.0f
#define GAME_SCREEN_MSG_TOO_BAD_HEIGHT	 59.0f

#define GAME_SCREEN_MSG_NEXT_CHALLENGE_WIDTH	256.0f
#define GAME_SCREEN_MSG_NEXT_CHALLENGE_HEIGHT	 32.0f

// Durações dos estados
#define GAME_SCREEN_FEEDBACK_DUR_GAME_OVER			7.000f
#define GAME_SCREEN_FEEDBACK_DUR_GOAL				2.000f
#define GAME_SCREEN_FEEDBACK_DUR_TOO_BAD			2.000f
#define GAME_SCREEN_FEEDBACK_DUR_NEXT_LV			0.500f

#define GAME_SCREEN_RESULT_SHOWN_DUR				3.000f

#define GAME_SCREEN_WAITING_TIME_BEFORE_FEEDBACK	2.413f

// duração da mensagem "game over"
#define GAME_SCREEN_GAMEOVER_TIME 2.0f

// Configurações do cronômetro
#define GAME_SCREEN_CHRONOMETER_OFFSET_X -7.0f

// Animações da bandeira de escanteio
#define GAME_SCREEN_FLAG_ANIM_STOPPED	0
#define GAME_SCREEN_FLAG_ANIM_ANIMATING 1
#define GAME_SCREEN_FLAGS_TIME_BETWEEN_ANIM	3.0f

// Controles do Replay
#define REPLAY_BTS_SIZE 32.0f
#define REPLAY_BTS_SPACE_BETWEEN 10.0f
#define REPLAY_BTS_DIST_FROM_BOTTOM 10.0f

// Máscara a ser utilizada no método GameScreen::checkButtonsCollision
#define INTERFACE_BT_MASK_KICK			0x01
#define INTERFACE_BT_MASK_PAUSE			0x02
#define INTERFACE_BT_MASK_INFO			0x04
#define INTERFACE_BT_MASK_FOCUS_ON_BALL	0x08
#define INTERFACE_BT_MASK_REPLAY        0x10
#define INTERFACE_BT_MASK_ALL			0x1F

/*==============================================================================================
 
 CONSTRUTOR
 
 ==============================================================================================*/

GameScreen::GameScreen( GameInfo& gameInfo, LoadingView* pLoadingView, bool continuing )
			:	Scene( SCENE_N_OBJECTS ),
				EventListener( EVENT_TOUCH ),
				AccelerometerListener( ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC ),
				OGLTransitionListener(),
				AnimatedCameraListener(),
				SpriteListener(),
				SetEffectGroupListener(),
				InterfaceControlListener(),
				ChronometerListener(),
				matchState( MATCH_STATE_UNDEFINED ),
				matchLastState( MATCH_STATE_UNDEFINED ),
				nActiveTouches( 0 ),
				initDistBetweenTouches( 0.0f ),
				stateTimeCounter( 0.0f ),
				stateDuration( 0.0f ),
				leftFlagTimeCounter( 0.0f ),
				rightFlagTimeCounter( 0.0f ),
				aimDraggingTouch( -1 ),
				ballDraggingTouch( -1 ),
				fieldDraggingTouch( -1 ),
				buttonPressingTouch( -1 ),
				movingCamera( false ),
				cameraMovement(),
				lastBallInitialPosition(),
				ballDestPos(),
				pBall( NULL ),
				pKeeper( NULL ),
				pPlayer( NULL ),
				pCrowd( NULL ),
				pBarrier( NULL ),
				pBallTarget( NULL ),
				pCurrTransition( NULL ),
				pSliceTransition( NULL ),
				pCurtainTransition( NULL ),
				pControlsCamera( NULL ),
				pInfoTab( NULL ),
				pPowerMeter( NULL ),
				pDirectionMeter( NULL ),
				pSetEffectControls( NULL ),
				pBtFocusOnBall( NULL ),
				pBtKick( NULL ),
				pBtPause( NULL ),
				pBtInfo( NULL ),
				replay( false ),	
				iReplayTabGroup(30),
				// OLD
				// settingRestPos( 0 ),
				realPosSortables(),
				gameInfo( gameInfo ),
				vibrated( false )
{
	// Inicializa os vetores da classe
#if DEBUG
	setName( "GameScreen" );
#endif

	memset( replayBts, NULL, sizeof( Button* ) * REPLAY_N_BUTTONS );

	////<DM>
	iReplaySpeedIndex = 2;
	iReplaySpeedVector[0] = 0.25f;
	for( uint8 c = 1 ; c < 5 ; ++c )
		iReplaySpeedVector[c] = iReplaySpeedVector[ c-1 ] * 2.0f;
	
	// Zera as variáveis de controles do jogo, se não estiver continuando.
	if( !continuing )
		gameInfo.onNewGame();	
	///</DM>
	
	if( !build( pLoadingView, continuing ) )
	{
#if DEBUG
		throw ConstructorException( "GameScreen::GameScreen: Unable to create object" );
#else
		throw ConstructorException();
#endif
	}
}

/*==============================================================================================
 
 DESTRUTOR
 
 ==============================================================================================*/

GameScreen::~GameScreen( void )
{
	int32 levelMaxPts = GameUtils::GetLevelScoreTarget( getDifficultyLevel( gameInfo.currLevel ) );
	
	//<DM>
	if(( gameInfo.gameMode == GAME_MODE_RANKING ) && ( matchState != MATCH_STATE_UNDEFINED ) && ( matchState != MATCH_STATE_GAME_OVER ) && ( matchState != MATCH_STATE_FEEDBACK_GAME_OVER ))
	{		
		if( gameInfo.levelScore >= levelMaxPts )
		{
			onLoadNewLevel();
			resetLevel();
		}
		else
		{
			if( gameInfo.currTry == FOUL_CHALLENGE_TRIES_PER_FOUL )
			{
				clean();
				return;
			}
		}
		[(( FreeKickAppDelegate* )APP_DELEGATE ) saveGameState];
	}		
	//</DM>			
	else ///DM: vamos ver se vai doer...rationale: nas condições em que haveria o salvamento, há exclusão mutua com o gravamento em placar...
	{
		// Garante que iremos salvar a pontuação do jogo mesmo em eventos de quit e abort
		if( ( gameInfo.gameMode == GAME_MODE_RANKING ) && ( matchState != MATCH_STATE_UNDEFINED ) && ( gameInfo.points > 0 )  )
		{
			[(( FreeKickAppDelegate* )APP_DELEGATE ) saveScoreIfRecord: gameInfo.points];		
			
			// Impede que por alguma razão salvemos a pontuação duas vezes
			gameInfo.points = 0;
			[(( FreeKickAppDelegate* )APP_DELEGATE ) removeStateFile];
		}
	}

	clean();
}

/*==============================================================================================
 
 MÉTODO clean
 Libera os recursos alocados pelo objeto.
 
 ==============================================================================================*/

void GameScreen::clean( void )
{
	// OBS: Ver comentário em render()
	DELETE( pControlsCamera );
	
	DELETE( pCurtainTransition );
	DELETE( pSliceTransition );
	
	DELETE( pSetEffectControls );
	
	DELETE( pPowerMeter );
	DELETE( pDirectionMeter );
	DELETE( pInfoTab );
	
	DELETE( pBtFocusOnBall );
	DELETE( pBtKick );
	DELETE( pBtPause );
	DELETE( pBtInfo );
	
	DELETE( pChronometer );
	
	UnloadReplayButtons();
	
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
		DELETE( feedbackMsgs[i] );
	
	DELETE(  pBarrier );
}

/*==============================================================================================
 
 MÉTODO build
 Inicializa o objeto.
 
 ==============================================================================================*/

bool GameScreen::build( LoadingView* hLoadindView, bool continuing )
{
	{ // Evita erros com os gotos
		
#if DEBUG
		LOG_FREE_MEM( "Inicio de GameScreen::build" );
#endif
		
		// Cria os controles que devem ser renderizados sobre os elementos do jogo e não devem ser
		// transformados pelo zoom da câmera
		if( !createControls( hLoadindView ) )
			goto Error;
		
#if DEBUG
		LOG_FREE_MEM( "Carregou os controles" );
#endif
		
		// Cria os objetos que irão sofrer as transformações da câmera
		AnimatedCamera* pCamera = new AnimatedCamera( this, CAMERA_TYPE_AIR, -100.0f, 100.0f );
		if( !pCamera )
			goto Error;
		
		setCurrentCamera( pCamera );
		pCamera->setLandscapeMode( true );
		
		TextureFrame fieldImgFrame( 0.0f, 0.0f, 92.0f, 51.0f, 0.0f, 0.0f );
#if CONFIG_TEXTURES_16_BIT
		TexturePattern* pField = new TexturePattern( "field00", &fieldImgFrame, ResourceManager::GetTexture2D );
		
		// OLD: Gradientes de verde ficam com o aspecto muito ruim em 16 bits. Por isso utilizamos uma imagem de 32 bits para o campo
		//TexturePattern* pField = new TexturePattern( "field00", &fieldImgFrame, ResourceManager::Get16BitTexture2D );
#else
		TexturePattern* pField = new TexturePattern( "field00", &fieldImgFrame );
#endif
		if( !pField || !insertObject( pField ) )
		{
			delete pField;
			goto Error;
		}
		pField->setSize( SCREEN_WIDTH * 14.0f, SCREEN_HEIGHT * 13.0f );
		pField->setPosition( ( ( SCREEN_WIDTH - pField->getWidth() ) * 0.5f ) - ( pField->getWidth() * 0.70f ), ( ( SCREEN_HEIGHT - pField->getHeight() ) * 0.5f ) - ( pField->getHeight() * 0.3f ) );
		
#if DEBUG
		pField->setName( "Campo" );
#endif
		
		ParkingLot* pParkingLot = new ParkingLot();
		if( !pParkingLot || !insertObject( pParkingLot ) )
		{
			delete pParkingLot;
			goto Error;
		}
		pParkingLot->setPosition( CAMERA_LEFT_LIMIT, CAMERA_TOP_LIMIT );
		
#if DEBUG
		pParkingLot->setName( "Estacionamento" );
#endif
		
		FieldMarks* pFieldMarks = new FieldMarks();
		if( !pFieldMarks || !insertObject( pFieldMarks ) )
		{
			delete pFieldMarks;
			goto Error;
		}
		
#if DEBUG
		pFieldMarks->setName( "Linhas do Campo" );
		LOG_FREE_MEM( "Antes de carregar Crowd" );
#endif
		
		pCrowd = new Crowd();
		if( !pCrowd || !insertObject( pCrowd ) )
		{
			DELETE( pCrowd );
			goto Error;
		}
		pCrowd->setRealPosition( -5.0f, 0.0f, 5.0f );
		
#if DEBUG
		pCrowd->setName( "Torcida" );
		LOG_FREE_MEM( "Apos carregar Crowd" );
#endif
		
		[hLoadindView changeProgress: 0.0021f];
		
		// Cria a sombra da bandeira do corner esquerdo
#if CONFIG_TEXTURES_16_BIT
		Sprite* pLeftFlagShadow = new Sprite( "fls", "fls", ResourceManager::Get16BitTexture2D );
#else
		Sprite* pLeftFlagShadow = new Sprite( "fls", "fls" );
#endif
		if( !pLeftFlagShadow || !insertObject( pLeftFlagShadow ) )
		{
			delete pLeftFlagShadow;
			goto Error;
		}
		Point3f flagPos = GameUtils::isoGlobalToPixels( -REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f );
		pLeftFlagShadow->setAnchor( CornerFlag::GetCornerFlagAnchorX(), CornerFlag::GetCornerFlagAnchorY(), 0.0f );
		pLeftFlagShadow->setPositionByAnchor( flagPos.x + ISO_CORNER_LEFT_FLAG_OFFSET_X, flagPos.y + ISO_CORNER_LEFT_FLAG_OFFSET_Y, 0.0f );
		pLeftFlagShadow->setListener( this );
		
#if DEBUG
		pLeftFlagShadow->setName( "Sombra da bandeira esquerda" );
		LOG_FREE_MEM( "Carregou left flag shadow" );
#endif
		
		// Cria a sombra da bandeira do corner direito
		Sprite *pRightFlagShadow = new Sprite( pLeftFlagShadow );
		if( !pRightFlagShadow || !insertObject( pRightFlagShadow ) )
		{
			delete pRightFlagShadow;
			goto Error;
		}
		flagPos = GameUtils::isoGlobalToPixels( REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f );
		pRightFlagShadow->setAnchor( CornerFlag::GetCornerFlagAnchorX(), CornerFlag::GetCornerFlagAnchorY(), 0.0f );
		pRightFlagShadow->setPositionByAnchor( flagPos.x + ISO_CORNER_RIGHT_FLAG_OFFSET_X, flagPos.y + ISO_CORNER_RIGHT_FLAG_OFFSET_Y, 0.0f );
		pRightFlagShadow->setListener( this );
		
#if DEBUG
		pRightFlagShadow->setName( "Sombra da bandeira da direita" );
		LOG_FREE_MEM( "Carregou right flag shadow" );
#endif
		
		[hLoadindView changeProgress: 0.0135f];
		
		// Cria a metade do gol que será sempre renderizada antes da bola
		Goal* pGoalBottom = new Goal();
		if( !pGoalBottom || !insertObject( pGoalBottom ) )
		{
			delete pGoalBottom;
			goto Error;
		}
		
		[hLoadindView changeProgress: 0.0068f];
		
#if DEBUG
		pGoalBottom->setName( "Gol - parte inferior" );
		LOG_FREE_MEM( "Carregou goal bottom" );
#endif
		
		// Cria a bola
		pBall = new IsoBall();
		if( !pBall || !insertObject( pBall ) )
		{
			delete pBall;
			goto Error;
		}
		
		[hLoadindView changeProgress: 0.0143f];
		
#if DEBUG
		pBall->setName( "Bola" );
		LOG_FREE_MEM( "Carregou a bola" );
#endif
		
		// Cria o jogador
		pPlayer = new IsoPlayer();
		if( !pPlayer || !insertObject( pPlayer ) )
		{
			delete pPlayer;
			goto Error;
		}
		
		[hLoadindView changeProgress: 0.1216f];
		
#if DEBUG
		pPlayer->setName( "Jogador" );
		LOG_FREE_MEM( "Carregou player" );
#endif
		
		// Cria a metade do gol que será sempre renderizada após a bola
		Goal* pGoalTop = new Goal( pGoalBottom );
		if( !pGoalTop || !insertObject( pGoalTop ) )
		{
			delete pGoalTop;
			goto Error;
		}
		
		// Posiciona o gol
		pGoalBottom->setRealPosition( -( REAL_GOAL_WIDTH * 0.5f ), 0.0f, 0.0f );
		pGoalTop->setRealPosition( ( REAL_GOAL_WIDTH * 0.5f ), 0.0f, 0.0f );
		
		[hLoadindView changeProgress: 0.0068f];
		
#if DEBUG
		pGoalTop->setName( "Gol - parte superior" );
		LOG_FREE_MEM( "Carregou goal top" );
#endif
		
		// Cria o goleiro
		pKeeper = new IsoKeeper();
		if( !pKeeper || !insertObject( pKeeper ) )
		{
			delete pKeeper;
			goto Error;
		}
		
#if DEBUG
		pKeeper->setName( "Goleiro" );
		LOG_FREE_MEM( "Carregou keeper" );
#endif
		
		[hLoadindView changeProgress: 0.1233f];
		
		// Cria o alvo que a bola deverá acertar para que o jogador passe de fase
		pBallTarget = new BallTarget();
		if( !pBallTarget || !insertObject( pBallTarget ) )
		{
			delete pBallTarget;
			goto Error;
		}
		
		[hLoadindView changeProgress: 0.0017f];
		
#if DEBUG
		pBallTarget->setName( "Alvo" );
		LOG_FREE_MEM( "Carregou ball target" );
#endif
		
		// Cria a bandeira do corner esquerdo
		CornerFlag* pLeftFlag = new CornerFlag( ISO_CORNER_LEFT_FLAG_OFFSET_X, ISO_CORNER_LEFT_FLAG_OFFSET_Y );
		if( !pLeftFlag || !insertObject( pLeftFlag ) )
		{
			delete pLeftFlag;
			goto Error;
		}
		pLeftFlag->setRealPosition( -REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f );
		pLeftFlag->setListener( this );
		
#if DEBUG
		pLeftFlag->setName( "Bandeirinha da esquerda" );
		LOG_FREE_MEM( "Carregou left flag" );
#endif
		
		// Cria a bandeira do corner direito
		CornerFlag *pRightFlag = new CornerFlag( ISO_CORNER_RIGHT_FLAG_OFFSET_X, ISO_CORNER_RIGHT_FLAG_OFFSET_Y, pLeftFlag );
		if( !pRightFlag || !insertObject( pRightFlag ) )
		{
			delete pRightFlag;
			goto Error;
		}
		pRightFlag->setRealPosition( REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f );
		pRightFlag->setListener( this );
		
		[hLoadindView changeProgress: 0.0270f];
		
#if DEBUG
		pRightFlag->setName( "Bandeirinha da direita" );
		LOG_FREE_MEM( "Carregou right flag" );
#endif
		
		// Cria a barreira
		if( !createBarrier( hLoadindView ) )
			goto Error;
		
#if DEBUG
		LOG_FREE_MEM( "Carregar tudo" );
#endif
		
		// Determina quais objetos da cena deverão ser constantemente ordenados de acordo com a sua ordem Z
		for( uint8 i = FIRST_REAL_POS_OWNER ; i <= LAST_REAL_POS_OWNER ; ++i )
		{
			RealPosOwner* pAux = dynamic_cast< RealPosOwner* >( getObject( i ) );
			if( pAux )
				realPosSortables.push_back( pAux );
		}
		
		
		// OBS: Não queremos que o jogador ache que ainda falta muito...
		[hLoadindView setProgress: 0.9f];
		
		if (!createReplayButtons())
			goto Error;
		
		// Coloca a partida em seu estado inicial
		if (!continuing)
			setState( MATCH_STATE_NEXT_LEVEL );
		else
			setState( MATCH_STATE_CONTINUING);
		onOGLTransitionEnd();
		
		// OLD
		// OBS: Não queremos que o jogador ache que ainda falta muito...
		//[hLoadindView changeProgress: 0.331f];
		
		return true;
		
	} // Evita erros com os gotos
	
Error:
	clean();
	return false;
}

/*===========================================================================================
 
 MÉTODO createControls
 Cria os controles que devem ser renderizados sobre os elementos do jogo e não devem ser
 transformados pelo zoom da câmera.
 
 ============================================================================================*/

bool GameScreen::createControls( LoadingView* hLoadindView )
{
	// Câmera que irá renderizar os controles
	pControlsCamera = new OrthoCamera();
	if( !pControlsCamera )
		return false;
	
	pControlsCamera->setLandscapeMode( true );
	
	// Transição de tela (levanta cortina preta)
	pCurtainTransition = new CurtainTransition( TRANSITION1_DURATION, this );
	if( !pCurtainTransition )
		return false;
	
	// Transição de tela (fecha cortina preta)
	pSliceTransition = new SliceTransition( TRANSITION1_NSLICES, TRANSITION1_DURATION, this );
	if( !pSliceTransition )
		return false;
	
	// Barra de força
	pPowerMeter = new PowerMeter( ISO_POWER_MIN_VALUE, ISO_POWER_MAX_VALUE, (( ISO_POWER_MAX_VALUE - ISO_POWER_MIN_VALUE ) * 0.5f ) + ISO_POWER_MIN_VALUE, this );
	if( !pPowerMeter )
		return false;
	
	[hLoadindView changeProgress: 0.0042f];
	
	// Controle de direção do chute
	pDirectionMeter = new DirectionMeter( ISO_DIRECTION_MIN_VALUE, ISO_DIRECTION_MAX_VALUE, (( ISO_DIRECTION_MAX_VALUE - ISO_DIRECTION_MIN_VALUE ) * 0.5f ) + ISO_DIRECTION_MIN_VALUE, this );
	if( !pDirectionMeter )
		return false;
	
	[hLoadindView changeProgress: 0.0042f];
	
	// Barra de direção
	pInfoTab = new InfoTab( &gameInfo, this );
	if( !pInfoTab )
		return false;
	pInfoTab->setPosition( SCREEN_WIDTH, 0.0f );
	
	[hLoadindView changeProgress: 0.0116f];
	
	// Controles para determinar o efeito aplicado à bola
	pSetEffectControls = new SetEffectGroup( this, this );
	if( !pSetEffectControls )
		return false;
	
	[hLoadindView changeProgress: 0.0055f];	
	
	// Botão de "Focar a Bola"
	pBtFocusOnBall = new Button( "ctf", this );
	if( !pBtFocusOnBall )
		return false;
	
	TextureFrame frame( 0, 0, GAME_SCREEN_BT_FOCUS_WIDTH, GAME_SCREEN_BT_FOCUS_HEIGHT, 0, 0 );
	pBtFocusOnBall->setImageFrame( frame );
	pBtFocusOnBall->setPosition( GAME_SCREEN_BT_FOCUS_DIST_FROM_RIGHT, GAME_SCREEN_BT_FOCUS_DIST_FROM_TOP );
	
	[hLoadindView changeProgress: 0.0004f];
	
	// Botão de "Chutar a Bola"
	pBtKick = new Button( "ctk", this );
	if( !pBtKick )
		return false;
	
	frame.set( 0, 0, GAME_SCREEN_BT_KICK_WIDTH, GAME_SCREEN_BT_KICK_HEIGHT, 0, 0 );
	pBtKick->setImageFrame( frame );
	pBtKick->setPosition( GAME_SCREEN_BT_KICK_DIST_FROM_LEFT, SCREEN_HEIGHT - pBtKick->getHeight() - GAME_SCREEN_BT_KICK_DIST_FROM_BOTTOM );
	
	[hLoadindView changeProgress: 0.0004f];
	
	// Botão de "Pausar o Jogo"
	pBtPause = new Button( "ctp", this );
	if( !pBtPause )
		return false;
	
	[hLoadindView changeProgress: 0.0004f];
	
	frame.set( 0, 0, GAME_SCREEN_BT_PAUSE_WIDTH, GAME_SCREEN_BT_PAUSE_HEIGHT, 0, 0 );
	pBtPause->setImageFrame( frame );
	pBtPause->setPosition( SCREEN_WIDTH - pBtPause->getWidth() - GAME_SCREEN_BT_PAUSE_DIST_FROM_RIGHT, SCREEN_HEIGHT - pBtPause->getHeight() - GAME_SCREEN_BT_PAUSE_DIST_FROM_BOTTOM );
	
	// Botão de "Mostar Informações da fase"
	pBtInfo = new Button( "cti", this );
	if( !pBtInfo )
		return false;
	
	frame.set( 0, 0, GAME_SCREEN_BT_INFO_WIDTH, GAME_SCREEN_BT_INFO_HEIGHT, 0, 0 );
	pBtInfo->setImageFrame( frame );
	pBtInfo->setPosition( SCREEN_WIDTH - pBtInfo->getWidth() - GAME_SCREEN_BT_INFO_DIST_FROM_RIGHT, GAME_SCREEN_BT_INFO_DIST_FROM_TOP );
	
	[hLoadindView changeProgress: 0.0004f];
	
	// Posiciona a barra de força
	pPowerMeter->setPosition( GAME_SCREEN_POWER_METER_DIST_FROM_RIGHT, GAME_SCREEN_POWER_METER_DIST_FROM_TOP );
	pPowerMeter->configAnim( GAME_SCREEN_POWER_METER_DIST_FROM_RIGHT );
	
	// Posiciona a barra de direção
	pDirectionMeter->setPosition( ( SCREEN_WIDTH - pDirectionMeter->getWidth() ) * 0.5f, SCREEN_HEIGHT - pDirectionMeter->getHeight() - GAME_SCREEN_DIRECTION_METER_DIST_FROM_BOTTOM );
	pDirectionMeter->configAnim( pDirectionMeter->getPosition()->y );
	
	// Cria as mensagens de feedback
	char msgPath[] = { 'm', 's', 'g', ' ', ' ', '\0' };
	char msgPathSuffix[ GAME_SCREEN_N_FFEDBACK_MSGS ][2] = { { 'g', 'l' }, { 'g', 'o' }, { 't', 'b' }, { 'n', 'c' } };
	
	float msgsFrames[ GAME_SCREEN_N_FFEDBACK_MSGS ][2] = { { 252.0f, 97.0f }, { 254.0f, 49.0f }, { 213.0f, 59.0f }, { 256.0f, 32.0f } };
	FeedbackMsgAnimType msgsAnims[ GAME_SCREEN_N_FFEDBACK_MSGS ] = { FEEDBACK_MSG_ANIM_VIEWPORT, FEEDBACK_MSG_ANIM_FADE, FEEDBACK_MSG_ANIM_BOUNCE, FEEDBACK_MSG_ANIM_NONE };
	
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
	{
		msgPath[3] = msgPathSuffix[i][0];
		msgPath[4] = msgPathSuffix[i][1];
		
		frame.width = msgsFrames[i][0];
		frame.height = msgsFrames[i][1];
		
#if CONFIG_TEXTURES_16_BIT
		feedbackMsgs[i] = new FeedbackMsg( msgPath, &frame, this, ResourceManager::Get16BitTexture2D );
#else
		feedbackMsgs[i] = new FeedbackMsg( msgPath, &frame, this );
#endif
		
		if( !feedbackMsgs[i] )
			return false;
		
		feedbackMsgs[i]->setPosition( ( SCREEN_WIDTH - feedbackMsgs[i]->getWidth() ) * 0.5f, ( SCREEN_HEIGHT - feedbackMsgs[i]->getHeight() ) * 0.5f, 0.0f );
		feedbackMsgs[i]->setCurrAnimType( msgsAnims[i] );
	}
	
	[hLoadindView changeProgress: 0.0076f];
	
	pChronometer = new Chronometer( this, this );
	if( !pChronometer )
		return false;
	
	pChronometer->setPosition( ( ( SCREEN_WIDTH - pChronometer->getWidth() ) * 0.5f ) + GAME_SCREEN_CHRONOMETER_OFFSET_X, 0.0f );
	
	[hLoadindView changeProgress: 0.0012f];

	return true;
}

/*===============================================================================
 
 MÉTODO UnloadReplayButtons
Descarrega os assets 
 
 ================================================================================*/

void GameScreen::UnloadReplayButtons( void )
{
	/// limpa as imagens do replay. (o resto é feito pelo objectgroup)
	DELETE( iReplaySign);	
	DELETE( replayBts[REPLAY_BT_STOP] );
	DELETE( replayBts[REPLAY_BT_CAMTOGGLE] );	
}

/*===============================================================================
 
 MÉTODO createReplayButtons
 Cria os botões utilizados para controlar a tela de replay.
 
 ================================================================================*/

bool GameScreen::createReplayButtons( void )
{
	{	
	
#if CONFIG_TEXTURES_16_BIT
	iReplayTabBG = new RenderableImage("rquadro_big",  RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
#else
	iReplayTabBG = new RenderableImage("rquadro_big");
#endif
	
	if (!iReplayTabBG) goto Error;	
	iReplayTabBG->setPosition(324,0);				
	iReplayTabGroup.insertObject(iReplayTabBG);
	
#if CONFIG_TEXTURES_16_BIT	
	iReplayTempoImage=new RenderableImage("tempo",  RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
#else
	iReplayTempoImage=new RenderableImage("tempo");
#endif
	
	if (!iReplayTempoImage) goto Error;	
	
	iReplayTempoImage->setPosition(366,13);
	iReplayTabGroup.insertObject(iReplayTempoImage);

#if CONFIG_TEXTURES_16_BIT
	iReplaySpeedPaneBg=new RenderableImage("quadro_1",  RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
#else
	iReplaySpeedPaneBg=new RenderableImage("quadro_1");
#endif
	
	if (!iReplaySpeedPaneBg) goto Error;	
	
	iReplaySpeedPaneBg->setPosition(349,86);
	iReplayTabGroup.insertObject(iReplaySpeedPaneBg);

#if CONFIG_TEXTURES_16_BIT
	iReplayTabButtonsBG=new RenderableImage("quadro_2",  RenderableImage::CreateDefaultVertexSet,ResourceManager::Get16BitTexture2D);
#else
	iReplayTabButtonsBG=new RenderableImage("quadro_2");	
#endif
	
	if (!iReplayTabButtonsBG) goto Error;	
	
	iReplayTabButtonsBG->setPosition(344,28);
	iReplayTabGroup.insertObject(iReplayTabButtonsBG);	
	
	////////////////////////////////////////////
		
	const char* aux[] = { "back",  "menos", "pause", "mais","ctf","apito_back", "play","hide","showtab"};
	Point3f coords[]={
		Point3f(338,100),
		Point3f(337,40),
		Point3f(427,95),
		Point3f(430,36),
		Point3f(18,254),
		Point3f(395,244),
		Point3f(379,96),
		Point3f(375,165),
		Point3f(480-REPLAY_BTS_SPACE_BETWEEN-80,REPLAY_BTS_SPACE_BETWEEN)		
	};
	
	for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
	{
		replayBts[i] = new Button( aux[i] ,this); 
		
		if( !replayBts[i] )
			return false;
		
		replayBts[i]->setActive( false );
		replayBts[i]->setVisible( false );
		replayBts[i]->setPosition( &coords[i] );
		
		if (i!=REPLAY_BT_STOP && i!=REPLAY_BT_CAMTOGGLE && i!=REPLAY_BT_SHOW)
			iReplayTabGroup.insertObject(replayBts[i]);
	}
	
	//////////////////////////////////////	
	
	/////////////////////////////
	// DM: aqui o carregamento de figuras em 16 bits não funciona. A transparência fica cheia de lixo.

	iReplaySign=new RenderableImage("replaySign");	
		
	if (!iReplaySign) goto Error;	
		
	iReplaySign->setPosition(REPLAY_BTS_SPACE_BETWEEN,REPLAY_BTS_SPACE_BETWEEN);
	iReplaySign->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		
	iReplaySignFadeTimeout=0.0f;
		
		{
			Color color;
			iReplaySign->getVertexSetColor(&color);
			color.setFloatA( 0.0f );
			iReplaySign->setVertexSetColor(color);
		}	
		
	iReplaySpeedImages[0]=new RenderableImage("14");	

	if (!iReplaySpeedImages[0]) goto Error;	
	
	iReplaySpeedImages[0]->setPosition(387,46);
	iReplaySpeedImages[0]->setVisible(false);
	iReplayTabGroup.insertObject(iReplaySpeedImages[0]);


	iReplaySpeedImages[1]=new RenderableImage("12");	

	if (!iReplaySpeedImages[1]) goto Error;	
	
	iReplaySpeedImages[1]->setPosition(387,46);	
	iReplaySpeedImages[1]->setVisible(false);
	iReplayTabGroup.insertObject(iReplaySpeedImages[1]);	


	iReplaySpeedImages[2]=new RenderableImage("1x");		
	
	if (!iReplaySpeedImages[2]) goto Error;	
	
	iReplaySpeedImages[2]->setPosition(387,46);	
	iReplaySpeedImages[2]->setVisible(true);	
	iReplayTabGroup.insertObject(iReplaySpeedImages[2]);	


	iReplaySpeedImages[3]=new RenderableImage("2x");
	
	if (!iReplaySpeedImages[3]) goto Error;	
	
	iReplaySpeedImages[3]->setPosition(387,46);	
	iReplaySpeedImages[3]->setVisible(false);	
	iReplayTabGroup.insertObject(iReplaySpeedImages[3]);	


	iReplaySpeedImages[4]=new RenderableImage("4x");	
	
	if (!iReplaySpeedImages[4]) goto Error;	
	
	iReplaySpeedImages[4]->setPosition(387,46);	
	iReplaySpeedImages[4]->setVisible(false);	
	iReplayTabGroup.insertObject(iReplaySpeedImages[4]);	
	///////////////////////////////////////////
		
	iReplayTabGroup.setPosition(480,0);		
	iReplayTabGroup.setVisible(false);
	
	return true;
	}
Error:
	return false;
	
}

/*===========================================================================================
 
 MÉTODO createBarrier
 Cria a barreira de jogadores.
 
 ============================================================================================*/

bool GameScreen::createBarrier( LoadingView* hLoadindView )
{
	// Cria o gerenciador dos jogadores da barreira
	pBarrier = new Barrier( this );
	if( !pBarrier )
		return false;
	
	// Cria os jogadores que podem ser utilizados na barreira
	FearsomeBarrierPlayer* pFearsome = new FearsomeBarrierPlayer();
	if( !pFearsome || !insertObject( pFearsome ) )
	{
		delete pFearsome;
		return false;
	}
	
#if DEBUG && ( IS_CURR_TEST( TEST_BARRIER_SHADOWS ) || IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
	pBarrier->registerPlayer( GAME_MODE_TRAINING, OBJ_INDEX_BARRIER_FEARSOME );
#else
	pBarrier->registerPlayer( GAME_MODE_RANKING, OBJ_INDEX_BARRIER_FEARSOME );
#endif
	
	[hLoadindView changeProgress: 0.0405f];
	
#if DEBUG
	LOG_FREE_MEM( "Carregou jogador de barreira - Fearsome" );
#endif
	
	BlondBarrierPlayer* pBlond = new BlondBarrierPlayer();
	if( !pBlond || !insertObject( pBlond ) )
	{
		delete pBlond;
		return false;
	}
#if DEBUG && ( IS_CURR_TEST( TEST_BARRIER_SHADOWS ) || IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
	pBarrier->registerPlayer( GAME_MODE_TRAINING, OBJ_INDEX_BARRIER_BLOND );
#else
	pBarrier->registerPlayer( GAME_MODE_RANKING, OBJ_INDEX_BARRIER_BLOND );
#endif
	
	[hLoadindView changeProgress: 0.0811f];
	
#if DEBUG
	LOG_FREE_MEM( "Carregou jogador de barreira - Loiro" );
#endif
	
	JapaneseBarrierPlayer* pJapanese = new JapaneseBarrierPlayer();
	if( !pJapanese || !insertObject( pJapanese ) )
	{
		delete pJapanese;
		return false;
	}
#if DEBUG && ( IS_CURR_TEST( TEST_BARRIER_SHADOWS ) || IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
	pBarrier->registerPlayer( GAME_MODE_TRAINING, OBJ_INDEX_BARRIER_JAPANESE );
#else
	pBarrier->registerPlayer( GAME_MODE_RANKING, OBJ_INDEX_BARRIER_JAPANESE );
#endif
	
	[hLoadindView changeProgress: 0.0405f];
	
#if DEBUG
	LOG_FREE_MEM( "Carregou jogador de barreira - Japonês" );
#endif
	
	SleepyBarrierPlayer* pSleepy = new SleepyBarrierPlayer();
	if( !pSleepy || !insertObject( pSleepy ) )
	{
		delete pSleepy;
		return false;
	}
#if DEBUG && ( IS_CURR_TEST( TEST_BARRIER_SHADOWS ) || IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
	pBarrier->registerPlayer( GAME_MODE_TRAINING, OBJ_INDEX_BARRIER_SLEEPY );
#else
	pBarrier->registerPlayer( GAME_MODE_RANKING, OBJ_INDEX_BARRIER_SLEEPY );
#endif
	
	[hLoadindView changeProgress: 0.0304f];
	
#if DEBUG
	LOG_FREE_MEM( "Carregou jogador de barreira - Dorminhoco" );
#endif
	
	WoodBarrierPlayer* pWood = NULL;
	for( uint8 i = 0 ; i < BARRIER_MAX_N_PLAYERS ; ++i )
	{
		pWood = new WoodBarrierPlayer( pWood );
		if( !pWood || !insertObject( pWood ) )
		{
			delete pWood;
			return false;
		}
#if DEBUG && ( IS_CURR_TEST( TEST_BARRIER_SHADOWS ) || IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
		pBarrier->registerPlayer( GAME_MODE_RANKING, OBJ_INDEX_BARRIER_WOOD_FIRST + i );
#else
		pBarrier->registerPlayer( GAME_MODE_TRAINING, OBJ_INDEX_BARRIER_WOOD_FIRST + i );
#endif
	}
	
	[hLoadindView changeProgress: 0.054f];
	
#if DEBUG
	LOG_FREE_MEM( "Carregou jogador de barreira - Madeira" );
#endif
	
	return true;
}

/*===========================================================================================
 
MÉTODO resume
	Indica que a aplicação irá resumir sua execução. Quando a aplicação chama este método da
cena, significa que a cena voltará ser atualizada através de eventos de update.
 
=============================================================================================*/

void GameScreen::resume( void )
{
	unpause();
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.
 
=============================================================================================*/

bool GameScreen::render( void )
{
	if( Scene::render() )
	{
#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
		// Renderiza os pontos de controle da curva da bola
		Point3f bezierOrigin, bezierControl1, bezierControl2, bezierDest;
		pBall->movFunc.getOrigin( &bezierOrigin );
		pBall->movFunc.getControl1( &bezierControl1 );
		pBall->movFunc.getControl2( &bezierControl2 );
		pBall->movFunc.getDestination( &bezierDest );
		
		glColor4ub( 255, 0, 0, 255 );
		glPointSize( 10.0f );
		glEnableClientState( GL_VERTEX_ARRAY );
		
		glMatrixMode( GL_MODELVIEW );
		
		glPushMatrix();
		Point3f aux = GameUtils::isoGlobalToPixels( bezierOrigin.x, bezierOrigin.y, bezierOrigin.z );
		glVertexPointer( 3, GL_FLOAT, 0, &aux );
		glDrawArrays( GL_POINTS, 0, 1 );
		glPopMatrix();
		
		glPushMatrix();
		aux = GameUtils::isoGlobalToPixels( bezierControl1.x, bezierControl1.y, bezierControl1.z );
		glVertexPointer( 3, GL_FLOAT, 0, &aux );
		glDrawArrays( GL_POINTS, 0, 1 );
		glPopMatrix();
		
		glPushMatrix();
		aux = GameUtils::isoGlobalToPixels( bezierControl2.x, bezierControl2.y, bezierControl2.z );
		glVertexPointer( 3, GL_FLOAT, 0, &aux );
		glDrawArrays( GL_POINTS, 0, 1 );
		glPopMatrix();
		
		glPushMatrix();
		aux = GameUtils::isoGlobalToPixels( bezierDest.x, bezierDest.y, bezierDest.z );
		glVertexPointer( 3, GL_FLOAT, 0, &aux );
		glDrawArrays( GL_POINTS, 0, 1 );
		glPopMatrix();
		
		glPointSize( 1.0f );
		
		// Renderiza a trajetória da bola
		glColor4ub( 0, 0, 255, 127 );
		glLineWidth( 10.0f );
		
		glPushMatrix();
		glVertexPointer( 3, GL_FLOAT, 0, &( pBall->ballPath[0] ) );
		glDrawArrays( GL_LINE_STRIP, 0, pBall->ballPath.size() );
		glPopMatrix();
		
		
		// Desenha os quads de colisão do gol
		glColor4ub( 255, 0, 255, 255 );
		glLineWidth( 2.0f );
		
		Point3f left[4] = {
			GameUtils::isoGlobalToPixels( REAL_LEFT_POST_END, 0.0f, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_LEFT_POST_START, 0.0f, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_LEFT_POST_START, REAL_FLOOR_TO_BAR, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_LEFT_POST_END, REAL_FLOOR_TO_BAR, 0.0f )
		};
		
		
		glVertexPointer( 3, GL_FLOAT, 0, left );
		glDrawArrays( GL_LINE_LOOP, 0, 4 );
		
		Point3f right[4] = {
			GameUtils::isoGlobalToPixels( REAL_RIGHT_POST_END, 0.0f, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_RIGHT_POST_START, 0.0f, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_RIGHT_POST_START, REAL_FLOOR_TO_BAR, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_RIGHT_POST_END, REAL_FLOOR_TO_BAR, 0.0f )
		};
		
		
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINE_LOOP, 0, 4 );
		
		Point3f top[4] = {
			GameUtils::isoGlobalToPixels( REAL_LEFT_POST_END, REAL_BAR_BOTTOM, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_RIGHT_POST_END, REAL_BAR_BOTTOM, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_RIGHT_POST_END, REAL_BAR_TOP, 0.0f ),
			GameUtils::isoGlobalToPixels( REAL_LEFT_POST_END, REAL_BAR_TOP, 0.0f )
		};
		
		
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINE_LOOP, 0, 4 );
		
		
		glLineWidth( 1.0f );
		glColor4ub( 255, 255, 255, 255 );
		
#endif
		
		return renderInterfaceControls();
	}
	
	return false;
}

/*===========================================================================================
 
 MÉTODO hideInterfaceControls
 Esconde todos os controles.
 
 ============================================================================================*/

void GameScreen::hideInterfaceControls( void )
{
	pBtInfo->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	pBtPause->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	pBtKick->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	pBtFocusOnBall->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	pInfoTab->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	
	pPowerMeter->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	pDirectionMeter->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	
	pInfoTab->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	pChronometer->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
	
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
		feedbackMsgs[i]->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );	
	
	pSetEffectControls->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
}

/*===========================================================================================
 
 MÉTODO renderInterfaceControls
 Renderiza os controles da interface.
 
 ============================================================================================*/

bool GameScreen::renderInterfaceControls( void )
{
	bool ret = false;
	
	// OBS: É ÓBVIO que isso ficaria melhor sendo uma outra Scene cuja câmera fosse pControlsCamera. No entanto,
	// a ordem é "dane-se, faça logo". Além disso, como nem tudo é tão fácil quanto parece, resolvi deixar assim
	pControlsCamera->place();
	
	if (
		matchState == MATCH_STATE_SHOWING_REPLAY ||
		matchState == MATCH_STATE_REPLAY_SHOWN ||
		matchState == MATCH_STATE_HIDING_REPLAY ||			 
		matchState == MATCH_STATE_PLAYER_RUNNING_REPLAY ||
		matchState == MATCH_STATE_PLAYER_KICKING_REPLAY ||
		matchState == MATCH_STATE_ANIMATING_REPLAY 
		)	
		
	{
		iReplaySign->render();				
		replayBts[REPLAY_BT_STOP]->render();
		replayBts[REPLAY_BT_CAMTOGGLE]->render();
		
		if (iReplayTabGroup.isVisible())		
			iReplayTabGroup.render();		
		else
		{
			replayBts[REPLAY_BT_SHOW]->render();
		}
	}
	
	ret |= pSetEffectControls->render();
	
	ret |= pBtInfo->render();
	ret |= pBtPause->render();
	ret |= pBtKick->render();
	ret |= pBtFocusOnBall->render();
	
	ret |= pPowerMeter->render();
	ret |= pDirectionMeter->render();
	
	ret |= pInfoTab->render();
	
	ret |= pChronometer->render();
	
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
	{
		if( i != GAME_SCREEN_FEEDBACK_MSG_GAME_OVER )
			ret |= feedbackMsgs[i]->render();
	}
	
	
	ret |= pCurrTransition->render();
	
	feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_GAME_OVER ]->render();
	
	return ret;
}

/*===========================================================================================
 
 MÉTODO updateInterfaceControls
 Atualiza os controles da interface.
 
 ============================================================================================*/

bool GameScreen::updateInterfaceControls( float timeElapsed )
{
	bool ret = false;
	
	// OBS: É ÓBVIO que isso ficaria melhor sendo uma outra Scene cuja câmera fosse pControlsCamera. No entanto,
	// a ordem é "dane-se, faça logo". Além disso, como nem tudo é tão fácil quanto parece, resolvi deixar assim
	ret |= pBtInfo->update( timeElapsed );
	ret |= pBtPause->update( timeElapsed );
	ret |= pBtKick->update( timeElapsed );
	ret |= pBtFocusOnBall->update( timeElapsed );
	
	ret |= pPowerMeter->update( timeElapsed );
	ret |= pDirectionMeter->update( timeElapsed );
	
	ret |= pInfoTab->update( timeElapsed );
	ret |= pChronometer->update( timeElapsed );
	ret |= pSetEffectControls->update( timeElapsed );
	
	ret |= pCurrTransition->update( timeElapsed );
	
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
		ret |= feedbackMsgs[i]->update( timeElapsed );

	for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
		ret |= replayBts[i]->update( timeElapsed );
	
	return ret;
}

/*===========================================================================================
 
 MÉTODO update
 Atualiza o objeto.
 
 ============================================================================================*/

bool GameScreen::update( float timeElapsed )
{
	if( !isActive() )
		return false;
	
	if (matchState == MATCH_STATE_REPLAY_SHOWN || matchState== MATCH_STATE_ANIMATING_REPLAY)
	{
		///efeito ciclico de fade in/fade out
		Color color;
		iReplaySign->getVertexSetColor(&color);
		iReplaySignFadeTimeout+=2*timeElapsed;
		if (iReplaySignFadeTimeout>2.0f)
			iReplaySignFadeTimeout-=2.0f;
		
		if( iReplaySignFadeTimeout > 1.0f )
			color.setFloatA( 1.0f - ( iReplaySignFadeTimeout - 1.0f ) );
		else
			color.setFloatA( iReplaySignFadeTimeout );

		iReplaySign->setVertexSetColor(color);		
		
		///efeito de entrada da aba de replay.		
		if (iReplayTabGroup.isVisible() && iReplayTabGroup.getPosition()->x>0)
			iReplayTabGroup.move(-32,0);
		
	}
	
	switch( matchState )
	{
		case MATCH_STATE_REPLAY_SHOWN:		
			if (iReplayState==REPLAY_BT_PLAY)
				updateMovingObjects( timeElapsed*iReplaySpeedVector[iReplaySpeedIndex]);		
			break;
			
		case MATCH_STATE_FEEDBACK_GOAL:
		case MATCH_STATE_FEEDBACK_TOO_BAD:
		case MATCH_STATE_FEEDBACK_GAME_OVER:
			if( stateTimeCounter >= 0.0f )
			{
				stateTimeCounter += timeElapsed;
				if( stateTimeCounter >= stateDuration )
				{
					stateTimeCounter = -1.0f;
					stateDuration = -1.0f;
					
					if( feedbackMsgs[ matchState - MATCH_STATE_FEEDBACK_GOAL ]->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN )
					{
						if( matchState == MATCH_STATE_FEEDBACK_GAME_OVER )
							feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_GAME_OVER ]->startHideAnimation();
						else
							setState( MATCH_STATE_HIDING_FEEDBACK );
					}
					else
					{
#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
						// Passa qualquer coisa no parâmetro. Não tem problema pois não vamos utilizar este valor
						onDoubleClick( 0 );
#else
						feedbackMsgs[ matchState - MATCH_STATE_FEEDBACK_GOAL ]->startShowAnimation();
#endif
					}
				}
			}
			
			if( matchState != MATCH_STATE_FEEDBACK_GAME_OVER )
				updateMovingObjects( timeElapsed );
			
			break;
			
		case MATCH_STATE_FEEDBACK_NEXT_CHALLENGE:
			if( stateTimeCounter >= 0.0f )
			{
				stateTimeCounter += timeElapsed;
				if( stateTimeCounter >= stateDuration )
				{
					stateTimeCounter = -1.0f;
					
					pBtKick->startShowAnimation();
					pBtPause->startShowAnimation();
					pBtFocusOnBall->startShowAnimation();
					pBtInfo->startShowAnimation();
				}
			}
			break;
			
		case MATCH_STATE_RESULT_SHOWN:
			if( stateTimeCounter >= 0.0f )
			{
				stateTimeCounter += timeElapsed;
				if( stateTimeCounter >= stateDuration )
				{
					stateTimeCounter = -1.0f;
					stateDuration = -1.0f;
					
					onResultShown();
				}
			}
			else
			{
				if( !pInfoTab->isDoingUpdateAnimation() )
				{
					stateTimeCounter = 0.0f;
					stateDuration = GAME_SCREEN_RESULT_SHOWN_DUR;
				}
			}
			// Sem break
			
		case MATCH_STATE_GETTING_READY:
		case MATCH_STATE_BEFORE_FOCUS_ON_BALL:
		case MATCH_STATE_FOCUSING_ON_BALL:
		case MATCH_STATE_AFTER_FOCUS_ON_BALL:
		case MATCH_STATE_SHOWING_INFO:
		case MATCH_STATE_INFO_SHOWN:
		case MATCH_STATE_HIDING_INFO:
		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:
		case MATCH_STATE_SHOWING_KICK_CONTROLS:
		case MATCH_STATE_SETTING_KICK:
		case MATCH_STATE_HIDING_KICK_CONTROLS:
		case MATCH_STATE_HIDING_EFFECT_KICK_BT:
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
#if DEBUG && ( IS_CURR_TEST( TEST_BALL_SHADOW ) || ( IS_CURR_TEST( TEST_BALL_INSIDE_GOAL ) ) )
		{
			static bool ballGoingUp = true;
			
			Point3f currBallRealPos = *( pBall->getRealPosition() );
			
			if( !ballGoingUp )
			{
				currBallRealPos.y -= 0.05f;
				if( currBallRealPos.y < 0.0f )
				{
					currBallRealPos.y = 0.0f;
					ballGoingUp = true;
				}
			}
			else
			{
				currBallRealPos.y += 0.05f;
				
				if( currBallRealPos.y > 15.0f )
				{
					currBallRealPos.y = 15.0f;
					ballGoingUp = false;
				}
				
			}
			pBall->setRealPosition( currBallRealPos.x, currBallRealPos.y, currBallRealPos.z );
			
#if IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
			pBall->keepBallInsideGoal();
#endif
		}
#endif
			
		{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			
			AnimatedCamera* pCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );
			
			if( movingCamera )
			{
				onMoveCamera( cameraMovement.x, cameraMovement.y );
				
				if( ballDraggingTouch >= 0 )
				{
					Point3f ballPos = ( *pBall->getRealPosition() ) + GameUtils::isoPixelsToGlobalY0( cameraMovement.x, cameraMovement.y );
					ballPos = GameUtils::isoGlobalToPixels( ballPos );
					onBallPosChanged( ballPos );
				}
				else if( aimDraggingTouch >= 0 )
				{
					Point3f ballTargetPos = ( *pBallTarget->getRealPosition() ) + GameUtils::isoPixelsToGlobalY0( cameraMovement.x, cameraMovement.y );
					ballTargetPos = GameUtils::isoGlobalToPixels( ballTargetPos );
					onBallTargetPosChanged( ballTargetPos );
				}
			}
			else
			{
				pCamera->update( timeElapsed );
				
				// Garante que não extrapolou os limites estabelecidos
				onMoveCamera( 0.0f, 0.0f );
			}
			
			pKeeper->update( timeElapsed );
			pBarrier->update( timeElapsed );
			
			pBall->update( timeElapsed );
			pBallTarget->update( timeElapsed );
			
			updateFlags( timeElapsed );
			
		} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;
			
		case MATCH_STATE_PLAYER_KICKING:
		case MATCH_STATE_PLAYER_RUNNING:
		case MATCH_STATE_PLAYER_KICKING_REPLAY:
		case MATCH_STATE_PLAYER_RUNNING_REPLAY:
#if DEBUG && IS_CURR_TEST( TEST_PLAYER_KICK )
			timeElapsed /= 10.0f;
#endif
			
			pKeeper->update( timeElapsed );
			pBarrier->update( timeElapsed );
			
			pBallTarget->update( timeElapsed );
			updateFlags( timeElapsed );
			
			if( pBall->getAuraState() == BALL_AURA_STATE_HIDDEN )
				pPlayer->update( timeElapsed );
			else
				pBall->update( timeElapsed );
			
		case MATCH_STATE_SHOWING_EFFECT_CONTROLS:
		case MATCH_STATE_SETTING_EFFECT:
			break;
			
		case MATCH_STATE_ANIMATING_PLAY:
			///<DM> reseta a camera.
			movingCamera=true;			
			///</DM>
			updateAnimatingPlay( timeElapsed );
			break;

		case MATCH_STATE_ANIMATING_REPLAY:			
#if DEBUG && ( IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) || IS_CURR_TEST( TEST_SOCCER_BALL_POS ) || IS_CURR_TEST( TEST_BARRIER_ANIM ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
			updateAnimatingPlay( 0.01f );
#else
			if( iReplayState == REPLAY_BT_PLAY )
				updateAnimatingPlay( timeElapsed * iReplaySpeedVector[iReplaySpeedIndex] );
#endif
			break;
			
		case MATCH_STATE_NEXT_LEVEL:
			updateMovingObjects( timeElapsed );
			
		case MATCH_STATE_PAUSED:
		case MATCH_STATE_TRANSITION:
			break;
			
			
#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )
		case MATCH_STATE_TEST_0:

			// pPlayer->update( timeElapsed );
			// pKeeper->update( timeElapsed );
			
			static float bla = 0.0f;
			bla += timeElapsed;
			if( bla > 5.0f )
			{
				setState( MATCH_STATE_TEST_0 );
				bla = 0.0f;
			}
			break;
#endif
	}
	
	// Atualiza os controles da interface
	updateInterfaceControls( timeElapsed );
	
	return true;
}

/*===========================================================================================
 
 MÉTODO handleEvent
 Método que trata os eventos enviados pelo sistema.
 
 ============================================================================================*/

bool GameScreen::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	// Obtém a informação do toque
	const NSArray* hTouchesArray = ( NSArray* )pParam;
	
	// OBS : Acho que isso nunca acontece, mas...
	uint8 nTouches = [hTouchesArray count];
	if( nTouches == 0 )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
		{
			int8 touchIndex = -1;
			for( uint8 i = 0 ; ( i < nTouches ) && ( nActiveTouches < MAX_TOUCHES ) ; ++i )	
			{
				if( ( touchIndex = startTrackingTouch( [hTouchesArray objectAtIndex:i] ) ) >= 0 )
					onNewTouch( touchIndex, &trackedTouches[i].startPos );
			}
		}
			return true;
			
		case EVENT_TOUCH_MOVED:
			// Se possuímos mais de um toque, estamos recebendo uma operação de zoom
#if MAX_TOUCHES > 1
			if( nActiveTouches > 1 ) // No caso deste jogo, > 1 será sempre == 2 
			{
				// Fazer zoom quando o usuário move apenas um dedo está causando bugs estranhos
				///DM: adicionei os estados de replay aos estados possíveis de se dar zoom.					
#if DEBUG && ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) )
				if( ( nTouches == 1 ) || ( ( matchState != MATCH_STATE_GETTING_READY ) && ( matchState != MATCH_STATE_ANIMATING_PLAY ) && ( matchState != MATCH_STATE_ANIMATING_REPLAY ) ) )
					return false;
#else
				if( ( nTouches == 1 ) || ( matchState != MATCH_STATE_GETTING_READY && matchState != MATCH_STATE_ANIMATING_REPLAY && matchState != MATCH_STATE_REPLAY_SHOWN) )
					return false;
#endif
				
				int8 touchIndex = -1;
				Point3f aux[ MAX_TOUCHES ] = { trackedTouches[0].startPos, trackedTouches[1].startPos };
				
				for( uint8 i = 0 ; i < nTouches ; ++i )
				{
					UITouch* hTouch = [hTouchesArray objectAtIndex:i];
					if( ( touchIndex = isTrackingTouch( hTouch ) ) >= 0 )
					{
						CGPoint currTouchPos = [hTouch locationInView: NULL];
						aux[ touchIndex ].set( currTouchPos.x, currTouchPos.y );
					}
				}
				
				// Se os toques estão se aproximando, é um zoom out
				float currZoom = pCurrCamera->getZoomFactor();
				float newDistBetweenTouches = ( aux[0] - aux[1] ).getModule();
				float movement = fabsf( newDistBetweenTouches - initDistBetweenTouches );
				if( movement < ZOOM_MIN_DIST_TO_CHANGE )
					return false;
				
				if( NanoMath::fltn( newDistBetweenTouches, initDistBetweenTouches ) )
				{
					float newZoom = currZoom * powf( ZOOM_OUT_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
					
					if( newZoom < ZOOM_MIN )
						pCurrCamera->zoom( ZOOM_MIN );
					else
						pCurrCamera->zoom( newZoom );
				}
				// Se os toques estão se afastando, é um zoom in
				else if( NanoMath::fgtn( newDistBetweenTouches, initDistBetweenTouches ) )
				{
					float newZoom = currZoom * powf( ZOOM_IN_FACTOR, movement / ZOOM_MIN_DIST_TO_CHANGE );
					
					if( newZoom > ZOOM_MAX )
						pCurrCamera->zoom( ZOOM_MAX );
					else
						pCurrCamera->zoom( newZoom );
				}
				
				// Garante que o zoom não fez a câmera extrapolar os limites
#if ! CONFIG_FREE_CAM_MOVEMENT
				onMoveCamera( 0.0f, 0.0f );
#endif
				
#if DEBUG
#if IS_CURR_TEST( TEST_ZOOM )
				LOG( @"Curr Camera Zoom: %.3f", pCurrCamera->getZoomFactor() );
#endif
#endif
				
				initDistBetweenTouches = newDistBetweenTouches;
			}
			// Movimentação de toque único
			else
			{
#endif
				UITouch* hTouch = [hTouchesArray objectAtIndex:0];
				int8 touchIndex = isTrackingTouch( hTouch );
				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					CGPoint lastPos = [hTouch previousLocationInView:NULL];
					
					Point3f touchCurrPos( currPos.x, currPos.y );
					Point3f touchLastPos( lastPos.x, lastPos.y );
					
					onSingleTouchMoved( touchIndex, &touchCurrPos, &touchLastPos );
				}
#if MAX_TOUCHES > 1
			}
#endif
			return true;
			
		case EVENT_TOUCH_CANCELED:
		case EVENT_TOUCH_ENDED:
			for( uint8 i = 0 ; i < nTouches ; ++i )
			{
				UITouch* hTouch = [hTouchesArray objectAtIndex:i];
				int8 touchIndex = isTrackingTouch( hTouch );
				
				if( touchIndex >= 0 )
				{
					CGPoint temp = [hTouch locationInView:NULL];
					
					Point3f currPos( temp.x, temp.y );
					currPos = Utils::MapPointByOrientation( &currPos );
					
					Point3f touchPos( currPos.x, currPos.y );
					
#if DEBUG && ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_BALL_TARGET_HIT ) )
					// Verifica se houve um toque duplo
					if( [hTouch tapCount] == 2 )
					{
						onDoubleClick( touchIndex );
					}
#endif
					
					onTouchEnded( touchIndex, &touchPos );
					
					// Cancela o rastreamento do toque
					stopTrackingTouch( touchIndex );
				}
			}
			return true;
	}
	return false;
}

/*===========================================================================================
 
 MÉTODO onNewTouch
 Trata eventos de início de toque.
 
 ============================================================================================*/

void GameScreen::onNewTouch( int8 touchIndex, const Point3f* pTouchPos )
{
#if DEBUG
	LOG( "GameScreen::onNewTouch : matchState %d, touchIndex %d, TouchPos x = %.3f, y = %.3f, z = %.3f\n", matchState, touchIndex, pTouchPos->x, pTouchPos->y, pTouchPos->z );
#endif
	switch( matchState )
	{
		case MATCH_STATE_REPLAY_SHOWN:
		case MATCH_STATE_ANIMATING_REPLAY:		
			
			///botões da aba de replay.
			if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_REPLAY ) )
			{
				buttonPressingTouch = touchIndex;
				break;
			}/*
			 else	///faz reaparecer a aba de controles de replay.
			 if (Utils::IsPointInsideRect( pTouchPos,iReplayTabBG->getPosition()->x,iReplayTabBG->getPosition()->y,iReplayTabBG->getWidth(),iReplayTabBG->getHeight()))
			 {
			 SetReplayControlVisibility(true);
			 onReplayCommand(REPLAY_BT_PAUSEPLAY);
			 }*/
			else
			{ ///movimentando a camera.
				dynamic_cast< AnimatedCamera* >( pCurrCamera )->cancelAnim();
				movingCamera = true;				
				cameraMovement.set();
			}
			// sem break mesmo, para poder tratar scroll, zoom, etc. durante o replay
			
#if DEBUG &&  ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) )
		case MATCH_STATE_ANIMATING_PLAY:
#endif
		case MATCH_STATE_GETTING_READY:
			// Se possuímos mais de um toque simultâneo, ignoramos o evento
			if( nActiveTouches > 1 )
			{				
				buttonPressingTouch = -1;
				Color colorUnpressed( GAME_SCREEN_BT_COLOR_UNPRESSED );
				pBtInfo->setVertexSetColor( colorUnpressed );
				pBtKick->setVertexSetColor( colorUnpressed );
				pBtPause->setVertexSetColor( colorUnpressed );
				pBtFocusOnBall->setVertexSetColor( colorUnpressed );
				break;
			}
			
			aimDraggingTouch = -1;
			ballDraggingTouch = -1;
			fieldDraggingTouch = -1;
			buttonPressingTouch = -1;
			
			if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_ALL ) )
			{
				buttonPressingTouch = touchIndex;
				break;
			}
			if( ( gameInfo.gameMode == GAME_MODE_TRAINING ) && checkTouchBallCollision( touchIndex ) )
			{
				ballDraggingTouch = touchIndex;
			}
			else if( ( gameInfo.gameMode == GAME_MODE_TRAINING ) && checkTouchAimCollision( touchIndex ) )
			{
				aimDraggingTouch = touchIndex;
			}
			else
			{
				fieldDraggingTouch = touchIndex;
			}
			break;
			
		case MATCH_STATE_INFO_SHOWN:
		case MATCH_STATE_RESULT_SHOWN:
			if( nActiveTouches > 1 )
			{
				pInfoTab->setCloseButtonState( false, GAME_SCREEN_BT_COLOR_UNPRESSED );
			}
			else
			{
				if( pInfoTab->checkCloseButtonCollision( pTouchPos ) )
				{
					[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
					
					pInfoTab->setCloseButtonState( true, GAME_SCREEN_BT_COLOR_PRESSED );
				}
			}
			break;
			
			// Ignora toques nesses estados
		case MATCH_STATE_BEFORE_FOCUS_ON_BALL:
		case MATCH_STATE_FOCUSING_ON_BALL:
		case MATCH_STATE_AFTER_FOCUS_ON_BALL:
			break;
			// Verifica se o usuário apertou o botão de pause
		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:            
#if !( DEBUG &&  ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) ) )
		case MATCH_STATE_ANIMATING_PLAY:
#endif
		case MATCH_STATE_FEEDBACK_NEXT_CHALLENGE:           
		case MATCH_STATE_PLAYER_RUNNING:
		case MATCH_STATE_PLAYER_KICKING:
		case MATCH_STATE_SHOWING_EFFECT_CONTROLS:
		case MATCH_STATE_HIDING_EFFECT_KICK_BT:
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
		case MATCH_STATE_SHOWING_KICK_CONTROLS:
		case MATCH_STATE_HIDING_KICK_CONTROLS:
			if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE ) )
			{
				buttonPressingTouch = touchIndex;
			}
			break;
			
			// Verifica se o usuário apertou um dos botões
#if ! TARGET_IPHONE_SIMULATOR
		case MATCH_STATE_SETTING_EFFECT:
		case MATCH_STATE_SETTING_KICK:
			if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE | INTERFACE_BT_MASK_KICK ) )
			{
				buttonPressingTouch = touchIndex;
			}
			break;
#endif
#if TARGET_IPHONE_SIMULATOR
		case MATCH_STATE_SETTING_EFFECT:
			pSetEffectControls->stopListeningTo( EVENT_TOUCH );
			
			if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE | INTERFACE_BT_MASK_KICK ) )
			{
				buttonPressingTouch = touchIndex;
			}
			else
			{
				pSetEffectControls->startListeningTo( EVENT_TOUCH );
			}
			break;
		case MATCH_STATE_SETTING_KICK:
			pPowerMeter->stopListeningTo( EVENT_TOUCH );
			pDirectionMeter->stopListeningTo( EVENT_TOUCH );
			
			if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE | INTERFACE_BT_MASK_KICK ) )
			{
				buttonPressingTouch = touchIndex;
			}
			else
			{
				pPowerMeter->startListeningTo( EVENT_TOUCH );
				pDirectionMeter->startListeningTo( EVENT_TOUCH );
			}
			break;
#endif
	}
}


/*===========================================================================================
 
MÉTODO checkButtonsCollision
	Checa colisão do toque com os botões da interface.
 
=============================================================================================*/

#define INTERFACE_N_COMMAND_BTS 4

bool GameScreen::checkButtonsCollision( const Point3f* pTouchPos, uint8 btMask, bool manageBtsColors )
{
	// Inicializa o vetor que indica se os botões estavam ou não pressionados
	bool wasntPressed[INTERFACE_N_COMMAND_BTS + REPLAY_N_BUTTONS];
	
	Color aux, colorUnpressed( GAME_SCREEN_BT_COLOR_UNPRESSED );
	Button* commandBts[ INTERFACE_N_COMMAND_BTS ] = { pBtKick, pBtPause, pBtInfo, pBtFocusOnBall };

	for( uint8 i = 0 ; i < INTERFACE_N_COMMAND_BTS ; ++i )
		wasntPressed[i] = ( *( commandBts[i]->getVertexSetColor( &aux ) ) == colorUnpressed );
	
	for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
		wasntPressed[INTERFACE_N_COMMAND_BTS + i] = ( *( replayBts[i]->getVertexSetColor( &aux ) ) == colorUnpressed );

	// Atualiza as cores dos vértices dos botões
	if( manageBtsColors )
	{
		for( uint8 i = 0 ; i < INTERFACE_N_COMMAND_BTS ; ++i )
			commandBts[i]->setVertexSetColor( Color( colorUnpressed.getFloatR(), colorUnpressed.getFloatG(), colorUnpressed.getFloatB(), commandBts[i]->getVertexSetColor( &aux )->getFloatA() ) );
		
		for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
			replayBts[i]->setVertexSetColor( Color( colorUnpressed.getFloatR(), colorUnpressed.getFloatG(), colorUnpressed.getFloatB(), replayBts[i]->getVertexSetColor( &aux )->getFloatA() ) );			
	}
	
	// Checa se houve colisão do toque com cada botão da interface
	// TODOO: Daria pra fazer todos os testes em um único loop, caso os botões possuíssem uma propriedade "área de colisão". Da forma que está, tais
	// áreas estão hardcoded nas chamadas de Utils::IsPointInsideRect...
	Point3f btPos = *( pBtKick->getPosition() );
	if( ( btMask & INTERFACE_BT_MASK_KICK ) && pBtKick->isVisible() && (pBtKick->getInterfaceControlState()==INTERFACE_CONTROL_STATE_SHOWN) && Utils::IsPointInsideRect( pTouchPos, 0.0f, btPos.y, btPos.x + pBtKick->getWidth(), SCREEN_HEIGHT - btPos.y ) )
	{
		if( wasntPressed[0] )
			[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
		
		if( manageBtsColors )
			pBtKick->setVertexSetColor( GAME_SCREEN_BT_COLOR_PRESSED );
		
#if DEBUG
		LOG( "Kick Pressed\n" );
#endif
		
		return true;
	}
	
	btPos = *( pBtPause->getPosition() );
	if( ( btMask & INTERFACE_BT_MASK_PAUSE ) && pBtPause->isVisible()&& (pBtPause->getInterfaceControlState()==INTERFACE_CONTROL_STATE_SHOWN) && Utils::IsPointInsideRect( pTouchPos, btPos.x, btPos.y, SCREEN_WIDTH - btPos.x, SCREEN_HEIGHT - btPos.y ) )
	{
		if( wasntPressed[1] )
			[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
		
		if( manageBtsColors )
			pBtPause->setVertexSetColor( GAME_SCREEN_BT_COLOR_PRESSED );
		
#if DEBUG
		LOG( "Pause Pressed\n" );
#endif
		
		return true;
	}
	
	btPos = *( pBtInfo->getPosition() );
	if( ( btMask & INTERFACE_BT_MASK_INFO ) && pBtInfo->isVisible() && (pBtInfo->getInterfaceControlState()==INTERFACE_CONTROL_STATE_SHOWN)&& Utils::IsPointInsideRect( pTouchPos, btPos.x, 0.0f, SCREEN_WIDTH - btPos.x, btPos.y + pBtInfo->getHeight() ) )
	{
		if( wasntPressed[2] )
			[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
		
		if( manageBtsColors )
			pBtInfo->setVertexSetColor( GAME_SCREEN_BT_COLOR_PRESSED );
		
#if DEBUG
		LOG( "Info Pressed\n" );
#endif
		
		return true;
	}
	
	btPos = *( pBtFocusOnBall->getPosition() );
	if( ( btMask & INTERFACE_BT_MASK_FOCUS_ON_BALL ) && pBtFocusOnBall->isVisible() && (pBtFocusOnBall->getInterfaceControlState()==INTERFACE_CONTROL_STATE_SHOWN)&& Utils::IsPointInsideRect( pTouchPos, 0.0f, 0.0f, btPos.x + pBtFocusOnBall->getWidth(), btPos.y + pBtFocusOnBall->getHeight() ) )
	{
		if( wasntPressed[3] )
			[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
		
		if( manageBtsColors )
			pBtFocusOnBall->setVertexSetColor( GAME_SCREEN_BT_COLOR_PRESSED );
		
#if DEBUG
		LOG( "Focus Pressed\n" );
#endif
		
		return true;
	}
	
	if( btMask & INTERFACE_BT_MASK_REPLAY )
	{
		int32 width , height;
		
		for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
		{
			// Os botões de esconder a aba de replay, sair do replay e ir para a câmera são maiores que os outros
			if( i == REPLAY_BT_HIDE || i == REPLAY_BT_STOP || i == REPLAY_BT_CAMTOGGLE || i == REPLAY_BT_SHOW )
			{
				width = replayBts[ REPLAY_BT_HIDE ]->getWidth();
				height = replayBts[ REPLAY_BT_HIDE ]->getHeight();
			}
			else
			{
				// Os outros têm a mesma área de colisão 
				width = 40;
				height = width;
			}
			
			btPos = *( replayBts[i]->getPosition() );
			if( replayBts[i]->isVisible() && ( replayBts[i]->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) && Utils::IsPointInsideRect( pTouchPos, btPos.x, btPos.y, width, height ) )
			{
				if( wasntPressed[INTERFACE_N_COMMAND_BTS + i] )
				{
					[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
					
					if( manageBtsColors )
						replayBts[i]->setVertexSetColor( GAME_SCREEN_BT_COLOR_PRESSED );
				}				
				
				return true;
			}
		}
	}
	return false;
}

#undef INTERFACE_N_COMMAND_BTS

/*===========================================================================================
 
 MÉTODO getPickingRay
 Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
 jogo.
 
 ============================================================================================*/

void GameScreen::getPickingRay( Ray& ray, const Point3f& p ) const
{
#warning Está errado!!!!!!!!!!!! Será q é em GetPickingRay, ou será q tenho q tirar Utils::MapPointByOrientation? Verificar...
	
	Utils::GetPickingRay( &ray, pCurrCamera, p.x, p.y );
	
	// Corrige a origem do raio, já que esta será sempre a posição do observador (centro da tela)
	// OBS: Esta abordagem está PORCA!!!!! O certo seria utilizarmos:
	// Utils::Unproject( &p, pCurrCamera, &trackedTouches[ touchIndex ].unmappedStartPos );
	// No entanto, a saída de Utils::Unproject não está produzindo valores corretos com a projeção ortogonal
	
	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	Point3f screenCenter( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	ray.origin += ( Utils::MapPointByOrientation( &p ) - screenCenter ) * invZoom;
}

/*===========================================================================================
 
 MÉTODO checkTouchBallCollision
 Checa colisão do toque com a bola.
 
 ============================================================================================*/

bool GameScreen::checkTouchBallCollision( int8 touchIndex )
{
	// Obtém o raio de picking
	Ray r;
	getPickingRay( r, trackedTouches[ touchIndex ].unmappedStartPos );
	
	// Testa colisão do raio com os triângulos que formam o sprite da bola	
	Triangle t0, t1;
	pBall->getTouchCollisionArea( t0, t1 );
	
	float t, u, v;
	
#if DEBUG
	bool ret1 = Utils::IntersectRayTriangle( false, &r, &t0, &t, &u, &v );
	bool ret2 = Utils::IntersectRayTriangle( false, &r, &t1, &t, &u, &v );
	return ret1 || ret2;
#else
	return Utils::IntersectRayTriangle( false, &r, &t0, &t, &u, &v ) || Utils::IntersectRayTriangle( false, &r, &t1, &t, &u, &v );
#endif
}

/*===========================================================================================
 
 MÉTODO checkTouchAimCollision
 Checa colisão do toque com a mira.
 
 ============================================================================================*/

bool GameScreen::checkTouchAimCollision( int8 touchIndex )
{
	// Obtém o raio de picking
	Ray r;
	getPickingRay( r, trackedTouches[ touchIndex ].unmappedStartPos );
	
	// Testa colisão do raio com os triângulos que formam o sprite da bola	
	Triangle t0, t1;
	pBallTarget->getTouchCollisionArea( t0, t1 );
	
	float t, u, v;
	return Utils::IntersectRayTriangle( false, &r, &t0, &t, &u, &v ) || Utils::IntersectRayTriangle( false, &r, &t1, &t, &u, &v );
}

/*===========================================================================================
 
 MÉTODO onTouchEnded
 Trata eventos de fim de toque.
 
 ============================================================================================*/

void GameScreen::onTouchEnded( int8 touchIndex, const Point3f* pTouchPos )
{	
#if DEBUG
	#if IS_CURR_TEST( TEST_FEEDBACK_MSG_GOAL )
		
		setState( MATCH_STATE_FEEDBACK_GOAL );
		
	#elif IS_CURR_TEST( TEST_FEEDBACK_MSG_GAME_OVER )
		
		setState( MATCH_STATE_FEEDBACK_GAME_OVER );
		
	#elif IS_CURR_TEST( TEST_FEEDBACK_MSG_TOO_BAD )
		
		setState( MATCH_STATE_FEEDBACK_TOO_BAD );
		
	#elif IS_CURR_TEST( TEST_FEEDBACK_MSG_NEXT_CHALLENGE )
		
		setState( MATCH_STATE_FEEDBACK_NEXT_CHALLENGE );
		
	#endif
#endif
	
	switch( matchState )
	{
#if DEBUG &&  ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) )
		case MATCH_STATE_ANIMATING_PLAY:
		case MATCH_STATE_ANIMATING_REPLAY:
#endif
			
		case MATCH_STATE_REPLAY_SHOWN:
		case MATCH_STATE_ANIMATING_REPLAY:
			if( touchIndex == buttonPressingTouch )
			{			
				if( !checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_REPLAY, false ) )
				{
					movingCamera = true;
					cameraMovement.set();
				}
				else
				{
					Color aux, pressed( GAME_SCREEN_BT_COLOR_PRESSED );
					for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
					{
						if( *( replayBts[i]->getVertexSetColor( &aux ) ) == pressed )
						{
							onReplayCommand( i );
							break;
						}
					}
				}
			}
			break;
			
		case MATCH_STATE_GETTING_READY:
			{
				movingCamera = false;
				cameraMovement.set();
				
				if( touchIndex == aimDraggingTouch )
				{
					aimDraggingTouch = -1;
				}
				else if( touchIndex == ballDraggingTouch )
				{
					ballDraggingTouch = -1;
				}
				else if( touchIndex == fieldDraggingTouch )
				{
					fieldDraggingTouch = -1;
				}
				else if( touchIndex == buttonPressingTouch )
				{
					if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_INFO, false ) )
					{
						setState( MATCH_STATE_SHOWING_INFO );
					}
					else if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_KICK, false ) )
					{
						if( gameInfo.gameMode == GAME_MODE_TRAINING )
						{
							// OLD
							//pBall->setAuraVisible( false );
							
							lastBallInitialPosition = *( pBall->getRealPosition() );
						}
						
						pBtKick->setVertexSetColor( GAME_SCREEN_BT_COLOR_UNPRESSED );
						
						if( gameInfo.cameraAutoAdjust )
							setState( MATCH_STATE_ADJUSTING_CAM_TO_SHOT );
						else
							setState( MATCH_STATE_SHOWING_KICK_CONTROLS );
					}
					else if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE, false ) )
					{
						setState( MATCH_STATE_PAUSED );
					}
					else if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_FOCUS_ON_BALL, false ) )
					{
						setState( MATCH_STATE_BEFORE_FOCUS_ON_BALL );
					}
				}
			}
			break;
			
		case MATCH_STATE_SETTING_KICK:
			if( touchIndex == buttonPressingTouch )
			{
				if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_KICK, false ) )
				{
					onDirAndPowerSet();
				}
				else if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE, false ) )
				{
					setState( MATCH_STATE_PAUSED );
				}
			}
			break;
			
		case MATCH_STATE_SETTING_EFFECT:
			if( touchIndex == buttonPressingTouch )
			{
				if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_KICK, false ) )
				{
					pChronometer->pause( true );
					pSetEffectControls->setEffect();
					setState( MATCH_STATE_HIDING_EFFECT_KICK_BT );
				}
				else if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE, false ) )
				{
					setState( MATCH_STATE_PAUSED );
				}
			}
			break;

		// Ignora toques nesses estados
		case MATCH_STATE_BEFORE_FOCUS_ON_BALL:
		case MATCH_STATE_FOCUSING_ON_BALL:
		case MATCH_STATE_SHOWING_REPLAY:
		case MATCH_STATE_HIDING_REPLAY:
		case MATCH_STATE_AFTER_FOCUS_ON_BALL:
			break;
			
		// Verifica se o usuário apertou o botão de pause
		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:
#if !( DEBUG &&  ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) ) )
		case MATCH_STATE_ANIMATING_PLAY:
#endif
		case MATCH_STATE_FEEDBACK_NEXT_CHALLENGE:	
		case MATCH_STATE_PLAYER_RUNNING:
		case MATCH_STATE_PLAYER_KICKING:
		case MATCH_STATE_SHOWING_EFFECT_CONTROLS:
		case MATCH_STATE_HIDING_EFFECT_KICK_BT:
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
		case MATCH_STATE_SHOWING_KICK_CONTROLS:
		case MATCH_STATE_HIDING_KICK_CONTROLS:
			if( touchIndex == buttonPressingTouch )
			{
				if( checkButtonsCollision( pTouchPos, INTERFACE_BT_MASK_PAUSE, false ) )
				{
					setState( MATCH_STATE_PAUSED );
				}
			}
			break;

		case MATCH_STATE_INFO_SHOWN:
#if DEBUG && IS_CURR_TEST( TEST_POINTS_ANIM )	
			gameInfo.points += 30;
			gameInfo.levelScore += 30;
			pInfoTab->updateScore( false );
#endif
			if( pInfoTab->isCloseButtonPressed() )
				setState( MATCH_STATE_HIDING_INFO );
			break;
			
		case MATCH_STATE_RESULT_SHOWN:
			if( pInfoTab->isCloseButtonPressed() )
			{
				pInfoTab->setCloseButtonState( false, GAME_SCREEN_BT_COLOR_UNPRESSED );
				
				stateTimeCounter = -1.0f;
				stateDuration = -1.0f;
				
				onResultShown();
			}
			break;
	}
	
	// Desativa um possível botão pressionado
	if( touchIndex == buttonPressingTouch )
	{
		buttonPressingTouch = -1;
		Color colorUnpressed( GAME_SCREEN_BT_COLOR_UNPRESSED );
		pBtInfo->setVertexSetColor( colorUnpressed );
		pBtKick->setVertexSetColor( colorUnpressed );
		pBtPause->setVertexSetColor( colorUnpressed );
		pBtFocusOnBall->setVertexSetColor( colorUnpressed );
		
		for( uint8 i = 0 ; i < REPLAY_N_BUTTONS ; ++i )
			replayBts[i]->setVertexSetColor( colorUnpressed );
	}
}

/*===========================================================================================
 
 MÉTODO onDoubleClick
 Trata eventos de clique duplo.
 
 ============================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_BALL_TARGET_HIT ) )

void GameScreen::onDoubleClick( int8 touchIndex )
{
#if IS_CURR_TEST( TEST_BALL_FX_FACTOR )
	setState( MATCH_STATE_NEXT_TRY );
#else
	if( pBallTarget->getState() == BALL_TARGET_UNHIT )
		pBallTarget->setState( BALL_TARGET_HIT );
	else
		pBallTarget->setState( BALL_TARGET_UNHIT );
#endif
	
	//	switch( matchState )
	//	{
	//		case MATCH_STATE_GETTING_READY:
	//			if( gameInfo.cameraAutoAdjust ) 
	//				setState( MATCH_STATE_ADJUSTING_CAM_TO_SHOT );
	//			else
	//				setState( MATCH_STATE_SHOWING_KICK_CONTROLS );
	//			break;
	//
	//		case MATCH_STATE_SETTING_KICK:
	//			currPlay.set( 0.0f, 0.0f, ISO_POWER_MAX_VALUE * 0.5 );
	//			onKick();
	//			break;
	//	}
}

#endif

/*===========================================================================================
 
 MÉTODO onSingleTouchMoved
 Trata eventos de movimentação de toques.
 
 ============================================================================================*/

void GameScreen::onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos )
{
	switch( matchState )
	{
		case MATCH_STATE_REPLAY_SHOWN:
		case MATCH_STATE_ANIMATING_REPLAY:
			if( buttonPressingTouch >= 0 )
			{			
				Point3f fixedTouchPos = Utils::MapPointByOrientation( pCurrPos );				
				if( !checkButtonsCollision( &fixedTouchPos, INTERFACE_BT_MASK_REPLAY ) )
				{
					buttonPressingTouch = -1;
				}
			}
			else
			{
				dynamic_cast< AnimatedCamera* >( pCurrCamera )->cancelAnim();
				movingCamera = false;
				cameraMovement.set();
			}
			
#if DEBUG &&  ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) )
		case MATCH_STATE_ANIMATING_PLAY:
		case MATCH_STATE_ANIMATING_REPLAY:
#endif
		case MATCH_STATE_GETTING_READY:
			if( ( ballDraggingTouch >= 0 ) || ( aimDraggingTouch >= 0 ) || ( fieldDraggingTouch >= 0 ) )
			{
				Ray r;
				getPickingRay( r, *pCurrPos );
				
				if( touchIndex == ballDraggingTouch )
				{
					onBallPosChanged( r.origin );
					
					// Converte a posição da bola para posição da tela
					// OBS: Porco, mas está funcionando
					Point3f ballScreenPos;
					Utils::Project( &ballScreenPos, pCurrCamera, pBall->getPosition() );
					ballScreenPos = Utils::MapPointByOrientation( &ballScreenPos );
					ballScreenPos.x = ( ballScreenPos.x - SCREEN_WIDTH ) * -1;
					
					moveCameraIfDraggingNearBounds( ballScreenPos.x, ballScreenPos.y, pBall->getWidth(), pBall->getHeight() );
				}
				else if( touchIndex == aimDraggingTouch )
				{
					onBallTargetPosChanged( r.origin );	
					
					// Converte a posição da bola para posição da tela
					// OBS: Porco, mas está funcionando
					Point3f ballTargetScreenPos;
					Utils::Project( &ballTargetScreenPos, pCurrCamera, pBallTarget->getPosition() );
					ballTargetScreenPos = Utils::MapPointByOrientation( &ballTargetScreenPos );
					ballTargetScreenPos.x = ( ballTargetScreenPos.x - SCREEN_WIDTH ) * -1;
					
					moveCameraIfDraggingNearBounds( ballTargetScreenPos.x, ballTargetScreenPos.y, pBallTarget->getWidth(), pBallTarget->getHeight() );
				}
				else //if( touchIndex == fieldDraggingTouch )
				{
					float invZoom = 1.0f / pCurrCamera->getZoomFactor();
					Point3f currPos = Utils::MapPointByOrientation( pCurrPos );
					Point3f lastPos = Utils::MapPointByOrientation( pLastPos );
					onMoveCamera( ( currPos.x - lastPos.x ) * invZoom, ( currPos.y - lastPos.y ) * invZoom );
				}
			}
			else if( buttonPressingTouch >= 0 )
			{
				Point3f fixedTouchPos = Utils::MapPointByOrientation( pCurrPos );
				
				if( !checkButtonsCollision( &fixedTouchPos, INTERFACE_BT_MASK_ALL ) )
					buttonPressingTouch = -1;
			}
			break;
			
			
			
		case MATCH_STATE_INFO_SHOWN:
		case MATCH_STATE_RESULT_SHOWN:
		{
			Point3f fixedTouchPos = Utils::MapPointByOrientation( pCurrPos );
			if( !pInfoTab->checkCloseButtonCollision( &fixedTouchPos ) )
				pInfoTab->setCloseButtonState( false, GAME_SCREEN_BT_COLOR_UNPRESSED );
		}
			break;
			
		// Ignora toques nesses estados
		case MATCH_STATE_BEFORE_FOCUS_ON_BALL:
		case MATCH_STATE_FOCUSING_ON_BALL:
		case MATCH_STATE_SHOWING_REPLAY:
		case MATCH_STATE_HIDING_REPLAY:			
		case MATCH_STATE_AFTER_FOCUS_ON_BALL:
			break;
			
		// Verifica se o usuário apertou o botão de pause
		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:
#if !( DEBUG &&  ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) ) )
		case MATCH_STATE_ANIMATING_PLAY:
		// case MATCH_STATE_ANIMATING_REPLAY:			
#endif
		case MATCH_STATE_FEEDBACK_NEXT_CHALLENGE:	
		case MATCH_STATE_PLAYER_RUNNING:
		case MATCH_STATE_PLAYER_KICKING:
		case MATCH_STATE_SHOWING_EFFECT_CONTROLS:
		case MATCH_STATE_HIDING_EFFECT_KICK_BT:
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
		case MATCH_STATE_SHOWING_KICK_CONTROLS:
		case MATCH_STATE_HIDING_KICK_CONTROLS:
			if( buttonPressingTouch >= 0 )
			{
				Point3f fixedTouchPos = Utils::MapPointByOrientation( pCurrPos );
				
				if( !checkButtonsCollision( &fixedTouchPos, INTERFACE_BT_MASK_PAUSE ) )
					buttonPressingTouch = -1;
			}
			break;
			
			// Verifica se o usuário apertou um dos botões disponíveis
		case MATCH_STATE_SETTING_EFFECT:
		case MATCH_STATE_SETTING_KICK:
			if( buttonPressingTouch >= 0 )
			{
				Point3f fixedTouchPos = Utils::MapPointByOrientation( pCurrPos );
				
				if( !checkButtonsCollision( &fixedTouchPos, INTERFACE_BT_MASK_KICK | INTERFACE_BT_MASK_PAUSE ) )
					buttonPressingTouch = -1;
			}
			break;
	}
}

/*===========================================================================================
 
 MÉTODO onMoveCamera
 Movimenta a câmera respeitando as restrições de posição.
 
 ============================================================================================*/

void GameScreen::onMoveCamera( float dx, float dy )
{
#if CONFIG_FREE_CAM_MOVEMENT
	
	pCurrCamera->move( dx, dy );
	
#else
	
	Point3f camPos = *( pCurrCamera->getPosition() );
	
	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	float halfCamViewVolumeWidth = ( SCREEN_WIDTH * invZoom ) * 0.5f;
	float halfCamViewVolumeHeight = ( SCREEN_HEIGHT * invZoom ) * 0.5f;
	
	camPos.x += dx;
	camPos.y += dy ;
	
	if( camPos.x + halfCamViewVolumeWidth > CAMERA_RIGHT_LIMIT )
	{
		camPos.x -= ( camPos.x + halfCamViewVolumeWidth ) - CAMERA_RIGHT_LIMIT;
	}
	else if( camPos.x - halfCamViewVolumeWidth < CAMERA_LEFT_LIMIT )
	{
		camPos.x += CAMERA_LEFT_LIMIT - ( camPos.x - halfCamViewVolumeWidth );
	}
	
	if( camPos.y - halfCamViewVolumeHeight < CAMERA_TOP_LIMIT )
	{
		camPos.y += CAMERA_TOP_LIMIT - ( camPos.y - halfCamViewVolumeHeight );
	}
	else if( camPos.y + halfCamViewVolumeHeight > CAMERA_BOTTOM_LIMIT )
	{
		camPos.y -= ( camPos.y + halfCamViewVolumeHeight ) - CAMERA_BOTTOM_LIMIT;
	}
	
	pCurrCamera->setPosition( camPos.x, camPos.y, 0.0f );
	
#endif
	
#if DEBUG && IS_CURR_TEST( TEST_CAMERA_POS )
	Point3f currCamPos = *( pCurrCamera->getPosition() );
	LOG( @"Cam Pos: x = %.3f, y = %.3f, z = = %.3f", currCamPos.x, currCamPos.y, currCamPos.z );
#endif
}

/*===========================================================================================
 
 MÉTODO isTrackingTouch
 Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja sendo
 acompanhado.
 
 ============================================================================================*/

int8 GameScreen::isTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == ( int32 )hTouch )
			return i;
	}
	return -1;
}

/*===========================================================================================
 
 MÉTODO startTrackingTouch
 Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
 fazê-lo.
 
 ============================================================================================*/

int8 GameScreen::startTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == 0 )
		{
			++nActiveTouches;
			
			trackedTouches[i].Id = ( int32 )hTouch;
			
			CGPoint temp = [hTouch locationInView: NULL];
			trackedTouches[i].unmappedStartPos.set( temp.x, temp.y );
					
			Point3f touchPos( temp.x, temp.y );
			touchPos = Utils::MapPointByOrientation( &touchPos );
			trackedTouches[i].startPos.set( touchPos.x, touchPos.y );
			
			if( nActiveTouches == 2 )
				initDistBetweenTouches = ( trackedTouches[0].startPos - trackedTouches[1].startPos ).getModule();
			
			return i;
		}
	}
	return -1;
}

/*===========================================================================================
 
 MÉTODO stopTrackingTouch
 Cancela o rastreamento do toque recebido como parâmetro.
 
 ============================================================================================*/

void GameScreen::stopTrackingTouch( int8 touchIndex )
{
	if( touchIndex < 0 )
		return;
	
	if( trackedTouches[ touchIndex ].Id != 0 )
	{
		--nActiveTouches;
		trackedTouches[ touchIndex ].Id = 0;
		initDistBetweenTouches = 0.0f;
	}
}
/*===========================================================================================
 
 MÉTODO SetReplayControlVisibility
 Controla a visibilidade dos controles de replay
 
 ============================================================================================*/

void GameScreen::SetReplayControlVisibility(bool aVisible)
{
	if (aVisible)
	{
		Color color;
		iReplaySign->getVertexSetColor(&color);
		color.setFloatA( 0.0f );
		iReplaySign->setVertexSetColor(color);
		
		iReplayTabBG->setVisible(true);
		iReplayTempoImage->setVisible(true);
		iReplaySpeedPaneBg->setVisible(true);
		iReplayTabButtonsBG->setVisible(true);
		
		iReplayTabGroup.setVisible(true);
		iReplaySpeedImages[iReplaySpeedIndex]->setVisible(true);			
		
		for( uint8 i = 0 ; i < REPLAY_N_BUTTONS -1 ; ++i )
			replayBts[i]->startShowAnimation();
		
		replayBts[REPLAY_BT_SHOW]->startHideAnimation();
	}
	else
	{	
		iReplayTabBG->setVisible(false);
		iReplayTempoImage->setVisible(false);
		iReplaySpeedPaneBg->setVisible(false);
		iReplayTabButtonsBG->setVisible(false);
		
		
		iReplayTabGroup.setVisible(false);
		iReplaySpeedImages[iReplaySpeedIndex]->setVisible(false);	
		
		for( uint8 i = 0 ; i < REPLAY_N_BUTTONS -1 ; ++i )
			if (i!=REPLAY_BT_CAMTOGGLE && i!=REPLAY_BT_STOP)			
				replayBts[i]->startHideAnimation();
		
		replayBts[REPLAY_BT_SHOW]->startShowAnimation();
		
	}
}


/*===========================================================================================
 
 MÉTODO onReplayCommand
 Executa a ação de replay correspondente ao botão pressionado.
 
 ============================================================================================*/

void GameScreen::onReplayCommand( uint8 btIndex )
{
	switch( btIndex )
	{
		case REPLAY_BT_REWIND:
			resetLevel(true);
			setState(MATCH_STATE_REPLAY_SHOWN);
			iReplayState = -1;
			pCurrCamera->zoom( ZOOM_MIN );			
			break;
			
		case REPLAY_BT_HIDE:
			SetReplayControlVisibility(false);
			break;
			
		case REPLAY_BT_PLAY:
			resetLevel( true );
			SetReplayControlVisibility(false);
			movingCamera=true;
			iReplayState = REPLAY_BT_PLAY;
			break;
		case REPLAY_BT_PAUSEPLAY:
			if (iReplayState == REPLAY_BT_PAUSEPLAY)
			{				
				iReplayState = REPLAY_BT_PLAY;
			}
			else
				if (iReplayState == REPLAY_BT_PLAY)
				{
					
					iReplayState = REPLAY_BT_PAUSEPLAY;				
				}
			break;
		case REPLAY_BT_CAMTOGGLE:
		{
			AnimatedCamera* pSceneCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );	
			pSceneCamera->setPosition(pBall->getPosition());
			movingCamera=!movingCamera;			
			if (movingCamera)
				moveCameraTo( pBall->getRealPosition(), CAMERA_FOLLOW_DEFAULT_TIME );
		}
			break;
		case REPLAY_BT_STOP:
			setState(MATCH_STATE_HIDING_REPLAY);
			break;			
		case REPLAY_BT_SLOWDOWN:
			iReplaySpeedImages[iReplaySpeedIndex]->setVisible(false);
			if (iReplaySpeedIndex>0)
				iReplaySpeedIndex--;			
			iReplaySpeedImages[iReplaySpeedIndex]->setVisible(true);			
			break;
		case REPLAY_BT_FASTER:
			iReplaySpeedImages[iReplaySpeedIndex]->setVisible(false);			
			if (iReplaySpeedIndex < 4)
				iReplaySpeedIndex++;			
			iReplaySpeedImages[iReplaySpeedIndex]->setVisible(true);			
			break;
		case REPLAY_BT_SHOW:
			SetReplayControlVisibility(true);
			onReplayCommand(REPLAY_BT_PAUSEPLAY);
			break;
	}
}

/*===========================================================================================
 
 MÉTODO moveCameraIfDraggingNearBounds
 Verifica se o objeto arrastado está perto dos cantos da tela. Se estiver, movimenta a
 câmera na direção em que o objeto está sendo arrastado.
 
 ============================================================================================*/

void GameScreen::moveCameraIfDraggingNearBounds( float screenX, float screenY, float width, float height )
{	
	// Verifica está está perto das bordas da tela
	movingCamera = false;
	cameraMovement.set();
	
	float currZoom = pCurrCamera->getZoomFactor();
	float currSpeed = NanoMath::lerp( ( currZoom - ZOOM_MIN ) / ( ZOOM_MAX - ZOOM_MIN ), CAMERA_AUTO_MOVE_MAX_SPEED, CAMERA_AUTO_MOVE_MIN_SPEED );
	
	float cameraAutoMoveArea = CAMERA_AUTO_MOVE_AREA;
	if( currZoom < 1.0f )
		cameraAutoMoveArea = CAMERA_AUTO_MOVE_AREA * ( 1.0f / currZoom ) * 0.5f; // 0.5f => Número mágico...
	
	if( screenX < cameraAutoMoveArea )
	{
		movingCamera = true;
		cameraMovement.x = -currSpeed;
	}
	else
	{
		float ballXEnd = screenX + ( width * currZoom );
		float viewVolumeXEnd = SCREEN_WIDTH - cameraAutoMoveArea;
		
		if( ballXEnd > viewVolumeXEnd )
		{
			movingCamera = true;
			cameraMovement.x = currSpeed;
		}
	}
	
	if( screenY < cameraAutoMoveArea )
	{
		movingCamera = true;
		cameraMovement.y = -currSpeed;
	}
	else
	{
		float ballYEnd = screenY + ( height * currZoom );
		float viewVolumeYEnd = SCREEN_HEIGHT - cameraAutoMoveArea;
		
		if( ballYEnd > viewVolumeYEnd )
		{
			movingCamera = true;
			cameraMovement.y = currSpeed;
		}
	}
	
	if(movingCamera )
		onMoveCamera( cameraMovement.x, cameraMovement.y );
}

/*===========================================================================================
 
 MÉTODO onAccelerate
 Recebe os eventos do acelerômetro.
 
 ============================================================================================*/

void GameScreen::onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration )
{
	if( !isActive() )
		return;
	
	if( matchState == MATCH_STATE_SETTING_KICK )
	{
		float angleX, angleY;
		GameUtils::GetDeviceAngles( &angleX, &angleY, pRestPosition, pAcceleration );
		
		pPowerMeter->setCurrValuePercent( ( angleX + ACCEL_HALF_MOVEMENT_FREEDOM_X ) / ACCEL_MOVEMENT_FREEDOM_X );
		pDirectionMeter->setCurrValuePercent( ( angleY + ACCEL_HALF_MOVEMENT_FREEDOM_Y ) / ACCEL_MOVEMENT_FREEDOM_Y );	
	}
}

/*===============================================================================
 
 MÉTODO moveCameraTo
 Move a câmera para a posição 'position' real em 'time' segundos.
 
 ===============================================================================*/

void GameScreen::moveCameraTo( const Point3f* pRealPosition, float time )
{
	moveCameraTo( pRealPosition, pCurrCamera->getZoomFactor(), time );
}

/*===============================================================================
 
 MÉTODO moveCameraTo
 Move a câmera para a posição 'position' real em 'time' segundos.
 
 ===============================================================================*/

void GameScreen::moveCameraTo( const Point3f* pRealPosition, float finalZoom, float time )
{
	Point3f destination = *pRealPosition;
	
	if( destination.x < -REAL_FIELD_WIDTH * 0.5f )
		destination.x = -REAL_FIELD_WIDTH * 0.5f;
	else if( destination.x > REAL_FIELD_WIDTH * 0.5f )
		destination.x = REAL_FIELD_WIDTH * 0.5f;
	
	if( destination.y < 0.0f )
		destination.y = 0.0f;
	
	if( destination.z < CAMERA_MIN_Z )	
		return;
	
	if( destination.z > FOUL_MAX_Z_LIMIT )
	{
		destination.z = FOUL_MAX_Z_LIMIT;
	}
	
	// Configura e inicia a animação da câmera
	AnimatedCamera* pSceneCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );	
	
	destination = GameUtils::isoGlobalToPixels( destination );
	destination.z = 0.0f;
	
	pSceneCamera->setAnim( &destination, finalZoom, time );
	pSceneCamera->startAnim();
	
	if( ( time <= 0.0f ) || ( ( destination == *( pSceneCamera->getPosition() ) ) && ( pSceneCamera->getZoomFactor() == finalZoom ) ) )
		pSceneCamera->endAnim();
}

/*===============================================================================
 
 MÉTODO adjustCamera
 Atualiza a posição e o zoom da câmera para englobar o jogador e o gol.
 
 ===============================================================================*/

void GameScreen::adjustCamera( float time )
{	
	// Cria um vetor de posições chave
	Point3f ballPos = *( pKeeper->getPosition() );
	Point3f playerPos = *( pPlayer->getPosition() );
	
	Object* pGoalTop = getObject( OBJ_INDEX_GOAL_TOP );
	Point3f goalTopPos = *( pGoalTop->getPosition() );
	
	Object* pGoalBottom = getObject( OBJ_INDEX_GOAL_BOTTOM );
	Point3f goalBottomPos = *( pGoalBottom->getPosition() );
	
#define KEY_COORDS_ARRAY_SIZE 16
	
	Point3f keyCoords[ KEY_COORDS_ARRAY_SIZE ] =
	{
		Point3f( playerPos ),
		Point3f( playerPos.x, playerPos.y + pPlayer->getHeight() ),
		Point3f( playerPos.x + pPlayer->getWidth(), playerPos.y + pPlayer->getHeight() ),
		Point3f( playerPos.x + pPlayer->getWidth(), playerPos.y ),
		
		Point3f( ballPos ),
		Point3f( ballPos.x, ballPos.y + pBall->getHeight() ),
		Point3f( ballPos.x + pBall->getWidth(), ballPos.y + pBall->getHeight() ),
		Point3f( ballPos.x + pBall->getWidth(), ballPos.y ),
		
		Point3f( goalTopPos ),
		Point3f( goalTopPos.x, goalTopPos.y + pGoalTop->getHeight() ),
		Point3f( goalTopPos.x + pGoalTop->getWidth(), goalTopPos.y + pGoalTop->getHeight() ),
		Point3f( goalTopPos.x + pGoalTop->getWidth(), goalTopPos.y ),
		
		Point3f( goalBottomPos ),
		Point3f( goalBottomPos.x, goalBottomPos.y + pGoalBottom->getHeight() ),
		Point3f( goalBottomPos.x + pGoalBottom->getWidth(), goalBottomPos.y + pGoalBottom->getHeight() ),
		Point3f( goalBottomPos.x + pGoalBottom->getWidth(), goalBottomPos.y )
	};
	
	Point3f min, max;
	getMinCoords( &min, keyCoords, KEY_COORDS_ARRAY_SIZE );
	getMaxCoords( &max, keyCoords, KEY_COORDS_ARRAY_SIZE );
	
	Point3f p1 = GameUtils::isoPixelsToGlobalY0( min );
	Point3f p2 = GameUtils::isoPixelsToGlobalY0( max );
	
#undef KEY_COORDS_ARRAY_SIZE
	
	adjustCamera( p1, p2, time );
}

/*===============================================================================
 
 MÉTODO getMinCoords
 Obtém as maiores coordenadas x e y contidas no array de pontos keyCoords.
 
 ===============================================================================*/

Point3f* GameScreen::getMinCoords( Point3f* pMin, const Point3f* keyCoords, uint8 nCoords ) const
{
	pMin->set( FLT_MAX, FLT_MAX, 0.0f );
	
	for( uint8 i = 0 ; i < nCoords ; ++i )
	{
		if( keyCoords[i].x < pMin->x )
			pMin->x = keyCoords[i].x;
		
		if( keyCoords[i].y < pMin->y )
			pMin->y = keyCoords[i].y;
	}
	return pMin;
}

/*===============================================================================
 
 MÉTODO getMaxCoords
 Obtém as maiores coordenadas x e y contidas no array de pontos keyCoords.
 
 ===============================================================================*/

Point3f* GameScreen::getMaxCoords( Point3f* pMax, const Point3f* keyCoords, uint8 nCoords ) const
{
	pMax->set( FLT_MIN, FLT_MIN, 0.0f );
	
	for( uint8 i = 0 ; i < nCoords ; ++i )
	{
		if( keyCoords[i].x > pMax->x )
			pMax->x = keyCoords[i].x;
		
		if( keyCoords[i].y > pMax->y )
			pMax->y = keyCoords[i].y;
	}
	return pMax;
}

/*===============================================================================
 
 MÉTODO adjustCamera
 Atualiza a posição e o zoom da câmera para englobar os pontos _p1 e _p2.
 
 ===============================================================================*/

void GameScreen::adjustCamera( Point3f& _p1, Point3f& _p2, float time )
{
	// Calcula o ponto de destino
	Point3f min, max;
	Point3f p1 = GameUtils::isoGlobalToPixels( _p1 );
	Point3f p2 = GameUtils::isoGlobalToPixels( _p2 );
	
	if( p1.x < p2.x )
	{
		min.x = p1.x;
		max.x = p2.x;
	}
	else
	{
		min.x = p2.x;
		max.x = p1.x;
	}
	
	if( p1.y < p2.y )
	{
		min.y = p1.y;
		max.y = p2.y;
	}
	else
	{
		min.y = p2.y;
		max.y = p1.y;
	}
	
	Point3f dest( ( min.x + max.x ) * 0.5f, ( min.y + max.y ) * 0.5f, 0.0f );
	dest = GameUtils::isoPixelsToGlobalY0( dest.x, dest.y );
	
	// Calcula o zoom
	float widthNeeded = ( max.x - min.x );
	float heightNeeded = ( max.y - min.y );
	
	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	float currWidth = ( SCREEN_WIDTH * invZoom );
	float currHeight = ( SCREEN_HEIGHT * invZoom );
	
	float zoomNeededInX = widthNeeded / currWidth;
	float zoomNeededInY = heightNeeded / currHeight;	
	float finalZoom = invZoom * ( zoomNeededInX > zoomNeededInY ? zoomNeededInX : zoomNeededInY );
	
	moveCameraTo( &dest, 1.0f / finalZoom, time );
}

/*===============================================================================
 
 MÉTODO setState
 Modifica o estado da partida.
 
 ================================================================================*/

void GameScreen::setState( MatchState newState )
{
	// Pára de receber os eventos dos acelerômetros
	AccelerometerManager::GetInstance()->stopAccelerometers();
	
	// Gambiarra: O ideal seria ficar dentro do swicth, mas fazemos um teste dentro de pause() ( matchState != MATCH_STATE_PAUSED ) que retornaria sempre false
	// por causa de 'matchState = newState'. Seria possível resolver o caso colocando as linhas 'matchLastState = matchState;' e 'matchState = newState' no final
	// do método, mas aí teríamos que alterar e verificar mto código...	
	if( newState == MATCH_STATE_PAUSED )
	{
#if !( DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
		pause();
		
		matchLastState = matchState;
		matchState = newState;
#endif
		return;
	}
	
	matchLastState = matchState;
	matchState = newState;
	
	switch( matchState )
	{	
		///<DM>
		case MATCH_STATE_SHOWING_REPLAY:
			setState( MATCH_STATE_REPLAY_SHOWN );
			break;

		case MATCH_STATE_REPLAY_SHOWN:
			break;

		case MATCH_STATE_HIDING_REPLAY:
			SetReplayControlVisibility(false);
			pSetEffectControls->Load();
			setState(MATCH_STATE_SHOWING_RESULT);
			break;
		///</DM>

		case MATCH_STATE_SHOWING_RESULT:
			pInfoTab->startShowAnimation();
			break;
			
		case MATCH_STATE_HIDING_INFO:
			pInfoTab->setCloseButtonState( false, GAME_SCREEN_BT_COLOR_UNPRESSED );
			pInfoTab->startHideAnimation();
			break;
			
		case MATCH_STATE_SHOWING_INFO:
			pBtInfo->setVertexSetColor( GAME_SCREEN_BT_COLOR_UNPRESSED );
			pBtInfo->startHideAnimation();
			pBtKick->startHideAnimation();
			pBtFocusOnBall->startHideAnimation();
			break;
			
		case MATCH_STATE_RESULT_SHOWN:
			stateTimeCounter = -1.0f;
			
			///<DM> reseta o replay
			if( gameInfo.gameMode == GAME_MODE_RANKING )
			{
				stateDuration = -1.0f;
				iReplayTabGroup.setPosition( SCREEN_WIDTH, 0.0f );
			}
			///<DM>

			pInfoTab->startUpdateAnimation();
			break;
			
		case MATCH_STATE_BEFORE_FOCUS_ON_BALL:
			// OLD: A transição ficava muito longa
			// pBtInfo->startHideAnimation();
			// pBtKick->startHideAnimation();
			
			setState( MATCH_STATE_FOCUSING_ON_BALL );
			break;
			
		case MATCH_STATE_FOCUSING_ON_BALL:
			pBtFocusOnBall->setVertexSetColor( GAME_SCREEN_BT_COLOR_UNPRESSED );
			moveCameraTo( pBall->getRealPosition(), CAMERA_FOCUS_ON_BALL_DELAY );
			break;
			
		case MATCH_STATE_AFTER_FOCUS_ON_BALL:
			// OLD: A transição ficava muito longa
			// pBtInfo->startShowAnimation();
			// pBtKick->startShowAnimation();
			
			setState( MATCH_STATE_GETTING_READY );
			break;
			
		case MATCH_STATE_GETTING_READY:
		case MATCH_STATE_INFO_SHOWN:
		case MATCH_STATE_PLAYER_KICKING:
		case MATCH_STATE_PLAYER_KICKING_REPLAY:			
		case MATCH_STATE_HIDING_EFFECT_KICK_BT:
			break;
			
		case MATCH_STATE_SETTING_KICK:
		case MATCH_STATE_SETTING_EFFECT:
			pChronometer->pause( false );
			
			// Começa a receber os eventos do acelerômetro
			AccelerometerManager::GetInstance()->startAccelerometers();
			break;
			
		case MATCH_STATE_SHOWING_EFFECT_CONTROLS:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			
				#if DEBUG && IS_CURR_TEST( TEST_PLAYER_KICK )
					sleep( 10 );
				#endif

				pBtKick->startShowAnimation();
				
				float difficultyPercent = getDifficultyPercent();
				
				pChronometer->setTimeInterval( NanoMath::lerp( difficultyPercent, MATCH_SETTING_EFFECT_MAX_TOTAL_TIME, MATCH_SETTING_EFFECT_MIN_TOTAL_TIME ), 0.0f );
				pChronometer->pause( true );
				pChronometer->startShowAnimation();
				
				pSetEffectControls->reset( difficultyPercent, ( currPlay.power - ISO_POWER_MIN_VALUE ) / ( ISO_POWER_MAX_VALUE - ISO_POWER_MIN_VALUE ) );
				pSetEffectControls->startShowAnimation();
				
			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases			
			break;
			
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
			pBtKick->startHideAnimation();

			///<DM>
			pBtPause->startHideAnimation();
			///</DM>

			pChronometer->pause( true );
			pChronometer->startHideAnimation();
			break;
			
		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:
			pBtInfo->startHideAnimation();
			pBtFocusOnBall->startHideAnimation();
			break;
			
		case MATCH_STATE_SHOWING_KICK_CONTROLS:
			pChronometer->setTimeInterval( NanoMath::lerp( getDifficultyPercent(), MATCH_SETTING_DIR_N_PWR_MAX_TOTAL_TIME, MATCH_SETTING_DIR_N_PWR_MIN_TOTAL_TIME ), 0.0f );
			pChronometer->pause( true );
			
			if( gameInfo.cameraAutoAdjust )
			{
				onInterfaceControlAnimCompleted( pBtInfo );
			}
			else
			{
				pBtInfo->startHideAnimation();
				pBtFocusOnBall->startHideAnimation();
			}
			break;
			
		case MATCH_STATE_HIDING_KICK_CONTROLS:
			pChronometer->pause( true );
			pChronometer->startHideAnimation();
			
			pBtKick->startHideAnimation();
			
			pPowerMeter->startHideAnimation();
			pDirectionMeter->startHideAnimation();
			
			pBall->setAuraState( BALL_AURA_STATE_FADING_OUT );
			break;
			
		case MATCH_STATE_PLAYER_RUNNING:
		case MATCH_STATE_PLAYER_RUNNING_REPLAY:
			pPlayer->setListener( this );
			pPlayer->setState( PLAYER_RUNNING );
			break;
			
		case MATCH_STATE_ANIMATING_REPLAY:			
		case MATCH_STATE_ANIMATING_PLAY:
			
			pPlayer->setListener( NULL );
			break;
			
			
		case MATCH_STATE_NEXT_LEVEL:
		case MATCH_STATE_NEXT_TRY:
		case MATCH_STATE_NEXT_TRAINING:
			{
				// Pára o som ambiente, desde que tenhamos torcida
				FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* )APP_DELEGATE;
				
				if( ( pCrowd->getCrowdState() == CROWD_STATE_BIT ) || ( pCrowd->getCrowdState() == CROWD_STATE_FULL ) )
					[hApplication stopAudioAtIndex: SOUND_INDEX_CROWD_AMBIENT];
				
				[hApplication stopAudioAtIndex: SOUND_INDEX_AMBIENT];
				[hApplication stopAudioAtIndex: SOUND_INDEX_CROWD_GOAL];
				[hApplication stopAudioAtIndex: SOUND_INDEX_GOAL];
				[hApplication stopAudioAtIndex: SOUND_INDEX_TOO_BAD];
			}
			// Sem break mesmo

		case MATCH_STATE_GAME_OVER:
			dynamic_cast< AnimatedCamera* >( pCurrCamera )->cancelAnim();
			
			pCurrTransition = pSliceTransition;
			pCurrTransition->reset();
			break;
			
		case MATCH_STATE_TRANSITION:
			pCurrTransition = pCurtainTransition;
			pCurrTransition->reset();
			break;
			
		case MATCH_STATE_FEEDBACK_NEXT_CHALLENGE:
			{
				if( ( gameInfo.currTry == 0 ) && ( gameInfo.gameMode != GAME_MODE_TRAINING ) )
				{
					stateTimeCounter = -1.0f;
					stateDuration = -1.0f;
					feedbackMsgs[ matchState - MATCH_STATE_FEEDBACK_GOAL ]->startShowAnimation();
				}
				else
				{
					stateTimeCounter = GAME_SCREEN_FEEDBACK_DUR_NEXT_LV;
					stateDuration = GAME_SCREEN_FEEDBACK_DUR_NEXT_LV;
				}
				
				// Toca o som ambiente
				FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* )APP_DELEGATE;
				if( ( pCrowd->getCrowdState() == CROWD_STATE_BIT ) || ( pCrowd->getCrowdState() == CROWD_STATE_FULL ) )
					[hApplication playAudioNamed: SOUND_NAME_CROWD_AMBIENT AtIndex: SOUND_INDEX_CROWD_AMBIENT Looping: true];
				
				[hApplication playAudioNamed: SOUND_NAME_AMBIENT AtIndex: SOUND_INDEX_AMBIENT Looping: true];
			}
			break;
			
		case MATCH_STATE_FEEDBACK_GAME_OVER:
			{
				// Não precisamos esperar mais ainda para fazer a animação de Game Over
				stateTimeCounter = GAME_SCREEN_GAMEOVER_TIME;
				stateDuration = GAME_SCREEN_GAMEOVER_TIME;
				
				// Descarrega todos os sons atuais
				FreeKickAppDelegate *hApplication = ( FreeKickAppDelegate* )APP_DELEGATE;
				[hApplication unloadSounds];
				[hApplication removeStateFile];
				// Toca o som de Game Over
				[hApplication playAudioNamed: SOUND_NAME_GAME_OVER AtIndex: SOUND_INDEX_GAME_OVER Looping: false];
			}
			break;
			
		case MATCH_STATE_FEEDBACK_GOAL:
			{
				FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* ) APP_DELEGATE;
				
				if( ( pCrowd->getCrowdState() == CROWD_STATE_BIT ) || ( pCrowd->getCrowdState() == CROWD_STATE_FULL ) )
				{
					///<DM>
					// [hApplication stopAudioAtIndex: SOUND_INDEX_CROWD_AMBIENT];
					[hApplication unloadSoundAtIndex: SOUND_INDEX_CROWD_AMBIENT];					
					///</DM>
					[hApplication playAudioNamed: SOUND_NAME_CROWD_GOAL AtIndex: SOUND_INDEX_CROWD_GOAL Looping: false];
					
				}
				
				[hApplication stopAudioAtIndex: SOUND_INDEX_AMBIENT];
				[hApplication playAudioNamed: SOUND_NAME_GOAL AtIndex: SOUND_INDEX_GOAL Looping: true];
				
				onTryEnded( TRY_RESULT_GOAL );
				
				stateTimeCounter = 0.0f;
				stateDuration = GAME_SCREEN_WAITING_TIME_BEFORE_FEEDBACK;
				
				pBtPause->startHideAnimation();
			}
			break;
			
		case MATCH_STATE_FEEDBACK_TOO_BAD:
			{
				FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* ) APP_DELEGATE;
				
				[hApplication stopAudioAtIndex: SOUND_INDEX_AMBIENT];				
				[hApplication playAudioNamed: SOUND_NAME_TOO_BAD AtIndex: SOUND_INDEX_TOO_BAD Looping: false];
				
				onTryEnded( TRY_RESULT_OUT );
				
				stateTimeCounter = 0.0f;
				stateDuration = GAME_SCREEN_WAITING_TIME_BEFORE_FEEDBACK;
				
				pBtPause->startHideAnimation();
			}
			break;
			
		case MATCH_STATE_HIDING_FEEDBACK:
			for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
			{
				if( feedbackMsgs[i]->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN )
				{
					feedbackMsgs[i]->startHideAnimation();
					break;
				}
			}
			break;

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) )
		case MATCH_STATE_TEST_0:
			break;
#endif
	}
}

/*===============================================================================
 
 MÉTODO onGameOver
 Inicia a transição para a tela de jogo adequada.
 
 ================================================================================*/

void GameScreen::onGameOver( void )
{
	[(( FreeKickAppDelegate* )APP_DELEGATE ) onGameOver: gameInfo.points];
}

/*===============================================================================
 
 MÉTODO pause
 Suspende o processamento dos elementos do jogo e exibe a tela de pause.
 
 ================================================================================*/

void GameScreen::pause( void )
{
	if( matchState != MATCH_STATE_PAUSED )
	{
		// Pára de receber os eventos do acelerômetro
		if( ( matchLastState == MATCH_STATE_SETTING_KICK ) || ( matchLastState == MATCH_STATE_SETTING_EFFECT ) )
			AccelerometerManager::GetInstance()->stopAccelerometers();
		
		// Pausa todos os sons atuais
		FreeKickAppDelegate* hApplication = ( FreeKickAppDelegate* ) APP_DELEGATE;
		[hApplication pauseAllSounds: true];
		
		// Toca o som de pause
		[hApplication playAudioNamed: SOUND_NAME_WHISTLE AtIndex: SOUND_INDEX_WHISTLE Looping: false];
		
		// Exibe a tela de pause
		pBtPause->setVertexSetColor( GAME_SCREEN_BT_COLOR_UNPRESSED );
		[hApplication performTransitionToView: VIEW_INDEX_PAUSE_SCREEN ];
	}
}

/*===============================================================================
 
 MÉTODO unpause
 Retorna para o estado anterior ao estado MATCH_STATE_PAUSED.
 
 ================================================================================*/

void GameScreen::unpause( void )
{
	if( matchState == MATCH_STATE_PAUSED )
	{
		matchState = matchLastState;
		
		// Volta a executar todos os sons
		[(( FreeKickAppDelegate* ) APP_DELEGATE ) pauseAllSounds: false];
		
		if( ( matchState == MATCH_STATE_SETTING_KICK ) || ( matchState == MATCH_STATE_SETTING_EFFECT ) )
		{
			// Volta a receber os eventos do acelerômetro
			AccelerometerManager::GetInstance()->startAccelerometers();
		}
	}
}

/*===============================================================================
 
 MÉTODO updateAnimatingPlay
 Atualiza a partida quando ela está no estado MATCH_STATE_ANIMATING_PLAY ou MATCH_STATE_ANIMATING_REPLAY.
 
 =============================================================================== */

void GameScreen::updateAnimatingPlay( float timeElapsed )
{
	switch( updateMovingObjects( timeElapsed ) )
	{
		case TRY_RESULT_OUT:
#if DEBUG
			LOG( "Try Resul: OUT\n" );
#endif
			
			// Queremos evitar qualquer resíduo de animação da câmera
			dynamic_cast< AnimatedCamera* >( pCurrCamera )->cancelAnim();
			
			setState( matchState == MATCH_STATE_ANIMATING_PLAY ? MATCH_STATE_FEEDBACK_TOO_BAD :  MATCH_STATE_SHOWING_REPLAY );
			break;
			
		case TRY_RESULT_GOAL:
#if DEBUG
			LOG( "Try Resul: GOAL\n" );
#endif
			
			// Queremos evitar qualquer resíduo de animação da câmera
			dynamic_cast< AnimatedCamera* >( pCurrCamera )->cancelAnim();
			
			setState( matchState == MATCH_STATE_ANIMATING_PLAY ? MATCH_STATE_FEEDBACK_GOAL : MATCH_STATE_SHOWING_REPLAY );
			break;
			
		case TRY_RESULT_UNDEFINED:
#if DEBUG
			LOG( "Try Resul: UNDEFINED\n" );
#endif
			break;
	}
}

/*===============================================================================
 
 MÉTODO updateFlags
 Verifica se devemos tocar a animação das bandeiras de escanteio.
 
 ================================================================================*/

bool GameScreen::updateFlags( float timeElapsed )
{
	bool ret = false;
	
	Sprite* pLeftFlag = dynamic_cast< Sprite* >( getObject( OBJ_INDEX_LEFT_FLAG ) );
	
	if( pLeftFlag->getCurrAnimSequence() == GAME_SCREEN_FLAG_ANIM_STOPPED )
	{
		leftFlagTimeCounter += timeElapsed;
		
		if( leftFlagTimeCounter >= GAME_SCREEN_FLAGS_TIME_BETWEEN_ANIM )
		{
			float percent = Random::GetFloat();
			if( percent >= 0.63f && percent <= 0.93f )
			{
				pLeftFlag->setAnimSequence( GAME_SCREEN_FLAG_ANIM_ANIMATING, false );
				dynamic_cast< Sprite* >( getObject( OBJ_INDEX_LEFT_FLAG_SHADOW ) )->setAnimSequence( GAME_SCREEN_FLAG_ANIM_ANIMATING, false );
			}
			else
			{
				leftFlagTimeCounter = 0.0f;
			}
		}
	}
	else
	{
		ret |= pLeftFlag->update( timeElapsed );
		dynamic_cast< Sprite* >( getObject( OBJ_INDEX_LEFT_FLAG_SHADOW ) )->update( timeElapsed );
	}
	
	Sprite* pRightFlag = dynamic_cast< Sprite* >( getObject( OBJ_INDEX_RIGHT_FLAG ) );
	
	if( pRightFlag->getCurrAnimSequence() == GAME_SCREEN_FLAG_ANIM_STOPPED )
	{
		rightFlagTimeCounter += timeElapsed;
		
		if( rightFlagTimeCounter >= GAME_SCREEN_FLAGS_TIME_BETWEEN_ANIM )
		{
			float percent = Random::GetFloat();
			if( percent >= 0.27 && percent <= 0.57f )
			{
				pRightFlag->setAnimSequence( GAME_SCREEN_FLAG_ANIM_ANIMATING, false );
				dynamic_cast< Sprite* >( getObject( OBJ_INDEX_RIGHT_FLAG_SHADOW ) )->setAnimSequence( GAME_SCREEN_FLAG_ANIM_ANIMATING, false );
			}
			else
			{
				rightFlagTimeCounter = 0.0f;
			}
		}
	}
	else
	{
		ret |= pRightFlag->update( timeElapsed );
		dynamic_cast< Sprite* >( getObject( OBJ_INDEX_RIGHT_FLAG_SHADOW ) )->update( timeElapsed );
	}
	
	return ret;
}

/*===============================================================================
 
 MÉTODO updateMovingObjects
 Atualiza todos os objetos animados da cena.
 
 ================================================================================*/

TryResult GameScreen::updateMovingObjects( float timeElapsed )
{
	TryResult ret = TRY_RESULT_UNDEFINED;
	uint8 previousBallState = pBall->getState();
	
	pBall->update( timeElapsed );
	
	
	pKeeper->update( timeElapsed );
	pPlayer->update( timeElapsed );
	
	pBarrier->onBallGettingClose( pBall );
	pBarrier->update( timeElapsed );
	
	updateFlags( timeElapsed );
	
	// Checa colisão da bola com os elementos da cena
	if( checkBallCrowdCollision() )
		vibrate();
	
	if( keeperMayDefend() )
	{
		if( ( pBall->checkPlayerCollision( pKeeper, COLLISION_KEEPER ) == BALL_STATE_REJECTED_KEEPER ) && ( previousBallState != BALL_STATE_REJECTED_KEEPER ) )
			[(( FreeKickAppDelegate* ) APP_DELEGATE )playAudioNamed: SOUND_NAME_KEEPER AtIndex: SOUND_INDEX_KEEPER Looping: false];
	}
	
	for( uint8 i = 0 ; i < pBarrier->getNumberOfPlayers() ; ++i )
	{
		BarrierPlayer *pBarrierPlayer = pBarrier->getPlayer( i );
		if( ( pBall->checkPlayerCollision( pBarrierPlayer, COLLISION_BARRIER ) == BALL_STATE_REJECTED_BARRIER ) && ( previousBallState != BALL_STATE_REJECTED_BARRIER ) )
		{
			uint8 audioName, audioIndex;
			pBarrierPlayer->getCollisionSoundNameAndIndex( &audioName, &audioIndex );
			
			FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* ) APP_DELEGATE;
			
			if( ![hAppDelegate isPlayingAudioWithIndex: audioIndex] )
				[hAppDelegate playAudioNamed: audioName AtIndex: audioIndex Looping: false];
			///<DM> impede que a bola rebata em mais de um jogador da barreira, para evitar glitches
			i= pBarrier->getNumberOfPlayers();
			///</DM>
		}
	}
	
	// Pode ter colidido com o goleiro ou com a barreira, então obtém novamente o estado atual da bola
	uint8 currentBallState = pBall->getState();
	
	// Verifica se colidiu com o alvo
	if( ( currentBallState == BALL_STATE_GOAL ) || ( currentBallState == BALL_STATE_POST_GOAL ) )
	{
#warning Parte do princípio que a etapa de atualização é muito pequena! O certo seria voltar a bola no tempo e conferir se acertou o alvo com hitBullsEye
		
		if( pBallTarget->hitBullsEye( &ballDestPos ) && ( pBallTarget->getState() != BALL_TARGET_HIT ))
			pBallTarget->setState( BALL_TARGET_HIT );
	}
	pBallTarget->update( timeElapsed );
	
	// Movimenta a câmera apenas se a bola está em movimento e dentro de campo
	Point3f ballRealPos = *( pBall->getRealPosition() );
	
#if ! IS_CURR_TEST( TEST_BALL_ROTATION )
	
	if( matchState == MATCH_STATE_ANIMATING_PLAY || matchState == MATCH_STATE_ANIMATING_REPLAY )
	{
		AnimatedCamera* pSceneCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );
		
		if( ( currentBallState != BALL_STATE_STOPPED ) && ( currentBallState != BALL_STATE_OUT ) )
		{
#if IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS )
			moveCameraTo( pKeeper->getRealPosition(), 0.0f );
#elif IS_CURR_TEST( TEST_PLAYER_KICK ) || IS_CURR_TEST( TEST_PLAYER_MIRROR )
			moveCameraTo( pPlayer->getRealPosition(), CAMERA_FOLLOW_DEFAULT_TIME );
#else
			// TODOO : Implementar no update : câmera seguir a bola atrasada
			if( !pSceneCamera->isAnimating() )
				moveCameraTo( &ballRealPos, CAMERA_FOLLOW_DEFAULT_TIME );
			else
				pSceneCamera->changeCurrAnimDest( &ballRealPos );				
#endif
			
			if (movingCamera)
				pSceneCamera->update( timeElapsed );
			
			// Garante que não extrapolou os limites estabelecidos
			onMoveCamera( 0.0f, 0.0f );
		}
		else
		{
			pSceneCamera->cancelAnim();
		}
	}
	
#else
	
	moveCameraTo( &ballRealPos, 0.0f );
	pCurrCamera->zoom( ZOOM_MAX );
	
#endif
	
	if( currentBallState != previousBallState )
	{
		switch( currentBallState )
		{
			case BALL_STATE_POST_BACK:
			case BALL_STATE_POST_OUT:
				vibrate();
				[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_POST AtIndex: SOUND_INDEX_POST Looping: false];
				ret = TRY_RESULT_OUT;
				break;
				
			case BALL_STATE_REJECTED_BARRIER:
			case BALL_STATE_REJECTED_KEEPER:
			{
				vibrate();
				
			}
			case BALL_STATE_OUT:
				ret = TRY_RESULT_OUT;
				break;
				
			case BALL_STATE_POST_GOAL:
				vibrate();
				[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_POST AtIndex: SOUND_INDEX_POST Looping: false];
				
			case BALL_STATE_GOAL:
				ret = TRY_RESULT_GOAL;
				break;
		}
	}
	
	updateZOrder();
	return ret;
}

/*===============================================================================
 
 MÉTODO keeperMayDefend
 Indica se o goleiro pode defender a bola
 
 ================================================================================*/

bool GameScreen::keeperMayDefend( void ) const
{
	// Se não foi um chute no alvo ou estamos no modo treino e o goleiro está
	// exatamente na frente do alvo, damos a chance de o goleiro defender.
	bool condition2 = false;
	if( gameInfo.gameMode == GAME_MODE_TRAINING )
	{
		Point3f keeperInitPos = pKeeper->getInitialPosition();
		NanoRect keeperRect( keeperInitPos.x - ( REAL_KEEPER_WIDTH * 0.5f ), keeperInitPos.y + REAL_KEEPER_HEIGHT, REAL_KEEPER_WIDTH, REAL_KEEPER_HEIGHT );
		NanoRect ballTargetRect( pBallTarget->getRealPosition()->x - BULLS_EYE_DISTANCE, pBallTarget->getRealPosition()->y + BULLS_EYE_DISTANCE, BULLS_EYE_DISTANCE * 2.0f, BULLS_EYE_DISTANCE * 2.0f );
		
		condition2 = Utils::CheckRectCollision( &keeperRect, &ballTargetRect );
	}
	
	return !pBallTarget->hitBullsEye( &ballDestPos ) || condition2;
}

/*===============================================================================
 
 MÉTODO updateZOrder
 Atualiza a ordem de desenho dos objetos da cena.
 
 ================================================================================*/

void GameScreen::updateZOrder( void )
{
	// Ordena os elementos da cena de acordo com a sua ordem z
	std::sort( realPosSortables.begin(), realPosSortables.end(), RealPosOwner::StdSortRealPosComparer );
	
	// Acerta a ordem z dos elementos da cena
	// OBS: Opa! Cast perigoso! Ser um RealPosOwner não garante que também é um Object!
	for( uint8 i = 0 ; i < realPosSortables.size() ; ++i )
		setObjectZOrder( dynamic_cast< Object* >( realPosSortables[i] ), FIRST_REAL_POS_OWNER + i );
	
	// Conserta o problema que temos com a profundidade do gol
	Point3f ballRealPos = *( pBall->getRealPosition() );
	
	// Se está dentro do gol, deve ser desenhada acima de GoalBottom independentemente do z
	if( ( ballRealPos.x >= ( -REAL_GOAL_WIDTH * 0.5f )) && ( ballRealPos.x <= ( REAL_GOAL_WIDTH * 0.5f )) && ( ballRealPos.z < 0.0f ) && ( ballRealPos.z > REAL_GOAL_DEPTH ) )
		setObjectZOrder( pBall, getObjectZOrder( getObject( OBJ_INDEX_GOAL_BOTTOM ) ) );
	
	// Se está fora do gol, à direita, deve ser desenhada acima de GoalTop independentemente do z
	else if( ballRealPos.x > ( REAL_GOAL_WIDTH * 0.5f ) )
		setObjectZOrder( pBall, getObjectZOrder( getObject( OBJ_INDEX_GOAL_TOP ) ) );
}

/*===============================================================================
 
 MÉTODO checkBallCrowdCollision
 Checa colisão da bola com a torcida.
 
 ================================================================================*/

bool GameScreen::checkBallCrowdCollision( void )
{
	Point3f ballRealPos = *( pBall->getRealPosition() );
	
	// Se a bola invadiu a torcida, traz ela de volta para o campo
	
	// Trata colisão com os quads de colisão da torcida
	int8 crowdNCollisionQuads = pCrowd->getNCollisionQuads();
	
	for( int8 i = 0 ; i < crowdNCollisionQuads ; ++i )
	{
		if( pCrowd->getLastCollisionQuadIndex() != i )
		{
			const CollisionQuad *pQuad = pCrowd->getCollisionQuad( i );
			
			
			if( pQuad->distanceTo( &ballRealPos ) <= REAL_BALL_RADIUS)
			{				
				if( pQuad->isInsideQuad( &ballRealPos, REAL_BALL_RADIUS ))
				{					
#if DEBUG
					LOG( "Colidiu com a torcida em %d\n",i );
#endif
					
					// Colidiu com o plano do quad da torcida
					pCrowd->setLastCollisionQuadIndex( i );
					pBall->setCollisionWithQuad( pQuad );
					
					return true;
				}
			}
		}
	}
	
	
	if( ballRealPos.z < pCrowd->getCollisionTopZ())	
		ballRealPos.z = pCrowd->getCollisionTopZ();
	
	if( ballRealPos.x < -( REAL_FIELD_WIDTH * 0.5f ) )
	{
		if( ballRealPos.x < pCrowd->getCollisionLeftX() )
			ballRealPos.x = pCrowd->getCollisionLeftX();
	}
	else if( ballRealPos.x > ( REAL_FIELD_WIDTH * 0.5f ) )
	{
		if( ballRealPos.x > pCrowd->getCollisionRightX() )
			ballRealPos.x = pCrowd->getCollisionRightX();
	}
	
	pBall->setRealPosition( ballRealPos.x, ballRealPos.y, ballRealPos.z );
	
	return false;
}

/*===============================================================================
 
 MÉTODO onOGLTransitionEnd
 Método chamado quando a transição de fases termina.
 
 ================================================================================*/

void GameScreen::onOGLTransitionEnd( void )
{
	switch( matchState )
	{
		case MATCH_STATE_GAME_OVER:
			setState( MATCH_STATE_FEEDBACK_GAME_OVER );
			break;
			
		case MATCH_STATE_NEXT_TRY:
			resetLevel();
			setState( MATCH_STATE_TRANSITION );
			break;

		///<DM>
		case MATCH_STATE_CONTINUING:			
			lastBallInitialPosition = gameInfo.iSavedBallPosition;

			// Anti-hack: Garante que carregamos uma posição válida
			fixBallPos( lastBallInitialPosition );

			onLoadLevel( &lastBallInitialPosition );

			setState( MATCH_STATE_TRANSITION );
			break;
		///</DM>

		case MATCH_STATE_NEXT_LEVEL:
			onLoadNewLevel();

			///<DM>
			if( gameInfo.gameMode == GAME_MODE_RANKING )
			{
				gameInfo.iSavedBallPosition = lastBallInitialPosition;
				gameInfo.iSavedTargetPosition = *( pBallTarget->getRealPosition() );
			}
			///</DM>

			setState( MATCH_STATE_TRANSITION );
			break;
			
		case MATCH_STATE_NEXT_TRAINING:
			resetTraining();
			setState( MATCH_STATE_TRANSITION );
			break;
			
		case MATCH_STATE_TRANSITION:
#if IS_CURR_TEST( TEST_SET_EFFECT )
			setState( MATCH_STATE_SETTING_EFFECT );
#else
			setState( MATCH_STATE_FEEDBACK_NEXT_CHALLENGE );
#endif
			break;
	}
}

/*===============================================================================
 
MÉTODO resetInfoTab
	Reseta o mostrador de informações do jogo.
 
=================================================================================*/

void GameScreen::resetInfoTab( void )
{
	pInfoTab->updateScore( true );
	///<DM>
	//	gameInfo.levelScore = 0.0f;
	//	gameInfo.currTry = 0;
	//	for( uint8 i = 0 ; i < FOUL_CHALLENGE_TRIES_PER_FOUL ; ++i )
	//		gameInfo.tries[ i ] = TRY_RESULT_NOT_USED;
	///</DM>
	pInfoTab->updateTries();
}

/*===============================================================================
 
MÉTODO resetTraining
	Reseta o treino atual.
 
=================================================================================*/

void GameScreen::resetTraining( void )
{
	gameInfo.points = 0;
	resetTries();
	resetInfoTab();
	resetLevel();
}

/*===============================================================================
 
MÉTODO resetTries
	Restaura as tentativas que o jogador possui para passar de fase.
 
=================================================================================*/

///<DM>
void GameScreen::resetTries( void )
{
	gameInfo.levelScore = 0.0f;
	gameInfo.currTry = 0;
	for( uint8 i = 0 ; i < FOUL_CHALLENGE_TRIES_PER_FOUL ; ++i )
		gameInfo.tries[ i ] = TRY_RESULT_NOT_USED;
	
}
///</DM>

/*===============================================================================
 
 MÉTODO resetLevel
 Reseta a fase atual.
 
 ================================================================================*/

// OBS: Poderíamos fazer isto de um jeito mais elegante utilizando onLoadNewLevel...
void GameScreen::resetLevel( bool replay )
{
	pCrowd->setLastCollisionQuadIndex( -1 );

	// Apaga as mensagens de feedback
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
		feedbackMsgs[i]->setVisible( false );
	
	if( replay )
	{
		pBall->reset(&gameInfo.iSavedBallPosition);
		
		AnimatedCamera* pSceneCamera = dynamic_cast< AnimatedCamera* >( pCurrCamera );	
		pSceneCamera->setPosition( pBall->getPosition() );
		
		// Posiciona o goleiro de acordo com a posição da bola
		pKeeper->reset();
		pKeeper->prepare( &lastBallInitialPosition, pBarrier->getNumberOfPlayers() );		
		
		// Testa se a bola acertará o alvo. Caso acerte, força o goleiro a não defender o chute
		switch( pBall->getLastPlayResult() )
		{
			case BALL_STATE_REJECTED_BARRIER:
				pKeeper->setBallCollisionTest( false );	
				break;
				
			case BALL_STATE_REJECTED_KEEPER:
				pBarrier->setBallCollisionTest( false );	
				break;
				
			case BALL_STATE_POST_OUT:				
			case BALL_STATE_OUT:
			case BALL_STATE_POST_BACK:
			case BALL_STATE_GOAL:
			case BALL_STATE_POST_GOAL:				
				pKeeper->setBallCollisionTest( false );	
				pBarrier->setBallCollisionTest( false );	
				break;

#if DEBUG
			default: 
				// Nunca deveria chegar aqui!
				LOG( "---->ERRO! ESTADO INVALIDO DA BOLA NO REPLAY!!!!!\n" );
#endif
		}
	}
	else
	{
		// Reseta a bola
		pBall->reset( &lastBallInitialPosition );
		pBall->setAuraState( BALL_AURA_STATE_LOOPING );
		
		// Posiciona o goleiro de acordo com a posição da bola
		pKeeper->reset();
		pKeeper->prepare( &lastBallInitialPosition, pBarrier->getNumberOfPlayers() );
		
		// Posiciona a câmera na bola
		adjustCamera( 0.0f );				
		
		// Esconde todos os controles
		hideInterfaceControls();		
	}
	
	// Ativa a animação correta do alvo
	pBallTarget->setState( BALL_TARGET_UNHIT );
	
	// Posiciona o jogador em relação à bola
	pPlayer->reset( &lastBallInitialPosition );
	
	// OBS : Aqui não funciona chamar pBarrier->prepare( lastBallInitialPosition ), pois este método
	// irá remontar a barreira
	for( uint8 i = 0 ; i < pBarrier->getNumberOfPlayers() ; ++i )
	{
		BarrierPlayer *pPlayer = pBarrier->getPlayer( i );
		pPlayer->reset();
		pPlayer->prepare( lastBallInitialPosition );
	}
	
	// Garante que todos os elementos que podem trocar de ordem Z com a bola estão corretos
	updateZOrder();
	
	// Reseta as variáveis de controle
	aimDraggingTouch = -1;
	ballDraggingTouch = -1;
	fieldDraggingTouch = -1;
	buttonPressingTouch = -1;
	vibrated = false;
	movingCamera = false;
	cameraMovement.set();
	ballDestPos.set();
	
	if( replay )
		setState( MATCH_STATE_PLAYER_RUNNING_REPLAY );
}

/*===============================================================================
 
MÉTODO onLoadNewLevel
	Carrega uma nova fase.
 
=================================================================================*/

//<DM>
void GameScreen::onLoadNewLevel( void )
{
	if( gameInfo.gameMode == GAME_MODE_RANKING )
	{
		// Incrementa o número da fase atual
		++gameInfo.currLevel;
		if( gameInfo.currLevel > MAX_LEVEL )
			gameInfo.currLevel = MAX_LEVEL;
	}

	resetTries();
	onLoadLevel();
}
//</DM>

/*===============================================================================
 
MÉTODO onLoadLevel
	Carrega uma determinada fase.
 
=================================================================================*/

void GameScreen::onLoadLevel( const Point3f *pUsingBallPos )
{
	pBall->setState( BALL_STATE_STOPPED );
	
	// Esconde todos os controles
	for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
		feedbackMsgs[i]->setVisible( false );
	
	hideInterfaceControls();
	
	Point3f ballRealPos;
	if( gameInfo.gameMode == GAME_MODE_RANKING )
	{
		//<DM> 
		// OLD: Agora isso é feito em onLoadNewLevel, que é diferente deste método ( onLoadLevel )
		// Incrementa o número da fase atual
//		++gameInfo.currLevel;
//		if( gameInfo.currLevel > MAX_LEVEL )
//			gameInfo.currLevel = MAX_LEVEL;
		//</DM>

		// Reseta as variáveis de controle
		resetInfoTab();
		pInfoTab->updateLevel();
		
		// Coloca a bola em uma posição aleatória
		if( pUsingBallPos == NULL )
		{
			pBall->reset( getRandomBallPos( &ballRealPos ) );
		}
		else		
		{
			pBall->reset( pUsingBallPos );
			ballRealPos = *pUsingBallPos;
		}	
		lastBallInitialPosition = *( pBall->getRealPosition() );
		
		// Posiciona a mira levando em conta a posição da bola
		pBallTarget->reset( &ballRealPos );
		
		// OBS: Não usar setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN ) aqui!!!!!!!!
		feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_NEXT_LV ]->setVisible( true );

		///<DM>		
		// OLD: Agora a arquibancada do jogo já começa com gente
		// Conforme o jogador vai passando de fase, a arquibancada vai enchendo
		// int8 nextCrowdState = static_cast< int8 >( NanoMath::lerp( NanoMath::clamp( gameInfo.currLevel, 0, FIRST_LEVEL_WITH_FULL_CROWD ) / FIRST_LEVEL_WITH_FULL_CROWD, CROWD_STATE_EMPTY, CROWD_STATE_FULL ) );
		int8 nextCrowdState = static_cast< int8 >( NanoMath::lerp( NanoMath::clamp( gameInfo.currLevel, 0, FIRST_LEVEL_WITH_FULL_CROWD ) / FIRST_LEVEL_WITH_FULL_CROWD, CROWD_STATE_BIT, CROWD_STATE_FULL ) );
		///</DM>

		// Evita uma possível imprecisão de float
		if( nextCrowdState > CROWD_STATE_FULL )
			nextCrowdState = CROWD_STATE_FULL;

		pCrowd->setCrowdState( static_cast< CrowdState >( nextCrowdState ) );
		
		// Monta a barreira
		///<DM>
		if( matchState != MATCH_STATE_CONTINUING )
		{
			pBarrier->reset( gameInfo.gameMode, ballRealPos, gameInfo );
		}
		else
		{
			pBarrier->restoreBarrier( gameInfo.barrierPlayersVector, gameInfo.barrierSize );
			pBarrier->prepare( lastBallInitialPosition );
		}
		///</DM>
	}
	else // if( gameInfo.gameMode == GAME_MODE_TRAINING )
	{
		// Coloca a bola ou na última posição onde foi cobrada uma falta ou na marca do penâlti, caso seja a primeira tentativa do modo treino
#if DEBUG && IS_CURR_TEST( TEST_BALL_SHADOW )
		ballRealPos.set( 0.0f, 0.0f, 5.0f );
		pBallTarget->setVisible( false );
#elif DEBUG && IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
		ballRealPos.set();
		pBallTarget->setVisible( false );
#else
		Point3f zero;
		if( lastBallInitialPosition == zero )
		{
			ballRealPos.set( 0.0f, 0.0f, REAL_PENALTY_TO_GOAL );
			lastBallInitialPosition = ballRealPos;
		}
		else
		{
			ballRealPos = lastBallInitialPosition;
		}
#endif
		
		pBall->reset( &ballRealPos );
		
		// Posiciona a mira no centro do gol provisoriamente até que o jogador defina sua posição
		pBallTarget->reset();
		
		// No treino, a arquibancada sempre estará vazia
		pCrowd->setCrowdState( CROWD_STATE_EMPTY );
		
		// Monta a barreira
		pBarrier->reset( gameInfo.gameMode, ballRealPos, gameInfo );
	}
	
#if DEBUG
	LOG_FREE_MEM( "Carregou estado da torcida" );
#endif
	
	// Ativa a aura da bola
#if DEBUG && ( IS_CURR_TEST( TEST_BALL_SHADOW ) || IS_CURR_TEST( TEST_BALL_INSIDE_GOAL ) )
	pBall->setAuraState( BALL_AURA_STATE_HIDDEN );
#else
	pBall->setAuraState( BALL_AURA_STATE_LOOPING );
#endif
	
	// Se está dentro da grande área, não tem barreira
	if(( ballRealPos.z < REAL_BIG_AREA_DEPTH ) && ( ballRealPos.x > ( -REAL_BIG_AREA_WIDTH * 0.5f )) && ( ballRealPos.x < ( REAL_BIG_AREA_WIDTH * 0.5f )) )
		pBarrier->undoCurrentBarrier( gameInfo.gameMode );
	
	// Posiciona o goleiro de acordo com a posição da bola
	pKeeper->reset();
	pKeeper->prepare( &ballRealPos, pBarrier->getNumberOfPlayers() );
	
	// Posiciona o jogador em relação à bola
	pPlayer->reset( &ballRealPos );
	
	// Garante que todos os elementos que podem trocar de ordem Z com a bola estão corretos
	updateZOrder();
	
	// Posiciona a câmera
	adjustCamera( 0.0f );
	
	// Reseta as variáveis de controle
	aimDraggingTouch = -1;
	ballDraggingTouch = -1;
	fieldDraggingTouch = -1;
	buttonPressingTouch = -1;
	vibrated = false;
	movingCamera = false;
	cameraMovement.set();
	ballDestPos.set();
	
#if DEBUG && ( IS_CURR_TEST( TEST_BALL_SHADOW ) || IS_CURR_TEST( TEST_BALL_INSIDE_GOAL ) )
	pPlayer->setVisible( false );
	pKeeper->setVisible( false );
#endif
}

/*===============================================================================
 
 MÉTODO getRandomBallPos
 Obtém uma posição aleatória válida para a bola.
 
 ================================================================================*/

Point3f* GameScreen::getRandomBallPos( Point3f* pOut ) const
{
	pOut->x = Random::GetFloat( FOUL_CHALLENGE_LEFT_LIMIT, FOUL_CHALLENGE_RIGHT_LIMIT );
	pOut->y = 0.0f;
	pOut->z = Random::GetFloat( FOUL_MIN_Z_DISTANCE, FOUL_CHALLENGE_MAX_Z );
	
	// Verifica se caiu dentro da grande área
	if( ( pOut->z <= REAL_BIG_AREA_DEPTH ) && ( pOut->x > -( REAL_BIG_AREA_WIDTH * 0.5f ) ) && ( pOut->x < ( REAL_BIG_AREA_WIDTH * 0.5f ) ) )
	{
		// OLD : Agora, no modo Endless, nunca pode bater de dentro da grande área
		//		// Verifica se ainda pode bater de dentro da grande área
		//		float currLevel = static_cast< float >( NanoMath::clamp( gameInfo.currLevel, 0.0f, MATCH_MAX_LEVEL_BALL_POS_INSIDE_BIG_AREA ) );
		//		float chance = NanoMath::lerp( currLevel / MATCH_MAX_LEVEL_BALL_POS_INSIDE_BIG_AREA, MATCH_MAX_CHANCE_BALL_POS_INSIDE_BIG_AREA, MATCH_MIN_CHANCE_BALL_POS_INSIDE_BIG_AREA );
		//
		//		if( NanoMath::fgeq( chance, MATCH_MIN_CHANCE_BALL_POS_INSIDE_BIG_AREA ) )
		//		{
		//			// Verifica se está dentro da pequena área
		//			if( ( pOut->z <= REAL_SMALL_AREA_DEPTH ) && ( pOut->x > -( REAL_SMALL_AREA_WIDTH * 0.5f ) ) && ( pOut->x < ( REAL_SMALL_AREA_WIDTH * 0.5f ) ) )
		//			{
		//				fixBallPosIfInsideArea( *pOut, -( REAL_SMALL_AREA_WIDTH * 0.5f ), ( REAL_SMALL_AREA_WIDTH * 0.5f ), 0.0f, REAL_SMALL_AREA_DEPTH );
		//			}
		//		}
		//		else
		//		{
		//			fixBallPosIfInsideArea( *pOut, -( REAL_BIG_AREA_WIDTH * 0.5f ), ( REAL_BIG_AREA_WIDTH * 0.5f ), 0.0f, REAL_BIG_AREA_DEPTH );
		//		}
		
		fixBallPosIfInsideArea( *pOut, -( REAL_BIG_AREA_WIDTH * 0.5f ), ( REAL_BIG_AREA_WIDTH * 0.5f ), 0.0f, REAL_BIG_AREA_DEPTH );
	}
	return pOut;
}

/*===============================================================================
 
 MÉTODO fixBallPosIfInsideArea
 Conserta a posição da bola no modo treino.
 
 ================================================================================*/

void GameScreen::fixBallPosIfInsideArea( Point3f& pos, float minX, float maxX, float minZ, float maxZ ) const
{
	Point3f dir = pos;
	dir.normalize();
	
	float halfAreaWidth = ( maxX - minX ) * 0.5f;
	
	if( NanoMath::fltn( pos.x, 0.0f ) )
	{
		pos.z = fabsf( tanf( NanoMath::degreesToRadians( dir.getAngle( 1.0f, 0.0f, 0.0f ) ) ) ) * halfAreaWidth;
		
		if( pos.z > maxZ )
		{
			pos.z = maxZ;
			pos.x = -fabsf( tanf( NanoMath::degreesToRadians( dir.getAngle( 0.0f, 0.0f, 1.0f ) ) ) * maxZ );
		}
		else
		{
			pos.x = minX;
		}
	}
	else if( NanoMath::fgtn( pos.x, 0.0f ) )
	{
		pos.z = fabsf( tanf( NanoMath::degreesToRadians( dir.getAngle( -1.0f, 0.0f, 0.0f ) ) ) ) * halfAreaWidth;
		
		if( pos.z > maxZ )
		{
			pos.z = maxZ;
			pos.x = fabsf( tanf( NanoMath::degreesToRadians( dir.getAngle( 0.0f, 0.0f, 1.0f ) ) ) * maxZ );
		}
		else
		{
			pos.x = maxX;
		}
	}
	else
	{
		pos.x = 0.0f;
		pos.z = maxZ;
	}
}

/*====================================================================================
 
 MÉTODO getDifficultyLevel
 Obtém qual a fase em termos de dificuldade (o jogador pode atingir aa última fase
 em termos de dificuldade mas continuar jogando).
 
 ======================================================================================*/

int16 GameScreen::getDifficultyLevel( int16 level ) const
{
	return level > MAX_DIFFICULTY_LEVEL ? MAX_DIFFICULTY_LEVEL : level;
}

/*====================================================================================
 
 MÉTODO onTryEnded
 Atualiza a pontuação do jogador e o resultado da jogada.
 
 ======================================================================================*/

void GameScreen::onTryEnded( TryResult currTryResult )
{
#warning Parte do princípio que a etapa de atualização é muito pequena! O certo seria voltar a bola no tempo e conferir a pontuação com getKickScore()
	
	// Só por garantia
	if( ( matchState == MATCH_STATE_FEEDBACK_GOAL ) || ( matchState == MATCH_STATE_FEEDBACK_TOO_BAD ) )
	{
		// Armazena os pontos ganhos
		uint32 kickScore = getKickScore();
		gameInfo.points += kickScore;

		if( gameInfo.gameMode == GAME_MODE_RANKING )
		{
			gameInfo.levelScore += kickScore;
			
			// Se o jogador atingiu a meta e ainda restam chutes, fornece o bônus para cada
			// chute restante
			int32 levelMaxPts = GameUtils::GetLevelScoreTarget( getDifficultyLevel( gameInfo.currLevel ) );
			if( gameInfo.levelScore >= levelMaxPts )
			{
				// Deixamos que levelScore fique maior do que levelMaxPts para reforçar a idéia de que
				// o jogador passou de fase
				//gameInfo.levelScore = levelMaxPts;
				
				if( ( gameInfo.currTry + 1 ) < FOUL_CHALLENGE_TRIES_PER_FOUL )
					gameInfo.points += SCORE_BONUS_PER_UNUSED_TRY * ( FOUL_CHALLENGE_TRIES_PER_FOUL - ( gameInfo.currTry + 1 ) );
			}
		}
		
		// Incrementa o contador de tentativas
		gameInfo.tries[ gameInfo.currTry++ ] = currTryResult;
		
		// Atualiza a interface
		pInfoTab->updateTries();
	}
}

/*====================================================================================
 
 MÉTODO onResultShown
 Verifica se este conseguiu passar de fase. Altera o estado da partida de acordo
 com o resultado obtido.
 
 ======================================================================================*/

void GameScreen::onResultShown( void )
{
	if( gameInfo.gameMode == GAME_MODE_TRAINING )
	{
		setState( MATCH_STATE_NEXT_TRAINING );
	}
	else // if( gameInfo.gameMode == GAME_MODE_RANKING )
	{
		if( gameInfo.levelScore >= GameUtils::GetLevelScoreTarget( getDifficultyLevel( gameInfo.currLevel ) ) )
		{
			setState( MATCH_STATE_NEXT_LEVEL );
		}
		else
		{
			if( gameInfo.currTry >= FOUL_CHALLENGE_TRIES_PER_FOUL )
				setState( MATCH_STATE_GAME_OVER );
			else
				setState( MATCH_STATE_NEXT_TRY );
		}
	}
}

/*===============================================================================
 
 MÉTODO getKickScore
 Calcula a pontuação obtida com o último chute.
 
 ===============================================================================*/

uint32 GameScreen::getKickScore( void ) const
{
	float points = SCORE_MIN_POINTS_PER_KICK;
	BallState playResult = pBall->getLastPlayResult();
	
	// O cálculo abaixo fica furado quando a bola bate na barreira, já que levamos sua posição para a linha de fundo.
	// Ou seja, o jogador ganharia mais pontos do que SCORE_AFTER_BARRIER_POINTS
	if( playResult != BALL_STATE_REJECTED_BARRIER )
	{
		Point3f ballTargetPos = *( pBallTarget->getRealPosition() );
		
		// OLD: Assim ficaria errado, pois a bola pode ter se locomovido antes de chamarmos getKickScore(). O correto
		// é obtermos a posição de definição da jogada
		// Point3f ballPos = *( pBall->getRealPosition() );
		Point3f ballPos = *( pBall->getLastPositionAtGoal() );
		
		// Desconsidera-se a posição z para efeito de cálculo dos pontos (leva a bola para a linha de fundo)
		ballPos.z = 0.0f;
		
		float distanceModule = ( ballTargetPos - ballPos ).getModule() - BULLS_EYE_TOLERANCE;
		if ( distanceModule < 0.0f )
			distanceModule = 0.0f;
		
		if( distanceModule <= SCORE_MAX_DISTANCE )
		{
			// OLD
			// // Linear
			// points += NanoMath::lerp( distanceModule / SCORE_MAX_DISTANCE, SCORE_PERFECT_SHOOT_POINTS, 0.0f );
			
			// Quadrática
			// Pontos:
			// - x = Distância do alvo
			// - y = Pontos ganhos
			Point3f best( 0.0f, SCORE_PERFECT_SHOOT_POINTS );
			Point3f worst( SCORE_MAX_DISTANCE, 0.0f );
			
			Point3f control1( 0.0f, 0.0f );
			Point3f control2( ( SCORE_MAX_DISTANCE / 100.0f ) * 10.0f, 0.0f );
			
			// distanceModule - ( BULLS_EYE_DISTANCE / 3 ) => Torna mais fácil atingir o centro do alvo e ganhar o máximo de pontos por um chute
			points += BezierCurve( &best, &control1, &control2, &worst ).getYAt( NanoMath::clamp( distanceModule - ( BULLS_EYE_DISTANCE / 3 ), 0.0f, SCORE_MAX_DISTANCE ) / SCORE_MAX_DISTANCE );
		}
	}
	
	switch( playResult )
	{
		case BALL_STATE_GOAL:
		case BALL_STATE_POST_GOAL:
			points += SCORE_GOAL_POINTS + SCORE_AFTER_BARRIER_POINTS;
			break;
			
		case BALL_STATE_POST_BACK:
			points += SCORE_POST_POINTS + SCORE_AFTER_BARRIER_POINTS;
			break;
			
		case BALL_STATE_REJECTED_KEEPER:
			points += SCORE_AFTER_BARRIER_POINTS;
		case BALL_STATE_REJECTED_BARRIER:
			break;
			
		case BALL_STATE_POST_OUT:
			points += SCORE_POST_POINTS;
		case BALL_STATE_OUT:
			points += SCORE_AFTER_BARRIER_POINTS;
			points *= SCORE_OUT_FACTOR;
			break;
	}
	
	// Só por garantia (alguém pode ter configurado os defines de uma maneira incorreta)
	uint32 res = static_cast< uint32 >( round( points ) );
	return res > SCORE_MAX_POINTS_PER_KICK ? SCORE_MAX_POINTS_PER_KICK : res;
}

/*===============================================================================
 
 MÉTODO getDifficultyPercent
 Retorna a porcentagem de dificuldade (em relação à dificuldade máxima) do
 nível atual.
 
 ================================================================================*/

float GameScreen::getDifficultyPercent( void ) const
{
	return NanoMath::clamp( gameInfo.currLevel, 0, MAX_DIFFICULTY_LEVEL ) / MAX_DIFFICULTY_LEVEL;
}

/*===============================================================================
 
 MÉTODO onEffectSet
 Evento chamado assim que o jogador determina o efeito a ser aplicado na bola.
 
 ================================================================================*/

void GameScreen::onEffectSet( void )
{
	// Pára de receber os eventos dos acelerômetros
	AccelerometerManager::GetInstance()->stopAccelerometers();
	
	// Obtém a altura do chute
#if DEBUG && IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS )			
	static uint8 count = 0;
	float heights[] = { 0.0f, REAL_FLOOR_TO_BAR , REAL_FLOOR_TO_BAR * 2.0f, 0.0f, REAL_FLOOR_TO_BAR * 0.5f, REAL_FLOOR_TO_BAR, 0.0f, REAL_FLOOR_TO_BAR * 0.5f, REAL_FLOOR_TO_BAR };
	currPlay.height = heights[ count ];
	count = ( count + 1 ) % 9;
#else
	currPlay.height = pSetEffectControls->getEffectHeight();
#endif
	
	// Obtém o fator de efeito do chute
	currPlay.fx = pSetEffectControls->getEffectFactor();
	
	// Obtém a velocidade e o eixo de rotação da bola
#if DEBUG && IS_CURR_TEST( TEST_BALL_ROTATION )
	currPlay.power = NanoMath::lerp( 0.5f, ISO_POWER_MIN_VALUE, ISO_POWER_MAX_VALUE );
#endif
	
	pSetEffectControls->getEffectAngularSpeed( currPlay.angularSpeed, currPlay.angularSpeedAxis, currPlay.power );
	
	// Modifica o estado da partida
#if DEBUG && IS_CURR_TEST( TEST_SET_EFFECT )
	setState( MATCH_STATE_NEXT_LEVEL );
#else
	setState( MATCH_STATE_HIDING_EFFECT_CONTROLS );
#endif
}

/*===============================================================================
 
 MÉTODO onInterfaceControlAnimCompleted
 Método chamado para indicar que a animação do controle foi finalizada.
 
 ===============================================================================*/

void GameScreen::onInterfaceControlAnimCompleted( InterfaceControl* pControl )
{
	switch( matchState )
	{
		case MATCH_STATE_FEEDBACK_NEXT_CHALLENGE:
			{
				if( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN )
				{
					stateTimeCounter = 0.0f;
					stateDuration = GAME_SCREEN_FEEDBACK_DUR_NEXT_LV;
				}
				else
				{
					if( ( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
					   ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
					   ( pBtPause->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
					   ( pBtFocusOnBall->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
					{
						feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_NEXT_LV ]->setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN );
						setState( MATCH_STATE_GETTING_READY );
					}
				}
			}
			break;
			
		case MATCH_STATE_FEEDBACK_GOAL:
			if( ( pControl == feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_GOAL ] ) && ( feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_GOAL ]->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
			{
				stateTimeCounter = 0.0f;
				stateDuration = GAME_SCREEN_FEEDBACK_DUR_GOAL;
			}
			break;
			
		case MATCH_STATE_FEEDBACK_GAME_OVER:
			if( pControl == feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_GAME_OVER ] )
			{
				InterfaceControlState gameOverMsgState = feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_GAME_OVER ]->getInterfaceControlState();
				if( gameOverMsgState == INTERFACE_CONTROL_STATE_HIDDEN )
				{
					// Coloca o jogo em um estado inválido para impedirmos novas atualizações / renderizações
					setState( MATCH_STATE_UNDEFINED );
				}
				else if( gameOverMsgState == INTERFACE_CONTROL_STATE_SHOWN )
				{
					stateTimeCounter = 0.0f;
					stateDuration = GAME_SCREEN_FEEDBACK_DUR_GAME_OVER;
				}	
			}
			break;
			
		case MATCH_STATE_FEEDBACK_TOO_BAD:
			if( ( pControl == feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_TOO_BAD ] ) && ( feedbackMsgs[ GAME_SCREEN_FEEDBACK_MSG_TOO_BAD ]->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
			{
				stateTimeCounter = 0.0f;
				stateDuration = GAME_SCREEN_FEEDBACK_DUR_TOO_BAD;
			}
			break;

		///<DM>
		case MATCH_STATE_HIDING_REPLAY:			
			SetReplayControlVisibility( false );			
			iReplayTabGroup.setVisible( false );
			setState( MATCH_STATE_SHOWING_RESULT );			
			break;
		///</DM>

		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:
			if( ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
			   ( pBtFocusOnBall->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) )
			{
				moveCameraTo( pBall->getRealPosition(), 1.0f, CAMERA_ADJUST_TIME );
			}
			break;
			
		case MATCH_STATE_SHOWING_KICK_CONTROLS:
			if( ( pControl == pBtInfo ) || ( pControl == pBtFocusOnBall ) )
			{
				if( ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
				   ( pBtFocusOnBall->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) )
				{
					pChronometer->startShowAnimation();
					pPowerMeter->startShowAnimation();
					pDirectionMeter->startShowAnimation();
				}
			}
			else if( ( pControl == pChronometer ) || ( pControl == pPowerMeter ) || ( pControl == pDirectionMeter ) )
			{
				if( ( pChronometer->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
				   ( pPowerMeter->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
				   ( pDirectionMeter->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
				{
					setState( MATCH_STATE_SETTING_KICK );
				}
			}
			break;
			
		case MATCH_STATE_HIDING_KICK_CONTROLS:
			if( ( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
			   ( pChronometer->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
			   ( pPowerMeter->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
			   ( pDirectionMeter->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) )
			{
				setState( MATCH_STATE_PLAYER_RUNNING );
			}
			break;
			
		case MATCH_STATE_SHOWING_INFO:
			if( ( pControl == pBtInfo ) || ( pControl == pBtKick ) || ( pControl == pBtFocusOnBall ) )
			{
				if( ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
				   ( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
				   ( pBtFocusOnBall->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) )
				{
					pInfoTab->startShowAnimation();
				}
			}
			else if( pControl == pInfoTab )
			{
				setState( MATCH_STATE_INFO_SHOWN );
			}	
			break;
			
		case MATCH_STATE_HIDING_INFO:
			if( pControl == pInfoTab )
			{
				pBtInfo->startShowAnimation();
				pBtKick->startShowAnimation();
				pBtFocusOnBall->startShowAnimation();
			}
			else if( ( pControl == pBtInfo ) || ( pControl == pBtKick ) || ( pControl == pBtFocusOnBall ) )
			{
				if( ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
				   ( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
				   ( pBtFocusOnBall->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
				{
					setState( MATCH_STATE_GETTING_READY );
				}
			}
			break;
			
		case MATCH_STATE_SHOWING_EFFECT_CONTROLS:
			if( ( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
			   ( pSetEffectControls->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
			   ( pChronometer->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
			{
				setState( MATCH_STATE_SETTING_EFFECT );
			}
			break;
			
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
			if( ( pControl == pBtKick ) || ( pControl == pChronometer ) )
			{
				if( ( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
				   ///<DM>
				   ( pBtPause->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&				   
				   ///</DM>
				   ( pChronometer->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) )
				{
					pSetEffectControls->startHideAnimation();	
				}
			}
			else if( pControl == pSetEffectControls )
			{
				if( pSetEffectControls->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN )
				{
					// OLD
					// setState( MATCH_STATE_PLAYER_KICKING );
					
					// Posiciona a câmera
					adjustCamera( CAMERA_ADJUST_TIME );
				}
			}
			break;
			
		case MATCH_STATE_SHOWING_RESULT:
			if( pInfoTab->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN )
				setState( MATCH_STATE_RESULT_SHOWN );
			break;
			
		case MATCH_STATE_HIDING_FEEDBACK:
			{
				bool allHidden = true;
				for( uint8 i = 0 ; i < GAME_SCREEN_N_FFEDBACK_MSGS ; ++i )
				{
					if( feedbackMsgs[i]->getInterfaceControlState() != INTERFACE_CONTROL_STATE_HIDDEN )
					{
						allHidden = false;
						break;
					}
				}
				
				if( !allHidden )
					break;

				if( gameInfo.gameMode == GAME_MODE_TRAINING )
				{
					setState( MATCH_STATE_SHOWING_RESULT );
				}
				else
				{
					pSetEffectControls->Unload();
					iReplaySpeedIndex = 2;
					SetReplayControlVisibility( true );
					setState( MATCH_STATE_SHOWING_REPLAY );
				}
			}
			break;

		// OLD : A animação ficava muito longa
		//	case MATCH_STATE_BEFORE_FOCUS_ON_BALL:
		//		if( ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) &&
		//			( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDDEN ) )
		//		{
		//			setState( MATCH_STATE_FOCUSING_ON_BALL );
		//		}
		//		break;
		//
		// case MATCH_STATE_AFTER_FOCUS_ON_BALL:
		//		if( ( pBtInfo->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) &&
		//			( pBtKick->getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWN ) )
		//		{
		//			setState( MATCH_STATE_GETTING_READY );
		//		}
		//		break;
	}
}

/*===============================================================================
 
 MÉTODO onStopTimeReached
 Método chamado quando o cronômetro termina sua contagem.
 
 ===============================================================================*/

void GameScreen::onStopTimeReached( void )
{
	if( matchState == MATCH_STATE_SETTING_KICK )
	{
		onDirAndPowerSet();
	}
	else if( matchState == MATCH_STATE_SETTING_EFFECT )
	{
		pSetEffectControls->setEffect();
		setState( MATCH_STATE_HIDING_EFFECT_KICK_BT );
	}
}

/*===============================================================================
 
 MÉTODO onDirAndPowerSet
 Evento chamado assim que o jogador define a direção e a força do chute.
 
 ===============================================================================*/

void GameScreen::onDirAndPowerSet( void )
{
	// Pára de receber os eventos dos acelerômetros
	AccelerometerManager::GetInstance()->stopAccelerometers();
	
#if DEBUG && IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS )			
	
	currPlay.power = NanoMath::lerp( 0.5f, ISO_POWER_MIN_VALUE, ISO_POWER_MAX_VALUE );
	
	static uint8 count = 0;
	float directions[] = { -REAL_GOAL_WIDTH, -REAL_GOAL_WIDTH, -REAL_GOAL_WIDTH, REAL_GOAL_WIDTH, REAL_GOAL_WIDTH, REAL_GOAL_WIDTH, 0.0f, 0.0f, 0.0f };
	
	currPlay.direction = ( Point3f( directions[ count ], 0.0f, 0.0f ) - *( pBall->getRealPosition() ) ).getNormalized();
	currPlay.direction.y = 0.0f;
	currPlay.direction *= currPlay.power;
	
	count = ( count + 1 ) % 9;
	
#else
	
	currPlay.power = pPowerMeter->getCurrValue();
	
	currPlay.direction = ( Point3f( pDirectionMeter->getCurrValue(), 0.0f, 0.0f ) - *( pBall->getRealPosition() ) ).getNormalized();
	currPlay.direction.y = 0.0f;
	currPlay.direction *= currPlay.power;
	
#endif
	
	setState( MATCH_STATE_HIDING_KICK_CONTROLS );
	
	// OLD
	//setState( MATCH_STATE_PLAYER_RUNNING );
}

/*===============================================================================
 
 MÉTODO onKick
 Método chamado assim que o personagem chuta a bola.
 
 ================================================================================*/

void GameScreen::onKick( void )
{
	// Configura o chute - se for replay, refaz última jogada
	pBall->kick( &ballDestPos, &currPlay, matchState == MATCH_STATE_PLAYER_KICKING_REPLAY );
	pKeeper->setJumpDirection( &ballDestPos, pBall->getTimeToGoal() );
	
	// Modifica o estado da partida
	setState( matchState == MATCH_STATE_PLAYER_KICKING ? MATCH_STATE_ANIMATING_PLAY : MATCH_STATE_ANIMATING_REPLAY );
}

/*===============================================================================
 
 MÉTODO onCameraAnimEnded
 Indica que a animação da câmera terminou.
 
 ================================================================================*/

void GameScreen::onCameraAnimEnded( void )
{
	onMoveCamera( 0.0f, 0.0f );
	
	switch( matchState )
	{
		case MATCH_STATE_ADJUSTING_CAM_TO_SHOT:
			setState( MATCH_STATE_SHOWING_KICK_CONTROLS );
			break;
			
		case MATCH_STATE_FOCUSING_ON_BALL:
			setState( MATCH_STATE_AFTER_FOCUS_ON_BALL );
			break;
			
		case MATCH_STATE_HIDING_EFFECT_CONTROLS:
			setState( MATCH_STATE_PLAYER_KICKING );
			break;
	}
}

/*===============================================================================
 
 MÉTODO onCameraAnimEnded
 Chamado quando o sprite muda de etapa de animação.
 
 ================================================================================*/

void GameScreen::onAnimStepChanged( Sprite* pSprite )
{
	switch( matchState )
	{
		case MATCH_STATE_PLAYER_RUNNING:
		case MATCH_STATE_PLAYER_RUNNING_REPLAY:
			// pSprite == NULL => Gambiarra necessária já que pPlayer é um grupo !!!
			if( ( pSprite == NULL ) && ( pPlayer->isOnBeforeKickAnimStep() ) )
				setState( matchState == MATCH_STATE_PLAYER_RUNNING ? MATCH_STATE_SHOWING_EFFECT_CONTROLS : MATCH_STATE_PLAYER_KICKING_REPLAY );
			break;
			
		case MATCH_STATE_PLAYER_KICKING:
		case MATCH_STATE_PLAYER_KICKING_REPLAY:
			// pSprite == NULL => Gambiarra necessária já que pPlayer é um grupo !!!
			if( ( pSprite == NULL ) && ( pPlayer->isOnKickAnimStep() ) )
				onKick();
			break;
	}
}

/*===============================================================================
 
 MÉTODO onAnimEnded
 Chamado quando o sprite termina um sequência de animação.
 
=================================================================================*/

void GameScreen::onAnimEnded( Sprite* pSprite )
{
	Sprite* pLeftFlag = dynamic_cast< Sprite* >( getObject( OBJ_INDEX_LEFT_FLAG ) );
	Sprite* pRightFlag = dynamic_cast< Sprite* >( getObject( OBJ_INDEX_RIGHT_FLAG ) );
	
	if( ( pSprite == pLeftFlag ) && ( pLeftFlag->getCurrAnimSequence() == GAME_SCREEN_FLAG_ANIM_ANIMATING ) )
	{
		leftFlagTimeCounter = 0.0f;
		pLeftFlag->setAnimSequence( GAME_SCREEN_FLAG_ANIM_STOPPED, false );
		
		dynamic_cast< Sprite* >( getObject( OBJ_INDEX_LEFT_FLAG_SHADOW ) )->setAnimSequence( GAME_SCREEN_FLAG_ANIM_STOPPED, false );
	}
	else if( ( pSprite == pRightFlag ) && ( pRightFlag->getCurrAnimSequence() == GAME_SCREEN_FLAG_ANIM_ANIMATING ) )
	{
		rightFlagTimeCounter = 0.0f;
		pRightFlag->setAnimSequence( GAME_SCREEN_FLAG_ANIM_STOPPED, false );
		
		dynamic_cast< Sprite* >( getObject( OBJ_INDEX_RIGHT_FLAG_SHADOW ) )->setAnimSequence( GAME_SCREEN_FLAG_ANIM_STOPPED, false );
	}
}

/*===============================================================================
 
 MÉTODO fixBallPos
	Impede que a bola fique em uma posição inválida.
 
=================================================================================*/

//<DM>
void GameScreen::fixBallPos( Point3f& ballNewRealPos )
{
	Point3f finalBallPos = ballNewRealPos;
	
#if DEBUG && ( IS_CURR_TEST( TEST_BALL_INSIDE_GOAL ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
	
	// Vazio
	
#else
	
	if( ( ballNewRealPos.z < REAL_SMALL_AREA_DEPTH ) && ( ballNewRealPos.x > ( -REAL_SMALL_AREA_WIDTH * 0.5f )) && ( ballNewRealPos.x < ( REAL_SMALL_AREA_WIDTH * 0.5f )) )
	{
		Point3f currBallPos = *( pBall->getRealPosition() );
		
		if( ( currBallPos.x > ( -REAL_SMALL_AREA_WIDTH * 0.5f )) && ( currBallPos.x < ( REAL_SMALL_AREA_WIDTH * 0.5f )) )
		{
			finalBallPos.z = REAL_SMALL_AREA_DEPTH;
		}
		else
		{
			if( currBallPos.x <= ( -REAL_SMALL_AREA_WIDTH * 0.5f ) )
			{
				finalBallPos.x = -REAL_SMALL_AREA_WIDTH * 0.5f;
				
				if( currBallPos.z > REAL_SMALL_AREA_DEPTH )
					finalBallPos.z = REAL_SMALL_AREA_DEPTH;
			}
			else
			{
				finalBallPos.x = REAL_SMALL_AREA_WIDTH * 0.5f;
				
				if( currBallPos.z > REAL_SMALL_AREA_DEPTH )
					finalBallPos.z = REAL_SMALL_AREA_DEPTH;
			}
		}
	}
	
	if( finalBallPos.x < FOUL_CHALLENGE_LEFT_LIMIT )
	{
		finalBallPos.x = FOUL_CHALLENGE_LEFT_LIMIT;
	}
	else if( finalBallPos.x > FOUL_CHALLENGE_RIGHT_LIMIT )
	{
		finalBallPos.x = FOUL_CHALLENGE_RIGHT_LIMIT;
	}
	
	if( finalBallPos.z < FOUL_MIN_Z_DISTANCE )
	{
		finalBallPos.z = FOUL_MIN_Z_DISTANCE;
	}
	else if( finalBallPos.z > FOUL_CHALLENGE_MAX_Z )
	{
		finalBallPos.z = FOUL_CHALLENGE_MAX_Z;
	}
#endif
	
	pBall->setRealPosition( &finalBallPos );
	
#if DEBUG && IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
	pBall->keepBallInsideGoal();
#endif
	
	ballNewRealPos = finalBallPos;
}
//</DM>

/*===============================================================================
 
 MÉTODO onBallPosChanged
 Reposiciona os elementos da cena de acordo com a posição da bola.
 
 ================================================================================*/

void GameScreen::onBallPosChanged( const Point3f& ballNewPos )
{
	Point3f ballNewRealPos = GameUtils::isoPixelsToGlobalY0( ballNewPos );
	fixBallPos( ballNewRealPos );	
	
	pPlayer->prepare( ballNewRealPos );
	
#if DEBUG && IS_CURR_TEST( TEST_BARRIER_SHADOWS )
	
	pKeeper->setVisible( false );
	pBarrier->reset( gameInfo.gameMode, ballNewRealPos );
	
#elif DEBUG && IS_CURR_TEST( TEST_BALL_SHADOW )
	
	pKeeper->setVisible( false );
	pBarrier->undoCurrentBarrier( gameInfo.gameMode );
	pPlayer->setVisible( false );
	
#else

	// Se está dentro da grande área, não tem barreira
	if(( ballNewRealPos.z < REAL_BIG_AREA_DEPTH ) && ( ballNewRealPos.x > ( -REAL_BIG_AREA_WIDTH * 0.5f )) && ( ballNewRealPos.x < ( REAL_BIG_AREA_WIDTH * 0.5f )) )
		pBarrier->undoCurrentBarrier( gameInfo.gameMode );
	else
		pBarrier->reset( gameInfo.gameMode, ballNewRealPos, gameInfo );
	
	pKeeper->reset();
	pKeeper->prepare( &ballNewRealPos, pBarrier->getNumberOfPlayers() );
	
#endif
	
	// Garante que todos os elementos que podem trocar de ordem Z com a bola estão corretos
	updateZOrder();
}

/*===============================================================================
 
 MÉTODO onBallTargetPosChanged
 Garante que a nova posição do alvo da bola é válida.
 
 ================================================================================*/

void GameScreen::onBallTargetPosChanged( const Point3f& ballTargetPos )
{
	Point3f ballTargetNewRealPos = GameUtils::isoPixelsToGlobalZ0( ballTargetPos );
	
	if( ballTargetNewRealPos.y > REAL_FLOOR_TO_BAR /* OLD REAL_BAR_BOTTOM */ )
		ballTargetNewRealPos.y = REAL_FLOOR_TO_BAR /* OLD REAL_BAR_BOTTOM */;
	else if( ballTargetNewRealPos.y < 0.0f )
		ballTargetNewRealPos.y = 0.0f;
	
	if( ballTargetNewRealPos.x < REAL_LEFT_POST_START )
		ballTargetNewRealPos.x = REAL_LEFT_POST_START;
	else if( ballTargetNewRealPos.x > REAL_RIGHT_POST_START )
		ballTargetNewRealPos.x = REAL_RIGHT_POST_START;
	
	ballTargetNewRealPos.z = ISO_BALL_TARGET_REAL_Z;
	
	pBallTarget->setRealPosition( &ballTargetNewRealPos );
}

/*===============================================================================
 
 MÉTODO vibrate
 Faz o aparelho vibrar caso ainda não tenhamos utilizado a vibração nesta jogada.
 
 ================================================================================*/

void GameScreen::vibrate( void )
{
	if( !vibrated )
	{
		vibrated = true;
		[(( FreeKickAppDelegate* )APP_DELEGATE ) vibrate: VIBRATION_TIME_DEFAULT];
	}
}

