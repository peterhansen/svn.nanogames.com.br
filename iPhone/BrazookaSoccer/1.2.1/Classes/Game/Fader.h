/*
 *  Fader.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/13/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FADER_H
#define FADER_H 1

#include "Color.h"
#include "Object.h"

class Fader : public Object
{
	public:
		// Construtor
		Fader( const Color& color, float startAlpha, float finalAlpha, float alphaTransitionTime );
	
		// Destrutor
		virtual ~Fader( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

		// Coloca o objeto em seu estado inicial
		void reset( void );
	
		// Retorna a cor d fader
		const Color* getColor( void ) const;
	
		// Determina a cor do fader
		void setColor( const Color* pColor );

	private:
		// Cor utilizada para escurecer a tela
		Color faderColor;
	
		// Controladores da animação de fade
		float startAlpha, finalAlpha;
		float timeCounter, alphaTransitionTime;
};

// Implementação dos métodos inline

// Retorna a cor d fader
inline const Color* Fader::getColor( void ) const
{
	return &faderColor;
}
	
// Determina a cor do fader
inline void Fader::setColor( const Color* pColor )
{
	if( pColor )
		faderColor = *pColor;
}

#endif
