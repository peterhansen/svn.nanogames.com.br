/*
 *  GLButton.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GL_BUTTON
#define GL_BUTTON

#include "ObjectGroup.h"
#include "Sprite.h"

class GLButtonListener;

class GLButton : public ObjectGroup
{
	public:
		// Construtores
		// Exceções: ConstructorException
		GLButton( const char* pOffImgsId, const char* pOffDescName, const char* pOnImgsId, const char* pOnDescName, GLButtonListener* pListener = NULL );
	
		// Destrutor
		virtual ~GLButton( void ){};
	
		// Indica o estado do botão
		void setState( bool on );

	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( const char* pOffImgsId, const char* pOffDescName, const char* pOnImgsId, const char* pOnDescName );
		
		// Objeto que irá receber os eventos desta classe
		GLButtonListener* pListener;
	
		// Imagem exibida quando o botão não está selecionado
		Sprite* pButtonOff;
	
		// Imagem exibida quando o botão está selecionado
		Sprite* pButtonOn;
};

class GLButtonListener
{
	public:
		// Evento chamado quando o botão recebe um clique
		virtual void GLButtonOnClick( GLButton* pButton ) = 0;
};

#endif