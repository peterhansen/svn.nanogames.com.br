/*
 *  WoodBarrierPlayer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef WOOD_BARRIER_PALYER_H
#define WOOD_BARRIER_PALYER_H 1

#include "BarrierPlayer.h"

#include "Tests.h"

// TODOO : Aumentar o número de quadrados de colisão por personagem
#define WOOD_N_COLLISION_QUADS 7

class WoodBarrierPlayer : public BarrierPlayer
{
	public:
		// Construtor
		WoodBarrierPlayer( WoodBarrierPlayer* pBase );
	
		// Destrutor
		virtual ~WoodBarrierPlayer( void );
	
		// Retorna a largura real do jogador da barreira (em metros)
		virtual float getRealWidth( void ) const;

		// Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
		// que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente
		virtual void prepare( const Point3f& ballRealPos );

		// Testa colisão deste objeto com a área passada como parâmetro
		virtual bool checkCollision( const CollisionQuad& area );
	
		// Retorna o índice do som que deve ser tocado quando a bola colide com este jogador
		virtual void getCollisionSoundNameAndIndex( uint8* pName, uint8* pIndex ) const;
	
		#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
			// Renderiza o objeto
			virtual bool render( void );
		#endif
	
	private:
		// Atualiza o frame do jogador de acordo com a direção para a qual está virado
		virtual void updateFrame( void );

		// Atualiza a posição da bola na tela (não altera a posição real)
		virtual void updatePosition( void );
	
		// TODOO : Aumentar o número de quadrados de colisão por personagem
//		// Áreas extras de colisão do jogador
//		CollisionQuad collisionQuads[ WOOD_N_COLLISION_QUADS ];
};

#endif
