/*
 *  PlayerStates.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PLAYER_STATES_H
#define PLAYER_STATES_H

typedef enum PLAYER_STATE
{
	PLAYER_STOPPED,		// Parado
	PLAYER_RUNNING,		// Correndo
	PLAYER_SHOOTING,	// Está chutando a bola
	PLAYER_SHOT,		// Já chutou a bola
	PLAYER_JUMPING		// Pulando

}PLAYER_STATE;

#endif
