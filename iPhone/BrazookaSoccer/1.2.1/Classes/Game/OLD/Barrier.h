/*
 *  GameUtils.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BARRIER_H
#define BARRIER_H 1

#include "Iso_Defines.h"
#include "GameInfo.h"


#include "ObjectGroup.h"
#include "Point3f.h"
#include "Updatable.h"

#include "Tests.h"

#include <vector>

// Foward Declarations
class Ball;
class ObjectGroup;
class BarrierPlayer;

class Barrier : public Updatable
{
	public:
		// Construtor
		Barrier( ObjectGroup* pPlayersGroup );

		// Destrutor
		virtual ~Barrier( void );
	
		// Indica que um jogador pode ser utilizado em um modo de jogo
		void registerPlayer( GameMode gameMode, uint16 index );
	
		// Cria uma nova barreira e posiciona a mesma em relação à posição da bola
		void reset( GameMode gameMode, const Point3f& ballPosition ,GameInfo &aGameInfo);
	
		// Posiciona a barreira em relação à posição da bola
		void prepare( const Point3f& ballPosition );
	
		// Este método deve ser chamado quando a bola se aproximar da barreira. Então, poderemos
		// iniciar as animações dos personagens
		void onBallGettingClose( const Ball* pBall );
	
		// Desfaz a barreira atual
		void undoCurrentBarrier( GameMode gameMode );
	
		// Retorna o número de jogadores atualmente na barreira
		uint8 getNumberOfPlayers( void ) const;
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Retorna um jogador da barreira ou NULL caso a barreira esteja vazia
		BarrierPlayer* getPlayer( int8 index );

		// Define se o teste de colisão com a bola deve ser feito
		void setBallCollisionTest( bool checkCollision );
	
	
		#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )

			// Imprime as áreas de colisão de cada personagem da barreira
			void printCollisionQuads( void ) const;

		#endif

		///<DM>
		void restoreBarrier( int8 *ptrvec, int8 size );
		///</DM>

	private:	
		// Calcula quantos jogadores devem entrar na barreira
		uint8 calcNumberOfPlayers( const Point3f& ballPosition );

		// Faz os jogadores da barreira olharem para a bola
		void preparePlayer( BarrierPlayer* pPlayer, const Point3f& basePosition, const Point3f& playerPos, const Point3f& ballPosition, const Point3f& ortho );
	
		// Número de jogadores na barreira atual
		uint8 numberOfPlayers;
	
		// Grupo que contém os jogadores que serão utilizados na barreira
		ObjectGroup* pPlayersGroup;
	
		// Jogadores sendo utilados na barreira atual
		BarrierPlayer* barrierPlayers[ BARRIER_MAX_N_PLAYERS ];
	
		// Arrays contendo, para cada modo de jogo, os jogadores que podem ser utilzados na barreira
		std::vector< uint16 > trainingModePlayers;
		std::vector< uint16 > rankingModePlayers;
};

// Retorna o número de jogadores atualmente na barreira
inline uint8 Barrier::getNumberOfPlayers( void ) const
{
	return numberOfPlayers;
}



#endif
