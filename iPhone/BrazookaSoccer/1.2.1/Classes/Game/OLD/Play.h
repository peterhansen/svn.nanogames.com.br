/*
 *  Play.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PLAY_H
#define PLAY_H 1

#include "Point3f.h"

struct Play
{
	// Construtor
	Play( const Point3f* pDirection = NULL, float height = 0.0f, float power = 0.0f, float fx = 0.0f, float angularSpeed = 0.0f, const Point3f* pAngularSpeedAxis = NULL );
	
	// Inicializa o objeto
	void set( const Point3f* pDirection = NULL, float height = 0.0f, float power = 0.0f, float fx = 0.0f, float angularSpeed = 0.0f, const Point3f* pAngularSpeedAxis = NULL );
	
	// Direção do chute
	Point3f direction;
	
	// Altura do chute
	float height;
	
	// Força do chute
	float power;
	
	// Fator de efeito aplicado à bola. Corresponde ao módulo do vetor ortogonal à direção do chute.
	// É utilizado para para gerar os dois pontos de controle de uma curva Bezier. Do modo como está,
	// o efeito não altera o destino da bola.
	float fx;
	
	// Efeito do chute
	float angularSpeed;
	Point3f angularSpeedAxis;
};

#endif
