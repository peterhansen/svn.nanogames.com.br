/*
 *  IsoBall.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ISO_BALL_H
#define ISO_BALL_H 1

#include "Ball.h"
#include "Sprite.h"

#include "Tests.h"

#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
	#include <vector>
#endif

enum BallAuraState
{
	BALL_AURA_STATE_HIDDEN =		0,
	BALL_AURA_STATE_LOOPING =		1,
	BALL_AURA_STATE_FADING_OUT =	2
};

// Declarações adiadas
class SoccerBall;
class SoccerBallShadow;

class IsoBall : public Ball, public SpriteListener
{
	public:
		// Construtor
		// Exceções: ConstructorException
		IsoBall( void );
	
		// Destrutor
		virtual ~IsoBall( void );

		// Reinicializa os atributos do objeto
		virtual void reset( const Point3f* pRealPosition = NULL );
	
		// Define os parâmetros da bola após o chute
		virtual Point3f* kick( Point3f* pOut, Play* pPlay, bool replay );
	
		// TODOO : Descomentar quando tivermos placas de publicidade
		// Verifica se a bola colidiu com as placas de publicidade no fundo do campo, atualizando
		// sua velocidade e direção conforme necessário
		//void checkBoardsCollision( void );

		// Getters e Setters dos atributos da bola
		Point3f getLastBoardCollisionDirection( void ) const;
		void setLastBoardCollisionDirection( const Point3f* pBoardCollisionDirection );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Obtém a área da bola que serve como área de colisão para toques. Esta área
		// pode ser maior ou menor do que a área do grupo
		void getTouchCollisionArea( Triangle& t0, Triangle& t1 );
	
		// Determina o estado da aura da bola
		void setAuraState( BallAuraState state );
	
		// Retorna o estado da aura da bola
		BallAuraState getAuraState( void ) const;

		// Renderiza o objeto
		virtual bool render( void );
	
		#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
			// Trajetória da bola
			std::vector< Point3f > ballPath;
		#endif

	private:
		// Atualiza a posição da bola na tela (não altera a posição real)
		virtual void updatePosition( void );
	
	
	#if DEBUG && IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
	
	public:
	
	#endif

		// Evita que a bola "fure" a rede após um gol
		virtual void keepBallInsideGoal( void );
	
	#if DEBUG && IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
	
	private:

	#endif
	
		// Trata a movimentação da bola após ela ir para fora. Testa colisão da bola
		// com a rede, de forma que a bola não "fure" a rede pelo lado de fora
		virtual void handleBallOut( void );
	
		// Chamado quando um sprite termina uma sequência de animação
		virtual void onAnimEnded( Sprite* pSprite );

		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );
	
		// Limpa as variáveis do objeto
		void clear( void );
	
		// Reduz a velocidade angular da bola após colisões
		void slowDownAngSpeed( float timeElapsed );
	
		// Indica se já checou colisão com as placas de publicidade
		bool checkedBoardsCollision;
	
		// Imagem da bola e de sua sombra
		Sprite* pAura;
		SoccerBall* pBall;
		SoccerBallShadow* pBallShadow;

		// Velocidade angular da bola
		float angularSpeed;
		Point3f angularSpeedAxis;
	
		// Vetor de direção da bola após a última colisão com placas de publicidade
		Point3f lastBoardCollisionDirection;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

/*==============================================================================================

MÉTODO getLastBoardCollisionDirection
	Obtém o vetor de direção da bola após a última colisão com placas de publicidade.

==============================================================================================*/

inline Point3f IsoBall::getLastBoardCollisionDirection( void ) const
{
	return lastBoardCollisionDirection;
};

/*==============================================================================================

MÉTODO setLastBoardCollisionDirection
	Determina o vetor de direção da bola após a última colisão com placas de publicidade.

==============================================================================================*/

inline void IsoBall::setLastBoardCollisionDirection( const Point3f* pBoardCollisionDirection )
{
	lastBoardCollisionDirection = *pBoardCollisionDirection;
};

#endif

