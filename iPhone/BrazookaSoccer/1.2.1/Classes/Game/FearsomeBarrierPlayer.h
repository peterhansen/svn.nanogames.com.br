/*
 *  FearsomeBarrierPlayer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FEARSOME_BARRIER_PALYER_H
#define FEARSOME_BARRIER_PALYER_H 1

#include "BarrierPlayer.h"
#include "Sprite.h"

#include "Tests.h"

class FearsomeBarrierPlayer : public BarrierPlayer, public SpriteListener
{
	public:
		// Construtor
		FearsomeBarrierPlayer( void );
	
		// Destrutor
		virtual ~FearsomeBarrierPlayer( void );
	
		// Retorna a largura real do jogador da barreira (em metros)
		virtual float getRealWidth( void ) const;

		// Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
		// que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente
		virtual void prepare( const Point3f& ballRealPos );
	
		// Retorna o índice do som que deve ser tocado quando a bola colide com este jogador
		virtual void getCollisionSoundNameAndIndex( uint8* pName, uint8* pIndex ) const;
	
		#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
			// Renderiza o objeto
			virtual bool render( void );
		#endif
	
	private:
		// Atualiza o frame do jogador de acordo com a direção para a qual está virado
		virtual void updateFrame( void );
	
		// Atualiza a posição da bola na tela (não altera a posição real)
		virtual void updatePosition( void );
	
		// Faz com que o jogador exiba alguma reação devido à aproximação da bola
		virtual void onBallGettingClose( void );
	
		// Chamado quando o sprite muda de etapa de animação
		virtual void onAnimStepChanged( Sprite* pSprite );
};

#endif
