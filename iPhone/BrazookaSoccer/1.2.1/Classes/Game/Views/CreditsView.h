//
//  CreditsView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 12/19/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef CREDITS_VIEW_H
#define CREDITS_VIEW_H 1

#import "GameBasicView.h"
#import <UIKit/UIAlert.h>

#include <vector>

enum CreditsViewState
{
	CREDITS_VIEW_STATE_HIDDEN = 0,
	CREDITS_VIEW_STATE_SHOWING_STEP_0,
	CREDITS_VIEW_STATE_SHOWING_STEP_1,
	CREDITS_VIEW_STATE_SHOWING_STEP_2,
	CREDITS_VIEW_STATE_SHOWING_STEP_3,
	CREDITS_VIEW_STATE_SHOWN,
	CREDITS_VIEW_STATE_HIDING
};

@interface CreditsView : GameBasicView <UIAlertViewDelegate>
{
	@private
		// Handlers para os elementos da interface
		IBOutlet UIButton *hBtGoToURL, *hBtBack;
		IBOutlet UIImageView *hIcon;
	
		// OLD
		//IBOutlet UIImageView *hImgHairLeft, *hImgHairRight;
		IBOutlet UIImageView *hImgHair;
	
		IBOutlet UIImageView *hImgNanoLogo;
		IBOutlet UILabel *hLbCopyright0, *hLbCopyright1;
	
		// Estado atual da view
		CreditsViewState currState;
	
		// Controlador dos sons
		std::vector< bool > soundFlags;
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Abandona a aplicação e abre o navegador no site da nanogames
- ( IBAction ) onGoToURL;

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end

#endif
