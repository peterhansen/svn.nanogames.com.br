//
//  PlayMenuController.mm
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import "PlayMenuController.h"

#include "ApplicationManager.h"
#include "Config.h"

@implementation PlayMenuController

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
-( id )initWithNibName:( NSString* )nibNameOrNil bundle:( NSBundle* )nibBundleOrNil
{
    if(( self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil] ))
	{
        // Custom initialization
		// ...
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
-( void )loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-( void )viewDidLoad
{
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
-( BOOL )shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return ( interfaceOrientation == UIInterfaceOrientationPortrait );
}
*/

-( void )didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview
    [super didReceiveMemoryWarning];
	
    // Release anything that's not essential, such as cached data
	//...
}

-( void )dealloc
{
    [super dealloc];
}

// Mensagem chamada quando o jogador seleciona a opção "New Game"
-( IBAction )onNewGame
{
}

// Mensagem chamada quando o jogador seleciona a opção "Load Game"
-( IBAction )onLoadGame
{
}

// Mensagem chamada quando o jogador seleciona a opção "Back"
-( IBAction )onBack
{
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

@end
