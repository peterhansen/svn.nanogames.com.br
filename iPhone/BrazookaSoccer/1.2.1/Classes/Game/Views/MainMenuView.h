//
//  MainMenuView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef MAIN_MENU_H
#define MAIN_MENU_H 1

#import "GameBasicView.h"

#include <vector>

enum MainMenuViewState
{
	MAIN_MENU_VIEW_STATE_HIDDEN = 0,
	MAIN_MENU_VIEW_STATE_SHOWING,
	MAIN_MENU_VIEW_STATE_SHOWN,
	MAIN_MENU_VIEW_STATE_HIDING
};

@interface MainMenuView : GameBasicView
{
	@private
		// Handlers para os botões do menu
		IBOutlet UIButton* hBtPlay;
		IBOutlet UIButton* hBtRanking;
		IBOutlet UIButton* hBtOptions;
		IBOutlet UIButton* hBtHelp;
		IBOutlet UIButton* hBtCredits;
		IBOutlet UIButton* hBtNanoOnline;
	
		// Estado atual da view
		MainMenuViewState currState;
	
		// Auxiliares
		std::vector< bool > soundFlags;
		float largestWidth;
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Indica que o usuário selecionou uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end

#endif
