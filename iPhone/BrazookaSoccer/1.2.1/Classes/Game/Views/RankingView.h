//
//  RankingView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 6/18/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef RANKING_VIEW_H
#define RANKING_VIEW_H 1

#import "GameBasicView.h"

#include "Config.h"
#include "NanoTypes.h"

#include <vector>

#define RANKING_LABELS_N_CHARS 11

enum RankingViewState
{
	RANKING_VIEW_STATE_HIDDEN = 0,
	RANKING_VIEW_STATE_SHOWING_STEP_0,
	RANKING_VIEW_STATE_SHOWING_STEP_1,
	RANKING_VIEW_STATE_SHOWING_STEP_2,
	RANKING_VIEW_STATE_SHOWING_STEP_3,
	RANKING_VIEW_STATE_SHOWING_STEP_4,
	RANKING_VIEW_STATE_SHOWN,
	RANKING_VIEW_STATE_HIDING
};

@interface RankingView : GameBasicView
{
	@private
		// Handlers para os controles da interface
		IBOutlet UIButton* hBtBack;
	
		IBOutlet UIImageView *hImgHair;
		IBOutlet UIImageView *hImgTag;

		IBOutlet UIImageView *hImg1stIcon, *hImg1stPtsBkg, *hImg1stPtsTitle;
		IBOutlet UIImageView *hImg2ndIcon, *hImg2ndPtsBkg, *hImg2ndPtsTitle;
		IBOutlet UIImageView *hImg3rdIcon, *hImg3rdPtsBkg, *hImg3rdPtsTitle;
		IBOutlet UIImageView *hImg4thIcon, *hImg4thPtsBkg, *hImg4thPtsTitle;
		IBOutlet UIImageView *hImg5thIcon, *hImg5thPtsBkg, *hImg5thPtsTitle;
	
		// Estado atual da view
		RankingViewState currState;
	
		// Índice do ranking que deve ficar piscando
		float blinkTimeCounter;
		int8 blinkingRecordIndex;

		// Número de separadores decimais na maior pontuação que pode ser alcançada no jogo
		int8 nDots;
	
		// Número de dígitos contidos na maior pontuação que pode ser alcançada no jogo
		int16 maxPointsLen;

		// Views utilizadas para formarmos os labels das pontuações
		UIImageView* labelPts[ APP_N_RECORDS ][ RANKING_LABELS_N_CHARS ];
	
		// Controlador dos sons
		bool soundFlag;
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition:( int8 )blinkingRecord setRecords:( const uint32* )records;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end

#endif
