#import "RankingView.h"

#include "ObjcMacros.h"
#include "Tests.h"
#include "Utils.h"

#include "FreeKickAppDelegate.h"

// Duração da animação de fadein do ícone
#define RANKING_VIEW_ICONS_ANIM_DUR 0.200f

// Quantidade de tempo a mais que cada colocação do ranking, após a 1a, leva para terminar a animação de fadein
#define RANKING_VIEW_ICONS_ANIM_DIFF 0.050f

// Configuração da animação de viewport dos "labels" que exibem os pontos
#define RANKING_VIEW_PTS_BKGS_ANIM_DUR 0.200f
#define RANKING_VIEW_PTS_BKGS_ANIM_DIFF 0.050f

// Tempo total das animações
#define RANKING_VIEW_HAIR_ANIM_DUR 0.150f
#define RANKING_VIEW_TAG_ANIM_DUR 0.150f
#define RANKING_VIEW_BT_BACK_ANIM_DUR 0.150f

// Número de pixels, dos controles da esquerda, que ficam fora da tela
#define RANKING_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN 0.0f
#define RANKING_VIEW_TAG_PIXELS_OUTSIDE_SCREEN 0.0f
#define RANKING_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN 16.0f

// Dimensões dos algarismos da fonte utilizada
#define GOOD_GIRL_NUM_FRAME_WIDTH 15.0f
#define GOOD_GIRL_NUM_FRAME_HEIGHT 17.0f
#define GOOD_GIRL_DOT_FRAME_WIDTH 3.0f

// Offset dos labels em relação às imagens de fundo
#define RANKING_VIEW_PTS_LABELS_OFFSET_Y 13.0f
#define RANKING_VIEW_PTS_LABELS_OFFSET_X 30.0f

// Velocidade com que o label do último recorde pisca
#define RANKING_VIEW_BLINKING_RECORD_DUR 0.150f

// Tamanho do buffer para formatação dos pontos
#define UPDATE_RANKING_LABELS_BUFFER_LEN 32

// Extensão da classe para declarar métodos privados
@interface RankingView ()

//- ( bool )buildRankingView;

- ( void ) setState:( RankingViewState ) state;
- ( void ) updateRankingLabels:( const uint32* ) pRecords;

- ( bool ) isRecordVisible:( int8 )recordIndex;
- ( void ) setRecord:( int8 )recordIndex Visible:( bool )visible;

@end

// Início da implementação da classe
@implementation RankingView

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self buildRankingView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildRankingView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM buildRankingView
	Inicializa a view.

==============================================================================================*/

//- ( void )buildRankingView
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	[hImg1stPtsBkg setClipsToBounds: YES];
	[hImg2ndPtsBkg setClipsToBounds: YES];
	[hImg3rdPtsBkg setClipsToBounds: YES];
	[hImg4thPtsBkg setClipsToBounds: YES];
	[hImg5thPtsBkg setClipsToBounds: YES];
	
	[hImg1stPtsTitle setClipsToBounds: YES];
	[hImg2ndPtsTitle setClipsToBounds: YES];
	[hImg3rdPtsTitle setClipsToBounds: YES];
	[hImg4thPtsTitle setClipsToBounds: YES];
	[hImg5thPtsTitle setClipsToBounds: YES];
	
	char buffer[ UPDATE_RANKING_LABELS_BUFFER_LEN ];
	snprintf( buffer, UPDATE_RANKING_LABELS_BUFFER_LEN, "%d", static_cast< int32 >( SCORE_MAX ) );
	maxPointsLen = strlen( buffer );
	nDots = maxPointsLen <= 0 ? 0 : ( maxPointsLen - 1 ) / 3;

	// Views utilizadas para formarmos os labels das pontuações
	UIView* bkgs[] = { hImg1stPtsBkg, hImg2ndPtsBkg, hImg3rdPtsBkg, hImg4thPtsBkg, hImg5thPtsBkg };
	
	for( uint8 i = 0 ; i < APP_N_RECORDS; ++i )
	{
		float currY = bkgs[ i ].frame.origin.y + RANKING_VIEW_PTS_LABELS_OFFSET_Y;
		float currX = bkgs[ i ].frame.origin.x + RANKING_VIEW_PTS_LABELS_OFFSET_X;

		for( uint8 j = 0 ; j < RANKING_LABELS_N_CHARS ; ++j )
		{
			labelPts[ i ][ j ] = [[UIImageView alloc] initWithImage:nil];
			if( labelPts[ i ][ j ] == nil )
			{
				#if DEBUG
					LOG( "RankingView::awakeFromNib : Unable to create labelPts[ %d ][ %d ]\n", i, j );
				#endif

				[APP_DELEGATE quit: ERROR_ALLOCATING_VIEWS];
			}
			
			// &3 == %4
			if( ( ( j + 1 ) & 3 ) == 0 )
			{
				[labelPts[ i ][ j ] setFrame: CGRectMake( currX, currY, GOOD_GIRL_DOT_FRAME_WIDTH, GOOD_GIRL_NUM_FRAME_HEIGHT )];
				currX += GOOD_GIRL_DOT_FRAME_WIDTH;
			}
			else
			{
				[labelPts[ i ][ j ] setFrame: CGRectMake( currX, currY, GOOD_GIRL_NUM_FRAME_WIDTH, GOOD_GIRL_NUM_FRAME_HEIGHT )];
				currX += GOOD_GIRL_NUM_FRAME_WIDTH;
			}
			
			[self addSubview: labelPts[ i ][ j ]];
			
			[labelPts[ i ][ j ] setClipsToBounds: YES];
			[labelPts[ i ][ j ] setContentMode: UIViewContentModeTopLeft];

			[labelPts[ i ][ j ] release];
		}
	}
	
	soundFlag = false;
	
	blinkTimeCounter = 0.0f;
	blinkingRecordIndex = -1;
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
    [super dealloc];
}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	switch( currState )
	{
		case RANKING_VIEW_STATE_SHOWING_STEP_0:
		case RANKING_VIEW_STATE_SHOWING_STEP_2:
		case RANKING_VIEW_STATE_SHOWING_STEP_4:
			{
				uint8 currAnimatingView = ( ( int8 )currState - ( int8 )RANKING_VIEW_STATE_SHOWING_STEP_0 ) >> 1;
				UIView* hViews[] = { hImgHair, hImgTag, hBtBack };
				float hViewsAnimDurs[] = { RANKING_VIEW_HAIR_ANIM_DUR, RANKING_VIEW_TAG_ANIM_DUR, RANKING_VIEW_BT_BACK_ANIM_DUR };
				float hViewsPixelsOut[] = { RANKING_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN, RANKING_VIEW_TAG_PIXELS_OUTSIDE_SCREEN, RANKING_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN };

				float finalX = SCREEN_WIDTH - hViews[ currAnimatingView ].frame.size.width + hViewsPixelsOut[ currAnimatingView ];
				float nextX = hViews[ currAnimatingView ].frame.origin.x - ( ( timeElapsed * hViews[ currAnimatingView ].frame.size.width ) / hViewsAnimDurs[ currAnimatingView ] );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlag )
				{
					soundFlag = true;
					
					if( currState == RANKING_VIEW_STATE_SHOWING_STEP_0 )
						[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_HAIR_MOVE AtIndex: SOUND_INDEX_HAIR_MOVE Looping: false];
					else if( currState == RANKING_VIEW_STATE_SHOWING_STEP_4 )
						[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: ( RankingViewState )( currState + 1 )];
					
					soundFlag = false;
				}

				[hViews[ currAnimatingView ] setFrame: CGRectMake( nextX, hViews[ currAnimatingView ].frame.origin.y, hViews[ currAnimatingView ].frame.size.width, hViews[ currAnimatingView ].frame.size.height )];
			}
			break;

		case RANKING_VIEW_STATE_SHOWING_STEP_1:
			{
				uint8 done = 0;
				float diff = 0.0f;
				UIImageView* iconsArray[ APP_N_RECORDS ] = { hImg1stIcon, hImg2ndIcon, hImg3rdIcon, hImg4thIcon, hImg5thIcon };
				
				for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
				{
					float nextAlpha = iconsArray[ i ].alpha + ( timeElapsed / ( RANKING_VIEW_ICONS_ANIM_DUR + diff ));
					
					if( nextAlpha > 1.0f )
					{
						nextAlpha = 1.0f;
						++done;
					}

					[iconsArray[ i ] setAlpha: nextAlpha];
					
					diff += RANKING_VIEW_ICONS_ANIM_DIFF;
				}

				if( done == APP_N_RECORDS )
					[self setState: RANKING_VIEW_STATE_SHOWING_STEP_2 ];
			}
			break;
			
		case RANKING_VIEW_STATE_SHOWING_STEP_3:
			{
				uint8 done = 0;
				float diff = 0.0f;
				UIImageView* ptsBkgArray[ APP_N_RECORDS ] = { hImg1stPtsBkg, hImg2ndPtsBkg, hImg3rdPtsBkg, hImg4thPtsBkg, hImg5thPtsBkg };
				UIImageView* ptsTitlesArray[ APP_N_RECORDS ] = { hImg1stPtsTitle, hImg2ndPtsTitle, hImg3rdPtsTitle, hImg4thPtsTitle, hImg5thPtsTitle };

				for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
				{
					float imageWidth = ptsBkgArray[ i ].image.size.width;
					
					CGRect currFrame = ptsBkgArray[ i ].frame;
					float nextWidth = currFrame.size.width + ( ( timeElapsed * imageWidth ) / ( RANKING_VIEW_PTS_BKGS_ANIM_DUR + diff ));
					
					if( nextWidth > imageWidth )
					{
						nextWidth = imageWidth;
						++done;
					}

					[ptsBkgArray[ i ] setFrame: CGRectMake( currFrame.origin.x, currFrame.origin.y, nextWidth, currFrame.size.height )];
					
					float titleViewportWidth = currFrame.origin.x + nextWidth - ptsTitlesArray[ i ].frame.origin.x;
					if( titleViewportWidth <= 0.0f )
						titleViewportWidth = 0.0f;
					else if( titleViewportWidth > ptsTitlesArray[ i ].image.size.width )
						titleViewportWidth = ptsTitlesArray[ i ].image.size.width;
					
					[ptsTitlesArray[ i ] setFrame: CGRectMake( ptsTitlesArray[ i ].frame.origin.x, ptsTitlesArray[ i ].frame.origin.y, titleViewportWidth, ptsTitlesArray[ i ].frame.size.height )];
					
					for( uint8 j = 0 ; j < RANKING_LABELS_N_CHARS ; ++j )
					{ 
						CGRect charFrame = labelPts[ i ][ j ].frame;
						float charViewportWidth = currFrame.origin.x + nextWidth - charFrame.origin.x;
						if( charViewportWidth <= 0.0f )
						{
							charViewportWidth = 0.0f;
						}
						else
						{
							// &3 == %4
							if( ( ( j + 1 ) & 3 ) == 0 )
							{
								if( charViewportWidth > GOOD_GIRL_DOT_FRAME_WIDTH )
									charViewportWidth = GOOD_GIRL_DOT_FRAME_WIDTH;
							}
							else
							{
								if( charViewportWidth > GOOD_GIRL_NUM_FRAME_WIDTH )
									charViewportWidth = GOOD_GIRL_NUM_FRAME_WIDTH;
							}
						}
						[labelPts[i][j] setFrame: CGRectMake( charFrame.origin.x, charFrame.origin.y, charViewportWidth, charFrame.size.height )];
					}
					
					diff += RANKING_VIEW_PTS_BKGS_ANIM_DIFF;
				}

				if( done == APP_N_RECORDS )
					[self setState: RANKING_VIEW_STATE_SHOWING_STEP_4 ];
			}
			break;
			
		case RANKING_VIEW_STATE_SHOWN:
			if( blinkingRecordIndex >= 0 )
			{
				blinkTimeCounter += timeElapsed;
						
				if( blinkTimeCounter > RANKING_VIEW_BLINKING_RECORD_DUR )
				{
					blinkTimeCounter = static_cast< float >( fmod( blinkTimeCounter, RANKING_VIEW_BLINKING_RECORD_DUR ) );
					[self setRecord: blinkingRecordIndex Visible: ![self isRecordVisible: blinkingRecordIndex]];
				}
			}
			break;

		case RANKING_VIEW_STATE_HIDING:
			// OBS : Não estamos utilizando...
			break;
	}
}

/*==============================================================================================

MENSAGEM isRecordVisible
	Indica se o label do recorde está visível.

==============================================================================================*/

- ( bool ) isRecordVisible:( int8 )recordIndex
{
	return labelPts[ recordIndex ][ 0 ].hidden == NO;
}

/*==============================================================================================

MENSAGEM setRecord: Visible:
	Determina se o label do recorde deve ficar visível.

==============================================================================================*/

- ( void ) setRecord:( int8 )recordIndex Visible:( bool )visible
{
	uint8 total = maxPointsLen + nDots;
	for( uint8 i = 0 ; i < total ; ++i )
		[labelPts[ recordIndex ][ i ] setHidden: visible ? NO : YES];
}

/*==============================================================================================

MENSAGEM setState
	Determina o estado da view.

==============================================================================================*/

- ( void ) setState:( RankingViewState ) state
{
	currState = state;
	
	UIImageView* iconsArray[ APP_N_RECORDS ] = { hImg1stIcon, hImg2ndIcon, hImg3rdIcon, hImg4thIcon, hImg5thIcon };
	UIImageView* ptsBkgArray[ APP_N_RECORDS ] = { hImg1stPtsBkg, hImg2ndPtsBkg, hImg3rdPtsBkg, hImg4thPtsBkg, hImg5thPtsBkg };
	UIImageView* ptsTitlesArray[ APP_N_RECORDS ] = { hImg1stPtsTitle, hImg2ndPtsTitle, hImg3rdPtsTitle, hImg4thPtsTitle, hImg5thPtsTitle };
	
	switch( state )
	{
		case RANKING_VIEW_STATE_HIDDEN:
			{	
				for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
				{
					[iconsArray[ i ] setAlpha: 0.0f];
					[ptsTitlesArray[ i ] setFrame: CGRectMake( ptsTitlesArray[ i ].frame.origin.x, ptsTitlesArray[ i ].frame.origin.y, 0.0f, ptsTitlesArray[ i ].frame.size.height )];

					CGRect currFrame = ptsBkgArray[ i ].frame;
					[ptsBkgArray[ i ] setFrame: CGRectMake( currFrame.origin.x, currFrame.origin.y, 0.0f, currFrame.size.height )];

					for( uint8 j = 0 ; j < RANKING_LABELS_N_CHARS ; ++j )
					{
						CGRect labelFrame = [labelPts[i][j] frame];
						[labelPts[i][j] setFrame: CGRectMake( labelFrame.origin.x, labelFrame.origin.y, 0.0f, labelFrame.size.height )];
					}
				}

				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height ) ];
				[hImgTag setFrame: CGRectMake( SCREEN_WIDTH, hImgTag.frame.origin.y, hImgTag.frame.size.width, hImgTag.frame.size.height ) ];
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH, hImgHair.frame.origin.y, hImgHair.frame.size.width, hImgHair.frame.size.height ) ];
				
				soundFlag = false;

				[self suspend];
			}
			break;

		case RANKING_VIEW_STATE_SHOWN:
			{
				for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
				{
					[iconsArray[ i ] setAlpha: 1.0f];
					[ptsTitlesArray[ i ] setFrame: CGRectMake( ptsTitlesArray[ i ].frame.origin.x, ptsTitlesArray[ i ].frame.origin.y, ptsTitlesArray[ i ].image.size.width, ptsTitlesArray[ i ].frame.size.height )];

					CGRect currFrame = ptsBkgArray[ i ].frame;
					[ptsBkgArray[ i ] setFrame: CGRectMake( currFrame.origin.x, currFrame.origin.y, ptsBkgArray[ i ].image.size.width, currFrame.size.height )];

					for( uint8 j = 0 ; j < RANKING_LABELS_N_CHARS ; ++j )
					{
						CGRect labelFrame = [labelPts[i][j] frame];
						
						// &3 == %4
						if( ( ( j + 1 ) & 3 ) == 0 )
							[labelPts[i][j] setFrame: CGRectMake( labelFrame.origin.x, labelFrame.origin.y, GOOD_GIRL_DOT_FRAME_WIDTH, labelFrame.size.height )];
						else
							[labelPts[i][j] setFrame: CGRectMake( labelFrame.origin.x, labelFrame.origin.y, GOOD_GIRL_NUM_FRAME_WIDTH, labelFrame.size.height )];
					}
				}
				
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH - hBtBack.frame.size.width + RANKING_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height ) ];
				[hImgTag setFrame: CGRectMake( SCREEN_WIDTH - hImgTag.frame.size.width + RANKING_VIEW_TAG_PIXELS_OUTSIDE_SCREEN, hImgTag.frame.origin.y, hImgTag.frame.size.width, hImgTag.frame.size.height ) ];
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH - hImgHair.frame.size.width + RANKING_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN, hImgHair.frame.origin.y, hImgHair.frame.size.width, hImgHair.frame.size.height ) ];
				
				if( blinkingRecordIndex < 0 )
					[self suspend];
				else
					blinkTimeCounter = 0.0f;
			}
			break;

		case RANKING_VIEW_STATE_SHOWING_STEP_0:
		case RANKING_VIEW_STATE_HIDING:
			[self resume];
			break;
			
		case RANKING_VIEW_STATE_SHOWING_STEP_1:
		case RANKING_VIEW_STATE_SHOWING_STEP_2:
		case RANKING_VIEW_STATE_SHOWING_STEP_3:
			break;
	}
}

/*==============================================================================================

MENSAGEM updateRecordsLabels
	Monta os labels das pontuações.

==============================================================================================*/

- ( void ) updateRankingLabels:( const uint32* ) pRecords
{
	// Carrega as imagens dos números e do separador decimal
	char path[ PATH_MAX ];
	char fileName[] = { 'g', 'g', ' ', '\0' };
	
	UIImage* charsImages[ RANKING_LABELS_N_CHARS ];
	for( uint8 i = 0 ; i < RANKING_LABELS_N_CHARS - 1 ; ++i )
	{
		fileName[2] = '0' + i;
		charsImages[ i ] = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( path, PATH_MAX, fileName, "png" ))];
	}
	charsImages[ RANKING_LABELS_N_CHARS - 1 ] = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( path, PATH_MAX, "dot", "png" ))];
	
	// Preenche os labels
	int8 charImgIndex;
	char buffer[ UPDATE_RANKING_LABELS_BUFFER_LEN ];

	for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		// 0* => Garante que, no mínimo, pRecords[i] será impresso com 'maxPointsLen' dígitos, preenchendo números menores com 0s à esquerda
		snprintf( buffer, UPDATE_RANKING_LABELS_BUFFER_LEN, "%0*d", maxPointsLen, pRecords[i] > static_cast< int32 >( SCORE_MAX ) ? static_cast< int32 >( SCORE_MAX ) : pRecords[i] );

		int16 currNumberIndex = 0;
		for( uint8 j = 0 ; j < maxPointsLen + nDots ; ++j )
		{
			// &3 == %4
			if( ( ( j + 1 ) & 3 ) == 0 )
			{
				charImgIndex = RANKING_LABELS_N_CHARS - 1;
			}
			else
			{
				charImgIndex = buffer[ currNumberIndex++ ] - '0'; 
			}

			[labelPts[ i ][ j ] setImage: charsImages[ charImgIndex ] ];
		}
	}
}

/*==============================================================================================

MENSAGEM onBtPressed
	Indica que o usuário selecionou uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtPressed:( id )hButton
{
	if( currState != RANKING_VIEW_STATE_SHOWN )
		return;

	if( hButton == hBtBack )
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM onBeforeTransition:setRecords:
	Método chamado antes de iniciarmos uma transição para esta view.

==============================================================================================*/

- ( void )onBeforeTransition:( int8 )blinkingRecord setRecords:( const uint32* )records
{
	blinkTimeCounter = 0.0f;
	
	#if DEBUG && IS_CURR_TEST( TEST_BLINKING_RECORD )
		blinkingRecordIndex = 0;
	#else
		blinkingRecordIndex = blinkingRecord;
	#endif

	[self updateRankingLabels: records];

	[self setState: RANKING_VIEW_STATE_HIDDEN];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	[self setState: RANKING_VIEW_STATE_SHOWING_STEP_0];
}

// Fim da implementação da classe
@end

