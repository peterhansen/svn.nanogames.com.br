#include "FieldMarks.h"

#include "Config.h"
#include "FreeKickAppDelegate.h"
#include "GameUtils.h"

#include "Exceptions.h"
#include "MathFuncs.h"
#include "ResourceManager.h"
#include "Scene.h"

// Configurações do point sprite utilizado para renderizar os pontos das marcações
#define MIN_POINT_SIZE 3.0f
#define MAX_POINT_SIZE 18.0f

// Número de linhas utilizadas para fazer as marcações do campo
#define N_FIELD_LINES 10

// Incremento utilizado nos eixos X e Y no algoritmo de geração de linhas de Bresenham
#define LINE_X_STEP MIN_POINT_SIZE
#define LINE_Y_STEP MIN_POINT_SIZE

// Incremento de ângulo utilizado na criação de círculos
#define FIELD_MARKS_CIRCLE_ANGLE_N_POINTS 720.0f

// Macro para dar melhor leitura ao código
#define SET_PIXEL( vector, x, y )		\
		{								\
			vector.push_back( ( x ) );	\
			vector.push_back( ( y ) );	\
		}

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

FieldMarks::FieldMarks( void )
		   : Object(), bufferSize( 0 ), pBuffer( NULL ), marksColor( static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ) ), texture( NULL )
{
	build();
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

FieldMarks::~FieldMarks( void )
{
	DELETE_VEC( pBuffer );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool FieldMarks::render( void )
{
	// Verifica se podemos renderizar o objeto
	if( !isVisible() )
		return false;

	// OpenGL uses the following formula to compute the current size (Ss) of a point
	// sprite:
	//
	// Ss = Vh * Si * sqrt(1/(A + B * De + C *( De2 )))
	//
	// Input values for this formula are the height of the viewport (Vh), the initial
	// point size (Si), the distance from the eye to the position (De) and three
	// user-defined values (A, B and C) which specify a function that is either static,
	// linear or quadratic
	float staticFunc[] =  { 1.0f, 0.0f, 0.0f };
	glPointParameterfv( GL_POINT_DISTANCE_ATTENUATION, staticFunc );
	
	// The alpha of a point is calculated to allow the fading of points 
    // instead of shrinking them past a defined threshold size. The threshold 
    // is defined by GL_POINT_FADE_THRESHOLD_SIZE_ARB and is not clamped to 
    // the minimum and maximum point sizes.
	glPointParameterf( GL_POINT_FADE_THRESHOLD_SIZE, 0.0f );
	
	// Define os tamanhos limites dos pontos
	glPointParameterf( GL_POINT_SIZE_MIN, MIN_POINT_SIZE );
	glPointParameterf( GL_POINT_SIZE_MAX, MAX_POINT_SIZE );

	// Determina o tamanho atual do ponto
	Scene* pCurrScene = [[(( FreeKickAppDelegate* )[ApplicationManager GetInstance] ) hGLView] getCurrScene];
	Camera* pCurrCamera = pCurrScene->getCurrentCamera();

	glPointSize( NanoMath::lerp( ( pCurrCamera->getZoomFactor() - ZOOM_MIN ) / ( ZOOM_MAX - ZOOM_MIN ), MIN_POINT_SIZE, MAX_POINT_SIZE ) );

	// Ativa o blending
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE );
	
	// Carrega a textura
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	texture->load();
	
    // Specify point sprite texture coordinate replacement mode for each 
    // texture unit
    glTexEnvi( GL_POINT_SPRITE_OES, GL_COORD_REPLACE_OES, GL_TRUE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	
	glEnable( GL_POINT_SPRITE_OES );
	
	glEnableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	glDisableClientState( GL_NORMAL_ARRAY );
	
	glColor4ub( 255, 255, 255, 255 );
	
	// Renderiza o point sprite
	glVertexPointer( 2, GL_SHORT, 0, pBuffer );
	glDrawArrays( GL_POINTS, 0, bufferSize >> 1 );
	
	glDisableClientState( GL_VERTEX_ARRAY );
	glDisable( GL_POINT_SPRITE_OES );
	
	texture->unload();
	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );
	
	// Restaura o valor de GL_BLEND
	if( blendWasEnabled )
		glEnable( GL_BLEND );
	else
		glDisable( GL_BLEND );
	
	return true;
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

void FieldMarks::build( void )
{
	{ // Evita problemas com os gotos

		#if CONFIG_TEXTURES_16_BIT
			texture = ResourceManager::Get16BitTexture2D( "grass" );
		#else
			texture = ResourceManager::GetTexture2D( "grass" );
		#endif
		
		if( !texture )
			goto Error;
		
		// Cria um vector auxiliar para conter todos os pontos gerados
		std::vector< int16 > tempBuffer;
		
		// Cria as linhas do campo
		Point3f linesCoords[ N_FIELD_LINES ][ 2 ] =
		{
			// Lateral esquerda
			GameUtils::isoGlobalToPixels( - ( REAL_FIELD_WIDTH * 0.5f ), 0.0f, 0.0f ), GameUtils::isoGlobalToPixels( - ( REAL_FIELD_WIDTH * 0.5f ), 0.0f, REAL_FIELD_DEPTH ),
			
			// Linha de fundo
			GameUtils::isoGlobalToPixels( - ( REAL_FIELD_WIDTH * 0.5f ), 0.0f, 0.0f ), GameUtils::isoGlobalToPixels( REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f ),
			
			// Pequena área - frente
			GameUtils::isoGlobalToPixels( - ( REAL_SMALL_AREA_WIDTH * 0.5f ), 0.0f, REAL_SMALL_AREA_DEPTH ), GameUtils::isoGlobalToPixels( REAL_SMALL_AREA_WIDTH * 0.5f, 0.0f, REAL_SMALL_AREA_DEPTH ),
			
			// Pequena área - esquerda
			GameUtils::isoGlobalToPixels( - ( REAL_SMALL_AREA_WIDTH * 0.5f ), 0.0f, REAL_SMALL_AREA_DEPTH ), GameUtils::isoGlobalToPixels( - ( REAL_SMALL_AREA_WIDTH * 0.5f ), 0.0f, 0.0f ),
			
			// Pequena área - direita
			GameUtils::isoGlobalToPixels( REAL_SMALL_AREA_WIDTH * 0.5f, 0.0f, REAL_SMALL_AREA_DEPTH ), GameUtils::isoGlobalToPixels( REAL_SMALL_AREA_WIDTH * 0.5f, 0.0f, 0.0f ),
			
			// Grande área - frente
			GameUtils::isoGlobalToPixels( - ( REAL_BIG_AREA_WIDTH * 0.5f ), 0.0f, REAL_BIG_AREA_DEPTH ), GameUtils::isoGlobalToPixels( REAL_BIG_AREA_WIDTH * 0.5f, 0.0f, REAL_BIG_AREA_DEPTH ),
			
			// Grande área - esquerda
			GameUtils::isoGlobalToPixels( - ( REAL_BIG_AREA_WIDTH * 0.5f ), 0.0f, REAL_BIG_AREA_DEPTH ), GameUtils::isoGlobalToPixels( - ( REAL_BIG_AREA_WIDTH * 0.5f ), 0.0f, 0.0f ),
			
			// Grande área - direita		
			GameUtils::isoGlobalToPixels( REAL_BIG_AREA_WIDTH * 0.5f, 0.0f, REAL_BIG_AREA_DEPTH ), GameUtils::isoGlobalToPixels( REAL_BIG_AREA_WIDTH * 0.5f, 0.0f, 0.0f ),
			
			// Meio de campo
			GameUtils::isoGlobalToPixels( - ( REAL_FIELD_WIDTH * 0.5f ), 0.0f, REAL_FIELD_DEPTH * 0.5f ), GameUtils::isoGlobalToPixels( REAL_FIELD_WIDTH * 0.5f, 0.0f, REAL_FIELD_DEPTH * 0.5f ),
			
			// Lateral direita
			GameUtils::isoGlobalToPixels( REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f ), GameUtils::isoGlobalToPixels( REAL_FIELD_WIDTH * 0.5f, 0.0f, REAL_FIELD_DEPTH )
		};

		for( uint8 i = 0 ; i < N_FIELD_LINES ; ++i )
			PreProcessLine( tempBuffer, linesCoords[i][0].x, linesCoords[i][0].y, linesCoords[i][1].x, linesCoords[i][1].y );
		
		// Cria o grande círculo
		Point3f p( 0.0f, 0.0f, REAL_FIELD_DEPTH * 0.5f );
		PreProcessCircle( tempBuffer, p, REAL_FIELD_CENTER_CIRCLE_RADIUS, FIELD_MARKS_CIRCLE_ANGLE_N_POINTS );

		// Cria o centro do campo
		PreProcessCircle( tempBuffer, p, 0.16f, 20.0f );

		// Cria a marca do pênalti
		p.set( 0.0f, 0.0f, REAL_PENALTY_TO_GOAL );
		PreProcessCircle( tempBuffer, p, 0.05f, 5.0f );
		
		// Cria a meia lua
		float auxStartAngle = 37.0f;
		p.z += 0.80f;
		PreProcessCircle( tempBuffer, p, REAL_FIELD_HALF_ARC_RADIUS, 600.0f, auxStartAngle, 180.0f - auxStartAngle );

		// Cria o quarto de lua do escanteio direito
		p.set( -REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f );
		PreProcessCircle( tempBuffer, p, REAL_FIELD_CORNER_QUARTER_RADIUS, 85.0f, 0.0f, 90.0f );

		// Cria o quarto de lua do escanteio esquerdo
		p.set( REAL_FIELD_WIDTH * 0.5f, 0.0f, 0.0f );
		PreProcessCircle( tempBuffer, p, REAL_FIELD_CORNER_QUARTER_RADIUS, 60.0f, 90.0f, 180.0f );
		
		// Armazena tudo em uma estrutura mais simples
		bufferSize = tempBuffer.size();
		pBuffer = new int16[ bufferSize ];
		if( !pBuffer )
			goto Error;
		
		// OBS: Seria melhor utilizar copy< , , > ...
		for( int32 i = 0 ; i < bufferSize ; ++i )
			pBuffer[ i ] = tempBuffer[ i ];
		
	} // Evita problemas com os gotos
	
	return;
	
	// Label de tratamento de erros
	Error:
		#if TARGET_IPHONE_SIMULATOR
			throw ConstructorException( "FieldMarks::build( void ) : Unable to create object" );
		#else
			throw ConstructorException();
		#endif
}

/*==============================================================================================

MÉTODO PreProcessLine
	Renderiza uma linha, ponto a ponto, utilizando o algoritmo de Bresenham.

==============================================================================================*/

#define SWAP( x, y ) ( x ^= y ^= x ^= y )

void FieldMarks::PreProcessLine( std::vector< int16 >& tempBuffer, int32 x0, int32 y0, int32 x1, int32 y1 )
{
	int32 dx = x1 - x0;
	int32 dy = y1 - y0;
	int32 steep = ( abs( dy ) >= abs( dx ) );

	if( steep )
	{
		SWAP( x0, y0 );
		SWAP( x1, y1 );

		dx = x1 - x0;
		dy = y1 - y0;
	}
	
	int32 nPoints = abs( steep ? ( dy / LINE_Y_STEP ) : ( dx / LINE_X_STEP ) ) << 1;

	int32 xstep = LINE_X_STEP;
	if( dx < 0 )
	{
		xstep = -LINE_X_STEP;
		dx = -dx;
	}

	int32 ystep = LINE_Y_STEP;
	if( dy < 0 )
	{
		ystep = -LINE_Y_STEP;		
		dy = -dy; 
	}

	int32 TwoDy = dy << 1;
	int32 TwoDyTwoDx = TwoDy - ( dx << 1 );
	int32 e = TwoDy - dx;
	int32 counter = 0;
	
	//OLD
	// Modifica a condição de parada de acordo com o tamanho do passo
	//x1 = x0 + ( ( dx / X_STEP ) * xstep );

	if( steep )
	{
		// OLD : Condição de parada original
		// for( ; x0 != x1 ; x0 += xstep )
		for( ; counter < nPoints ; x0 += xstep, counter += 2 )
		{
			SET_PIXEL( tempBuffer, y0, x0 );

			if( e > 0 )
			{
				e += TwoDyTwoDx;
				y0 += ystep;
			}
			else
			{
				e += TwoDy;
			}
		}
	}
	else
	{
		// OLD : Condição de parada original
		// for( ; x0 != x1 ; x0 += xstep )
		for( ; counter < nPoints ; x0 += xstep, counter += 2 )
		{
			SET_PIXEL( tempBuffer, x0, y0 );

			if( e > 0 )
			{
				e += TwoDyTwoDx;
				y0 += ystep;
			}
			else
			{
				e += TwoDy;
			}
		}
	}
}

#undef SWAP( x, y )

/*==============================================================================================

MÉTODO PreProcessCircle
	Renderiza um círculo, ponto a ponto, utilizando o algoritmo de Bresenham.

==============================================================================================*/

void FieldMarks::PreProcessCircle( std::vector< int16 >& tempBuffer, const Point3f& center, float radius, float lod, float startAngle, float endAngle )
{
	startAngle = NanoMath::clamp( startAngle, 0.0f, 360.0f );
	endAngle = NanoMath::clamp( endAngle, 0.0f, 360.0f );
	
	lod = 360.0f / lod;
	
	float cos, sin;
	Point3f p;

	while( startAngle < endAngle )
	{
		cos = cosf( NanoMath::degreesToRadians( startAngle ) );
		sin = sinf( NanoMath::degreesToRadians( startAngle ) );

		p = center + Point3f( radius * cos, 0.0f, radius * sin );
		p = GameUtils::isoGlobalToPixels( p.x, p.y, p.z );

		SET_PIXEL( tempBuffer, p.x, p.y );
		startAngle += lod;
	}
}

/*==============================================================================================

MÉTODO PreProcessArc
	Renderiza um arco de círculo, ponto a ponto, utilizando o algoritmo de Bresenham.

==============================================================================================*/

void FieldMarks::PreProcessArc( std::vector< int16 >& tempBuffer, int32 centerX, int32 centerY, int32 radius, int32 startAngle, int32 endAngle )
{
	// Standard Midpoint Circle algorithm
	int32 p = ( 5 - ( radius << 2 ) ) >> 2;
	int32 x = 0;
	int32 y = radius;

	KeepIfArcPoint( tempBuffer, centerX, centerY, x, y, startAngle, endAngle );

	while( x <= y )
	{
		++x;
		if( p < 0 )
		{
			p += ( x << 1 ) + 1;
		}
		else
		{
			--y;
			p += ( ( x - y ) << 1 ) + 1;
		}
		KeepIfArcPoint( tempBuffer, centerX, centerY, x, y, startAngle, endAngle );
	}
}

/*==============================================================================================

MÉTODO KeepIfArcPoint
	Verifica se o ponto x, y pertence ao arco que deve ser criado.

==============================================================================================*/

void FieldMarks::KeepIfArcPoint( std::vector< int16 >& tempBuffer, int32 centerX, int32 centerY, int32 x, int32 y, int32 startAngle, int32 endAngle )
{
	// Calculate the angle the current point makes with the circle center
	float radians = static_cast< float >( atan2( y, x ) );
	int32 angle = static_cast< int32 >( NanoMath::radiansToDegrees( radians ) );

	// Draw the circle points as long as they lie in the range specified
	if( x < y )
	{
		// draw point in range 0 to 45 degrees
		if( 90 - angle >= startAngle && 90 - angle <= endAngle )
			SET_PIXEL( tempBuffer, centerX - y, centerY - x );

		// draw point in range 45 to 90 degrees
		if( angle >= startAngle && angle <= endAngle)
			SET_PIXEL( tempBuffer, centerX - x, centerY - y );

		// draw point in range 90 to 135 degrees
		if( 180 - angle >= startAngle && 180 - angle <= endAngle )
			SET_PIXEL( tempBuffer, centerX + x, centerY - y );

		// draw point in range 135 to 180 degrees
		if( angle + 90 >= startAngle && angle + 90 <= endAngle )
			SET_PIXEL( tempBuffer, centerX + y, centerY - x );

		// draw point in range 180 to 225 degrees
		if( 270 - angle >= startAngle && 270 - angle <= endAngle )
			SET_PIXEL( tempBuffer, centerX + y, centerY + x );

		// draw point in range 225 to 270 degrees
		if( angle + 180 >= startAngle && angle + 180 <= endAngle )
			SET_PIXEL( tempBuffer, centerX + x, centerY + y );

		// draw point in range 270 to 315 degrees
		if( 360 - angle >= startAngle && 360 - angle <= endAngle )
			SET_PIXEL( tempBuffer, centerX - x, centerY + y );

		// draw point in range 315 to 360 degrees
		if( angle + 270 >= startAngle && angle + 270 <= endAngle )
			SET_PIXEL( tempBuffer, centerX - y, centerY + x );
	}
}
