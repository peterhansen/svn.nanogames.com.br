/*
 *  diabinho.h
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef DEVIL_H
#define DEVIL_H 1

#include "GenericDoll.h"

/*
 cada classe de um boneco vai ter como dados as animações de zona normal e gde, assim como a imagem do rosto do boneco de modo estático. 
 
 */


class Sprite;

class Diabinho : public GenericDoll {

public:
	
	Diabinho();
	
	~Diabinho(){
#if DEBUG
		NSLog(@"deletando diabinho");
		
#endif
		
	};
	
	
	Sprite* getLineHPic( void );	

	Sprite* getLineVPic( void );	

	static void LoadAllImages( void );
	
	static void RemoveAllImages( void );
	
	Sprite* getZoneSprt( void );
	
	Sprite* getBigZoneSprt( void );
	
protected:

	static 	Sprite* bigZoneSpt;
	static  Sprite* ZoneSpt;
	
};



inline Sprite* Diabinho::getZoneSprt( void ){
	return ZoneSpt;
}

inline Sprite* Diabinho::getBigZoneSprt( void ){
	return ZoneSpt;
}


#endif