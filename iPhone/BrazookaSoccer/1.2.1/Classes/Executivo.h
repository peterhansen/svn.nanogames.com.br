/*
 *  Executivo.h
 *  dotGame
 *
 *  Created by Max on 2/1/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef EXECUTIVE_H
#define EXECUTIVE_H 1

#include "GenericDoll.h"

/*
 cada classe de um boneco vai ter como dados as animações de zona normal e gde, assim como a imagem do rosto do boneco de modo estático. 
 
 */


class Sprite;

class Executivo : public GenericDoll {

public:
	
	Executivo();
	
	~Executivo(){
#if DEBUG
		NSLog(@"deletando executivo");
		
#endif
	};
	
	static void LoadAllImages( void );
	
	static void RemoveAllImages( void );
	
	Sprite* getZoneSprt( void );
	
	Sprite* getBigZoneSprt( void );
	
	Sprite* getLineHPic( void );	

	Sprite* getLineVPic( void );	
protected:

	static 	Sprite* bigZoneSpt;

	static  Sprite* ZoneSpt;
	
};



inline Sprite* Executivo::getZoneSprt( void ){
	return ZoneSpt;
}

inline Sprite* Executivo::getBigZoneSprt( void ){
	return ZoneSpt;
}


#endif