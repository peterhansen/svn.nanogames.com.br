/*
 *  PUBomb.mm
 *  dotGame
 *
 *  Created by Max on 2/10/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "PUBomb.h"
#include "Random.h"
#include "DotGame.h"

#define MINIMAL_TIME_TO_EXPLODE 0
#define MAXIMAL_TIME_TO_EXPLODE 6//5

PUBomb::PUBomb( PowerUpListener *p ):PowerUp( POWER_UP_TYPE_BOMB,  "puGen" /*depois colocar aqui a imagem da bomba*/,p),
timeToExplode( -1 )
{
	if( !buildBomb() ){
#if DEBUG
		NSLog(@"power up não criado");
#endif
	}
	
}

void PUBomb::onRelease( void ){
	
	DotGame* dg = static_cast< DotGame* > ( getPowerUpListener() );
	
	ZoneGroup *z = dg->getZoneGroup();
	LineGroup *l = dg->getLineGroup();
	
	switch ( sizeBomb ) {
		case BOMB_SIZE_SMALL :
			//l->resetLine(  );
			break;
		case BOMB_SIZE_MEDIUM :
			
			break;
		case BOMB_SIZE_BIG :
			
			break;
	}
	
//	updatePowerUp(); 
//	PowerUp::onRelease();
	//pListener->PowerUpEffect( this );
}


void PUBomb::gainedByPlayer( Player* p ){
	timeToExplode = Random::GetInt( MINIMAL_TIME_TO_EXPLODE, MAXIMAL_TIME_TO_EXPLODE );
}

bool PUBomb::buildBomb( void ){
#if DEBUG
	setName("pwr up bomba");
#endif
	timeToExplode = Random::GetInt( MINIMAL_TIME_TO_EXPLODE, MAXIMAL_TIME_TO_EXPLODE );
	
	switch ( Random::GetInt( 0, 2 ) ) {
		case 0:
			sizeBomb = BOMB_SIZE_SMALL;
			break;
		case 1:
			sizeBomb = BOMB_SIZE_MEDIUM;
			break;
		case 2:
			sizeBomb =	BOMB_SIZE_BIG;		
			break;
			
	}
	return true;
	
}

void PUBomb::updatePowerUp( void ){
	if( timeToExplode <= 0 ){
		onRelease();
		return;
	}else {
		timeToExplode--;
	}
}
