/*
 *  AccelerometerListener.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/9/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACCELEROMETER_LISTENER_H
#define ACCELEROMETER_LISTENER_H 1

// TASK : Agrupar / derivar esta classe em / de EventListener !!!!!!!!!!

#include "Point3f.h"

// Modos de reportação dos acelerômetros
typedef enum AccelerometerReportMode
{
	ACC_REPORT_MODE_RAW = 0,
	ACC_REPORT_MODE_GRAVITY,
	ACC_REPORT_MODE_GRAVITY_VEC,
	ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC,
	ACC_REPORT_MODE_MOVEMENT,
	ACC_REPORT_MODE_AVERAGE,
	ACC_REPORT_MODE_POLINOMIAL_2,
	ACC_REPORT_MODE_POLINOMIAL_3,
	ACC_REPORT_MODE_POLINOMIAL_4
} AccelerometerReportMode;

class AccelerometerListener
{
	public:
		// Destrutor
		virtual ~AccelerometerListener( void );

		// Recebe os eventos do acelerômetro
		virtual void onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration ) = 0;
	
		// Determina a porcentagem da aceleração que corresponde ao efeito da gravidade
		inline void setGravityFilter( float filter ) { gravityFilter = filter; };
	
		// Obtém a porcentagem da aceleração que corresponde ao efeito da gravidade
		inline float getGravityFilter( void ) const { return gravityFilter; };
	
		// Determina o multiplicador que será aplicado na força reportada pelos acelerômetros
		inline void setBooster( float booster ){ this->booster = booster; };
	
		// Obtém o multiplicador que está sendo aplicado na força reportada pelos acelerômetros
		inline float getBooster( void ) const { return booster; };
	
		// Determina o modo de cálculo da força
		inline void setReportMode( AccelerometerReportMode mode ) { reportMode = mode; }
	
		// Obtém o modo de cálculo da força
		inline AccelerometerReportMode getReportMode( void ) const { return reportMode; };
	
		// Determina o peso dos valores antigos (todos os valores, exceto o último reportado
		// pelos acelerômetros) utilizado no cálculo de médias
		void setAverageOldValuesWeight( float weight );
	
		// Retorna o peso dos valores antigos (todos os valores, exceto o último reportado
		// pelos acelerômetros) utilizado no cálculo de médias
		inline float getAverageOldValuesWeight( void ) const { return averageOldValuesWeight; };

	protected:
		// Construtor
		AccelerometerListener( AccelerometerReportMode mode, float booster = 1.0f, float gravityFilter = 0.1f );
	
	private:
		// Modo de reportação que este listener está utilizando
		AccelerometerReportMode reportMode;
	
		// Índice deste objeto para o gerenciador de acelerômetros
		int16 accManagerIndex;

		// Multiplicador que será aplicado na força reportada pelos acelerômetros 
		float booster;

		// Porcentagem da aceleração que corresponde ao efeito da gravidade
		float gravityFilter;
	
		// Peso dos valores antigos (todos os valores, exceto o último reportado
		// pelos acelerômetros) utilizado no cálculo de médias
		// Interfere apenas nos modos de reportação ACC_REPORT_MODE_AVERAGE_GRAVITY_VEC
		// e ACC_REPORT_MODE_AVERAGE
		float averageOldValuesWeight;
};

#endif
