/*
 *  NOProfileSelectView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_PROFILE_SELECT_VIEW_H
#define NANO_ONLINE_PROFILE_SELECT_VIEW_H

// Components
#include "NOBaseView.h"

// C++
#include <vector>

// Declarações Adiadas
//@class UICoverFlowViewController;

@interface NOProfileSelectView : NOBaseView
{
	@private
		// Perfis salvos localmente
		std::vector< NOCustomer > localProfiles;

		// Elementos da interface
		IBOutlet UIButton *hBtDelete, *hBtEdit, *hBtLogin;

//		UICoverFlowViewController *hProfilesListController;
		IBOutlet UIScrollView *hProfilesList;
	
		// Imagem que indica qual perfil está selecionado
		IBOutlet UIImageView *hProfilehighlightBlue, *hProfilehighlightPink, *hOnlineMark;
	
		// índice do perfil selecionado
		int32 currSelectedProfile;
	
		// Controla a máquina de estados da view
		int8 viewState;
}

// Método chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

@end

#endif
