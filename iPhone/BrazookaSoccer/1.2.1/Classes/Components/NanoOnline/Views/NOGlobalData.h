/*
 *  NOGlobalData.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 12/18/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_GLOBAL_DATA_H
#define NO_GLOBAL_DATA_H

#include "NanoTypes.h"
#include "NOString.h"

// Declarações adiadas
class NOConf;
class NOCustomer;
class NORanking;

// Essa classe contém os dados que devem existir durante qualquer momento da aplicação que contém o NanoOnline.
// Para isso, funciona como uma espécie de Singleton, utilizando variáveis estáticas
class NOGlobalData
{
	public:
		static NOCustomer& GetActiveProfile( void );
		static NORanking& GetRanking( void );
		static NOConf& GetConfig( void );
};

// Função auxiliar para obter textos ligados ao NanoOnline no formato C++
// As views do NanoOnline utilizam o método GetText de NOControllerView, pois este retorna strings no formato Cocoa
void GetNOText( uint16 textIndex, NOString& outText );

#endif
