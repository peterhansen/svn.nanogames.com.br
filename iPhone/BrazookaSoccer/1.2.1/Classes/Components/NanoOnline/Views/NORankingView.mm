#include "NORankingView.h"

// Components
#include "DeviceInterface.h"
#include "ObjcMacros.h"
#include "Utils.h"

// NanoOnline
#include "NOTextsIndexes.h"
#include "NOViewsIndexes.h"

// OLD
// C++
//#include <bits/stl_pair.h>

// Macros auxiliares
#define ON_ERROR( str ) [[NOControllerView sharedInstance] showError: str]

// Definições da tabela que exibe as pontuações
#define NO_RNKVIEW_TABLE_SECTION_LOCAL				0
#define NO_RNKVIEW_TABLE_SECTION_ONLINE				1
#define NO_RNKVIEW_TABLE_SECTION_ONLINE_TOP10		2
#define NO_RNKVIEW_TABLE_SECTION_ONLINE_CLOSEST		3

// Altura mínima de cada linha da tabela
#define NO_RNKVIEW_TABLE_DEFAULT_ROW_HEIGHT 38.0f

// Número de posições, abaixo da pontuação do jogador, que são exibidas
#define NO_RNKVIEW_N_BETTER_THAN_PLAYER 15

// Número de posições, acima da pontuação do jogador, que são exibidas
#define NO_RNKVIEW_N_WORSE_THAN_PLAYER NO_RNKVIEW_N_BETTER_THAN_PLAYER

// Tamanho do buffer utilizado para a formatação das strings exibidas na tabela
// de pontuações
#define NO_RNKVIEW_FORMAT_ENTRY_BUFFER_LEN 64

// Buffer para verificar quantos caracteres precisamos para armazenar a maior pontuação possível
#define NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN 32

// Identificações das subviews contidas nas células da tabela (um inteiro qualquer)
#define NO_RNKVIEW_TABLE_CELL_BKG_TAG		322
#define NO_RNKVIEW_TABLE_CELL_LABEL_TAG		545
#define NO_RNKVIEW_TABLE_CELL_CHECKBOX_TAG	667

// Offset do label da célula para as bordas horizontais da tabela
#define NO_RNKVIEW_TABLE_CELL_EXTRA_LABEL_OFFSET 10.0f

// Espaço entre o label e a checkbox da célula
#define NO_RNKVIEW_TABLE_CELL_SPACE_BETWEEN_LABEL_N_CKB 5.0f

// Configurações do tamanho da tabela quando estamos no modo RANKING_VIEW_MODE_GLOBAL
#define NO_RNKVIEW_GLOBAL_MODE_TABLE_HEIGHT 240.0f
#define NO_RNKVIEW_GLOBAL_MODE_TABLE_BORDER_HEIGHT 248.0f

// Estados da checkbox
#define CHECK_BOX_STATE_UNCHECKED	0
#define CHECK_BOX_STATE_CHECKED		1
#define CHECK_BOX_STATE_HIDDEN		2

// Número máximo de entradas na tabela
#define NO_RNKVIEW_N_TABLE_ROWS 10

// Estados da view
#define NO_RNKVIEW_STATE_UNDEFINED						-1
#define NO_RNKVIEW_STATE_LOCAL_IDLE						 0
#define NO_RNKVIEW_STATE_GLOBAL_IDLE					 1
#define NO_RNKVIEW_STATE_LOCAL_NO_RECORDS				 2
#define NO_RNKVIEW_STATE_LOCAL_SUBMITING_ROWS			 3
#define NO_RNKVIEW_STATE_LOCAL_DELETING_ROWS			 4
#define NO_RNKVIEW_STATE_GLOBAL_REQUESTING_DATA			 5
#define NO_RNKVIEW_STATE_GLOBAL_REQUEST_ERROR			 6

// Definições dos popups
#define POPUP_YES_BT_INDEX	0
#define POPUP_NO_BT_INDEX	1

#define POPUP_OK_BT_INDEX	0

// Definições dos botões do modo local
#define NO_RNKVIEW_SPACE_BETWEEN_LOCAL_MODE_BTS 10.0f

#define TOGGLE_CHECKS_BT_CHECK_TITLE	[NOControllerView GetText: NO_TXT_CHECK_ALL]
#define TOGGLE_CHECKS_BT_UNCHECK_TITLE	[NOControllerView GetText: NO_TXT_UNCHECK_ALL]

// Extensão da classe para declarar métodos privados
@interface NORankingView ( Private )

// Inicializa a view
-( bool )buildNORankingView;

// Libera a memória alocada pelo objeto
-( void )cleanNORankingView;

// OLD
// Configura as seções da tabela de pontuações
//-( void )fillTableSections;

// Cria a string que será exibida na célula da tabela de pontuações de acordo com os dados
// contidos em 'entry'
-( NSString* )getRankingStrForEntry:( const NORankingEntry& )entry AtIndex:( uint16 )index;

// Retorna a cor do texto a ser utilizada em uma determinada linha da tabela
-( UIColor* )getTxtColorForCell:( UITableViewCell* )hCell AtRow:( NSIndexPath* )hIndexPath;

// Inicializa a interface do modo RANKING_VIEW_MODE_LOCAL
-( void )initLocalMode;

// Obtém do servidor os dados necessários para preenchermos a interface do modo RANKING_VIEW_MODE_GLOBAL
-( void )initGlobalModeData;

// Inicializa a interface do modo RANKING_VIEW_MODE_GLOBAL
-( void )initGlobalModeInterface;

// Desabilita os elementos com os quais o usuário pode interagir
-( void )enableInterface:( bool )b;

// Trata a interação do usuário com as checkboxes das células
-( void )toggleCheckBox:( UIControl* )hSender;

// Cria uma célula da tabela do NanoOnline
-( UITableViewCell* )createTableCell;

// Cria uma checkbox compatível com célula da tabela do NanoOnline
-( bool )createCheckBoxInCell:( UITableViewCell* )hCell;

// Envia os recordes selecionados para o NanoOnline
-( void )submitRecords;

// Deleta os recordes selecionados pelo usuário
-( void )deleteRecords;

// Deleta os recordes já submetidos para o NanoOnline
-( void )deleteSubmittedRecords;

// Modifica o título do botão
-( void )changeBt:( UIButton* )hBt Title:( NSString* )hNewTitle;

// Desmarca todas as checkboxes, caso todas estejam marcadas. Caso contrário, marca todas as
// checkboxes que ainda não o foram
-( void )toggleChecks;

// Retorna a cor de texto padrão utilizada nesta tela
+( UIColor* )GetNanoColor;

@end

// Início da implementação da classe
@implementation NORankingView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNORankingView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNORankingView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNORankingView
	Inicializa a view.

==============================================================================================*/

-( bool )buildNORankingView
{
	// Inicializa as variáveis da classe
	// OLD tableSections = std::map< NSInteger, TableSection >();
	hCkbStateDict = nil;
	hImgChecked = nil;
	hImgUnChecked = nil;
	auxRanking = NORanking();
	noListener.setCocoaListener( self );
	
	viewState = NO_RNKVIEW_STATE_UNDEFINED;
	viewMode = RANKING_VIEW_MODE_UNDEFINED;
	
	// Armazena quantos caracteres precisamos para o maior apelido
	uint32 dummy;
	NOCustomer::GetNicknameSupportedLen( dummy, nickMaxLen );
	
	{ // Evita erros de compilação por causa dos gotos

		// Aloca os elementos da view que não serão criados pelo Interface Builder
		hCkbStateDict = [NSMutableDictionary dictionaryWithCapacity: NANO_ONLINE_RANKING_DEFAULT_MAX_ONLINE_ENTRIES];
		if( !hCkbStateDict )
			goto Error;
		
		[hCkbStateDict retain];
		
		char imgsType[] = "png", imgName[] = "nockbv";
		char buffer[ PATH_MAX ];
		hImgChecked = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, imgName, imgsType ))];
		if( !hImgChecked )
			goto Error;
		
		[hImgChecked retain];

		imgName[5] = '\0';
		hImgUnChecked = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, imgName, imgsType ))];
		if( !hImgUnChecked )
			goto Error;
		
		[hImgUnChecked retain];

		// Configura os elementos da view que não serão criados pelo Interface Builder
		
		// OLD
		//[self fillTableSections];
		
		[self enableInterface: false];

		return true;
		
	} // Evita erros de compilação por causa dos gotos

	Error:
		[self cleanNORankingView];
		return false;
}

/*==============================================================================================

MENSAGEM initViewInMode:forRanking:
	Inicializa a view com os dados do ranking.

==============================================================================================*/

-( void )initViewInMode:( NORankingViewMode )mode forRanking:( NORanking* )pRnk
{
	viewMode = mode;
	viewState = NO_RNKVIEW_STATE_UNDEFINED;
	
	pRanking = pRnk;
	
	std::vector< NOCustomer > localProfiles;
	[NOControllerView loadProfiles: localProfiles];

	std::vector< NOProfileId > profilesIds;
	std::vector< NOCustomer >::size_type nProfiles = localProfiles.size();
	for( std::vector< NOCustomer >::size_type i = 0 ; i < nProfiles ; ++i )
		profilesIds.push_back( localProfiles[i].getProfileId() );

	pRanking->setHighlightedProfilesIds( profilesIds );

	NSString* hBtActionTitle;
	switch( viewMode )
	{
		case RANKING_VIEW_MODE_LOCAL:
			hBtActionTitle = [NOControllerView GetText: NO_TXT_SUBMIT];
			[self setScreenTitle: [NOControllerView GetText: NO_TXT_LOCAL]];
			[self initLocalMode];
			break;

		case RANKING_VIEW_MODE_GLOBAL:
			hBtActionTitle = @""; // @"Show Me";
			[self setScreenTitle: [NOControllerView GetText: NO_TXT_GLOBAL]];
			
			if( pRanking )
			{
				viewState = NO_RNKVIEW_STATE_GLOBAL_REQUESTING_DATA;
				pRanking->getOnlineEntries().clear();
			}

			break;
	}
	
	[self changeBt: hBtAction Title: hBtActionTitle];
}

/*==============================================================================================

MENSAGEM fillTableSections
	Configura as seções da tabela de pontuações.

==============================================================================================*/

// OLD
//-( void )fillTableSections
//{
//	// TODOO : Obter o número correto de pontuações locais!!!
//	TableSection aux( 1, "Local Ranking" );
//	tableSections.insert( std::make_pair( NO_RNKVIEW_TABLE_SECTION_LOCAL, aux ) );
//	
//	aux.set( 0, "Online Ranking" );
//	tableSections.insert( std::make_pair( NO_RNKVIEW_TABLE_SECTION_ONLINE, aux ) );
//	
//	aux.set( 10, "Online - Top 10" );
//	tableSections.insert( std::make_pair( NO_RNKVIEW_TABLE_SECTION_ONLINE_TOP10, aux ) );
//	
//	// TODOO : Está errado! O certo é ver se possuímos NO_RNKVIEW_N_BETTER_THAN_PLAYER acima
//	// e NO_RNKVIEW_N_WORSE_THAN_PLAYER abaixo antes de determinarmos o número de linhas !!!
//	aux.set( ( NO_RNKVIEW_N_BETTER_THAN_PLAYER + NO_RNKVIEW_N_WORSE_THAN_PLAYER ) + 1, "Online - Closest to You" );
//	tableSections.insert( std::make_pair( NO_RNKVIEW_TABLE_SECTION_ONLINE_CLOSEST, aux ) );
//}

/*==============================================================================================

MENSAGEM getRankingStrForEntry:AtIndex:
	Cria a string que será exibida na célula da tabela de pontuações de acordo com os dados
contidos em 'entry'.

==============================================================================================*/

-( NSString* )getRankingStrForEntry:( const NORankingEntry& )entry AtIndex:( uint16 )index
{
	NOString nickname;
	entry.getNickname( nickname );

	int64 score;
	char buffer[ NO_RNKVIEW_FORMAT_ENTRY_BUFFER_LEN ];
	
	#ifdef NANO_ONLINE_UNICODE_SUPPORT
		snprintf( buffer, NO_RNKVIEW_FORMAT_ENTRY_BUFFER_LEN, "%0*d    %*.*ls    %*lld", static_cast< int32 >( nRecordsLen ), static_cast< int32 >( index + 1 ), static_cast< int32 >( nickMaxLen ), static_cast< int32 >( nickMaxLen ), entry.getNickname( nickname ).c_str(), static_cast< int32 >( maxPointsLen ), entry.getScore( score ) );
	#else
		snprintf( buffer, NO_RNKVIEW_FORMAT_ENTRY_BUFFER_LEN, "%0*d    %*.*s    %*lld", static_cast< int32 >( nRecordsLen ), static_cast< int32 >( index + 1 ), static_cast< int32 >( nickMaxLen ), static_cast< int32 >( nickMaxLen ), entry.getNickname( nickname ).c_str(), static_cast< int32 >( maxPointsLen ), entry.getScore( score ) );
	#endif

	return CHAR_ARRAY_TO_NSSTRING( buffer );
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//-( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM initLocalMode
	Inicializa a interface do modo RANKING_VIEW_MODE_LOCAL.

==============================================================================================*/

-( void )initLocalMode
{
	if( !pRanking )
		return;

	[hGoldMedal setUserInteractionEnabled: NO];
	[hGoldMedal setHidden: YES];
	[h1stNick setUserInteractionEnabled: NO];
	[h1stNick setEnabled: NO];
	[h1stNick setHidden: YES];
	[h1stPoints setUserInteractionEnabled: NO];
	[h1stPoints setEnabled: NO];
	[h1stPoints setHidden: YES];

	[hSilverMedal setUserInteractionEnabled: NO];
	[hSilverMedal setHidden: YES];
	[h2ndNick setUserInteractionEnabled: NO];
	[h2ndNick setEnabled: NO];
	[h2ndNick setHidden: YES];
	[h2ndPoints setUserInteractionEnabled: NO]; 
	[h2ndPoints setEnabled: NO];
	[h2ndPoints setHidden: YES];

	[hBronzeMedal setUserInteractionEnabled: NO];
	[hBronzeMedal setHidden: YES];
	[h3rdNick setUserInteractionEnabled: NO];
	[h3rdNick setEnabled: NO];
	[h3rdNick setHidden: YES];
	[h3rdNick setUserInteractionEnabled: NO];  
	[h3rdPoints setEnabled: NO];
	[h3rdPoints setHidden: YES];
	
	CGRect auxFrame = [hRankingsTable frame];
	auxFrame.origin.x = ( SCREEN_WIDTH - auxFrame.size.width ) * 0.5f;
	[hRankingsTable setFrame: auxFrame];
	
	auxFrame = [hTableBorder frame];
	auxFrame.origin.x = ( SCREEN_WIDTH - auxFrame.size.width ) * 0.5f;
	[hTableBorder setFrame: auxFrame];
	
	// Centraliza os botões abaixo da tabela
	CGRect btActionFrame = [hBtAction frame];
	CGRect btDeleteFrame = [hBtDelete frame];
	CGRect btToggleChecksFrame = [hBtToggleChecks frame];
	
	btActionFrame.origin.x = ( SCREEN_WIDTH - ( btActionFrame.size.width + btDeleteFrame.size.width + btToggleChecksFrame.size.width + ( 2.0f * NO_RNKVIEW_SPACE_BETWEEN_LOCAL_MODE_BTS )) ) * 0.5f;
	[hBtAction setFrame: btActionFrame];
	
	btDeleteFrame.origin.x = btActionFrame.origin.x + btActionFrame.size.width + NO_RNKVIEW_SPACE_BETWEEN_LOCAL_MODE_BTS;
	[hBtDelete setFrame: btDeleteFrame];
	[self changeBt: hBtDelete Title: [NOControllerView GetText: NO_TXT_DELETE]];
	
	btToggleChecksFrame.origin.x = btDeleteFrame.origin.x + btDeleteFrame.size.width + NO_RNKVIEW_SPACE_BETWEEN_LOCAL_MODE_BTS;
	[hBtToggleChecks setFrame: btToggleChecksFrame];
	
	[self changeBt: hBtToggleChecks Title: TOGGLE_CHECKS_BT_CHECK_TITLE];

	// Armazena quantos caracteres precisamos para a maior pontuação
	int64 aux;
	char buffer[ NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN ];
	snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%lld", pRanking->getMaxPoints( aux ) );
	maxPointsLen = strlen( buffer );
	nDecimalSeparators = maxPointsLen <= 0 ? 0 : ( maxPointsLen - 1 ) / 3;
	
	// Armazena quantos caracteres precisamos para a última colocação
	size_t nEntries = pRanking->getLocalEntries().size();
	if( nEntries )
	{
		snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%d", static_cast< int32 >( nEntries ) );
		nRecordsLen = strlen( buffer );
		
		viewState = NO_RNKVIEW_STATE_LOCAL_IDLE;
	}
	else
	{
		viewState = NO_RNKVIEW_STATE_LOCAL_NO_RECORDS;
		nRecordsLen = 0;
	}
}

/*==============================================================================================

MENSAGEM initGlobalModeData
	Obtém do servidor os dados necessários para preenchermos a interface do modo RANKING_VIEW_MODE_GLOBAL.

==============================================================================================*/

-( void )initGlobalModeData
{
	if( !pRanking )
		return;

	// Obtém os dados do ranking online
	if( NORanking::SendHighScorestRequest( pRanking, &noListener ) )
	{
		viewState = NO_RNKVIEW_STATE_GLOBAL_REQUESTING_DATA;
		[[NOControllerView sharedInstance] showWaitViewWithText: [NOControllerView GetText: NO_TXT_DOWNLOAD_ONLINE_RANKING]];
	}
	else
	{
		viewState = NO_RNKVIEW_STATE_GLOBAL_REQUEST_ERROR;
		[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_COULDNT_SEND_REQUEST_TO_SERVER] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
	}
}

/*==============================================================================================

MENSAGEM initGlobalModeInterface
	Inicializa a interface do modo RANKING_VIEW_MODE_GLOBAL.

==============================================================================================*/

-( void )initGlobalModeInterface
{
	if( pRanking->getOnlineEntries().empty() )
	{
		viewState = NO_RNKVIEW_STATE_GLOBAL_REQUEST_ERROR;
		[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_NO_ONLINE_RANKING] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
		return;
	}
	
	// Armazena quantos caracteres precisamos para a maior pontuação
	int64 score;
	char buffer[ NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN ];
	snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%lld", pRanking->getMaxPoints( score ) );
	maxPointsLen = strlen( buffer );
	nDecimalSeparators = maxPointsLen <= 0 ? 0 : ( maxPointsLen - 1 ) / 3;
	
	// Armazena quantos caracteres precisamos para a última colocação
	snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%d", pRanking->getOnlineEntries()[ pRanking->getOnlineEntries().size() - 1 ].getRankingPos() );
	nRecordsLen = strlen( buffer );
	
	std::vector< NORankingEntry > bestTrio;
	pRanking->getBestTrio( bestTrio );

	NSString *hAux;
	NOString nickname;
	UIFont *hFont = [UIFont fontWithName: @"Courier-Bold" size: 14.0f];
	
	hAux = [NOControllerView ConvertSTDStringToNSString: bestTrio[0].getNickname( nickname )];
	[h1stNick setText: hAux];
	[h1stNick setTextColor: [NORankingView GetNanoColor]];
	[h1stNick setFont: hFont];

	snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%lld", bestTrio[0].getScore( score ) );
	[h1stPoints setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
	[h1stPoints setTextColor: [NORankingView GetNanoColor]];
	[h1stPoints setFont: hFont];

	// No lançamento de um jogo, podemos ainda não ter 3 recordes. Mas. para entrar nesta tela, precisamos
	// de pelo menos uma entrada, por isso checamos apenas se possuímos um 2o e um 3o colocado
	size_t bestTrioSize = bestTrio.size();
	if( bestTrioSize > 1 )
	{
		hAux = [NOControllerView ConvertSTDStringToNSString: bestTrio[1].getNickname( nickname )];
		[h2ndNick setText: hAux];
		[h2ndNick setTextColor: [NORankingView GetNanoColor]];
		[h2ndNick setFont: hFont];

		snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%lld", bestTrio[1].getScore( score ) );
		[h2ndPoints setText: CHAR_ARRAY_TO_NSSTRING( buffer )]; 
		[h2ndPoints setTextColor: [NORankingView GetNanoColor]];
		[h2ndPoints setFont: hFont];
		
		if( bestTrioSize > 2 )
		{
			hAux = [NOControllerView ConvertSTDStringToNSString: bestTrio[2].getNickname( nickname )];
			[h3rdNick setText: hAux];
			[h3rdNick setTextColor: [NORankingView GetNanoColor]];
			[h3rdNick setFont: hFont];

			snprintf( buffer, NO_RANKING_VIEW_MAX_POINTS_BUFFER_LEN, "%lld", bestTrio[2].getScore( score ) );
			[h3rdPoints setText: CHAR_ARRAY_TO_NSSTRING( buffer )]; 
			[h3rdPoints setTextColor: [NORankingView GetNanoColor]];
			[h3rdPoints setFont: hFont];
		}
	}
	
	// TODOO: Por enquanto a função "Show Me", que mostra a melhor colocação
	// do usuário ativo na tabela, não está funcionando
	[hBtAction setUserInteractionEnabled: NO];
	[hBtAction setEnabled: NO];
	[hBtAction setHidden: YES];
	
	// Aumenta a tabela já que não estamos exibindo o botão
	CGRect auxFrame = [hRankingsTable frame];
	auxFrame.size.height = NO_RNKVIEW_GLOBAL_MODE_TABLE_HEIGHT;
	[hRankingsTable setFrame: auxFrame];
	
	auxFrame = [hTableBorder frame];
	auxFrame.size.height = NO_RNKVIEW_GLOBAL_MODE_TABLE_BORDER_HEIGHT;
	[hTableBorder setFrame: auxFrame];
	
	// Desabilita os botões que não iremos utilizar neste modo
	[hBtDelete setHidden: YES];
	[hBtDelete setEnabled: NO];
	
	[hBtToggleChecks setHidden: YES];
	[hBtToggleChecks setEnabled: NO];

	// Atualiza a tabela
	[hRankingsTable reloadData];
		
	viewState = NO_RNKVIEW_STATE_GLOBAL_IDLE;
	[self enableInterface: true];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self cleanNORankingView];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM cleanNORankingView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanNORankingView
{
	[hCkbStateDict release];
	[hImgChecked release];
	[hImgUnChecked release];
}

/*==============================================================================================

MENSAGEM getTxtColorForRow:
	Retorna a cor do texto a ser utilizada em uma determinada linha da tabela.

===============================================================================================*/

-( UIColor* )getTxtColorForCell:( UITableViewCell* )hCell AtRow:( NSIndexPath* )hIndexPath
{
	UIColor *hColorText;

	switch( [hIndexPath row] )
	{
		case 0:
		case 1:
		case 2:
			hColorText = [UIColor whiteColor];
			break;
			
		default:
			{
				UIColor *hCurrBkgColor = [[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_BKG_TAG] backgroundColor];
				UIColor *hWhite = [UIColor whiteColor];
				
				if( CGColorEqualToColor( [hCurrBkgColor CGColor], [hWhite CGColor] ) )
					hColorText = [NORankingView GetNanoColor];
				else
					hColorText = [UIColor whiteColor];
			}
			break;
	}
	
	return hColorText;
}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando o botão de ação deste modo de visualização é pressionado.

===============================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	switch( viewMode )
	{
		case RANKING_VIEW_MODE_LOCAL:
			if( hButton == hBtAction )
			{
				[self submitRecords];
			}
			else if( hButton == hBtDelete )
			{
				// Verifica se tem algum recorde selecionado
				bool hasCheckedRecords = false;
				for( NSIndexPath *hCellPath in hCkbStateDict )
				{
					if( [(( NSNumber* )[hCkbStateDict objectForKey: hCellPath] ) unsignedCharValue] == CHECK_BOX_STATE_CHECKED )
					{
						hasCheckedRecords = true;
						break;
					}
				}
				
				if( hasCheckedRecords )
				{
					viewState = NO_RNKVIEW_STATE_LOCAL_DELETING_ROWS;
					[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_SURE_DELETE_RECORDS] CancelBtIndex: POPUP_NO_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_YES], [NOControllerView GetText: NO_TXT_NO], nil];
				}
				else
				{
					[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_SHOULD_CHECK_TO_DELETE] CancelBtIndex: POPUP_NO_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
				}
			}
			else if( hButton == hBtToggleChecks )
			{
				[self toggleChecks];
			}
			break;
			
		case RANKING_VIEW_MODE_GLOBAL:
		default:
			break;
	}
}

/*==============================================================================================

MENSAGEM submitRecords
	Envia os recordes selecionados para o NanoOnline.

===============================================================================================*/

-( void )submitRecords
{
	// Modifica o ranking temporário para conter apenas os recordes selecionados
	NORanking::NOEntriesContainer& entriesToSubmit = auxRanking.getLocalEntries();
	NORanking::NOEntriesContainer& localEntries = pRanking->getLocalEntries();
	entriesToSubmit.clear();

	for( NSIndexPath *hCellPath in hCkbStateDict )
	{
		if( [(( NSNumber* )[hCkbStateDict objectForKey: hCellPath] ) unsignedCharValue] == CHECK_BOX_STATE_CHECKED )
			entriesToSubmit.push_back( localEntries[ [hCellPath row] ] );
	}
	
	if( entriesToSubmit.empty() )
	{
		[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_SHOULD_CHECK_TO_SUBMIT] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil ];
		return;
	}

	// Envia a requisição
	if( !NORanking::SendSubmitRequest( &auxRanking, &noListener ) )
	{
		NOString error;
		[NOControllerView ConvertNSString: [NOControllerView GetText: NO_TXT_COULDNT_CREATE_REQUEST] toSTDString: error];
		ON_ERROR( error );
		return;
	}
	
	viewState = NO_RNKVIEW_STATE_LOCAL_SUBMITING_ROWS;
	
	// Tudo foi OK, então apaga quaisquer indicações de erros e mostra a mensagem de "Por favor aguarde"
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	[hNOController hideError];
	[hNOController showWaitViewWithText: nil];
}

/*==============================================================================================

MENSAGEM deleteRecords
	Deleta os recordes selecionados pelo usuário.

===============================================================================================*/

-( void )deleteRecords
{
	// Retira da tabela e do ranking as pontuações submetidas
	std::vector< NORanking::NOEntriesContainer::iterator > iterators;
	NORanking::NOEntriesContainer& localEntries = pRanking->getLocalEntries();
	
	NSMutableArray *hRowsToDelete = [NSMutableArray arrayWithCapacity: auxRanking.getLocalEntries().size()];
	
	for( NSIndexPath *hCellPath in hCkbStateDict )
	{
		if( [(( NSNumber* )[hCkbStateDict objectForKey: hCellPath] ) unsignedCharValue] == CHECK_BOX_STATE_CHECKED )
		{
			[hRowsToDelete addObject: hCellPath];
			
			NORanking::NOEntriesContainer::iterator it = localEntries.begin();
			std::advance( it, [hCellPath row] );
			iterators.push_back( it );
		}
	}

	// Não pode mudar a ordem  dessas remoções!!!
	int32 nIterators = iterators.size();
	for( int32 i = 0 ; i < nIterators ; ++i )
		localEntries.erase( iterators[i] );
	
	#if DEBUG
		pRanking->print();
	#endif
	
	[NOControllerView saveNotSubmittedRecords];

	[hCkbStateDict removeObjectsForKeys: hRowsToDelete];

	[hRankingsTable beginUpdates];
	[hRankingsTable deleteRowsAtIndexPaths: hRowsToDelete withRowAnimation: UITableViewRowAnimationFade];
	[hRankingsTable endUpdates];
	[hRankingsTable reloadData];
}

/*==============================================================================================

MENSAGEM deleteSubmittedRecords
	Deleta os recordes já submetidos para o NanoOnline.

===============================================================================================*/

-( void )deleteSubmittedRecords
{
	// Atualiza o ranking online do ranking que não é temporário
	pRanking->swapOnlineEntries( auxRanking );

	[self deleteRecords];

	// Esvazia o ranking auxiliar
	auxRanking.clear();
}

/*==============================================================================================

MENSAGEM changeBt:Title:
	Modifica o título do botão.

===============================================================================================*/

-( void )changeBt:( UIButton* )hBt Title:( NSString* )hNewTitle
{
	[hBt setTitle: hNewTitle forState: UIControlStateNormal];
	[hBt setTitle: hNewTitle forState: UIControlStateHighlighted];
	[hBt setTitle: hNewTitle forState: UIControlStateDisabled];
	[hBt setTitle: hNewTitle forState: UIControlStateSelected];
	
	// Só para versões de OS acima de 3.0
	std::string currOsVersion;
	DeviceInterface::GetDeviceSystemVersion( currOsVersion );
	if( Utils::CompareVersions( currOsVersion, "3.0" ) >= 0 )
		[[hBt titleLabel] setAdjustsFontSizeToFitWidth: YES];
}

/*==============================================================================================

MENSAGEM toggleChecks
	Desmarca todas as checkboxes, caso todas estejam marcadas. Caso contrário, marca todas as
checkboxes que ainda não o foram.

===============================================================================================*/

-( void )toggleChecks
{
	int8 nextCheckboxesState;

	if( [[hBtToggleChecks titleForState: UIControlStateNormal] compare: TOGGLE_CHECKS_BT_CHECK_TITLE] == NSOrderedSame )
	{
		nextCheckboxesState = CHECK_BOX_STATE_CHECKED;
		[self changeBt: hBtToggleChecks Title: TOGGLE_CHECKS_BT_UNCHECK_TITLE];
	}
	else
	{
		nextCheckboxesState = CHECK_BOX_STATE_UNCHECKED;
		[self changeBt: hBtToggleChecks Title: TOGGLE_CHECKS_BT_CHECK_TITLE];
	}


	NSIndexPath *hIndexPath;
	size_t nRows = pRanking->getLocalEntries().size();
	NSNumber *hValue = [NSNumber numberWithUnsignedChar: nextCheckboxesState];
	UIImage *hNextStateImg = nextCheckboxesState == CHECK_BOX_STATE_UNCHECKED ? hImgUnChecked : hImgChecked;
	
	for( size_t i = 0 ; i < nRows ; ++i )
	{	
		hIndexPath = [NSIndexPath indexPathForRow: i inSection: 0];
		[(( UIButton* )[[hRankingsTable cellForRowAtIndexPath: hIndexPath] viewWithTag: NO_RNKVIEW_TABLE_CELL_CHECKBOX_TAG]) setImage: hNextStateImg forState: UIControlStateNormal];
			
		[hCkbStateDict setObject: hValue forKey: hIndexPath];
	}
}

/*==============================================================================================

MENSAGEM onNOSuccessfulResponse
	Indica que uma requisição foi respondida e terminada com sucesso.

===============================================================================================*/

-( void )onNOSuccessfulResponse
{
	[[NOControllerView sharedInstance] hideWaitView];

	switch( viewMode )
	{
		case RANKING_VIEW_MODE_LOCAL:
			// Não deixa o usuário submeter os mesmo recordes novamente
			[self deleteSubmittedRecords];

			// Mostra o popup de feedback
			[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_RECORDS_SUBMITTED] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
			break;
			
		case RANKING_VIEW_MODE_GLOBAL:
			switch( viewState )
			{
				case NO_RNKVIEW_STATE_GLOBAL_REQUESTING_DATA:
					[self initGlobalModeInterface];
					break;
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM onNOError:WithErrorDesc:
	Sinaliza erros ocorridos nas operações do NanoOnline.

===============================================================================================*/

-( void )onNOError:( NOErrors )errorCode WithErrorDesc:( NSString* )hErrorDesc
{
	[[NOControllerView sharedInstance] hideWaitView];

	NOString aux;
	[NOControllerView ConvertNSString: hErrorDesc toSTDString: aux];
	
	if( ( viewMode == RANKING_VIEW_MODE_GLOBAL ) && ( viewState == NO_RNKVIEW_STATE_GLOBAL_REQUESTING_DATA ) )
	{
		viewState = NO_RNKVIEW_STATE_GLOBAL_REQUEST_ERROR;
		[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_DOWNLOAD_ERROR] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
	}
	else
	{
		ON_ERROR( aux );
	}
}

/*==============================================================================================

MENSAGEM onNORequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

===============================================================================================*/

-( void )onNORequestCancelled
{
	[[NOControllerView sharedInstance] hideWaitView];
	
	if( ( viewMode == RANKING_VIEW_MODE_GLOBAL ) && ( viewState == NO_RNKVIEW_STATE_GLOBAL_REQUESTING_DATA ) )
	{
		// Sai da tela
		viewState = NO_RNKVIEW_STATE_GLOBAL_REQUEST_ERROR;
		[self alertView: nil clickedButtonAtIndex: POPUP_OK_BT_INDEX];
	}
}

/*==============================================================================================

MENSAGEM onNOProgressChangedTo:ofTotal:
	Indica o progresso da requisição atual.

===============================================================================================*/

-( void )onNOProgressChangedTo:( int32 )currBytes ofTotal:( int32 )totalBytes
{
	[[NOControllerView sharedInstance] setProgress: static_cast< float >( currBytes ) / totalBytes];
}

/*==============================================================================================

MENSAGEM enableInterface:
	Desabilita os elementos com os quais o usuário pode interagir.
 
===============================================================================================*/

-( void )enableInterface:( bool )b
{
	BOOL enabled = b ? YES : NO;
	
	[self setHidden: !enabled];
	[self setUserInteractionEnabled: enabled];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];
	
	if( viewState == NO_RNKVIEW_STATE_UNDEFINED )
	{
		[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_UNEXPECTED_ERROR] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
		return;
	}
	
	switch( viewMode )
	{
		case RANKING_VIEW_MODE_LOCAL:
			switch( viewState )
			{	
				case NO_RNKVIEW_STATE_LOCAL_NO_RECORDS:
					[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_NO_RECORDS_AVAILABLE] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
					break;
					
				case NO_RNKVIEW_STATE_LOCAL_IDLE:
					[self enableInterface: true];
					break;
			}
			break;

		case RANKING_VIEW_MODE_GLOBAL:
			[self initGlobalModeData];
			break;
			
#if DEBUG
		default:
			assert_n_log( false, "No valid mode set for NORankingView until method onBecomeCurrentScreen" );
			break;
#endif
	}
}

/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário pressiona um dos botões do popup.

================================================================================================*/

-( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	switch( viewState )
	{
		case NO_RNKVIEW_STATE_LOCAL_DELETING_ROWS:
			if( buttonIndex == POPUP_YES_BT_INDEX )
				[self deleteRecords];

			// Sem break mesmo

		case NO_RNKVIEW_STATE_LOCAL_SUBMITING_ROWS:
			if( pRanking->getLocalEntries().empty() )
			{
				viewState = NO_RNKVIEW_STATE_LOCAL_NO_RECORDS;
				[self enableInterface: false];
				[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_NO_MORE_RECORDS_TO_SUBMIT] CancelBtIndex: POPUP_OK_BT_INDEX AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
			}
			else
			{
				viewState = NO_RNKVIEW_STATE_LOCAL_IDLE;
			}
			break;

		case NO_RNKVIEW_STATE_UNDEFINED:
		case NO_RNKVIEW_STATE_LOCAL_NO_RECORDS:
		case NO_RNKVIEW_STATE_GLOBAL_REQUEST_ERROR:
			{
				viewState = NO_RNKVIEW_STATE_UNDEFINED;
			
				NOControllerView *hNOController = [NOControllerView sharedInstance];
				[hNOController setHistoryAsShortestWayToView: NO_VIEW_INDEX_RANKING_ACTION];
				[hNOController performTransitionToView: NO_VIEW_INDEX_RANKING_ACTION];
			}
			break;

		default:
			break;
	}
}

/*==============================================================================================

MENSAGEM toggleCheckBox:
	Trata a interação do usuário com as checkboxes das células.

===============================================================================================*/

-( void )toggleCheckBox:( UIControl* )hSender 
{
	UITableViewCell *hCell = ( UITableViewCell* )[hSender superview];
	
	UIButton *hCheckBox = ( UIButton* )[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_CHECKBOX_TAG];
	
	NSIndexPath *hCellPath = [hRankingsTable indexPathForCell: hCell];
	NSNumber *hValue = [hCkbStateDict objectForKey: hCellPath];

	if( !hValue )
		hValue = [NSNumber numberWithUnsignedChar: CHECK_BOX_STATE_UNCHECKED];

	switch( [hValue unsignedCharValue] )
	{
		case CHECK_BOX_STATE_UNCHECKED:
			[hCheckBox setImage: hImgChecked forState: UIControlStateNormal];
			hValue = [NSNumber numberWithUnsignedChar: CHECK_BOX_STATE_CHECKED];
			break;
		
		case CHECK_BOX_STATE_CHECKED:
			[hCheckBox setImage: hImgUnChecked forState: UIControlStateNormal];
			hValue = [NSNumber numberWithUnsignedChar: CHECK_BOX_STATE_UNCHECKED];
			break;
		
		case CHECK_BOX_STATE_HIDDEN:
			break;
	}
	[hCkbStateDict setObject: hValue forKey: hCellPath];
	
	// Verifica se o título do botão hBtToggleChecks ainda está correto
	for( NSIndexPath *hCellPath in hCkbStateDict )
	{
		if( [(( NSNumber* )[hCkbStateDict objectForKey: hCellPath] ) unsignedCharValue] == CHECK_BOX_STATE_UNCHECKED )
		{
			[self changeBt: hBtToggleChecks Title: TOGGLE_CHECKS_BT_CHECK_TITLE];
			return;
		}
	}
	[self changeBt: hBtToggleChecks Title: TOGGLE_CHECKS_BT_UNCHECK_TITLE];
}

/*==============================================================================================

PROTOCOLO UITableViewDelegate

===============================================================================================*/

-( CGFloat )tableView:( UITableView* )hTableView heightForRowAtIndexPath:( NSIndexPath* )indexPath
{
	return NO_RNKVIEW_TABLE_DEFAULT_ROW_HEIGHT;
}

-( void )tableView:( UITableView* )hTableView didSelectRowAtIndexPath:( NSIndexPath* )hIndexPath
{
	UITableViewCell* hCell = [hTableView cellForRowAtIndexPath: hIndexPath];
	if( [hCell selectionStyle] != UITableViewCellSelectionStyleNone )
	{
		UILabel *hLabel = ( UILabel* )[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_LABEL_TAG];
		[hLabel setTextColor: [self getTxtColorForCell: hCell AtRow: hIndexPath]];
	}
}

-( void )tableView:( UITableView* )hTableView didDeselectRowAtIndexPath:( NSIndexPath* )hIndexPath
{
	[self tableView: hTableView didSelectRowAtIndexPath: hIndexPath];
}

/*==============================================================================================

PROTOCOLO UITableViewDataSource

===============================================================================================*/

-( UITableViewCell* )tableView:( UITableView* )hTableView cellForRowAtIndexPath:( NSIndexPath* )hIndexPath
{
	if( !pRanking )
		return nil;

	UITableViewCell *hCell = [hTableView dequeueReusableCellWithIdentifier: @"simpleRanking"];
	if( hCell == nil )
	{
		hCell = [self createTableCell];
		if( !hCell )
			return nil;
	}

	UIFont *hFont;
	UIColor *hColorBkg;
	
	int32 index = [hIndexPath row];
	switch( index )
	{
		case 0:
			hFont = [UIFont fontWithName: @"Courier-BoldOblique" size: 14.0f];
			hColorBkg = [UIColor colorWithRed: 246.0f / 255.0f green: 219.0f / 255.0f blue: 35.0f / 255.0f alpha: 1.0f];
			break;
			
		case 1:
			hFont = [UIFont fontWithName: @"Courier-BoldOblique" size: 14.0f];
			hColorBkg = [UIColor colorWithRed: 226.0f / 255.0f green: 227.0f / 255.0f blue: 228.0f / 255.0f alpha: 1.0f];
			break;

		case 2:
			hFont = [UIFont fontWithName: @"Courier-BoldOblique" size: 14.0f];
			hColorBkg = [UIColor colorWithRed: 225.0f / 255.0f green: 144.0f / 255.0f blue: 46.0f / 255.0f alpha: 1.0f];
			break;
			
		default:
			hFont = [UIFont fontWithName: @"Courier-Bold" size: 14.0f];
			
			// Se for um recorde deste perfil ativo, modifica a cor
			if( viewMode == RANKING_VIEW_MODE_GLOBAL )
			{
				NORankingEntry temp = pRanking->getOnlineEntries()[ index ];
				
				std::vector< NOProfileId >& profilesToFocus = pRanking->getHighlightedProfilesIds();
				std::vector< NOProfileId >::iterator found = std::find( profilesToFocus.begin(), profilesToFocus.end(), temp.getProfileId() );

				if( found == profilesToFocus.end() )
					hColorBkg = [UIColor whiteColor];
				else
					hColorBkg = [NORankingView GetNanoColor];// [UIColor colorWithRed: 6.0f / 255.0f green: 80.0f / 255.0f blue: 6.0f / 255.0f alpha: 1.0f];
			}
			else
			{
				hColorBkg = [UIColor whiteColor];
			}
			break;
	}
	
	if( ( hFont == nil ) || ( hColorBkg == nil ) )
		return nil;
	
	[[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_BKG_TAG] setBackgroundColor: hColorBkg];

	// OLD : O label que já vem em uma UITableViewCell não é completamente configurável.
	// Por isso criamos e configuramos um label extra
	//UILabel *hLabel = [hCell textLabel];
	
	UILabel *hLabel = ( UILabel* )[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_LABEL_TAG];
	[hLabel setFont: hFont];
	[hLabel setMinimumFontSize: 10.0f];
	[hLabel setAdjustsFontSizeToFitWidth: YES];
	
	if( viewMode == RANKING_VIEW_MODE_GLOBAL )
	{
		NORankingEntry temp = pRanking->getOnlineEntries()[ index ];
		[hLabel setText: [self getRankingStrForEntry: temp AtIndex: temp.getRankingPos()]];
	}
	else
	{
		[hLabel setText: [self getRankingStrForEntry: pRanking->getLocalEntries()[ index ] AtIndex: index]];		
	
		NSNumber *hValue = [hCkbStateDict objectForKey: hIndexPath];
		
		// O default é visível desmarcado
		if( !hValue )
			hValue = [NSNumber numberWithUnsignedChar: CHECK_BOX_STATE_UNCHECKED];
		
		UIButton *hCheckBox = ( UIButton* )[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_CHECKBOX_TAG];
		switch( [hValue unsignedCharValue] )
		{			
			case CHECK_BOX_STATE_CHECKED:
				[hCheckBox setHidden: NO];
				[hCheckBox setEnabled: YES];
				[hCheckBox setImage: hImgChecked forState: UIControlStateNormal];
				break;
			
			case CHECK_BOX_STATE_HIDDEN:
				[hCheckBox setHidden: YES];
				[hCheckBox setEnabled: NO];
				break;
				
			case CHECK_BOX_STATE_UNCHECKED:
			default:
				[hCheckBox setHidden: NO];
				[hCheckBox setEnabled: YES];
				[hCheckBox setImage: hImgUnChecked forState: UIControlStateNormal];
				break;
		}
	}

	[hLabel setTextColor: [self getTxtColorForCell: hCell AtRow: hIndexPath]];
	
	return hCell; 
}

-( NSInteger )tableView:( UITableView* )tableView numberOfRowsInSection:( NSInteger )section
{
	if( !pRanking )
		return 0;

	if( viewMode == RANKING_VIEW_MODE_GLOBAL )
		return pRanking->getOnlineEntries().size();
	else
		return pRanking->getLocalEntries().size();
	
	// OLD
	//return tableSections.find( section ) == tableSections.end() ? 0 : tableSections[ section ].nRows;
}

-( NSInteger )numberOfSectionsInTableView:( UITableView* )tableView
{
	return 1;
	
	// OLD
	//return tableSections.size();
}

// OLD
//-( NSString* )tableView:( UITableView* )tableView titleForFooterInSection:( NSInteger )section
//{
//	std::string aux = tableSections.find( section ) == tableSections.end() ? "" : tableSections[ section ].footerStr;
//	return STD_STRING_TO_NSSTRING( aux );
//}
//
//-( NSString* )tableView:( UITableView* )tableView titleForHeaderInSection:( NSInteger )section
//{
//	std::string aux = tableSections.find( section ) == tableSections.end() ? "" : tableSections[ section ].headerStr;
//	return STD_STRING_TO_NSSTRING( aux );
//}

/*==============================================================================================

MENSAGEM createTableCell
	Cria uma célula da tabela do NanoOnline.

===============================================================================================*/

-( UITableViewCell* )createTableCell
{
	UITableViewCell *hCell = [[[UITableViewCell alloc] initWithFrame: CGRectZero reuseIdentifier: @"simpleRanking"] autorelease];
	if( !hCell )
		return nil;

	[hCell setSelectionStyle:  UITableViewCellSelectionStyleNone];
	
	[[[hCell subviews] objectAtIndex: 0] setTag: NO_RNKVIEW_TABLE_CELL_BKG_TAG];
	[[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_BKG_TAG] setBackgroundColor: [UIColor whiteColor]];

	// O label que já vem em uma UITableViewCell não é completamente configurável.
	// Por isso criamos e configuramos um label extra
	UILabel *hExtraLabelView = [[UILabel alloc] initWithFrame: CGRectMake( NO_RNKVIEW_TABLE_CELL_EXTRA_LABEL_OFFSET, 0.0f, hRankingsTable.frame.size.width - NO_RNKVIEW_TABLE_CELL_EXTRA_LABEL_OFFSET, NO_RNKVIEW_TABLE_DEFAULT_ROW_HEIGHT )]; 
	if( !hExtraLabelView )
		return nil;
	
	[hCell addSubview: hExtraLabelView]; 
	[hExtraLabelView release]; 

	[hExtraLabelView setTag: NO_RNKVIEW_TABLE_CELL_LABEL_TAG];
	[hExtraLabelView setBackgroundColor: [UIColor clearColor]];
	
	// Utiliza o label original para configurar algumas propriedades do label auxiliar
	std::string osVersion;
	DeviceInterface::GetDeviceSystemVersion( osVersion );

	if( Utils::CompareVersions( osVersion, "3.0" ) >= 0 )
	{
		UILabel *hTemplateLabel = [hCell textLabel];
		[hExtraLabelView setShadowColor: [hTemplateLabel shadowColor]];
		[hExtraLabelView setBaselineAdjustment: [hTemplateLabel baselineAdjustment]];
		[hExtraLabelView setTextAlignment: [hTemplateLabel textAlignment]];
		[hExtraLabelView setLineBreakMode: [hTemplateLabel lineBreakMode]];
	}
	else
	{
		[hExtraLabelView setBaselineAdjustment: UIBaselineAdjustmentAlignBaselines];
		[hExtraLabelView setTextAlignment: UITextAlignmentLeft];
		[hExtraLabelView setLineBreakMode: UILineBreakModeTailTruncation];
	}
	
	// Cria e configura a checkbox
	return [self createCheckBoxInCell: hCell] ? hCell : nil;
}

/*==============================================================================================

MENSAGEM createCheckBoxInCell:
	Cria uma checkbox compatível com célula da tabela do NanoOnline.

===============================================================================================*/

-( bool )createCheckBoxInCell:( UITableViewCell* )hCell
{
	switch( viewMode )
	{
		case RANKING_VIEW_MODE_LOCAL:
			{
				UIButton *hCheckBox = [UIButton buttonWithType: UIButtonTypeCustom];
				if( hCheckBox == nil )
					return false;

				[hCheckBox setTag: NO_RNKVIEW_TABLE_CELL_CHECKBOX_TAG];
				[hCheckBox addTarget: self action: @selector( toggleCheckBox: ) forControlEvents: UIControlEventTouchUpInside];

				[hCheckBox setImage: hImgUnChecked forState: UIControlStateNormal];
				
				CGSize ckbSize = [hImgUnChecked size];
				[hCheckBox setFrame: CGRectMake( hRankingsTable.frame.size.width - NO_RNKVIEW_TABLE_CELL_EXTRA_LABEL_OFFSET - ckbSize.width, ( NO_RNKVIEW_TABLE_DEFAULT_ROW_HEIGHT - ckbSize.height ) * 0.5f, ckbSize.width, ckbSize.height )];
				
				// Agora que temos a checkbox, reconfigura o frame do label
				UILabel *hExtraLabelView = ( UILabel* )[hCell viewWithTag: NO_RNKVIEW_TABLE_CELL_LABEL_TAG];
				CGRect auxFrame = [hExtraLabelView frame];
				auxFrame.size.width -= NO_RNKVIEW_TABLE_CELL_SPACE_BETWEEN_LABEL_N_CKB + ckbSize.width;
				[hExtraLabelView setFrame: auxFrame];
				
				[hCell addSubview: hCheckBox];
				[hCell setAccessoryView: nil];
			}
			break;

		case RANKING_VIEW_MODE_GLOBAL:
		default:
			break;
	}

	return true;
}

/*==============================================================================================

MENSAGEM GetNanoColor
	Retorna a cor de texto padrão utilizada nesta tela.

===============================================================================================*/

+( UIColor* )GetNanoColor
{
	return [UIColor colorWithRed: 50.0f/255.0f green: 152.0f/255.0f blue: 151.0f/255.0f alpha: 1.0f];
}

// Fim da implementação da classe
@end
