/*
 *  NOBaseView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/24/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_BASE_VIEW_H
#define NANO_ONLINE_BASE_VIEW_H

// Componentes
#include "UpdatableView.h"
#include "NOControllerView.h"

@interface NOBaseView : UpdatableView
{
	@protected
		// Título desta tela
		NSString *hScreenTitle;
	
	// OLD: Agora NOControllerView é um singleton
//	@private
		// Controlador desta view
		// DEVE SER o delegate de todos os campos de texto contidos nesta view 
		//NOControllerView *hNOController;
}

// OLD: Agora NOControllerView é um singleton
//@property ( readwrite, nonatomic, assign, setter = setNOController, getter = getNOController ) NOControllerView *hNOController;

@property ( readwrite, nonatomic, assign, setter = setScreenTitle, getter = getScreenTitle ) NSString *hScreenTitle;

// Exibe um popup para o usuário
// Se desejar interceptar a resposta do popup, basta implementar o método:
// -( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
-( void )showPopUpWithTitle:( NSString* )hTitle Msg:( NSString* )hMsg CancelBtIndex:( int8 )cancelIndex AndBts:( NSString* )firstBtTitle, ...;

@end

#endif
