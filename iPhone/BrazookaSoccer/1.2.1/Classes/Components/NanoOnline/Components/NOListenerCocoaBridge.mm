#include "NOListenerCocoaBridge.h"

// Components
#include "ObjcMacros.h"
#include "Utils.h"

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

NOListenerCocoaBridge::NOListenerCocoaBridge( id< NOCocoaListener > hCocoaNOListener ) : hCocoaNOListener( hCocoaNOListener )
{
}

/*==============================================================================================

MÉTODO setCocoaListener
	Faz a ligação entre o listener C++ e o listener Cocoa.

===============================================================================================*/

void NOListenerCocoaBridge::setCocoaListener( id< NOCocoaListener > hListener )
{
	hCocoaNOListener = hListener;
}

/*==============================================================================================

MÉTODO onNORequestSent
	Indica que uma requisição foi enviada para o NanoOnline.

===============================================================================================*/

void NOListenerCocoaBridge::onNORequestSent( void )
{
	if( hCocoaNOListener != nil )
	{
		if( [hCocoaNOListener respondsToSelector: @selector( onNORequestSent )] )
			[hCocoaNOListener onNORequestSent];
	}
}

/*==============================================================================================

MÉTODO onNORequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

===============================================================================================*/

void NOListenerCocoaBridge::onNORequestCancelled( void )
{
	if( hCocoaNOListener != nil )
	{
		if( [hCocoaNOListener respondsToSelector: @selector( onNORequestCancelled )] )
			[hCocoaNOListener onNORequestCancelled];
	}
}

/*==============================================================================================

MÉTODO onNOSuccessfulResponse
	Indica que uma requisição foi respondida e terminada com sucesso.

===============================================================================================*/

void NOListenerCocoaBridge::onNOSuccessfulResponse( void )
{
	if( hCocoaNOListener != nil )
	{
		if( [hCocoaNOListener respondsToSelector: @selector( onNOSuccessfulResponse )] )
			[hCocoaNOListener onNOSuccessfulResponse];
	}
}

/*==============================================================================================

MÉTODO onNOError
	Sinaliza erros ocorridos nas operações do NanoOnline.

===============================================================================================*/

void NOListenerCocoaBridge::onNOError( NOErrors errorCode, const NOString& errorStr )
{
	if( hCocoaNOListener != nil )
	{
		std::string aux;
		Utils::WStringToString( errorStr, aux );
		[hCocoaNOListener onNOError: errorCode WithErrorDesc: STD_STRING_TO_NSSTRING( aux )];
	}
}

/*==============================================================================================

MÉTODO onNOProgressChanged
	Indica o progresso da requisição atual.

===============================================================================================*/

void NOListenerCocoaBridge::onNOProgressChanged( int32 currBytes, int32 totalBytes )
{
	if( hCocoaNOListener != nil )
	{
		if( [hCocoaNOListener respondsToSelector: @selector( onNOProgressChangedTo:ofTotal: )] )
			[hCocoaNOListener onNOProgressChangedTo: currBytes ofTotal: totalBytes];
	}
}
