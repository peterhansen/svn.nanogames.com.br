/*
 *  NOeMap.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_MAP_H
#define NANO_ONLINE_MAP_H 1

// Components
#include "NanoTypes.h"

// C++
#include <map>
#include <boost/shared_ptr.hpp>

typedef std::map< int8, boost::shared_ptr< void > > NOMap;

#endif
