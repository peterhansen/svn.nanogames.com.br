#include "NetworkManager.h"

// Components
#include "INetworkListener.h"
#include "Macros.h"
#include "NetworkManagerErrors.h"
#include "NetworkTextsIndexes.h"

// Foundation
#include <CFNetwork/CFNetworkErrors.h>

// C++
#include <string>

// Macros de desalocação de objetos CF*
#define CFKILL( p ) { CFRelease( p ); p = NULL; }
#define SAFE_CFKILL( p ) { if( p != NULL )CFKILL( p ); }

// Tamanho do buffer de leitura que irá armazenar as respostas das requisições HTTP
#define NETWORK_MANAGER_READ_BUFFER_LEN 1024

// Códigos de erro HTTP
// http://en.wikipedia.org/wiki/List_of_HTTP_status_codes

#define HTTP_STATUS_OK 200

// Indica se o código de erro indica redirecionamento
#define IS_REDIRECTION_HTTP_STATUS_CODE( errorCode ) (( errorCode >= 300 ) && ( errorCode <= 307 ))

/*==============================================================================================

FUNÇÃO PrintError

==============================================================================================*/

#if DEBUG

void PrintError( CFErrorRef pError )
{
	char buffer[ NETWORK_MANAGER_READ_BUFFER_LEN ];

	CFStringRef pErrorDesc = CFErrorCopyDescription( pError );
	if( pErrorDesc )
	{
		if( CFStringGetCString( pErrorDesc, buffer, NETWORK_MANAGER_READ_BUFFER_LEN, kCFStringEncodingUTF8 ) == TRUE )
			LOG( ">>> NetworkManager::PrintError - Error %d: %s\n", CFErrorGetCode( pError ), buffer );

		CFKILL( pErrorDesc );
	}
	
	CFStringRef pErrorDomain = CFErrorGetDomain( pError );
	if( pErrorDomain )
	{
		if( CFStringGetCString( pErrorDomain, buffer, NETWORK_MANAGER_READ_BUFFER_LEN, kCFStringEncodingUTF8 ) == TRUE )
			LOG( ">>> NetworkManager::PrintError - Error Domain %d: %s\n", CFErrorGetCode( pError ), buffer );

		CFKILL( pErrorDomain );
	}

	CFStringRef pFailureReason = CFErrorCopyFailureReason( pError );
	if( pFailureReason )
	{
		if( CFStringGetCString( pFailureReason, buffer, NETWORK_MANAGER_READ_BUFFER_LEN, kCFStringEncodingUTF8 ) == TRUE )
			LOG( ">>> NetworkManager::PrintError - Failure Reason: %s\n", buffer );

		CFKILL( pFailureReason );
	}

	CFStringRef pRecoverySug = CFErrorCopyRecoverySuggestion( pError );
	if( pRecoverySug )
	{
		if( CFStringGetCString( pRecoverySug, buffer, NETWORK_MANAGER_READ_BUFFER_LEN, kCFStringEncodingUTF8 ) == TRUE )
			LOG( ">>> NetworkManager::PrintError - Recovery Suggestion: %s\n", buffer );

		CFKILL( pRecoverySug );
	}
}

#endif

/*==============================================================================================

FUNÇÃO GetText

==============================================================================================*/

#define GET_TXT_BUFFER_SIZE 512

void GetText( uint16 textIndex, std::string& outText )
{
	char buffer[ GET_TXT_BUFFER_SIZE ];
	snprintf( buffer, GET_TXT_BUFFER_SIZE, "%d", static_cast< int32 >( textIndex ) );
	
	CFStringRef hTemp = CFStringCreateWithCString( NULL, buffer, kCFStringEncodingUTF8 );

	CFStringRef hLocalizedStr = CFCopyLocalizedStringFromTable( hTemp, CFSTR( "net" ), NULL );
	if( hLocalizedStr )
	{
		CFStringGetCString( hLocalizedStr, buffer, GET_TXT_BUFFER_SIZE, kCFStringEncodingUTF8 );
		outText.append( buffer );

		CFRelease( hLocalizedStr );
	}
	
	CFRelease( hTemp );
}

#undef GET_TXT_BUFFER_SIZE

/*==============================================================================================

FUNÇÃO GetMessageForErrorCode
	Fornece a descrição por escrito de um erro.

==============================================================================================*/

std::string* GetMessageForErrorCode( NetworkManagerErrors errorCode, std::string* pOutErrorStr )
{
	switch( errorCode )
	{
		case NETWORK_ERROR_HOST_NOT_FOUND:
			GetText( NET_TXT_HOST_NOT_FOUND, *pOutErrorStr );
			break;

		case NETWORK_ERROR_UNKNOWN:
			GetText( NET_TXT_UNKNOWN_HOST_ERROR, *pOutErrorStr );
			break;

		case NETWORK_ERROR_UNKNOWN_CLIENT_VERSION:
			GetText( NET_TXT_SOCKS_ERR_UNKNOWN_CLIENT_VERSION, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_UNSUPPORTED_SERVER_VERSION:
			GetText( NET_TXT_SOCKS_ERR_UNSUPPORTED_SERVER_VERSION, *pOutErrorStr );
			break;

		case NETWORK_ERROR_REQUEST_FAILED:
			GetText( NET_TXT_SOCKS4_ERR_REQUEST_FAILED, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_IDENTD_FAILED:
			GetText( NET_TXT_SOCKS4_ERR_IDENTD_FAILED, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_ID_CONFLICT:
			GetText( NET_TXT_SOCKS4_ERR_ID_CONFLICT, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_UNKNOWN_STATUS_CODE:
			GetText( NET_TXT_SOCKS4_ERR_UNKNOWN_STATUS_CODE, *pOutErrorStr );
			break;

		case NETWORK_ERROR_BAD_STATE:
			GetText( NET_TXT_SOCKS5_ERR_BAD_STATE, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_BAD_RESPONSE_ADDR:
			GetText( NET_TXT_SOCKS5_ERR_BAD_RESPONSE_ADDRESS, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_BAD_CREDENTIALS:
			GetText( NET_TXT_SOCKS5_ERR_BAD_CREDENTIALS, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_UNSUPPORTED_NEGOTIATION_METHOD:
			GetText( NET_TXT_SOCKS5_ERR_UNSUPPORTED_NEG_METHOD, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_NO_ACCEPTABLE_METHOD:
			GetText( NET_TXT_SOCKS5_ERR_NO_ACCEPTABLE_METHOD, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_FTP_UNEXPECTED_STATUS_CODE:
			GetText( NET_TXT_FTP_ERR_NO_ACCEPTABLE_METHOD, *pOutErrorStr );
			break;

		case NETWORK_ERROR_HTTP_AUTHENTICATION_TYPE_UNSUPPORTED:
			GetText( NET_TXT_HTTP_ERR_AUTHENTICATION_UNSUPPORTED, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_BAD_CREDENTIALS:
			GetText( NET_TXT_HTTP_ERR_BAD_CREDENTIALS, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_CONNECTION_LOST:
			GetText( NET_TXT_HTTP_ERR_CONNECTION_LOST, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_PARSE_FAILURE:
			GetText( NET_TXT_HTTP_ERR_PARSE_FAILURE, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_REDIRECTION_LOOP:
			GetText( NET_TXT_HTTP_ERR_REDIRECTION_LOOP, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_BAD_URL:
			GetText( NET_TXT_HTTP_ERR_BAD_URL, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_PROXY_CONNECTION_FAILURE:
			GetText( NET_TXT_HTTP_ERR_PROXY_CONN_FAILURE, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_HTTP_BAD_PROXY_CREDENTIALS:
			GetText( NET_TXT_HTTP_ERR_PROXY_CREDENTIALS, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_PAC_FILE_ERROR:
			GetText( NET_TXT_HTTP_ERR_PAC_FILE_ERROR, *pOutErrorStr );
			break;

		case NETWORK_ERROR_COULDNT_REDIRECT:
			GetText( NET_TXT_NETWORK_ERR_COULDNT_REDIRECT, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_COLLISION:
			GetText( NET_TXT_NETWORK_ERR_COLLISION, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_NOT_FOUND:
			GetText( NET_TXT_NETWORK_ERR_NOT_FOUND, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_IN_PROGRESS:
			GetText( NET_TXT_NETWORK_ERR_IN_PROGRESS, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_BAD_ARGUMENT:
			GetText( NET_TXT_NETWORK_ERR_BAD_ARGUMENT, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_CANCEL:
			GetText( NET_TXT_NETWORK_ERR_CANCEL, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_INVALID:
			GetText( NET_TXT_NETWORK_ERR_INVALID, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_TIMEOUT:
			GetText( NET_TXT_NETWORK_ERR_TIMEOUT, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_DNS_FAILURE:
			GetText( NET_TXT_NETWORK_ERR_DNS_FAILURE, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_TOO_MANY_REDIRECTIONS:
			GetText( NET_TXT_NETWORK_ERR_MANY_REDIRECTIONS, *pOutErrorStr );
			break;
			
		case NETWORK_ERROR_INTERNAL:
		default:
			GetText( NET_TXT_NETWORK_ERR_INTERNAL, *pOutErrorStr );
			break;
	}
	
	return pOutErrorStr;
}

/*==============================================================================================

FUNÇÃO TranslateErrorCode
	Transforma os códigos de erro do sistema em códigos de erro dos componentes.

==============================================================================================*/

// Mantendo a compatibilidade do código com versões de iPhoneOS anteriores à 3.0
#if ! defined( __IPHONE_3_0 )
	#define kCFErrorPACFileError 308
	#define kCFErrorPACFileAuth 309
#endif

NetworkManagerErrors TranslateErrorCode( CFNetworkErrors errorCode, std::string* pOutErrorStr )
{
	NetworkManagerErrors translatedError;

	switch( errorCode )
	{
		case kCFHostErrorHostNotFound:
			translatedError = NETWORK_ERROR_HOST_NOT_FOUND;
			break;

		case kCFHostErrorUnknown:
			translatedError = NETWORK_ERROR_UNKNOWN;
			break;

		case kCFSOCKSErrorUnknownClientVersion:
			translatedError = NETWORK_ERROR_UNKNOWN_CLIENT_VERSION;
			break;
			
		case kCFSOCKSErrorUnsupportedServerVersion:
			translatedError = NETWORK_ERROR_UNSUPPORTED_SERVER_VERSION;
			break;

		case kCFSOCKS4ErrorRequestFailed:
			translatedError = NETWORK_ERROR_REQUEST_FAILED;
			break;
			
		case kCFSOCKS4ErrorIdentdFailed:
			translatedError = NETWORK_ERROR_IDENTD_FAILED;
			break;
			
		case kCFSOCKS4ErrorIdConflict:
			translatedError = NETWORK_ERROR_ID_CONFLICT;
			break;
			
		case kCFSOCKS4ErrorUnknownStatusCode:
			translatedError = NETWORK_ERROR_UNKNOWN_STATUS_CODE;
			break;

		case kCFSOCKS5ErrorBadState:
			translatedError = NETWORK_ERROR_BAD_STATE;
			break;
			
		case kCFSOCKS5ErrorBadResponseAddr:
			translatedError = NETWORK_ERROR_BAD_RESPONSE_ADDR;
			break;
			
		case kCFSOCKS5ErrorBadCredentials:
			translatedError = NETWORK_ERROR_BAD_CREDENTIALS;
			break;
			
		case kCFSOCKS5ErrorUnsupportedNegotiationMethod:
			translatedError = NETWORK_ERROR_UNSUPPORTED_NEGOTIATION_METHOD;
			break;
			
		case kCFSOCKS5ErrorNoAcceptableMethod:
			translatedError = NETWORK_ERROR_NO_ACCEPTABLE_METHOD;
			break;
			
		case kCFFTPErrorUnexpectedStatusCode:
			translatedError = NETWORK_ERROR_FTP_UNEXPECTED_STATUS_CODE;
			break;

		case kCFErrorHTTPAuthenticationTypeUnsupported:
			translatedError = NETWORK_ERROR_HTTP_AUTHENTICATION_TYPE_UNSUPPORTED;
			break;
			
		case kCFErrorHTTPBadCredentials:
			translatedError = NETWORK_ERROR_HTTP_BAD_CREDENTIALS;
			break;
			
		case kCFErrorHTTPConnectionLost:
			translatedError = NETWORK_ERROR_HTTP_CONNECTION_LOST;
			break;
			
		case kCFErrorHTTPParseFailure:
			translatedError = NETWORK_ERROR_HTTP_PARSE_FAILURE;
			break;
			
		case kCFErrorHTTPRedirectionLoopDetected:
			translatedError = NETWORK_ERROR_HTTP_REDIRECTION_LOOP;
			break;
			
		case kCFErrorHTTPBadURL:
			translatedError = NETWORK_ERROR_HTTP_BAD_URL;
			break;
			
		case kCFErrorHTTPProxyConnectionFailure:
			translatedError = NETWORK_ERROR_HTTP_PROXY_CONNECTION_FAILURE;
			break;
			
		case kCFErrorHTTPBadProxyCredentials:
			translatedError = NETWORK_ERROR_HTTP_BAD_PROXY_CREDENTIALS;
			break;

		case kCFErrorPACFileError:
			translatedError = NETWORK_ERROR_PAC_FILE_ERROR;
			break;

		case kCFNetServiceErrorCollision:
			translatedError = NETWORK_ERROR_COLLISION;
			break;
			
		case kCFNetServiceErrorNotFound:
			translatedError = NETWORK_ERROR_NOT_FOUND;
			break;
			
		case kCFNetServiceErrorInProgress:
			translatedError = NETWORK_ERROR_IN_PROGRESS;
			break;
			
		case kCFNetServiceErrorBadArgument:
			translatedError = NETWORK_ERROR_BAD_ARGUMENT;
			break;
			
		case kCFNetServiceErrorCancel:
			translatedError = NETWORK_ERROR_CANCEL;
			break;
			
		case kCFNetServiceErrorInvalid:
			translatedError = NETWORK_ERROR_INVALID;
			break;
			
		case kCFNetServiceErrorTimeout:
			translatedError = NETWORK_ERROR_TIMEOUT;
			break;
			
		case kCFNetServiceErrorDNSServiceFailure:
			translatedError = NETWORK_ERROR_DNS_FAILURE;
			break;
			
		case kCFNetServiceErrorUnknown:
		default:
			translatedError = NETWORK_ERROR_INTERNAL;
			break;
	}
	
	GetMessageForErrorCode( translatedError, pOutErrorStr );
	return translatedError;
}

// Mantendo a compatibilidade do código com versões de iPhoneOS anteriores à 3.0
#if ! defined( __IPHONE_3_0 )
	#undef kCFErrorPACFileError
	#undef kCFErrorPACFileAuth
#endif

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

NetworkManager::NetworkManager( INetworkListener *pListener ) : pCurrRequest( NULL ), pReadStream( NULL ), pListener( pListener ), responseTotalLen( -1 ), responseBytesCounter( 0 ), responseBuffer()
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

NetworkManager::~NetworkManager( void )
{
	clean();
}

/*==============================================================================================

MÉTODO clean
	Libera a memória alocada pelo objeto.

==============================================================================================*/

void NetworkManager::clean( bool shouldDeleteRequest )
{
	if( shouldDeleteRequest )
		SAFE_CFKILL( pCurrRequest );

	if( pReadStream )
	{
		CFReadStreamUnscheduleFromRunLoop( pReadStream, CFRunLoopGetCurrent(), kCFRunLoopCommonModes );
		CFReadStreamClose( pReadStream );
		CFKILL( pReadStream );
	}
	responseTotalLen = -1;
	responseBytesCounter = 0;
	
	responseBuffer.clear();
}

/*==============================================================================================

MÉTODO sendRequest
	Inicia uma requisição HTTP. Quando retorna false, o chamador fica encarregado de destruir
pRequest. Em caso contrário, NetworkManager será o responsável por fazê-lo.

==============================================================================================*/

bool NetworkManager::sendRequest( CFHTTPMessageRef pRequest )
{
	// Se já possuímos uma stream aberta, ignora a chamada
	if( pReadStream )
		return false;

	// Cria a stream que irá tratar a requisição
	pReadStream = createReadStream( pRequest, false );
	if( !pReadStream )
		return false;
	
	// Armazena a requisição
	pCurrRequest = pRequest;
	
	// Zera o buffer
	responseTotalLen = -1;
	responseBytesCounter = 0;
	responseBuffer.clear();

	return true;
}

/*==============================================================================================

MÉTODO CreateReadStream
	Cria uma stream de leitura de dados. Retorna NULL em casos de erro.

==============================================================================================*/

CFReadStreamRef NetworkManager::createReadStream( const CFHTTPMessageRef pHTTPRequest, bool autoRedirect )
{
	// Envia a requisição e espera pela resposta. A partir daqui, o método readEventHandler fica responsável por
	// tratar os dados recebidos
	CFReadStreamRef pAux = CFReadStreamCreateForHTTPRequest( kCFAllocatorDefault, pHTTPRequest );
	if( !pAux )
		return NULL;

	// Flags possíveis:
	// - kCFStreamEventNone
	// - kCFStreamEventOpenCompleted
	// - kCFStreamEventHasBytesAvailable
	// - kCFStreamEventCanAcceptBytes
	// - kCFStreamEventErrorOccurred
	// - kCFStreamEventEndEncountered
	#if DEBUG
	CFOptionFlags registeredEvents = kCFStreamEventHasBytesAvailable | kCFStreamEventErrorOccurred | kCFStreamEventEndEncountered | kCFStreamEventOpenCompleted; 
	#else
	CFOptionFlags registeredEvents = kCFStreamEventHasBytesAvailable | kCFStreamEventErrorOccurred | kCFStreamEventEndEncountered; 
	#endif
	CFStreamClientContext myContext = { 0, this, NULL, NULL, NULL };

	if( CFReadStreamSetClient( pAux, registeredEvents, ReadEventHandler, &myContext ) == TRUE )
	{
		CFReadStreamScheduleWithRunLoop( pAux, CFRunLoopGetCurrent(), kCFRunLoopCommonModes );
		CFReadStreamSetProperty( pAux, kCFStreamPropertyHTTPShouldAutoredirect, autoRedirect ? kCFBooleanTrue : kCFBooleanFalse );
		
		if( CFReadStreamOpen( pAux ) == FALSE )
		{
			CFKILL( pAux );
		}
	}
	else
	{
		CFKILL( pAux );
	}
	
	return pAux;
}

/*==============================================================================================

MÉTODO cancelPendingRequest
	Cancela a requisição pendente. Caso não haja uma requisição, não faz nada.

==============================================================================================*/

void NetworkManager::cancelPendingRequest( void )
{
	if( pReadStream )
	{
		clean();
		
		if( pListener )
			pListener->onNetworkRequestCancelled();
	}
}

/*==============================================================================================

MÉTODO ReadEventHandler
	Callback para tratar eventos das streams de leitura.

==============================================================================================*/

void NetworkManager::ReadEventHandler( CFReadStreamRef pStream, CFStreamEventType event, void *pUser )
{
	NetworkManager* pMe = static_cast< NetworkManager* >( pUser );
	
	// Garante que não vamos continuar lendo dados após termos cancelado uma requisição (um evento pode ter ficado
	// pendente...)
	if( pMe->pReadStream == NULL )
		return;

	switch( event )
	{ 
		#if DEBUG
		case kCFStreamEventOpenCompleted:
			LOG( ">>> NetworkManager::ReadEventHandler - kCFStreamEventOpenCompleted\n" );
			break;
		#endif

		case kCFStreamEventHasBytesAvailable:
			{
				CFHTTPMessageRef pResponse = ( CFHTTPMessageRef )CFReadStreamCopyProperty( pStream, kCFStreamPropertyHTTPResponseHeader );

				#if DEBUG
				{
					// Obtém o status da resposta por escrito
					CFStringRef pStatusLine = CFHTTPMessageCopyResponseStatusLine( pResponse );

					char buffer[ NETWORK_MANAGER_READ_BUFFER_LEN ];
					if( CFStringGetCString( pStatusLine, buffer, NETWORK_MANAGER_READ_BUFFER_LEN, kCFStringEncodingUTF8 ) == TRUE )
						LOG( ">>> NetworkManager::ReadEventHandler - Request Status: %s\n", buffer );
				
					CFKILL( pStatusLine );
				}
				#endif

				// OBS: Outros métodos que podem ser utilizados antes de destruirmos pResponse:
//				CFHTTPMessageCopyBody  
//				CFHTTPMessageCopyAllHeaderFields  
//				CFHTTPMessageCopyHeaderFieldValue  
//				CFHTTPMessageCopyRequestMethod  
//				CFHTTPMessageCopyRequestURL  
//				CFHTTPMessageCopySerializedMessage  
//				CFHTTPMessageCopyVersion  
//				CFHTTPMessageIsRequest  
//				CFHTTPMessageIsHeaderComplete
				UInt32 httpStatusCode = CFHTTPMessageGetResponseStatusCode( pResponse );
				
				// Obtém o número total de bytes da transação
				if( pMe->responseTotalLen < 0 )
				{
					CFStringRef hContentLenStr = CFHTTPMessageCopyHeaderFieldValue( pResponse, CFSTR( "Content-Length" ) );

					// Se não conseguimos ler, a notificação de progresso vai ficar danificada. Porém, o usuário poderá
					// verificar o erro ao receber "totalBytes < 0" nas chamadas de 
					// "void onNetworkProgressChanged( int32 currBytes, int32 totalBytes )".
					if( hContentLenStr )
					{					
						#if DEBUG
							CFShow( hContentLenStr );
						#endif
						
						pMe->responseTotalLen = CFStringGetIntValue( hContentLenStr );
						pMe->responseBytesCounter = 0;

						CFKILL( hContentLenStr );
					}
				}

				CFKILL( pResponse );
				
				if( IS_REDIRECTION_HTTP_STATUS_CODE( httpStatusCode ) )
				{
					// Se foi um redirecionamento, tenta carregar a outra URL (desde que ainda não tenha tentado outro redirecionamento)
					CFBooleanRef pRedirecting = static_cast< CFBooleanRef >( CFReadStreamCopyProperty( pStream, kCFStreamPropertyHTTPShouldAutoredirect ) );
					bool alreadyRedirecting = pRedirecting == kCFBooleanTrue ? true : false;
					
					if( !alreadyRedirecting )
					{
						pMe->clean( false );

						pMe->pReadStream = pMe->createReadStream( pMe->pCurrRequest, true );
						if( pMe->pReadStream == NULL )
						{
							CFKILL( pMe->pCurrRequest );

							if( pMe->pListener )
							{
								std::string errorStr;
								pMe->pListener->onNetworkError( NETWORK_ERROR_COULDNT_REDIRECT, *GetMessageForErrorCode( NETWORK_ERROR_COULDNT_REDIRECT, &errorStr ) );
							}
						}
					}
					else
					{
						pMe->clean();

						if( pMe->pListener )
						{
							std::string errorStr;
							pMe->pListener->onNetworkError( NETWORK_ERROR_TOO_MANY_REDIRECTIONS, *GetMessageForErrorCode( NETWORK_ERROR_TOO_MANY_REDIRECTIONS, &errorStr ) );
						}
					}
				}
				else if( httpStatusCode == HTTP_STATUS_OK )
				{
					// Aqui podemos chamar CFReadStreamRead sem problemas. Não iremos bloquear a thread pois temos
					// bytes disponíveis para leitura 
					uint8 buffer[ NETWORK_MANAGER_READ_BUFFER_LEN ]; 
					CFIndex bytesRead = CFReadStreamRead( pStream, buffer, NETWORK_MANAGER_READ_BUFFER_LEN );
					if( bytesRead > 0 )
					{
						pMe->responseBuffer.insert( pMe->responseBuffer.end(), buffer, buffer + bytesRead );

						#if DEBUG
							buffer[ bytesRead ] = '\0';
							LOG( ">>> NetworkManager::ReadEventHandler - Data: %s\n", buffer );
						#endif
						
						pMe->responseBytesCounter += bytesRead;
						pMe->pListener->onNetworkProgressChanged( pMe->responseBytesCounter, pMe->responseTotalLen );
					}
					// Podemos ignorar valores inferiores ou iguais a 0 de bytesRead, já que nesses casos receberemos outros eventos
					//else
					//{
					//}
				}
				else
				{
					// Se recebemos alguma coisa que não sabemos tratar, temos que disparar um erro...
					NetworkManagerErrors errorCode = NETWORK_ERROR_INTERNAL;
				
					std::string errorStr;
					GetMessageForErrorCode( errorCode, &errorStr );
					
					pMe->clean();

					if( pMe->pListener )
						pMe->pListener->onNetworkError( errorCode, errorStr );
				}
			}
			break; 

		case kCFStreamEventErrorOccurred:
			{
				NetworkManagerErrors errorCode = NETWORK_ERROR_INTERNAL;
				
				std::string errorStr;
				GetMessageForErrorCode( errorCode, &errorStr );

				CFErrorRef pError = CFReadStreamCopyError( pStream );
				if( pError )
				{
					errorCode = TranslateErrorCode( static_cast< CFNetworkErrors >( CFErrorGetCode( pError ) ), &errorStr );
					
					#if DEBUG
						PrintError( pError );
					#endif
					
					CFKILL( pError );
				}
				
				pMe->clean();

				if( pMe->pListener )
					pMe->pListener->onNetworkError( errorCode, errorStr );
			}
			break;

		case kCFStreamEventEndEncountered:
			{
				if( pMe->pListener )
				{
					pMe->pListener->onNetworkProgressChanged( pMe->responseTotalLen, pMe->responseTotalLen );
					pMe->pListener->onNetworkRequestCompleted( pMe->responseBuffer );
				}
				
				pMe->clean();
			}
			break; 
	}
}


