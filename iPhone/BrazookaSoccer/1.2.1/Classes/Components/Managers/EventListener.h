/*
 *  EventListener.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 11/12/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EVENT_LISTENER_H
#define EVENT_LISTENER_H 1

#include "EventTypes.h"
#include "NanoTypes.h"

class EventListener
{
	public:
		// Destrutor
		virtual ~EventListener( void );
	
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) = 0;

		// Indica que o listener deve começar a receber eventos dos tipo "eventType"
		inline void startListeningTo( EventTypes eventTypes ) { listeningTo |= eventTypes; };

		// Indica que o listener deve parar de receber eventos do tipo "eventType"
		inline void stopListeningTo( EventTypes eventTypes ) { listeningTo &= ~eventTypes; };

		// Indica se o listener está esperando eventos dos tipo "eventType"
		inline bool isListeningTo( EventTypes eventTypes ) const { return ( listeningTo & eventTypes ) == eventTypes; };
	
	protected:
		// Construtor
		EventListener( EventTypes listeningTo = EVENT_NONE );

	private:
		// Índice deste objeto para o gerenciador de eventos
		int16 eventManagerIndex;
	
		// Indica por quais tipos de eventos o listener está esperando
		int16 listeningTo;
};

#endif
