/*
 *  ApplicationErrors.h
 *  NetworkTest
 *
 *  Created by Daniel Lopes Alves on 9/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef APPLICATION_ERRORS_H
#define APPLICATION_ERRORS_H

// Erros padrões
#define ERROR_NONE					0x0000
#define ERROR_MEMORY_WARNING		0x0001
#define ERROR_CREATING_LOADING_VIEW	0x0002

// Erros NanoOnline
#define ERROR_NO_ALOCATING_DATA		0x0050
#define ERROR_NO_ALOCATING_VIEWS	0x0051

// Erros do usuário
#define ERROR_USER					0x0100

#endif
