#include "Pattern.h"

#include <math.h>

#include "Macros.h"

// TODOO
#warning Pattern TODOS
#warning Otimizar para renderizar de acordo com o viewport

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Pattern::Pattern( Object* pFill ) : Object(), offsetX( 0.0f ), offsetY( 0.0f ), pFill( pFill )
{
}
		
/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Pattern::~Pattern( void )
{
	DESTROY( pFill );
}

/*==============================================================================================

MÉTODO setFill
	Determina um objeto de preenchimento para o pattern.

==============================================================================================*/

void Pattern::setFill( Object* pPatternFill )
{
	// Apaga o objeto de preenchimento antigo
	delete pFill;
	
	// Armazena os parâmetros
	pFill = pPatternFill;
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Pattern::render( void )
{
	if( !Object::render() || ( pFill == NULL ) || !pFill->isVisible() )
		return false;

	// TODOO
//	// TODOO : Vai dar problema se estiver dentro de um grupo que não seja o maior grupode todos!!!!
//	// O certo seria ter um método getScreenPos() !!!
//	Viewport sizeViewport( position.x, position.y, size.x, size.y );
//	
//	// Obtém a interseção dos viewports
//	Viewport intersection;
//	bool customViewport = viewport.getIntersection( intersection, sizeViewport ) != GET_DEFAULT_VIEWPORT();
//	
//	if( customViewport )
//	{
//		glEnable( GL_SCISSOR_TEST );
//		glScissor( intersection.x, intersection.y, intersection.width, intersection.height );
//	}
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}

	float fillWidth = pFill->getWidth(), fillHeight = pFill->getHeight();
	uint16 xTimes = static_cast< uint16 >( ceil( size.x / fillWidth ) );
	uint16 yTimes = static_cast< uint16 >( ceil( size.y / fillHeight ) );
	
	float xInc = fillWidth /* TODOO + offsetX */;
	float yInc = fillHeight /* TODOO + offsetY */;
	
	pFill->setPosition();

	glMatrixMode( GL_MODELVIEW );
	
	for( uint16 y = 0; y < yTimes ; ++y )
	{
		glPushMatrix();
		glTranslatef( position.x, position.y + ( y * yInc ), 0.0f );

		for( uint16 x = 0; x < xTimes ; ++x )
		{
			pFill->render();
			glTranslatef( xInc, 0.0f, 0.0f );
		}

		glPopMatrix();
	}

	if( customViewport )
		glDisable( GL_SCISSOR_TEST );

	return true;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool Pattern::update( float timeElapsed )
{
	if( !Object::update( timeElapsed ) || ( pFill == NULL ) )
		return false;

	pFill->update( timeElapsed );

	return true;
}

/*==============================================================================================

MÉTODO setOffset
	Determina um offset de desenho entre as repetições do pattern.

==============================================================================================*/

void Pattern::setOffset( float x, float y )
{
	offsetX = x;
	offsetY = y;
}
