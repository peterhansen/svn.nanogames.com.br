#include "PerspectiveCamera.h"

// Components
#include "MathFuncs.h"
#include "ObjcMacros.h"

// OpenGL
#include "GLHeaders.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

PerspectiveCamera::PerspectiveCamera( float fovy, float aspect, float zNear, float zFar, CameraType cameraType )
: Camera( cameraType ), projection( 0.0f )
{
	setPerspective( fovy, aspect, zNear, zFar );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

PerspectiveCamera::PerspectiveCamera( float left, float right, float bottom, float top,
									  float zNear, float zFar, CameraType cameraType )
: Camera( cameraType ), projection()
{
	setFrustum( left, right, bottom, top, zNear, zFar );
}

/*==============================================================================================

MÉTODO setPerspective
	Determina a matriz de projeção utilizando gluPerspective.

==============================================================================================*/

void PerspectiveCamera::setPerspective( float fovy, float aspect, float zNear, float zFar )
{
    const float radians = static_cast< float >( ( fovy / 2.0f ) * ( M_PI / 180.0f ) );

    const float deltaZ = zFar - zNear;
    const float sine = static_cast< float >( sin( radians ) );
    if( NanoMath::feql( deltaZ, 0.0f ) || NanoMath::feql( sine, 0.0f ) || NanoMath::feql( aspect, 0.0f ) )
		return;
	
    const float cotangent = static_cast< float >( cos( radians ) / sine );

    projection._00 = cotangent / aspect;
    projection._11 = cotangent;
    projection._22 = -( zFar + zNear ) / deltaZ;
	projection._23 = -1.0f;
    projection._32 = -2.0f * zNear * zFar / deltaZ;
    projection._33 = 0.0f;
}

/*==============================================================================================

MÉTODO setFrustum
	Determina a matriz de projeção utilizando glFrustum.

==============================================================================================*/

void PerspectiveCamera::setFrustum( float left, float right, float bottom, float top, float zNear, float zFar )
{
	int mattrixMode;
	glGetIntegerv( GL_MATRIX_MODE, &mattrixMode );
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glFrustumf( left, right, bottom, top, zNear, zFar );
	glGetFloatv( GL_PROJECTION_MATRIX, reinterpret_cast< float* >( &projection ) );
	glPopMatrix();
	glMatrixMode( mattrixMode );
}

/*==============================================================================================

MÉTODO place
	Posiciona a câmera na cena.

==============================================================================================*/

void PerspectiveCamera::place( void )
{
	glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glMultMatrixf( projection );

	Camera::place();
}

/*==============================================================================================

MÉTODO getViewVolume
	Retorna o volume de visualização da câmera.

==============================================================================================*/

Box* PerspectiveCamera::getViewVolume( Box* pViewVolume )
{
	// TODOO : Implementar!!!!!!!!!
	return pViewVolume;
}

/*==============================================================================================

MÉTODO getProjectionMatrix
	Obtém a matriz de projeção da câmera.

==============================================================================================*/

Matrix4x4* PerspectiveCamera::getProjectionMatrix( Matrix4x4* pOut ) const
{
	*pOut = projection;
	
	// Matrizes openGL são column-major
	pOut->transpose();

	return pOut;
}
