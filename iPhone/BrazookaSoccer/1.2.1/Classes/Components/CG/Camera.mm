#include "Camera.h"

// Components
#include "Macros.h"

// OpenGL
#include "GLHeaders.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Camera::Camera( CameraType cameraType ) : type( cameraType ), zoomFactor( 1.0f ),
										  pos( 0.0f, 0.0f, 0.0f ),
										  right( 1.0f, 0.0f, 0.0f ),
										  up( 0.0f, 1.0f, 0.0f ),
										  look( 0.0f, 0.0f, -1.0f )
{
}

/*==============================================================================================

MÉTODO place
	Configura os atributos da câmera na API gráfica.

==============================================================================================*/

void Camera::place( void )
{
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	
	// Matrizes openGL são column-major
	Matrix4x4 modelView;
	glMultMatrixf( getModelViewMatrix( &modelView )->transpose() );
}

/*==============================================================================================

MÉTODO lookAt
	Equivalente a gluLookAt
	- http://www.opengl.org/documentation/specs/man_pages/hardcopy/GL/html/glu/lookat.html

==============================================================================================*/

void Camera::lookAt( const Point3f* pEye, const Point3f* pCenter, const Point3f* pUp )
{
	Point3f f = Point3f( pCenter->x - pEye->x, pCenter->y - pEye->y, pCenter->z - pEye->z );
	f.normalize();
	
	Point3f up = *pUp;
	up.normalize();
	
	const Point3f s = f % up;
	const Point3f u = s % f;
	
	const float m[] = {	 s.x,   s.y,  s.z, 0.0f,
						 u.x,   u.y,  u.z, 0.0f,
						-f.x,  -f.y, -f.z, 0.0f,
						 0.0f, 0.0f, 0.0f, 1.0f };

	glMultMatrixf( m );
	glTranslatef( -pEye->x, -pEye->y, -pEye->z );
}

/*==============================================================================================

MÉTODO zoom
	Aproxima a cena em x vezes.

==============================================================================================*/

void Camera::zoom( float x )
{
	zoomFactor = fabsf( x );
}

/*==============================================================================================

MÉTODO pitch
	Rotaciona a câmera ao redor do eixo right.

==============================================================================================*/

void Camera::pitch( float angle )
{
	Matrix4x4 m;
	Utils::GetRotationMatrix( &m, angle, &right );
	
	Utils::TransformCoord( &up, &up, &m );
	Utils::TransformCoord( &look, &look, &m );
}

/*==============================================================================================

MÉTODO yaw
	Rotaciona a câmera ao redor do eixo up.

==============================================================================================*/

void Camera::yaw( float angle )
{
	Matrix4x4 m;

	switch( type )
	{
		case CAMERA_TYPE_LAND:
			{
				Point3f worldUp( 0.0f, 1.0f, 0.0f );
				Utils::GetRotationMatrix( &m, angle, &worldUp );
			}
			break;
			
		case CAMERA_TYPE_AIR:
			Utils::GetRotationMatrix( &m, angle, &up );
			break;
	}

	Utils::TransformCoord( &right, &right, &m );
	Utils::TransformCoord( &look, &look, &m );
}

/*==============================================================================================

MÉTODO roll
	Rotaciona a câmera ao redor do eixo look.

==============================================================================================*/

void Camera::roll( float angle )
{
	if( type == CAMERA_TYPE_AIR )
	{
		Matrix4x4 m;
		Utils::GetRotationMatrix( &m, angle, &look );
		
		Utils::TransformCoord( &right, &right, &m );
		Utils::TransformCoord( &up, &up, &m );
	}
}

/*==============================================================================================

MÉTODO walk
	Movimenta a câmera ao longo do eixo look.

==============================================================================================*/

void Camera::walk( float units )
{
	switch( type )
	{
		case CAMERA_TYPE_LAND:
			pos += Point3f( look.x, 0.0f, look.z ) * units;
			break;
			
		case CAMERA_TYPE_AIR:
			pos += look * units;
			break;
	}
}

/*==============================================================================================

MÉTODO strafe
	Movimenta a câmera ao longo do eixo right.

==============================================================================================*/

void Camera::strafe( float units )
{
	switch( type )
	{
		case CAMERA_TYPE_LAND:
			pos += Point3f( right.x, 0.0f, right.z ) * units;
			break;
			
		case CAMERA_TYPE_AIR:
			pos += right * units;
			break;
	}
}

/*==============================================================================================

MÉTODO fly
	Movimenta a câmera ao longo do eixo up.

==============================================================================================*/

void Camera::fly( float units )
{
	if( type == CAMERA_TYPE_AIR )
		pos += up * units;
}

/*==============================================================================================

MÉTODO lookAt
	Direciona o eixo look.

==============================================================================================*/

void Camera::lookAt( const Point3f* pSpot )
{
	look.set( pSpot->x - pos.x, pSpot->y - pos.y, pSpot->z - pos.z );
	makeAxesOrthogonal( &look, &up, &right );
}

/*==============================================================================================

MÉTODO getModelViewMatrix
	Obtém a matriz de visualização da mera.

==============================================================================================*/

Matrix4x4* Camera::getModelViewMatrix( Matrix4x4* pOut ) const
{
	// Mantém os eixos da câmera ortogonais entre si
	Point3f _look = look;
	Point3f _up = up;
	Point3f _right = right;
	makeAxesOrthogonal( &_look, &_up, &_right );
	
	// Constrói a matriz
	// OBS: Matrizes OpenGL são ordenadas da forma column-major
	pOut->set(           _right.x,       _right.y,        _right.z, 0.0f,
			                _up.x,          _up.y,           _up.z, 0.0f,
			             -_look.x,       -_look.y,        -_look.z, 0.0f,
				-( _right * pos ), -( _up * pos ), ( _look * pos ), 1.0f );

	Matrix4x4 identity;
	( *pOut ) *= identity;
	
	// Matrizes openGL são column-major
	pOut->transpose();
	
	return pOut;
}

/*==============================================================================================

MÉTODO makeAxesOrthogonal
	Garante que os eixos right, up e look são ortogonais entre si.

==============================================================================================*/

void Camera::makeAxesOrthogonal( Point3f* pLook, Point3f* pUp, Point3f* pRight )
{
	pLook->normalize();
	pUp->normalize();
	
	*pRight = ( *pLook ) % ( *pUp );
	*pUp = ( *pRight ) % ( *pLook );
}

