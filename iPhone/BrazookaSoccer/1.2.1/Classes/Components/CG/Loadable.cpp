#include "Loadable.h"

/*==============================================================================================
 
 CONSTRUTOR
 
 ==============================================================================================*/

Loadable::Loadable( LoadableListener* pListener, int32 loadableId, uint32 data ) : pListener( pListener ), myId( loadableId ), data( data )
{
}

/*==============================================================================================
 
DESTRUTOR
 
===============================================================================================*/

Loadable::~Loadable( void )
{
	pListener = NULL;
}

/*==============================================================================================
 
MÉTODO load
	Ativa os estados do OpenGL.
 
==============================================================================================*/


void Loadable::load( void )
{
	if( pListener )
		pListener->loadableHandleEvent( LOADABLE_OP_LOAD, myId, data );
}

/*==============================================================================================
 
MÉTODO unload
	Desativa os estados do OpenGL.
 
==============================================================================================*/

void Loadable::unload( void )
{
	if( pListener )
		pListener->loadableHandleEvent( LOADABLE_OP_UNLOAD, myId, data );
}

/*==============================================================================================
 
MÉTODO setListener
	Determina um listener que irá receber os eventos desse objeto.
 
==============================================================================================*/

void Loadable::setListener( LoadableListener* pListener )
{
	this->pListener = pListener;
}

/*==============================================================================================
 
MÉTODO setListenerData
	Determina dados extras para serem enviados ao listener.
 
==============================================================================================*/

void Loadable::setListenerData( uint32 data )
{
	this->data = data;
}
