/*
 *  Bitmap.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 6/25/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BITMAP_H
#define BITMAP_H 1

#include <stdio.h> // Definição de NULL

#include "NanoTypes.h"

struct BmpInfoHeader
{
	int32 headerSize;
	int32 width;
	int32 height;
	int16 numColorPlanes;
	int16 bpp;
	int32 compressionType;
	int32 rawDataSize;
	int32 horResolution;
	int32 verResolution;
	int32 paletteNColors;
	int32 nImportantColors;
};

class BitmapParser
{
	public:
		// Construtor
		BitmapParser( void );
	
		// Preenche as estruturas do bitmap com os dados obtidos de pData
		bool parse( const uint8* pData );
	
		// Retorna a largura da imagem em pixels
		uint32 getWidth( void ) const;
	
		// Retorna a altura da imagem em pixels
		uint32 getHeight( void ) const;
	
		// Retorna o tamanho, em bytes, dos dados (<paleta de cores> + pixels) da imagem
		uint32 getRawDataSize( void ) const;
	
		// Retorna um ponteiro para os dados (<paleta de cores> + pixels) da imagem
		const uint8* getRawDataPointer( void ) const;

	private:
		#if DEBUG
			// Imprime a paleta da imagem na tela
			void printPalette( void );
		#endif
	
		// Ponteiro para o arquivo
		uint8* pBmpData;
	
		// Ponteiro para a paleta de cores
		uint8* pPalette;
	
		// Ponteiro para os pixels da imagem
		uint8* pPixels;
	
		// Dados do bitmap
		BmpInfoHeader infoHeader;
};

// Implementação dos métodos inline

// Retorna a largura da imagem em pixels
inline uint32 BitmapParser::getWidth( void ) const
{
	return infoHeader.width;
}
	
// Retorna a altura da imagem em pixels
inline uint32 BitmapParser::getHeight( void ) const
{
	return infoHeader.height;
}
	
// Retorna o tamanho, em bytes, dos dados (<paleta de cores> + pixels) da imagem
inline uint32 BitmapParser::getRawDataSize( void ) const
{
	return infoHeader.rawDataSize + ( infoHeader.paletteNColors * 4 );
}
	
// Retorna um ponteiro para os dados (<paleta de cores> + pixels) da imagem
inline const uint8* BitmapParser::getRawDataPointer( void ) const
{
	return pPalette != NULL ? pPalette : pPixels;
}

#endif
