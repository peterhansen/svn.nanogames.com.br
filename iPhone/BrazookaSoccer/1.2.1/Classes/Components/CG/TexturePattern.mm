#include "TexturePattern.h"

// Components
#include "Camera.h"
#include "Exceptions.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"
#include "Quad.h"
#include "Scene.h"
#include "TextureFrame.h"

// OpenGL
#include "GLHeaders.h"

// TODOO
#warning TexturePattern TODOS
#warning Esta classe deve derivar de RenderableImage!!!!!!!!!!!
#warning Otimizar para renderizar de acordo com o viewport

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

TexturePattern::TexturePattern( const char* pImg, const TextureFrame* pFrame, LoadTextureFunction pLoadTextureFunction )
			   : Object(), offsetX( 0.0f ), offsetY( 0.0f ), vertexes()
{
	build( pLoadTextureFunction( pImg, NULL ), pFrame );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

TexturePattern::TexturePattern( const TexturePattern* pOther )
			   : Object(), offsetX( pOther->offsetX ), offsetY( pOther->offsetY ), vertexes()
{
	build( pOther->pTex, NULL );
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

void TexturePattern::build( Texture2DHandler pImg, const TextureFrame* pFrame )
{
	pTex = pImg;
	if( !pTex )
#if DEBUG
		throw ConstructorException( "TexturePattern::build( const char*, const TextureFrame*, LoadTextureFunction ): Unable to load image" );
#else
		throw ConstructorException();
#endif
	
	if( pFrame != NULL )
		pTex->changeFrame( 0, pFrame );
	
	float texCoords[ QUAD_N_VERTEXES * 2 ];
	vertexes.mapTexCoords( pTex->getFrameTexCoords( 0, texCoords ));

	Object::setSize( 0.0f, 0.0f, 1.0f );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool TexturePattern::render( void )
{
	if( !Object::render() )
		return false;
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );
	
	// Carrega a textura
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pTex->load();
	
	// Determina o modo de texturização
	glColor4ub( 255, 255, 255, 255 );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	
	if( NanoMath::fdif( scale.x, 1.0f ) || NanoMath::fdif( scale.y, 1.0f ) )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	}
	else
	{	
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	}

	// Renderiza o objeto
	// OBS: Trata a renderização por fora de Quad para otimizar a renderização
	glDisableClientState( GL_NORMAL_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	
	const float* pAux = reinterpret_cast< const float* >( vertexes.getVertexes() );
	
	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( 3, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_POSITION_START ] );
	
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	glTexCoordPointer( 2, GL_FLOAT, Vertex::VERTEX_COMPONENTS_STRIDE, &pAux[ Vertex::VERTEX_TEXCOORD_START ] );
	
	TextureFrame texFrame = *( pTex->getFrame( 0 ) );
	
	// TODOO : Parametrizar este * 4.0f !!!!
	float fillWidth = texFrame.width * scale.x/* * 4.0f*/, fillHeight = texFrame.height * scale.y /** 4.0f*/;
	uint16 xTimes = static_cast< uint16 >( ceil( size.x / fillWidth ) );
	uint16 yTimes = static_cast< uint16 >( ceil( size.y / fillHeight ) );
	
	glMatrixMode( GL_MODELVIEW );
	
	float startX = position.x + ( getWidth() * 0.5f ) + 0.375f;
	float startY = position.y + ( getHeight() * 0.5f ) + 0.375f;
	for( uint16 y = 0; y < yTimes ; ++y )
	{
		for( uint16 x = 0; x < xTimes ; ++x )
		{
			glPushMatrix();
			
			// Posiciona o objeto
			glTranslatef( startX + ( x * fillWidth ) - x, startY + ( y * fillHeight ) - y, 0.0f );

			// Redimensiona o objeto
			glScalef( fillWidth, fillHeight, 1.0f );

			// Renderiza
			glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );

			glPopMatrix();
		}
	}
	
	// Reseta os estados do OpenGL
	pTex->unload();

	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );

	if( customViewport )
		glDisable( GL_SCISSOR_TEST );

	return true;
}

/*==============================================================================================

MÉTODO setOffset
	Determina um offset de desenho entre as repetições do pattern.

==============================================================================================*/

void TexturePattern::setOffset( float x, float y )
{
	offsetX = x;
	offsetY = y;
}
