#include "Scene.h"

// Components
#include "Macros.h"

// OpenGL
#include "GLHeaders.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Scene::Scene( uint16 maxObjects ) : ObjectGroup( maxObjects ), clearBits( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ), clearDepth( 1.0f ), clearStencil( 0 ), pCurrCamera( NULL ), clearColor()
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Scene::~Scene( void )
{
	DELETE( pCurrCamera );
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool Scene::render( void )
{
	glClearColor( clearColor.getFloatR(), clearColor.getFloatG(), clearColor.getFloatB(), clearColor.getFloatA() );
	glClearDepthf( clearDepth );
	
	// TODOO : Stencil ainda não é suportado pelo iphone. Ver se esse trecho de código vai dar problema
//	glClearStencil( clearStencil );

	glClear( clearBits );
	
	pCurrCamera->place();
	
	return ObjectGroup::render();
}

/*==============================================================================================

MÉTODO setCurrentCamera

==============================================================================================*/

void Scene::setCurrentCamera( Camera* pCamera )
{
	// Deleta a câmera atual
	SAFE_DELETE( pCurrCamera );

	// Armazena a nova câmera
	pCurrCamera = pCamera;
}

