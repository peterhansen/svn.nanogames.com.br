/*
 *  Triangle.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef TRIANGLE_H
#define TRIANGLE_H 1

#include <cstdio> // Definição de NULL

#include "Point3f.h"

class Triangle
{
	public:
		// XCode ainda não suporta o padrão N2210
		// http://www.google.com.br/search?hl=pt-BR&client=firefox-a&rls=org.mozilla%3Apt-BR%3Aofficial&q=C%2B%2B+n2210+&btnG=Pesquisar&meta=
		// Vértices do triângulo
		/*union
		{
			struct
			{
				Point3f v0, v1, v2;
			};
			Point3f vertexes[3];
		};*/
		// Vértices do triângulo
		Point3f vertexes[3];
	
		// Construtores
		Triangle( void ) {};
		Triangle( const Point3f* pV0, const Point3f* pV1, const Point3f* pV2 );
	
		// Inicializa o objeto
		inline void set( const Point3f* pV0 = NULL, const Point3f* pV1 = NULL, const Point3f* pV2 = NULL ){ if( pV0 )vertexes[0] = *pV0; else vertexes[0].set(); if( pV1 )vertexes[1] = *pV1; else vertexes[1].set(); if( pV2 )vertexes[2] = *pV2; else vertexes[2].set(); };
	
		// Retorna um ponto interno ao triângulo, utilizando as coordenadas baricêntricas u e v
		Point3f* getWithBarycentricCoords( Point3f* pOut, float u, float v );
};

#endif
