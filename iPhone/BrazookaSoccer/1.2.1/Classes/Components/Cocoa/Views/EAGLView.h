//
//  EAGLView.h
//  Components
//
//  Created by Daniel Lopes Alves on 10/3/08.
//  Copyright Nano Games 2008. All rights reserved.
//

#ifndef OPENGL_VIEW_H
#define OPENGL_VIEW_H 1

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>
#import <UIKit/UIKit.h>

#include "AudioManager.h"
#include "Color.h"
#include "Scene.h"

#if USE_OFFSCREEN_BUFFER
	#include "Texture2D.h"
#endif

enum UpdateMode
{
	UPDATE_MODE_FIXED = 0,
	UPDATE_MODE_VARIABLE = 1
};

// TODOO : Passar a derivar de UpdatableView
@interface EAGLView : UIView
{	
	@private
		// Largura e altura da tela e do back buffer
		int32 backingWidth;
		int32 backingHeight;

		// Contexto de renderização do OpenGL
		EAGLContext* hContext;
		
		// Nome do OpenGL para os buffers utilizados na renderização
		uint32 viewRenderbuffer, viewFramebuffer;
		
		// Nome do OpenGL para os buffers de profundidade
		uint32 depthRenderbuffer;

		// Controlam a atualização da tela
		NSTimer* hAnimationTimer;
		NSTimeInterval animationInterval;
	
		// Ponteiro para a cena atual
		Scene* pCurrScene;
	
		// Indica o modo de atualização da cena atual
		UpdateMode updateMode;
	
		// Armazena a última vez que chamamos update
		double lastUpdateTime;
	
		// Indica se devemos fazer a atualização da cena
		bool updateEnabled;
	
		#if USE_OFFSCREEN_BUFFER

			// Nome do OpenGL para os buffers "offscreen"
			uint32 offscreenFrameBuffer, offscreenDepthBuffer;
	
			// Textura que representa o offscreen renderbuffer
			Texture2D* pOffscreenRenderBuffer;

			// Indica se devemos exibir o conteúdo do offscreen buffer na tela
			bool showOffscreenBuffer;
	
			// Indica se estamos renderizando fora de tela
			bool usingOffscreenBuffer;

		#endif
}

@property ( nonatomic, assign, setter = setCurrScene, getter = getCurrScene ) Scene* pCurrScene;
@property ( nonatomic, assign, setter = setAnimationInterval ) NSTimeInterval animationInterval;

// Inicia o loop de renderização
- ( void ) startAnimation;

// Pára o loop de renderização
- ( void ) stopAnimation;

// Atualiza e renderiza a cena do jogo
- ( void ) run;

// Renderiza a cena do jogo
- ( void ) render;

// Pára o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL
- ( void ) suspend;

// Reinicia o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL
- ( void ) resume;

// Determina se devemos renderizar o offscreen buffer ao invés da cena atual
- ( void ) showOffscreenBuffer: ( bool )s;

// Indica se o usuário deseja executar as próximas rederizações fora da tela
- ( bool ) setOffscreenRender: ( bool )s;

// Limpa o offscreen buffer com a cor desejada
- ( void ) clearOffscreenBuffer: ( Color* ) pColor;

// Inicializa a operação de renderização
- ( void ) prepareRender;

// Finaliza a operação de renderização
- ( void ) finishRender;

// Indica se esta view deve receber múltiplos eventos de toque
- ( void ) enableMultipleTouch: ( bool )s;

// Indica que esta view não deve repassar os eventos de toque para outras views, se tornando
// a única tratadora de eventos da aplicação
- ( void ) enableExclusiveTouch: ( bool )s;

// Indica se devemos fazer a atualização da cena
- ( void ) setUpdateEnabled:( bool )enabled;

// Indica se estamos fazendo a atualização da cena
- ( bool ) isUpdateEnabled;

// Determina o modo de atualização da cena atual
- ( void ) setUpdateMode:( UpdateMode )mode;

@end

#endif

