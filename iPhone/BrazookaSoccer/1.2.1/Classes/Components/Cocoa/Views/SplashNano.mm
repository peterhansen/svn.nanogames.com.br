#import "SplashNano.h"

#include "ApplicationManager.h"
#include "Config.h"

// Extensão da classe para declarar métodos privados
@interface SplashNano ( Private )

//// Inicializa o objeto
//-( bool )buildSplashNano;

//// Libera a memória alocada pelo objeto
//-( void )cleanSplashNano;

// Chama a próxima tela do jogo
-( void )onEnd;

@end

// Início da implementação da classe
@implementation SplashNano

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self buildSplashNano] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildSplashNano] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	// [self cleanSplashNano];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM buildSplashNano
	Inicializa o objeto.

==============================================================================================*/

//-( bool )buildSplashNano
//{
//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view
//		// ...
//
//		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self cleanSplashNano];
//		return false;
//}

/*==============================================================================================

MENSAGEM cleanSplashNano
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanSplashNano
//{
//}

/*==============================================================================================

MENSAGEM onEnd
	Chama a próxima tela do jogo.

==============================================================================================*/

- ( void )onEnd
{
	[[ApplicationManager GetInstance] performTransitionToView: VIEW_INDEX_SPLASH_GAME];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.

==============================================================================================*/

-( void )onBecomeCurrentScreen
{
	[NSTimer scheduledTimerWithTimeInterval:SPLASH_NANO_DURATION target:self selector:@selector(onEnd) userInfo:NULL repeats:NO];
}

// Fim da implementação da classe
@end
