/*
 *  NOCallOutView.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/14/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef NO_CALLOUT_VIEW_H
#define NO_CALLOUT_VIEW_H

// Foundation
#import <UIKit/UIControl.h>

// Foward Classes
@class UILabel;

@interface NOCallOutView : UIControl
{
	@private
		// Contém a imagem da callout
		UIImageView *hImgView;
	
		// Labels que irão exibir as mensagens
		UILabel *hTempTitleLabel, *hTitleLabel;
	
		// Ponto para onde a "seta" da callout irá apontar
		CGPoint refPoint;
	
		// Controlador da animação
		NSTimer *hTimer;
}

// Reproduz os efeitos de "setTemporaryTitle:" de UICalloutView. Não podemos utilizar tal classe pois a Apple está reclamando que é uma API privada...
-( void )setTempTitle:( NSString* )hTempTitle;

// Retorna o título temporário da callout
-( NSString* )tempTitle;

// Reproduz os efeitos de "setTitle:" de UICalloutView. Não podemos utilizar tal classe pois a Apple está reclamando que é uma API privada...
-( void )setTitle:( NSString* )hTitle;

// Retorna o título principal da callout
-( NSString* )title;

// Reproduz os efeitos de "setAnchorPoint:boundaryRect:animate:" de UICalloutView. Não podemos utilizar tal classe pois a Apple está reclamando que é uma API privada...
-( void )setRefPoint:( CGPoint )p;

// Exibe a view
-( void )showCallout;

@end

#endif
