/*
 *  RefCounter.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/19/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef REF_COUNTER_H
#define REF_COUNTER_H 1

#include "Macros.h"

// OBS: Só utilizar esta classe caso seja necessário um nível extra de indireção!!! Caso contrário, utilizar std::tr1::shared_ptr<T>,
// contido em <tr1/boost_shared_prt>, ou boost::shared_ptr<T>, contido em <boost/shared_ptr.hpp>!!!!

class RefCounterBase
{
	public:
		virtual ~RefCounterBase( void ){};
};

template< class T > class RefCounter : public RefCounterBase
{
	public:
		// Construtores
		RefCounter( T* pObj = NULL ) : pRef( pObj ), pRefCount( pRef == NULL ? NULL : new int16( 1 ) ){};
	
		RefCounter( const RefCounter& r ) : pRef( r.pRef ), pRefCount( r.pRefCount )
		{
			if( pRefCount )
				++( *pRefCount );
		};
	
		// Destrutor
		virtual ~RefCounter( void ){ clean(); };

		// Operadores de referência, necessários para que RefCounter funcione como um
		// ponteiro comum
		T* operator->() const { return pRef; };
		T& operator*() const { return *pRef; };
	
		// Operador de atribuição
		RefCounter& operator=( const RefCounter& r )
		{
			// Evita auto atribuição
			if( pRef == r.pRef )
				return *this;

			// Este RefCounter pára de referenciar o objeto antigo
			clean();

			// E começa a referenciar o novo objeto
			pRef = r.pRef;
			pRefCount = r.pRefCount;

			if( pRefCount )
				++( *pRefCount );

			return *this;
		};
	
		// Operador de conversão para bool
		operator bool() const { return pRef != NULL; };
	
		// Retorna quantas referências este objeto possui
		uint16 getRefCount( void ) const { return pRefCount == NULL ? 0 : *pRefCount; };
	
		// Retorna o objeto refenciado
		T* getRef( void ){ return pRef; };
	
		// Necessário para garantirmos os relacionamentos de herança entre templates
		// de classes relacionadas
		template< class D >	operator RefCounter<D>(){ return RefCounter<D>( pRef ); };

	private:
		// Libera os recursos alocados pelo objeto
		void clean( void )
		{
			if(( pRefCount != NULL ) && ( --( *pRefCount ) <= 0 ))
			{
				DELETE( pRef );
				DELETE( pRefCount );
			}
		};
	
		// Objeto cujo número de referências estamos controlando
		T* pRef;
	
		// Contagem de referências para o objeto
		int16* pRefCount;
};

// Operadores de igualdade. Não podem ficar dentro da classe por questões de sintaxe de templates
template<class T, class U> inline bool operator==(RefCounter<T> const & a, RefCounter<U> const & b)
{
    return a.getRef() == b.getRef();
};

template<class T, class U> inline bool operator!=(RefCounter<T> const & a, RefCounter<U> const & b)
{
    return a.getRef() != b.getRef();
};

template<class T, class U> inline bool operator==(RefCounter<T> const & a, U* b)
{
    return a.getRef() == b;
};

template<class T, class U> inline bool operator!=(RefCounter<T> const & a, U* b)
{
    return a.getRef() != b;
};

template<class T, class U> inline bool operator==(T* a, RefCounter<U> const & b)
{
    return a == b.getRef();
};

template<class T, class U> inline bool operator!=(T* a, RefCounter<U> const & b)
{
    return a != b.getRef();
};

#endif
