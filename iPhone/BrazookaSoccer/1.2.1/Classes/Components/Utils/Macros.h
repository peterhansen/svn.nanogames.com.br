/*
 *  Macros.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/23/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MACROS_H
#define MACROS_H 1

// Components
#include "Utils.h"

// Para utilizarmos em métodos OpenGL nos quais 0 representa o cancelamento da operação, como
// em glBindTexture( GL_TEXTURE_2D, 0 )
#ifndef GL_NONE
	#define GL_NONE 0
#endif

// Deleta um ponteiro e coloca seu valor como NULL
#define DELETE( p ) { delete p; p = NULL; }
#define DELETE_VEC( p ) { delete[] p; p = NULL; }

// Testa se um ponteiro é nulo antes de deletá-lo
#define SAFE_DELETE( p ) { if( p ) DELETE( p ) }
#define SAFE_DELETE_VEC( p ) { if( p ) DELETE_VEC( p ) }

// Funções de log para as versões debug
#if DEBUG
	#define LOG( s, ... ) Utils::Log( s, ##__VA_ARGS__ )
	#define GLLOG() Utils::glLog( __FILE__, __LINE__ )

	#define LOG_FREE_MEM( string ) LOG( "Free Mem %s: %.4f\n", string, Utils::GetFreeMemory() );
#endif

// Extensão de assert: permite acrescentarmos uma string que descreve o erro
#define assert_n_log( e, str, ... )				\
		if( ( e ) == 0 )						\
		{										\
			LOG( str, ##__VA_ARGS__ );			\
			LOG( "\n" );						\
			__assert( #e, __FILE__, __LINE__ );	\
		}								

#endif // #ifndef MACROS_H
