/*
 *  DevilBg.h
 *  dotGame
 *
 *  Created by Max on 2/11/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */


#ifndef DEVIL_BG_H
#define DEVIL_BG_H 1

#include "Board.h"


class DevilBg : public Board {
	
public:
	DevilBg( GameBaseInfo* g );
	
	~DevilBg(){};
	
protected:	
	void generateBackground( void ) ;
	
	void configureMarks( void ) ;
	
	void configureLinesBkgd( void );
	
	
};


#endif