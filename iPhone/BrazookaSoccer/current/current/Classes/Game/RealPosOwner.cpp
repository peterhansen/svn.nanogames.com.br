#include "RealPosOwner.h"

/*===============================================================================

DESTRUTOR

=============================================================================== */

RealPosOwner::~RealPosOwner( void )
{
}

/*===============================================================================

MÉTODO getRealPosition
   Retorna a posição do objeto no mundo real.

=============================================================================== */

const Point3f* RealPosOwner::getRealPosition( void ) const
{
	return &realPosition;
}

/*===============================================================================

MÉTODO setRealPosition
   Define a posição do objeto no mundo real.

================================================================================*/

void RealPosOwner::setRealPosition( float x, float y, float z )
{
	realPosition.set( x, y, z );
	updatePosition();
}

/*====================================================================================

MÉTODO QSortRealPosComparer
	Para utilização com qsort.
 
	-1 => Deve ser renderizado mais abaixo
	 1 => Deve ser renderizado mais acima
	 0 => Mesma posição... Do jeito que está vai causar flickering

=======================================================================================*/

#warning Este método está errado!!! o primeiro if deve retornar 1 e o último return -1! Mas aí teríamos que rever também GameScreen::updateZOrder()

int RealPosOwner::QSortRealPosComparer( const void* pA, const void* pB )
{
	const RealPosOwner* pPlayerA = static_cast< const RealPosOwner* >( pA );
	const RealPosOwner* pPlayerB = static_cast< const RealPosOwner* >( pB );

	if( pPlayerA->getRealPosition()->z < pPlayerB->getRealPosition()->z )
		return -1;

	if( pPlayerA->getRealPosition()->z == pPlayerB->getRealPosition()->z )
	{
		if( pPlayerA->getRealPosition()->x < pPlayerB->getRealPosition()->x )
		{
			return -1;
		}
		else if( pPlayerA->getRealPosition()->x > pPlayerB->getRealPosition()->x )
		{
			return 1;
		}
		else
		{
			if( pPlayerA->getRealPosition()->y < pPlayerB->getRealPosition()->y )
			{
				return -1;
			}
			else if( pPlayerA->getRealPosition()->y > pPlayerB->getRealPosition()->y )
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}

	return 1;
}

/*====================================================================================

MÉTODO StdSortRealPosComparer
	Para utilização com std::sort.

=======================================================================================*/

bool RealPosOwner::StdSortRealPosComparer( const RealPosOwner* pA, const RealPosOwner* pB )
{
	return QSortRealPosComparer( pA, pB ) == -1;
}
