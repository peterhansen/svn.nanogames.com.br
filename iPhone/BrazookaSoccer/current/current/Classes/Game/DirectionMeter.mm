#include "DirectionMeter.h"

#include "Exceptions.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"
#include "RenderableImage.h"

#include "InterfaceControlListener.h"

#include "FreeKickAppDelegate.h"

// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
		if( !pObject || !pGroup->insertObject( pObject ) )	\
		{													\
			delete pObject;									\
			goto Error;										\
		}

// Define o número de objetos no grupo
#define DIR_METER_N_OBJS 4

// Índices dos objetos no grupo
#define DIR_METER_OBJ_INDEX_BACK	0
#define DIR_METER_OBJ_INDEX_METER	1
#define DIR_METER_OBJ_INDEX_TAG		2
#define DIR_METER_OBJ_INDEX_FRONT	3

// Configurações da imagem de fundo
#define DIR_METER_BACK_IMG_FRAME_WIDTH 139.0f 
#define DIR_METER_BACK_IMG_FRAME_HEIGHT 50.0f

// Configurações da imagem da borda
#define DIR_METER_METER_IMG_FRAME_WIDTH 114.0f
#define DIR_METER_METER_IMG_FRAME_HEIGHT 27.0f
#define DIR_METER_METER_DIST_FROM_FRONT_IMG_X 12.0f
#define DIR_METER_METER_DIST_FROM_FRONT_IMG_Y  6.0f

// Configurações da imagem da aba
#define DIR_METER_TAG_IMG_FRAME_WIDTH 91.0f
#define DIR_METER_TAG_IMG_FRAME_HEIGHT 29.0f

// Configurações da imagem da borda
#define DIR_METER_FRONT_IMG_FRAME_WIDTH 130.0f
#define DIR_METER_FRONT_IMG_FRAME_HEIGHT 38.0f

// Distância que devemos posicionar o topo da imagem do fundo em relação ao topo da imagem da tag
#define DIR_METER_BACK_OFFSET_FROM_TAG_TOP 12.0f

// Configurações das animações
#define DIR_METER_ANIM_MOVE_DUR 0.100f
#define DIR_METER_ANIM_TAG_DUR	0.200f

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

DirectionMeter::DirectionMeter( float minValue, float maxValue, float initValue, InterfaceControlListener* pControlListener )
				:	ObjectGroup( DIR_METER_N_OBJS ), InterfaceControl( pControlListener ),
					MeterBar( minValue, maxValue, initValue ), animFinalY( 0.0f ), soundFlag( false )
{
	build();
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

DirectionMeter::~DirectionMeter( void )
{
}

/*===========================================================================================
 
MÉTODO setCurrValuePercent
	Determina o valor atual do medidor (porcentagem).

============================================================================================*/

void DirectionMeter::setCurrValuePercent( float percent )
{
	MeterBar::setCurrValuePercent( percent );
	
	Point3f frontScreenPos = *( getPosition() ) + *( getObject( DIR_METER_OBJ_INDEX_FRONT )->getPosition() );
	float startX = frontScreenPos.x + DIR_METER_METER_DIST_FROM_FRONT_IMG_X;

	float nextWidth;
	percent = NanoMath::clamp( percent, 0.0f, 1.0f );
	if( percent < 0.5f )
	{
		float aux = ( 0.5f - percent ) * DIR_METER_METER_IMG_FRAME_WIDTH;
		startX += ( DIR_METER_METER_IMG_FRAME_WIDTH * 0.5f ) - aux;
		nextWidth = aux;
	}
	else if( percent > 0.5f )
	{
		startX += ( DIR_METER_METER_IMG_FRAME_WIDTH * 0.5f );
		nextWidth = ( percent - 0.5f ) * DIR_METER_METER_IMG_FRAME_WIDTH;
	}
	else
	{
		// OBS: Tanto faz, já que nextWidth será 0.0f
		//startX += ( DIR_METER_METER_IMG_FRAME_WIDTH * 0.5f );

		nextWidth = 0.0f;
	}
	
	Viewport viewport( startX, frontScreenPos.y + DIR_METER_METER_DIST_FROM_FRONT_IMG_Y, nextWidth, DIR_METER_METER_IMG_FRAME_HEIGHT );
	getObject( DIR_METER_OBJ_INDEX_METER )->setViewport( viewport );
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/

void DirectionMeter::build( void )
{
	{  // Evita erros de compilação por causa dos gotos

		// Aloca as imagens
		char path[] = { 'c', 't', 'd', ' ', '\0' };
		
		path[3] = 'b';

		#if CONFIG_TEXTURES_16_BIT
			RenderableImage* pBack = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pBack );
			
			path[3] = 'm';
			RenderableImage* pMeter = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pMeter );
			
			path[3] = 't';
			RenderableImage* pTag = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pTag );
			
			path[3] = 'f';
			RenderableImage* pFront = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pFront );
		#else
			RenderableImage* pBack = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pBack );
			
			path[3] = 'm';
			RenderableImage* pMeter = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pMeter );
			
			path[3] = 't';
			RenderableImage* pTag = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pTag );
			
			path[3] = 'f';
			RenderableImage* pFront = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pFront );
		#endif
		
		// Configura as imagens
		TextureFrame frame( 0.0f, 0.0f, DIR_METER_BACK_IMG_FRAME_WIDTH, DIR_METER_BACK_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pBack->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, DIR_METER_METER_IMG_FRAME_WIDTH, DIR_METER_METER_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pMeter->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, DIR_METER_TAG_IMG_FRAME_WIDTH, DIR_METER_TAG_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pTag->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, DIR_METER_FRONT_IMG_FRAME_WIDTH, DIR_METER_FRONT_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pFront->setImageFrame( frame );
		
		// Determina o tamanho do grupo
		setSize( DIR_METER_BACK_IMG_FRAME_WIDTH, DIR_METER_BACK_IMG_FRAME_HEIGHT + DIR_METER_BACK_OFFSET_FROM_TAG_TOP );
		
		// Posiciona as imagens no grupo
		pTag->setPosition( getWidth() - pTag->getWidth() - 10.0f, 0.0f );
		pBack->setPosition( 0.0f, pTag->getPosition()->y + DIR_METER_BACK_OFFSET_FROM_TAG_TOP );
		pFront->setPosition( pBack->getPosition()->x + 5.0f, pBack->getPosition()->y + 7.0f );
		pMeter->setPosition( pFront->getPosition()->x + DIR_METER_METER_DIST_FROM_FRONT_IMG_X, pFront->getPosition()->y + DIR_METER_METER_DIST_FROM_FRONT_IMG_Y );

		return;
	
	}  // Evita erros de compilação por causa dos gotos

	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "DirectionMeter::build: Unable to create object" );
	#else
		throw ConstructorException( "" );
	#endif
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool DirectionMeter::update( float timeElapsed )
{
	if( !ObjectGroup::update( timeElapsed ) )
		return false;

	if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWING )
	{
		float currY = getPosition()->y;
		if( currY != animFinalY )
		{
			float nextY = currY - ( ( timeElapsed * ( SCREEN_HEIGHT - animFinalY ) ) / DIR_METER_ANIM_MOVE_DUR );
			
			if( ( nextY < SCREEN_HEIGHT ) && !soundFlag )
			{
				soundFlag = true;
				[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed:SOUND_NAME_TAG_INCOMING AtIndex:SOUND_INDEX_TAG_1_INCOMING Looping: false];
			}

			
			if( nextY < animFinalY )
				nextY = animFinalY;

			setPosition( getPosition()->x, nextY );
		}
		else
		{
			Object *pTag = getObject( DIR_METER_OBJ_INDEX_TAG );

			float nextTagY = pTag->getPosition()->y - ( ( timeElapsed * pTag->getHeight() ) / DIR_METER_ANIM_TAG_DUR );
	
			if( nextTagY < 0.0f )
			{
				nextTagY = 0.0f;
				setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
			}
			
			pTag->setPosition( pTag->getPosition()->x, nextTagY );
		}
	}
	else if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDING )
	{
		float nextY = getPosition()->y + ( ( timeElapsed * ( SCREEN_HEIGHT - animFinalY ) ) / DIR_METER_ANIM_MOVE_DUR );
		
		if( nextY > SCREEN_HEIGHT )
		{
			nextY = SCREEN_HEIGHT;
			setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
		}

		setPosition( getPosition()->x, nextY );
	}
	
	return true;
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exebição do controle.

============================================================================================*/

void DirectionMeter::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		setActive( true );
		setVisible( true );
	}
	else
	{
		Object *pTag = getObject( DIR_METER_OBJ_INDEX_TAG );
		pTag->setPosition( pTag->getPosition()->x, pTag->getHeight() );

		setPosition( getPosition()->x, SCREEN_HEIGHT );

		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}

/*===========================================================================================
 
MÉTODO configAnim
	Configura os parâmetros das animações.

============================================================================================*/

void DirectionMeter::configAnim( float finalY )
{
	animFinalY = finalY;
	
	Object *pTag = getObject( DIR_METER_OBJ_INDEX_TAG );
	pTag->setViewport( 0.0f, 0.0f, SCREEN_WIDTH, animFinalY + pTag->getHeight() );
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/

void DirectionMeter::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
	{
		soundFlag = false;
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
	}
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/

void DirectionMeter::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}

/*===========================================================================================
 
MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

bool DirectionMeter::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
		case EVENT_TOUCH_MOVED:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				// Obtém a informação do toque
				const NSArray* pTouchesArray = ( NSArray* )pParam;
				const UITouch* pTouch = [pTouchesArray objectAtIndex: 0];
				CGPoint temp = [pTouch locationInView : NULL];
				
				Point3f touchPos( temp.x, temp.y );
				touchPos = Utils::MapPointByOrientation( &touchPos );
				touchPos.z = 0.0f;

				if( Utils::IsPointInsideRect( &touchPos, this ) )
					setCurrValuePercent( NanoMath::clamp( ( touchPos.x - getPosition()->x ) / getWidth(), 0.0f, 1.0f ) );

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			return true;

		case EVENT_TOUCH_ENDED:
		case EVENT_TOUCH_CANCELED:
		default:
			return false;
	}
}

#endif
