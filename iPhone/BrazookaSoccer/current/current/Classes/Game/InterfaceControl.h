/*
 *  InterfaceControl.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef INTERFACE_CONTROL_H
#define INTERFACE_CONTROL_H 1

// Forward Declarations
class InterfaceControlListener;

// Possíveis estados de exebição do controle
enum InterfaceControlState
{
	INTERFACE_CONTROL_STATE_UNDEFINED = -1,
	INTERFACE_CONTROL_STATE_HIDDEN = 0,
	INTERFACE_CONTROL_STATE_SHOWN = 1,
	INTERFACE_CONTROL_STATE_SHOWING = 2,
	INTERFACE_CONTROL_STATE_HIDING = 3
};

class InterfaceControl
{
	public:
		// Destrutor
		virtual ~InterfaceControl( void );
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void ){};

		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void ){};
	
		// Retorna o estado de exebição do controle
		InterfaceControlState getInterfaceControlState( void ) const;
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
	protected:
		// Construtor
		InterfaceControl( InterfaceControlListener* pListener );
	
	private:
		// Listener dos eventos deste objeto
		InterfaceControlListener* pListener;
	
		// Estado de exebição do controle
		InterfaceControlState interfaceControlState;
};

// Implementação dos métodos inline

// Retorna o estado de exebição do controle
inline InterfaceControlState InterfaceControl::getInterfaceControlState( void ) const
{
	return interfaceControlState;
}

#endif
