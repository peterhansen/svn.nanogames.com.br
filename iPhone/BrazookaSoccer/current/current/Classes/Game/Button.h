/*
 *  Button.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/12/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BUTTON_H
#define BUTTON_H 1

#include "RenderableImage.h"
#include "InterfaceControl.h"
#include "GameUtils.h"

// Forward Declarations
class InterfaceControlListener;

class Button : public RenderableImage, public InterfaceControl
{
	public:
	
#if DEBUG && TEST_BUTTONS	
	int mywidth;
	int myheight;
	int myx;
	int myy;
#endif
	
		// Construtor
		Button( const char* pPath, InterfaceControlListener* pListener );
	
		// Destrutor
		virtual ~Button( void );
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
#if DEBUG && TEST_BUTTONS
		virtual bool render(void)
		{			
			if (RenderableImage::render())			
			{
				// Renderiza o quad de colisão
				glDisable( GL_BLEND );
				glDisable( GL_TEXTURE_2D );
				
				glEnableClientState( GL_VERTEX_ARRAY );
				glLineWidth( 4.0f );
				glPointSize( 8.0f );
				
				glMatrixMode( GL_MODELVIEW );
				glPushMatrix();
				
				Point3f pos = *getPosition();
				
				if (myx!=0)
					pos.x=myx;
				if (myy!=0)
					pos.y=myy;
				
				Point3f dx = Point3f(mywidth*scale.x ,0,0);
				Point3f dy = Point3f(0,myheight*scale.y,0);
				
				dy = -dy;
				
				Point3f top[ 2 ] = {
					pos,
					pos - dy,
				};
				
				glColor4ub( 255, 0, 0, 255 );
				glVertexPointer( 3, GL_FLOAT, 0, top );
				glDrawArrays( GL_LINES, 0, 2 );
				
				Point3f right[ 2 ] = {
					pos,
					pos + dx,
				};
				
				glColor4ub( 0, 255, 0, 255 );
				glVertexPointer( 3, GL_FLOAT, 0, right );
				glDrawArrays( GL_LINES, 0, 2 );
				
				Point3f others[ 4 ] = {
					pos - dy,
					pos - dy + dx,
					pos - dy + dx,
					pos + dx
				};
				
				glColor4ub( 255, 255, 0, 255 );
				glVertexPointer( 3, GL_FLOAT, 0, others );
				glDrawArrays( GL_LINES, 0, 4 );
				
				Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
				if( 1.0f > 0.0f )
					glColor4ub( 179, 179, 179, 255 );
				else
					glColor4ub( 255, 183, 15, 255 );
				
				glVertexPointer( 3, GL_FLOAT, 0, &nomal );
				glDrawArrays( GL_POINTS, 0, 1 );
				

				glPopMatrix();
				glLineWidth( 1.0f );
				glPointSize( 1.0f );
				
				glColor4ub( 255, 255, 255, 255 );
				
				return true;
			}
			return false;
			
		}
#endif
};

#endif
