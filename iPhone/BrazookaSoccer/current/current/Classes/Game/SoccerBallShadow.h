/*
 *  SoccerBallShadow.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 7/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SOCCER_BALL_SHADOW_H
#define SOCCER_BALL_SHADOW_H 1

#include "Object.h"
#include "RealPosOwner.h"

// Declarações adiadas
class SoccerBall;

class SoccerBallShadow : public Object, public RealPosOwner
{
	public:
		// Construtor
		SoccerBallShadow( const SoccerBall* pBall );
	
		// Destrutor
		virtual ~SoccerBallShadow( void );
	
		// Renderiza o objeto
		virtual bool render( void );

	private:
		// Atualiza a posição do objeto na tela
		virtual void updatePosition( void );
	
		// Bola para a qual iremos criar a sombra
		const SoccerBall* pBall;
};

#endif
