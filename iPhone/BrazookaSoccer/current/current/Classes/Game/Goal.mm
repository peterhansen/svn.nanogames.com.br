#include "Goal.h"

#include "GameUtils.h"

// OLD
//// Offset da imagem inferior do gol
//#define GOAL_BOTTOM_OFFSET_X    0.0f
//#define GOAL_BOTTOM_OFFSET_Y -230.0f
//
//// Offset da imagem superior do gol em relação à imagem inferior
//#define GOAL_TOP_OFFSET_X ( GOAL_BOTTOM_OFFSET_X - 357.0f )
//#define GOAL_TOP_OFFSET_Y ( GOAL_BOTTOM_OFFSET_Y - 206.0f )
//
//// Tamanho original do gol
//#define GOAL_WIDTH_BOTTOM 478.0f
//#define GOAL_HEIGHT_BOTTOM 381.0f
//
//#define GOAL_WIDTH_TOP 469.0f
//#define GOAL_HEIGHT_TOP 430.0f

// Offset da imagem inferior do gol
#define GOAL_BOTTOM_OFFSET_X 0.0f
#define GOAL_BOTTOM_OFFSET_Y 0.0f

#define GOAL_BOTTOM_ANCHOR_X	  3.0f
#define GOAL_BOTTOM_ANCHOR_Y	113.0f

#define GOAL_BOTTOM_IMG_WIDTH	239.0f
#define GOAL_BOTTOM_IMG_HEIGHT	191.0f

#define GOAL_BOTTOM_SCALE_X	( 1.0f + ( ( 50.00f * 2.0f ) / 100.0f ) )
#define GOAL_BOTTOM_SCALE_Y	( 1.0f + ( ( 50.13f * 2.0f ) / 100.0f ) )

// Offset da imagem superior do gol em relação à imagem inferior
#define GOAL_TOP_OFFSET_X ( GOAL_BOTTOM_OFFSET_X + 5.0f )
#define GOAL_TOP_OFFSET_Y ( GOAL_BOTTOM_OFFSET_Y - 1.0f )

#define GOAL_TOP_ANCHOR_X	0.0f
#define GOAL_TOP_ANCHOR_Y	0.0f

#define GOAL_TOP_IMG_WIDTH	235.0f
#define GOAL_TOP_IMG_HEIGHT	215.0f

#define GOAL_TOP_SCALE_X	( 1.0f + ( ( 50.11f * 2.0f ) / 100.0f ) )
#define GOAL_TOP_SCALE_Y	( 1.0f + ( ( 50.00f * 2.0f ) / 100.0f ) )

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/
	
Goal::Goal( Goal* pGoalBottom )
	 : RealPosOwner(),

// OLD
//#if CONFIG_TEXTURES_16_BIT
//	   RenderableImage( bottomPart ? "goalBottom_" : "goalTop_", RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D ),
//#else
//	   RenderableImage( bottomPart ? "goalBottom_" : "goalTop_" ),
//#endif

#if CONFIG_TEXTURES_16_BIT
	   RenderableImage( pGoalBottom == NULL ? "goalBottom" : "goalTop", RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D ),
#else
	   RenderableImage( pGoalBottom == NULL ? "goalBottom" : "goalTop" ),
#endif
	   pGoalBottom( pGoalBottom ),
	   offsetX( 0.0f ),
	   offsetY( 0.0f )
{
	if( pGoalBottom == NULL )
	{
		// OLD
//		setSize( GOAL_WIDTH_BOTTOM, GOAL_HEIGHT_BOTTOM );
		
		setImageFrame( TextureFrame( 0.0f, 0.0f, GOAL_BOTTOM_IMG_WIDTH, GOAL_BOTTOM_IMG_HEIGHT, 0.0f, 0.0f ) );
		setScale( GOAL_BOTTOM_SCALE_X, GOAL_BOTTOM_SCALE_Y );

		setAnchor( GOAL_BOTTOM_ANCHOR_X * GOAL_BOTTOM_SCALE_X, GOAL_BOTTOM_ANCHOR_Y * GOAL_BOTTOM_SCALE_Y, 0.0f );
		offsetX = GOAL_BOTTOM_OFFSET_X * GOAL_BOTTOM_SCALE_X;
		offsetY = GOAL_BOTTOM_OFFSET_Y * GOAL_BOTTOM_SCALE_Y;
	}
	else
	{
		// OLD
//		setSize( GOAL_WIDTH_TOP, GOAL_HEIGHT_TOP );

		setImageFrame( TextureFrame( 0.0f, 0.0f, GOAL_TOP_IMG_WIDTH, GOAL_TOP_IMG_HEIGHT, 0.0f, 0.0f ) );
		setScale( GOAL_TOP_SCALE_X, GOAL_TOP_SCALE_Y );

		setAnchor( GOAL_TOP_ANCHOR_X * GOAL_TOP_SCALE_X, GOAL_TOP_ANCHOR_Y * GOAL_TOP_SCALE_Y, 0.0f );
		offsetX = GOAL_TOP_OFFSET_X * GOAL_TOP_SCALE_X;
		offsetY = GOAL_TOP_OFFSET_Y * GOAL_TOP_SCALE_Y;
	}
}

/*============================================================================================

DESTRUTOR

=============================================================================================*/

Goal::~Goal( void )
{	
	pGoalBottom = NULL;
}

/*============================================================================================

MÉTODO updatePosition
	Atualiza a posição do objeto na tela.

=============================================================================================*/

void Goal::updatePosition( void )
{
	Point3f goalPos = GameUtils::isoGlobalToPixels( realPosition );

	// OLD
//	setPosition( goalPos.x + offsetX, goalPos.y + offsetY );
	
	if( pGoalBottom == NULL )
	{
		setPositionByAnchor( goalPos.x + offsetX, goalPos.y + offsetY, 0.0f );
	}
	else
	{
		Point3f bottomPos = *( pGoalBottom->getPosition() );
		setPosition( bottomPos.x + offsetX, bottomPos.y + offsetY, bottomPos.z );
	}
}
