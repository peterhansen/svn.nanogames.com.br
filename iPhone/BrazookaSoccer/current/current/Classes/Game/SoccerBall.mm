#include "SoccerBall.h"

#include "Exceptions.h"
#include "Sphere.h"

#if DEBUG
	#include "Tests.h"
#endif

// Nível de detalhamento da esfera que utilizaremos para renderizar a bola
#define SOCCER_BALL_SPHERE_LOD 30

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

SoccerBall::SoccerBall( const RealPosOwner* pParent )
		#if CONFIG_TEXTURES_16_BIT
			: RenderableImage( "b", &( SoccerBall::CreateVertexSet ), ResourceManager::Get16BitTexture2D ),
		#else
			: RenderableImage( "b", &( SoccerBall::CreateVertexSet ) ),
		#endif
			 pParent( pParent ),
			 nTrailPos( 0 ),
			 insertIndex( 0 )
{
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

SoccerBall::~SoccerBall( void )
{
	pParent = NULL;
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/

bool SoccerBall::render( void )
{
	if( !Object::render() )
		return false;
	
	glEnable( GL_DEPTH_TEST );
	
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	glEnable( GL_BLEND );
	glBlendFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
	
	// Carrega a textura
	GLboolean tex2DWasEnabled = glIsEnabled( GL_TEXTURE_2D );
	glEnable( GL_TEXTURE_2D );
	pImg->load();
	
	// Determina o modo de texturização
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	
	// Determina o shade model
	GLint lastShadeModel;
	glGetIntegerv( GL_SHADE_MODEL, &lastShadeModel );
	glShadeModel( GL_FLAT );

	Matrix4x4 aux;
	rotation.getTranspose( &aux );
	
	glColor4ub( 255, 255, 255, 255 );
	glMatrixMode( GL_MODELVIEW );
	
	float width = size.x * scale.x;
	float height = size.y * scale.y;
	float depth = size.z * scale.z;

	// TODOO : Implementar um rastro no update
//	for()
//	{
		glPushMatrix();
		
		// Posiciona o objeto
		glTranslatef( position.x + ( width * 0.5f ), position.y + ( height * 0.5f ), position.z );

		// Redimensiona o objeto
		glScalef( width, height, depth );
	
		// Rotaciona o objeto
		glMultMatrixf( aux );

		// Renderiza a bola
		pVertexSet->render();
		
		glPopMatrix();
//	}

	// Reseta os estados do OpenGL
	pImg->unload();

	if( tex2DWasEnabled == GL_FALSE )
		glDisable( GL_TEXTURE_2D );

	if( blendWasEnabled == GL_FALSE )
		glDisable( GL_BLEND );
	
	// Reseta o shade model
	glShadeModel( lastShadeModel );
	
	glDisable( GL_DEPTH_TEST );

	return true;
}

/*===========================================================================================
 
MÉTODO CreateVertexSet
	Cria o conjunto de vértices que será utilizado para renderizar o objeto.

============================================================================================*/

VertexSetHandler SoccerBall::CreateVertexSet( void )
{
	Sphere* pSphere = new Sphere( SOCCER_BALL_SPHERE_LOD );
	if( !pSphere )
#if TARGET_IPHONE_SIMULATOR
		throw ConstructorException( "SoccerBall::CreateVertexSet( void ): Unable to create vertex set" );
#else
		throw ConstructorException( "" );
#endif

	pSphere->disableRenderStates( RENDER_STATE_COLOR_ARRAY | RENDER_STATE_NORMAL_ARRAY );
	return VertexSetHandler( pSphere );
}

/*===========================================================================================
 
MÉTODO insertTrailPos
	Acrescenta uma posição ao vetor de rastro.

============================================================================================*/

void SoccerBall::insertTrailPos( Point3f& pos )
{
	trailPos[ insertIndex ] = pos;
	insertIndex = ( insertIndex + 1 ) % SOCCER_BALL_MAX_TRAIL_LEN;
	++nTrailPos;
	if( nTrailPos > SOCCER_BALL_MAX_TRAIL_LEN )
		nTrailPos = SOCCER_BALL_MAX_TRAIL_LEN;
}

/*===========================================================================================
 
MÉTODO cleanTrail
	Limpa o rastro.

============================================================================================*/

void SoccerBall::cleanTrail( void )
{
	nTrailPos = 0;
	insertIndex = 0;
}
