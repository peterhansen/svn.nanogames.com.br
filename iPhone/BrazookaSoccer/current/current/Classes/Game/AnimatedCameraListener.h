/*
 *  AnimatedCameraListener.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/6/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ANIMATED_CAMERA_LISTENER_H
#define ANIMATED_CAMERA_LISTENER_H

class AnimatedCameraListener
{
	public:
		// Destrutor
		virtual ~AnimatedCameraListener( void ){};

		// Indica que a animação da câmera terminou
		virtual void onCameraAnimEnded( void ) = 0;
};

#endif
