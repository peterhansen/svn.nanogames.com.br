/*
 *  AnimatedCamera.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/6/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ANIMATED_CAMERA_H
#define ANIMATED_CAMERA_H 1

#include "OrthoCamera.h"
#include "Updatable.h"

#include "AnimatedCameraListener.h"

class AnimatedCamera : public OrthoCamera, public Updatable
{
	public:
		// Construtor
		AnimatedCamera( AnimatedCameraListener* pListener = NULL, CameraType cameraType = CAMERA_TYPE_AIR, float zNear = -1.0f, float zFar = 1.0f );
	
		// Destrutor
		virtual ~AnimatedCamera( void );
	
		// Determina uma animação de movimenta da câmera, de sua posição atual até a posição
		// de destino, que será executada intervalo de tempo 'time'
		void setAnim( const Point3f* pDestPos, float finalZoom, float time );
	
		// Inicia a animação configurada por setAnim
		void startAnim( void );
	
		// Termina a animação de movimentação da câmera. Se a animação ainda não estiver
		// completa, a câmera é levada diretamente para a sua posição final
		void endAnim( void );
	
		// Cancela a animação, deixando a câmera em seu estado atual
		void cancelAnim( void );
	
		// Altera o destino da animação atual sem iniciar uma nova animação
		void changeCurrAnimDest( const Point3f* pDestPos );
	
		// Indica se a câmera está no meio de uma animação
		inline bool isAnimating( void ) const { return animating; };
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Obtém a matriz de projeção da câmera
		virtual Matrix4x4* getProjectionMatrix( Matrix4x4* pOut ) const;

	private:
		// Objeto que irá receber informações sobre a animação da câmera
		AnimatedCameraListener* pListener;
	
		// Controladores da animação de movimentação da câmera
		bool animating;
		float animTimeCounter, animTime;
	
		float animDestZoom;
		float zoomSpeed;
	
		Point3f animDestPos;
		Point3f animSpeed;
};

#endif
