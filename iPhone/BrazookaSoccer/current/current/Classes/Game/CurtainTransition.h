/*
 *  CurtainTransition.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CURTAIN_TRANSITION_H
#define CURTAIN_TRANSITION_H 1

#include "Color.h"
#include "OGLTransition.h"

class CurtainTransition : public OGLTransition
{
	public:
		// Construtor
		CurtainTransition( float duration, OGLTransitionListener* pListener, const Color& color = Color( COLOR_BLACK ) );

		// Destrutor
		virtual ~CurtainTransition( void );

		// Renderiza o objeto
		virtual bool render( void );

		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Reinicializa o objeto
		virtual void reset( void );

	private:
		// Indica se a transição terminou
		bool ended;
	
		// Controladores da transição
		float heightCounter;
	
		// Velocidade com a qual a cortina cresce
		float transitionSpeed;

		// Cor da cortina que irá cobrir a tela
		Color color;
};

#endif
