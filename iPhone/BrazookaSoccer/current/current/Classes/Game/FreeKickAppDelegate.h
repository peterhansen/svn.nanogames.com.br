//
//  FreeKickAppDelegate.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#ifndef GAME_APP_DELEGATE_H 
#define GAME_APP_DELEGATE_H 1

#include "ApplicationManager.h"
#include "Config.h"

#include "EAGLView.h"
#include "Iso_Defines.h"
#include "GameInfo.h"
#include "MainMenuView.h"
#include "OptionsView.h"
#include "SplashNano.h"

// OLD
//#include "SplashGame.h"
#include "MoviePlayerView.h"

// Forward Declarations
class Font;

@interface FreeKickAppDelegate : ApplicationManager
{
	@private
		// View do splash da nano
		SplashNano* hSplashNano;
	
		// View do splash do jogo
		// OLD SplashGame* hSplashGame;
		MoviePlayerView *hSplashGame;
	
		// Menu Principal. Deixamos pré carregado apenas no início do jogo. Assim impedimos a engasgada na transição
		// do vídeo de abertura para o menu
		MainMenuView* hMainMenu;
	
		// View do jogo. Contém NULL caso estejamos em uma tela diferente da tela de pause e, obviamente, da própria tela de jogo 
		EAGLView* hGLView;

		/************* DADOS DA APLICAÇÃO *************/
	
		///<DM> informações para salvamento de jogo
		bool bLoadGameState;
		///</DM>
	
		// Idioma atual do jogo
		uint8 currLanguage;
	
		// Indica que recorde devemos piscar na tela de ranking
		int8 highScoreIndex;
	
		// Informações do jogo atual
		GameInfo gameInfo;
	
		// Sons do jogo
		#if ( APP_N_SOUNDS > 0 ) && SOUND_WITH_OPEN_AL
			AudioSource* appSounds[ APP_N_SOUNDS ];
		#endif

		// Textos do jogo
		#if ( APP_N_LANGUAGES > 0 ) && ( APP_N_TEXTS > 0 )
			char* texts[ APP_N_LANGUAGES ][ APP_N_TEXTS ];
		#endif

		// Fontes da aplicação
		#if APP_N_FONTS > 0
			Font* appFonts[ APP_N_FONTS ];
		#endif

		// Recordes salvos
		#if APP_N_RECORDS > 0
			uint32 records[ APP_N_RECORDS ];
		#endif
}

// Gera os getters e setters para as propriedades a seguir
@property ( nonatomic, readonly ) EAGLView* hGLView;
@property ( nonatomic, setter = setLanguage, getter = getLanguage ) uint8 currLanguage;

// Retorna um texto da aplicação
- ( const char* ) getText: ( uint16 ) textIndex;
	
// Retorna uma fonte da aplicação
- ( Font* ) getFont:( uint8 )fontIndex;

// Retorna a fonte embutida que mais se aproxima da fonte utilizada na aplicação
- ( UIFont* ) getiPhoneFont;

// Inicia a reprodução de um som da aplicação
- ( void ) playAudioNamed:( uint8 )audioName AtIndex:( uint8 ) audioIndex Looping:( bool )looping;

// Pára de reproduzir todos os sons
- ( void ) stopAudio;

// Pára de reproduzir o som indicado
- ( void ) stopAudioAtIndex: ( uint8 )index;

// (Des)Pausa todos os sons que estão sendo executados no momento
- ( void ) pauseAllSounds: ( bool )pause;

// Indica se está tocando um som específico
- ( bool )isPlayingAudioWithIndex:( uint8 )index;

// Indica se a aplicação está tocando algum som
- ( bool ) isPlayingAudio;

// Retorna o percentual do volume máximo utilizado na reprodução de sons
- ( float ) getAudioVolume;

// Determina o volume dos sons da aplicação
- ( void ) setVolume:( float )volume;

// Exibe a tela de fim de jogo
- ( void ) onGameOver: ( int32 )score;

// Retorna o lugar da pontuação na tabela de recordes ou um número negativo caso a jogador
// não tenha alcançado um valor que supere o menor recorde
// OLD: Agora só utilizamos setHighScore
//- ( int8 ) isHighScore: ( int32 )score;

// Armazena a pontuação caso esta seja um recorde
- ( int8 ) setHighScore: ( int32 )score;

// Salva a pontuação atual caso esta seja um recorde
- ( bool ) saveScoreIfRecord: ( int32 )score;

///<DM>

// Salva o estado do jogo
- ( bool ) saveGameState;

// Apaga o arquivo que contém o estado do jogo salvo
-( void ) removeStateFile;

// Retorna o nome do arquivo onde o estado do jogo está salvo
-( NSString* )getStateFileName;

-( void )scheduleLoadState;

///</DM>

#if APP_N_SOUNDS > 0

	// Retira da memória os sons já carregados
	-( void ) unloadSounds;
	///<DM>
	-( void ) unloadSoundAtIndex: (int)index;
	///</DM>
#endif

@end

#endif


