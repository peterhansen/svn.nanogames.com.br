#import "ConfirmLoadView.h"
#import "DeviceInterface.h"
// Components
#include "ObjcMacros.h"
#import "FreeKickAppDelegate.h"
// Definições auxiliares
#define CONFIRM_LOAD_VIEW_ANIM_DUR 0.300f

// Extensão da classe para declarar métodos privados
@interface ConfirmLoadView ( Private )

// Inicializa a view
-( bool )buildConfirmLoadView;

// Libera a memória alocada pelo objeto
//-( void )cleanConfirmLoadView;

@end

@implementation ConfirmLoadView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

================================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildConfirmLoadView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

================================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildConfirmLoadView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildConfirmLoadView
	Inicializa a view.

================================================================================================*/

-( bool )buildConfirmLoadView
{
	// Inicializa as variáveis da classe
	hYesSelector = nil;
	hYesTarget = nil;
	
	hNoSelector = nil;
	hNoTarget = nil;
	
//	{ // Evita erros de compilação por causa dos gotos
		
		// Aloca os objetos que não foram criados via Interface Builder
		// ...

		[self setHidden: YES];
	
		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self cleanNOProfileSelectView];
//		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

================================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Configura os elementos da view que foram criados pelo InterfaceBuilder
	[self setHidden: YES];
	
	 FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;	
	
	hBtYesText = [[FontLabel alloc] initWithFrame:/*CGRectMake(17, 6, 0, 0)*/[hBtYes bounds] fontName:@"Good Girl" pointSize:20.0f];	
	hBtYesText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CONFIRMLOAD_YES] );	 
	
	hBtNoText = [[FontLabel alloc] initWithFrame:/*CGRectMake(24, 6, 0, 0)*/ [hBtNo bounds] fontName:@"Good Girl" pointSize:20.0f];	
	hBtNoText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CONFIRMLOAD_NO] );	 

	hQuestionText = [[FontLabel alloc] initWithFrame:/*CGRectMake(134, 97, 217, 83)*/[hQuestionBox bounds] fontName:@"Good Girl" pointSize:17.0f];	
	hQuestionText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CONFIRMLOAD_QUESTION] );	 
	hQuestionText.lineBreakMode = UILineBreakModeWordWrap;
	[hQuestionText setNumberOfLines:5];
	
	FontLabel *txtArray[MSG_N_CONFIRM_LOAD_VIEW]={hBtYesText,hBtNoText,hQuestionText};
	UIView *viewArray[MSG_N_CONFIRM_LOAD_VIEW]={hBtYes,hBtNo,hQuestionBox};	
	
	std::string currOsVersion;
	DeviceInterface::GetDeviceSystemVersion( currOsVersion );
	bool olderThan30 = Utils::CompareVersions( currOsVersion, "3.0" ) < 0;
	
	
	for( uint8 i = 0 ; i < MSG_N_CONFIRM_LOAD_VIEW ; ++i )
	{
		txtArray[i].textColor = [UIColor whiteColor];
		txtArray[i].backgroundColor = nil;
		txtArray[i].opaque = NO;
		txtArray[i].shadowColor=[UIColor darkGrayColor];
		[txtArray[i] setTextAlignment:UITextAlignmentCenter];		
		[txtArray[i] setAdjustsFontSizeToFitWidth:YES];		
		txtArray[i].shadowOffset = CGSizeMake(-1,1);			
		[viewArray[i] addSubview:txtArray[i]];	
		
		if (olderThan30)
		{			
			
			UIView *handler;
			[viewArray[i] layoutSubviews];
			[viewArray[i] bringSubviewToFront:txtArray[i]];
			NSArray *views=[viewArray[i] subviews];
			int viewscount=[views count];
			
			for (uint8 c=0;c<viewscount;c++ )		
			{			
				handler=[views objectAtIndex:c ];
				if (handler!=txtArray[i])
					[handler setHidden:YES ];
			}
		}
		
		[txtArray[i] release];	
	}		
	
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

================================================================================================*/

//- ( void )dealloc
//{
//	//[self cleanConfirmLoadView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM acceptsFirstResponder

================================================================================================*/

-( BOOL )acceptsFirstResponder
{
	return YES;
}

/*==============================================================================================

MENSAGEM onBtPressed:
	Chamado quando um botão é pressionado.

================================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	[self setHidden: YES];

	if( hButton == hBtYes )
	{
		if( hYesSelector && hYesTarget )
		{
			if( [hYesTarget respondsToSelector: hYesSelector] )
				[hYesTarget performSelector: hYesSelector];
		}
	}
	else
	{
		if( hNoSelector && hNoTarget )
		{
			if( [hNoTarget respondsToSelector: hNoSelector] )
				[hNoTarget performSelector: hNoSelector];
		}
	}	
}

/*==============================================================================================

MENSAGEM setYesSelector:WithTarget:
	Determina a callback para a resposta 'Yes'.

================================================================================================*/

-( void )setYesSelector:( SEL )hSelector WithTarget:( id )target
{
	hYesSelector = hSelector;
	hYesTarget = target;
}

/*==============================================================================================

MENSAGEM setNoSelector:WithTarget:
	Determina a callback para a resposta 'No'.

================================================================================================*/

-( void )setNoSelector:( SEL )hSelector WithTarget:( id )target
{	
	hNoSelector = hSelector;
	hNoTarget = target;
}

/*==============================================================================================

MENSAGEM startShowAnim
	Executa a animação que exibe o popup.

================================================================================================*/

-( void )startShowAnim
{
	[self setAlpha: 0.0f];
	[self setHidden: NO];

	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: CONFIRM_LOAD_VIEW_ANIM_DUR];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseIn];
	[self setAlpha: 1.0f];
	[UIView commitAnimations];
}

@end
