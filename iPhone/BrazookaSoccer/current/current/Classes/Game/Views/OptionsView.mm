#import "OptionsView.h"
#import "DeviceInterface.h"
// Components
#include "Config.h"
#include "ObjcMacros.h"

// Game
#include "GameInfo.h"
#include "FreeKickAppDelegate.h"

// Duração das animações de exibição dos botões
#define OPTIONS_VIEW_LEFT_BUTTONS_ANIM_DUR	0.150f
#define OPTIONS_VIEW_RIGHT_BUTTONS_ANIM_DUR	0.100f

// Quantidade de tempo a mais que cada opção do menu, após a 1a, leva para terminar a animação de entrada na tela
#define OPTIONS_VIEW_LEFT_BUTTONS_ANIM_DIFF 0.075f

// Duração da animação de fadein do ícone
#define OPTIONS_VIEW_ICON_ANIM_DUR 0.350f

// Número de pixels, dos botões, que ficam fora da tela
#define OPTIONS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN 8
#define OPTIONS_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN 16

// Número de opções no menu de opções
#define OPTIONS_VIEW_N_OPTIONS 3

// Número de pixels, dos controles da esquerda, que ficam fora da tela
#define OPTIONS_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN 33.0f

// Tempo total das animações
#define OPTIONS_VIEW_HAIR_ANIM_DUR 0.150f

// Configurações da barra de volume
#define OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH 18

// Configurações das checkboxs
#define OPTIONS_VIEW_VB_CHECK_ON_POS_X	283.0f
#define OPTIONS_VIEW_VB_CHECK_OFF_POS_X	386.0f

// Número de tags que emitem som ao entrar na tela
#define OPTIONS_VIEW_N_TAGS_WITH_SOUND ( OPTIONS_VIEW_N_OPTIONS + 1 )

// Define em quantos pixels os controles de volume devem ser deslocados quando não tivermos vibração
#define OPTIONS_VIEW_NO_VIB_VOLUME_CONTROLS_OFFSET_Y 50

// Extensão da classe para declarar métodos privados
@interface OptionsView( Private )

- ( bool ) buildOptionsView;

- ( void ) setState:( OptionsViewState ) state;
- ( void ) hideInterfaceControls;

// Determina o estado das checkboxes de vribração
- ( void ) setVibrationStatus:( bool )vibrationOn;

@end

// Implementção da classe
@implementation OptionsView

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		// Inicializa o objeto
		if( ![self buildOptionsView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildOptionsView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildOptionsView
	Inicializa a view.

==============================================================================================*/

- ( bool )buildOptionsView
{
	pMyGameInfo = NULL;
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
	[hHelpBox setFont: [(( FreeKickAppDelegate* )APP_DELEGATE ) getiPhoneFont]];

	// Modifica a aparência da barra de volume
	char buffer[ PATH_MAX ];
	UIImage *hThumbImg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "optThumb", "png" )) ];
	[hVolumeSlider setThumbImage: hThumbImg forState: UIControlStateNormal];
	[hVolumeSlider setThumbImage: hThumbImg forState: UIControlStateDisabled];
	
	UIImage *hThumbPressedImg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "optThumbp", "png" )) ];
	[hVolumeSlider setThumbImage: hThumbPressedImg forState: UIControlStateHighlighted];
	[hVolumeSlider setThumbImage: hThumbPressedImg forState: UIControlStateSelected];
	
	UIImage *hMaxAux = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "optTrkMin", "png" )) ];
	UIImage *hMaxImg = [hMaxAux stretchableImageWithLeftCapWidth: OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH topCapHeight: 0];
	
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateNormal];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateDisabled];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateHighlighted];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateSelected];

	[hMaxImg release];
	
	UIImage *hMinAux = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "optTrkMax", "png" )) ];
	UIImage *hMinImg = [hMinAux stretchableImageWithLeftCapWidth: OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH topCapHeight: 0];

	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateNormal];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateDisabled];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateHighlighted];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateSelected];
	
	[hMinImg release];
	
	[hVolumeSlider setContinuous: YES];
	
	// Se o device não possui vibração, não exibiremos os respectivos controles e temos que centralizar os controles de volume
	if( [(( FreeKickAppDelegate* )APP_DELEGATE ) hasVibration] )
	{
		[hVolumeSlider setFrame: CGRectMake( hVolumeSlider.frame.origin.x, hVolumeSlider.frame.origin.y - 10.0f, hVolumeSlider.frame.size.width, hVolumeSlider.frame.size.height + 20.0f )];
	}
	else
	{
		[hImgVolumeText setFrame: CGRectMake( hImgVolumeText.frame.origin.x, hImgVolumeText.frame.origin.y - 10.0f + OPTIONS_VIEW_NO_VIB_VOLUME_CONTROLS_OFFSET_Y, hImgVolumeText.frame.size.width, hImgVolumeText.frame.size.height + 20.0f ) ];
		[hVolumeSlider setFrame: CGRectMake( hVolumeSlider.frame.origin.x, hVolumeSlider.frame.origin.y - 10.0f + OPTIONS_VIEW_NO_VIB_VOLUME_CONTROLS_OFFSET_Y, hVolumeSlider.frame.size.width, hVolumeSlider.frame.size.height + 20.0f )];
	}
	
	[self setStyle: OPTIONS_VIEW_STYLE_PAUSE_OPTIONS];
	[self setState: OPTIONS_VIEW_STATE_HIDDEN];
	
	soundFlags = std::vector< bool >( OPTIONS_VIEW_N_TAGS_WITH_SOUND, false );

	 FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;		

	hImgVolumeText = [[FontLabel alloc] initWithFrame:/*CGRectMake(311, 18, 84, 21)*/[hVolumeBox bounds] fontName:@"Good Girl" pointSize:20.5f];	
	hImgVolumeText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_VOLUME] );	
	
	hImgVbText = [[FontLabel alloc] initWithFrame:/*CGRectMake(303,119,120,21)*/[hVibrationBox bounds] fontName:@"Good Girl" pointSize:21.5f];	
	hImgVbText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_VIBRATION] );
	
	
	hVbCheckBoxOnText = [[FontLabel alloc] initWithFrame:[hYesBox bounds] fontName:@"Good Girl" pointSize:22.0f];	
	hVbCheckBoxOnText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_ON] );
	
	hVbCheckBoxOffText = [[FontLabel alloc] initWithFrame:[hNoBox bounds] fontName:@"Good Girl" pointSize:21.5f];	
	hVbCheckBoxOffText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_OFF] );
	
	
	hBtHelpText = [[FontLabel alloc] initWithFrame:[hBtHelp bounds] fontName:@"Good Girl" pointSize:17.0f];	
	hBtHelpText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_HELP] );
	
	
	hBtQuitText = [[FontLabel alloc] initWithFrame:[hBtQuit bounds] fontName:@"Good Girl" pointSize:17.5f];	
	hBtQuitText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_QUIT] );
	
	hBtBackText = [[FontLabel alloc] initWithFrame:[hBtBack bounds] fontName:@"Good Girl" pointSize:17.5f];	
	hBtBackText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_BACK]);
	
	hBtSettingsText = [[FontLabel alloc] initWithFrame:[hBtSettings bounds] fontName:@"Good Girl" pointSize:16.5f];	
	hBtSettingsText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_SETTING] );	
	
	hImgQuitQuestionText = [[FontLabel alloc] initWithFrame:/*CGRectMake(233,74,234,18)*/[hQuestion bounds] fontName:@"Good Girl" pointSize:18.0f];	
	hImgQuitQuestionText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_GOBACK] );	
	
	hBtYesText = [[FontLabel alloc] initWithFrame:[hBtYes bounds] fontName:@"Good Girl" pointSize:22.0f];	
	hBtYesText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_OPTIONS_YES] );		
	
	FontLabel *txtArray[MSG_N_OPTIONS_VIEW]={hImgVolumeText,hImgVbText,hBtHelpText,hVbCheckBoxOnText,hVbCheckBoxOffText,hBtQuitText,hBtBackText,hBtSettingsText, hBtYesText,hImgQuitQuestionText};
	UIView *viewArray[MSG_N_OPTIONS_VIEW]={hVolumeBox,hVibrationBox,hBtHelp,hYesBox,hNoBox,hBtQuit,hBtBack,hBtSettings, hBtYes,hQuestion};	
	std::string currOsVersion;
	DeviceInterface::GetDeviceSystemVersion( currOsVersion );
	bool olderThan30 = Utils::CompareVersions( currOsVersion, "3.0" ) < 0;
	
	for( uint8 i = 0 ; i < MSG_N_OPTIONS_VIEW ; ++i )
	{
		txtArray[i].textColor = [UIColor whiteColor];
		txtArray[i].backgroundColor = nil;
		txtArray[i].opaque = NO;
//		[txtArray[i] sizeToFit];		
		[txtArray[i] setAdjustsFontSizeToFitWidth:YES];				
		txtArray[i].shadowColor=[UIColor darkGrayColor];
		txtArray[i].shadowOffset = CGSizeMake(-1,1);
		[viewArray[i] addSubview:txtArray[i]];
		[txtArray[i] setTextAlignment:UITextAlignmentCenter];		
		if (olderThan30)
		{			
			
			UIView *handler;
			[viewArray[i] layoutSubviews];
			[viewArray[i] bringSubviewToFront:txtArray[i]];
			NSArray *views=[viewArray[i] subviews];
			int viewscount=[views count];
			
			for (uint8 c=0;c<viewscount;c++ )		
			{			
				handler=[views objectAtIndex:c ];
				if (handler!=txtArray[i])
					[handler setHidden:YES ];
			}
		}
		[txtArray[i] release];	
	}
	
	/////////////
	{
		NSArray *subviews=hBtBack.subviews;
		FontLabel *fl=[subviews lastObject];
		
		CGPoint centro=(fl).center;
		centro.y=centro.y*0.8f;
		(fl).center=centro;
	}
	/////////////
	
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
    [super dealloc];
}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	switch( currState )
	{
		case OPTIONS_VIEW_STATE_SHOWING_STEP_0:
			{
				float finalX = SCREEN_WIDTH - hImgHair.frame.size.width + OPTIONS_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN;
				float nextX = hImgHair.frame.origin.x - ( ( timeElapsed * hImgHair.frame.size.width ) / OPTIONS_VIEW_HAIR_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlags[0] )
				{
					soundFlags[0] = true;
					[(( FreeKickAppDelegate* )APP_DELEGATE) playAudioNamed: SOUND_NAME_HAIR_MOVE AtIndex: SOUND_INDEX_HAIR_MOVE Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: OPTIONS_VIEW_STATE_SHOWING_STEP_1];
					
					soundFlags[0] = false;
				}

				[hImgHair setFrame: CGRectMake( nextX, hImgHair.frame.origin.y, hImgHair.frame.size.width, hImgHair.frame.size.height )];
			}
			break;

		case OPTIONS_VIEW_STATE_SHOWING_STEP_1:
			{
				bool playedSound = false;  // Impede que dois sons sejam tocados exatamente ao mesmo tempo

				uint8 done = 0;
				float diff = 0.0f;
				UIButton* btsArray[ OPTIONS_VIEW_N_OPTIONS ] = { hBtSettings, hBtHelp, hBtQuit };
				
				for( uint8 i = 0 ; i < OPTIONS_VIEW_N_OPTIONS ; ++i )
				{
					if( !btsArray[ i ].enabled )
					{
						++done;
					}
					else
					{
						float totalMovement = largestWidth - OPTIONS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
						float nextX = btsArray[ i ].frame.origin.x + (( timeElapsed * totalMovement ) / ( OPTIONS_VIEW_LEFT_BUTTONS_ANIM_DUR + diff ));
						
						if( ( ( nextX + btsArray[ i ].frame.size.width ) > 0.0f ) && !soundFlags[i] && !playedSound )
						{
							playedSound = true;
							soundFlags[i] = true;
							[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING + i Looping: false];
						}
						
						if( nextX > -OPTIONS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN )
						{
							nextX = -OPTIONS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
							++done;
						}

						[btsArray[ i ] setFrame: CGRectMake( nextX, btsArray[ i ].frame.origin.y, btsArray[ i ].frame.size.width, btsArray[ i ].frame.size.height )];
						
						diff += OPTIONS_VIEW_LEFT_BUTTONS_ANIM_DIFF;
					}
				}
				
				[hIcon setAlpha: hIcon.alpha + ( timeElapsed / OPTIONS_VIEW_ICON_ANIM_DUR ) ];
				if( hIcon.alpha > 1.0f )
				{
					[hIcon setAlpha: 1.0f];
					++done;
				}
				
				// +1 => Animação do ícone
				if( done == OPTIONS_VIEW_N_OPTIONS + 1 )
					[self setState: OPTIONS_VIEW_STATE_SHOWING_STEP_2 ];
			}
			break;
			
		case OPTIONS_VIEW_STATE_SHOWING_STEP_2:
			{
				float totalMovement = hBtBack.frame.size.width - OPTIONS_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN;
				
				float finalX = SCREEN_WIDTH - hBtBack.frame.size.width + OPTIONS_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN;
				float nextX = hBtBack.frame.origin.x - (( timeElapsed * totalMovement ) / OPTIONS_VIEW_RIGHT_BUTTONS_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlags[ OPTIONS_VIEW_N_TAGS_WITH_SOUND - 1 ] )
				{
					soundFlags[ OPTIONS_VIEW_N_TAGS_WITH_SOUND - 1 ] = true;
					[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING + ( OPTIONS_VIEW_N_TAGS_WITH_SOUND - 1 ) Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: OPTIONS_VIEW_STATE_SHOWN];
				}
				
				[hBtBack setFrame: CGRectMake( nextX, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height )];
			}
			break;
		case OPTIONS_VIEW_STATE_SHOWN:
				hBtBackText.textColor = [UIColor redColor];			
			break;
		case OPTIONS_VIEW_STATE_HIDING:
			{
				uint8 done = 0;
				float diff = 0.0f;
				UIButton* btsArray[ OPTIONS_VIEW_N_OPTIONS ] = { hBtSettings, hBtHelp, hBtQuit };
				
				for( uint8 i = 0 ; i < OPTIONS_VIEW_N_OPTIONS ; ++i )
				{
					if( !btsArray[ i ].enabled )
					{
						++done;
					}
					else
					{
						float totalMovement = largestWidth - OPTIONS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;	
						float nextX = btsArray[ i ].frame.origin.x - (( timeElapsed * totalMovement ) / ( OPTIONS_VIEW_LEFT_BUTTONS_ANIM_DUR + diff ));
						
						if( nextX < -largestWidth )
						{
							nextX = -largestWidth;
							++done;
						}
						
						[btsArray[ i ] setFrame: CGRectMake( nextX, btsArray[ i ].frame.origin.y, btsArray[ i ].frame.size.width, btsArray[ i ].frame.size.height )];
						
						diff += OPTIONS_VIEW_LEFT_BUTTONS_ANIM_DIFF;
					}
				}
				
				[hIcon setAlpha: hIcon.alpha - ( timeElapsed / OPTIONS_VIEW_ICON_ANIM_DUR ) ];
				if( hIcon.alpha < 0.0f )
				{
					[hIcon setAlpha: 0.0f];
					++done;
				}

				// +1 => Animação do ícone
				if( done == OPTIONS_VIEW_N_OPTIONS + 1 )
					[self setState: OPTIONS_VIEW_STATE_HIDDEN ];
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM setState
	Determina o estado da view.

==============================================================================================*/

- ( void ) setState:( OptionsViewState ) state
{
	currState = state;
	
	switch( state )
	{
		case OPTIONS_VIEW_STATE_HIDDEN:
			{
				CGRect aux;
				UIButton* btsArray[ OPTIONS_VIEW_N_OPTIONS ] = { hBtSettings, hBtHelp, hBtQuit };

				for( uint8 i = 0 ; i < OPTIONS_VIEW_N_OPTIONS ; ++i )
				{
					aux = btsArray[ i ].frame;
					[btsArray[ i ] setFrame: CGRectMake( -largestWidth, aux.origin.y, aux.size.width, aux.size.height )];
				}
				
				[hIcon setAlpha: 0.0f];
				
				aux = hBtBack.frame;
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH, aux.origin.y, aux.size.width, aux.size.height )];
				
				aux = hImgHair.frame;
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH, aux.origin.y, aux.size.width, aux.size.height ) ];
				
				soundFlags = std::vector< bool >( OPTIONS_VIEW_N_TAGS_WITH_SOUND, false );
				
				[self suspend];
			}
			break;

		case OPTIONS_VIEW_STATE_SHOWN:
			{
				CGRect aux;
				UIButton* btsArray[ OPTIONS_VIEW_N_OPTIONS ] = { hBtSettings, hBtHelp, hBtQuit };

				for( uint8 i = 0 ; i < OPTIONS_VIEW_N_OPTIONS ; ++i )
				{
					aux = btsArray[ i ].frame;
					[btsArray[ i ] setFrame: CGRectMake( -OPTIONS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN, aux.origin.y, aux.size.width, aux.size.height )];
				}
				
				[hIcon setAlpha: 1.0f];

				aux = hBtBack.frame;
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH - aux.size.width + OPTIONS_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN, aux.origin.y, aux.size.width, aux.size.height )];
				
				aux = hImgHair.frame;
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH - aux.size.width + OPTIONS_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN, aux.origin.y, aux.size.width, aux.size.height ) ];
				
				// A opção padrão é "Settings"
				[self onBtPressed: hBtSettings];
				
				[self suspend];
			}
			break;

		case OPTIONS_VIEW_STATE_SHOWING_STEP_0:
		case OPTIONS_VIEW_STATE_HIDING:
			[self resume];
			break;
			
		case OPTIONS_VIEW_STATE_SHOWING_STEP_1:
			break;
	}
}

/*==============================================================================================

MENSAGEM onVolumeChanged
	Indica que o usuário alterou o volume dos sons da aplicação.

==============================================================================================*/

- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;
{
	[( ( FreeKickAppDelegate* )APP_DELEGATE ) setVolume: [hVolumeSlider value]];
}

/*==============================================================================================

MENSAGEM onBtPressed
	Indica que o usuário selecionou uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtPressed:( id )hButton
{
	if( currState != OPTIONS_VIEW_STATE_SHOWN )
		return;
	
	if( ( hButton == hVbCheckBoxOn ) || ( hButton == hVbCheckBoxOff ) )
	{
		[self setVibrationStatus: hButton == hVbCheckBoxOn];
	}
	else if( hButton == hBtYes )
	{
		FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;
		[hAppDelegate stopAudio];
		
		//<DM>
		if( pMyGameInfo->gameMode == GAME_MODE_RANKING )
			[hAppDelegate saveGameState];
		//</DM>		

		[hAppDelegate performTransitionToView: VIEW_INDEX_MAIN_MENU];
	}
	else
	{
		[self hideInterfaceControls];

		if( hButton == hBtSettings )
		{
			[hVolumeSlider setHidden: NO];
			[hVolumeSlider setEnabled: YES];

			[hImgVolumeText setHidden: NO];
			
			if( [(( FreeKickAppDelegate* )APP_DELEGATE ) hasVibration] )
			{
				[hVbCheck setHidden: NO];
				[hImgVbText setHidden: NO];
				
				[hVbCheckBoxOn setHidden: NO];
				[hVbCheckBoxOn setEnabled: YES];
				[hYesBox setHidden:NO];
				
				[hVbCheckBoxOff setHidden: NO];
				[hVbCheckBoxOff setEnabled: YES];
				[hNoBox setHidden:NO];				
			}
		}

		// TODOO : Implementar no update
	//	else if( hButton == hBtCalibrate )
	//	{
	//	}

		else if( hButton == hBtHelp )
		{
			[hHelpBox setHidden: NO];
			[hHelpBox setUserInteractionEnabled: YES];
		}
		else if( hButton == hBtQuit )
		{
			[hImgQuitQuestionText setHidden: NO];
	
			[hBtYes setHidden: NO];
			[hBtYes setEnabled: YES];
		}
		else if( hButton == hBtBack )
		{
			[APP_DELEGATE performTransitionToView: previousViewIndex];
		}
	}
}

/*==============================================================================================

MENSAGEM setVibrationStatus
	Determina o estado das checkboxes de vribração.

==============================================================================================*/

- ( void ) setVibrationStatus:( bool )vibrationOn
{
	if( [(( FreeKickAppDelegate* )APP_DELEGATE ) hasVibration] )
	{
		CGRect checkFrame = hVbCheck.frame;
		[hVbCheck setFrame: CGRectMake( vibrationOn ? OPTIONS_VIEW_VB_CHECK_ON_POS_X : OPTIONS_VIEW_VB_CHECK_OFF_POS_X, checkFrame.origin.y, checkFrame.size.width, checkFrame.size.height )];
		
		[(( FreeKickAppDelegate* ) APP_DELEGATE ) setVibrationStatus: vibrationOn];
	}
}

/*==============================================================================================

MENSAGEM setStyle
	Determina se devemos exibir o menu de opções ou o menu de pausa.

==============================================================================================*/

- ( void ) setStyle:( OptionsViewStyle ) style
{
	currStyle = style;

	[self hideInterfaceControls];

	if( style == OPTIONS_VIEW_STYLE_MAIN_OPTIONS )
	{
		[hBtQuit setHidden: YES];
		[hBtQuit setEnabled: NO];
		
		[hBtHelp setHidden: YES];
		[hBtHelp setEnabled: NO];
	}
	else // if( style == OPTIONS_VIEW_STYLE_PAUSE_OPTIONS )
	{
		[hBtQuit setHidden: NO];
		[hBtQuit setEnabled: YES];
		
		[hBtHelp setHidden: NO];
		[hBtHelp setEnabled: YES];
	}
	
	largestWidth = 0.0f;

	UIButton* btsArray[ OPTIONS_VIEW_N_OPTIONS ] = { hBtSettings, hBtHelp, hBtQuit };
	for( uint8 i = 0 ; i < OPTIONS_VIEW_N_OPTIONS ; ++i )
	{
		if( btsArray[ i ].enabled && ( btsArray[ i ].frame.size.width > largestWidth ) )
			largestWidth = btsArray[ i ].frame.size.width;
	}

	[self setState: OPTIONS_VIEW_STATE_HIDDEN];
}

/*==============================================================================================

MENSAGEM setPrevScreen
	Indica que tela deverá ser chamada quando sairmos deste menu.

==============================================================================================*/

- ( void ) setPrevScreen:( int8 ) prevViewIndex
{
	previousViewIndex = prevViewIndex;
}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes de iniciarmos uma transição para esta view.

==============================================================================================*/

- ( void )onBeforeTransition:( GameInfo* )pGameInfo
{
	pMyGameInfo = pGameInfo;

	// Atualiza a barra de volume
	[hVolumeSlider setValue: [(( FreeKickAppDelegate* ) APP_DELEGATE ) getAudioVolume]];
	
	// Atualiza o valor da vibração
	[self setVibrationStatus: [(( FreeKickAppDelegate* ) APP_DELEGATE ) isVibrationOn]];


	{
		std::string finaltext;
		std::string ver;
		[[ApplicationManager GetInstance] appVersion :ver];
		finaltext=[(( FreeKickAppDelegate* )APP_DELEGATE) getText: TEXT_INDEX_HELP];		
		finaltext+=ver;
		hHelpBox.text = CHAR_ARRAY_TO_NSSTRING(finaltext.c_str() );
	}
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	if( currStyle == OPTIONS_VIEW_STYLE_PAUSE_OPTIONS )
		[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed:SOUND_NAME_PAUSE_SAMBA AtIndex:SOUND_INDEX_PAUSE_SAMBA Looping: true];

	[self setState: OPTIONS_VIEW_STATE_SHOWING_STEP_0];
}

/*==============================================================================================

MENSAGEM hideInterfaceControls
	Esconde os controles da interface.

==============================================================================================*/

- ( void ) hideInterfaceControls
{
	[hHelpBox setHidden: YES];
	[hHelpBox setUserInteractionEnabled: NO];
	
	[hVolumeSlider setHidden: YES];
	[hVolumeSlider setEnabled: NO];
	
	[hVbCheck setHidden: YES];
	[hImgVbText setHidden: YES];
	[hImgVolumeText setHidden: YES];
	[hImgQuitQuestionText setHidden: YES];
		
	[hVbCheckBoxOn setHidden: YES];
	[hVbCheckBoxOn setEnabled: NO];
	[hYesBox setHidden:YES];
	
	[hVbCheckBoxOff setHidden: YES];
	[hVbCheckBoxOff setEnabled: NO];
	[hNoBox setHidden:YES];
	
	[hBtYes setHidden: YES];
	[hBtYes setEnabled: NO];
}

// Fim da implementação da classe
@end
