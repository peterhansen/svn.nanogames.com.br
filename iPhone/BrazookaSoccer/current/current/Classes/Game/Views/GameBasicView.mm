#include "GameBasicView.h"

// Components
#include "ObjcMacros.h"

// Game
#include "Config.h"
#include "FreeKickAppDelegate.h"

// Extensão da classe para declarar métodos privados
//@interface GameBasicView( Private )
//	// ...
//@end

// Implementção da classe
@implementation GameBasicView

/*==============================================================================================

MENSAGEM onBtClick:
	Indica que o usuário está pressionando uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtClick:( id )hButton
{
	[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_BT_CLICK AtIndex: SOUND_INDEX_BT_CLICK Looping: false];
}

// Fim da implementação da classe
@end
