#include "PlayMenuView.h"
#import "DeviceInterface.h"
// Components
#include "Config.h"
#include "FileSystem.h"
#include "ObjcMacros.h"
#include "Point3f.h"

// Game
#include "GameInfo.h"
#include "Iso_Defines.h"
#include "FreeKickAppDelegate.h"

// Duração das animações de exibição dos botões
#define PLAY_MENU_VIEW_LEFT_BUTTONS_ANIM_DUR	0.150f
#define PLAY_MENU_VIEW_RIGHT_BUTTONS_ANIM_DUR	0.100f

// Quantidade de tempo a mais que cada opção do menu, após a 1a, leva para terminar a animação de entrada na tela
#define PLAY_MENU_VIEW_LEFT_BUTTONS_ANIM_DIFF 0.075f

// Duração da animação de fadein do ícone
#define PLAY_MENU_VIEW_ICON_ANIM_DUR 0.350f

// Número de pixels, dos botões, que ficam fora da tela
#define PLAY_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN 8
#define PLAY_MENU_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN 16

// Número de opções no menu de opções
#define PLAY_MENU_VIEW_N_OPTIONS 2

// Número de pixels, dos controles da esquerda, que ficam fora da tela
#define PLAY_MENU_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN 33.0f

// Tempo total das animações
#define PLAY_MENU_VIEW_HAIR_ANIM_DUR 0.150f

// Outras definições
#define PLAY_MENU_VIEW_N_FADING_CONTROLS 2

// Número de tags que emitem som ao entrar na tela
#define PLAY_MENU_VIEW_N_TAGS_WITH_SOUND ( PLAY_MENU_VIEW_N_OPTIONS + 1 )

// Define a opção padrão do menu
#define PLAY_MENU_VIEW_OPT_ENDLESS	0
#define PLAY_MENU_VIEW_OPT_TRAINING	1
#define PLAY_MENU_VIEW_DEFAULT_OPT PLAY_MENU_VIEW_OPT_ENDLESS

// Extensão da classe para declarar métodos privados
@interface PlayMenuView( Private )

// Inicializa a view
- ( bool )buildPlayMenuView;

// Desaloca os elementos da view que não foram alocados via InterfaceBuilder
-( void )cleanPlayMenuView;

// Determina o estado da view
- ( void )setState:( PlayMenuViewState ) state;

// Método chamado quando o jogador indica que deseja continuar o último jogo
-( void )onContinueLastGame;

// Método chamado quando o jogador indica que quer começar um novo jogo
-( void )onStartNewGame;

// Carrega a próxima view do jogo
-( void )onNextScreen;

@end

// Início da implementação da classe
@implementation PlayMenuView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildPlayMenuView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildPlayMenuView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildPlayMenuView
	Inicializa a view.

================================================================================================*/

-( bool )buildPlayMenuView
{
	hConfirmPopUp = nil;

	{ // Evita erros de compilação por causa dos gotos
	
		hConfirmPopUp = ( ConfirmLoadView* )[[ApplicationManager GetInstance] loadViewFromXib: "ConfirmLoadView" ];
		if( !hConfirmPopUp )
			goto Error;

		[hConfirmPopUp setNoSelector: @selector( onStartNewGame ) WithTarget: self];
		[hConfirmPopUp setYesSelector: @selector( onContinueLastGame ) WithTarget: self];

		return true;
		
	} // Evita erros de compilação por causa dos gotos

	Error:
		[self cleanPlayMenuView];
		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
	[hText setFont: [(( FreeKickAppDelegate* )APP_DELEGATE ) getiPhoneFont]];
	
	// Verifica qual o maior botão
	largestWidth = 0.0f;
	
	FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;		
	
	hBtEndlessText = [[FontLabel alloc] initWithFrame:[hBtEndless bounds] fontName:@"Good Girl" pointSize:18.0f];	
	hBtEndlessText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_PLAYMENU_ENDLESS] );

	hBtTrainingText = [[FontLabel alloc] initWithFrame:[hBtTraining bounds] fontName:@"Good Girl" pointSize:18.0f];	
	hBtTrainingText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_PLAYMENU_TRAINING] );

	
	hBtStartText = [[FontLabel alloc] initWithFrame:[hBtStart bounds] fontName:@"Good Girl" pointSize:18.0f];	
	hBtStartText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_PLAYMENU_START] );
	
	hBtBackText = [[FontLabel alloc] initWithFrame:[hBtBack bounds] fontName:@"Good Girl" pointSize:17.5f];	
	hBtBackText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_PLAYMENU_BACK] );	
	
	UIButton* btsArray[ PLAY_MENU_VIEW_N_OPTIONS+2 ] = { hBtEndless, hBtTraining, hBtStart, hBtBack };
	FontLabel *txtArray[ PLAY_MENU_VIEW_N_OPTIONS+2]= { hBtEndlessText, hBtTrainingText,hBtStartText,hBtBackText};	
	std::string currOsVersion;
	DeviceInterface::GetDeviceSystemVersion( currOsVersion );
	bool olderThan30 = Utils::CompareVersions( currOsVersion, "3.0" ) < 0;

	
	for( uint8 i = 0 ; i < PLAY_MENU_VIEW_N_OPTIONS+2 ; ++i )
	{
		float currBtWidth = btsArray[ i ].frame.size.width;
		if( currBtWidth > largestWidth )
			largestWidth = currBtWidth;
		
		txtArray[ i ].textColor = [UIColor whiteColor];
		txtArray[ i ].backgroundColor = nil;
		txtArray[ i ].opaque = NO;
		[txtArray[i] setAdjustsFontSizeToFitWidth:YES];		
		txtArray[ i ].shadowColor=[UIColor darkGrayColor];
		txtArray[ i ].shadowOffset = CGSizeMake(-1,1);			
		[btsArray[i] addSubview:txtArray[i]];	
		[txtArray[i] setTextAlignment:UITextAlignmentCenter];					
		if (olderThan30)
		{			
			
			UIView *handler;
			[btsArray[i] layoutSubviews];
			[btsArray[i] bringSubviewToFront:txtArray[i]];
			NSArray *views=[btsArray[i] subviews];
			int viewscount=[views count];
			
			for (uint8 c=0;c<viewscount;c++ )		
			{			
				handler=[views objectAtIndex:c ];
				if (handler!=txtArray[i])
					[handler setHidden:YES ];
			}
		}
		[txtArray[i] release];	
		
	}
	
	/////////////
	{
		NSArray *subviews=hBtBack.subviews;
		FontLabel *fl=[subviews lastObject];
		
		CGPoint centro=(fl).center;
		centro.y=centro.y*0.8f;
		(fl).center=centro;
	}
	/////////////
	
	soundFlags = std::vector< bool >( PLAY_MENU_VIEW_N_TAGS_WITH_SOUND, false );
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self cleanPlayMenuView];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM cleanPlayMenuView
	Desaloca os elementos da view que não foram alocados via InterfaceBuilder.

==============================================================================================*/

-( void )cleanPlayMenuView
{
	if( hConfirmPopUp )
	{
		if( [hConfirmPopUp superview] != nil )	
			[hConfirmPopUp removeFromSuperview];
		
		[hConfirmPopUp release];	
		hConfirmPopUp = nil;
	}
}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	switch( currState )
	{
		case PLAY_MENU_VIEW_STATE_SHOWING_STEP_0:
			{
				CGRect hairFrame = hImgHair.frame;

				float finalX = SCREEN_WIDTH - hairFrame.size.width + PLAY_MENU_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN;
				float nextX = hairFrame.origin.x - ( ( timeElapsed * hairFrame.size.width ) / PLAY_MENU_VIEW_HAIR_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlags[0] )
				{
					soundFlags[0] = true;
					[(( FreeKickAppDelegate* )APP_DELEGATE) playAudioNamed: SOUND_NAME_HAIR_MOVE AtIndex: SOUND_INDEX_HAIR_MOVE Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: PLAY_MENU_VIEW_STATE_SHOWING_STEP_1];
					
					soundFlags[0] = false;
				}

				[hImgHair setFrame: CGRectMake( nextX, hairFrame.origin.y, hairFrame.size.width, hairFrame.size.height )];
			}
			break;

		case PLAY_MENU_VIEW_STATE_SHOWING_STEP_1:
			{
				bool playedSound = false;  // Impede que dois sons sejam tocados exatamente ao mesmo tempo

				uint8 done = 0;
				float diff = 0.0f;

				CGRect aux;
				UIButton* btsArray[ PLAY_MENU_VIEW_N_OPTIONS ] = { hBtEndless, hBtTraining };
				
				for( uint8 i = 0 ; i < PLAY_MENU_VIEW_N_OPTIONS ; ++i )
				{
					aux = btsArray[ i ].frame;

					float totalMovement = largestWidth - PLAY_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
					float nextX = aux.origin.x + (( timeElapsed * totalMovement ) / ( PLAY_MENU_VIEW_LEFT_BUTTONS_ANIM_DUR + diff ));
					
					if( ( ( nextX + aux.size.width ) > 0.0f ) && !soundFlags[i] && !playedSound )
					{
						playedSound = true;
						soundFlags[i] = true;
						[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING + i Looping: false];
					}
					
					if( nextX > -PLAY_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN )
					{
						nextX = -PLAY_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
						++done;
					}

					[btsArray[ i ] setFrame: CGRectMake( nextX, aux.origin.y, aux.size.width, aux.size.height )];
					
					diff += PLAY_MENU_VIEW_LEFT_BUTTONS_ANIM_DIFF;
				}
				
				[hIcon setAlpha: hIcon.alpha + ( timeElapsed / PLAY_MENU_VIEW_ICON_ANIM_DUR ) ];
				if( hIcon.alpha > 1.0f )
				{
					[hIcon setAlpha: 1.0f];
					++done;
				}
				
				// +1 => Animação do ícone
				if( done == PLAY_MENU_VIEW_N_OPTIONS + 1 )
					[self setState: PLAY_MENU_VIEW_STATE_SHOWING_STEP_2 ];
			}
			break;
			
		case PLAY_MENU_VIEW_STATE_SHOWING_STEP_2:
			{
				uint8 done = 0;
				
				UIView* fadingControls[ PLAY_MENU_VIEW_N_FADING_CONTROLS ] = { hText, hBtStart };
				
				for( uint8 i = 0 ; i < PLAY_MENU_VIEW_N_FADING_CONTROLS ; ++i )
				{
					[fadingControls[i] setAlpha: fadingControls[i].alpha + ( timeElapsed / PLAY_MENU_VIEW_ICON_ANIM_DUR ) ];
					if( fadingControls[i].alpha > 1.0f )
					{
						[fadingControls[i] setAlpha: 1.0f];
						++done;
					}
				}
				
				if( done == PLAY_MENU_VIEW_N_FADING_CONTROLS )
					[self setState: PLAY_MENU_VIEW_STATE_SHOWING_STEP_3];
			}
			break;
			
		case PLAY_MENU_VIEW_STATE_SHOWING_STEP_3:
			{
				CGRect btBackFrame = hBtBack.frame;
				float totalMovement = btBackFrame.size.width - PLAY_MENU_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN;
				
				float finalX = SCREEN_WIDTH - btBackFrame.size.width + PLAY_MENU_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN;
				float nextX = btBackFrame.origin.x - (( timeElapsed * totalMovement ) / PLAY_MENU_VIEW_RIGHT_BUTTONS_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlags[ PLAY_MENU_VIEW_N_TAGS_WITH_SOUND - 1 ] )
				{
					soundFlags[ PLAY_MENU_VIEW_N_TAGS_WITH_SOUND - 1 ] = true;
					[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING + ( PLAY_MENU_VIEW_N_TAGS_WITH_SOUND - 1 ) Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: PLAY_MENU_VIEW_STATE_SHOWN];
				}
				
				[hBtBack setFrame: CGRectMake( nextX, btBackFrame.origin.y, btBackFrame.size.width, btBackFrame.size.height )];
			}
			break;
			
		case PLAY_MENU_VIEW_STATE_HIDING:
			{
				uint8 done = 0;
				float diff = 0.0f;
				
				CGRect aux;
				UIButton* btsArray[ PLAY_MENU_VIEW_N_OPTIONS ] = { hBtEndless, hBtTraining };
				
				for( uint8 i = 0 ; i < PLAY_MENU_VIEW_N_OPTIONS ; ++i )
				{
					aux = btsArray[ i ].frame;

					float totalMovement = largestWidth - PLAY_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;	
					float nextX = aux.origin.x - (( timeElapsed * totalMovement ) / ( PLAY_MENU_VIEW_LEFT_BUTTONS_ANIM_DUR + diff ));
					
					if( nextX < -largestWidth )
					{
						nextX = -largestWidth;
						++done;
					}
					
					[btsArray[ i ] setFrame: CGRectMake( nextX, aux.origin.y, aux.size.width, aux.size.height )];
					
					diff += PLAY_MENU_VIEW_LEFT_BUTTONS_ANIM_DIFF;
				}
				
				[hIcon setAlpha: hIcon.alpha - ( timeElapsed / PLAY_MENU_VIEW_ICON_ANIM_DUR ) ];
				if( hIcon.alpha < 0.0f )
				{
					[hIcon setAlpha: 0.0f];
					++done;
				}

				// +1 => Animação do ícone
				if( done == PLAY_MENU_VIEW_N_OPTIONS + 1 )
					[self setState: PLAY_MENU_VIEW_STATE_HIDDEN ];
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM setState
	Determina o estado da view.

==============================================================================================*/

- ( void ) setState:( PlayMenuViewState ) state
{
	currState = state;
	
	switch( state )
	{
		case PLAY_MENU_VIEW_STATE_HIDDEN:
			{
				CGRect aux;
				UIButton* btsArray[ PLAY_MENU_VIEW_N_OPTIONS ] = { hBtEndless, hBtTraining };

				for( uint8 i = 0 ; i < PLAY_MENU_VIEW_N_OPTIONS ; ++i )
				{
					aux = btsArray[ i ].frame;
					[btsArray[ i ] setFrame: CGRectMake( -largestWidth, aux.origin.y, aux.size.width, aux.size.height )];
				}
				
				[hIcon setAlpha: 0.0f];
				[hText setAlpha: 0.0f];
				[hBtStart setAlpha: 0.0f];
				
				aux = hBtBack.frame;
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH, aux.origin.y, aux.size.width, aux.size.height )];
				
				aux = hImgHair.frame;
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH, aux.origin.y, aux.size.width, aux.size.height ) ];
				
				soundFlags = std::vector< bool >( PLAY_MENU_VIEW_N_TAGS_WITH_SOUND, false );
				
				[self suspend];
			}
			break;

		case PLAY_MENU_VIEW_STATE_SHOWN:
			{
				CGRect aux;
				UIButton* btsArray[ PLAY_MENU_VIEW_N_OPTIONS ] = { hBtEndless, hBtTraining };

				for( uint8 i = 0 ; i < PLAY_MENU_VIEW_N_OPTIONS ; ++i )
				{
					aux = btsArray[ i ].frame;
					[btsArray[ i ] setFrame: CGRectMake( -PLAY_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN, aux.origin.y, aux.size.width, aux.size.height )];
				}
				
				[hIcon setAlpha: 1.0f];
				[hText setAlpha: 1.0f];
				[hBtStart setAlpha: 1.0f];

				aux = hBtBack.frame;
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH - aux.size.width + PLAY_MENU_VIEW_RIGHT_BTS_PIXELS_OUTSIDE_SCREEN, aux.origin.y, aux.size.width, aux.size.height )];
				
				aux = hImgHair.frame;
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH - aux.size.width + PLAY_MENU_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN, aux.origin.y, aux.size.width, aux.size.height ) ];

				[self suspend];
			}
			break;

		case PLAY_MENU_VIEW_STATE_SHOWING_STEP_0:
		case PLAY_MENU_VIEW_STATE_HIDING:
			[self resume];
			break;
			
		case PLAY_MENU_VIEW_STATE_SHOWING_STEP_1:
			break;
	}
}

/*==============================================================================================

MENSAGEM onBeforeTransition:
	Método chamado antes de iniciarmos uma transição para esta view.

==============================================================================================*/

- ( void )onBeforeTransition:( GameInfo* )pGameInfo
{
	pMyGameInfo = pGameInfo;
	
	// Reseta a view
	currState = PLAY_MENU_VIEW_STATE_UNDEFINED;
	[self setState: PLAY_MENU_VIEW_STATE_HIDDEN];
	
	// Deixa selecionada a opção padrão
	#if PLAY_MENU_VIEW_DEFAULT_OPT == PLAY_MENU_VIEW_OPT_TRAINING
		[self onBtPressed: hBtTraining];
	#else
		[self onBtPressed: hBtEndless];
	#endif
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

- ( void ) onBecomeCurrentScreen
{	
	// Inicia a animação
	[self setState: PLAY_MENU_VIEW_STATE_SHOWING_STEP_0];
}

/*==============================================================================================

MENSAGEM onBtPressed
	Indica que o usuário selecionou uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtPressed:( UIButton* )hButton
{
	if( ( currState != PLAY_MENU_VIEW_STATE_SHOWN ) && ( currState != PLAY_MENU_VIEW_STATE_HIDDEN ) )
		return;
	
	if( hButton == hBtStart )
	{
		//<DM>
		FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;
		if( ( pMyGameInfo->gameMode == GAME_MODE_RANKING ) && ( FileSystem::FileExists( NSSTRING_TO_STD_STRING( [hAppDelegate getStateFileName] ) ) == FS_OK ) )
		{
			[[ApplicationManager GetInstance].hViewManager addSubview: hConfirmPopUp];
			[[ApplicationManager GetInstance].hViewManager bringSubviewToFront: hConfirmPopUp];
			[hConfirmPopUp setExclusiveTouch: YES];
			[self setUserInteractionEnabled: NO];

			[hConfirmPopUp startShowAnim];
		}
		else
		//</DM>
		{
			[self onNextScreen];
		}
	}
	else if( hButton == hBtTraining )
	{
		pMyGameInfo->gameMode = GAME_MODE_TRAINING;
		
		// Modifica o texto de descrição do modo de jogo
		hText.text = CHAR_ARRAY_TO_NSSTRING( [(( FreeKickAppDelegate* )APP_DELEGATE) getText: TEXT_INDEX_GAME_MODE_TRAINING] );
		[hText scrollRangeToVisible: NSMakeRange( 0, 1 ) ];
	}
	else // if( hButton == hBtEndless )
	{
		pMyGameInfo->gameMode = GAME_MODE_RANKING;
		
		// Modifica o texto de descrição do modo de jogo
		hText.text = CHAR_ARRAY_TO_NSSTRING( [(( FreeKickAppDelegate* )APP_DELEGATE) getText: TEXT_INDEX_GAME_MODE_RANKING] );
		[hText scrollRangeToVisible: NSMakeRange( 0, 1 )];
	}
	
}

/*==============================================================================================

MENSAGEM onBack
	Mensagem chamada quando o jogador seleciona a opção "Back".

===============================================================================================*/

- ( IBAction ) onBack
{
	pMyGameInfo->gameMode = GAME_MODE_UNDEFINED;

	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM onContinueLastGame
	Método chamado quando o jogador indica que deseja continuar o último jogo.

===============================================================================================*/

-( void )onContinueLastGame
{
	[(( FreeKickAppDelegate* )APP_DELEGATE ) scheduleLoadState];
	[self onNextScreen];
}

/*==============================================================================================

MENSAGEM onStartNewGame
	Método chamado quando o jogador indica que quer começar um novo jogo.

===============================================================================================*/

-( void )onStartNewGame
{
	[(( FreeKickAppDelegate* )APP_DELEGATE ) removeStateFile];
	[self onNextScreen];
}

/*==============================================================================================

MENSAGEM onNextScreen
	Carrega a próxima view do jogo.

===============================================================================================*/

-( void )onNextScreen
{
	// Retira o popup do view manager antes de chamar performTransitionToView: para
	// evitarmos possíveis bugs
	[self cleanPlayMenuView];
	
	FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;

	if( pMyGameInfo->gameMode == GAME_MODE_RANKING )
		[hAppDelegate performTransitionToView: VIEW_INDEX_PLAY_WITH_PROFILE];
	else
		[hAppDelegate performTransitionToView: VIEW_INDEX_LOAD_GAME];
}

// Fim da implementação da classe
@end

