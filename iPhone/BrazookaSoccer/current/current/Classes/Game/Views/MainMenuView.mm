#import "MainMenuView.h"
#import "DeviceInterface.h"
#include "Config.h"
#include "ObjcMacros.h"
#include "AppleAudioManager.h"

#include "FreeKickAppDelegate.h"

// Duração das animações de exibição dos botões
#define MAIN_MENU_VIEW_LEFT_BUTTONS_ANIM_DUR 0.150f

// Quantidade de tempo a mais que cada opção do menu, após a 1a, leva para terminar a animação de entrada na tela
#define MAIN_MENU_VIEW_LEFT_BUTTONS_ANIM_DIFF 0.075f

// Número de pixels, dos botões, que ficam fora da tela
#define MAIN_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN 8

// Número de opções no menu de opções
#define MAIN_MENU_VIEW_N_OPTIONS 6

// Extensão da classe para declarar métodos privados
@interface MainMenuView ()

//- ( bool ) buildMainMenuView;
- ( void ) setState:( MainMenuViewState ) state;

// Inicia a reprodução da música tema do jogo
-( void )playTheme;

@end

// Início da implementação da classe
@implementation MainMenuView

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog( @"iAd baixado com sucesso." );
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog( @"Nao foi possivel realizar o download do iAd." );
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner {
    // restauramos a orientação da tela, pois a chamada do iAd a altera, e carregamos novamente
    // o menu principal
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    [ [ FreeKickAppDelegate GetInstance ] performTransitionToView: VIEW_INDEX_MAIN_MENU ];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave {
    // escondemos o menu (pois caso contrário, ele aparecerá rotacionado quando o iAd for fechado)
    // e paramos todas as músicas antes de prosseguir para o conteúdo do iAd.
    self.hidden = YES; 
    AppleAudioManager::GetInstance()->stopAllSounds();
    return YES;
}

- (void)setMoviePlayerView:(MoviePlayerView*)view {
    movieView = view;
}

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self buildMainMenuView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildMainMenuView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM buildMainMenuView
	Inicializa a view.

==============================================================================================*/

//- ( bool )buildMainMenuView
//{	
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	largestWidth = 0.0f;
	
	FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;
	
	hBtPlayText = [[FontLabel alloc] initWithFrame:[hBtPlay bounds] fontName:@"Good Girl" pointSize:17.0f];	
	hBtPlayText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_MAINMENU_PLAY] );

	hBtRankingText = [[FontLabel alloc] initWithFrame:[hBtRanking bounds] fontName:@"Good Girl" pointSize:17.0f];
	hBtRankingText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_MAINMENU_RANKING] );
	
    hBtOptionsText = [[FontLabel alloc] initWithFrame:[hBtOptions bounds] fontName:@"Good Girl" pointSize:17.0f];
	hBtOptionsText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_MAINMENU_OPTIONS] );
	
	hBtHelpText = [[FontLabel alloc] initWithFrame:[hBtHelp bounds] fontName:@"Good Girl" pointSize:17.0f];	
	hBtHelpText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_MAINMENU_HELP] );
	
	hBtCreditsText = [[FontLabel alloc] initWithFrame:[hBtCredits bounds] fontName:@"Good Girl" pointSize:17.0f];
	hBtCreditsText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_MAINMENU_CREDITS] );
	
	hBtNanoOnlineText = [[FontLabel alloc] initWithFrame:[hBtNanoOnline bounds] fontName:@"Good Girl" pointSize:17.0f];	
	hBtNanoOnlineText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_MAINMENU_NANOONLINE] );
	
	UIButton* btsArray[ MAIN_MENU_VIEW_N_OPTIONS ] = { hBtPlay, hBtRanking, hBtNanoOnline, hBtOptions, hBtHelp, hBtCredits };
	FontLabel *txtArray[ MAIN_MENU_VIEW_N_OPTIONS]= { hBtPlayText, hBtRankingText, hBtNanoOnlineText, hBtOptionsText, hBtHelpText, hBtCreditsText };	
	std::string currOsVersion;
	DeviceInterface::GetDeviceSystemVersion( currOsVersion );
	bool olderThan30 = Utils::CompareVersions( currOsVersion, "3.0" ) < 0;
	
	

	for( uint8 i = 0 ; i < MAIN_MENU_VIEW_N_OPTIONS ; ++i )
	{
		float currBtWidth = btsArray[ i ].frame.size.width;
		if( currBtWidth > largestWidth )
			largestWidth = currBtWidth;

		txtArray[ i].textColor = [UIColor whiteColor];
		txtArray[ i].backgroundColor = nil;
		txtArray[ i].opaque = NO;
		
		txtArray[ i].shadowColor=[UIColor darkGrayColor];
		txtArray[ i].shadowOffset = CGSizeMake(-1,1);		
		[btsArray[i] addSubview:txtArray[i]];			

		[txtArray[i] setAdjustsFontSizeToFitWidth:YES];
		[txtArray[i] setTextAlignment:UITextAlignmentCenter];		
		
		//pequeno 'atalho' para fazer a animacao do menu ficar mais rapida
		if (olderThan30)
		{			
			
		UIView *handler;
		[btsArray[i] layoutSubviews];
		[btsArray[i] bringSubviewToFront:txtArray[i]];
		NSArray *views=[btsArray[i] subviews];
		int viewscount=[views count];
			
			for (uint8 c=0;c<viewscount;c++ )		
			{			
				handler=[views objectAtIndex:c ];
				if (handler!=txtArray[i])
					[handler setHidden:YES ];
			}
		}
		
		
		[txtArray[i] release];
	}


	soundFlags = std::vector< bool >( MAIN_MENU_VIEW_N_OPTIONS, false );
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
    [super dealloc];
}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	switch( currState )
	{
		case MAIN_MENU_VIEW_STATE_SHOWING:
			{
				bool playedSound = false; // Impede que dois sons sejam tocados exatamente ao mesmo tempo

				uint8 done = 0;
				float diff = 0.0f;
				UIButton* btsArray[ MAIN_MENU_VIEW_N_OPTIONS ] = { hBtPlay, hBtRanking, hBtNanoOnline, hBtOptions, hBtHelp, hBtCredits };
				
				for( uint8 i = 0 ; i < MAIN_MENU_VIEW_N_OPTIONS ; ++i )
				{
					float totalMovement = largestWidth - MAIN_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
					float nextX = btsArray[ i ].frame.origin.x + (( timeElapsed * totalMovement ) / ( MAIN_MENU_VIEW_LEFT_BUTTONS_ANIM_DUR + diff ));
					
					// Se começou a aparecer na tela, toca o som de entrada
					if( ( ( nextX + btsArray[ i ].frame.size.width ) > 0.0f ) && !soundFlags[i] && !playedSound )
					{
						playedSound = true;
						soundFlags[i] = true;
						[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING + i Looping: false];
					}
					
					if( nextX > -MAIN_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN )
					{
						nextX = -MAIN_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
						++done;
					}

					[btsArray[ i ] setFrame: CGRectMake( nextX, btsArray[ i ].frame.origin.y, btsArray[ i ].frame.size.width, btsArray[ i ].frame.size.height )];
					
					diff += MAIN_MENU_VIEW_LEFT_BUTTONS_ANIM_DIFF;
				}

				if( done == MAIN_MENU_VIEW_N_OPTIONS )
					[self setState: MAIN_MENU_VIEW_STATE_SHOWN ];
			}
			break;
			
		case MAIN_MENU_VIEW_STATE_HIDING:
			{
				uint8 done = 0;
				float diff = 0.0f;
				UIButton* btsArray[ MAIN_MENU_VIEW_N_OPTIONS ] = { hBtPlay, hBtRanking, hBtNanoOnline, hBtOptions, hBtHelp, hBtCredits };
				
				for( uint8 i = 0 ; i < MAIN_MENU_VIEW_N_OPTIONS ; ++i )
				{
					float totalMovement = largestWidth - MAIN_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;	
					float nextX = btsArray[ i ].frame.origin.x - (( timeElapsed * totalMovement ) / ( MAIN_MENU_VIEW_LEFT_BUTTONS_ANIM_DUR + diff ));
					
					if( nextX < -largestWidth )
					{
						nextX = -largestWidth;
						++done;
					}
					
					[btsArray[ i ] setFrame: CGRectMake( nextX, btsArray[ i ].frame.origin.y, btsArray[ i ].frame.size.width, btsArray[ i ].frame.size.height )];
					
					diff += MAIN_MENU_VIEW_LEFT_BUTTONS_ANIM_DIFF;
				}

				if( done == MAIN_MENU_VIEW_N_OPTIONS )
					[self setState: MAIN_MENU_VIEW_STATE_HIDDEN ];
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM setState
	Determina o estado da view.

==============================================================================================*/

- ( void ) setState:( MainMenuViewState ) state
{
	currState = state;
	
	switch( state )
	{
		case MAIN_MENU_VIEW_STATE_HIDDEN:
			{
				UIButton* btsArray[ MAIN_MENU_VIEW_N_OPTIONS ] = { hBtPlay, hBtRanking, hBtNanoOnline, hBtOptions, hBtHelp, hBtCredits };
				for( uint8 i = 0 ; i < MAIN_MENU_VIEW_N_OPTIONS ; ++i )
				{
					[btsArray[ i ] setFrame: CGRectMake( -largestWidth, btsArray[ i ].frame.origin.y, btsArray[ i ].frame.size.width, btsArray[ i ].frame.size.height )];
					soundFlags[i] = false;
				}

				[self suspend];
			}
			break;

		case MAIN_MENU_VIEW_STATE_SHOWN:
			{
				UIButton* btsArray[ MAIN_MENU_VIEW_N_OPTIONS ] = { hBtPlay, hBtRanking, hBtNanoOnline, hBtOptions, hBtHelp, hBtCredits };
				for( uint8 i = 0 ; i < MAIN_MENU_VIEW_N_OPTIONS ; ++i )
				{
					[btsArray[ i ] setFrame: CGRectMake( -MAIN_MENU_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN, btsArray[ i ].frame.origin.y, btsArray[ i ].frame.size.width, btsArray[ i ].frame.size.height )];
					soundFlags[i] = true;
				}

				[self suspend];
			}
			break;

		case MAIN_MENU_VIEW_STATE_SHOWING:
		case MAIN_MENU_VIEW_STATE_HIDING:
			[self resume];
			break;
	}
}

/*==============================================================================================

MENSAGEM onBtPressed:
	Indica que o usuário selecionou uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtPressed:( id )hButton
{
	if( currState != MAIN_MENU_VIEW_STATE_SHOWN )
		return;

	if( hButton == hBtPlay )
	{
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_PLAY_MENU];
	}
	else if( hButton == hBtRanking )
	{
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_RANKING];
	}
	else if( hButton == hBtOptions )
	{
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_OPTIONS];
	}
	else if( hButton == hBtHelp )
	{
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_HELP];
	}
	else if( hButton == hBtCredits )
	{
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_CREDITS];
	}
	else if( hButton == hBtNanoOnline )
	{
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_NANO_ONLINE_FROM_MAIN_MENU];
	}
}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes de iniciarmos uma transição para esta view.

==============================================================================================*/

- ( void )onBeforeTransition
{    
	[self setState: MAIN_MENU_VIEW_STATE_HIDDEN];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	// OBS: NÃO PODE TOCAR O SOM ANTES DE FAZER A TRANSIÇÃO QUE REMOVERÁ O VÍDEO DA TELA!!!! FAZÊ-LO CAUSA FLICKERING DE VÍDEO EM
	// VERSÕES DO IPHONEOS INFERIORES À 3.0!!!!
	// Por isso [hMainMenu onBecomeCurrentScreen] ficou responsável por tocar o som do menu	
	if( ![(( FreeKickAppDelegate* )APP_DELEGATE ) isPlayingAudioWithIndex: SOUND_INDEX_SPLASH] )
	{
		[NSTimer scheduledTimerWithTimeInterval: 0.05f target:self selector:@selector( playTheme ) userInfo:NULL repeats:NO];
	}
	else
	{
		// Colocamos esta linha dentro do else porque chamá-la logo após agendar o timer estava engasgando
		// o processamento. Fazemos a mesma chamada em playTheme
		[self setState: MAIN_MENU_VIEW_STATE_SHOWING];
	}
    
    // antes da tela ser a atual, avisamos ao iAd que seu delegate é
    // a view onde ele está contido.
    iAd.delegate = self;
}

/*==============================================================================================

MENSAGEM playTheme
	Inicia a reprodução da música tema do jogo.

==============================================================================================*/

-( void )playTheme
{
	[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
	
	[self setState: MAIN_MENU_VIEW_STATE_SHOWING];
}

// Fim da implementação da classe
@end
