//
//  MainMenuView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef MAIN_MENU_H
#define MAIN_MENU_H 1

#import "GameBasicView.h"
#import "FontLabel.h"
#import "FontManager.h"
#include <vector>
#import <iAd/iAd.h>

#import "IAdViewController.h"
#import "MoviePlayerView.h"

#define MAIN_MENU_VIEW_N_OPTIONS 6


enum MainMenuViewState
{
	MAIN_MENU_VIEW_STATE_HIDDEN = 0,
	MAIN_MENU_VIEW_STATE_SHOWING,
	MAIN_MENU_VIEW_STATE_SHOWN,
	MAIN_MENU_VIEW_STATE_HIDING
};

@interface MainMenuView : GameBasicView <ADBannerViewDelegate>
{
	@private
		// Handlers para os botões do menu
	IBOutlet UIButton* hBtPlay;
	FontLabel * hBtPlayText;
	
	IBOutlet UIButton* hBtRanking;
	FontLabel * hBtRankingText;
	
	IBOutlet UIButton* hBtOptions;
	FontLabel * hBtOptionsText;
	
	IBOutlet UIButton* hBtHelp;
	FontLabel * hBtHelpText;	
	
	IBOutlet UIButton* hBtCredits;
	FontLabel * hBtCreditsText;
	
	IBOutlet UIButton* hBtNanoOnline;
	FontLabel * hBtNanoOnlineText;	
	
		// Estado atual da view
		MainMenuViewState currState;
	
		// Auxiliares
		std::vector< bool > soundFlags;
		float largestWidth;
    
    // Por motivos místicos, essa referência precisa estar aqui para que
    // o iAd funcione.
    IBOutlet IAdViewController* adController;
    
    // Referência para a BannerView do iAd
    IBOutlet ADBannerView *iAd;
    
    // Referencia para a MoviePlayerView
    MoviePlayerView* movieView;
}

- (void)setMoviePlayerView:(MoviePlayerView*)view;

// métodos do delegate ADBannerView
- (void)bannerViewActionDidFinish:(ADBannerView *)banner;
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave;
- (void)bannerViewDidLoadAd:(ADBannerView *)banner;
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error;

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Indica que o usuário selecionou uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end

#endif
