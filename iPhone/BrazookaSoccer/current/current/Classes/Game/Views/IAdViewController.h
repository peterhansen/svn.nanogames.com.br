//
//  IAdViewController.h
//  FreeKick
//
//  Created by Ygor Speranza on 8/2/11.
//  Copyright 2011 Nano Studio. All rights reserved.
//

#import <iAd/iAd.h>

@interface IAdViewController : UIViewController {
    ADBannerView *adView;
}
@property (nonatomic,assign) ADBannerView* adView;

@end
