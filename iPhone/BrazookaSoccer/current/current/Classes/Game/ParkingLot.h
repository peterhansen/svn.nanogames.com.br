/*
 *  ParkingLot.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 7/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PARKING_LOT_H
#define PARKING_LOT_H 1

#include "ObjectGroup.h"

class ParkingLot : public ObjectGroup
{
	public:
		// Construtor
		// Exceções: ConstructorException
		ParkingLot( void );
	
		// Destrutor
		virtual ~ParkingLot( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );

		float viewVolumeWidth, viewVolumeHeight;
};

#endif
