/*
 *  RealPosOwner.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef REAL_POS_OWNER_H
#define REAL_POS_OWNER_H 1

#include <stdio.h> // Definição de NULL

#include "Point3f.h"

class RealPosOwner
{
	public:
		#if DEBUG
			// Construtor
			RealPosOwner( float x, float y, float z ) : realPosition( x, y, z ), renderPriority( 0 ) {};
		#endif
	
		// Destrutor
		virtual ~RealPosOwner( void );
	
		// Retorna a posição do objeto no mundo real
		virtual const Point3f* getRealPosition( void ) const;

		// Define a posição do objeto no mundo real
		virtual void setRealPosition( float x, float y, float z );

		void setRealPosition( const Point3f* pRealPosition = NULL );
		void moveRealPosition( float x = 0.0f, float y = 0.0f, float z = 0.0f );
	
		// Para utilização com qsort
		static int QSortRealPosComparer( const void* pA, const void* pB );
	
		// Para utilização com std::sort
		static bool StdSortRealPosComparer( const RealPosOwner* pA, const RealPosOwner* pB );
	
	protected:
		// Construtor
		RealPosOwner( void ) : realPosition(), renderPriority( 0 ){};
	
		// Atualiza a posição do objeto na tela
		#if DEBUG
			virtual void updatePosition( void ){};
		#else
			virtual void updatePosition( void ) = 0;
		#endif
	
		// Determina uma prioridade de renderização para o objeto
		void setRenderPriority( uint8 priority );

		// Posição real do objeto
		Point3f realPosition;
	
	private:
		// Prioridade de renderização do objeto. Auxilia o algoritmo de ordenação caso dois objetos tenham exatamente a mesma posição
		uint8 renderPriority;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

inline void RealPosOwner::setRealPosition( const Point3f* pRealPosition )
{
	if( pRealPosition )
		RealPosOwner::setRealPosition( pRealPosition->x, pRealPosition->y, pRealPosition->z );
	else
		RealPosOwner::setRealPosition( 0.0f, 0.0f, 0.0f );
}

inline void RealPosOwner::moveRealPosition( float x, float y, float z )
{
	Point3f aux = *getRealPosition();
	RealPosOwner::setRealPosition( aux.x + x, aux.y + y, aux.z + z );
}

inline void RealPosOwner::setRenderPriority( uint8 priority )
{
	renderPriority = priority;
}

#endif
