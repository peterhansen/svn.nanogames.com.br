#ifndef ISO_KEEPER_H
#define ISO_KEEPER_H 1

#include "GenericIsoPlayer.h"
#include "Sprite.h"

#include "Tests.h"

// Define o número de sprites que compoem as animações do goleiro
#define GOAL_KEEPER_N_SPRITES 10

// Define o número de sprites de sombra que compoem as animações do goleiro
#define GOAL_KEEPER_SHADOW_N_SPRITES GOAL_KEEPER_N_SPRITES

typedef enum GoalKeeperState
{
	GOAL_KEEPER_UNDEFINED = -1,
	GOAL_KEEPER_STOPPED = 0,
	GOAL_KEEPER_PREPARING,
	GOAL_KEEPER_JUMPING_BOTTOM,
	GOAL_KEEPER_JUMPING_UP_GOING_UP,
	GOAL_KEEPER_JUMPING_UP_GOING_DOWN,
	GOAL_KEEPER_JUMPING_RIGHT,
	GOAL_KEEPER_JUMPING_RIGHT_LOW,
	GOAL_KEEPER_JUMPING_RIGHT_MED,
	GOAL_KEEPER_JUMPING_RIGHT_HIGH,
	GOAL_KEEPER_JUMPING_LEFT,
	GOAL_KEEPER_JUMPING_LEFT_LOW,
	GOAL_KEEPER_JUMPING_LEFT_MED,
	GOAL_KEEPER_JUMPING_LEFT_HIGH
	
} GoalKeeperState;

class IsoKeeper : public GenericIsoPlayer, public SpriteListener
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException e NanoBaseException
		IsoKeeper( void );

		// Destrutor
		virtual ~IsoKeeper( void );

		// Reinicializa o objeto
		virtual void reset( const Point3f* pPosition = NULL );

		// Reposiciona o goleiro, de acordo com a posição da falta e do número de jogadores na
		// barreira
		void prepare( const Point3f *pBallPosition, uint8 barrierPlayers );

		// Define a direção do pulo do goleiro, de acordo com a direção e força da bola e número
		// de jogadores na barreira
		void setJumpDirection( const Point3f *pDirection, float timeToGoal );

		// Getters e Setters dos atributos da classe
		inline Point3f getInitialPosition( void ) const;
		inline void setInitialPosition( Point3f initialPosition );

		inline Point3f getJumpSpeed( void ) const;
		inline void setLastJumpDirection( Point3f lastJumpDirection );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Define o estado do jogador
		void setState( GoalKeeperState s );

		// Retorna o estado do jogador
		inline GoalKeeperState getState( void ) const;
	
		// Chamado quando o sprite muda de etapa de animação
		virtual void onAnimStepChanged( Sprite* pSprite );
	
		#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) )
			// Renderiza o objeto
			virtual bool render( void );

			inline void setStateFrame( uint8 frame );
			inline uint8 getStateNFrames( void );
		#endif
	
		inline void setExpectedBallTarget(Point3f ballTargetPosition);
	protected:
		// Atualiza a posição do jogador na tela
		virtual void updatePosition( void );

		// Atualiza o frame do jogador de acordo com a direção para a qual está virado
		virtual void updateFrame( void );

	private:
		// Inicializa o objeto
		void build( void );
	
		// Limpa as variáveis do objeto
		void clear( void );
	
		// Aloca as imagens correspondentes ao pulo que o goleiro irá utilizar
		void allocAnim( int16 spriteIndex );
	
		// Deleta as imagens correspondentes ao pulo que o goleiro utilizou da última vez
		void freeCurrAnim( void );
	
		// Retorna o número de passos da animação em quais o goleiro está realmente pulando
		int16 getNJumpFrames( int16* firstJumpAnimStep, GoalKeeperState s );

		// Retorna o índice do sprite e o índice da animação que deveremos utilizar para
		// um dado estado
		int8 getSpriteIndexForState( int16* pAnimSeq, GoalKeeperState s );
	
		// Animações do goleiro
		Sprite* goalKeeper[ GOAL_KEEPER_N_SPRITES ];
	
		// Sombras do goleiro
		Sprite* shadow[ GOAL_KEEPER_SHADOW_N_SPRITES ];
	
		// Índice da sombra e do sprite que estamos utilizando
		int8 currSpriteIndex, jumpSpriteIndex;
	
		// Estado do jogador
		GoalKeeperState state, nextState;

		// Vetor velocidade do pulo
		Point3f jumpSpeed;
	
		// Posição inicial antes de mover-se
		Point3f initialPosition;
	
		Point3f expectedBallTarget;

		// Posição x inicial do pulo (no caso de bolas muito distantes, o goleiro pode
		// mover-se lateralmente um pouco antes do pulo)
		float jumpXPosition;	

		// Velocidade vertical instantânea do pulo
		float instantYSpeed;	

		// Tempo acumulado desde o início do pulo
		float accFrameTime, accJumpTime;
	
		// Tempo que o goleiro espera para iniciar o pulo
		float waitTime;
	
		// Duração que cada frame deverá ter para que completemos a animação de um estado
		// em jumpTime segundos
		float timePerFrame;
	
		// Indica se o goleiro tocou o chão novamente após o pulo
		bool returnedToFloor;
	
		// Tempo de duração do pulo
		float jumpTime;
		
		// Tempo de duração do atrito do goleiro com o chão após o pulo
		float floorTime;
	
		// Primeira etapa da animação atual na qual o goleiro sai do chão
		int16 firstJumpStep;
};

// Implementação dos métodos inline
inline GoalKeeperState IsoKeeper::getState( void ) const
{
	return nextState != GOAL_KEEPER_UNDEFINED ? nextState : state;
}

inline Point3f IsoKeeper::getInitialPosition( void ) const
{
	return initialPosition;
}

inline void IsoKeeper::setInitialPosition( Point3f initialPosition )
{
	this->initialPosition = initialPosition;
}

inline Point3f IsoKeeper::getJumpSpeed( void ) const
{
	return jumpSpeed;
}

inline void IsoKeeper::setLastJumpDirection( Point3f lastJumpDirection )
{
	jumpSpeed = lastJumpDirection;
}

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) )

inline void IsoKeeper::setStateFrame( uint8 frame )
{
	goalKeeper[ currSpriteIndex ]->setCurrentAnimSequenceStep( frame );
}

inline uint8 IsoKeeper::getStateNFrames( void )
{
	return goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceNSteps();
}

#endif
inline void IsoKeeper::setExpectedBallTarget(Point3f ballTargetPosition)
{
	expectedBallTarget=ballTargetPosition;
}

#endif
