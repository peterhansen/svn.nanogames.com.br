#include "IsoPlayer.h"

#include "Config.h"
#include "GameUtils.h"
#include "Iso_Defines.h"

#include "Exceptions.h"
#include "ObjcMacros.h"
#include "MirrorOp.h"

#include "FreeKickAppDelegate.h"

// Número de objetos no grupo
#define ISO_PLAYER_N_ITEMS ( PLAYER_N_SPRITES + PLAYER_SHADOW_N_SPRITES )

// Índices dos objetos contidos no grupo
#define PLAYER_FRAMES_SET_0		0
#define PLAYER_FRAMES_SET_1		1
#define PLAYER_FRAMES_SET_2		2

// Define o frame set do frame do chute
#define PLAYER_FRAMES_SET_KICK PLAYER_FRAMES_SET_1

// Define o frame exato do chute
#define PLAYER_ANIM_KICK_FRAME_BEFORE_KICK 1
#define PLAYER_ANIM_KICK_FRAME_KICK ( PLAYER_ANIM_KICK_FRAME_BEFORE_KICK + 1 )

// Define o offset da imagem dos sprites do jogador para que seu posicionamento fique correto de acordo com a posição da bola
#define PLAYER_OFFSET_X -45.0f
#define PLAYER_OFFSET_Y  18.0f

/*===============================================================================

CONSTRUTOR

=============================================================================== */

IsoPlayer::IsoPlayer( void )
		  : GenericIsoPlayer( ISO_PLAYER_N_ITEMS ),
			SpriteListener(),
			animationFrame( 0 ),
			firstAnimationFrame( 0 ),
			accAnimationTime( 0.0f ),
			state( PLAYER_UNDEFINED ),
			nextState( PLAYER_UNDEFINED ),
			mirrorOps( MIRROR_NONE ),
			currSpriteIndex( -1 ),
			pListener( NULL )
{
	// Inicializa os arrays
	memset( player, 0, sizeof( Sprite* ) * PLAYER_N_SPRITES );
	memset( shadow, 0, sizeof( Sprite* ) * PLAYER_SHADOW_N_SPRITES );
	
	if( !build() )
#if TARGET_IPHONE_SIMULATOR
		throw NanoBaseException( "IsoPlayer::IsoPlayer( GameInfo* ): nao foi possivel criar o objeto" );
#else
		throw NanoBaseException();
#endif
}

/*===============================================================================

DESTRUTOR

=============================================================================== */

IsoPlayer::~IsoPlayer( void )
{
	clear();
}

/*===============================================================================

MÉTODO build
	Inicializa o objeto.

=============================================================================== */

#define PLAYER_BUILD_BUFFER_SIZE 5

bool IsoPlayer::build( void )
{
	char preffix[3] = { 's', 'p', '\0' };
	char buffer[PLAYER_BUILD_BUFFER_SIZE];
	
	#if DEBUG
		char nameAuxBuffer[ OBJECT_NAME_MAX_LENGTH ];
	#endif

	for( uint8 i = 0 ; i < PLAYER_N_SPRITES ; ++i )
	{		
		snprintf( buffer, PLAYER_BUILD_BUFFER_SIZE, "%ss%d", preffix, static_cast< int32 >( i ) );
		
		#if CONFIG_TEXTURES_16_BIT
			shadow[i] = new Sprite( buffer, buffer, ResourceManager::Get16BitTexture2D );
		#else
			shadow[i] = new Sprite( buffer, buffer, ResourceManager::GetTexture2D );
		#endif
		if( !shadow[i] || ! insertObject( shadow[i] ) )
		{
			delete shadow[i];
			goto Error;
		}

		shadow[i]->setActive( false );
		shadow[i]->setVisible( false );
		
		#if DEBUG
			sprintf( nameAuxBuffer, "PlayerShadowSprite_%d", static_cast< int32 >( i ) );
			shadow[i]->setName( nameAuxBuffer );
		#endif
		
		snprintf( buffer, PLAYER_BUILD_BUFFER_SIZE, "%s%d", preffix, static_cast< int32 >( i ) );

		#if CONFIG_TEXTURES_16_BIT
			player[i] = new Sprite( buffer, buffer, ResourceManager::Get16BitTexture2D );
		#else
			player[i] = new Sprite( buffer, buffer, ResourceManager::GetTexture2D );
		#endif
		if( !player[i] || ! insertObject( player[i] ) )
		{
			delete player[i];
			goto Error;
		}
		
		player[i]->setActive( false );
		player[i]->setVisible( false );
		player[i]->setListener( this );

		#if DEBUG
			sprintf( nameAuxBuffer, "PlayerSprite_%d", static_cast< int32 >( i ) );
			player[i]->setName( nameAuxBuffer );
		#endif
	}

	// O tamanho do grupo deve ser igual ao tamanho do jogador
	setSize( player[0]->getWidth(), player[0]->getHeight() );

	return true;

	// Label de tratamento de erros
	Error:
		clear();
		return false;
}

#undef PLAYER_BUILD_BUFFER_SIZE

/*===============================================================================

MÉTODO clear
	Limpa as variáveis do objeto.

=============================================================================== */

void IsoPlayer::clear( void )
{
	state = PLAYER_UNDEFINED;
	nextState = PLAYER_UNDEFINED;
	mirrorOps = MIRROR_NONE;
	currSpriteIndex = -1;
	pListener = NULL;
	
	memset( player, 0, sizeof( Sprite* ) * PLAYER_N_SPRITES );
	memset( shadow, 0, sizeof( Sprite* ) * PLAYER_SHADOW_N_SPRITES );
}

/*===============================================================================

MÉTODO reset
	Reinicializa o objeto.

=============================================================================== */

void IsoPlayer::reset( const Point3f* pPosition )
{
	float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, REAL_PLAYER_HEIGHT, 0.0f ).y ) / player[0]->getStepFrameHeight() );
	setScale( scaleFactor, scaleFactor );

	setRealPosition( pPosition );
	
	setState( PLAYER_STOPPED );
	setBallCollisionTest( true );
}

/*===============================================================================

MÉTODO setState	
	Define o estado do jogador.

=============================================================================== */

void IsoPlayer::setState( PlayerState s )
{
	state = s;

	if( currSpriteIndex >= 0 )
	{
		shadow[ currSpriteIndex ]->setVisible( false );
		shadow[ currSpriteIndex ]->setAnimSequence( -1, false );
		
		player[ currSpriteIndex ]->setActive( false );
		player[ currSpriteIndex ]->setVisible( false );
		player[ currSpriteIndex ]->setAnimSequence( -1, false );
	}

	int16 animSeq = 0;
	switch( state )
	{	
		case PLAYER_RUNNING:
			animSeq = 1;
			currSpriteIndex = 0;
			nextState = PLAYER_SHOOTING;
			break;
			
		case PLAYER_SHOOTING:
			currSpriteIndex = 1;
			nextState = PLAYER_SHOT;
			break;

		case PLAYER_SHOT:
			currSpriteIndex = 2;
			nextState = PLAYER_STOPPED;
			break;
			
		case PLAYER_STOPPED:
			currSpriteIndex = 0;
			nextState = PLAYER_UNDEFINED;
			break;
			
#if DEBUG
		default:
			throw InvalidArgumentException( "IsoPlayer::setState( PlayerState ): Invalid state" );
			break;
#endif
	}

	shadow[ currSpriteIndex ]->setVisible( true );
	shadow[ currSpriteIndex ]->setAnimSequence( animSeq, false );
	
	player[ currSpriteIndex ]->setActive( true );
	player[ currSpriteIndex ]->setVisible( true );
	player[ currSpriteIndex ]->setAnimSequence( animSeq, false );
}

/*===============================================================================

MÉTODO updatePosition
   Função utilitária para converter a posição real do jogador para uma posição
na tela.

=============================================================================== */

void IsoPlayer::updatePosition( void )
{
	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	
	float offsetX = PLAYER_OFFSET_X - ( getWidth() * 0.5f );
	if( ( p.x > 0.0f ) && ( p.y > 0.0f ) )
	{
		if( !isMirrored( MIRROR_HOR ) )
			mirror( MIRROR_HOR );
		
		offsetX = -offsetX;
	}
	else if( isMirrored( MIRROR_HOR ) )
	{
		unmirror( MIRROR_HOR );
	}

	setPosition( p.x + offsetX, p.y - getHeight() + PLAYER_OFFSET_Y );
}

/*===============================================================================

MÉTODO onAnimStepChanged
	Chamado quando o sprite muda de etapa de animação.

================================================================================*/

void IsoPlayer::onAnimStepChanged( Sprite* pSprite )
{
	shadow[ currSpriteIndex ]->setCurrentAnimSequenceStep( player[ currSpriteIndex ]->getCurrAnimSequenceStep() );
	
	if( ( pListener != NULL ) && ( currSpriteIndex == PLAYER_FRAMES_SET_1 ) )
		pListener->onAnimStepChanged( NULL );
	
	// Se estamos no frame do chute, toca o som
	if( isOnKickAnimStep() )
		[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_KICK AtIndex: SOUND_INDEX_KICK Looping: false];
}

/*===============================================================================

MÉTODO onAnimEnded
	Chamado quando o sprite termina um sequência de animação.

================================================================================*/

void IsoPlayer::onAnimEnded( Sprite* pSprite )
{
	if( ( pListener != NULL ) && ( currSpriteIndex == PLAYER_FRAMES_SET_1 ) )
		pListener->onAnimEnded( NULL );

	if( nextState != PLAYER_UNDEFINED )
		setState( nextState );
}

/*===============================================================================

MÉTODO onAnimLooped
	Chamado quando o sprite reinicia automaticamente uma sequência de animação.

================================================================================*/

void IsoPlayer::onAnimLooped( Sprite* pSprite )
{
	if( ( pListener != NULL ) && ( currSpriteIndex == PLAYER_FRAMES_SET_1 ) )
		pListener->onAnimLooped( NULL );
}

/*===============================================================================

MÉTODO isOnKickStep
	Retorna se o jogador está no frame do chute em sua animação de chutar a bola.

================================================================================*/

bool IsoPlayer::isOnKickAnimStep( void )
{
	return ( currSpriteIndex == PLAYER_FRAMES_SET_KICK ) && ( player[ currSpriteIndex ]->getCurrAnimSequenceStep() == PLAYER_ANIM_KICK_FRAME_KICK );
}

/*===============================================================================

MÉTODO isOnKickStep
	Retorna se o jogador está no frame anterior ao frame do chute em sua animação
de chutar a bola.

================================================================================*/

bool IsoPlayer::isOnBeforeKickAnimStep( void )
{
	return ( currSpriteIndex == PLAYER_FRAMES_SET_1 ) && ( player[ currSpriteIndex ]->getCurrAnimSequenceStep() == PLAYER_ANIM_KICK_FRAME_BEFORE_KICK );
}

/*===============================================================================

MÉTODO update
	Atualiza o objeto.

================================================================================*/

#if DEBUG && IS_CURR_TEST( TEST_PLAYER_KICK )

	bool IsoPlayer::update( float timeElapsed )
	{
		return ObjectGroup::update( timeElapsed );
	}

#endif

/*==============================================================================================

MÉTODO mirror
	Determina as operações de espelhamento que devem ser aplicadas sobre o sprite.

==============================================================================================*/

void IsoPlayer::mirror( MirrorOp ops )
{
	bool wasntMirroredHor = !isMirrored( MIRROR_HOR );
	bool wasntMirroredVer = !isMirrored( MIRROR_VER );
	
	mirrorOps |= ops;
	
	if( wasntMirroredHor && isMirrored( MIRROR_HOR ) )
		rotate( 180.0f, 0.0f, 1.0f, 0.0f );
		
	if( wasntMirroredVer && isMirrored( MIRROR_VER ) )
		rotate( 180.0f, 1.0f, 0.0f, 0.0f );
}

/*==============================================================================================

MÉTODO isMirrored
	Indica se uma determinada operação de espelhamento está aplicada sobre o sprite.

==============================================================================================*/

bool IsoPlayer::isMirrored( MirrorOp op )
{
	return ( mirrorOps & op ) != 0;
}

/*==============================================================================================

MÉTODO unmirror
	Cancela operações de espelhamento.

==============================================================================================*/

void IsoPlayer::unmirror( MirrorOp ops )
{
	bool wasMirroredHor = isMirrored( MIRROR_HOR );
	bool wasMirroredVer = isMirrored( MIRROR_VER );

	mirrorOps &= ~ops;
	
	if( wasMirroredHor && !isMirrored( MIRROR_HOR ) )
		rotate( 180.0f, 0.0f, 1.0f, 0.0f );
		
	if( wasMirroredVer && !isMirrored( MIRROR_VER ) )
		rotate( 180.0f, 1.0f, 0.0f, 0.0f );
}

/*==============================================================================================

MÉTODO prepare
	Indica que o jogador deve se posicionar de acordo com a posição da bola.

==============================================================================================*/

void IsoPlayer::prepare( const Point3f& ballRealPos )
{
	setRealPosition( &ballRealPos );
}

#if DEBUG 

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool IsoPlayer::render( void )
{
	return GenericIsoPlayer::render();
}

#endif

