#include "CollisionQuad.h"

#if DEBUG
	#include "Macros.h"
#endif

// Tolerância mínima para deteção de colisão
#define MIN_TOLERANCE 0.01f

/*===============================================================================

CONSTRUTOR

=============================================================================== */

CollisionQuad::CollisionQuad( void ) : top(), right(), normal(), position(), center(), d( 0.0f ), diagonal( 0.0f ), width( 0.0f ), height( 0.0f )
{
}

/*===============================================================================

CONSTRUTOR

=============================================================================== */

CollisionQuad::CollisionQuad( Point3f pos, Point3f top, Point3f right, ORIENTATION orientation, float width, float height )
			  : top( top.normalize() ),
				right( right.normalize() ),
				normal( ( right % top ).normalize() ),
				position(),
				center(),
				d( 0.0f ),
				diagonal( static_cast< float >( sqrt(( width * width ) + ( height * height )) ) ),
				width( width ),
				height( height )
{	
	// Determina center e position
	setPosByOrientation( pos, orientation );

	// Para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) é um ponto qualquer pertencente a esse plano
	d = ( normal * -1.0f ) * position;
}

/*===============================================================================

CONSTRUTOR

=============================================================================== */

CollisionQuad::CollisionQuad( Point3f bottomLeft, Point3f topLeft, Point3f bottomRight )
			  : top(),
				right(),
				normal(),
				position(),
				center(),
				d( 0.0f ),
				diagonal( 0.0f ),
				width( 0.0f ),
				height( 0.0f )
{
	set( bottomLeft, topLeft, bottomRight );
}

/*===============================================================================

MÉTODO set
   Inicializa o objeto.

=============================================================================== */

void CollisionQuad::set( Point3f bottomLeft, Point3f topLeft, Point3f bottomRight )
{
	top = topLeft - bottomLeft;
	height = top.getModule();
	top.normalize();

	right = ( bottomRight - bottomLeft );
	width = right.getModule();
	right.normalize();

	normal = right % top;
	normal.normalize();

	position = topLeft;
	center = topLeft + right * ( width * 0.5f ) - top * ( height * 0.5f );

	diagonal = static_cast< float >( sqrt(( width * width ) + ( height * height )) );

	// Para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) é um ponto qualquer pertencente a esse plano
	d = ( normal * -1.0f ) * position;
}

/*===============================================================================

MÉTODO set
   Inicializa o objeto.

=============================================================================== */

void CollisionQuad::set( Point3f pos, Point3f myTop, Point3f myRight, ORIENTATION orientation, float myWidth, float myHeight )
{
	top = myTop;
	right = myRight;
	width = myWidth;
	height = myHeight;

	top.normalize();
	right.normalize();
	normal = right % top;
	normal.normalize();

	diagonal = static_cast< float >( sqrt(( width * width ) + ( height * height )) );

	// Determina center e position
	setPosByOrientation( pos, orientation );

	// Para obter a coordenada d: d = -(a,b,c) dot (x1,y1,z1), onde (x1,y1,z1) é um ponto qualquer pertencente a esse plano.
	d = ( normal * -1.0f ) * position;
}

/*===============================================================================

MÉTODO distanceTo
	Retorna a distância do ponto ao plano do quad.

=============================================================================== */

float CollisionQuad::distanceTo( const Point3f *pPoint ) const
{
	return normal * ( *pPoint - center );
}

/*===============================================================================

MÉTODO isInsideQuad
	Indica se o ponto está dentro dos limites de altura/largura do quad (não testa
se o ponto pertence ao plano).

=============================================================================== */

bool CollisionQuad::isInsideQuad( const Point3f* pPoint, float tolerance ) const
{
	if( tolerance < MIN_TOLERANCE )
		tolerance = MIN_TOLERANCE;

	// primeiro teste: se ponto estiver numa distância em relação ao centro do quad maior que a
	// metade da diagonal do mesmo, não pode estar colidindo
	Point3f distance = *pPoint - center;
	if( distance.getModule() > ( ( diagonal * 0.5f ) + tolerance ) )
		return false;

	// colide com a esfera ao redor do centro do Quad; agora verifica-se se o ponto colide com o
	// Quad em si. Para isso, projeta-se o ponto nos vetores top e right do Quad. Caso o módulo
	// das projeções sejam menores que a altura e largura do Quad, há colisão. Como os módulos
	// de top e right são, respectivamente, height e width, não há necessidade de calculas seus
	// módulos.
	// cálculo da projeção de um vetor sobre outro: proj(B,A) = ( (A.B)/|A|≤ ) * A
			
	// OLD
	//Point3f projTop = top * ( top * distance / ( height * height ) );

	Point3f projTop = top * ( top * distance );		

	if( projTop.getModule() < ( height * 0.5f ) + tolerance )
	{
		// OLD
		//Point3f projRight = right * ( right * distance / ( width  * width  ) );
		
		Point3f projRight = right * ( right * distance );

		if( projRight.getModule() < ( width * 0.5f ) + tolerance )
			return true;
	}
	return false;
}

/*===============================================================================

MÉTODO setPosByOrientation
   Ajusta a posição do quad de acordo com a orientação recebida. Internamente,
a orientação será sempre TOP_LEFT.

=============================================================================== */

void CollisionQuad::setPosByOrientation( Point3f pos, ORIENTATION orientation )
{
	Point3f r = right.getNormalized() * width, t = top.getNormalized() * height;

	switch( orientation )
	{
		case ORIENT_TOP_LEFT:
			center = pos + ( r * 0.5f ) - ( t * 0.5f );
			position = pos;
			break;

		case ORIENT_TOP:
			center = pos - ( t * 0.5f );
			position = pos - ( r * 0.5f );
			break;

		case ORIENT_TOP_RIGHT:
			center = pos - ( r * 0.5f ) - ( t * 0.5f );
			position = pos - r;
			break;

		case ORIENT_RIGHT:
			center = pos - ( r * 0.5f );
			position = pos - r + ( t * 0.5f );
			break;

		case ORIENT_BOTTOM_RIGHT:
			center = pos - ( r * 0.5f ) + ( t * 0.5f );
			position = pos - r + t;
			break;

		case ORIENT_BOTTOM:
			center = pos + ( t * 0.5f );
			position = pos - ( r * 0.5f ) + t;
			break;

		case ORIENT_BOTTOM_LEFT:
			center = pos + ( r * 0.5f ) + ( t * 0.5f );
			position = pos + t;
			break;

		case ORIENT_LEFT:
			center = pos + ( r * 0.5f );
			position = pos + ( t * 0.5f );
			break;

		case ORIENT_CENTER:
			center = pos;
			position = pos - ( r * 0.5f ) + ( t * 0.5f );
			break;
	}
}

/*===============================================================================

MÉTODO print
	Imprime as informações do quadrado de colisão.

=============================================================================== */

#if DEBUG

void CollisionQuad::print( void ) const
{
	LOG( "PositionX = %.3f, PositionY = %.3f, PositionZ = %.3f, D = %.3f, Diagonal = %.3f, Width = %.3f, Height = %.3f\n", position.x, position.y, position.z, d, diagonal, width, height );	
}

#endif

