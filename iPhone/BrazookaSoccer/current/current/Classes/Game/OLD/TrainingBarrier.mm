#include "TrainingBarrier.h"

#include "Ball_Defines.h"
#include "GameUtils.h"

#include "Exceptions.h"
#include "MirrorOp.h"
#include "Point2i.h"
#include "Sprite.h"

// Número de objetos no grupo
#warning Só utiliza 1 objeto apesar de declarar que o grupo possui 2 objetos
#define TRAINING_BARRIER_N_ITEMS 2

// Número de "jogadores" na placa da barreira no modo treino
#define TRAINING_BARRIER_N_PLAYERS 4

// Largura total da barreira de treino
#define TRAINING_BARRIER_WIDTH ( BARRIER_PLAYERS_DISTANCE * TRAINING_BARRIER_N_PLAYERS )

/*===============================================================================

CONSTRUTOR

=============================================================================== */

TrainingBarrier::TrainingBarrier( void ) // TODOO : Barrier ( TRAINING_BARRIER_N_ITEMS )
{
	if( !build() )
#if TARGET_IPHONE_SIMULATOR
		throw NanoBaseException( "TrainingBarrier::TrainingBarrier( GameInfo* ): nao foi possivel criar o objeto" );
#else
		throw NanoBaseException();
#endif

	numberOfPlayers = TRAINING_BARRIER_N_FRAMES;

	// OLD
//	int16 tX[ TRAINING_BARRIER_N_FRAMES ] = { TRAINING_BARRIER_X_OFFSET },
//		  tY[ TRAINING_BARRIER_N_FRAMES ] = { TRAINING_BARRIER_Y_OFFSET };
//
//	for( uint8 i = 0; i < TRAINING_BARRIER_N_FRAMES; ++i )
//	{
//		offsetX[ i ] = tX[ i ];
//		offsetY[ i ] = tY[ i ];
//	}
}

/*===============================================================================

MÉTODO build
	Inicializa o objeto.

================================================================================*/

bool TrainingBarrier::build( void )
{
	return true;
	// TODOO
//	Sprite* pImage = new Sprite();
//	if( !pImage )
//		return false;
//
//	return insertObject( pImage );
}

/*===============================================================================

MÉTODO prepare
	Posiciona a barreira em relação à posição da bola.

================================================================================*/

void TrainingBarrier::prepare( const Point3f* pBallPosition )
{
	Point3f postPosition;

	if( pBallPosition->x < 0.0f )
	{
		// A bola está mais próxima da trave esquerda
		postPosition.set( -REAL_GOAL_WIDTH * 0.5f );
	}
	else
	{
		// A bola está mais próxima da trave direita
		postPosition.set( REAL_GOAL_WIDTH * 0.5f );
	}

	Point3f postToBallVector = postPosition - *pBallPosition;

	// define a posição do jogador-base da barreira (do jogador mais à esquerda)
	Point3f basePosition = *pBallPosition + ( postToBallVector.getNormalized() * BARRIER_DISTANCE_TO_BALL );

	// define a posição geral da barreira como a posição do seu jogador central, para que
	// após o lookAt() todos estejam virados para a bola.
	realPosition = basePosition;					   
	lookAt( pBallPosition );

	// define a área de colisão da barreira
	Point3f topVector = Point3f( 0.0f, 1.0f );
	Point3f rightVector;

	switch( direction )
	{
		case DIRECTION_RIGHT:
			rightVector.set( 0.0f, 0.0f, 1.0f );
			break;

		case DIRECTION_DOWN_RIGHT:
			rightVector.set( -0.70710678f, 0.0f, 0.70710678f );
			break;

		case DIRECTION_DOWN:
			rightVector.set( 1.0f );
			break;

		case DIRECTION_DOWN_LEFT:
			rightVector.set( -0.70710678f, 0.0f, -0.70710678f );
			break;

		case DIRECTION_LEFT:
			rightVector.set( 0.0f, 0.0f, 1.0f );
			break;
	}
	collisionArea.set( basePosition, topVector, rightVector, ORIENT_BOTTOM, TRAINING_BARRIER_WIDTH, REAL_BARRIER_HEIGHT );

	setBallCollisionTest( true );

	updateFrame();
	setVisible( true );
}

/*===============================================================================

MÉTODO updateFrame
	Atualiza o frame atual dos jogadores, de acordo com a direção para a qual
estão virados.

================================================================================*/

void TrainingBarrier::updateFrame( void )
{
	uint8 frameIndex;
	MirrorOp mirror = MIRROR_NONE;

	switch( direction )
	{
		case DIRECTION_RIGHT:
			frameIndex = 3;
			mirror = MIRROR_HOR;
			break;
		
		case DIRECTION_DOWN_RIGHT:
		case DIRECTION_DOWN:
		case DIRECTION_DOWN_LEFT:
		case DIRECTION_LEFT:
			frameIndex = direction;
			break;

		default:
			return;
	}

	Sprite* pBarrier = ( Sprite* )getObject( 0 );
	pBarrier->setCurrentAnimSequenceStep( frameIndex );
	pBarrier->mirror( mirror );

	Point3f p = GameUtils::isoGlobalToPixels( realPosition );

	// OLD
	//setPosition( p.x + offsetX[ currentFrame ], p.y + offsetY[ currentFrame ] );

	setPosition( p.x, p.y );
}

