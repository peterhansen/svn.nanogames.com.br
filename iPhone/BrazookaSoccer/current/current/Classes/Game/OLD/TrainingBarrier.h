/*
 *  TrainingBarrier.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/5/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TRAINING_BARRIER_H
#define TRAINING_BARRIER_H

#include "Barrier.h"

// Número de frames da imagem da barreira de treino
#define TRAINING_BARRIER_N_FRAMES 4

class TrainingBarrier : public Barrier
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException e NanoBaseException
		TrainingBarrier( void );
	
		// Destrutor
		virtual ~TrainingBarrier( void ){};
	
		// Posiciona a barreira em relação à posição da bola
		virtual void prepare( const Point3f* pBallPosition );
	
		// Atualiza o frame atual dos jogadores, de acordo com a direção para a qual estão virados
		virtual void updateFrame( void );
	
	private:
		// Inicializa o objeto
		bool build( void );
};

#endif
