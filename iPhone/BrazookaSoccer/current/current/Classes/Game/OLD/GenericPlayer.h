/*
 *  GenericPlayer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GENERIC_PLAYER_H
#define GENERIC_PLAYER_H

#include "ObjectGroup.h"
#include "Point3f.h"

#include "CollisionQuad.h"
#include "RealPosOwner.h"

class GenericPlayer : public RealPosOwner, public ObjectGroup
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException
		explicit GenericPlayer( uint16 maxObjects );
	
		// Destrutor
		virtual ~GenericPlayer( void );

		// Retorna o quad que define a área de colisão do jogador
		const CollisionQuad* getCollisionArea( void ) const;

		// Define se o teste de colisão com a bola deve ser feito
		void setBallCollisionTest( bool checkCollision );

		// Retorna se o teste de colisão com a bola está sendo feito
		bool getBallCollisionTest( void ) const;
	
//		// Indica que o jogador deve se posicionar de acordo com a posição da bola
//		virtual void prepare( const Point3f& ballRealPos ) = 0;
//
//		// Testa colisão deste objeto com a área passada como parâmetro
//		virtual bool checkCollision( const CollisionQuad& area ) = 0;

	protected:
		// Retângulo que define a área de colisão no espaço 3d
		CollisionQuad collisionArea; 

		// Indica se o teste de colisão com a bola deve ser feito
		bool checkBallCollision; 
};

// Implementação dos métodos inline

inline const CollisionQuad* GenericPlayer::getCollisionArea( void ) const
{
	return &collisionArea;
}

inline void GenericPlayer::setBallCollisionTest( bool checkCollision )
{
	checkBallCollision = checkCollision;
}

inline bool GenericPlayer::getBallCollisionTest( void ) const
{
	return checkBallCollision;
}

#endif
