	/*
 *  OGLTransition.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OGLTRANSITION_H
#define OGLTRANSITION_H 1

#include "Renderable.h"
#include "Updatable.h"

class OGLTransitionListener
{
	public:
		// Destrutor
		virtual ~OGLTransitionListener( void ){};

		// Método chamado quando a transição termina
		virtual void onOGLTransitionEnd( void ) = 0;
};

class OGLTransition : public Renderable, public Updatable
{
	public:
		// Construtor
		OGLTransition( OGLTransitionListener* pListener = NULL );

		// Destrutor
		virtual ~OGLTransition( void );

		// Reinicializa o objeto
		virtual void reset( void ) = 0;

	protected:
		// Objeto que irá receber os eventos da transição
		OGLTransitionListener* pListener;
};

#endif
