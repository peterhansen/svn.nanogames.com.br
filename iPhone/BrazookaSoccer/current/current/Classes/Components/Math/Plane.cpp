#include "Plane.h"

#include "MathFuncs.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Plane::Plane( void ) : right(), top(), normal(), d( 0.0f )
{
}

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

Plane::Plane( const Point3f* pTop, const Point3f* pRight, float d ) : right(), top(), normal(), d( 0.0f )
{
	set( pTop, pRight, d );
}
/*===========================================================================================

MÉTODO set
	Inicializa o objeto.

============================================================================================*/

void Plane::set( const Point3f* pTop, const Point3f* pRight, float d )
{
	if( pTop )
		top = *pTop;
	else
		top.set( 0.0f, 1.0f, 0.0f );

	if( pRight )
		right = *pRight;
	else
		right.set( 1.0f );

	this->d = d;
	normal = right % top;

	normal.normalize();
}

/*===========================================================================================

OPERADOR ==

============================================================================================*/

bool Plane::operator == ( const Plane& p ) const
{
	return	( right == p.right )
			&& ( top == p.top )
			&& ( normal == p.normal )
			&& ( NanoMath::fcmp( d, p.d ) == 0 );
}

/*===========================================================================================

OPERADOR !=

============================================================================================*/

bool Plane::operator != ( const Plane& p ) const
{
	return	( right != p.right )
			|| ( top != p.top )
			|| ( normal != p.normal )
			|| ( NanoMath::fcmp( d, p.d ) != 0 );
}
