/*
 *  Box.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 3/10/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BOX_H
#define BOX_H 1

struct Box
{
	// Construtores
	Box( void );
	Box( float x, float y, float z, float width, float height, float depth );

	// Inicializa o objeto
	void set( float x = 0.0f, float y = 0.0f, float z = 0.0f, float width = 0.0f, float height = 0.0f, float depth = 0.0f );
	
	// Posição do paralelepípedo
	float x, y, z;

	// Dimensões do paralelepípedo
	float width, height, depth;
};

#endif
