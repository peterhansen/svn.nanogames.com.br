/*
 *  GLHeaders.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GL_HEADERS_H
#define GL_HEADERS_H 1

#include <OpenGLES/EAGL.h>
#include <OpenGLES/ES1/gl.h>
#include <OpenGLES/ES1/glext.h>

#endif
