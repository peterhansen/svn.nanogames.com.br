#include "DeviceInterface.h"
#include "ObjcMacros.h"

#include <UIKit/UIDevice.h>

/*==============================================================================================

MÉTODO GetDeviceUID
	A string unique to each device based on various hardware details.

===============================================================================================*/

std::string& DeviceInterface::GetDeviceUID( std::string& str, bool append )
{
	#if DEBUG
		LOG( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] uniqueIdentifier] ) );
	#endif

	if( append )
		str.append( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] uniqueIdentifier] ) );
	else
		str = NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] uniqueIdentifier] );

	return str;
}

/*==============================================================================================

MÉTODO GetDeviceName
	The name identifying the device.

===============================================================================================*/

std::string& DeviceInterface::GetDeviceName( std::string& str, bool append )
{
	#if DEBUG
		LOG( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] name] ) );
	#endif

	if( append )
		str.append( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] name] ) );
	else
		str = NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] name] );

	return str;
}

/*==============================================================================================

MÉTODO GetDeviceSystemName
	The name of the operating system running on the device represented by the receiver.

===============================================================================================*/

std::string& DeviceInterface::GetDeviceSystemName( std::string& str, bool append )
{
	#if DEBUG
		LOG( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] systemName] ) );
	#endif
	
	if( append )
		str.append( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] systemName] ) );
	else
		str = NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] systemName] );

	return str;
}

/*==============================================================================================

MÉTODO GetDeviceSystemVersion
	The current version of the operating system.

===============================================================================================*/

std::string& DeviceInterface::GetDeviceSystemVersion( std::string& str, bool append )
{
	#if DEBUG
		LOG( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] systemVersion] ) );
	#endif
	
	if( append )
		str.append( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] systemVersion] ) );
	else
		str = NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] systemVersion] );

	return str;
}

/*==============================================================================================

MÉTODO GetDeviceModel
	The model of the device.

===============================================================================================*/

std::string& DeviceInterface::GetDeviceModel( std::string& str, bool append )
{
	#if DEBUG
		LOG( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] model] ) );
	#endif
	
	if( append )
		str.append( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] model] ) );
	else
		str = NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] model] );

	return str;
}

/*==============================================================================================

MÉTODO GetDeviceLocalizedModel
	The model of the device as a localized string.

===============================================================================================*/

std::string& DeviceInterface::GetDeviceLocalizedModel( std::string& str, bool append )
{
	#if DEBUG
		LOG( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] localizedModel] ) );
	#endif
	
	if( append )
		str.append( NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] localizedModel] ) );
	else
		str = NSSTRING_TO_CHAR_ARRAY( [[UIDevice currentDevice] localizedModel] );
	
	return str;
}
