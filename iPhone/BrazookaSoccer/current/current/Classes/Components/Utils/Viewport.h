/*
 *  Viewport.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/5/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef VIEWPORT_H
#define VIEWPORT_H 1

#include "NanoTypes.h"

struct Viewport
{
	// Construtor
	Viewport( void );
	Viewport( int32 x, int32 y, int32 width, int32 height );
	
	// Aplica o viewport levando em consideração a orientação do device
	void apply( void ) const;
	
	// Retorna a interseção deste viewport com o viewport passado como parâmetro
	Viewport& getIntersection( Viewport& ret, const Viewport& other ) const;
	
	// Inicializa o objeto
	void set( int32 x, int32 y, int32 width, int32 height );
	
	// Operadores de comparação
	bool operator == ( const Viewport& other ) const;
	bool operator != ( const Viewport& other ) const;

	// Dados
	union
	{
		struct
		{
			int32 x, y, width, height;
		};
		int32 viewport[4];
	};
};

#endif
