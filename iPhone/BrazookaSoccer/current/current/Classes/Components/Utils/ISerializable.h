/*
 *  ISerializable.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/10/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ISERIALIZABLE_H
#define ISERIALIZABLE_H 1

#include "MemoryStream.h"

class ISerializable
{
	public:
		// Destrutor
		virtual ~ISerializable( void ){};

		// Lê o objeto de uma stream
		virtual void serialize( MemoryStream& stream ) const = 0;

		// Escre o objeto em uma stream 
		virtual void unserialize( MemoryStream& stream ) = 0;
};

#endif
