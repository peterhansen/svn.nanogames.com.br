/*
 *  TextAlignment.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/29/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEXT_ALIGNMENT_H
#define TEXT_ALIGNMENT_H 1

#define ALIGN_HOR_LEFT   0x01
#define ALIGN_HOR_RIGHT  0x02
#define ALIGN_HOR_CENTER 0x04

#define ALIGN_VER_TOP    0x10
#define ALIGN_VER_BOTTOM 0x20
#define ALIGN_VER_CENTER 0x40

typedef uint8 TextAlignment;

#endif
