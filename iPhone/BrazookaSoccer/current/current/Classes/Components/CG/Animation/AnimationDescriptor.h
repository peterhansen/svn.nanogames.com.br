/*
 *  AnimationDescriptor.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/12/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ANIMATION_DESCRIPTOR_H
#define ANIMATION_DESCRIPTOR_H 1

#include "AnimSequence.h"

class AnimationDescriptor
{
	public:
		// Construtores
		AnimationDescriptor( void );
		AnimationDescriptor( const AnimationDescriptor& ref );
	
		// Destrutor
		virtual ~AnimationDescriptor( void );
	
		// Obtém as informações da textura através da leitura de um arquivo descritor
		bool readFromFile( const char* pFileName );
	
		// Obtém as informações da textura através do parser de arquivos
		bool readFromScanner( const NSScanner* hScanner );
	
		// Retorna o número de sequências de animação existentes no descritor
		inline uint16 getNAnimSequences( void ) const { return nSequences; };
	
		// Retorna a sequência de animação desejada
		inline const AnimSequence* getAnimSequence( uint16 sequence ) const { return sequence < nSequences ? &pSequences[sequence] : NULL; };
	
		// Operador de atribuição
		AnimationDescriptor& operator=( const AnimationDescriptor& ref );

	private:
		// Número de sequências de animação
		uint16 nSequences;

		// Sequências de animação
		AnimSequence* pSequences;

		// Lê do arquivo uma sequência de tempo constante
		bool scanFixedSequence( AnimSequence* pSequence, const NSString* hSequence );

		// Lê do arquivo uma sequência de tempo variável
		bool scanVariableSequence( AnimSequence* pSequence, const NSString* hSequence );
	
		// Libera os recursos alocados pelo objeto
		void clean( void );
};

#endif
