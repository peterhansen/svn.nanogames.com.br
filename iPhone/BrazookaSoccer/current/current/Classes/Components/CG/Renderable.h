/*
 *  Renderable.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef RENDERABLE_H
#define RENDERABLE_H 1

class Renderable
{
	public:
		// Construtor
		Renderable( bool visible = true );

		// Destrutor
		virtual ~Renderable( void ){};
	
		// Renderiza o objeto
		virtual bool render( void ){ return isVisible(); };
	
		// Determina se o objeto está visível
		virtual void setVisible( bool b ){ visible = b; };
	
		// Retorna se o objeto está sofrendo renderizações
		inline bool isVisible( void ) const { return visible; };

	private:
		// Indica se o objeto está visível
		bool visible;
};

#endif

