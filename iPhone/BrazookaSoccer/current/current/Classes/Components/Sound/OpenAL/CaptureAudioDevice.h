/*
 *  CaptureAudioDevice.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 12/15/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef CAPTURE_AUDIO_DEVICE_H
#define CAPTURE_AUDIO_DEVICE_H 1

#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include "AudioDevice.h"
#include "NanoTypes.h"

// Define o tempo de captura padrão
#define DEFAULT_CAPTURE_TIME 1

class CaptureAudioDevice : public AudioDevice
{
	public:
		// Construtor
		CaptureAudioDevice( uint8 maxCaptureTime = DEFAULT_CAPTURE_TIME );
		
		//Destrutor
		virtual ~CaptureAudioDevice( void );
	
		// Pára a captura de dados
		virtual bool suspend( void ) const;
	
		// Inicia a captura de dados
		virtual bool resume( void ) const;
	
		// Obtém os dados capturados. Retorna NULL em casos de erro
		uint8* getCapturedSamples( int32* pNSamples );

	private:
		// Buffer para armazenar os dados capturados
		uint8* pCaptureDataBuffer;
};

#endif COMPONENTS_CONFIG_ENABLE_OPENAL

#endif CAPTURE_AUDIO_DEVICE_H
