#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include "AudioManager.h"
#include "Macros.h"
#include "Exceptions.h"

// Inicializa as variáveis estáticas da classe
AudioManager* AudioManager::pSingleton = NULL;

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AudioManager::AudioManager( void ) : pPlaybackDevice( NULL ), pCaptureDevice( NULL )
{
	pPlaybackDevice = new PlaybackAudioDevice();

	if( supportsCapture() )
	{
		try
		{
			pCaptureDevice = new CaptureAudioDevice();
		}
		catch( ... )
		{
			DESTROY( pPlaybackDevice );
			throw;
		}
	}
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

AudioManager::~AudioManager( void )
{
	SAFE_DELETE( pPlaybackDevice );
	SAFE_DELETE( pCaptureDevice );
}

/*==============================================================================================

MÉTODO Create
	 Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance().
No entanto, isso poderia resultar em exceções disparadas no meio do código da aplicação, o que
impactaria o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
chamado no início da aplicação, já que o usuário dos componentes parte do princípio que sempre
GetInstance() sempre retornará um valor válido.

==============================================================================================*/
 
bool AudioManager::Create( void )
{
	if( !pSingleton )
		pSingleton = new AudioManager();
	return pSingleton != NULL;
}

/*==============================================================================================

MÉTODO Destroy
	Deleta o singleton.

==============================================================================================*/

void AudioManager::Destroy( void )
{
	SAFE_DELETE( pSingleton );
}

/*==============================================================================================

MÉTODO supportsCapture
	Indica se a plataforma suporta captura de áudio.

==============================================================================================*/

bool AudioManager::supportsCapture( void )
{
	if( alcIsExtensionPresent( NULL, "ALC_EXT_CAPTURE" ) == ALC_TRUE )
	{
		const ALCchar* pDevicesList = alcGetString( NULL, ALC_CAPTURE_DEVICE_SPECIFIER );
		return pDevicesList[0] != '\0';
	}
	return false;
}

/*==============================================================================================

MÉTODO supportsReverbAndEffects
	Indica se a plataforma suporta efeitos sonoros.

==============================================================================================*/

bool AudioManager::supportsReverbAndEffects( void )
{
	return alcIsExtensionPresent( NULL, "ALC_EXT_ASA" ) == ALC_TRUE;
}

/*==============================================================================================

MÉTODO suspend
	Suspende a execução dos devices existentes.

==============================================================================================*/

bool AudioManager::suspend( void ) const
{
	bool ret = true;
	ret &= pPlaybackDevice->suspend();
	if( pCaptureDevice )
		ret &= pCaptureDevice->suspend();
	return ret;
}

/*==============================================================================================

MÉTODO resume
	Resume a execução dos devices existentes.

==============================================================================================*/

bool AudioManager::resume( void ) const
{
	bool ret = true;
	ret &= pPlaybackDevice->resume();
	if( pCaptureDevice )
		ret &= pCaptureDevice->resume();
	return ret;
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Extension API Procs
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef ALvoid	AL_APIENTRY	(*alMacOSXRenderChannelCountProcPtr) (const ALint value);
//ALvoid  alMacOSXRenderChannelCountProc(const ALint value)
//{
//	static	alMacOSXRenderChannelCountProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alMacOSXRenderChannelCountProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alMacOSXRenderChannelCount");
//    }
//    
//    if(proc)
//        proc(value);
//
//    return;
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef ALvoid	AL_APIENTRY	(*alcMacOSXRenderingQualityProcPtr) (const ALint value);
//ALvoid  alcMacOSXRenderingQualityProc(const ALint value)
//{
//	static	alcMacOSXRenderingQualityProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alcMacOSXRenderingQualityProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alcMacOSXRenderingQuality");
//    }
//    
//    if(proc)
//        proc(value);
//
//    return;
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef ALvoid	AL_APIENTRY	(*alcMacOSXMixerOutputRateProcPtr) (const ALdouble value);
//ALvoid  alcMacOSXMixerOutputRateProc(const ALdouble value)
//{
//	static	alcMacOSXMixerOutputRateProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alcMacOSXMixerOutputRateProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alcMacOSXMixerOutputRate");
//    }
//    
//    if(proc)
//        proc(value);
//
//    return;
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef ALdouble (*alcMacOSXGetMixerOutputRateProcPtr) ();
//ALdouble  alcMacOSXGetMixerOutputRateProc()
//{
//	static	alcMacOSXGetMixerOutputRateProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alcMacOSXGetMixerOutputRateProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alcMacOSXGetMixerOutputRate");
//    }
//    
//    if(proc)
//        return proc();
//
//    return 0.0;
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef ALvoid	AL_APIENTRY	(*alBufferDataStaticProcPtr) (const ALint bid, ALenum format, ALvoid* data, ALsizei size, ALsizei freq);
//ALvoid  alBufferDataStaticProc(const ALint bid, ALenum format, ALvoid* data, ALsizei size, ALsizei freq)
//{
//	static	alBufferDataStaticProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alBufferDataStaticProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alBufferDataStatic");
//    }
//    
//    if(proc)
//        proc(bid, format, data, size, freq);
//
//    return;
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef OSStatus	(*alcASASetSourceProcPtr)	(const ALuint property, ALuint source, ALvoid *data, ALuint dataSize);
//OSStatus  alcASASetSourceProc(const ALuint property, ALuint source, ALvoid *data, ALuint dataSize)
//{
//    OSStatus	err = noErr;
//	static	alcASASetSourceProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alcASASetSourceProcPtr) alcGetProcAddress(NULL, (const ALCchar*) "alcASASetSource");
//    }
//    
//    if(proc)
//        err = proc(property, source, data, dataSize);
//    return (err);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef OSStatus	(*alcASASetListenerProcPtr)	(const ALuint property, ALvoid *data, ALuint dataSize);
//OSStatus  alcASASetListenerProc(const ALuint property, ALvoid *data, ALuint dataSize)
//{
//    OSStatus	err = noErr;
//	static	alcASASetListenerProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alcASASetListenerProcPtr) alcGetProcAddress(NULL, "alcASASetListener");
//    }
//    
//    if(proc)
//        err = proc(property, data, dataSize);
//    return (err);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//typedef OSStatus	(*alcASAGetListenerProcPtr)	(const ALuint property, ALvoid *data, ALuint *dataSize);
//OSStatus  alcASAGetListenerProc(const ALuint property, ALvoid *data, ALuint *dataSize)
//{
//    OSStatus	err = noErr;
//	static	alcASAGetListenerProcPtr	proc = NULL;
//    
//    if(proc == NULL) {
//        proc = (alcASAGetListenerProcPtr) alcGetProcAddress(NULL, "alcASAGetListener");
//    }
//    
//    if(proc)
//        err = proc(property, data, dataSize);
//    return (err);
//}
/*===================================================
 SOURCES
 ===================================================*/
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourcePlayState:(int)inTag :(int)inCheckBoxValue
//{
//	if(mSourceOn[inTag] == inCheckBoxValue)
//		return;
//	
//	mSourceOn[inTag] = inCheckBoxValue;
//	
//	if(mSourceOn[inTag])
//		alSourcePlay(gSource[inTag]);
//	else
//		alSourceStop(gSource[inTag]);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
////  Pitch
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourcePitch:(int)inTag :(float)inPitch
//{
//	alSourcef(gSource[inTag], AL_PITCH, inPitch);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Source Gain
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceGain:(int)inTag :(float)inGain
//{
//	alSourcef(gSource[inTag], AL_GAIN, inGain);
//	// alSourcef (gSource[inTag], AL_SEC_OFFSET, 1.0); // quick way to test the set offset API until controls can be added to UI
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Source Rolloff
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceRolloffFactor:(int)inTag :(float)inRolloff
//{
//	alSourcef(gSource[inTag], AL_ROLLOFF_FACTOR, inRolloff);
//}
//
//- (float) getSourceRolloffFactor:(int)inTag
//{
//	float	ro;
//	alGetSourcef(gSource[inTag], AL_ROLLOFF_FACTOR, &ro);
//	return ro;
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Source Direction & Velocity
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceVelocity:(int)inTag :(float) inVelocity
//{
//	mSourceVelocityScaler[inTag] = inVelocity;
//
//	float	velocities[3];
//	velocities[1] = 0;	// No Change to the Y vector
//	[self getSourceVelocities: inTag : &velocities[0] : &velocities[2]];
//	
//	alSourcefv(gSource[inTag], AL_VELOCITY, velocities);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceDirection:(int)inTag
//{
//	float	directions[3];
//	directions[1] = 0;		// No Change to the Y vector
//	
//	if(gSourceDirectionOnOff[inTag] == false)
//	{
//		directions[0] = 0;
//		directions[2] = 0;
//		alSourcefv(gSource[inTag], AL_DIRECTION, directions);
//		
//		alSourcef(gSource[inTag], AL_CONE_INNER_ANGLE, 360.0);
//		alSourcef(gSource[inTag], AL_CONE_OUTER_ANGLE, 360.0);
//		alSourcef(gSource[inTag], AL_CONE_OUTER_GAIN, 0.0);
//	}
//	else
//	{
//		[self getSourceDirections: inTag : &directions[0] : &directions[2]];
//		
//		alSourcefv(gSource[inTag], AL_DIRECTION, directions);
//		alSourcef(gSource[inTag], AL_CONE_INNER_ANGLE, 90	);
//		alSourcef(gSource[inTag], AL_CONE_OUTER_ANGLE, 180);
//		alSourcef(gSource[inTag], AL_CONE_OUTER_GAIN, mSourceOuterConeGain[inTag]	);
//	}
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceOuterConeGain:(int)inTag :(float) inGain
//{
//	mSourceOuterConeGain[inTag] = inGain;
//	
//	alSourcef(gSource[inTag], AL_CONE_OUTER_GAIN, mSourceOuterConeGain[inTag]);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceInnerConeAngle:(int)inTag :(float) inAngle
//{
//	mSourceInnerConeAngle[inTag] = inAngle;
//	
//	alSourcef(gSource[inTag], AL_CONE_INNER_ANGLE, mSourceInnerConeAngle[inTag]);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceOuterConeAngle:(int)inTag :(float) inAngle
//{
//	mSourceOuterConeAngle[inTag] = inAngle;
//	
//	alSourcef(gSource[inTag], AL_CONE_OUTER_ANGLE, mSourceOuterConeAngle[inTag]);
//	[self drawSourceWithDirection :inTag];
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceDirectionOnOff:(int)inTag :(int)inCheckBoxValue
//{
//	if(gSourceDirectionOnOff[inTag] == inCheckBoxValue)
//		return;
//	
//	gSourceDirectionOnOff[inTag] = inCheckBoxValue;
//	[self setSourceDirection: inTag];
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceAngle:(int)inTag :(float)inAngle
//{
//	gSourceAngle[inTag] = inAngle;
//
//	[self setSourceVelocity: inTag : mSourceVelocityScaler[inTag]];
//	[self setSourceDirection: inTag];
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) getSourceVelocities:(int)inTag : (float*) outX : (float*) outZ
//{
//	float	rads = DEG2RAD(gSourceAngle[inTag]);
//									 
//	if(outX) *outX = sin(rads) * mSourceVelocityScaler[inTag];
//	if(outZ) *outZ = -cos(rads) * mSourceVelocityScaler[inTag];
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) getSourceDirections:(int)inTag : (float*) outX : (float*) outZ
//{
//	float	rads = DEG2RAD(gSourceAngle[inTag]);
//									 
//	if(outX) *outX = sin(rads);		
//	if(outZ) *outZ = -cos(rads);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Render Channels
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setRenderChannels:(int)inCheckBoxValue
//{
//	// Global Setting:
//	// Used to Force OpenAL to render to stereo, even if the user's default audio hw is multichannel
//
//	UInt32		setting = (inCheckBoxValue == 0) ? alcGetEnumValue(NULL, "ALC_RENDER_CHANNEL_COUNT_MULTICHANNEL") : alcGetEnumValue(NULL, "ALC_RENDER_CHANNEL_COUNT_STEREO");
//	
//	alMacOSXRenderChannelCountProc((const ALint) setting);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Render Quality
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setRenderQuality:(int)inCheckBoxValue
//{
//	// Global Setting:
//	// Used to turn on HRTF Rendering when OpenAL is rendering to stereo
//	
//	UInt32		setting = (inCheckBoxValue == 0) ? alcGetEnumValue(NULL, "ALC_SPATIAL_RENDERING_QUALITY_LOW") : alcGetEnumValue(NULL, "ALC_SPATIAL_RENDERING_QUALITY_HIGH");
//	
//	alcMacOSXRenderingQualityProc((const ALint) setting);
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Source Reverb Level
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceReverb:(int)inTag :(float)inReverbSendLevel
//{
//	ALfloat		level = inReverbSendLevel;
//	alcASASetSourceProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_SEND_LEVEL"), gSource[inTag], &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Source Reverb Level
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceOcclusion:(int)inTag :(float)inLevel
//{
//	ALfloat		level = inLevel;
//	alcASASetSourceProc(alcGetEnumValue(NULL, "ALC_ASA_OCCLUSION"), gSource[inTag], &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Source Reverb Level
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSourceObstruction:(int)inTag :(float)inReverbSendLevel
//{
//	ALfloat		level = inReverbSendLevel;
//	alcASASetSourceProc(alcGetEnumValue(NULL, "ALC_ASA_OBSTRUCTION"), gSource[inTag], &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Global Reverb Level
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setGlobalReverb :(float)inReverbLevel
//{
//	ALfloat		level = inReverbLevel;
//	alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_GLOBAL_LEVEL"), &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Reverb ON
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setReverbOn:(int)inCheckBoxValue
//{
//	UInt32		setting = (inCheckBoxValue == 0) ? 0 : 1;
//	alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_ON"), &setting, sizeof(setting));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setReverbEQGain:(float)inLevel
//{
//	ALfloat		level = inLevel;
//	alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_EQ_GAIN"), &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setReverbEQBandwidth:(float)inLevel
//{
//	ALfloat		level = inLevel;
//	alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_EQ_BANDWITH"), (ALvoid *) &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setReverbEQFrequency:(float)inLevel
//{
//	ALfloat		level = inLevel;
//	alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_EQ_FREQ"), &level, sizeof(level));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setReverbRoomType:(int)inTag controlIndex:(int) inIndex title:(NSString*) inTitle
//{
//	UInt32		roomtype = inTag;
//	UInt32		roomIndex = inIndex;
//	if(roomIndex < 12)
//	{
//		// the 1st 12 menu items have the proper reverb constant in the tag value
//		alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_ROOM_TYPE"), &roomtype, sizeof(roomtype));
//	}
//	else
//	{
//		const char *fullPathToFile;
//		fullPathToFile =[[[NSBundle mainBundle] pathForResource:inTitle ofType:@"aupreset" inDirectory:[NSString stringWithCString:"ReverbPresets"]]UTF8String];
//
//		alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_PRESET"), (void *) fullPathToFile, strlen(fullPathToFile));
//	}
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setReverbQuality:(int)inTag
//{
//	UInt32		quality = inTag;
//	alcASASetListenerProc(alcGetEnumValue(NULL, "ALC_ASA_REVERB_QUALITY"), &quality, sizeof(quality));
//}
//
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Doppler Factor
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setDopplerFactor :(float)inValue
//{
//	alDopplerFactor(inValue);
//}
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//// Speed Of Sound
//// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//- (void) setSpeedOfSound :(float)inValue
//{
//	alSpeedOfSound(inValue);
//}

#endif COMPONENTS_CONFIG_ENABLE_OPENAL
