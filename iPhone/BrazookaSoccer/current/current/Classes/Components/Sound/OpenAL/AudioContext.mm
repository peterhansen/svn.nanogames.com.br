#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include "AudioContext.h"
#include "PlaybackAudioDevice.h"
#include "Exceptions.h"
#include "Macros.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

AudioContext::AudioContext( const PlaybackAudioDevice* pParent ) : pContext( NULL ), pParentDevice( pParent )
{
	// Limpa o código de erro
	ALCdevice* pDevice = pParentDevice->getOpenALDevice();
	alcGetError( pDevice );
				
	pContext = alcCreateContext( pDevice, NULL );
				
	if( ( pContext == NULL ) || ( alcGetError( pDevice ) != AL_NO_ERROR ) )
	#if DEBUG
		throw OpenALException( "AudioContext::AudioContext()" );
	#else
		throw OpenALException();
	#endif
	
	memset( sources, 0, sizeof( AudioSource* ) * MAX_CONTEXT_SOURCES );
	memset( wasPlaying, 0, sizeof( bool ) * MAX_CONTEXT_SOURCES );
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

AudioContext::~AudioContext( void )
{
	if( isCurrentContext() )
		alcMakeContextCurrent( NULL );
	
	for( uint8 i = 0 ; i < MAX_CONTEXT_SOURCES ; ++i )
		SAFE_DELETE( sources[i] );

	alcDestroyContext( pContext );
		
	pParentDevice = NULL;
	pContext = NULL;	
}

/*==============================================================================================

MÉTODO makeCurrentContext
	Torna este AudioContext o atual.
 
==============================================================================================*/

bool AudioContext::makeCurrentContext( void )
{
	// Limpa o código de erro
	ALCdevice* pDevice = pParentDevice->getOpenALDevice();
	alcGetError( pDevice );
	
	// LEAK : Até a versão 2.2 do SDK, chamar alcMakeContextCurrent( pContext ) causa memory leak 
	// em AudioToolBox

	// Verifica se houve erros durante a operação
	const bool ret = ( alcMakeContextCurrent( pContext ) == AL_TRUE ) && ( alcGetError( pDevice ) == AL_NO_ERROR );
	
	// Reseta o estados das fontes do contexto
	memset( wasPlaying, 0, sizeof( bool ) * MAX_CONTEXT_SOURCES );
	
	// Retorna se houve erros durante a operação
	return ret;
};

/*==============================================================================================

MÉTODO makeCurrentDevice
	Suspende a execução deste AudioContext. Mudanças de estado no OpenAL serão aceitas, mas
não processadas.
 
==============================================================================================*/

bool AudioContext::suspend( void )
{
	if( !isCurrentContext() )
		return true;
	
	// Pausa as fontes de som
	for( uint8 i = 0 ; i < MAX_CONTEXT_SOURCES ; ++i )
	{
		if( sources[i] && ( sources[i]->getState() == AL_PLAYING ) )
		{
			wasPlaying[i] = true;
			sources[i]->pause();
		}
	}

	// Limpa o código de erro
	ALCdevice* pDevice = pParentDevice->getOpenALDevice();
	alcGetError( pDevice );

	alcSuspendContext( pContext );
	
	// Retorna se houve erros durante a operação
	return ( alcGetError( pDevice ) == AL_NO_ERROR );
}

/*==============================================================================================

MÉTODO makeCurrentDevice
	Processa todas as mudanças de estado pendentes e retoma a execução do device.
 
==============================================================================================*/

bool AudioContext::resume( void )
{
	if( !isCurrentContext() )
		return true;
	
	// Resume a execução dos sons que estavam tocando antes de suspendermos a execução pausados
	for( uint8 i = 0 ; i < MAX_CONTEXT_SOURCES ; ++i )
	{
		// Não podemos checar apenas "sources[i]->getState() == AL_PAUSED", pois poderíamos cair
		// no erro de colocar para tocar uma fonte que foi pausada pelo usuário
		if( sources[i] && wasPlaying[i] )
		{
			wasPlaying[i] = false;
			sources[i]->play();
		}
	}
		
	// Limpa o código de erro
	ALCdevice* pDevice = pParentDevice->getOpenALDevice();
	alcGetError( pDevice );

	alcProcessContext( pContext );
	
	// Retorna se houve erros durante a operação
	return ( alcGetError( pDevice ) == AL_NO_ERROR );
}

/*==============================================================================================

MÉTODO newAudioSource
	Obtém um novo AudioSource para este AudioContext.
 
==============================================================================================*/

AudioSource* AudioContext::newAudioSource( uint8* pIndex )
{
	// Só o contexto atual pode criar novas fontes de áudio
	if( !isCurrentContext() )
		return NULL;
	
	// Verifica se possui um índice livre
	uint8 i = 0;
	for( ; i < MAX_CONTEXT_SOURCES ; ++i )
	{
		if( sources[i] == NULL )
			break;
	}
	
	if( i >= MAX_CONTEXT_SOURCES )
	{
		#if DEBUG
			LOG( "AudioContext::newAudioSource(): o numero de sources ja chegou ao maximo permitido\n" );
		#endif
		return NULL;
	}

	// Cria a fonte de som
	AudioSource* pNewSource = new AudioSource( this );
	if( pNewSource )
	{
		// Procura o primeiro índice livre
		if( pIndex )
			*pIndex = i;
		
		sources[ i ] = pNewSource;
	}	
	return pNewSource;
}

/*==============================================================================================

MÉTODO getAudioSource
	Obtém um AudioSource deste AudioContext.
 
==============================================================================================*/

AudioSource* AudioContext::getAudioSource( uint8 index ) const
{
#if DEBUG
	if( index >= MAX_CONTEXT_SOURCES )
	{
		LOG( "AudioContext::getAudioSource(): O indice desejado eh maior do que o numero de fontes possuidas pelo contexto\n" );
		return NULL;
	}
#endif
	return sources[ index ];
}

#endif COMPONENTS_CONFIG_ENABLE_OPENAL
