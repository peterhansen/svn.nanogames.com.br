/*
 *  AppleAudioManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 5/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef APPLE_AUDIO_MANAGER_H
#define APPLE_AUDIO_MANAGER_H 1

#if COMPONENTS_CONFIG_ENABLE_APPLE_SOUND_API

#include <AudioToolbox/AudioQueue.h>
#include <AudioToolbox/AudioFile.h>
#include <AudioToolbox/AudioFileStream.h>

#include "NanoTypes.h" // uint8

// TODOO : Dar uma olhada em MPVolumeView

class AppleAudioManager
{
	public:
		// Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance(). No entanto, isso
		// poderia resultar em exceções disparadas no meio do código da aplicação, o que impactaria
		// o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
		// chamado no início da aplicação, já que o usuário dos componentes parte do princípio que
		// sempre GetInstance() sempre retornará um valor válido 
		// Exceções : OpenALException
		static bool Create( uint8 nAppSounds );
	
		// Deleta o singleton
		static void Destroy( void );
	
		// Retorna a instância do objeto singleton
		static AppleAudioManager* GetInstance( void ) { return pSingleton; };
	
		// Suspende a execução dos devices existentes
		bool suspend( void );
	
		// Resume a execução dos devices existentes
		bool resume( void );
	
		// Indica se algum som está sendo tocado
		bool isPlaying( uint8 soundIndex ) const;
	
		// Determina o volume dos sons da aplicação
		void setVolume( float volume );
	
		// Retorna o volume dos sons da aplicação
		float getVolume( void ) const { return volume; };
	
		// Executa um som da aplicação. Caso o som ainda não esteja carregado, playSound se encarrega
		// de chamar loadSound
		void playSound( uint8 soundName, uint8 soundIndex, bool looping, bool playAgainIfAlreadyPlaying = true );
	
		// Pára a execução de um som da aplicação
		void stopSound( uint8 soundIndex );
	
		// Pára a execução de todos os sons da aplicação
		void stopAllSounds( void );

		// Pára / retoma a execução do som indicado
		void pauseSound( uint8 soundIndex, bool paused );
	
		// Pára / retoma a execução de todos os sons da aplicação
		void pauseAllSounds( bool paused );

		// Retira um som da memória
		bool unloadSound( uint8 soundIndex );
	
		// Carrega um som para ser reproduzido posteriormente
		bool loadSound( uint8 soundName, uint8 soundIndex );
	
	private:
		// Controla a execução do áudio
		static void AudioQueueBufferCallback( void* inUserData, AudioQueueRef inAudioQueue, AudioQueueBufferRef inCompleteAudioQueueBuffer );

		// Contrutor
		// Exceções : OpenALException
		AppleAudioManager( uint8 nAppSounds );
	
		// Destrutor
		~AppleAudioManager( void );

		// Única instância da classe
		static AppleAudioManager* pSingleton;
	
		// Dados de controle
		struct AudioQueueData
		{
			// Construtor
			AudioQueueData( AppleAudioManager* pAudioManager, uint8 soundIndex ) : audioFile( 0 ), dataFormat(), pQueue( NULL ), pBuffer( NULL ), currentPacket( 0 ), totalPackets( 0 ),
																				   numPacketsToRead( 0 ), mPrimingFrames( 0 ), mRemainderFrames( 0 ), pPacketDescs( NULL ),
																				   looping( false ), running( false ), wasPlayingBeforeSuspend( false ), pAudioManager( pAudioManager ),
																				   soundIndex( soundIndex )
			{
			};
			
			// Destrutor
			~AudioQueueData( void )
			{
				//OBS: Não é necessário chamar AudioQueueFreeBuffer pois AudioQueueDispose já o fará
				//AudioQueueFreeBuffer( pQueue, pBuffer );

				AudioQueueDispose( pQueue, YES );
				
				if( audioFile )
					AudioFileClose( audioFile );

				delete[] pPacketDescs;
				pPacketDescs = NULL;
				
				pAudioManager = NULL;
			};
			
			// Auxiliares
			AudioFileID audioFile;
			AudioStreamBasicDescription dataFormat;
			AudioQueueRef pQueue;
			AudioQueueBufferRef pBuffer;
			UInt64 currentPacket;
			UInt64 totalPackets;
			UInt32 numPacketsToRead;
			SInt32 mPrimingFrames;
			SInt32 mRemainderFrames;
			AudioStreamPacketDescription *pPacketDescs;
			
			// Indica se a execução do som está em loop
			bool looping;
			
			// Indica se o som está sendo executado
			bool running;

			// Indica se estávamos tocando o som antes de recebermos um evento suspend
			bool wasPlayingBeforeSuspend;
			
			// Tocador de som responsável por reproduzir este áudio
			AppleAudioManager* pAudioManager;
			
			// Índice desse som em pAudioManager
			const uint8 soundIndex;
		};

		// Volume do tocador de sons [0.0f, 1.0f]
		float volume;
	
		// Indica se a execução de sons está suspensa
		bool paused;

		// Número de sons armazenados nos buffers de AppleAudioManager
		uint8 nAppSounds;

		// Ponteiro para o som que está sendo tocado no momento
		AudioQueueData** pSounds;
};

#endif COMPONENTS_CONFIG_ENABLE_APPLE_SOUND_API

#endif APPLE_AUDIO_MANAGER_H
