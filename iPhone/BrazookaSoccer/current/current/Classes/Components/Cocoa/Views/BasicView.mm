#include "BasicView.h"

//// Extensão da classe para declarar métodos privados
//@interface BasicView ( Private )
//
//// Inicializa o objeto
//-( bool )buildBasicView;
//
//// Libera a memória alocada pelo objeto
//-( void )cleanBasicView;
//
//@end

// Início da implementação da classe
@implementation BasicView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self buildBasicView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildBasicView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	// [self cleanBasicView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

//-( bool )build
//{
//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view
//		// ...
//
//		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self cleanBasicView];
//		return false;
//}

/*==============================================================================================

MENSAGEM cleanBasicView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanBasicView
//{
//}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes que iniciarmos a transição para esta view.
 
===============================================================================================*/

-( void ) onBeforeTransition
{
	// Implementação padrão
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	// Implementação padrão
}

// Fim da implementação da classe
@end
