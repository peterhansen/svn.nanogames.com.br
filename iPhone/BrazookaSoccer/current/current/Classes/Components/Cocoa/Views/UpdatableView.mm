#import "UpdatableView.h"

#include "Config.h"

// Extensão da classe para declarar métodos privados
@interface UpdatableView ( Private )

// Inicializa a view
- ( bool ) buildUpdatableView;

// Controlador do timer
- ( void ) timerProxy;

@end

// Implementção da classe
@implementation UpdatableView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize suspendBeforeDealloc;

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		// Inicializa o objeto
		if( ![self buildUpdatableView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildUpdatableView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

- ( bool )buildUpdatableView
{
	hUpdateTimer = NULL;
	updateInterval = DEFAULT_ANIMATION_INTERVAL;
	suspendBeforeDealloc=true;
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//	// ...
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	if (suspendBeforeDealloc)
		[self suspend];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM timerProxy
	Atualiza a view.

==============================================================================================*/

- ( void )timerProxy
{
	if( [hUpdateTimer isValid] )
		[self update: static_cast< float >( updateInterval )];
}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	// Implementação default
}

/*==============================================================================================

MENSAGEM suspend
	Pára o processamento da view.

==============================================================================================*/

- ( void ) suspend
{
	if( hUpdateTimer )
	{
		[hUpdateTimer invalidate];
		
		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hAnimationTimer invalidate] chama
		// [hAnimationTimer release] automaticamente
		hUpdateTimer = NULL;
	}
}

/*==============================================================================================

MENSAGEM resume
	Reinicia o processamento da view.

==============================================================================================*/

- ( void ) resume
{
	[self suspend];
	hUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:updateInterval target:self selector:@selector( timerProxy ) userInfo:NULL repeats:YES];
}

// Fim da implementação da classe
@end
