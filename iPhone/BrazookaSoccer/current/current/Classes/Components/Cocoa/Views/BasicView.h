/*
 *  BasicView.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/24/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BASIC_VIEW_H
#define BASIC_VIEW_H

#include <UIKit/UIKit.h>

@interface BasicView : UIView
{
}

// Método chamado antes que iniciarmos a transição para esta view
-( void ) onBeforeTransition;

// Método chamado assim que esta view passa a ser a principal view da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
