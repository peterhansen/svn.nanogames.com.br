#include "LoadingThread.h"

// Extensão da classe para declarar métodos privados
@interface LoadingThread ()

@end

// Início da implementação da classe
@implementation LoadingThread

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

-( void )dealloc
{
	[super dealloc];
}

/*==============================================================================================

MENSAGEM main
	Configura a thread de carregamento.

==============================================================================================*/

-( void )loadWithObj:( id )obj Method:( SEL )selector AndParam:( id )param
{
	loadingObj = obj;
	loadingFunc = selector;
	loadingFuncParam = param;
}

/*==============================================================================================

MENSAGEM main
	Ponto de entrada da thread.

==============================================================================================*/

- ( void ) main
{
	if( ( loadingObj != NULL ) && ( loadingFunc != NULL ) && ( loadingFuncParam != NULL ) )
	{
		NSAutoreleasePool* hPool = [[NSAutoreleasePool alloc] init];
		
		[loadingObj performSelector: loadingFunc withObject: loadingFuncParam];

		[hPool release];
	}
}

// Fim da implementação da classe
@end
