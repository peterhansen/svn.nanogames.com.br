/*
 *  UILimitedTextField.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/29/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UI_LIMITED_TEXT_FIELD_H
#define UI_LIMITED_TEXT_FIELD_H

// Apple
#import <UIKit/UIKit.h>

// Components
#include "NanoTypes.h"

// Declarações adiadas
@class CustomTextFieldDelegate;

@interface UILimitedTextField : UITextField < UITextFieldDelegate >
{
	@private	
		// Precisamos armazenar um outro delegate, já que este objeto determina o delegate
		// de seu próprio UITextField
		CustomTextFieldDelegate* hInsideDelegate;
}

// Determina quantos caracteres a campo de texto suporta
-( void )setMinLimit:( uint32 )min AndMaxLimit:( uint32 )max;

@end

#endif
