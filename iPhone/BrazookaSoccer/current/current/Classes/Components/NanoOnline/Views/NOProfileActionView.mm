#include "NOProfileActionView.h"

// NanoOnline
#include "NOTextsIndexes.h"
#include "NOViewsIndexes.h"

// Extensão da classe para declarar métodos privados
@interface NOProfileActionView ( Private )

// Inicializa o objeto
-( bool )buildNOProfileActionView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOProfileActionView;

@end

// Início da implementação da classe
@implementation NOProfileActionView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	  {
		if( ![self buildNOProfileActionView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOProfileActionView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOProfileActionView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNOProfileActionView
{
	// Determina o título da tela
	[self setScreenTitle: [NOControllerView GetText: NO_TXT_PROFILE]];
	return true;
	
//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view que não serão criados pelo Interface Builder
//		// ...
//		
//		// Configura os elementos da view que não serão criados pelo Interface Builder
//		//..
//
//		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self cleanNOProfileActionView];
//		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Configura os elementos da view que foram criados pelo InterfaceBuilder
	[hLbSelect setText: [NOControllerView GetText: NO_TXT_PROFILE_ACTION__MENU_SELECT]];
	[hLbDownload setText: [NOControllerView GetText: NO_TXT_DOWNLOAD]];
	[hLbCreate setText: [NOControllerView GetText: NO_TXT_CREATE]];
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	//[self cleanNOProfileActionView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNOProfileActionView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOProfileActionView
//{
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.

===============================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( hButton == hBtSelect )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_PROFILE_SELECT];
	}
	else if( hButton == hBtDownload )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_PROFILE_DOWNLOAD];
	}
	else if( hButton == hBtCreate )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_PROFILE_CREATE];
	}
}

// Fim da implementação da classe
@end
