/*
 *  NOProfileAuthenticateView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 10/1/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_PROFILE_AUTHENTICATE_VIEW_H
#define NO_PROFILE_AUTHENTICATE_VIEW_H

// Componentes
#include "NOBaseView.h"
#include "NOListenerCocoaBridge.h"

// Declarações Adiadas
@class UICustomSwitch;
@class UILimitedTextField;

enum NOProfileAuthenticateViewMode
{
	PROFILE_AUTHENTICATE_VIEW_MODE_UNDEFINED	= 0,
	PROFILE_AUTHENTICATE_VIEW_MODE_LOGIN		= 1,
	PROFILE_AUTHENTICATE_VIEW_MODE_DOWNLOAD		= 2
};

@interface NOProfileAuthenticateView : NOBaseView <NOCocoaListener>
{
	@private
		NOCustomer currCustomer;
		NOListenerCocoaBridge noListener;
	
		// Modo de visualização da tela
		NOProfileAuthenticateViewMode viewMode;
	
		// Elementos de interface contidos na view
		IBOutlet UILimitedTextField *hTbNickname;
		IBOutlet UILimitedTextField *hTbPassword;
	
		IBOutlet UICustomSwitch *hSwRememberMe;
		IBOutlet UICustomSwitch *hSwRememberPassword;
	
		IBOutlet UIButton *hBtOk;
	
		// Para conseguirmos traduzir os textos sem termos que criar
		// outros xibs
		IBOutlet UILabel *hLbNickname;
		IBOutlet UILabel *hLbPassword;
		IBOutlet UILabel *hLbRememberMe;
		IBOutlet UILabel *hLbRememberPassword;
}

@property ( readwrite, nonatomic, assign, getter = getViewMode, setter = setViewMode )NOProfileAuthenticateViewMode viewMode;

// Este método deve ser chamado antes de a view ser exibida. Ele prrenche os campos do formulário
// com os dados do perfil passado como parâmetro
-( void )setProfileToAuthenticate:( const NOCustomer* )pProfileToAuthenticate;

// Método chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

@end

#endif
