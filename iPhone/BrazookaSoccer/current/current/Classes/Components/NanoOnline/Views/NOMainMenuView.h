/*
 *  NOMainMenuView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_MAIN_MENU_VIEW_H
#define NANO_ONLINE_MAIN_MENU_VIEW_H

// Components
#include "NOBaseView.h"

@interface NOMainMenuView : NOBaseView
{
	@private
		// Botões disponíeveis na interface
		IBOutlet UIButton *hBtProfile;
		IBOutlet UIButton *hBtRanking;
		IBOutlet UIButton *hBtHelp;
		IBOutlet UIButton *hBtExit;
	
		// Para conseguirmos traduzir os textos sem termos que criar
		// outros xibs
		IBOutlet UILabel *hLbProfile;
		IBOutlet UILabel *hLbRanking;
		IBOutlet UILabel *hLbHelp;
		IBOutlet UILabel *hLbExit;
}

// Método chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

@end

#endif
