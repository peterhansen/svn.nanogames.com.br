#include "NOGlobalData.h"

// Cocoa
#include <CoreFoundation/CFBase.h>
#include <CoreFoundation/CFBundle.h>
#include <CoreFoundation/CFString.h>

// NanoOnline
#include "NOConf.h"
#include "NOCustomer.h"
#include "NORanking.h"
#include "NOString.h"

/*==============================================================================================

FUNÇÃO ConvertNSStringToSTDString

==============================================================================================*/

void ConvertNSStringToSTDString( const CFStringRef hStr, NOString& outStr )
{
	outStr.clear();

	int32 nChars = CFStringGetLength( hStr );
	for( int32 i = 0 ; i < nChars ; ++i )
		outStr.push_back( static_cast< std::wstring::value_type >( CFStringGetCharacterAtIndex( hStr, i ) ) );
}

/*==============================================================================================

FUNÇÃO GetNOText

===============================================================================================*/

#define GET_TXT_BUFFER_SIZE 32

void GetNOText( uint16 textIndex, NOString& outText )
{
	char buffer[ GET_TXT_BUFFER_SIZE ];
	snprintf( buffer, GET_TXT_BUFFER_SIZE, "%d", static_cast< int32 >( textIndex ) );
	
	CFStringRef hKey = CFStringCreateWithCString( NULL, buffer, kCFStringEncodingUTF8 );
	if( hKey )
	{
		CFStringRef hLocalizedStr = CFCopyLocalizedStringFromTable( hKey, CFSTR( "no" ), NULL );
		if( hLocalizedStr )
		{
			ConvertNSStringToSTDString( hLocalizedStr, outText );
			CFRelease( hLocalizedStr );
		}
		
		CFRelease( hKey );
	}
}

#undef GET_TXT_BUFFER_SIZE

/*==============================================================================================

FUNÇÃO GetNOViewsBundleName
	Retorna o nome do bundle que contém as views utilizadas no NanoOnline.

===============================================================================================*/

const char* GetNOViewsBundleName( void )
{
	static const char* kNOViewsBundle = "NanoOnlineViews.bundle";
	return kNOViewsBundle;
}

/*==============================================================================================

FUNÇÃO GetNOImagesBundleName
	Retorna o nome do bundle que contém as imagens utilizadas no NanoOnline.

===============================================================================================*/

const char* GetNOImagesBundleName( void )
{
	static const char* kNOImagesBundle = "NanoOnlineImages.bundle";
	return kNOImagesBundle;
}

/*==============================================================================================

MÉTODO ESTÁTICO GetActiveProfile
	Retorna uma referência para o perfil atualmente logado no device.

===============================================================================================*/

NOCustomer& NOGlobalData::GetActiveProfile( void )
{
	// Perfil atualmente logado no device
	// Essa variável vai ser criada e inicializada com o construtor padrão na 1a
	// vez que o método NOGlobalData::GetActiveProfile for chamado. Será destruída
	// apenas quando a aplicação terminar. Para maiores informações, ver a política
	// de destruição de variáveis estáticas de C++ e a documentação de "atexit()"
	static NOCustomer activeProfile;
	return activeProfile;
}

/*==============================================================================================

MÉTODO ESTÁTICO GetRanking
	Retorna uma referência para o ranking contendo as melhores pontuações ainda não submetidas.

===============================================================================================*/

NORanking& NOGlobalData::GetRanking( void )
{
	// Ranking contendo as melhores pontuações ainda não submetidas
	// e os dados do ranking online
	// Essa variável vai ser criada e inicializada com o construtor padrão na 1a
	// vez que o método NOGlobalData::GetRanking for chamado. Será destruída
	// apenas quando a aplicação terminar. Para maiores informações, ver a política
	// de destruição de variáveis estáticas de C++ e a documentação de "atexit()"
	static NORanking ranking;
	return ranking;
}

/*==============================================================================================

MÉTODO ESTÁTICO GetConfig
	Retorna a configuração do NanoOnline.

===============================================================================================*/

NOConf& NOGlobalData::GetConfig( void )
{
	// Configuração do NanoOnline
	// Essa variável vai ser criada e inicializada com o construtor padrão na 1a
	// vez que o método NOGlobalData::GetConfig for chamado. Será destruída
	// apenas quando a aplicação terminar. Para maiores informações, ver a política
	// de destruição de variáveis estáticas de C++ e a documentação de "atexit()"
	static NOConf configuration;
	return configuration;
}
