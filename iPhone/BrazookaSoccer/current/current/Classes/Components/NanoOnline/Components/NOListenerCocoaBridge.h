/*
 *  NOListenerCocoaBridge.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/14/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_LISTENER_COCOA_BRIDGE_H
#define NANO_ONLINE_LISTENER_COCOA_BRIDGE_H 1

#include "INOListener.h"

// Protocolo que informa ao delegate os eventos do NanoOnline
@protocol NOCocoaListener < NSObject >

	@optional
		// Indica que uma requisição foi enviada para o NanoOnline
		-( void )onNORequestSent;

		// Indica que uma requisição foi respondida e terminada com sucesso
		-( void )onNOSuccessfulResponse;

		// Indica que a requisição foi cancelada pelo usuário
		-( void )onNORequestCancelled;

		// Indica o progresso da requisição atual
		-( void )onNOProgressChangedTo:( int32 )currBytes ofTotal:( int32 )totalBytes;

	@required
		// Sinaliza erros ocorridos nas operações do NanoOnline
		- ( void )onNOError:( NOErrors )errorCode WithErrorDesc:( NSString* )hErrorDesc;
@end

class NOListenerCocoaBridge : public INOListener
{
	public:
		// Construtor
		explicit NOListenerCocoaBridge( id< NOCocoaListener > hCocoaNOListener = nil );

		// Destrutor
		virtual ~NOListenerCocoaBridge( void ){};
	
		// Faz a ligação entre o listener C++ e o listener Cocoa
		void setCocoaListener( id< NOCocoaListener > hListener );
	
		// Indica que uma requisição foi enviada para o NanoOnline
		virtual void onNORequestSent( void );
	
		// Indica que a requisição foi cancelada pelo usuário
		virtual void onNORequestCancelled( void );
	
		// Indica que uma requisição foi respondida e terminada com sucesso
		virtual void onNOSuccessfulResponse( void );
	
		// Sinaliza erros ocorridos nas operações do NanoOnline
		virtual void onNOError( NOErrors errorCode, const NOString& errorStr );
	
		// Indica o progresso da requisição atual
		virtual void onNOProgressChanged( int32 currBytes, int32 totalBytes );
	
	private:
		// Objeto cocoa que irá receber os eventos do NanoOnline
		id< NOCocoaListener > hCocoaNOListener;
};

#endif
