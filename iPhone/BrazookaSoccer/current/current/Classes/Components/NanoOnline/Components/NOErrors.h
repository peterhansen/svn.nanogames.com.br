/*
 *  NOErrors.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/14/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_ERRORS_H
#define NANO_ONLINE_ERRORS_H 1

enum NOErrors
{
	NO_ERROR_NONE						= 0,
	NO_ERROR_INTERNAL					= 1,
	NO_ERROR_NETWORK_ERROR				= 2,
	NO_ERROR_INVALID_RESPONSE_FORMAT	= 3,
	NO_ERROR_INVALID_DATA_SUBMITED		= 4,
};

#endif
