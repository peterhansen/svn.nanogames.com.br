/*
 *  EventManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/12/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H 1

#include "EventListener.h"
#include "EventTypes.h"
#include "NanoTypes.h"

// Tamanho do array de listeners
#define EVENTMANAGER_LISTENERS_ARRAY_LEN 32

// TASK : Otimizar com controle de índice do usuário
class EventManager
{
	public:
		// Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance(). No entanto, isso
		// poderia resultar em exceções disparadas no meio do código da aplicação, o que impactaria
		// o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
		// chamado no início da aplicação, já que o usuário dos componentes parte do princípio que
		// sempre GetInstance() sempre retornará um valor válido 
		static bool Create( void );
	
		// Deleta o singleton
		static void Destroy( void );
	
		// Retorna a instância do objeto singleton
		static EventManager* GetInstance( void ) { return pSingleton; };
	
		// Acrescenta um objeto ao array de listeners e retorna o seu índice
		int16 insertListener( EventListener* pListener );
	
		// Remove o listener recebido do array de listeners
		bool removeListener( int16 listenerIndex );
	
		// Limpa o array de listeners
		void removeAllListeners( void );
	
		// Retorna o listener de eventos desejado ou NULL caso este não exista
		EventListener* getListener( int16 listenerIndex );
	
		// Indica que um evento deve ser tratado pela aplicação. Retorna se algum EventListener
		// tratou o evento passado
		bool setEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) const;

	private:
		// Construtor
		EventManager( void );

		// Destrutor
		~EventManager( void );

		// Ponteiro para a única instância da classe
		static EventManager* pSingleton;
	
		// OBS : Seria necessário se fizéssemos os "chega pra cá" na hora das remoções. No
		// entanto, sem um controle melhor, utilizar está técnica invalidaria o atributo
		// 'eventManagerIndex' da classe EventListener
		// Número de slots ocupados em eventListeners
		//int16 nListeners;

		// Array de objetos tratadores de eventos
		EventListener* eventListeners[ EVENTMANAGER_LISTENERS_ARRAY_LEN ];
};

#endif
