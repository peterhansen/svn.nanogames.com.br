#include "WoodBarrierPlayer.h"

#include "Exceptions.h"
#include "Sprite.h"

#include "Config.h"
#include "GameUtils.h"

// Número de objetos contidos no grupo
#define WOOD_N_OBJS 1

// Altura deste personagem
#define WOOD_WIDTH	0.60f
#define WOOD_HEIGHT	1.70f

// Offset para ignorarmos a sombra que, neste caso, está junta da imagem
#define WOOD_SHADOW_OFFSET 35.0f

// Offset do pixel de referência do personagem
#define WOOD_FEET_OFFSET_X ( WOOD_SHADOW_OFFSET - 10.0f )
#define WOOD_FEET_OFFSET_Y 55.0f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

WoodBarrierPlayer::WoodBarrierPlayer( WoodBarrierPlayer* pBase ) : BarrierPlayer( WOOD_N_OBJS )
{
	// Aloca o personagem
	Sprite* pWood = NULL;
	if( pBase == NULL )
	{
		char aux[] = "wd";
		
		#if CONFIG_TEXTURES_16_BIT
			pWood = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			pWood = new Sprite( aux, aux );	
		#endif
		
		float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, WOOD_HEIGHT, 0.0f ).y ) / pWood->getStepFrameHeight() );
		pWood->setScale( scaleFactor, scaleFactor );
	}
	else
	{
		pWood = new Sprite( static_cast< Sprite* >( pBase->getObject( 0 ) ) );	
	}
	
	if( !pWood || !insertObject( pWood ) )
	{
		delete pWood;
		goto Error;
	}

	// Determina o tamanho do grupo
	setSize( pWood->getWidth(), pWood->getHeight() );
	
	#if DEBUG
		setName( "Woody" );
	#endif

	// TODOO : Determina as áreas extras de colisão do personagem

	return;
	
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "WoodBarrierPlayer::WoodBarrierPlayer( void ): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

WoodBarrierPlayer::~WoodBarrierPlayer( void )
{
}

/*===========================================================================================
 
MÉTODO getRealWidth
	Retorna a largura real do jogador da barreira (em metros).

============================================================================================*/

float WoodBarrierPlayer::getRealWidth( void ) const
{
	return WOOD_WIDTH;
}

/*===========================================================================================
 
MÉTODO updateFrame
	Atualiza o frame do jogador de acordo com a direção para a qual está virado.

============================================================================================*/

void WoodBarrierPlayer::updateFrame( void )
{
	float width = WOOD_WIDTH, height = WOOD_HEIGHT - 0.20f;
	Point3f top( VECTOR_UP_90 ), right, realOffset, screenOffset( WOOD_SHADOW_OFFSET );
	
	getOrtho( &right );

	Sprite* pWood = static_cast< Sprite* >( getObject( 0 ) );
	if( pWood->getCurrAnimSequence() == 1 )
	{
		screenOffset.x += 10.0f;
		width -= 0.15f;
	}
	// OBS: Não estamos utilizando
//	else if( pWood->getCurrAnimSequence() == 6 )
//	{
//		realOffset.x += 0.08f;
//		realOffset.y += 0.05f;
//	}

	// OLD
//	TextureFrame currFrameInfo;
//	pWood->getStepFrameInfo( &currFrameInfo, pWood->getCurrAnimSequence(), pWood->getCurrAnimSequenceStep() );
//
//	Point3f aux = GameUtils::isoGlobalToPixels( realPosition.x + realOffset.x, realPosition.y + realOffset.y, realPosition.z + realOffset.z );
//	aux.x += pWood->getPosition()->x + currFrameInfo.offsetX + screenOffset.x;
//	aux.y += pWood->getPosition()->y + currFrameInfo.offsetY + screenOffset.y;
//	Point3f realPos = GameUtils::isoPixelsToGlobalY0( aux.x, aux.y );
//
//	collisionArea.set( realPos, top, right, ORIENT_TOP_LEFT, width, height );
	
	collisionArea.set( realPosition + realOffset, top, right, ORIENT_BOTTOM, width, height );
}

/*===========================================================================================
 
MÉTODO prepare
	Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente.

============================================================================================*/

void WoodBarrierPlayer::prepare( const Point3f& ballRealPos )
{
	Sprite* pWood = static_cast< Sprite* >( getObject( 0 ) );
	switch( direction )
	{
		case DIRECTION_UP_RIGHT:
			// OBS: Fica feio, então utilizamos DIRECTION_DOWN_RIGHT
//			pWood->setAnimSequence( 6, false );
//			break;
			
		case DIRECTION_RIGHT:
			// OBS: Fica feio, então utilizamos DIRECTION_DOWN_RIGHT
//			pWood->setAnimSequence( 5, false );
//			break;
			
		case DIRECTION_DOWN_RIGHT:
			pWood->setAnimSequence( 4, false );
			break;

		case DIRECTION_DOWN:
			pWood->setAnimSequence( 3, false );
			break;

		case DIRECTION_DOWN_LEFT:
			pWood->setAnimSequence( 2, false );
			break;

		case DIRECTION_LEFT:
			pWood->setAnimSequence( 1, false );
			break;

		case DIRECTION_UP_LEFT:
			pWood->setAnimSequence( 0, false );
			break;
			
		// Nunca acontecerá esta possibilidade
//		case DIRECTION_UP:
//			break;
	}
	
	TextureFrame currFrameInfo;
	pWood->getStepFrameInfo( &currFrameInfo, pWood->getCurrAnimSequence(), pWood->getCurrAnimSequenceStep() );
	setAnchor( currFrameInfo.offsetX + WOOD_FEET_OFFSET_X + ( currFrameInfo.width * pWood->getScale()->x * 0.5f ),
			   currFrameInfo.offsetY + (( currFrameInfo.height - WOOD_FEET_OFFSET_Y ) * pWood->getScale()->y ), 0.0f );
}

/*===============================================================================

MÉTODO updatePosition
	Atualiza a posição da bola na tela (não altera a posição real).
   
=============================================================================== */

void WoodBarrierPlayer::updatePosition( void )
{
	// Atualiza a orientação e o retângulo de colisão go goleiro
	updateFrame();

	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( p.x, p.y, 0.0f );
}

/*===========================================================================================
 
MÉTODO checkCollision
	Testa colisão deste objeto com a área passada como parâmetro.

============================================================================================*/

bool WoodBarrierPlayer::checkCollision( const CollisionQuad& area )
{
	// TODOO : Checa colisão com as áreas extras de colisão do personagem	
	return false;
}

/*===========================================================================================
 
MÉTODO getCollisionSoundNameAndIndex
	Retorna o índice do som que deve ser tocado quando a bola colide com este jogador.

============================================================================================*/

void WoodBarrierPlayer::getCollisionSoundNameAndIndex( uint8* pName, uint8* pIndex ) const
{
	*pName = SOUND_NAME_BARRIER_WOODY;
	*pIndex = SOUND_INDEX_BARRIER_WOODY;
}

/*===============================================================================

MÉTODO render
	Renderiza o objeto.

================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )

bool WoodBarrierPlayer::render( void )
{
	if( ObjectGroup::render() )
	{
		// Renderiza o quad de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		Point3f pos = GameUtils::isoGlobalToPixels( collisionArea.position );
		Point3f dx = GameUtils::isoGlobalToPixels( collisionArea.right * collisionArea.width );
		Point3f dy = GameUtils::isoGlobalToPixels( collisionArea.top * collisionArea.height );

		dy = -dy;
		
		Point3f top[ 2 ] = {
								pos,
								pos + dy,
							};
		
		glColor4ub( 255, 0, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f right[ 2 ] = {
								pos,
								pos + dx,
							};
		
		glColor4ub( 0, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f others[ 4 ] = {
								pos + dy,
								pos + dy + dx,
								pos + dy + dx,
								pos + dx
							};

		glColor4ub( 255, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, others );
		glDrawArrays( GL_LINES, 0, 4 );

		Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
		if( collisionArea.normal.z > 0.0f )
			glColor4ub( 179, 179, 179, 255 );
		else
			glColor4ub( 255, 183, 15, 255 );

		glVertexPointer( 3, GL_FLOAT, 0, &nomal );
		glDrawArrays( GL_POINTS, 0, 1 );

//		// Renderiza a moldura do sprite
//		Sprite* pWood = static_cast< Sprite* >( getObject( 0 ) );
//		TextureFrame currFrameInfo;
//		pWood->getStepFrameInfo( &currFrameInfo, pWood->getCurrAnimSequence(), pWood->getCurrAnimSequenceStep() );
//		
//		float spritePosX = getPosition()->x + pWood->getPosition()->x + currFrameInfo.offsetX + 0.375f;
//		float spritePosY = getPosition()->y + pWood->getPosition()->y + currFrameInfo.offsetY + 0.375f;
//		
//		float spriteWidth = currFrameInfo.width * pWood->getScale()->x;
//		float spriteHeight = currFrameInfo.height * pWood->getScale()->y;
//		
//		Point3f border[ 4 ] = {
//								Point3f( spritePosX, spritePosY ),
//								Point3f( spritePosX, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY )
//							  };
//		
//		glColor4ub( 0, 255, 255, 255 );
//
//		glVertexPointer( 3, GL_FLOAT, 0, border );
//		glDrawArrays( GL_LINE_LOOP, 0, 4 );

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		
		glColor4ub( 255, 255, 255, 255 );
		
		return true;
	}
	return false;
}

#endif
