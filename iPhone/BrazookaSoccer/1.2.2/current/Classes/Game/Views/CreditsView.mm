#import "CreditsView.h"
#import "DeviceInterface.h"

#include "Config.h"
#include "ObjcMacros.h"

#include "FreeKickAppDelegate.h"

// Duração da animação de fadein do ícone
#define CREDITS_VIEW_CONTROLS_ANIM_DUR 0.300f

// Quantidade de tempo a mais que cada colocação do ranking, após a 1a, leva para terminar a animação de fadein
#define CREDITS_VIEW_CONTROLS_ANIM_DIFF 0.100f

//// Tempo total das animações
#define CREDITS_VIEW_HAIR_ANIM_DUR 0.150f
#define CREDITS_VIEW_BT_BACK_ANIM_DUR 0.150f

// Número de pixels, dos controles, que ficam fora da tela
#define CREDITS_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN 33.0f
#define CREDITS_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN 16.0f
#define CREDITS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN 8.0f

// Número de tags que emitem som ao entrar na tela
#define CREDITS_VIEW_N_TAGS_WITH_SOUND 2

// Extensão da classe para declarar métodos privados
@interface CreditsView ()

//- ( bool )buildCreditsView;

- ( void ) setState:( CreditsViewState ) state;

@end

// Início da implementção da classe
@implementation CreditsView

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self buildCreditsView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildCreditsView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM buildCreditsView
	Inicializa a view.

==============================================================================================*/

//- ( void )buildCreditsView
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// OLD
//	// Posiciona as partes da cabeça corretamente
//	[hImgHairLeft setFrame: CGRectMake( ( SCREEN_WIDTH * 0.5f ) - hImgHairLeft.frame.size.width, SCREEN_HEIGHT, hImgHairLeft.frame.size.width, hImgHairLeft.frame.size.height )];
//	[hImgHairRight setFrame: CGRectMake( ( SCREEN_WIDTH * 0.5f ), SCREEN_HEIGHT, hImgHairRight.frame.size.width, hImgHairRight.frame.size.height )];
//	
//	// Espelha a imagem do lado direito, já que esta é formada pela imagem do lado esquerdo
//	CGAffineTransform transform = hImgHairRight.transform;
//	transform = CGAffineTransformScale( transform, -1.0, 1.0f );
//	[hImgHairRight setTransform:transform];
	
	soundFlags = std::vector< bool >( CREDITS_VIEW_N_TAGS_WITH_SOUND, false );
	
    FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;	
	
	hBtBackText = [[FontLabel alloc] initWithFrame:[hBtBack bounds] fontName:@"Good Girl" pointSize:16.0f];	
	hBtBackText.text =CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_BACK] );	 
	
	hLbCopyright1.text=CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_RIGHTS] );	 
	
	hBtGoToURLText = [[FontLabel alloc] initWithFrame:[hBtGoToURL bounds] fontName:@"Good Girl" pointSize:17.0f];	
	hBtGoToURLText.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_WEBSITE] );	 
	
	
	
	FontLabel *txtArray[MSG_N_CREDITS_VIEW]={hBtBackText,hBtGoToURLText};
	UIView *viewArray[MSG_N_CREDITS_VIEW]={hBtBack,hBtGoToURL};	
	std::string currOsVersion;
	DeviceInterface::GetDeviceSystemVersion( currOsVersion );
	bool olderThan30 = Utils::CompareVersions( currOsVersion, "3.0" ) < 0;
	
	for( uint8 i = 0 ; i < MSG_N_CREDITS_VIEW ; ++i )
	{
		txtArray[i].textColor = [UIColor whiteColor];
		txtArray[i].backgroundColor = nil;
		txtArray[i].opaque = NO;
		txtArray[i].shadowColor=[UIColor darkGrayColor];
		txtArray[i].shadowOffset = CGSizeMake(-1,1);			
		[viewArray[i] addSubview:txtArray[i]];	
		[txtArray[i] setTextAlignment:UITextAlignmentCenter];
		[txtArray[i] setAdjustsFontSizeToFitWidth:YES];		
		if (olderThan30)
		{			
			
			UIView *handler;
			[viewArray[i] layoutSubviews];
			[viewArray[i] bringSubviewToFront:txtArray[i]];
			NSArray *views=[viewArray[i] subviews];
			int viewscount=[views count];
			
			for (uint8 c=0;c<viewscount;c++ )		
			{			
				handler=[views objectAtIndex:c ];
				if (handler!=txtArray[i])
					[handler setHidden:YES ];
			}
		}		
		[txtArray[i] release];	
	}	
	
/////////////
	
	{
	NSArray *subviews=hBtBack.subviews;
	FontLabel *fl=[subviews lastObject];
	
	CGPoint centro=(fl).center;
	centro.y=centro.y*0.8f;
	(fl).center=centro;
	}
	 
/////////////
	
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
    [super dealloc];
}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	switch( currState )
	{
		case CREDITS_VIEW_STATE_SHOWING_STEP_0:
			{
				float finalX = SCREEN_WIDTH - hImgHair.frame.size.width + CREDITS_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN;
				float nextX = hImgHair.frame.origin.x - ( ( timeElapsed * hImgHair.frame.size.width ) / CREDITS_VIEW_HAIR_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlags[0] )
				{
					soundFlags[0] = true;
					[(( FreeKickAppDelegate* )APP_DELEGATE) playAudioNamed: SOUND_NAME_HAIR_MOVE AtIndex: SOUND_INDEX_HAIR_MOVE Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: CREDITS_VIEW_STATE_SHOWING_STEP_1];
					
					soundFlags[0] = false;
				}

				[hImgHair setFrame: CGRectMake( nextX, hImgHair.frame.origin.y, hImgHair.frame.size.width, hImgHair.frame.size.height )];
			}
			break;

		case CREDITS_VIEW_STATE_SHOWING_STEP_1:
			{
				#define N_ANIMATABLE_CONTROLS 4

				uint8 done = 0;
				float diff = 0.0f;
				UIView* controlsArray[ N_ANIMATABLE_CONTROLS ] = { hIcon, hImgNanoLogo, hLbCopyright0, hLbCopyright1 };
				
				for( uint8 i = 0 ; i < N_ANIMATABLE_CONTROLS ; ++i )
				{
					float nextAlpha = controlsArray[ i ].alpha + ( timeElapsed / ( CREDITS_VIEW_CONTROLS_ANIM_DUR + diff ));
					
					if( nextAlpha > 1.0f )
					{
						nextAlpha = 1.0f;
						++done;
					}

					[controlsArray[ i ] setAlpha: nextAlpha];
					
					diff += CREDITS_VIEW_CONTROLS_ANIM_DIFF;
				}

				if( done == N_ANIMATABLE_CONTROLS )
					[self setState: CREDITS_VIEW_STATE_SHOWING_STEP_2 ];
				
				#undef N_ANIMATABLE_CONTROLS
			}
			break;

		case CREDITS_VIEW_STATE_SHOWING_STEP_2:
			{
				float totalMovement = hBtGoToURL.frame.size.width - CREDITS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
				
				float finalX = -CREDITS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN;
				float nextX = hBtGoToURL.frame.origin.x + (( timeElapsed * totalMovement ) / CREDITS_VIEW_BT_BACK_ANIM_DUR );
				
				if( ( ( nextX + hBtGoToURL.frame.size.width ) > 0.0f ) && !soundFlags[0] )
				{
					soundFlags[0] = true;
					[(( FreeKickAppDelegate* )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING Looping: false];
				}
				
				if( nextX > finalX )
				{
					nextX = finalX;
					[self setState: CREDITS_VIEW_STATE_SHOWING_STEP_3];
				}
				
				[hBtGoToURL setFrame: CGRectMake( nextX, hBtGoToURL.frame.origin.y, hBtGoToURL.frame.size.width, hBtGoToURL.frame.size.height )];
			}
			break;

		case CREDITS_VIEW_STATE_SHOWING_STEP_3:
			{
				float totalMovement = hBtBack.frame.size.width - CREDITS_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN;
				
				float finalX = SCREEN_WIDTH - hBtBack.frame.size.width + CREDITS_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN;
				float nextX = hBtBack.frame.origin.x - (( timeElapsed * totalMovement ) / CREDITS_VIEW_BT_BACK_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlags[1] )
				{
					soundFlags[1] = true;
					[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_1_INCOMING Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: CREDITS_VIEW_STATE_SHOWN];
				}
				
				[hBtBack setFrame: CGRectMake( nextX, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height )];
			}
			break;
			
		case CREDITS_VIEW_STATE_HIDING:
			// TODOO : Fazer animações antes de transitar para a outra view ou animar direto? Acho melhor animar direto...
//			{
//				uint8 done = 0;
//				float diff = 0.0f;
//			
//				if( done == APP_N_RECORDS )
//					[self setState: CREDITS_VIEW_STATE_HIDDEN ];
//			}
			break;
	}
}

/*==============================================================================================

MENSAGEM setState
	Determina o estado da view.

==============================================================================================*/

- ( void ) setState:( CreditsViewState ) state
{
	currState = state;

	switch( state )
	{
		case CREDITS_VIEW_STATE_HIDDEN:
			{	
				[hIcon setAlpha: 0.0f];
				[hImgNanoLogo setAlpha: 0.0f];
				[hLbCopyright0 setAlpha: 0.0f];
				[hLbCopyright1 setAlpha: 0.0f];
				
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH, hImgHair.frame.origin.y, hImgHair.frame.size.width, hImgHair.frame.size.height ) ];
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height )];
				
				[hBtGoToURL setFrame: CGRectMake( -hBtGoToURL.frame.size.width, hBtGoToURL.frame.origin.y, hBtGoToURL.frame.size.width, hBtGoToURL.frame.size.height )];
				
				soundFlags = std::vector< bool >( CREDITS_VIEW_N_TAGS_WITH_SOUND, false );

				[self suspend];
			}
			break;

		case CREDITS_VIEW_STATE_SHOWN:
			{
				[hIcon setAlpha: 1.0f];
				[hImgNanoLogo setAlpha: 1.0f];
				[hLbCopyright0 setAlpha: 1.0f];
				[hLbCopyright1 setAlpha: 1.0f];
				
				[hImgHair setFrame: CGRectMake( SCREEN_WIDTH - hImgHair.frame.size.width + CREDITS_VIEW_HAIR_PIXELS_OUTSIDE_SCREEN, hImgHair.frame.origin.y, hImgHair.frame.size.width, hImgHair.frame.size.height ) ];
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH - hBtBack.frame.size.width + CREDITS_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height ) ];
				
				[hBtGoToURL setFrame: CGRectMake( -CREDITS_VIEW_LEFT_BTS_PIXELS_OUTSIDE_SCREEN, hBtGoToURL.frame.origin.y, hBtGoToURL.frame.size.width, hBtGoToURL.frame.size.height )];
				
				[self suspend];
			}
			break;

		case CREDITS_VIEW_STATE_SHOWING_STEP_0:
		case CREDITS_VIEW_STATE_HIDING:
			[self resume];
			break;
			
		case CREDITS_VIEW_STATE_SHOWING_STEP_1:
			break;
	}
}

/*==============================================================================================

MENSAGEM onBtPressed
	Indica que o usuário selecionou uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtPressed:( id )hButton
{
	if( currState != CREDITS_VIEW_STATE_SHOWN )
		return;

	if( hButton == hBtBack )
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM onGoToURL
	Abandona a aplicação e abre o navegador no site da nanogames.

==============================================================================================*/

- ( IBAction ) onGoToURL
{
	#if DEBUG
		LOG_FREE_MEM( "Antes de exibir o popup" );
	#endif
    FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;	
	UIAlertView *hPopUp = [[UIAlertView alloc] initWithTitle: CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_ALERT] ) message: CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_ALERTMSG] ) delegate: self cancelButtonTitle: nil otherButtonTitles: CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_YES] ), CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_CREDITS_NO] ), nil]; 
	[hPopUp setCancelButtonIndex: 1];
	[hPopUp show];
	[hPopUp release];
}

/*==============================================================================================

MENSAGEM alertView clickedButtonAtIndex
	Chamada quando o usuário pressiona um dos botões do menu.

==============================================================================================*/

- ( void ) alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	#if DEBUG
		LOG_FREE_MEM( "Depois de esconder o popup" );
	#endif
	
	if( buttonIndex == 0 )
		[[UIApplication sharedApplication] openURL: [NSURL URLWithString: @"http://www.nanogames.com.br"]];
}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes de iniciarmos uma transição para esta view.

==============================================================================================*/

- ( void )onBeforeTransition
{
	[self setState: CREDITS_VIEW_STATE_HIDDEN];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	[self setState: CREDITS_VIEW_STATE_SHOWING_STEP_0];
}

// Fim da implementação da classe
@end
