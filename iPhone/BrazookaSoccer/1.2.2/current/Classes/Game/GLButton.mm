#include "GLButton.h"
#include "Exceptions.h"

// Número de objetos contidos no grupo
#define GL_BT_N_OBJS 2

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

GLButton::GLButton( const char* pOffImgsId, const char* pOffDescName, const char* pOnImgsId, const char* pOnDescName, GLButtonListener* pListener )
		 : ObjectGroup( GL_BT_N_OBJS ), pListener( pListener ),
		   pButtonOff( NULL ), pButtonOn( NULL )
{
	build( pOffImgsId, pOffDescName, pOnImgsId, pOnDescName );
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/
		
void GLButton::build( const char* pOffImgsId, const char* pOffDescName, const char* pOnImgsId, const char* pOnDescName )
{
	pButtonOff = new Sprite( pOffImgsId, pOffDescName );
	if( !pButtonOff || !insertObject( pButtonOff ) )
	{
		delete pButtonOff;
		goto Error;
	}
	
	pButtonOn = new Sprite( pOnImgsId, pOnDescName );
	if( !pButtonOn || !insertObject( pButtonOn ) )
	{
		delete pButtonOn;
		goto Error;
	}

	pButtonOn->setAnimSequence( 0, false );
	pButtonOff->setAnimSequence( 0, false );
	setSize( pButtonOn->getWidth(), pButtonOn->getHeight() );
	
	setState( false );
	
	return;
	
	// Label de tratamento de erros
	Error:
		#if TARGET_IPHONE_SIMULATOR
			throw ConstructorException( "GLButton::build : Unable to create object" );
		#else
			throw ConstructorException();
		#endif
}
		
/*==============================================================================================

MÉTODO setState
	Indica o estado do botão.

==============================================================================================*/

void GLButton::setState( bool on )
{	
	pButtonOff->setVisible( !on );
	pButtonOn->setVisible( on );
}
