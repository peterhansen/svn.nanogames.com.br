#include "Point3f.h"

// Game
#include "Iso_Defines.h"
#include "GameInfo.h"

// C++
#include <string.h> // memset

// Construtor
GameInfo::GameInfo( void )
		: cameraAutoAdjust( true ),
		  currTry( -1 ),
		  currLevel( -1 ),
		  levelScore( 0 ),
		  points( 0 ),
		  gameMode( GAME_MODE_UNDEFINED ),
		  iSavedBallPosition(),
		  iSavedTargetPosition(),
		  barrierSize( 0 ),
		  iBackupAngularSpeed( 0.0f ),
		  iBackupAngularSpeedAxis()
{
	memset( tries, 0, sizeof( GameMode ) * FOUL_CHALLENGE_TRIES_PER_FOUL );
	memset( barrierPlayersVector, 0, sizeof( int8 ) * BARRIER_MAX_N_PLAYERS );
}
	
// Reseta as vaiáveis de controles do jogo
void GameInfo::onNewGame( void )
{
	currTry = -1;
	currLevel = -1;
	
	levelScore = 0;
	points = 0;
	
	iSavedBallPosition.set();
	iSavedTargetPosition.set();
	barrierSize = 0;
	iBackupAngularSpeed = 0.0f;
	iBackupAngularSpeedAxis.set();

	memset( tries, 0, sizeof( GameMode ) * FOUL_CHALLENGE_TRIES_PER_FOUL );
	memset( barrierPlayersVector, 0, sizeof( int8 ) * BARRIER_MAX_N_PLAYERS );
}
