#include "IsoKeeper.h"

#include "Ball_Defines.h"
#include "GameUtils.h"
#include "Iso_Defines.h"

#include "Exceptions.h"
#include "MathFuncs.h"
#include "Quad.h"

// OLD : Agora deixamos alocadas apenas as animações do pulo atual e do goeliro parado
// Número de objetos no grupo
//#define GOAL_KEEPER_N_OBJS ( GOAL_KEEPER_N_SPRITES + GOAL_KEEPER_SHADOW_N_SPRITES )
//
//// Índices dos sprites contidos no grupo
//#define GK_SPRITE_STOPPED_INDEX				0
//#define GK_SPRITE_JUMPING_UP_INDEX			1
//#define GK_SPRITE_JUMPING_BOTTOM_INDEX		2
//#define GK_SPRITE_JUMPING_RIGHT_INDEX			3
//#define GK_SPRITE_JUMPING_RIGHT_HIGH_INDEX	4
//#define GK_SPRITE_JUMPING_RIGHT_MED_INDEX		5
//#define GK_SPRITE_JUMPING_RIGHT_LOW_INDEX		6
//#define GK_SPRITE_JUMPING_LEFT_INDEX			7
//#define GK_SPRITE_JUMPING_LEFT_HIGH_INDEX		8
//#define GK_SPRITE_JUMPING_LEFT_MED_INDEX		9
//#define GK_SPRITE_JUMPING_LEFT_LOW_INDEX		9 // Não possuímos esquerda-baixo...

// Número de objetos no grupo
#define GOAL_KEEPER_N_OBJS 4

// Índices dos objetos contidos no grupo
#define GOAL_KEEPER_OBJ_INDEX_STOPPED_SHADOW	0
#define GOAL_KEEPER_OBJ_INDEX_STOPPED_KEEPER	1
#define GOAL_KEEPER_OBJ_INDEX_CURR_JUMP_SHADOW	2
#define GOAL_KEEPER_OBJ_INDEX_CURR_JUMP_KEEPER	3

// Índices das animações do goleiro nos vetores goalKeeper e shadow
#define GK_SPRITE_STOPPED_INDEX				0
#define GK_SPRITE_JUMPING_UP_INDEX			1
#define GK_SPRITE_JUMPING_BOTTOM_INDEX		2
#define GK_SPRITE_JUMPING_RIGHT_INDEX		3
#define GK_SPRITE_JUMPING_RIGHT_HIGH_INDEX	4
#define GK_SPRITE_JUMPING_RIGHT_MED_INDEX	5
#define GK_SPRITE_JUMPING_RIGHT_LOW_INDEX	6
#define GK_SPRITE_JUMPING_LEFT_INDEX		7
#define GK_SPRITE_JUMPING_LEFT_HIGH_INDEX	8
#define GK_SPRITE_JUMPING_LEFT_MED_INDEX	9
#define GK_SPRITE_JUMPING_LEFT_LOW_INDEX	9 // OBS: Não possuímos esquerda-baixo

// Índices das animações dos sprites
#define GK_SPRITE_STOPPED_ANIM_STOPPED				0
#define GK_SPRITE_STOPPED_ANIM_PREPARING			1
#define GK_SPRITE_STOPPED_ANIM_PREPARING_REVERSE	2

#define GK_SPRITE_JUMPING_UP_ANIM			0
#define GK_SPRITE_JUMPING_BOTTOM_ANIM		0
#define GK_SPRITE_JUMPING_LEFT_ANIM			0
#define GK_SPRITE_JUMPING_LEFT_LOW_ANIM		0
#define GK_SPRITE_JUMPING_LEFT_MED_ANIM		0
#define GK_SPRITE_JUMPING_LEFT_HIGH_ANIM	0
#define GK_SPRITE_JUMPING_RIGHT_ANIM		0
#define GK_SPRITE_JUMPING_RIGHT_LOW_ANIM	0
#define GK_SPRITE_JUMPING_RIGHT_MED_ANIM	0
#define GK_SPRITE_JUMPING_RIGHT_HIGH_ANIM	0

// Define o alpha da sombra do goleiro
#define GOAL_KEEPER_SHADOW_ALPHA 0.302f

// Tempo máximo de duração de um frame das animações do goleiro
#define GOAL_KEEPER_MAX_FRAME_DURATION 0.05f

// Macros auxiliares para verificação de estado
#define IS_JUMPING_LEFT( state ) ( ( state == 	GOAL_KEEPER_JUMPING_LEFT ) || ( state == 	GOAL_KEEPER_JUMPING_LEFT_LOW )|| ( state == 	GOAL_KEEPER_JUMPING_LEFT_MED ) || ( state == GOAL_KEEPER_JUMPING_LEFT_HIGH ) )
#define IS_JUMPING_RIGHT( state ) ( ( state == 	GOAL_KEEPER_JUMPING_RIGHT ) || ( state == 	GOAL_KEEPER_JUMPING_RIGHT_LOW ) || ( state == 	GOAL_KEEPER_JUMPING_RIGHT_MED ) || ( state == 	GOAL_KEEPER_JUMPING_RIGHT_HIGH ) )

// Tempo de duração do atrito
#define GOAL_KEEPER_FRICTION_DUR 0.8f

// Duração das etapadas de animação que não correspondem a pulos
#define GOAL_KEEPER_NON_JUMP_ANIM_STEP_DUR 0.05f

// distâncias máximas que o goleiro consegue percorrer com um pulo. Os valores referem-se
// à variação da posição dos seus pés em relação à posição original.
#define MAX_X_DISTANCE 1.65f
#define MAX_Y_DISTANCE 0.90f

// Real Y a partir de qual o goleiro executará pulos altos
#define GOAL_KEEPER_HIGH_JUMP_Y ( MAX_Y_DISTANCE * 0.7f )

// Real Y a partir de qual o goleiro executará pulos de média altura
#define GOAL_KEEPER_MEDIUM_JUMP_Y ( MAX_Y_DISTANCE * 0.5f )

// Real X a partir de qual o goleiro executará pulos longos
#define GOAL_KEEPER_LONG_JUMP_X ( MAX_X_DISTANCE * 0.6f )

// Tempo em segundos que o goleiro leva no frame de início de pulo (tempo de reflexo em relação ao chute - quanto maior, mais lento)
#define GOAL_KEEPER_MIN_JUMP_TIME 0.33f

// Porcentagem máxima do gol que o goleiro se desloca ao se reposicionar de acordo com a barreira
#define MAX_GOAL_PERCENT 0.7f

// Offsets do goleiro no modo isométrico
#define ISO_KEEPER_STOPPED_X_OFFSET			  3.0f
#define ISO_KEEPER_STOPPED_Y_OFFSET			 -5.0f

// Tempo mínimo de reação do goleiro
#define KEEPER_MIN_WAIT_TIME 0.2f

/*===============================================================================

CONSTRUTOR

================================================================================*/

IsoKeeper::IsoKeeper( void )
		  : GenericIsoPlayer( GOAL_KEEPER_N_OBJS ),
			currSpriteIndex( -1 ),
			jumpSpriteIndex( -1 ),
			state( GOAL_KEEPER_UNDEFINED ),
			nextState( GOAL_KEEPER_UNDEFINED ),
			jumpSpeed(),
			initialPosition(),
			jumpXPosition( 0.0f ),
			instantYSpeed( 0.0f ),
			accFrameTime( 0.0f ),
			accJumpTime( 0.0f ),
			waitTime( 0.0f ),
			timePerFrame( 0.0f ),
			returnedToFloor( false ),
			jumpTime( 0.0f ),
			floorTime( 0.0f ),
			firstJumpStep( 0 )
{
	// Inicializa os vetores
	memset( goalKeeper, 0, sizeof( Sprite* ) * GOAL_KEEPER_N_SPRITES );
	memset( shadow, 0, sizeof( Sprite* ) * GOAL_KEEPER_SHADOW_N_SPRITES );
	
	build();
}

/*===============================================================================

DESTRUTOR

================================================================================*/

IsoKeeper::~IsoKeeper( void )
{
	clear();
}

/*===============================================================================

MÉTODO build
   Inicializa o objeto.

=============================================================================== */

// OLD : Antigamente carregávamos todas as animações ao mesmo tempo. Agora, carregamos apenas a animação do pulo atual e do goleiro parado
//#define GOAL_KEEPER_BUILD_BUFFER_SIZE 6
//
//void IsoKeeper::build( void )
//{
//	char* preffix = "gk";
//	char suffix[ GOAL_KEEPER_N_SPRITES ][3] = { "p", "a", "b", "d", "da", "dm", "db", "e", "ea", "em" };
//	
//	char buffer[GOAL_KEEPER_BUILD_BUFFER_SIZE];
//	
//	#if TARGET_IPHONE_SIMULATOR
//		char nameAuxBuffer[ OBJECT_NAME_MAX_LENGTH ];
//	#endif
//
//	for( uint8 i = 0 ; i < GOAL_KEEPER_N_SPRITES ; ++i )
//	{
//		snprintf( buffer, GOAL_KEEPER_BUILD_BUFFER_SIZE, "%ss%s", preffix, suffix[i] );
//		
//		#if CONFIG_TEXTURES_16_BIT
//			shadow[i] = new Sprite( buffer, buffer, ResourceManager::Get16BitTexture2D );
//		#else
//			shadow[i] = new Sprite( buffer, buffer, ResourceManager::GetTexture2D );
//		#endif
//		if( !shadow[i] || !insertObject( shadow[i] ) )
//		{
//			delete shadow[i];
//			goto Error;
//		}
//		shadow[i]->setVertexSetColor( Color( 0.0f, 0.0f, 0.0f, GOAL_KEEPER_SHADOW_ALPHA ) );
//		shadow[i]->setActive( false );
//		shadow[i]->setVisible( false );
//		
//		#if TARGET_IPHONE_SIMULATOR
//			sprintf( nameAuxBuffer, "GoalKeeperShadowSprite_%d", i );
//			shadow[i]->setName( nameAuxBuffer );
//		#endif
//		
//		snprintf( buffer, GOAL_KEEPER_BUILD_BUFFER_SIZE, "%s%s", preffix, suffix[i] );
//
//		#if CONFIG_TEXTURES_16_BIT
//			goalKeeper[i] = new Sprite( buffer, buffer, ResourceManager::Get16BitTexture2D );
//		#else
//			goalKeeper[i] = new Sprite( buffer, buffer, ResourceManager::GetTexture2D );
//		#endif
//		if( !goalKeeper[i] || ! insertObject( goalKeeper[i] ) )
//		{
//			delete goalKeeper[i];
//			goto Error;
//		}
//		
//		goalKeeper[i]->setActive( false );
//		goalKeeper[i]->setVisible( false );
//
//		goalKeeper[i]->setListener( static_cast< SpriteListener* >( this ) );
//		
//		#if TARGET_IPHONE_SIMULATOR
//			sprintf( nameAuxBuffer, "GoalKeeperSprite_%d", i );
//			goalKeeper[i]->setName( nameAuxBuffer );
//		#endif
//	}
//	
//	// Determina o tamanho do grupo
//	setSize( goalKeeper[0]->getWidth(), goalKeeper[0]->getHeight() );
//
//	return;
//
//	// Label de tratamento de erros
//	Error:
//		clear();
//		#if TARGET_IPHONE_SIMULATOR
//			throw ConstructorException( "IsoKeeper::build( void ): Unable to create object" );
//		#else
//			throw ConstructorException();
//		#endif
//}
//
//#undef GOAL_KEEPER_BUILD_BUFFER_SIZE

void IsoKeeper::build( void )
{
	// O goleiro parado sempre está na memória
	allocAnim( GK_SPRITE_STOPPED_INDEX );
	
	// Coloca o goleiro em seu estado inicial
	reset();

	// Determina o tamanho do grupo
	setSize( goalKeeper[ GK_SPRITE_STOPPED_INDEX ]->getWidth(), goalKeeper[ GK_SPRITE_STOPPED_INDEX ]->getHeight() );
	
	float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, REAL_KEEPER_HEIGHT, 0.0f ).y ) / goalKeeper[ GK_SPRITE_STOPPED_INDEX ]->getStepFrameHeight() );
	setScale( scaleFactor, scaleFactor );

	return;
}

/*===============================================================================

MÉTODO clear
   Limpa as variáveis do objeto.

=============================================================================== */

void IsoKeeper::clear( void )
{
	currSpriteIndex = -1;
	jumpSpriteIndex = -1;
	state = GOAL_KEEPER_UNDEFINED;
	nextState = GOAL_KEEPER_UNDEFINED;
	jumpSpeed.set();
	initialPosition.set();
	jumpXPosition = 0.0f;
	instantYSpeed = 0.0f;
	accFrameTime = 0.0f;
	accJumpTime = 0.0f;
	waitTime = 0.0f;
	timePerFrame = 0.0f;
	returnedToFloor = false;
	jumpTime = 0.0f;
	floorTime = 0.0f;
	firstJumpStep = 0;
	
	memset( goalKeeper, 0, sizeof( Sprite* ) * GOAL_KEEPER_N_SPRITES );
	memset( shadow, 0, sizeof( Sprite* ) * GOAL_KEEPER_SHADOW_N_SPRITES );
}

/*===============================================================================

MÉTODO reset
   Reinicializa o objeto.

=============================================================================== */

void IsoKeeper::reset( const Point3f* pPosition )
{
	setState( GOAL_KEEPER_STOPPED );

	if( pPosition )
		setRealPosition( pPosition->x, pPosition->y, ISO_KEEPER_REAL_Z );
	else
		setRealPosition( 0.0f, 0.0f, ISO_KEEPER_REAL_Z );

	initialPosition = *getRealPosition();

	setBallCollisionTest( true );
}

/*===============================================================================

MÉTODO update
	Atualiza o objeto.

=============================================================================== */

bool IsoKeeper::update( float timeElapsed )
{
	if( ( state <= GOAL_KEEPER_STOPPED ) || !isActive() )
		return false;
	
	
	accFrameTime += timeElapsed;
	
	int16 currAnimSeqStep = goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceStep();
	if( !returnedToFloor && ( ( state == GOAL_KEEPER_PREPARING ) || ( state == GOAL_KEEPER_JUMPING_UP_GOING_DOWN ) || ( currAnimSeqStep < firstJumpStep ) ) )
	{
		moveRealPosition( ( jumpXPosition - getRealPosition()->x ) * timeElapsed, 0.0f, 0.0f );
		if (jumpXPosition > 0)	
		{
			if (realPosition.x > REAL_RIGHT_POST_END )
				realPosition.x = REAL_RIGHT_POST_END;
		}
		else
		{
			if (realPosition.x < REAL_LEFT_POST_END)
				realPosition.x = REAL_LEFT_POST_END;
		}

		
		if( state == GOAL_KEEPER_PREPARING )
		{
			if( currAnimSeqStep == 0 )
			{
				if( accFrameTime > waitTime )
				{
					if( waitTime > 0.0f )
						accFrameTime = static_cast< float >( fmod( accFrameTime, waitTime ) );
					else
						accFrameTime = 0.0f;

					goalKeeper[ currSpriteIndex ]->setCurrentAnimSequenceStep( 1 );
				}
			}
			else
			{
				if( accFrameTime > GOAL_KEEPER_NON_JUMP_ANIM_STEP_DUR )
				{
					accFrameTime = static_cast< float >( fmod( accFrameTime, GOAL_KEEPER_NON_JUMP_ANIM_STEP_DUR ) );
					//<DM>				
					if (expectedBallTarget.x < (REAL_SMALL_AREA_WIDTH*0.5f) && expectedBallTarget.x > (-REAL_SMALL_AREA_WIDTH*0.5f) )						
							setState( nextState );				
					initialPosition=realPosition;
					//</DM>					
				}
			}
		}
		else
		{
			if( accFrameTime > GOAL_KEEPER_NON_JUMP_ANIM_STEP_DUR )
			{
				accFrameTime = static_cast< float >( fmod( accFrameTime, GOAL_KEEPER_NON_JUMP_ANIM_STEP_DUR ) );
				goalKeeper[ currSpriteIndex ]->setCurrentAnimSequenceStep( currAnimSeqStep + 1 );
			}
		}
		return true;
	}
	
	accJumpTime += timeElapsed;
	
	Point3f aux( initialPosition.x + ( jumpSpeed.x * accJumpTime ),
				 initialPosition.y + ( jumpSpeed.y * accJumpTime ) + (( GRAVITY_ACCELERATION_KEEPER * accJumpTime * accJumpTime ) * 0.5f ),
				 getRealPosition()->z );

	if( NanoMath::fltn( aux.y, 0.0f ) || returnedToFloor )
	{
		// Verifica se tocou no chão após o pulo
		if( ( accJumpTime > jumpTime * 0.5 ) && !returnedToFloor )
		{
			returnedToFloor = true;
			initialPosition.x = aux.x;
			accJumpTime = 0.0f;
			jumpSpeed.y = 0.0f;
		}
		
		aux.y = 0.0f;

		// Faz o goleiro deslizar quando ele volta a tocar o chão depois de um pulo
		if( returnedToFloor )
		{
			// OBS: Não queremos que o goleiro deslize no chão após um pulo para cima ou uma defesa estática
			if( ( state == GOAL_KEEPER_JUMPING_UP_GOING_UP ) || ( state == GOAL_KEEPER_JUMPING_UP_GOING_DOWN ) || ( state == GOAL_KEEPER_JUMPING_BOTTOM ) )
			{
				jumpSpeed.x = 0.0f;
			}
			else
			{
				floorTime += timeElapsed;
				float totalArea = GOAL_KEEPER_FRICTION_DUR * jumpSpeed.x * 0.5f;
				
				if( floorTime >= GOAL_KEEPER_FRICTION_DUR )
				{
					aux.x = initialPosition.x + totalArea;
					initialPosition.x = aux.x;
					jumpSpeed.x = 0.0f;
				}
				else
				{
					aux.x = initialPosition.x + totalArea - ( ( GOAL_KEEPER_FRICTION_DUR - floorTime ) * ( jumpSpeed.x * ( GOAL_KEEPER_FRICTION_DUR - floorTime ) ) * 0.5f );
				}
			}
		}
	}

	setRealPosition( &aux );
		
	if( accFrameTime < timePerFrame )
		return false;
	
	accFrameTime = static_cast< float >( fmod( accFrameTime, timePerFrame ) );

	if( !goalKeeper[ currSpriteIndex ]->setCurrentAnimSequenceStep( goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceStep() + 1 ) )
	{
		// OBS: GAMBIARRA
		if( ( nextState == GOAL_KEEPER_JUMPING_UP_GOING_DOWN ) && returnedToFloor )
			setState( nextState );
	}

	return true;
}

/*===============================================================================

MÉTODO prepare
   Reposiciona o goleiro, de acordo com a posição da falta e do número de
jogadores na barreira.

=============================================================================== */

void IsoKeeper::prepare( const Point3f *pBallPosition, uint8 barrierPlayers )
{
	// O goleiro se posiciona no canto oposto ao da bola
	
	float percent = MAX_GOAL_PERCENT * 
					 ( barrierPlayers / ( float ) BARRIER_MAX_N_PLAYERS ) * 
					 ( pBallPosition->x / ( REAL_FIELD_WIDTH * 0.5f ) );
	
	Point3f aux = *getRealPosition();
	aux.x = -( REAL_GOAL_WIDTH * 0.5f ) * percent;
	setRealPosition( &aux );
	
	initialPosition = *getRealPosition();
}

/*===============================================================================

MÉTODO setJumpDirection
   Define a direção do pulo do goleiro, de acordo com a direção e força da
bola e número de jogadores na barreira.

=============================================================================== */

void IsoKeeper::setJumpDirection( const Point3f *pDirection, float timeToGoal )
{
	Point3f direction = *pDirection;

	// o pulo total do goleiro depende da sua posição atual
	direction -= realPosition;

	// limita o pulo do goleiro
	if( direction.x > MAX_X_DISTANCE )
		direction.x = MAX_X_DISTANCE;
	else if( direction.x < -MAX_X_DISTANCE )
		direction.x = -MAX_X_DISTANCE;

	if( direction.y > MAX_Y_DISTANCE )
		direction.y = MAX_Y_DISTANCE;
	else if( direction.y < 0.0f )
		direction.y = 0.0f;

	// Verifica que salto o goleiro deverá utilizar
	GoalKeeperState auxState = GOAL_KEEPER_UNDEFINED;
	
	// pula para cima
	jumpSpeed.y = static_cast< float >( sqrt( -4.0f * GRAVITY_ACCELERATION_KEEPER * direction.y ) * 0.5f );
	jumpTime = -jumpSpeed.y / GRAVITY_ACCELERATION_KEEPER;
	
	if( jumpTime < GOAL_KEEPER_MIN_JUMP_TIME )
		jumpTime = GOAL_KEEPER_MIN_JUMP_TIME;

	jumpSpeed.x = direction.x / jumpTime;

	if( fabs( direction.x ) <= GOAL_KEEPER_LONG_JUMP_X )
	{
		if( direction.y >= GOAL_KEEPER_HIGH_JUMP_Y )
		{
			auxState = GOAL_KEEPER_JUMPING_UP_GOING_UP;
		}
		else
		{
			jumpSpeed.x = 0.0f;
			jumpSpeed.y = 0.0f;

			auxState = GOAL_KEEPER_JUMPING_BOTTOM;
		}
	}
	else
	{
		if( direction.y >= GOAL_KEEPER_HIGH_JUMP_Y )
		{
			auxState = direction.x < 0.0f ? GOAL_KEEPER_JUMPING_RIGHT_HIGH : GOAL_KEEPER_JUMPING_LEFT_HIGH;
		}
		else
		{
			if( direction.y < GOAL_KEEPER_MEDIUM_JUMP_Y )
				auxState = direction.x < 0.0f ? GOAL_KEEPER_JUMPING_RIGHT_LOW : GOAL_KEEPER_JUMPING_LEFT_LOW;
			else
				auxState = direction.x < 0.0f ? GOAL_KEEPER_JUMPING_RIGHT_MED : GOAL_KEEPER_JUMPING_LEFT_MED;
		}
	}

	// Calcula o tempo que o goleiro espera antes de iniciar o pulo
	int16 nJumpFrames = getNJumpFrames( &firstJumpStep, auxState );
	timePerFrame = jumpTime / nJumpFrames;

	if( NanoMath::fltn( timeToGoal, jumpTime ) )
		waitTime = 0.0f;
	else
		waitTime = timeToGoal - jumpTime;
	
	if( waitTime < KEEPER_MIN_WAIT_TIME )
		waitTime = KEEPER_MIN_WAIT_TIME;
///TODOO: responder de onde veio este 0.5f?
	jumpXPosition = realPosition.x + direction.x * waitTime * 0.5f;
	
//	LOG( "GOLEIRO(1): %.3f, %.3f, %.3f -> %.3f, %.3f / %d", pDirection->x, pDirection->y, pDirection->z, jumpXPosition, waitTime, auxState );
//	LOG( "GOLEIRO(2): %.3f, %.3f, %.3f / %.3f, %.3f, %.3f", realPosition.x, realPosition.y, realPosition.z, direction.x, direction.y, direction.z );
	
	// Zera os controladores de tempo
	accFrameTime = 0.0f;
	accJumpTime = 0.0f;
	returnedToFloor = false;
	floorTime = 0.0f;

	setState( auxState );
}

/*===================================================================================

MÉTODO allocAnim
   Aloca as imagens correspondentes ao pulo que o goleiro irá utilizar.

=====================================================================================*/

#define GOAL_KEEPER_BUILD_BUFFER_SIZE 6

void IsoKeeper::allocAnim( int16 spriteIndex )
{
	if( spriteIndex > -1 )
	{
		char preffix[3] = { 'g', 'k', '\0' };
		char suffix[ GOAL_KEEPER_N_SPRITES ][3] = { "p", "a", "b", "d", "da", "dm", "db", "e", "ea", "em" };
		
		char buffer[GOAL_KEEPER_BUILD_BUFFER_SIZE];
		
		if( shadow[ spriteIndex ] == NULL )
		{
			snprintf( buffer, GOAL_KEEPER_BUILD_BUFFER_SIZE, "%ss%s", preffix, suffix[ spriteIndex ] );
			
			#if CONFIG_TEXTURES_16_BIT
				shadow[ spriteIndex ] = new Sprite( buffer, buffer, ResourceManager::Get16BitTexture2D );
			#else
				shadow[ spriteIndex ] = new Sprite( buffer, buffer, ResourceManager::GetTexture2D );
			#endif
			if( !shadow[ spriteIndex ] || !insertObject( shadow[ spriteIndex ], NULL ) )
			{
				DELETE( shadow[ spriteIndex ] );
				goto Error;
			}
			shadow[ spriteIndex ]->setVertexSetColor( Color( 0.0f, 0.0f, 0.0f, GOAL_KEEPER_SHADOW_ALPHA ) );
			shadow[ spriteIndex ]->setActive( false );
			shadow[ spriteIndex ]->setVisible( false );
		}
		
		if( goalKeeper[ spriteIndex ] == NULL )
		{
			snprintf( buffer, GOAL_KEEPER_BUILD_BUFFER_SIZE, "%s%s", preffix, suffix[ spriteIndex ] );

			#if CONFIG_TEXTURES_16_BIT
				goalKeeper[ spriteIndex ] = new Sprite( buffer, buffer, ResourceManager::Get16BitTexture2D );
			#else
				goalKeeper[ spriteIndex ] = new Sprite( buffer, buffer, ResourceManager::GetTexture2D );
			#endif
			if( !goalKeeper[ spriteIndex ] || !insertObject( goalKeeper[ spriteIndex ], NULL ) )
			{
				DELETE( goalKeeper[ spriteIndex ] );
				goto Error;
			}
			
			goalKeeper[ spriteIndex ]->setActive( false );
			goalKeeper[ spriteIndex ]->setVisible( false );

			goalKeeper[ spriteIndex ]->setListener( static_cast< SpriteListener* >( this ) );
		}
	}
	return;
	
	// Label de tratamento de erros
	Error:
		clear();

		#if TARGET_IPHONE_SIMULATOR
			throw ConstructorException( "IsoKeeper::allocAnim( int16 ): Unable to alloc anim" );
		#else
			throw ConstructorException();
		#endif
}

#undef GOAL_KEEPER_BUILD_BUFFER_SIZE

/*===================================================================================

MÉTODO freeCurrAnim
   Deleta as imagens correspondentes ao pulo que o goleiro utilizou da última vez.

=====================================================================================*/

void IsoKeeper::freeCurrAnim( void )
{
	if( ( jumpSpriteIndex >= 0 ) && ( jumpSpriteIndex != GK_SPRITE_STOPPED_INDEX ) )
	{
		removeObject( GOAL_KEEPER_OBJ_INDEX_CURR_JUMP_KEEPER );
		goalKeeper[ jumpSpriteIndex ] = NULL;
	
		removeObject( GOAL_KEEPER_OBJ_INDEX_CURR_JUMP_SHADOW );
		shadow[ jumpSpriteIndex ] = NULL;
		
		jumpSpriteIndex = -1;
	}
}

/*===================================================================================

MÉTODO getNJumpFrames
   Retorna o número de passos da animação em quais o goleiro está realmente pulando.

=====================================================================================*/

int16 IsoKeeper::getNJumpFrames( int16* firstJumpAnimStep, GoalKeeperState s )
{	
	int16 nJumpFrames = 0;
	*firstJumpAnimStep = -1;
	switch( s )
	{
		case GOAL_KEEPER_UNDEFINED:
		case GOAL_KEEPER_STOPPED:
		case GOAL_KEEPER_PREPARING:
		case GOAL_KEEPER_JUMPING_BOTTOM:
		case GOAL_KEEPER_JUMPING_LEFT:
		case GOAL_KEEPER_JUMPING_RIGHT:
		case GOAL_KEEPER_JUMPING_UP_GOING_DOWN:
			break;

		case GOAL_KEEPER_JUMPING_UP_GOING_UP:
			nJumpFrames = 3;
			*firstJumpAnimStep = 0;
			break;

		case GOAL_KEEPER_JUMPING_LEFT_LOW: // OBS: GOAL_KEEPER_JUMPING_LEFT_LOW está usando GOAL_KEEPER_JUMPING_LEFT_MED já que não temos uma sequência específica
		case GOAL_KEEPER_JUMPING_LEFT_MED:
			nJumpFrames = 2;
			*firstJumpAnimStep = 3;
			break;
			
		case GOAL_KEEPER_JUMPING_LEFT_HIGH:
			nJumpFrames = 5;
			*firstJumpAnimStep = 1;
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_LOW:
			nJumpFrames = 4;
			*firstJumpAnimStep = 1;
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_MED:
			nJumpFrames = 3;
			*firstJumpAnimStep = 1;
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_HIGH:
			nJumpFrames = 5;
			*firstJumpAnimStep = 1;
			break;
	}
	return nJumpFrames;
}

/*===============================================================================

MÉTODO setState
   Define o estado atual do goleiro.

=============================================================================== */

void IsoKeeper::setState( GoalKeeperState s )
{
	if( ( s > GOAL_KEEPER_PREPARING ) && ( nextState != s ) )
	{
		state = GOAL_KEEPER_PREPARING;
		nextState = s;
		
		// Garante que não temos uma animação já alocada
		freeCurrAnim();
		
		// Aloca a animação do pulo
		int16 dummy;
		jumpSpriteIndex = getSpriteIndexForState( &dummy, nextState );
		allocAnim( jumpSpriteIndex );
	}
	else
	{
		state = s;
		
		if( state == GOAL_KEEPER_JUMPING_UP_GOING_UP )
			nextState = GOAL_KEEPER_JUMPING_UP_GOING_DOWN;
		else
			nextState = GOAL_KEEPER_UNDEFINED;
	}
//<DM> evita um EXC_BAD_ACCESS ocasional durante replay </DM>

	if( currSpriteIndex >= 0 /*<DM>*/ && shadow[ currSpriteIndex ]!=NULL && currSpriteIndex<GOAL_KEEPER_SHADOW_N_SPRITES/*</DM>*/)
	{
		shadow[ currSpriteIndex ]->setVisible( false );
		shadow[ currSpriteIndex ]->setAnimSequence( -1, false );
		
		goalKeeper[ currSpriteIndex ]->setActive( false );
		goalKeeper[ currSpriteIndex ]->setVisible( false );
		goalKeeper[ currSpriteIndex ]->setAnimSequence( -1, false );
		
		// OLD:  Estava dando uma travada perceptível na bola
//		freeCurrAnim();
	}

	int16 animSeqIndex;
	currSpriteIndex = getSpriteIndexForState( &animSeqIndex, state );
	
	// OLD: Estava dando uma travada perceptível na bola
//	allocAnim( currSpriteIndex );

	shadow[ currSpriteIndex ]->setVisible( true );
	shadow[ currSpriteIndex ]->setAnimSequence( animSeqIndex, false );
	
	goalKeeper[ currSpriteIndex ]->setActive( true );
	goalKeeper[ currSpriteIndex ]->setVisible( true );
	goalKeeper[ currSpriteIndex ]->setAnimSequence( animSeqIndex, false );

	TextureFrame currFrameInfo;
	goalKeeper[ currSpriteIndex ]->getStepFrameInfo( &currFrameInfo, goalKeeper[ currSpriteIndex ]->getCurrAnimSequence(), goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceStep() );
	
	setAnchor( ( currFrameInfo.offsetX * getScale()->x ) + (( currFrameInfo.width  + ISO_KEEPER_STOPPED_X_OFFSET ) * getScale()->x * 0.5f ),
			   ( currFrameInfo.offsetY * getScale()->y ) + (( currFrameInfo.height + ISO_KEEPER_STOPPED_Y_OFFSET ) * getScale()->y ), 0.0f );
}

/*===============================================================================

MÉTODO updatePosition
   Função utilitária para converter a posição real do goleiro para uma posição
na tela.

=============================================================================== */

void IsoKeeper::updatePosition( void )
{
	// Atualiza a orientação e o retângulo de colisão go goleiro
	updateFrame();
	
	Point3f goalKeeperRealPos = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( goalKeeperRealPos.x, goalKeeperRealPos.y, 0.0f );
}

/*===============================================================================

MÉTODO updateFrame
   Atualiza o frame atual do jogador, de acordo com a direção para a qual ele
está virado.
	OBS: Esse método dá MTO trabalho!!!

=============================================================================== */

void IsoKeeper::updateFrame( void )
{	
	float width = REAL_KEEPER_WIDTH, height;
	Point3f top, right, offset;
	ORIENTATION orientation = ORIENT_BOTTOM;
	
	int16 currAnimSeqStep = goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceStep();
	
	switch( state )
	{
		case GOAL_KEEPER_STOPPED:
		case GOAL_KEEPER_PREPARING:
			{
				top.set( VECTOR_UP_90 );
				right.set( VECTOR_RIGHT_90 );

				if( currAnimSeqStep == 0 )
					width = REAL_KEEPER_INTERMEDIATE_WIDTH;
				else
					width = REAL_KEEPER_WIDTH_ARMS;
				
				height = REAL_KEEPER_HEIGHT;
			}
			break;
			
		case GOAL_KEEPER_JUMPING_UP_GOING_DOWN:
			{
				top.set( VECTOR_UP_90 );
				right.set( VECTOR_RIGHT_90 );

				if( currAnimSeqStep == 0 )
					width = REAL_KEEPER_WIDTH_ARMS;
				else
					width = REAL_KEEPER_INTERMEDIATE_WIDTH;
				
				height = REAL_KEEPER_HEIGHT;
			}
			break;

		case GOAL_KEEPER_JUMPING_BOTTOM:
			{
				top.set( VECTOR_UP_90 );
				right.set( VECTOR_RIGHT_90 );

				width = REAL_KEEPER_INTERMEDIATE_WIDTH;
				height = REAL_KEEPER_LOW_HEIGHT;
			}
			break;

		case GOAL_KEEPER_JUMPING_UP_GOING_UP:
			{
				top.set( VECTOR_UP_90 );
				right.set( VECTOR_RIGHT_90 );

				width = REAL_KEEPER_WIDTH;

				if( currAnimSeqStep == 0 )
					height = REAL_KEEPER_HEIGHT;
				else
					height = REAL_KEEPER_HEIGHT_ARMS;
			}
			break;

		case GOAL_KEEPER_JUMPING_RIGHT:
			if( currAnimSeqStep == 0 )
			{
				top.set( VECTOR_UP_40_LEFT );
				right.set( VECTOR_RIGHT_90 );

				height = REAL_KEEPER_LOW_HEIGHT;
				width = REAL_KEEPER_INTERMEDIATE_WIDTH;
			}
			else
			{
				top.set( VECTOR_UP_15_LEFT );
				right.set( VECTOR_RIGHT_30_LEFT);
				
				height = REAL_KEEPER_INTERMEDIATE_HEIGHT;	
				width = REAL_KEEPER_WIDTH;
			}
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_LOW:
			switch( currAnimSeqStep )
			{
				case 0:
					top.set( VECTOR_UP_30_LEFT );
					right.set( VECTOR_RIGHT_30_LEFT );
					
					width = REAL_KEEPER_WIDTH;
					height = REAL_KEEPER_HEIGHT;
					break;
					
				case 1:
					top.set( VECTOR_UP_15_LEFT );
					right.set( VECTOR_RIGHT_30_LEFT );
					
					width = REAL_KEEPER_WIDTH;
					height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
					break;
					
				case 2:
				case 3:
				case 4:
					top.set( VECTOR_UP_15_LEFT );
					right.set( VECTOR_RIGHT_45_LEFT );
					
					width = REAL_KEEPER_LOW_WIDTH;
					height = REAL_KEEPER_INTERMEDIATE_HEIGHT_HIGH;
					break;
					
				case 5:
					top.set( VECTOR_UP_0_LEFT );
					right.set( VECTOR_RIGHT_0_LEFT );
					
					width = REAL_KEEPER_LAID_WIDTH;
					height = REAL_KEEPER_LAID_HEIGHT;
					break;
			}
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_MED:
			switch( currAnimSeqStep )
			{
				case 0:
					{
						top.set( VECTOR_UP_60_LEFT );
						right.set( VECTOR_RIGHT_90 );

						width = REAL_KEEPER_WIDTH_ARMS;
						height = REAL_KEEPER_LOW_HEIGHT;
					}
					break;
					
				case 1:
					{
						top.set( VECTOR_UP_10_LEFT );
						right.set( VECTOR_RIGHT_60_LEFT );

						width = REAL_KEEPER_WIDTH_ARMS;
						height = REAL_KEEPER_HEIGHT_ARMS;
						
						 orientation = ORIENT_BOTTOM_LEFT;
					}
					break;

				case 2:
					{
						top.set( VECTOR_UP_5_LEFT );
						right.set( VECTOR_RIGHT_70_LEFT );

						width = REAL_KEEPER_WIDTH_ARMS;
						height = REAL_KEEPER_HEIGHT_STRETCHED;
						
						 orientation = ORIENT_BOTTOM_LEFT;
					}
					break;

				case 3:
					{
						top.set( VECTOR_UP_10_LEFT );
						right.set( VECTOR_RIGHT_50_LEFT );

						width = REAL_KEEPER_WIDTH_ARMS;
						height = REAL_KEEPER_HEIGHT_ARMS;
					}
					break;
					
				case 4:
				case 5:
					{
						top.set( VECTOR_UP_0_LEFT );
						right.set( VECTOR_RIGHT_50_LEFT );

						width = REAL_KEEPER_INTERMEDIATE_WIDTH;
						height = REAL_KEEPER_HEIGHT_ARMS + 0.5f;
					}
					break;
			}
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_HIGH:
			switch( currAnimSeqStep )
			{
				case 0:
					top.set( VECTOR_UP_60_LEFT );
					right.set( VECTOR_RIGHT_60_LEFT );
					
					width = REAL_KEEPER_WIDTH_ARMS;
					height = REAL_KEEPER_LOW_HEIGHT;
					break;
					
				case 1:
					top.set( VECTOR_UP_50_LEFT );
					right.set( VECTOR_RIGHT_60_LEFT );
					
					width = REAL_KEEPER_WIDTH_ARMS;
					height = REAL_KEEPER_HEIGHT;
					break;
					
				case 2:
					top.set( VECTOR_UP_40_LEFT );
					right.set( VECTOR_RIGHT_60_LEFT );
					
					width = REAL_KEEPER_WIDTH;
					height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
					break;
					
				case 3:
					top.set( VECTOR_UP_35_LEFT );
					right.set( VECTOR_RIGHT_60_LEFT );
					
					width = REAL_KEEPER_WIDTH;
					height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
					break;
					
				case 4:
					{
						top.set( VECTOR_UP_NEG_10_LEFT );
						right.set( VECTOR_RIGHT_45_LEFT );
						
						width = REAL_KEEPER_LAID_WIDTH - 0.1f;
						height = REAL_KEEPER_HEIGHT_ARMS;
						
						orientation = ORIENT_BOTTOM_LEFT;
						
						offset.set( 0.0f, 0.5f, 0.0f );
					}
					break;
					
				case 5:
					{
						top.set( VECTOR_UP_NEG_22_5_LEFT );
						right.set( VECTOR_RIGHT_45_LEFT );
						
						width = REAL_KEEPER_WIDTH + 0.1f;
						height = REAL_KEEPER_HEIGHT_ARMS + 0.5f;
						
						orientation = ORIENT_BOTTOM_LEFT;
						
						offset.set( 0.0f, 0.8f, 0.0f );
					}
					break;
					
				case 6:
				case 7:
					top.set( VECTOR_UP_0_LEFT );
					right.set( VECTOR_RIGHT_60_LEFT );
					
					width = REAL_KEEPER_WIDTH;
					height = REAL_KEEPER_HEIGHT_ARMS + 0.3f;
					
					orientation = ORIENT_BOTTOM_LEFT;
					break;
			}
			break;

		case GOAL_KEEPER_JUMPING_LEFT:
			{
				top.set( VECTOR_UP_60 );
				right.set( VECTOR_RIGHT_80 );

				if( currAnimSeqStep == 0 )
					width = REAL_KEEPER_WIDTH;
				else
					width = REAL_KEEPER_INTERMEDIATE_WIDTH;
				
				height = REAL_KEEPER_HEIGHT;
			}
			break;

		case GOAL_KEEPER_JUMPING_LEFT_LOW: // OBS: Não possuímos esquerda-baixo...
		case GOAL_KEEPER_JUMPING_LEFT_MED:
			switch( currAnimSeqStep )
			{
				case 0:
				case 1:
					{
						top.set( VECTOR_UP_60 );
						right.set( VECTOR_RIGHT_80 );
						width = REAL_KEEPER_WIDTH + ( currAnimSeqStep * ( REAL_KEEPER_INTERMEDIATE_WIDTH - REAL_KEEPER_WIDTH ));
						height = REAL_KEEPER_HEIGHT;
					}
					break;

				case 2:
					{
						top.set( VECTOR_UP_30 );
						right.set( VECTOR_RIGHT_45 );
						width = REAL_KEEPER_LOW_WIDTH;
						height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
						orientation = ORIENT_BOTTOM_RIGHT;
					}
					break;
				
				case 3:
					{
						top.set( VECTOR_UP_25 );
						right.set( VECTOR_RIGHT_80 );
						
						width = REAL_KEEPER_WIDTH;
						height = REAL_KEEPER_INTERMEDIATE_HEIGHT_HIGH;
					}
					break;
					
				case 4:
					{
						top.set( VECTOR_UP_20 );
						right.set( VECTOR_RIGHT_60 );
						
						width = REAL_KEEPER_LOW_WIDTH;
						height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
					}
					break;
					
				case 5:
					{
						top.set( VECTOR_UP_10 );
						right.set( VECTOR_RIGHT_65 );
						
						width = REAL_KEEPER_LAID_WIDTH;
						height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
					}
					break;
			}
			break;

		case GOAL_KEEPER_JUMPING_LEFT_HIGH:
			switch( currAnimSeqStep )
			{
				case 0:
					{
						top.set( VECTOR_UP_80 );
						right.set( VECTOR_RIGHT_90 );
						width = REAL_KEEPER_WIDTH;
						height = REAL_KEEPER_HEIGHT;
					}
					break;
					
				case 1:
					{
						top.set( VECTOR_UP_60 );
						right.set( VECTOR_RIGHT_60 );
						width = REAL_KEEPER_LAID_WIDTH - 0.1f;
						height = REAL_KEEPER_INTERMEDIATE_HEIGHT;
					}
					break;

				case 2:
					{
						top.set( VECTOR_UP_37_5 );
						right.set( VECTOR_RIGHT_70_LEFT );
						width = REAL_KEEPER_INTERMEDIATE_WIDTH;
						height = REAL_KEEPER_HEIGHT_STRETCHED;
						
						orientation = ORIENT_BOTTOM_RIGHT;
					}
					break;
					
				case 3:
					{
						top.set( VECTOR_UP_35 );
						right.set( VECTOR_RIGHT_40 );
						width = REAL_KEEPER_LAID_WIDTH - 0.4f;
						height = REAL_KEEPER_HEIGHT_STRETCHED;
						
						orientation = ORIENT_BOTTOM_RIGHT;
					}
					break;

				case 4:
					{
						top.set( VECTOR_UP_17_5 );
						right.set( VECTOR_RIGHT_15 );
						width = REAL_KEEPER_LAID_WIDTH - 0.2f;
						height = REAL_KEEPER_HEIGHT_STRETCHED;
						
						offset.set( -0.65f, 0.0f );

						orientation = ORIENT_BOTTOM_RIGHT;
					}
					break;

				case 5:
					{
						top.set( VECTOR_UP_10 );
						right.set( VECTOR_RIGHT_15 );
						width = REAL_KEEPER_LAID_WIDTH - 0.15f;
						height = REAL_KEEPER_HEIGHT_STRETCHED;
						
						offset.set( -0.65f, 0.0f );

						orientation = ORIENT_BOTTOM_RIGHT;
					}
					break;
					
				case 6:
					{
						top.set( VECTOR_UP_0 );
						right.set( VECTOR_RIGHT_30 );
						width = REAL_KEEPER_LAID_WIDTH - 0.35f;
						height = REAL_KEEPER_HEIGHT_STRETCHED - 0.45f;
						
						offset.set( -0.65f, 0.0f );

						orientation = ORIENT_BOTTOM_RIGHT;
					}
					break;
			}
			break;
			
#if DEBUG
			default:
				throw InvalidOperationException( "IsoKeeper::updateFrame( void ) : Collision area is inexistent for state" );
				break;
#endif
	}

	collisionArea.set( realPosition + offset, top, right, orientation, width, height );
}

/*===============================================================================

MÉTODO onAnimStepChanged
	Chamado quando o sprite muda de etapa de animação.

================================================================================*/

void IsoKeeper::onAnimStepChanged( Sprite* pSprite )
{
	shadow[ currSpriteIndex ]->setCurrentAnimSequenceStep( goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceStep() );
	
	#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )
		LOG( "Sprite: %s ; Step: %d", pSprite->getName(), goalKeeper[ currSpriteIndex ]->getCurrAnimSequenceStep() );
	#endif
	
	// Garante que o posicionamento dos elementos do grupo estará correto
	updatePosition();
}

/*===============================================================================

MÉTODO render
	Renderiza o objeto.

================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) )

bool IsoKeeper::render( void )
{	
	if( ObjectGroup::render() )
	{
		// Renderiza o quad de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		Point3f pos = GameUtils::isoGlobalToPixels( collisionArea.position );
		Point3f dx = GameUtils::isoGlobalToPixels( collisionArea.right * collisionArea.width );
		Point3f dy = GameUtils::isoGlobalToPixels( collisionArea.top * collisionArea.height );

		dy = -dy;
		
		Point3f top[ 2 ] = {
								pos,
								pos + dy,
							};
		
		glColor4ub( 255, 0, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f right[ 2 ] = {
								pos,
								pos + dx,
							};
		
		glColor4ub( 0, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f others[ 4 ] = {
								pos + dy,
								pos + dy + dx,
								pos + dy + dx,
								pos + dx
							};
		
		glColor4ub( 255, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, others );
		glDrawArrays( GL_LINES, 0, 4 );
		
		Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
		if( collisionArea.normal.z > 0.0f )
			glColor4ub( 179, 179, 179, 255 );
		else
			glColor4ub( 255, 183, 15, 255 );
			
		glVertexPointer( 3, GL_FLOAT, 0, &nomal );
		glDrawArrays( GL_POINTS, 0, 1 );
		
		// Renderiza a moldura do sprite
		Point3f borderPos = *getPosition();
		Point3f border[ 4 ] = {
								borderPos,
								Point3f( borderPos.x, borderPos.y + getHeight() ),
								Point3f( borderPos.x + getWidth(), borderPos.y + getHeight() ),
								Point3f( borderPos.x + getWidth(), borderPos.y )
							  };
		
		glColor4ub( 0, 255, 255, 255 );

		glVertexPointer( 3, GL_FLOAT, 0, border );
		glDrawArrays( GL_LINE_LOOP, 0, 4 );

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		
		return true;
	}
	return false;
}

#endif

/*===============================================================================

MÉTODO getSpriteIndexForState
	Retorna o índice do sprite e o índice da animação que deveremos utilizar para
um dado estado.

================================================================================*/

int8 IsoKeeper::getSpriteIndexForState( int16* pAnimSeq, GoalKeeperState s )
{
	int8 ret;
	switch( s )
	{	
		case GOAL_KEEPER_PREPARING:
			ret = GK_SPRITE_STOPPED_INDEX;
			*pAnimSeq = GK_SPRITE_STOPPED_ANIM_PREPARING;
			break;
			
		case GOAL_KEEPER_JUMPING_BOTTOM:
			ret = GK_SPRITE_JUMPING_BOTTOM_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_BOTTOM_ANIM;
			break;
			
		case GOAL_KEEPER_JUMPING_UP_GOING_DOWN:
			ret = GK_SPRITE_STOPPED_INDEX;
			*pAnimSeq = GK_SPRITE_STOPPED_ANIM_PREPARING_REVERSE;
			break;
			
		case GOAL_KEEPER_JUMPING_UP_GOING_UP:
			ret = GK_SPRITE_JUMPING_UP_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_UP_ANIM;
			break;
			
		case GOAL_KEEPER_JUMPING_LEFT:
			ret = GK_SPRITE_JUMPING_LEFT_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_LEFT_ANIM;
			break;

		case GOAL_KEEPER_JUMPING_LEFT_LOW:
			ret = GK_SPRITE_JUMPING_LEFT_LOW_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_LEFT_LOW_ANIM;
			break;
			
		case GOAL_KEEPER_JUMPING_LEFT_MED:
			ret = GK_SPRITE_JUMPING_LEFT_MED_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_LEFT_MED_ANIM;
			break;
			
		case GOAL_KEEPER_JUMPING_LEFT_HIGH:
			ret = GK_SPRITE_JUMPING_LEFT_HIGH_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_LEFT_HIGH_ANIM;
			break;
			
		case GOAL_KEEPER_JUMPING_RIGHT:
			ret = GK_SPRITE_JUMPING_RIGHT_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_RIGHT_ANIM;
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_LOW:
			ret = GK_SPRITE_JUMPING_RIGHT_LOW_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_RIGHT_LOW_ANIM;
			break;

		case GOAL_KEEPER_JUMPING_RIGHT_MED:
			ret = GK_SPRITE_JUMPING_RIGHT_MED_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_RIGHT_MED_ANIM;
			break;
			
		case GOAL_KEEPER_JUMPING_RIGHT_HIGH:
			ret = GK_SPRITE_JUMPING_RIGHT_HIGH_INDEX;
			*pAnimSeq = GK_SPRITE_JUMPING_RIGHT_HIGH_ANIM;
			break;

		case GOAL_KEEPER_STOPPED:
			ret = GK_SPRITE_STOPPED_INDEX;
			*pAnimSeq = GK_SPRITE_STOPPED_ANIM_STOPPED;
			break;
			
#if TARGET_IPHONE_SIMULATOR
		default:
			throw InvalidArgumentException( "IsoKeeper::getSpriteIndexForState( int16*, GoalKeeperState ) : Invalid state" );
			break;
#endif
	}
	return ret;
}
