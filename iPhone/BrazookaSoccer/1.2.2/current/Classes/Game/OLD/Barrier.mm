#include "Point3f.h"
#include "Barrier.h"

#include "Ball.h"
#include "BarrierPlayer.h"
#include "Ball_Defines.h"
#include "GameUtils.h"

#include "MathFuncs.h"
#include "ObjectGroup.h"
#include "Random.h"

#include <algorithm>

#include "Tests.h"

#if DEBUG
	#include "Macros.h"
#endif

// OBS : A melhor solução seria:
// 1) Criar BARRIER_MAX_N_PLAYERS slots VAZIOS em pPlayersGroup
// 2) Alocar as TEXTURAS dos jogadores e registrá-las através de registerPlayer( GameMode gameMode, Texture2DHandler playerTex )
// 3) Em prepare, alocar numberOfPlayers sprites com as texturas criadas em 2, randomicamente (assim, poderíamos ter + de um jogador com a mesma imagem, o q pode ser desejável)
// 4) Inserir os sprites criados nos slots vazios de pPlayersGroup
// 5) Ao acabar de usar a barreira, bastaria deletar os sprites criados e setar NULL nos slots correspondentes de pPlayersGroup
// Ou seja, barrier seria um tipo de factory de BarrierPlafile://localhost/Projetos/iPhone/FreeKick/Classes/Game/GameScreen.mmyers

/*===============================================================================

CONSTRUTOR
   
===============================================================================*/

Barrier::Barrier( ObjectGroup* pPlayersGroup ) : Updatable(), numberOfPlayers( 0 ), pPlayersGroup( pPlayersGroup ), trainingModePlayers(), rankingModePlayers()
{
	memset( barrierPlayers, NULL, sizeof( BarrierPlayer* ) * BARRIER_MAX_N_PLAYERS );
}

/*===============================================================================

DESTRUTOR
   
===============================================================================*/

Barrier::~Barrier( void )
{
	pPlayersGroup = NULL;
	memset( barrierPlayers, NULL, sizeof( BarrierPlayer* ) * BARRIER_MAX_N_PLAYERS );
}

/*===============================================================================

MÉTODO registerPlayer
   Indica que um jogador pode ser utilizado em um modo de jogo.

===============================================================================*/

void Barrier::registerPlayer( GameMode gameMode, uint16 index )
{
	switch( gameMode )
	{
		case GAME_MODE_TRAINING:
			trainingModePlayers.push_back( index );
			break;
		
		case GAME_MODE_RANKING:
			rankingModePlayers.push_back( index );
			break;
		
//		case GAME_MODE_STORY:
//			break;
	}
}

/*===============================================================================

MÉTODO prepare
   Posiciona a barreira em relação à posição da bola.

===============================================================================*/

void Barrier::prepare( const Point3f& ballPosition )
{
	// Calcula a posição da trave mais próxima
	Point3f postPosition;
	if( ballPosition.x < 0.0f )
	{
		// Esquerda
		postPosition.set( REAL_LEFT_POST_MIDDLE );
	}
	else
	{
		// Direita
		postPosition.set( REAL_RIGHT_POST_MIDDLE );
	}

	// Vetor no eixo y
	Point3f upVector( 0.0f, 1.0f, 0.0f );

	// Vetor ortogonal à linha da bola até o centro do gol
	Point3f postToBallVector = postPosition - ballPosition;
	Point3f ortho = ( upVector % postToBallVector ).getNormalized();
	
	// Calcula a largura total da barreira
	float barrierTotalWidth = 0.0f;
	for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
		barrierTotalWidth += barrierPlayers[i]->getRealWidth();
	
	// Define a posição do jogador-base da barreira (jogador mais à esquerda)
	// *0.1f => Desloca a barreira de forma a proteger um pouco mais a trave mais próxima da bola
	#if DEBUG && IS_CURR_TEST( TEST_BARRIER_SHADOWS )
		Point3f basePosition( 0.0f, 0.0f, 0.0f );
	#else
		Point3f basePosition = ballPosition + ( postToBallVector.getNormalized() * BARRIER_DISTANCE_TO_BALL ) - ( ortho * barrierTotalWidth * 0.1f );
	#endif

	if( ballPosition.x < 0.0f )
	{
		ortho *= -1.0f;
	
		// Posiciona os jogadores
		// OBS : Fazemos o loop na ordem inversa para que os jogadores não mudem de posição
		Point3f playerPos = basePosition;
		for( int8 i = numberOfPlayers - 1 ; i >= 0 ; --i )
		{
			preparePlayer( barrierPlayers[i], basePosition, playerPos, ballPosition, ortho );
			playerPos += ortho * barrierPlayers[i]->getRealWidth();
		}
	}
	else
	{
		// Posiciona os jogadores
		Point3f playerPos = basePosition;
		for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
		{
			preparePlayer( barrierPlayers[i], basePosition, playerPos, ballPosition, ortho * -1.0f );
			playerPos += ortho * barrierPlayers[i]->getRealWidth();
		}
	}
}

/*===============================================================================

MÉTODO reset
   Cria uma nova barreira.

===============================================================================*/

void Barrier::reset( GameMode gameMode, const Point3f& ballPosition /*<DM>*/ , GameInfo &aGameInfo /*</DM*/)
{
	// Calcula quantos jogadores devem entrar na barreira
	#if DEBUG && IS_CURR_TEST( TEST_BARRIER_SHADOWS )
		uint8 newNumberOfPlayers = 1;
	#elif DEBUG && ( IS_CURR_TEST( TEST_BARRIER_FORMATION ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
		uint8 newNumberOfPlayers = 4;
	#elif DEBUG && ( IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) )
		uint8 newNumberOfPlayers = 0;
	#else
		uint8 newNumberOfPlayers = calcNumberOfPlayers( ballPosition );
	#endif
	
	if( newNumberOfPlayers != numberOfPlayers )
	{
		numberOfPlayers = newNumberOfPlayers;

		// Desfaz a barreira atual
		undoCurrentBarrier( gameMode );
		
		for( uint8 i = 0 ; i < BARRIER_MAX_N_PLAYERS ; ++i )
			barrierPlayers[i] = NULL;

		// Monta a barreira
		std::vector< uint16 > playersCopy;
		switch( gameMode )
		{
			case GAME_MODE_TRAINING:
				for( std::vector< uint16 >::iterator it = trainingModePlayers.begin() ; it != trainingModePlayers.end() ; ++it )
					playersCopy.push_back( *it );
				break;

			case GAME_MODE_RANKING:
				for( std::vector< uint16 >::iterator it = rankingModePlayers.begin() ; it != rankingModePlayers.end() ; ++it )
					playersCopy.push_back( *it );
				break;

	//		case GAME_MODE_STORY:
	//			break;
		}
		
		// OBS : Não precisaríamos deste if se pudéssemos repetir personagens !!!!
		// Garante que não teremos problemas por falta de personagens
		if( playersCopy.size() < numberOfPlayers )
			numberOfPlayers = playersCopy.size();

		
	
		

			///<DM>
			aGameInfo.barrierSize=0;		
			///</DM>
		for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
		{
			// Soteia um jogador de acordo com o modo de jogo
			#if DEBUG && IS_CURR_TEST( TEST_BARRIER_SHADOWS ) 
				uint8 rand = 3;
			#else
				uint8 rand = Random::GetInt( 0, playersCopy.size() - 1 );
			#endif

			barrierPlayers[i] = dynamic_cast< BarrierPlayer* >( pPlayersGroup->getObject( playersCopy[ rand ] ) );
			

			///<DM>		
			aGameInfo.barrierPlayersVector[aGameInfo.barrierSize++]=playersCopy[ rand ];
			///</DM>
			
			// Impede de sortearmos este jogador novamente
			playersCopy.erase( playersCopy.begin() + rand );
		}
	}
	
	// Posiciona os jogadores
	prepare( ballPosition );
}

/*===============================================================================

MÉTODO onBallGettingClose
   Este método deve ser chamado quando a bola se aproximar da barreira. Então,
poderemos iniciar as animações dos personagens.

===============================================================================*/

void Barrier::onBallGettingClose( const Ball* pBall )
{
	Point3f ballRealSpeed, ballRealPos = *( pBall->getRealPosition() );
	float realSpeedModule = pBall->getRealSpeed( &ballRealSpeed )->getModule();

	for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
	{
		#if DEBUG && IS_CURR_TEST( TEST_BARRIER_ANIM )
			float bla = ( ( barrierPlayers[i]->getCollisionArea()->distanceTo( &ballRealPos ) / pBall->getRealSpeed( &ballRealSpeed )->getModule() ) );
			LOG( "Tempo restante: %.3f", bla );
		#endif

		if( ( ( barrierPlayers[i]->getCollisionArea()->distanceTo( &ballRealPos ) / realSpeedModule ) ) < 0.5f )
			barrierPlayers[i]->onBallGettingClose();
	}
}

/*===============================================================================

MÉTODO update
   Atualiza o objeto.

===============================================================================*/

bool Barrier::update( float timeElapsed )
{
	if( !isActive() )
		return false;

	bool ret = false;
	for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
		ret |= barrierPlayers[i]->update( timeElapsed );
	return ret;
}

/*===============================================================================

MÉTODO getPlayer
   Retorna um jogador da barreira ou NULL caso a barreira esteja vazia.

===============================================================================*/

BarrierPlayer* Barrier::getPlayer( int8 index )
{
	if( numberOfPlayers <= 0 || ( index >= numberOfPlayers ) )
		return NULL;
	return barrierPlayers[ index ];
}

/*===============================================================================

MÉTODO calcNumberOfPlayers
   Calcula quantos jogadores devem entrar na barreira.

===============================================================================*/

uint8 Barrier::calcNumberOfPlayers( const Point3f& ballPosition )
{
	float distance = ballPosition.getModule();
	if( distance < BARRIER_MIN_DISTANCE )
		distance = BARRIER_MIN_DISTANCE;
	else if( distance > BARRIER_MAX_DISTANCE )
		distance = BARRIER_MAX_DISTANCE;

	return static_cast< uint8 >( NanoMath::lerp( ( distance - BARRIER_MIN_DISTANCE ) / ( BARRIER_MAX_DISTANCE - BARRIER_MIN_DISTANCE ), BARRIER_MAX_N_PLAYERS, BARRIER_MIN_N_PLAYERS ) );
}

/*===============================================================================

MÉTODO preparePlayer
   Faz o jogador da barreira olhar para a bola.

===============================================================================*/

void Barrier::preparePlayer( BarrierPlayer* pPlayer, const Point3f& basePosition, const Point3f& playerPos, const Point3f& ballPosition, const Point3f& ortho )
{
	// Direciona de acordo com o jogador base da barreira
	pPlayer->reset( NULL );
	pPlayer->setOrtho( &ortho );
	pPlayer->setRealPosition( basePosition.x, basePosition.y, basePosition.z );
	pPlayer->lookAt( &ballPosition, true );
	pPlayer->prepare( ballPosition );
	
	// Posiciona o jogador corretamente
	pPlayer->setRealPosition( playerPos.x, playerPos.y, playerPos.z );

	// Deixa visível e ativa o teste de colisão
	pPlayer->setBallCollisionTest( true );
	pPlayer->setVisible( true );
}

/*===============================================================================

MÉTODO undoCurrentBarrier
   Desfaz a barreira atual.

===============================================================================*/

void Barrier::undoCurrentBarrier( GameMode gameMode )
{
//	if( gameMode == GAME_MODE_TRAINING )
//	{
		for( uint8 i = 0 ; i < trainingModePlayers.size(); ++i )
		{
			BarrierPlayer* pBarrierPlayer = dynamic_cast< BarrierPlayer* >( pPlayersGroup->getObject( trainingModePlayers[i] ) );
			pBarrierPlayer->setVisible( false );
			pBarrierPlayer->setActive( false );
			pBarrierPlayer->setBallCollisionTest( false );
		}
//	}
//	else if( gameMode == GAME_MODE_RANKING )
//	{
		for( uint8 i = 0 ; i < rankingModePlayers.size(); ++i )
		{
			BarrierPlayer* pBarrierPlayer = dynamic_cast< BarrierPlayer* >( pPlayersGroup->getObject( rankingModePlayers[i] ) );
			pBarrierPlayer->setVisible( false );
			pBarrierPlayer->setActive( false );
			pBarrierPlayer->setBallCollisionTest( false );
		}
//	}
}


///<DM>
void Barrier::restoreBarrier( int8 *ptrvec, int8 size )
{
	undoCurrentBarrier( GAME_MODE_RANKING );
	
	for( uint8 i = 0 ; i < BARRIER_MAX_N_PLAYERS ; ++i )
		barrierPlayers[i] = NULL;

	numberOfPlayers = size;
	for( int8 c = 0 ; c < size ; ++c )
		barrierPlayers[c] = dynamic_cast< BarrierPlayer* >( pPlayersGroup->getObject( ptrvec[c] ) );
}
///</DM>


// Define se o teste de colisão com a bola deve ser feito
void Barrier::setBallCollisionTest( bool checkCollision ) {
	for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
	{
		barrierPlayers[i]->setBallCollisionTest( checkCollision );
	}	
}



/*===============================================================================

MÉTODO undoCurrentBarrier
   Imprime as áreas de colisão de cada personagem da barreira.

===============================================================================*/

#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )

void Barrier::printCollisionQuads( void ) const
{
	for( uint8 i = 0 ; i < numberOfPlayers ; ++i )
	{	
		LOG( "Jogador %d", i+1 );
		( barrierPlayers[i]->getCollisionArea() )->print();
		LOG( "\n" );
	}
}


#endif