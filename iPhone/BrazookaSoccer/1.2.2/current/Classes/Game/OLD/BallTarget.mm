#include "BallTarget.h"

#include "Config.h"
#include "GameUtils.h"
#include "Iso_Defines.h"
#include "ObjcMacros.h"

#include "FreeKickAppDelegate.h"

#include "Random.h"

// Índices das animações do sprite
#define BALL_TARGET_ANIM_INDEX_STOPPED		0
#define BALL_TARGET_ANIM_INDEX_FADE_IN_OUT	1

// Velocidade da animação de piscar
#define BALL_TARGET_BLINKING_SPEED 0.100f

// Cor da mira quando esta é acertada pela bola
#define BALL_TARGET_COLOR_WHEN_HIT static_cast< uint8 >( 253 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 122 ), static_cast< uint8 >( 255 )

// Número de vezes que a mira pisca quando acertada
#define BALL_TARGET_N_BLINKS 3

// Configurações das animações
#define BALL_TARGET_FADE_ANIM_DUR 0.480f

/*===============================================================================

CONSTRUTOR

================================================================================*/

BallTarget::BallTarget( void )
		#if CONFIG_TEXTURES_16_BIT
			: RenderableImage( "t", RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D )
		#else
			  : RenderableImage( "t" )
		#endif
			, fadingIn( false ), currState( BALL_TARGET_UNHIT ), blinksCounter( 0 ), blinkAux( 0.0f )
{
	TextureFrame frame( 0.0f, 0.0f, 93.0f, 112.0f, 0.0f, 0.0f );
	setImageFrame( frame );
	
	setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	setState( BALL_TARGET_UNHIT );
}

/*===============================================================================

DESTRUTOR

================================================================================*/

BallTarget::~BallTarget( void )
{
	#if DEBUG
		LOG( "Destruindo Ball Target\n" );
	#endif
};

/*===================================================================================

MÉTODO reset
	Posiciona a mira no centro do gol.

===================================================================================*/

void BallTarget::reset( void )
{
	Point3f goalCenter( 0.0f, REAL_FLOOR_TO_BAR * 0.5f, ISO_BALL_TARGET_REAL_Z );
	setRealPosition( &goalCenter );

	setState( BALL_TARGET_UNHIT );
}

/*===================================================================================

MÉTODO reset
	Gera uma nova posição semi-aleatória do alvo, de acordo com a posição da falta.

===================================================================================*/

void BallTarget::reset( const Point3f *pBallPosition )
{
	float leftPercent = ( GOAL_AREA_PERCENT * 0.5f ) - ( pBallPosition->x / MAX_FOUL_X_DISTANCE ) * GOAL_AREA_PERCENT * 0.5f;

	if( leftPercent < 0.0f )
		leftPercent = 0.0f;
	else if( leftPercent > GOAL_AREA_PERCENT )
		leftPercent = GOAL_AREA_PERCENT;

	// Posições válidas estão compreendidas entre as traves menos a largura da bola
	Point3f rnd( 0.0f, 0.0f, ISO_BALL_TARGET_REAL_Z );
    rnd.x = Random::GetFloat( GOAL_AREA_PERCENT );

	if( rnd.x < leftPercent )
	{
		rnd.x = REAL_LEFT_POST_START + REAL_GOAL_WIDTH  * leftPercent * rnd.x;
	}
	else
	{
		float rightPercent = GOAL_AREA_PERCENT - leftPercent;
		rnd.x = REAL_RIGHT_POST_START - REAL_GOAL_WIDTH * rightPercent * rnd.x;
	}

	// Posição varia de 0 até a altura do travessão menos a largura da bola
	rnd.y = Random::GetFloat( REAL_BAR_BOTTOM );

	setRealPosition( &rnd );
	setState( BALL_TARGET_UNHIT );
}

/*===============================================================================

MÉTODO updatePosition
   Atualiza a posição do objeto na tela.

===============================================================================*/

void BallTarget::updatePosition( void )
{
	Point3f pos = GameUtils::isoGlobalToPixels( realPosition );
	setPosition( pos.x - ( getWidth() * 0.5f ), pos.y - ( getHeight() * 0.5f ) );
}

/*===============================================================================

MÉTODO hasHitBullsEye
   Indica se a bola acertou o centro do alvo.

===============================================================================*/

bool BallTarget::hitBullsEye( const Point3f* pBallPosition ) const
{
	Point3f pos( pBallPosition->x, pBallPosition->y, 0.0f );
	pos -= *getRealPosition();
	return ( pos.getModule() - BULLS_EYE_TOLERANCE <= BULLS_EYE_DISTANCE ); 
}

/*===============================================================================

MÉTODO getTouchCollisionArea
	Obtém a área do alvo que serve como área de colisão para toques. Esta área
pode ser maior ou menor do que a área do objeto.
   
===============================================================================*/

void BallTarget::getTouchCollisionArea( Triangle& t0, Triangle& t1 )
{
	Point3f pos = *getPosition();
	Point3f p0( pos.x, pos.y );
	Point3f p1( pos.x, pos.y + getHeight() );
	Point3f p2( pos.x + getWidth(), pos.y );
	Point3f p3( pos.x + getWidth(), pos.y + getHeight() );

	t0.set( &p0, &p1, &p2 );
	t1.set( &p1, &p2, &p3 );
}

/*===============================================================================

MÉTODO setState
	Determina o estado do objeto.
   
===============================================================================*/

void BallTarget::setState( BallTargetState state )
{
	switch( state )
	{
		case BALL_TARGET_UNHIT:
			{
				Color transparentWhite( static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 0 ) );
				setVertexSetColor( transparentWhite );
				fadingIn = true;
			}
			break;

		case BALL_TARGET_HIT:
			{
				Color c( BALL_TARGET_COLOR_WHEN_HIT );
				setVertexSetColor( c );
				
				blinkAux = 0.0f;
				blinksCounter = 0;
			}
			break;
	}
	currState = state;
}

/*===============================================================================

MÉTODO update
	Atualiza o objeto.
   
===============================================================================*/

bool BallTarget::update( float timeElapsed )
{
	switch( currState )
	{
		case BALL_TARGET_UNHIT:
			{
				if( !RenderableImage::update( timeElapsed ) )
					return false;
				
				Color currColor;
				getVertexSetColor( &currColor );

				if( fadingIn )
				{
					currColor.setFloatA( currColor.getFloatA() + ( timeElapsed / BALL_TARGET_FADE_ANIM_DUR ) );

					if( currColor.getFloatA() >= 1.0f )
					{
						currColor.setFloatA( 1.0f );
						fadingIn = false;
					}	
				}
				else
				{
					currColor.setFloatA( currColor.getFloatA() - ( timeElapsed / BALL_TARGET_FADE_ANIM_DUR ) );

					if( currColor.getFloatA() <= 0.0f )
					{
						currColor.setFloatA( 0.0f );
						fadingIn = true;
					}
				}

				setVertexSetColor( currColor );
			}
			return true;

		case BALL_TARGET_HIT:
			{
				if( blinksCounter < BALL_TARGET_N_BLINKS )
				{
					blinkAux += timeElapsed;
					if( blinkAux >= BALL_TARGET_BLINKING_SPEED )
					{
						blinkAux = static_cast< float >( fmod( blinkAux, BALL_TARGET_BLINKING_SPEED ) );
						setVisible( !isVisible() );
						
						if( isVisible() )
						{
							[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed: SOUND_NAME_TARGET_HIT AtIndex: SOUND_INDEX_TARGET_HIT Looping: false];
							++blinksCounter;
						}
					}
				}
			}
			return true;
	}
	return false;
}
