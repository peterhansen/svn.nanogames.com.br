/*
 *  GameUtils.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GAME_UTILS_H
#define GAME_UTILS_H

#include <string>

#include "Point3f.h"

// Valores dos vetores para cada ângulo de inclinação do personagem (usados para posicionar
// corretamente as áreas de colisão)
#define VECTOR_UP_0				 1.00000000f,  0.00000000f, 0.0f
#define VECTOR_UP_5				 0.99619470f,  0.08715574f, 0.0f
#define VECTOR_UP_10			 0.98480775f,  0.17364818f, 0.0f
#define VECTOR_UP_15			 0.96592583f,  0.25881905f, 0.0f
#define VECTOR_UP_17_5			 0.95371695f,  0.30070580f, 0.0f
#define VECTOR_UP_20			 0.93969262f,  0.34202014f, 0.0f
#define VECTOR_UP_25			 0.90630779f,  0.42261826f, 0.0f
#define VECTOR_UP_30			 0.86602500f,  0.50000000f, 0.0f
#define VECTOR_UP_35			 0.81915204f,  0.57357644f, 0.0f
#define VECTOR_UP_37_5			 0.79335334f,  0.60876143f, 0.0f
#define VECTOR_UP_40			 0.76604444f,  0.64278761f, 0.0f
#define VECTOR_UP_45			 0.70710678f,  0.70710678f, 0.0f
#define VECTOR_UP_50			 0.64278761f,  0.76604444f, 0.0f
#define VECTOR_UP_55			 0.57357644f,  0.81915204f, 0.0f
#define VECTOR_UP_60			 0.50000000f,  0.86602500f, 0.0f
#define VECTOR_UP_65			 0.42261826f,  0.90630779f, 0.0f
#define VECTOR_UP_70			 0.34202014f,  0.93969262f, 0.0f
#define VECTOR_UP_75			 0.25881905f,  0.96592583f, 0.0f
#define VECTOR_UP_80			 0.17364818f,  0.98480775f, 0.0f
#define VECTOR_UP_85			 0.08715574f,  0.99619470f, 0.0f
#define VECTOR_UP_90			 0.00000000f,  1.00000000f, 0.0f
#define VECTOR_UP_85_LEFT		-0.08715574f,  0.99619470f, 0.0f
#define VECTOR_UP_80_LEFT		-0.17364818f,  0.98480775f, 0.0f
#define VECTOR_UP_75_LEFT		-0.25881905f,  0.96592583f, 0.0f
#define VECTOR_UP_70_LEFT		-0.34202014f,  0.93969262f, 0.0f
#define VECTOR_UP_65_LEFT		-0.42261826f,  0.90630779f, 0.0f
#define VECTOR_UP_60_LEFT		-0.50000000f,  0.86602500f, 0.0f
#define VECTOR_UP_55_LEFT		-0.57357644f,  0.81915204f, 0.0f
#define VECTOR_UP_50_LEFT		-0.64278761f,  0.76604444f, 0.0f
#define VECTOR_UP_45_LEFT		-0.70710678f,  0.70710678f, 0.0f
#define VECTOR_UP_40_LEFT		-0.76604444f,  0.64278761f, 0.0f
#define VECTOR_UP_35_LEFT		-0.81915204f,  0.57357644f, 0.0f
#define VECTOR_UP_30_LEFT		-0.86602500f,  0.50000000f, 0.0f
#define VECTOR_UP_25_LEFT		-0.90630779f,  0.42261826f, 0.0f
#define VECTOR_UP_20_LEFT		-0.93969262f,  0.34202014f, 0.0f
#define VECTOR_UP_15_LEFT		-0.96592583f,  0.25881905f, 0.0f
#define VECTOR_UP_10_LEFT		-0.98480775f,  0.17364818f, 0.0f
#define VECTOR_UP_5_LEFT		-0.99619470f,  0.08715574f, 0.0f
#define VECTOR_UP_0_LEFT		-1.00000000f,  0.00000000f, 0.0f
#define VECTOR_UP_NEG_5_LEFT	-0.99619470f, -0.08715574f, 0.0f
#define VECTOR_UP_NEG_10_LEFT	-0.98480775f, -0.17364818f, 0.0f
#define VECTOR_UP_NEG_15_LEFT	-0.96592583f, -0.25881905f, 0.0f
#define VECTOR_UP_NEG_17_5_LEFT	-0.95371695f, -0.30070580f, 0.0f
#define VECTOR_UP_NEG_20_LEFT	-0.93969262f, -0.34202014f, 0.0f
#define VECTOR_UP_NEG_22_5_LEFT -0.92387953f, -0.38268343f, 0.0f
#define VECTOR_UP_NEG_25_LEFT	-0.90630779f, -0.42261826f, 0.0f

#define VECTOR_RIGHT_0			 0.00000000f, -1.00000000f, 0.0f
#define VECTOR_RIGHT_5			 0.08715574f, -0.99619470f, 0.0f
#define VECTOR_RIGHT_10			 0.17364818f, -0.98480775f, 0.0f
#define VECTOR_RIGHT_15			 0.25881905f, -0.96592583f, 0.0f
#define VECTOR_RIGHT_20			 0.34202014f, -0.93969262f, 0.0f
#define VECTOR_RIGHT_25			 0.42261826f, -0.90630779f, 0.0f
#define VECTOR_RIGHT_30			 0.50000000f, -0.86602500f, 0.0f
#define VECTOR_RIGHT_35			 0.57357644f, -0.81915204f, 0.0f
#define VECTOR_RIGHT_40			 0.64278761f, -0.76604444f, 0.0f
#define VECTOR_RIGHT_45			 0.70710678f, -0.70710678f, 0.0f
#define VECTOR_RIGHT_50			 0.76604444f, -0.64278761f, 0.0f
#define VECTOR_RIGHT_55			 0.81915204f, -0.57357644f, 0.0f
#define VECTOR_RIGHT_60			 0.86602500f, -0.50000000f, 0.0f
#define VECTOR_RIGHT_65			 0.90630779f, -0.42261826f, 0.0f
#define VECTOR_RIGHT_70			 0.93969262f, -0.34202014f, 0.0f
#define VECTOR_RIGHT_75			 0.96592583f, -0.25881905f, 0.0f
#define VECTOR_RIGHT_80			 0.98480775f, -0.17364818f, 0.0f
#define VECTOR_RIGHT_85			 0.99619470f, -0.08715574f, 0.0f
#define VECTOR_RIGHT_90			 1.00000000f,  0.00000000f, 0.0f
#define VECTOR_RIGHT_85_LEFT	 0.99619470f,  0.08715574f, 0.0f
#define VECTOR_RIGHT_80_LEFT	 0.98480775f,  0.17364818f, 0.0f
#define VECTOR_RIGHT_75_LEFT	 0.96592583f,  0.25881905f, 0.0f
#define VECTOR_RIGHT_70_LEFT	 0.93969262f,  0.34202014f, 0.0f
#define VECTOR_RIGHT_65_LEFT	 0.90630779f,  0.42261826f, 0.0f
#define VECTOR_RIGHT_60_LEFT	 0.86602500f,  0.50000000f, 0.0f
#define VECTOR_RIGHT_55_LEFT	 0.81915204f,  0.57357644f, 0.0f
#define VECTOR_RIGHT_50_LEFT	 0.76604444f,  0.64278761f, 0.0f
#define VECTOR_RIGHT_45_LEFT	 0.70710678f,  0.70710678f, 0.0f
#define VECTOR_RIGHT_40_LEFT	 0.64278761f,  0.76604444f, 0.0f
#define VECTOR_RIGHT_35_LEFT	 0.57357644f,  0.81915204f, 0.0f
#define VECTOR_RIGHT_30_LEFT	 0.50000000f,  0.86602500f, 0.0f
#define VECTOR_RIGHT_25_LEFT	 0.42261826f,  0.90630779f, 0.0f
#define VECTOR_RIGHT_20_LEFT	 0.34202014f,  0.93969262f, 0.0f
#define VECTOR_RIGHT_15_LEFT	 0.25881905f,  0.96592583f, 0.0f
#define VECTOR_RIGHT_10_LEFT	 0.17364818f,  0.98480775f, 0.0f
#define VECTOR_RIGHT_5_LEFT		 0.08715574f,  0.99619470f, 0.0f
#define VECTOR_RIGHT_0_LEFT		 0.00000000f,  1.00000000f, 0.0f

namespace GameUtils
{
	// Converte uma posição do mundo real para uma posição da tela
	Point3f isoGlobalToPixels( float x = 0.0f, float y = 0.0f, float z  = 0.0f );
	
	// Converte uma posição do mundo real para uma posição da tela
	inline Point3f isoGlobalToPixels( const Point3f& realPosition )
	{
		return isoGlobalToPixels( realPosition.x, realPosition.y, realPosition.z );
	};
	
	// Converte uma posição da tela para uma posição do mundo real. Assume que x no mundo real é 0
	Point3f isoPixelsToGlobalX0( float x, float y );
	
	// Converte uma posição da tela para uma posição do mundo real. Assume que x no mundo real é 0
	inline Point3f isoPixelsToGlobalX0( const Point3f& screenPosition )
	{
		return isoPixelsToGlobalX0( screenPosition.x, screenPosition.y );
	};

	// Converte uma posição da tela para uma posição do mundo real. Assume que y no mundo
	// real é 0
	Point3f isoPixelsToGlobalY0( float x = 0.0f, float y = 0.0f );
	
	// Converte uma posição da tela para uma posição do mundo real. Assume que y no mundo
	// real é 0
	inline Point3f isoPixelsToGlobalY0( const Point3f& screenPosition )
	{
		return isoPixelsToGlobalY0( screenPosition.x, screenPosition.y );
	};

	// Converte uma posição da tela para uma posição do mundo real. Assume que z no mundo
	// real é 0
	Point3f isoPixelsToGlobalZ0( float x = 0.0f, float y = 0.0f );
	
	// Converte uma posição da tela para uma posição do mundo real. Assume que z no mundo
	// real é 0
	inline Point3f isoPixelsToGlobalZ0( const Point3f& screenPosition )
	{
		return isoPixelsToGlobalZ0( screenPosition.x, screenPosition.y );
	};

	// Retorna a angulação do device nos eixos x e y, levando em conta a posição de repouso
	// escolhida pelo usuário
	void GetDeviceAngles( float* pAngleX, float* pAngleY, const Point3f* pRestPosition, const Point3f* pAcceleration );
	
	
	// Calcula a pontuação mínima necessária para uma determinada fase através de uma
	// equação quadrática
	int32 GetLevelScoreTarget( int16 level );
};

#endif

