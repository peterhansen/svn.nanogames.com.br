#include "GameUtils.h"

#include "Config.h"
#include "Macros.h"
#include "MathFuncs.h"

#include "Iso_Defines.h"
#include "Ball_Defines.h"

/*==============================================================================================

MÉTODO isoGlobalToPixels
	Converte uma posição do mundo real para uma posição da tela.

==============================================================================================*/

Point3f GameUtils::isoGlobalToPixels( float x, float y, float z )
{
	return Point3f( ISO_XREAL_X_OFFSET * x + ISO_ZREAL_X_OFFSET * z,
				    -y * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR + ISO_XREAL_Y_OFFSET * x + ISO_ZREAL_Y_OFFSET * z );
}

/*==============================================================================================

MÉTODO isoPixelsToGlobalX0
	Converte uma posição da tela para uma posição do mundo real. Assume que x no mundo real é 0.

==============================================================================================*/

Point3f GameUtils::isoPixelsToGlobalX0( float x, float y )
{
	// Equação 1:
	// xPixel = ( ISO_ZREAL_X_OFFSET * zGlobal )
	
	// Equação 2:
	// yPixel = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) + ( ISO_ZREAL_Y_OFFSET * zGlobal )
	
	float zGlobal = x / ISO_ZREAL_X_OFFSET;
	float yGlobal = - (( y - ( ISO_ZREAL_Y_OFFSET * zGlobal ) ) * REAL_FLOOR_TO_BAR ) / ISO_FLOOR_TO_BAR;

	return Point3f( 0.0f, yGlobal, zGlobal );
}

/*==============================================================================================

MÉTODO isoPixelsToGlobalY0
	Converte uma posição da tela para uma posição do mundo real. Assume que y no mundo real é 0.

==============================================================================================*/

Point3f GameUtils::isoPixelsToGlobalY0( float x, float y )
{
	// Equação 1:
	// xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * zGlobal )
	
	// Equação 2:
	// yPixel = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) + ( ISO_XREAL_Y_OFFSET * xGlobal ) + ( ISO_ZREAL_Y_OFFSET * zGlobal )

	// Simplificando para yGlobal sempre = 0:
	// yPixel = ( ISO_XREAL_Y_OFFSET * xGlobal ) + ( ISO_ZREAL_Y_OFFSET * zGlobal )
	
	// Isola zGlobal na Equação 2:
	// zGlobal = ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) / ISO_ZREAL_Y_OFFSET
	
	// Substitui na Equação 1:
	// xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * ( ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) / ISO_ZREAL_Y_OFFSET ) )
	
	// Isola xGlobal
	// 1) xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * (( yPixel / ISO_ZREAL_Y_OFFSET ) - ( ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET ))
	// 2) xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET ) - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET )
	// 3) ( ISO_XREAL_X_OFFSET * xGlobal ) = xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET ) + ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET )
	// 4) ( ISO_XREAL_X_OFFSET * xGlobal ) - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET * xGlobal / ISO_ZREAL_Y_OFFSET ) = xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET )
	// 5) xGlobal * ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET )) = xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET )
	// 6) xGlobal = ( xPixel - ( ISO_ZREAL_X_OFFSET * yPixel / ISO_ZREAL_Y_OFFSET )) / ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET ))
	// 7) xGlobal = ( xPixel - ( ISO_ZREAL_X_OFFSET / ISO_ZREAL_Y_OFFSET * yPixel )) / ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET ))

	// Calcula xGlobal
	float xGlobal = ( x - ( ISO_ZREAL_X_OFFSET / ISO_ZREAL_Y_OFFSET * y )) / ( ISO_XREAL_X_OFFSET - ( ISO_ZREAL_X_OFFSET * ISO_XREAL_Y_OFFSET / ISO_ZREAL_Y_OFFSET ));
	
	// Evita imprecisões de ponto flutuante
	if( NanoMath::feql( xGlobal, 0.0f ) )
		xGlobal = 0.0f;

	// Calcula zGlobal
	float zGlobal = ( y - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) / ISO_ZREAL_Y_OFFSET;
	
	// Evita imprecisões de ponto flutuante
	if( NanoMath::feql( zGlobal, 0.0f ) )
		zGlobal = 0.0f;
		
	return Point3f( xGlobal, 0.0f, zGlobal );
}

/*==============================================================================================

MÉTODO isoPixelsToGlobalZ0
	Converte uma posição da tela para uma posição do mundo real. Assume que z no mundo real é 0.

==============================================================================================*/

Point3f GameUtils::isoPixelsToGlobalZ0( float x, float y )
{
	// Equação 1:
	// xPixel = ( ISO_XREAL_X_OFFSET * xGlobal ) + ( ISO_ZREAL_X_OFFSET * zGlobal )
	
	// Equação 2:
	// yPixel = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) + ( ISO_XREAL_Y_OFFSET * xGlobal ) + ( ISO_ZREAL_Y_OFFSET * zGlobal )

	// Simplificando para zGlobal sempre = 0:
	// xPixel = ( ISO_XREAL_X_OFFSET * xGlobal )
	// yPixel = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR ) + ( ISO_XREAL_Y_OFFSET * xGlobal )
	
	// Isola yGlobal na segunda equação
	// 1) yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR )
	// 2) yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) = ( -yGlobal * ISO_FLOOR_TO_BAR / REAL_FLOOR_TO_BAR )
	// 3) REAL_FLOOR_TO_BAR * ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) = -yGlobal * ISO_FLOOR_TO_BAR
	// 4) ( REAL_FLOOR_TO_BAR * ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) ) / ISO_FLOOR_TO_BAR = -yGlobal
	// 5) yGlobal = -( ( REAL_FLOOR_TO_BAR * ( yPixel - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) ) / ISO_FLOOR_TO_BAR )
	
	float xGlobal = x / ISO_XREAL_X_OFFSET;
	float yGlobal = -( ( REAL_FLOOR_TO_BAR * ( y - ( ISO_XREAL_Y_OFFSET * xGlobal ) ) ) / ISO_FLOOR_TO_BAR );

	return Point3f( xGlobal, yGlobal, 0.0f );
}

/*==============================================================================================

MÉTODO GetDeviceAngles
	Retorna a angulação do device nos eixos x e y, levando em conta a posição de repouso
escolhida pelo usuário.

==============================================================================================*/

void GameUtils::GetDeviceAngles( float* pAngleX, float* pAngleY, const Point3f* pRestPosition, const Point3f* pAcceleration )
{
	float accelModule = pAcceleration->getModule();
	if( NanoMath::feql( accelModule, 0.0f ) )
		return;

	Point3f xAxis( 1.0f, 0.0f, 0.0f ), yAxis( 0.0f, 1.0f, 0.0f ), restPos = *pRestPosition;
	float restAngleToX = 0.0f, restAngleToY = 0.0f;
	float resPosModule = restPos.getModule();
	
	if( NanoMath::feql( resPosModule, 0.0f ) )
	{
		// Se chegarmos aqui, teríamos um problema. Então, por via das dúvidas, considera o
		// eixo padrão como posição de repouso
		restPos.set( 0.0f, 1.0f, 1.0f );
	}
	
	// Obtém a angulação do eixo de repouso em relação aos eixos padrão
	// TODOO : É PORCO calcular restAngleToY e restAngleToZ a cada chamada de
	// onAccelerate, já que, uma vez determinada a posição de repouso, 
	// restAngleToY e restAngleToZ se manterão eternamente iguais
	restAngleToX = restPos.getAngle( &xAxis );
	restAngleToY = restPos.getAngle( &yAxis );
	
	// Gera um intervalo significativo, pois, na prática, o usuário não consegue gerar
	// valores que compreendam todo o intervalo [-180º, 180º]
	float minAngleX = restAngleToX - ACCEL_HALF_MOVEMENT_FREEDOM_X, maxAngleX = restAngleToX + ACCEL_HALF_MOVEMENT_FREEDOM_X;
	float minAngleY = restAngleToY - ACCEL_HALF_MOVEMENT_FREEDOM_Y, maxAngleY = restAngleToY + ACCEL_HALF_MOVEMENT_FREEDOM_Y;

	// Obtém a angulação da aceleração em relação aos eixos padrão
	float accelAngleToX = pAcceleration->getAngle( &xAxis );
	float accelAngleToY = pAcceleration->getAngle( &yAxis );

	// TODOO : Verifica se estamos dando a "volta", pois getAngle() retorna apenas valores
	// no intervalo [0º, 180º] e não no intervalo [-180º, 180º]. O problema ocorre sempre
	// que o sinal de restPos.z fica diferente de pAcceleration->z
	if( (( restPos.z < 0.0f ) && ( pAcceleration->z > 0.0f )) || (( restPos.z > 0.0f ) && ( pAcceleration->z < 0.0f )) )
	{
//		if( /* ??? */ )
			accelAngleToX = maxAngleX;
//		else
//			accelAngleToX = minAngleX;
	}
	
//	#if DEBUG
//		LOG( "\n\nRestP: X = %.3f Y = %.3f Z = %.3f\nAccel: X = %.3f Y = %.3f Z = %.3f\nAngle: X = %.3f Y = %.3f\n\n", restPos.x, restPos.y, restPos.z, pAcceleration->x, pAcceleration->y, pAcceleration->z, accelAngleToX, accelAngleToY );
//	#endif
	
	// Evita imprecisões de ponto flutuante
	accelAngleToX = NanoMath::clamp( accelAngleToX, minAngleX, maxAngleX );
	accelAngleToY = NanoMath::clamp( accelAngleToY, minAngleY, maxAngleY );

	// Obtém a angulação entre a aceleração e o eixo de repouso
	float angleX = -( accelAngleToX - restAngleToX );
	float angleY = accelAngleToY - restAngleToY;
	
//	#if DEBUG
//		LOG( "\n\angleX = %.3f\angleY = %.3f\n\n", angleX, angleY );
//	#endif
	
	// Evita imprecisões de ponto flutuante
	angleX = NanoMath::clamp( angleX, -ACCEL_HALF_MOVEMENT_FREEDOM_X, ACCEL_HALF_MOVEMENT_FREEDOM_X );
	angleY = NanoMath::clamp( angleY, -ACCEL_HALF_MOVEMENT_FREEDOM_Y, ACCEL_HALF_MOVEMENT_FREEDOM_Y );
	
	// Retorna os valores
	if( pAngleX )
		*pAngleX = angleX;

	if( pAngleY )
		*pAngleY = angleY;
	
//	#if DEBUG
//		LOG( "Final Angle: X = %.3f, Y = %.3f", angleX, angleY );
//	#endif
}

/*===============================================================================

MÉTODO GetLevelScoreTarget
	Calcula a pontuação mínima necessária para uma determinada fase através de uma
equação quadrática.

================================================================================*/

int32 GameUtils::GetLevelScoreTarget( int16 level )
{
	if( level < 0 )
		return 0;

	int32 scoreTarget = static_cast< int32 >( round( ( SCORE_FUNC_A * pow( level, SCORE_FUNC_EXP )) + ( level * SCORE_FUNC_B ) + SCORE_FUNC_C ) );
	if( scoreTarget > SCORE_NEEDED_ON_MAX_DIFFICULTY_LEVEL )
		scoreTarget = SCORE_NEEDED_ON_MAX_DIFFICULTY_LEVEL;
	return scoreTarget;
}
