/*
 *  CollisionQuad.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef COLLISION_QUAD_H
#define COLLISION_QUAD_H

#include "Point3f.h"

typedef enum ORIENTATION
{
	ORIENT_TOP_LEFT,
	ORIENT_TOP,
	ORIENT_TOP_RIGHT,
	ORIENT_RIGHT,
	ORIENT_BOTTOM_RIGHT,
	ORIENT_BOTTOM,
	ORIENT_BOTTOM_LEFT,
	ORIENT_LEFT,
	ORIENT_CENTER
} ORIENTATION;

class CollisionQuad
{
	public:
		// Construtores
		CollisionQuad( void );
		CollisionQuad( Point3f pos, Point3f top, Point3f right, ORIENTATION orientation, float width, float height );
		CollisionQuad( Point3f bottomLeft, Point3f topLeft, Point3f bottomRight );
		
		// Inicializa o objeto
		void set( Point3f bottomLeft, Point3f topLeft, Point3f bottomRight );
		void set( Point3f pos, Point3f top, Point3f right, ORIENTATION orientation, float width, float height );

		// Retorna a distância do ponto ao plano do quad
		float distanceTo( const Point3f* pPoint ) const;

		// Indica se o ponto está dentro dos limites de altura/largura do quad (não testa se o
		// ponto pertence ao plano)
		bool isInsideQuad( const Point3f* pPoint, float tolerance ) const;

		// Vetores unitários do plano
		Point3f top, right, normal, position;
	
		// Armazena a posição central do quad, de forma a agilizar cálculos de colisão
		Point3f center;		 
	
		// Coordenada d da equação do plano: p = ax + by + cz + d
		float d;
	
		// Comprimento da diagonal do quad
		float diagonal;

		// Módulos dos vetores (definem a largura e altura do Quad)
		float width, height;
	
		#if DEBUG
			// Imprime as informações do quadrado de colisão
			void print( void ) const;	
		#endif

	private:
		// Ajusta a posição do quad de acordo com a orientação recebida. Internamente,
		// a orientação será sempre TOP_LEFT
		void setPosByOrientation( Point3f pos, ORIENTATION orientation );
};

#endif

