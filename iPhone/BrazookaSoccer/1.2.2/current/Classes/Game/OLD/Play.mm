#include "Play.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Play::Play( const Point3f* pDirection, float height, float power, float fx, float angularSpeed, const Point3f* pAngularSpeedAxis )
: direction( pDirection ? *pDirection : Point3f() ), height( height ), power( power ), fx( fx ), angularSpeed( angularSpeed ), angularSpeedAxis( pAngularSpeedAxis ? *pAngularSpeedAxis : Point3f() )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Play::set( const Point3f* pDirection, float height, float power, float fx, float angularSpeed, const Point3f* pAngularSpeedAxis )
{
	if( pDirection )
		direction = *pDirection;
	else
		direction.set();

	this->height = height;
	this->power = power;
	this->fx = fx;

	this->angularSpeed = angularSpeed;
	
	if( pAngularSpeedAxis )
		angularSpeedAxis = *pAngularSpeedAxis;
	else
		angularSpeedAxis.set();
}
