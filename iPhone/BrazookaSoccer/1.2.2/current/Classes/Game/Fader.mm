#include "Fader.h"

#include "GLHeaders.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"
#include "Quad.h"

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

Fader::Fader( const Color& color, float startAlpha, float finalAlpha, float alphaTransitionTime )
	  : Object(), faderColor( color ), startAlpha( startAlpha ), finalAlpha( finalAlpha ),
		timeCounter( 0.0f ), alphaTransitionTime( alphaTransitionTime )
{
	faderColor.setFloatA( startAlpha );
	setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

Fader::~Fader( void )
{
}

/*===========================================================================================
 
MÉTODO render
	Renderiza o objeto.

============================================================================================*/

bool Fader::render( void )
{
	if( !Object::render() )
		return false;
	
	// Habilita a transparência
	GLboolean blendWasEnabled = glIsEnabled( GL_BLEND );
	if( faderColor.getFloatA() < 1.0f ) 
	{
		glEnable( GL_BLEND );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	}
	else
	{
		glDisable( GL_BLEND );
	}
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glTranslatef( position.x + ( size.x * 0.5f ), position.y + ( size.y * 0.5f ), position.z );
	glScalef( size.x, size.y, size.z );
	
	// Rotaciona o objeto
	Matrix4x4 rot;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &rot ) ) );
	
	glColor4ub( faderColor.getUbR(), faderColor.getUbG(), faderColor.getUbB(), faderColor.getUbA() );
	glShadeModel( GL_FLAT );

	Quad aux;
	aux.disableRenderStates( RENDER_STATE_COLOR_ARRAY | RENDER_STATE_TEXCOORD_ARRAY | RENDER_STATE_NORMAL_ARRAY );
	aux.render();

	glPopMatrix();
	
	if( blendWasEnabled )
		glEnable( GL_BLEND );
	else
		glDisable( GL_BLEND );
	
	return true;
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool Fader::update( float timeElapsed )
{
	if( !Object::update( timeElapsed ) )
		return false;

	timeCounter += timeElapsed;
	float percent = timeCounter / alphaTransitionTime;
	if( percent > 1.0f )
		percent = 1.0f;

	faderColor.setFloatA( NanoMath::lerp( percent, startAlpha, finalAlpha ) );

	return true;
}

/*===========================================================================================
 
MÉTODO reset
	Coloca o objeto em seu estado inicial.

============================================================================================*/

void Fader::reset( void )
{
	timeCounter = 0.0f;
	faderColor.setFloatA( startAlpha );
}
