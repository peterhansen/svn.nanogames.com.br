#include "JapaneseBarrierPlayer.h"

#include "Exceptions.h"
#include "Sprite.h"

#include "GameUtils.h"

// Número de objetos contidos no grupo
#define JAPANESE_N_OBJS 2

// Índices dos objetos contidos no grupo
#define JAPANESE_OBJ_INDEX_SHADOW 0
#define JAPANESE_OBJ_INDEX_PLAYER 1

// Altura e largura deste personagem
#define JAPANESE_WIDTH	0.90f
#define JAPANESE_HEIGHT 1.75f

// Offset do pixel de referência do personagem
#define JAPANESE_FEET_OFFSET_Y 18.0f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

JapaneseBarrierPlayer::JapaneseBarrierPlayer( void ) : BarrierPlayer( JAPANESE_N_OBJS )
{
	{
		// Aloca o personagem
		char aux[] = "jps";
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pShadow = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pShadow = new Sprite( aux, aux );
		#endif
		if( !pShadow || !insertObject( pShadow ) )
		{
			delete pShadow;
			goto Error;
		}
		
		aux[2] = '\0';
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pJapanese = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pJapanese = new Sprite( aux, aux );
		#endif
		if( !pJapanese || !insertObject( pJapanese ) )
		{
			delete pJapanese;
			goto Error;
		}	
		pJapanese->setListener( this );

		// Determina o tamanho do grupo
		setSize( pJapanese->getWidth(), pJapanese->getHeight() );
		
		float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, JAPANESE_HEIGHT, 0.0f ).y ) / pJapanese->getStepFrameHeight() );
		setScale( scaleFactor, scaleFactor );
		
		#if DEBUG
			setName( "japa" );
		#endif

		return;

	}
	
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "JapaneseBarrierPlayer::JapaneseBarrierPlayer( void ): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

JapaneseBarrierPlayer::~JapaneseBarrierPlayer( void )
{
}

/*===========================================================================================
 
MÉTODO getRealWidth
	Retorna a largura real do jogador da barreira (em metros).

============================================================================================*/

float JapaneseBarrierPlayer::getRealWidth( void ) const
{
	return JAPANESE_WIDTH - 0.30f;
}

/*===========================================================================================
 
MÉTODO prepare
	Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente.

============================================================================================*/

void JapaneseBarrierPlayer::prepare( const Point3f& ballRealPos )
{
	Sprite* pJapanese = static_cast< Sprite* >( getObject( JAPANESE_OBJ_INDEX_PLAYER ) );
	switch( direction )
	{
		case DIRECTION_DOWN:
			pJapanese->setAnimSequence( 1, false );
			
			if( pJapanese->isMirrored( MIRROR_HOR ) )
				pJapanese->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_DOWN_LEFT:
		case DIRECTION_LEFT:
			pJapanese->setAnimSequence( 0, false );

			if( pJapanese->isMirrored( MIRROR_HOR ) )
				pJapanese->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_LEFT:
			pJapanese->setAnimSequence( 2, false );
			
			if( pJapanese->isMirrored( MIRROR_HOR ) )
				pJapanese->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_RIGHT:
			pJapanese->setAnimSequence( 2, false );
			
			if( !pJapanese->isMirrored( MIRROR_HOR ) )
				pJapanese->mirror( MIRROR_HOR );

			break;
	
		case DIRECTION_DOWN_RIGHT:
		case DIRECTION_RIGHT:
			pJapanese->setAnimSequence( 0, false );
			
			if( !pJapanese->isMirrored( MIRROR_HOR ) )
				pJapanese->mirror( MIRROR_HOR );

			break;
			
		// Nunca acontecerá esta possibilidade
//		case DIRECTION_UP:
//			break;
	}

	TextureFrame currFrameInfo;
	pJapanese->getStepFrameInfo( &currFrameInfo, pJapanese->getCurrAnimSequence(), pJapanese->getCurrAnimSequenceStep() );
	setAnchor( ( currFrameInfo.offsetX * getScale()->x ) + ( currFrameInfo.width * getScale()->x * 0.5f ),
			   ( currFrameInfo.offsetY * getScale()->y ) + (( currFrameInfo.height - JAPANESE_FEET_OFFSET_Y ) * getScale()->y ), 0.0f );
}

/*===========================================================================================
 
MÉTODO updateFrame
	Atualiza o frame do jogador de acordo com a direção para a qual está virado.

============================================================================================*/

void JapaneseBarrierPlayer::updateFrame( void )
{
	float width = JAPANESE_WIDTH, height = JAPANESE_HEIGHT;
	Point3f top( VECTOR_UP_90 ), right, offset( 0.05f );
	
	getOrtho( &right );

	Sprite* pJapanese = static_cast< Sprite* >( getObject( JAPANESE_OBJ_INDEX_PLAYER ) );
	if( pJapanese->getCurrAnimSequence() == 2 )
		width -= 0.2f;

	// OLD
//	TextureFrame currFrameInfo;
//	pJapanese->getStepFrameInfo( &currFrameInfo, pJapanese->getCurrAnimSequence(), pJapanese->getCurrAnimSequenceStep() );
//
//	Point3f aux = GameUtils::isoGlobalToPixels( realPosition.x + offset.x, realPosition.y + offset.y, realPosition.z + offset.z );
//	aux.x += pJapanese->getPosition()->x + currFrameInfo.offsetX;
//	aux.y += pJapanese->getPosition()->y + currFrameInfo.offsetY;
//	Point3f realPos = GameUtils::isoPixelsToGlobalY0( aux.x, aux.y );
//
//	collisionArea.set( realPos, top, right, ORIENT_TOP_LEFT, width, height );
	
	collisionArea.set( realPosition + offset, top, right, ORIENT_BOTTOM, width, height );
}

/*===============================================================================

MÉTODO updatePosition
	Atualiza a posição da bola na tela (não altera a posição real).
   
=============================================================================== */

void JapaneseBarrierPlayer::updatePosition( void )
{
	// Atualiza a orientação e o retângulo de colisão go goleiro
	updateFrame();
	
	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( p.x, p.y, 0.0f );
}

/*===============================================================================

MÉTODO onAnimStepChanged
	Chamado quando o sprite muda de etapa de animação.

================================================================================*/

void JapaneseBarrierPlayer::onAnimStepChanged( Sprite* pSprite )
{
	Sprite* pJapanese = static_cast< Sprite* >( getObject( JAPANESE_OBJ_INDEX_PLAYER ) );
	Sprite* pShadow = static_cast< Sprite* >( getObject( JAPANESE_OBJ_INDEX_SHADOW ) );
	
	int16 currSeq = pJapanese->getCurrAnimSequence();
	if( ( currSeq == 0 ) && pJapanese->isMirrored( MIRROR_HOR ) )
		pShadow->setAnimSequence( 3, false, false );
	else
		pShadow->setAnimSequence( currSeq, false, false );

	pShadow->setCurrentAnimSequenceStep( pJapanese->getCurrAnimSequenceStep() );
	
	// Garante que o posicionamento dos elementos do grupo estará correto
	updatePosition();
}

/*===============================================================================

MÉTODO render
	Renderiza o objeto.

================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )

bool JapaneseBarrierPlayer::render( void )
{
	if( ObjectGroup::render() )
	{
		// Renderiza o quad de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		Point3f pos = GameUtils::isoGlobalToPixels( collisionArea.position );
		Point3f dx = GameUtils::isoGlobalToPixels( collisionArea.right * collisionArea.width );
		Point3f dy = GameUtils::isoGlobalToPixels( collisionArea.top * collisionArea.height );

		dy = -dy;
		
		Point3f top[ 2 ] = {
								pos,
								pos + dy,
							};
		
		glColor4ub( 255, 0, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f right[ 2 ] = {
								pos,
								pos + dx,
							};
		
		glColor4ub( 0, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f others[ 4 ] = {
								pos + dy,
								pos + dy + dx,
								pos + dy + dx,
								pos + dx
							};

		glColor4ub( 255, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, others );
		glDrawArrays( GL_LINES, 0, 4 );

		Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
		if( collisionArea.normal.z > 0.0f )
			glColor4ub( 179, 179, 179, 255 );
		else
			glColor4ub( 255, 183, 15, 255 );

		glVertexPointer( 3, GL_FLOAT, 0, &nomal );
		glDrawArrays( GL_POINTS, 0, 1 );

//		// Renderiza a moldura do sprite
//		Sprite* pJapanese = static_cast< Sprite* >( getObject( JAPANESE_OBJ_INDEX_PLAYER ) );
//		TextureFrame currFrameInfo;
//		pJapanese->getStepFrameInfo( &currFrameInfo, pJapanese->getCurrAnimSequence(), pJapanese->getCurrAnimSequenceStep() );
//		
//		float spritePosX = getPosition()->x + pJapanese->getPosition()->x + currFrameInfo.offsetX + 0.375f;
//		float spritePosY = getPosition()->y + pJapanese->getPosition()->y + currFrameInfo.offsetY + 0.375f;
//		
//		float spriteWidth = currFrameInfo.width * getScale()->x;
//		float spriteHeight = currFrameInfo.height * getScale()->y;
//		
//		Point3f border[ 4 ] = {
//								Point3f( spritePosX, spritePosY ),
//								Point3f( spritePosX, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY )
//							  };
//		
//		glColor4ub( 0, 255, 255, 255 );
//
//		glVertexPointer( 3, GL_FLOAT, 0, border );
//		glDrawArrays( GL_LINE_LOOP, 0, 4 );

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		
		glColor4ub( 255, 255, 255, 255 );
		
		return true;
	}
	return false;
}

#endif
