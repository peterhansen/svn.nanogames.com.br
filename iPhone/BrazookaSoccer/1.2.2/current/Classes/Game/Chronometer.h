/*
 *  Chronometer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CHRONOMETER_H
#define CHRONOMETER_H 1

#include "ObjectGroup.h"

#include "InterfaceControl.h"

// Forward Declarations
class ChronometerListener;
class InterfaceControlListener;

class Chronometer : public ObjectGroup, public InterfaceControl
{
	public:
		// Construtores
		Chronometer( InterfaceControlListener* pControlListener, ChronometerListener* pChronometerListener );
	
		// Destrutor
		virtual ~Chronometer( void );
	
		// Determina em que valor de tempo o cronômetro deve para de contar
		void setTimeInterval( float startAt, float stopAt );
	
		// Retorna em que valor de tempo o cronômetro deve para de contar
		void getTimeInterval( float* pStartAt, float* pStopAt ) const;
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Paralisa / retoma a contagem de tempo
		void pause( bool state );
	
	private:
		// Inicializa o objeto
		void build( void );
	
		// Atualiza o texto exibido pelo cronômetro
		void updateTimeLabel( void );
	
		// Indica se a contagem de tempo está suspensa
		bool paused;
	
		// Controlador de sons
		bool soundFlag;

		// Listener de eventos
		ChronometerListener* pListener;

		// Controladores do tempo decorrido
		float currTime;
		float startTime, stopTime;
};

// Implementação dos métodos inline

// Paralisa / retoma a contagem de tempo
inline void Chronometer::pause( bool state )
{
	paused = state;
}

#endif
