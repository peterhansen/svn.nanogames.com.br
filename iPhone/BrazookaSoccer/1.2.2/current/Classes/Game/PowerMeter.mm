#include "PowerMeter.h"

#include "Exceptions.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"
#include "RenderableImage.h"

#include "InterfaceControlListener.h"

#include "FreeKickAppDelegate.h"

// Macro para auxiliar na inserção de objetos no grupo
#define CHECKED_GROUP_INSERT( pGroup, pObject )				\
		if( !pObject || !pGroup->insertObject( pObject ) )	\
		{													\
			delete pObject;									\
			goto Error;										\
		}

// Define o número de objetos no grupo
#define POWER_METER_N_OBJS 4

// Índices dos objetos no grupo
#define POWER_METER_OBJ_INDEX_BACK	0
#define POWER_METER_OBJ_INDEX_METER	1
#define POWER_METER_OBJ_INDEX_TAG	2
#define POWER_METER_OBJ_INDEX_FRONT	3

// Configurações da imagem de fundo
#define POWER_METER_BACK_IMG_FRAME_WIDTH   51.0f 
#define POWER_METER_BACK_IMG_FRAME_HEIGHT 137.0f

// Configurações da imagem do medidor
#define POWER_METER_METER_IMG_FRAME_WIDTH   27.0f
#define POWER_METER_METER_IMG_FRAME_HEIGHT 114.0f
#define POWER_METER_METER_DIST_FROM_FRONT_IMG_X	 6.0f
#define POWER_METER_METER_DIST_FROM_FRONT_IMG_Y 13.0f

// Configurações da imagem da aba
#define POWER_METER_TAG_IMG_FRAME_WIDTH  72.0f
#define POWER_METER_TAG_IMG_FRAME_HEIGHT 27.0f

// Configurações da imagem da borda
#define POWER_METER_FRONT_IMG_FRAME_WIDTH   39.0f
#define POWER_METER_FRONT_IMG_FRAME_HEIGHT 131.0f

// Configurações das animações
#define POWER_METER_ANIM_MOVE_DUR		0.100f
#define POWER_METER_ANIM_ROTATE_DUR		0.200f

#define POWER_METER_ANIM_ROTATE_TOTAL_ROT 90.0f

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

PowerMeter::PowerMeter( float minValue, float maxValue, float initValue, InterfaceControlListener* pControlListener )
			:	ObjectGroup( POWER_METER_N_OBJS ), InterfaceControl( pControlListener ),
				MeterBar( minValue, maxValue, initValue ), degreesCounter( 0.0f ), animFinalX( 0.0f ), soundFlag( false )
{
	build();
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

PowerMeter::~PowerMeter( void )
{
}

/*===========================================================================================
 
MÉTODO setCurrValuePercent
	Determina o valor atual do medidor (porcentagem).

============================================================================================*/

void PowerMeter::setCurrValuePercent( float percent )
{
	MeterBar::setCurrValuePercent( percent );
	
	float yDiff = NanoMath::lerp( NanoMath::clamp( percent, 0.0f, 1.0f ), POWER_METER_METER_IMG_FRAME_HEIGHT, 0.0f );

	Point3f frontScreenPos = *( getPosition() ) + *( getObject( POWER_METER_OBJ_INDEX_FRONT )->getPosition() );
	Viewport viewport( frontScreenPos.x + POWER_METER_METER_DIST_FROM_FRONT_IMG_X, frontScreenPos.y + POWER_METER_METER_DIST_FROM_FRONT_IMG_Y + yDiff, POWER_METER_METER_IMG_FRAME_WIDTH, POWER_METER_METER_IMG_FRAME_HEIGHT - yDiff );
	getObject( POWER_METER_OBJ_INDEX_METER )->setViewport( viewport );
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/	

void PowerMeter::build( void )
{
	{ // Evita erros de compilação por causa dos gotos

		// Aloca as imagens
		char path[] = { 'c', 't', 'p', ' ', '\0' };
		
		path[3] = 'b';
		
		#if CONFIG_TEXTURES_16_BIT

			RenderableImage* pBack = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pBack );
			
			path[3] = 'm';
			RenderableImage* pMeter = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pMeter );
			
			path[3] = 't';
			RenderableImage* pTag = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pTag );
			
			path[3] = 'f';
			RenderableImage* pFront = new RenderableImage( path, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
			CHECKED_GROUP_INSERT( this, pFront );
				
		#else

			RenderableImage* pBack = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pBack );
			
			path[3] = 'm';
			RenderableImage* pMeter = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pMeter );
			
			path[3] = 't';
			RenderableImage* pTag = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pTag );
			
			path[3] = 'f';
			RenderableImage* pFront = new RenderableImage( path );
			CHECKED_GROUP_INSERT( this, pFront );

		#endif
		
		// Configura as imagens
		TextureFrame frame( 0.0f, 0.0f, POWER_METER_BACK_IMG_FRAME_WIDTH, POWER_METER_BACK_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pBack->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, POWER_METER_METER_IMG_FRAME_WIDTH, POWER_METER_METER_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pMeter->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, POWER_METER_TAG_IMG_FRAME_WIDTH, POWER_METER_TAG_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pTag->setImageFrame( frame );
		
		frame.set( 0.0f, 0.0f, POWER_METER_FRONT_IMG_FRAME_WIDTH, POWER_METER_FRONT_IMG_FRAME_HEIGHT, 0.0f, 0.0f );
		pFront->setImageFrame( frame );
		
		// Posiciona as imagens no grupo
		//pBack->setPosition( 0.0f, 0.0f );
		pFront->setPosition( pBack->getPosition()->x + 18.0f, pBack->getPosition()->y + 2.0f );
		pMeter->setPosition( pFront->getPosition()->x + POWER_METER_METER_DIST_FROM_FRONT_IMG_X, pFront->getPosition()->y + POWER_METER_METER_DIST_FROM_FRONT_IMG_Y );
		pTag->setPosition( pFront->getPosition()->x + 30.0f, pFront->getPosition()->y + 9.0f );
		
		// Determina o tamanho do grupo
		setSize( POWER_METER_BACK_IMG_FRAME_WIDTH + POWER_METER_TAG_IMG_FRAME_WIDTH + ( pTag->getPosition()->x - ( pBack->getPosition()->x + pBack->getWidth() ) ), POWER_METER_BACK_IMG_FRAME_HEIGHT );

	}  // Evita erros de compilação por causa dos gotos

	return;

	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "PowerMeter::build: Unable to create object" );
	#else
		throw ConstructorException( "" );
	#endif
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool PowerMeter::update( float timeElapsed )
{
	if( !ObjectGroup::update( timeElapsed ) )
		return false;

	if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_SHOWING )
	{
		float currX = getPosition()->x;
		
		if( currX < animFinalX )
		{
			float totalMovement = ( animFinalX - ( -getWidth() ) );
			float nextX = currX + ( timeElapsed * totalMovement ) / POWER_METER_ANIM_MOVE_DUR;
			
			if( ( nextX + getWidth() > 0.0f ) && !soundFlag )
			{
				soundFlag = true;
				[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudioNamed:SOUND_NAME_TAG_INCOMING AtIndex:SOUND_INDEX_TAG_0_INCOMING Looping: false];
			}

			if( nextX > animFinalX )
				nextX = animFinalX;
			
			setPosition( nextX, getPosition()->y );
		}
		else
		{
			Object *pTag = getObject( POWER_METER_OBJ_INDEX_TAG );

			float degrees = ( timeElapsed * POWER_METER_ANIM_ROTATE_TOTAL_ROT ) / POWER_METER_ANIM_ROTATE_DUR;
			degreesCounter += degrees;
			
			if( degreesCounter >= POWER_METER_ANIM_ROTATE_TOTAL_ROT )
			{
				Matrix4x4 identity;
				pTag->setRotation( &identity );

				setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWN, true );
			}
			else
			{
				pTag->rotate( degrees, 0.0f, 0.0f, -1.0f );
			}
		}
	}
	else if( getInterfaceControlState() == INTERFACE_CONTROL_STATE_HIDING )
	{
		float startX = -getWidth();
		float currX = getPosition()->x;
		
		float totalMovement = ( animFinalX - startX );
		float nextX = currX - ( timeElapsed * totalMovement ) / POWER_METER_ANIM_MOVE_DUR;
		
		if( nextX < startX )
		{
			nextX = startX;
			setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDDEN, true );
		}
		
		setPosition( nextX, getPosition()->y );
	}
	
	return true;
}

/*===========================================================================================
 
MÉTODO setInterfaceControlState
	Determina o estado de exebição do controle.

============================================================================================*/

void PowerMeter::setInterfaceControlState( InterfaceControlState state, bool notifyListener )
{
	if( state != INTERFACE_CONTROL_STATE_HIDDEN )
	{
		setActive( true );
		setVisible( true );
	}
	else
	{
		degreesCounter = 0.0f;

		getObject( POWER_METER_OBJ_INDEX_TAG )->setRotation( -POWER_METER_ANIM_ROTATE_TOTAL_ROT, 0.0f, 0.0f, -1.0f );		
		setPosition( -getWidth(), getPosition()->y );

		setActive( false );
		setVisible( false );
	}
	InterfaceControl::setInterfaceControlState( state, notifyListener );
}

/*===========================================================================================
 
MÉTODO startShowAnimation
	Faz uma animação para exibir o controle.

============================================================================================*/

void PowerMeter::startShowAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_SHOWING ) && ( currControlState != INTERFACE_CONTROL_STATE_SHOWN ) )
	{
		soundFlag = false;
		setInterfaceControlState( INTERFACE_CONTROL_STATE_SHOWING );
	}
}

/*===========================================================================================
 
MÉTODO startHideAnimation
	Faz uma animação para esconder o controle.

============================================================================================*/

void PowerMeter::startHideAnimation( void )
{
	InterfaceControlState currControlState = getInterfaceControlState();
	if( ( currControlState != INTERFACE_CONTROL_STATE_HIDING ) && ( currControlState != INTERFACE_CONTROL_STATE_HIDDEN ) )
		setInterfaceControlState( INTERFACE_CONTROL_STATE_HIDING );
}

/*===========================================================================================
 
MÉTODO configAnim
	Configura os parâmetros das animações.

============================================================================================*/

void PowerMeter::configAnim( float finalX )
{
	animFinalX = finalX;
	
	Object *pTag = getObject( POWER_METER_OBJ_INDEX_TAG );
	
	float x = animFinalX + pTag->getPosition()->x;
	Viewport tagViewport( x, 0.0f, SCREEN_WIDTH - x, SCREEN_HEIGHT );
	pTag->setViewport( tagViewport );
}

/*===========================================================================================
 
MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

============================================================================================*/

#if TARGET_IPHONE_SIMULATOR

bool PowerMeter::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
		case EVENT_TOUCH_MOVED:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases

				// Obtém a informação do toque
				const NSArray* pTouchesArray = ( NSArray* )pParam;
				const UITouch* pTouch = [pTouchesArray objectAtIndex: 0];
				CGPoint temp = [pTouch locationInView : NULL];

				Point3f touchPos( temp.x, temp.y );
				touchPos = Utils::MapPointByOrientation( &touchPos );
				touchPos.z = 0.0f;

				if( Utils::IsPointInsideRect( &touchPos, this ) )
					setCurrValuePercent( NanoMath::lerp( ( touchPos.y - getPosition()->y ) / getHeight(), 1.0f, 0.0f ) );

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			return true;

		case EVENT_TOUCH_ENDED:
		case EVENT_TOUCH_CANCELED:
		default:
			return false;
	}
}

#endif
