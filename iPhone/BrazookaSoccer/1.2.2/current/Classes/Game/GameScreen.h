/*
 *  GameScreen.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GAME_SCREEN_H
#define GAME_SCREEN_H 1

// C++
#include <vector>

// Components
#include "AccelerometerListener.h"
#include "EventListener.h"
#include "Scene.h"
#include "Touch.h"

// Game
#include "AnimatedCameraListener.h"
#include "ChronometerListener.h"
#include "Iso_Defines.h"
#include "GameInfo.h"
#include "InterfaceControlListener.h"
#include "OGLTransition.h"
#include "Play.h"
#include "SetEffectGroup.h"
#include "RenderableImage.h"
#include "Config.h"

#include "Tests.h"

// Foward Declarations
class Barrier;
class BarrierPlayer;
class BallTarget;
class Button;
class Chronometer;
class Crowd;
class CurtainTransition;
class DirectionMeter;
class FeedbackMsg;
class GLButton;
class InfoTab;
class InterfaceControl;
class IsoBall;
class IsoKeeper;
class IsoPlayer;
class OrthoCamera;
class PowerMeter;
class RealPosOwner;
class SliceTransition;

@class LoadingView;

// Tamanho do array de botões do replay
// Tamanho do array das mensagens de feedback
#define GAME_SCREEN_N_FFEDBACK_MSGS 4

#define REPLAY_N_BUTTONS 9
#define INTERFACE_N_COMMAND_BTS 4


class GameScreen :	public Scene, public EventListener, public AccelerometerListener, public OGLTransitionListener, public AnimatedCameraListener,
					public SpriteListener, public SetEffectGroupListener, public InterfaceControlListener, public ChronometerListener
{
	public:
		// Construtor
		GameScreen( GameInfo& gameInfo, LoadingView* hLoadindView, bool continuing );
	
		// Destrutor
		virtual ~GameScreen( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Indica que a aplicação irá resumir sua execução. Quando a aplicação
		// chama este método da cena, significa que a cena voltará ser atualizada
		// através de eventos de update
		virtual void resume( void );
	
		virtual void suspend();
	
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
	
		// Recebe os eventos do acelerômetro
		virtual void onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration );
	
		// Chamado quando o sprite muda de etapa de animação
		virtual void onAnimStepChanged( Sprite* pSprite );
	
		// Chamado quando o sprite termina um sequência de animação
		virtual void onAnimEnded( Sprite* pSprite );
	
		// Evento chamado assim que o jogador determina o efeito a ser aplicado na bola
		virtual void onEffectSet( void );
	
		// Indica se o jogo acabou
		inline bool isGameOver( void ) const { return matchState == MATCH_STATE_UNDEFINED; };
	
		// Inicia a transição para a tela de jogo adequada
		void onGameOver( void );
	
		// Retorna para o estado anterior ao estado MATCH_STATE_PAUSED
		void unpause( void );

	private:
		// Estados da partida
		enum MatchState
		{
			MATCH_STATE_UNDEFINED = -1,
			MATCH_STATE_PAUSED = 0,
			MATCH_STATE_GETTING_READY,
			MATCH_STATE_ADJUSTING_CAM_TO_SHOT,
			MATCH_STATE_SHOWING_KICK_CONTROLS,
			MATCH_STATE_SETTING_KICK,
			MATCH_STATE_HIDING_KICK_CONTROLS,
			MATCH_STATE_PLAYER_RUNNING,
			MATCH_STATE_SHOWING_EFFECT_CONTROLS,
			MATCH_STATE_SETTING_EFFECT,
			MATCH_STATE_HIDING_EFFECT_KICK_BT,
			MATCH_STATE_HIDING_EFFECT_CONTROLS,
			MATCH_STATE_PLAYER_KICKING,
			MATCH_STATE_ANIMATING_PLAY,
			MATCH_STATE_SHOWING_RESULT,
			MATCH_STATE_RESULT_SHOWN,
			MATCH_STATE_NEXT_LEVEL,
			MATCH_STATE_NEXT_TRY,
			MATCH_STATE_GAME_OVER,
			MATCH_STATE_NEXT_TRAINING,
			MATCH_STATE_TRANSITION,
			MATCH_STATE_SHOWING_INFO,
			MATCH_STATE_INFO_SHOWN,
			MATCH_STATE_HIDING_INFO,
			MATCH_STATE_BEFORE_FOCUS_ON_BALL,
			MATCH_STATE_FOCUSING_ON_BALL,
			MATCH_STATE_AFTER_FOCUS_ON_BALL,
			MATCH_STATE_FEEDBACK_GOAL,
			MATCH_STATE_FEEDBACK_GAME_OVER,
			MATCH_STATE_FEEDBACK_TOO_BAD,
			MATCH_STATE_FEEDBACK_NEXT_CHALLENGE,
			//<DM>			
			MATCH_STATE_CONTINUING,			
			MATCH_STATE_SHOWING_REPLAY,
			MATCH_STATE_REPLAY_SHOWN,
			MATCH_STATE_HIDING_REPLAY,			 
			MATCH_STATE_PLAYER_RUNNING_REPLAY,			
			MATCH_STATE_PLAYER_KICKING_REPLAY,			
			MATCH_STATE_ANIMATING_REPLAY,			
			//</DM>
			MATCH_STATE_HIDING_FEEDBACK
			
			// TODOO : Descomentar no update
//			MATCH_STATE_SHOWING_REPLAY

#if DEBUG && ( IS_CURR_TEST( TEST_GOAL_KEEPER_JUMPS ) || IS_CURR_TEST( TEST_COLLISION_AREAS ) )
			, MATCH_STATE_TEST_0,
			MATCH_STATE_TEST_1
#endif
		};
	
		// Libera os recursos alocados pelo objeto
		void clean( void );
	
		// Inicializa o objeto
		bool build( LoadingView* hLoadindView, bool continuing = false );
	
		// Esconde todos os controles
		void hideInterfaceControls( void );
	
		// Renderiza os controles da interface
		bool renderInterfaceControls( void );
	
		// Atualiza os controles da interface
		bool updateInterfaceControls( float timeElapsed );
	
		// Método chamado para indicar que a animação do controle foi finalizada
		virtual void onInterfaceControlAnimCompleted( InterfaceControl* pControl );
	
		// Método chamado quando o cronômetro termina sua contagem
		virtual void onStopTimeReached( void );

		// TODOO : Descomentar no update
		// Cria os botões utilizados para controlar a tela de replay
		bool createReplayButtons( void );
	
		// Cria a barreira de jogadores
		bool createBarrier( LoadingView* hLoadindView );
	
		///<DM>
		///metodo de acesso a barreira (usado para salvar o jogo)
		Barrier *getBarrier();
		///</DM>
	
		// Cria os controles que devem ser renderizados sobre os elementos do jogo e não devem ser
		// transformados pelo zoom da câmera
		bool createControls( LoadingView* hLoadindView );
	
		// Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja
		// sendo acompanhado
		int8 isTrackingTouch( const UITouch* hTouch );
		
		// Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
		// fazê-lo
		int8 startTrackingTouch( const UITouch* hTouch );
	
		// Cancela o rastreamento do toque recebido como parâmetro
		void stopTrackingTouch( int8 touchIndex );
	
		// Move a câmera para a posição 'position' real em 'time' segundos
		void moveCameraTo( const Point3f* pRealPosition, float time );
		void moveCameraTo( const Point3f* pRealPosition, float finalZoom, float time );
	
		// Verifica se o objeto arrastado está perto dos cantos da tela. Se estiver, movimenta a
		// câmera na direção em que o objeto está sendo arrastado
		void moveCameraIfDraggingNearBounds( float screenX, float screenY, float width, float height );
	
		// Atualiza a posição e o zoom da câmera para englobar o jogador e o gol
		void adjustCamera( float time );
	
		// Atualiza a posição e o zoom da câmera para englobar os pontos p1 e p2
		void adjustCamera( const Point3f& _p1, const Point3f& _p2, float time );

	
		float calcPlaySceneDestPosAndZoom( const Point3f& _p1, const Point3f& _p2, Point3f& _dest ) const;
	
		void calcPlayScenePivots( Point3f& p1, Point3f& p2 );
	
		// Obtém as menores coordenadas x e y contidas no array de pontos keyCoords
		Point3f* getMinCoords( Point3f* pMin, const Point3f* keyCoords, uint8 nCoords ) const;
	
		// Obtém as maiores coordenadas x e y contidas no array de pontos keyCoords
		Point3f* getMaxCoords( Point3f* pMax, const Point3f* keyCoords, uint8 nCoords ) const;

		// Movimenta a câmera respeitando as restrições de posição
		void onMoveCamera( float dx, float dy );

		// Método chamado assim que o personagem chuta a bola
		void onKick( void );
	
		// Modifica o estado da partida
		void setState( MatchState newState );
	
		// Suspende o processamento dos elementos do jogo e exibe a tela de pause.
		void pause( void );
	
		// Atualiza a partida quando ela está no estado MATCH_STATE_ANIMATING_PLAY
		void updateAnimatingPlay( float timeElapsed );
	
		// Atualiza todos os objetos animados da cena
		TryResult updateMovingObjects( float timeElapsed );
	
		// Verifica se devemos tocar a animação das bandeiras de escanteio
		bool updateFlags( float timeElapsed );
	
		// Atualiza a ordem de desenho dos objetos da cena
		void updateZOrder( void );

		// Trata eventos de início de toque
		void onNewTouch( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de fim de toque
		void onTouchEnded( int8 touchIndex, const Point3f* pTouchPos );
	
		#if DEBUG && ( IS_CURR_TEST( TEST_BALL_FX_FACTOR ) || IS_CURR_TEST( TEST_BALL_TARGET_HIT ) )
			// Trata eventos de clique duplo
			void onDoubleClick( int8 touchIndex );
		#endif
	
		// Trata eventos de movimentação de toques
		void onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos );
	
		// Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
		// jogo
		void getPickingRay( Ray& ray, const Point3f& p ) const;

		// Checa colisão do toque com a mira.
		bool checkTouchAimCollision( int8 touchIndex );
	
		// Checa colisão do toque com a bola
		bool checkTouchBallCollision( int8 touchIndex );
	
		// Método chamado quando a transição termina
		virtual void onOGLTransitionEnd( void );
	
		// Executa a ação de replay correspondente ao botão pressionado
		void onReplayCommand( uint8 btIndex );
	
		// Carrega uma nova fase
		void onLoadNewLevel( void );
	
		//<DM>
		/// Configura toda a fase
		void onLoadLevel(const Point3f *pBallPos=NULL );		
		//</DM>
	
		// Reseta a fase atual
		void resetLevel( bool replay = false );

		// Reseta o mostrador de informações do jogo
		void resetInfoTab( void );
	
		///<DM>
		void resetTries();
		///</DM>

		// Reseta o treino atual
		void resetTraining( void );

		// Obtém qual a fase em termos de dificuldade (o jogador pode atingir aa última
		// fase em termos de dificuldade mas continuar jogando)
		int16 getDifficultyLevel( int16 level ) const;

		// Atualiza a pontuação do jogador e o resultado da jogada
		void onTryEnded( TryResult currTryResult );
	
		// Verifica se este conseguiu passar de fase. Altera o estado da partida de acordo
		// com o resultado obtido
		void onResultShown( void );
	
		// Calcula a pontuação obtida com o último chute
		uint32 getKickScore( void ) const;
	
		// Retorna a porcentagem de dificuldade (em relação à dificuldade máxima) do nível atual
		float getDifficultyPercent( void ) const;
	
		// Evento chamado assim que o jogador define a direção e a força do chute
		void onDirAndPowerSet( void );
	
		// Obtém uma posição aleatória válida para a bola
		Point3f* getRandomBallPos( Point3f* pOut ) const;
	
		// Reposiciona os elementos da cena de acordo com a posição da bola
		void onBallPosChanged( const Point3f& ballNewPos );
	
		// Garante que a nova posição do alvo da bola é válida
		void onBallTargetPosChanged( const Point3f& ballTargetNewPos );

		// Indica que a animação da câmera terminou
		virtual void onCameraAnimEnded( void );
	
		// Conserta a posição da bola no modo treino
		void fixBallPosIfInsideArea( Point3f& pos, float minX, float maxX, float minZ, float maxZ ) const;
	
		// Checa colisão do toque com os botões da interface
		bool checkButtonsCollision( const Point3f* pTouchPos, uint8 btMask, bool manageBtsColors = true );
	
		// Checa colisão da bola com a torcida
		bool checkBallCrowdCollision( void );
	
		// Verifica se devemos tocar a animação da bandeira
		void updateFlag( float timeElapsed );
	
		// Faz o aparelho vibrar caso ainda não tenhamos utilizado a vibração nesta jogada
		void vibrate( void );
	
		// Indica se o goleiro pode defender a bola
		bool keeperMayDefend( void ) const;
	
		void cancelTrackedTouches();		
		

		// Estado da partida
		MatchState matchState, matchLastState;
	
		// Variáveis utilizadas para o controle dos gestos realizados no touchscreen
		uint8 nActiveTouches;
		float initDistBetweenTouches;
		Touch trackedTouches[ MAX_TOUCHES ];
	
		// Controlam a duração de estados de transição
		float stateTimeCounter, stateDuration;
	
		// Controlam o tempo de pausa entre as animações das bandeirinhas
		float leftFlagTimeCounter, rightFlagTimeCounter;
	
		// Índices do toques que estão fazendo o drag'n'drop da bola, do alvo e do campo nos estados MATCH_STATE_SETTING_TRAINING e MATCH_STATE_GETTING_READY
		int8 aimDraggingTouch, ballDraggingTouch, fieldDraggingTouch;
	
		// Índice do toque que está pressionando algum botão
		int8 buttonPressingTouch;
	
		// Auxiliares da câmera
		bool movingCamera;
		Point3f cameraMovement;
	
		// Posição da última falta cobrada
		Point3f lastBallInitialPosition;
	
		// Posição de destino da bola relativa à jogada atual
		Point3f ballDestPos;

		// Bola
		IsoBall* pBall;
	
		// Goleiro
		IsoKeeper* pKeeper;
	
		// Jogador
		IsoPlayer* pPlayer;
	
		// Torcida
		Crowd* pCrowd;
	
	///<DM>
		// Jogadores da barreira
	//	BarrierPlayer* barrierPlayers[ BARRIER_N_DIFFERENT_CHARACTERS ];
	///</DM>
		// Gerenciador dos jogadores da barreira
		Barrier *pBarrier;
	
		// Alvo que a bola deverá acertar para que o jogador passe de fase
		BallTarget* pBallTarget;

		// Responsáveis pela renderização das transições de fases
		OGLTransition *pCurrTransition;
		SliceTransition *pSliceTransition;
		CurtainTransition *pCurtainTransition;
	
		// Câmera utilizada para renderizar os controles da cena
		OrthoCamera* pControlsCamera;
	
		// Controles renderizados sobre a cena
		InfoTab* pInfoTab;

		PowerMeter* pPowerMeter;
		DirectionMeter* pDirectionMeter;
	
		SetEffectGroup* pSetEffectControls;

		Button* pBtFocusOnBall;
		Button* pBtKick;
		Button* pBtPause;
		Button* pBtInfo;

		FeedbackMsg* feedbackMsgs[ GAME_SCREEN_N_FFEDBACK_MSGS ];
	
		Chronometer* pChronometer;

		Button* replayBts[ REPLAY_N_BUTTONS ];
	
		// Indica se estamos exibindo o replay
		bool replay;

		// Jogada atual
		Play currPlay;

		// OLD
//		// Indica se estamos determinando a posição de repouso do device dinamicamente
//		int8 settingRestPos; 
	
		// Objetos que devem ser ordenados de acordo com a sua ordem Z
		std::vector< RealPosOwner* > realPosSortables;
	
		// Informações persistentes sobre a partida
		GameInfo& gameInfo;
	
		// Indica se já utilizamos a vibração neste chute
		bool vibrated;
	
		///<DM>
		///O estado do replay
		int32 iReplayState;
		///O tempo restante até o fade in/fade out do R de replay ([0,2] , com [1,2] sendo fade out)
		float iReplaySignFadeTimeout;
		///O ObjectGroup da aba em sí
		ObjectGroup iReplayTabGroup;
		///Imagem do fundo da aba
		RenderableImage *iReplayTabBG;
		///Imagem de fundo do painel de controle de velocidade
		RenderableImage *iReplaySpeedPaneBg;
		///Image de fundo do painel de controle de fluxo do replay
		RenderableImage *iReplayTabButtonsBG;	
		///Imagem da aba de replay para a palavra "tempo"
		RenderableImage *iReplayTempoImage;		
		///Imagens representando cada uma das velocidades disponíveis
		RenderableImage *iReplaySpeedImages[5];	
		///Imagem do sinal de replay piscante.
		RenderableImage *iReplaySign;
		///Velocidade de replay atual
		int32 iReplaySpeedIndex;
		///Velocidades de replay possíveis
		float iReplaySpeedVector[5];
		///Garante que a posição da bola é válida
		void fixBallPos( Point3f& ballNewPos );	
		// Controla visibilidade dos controles de replay
		void SetReplayControlVisibility(bool aVisible);
		void UnloadReplayButtons();
	
		///</DM>
};

#endif
