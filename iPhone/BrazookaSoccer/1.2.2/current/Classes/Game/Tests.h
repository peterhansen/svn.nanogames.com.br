/*
 *  Tests.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/6/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEST_H
#define TEST_H 1

// Testes
#define TEST_NONE							0
#define TEST_COLLISION_AREAS				1
#define TEST_GOAL_KEEPER_JUMPS				2
#define TEST_ZOOM							3
#define TEST_PLAYER_KICK					4
#define TEST_PLAYER_MIRROR					5
#define TEST_BALL_TARGET					6
#define TEST_SET_EFFECT						7
#define TEST_BALL_POSITION					8
#define TEST_BALL_ROTATION					9
#define TEST_BALL_FX_FACTOR					10
#define TEST_PVR							11
#define TEST_PALETTE						12
#define TEST_POINTS_ANIM					13
#define TEST_FEEDBACK_MSG_GOAL				14
#define TEST_FEEDBACK_MSG_GAME_OVER			15
#define TEST_FEEDBACK_MSG_TOO_BAD			16
#define TEST_FEEDBACK_MSG_NEXT_CHALLENGE	17
#define TEST_CAMERA_POS						18
#define TEST_PLAY_ANIMATION					19
#define TEST_SOCCER_BALL_POS				20
#define TEST_BARRIER_ANIM					21
#define TEST_BARRIER_SHADOWS				22
#define TEST_BLINKING_RECORD				23
#define TEST_BARRIER_FORMATION				24
#define TEST_BALL_SHADOW					25
#define TEST_BALL_INSIDE_GOAL				26
#define TEST_BALL_TARGET_HIT				27
#define TEST_BUTTONS						28
#if DEBUG

#define CURR_TEST	TEST_BUTTONS
#define IS_CURR_TEST( x ) ( CURR_TEST == ( x ) )

#else

#define IS_CURR_TEST( x ) 0

#endif

#endif
