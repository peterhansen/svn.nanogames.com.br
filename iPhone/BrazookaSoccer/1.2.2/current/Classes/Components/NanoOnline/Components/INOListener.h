/*
 *  INOListener.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 8/28/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef INANO_ONLINE_LISTENER_H
#define INANO_ONLINE_LISTENER_H 1

// Components
#include "NanoTypes.h"

// NanoOnline
#include "NOErrors.h"
#include "NOString.h"

class INOListener
{
	public:
		// Destrutor
		virtual ~INOListener( void ){};
	
		// Indica que uma requisição foi enviada para o NanoOnline
		virtual void onNORequestSent( void ) = 0;
	
		// Indica que a requisição foi cancelada pelo usuário
		virtual void onNORequestCancelled( void ) = 0;
	
		// Indica que uma requisição foi respondida e terminada com sucesso
		virtual void onNOSuccessfulResponse( void ) = 0;
	
		// Sinaliza erros ocorridos nas operações do NanoOnline
		virtual void onNOError( NOErrors errorCode, const NOString& errorStr ) = 0;
	
		// Indica o progresso da requisição atual
		virtual void onNOProgressChanged( int32 currBytes, int32 totalBytes ) = 0;
};

#endif
