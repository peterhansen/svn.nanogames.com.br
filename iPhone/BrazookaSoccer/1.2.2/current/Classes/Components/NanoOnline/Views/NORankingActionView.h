/*
 *  NORankingActionView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 10/1/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_RANKING_ACTION_VIEW_H
#define NO_RANKING_ACTION_VIEW_H

// Componentes
#include "NOBaseView.h"

@interface NORankingActionView : NOBaseView
{
	@private
		// Elementos de interface contidos na view	
		IBOutlet UIButton *hBtLocal;
		IBOutlet UIButton *hBtGlobal;
	
		// Para conseguirmos traduzir os textos sem termos que criar
		// outros xibs
		IBOutlet UILabel *hLbLocalRecords;
		IBOutlet UILabel *hLbOnlineRanking;
}

// Método chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

@end

#endif
