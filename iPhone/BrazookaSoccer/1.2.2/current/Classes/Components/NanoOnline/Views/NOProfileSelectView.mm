#include "NOProfileSelectView.h"

// Components
#include "FileSystem.h"
#include "ObjcMacros.h"
//#include "UICoverFlowViewController.h"

// NanoOnline
#include "NOTextsIndexes.h"
#include "NOViewsIndexes.h"

// Macros auxiliares
#define ON_ERROR( str ) [[NOControllerView sharedInstance] showError: str]

// Estados da view
#define VIEW_STATE_NO_ERROR				0
#define VIEW_STATE_CORRUPTED			1
#define VIEW_STATE_NO_PROFILES			2
#define VIEW_STATE_DELETING_PROFILE		3
#define VIEW_STATE_COULDNT_CREATE_LIST	4
#define VIEW_STATE_LOGGING_OUT			5

// Espaço entre cada avatar da lista de perfis
#define DIST_BETWEEN_PROFILES_AVATARS 5.0f

// Espaço entre a imagem e o nome do avatar
#define DIST_BETWEEN_AVATAR_IMG_N_NAME 3.0f

// Espaço entre o avatar e o indicador de perfil online
#define SPACE_BETWEEN_ONLINE_MARK_N_AVATAR 3.0f

// Auxiliares para obtermos as views mais facilmente
#define AVATAR_IMG_BASE_TAG	100
#define AVATAR_NAME_BASE_TAG 200

// Identificadores das animações
#define NO_PROF_SELECT_VIEW_BT_OUT				@"PSVBO"
#define NO_PROF_SELECT_VIEW_HIGHLIGHT			@"PSVHL"
#define NO_PROF_SELECT_VIEW_AVATARS_POS_RESET	@"PSVAR%d"

// O dos avatares femininos não está encaixando perfeitamente...
#warning: A imagem deveria encaixar perfeitamente, mas no caso do highlight rosa, isso não acontece...
#define PINK_HIGHLIGHT_ADJUSTMENT 1

// Extensão da classe para declarar métodos privados
@interface NOProfileSelectView ( Private )

// Inicializa a view
-( bool )buildNOProfileSelectView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOProfileSelectView;

//// Inicializa a flow view dos perfis
//-( bool )buildFlowView;

// Cria as imagens necessárias para representarmos os perfis e acrescenta-as à ScrollView
-( bool )buildProfilesListView;

// Remove um perfil da lista de perfis selecionáveis
-( void )removeProfile:( int32 )index;

// Reposiciona os avatares na lista de perfis
-( void )resetAvatarsPositions:( bool )animated;

// Seleciona um perfil da lista de perfis através de seu índice no vetor de perfis locais
-( void )selectProfileImg:( int32 )index animatingHighlightMovement:( bool )animHighlight;

// Seleciona um perfil da lista de perfis através do ID único do perfil
-( void )selectProfileImgWithProfileId:( NOProfileId )profileId;

// Desabilita os elementos com os quais o usuário pode interagir
-( void )disableInterface:( bool )animated;

// Verifica se o perfil selecionado é o perfil ativo (logado) no NanoOnline
-( bool )isSelectedProfileTheActiveProfile;

// Verifica se o perfil selecionado é o perfil temporário (que está sendo manipulado) no NanoOnline
-( bool )isSelectedProfileTheTempProfile;

// Modifica o título de um botão
-( void )changeBt:( UIButton* )hBt Title:( NSString* )hTitle;

@end

// Início da implementação da classe
@implementation NOProfileSelectView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOProfileSelectView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOProfileSelectView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOProfileSelectView
	Inicializa a view.

================================================================================================*/

-( bool )buildNOProfileSelectView
{
	[self setScreenTitle: [NOControllerView GetText: NO_TXT_PROFILE_MANAGER]];

	// Inicializa as variáveis da classe
//	hProfilesListController = nil;
	currSelectedProfile = -1;
	viewState = VIEW_STATE_NO_ERROR;
	
//	{ // Evita erros de compilação por causa dos gotos
		
		// Carrega os usuários gravados localmente
		switch( [NOControllerView loadProfiles: localProfiles] )
		{
			case FS_OK:
				// OLD
//				if( ![self buildFlowView] )
//					goto Error;

				if( localProfiles.empty() )
					viewState = VIEW_STATE_NO_PROFILES;

				break;

			case FS_FILE_DO_NOT_EXIST:
				// No profiles saved. You must create or download a profile first.
				viewState = VIEW_STATE_NO_PROFILES;
				break;

			case FS_APP_WDIR_NOT_FOUND:
			case FS_UNKNOWN_ERROR:
			default:
				// Profiles corrupted. You must create or download a profile.
				viewState = VIEW_STATE_CORRUPTED;
				break;
		}

		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self cleanNOProfileSelectView];
//		return false;
}

/*==============================================================================================

MENSAGEM buildProfilesListView
	Cria as imagens necessárias para representarmos os perfis e acrescenta-as à ScrollView.

================================================================================================*/

-( bool )buildProfilesListView
{
	UIImage *hMaleImg = [NOControllerView getDefaultAvatarForGender: GENDER_M];
	UIImage *hFemaleImg = [NOControllerView getDefaultAvatarForGender: GENDER_F];
	
	if( ( hMaleImg == nil ) || ( hFemaleImg == nil ) )
		return false;

	NOString nickname;
	
	int32 count = 0;
	std::vector< NOCustomer >::const_iterator postLast = localProfiles.end();
	
	for( std::vector< NOCustomer >::const_iterator it = localProfiles.begin() ; it != postLast ; ++it )
	{
		UIButton *hAvatarImg = [UIButton buttonWithType: UIButtonTypeCustom];
		if( hAvatarImg == nil )
			return false;

		[hProfilesList addSubview: hAvatarImg];
		[hAvatarImg addTarget: self action: @selector( onProfileSelected: ) forControlEvents: UIControlEventTouchUpInside];
		[hAvatarImg setImage: ( it->getGender() == GENDER_M ? hMaleImg : hFemaleImg ) forState: UIControlStateNormal];
		[hAvatarImg setTag: AVATAR_IMG_BASE_TAG + count];
		
		UILabel *hAvatarName = [[UILabel alloc]init];
		if( hAvatarName == nil )
			return false;
		
		[hProfilesList addSubview: hAvatarName];

		[hAvatarName setTextAlignment: UITextAlignmentCenter];
		[hAvatarName setTextColor: [UIColor blackColor]];
		[hAvatarName setBackgroundColor: [UIColor clearColor]];
		[hAvatarName setAdjustsFontSizeToFitWidth: YES];
		[hAvatarName setMinimumFontSize: 8.0f];

		[hAvatarName setText: [NOControllerView ConvertSTDStringToNSString: it->getNickname( nickname )]];
		[hAvatarName setTag: AVATAR_NAME_BASE_TAG + count++];
	}

	[self resetAvatarsPositions: false];

	return true;
}

/*==============================================================================================

MENSAGEM buildFlowView
	Inicializa a flow view dos perfis.

================================================================================================*/

// OLD
//-( bool )buildFlowView
//{
//	// Carrega as imagens default dos avatares
//	UIImage *hImgMale = [NOControllerView getDefaultAvatarForGender: GENDER_M];
//	UIImage *hImgFemale = [NOControllerView getDefaultAvatarForGender: GENDER_F];
//	
//	if( ( hImgMale == nil ) || ( hImgFemale == nil ) )
//		return false;
//
//	NSArray *hAux;
//	std::string nickname;
//	NSArray *hNicknames = [NSArray array];
//	NSArray *hAvatars = [NSArray array];
//	std::size_t nProfiles = localProfiles.size();
//
//	for( std::size_t i = 0 ; i < nProfiles ; ++i )
//	{
//		hAux = [hNicknames arrayByAddingObject: STD_STRING_TO_NSSTRING( localProfiles[i].getNickname( nickname ) ) ];
//		[hNicknames release];
//		hNicknames = hAux;
//
//		// Carrega o avatar gravado localmente
//		// OBS: Por enquanto utilizamos imagens fixas, uma para cada sexo
//		if( localProfiles[i].getGender() == GENDER_M )
//		{
//			hAux = [hAvatars arrayByAddingObject: hImgMale];
//			[hAvatars release];
//		}
//		else
//		{
//			hAux = [hAvatars arrayByAddingObject: hImgFemale];
//			[hAvatars release];
//		}
//		hAvatars = hAux;
//	}
//
//	// Aloca os elementos da view que não serão criados pelo Interface Builder
//	hProfilesListController = [[UICoverFlowViewController alloc] initWithItemsImgs: hAvatars AndTitles: hNicknames];
//	
//	[hAvatars release];
//	[hNicknames release];
//	
//	if( hProfilesListController == nil )
//		return false;
//
//	// Configura os elementos da view que não serão criados pelo Interface Builder
//	//[hProfilesListController setFrame: CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, 100.0f )];
//	
//	[self addSubview: hProfilesListController.view];
//	
//	return true;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

-( void )awakeFromNib
{
	[super awakeFromNib];
	
	if( ![self buildProfilesListView] )
		viewState = VIEW_STATE_COULDNT_CREATE_LIST;
	
	hBtEdit.adjustsImageWhenDisabled = YES;
	
	[self changeBt: hBtEdit Title: [NOControllerView GetText: NO_TXT_EDIT]];
	[self changeBt: hBtLogin Title: [NOControllerView GetText: NO_TXT_LOGIN]];
	[self changeBt: hBtDelete Title: [NOControllerView GetText: NO_TXT_DELETE]];
	
	if( viewState != VIEW_STATE_NO_ERROR )
	{
		[self disableInterface: false];
	}
	else
	{	
		NOControllerView *hNOController = [NOControllerView sharedInstance];

		NOProfileId lastProfileLoggedIn = [hNOController getLastLoggedInProfile];
		std::vector< NOCustomer >::size_type i = 0, nProfiles = localProfiles.size();
		
		if( lastProfileLoggedIn != CUSTOMER_PROFILE_ID_NONE )
		{
			for( ; i < nProfiles ; ++i )
			{
				if( localProfiles[i].getProfileId() == lastProfileLoggedIn )
				{
					// O último a logar pode ainda não estar logado nesta sessão
					NOCustomer active;
					[hNOController getActiveProfile: &active];
					
					if( active.getProfileId() != CUSTOMER_PROFILE_ID_NONE )
					{
						CGRect btProfileFrame = [[self viewWithTag: AVATAR_IMG_BASE_TAG + i] frame];
						CGRect temp = [hOnlineMark frame];
											
						temp.origin.x = btProfileFrame.origin.x + ( ( btProfileFrame.size.width - temp.size.width ) * 0.5f );
						temp.origin.y = btProfileFrame.origin.y - temp.size.height - SPACE_BETWEEN_ONLINE_MARK_N_AVATAR;
						[hOnlineMark setFrame: temp];
						[hOnlineMark setHidden: NO];
					}
					break;
				}
			}
		}

		NOCustomer tempProfile;
		[hNOController getTempProfile: &tempProfile];
		NOProfileId tempProfileID = tempProfile.getProfileId();
		
		// Damos preferência ao perfil que está sendo manipulado no momento, depois ao perfil ativo
		if( ( lastProfileLoggedIn == CUSTOMER_PROFILE_ID_NONE ) || ( i >= nProfiles ) || ( tempProfileID != CUSTOMER_PROFILE_ID_NONE ) )
		{
			// Não teremos problemas se passarmos CUSTOMER_PROFILE_ID_NONE aqui.
			// Se isto acontecer, selecionaremos o perfil do meio da lista de perfis
			[self selectProfileImgWithProfileId: tempProfileID];
		}
		else
		{
			[self selectProfileImg: i animatingHighlightMovement: false];
		}
	}
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
//	[self cleanNOProfileSelectView];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM cleanNOProfileSelectView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOProfileSelectView
//{
//	[hProfilesListController release];
//}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.

===============================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( currSelectedProfile < 0 )
		return;
		
	if( hButton == hBtLogin )
	{
		NOControllerView *hNOController = [NOControllerView sharedInstance];

		// Verifica se é logout
		if( [self isSelectedProfileTheActiveProfile] )
		{
			[hOnlineMark setHidden: YES];

			// Faz logout
			viewState = VIEW_STATE_LOGGING_OUT;
			[hNOController setActiveProfile: NULL];
			[self selectProfileImg: currSelectedProfile animatingHighlightMovement: false];
			[self showPopUpWithTitle: @"" Msg: [NOControllerView GetText: NO_TXT_LOGGED_OUT] CancelBtIndex: 0 AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
		}
		else
		{
			[hNOController setTempProfile: &localProfiles[ currSelectedProfile ]];
			[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_LOGIN];
		}
	}
	else if( hButton == hBtDelete )
	{
		viewState = VIEW_STATE_DELETING_PROFILE;

		NSString *hMsg = [NOControllerView GetText: NO_TXT_DELETES_FROM_DEVICE_ONLY];
		if( [self isSelectedProfileTheActiveProfile] )
			hMsg = [hMsg stringByAppendingFormat: @"\n\n%@", [NOControllerView GetText: NO_TXT_PS_DELETING_LOGGED_PROFILE]];
			
		[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: hMsg CancelBtIndex: 1 AndBts: [NOControllerView GetText: NO_TXT_YES], [NOControllerView GetText: NO_TXT_NO], nil ];
	}
	else if( hButton == hBtEdit )
	{
		NOControllerView *hNOController = [NOControllerView sharedInstance];
		[hNOController setTempProfile: &localProfiles[ currSelectedProfile ]];
		[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_EDIT];
	}
}

/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário pressiona um dos botões do popup.

================================================================================================*/

-( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	switch( viewState )
	{
		case VIEW_STATE_CORRUPTED:
		case VIEW_STATE_NO_PROFILES:
		case VIEW_STATE_COULDNT_CREATE_LIST:
			{
				NOControllerView *hNOController = [NOControllerView sharedInstance];
				[hNOController setHistoryAsShortestWayToView: NO_VIEW_INDEX_PROFILE_ACTION];
				[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_ACTION];
			}
			break;
			
		case VIEW_STATE_DELETING_PROFILE:
			if( buttonIndex == 0 )
			{
				// Remove o perfil do disco
				if( [NOControllerView deleteProfile: localProfiles[ currSelectedProfile ]] != FS_OK )
				{
					NOString error;
					[NOControllerView ConvertNSString: [NSString stringWithFormat: @"%@: %@", [NOControllerView GetText: NO_TXT_UNEXPECTED_ERROR], [NOControllerView GetText: NO_TXT_COULDNT_DELETE_PROFILE]] toSTDString: error];
					ON_ERROR( error );
					return;
				}
				
				// Tudo foi OK, então apaga quaisquer indicações de erros
				NOControllerView *hNOController = [NOControllerView sharedInstance];
				[hNOController hideError];
				
				// Se removeu o perfil ativo, indica que não há um perfil online
				if( [self isSelectedProfileTheActiveProfile] )
				{
					[hOnlineMark setHidden: YES];
					[hNOController setActiveProfile: NULL];
				}
				
				// Se removeu o perfil que estava sendo manipulado, indica que não há um perfil temporário
				if( [self isSelectedProfileTheTempProfile] )
					[hNOController setTempProfile: NULL];

				// Remove o perfil do array local
				[self removeProfile: currSelectedProfile];

				// Se não há mais perfis, fornece o feedback e sai da tela
				if( localProfiles.empty() )
				{
					currSelectedProfile = -1;
					[self disableInterface: true];

					viewState = VIEW_STATE_NO_PROFILES;
					[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_NO_PROFILES_SAVED] CancelBtIndex: 0 AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
					return;
				}
				
				// Senão, seleciona o perfil que tomou o lugar do perfil deletado
				int32 nProfilesLeft = localProfiles.size();
				if( currSelectedProfile < nProfilesLeft )
				{
					[self selectProfileImg: currSelectedProfile animatingHighlightMovement: true];
				}
				// Ou o perfil imediatamente anterior, caso tenhamos deletado o último da lista
				else
				{
					[self selectProfileImg: nProfilesLeft - 1  animatingHighlightMovement: true];
				}
			}
			viewState = VIEW_STATE_NO_ERROR;
			break;
			
		case VIEW_STATE_LOGGING_OUT:
			viewState = VIEW_STATE_NO_ERROR;
			break;
	}
}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes que iniciarmos a transição para esta view.
 
===============================================================================================*/

//-( void ) onBeforeTransition
//{
//}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	[super onBecomeCurrentScreen];

	switch( viewState )
	{
		case VIEW_STATE_NO_ERROR:
			break;
			
		case VIEW_STATE_NO_PROFILES:
			[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_NO_PROFILES_SAVED] CancelBtIndex: 0 AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
			break;
			
		case VIEW_STATE_CORRUPTED:
			[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ALERT] Msg: [NOControllerView GetText: NO_TXT_PROFILES_CORRUPTED] CancelBtIndex: 0 AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
			break;
			
		case VIEW_STATE_COULDNT_CREATE_LIST:
			[self showPopUpWithTitle: [NOControllerView GetText: NO_TXT_ERROR] Msg: [NOControllerView GetText: NO_TXT_COULDNT_LOAD_PROFILES_LIST] CancelBtIndex: 0 AndBts: [NOControllerView GetText: NO_TXT_OK], nil];
			break;
	}
}

/*==============================================================================================

MENSAGEM onProfileSelected:
	Trata a seleção do perfil atual.
 
===============================================================================================*/
		
-( void )onProfileSelected:( UIButton* )hBtProfile
{
	[self selectProfileImg: [hBtProfile tag] - AVATAR_IMG_BASE_TAG animatingHighlightMovement: false];
}

/*==============================================================================================

MENSAGEM removeProfile:
	Remove um perfil da lista de perfis.
 
===============================================================================================*/

-( void )removeProfile:( int32 )index
{
	if( !localProfiles.empty() )
	{
		[[self viewWithTag: AVATAR_IMG_BASE_TAG + index] removeFromSuperview];
		[[self viewWithTag: AVATAR_NAME_BASE_TAG + index] removeFromSuperview];
		
		std::vector< NOCustomer >::size_type lastProfile = localProfiles.size();
		for( std::vector< NOCustomer >::size_type i = index + 1 ; i < lastProfile ; ++i )
		{
			[[self viewWithTag: AVATAR_IMG_BASE_TAG + i] setTag: AVATAR_IMG_BASE_TAG + i - 1];
			[[self viewWithTag: AVATAR_NAME_BASE_TAG + i] setTag: AVATAR_NAME_BASE_TAG + i - 1];
		}
		
		localProfiles.erase( localProfiles.begin() + currSelectedProfile );
		
		[self resetAvatarsPositions: true];
	}
}

/*==============================================================================================

MENSAGEM resetAvatarsPositions:
	Reposiciona os avatares na lista de perfis.
 
===============================================================================================*/

-( void )resetAvatarsPositions:( bool )animated
{
	CGSize avatarSize = [NOControllerView getDefaultAvatarForGender: GENDER_M].size;
	size_t nProfiles = localProfiles.size();

	float initX;
	float avatarsListTotalWidth = ( nProfiles * ( avatarSize.width + DIST_BETWEEN_PROFILES_AVATARS ) ) + DIST_BETWEEN_PROFILES_AVATARS;
	
	if( avatarsListTotalWidth > SCREEN_WIDTH )
		initX = DIST_BETWEEN_PROFILES_AVATARS;
	else
		initX = ( SCREEN_WIDTH - avatarsListTotalWidth ) * 0.5f;
	
	CGRect currProfileFrame;
	currProfileFrame.origin.x = initX;
	currProfileFrame.origin.y = ( [hProfilesList frame].size.height - avatarSize.height ) * 0.5f;
	currProfileFrame.size = avatarSize;	

	if( animated )
	{
		for( size_t i = 0 ; i < nProfiles; ++i )
		{
			UIView *hAvatarImg = [hProfilesList viewWithTag: AVATAR_IMG_BASE_TAG + i];

			UILabel *hAvatarName = ( UILabel* )[hProfilesList viewWithTag: AVATAR_NAME_BASE_TAG + i];
			UIFont *hFont = [hAvatarName font];

			[UIView beginAnimations: [NSString stringWithFormat: NO_PROF_SELECT_VIEW_AVATARS_POS_RESET, i, nil] context: nil];
			[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
			[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
			[hAvatarImg setFrame: currProfileFrame];
			[hAvatarName setFrame: CGRectMake( currProfileFrame.origin.x, currProfileFrame.origin.y + currProfileFrame.size.height + DIST_BETWEEN_AVATAR_IMG_N_NAME, currProfileFrame.size.width, [hFont ascender] - [hFont descender] ) ];
			[UIView commitAnimations];
			
			currProfileFrame.origin.x += currProfileFrame.size.width + DIST_BETWEEN_PROFILES_AVATARS;
		}
	}
	else
	{
		for( size_t i = 0 ; i < nProfiles; ++i )
		{
			UIView *hAvatarImg = [hProfilesList viewWithTag: AVATAR_IMG_BASE_TAG + i];
			[hAvatarImg setFrame: currProfileFrame];
			
			UILabel *hAvatarName = ( UILabel* )[hProfilesList viewWithTag: AVATAR_NAME_BASE_TAG + i];
			UIFont *hFont = [hAvatarName font];
			[hAvatarName setFrame: CGRectMake( currProfileFrame.origin.x, currProfileFrame.origin.y + currProfileFrame.size.height + DIST_BETWEEN_AVATAR_IMG_N_NAME, currProfileFrame.size.width, [hFont ascender] - [hFont descender] ) ];
			
			currProfileFrame.origin.x += currProfileFrame.size.width + DIST_BETWEEN_PROFILES_AVATARS;
		}
	}
	
	if( currProfileFrame.origin.x < SCREEN_WIDTH )
		[hProfilesList setContentSize: CGSizeMake( SCREEN_WIDTH + ( 2.0f * DIST_BETWEEN_PROFILES_AVATARS ), avatarSize.height )];
	else
		[hProfilesList setContentSize: CGSizeMake( currProfileFrame.origin.x - initX, avatarSize.height )];
	
	// TODOO: Centralizar:
	// - No perfil ativo
	// - Caso não haja um perfil ativo, no último perfil que logou
	// - Senão, no perfil central
	if( avatarsListTotalWidth > SCREEN_WIDTH )
		[hProfilesList setContentOffset: CGPointMake( -HALF_SCREEN_WIDTH + ( avatarsListTotalWidth * 0.5f ), 0.0f ) animated: animated ? YES : NO];
}

/*==============================================================================================

MENSAGEM selectProfileImg:
	Seleciona um perfil da lista de perfis através de seu índice no vetor de perfis locais.
 
===============================================================================================*/

-( void )selectProfileImg:( int32 )index animatingHighlightMovement:( bool )animHighlight
{
	if( !localProfiles.empty() )
	{
		UIButton *hBtProfile = ( UIButton* )[self viewWithTag: AVATAR_IMG_BASE_TAG + index];
		
		// Se não movermos os dois, podemos ter animações feias ao deletarmos profiles.
		CGRect btProfileFrame = [hBtProfile frame];

		UIImageView *hHighlight;
		if( localProfiles[ index ].getGender() == GENDER_M )
		{
			hHighlight = hProfilehighlightBlue;
			[hProfilehighlightPink setHidden: YES];

			CGRect temp = btProfileFrame;
			temp.origin.y += PINK_HIGHLIGHT_ADJUSTMENT;

			[hProfilehighlightPink setFrame: temp];
		}
		else
		{
			hHighlight = hProfilehighlightPink;
			[hProfilehighlightBlue setHidden: YES];
			[hProfilehighlightBlue setFrame: btProfileFrame];

			btProfileFrame.origin.y += PINK_HIGHLIGHT_ADJUSTMENT;
		}
		
		[hProfilesList bringSubviewToFront: hHighlight];
		
		if( animHighlight )
		{
			[UIView beginAnimations: NO_PROF_SELECT_VIEW_HIGHLIGHT context: nil];
			[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
			[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
			[hHighlight setFrame: btProfileFrame];
			[UIView commitAnimations];
		}
		else
		{
			[hHighlight setFrame: btProfileFrame];
		}
		
		[hHighlight setHidden: NO];

		currSelectedProfile = index;

		if( [self isSelectedProfileTheActiveProfile] )
		{
			[hBtEdit setAlpha: 1.0f];
			[hBtEdit setEnabled: YES];
			
			[self changeBt: hBtLogin Title: [NOControllerView GetText: NO_TXT_LOGOUT]];
		}
		else
		{
			[hBtEdit setEnabled: NO];
			[hBtEdit setAlpha: 0.5f];
			
			[self changeBt: hBtLogin Title: [NOControllerView GetText: NO_TXT_LOGIN]];
		}
		
		[hProfilesList scrollRectToVisible: btProfileFrame animated: animHighlight ? YES : NO];
	}
}

/*==============================================================================================

MENSAGEM selectProfileImgWithProfileId:
	Seleciona um perfil da lista de perfis através do ID único do perfil.
 
===============================================================================================*/

-( void )selectProfileImgWithProfileId:( NOProfileId )profileId
{
	std::vector< NOCustomer >::size_type nProfiles = localProfiles.size();

	if( profileId != CUSTOMER_PROFILE_ID_NONE )
	{
		for( std::vector< NOCustomer >::size_type i = 0 ; i < nProfiles ; ++i )
		{
			if( localProfiles[i].getProfileId() == profileId )
			{
				[self selectProfileImg: i animatingHighlightMovement: false];
				return;
			}
		}
	}
	
	[self selectProfileImg: nProfiles / 2 animatingHighlightMovement: false];
}

/*==============================================================================================

MENSAGEM isSelectedProfileTheActiveProfile
	Verifica se o perfil selecionado é o perfil ativo (logado) no NanoOnline.
 
===============================================================================================*/

-( bool )isSelectedProfileTheActiveProfile
{
	NOCustomer activeProfileCopy;
	[[NOControllerView sharedInstance] getActiveProfile: &activeProfileCopy];

	std::vector< NOCustomer >::iterator itSelected = localProfiles.begin() + currSelectedProfile;
	return itSelected->getProfileId() == activeProfileCopy.getProfileId();
	// OLD return ( *itSelected ) == activeProfileCopy;
}

/*==============================================================================================

MENSAGEM isSelectedProfileTheTempProfile
	Verifica se o perfil selecionado é o perfil temporário (que está sendo manipulado) no NanoOnline.
 
===============================================================================================*/

-( bool )isSelectedProfileTheTempProfile
{
	NOCustomer tempProfileCopy;
	[[NOControllerView sharedInstance] getTempProfile: &tempProfileCopy];

	std::vector< NOCustomer >::iterator itSelected = localProfiles.begin() + currSelectedProfile;
	return itSelected->getProfileId() == tempProfileCopy.getProfileId();
}

/*==============================================================================================

MENSAGEM disableInterface:
	Desabilita os elementos com os quais o usuário pode interagir.
 
===============================================================================================*/

-( void )disableInterface:( bool )animated
{
	[self setUserInteractionEnabled: NO];

	[hOnlineMark setHidden: YES];
	[hProfilehighlightBlue setHidden: YES];
	[hProfilehighlightPink setHidden: YES];

	if( animated )
	{
		[UIView beginAnimations: NO_PROF_SELECT_VIEW_BT_OUT context: nil];
		[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
		[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
		[hBtEdit setAlpha: 0.0f];
		[hBtLogin setAlpha: 0.0f];
		[hBtDelete setAlpha: 0.0f];
		[UIView commitAnimations];
	}
	else
	{
		[hBtEdit setAlpha: 0.0f];
		[hBtLogin setAlpha: 0.0f];
		[hBtDelete setAlpha: 0.0f];
	}
}

/*==============================================================================================

MENSAGEM changeBt:Title:
	Modifica o título de um botão.

==============================================================================================*/

-( void )changeBt:( UIButton* )hBt Title:( NSString* )hTitle
{
	[hBt setTitle: hTitle forState: UIControlStateNormal];
	[hBt setTitle: hTitle forState: UIControlStateSelected];
	[hBt setTitle: hTitle forState: UIControlStateHighlighted];
	[hBt setTitle: hTitle forState: UIControlStateDisabled];
}

// Fim da implementação da classe
@end
