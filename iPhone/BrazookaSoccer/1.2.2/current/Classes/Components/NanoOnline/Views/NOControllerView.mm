#include "NOControllerView.h"

// Apple
#include <QuartzCore/CAAnimation.h>

// Components
#include "ApplicationErrors.h"
#include "ApplicationManager.h"
#include "DeviceInterface.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"
#include "UICallOutView.h"
#include "UIImgProgressBar.h"
#include "UILimitedTextField.h"
#include "UIWaitView.h"
#include "Utils.h"

// NanoOnline
#include "NOConstants.h"
#include "NOConnection.h"
#include "NOGlobalData.h"
#include "NOHelpView.h"
#include "NOMainMenuView.h"
#include "NOProfileActionView.h"
#include "NOProfileAuthenticateView.h"
#include "NOProfileInfoView.h"
#include "NOProfileSelectView.h"
#include "NORankingActionView.h"
#include "NORankingView.h"
#include "NOTextsIndexes.h"
#include "NOViewsIndexes.h"

// Largura da imagem que corresponde ao canto direito da barra de progresso (ler documentação de UISlider) 
#define NO_SLIDER_LEFT_CAP_WIDTH 9
#define NO_SLIDER_TOP_CAP_HEIGHT 7

// Altura da barra de ferramentas
#define NO_TOOLBAR_BAR_OFFSET_Y 8.0f
#define NO_TOOLBAR_BAR_HEIGHT 35.0f

// Definições da barra de progresso
#define NO_PROGRESS_BAR_X 15.0f
#define NO_PROGRESS_BAR_WIDTH 325.0f

// Definições para o botão de erro
#define NO_BT_ERROR_DIST_FROM_PROG_BAR 5.0f

// Duração padrão das animações de transição de telas
#define NANO_ONLINE_SCREEN_TRANSITION_DURATION 0.75f

// Tempo que a tela de scroll leva para começar a seguir a animação do teclado
// DEVE SER MAIOR IGUAL A 0.050f !!!!
#define FOLLOW_KEYBOARD_DELAY 0.050f

// Define a que distância o teclado deve ficar do campo de texto selecionado
#define KEYBOARD_DIST_FROM_TXT_FIELD 5.0f

// Indica se o botão de "Cancela Conecção" deve sobrepor o botão de "Voltar", ou se eles devem ficar lado a lado
#define NO_CONTROLLER_VIEW_BT_CANCELCONN_HIDES_BT_BACK 1

// Definições das animações dos itens da interface
#define NO_INTERFACE_ITEM_ANIM_DUR 0.300f
#define NO_INTERFACE_ITEM_ANIM_CURVE UIViewAnimationCurveEaseIn

// Identificadores das animações
#define NO_CONTROLLER_VIEW_ANIM_ID_TOP_BAR @"NOI"
#define NO_CONTROLLER_VIEW_ANIM_ID_BACK_BT @"NOBBT"
#define NO_CONTROLLER_VIEW_ANIM_ID_KEYB_OUT @"NOKBO"
#define NO_CONTROLLER_VIEW_ANIM_ID_KEYB_IN @"NOKBI"
#define NO_CONTROLLER_VIEW_ANIM_ID_ERROR_BT_IN @"NOEBTI"
#define NO_CONTROLLER_VIEW_ANIM_ID_ERROR_BT_OUT @"NOEBTO"
#define NO_CONTROLLER_VIEW_ANIM_ID_CALLOUT @"NOCOV"
#define NO_CONTROLLER_VIEW_ANIM_ID_ERRMSG_IN @"NOEI"
#define NO_CONTROLLER_VIEW_ANIM_ID_ERRMSG_OUT @"NOEO"
#define NO_CONTROLLER_VIEW_ANIM_ID_CANCEL_CONN_BT @"NOCCBT"


// Singleton desta classe
static NOControllerView* hSingleton = nil;

/*==============================================================================================

FUNÇÕES AUXILIARES

==============================================================================================*/

static void GetProfilesFileName( std::string& outStr )
{
	outStr = "p.non";
}

static void GetConfigurationFileName( std::string& outStr )
{
	outStr = "c.non";
}

static void GetRankingFileName( std::string& outStr )
{
	outStr = "r.non";
}

/*==============================================================================================

FUNCTORS AUXILIARES

==============================================================================================*/

// Caso você tenha dúvidas sobre o que eu estou fazendo aqui, não se assuste, mas tb não ache que
// é gambiarra. Apenas vá estudar um pouco mais de C++. Dica: Functors
struct RecordBelongsToCustomer : std::binary_function< NORankingEntry, NOCustomer, bool >
{
	bool operator()( const NORankingEntry& entry, const NOCustomer& customer ) const
	{
		return customer.getProfileId() == entry.getProfileId();
	}
};

struct CompareNOCustomerNOProfileId : std::binary_function< NOCustomer, NOProfileId, bool >
{
	bool operator()( const NOCustomer& customer, const NOProfileId& profileId ) const
	{
		return customer.getProfileId() == profileId;
	}
};

/*==============================================================================================

EXTENSÃO DA CLASSE
	Declaração de métodos "privados".

==============================================================================================*/

@interface NOControllerView ( Private )

// Inicializa o objeto
-( bool )buildNOControllerView;

// Libera a memória alocada pelo objeto
-( void )cleanNOControllerView;

// Verifica que elementos da interface devem ficar visíveis
-( void )updateInterfaceControls:( NOBaseView* )hNextView;

// Insere a view recebida em uma scroll view configurada para o NanoOnline
-( UIScrollView* )encapsulateViewInScrollView:( UIView* )hView;

// Atribui hNOController como delegate de todos os campos de texto contidos nesta view
-( void )setViewTxtFieldsDelegate:( UIView* )hView;

// Método chamado para terminar o NanoOnline
-( void )onShutDown;

// Realiza a animação padrão para modificar o estado do botão
-( void )enableBackButton:( bool )b;														// Chama enable:button:withAnimId:
-( void )enableCancelConnectionButton:( bool )b;											// Chama enable:button:withAnimId:
-( void )enable:( bool )b button:( UIButton* )hButton withAnimId:( NSString* )hAnimationID;

// Inicia a animação de scroll do campo que acompanha a movimentação do teclado
-( void )startAnimation:( SEL )hSelector WithUserInfo:( NSDictionary* )hDictionary;

// Pára a animação de scroll do campo que acompanha a movimentação do teclado
-( void )stopAnimation;

// Workaround para teclados do tipo Number Pad. Simula o clique da tecla "Done"
-( void )onNumberPadDoneBt:( UIButton* )hDoneButton;

// Retorna uma UICallOutView que não precisa sofrer um release explícito
-( bool )createErrorCallout;

// Acrescenta uma CalloutView à view controladora
-( void )hideErrorCallout;

// Animações do botão de erro
-( void )showErrorBt;
-( void )hideErrorBt;

// Animações do label de erro
-( void )showErrorLb;
-( void )hideErrorLb;

// Auxiliar no workaround de teclados do tipo Number Pad. Retorna as imagens utilizadas no botão
// "Done" criado por cima do teclado
+( UIImage* )getNumberPadDoneBtImg:( bool )buttonUp;

// Acha a view do teclado
+( UIView* )findKeyboardView;

// Carrega as configurações do NanoOnline
-( void )loadConfigurations;

// Salva as configurações do NanoOnline
-( void )saveConfigurations;

// Retorna o perfil que possui o profileId recebido como parâmetro. Caso ele não exista, o campo
// profileId de customer fica com seu valor padrão
+( void )loadCustomerWithId:( NOProfileId )profileId inRef:( NOCustomer& )customer;

@end

/*==============================================================================================

IMPLEMENTAÇÃO DA CLASSE

==============================================================================================*/

// Início da implementação da classe
@implementation NOControllerView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hViewManager, onEndViewIndex, noFirstView;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
	// GAMBIARRA! Ver comentários em sharedInstance
	if( hSingleton != nil )
	{
		// Opa! Não podemos ter duas instâncias desta classe...
		goto Error;
	}
	
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOControllerView] )
			goto Error;
		
		// GAMBIARRA! Ver comentários em sharedInstance
		hSingleton = self;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	// GAMBIARRA! Ver comentários em sharedInstance
	if( hSingleton != nil )
	{
		// Opa! Não podemos ter duas instâncias desta classe...
		goto Error;
	}
	
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOControllerView] )
			goto Error;
		
		// GAMBIARRA! Ver comentários em sharedInstance
		hSingleton = self;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Indica que este objeto irá receber os eventos do controlador de telas
	[hViewManager setDelegate: self];
	
	CGRect aux = [hBarTop frame];
	[hBarTop setFrame: CGRectMake( aux.origin.x, -aux.size.height, aux.size.width, aux.size.height )];
	
	aux = [hViewTitle frame];
	[hViewTitle setFrame: CGRectMake( aux.origin.x, -aux.size.height, aux.size.width, aux.size.height )];
	
	[hBtBack setAlpha: 0.0f];
	[hBtBack setEnabled: NO];
	
	[hBtCancelConnection setAlpha: 0.0f];
	[hBtCancelConnection setEnabled: NO];
	
	CGRect btErrorCurrFrame = [hBtError frame];
	CGRect progressBarFrame = [hProgressBar frame];
	[hBtError setFrame: CGRectMake( [hProgressBar isHidden] ? NO_BT_ERROR_DIST_FROM_PROG_BAR + 10.0f: progressBarFrame.origin.x + progressBarFrame.size.width + NO_BT_ERROR_DIST_FROM_PROG_BAR,
								    btErrorCurrFrame.origin.y,
								    btErrorCurrFrame.size.width,
								    btErrorCurrFrame.size.height )];
	
	[hBtError setEnabled: NO];
	[hBtError setAlpha: 0.0f];
	
	CGRect lbErrorFrame = [hLbErrorMsg frame];
	lbErrorFrame.origin.y = SCREEN_HEIGHT;
	[hLbErrorMsg setFrame: lbErrorFrame];
	
	#if NO_CONTROLLER_VIEW_BT_CANCELCONN_HIDES_BT_BACK
	
		[hBtCancelConnection setFrame: [hBtBack frame]];
		[hBtCancelConnection setContentVerticalAlignment: [hBtBack contentVerticalAlignment]];
		[hBtCancelConnection setContentHorizontalAlignment: [hBtBack contentHorizontalAlignment]];

	#endif
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self saveConfigurations];

	[self cleanNOControllerView];
	[super dealloc];
	
	// GAMBIARRA! Ver comentários em sharedInstance
	hSingleton = nil;
}

/*==============================================================================================

MENSAGEM buildNOControllerView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNOControllerView
{
	float aux = FOLLOW_KEYBOARD_DELAY;
	if( aux < 0.050f )
	{
		#if DEBUG
			LOG( ">>> NOController::buildNOControllerView() => FOLLOW_KEYBOARD_DELAY deve ser maior igual a 0.050f !!!\n" );
		#endif
		return false;
	}
	
	// Inicializa as variáveis da classe
	hProgressBar = nil;
	onEndViewIndex = -1;
	currViewIndex = -1;
	lastViewIndex = -1;
	noFirstView = NO_VIEW_INDEX_MAIN_MENU;
	
	hViewManager = nil;
	hBtBack = nil;
	hViewTitle = nil;
	hBarBottom = nil;
	hBarTop = nil;
	hNanoOnlineLogo = nil;
	
	offsetBeforeKeyboard = 0.0f;
	keyboardHeight = 0.0f;
	hLastTextField = nil;
	hAnimationTimer = nil;
	
	hCurrScrollView = nil;
	hWaitView = nil;
	btBackWasEnabled = false;
	
	// Inicializa os objetos C++ na mão, já que o ObjC não o está fazendo, apesar
	// de estarmos ligando a flag correspondente	
	tempProfile = NOCustomer();
	
	// Carrega possíveis configurações
	[self loadConfigurations];
	
	// Carrega possíveis recordes ainda não submetidos
	[NOControllerView loadNotSubmittedRecords];
	
	// Watch the keyboard so we can adjust the user interface if necessary
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( keyboardDidShow: ) name: UIKeyboardDidShowNotification object: nil];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( keyboardDidHide: ) name: UIKeyboardDidHideNotification object: nil];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( keyboardWillHide: ) name: UIKeyboardWillHideNotification object: nil];
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( keyboardWillShow: ) name: UIKeyboardWillShowNotification object: nil];

	{ // Evita erros de compilação por causa dos gotos

		hProgressBar = [[UIImgProgressBar alloc] initWithBkgImg: "nopbmn" FillImg: "nopbmxi" LeftCapWidth: NO_SLIDER_LEFT_CAP_WIDTH TopCapHeight: NO_SLIDER_TOP_CAP_HEIGHT ];
		if( !hProgressBar )
			goto Error;

		[self addSubview: hProgressBar];
		[hProgressBar release];
		
		// Configura os elementos da view
		CGRect barFrame = [hProgressBar frame];
		[hProgressBar setFrame: CGRectMake( NO_PROGRESS_BAR_X, SCREEN_HEIGHT - NO_TOOLBAR_BAR_HEIGHT + NO_TOOLBAR_BAR_OFFSET_Y + (( NO_TOOLBAR_BAR_HEIGHT - NO_TOOLBAR_BAR_OFFSET_Y - barFrame.size.height ) * 0.5f), NO_PROGRESS_BAR_WIDTH, barFrame.size.height )];
		[hProgressBar setHidden: YES];

		// Acrescenta uma CalloutView à view controladora
		if( ![self createErrorCallout] )
			goto Error;

		return true;

	} // Evita erros de compilação por causa dos gotos

	Error:
		[self cleanNOControllerView];
		return false;
}

/*==============================================================================================

MENSAGEM cleanNOControllerView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanNOControllerView
{
	[self stopAnimation];
	[[NSNotificationCenter defaultCenter] removeObserver: self];
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
  	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
		- UIViewAnimationTransitionFlipFromLeft
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
===============================================================================================*/

- ( void )performTransitionToView:( int8 )newIndex
{
	[self performTransitionToView: newIndex WithLoadingView: NULL];
}

- ( void )performTransitionToView:( int8 )newIndex WithLoadingView:( const LoadingView* )hLoadindView
{
	// Não interrompe uma transição
	if( [hViewManager isTransitioning] )
		return;
	
	lastViewIndex = currViewIndex;

	NOBaseView *hNextView = nil;
	NSString *hTransition = @"oglFlip", *hDirection = kCATransitionFromTop;
	
	bool keepCurrView = false, nullTransition = false;

	switch( newIndex )
	{
		case NO_VIEW_INDEX_SHUT_DOWN:
			[self onShutDown];
			currViewIndex = -1;
			return;

		case NO_VIEW_INDEX_MAIN_MENU:
			{
				NOMainMenuView* hAux = static_cast< NOMainMenuView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOMainMenuView" synchOrientation: false ] );
				hNextView = hAux;
				
				if( lastViewIndex == -1 )
				{
					hTransition = kCATransitionFade;
					hDirection = nil;
				}
			}
			break;

		case NO_VIEW_INDEX_PROFILE_ACTION:
			{
				NOProfileActionView* hAux = static_cast< NOProfileActionView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOProfileActionView" synchOrientation: false] );
				hNextView = hAux;
			}
			break;
			
		case NO_VIEW_INDEX_PROFILE_SELECT:
			{
				NOProfileSelectView* hAux = static_cast< NOProfileSelectView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOProfileSelectView" synchOrientation: false] );
				hNextView = hAux;
			}
			break;
			
		case NO_VIEW_INDEX_PROFILE_CREATE:
			{
				NOProfileInfoView* hAux = static_cast< NOProfileInfoView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOProfileInfoView" synchOrientation: false] );
				[hAux setViewMode: PROFILE_INFO_VIEW_MODE_CREATING];
				hNextView = hAux;
			}
			break;
			
		case NO_VIEW_INDEX_PROFILE_EDIT:
			{
				NOProfileInfoView* hAux = static_cast< NOProfileInfoView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOProfileInfoView" synchOrientation: false] );
				[hAux setViewMode: PROFILE_INFO_VIEW_MODE_EDITING];
				
				NOCustomer temp;
				[self getTempProfile: &temp];
				[hAux setProfileToEdit: &temp];
				hNextView = hAux;
			}
			break;
			
		case NO_VIEW_INDEX_PROFILE_DOWNLOAD:
			{
				NOProfileAuthenticateView* hAux = static_cast< NOProfileAuthenticateView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOProfileAuthenticateView" synchOrientation: false] );
				[hAux setViewMode: PROFILE_AUTHENTICATE_VIEW_MODE_DOWNLOAD];
				hNextView = hAux;
			}
			break;
			
		case NO_VIEW_INDEX_PROFILE_LOGIN:
			{
				NOProfileAuthenticateView* hAux = static_cast< NOProfileAuthenticateView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOProfileAuthenticateView" synchOrientation: false] );
				[hAux setViewMode: PROFILE_AUTHENTICATE_VIEW_MODE_LOGIN];
				
				NOCustomer temp;
				[self getTempProfile: &temp];
				[hAux setProfileToAuthenticate: &temp];
				hNextView = hAux;
			}
			break;
			
		case NO_VIEW_INDEX_RANKING_ACTION:
			{
				NORankingActionView* hAux = static_cast< NORankingActionView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NORankingActionView" synchOrientation: false] );
				hNextView = hAux;
			}
			break;

		case NO_VIEW_INDEX_RANKING_LOCAL:
			{
				NORankingView* hAux = static_cast< NORankingView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NORankingView" synchOrientation: false] );
				
				NORanking& ranking = NOGlobalData::GetRanking();
				[hAux initViewInMode: RANKING_VIEW_MODE_LOCAL forRanking: &ranking ];
				hNextView = hAux;
			}
			break;

		case NO_VIEW_INDEX_RANKING_GLOBAL:
			{
				NORankingView* hAux = static_cast< NORankingView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NORankingView" synchOrientation: false] );
				
				NORanking& ranking = NOGlobalData::GetRanking();
				[hAux initViewInMode: RANKING_VIEW_MODE_GLOBAL forRanking: &ranking ];
				hNextView = hAux;
			}
			break;

		case NO_VIEW_INDEX_HELP:
			{
				NOHelpView* hAux = static_cast< NOHelpView* >( [[ApplicationManager GetInstance] loadViewFromXib: "NOHelpView" synchOrientation: false] );
				hNextView = hAux;
			}
			break;

		default:
			#if DEBUG
				LOG( "NanoOnlineMotherView performTransitionToView:WithLoadingView: - Invalid view index" );
			#endif
			return;
	}
	
	// Verifica se conseguimos alocar a nova view
	if( ( hNextView == nil ) && ( !nullTransition ) )
	{
		[[ApplicationManager GetInstance] quit: ERROR_NO_ALOCATING_VIEWS];
		return;
	}
	
	// Configura os campos de texto
	[self setViewTxtFieldsDelegate: hNextView];
	
	// Encapsula a próxima view em uma scroll view
	hCurrScrollView = [self encapsulateViewInScrollView: hNextView];
	if( hCurrScrollView == nil )
	{
		[hNextView release];
		[[ApplicationManager GetInstance] quit: ERROR_NO_ALOCATING_VIEWS];
		return;
	}
	
	// OLD: Agora NOControllerView é um singleton
	// Determina o controlador da view criada
	//[hNextView setNOController: self];
	
	// Verifica que elementos da interface devem ficar visíveis
	[self updateInterfaceControls: hNextView];
	
	// Verifica se devemos habilitar o botão de voltar
	screensIndexesStack.push( newIndex );
	if( noFirstView == NO_VIEW_INDEX_MAIN_MENU )
		[self enableBackButton: screensIndexesStack.size() > 1 ];
	else
		[self enableBackButton: screensIndexesStack.size() >= 1 ];

	// Executa a transição
	if( hTransition != nil )
	{
		// Executa a transição de views
		NSArray* hSubviews = [hViewManager subviews];
		[hViewManager transitionFromSubview: ( [hSubviews count] > 0 ? [hSubviews objectAtIndex: 0] : nil ) toSubview: hCurrScrollView keepOldView: keepCurrView freeNewView: true transition: hTransition direction: hDirection duration: NANO_ONLINE_SCREEN_TRANSITION_DURATION ];
	}
	else
	{
		[hViewManager addSubview: hCurrScrollView];
		[hCurrScrollView release];
		
		[hNextView onBecomeCurrentScreen];

		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			
			if( hCurrView != hCurrScrollView )
			{
				if( keepCurrView )
					[hCurrView retain];

				[hCurrView removeFromSuperview];
			}
		}
		
		[[ApplicationManager GetInstance] syncNewViewWithApplicationState];
	}

	// Atualiza o índice da view que está sendo exibida
	currViewIndex = newIndex;
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

===============================================================================================*/

//- ( void )transitionDidStart:( ViewManager* )hManager
//{
//}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

===============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )hManager
{
	#if DEBUG
		LOG( ">>> NanoOnline View Manager has %d subviews\n", [[hManager subviews] count] );
	#endif

	switch( currViewIndex )
	{
		case NO_VIEW_INDEX_MAIN_MENU:
		case NO_VIEW_INDEX_PROFILE_ACTION:
		case NO_VIEW_INDEX_PROFILE_SELECT:
		case NO_VIEW_INDEX_PROFILE_CREATE:
		case NO_VIEW_INDEX_PROFILE_EDIT:
		case NO_VIEW_INDEX_PROFILE_DOWNLOAD:
		case NO_VIEW_INDEX_RANKING_ACTION:
		case NO_VIEW_INDEX_RANKING_LOCAL:
		case NO_VIEW_INDEX_RANKING_GLOBAL:
		case NO_VIEW_INDEX_HELP:
			{
				// OLD
//				NSArray* hSubviews = [hViewManager subviews];
//				if( [hSubviews count] > 0 )
//				{
//					UIView *hCurrView = [hSubviews objectAtIndex:0];
//					if( [hCurrView respondsToSelector: @selector( onBecomeCurrentScreen )] )
//						[hCurrView onBecomeCurrentScreen];
//				}
				
				NSArray* hSubviews = [hViewManager subviews];
				if( [hSubviews count] > 0 )
				{					
					// A primeira subview será a scrollview
					UIScrollView *hScrollView = [hSubviews objectAtIndex: 0];
					
					#if DEBUG
						LOG( ">>> NanoOnline ViewManager ScrollView has %d subviews\n", [[hScrollView subviews] count] );
					#endif
					
					NOBaseView* hNOView = static_cast< NOBaseView* >( [[hScrollView subviews] objectAtIndex: 0] );
					[hNOView onBecomeCurrentScreen];
				}
			}
			break;
	}
	
	[[ApplicationManager GetInstance] syncNewViewWithApplicationState];
}

/*==============================================================================================

MENSAGEM transitionDidCancel:
	Chamada quando uma transição de views é cancelada.

===============================================================================================*/

//- ( void )transitionDidCancel:( ViewManager* )hManager
//{
//}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void )onBecomeCurrentScreen
{
	[self performTransitionToView: noFirstView];
}

/*==============================================================================================

MENSAGEM getInterfaceAnimDur
	Retorna a duração padrão das animações do NanoOnline.
 
===============================================================================================*/

+( float )getInterfaceAnimDur
{
	return NO_INTERFACE_ITEM_ANIM_DUR;
}

/*==============================================================================================

MENSAGEM getInterfaceAnimStyle
	Retorna o estilo padrão das animações do NanoOnline.
 
===============================================================================================*/

+( UIViewAnimationCurve )getInterfaceAnimStyle
{
	return NO_INTERFACE_ITEM_ANIM_CURVE;
}

/*==============================================================================================

MENSAGEM updateInterfaceControls
	Verifica que elementos da interface devem ficar visíveis.
 
===============================================================================================*/

-( void )updateInterfaceControls:( NOBaseView* )hNextView
{
	[self hideError];
	[self enableCancelConnectionButton: false];
	
	[hProgressBar setHidden: YES];
	
	NSString *hScreenTitle = [hNextView getScreenTitle];
	[hViewTitle setText: hScreenTitle];
	
	CGRect barTopFrame = [hBarTop frame];
	CGRect titleFrame = [hViewTitle frame];

	[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_TOP_BAR context: nil];
	[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur] ];
	[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle] ];
	
	if( [hScreenTitle length] > 0 )
	{
		[hNanoOnlineLogo setAlpha: 0.0f];
		
		[hBarTop setFrame: CGRectMake( barTopFrame.origin.x, 0.0f, barTopFrame.size.width, barTopFrame.size.height )];
		[hViewTitle setFrame: CGRectMake( titleFrame.origin.x, 0.0f, titleFrame.size.width, titleFrame.size.height )];
	}
	else
	{
		[hNanoOnlineLogo setAlpha: 1.0f];
		
		[hBarTop setFrame: CGRectMake( barTopFrame.origin.x, -barTopFrame.size.height, barTopFrame.size.width, barTopFrame.size.height )];
		[hViewTitle setFrame: CGRectMake( titleFrame.origin.x, -titleFrame.size.height, titleFrame.size.width, titleFrame.size.height )];
	}

	[UIView commitAnimations];
}

/*==============================================================================================

MENSAGEM animationDidStop:finished:context:
	Indica que a animação de uma view terminou.
 
===============================================================================================*/

-( void )animationDidStop:( NSString* )animationID finished:( NSNumber* )finished context:( void* )context
{
//	if( [animationID compare: NO_CONTROLLER_VIEW_ANIM_ID_BACK_BT] == NSOrderedSame )
//	{
//		if( [hBtBack alpha] == 1.0f )
//			[hBtBack setEnabled: YES];
//	}
//	else if( [animationID compare: NO_CONTROLLER_VIEW_ANIM_ID_CANCEL_CONN_BT] == NSOrderedSame )
//	{
//		if( [hBtCancelConnection alpha] == 1.0f )
//			[hBtCancelConnection setEnabled: YES];
//	}
}

/*==============================================================================================

MENSAGEM encapsulateViewInScrollView:
	Insere a view recebida em uma scroll view configurada para o NanoOnline.
 
===============================================================================================*/

-( UIScrollView* )encapsulateViewInScrollView:( UIView* )hView
{
	// OBS: Se alguma das views no NanoOnline passar a conter uma subview que gere conflitos com uma UIScrollView normal (i.e. UIPicker e UIDatePicker),
	// hAux deverá deixar de ser uma UIScrollView e passar a ser uma UICustomScrollView (incluída nos componentes)
	#if DEBUG
		CGRect viewManagerFrame = [hViewManager frame];
	#endif

	CGSize viewManagerSize = [hViewManager frame].size;
	UIScrollView *hAux = [[UIScrollView alloc] initWithFrame: CGRectMake( 0.0f, 0.0f, viewManagerSize.width, viewManagerSize.height )];
	if( hAux != nil )
	{
		// Insere a view na scroll view
		[hAux addSubview: hView];
		[hView release];

		// Configura a scroll view
		CGRect viewFrame = [hView frame];
		[hAux setContentSize: CGSizeMake( viewFrame.size.width, viewFrame.size.height + NO_TOOLBAR_BAR_HEIGHT - NO_TOOLBAR_BAR_OFFSET_Y )];
		[hAux setScrollEnabled: YES];
		[hAux setDelaysContentTouches: YES];
	}
	return hAux;
}

/*==============================================================================================

MENSAGEM setViewTxtFieldsDelegate
	Atribui hNOController como delegate de todos os campos de texto contidos nesta view.
 
===============================================================================================*/

-( void )setViewTxtFieldsDelegate:( UIView* )hView
{
	if( [hView isKindOfClass: [UITextField class]] )
		[(( UITextField* )hView ) setDelegate: self];
	
	NSArray* hSubviews = [hView subviews];
	if( hSubviews != nil )
	{
		for( UIView *hSubview in hSubviews )
			[self setViewTxtFieldsDelegate: hSubview];
	}
}

/*==============================================================================================

MENSAGEM onShutDown
	Método chamado para terminar o NanoOnline.

===============================================================================================*/

-( void )onShutDown
{
	[[ApplicationManager GetInstance] performTransitionToView: onEndViewIndex];
}

/*==============================================================================================

MENSAGEM setProgress
	Determina a porcentagem que a barra de progresso deve indicar. Todos os valores recebidos
serão colocados no intervalo [0.0, 1.0].

===============================================================================================*/

-( void )setProgress:( float )progress;
{
	[hProgressBar setProgress: progress];
}

/*==============================================================================================

MENSAGEM showWaitViewWithText:
	Trava a interface atual, exibindo um texto e uma animação de espera.

===============================================================================================*/

-( bool )showWaitViewWithText:( NSString* )hText
{
	if( hWaitView != nil )
		[self hideWaitView];
	
	CGSize viewManagerSize = [hViewManager frame].size;
	hWaitView = [[UIWaitView alloc] initWithFrame: CGRectMake( 0.0f, 0.0f, viewManagerSize.width, viewManagerSize.height )];
	if( hWaitView == nil )
		return false;
	
	[hViewManager addSubview: hWaitView];
	[hWaitView release];
	
	if( hText )
		[hWaitView setText: hText];
	else
		[hWaitView setText: [NOControllerView GetText: NO_TXT_PLEASE_WAIT]];
	
	[hCurrScrollView setUserInteractionEnabled: NO];
	
	if( [hBtBack isEnabled] )
	{
		btBackWasEnabled = true;
		[self enableBackButton: false];
	}
	
	// TODOO: Habilitar na próxima versão!!!!!!!!!!!!
	// Fazer, inclusive:
	// - Em casos de erro, mostrar a barra cheia, mas vermelha
	// - Enquanto o progresso não for 100%, barra enche azul
	// - Progresso 100%, sem erros, barra fica verde
	// [hProgressBar setHidden: NO];

	[self enableCancelConnectionButton: true];
	
	[hWaitView setExclusiveTouch: YES];
	[hWaitView show];

	return true;
}

/*==============================================================================================

MENSAGEM hideWaitView:
	Desativa o texto e a animação de espera, destravando a interface.

===============================================================================================*/

-( void )hideWaitView
{
	[hWaitView hide: true];
	hWaitView = nil;
	
	[hCurrScrollView setUserInteractionEnabled: YES];
	
	if( btBackWasEnabled )
		[self enableBackButton: true];
	
	// A barra de progresso ficará com a cor necessária para indicar o que aconteceu.
	//[hProgressBar setHidden: YES];
	[self enableCancelConnectionButton: false];
}

/*==============================================================================================

MENSAGEM showError:
	Exibe para o usuário a descrição de um erro ocorrido.

===============================================================================================*/

-( void )showError:( const NOString& )errorStr
{
	#if DEBUG
		LOG( ">>> NanoOnlineMotherView::showError => %ls\n", errorStr.c_str() );
	#endif

	[hLbErrorMsg setText: [NSString stringWithFormat: @"%@: %@", [NOControllerView GetText: NO_TXT_ERROR], [NOControllerView ConvertSTDStringToNSString: errorStr]]];
	[self showErrorBt];

	CGRect btErrFrame = [hBtError frame];
	[hCallout setAlpha: 1.0f];
	
	// btErrFrame.origin.y + 10.0f => A área do botão é maior do que a imagem utilizada. Assim facilitamos os cliques
	[hCallout setAnchorPoint: CGPointMake( btErrFrame.origin.x + ( btErrFrame.size.width / 2.0f ), btErrFrame.origin.y + 10.0f ) boundaryRect: CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT ) animate: YES];
	
	if( !hCalloutTimer )
		hCalloutTimer = [NSTimer scheduledTimerWithTimeInterval: 5.0f target: self selector: @selector( hideErrorCallout ) userInfo: nil repeats: NO];
}

-( void )showErrorBt
{
	[hBtError setEnabled: YES];

	if( [hBtError alpha] < 1.0f )
	{
		[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_ERROR_BT_IN context: nil];
		[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
		[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
		[hBtError setAlpha: 1.0f];
		[UIView commitAnimations];
	}
}

-( void )hideErrorBt
{
	[hBtError setEnabled: NO];

	if( [hBtError alpha] > 0.0f )
	{
		[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_ERROR_BT_OUT context: nil];
		[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
		[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
		[hBtError setAlpha: 0.0f];
		[UIView commitAnimations];
	}
}

/*==============================================================================================

MENSAGEM toggleErrorMsg
	Exibe ou retira da tela a mensagem que descreve o último erro ocorrido.

===============================================================================================*/

-( void )toggleErrorMsg
{
	[self hideErrorCallout];

	if( [hLbErrorMsg frame].origin.y >= SCREEN_HEIGHT )
		[self showErrorLb];
	else
		[self hideErrorLb];
}

-( void )showErrorLb
{
	CGRect frame = [hLbErrorMsg frame];
	[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_ERRMSG_OUT context: nil];
	[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
	[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
	[hLbErrorMsg setFrame: CGRectMake( frame.origin.x, SCREEN_HEIGHT - ( NO_TOOLBAR_BAR_HEIGHT - NO_TOOLBAR_BAR_OFFSET_Y ) - frame.size.height, frame.size.width, frame.size.height )];
	[UIView commitAnimations];
}

-( void )hideErrorLb
{
	CGRect frame = [hLbErrorMsg frame];
	[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_ERRMSG_OUT context: nil];
	[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
	[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
	[hLbErrorMsg setFrame: CGRectMake( frame.origin.x, SCREEN_HEIGHT, frame.size.width, frame.size.height )];
	[UIView commitAnimations];
}

/*==============================================================================================

MENSAGEM hideErrorCallout
	Acrescenta uma CalloutView à view controladora.

===============================================================================================*/

-( void )hideErrorCallout
{
	hCalloutTimer = nil;

	if( [hCallout alpha] > 0.0f )
	{
		[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_CALLOUT context: nil];
		[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
		[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
		[UIView setAnimationBeginsFromCurrentState: YES];
		[hCallout setAlpha: 0.0f];
		[UIView commitAnimations];
	}
}

/*==============================================================================================

MENSAGEM hideError
	Esconde os componentes de interface que indicam erros.

===============================================================================================*/

-( void )hideError
{
	[self hideErrorCallout];
	[self hideErrorBt];
	[self hideErrorLb];
}

/*==============================================================================================

MENSAGEM createErrorCallout
	Acrescenta uma CalloutView à view controladora.

===============================================================================================*/

-( bool )createErrorCallout
{ 
	hCallout = [[[UICalloutView alloc] initWithFrame:CGRectZero] autorelease]; 
	if( !hCallout )
		return false;

	[self addSubview: hCallout];

	hCallout.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
	hCallout.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter; 
	
	[hCallout setTemporaryTitle: [NOControllerView GetText: NO_TXT_ERROR_OCCURED]];
	[hCallout setTitle: [NOControllerView GetText: NO_TXT_CLICK_FOR_INFO]];
	[hCallout setAlpha: 0.0f];

	return true; 
} 

/*==============================================================================================

MENSAGEM enable:button:withAnimId:
	Realiza a animação padrão para modificar o estado do botão.

===============================================================================================*/

-( void )enableBackButton:( bool )b
{
	[self enable: b button: hBtBack withAnimId: NO_CONTROLLER_VIEW_ANIM_ID_BACK_BT];
}

-( void )enableCancelConnectionButton:( bool )b
{
	[self enable: b button: hBtCancelConnection withAnimId: NO_CONTROLLER_VIEW_ANIM_ID_CANCEL_CONN_BT];
}

-( void )enable:( bool )b button:( UIButton* )hButton withAnimId:( NSString* )hAnimationID
{
	if( hButton )
	{
		[hButton setEnabled: b ? YES : NO];
		
		[UIView beginAnimations: hAnimationID context: nil];
		[UIView setAnimationDuration: [NOControllerView getInterfaceAnimDur]];
		[UIView setAnimationCurve: [NOControllerView getInterfaceAnimStyle]];
		[UIView setAnimationDelegate: self];
		[UIView setAnimationDidStopSelector: @selector( animationDidStop:finished:context: )];

		[hButton setAlpha: b ? 1.0f : 0.0f];

		[UIView commitAnimations];
	}
}

/*==============================================================================================

MENSAGEM setHistoryAsShortestWayToView:
	Altera o caminho percorrido pelo usuário nas telas do NanoOnline. O novo caminho considerado
será o menor da tela principal até a tela recebida como parâmetro.

===============================================================================================*/

-( void )setHistoryAsShortestWayToView:( int8 )viewIndex
{
	while( !screensIndexesStack.empty() )
		screensIndexesStack.pop();

	std::vector< int8 > shortestWay;
	
	if( noFirstView == NO_VIEW_INDEX_MAIN_MENU )
		shortestWay.push_back( NO_VIEW_INDEX_MAIN_MENU );

	switch( viewIndex )
	{
		case NO_VIEW_INDEX_PROFILE_ACTION:
		case NO_VIEW_INDEX_RANKING_ACTION:
		case NO_VIEW_INDEX_HELP:
			break;

		case NO_VIEW_INDEX_RANKING_LOCAL:
		case NO_VIEW_INDEX_RANKING_GLOBAL:
			if( noFirstView != viewIndex )
				shortestWay.push_back( NO_VIEW_INDEX_RANKING_ACTION );
			break;
			
		case NO_VIEW_INDEX_PROFILE_SELECT:
		case NO_VIEW_INDEX_PROFILE_DOWNLOAD:
		case NO_VIEW_INDEX_PROFILE_CREATE:
			if( noFirstView != viewIndex )
				shortestWay.push_back( NO_VIEW_INDEX_PROFILE_ACTION );
			break;
			
		case NO_VIEW_INDEX_PROFILE_EDIT:
		case NO_VIEW_INDEX_PROFILE_LOGIN:
			if( noFirstView != viewIndex )
			{
				shortestWay.push_back( NO_VIEW_INDEX_PROFILE_ACTION );
				shortestWay.push_back( NO_VIEW_INDEX_PROFILE_SELECT );
			}
			break;
	}
	
	std::vector< int8 >::size_type total = shortestWay.size();
	for( std::vector< int8 >::size_type i = 0 ; i < total ; ++i )
		screensIndexesStack.push( shortestWay[i] );
}

/*==============================================================================================

MENSAGEM onBtBackPressed
	Evento recebido quando o usuário pressiona o botão de 'voltar'.

===============================================================================================*/

-( IBAction )onBtBackPressed
{
	if( !screensIndexesStack.empty() )
	{
		// Retira a view antiga da pilha
		screensIndexesStack.pop();

		// Se possuímos uma view anterior...
		if( !screensIndexesStack.empty() )
		{
			[self performTransitionToView: screensIndexesStack.top()];
		
			// Se não fizermos este pop(), screensIndexesStack.top() será duplicado na pilha,
			// já que performTransitionToView empilha o índice da nova tela
			screensIndexesStack.pop();
		}
		// Senão, sai do NanoOnline
		else
		{
			[self onShutDown];
		}

		if( noFirstView == NO_VIEW_INDEX_MAIN_MENU )
			[self enableBackButton: screensIndexesStack.size() > 1 ];
		else
			[self enableBackButton: screensIndexesStack.size() >= 1 ];
	}
	else
	{
		[self onShutDown];
	}
}

/*==============================================================================================

MENSAGEM onBtCancelConnectionPressed
	Cancela uma requisição incompleta.

===============================================================================================*/

-( IBAction )onBtCancelConnectionPressed
{
	NOConnection::GetInstance()->cancelPendingRequest();
}

/*==============================================================================================

MENSAGEM textFieldShouldBeginEditing:
	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( BOOL )textFieldShouldBeginEditing:( UITextField* )hTextField
{
	hLastTextField = ( UILimitedTextField* )hTextField;
	return YES;
}

/*==============================================================================================

MENSAGEM keyboardDidShow:

===============================================================================================*/

-( void ) keyboardDidShow:( NSNotification* )hNotification
{	
	CGRect scrollFrame = [hCurrScrollView frame];
	scrollFrame.size.height -= keyboardHeight;
	[hCurrScrollView setFrame: scrollFrame];
	
	CGRect txtFieldFrame = [hLastTextField frame];
	txtFieldFrame.size.height += KEYBOARD_DIST_FROM_TXT_FIELD;
	[hCurrScrollView scrollRectToVisible: txtFieldFrame animated: YES];
}

/*==============================================================================================

MENSAGEM keyboardDidHide:

===============================================================================================*/

-( void ) keyboardDidHide:( NSNotification* )hNotification
{
}

/*==============================================================================================

MENSAGEM keyboardWillHide:

===============================================================================================*/

-( void ) keyboardWillHide:( NSNotification* )hNotification
{
	float currOffsetY = [hCurrScrollView contentOffset].y;
	if( currOffsetY != offsetBeforeKeyboard )
	{
		NSDictionary *hCopy = [[hNotification userInfo] copy];
		[self startAnimation: @selector( followKeyboardOut: ) WithUserInfo: hCopy];
		[hCopy release];
	}

	CGRect scrollFrame = [hCurrScrollView frame];
	scrollFrame.size.height += keyboardHeight;
	[hCurrScrollView setFrame: scrollFrame];
	
	// Alterar o frame da scrollview caga o offset atual, então precisamos resetar o offset que estava
	// antes. Assim a animação rola suave, como uma pluma na brisa.......(essa metáfora foi brincadeira,
	// sua besta)
	[hCurrScrollView setContentOffset: CGPointMake( 0.0f, currOffsetY )];
}

/*==============================================================================================

MENSAGEM keyboardWillShow:

===============================================================================================*/

-( void ) keyboardWillShow:( NSNotification* )hNotification
{
	if( hLastTextField != nil )
	{
		// Se for teclado numérico, temos que fazer um workaround para compensar a falta
		// da tecla done
		if( [hLastTextField keyboardType] == UIKeyboardTypeNumberPad )
		{
			UIView *hKeyboardView = [NOControllerView findKeyboardView];
			if( hKeyboardView != nil )
			{
				UIImage *hImgBtDoneStateNormal = [NOControllerView getNumberPadDoneBtImg: true];
				UIImage *hImgBtDoneStateHighlighted = [NOControllerView getNumberPadDoneBtImg: false];
	
				if( ( hImgBtDoneStateNormal != nil ) && ( hImgBtDoneStateHighlighted != nil ) )
				{
					UIButton *hDoneButton = [UIButton buttonWithType: UIButtonTypeCustom];
					[hDoneButton setImage: hImgBtDoneStateNormal forState: UIControlStateNormal];
					[hDoneButton setImage: hImgBtDoneStateHighlighted forState: UIControlStateHighlighted];
					[hDoneButton addTarget: self action: @selector( onNumberPadDoneBt: ) forControlEvents: UIControlEventTouchUpInside];
					hDoneButton.adjustsImageWhenHighlighted = NO;
					
					[hKeyboardView addSubview: hDoneButton];

					[hDoneButton setFrame: CGRectMake( 0, hKeyboardView.frame.size.height - hImgBtDoneStateNormal.size.height, hImgBtDoneStateNormal.size.width, hImgBtDoneStateNormal.size.height )];
				}
			}
		}
	
		NSDictionary *hCopy = [[hNotification userInfo] copy];
		[self startAnimation: @selector( followKeyboardIn: ) WithUserInfo: hCopy];
		[hCopy release];
	}
}

/*==============================================================================================

MENSAGEM findKeyboardView
	Acha a view do teclado.

===============================================================================================*/

+( UIView* )findKeyboardView
{
	NSArray *hAppWindows = [[UIApplication sharedApplication] windows];
	uint32 nWindows = [hAppWindows count];
	
	// A primeira janela é a janela da aplicação, por isso começamos com i == 1
	for( uint32 i = 1 ; i < nWindows ; ++i )
	{
		UIWindow *hTempWindow = [hAppWindows objectAtIndex: i];
		NSArray *hWindowSubviews = [hTempWindow subviews];
		
		if( hWindowSubviews != nil )
		{
			uint32 nSubviews = [hWindowSubviews count];

			for( uint32 j = 0 ; j < nSubviews ; ++j )
			{
				UIView *hSubView = [hWindowSubviews objectAtIndex: j];
				
				#if DEBUG
					NSString *hAux = [hSubView description];
					LOG( "%s\n", NSSTRING_TO_CHAR_ARRAY( hAux ) );
				#endif

				if( [[hSubView description] hasPrefix: @"<UIKeyboard"] )
					return hSubView;
			}
		}
	}
	return nil;
}

/*==============================================================================================

MENSAGEM onNumberPadDoneBt:
	Workaround para teclados do tipo Number Pad. Simula o clique da tecla "Done".

===============================================================================================*/

-( void )onNumberPadDoneBt:( UIButton* )hDoneButton
{
	// Chama um método escondido da classe UILimitedTextField para acessar textFieldShouldReturn
	// que por sua vez irá remover o teclado da tela.
	// O ideal seria que pudéssemos enviar um evento "Return" do teclado via programação, o que
	// automaticamente chamaria textFieldShouldReturn de nosso "hiddenDelegate", tornando este
	// método oculto desnecessário. Todavia, não descobri como fazê-lo...
	[[hLastTextField hiddenDelegate] textFieldShouldReturn: hLastTextField];
}

/*==============================================================================================

MENSAGEM startAnimation
	Inicia a animação de scroll do campo que acompanha a movimentação do teclado.

==============================================================================================*/

- ( void ) startAnimation:( SEL )hSelector WithUserInfo:( NSDictionary* )hDictionary
{	
	[self stopAnimation];
	hAnimationTimer = [NSTimer scheduledTimerWithTimeInterval: FOLLOW_KEYBOARD_DELAY target: self selector: hSelector userInfo: hDictionary repeats: NO];
}

/*==============================================================================================

MENSAGEM stopAnimation
	Pára a animação de scroll do campo que acompanha a movimentação do teclado.

==============================================================================================*/

- ( void ) stopAnimation
{
	if( hAnimationTimer )
	{
		[hAnimationTimer invalidate];
		
		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hAnimationTimer invalidate] chama
		// [hAnimationTimer release] automaticamente
		hAnimationTimer = nil;
	}
}

/*==============================================================================================

MENSAGEM followKeyboardOut

===============================================================================================*/

-( void )followKeyboardOut:( NSTimer* )hTimer
{
	// Obtém as informações do teclado
	NSDictionary* hInfo = [hTimer userInfo];

	// Obtém a versão do OS
	std::string osVersion;
	DeviceInterface::GetDeviceSystemVersion( osVersion );

	double animDur;
	UIViewAnimationCurve animCurve;
	if( Utils::CompareVersions( osVersion, "3.0" ) < 0 )
	{
		animDur = 0.3;
		animCurve =  UIViewAnimationCurveEaseInOut;
	}
	else
	{
		#if defined( __IPHONE_3_0 )
			[[hInfo objectForKey: UIKeyboardAnimationDurationUserInfoKey] getValue: &animDur];
			[[hInfo objectForKey: UIKeyboardAnimationCurveUserInfoKey] getValue: &animCurve];
		#else
			animDur = 0.3;
			animCurve =  UIViewAnimationCurveEaseInOut;
		#endif
	}

	CGPoint currScrollOffset = [hCurrScrollView contentOffset];

	[UIView beginAnimations: NO_CONTROLLER_VIEW_ANIM_ID_KEYB_OUT context: nil];
	[UIView setAnimationDuration: animDur];
	[UIView setAnimationCurve: animCurve];
	[UIView setAnimationBeginsFromCurrentState: YES];
	[hCurrScrollView setContentOffset: CGPointMake( currScrollOffset.x, offsetBeforeKeyboard )];
	
	[UIView commitAnimations];

	hAnimationTimer = nil;
}

/*==============================================================================================

MENSAGEM followKeyboardIn
	Não utilizamos a mesma técnica que followKeyboardOut (imitar EXATAMENTE e simultaneamente a
animação do teclado) pois estava dando bugs quando o campo selecionado estava no limite
inferior da UIScrollView. Desta forma, estes erros não ocorrem mais, mas há um lag na animação
da interface. É possível obter através do SVN a versão antiga.

===============================================================================================*/

-( void )followKeyboardIn:( NSTimer* )hTimer
{
	// Obtém a posição do campo de texto na tela
	offsetBeforeKeyboard = [hCurrScrollView contentOffset].y;

	// Move o conteúdo da scrollview para que o teclado não sobreponha o campo de texto atual
	keyboardHeight = [[[hTimer userInfo] objectForKey: UIKeyboardBoundsUserInfoKey] CGRectValue].size.height;
	
	hAnimationTimer = nil;
}

/*==============================================================================================

MENSAGEM getDefaultAvatarForGender:
	Determina o perfil ativo.

===============================================================================================*/

-( void )setActiveProfile:( const NOCustomer* )pProfile
{
	if( pProfile )
		NOGlobalData::GetActiveProfile() = *pProfile;
	else
		NOGlobalData::GetActiveProfile() = NOCustomer();
}

/*==============================================================================================

MENSAGEM getActiveProfile:
	Retorna o perfil ativo.

===============================================================================================*/

-( NOCustomer* )getActiveProfile:( NOCustomer* )pOut
{
	if( pOut )
		*pOut = NOGlobalData::GetActiveProfile();
	
	return pOut;
}

/*==============================================================================================

MENSAGEM setTempProfile:
	Determina os dados do perfil temporário.

===============================================================================================*/

-( void )setTempProfile:( const NOCustomer* )pProfile
{
	if( pProfile )
		tempProfile = *pProfile;
	else
		tempProfile = NOCustomer();
}

/*==============================================================================================

MENSAGEM getTempProfile
	Retorna o perfil temporário.

===============================================================================================*/

-( NOCustomer* )getTempProfile:( NOCustomer* )pOut
{
	if( pOut )
		*pOut = tempProfile;
	
	return pOut;
}

/*==============================================================================================

MENSAGEM setLastLoggedInProfile:
	Salva a identificação única do último perfil a se logar no aparelho.

===============================================================================================*/

-( void )setLastLoggedInProfile:( NOProfileId )profileId
{
	NOGlobalData::GetConfig().lastLoggedInProfile = profileId;
}

/*==============================================================================================

MENSAGEM getLastLoggedInProfile
	Retorna a identificação única do último perfil a se logar no aparelho.

===============================================================================================*/

-( NOProfileId )getLastLoggedInProfile
{
	return NOGlobalData::GetConfig().lastLoggedInProfile;
}

/*==============================================================================================

MENSAGEM sharedInstance
	Retorna o singleton desta classe.

===============================================================================================*/

+( NOControllerView* )sharedInstance
{
	// O certo seria fazermos o seguinte:
	//
//	if( hSingleton == nil )
//		hSingleton = [[super allocWithZone:NULL] init];
//
//	return hSingleton;
	//
	// Mas, para agilizar a programação, este controlador também é uma view,
	// logo carregamos seus elementos gráficos a partir de um arquivo .NIB/.XIB
	// Não descobri um modo de misturar o padrão singleton de ObjC (descrito em
	// http://en.wikipedia.org/wiki/Singleton_pattern#Objective_C)
	// com instanciações através de initWithCoder, porém poderia ter pesquisado
	// mais se houvesse tempo.
	// Enfim, faço uma maracutaia (vulgo "gambiarra") através dos 2 construtores
	// (initWithFrame e initWithCoder) para que o padrão funcione corretamente.

	return hSingleton;
}

/*==============================================================================================

MENSAGEM getDefaultAvatarForGender:
	Retorna o avatar padrão para determinado sexo.

===============================================================================================*/

+( UIImage* )getDefaultAvatarForGender:( Gender )g
{
	char imgsExt[] = "png";

	char imgPath[] = "prd ";
	imgPath[3] = g == GENDER_M ? 'm' : 'f';
	
	char buffer[ PATH_MAX ];
	return [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, imgPath, imgsExt ))];
}

/*==============================================================================================

MENSAGEM ConvertNSString:toSTDString:
	Adiciona uma camada de abstração na conversão de strings. Dentro do NanoOnline, o usuário só
deve utilizar esta função, e não as macros STD_STRING_TO_NSSTRING e NSSTRING_TO_STD_STRING.

===============================================================================================*/

#ifdef NANO_ONLINE_UNICODE_SUPPORT

+( void )ConvertNSString:( const NSString* )hStr toSTDString:( std::wstring& )outStr
{
	outStr.clear();

	int32 nChars = [hStr length];
	for( int32 i = 0 ; i < nChars ; ++i )
		outStr.push_back( static_cast< std::wstring::value_type >( [hStr characterAtIndex: i] ) );
}

+( NSString* )ConvertSTDStringToNSString:( const std::wstring& )str
{
	unichar currChar;
	NSString *hAux = @"";
	size_t nChars = str.length();
	for( size_t i = 0 ; i < nChars ; ++i )
	{
		currChar = static_cast< unichar >( str[i] );
		hAux = [hAux stringByAppendingString: [NSString stringWithCharacters: &currChar length: 1]];
	}
	return hAux;
}

#else

+( void )ConvertNSString:( const NSString* )hStr toSTDString:( std::string& )outStr
{
	outStr = NSSTRING_TO_STD_STRING( hStr );
}

+( NSString* )ConvertSTDStringToNSString:( const std::string& )str
{
	return STD_STRING_TO_NSSTRING( str );
}

#endif

/*==============================================================================================

MENSAGEM getNumberPadDoneBtImg:
	Auxiliar no workaround de teclados do tipo Number Pad. Retorna as imagens utilizadas no botão
"Done" criado por cima do teclado.

===============================================================================================*/

+( UIImage* )getNumberPadDoneBtImg:( bool )buttonUp
{
	char imgsExt[] = "png";

	char imgPath[] = "kbbtdone ";
	imgPath[8] = buttonUp ? 'u' : 'd';
	
	char buffer[ PATH_MAX ];
	return [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, imgPath, imgsExt ))];
}

/*==============================================================================================

MENSAGEM loadConfigurations
	Carrega as configurações do NanoOnline.

================================================================================================*/

-( void )loadConfigurations
{
	std::string fileName;
	GetConfigurationFileName( fileName );
	
	FILE* pConfFile;
	FsOpRes fsStatus = FileSystem::Open( fileName, "rb", &pConfFile );
	if( fsStatus != FS_OK )
		return;
	
	MemoryStream fileData;
	fsStatus = FileSystem::ReadAllBytes( pConfFile, fileData, true );
	FileSystem::Close( pConfFile );
	
	if( fsStatus == FS_OK )
	{
		std::string fileNanoOnlineVersion;
		fileData.readUTF8String( fileNanoOnlineVersion );

		NOConf& config = NOGlobalData::GetConfig();

		// Lê apenas se for a mesma versão. Se tiver uma versão mais avançada, não é para
		// conseguir ler. No entanto, poderíamos ler versões mais antigas (o usuário pode
		// ter feito update do cliente)
		if( Utils::CompareVersions( fileNanoOnlineVersion, NANO_ONLINE_CLIENT_VERSION ) == 0 )
			fileData.readData( reinterpret_cast< uint8* >( &config ), sizeof( NOConf ) );
		else
			config = NOConf();
	}
}

/*==============================================================================================

MENSAGEM saveConfigurations
	Salva as configurações do NanoOnline.

================================================================================================*/

-( void )saveConfigurations
{
	std::string fileName;
	GetConfigurationFileName( fileName );
	
	FILE* pConfFile;
	FsOpRes fsStatus = FileSystem::Open( fileName, "wb", &pConfFile );
	if( fsStatus != FS_OK )
		return;

	MemoryStream fileData;
	NOConf& config = NOGlobalData::GetConfig();
	fileData.writeUTF8String( NANO_ONLINE_CLIENT_VERSION );
	fileData.writeData( reinterpret_cast< uint8* >( &config ), sizeof( NOConf ) );

	FileSystem::WriteAllBytes( pConfFile, fileData, true );
	FileSystem::Close( pConfFile );
}

/*==============================================================================================

MENSAGEM insertNewRecord:
	Salva a pontuação do usuário caso ele não tenha uma melhor já salva e ainda não submetida.

================================================================================================*/

+( void )insertNewRecord:( const int64& )newScore forProfileId:( NOProfileId )profileId;
{
	NOCustomer customer;
	[NOControllerView loadCustomerWithId: profileId inRef: customer];

	// Bom, não era pra acontecer de furar este if... Isso só aconteceria
	// se permitíssemos que um usuário jogasse sem um perfil logado e ainda
	// assim deixássemos submeter um pontuação...
	if( customer.getProfileId() > 0 )
	{	
		[self loadNotSubmittedRecords];
		
		// Verifica se já possuímos uma entrada para este perfil
		// Caso você tenha dúvidas sobre o que eu estou fazendo aqui, não se assuste, mas tb não ache que
		// é gambiarra. Apenas vá estudar um pouco mais de C++. Dica: Functors
		bool changed = false;
		std::string nickname;
		NORanking::NOEntriesContainer& entries = NOGlobalData::GetRanking().getLocalEntries();
		NORanking::NOEntriesContainer::iterator it = find_if( entries.begin(), entries.end(), std::bind2nd( RecordBelongsToCustomer(), customer ) );
		if( it == entries.end() )
		{
			// Insere o novo recorde
			NOString nickname;
			entries.push_back( NORankingEntry( profileId, newScore, customer.getNickname( nickname ) ) );
			
			changed = true;
		}
		else
		{
			int64 currScore;
			it->getScore( currScore );
			
			if( currScore < newScore )
			{
				it->setTimeStamp( Utils::GetTimeStamp() );
				it->setScore( newScore );
				
				changed = true;
			}
		}
		
		if( changed )
		{
			NOGlobalData::GetRanking().sort();
			[self saveNotSubmittedRecords];
		}
	}
}

/*==============================================================================================

MENSAGEM loadNotSubmittedRecords
	Carrega as pontuações ainda não submetidas.

================================================================================================*/

+( void )loadNotSubmittedRecords
{
	std::string fileName;
	GetRankingFileName( fileName );
	
	// Lê o arquivo
	FILE* pRecordsFile;
	FsOpRes fsStatus = FileSystem::Open( fileName, "rb", &pRecordsFile );
	if( fsStatus != FS_OK )
		return;

	MemoryStream fileData;
	fsStatus = FileSystem::ReadAllBytes( pRecordsFile, fileData, true );
	FileSystem::Close( pRecordsFile );
	
	if( fsStatus == FS_OK )
	{
		std::string fileNanoOnlineVersion;
		fileData.readUTF8String( fileNanoOnlineVersion );
		
		NORanking& ranking = NOGlobalData::GetRanking();
		ranking.clear();

		// Lê apenas se for a mesma versão. Se tiver uma versão mais avançada, não é para
		// conseguir ler. No entanto, poderíamos ler versões mais antigas (o usuário pode
		// ter feito update do cliente)
		if( Utils::CompareVersions( fileNanoOnlineVersion, NANO_ONLINE_CLIENT_VERSION ) == 0 )
			ranking.unserialize( fileData );
		else
			ranking = NORanking();
	}
}

/*==============================================================================================

MENSAGEM saveNotSubmittedRecords
	Salva as pontuações ainda não submetidas.

================================================================================================*/

+( void )saveNotSubmittedRecords
{
	std::string fileName;
	GetRankingFileName( fileName );
	
	FILE* pRecordsFile;
	FsOpRes fsStatus = FileSystem::Open( fileName, "wb", &pRecordsFile );
	if( fsStatus != FS_OK )
		return;

	MemoryStream fileData;
	fileData.writeUTF8String( NANO_ONLINE_CLIENT_VERSION );
	NOGlobalData::GetRanking().serialize( fileData );

	FileSystem::WriteAllBytes( pRecordsFile, fileData, true );
	FileSystem::Close( pRecordsFile );
}

/*==============================================================================================

MENSAGEM loadCustomerWithId:inRef:
	Retorna o perfil que possui o profileId recebido como parâmetro. Caso ele não exista, o campo
profileId de customer fica com seu valor padrão.

================================================================================================*/

+( void )loadCustomerWithId:( NOProfileId )profileId inRef:( NOCustomer& )customer
{	
	std::vector< NOCustomer > profiles;
	if( [NOControllerView loadProfiles: profiles] == FS_OK )
	{
		// Caso você tenha dúvidas sobre o que eu estou fazendo aqui, não se assuste, mas tb não ache que
		// é gambiarra. Apenas vá estudar um pouco mais de C++. Dica: Functors
		std::vector< NOCustomer >::iterator it = std::find_if( profiles.begin(), profiles.end(), std::bind2nd( CompareNOCustomerNOProfileId(), profileId ) );
		if( it != profiles.end() )
			customer = *it;
	}
}

/*==============================================================================================

MENSAGEM loadProfiles:
	Carrega os perfis salvos localmente no vetor passado como parâmetro.

================================================================================================*/

+( FsOpRes )loadProfiles:( std::vector< NOCustomer >& )outProfiles
{
	std::string fileName;
	GetProfilesFileName( fileName );
	
	// Lê o arquivo
	FILE* pProfilesFile;
	FsOpRes fsStatus = FileSystem::Open( fileName, "rb", &pProfilesFile );
	if( fsStatus != FS_OK )
		return fsStatus;

	MemoryStream fileData;
	fsStatus = FileSystem::ReadAllBytes( pProfilesFile, fileData, true );
	FileSystem::Close( pProfilesFile );
	
	if( fsStatus == FS_OK )
	{
		outProfiles.clear();

		std::string fileNanoOnlineVersion;
		fileData.readUTF8String( fileNanoOnlineVersion );

		// Lê apenas se for a mesma versão. Se tiver uma versão mais avançada, não é para
		// conseguir ler. No entanto, poderíamos ler versões mais antigas (o usuário pode
		// ter feito update do cliente)
		if( Utils::CompareVersions( fileNanoOnlineVersion, NANO_ONLINE_CLIENT_VERSION ) != 0 )
		{
			// O arquivo é de uma versão que não sabemos ler. Iremos apagá-lo,
			// e tratar a situação como se o arquivo não existisse. O usuário
			// poderá baixar os perfis novamente através da função "Profile Download"
			
			if( FileSystem::DeleteFile( fileName ) == FS_OK )
				fsStatus = FS_FILE_DO_NOT_EXIST;
			else
				fsStatus = FS_UNKNOWN_ERROR; // Simula um erro desconhecido para evitarmos outras operações
		}
		else
		{
			uint16 nProfiles = static_cast< uint16 >( fileData.readInt16() );
			outProfiles.reserve( nProfiles );

			for( uint16 i = 0 ; i < nProfiles ; ++i )
			{
				NOCustomer customerRead;
				customerRead.unserialize( fileData );
				outProfiles.push_back( customerRead );
			}
		}
	}
	return fsStatus;
}

/*==============================================================================================

MENSAGEM saveProfiles:
	Salva localmente os perfis recebidos.

================================================================================================*/

+( FsOpRes )saveProfiles:( const std::vector< NOCustomer >& )profiles
{
	// Armazena os perfis em uma stream
	MemoryStream data;
	
	data.writeUTF8String( NANO_ONLINE_CLIENT_VERSION );
	
	std::size_t nProfiles = profiles.size();
	data.writeInt16( static_cast< int16 >( nProfiles ) );

	for( std::size_t i = 0 ; i < nProfiles ; ++i )
		profiles[i].serialize( data );

	// Salva os perfis
	std::string fileName;
	GetProfilesFileName( fileName );
	
	FILE* pProfilesFile;
	FsOpRes fsStatus = FileSystem::Open( fileName, "w", &pProfilesFile );
	if( fsStatus != FS_OK )
		return fsStatus;

	fsStatus = FileSystem::WriteAllBytes( pProfilesFile, data, true );
	FileSystem::Close( pProfilesFile );
	
	return fsStatus;
}

/*==============================================================================================

MENSAGEM insertProfile:
	Salva um novo perfil localmente.

================================================================================================*/

+( FsOpRes )insertProfile:( const NOCustomer& )profile
{
	std::vector< NOCustomer > currProfiles;
	FsOpRes fsStatus = [NOControllerView loadProfiles: currProfiles];
	if( ( fsStatus == FS_OK ) || ( fsStatus == FS_FILE_DO_NOT_EXIST ) )
	{
		// Verifica se o perfil já existe
		std::vector< NOCustomer >::iterator itProfile = std::find( currProfiles.begin(), currProfiles.end(), profile );
		if( itProfile == currProfiles.end() )
		{
			currProfiles.push_back( profile );
		}
		else
		{
			// Atualiza o perfil, pois podemos ter mudado o valor algum campo
			*itProfile = profile;
		}
		return [NOControllerView saveProfiles: currProfiles];
	}
	return fsStatus;
}

/*==============================================================================================

MENSAGEM updateProfile:
	Atualiza um perfil localmente.

================================================================================================*/

+( FsOpRes )updateProfile:( const NOCustomer& )profile
{
	std::vector< NOCustomer > currProfiles;
	FsOpRes fsStatus = [NOControllerView loadProfiles: currProfiles];
	if( fsStatus == FS_OK )
	{
		std::vector< NOCustomer >::iterator profileToEdit = std::find( currProfiles.begin(), currProfiles.end(), profile );
		if( profileToEdit != currProfiles.end() )
		{
			*profileToEdit = profile;
			return [NOControllerView saveProfiles: currProfiles];
		}
	}
	return fsStatus;
}

/*==============================================================================================

MENSAGEM deleteProfile:
	Remove um perfil do array de perfis local.

================================================================================================*/

+( FsOpRes )deleteProfile:( const NOCustomer& )profile
{
	std::vector< NOCustomer > currProfiles;
	FsOpRes fsStatus = [NOControllerView loadProfiles: currProfiles];
	if( fsStatus == FS_OK )
	{
		std::vector< NOCustomer >::iterator profileToDelete	= std::find( currProfiles.begin(), currProfiles.end(), profile );
		if( profileToDelete != currProfiles.end() )
		{
			currProfiles.erase( profileToDelete );
			return [NOControllerView saveProfiles: currProfiles];
		}
	}
	return fsStatus;
}

/*==============================================================================================

MENSAGEM GetText:
	Retorna um texto do NanoOnline.

================================================================================================*/

#define GET_TXT_BUFFER_SIZE 32

+( NSString* )GetText:( uint16 )textIndex
{
	char buffer[ GET_TXT_BUFFER_SIZE ];
	snprintf( buffer, GET_TXT_BUFFER_SIZE, "%d", static_cast< int32 >( textIndex ) );

	return NSLocalizedStringFromTable( CHAR_ARRAY_TO_NSSTRING( buffer ), @"no", NULL );
}

#undef GET_TXT_BUFFER_SIZE

// Fim da implementação da classe
@end
