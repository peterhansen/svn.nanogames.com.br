#include "ApplicationManager.h"

#include "LoadingView.h"
#include <AudioToolbox/AudioToolbox.h>

// TODOO : Não era para este arquivo incluir arquivos que não pertecem aos componentes !!!
#include "ObjcMacros.h"

// Implementação da classe
@implementation ApplicationManager

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hWindow, hViewManager;


/*==============================================================================================
 
 MENSAGEM appVersion
Obtem a versao vinda diretamente do info.plist 
 ==============================================================================================*/

- (void)appVersion:(std::string &) dst
{
	static std::string appVersion;
	if (appVersion=="")
		{
			appVersion=(NSSTRING_TO_CHAR_ARRAY([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"  ]));
#if DEBUG
			LOG(appVersion.c_str());
#endif
		}
	dst=appVersion;
}

/*==============================================================================================
 
 MENSAGEM isIPod
 Responde se estamos no iPod ou nao
 ==============================================================================================*/

- (BOOL)isIPod
{
	if ([[[UIDevice currentDevice] model] compare: @"iPod touch" options: NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch | NSWidthInsensitiveSearch ] == NSOrderedSame)
		return YES;
	return NO;
}


/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/


- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	// Inicializa as variáveis da classe
	currViewIndex = -1;
	lastViewIndex = -1;
	appState = APPLICATION_STATE_RUNNING;
	
	vibrationOn = false;
	hasVibrationFunc =  ![self isIPod ];
	//[[[UIDevice currentDevice] model] compare: @"iPod touch" options: NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch | NSWidthInsensitiveSearch ] != NSOrderedSame;
	
	// Determina a orientação inicial da aplicação de acordo com a informação contida em Info.plist
	NSString* hOrientationDesc;
	if( ( hOrientationDesc = [[[NSBundle mainBundle] infoDictionary] objectForKey: @"UIInterfaceOrientation"] ) != nil )
	{
		UIInterfaceOrientation orientation = UIInterfaceOrientationPortrait;
		if( [hOrientationDesc compare: @"UIInterfaceOrientationPortraitUpsideDown" ] == NSOrderedSame )
		{
			orientation = UIInterfaceOrientationPortraitUpsideDown;
		}
		else if( [hOrientationDesc compare: @"UIInterfaceOrientationLandscapeLeft" ] == NSOrderedSame )
		{
			orientation = UIInterfaceOrientationLandscapeLeft;
		}
		else if( [hOrientationDesc compare: @"UIInterfaceOrientationLandscapeRight" ] == NSOrderedSame )
		{
			orientation = UIInterfaceOrientationLandscapeRight;
		}

		[application setStatusBarOrientation: orientation animated: NO];
	}
	else
	{
		// O padrão é UIInterfaceOrientationPortrait
		[application setStatusBarOrientation: UIInterfaceOrientationPortrait animated: NO];
	}
	
	// Por padrão, a vibração está habilitada
	[self setVibrationStatus: hasVibrationFunc];
	
	// Impede o device de entrar no modo sleep durante o jogo
	[[UIApplication sharedApplication] setIdleTimerDisabled: YES];
	
	// Indica que este objeto irá receber os eventos do controlador de telas
	// OBS: hViewManager foi criado quando carregamos MainWindow.xib, o que ocorre logo antes de applicationDidFinishLaunching ser chamada
	[hViewManager setDelegate: self];
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS : Descomentar se for necessário personalizar
//- ( void )dealloc
//{	
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM applicationWillResignActive
	Mensagem chamada quando a aplicação vai ser suspensa.

==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	appState = APPLICATION_STATE_SUSPENDED;
}

/*==============================================================================================

MENSAGEM applicationDidBecomeActive
	Mensagem chamada quando a aplicação é reiniciada.

==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	// Inicializa a string da versao da aplicacao		
	appState = APPLICATION_STATE_RUNNING;
}

/*==============================================================================================

MENSAGEM getApplicationState
	Retorna o estado da aplicação.

==============================================================================================*/

- ( ApplicationState )getApplicationState
{
	return appState;
}

/*==============================================================================================

MENSAGEM quit
	Termina a aplicação.

==============================================================================================*/

- ( void )quit:( uint32 )error
{
#if DEBUG
	LOG( "ERROR: Quitting with error: %d\n", error );
#endif

	// OLD : Terminate é uma API privada, e a m&$#@ da Apple reclama se utilizarmos tais
	// APIs. Já que ela não quer que façamos o desligamento da aplicação da forma
	// correta, aplicaremos sempre a forma "bruta"
	UIApplication *app = [UIApplication sharedApplication];
//	SEL selector = @selector( terminate );
//
//	if( [app respondsToSelector:selector] )
//	{
//		[app performSelector:selector];
//	}
//	else
//	{
		[self applicationWillTerminate: app];
		exit( 0 );
//	}
}

/*==============================================================================================

MENSAGEM startLoadingWithTarget: AndSelector:
	Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos
da aplicação.

==============================================================================================*/

-( void ) startLoadingWithTarget:( id )target AndSelector:( SEL )aSelector CallingAfterLoad:( SEL )onLoadEndedSelector
{
	// OLD
//	// Cria a tela de loading
//	UIActivityIndicatorView* hLoadingIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)]; 
//
//	if( [[ApplicationManager GetInstance] isOrientationLandscape] )
//		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH)];
//	else
//		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT)];
//	
//	[hLoadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge]; 
//	[hViewManager addSubview:hLoadingIndicator];
//	[hLoadingIndicator startAnimating];
//	[hLoadingIndicator release];
//
//	// Timer que irá carregar as views do jogo
//	[NSTimer scheduledTimerWithTimeInterval:0.001f target:self selector:aSelector userInfo:NULL repeats:NO];
	
	LoadingView* hLoadingView = ( LoadingView* )[[ApplicationManager GetInstance] loadViewFromXib: "LoadingView" ];
	if( !hLoadingView )
		[self quit: ERROR_CREATING_LOADING_VIEW];
	
	[hLoadingView setLoadingTarget: target AndSelector: aSelector CallingAfterLoad: onLoadEndedSelector];
	
	[hViewManager addSubview: hLoadingView];

	[hLoadingView onBecomeCurrentScreen];
	[hLoadingView release];
}

/*==============================================================================================

MENSAGEM getScreenWidth
	Retorna a largura da tela.

==============================================================================================*/

- ( float )getScreenWidth
{
	return [self isOrientationLandscape] ? [UIScreen mainScreen].bounds.size.height : [UIScreen mainScreen].bounds.size.width;
}

/*==============================================================================================

MENSAGEM getScreenHeight
	Retorna a altura da tela.

==============================================================================================*/

- ( float )getScreenHeight
{
	return [self isOrientationLandscape] ? [UIScreen mainScreen].bounds.size.width : [UIScreen mainScreen].bounds.size.height;
}

/*==============================================================================================

MENSAGEM getDefaultViewport
	Retorna o viewport padrão da aplicação.

==============================================================================================*/

- ( Viewport )getDefaultViewport
{
	Viewport v( 0, 0, static_cast<int32>( [self getScreenWidth] ), static_cast<int32>( [self getScreenHeight] ) );
	return v;
}

/*==============================================================================================

MENSAGEM getOrientation
	Retorna a orientação da aplicação.

==============================================================================================*/

- ( UIInterfaceOrientation )getOrientation
{
	return [[UIApplication sharedApplication]statusBarOrientation];
}

/*==============================================================================================

MENSAGEM isOrientationLandscape
	Indica se a orientação da aplicação pertence a família landscape.

==============================================================================================*/

- ( bool )isOrientationLandscape
{
	return UIInterfaceOrientationIsLandscape( [self getOrientation] );
}

/*==============================================================================================

MENSAGEM isOrientationPortrait
	Indica se a orientação da aplicação pertence a família portrait.

==============================================================================================*/

- ( bool )isOrientationPortrait
{
	return UIInterfaceOrientationIsPortrait( [self getOrientation] );
}

/*==============================================================================================

MENSAGEM GetInstance
	Retorna o controlador da aplicação.

==============================================================================================*/

+ ( ApplicationManager* ) GetInstance
{
	return ( ApplicationManager* )[[UIApplication sharedApplication] delegate];
}

/*==============================================================================================

MÉTODO loadViewFromXib
	Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente, é
necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController à view
propriamente dita.

==============================================================================================*/

- ( UIView* ) loadViewFromXib:( const char* )pViewName
{
	return [self loadViewFromXib: pViewName synchOrientation: true];
}

- ( UIView* ) loadViewFromXib:( const char* )pViewName synchOrientation:( bool )synchOrientation
{
	UIView *hView = NULL;
	UIViewController *hViewController = [[UIViewController alloc] initWithNibName: CHAR_ARRAY_TO_NSSTRING( pViewName ) bundle:NULL];
	
	if( hViewController )
	{
		// Carrega a view já na orientação correta
		// TODOO : synchOrientation é utilizado para corrigir uma falha de lógica quando contemos uma view dentro de outra view
		// que já foi rotacionada por loadViewFromXib. Sem este parâmetro, uma view carregada por loadViewFromXib seria rotacionada
		// n vezes, onde n é o número de views aninhadas
		if( synchOrientation )
		{
			if( [self isOrientationLandscape] )
			{
				CGAffineTransform transform = hViewController.view.transform;
				CGRect bounds = CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT );
				CGPoint center = CGPointMake( bounds.size.height * 0.5f, bounds.size.width * 0.5f );

				hViewController.view.center = center;
				
				if( [self getOrientation] == UIInterfaceOrientationLandscapeRight )
					transform = CGAffineTransformRotate( transform, static_cast< float >( M_PI_2 ) );
				else
					transform = CGAffineTransformRotate( transform, static_cast< float >( -M_PI_2 ) );
				
				hViewController.view.transform = transform;
			}
		}

		hView = [hViewController view];
		[hView retain];
		[hViewController release];
	}

	return hView;
}

/*==============================================================================================

MÉTODO loadViewControllerFromXib
	Carrega um viewcontroller e sua view a partir de um arquivo .xib.

==============================================================================================*/

- ( UIViewController* ) loadViewControllerFromXib:( const char* )pViewControllerName
{
	UIViewController* hViewController = [[UIViewController alloc] initWithNibName: CHAR_ARRAY_TO_NSSTRING( pViewControllerName ) bundle:NULL];
	
	// Carrega a view já na orientação correta
	if( [self isOrientationLandscape] )
	{
		CGAffineTransform transform = hViewController.view.transform;
		CGRect bounds = CGRectMake( 0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT );
		CGPoint center = CGPointMake( bounds.size.height * 0.5f, bounds.size.width * 0.5f );

		hViewController.view.center = center;
		
		if( [self getOrientation] == UIInterfaceOrientationLandscapeRight )
			transform = CGAffineTransformRotate( transform, static_cast< float >( M_PI_2 ) );
		else
			transform = CGAffineTransformRotate( transform, static_cast< float >( -M_PI_2 ) );
		
		hViewController.view.transform = transform;
	}

	[hViewController retain];
	return hViewController;
}

/*==============================================================================================

MENSAGEM setVibrationStatus:
	Indica se o device possui a funcionalidade de vibração.

==============================================================================================*/

- ( bool ) hasVibration
{
	return hasVibrationFunc;
}

/*==============================================================================================

MENSAGEM setVibrationStatus:
	Determina se a vibração está ligada ou desligada.

==============================================================================================*/
	
- ( void )setVibrationStatus:( bool ) on
{
	if( [self hasVibration] )
		vibrationOn = on;
	else
		vibrationOn = false;
}

/*==============================================================================================

MENSAGEM isVibrationOn
	Indica se a vibração está ligada / desligada.

==============================================================================================*/
	
- ( bool )isVibrationOn
{
	return vibrationOn;
}

/*==============================================================================================

MENSAGEM vibrate:
	Faz o device vibrar.

==============================================================================================*/

- ( void )vibrate:( float )time;
{
	if( [self isVibrationOn] )
	{
		// TASK: Como fazer para determinar o tempo de vibração ?????
		AudioServicesPlaySystemSound( kSystemSoundID_Vibrate );

//		// TODOO Rodar em uma thread separada....
//		for( int32 i = 0 ; i < nTimes ; ++i )
//		{
//			AudioServicesPlaySystemSound( kSystemSoundID_Vibrate );
//			sleep (0.7f );
//		}
	}
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

- ( void )transitionDidStart:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

- ( void )performTransitionToView:( int8 )newIndex
{
	[self performTransitionToView: newIndex WithLoadingView: NULL];
}

- ( void )performTransitionToView:( int8 )newIndex WithLoadingView:( const LoadingView* )hLoadindView
{
	// Vazio mesmo. Queremos que as subclasses implementem este método
}

/*==============================================================================================

MENSAGEM syncNewViewWithApplicationState
	Sincrozina o estado da view atual com o estado da aplicação. Este método serve para contornar
a possibilidade de recebermos um evento de suspend enquanto estamos transitando entre views.

==============================================================================================*/
	
- ( void ) syncNewViewWithApplicationState
{
	if( [self getApplicationState] == APPLICATION_STATE_SUSPENDED )
	{
		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			if( [hCurrView respondsToSelector: @selector( suspend )] )
				[hCurrView suspend];
		}
	}
}

// Fim da implementação da classe
@end
