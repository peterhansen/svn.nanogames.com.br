/*
 *  ApplicationManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 2/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef APPLICATION_MANAGER_H
#define APPLICATION_MANAGER_H 1

// Apple
#import <UIKit/UIKit.h>

// Components
#include "ApplicationErrors.h"
#include "NanoTypes.h"
#include "ViewManager.h"
#include "Viewport.h"
#include <string>

enum ApplicationState
{
	APPLICATION_STATE_RUNNING =		0,
	APPLICATION_STATE_SUSPENDED =	1
};

@interface ApplicationManager : NSObject <UIApplicationDelegate, TransitionDelegate>
{
	@protected	
		// Índice da view atual
		int8 currViewIndex;
	
		// Índice da última view
		int8 lastViewIndex;

		// Janela da aplicação
		IBOutlet UIWindow* hWindow;
		
		// Responsável por realiza a transição entre as views da aplicação
		IBOutlet ViewManager* hViewManager;
	
	@private
		// Estado atual da aplicação
		ApplicationState appState;
	
		// Indica se o device possui vibração
		bool hasVibrationFunc;
	
		// Indica se a vibração está ligada / desligada
		bool vibrationOn;
}

// Cria os setters e getters para estas propriedades
@property ( nonatomic, assign ) UIWindow* hWindow;
@property ( nonatomic, assign ) ViewManager* hViewManager;

// Copia para uma outra string a versao da aplicacao


// Retorna o controlador da aplicação
+ ( ApplicationManager* ) GetInstance;

// Retorna o estado da aplicação
- ( ApplicationState )getApplicationState;

// Termina a aplicação
- ( void )quit:( uint32 )error;

// Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos da aplicação
-( void ) startLoadingWithTarget:( id )target AndSelector:( SEL )aSelector CallingAfterLoad:( SEL )onLoadEndedSelector;

// Retorna a largura da tela
- ( float )getScreenWidth;

// Retorna a altura da tela
- ( float )getScreenHeight;

// Retorna o viewport padrão da aplicação
- ( Viewport )getDefaultViewport;

// Retorna a orientação da aplicação
- ( UIInterfaceOrientation )getOrientation;

// Indica se a orientação da aplicação pertence a família landscape
- ( bool )isOrientationLandscape;

// Indica se a orientação da aplicação pertence a família portrait
- ( bool )isOrientationPortrait;

// Carrega uma view a partir de um arquivo .xib. No entanto, para que funcione corretamente,
// é necessário linkar através do InterfaceBuilder a propriedade "view" de um UIViewController
// à view propriamente dita
- ( UIView* ) loadViewFromXib:( const char* )pViewName;
- ( UIView* ) loadViewFromXib:( const char* )pViewName synchOrientation:( bool )synchOrientation;

// Carrega um viewcontroller e sua view a partir de um arquivo .xib
- ( UIViewController* ) loadViewControllerFromXib:( const char* )pViewControllerName;

// Indica se o device possui a funcionalidade de vibração
- ( bool ) hasVibration;

// Determina se a vibração está ligada ou desligada
- ( void ) setVibrationStatus:( bool ) on;

// Indica se a vibração está ligada / desligada
- ( bool ) isVibrationOn;

// Faz o device vibrar
- ( void ) vibrate:( float )time;

// Sincrozina o estado da view atual com o estado da aplicação. Este método serve para contornar
// a possibilidade de recebermos um evento de suspend enquanto estamos transitando entre views
- ( void ) syncNewViewWithApplicationState;

// diz se iPod ou nao
- (BOOL)isIPod;

- (void)appVersion:(std::string &) dst;

@end

#endif
