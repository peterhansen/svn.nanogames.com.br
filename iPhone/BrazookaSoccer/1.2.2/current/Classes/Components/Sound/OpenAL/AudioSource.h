/*
 *  AudioSource.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 12/4/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_SOURCE_H
#define AUDIO_SOURCE_H 1

#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

#include "AudioBufferAL.h"
#include "Point3f.h"

// Número máximo de buffers de áudio que podem ser colocados em sequência em uma fonte
#define MAX_AUDIO_BUFFERS_PER_SOURCE 5

// Pré-definição da classe AudioContext para evitar #includes recursivos
class AudioContext;

class AudioSource
{
	public:
		// Destrutor
		virtual ~AudioSource( void );
	
		// Retorna o AudioContext que controla este AudioSource
		inline const AudioContext* getParentContext( void ) const { return pParentContext; };
	
		// Retorna o estado da fonte de aúdio
		ALint getState( void ) const;
	
		// Adiciona um buffer de áudio à fonte
		bool insertAudioBuffer( AudioBufferAL* pBuffer, uint8* pIndex = NULL );
	
		// Remove um buffer de áudio da fonte. Retorna o buffer se conseguir executar a remoção ou
		// NULL em caso de falhas
		AudioBufferAL* removeAudioBuffer( uint8 index );
	
		// Inicia a execução da sequência de áudios ligados à source
		bool play( void ) const;
	
		// Suspende a execução da sequência de aúdios ligados à source
		bool pause( void ) const;
	
		// Pára a execução da sequência de aúdios ligados à source
		bool stop( void ) const;
		
		// Reinicia a execução da sequência de aúdios ligados à source
		bool rewind( void ) const;
	
		// Indica que a sequência de áudios deve ser executada em loop
		bool setLooping( bool loop ) const;
	
		// Retorna se a sequência de áudios está sendo executada em loop
		bool isLooping( void ) const;
	
		// Indica se os atributos "position", "speed", "cone" e "direction" devem ser interpretados
		// relativos à posição do listener
		bool setRelativeToListener( bool b ) const;
	
		// Determina a posição da fonte de áudio
		bool setPosition( const Point3f* pPos ) const;
	
		// Determina a velocidade da fonte de áudio
		bool setSpeed( const Point3f* pVel ) const;
	
		// Determina a direção do áudio emitido pela fonte
		bool setDirection( const Point3f* pDir ) const;
	
		// Determina o volume atual da fonte de áudio
		bool setGain( float gain ) const;
	
		// Determina o volume mínimo da fonte de áudio
		bool setMinGain( float minGain ) const;
	
		// Determina o volume máximo da fonte de áudio
		bool setMaxGain( float maxGain ) const;
	
		// Retorna o volume máximo da fonte de áudio
		bool getMaxGain( float* pMaxGain ) const;
	
		// Determina qual a maior distância que a fonte pode ficar do listener
		bool setMaxDistance( float maxDistance ) const;
	
		// Determina a distância de referência em relação ao listener
		bool setRefDistance( float referenceDistance ) const;
	
		// Determina o fator de atenuação por distância. Seu valor padrão é 1.0f. 0.0f indica que
		// não há atenuação por distância
		bool setRollOffFactor( float rollOffFactor ) const;
	
	protected:
		// Objetos da classe AudioContext terão acesso aos métodos e atributos protected de AudioSource
		friend class AudioContext;
	
		// Construtor
		// Exceções : OpenALException
		AudioSource( AudioContext* pContext );

	private:
		// Métodos auxiliares
		ALint SetSourceI( ALenum param, ALint value ) const;
		ALint GetSourceI( ALenum param, ALint* pValue ) const;
	
		ALint SetSourceF( ALenum param, ALfloat pValue ) const;
		ALint GetSourceF( ALenum param, ALfloat* pValue ) const;
	
		ALint SetSourceFV( ALenum param, const ALfloat* pValues ) const;
		ALint GetSourceFV( ALenum param, ALfloat* pValues ) const;

		ALint ExecSourceFunction( uint8 code ) const;
	
		// Sequência de áudios a serem executados pela fonte
		AudioBufferAL* buffers[ MAX_AUDIO_BUFFERS_PER_SOURCE ];
	
		// ID único da fonte de som fornecido pelo OpenAL
		ALuint sourceId;
	
		// Ponteiro para o contexto que controla esta fonte de som
		AudioContext* pParentContext;
};

#endif COMPONENTS_CONFIG_ENABLE_OPENAL

#endif AUDIO_SOURCE_H
