/*
 *  AudioBuffer.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 12/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_BUFFER_H
#define AUDIO_BUFFER_H 1

#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

#include "NanoTypes.h"

class AudioBufferAL
{
	public:
		// Construtor
		// Exceções : OpenALException
		AudioBufferAL( void );
	
		// Destrutor
		virtual ~AudioBufferAL( void );
	
		// Retorna o ID do buffer
		inline ALuint getBufferId( void ) const { return bufferId; };
	
		// Preenche este buffer com os dados contidos em pData
		bool fillBuffer( const ALvoid* pData, ALsizei size, ALenum format, ALsizei freq );
	
		// Retorna a contagem de referências do objeto
		inline uint16 getRefCount( void ) const { return refCount; };
	
		// Incrementa a contagem de referências do objeto
		inline void increaseRefCount( void ){ ++refCount; };
	
		// Decrementa a contagem de referências do objeto e retorna se esta chegou a zero
		inline bool decreaseRefCount( void ){ if( refCount > 0 )return ( --refCount ) == 0; return true; };

	private:
		// Contador de referências
		uint16 refCount;

		// ID único do buffer fornecido pelo OpenAL
		ALuint bufferId;
};

#endif COMPONENTS_CONFIG_ENABLE_OPENAL

#endif AUDIO_BUFFER_H
