#include "Utils.h"

// Components
#include "ObjcMacros.h"
#include "MathFuncs.h"

// C
#include <stdio.h>
#include <stdarg.h>
#include <sys/sysctl.h>
#import <mach/mach.h>
#import <mach/mach_host.h>

// iPhoneOS / Foundation
#include <AudioToolbox/AudioToolbox.h>

// OpenGL
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

/*==============================================================================================

FUNÇÃO GetElapsedTime
	 Retorna o tempo transcorrido desde o início da execução do programa.

==============================================================================================*/

double Utils::GetElapsedTime( void )
{	
	// OLD1
//	return static_cast< double >( clock() ) / CLOCKS_PER_SEC;
	
	// OLD2
//	return [NSDate timeIntervalSinceReferenceDate];
	
	return CFAbsoluteTimeGetCurrent();
}

/*==============================================================================================

FUNÇÃO GetLogFileHandle
	 Retorna um file handle para o arquivo padrão de log da aplicação.

==============================================================================================*/

// TODOO
//static FILE* GetLogFileHandle( void )
//{
//	char path[FILENAME_MAX];
//	path[0] = '\0';
//
//#if TARGET_IPHONE_SIMULATOR
//	const char* logPath = [NSHomeDirectory() UTF8String];
//	strcat( path, logPath );
//	strcat( path, "/tmp/log.txt" );
//#else
//	const char* logPath = [NSTemporaryDirectory() UTF8String];
//	strcat( path, logPath );
//	strcat( path, "/log.txt" );
//#endif
//
//	return fopen( path, "a" );
//}

/*==============================================================================================

FUNÇÃO LogWithFileHandle
	 Retorna um file handle para o arquivo padrão de log da aplicação.

==============================================================================================*/

// TODOO
//static bool LogWithFileHandle( FILE* pFile, NSString* hFormat, va_list params )
//{
//	bool ret = false;
//	if( pFile )
//	{
//		NSString* hLog = [[NSString alloc]initWithFormat:hFormat locale:NULL arguments:params];
//
//		if( hLog )
//		{
//			const char *pAux = [hLog UTF8String];
//			
//			if( fprintf( pFile, pAux ) >= 0 )
//				ret = true;
//
//			fclose( pFile );
//			
//			[hLog release];
//		}
//	}
//	return ret;
//}

/*==============================================================================================

MÉTODO Log
	 Salva a string de log no arquivo padrão retornado por getLogFileHandle().

==============================================================================================*/

void Utils::Log( const char* pFormatStr, ... )
{
	va_list params;
	va_start( params, pFormatStr );
	
#if TARGET_IPHONE_SIMULATOR
	vfprintf( stderr, pFormatStr, params );
#else
	NSLogv( CHAR_ARRAY_TO_NSSTRING( pFormatStr ), params );
#endif
	
	va_end( params );
}

/*==============================================================================================

MÉTODO glLog
	 Loga o erro do openGL na janela console.

==============================================================================================*/

#if DEBUG

void Utils::glLog( const char* pFileName, const unsigned long line )
{
	const GLenum error = glGetError();
	if( error != GL_NO_ERROR )
		Utils::Log( "Arquivo %s, linha %d: glError %d\n", pFileName, line, error );
}

#endif

/*==============================================================================================

MÉTODO PrintAllFonts
	 Imprime no console todas as fontes disponíveis no device.

==============================================================================================*/

#if DEBUG

void PrintAllFonts( void )
{	
	// List all fonts on iPhone
	NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
	NSArray *fontNames;
	NSUInteger indFamily, indFont;

	for( indFamily = 0 ; indFamily < [familyNames count] ; ++indFamily )
	{
		LOG( "Family name: %s\n", NSSTRING_TO_CHAR_ARRAY( [familyNames objectAtIndex:indFamily] ) );

		fontNames = [[NSArray alloc] initWithArray: [UIFont fontNamesForFamilyName: [familyNames objectAtIndex:indFamily]]];

		for( indFont = 0 ; indFont < [fontNames count] ; ++indFont )
			LOG( "    Font name: %s\n", NSSTRING_TO_CHAR_ARRAY( [fontNames objectAtIndex:indFont] ) );

		[fontNames release];
	}

	[familyNames release];
}

#endif

/*==============================================================================================

MÉTODO LogThroughSocket
	 Escreve a string de log na conexão socket.

==============================================================================================*/

// TASK
//bool Utils::LogThroughSocket( const char* pHostName, uint16 port, NSString* hFormat, ... )
//{
//	// Inicializa a estrutura de identificação do host
//	// OBS: Não é necessário desalocar esta estrutura, pois ela aponta para dados estáticos
//	const struct hostent* hptr = gethostbyname( pHostName );
//	if( !hptr )
//		return false;
//
//	// Configura o servidor do socket
//	struct sockaddr_in server;
//	memcpy( ( char* )&server.sin_addr, hptr->h_addr_list[0], hptr->h_length );
//
//	server.sin_family = AF_INET;
//	server.sin_port = htons( ( int16 )port ); // Converte para o formato de internet
//	
//	// Obtém o identificador do socket
//	const int32 sockDesc = socket( AF_INET, SOCK_STREAM, 0 );
//	if( sockDesc < 0 )
//		return false;
//
//	// Abre e estabelece a conexão
//	const int32 fileDesc = connect( sockDesc, ( struct sockaddr* )&server, sizeof( server ) );
//	if( fileDesc < 0 )
//		return false;
//	
//	// Faz o log
//	va_list params;
//	va_start( params, hFormat );
//	
//	const bool ret = logWithFileHandle( fdopen( fileDesc, "w" ), hFormat, params );
//	
//	va_end( params );
//
//	return ret;
//}

/*==============================================================================================

MÉTODO Fcmp
	Compara dois números em ponto flutuante. Referência:
	- http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm

==============================================================================================*/

// TASK
//int8 Utils::Fcmp( float f1, float f2, int32 maxUlps )
//{
//	// Garante que maxUlps é pequeno o suficiente para que os valores de NAN padrão não
//	// sejam comparados como iguais a um valor
//    maxUlps = NanoMath::clamp( maxUlps, 1, 4 * 1024 * 1024 );
//
//	// Faz com que f1AsInt esteja lexicograficamente ordenado como um inteiro complemento a 2
//    int32 f1AsInt = *( int32* )&f1;
//    if( f1AsInt < 0 )
//        f1AsInt = 0x80000000 - f1AsInt;
//
//    // Faz com que f2AsInt esteja lexicograficamente ordenado como um inteiro complemento a 2
//    int32 f2AsInt = *( int32* )&f2;
//    if( f2AsInt < 0 )
//        f2AsInt = 0x80000000 - f2AsInt;
//
//	return NANO_ABS( f1AsInt - f2AsInt ) <= maxUlps ? 0 : ( f1AsInt > f2AsInt ? 1 : -1 );
//}

// TASK
//int8 Utils::Fcmp( double f1, double f2, int64 maxUlps )
//{
//	// Garante que maxUlps é pequeno o suficiente para que os valores de NAN padrão não
//	// sejam comparados como iguais a um valor
//    maxUlps = NanoMath::clamp( maxUlps, 1, 4 * 1024 * 1024 * 1024 );
//
//	// Faz com que f1AsInt esteja lexicograficamente ordenado como um inteiro complemento a 2
//    int64 f1AsInt = *( int64* )&f1;
//    if( f1AsInt < 0 )
//        f1AsInt = 0x8000000000000000LL - f1AsInt;
//
//    // Faz com que f2AsInt esteja lexicograficamente ordenado como um inteiro complemento a 2
//    int64 f2AsInt = *( int64* )&f2;
//    if( f2AsInt < 0 )
//        f2AsInt = 0x8000000000000000LL - f2AsInt;
//
//	return NANO_ABS( f1AsInt - f2AsInt ) <= maxUlps ? 0 : ( f1AsInt > f2AsInt ? 1 : -1 );
//}

/*==============================================================================================

MÉTODO IntersectRayTriangle
	Retorna se o raio colide com o triângulo. Retorna as coordenadas baricêntricas em u, v e a
distância para a origem do raio em t.

==============================================================================================*/

bool Utils::IntersectRayTriangle( bool testCull, const Ray* pRay, const Triangle* pTriangle, float* t, float* u, float* v )
{
	// Acha os vetores para dois extremos que compartilhem vert0
	const Point3f edge1 = pTriangle->vertexes[1] - pTriangle->vertexes[0];
	const Point3f edge2 = pTriangle->vertexes[2] - pTriangle->vertexes[0];
	
	// Calcula o determinante
	const Point3f pVec = pRay->direction % edge2;
	const float det = edge1 * pVec;
	
	if( testCull )
	{
		// Se o determinante é zero, o raio está no mesmo plano do triângulo
		if( NanoMath::fltn( det, 0.0f ) )
			return false;
		
		// Calcula a distância de vert0 para a origem do raio
		const Point3f tVec = pRay->origin - pTriangle->vertexes[0];
		
		// Calcula o parâmetro U e testa os limites
		*u = tVec * pVec;
		if( NanoMath::fltn( *u, 0.0f ) || NanoMath::fgtn( *u, det ) )
			return false;
		
		// Calcula o parâmetro V e testa os limites
		const Point3f qVec = tVec % edge1;
		
		*v = pRay->direction * qVec;
		if( NanoMath::fltn( *v, 0.0f ) || NanoMath::fgtn( *u + *v, det ) )
			return false;
		
		// O raio intersecta o triângulo. Calcula t
		*t = edge2 * qVec;
		const float invDet = 1.0f / det;
		*t *= invDet;
		*u *= invDet;
		*v *= invDet;
	}
	else
	{
		// Se o determinante é zero, o raio está no mesmo plano do triângulo
		if( NanoMath::feql( det, 0.0f ) )
			return false;
		
		// Calcula a distância de vert0 para a origem do raio
		const Point3f tVec = pRay->origin - pTriangle->vertexes[0];
		
		// Calcula o parâmetro U e testa os limites
		const float invDet = 1.0f / det;
		*u = ( tVec * pVec ) * invDet;
		if( NanoMath::fltn( *u, 0.0f ) || NanoMath::fgtn( *u, 1.0f ) )
			return false;
		
		// Calcula o parâmetro V e testa os limites
		const Point3f qVec = tVec % edge1;

		*v = ( pRay->direction * qVec ) * invDet;
		if( NanoMath::fltn( *v, 0.0f ) || NanoMath::fgtn( *u + *v, 1.0f ) )
			return false;

		// O raio intersecta o triângulo. Calcula t
		*t = ( edge2 * qVec ) * invDet;
	}
	return true;
}

/*==============================================================================================

MÉTODO GetPickingRay
	Cria um raio a partir das coordenadas da tela recebidas.

==============================================================================================*/

Ray* Utils::GetPickingRay( Ray* pRay, const Camera* pCamera, int32 x, int32 y )
{
	// Obtém o viewport
	Viewport viewport;
	GetViewport( &viewport );
	
	// Transforma as coordenadas para o espaço local
	Point3f near( x, y, 0.0f );
	if( !Unproject( &near, pCamera, &near ) )
		return  NULL;

	Point3f far( x, y, 1.0f );
	if( !Unproject( &far, pCamera, &far ) )
		return  NULL;
	
	// Determina a direção do raio
	pRay->direction = far - near;
	pRay->direction.normalize();
	
	// Manda a origem do raio do espaço do obsevador para o espaço local
	Matrix4x4 modelViewInverse;
	pCamera->getModelViewMatrix( &modelViewInverse );
	modelViewInverse.inverse();
	
	pRay->origin.set();
	if( !TransformCoord( &pRay->origin, &pRay->origin, &modelViewInverse ) )
		return NULL;

	return pRay;
}

/*==============================================================================================

MÉTODO Unproject
	Leva as coordenadas do espaço da tela para o espaço local.

==============================================================================================*/

bool Utils::Unproject( Point3f* pUnprojected, const Camera* pCamera, const Point3f* pPoint )
{
	Viewport viewport;
	GetViewport( &viewport );
	
	Matrix4x4 transforms, modelview;
	pCamera->getModelViewMatrix( &modelview );
	pCamera->getProjectionMatrix( &transforms );
	
	transforms *= modelview;

	if( !transforms.inverse() )
		return false;

    // Mapeia x e y a partir das coordenadas da tela e coloca suas 3 componentes no intervalo [-1,1]
	Point3f aux(	((( pPoint->x - viewport.x ) / viewport.width  ) * 2.0f ) - 1.0f,
					((( pPoint->y - viewport.y ) / viewport.height ) * 2.0f ) - 1.0f,
					( pPoint->z * 2.0f ) - 1.0f );
	
	// Transforma as coordenadas
	if( TransformCoord( &aux, &aux, &transforms ) == NULL )
		return false;
	
	*pUnprojected = aux;
	
	return true;
}

/*==============================================================================================

MÉTODO Project
	Leva as coordenadas do espaço local para o espaço da tela.

==============================================================================================*/

bool Utils::Project( Point3f* pProjected, const Camera* pCamera, const Point3f* pPoint )
{
	// Obtém as matrizes de transformação
	Matrix4x4 modelView, projection;
	pCamera->getModelViewMatrix( &modelView );
	pCamera->getProjectionMatrix( &projection );
	
	// Transforma as coordenadas
	Point3f aux;
	if( !TransformCoord( &aux, pPoint, &modelView ) 
		|| !TransformCoord( &aux, &aux, &projection ) )
		return false;
	
	// Mapeia os valores no intervalo [0.0f, 1.0f]
	aux.x = ( aux.x * 0.5f ) + 0.5f;
	aux.y = ( aux.y * 0.5f ) + 0.5f;
	aux.z = ( aux.z * 0.5f ) + 0.5f;

    // Mapeia x e y para o viewport
	Viewport viewport;
	GetViewport( &viewport );

    aux.x = ( aux.x * viewport.width ) + viewport.x;
	aux.y = ( aux.y * viewport.height ) + viewport.y;
	
	*pProjected = aux;

    return true;
}

/*==============================================================================================

MÉTODO TransformRay
	Transforma as coordenadas do raio utilizando a matriz 4x4 apontada por pTransformMtx.

==============================================================================================*/

void Utils::TransformRay( Ray* pRay, const Matrix4x4* pTransformMtx )
{
	// Aplica a transformação na origem do raio ( w = 1 )
	TransformCoord( &pRay->origin, &pRay->origin, pTransformMtx );

	// Aplica a transformação na direção do raio ( w = 0 )
	TransformNormal( &pRay->direction, &pRay->direction, pTransformMtx );

	// Normaliza a direção do raio
	pRay->direction.normalize();
}

/*==============================================================================================

MÉTODO TransformCoord
	Transforma o ponto, pPoint(x, y, z, 1), pela matriz, pMtx, projetando o resultado de volta ao
plano w = 1.

==============================================================================================*/

Point3f* Utils::TransformCoord( Point3f* pOut, const Point3f* pPoint, const Matrix4x4* pMtx )
{
	// Supõe pPoint->w == 1
	const float x = pPoint->x * pMtx->_00 + pPoint->y * pMtx->_01 + pPoint->z *  pMtx->_02 + pMtx->_03;
	const float y = pPoint->x * pMtx->_10 + pPoint->y * pMtx->_11 + pPoint->z *  pMtx->_12 + pMtx->_13;
	const float z = pPoint->x * pMtx->_20 + pPoint->y * pMtx->_21 + pPoint->z *  pMtx->_22 + pMtx->_23;
	const float w = pPoint->x * pMtx->_30 + pPoint->y * pMtx->_31 + pPoint->z *  pMtx->_32 + pMtx->_33;

	if( NanoMath::feql( w, 0.0f ) )
		return NULL;

	pOut->x = x / w;
	pOut->y = y / w;
	pOut->z = z / w;

	return pOut;
}

/*==============================================================================================

MÉTODO TransformNormal
	Transforma o vetor, pNormal(x, y, z, 0), pela matriz pMtx.

==============================================================================================*/

Point3f* Utils::TransformNormal( Point3f* pOut, const Point3f* pNormal, const Matrix4x4* pMtx )
{
	// Supõe pNormal->w == 0
	const float x = pNormal->x * pMtx->_00 + pNormal->y * pMtx->_01 + pNormal->z *  pMtx->_02;
    const float y = pNormal->x * pMtx->_10 + pNormal->y * pMtx->_11 + pNormal->z *  pMtx->_12;
    const float z = pNormal->x * pMtx->_20 + pNormal->y * pMtx->_21 + pNormal->z *  pMtx->_22;
    const float w = pNormal->x * pMtx->_30 + pNormal->y * pMtx->_31 + pNormal->z *  pMtx->_32;

	// TODOO Quando estamos transformando uma normal, temos que fazer estes testes????
	if( NanoMath::feql( w, 0.0f ) )
		return NULL;

	pOut->x = x / w;
	pOut->y = y / w;
	pOut->z = z / w;

	return pOut;
}

/*==============================================================================================

MÉTODO GetProjectionMatrix
	Retorna a matriz de projeção que está sendo utilizada pela API gráfica.

==============================================================================================*/

Matrix4x4* Utils::GetProjectionMatrix( Matrix4x4* pOut )
{
	glGetFloatv( GL_PROJECTION_MATRIX, *pOut );
	pOut->transpose(); // Matrizes OpenGL são column-major
	return pOut;
}

/*==============================================================================================

MÉTODO GetModelViewMatrix
	Retorna a matriz de modelo/visão que está sendo utilizada pela API gráfica.

==============================================================================================*/

Matrix4x4* Utils::GetModelViewMatrix( Matrix4x4* pOut )
{
	glGetFloatv( GL_MODELVIEW_MATRIX, *pOut );
	pOut->transpose(); // Matrizes OpenGL são column-major
	return pOut;
}

/*==============================================================================================

MÉTODO GetViewport
	Retorna o viewport que está sendo utilizado pela API gráfica.

==============================================================================================*/

Viewport* Utils::GetViewport( Viewport* pOut )
{
	glGetIntegerv( GL_VIEWPORT, reinterpret_cast< int32* >( pOut ) );
	return pOut;
}

/*==============================================================================================

MÉTODO GetRotationMatrix
	Obtém uma matriz de rotação utilizando as funcionalidades da API gráfica.

==============================================================================================*/

Matrix4x4* Utils::GetRotationMatrix( Matrix4x4* pOut, float angle, const Point3f* pAxis )
{
	return GetRotationMatrix( pOut, angle, pAxis->x, pAxis->y, pAxis->z );
}

/*==============================================================================================

MÉTODO GetRotationMatrix
	Obtém uma matriz de rotação utilizando as funcionalidades da API gráfica.

==============================================================================================*/

Matrix4x4* Utils::GetRotationMatrix( Matrix4x4* pOut, float angle, float axisX, float axisY, float axisZ )
{
	int32 currMatrixMode;
	glGetIntegerv( GL_MATRIX_MODE, &currMatrixMode );
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glRotatef( angle, axisX, axisY, axisZ );
	GetModelViewMatrix( pOut );
	glPopMatrix();
	glMatrixMode( currMatrixMode );
	return pOut;
}

/*==============================================================================================

MÉTODO FindRotationBetweenVectors
	Obtém o ângulo e o eixo de rotação entre dois vetores.

==============================================================================================*/

float Utils::FindRotationBetweenVectors( Point3f* pOut, const Point3f* pV1, const Point3f* pV2 )
{
	Point3f v1 = *pV1, v2 = *pV2;
	v1.normalize();
	v2.normalize();
	
	*pOut = ( v1 % v2 );
	pOut->normalize();

	return NanoMath::radiansToDegrees( static_cast< float >( acos( v1 * v2 ) ) );
}

/*==============================================================================================

FUNÇÃO LoadAudioFile
	Carrega um arquivo de áudio.

==============================================================================================*/

#if COMPONENTS_CONFIG_ENABLE_OPENAL

static void* LoadAudioFile( CFURLRef fileURL, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq )
{
	void* pData = NULL;
	AudioFileID	afid = 0;
	
	{  // Evita erros de compilação por causa dos gotos

		// Abre o arquivo
		OSStatus err = AudioFileOpenURL( fileURL, kAudioFileReadPermission, 0, &afid );
		if( err )
		{
			#if DEBUG
				LOG( "LoadAudioFile: AudioFileOpenURL FAILED, Error = %ld\n", err );
			#endif

			goto Exit;
		}
		
		// Obtém o formato dos dados do áudio
		AudioStreamBasicDescription fileFormat;
		UInt32 propertySize = sizeof( fileFormat );

		err = AudioFileGetProperty( afid, kAudioFilePropertyDataFormat, &propertySize, &fileFormat );
		if( err )
		{
			#if DEBUG
				LOG( "LoadAudioFile: AudioFileGetProperty( kAudioFileProperty_DataFormat ) FAILED, Error = %ld\n", err );
			#endif

			goto Exit;
		}
		
		// Verifica se o formato é suportado
		if( fileFormat.mChannelsPerFrame > 2 )
		{
			#if DEBUG
				LOG( "LoadAudioFile - Unsupported Format, channel count is greater than stereo\n" );
			#endif

			goto Exit;
		}
	
		// TODOO : Não precisamos utilizar apenas PCMs... Verificar as implicações de utilizar vários formatos...
		if(( fileFormat.mFormatID != kAudioFormatLinearPCM ) || ( !TestAudioFormatNativeEndian( fileFormat )))
		{
			#if DEBUG
				LOG( "LoadAudioFile - Unsupported Format, must be little-endian PCM\n" );
			#endif

			goto Exit;
		}
		
		if(( fileFormat.mBitsPerChannel != 8 ) && ( fileFormat.mBitsPerChannel != 16 ))
		{
			#if DEBUG
				LOG( "LoadAudioFile - Unsupported Format, must be 8 or 16 bit PCM\n" );
			#endif

			goto Exit;
		}
		
		// TODOO
		// Acha mBitsPerChannel na mão caso venha zerado
//		uint32 bitRate = 0;
//		propertySize = sizeof( bitRate );
//		err = AudioFileGetProperty( afid, kAudioFilePropertyBitRate, &propertySize, &bitRate );
//		if( err )
//		{
//			#if DEBUG
//				LOG( "LoadAudioFile: AudioFileGetProperty( kAudioFilePropertyBitRate ) FAILED, Error = %ld\n", err );
//			#endif
//
//			goto Exit;
//		}
//		uint32 mBitsPerChannel = bitRate / ( fileFormat.mChannelsPerFrame * fileFormat.mSampleRate );

		// Obtém o tamanho total do arquivo
		uint64 fileDataSize = 0;
		propertySize = sizeof( fileDataSize );

		err = AudioFileGetProperty( afid, kAudioFilePropertyAudioDataByteCount, &propertySize, &fileDataSize );
		if( err )
		{
			#if DEBUG
				LOG( "LoadAudioFile: AudioFileGetProperty( kAudioFilePropertyAudioDataByteCount ) FAILED, Error = %ld\n", err );
			#endif

			goto Exit;
		}
	
		// Lê todos os dados para a memória
		UInt32 dataSize = fileDataSize;
		pData = malloc( dataSize );
		if( pData )
		{
			err = AudioFileReadBytes( afid, false, 0, &dataSize, pData );
			if( err == noErr )
			{
				if( pSize )
					*pSize = ( ALsizei )dataSize;
				
				if( pFormat )
					*pFormat = ( fileFormat.mChannelsPerFrame > 1 ) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
				
				if( pFreq )
					*pFreq = ( ALsizei )fileFormat.mSampleRate;
			}
			else 
			{
				free( pData );
				pData = NULL;
				
				#if DEBUG
					LOG( "LoadAudioFile: AudioFileReadBytes FAILED, Error = %ld\n", err );
				#endif

				goto Exit;
			}	
		}
	
	} // Evita erros de compilação por causa dos gotos
	
	Exit:
		if( afid )
			AudioFileClose( afid );

		return pData;
}

#endif

/*==============================================================================================

MÉTODO GetAudioFile
	Carrega um arquivo de áudio.

==============================================================================================*/

#if COMPONENTS_CONFIG_ENABLE_OPENAL

void* Utils::GetAudioFile( const char* pFileName, const char* pFileType, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq )
{
	// Obtém a path atual do arquivo de áudio que está contido no pacote da aplicação
	NSString* filePath = GetPathForResource( pFileName, pFileType );
	
	// Retorna o arquivo carregado
	return LoadAudioFile( ( CFURLRef )[NSURL fileURLWithPath: filePath], pSize, pFormat, pFreq );
}

#endif

/*==============================================================================================

MÉTODO GetAudioSource
	Carrega uma fonte de áudio contendo o arquivo passado como parâmetro.

==============================================================================================*/

#if COMPONENTS_CONFIG_ENABLE_OPENAL

AudioSource* Utils::GetAudioSource( AudioContext* pContext, const char* pFileName, const char* pFileType )
{	
	// Se o contexto passado como parâmetro não for o contexto atual, não cria a fonte de som,
	// pois pSource->insertAudioBuffer() retornaria erro ( AL_INVALID_VALUE ). Não torna pContext
	// o contexto atual porque não sabemos se tal operação acarretará em erros para o usuário
	// deste método
	if( !pContext->isCurrentContext() )
		return NULL;

	ALenum format;
	ALsizei size, freq;
	AudioSource* pSource = NULL;
	
	void* pSound = Utils::GetAudioFile( pFileName, pFileType, &size, &format, &freq );
	if( pSound == NULL )
		return pSource;

	{ // Evita problemas com os gotos

		pSource = pContext->newAudioSource();
		if( !pSource )
			goto Exit;

		AudioBufferAL* pBuffer = new AudioBufferAL();
		if( !pBuffer || !pBuffer->fillBuffer( pSound, size, format, freq ) || !pSource->insertAudioBuffer( pBuffer ) )
		{
			SAFE_DELETE( pBuffer );
			DELETE( pSource );
			goto Exit;
		}
		
	} // Evita problemas com os gotos
	
	Exit:
		// AudioBuffer guarda uma cópia do aúdio, logo podemos liberar a memória alocada pelo método
		// GetAudioFile()
		free( pSound );
		pSound = NULL;
	
		// Retorna a fonte de áudio
		return pSource;
}

#endif

/*==============================================================================================

MÉTODO GetPathForResource
	Retorna a path do recurso contido no pacote da aplicação.

==============================================================================================*/

char* Utils::GetPathForResource( char* pBuffer, int32 bufferSize, const char* pResourceName, const char* pResourceType )
{
	snprintf( pBuffer, bufferSize, "%s", NSSTRING_TO_CHAR_ARRAY( [[NSBundle mainBundle] pathForResource: CHAR_ARRAY_TO_NSSTRING( pResourceName ) ofType: CHAR_ARRAY_TO_NSSTRING( pResourceType )] ) );
	return pBuffer;
}

/*==============================================================================================

MÉTODO GetURLForResource
	Retorna a URL do recurso contido no pacote da aplicação.

==============================================================================================*/

CFURLRef Utils::GetURLForResource( const char* pResourceName, const char* pResourceType )
{
	char buffer[ PATH_MAX ];
	return ( CFURLRef )[NSURL fileURLWithPath: CHAR_ARRAY_TO_NSSTRING( GetPathForResource( buffer, PATH_MAX, pResourceName, pResourceType ))];
}

/*==============================================================================================

MÉTODO MapPointByOrientation
	Mapeia o ponto de acordo com a orientação do device.

==============================================================================================*/

Point3f Utils::MapPointByOrientation( const Point3f* pPoint )
{
	Point3f ret;
	switch( [[ApplicationManager GetInstance] getOrientation] )
	{
		// TODOO : Não foi testado
		case UIInterfaceOrientationPortraitUpsideDown:
			ret.x = SCREEN_WIDTH - pPoint->x - 1.0f;
			ret.y = SCREEN_HEIGHT - pPoint->y - 1.0f;
			break;

		case UIInterfaceOrientationLandscapeRight:
			ret.x = pPoint->y;
			ret.y = SCREEN_HEIGHT - pPoint->x - 1.0f;
			break;

		// TODOO : Não foi testado
		case UIInterfaceOrientationLandscapeLeft:
			ret.x = SCREEN_WIDTH - pPoint->y - 1.0f;
			ret.y = pPoint->x;
			break;
		
		//case UIInterfaceOrientationPortrait:
		default:
			ret = *pPoint;
			break;
	}
	return ret;
}

/*==============================================================================================

MÉTODO isPointInsideRect
	Retorna se o ponto está dentro do retângulo.

==============================================================================================*/

bool Utils::IsPointInsideRect( const Point3f* pPoint, float x, float y, float width, float height )
{
	return NanoMath::fgeq( pPoint->x, x )
		   && NanoMath::fleq( pPoint->x, x + width )
		   && NanoMath::fgeq( pPoint->y, y )
           && NanoMath::fleq( pPoint->y, y + height );
}

/*==============================================================================================

MÉTODO isPointInsideRect
	Retorna se o ponto está dentro do retângulo.

==============================================================================================*/

bool Utils::IsPointInsideRect( const Point3f* pPoint, const Object* pObj )
{
	Point3f objPos = *( pObj->getPosition() );
	return IsPointInsideRect( pPoint, objPos.x, objPos.y, pObj->getWidth(), pObj->getHeight() );
}

/*==============================================================================================

MÉTODO GetPointSizesInfo
	Retorna o tamanho mínimo e o tamanho máximo que o hardware suporta para um ponto.

==============================================================================================*/

void Utils::GetPointSizesInfo( float* pMinPointSize, float* pMaxPointSize )
{ 
	float range[2]; 
	glGetFloatv( GL_ALIASED_POINT_SIZE_RANGE, range ); 
	
	if( pMinPointSize )
		*pMinPointSize = range[0]; 
	
	if( pMaxPointSize )
		*pMaxPointSize = range[1]; 
}

/*==================================================================================

MÉTODO GetLineIntersection2D
	Supondo um segmento de reta entre os pontos K e L e outro entre os pontos M e N,
esta função determina os valores dos parâmentros S (da reta KL) e T(da reta MN) no
ponto de intersecção das retas-suporte dos segmentos passados como parâmetro.
	Se S e T retornados pela função estiverem entre 0 e 1, então a interseção dos
segmentos existe.
	s: Valor do parâmetro no ponto de interseção (sobre a reta KL)
	t: Valor do parâmetro no ponto de interseção (sobre a reta MN) 

===================================================================================*/

bool Utils::GetLineIntersection2D( float& s, float& t, const Line& kl, const Line& mn )
{
	float det = ( mn.p1.x - mn.p0.x ) * ( kl.p1.y - kl.p0.y)  -  ( mn.p1.y - mn.p0.y ) * ( kl.p1.x - kl.p0.x );

	if( NanoMath::feql( det, 0.0f ) )
		return false;

	s = ( ( mn.p1.x - mn.p0.x ) * ( mn.p0.y - kl.p0.y ) - ( mn.p1.y - mn.p0.y ) * ( mn.p0.x - kl.p0.x ) ) / det;
	t = ( ( kl.p1.x - kl.p0.x ) * ( mn.p0.y - kl.p0.y ) - ( kl.p1.y - kl.p0.y ) * ( mn.p0.x - kl.p0.x ) ) / det;

	return true;
}

/*==================================================================================

MÉTODO GetFreeMemory
	Retorna a quantidade de memória livre.

===================================================================================*/

float Utils::GetFreeMemory( void )
{
	size_t length;
    int mib[6];
	
	int pagesize;
    mib[0] = CTL_HW;
    mib[1] = HW_PAGESIZE;
    length = sizeof( pagesize );
    sysctl( mib, 2, &pagesize, &length, NULL, 0 );
   
    mach_msg_type_number_t count = HOST_VM_INFO_COUNT;
   
    vm_statistics_data_t vmstat;
    host_statistics( mach_host_self(), HOST_VM_INFO, ( host_info_t )&vmstat, &count );
	
	// OBS: Outros dados disponíveis
//	double total = vmstat.wire_count + vmstat.active_count + vmstat.inactive_count + vmstat.free_count;
//	double free = vmstat.free_count / total;
//
//	float percentFree = free * 100.0f;
//  float available = (( float )(( vmstat.wire_count + vmstat.active_count + vmstat.inactive_count + vmstat.free_count) * pagesize)) / 0x100000;	
//  float remaining = static_cast< float >( vmstat.free_count * pagesize ) / 0x100000;

	// Retorna 'remaining'
	return static_cast< float >( vmstat.free_count * pagesize ) / 0x100000;
}

/*==================================================================================

MÉTODO CheckRectCollision
	Faz o teste de colisão entre dois quads.

===================================================================================*/

bool Utils::CheckRectCollision( const NanoRect *pRect1, const NanoRect *pRect2 )
{
	if( pRect1->x + pRect1->width < pRect2->x )
		return false;
	
	if( pRect1->x > pRect2->x + pRect2->width )
		return false;

	if( pRect1->y + pRect1->height < pRect2->y )
		return false;
	
	if( pRect1->y > pRect2->y + pRect2->height )
		return false;
	
	return true;
}

/*==================================================================================

MÉTODO IsLeapYear

	 Verifica se o ano é bissexto. Para isso, utilizamos as regras:

		 1. De 4 em 4 anos é ano bissexto.
		 2. De 100 em 100 anos não é ano bissexto.
		 3. De 400 em 400 anos é ano bissexto.
		 4. Prevalece as últimas regras sobre as primeiras.

	 Para melhor entender

		* São bissextos todos os anos múltiplos de 400, p.ex: 1600, 2000, 2400, 2800
		* Não são bissextos todos os múltiplos de 100 e não de 400, p.ex: 1700, 1800, 1900, 2100, 2200, 2300, 2500...
		* São bissextos todos os múltiplos de 4 e não múltiplos de 100, p.ex: 1996, 2004, 2008, 2012, 2016...
		* Não são bissextos todos os demais anos.

===================================================================================*/

bool Utils::IsLeapYear( uint32 year )
{
	if( year % 100 == 0 )
		year /= 100;
	
	// &3 == %4
	return ( year & 3 ) == 0;
}

/*==================================================================================

MÉTODO Today
	Retorna o dia atual.

===================================================================================*/

void Utils::Today( uint8 *pDay, uint8 *pMonth, uint32 *pYear )
{
	NSCalendar *hCalendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
	NSDateComponents *hDateComps = [hCalendar components: NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate: [NSDate date]];
	[hCalendar release];

	if( pDay )
		*pDay = [hDateComps day];
	
	if( pMonth )
		*pMonth = [hDateComps month];
	
	if( pYear )
		*pYear = [hDateComps year];
}

/*==================================================================================

MÉTODO StringToWString
	Converte uma std::string em uma std::wstring.

===================================================================================*/

std::wstring& Utils::StringToWString( const std::string& from, std::wstring& to )
{
	std::wstring temp( from.length(), L' ' );
	std::copy( from.begin(), from.end(), temp.begin() );
	
	to = temp;
	return to;
}

/*==================================================================================

MÉTODO WStringToString
	Converte uma std::wstring em uma std::string.

===================================================================================*/

std::string& Utils::WStringToString( const std::wstring& from, std::string& to )
{
	std::string temp( from.length(), ' ' );
	std::copy( from.begin(), from.end(), temp.begin() );
	
	to = temp;
	return to;
}

/*==================================================================================

MÉTODO CompareVersions
	Compara duas versões no formato "xy.zw.tu", sendo que pode haver 'n' caracteres
'.', separados por 'm' números. A versão "523.8.345.14223", pore exemplo, é válida,
desde que a versão a ser comparada possua o formato "abc.d.efg.hijk".
	Este método retorna:
		- < 0, caso 'currVersion' seja menor que 'versionToTestAgainst'
		- = 0, caso 'currVersion' e 'versionToTestAgainst' sejam iguais
		- > 0, caso 'currVersion' seja maior que 'versionToTestAgainst'

===================================================================================*/

int32 Utils::CompareVersions( const std::string currVersion, const std::string versionToTestAgainst )
{
	std::string s1( currVersion );
	std::string s2( versionToTestAgainst );
	
	// Retira os pontos
	std::string::size_type found = std::string::npos;
	while( ( found = s1.find_first_of( "." ) ) != std::string::npos )
	{
		std::string::iterator it = s1.begin();
		std::advance( it, found );
		s1.erase( it );
	}
	
	found = std::string::npos;
	while( ( found = s2.find_first_of( "." ) ) != std::string::npos )
	{
		std::string::iterator it = s2.begin();
		std::advance( it, found );
		s2.erase( it );
	}
	
	size_t s1Len = s1.length();
	size_t s2Len = s2.length();
	if( s1Len < s2Len )
	{
		s1.append( s2Len - s1Len, '0' );
	}
	else if( s1Len > s2Len )
	{
		s2.append( s1Len - s2Len, '0' );
	}
	
	return atoi( s1.c_str() ) - atoi( s2.c_str() );
}

/*==================================================================================

MÉTODO ExplodeHierarchy
	Imprime como é montada a hierarquia de views de uma view.

===================================================================================*/

#if DEBUG

#define outstring(anObject) [[anObject description] UTF8String] 

void Utils::ExplodeHierarchy( UIView* hView, int32 level )
{ 
	// Indent to show the current level 
	for( int32 i = 0;  i < level; ++i )
		printf( "—" );

	// Show the class and superclass for the current object 
	printf( "%s:%s\n", outstring( [hView class] ), outstring( [hView superclass] ) ); 

	// Recurse for all subviews 
	for( UIView *hSubview in [hView subviews] ) 
		ExplodeHierarchy( hSubview, level + 1 ); 
}

#endif

/*==============================================================================================
 
 FUNÇÃO GetText
 
 ==============================================================================================*/

#define GET_TXT_BUFFER_SIZE 512

void Utils::GetComponentsText( uint16 textIndex, std::string& outText )
{
	char buffer[ GET_TXT_BUFFER_SIZE ];
	snprintf( buffer, GET_TXT_BUFFER_SIZE, "%d", static_cast< int32 >( textIndex ) );
	
	CFStringRef hTemp = CFStringCreateWithCString( NULL, buffer, kCFStringEncodingUTF8 );
	
	CFStringRef hLocalizedStr = CFCopyLocalizedStringFromTable( hTemp, CFSTR( "comp" ), NULL );
	if( hLocalizedStr )
	{
		CFStringGetCString( hLocalizedStr, buffer, GET_TXT_BUFFER_SIZE, kCFStringEncodingUTF8 );
		outText.append( buffer );
		
		CFRelease( hLocalizedStr );
	}
	
	CFRelease( hTemp );
}

#undef GET_TXT_BUFFER_SIZE