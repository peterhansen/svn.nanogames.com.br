/*
 *  NanoRect.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 5/11/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_RECT_H
#define NANO_RECT_H 1

struct NanoRect
{
	float x, y, width, height;
	
	NanoRect( void ) : x( 0.0f ), y( 0.0f ), width( 0.0f ), height( 0.0f )
	{
	};
	
	NanoRect( float x, float y, float width, float height ) : x( x ), y( y ), width( width ), height( height )
	{
	};
	
	void set( float x = 0.0f, float y = 0.0f, float width = 0.0f, float height = 0.0f )
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	};
};

#endif
