#include "Viewport.h"

// Components
#include "ApplicationManager.h"
#include "ObjcMacros.h"

// OpenGL
#include "GLHeaders.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Viewport::Viewport( void ) : x( 0 ), y( 0 ), width( 0 ), height( 0 )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Viewport::Viewport( int32 x, int32 y, int32 width, int32 height ) : x( x ), y( y ), width( width ), height( height )
{
}

/*==============================================================================================

MÉTODO apply
	Aplica o viewport levando em consideração a orientação do device.

==============================================================================================*/

void Viewport::apply( void ) const
{
	switch( [APP_DELEGATE getOrientation] )
	{
		case UIInterfaceOrientationPortrait:
			glScissor( x, SCREEN_HEIGHT - 1 - y - height, width, height );
			break;

		case UIInterfaceOrientationPortraitUpsideDown:
			#if DEBUG
				assert_n_log( false, "ERROR in Viewport::apply( void ) => Orientation UIInterfaceOrientationPortraitUpsideDown not implemented" );
			#endif
			break;

		case UIInterfaceOrientationLandscapeLeft:
			#if DEBUG
				assert_n_log( false, "ERROR in Viewport::apply( void ) => Orientation UIInterfaceOrientationLandscapeLeft not implemented" );
			#endif
			break;

		case UIInterfaceOrientationLandscapeRight:
			glScissor( SCREEN_HEIGHT - 1 - y - height, SCREEN_WIDTH - 1 - x - width, height, width );
			break;
	}
}

/*==============================================================================================

MÉTODO getIntersection
	Retorna a interseção deste viewport com o viewport passado como parâmetro.

==============================================================================================*/

Viewport& Viewport::getIntersection( Viewport& ret, const Viewport& other ) const
{
	ret.x = this->x > other.x ? this->x : other.x;
	ret.y = this->y > other.y ? this->y : other.y;
	
	ret.width = this->width < other.width ? this->width : other.width;
	ret.height = this->height < other.height ? this->height : other.height;

   return ret;
}

/*==============================================================================================

MÉTODO getIntersection
	Inicializa o objeto.

==============================================================================================*/

void Viewport::set( int32 x, int32 y, int32 width, int32 height )
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}

/*==============================================================================================

OPERATOR ==

==============================================================================================*/

bool Viewport::operator == ( const Viewport& other ) const
{
	return ( x == other.x ) && ( y == other.y ) && ( width == other.width ) && ( height == other.height );
}

/*==============================================================================================

OPERATOR !=

==============================================================================================*/

bool Viewport::operator != ( const Viewport& other ) const
{
	return ( x != other.x ) || ( y != other.y ) || ( width != other.width ) || ( height != other.height );
}

