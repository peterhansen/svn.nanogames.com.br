/*
 *  MathFuncs.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 5/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef MATH_FUNCS_H
#define MATH_FUNCS_H 1

#include <cmath>
#include "NanoTypes.h"

// Variação mínima para considerarmos diferentes 2 números em ponto flutuante
#define DEFAULT_FLOAT_EPSILON 0.00001f

// M_PI * 2
#define TWO_PI 6.283185307179587f

namespace NanoMath
{
	// Retorna o primeiro múltiplo de m no intervalo [n, MAX_INT]
	int32 nextMulOf( int32 n, int32 m );

	// Converte graus em radianos
	inline float degreesToRadians( float degrees ){ return static_cast< float >( ( degrees * M_PI ) / 180.0f ); };
	
	// Converte radianos em graus
	inline float radiansToDegrees( float radians ){ return static_cast< float >( ( 180.0f * radians ) / M_PI ); };

	// TODOO : Pode virar template
	// Restringe o número x ao intervalo [min, max]
	inline float clamp( float x, float min, float max ){ return x <= min ? min : ( x >= max ? max : x ); };

	// TODOO : Pode virar template
	// Interpolação linear
	inline float lerp( float percent, float x, float y ){ return x + ( percent * ( y - x ) ); };
	
	// Comparação de números em ponto flutuante
	// Para uma comparação aprimorada, ver:
	// - http://www.cygnus-software.com/papers/comparingfloats/Comparing%20floating%20point%20numbers.htm
	inline int8 fcmp( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fabs( f1 - f2 ) <= epsilon ? 0 : ( f1 > f2 ? 1 : -1 ); };

	// ==
	inline bool feql( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fcmp( f1, f2, epsilon ) == 0; };

	// !=
	inline bool fdif( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fcmp( f1, f2, epsilon ) != 0; };

	// <
	inline bool fltn( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fcmp( f1, f2, epsilon ) == -1; };

	// >
	inline bool fgtn( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fcmp( f1, f2, epsilon ) == 1; };

	// <=
	inline bool fleq( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fcmp( f1, f2, epsilon ) <= 0; };

	// >=
	inline bool fgeq( float f1, float f2, float epsilon = DEFAULT_FLOAT_EPSILON ){ return fcmp( f1, f2, epsilon ) >= 0; };
}

#endif
