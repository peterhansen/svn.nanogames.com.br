#include "Bitwise.h"

/*==============================================================================================

MÉTODO getNextPowerOf2
	Retorna a próxima potência de 2 em relação a n.

==============================================================================================*/

int32 Bitwise::getNextPowerOf2( int32 n )
{
	--n;
	n |= n >> 1;
	n |= n >> 2;
	n |= n >> 4;
	n |= n >> 8;
	n |= n >> 16;
	return ++n;
}
