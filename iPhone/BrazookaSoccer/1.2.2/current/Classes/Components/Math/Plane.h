/*
 *  Plane.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 3/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PLANE_H
#define PLANE_H 1

#include <cstdio> // Definição de NULL

#include "Point3f.h"

class Plane
{
	public:
		// Vetor apontando para a "direita" do plano
		Point3f	right;	
	
		// Vetor apontando para o "topo" do plano
		Point3f	top;
	
		// Normal do plano
		Point3f	normal;

		// Deslocamento
		float d;

		// Construtores
		Plane( void );
		Plane( const Point3f* pTop, const Point3f* pRight, float d = 0.0f );
	
		// Inicializa o objeto
		void set( const Point3f* pTop = NULL, const Point3f* pRight = NULL, float d = 0.0f );
	
		// Operadores de comparação
		bool operator == ( const Plane& p ) const;
		bool operator != ( const Plane& p ) const;
};

#endif
