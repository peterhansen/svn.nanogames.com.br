#include "UICustomSwitch.h"

// Components
#include "DeviceInterface.h"
#include "Utils.h"

#define DEFAULT_SYSTEM_FONT_SIZE 17.0f

#define DEFAULT_LABEL_WIDTH 50.0f
#define DEFAULT_LABEL_HEIGHT 20.0f

// Add the undocumented alternate colors 
@interface UISwitch( Extended )
	-( void )setAlternateColors:( BOOL ) boolean; 
@end

// Expose the _UISwitchSlider class 
@interface _UISwitchSlider : UIView 
@end 

@interface UICustomSwitch( Private )
	-( bool )buildUICustomSwitch;
	-( _UISwitchSlider* )slider;
	-( UIView* )textHolder;
	-( UILabel* )leftLabel;
	-( UILabel* )rightLabel;
@end

@implementation UICustomSwitch

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildUICustomSwitch] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildUICustomSwitch] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

-( bool )buildUICustomSwitch
{
	UIView *hTextHolder = [self textHolder];
	hLeftLabel = [[hTextHolder subviews] objectAtIndex: 0];
	hRightLabel = [[hTextHolder subviews] objectAtIndex: 1]; 

	// Verifica se realmente são labels
	// OBS: Devemos fazer isso já que quando o idioma do device não é inglês,
	// os textos default 'ON" / "OFF" passam a ser imagens (UIImageViews) de 0 e 1.
	if( ![hLeftLabel isKindOfClass: [UILabel class]] )
	{
		UIImageView* hLeftImage = ( UIImageView* )hLeftLabel;
        hLeftImage.image = nil;
        hLeftImage.frame = CGRectMake( 0.0f, 0.0f, 0.0f, 0.0f );
		
		UIImageView* hRightImage = ( UIImageView* )hRightLabel;
        hRightImage.image = nil;
        hRightImage.frame = CGRectMake( 0.0f, 0.0f, 0.0f, 0.0f );
		
		std::string currOsVersion;
		DeviceInterface::GetDeviceSystemVersion( currOsVersion );
		bool olderThan30 = Utils::CompareVersions( currOsVersion, "3.0" ) < 0;

		// Cria labels
		CGRect leftRect;
		UILabel *hAuxLabelLeft;
		
		if( !olderThan30 )
		{
#if TARGET_IPHONE_SIMULATOR
			leftRect =CGRectMake( -25.0f, -10.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT );
#else
			leftRect =CGRectMake( -45.0f, 3.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT );
#endif
		}
		else
		{
#if TARGET_IPHONE_SIMULATOR
			leftRect=CGRectMake( -25.0f, -10.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT );
#else
			leftRect=CGRectMake( -35.0f, 15.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT );
#endif
		}
		
		hAuxLabelLeft = [[UILabel alloc] initWithFrame: leftRect];
		
		if( !hAuxLabelLeft )
			return false;
		[hLeftImage addSubview: hAuxLabelLeft];		
		[hAuxLabelLeft setTextAlignment: UITextAlignmentCenter];
		[hAuxLabelLeft setTextColor: [UIColor whiteColor]];
		[hAuxLabelLeft setFont: [UIFont boldSystemFontOfSize: DEFAULT_SYSTEM_FONT_SIZE]];
		[hAuxLabelLeft setBackgroundColor: [UIColor clearColor]];
		
		UILabel *hAuxLabelRight;
		if( !olderThan30 )
		{
#if TARGET_IPHONE_SIMULATOR
			hAuxLabelRight = [[UILabel alloc] initWithFrame: CGRectMake( -25.0f, -10.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT )];
#else
			hAuxLabelRight = [[UILabel alloc] initWithFrame: CGRectMake( -5.0f, 3.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT )];
#endif
		}
		else
		{
#if TARGET_IPHONE_SIMULATOR
			hAuxLabelRight = [[UILabel alloc] initWithFrame: CGRectMake( -25.0f, -10.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT )];
#else
			hAuxLabelRight = [[UILabel alloc] initWithFrame: CGRectMake( -20.0f , -10.0f, DEFAULT_LABEL_WIDTH, DEFAULT_LABEL_HEIGHT )];
#endif
		}

		if( !hAuxLabelRight )
			return false;
		[hRightImage addSubview: hAuxLabelRight];
		[hAuxLabelRight setFont: [UIFont boldSystemFontOfSize: DEFAULT_SYSTEM_FONT_SIZE]];
		[hAuxLabelRight setTextAlignment: UITextAlignmentCenter];
		[hAuxLabelRight setBackgroundColor: [UIColor clearColor]];
		
		hLeftLabel = hAuxLabelLeft;
		hRightLabel = hAuxLabelRight;
	}
	
	
	return true;
}

-( _UISwitchSlider* )slider
{ 
	return [[self subviews] lastObject]; 
}

-( UIView* )textHolder
{ 
	return [[[self slider] subviews] objectAtIndex:2]; 
}

-( UILabel* )leftLabel
{
	return hLeftLabel;
}

-( UILabel* )rightLabel
{ 
	return hRightLabel; 
}

-( void )setLeftLabelText:( NSString* )labelText
{
	[hLeftLabel setText: labelText];
}

-( void )setRightLabelText:( NSString* )labelText
{
	[hRightLabel setText: labelText];
}

@end 
