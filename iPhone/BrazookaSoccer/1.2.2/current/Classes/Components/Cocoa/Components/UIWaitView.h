/*
 *  UIWaitView.h
 *  NetworkTest
 *
 *  Created by Daniel Lopes Alves on 11/18/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UIWAIT_VIEW_H
#define UIWAIT_VIEW_H

@interface UIWaitView : UIView
{
	@private
		UILabel *hLbWaitMsg;
		UIActivityIndicatorView *hActivityIndicator;
}

// Métodos para manipular o texto da view
-( void )setFont:( UIFont* )hFont;

-( void )setText:( NSString* )hText;
-( void )setTextColor:( UIColor* )hColor;

-( void )setTextShadowColor:( UIColor* )hColor;
-( void )setTextShadowOffset:( CGSize )offset;

// Métodos para exibir / esconder a view de espera
-( void )show;
-( void )hide:( bool )destroy;

@end

#endif
