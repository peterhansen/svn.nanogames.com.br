#include "UIImgProgressBar.h"

// Components
#include "ObjcMacros.h"
#include "MathFuncs.h"

// Definições auxiliares
#define PROGRESS_BAR_DEFAULT_WIDTH 15.0f

// Extensão da classe para declarar métodos privados
@interface UIImgProgressBar ( Private )

// Inicializa a view
-( bool )buildWithBkgImg:( const std::string& )bkgImgPath AndFillImg:( const std::string& )fillImgPath LeftCapWidth:( uint16 )leftCapWidth TopCapHeight:( uint16 )topCapHeight;

//// Libera a memória alocada pelo objeto
//-( void )clean;

// Cria uma imagem para utilizarmos nos controles de tamanho variável
-( UIImageView* )createStretchableImageWithPath:( const std::string& )path LeftCapWidth:( uint16 )leftCapWidth TopCapHeight:( uint16 )topCapHeight;

@end

// Início da implementação da classe
@implementation UIImgProgressBar

/*==============================================================================================

MENSAGEM initWithBkgImg:FillImg:LeftCapWidth:TopCapHeight:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithBkgImg:( const std::string& )bkgImgPath FillImg:( const std::string& )fillImgPath LeftCapWidth:( uint16 )leftCapWidth TopCapHeight:( uint16 )topCapHeight;
{
	// Passa um frame qualquer. A inicialização correta será feita em buildWithBkgImg:AndFillImg:LeftCapWidth:TopCapHeight:
    if( ( self = [super initWithFrame: CGRectMake( 0.0f, 0.0f, 1.0f, 1.0f )] ) )
	{
		if( ![self buildWithBkgImg: bkgImgPath AndFillImg: fillImgPath LeftCapWidth: leftCapWidth TopCapHeight: topCapHeight] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM buildWithBkgImg:AndFillImg:LeftCapWidth:TopCapHeight:
	Inicializa a view.

==============================================================================================*/

- ( bool )buildWithBkgImg:( const std::string& )bkgImgPath AndFillImg:( const std::string& )fillImgPath LeftCapWidth:( uint16 )leftCapWidth TopCapHeight:( uint16 )topCapHeight
{
	hImgBkg = nil;
	hImgFill = nil;
	currProgress = 0.0f;
	
	{ // Evita erros de compilação por causa dos gotos

		// Aloca as imagens
		hImgBkg = [self createStretchableImageWithPath: bkgImgPath LeftCapWidth: leftCapWidth TopCapHeight: topCapHeight];
		if( hImgBkg == nil )
			goto Error;
		
		[self addSubview: hImgBkg];
		[hImgBkg release];
			
		hImgFill = [self createStretchableImageWithPath: fillImgPath LeftCapWidth: leftCapWidth TopCapHeight: topCapHeight];
		if( hImgFill == nil )
			goto Error;
		
		[self addSubview: hImgFill];
		[hImgFill release];

		// Configura as imagens
		CGRect aux = hImgBkg.frame;
		[super setFrame: CGRectMake( 0.0f, 0.0f, aux.size.width + PROGRESS_BAR_DEFAULT_WIDTH, aux.size.height )];
		[self setFrame: self.frame];
		[self setOpaque: NO];

		return true;
	
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
//		[self clean];
		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self clean];
//  [super dealloc];
//}

/*==============================================================================================

MENSAGEM clean
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )clean
//{
//}

/*==============================================================================================

MENSAGEM setProgress:
	Determina o progresso que a barra deve indicar. Coloca todos os valores no intervalo
[0.0f, 1.0f].

===============================================================================================*/

-( void )setProgress:( float )progress
{
	currProgress = NanoMath::clamp( progress, 0.0f, 1.0f );
	
	CGRect barFrame = [self frame];
	[hImgFill setFrame: CGRectMake( 0.0f, 0.0f, barFrame.size.width * currProgress, barFrame.size.height )];
}

/*==============================================================================================

MENSAGEM setFrame:
	Ver propriedade 'frame' de UIView.

===============================================================================================*/

-( void )setFrame:( CGRect )frame
{
	[super setFrame: frame];
	
	if( ( hImgBkg != nil ) && ( hImgFill != nil ) )
	{
		CGSize barSize = [self frame].size;
		CGRect barFrame = CGRectMake( 0.0f, 0.0f, barSize.width, barSize.height );
		[hImgBkg setFrame: barFrame];
		
		[self setProgress: currProgress];
	}
}

/*==============================================================================================

MENSAGEM createStretchableImageWithPath:
	Cria uma imagem para utilizarmos nos controles de tamanho variável.
 
	OBS: Os métodos utilizados para alocar as UIImages não chamam retain, por isso nunca
chamamos [hImgAux release].

===============================================================================================*/

-( UIImageView* )createStretchableImageWithPath:( const std::string& )path LeftCapWidth:( uint16 )leftCapWidth TopCapHeight:( uint16 )topCapHeight
{
	UIImageView *hImgView = nil;

	char buffer[ PATH_MAX ];
	UIImage *hImgAux = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, path.c_str(), "png" ))];

	if( hImgAux != nil )
	{
		hImgAux = [hImgAux stretchableImageWithLeftCapWidth: leftCapWidth topCapHeight: topCapHeight];
		if( hImgAux != nil )
			hImgView = [[UIImageView alloc] initWithImage: hImgAux];
	}

	return hImgView;
}

// Fim da implementação da classe
@end

