//
//  SplashNano.h
//  Components
//
//  Created by Daniel Lopes Alves on 12/18/08.
//  Copyright 2008 Nano Games. All rights reserved.
//

#ifndef SPLASH_NANO_H
#define SPLASH_NANO_H 1

#include "BasicView.h"

@interface SplashNano : BasicView
{	
	@private	
	IBOutlet UILabel *hCopyrightNoticeLabel;

}
- ( void )awakeFromNib;
@end

#endif
