//
//  ConfimLoadView.h
//  FreeKick
//
//  Created by Daniel Monteiro on 10/2/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ConfimLoadView : UIView {

}
- (IBAction) AnswerYes;
- (IBAction) AnswerNo;
@end
