#include "AccelerometerInterface.h"
#include "Point3f.h"

#if DEBUG
	#include "Macros.h"
#endif

// Extensão da classe para declarar métodos privados
@interface AccelerometerInterface ()

// Verifica se a versão do sistema operacional é mais antiga que iPhoneOS 3.0
- ( bool )isOsVersionOlderThan3_0;

@end

// Início da implementação da classe
@implementation AccelerometerInterface

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize pListener;

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

-( id ) init
{
	if( ( self = [super init] ) )
	{
		// Inicializa o objeto
		pListener = NULL;
		pActiveListener = NULL;
		wasActiveBeforeSuspend = false;

		osVersionOlderThan3_0 = [self isOsVersionOlderThan3_0];
		
		if( osVersionOlderThan3_0 )
			[[UIAccelerometer sharedAccelerometer] setDelegate: self];
	}
	return self;
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

- ( void )dealloc
{
	// No destrutor, queremos garantir que chamaremos [[UIAccelerometer sharedAccelerometer] setDelegate: nil]
	// em [self stopListening], por isso setamos osVersionOlderThan3_0 para false
	osVersionOlderThan3_0 = false;

	[self stopSendingEvents];

	pListener = NULL;

    [super dealloc];
}

/*===========================================================================================
 
MENSAGEM setUpdateInterval
	Determina a frequência com que o usuário deseja receber os eventos do acelerômetro.
 
============================================================================================*/

- ( void ) setUpdateInterval:( float )secs
{
	if( [NSThread isMainThread] )
		[[UIAccelerometer sharedAccelerometer] setUpdateInterval: secs];
#if DEBUG
	else
		LOG( "Não é possível alterar o estado dos acelerômetros numa thread diferente da principal" );
#endif
}

/*===========================================================================================
 
MENSAGEM startSendingEvents
	Começa a receber os eventos do acelerômetro.
 
============================================================================================*/

- ( void ) startSendingEvents
{
	if( [NSThread isMainThread] )
	{
		pActiveListener = pListener;

		if( !osVersionOlderThan3_0 )
			[[UIAccelerometer sharedAccelerometer] setDelegate: self];
	}
#if DEBUG
	else
		LOG( "Não é possível alterar o estado dos acelerômetros numa thread diferente da principal" );
#endif
}

/*===========================================================================================
 
MENSAGEM stopSendingEvents
	Pára de receber os eventos do acelerômetro.
 
============================================================================================*/

- ( void ) stopSendingEvents
{
	if( [NSThread isMainThread] )
	{
		pActiveListener = NULL;

		if( !osVersionOlderThan3_0 )
			[[UIAccelerometer sharedAccelerometer] setDelegate: nil];
	}
#if DEBUG
	else
		LOG( "Não é possível alterar o estado dos acelerômetros numa thread diferente da principal" );
#endif
}

/*===========================================================================================
 
MENSAGEM isSendingEvents
	Indica se está enviando eventos para o listener.
 
============================================================================================*/

- ( bool ) isSendingEvents
{
	return pActiveListener != NULL;
}

/*==============================================================================================

MENSAGEM suspend
	Pára o processamento dos acelerômetros.

==============================================================================================*/

- ( void ) suspend
{
	wasActiveBeforeSuspend = [self isSendingEvents];
	
	if( wasActiveBeforeSuspend )
		[self stopSendingEvents];
}

/*==============================================================================================

MENSAGEM resume
	Reinicia o processamento dos acelerômetros.

==============================================================================================*/

- ( void ) resume
{
	if( wasActiveBeforeSuspend )
		[self startSendingEvents];
}

/*===========================================================================================
 
MENSAGEM accelerometer: didAccelerate:
	Implementação do protocolo UIAccelerometerDelegate.
 
============================================================================================*/		

- ( void ) accelerometer:( UIAccelerometer* )accelerometer didAccelerate:( UIAcceleration* )acceleration
{
	if( pActiveListener != NULL )
	{
		Point3f aux( static_cast< float >( [acceleration x] ), static_cast< float >( [acceleration y] ), static_cast< float >( [acceleration z] ) );
		pActiveListener->setAccelerometerEvent( &aux );
	}
}

/*==============================================================================================

MENSAGEM isOsVersionOlderThan3_0
	Verifica se a versão do sistema operacional é mais antiga que iPhoneOS 3.0.

==============================================================================================*/

#define N_OLDER_OS_VERSIONS 4

- ( bool )isOsVersionOlderThan3_0
{
	NSString *hVersion = [[UIDevice currentDevice] systemVersion];
	NSString *hVersions[ N_OLDER_OS_VERSIONS ] = { @"2.0", @"2.1", @"2.2", @"2.2.1" };
	
	for( uint8 i = 0 ; i < N_OLDER_OS_VERSIONS ; ++i )
	{
		if( [hVersion compare: hVersions[i] ] == NSOrderedSame )
			return true;
	}
	return false;
}

#undef N_OLDER_OS_VERSIONS

// Fim da implementação da classe
@end

