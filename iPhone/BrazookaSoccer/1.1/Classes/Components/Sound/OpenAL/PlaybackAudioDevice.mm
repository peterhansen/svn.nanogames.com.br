#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include "PlaybackAudioDevice.h"
#include "Exceptions.h"
#include "Macros.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

PlaybackAudioDevice::PlaybackAudioDevice( void ) : AudioDevice()
{
	pDevice = alcOpenDevice( NULL );
	if( !pDevice )
#if DEBUG
		throw OpenALException( "PlaybackAudioDevice::PlaybackAudioDevice()" );
#else
		throw OpenALException();
#endif
	
	pListener = new AudioListener();	
	if( !pListener )
	{
		// Libera o device. Este chamada é necessária, pois o destrutor desta classe não será chamado, já que a construção
		// do objeto ficará incompleta devido ao lançamento da exceção abaixo. Para maiores informações, leia o livro do
		// Bjarne Stroustroup
		alcCloseDevice( pDevice );
		
#if DEBUG
		throw OpenALException( "PlaybackAudioDevice::PlaybackAudioDevice()" );
#else
		throw OpenALException();
#endif
	}
	
	memset( contexts, 0, sizeof( AudioContext* ) * MAX_DEVICE_CONTEXTS );
}
	
/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

PlaybackAudioDevice::~PlaybackAudioDevice( void )
{
	for( uint8 i = 0 ; i < MAX_DEVICE_CONTEXTS ; ++i )
		SAFE_DELETE( contexts[i] );

	alcCloseDevice( pDevice );
}

/*==============================================================================================

MÉTODO newAudioContext
	Obtém um novo AudioContext para este AudioDevice.
 
==============================================================================================*/

AudioContext* PlaybackAudioDevice::newAudioContext( uint8* pIndex )
{
	// Verifica se possui um índice livre
	uint8 i = 0;
	for( ; i < MAX_DEVICE_CONTEXTS ; ++i )
	{
		if( contexts[i] == NULL )
			break;
	}
	
	if( i >= MAX_DEVICE_CONTEXTS )
	{
		#if DEBUG
			LOG( @"PlaybackAudioDevice::newAudioContext(): o numero de contexts ja chegou ao maximo permitido\n" );
		#endif
		return NULL;
	}
	
	AudioContext* pNewContext = new AudioContext( this );
	if( pNewContext )
	{
		if( pIndex )
			*pIndex = i;

		contexts[ i ] = pNewContext;
	}
	return pNewContext;
}

/*==============================================================================================

MÉTODO getAudioContext
	Obtém um AudioContext deste device.
 
==============================================================================================*/

AudioContext* PlaybackAudioDevice::getAudioContext( uint8 index ) const
{
#if DEBUG
	if( index >= MAX_DEVICE_CONTEXTS )
	{
		LOG( @"AudioDevice::getAudioContext(): O indice desejado eh maior do que o numero de contextos possuidos pelo device\n" );
		return NULL;
	}
#endif
	return contexts[ index ];
}

/*==============================================================================================

MÉTODO suspend
	Suspende a execução do device.
 
==============================================================================================*/

bool PlaybackAudioDevice::suspend( void ) const
{
	bool ret = true;
	for( uint8 i = 0 ; i < MAX_DEVICE_CONTEXTS ; ++i )
	{
		if( contexts[i] )
			ret &= contexts[i]->suspend();
	}
	return ret;
}

/*==============================================================================================

MÉTODO resume
	Resume a execução dos device.
 
==============================================================================================*/

bool PlaybackAudioDevice::resume( void ) const
{
	bool ret = true;
	for( uint8 i = 0 ; i < MAX_DEVICE_CONTEXTS ; ++i )
	{
		if( contexts[i] )
			ret |= contexts[i]->resume();
	}
	return ret;
}

/*==============================================================================================

MÉTODO setDistanceModel
	Determina o modelo matemático a ser usado na atenuação por distância. Seu valor padrão é
AL_INVERSE_DISTANCE_CLAMPED.
 
==============================================================================================*/

bool PlaybackAudioDevice::setDistanceModel( ALenum model ) const
{
	// Limpa o código de erro
	alGetError();
	
	alDistanceModel( model );
	
	return alGetError() == AL_NO_ERROR;
}

#endif COMPONENTS_CONFIG_ENABLE_OPENAL
