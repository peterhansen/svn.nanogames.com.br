/*
 *  AudioDevice.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 12/3/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_DEVICE_H
#define AUDIO_DEVICE_H 1

#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

class AudioDevice
{
	public:
		// Destrutor
		virtual ~AudioDevice( void ){};

		// Obtém o device que controla o dispositivo de áudio
		inline ALCdevice* getOpenALDevice( void ) const { return pDevice; };
	
		// Suspende a execução do device
		virtual bool suspend( void ) const = 0;
	
		// Resume a execução dos device
		virtual bool resume( void ) const = 0;
	
	protected:
		// Construtor
		// Exceções : OpenALException
		AudioDevice( void ) : pDevice( NULL ){};

		// Ponteiro para o device OpenAL
		ALCdevice* pDevice;
};

#endif COMPONENTS_CONFIG_ENABLE_OPENAL

#endif AUDIO_DEVICE_H
