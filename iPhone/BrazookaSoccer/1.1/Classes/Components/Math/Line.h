/*
 *  Line.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 3/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef LINE_H
#define LINE_H 1

#include "Point3f.h"

typedef struct Line
{
	// Pontos que formam a linha
	Point3f p0;
	Point3f p1;
	
	// Construtor
	Line( void ) : p0(), p1() {};
	Line( Point3f& p0, Point3f& p1 ) : p0( p0 ), p1( p1 ) {};
	
	// Inicializadores
	inline void set( Point3f& p0, Point3f& p1 )
	{
		this->p0 = p0;
		this->p1 = p1;
	};

	inline void set( float x0, float y0, float z0, float x1, float y1, float z1 )
	{
		p0.set( x0, y0, z0 );
		p1.set( x1, y1, z1 );
	};

} Line;

#endif
