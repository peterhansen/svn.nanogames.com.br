#include "Matrix4x4.h"

#include "MathFuncs.h"

#include <math.h>
#include <string.h>

// Define o número de linhas e colunas da matriz
#define N_ROWS 4
#define N_COLUMNS 4

// Define o número de elementos existentes na matriz
#define N_ELEMENTS ( N_ROWS * N_COLUMNS )

// Variação mínima para considerarmos diferentes 2 números em ponto flutuante
#define MTX4X4_EPSILON 0.000000001f

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4( void )
{
	identity();
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4( float value )
{
	set( value );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4( const float* pElements )
{
	set( pElements );
}

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Matrix4x4::Matrix4x4(	float _00, float _01, float _02, float _03,
						float _10, float _11, float _12, float _13,
						float _20, float _21, float _22, float _23,
						float _30, float _31, float _32, float _33 )
					:	_00( _00 ), _01( _01 ), _02( _02 ), _03( _03 ),
						_10( _10 ), _11( _11 ), _12( _12 ), _13( _13 ),
						_20( _20 ), _21( _21 ), _22( _22 ), _23( _23 ),
						_30( _30 ), _31( _31 ), _32( _32 ), _33( _33 )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix4x4::set( float value )
{
	_00 = value;
	_01 = value;
	_02 = value;
	_03 = value;
	_10 = value;
	_11 = value;
	_12 = value;
	_13 = value;
	_20 = value;
	_21 = value;
	_22 = value;
	_23 = value;
	_30 = value;
	_31 = value;
	_32 = value;
	_33 = value;
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix4x4::set( const float* pElements )
{
	memcpy( m, pElements, sizeof( float ) << 4 );
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Matrix4x4::set( float _00, float _01, float _02, float _03,
					 float _10, float _11, float _12, float _13,
					 float _20, float _21, float _22, float _23,
					 float _30, float _31, float _32, float _33 )
{
	this->_00 = _00;
	this->_01 = _01;
	this->_02 = _02;
	this->_03 = _03;
	this->_10 = _10;
	this->_11 = _11;
	this->_12 = _12;
	this->_13 = _13;
	this->_20 = _20;
	this->_21 = _21;
	this->_22 = _22;
	this->_23 = _23;
	this->_30 = _30;
	this->_31 = _31;
	this->_32 = _32;
	this->_33 = _33;
}

/*==============================================================================================

OPERADOR () lvalue
 
==============================================================================================*/

float& Matrix4x4::operator () ( uint8 row, uint8 column )
{
	return m[ row ][ column ];
}

/*==============================================================================================

OPERADOR () rvalue
 
==============================================================================================*/

float Matrix4x4::operator () ( uint8 row, uint8 column ) const
{
	return m[ row ][ column ];
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

Matrix4x4::operator float* ()
{
	return &m[0][0];
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

Matrix4x4::operator const float* () const
{
	return &m[0][0];
}

/*==============================================================================================

OPERADOR *=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator *= ( const Matrix4x4& mtx )
{
// TODOO : Necessário ???
//	// Evita as imprecisões numéricas
//	if( isIdentity() )
//	{
//		*this = mtx;
//	}
//	else if( !mtx.isIdentity() )
//	{
		Matrix4x4 res;
		for( uint8 row = 0 ; row < N_ROWS ; ++row )
		{
			for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			{
				res.m[ row ][ column ]  =	m[ row ][ 0 ] * mtx.m[ 0 ][ column ] +
											m[ row ][ 1 ] * mtx.m[ 1 ][ column ] +
											m[ row ][ 2 ] * mtx.m[ 2 ][ column ] +
											m[ row ][ 3 ] * mtx.m[ 3 ][ column ];
			}
		}
		*this = res;
//	}
	return *this;
}

/*==============================================================================================

OPERADOR +=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator += ( const Matrix4x4& mtx )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][column ] += mtx.m[ row ][ column ];
	}
	return *this;
}

/*==============================================================================================

OPERADOR -=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator -= ( const Matrix4x4& mtx )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] -= mtx.m[ row ][ column ];
	}
	return *this;
}

/*==============================================================================================

OPERADOR *=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator *= ( float f )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] *= f;
	}
	return *this;
}

/*==============================================================================================

OPERADOR /=
 
==============================================================================================*/

Matrix4x4& Matrix4x4::operator /= ( float f )
{
	// É mais rápido multiplicar do que dividir, por isso invertemos o determinante e depois o
	// multiplicamos pelos elementos da matriz 
	f = 1.0f / f;
	return ( *this ) *= f;
}

/*==============================================================================================

OPERADOR + unário
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator + () const
{
	return *this;
}

/*==============================================================================================

OPERADOR - unário
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator - () const
{
	Matrix4x4 res = *this;
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			res.m[ row ][ column ] = -m[ row ][ column ];
	}
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator * ( const Matrix4x4& mtx ) const
{
	Matrix4x4 res = *this;
	res *= mtx;
	return res;
}

/*==============================================================================================

OPERADOR +
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator + ( const Matrix4x4& mtx ) const
{
	Matrix4x4 res = *this;
	res += mtx;
	return res;
}

/*==============================================================================================

OPERADOR -
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator - ( const Matrix4x4& mtx ) const
{
	Matrix4x4 res = *this;
	res -= mtx;
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator * ( float f ) const
{
	Matrix4x4 res = *this;
	res *= f;
	return res;
}

/*==============================================================================================

OPERADOR /
 
==============================================================================================*/

Matrix4x4 Matrix4x4::operator / ( float f ) const
{
	Matrix4x4 res = *this;
	res /= f;
	return res;
}

/*==============================================================================================

OPERADOR *
 
==============================================================================================*/

Matrix4x4 operator * ( float f, const Matrix4x4& mtx )
{
	return mtx * f;
}

/*==============================================================================================

OPERADOR ==
 
==============================================================================================*/

bool Matrix4x4::operator == ( const Matrix4x4& mtx ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( NanoMath::fcmp( m[ row ][column ], mtx.m[ row ][ column ], MTX4X4_EPSILON ) != 0 )
				return false;
		}
	}
	return true;
}

/*==============================================================================================

OPERADOR !=
 
==============================================================================================*/
	
bool Matrix4x4::operator != ( const Matrix4x4& mtx ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( NanoMath::fcmp( m[ row ][column ], mtx.m[ row ][ column ], MTX4X4_EPSILON ) == 0 )
				return true;
		}
	}
	return false;
}

/*===========================================================================================

MÉTODO transpose
	Transforma esta matriz em sua matriz transposta.

============================================================================================*/

Matrix4x4& Matrix4x4::transpose( void )
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = row + 1 ; column < N_COLUMNS ; ++column )
		{
			const float aux = m[ row ][ column ];
			m[ row ][ column ] = m[ column ][ row ];
			m[ column ][ row ] = aux;
		}
	}
	return *this;
}

/*===========================================================================================

MÉTODO inverse
	Transforma esta matriz em sua matriz inversa.

============================================================================================*/

bool Matrix4x4::inverse( void )
{
	// Executa sequencialmente as reduções necessárias para se criar a matriz adjunta. Já coloca
	// seus fatores na forma transposta, além de realizar as inversões de sinal antecipadamente,
	// para otimizar a rotina (ver o método "Matrix* adjoint( Matrix* )" da classe Matrix para
	// maiores informações sobre o algoritmo genérico para uma matrix NxN)
	Matrix4x4 inv;
	
	// Coluna 0


	// Acha o elemento [0][0] retirando a linha 0 e a coluna 0
    inv.m[0][0] = + ( _11 * _22 * _33 ) + ( _12 * _23 * _31 ) + ( _21 * _13 * _32 )
				  - ( _11 * _23 * _32 ) - ( _12 * _21 * _33 ) - ( _13 * _22 * _31 );
	
	// Acha o elemento [1][0] ([0][1] já transposto) retirando a linha 0 e a coluna 1
    inv.m[1][0] = - ( _10 * _22 * _33 ) - ( _12 * _23 * _30 ) - ( _13 * _20 * _32 )
				  + ( _10 * _23 * _32 ) + ( _12 * _20 * _33 ) + ( _13 * _22 * _30 );
	
	// Acha o elemento [2][0] ([0][2] já transposto) retirando a linha 0 e a coluna 2
    inv.m[2][0] = + ( _10 * _21 * _33 ) + ( _11 * _23 * _30 ) + ( _13 * _20 * _31 )
				  - ( _10 * _23 * _31 ) - ( _11 * _20 * _33 ) - ( _13 * _21 * _30 );
	
	// Acha o elemento [3][0] ([0][3] já transposto) retirando a linha 0 e a coluna 3
    inv.m[3][0] = - ( _10 * _21 * _32 ) - ( _11 * _22 * _30 ) - ( _20 * _12 * _31 )
				  + ( _10 * _22 * _31 ) + ( _11 * _20 * _32 ) + ( _12 * _21 * _30 );


	// Coluna 1


	// Acha o elemento [0][1] ([1][0] já transposto) retirando a linha 1 e a coluna 0
    inv.m[0][1] = - ( _01 * _22 * _33 ) - ( _02 * _23 * _31 ) - ( _03 * _21 * _32 )
				  + ( _01 * _23 * _32 ) + ( _02 * _21 * _33 ) + ( _03 * _22 * _31 );
	
	// Acha o elemento [1][1] retirando a linha 1 e a coluna 1
    inv.m[1][1] = + ( _00 * _22 * _33 ) + ( _02 * _23 * _30 ) + ( _03 * _20 * _32 ) 
				  - ( _00 * _23 * _32 ) - ( _02 * _20 * _33 ) - ( _03 * _22 * _30 );
	
	// Acha o elemento [2][1] ([1][2] já transposto) retirando a linha 1 e a coluna 2
    inv.m[2][1] = - ( _00 * _21 * _33 ) - ( _01 * _23 * _30 ) - ( _03 * _20 * _31 )
				  + ( _00 * _23 * _31 ) + ( _01 * _20 * _33 ) + ( _03 * _21 * _30 );

	// Acha o elemento [3][1] ([1][3] já transposto) retirando a linha 1 e a coluna 3
    inv.m[3][1] = + ( _00 * _21 * _32 ) + ( _01 * _22 * _30 ) + ( _02 * _20 * _31 )
				  - ( _00 * _22 * _31 ) - ( _01 * _20 * _32 ) - ( _02 * _21 * _30 );


	// Coluna 2


	// Acha o elemento [0][2] ([2][0] já transposto) retirando a linha 2 e a coluna 0
    inv.m[0][2] = + ( _01 * _12 * _33 ) + ( _02 * _13 * _31 ) + ( _03 * _11 * _32 )
				  - ( _01 * _13 * _32 ) - ( _02 * _11 * _33 ) - ( _03 * _12 * _31 );
	
	// Acha o elemento [1][2] ([2][1] já transposto) retirando a linha 2 e a coluna 1
    inv.m[1][2] = - ( _00 * _12 * _33 ) - ( _02 * _13 * _30 ) - ( _03 * _10 * _32 ) 
				  + ( _00 * _13 * _32 ) + ( _02 * _10 * _33 ) + ( _03 * _12 * _30 );
	
	// Acha o elemento [2][2] retirando a linha 2 e a coluna 2
    inv.m[2][2] = + ( _00 * _11 * _33 ) + ( _01 * _13 * _30 ) + ( _03 * _10 * _31 )
				  - ( _00 * _13 * _31 ) - ( _01 * _10 * _33 ) - ( _03 * _11 * _30 );
	
	// Acha o elemento [3][2] ([2][3] já transposto) retirando a linha 2 e a coluna 3
    inv.m[3][2] = - ( _00 * _11 * _32 ) - ( _30 * _01 * _12 ) - ( _02 * _10 * _31 )
				  + ( _00 * _12 * _31 ) + ( _01 * _10 * _32 ) + ( _02 * _11 * _30 );

	
	// Coluna 3
	

	// Acha o elemento [0][3] ([3][0] já transposto) retirando a linha 3 e a coluna 0
    inv.m[0][3] = - ( _01 * _12 * _23 ) - ( _02 * _13 * _21 ) - ( _03 * _11 * _22 )
				  + ( _01 * _13 * _22 ) + ( _02 * _11 * _23 ) + ( _03 * _12 * _21 );
	
	// Acha o elemento [1][3] ([3][1] já transposto) retirando a linha 3 e a coluna 1
    inv.m[1][3] = + ( _00 * _12 * _23 ) + ( _02 * _13 * _20 ) + ( _03 * _10 * _22 )
				  - ( _00 * _13 * _22 ) - ( _02 * _10 * _23 ) - ( _03 * _12 * _20 );
	
	// Acha o elemento [2][3] ([3][2] já transposto) retirando a linha 3 e a coluna 2
    inv.m[2][3] = - ( _00 * _11 * _23 ) - ( _01 * _13 * _20 ) - ( _03 * _10 * _21 )
				  + ( _00 * _13 * _21 ) + ( _01 * _10 * _23 ) + ( _03 * _11 * _20 );
	
	// Acha o elemento [3][3] retirando a linha 3 e a coluna 3
    inv.m[3][3] = + ( _00 * _11 * _22 ) + ( _01 * _12 * _20 ) + ( _02 * _10 * _21 )
				  - ( _00 * _12 * _21 ) - ( _01 * _10 * _22 ) - ( _02 * _11 * _20 );

	// Acha o determinante da matriz original (ver o método "float determinant( const Matrix* )"
	// da classe Matrix para maiores informações sobre o algoritmo genérico para uma matrix NxN)
	float det = _00 * inv.m[0][0] + _10 * inv.m[0][1] + _20 * inv.m[0][2] + _30 * inv.m[0][3];
	
	// Se o determinante for 0, a matriz não é inversível
    if( NanoMath::feql( det, 0.0f, MTX4X4_EPSILON ) )
		return false;

	// É mais rápido multiplicar do que dividir, por isso invertemos o determinante e depois o
	// multiplicamos pelos elementos da matriz adjunta
    det = 1.0f / det;

	// Cria a matriz inversa
    for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
			m[ row ][ column ] = inv.m[ row ][ column ] * det;
	}

	return true;
}

/*===========================================================================================

MÉTODO identity
	Transforma esta matriz em uma matriz identidade.

============================================================================================*/

void Matrix4x4::identity( void )
{
	_00 = 1.0f;
	_01 = 0.0f;
	_02 = 0.0f;
	_03 = 0.0f;
	_10 = 0.0f;
	_11 = 1.0f;
	_12 = 0.0f;
	_13 = 0.0f;
	_20 = 0.0f;
	_21 = 0.0f;
	_22 = 1.0f;
	_23 = 0.0f;
	_30 = 0.0f;
	_31 = 0.0f;
	_32 = 0.0f;
	_33 = 1.0f;
}

/*===========================================================================================

MÉTODO isIdentity
	Indica se esta matriz é uma matriz identidade.

============================================================================================*/

bool Matrix4x4::isIdentity( void ) const
{
	for( uint8 row = 0 ; row < N_ROWS ; ++row )
	{
		for( uint8 column = 0 ; column < N_COLUMNS ; ++column )
		{
			if( row != column )
			{
				if( NanoMath::fdif( m[ row ][ column ], 0.0f, MTX4X4_EPSILON ) )
					return false;
			}
			else
			{
				if( NanoMath::fdif( m[ row ][ column ], 1.0f, MTX4X4_EPSILON ) )
					return false;
			}
		}
	}
	return true;
}

