#include "Point2i.h"
#include "Point3f.h"

#include <cmath>

/*==============================================================================================

 CONSTRUTOR
 
==============================================================================================*/

Point2i::Point2i( int32 x, int32 y ) : x( x ), y( y )
{
}

/*==============================================================================================

 CONSTRUTOR
 
==============================================================================================*/

Point2i::Point2i( const Point3f* pPoint ) : x( pPoint->x ), y( pPoint->y )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.
 
==============================================================================================*/

void Point2i::set( Point3f& point )
{
	x = point.x;
	y = point.y;
}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

//Point2i::operator int32* ()
//{
//	return p;
//}

/*==============================================================================================

OPERADOR float*
 
==============================================================================================*/

//Point2i::operator const int32* () const
//{
//	return p;
//}

/*==============================================================================================

 OPERADOR + ( unário )
 
==============================================================================================*/

Point2i Point2i::operator + () const
{
	return *this;
}

/*==============================================================================================

 OPERADOR - ( unário )
 
==============================================================================================*/

Point2i Point2i::operator - () const
{
	return Point2i( -x, -y );
}

/*==============================================================================================

 OPERADOR +
 
==============================================================================================*/

Point2i Point2i::operator + ( const Point2i &point ) const
{
	return Point2i( x + point.x, y + point.y );
}

/*==============================================================================================

 OPERADOR -
 
==============================================================================================*/

Point2i Point2i::operator - ( const Point2i &point ) const
{
	return Point2i( x - point.x, y - point.y );
}

/*==============================================================================================

 OPERADOR *
 
==============================================================================================*/

Point2i Point2i::operator * ( float f ) const
{
	return Point2i( f * x, f * y );
}

/*==============================================================================================

OPERADOR *
	Permite que façamos 4.0f * point, pois o outro "operador *" permite apenas point * 4.0f.

==============================================================================================*/

Point2i operator * ( float f, const Point2i& point )
{
	return point * f;
}

/*==============================================================================================

 OPERADOR /
 
==============================================================================================*/

Point2i Point2i::operator / ( float f ) const
{
	return Point2i( x / f, y / f );
}

/*==============================================================================================

 OPERADOR +=
 
==============================================================================================*/

Point2i& Point2i::operator += ( const Point2i &point )
{
	x += point.x;
	y += point.y;
	return *this;
}

/*==============================================================================================

 OPERADOR -=
 
==============================================================================================*/

Point2i& Point2i::operator -= ( const Point2i &point )
{
	x -= point.x;
	y -= point.y;
	return *this;
}

/*==============================================================================================

 OPERADOR =
 
==============================================================================================*/

// TODOO 
//Point2i& Point2i::operator = ( const Point3f& point )
//{
//	x = point.x;
//	y = point.y;
//	return *this;
//}

/*==============================================================================================

 OPERADOR *=
 
==============================================================================================*/

Point2i& Point2i::operator *= ( float f )
{
	x *= f;
	y *= f;
	return *this;
}

/*==============================================================================================

 OPERADOR /=
 
==============================================================================================*/

Point2i& Point2i::operator /= ( float f )
{
	x /= f;
	y /= f;
	return *this;
}

/*==============================================================================================

 OPERADOR ==
 
==============================================================================================*/

bool Point2i::operator == ( const Point2i &point ) const
{
	return ( x == point.x ) && ( y == point.y ); 
}

/*==============================================================================================

 OPERADOR !=
 
==============================================================================================*/

bool Point2i::operator != ( const Point2i &point ) const
{
	return ( x != point.x ) || ( y != point.y ); 
}

/*==============================================================================================

 OPERADOR % (produto vetorial)
	O produto vetorial só está definido para 3 e 7 dimensões.

==============================================================================================*/

//Point2i Point2i::operator % ( const Point2i& point ) const
//{
//}

/*==============================================================================================

 OPERADOR * (produto interno)
 
==============================================================================================*/

float Point2i::operator * ( const Point2i& point ) const
{
	return x * point.x + y * point.y;
}

/*==============================================================================================
 
 MÉTODO getModule
	Retorna o módulo do vetor descrito pelo ponto.
 
 ==============================================================================================*/

float Point2i::getModule( void ) const
{
	return static_cast< float >( sqrt( x*x + y*y ) );
}

/*==============================================================================================

 MÉTODO normalize
	Normaliza o vetor descrito pelo ponto.
 
 ==============================================================================================*/

Point2i& Point2i::normalize( void )
{
	const float module = getModule();
	if( module )
		( *this ) /= module;
	return *this;
}

/*==============================================================================================

 MÉTODO getNormalized
	Retorna normalizado o vetor descrito pelo ponto.
 
 ==============================================================================================*/

Point2i Point2i::getNormalized( void ) const
{
	Point2i aux = *this;
	aux.normalize();
	return aux;
}

