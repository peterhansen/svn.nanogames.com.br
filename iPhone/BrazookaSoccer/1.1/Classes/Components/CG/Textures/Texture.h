#ifndef TEXTURE_H
#define TEXTURE_H 1

/*==============================================================================================

 CLASSE Texture
 
==============================================================================================*/

#include "Loadable.h"

class Texture : public Loadable
{
	public:
		// Construtor
		Texture( LoadableListener *pListener = NULL, int32 loadableId = -1 );
	
		// Destrutor
		virtual ~Texture( void );

		// Retorna o ID único da textura
		inline uint32 getTextureId( void ) const { return texId; };

		// Obtém o array de bytes da textura
		virtual const uint8* getTextureData( void ) const = 0;

	protected:
		// ID único da textura para o OpenGL
		uint32 texId;	
};

#endif