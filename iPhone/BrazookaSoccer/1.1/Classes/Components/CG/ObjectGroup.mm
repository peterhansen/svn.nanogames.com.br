#include "ObjectGroup.h"

// Components
#include "Exceptions.h"
#include "ObjcMacros.h"

// OpenGL
#include "GLHeaders.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

ObjectGroup::ObjectGroup( uint16 maxObjects )
			: Object(), maxObjects( maxObjects ), nObjects( 0 ), pObjects( NULL ),
			  pObjectsZOrder( NULL )
{
	if( !build() )
#if DEBUG
		throw OutOfMemoryException( "ObjectGroup::ObjectGroup( uint16 ): unable to create object" );
#else
		throw OutOfMemoryException();
#endif
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

ObjectGroup::~ObjectGroup( void )
{
	removeAllObjects();
		
	DELETE_VEC( pObjects );
	DELETE_VEC( pObjectsZOrder );
}

/*==============================================================================================

MÉTODO insertObject
	Insere um novo objeto no grupo.

==============================================================================================*/

bool ObjectGroup::insertObject( Object* pObject, uint16 *pIndex )
{
	// Verifica se o objeto passado é valido e se ainda possuímos espaço no grupo para armazená-lo
	if( ( pObject == NULL ) || ( nObjects >= maxObjects ) )
		return false;
	
	// Armazena o objeto no grupo
	pObjects[ nObjects ] = pObject;
	
	// Indica que este grupo é o pai do objeto
	pObjects[ nObjects ]->setParent( this );
	
	// Atribui uma zOrder para o objeto, que, por default, é o seu índice no vetor pObjects
	pObjectsZOrder[ nObjects ] = pObject;
	
	// Armazena no parâmetro de retorno o índice do grupo no qual o objeto foi inserido
	if( pIndex )
		*pIndex = nObjects;
	
	// Atualiza a variável que controla quantos elementos possuímos no grupo
	++nObjects;

	return true;
}

/*==============================================================================================

MÉTODO removeObject
	Retira um objeto do grupo.

==============================================================================================*/

bool ObjectGroup::removeObject( uint16 index )
{
	// Verifica se o índice está em um intervalo válido
	if( index >= nObjects )
		return false;
	
	int16 zOrder = getObjectZOrder( index );
	if( zOrder == -1 )
		return false;

	// Apaga o objeto
	DELETE( pObjects[ index ] );
	
	// Faz um "chega pra cá" nos elementos do grupo
	uint16 max = nObjects - 1;
	for( uint16 i = index ; i < max ; ++i )
		pObjects[i] = pObjects[i+1];
	
	pObjects[ max ] = NULL;
	
	// Faz um "chega pra cá" no vetor de zOrder
	for( uint16 i = zOrder ; i < max ; ++i )
		pObjectsZOrder[i] = pObjectsZOrder[i+1];
	
	pObjectsZOrder[ max ] = NULL;

	// Atualiza a variável que controla quantos elementos possuímos no grupo
	--nObjects;
		
	return true;
}

/*==============================================================================================

MÉTODO removeAllObjects
	Esvazia o grupo.

==============================================================================================*/

void ObjectGroup::removeAllObjects( void )
{
	#if DEBUG
		LOG( "Destruindo ObjectGroup de %s", getName() );
	#endif

	// Apaga os objetos do grupo
	for( uint16 i = 0 ; i < nObjects ; ++i )
	{
		#if DEBUG
			LOG( "Destruindo objeto %02d de %s - %s", i, getName(), pObjects[i]->getName() );
		#endif
		
		DELETE( pObjects[i] );
		pObjectsZOrder[i] = NULL;
	}

	// Atualiza a variável que controla quantos elementos possuímos no grupo
	nObjects = 0;
}

/*==============================================================================================

MÉTODO getObject
	Retorna o objeto contido no grupo no índice passado como parâmetro.

==============================================================================================*/

Object* ObjectGroup::getObject( uint16 index )
{
	// Verifica se o índice está em um intervalo válido
	if( index >= nObjects )
		return NULL;
	return pObjects[ index ];
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool ObjectGroup::render( void )
{
	if( !Object::render() )
		return false;

	bool ret = false;
	
	bool customViewport = viewport != GET_DEFAULT_VIEWPORT();
	if( customViewport )
	{
		glEnable( GL_SCISSOR_TEST );
		viewport.apply();
	}
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x, position.y, position.z );
	glScalef( scale.x, scale.y, scale.z );
	
	Matrix4x4 aux;
	glMultMatrixf( reinterpret_cast< float* >( rotation.getTranspose( &aux ) ) ); // Matrizes OpenGL são column-major

	// OLD
//	for( uint16 i = 0 ; i < nObjects ; ++i )
//		ret |= pObjects[i]->render();
	
	// Renderiza de acordo com a ordem Z
	for( uint16 i = 0 ; i < nObjects ; ++i )
		ret |= pObjectsZOrder[i]->render();
	
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
	
	if( customViewport )
		glDisable( GL_SCISSOR_TEST );
	
#if DEBUG	
	//renderBorder();
#endif

	return ret;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool ObjectGroup::update( float timeElapsed )
{
	if( !Object::update( timeElapsed ) )
		return false;

	bool ret = false;

	for( uint16 i = 0 ; i < nObjects ; ++i )
		ret |= pObjects[i]->update( timeElapsed );
	
	return ret;
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool ObjectGroup::build( void )
{
	pObjects = new Object*[ maxObjects ];
	if( pObjects == NULL )
		return false;
	
	pObjectsZOrder = new Object*[ maxObjects ];
	if( pObjectsZOrder == NULL )
	{
		DELETE_VEC( pObjects );
		return false;
	}
	
	size.set( SCREEN_WIDTH, SCREEN_HEIGHT, 1.0f );

	return true;
}

/*==============================================================================================

MÉTODO setObjectZOrder
	Determina o slot do grupo onde o objeto deve ser inserido.

==============================================================================================*/

bool ObjectGroup::setObject( uint16 index, Object* pObject )
{
#warning bool ObjectGroup::setObject( uint16 index, Object* pObject ) : Método não está implementado
	return false;

	// TODOO : Ainda não podemos inserir NULL no grupo, então este método perde parte de sua funcionalidade
//	// Verifica se o índice está em um intervalo válido
//	if( index >= nObjects )
//		return false;
//
//	int16 zOrder = index;
//	if( pObjects[ index ] != NULL )
//	{
//		// Obtém a ordem z do objeto antigo
//		zOrder = getObjectZOrder( index );
//
//		// Apaga o objeto antigo
//		delete pObjects[ index ];
//	}
//
//	// Armazena o objeto novo
//	pObjects[ index ] = pObject;
//	pObjects[ index ]->setParent( this );
//	
//	// Atribui uma zOrder para o objeto, que, por default, é o seu índice no vetor pObjects
//	setObjectZOrder( index, zOrder );
//	
//	return true;
}

/*==============================================================================================

MÉTODO setObjectZOrder
	Determina a ordem Z do objeto de índice 'objIndex'.

==============================================================================================*/

bool ObjectGroup::setObjectZOrder( uint16 objIndex, uint16 zOrder )
{
	// Verifica se é um índice de objeto válido
	if( objIndex >= nObjects )
		return false;
	
	return setObjectZOrder( pObjects[ objIndex ], zOrder );
}

/*==============================================================================================

MÉTODO setObjectZOrder
	Determina a ordem Z do objeto.

==============================================================================================*/

bool ObjectGroup::setObjectZOrder( Object* pObject, uint16 zOrder )
{
	// Verifica se é uma zOrder válida
	if( zOrder >= nObjects )
		return false;

	// Verifica se é um índice de objeto válido
	int16 currZOrder = getObjectZOrder( pObject );
	if( currZOrder < 0 )
		return false;
	
	if( currZOrder != zOrder )
	{
		if( currZOrder < zOrder )
		{
			for( int16 i = currZOrder ; i < zOrder ; ++i )
				pObjectsZOrder[ i ] = pObjectsZOrder[ i + 1 ];	
		}
		else
		{
			for( int16 i = currZOrder ; i > zOrder ; --i )
				pObjectsZOrder[ i ] = pObjectsZOrder[ i - 1 ];	
		}

		pObjectsZOrder[ zOrder ] = pObject;
	}
	return true;
}

/*==============================================================================================

MÉTODO switchObjectsZOrder
	Troca a ordem z de 2 objetos.

==============================================================================================*/

bool ObjectGroup::switchObjectsZOrder( uint16 obj1Index, uint16 obj2Index )
{
	if( ( obj1Index >= nObjects ) || ( obj2Index >= nObjects ) )
		return false;

	int16 obj1ZOrder = getObjectZOrder( obj1Index );
	if( obj1ZOrder == -1 )
		return false;

	int16 obj2ZOrder = getObjectZOrder( obj2Index );
	if( obj2ZOrder == -1 )
		return false;

	Object* pTemp = pObjectsZOrder[ obj1ZOrder ];
	pObjectsZOrder[ obj1ZOrder ] = pObjectsZOrder[ obj2ZOrder ];
	pObjectsZOrder[ obj2ZOrder ] = pTemp;

	return true;
}

/*==============================================================================================

MÉTODO getObjectZOrder
	Retorna a ordem Z do objeto de índice 'objIndex'.

==============================================================================================*/

int16 ObjectGroup::getObjectZOrder( uint16 objIndex ) const
{
	// Verifica se o índice está em um intervalo válido
	if( objIndex >= nObjects )
		return -1;
	
	return getObjectZOrder( pObjects[ objIndex ] );
}

/*==============================================================================================

MÉTODO getObjectZOrder
	Retorna a ordem Z do objeto.

==============================================================================================*/

int16 ObjectGroup::getObjectZOrder( const Object* pObject ) const
{
	for( uint16 i = 0 ; i < nObjects ; ++i )
	{
		if( pObjectsZOrder[i] == pObject )
			return i;
	}
	return -1;
}

/*==============================================================================================

MÉTODO getObjectIndex
	Retorna o índice do objeto ou -1 caso este não esteja inserido no grupo.

==============================================================================================*/

int16 ObjectGroup::getObjectIndex( const Object* pObject ) const
{
	for( uint16 i = 0 ; i < nObjects ; ++i )
	{
		if( pObjects[ i ] == pObject )
			return i;
	}
	return -1;
}

/*==============================================================================================

MÉTODO renderBorder
	Renderiza um quad em volta da área do objeto.

==============================================================================================*/

#if DEBUG

void ObjectGroup::renderBorder( void )
{
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE );
	glEnableClientState( GL_VERTEX_ARRAY );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();

	glTranslatef( position.x + ( size.x * scale.x * 0.5f ), position.y + ( size.y * scale.y * 0.5f ), position.z );
	glScalef( scale.x, scale.y, scale.z );
	glScalef( size.x, size.y, size.z );
	
	Matrix4x4 aux;
	glMultMatrixf( ( float* )rotation.getTranspose( &aux ) ); // Matrizes OpenGL são column-major
	
	Point3f border[] =	{
							Point3f( -0.5f, -0.5f ),
							Point3f(  0.5f, -0.5f ),
							Point3f(  0.5f,  0.5f ),
							Point3f( -0.5f,  0.5f )
						};
	
	glColor4ub( 255, 0, 0, 255 );
	glVertexPointer( 3, GL_FLOAT, 0, border );
	glDrawArrays( GL_LINE_LOOP, 0, 4 );
	
	glPopMatrix();
}

#endif
