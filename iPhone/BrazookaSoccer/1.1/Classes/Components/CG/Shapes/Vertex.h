/*
 *  Vertex.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef VERTEX_H
#define VERTEX_H 1

#include "Color.h"
#include "Point3f.h"

// Define o número de componentes que constituem o vértice
#define VERTEX_N_COMPONENTS 12

// TASK : Fazer uma classe Point que contenha:
// - texCoord
// - normal
// - color
// Classe base ter byte descritor dos campos que são implementados. Este campo será preenchido pelas subclasses. Essa idéia vem de DirectX

class Vertex
{
	public:
		// Assim podemos acessar os dados do vértice através de seus valores individuais ou
		// como um array
		union
		{
			struct
			{
				float posX, posY, posZ;
				float s, t;
				float r, g, b, a;
				float normalX, normalY, normalZ;
			};
			float vertex[ VERTEX_N_COMPONENTS ];
		};

		// Constantes para facilitarem o uso de arrays de vértices
		static const int32 VERTEX_POSITION_START = 0;
		static const int32 VERTEX_TEXCOORD_START = 3;
		static const int32 VERTEX_COLOR_START = 5;
		static const int32 VERTEX_NORMAL_START = 9;
		static const int32 VERTEX_COMPONENTS_STRIDE = sizeof( float ) * VERTEX_N_COMPONENTS;

		// Construtores
		Vertex( void );
		Vertex( const float* pValues );
		Vertex( const Point3f* pPosition, float s, float t, const Color* pColor, const Point3f* pNormal );
		Vertex( float posX, float posY, float posZ, float s, float t, float r, float g, float b, float a, float normalX, float normalY, float normalZ );
	
		// Determina os atributos do vértice
		void set( const float* pValues );
		void set( float posX = 0, float posY = 0, float posZ = 0, float s = 0, float t = 0, float r = 0, float g = 0, float b = 0, float a = 0, float normalX = 0, float normalY = 0, float normalZ  = 0 );

		// Determina a posição do vértice
		inline void setPosition( const Point3f* pPosition ){ posX = pPosition->x; posY = pPosition->y; posZ = pPosition->z; };
		inline void setPosition( float x, float y, float z ){ posX = x; posY = y; posZ = z; };
		
		// Determina a coordenada de textura do vértice
		inline void setTexCoords( float texS, float texT ){ s = texS; t = texT; };
		
		// Determina a cor do vértice
		inline void setColor( const Color* pColor ){ r = pColor->getFloatR(); g = pColor->getFloatG(); b = pColor->getFloatB(); a = pColor->getFloatA(); };
		inline void setColor( float red, float green, float blue, float alpha ){ r = red; g = green; b = blue; a = alpha; };
	
		// Determina a normal do vértice
		inline void setNormal( const Point3f* pNormal ){ normalX = pNormal->x; normalY = pNormal->y; normalZ = pNormal->z; };
		inline void setNormal( float x, float y, float z ){ normalX = x; normalY = y; normalZ = z; };
};

#endif

