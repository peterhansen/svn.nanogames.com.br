/*
 *  VertexSet.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/29/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef VERTEXSET_H
#define VERTEXSET_H 1

#include "RefCounter.h"
#include "Renderable.h"
#include "Vertex.h"

#define RENDER_STATE_VERTEX_ARRAY	0x0001
#define RENDER_STATE_TEXCOORD_ARRAY	0x0002
#define RENDER_STATE_COLOR_ARRAY	0x0004
#define RENDER_STATE_NORMAL_ARRAY	0x0008

class VertexSet : public Renderable
{
	public:
		// Destrutor
		virtual ~VertexSet( void ){};
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Retorna o array de vértices que compõe a forma
		virtual const Vertex* getVertexes( void ) const = 0;
	
		// Retorna quantos vértices que compõem a forma
		virtual uint16 getNVertexes( void ) const = 0;
	
		// Mapeia as coordenadas de textura nos vértices do objeto. O parâmetro pTexCoords
		// deve apontar para um array de 8 floats, representando 4 pares (s,t) que definem
		// uma área quadrada na textura ativa. Os pares devem seguir a ordem: bottom-left,
		// top-left, bottom-right e top-right
		virtual void mapTexCoords( const float* pTexCoords ) = 0;
	
		// Determina quais estados devemos habilitar para a renderização do objeto
		void enableRenderStates( uint16 states );
	
		// Determina quais estados devemos desabilitar para a renderização do objeto
		void disableRenderStates( uint16 states );
	
		// Retorna quais estados de renderização estão habilitados
		uint16 getRenderStates( void ) const { return renderStates; };
	
	protected:
		// Construtor
		VertexSet( uint16 renderStates = RENDER_STATE_VERTEX_ARRAY | RENDER_STATE_TEXCOORD_ARRAY );
	
		// TODOO : Passar para a classe Renderable???
		// Indica quais estados devemos habilitar para a renderização do objeto
		uint16 renderStates;
};

// Cria um apelido para os smart pointers utilizados
typedef RefCounter< VertexSet > VertexSetHandler;

// Tipo de ponteiro para as funções de criação de vértices
typedef VertexSetHandler ( *CreateVertexSetFunction )( void );

#endif

