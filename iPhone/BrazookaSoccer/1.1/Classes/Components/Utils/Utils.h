/*
 *  Utils.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/16/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef UTILS_H
#define UTILS_H 1

// Components
#include "Camera.h"
#include "Line.h"
#include "Matrix4x4.h"
#include "NanoRect.h"
#include "Object.h"
#include "Ray.h"
#include "Triangle.h"
#include "Viewport.h"

// C++
#include <string>
#include <ctime> // função 'time'

#if COMPONENTS_CONFIG_ENABLE_OPENAL
	#include "AudioContext.h"
#endif

// iPhoneOS
#include <CoreFoundation/CFURL.h>

namespace Utils
{
	// Retorna o tempo transcorrido desde o início da execução do programa
	double GetElapsedTime( void );
	
	// Escreve a string no arquivo de log padrão
	void Log( const char* pFormatStr, ... );

#if DEBUG
	// Loga o erro do openGL na janela console
	void glLog( const char* pFileName, const unsigned long line );

	// Imprime no console todas as fontes disponíveis no device
	void PrintAllFonts( void );
#endif

	// Retorna se o raio colide com o triângulo. Retorna as coordenadas baricêntricas em u, v e a distância para a origem do raio em t 
	bool IntersectRayTriangle( bool testCull, const Ray* pRay, const Triangle* pTriangle, float* t, float* u, float* v );

	// Cria um raio a partir das coordenadas da tela recebidas
	Ray* GetPickingRay( Ray* pRay, const Camera* pCamera, int32 x, int32 y );

	// Leva as coordenadas do espaço da tela para o espaço local
	bool Unproject( Point3f* pUnprojected, const Camera* pCamera, const Point3f* pPoint );

	// Leva as coordenadas do espaço local para o espaço da tela
	bool Project( Point3f* pProjected, const Camera* pCamera, const Point3f* pPoint );

	// Transforma as coordenadas do raio utilizando a matriz 4x4 apontada por pTransformMtx
	void TransformRay( Ray* pRay, const Matrix4x4* pTransformMtx );

	// Transforma o ponto, pPoint(x, y, z, 1), pela matriz, pMtx, projetando o resultado de volta ao
	// plano w = 1
	Point3f* TransformCoord( Point3f* pOut, const Point3f* pPoint, const Matrix4x4* pMtx );

	// Transforma o vetor, pNormal(x, y, z, 0), pela matriz pMtx
	Point3f* TransformNormal( Point3f* pOut, const Point3f* pNormal, const Matrix4x4* pMtx );

	// Retorna a matriz de projeção que está sendo utilizada pela API gráfica
	Matrix4x4* GetProjectionMatrix( Matrix4x4* pOut );

	// Retorna a matriz de modelo/visão que está sendo utilizada pela API gráfica
	Matrix4x4* GetModelViewMatrix( Matrix4x4* pOut );

	// Retorna o viewport que está sendo utilizado pela API gráfica
	Viewport* GetViewport( Viewport* pOut );

	// Obtém uma matriz de rotação utilizando as funcionalidades da API gráfica
	Matrix4x4* GetRotationMatrix( Matrix4x4* pOut, float angle, const Point3f* pAxis );
	Matrix4x4* GetRotationMatrix( Matrix4x4* pOut, float angle, float axisX, float axisY, float axisZ );

	// Supondo um segmento de reta entre os pontos K e L e outro entre os pontos M e N,
	// esta função determina os valores dos parâmentros S (da reta KL) e T(da reta MN)
	// no ponto de intersecção das retas-suporte dos segmentos passados como parâmetro.
	// Se S e T retornados pela função estiverem entre 0 e 1, então a interseção dos
	// segmentos existe
	// s: Valor do parâmetro no ponto de interseção (sobre a reta KL)
	// t: Valor do parâmetro no ponto de interseção (sobre a reta MN) 
	bool GetLineIntersection2D( float& s, float& t, const Line& kl, const Line& mn );

	// Obtém o ângulo e o eixo de rotação entre dois vetores
	float FindRotationBetweenVectors( Point3f* pOut, const Point3f* pV1, const Point3f* pV2 );

#if COMPONENTS_CONFIG_ENABLE_OPENAL

	// Carrega um arquivo de áudio
	void* GetAudioFile( const char* pFileName, const char* pFileType, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq );

	// Carrega uma fonte de áudio contendo o arquivo passado como parâmetro
	AudioSource* GetAudioSource( AudioContext* pContext, const char* pFileName, const char* pFileType );

#endif

	// Retorna a path do recurso contido no pacote da aplicação
	char* GetPathForResource( char* pBuffer, int32 bufferSize, const char* pResourceName, const char* pResourceType );

	// Retorna a URL do recurso contido no pacote da aplicação.
	CFURLRef GetURLForResource( const char* pResourceName, const char* pResourceType );

	// Mapeia o ponto de acordo com a orientação do device
	Point3f MapPointByOrientation( const Point3f* pPoint );

	// Retorna se o ponto está dentro do retângulo
	bool IsPointInsideRect( const Point3f* pPoint, const Object* pObj );
	bool IsPointInsideRect( const Point3f* pPoint, float x, float y, float width, float height );

	//Retorna o tamanho mínimo e o tamanho máximo que o hardware suporta para um ponto
	void GetPointSizesInfo( float* pMinPointSize, float* pMaxPointSize );

	// Retorna a quantidade de memória livre
	float GetFreeMemory( void );

	// Faz o teste de colisão entre dois quads
	bool CheckRectCollision( const NanoRect *pRect1, const NanoRect *pRect2 );

	// Verifica se o ano é bissexto
	bool IsLeapYear( uint32 year );

	// Retorna o dia atual
	void Today( uint8 *pDay, uint8 *pMonth, uint32 *pYear );

	// Returns the value of time in seconds since 0 hours, 0 minutes, 0 seconds,
	// January 1, 1970, Coordinated Universal Time, without including leap seconds
	inline time_t GetTimeStamp( void )
	{
		return time( NULL );
	}

	// Converte uma std::string em uma std::wstring
	std::wstring& StringToWString( const std::string& from, std::wstring& to );

	// Converte uma std::wstring em uma std::string
	std::string& WStringToString( const std::wstring& from, std::string& to );
	
	// Compara duas versões no formato "xy.zw.tu", sendo que pode haver 'n' caracteres '.', separados por 'm' números.
	// A versão "523.8.345.14223", pore exemplo, é válida, desde que a versão a ser comparada possua o formato
	// "abc.d.efg.hijk".
	// Este método retorna:
	// < 0, caso 'currVersion' seja menor que 'versionToTestAgainst'
	// = 0, caso 'currVersion' e 'versionToTestAgainst' sejam iguais
	// > 0, caso 'currVersion' seja maior que 'versionToTestAgainst'
	int32 CompareVersions( const std::string currVersion, const std::string versionToTestAgainst );
};

#endif
