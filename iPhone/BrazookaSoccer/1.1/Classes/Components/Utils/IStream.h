/*
 *  IStream.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/11/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ISTREAM_H
#define ISTREAM_H 1

class IStream
{
	public:
		// Destrutor
		virtual ~IStream( void ){};
};

#endif
