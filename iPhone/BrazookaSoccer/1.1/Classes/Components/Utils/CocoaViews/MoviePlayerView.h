/*
 *  MoviePlayerView.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright Nano Games 2009. All rights reserved.
 *
 */

#ifndef MOVIE_PLAYER_VIEW_H
#define MOVIE_PLAYER_VIEW_H 1

// Apple Foundation
#import <UIKit/UIKit.h>

// C++
#include <string>

// Components
#include "NanoTypes.h" //uint8

// Declarações adiadas
@class MPMoviePlayerController;

// TODOO : Passar a derivar de UpdatableView
@interface MoviePlayerView : UIImageView
{
	@private
		// Tocador de vídeos
		MPMoviePlayerController* hMoviePlayer;
	
		// Nome do vídeo que esta view irá executar
		std::string movieToPlay;
	
		// Índice da view a ser exibida após o término do vídeo
		uint8 nextView;
}

// Construtor chamado quando carregamos a view via código
-( id )initWithMovie:( const std::string& )movieName AndSuportImg:( const std::string& )supportImg WithNextViewIndex:( uint8 )nextViewIndex;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
