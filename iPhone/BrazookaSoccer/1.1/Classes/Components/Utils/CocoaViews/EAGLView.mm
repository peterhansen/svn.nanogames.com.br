#import "EAGLView.h"
#include "Config.h"

#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/EAGLDrawable.h>

#import "NetworkTestAppDelegate.h"
#include "GameScreen.h"

#include "AccelerometerManager.h"
#include "EventManager.h"
#include "ObjcMacros.h"

#if USE_OFFSCREEN_BUFFER
	#include "Quad.h"
#endif

// Extensão da classe para declarar métodos privados
@interface EAGLView ( Private )

- ( bool ) createFramebuffers;
- ( void ) destroyFramebuffers;

#if USE_OFFSCREEN_BUFFER
	- ( bool ) createOffscreenFramebuffer;
#endif

@end

// Implementção da classe
@implementation EAGLView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize animationInterval;

-( void )setCurrScene:( Scene* )pNewScene
{
	SAFE_DELETE( pCurrScene );
	pCurrScene = pNewScene;
}

-( Scene* )getCurrScene
{
	return pCurrScene;
}

/*==============================================================================================

MENSAGEM layerClass
	Indica que classe utilizaremos para construir a layer de renderização da view. Sua implementação
é obrigatória para criarmos uma layer OpenGL.

==============================================================================================*/

+ ( Class ) layerClass
{
	return [CAEAGLLayer class];
}

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor.

==============================================================================================*/

- ( id ) initWithFrame:( CGRect )frame
{
	if( ( self = [super initWithFrame : frame] ) )
	{
		// Inicializa as variçveis do objeto
		backingWidth = 0;
		backingHeight = 0;
		hContext = NULL;
		viewRenderbuffer = 0;
		viewFramebuffer = 0;		
		depthRenderbuffer = 0;

		hAnimationTimer = NULL;
		animationInterval = -1;
		pCurrScene = NULL;
		lastUpdateTime = 0.0f;
		updateEnabled = true;
		
		#if USE_OFFSCREEN_BUFFER
		
			offscreenFrameBuffer = 0;
			offscreenDepthBuffer = 0;
			pOffscreenRenderBuffer = NULL;
		
			usingOffscreenBuffer = false;
			showOffscreenBuffer = false;

		#endif

		// Determina o intervalo máximo de atualização de tela
		animationInterval = DEFAULT_ANIMATION_INTERVAL;
		[self setUpdateMode: UPDATE_MODE_VARIABLE];
		
		// Get the layer
		CAEAGLLayer* pEaglLayer = ( CAEAGLLayer* )self.layer;
		
		pEaglLayer.opaque = YES;
		pEaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, NULL];
		
		hContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
		
		if( !hContext || ![EAGLContext setCurrentContext : hContext] || ![self createFramebuffers] )
			goto Error;
		
		// Indica se iremos suportar mais de um toque
		[self enableMultipleTouch: ( MAX_TOUCHES > 1 ) ];
		
		return self;
	}

	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void ) dealloc
{
	[self suspend];

	SAFE_DELETE( pCurrScene );
	
	[self destroyFramebuffers];
	
	if( [EAGLContext currentContext] == hContext )
		[EAGLContext setCurrentContext : NULL];
	
	RELEASE_HANDLER( hContext );

	[super dealloc];
}

/*==============================================================================================

MENSAGEM run
	Atualiza e renderiza a cena do jogo.

==============================================================================================*/

- ( void ) run
{
	if( !pCurrScene )
		return;

	if(	lastUpdateTime == 0.0f )
		lastUpdateTime = Utils::GetElapsedTime();

	// Atualiza os elementos da cena
	double currTime = Utils::GetElapsedTime();
	double diff = currTime - lastUpdateTime;

//	#if DEBUG
//		LOG( @"ElapsedTime: %f", diff );
//	#endif

	const double maxAllowed = MAX_UPDATE_INTERVAL;
	if( diff > maxAllowed )
		diff = maxAllowed;

	if( updateEnabled )
		pCurrScene->update( static_cast< float >( updateMode == UPDATE_MODE_VARIABLE ? diff : animationInterval ) );
	
	lastUpdateTime = Utils::GetElapsedTime();

	// Renderiza a cena do jogo
	[self render];
	
	// Opa! Game Over!
	GameScreen* pGameScreen = dynamic_cast< GameScreen* >( pCurrScene );
	if( pGameScreen->isGameOver() )
		pGameScreen->onGameOver();
}

/*==============================================================================================

MENSAGEM render
	Renderiza a cena do jogo.

==============================================================================================*/

- ( void )render
{
	// Inicializa a operação de renderização
	[self prepareRender];

#if USE_OFFSCREEN_BUFFER
	// Renderiza a cena
	if( showOffscreenBuffer )
	{
		// TODOO : Vai ficar errado se estiver landscape!!!!!!!! O certo seria criar
		// uma orthoCamera e renderizar!!!
		glMatrixMode( GL_PROJECTION );
		glLoadIdentity();
		glOrthof( 0.0f, SCREEN_WIDTH, 0.0f, SCREEN_HEIGHT, -1.0f, 1.0f );
		glScalef( 1.0f, -1.0f, 1.0f );
		glTranslatef( 0.0f, -SCREEN_HEIGHT, 0.0f );
		glMatrixMode( GL_MODELVIEW );
		glLoadIdentity();
		glTranslatef( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT, 0.0f );
		glScalef( SCREEN_WIDTH, SCREEN_HEIGHT, 0.0f );

		glEnable( GL_TEXTURE_2D );
		glEnable( GL_BLEND );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		pOffscreenRenderBuffer->load();
		
		Quad aux;
		aux.render();
		
		pOffscreenRenderBuffer->unload();
	}
	else
	{
#endif
		pCurrScene->render();
#if USE_OFFSCREEN_BUFFER
	}
#endif
	
	// Finaliza a operação de renderização
	[self finishRender];
}

/*==============================================================================================

MENSAGEM prepareRender
	Inicializa a operação de renderização.

==============================================================================================*/

- ( void ) prepareRender
{
	[EAGLContext setCurrentContext : hContext];
	glBindFramebufferOES( GL_FRAMEBUFFER_OES, viewFramebuffer );
}

/*==============================================================================================

MENSAGEM finishRender
	Finaliza a operação de renderização.

==============================================================================================*/

- ( void ) finishRender
{
	glBindRenderbufferOES( GL_RENDERBUFFER_OES, viewRenderbuffer );
	[hContext presentRenderbuffer : GL_RENDERBUFFER_OES];
}

/*==============================================================================================

MENSAGEM layoutSubviews
	Indica que as views da aplicação devem ser exibidas. Se a view tiver sido redimensionada,
teremos que atualizar o framebuffer para que ele fique do mesmo tamanho de nossa área da tela.

==============================================================================================*/

- ( void ) layoutSubviews
{
	// TODOO : Enviar um evento para os componentes informando que os buffers foram trocados !!!
	CGRect bounds = [self bounds];
	if( ( roundf( bounds.size.width ) != backingWidth ) || ( roundf( bounds.size.height ) != backingHeight ) )
	{
		[EAGLContext setCurrentContext : hContext];

		[self destroyFramebuffers];

		if( ![self createFramebuffers] )
		{
			// Termina a aplicação
			[( NetworkTestAppDelegate* )APP_DELEGATE quit: ERROR_NO_FRAME_BUFFER];
			return;
		}

		// Renderiza a view
		[self render];
	}
}

/*==============================================================================================

MENSAGEM createFramebuffers
	Aloca os buffers necessçrios à renderização OpenGL.

==============================================================================================*/

- ( bool ) createFramebuffers
{
	glGenFramebuffersOES( 1, &viewFramebuffer );
	glGenRenderbuffersOES( 1, &viewRenderbuffer );

	glBindFramebufferOES( GL_FRAMEBUFFER_OES, viewFramebuffer );	
	glBindRenderbufferOES( GL_RENDERBUFFER_OES, viewRenderbuffer );

	[hContext renderbufferStorage : GL_RENDERBUFFER_OES fromDrawable : ( CAEAGLLayer* )self.layer];

	glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer );	
	glGetRenderbufferParameterivOES( GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth );
	glGetRenderbufferParameterivOES( GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight );
	
	// Aloca o buffer de profundidade
	if( USE_DEPTH_BUFFER )
	{
		glGenRenderbuffersOES( 1, &depthRenderbuffer );
		glBindRenderbufferOES( GL_RENDERBUFFER_OES, depthRenderbuffer );
		glRenderbufferStorageOES( GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, backingWidth, backingHeight );
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer );
	}

	if( glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) != GL_FRAMEBUFFER_COMPLETE_OES )
	{
#if DEBUG
		LOG( "failed to make complete framebuffer object %x\n", glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) );
#endif
		return false;
	}

	#if USE_OFFSCREEN_BUFFER
		// Cria um buffer para renderizações fora da tela
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, GL_NONE );
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, GL_NONE );

		bool ret = [self createOffscreenFramebuffer];
	
		// Começa a aplicação utilizando o buffer "da tela"
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer );
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer );
		
		return ret;
	#else
		return true;
	#endif
}

/*==============================================================================================

MENSAGEM createOffscreenFramebuffer
	Cria o FBO que permitirç renderizações fora da tela.

==============================================================================================*/

#if USE_OFFSCREEN_BUFFER

- ( bool ) createOffscreenFramebuffer
{
	// Cria uma textura para ser o buffer de renderização
	pOffscreenRenderBuffer = new Texture2D();
	if( !pOffscreenRenderBuffer || !pOffscreenRenderBuffer->loadTextureEmpty( Utils::GetNextHighestPowerOf2( backingWidth ), Utils::GetNextHighestPowerOf2( backingHeight ) ) )
		return false;
	
	glFramebufferTexture2DOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_TEXTURE_2D, pOffscreenRenderBuffer->getTextureId(), 0 );

	// Aloca o buffer de profundidade
	if( USE_DEPTH_BUFFER )
	{
		glGenRenderbuffersOES( 1, &offscreenDepthBuffer );
		glBindRenderbufferOES( GL_RENDERBUFFER_OES, offscreenDepthBuffer );
		glRenderbufferStorageOES( GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, pOffscreenRenderBuffer->getWidth(), pOffscreenRenderBuffer->getHeight() );
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, offscreenDepthBuffer );
	}

	if( glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) != GL_FRAMEBUFFER_COMPLETE_OES )
	{
#if DEBUG
		LOG( @"Failed to make complete offscreen FBO %x\n", glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) );
#endif
		return false;
	}
	
	glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer );
	glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer );
	
	return true;
}

#endif

/*==============================================================================================

MENSAGEM destroyFramebuffers
	Libera a memória utilizada pelos buffers do OpenGL.

==============================================================================================*/

- ( void ) destroyFramebuffers
{
	// Destrói os buffers da tela
	glDeleteFramebuffersOES( 1, &viewFramebuffer );
	viewFramebuffer = 0;

	glDeleteRenderbuffersOES( 1, &viewRenderbuffer );
	viewRenderbuffer = 0;
	
	if( depthRenderbuffer > 0 )
	{
		glDeleteRenderbuffersOES( 1, &depthRenderbuffer );
		depthRenderbuffer = 0;
	}
	
	#if USE_OFFSCREEN_BUFFER

		// Destrói os buffers para renderizações fora da tela
		glDeleteFramebuffersOES( 1, &offscreenFrameBuffer );
		offscreenFrameBuffer = 0;

		SAFE_DELETE( pOffscreenRenderBuffer );
		
		if( offscreenDepthBuffer > 0 )
		{
			glDeleteRenderbuffersOES( 1, &offscreenDepthBuffer );
			offscreenDepthBuffer = 0;
		}

	#endif
}

/*==============================================================================================

MENSAGEM startAnimation
	Inicia o loop de renderização.

==============================================================================================*/

- ( void ) startAnimation
{
	[self stopAnimation];
	hAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector(run) userInfo:NULL repeats:YES];
	
	// OLD
//	lastUpdateTime = Utils::GetElapsedTime();
}

/*==============================================================================================

MENSAGEM stopAnimation
	Pára o loop de renderização.

==============================================================================================*/

- ( void ) stopAnimation
{
	if( hAnimationTimer )
	{
		[hAnimationTimer invalidate];
		
		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hAnimationTimer invalidate] chama
		// [hAnimationTimer release] automaticamente
		hAnimationTimer = NULL;
	}
}

/*==============================================================================================

MENSAGEM touchesCanceled
	Chamada quando um evento do sistema, como um aviso de pouca memória, cancela o evento.

==============================================================================================*/

- ( void ) touchesCancel
{
	#if DEBUG
	@try
	{
	#endif

		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_CANCELED, NULL );

	#if DEBUG
	}
	@catch( NSException* hException )
	{
		LOG( "touchesCancel" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM touchesEnded
	Chamada quando uma sequência de um toque termina.

==============================================================================================*/

- ( void )touchesEnded: ( NSSet* )touches withEvent: ( UIEvent* )event
{
	#if DEBUG
	@try
	{
	#endif
		
		const NSArray* pTouchesArray = [touches allObjects];
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_ENDED, pTouchesArray );

	#if DEBUG
	}
	@catch( NSException* hException )
	{
		LOG( "touchesEnded" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM touchesMoved
	Chamada quando um toque jç iniciado é movimentado.

==============================================================================================*/

- ( void )touchesMoved: ( NSSet* )touches withEvent: ( UIEvent* )event
{
	#if DEBUG
	@try
	{
	#endif

		const NSArray* pTouchesArray = [touches allObjects];
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_MOVED, pTouchesArray );

	#if DEBUG
	}
	@catch( NSException* hException )
	{
		LOG( "touchesMoved" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM touchesBegan
	Chamada quando uma nova sequência de toque começa.

==============================================================================================*/

- ( void )touchesBegan: ( NSSet* )touches withEvent: ( UIEvent* )event
{
	#if DEBUG
	@try
	{
	#endif

		const NSArray* pTouchesArray = [touches allObjects];
		EventManager::GetInstance()->setEvent( EVENT_TOUCH, EVENT_TOUCH_BEGAN, pTouchesArray );

	#if DEBUG
	}
	@catch( NSException* hException )
	{
		LOG( "touchesBegan" );
	}
	#endif
}

/*==============================================================================================

MENSAGEM suspend
	Pára o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL.

==============================================================================================*/

- ( void ) suspend
{
	[self stopAnimation];
	pCurrScene->suspend();
}

/*==============================================================================================

MENSAGEM resume
	Reinicia o processamento dos acelerômetros, a renderização OpenGL e os processos OpenAL.

==============================================================================================*/

- ( void ) resume
{
	pCurrScene->resume();
	[self startAnimation];
}

/*==============================================================================================

MENSAGEM showOffscreenBuffer:
	Determina se devemos renderizar o offscreen buffer ao invés da cena atual.

==============================================================================================*/

- ( void ) showOffscreenBuffer: ( bool )s
{
	#if USE_OFFSCREEN_BUFFER
		showOffscreenBuffer = s;
	#endif
}

/*==============================================================================================

MENSAGEM setOffscreenRender:
	Indica se o usuçrio deseja executar as próximas rederizações fora da tela.

==============================================================================================*/

- ( bool ) setOffscreenRender: ( bool )s
{
	#if USE_OFFSCREEN_BUFFER
	
		if( s == usingOffscreenBuffer )
			return true;
		
		// OBS : Acho q n precisa dessas duas linhas. Basta associar diretamente sem ter q quebrar os vínculos anteriores
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, GL_NONE );
		glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, GL_NONE );

		usingOffscreenBuffer = s;
		if( usingOffscreenBuffer )
		{
			glFramebufferTexture2DOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_TEXTURE_2D, pOffscreenRenderBuffer->getTextureId(), 0 );
			glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, offscreenDepthBuffer );
		}
		else
		{
			glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, viewRenderbuffer );
			glFramebufferRenderbufferOES( GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, depthRenderbuffer );
		}
		
		return glCheckFramebufferStatusOES( GL_FRAMEBUFFER_OES ) == GL_FRAMEBUFFER_COMPLETE_OES;
	
	#else
		return false;
	#endif
}

/*==============================================================================================

MENSAGEM clearOffscreenBuffer:
	Limpa o offscreen buffer com a cor desejada.

==============================================================================================*/

- ( void ) clearOffscreenBuffer: ( Color* ) pColor
{
	#if USE_OFFSCREEN_BUFFER

		bool lastState = usingOffscreenBuffer;
		
		[self setOffscreenRender: true];

		glClearColor( pColor->getFloatR(), pColor->getFloatG(), pColor->getFloatB(), pColor->getFloatA() );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		
		[self setOffscreenRender: lastState];

	#endif
}

/*==============================================================================================

MENSAGEM setCurrScene
	Determina a cena atual.

==============================================================================================*/

-( void )setCurrScene:( Scene* )pNewScene
{
	delete pCurrScene;
	pCurrScene = pNewScene;
}

/*==============================================================================================

MENSAGEM enableMultipleTouch:
	Indica se esta view deve receber múltiplos eventos de toque.

==============================================================================================*/

- ( void ) enableMultipleTouch: ( bool )s
{
	[self setMultipleTouchEnabled: s ? YES : NO];
}

/*==============================================================================================

MENSAGEM enableExclusiveTouch:
	Indica que esta view não deve repassar os eventos de toque para outras views, se tornando
a única tratadora de eventos da aplicação.

==============================================================================================*/

- ( void ) enableExclusiveTouch: ( bool )s
{
	[self setExclusiveTouch: s ? YES : NO];
}

/*==============================================================================================

MENSAGEM setUpdateEnabled:
	Indica se devemos fazer a atualização da cena.

==============================================================================================*/

- ( void ) setUpdateEnabled:( bool )enabled
{
	updateEnabled = enabled;
	if( updateEnabled )
		lastUpdateTime = 0.0f;
}

/*==============================================================================================

MENSAGEM isUpdateEnabled
	Indica se estamos fazendo a atualização da cena.

==============================================================================================*/

- ( bool ) isUpdateEnabled
{
	return updateEnabled;
}

/*==============================================================================================

MENSAGEM setUpdateMode:
	Determina o modo de atualização da cena atual.

==============================================================================================*/

- ( void ) setUpdateMode:( UpdateMode )mode
{
	updateMode = mode;
}

// TASK : Permitir que o usuário desta classe utilize estes métodos
//- beginGeneratingDeviceOrientationNotifications
//- endGeneratingDeviceOrientationNotifications

// Fim da implementção da classe
@end


// TODOO: Pesquisar
//NSTimeInterval waitingTime = 0.01; // ten milliseconds
//NSDate *recoverDate = [[NSDate date] addTimeInterval:waitingTime + [[NSDate date] timeIntervalSinceNow]];
//[NSThread sleepUntilDate: recoverDate]; // Blocks the current thread until the time specified by recoverDate (10 ms)
//
///*****************************************
//  * idler.c
//  *
//  * Uses IOKit to figure out the idle time of the system. The idle time
//  * is stored as a property of the IOHIDSystem class; the name is
//  * HIDIdleTime. Stored as a 64-bit int, measured in ns.
//  *
//  * The program itself just prints to stdout the time that the computer has
//  * been idle in seconds.
//  *
//  * Compile with gcc -Wall -framework IOKit -framework Carbon idler.c -o
//  * idler
//  *
//  * Copyright (c) 2003, Stanford University
//  * All rights reserved.
//  *
//  * Redistribution and use in source and binary forms, with or without
//  * modification, are permitted provided that the following conditions are
//  * met:
//  * Redistributions of source code must retain the above copyright notice,
//  * this list of conditions and the following disclaimer.
//  *
//  * Redistributions in binary form must reproduce the above copyright notice,
//  * this list of conditions and the following disclaimer in the documentation
//  * and/or other materials provided with the distribution.
//  *
//  * Neither the name of Stanford University nor the names of its contributors
//  * may be used to endorse or promote products derived from this software
//  * without specific prior written permission.
//  *
//  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
//  * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
//  * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
//  * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
//  * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
//  * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
//  * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
//  * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//  */
// 
//#include 
//#include 
//#include 
// 
//#include 
//#include 
//#include 
// 
///* 10^9 --  number of ns in a second */
//#define NS_SECONDS 1000000000
// 
//int main(int argc, char **argv) {
//  mach_port_t masterPort;
//  io_iterator_t iter;
//  io_registry_entry_t curObj;
// 
//  IOMasterPort(MACH_PORT_NULL, &masterPort);
// 
//  /* Get IOHIDSystem */
//  IOServiceGetMatchingServices(masterPort,
//                 IOServiceMatching("IOHIDSystem"),
//                 &iter);
//  if(iter == 0) {
//    printf("Error accessing IOHIDSystem\n");
//    exit(1);
//  }
// 
//  curObj = IOIteratorNext(iter);
// 
//  if(curObj == 0) {
//    printf("Iterator's empty!\n");
//    exit(1);
//  }
// 
//  CFMutableDictionaryRef properties = 0;
//  CFTypeRef obj;
// 
//  if(IORegistryEntryCreateCFProperties(curObj, &properties,
//                   kCFAllocatorDefault, 0) ==
//      KERN_SUCCESS && properties != NULL) {
// 
//    obj = CFDictionaryGetValue(properties, CFSTR("HIDIdleTime"));
//    CFRetain(obj);
//  } else {
//    printf("Couldn't grab properties of system\n");
//    obj = NULL;
//  }
// 
//  if(obj) {
//    uint64_t tHandle;
// 
//    CFTypeID type = CFGetTypeID(obj);
// 
//    if(type == CFDataGetTypeID()) {
//      CFDataGetBytes((CFDataRef) obj,
//           CFRangeMake(0, sizeof(tHandle)),
//           (UInt8*) &tHandle);
//    }  else if(type == CFNumberGetTypeID()) {
//      CFNumberGetValue((CFNumberRef)obj,
//             kCFNumberSInt64Type,
//             &tHandle);
//    } else {
//      printf("%d: unsupported type\n", (int)type);
//      exit(1);
//    }
// 
//    CFRelease(obj);
// 
//    // essentially divides by 10^9
//    tHandle >>= 30;
//    printf("%qi\n", tHandle);
//  } else {
//    printf("Can't find idle time\n");
//  }
// 
//  /* Release our resources */
//  IOObjectRelease(curObj);
//  IOObjectRelease(iter);
//  CFRelease((CFTypeRef)properties);
// 
//  return 0;
//}
//
//---- HIDIdleTime.h ----
//#import <Foundation/Foundation.h>
//@interface HIDIdleTime : NSObject {
//   io_registry_entry_t _hidEntry;
//}
//- (uint64_t)idleTime;
//- (unsigned)idleTimeInSeconds;
//@end
//
//---- HIDIdleTime ----
//#import "HIDIdleTime.h"
//#import <JRFoundation/JRFoundation.h>
//
//@implementation HIDIdleTime
//
//- (id)init {
//   self = [super init];
//   if( self ) {
//       mach_port_t masterPort;
//       kern_return_t err = IOMasterPort( MACH_PORT_NULL, &masterPort );
//       [NSException raiseKernReturn:err];
//       
//       io_iterator_t  hidIter;
//       err = IOServiceGetMatchingServices( masterPort,
//IOServiceMatching("IOHIDSystem"), &hidIter );
//       [NSException raiseKernReturn:err];
//       NSAssert0( hidIter != NULL );
//       
//       _hidEntry = IOIteratorNext( hidIter );
//       NSAssert0( _hidEntry != NULL );
//       IOObjectRelease(hidIter);
//   }
//   return self;
//}
//
//- (void)dealloc {
//   if( _hidEntry ) {
//       kern_return_t err = IOObjectRelease( _hidEntry );
//       [NSException raiseKernReturn:err];
//       _hidEntry = NULL;
//   }
//   [super dealloc];
//}
//
//- (uint64_t)idleTime {
//   NSMutableDictionary *hidProperties;
//   kern_return_t err = IORegistryEntryCreateCFProperties( _hidEntry,
//(CFMutableDictionaryRef*) &hidProperties, kCFAllocatorDefault, 0 );
//   [NSException raiseKernReturn:err];
//   NSAssert0( hidProperties != nil );
//   [hidProperties autorelease];
//   
//   id hidIdleTimeObj = [hidProperties objectForKey:@"HIDIdleTime"];
//   NSAssert0( [hidIdleTimeObj isKindOfClass:[NSData class]] ||
//[hidIdleTimeObj isKindOfClass:[NSNumber class]] );
//   uint64_t result;
//   if( [hidIdleTimeObj isKindOfClass:[NSData class]] ) {
//       NSAssert0( [(NSData*)hidIdleTimeObj length] == sizeof( result ) );
//       [hidIdleTimeObj getBytes:&result];
//   } else {
//       ASSERT_CAST( result, long long );
//       result = [hidIdleTimeObj longLongValue];
//   }
//   
//   return result;
//}
//
//- (unsigned)idleTimeInSeconds {
//   #define NS_SECONDS 1000000000 // 10^9 -- number of ns in a second
//   return [self idleTime] / NS_SECONDS;
//}
//
//- (void)sendEvent:(UIEvent *)event {
//    [super sendEvent:event];
//
//    // Only want to reset the timer on a Began touch or an Ended touch, to reduce the number of timer resets.
//    NSSet *allTouches = [event allTouches];
//    if([allTouches count] > 0) {
//        // allTouches count only ever seems to be 1, so anyObject works here.
//        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
//        if(phase == UITouchPhaseBegan || phase == UITouchPhaseEnded)
//            [self resetIdleTimer];
//    }
//}
//
//- (void)resetIdleTimer {
//    if(idleTimer) {
//        [idleTimer invalidate];
//        [idleTimer release];
//    }
//
//    idleTimer = [[NSTimer scheduledTimerWithTimeInterval:maxIdleTime target:self selector:@selector(idleTimerExceeded) userInfo:nil repeats:NO] retain];
//}
//
//- (void)idleTimerExceeded {
//    NSLog(@"idle time exceeded");
//}
//
////idleTimerDisabled
////A Boolean value that controls whether the idle timer is disabled for the application.
////
////@property(nonatomic, getter=isIdleTimerDisabled) BOOL idleTimerDisabled
////Discussion
////The default value of this property is NO. When most applications have no touches as user input for a short period, the system puts the device into a “sleep” state where the screen dims. This is done for the purposes of conserving power. However, applications that don't have user input except for the accelerometer—games, for instance—can, by setting this property to YES, disable the “idle timer” to avert system sleep.
//
//
//@end
