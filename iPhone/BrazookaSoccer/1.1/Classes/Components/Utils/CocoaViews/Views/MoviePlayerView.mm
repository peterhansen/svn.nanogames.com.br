#include "MoviePlayerView.h"

// AppleFoundation
#import <MediaPlayer/MediaPlayer.h>

// Components
#include "ApplicationManager.h"
#include "ObjcMacros.h"
#include "Utils.h"

// Extensão da classe para declarar métodos privados
@interface MoviePlayerView ( Private )

	// Inicializa o objeto
	- ( bool ) buildWithMovie:( const std::string& )movieName AndSuportImg:( const std::string& )supportImg WithNextViewIndex:( uint8 )nextViewIndex AndVideoEffectiveTime:( float )secs;

	// Cria o reprodutor de vídeos
	- ( bool ) createMoviePlayer;

	// Desaloca o componente reprodutor de vídeos
	- ( void ) deallocMoviePlayer;

	// Impede o flickering no final da exibição do vídeo
	- ( void ) onEnding;

	// Chama a próxima tela do jogo
	-( void ) onEnd;

@end

// Início da implementação da classe
@implementation MoviePlayerView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithMovie:AndSuportImg:WithNextViewIndex:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithMovie:( const std::string& )movieName AndSuportImg:( const std::string& )supportImg WithNextViewIndex:( uint8 )nextViewIndex AndVideoEffectiveTime:( float )secs
{
    if( ( self = [super initWithFrame: [[UIScreen mainScreen] bounds]] ) )
	{
		// Inicializa o objeto
		if( ![self buildWithMovie: movieName AndSuportImg: supportImg WithNextViewIndex: nextViewIndex AndVideoEffectiveTime: secs] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

- ( bool ) buildWithMovie:( const std::string& )movieName AndSuportImg:( const std::string& )supportImg WithNextViewIndex:( uint8 )nextViewIndex AndVideoEffectiveTime:( float )secs
{
	effectiveTime = secs;
	
	// Background para impedir o fade final do vídeo
	char aux[ PATH_MAX ];
	UIImage* hImg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( aux, PATH_MAX, supportImg.c_str(), "png" ) ) ];
	if( hImg == nil )
		return false;
	
	UIImageView* hBkg = [[UIImageView alloc]initWithImage: hImg];
	if( hBkg == NULL )
		return false;

	if( [APP_DELEGATE isOrientationLandscape] )
	{
		CGPoint center = CGPointMake( HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH );
		hBkg.center = center;

		CGAffineTransform transform = [hBkg transform];
		transform = CGAffineTransformRotate( transform, static_cast< float >( ( M_PI * 0.5f ) ) );
		hBkg.transform = transform;
	}

	[self addSubview: hBkg];
	hBkg.hidden = YES;
	
	// OLD: Não podemos criar o MoviePlayer numa thread que não seja a principal
//	return [self createMoviePlayer];
	
	// Armazena os parâmetros necessários
	movieToPlay = movieName;
	nextView = nextViewIndex;
	
	return true;
}

/*==============================================================================================

MENSAGEM createMoviePlayer
	Cria o reprodutor de vídeos.

==============================================================================================*/

-( bool )createMoviePlayer
{
	//	Because it takes time to load movie files into memory, MPMoviePlayerController
	//	automatically begins loading your movie file shortly after you initialize a new 
	//	instance. When it is done preloading the movie file, it sends the
	//	MPMoviePlayerContentPreloadDidFinishNotification notification to any registered 
	//	observers. If an error occurred during loading, the userInfo dictionary of the 
	//	notification object contains the error information. If you call the play method 
	//	before preloading is complete, no notification is sent and your movie begins 
	//	playing as soon as it is loaded into memory

	// OBS: Não utilizado
	// Registra-se para receber a mensagem indicando que o vídeo está pronto para reprodução
//	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( moviePreloadDidFinish: ) name: MPMoviePlayerContentPreloadDidFinishNotification object: nil ];
	
    // Carrega o vídeo
	hMoviePlayer = [[ MPMoviePlayerController alloc ] initWithContentURL: reinterpret_cast< const NSURL* >( Utils::GetURLForResource( movieToPlay.c_str(), "m4v" )) ];
	if( hMoviePlayer == NULL )
		return false;
	
	// OLD : setOrientation:animated: é uma API privada, e a m&$#@ da Apple reclama se utilizarmos tais
	// APIs. Já que ela não quer que façamos a rotação do vídeo da forma correta, aplicaremos a forma
	// "bruta" : rotacionamos o próprio arquivo de vídeo. No entanto, caso façamos uma aplicação que
	// possa ser utilizada em quaisquer orientações, teremos que mandar vários arquivos... Viva Windows!!!
	
	// Se a chamada não funcionar, não exibimos o vídeo
	// ATENCAO: Esta chamada não é documentada, mas é o único jeito de fazer um vídeo tocar em qualquer orientação quando utilizamos MPMoviePlayerController
//	if( [hMoviePlayer respondsToSelector: @selector( setOrientation:animated: )] )
//	{
//		[hMoviePlayer setOrientation: [APP_DELEGATE getOrientation] animated: NO];
//
//		// OBS: Não utilizado
//		// Registra-se para obter os eventos da reprodução do vídeo
//	//  [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( moviePlayBackDidFinish: ) name: MPMoviePlayerPlaybackDidFinishNotification object: nil];
//	//  [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( movieScalingModeDidChange: ) name: MPMoviePlayerScalingModeDidChangeNotification object: nil];
//
		// Poderíamos usar MPMovieScalingModeNone, pois já fazemos os videos na resolução de tela do iPhone. No entanto, utilizei
		// MPMovieScalingModeFill caso desejemos economizar espaço
		//hMoviePlayer.scalingMode = MPMovieScalingModeNone;
		hMoviePlayer.scalingMode = MPMovieScalingModeFill;
		
		// Não deixa o usuário ter controle sobre a execução do vídeo
		hMoviePlayer.movieControlMode = MPMovieControlModeHidden;

		// A cor de background do vídeo poder ser qualquer UIColor
		hMoviePlayer.backgroundColor = [UIColor clearColor];
		
		return true;
//	}
//	else
//	{
//		[self deallocMoviePlayer];
//		return false;
//	}
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self deallocMoviePlayer];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM deallocMoviePlayer
	Desaloca o componente reprodutor de vídeos.

==============================================================================================*/

- ( void )deallocMoviePlayer
{
	if( hMoviePlayer != NULL )
	{
		// Cancela os eventos da exibição de vídeo
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerContentPreloadDidFinishNotification object:nil];
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object: hMoviePlayer];
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerScalingModeDidChangeNotification object: hMoviePlayer];
		
		[hMoviePlayer stop];

		// Deleta o moviePlayer
		[hMoviePlayer release];
		hMoviePlayer = NULL;
	}
}

/*==============================================================================================

MENSAGEM onEnd
	Chama a próxima tela do jogo.

==============================================================================================*/

- ( void ) onEnd
{
	[[ApplicationManager GetInstance] performTransitionToView: nextView];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	// OBS: Antigamente este código ficava em build, mas precisamos garantir que o
	// MoviePlayer é criado na thread principal, por isso passamos o código para
	// onBecomeCurrentScreen
	if( [self createMoviePlayer] )
	{
		// As soon as you call the play: method, the player initiates a transition that fades 
		// the screen from your current window content to the designated background 
		// color of the player. If the movie cannot begin playing immediately, the player 
		// object continues displaying the background color and may also display a progress 
		// indicator to let the user know the movie is loading. When playback finishes, the 
		// player uses another fade effect to transition back to your window content
		[hMoviePlayer play];
		[NSTimer scheduledTimerWithTimeInterval: effectiveTime target:self selector:@selector( onEnding ) userInfo:NULL repeats:NO];
	}
	else
	{
		[self onEnding];
	}
}

/*==============================================================================================

MENSAGEM moviePreloadDidFinish
	Mensagem chamada quando o vídeo acaba de ser carregado.

==============================================================================================*/

//- ( void )moviePreloadDidFinish:( NSNotification* )hNotification
//{
//	MPMoviePlayerController* hMoviePlayer = [ hNotification object ];
//}

/*==============================================================================================

MENSAGEM onEnding
	Tenta impedir o flickering no final da exibição do vídeo.

==============================================================================================*/

-( void ) onEnding
{
	#if DEBUG
		LOG( "SplashGame: Chamou onEnding" );
	#endif

	// Impede o fade no final do vídeo
	UIImageView* hBkg = ( UIImageView* )[[self subviews] objectAtIndex: 0];
	hBkg.hidden = NO;
	[self bringSubviewToFront: hBkg];
	[hBkg setNeedsDisplay];

	[self deallocMoviePlayer];

	[self onEnd];
}

/*==============================================================================================

MENSAGEM moviePlayBackDidFinish
	Mensagem chamada quando o video terminar de ser exibido.

==============================================================================================*/

//- ( void )moviePlayBackDidFinish:( NSNotification* )hNotification
//{
//	// Impede o fade no final do vídeo
//	UIImageView* hBkg = ( UIImageView* )[[self subviews] objectAtIndex: 0];
//	hBkg.hidden = NO;
//	
//	CGRect screenSize = [[UIScreen mainScreen] bounds];
//	[hBkg drawRect: screenSize];
//	[hBkg setNeedsDisplay];
//	
//	[hMoviePlayer stop];
//	[self deallocMoviePlayer];
//
//	[hBkg drawRect: screenSize];
//	[hBkg setNeedsDisplay];
//
//	[self onEnd];
//}

/*==============================================================================================

MENSAGEM movieScalingModeDidChange
	Mensagem chamada quando a escala do video é alterada.

==============================================================================================*/

//- ( void )movieScalingModeDidChange:( NSNotification* )hNotification
//{
//	MPMoviePlayerController* hMoviePlayer = [ hNotification object ];
//}

// Fim da implementação da classe
@end
