#include "EventManager.h"
#include "Macros.h"

// Inicializa as variáveis estáticas da classe
EventManager* EventManager::pSingleton = NULL;

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

EventManager::EventManager( void )
// TASK : Otimizar com controle de índice do usuário
//: nListeners( 0 )
{
	removeAllListeners();
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

EventManager::~EventManager( void )
{
	// TASK : Otimizar com controle de índice do usuário
	//nListeners = 0;
}

/*==============================================================================================

MÉTODO Create
	 Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance().
No entanto, isso poderia resultar em exceções disparadas no meio do código da aplicação, o que
impactaria o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
chamado no início da aplicação, já que o usuário dos componentes parte do princípio que sempre
GetInstance() sempre retornará um valor válido.

==============================================================================================*/
 
bool EventManager::Create( void )
{
	if( !pSingleton )
		pSingleton = new EventManager();
	return pSingleton != NULL;
}

/*==============================================================================================

MÉTODO Destroy
	Deleta o singleton.

==============================================================================================*/

void EventManager::Destroy( void )
{
	SAFE_DELETE( pSingleton );
}

/*==============================================================================================

MÉTODO insertListener
	Acrescenta um objeto ao array de listeners.

==============================================================================================*/

int16 EventManager::insertListener( EventListener* pHandler )
{
// TASK : Otimizar com controle de índice do usuário
//	if( nListeners < EVENTMANAGER_LISTENERS_ARRAY_LEN )
//	{
//		eventListeners[ nListeners++ ] = pHandler;
//		return true;
//	}
//	return false;
	
	// Procura um slot vazio
	int16 i = 0;
	for( ; i < EVENTMANAGER_LISTENERS_ARRAY_LEN ; ++i )
	{
		if( eventListeners[i] == NULL )
		{
			eventListeners[i] = pHandler;
			break;
		}
	}

	return i >= EVENTMANAGER_LISTENERS_ARRAY_LEN ? -1 : i;
}

/*==============================================================================================

MÉTODO removeListener
	Remove o listener recebido do array de listeners.

==============================================================================================*/

bool EventManager::removeListener( int16 listenerIndex )
{
// TASK : Otimizar com controle de índice do usuário
//	uint8 index = 0;
//	for( ; index < nListeners ; ++index )
//	{
//		if( eventListeners[index] == pListener )
//		{
//			--nListeners;
//			break;
//		}
//	}
//	
//	// "Chega pra cá"
//	while( index < nListeners )
//		eventListeners[index] = eventListeners[ ++index ];
	
	// Testa se o índice é válido
	if(( listenerIndex < 0 ) && ( listenerIndex >= EVENTMANAGER_LISTENERS_ARRAY_LEN ))
		return false;
	
	eventListeners[ listenerIndex ] = NULL;
	return true;
}

/*==============================================================================================

MÉTODO removeAllListeners
	Limpa o array de listeners.

==============================================================================================*/

void EventManager::removeAllListeners( void )
{
	// TASK : Otimizar com controle de índice do usuário
	//nListeners = 0;

	memset( eventListeners, 0, sizeof( EventListener* ) * EVENTMANAGER_LISTENERS_ARRAY_LEN );
}

/*==============================================================================================

MÉTODO getListener
	Retorna o listener de eventos desejado ou NULL caso este não exista.

==============================================================================================*/

EventListener* EventManager::getListener( int16 listenerIndex )
{
	if(( listenerIndex < 0 ) && ( listenerIndex >= EVENTMANAGER_LISTENERS_ARRAY_LEN ))
		return NULL;
	return eventListeners[ listenerIndex ];
}

/*==============================================================================================

MÉTODO setEvent
	Indica que um evento deve ser tratado pela aplicação. Retorna se algum EventListener tratou
o evento passado.

==============================================================================================*/

bool EventManager::setEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam ) const
{
// TASK : Otimizar com controle de índice do usuário
//	bool ret = false;
//
//	for( uint8 i = 0 ; i < nListeners ; ++i )
//	{
//		if( eventListeners[i]->isListeningTo( evtType ) )
//			ret |= eventListeners[i]->handleEvent( evtType, evtSpecific, pParam );
//	}
//
//	return ret;
	
	// TODOO : Eventos de toque devem ser mapeados com Utils::MapPointByOrientation
	// antes de serem repassados para os listeners!!!
	
	bool ret = false;

	for( int16 i = 0 ; i < EVENTMANAGER_LISTENERS_ARRAY_LEN ; ++i )
	{
		if( eventListeners[i] && eventListeners[i]->isListeningTo( evtType ) )
			ret |= eventListeners[i]->handleEvent( evtType, evtSpecific, pParam );
	}

	return ret;
}
