#include "EventListener.h"
#include "EventManager.h"
#include "Exceptions.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

EventListener::EventListener( EventTypes listeningTo ) : eventManagerIndex( -1 ), listeningTo( listeningTo )
{
	eventManagerIndex = EventManager::GetInstance()->insertListener( this );
	if( eventManagerIndex < 0 )
#if DEBUG
		throw ConstructorException( "EventListener::EventListener(): nao foi possivel criar o objeto" );
#else
		throw ConstructorException();
#endif
}
	
/*==============================================================================================

DESTRUTOR

==============================================================================================*/

EventListener::~EventListener( void )
{
	EventManager::GetInstance()->removeListener( eventManagerIndex );
}
