/*
 *  NOString.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 12/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_STRING_H
#define NO_STRING_H

// C++
#include <string>

#ifdef NANO_ONLINE_UNICODE_SUPPORT

	typedef std::wstring NOString;

	// TODOO
//	typedef std::wstring NOWideStrType;
//	typedef std::string NONarrowStrType;

#else

	typedef std::string NOString;

	// TODOO
//	typedef std::string NOWideStrType;
//	typedef std::string NONarrowStrType;

#endif

#endif
