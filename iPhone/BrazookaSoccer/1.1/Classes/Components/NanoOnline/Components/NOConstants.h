/*
 *  NOConstants.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/1/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef	NANO_ONLINE_CONSTANTS_H
#define NANO_ONLINE_CONSTANTS_H 1

// Endereço raiz do Nano Online
#if TARGET_IPHONE_SIMULATOR
	#define NANO_ONLINE_URL "http://localhost:3000"
#else
	#if DISTRIBUTION
		#define NANO_ONLINE_URL "http://online.nanogames.com.br"
	#else
		#define NANO_ONLINE_URL "http://192.168.1.8:3000"
	#endif
#endif

// Id de aplicativo dummy (usado para testes e debug)
#if DEBUG	
	#define NANO_ONLINE_ID_APP_DUMMY "DMMY"
#endif

// Versão do cliente Nano Online
#define NANO_ONLINE_CLIENT_VERSION "0.0.3"
	
// Os ids de chaves globais são negativos, para evitar possíveis confusões com ids específicos de
// um serviço (registro de usuário, ranking online, multiplayer, propagandas, etc.). Este devem
// utilizar ids positivos

#define NANO_ONLINE_ID_APP					-128		// Id de chave global: aplicativo/jogo. Tipo do valor: String
#define NANO_ONLINE_ID_APP_VERSION			-127		// Id de chave global: versão do aplicativo/jogo. Tipo do valor: string
#define NANO_ONLINE_ID_LANGUAGE				-126		// Id de chave global: idioma atual do aplicativo. Tipo do valor: byte
#define NANO_ONLINE_ID_NANO_ONLINE_VERSION	-125		// Id de chave global: versão do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string
#define NANO_ONLINE_ID_RETURN_CODE			-124		// Id de chave global: código de retorno. Tipo do valor: short
#define NANO_ONLINE_ID_CUSTOMER_ID			-123		// Id de chave global: usuário. Tipo do valor: int
#define NANO_ONLINE_ID_ERROR_MESSAGE		-122		// Id de chave global: mensagem de erro. Tipo do valor: string
#define NANO_ONLINE_ID_SPECIFIC_DATA		-121		// Id de chave global: início de dados específicos. Tipo do valor: byte[] (tamanho variável)

#define NANO_ONLINE_ID_MACOS_RAILS_BUG_FIX	-100		// Id de chave global: Byte extra enviado nas requisições POST feitas via MacOS e iPhoneOS. Sem fazer isso, o Rails ignora o último byte do corpo de uma mensagem quando seu valor é 0, causando bugs estranhos

// Códigos de erro. Assim como as chaves, os valores de retorno globais possuem valores negativos para
// que não haja confusão com os valores utilizados internamente em cada serviço
#define NANO_ONLINE_RC_OK						 0
#define NANO_ONLINE_RC_SERVER_INTERNAL_ERROR	-1
#define NANO_ONLINE_RC_DEVICE_NOT_SUPPORTED		-2
#define NANO_ONLINE_RC_APP_NOT_FOUND			-3


#endif
