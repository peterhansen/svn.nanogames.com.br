/*
 *  NanoOnlineManager.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 8/28/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_MANAGER_H
#define NANO_ONLINE_MANAGER_H 1

// Componentes
#include "INetworkListener.h"
#include "MemoryStream.h"
#include "NanoTypes.h"
#include "NOCustomer.h"
#include "NOMap.h"
#include "NetworkManager.h"

// C++
#include <map>
#include <string>

// Declarações adiadas
class INOListener;
class NORequestHolderBase;

class NOConnection : private INetworkListener
{
	public:
		// Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance(). No entanto, isso
		// poderia resultar em exceções disparadas no meio do código da aplicação, o que impactaria
		// o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
		// chamado no início da aplicação, já que o usuário dos componentes parte do princípio que
		// GetInstance() sempre retornará um valor válido 
		static bool Create( void );
		static bool Create( const std::string& host, INOListener *pListener = NULL );
		static bool Create( const std::string& appShortName, const std::string& appVersion, const std::string& host, INOListener *pListener = NULL );
	
		// Deleta o singleton
		static void Destroy( void );
	
		// Retorna a instância do objeto singleton
		static NOConnection* GetInstance( void ) { return pSingleton; };

		// Armazena o host da conexão
		void setHost( const std::string& host );
	
		// Determina o idioma do NanoOnline
		void setLanguage( int16 languageId );
	
		// Determina o identificador do usuário que está se conectando ao NanoOnline
		void setCustomer( NOProfileId customerId );
	
		// Determina o identificador da aplicação
		void setAppShortName( const std::string& name );
	
		// Determina a versão da aplicação
		void setAppVersion( const std::string& version );
	
		// Determina um objeto para receber e tratar os eventos do NanoOnline
		void setNOListener( INOListener* pListener );
	
		// Envia uma requisição para o NanoOnline. Deve existir um listener.
		bool sendRequest( const std::string& commandURL, const NORequestHolderBase& resquester );
	
		// Cancela a requisição pendente. Caso não haja uma requisição, não faz nada.
		void cancelPendingRequest( void );

	private:
		// Construtor
		NOConnection( void );
		NOConnection( const std::string& host, INOListener *pListener = NULL );
		NOConnection( const std::string& appShortName, const std::string& appVersion, const std::string& host, INOListener *pListener = NULL );
	
		// Destrutor
		virtual ~NOConnection( void );

		// Não queremos copiar objetos do tipo NOConnection
		NOConnection( const NOConnection& );
		NOConnection& operator=( const NOConnection& );
	
		// Inicializa o objeto
//		void build( void );
	
		// Libera a memória alocada pelo objeto
		void clean( void );

		// Envia o cabeçalho genérico de uma conexão ao NanoOnline
		void writeHeader( MemoryStream& output, NOProfileId customerId = CUSTOMER_PROFILE_ID_NONE );
	
		// Indica que ocorreu um erro na requisição
		virtual	void onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr );

		// Indica que a requisição terminou com sucesso
		virtual void onNetworkRequestCompleted( std::vector< uint8 >& data );
	
		// Indica que a requisição foi cancelada pelo usuário
		virtual void onNetworkRequestCancelled( void );

		// Identificação do usuário que está logado no NanoOnline
		NOProfileId currCustomerId;
	
		// Identificação do idioma utilizado no NanoOnline
		int16 languageId;
	
		// Objeto que está fazendo a requisição atual
		// OBS: Poderia virar um vector< NORequestHolderBase > (ou vector< shared_ptr< NORequestHolderBase > >) e termos várias requisições simultâneas
		NORequestHolderBase* pCurrRequester;
	
		// Objeto que irá receber os eventos de NanoOnlineManager
		INOListener* pNOListener;
	
		// Objeto responsável por tratar as requisições via internet
		NetworkManager netManager;
	
		// Identificador da aplicação
		std::string appShortName;
	
		// Versão da aplicação
		std::string appVersion;
	
		// Path do host
		std::string hostUrl;
	
		// Única instância da classe
		static NOConnection* pSingleton;
};

#endif
