#include "NORanking.h"

// Components
#if DEBUG
	#include "Macros.h"
#endif

#include "MemoryStream.h"
#include "NOConnection.h"
#include "NOConstants.h"
#include "NORequestHolder.h"

// Typedefs para deixar o código mais claro
typedef NORanking::NOEntriesContainer::iterator EntriesIt;
typedef NORanking::NOEntriesContainer::const_iterator EntriesConstIt;

// Parâmetros utilizados quando estamos submetendo um ranking
#define NANO_ONLINE_RANKING_PARAM_N_ENTRIES		0
#define NANO_ONLINE_RANKING_PARAM_SUB_TYPE		1

// Parâmetros utilizados quando estamos pedindo para o NanoOnline as melhores pontuações de um ranking
#define NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES	2
#define NANO_ONLINE_RANKING_PARAM_END				3
#define NANO_ONLINE_RANKING_PARAM_LIMIT				4
#define NANO_ONLINE_RANKING_PARAM_OFFSET			5

/*==============================================================================================

FUNÇÕES AUXILIARES

===============================================================================================*/

static bool rnkSortDesc( const NORankingEntry& a, const NORankingEntry& b )
{
	int64 scoreA, scoreB;
	return a.getScore( scoreA ) > b.getScore( scoreB );
}

static bool rnkSortAsc( const NORankingEntry& a, const NORankingEntry& b )
{
	int64 scoreA, scoreB;
	return a.getScore( scoreA ) < b.getScore( scoreB );
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

NORanking::NORanking( const int64& maxPoints )
		  : maxPoints( maxPoints ), rnkType( 0 ), decrescent( false ),
			onlineMaxEntries( NANO_ONLINE_RANKING_DEFAULT_MAX_ONLINE_ENTRIES ),
			highlightedProfilesIds(), localEntries(), onlineEntries()
{
}

/*==============================================================================================

CONSTRUTOR DE CÓPIA

===============================================================================================*/

NORanking::NORanking( const NORanking& other )
		  : maxPoints( other.maxPoints ), rnkType( other.rnkType ), decrescent( other.decrescent ),
			onlineMaxEntries( other.onlineMaxEntries ),
			highlightedProfilesIds( other.highlightedProfilesIds ), localEntries( other.localEntries ),
			onlineEntries( other.onlineEntries )
{
}

/*==============================================================================================

DESTRUTOR

===============================================================================================*/

NORanking::~NORanking( void )
{
}

/*==============================================================================================

OPERADOR =

===============================================================================================*/

NORanking& NORanking::operator=( const NORanking& other )
{
	maxPoints = other.maxPoints;
	rnkType = other.rnkType;
	decrescent = other.decrescent;
	onlineMaxEntries = other.onlineMaxEntries;

	highlightedProfilesIds.clear();
	std::copy( other.highlightedProfilesIds.begin(), other.highlightedProfilesIds.end(), highlightedProfilesIds.end() );
			  
	localEntries.clear();
	std::copy( other.localEntries.begin(), other.localEntries.end(), localEntries.end() );
			  
	onlineEntries.clear();
	std::copy( other.onlineEntries.begin(), other.onlineEntries.end(), onlineEntries.end() );
	
	return *this;
}

/*==============================================================================================

MÉTODO serialize
	Lê o objeto de uma stream.

===============================================================================================*/

void NORanking::serialize( MemoryStream& stream ) const
{
	// Escreve o número de entradas do ranking local
	stream.writeInt8( static_cast< int8 >( localEntries.size() ) );
	
	// Escreve as entradas do ranking local
	EntriesConstIt end = localEntries.end();
	for( EntriesConstIt i = localEntries.begin() ; i != end ; ++i )
		i->serialize( stream );
	
	// Escreve o número de entradas do ranking online
	stream.writeInt8( static_cast< int8 >( onlineEntries.size() ) );
	
	// Escreve as entradas do ranking online
	end = onlineEntries.end();
	for( EntriesConstIt i = onlineEntries.begin() ; i != end ; ++i )
		i->serialize( stream );
}

/*==============================================================================================

MÉTODO unserialize
	Escre o objeto em uma stream.

===============================================================================================*/

void NORanking::unserialize( MemoryStream& stream )
{
	// Lê o número de entradas do ranking local
	int8 nEntries = stream.readInt8();
	
	// Lê as entradas do ranking local
	for( int8 i = 0 ; i < nEntries ; ++i )
	{
		NORankingEntry aux;
		aux.unserialize( stream );
		localEntries.push_back( aux );
	}
	
	// Lê o número de entradas do ranking online
	nEntries = stream.readInt8();
	
	// Lê as entradas do ranking online
	for( int8 i = 0 ; i < nEntries ; ++i )
	{
		NORankingEntry aux;
		aux.unserialize( stream );
		onlineEntries.push_back( aux );
	}
}

/*==============================================================================================

MÉTODO getBestTrio
	Retorna as 3 melhores pontuações existentes no NanoOnline para este ranking.

===============================================================================================*/

void NORanking::getBestTrio( NOEntriesContainer& out ) const
{
	// Logo após o lançamento de um jogo, é possível que ainda não tenhamos 3 recordes...
	size_t nOnlineEntries = onlineEntries.size();
	size_t max = nOnlineEntries < 3 ? nOnlineEntries : 3;
	
	for( size_t i = 0 ; i < max ; ++i )
		out.push_back( onlineEntries[ i ] );
	
	for( size_t i = max ; i < 3 ; ++i )
		out.push_back( NORankingEntry() );
}

/*==============================================================================================

MÉTODO ESTÁTICO SendHighScorestRequest

===============================================================================================*/

bool NORanking::SendHighScorestRequest( NORanking* pRanking, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/ranking_entries/high_scores", MakeNORequestHolder( *pRanking, &NORanking::highScores, &NORanking::submitScoresResponse ) );
}

/*==============================================================================================

MÉTODO highScores
	Pede as melhores pontuações do rankingOnline.

===============================================================================================*/

bool NORanking::highScores( MemoryStream& stream )
{
	// Número de entradas que estamos enviando. Apenas para manter o protocolo
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_N_ENTRIES );
	stream.writeInt16( 0 );
	
	// Tipo do ranking. Caso seja um ranking crescente, envia um valor negativo para indicá-lo
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_SUB_TYPE );
	stream.writeInt8( isDecrescent() ? rnkType : -rnkType );
	
	// Indica de quais perfis desejamos saber as pontuações
	if( !highlightedProfilesIds.empty() )
	{
		stream.writeInt8( NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES );
		
		int32 nProfiles = static_cast< int32 >( highlightedProfilesIds.size() );
		stream.writeInt16( static_cast< int16 >( nProfiles ) );
		
		while( --nProfiles >= 0 )
			stream.writeInt32( highlightedProfilesIds[ nProfiles ] );
	}

	// Número de entradas que queremos receber de volta
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_LIMIT );
	stream.writeInt16( onlineMaxEntries );

	// Queremos sempre os "onlineMaxEntries" 1os, então indicamos um offset nulo
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_OFFSET );
	stream.writeInt32( 0 );

	// Fim da requisição
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_END );

	return true;
}

/*==============================================================================================

MÉTODO ESTÁTICO SendSubmitRequest

===============================================================================================*/

bool NORanking::SendSubmitRequest( NORanking* pRanking, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/ranking_entries/submit", MakeNORequestHolder( *pRanking, &NORanking::submitScores, &NORanking::submitScoresResponse ) );
}

/*==============================================================================================

MÉTODO submitScores
	Envia a pontuação do jogador para o NanoOnline.

===============================================================================================*/

bool NORanking::submitScores( MemoryStream& stream )
{
	// Número de entradas que estamos enviando
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_N_ENTRIES );
	stream.writeInt16( static_cast< int16 >( localEntries.size() ) );

	// Tipo do ranking. Caso seja um ranking crescente, envia um valor negativo para indicá-lo
	stream.writeInt8( NANO_ONLINE_RANKING_PARAM_SUB_TYPE );
	stream.writeInt8( isDecrescent() ? rnkType : -rnkType );

	#if DEBUG
		LOG( "NORanking::submitScores >>> Pontuações a serem enviadas:\n\n" );
	#endif

	int64 score, timeStamp;
	EntriesConstIt end = localEntries.end();
	for( EntriesConstIt i = localEntries.begin() ; i != end ; ++i )
	{
		stream.writeInt32( i->getProfileId() );
		stream.writeInt64( i->getScore( score ) );
		stream.writeInt64( i->getTimeStamp( timeStamp ) );

		#if DEBUG
			struct tm decodedTimeStamp;
			memset( &decodedTimeStamp, 0, sizeof( struct tm ));
			time_t aux = static_cast< time_t >( timeStamp );
			gmtime_r( &aux, &decodedTimeStamp );

			LOG( "NORanking::submitScores >>> Jogador %04d, Pontuação %04lld feita em %04d/%02d/%02d - %02d:%02d:%02d\n", i->getProfileId(), score, decodedTimeStamp.tm_year + 1900, decodedTimeStamp.tm_mon + 1, decodedTimeStamp.tm_mday, decodedTimeStamp.tm_hour, decodedTimeStamp.tm_min, decodedTimeStamp.tm_sec );
		#endif
	}
	
	#if DEBUG
		LOG( "\n" );
	#endif

	return true;
}

/*==============================================================================================

MÉTODO submitScoresResponse
	Trata a resposta do envio da pontuação do jogador para o NanoOnline.

===============================================================================================*/

bool NORanking::submitScoresResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			{
				// Lê o ranking online enviado como resposta
				std::vector< uint8 >* pSpecificData = static_cast< std::vector< uint8 >* >( table[ NANO_ONLINE_ID_SPECIFIC_DATA ].get() );
				
				MemoryStream dataStream;
				dataStream.swapBuffer( *pSpecificData );
				
				return readSubmitResponseData( dataStream, errorStr );
			}
			return true;
			
		default:
			#if DEBUG
				assert_n_log( false, "Unrecognized param return code sent to createProfileResponse in response to createProfile request" );
			#else
				errorStr = L"Unknown error";
			#endif
			return false;
	}
	
	return false;
}

/*==============================================================================================

MÉTODO readSubmitResponseData
	Lê as entradas do ranking enviadas pelo NanoOnline.

===============================================================================================*/

bool NORanking::readSubmitResponseData( MemoryStream& dataStream, NOString& errorStr )
{
	onlineEntries.clear();

	while( !dataStream.eos() )
	{
		int8 id = dataStream.readInt8();

		#if DEBUG
			LOG( ">>> NORanking::readSubmitResponseData => Read attrib id = %d\n", static_cast< int32 >( id ) );
		#endif
		
		if( id == NANO_ONLINE_RANKING_PARAM_END )
			break;

		switch( id )
		{
			case NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES:
			case NANO_ONLINE_RANKING_PARAM_N_ENTRIES:
				{
					int16 totalEntries = dataStream.readInt16();

					if( totalEntries > 0 )
					{
						int32 tempId;
						int64 tempScore;
						NOString tempNick;

						#if DEBUG
							LOG( "\n\nNORanking::readSubmitResponseData\n\n" );
						#endif

						for( int16 i = 0 ; i < totalEntries ; ++i )
						{
							tempId = dataStream.readInt32();
							dataStream.readInt64( tempScore );
							
							#ifdef NANO_ONLINE_UNICODE_SUPPORT
								dataStream.readUTF32String( tempNick );
							#else
								dataStream.readUTF8String( tempNick );
							#endif
							
							#if DEBUG
								#ifdef NANO_ONLINE_UNICODE_SUPPORT
									LOG( "User %ls (id = %d ) => %lld points\n", tempNick.c_str(), tempId, tempScore );
								#else
									LOG( "User %s (id = %d ) => %lld points\n", tempNick.c_str(), tempId, tempScore );
								#endif
							#endif
							
							onlineEntries.push_back( NORankingEntry( tempId, tempScore, tempNick, id == NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES ? dataStream.readInt32() : i ) );
						}
						#if DEBUG
							LOG( "\n" );
						#endif
					}
				}
				break;

			// Não sabe ler o campo, então ignora
			default:
				#if DEBUG
					assert_n_log( false, ">>> NORanking::readSubmitResponseData => Unknown parameter" );
				#endif
				break;
		}
	}
	
	// Ordena os recordes
	sort();
	
	return true;
}

/*==============================================================================================

MÉTODO sort
	Ordena as entradas do ranking de acordo com a configuração do mesmo.

===============================================================================================*/

void NORanking::sort( void )
{
	std::sort( onlineEntries.begin(), onlineEntries.end(), decrescent ? rnkSortDesc : rnkSortAsc );
}

/*==============================================================================================

MÉTODO print
	Exibe o conteúdo do ranking no console.

===============================================================================================*/

#if DEBUG

void NORanking::print( void )
{
	LOG( "\n\nNORanking::print\n\n" );

	int32 tempId;
	int64 tempScore;
	NOString tempNick;

	for( size_t i = 0 ; i < localEntries.size() ; ++i )
	{
		tempId = localEntries[i].getProfileId();
		localEntries[i].getScore( tempScore );
		localEntries[i].getNickname( tempNick );
		
		#ifdef NANO_ONLINE_UNICODE_SUPPORT
			LOG( "User %ls (id = %05d ) => %06lld points\n", tempNick.c_str(), tempId, tempScore );
		#else
			LOG( "User %s (id = %05d ) => %06lld points\n", tempNick.c_str(), tempId, tempScore );
		#endif
	}
	
	LOG( "\n" );
	
	for( size_t i = 0 ; i < onlineEntries.size() ; ++i )
	{
		tempId = onlineEntries[i].getProfileId();
		onlineEntries[i].getScore( tempScore );
		onlineEntries[i].getNickname( tempNick );
		
		#ifdef NANO_ONLINE_UNICODE_SUPPORT
			LOG( "User %ls (id = %05d ) => %06lld points\n", tempNick.c_str(), tempId, tempScore );
		#else
			LOG( "User %s (id = %05d ) => %06lld points\n", tempNick.c_str(), tempId, tempScore );
		#endif
	}

	LOG( "\n" );
}

#endif
