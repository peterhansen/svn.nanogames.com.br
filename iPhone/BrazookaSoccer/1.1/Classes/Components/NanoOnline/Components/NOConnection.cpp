#include "NOConnection.h"

// Componentes
#include "DeviceInterface.h"
#include "INOListener.h"
#include "Macros.h"
#include "NOConstants.h"
#include "NORequestHolder.h"
#include "Utils.h"

// C++
#include <bits/stl_pair.h>
#include <vector>

// Macros de desalocação de objetos CF*
#define CFKILL( p ) { CFRelease( p ); p = NULL; }
#define SAFE_CFKILL( p ) { if( p != NULL )CFKILL( p ); }

// Assinatura dos pacotes enviados ao NanoOnline
#define NANO_ONLINE_SIGNATURE_LEN 9
#define NANO_ONLINE_SIGNATURE { 137, 110, 97, 110, 111, 13, 10, 26, 10 }

// TODOO : Colocar estes typedefs em NanoTypes.h???
typedef boost::shared_ptr< char > CStrPtr;
typedef boost::shared_ptr< NOString > StrPtr;
typedef boost::shared_ptr< uint8 > UInt8Ptr;
typedef boost::shared_ptr< int16 > Int16Ptr;
typedef boost::shared_ptr< int32 > Int32Ptr;
typedef boost::shared_ptr< int64 > Int64Ptr;
typedef boost::shared_ptr< std::vector< uint8 > > ByteVectPtr;

// Inicializa as variáveis estáticas da classe
NOConnection* NOConnection::pSingleton = NULL;

/*==============================================================================================

FUNÇÃO GetVersion
	Retorna a versão do cliente NanoOnline.

===============================================================================================*/

std::string* GetNanoOnlineClientVersion( std::string* pVersion )
{
	pVersion->append( NANO_ONLINE_CLIENT_VERSION );
	return pVersion;
}

/*==============================================================================================

FUNÇÃO GetDeviceUserAgent
	Retorna o user agent que utilizaremos nas conexões.

===============================================================================================*/

CFStringRef GetDeviceUserAgent( void )
{ 
	std::string userAgentBuffer;

	DeviceInterface::GetDeviceModel( userAgentBuffer, false );
	userAgentBuffer.append( " - " );

	DeviceInterface::GetDeviceSystemName( userAgentBuffer, true );
	userAgentBuffer.append( " " );

	DeviceInterface::GetDeviceSystemVersion( userAgentBuffer, true );
	userAgentBuffer.append( " - " );

	DeviceInterface::GetDeviceUID( userAgentBuffer, true );

	#if DEBUG
		LOG( ">>> NanoOnline - User Agent: %s\n", userAgentBuffer.c_str() );
	#endif

	return CFStringCreateWithCString( kCFAllocatorDefault, userAgentBuffer.c_str(), kCFStringEncodingUTF8 );
}

/*==============================================================================================

FUNÇÃO ReadHeader
	Recebe o cabeçalho genérico de uma conexão ao NanoOnline.

==============================================================================================*/

NOMap& ReadHeader( NOMap& table, MemoryStream& input )
{
	#if DEBUG
		LOG( ">>> NOConnection => Parsing response\n" );
	#endif
	
	NOMap tempTable;

	bool ret = true;
	while( !input.eos() && ret )
	{
		int8 id = input.readInt8();

		#if DEBUG
			LOG( ">>> NOConnection => Leu ID: %d\n", static_cast< int32 >( id ) );
		#endif

		switch( id )
		{
			case NANO_ONLINE_ID_APP:
			case NANO_ONLINE_ID_RETURN_CODE:
				{
					Int16Ptr pShort( new int16( input.readInt16() ) );
					if( pShort == NULL )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pShort ) );
						
						#if DEBUG
							LOG( ">>> NOConnection => Read int16: %d\n\n", *pShort );
						#endif
					}
				}
				break;

			case NANO_ONLINE_ID_CUSTOMER_ID:
				{
					Int32Ptr pInteger( new int32( input.readInt32() ) );
					if( pInteger == NULL )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pInteger ) );
						
						#if DEBUG
							LOG( ">>> NOConnection => Read int32: %d\n\n", *pInteger );
						#endif
					}
				}
				break;

			case NANO_ONLINE_ID_LANGUAGE:
				{
					UInt8Ptr pByte( new uint8( static_cast< uint8 >( input.readInt8() ) ) );
					if( pByte == NULL )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pByte ) );
						
						#if DEBUG
							LOG( ">>> NOConnection => Read byte: %d\n\n", *pByte );
						#endif
					}
				}
				break;

			case NANO_ONLINE_ID_APP_VERSION:
			case NANO_ONLINE_ID_NANO_ONLINE_VERSION:
			case NANO_ONLINE_ID_ERROR_MESSAGE:
				{
					#ifdef NANO_ONLINE_UNICODE_SUPPORT
					StrPtr pStr( new NOString() );
					if( ( pStr == NULL ) || ( input.readUTF32String( *pStr ).empty() ) )
					#else
					StrPtr pStr( new std::string() );
					if( ( pStr == NULL ) || ( input.readUTF8String( *pStr ).empty() ) )
					#endif
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pStr ) );
						
						#if DEBUG
							#ifdef NANO_ONLINE_UNICODE_SUPPORT
								LOG( ">>> NOConnection => Read string: %ls\n\n", pStr->c_str() );
							#else
								LOG( ">>> NOConnection => Read string: %s\n\n", pStr->c_str() );
							#endif
						#endif
					}
				}
				break;

			case NANO_ONLINE_ID_SPECIFIC_DATA:
				{
					ByteVectPtr pSpecificData( new std::vector< uint8 >() );
					if( ( pSpecificData == NULL ) || ( input.readData( *pSpecificData ).size() == 0 ) )
					{
						ret = false;
					}
					else
					{
						tempTable.insert( std::make_pair( id, pSpecificData ) );
						
						#if DEBUG
							LOG( ">>> NOConnection => Read byte array\n\n" );
						#endif
					}
				}
				break;

		
			default:
				#if DEBUG
					LOG( "%s\n", input.begin() );
					assert_n_log( false, ">>> NOConnection => ReadHeader( NOMap&, MemoryStream& ): Unknown field id in response header" );
				#else
					ret = false;
				#endif
				break;
		
		}
	}
	
	if( ret )
		table = tempTable;
	else
		table.clear();
	
	return table;
}

/*==============================================================================================

MÉTODO Create
	 Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance().
No entanto, isso poderia resultar em exceções disparadas no meio do código da aplicação, o que
impactaria o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
chamado no início da aplicação, já que o usuário dos componentes parte do princípio que
GetInstance() sempre retornará um valor válido.

==============================================================================================*/
 
bool NOConnection::Create( void )
{
	if( !pSingleton )
		pSingleton = new NOConnection();
	return pSingleton != NULL;
}

bool NOConnection::Create( const std::string& host, INOListener *pListener )
{
	if( !pSingleton )
		pSingleton = new NOConnection( host, pListener );
	return pSingleton != NULL;
}

bool NOConnection::Create( const std::string& appShortName, const std::string& appVersion, const std::string& host, INOListener *pListener )
{
	if( !pSingleton )
		pSingleton = new NOConnection( appShortName, appVersion, host, pListener );
	return pSingleton != NULL;
}

/*==============================================================================================

MÉTODO Destroy
	Deleta o singleton.

==============================================================================================*/

void NOConnection::Destroy( void )
{
	DELETE( pSingleton );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

NOConnection::NOConnection( void )
				 : INetworkListener(),
				   currCustomerId( CUSTOMER_PROFILE_ID_NONE ),
				   languageId( -1 ),
				   pCurrRequester( NULL ),
				   pNOListener( NULL ),
				   netManager( this ),
				   appShortName(),
				   appVersion()
{
//	build();
	setHost( "" );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

NOConnection::NOConnection( const std::string& host, INOListener *pListener )
				 : INetworkListener(),
				   currCustomerId( CUSTOMER_PROFILE_ID_NONE ),
				   languageId( -1 ),
				   pCurrRequester( NULL ),
				   pNOListener( pListener ),
				   netManager( this ),
				   appShortName(),
				   appVersion()
{	
//	build();
	setHost( host );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

NOConnection::NOConnection( const std::string& appShortName, const std::string& appVersion, const std::string& host, INOListener *pListener )
				 : INetworkListener(),
				   currCustomerId( CUSTOMER_PROFILE_ID_NONE ),
				   languageId( -1 ),
				   pCurrRequester( NULL ),
				   pNOListener( pListener ),
				   netManager( this ),
				   appShortName( appShortName ),
				   appVersion( appVersion )
{
//	build();
	setHost( host );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

NOConnection::~NOConnection( void )
{
	clean();
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

===============================================================================================*/

//void NOConnection::build( void )
//{
//}

/*==============================================================================================

MÉTODO clean
	Libera a memória alocada pelo objeto.

==============================================================================================*/

void NOConnection::clean( void )
{
	DELETE( pCurrRequester );
}

/*==============================================================================================

MÉTODO setHost
	Armazena o host da conexão.

==============================================================================================*/

void NOConnection::setHost( const std::string& host )
{
	hostUrl = host;
}

/*==============================================================================================

MÉTODO setLanguage
	Determina o idioma do NanoOnline.

==============================================================================================*/

void NOConnection::setLanguage( int16 languageId )
{
	this->languageId = languageId;
}

/*==============================================================================================

MÉTODO setCustomer
	Determina o identificador do usuário que está se conectando ao NanoOnline.

==============================================================================================*/

void NOConnection::setCustomer( NOProfileId customerId )
{
	currCustomerId = customerId;
}

/*==============================================================================================

MÉTODO setAppShortName
	Determina o identificador da aplicação.

==============================================================================================*/

void NOConnection::setAppShortName( const std::string& name )
{
	appShortName = name;
}

/*==============================================================================================

MÉTODO setAppVersion
	Determina a versão da aplicação.

==============================================================================================*/

void NOConnection::setAppVersion( const std::string& version )
{
	appVersion = version;
}

/*==============================================================================================

MÉTODO setNOListener
	Determina um objeto para receber e tratar os eventos do NanoOnline.

==============================================================================================*/

void NOConnection::setNOListener( INOListener* pListener )
{
	pNOListener = pListener;
}

/*==============================================================================================

MÉTODO sendScore
	Envia uma requisição para o NanoOnline.

==============================================================================================*/

bool NOConnection::sendRequest( const std::string& commandURL, const NORequestHolderBase& resquester )
{
	#if DEBUG
		assert_n_log( ( pNOListener != NULL ), ">>> NOConnection => There must be a listener set for a request to be sent" );
	#endif
	
	// Mata uma request pendente e armazena a nova
	clean();
	pCurrRequester = resquester.clone();

	// Cria a requisição HTTP
	bool ret = false;
	
	CFURLRef pUrl = NULL;
	CFDataRef pBodyData = NULL;
	CFHTTPMessageRef pRequest = NULL;

	if( !hostUrl.empty() )
	{
		#if DEBUG
			LOG( ">>> NOConnection: Sending request command %s\n", ( hostUrl + commandURL ).c_str() );
		#endif

		// Monta a URL
		CFStringRef pUrlStrAux = CFStringCreateWithCString( kCFAllocatorDefault, ( hostUrl + commandURL ).c_str(), kCFStringEncodingUTF8 );
		if( !pUrlStrAux )
			goto End;

		pUrl = CFURLCreateWithString( kCFAllocatorDefault, pUrlStrAux, NULL );
		CFKILL( pUrlStrAux );
		if( !pUrl )
			goto End;

		// Cria a requisição
		pRequest = CFHTTPMessageCreateRequest( kCFAllocatorDefault, CFSTR( "POST" ), pUrl, kCFHTTPVersion1_1 );
		if( !pRequest )
			goto End;

		CFKILL( pUrl );
		
		// Determina o user agent
		CFStringRef pUserAgent = GetDeviceUserAgent();
		if( !pUserAgent )
		{
			CFKILL( pRequest );
			goto End;
		}
		CFHTTPMessageSetHeaderFieldValue( pRequest, CFSTR( "User-Agent" ), pUserAgent );
		CFKILL( pUserAgent );
		
		// Determina o corpo da mensagem (geralmente para requisições POST)
		MemoryStream nanoOnlineData;
		writeHeader( nanoOnlineData );

		if( !pCurrRequester->onRequest( nanoOnlineData ) )
		{
			CFKILL( pRequest );
			goto End;
		}
			
		nanoOnlineData.writeInt8( NANO_ONLINE_ID_MACOS_RAILS_BUG_FIX );

		#if DEBUG
			LOG( ">>> NanoOnline Data Size %d\n", nanoOnlineData.getSize() );
		#endif

		pBodyData = CFDataCreate( kCFAllocatorDefault, nanoOnlineData.begin(), nanoOnlineData.getSize() );
		if( !pBodyData )
		{
			CFKILL( pRequest );
			goto End;
		}
			
		// Já criamos o corpo da mensagem, então podemos esvaziar a stream
		nanoOnlineData.clear();

		CFHTTPMessageSetBody( pRequest, pBodyData );
		CFKILL( pBodyData );

		// Envia a requisição
		ret = netManager.sendRequest( pRequest );
		if( !ret )
		{
			CFKILL( pRequest );
		}
		else
		{
			pNOListener->onNORequestSent();
		}
	}

	// Desaloca os objetos criados dinamicamente pelo método
	End:
		SAFE_CFKILL( pBodyData );
		SAFE_CFKILL( pUrl );
	
		// OBS: NÃO É PARA DESTRUIRMOS pRequest AQUI!!! SE CONSEGUIMOS ENVIAR A REQUISIÇÃO, netManager SERÁ 
		// O ENCARREGADO DE FAZÊ-LO !!!
		//SAFE_CFKILL( pRequest );

		return ret;
}

/*==============================================================================================

MÉTODO writeHeader
	Envia o cabeçalho genérico de uma conexão ao NanoOnline.

==============================================================================================*/

void NOConnection::writeHeader( MemoryStream& output, NOProfileId customerId )
{
	// Escreve a assinatura dos pacotes
	uint8 signature[ NANO_ONLINE_SIGNATURE_LEN ] = NANO_ONLINE_SIGNATURE;
	output.writeData( signature, NANO_ONLINE_SIGNATURE_LEN );

	// Escreve a versão do cliente
	output.writeInt8( NANO_ONLINE_ID_NANO_ONLINE_VERSION );
	
	#if DEBUG
		LOG( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_NANO_ONLINE_VERSION: %d\n", NANO_ONLINE_ID_NANO_ONLINE_VERSION );
	#endif
	
	std::string clientVersion;
	output.writeUTF8String( GetNanoOnlineClientVersion( &clientVersion )->c_str() );
	
	#if DEBUG
		LOG( "Value: %s\n\n", clientVersion.c_str() );
	#endif

	// Escreve o identificador da aplicação
	output.writeInt8( NANO_ONLINE_ID_APP );
	output.writeUTF8String( appShortName.c_str() );
	
	#if DEBUG
		LOG( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_APP: %d\nValue: %s\n\n", NANO_ONLINE_ID_APP, appShortName.c_str() );
	#endif

	// Escreve o identificador do usuário
	output.writeInt8( NANO_ONLINE_ID_CUSTOMER_ID );
	output.writeInt32( customerId );
	
	#if DEBUG
		LOG( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_CUSTOMER_ID: %d\nValue: %d\n\n", NANO_ONLINE_ID_CUSTOMER_ID, customerId );
	#endif

	// Escreve o identificador do idioma
	output.writeInt8( NANO_ONLINE_ID_LANGUAGE );
	output.writeInt8( languageId );
	
	#if DEBUG
		LOG( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_LANGUAGE: %d\nValue: %d\n\n", NANO_ONLINE_ID_LANGUAGE, languageId );
	#endif

	// Escreve a versão da aplicação
	output.writeInt8( NANO_ONLINE_ID_APP_VERSION );
	output.writeUTF8String( appVersion.c_str() );
	
	#if DEBUG
		LOG( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_APP_VERSION: %d\nValue: %s\n\n", NANO_ONLINE_ID_APP_VERSION, appVersion.c_str() );
	#endif

	// Escreve o identificador dos dados específicos
	output.writeInt8( NANO_ONLINE_ID_SPECIFIC_DATA );
	
	#if DEBUG
		LOG( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_SPECIFIC_DATA: %d\nValue: Empty\n\n", NANO_ONLINE_ID_SPECIFIC_DATA );
	#endif
}

/*==============================================================================================

MÉTODO cancelPendingRequest
	Cancela a requisição pendente. Caso não haja uma requisição, não faz nada.

==============================================================================================*/

void NOConnection::cancelPendingRequest( void )
{
	netManager.cancelPendingRequest();
}

/*==============================================================================================

MÉTODO onNetworkError
	Indica que ocorreu um erro na requisição.

==============================================================================================*/

void NOConnection::onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr )
{
	#if DEBUG
		LOG( ">>> NOConnection => Network Error: %s\n", errorStr.c_str() );
	#endif
	
	NOString aux;
	Utils::StringToWString( errorStr, aux );

	pNOListener->onNOError( NO_ERROR_NETWORK_ERROR, aux );
	
	clean();
}

/*==============================================================================================

MÉTODO onNetworkRequestCompleted
	Indica que a requisição terminou com sucesso.

==============================================================================================*/

void NOConnection::onNetworkRequestCompleted( std::vector< uint8 >& data )
{
	#if DEBUG
		LOG( ">>> NOConnection => Request Completed\n" );
	#endif

	// OBS: Esta chamada invalida os dados do parâmetro 'data'. No entanto, otimizamos o consumo de memória
	MemoryStream dataStream;
	dataStream.swapBuffer( data );

	// Faz o parse dos dados
	NOMap table;
	ReadHeader( table, dataStream );
	dataStream.clear();
	
	if( table.size() == 0 )
	{
		NOString error( L"Invalid Response Format" );
		pNOListener->onNOError( NO_ERROR_INVALID_RESPONSE_FORMAT, error );
	}
	else if( table.find( NANO_ONLINE_ID_ERROR_MESSAGE ) != table.end() )
	{
		NOString error = *static_cast< const NOString* >( table[ NANO_ONLINE_ID_ERROR_MESSAGE ].get() );
		pNOListener->onNOError( NO_ERROR_INVALID_DATA_SUBMITED, error );
	}
	else
	{
		int16 returnCode = NANO_ONLINE_RC_OK;
		if( table.find( NANO_ONLINE_ID_RETURN_CODE ) != table.end() )
			returnCode = *static_cast< const int16* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() );
		
		if( returnCode < NANO_ONLINE_RC_OK )
		{
			NOString error;
			switch( returnCode )
			{
				case NANO_ONLINE_RC_DEVICE_NOT_SUPPORTED:
					error = L"Device not supported";
					break;

				case NANO_ONLINE_RC_APP_NOT_FOUND:
					error = L"This application is no longer supported";
					break;
					
				case NANO_ONLINE_RC_SERVER_INTERNAL_ERROR:
				default:
					error = L"Server internal error";
					break;
			}
			pNOListener->onNOError( NO_ERROR_INTERNAL, error );
		}
		else
		{		
			NOString error;
			if( pCurrRequester->onResponse( table, error ) )
				pNOListener->onNOSuccessfulResponse();
			else
				pNOListener->onNOError( NO_ERROR_INVALID_RESPONSE_FORMAT, error );
		}
	}

	clean();
}

/*==============================================================================================

MÉTODO onNetworkRequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

==============================================================================================*/

void NOConnection::onNetworkRequestCancelled( void )
{
	#if DEBUG
		LOG( ">>> NOConnection => Request Cancelled\n" );
	#endif

	pNOListener->onNORequestCancelled();
	clean();
}
