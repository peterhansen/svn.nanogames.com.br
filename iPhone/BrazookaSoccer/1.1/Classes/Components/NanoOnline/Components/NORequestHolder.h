/*
 *  NORequestHolder.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/14/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_REQUEST_HOLDER_H
#define NANO_ONLINE_REQUEST_HOLDER_H 1

// Components
#include "MemoryStream.h"
#include "NOMap.h"
#include "NOString.h"

class NORequestHolderBase
{
	public:
		virtual ~NORequestHolderBase() = 0;

		virtual bool onRequest( MemoryStream& stream ) const = 0;
		virtual bool onResponse( NOMap& map, NOString& errorStr ) const = 0;
	
		virtual NORequestHolderBase* clone( void ) const = 0;
};

inline NORequestHolderBase::~NORequestHolderBase()
{
}

// TODOO : Parametrizar o tipo de ponteiro também. Assim também poderemos utilizar smart pointers ao invés de apenas ponteiros "crus"

template< typename T >
class NORequestHolder : public NORequestHolderBase
{
	public:
		// Método que preenche a parte específica de uma requisição a ser enviada para o NanoOnline
		typedef bool ( T::*NORequestHandler )( MemoryStream& stream );

		// Método que trata a resposta de uma requisição feita ao NanoOnline
		typedef bool ( T::*NOResponseHandler )( NOMap& map, NOString& errorStr );
	
		NORequestHolder( void ) : pObject( NULL ), requestHandler( NULL ), responseHandler( NULL )
		{
		};

		NORequestHolder( T& obj, NORequestHandler request, NOResponseHandler response ) : pObject( &obj ), requestHandler( request ), responseHandler( response )
		{
		};
	
		virtual ~NORequestHolder()
		{
		};

		virtual bool onRequest( MemoryStream& stream ) const
		{
			if( pObject != NULL )
				return ( pObject->*requestHandler )( stream );
			return false;
		};

		virtual bool onResponse( NOMap& table, NOString& errorStr ) const
		{
			if( pObject != NULL )
				return ( pObject->*responseHandler )( table, errorStr );
			return false;
		};

		virtual NORequestHolderBase* clone( void ) const
		{
			if( pObject != NULL )
				return new NORequestHolder< T >( *pObject, requestHandler, responseHandler );
			return new NORequestHolder< T >();
		};

	private:
		T* pObject;
		NORequestHandler requestHandler;
		NOResponseHandler responseHandler;
};

template< typename T > inline NORequestHolder< T > MakeNORequestHolder( T& t, bool ( T::*request )( MemoryStream& ), bool ( T::*response )( NOMap&, NOString& ) )
{
	return NORequestHolder< T >( t, request, response );
}

#endif
