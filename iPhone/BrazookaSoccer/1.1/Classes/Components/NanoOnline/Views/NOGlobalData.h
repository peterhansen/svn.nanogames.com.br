/*
 *  NOGlobalData.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 12/18/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NO_GLOBAL_DATA_H
#define NO_GLOBAL_DATA_H

// Declarações adiadas
class NOConf;
class NOCustomer;
class NORanking;

// Essa classe contém os dados que devem existir durante qualquer momento da aplicação que contém o NanoOnline.
// Para isso, funciona como uma espécie de Singleton, utilizando variáveis estáticas
class NOGlobalData
{
	public:
		static NOCustomer& GetActiveProfile( void );
		static NORanking& GetRanking( void );
		static NOConf& GetConfig( void );
};

#endif
