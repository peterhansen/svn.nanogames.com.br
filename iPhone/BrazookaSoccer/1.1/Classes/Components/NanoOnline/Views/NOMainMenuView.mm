#include "NOMainMenuView.h"

// Components
#include "NOViewsIndexes.h"

// Extensão da classe para declarar métodos privados
@interface NOMainMenuView ( Private )

// Inicializa a view
-( bool )buildNOMainMenuView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOMainMenuView;

@end

// Início da implementação da classe
@implementation NOMainMenuView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOMainMenuView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOMainMenuView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOMainMenuView
	Inicializa a view.

==============================================================================================*/

- ( bool )buildNOMainMenuView
{
	// Inicializa as variáveis da classe
	[self setScreenTitle: @""];
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{	
//	[super drawRect: rect];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self cleanNOMainMenuView];
//  [super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNOMainMenuView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOMainMenuView
//{
//}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.

===============================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( hButton == hBtProfile )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_PROFILE_ACTION];
	}
	else if( hButton == hBtRanking )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_RANKING_ACTION];
	}
	else if( hButton == hBtHelp )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_HELP];
	}
	else
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_SHUT_DOWN];
	}
}

// Fim da implementação da classe
@end
