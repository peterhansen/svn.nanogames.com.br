/*
 *  NOControllerView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/18/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_CONTROLLER_VIEW_H
#define NANO_ONLINE_CONTROLLER_VIEW_H

// Apple Foundation
#import <UIKit/UIKit.h>

// Components
#include "FileSystem.h"
#include "NanoTypes.h"
#include "UpdatableView.h"
#include "ViewManager.h"

// Components - NanoOnline
#include "NOConf.h"
#include "NOCustomer.h"
#include "NORanking.h"

// C++
#include <stack>
#include <string>
#include <vector>

// Declarações adiadas
@class UICalloutView;
@class UIImgProgressBar;
@class UILimitedTextField;
@class UIWaitView;

@interface NOControllerView : UpdatableView <UITextFieldDelegate, TransitionDelegate>
{
	@private
		// Barra de progresso do NanoOnline
		UIImgProgressBar* hProgressBar;

		// Índice da view a ser chamada quando o usuário sair do NanoOnline
		int8 onEndViewIndex;
	
		// Índice da view atual
		int8 currViewIndex;
	
		// Índice da última view
		int8 lastViewIndex;
	
		// Índice da primeira tela do NanoOnline. Não precisamos começar, necessariamente, em NO_VIEW_INDEX_MAIN_MENU
		int8 noFirstView;

		// Variáveis auxiliares para tratar o teclado e suas animações
		float keyboardHeight;
		float offsetBeforeKeyboard;
		NSTimer* hAnimationTimer;
		UILimitedTextField *hLastTextField;
	
		// Auxiliares de tratamento de interface
		UIScrollView *hCurrScrollView;
		UIWaitView *hWaitView;
		bool btBackWasEnabled;

		// Responsável por realiza a transição entre as views da aplicação
		IBOutlet ViewManager* hViewManager;

		// Tratamento de Erros
		IBOutlet UIButton *hBtError;
		IBOutlet UILabel *hLbErrorMsg;
		UICalloutView *hCallout;
		NSTimer *hCalloutTimer;
	
		// Botão utilizado para voltar à tela anterior
		IBOutlet UIButton *hBtBack;
	
		// Botão utilzado para cancelar conexões ativas
		IBOutlet UIButton *hBtCancelConnection;
	
		// Outros elementos da interface
		IBOutlet UILabel *hViewTitle;
		IBOutlet UIImageView *hBarBottom;
		IBOutlet UIImageView *hBarTop;
		IBOutlet UIImageView *hNanoOnlineLogo;
	
		// Índices das telas que foram visitadas
		std::stack< int8 > screensIndexesStack;
	
		// Perfil utilizado para manipulação de dados entre telas diferentes
		NOCustomer tempProfile;
}

@property ( readonly, nonatomic, assign, /*setter = setViewManager,*/ getter = getViewManager ) ViewManager *hViewManager;
@property ( readwrite, nonatomic, assign, setter = setLastViewIndex, getter = getLastViewIndex ) int8 onEndViewIndex;
@property ( readwrite, nonatomic, assign, setter = setNOFirstView, getter = getNOFirstView ) int8 noFirstView;

// Método chamado assim que esta view passa a ser a principal view da aplicação
-( void )onBecomeCurrentScreen;

// Determina a porcentagem que a barra de progresso deve indicar. Todos os valores recebidos
// serão colocados no intervalo [0.0, 1.0]
-( void )setProgress:( float )progress;

// Trava a interface atual, exibindo um texto e uma animação de espera
-( bool )showWaitViewWithText:( NSString* )hText;

// Desativa o texto e a animação de espera, destravando a interface
-( void )hideWaitView;

// Exibe para o usuário a descrição de um erro ocorrido
-( void )showError:( const NOString& )errorStr;

// Esconde os componentes de interface que indicam erros
-( void )hideError;

// Exibe ou retira da tela a mensagem que descreve o último erro ocorrido
-( IBAction )toggleErrorMsg;

// Evento recebido quando o usuário pressiona o botão de 'voltar'
-( IBAction )onBtBackPressed;

// Cancela uma requisição incompleta
-( IBAction )onBtCancelConnectionPressed;

// Altera o caminho percorrido pelo usuário nas telas do NanoOnline.
// O novo caminho considerado será o menor da tela principal até a 
// tela recebida como parâmetro.
-( void )setHistoryAsShortestWayToView:( int8 )viewIndex;

// Determina o perfil ativo
-( void )setActiveProfile:( const NOCustomer* )pProfile;

// Retorna o perfil ativo
-( NOCustomer* )getActiveProfile:( NOCustomer* )pOut;

// Determina os dados do perfil temporário
-( void )setTempProfile:( const NOCustomer* )pProfile;

// Retorna o perfil temporário
-( NOCustomer* )getTempProfile:( NOCustomer* )pOut;

// Salva a identificação única do último perfil a se logar no aparelho
-( void )setLastLoggedInProfile:( NOProfileId )profileId;

// Retorna a identificação única do último perfil a se logar no aparelho
-( NOProfileId )getLastLoggedInProfile;

// Retorna o singleton desta classe
+( NOControllerView* )sharedInstance;

// Retornam informações sobre o padrão das animações do NanoOnline
+( float )getInterfaceAnimDur;
+( UIViewAnimationCurve )getInterfaceAnimStyle;

// Retorna o avatar padrão para determinado sexo
+( UIImage* )getDefaultAvatarForGenre:( Genre )g;

// Adiciona uma camada de abstração na conversão de strings. Dentro do NanoOnline,
// o usuário só deve utilizar esta função, e não as macros STD_STRING_TO_NSSTRING
// e NSSTRING_TO_STD_STRING
#ifdef NANO_ONLINE_UNICODE_SUPPORT

+( void )ConvertNSString:( const NSString* )hStr toSTDString:( std::wstring& )outStr;
+( NSString* )ConvertSTDStringToNSString:( const std::wstring& )str;

#else

+( void )ConvertNSString:( const NSString* )hStr toSTDString:( std::string& )outStr;
+( NSString* )ConvertSTDStringToNSString:( const std::string& )str;

#endif

// Salva a pontuação do usuário caso ele não tenha uma melhor já salva e ainda não submetida
+( void )insertNewRecord:( const int64& )score forProfileId:( NOProfileId )profileId;

// Carrega as pontuações ainda não submetidas
+( void )loadNotSubmittedRecords;

// Salva as pontuações ainda não submetidas
+( void )saveNotSubmittedRecords;

// Carrega os perfis salvos localmente no vetor passado como parâmetro
+( FsOpRes )loadProfiles:( std::vector< NOCustomer >& )outProfiles;

// Salva localmente os perfis recebidos
+( FsOpRes )saveProfiles:( const std::vector< NOCustomer >& )profiles;

// Acrescenta um novo perfil ao array de perfis local
+( FsOpRes )insertProfile:( const NOCustomer& )profile;

// Atualiza um perfil localmente
+( FsOpRes )updateProfile:( const NOCustomer& )profile;

// Remove um perfil do array de perfis local
+( FsOpRes )deleteProfile:( const NOCustomer& )profile;

@end

#endif
