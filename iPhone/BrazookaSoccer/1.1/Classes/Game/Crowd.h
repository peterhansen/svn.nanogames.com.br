/*
 *  Crowd.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/10/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CROWD_H
#define CROWD_H 1

#include "ObjectGroup.h"

#include "CollisionQuad.h"
#include "RealPosOwner.h"

#include "Tests.h"

// Número de quads de colisão
#define CROWD_N_COLLISION_QUADS 4

// Forward Declarations
class RenderableImage;

// Possíveis estados da torcida
enum CrowdState
{
	CROWD_STATE_UNDEFINED = -1,
	CROWD_STATE_EMPTY = 0,
	CROWD_STATE_BIT,
	CROWD_STATE_FULL
};

class Crowd : public ObjectGroup, public RealPosOwner
{
	public:
		// Construtor
		// Exceções: ConstructorException
		Crowd( void );
	
		// Destrutor
		virtual ~Crowd( void );
	
		// Determina o estado do objeto
		void setCrowdState( CrowdState state );
	
		// Retorna o estado do objeto
		CrowdState getCrowdState( void ) const;
	
		// Retorna o número de quads de colisão possuídos pelo objeto
		uint8 getNCollisionQuads( void ) const;
	
		// Retorna o quad de colisão possuído pelo objeto
		const CollisionQuad* getCollisionQuad( uint8 index ) const;
	
		#if DEBUG && IS_CURR_TEST( TEST_COLLISION_AREAS )
			// Renderiza o objeto
			virtual bool render( void );
		#endif
	
		void setLastCollisionQuadIndex( int8 index );
		int8 getLastCollisionQuadIndex( void ) const;
	
		// Retorna a menor coordenada z a partir da qual temos torcida
		float getCollisionTopZ( void ) const;
	
		// Retorna a menor coordenada x a partir da qual temos torcida
		float getCollisionLeftX( void ) const;
	
		// Retorna a maior coordenada x a partir da qual temos torcida
		float getCollisionRightX( void ) const;

	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( CrowdState state );
	
		// Atualiza a posição do objeto na tela
		virtual void updatePosition( void );
	
		// Estado atual da torcida
		CrowdState crowdState;
	
		// Quads de colisão da torcida
		int8 lastCollisionQuad;
		CollisionQuad collisionQuads[ CROWD_N_COLLISION_QUADS ];
};

// Implementação dos métodos inline

// Retorna o estado do objeto
inline CrowdState Crowd::getCrowdState( void ) const
{
	return crowdState;
}

// Retorna o número de quads de colisão possuídos pelo objeto
inline uint8 Crowd::getNCollisionQuads( void ) const
{
	return CROWD_N_COLLISION_QUADS;
}
	
// Retorna o quad de colisão possuído pelo objeto
inline const CollisionQuad* Crowd::getCollisionQuad( uint8 index ) const
{
	return &collisionQuads[ index ];
}

// Retorna o último quad com o qual a bola colidiu
inline int8 Crowd::getLastCollisionQuadIndex( void ) const
{
	return lastCollisionQuad;
}

// Determina o último quad com o qual a bola colidiu
inline void Crowd::setLastCollisionQuadIndex( int8 index )
{
	lastCollisionQuad = index;
}

#endif
