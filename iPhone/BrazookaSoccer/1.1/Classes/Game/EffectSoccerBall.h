/*
 *  EffectSoccerBall.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef EFFECT_SOCCER_BALL_H
#define EFFECT_SOCCER_BALL_H 1

#include "RenderableImage.h"

class EffectSoccerBall : public RenderableImage
{
	public:
		// Construtor
		EffectSoccerBall( void );

		// Destrutor
		virtual ~EffectSoccerBall( void );
	
		// Retorna o raio da imagem da bola
		inline float getRadius( void ) const { return radius; };
	
		// Retorna a posição do centro da bola
		Point3f getCenterPosition( void ) const;
	
	private:
		// Raio da imagem da bola
		float radius;
};

#endif
