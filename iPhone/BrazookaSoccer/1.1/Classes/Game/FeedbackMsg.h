/*
 *  FeedbackMsg.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/30/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef	FEEDBACK_MSG_H
#define FEEDBACK_MSG_H 1

#include "RenderableImage.h"

#include "InterfaceControl.h"

// Forward Declarations
class InterfaceControlListener;

enum FeedbackMsgAnimType
{
	FEEDBACK_MSG_ANIM_NONE = 0,
	FEEDBACK_MSG_ANIM_VIEWPORT = 1,
	FEEDBACK_MSG_ANIM_BOUNCE = 2,
	FEEDBACK_MSG_ANIM_FADE = 3
};

class FeedbackMsg : public RenderableImage, public InterfaceControl
{
	public:
		// Construtor
		// Exceções: ConstructorException
		FeedbackMsg( const char* pImgPath, const TextureFrame* pImgFrame, InterfaceControlListener* pListener, LoadTextureFunction pLoadTextureFunction = &( ResourceManager::GetTexture2D ) );
	
		// Destrutor
		virtual ~FeedbackMsg( void );
	
		// Determina o tipo de animações utilizado pelo controle
		void setCurrAnimType( FeedbackMsgAnimType animType );
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

	private:
		// Métodos que realizam as animações
		void animViewport( float timeElapsed );
		void animBounce( float timeElapsed );
		void animFade( float timeElapsed );

		// Tipo de animação do controle
		FeedbackMsgAnimType currAnimType;
	
		// Auxiliares das animações
		float speedY;
		float acumWidth;
		
};

#endif
