//
//  ConfirmLoadView.h
//  FreeKick
//
//  Created by Daniel Monteiro on 10/2/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#ifndef CONFIRM_LOAD_VIEW_H
#define CONFIRM_LOAD_VIEW_H

#include "GameBasicView.h"

@interface ConfirmLoadView : GameBasicView
{
	@private
		// Handlers para os elementos criados via InterfaceBuilder
		IBOutlet UIButton *hBtYes, *hBtNo;

		// Callbacks para as respostas do popup
		SEL hYesSelector, hNoSelector;
		id hYesTarget, hNoTarget;
}

// Chamado quando um botão é pressionado
-( IBAction )onBtPressed:( UIButton* )hButton;

// Determinam as callbacks que devem ser chamadas para cada resposta do popup
-( void )setNoSelector:( SEL )hSelector WithTarget:( id )target;
-( void )setYesSelector:( SEL )hSelector WithTarget:( id )target;

// Executa a animação que exibe o popup
-( void )startShowAnim;

@end

#endif
