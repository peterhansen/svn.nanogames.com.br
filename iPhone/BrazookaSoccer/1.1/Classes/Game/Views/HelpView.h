//
//  HelpView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 6/19/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef HELP_VIEW_H
#define HELP_VIEW_H 1

#import "GameBasicView.h"

enum HelpViewState
{
	HELP_VIEW_STATE_UNDEFINED = -1,
	HELP_VIEW_STATE_HIDDEN = 0,
	HELP_VIEW_STATE_SHOWING_STEP_0 = 1,
	HELP_VIEW_STATE_SHOWING_STEP_1 = 2,
	HELP_VIEW_STATE_SHOWING_STEP_2 = 3,
	HELP_VIEW_STATE_SHOWN = 4,
	HELP_VIEW_STATE_HIDING = 5
};

enum HelpViewMode
{
	HELP_VIEW_MODE_UNDEFINED = -1,
	HELP_VIEW_MODE_HELP = 0,
	HELP_VIEW_MODE_PLAY_WITH_PROFILE = 1
};

@interface HelpView : GameBasicView
{
	@private
		// Handlers para os elementos da interface
		IBOutlet UIButton *hBtBack;
		IBOutlet UITextView *hTxtHelp;
		IBOutlet UIImageView *hImgHairLeft, *hImgHairRight;
		IBOutlet UIButton *hBtChangeProfile, *hBtConfirmProfile;
	
		// Estado atual da view
		HelpViewState currState;
	
		// Modo da interface da view
		HelpViewMode viewMode;
	
		// Controlador dos sons
		bool soundFlag;
}

@property( readwrite, nonatomic, assign, getter = getViewMode, setter = setViewMode ) HelpViewMode viewMode;

// Atualiza a view
-( void )update:( float )timeElapsed;

// Indica que o usuário está pressionando uma opção do menu
-( IBAction )onBtPressed:( id )hButton;

// Método chamado antes de iniciarmos uma transição para esta view
-( void )onBeforeTransition;

// Método chamado quando a view se torna a view principal da aplicação
-( void )onBecomeCurrentScreen;

@end

#endif
