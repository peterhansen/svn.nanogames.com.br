//
//  ConfirmLoadView.m
//  FreeKick
//
//  Created by Daniel Monteiro on 10/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import "ConfirmLoadView.h"
#import "Config.h"

@implementation ConfirmLoadView


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)dealloc {
    [super dealloc];
}

- (BOOL)acceptsFirstResponder
{
	return YES;
}

- (IBAction) AnswerYES
{
	[[[UIApplication sharedApplication] delegate] scheduleLoadState];
	[self setHidden:YES];
	[[[UIApplication sharedApplication] delegate]performTransitionToView:VIEW_INDEX_LOAD_GAME];
}

- (IBAction) AnswerNO
{
	[[[UIApplication sharedApplication] delegate] removeStateFile];
	[self setHidden:YES];	
	[[[UIApplication sharedApplication] delegate]performTransitionToView:VIEW_INDEX_LOAD_GAME];	
}

@end
