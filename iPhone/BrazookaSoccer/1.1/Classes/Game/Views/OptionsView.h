//
//  OptionsView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 6/17/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef OPTIONS_VIEW_H
#define OPTIONS_VIEW_H 1

#import "GameBasicView.h"

#include "NanoTypes.h"

#include <vector>

enum OptionsViewState
{
	OPTIONS_VIEW_STATE_UNDEFINED = -1,
	OPTIONS_VIEW_STATE_HIDDEN = 0,
	OPTIONS_VIEW_STATE_SHOWING_STEP_0,
	OPTIONS_VIEW_STATE_SHOWING_STEP_1,
	OPTIONS_VIEW_STATE_SHOWING_STEP_2,
	OPTIONS_VIEW_STATE_SHOWN,
	OPTIONS_VIEW_STATE_HIDING
};

enum OptionsViewStyle
{
	OPTIONS_VIEW_STYLE_PAUSE_OPTIONS = 0,
	OPTIONS_VIEW_STYLE_MAIN_OPTIONS
};

@interface OptionsView : GameBasicView
{
	@private
		// Handlers para as imagens caso queiramos fazer animações
		IBOutlet UIImageView *hImgHair;
		IBOutlet UIImageView *hIcon;

		// Handlers para os botões
		IBOutlet UIButton *hBtBack;

		IBOutlet UIButton *hBtSettings;
		
		// TODOO : Implementar no update
		// IBOutlet UIButton *hBtCalibrate;

		IBOutlet UIButton *hBtHelp;
		IBOutlet UIButton *hBtQuit;

		// Handlers para cada um dos elementos das subopções do menu
		IBOutlet UITextView *hHelpBox;

		IBOutlet UIImageView *hImgVolume;
		IBOutlet UISlider *hVolumeSlider;
	
		IBOutlet UIImageView *hImgVb;
		IBOutlet UIImageView *hVbCheck;
		IBOutlet UIButton *hVbCheckBoxOn, *hVbCheckBoxOff;
	
		IBOutlet UIImageView *hImgQuitQuestion;
		IBOutlet UIButton *hBtYes;
	
		// Estado atual da view
		OptionsViewStyle currStyle;
		OptionsViewState currState;
	
		// Auxiliares
		float largestWidth;
		int8 previousViewIndex;
	
		// Controlador dos sons
		std::vector< bool > soundFlags;
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Indica que o usuário alterou o volume dos sons da aplicação
- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( id )hButton;

// Determina se devemos exibir o menu de opções ou o menu de pausa
- ( void ) setStyle:( OptionsViewStyle ) style;

// Indica que tela deverá ser chamada quando sairmos deste menu
- ( void ) setPrevScreen:( int8 ) prevViewIndex;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

@end

#endif
