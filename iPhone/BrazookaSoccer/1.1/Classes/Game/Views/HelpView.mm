#import "HelpView.h"

// Components
#include "Config.h"
#include "ObjcMacros.h"

// NanoOnline
#include "NOControllerView.h"
#include "NOCustomer.h"
#include "NOGlobalData.h"
#include "NOString.h"

// Game
#include "FreeKickAppDelegate.h"

// Duração da animação de fadein do ícone
#define HELP_VIEW_CONTROLS_ANIM_DUR 0.300f

// Quantidade de tempo a mais que cada colocação do ranking, após a 1a, leva para terminar a animação de fadein
#define HELP_VIEW_CONTROLS_ANIM_DIFF 0.100f

//// Tempo total das animações
#define HELP_VIEW_HAIR_ANIM_DUR 0.400f
#define HELP_VIEW_BT_BACK_ANIM_DUR 0.150f

// Número de pixels, dos controles da esquerda, que ficam fora da tela
#define HELP_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN 16.0f

// Extensão da classe para declarar métodos "privados"
@interface HelpView( Private )

- ( bool )buildHelpView;
- ( void ) setState:( HelpViewState ) state;

// Chamado quando o usuário pressiona o botão "voltar"
-( void )onBack;

// Modifica a imagem de um botão
-( void )changeBt:( UIButton* )hBt WithImg:( const std::string& )imgName;

@end

// Início da implementção da classe
@implementation HelpView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize viewMode;

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		// Inicializa o objeto
		if( ![self buildHelpView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildHelpView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildHelpView
	Inicializa a view.

==============================================================================================*/

-( bool )buildHelpView
{
	currState = HELP_VIEW_STATE_UNDEFINED;
	viewMode = HELP_VIEW_MODE_UNDEFINED;
	soundFlag = false;
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Modifica a fonte da caixa de texto (o lixo do IB não permite-nos fazer o mesmo através dele)
	[hTxtHelp setFont: [(( FreeKickAppDelegate* )APP_DELEGATE ) getiPhoneFont]];

	[hImgHairLeft setClipsToBounds: YES];
	[hImgHairRight setClipsToBounds: YES];
	
	// Espelha a imagem do lado direito, já que esta é formada pela imagem do lado esquerdo
	CGAffineTransform transform = hImgHairRight.transform;
	transform = CGAffineTransformScale( transform, -1.0f, 1.0f );
	[hImgHairRight setTransform:transform];
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM update
	Atualiza a view.

==============================================================================================*/

- ( void ) update:( float )timeElapsed
{
	switch( currState )
	{
		case HELP_VIEW_STATE_SHOWING_STEP_0:
			{
				float finalX = ( SCREEN_WIDTH * 0.5f );
				int32 nextX = hImgHairRight.frame.origin.x + ( ( timeElapsed * ( hImgHairRight.frame.size.width + ( SCREEN_WIDTH * 0.5f ) ) ) / HELP_VIEW_HAIR_ANIM_DUR );
				
				if( ( ( nextX + ( hImgHairRight.frame.size.width * 2.0f ) ) > 0.0f ) && !soundFlag )
				{
					soundFlag = true;
					[(( FreeKickAppDelegate* )APP_DELEGATE) playAudioNamed: SOUND_NAME_HAIR_MOVE AtIndex: SOUND_INDEX_HAIR_MOVE Looping: false];
				}
				
				if( nextX > finalX )
				{
					nextX = finalX;
					[self setState: HELP_VIEW_STATE_SHOWING_STEP_1];
					
					soundFlag = false;
				}
				
				[hImgHairRight setFrame: CGRectMake( nextX, 0.0f, hImgHairRight.frame.size.width, hImgHairRight.frame.size.height )];
				[hImgHairLeft setFrame: CGRectMake( hImgHairRight.frame.origin.x - hImgHairLeft.frame.size.width, 0.0f, hImgHairLeft.frame.size.width, hImgHairLeft.frame.size.height )];
			}
			break;

		case HELP_VIEW_STATE_SHOWING_STEP_1:
			{
				float nextAlpha = hTxtHelp.alpha + ( timeElapsed / HELP_VIEW_CONTROLS_ANIM_DUR );
				
				if( nextAlpha > 1.0f )
				{
					nextAlpha = 1.0f;
					[self setState: HELP_VIEW_STATE_SHOWING_STEP_2 ];
				}

				[hTxtHelp setAlpha: nextAlpha];
				
				if( viewMode == HELP_VIEW_MODE_PLAY_WITH_PROFILE )
				{
					[hBtChangeProfile setAlpha: nextAlpha];
					[hBtConfirmProfile setAlpha: nextAlpha];
				}
			}
			break;

		case HELP_VIEW_STATE_SHOWING_STEP_2:
			{
				float totalMovement = hBtBack.frame.size.width - HELP_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN;
				
				float finalX = SCREEN_WIDTH - hBtBack.frame.size.width + HELP_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN;
				float nextX = hBtBack.frame.origin.x - (( timeElapsed * totalMovement ) / HELP_VIEW_BT_BACK_ANIM_DUR );
				
				if( ( nextX < SCREEN_WIDTH ) && !soundFlag )
				{
					soundFlag = true;
					[(( FreeKickAppDelegate * )APP_DELEGATE) playAudioNamed: SOUND_NAME_TAG_INCOMING AtIndex: SOUND_INDEX_TAG_0_INCOMING Looping: false];
				}
				
				if( nextX < finalX )
				{
					nextX = finalX;
					[self setState: HELP_VIEW_STATE_SHOWN];
				}
				
				[hBtBack setFrame: CGRectMake( nextX, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height )];
			}
			break;
			
		case HELP_VIEW_STATE_HIDING:
			// TODOO : Fazer animações antes de transitar para a outra view ou animar direto? Acho melhor animar direto...
//			{
//				uint8 done = 0;
//				float diff = 0.0f;
//			
//				if( done == APP_N_RECORDS )
//					[self setState: HELP_VIEW_STATE_HIDDEN ];
//			}
			break;
	}
}

/*==============================================================================================

MENSAGEM setState
	Determina o estado da view.

==============================================================================================*/

- ( void ) setState:( HelpViewState ) state
{
	currState = state;

	switch( state )
	{
		case HELP_VIEW_STATE_HIDDEN:
			{	
				[hTxtHelp setAlpha: 0.0f];
				
				[hImgHairRight setFrame: CGRectMake( -hImgHairRight.frame.size.width, 0.0f, hImgHairRight.frame.size.width, hImgHairRight.frame.size.height )];
				[hImgHairLeft setFrame: CGRectMake( hImgHairRight.frame.origin.x - hImgHairLeft.frame.size.width, 0.0f, hImgHairLeft.frame.size.width, hImgHairLeft.frame.size.height )];
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height )];
				
				soundFlag = false;

				[self suspend];
			}
			break;

		case HELP_VIEW_STATE_SHOWN:
			{
				[hTxtHelp setAlpha: 1.0f];
				
				[hImgHairLeft setFrame: CGRectMake( ( SCREEN_WIDTH * 0.5f ) - hImgHairLeft.frame.size.width, 0.0f, hImgHairLeft.frame.size.width, hImgHairLeft.frame.size.height )];
				[hImgHairRight setFrame: CGRectMake( ( SCREEN_WIDTH * 0.5f ), 0.0f, hImgHairRight.frame.size.width, hImgHairRight.frame.size.height )];
				[hBtBack setFrame: CGRectMake( SCREEN_WIDTH - hBtBack.frame.size.width + HELP_VIEW_BT_BACK_PIXELS_OUTSIDE_SCREEN, hBtBack.frame.origin.y, hBtBack.frame.size.width, hBtBack.frame.size.height ) ];
				
				[self suspend];
			}
			break;

		case HELP_VIEW_STATE_SHOWING_STEP_0:
		case HELP_VIEW_STATE_HIDING:
			[self resume];
			break;
			
		case HELP_VIEW_STATE_SHOWING_STEP_1:
			break;
	}
}

/*==============================================================================================

MENSAGEM onBtPressed
	Indica que o usuário selecionou uma opção do menu.

==============================================================================================*/

- ( IBAction ) onBtPressed:( id )hButton
{
	if( currState != HELP_VIEW_STATE_SHOWN )
		return;

	if( hButton == hBtBack )
		[self onBack];
	else if( hButton == hBtChangeProfile )
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_NANO_ONLINE_FROM_PLAY_WITH_PROFILE];
	else if( hButton == hBtConfirmProfile )
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_LOAD_GAME];
}

/*==============================================================================================

MENSAGEM onBeforeTransition
	Método chamado antes de iniciarmos uma transição para esta view.

==============================================================================================*/

- ( void )onBeforeTransition
{
	if( viewMode == HELP_VIEW_MODE_UNDEFINED )
		return;

	FreeKickAppDelegate *hAppDelegate = ( FreeKickAppDelegate* )APP_DELEGATE;

	if( viewMode == HELP_VIEW_MODE_HELP )
	{
		hTxtHelp.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_HELP] );
	}
	else// if( viewMode == HELP_VIEW_MODE_PLAY_WITH_PROFILE )
	{
		[hTxtHelp setTextAlignment: UITextAlignmentCenter];

		[hBtChangeProfile setAlpha: 0.0f];
		[hBtChangeProfile setHidden: NO];
		[hBtChangeProfile setUserInteractionEnabled: YES];

		[hBtConfirmProfile setAlpha: 0.0f];
		[hBtConfirmProfile setHidden: NO];
		[hBtConfirmProfile setUserInteractionEnabled: YES];

		if( NOGlobalData::GetActiveProfile().getProfileId() >= 0 )
		{
			NOString nickname;
			NOGlobalData::GetActiveProfile().getNickname( nickname );

			hTxtHelp.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_PLAYING_WITH_PROFILE] );
			hTxtHelp.text = [[hTxtHelp text] stringByAppendingFormat: @"\n\n%@\n\n%s", [NOControllerView ConvertSTDStringToNSString: nickname.c_str()], [hAppDelegate getText: TEXT_INDEX_CHANGE_PROFILE], nil];
			
			[hTxtHelp setUserInteractionEnabled: NO];
		}
		else
		{
			CGRect txtFrame = [hTxtHelp frame];
			txtFrame.size.height -= ( txtFrame.size.height * 0.2f );
			[hTxtHelp setFrame: txtFrame];
			
			[self changeBt: hBtChangeProfile WithImg: "optYes"];
			[self changeBt: hBtConfirmProfile WithImg: "optNo"];
			
			hTxtHelp.text = CHAR_ARRAY_TO_NSSTRING( [hAppDelegate getText: TEXT_INDEX_PLAYING_WITH_NO_PROFILE] );
		}
	}

	[self setState: HELP_VIEW_STATE_HIDDEN];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

===============================================================================================*/

-( void )onBecomeCurrentScreen
{
	if( viewMode != HELP_VIEW_MODE_UNDEFINED )
		[self setState: HELP_VIEW_STATE_SHOWING_STEP_0];
	else
		[self onBack];
}

/*==============================================================================================

MENSAGEM onBack
	Chamado quando o usuário pressiona o botão "voltar".

===============================================================================================*/

-( void )onBack
{
	if( viewMode != HELP_VIEW_MODE_HELP )
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_PLAY_MENU];
	else
		[APP_DELEGATE performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM ChangeBt:WithImg:
	Modifica a imagem de um botão.

===============================================================================================*/

-( void )changeBt:( UIButton* )hBt WithImg:( const std::string& )imgName
{
	char buffer[ PATH_MAX ];
	UIImage *hImg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, imgName.c_str(), "png" ))];
	if( hImg )
	{
		[hBt setImage: hImg forState: UIControlStateNormal];
		
		// Se determinarmos as outras imagens, o cocoa pára de fazer os ajustes automáticos (como escurecer
		// a imagem quando pressionado)
//		[hBt setImage: hImg forState: UIControlStateDisabled];
//		[hBt setImage: hImg forState: UIControlStateSelected];
//		[hBt setImage: hImg forState: UIControlStateHighlighted];
	}
	else
	{
		// No fim das contas, isso nos faz voltar ao menu principal, independentemente do modo
		// Seria mais elegante voltarmos para a tela anterior, mas, no fim das contas, isto
		// não deveria ocorrer
		viewMode = HELP_VIEW_MODE_UNDEFINED;
	}
}

// Fim da implementação da classe
@end
