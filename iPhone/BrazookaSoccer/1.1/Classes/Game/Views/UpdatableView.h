//
//  UpdatableView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 6/17/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef UPDATABLE_VIEW_H
#define UPDATABLE_VIEW_H 1

#import <UIKit/UIKit.h>

@interface UpdatableView : UIView
{
	@private
		// Controlam a atualização da view
		NSTimer* hUpdateTimer;
		NSTimeInterval updateInterval;
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Indica que o usuário está pressionando uma opção do menu.
- ( IBAction ) onBtClick:( id )hButton;

// Pára o processamento da view
- ( void ) suspend;

// Reinicia o processamento da view
- ( void ) resume;

// Controlador do timer
- ( void )timerProxy;

@end

#endif
