/*
 *  SleepyBarrierPlayer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SLEEPY_BARRIER_PALYER_H
#define SLEEPY_BARRIER_PALYER_H 1

#include "BarrierPlayer.h"
#include "Sprite.h"

#include "Tests.h"


class SleepyBarrierPlayer : public BarrierPlayer, public SpriteListener
{
	public:
		// Construtor
		SleepyBarrierPlayer( void );
	
		// Destrutor
		virtual ~SleepyBarrierPlayer( void );
	
		// Retorna a largura real do jogador da barreira (em metros)
		virtual float getRealWidth( void ) const;

		// Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
		// que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente
		virtual void prepare( const Point3f& ballRealPos );

		#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )
			// Renderiza o objeto
			virtual bool render( void );
		#endif

	private:
		// Atualiza o frame do jogador de acordo com a direção para a qual está virado
		virtual void updateFrame( void );
	
		// Atualiza a posição da bola na tela (não altera a posição real)
		virtual void updatePosition( void );
	
		// Chamado quando o sprite muda de etapa de animação
		virtual void onAnimStepChanged( Sprite* pSprite );
};

#endif
