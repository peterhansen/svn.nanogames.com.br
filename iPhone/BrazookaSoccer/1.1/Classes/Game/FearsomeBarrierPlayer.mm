#include "FearsomeBarrierPlayer.h"

#include "Exceptions.h"

#include "Config.h"
#include "GameUtils.h"

// Número de objetos contidos no grupo
#define FEARSOME_N_OBJS 2

// Índices dos objetos contidos no grupo
#define FEARSOME_OBJ_INDEX_SHADOW 0
#define FEARSOME_OBJ_INDEX_PLAYER 1

// Altura e largura reais deste personagem
#define FEARSOME_WIDTH	0.70f
#define FEARSOME_HEIGHT 1.72f

// Offset do pixel de referência do personagem
#define FEARSOME_FEET_OFFSET_Y 18.0f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

FearsomeBarrierPlayer::FearsomeBarrierPlayer( void ) : BarrierPlayer( FEARSOME_N_OBJS )
{
	{ // Evita erros de compilação por causa dos gotos

		// Aloca o personagem
		char aux[] = "gs";
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pShadow = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pShadow = new Sprite( aux, aux );
		#endif
		if( !pShadow || !insertObject( pShadow ) )
		{
			delete pShadow;
			goto Error;
		}
		
		aux[1] = '\0';
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pFearsome = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pFearsome = new Sprite( aux, aux );
		#endif
		if( !pFearsome || !insertObject( pFearsome ) )
		{
			delete pFearsome;
			goto Error;
		}
		pFearsome->setListener( this );

		// Determina o tamanho do grupo
		setSize( pFearsome->getWidth(), pFearsome->getHeight() );

		float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, FEARSOME_HEIGHT, 0.0f ).y ) / pFearsome->getStepFrameHeight() );
		setScale( scaleFactor, scaleFactor );
		
		#if DEBUG
			setName( "gay" );
		#endif

		return;

	} // Evita erros de compilação por causa dos gotos
			
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "FearsomeBarrierPlayer::FearsomeBarrierPlayer( void ): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

FearsomeBarrierPlayer::~FearsomeBarrierPlayer( void )
{
}

/*===========================================================================================
 
MÉTODO getRealWidth
	Retorna a largura real do jogador da barreira (em metros).

============================================================================================*/

float FearsomeBarrierPlayer::getRealWidth( void ) const
{
	return FEARSOME_WIDTH - 0.15f;
}

/*===========================================================================================
 
MÉTODO prepare
	Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente.

============================================================================================*/

void FearsomeBarrierPlayer::prepare( const Point3f& ballRealPos )
{
	setActive( false );

	Sprite* pFearsome = static_cast< Sprite* >( getObject( FEARSOME_OBJ_INDEX_PLAYER ) );
	switch( direction )
	{
		case DIRECTION_LEFT:
		case DIRECTION_DOWN_LEFT:
		case DIRECTION_DOWN:
		case DIRECTION_DOWN_RIGHT:
		case DIRECTION_RIGHT:
			pFearsome->setAnimSequence( 0, false );
			
			if( pFearsome->isMirrored( MIRROR_HOR ) )
				pFearsome->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_LEFT:
			pFearsome->setAnimSequence( 1, false );

			if( pFearsome->isMirrored( MIRROR_HOR ) )
				pFearsome->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_RIGHT:
			pFearsome->setAnimSequence( 1, false );

			if( !pFearsome->isMirrored( MIRROR_HOR ) )
				pFearsome->mirror( MIRROR_HOR );

			break;

		// Nunca acontecerá esta possibilidade
//		case DIRECTION_UP:
//			break;
	}
	
	TextureFrame currFrameInfo;
	pFearsome->getStepFrameInfo( &currFrameInfo, pFearsome->getCurrAnimSequence(), pFearsome->getCurrAnimSequenceStep() );
	setAnchor( ( currFrameInfo.offsetX * getScale()->x ) + ( currFrameInfo.width * getScale()->x * 0.5f ),
			   ( currFrameInfo.offsetY * getScale()->y ) + (( currFrameInfo.height - FEARSOME_FEET_OFFSET_Y ) * getScale()->y ), 0.0f );
}

/*===============================================================================

MÉTODO onAnimStepChanged
	Chamado quando o sprite muda de etapa de animação.

================================================================================*/

void FearsomeBarrierPlayer::onAnimStepChanged( Sprite* pSprite )
{
	Sprite* pFearsome = static_cast< Sprite* >( getObject( FEARSOME_OBJ_INDEX_PLAYER ) );
	Sprite* pShadow = static_cast< Sprite* >( getObject( FEARSOME_OBJ_INDEX_SHADOW ) );
	
	pShadow->setAnimSequence( pFearsome->getCurrAnimSequence(), false, false );
	pShadow->setCurrentAnimSequenceStep( pFearsome->getCurrAnimSequenceStep() );
	
	// Garante que o posicionamento dos elementos do grupo estará correto
	updatePosition();
}

/*===========================================================================================
 
MÉTODO updateFrame
	Atualiza o frame do jogador de acordo com a direção para a qual está virado.

============================================================================================*/

void FearsomeBarrierPlayer::updateFrame( void )
{
	float width = FEARSOME_WIDTH, height = FEARSOME_HEIGHT;
	Point3f top( VECTOR_UP_90 ), right, realOffset, screenOffset;
	
	getOrtho( &right );

	Sprite* pFearsome = static_cast< Sprite* >( getObject( FEARSOME_OBJ_INDEX_PLAYER ) );
	if( pFearsome->getCurrAnimSequence() == 0 )
	{
		int16 step = pFearsome->getCurrAnimSequenceStep();
		if( step == 1 )
		{
			top.set( VECTOR_UP_80 );
			screenOffset.x += 10.0f;
			height += 0.15f;
		}
		else if( step == 2 )
		{
			top.set( VECTOR_UP_80 );
			screenOffset.x += 7.0f;
		}
	}

	// OLD
//	TextureFrame currFrameInfo;
//	pFearsome->getStepFrameInfo( &currFrameInfo, pFearsome->getCurrAnimSequence(), pFearsome->getCurrAnimSequenceStep() );
//
//	Point3f aux = GameUtils::isoGlobalToPixels( realPosition.x + realOffset.x, realPosition.y + realOffset.y, realPosition.z + realOffset.z );
//	aux.x += pFearsome->getPosition()->x + currFrameInfo.offsetX + screenOffset.x;
//	aux.y += pFearsome->getPosition()->y + currFrameInfo.offsetY + screenOffset.y;
//	Point3f realPos = GameUtils::isoPixelsToGlobalY0( aux.x, aux.y );
//
//	collisionArea.set( realPos, top, right, ORIENT_TOP_LEFT, width, height );
	
	collisionArea.set( realPosition + realOffset, top, right, ORIENT_BOTTOM, width, height );
}

/*===============================================================================

MÉTODO updatePosition
	Atualiza a posição da bola na tela (não altera a posição real).
   
=============================================================================== */

void FearsomeBarrierPlayer::updatePosition( void )
{
	// Atualiza a orientação e o retângulo de colisão go goleiro
	updateFrame();

	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( p.x, p.y, 0.0f );
}

/*===============================================================================

MÉTODO onBallGettingClose
	Faz com que o jogador exiba alguma reação devido à aproximação da bola.

================================================================================*/

void FearsomeBarrierPlayer::onBallGettingClose( void )
{
	if( !didReactToBall() )
	{
		BarrierPlayer::onBallGettingClose();
		setActive( true );
	}
}

/*===========================================================================================
 
MÉTODO getCollisionSoundNameAndIndex
	Retorna o índice do som que deve ser tocado quando a bola colide com este jogador.

============================================================================================*/

void FearsomeBarrierPlayer::getCollisionSoundNameAndIndex( uint8* pName, uint8* pIndex ) const
{
	*pName = SOUND_NAME_BARRIER_FEARSOME;
	*pIndex = SOUND_INDEX_BARRIER_FEARSOME;
}

/*===============================================================================

MÉTODO render
	Renderiza o objeto.

================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )

bool FearsomeBarrierPlayer::render( void )
{
	if( ObjectGroup::render() )
	{
		// Renderiza o quad de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		Point3f pos = GameUtils::isoGlobalToPixels( collisionArea.position );
		Point3f dx = GameUtils::isoGlobalToPixels( collisionArea.right * collisionArea.width );
		Point3f dy = GameUtils::isoGlobalToPixels( collisionArea.top * collisionArea.height );

		dy = -dy;
		
		Point3f top[ 2 ] = {
								pos,
								pos + dy,
							};
		
		glColor4ub( 255, 0, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f right[ 2 ] = {
								pos,
								pos + dx,
							};
		
		glColor4ub( 0, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f others[ 4 ] = {
								pos + dy,
								pos + dy + dx,
								pos + dy + dx,
								pos + dx
							};

		glColor4ub( 255, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, others );
		glDrawArrays( GL_LINES, 0, 4 );

		Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
		if( collisionArea.normal.z > 0.0f )
			glColor4ub( 179, 179, 179, 255 );
		else
			glColor4ub( 255, 183, 15, 255 );

		glVertexPointer( 3, GL_FLOAT, 0, &nomal );
		glDrawArrays( GL_POINTS, 0, 1 );

//		// Renderiza a moldura do sprite
//		Sprite* pFearsome = static_cast< Sprite* >( getObject( FEARSOME_OBJ_INDEX_PLAYER ) );
//		TextureFrame currFrameInfo;
//		pFearsome->getStepFrameInfo( &currFrameInfo, pFearsome->getCurrAnimSequence(), pFearsome->getCurrAnimSequenceStep() );
//		
//		float spritePosX = getPosition()->x + pFearsome->getPosition()->x + currFrameInfo.offsetX + 0.375f;
//		float spritePosY = getPosition()->y + pFearsome->getPosition()->y + currFrameInfo.offsetY + 0.375f;
//		
//		float spriteWidth = currFrameInfo.width * getScale()->x;
//		float spriteHeight = currFrameInfo.height * getScale()->y;
//		
//		Point3f border[ 4 ] = {
//								Point3f( spritePosX, spritePosY ),
//								Point3f( spritePosX, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY )
//							  };
//		
//		glColor4ub( 0, 255, 255, 255 );
//
//		glVertexPointer( 3, GL_FLOAT, 0, border );
//		glDrawArrays( GL_LINE_LOOP, 0, 4 );

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		
		glColor4ub( 255, 255, 255, 255 );
		
		return true;
	}
	return false;
}

#endif
