#include "TestScene.h"
#include "Macros.h"
#include "OrthoCamera.h"
//#include "Pattern.h"

TestScene::TestScene( void )
		  : Scene( 1 ),
			pAway( new Sprite( "away", [Utils::GetPathForResource( @"away", @"bin" ) UTF8String] ) ),
			pText( new Label() ),
			pGroup( new ObjectGroup( 2 ) )
{
	pFont = new Font( "fontBonus", [Utils::GetPathForResource( @"fontBonus", @"bin" ) UTF8String] );
	
	pText->setFont( pFont );
	pText->setText( "8989", true, true );
	pText->setPosition( ( SCREEN_WIDTH - pText->getWidth() ) * 0.5f, 0.0f, 0.0f );
	
	OrthoCamera* pCamera = new OrthoCamera();
	setCurrentCamera( pCamera );
	
	pAway->setPosition( ( SCREEN_WIDTH - pAway->getWidth() ) * 0.5f, ( SCREEN_HEIGHT - pAway->getHeight() ) * 0.5f, 0.0f );
	pAway->setAnimSequence( 0, true );
	
	pGroup = new ObjectGroup( 2 );

//	Pattern* pOrangeQuad = new Pattern( COLOR_ORANGE );
//	pGroup->insertObject( pOrangeQuad );
//	
//	pOrangeQuad->setSize( 10.0f, 10.0f, 1.0f );
//	pOrangeQuad->setPosition( 0.0f, 0.0f, 0.0f );
//
//	Pattern* pGreenQuad = new Pattern( COLOR_GREEN );
//	pGroup->insertObject( pGreenQuad );
//	pGreenQuad->setSize( 60.0f, 10.0f, 1.0f );
//	
//	float bla = pOrangeQuad->getHeight();
//	pGreenQuad->setPosition( 0.0f, bla, 0.0f );
	
	pGroup->setSpeed( 5.0f, 0.0f, 0.0f );
	pGroup->setAcceleration( 500.0f, 0.0f, 0.0f );
}

TestScene::~TestScene( void )
{
	SAFE_DELETE( pAway );
	SAFE_DELETE( pText );
	SAFE_DELETE( pGroup );
}

bool TestScene::render( void )
{
	glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT );
	glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	pCurrCamera->place();
	
	pFont->renderCharAt( '9', 0.0f, 0.0f );

	bool ret = false;
	ret |= pAway->render();
	ret |= pText->render();
	ret |= pGroup->render();
	return ret;
}

// Atualiza o objeto
bool TestScene::update( float timeElapsed )
{
	bool ret = false;
	ret |= pAway->update( timeElapsed );
	ret |= pText->update( timeElapsed );
	ret |= pGroup->update( timeElapsed );
	return ret;
}
