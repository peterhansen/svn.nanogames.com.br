#import "SplashGame.h"

#include "ApplicationManager.h"
#include "Config.h"
#include "Macros.h"
#include "Utils.h"

// Tempo total do vídeo
#define VIDEO_TOTAL_TIME 13.750f

// Tempo extra no final do vídeo no qual mantemos o último frame fixo, necessário para impedirmos o flickering
#define VIDEO_EXTRA_TIME  0.550f

// Tempo que esperamos antes de ativar o timer que impedirá o flickering
#define TIMER_SLEEP_TIME ( VIDEO_TOTAL_TIME - VIDEO_EXTRA_TIME )

// Extensão da classe para declarar métodos privados
@interface SplashGame ()

	// Inicializa o objeto
	- ( bool ) buildSplashGame;

	// Cria o reprodutor de vídeos
	- ( bool ) createMoviePlayer;

	// Desaloca o componente reprodutor de vídeos
	- ( void ) deallocMoviePlayer;

	// Impede o flickering no final da exibição do vídeo
	- ( void ) onEnding;

@end

// Início da implementação da classe
@implementation SplashGame

/*==============================================================================================

MENSAGEM initWithFrameAndLanguage
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		// Inicializa o objeto
		if( ![self buildSplashGame] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildSplashGame] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM buildSplashGame
	Inicializa o objeto.

==============================================================================================*/

- ( bool ) buildSplashGame
{
	// Background para impedir o fade final do vídeo
	UIImage* hImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"0", @"png" )];
	if( hImg == nil )
		return false;
	
	UIImageView* hBkg = [[UIImageView alloc]initWithImage: hImg];
	if( hBkg == NULL )
		return false;
	
	CGPoint center = CGPointMake( HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH );
	hBkg.center = center;

	CGAffineTransform transform = [hBkg transform];
	transform = CGAffineTransformRotate( transform, static_cast< float >( ( M_PI * 0.5f ) ) );
	hBkg.transform = transform;

	[self addSubview: hBkg];
	hBkg.hidden = YES;
	
	// OLD: Não podemos criar o MoviePlayer numa thread que não seja a principal
//	return [self createMoviePlayer];
	
	return true;
}

/*==============================================================================================

MENSAGEM createMoviePlayer
	Cria o reprodutor de vídeos.

==============================================================================================*/

-( bool )createMoviePlayer
{
	//	Because it takes time to load movie files into memory, MPMoviePlayerController
	//	automatically begins loading your movie file shortly after you initialize a new 
	//	instance. When it is done preloading the movie file, it sends the
	//	MPMoviePlayerContentPreloadDidFinishNotification notification to any registered 
	//	observers. If an error occurred during loading, the userInfo dictionary of the 
	//	notification object contains the error information. If you call the play method 
	//	before preloading is complete, no notification is sent and your movie begins 
	//	playing as soon as it is loaded into memory

	// OBS: Não utilizado
	// Registra-se para receber a mensagem indicando que o vídeo está pronto para reprodução
//	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( moviePreloadDidFinish: ) name: MPMoviePlayerContentPreloadDidFinishNotification object: nil ];
	
    // Carrega o vídeo
	hMoviePlayer = [[ MPMoviePlayerController alloc ] initWithContentURL: Utils::GetURLForResource( @"0", @"m4v" )];
	if( hMoviePlayer == NULL )
		return false;

	// OBS: Não utilizado
    // Registra-se para obter os eventos da reprodução do vídeo
//  [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( moviePlayBackDidFinish: ) name: MPMoviePlayerPlaybackDidFinishNotification object: nil];
//  [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( movieScalingModeDidChange: ) name: MPMoviePlayerScalingModeDidChangeNotification object: nil];

	// Poderíamos usar MPMovieScalingModeNone, pois já fazemos os videos na resolução de tela do iPhone. No entanto, utilizei
	// MPMovieScalingModeFill caso desejemos economizar espaço
	//hMoviePlayer.scalingMode = MPMovieScalingModeNone;
	hMoviePlayer.scalingMode = MPMovieScalingModeFill;
    
    // Não deixa o usuário ter controle sobre a execução do vídeo
	hMoviePlayer.movieControlMode = MPMovieControlModeHidden;

    // A cor de background do vídeo poder ser qualquer UIColor
    hMoviePlayer.backgroundColor = [UIColor clearColor];
	
	return true;
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self deallocMoviePlayer];
    [super dealloc];
}

/*==============================================================================================

MENSAGEM deallocMoviePlayer
	Desaloca o componente reprodutor de vídeos.

==============================================================================================*/

- ( void )deallocMoviePlayer
{
	if( hMoviePlayer != NULL )
	{
		// Cancela os eventos da exibição de vídeo
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerContentPreloadDidFinishNotification object:nil];
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object: hMoviePlayer];
//		[[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerScalingModeDidChangeNotification object: hMoviePlayer];
		
		[hMoviePlayer stop];

		// Deleta o moviePlayer
		[hMoviePlayer release];
		hMoviePlayer = NULL;
	}
}

/*==============================================================================================

MENSAGEM onEnd
	Chama a próxima tela do jogo.

==============================================================================================*/

- ( void ) onEnd
{
	[[ApplicationManager GetInstance] performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	// OBS: Antigamente este código ficava em buildSplashGame, mas precisamos garantir que o
	// MoviePlayer é criado na thread principal, por isso passamos o código para
	// onBecomeCurrentScreen
	if( [self createMoviePlayer] )
	{
		// As soon as you call the play: method, the player initiates a transition that fades 
		// the screen from your current window content to the designated background 
		// color of the player. If the movie cannot begin playing immediately, the player 
		// object continues displaying the background color and may also display a progress 
		// indicator to let the user know the movie is loading. When playback finishes, the 
		// player uses another fade effect to transition back to your window content
		[hMoviePlayer play];
		[NSTimer scheduledTimerWithTimeInterval: TIMER_SLEEP_TIME target:self selector:@selector( onEnding ) userInfo:NULL repeats:NO];
	}
	else
	{
		[self onEnding];
	}
}

/*==============================================================================================

MENSAGEM moviePreloadDidFinish
	Mensagem chamada quando o vídeo acaba de ser carregado.

==============================================================================================*/

//- ( void )moviePreloadDidFinish:( NSNotification* )hNotification
//{
//	MPMoviePlayerController* hMoviePlayer = [ hNotification object ];
//}

/*==============================================================================================

MENSAGEM onEnding
	Tenta impedir o flickering no final da exibição do vídeo.

==============================================================================================*/

-( void ) onEnding
{
	#if DEBUG
		LOG( @"SplashGame: Chamou onEnding" );
	#endif

	// Impede o fade no final do vídeo
	UIImageView* hBkg = ( UIImageView* )[[self subviews] objectAtIndex: 0];
	hBkg.hidden = NO;
	[self bringSubviewToFront: hBkg];
	[hBkg setNeedsDisplay];

	[self deallocMoviePlayer];

	[self onEnd];
}

/*==============================================================================================

MENSAGEM moviePlayBackDidFinish
	Mensagem chamada quando o video terminar de ser exibido.

==============================================================================================*/

//- ( void )moviePlayBackDidFinish:( NSNotification* )hNotification
//{
//	// Impede o fade no final do vídeo
//	UIImageView* hBkg = ( UIImageView* )[[self subviews] objectAtIndex: 0];
//	hBkg.hidden = NO;
//	
//	CGRect screenSize = [[UIScreen mainScreen] bounds];
//	[hBkg drawRect: screenSize];
//	[hBkg setNeedsDisplay];
//	
//	[hMoviePlayer stop];
//	[self deallocMoviePlayer];
//
//	[hBkg drawRect: screenSize];
//	[hBkg setNeedsDisplay];
//
//	[self onEnd];
//}

/*==============================================================================================

MENSAGEM movieScalingModeDidChange
	Mensagem chamada quando a escala do video é alterada.

==============================================================================================*/

//- ( void )movieScalingModeDidChange:( NSNotification* )hNotification
//{
//	MPMoviePlayerController* hMoviePlayer = [ hNotification object ];
//}

// Fim da implementação da classe
@end
