/*
 *  TestScene.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 1/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEST_SCENE_H
#define TEST_SCENE_H

#include "Label.h"
#include "Scene.h"
#include "Sprite.h"
#include "ObjectGroup.h"

class TestScene : public Scene
{
	public:
		// Construtor
		TestScene( void );
	
		// Destrutor
		virtual ~TestScene( void );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
	private:
		Font* pFont;
		Label* pText;
		Sprite* pAway;
		ObjectGroup* pGroup;
};

#endif

