#ifndef SAVED_REPLAY_H
#define SAVED_REPLAY_H

#include "Play.h"
#include "Point3f.h"

// Tipos de replay
#define REPLAY_TYPE_NOT_SAVED			0
#define REPLAY_TYPE_PENALTY_TRAINING	1
#define REPLAY_TYPE_PENALTY_MATCH		2
#define REPLAY_TYPE_FOUL_TRAINING		3
#define REPLAY_TYPE_FOUL_CHALLENGE		4

// Estrutura usada para armazenar replays salvos
typedef struct SavedReplay
{
	// atributos comuns:
	Play	play;							// direção, altura, efeito e força do chute
	Point3f	collisionDirection;				// vetor velocidade da bola após colisão com barreira, trave, goleiro
	float	collisionTime;					// intervalo de tempo entre o chute e a colisão que definiu a jogada
	uint8	playResult,						// resultado da jogada (gol, trave, fora, barreira, etc)
			replayType;						// não salvo, pênalti (treino), pênalti (copa), treino de faltas, desafio de faltas
	double	date;							// data da gravação. Ver [NSDate timeIntervalSinceReferenceDate]

	// atributos do modo falta:
	uint8	grassIndex;						// tipo do pattern do gramado
	Point3f	ballPosition,					// posição de origem do chute
			ballPositionAtGoal,				// posição da bola ao cruzar a linha de fundo
			boardCollisionDirection,		// vetor velocidade da bola após colisão com placa de publicidade
			keeperPosition,					// posição do goleiro
			keeperJumpDirection,			// vetor velocidade do pulo do goleiro
			crosshairPosition;				// posição do alvo
} SavedReplay;


#endif
