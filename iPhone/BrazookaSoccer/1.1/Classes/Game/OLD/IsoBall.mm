#include "IsoBall.h"

#include "Tests.h"

#include "Ball_Defines.h"
#include "GameUtils.h"
#include "Iso_Defines.h"
#include "SoccerBall.h"
#include "SoccerBallShadow.h"

#include "Exceptions.h"
#include "MathFuncs.h"
#include "Point2i.h"
#include "Random.h"

// Escala da imagem da aura

// OLD
//#define ISO_BALL_AURA_SCALE_X 1.0f
//#define ISO_BALL_AURA_SCALE_Y ISO_BALL_AURA_SCALE_X

#define ISO_BALL_AURA_SCALE_X 1.0f
#define ISO_BALL_AURA_SCALE_Y ISO_BALL_AURA_SCALE_X

// Offset de posicionamento da aura

// OLD
//#define ISO_BALL_AURA_OFFSET_X ( -1.0f * ISO_BALL_AURA_SCALE_X )
//#define ISO_BALL_AURA_OFFSET_Y (  4.0f * ISO_BALL_AURA_SCALE_Y )

#define ISO_BALL_AURA_OFFSET_X	(- 6.0f * ISO_BALL_AURA_SCALE_X )
#define ISO_BALL_AURA_OFFSET_Y	( 20.0f * ISO_BALL_AURA_SCALE_Y )

// Fator de desaceleração angular da bola após colisões (valor arbitrário)
#define ISO_BALL_ANG_SPEED_SLOW_DOWN_FACTOR 0.73914f

// Índices das animações da aura da bola
#define ISO_BALL_AURA_ANIM_INDEX_LOOPING	0
#define ISO_BALL_AURA_ANIM_INDEX_FADING		0 // É a mesma animação que ISO_BALL_AURA_ANIM_INDEX_LOOPING, porém passamos looping == false

// Aumentamos a área de colisão da bola para evitarmos possíveis problemas
#define ISO_BALL_RADIUS_EXTRA 0.10f

// Profundidade a partir da qual a altura do gol começa a diminuir
#define ISO_BALL_HIGHEST_Y_DEPTH_LIMIT ( -1.5f + REAL_BALL_RADIUS + ISO_BALL_RADIUS_EXTRA )

/*===============================================================================

CONSTRUTOR
   
================================================================================*/

IsoBall::IsoBall( void )
		: Ball(),
		  checkedBoardsCollision( false ),
		  pAura( NULL ),
		  pBall( NULL ),
		  pBallShadow( NULL ),
		  angularSpeed( 0.0f ),
		  angularSpeedAxis(),
		  lastBoardCollisionDirection()
{
	build();
}

/*===============================================================================

DESTRUTOR
   
================================================================================*/

IsoBall::~IsoBall( void )
{
	clear();
}

/*===============================================================================

MÉTODO build
	Inicializa o objeto.

================================================================================*/

void IsoBall::build( void )
{
	{ // Evita erros de compilação por causa dos gotos

		char auraFileName[] = { "au" };
		pAura = new Sprite( auraFileName, auraFileName );
		if( !pAura || !insertObject( pAura ) )
		{
			delete pAura;
			goto Error;
		}
		pAura->setListener( this );
		pAura->setBlenFunc( GL_ONE_MINUS_SRC_ALPHA, GL_ONE );
		pAura->setScale( ISO_BALL_AURA_SCALE_X, ISO_BALL_AURA_SCALE_Y );
		
		pBall = new SoccerBall( this );
		if( !pBall || !insertObject( pBall ) )
		{
			delete pBall;
			goto Error;
		}
		
		pBallShadow = new SoccerBallShadow( pBall );
		if( !pBallShadow )
			goto Error;
		
		float scaleFactor = GameUtils::isoGlobalToPixels( REAL_BALL_DIAMETER, 0.0f, 0.0f ).x;
		pBall->setScale( scaleFactor / pBall->getWidth(), scaleFactor / pBall->getHeight() );

		setSize( pBall->getWidth(), pBall->getHeight() );
		
		// Posiciona a aura da bola
		pAura->setPosition( ( ISO_BALL_AURA_OFFSET_X * pBall->getScaleX() ) + ( ( pBall->getWidth() - pAura->getWidth() ) * 0.5f ),
							( ISO_BALL_AURA_OFFSET_Y * pBall->getScaleY() ) + ( ( pBall->getHeight() - pAura->getHeight() ) * 0.5f ) );
		pAura->setAnimSequence( ISO_BALL_AURA_ANIM_INDEX_LOOPING, true );
		
		return;
		
	} // Evita erros de compilação por causa dos gotos

	// Label de tratamento de erros
	Error:
		clear();

#if TARGET_IPHONE_SIMULATOR
		throw ConstructorException( "IsoBall::build( void ): Unable to create object" );
#else
		throw ConstructorException();
#endif
}

/*===============================================================================

MÉTODO clear
	Limpa as variáveis do objeto.

================================================================================*/

void IsoBall::clear( void )
{
	DELETE( pBallShadow );
	
	checkedBoardsCollision = false;
	
	pAura = NULL;
	pBall = NULL;
	pBallShadow = NULL;

	angularSpeed = 0.0f;
	angularSpeedAxis.set();

	lastBoardCollisionDirection.set();
}

/*===============================================================================

MÉTODO reset
	Reinicializa os atributos do objeto.

================================================================================*/

void IsoBall::reset( const Point3f* pRealPosition )
{
	angularSpeed = 0.0f;
	angularSpeedAxis.set();

	Matrix4x4 identity;
	pBall->setRotation( &identity );

	checkedBoardsCollision = false;
	
	#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
		ballPath.clear();
	#endif
	
	Ball::reset( pRealPosition );
}

/*===============================================================================
 
MÉTODO reset
	Renderiza o objeto.

================================================================================*/

bool IsoBall::render( void )
{
	bool ret = pBallShadow->render();
	ret |= Ball::render();	
	return ret;
}

/*======================================================================================

MÉTODO kick
   Recebe um ponteiro para uma jogada e define os novos valores da velocidade da bola. O
valor retornado refere-se à posição final estimada da bola, quando esta cruza a linha de
fundo. Para o cálculo da posição final da bola, não são considerados possíveis obstáculos
no caminho, como a barreira, goleiro ou jogadores.

========================================================================================*/

Point3f* IsoBall::kick( Point3f* pOut, Play* pPlay, bool replay )
{
	Ball::kick( pOut, pPlay, replay );
	
	angularSpeed = pPlay->angularSpeed;
	angularSpeedAxis = pPlay->angularSpeedAxis;
	
	return pOut;
}

/*===============================================================================

MÉTODO checkBoardsCollision
	Verifica se a bola colidiu com as placas de publicidade no fundo do campo,
atualizando sua velocidade e direção conforme necessário.

=============================================================================== */

// TODOO : Descomentar quando tivermos placas de publicidade
//void IsoBall::checkBoardsCollision( void )
//{
//	if( !checkedBoardsCollision )
//	{
//		if( realPosition.z <= ISO_BACK_BOARDS_Z )
//		{
//			checkedBoardsCollision = true;
//
//			if( realPosition.y <= ISO_BACK_BOARDS_REAL_HEIGHT )
//			{
//				// TODOO : Descomentar quando tivermos as músicas
//				//[(( FreeKickAppDelegate* )APP_DELEGATE ) playAudio: SOUND_INDEX_BOARD Looping: false];
//
//				if( isReplay )
//				{
//					realSpeed = lastBoardCollisionDirection;
//					realInitialYSpeed = realSpeed.y;
//				}
//				else
//				{
//					uint16 rnd = Random::GetInt();
//					realSpeed.z = MIN_Z_SPEED_AFTER_BOARD_COLLISION + ( rnd % MAX_Z_SPEED_AFTER_BOARD_COLLISION );
//
//					// Direção vertical da bola é semi-aleatória após impacto
//					rnd = Random::GetInt();
//					realInitialYSpeed = realSpeed.y = rnd % MAX_Y_SPEED_AFTER_BOARD_COLLISION;
//
//					lastBoardCollisionDirection = realSpeed;
//				}
//				accTime = 0.0f;
//				accTimeY = 0.0f;
//				realInitialPosition = realPosition;
//			}
//		}
//	}
//}

/*===============================================================================

MÉTODO update
	Atualiza o objeto.
   
=============================================================================== */

bool IsoBall::update( float timeElapsed )
{
#if DEBUG && IS_CURR_TEST( TEST_BALL_SHADOW )
	return true;
#else
	if( !Ball::update( timeElapsed ) )
		return false;

	if( NanoMath::fdif( angularSpeed, 0.0f ) )
		pBall->rotate( angularSpeed * timeElapsed, angularSpeedAxis.x, angularSpeedAxis.y, angularSpeedAxis.z );

	switch( ballState )
	{
		case BALL_STATE_GOAL:
		case BALL_STATE_POST_GOAL:
		case BALL_STATE_POST_BACK:
		case BALL_STATE_REJECTED_KEEPER:
		case BALL_STATE_REJECTED_BARRIER:
			slowDownAngSpeed( timeElapsed );
			break;

		case BALL_STATE_OUT:
		case BALL_STATE_POST_OUT:
			// TODOO : Descomentar quando tivermos placas de publicidade
			//checkBoardsCollision();

			slowDownAngSpeed( timeElapsed );
			break;
	}

	return true;
#endif
}

/*===============================================================================

MÉTODO slowDownAngSpeed
	Reduz a velocidade angular da bola após colisões.

=============================================================================== */

void IsoBall::slowDownAngSpeed( float timeElapsed )
{
	angularSpeed -= angularSpeed * ISO_BALL_ANG_SPEED_SLOW_DOWN_FACTOR * timeElapsed;
}

/*===============================================================================

MÉTODO keepBallInsideGoal

	Evita que a bola "fure" a rede após um gol

=============================================================================== */

void IsoBall::keepBallInsideGoal( void )
{
	Point3f currRealPos = *( getRealPosition() );

	if( ( currRealPos.z - REAL_BALL_RADIUS - ISO_BALL_RADIUS_EXTRA ) <= REAL_GOAL_DEPTH )
		currRealPos.z = REAL_GOAL_DEPTH + REAL_BALL_RADIUS + ISO_BALL_RADIUS_EXTRA;

	if( currRealPos.x <= ( REAL_LEFT_POST_START + ISO_BALL_RADIUS_EXTRA ) )
		currRealPos.x = REAL_LEFT_POST_START + ISO_BALL_RADIUS_EXTRA;
	else if( currRealPos.x >= ( REAL_RIGHT_POST_START - ISO_BALL_RADIUS_EXTRA ) )
		currRealPos.x = REAL_RIGHT_POST_START - ISO_BALL_RADIUS_EXTRA;

	float highestY = REAL_BAR_BOTTOM - ISO_BALL_RADIUS_EXTRA;

	if( currRealPos.z < ISO_BALL_HIGHEST_Y_DEPTH_LIMIT )
	{
		float zAux = REAL_GOAL_DEPTH + REAL_BALL_RADIUS + ISO_BALL_RADIUS_EXTRA;
		float percent = NanoMath::clamp( -( currRealPos.z - ISO_BALL_HIGHEST_Y_DEPTH_LIMIT ) / ( -zAux + ISO_BALL_HIGHEST_Y_DEPTH_LIMIT ), 0.0f, 1.0f );

		highestY = NanoMath::lerp( percent, highestY, 0.0f );
	}

	if( currRealPos.y >= highestY )
	{
		currRealPos.y = highestY;
		realInitialPosition.y = currRealPos.y;

		realSpeed.y = 0.0f;
		realInitialYSpeed = 0.0f;

		accTimeY = 0.0f;
	}
	
	setRealPosition( &currRealPos );
}

/*===============================================================================

MÉTODO handleBallOut
	Trata a movimentação da bola após ela ir para fora. Testa colisão da bola
com a rede, de forma que a bola não "fure" a rede pelo lado de fora.

=============================================================================== */

void IsoBall::handleBallOut( void )
{
	// Evita que a bola "fure" a rede pelo lado de fora
	if( realPosition.y <= REAL_FLOOR_TO_BAR && realPosition.z <= 0.0f && realPosition.z >= REAL_GOAL_DEPTH )
	{
		if( realSpeed.x > 0.0f )
		{
			if( ( realPosition.x > ( -REAL_GOAL_WIDTH * 0.5f ) ) && ( realPosition.x < ( REAL_GOAL_WIDTH * 0.5f ) ) )
			{
				realPosition.x = ( -REAL_GOAL_WIDTH * 0.5f );
				realSpeed.x = 0;
			}
		}
		else
		{
			if( ( realPosition.x < ( REAL_GOAL_WIDTH * 0.5f ) ) && ( realPosition.x > ( -REAL_GOAL_WIDTH * 0.5f ) ) )
			{
				realPosition.x = REAL_GOAL_WIDTH * 0.5f;
				realSpeed.x = 0;
			}
		}
	}
}

/*===============================================================================

MÉTODO updatePosition
	Atualiza a posição da bola na tela (não altera a posição real).
   
=============================================================================== */

void IsoBall::updatePosition( void )
{
	#if DEBUG && IS_CURR_TEST( TEST_BALL_ROTATION )
		realPosition.set( 2.0f );
	#endif

	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPosition( p.x + ISO_BALL_X_OFFSET - ( getWidth() * 0.5f ), p.y + ISO_BALL_Y_OFFSET - ( getHeight() * 0.5f ) );
	
	#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
	if( getState() == BALL_STATE_MOVING )
		ballPath.push_back( Point3f( p.x + ISO_BALL_X_OFFSET, p.y + ISO_BALL_Y_OFFSET, 0.0f ) );
	#endif

	pBallShadow->setRealPosition( realPosition.x, 0.0f, realPosition.z );
}

/*===============================================================================

MÉTODO getTouchCollisionArea
	Obtém a área da bola que serve como área de colisão para toques. Esta área
pode ser maior ou menor do que a área do grupo.
   
=============================================================================== */

void IsoBall::getTouchCollisionArea( Triangle& t0, Triangle& t1 )
{
	Point3f auraPos = *getPosition() + *( pAura->getPosition() );
	Point3f p0( auraPos.x, auraPos.y );
	Point3f p1( auraPos.x, auraPos.y + pAura->getHeight() );
	Point3f p2( auraPos.x + pAura->getWidth(), auraPos.y );
	Point3f p3( auraPos.x + pAura->getWidth(), auraPos.y + pAura->getHeight() );

	t0.set( &p0, &p1, &p2 );
	t1.set( &p1, &p2, &p3 );
}

/*===============================================================================

MÉTODO onAnimEnded
	Chamado quando um sprite termina uma sequência de animação.

================================================================================*/

void IsoBall::onAnimEnded( Sprite* pSprite )
{
	if( pSprite == pAura )
	{
		if( !pAura->isCurrAnimSequenceLooping() )
			setAuraState( BALL_AURA_STATE_HIDDEN );
	}
}

/*===============================================================================

MÉTODO setAuraState
	Determina o estado da aura da bola.
   
================================================================================*/

void IsoBall::setAuraState( BallAuraState state )
{
	bool visibleAndActive = true;
	switch( state )
	{
		case BALL_AURA_STATE_HIDDEN:
			visibleAndActive = false;
			break;
			
		case BALL_AURA_STATE_LOOPING:
			pAura->setAnimSequence( ISO_BALL_AURA_ANIM_INDEX_LOOPING, true );
			break;
			
		case BALL_AURA_STATE_FADING_OUT:
			// Impede um corte na animação atual
			int16 loopingSequenceStep = pAura->getCurrAnimSequenceStep();
			pAura->setAnimSequence( ISO_BALL_AURA_ANIM_INDEX_FADING, false );
			pAura->setCurrentAnimSequenceStep( loopingSequenceStep );
			break;
	}

	pAura->setActive( visibleAndActive );
	pAura->setVisible( visibleAndActive );
}

/*===============================================================================

MÉTODO getAuraState
	Retorna o estado da aura da bola.
   
================================================================================*/

BallAuraState IsoBall::getAuraState( void ) const
{
	if( !pAura->isVisible() )
		return BALL_AURA_STATE_HIDDEN;
	
	return pAura->isCurrAnimSequenceLooping() == ISO_BALL_AURA_ANIM_INDEX_LOOPING ? BALL_AURA_STATE_LOOPING : BALL_AURA_STATE_FADING_OUT;
}
