#ifndef BALL_TARGET_H
#define BALL_TARGET_H 1

// OLD
//#include "Sprite.h"
#include "RenderableImage.h"

#include "RealPosOwner.h"

enum BallTargetState
{
	BALL_TARGET_UNHIT = 0,
	BALL_TARGET_HIT,
};

//OLD
//class BallTarget : public RealPosOwner, public Sprite
class BallTarget : public RealPosOwner, public RenderableImage
{
	public:
		// Construtor
		// Exceções: ConstructorException
		BallTarget( void );
	
		// Destrutor
		virtual ~BallTarget( void );

		// Reinicializa o objeto, posicionando a mira no centro do gol
		void reset( void );
		
		// Reinicializa o objeto, gerando uma posição para a mira relativa à posição de falta
		void reset( const Point3f* pBallPosition );
	
		// Indica se a bola acertou o centro do alvo
		bool hitBullsEye( const Point3f* pBallPosition ) const;
	
		// Obtém a área do alvo que serve como área de colisão para toques. Esta área
		// pode ser maior ou menor do que a área do objeto
		void getTouchCollisionArea( Triangle& t0, Triangle& t1 );
	
		// Determina o estado do objeto
		void setState( BallTargetState state );
	
		// Retorna o estado do objeto
		inline BallTargetState getState( void ) const { return currState; } ;
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

	private:
		// Atualiza a posição do objeto na tela
		virtual void updatePosition( void );
	
		// Inicializa o objeto
		// Exceções: ConstructorException
		//void build( void );
	
		// Auxiliar de animação no estado AIM_STATE_BLINKING
		bool fadingIn;
	
		// Estado atual do objeto
		BallTargetState currState;
	
		// Auxiliares de animação
		uint8 blinksCounter;
		float blinkAux;
};

#endif
