/*
 *  GameInfo.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GAMEINFO_H
#define GAMEINFO_H 1

// Components
#include "NanoTypes.h"

// Game
#include "Config.h"
#include "Iso_Defines.h"

enum GameMode
{
	GAME_MODE_UNDEFINED = -1,
	GAME_MODE_TRAINING = 0,
	GAME_MODE_RANKING = 1/*,
	GAME_MODE_STORY*/
};

enum TryResult
{
	TRY_RESULT_UNDEFINED = -1,
	TRY_RESULT_NOT_USED = 0,
	TRY_RESULT_GOAL = 1,
	TRY_RESULT_OUT = 2
};

struct GameInfo
{
	// Construtor
	GameInfo( void );
	
	// Reseta as vaiáveis de controles do jogo
	void onNewGame( void );
	
	// Indica se a câmera deve auto-ajustar o foco quando o jogador confirmar a posição de chute
	// ou se deve permanecer com o foco que o jogador configurou
	bool cameraAutoAdjust;
	
	// Resultados das jogadas
	TryResult tries[ FOUL_CHALLENGE_TRIES_PER_FOUL ];
	
	// Jogada atual
	int8 currTry;
	
	// Fase na qual o jogador se encontra
	int16 currLevel;

	// Pontuação acumulada na fase atual
	int32 levelScore;
	
	// Pontuação total do jogador durante o jogo
	uint32 points;
	
	// Mode de jogo atual
	GameMode gameMode;
	
	///<DM>

	///posição da bola ao começar a jogada
	Point3f iSavedBallPosition;
	
 	///posição do alvo ao começar a jogada
	Point3f iSavedTargetPosition;
	
	///indices de barreira no level
	int8 barrierPlayersVector[ BARRIER_MAX_N_PLAYERS ];
	
	///qtd dos jogados da barreira
	int8 barrierSize;
	
	float iBackupAngularSpeed;
	Point3f iBackupAngularSpeedAxis;
	
	///</DM>
};

#endif

