#include "CornerFlag.h"

#include "GameUtils.h"

// Posição âncora das bandeiras de escanteio
#define FLAG_ANCHOR_POS_X 86.0f
#define FLAG_ANCHOR_POS_Y 108.0f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

CornerFlag::CornerFlag( float offsetX, float offsetY )
		#if CONFIG_TEXTURES_16_BIT
			: Sprite( "fl", "fl", ResourceManager::Get16BitTexture2D )
		#else
			: Sprite( "fl", "fl" )
		#endif
		, offsetX( offsetX ), offsetY( offsetY )
{
	setAnchor( FLAG_ANCHOR_POS_X, FLAG_ANCHOR_POS_Y, 0.0f );
}

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

CornerFlag::CornerFlag( float offsetX, float offsetY, const CornerFlag* pBase ) : Sprite( pBase ), offsetX( offsetX ), offsetY( offsetY )
{
	setAnchor( FLAG_ANCHOR_POS_X, FLAG_ANCHOR_POS_Y, 0.0f );
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

CornerFlag::~CornerFlag( void )
{
}

/*===========================================================================================
 
MÉTODO GetCornerFlagAnchorX
 
============================================================================================*/

float CornerFlag::GetCornerFlagAnchorX( void )
{
	return FLAG_ANCHOR_POS_X;
}

/*===========================================================================================
 
MÉTODO GetCornerFlagAnchorY
 
============================================================================================*/

float CornerFlag::GetCornerFlagAnchorY( void )
{
	return FLAG_ANCHOR_POS_Y;
}

/*===========================================================================================
 
MÉTODO updatePosition
	Atualiza a posição do objeto na tela.
 
============================================================================================*/

void CornerFlag::updatePosition( void )
{
	Point3f flagPos = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( flagPos.x + offsetX, flagPos.y + offsetY, 0.0f );
}
