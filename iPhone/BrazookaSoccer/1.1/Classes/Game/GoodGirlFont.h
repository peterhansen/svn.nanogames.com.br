/*
 *  GoodGirlFont.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 7/8/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GOOD_GIRL_FONT_H
#define GOOD_GIRL_FONT_H 1

#include "Font.h"

class GoodGirlFont : public Font
{	
	public:
		// Construtores
		// Exceções: InvalidArgumentException
		GoodGirlFont( void );
	
		// Destrutor
		virtual ~GoodGirlFont( void );
	
		// Retorna o tamanho do caractere na fonte
		virtual Point2i* getCharSize( Point2i* pOut, unichar c ) const;
	
		// Renderiza o texto com o espaçamento de caracteres desejado

		virtual void renderText( const char* pText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const;

		// OLD
//		virtual void renderText( const NSString* hText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const;
};

#endif
