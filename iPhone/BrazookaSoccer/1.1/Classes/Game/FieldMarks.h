/*
 *  FieldMarks.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FIELD_MARKS_H
#define FIELD_MARKS_H

#include "Color.h"
#include "Object.h"
#include "Texture2D.h"

#include <vector>

class FieldMarks : public Object
{
	public:
		// Construtor
		FieldMarks( void );
	
		// Destrutor
		virtual ~FieldMarks( void );

		// Renderiza o objeto
		virtual bool render( void );
	
		// Determina a cor utilizada nas marcações do campo
		void setColor( const Color& color );

	private:
		// Inicializa o objeto
		void build( void );
	
		// Renderiza uma linha, ponto a ponto, utilizando o algoritmo de Bresenham
		static void PreProcessLine( std::vector< int16 >& tempBuffer, int32 x0, int32 y0, int32 x1, int32 y1 );

		// Renderiza um círculo, ponto a ponto, utilizando o algoritmo de Bresenham
		static void PreProcessCircle( std::vector< int16 >& tempBuffer, const Point3f& center, float radius, float lod, float startAngle = 0.0f, float endAngle = 360.0f );
	
		// Renderiza um arco de círculo, ponto a ponto, utilizando o algoritmo de Bresenham.
		static void PreProcessArc( std::vector< int16 >& tempBuffer, int32 centerX, int32 centerY, int32 radius, int32 startAngle, int32 endAngle );
	
		// Verifica se o ponto x, y pertence ao arco que deve ser criado
		static void KeepIfArcPoint( std::vector< int16 >& tempBuffer, int32 centerX, int32 centerY, int32 x, int32 y, int32 startAngle, int32 endAngle );
	
		// Buffer para otimizar a renderização da linha
		int32 bufferSize;
		int16* pBuffer;

		// Cor utilizada nas marcações do campo
		Color marksColor;
	
		// Textura aplicada nos pontos da linha
		Texture2DHandler texture;
};

// Determina a cor utilizada nas marcações do campo
inline void FieldMarks::setColor( const Color& color )
{
	marksColor = color;
}

#endif