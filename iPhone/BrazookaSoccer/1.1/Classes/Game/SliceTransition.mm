#include "SliceTransition.h"

#include "GLHeaders.h"
#include "ObjcMacros.h"
#include "Quad.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

SliceTransition::SliceTransition( uint8 nSlices, float duration, OGLTransitionListener* pListener, const Color* pColor )
		   : OGLTransition( pListener ), nSlices( nSlices ),
			 maxQuadWidth( SCREEN_WIDTH / nSlices ), widthCounter( 0.0f ),
			 transitionSpeed( maxQuadWidth / duration ),
			 color( pColor == NULL ? Color( COLOR_BLACK ) : *pColor )
{

}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

SliceTransition::~SliceTransition( void )
{
}

/*==============================================================================================

MÉTODO render
	Renderiza o objeto.

==============================================================================================*/

bool SliceTransition::render( void )
{
	if( !OGLTransition::render() || ( color.getUbA() == 0 ) )
		return false;
	
	glColor4ub( color.getUbR(), color.getUbG(), color.getUbB(), color.getUbA() );
	glMatrixMode( GL_MODELVIEW );

	Quad q;
	q.disableRenderStates( RENDER_STATE_TEXCOORD_ARRAY | RENDER_STATE_COLOR_ARRAY | RENDER_STATE_NORMAL_ARRAY );
	
	for( uint8 i = 0 ; i < nSlices ; ++i )
	{
		glPushMatrix();
	
		glTranslatef( ( maxQuadWidth * 0.5f ) + ( i * maxQuadWidth ), HALF_SCREEN_HEIGHT, 0.0f );
		glScalef( widthCounter, SCREEN_HEIGHT, 1.0f );
		
		q.render();
		
		glPopMatrix();
	}

	return true;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/

bool SliceTransition::update( float timeElapsed )
{
	if( !OGLTransition::update( timeElapsed ) )
		return false;
	
	if( widthCounter > maxQuadWidth )
		pListener->onOGLTransitionEnd();
	else
		widthCounter += transitionSpeed * timeElapsed;
	
	return true;
}

/*==============================================================================================

MÉTODO update
	Reinicializa o objeto.

==============================================================================================*/

void SliceTransition::reset( void )
{
	widthCounter = 0.0f;
}

