/*
 *  DirectionMeter.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef	DIRECTION_METER_H
#define DIRECTION_METER_H 1

#include "ObjectGroup.h"

#include "InterfaceControl.h"
#include "MeterBar.h"

// Forward Declarations
class InterfaceControlListener;

class DirectionMeter : public ObjectGroup, public MeterBar, public InterfaceControl
{
	public:
		// Construtor
		// Exceções: ConstructorException
		DirectionMeter( float minValue, float maxValue, float initValue, InterfaceControlListener* pControlListener );
	
		// Destrutor
		virtual ~DirectionMeter( void );
	
		// Determina o valor atual do medidor (porcentagem)
		virtual void setCurrValuePercent( float percent );
	
		// Configura os parâmetros das animações
		void configAnim( float finalY );
	
		// Faz uma animação para exibir o controle
		virtual void startShowAnimation( void );
	
		// Faz uma animação para esconder o controle
		virtual void startHideAnimation( void );
	
		// Determina o estado de exebição do controle
		virtual void setInterfaceControlState( InterfaceControlState state, bool notifyListener = false );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

	private:
		#if TARGET_IPHONE_SIMULATOR
			// Método que trata os eventos enviados pelo sistema
			virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
		#endif
	
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );
	
		// Auxiliares das animações
		float animFinalY;
	
		// Controlador de sons
		bool soundFlag;
};

#endif
