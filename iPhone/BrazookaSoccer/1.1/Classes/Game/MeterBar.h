/*
 *  MeterBar.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef METER_BAR_H
#define METER_BAR_H 1

#if TARGET_IPHONE_SIMULATOR
	#include "EventListener.h"
#endif

#if TARGET_IPHONE_SIMULATOR
class MeterBar : public EventListener
#else
class MeterBar
#endif
{
	public:
		// Construtor
		MeterBar( float minValue, float maxValue, float initValue );
	
		// Destrutor
		virtual ~MeterBar( void );
	
		// Determina o intervalo de valores aceito pelo medidor
		void setMeterInterval( float min, float max );
	
		// Determina o valor atual do medidor (absoluto)
		void setCurrValue( float value );
	
		// Determina o valor atual do medidor (porcentagem)
		virtual void setCurrValuePercent( float percent );
	
		// Retorna o valor atual do medidor
		float getCurrValue( void ) const;

	private:
		// Intervalo de valores permitido pelo medidor
		float minValue, maxValue;
	
		// Valor atual do medidor
		float currValue;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

inline float MeterBar::getCurrValue( void ) const
{
	return currValue;
};

#endif
