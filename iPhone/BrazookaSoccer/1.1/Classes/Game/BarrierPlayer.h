/*
 *  BarrierPlayer.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BARRIER_PLAYER_H
#define BARRIER_PLAYER_H 1

#include "GenericIsoPlayer.h"

class BarrierPlayer : public GenericIsoPlayer
{
	public:
		// Destrutor
		virtual ~BarrierPlayer( void );
	
		// Retorna a largura real do jogador da barreira (em metros)
		virtual float getRealWidth( void ) const = 0;
	
		// Reinicializa o objeto
		virtual void reset( const Point3f* pBallPosition = NULL ){ reactedToBall = false; setBallCollisionTest( true ); };
	
		// Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
		// que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente
		virtual void prepare( const Point3f& ballRealPos ) = 0;
	
		// Testa colisão deste objeto com a área passada como parâmetro
		virtual bool checkCollision( const CollisionQuad& area ){ return false; };
	
		// Faz com que o jogador exiba alguma reação devido à aproximação da bola
		virtual void onBallGettingClose( void ){ reactedToBall = true; };
	
		// Retorna o índice do som que deve ser tocado quando a bola colide com este jogador
		virtual void getCollisionSoundNameAndIndex( uint8* pName, uint8* pIndex ) const;
	
		// Determina o vetor ortogonal ao vetor de direção da barreira à bola
		void setOrtho( const Point3f *pVector );
	
		// Retorna o vetor ortogonal ao vetor de direção da barreira à bola
		Point3f* getOrtho( Point3f* pOut ) const;
	
	protected:
		// Construtor
		explicit BarrierPlayer( uint16 maxObjects );
	
		// Indica se o jogador já respondeu à aproximação da bola
		bool didReactToBall( void ) const { return reactedToBall; };
	
	private:
		// Indica se o jogador reagiu à aproximação da bola
		bool reactedToBall;
	
		// Vetor ortogonal ao vetor de direção da barreira à bola
		Point3f ortho;
};

// Implementação dos métodos inline

inline void BarrierPlayer::setOrtho( const Point3f *pVector )
{
	ortho = *pVector;
}

inline Point3f* BarrierPlayer::getOrtho( Point3f* pOut ) const
{
	if( pOut )
		*pOut = ortho;
	return pOut;
}

#endif
