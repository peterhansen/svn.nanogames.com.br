#include "Aim.h"

#include "Exceptions.h"
#include "ObjcMacros.h"
#include "RenderableImage.h"

#include "FreeKickAppDelegate.h"

// Define o número de objetos no grupo
#define AIM_N_OBJS 2

// Define os índices dos objetos do grupo
#define AIM_OBJ_INDEX_SPRITE_BLINK	0
#define AIM_OBJ_INDEX_SPRITE_SHOT	1

// Duração da animação de fade in / fade out
#define AIM_FADE_ANIM_DUR 1.080f

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

Aim::Aim( void ) : ObjectGroup( AIM_N_OBJS ), fadingIn( false ), state( AIM_STATE_UNDEFINED ), radius( 0.0f ), pListener( NULL )
{
	build();
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

Aim::~Aim( void )
{
	pListener = NULL;
}

/*===========================================================================================
 
MÉTODO getCenterPosition
	Retorna a posição do centro da mira.

============================================================================================*/

Point3f Aim::getCenterPosition( void ) const
{
	return Point3f( position.x + ( getWidth() * 0.5f ), position.y + ( getHeight() * 0.5f ), 0.0f );
}

/*===========================================================================================
 
MÉTODO getCenterPosition
	Posiciona a mira de acordo com o seu centro.

============================================================================================*/

void Aim::setCenterPosition( float x, float y )
{
	setPosition( x - ( getWidth() * 0.5f ), y - ( getHeight() * 0.5f ), position.z );
}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/

void Aim::build( void )
{
	{  // Evita problemas com os gotos
		
		char blinkingFileName[] = "m";

		#if CONFIG_TEXTURES_16_BIT
			// OLD
			//Sprite* pBlinking = new Sprite( blinkingFileName, blinkingFileName, ResourceManager::Get16BitTexture2D );
			RenderableImage* pBlinking = new RenderableImage( blinkingFileName, RenderableImage::CreateDefaultVertexSet, ResourceManager::Get16BitTexture2D );
		#else
			// OLD
			//Sprite* pBlinking = new Sprite( blinkingFileName, blinkingFileName );
			RenderableImage* pBlinking = new RenderableImage( blinkingFileName );
		#endif

		if( !pBlinking || !insertObject( pBlinking ) )
		{
			delete pBlinking;
			goto Error;
		}
		
		TextureFrame frame( 0.0f, 0.0f, 60.0f, 60.0f, 0.0f, 0.0f );
		pBlinking->setImageFrame( frame );

		pBlinking->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		
		char shotFileName[] = "s";
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pShot = new Sprite( shotFileName, shotFileName, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pShot = new Sprite( shotFileName, shotFileName );
		#endif
		if( !pShot || !insertObject( pShot ) )
		{
			delete pShot;
			goto Error;
		}
		pShot->setListener( this );

		setSize( pBlinking->getWidth(), pBlinking->getHeight() );
		
		radius = pBlinking->getWidth() * 0.5f;
		
		setState( AIM_STATE_UNDEFINED );

	} // Evita problemas com os gotos
	
	return;

	// Label de tratamento de erros
	Error:
#if TARGET_IPHONE_SIMULATOR
		throw ConstructorException( "void Aim::build( void ): Unable to create object" );
#else
		throw ConstructorException();
#endif
}

/*===========================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/

bool Aim::update( float timeElapsed )
{
	switch( state )
	{
		case AIM_STATE_BLINKING:
			{
				if( !ObjectGroup::update( timeElapsed ) )
					return false;

				RenderableImage* pBlink = static_cast< RenderableImage* >( getObject( AIM_OBJ_INDEX_SPRITE_BLINK ) );
				
				Color currColor;
				pBlink->getVertexSetColor( &currColor );

				if( fadingIn )
				{
					currColor.setFloatA( currColor.getFloatA() + ( timeElapsed / AIM_FADE_ANIM_DUR ) );

					if( currColor.getFloatA() >= 1.0f )
					{
						currColor.setFloatA( 1.0f );
						fadingIn = false;
					}	
				}
				else
				{
					currColor.setFloatA( currColor.getFloatA() - ( timeElapsed / AIM_FADE_ANIM_DUR ) );

					if( currColor.getFloatA() <= 0.0f )
					{
						currColor.setFloatA( 0.0f );
						fadingIn = true;
					}
				}

				pBlink->setVertexSetColor( currColor );
			}
			return true;
			
		case AIM_STATE_SHOT:
			return ObjectGroup::update( timeElapsed );

		case AIM_STATE_UNDEFINED:
			return false;
	}

	return false;
}

/*===========================================================================================
 
MÉTODO setState
	Chamado quando o sprite termina um sequência de animação.

============================================================================================*/

void Aim::setState( AimState s )
{
	state = s;

	RenderableImage* pBlink = static_cast< RenderableImage* >( getObject( AIM_OBJ_INDEX_SPRITE_BLINK ) );
	Sprite* pShot = static_cast< Sprite* >( getObject( AIM_OBJ_INDEX_SPRITE_SHOT ) );
	
	pBlink->setActive( false );
	pBlink->setVisible( false );
	
	pShot->setActive( false );
	pShot->setVisible( false );
	
	switch( state )
	{
		case AIM_STATE_UNDEFINED:
			break;
		
		case AIM_STATE_BLINKING:
			{
				Color transparentWhite( static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 255 ), static_cast< uint8 >( 0 ) );
				pBlink->setVertexSetColor( transparentWhite );
				fadingIn = true;

				pBlink->setActive( true );
				pBlink->setVisible( true );
			}
			break;
			
		case AIM_STATE_SHOT:
			pShot->setAnimSequence( 0, false );
			pShot->setActive( true );
			pShot->setVisible( true );
			
			[(( FreeKickAppDelegate* ) APP_DELEGATE ) playAudioNamed:SOUND_NAME_FX_AIM_CONFIRM AtIndex:SOUND_INDEX_FX_AIM_CONFIRM Looping: false];
			break;

#if TARGET_IPHONE_SIMULATOR
		default:
			throw InvalidArgumentException( "void Aim::setState( AimState s ): Invalid state" );
			break;
#endif
	}
}

/*===========================================================================================
 
MÉTODO onAnimEnded
	Chamado quando o sprite termina um sequência de animação.

============================================================================================*/

void Aim::onAnimEnded( Sprite* pSprite )
{
	pListener->onAnimEnded( NULL );
}
