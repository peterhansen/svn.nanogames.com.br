/*
 *  BrazookaRankingFeed.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/19/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef BRAZOOKA_RANKING_FEED_H
#define BRAZOOKA_RANKING_FEED_H

#include "NOFacebookFeed.h"

class BrazookaRankingFeed : public NOFacebookFeed
{
	public:
		// Construtor e Destrutor
		BrazookaRankingFeed( void );
		BrazookaRankingFeed( const int64& score, const NOCustomer *pFeedTarget );
		virtual ~BrazookaRankingFeed( void );
	
	private:
		// Escreve os parâmetros específicos do feed
		virtual void writeFeedParams( MemoryStream& stream );
	
		// Entrada do ranking que deveremos
		int64 rnkScore;
};

#endif
