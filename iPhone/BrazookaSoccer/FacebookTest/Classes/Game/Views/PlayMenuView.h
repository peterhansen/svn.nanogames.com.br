//
//  PlayMenuView.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#ifndef PLAY_MENU_H
#define PLAY_MENU_H 1

// Game
#include "GameBasicView.h"
#include "ConfirmLoadView.h"

// C++
#include <vector>

enum PlayMenuViewState
{
	PLAY_MENU_VIEW_STATE_UNDEFINED = -1,
	PLAY_MENU_VIEW_STATE_HIDDEN = 0,
	PLAY_MENU_VIEW_STATE_SHOWING_STEP_0,
	PLAY_MENU_VIEW_STATE_SHOWING_STEP_1,
	PLAY_MENU_VIEW_STATE_SHOWING_STEP_2,
	PLAY_MENU_VIEW_STATE_SHOWING_STEP_3,
	PLAY_MENU_VIEW_STATE_SHOWN,
	PLAY_MENU_VIEW_STATE_HIDING
};

// Forward Declarations
class GameInfo;

@interface PlayMenuView : GameBasicView
{
	@private
		// Ponteiro para os dados do jogo
		GameInfo *pMyGameInfo;
	
		// Handlers para as imagens caso queiramos fazer animações
		IBOutlet UIImageView *hImgHair;
		IBOutlet UIImageView *hIcon;

		// Handlers para os controles da interface
		IBOutlet UITextView *hText;
		IBOutlet UIButton *hBtStart, *hBtTraining, *hBtEndless, *hBtBack;
		
		// Estado atual da view
		PlayMenuViewState currState;
	
		// Auxiliares
		float largestWidth;
	
		// Controlador dos sons
		std::vector< bool > soundFlags;
		
		///<DM>
		ConfirmLoadView *hConfirmPopUp;
		///</DM>
}

// Atualiza a view
- ( void ) update:( float )timeElapsed;

// Método chamado antes de iniciarmos uma transição para esta view
- ( void )onBeforeTransition:( GameInfo* )pGameInfo;

// Método chamado quando a view se torna a view principal da aplicação
- ( void ) onBecomeCurrentScreen;

// Indica que o usuário está pressionando uma opção do menu
- ( IBAction ) onBtPressed:( UIButton* )hButton;

// Mensagem chamada quando o jogador seleciona a opção "Back"
- ( IBAction ) onBack;



@end

#endif
