/*
 *  SliceTransition.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 3/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SLICE_TRANSITION_H
#define SLICE_TRANSITION_H

#include "Color.h"
#include "OGLTransition.h"

class SliceTransition : public OGLTransition
{
	public:
		// Construtor
		SliceTransition( uint8 nSlices, float duration, OGLTransitionListener* pListener, const Color* pColor = NULL );

		// Destrutor
		virtual ~SliceTransition( void );

		// Renderiza o objeto
		virtual bool render( void );

		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Reinicializa o objeto
		virtual void reset( void );

	private:
		// Número de faixas no qual a tela será cortada
		uint8 nSlices;
	
		// Controladores da transição
		float maxQuadWidth;
		float widthCounter;
	
		// Velocidade com a qual a cortina cresce
		float transitionSpeed;

		// Cor da cortina que irá cobrir a tela
		Color color;
};

#endif
