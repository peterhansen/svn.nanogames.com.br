#include "SleepyBarrierPlayer.h"

#include "Exceptions.h"
#include "Sprite.h"

#include "GameUtils.h"

// Número de objetos contidos no grupo
#define SLEEPY_N_OBJS 2

// Índices dos objetos contidos no grupo
#define SLEEPY_OBJ_INDEX_SHADOW 0
#define SLEEPY_OBJ_INDEX_PLAYER 1

// Altura e largura deste personagem
#define SLEEPY_WIDTH	0.90f
#define SLEEPY_HEIGHT	1.70f

// Offset do pixel de referência do personagem
#define SLEEPY_FEET_OFFSET_Y 18.0f

// TODOO : Acrescentar o "zzzzzzzz" no update

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

SleepyBarrierPlayer::SleepyBarrierPlayer( void ) : BarrierPlayer( SLEEPY_N_OBJS )
{
	{ // Evita erros de compilação por causa dos gotos

		// Aloca o personagem
		char aux[] = "slps";
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pShadow = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pShadow = new Sprite( aux, aux );
		#endif
		if( !pShadow || !insertObject( pShadow ) )
		{
			delete pShadow;
			goto Error;
		}
		
		aux[3] = '\0';
		
		#if CONFIG_TEXTURES_16_BIT
			Sprite* pSleepy = new Sprite( aux, aux, ResourceManager::Get16BitTexture2D );
		#else
			Sprite* pSleepy = new Sprite( aux, aux );
		#endif
		if( !pSleepy || !insertObject( pSleepy ) )
		{
			delete pSleepy;
			goto Error;
		}
		pSleepy->setListener( this );

		// Determina o tamanho do grupo
		setSize( pSleepy->getWidth(), pSleepy->getHeight() );
		
		float scaleFactor = static_cast< float >( fabs( GameUtils::isoGlobalToPixels( 0.0f, SLEEPY_HEIGHT, 0.0f ).y ) / pSleepy->getStepFrameHeight() );
		setScale( scaleFactor, scaleFactor );
		
		#if DEBUG
			setName( "negao" );
		#endif

		return;
		
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
	#if DEBUG
		throw ConstructorException( "SleepyBarrierPlayer::SleepyBarrierPlayer( void ): Unable to create object" );
	#else
		throw ConstructorException();
	#endif
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

SleepyBarrierPlayer::~SleepyBarrierPlayer( void )
{
}

/*===========================================================================================
 
MÉTODO getRealWidth
	Retorna a largura real do jogador da barreira (em metros).

============================================================================================*/

float SleepyBarrierPlayer::getRealWidth( void ) const
{
	return SLEEPY_WIDTH - 0.30f;
}

/*===========================================================================================
 
MÉTODO prepare
	Carrega a animação correspondente à direção para a qual a barreira esta virada. Assume
que o método GenericIsoPlayer::lookAt( const Point3f*, bool ) foi chamado anteriormente.

============================================================================================*/

void SleepyBarrierPlayer::prepare( const Point3f& ballRealPos )
{
	Sprite* pSleepy = static_cast< Sprite* >( getObject( SLEEPY_OBJ_INDEX_PLAYER ) );
	switch( direction )
	{
		case DIRECTION_DOWN:
			pSleepy->setAnimSequence( 0, false );
			
			if( pSleepy->isMirrored( MIRROR_HOR ) )
				pSleepy->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_DOWN_LEFT:
		case DIRECTION_LEFT:
			pSleepy->setAnimSequence( 1, false );

			if( pSleepy->isMirrored( MIRROR_HOR ) )
				pSleepy->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_LEFT:
			pSleepy->setAnimSequence( 2, false );
			
			if( pSleepy->isMirrored( MIRROR_HOR ) )
				pSleepy->unmirror( MIRROR_HOR );

			break;

		case DIRECTION_UP_RIGHT:
			pSleepy->setAnimSequence( 2, false );
			
			if( !pSleepy->isMirrored( MIRROR_HOR ) )
				pSleepy->mirror( MIRROR_HOR );

			break;
	
		case DIRECTION_DOWN_RIGHT:
		case DIRECTION_RIGHT:
			pSleepy->setAnimSequence( 1, false );
			
			if( !pSleepy->isMirrored( MIRROR_HOR ) )
				pSleepy->mirror( MIRROR_HOR );

			break;
			
		// Nunca acontecerá esta possibilidade
//		case DIRECTION_UP:
//			break;
	}
	
	TextureFrame currFrameInfo;
	pSleepy->getStepFrameInfo( &currFrameInfo, pSleepy->getCurrAnimSequence(), pSleepy->getCurrAnimSequenceStep() );
	setAnchor( ( currFrameInfo.offsetX * getScale()->x ) + ( currFrameInfo.width * getScale()->x * 0.5f ),
			   ( currFrameInfo.offsetY * getScale()->y ) + (( currFrameInfo.height - SLEEPY_FEET_OFFSET_Y ) * getScale()->y ), 0.0f );
}

/*===========================================================================================
 
MÉTODO updateFrame
	Atualiza o frame do jogador de acordo com a direção para a qual está virado.

============================================================================================*/

void SleepyBarrierPlayer::updateFrame( void )
{
	float width = SLEEPY_WIDTH, height = SLEEPY_HEIGHT;
	Point3f top( VECTOR_UP_90 ), right, realOffset( 0.05f ), screenOffset;
	
	getOrtho( &right );

	Sprite* pSleepy = static_cast< Sprite* >( getObject( SLEEPY_OBJ_INDEX_PLAYER ) );
	if( pSleepy->getCurrAnimSequence() == 1 )
	{
		realOffset.x += 0.05f;
		height -= 0.05f;
	}
	else if( pSleepy->getCurrAnimSequence() == 2 )
	{
		realOffset.x += 0.1f;
		screenOffset.y += 5.0f;
		height -= 0.05f;
	}

	// OLD
//	TextureFrame currFrameInfo;
//	pSleepy->getStepFrameInfo( &currFrameInfo, pSleepy->getCurrAnimSequence(), pSleepy->getCurrAnimSequenceStep() );
//
//	Point3f aux = GameUtils::isoGlobalToPixels( realPosition.x + realOffset.x, realPosition.y + realOffset.y, realPosition.z + realOffset.z );
//	aux.x += pSleepy->getPosition()->x + currFrameInfo.offsetX + screenOffset.x;
//	aux.y += pSleepy->getPosition()->y + currFrameInfo.offsetY + screenOffset.y;
//	Point3f realPos = GameUtils::isoPixelsToGlobalY0( aux.x, aux.y );
//
//	collisionArea.set( realPos, top, right, ORIENT_TOP_LEFT, width, height );
	
	collisionArea.set( realPosition + realOffset, top, right, ORIENT_BOTTOM, width, height );
}

/*===============================================================================

MÉTODO updatePosition
	Atualiza a posição da bola na tela (não altera a posição real).
   
=============================================================================== */

void SleepyBarrierPlayer::updatePosition( void )
{
	// Atualiza a orientação e o retângulo de colisão go goleiro
	updateFrame();

	Point3f p = GameUtils::isoGlobalToPixels( realPosition );
	setPositionByAnchor( p.x, p.y, 0.0f );
}

/*===============================================================================

MÉTODO onAnimStepChanged
	Chamado quando o sprite muda de etapa de animação.

================================================================================*/

void SleepyBarrierPlayer::onAnimStepChanged( Sprite* pSprite )
{	
	Sprite* pSleepy = static_cast< Sprite* >( getObject( SLEEPY_OBJ_INDEX_PLAYER ) );
	Sprite* pShadow = static_cast< Sprite* >( getObject( SLEEPY_OBJ_INDEX_SHADOW ) );
	
	int16 currSeq = pSleepy->getCurrAnimSequence();
	if( ( currSeq == 1 ) && pSleepy->isMirrored( MIRROR_HOR ) )
		pShadow->setAnimSequence( 3, false, false );
	else
		pShadow->setAnimSequence( pSleepy->getCurrAnimSequence(), false, false );

	pShadow->setCurrentAnimSequenceStep( pSleepy->getCurrAnimSequenceStep() );
	
	// Garante que o posicionamento dos elementos do grupo estará correto
	updatePosition();
}

/*===============================================================================

MÉTODO render
	Renderiza o objeto.

================================================================================*/

#if DEBUG && ( IS_CURR_TEST( TEST_COLLISION_AREAS ) || IS_CURR_TEST( TEST_BALL_FX_FACTOR ) )

bool SleepyBarrierPlayer::render( void )
{
	if( ObjectGroup::render() )
	{
		// Renderiza o quad de colisão
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );

		glEnableClientState( GL_VERTEX_ARRAY );
		glLineWidth( 4.0f );
		glPointSize( 8.0f );
		
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix();
		
		Point3f pos = GameUtils::isoGlobalToPixels( collisionArea.position );
		Point3f dx = GameUtils::isoGlobalToPixels( collisionArea.right * collisionArea.width );
		Point3f dy = GameUtils::isoGlobalToPixels( collisionArea.top * collisionArea.height );

		dy = -dy;
		
		Point3f top[ 2 ] = {
								pos,
								pos + dy,
							};
		
		glColor4ub( 255, 0, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, top );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f right[ 2 ] = {
								pos,
								pos + dx,
							};
		
		glColor4ub( 0, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, right );
		glDrawArrays( GL_LINES, 0, 2 );
		
		Point3f others[ 4 ] = {
								pos + dy,
								pos + dy + dx,
								pos + dy + dx,
								pos + dx
							};

		glColor4ub( 255, 255, 0, 255 );
		glVertexPointer( 3, GL_FLOAT, 0, others );
		glDrawArrays( GL_LINES, 0, 4 );

		Point3f nomal = pos + ( dx * 0.5f ) + ( dy * 0.5f );
		if( collisionArea.normal.z > 0.0f )
			glColor4ub( 179, 179, 179, 255 );
		else
			glColor4ub( 255, 183, 15, 255 );

		glVertexPointer( 3, GL_FLOAT, 0, &nomal );
		glDrawArrays( GL_POINTS, 0, 1 );

//		// Renderiza a moldura do sprite
//		Sprite* pSleepy = static_cast< Sprite* >( getObject( SLEEPY_OBJ_INDEX_PLAYER ) );
//		TextureFrame currFrameInfo;
//		pSleepy->getStepFrameInfo( &currFrameInfo, pSleepy->getCurrAnimSequence(), pSleepy->getCurrAnimSequenceStep() );
//		
//		float spritePosX = getPosition()->x + pSleepy->getPosition()->x + currFrameInfo.offsetX + 0.375f;
//		float spritePosY = getPosition()->y + pSleepy->getPosition()->y + currFrameInfo.offsetY + 0.375f;
//		
//		float spriteWidth = currFrameInfo.width * getScale()->x;
//		float spriteHeight = currFrameInfo.height * getScale()->y;
//		
//		Point3f border[ 4 ] = {
//								Point3f( spritePosX, spritePosY ),
//								Point3f( spritePosX, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY + spriteHeight ),
//								Point3f( spritePosX + spriteWidth, spritePosY )
//							  };
//		
//		glColor4ub( 0, 255, 255, 255 );
//
//		glVertexPointer( 3, GL_FLOAT, 0, border );
//		glDrawArrays( GL_LINE_LOOP, 0, 4 );

		glPopMatrix();
		glLineWidth( 1.0f );
		glPointSize( 1.0f );
		
		glColor4ub( 255, 255, 255, 255 );
		
		return true;
	}
	return false;
}

#endif
