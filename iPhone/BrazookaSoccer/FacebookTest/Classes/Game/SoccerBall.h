/*
 *  SoccerBall.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 4/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SOCCER_BALL_H
#define SOCCER_BALL_H 1

#include "RealPosOwner.h"
#include "RenderableImage.h"

#define SOCCER_BALL_MAX_TRAIL_LEN 12

class SoccerBall : public RenderableImage
{
	public:
		// Construtor
		SoccerBall( const RealPosOwner* pParent );
	
		// Destrutor
		virtual ~SoccerBall( void );
	
		// Renderiza o objeto
		virtual bool render( void );

		// Acrescenta uma posição ao vetor de rastro
		void insertTrailPos( Point3f& pos );
	
		// Limpa o rastro
		void cleanTrail( void );
	
		// Retorna os vértices do objeto
		VertexSetHandler getBallVertexSet( void ) const;
	
		// Retorna a posição do objeto no mundo real
		const Point3f* getRealPosition( void ) const;

	private:
		// Cria o conjunto de vértices que será utilizado para renderizar o objeto
		static VertexSetHandler CreateVertexSet( void );
	
		// Objeto encarregado de posicionar a bola
		const RealPosOwner* pParent;
	
		// Auxiliares para realizarmos o efeito de rastro
		uint8 nTrailPos;
		uint8 insertIndex;
		Point3f trailPos[ SOCCER_BALL_MAX_TRAIL_LEN ];
};

/*===========================================================================================
 
IMPLEMENTAÇÃO DOS MÉTODOS INLINE

============================================================================================*/

inline VertexSetHandler SoccerBall::getBallVertexSet( void ) const
{
	return pVertexSet;
}

inline const Point3f* SoccerBall::getRealPosition( void ) const
{
	return pParent->getRealPosition();
}

#endif
