#include "BarrierPlayer.h"

#include "Config.h"

/*===========================================================================================
 
CONSTRUTOR
 
============================================================================================*/

BarrierPlayer::BarrierPlayer( uint16 maxObjects ) : GenericIsoPlayer( maxObjects ), reactedToBall( false ), ortho()
{
}

/*===========================================================================================
 
DESTRUTOR
 
============================================================================================*/

BarrierPlayer::~BarrierPlayer( void )
{
}

/*===========================================================================================
 
MÉTODO getCollisionSoundNameAndIndex
	Retorna o índice do som que deve ser tocado quando a bola colide com este jogador.

============================================================================================*/

void BarrierPlayer::getCollisionSoundNameAndIndex( uint8* pName, uint8* pIndex ) const
{
	*pName = SOUND_NAME_BARRIER_GEN;
	*pIndex = SOUND_INDEX_BARRIER_GEN;
}
