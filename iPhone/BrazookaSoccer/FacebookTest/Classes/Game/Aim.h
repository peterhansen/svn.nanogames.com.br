/*
 *  Aim.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef AIM__H
#define AIM__H 1

#include "ObjectGroup.h"
#include "Sprite.h"

typedef enum AimState
{
	AIM_STATE_UNDEFINED = -1,
	AIM_STATE_BLINKING = 0,
	AIM_STATE_SHOT
}AimState;

class Aim : public ObjectGroup, public SpriteListener
{
	public:
		// Construtor
		// Exceções: ConstructorException
		Aim( void );

		// Destrutor
		virtual ~Aim( void );

		// Retorna o raio da imagem da mira
		float getRadius( void ) const;
	
		// Retorna a posição do centro da mira
		Point3f getCenterPosition( void ) const;
	
		// Posiciona a mira de acordo com o seu centro
		void setCenterPosition( float x, float y );
	
		// Determina o estado do objeto
		void setState( AimState s );
	
		// Retorna o estado do objeto
		AimState getState( void ) const;
	
		// Chamado quando o sprite termina um sequência de animação
		virtual void onAnimEnded( Sprite* pSprite );
	
		// Determina o objeto que irá receber eventos do sprite
		void setListener( SpriteListener* pListener );

		// Atualiza o objeto
		virtual bool update( float timeElapsed );

	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );
	
		// Auxiliar de animação no estado AIM_STATE_BLINKING
		bool fadingIn;
	
		// Estado do objeto
		AimState state;
	
		// Raio da imagem da mira
		float radius;

		// Objeto que irá receber eventos do sprite
		SpriteListener* pListener;
};

// Implementação dos métodos inline

inline float Aim::getRadius( void ) const
{
	return radius;
}

inline AimState Aim::getState( void ) const
{
	return state;
}

inline void Aim::setListener( SpriteListener* pListener )
{
	this->pListener = pListener;
}

#endif
