#include "AnimatedCamera.h"

#include "GLHeaders.h"
#include "ObjcMacros.h"
#include "MathFuncs.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AnimatedCamera::AnimatedCamera( AnimatedCameraListener* pListener, CameraType cameraType, float zNear, float zFar )
			   : OrthoCamera( cameraType, zNear, zFar ), pListener( pListener ), animating( false ),
				 animTimeCounter( 0.0f ), animTime( 0.0f ), animDestPos(), animSpeed(),
				 animDestZoom( 0.0f ), zoomSpeed( 0.0f )
{
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

AnimatedCamera::~AnimatedCamera( void )
{
	pListener = NULL;
};

/*==============================================================================================

MÉTODO setAnim
	Determina uma animação de movimenta da câmera, de sua posição atual até a posição de
destino, que será executada intervalo de tempo 'time'.

==============================================================================================*/

void AnimatedCamera::setAnim( const Point3f* pDestPos, float finalZoom, float time )
{
	animTime = time;
	
	changeCurrAnimDest( pDestPos );
	
	animDestZoom = finalZoom;
	zoomSpeed = ( finalZoom - getZoomFactor() ) / animTime;
}

/*==============================================================================================

MÉTODO startAnim
	Inicia a animação configurada por setAnim.

==============================================================================================*/

void AnimatedCamera::startAnim( void )
{
	if( animating )
		endAnim();
		
	animTimeCounter = 0.0f;		
	animating = true;
}

/*==============================================================================================

MÉTODO endAnim
	Termina a animação de movimentação da câmera. Se a animação ainda não estiver completa,
a câmera é levada diretamente para a sua posição final.

==============================================================================================*/

void AnimatedCamera::endAnim( void )
{
	if( animating )
	{
		animating = false;

		setPosition( &animDestPos );
		zoom( animDestZoom );
		
		if( pListener )
			pListener->onCameraAnimEnded();
	}
}

/*==============================================================================================

MÉTODO cancelAnim
	Cancela a animação, deixando a câmera em seu estado atual.

==============================================================================================*/

void AnimatedCamera::cancelAnim( void )
{
	if( animating )
	{
		animating = false;

		if( pListener )
			pListener->onCameraAnimEnded();
	}
}

/*==============================================================================================

MÉTODO changeCurrAnimDest
	Altera o destino da animação atual sem iniciar uma nova animação.

==============================================================================================*/

void AnimatedCamera::changeCurrAnimDest( const Point3f* pDestPos )
{
	animDestPos.set( pDestPos->x, pDestPos->y, 0.0f );
	animSpeed = ( animDestPos - pos ) / animTime;
	animSpeed.z = 0.0f;
}

/*==============================================================================================

MÉTODO update
	Atualiza o objeto.

==============================================================================================*/	

bool AnimatedCamera::update( float timeElapsed )
{
	if( !Updatable::update( timeElapsed ) || !animating )
		return false;
	
	animTimeCounter += timeElapsed;
	if( NanoMath::fgeq( animTimeCounter, animTime ) )
	{
		endAnim();
	}
	else
	{
		move( animSpeed.x * timeElapsed, animSpeed.y * timeElapsed, 0.0f );
		zoom( getZoomFactor() + ( zoomSpeed * timeElapsed ) );
	}
	
	return true;
}

/*==============================================================================================

MÉTODO getProjectionMatrix
	Obtém a matriz de projeção da câmera.

==============================================================================================*/

Matrix4x4* AnimatedCamera::getProjectionMatrix( Matrix4x4* pOut ) const
{
	OrthoCamera::getProjectionMatrix( pOut );
	
	// Converte novamente para column-major
	pOut->transpose();

	float invZoom = 1.0f / zoomFactor;
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	glMultMatrixf( *pOut );
	glTranslatef( HALF_SCREEN_WIDTH * invZoom, HALF_SCREEN_HEIGHT * invZoom, 0.0f );
	glGetFloatv( GL_PROJECTION_MATRIX, *pOut );
	glPopMatrix();
	
	// Converte para row-major
	pOut->transpose();
	
	return pOut;
}
