#include "BrazookaRankingFeed.h"

// Components
#include "MemoryStream.h"

// NanoOnline
#include "NOFacebookConstants.h"

// Game
#include "BrazookaSoccerFacebookFeedTypes.h"

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

BrazookaRankingFeed::BrazookaRankingFeed( void )
					: NOFacebookFeed( BRAZ_SOCCER_FEED_TYPE_RNK, NULL ),
					  rnkScore( 0LL )
{
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

BrazookaRankingFeed::BrazookaRankingFeed( const int64& score, const NOCustomer *pFeedTarget )
					: NOFacebookFeed( BRAZ_SOCCER_FEED_TYPE_RNK, pFeedTarget ),
					  rnkScore( score )
{
}

/*==============================================================================================

DESTRUTOR

===============================================================================================*/

BrazookaRankingFeed::~BrazookaRankingFeed( void )
{
}

/*==============================================================================================

MÉTODO writeFeedParams
	Escreve os parâmetros específicos do feed.

===============================================================================================*/

void BrazookaRankingFeed::writeFeedParams( MemoryStream& stream )
{
	stream.writeInt8( FB_FEED_PARAM_TYPE_INT64 );
	stream.writeUTF32String( L"score" );
	stream.writeInt64( rnkScore );
	
	stream.writeInt8( FB_FEED_PARAM_TYPE_END );
}
