/*
 *  Goal.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 6/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GOAL_H
#define GOAL_H 1

#include "RenderableImage.h"

#include "RealPosOwner.h"

class Goal : public RealPosOwner, public RenderableImage
{
	public:
		// Construtor
		Goal( Goal* pGoalBottom = NULL );

		// Destrutor
		virtual ~Goal( void );

	private:
		// Atualiza a posição do objeto na tela
		virtual void updatePosition( void );
	
		// Ponteiro para a parte inferior do gol ou NULL caso esta seja a própria parte inferior
		Goal* pGoalBottom;
	
		// Offset de posicionamento
		float offsetX, offsetY;
};

#endif
