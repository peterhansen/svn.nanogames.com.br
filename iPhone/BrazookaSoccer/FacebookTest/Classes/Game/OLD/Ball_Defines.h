#ifndef BALL_DEFINES_H
#define BALL_DEFINES_H

// aceleração da gravidade (equivalente a 9,8m/s≤)
#define GRAVITY_ACCELERATION	( -9.8f )
#define GRAVITY_ACCELERATION_KEEPER	( -19.6f )

// Constante para ajustar as dimensões do campo. Apesar de fugir do tamanho real de um campo, torna melhor a visualização do jogo
///<DM/> vários ajustes para reduzir o campo
#define ADJUSTMENTS_CONST 0.6f ///DM: Old: 0.8f

// comprimentos do "mundo real"
#define REAL_GOAL_WIDTH			7.32f							// oficial: 7,32m
#define REAL_FLOOR_TO_BAR		2.44f							// oficial: 2,44m
#define REAL_POST_WIDTH			0.16f // OLD 0.12f				// oficial: 12cm
#define REAL_BALL_RADIUS		0.20f // OLD 0.15f				// oficial: 35cm
#define REAL_BALL_DIAMETER		( REAL_BALL_RADIUS * 2.0f )
#define REAL_GOAL_DEPTH			-1.90f // OLD -2,1m (profundidade máxima que a bola vai no gol)

// limites de colisão com as traves
#define REAL_BAR_FIX_CONST 0.12f

// travessão
#define REAL_BAR_BOTTOM			( REAL_FLOOR_TO_BAR - REAL_BALL_RADIUS + REAL_BAR_FIX_CONST )
#define REAL_BAR_TOP			( REAL_BAR_BOTTOM + REAL_POST_WIDTH + ( REAL_BALL_RADIUS * 2.0f ) )
#define REAL_BAR_MIDDLE			( ( REAL_BAR_TOP + REAL_BAR_BOTTOM ) * 0.5f )

// trave esquerda
#define REAL_LEFT_POST_START	( -( REAL_GOAL_WIDTH * 0.5f ) + REAL_BALL_RADIUS + REAL_BAR_FIX_CONST )
#define REAL_LEFT_POST_END		( REAL_LEFT_POST_START - REAL_POST_WIDTH - ( REAL_BALL_RADIUS * 2.0f ) )
#define REAL_LEFT_POST_MIDDLE	( ( REAL_LEFT_POST_START + REAL_LEFT_POST_END ) * 0.5f )

// trave direita
#define REAL_RIGHT_POST_START	( ( REAL_GOAL_WIDTH * 0.5f ) - REAL_BALL_RADIUS - REAL_BAR_FIX_CONST )
#define REAL_RIGHT_POST_END		( REAL_RIGHT_POST_START + REAL_POST_WIDTH + ( REAL_BALL_RADIUS * 2.0f ) )
#define REAL_RIGHT_POST_MIDDLE	( ( REAL_RIGHT_POST_START + REAL_RIGHT_POST_END ) * 0.5f )

// dimensões da pequena área
#define REAL_SMALL_AREA_DEPTH ( 5.5f * ADJUSTMENTS_CONST )  // oficial: 5,5m
#define REAL_SMALL_AREA_WIDTH ( 18.0f * ADJUSTMENTS_CONST ) // oficial: 18m

// dimensões da grande área
#define REAL_BIG_AREA_DEPTH ( 15.5f * ADJUSTMENTS_CONST ) // oficial: 16,5m , DM - OLD:13.0f
#define REAL_BIG_AREA_WIDTH ( 32.0f * ADJUSTMENTS_CONST ) // oficial: 40m

#define REAL_PENALTY_TO_GOAL	( ( REAL_SMALL_AREA_DEPTH + REAL_BIG_AREA_DEPTH ) * 0.5f )	// oficial: 11m

// dimensões totais do campo
#define REAL_FIELD_WIDTH ( 64.0f * ADJUSTMENTS_CONST ) // oficial: mínimo de 64m e máximo de 75m
#define REAL_FIELD_DEPTH ( 90.0f * ADJUSTMENTS_CONST )	// oficial: mínimo de 100m e máximo de 110

// Raio do grande círculo
#define REAL_FIELD_CENTER_CIRCLE_RADIUS ( 6.25f * ADJUSTMENTS_CONST ) // oficial: 9.15m

// Raio da meia lua. O centro do círculo é a marca do pênalti
#define REAL_FIELD_HALF_ARC_RADIUS REAL_FIELD_CENTER_CIRCLE_RADIUS

// Raio do quarto de círculo do corner
#define REAL_FIELD_CORNER_QUARTER_RADIUS ( 1.0f * ADJUSTMENTS_CONST ) // oficial: 1.0m

// largura do goleiro com os braços próximos ao corpo
#define REAL_KEEPER_WIDTH 1.0f

// largura do goleiro com os braços abertos
#define REAL_KEEPER_WIDTH_ARMS ( REAL_KEEPER_WIDTH + 0.55f )

// Largura entre REAL_KEEPER_WIDTH e REAL_KEEPER_WIDTH_ARMS
#define REAL_KEEPER_INTERMEDIATE_WIDTH (( REAL_KEEPER_WIDTH + REAL_KEEPER_WIDTH_ARMS ) * 0.5f )

// Largura utilizada quando o goleiro está todo esticado
#define REAL_KEEPER_LOW_WIDTH ( REAL_KEEPER_WIDTH - 0.1f )

// valor mínimo do pulo do goleiro para que ele pule para cima (no caso da bola estar próxima
// a ele horizontalmente)
#define REAL_KEEPER_HEIGHT 1.8f

// altura do goleiro com os braços esticados
#define REAL_KEEPER_HEIGHT_ARMS ( REAL_KEEPER_HEIGHT + 0.7f )

// Altura entre REAL_KEEPER_HEIGHT e REAL_KEEPER_HEIGHT_ARMS
#define REAL_KEEPER_INTERMEDIATE_HEIGHT (( REAL_KEEPER_HEIGHT + REAL_KEEPER_HEIGHT_ARMS ) * 0.5f )

// Goleiro com os braços pouco levantados
#define REAL_KEEPER_INTERMEDIATE_HEIGHT_HIGH ( REAL_KEEPER_INTERMEDIATE_HEIGHT + 0.2f )

// Altura utilizada quando o goleiro está com as pernas flexionadas
#define REAL_KEEPER_LOW_HEIGHT ( REAL_KEEPER_HEIGHT - 0.25f )

#define REAL_KEEPER_HEIGHT_STRETCHED ( REAL_KEEPER_HEIGHT_ARMS + 0.8f )

// Dimensões do goleiro deitado
#define REAL_KEEPER_LAID_WIDTH ( REAL_KEEPER_WIDTH - 0.2f )
#define REAL_KEEPER_LAID_HEIGHT ( REAL_KEEPER_HEIGHT_ARMS + 0.1f )

// Altura do jogador no mundo real
#define REAL_PLAYER_HEIGHT 1.65f

// Posição real da marca do pênalti
#define REAL_PENALTY_POSITION 0.0f, 0.0f, REAL_PENALTY_TO_GOAL

#endif