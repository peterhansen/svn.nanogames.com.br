#include "GenericIsoPlayer.h"

#include "Point2i.h"
#include "MathFuncs.h"

#include "GameUtils.h"

#if DEBUG
	#include "Macros.h"
#endif

using namespace GameUtils;

// Limites das tangentes dos ângulos para realizar a rotação dos jogadores. Os valores
// referem-se às tangentes de 30 e 60 graus
#define ANGLE_LOW	25.0f // OLD 0.58f
#define ANGLE_HIGH	60.0f // OLD 1.73f

/*===============================================================================

CONSTRUTOR

=============================================================================== */

#warning Seria melhor que esta classe fosse apenas uma interface e deixar a decisão de derivar de Object, ObjectGroup, Sprite e etc para a classe final

GenericIsoPlayer::GenericIsoPlayer( uint16 maxObjects ) : GenericPlayer( maxObjects )
{
}

/*===============================================================================

DESTRUTOR

=============================================================================== */

GenericIsoPlayer::~GenericIsoPlayer( void )
{
	#if DEBUG
		LOG( "Destruindo GenericIsoPlayer\n" );
	#endif
}

/*====================================================================================

MÉTODO lookAt
	Faz o jogador virar de frente para a posição "pPosition". O cálculo leva em
conta que os jogadores podem estar virados em 8 direções. Toma-se o vetor que vai
da posição do jogador à posição que ele vai olhar (distance), e em seguida testa-se
qual direção é a mais próxima dele (angle). Depois, é realizada a troca do frame em si.

=======================================================================================*/

void GenericIsoPlayer::lookAt( const Point3f* pPosition, bool forceFrameUpdate )
{
	// OLD
//	// A posição default (0.0f, 0.0f, 0.0f) é o centro do gol
//	Point3f lookAtPosition;
//	if( pPosition != NULL )
//		lookAtPosition = *pPosition;
//
//	Point3f distance = ( realPosition - lookAtPosition ).normalize();
//	float angle = fabs( normalDistance.z / ( NanoMath::fcmp( normalDistance.x, 0.0f ) ? 0.1f : normalDistance.x ) );
//	
//	IsoDirection oldDirection = direction;
//
//	if( NanoMath::fgeq( distance.z, 0.0f ) )
//	{
//		if( NanoMath::fleq( distance.x, 0.0f ) )
//		{
//			if( angle >= ANGLE_HIGH )
//				direction = DIRECTION_UP;
//			else if( angle <= ANGLE_LOW )
//				direction = DIRECTION_RIGHT;
//			else
//				direction = DIRECTION_UP_RIGHT;
//		}
//		else
//		{
//			if( angle >= ANGLE_HIGH )
//				direction = DIRECTION_UP_LEFT;
//			else if( angle <= ANGLE_LOW )
//				direction = DIRECTION_LEFT;
//			else
//				direction = DIRECTION_UP_LEFT;
//		}
//	}
//	else
//	{
//		if( NanoMath::fleq( distance.x, 0.0f ) )
//		{
//			if( angle >= ANGLE_HIGH )
//				direction = DIRECTION_DOWN;
//			else if( angle <= ANGLE_LOW )
//				direction = DIRECTION_RIGHT;
//			else
//				direction = DIRECTION_DOWN_RIGHT;
//		}
//		else
//		{			
//			if( angle >= ANGLE_HIGH )
//				direction = DIRECTION_DOWN;
//			else if( angle <= ANGLE_LOW )
//				direction = DIRECTION_LEFT;
//			else
//				direction = DIRECTION_DOWN_LEFT;
//		}
//	}
	
	// A posição default (0.0f, 0.0f, 0.0f) é o centro do gol
	Point3f lookAtPosition;
	if( pPosition != NULL )
		lookAtPosition = *pPosition;

	Point3f distance = ( realPosition - lookAtPosition ).normalize();
	
	Point3f defaultVec( 0.0f, 0.0f, -1.0f );
	float angle = defaultVec.getAngle( &distance );
	
	IsoDirection oldDirection = direction;

	if( NanoMath::fgeq( distance.z, 0.0f ) )
	{
		if( NanoMath::fleq( lookAtPosition.x, 0.0f ) )
		{
			if( angle >= ANGLE_HIGH )
				direction = DIRECTION_UP;
			else if( angle <= ANGLE_LOW )
				direction = DIRECTION_RIGHT;
			else
				direction = DIRECTION_UP_RIGHT;
		}
		else
		{
			if( angle >= ANGLE_HIGH )
				direction = DIRECTION_DOWN_RIGHT;
			else if( angle <= ANGLE_LOW )
				direction = DIRECTION_LEFT;
			else
				direction = DIRECTION_UP_LEFT;
		}
	}
	else
	{
		if( NanoMath::fleq( lookAtPosition.x, 0.0f ) )
		{
			if( angle >= ANGLE_HIGH )
				direction = DIRECTION_UP_LEFT;
			else if( angle <= ANGLE_LOW )
				direction = DIRECTION_DOWN_LEFT;
			else
				direction = DIRECTION_LEFT;
		}
		else
		{			
			if( angle >= ANGLE_HIGH )
				direction = DIRECTION_RIGHT;
			else if( angle <= ANGLE_LOW )
				direction = DIRECTION_DOWN_LEFT;
			else
				direction = DIRECTION_DOWN;
		}
	}

	if( forceFrameUpdate || ( oldDirection != direction ))
		updateFrame();
}
