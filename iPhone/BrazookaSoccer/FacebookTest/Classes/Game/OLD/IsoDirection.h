/*
 *  IsoDirection.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ISO_DIRECTION_H
#define ISO_DIRECTION_H 1

typedef enum IsoDirection
{
	DIRECTION_DOWN_RIGHT = 0,
	DIRECTION_DOWN,
	DIRECTION_DOWN_LEFT,
	DIRECTION_LEFT,
	DIRECTION_UP_LEFT,
	DIRECTION_UP,
	DIRECTION_UP_RIGHT,
	DIRECTION_RIGHT

} IsoDirection;

#endif
