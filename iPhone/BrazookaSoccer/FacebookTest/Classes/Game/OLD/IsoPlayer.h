#ifndef ISO_PLAYER_H
#define ISO_PLAYER_H

#include "GenericIsoPlayer.h"
#include "Sprite.h"

#include "Tests.h"

typedef enum PlayerState
{
	PLAYER_UNDEFINED = -1,
	PLAYER_STOPPED = 0,
	PLAYER_RUNNING = 1,
	PLAYER_SHOOTING = 2,
	PLAYER_SHOT = 3

} PlayerState;

// Define o número de sprites que compoem as animações do goleiro
#define PLAYER_N_SPRITES 3

// Define o número de sprites de sombra que compoem as animações do goleiro
#define PLAYER_SHADOW_N_SPRITES PLAYER_N_SPRITES

class IsoPlayer : public GenericIsoPlayer, public SpriteListener
{
	public:
		// Construtor
		// Exceções: pode disparar OutOfMemoryException e NanoBaseException
		IsoPlayer( void );
	
		// Destrutor
		virtual ~IsoPlayer( void );

		// Reinicializa o objeto
		virtual void reset( const Point3f* pPosition = NULL );
	
		// Define o estado do jogador
		void setState( PlayerState s );
	
		// Determina um objeto que irá receber eventos do jogador
		void setListener( SpriteListener* pListener );

		// Retorna o estado do jogador
		PlayerState getState( void ) const;
	
		// Chamado quando o sprite muda de etapa de animação
		virtual void onAnimStepChanged( Sprite* pSprite );
	
		// Chamado quando o sprite termina um sequência de animação
		virtual void onAnimEnded( Sprite* pSprite );
	
		// Chamado quando o sprite reinicia automaticamente uma sequência de animação
		virtual void onAnimLooped( Sprite* pSprite );
	
		// Retorna se o jogador está no frame do chute em sua animação de chutar a bola
		bool isOnKickAnimStep( void );
	
		// Retorna se o jogador está no frame anterior ao frame do chute em sua animação de
		// chutar a bola
		bool isOnBeforeKickAnimStep( void );
	
		#if DEBUG
			// Renderiza o objeto
			virtual bool render( void );

			#if IS_CURR_TEST( TEST_PLAYER_KICK )
				// Atualiza o objeto
				virtual bool update( float timeElapsed );
			#endif
		#endif
	
		// Determina as operações de espelhamento que devem ser aplicadas sobre o sprite
		void mirror( MirrorOp ops );
	
		// Indica se uma determinada operação de espelhamento está aplicada sobre o sprite
		bool isMirrored( MirrorOp ops );
	
		// Cancela operações de espelhamento
		void unmirror( MirrorOp ops );
	
		// Indica que o jogador deve se posicionar de acordo com a posição da bola
		virtual void prepare( const Point3f& ballRealPos );

	protected:
		// Atualiza a posição do jogador na tela
		virtual void updatePosition( void );

		// Atualiza o frame do jogador de acordo com a direção para a qual está virado
		virtual void updateFrame( void ){};

		// Armazena o frame atual da animação corrente INDEPENDENTEMENTE DA DIREÇÃO DO JOGADOR
		uint8 animationFrame;

		// Armazena o primeiro frame DA IMAGEM COMPLETA que contém a animação atual, considerando a direção do jogador
		uint8 firstAnimationFrame;

		// Tempo acumulado desde o início da última animação
		float accAnimationTime;
	
	private:
		// Inicializa o objeto
		bool build( void );
	
		// Limpa as variáveis do objeto
		void clear( void );
	
		// Estado do jogador
		PlayerState state, nextState;
	
		// Operações de espelhamento aplicadas ao jogador
		uint8 mirrorOps;
	
		// Índice da sombra e do sprite que estamos utilizando
		int8 currSpriteIndex;
	
		// Determina um objeto que irá receber eventos do jogador
		SpriteListener* pListener;
	
		// Imagens que compõem o jogador
		Sprite* player[ PLAYER_N_SPRITES ];
		Sprite* shadow[ PLAYER_SHADOW_N_SPRITES ];
};

// Implementação das funções inline
inline PlayerState IsoPlayer::getState( void ) const
{
	return state;
}

inline void IsoPlayer::setListener( SpriteListener* pListener )
{
	this->pListener = pListener;
}

#endif
