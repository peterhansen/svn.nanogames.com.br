/*
 *  Ball.h
 *  FreeKick
 *
 *  Created by Daniel Lopes Alves on 2/3/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BALL_H
#define BALL_H 1

#include "Bezier.h"
#include "Point3f.h"
#include "ObjectGroup.h"
#include "Iso_Defines.h"
#include "GameInfo.h"
#include "GenericPlayer.h"
#include "Play.h"
#include "RealPosOwner.h"

#include "Tests.h"

// tipos de colisão com jogador
#define COLLISION_KEEPER	0
#define COLLISION_BARRIER	1

// possíveis colisões da bola
#define POST_LEFT	0
#define POST_RIGHT	1
#define POST_BAR	2

enum BallState
{
	BALL_STATE_UNDEFINED		=  -1,
	BALL_STATE_STOPPED			=   0,
	BALL_STATE_MOVING			=   1,
	BALL_STATE_GOAL				=   2,
	BALL_STATE_OUT				=   4,
	BALL_STATE_POST_BACK		=   8,	// Bateu na trave e voltou
	BALL_STATE_POST_GOAL		=  10,	// Bateu na trave e entrou
	BALL_STATE_POST_OUT			=  12,	// Bateu na trave e foi para fora
	BALL_STATE_REJECTED_KEEPER	=  16,
	BALL_STATE_REJECTED_BARRIER =  32
};

class Ball : public RealPosOwner, public ObjectGroup
{
	public:
		// Construtor
		Ball( void );
	
		// Destrutor
		virtual ~Ball( void );
	
		// Retorna o tempo total (em segundos) que a bola levará para chegar ao gol
		inline float getTimeToGoal( void ) { return timeToGoal; };
	
		// Define os parâmetros da bola após o chute
		virtual Point3f* kick( Point3f* pOut, Play* pPlay, bool replay );

		// Checa colisão da bola com o jogador recebido como parâmetro
		uint8 checkPlayerCollision( GenericPlayer *p, uint8 type );
	
		// Determina que a bola colidiu com o quad passado como parâmetro
		void setCollisionWithQuad( const CollisionQuad* pQuad );
	
		// Reinicializa os atributos do objeto
		virtual void reset( const Point3f* pRealPosition = NULL );

		// Getters e Setters dos atributos da bola
		virtual void setState( BallState s ) { ballState = s; };
		inline BallState getState( void ) { return ballState; };

		inline BallState getLastPlayResult( void ) const { return lastPlayResult; };
		inline void setLastPlayResult( BallState playResult ) { lastPlayResult = playResult; };

		inline const Point3f* getLastPositionAtGoal( void ) const { return &realLastGoalPosition; };
		inline void setLastPositionAtGoal( Point3f positionAtGoal ) { realLastGoalPosition = positionAtGoal; };

		inline float getLastCollisionTime( void ) const { return lastCollisionTime; };
		inline void setLastCollisionTime( float collisionTime ) { lastCollisionTime = collisionTime; };

		inline float getAccTime( void ) const { return accTime; };


	
		inline Point3f getLastCollisionDirection( void ) const { return lastCollisionDirection; };
		inline void setLastCollisionDirection( Point3f collisionDirection ) { lastCollisionDirection = collisionDirection; };
	
		inline Point3f* getRealSpeed( Point3f *pOut ) const { if( pOut )*pOut = realSpeed; return pOut; };

	protected:
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
	#if DEBUG && IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
	
	public:
	
	#endif

		// Evita que a bola "fure" a rede após um gol
		virtual void keepBallInsideGoal( void ) = 0;
	
	#if DEBUG && IS_CURR_TEST( TEST_BALL_INSIDE_GOAL )
	
	protected:

	#endif

		// Trata a movimentação da bola após ela ir para fora. Testa colisão da bola
		// com a rede, de forma que a bola não "fure" a rede pelo lado de fora
		virtual void handleBallOut( void ) = 0;

		// métodos auxiliares para melhorar a clareza de código em alguns trechos
		BallState checkGoalCollision( void );
	
		// Trata a colisão com as traves
		BallState handlePostCollision( uint8 postIndex );

		// Desacelera a bola
		void reduceBallSpeed( float timeElapsed );

		// Indica as posições x, y, e z da bola no "mundo real". Esses são os valores efetivamente
		// usados para realizar os cálculos da bola.
		Point3f realInitialPosition, // usado para realizar os cálculos após colisão com goleiro, trave ou barreira
			realSpeed,
			realLastGoalPosition; // usado para realizar os cálculos de pontos no modo desafio de faltas

		float realInitialYSpeed; // usado para realizar cálculos de colisão da bola

		float accTime,			// tempo acumulado em segundos desde a última colisão (incluindo chute ou colisão com jogadores)
			  accTimeY;			// tempo acumulado em segundos desde a última colisão com o chão (ou chute)

		BallState ballState;

		float timeToGoal;			// tempo total que a bola levará para chegar ao gol (em segundos)

		bool fxActive;				// Indica se a movimentação da bola deve ser feita pela função de efeito
		bool checkedGoalCollision;	// indica se já foi efetuado o cálculo de colisão com o gol na movimentação atual da bola

		// armazena o instante de tempo da última jogada em que ocorreu a definição do resultado (gol, trave,
		// fora, etc), para que no replay o teste de colisão seja feito exatamente nesse instante
		float lastCollisionTime;
		///<DM> Variaveis auxiliares que garantem a integridade do replay
		Point3f iBackupDirection;
		float iBackupFX;
		Point3f iAuxCollisionDirection;
private:
		///<DM>
		BallState lastPlayResult;

		Point3f lastCollisionDirection;

		// caso a jogada seja replay, força o último resultado calculado
		bool isReplay;
	
		// Função que controla a movimentação da bola
		#if DEBUG && IS_CURR_TEST( TEST_BALL_FX_FACTOR )
		public:
		#endif
		BezierCurve movFunc;
	
	private:
		// Obtém a posição da bola no momento indicado
		Point3f* getRealPosAtTime( Point3f* pOut, float time );

		// Obtém a posição da bola no momento em que ela colidiu com o jogador
		Point3f* rewindToPlayerCollisionQuad( Point3f* pCollisionPos, const GenericPlayer *pPlayer, float minTime, float maxTime );
};

#endif
