/*
 *  AppleAudio.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef APPLE_AUDIO_H
#define APPLE_AUDIO_H

// Components
#include "IAudio.h"

// Apple Foundation
#include <AudioToolbox/AudioQueue.h>
#include <AudioToolbox/AudioFile.h>
#include <AudioToolbox/AudioFileStream.h>

class AppleAudio : public IAudio
{
	public:
		// Destrutor
		virtual ~AppleAudioBuffer( void );

	private:
		// Construtor
		AppleAudioBuffer( void );
	
		// Carrega um recurso que ainda não tenha sido carregado
		virtual ShrdResPtr loadResource( const std::string& fileName, const std::string& fileExt );
	
		// Descritores do arquivo de áudio
		AudioFileID audioFile;

		AudioStreamBasicDescription dataFormat;
		AudioStreamPacketDescription *pPacketDescs;
	
		AudioQueueRef pQueue;
		AudioQueueBufferRef pBuffer;

		UInt64 totalPackets;
		UInt32 numPacketsToRead;
	
		SInt32 mPrimingFrames;
		SInt32 mRemainderFrames;
};

#endif
