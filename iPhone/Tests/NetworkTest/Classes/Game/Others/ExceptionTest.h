/*
 *  ExceptionTest.h
 *  NetworkTest
 *
 *  Created by Daniel Lopes Alves on 9/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef EXCEPTION_TEST_H
#define EXCEPTION_TEST_H 1

#include "NanoTypes.h"

class ExceptionTest
{
	public:
		// Construtor
		ExceptionTest( void );

	private:
		int32 x;
};

#endif
