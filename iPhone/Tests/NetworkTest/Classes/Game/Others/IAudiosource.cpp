#include "IAudioSource.h"

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

IAudioSource::IAudioSource( void )
			: volume( 1.0f ),
			  loopCount( 0 ),
			  audioSourceState( AUDIO_SOURCE_STATE_UNDEFINED ),
			  stateBeforeSuspend( AUDIO_SOURCE_STATE_UNDEFINED )
{
}

/*==============================================================================================

MÉTODO play
	Inicia a execução do áudio, colocando-o no estado AUDIO_SOURCE_STATE_PLAYING.

===============================================================================================*/

void IAudioSource::play( int8 loopCount, bool restartIfAlreadyPlaying )
{
	audioSourceState = AUDIO_SOURCE_STATE_PLAYING;
}

/*==============================================================================================

MÉTODO stop
	Pára a execução do áudio, colocando-o no estado AUDIO_SOURCE_STATE_STOPPED.

===============================================================================================*/

void IAudioSource::stop( void )
{
	audioSourceState = AUDIO_SOURCE_STATE_STOPPED;
}

/*==============================================================================================

MÉTODO pause
	Pausa a execução do áudio, colocando-o no estado AUDIO_SOURCE_STATE_PAUSED.

===============================================================================================*/

void IAudioSource::pause( bool pause )
{
	audioSourceState = AUDIO_SOURCE_STATE_PAUSED;
}

/*==============================================================================================

MÉTODO suspend
	Trata a suspensão da aplicação, colocando o áudio no estado AUDIO_SOURCE_STATE_SUSPENDED.

===============================================================================================*/

void IAudioSource::suspend( void )
{
	if( audioSourceState != AUDIO_SOURCE_STATE_SUSPENDED )
	{
		stateBeforeSuspend = audioSourceState;

		pause( true );
		
		audioSourceState = AUDIO_SOURCE_STATE_SUSPENDED;
	}
}

/*==============================================================================================

MÉTODO resume
	Trata o retorno do estado AUDIO_SOURCE_STATE_SUSPENDED, colocando o áudio do estado
'stateBeforeSuspend'.

===============================================================================================*/

void IAudioSource::resume( void )
{
	if( audioSourceState == AUDIO_SOURCE_STATE_SUSPENDED )
	{
		audioSourceState = stateBeforeSuspend;
		
		switch( audioSourceState )
		{
			case AUDIO_SOURCE_STATE_PLAYING:
				pause( false );
				break;
				
			case AUDIO_SOURCE_STATE_STOPPED:
			case AUDIO_SOURCE_STATE_PAUSED:
			default:
				break;
		}
	}
}
