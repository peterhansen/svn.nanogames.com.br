#include "AppleAudio.h"

// Components
#include "Macros.h"

// Tamanho dos buffers utilizados no streaming do áudio
#define BUFFER_SIZE_IN_BYTES 0x10000

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

AppleAudioBuffer::AppleAudioBuffer( void )
				: audioFile( 0 ),
				  dataFormat(),
				  pPacketDescs( NULL ),
				  pQueue( NULL ),
				  pBuffer( NULL ),
				  totalPackets( 0 ),
				  numPacketsToRead( 0 ),
				  mPrimingFrames( 0 ),
				  mRemainderFrames( 0 )
{
}

/*==============================================================================================

DESTRUTOR

===============================================================================================*/

AppleAudio::~AppleAudioBuffer( void )
{
	//OBS: Não é necessário chamar AudioQueueFreeBuffer pois AudioQueueDispose já o fará
	//AudioQueueFreeBuffer( pQueue, pBuffer );

	AudioQueueDispose( pQueue, YES );
	
	if( audioFile )
	{
		AudioFileClose( audioFile );
		audioFile = 0;
	}

	DELETE_VEC( pPacketDescs );
}

/*==============================================================================================

MÉTODO loadResource
	Carrega um recurso que ainda não tenha sido carregado.

===============================================================================================*/

ShrdResPtr AppleAudio::loadResource( const std::string& fileName, const std::string& fileExt )
{
	// Abre o arquivo
	AudioFileOpenURL( Utils::GetURLForResource( fileName, fileExt ), 0x01/*fsRdPerm*/, 0/*inFileTypeHint*/, &audioFile );
		
	// Obtém o formato do arquivo
	UInt32 size = sizeof( dataFormat );
	AudioFileGetProperty( audioFile, kAudioFilePropertyDataFormat, &size, &dataFormat );
		
		// Agenda a reprodução do som
		AudioQueueNewOutput( &dataFormat, AudioQueueBufferCallback, this, CFRunLoopGetCurrent(), kCFRunLoopCommonModes, 0, &pQueue );

		// We have a couple of things to take care of now
		// (1) Setting up the conditions around VBR or a CBR format - affects how we will read from the file
		// If format is VBR we need to use a packet table
		UInt32 bufferSize = BUFFER_SIZE_IN_BYTES;
		if( ( dataFormat.mBytesPerPacket == 0 ) || ( dataFormat.mFramesPerPacket == 0 ) )
		{
			// first check to see what the max size of a packet is - if it is bigger
			// than our allocation default size, that needs to become larger
			UInt32 maxPacketSize;
			size = sizeof( maxPacketSize );
			AudioFileGetProperty( audioFile, kAudioFilePropertyPacketSizeUpperBound, &size, &maxPacketSize );
			if( maxPacketSize > bufferSize ) 
				bufferSize = maxPacketSize;

			numPacketsToRead = bufferSize / maxPacketSize;
			
			// We also need packet descpriptions for the file reading
			pPacketDescs = new AudioStreamPacketDescription[ numPacketsToRead ];
			if( !pPacketDescs )
				goto Error;
		}
		else
		{
			numPacketsToRead = bufferSize / dataFormat.mBytesPerPacket;
			pPacketDescs = NULL;
		}

		size = sizeof( totalPackets );
		AudioFileGetProperty( audioFile, kAudioFileStreamProperty_AudioDataPacketCount, &size, &( totalPackets ) );

		// (2) If the file has a cookie, we should get it and set it on the AudioQueue
		size = sizeof( UInt32 );
		OSStatus result = AudioFileGetPropertyInfo( audioFile, kAudioFilePropertyMagicCookieData, &size, NULL );
		if( !result && size )
		{
			char* pCookie = new char[ size ];
			if( !pCookie )
				goto Error;

			AudioFileGetProperty( audioFile, kAudioFilePropertyMagicCookieData, &size, pCookie );
			AudioQueueSetProperty( pQueue, kAudioQueueProperty_MagicCookie, pCookie, size );

			DELETE_VEC( pCookie );
		}
		
		// Channel layout?
		OSStatus err = AudioFileGetPropertyInfo( audioFile, kAudioFilePropertyChannelLayout, &size, NULL );
		if( err == noErr && size > 0 )
		{
			AudioChannelLayout* acl = ( AudioChannelLayout* )malloc( size );
			if( acl == NULL )
				goto Error;

			AudioFileGetProperty( audioFile, kAudioFilePropertyChannelLayout, &size, acl );
			AudioQueueSetProperty( pQueue, kAudioQueueProperty_ChannelLayout, acl, size );

			free( acl );
			acl = NULL;
		}
		
		AudioFilePacketTableInfo packetTableInfo;
		size = sizeof( packetTableInfo );
		AudioFileGetProperty( audioFile, kAudioFilePropertyPacketTableInfo, &size, &packetTableInfo );

		mPrimingFrames = packetTableInfo.mPrimingFrames;
		mRemainderFrames = packetTableInfo.mRemainderFrames;
		
		// Prime the queue with some data before starting
		AudioQueueAllocateBuffer( pQueue, bufferSize, &pBuffer );
		
		return true;
	
	} // Evita erros de compilação por causa dos gotos
	
	// Label de tratamento de erros
	Error:
		DELETE( this );
		return false;
}

