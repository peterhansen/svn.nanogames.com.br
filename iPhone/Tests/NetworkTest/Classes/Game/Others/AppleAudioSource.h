/*
 *  AppleAudioSource.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef APPLE_AUDIO_SOURCE_H
#define APPLE_AUDIO_SOURCE_H

#include "IAudioSource.h"

class AppleAudioSource : public IAudioSource
{
	public:
		// Construtor
		AppleAudioSource( AppleAudioManager* pAudioManager, uint8 soundIndex, AudioPtr pAudio ) : pAudioManager( pAudioManager ), soundIndex( soundIndex ), state( AUDIO_SOURCE_STATE_UNDEFINED ), looping( false ), wasPlayingBeforeSuspend( false ), volume( 1.0f ), currentPacket( 0 ), pAudio( pAudio )
		{
		}

		// Destrutor
		virtual ~AppleAudioSource( void )
		{
			pAudioManager = NULL;
		}

	private:
		// Tocador de som responsável por reproduzir este áudio
		AppleAudioManager* pAudioManager;

		// Índice deste áudio em pAudioManager
		const uint8 soundIndex;
	
		// Indica o estado de execução do áudio
		AudioState state;
	
		// Indica se a execução do áudio está em loop
		bool looping;

		// Indica se estávamos tocando o áudio antes de recebermos um evento suspend
		bool wasPlayingBeforeSuspend;
	
		// Volume deste áudio
		float volume;
	
		// Ponto de reprodução atual
		UInt64 currentPacket;
	
		// Áudio que esta fonte irá reproduzir
		AudioPtr pAudio;
};

#endif
