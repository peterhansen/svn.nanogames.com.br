/*
 *  IAudio.h
 *  NetworkTest
 *
 *  Created by Daniel Lopes Alves on 9/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef IAUDIO_H
#define IAUDIO_H

// Components
#include "ResourceManager.h"

// Tipo do ponteiro a ser utilizado para se referenciar um IAudio
typedef boost::shared_ptr< IAudio* > AudioPtr;

class IAudio : public ISharedResource
{
	public:
		// Destrutor
		virtual ~IAudio( void ){};
};

#endif
