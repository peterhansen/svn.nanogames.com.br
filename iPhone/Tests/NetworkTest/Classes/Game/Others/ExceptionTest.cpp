#include "ExceptionTest.h"

// C++

#include <memory>

// Components
#include "Exceptions.h"

#define KBYTE	1024LL
#define	MBYTE	( 1024LL * KBYTE )
#define GBYTE	( 1024LL * MBYTE )

struct LotsOfMem
{
	uint8 mem[ GBYTE ];
};

#define ARRAY_LEN 10

ExceptionTest::ExceptionTest( void ) : x( 0 )
{
	printf( ">>>>>>>> ExceptionTest - inicio\n\n" );

//	try
//	{
		LotsOfMem* lotsOfMem[ ARRAY_LEN ];
		memset( lotsOfMem, NULL, sizeof( LotsOfMem* ) * ARRAY_LEN );
		
		for( uint8 i = 0 ; i < ARRAY_LEN ; ++i )
		{
			// lotsOfMem[i] = new( std::nothrow )LotsOfMem();
			lotsOfMem[i] = new LotsOfMem();
			if( lotsOfMem[i] == NULL )
			{
				printf( ">>>>>>>> Nao conseguiu alocar lotsOfMem[%d]\n", i );
				break;
			}
			else
			{
				printf( ">>>>>>>> Conseguiu alocar lotsOfMem[%d] = %p\n", i, lotsOfMem[i] );
			}
		}
		
		for( uint8 i = 0 ; i < ARRAY_LEN ; ++i )
			delete lotsOfMem[i];
//	}
//	catch( std::bad_alloc& ex )
//	{
//		printf( ">>>>>>>> Exceção disparada por new => %s\n", ex.what() );
//	}
//	catch( ... )
//	{
//		printf( ">>>>>>>> Exceção disparada por new\n" );
//	}
	
	printf( "\n>>>>>>>> ExceptionTest - fim\n" );
	printf( "\n" );
}
