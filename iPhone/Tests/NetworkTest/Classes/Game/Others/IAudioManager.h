/*
 *  IAudioManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 8/28/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef IAUDIO_MANAGER_H
#define IAUDIO_MANAGER_H

#include "NanoTypes.h"

class IAudioManager
{
	public:
		// Destrutor
		virtual ~AppleAudioManager( void ){};
	
		// Suspende a execução do tocador de sons
		virtual bool suspend( void ) = 0;
	
		// Resume a execução do tocador de sons
		virtual bool resume( void ) = 0;
	
		// Determina o volume com que o tocador irá reproduzir seus sons
		virtual void setVolume( float volume ) = 0;
	
		// Retorna o volume com que o tocador está reproduzindo seus sons
		virtual float getVolume( void ) const = 0;
	
		// Retorna um som contido no tocador
		virtual ISound* getSound( uint8 soundIndex ) = 0;
	
		// Reproduz um som da aplicação. Caso o som ainda não esteja carregado, playSound se encarrega
		// de chamar loadSound
		virtual void playSound( uint8 soundName, uint8 soundIndex, bool looping, bool playAgainIfAlreadyPlaying = true ) = 0;
	
		// Pára a reprodução de um som
		virtual void stopSound( uint8 soundIndex ) = 0;
	
		// Pára a reprodução de todos os sons
		virtual void stopAllSounds( void ) = 0;

		// Pára / retoma a reprodução de um som
		virtual void pauseSound( uint8 soundIndex, bool paused ) = 0;
	
		// Pára / retoma a reprodução de todos os sons da aplicação
		virtual void pauseAllSounds( bool paused ) = 0;
	
		// Indica se algum som está sendo reproduzido
		virtual bool isPlaying( void ) const = 0;

		// Indica se o som está sendo reproduzido
		virtual bool isPlaying( uint8 soundIndex ) const = 0;

		// Retira um som da memória
		virtual bool unloadSound( uint8 soundIndex ) = 0;
	
		// Carrega um som na memória para ser reproduzido posteriormente
		virtual bool loadSound( uint8 soundName, uint8 soundIndex ) = 0;
};

#endif
