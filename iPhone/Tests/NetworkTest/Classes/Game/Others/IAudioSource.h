/*
 *  IAudioSource.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef IAUDIO_SOURCE_H
#define IAUDIO_SOURCE_H

#include "NanoTypes.h"

enum AudioSourceState
{
	AUDIO_SOURCE_STATE_UNDEFINED	= -1,
	AUDIO_SOURCE_STATE_STOPPED		=  0,
	AUDIO_SOURCE_STATE_PLAYING		=  1,
	AUDIO_SOURCE_STATE_PAUSED		=  2,
	AUDIO_SOURCE_STATE_SUSPENDED	=  3
};

// Valor a ser passado para play() para indicar loop infinito
#define AUDIO_SOURCE_ENDLESS_LOOP -1

class IAudioSource
{
	public:
		// Destrutor
		virtual ~IAudioSource( void ) = 0;
	
		// Inicia a execução do áudio, colocando-o no estado AUDIO_SOURCE_STATE_PLAYING
		virtual void play( int8 loopCount = 1, bool restartIfAlreadyPlaying = true ) = 0;
	
		// Pára a execução do áudio, colocando-o no estado AUDIO_SOURCE_STATE_STOPPED
		virtual void stop( void ) = 0;
		
		// Pausa a execução do áudio, colocando-o no estado AUDIO_SOURCE_STATE_PAUSED
		virtual void pause( bool pause ) = 0;
	
		// Determina o volume de reprodução deste áudio. O valor recebido será 
		virtual void setVolume( float v );
	
		// Retorna o volume de reprodução deste áudio
		float getVolume( void ) const;
	
		// Retorna quantas vezes o áudio ainda deve ser reproduzido antes de
		// voltar ao estado AUDIO_SOURCE_STATE_STOPPED
		int8 getLoopCount( void ) const;

		// Retorna o estado deste aúdio
		AudioSourceState getState( void ) const;
	
		// Trata a suspensão da aplicação, colocando o áudio no estado AUDIO_SOURCE_STATE_SUSPENDED
		void suspend( void );
	
		// Trata o retorno do estado AUDIO_SOURCE_STATE_SUSPENDED, colocando o áudio do estado 'stateBeforeSuspend'
		void resume( void );
	
	protected:
		// Construtor
		IAudioSource( void );

	private:
		// Volume deste áudio
		float volume;
	
		// Indica quantas vezes consecutivas o áudio será executado. Valores negativos indicam loop infinito
		int8 loopCount;
	
		// Estado de execução do áudio
		AudioSourceState audioSourceState;

		// Indica se estávamos tocando o áudio antes de recebermos um evento suspend
		AudioSourceState stateBeforeSuspend;
};

// Implementação dos métodos inline

IAudioSource::~IAudioSource( void )
{
}

void IAudioSource::setVolume( float v )
{
	volume = v;
}

float IAudioSource::getVolume( void ) const
{
	return volume;
}

int8 IAudioSource::getLoopCount( void ) const
{
	return loopCount;
}

AudioSourceState IAudioSource::getState( void ) const
{
	return audioSourceState;
}

#endif
