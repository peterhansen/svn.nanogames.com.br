/*
 *  ISharedResource.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ISHARED_RESOURCE_H
#define ISHARED_RESOURCE_H

// C++
#include <string>
#include <boost/shared_ptr.hpp>

// Components
#include "ResourceManager.h"

typedef boost::shared_ptr< void* > ShrdResPtr;

class ISharedResource
{
	public:
		// Destrutor
		virtual ~ISharedResource( void ) = 0;
	
		// Carrega um recurso que ainda não tenha sido carregado
		virtual ShrdResPtr loadResource( const std::string& fileName, const std::string& fileExt ) = 0;
	
	protected:
		// Obtém um recurso compartilhado. Caso ele ainda não esteja carregado, carrega-o na memória
		ShrdResPtr getResource( const std::string& fileName, const std::string& fileExt )
		{
			ShrdResPtr pResource = ResourceManager::IsResourceLoaded( fileName, fileExt );
			if( pResource != NULL )
				return pResource;
			else
				return ResourceManager::RegisterResource( fileName, fileExt, loadResource( fileName, fileExt ) );
		}
};

// Implementação dos métodos inline

ISharedResource::~ISharedResource( void )
{
}

#endif
