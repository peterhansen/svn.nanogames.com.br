#include "MemoryStream.h"

// C++
#include <cstdio>
#include <cstring>
#include <boost/shared_ptr.hpp>

// Components
#if DEBUG
	#include "Macros.h"
#endif

using namespace std;

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

MemoryStream::MemoryStream( void )
			: stream()
{
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

MemoryStream::MemoryStream( const uint8* pMemory, uint32 memSize )
			: stream( pMemory, pMemory + memSize )
{
}

/*==============================================================================================

DESTRUTOR

===============================================================================================*/

//MemoryStream::~MemoryStream( void )
//{
//}

/*==============================================================================================

MÉTODO readBool
	Lê um booleano de 8 bits.

===============================================================================================*/

bool MemoryStream::readBool( void )
{
	return readInt8() != 0 ? true : false;
}

/*==============================================================================================

MÉTODO readInt8
	Lê um inteiro de 8 bits.

===============================================================================================*/

int8 MemoryStream::readInt8( void )
{
	int8 ret = *reinterpret_cast< int8* >( &stream[ 0 ] );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + sizeof( int8 ) );

	return ret;
}

/*==============================================================================================

MÉTODO readInt16
	Lê um inteiro de 16 bits.

===============================================================================================*/

int16 MemoryStream::readInt16( void )
{
	int16 ret = *reinterpret_cast< int16* >( &stream[ 0 ] );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + sizeof( int16 ) );

	return ret;
}

/*==============================================================================================

MÉTODO readInt32
	Lê um inteiro de 32 bits.

===============================================================================================*/

int32 MemoryStream::readInt32( void )
{
	int32 ret = *reinterpret_cast< int32* >( &stream[ 0 ] );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + sizeof( int32 ) );

	return ret;
}

/*==============================================================================================

MÉTODO readInt64
	Lê um inteiro de 64 bits.

===============================================================================================*/

int64& MemoryStream::readInt64( int64& out )
{
	out = *reinterpret_cast< int64* >( &stream[ 0 ] );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + sizeof( int64 ) );

	return out;
}

/*==============================================================================================

MÉTODO readFloat
	Lê um ponto flutuante de 32 bits.

===============================================================================================*/

float MemoryStream::readFloat( void )
{
	float ret = *reinterpret_cast< float* >( &stream[ 0 ] );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + sizeof( float ) );

	return ret;
}

/*==============================================================================================

MÉTODO readDouble
	Lê um ponto flutuante de 64 bits.

===============================================================================================*/

double& MemoryStream::readDouble( double& out )
{
	out = *reinterpret_cast< double* >( &stream[ 0 ] );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + sizeof( double ) );

	return out;
}

/*==============================================================================================

MÉTODO readUTF8String
	Lê uma string de caracteres de 8 bits.

===============================================================================================*/

string& MemoryStream::readUTF8String( string& outStr )
{
	// OBS: O certo seria ter apenas um método readString e, de acordo
	// com o tamanho de caractere recebido, chamar a função de leitura
	// apropriada
	size_t charTypeSize = readInt8();
	int16 len = readInt16();

	if( len == 0 )
	{
		outStr = "";
	}
	else
	{
		// OBS: + 1 => Teremos que acrescentar o '\0'
		boost::shared_ptr< string::value_type > pTemp( new string::value_type[ len + 1 ] );
		string::value_type *pRaw = pTemp.get();

		if( pRaw != NULL )
		{
			readData( reinterpret_cast< uint8* >( pRaw ), sizeof( string::value_type ) * len );
			pRaw[ len ] = '\0';
			outStr = pRaw;
		}
	}
	return outStr;
}

/*==============================================================================================

MÉTODO readUTF32String
	Lê uma string de caracteres de 32 bits.

===============================================================================================*/

wstring& MemoryStream::readUTF32String( wstring& outWStr )
{
	// OBS: O certo seria ter apenas um método readString e, de acordo
	// com o tamanho de caractere recebido, chamar a função de leitura
	// apropriada
	size_t charTypeSize = readInt8();
	int16 len = readInt16();

	if( len == 0 )
	{
		outWStr = L"";
	}
	else
	{
		// OBS: + 1 => Teremos que acrescentar o '\0'
		boost::shared_ptr< wstring::value_type > pTemp( new wstring::value_type[ len + 1 ] );
		wstring::value_type *pRaw = pTemp.get();

		if( pRaw != NULL )
		{
			readData( reinterpret_cast< uint8* >( pRaw ), sizeof( wstring::value_type ) * len );
			pRaw[ len ] = '\0';
			outWStr = pRaw;
		}
		
		#if DEBUG
			LOG( "\n>>>>>>>>>>>> MemoryStream::readUTF32String = %ls\n", pRaw );
		
			for( int32 i = 0 ; i < len ; ++i )
				LOG( "%x ", pRaw[i] );

			LOG( "\n\n" );
		#endif
	}
	return outWStr;
}

/*==============================================================================================

MÉTODO readData
	Lê um array de bytes.

===============================================================================================*/

uint8* MemoryStream::readData( uint8* pOutBuffer, int32 bufferSize )
{
	memcpy( pOutBuffer, reinterpret_cast< int8* >( &stream[0] ), bufferSize );

	vector< uint8 >::iterator begin = stream.begin();
	stream.erase( begin, begin + bufferSize );

	return pOutBuffer;
}

vector< uint8 >& MemoryStream::readData( vector< uint8 >& outBuffer, int32 dataLen )
{
	vector< uint8 >::iterator streamBegin = stream.begin();
	
	if( dataLen >= 0 )
	{
		outBuffer.insert( outBuffer.end(), streamBegin, streamBegin + dataLen );
		stream.erase( streamBegin, streamBegin + dataLen );
	}
	else
	{
		vector< uint8 >::iterator streamEnd = stream.end();
		outBuffer.insert( outBuffer.end(), streamBegin, streamEnd );
		stream.erase( streamBegin, streamEnd );
	}

	return outBuffer;
}

/*==============================================================================================

MÉTODO writeBool
	Escreve um booleano de 8 bits.

===============================================================================================*/

void MemoryStream::writeBool( bool b )
{
	writeInt8( b ? 1 : 0 );
}

/*==============================================================================================

MÉTODO writeInt8
	Escreve um inteiro de 8 bits.

===============================================================================================*/

void MemoryStream::writeInt8( int8 n )
{
	uint8* pStart = reinterpret_cast< uint8* >( &n );
	stream.insert( stream.end(), pStart, pStart + sizeof( int8 ) );
}

/*==============================================================================================

MÉTODO writeInt16
	Escreve um inteiro de 8 bits.

===============================================================================================*/

void MemoryStream::writeInt16( int16 n )
{
	uint8* pStart = reinterpret_cast< uint8* >( &n );
	stream.insert( stream.end(), pStart, pStart + sizeof( int16 ) );
}

/*==============================================================================================

MÉTODO writeInt32
	Escreve um inteiro de 8 bits.

===============================================================================================*/

void MemoryStream::writeInt32( int32 n )
{
	uint8* pStart = reinterpret_cast< uint8* >( &n );
	stream.insert( stream.end(), pStart, pStart + sizeof( int32 ) );
}

/*==============================================================================================

MÉTODO writeInt64
	Escreve um inteiro de 8 bits.

===============================================================================================*/

void MemoryStream::writeInt64( const int64& n )
{
	const uint8* pStart = reinterpret_cast< const uint8* >( &n );
	stream.insert( stream.end(), pStart, pStart + sizeof( int64 ) );
}

/*==============================================================================================

MÉTODO writeFloat
	Escreve um ponto flutuante de 32 bits.

===============================================================================================*/

void MemoryStream::writeFloat( float n )
{
	uint8* pStart = reinterpret_cast< uint8* >( &n );
	stream.insert( stream.end(), pStart, pStart + sizeof( float ) );
}

/*==============================================================================================

MÉTODO writeDouble
	Escreve um ponto flutuante de 64 bits.

===============================================================================================*/

void MemoryStream::writeDouble( const double& n )
{
	const uint8* pStart = reinterpret_cast< const uint8* >( &n );
	stream.insert( stream.end(), pStart, pStart + sizeof( double ) );
}

/*==============================================================================================

MÉTODO writeUTF8String
	Escreve uma string de caracteres de 8 bits.

===============================================================================================*/

void MemoryStream::writeUTF8String( const string& str )
{
	size_t charTypeSize = sizeof( string::value_type );
	writeInt8( static_cast< int8 >( charTypeSize ));

	// Escreve o tamanho da string em bytes
	int16 len = static_cast< int16 >( str.empty() ? 0 : str.length() );
	writeInt16( len );

	// Escreve a string
	writeData( reinterpret_cast< const uint8* >( str.c_str() ), len * charTypeSize );
	
	// OLD
//	// Escreve o tamanho da string
//	int16 len = static_cast< int16 >( strlen( pStr ) );
//	writeInt16( len );
//
//	// Escreve a string
//	writeData( reinterpret_cast< const uint8* >( pStr ), sizeof( char ) * len );
}

/*==============================================================================================

MÉTODO writeUTF32String
	Escreve uma string de caracteres de 32 bits.

===============================================================================================*/

void MemoryStream::writeUTF32String( const wstring& wStr )
{
	size_t charTypeSize = sizeof( wstring::value_type );
	writeInt8( static_cast< int8 >( charTypeSize ) );

	// Escreve o tamanho da string
	int16 len = static_cast< int16 >( wStr.empty() ? 0 : wStr.length() );
	writeInt16( len );

	// Escreve a string
	writeData( reinterpret_cast< const uint8* >( wStr.c_str() ), len * charTypeSize );
	
	// OLD
//	// Escreve o tamanho da string
//	int16 len = static_cast< int16 >( wcslen( pWStr ) );
//	writeInt16( len );
//
//	// Escreve a string
//	writeData( reinterpret_cast< const uint8* >( pWStr ), sizeof( wchar_t ) * len );
}

/*==============================================================================================

MÉTODO writeData
	Escreve um array de bytes.

===============================================================================================*/

void MemoryStream::writeData( const uint8* pData, int32 dataLen )
{
	stream.insert( stream.end(), pData, pData + dataLen );
}

/*==============================================================================================

MÉTODO seek
	Avança a stream até a posição indicada.

===============================================================================================*/

void MemoryStream::seek( uint32 location )
{ 
	if( location >= stream.size() )
		clear();
	else
		stream.erase( stream.begin(), stream.begin() + location );
}
