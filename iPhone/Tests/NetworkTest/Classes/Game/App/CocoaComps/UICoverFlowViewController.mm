#import "UICoverFlowViewController.h"

// Extende a classe UIView para revelar os seus métodos 'heartbeat'
@interface UIView ( Extended )
	-( void ) startHeartbeat:( SEL )aSelector inRunLoopMode:( id )mode;
	-( void ) stopHeartbeat:( SEL )aSelector;
@end

//
// Classe privada FlipView
// Impede toques de chegarem até a camada Coverflow que está atrás
//
@interface FlipView : UIView
{
	@private
		UILabel *hFlipLabel;
}

@property ( readwrite, nonatomic, assign, getter = getFlipLabel, setter = setFlipLabel ) UILabel *hFlipLabel;

// Inicializa a view
-( bool )build;

// Libera a memória alocada pelo objeto
-( void )clean;

@end

// Início da implementação da classe
@implementation FlipView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hFlipLabel;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor.

==============================================================================================*/

-( FlipView* ) initWithFrame:( CGRect )frame
{
	if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self build] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

-( bool )build
{
	// Inicializa as variáveis da classe
	hFlipLabel = nil;

	{ // Evita erros de compilação por causa dos gotos

		// Add the flip label that shows the name and hex value
		hFlipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 480.0f, 480.0f)];
		if( hFlipLabel == nil )
			goto Error;

		hFlipLabel.textColor = [UIColor whiteColor];
		hFlipLabel.backgroundColor = [UIColor clearColor];
		hFlipLabel.textAlignment = UITextAlignmentCenter;
		hFlipLabel.font = [UIFont boldSystemFontOfSize:24.0f];
		hFlipLabel.userInteractionEnabled = NO;
		[hFlipLabel setNumberOfLines: 1];
		
		[self addSubview: hFlipLabel];
		[hFlipLabel release];
	
		return true;

	} // Evita erros de compilação por causa dos gotos

	Error:
		[self clean];
		return false;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self clean];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM clean
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )clean
{
	[hFlipLabel release];
}

/*==============================================================================================

MENSAGEM setText

==============================================================================================*/

-( void ) setText:( NSString* )hText
{
	hFlipLabel.text = hText;
}

/*==============================================================================================

MENSAGEM touchesEnded
	Chamada quando uma sequência de um toque termina.

==============================================================================================*/

#define CVC_VIEW_TAG 999

-( void )touchesEnded:( NSSet* )touches withEvent:( UIEvent* )event
{
	// When touched, remove the label and unflip the swatch
	[self removeFromSuperview];
	[( UICoverFlowView* )[[[UIApplication sharedApplication] keyWindow] viewWithTag: CVC_VIEW_TAG] flipSelectedCover];
}

#undef CVC_VIEW_TAG

// Fim da implementação da classe
@end


// Extensão da classe para declarar métodos privados
@interface UICoverFlowViewController ( Private )

// Inicializa a view
-( bool )buildWithItemsImgs:( NSArray* )hImgs AndTitles:( NSArray* )hTitles;

// Libera a memória alocada pelo objeto
-( void )clean;

@end

// Início da implementação da classe
@implementation UICoverFlowViewController

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hFlowView, currItem;

/*==============================================================================================

MENSAGEM init
	Construtor.

==============================================================================================*/

-( UICoverFlowViewController* )initWithItemsImgs:( NSArray* )hImgs AndTitles:( NSArray* )hTitles
{
	if( ( self = [super init] ) )
	{
		if( ![self buildWithItemsImgs: hImgs AndTitles: hTitles] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildWithItemsImgs:( NSArray* )hImgs AndTitles:( NSArray* )hTitles
{
	// Inicializa as variáveis da classe
	hFlippedView = nil;
	hFlowView = nil;
	hItemsImgs = nil;
	hItemsTitles = nil;

	currItem = -1;

	target = nil;
	selector = nil;

	flipOut = NO;
	
	{ // Evita erros de compilação por causa dos gotos
	
		hItemsImgs = [[NSMutableArray alloc] initWithArray: hImgs];
		if( !hItemsImgs )
			goto Error;

		hItemsTitles = [[NSMutableArray alloc] initWithArray: hTitles];
		if( !hItemsImgs )
			goto Error;
		
		// Create the flip object
		hFlippedView = [[FlipView alloc] initWithFrame: CGRectMake( 0.0f, 0.0f, 480.0f, 480.0f )];
		if( !hFlippedView )
			goto Error;

		if( UIInterfaceOrientationIsLandscape( [[UIApplication sharedApplication] statusBarOrientation] ) )
			[hFlippedView setTransform: CGAffineTransformMakeRotation( static_cast< float >( M_PI_2 ) )];

		[hFlippedView setUserInteractionEnabled: YES];
		
		// Initialize 
		hFlowView = [[UICoverFlowView alloc] initWithFrame: [[UIScreen mainScreen] applicationFrame] andCount: [hItemsTitles count]];
		if( !hFlowView )
			goto Error;

		[hFlowView setUserInteractionEnabled: YES];
		[hFlowView setHost: self];
		
		// Finish setting up the cover flow layer
		currItem = [hItemsTitles count] / 2;
		[[hFlowView getFlowLayer] selectCoverAtIndex: currItem];
		[[hFlowView getFlowLayer] setDelegate: self];
		
		return true;
		
	} // Evita erros de compilação por causa dos gotos

	Error:
		[self clean];
		return false;
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self clean];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM clean
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )clean
{
	[target release];

	[hFlippedView release];
	[hFlowView  release];

	[hItemsImgs release];
	[hItemsTitles release];
}

/*==============================================================================================

MENSAGEM coverFlow:selectionDidChange:

===============================================================================================*/

-( void )coverFlow:( UICoverFlowLayer* )hCoverFlow selectionDidChange:( int32 )index
{
	currItem = index;
	[[hFlowView getLabel] setText:[hItemsTitles objectAtIndex: index]];
}

/*==============================================================================================

MENSAGEM coverFlowFlipDidEnd:
	Detect the end of the flip -- both on reveal and hide.

===============================================================================================*/

-( void )coverFlowFlipDidEnd:( UICoverFlowLayer* )hCoverFlow 
{
	if( flipOut )
		[[[UIApplication sharedApplication] keyWindow] addSubview: hFlippedView];
	else
		[hFlippedView removeFromSuperview];
}

/*==============================================================================================

MENSAGEM coverFlow:requestImageAtIndex:quality:
	Detect the end of the flip -- both on reveal and hide.

===============================================================================================*/

-( void )coverFlow:( UICoverFlowLayer* )hCoverFlow requestImageAtIndex:( int32 )index quality:( int32 )quality
{
	UIImage *hCurrImg = [hItemsImgs objectAtIndex: index];
	[hCoverFlow setImage:[hCurrImg CGImage] atIndex: index type: quality];
}

/*==============================================================================================

MENSAGEM coverFlow:requestFlipLayerAtIndex:
	Return a flip layer, one that preferably integrates into the flip presentation.

===============================================================================================*/

-( id )coverFlow:( UICoverFlowLayer* )hCoverFlow requestFlipLayerAtIndex:( int32 )index
{
	if( flipOut )
		[hFlippedView removeFromSuperview];

	flipOut = !flipOut;
	
	// Prepare the flip text
	[hFlippedView setText:[NSString stringWithFormat:@"%@", [hItemsTitles objectAtIndex: index]]];
	
	// Flip with a simple blank square
	UIView *hView = [[[UIView alloc] initWithFrame: CGRectMake( 0.0f, 0.0f, 140.0f, 140.0f )] autorelease];
	[hView setBackgroundColor: [UIColor clearColor]];
	return [hView layer];
}

/*==============================================================================================

MENSAGEM selectedItem

===============================================================================================*/

-( int32 )selectedItem
{
	return currItem;
}

/*==============================================================================================

MENSAGEM start

===============================================================================================*/

-( void )start
{
	[hFlowView startHeartbeat: @selector( tick ) inRunLoopMode: ( id )kCFRunLoopDefaultMode];
	[[hFlowView getFlowLayer] transitionIn: 1.0f];
}

/*==============================================================================================

MENSAGEM stop

===============================================================================================*/

-( void )stop
{
	[hFlowView stopHeartbeat: @selector( tick )];
}

/*==============================================================================================

MENSAGEM loadView

===============================================================================================*/

-( void )loadView
{
	[super loadView];
	self.view = hFlowView;
	[self start];
}

/*==============================================================================================

MENSAGEM doubleTapCallback
	Callback para tratar cliques duplos.

===============================================================================================*/

-( void )doubleTapCallback
{
}

// Fim da implementação da classe
@end
