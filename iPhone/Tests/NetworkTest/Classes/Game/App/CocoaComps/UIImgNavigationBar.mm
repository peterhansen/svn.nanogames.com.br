#include "UIImgNavigationBar.h"

#include "ObjcMacros.h"
#include "Utils.h"

// Extensão da classe para declarar métodos privados
@interface UIImgNavigationBar ( Private )

// Inicializa a view
- ( bool )buildWithImg:( const std::string& )imgPath;

@end

// Início da implementação da classe
@implementation UIImgNavigationBar

/*==============================================================================================

MENSAGEM initWithFrame:AndImg:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithFrame:( CGRect )frame AndImg:( const std::string& )imgPath
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildWithImg: imgPath] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

- ( bool )buildWithImg:( const std::string& )imgPath
{
	hBkg = nil;

	[self setTranslucent: NO];
	[self setBarStyle: UIBarStyleDefault];
	
	char buffer[ PATH_MAX ];
	hBkg = [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, imgPath.c_str(), "png" ))];
	if( hBkg != nil )
	{
		[hBkg retain];
		return true;
	}
	return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessário personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

- ( void )drawRect:( CGRect )rect
{
	[hBkg drawInRect: rect];
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[hBkg release];
	hBkg = nil;

    [super dealloc];
}

// Fim da implementação da classe
@end


