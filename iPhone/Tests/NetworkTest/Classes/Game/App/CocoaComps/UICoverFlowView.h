/*
 *  CoverFlowView.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/25/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef COVER_FLOW_VIEW_H
#define COVER_FLOW_VIEW_H

// Apple
#import <UIKit/UIKit.h>
#import "UICoverFlowLayer.h"

// Components
#include "NanoTypes.h"

@protocol CoverFlowHost <NSObject>

	-( void )doubleTapCallback;

@end

@interface UIView ( LayerSet )

	-( void )setLayer:( id )aLayer;

@end

@interface UICoverFlowView : UIView
{
	@private
		id< CoverFlowHost > host;
		UICoverFlowLayer *hFlowLayer;
		UILabel *hLabel;
		UIImageView *hImg;
}

// Propriedades para manipularmos as variáveis da classe
@property ( readwrite, nonatomic, assign, getter = getLabel, setter = setLabel ) UILabel *hLabel;
@property ( readwrite, nonatomic, assign, getter = getHost, setter = setHost ) id < CoverFlowHost > host;
@property ( readwrite, nonatomic, assign, getter = getFlowLayer, setter = setFlowLayer ) UICoverFlowLayer *hFlowLayer;

// Construtor
-( UICoverFlowView* ) initWithFrame:( CGRect )frame andCount:( int32 )count;

// ...
-( void ) tick;

// ...
-( void ) flipSelectedCover;

@end

#endif
