#include "UILimitedTextField.h"

// C++
#include <limits>

#if DEBUG
	#include "ObjcMacros.h"
#endif

// Classe privada
@interface CustomTextFieldDelegate : NSObject< UITextFieldDelegate >
{
	@private
		uint32 minLen;
		uint32 maxLen;
	
		UITextField* hTxtField;

		id< UITextFieldDelegate > hOtherDelegate;
}

@property ( readwrite, nonatomic, assign, setter = setDelegate, getter = delegate ) id< UITextFieldDelegate > hOtherDelegate;

// Construtor
-( id )initWithTextField:( UITextField* )hTextField;

// Determina quantos caracteres a campo de texto suporta
-( void )setMinLimit:( uint32 )min AndMaxLimit:( uint32 )max;

// Verifica se a quantidade de caracteres do campo está dentro dos limites aceitos, fornecendo
// o respectivo feedback para o usuário.
-( void )checkLimits;

@end

// Implementação da classe privada
@implementation CustomTextFieldDelegate

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hOtherDelegate;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

-( id )initWithTextField:( UITextField* )hTextField;
{
    if( ( self = [super init] ) )
	{
		hTxtField = hTextField;
		[hTxtField addTarget: self action: @selector( onValueChanged ) forControlEvents: UIControlEventEditingChanged];

		minLen = 0;
		maxLen = std::numeric_limits< uint32 >::max();
	}
    return self;
}

/*==============================================================================================

MENSAGEM textFieldShouldBeginEditing:
	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( BOOL )textFieldShouldBeginEditing:( UITextField* )hTextField
{
	if( [hOtherDelegate respondsToSelector: @selector( textFieldShouldBeginEditing: )] )
		return [hOtherDelegate textFieldShouldBeginEditing: hTextField];
	return YES;
}

/*==============================================================================================

MENSAGEM textFieldShouldEndEditing:
 	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( BOOL )textFieldShouldEndEditing:( UITextField* )hTextField
{
	if( [hOtherDelegate respondsToSelector: @selector( textFieldShouldEndEditing: )] )
		return [hOtherDelegate textFieldShouldEndEditing: hTextField];
	return YES;
}

/*==============================================================================================

MENSAGEM textFieldDidBeginEditing:
 	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( void )textFieldDidBeginEditing:( UITextField* )hTextField
{
	if( [hOtherDelegate respondsToSelector: @selector( textFieldDidBeginEditing: )] )
		[hOtherDelegate textFieldDidBeginEditing: hTextField];
}

/*==============================================================================================

MENSAGEM textFieldDidEndEditing:
 	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( void )textFieldDidEndEditing:( UITextField* )hTextField
{
	if( [hOtherDelegate respondsToSelector: @selector( textFieldDidEndEditing: )] )
		[hOtherDelegate textFieldDidEndEditing: hTextField];
}

/*==============================================================================================

MENSAGEM textFieldShouldClear:
 	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( BOOL )textFieldShouldClear:( UITextField* )hTextField
{
	if( [hOtherDelegate respondsToSelector: @selector( textFieldShouldClear: )] )
		return [hOtherDelegate textFieldShouldClear: hTextField];
	return YES;
}

/*==============================================================================================

MENSAGEM textFieldShouldReturn:
 	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( BOOL )textFieldShouldReturn:( UITextField* )hTextField
{
	[hTextField resignFirstResponder];

	if( [hOtherDelegate respondsToSelector: @selector( textFieldShouldReturn: )] )
		return [hOtherDelegate textFieldShouldReturn: hTextField];

	return YES;
}

/*==============================================================================================

MENSAGEM textField:shouldChangeCharactersInRange:replacementString:
 	Ler documentação de UITextFieldDelegate.

===============================================================================================*/

-( BOOL )textField:( UITextField* )hTextField shouldChangeCharactersInRange:( NSRange )range replacementString:( NSString* )string
{
	BOOL ret = YES;
	if( [[[hTextField text] stringByReplacingCharactersInRange: range withString: string] length] > maxLen )
		ret = NO;
	
	if( [hOtherDelegate respondsToSelector: @selector( textField:shouldChangeCharactersInRange:replacementString: )] )
		ret &= [hOtherDelegate textField: hTextField shouldChangeCharactersInRange: range replacementString: string];

	return ret;
}

/*==============================================================================================

MENSAGEM setMinLimit:AndMaxLimit:
	Determina quantos caracteres a campo de texto suporta.

===============================================================================================*/

-( void )setMinLimit:( uint32 )min AndMaxLimit:( uint32 )max
{
	minLen = min;
	maxLen = max;
}

/*==============================================================================================

MENSAGEM checkLimits
 	Verifica se a quantidade de caracteres do campo está dentro dos limites aceitos, fornecendo
o respectivo feedback para o usuário.

===============================================================================================*/

-( void )checkLimits
{
	uint32 len = [[hTxtField text] length];
	if( ( len < minLen ) || ( len > maxLen ) )
		[hTxtField setTextColor: [UIColor redColor]];
	else
		[hTxtField setTextColor: [UIColor blackColor]];
}

/*==============================================================================================

MENSAGEM onValueChanged
 	Chamado quando o conteúdo da caixa de texto é alterado.

===============================================================================================*/

-( void )onValueChanged
{
	[self checkLimits];
}

// Fim da implementação da classe privada
@end

// Extensão da classe para declarar métodos privados
@interface UILimitedTextField ( Private )

// Inicializa o objeto
-( bool )buildUILimitedTextField;

// Libera a memória alocada pelo objeto
-( void )cleanUILimitedTextField;

// Método escondido para utilizar no workaround de teclados do tipo UIKeyboardTypeNumberPad
-( id< UITextFieldDelegate > )getHiddenDelegate;

@end

// Início da implementação da classe
@implementation UILimitedTextField

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildUILimitedTextField] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildUILimitedTextField] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	[self setClearsOnBeginEditing: NO];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	[self cleanUILimitedTextField];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM buildUILimitedTextField
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildUILimitedTextField
{
	hInsideDelegate = [[CustomTextFieldDelegate alloc] initWithTextField: self];
	if( hInsideDelegate == nil )
		return false;

	[super setDelegate: hInsideDelegate];

	[self setClearsOnBeginEditing: NO];

	return true;
}

/*==============================================================================================

MENSAGEM cleanUILimitedTextField
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )cleanUILimitedTextField
{
	[hInsideDelegate release];
	hInsideDelegate = nil;
}

/*==============================================================================================

MENSAGEM hiddenDelegate
	Método escondido para utilizar no workaround de teclados do tipo UIKeyboardTypeNumberPad.

===============================================================================================*/

-( id< UITextFieldDelegate > )hiddenDelegate
{
	return hInsideDelegate;
}

/*==============================================================================================

MENSAGEM setDelegate:
	Determina o delegate deste campo de texto.

===============================================================================================*/

-( void )setDelegate:( id< UITextFieldDelegate > )hOtherDelegate
{
	[hInsideDelegate setDelegate: hOtherDelegate];
}

/*==============================================================================================

MENSAGEM delegate
	Retorna o delegate deste campo de texto.

===============================================================================================*/

-( id< UITextFieldDelegate > )delegate
{
	return [hInsideDelegate delegate];
}

/*==============================================================================================

MENSAGEM setMinLimit:AndMaxLimit:
	Determina quantos caracteres a campo de texto suporta.

===============================================================================================*/

-( void )setMinLimit:( uint32 )min AndMaxLimit:( uint32 )max
{
	[hInsideDelegate setMinLimit: min AndMaxLimit: max];
}

// Fim da implementação da classe
@end
