/*
 *  UIImgNavigationBar.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/22/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UI_IMG_NAVIGATION_BAR_H
#define UI_IMG_NAVIGATION_BAR_H

// Apple Foundation
#include <UIKit/UIKit.h>

// C++
#include <string>

// Components
#include "NanoTypes.h"

@interface UIImgNavigationBar : UINavigationBar
{
	@private
		// Imagem utilizada como fundo da barra de navegação
		UIImage *hBkg;
}

// Construtor
-( id )initWithFrame:( CGRect )frame AndImg:( const std::string& )imgPath;

@end

#endif
