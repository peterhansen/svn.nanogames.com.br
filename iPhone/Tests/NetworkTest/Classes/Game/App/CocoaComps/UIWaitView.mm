#include "UIWaitView.h"

#include "ApplicationManager.h"

#define UIWAIT_VIEW_DEFAULT_TEXT @"Please Wait"

#define DIST_FROM_IND_N_LB 5.0f

// Extensão da classe para declarar métodos privados
@interface UIWaitView ( Private )

// Inicializa o objeto
-( bool )buildUIWaitView;

//// Libera a memória alocada pelo objeto
//-( void )cleanUIWaitView;

@end

// Início da implementação da classe
@implementation UIWaitView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildUIWaitView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildUIWaitView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildUIWaitView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildUIWaitView
{
	[self setHidden: YES];
	[self setUserInteractionEnabled: NO];
	[self setBackgroundColor: [UIColor blackColor]];
	[self setAlpha: 0.8f];
	
	hActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
	if( hActivityIndicator == nil )
		return false;
	
	[self addSubview: hActivityIndicator];
	[hActivityIndicator release];
	
	[hActivityIndicator setHidesWhenStopped: YES];
	[hActivityIndicator setFrame: CGRectMake( 0.0f, 0.0f, 32.0f, 32.0f )];
	
	hLbWaitMsg = [[UILabel alloc]init];
	if( hLbWaitMsg == nil )
		return false;
	
	[self addSubview: hLbWaitMsg];
	[hLbWaitMsg release];
	
	[hLbWaitMsg setText: UIWAIT_VIEW_DEFAULT_TEXT];
	[hLbWaitMsg setTextColor: [UIColor whiteColor]];
	[hLbWaitMsg setTextAlignment: UITextAlignmentCenter];
	[hLbWaitMsg setBackgroundColor: [UIColor clearColor]];
	[hLbWaitMsg setUserInteractionEnabled: NO];

	CGRect viewFrame = [self frame];
	//UIFont *hDefaultFont = [hLbWaitMsg font];
	UIFont *hDefaultFont = [UIFont fontWithName: @"Helvetica-Bold" size: 18.0f];
	[hLbWaitMsg setFont: hDefaultFont];
	[hLbWaitMsg setFrame: CGRectMake( 0.0f, 0.0f, viewFrame.size.width, [hDefaultFont ascender] - [hDefaultFont descender] )];
	
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self cleanUIWaitView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanUIWaitView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanUIWaitView
//{
//}

-( void )setFont:( UIFont* )hFont
{
	[hLbWaitMsg setFont: hFont];
}

-( void )setText:( NSString* )hText
{
	if( hText )
		[hLbWaitMsg setText: hText];
	else
		[hLbWaitMsg setText: UIWAIT_VIEW_DEFAULT_TEXT];
}

-( void )setTextColor:( UIColor* )hColor
{
	[hLbWaitMsg setTextColor: hColor];
}

-( void )setTextShadowColor:( UIColor* )hColor
{
	[hLbWaitMsg setShadowColor: hColor];
}
	
-( void )setTextShadowOffset:( CGSize )offset
{
	[hLbWaitMsg setShadowOffset: offset];
}

-( void )show
{
	[self setFrame: [self frame]];
	[hActivityIndicator startAnimating];
	[self setHidden: NO];
}

-( void )hide:( bool )destroy
{
	[self setHidden: YES];
	[hActivityIndicator stopAnimating];
	
	if( destroy )
		[self removeFromSuperview];
}

-( void )setFrame:( CGRect )frame
{
	[super setFrame: frame];

	if( ( hActivityIndicator != nil ) && ( hLbWaitMsg != nil ) )
	{
		// Posiciona os elementos da view
		CGRect actIndFrame = [hActivityIndicator frame];
		CGRect labelFrame = [hLbWaitMsg frame];

		float startY = ( frame.size.height - ( actIndFrame.size.height + DIST_FROM_IND_N_LB + labelFrame.size.height ) ) / 2.0f;

		//if( [[ApplicationManager GetInstance] isOrientationLandscape] )
			[hActivityIndicator setCenter: CGPointMake( frame.size.width / 2.0f, startY)];
		//else
		//	[hActivityIndicator setCenter: CGPointMake( HALF_SCREEN_WIDTH, startY)];
		
		startY += actIndFrame.size.height + DIST_FROM_IND_N_LB;
		
		[hLbWaitMsg setFrame: CGRectMake( ( frame.size.width - labelFrame.size.width ) / 2.0f, startY, labelFrame.size.width, labelFrame.size.height )];
	}
}

// Fim da implementação da classe
@end

