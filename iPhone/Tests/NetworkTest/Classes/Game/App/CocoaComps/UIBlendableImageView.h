/*
 *  UIBlendableImageView.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/22/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UIBLENDABLE_IMAGE_VIEW_H
#define UIBLENDABLE_IMAGE_VIEW_H

#include <UIKit/UIKit.h>

@interface UIBlendableImageView : UIView
{	
	@private
		// Imagem exibida pela view
		UIImage *hImage;
	
		// Cor utilizada para o blending
		UIColor *hBlendColor;
}

@end

#endif
