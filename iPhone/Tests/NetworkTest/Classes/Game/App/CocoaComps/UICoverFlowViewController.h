/*
 *  UICoverFlowViewController.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/25/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef COVER_FLOW_VIEW_CONTROLLER_H
#define COVER_FLOW_VIEW_CONTROLLER_H

// Apple
#import <UIKit/UIKit.h>

// Components
#include "NanoTypes.h"
#include "UICoverFlowView.h"

// Declarações adiadas
@class FlipView;

@interface UICoverFlowViewController : UIViewController <CoverFlowHost>
{
	@private
		FlipView *hFlippedView;
		UICoverFlowView *hFlowView;
	
		NSMutableArray *hItemsImgs;
		NSMutableArray *hItemsTitles;
	
		int32 currItem;

		id target;
		SEL selector;

		BOOL flipOut;
}

// Propriedades para manipularmos as variáveis da classe
@property ( readwrite, nonatomic, assign, getter = getFlowView, setter = setFlowView ) UICoverFlowView *hFlowView;
@property ( readwrite, nonatomic, assign, getter = getCurrItem, setter = setCurrItem ) int32 currItem;

// Construtor
-( UICoverFlowViewController* )initWithItemsImgs:( NSArray* )hImgs AndTitles:( NSArray* )hTitles;

@end

#endif
