#import "UICoverFlowView.h"

// Extensão da classe para declarar métodos privados
@interface UICoverFlowView ( Private )

// Inicializa a view
-( bool )buildWithCount:( int32 )count;

// Libera a memória alocada pelo objeto
-( void )clean;

@end

// Início da implementação da classe
@implementation UICoverFlowView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize host, hLabel, hFlowLayer;

/*==============================================================================================

MENSAGEM initWithFrame:andCount:
	Construtor.

==============================================================================================*/

-( UICoverFlowView* ) initWithFrame:( CGRect )frame andCount:( int32 )count
{
	if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildWithCount: count] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM buildWithCount:
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildWithCount:( int32 )count
{
	// Inicializa as variáveis da classe
	host = nil;
	hFlowLayer = nil;
	hLabel = nil;
	hImg = nil;

	{ // Evita erros de compilação por causa dos gotos

		hFlowLayer = [[UICoverFlowLayer alloc] initWithFrame:[[UIScreen mainScreen] bounds] numberOfCovers: count];
		if( hFlowLayer == nil )
			goto Error;

		[[self layer] addSublayer: hFlowLayer];

		// Add the placeholder (image stand-in) layer
		hImg = [[UIImageView alloc] initWithFrame: CGRectMake( 0.0f, 0.0f, 200.0f, 200.0f )];
		if( hImg == nil )
			goto Error;

		[hFlowLayer setPlaceholderImage: [hImg layer]];
		
		// Add its info (label) layer
		hLabel = [[UILabel alloc] init];
		if( hLabel == nil )
			goto Error;

		[hLabel setTextAlignment:UITextAlignmentCenter];
		[hLabel setFont:[UIFont boldSystemFontOfSize: 20.0f]];
		[hLabel setBackgroundColor:[UIColor colorWithRed: 0.0f green: 0.0f blue: 0.0f alpha: 0.0f]];
		[hLabel setTextColor:[UIColor colorWithRed: 1.0f green: 1.0f blue: 1.0f alpha: 0.75f]];
		[hLabel setNumberOfLines: 2];
		[hLabel setLineBreakMode:UILineBreakModeWordWrap];
		[hFlowLayer setInfoLayer:[hLabel layer]];
		
		return true;

	} // Evita erros de compilação por causa dos gotos

	Error:
		[self clean];
		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

-( void )dealloc
{
	[self clean];
	[super dealloc];
}

/*==============================================================================================

MENSAGEM clean
	Libera a memória alocada pelo objeto.

===============================================================================================*/

-( void )clean
{
	[host release];
	[hLabel release];
	[hImg release];
	[hFlowLayer release];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM touchesBegan
	Chamada quando uma nova sequência de toque começa.

==============================================================================================*/

-( void ) touchesBegan:( NSSet* )touches withEvent:( UIEvent* )event
{
	[hFlowLayer dragFlow: 0 atPoint: [[touches anyObject] locationInView: self ]];
}

/*==============================================================================================

MENSAGEM touchesMoved
	Chamada quando um toque já iniciado é movimentado.

==============================================================================================*/

-( void ) touchesMoved:( NSSet* )touches withEvent:( UIEvent* )event
{
	[hFlowLayer dragFlow: 1 atPoint: [[touches anyObject] locationInView: self]];
}

/*==============================================================================================

MENSAGEM touchesEnded
	Chamada quando uma sequência de um toque termina.

==============================================================================================*/

-( void ) touchesEnded:( NSSet* )touches withEvent:( UIEvent* )event
{
	if( [[touches anyObject] tapCount] == 2 )
	{
		if( host )
			[host doubleTapCallback];

		return;
	}
	
	[hFlowLayer dragFlow: 2 atPoint: [[touches anyObject] locationInView: self]];
}

/*==============================================================================================

MENSAGEM flipSelectedCover

==============================================================================================*/

-( void )flipSelectedCover
{
	[hFlowLayer flipSelectedCover];
}

/*==============================================================================================

MENSAGEM ignoresMouseEvents

==============================================================================================*/

-( BOOL )ignoresMouseEvents 
{
	return NO;
}

/*==============================================================================================

MENSAGEM tick

==============================================================================================*/

-( void )tick 
{
	[hFlowLayer displayTick];
}

// Fim da implementação da classe
@end
