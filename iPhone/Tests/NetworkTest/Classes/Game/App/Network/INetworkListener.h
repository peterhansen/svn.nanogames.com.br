/*
 *  INetworkListener.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef INETWORK_LISTENER_H
#define INETWORK_LISTENER_H 1

#include "NanoTypes.h"
#include "NetworkManagerErrors.h"

#include <string>
#include <vector>

class INetworkListener
{
	public:
		// Destrutor
		virtual ~INetworkListener( void ){};
	
		// Indica que ocorreu um erro na requisição
		virtual	void onNetworkError( NetworkManagerErrors errorCode, const std::string& errorStr ) = 0;

		// Indica que a requisição terminou com sucesso
		virtual void onNetworkRequestCompleted( std::vector< uint8 >& data ) = 0;
	
		// Indica que a requisição foi cancelada pelo usuário
		virtual void onNetworkRequestCancelled( void ) = 0;
};

#endif
