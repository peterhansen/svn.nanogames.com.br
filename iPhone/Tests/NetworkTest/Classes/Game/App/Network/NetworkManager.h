/*
 *  NetworkManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NETWORK_MANAGER_H
#define NETWORK_MANAGER_H 1

// Apple Foundation
#include <CFNetwork/CFHTTPAuthentication.h>
#include <CFNetwork/CFHTTPMessage.h>
#include <CFNetwork/CFHTTPStream.h>

// C++
#include <vector>

// Components
#include "NanoTypes.h"

// Declarações adiadas
class INetworkListener;

class NetworkManager
{
	public:
		// Construtor
		explicit NetworkManager( INetworkListener *pListener = NULL );
	
		// Destrutor
		~NetworkManager( void );

		// Inicia uma requisição HTTP. Quando retorna false, o chamador fica encarregado de
		// destruir pRequest. Em caso contrário, NetworkManager será o responsável por fazê-lo
		bool sendRequest( CFHTTPMessageRef pRequest );
	
		// Cancela a requisição pendente. Caso não haja uma requisição, não faz nada.
		void cancelPendingRequest( void );

	private:	
		// Libera a memória alocada pelo objeto
		void clean( bool shouldDeleteRequest = true );
	
		// Cria uma stream de leitura de dados. Retorna NULL em casos de erro
		CFReadStreamRef createReadStream( const CFHTTPMessageRef pHTTPRequest, bool autoRedirect );
	
		// Callback para tratar eventos das streams de leitura
		static void ReadEventHandler( CFReadStreamRef stream, CFStreamEventType event, void *pUser );
	
		// Última requisição HTTP feita
		CFHTTPMessageRef pCurrRequest;
	
		// Stream de leitura
		CFReadStreamRef pReadStream;
	
		// Buffer que armazena as respostas das requisições http
		std::vector< uint8 > responseBuffer;
	
		// Objeto que irá receber os eventos de NanoOnlineManager
		INetworkListener *pListener;
};

#endif
