/*
 *  FileSystem.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/5/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef FILE_SYSTEM_H
#define FILE_SYSTEM_H

// C++
#include <cstdio>
#include <string>

// Declarações adiadas
#include "MemoryStream.h"

enum FsOpRes
{
	FS_OK = 0,
	FS_APP_WDIR_NOT_FOUND = 1,
	FS_FILE_DO_NOT_EXIST = 2,
	FS_COULD_NOT_CREATE_FILE = 3,
	FS_READ_ERROR = 3,
	FS_WRITE_ERROR = 4,
	FS_COULD_NOT_DELETE = 5,
	FS_UNKNOWN_ERROR = 6
};

class FileSystem
{
	public:
		// Para ver os possíveis valores do parâmetro 'mode', consulte:
		// man fopen
		// Pode retornar:	
		// FS_APP_WDIR_NOT_FOUND
		// FS_FILE_DO_NOT_EXIST			( modes: "r", "rb", "r+" )
		// FS_COULD_NOT_CREATE_FILE		( modes: "w", "wb", "w+", "a", "ab, ""a+" )
		// FS_UNKNOWN_ERROR
		// FS_OK
		static FsOpRes Open( const std::string& fileName, const std::string& mode, FILE** pFile );

		// Fecha um arquivo, aplicando todas as modificações realizadas
		static void Close( FILE* pFile );

		// Pode retornar:
		// FS_READ_ERROR
		// FS_OK
		static FsOpRes ReadAllBytes( FILE* pFile, MemoryStream& buffer, bool decrypt = false );

		// Pode retornar:
		// FS_WRITE_ERROR
		// FS_OK
		static FsOpRes WriteAllBytes( FILE* pFile, const MemoryStream& buffer, bool encrypt = false );
	
		// Apaga um arquivo existente no disco
		// Pode retornar:
		// FS_APP_WDIR_NOT_FOUND
		// FS_FILE_DO_NOT_EXIST
		// FS_COULD_NOT_DELETE
		// FS_OK
		static FsOpRes DeleteFile( const std::string& fileName );

	private:
		// Não queremos criar, deletar ou copiar objetos do tipo FileSystem
		FileSystem();
		~FileSystem();
		FileSystem( const FileSystem& );
		FileSystem& operator=( const FileSystem& );
};

#endif
