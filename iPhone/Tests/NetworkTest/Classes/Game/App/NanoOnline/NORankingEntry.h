/*
 *  NORankingEntry.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/11/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_RANKING_ENTRY_H
#define NANO_ONLINE_RANKING_ENTRY_H 1

// Components
#include "ISerializable.h"
#include "NanoTypes.h"
#include "NOCustomer.h"

// C++
#include <string>

class NORankingEntry : public ISerializable
{
	public:
		// Construtores
		// O campo timeStamp é preenchido automaticamente com a data de criação do objeto NORankingEntry. Caso
		// deseje modificá-lo, utilizar o método setTimeStamp
		NORankingEntry( void );
		NORankingEntry( NOProfileId profileId, int64& score, const NOString& nickname, int32 rankingPos = -1 );
	
		// Destrutor
		virtual ~NORankingEntry( void ){};

		// Métodos Get
		NOProfileId getProfileId( void ) const;
		int32 getRankingPos( void ) const;
		int64& getScore( int64& out ) const;
		int64& getTimeStamp( int64& out ) const;
		NOString& getNickname( NOString& out ) const;

		// Métodos Set
		void setProfileId( NOProfileId newProfileId );
		void setRankingPos( int32 pos );
		void setScore( int64& newScore );
		void setTimeStamp( int64& stamp );
		void setNickname( const NOString& newNickname );
	
		// Lê o objeto de uma stream
		virtual void serialize( MemoryStream& stream ) const;

		// Escre o objeto em uma stream 
		virtual void unserialize( MemoryStream& stream );

	private:
		// Id do perfil do jogador
		NOProfileId profileId;
	
		// Pode parecer redundante, afinal, poderíamos ordenar NORankingEntries de acordo
		// com o campo score e utilizar a posição no conjunto para indicar a posição no
		// ranking. Mas quando estamos tratando um conjunto de NORankingEntries
		// retornados pelo servidor, podemos ter saltos entre entradas adjacentes. Isto
		// acontece quando queremos exibir pontuações de perfis registrados no device
		// que ficam fora dos 'n' 1os. Estes, por sua vez, sempre são mostrados
		int32 rankingPos;
	
		// Pontuação
		int64 score;
	
		// Data em que a pontuação foi alcançada
		int64 timeStamp;

		// Apelido do jogador
		NOString nickname;
};

// Implementação dos Métodos Inline

inline void NORankingEntry::setRankingPos( int32 pos )
{
	rankingPos = pos;
}

inline void NORankingEntry::setTimeStamp( int64& stamp )
{
	timeStamp = stamp;
}

inline NOProfileId NORankingEntry::getProfileId( void ) const
{
	return profileId;
}

inline int32 NORankingEntry::getRankingPos( void ) const
{	
	return rankingPos;
}

inline int64& NORankingEntry::getScore( int64& out ) const
{
	out = score;
	return out;
}	

inline NOString& NORankingEntry::getNickname( NOString& out ) const
{
	out = nickname;
	return out;
}

inline int64& NORankingEntry::getTimeStamp( int64& out ) const
{
	out = timeStamp;
	return out;
}

#endif
