#include "NOCustomer.h"

// C++
#include <cctype>
#include <boost/lexical_cast.hpp>

// Components
#include "INOListener.h"
#include "Macros.h"
#include "MemoryStream.h"
#include "NOConstants.h"
#include "NOConnection.h"
#include "NOMap.h"
#include "NORequestHolder.h"

// Define os comprimentos mínimo e máximo dos campos string
#define CUSTOMER_PASSWORD_MIN_LEN 6
#define CUSTOMER_PASSWORD_MAX_LEN 20

#define CUSTOMER_EMAIL_MIN_LEN 6
#define CUSTOMER_EMAIL_MAX_LEN 40

#define CUSTOMER_NICKNAME_MIN_LEN 2
#define CUSTOMER_NICKNAME_MAX_LEN 12

#define CUSTOMER_FIRSTNAME_MIN_LEN 0
#define CUSTOMER_FIRSTNAME_MAX_LEN 30

#define CUSTOMER_LASTNAME_MIN_LEN 0
#define CUSTOMER_LASTNAME_MAX_LEN 30

// Parâmetros de requisições ao NanoOnline
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME			0
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME			1
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME			2
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER		3
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL				4
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF					5
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD			6
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD_CONFIRM	7
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END			8
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENRE				9
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY			10
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID			11
#define NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP			12

#define NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME 0
#define NANO_ONLINE_CUSTOMER_PARAM_LOGIN_PASSWORD 1

// Códigos de retorno relativos à requisição de registro do usuário
#define RC_ERROR_NICKNAME_IN_USE		1
#define RC_ERROR_NICKNAME_FORMAT		2
#define RC_ERROR_NICKNAME_LENGTH		3
#define RC_ERROR_EMAIL_FORMAT			4
#define RC_ERROR_EMAIL_LENGTH			5
#define RC_ERROR_PASSWORD_WEAK			6
#define RC_ERROR_PASSWORD_CONFIRMATION	7
#define RC_ERROR_FIRST_NAME_LENGTH		8
#define RC_ERROR_LAST_NAME_LENGTH		9    
#define RC_ERROR_GENRE					10
#define RC_ERROR_GENRE_LENGTH			11
#define RC_ERROR_PHONE_NUMBER_FORMAT	12
#define RC_ERROR_PHONE_NUMBER_LENGTH	13
#define RC_ERROR_CPF_FORMAT				14

// Códigos de retorno relativos à requisição de login
#define RC_ERROR_LOGIN_NICKNAME_NOT_FOUND	1
#define RC_ERROR_LOGIN_PASSWORD_INVALID		2

/*==============================================================================================

FUNÇÃO IsEmailSpecialChar
	Retorna 0 se 'c' é um caractere especial válido de emails ou 1 em caso contrário.

===============================================================================================*/

int8 IsEmailSpecialChar( NOString::value_type c )
{
	NOString specialChars( L"!#$%&'*+-/=?ˆ_{}|~" );
	return specialChars.find_first_of( c ) == NOString::npos ? 0 : 1;
}

/*==============================================================================================

FUNÇÃO IsValidEmailPart
	Indica se a parte do email fornecido é válida. Caso não o seja, retorna o erro encontrado
em outError.

===============================================================================================*/

bool IsValidEmailPart( const NOString& emailPart, NOString& outError, bool needDots )
{
	NOString::const_iterator begin = emailPart.begin();
	
	if( *begin == '.' )
	{
		outError = L"Invalid email: Characater '.' cannot be the first one in a email part";
		return false;
	}
	else if( *emailPart.rbegin() == '.' )
	{
		outError = L"Invalid email: Characater '.' cannot be the last one in a email part";
		return false;
	}

	bool hasDots = false;
	NOString::value_type lastChar;
	NOString::const_iterator end = emailPart.end();
	
	for( NOString::const_iterator i = begin ; i != end ; ++i )
	{
		if( *i == '.' )
		{
			hasDots = true;

			if( lastChar == '.' )
			{
				outError = L"Invalid email: Characater '.' followed by another '.'";
				return false;
			}
		}
		else if( ( isdigit( *i ) == 0 ) && ( isalpha( *i ) == 0 ) && ( IsEmailSpecialChar( *i ) == 0 ) )
		{
			outError += L"Invalid email: Character ";
			outError += *i;
			outError += L" is not a valid email character";
			return false;
		}
		
		lastChar = *i;
	}
	
	if( needDots && !hasDots )
	{
		outError = L"Invalid email: Missing character '.' after '@'";
		return false;
	}

	return true;
}

/*==============================================================================================

FUNÇÃO IsValidEmail
	Indica se o email fornecido é válido. Caso não o seja, retorna o erro encontrado em outError.

===============================================================================================*/

bool IsValidEmail( const NOString& email, NOString& outError )
{
	// Valida a localização da '@'
	NOString::size_type i = email.find_first_of( '@' );
	if( i == 0 )
	{
		outError = L"Invalid email: First character is @";
		return false;
	}
	else if( i == NOString::npos )
	{
		outError = L"Invalid email: No character @ found";
		return false;
	}

	// Valida a primeira parte do email
	NOString firstPart = email.substr( 0, i );
	if( !IsValidEmailPart( firstPart, outError, false ) )
		return false;
	
	// Valida a segunda parte do email
	NOString secondPart = email.substr( i + 1 );
	return IsValidEmailPart( secondPart, outError, true );
}

/*==============================================================================================

FUNÇÃO IsValidPassword
	Indica se a senha fornecida é válida. Caso não o seja, retorna o erro encontrado em outError.

===============================================================================================*/

bool IsValidPassword( const NOString& password, NOString& outError )
{
	if( ( password.find_first_of( L"0123456789", 0 ) == NOString::npos ) || ( password.find_first_of( L"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 0 ) == NOString::npos ) )
	{
		outError = L"Invalid password: Your password must contain letters and numbers";
	   return false;
	}

	return true;
}

#define NO_MAX_AGE 120
#define NO_MIN_AGE 2

/*==================================================================================

MÉTODO IsValidNOBirthday
	Verifica se a data passada é uma data válida para representar o aniversário de
um usuário do NanoOnline.

===================================================================================*/

bool IsValidNOBirthday( uint8 day, uint8 month, uint32 year, NOString& outError )
{
	if( month == 0 || month > 12 )
	{
		outError = L"Months must be in the interval [1, 12]";
		return false;
	}

	uint32 currYear;
	Utils::Today( NULL, NULL, &currYear );
	uint32 minYear = currYear - NO_MAX_AGE;	// Se alguém com mais de 120 anos estiver jogando, merece um prêmio
	uint32 maxYear = currYear - NO_MIN_AGE;	// Menos de 2 anos é pouco, mas os pais podem estar criando um perfil para o filho
	
	if( ( year < minYear ) || ( year > maxYear ) )
	{
		std::wstringstream temp;
		temp << L"Years must be in the interval [" << minYear << L", " << maxYear << L"]";
		outError = temp.str();

		return false;
	}
	
	if( day == 0 )
	{
		outError = L"Days must be a value grater than zero";
		return false;
	}

	if( month == 2 )
	{
		uint8 maxFebDays = Utils::IsLeapYear( year ) ? 29 : 28;
		
		if( day > maxFebDays )
		{
			std::stringstream temp;
			temp << "February of the year " << year << " has only " << maxFebDays << " days";
			return false;
		}
	}
	else if( ( month == 4 ) || ( month == 6 ) || ( month == 9 ) || ( month == 11 ) )
	{
		if( day > 30 )
		{
			std::wstringstream temp;
			temp << "Month " <<  month << " has only 30 days";
			outError = temp.str();

			return false;
		}
	}
	else
	{
		if( day > 31 )
		{
			std::wstringstream temp;
			temp << "Month " <<  month << " has only 31 days";
			outError = temp.str();

			return false;
		}
	}
	return true;
}

/*==============================================================================================

FUNÇÃO CheckStrFieldLen
	Verifica se o comprimento do campo string é válido.

===============================================================================================*/

bool CheckStrFieldLen( const NOString& fieldName, uint16 len, uint16 min, uint16 max, NOString& outError )
{
	if( len < min )
	{
		std::wstringstream temp;
		temp << L"Invalid " << fieldName << L": Must be, at least, " << min << L" characters long";
		outError = temp.str();
		return false;
	}

	if( len > max )
	{
		std::wstringstream temp;
		temp << L"Invalid " + fieldName + L": Must have " << max << L" characters at maximum";
		outError = temp.str();
		return false;
	}
	
	return true;
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

NOCustomer::NOCustomer( void )
				  : profileId( CUSTOMER_PROFILE_ID_NONE ),
					rememberMe( true ),
					rememberPassword( false ),
					genre( GENRE_N ),
					encodedBirthday( 0 ),
					timeStamp( 0 ),
					email(),
					password(),
					nickname(),
					firstName(),
					lastName()
{
}

/*==============================================================================================

DESTRUTOR

===============================================================================================*/

NOCustomer::~NOCustomer( void )
{
}

/*==============================================================================================

MÉTODO isRegistered
	Indica se algum usuário está registrado.

===============================================================================================*/

bool NOCustomer::isRegistered( void ) const
{
	return profileId > 0;
}

/*==============================================================================================

MÉTODO setGenre
	Determina o sexo do usuário.

===============================================================================================*/

void NOCustomer::setGenre( Genre g )
{
	genre = g;
}

/*==============================================================================================

MÉTODO setRememberMe
	Determina se devemos armazenar este usuário localmente.

===============================================================================================*/

void NOCustomer::setRememberMe( bool b )
{
	rememberMe = b;
}

/*==============================================================================================

MÉTODO setRememberPassword
	Determina se devemos armazenar o password do usuário.

===============================================================================================*/

void NOCustomer::setRememberPassword( bool b )
{
	rememberPassword = b;
}

/*==============================================================================================

MÉTODO setNickname
	Determina o apelido do usuário do NanoOnline.

===============================================================================================*/

bool NOCustomer::setNickname( const NOString& customerNickname, NOString& outError )
{
	// Valida o tamanho da string
	uint32 min, max;
	GetNicknameSupportedLen( min, max );
	if( !CheckStrFieldLen( L"nickname", customerNickname.empty() ? 0 : customerNickname.length(), min, max, outError ) )
		return false;
	
	nickname = customerNickname;
	return true;
}

/*==============================================================================================

MÉTODO setFirstName
	Determina o primeiro nome do usuário do NanoOnline.

===============================================================================================*/

bool NOCustomer::setFirstName( const NOString& customerFirstName, NOString& outError )
{
	// Valida o tamanho da string
	uint32 min, max;
	GetFirstNameSupportedLen( min, max );
	if( !CheckStrFieldLen( L"first name", customerFirstName.empty() ? 0 : customerFirstName.length(), min, max, outError ) )
		return false;
	
	firstName = customerFirstName;
	return true;
}

/*==============================================================================================

MÉTODO setLastName
	Determina o último nome do usuário do NanoOnline.

===============================================================================================*/

bool NOCustomer::setLastName( const NOString& customerLastName, NOString& outError )
{
	// Valida o tamanho da string
	uint32 min, max;
	GetLastNameSupportedLen( min, max );
	if( !CheckStrFieldLen( L"last name", customerLastName.empty() ? 0 : customerLastName.length(), min, max, outError ) )
		return false;
	
	lastName = customerLastName;
	return true;
}

/*==============================================================================================

MÉTODO setEmail
	Determina o email do usuário do NanoOnline.

===============================================================================================*/

bool NOCustomer::setEmail( const NOString& customerEmail, NOString& outError )
{
	// Valida o tamanho da string
	uint32 min, max;
	GetEmailSupportedLen( min, max );
	if( !CheckStrFieldLen( L"email", customerEmail.empty() ? 0 : customerEmail.length(), min, max, outError ) )
		return false;
	
	// Valida o conteúdo da string
	if( !IsValidEmail( customerEmail, outError ) )
		return false;
	
	email = customerEmail;
	return true;
}

/*==============================================================================================

MÉTODO setPassword
	Determina a senha do usuário do NanoOnline.

===============================================================================================*/

bool NOCustomer::setPassword( const NOString& customerPassword, NOString& outError )
{
	// Valida o tamanho da string
	uint32 min, max;
	GetPasswordSupportedLen( min, max );
	if( !CheckStrFieldLen( L"password", customerPassword.empty() ? 0 : customerPassword.length(), min, max, outError ) )
		return false;
	
	// Valida o conteúdo da string
	if( !IsValidPassword( customerPassword, outError ) )
		return false;
	
	password = customerPassword;
	return true;
}

/*==============================================================================================

MÉTODO serialize
	Lê o objeto de uma stream.

===============================================================================================*/

void NOCustomer::serialize( MemoryStream& stream ) const
{
	stream.writeInt32( profileId );
		
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.writeUTF32String( nickname );
	stream.writeUTF32String( firstName );
	stream.writeUTF32String( lastName );
#else
	stream.writeUTF8String( nickname );
	stream.writeUTF8String( firstName );
	stream.writeUTF8String( lastName );
#endif

	// TODOO
//	stream.writeUTF8String( email );
	stream.writeUTF32String( email );
	
	// No disco não precisamos seguir o padrão do NanoOnline,
	// então podemos salvar o gênero como um byte
#if DEBUG
	int8 g = static_cast< int8 >( genre );
	stream.writeInt8( g );
#else
	stream.writeInt8( static_cast< int8 >( genre ) );
#endif
	
	stream.writeInt64( encodedBirthday );
	stream.writeInt64( timeStamp );

	stream.writeBool( rememberPassword );
	if( rememberPassword )
	{
		// TODOO
//		stream.writeUTF8String( password );
		stream.writeUTF32String( password );
	}
}

/*==============================================================================================

MÉTODO unserialize
	Escre o objeto em uma stream.

===============================================================================================*/

void NOCustomer::unserialize( MemoryStream& stream )
{
	profileId = stream.readInt32();

#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.readUTF32String( nickname );
	stream.readUTF32String( firstName );
	stream.readUTF32String( lastName );
#else
	stream.readUTF8String( nickname );
	stream.readUTF8String( firstName );
	stream.readUTF8String( lastName );
#endif

	// TODOO
//	stream.readUTF8String( email );
	stream.readUTF32String( email );
	
	// No disco não precisamos seguir o padrão do NanoOnline,
	// então podemos salvar o gênero como um byte
#if DEBUG
	int8 g = stream.readInt8();
	genre = static_cast< Genre >( g );
#else
	genre = static_cast< Genre >( stream.readInt8() );
#endif
	
	int64 aux;
	encodedBirthday = static_cast< time_t >( stream.readInt64( aux ) );
	timeStamp = static_cast< time_t >( stream.readInt64( aux ) );

	rememberPassword = stream.readBool();
	if( rememberPassword )
	{
		// TODOO
//		stream.readUTF8String( password );
		stream.readUTF32String( password );
	}
}

/*==============================================================================================

MÉTODO getBirthday

===============================================================================================*/

void NOCustomer::getBirthday( uint8* day, uint8* month, uint32* year ) const
{
	struct tm decodedBirthday;
	memset( &decodedBirthday, 0, sizeof( struct tm ));

	if( encodedBirthday != 0 )
		gmtime_r( &encodedBirthday, &decodedBirthday );
	else
		decodedBirthday.tm_mon = -1;
	
	if( day )
		*day = decodedBirthday.tm_mday;
	
	if( month )
		*month = decodedBirthday.tm_mon + 1; // Mês, nessa estrutura, vai de 0  a 11, mas dia vai de 1 a 31... Resolvi seguir o padrão do campo dia
	
	if( year )
		*year = decodedBirthday.tm_year + 1900;
}

/*==============================================================================================

MÉTODO setBirthday

===============================================================================================*/

bool NOCustomer::setBirthday( uint8 day, uint8 month, uint32 year, NOString& outError )
{
	if( !IsValidNOBirthday( day, month, year, outError ) )
		return false;

	char *tz = getenv( "TZ" );
	setenv( "TZ", "", 1 );
	tzset();

	struct tm birthday;
	memset( &birthday, 0, sizeof( struct tm ) );
	birthday.tm_mday = day;
	birthday.tm_mon = month - 1; // Mês, nessa estrutura, vai de 0  a 11, mas dia vai de 1 a 31... Resolvi seguir o padrão do campo dia
	birthday.tm_year = year - 1900;
	encodedBirthday = timegm( &birthday );
	
	// Se não conseguirmos representar a data por algum motivo obscuro, ignora-a
	if( encodedBirthday < 0 )
		encodedBirthday = 0;
	
	if( tz )
		setenv( "TZ", tz, 1 );
	else
		unsetenv( "TZ" );
	tzset();
	
	return true;
}

/*==============================================================================================

OPERADOR ==

===============================================================================================*/

bool NOCustomer::operator ==( const NOCustomer& rho ) const
{
	// Vários pontos do código do NanoOnline se baseiam nessa comparação feita apenas
	// por profileId. Na verdade, esta comparação não é uma má escolha, pois um usuário
	// cadastrado neste aparelho pode voltar mais tarde e baixar as atualizações feitas
	// em seu perfil através de outro device. Neste caso, profileId seria igual, mas todos
	// os outros campos poderiam conter valores diferentes, só que o usuário continua
	// sendo o mesmo
	return this->profileId == rho.profileId;
}

/*==============================================================================================

OPERADOR !=

===============================================================================================*/

bool NOCustomer::operator !=( const NOCustomer& rho ) const
{
	return !( *this == rho );
}

/*==============================================================================================

MÉTODO login
	Tenta fazer login do usuário no NanoOnline.

===============================================================================================*/

bool NOCustomer::login( MemoryStream& stream )
{
	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME );
	
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.writeUTF32String( nickname );
#else
	stream.writeUTF8String( nickname );
#endif

	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_PASSWORD );
	
	// TODOO
//	stream.writeUTF8String( password );
	stream.writeUTF32String( password );
	
	return true;
}

/*==============================================================================================

MÉTODO loginResponse
	Trata a resposta do login do usuário no NanoOnline.

===============================================================================================*/

bool NOCustomer::loginResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			{
				// O login é a única ação na qual o ID do usuário vem fora de NANO_ONLINE_ID_SPECIFIC_DATA
				if( table.find( NANO_ONLINE_ID_CUSTOMER_ID ) == table.end() )
				{
					errorStr = L"Invalid response";
					return false;
				}
				else
				{
					// Armazena o profileId do usuário
					profileId = *static_cast< const int32* >( table[ NANO_ONLINE_ID_CUSTOMER_ID ].get() );
				}
			}
			return true;
					
		case RC_ERROR_LOGIN_PASSWORD_INVALID:
			errorStr = L"Invalid password";
			return false;
			
		case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
			errorStr = L"Invalid nickname";
			return false;
			
		default:
			#if DEBUG
				assert_n_log( false, "Unrecognized param return code sent to loginResponse in response to login request" );
			#else
				errorStr = L"Unknown error";
			#endif
			return false;
	}
}

/*==============================================================================================

MÉTODO updateProfile
	Atualiza os dados do perfil do usuário.

===============================================================================================*/

bool NOCustomer::updateProfile( MemoryStream& stream )
{
	// Tentar atualizar um perfil que não possui um ID é um erro de lógica. A interface
	// do NanoOnline não deveria permitir que isso acontecesse...
	if( profileId == CUSTOMER_PROFILE_ID_NONE )
		return false;

	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID );
	stream.writeInt32( profileId );
	
	return createProfile( stream );
}

/*==============================================================================================

MÉTODO createProfile
	Informa os dados do perfil do usuário.

===============================================================================================*/

bool NOCustomer::createProfile( MemoryStream& stream )
{	
	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME );
#ifdef NANO_ONLINE_UNICODE_SUPPORT
	stream.writeUTF32String( nickname );
#else
	stream.writeUTF8String( nickname );
#endif

	if( !firstName.empty() )
	{
		stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME );

#ifdef NANO_ONLINE_UNICODE_SUPPORT
		stream.writeUTF32String( firstName );
#else
		stream.writeUTF8String( firstName );
#endif
	}
	
	if( !lastName.empty() )
	{
		stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME );

#ifdef NANO_ONLINE_UNICODE_SUPPORT
		stream.writeUTF32String( lastName );
#else
		stream.writeUTF8String( lastName );
#endif
	}

	// TODOO
//	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL );
//	stream.writeUTF8String( email );
//	
//	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD );
//	stream.writeUTF8String( password );
	
	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL );
	stream.writeUTF32String( email );
	
	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD );
	stream.writeUTF32String( password );
	
	if( genre != GENRE_N )
	{
		stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENRE );
		
		// Int32 => Caracteres seguem o padrão das strings do NanoOnline,
		// então possuem o tamanho de um wchar_t na plataforma
		stream.writeInt32( genre == GENRE_M ? 'm' : 'f' );
	}

	if( encodedBirthday != 0 )
	{
		stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY );
		stream.writeInt64( encodedBirthday );
	}

	// Não utilizamos estes campos
//	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER );
//	stream.writeUTF8String( phoneNumber );
//	
//	stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF );
//	stream.writeUTF8String( cpf );
	
	return true;
}

/*==============================================================================================

MÉTODO createProfileResponse
	Trata a resposta do cadastro do perfil no NanoOnline.

===============================================================================================*/

bool NOCustomer::createProfileResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			{
				std::vector< uint8 >* pSpecificData = static_cast< std::vector< uint8 >* >( table[ NANO_ONLINE_ID_SPECIFIC_DATA ].get() );
				
				MemoryStream dataStream;
				dataStream.swapBuffer( *pSpecificData );
				
				return readCreateProfileData( dataStream, errorStr );
			}
			return true;
			
		case RC_ERROR_NICKNAME_IN_USE:
			errorStr = L"Nickname already in use";
			return false;

		case RC_ERROR_NICKNAME_FORMAT:
			errorStr = L"Invalid nickname format";
			return false;
			
		case RC_ERROR_NICKNAME_LENGTH:
			errorStr = L"Invalid nickname length";
			return false;
			
		case RC_ERROR_EMAIL_FORMAT:
			errorStr = L"Invalid email format";
			return false;
			
		case RC_ERROR_EMAIL_LENGTH:
			errorStr = L"Invalid email length";
			return false;
			
		case RC_ERROR_PASSWORD_WEAK:
			errorStr = L"Weak password. Must be at least 6 characters long and contain numbers and letters";
			return false;
			
		case RC_ERROR_PASSWORD_CONFIRMATION:
			errorStr = L"Invalid password confirmation";
			return false;
			
		case RC_ERROR_FIRST_NAME_LENGTH:
			errorStr = L"Invalid first name length";
			return false;
			
		case RC_ERROR_LAST_NAME_LENGTH: 
			errorStr = L"Invalid last name length";
			break;

		case RC_ERROR_GENRE:
			errorStr = L"Invalid genre";
			return false;

		case RC_ERROR_GENRE_LENGTH:
			errorStr = L"Invalid genre length";
			return false;
			
//		case RC_ERROR_PHONE_NUMBER_FORMAT:
//			errorStr = "Invalid phone number format";
//			return false;
//			
//		case RC_ERROR_PHONE_NUMBER_LENGTH:
//			errorStr = "Invalid phone number length";
//			return false;
//			
//		case RC_ERROR_CPF_FORMAT:
//			errorStr = "Invalid cpf format";
//			return false;
			
		default:
			#if DEBUG
				assert_n_log( false, "Unrecognized param return code sent to createProfileResponse in response to createProfile request" );
			#else
				errorStr = L"Unknown error";
			#endif
			return false;
	}
	return false;
}

/*==============================================================================================

MÉTODO readCreateProfileData
	Lê os dados específicos da criação de perfil.

===============================================================================================*/

bool NOCustomer::readCreateProfileData( MemoryStream& dataStream, NOString& errorStr )
{	
	while( !dataStream.eos() )
	{
		#if DEBUG
		int8 id = dataStream.readInt8();
		LOG( ">>> NOCustomer::readCreateProfileData => Read attrib id = %d\n", static_cast< int32 >( id ) );
		switch( id )
		#else
		switch( dataStream.readInt8() )
		#endif
		{
			case NANO_ONLINE_ID_CUSTOMER_ID:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID:
				profileId = dataStream.readInt32();
				break;

			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP:
				{
					int64 aux;
					timeStamp = static_cast< time_t >( dataStream.readInt64( aux ) );
				}
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END:
				return true;

			// Não sabe ler o campo, então ignora
			default:
				#if DEBUG
					assert_n_log( false, ">>> NOCustomer::readCreateProfileData => Unknown parameter" );
				#endif
				break;
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO download
	Indica qual perfil deverá ser baixado.

===============================================================================================*/

bool NOCustomer::download( MemoryStream& stream )
{
	if( login( stream ) )
	{
		stream.writeInt8( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP );
		stream.writeInt64( timeStamp );
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO downloadResponse
	Trata a resposta do download do perfil.

===============================================================================================*/

bool NOCustomer::downloadResponse( NOMap& table, NOString& errorStr )
{
	switch( *( static_cast< const int8* >( table[ NANO_ONLINE_ID_RETURN_CODE ].get() ) ) )
	{
		case NANO_ONLINE_RC_OK:
			{
				std::vector< uint8 >* pSpecificData = static_cast< std::vector< uint8 >* >( table[ NANO_ONLINE_ID_SPECIFIC_DATA ].get() );
				
				MemoryStream dataStream;
				dataStream.swapBuffer( *pSpecificData );
				
				return readDownloadData( dataStream, errorStr );
			}
			break; // Nunca alcançará este break
					
		case RC_ERROR_LOGIN_PASSWORD_INVALID:
			errorStr = L"Invalid password";
			return false;
			
		case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
			errorStr = L"Invalid nickname";
			return false;
			
		default:
			#if DEBUG
				assert_n_log( false, "Unrecognized param return code sent to downloadResponse in response to download request" );
			#else
				errorStr = L"Unknown error";
			#endif
			return false;
	}
}

/*==============================================================================================

MÉTODO readDownloadData
	Lê os dados específicos do download de perfil.

===============================================================================================*/

bool NOCustomer::readDownloadData( MemoryStream& dataStream, NOString& errorStr )
{
	while( !dataStream.eos() )
	{
		#if DEBUG
		int8 id = dataStream.readInt8();
		LOG( ">>> NOCustomer::readDownloadData => Read attrib id = %d\n", static_cast< int32 >( id ) );
		switch( id )
		#else
		switch( dataStream.readInt8() )
		#endif
		{
			case NANO_ONLINE_ID_CUSTOMER_ID:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID:
				profileId = dataStream.readInt32();
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( nickname );
				#else
					dataStream.readUTF8String( nickname );
				#endif
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( firstName );
				#else
					dataStream.readUTF8String( firstName );
				#endif
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( lastName );
				#else
					dataStream.readUTF8String( lastName );
				#endif
				break;
			
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL:
				#ifdef NANO_ONLINE_UNICODE_SUPPORT
					dataStream.readUTF32String( email );
				#else
					dataStream.readUTF8String( email );
				#endif
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENRE:
				{
					#ifdef NANO_ONLINE_UNICODE_SUPPORT

						NOString::value_type c = dataStream.readInt32();
						if( ( c == L'm' ) || ( c == L'M' ) )
							genre = GENRE_M;
						else if( ( c == L'f' ) || ( c == L'F' ) )
							genre = GENRE_F;
						else
							genre = GENRE_N;

					#else

						char c = dataStream.readInt8();
						if( ( c == 'm' ) || ( c == 'M' ) )
							genre = GENRE_M;
						else if( ( c == 'f' ) || ( c == 'F' ) )
							genre = GENRE_F;
						else
							genre = GENRE_N;

					#endif
				}	
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY:
				{
					int64 aux;
					encodedBirthday = static_cast< time_t >( dataStream.readInt64( aux ) );
				}
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP:
				{
					int64 aux;
					timeStamp = static_cast< time_t >( dataStream.readInt64( aux ) );
				}
				break;
				
			// Ignora esses campos
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF:
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD_CONFIRM:
				{
					NOString dummy;
					
					#ifdef NANO_ONLINE_UNICODE_SUPPORT
						dataStream.readUTF32String( dummy );
					#else
						dataStream.readUTF8String( dummy );
					#endif
				}
				break;
				
			case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END:
				return true;

			// Não sabe ler o campo, então ignora
			default:
				#if DEBUG
					assert_n_log( false, ">>> NOCustomer::readDownloadData => Unknown parameter" );
				#endif
				break;
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO SendCreateRequest
	Envia uma requisição de cadastro ao NanoOnline.

===============================================================================================*/

bool NOCustomer::SendCreateRequest( NOCustomer* pCustomer, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/customers/add", MakeNORequestHolder( *pCustomer, &NOCustomer::createProfile, &NOCustomer::createProfileResponse ) );
}

/*==============================================================================================

MÉTODO SendEditRequest
	Envia uma requisição de atualização de cadastro ao NanoOnline.

===============================================================================================*/

bool NOCustomer::SendEditRequest( NOCustomer* pCustomer, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/customers/refresh", MakeNORequestHolder( *pCustomer, &NOCustomer::updateProfile, &NOCustomer::createProfileResponse ) );
}

/*==============================================================================================

MÉTODO SendLoginRequest
	Envia uma requisição de login ao NanoOnline.

===============================================================================================*/

bool NOCustomer::SendLoginRequest( NOCustomer* pCustomer, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/customers/login", MakeNORequestHolder( *pCustomer, &NOCustomer::login, &NOCustomer::loginResponse ) );
}

/*==============================================================================================

MÉTODO SendDownloadRequest
	Envia uma requisição de download ao NanoOnline.

===============================================================================================*/

bool NOCustomer::SendDownloadRequest( NOCustomer* pCustomer, INOListener* pListener )
{
	NOConnection::GetInstance()->setNOListener( pListener );
	return NOConnection::GetInstance()->sendRequest( "/customers/import", MakeNORequestHolder( *pCustomer, &NOCustomer::download, &NOCustomer::downloadResponse ) );
}

/*==============================================================================================

MÉTODO Get * SupportedLen
	Retorna o tamnanho de string (em caracteres) suportado para um campo de texto.

===============================================================================================*/

void NOCustomer::GetNicknameSupportedLen( uint32& min, uint32& max )
{
	min = CUSTOMER_NICKNAME_MIN_LEN;
	max = CUSTOMER_NICKNAME_MAX_LEN;
}

void NOCustomer::GetFirstNameSupportedLen( uint32& min, uint32& max )
{
	min = CUSTOMER_FIRSTNAME_MIN_LEN;
	max = CUSTOMER_FIRSTNAME_MAX_LEN;
}

void NOCustomer::GetLastNameSupportedLen( uint32& min, uint32& max )
{
	min = CUSTOMER_LASTNAME_MIN_LEN;
	max = CUSTOMER_LASTNAME_MAX_LEN;
}

void NOCustomer::GetPasswordSupportedLen( uint32& min, uint32& max )
{
	min = CUSTOMER_PASSWORD_MIN_LEN;
	max = CUSTOMER_PASSWORD_MAX_LEN;
}

void NOCustomer::GetEmailSupportedLen( uint32& min, uint32& max )
{
	min = CUSTOMER_EMAIL_MIN_LEN;
	max = CUSTOMER_EMAIL_MAX_LEN;
}
