/*
 *  NORanking.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_RANKING_H
#define NANO_ONLINE_RANKING_H 1

// Components
#include "NOMap.h"
#include "NORankingEntry.h"
#include "NOString.h"

// C++
#include <limits>
#include <vector>

// Declarações Adiadas
class MemoryStream;

// Número padrão do máximo de entradas no ranking online
#define NANO_ONLINE_RANKING_DEFAULT_MAX_ONLINE_ENTRIES 10

class NORanking : public ISerializable
{
	public:
		typedef std::vector< NORankingEntry > NOEntriesContainer;
	
		// Construtor
		NORanking( const int64& maxPoints = std::numeric_limits< int64 >::max() );
	
		// Construtor de cópia
		NORanking( const NORanking& other );

		// Destrutor
		virtual ~NORanking( void );
	
		// Atribuição
		NORanking& operator=( const NORanking& other );
	
		// Indica se o ranking está na forma crescente ou decrescente
		bool isDecrescent( void ) const;

		// Retornam as entradas deste ranking
		NOEntriesContainer& getLocalEntries( void );
		const NOEntriesContainer& getOnlineEntries( void ) const;
	
		// Troca as entradas online de dois rankings
		void swapOnlineEntries( NORanking& other );
	
		// Limpa todas as entradas (locais e online) de um ranking
		void clear( void );
	
		// Retorna as 3 melhores pontuações existentes no NanoOnline para este ranking
		void getBestTrio( NOEntriesContainer& out ) const;
	
		// Determina a melhor pontuação que pode ser feita neste ranking
		void setMaxPoints( const int64& max );

		// Retorna a melhor pontuação que pode ser feita neste ranking
		int64& getMaxPoints( int64& out ) const;
	
		// Determina quantas entradas do Ranking Online queremos exibir neste ranking
		void setOnlineMaxEntries( int16 nEntries );
	
		// Retorna quantas entradas do Ranking Online iremos exibir neste ranking
		int16 getOnlineMaxEntries( void ) const;
	
		// Determina os perfis que ranking deve focar, ou seja, aqueles que terão
		// suas pontuações exibidas mesmo se estiverem fora dos "OnlineMaxEntries"
		// 1os do ranking
		void setHighlightedProfilesIds( const std::vector< NOProfileId >& profilesIds );
	
		// Retorna quais perfis o ranking está focando, ou seja, aqueles que terão
		// suas pontuações exibidas mesmo se estiverem fora dos "OnlineMaxEntries"
		// 1os do ranking
		std::vector< NOProfileId >& getHighlightedProfilesIds( void );
	
		// Ordena as entradas do ranking de acordo com a configuração do mesmo
		void sort( void );
	
		// Lê o objeto de uma stream
		virtual void serialize( MemoryStream& stream ) const;

		// Escre o objeto em uma stream 
		virtual void unserialize( MemoryStream& stream );
	
		// Requisições
		static bool SendSubmitRequest( NORanking* pRanking, INOListener* pListener );
		static bool SendHighScorestRequest( NORanking* pRanking, INOListener* pListener );
	
		#if DEBUG
			// Exibe o conteúdo do ranking no console
			void print( void );
		#endif

	private:
		// Pede as melhores pontuações do rankingOnline
		bool highScores( MemoryStream& stream );
	
		// Envia a pontuação do jogador para o NanoOnline
		bool submitScores( MemoryStream& stream );

		// Trata a resposta do envio da pontuação do jogador para o NanoOnline
		bool submitScoresResponse( NOMap& table, NOString& errorStr );
	
		// Lê as entradas do ranking enviadas pelo NanoOnline
		bool readSubmitResponseData( MemoryStream& dataStream, NOString& errorStr );

		// Melhor pontuação que pode ser atingida neste ranking
		int64 maxPoints;

		// Tipo do ranking
		int8 rnkType;
	
		// Indica se o ranking está na forma crescente ou decrescente
		bool decrescent;
	
		// Número de entradas do Ranking Online que iremos exibir neste ranking
		int16 onlineMaxEntries;
	
		// Perfis existentes neste device
		std::vector< NOProfileId > highlightedProfilesIds;
		
		// Pontuações dos rankings local e online
		NOEntriesContainer localEntries;
		NOEntriesContainer onlineEntries;
};

// Implementação dos métodos inline

inline bool NORanking::isDecrescent( void ) const
{
	return decrescent;
}

inline NORanking::NOEntriesContainer& NORanking::getLocalEntries( void )
{
	return localEntries;
}

inline const NORanking::NOEntriesContainer& NORanking::getOnlineEntries( void ) const
{
	return onlineEntries;
}

inline void NORanking::setMaxPoints( const int64& max )
{
	maxPoints = max;
}

inline int64& NORanking::getMaxPoints( int64& out ) const
{
	out = maxPoints;
	return out;
}

inline void NORanking::setOnlineMaxEntries( int16 nEntries )
{
	onlineMaxEntries = nEntries;
}

inline int16 NORanking::getOnlineMaxEntries( void ) const
{
	return onlineMaxEntries;
}

inline void NORanking::setHighlightedProfilesIds( const std::vector< NOProfileId >& profilesIds )
{
	highlightedProfilesIds.clear();
	std::copy( profilesIds.begin(), profilesIds.end(), highlightedProfilesIds.begin() );
}

inline std::vector< NOProfileId >& NORanking::getHighlightedProfilesIds( void )
{
	return highlightedProfilesIds;
}

inline void NORanking::swapOnlineEntries( NORanking& other )
{
	onlineEntries.swap( other.onlineEntries );
}

inline void NORanking::clear( void )
{
	localEntries.clear();
	onlineEntries.clear();
}

#endif
