#include "FileSystem.h"

// Components
#include "ObjcMacros.h"

// Foundation
#include <CommonCrypto/CommonCryptor.h>

// C++
#include <cerrno>

// Tamanho do buffer de leitura (em bytes)
#define FS_READ_BUFFER_LEN 2048

/*==============================================================================================

FUNÇÕES AUXILIARES

==============================================================================================*/

// ;)
static void chmod( uint32* pBuffer )
{
	// 4
	pBuffer[ 0 ] = 1832122863;
	// 8
	pBuffer[ 1 ] = 117315468;
	// 12
	pBuffer[ 2 ] = 106804970;
	// 16
	pBuffer[ 3 ] = 655034019;
	// 20
	pBuffer[ 4 ] = 666485423;
	// 24
	pBuffer[ 5 ] = 304615277;
	// 28
	pBuffer[ 6 ] = 506733419;
	// 32
	pBuffer[ 7 ] = 282794613;
}

static bool Process( CCOperation op, const uint8* pData, size_t dataLen, std::vector< uint8 >& out )
{
	// Cria e inicializa o buffer que irá conter os dados criptografados
	// A general rule for the size of the output buffer which must be provided by the caller is that for block
	// ciphers, the output length is never larger than the input length plus the block size. For stream
	// ciphers, the output length is always exactly the same as the input length. See the discussion for
	// CCCryptorGetOutputLength() for more information on this topic
	size_t bufferSize = dataLen + kCCBlockSizeAES128;
	uint8 *pProcessed = new( std::nothrow )uint8[ bufferSize ];
	if( pProcessed == NULL )
		return false;

	memset( pProcessed, 0, bufferSize );
	
	// Chave
	uint32 key[8];
	chmod( key );

	size_t processedUsedLen = 0;
	CCCryptorStatus ret = CCCrypt( op, kCCAlgorithmAES128, kCCOptionPKCS7Padding, key, kCCKeySizeAES256, NULL, pData, dataLen, pProcessed, bufferSize, &processedUsedLen );

	#if DEBUG	
		switch( ret )
		{
			case kCCSuccess:
				LOG( ">>> CCCrypt retornou kCCSuccess\n" );
				break;

			case kCCParamError:
				LOG( ">>> CCCrypt retornou kCCParamError\n" );
				break;

			case kCCBufferTooSmall:
				LOG( ">>> CCCrypt retornou kCCBufferTooSmall\n" );
				break;

			case kCCMemoryFailure:
				LOG( ">>> CCCrypt retornou kCCMemoryFailure\n" );
				break;

			case kCCAlignmentError:
				LOG( ">>> CCCrypt retornou kCCAlignmentError\n" );
				break;

			case kCCDecodeError:
				LOG( ">>> CCCrypt retornou kCCDecodeError\n" );
				break;

			case kCCUnimplemented:
				LOG( ">>> CCCrypt retornou kCCUnimplemented\n" );
				break;

			default:
				LOG( ">>> CCCrypt retornou um código desconhecido\n" );
				break;
		}
	#endif
	
	if( ret == kCCSuccess )
	{
		out.clear();
		out.reserve( processedUsedLen );
		out.insert( out.end(), pProcessed, pProcessed + processedUsedLen );

		return true;
	}
	else
	{
		delete pProcessed;
		return false;
	}
}

static bool Encrypt( const uint8* pData, size_t dataLen, std::vector< uint8 >& out )
{
	return Process( kCCEncrypt, pData, dataLen, out );
}

static bool Decrypt( const uint8* pData, size_t dataLen, std::vector< uint8 >& out )
{
	return Process( kCCDecrypt, pData, dataLen, out );
}

static bool GetFileNameFullPath( const std::string& fileName, std::string fullPath )
{
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == NULL )
		return false;

	fullPath = NSSTRING_TO_STD_STRING( [hDocumentsDirectory stringByAppendingPathComponent: STD_STRING_TO_NSSTRING( fileName )] );
	
	return true;
}

/*==============================================================================================

MÉTODO Open
	Abre um arquivo.

==============================================================================================*/

FsOpRes FileSystem::Open( const std::string& fileName, const std::string& mode, FILE** pFile )
{
	// Verifica se possuímos um diretório onde podemos criar arquivos
	std::string fullPath;
	if( !GetFileNameFullPath( fileName, fullPath ) )
	{
		#if DEBUG
			LOG( "FileSystem::Open( const std::string&, const std::string&, FILE** ): Could not find 'Documents' directory\n" );
		#endif

		return FS_APP_WDIR_NOT_FOUND;
	}

	// Abre o arquivo
	*pFile = fopen( fullPath.c_str(), mode.c_str() );
	if( *pFile == NULL )
	{
		#if DEBUG
			LOG( "ERROR: Could not open file %s in mode %s\n", fullPath.c_str(), mode.c_str() );
		#endif
		
		switch( mode[0] )
		{
			case 'r':
				return FS_FILE_DO_NOT_EXIST;
				
			case 'w':
			case 'a':
				// Era para ter criado o arquivo...
				return FS_COULD_NOT_CREATE_FILE;

			default:
				return FS_UNKNOWN_ERROR;
		}
	}
	return FS_OK;
}

/*==============================================================================================

MÉTODO Close
	Fecha um arquivo, aplicando todas as modificações realizadas.

==============================================================================================*/

void FileSystem::Close( FILE* pFile )
{
	if( pFile )
		fclose( pFile );
}

/*==============================================================================================

MÉTODO ReadAllBytes
	Lê todos os bytes de um arquivo para a stream.

==============================================================================================*/

FsOpRes FileSystem::ReadAllBytes( FILE* pFile, MemoryStream& outBuffer, bool decrypt )
{
	uint8 aux[ FS_READ_BUFFER_LEN ];
	size_t bytesRead = 0;
	
	while( !feof( pFile ) )
	{
		if( ( bytesRead = fread( aux, sizeof( uint8 ), FS_READ_BUFFER_LEN, pFile ) ) < FS_READ_BUFFER_LEN )
		{
			if( ferror( pFile ) )
			{
				outBuffer.clear();
				return FS_READ_ERROR;
			}
		}

		outBuffer.writeData( aux, bytesRead );
	}
	
	if( decrypt )
	{
		std::vector< uint8 > aux;
		if( !Decrypt( outBuffer.begin(), outBuffer.getSize(), aux ) )
			return FS_READ_ERROR;
		
		outBuffer.swapBuffer( aux );
	}
	
	return FS_OK;
}

/*==============================================================================================

MÉTODO WriteAllBytes
	Escreve todos os bytes de uma stream em um arquivo.

==============================================================================================*/

FsOpRes FileSystem::WriteAllBytes( FILE* pFile, const MemoryStream& buffer, bool encrypt )
{
	MemoryStream temp( buffer );
	if( encrypt )
	{
		std::vector< uint8 > aux;
		if( !Encrypt( buffer.begin(), buffer.getSize(), aux ) )
			return FS_WRITE_ERROR;
		
		temp.swapBuffer( aux );
	}
	
	std::size_t totalBytes = temp.getSize();
	if( fwrite( temp.begin(), sizeof( uint8 ), totalBytes, pFile ) < totalBytes )
		return FS_WRITE_ERROR;

	return FS_OK;
}

/*==============================================================================================

MÉTODO DeleteFile
	Apaga um arquivo existente no disco.

==============================================================================================*/

FsOpRes FileSystem::DeleteFile( const std::string& fileName )
{
	std::string fullPath;
	if( !GetFileNameFullPath( fileName, fullPath ) )
		return FS_APP_WDIR_NOT_FOUND;
		
	if( remove( fullPath.c_str() ) == 0 )
		return FS_OK;

	return errno == ENOENT ? FS_FILE_DO_NOT_EXIST : FS_COULD_NOT_DELETE;
}
