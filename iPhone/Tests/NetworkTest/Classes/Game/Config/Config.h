/*
 *  Config.h
 *  Blank
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H 1

#include "ApplicationErrors.h"

/*=====================================
 
CONFIGURAÇÕES DOS COMPONENTES
 
======================================*/

// Define o número máximo de toques tratados pela aplicação
#define MAX_TOUCHES 2

// OBS: Ainda não é utilizado
// Duração padrão da vibração, em segundos
#define VIBRATION_TIME_DEFAULT 0.2f

// Define se utilizaremos um depth buffer na aplicação
#define USE_DEPTH_BUFFER 1

// Define se iremos utilizar um buffer para fazer renderizações fora da tela
#define USE_OFFSCREEN_BUFFER 0

// Intervalo padrão do timer responsçvel pelas atualizações da aplicação
#define DEFAULT_ANIMATION_INTERVAL ( 1.0f / 32.0f ) // 32 fps

// Tempo máximo (em segundos) que consideramos como transcorrido entre uma atualização e outra 
#define MAX_UPDATE_INTERVAL 0.125f

/*=====================================
 
CÂMERA DO JOGO
 
======================================*/

// Configuração do zoom
#if MAX_TOUCHES > 1

	#define ZOOM_IN_FACTOR 1.1f
	#define ZOOM_OUT_FACTOR ( 1.0f / ZOOM_IN_FACTOR )

	// TODOO : Definir os valores permitidos pela aplicação
	#define ZOOM_MIN 1.0f
	#define ZOOM_MAX 1.0f

	#define ZOOM_MIN_DIST_TO_CHANGE 10.0f

#endif

/*=====================================
 
ACELERÔMETROS
 
======================================*/

// Define qual o modo de interpretação dos valores gerados pelos acelerômetros
#define DEFAULT_ACCELEROMETER_CALC_MODE ACC_FORCE_MODE_AVERAGE

// Intervalo com que os acelerômetros reportam seus valores à aplicação
#define ACCELEROMETER_N_UPDATES_PER_SEC 60.0f
#define ACCELEROMETER_UPDATE_INTERVAL ( 1.0f / ACCELEROMETER_N_UPDATES_PER_SEC )

// Quantos valores acumulamos para calcular a posição de repouso do device
#define ACCEL_REST_POS_CALC_N_VALUES 32

// Liberdade de movimento (em graus) dada ao usuário
#define ACCEL_MOVEMENT_FREEDOM_X 40.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_X ( ACCEL_MOVEMENT_FREEDOM_X * 0.5f )

#define ACCEL_MOVEMENT_FREEDOM_Y 30.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_Y ( ACCEL_MOVEMENT_FREEDOM_Y * 0.5f )

// Define o intervalo aceito para o peso dos valores antigos utilzados no cálculo da média da
// aceleração reportada pelos acelerômetros
#define MIN_OLD_VALUES_WEIGHT 0.0f
#define MAX_OLD_VALUES_WEIGHT 0.9f

/*=====================================
 
RESOURCES
 
=====================================*/

// TODOO: Número máximo de recordes salvos pela aplicação
#define APP_N_RECORDS 0

// TODOO: Número de fontes suportado pela aplicação
#define APP_N_FONTS 0

// TODOO: Índices das fontes no vetor de fontes da aplicação
#define APP_FONT_MAIN	0

// TODOO: Número de idiomas suportados pela aplicação
#define APP_N_LANGUAGES 1

// TODOO: Índices dos idiomas
#define LANGUAGE_INDEX_ENGLISH	0

// TODOO: Número de textos da aplicação
#define APP_N_TEXTS 0

// TODOO: Índices dos textos do jogo
//#define TEXT_INDEX_HELP			0

// TODOO: Número de sons da aplicação
#define APP_N_SOUNDS 0

// TODOO: Índices dos sons do jogo
//#define SOUND_INDEX_SPLASH		0
//#define SOUND_INDEX_GAME_OVER		1

// TODOO: "Nomes" dos sons do jogo
//#define SOUND_NAME_SPLASH			0
//#define SOUND_NAME_GAME_OVER		1

/*=====================================
 
VIEWS
 
======================================*/

// TODOO: Índices das views da aplicação
#define VIEW_INDEX_SPLASH_NANO		 0
#define VIEW_INDEX_SPLASH_GAME		 1
#define VIEW_INDEX_LOAD_MENU		 2
#define VIEW_INDEX_MAIN_MENU		 3
#define VIEW_INDEX_PLAY_MENU		 4
#define VIEW_INDEX_GAME				 5
#define VIEW_INDEX_RANKING			 6
#define VIEW_INDEX_HELP				 7
#define VIEW_INDEX_CREDITS			 8
#define VIEW_INDEX_NEW_GAME			 9
#define VIEW_INDEX_LOAD_GAME		10
#define VIEW_INDEX_PAUSE_SCREEN		11
#define VIEW_INDEX_OPTIONS			12
#define VIEW_INDEX_NANO_ONLINE		13

// Duração padrão das animações de transição de views
#define DEFAULT_TRANSITION_DURATION 0.75f

// Duração do splash da nano (em segundos)
#define SPLASH_NANO_DURATION 2.0f

/*=====================================
 
CÓDIGOS DE ERRO
 
======================================*/

// Erros que podem terminar a aplicação
#define ERROR_ALLOCATING_DATA	ERROR_USER
#define ERROR_ALLOCATING_VIEWS	ERROR_USER + 1
#define ERROR_CHANGING_COLORS	ERROR_USER + 2
#define ERROR_NO_FRAME_BUFFER	ERROR_USER + 3

/*=====================================
 
VERSÃO DA APLICAÇÃO
 
======================================*/

#define APP_VERSION "1.1"
#define APP_SHORT_NAME "BRZK"

#endif
