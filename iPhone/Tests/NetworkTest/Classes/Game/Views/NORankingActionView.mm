#include "NORankingActionView.h"

// Components
#include "NOViewsIndexes.h"

// Extensão da classe para declarar métodos privados
@interface NORankingActionView ( Private )

// Inicializa o objeto
-( bool )buildNORankingActionView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNORankingActionView;

@end

// Início da implementação da classe
@implementation NORankingActionView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	  {
		if( ![self buildNORankingActionView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNORankingActionView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNORankingActionView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNORankingActionView
{
	// Determina o título da tela
	[self setScreenTitle: @"Ranking"];
	return true;	
	
//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view que não serão criados pelo Interface Builder
//		// ...
//		
//		// Configura os elementos da view que não serão criados pelo Interface Builder
//		//..
//
//		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self cleanNOProfileActionView];
//		return false;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//
//	// Configura os elementos da view que foram criados pelo InterfaceBuilder
//	// ...
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	//[self cleanNORankingActionView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNORankingActionView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNORankingActionView
//{
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.

===============================================================================================*/

-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( hButton == hBtLocal )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_RANKING_LOCAL];
	}
	else if( hButton == hBtGlobal )
	{
		[[NOControllerView sharedInstance] performTransitionToView: NO_VIEW_INDEX_RANKING_GLOBAL];
	}
}

// Fim da implementação da classe
@end
