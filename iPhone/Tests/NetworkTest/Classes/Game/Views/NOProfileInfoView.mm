#include "NOProfileInfoView.h"

// Components
#include "ObjcMacros.h"
#include "UICustomSwitch.h"
#include "UILimitedTextField.h"

// Components - NanoOnline
#include "NOViewsIndexes.h"

// Definições dos botões do popup
#define POPUP_YES_BT_INDEX	0
#define POPUP_NO_BT_INDEX	1

#define POPUP_OK_BT_INDEX	2

// Estados da view
#define NOPROFVIEW_STATE_UNDEFINED			-1
#define NOPROFVIEW_CREATE_STATE_IDLE		 0
#define NOPROFVIEW_EDIT_STATE_IDLE			 1
#define NOPROFVIEW_EDIT_STATE_SYNCH			 2
#define NOPROFVIEW_EDIT_STATE_AFTER_SYNCH	 3

// Macros auxiliares
#define ON_ERROR( wstdString ) [[NOControllerView sharedInstance] showError: wstdString]

#define RETURN_IF_ERROR( wstdString )		\
		if( !error.empty() )				\
		{									\
			ON_ERROR( wstdString );			\
			return;							\
		}

// Extensão da classe para declarar métodos privados
@interface NOProfileInfoView ( Private )

// Inicializa o objeto
-( bool )buildNOProfileInfoView;

//// Libera a memória alocada pelo objeto
//-( void )cleanNOProfileInfoView;

@end

// Início da implementação da classe
@implementation NOProfileInfoView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize viewMode;

-( void )setViewMode:( NOProfileInfoViewMode )mode
{
	viewMode = mode;

	NSString* hBtTitle;
	switch( viewMode )
	{
		case PROFILE_INFO_VIEW_MODE_CREATING:
			hBtTitle = @"Create";
			break;
			
		case PROFILE_INFO_VIEW_MODE_EDITING:
			hBtTitle = @"Edit";
			break;
	}
	
	[self setScreenTitle: hBtTitle];

	[hBtOk setTitle: hBtTitle forState: UIControlStateNormal];
	[hBtOk setTitle: hBtTitle forState: UIControlStateHighlighted];
	[hBtOk setTitle: hBtTitle forState: UIControlStateDisabled];
	[hBtOk setTitle: hBtTitle forState: UIControlStateSelected];
}

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildNOProfileInfoView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildNOProfileInfoView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildNOProfileInfoView
	Inicializa o objeto.

==============================================================================================*/

-( bool )buildNOProfileInfoView
{
	// Inicializa as variáveis da classe
	hTbNickname = nil;
	hTbEmail = nil;
	hTbPassword = nil;
	hTbLastName = nil;
	hTbFirstName = nil;
	hSwGenre = nil;
	hImgAvatar = nil;
	hBtOk = nil;
	
	inputDay = 0;
	inputMonth = 0;
	inputYear = 0;

	currCustomer = NOCustomer();
	noListener.setCocoaListener( self );
	
	viewMode = PROFILE_INFO_VIEW_MODE_UNDEFINED;
	viewState = NOPROFVIEW_STATE_UNDEFINED;
	
	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Configura os elementos da view que foram criados pelo InterfaceBuilder
	
	// Determina os valores da switch
	// OBS: Os caracteres de espaço servem para centralizarmos as letras 'M' e 'F' na imagem da switch
	[hSwGenre setLeftLabelText: @" M" ];
	[hSwGenre setRightLabelText: @"   F" ];

	// Determina os limites dos campos de texto
	uint32 minTextFieldLen, maxTextFieldLen;
	NOCustomer::GetNicknameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbNickname setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];
	
	NOCustomer::GetFirstNameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbFirstName setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetLastNameSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbLastName setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetPasswordSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbPassword setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	NOCustomer::GetEmailSupportedLen( minTextFieldLen, maxTextFieldLen );
	[hTbEmail setMinLimit: minTextFieldLen AndMaxLimit: maxTextFieldLen];

	[hTbBirthdayDay setMinLimit: 2 AndMaxLimit: 2];
	[hTbBirthdayMonth setMinLimit: 2 AndMaxLimit: 2];
	[hTbBirthdayYear setMinLimit: 4 AndMaxLimit: 4];
}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//	[self cleanNOProfileInfoView];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM cleanNOProfileInfoView
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )cleanNOProfileInfoView
//{
//}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado assim que esta view passa a ser a principal view da aplicação.
 
===============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	if( viewMode == PROFILE_INFO_VIEW_MODE_EDITING )
	{
		viewState = NOPROFVIEW_EDIT_STATE_SYNCH;

		if( NOCustomer::SendDownloadRequest( &currCustomer, &noListener ) )
		{
			[[NOControllerView sharedInstance] showWaitViewWithText: @"Synchronizing with Server"];
		}
		else
		{
			// OLD
			//[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server. Changes made to your profile on other devices won't apply here. Continue anyway?" CancelBtIndex: POPUP_NO_BT_INDEX AndBts: @"Yes", @"No", nil];
			
			[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server." CancelBtIndex: POPUP_OK_BT_INDEX AndBts: @"Ok", nil];			
		}
	}
	else
	{
		viewState = NOPROFVIEW_CREATE_STATE_IDLE;
	}
}

/*==============================================================================================

MENSAGEM setProfileToEdit:
	Este método deve ser chamado antes de a view ser exibida. Ele prrenche os campos do formulário
com os dados do perfil passado como parâmetro.
 
===============================================================================================*/

-( void )setProfileToEdit:( const NOCustomer* )pProfileToEdit
{
	if( pProfileToEdit )
	{
		currCustomer = *pProfileToEdit;

		NSString *hAux;

		NOString aux;
		currCustomer.getNickname( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbNickname setText: hAux];
		
		currCustomer.getFirstName( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbFirstName setText: hAux];

		currCustomer.getLastName( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbLastName setText: hAux];

		currCustomer.getEmail( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbEmail setText: hAux];
		
		currCustomer.getPassword( aux );
		hAux = [NOControllerView ConvertSTDStringToNSString: aux];
		[hTbPasswordConfirm setText: [hTbPassword text]];
		
		if( currCustomer.getGenre() == GENRE_F )
			[hSwGenre setOn: NO];
		else
			[hSwGenre setOn: YES];
		
		[self onSwValueChanged];
		
		currCustomer.getBirthday( &inputDay, &inputMonth, &inputYear );
		
		if( ( inputDay != 0 ) && ( inputMonth != 0 ) && ( inputYear != 0 ) )
		{
			#define PROFILE_EDIT_BUF_SIZE 8

			char buffer[ PROFILE_EDIT_BUF_SIZE ];
			snprintf( buffer, PROFILE_EDIT_BUF_SIZE, "%02d", static_cast< int32 >( inputDay ) );
			[hTbBirthdayDay setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
			
			snprintf( buffer, PROFILE_EDIT_BUF_SIZE, "%02d", static_cast< int32 >( inputMonth ) );
			[hTbBirthdayMonth setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
			
			snprintf( buffer, PROFILE_EDIT_BUF_SIZE, "%4d", inputYear );
			[hTbBirthdayYear setText: CHAR_ARRAY_TO_NSSTRING( buffer )];
			
			[self onBirthdayChanged: hTbBirthdayYear];

			#undef PROFILE_EDIT_BUF_SIZE
		}
	}
}

/*==============================================================================================

MENSAGEM onBtPressed:
	Método chamado quando um botão é pressionado.
 
===============================================================================================*/
		
-( IBAction )onBtPressed:( UIButton* )hButton
{
	if( hButton == hBtOk )
	{
//		#if DEBUG
//		
//				NOString error;
//				currCustomer.setNickname( L"aマリオ", error );
//		
//				currCustomer.setFirstName( L"Super", error );
//				currCustomer.setLastName( L"Mario", error );
//				currCustomer.setPassword( L"mario123", error );
//				currCustomer.setEmail( L"mario@nanogames.com.br", error );
//				currCustomer.setGenre( GENRE_M );
//		#else
				NOString error, aux;
				
				[NOControllerView ConvertNSString: [hTbNickname text] toSTDString: aux];
				currCustomer.setNickname( aux, error );
				RETURN_IF_ERROR( error );

				[NOControllerView ConvertNSString: [hTbFirstName text] toSTDString: aux];
				currCustomer.setFirstName( aux, error );
				RETURN_IF_ERROR( error );

				[NOControllerView ConvertNSString: [hTbLastName text] toSTDString: aux];
				currCustomer.setLastName( aux, error );
				RETURN_IF_ERROR( error );
				
				[NOControllerView ConvertNSString: [hTbEmail text] toSTDString: aux];
				currCustomer.setEmail( aux, error );
				RETURN_IF_ERROR( error );
				
				[NOControllerView ConvertNSString: [hTbPassword text] toSTDString: aux];
				currCustomer.setPassword( aux, error );
				RETURN_IF_ERROR( error );
				
				if( [[hTbPassword text] compare: [hTbPasswordConfirm text]] != NSOrderedSame )
				{
					error = L"Password and Password Confirmation must be equal";
					RETURN_IF_ERROR( error );
				}
				
				if( ( inputDay != 0 ) && ( inputMonth != 0 ) && ( inputYear != 0 ) )
				{
					currCustomer.setBirthday( inputDay, inputMonth, inputYear, error );
					RETURN_IF_ERROR( error );
				}
				
				currCustomer.setGenre( [hSwGenre isOn] == YES ? GENRE_M : GENRE_F );
//		#endif

		bool ret = false;
		if( viewMode == PROFILE_INFO_VIEW_MODE_CREATING )
			ret = NOCustomer::SendCreateRequest( &currCustomer, &noListener );
		else if( viewMode == PROFILE_INFO_VIEW_MODE_EDITING )
			ret = NOCustomer::SendEditRequest( &currCustomer, &noListener );

		if( !ret )
		{
			ON_ERROR( L"Could not create request" );
			return;
		}
		
		// Tudo foi OK, então apaga quaisquer indicações de erros e mostra a mensagem de "Por favor aguarde"
		NOControllerView *hNOController = [NOControllerView sharedInstance];
		[hNOController hideError];
		[hNOController showWaitViewWithText: nil];
	}
}

/*==============================================================================================

MENSAGEM onSwValueChanged
	Método chamado quando o usuário altera o valor da switch de sexo.

===============================================================================================*/

-( IBAction )onSwValueChanged
{
	[hImgAvatar setImage: [NOControllerView getDefaultAvatarForGenre: ( [hSwGenre isOn] ? GENRE_M : GENRE_F ) ]];
}

/*==============================================================================================

MENSAGEM onBirthdayChanged
	Verifica se os números informados na data de nascimento são válidos.

===============================================================================================*/

-( IBAction )onBirthdayChanged:( UILimitedTextField* )hTxtField
{
	inputDay = static_cast< uint8 >( strtol( NSSTRING_TO_CHAR_ARRAY( [hTbBirthdayDay text] ), static_cast< char** >( NULL ), 10 ) );
	inputMonth = static_cast< uint8 >( strtol( NSSTRING_TO_CHAR_ARRAY( [hTbBirthdayMonth text] ), static_cast< char** >( NULL ), 10 ) );
	inputYear = static_cast< uint32 >( strtol( NSSTRING_TO_CHAR_ARRAY( [hTbBirthdayYear text] ), static_cast< char** >( NULL ), 10 ) );
	
	NOString error;
	if( !currCustomer.setBirthday( inputDay, inputMonth, inputYear, error ))
	{
		[hTbBirthdayDay setTextColor: [UIColor redColor]];
		[hTbBirthdayMonth setTextColor: [UIColor redColor]];
		[hTbBirthdayYear setTextColor: [UIColor redColor]];
		
		// Iremos informar o erro explicitamente apenas na hora da submissão
		//ON_ERROR( error );
	}
	else
	{
		[hTbBirthdayDay setTextColor: [UIColor blackColor]];
		[hTbBirthdayMonth setTextColor: [UIColor blackColor]];
		[hTbBirthdayYear setTextColor: [UIColor blackColor]];
	}
}

/*==============================================================================================

MENSAGEM onNOSuccessfulResponse
	Indica que uma requisição foi respondida e terminada com sucesso.

===============================================================================================*/

- ( void )onNOSuccessfulResponse
{
	[[NOControllerView sharedInstance] hideWaitView];

	switch( viewMode )
	{
		case PROFILE_INFO_VIEW_MODE_CREATING:
			// Salva o perfil localmente
			if( [NOControllerView insertProfile: currCustomer] == FS_OK )
			{
				// Atualiza o perfil que está sendo manipulado
				[[NOControllerView sharedInstance] setTempProfile: &currCustomer];
				
				// Mostra o popup de feedback
				[self showPopUpWithTitle: @"" Msg: @"Profile created" CancelBtIndex: POPUP_OK_BT_INDEX AndBts: @"Ok", nil];
			}
			else
			{
				// Opa! Não conseguiu salvar localmente... Melhor abortar...
				ON_ERROR( L"Could not save profile" );
			}
			break;
			
		case PROFILE_INFO_VIEW_MODE_EDITING:
			// Edita o perfil localmente
			if( [NOControllerView updateProfile: currCustomer] == FS_OK )
			{
				// Atualiza o perfil que está sendo manipulado
				[[NOControllerView sharedInstance] setTempProfile: &currCustomer];
				
				if( viewState == NOPROFVIEW_EDIT_STATE_IDLE )
				{
					[self showPopUpWithTitle: @"" Msg: @"Profile editted" CancelBtIndex: POPUP_OK_BT_INDEX AndBts: @"Ok", nil];
				}
				else
				{
					viewState = NOPROFVIEW_EDIT_STATE_AFTER_SYNCH;
					[self setProfileToEdit: &currCustomer];
					[self showPopUpWithTitle: @"" Msg: @"Profile synched" CancelBtIndex: POPUP_OK_BT_INDEX AndBts: @"Ok", nil];	
				}
			}
			else
			{
				if( viewState == NOPROFVIEW_EDIT_STATE_SYNCH )
				{
					// OLD
					//[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server. Changes made to your profile on other devices won't apply here. Continue anyway?" CancelBtIndex: POPUP_NO_BT_INDEX AndBts: @"Yes", @"No", nil];
					
					[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server." CancelBtIndex: POPUP_OK_BT_INDEX AndBts: @"Ok", nil];
				}
				else
				{
					// Opa! Não conseguiu editar localmente... Melhor abortar...
					ON_ERROR( L"Could not save profile" );
				}
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário pressiona um dos botões do popup.

================================================================================================*/

-( void )alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	NOControllerView *hNOController = [NOControllerView sharedInstance];
	switch( viewMode )
	{
		case PROFILE_INFO_VIEW_MODE_CREATING:
			[hNOController setHistoryAsShortestWayToView:NO_VIEW_INDEX_PROFILE_SELECT];
			[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_SELECT];
			break;

		case PROFILE_INFO_VIEW_MODE_EDITING:
			switch( viewState )
			{
				case NOPROFVIEW_EDIT_STATE_AFTER_SYNCH:
					viewState = NOPROFVIEW_EDIT_STATE_IDLE;
					break;

				case NOPROFVIEW_EDIT_STATE_SYNCH:
					// OLD
//					if( buttonIndex == POPUP_YES_BT_INDEX )
//					{
//						viewState = NOPROFVIEW_EDIT_STATE_IDLE;
//						return;
//					}
					
					// Sem break mesmo
					
				case NOPROFVIEW_EDIT_STATE_IDLE:
					[hNOController setHistoryAsShortestWayToView:NO_VIEW_INDEX_PROFILE_SELECT];
					[hNOController performTransitionToView: NO_VIEW_INDEX_PROFILE_SELECT];
					break;
			}
			break;
	}
}

/*==============================================================================================

MENSAGEM onNOError:WithErrorDesc:
	Sinaliza erros ocorridos nas operações do NanoOnline.

===============================================================================================*/

-( void ) onNOError:( NOErrors )errorCode WithErrorDesc:( NSString* )hErrorDesc
{
	[[NOControllerView sharedInstance] hideWaitView];

	if( ( viewMode == PROFILE_INFO_VIEW_MODE_EDITING ) && ( viewState == NOPROFVIEW_EDIT_STATE_SYNCH ) )
	{
		// OLD
		//[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server. Changes made to your profile on other devices won't apply here. Continue anyway?" CancelBtIndex: POPUP_NO_BT_INDEX AndBts: @"Yes", @"No", nil];
		
		[self showPopUpWithTitle: @"Alert" Msg: @"Sorry, couldn't synchronize with server." CancelBtIndex: POPUP_OK_BT_INDEX AndBts: @"Ok", nil];
	}
	else
	{
		NOString aux;
		[NOControllerView ConvertNSString: hErrorDesc toSTDString: aux];

		ON_ERROR( aux );
	}
}

/*==============================================================================================

MENSAGEM onNORequestCancelled
	Indica que a requisição foi cancelada pelo usuário.

===============================================================================================*/

-( void )onNORequestCancelled
{
	[[NOControllerView sharedInstance] hideWaitView];
	
	if( ( viewMode == PROFILE_INFO_VIEW_MODE_EDITING ) && ( viewState == NOPROFVIEW_EDIT_STATE_SYNCH ) )
	{
		// Sai da tela
		
		// OLD
		//[self alertView: nil clickedButtonAtIndex: POPUP_NO_BT_INDEX];
		
		[self alertView: nil clickedButtonAtIndex: POPUP_OK_BT_INDEX];
	}
}

// Fim da implementação da classe
@end

