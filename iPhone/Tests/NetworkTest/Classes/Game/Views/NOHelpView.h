/*
 *  NOHelpView.h
 *  NanoOnline
 *
 *  Created by Daniel Lopes Alves on 9/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_ONLINE_HELP_VIEW_H
#define NANO_ONLINE_HELP_VIEW_H

// Components
#include "NOBaseView.h"

@interface NOHelpView : NOBaseView
{
	@private
		IBOutlet UITextView *hTxtHelp;
}

@end

#endif
