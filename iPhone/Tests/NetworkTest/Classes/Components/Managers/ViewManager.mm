#include "ViewManager.h"

// Apple
#import <QuartzCore/QuartzCore.h>

// Components
#include "NanoTypes.h"

// Identificador da animação realizada pela transição de views
#define ANIMATION_KEY @"VMA"

// Extensão da classe para declarar métodos privados
//@interface ViewManager ( Private )
//
//// Inicializa o objeto
//-( bool )build;
//
//// Libera a memória alocada pelo objeto
//-( void )clean;
//
//@end

// Implementação da classe
@implementation ViewManager

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize transitioning, delegate;

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//-( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect:
	Renderiza o objeto.

==============================================================================================*/

//-( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//-( void )dealloc
//{
//	[self clean];
//	[super dealloc];
//}

/*==============================================================================================

MENSAGEM build
	Inicializa o objeto.

==============================================================================================*/

//-( bool )build
//{
//	// Inicializa as variáveis da classe
//	// ...
//	
//	{ // Evita erros de compilação por causa dos gotos
//
//		// Aloca os elementos da view que não serão criados pelo Interface Builder
//		// ...
//		
//		// Configura os elementos da view que não serão criados pelo Interface Builder
//		//..
//
//		return true;
//		
//	} // Evita erros de compilação por causa dos gotos
//
//	Error:
//		//[self clean];
//		return false;
//}

/*==============================================================================================

MENSAGEM clean
	Libera a memória alocada pelo objeto.

===============================================================================================*/

//-( void )clean
//{
//}

/*==============================================================================================

MENSAGEM transitionFromSubview
	Informa que ViewManager deve realizar uma nova transição de views.

==============================================================================================*/

- ( void )transitionFromSubview:( UIView* )oldView toSubview:( UIView* )newView keepOldView:( bool )retainOldView freeNewView:( bool )releaseNewView transition:( NSString* )transition direction:( NSString* )direction duration:( NSTimeInterval )duration
{	
	// Se já estamos realizando alguma transição, ignoramos esta chamada
	if( transitioning )
		return;
	
	// Obtém as subviews de ViewManager
	NSArray *subViews = [self subviews];
	NSUInteger index = 0;
	
	if( [oldView superview] == self)
	{
		// Obtém o índice de oldView para que possamos inserir newView em seu lugar
		for( index = 0; [subViews objectAtIndex:index] != oldView ; ++index );

		if( retainOldView )
			[oldView retain];

		[oldView removeFromSuperview];
	}

	// Se realmente estamos indo para uma nova view e esta ainda não possui uma superview...
	if( newView && ( [newView superview] == NULL ) )
	{
		// Insere-a no antigo índice de oldView
		[self insertSubview:newView atIndex:index];
		
		if( releaseNewView )
			[newView release];
	}
	
	// TODOO : Ver tb setAnimationTransition:forView:cache:. Ela possui os parâmetros:
	// - UIViewAnimationTransitionNone
	// - UIViewAnimationTransitionFlipFromLeft
	// - UIViewAnimationTransitionFlipFromRight
	// - UIViewAnimationTransitionCurlUp
	// - UIViewAnimationTransitionCurlDown

	// Set up the animation
	CATransition *animation = [CATransition animation];
	[animation setDelegate:self];
	
	// Set the type and if appropriate direction of the transition
	[animation setType:transition];
	if( transition != kCATransitionFade )
		[animation setSubtype:direction];
	
	// Set the duration and timing function of the transtion -- duration is passed in as a parameter, use ease in/ease out as the timing function
	[animation setDuration:duration];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[self layer] addAnimation:animation forKey: ANIMATION_KEY ];
}

/*==============================================================================================

MENSAGEM cancelTransition
	Cancela a transição corrente (caso haja).

==============================================================================================*/

- ( void )cancelTransition
{
	// Cancela a animação. O resto do tratamento será feito em animationDidStop:finished:
	if( transitioning )
		[[self layer] removeAnimationForKey: ANIMATION_KEY ];
}

/*==============================================================================================

MENSAGEM animationDidStart
	Método implementado para recebermos eventos de CAAnimation.

==============================================================================================*/

- ( void )animationDidStart:( CAAnimation* )animation
{	
	transitioning = YES;
    
	// Armazena o valor atual de userInteractionEnabled para que possamos restaurá-lo em animationDidStop:finished:
	interactionWasEnabled = self.userInteractionEnabled;
	
	// Se a interação do usuário já não está desabilitada, desabilita-a durante a transição
	if( interactionWasEnabled )
		self.userInteractionEnabled = NO;
    
	// Informamos ao listener que a transição foi iniciada (caso este implemente o método transitionDidStart)
	if( [delegate respondsToSelector:@selector( transitionDidStart: )] )
		[delegate transitionDidStart:self];
}

/*==============================================================================================

MENSAGEM animationDidStop
	Método implementado para recebermos eventos de CAAnimation.

==============================================================================================*/

- ( void )animationDidStop:( CAAnimation* )animation finished:( BOOL )finished
{	
	transitioning = NO;
	
	// Volta ao valor original de userInteractionEnabled
	if( interactionWasEnabled )
		self.userInteractionEnabled = YES;
    
	// Informamos ao listener que a transição terminou
	if( finished )
	{
		if( [ delegate respondsToSelector:@selector( transitionDidFinish: ) ] )
			[ delegate transitionDidFinish:self ];
	}
	else
	{
		if( [ delegate respondsToSelector:@selector( transitionDidCancel: ) ] )
			[ delegate transitionDidCancel:self ];
	}
}

// Fim da implementação da classe
@end
