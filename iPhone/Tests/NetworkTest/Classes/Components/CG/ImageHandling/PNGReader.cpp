#include "PNGReader.h"

// Definição de NULL
#include <stdio.h>

// memmove
#include <string.h>

// Bibliotecas utilizadas para a descompressão dos arquivos PNG
#include "zlib.h"
#include "png.h"

// Definições auxiliares
#define PNG_SIGNATURE_LEN 8

#define PNG_CHUNK_IDAT_SIGNATURE 1413563465
#define PNG_CHUNK_IEND_SIGNATURE 1145980233

/*==============================================================================================

CONSTRUTOR
	Descomprime o PNG e aloca um buffer com o tamanho necessário para conter todos os pixels
da imagem.

==============================================================================================*/

PNG::PNG( const char* pPNGData )
	: width( -1 ),
	  height( -1 ),
	  bitDepth( 0 ),
	  colorType( 0 ),
	  compressionMethod( 0 ),
	  filterMethod( 0 ),
	  interlaceMethod( 0 ),
	  bpp( 0 ),
	  pPixels( NULL )
{
	if( !readDataWithLibPNG( pPNGData ) )
		clear();
}

/*==============================================================================================

DESTRUTOR
 
==============================================================================================*/

PNG::~PNG( void )
{
	clear();
}

/*==============================================================================================

MÉTODO clear
	Libera a memória alocada pela imagem.

==============================================================================================*/

void PNG::clear( void )
{
	width = -1;
	height = -1;
	
	delete[] pPixels;
	pPixels = NULL;
}

/*==============================================================================================

MÉTODO readData
	Lê as informações do arquivo da imagem PNG.
 
	Um arquivo PNG é dividido em chunks. Cada chunk possui o seguinte formato:
 
	struct pngChunk
	{
		// Fornece o tamanho do campo data do próprio pngChunk. Ou seja, não inclui na contagem
		// os campos  
		uint32 length;

		// Tipo do chunk. Os tipos de chunk são restritos a letras ASCII maiúsculas e minúsculas
		// (A-Z e a-z, ou 65-90 e 97-122)
		uint8 chunkType[4];

		// Dados do chunk. Pode ter tamanho 0.
		uint8 data[ length ];

		// CRC (Cyclic Redundancy Check) calculado sobre os campos chunkType e data
		uint32 crc;
	}

	Para maiores informações:
	- http://www.libpng.org/pub/png/spec/1.2/PNG-Contents.html

==============================================================================================*/

bool PNG::readData( const uint8* pPNGData )
{
	return false;

	// TODOO : Terminar este código. Basta descomprimir a imagem linha à linha. Utilizar
	// o código da libpng como base
	
//	// Verifica a assinatura do arquivo
//	// Os 8 primeiros bytes devem conter os valores decimais 137 80 78 71 13 10 26 10
//	uint32 currByte;
//	uint8 signature[ PNG_SIGNATURE_LEN ] = { 137, 80, 78, 71, 13, 10, 26, 10 };
//	for( currByte = 0 ; currByte < PNG_SIGNATURE_LEN ; ++currByte )
//	{
//		if( pPNGData[ currByte ] != signature[ currByte ] )
//			return false;
//	}
//
//	// Lê as informações do chunk IHDR
//	currByte += 8;	// Pula os campos length e chunkType
//	
//	width = ntohl( *( ( int32* )&pPNGData[ currByte ] ) );
//	currByte += 4;
//
//	height = ntohl( *( ( int32* )&pPNGData[ currByte ] ) );
//	currByte += 4;
//	
//	bitDepth = pPNGData[ currByte++ ];
//	colorType = pPNGData[ currByte++ ];
//	compressionMethod = pPNGData[ currByte++ ];
//	filterMethod = pPNGData[ currByte++ ];
//	interlaceMethod = pPNGData[ currByte++ ];
//	
//	currByte += 4;	// Pula o campo CRC
//	
//	// Procura pelo chunk IDAT
//	uint32 currChunkDataLength = 0;
//	uint32 currChunkType = 0;
//	bool foundIDat = false;
//	while( !foundIDat )
//	{
//		currChunkDataLength = ntohl( *( ( int32* )&pPNGData[ currByte ] ) );
//		currByte += 4;
//		
//		currChunkType = *( ( int32* )&pPNGData[ currByte ] );
//		currByte += 4;
//
//		switch( currChunkType )
//		{
//			case PNG_CHUNK_IDAT_SIGNATURE:
//				foundIDat = true;
//				break;
//
//			case PNG_CHUNK_IEND_SIGNATURE:
//				// Lemos o chunk que representa EOF sem achar um chunk IDAT
//				return false;
//				
//			default:
//				// Pula os campos data e crc
//				currByte += currChunkDataLength + 4;
//				break;
//		}
//	}
//	
//	// Aloca um buffer para conter os pixels da imagem
//	// << 2 => R, G, B, A
//	uint32 pixelsLen = ( width * height ) << 2;
//	pPixels = new uint8[ pixelsLen ];
//	if( pPixels == NULL )
//		return false;
//	
//	uint8* pCompressedPixels = new uint8[ currChunkDataLength ];
//	if( !pCompressedPixels )
//		return false;
//
//	memmove( pCompressedPixels, pPNGData, currChunkDataLength );
//
//	// Configura a estrutura de descompressão
//	z_stream strm;
//
//	strm.zalloc = Z_NULL;
//	strm.zfree = Z_NULL;
//	strm.opaque = Z_NULL;
//	
//	strm.avail_in = currChunkDataLength;
//	strm.next_in = pCompressedPixels;
//
//	strm.avail_out = pixelsLen;
//	strm.next_out = pPixels;
//
//    int zLibRet = inflateInit( &strm );
//    if( zLibRet != Z_OK )
//        return false;
//
//    zLibRet = inflate( &strm, Z_FINISH );
//    if( zLibRet != Z_STREAM_END )
//	{
//        inflateEnd( &strm );
//
////        if( zLibRet == Z_NEED_DICT || ( zLibRet == Z_BUF_ERROR && stream.avail_in == 0 ) )
////            return Z_DATA_ERROR;
////        return zLibRet;
//		
//		return false;
//    }
//
//    zLibRet = inflateEnd( &strm );
//	
//	if( zLibRet != Z_OK )
//		return false;
//	
//	#if DEBUG
//		// Imprime os pixels lidos
//		for( uint32 i = 0 ; i < pixelsLen ; ++i )
//		{
//			uint8 r = ( pPixels[i] >> 24 ) & 0xff;
//			uint8 g = ( pPixels[i] >> 16 ) & 0xff;
//			uint8 b = ( pPixels[i] >>  8 ) & 0xff;
//			uint8 a = ( pPixels[i] >>  0 ) & 0xff;
//			
//			printf( "Pixel %d: r = %d, g = %d, b = %d, a = %d\n", i, r, g, b, a );
//		}
//	#endif
//
//	// TODOO : Testar!!!
////	if( uncompress( pPixels, &pixelsLen, pPNGData, currChunkDataLength ) != Z_OK )
////		return false;
//
//	return true;
}

/*==============================================================================================

MÉTODO readDataWithLibPNG
	Lê as informações do arquivo da imagem PNG utilizando a libpng.

==============================================================================================*/

bool PNG::readDataWithLibPNG( const char* pPNGPath )
{
	FILE *fp;
	if( ( fp = fopen( pPNGPath, "rb" )) == NULL )
		return false;

	/* Create and initialize the png_struct with the desired error handler
	* functions.  If you want to use the default stderr and longjump method,
	* you can supply NULL for the last three parameters.  We also supply the
	* the compiler header file version, so that we know if the application
	* was compiled with a compatible version of the library.  REQUIRED
	*/
	png_structp png_ptr = png_create_read_struct( PNG_LIBPNG_VER_STRING, png_voidp_NULL, png_error_ptr_NULL, png_error_ptr_NULL );
	if( png_ptr == NULL )
	{
		fclose( fp );
		return false;
	}

	/* Allocate/initialize the memory for image information.  REQUIRED. */
	png_infop info_ptr = png_create_info_struct( png_ptr );
	if( info_ptr == NULL )
	{
		fclose( fp );
		png_destroy_read_struct( &png_ptr, png_infopp_NULL, png_infopp_NULL );
		return false;
	}

	/* Set error handling if you are using the setjmp/longjmp method (this is
	* the normal method of doing things with libpng).  REQUIRED unless you
	* set up your own error handlers in the png_create_read_struct() earlier.
	*/
	if( setjmp( png_jmpbuf( png_ptr ) ) )
	{
		/* Free all of the memory associated with the png_ptr and info_ptr */
		png_destroy_read_struct( &png_ptr, &info_ptr, png_infopp_NULL );
		fclose( fp );

		/* If we get here, we had a problem reading the file */
		return false;
	}

	/* Set up the input control if you are using standard C streams */
	png_init_io( png_ptr, fp );

	/* If we have already read some of the signature */
	png_set_sig_bytes( png_ptr, 0 );

	/*
	* If you have enough memory to read in the entire image at once,
	* and you need to specify only transforms that can be controlled
	* with one of the PNG_TRANSFORM_* bits (this presently excludes
	* dithering, filling, setting background, and doing gamma
	* adjustment), then you can read the entire image (including
	* pixels) into the info structure with this call:
	*/
	//    PNG_TRANSFORM_IDENTITY      No transformation
	//    PNG_TRANSFORM_STRIP_16      Strip 16-bit samples to
	//                                8 bits
	//    PNG_TRANSFORM_STRIP_ALPHA   Discard the alpha channel
	//    PNG_TRANSFORM_PACKING       Expand 1, 2 and 4-bit
	//                                samples to bytes
	//    PNG_TRANSFORM_PACKSWAP      Change order of packed
	//                                pixels to LSB first
	//    PNG_TRANSFORM_EXPAND        Perform set_expand()
	//    PNG_TRANSFORM_INVERT_MONO   Invert monochrome images
	//    PNG_TRANSFORM_SHIFT         Normalize pixels to the
	//                                sBIT depth
	//    PNG_TRANSFORM_BGR           Flip RGB to BGR, RGBA
	//                                to BGRA
	//    PNG_TRANSFORM_SWAP_ALPHA    Flip RGBA to ARGB or GA
	//                                to AG
	//    PNG_TRANSFORM_INVERT_ALPHA  Change alpha from opacity
	//                                to transparency
	//    PNG_TRANSFORM_SWAP_ENDIAN   Byte-swap 16-bit samples

	png_read_png( png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, png_voidp_NULL );

	/* At this point you have read the entire image */
	uint32 nRowBytes = png_get_rowbytes( png_ptr, info_ptr );
	pPixels = new uint8[ info_ptr->height * nRowBytes ];
	if( !pPixels )
	{
		png_destroy_read_struct( &png_ptr, &info_ptr, png_infopp_NULL );
		fclose( fp );
		return false;
	}

	uint32 currPixel = 0;
	for( uint32 i = 0 ; i < info_ptr->height ; ++i )
	{
		memmove( &pPixels[ currPixel ], info_ptr->row_pointers[ i ], nRowBytes );
		currPixel += nRowBytes;
	}
	
	width = info_ptr->width;
	height = info_ptr->height;
	bitDepth = info_ptr->bit_depth;
	colorType = info_ptr->color_type;
	compressionMethod = info_ptr->compression_type;
	filterMethod = info_ptr->filter_type;
	interlaceMethod = info_ptr->interlace_type;
	bpp = info_ptr->pixel_depth;

	/* Clean up after the read, and free any memory allocated - REQUIRED */
	png_destroy_read_struct( &png_ptr, &info_ptr, png_infopp_NULL );

	/* Close the file */
	fclose( fp );
	
//	#if DEBUG
//		// Imprime os pixels lidos
//		uint8 inc = bpp / 8;
//		for( uint32 i = 0 ; i < ( width * height * inc ) ; i += inc )
//		{
//			uint8 r = pPixels[i + 0];
//			uint8 g = pPixels[i + 1];
//			uint8 b = pPixels[i + 2];
//			uint8 a = inc == 4 ? pPixels[i + 3] : 255;
//
//			printf( "Pixel %d: r = %d, g = %d, b = %d, a = %d\n", i, r, g, b, a );
//		}
//	#endif

	/* That's it */
	return true;
}

