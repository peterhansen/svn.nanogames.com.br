#include "Font.h"

// Components
#include "Exceptions.h"
#include "FontDescriptor.h"
#include "ObjcMacros.h"
#include "Quad.h"

// OpenGL
#include "GLHeaders.h"

/*===========================================================================================
 
CONSTRUTOR

============================================================================================*/

Font::Font( const char* pFontImgsId, const char* pFontDescriptor, bool monoSpaced )
	 : monoSpaced( monoSpaced ), spaceWidth( 0 ), fontHeight( 0 ), pFontImage( NULL ),
	   hCharacterSet( NULL )
{
	if( !build( pFontImgsId, pFontDescriptor ) )
#if DEBUG
		throw InvalidArgumentException( "Font::Font( const char*, const char* ): nao foi possivel carregar a fonte" );
#else
		throw InvalidArgumentException();
#endif
}
	
/*===========================================================================================
 
DESTRUTOR

============================================================================================*/

Font::~Font( void )
{
	clean();
}

/*===========================================================================================
 
MÉTODO getCharSize
	Retorna o tamanho do caractere na fonte.

============================================================================================*/

Point2i* Font::getCharSize( Point2i* pOut, unichar c ) const
{
	if( monoSpaced )
	{
		pOut->x = pFontImage->getOriginalFrameWidth();
		pOut->y = fontHeight;
	}
	else
	{
		if( c == ' ' )
		{
			pOut->x = spaceWidth;
			pOut->y = fontHeight;
		}
		else
		{
			int32 charFrameIndex = getCharFrameIndex( c );
			if( charFrameIndex >= 0 )
			{
				const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );

				pOut->x = pCharFrame->width;
				pOut->y = pCharFrame->height;
			}
			else
			{
				pOut->set();
			}
		}
	}
	return pOut;
}

/*===========================================================================================
 
MÉTODO getTextSize
	Retorna qual a área necessária para se renderizar o texto.

============================================================================================*/

Point2i* Font::getTextSize( Point2i* pOut, const char* pText, float spacement ) const
{
	Point2i aux;
	uint32 nChars = strlen( pText );
	for( uint32 i = 0 ; i < nChars ; ++i )
	{
		pOut->x += getCharSize( &aux, pText[i] )->x + spacement;
		
		// Poderíamos retornar sempre fontHeight, mas assim fica mais genérico
		if( aux.y > pOut->y )
			pOut->y = aux.y;
	}

	// O último espaçamento deve ser retirado
	pOut->x -= spacement;

	return pOut;
}

// OLD
//Point2i* Font::getTextSize( Point2i* pOut, const NSString* hText, float spacement ) const
//{
//	Point2i aux;
//	uint32 nChars = [hText length];
//	for( uint32 i = 0 ; i < nChars ; ++i )
//	{
//		pOut->x += getCharSize( &aux, [hText characterAtIndex:i] )->x + spacement;
//		
//		// Poderíamos retornar sempre fontHeight, mas assim fica mais genérico
//		if( aux.y > pOut->y )
//			pOut->y = aux.y;
//	}
//
//	// O último espaçamento deve ser retirado
//	pOut->x -= spacement;
//
//	return pOut;
//}

/*===========================================================================================
 
MÉTODO renderCharAt
	Renderiza o caractere na posição x, y.

============================================================================================*/

void Font::renderCharAt( unichar c, float x, float y ) const
{
	int32 charFrameIndex = getCharFrameIndex( c );
	if( charFrameIndex < 0 )
		return;

	const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );
	
	float texCoords[8];
	pFontImage->getFrameTexCoords( charFrameIndex, texCoords );
	
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	
	glTranslatef( x + ( pCharFrame->width * 0.5f ), y + ( pCharFrame->height * 0.5f ), 0.0f );
	glScalef( pCharFrame->width, pCharFrame->height, 1.0f );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glEnable( GL_TEXTURE_2D );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	pFontImage->load();

	Quad q;
	q.quadVertexes[0].setTexCoords( texCoords[0], texCoords[1] );
	q.quadVertexes[1].setTexCoords( texCoords[2], texCoords[3] );
	q.quadVertexes[2].setTexCoords( texCoords[4], texCoords[5] );
	q.quadVertexes[3].setTexCoords( texCoords[6], texCoords[7] );
	q.render();
	
	pFontImage->unload();
	glPopMatrix();
}

/*===========================================================================================
 
MÉTODO renderText
	Renderiza o texto com o espaçamento de caracteres desejado.

============================================================================================*/

void Font::renderText( const char* pText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const
{
	// Habilita o blending para utilizarmos a transparência
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	// Habilita a textura da fonte
	glEnable( GL_TEXTURE_2D );
	pFontImage->load();
	
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	// TODOO : Receber o scale do label
//	if( FDIF( scale.x, 1.0f ) || FDIF( scale.y, 1.0f ) )
//	{
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
//	}
//	else
//	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
//	}
	
	// Indica que estamos no sistema local
	glMatrixMode( GL_MODELVIEW );
	
	// Checa o alinhamento horizontal. O default é ALIGN_HOR_LEFT
	float x = pArea->x;
	if( ( alignment & ALIGN_HOR_RIGHT ) != 0 )
	{
		Point2i aux;
		x = pArea->x + pArea->width - getTextSize( &aux, pText, spacement )->x;
	}
	else if( ( alignment & ALIGN_HOR_CENTER ) != 0 )
	{
		Point2i aux;
		x = pArea->x + (( pArea->width - getTextSize( &aux, pText, spacement )->x ) * 0.5f );
	}
	// OBS: Não é necessária a implementação
//	else if( ( alignment & ALIGN_HOR_LEFT ) != 0 )
//	{
//	}
	
	// Checa o alinhamento vertical. O default é ALIGN_VER_TOP
	float y = pArea->y;
	if( ( alignment & ALIGN_VER_BOTTOM ) != 0 )
	{
		y = pArea->y + pArea->height - fontHeight;
	}
	else if( ( alignment & ALIGN_VER_CENTER ) != 0 )
	{
		y = pArea->y + (( pArea->height - fontHeight ) * 0.5f );
	}
	// OBS: Não é necessária a implementação
//	else if( ( alignment & ALIGN_VER_TOP ) != 0 )
//	{
//	}

	// Percorre a string
	float texCoords[8];
	uint32 nChars = strlen( pText );
	
	for( uint32 i = 0 ; i < nChars ; ++i )
	{
		// Verifica se possuímos o caractere da string na fonte
		unichar c = pText[i];
		if( c != ' ' )
		{
			int32 charFrameIndex = getCharFrameIndex( c );
			if( charFrameIndex >= 0  )
			{				
				// Obtém as coordenadas de texura desse caractere
				const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );
				pFontImage->getFrameTexCoords( charFrameIndex, texCoords );
				
				// Renderiza o caractere
				Quad q;
				q.quadVertexes[0].setTexCoords( texCoords[0], texCoords[1] );
				q.quadVertexes[1].setTexCoords( texCoords[2], texCoords[3] );
				q.quadVertexes[2].setTexCoords( texCoords[4], texCoords[5] );
				q.quadVertexes[3].setTexCoords( texCoords[6], texCoords[7] );

				glPushMatrix();
		
				// OBS : Isso REALMENTE funciona...
				//	An optimum compromise that allows all primitives to be specified at integer
				//positions, while still ensuring predictable rasterization, is to translate x
				//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
				// away from the centers of pixels, while moving line vertices close enough to
				// the pixel centers
				glTranslatef( x + pCharFrame->offsetX + ( pCharFrame->width * 0.5f ) + 0.375f, y + pCharFrame->offsetY + ( pCharFrame->height * 0.5f ) + 0.375f, 0.0f );
				glScalef( pCharFrame->width, pCharFrame->height, 1.0f );
				
				q.render();
				
				glPopMatrix();
				
				// Incrementa o contador da posição x para a renderização do próximo caractere
				if( monoSpaced )
					x += pFontImage->getOriginalFrameWidth() + spacement;
				else
					x += pCharFrame->width + spacement;
			}
		}
		// Se o tamanho do caractere espaço for 0, considera que esse caractere não existe na fonte
		else if( spaceWidth > 0 )
		{
			// Incrementa o contador da posição x para a renderização do próximo caractere
			x += spaceWidth + spacement;
		}
	}
	
	// Desabilita a textura da fonte
	pFontImage->unload();
}

// OLD
//void Font::renderText( const NSString* hText, float spacement, const NanoRect* pArea, TextAlignment alignment ) const
//{
//	// Habilita o blending para utilizarmos a transparência
//	glEnable( GL_BLEND );
//	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
//
//	// Habilita a textura da fonte
//	glEnable( GL_TEXTURE_2D );
//	pFontImage->load();
//	
//	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
//
//	// TODOO : Receber o scale do label
////	if( FDIF( scale.x, 1.0f ) || FDIF( scale.y, 1.0f ) )
////	{
////		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
////		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
////	}
////	else
////	{
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
//		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
////	}
//	
//	// Indica que estamos no sistema local
//	glMatrixMode( GL_MODELVIEW );
//	
//	// Checa o alinhamento horizontal. O default é ALIGN_HOR_LEFT
//	float x = pArea->x;
//	if( ( alignment & ALIGN_HOR_RIGHT ) != 0 )
//	{
//		Point2i aux;
//		x = pArea->x + pArea->width - getTextSize( &aux, hText, spacement )->x;
//	}
//	else if( ( alignment & ALIGN_HOR_CENTER ) != 0 )
//	{
//		Point2i aux;
//		x = pArea->x + (( pArea->width - getTextSize( &aux, hText, spacement )->x ) * 0.5f );
//	}
//	// OBS: Não é necessária a implementação
////	else if( ( alignment & ALIGN_HOR_LEFT ) != 0 )
////	{
////	}
//	
//	// Checa o alinhamento vertical. O default é ALIGN_VER_TOP
//	float y = pArea->y;
//	if( ( alignment & ALIGN_VER_BOTTOM ) != 0 )
//	{
//		y = pArea->y + pArea->height - fontHeight;
//	}
//	else if( ( alignment & ALIGN_VER_CENTER ) != 0 )
//	{
//		y = pArea->y + (( pArea->height - fontHeight ) * 0.5f );
//	}
//	// OBS: Não é necessária a implementação
////	else if( ( alignment & ALIGN_VER_TOP ) != 0 )
////	{
////	}
//
//	// Percorre a string
//	float texCoords[8];
//	uint32 nChars = [hText length];
//	
//	for( uint32 i = 0 ; i < nChars ; ++i )
//	{
//		// Verifica se possuímos o caractere da string na fonte
//		unichar c = [hText characterAtIndex:i];
//		if( c != ' ' )
//		{
//			int32 charFrameIndex = getCharFrameIndex( c );
//			if( charFrameIndex >= 0  )
//			{				
//				// Obtém as coordenadas de texura desse caractere
//				const TextureFrame* pCharFrame = pFontImage->getFrame( charFrameIndex );
//				pFontImage->getFrameTexCoords( charFrameIndex, texCoords );
//				
//				// Renderiza o caractere
//				Quad q;
//				q.quadVertexes[0].setTexCoords( texCoords[0], texCoords[1] );
//				q.quadVertexes[1].setTexCoords( texCoords[2], texCoords[3] );
//				q.quadVertexes[2].setTexCoords( texCoords[4], texCoords[5] );
//				q.quadVertexes[3].setTexCoords( texCoords[6], texCoords[7] );
//
//				glPushMatrix();
//		
//				// OBS : Isso REALMENTE funciona...
//				//	An optimum compromise that allows all primitives to be specified at integer
//				//positions, while still ensuring predictable rasterization, is to translate x
//				//and y by 0.375. Such a translation keeps polygon and pixel image edges safely
//				// away from the centers of pixels, while moving line vertices close enough to
//				// the pixel centers
//				glTranslatef( x + pCharFrame->offsetX + ( pCharFrame->width * 0.5f ) + 0.375f, y + pCharFrame->offsetY + ( pCharFrame->height * 0.5f ) + 0.375f, 0.0f );
//				glScalef( pCharFrame->width, pCharFrame->height, 1.0f );
//				
//				q.render();
//				
//				glPopMatrix();
//				
//				// Incrementa o contador da posição x para a renderização do próximo caractere
//				if( monoSpaced )
//					x += pFontImage->getOriginalFrameWidth() + spacement;
//				else
//					x += pCharFrame->width + spacement;
//			}
//		}
//		// Se o tamanho do caractere espaço for 0, considera que esse caractere não existe na fonte
//		else if( spaceWidth > 0 )
//		{
//			// Incrementa o contador da posição x para a renderização do próximo caractere
//			x += spaceWidth + spacement;
//		}
//	}
//	
//	// Desabilita a textura da fonte
//	pFontImage->unload();
//}

/*===========================================================================================
 
MÉTODO build
	Inicializa o objeto.

============================================================================================*/

bool Font::build( const char* pFontImgsId, const char* pFontDescriptor )
{
	FontDescriptor fontDesc;
	if( !fontDesc.readFromFile( pFontDescriptor ))
		goto Error;
	
	// Aloca a textura da imagem
	pFontImage = new Texture2D();
	if( !pFontImage || !pFontImage->loadTextureInFile( pFontImgsId, &fontDesc ) )
		goto Error;
	
	// TODOOO : Permitir várias texturas para a fonte
//	pFontImages = new Texture2D*[ nImages ];
//	if( !pFontImages )
//		return false;
//
//	for( uint16 i = 0 ; i < nImages ; ++i )
//	{
//		pFontImages[ i ] = new Texture2D();
//		if( !pFontImages[ i ] || !pFontImages[ i ]->loadTextureInFile( pFontImgsId ) )
//			goto Error;
//	}
	
	// Armazena outros atributos da fonte
	spaceWidth = fontDesc.getSpaceCharWidth();
	fontHeight = fontDesc.getFontHeight();
	hCharacterSet = fontDesc.getCharSet();
	[hCharacterSet retain];

	return true;
	
	// Label de tratamento de erros
	Error:
		clean();
		return false;
}

/*===========================================================================================
 
MÉTODO clean
	Libera os recursos alocados pelo objeto.

============================================================================================*/

void Font::clean( void )
{
	SAFE_DELETE( pFontImage )
	SAFE_RELEASE( hCharacterSet );
}

/*===========================================================================================
 
MÉTODO getCharFrameIndex
	Retorna o índice do frame correspondente ao caractere.

============================================================================================*/

int32 Font::getCharFrameIndex( unichar c ) const
{
	uint32 nChars = [hCharacterSet length];
	for( uint32 i = 0 ; i < nChars ; ++i )
	{
		if( [hCharacterSet characterAtIndex:i] == c )
			return i;
	}
	return -1;
}

