/*
 *  TexturePattern.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 6/26/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEXTURE_PATTERN_H
#define TEXTURE_PATTERN_H 1

#include "Object.h"
#include "Quad.h"
#include "ResourceManager.h"
#include "Texture2D.h"

// Forward Declarations
class TextureFrame;

class TexturePattern : public Object
{
	public:
		// Construtores
		TexturePattern( const char* pImg, const TextureFrame* pFrame = NULL, LoadTextureFunction pLoadTextureFunction = &( ResourceManager::GetTexture2D ) );
		TexturePattern( const TexturePattern* pOther );
	
		// Destrutor
		virtual ~TexturePattern( void ){};
	
		// Renderiza o objeto
		virtual bool render( void );

		// Determina um offset de desenho entre as repetições do pattern
		void setOffset( float x, float y );
	
		// Obtém o offset de desenho entre as repetições do pattern
		inline float getOffsetX( void ) const { return offsetX; };
		inline float getOffsetY( void ) const { return offsetY; };
	
	private:
		// Inicializa o objeto
		// Exceções : Constructor Exception
		void build( Texture2DHandler pImg, const TextureFrame* pFrame );
	
		// Offset de desenho entre as repetições do pattern
		float offsetX, offsetY;
	
		// Conjunto de vértices utilizado para desenharmos a textura
		Quad vertexes;
	
		// Textura contendo a imagem do objeto
		Texture2DHandler pTex;
};

#endif
