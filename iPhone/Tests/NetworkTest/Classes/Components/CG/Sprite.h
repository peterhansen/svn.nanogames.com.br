/*
 *  Sprite.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SPRITE_H
#define SPRITE_H 1

// Components
#include "AnimationDescriptor.h"
#include "MirrorOp.h"
#include "NanoTypes.h"
#include "Object.h"
#include "ResourceManager.h"
#include "Texture2D.h"
#include "VertexSet.h"

// OpenGL
#include "GLHeaders.h"

class SpriteListener;

/*==============================================================================================

CLASSE Sprite

==============================================================================================*/

class Sprite : public Object
{
	public:
		// Construtores
		// Exceções: ConstructorException
		// TODOOO Sprite( const char* pSpriteIdentifier );
		Sprite( const Sprite* pSprite );

		// Exceções: ConstructorException
		Sprite( const char* pImagesIdentifier, const char* pSpriteDescriptorName, LoadTextureFunction pLoadTextureFunction = &( ResourceManager::GetTexture2D ), CreateVertexSetFunction pVertexesCreator = &( Sprite::CreateDefaultVertexSet ) );
	
		// Destrutor
		virtual ~Sprite( void );
	
		// Determina um objeto que irá receber eventos do sprite
		void setListener( SpriteListener* pListener );
	
		// Renderiza o objeto
		virtual bool render( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );

		// Determina a sequência de animação a ser tocada
		void setAnimSequence( int16 sequenceIndex, bool looping = false, bool autoAnim = true );
	
		// Retorna a sequência de animação atual
		int16 getCurrAnimSequence( void ) const;
	
		// Retorna o número de sequências de animação que o sprite possui
		int16 getNAnimSequences( void ) const;
	
		// Determina qual a etapa da animação atual
		bool setCurrentAnimSequenceStep( int16 step );
	
		// Indica se a sequência de animação atual está em looping
		bool isCurrAnimSequenceLooping( void ) const;

		// Retorna qual a etapa da animação atual
		int16 getCurrAnimSequenceStep( void ) const;

		// Retorna o número total de passos da animação atual
		int16 getCurrAnimSequenceNSteps( void ) const;
	
		// Retorna o número total de passos de uma dada animação
		int16 getAnimSequenceNSteps( int16 sequenceIndex ) const;
	
		// Retorna a largura do frame da etapa de animação atual
		int32 getStepFrameWidth( void ) const;
	
		// Retorna a altura do frame da etapa de animação atual
		int32 getStepFrameHeight( void ) const;
	
		// Retorna as informações sobre o frame utilizado pela etapa 'step' da sequência de animação 'sequenceIndex'
		TextureFrame* getStepFrameInfo( TextureFrame* pInfo, int16 sequenceIndex, int16 step ) const;

		// Incrementa o contador de tempo da etapa de animação atual. Retorna:
		// -1 : Quando devemos retroceder uma etapa de animação
		//  0 : Quando não devemos fazer modificações
		//  1 : Quando devemos avançar uma etapa de animação
		int8 handAnimUpdate( float timeElapsed );
	
		// Avança uma etapa da animação. Retorna se a animação terminou
		bool handAnimNextStep( void );
	
		// Volta uma etapa da animação. Retorna se a animação terminou
		bool handAnimPrevStep( void );
	
		// Determina as operações de espelhamento que devem ser aplicadas sobre o sprite
		void mirror( MirrorOp ops );
	
		// Indica se uma determinada operação de espelhamento está aplicada sobre o sprite
		bool isMirrored( MirrorOp ops );
	
		// Cancela operações de espelhamento
		void unmirror( MirrorOp ops );
	
		// Determina a cor do conjunto de vértices a ser utilizada na renderização
		void setVertexSetColor( const Color& color );
	
		// Retorna a cor do conjunto de vértices utilizado na renderização
		Color* getVertexSetColor( Color* pColor ) const;
	
		#if DEBUG
			// Imprime as sequências de animação do sprite
			void print( void );
	
			// Renderiza bordas para indicar as áreas do sprite e de sua imagem
			void renderFrames( void );
		#endif
	
		// TODOO: Pegar o valor padrão de acordo com a forma de carregar a imagem utilizada do disco!!! Ou seja, criar uma nova informação em Texture2D
		// Determina os fatores de blending que determinam como as componentes R, G, B e A são calculadas.
		// Os valores aceitos para srcFactor são:
		// - GL_ZERO
		// - GL_ONE
		// - GL_DST_COLOR
		// - GL_ONE_MINUS_DST_COLOR
		// - GL_SRC_ALPHA
		// - GL_ONE_MINUS_SRC_ALPHA
		// - GL_DST_ALPHA
		// - GL_ONE_MINUS_DST_ALPHA
		// - GL_SRC_ALPHA_SATURATE
		// O valor padrão é GL_ONE.
		//
		// Os valores aceitos para dstFactor são:
		// - GL_ZERO
		// - GL_ONE
		// - GL_SRC_COLOR
		// - GL_ONE_MINUS_SRC_COLOR
		// - GL_SRC_ALPHA
		// - GL_ONE_MINUS_SRC_ALPHA
		// - GL_DST_ALPHA
		// - GL_ONE_MINUS_DST_ALPHA
		// O valor padrão é GL_ONE_MINUS_SRC_ALPHA.
		void setBlenFunc( GLenum srcFactor, GLenum dstFactor );

	private:
		// Cria o conjunto de vértices que será utilizado para renderizar os sprites
		// Exceções: InvalidArgumentException
		static VertexSetHandler CreateDefaultVertexSet( void );
	
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( const Texture2DHandler& image, const VertexSetHandler& vertexes );
		void build( const char* pImagesIdentifier, const char* pSpriteDescriptorName, const VertexSetHandler& vertexes, LoadTextureFunction pLoadTextureFunction );
	
		// Executa a animação do sprite normalmente (ver updateRewind)
		bool updateForward( float timeElapsed );
	
		// Faz a animação do sprite voltar no tempo
		bool updateRewind( float timeElapsed );
	
		// Libera os recursos alocados pelo objeto
		//void clean( void );
	
		// Carrega as imagens que contém os frames do sprite
		//bool loadFramesImgs( const char* pImagesIdentifier, const TextureFrameDescriptor* pDescriptor );

		// Determina o frame atual do sprite
		void setCurrentFrame( int16 frameIndex );
	
		// Preenche as coordenadas de textura dos vértices em relação ao frame atual
		void setFrameTexCoords( void );

		// Vértices do sprite
		VertexSetHandler pVertexSet;
	
		// Frame atual do sprite
		int16 currFrame;
	
		// Contador para controlar a duração de cada frame do sprite
		float frameTimeCounter;

		// Fatores que indicam como as componentes R, G, B e A são calculadas no blending. Ver glBlendFunc
		GLenum texBlendSrcFactor, texBlendDstFactor;

		// Modo de texturização. Ver glTexEnv
		GLenum texEnvMode;

		// Textura contendo as imagens dos frames
		Texture2DHandler pFramesImg;
	
		// Descritor das animações do sprite
		AnimationDescriptor animDesc;

		// Operações de espelhamento aplicadas ao sprite
		uint8 mirrorOps;
	
		// Controladores da sequência de animação atual
		int16 currSequence;
		int16 currSequenceStep;
		bool loopingSequence, autoAnimation;
	
		// Objeto que irá receber eventos do sprite
		SpriteListener* pListener;
	
		// Cor do conjunto de vértices a ser utilizada na renderização
		Color vertexSetColor;
};

/*==============================================================================================

CLASSE SpriteListener

==============================================================================================*/

class SpriteListener
{
	public:
		// Destrutor
		virtual ~SpriteListener( void ){};

		// Chamado quando o sprite muda a etapa de animação
		virtual void onAnimStepChanged( Sprite* pSprite ){};
	
		// Chamado quando o sprite termina uma sequência de animação
		virtual void onAnimEnded( Sprite* pSprite ){};
	
		// Chamado quando o sprite reinicia automaticamente uma sequência de animação
		virtual void onAnimLooped( Sprite* pSprite ){};
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

==============================================================================================*/

inline bool Sprite::isCurrAnimSequenceLooping( void ) const
{
	return loopingSequence;
}

inline int16 Sprite::getCurrAnimSequence( void ) const
{
	return currSequence;
}

inline int16 Sprite::getCurrAnimSequenceStep( void ) const
{
	return currSequenceStep;
}

inline int16 Sprite::getCurrAnimSequenceNSteps( void ) const
{
	return animDesc.getAnimSequence( currSequence )->getNSteps();
}

inline int16 Sprite::getNAnimSequences( void ) const
{
	return animDesc.getNAnimSequences();
}

inline int16 Sprite::getAnimSequenceNSteps( int16 sequenceIndex ) const
{
	return animDesc.getAnimSequence( sequenceIndex )->getNSteps();
}

inline int32 Sprite::getStepFrameWidth( void ) const
{
	return ( pFramesImg->getFrames() )[ currFrame ].width;
}

inline int32 Sprite::getStepFrameHeight( void ) const
{
	return ( pFramesImg->getFrames() )[ currFrame ].height;
}

inline TextureFrame* Sprite::getStepFrameInfo( TextureFrame* pInfo, int16 sequenceIndex, int16 step ) const
{
	*pInfo = ( pFramesImg->getFrames() )[ animDesc.getAnimSequence( sequenceIndex )->getStepFrameIndex( step ) ];
	return pInfo;
}

inline Color* Sprite::getVertexSetColor( Color* pColor ) const
{
	*pColor = vertexSetColor;
	return pColor;
}

inline void Sprite::setBlenFunc( GLenum srcFactor, GLenum dstFactor )
{
	texBlendSrcFactor = srcFactor;
	texBlendDstFactor = dstFactor;
}

#endif
