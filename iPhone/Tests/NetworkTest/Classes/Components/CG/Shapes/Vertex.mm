#include "Vertex.h"
#include "Exceptions.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Vertex::Vertex( void ) : posX( 0.0f ), posY( 0.0f ), posZ( 0.0f ),
						 s( 0.0f ), t( 0.0f ),
						 r( 0.0f ), g( 0.0f ), b( 0.0f ), a( 0.0f ),
						 normalX( 0.0f ), normalY( 0.0f ), normalZ( 0.0f )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Vertex::Vertex( const float* pValues ) : posX( 0.0f ), posY( 0.0f ), posZ( 0.0f ),
										 s( 0.0f ), t( 0.0f ),
										 r( 0.0f ), g( 0.0f ), b( 0.0f ), a( 0.0f ),
										 normalX( 0.0f ), normalY( 0.0f ), normalZ( 0.0f )
{
	#if DEBUG
		if( pValues == NULL )
			throw InvalidArgumentException( "Vertex::Vertex: parameter pValues is NULL" );
	#endif
	set( pValues );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Vertex::Vertex( const Point3f* pPosition, float s, float t, const Color* pColor, const Point3f* pNormal )
	   : posX( pPosition->x ), posY( pPosition->y ), posZ( pPosition->z ),
		 s( s ), t( t ),
		 r( pColor->r ), g( pColor->g ), b( pColor->b ), a( pColor->a ),
		 normalX( pNormal->x ), normalY( pNormal->y ), normalZ( pNormal->z )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Vertex::Vertex( float posX, float posY, float posZ, float s, float t, float r, float g, float b, float a, float normalX, float normalY, float normalZ )
	   : posX( posX ), posY( posY ), posZ( posZ ),
		 s( s ), t( t ),
		 r( r ), g( g ), b( b ), a( a ),
		 normalX( normalX ), normalY( normalY ), normalZ( normalZ )
{
}

/*==============================================================================================

MÉTODO set
	Determina os atributos do vértice.

==============================================================================================*/

void Vertex::set( const float* pValues )
{
	if( pValues )
		memcmp( this, pValues, sizeof( float ) * VERTEX_N_COMPONENTS );
}

/*==============================================================================================

MÉTODO set
	Determina os atributos do vértice.

==============================================================================================*/

void Vertex::set( float posX, float posY, float posZ, float s, float t, float r, float g, float b, float a, float normalX, float normalY, float normalZ )
{
	this->posX = posX;
	this->posY = posY;
	this->posZ = posZ;
	this->s = s;
	this->t = t;
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
	this->normalX = normalX;
	this->normalY = normalY;
	this->normalZ = normalZ;
}

