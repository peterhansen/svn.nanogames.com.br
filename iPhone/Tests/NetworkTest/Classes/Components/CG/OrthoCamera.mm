#include "OrthoCamera.h"

// Components
#include "ObjcMacros.h"

// OpenGL
#include "GLHeaders.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

OrthoCamera::OrthoCamera( CameraType cameraType, float zNear, float zFar )
			: Camera( cameraType ),
			  landscapeModeOn( false ),
			  zNear( zNear ),
			  zFar( zFar )
{
}

/*==============================================================================================

MÉTODO place
	Posiciona a câmera na cena.

==============================================================================================*/

void OrthoCamera::place( void )
{
	bool depthTestIsEnabled = glIsEnabled( GL_DEPTH_TEST );
	glDisable( GL_DEPTH_TEST );
	
	if( landscapeModeOn )
		glViewport( 0, 0, SCREEN_HEIGHT, SCREEN_WIDTH ); 
	else
		glViewport( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT ); 
	
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	
	// Matrizes openGL são column-major	
	Matrix4x4 projection;
	glMultMatrixf( getProjectionMatrix( &projection )->transpose() );
	
	Camera::place();
	
	if( depthTestIsEnabled )
		glEnable( GL_DEPTH_TEST );
}

/*==============================================================================================

MÉTODO getViewVolume
	Retorna o volume de visualização da câmera.

==============================================================================================*/

Box* OrthoCamera::getViewVolume( Box* pViewVolume )
{
	// TODOO : Deve considerar o zoom ???
//	float invZoom = 1.0f / zoomFactor;
//	//pViewVolume->set( ( pos.x - HALF_SCREEN_WIDTH ) * invZoom, ( pos.y - HALF_SCREEN_HEIGHT )  * invZoom, 0.0f, SCREEN_WIDTH  * invZoom, SCREEN_HEIGHT * invZoom, 0.0f );
//	pViewVolume->set( pos.x - ( HALF_SCREEN_WIDTH * invZoom ), pos.y - ( HALF_SCREEN_HEIGHT * invZoom ), 0.0f, SCREEN_WIDTH  * invZoom, SCREEN_HEIGHT * invZoom, 0.0f );
//	return pViewVolume;

	pViewVolume->set( pos.x - HALF_SCREEN_WIDTH, pos.y - HALF_SCREEN_HEIGHT, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0f );
	return pViewVolume;
}

/*==============================================================================================

MÉTODO getProjectionMatrix
	Obtém a matriz de projeção da câmera.

==============================================================================================*/

Matrix4x4* OrthoCamera::getProjectionMatrix( Matrix4x4* pOut ) const
{
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	
	if( landscapeModeOn )
		glRotatef( -90.0f, 0.0f, 0.0f, 1.0f );

	float invZoom = 1.0f / zoomFactor;
	glOrthof( 0.0f, SCREEN_WIDTH * invZoom, 0.0f, SCREEN_HEIGHT * invZoom, zNear, zFar );
	
	// Faz com que o eixo Y cresça para baixo
	glScalef( 1.0f, -1.0f, 1.0f );
	glTranslatef( 0.0f, -SCREEN_HEIGHT * invZoom, 0.0f );
	
	glGetFloatv( GL_PROJECTION_MATRIX, *pOut );
	
	glPopMatrix();
	
	// Matrizes openGL são column-major
	pOut->transpose();
	
	return pOut;
}
