#include "Color.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( void ) : r( 0.0f ), g( 0.0f ), b( 0.0f ), a( 0.0f )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( float red, float green, float blue, float alpha ) : r( red ), g( green ), b( blue ), a( alpha )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( uint8 red, uint8 green, uint8 blue, uint8 alpha ) : r( red / 255.0f ), g( green / 255.0f ), b( blue / 255.0f ), a( alpha / 255.0f )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( PreDefinedColors color ) : r( 0.0f ), g( 0.0f ), b( 0.0f ), a( 0.0f )
{
	set( color );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( uint32 color ) : r( ( color >> 24 ) / 255.0f ), g( (( color >> 16 ) & 0xFF ) / 255.0f ), b( ( ( color >> 8 ) & 0xFF ) / 255.0f ), a( ( color & 0xFF ) / 255.0f )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Color::set( PreDefinedColors color )
{
	switch( color )
	{
		case COLOR_GREY:
			set4ub( 127, 127, 127, 255 );
			break;
			
		case COLOR_WHITE:
			set4ub( 255, 255, 255, 255 );
			break;
			
		case COLOR_RED:
			set4ub( 255, 0, 0, 255 );
			break;
			
		case COLOR_GREEN:
			set4ub( 0, 255, 0, 255 );
			break;
			
		case COLOR_BLUE:
			set4ub( 0, 0, 255, 255 );
			break;
			
		case COLOR_YELLOW:
			set4ub( 255, 255, 0, 255 );
			break;
			
		case COLOR_MAGENTA:
			set4ub( 255, 0, 255, 255 );
			break;
			
		case COLOR_CYAN:
			set4ub( 0, 255, 255, 255 );
			break;
			
		case COLOR_PURPLE:
			set4ub( 127, 0, 127, 255 );
			break;
			
		case COLOR_BROWN:
			set4ub( 153, 102, 51, 255 );
			break;
			
		case COLOR_ORANGE:
			set4ub( 255, 127, 0, 255 );
			break;
			
		case COLOR_TRANSPARENT:
			set4ub( 0, 0, 0, 0 );
			break;
			
		case COLOR_BLACK:
		default:
			set4ub();
			break;
	}
}

