#include "LoadingView.h"

#include "Utils.h"
#include "MathFuncs.h"
#include "ObjcMacros.h"

// Duração da animação do logo da nano
#define LOADING_VIEW_NANO_LOGO_ANIM_DUR 0.35f

// Tempo que esperamos entre as execuções da animação do logo da Nano Games
#define LOADING_VIEW_PAUSE_BETWEEN_LOGO_ANIMS 2.0f

// Intervalo entre as atualizações de tela
#define LOADING_VIEW_REFRESH_INTERVAL ( 1.0f / 32.0f )

// Extensão da classe para declarar métodos privados
@interface LoadingView ( Private )

// Inicializa o objeto
- ( bool )buildLoadingView;

// Atualiza a tela
- ( void )updateView;

@end

// Início da implementação da classe
@implementation LoadingView

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

//@synthesize /* ... */;

/*==============================================================================================

MENSAGEM initWithFrame: AndSelector:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

- ( id )initWithFrame:( CGRect )frame
{
    if( ( self = [super initWithFrame:frame] ) )
	{
		if( ![self buildLoadingView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

- ( id )initWithCoder:( NSCoder* )decoder
{
	if( ( self = [super initWithCoder:decoder] ) )
	{
		if( ![self buildLoadingView] )
			goto Error;
    }
    return self;
	
	// Tratamento de erros durante a inicialização
	Error:
		[self release];
		return NULL;
}

/*==============================================================================================

MENSAGEM buildLoadingView
	Inicializa a view.

==============================================================================================*/

- ( bool )buildLoadingView
{
	hThread = [[LoadingThread alloc] init];
	if( !hThread )
		return false;
	
	loadingObj = NULL;
	loadingFunc = NULL;
	onLoadEndedFunc = NULL;
	showed100Feedback = false;

	return true;
}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	char imgsType[] = "png";
	char buffer[ PATH_MAX ];
	[hNanoGamesLogo setAnimationImages: [NSArray arrayWithObjects:	[hNanoGamesLogo image],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl01", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl02", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl03", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl04", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl05", imgsType ))],
																    [UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl06", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl07", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl08", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl09", imgsType ))],
																	[UIImage imageWithContentsOfFile: CHAR_ARRAY_TO_NSSTRING( Utils::GetPathForResource( buffer, PATH_MAX, "nl10", imgsType ))],
																	nil]];

	[hNanoGamesLogo setAnimationDuration: LOADING_VIEW_NANO_LOGO_ANIM_DUR];
	[hNanoGamesLogo setAnimationRepeatCount: 1];
	
	[self setProgress: 0.0f];
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

- ( void )dealloc
{
	if( [hThread isExecuting] )
		[hThread cancel];
	
	while( ![hThread isFinished] );

	[hThread release];
	hThread = NULL;
	
	[self suspend];

    [super dealloc];
}

/*==============================================================================================

MENSAGEM setLoadingTarget: AndSelector:
	Determina um ponteiro para a função de loading.

==============================================================================================*/

-( void )setLoadingTarget:( id )target AndSelector:( SEL )selector CallingAfterLoad:( SEL )onLoadEndedSelector
{
	loadingObj = target;
	loadingFunc = selector;
	onLoadEndedFunc = onLoadEndedSelector;
}

/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

- ( void ) onBecomeCurrentScreen
{
	if( ( loadingObj != NULL ) && ( loadingFunc != NULL ) && ( onLoadEndedFunc != NULL ) )
	{
		loadingPercent = 0.0f;
		threadErrorCode = ERROR_NONE;
		showed100Feedback = false;

		// Inicia a animação do logo da nano
		[hNanoGamesLogo startAnimating];
		[self resume];

		// Inicia o carregamento
		[hThread loadWithObj:loadingObj Method:loadingFunc AndParam: self];
		[hThread start];
	}
}

/*==============================================================================================

MENSAGEM resume
	Pára o loop de renderização.

==============================================================================================*/

- ( void ) resume
{
	[self suspend];
	hLogoAnimController = [NSTimer scheduledTimerWithTimeInterval: LOADING_VIEW_PAUSE_BETWEEN_LOGO_ANIMS + LOADING_VIEW_NANO_LOGO_ANIM_DUR target:self selector:@selector( restartLogoAnim ) userInfo:NULL repeats:YES];
	hInterfaceUpdater = [NSTimer scheduledTimerWithTimeInterval: LOADING_VIEW_REFRESH_INTERVAL target:self selector:@selector( updateView ) userInfo:NULL repeats:YES];
}

/*==============================================================================================

MENSAGEM suspend
	Pára o loop de renderização.

==============================================================================================*/

- ( void ) suspend
{
	if( hLogoAnimController )
	{
		[hLogoAnimController invalidate];
		
		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hLogoAnimController invalidate] chama
		// [hAnimationTimer release] automaticamente
		hLogoAnimController = NULL;
	}
	
	if( hInterfaceUpdater )
	{
		[hInterfaceUpdater invalidate];

		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hInterfaceUpdater invalidate] chama
		// [hAnimationTimer release] automaticamente
		hInterfaceUpdater = NULL;
	}
}

/*==============================================================================================

MENSAGEM setProgress:
	Determina a porcentagem atual da barra de progresso, atualizando sua imagem e o label de
porcentagem do loading.

==============================================================================================*/

- ( void ) setProgress:( float )percent
{
	loadingPercent = NanoMath::clamp( percent, 0.0f, 1.0f );

//	#if IS_CURR_TEST( TEST_LOADING_PROGRESS )
//		// Deixa a thread gráfica atualizar
//		[NSThread sleepForTimeInterval: 2.00f];
//	#endif
}

/*==============================================================================================

MENSAGEM changeProgress:
	Modifica a porcentagem atual da barra de progresso em 'percent', atualizando sua imagem e
o label de porcentagem do loading.

==============================================================================================*/

- ( void ) changeProgress:( float )percent
{
	[self setProgress: loadingPercent + percent];
}

/*==============================================================================================

MENSAGEM setError:
	Indica que houve algum erro de processamento na thread.

==============================================================================================*/

-( void ) setError:( int16 )errorCode
{
	threadErrorCode = errorCode;
}

/*==============================================================================================

MENSAGEM getError
	Retorna se houve algum erro de processamento na thread.

==============================================================================================*/

-( int16 ) getError
{
	return threadErrorCode;
}

/*==============================================================================================

MENSAGEM restartLogoAnim:
	Reinicia a animação do logo da Nano Games.

==============================================================================================*/

-( void )restartLogoAnim
{
	[hNanoGamesLogo startAnimating];
}

/*==============================================================================================

MENSAGEM updateView
	Atualiza a tela.

==============================================================================================*/

-( void )updateView
{
	if( !showed100Feedback )
	{
		[hProgressLabel setText: [NSString stringWithFormat: @"%d%%", static_cast< int32 >( loadingPercent * 100.0f )]];
		[hProgressBar setProgress: loadingPercent];
		
		if( [hThread isFinished] )
		{
			[hProgressBar setProgress: 1.0f];
			showed100Feedback = true;
		}
	}
	else
	{		
		[self suspend];
		[loadingObj performSelector: onLoadEndedFunc withObject: self];
	}
}

// Fim da implementação da classe
@end
