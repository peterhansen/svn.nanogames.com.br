/*
 *  UIImgProgressBar.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 9/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef UI_IMG_PROGRESS_BAR_H
#define UI_IMG_PROGRESS_BAR_H

// Apple Foundation
#include <UIKit/UIKit.h>

// C++
#include <string>

// Components
#include "NanoTypes.h"

@interface UIImgProgressBar : UIView
{
	@private
		// Imagem utilizada como fundo da barra de progresso
		UIImageView *hImgBkg;
	
		// Imagem utilizada como preenchimento da barra de progresso
		UIImageView *hImgFill;
	
		// Progresso atual da barra de progresso (fica sempre no intervalo [0.0f, 1.0f])
		float currProgress;
}

// Construtor
-( id )initWithBkgImg:( const std::string& )bkgImgPath FillImg:( const std::string& )fillImgPath LeftCapWidth:( uint16 )leftCapWidth TopCapHeight:( uint16 )topCapHeight;

// Determina o progresso que a barra deve indicar. Coloca todos os valores no intervalo [0.0f, 1.0f]
-( void )setProgress:( float )progress;

@end

#endif
