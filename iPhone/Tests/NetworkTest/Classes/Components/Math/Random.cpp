#include "Random.h"

#include "MathFuncs.h"
#include "SFMT.h"

#include <cstdlib>	// versão inteira de abs()
#include <cmath>	// versão double de abs()
#include <time.h>

// Inicializa a variável estática da classe
bool Random::seeded = false;
bool Random::lastWas64BitVersion = false;

/*===========================================================================================

MÉTODO SetSeed
	Determina o inicializador do gerador de números randômicos.

============================================================================================*/

void Random::SetSeed( int32 seed )
{
	init_gen_rand( seed );
	seeded = true;
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro.

============================================================================================*/

int32 Random::GetInt( void )
{
	if( lastWas64BitVersion )
	{
		lastWas64BitVersion = false;
		seeded = false;
	}
	
	if( !seeded )
		SetSeed( GetDefaultSeed() );
	
	return gen_rand32();
}

int64& Random::GetInt64( int64& out )
{
	if( !lastWas64BitVersion )
	{
		lastWas64BitVersion = true;
		seeded = false;
	}

	if( !seeded )
		SetSeed( GetDefaultSeed() );
	
	out = gen_rand64();
	return out;
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro no intervalo [0, maxValue].

============================================================================================*/

int32 Random::GetInt( int32 maxValue )
{
	return GetInt( 0, maxValue );
}

int64& Random::GetInt64( int64& out, const int64& maxValue )
{
	return GetInt64( out, 0LL, maxValue );
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro no intervalo desejado.

============================================================================================*/

int32 Random::GetInt( int32 minValue, int32 maxValue )
{
	return ( abs( GetInt() ) % ( maxValue - minValue + 1 ) ) + minValue;
}

int64& Random::GetInt64( int64& out, const int64& minValue, const int64& maxValue )
{
	GetInt64( out );

	out = ( static_cast< int64 >( std::abs( out ) ) % ( maxValue - minValue + 1 ) ) + minValue;
	return out;
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo [0.0f, 1.0f].

============================================================================================*/

float Random::GetFloat( void )
{
	return static_cast< float >( to_real1( GetInt() ) );
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo [0.0f, maxValue]
	Equivale a chamar NextDouble( 0.0f, maxValue )

============================================================================================*/

float Random::GetFloat( float maxValue )
{
	return GetFloat( 0.0f, maxValue );
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo definido.

============================================================================================*/

float Random::GetFloat( float minValue, float maxValue )
{
	if( lastWas64BitVersion )
	{
		lastWas64BitVersion = false;
		seeded = false;
	}
	
	if( !seeded )
		SetSeed( GetDefaultSeed() );

	// [ 0.0f, 1.0f ]
	const float v = static_cast< float >( genrand_real1() );
	return NanoMath::lerp( v, minValue, maxValue );
}

/*===========================================================================================

MÉTODO GetDefaultSeed
	Retorna um número para ser utilizado na inicialização da máquina de geração de números
aleatórios.

============================================================================================*/

int32 Random::GetDefaultSeed( void )
{
	time_t timeInfo = time( NULL );

	if( timeInfo != static_cast< time_t >( -1 ) )
		return static_cast< int32 >( timeInfo );

	// Retorna qualquer coisa
	return 81728193;
}
