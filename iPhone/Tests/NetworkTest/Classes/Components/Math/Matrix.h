/*
 *  Matrix.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/27/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MATRIX_H
#define MATRIX_H 1

#include "Exceptions.h"
#include "NanoTypes.h"

// TASK : Transformar em uma classe template para podermos ter matrizes com elementos de qualquer tipo
class Matrix
{
	public:
		// Construtores
		Matrix( uint8 rows, uint8 columns, const float* pElements = NULL );
		Matrix( uint8 rows, uint8 columns, float value );
		Matrix( const Matrix& m );
	
		// Destrutor
		virtual ~Matrix( void );
	
		// Determina os elementos da matriz
		inline void setElements( const float* pElements );
	
		// Inicializa todos os elementos da matriz com o valor recebido como parâmetro
		inline void setElements( float value );
	
		// Obtém os elementos da matriz
		inline const float* getElements( void ) const { return pMtx; };
	
		// Retorna o número de linhas da matriz
		inline uint8 getNRows( void ) const { return nRows; };
	
		// Retorna o número de colunas da matriz
		inline uint8 getNColumns( void ) const { return nColumns; };
	
		// Indica se a matriz é quadrada
		inline bool isSquare( void ) const { return nRows == nColumns; };
	
		// Transforma a matriz em uma matriz identidade
		void setIdentity( void );
	
		// Indica se esta matriz é uma matriz identidade
		bool isIdentity( void ) const;
	
		// Retorna o determinante da matriz
		// - http://people.richland.edu/james/lecture/m116/matrices/determinant.html
		float determinant( void ) const;

		// Retorna a matriz inversa desta matriz
		Matrix* inverse( Matrix* pInverse ) const;
	
		// Transforma esta matriz em sua matriz inversa
		Matrix* inverse( void );
	
		// Retorna a matriz transposta desta matriz
		Matrix* transpose( Matrix* pTranspose ) const;
	
		// Transforma esta matriz em sua matriz transposta
		Matrix* transpose( void );
	
		// OPERADORES

		// Não é muito bom sobrecarregar o operador [] em casos de arrays multi-dimensionais. A
		// principal causa é o fato de ele só poder receber um parâmetro. A implementação que se faz
		// necessária e outros motivos para não usá-lo podem ser encontrados em:
		// - http://www.parashift.com/c++-faq-lite/operator-overloading.html#faq-13.10
		//float& operator[]( int32 index );
		//float operator[]( int32 index ) const;

		Matrix& operator=( const Matrix &m );

		Matrix operator-( const Matrix& m ) const;
		Matrix& operator-=( const Matrix& m );

		Matrix operator+( const Matrix& m ) const;
		Matrix& operator+=( const Matrix& m );
	
		Matrix operator/( float mul ) const;
		Matrix operator*( float mul ) const;
		Matrix operator*( const Matrix& m ) const;
	
		Matrix& operator/=( float div );
		Matrix& operator*=( float mul );
		Matrix& operator*=( const Matrix& m );

		float operator()( uint8 row, uint8 column ) const;
		float& operator()( uint8 row, uint8 column );

	private:
		// Inicializa o objeto
		inline void build( void );
	
		// Rotina recursiva para cálculo do determinante
		// - http://people.richland.edu/james/lecture/m116/matrices/determinant.html
		static float determinant( const Matrix* pMatrix );
	
		// Retorna a matriz adjunta desta matriz
		Matrix* adjoint( Matrix* pAdjoint ) const;
	
		// Retorna a linha / coluna que possui o maior número de zeros em pIndex. O valor de retorno
		// será true caso seja uma linha, ou false caso seja uma coluna
		bool getBetterChoice( int16* pIndex ) const;
	
		// Reduz a matriz pOrigin para pDest, retirando a linha indicada por skipRow e a coluna
		// indicada por skipColumn
		static Matrix* reduce( const Matrix* pOrigin, Matrix* pDest, uint8 skipRow, uint8 skipColumn );
	
		// Número de linhas da matriz
		uint8 nRows;

		// Número de colunas da matriz
		uint8 nColumns;
	
		// Matriz
		float* pMtx;
};

#endif