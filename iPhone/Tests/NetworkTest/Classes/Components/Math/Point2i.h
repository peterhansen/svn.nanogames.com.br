/*
 *  Point2i.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 1/19/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef POINT2I_H
#define POINT2I_H 1

#include "NanoTypes.h"

// Pré-definição de Point3f para que possamos utilizar seu nome da definição de Point2i. Assim
// evitamos #includes recursivos
class Point3f;

class Point2i
{
	public:
		// Assim podemos acessar os dados do ponto através de seus valores individuais ou como
		// um array
		union
		{
			struct
			{
				int32 x, y;
			};
			int32 p[2];
		};
	
		// XCode ainda não suporta o padrão N2210
		// http://www.google.com.br/search?hl=pt-BR&client=firefox-a&rls=org.mozilla%3Apt-BR%3Aofficial&q=C%2B%2B+n2210+&btnG=Pesquisar&meta=
		// Ressucita o construtor padrão
		// Point2i() = default;

		// Construtores
		explicit Point2i( int32 x = 0, int32 y = 0 );
		explicit Point2i( const Point3f* pPoint );

		// Inicializa o objeto
		inline void set( Point2i& point ){ *this = point; };
		inline void set( int32 x = 0, int32 y = 0 ){ this->x = x; this->y = y; };
	
		void set( Point3f& point );  // Não é inline pois aconteceria um include cíclico

		// Retorna o módulo do vetor descrito pelo ponto
		float getModule( void ) const;

		// Normaliza o vetor descrito pelo ponto
		Point2i& normalize( void );
	
		// Retorna normalizado o vetor descrito pelo ponto
		Point2i getNormalized( void ) const;
	
		// Operadores de conversão ( casting )
//		operator int32* ();
//		operator const int32* () const;
	
		// Operadores unários
		Point2i operator + () const;
		Point2i operator - () const;

		// Operadores binários
		Point2i operator + ( const Point2i& point ) const;
		Point2i operator - ( const Point2i& point ) const;
		Point2i operator * ( float f ) const;
		Point2i operator / ( float f ) const;
	
		// Produto Vetorial - Cross Product: O produto vetorial só está definido para 3 e 7
		// dimensões
		// Point2i operator % ( const Point2i& point ) const; 
	
		float operator * ( const Point2i& point ) const; // Produto Interno - Dot Product
	
		// Permite que façamos 4.0f * p, pois o outro "operador *" permite apenas p * 4.0f
		friend Point2i operator * ( float f, const Point2i& point );

		// Operadores de atribuição
		// TODOO Point2i& operator = ( const Point3f& point );
		Point2i& operator += ( const Point2i& point );
		Point2i& operator -= ( const Point2i& point );
		Point2i& operator *= ( float f );
		Point2i& operator /= ( float f );
	
		// Produto Vetorial - Cross Product: O produto vetorial só está definido para 3 e 7
		// dimensões
		// Point2i& operator %= ( const Point2i& point );

		// Operadores lógicos
		bool operator == ( const Point2i& point ) const;	
		bool operator != ( const Point2i& point ) const;
};

#endif

