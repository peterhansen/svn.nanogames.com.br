#include "Particle.h"
#include "ResourceManager.h"


// Inicializa as variáveis estáticas da classe
Texture2DHandler Particle::textures[ TOTAL_TYPES ];

#define PARTICLE_ROTATION_SPEED_MAX 480.0f


Particle::Particle( void ) : 
	RenderableImage(),
	accTime( 0.0f ),
	speedX( 0.0f ),
	speedY( 0.0f ),
	rotationSpeed( 0.0f )
{
}


void Particle::born( int8 type, Point3f &bornPosition ) {
	speedY = -Random::GetFloat( 0.0f, pFruitListener->getMaxYSpeed() );
	
	setImage(textures[type]);
	
	accTime = 0.0f;
	
	float scale = Random::GetFloat( 0.1f, 1.0f );
	setScale( scale, scale, 1.0f );

	rotationSpeed = -( PARTICLE_ROTATION_SPEED_MAX ) + Random::GetFloat( 0.0f, EXPLOSION_MAX_SPEED_X * 2.0f );
	speedX = -( EXPLOSION_MAX_SPEED_X * 0.5f ) + Random::GetFloat( 0.0f, EXPLOSION_MAX_SPEED_X );
	setVisible( true );

	initialPos = bornPosition;
}


bool Particle::update( float delta ) {
	if ( isVisible() ) {
		accTime += delta;

		position.set( initialPos.x + speedX * accTime,
					  initialPos.y + ( speedY * accTime ) + ( HALF_GRAVITY_ACC * accTime * accTime ) );
		
		Point3f axis( 0.0f, 0.0f, 1.0f );
		rotate(rotationSpeed * delta, &axis );

		if ( position.x <= -getWidth() || position.x >= pFruitListener->getParticleAreaWidth() || position.y >= pFruitListener->getParticleAreaHeight() ) {
			setVisible( false );
			pFruitListener->onDie();
		}
	}

	return true;
}


bool Particle::LoadImages( void )
{
	char fruitPath[] = { ' ', '_', 'p', 'i', 'e', 'c', 'e', '\0' };
	for( uint8 i = 0 ; i < TOTAL_TYPES ; ++i ) {
		fruitPath[0] = 'A' + i;
		
		Texture2DHandler hTexture = ResourceManager::GetTexture2D( fruitPath );
		if( !hTexture )
			goto Error;
		
		textures[i] = hTexture;
		
		TextureFrame frame( 0, 0, hTexture->getWidth(), hTexture->getHeight(), 0, 0 );
		hTexture->changeFrame( 0, &frame );
	}
		
	return true;
	
	// Label de tratamento de erros
Error:
	ReleaseImages();
	return false;
}


bool Particle::ReleaseImages( void )
{
	RefCounter< Texture2D > deleter( NULL );
	for( uint8 i = 0 ; i < TOTAL_TYPES ; ++i )
	{
		textures[i] = deleter;
	}
	
	return true;
}


void Particle::setFruitListener( FruitListener *pListener ) { 
	pFruitListener = pListener;
};
