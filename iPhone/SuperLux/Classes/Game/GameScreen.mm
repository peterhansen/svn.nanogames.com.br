#include "GameScreen.h"

#include "AudioManager.h"
#include "AudioSource.h"
#include "Exceptions.h"
#include "Font.h"
#include "Label.h"
#include "Random.h"
#include "RenderableImage.h"
#include "ResourceManager.h"
#include "Sprite.h"

#include "GameUtils.h"
#include "PepsiCaeBienoAppDelegate.h"
#include "ScoreLabel.h"
#include "Fruit.h"
#include "TipsView.h"

#if DEBUG
	#include "Tests.h"
#endif

// Determina o número de objetos na cena
#define SCENE_N_OBJECTS ( 50 + SCORE_LABELS_MAX )

// Macro para reduzir a digitação em build()
#define PEPSI_CAE_BIEN_CHECKED_INSERT( pObj )	if( !pObj || !insertObject( pObj ) )	\
												{										\
													delete pObj;						\
													goto Error;							\
												}

// Duração da transição de mostrar/esconder palavras, em milisegundos
#define TIME_TRANSITION 0.47f

#define TIME_BORDER_ANIMATION 0.666f // CAPETÃO!

// Pontuação máxima mostrada
#define SCORE_SHOWN_MAX 999999

#define SCORE_X 20.0f
#define SCORE_Y 377.0f

#define TIME_GAME_OVER		 3.0f
#define TIME_GAME_FINISHED	10.0f
#define VIBRATION_TIME_EXPLOSION 1.2f

// Configurações do botão de pause
#define GAME_BT_PAUSE_POS_X 361.0f
#define GAME_BT_PAUSE_POS_Y_ACCEL_ONLY		264.0f
#define GAME_BT_PAUSE_POS_Y_ACCEL_N_TOUCH	231.0f
#define GAME_BT_PAUSE_WIDTH 128.0f
#define GAME_BT_PAUSE_HEIGHT 64.0f

// Configurações dos botões de movimentar as peças para a esquerda e para a direita
#define GAME_BT_LEFT_DIST_FROM_LEFT		 9.0f
#define GAME_BT_LEFT_DIST_FROM_BOTTOM	 5.0f
#define GAME_BT_LEFT_WIDTH				63.0f
#define GAME_BT_LEFT_HEIGHT				51.0f

#define GAME_BT_RIGHT_DIST_FROM_RIGHT	GAME_BT_LEFT_DIST_FROM_LEFT
#define GAME_BT_RIGHT_DIST_FROM_BOTTOM	GAME_BT_LEFT_DIST_FROM_BOTTOM
#define GAME_BT_RIGHT_WIDTH				GAME_BT_LEFT_WIDTH
#define GAME_BT_RIGHT_HEIGHT			GAME_BT_LEFT_HEIGHT

// Configurações do botão "Opções"
#define GAME_BT_OPT_POS_X_OFFSET		  2.0f
#define GAME_BT_OPT_POS_Y_ACCEL_ONLY	GAME_BT_PAUSE_POS_Y_ACCEL_ONLY
#define GAME_BT_OPT_POS_Y_ACCEL_N_TOUCH GAME_BT_PAUSE_POS_Y_ACCEL_N_TOUCH
#define GAME_BT_OPT_WIDTH 118.0f
#define GAME_BT_OPT_HEIGHT 64.0f

#define NEXT_FRUIT_WIDTH	48.0f
#define NEXT_FRUIT_HEIGHT	48.0f

// Determina as dimensões das mensagens que exibem o nível atual no início de cada fase
#define GAME_LV_MSG_WIDTH 130.0f
#define GAME_LV_MSG_HEIGHT 64.0f

// Determina as dimensões das mensagens que exibem a mensagem de fim de jogo
#define GAME_END_MSG_WIDTH 207.0f
#define GAME_END_MSG_HEIGHT 70.0f

// Determina as dimensões da mensagem de pausa
#define GAME_PAUSE_MSG_WIDTH	130.0f
#define GAME_PAUSE_MSG_HEIGHT	 62.0f

// Definições das cores multiplicadas pelos botões em seus estados
#define GAME_SCREEN_BT_PRESSED_COLOR	( uint8 )127, ( uint8 )127, ( uint8 )127, ( uint8 )255
#define GAME_SCREEN_BT_UNPRESSED_COLOR	( uint8 )255, ( uint8 )255, ( uint8 )255, ( uint8 )255

// Definicao da posicao
#define PAUSE_AND_BACK_X  0.0f
#define PAUSE_AND_BACK_Y  415.0f

// Definicao da posicao
#define HEART_X  230.0f
#define HEART_Y  382.0f

// Definicao da posicao
#define SUPERLUX_X  66.0f
#define SUPERLUX_Y  12.0f

// Definicao da posicao
#define BOARD_GAME_X  0.0f
#define BOARD_GAME_Y  0.0f

// Definicao da posicao
#define BOARD_GAME_VIEWPORT_WIDTH  ( FRUITGROUP_WIDTH )
#define BOARD_GAME_VIEWPORT_HEIGHT  ( FRUITGROUP_HEIGHT )
#define BOARD_GAME_VIEWPORT_X  ( ( SCREEN_WIDTH - BOARD_GAME_VIEWPORT_WIDTH ) * 0.5f )
#define BOARD_GAME_VIEWPORT_Y  74.0f

#define MSG_BORDER_WIDTH 210.0f
#define MSG_BORDER_HEIGHT 38.0f
#define MSG_BORDER_BIG_WIDTH 179.0f
#define MSG_BORDER_BIG_HEIGHT 63.0f
#define MSG_BORDER_BIG_TEXT_Y 30.0f
#define MSG_BORDER_TEXT_Y_OFFSET ( -3.0f )

#define HEART_SEQUENCE_BLINKING 1

// Cores dos quads sob as imagens dos botões
#define GAME_SCREEN_BT_COLOR_PRESSED Color( 0.5f, 0.5f, 0.5f, 1.0f )
#define GAME_SCREEN_BT_COLOR_UNPRESSED Color( 1.0f, 1.0f, 1.0f, 1.0f )

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

GameScreen::GameScreen( void )
		  :	Scene( SCENE_N_OBJECTS ),
			EventListener( EVENT_TOUCH ),
			nActiveTouches( 0 ),
			settingRestPos( 0 ),
			initDistBetweenTouches( 0.0f ),
			level( 0 ),
			buttonPressingTouch( -1 ),
			timeToNextState( 0.0f ),
			timeLeft( 0.0f ),
			timeToNextPiece( 0.0f ),
			score( 0 ),
			scoreShown( 0 ),
			currSoundIndex( 0 ),
			lives(LIVES_MAX),
			shining( 0 ),
			borderTime( 0.0f ),
			borderState( BORDER_STATE_HIDDEN ),
			pPauseBack( NULL ),
			pScoreLabel( NULL ),
			pMessageLabel( NULL ),
			pTimeBar( NULL ),
			pMessageBorder( NULL ),
			pMessageBorderBig( NULL ),
			pEmitter( NULL ),
			timeScreenVibrating( 0.0f ),
			pMask( NULL )
{
	// Inicializa os arrays da classe
	memset( fruits, 0, sizeof( Fruit* ) * TOTAL_FRUITS );
	memset( hearts, 0, sizeof( Sprite* ) * LIVES_MAX );

	// Inicializa os componentes do da cena
	if( !build() )
		#if DEBUG
			throw ConstructorException( "GameScreen::GameScreen: Unable to create object" );
		#else
			throw ConstructorException();
		#endif
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

GameScreen::~GameScreen( void )
{
	saveScoreIfRecord();
}

/*==============================================================================================

MÉTODO saveScoreIfRecord
	Salva a pontuação atual caso esta seja um recorde.

==============================================================================================*/

void GameScreen::saveScoreIfRecord( void )
{
	if ( gameState != STATE_GAME_OVER )
		[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) saveScoreIfRecord: score];
}

/*==============================================================================================

MÉTODO build
	Inicializa o objeto.

==============================================================================================*/

bool GameScreen::build( void )
{
	Sprite * pSpritesLiq[TOTAL_TYPES];
	memset(pSpritesLiq, 0, sizeof(Sprite *)*TOTAL_TYPES);

	Sprite * pSpritesExp[TOTAL_TYPES];
	memset(pSpritesExp, 0, sizeof(Sprite *)*TOTAL_TYPES);

	{ // Evita erros com os gotos

		// Cria a câmera que irá renderrizar a cena do jogo
		OrthoCamera* pCamera = new( std::nothrow )OrthoCamera();
		if( !pCamera )
			return false;

		setCurrentCamera( pCamera );
		//pCamera->setLandscapeMode( true );

		// Aloca o background
		RenderableImage* pBkg = new( std::nothrow )RenderableImage( "bkgGame" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pBkg );
		
		pBkg->setPosition( 0.0f, 0.0f );
//		TextureFrame bkgFrame( 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0 );
//		pBkg->setImageFrame( bkgFrame );
			
		// Aloca o quadro da fase
		RenderableImage* pBoardGame = new( std::nothrow )RenderableImage( "board_game" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pBoardGame );
		
		pBoardGame->setPosition( BOARD_GAME_X, BOARD_GAME_Y );
		
		// Aloca o titulo SuperLux
		RenderableImage* pSuperLux = new( std::nothrow )RenderableImage( "superlux" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pSuperLux );
		
		pSuperLux->setPosition( SUPERLUX_X, SUPERLUX_Y );
		
		
		// Aloca a barra de tempo
		pTimeBar = new( std::nothrow )TimeBar();
		PEPSI_CAE_BIEN_CHECKED_INSERT( pTimeBar );
				
		fruitGroup = new( std::nothrow )ObjectGroup(TOTAL_FRUITS + 1);
		if( !fruitGroup || !insertObject( fruitGroup ) )
		{
			delete fruitGroup;
			goto Error;			
		}
		
		for ( int8 c = 0; c < TOTAL_COLUMNS; ++c )
		{
			for ( int8 r = 0; r < TOTAL_ROWS; ++r )
			{
				Fruit *f = new( std::nothrow )Fruit(this);
				if( !f || !fruitGroup->insertObject( f ) )
				{
					delete f;
					goto Error;
				}
				fruits[ c ][ r ] = f;
				//f.setListener( this );
			}
		}
		
		fruitGroup->setSize( FRUITGROUP_WIDTH, FRUITGROUP_HEIGHT );
		fruitGroup->setPosition( BOARD_GAME_VIEWPORT_X, BOARD_GAME_VIEWPORT_Y );
		fruitGroup->setViewport( static_cast<int32>( BOARD_GAME_VIEWPORT_X ), static_cast<int32>( BOARD_GAME_VIEWPORT_Y ), static_cast<int32>( BOARD_GAME_VIEWPORT_WIDTH ), static_cast<int32>( BOARD_GAME_VIEWPORT_HEIGHT ) );		
		
		// Aloca as liquifacoes das fru-fru-fru-frutiiiinhasssss ninja
		char fruit_id = 'A';
		char fruit_tex[20];
		char fruit_desc[20];
		for (int8 i=0; i<TOTAL_TYPES; i++)
		{
			sprintf(fruit_tex, "liq_%c_tex", fruit_id);
			sprintf(fruit_desc, "liq_%c_desc", fruit_id);
			pSpritesLiq[i] = new( std::nothrow )Sprite(fruit_tex, fruit_desc);

			if (pSpritesLiq[i] == NULL)
			{
				goto Error;
			}
			fruit_id++;
		}
	
		for (int8 i=0; i<LIQUIFY_TOTAL; i++)
		{
			
			SpriteGroup* liq = new( std::nothrow )SpriteGroup( false );
			PEPSI_CAE_BIEN_CHECKED_INSERT( liq );
						
			liquify.push_back( liq );

			for ( int8 j = 0; j < TOTAL_TYPES; ++j ) 
			{
				Sprite * pAux = new( std::nothrow )Sprite( pSpritesLiq[ j ] );
				if ((!pAux)||( !liq->insertObject( pAux ) ) )
				{
					delete pAux;
					goto Error;
				}
				pAux->setAnchor( ANCHOR_TOP | ANCHOR_HCENTER );
				pAux->setListener( liq );				
				
				if ( j == 0 ) {
					liq->setSize( pAux->getSize() );
				}
			}
		}	

		// Aloca as explosoes das fru-fru-fru-frutiiiinhasssss ninja
		fruit_id = 'A';
		for (int8 i=0; i<TOTAL_TYPES; i++)
		{
			sprintf(fruit_tex, "exp_%c_tex", fruit_id);
			sprintf(fruit_desc, "exp_%c", fruit_id);
			pSpritesExp[i] = new( std::nothrow )Sprite(fruit_tex, fruit_desc);
			
			if (pSpritesExp[i] == NULL)
			{
				goto Error;
			}
			fruit_id++;
		}
		
		for (int8 i=0; i<EXPLOSIONS_TOTAL; i++)
		{
			SpriteGroup* exp = new( std::nothrow )SpriteGroup( true );
			PEPSI_CAE_BIEN_CHECKED_INSERT( exp );
			
			explosion.push_back( exp );

			for ( int8 j = 0; j < TOTAL_TYPES; ++j ) 
			{
				Sprite * pAux = new( std::nothrow )Sprite( pSpritesExp[ j ] );
				if ((!pAux)||( !exp->insertObject( pAux ) ) )
				{
					delete pAux;
					goto Error;
				}
				pAux->setAnchor( ANCHOR_TOP | ANCHOR_HCENTER );
				pAux->setListener( exp );				
			}
		}	
		
		pEmitter = new( std::nothrow )DropEmitter();
		PEPSI_CAE_BIEN_CHECKED_INSERT( pEmitter );
		
		// Aloca a barra de pause e back
		RenderableImage *pPauseBar = new( std::nothrow )RenderableImage( "pauseBar" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pPauseBar );
		
		pPauseBar->setPosition( PAUSE_AND_BACK_X, PAUSE_AND_BACK_Y );
		
		pPauseBack = new( std::nothrow )RenderableImage( "pauseBt" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pPauseBack );
		
		TextureFrame aux( 0.0f, 0.0f, 94.0f, 32.0f, 0.0f, 0.0f );
		pPauseBack->setImageFrame( aux );
		
		pPauseBack->setPosition( ( SCREEN_WIDTH - pPauseBack->getWidth() ) * 0.5f, PAUSE_AND_BACK_Y );

		// Por mais incrível que pareça, com a linha acima a imagem não ficou centralizada...
		pPauseBack->move( 1.0f, 3.0f );
		
		// Aloca o coracao cheio
		for ( uint8 i = 0; i < LIVES_MAX; ++i )
		{
			if ( i == 0 )
				hearts[ i ] = new( std::nothrow )Sprite( "heart_tex", "heart" );
			else
				hearts[ i ] = new( std::nothrow )Sprite( hearts[ 0 ] );
			
			PEPSI_CAE_BIEN_CHECKED_INSERT( hearts[ i ] );
			
			hearts[ i ]->setPosition( HEART_X + ( hearts[ i ]->getWidth() * i ), HEART_Y );
		}
		
		
		// aloca a máscara que escurece o fundo da tela ao exibir mensagens
		pMask = new( std::nothrow ) RenderableImage( "black" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pMask );
		pMask->setVertexSetColor( Color( 0x00000077 ) );
		pMask->setSize( SCREEN_WIDTH, SCREEN_HEIGHT );				
		
		Font* pFont = [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_SMALL];
		
		pScoreLabel = new( std::nothrow )Label( pFont, false );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pScoreLabel );
		
		pScoreLabel->setPosition( SCORE_X, SCORE_Y );
		
		// Labels que informam a pontuação de cada jogada
		for( uint8 i = 0 ; i < SCORE_LABELS_MAX ; ++i )
		{
			scoreLabels[ i ] = new( std::nothrow )ScoreLabel( this );
			PEPSI_CAE_BIEN_CHECKED_INSERT( scoreLabels[ i ] );
		}
		
		pMessageBorder = new( std::nothrow )RenderableImage( "msg_border" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pMessageBorder );
		pMessageBorder->setVisible( false );
		pMessageBorder->setPosition( ( SCREEN_WIDTH - MSG_BORDER_WIDTH ) * 0.5f, ( SCREEN_HEIGHT - MSG_BORDER_HEIGHT ) * 0.5f );
		
		pMessageBorderBig = new( std::nothrow )RenderableImage( "msg_border_big" );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pMessageBorderBig );
		pMessageBorderBig->setVisible( false );		
		pMessageBorderBig->setPosition( ( SCREEN_WIDTH - MSG_BORDER_BIG_WIDTH ) * 0.5f, ( SCREEN_HEIGHT - pMessageBorderBig->getHeight() ) * 0.5f );
		
		Font* pFontMessageLabel = [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getFont: APP_FONT_NUMBERS_BIG];
		pMessageLabel = new( std::nothrow )Label( pFontMessageLabel, false );
		PEPSI_CAE_BIEN_CHECKED_INSERT( pMessageLabel );
		
		setBorderState(BORDER_STATE_HIDDEN, false );
		
		updateScoreLabel();
		
		setSize( FRUITGROUP_WIDTH, FRUITGROUP_HEIGHT );
				
		for (int8 j=0; j<TOTAL_TYPES; ++j)
		{
			delete pSpritesLiq[j];
			delete pSpritesExp[j];
		}
		return true;

	} // Evita erros com os gotos
	
	// Label de tratamento de erros
	Error:
		for (int8 j=0; j<TOTAL_TYPES; ++j)
		{
			delete pSpritesLiq[j];
			delete pSpritesExp[j];
		}
		return false;
}

/*==============================================================================================
 
 MÉTODO resetButtons
 Coloca todos os botões em seus estados iniciais.
 
 ==============================================================================================*/

//void GameScreen::resetButtons( void )
//{
//}

void GameScreen::updateTime( float delta, bool equalize ) {
	if ( timeShown != timeRemaining ) {
		if ( equalize ) {
			timeShown = timeRemaining;
		} else  {
			timeShown += timeSpeed * delta;
			if ( ( timeSpeed > 0.0f && timeShown >= timeRemaining ) || ( timeSpeed < 0.0f && timeShown <= timeRemaining ) ) {
				timeShown = timeRemaining;
			}
		}

		pTimeBar->setTime( timeShown, currentTimeGoal );
	}
} // fim do mÈtodo updateScore( boolean )

void GameScreen::prepareNextLevel()
{
	prepareLevel(level + 1);
}

/*==============================================================================================
 
 MÉTODO prepareLevel
 Prepara a fase.
 
 ==============================================================================================*/

void GameScreen::prepareLevel( int16 level )
{
	this->level = level;
	
	do {
		for ( int8 c = 0; c < TOTAL_COLUMNS; ++c ) {
			float yOffset = - FRUITGROUP_HEIGHT - Random::GetFloat( 0.0f, FRUITGROUP_HEIGHT );
			for ( int8 r = TOTAL_ROWS - 1; r >= 0; --r ) {
				Fruit * f = fruits[ c ][ r ];
				
				f->setType( TYPE_RANDOM );
				yOffset -= Random::GetFloat( 0.0f, f->getHeight() );
				f->reset( c, r, yOffset );
			}
		}
		
		
		// elimina as sequÍncias j· prontas
		
		FruitSequenceVec sequences;
		do {
			sequences.clear();
			getAllSequences( &sequences );
			
			for ( uint8 i = 0; i < sequences.size() ; ++i ) {
				for ( uint8 j = 0; j < sequences[ 0 ].size(); ++j ) {
					sequences[ 0 ][ j ]->setType( TYPE_RANDOM );
				}
			}
		} while ( sequences.size() > 0 );
	} while ( !hasSequence() );
	
	for ( uint8 i = 0; i < SCORE_LABELS_MAX; ++i )
		scoreLabels[ i ]->setState( SCORE_LABEL_STATE_IDLE );
	
	int8 difficultyLevel = (level < LEVEL_MAX ) ? level : LEVEL_MAX;
	
	stopScreenVibration();
	
	currentTimeGoal = TIME_INITIAL + ( ( TIME_FINAL - TIME_INITIAL ) * difficultyLevel / LEVEL_MAX );
	
	// reinicia a barra de tempo
	timeRemaining = 1;
	timeShown = 0;
	updateTime( 0, true );
	
	timeRemaining = currentTimeGoal * 0.5f;
	timeShown = 0;
	totalTime = 0;
	
	currentTimePerFruit = TIME_PER_FRUIT_INITIAL + ( ( TIME_PER_FRUIT_FINAL- TIME_PER_FRUIT_INITIAL ) * difficultyLevel / LEVEL_MAX );
	
	for ( int8 i = 0; i < EXPLOSIONS_TOTAL; ++i ) {
		getExplosionAt( i )->idle();
	}

	for ( int8 i = 0; i < LIQUIFY_TOTAL; ++i ) {
		getLiquifyAt( i )->idle();
	}
	
	while ( shining > 0 ) {
		Fruit * f = getFruit( Random::GetInt( 0, TOTAL_COLUMNS - 1 ), Random::GetInt( 0, TOTAL_ROWS - 1 ) );
		if ( f->getState() != STATE_BOMB ) {
			--shining;
			f->setState( STATE_FALLING_BOMB );
		}
	}
	
	setHintVisible( false );
	
	setState( STATE_BEGIN_LEVEL );
}


/*===============================================================================================================
 
MÉTODO update
	Atualiza o objeto.

============================================================================================*/
bool GameScreen::update( float delta )
{
	switch ( state ) {
		case STATE_PAUSED:
			return true;
			
		case STATE_UNPAUSING:
			break;
			
		default:
			ObjectGroup::update( delta );
			break;
	}
	
	if ( timeScreenVibrating > 0.0f ) {
		timeScreenVibrating -= delta;
		
		if ( timeScreenVibrating > 0.0f )
			setPosition( -VIBRATION_SCREEN_HALF_OFFSET + Random::GetFloat( 0.0f, VIBRATION_SCREEN_OFFSET ),
						-VIBRATION_SCREEN_HALF_OFFSET + Random::GetFloat( 0.0f, VIBRATION_SCREEN_OFFSET ) );
		else
			stopScreenVibration();
	}
	
	// caso o estado atual tenha uma duracao maxima definida, atualiza o contador
	if ( timeToNextState > 0.0f ) {
		timeToNextState -= delta;
		
		if ( timeToNextState <= 0.0f )
			onStateEnded();
	}
	
	switch ( state ) {
		case STATE_PLAYING:
			switch ( cursorState ) {
				case CURSOR_STATE_AVAILABLE:
				case CURSOR_STATE_UNDOING:
				case CURSOR_STATE_MOVING_FRUIT:
					if ( timeRemaining > 0.0f ) {
						if ( timeRemaining >= currentTimeGoal ) {
							setState( STATE_LEVEL_CLEARED );
						} else {
							totalTime += delta;
							if ( level < LEVEL_MAX - 2 && totalTime >= TIME_ACCELERATE ) {
								if ( totalTime < ( TIME_ACCELERATE_2 ) )
									delta = delta * 1.15f;
								else if ( totalTime < ( TIME_ACCELERATE_3 ) )
									delta = delta * 1.27f;
								else
									delta = delta * 1.52f;
							}
							
							changeTime( -delta );
							//if ( !hint.isVisible() && timeToHint > 0 ) {
							//	timeToHint -= delta;
							//	if ( timeToHint <= 0 )
							//		setHintVisible( true );
							//}
						}
					} else {
						// acabou o tempo do jogador
						--lives;
						hearts[ lives ]->setAnimSequence( HEART_SEQUENCE_BLINKING, false, true );
						if ( lives > 0 ) {
							setState( STATE_TIME_UP );
						} else {
							setState( STATE_GAME_OVER );
						}
					}
					break;
			}
			break;
	}
	if ( state != STATE_BEGIN_LEVEL && state != STATE_PIECES_FALLING )
		updateTime( delta, false );
	
	switch ( borderState ) {
		case BORDER_STATE_APPEARING:
		{
			borderTime += delta;
			
			if ( borderTime < TIME_BORDER_ANIMATION ) {
				Color c( 255, 255, 255, ( uint8 ) ( 255 * borderTime / TIME_BORDER_ANIMATION ) );
				
				if ( pMessageBorder->isVisible() )
					pMessageBorder->setVertexSetColor(c );
				else
					pMessageBorderBig->setVertexSetColor(c );								

				pMessageLabel->setVertexSetColor(c);
				
				c.r = 0.0f;
				c.g = 0.0f;
				c.b = 0.0f;
				c.a *= 0.5f;
				pMask->setVertexSetColor( c );
			} else {
				setBorderState( BORDER_STATE_VISIBLE, pMessageBorderBig->isVisible() );
			}
		}
		break;
			
		case BORDER_STATE_VISIBLE:
			borderTime -= delta;
			if ( borderTime <= 0.0f )
				setBorderState( BORDER_STATE_HIDING, pMessageBorderBig->isVisible() );
		break;
			
		case BORDER_STATE_HIDING:
		{
			borderTime -= delta;
			
			if ( borderTime > 0.0f ) {
				Color c( 255, 255, 255, ( uint8 ) ( 255 * borderTime / TIME_BORDER_ANIMATION ) );
				
				if ( pMessageBorder->isVisible() )
					pMessageBorder->setVertexSetColor(c );
				else
					pMessageBorderBig->setVertexSetColor(c );		
				pMessageLabel->setVertexSetColor(c);
				
				c.r = 0.0f;
				c.g = 0.0f;
				c.b = 0.0f;
				c.a *= 0.5f;
				pMask->setVertexSetColor( c );				
			} else {
				setBorderState( BORDER_STATE_HIDDEN, pMessageBorderBig->isVisible() );
			}
		}	
		break;
	}
	
	// atualiza a pontuacao
	updateScore( delta, false );
			
	return true;
}


void GameScreen::stopScreenVibration() {
	setPosition( 0, 0 );
	timeScreenVibrating = 0;
}


Point2i GameScreen::getCellAt( float x, float y ) {
	return Point2i( ( int16 ) ( ( x - fruitGroup->getPosX() ) / FRUIT_WIDTH ), ( int16 ) ( ( y - fruitGroup->getPosY() ) / FRUIT_HEIGHT ) );
}

void GameScreen::setCursorCell( float column, float row ) {
	cursorCell.set( ( TOTAL_COLUMNS + (int) column ) % TOTAL_COLUMNS, ( TOTAL_ROWS + (int) row ) % TOTAL_ROWS );
	//cursor.setRefPixelPosition( fruitGroup.getPosX() + ( cursorCell.x * FRUIT_WIDTH ) + ( FRUIT_WIDTH * 0.5f ),
	//						   fruitGroup.getPosY() + ( cursorCell.y * FRUIT_HEIGHT ) + ( FRUIT_HEIGHT * 0.5f ) );
}

void GameScreen::setCursorCell( Point2i * cell ) {
	setCursorCell( cell->x, cell->y );
}

/**
 * Troca a cÈlula do cursor.
 * @param columns quantidade de colunas a deslocar o cursor.
 * @param rows quantidade de linhas a deslocar o cursor.
 */
void GameScreen::moveCursorCell( float columns, float rows ) {
	setCursorCell( cursorCell.x + columns, cursorCell.y + rows );
}

/**
 * Indica se uma determinada cÈlula È adjacente ‡ posiÁ„o do cursor, ou seja, se a cÈlula est· num desses 2 casos:
 * 1. na mesma coluna que o cursor 
 * @param column
 * @param row
 * @return
 */
bool GameScreen::isAdjacentToCursor( float column, float row ) {
	float columnDiff = abs( column - cursorCell.x );
	float rowDiff = abs( row - cursorCell.y );
	
	return ( ( columnDiff == 1 && rowDiff == 0 ) || ( columnDiff == 0 && rowDiff == 1 ) );
}


bool GameScreen::isAdjacentToCursor( Point2i * cell ) {
	return isAdjacentToCursor( cell->x, cell->y );
}


void GameScreen::checkAndMoveCursorCell( float columns, float rows ) {
	switch ( cursorState ) {
		case CURSOR_STATE_AVAILABLE:
		case CURSOR_STATE_BLOCKED:
			moveCursorCell( columns, rows );
			break;
			
		case CURSOR_STATE_MOVING_FRUIT:
			// o teste sobre as colunas e linhas È para evitar movimentos nas diagonais
			Point2i point( static_cast<int32>( cursorCell.x + columns ), static_cast<int32>( cursorCell.y + rows ) );
			if ( ( columns == 0 || rows == 0 ) && switchFruits( cursorCell, point ) ) {
				setCursorState( CURSOR_STATE_BLOCKED );
			}
			break;
	}
}

/*===========================================================================================
 
MÉTODO handleEvent
	Método que trata os eventos enviados pelo sistema.

============================================================================================*/

bool GameScreen::handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam )
{
	if( evtType != EVENT_TOUCH )
		return false;
	
	// Obtém a informação do toque
	const NSArray* pTouchesArray = ( NSArray* )pParam;
	
	// OBS : Acho que isso nunca acontece, mas...
	uint8 nTouches = [pTouchesArray count];
	if( nTouches == 0 )
		return false;
	
	switch( evtSpecific )
	{
		case EVENT_TOUCH_BEGAN:
			{
				int8 touchIndex = -1;
				for( uint8 i = 0 ; ( i < nTouches ) && ( nActiveTouches < MAX_TOUCHES ) ; ++i )
				{
					if( ( touchIndex = startTrackingTouch( [pTouchesArray objectAtIndex:i] ) ) >= 0 )
						onNewTouch( touchIndex, &trackedTouches[i].startPos );
				}
			}
			return true;

		case EVENT_TOUCH_MOVED:
			// Se possuímos mais de um toque (no caso deste jogo, > 1 serç sempre 2), estamos recebendo uma operação
			// de zoom
			if( nActiveTouches > 1 )
			{
				// Fazer quando o usuário move apenas um dedo causa bugs estranhos
				if( nTouches == 1 )
					return false;
			}
			// Movimentação de toque único
			else
			{
				UITouch* hTouch = [pTouchesArray objectAtIndex:0];
				int8 touchIndex = isTrackingTouch( hTouch );
				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					CGPoint lastPos = [hTouch previousLocationInView:NULL];
					
					Point3f touchCurrPos( currPos.x, currPos.y );
					Point3f touchLastPos( lastPos.x, lastPos.y );

					onSingleTouchMoved( touchIndex, &touchCurrPos, &touchLastPos );
				}
			}
			return true;
		
		case EVENT_TOUCH_CANCELED:
		case EVENT_TOUCH_ENDED:
			for( uint8 i = 0 ; i < nTouches ; ++i )
			{
				UITouch* hTouch = [pTouchesArray objectAtIndex:i];
				int8 touchIndex = isTrackingTouch( hTouch );

				if( touchIndex >= 0 )
				{
					CGPoint currPos = [hTouch locationInView:NULL];
					currPos = Utils::MapPointByOrientation( &currPos );

					Point3f touchPos( currPos.x, currPos.y );
					onTouchEnded( touchIndex, &touchPos );
					
					// Cancela o rastreamento do toque
					stopTrackingTouch( touchIndex );
				}
			}
			return true;
	}
	return false;
}

/*===========================================================================================
 
MÉTODO onNewTouch
	Trata eventos de início de toque.

============================================================================================*/

void GameScreen::onNewTouch( int8 touchIndex, const Point3f* pTouchPos )
{	
	switch ( state ) {
		case STATE_PLAYING:
			
			// Cancela o pressionamento do botão de pause
			unpressPauseButton();
			
			if ( Utils::IsPointInsideRect( pTouchPos, fruitGroup ) ) {
				Point2i cellClicked = getCellAt( pTouchPos->x, pTouchPos->y );
				
				//#if DEBUG
					printf( "celula clicada: %f, %f -> %d, %d -> cursor: %d\n", pTouchPos->x, pTouchPos->y, cellClicked.x, cellClicked.y, cursorState );
				//#endif
				
				//getFruit(cellClicked.x, cellClicked.y )->setShining(true);
				/*
				Point3f p;
				Fruit *pFruit = getFruit(cellClicked.x, cellClicked.y );
				pFruit->getPositionByAnchor( &p );
				p += *fruitGroup->getPosition();
//				pEmitter->emit( pFruit->getType(), p );
				explodeFruitArea(pFruit);
	
						return;
					*/					
				switch ( cursorState ) {
					case CURSOR_STATE_AVAILABLE:
						setCursorCell( &cellClicked );
						setCursorState( CURSOR_STATE_MOVING_FRUIT );
						break;
						
					case CURSOR_STATE_MOVING_FRUIT:
						if ( isAdjacentToCursor( &cellClicked ) ) {
							checkAndMoveCursorCell( cellClicked.x - cursorCell.x, cellClicked.y - cursorCell.y );
						} else {
							setCursorCell( &cellClicked );
						}
						break;
				}
				
				//if ( hintButton != null )
				//	hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
			//} else if ( hintButton != null ) {
			//	if ( hintButton.contains( x, y ) ) {
			//		hintButton.setFrame( HINT_BUTTON_FRAME_PRESSED );
			//		keyPressed( ScreenManager.KEY_STAR );
			//	} else {
			//		hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
			//	}
			} else if( Utils::IsPointInsideRect( pTouchPos, pPauseBack ) ) {
				
				pressPauseButton( touchIndex );
			}
			break;
			
		case STATE_GAME_OVER:
		case STATE_NEW_RECORD:
			if ( timeToNextState <= ( TIME_MESSAGE * 0.5f ) )
				onStateEnded();
		break;
	}	
}

/*===========================================================================================
 
MÉTODO getPickingRay
	Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
jogo.

============================================================================================*/

void GameScreen::getPickingRay( Ray& ray, const Point3f& p ) const
{
	Utils::GetPickingRay( &ray, pCurrCamera, static_cast<int32>( p.x ), static_cast<int32>( p.y ) );

	// Corrige a origem do raio, jç que esta serç sempre a posição do observador (centro da tela)
	// OBS: Esta abordagem estç PORCA!!!!! O certo seria utilizarmos:
	// Utils::Unproject( &p, pCurrCamera, &trackedTouches[ touchIndex ].unmappedStartPos );
	// No entanto, a saída de Utils::Unproject não estç produzindo valores corretos com a projeção ortogonal

	float invZoom = 1.0f / pCurrCamera->getZoomFactor();
	Point3f screenCenter( HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT );
	ray.origin += ( Utils::MapPointByOrientation( &p ) - screenCenter ) * invZoom;
}

void GameScreen::pressPauseButton( int8 touchIndex )
{
	buttonPressingTouch = touchIndex;
	Color colorPressed( GAME_SCREEN_BT_COLOR_PRESSED );
	pPauseBack->setVertexSetColor( colorPressed );
}

void GameScreen::unpressPauseButton( void )
{
	buttonPressingTouch = -1;
	Color colorUnpressed( GAME_SCREEN_BT_COLOR_UNPRESSED );
	pPauseBack->setVertexSetColor( colorUnpressed );
}

/*===========================================================================================
 
MÉTODO onTouchEnded
	Trata eventos de fim de toque.

============================================================================================*/

void GameScreen::onTouchEnded( int8 touchIndex, const Point3f* pTouchPos )
{
//	if ( hintButton != null )
//		hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
	
	if( touchIndex == buttonPressingTouch )
	{
		unpressPauseButton();
		
		if( Utils::IsPointInsideRect( pTouchPos, pPauseBack ) )
		{
			PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;
			[hApplication playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
			[hApplication performTransitionToView: VIEW_INDEX_PAUSE_SCREEN];
			return;
		}
	}

	setCursorCell( -1, -1 );
}

/*===========================================================================================
 
MÉTODO onSingleTouchMoved
	Trata eventos de movimentação de toques.

============================================================================================*/

void GameScreen::onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos )
{
	if( buttonPressingTouch >= 0 )
	{
		Point3f fixedTouchPos = Utils::MapPointByOrientation( pCurrPos );
		
		if( !Utils::IsPointInsideRect( &fixedTouchPos, pPauseBack ) )
			unpressPauseButton();
	}
	else
	{
		if( Utils::IsPointInsideRect( pCurrPos, fruitGroup ) )
		{
			switch ( cursorState )
			{
				case CURSOR_STATE_AVAILABLE:
					{
						Point2i cell = getCellAt( pCurrPos->x, pCurrPos->y );
						setCursorCell( &cell );
					}
					break;
					
				default:
					onNewTouch( touchIndex, pCurrPos );
			}
		//} else if ( hintButton != null ) {
		//	if ( !hintButton.contains( x, y ) )
		//		hintButton.setFrame( HINT_BUTTON_FRAME_UNPRESSED );
		}
	}
}

/*===========================================================================================
 
MÉTODO isTrackingTouch
	Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja sendo
acompanhado.

============================================================================================*/

int8 GameScreen::isTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == ( int32 )hTouch )
			return i;
	}
	return -1;
}

/*===========================================================================================
 
MÉTODO startTrackingTouch
	Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
fazê-lo.

============================================================================================*/

int8 GameScreen::startTrackingTouch( const UITouch* hTouch )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
	{
		if( trackedTouches[i].Id == 0 )
		{
			++nActiveTouches;

			trackedTouches[i].Id = ( int32 )hTouch;
			
			CGPoint aux = [hTouch locationInView: NULL];
			trackedTouches[i].unmappedStartPos.set( aux.x, aux.y );

			aux = Utils::MapPointByOrientation( &aux );
			trackedTouches[i].startPos.set( aux.x, aux.y );
			
			if( nActiveTouches == 2 )
				initDistBetweenTouches = ( trackedTouches[0].startPos - trackedTouches[1].startPos ).getModule();

			return i;
		}
	}
	return -1;
}
	
/*===========================================================================================
 
MÉTODO stopTrackingTouch
	Cancela o rastreamento do toque recebido como parâmetro.

============================================================================================*/

void GameScreen::stopTrackingTouch( int8 touchIndex )
{
	if( touchIndex < 0 )
		return;

	if( trackedTouches[ touchIndex ].Id != 0 )
	{
		--nActiveTouches;
		trackedTouches[ touchIndex ].Id = 0;
		initDistBetweenTouches = 0.0f;
	}
}

/*===========================================================================================
 
MÉTODO cancelTrackedTouches
	Pára de rastrear os toques atuais.

============================================================================================*/

void GameScreen::cancelTrackedTouches( void )
{
	for( uint8 i = 0 ; i < MAX_TOUCHES ; ++i )
		trackedTouches[ i ].Id = 0;

	nActiveTouches = 0;
	initDistBetweenTouches = 0.0f;
}

/*====================================================================================

MÉTODO getNextScoreLabel

======================================================================================*/

ScoreLabel* GameScreen::getNextScoreLabel( void ) const
{
	int8 candidate = 0;
	for( int8 i = 0; i < SCORE_LABELS_MAX ; ++i )
	{
		switch( scoreLabels[ i ]->getState() )
		{
			case SCORE_LABEL_STATE_IDLE:
				return scoreLabels[ i ];

			case SCORE_LABEL_STATE_VANISHING:
				candidate = i;
				break;
		}
	}
	return scoreLabels[ candidate ];
}

void GameScreen::showMessage( const char* text, bool big )
{
	if ( text ) {
		pMessageLabel->setText( text );
		pMessageLabel->setPosition( ( SCREEN_WIDTH - pMessageLabel->getWidth() ) * 0.5f, big ? pMessageBorderBig->getPosY() + MSG_BORDER_BIG_TEXT_Y : ( ( SCREEN_HEIGHT - pMessageLabel->getHeight() ) * 0.5f ) + MSG_BORDER_TEXT_Y_OFFSET );
	}
	
	borderTime = TIME_BORDER_ANIMATION;
	setBorderState( text == NULL ? BORDER_STATE_HIDING : BORDER_STATE_APPEARING, big );
}

/*====================================================================================

MÉTODO setState
	Determina o estado da tela de jogo.

======================================================================================*/

void GameScreen::setState( int8 state, bool hideMessage )
{
	this->state = state;
	
	timeToNextState = 0.0f;
	
	if ( hideMessage )
		showMessage( NULL, false );
	
	switch ( state ) {
		case STATE_BEGIN_LEVEL:
			char levelTemp[20];
			sprintf(levelTemp, "N-VEL %d",level);
			showMessage(levelTemp, false );
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) stopAudio];
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_LEVEL_START AtIndex: SOUND_INDEX_LEVEL_START Looping: false];			
			timeToNextState = TIME_WAIT_PIECES_FALL;
			break;
			
		case STATE_PIECES_FALLING:
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_PIECES_FALL AtIndex: SOUND_INDEX_PIECES_FALL Looping: false];
			timeToNextState = TIME_MESSAGE;
		break;
			
		case STATE_PLAYING:
			cancelTrackedTouches();
			playGameBkgMusic();
			break;
			
		case STATE_TIME_UP:
			showMessage("TEMPO ESGOTADO");
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) stopAudio];
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_TIME_UP AtIndex: SOUND_INDEX_TIME_UP Looping: false];
			timeToNextState = TIME_MESSAGE;
			
			stopScreenVibration();
			break;
			
		case STATE_GAME_OVER:
			setCursorState( CURSOR_STATE_BLOCKED );
			showMessage("FIM DE JOGO");
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) stopAudio];				
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_TIME_UP AtIndex: SOUND_INDEX_TIME_UP Looping: false];
			timeToNextState = TIME_MESSAGE;
			
			stopScreenVibration();
			break;
			
		case STATE_LEVEL_CLEARED:
			setCursorState( CURSOR_STATE_BLOCKED );
			// Chama a tela de dicas de beleza
			timeToNextState = TIME_MESSAGE;
			
			showMessage("N-VEL COMPLETO!");
			
			updateScore( 0, true );

			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) stopAudio];			
			[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_LEVEL_COMPLETE AtIndex: SOUND_INDEX_LEVEL_COMPLETE Looping: false];
			
			stopScreenVibration();
			break;
			
		case STATE_PAUSED:

			break;
			
		case STATE_NEW_RECORD:
			showMessage("NOVO RECORDE!");
			timeToNextState = TIME_MESSAGE;
			break;
			
			
		case STATE_UNPAUSING:
			timeToNextState = WATER_TRANSITION_TIME;
			break;
	}
}

/*======================================================================================

MÉTODO onStateEnded

======================================================================================*/
void GameScreen::onStateEnded( void )
{
	switch ( state ) {
		case STATE_LEVEL_CLEARED:
			for ( int8 c = 0; c < TOTAL_COLUMNS; ++c ) {
				for ( int8 r = TOTAL_ROWS - 1; r >= 0; --r ) {
					if ( getFruit( c, r )->getState() == STATE_BOMB )
						++shining;
				}
			}
			[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_TIPS];
			break;
			
		case STATE_TIME_UP:
			prepareLevel(level);
			break;
			
		case STATE_BEGIN_LEVEL:
			setState( STATE_PIECES_FALLING, false );
		break;
			
		case STATE_UNPAUSING:
		case STATE_PIECES_FALLING:
			setState( STATE_PLAYING );
			break;
			
		case STATE_GAME_OVER:
			if ( [static_cast< PepsiCaeBienoAppDelegate* >( APP_DELEGATE ) isHighScore: score] >= 0 )
				setState( STATE_NEW_RECORD );
			else
				[static_cast< PepsiCaeBienoAppDelegate* >( APP_DELEGATE ) onGameOver: score];
			break;
			
		case STATE_NEW_RECORD:
			[static_cast< PepsiCaeBienoAppDelegate* >( APP_DELEGATE ) onGameOver: score];			
			break;
	}
}


/*======================================================================================

MÉTODO playGameBkgMusic
	Inicia a reprodução de uma música do jogo.

======================================================================================*/
void GameScreen::playGameBkgMusic( void )
{
	currSoundIndex = Random::GetInt( SOUND_INDEX_AMBIENT_1, SOUND_INDEX_AMBIENT_1 + ( SOUND_AMBIENT_TYPES_TOTAL) - 1 );
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: currSoundIndex AtIndex: currSoundIndex Looping: true];
	//[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
}


/*======================================================================================

MÉTODO getLevel
	Retorna a fase atual do jogo.

======================================================================================*/

int16 GameScreen::getLevel( void ) const
{
	return level;
}

/*======================================================================================

MÉTODO updateLevelLabel
	Atualiza o texto do label que exibe a fase atual.

======================================================================================*/

#define LEVEL_LABEL_BUFFER_LEN 16

void GameScreen::updateLevelLabel( void )
{
	char buffer[ LEVEL_LABEL_BUFFER_LEN ];
	snprintf( buffer, LEVEL_LABEL_BUFFER_LEN, "%d", static_cast<int16>( level ) );

	//static_cast< LevelInfo* >( getObject( OBJ_INDEX_LEVEL_INFO ) )->updateLevelLabel( buffer );
}

#undef LEVEL_LABEL_BUFFER_LEN



/*======================================================================================

MÉTODO updateScore
	Atualiza a pontuação que deve ser exibida pelo respectivo label.

	@param equalize indica se a pontuação exibida deve ser automaticamente igualada à
pontuação real.

======================================================================================*/

void GameScreen::updateScore( float timeElapsed, bool equalize )
{
	if( scoreShown < score )
	{
		if( equalize )
		{
			scoreShown = score;
		}
		else
		{
			scoreShown += scoreSpeed * timeElapsed;
			
			int32 aux = static_cast< int32 >( scoreShown );
			if( aux >= score )
			{
				if( aux > SCORE_SHOWN_MAX )
					scoreShown = SCORE_SHOWN_MAX;
				
				scoreShown = score;
			}
		}
		updateScoreLabel();
	}
}

/*======================================================================================

MÉTODO updateScoreLabel
	Atualiza o texto do label que exibe a pontuação do jogador.

======================================================================================*/

#define UPDATE_SCORE_LABEL_BUFFER_LEN 128

void GameScreen::updateScoreLabel( void )
{	
	// É meio cagado fazer este cálcula a cada vez que chamamos updateScoreLabel(), mas enfim...
	char buffer[ UPDATE_SCORE_LABEL_BUFFER_LEN ];
	snprintf( buffer, UPDATE_SCORE_LABEL_BUFFER_LEN, "%d", static_cast<uint32>( MAX_POINTS ) );
	uint8 maxPointsLen = strlen( buffer );
	
	snprintf( buffer, UPDATE_SCORE_LABEL_BUFFER_LEN, "%0*d", maxPointsLen, static_cast< uint32 >( scoreShown ) );
	pScoreLabel->setText(buffer, true, false);
}

#undef UPDATE_SCORE_LABEL_BUFFER_LEN



/*======================================================================================

MÉTODO changeScore
	Incrementa a pontuação e atualiza as variáveis e labels correspondentes.

	@param diff variação na pontuação (positiva ou negativa).

======================================================================================*/	

void GameScreen::changeScore( int32 diff )
{
	score += diff;
	scoreSpeed = ( score - scoreShown ) * 0.5f;
}

void GameScreen::changeTime( float diff ) {
	timeRemaining += diff;
	timeSpeed = ( timeRemaining - timeShown ) * 0.5f;
}



/*======================================================================================

MÉTODO getScore
	Retorna a pontuação atual do jogador.

======================================================================================*/

int32 GameScreen::getScore( void ) const
{
	return score;
}

/*======================================================================================

MÉTODO intersects

======================================================================================*/

bool GameScreen::intersects( const FruitSequence * pSeq1, const FruitSequence * pSeq2 ) const{

	for ( uint8 i = 0; i < pSeq1->size(); ++i ) {
		for ( uint8 j = 0; j < pSeq2->size(); ++j ) {
			if ( ( *pSeq1 )[i] == ( *pSeq2 )[j] )
				return true;
		}
	}
	
	return false;
}

/**
 * ObtÈm todas as sequÍncias e sub-sequÍncias existentes na cÈlula indicada.
 * @param column
 * @param row
 * @return
 */

FruitSequenceVec* GameScreen::getSequencesAt( FruitSequenceVec* pOut,  int8 column, int8 row )
{
	Fruit * pFruit = fruits[ column ][ row ];
	
	// primeiro testa o eixo horizontal
	int startX = column;
	int endX = column;
	// esquerda
	while ( startX > 0 && fruits[ startX - 1 ][ row ]->equals( pFruit ) ) {
		--startX;
	}
	// direita
	while ( endX < ( TOTAL_COLUMNS - 1 ) && fruits[ endX + 1 ][ row ]->equals( pFruit ) ) {
		++endX;
	}
	
	// testa o eixo vertical
	int startY = row;
	int endY = row;
	// cima
	while ( startY > 0 && fruits[ column ][ startY - 1 ]->equals( pFruit ) ) {
		--startY;
	}
	// baixo
	while ( endY < ( TOTAL_ROWS - 1 ) && fruits[ column ][ endY + 1 ]->equals( pFruit ) ) {
		++endY;
	}
	
	
	// caso tenha 2 sequÍncias v·lidas na cÈlula atual (horizontal E vertical), retorna a maior das 2
	int diffX = endX - startX;
	int diffY = endY - startY;
	
	#if DEBUG
		//printf( "start: %d, %d -> diff: %d, %d\n", startX, startY, diffX, diffY );
	#endif
	
	int8 TOTAL_COMBINATIONS[] = { 1, 3, 6, 10, 15, 21, 28 };
	
	if ( diffX > diffY ) {
		if ( diffX >= 2 ) {
			
			int8 total = TOTAL_COMBINATIONS[ diffX - 2 ];
			int8 initialLength = ( int8 ) ( diffX + 1 );
			
			for ( int8 i = 0, currentLength = initialLength; i < total; --currentLength )
			{
				int8 limit = ( int8 ) ( initialLength - currentLength );
				
				FruitSequence currSeq;

				for ( int8 j = 0; j <= limit; ++j, ++i )
				{
					for ( int c = 0; c < currentLength; ++c )
					{
						currSeq.push_back( getFruit( startX + c + j, row ) );
					}
				}
				
				pOut->push_back( currSeq );
			}
			return pOut;
		}
	} else {
		if ( diffY >= 2 ) {
			
			int8 total = TOTAL_COMBINATIONS[ diffY - 2 ];
			int8 initialLength = ( int8 ) ( diffY + 1 );

			for ( int8 i = 0, currentLength = initialLength; i < total; --currentLength )
			{
				int8 limit = ( int8 ) ( initialLength - currentLength );
				
				FruitSequence currSeq;
				
				for ( int8 j = 0; j <= limit; ++j, ++i )
				{
					for ( int c = 0; c < currentLength; ++c )
					{
						currSeq.push_back( getFruit( column, startY + c + j ) );	
					}
				}

				pOut->push_back( currSeq );
			}
			return pOut; 
		}
	}
	
	return NULL;
}


/**
 * Retorna a lista de sequÍncias ˙nicas existentes no tabuleiro.
 */
FruitSequenceVec* GameScreen::getAllSequences( FruitSequenceVec* pOut ) {
	FruitSequenceVec v, v2;
	
	for ( int8 c = 0; c < TOTAL_COLUMNS; ++c ) {
		for ( int8 r = 0; r < TOTAL_ROWS; ++r ) {
			FruitSequenceVec sequences;
			getSequencesAt( &sequences, c, r );
				for ( uint8 i = 0; i < sequences.size() ; ++i ) {
					v.push_back( sequences[ i ] );
					v2.push_back( sequences[ i ] );
				}
		}
	}
	
	uint32 size1 = v.size();
	for ( uint32 i = 0; i < size1; ++i ) {
		for ( uint32 j = i + 1; j < v2.size(); ) {
			FruitSequence sequence1 = v[i];
			FruitSequence sequence2 = v2[j];
			
			if ( intersects( &sequence1, &sequence2 ) ) {
				int ind = sequence1.size() < sequence2.size() ? i : j;
				v2.erase( v2.begin()+ind );
				
				++j;
			} else {
				++j;
			}
		}
	}
	
	for ( uint8 i = 0; i < v2.size(); ++i ) {
		pOut->push_back( v2[i] );
	}
	
	return pOut;
}


bool GameScreen::hasSequenceAt( int8 column, int8 row ) {
	FruitSequenceVec seq;
	getSequencesAt( &seq, column, row );
	return seq.size() > 0;
}


bool GameScreen::hasSequenceAt( int8 column, int8 row, int8 diffColumn, int8 diffRow ) {
	Fruit * originalFruit = fruits[ column ][ row ];
	if ( column + diffColumn >= TOTAL_COLUMNS || column + diffColumn < 0 )
		diffColumn = 0;
	
	if ( row + diffRow >= TOTAL_ROWS || row + diffRow < 0 )
		diffRow = 0;
	
	fruits[ column ][ row ] = fruits[ column + diffColumn ][ row + diffRow ];
	fruits[ column + diffColumn ][ row + diffRow ] = originalFruit;
	
	bool ret = hasSequenceAt( column, row );

	// destroca as frutas
	fruits[ column + diffColumn ][ row + diffRow ] = fruits[ column ][ row ];
	fruits[ column ][ row ] = originalFruit;
	
	return ret;
}


/**
 * Indica se existe pelo menos uma jogada disponÌvel para o jogador com o tabuleiro atual.
 */
bool GameScreen::hasSequence() {
	for ( int8 c = 0; c < TOTAL_COLUMNS; ++c ) {
		for ( int8 r = 0; r < TOTAL_ROWS; ++r ) {
			if ( hasSequenceAt( c, r ) || hasMoveAt( c, r ) ) {
				return true;
			}
		}
	}
	return false;
}


bool GameScreen::hasMoveAt( int8 column, int8 row ) {
	return hasSequenceAt( column, row, -1, 0 ) || hasSequenceAt( column, row, 1, 0 ) || hasSequenceAt( column, row, 0, -1 ) || hasSequenceAt( column, row, 0, 1 );
}


Fruit* GameScreen::getFruit( Point2i cell ) {
	return getFruit( cell.x, cell.y );
}


Fruit* GameScreen::getFruit( int8 column, int8 row ) {
	return fruits[ column ][ row ];
}


bool GameScreen::areFruitsIdle() {
	for ( int8 c = 0; c < TOTAL_COLUMNS; ++c ) {
		for ( int8 r = 0; r < TOTAL_ROWS; ++r ) {
			if ( !getFruit( c, r )->isIdle() )
				return false;
		}
	}
	
	return true;
}


void GameScreen::onSequenceEnded( int16 id1, int8 sequence ) {
//	if ( id1 == HINT_ID ) {
//		setHintVisible( false );
//		return;
//	}
 
	Point2i fruitCell( (id1 % TOTAL_COLUMNS), id1 / TOTAL_COLUMNS );
	
	Fruit * fruit = getFruit( fruitCell );
	int8 fruitSeq = sequence;
	switch ( fruitSeq ) {
		case SEQUENCE_EXPLODING:
		case SEQUENCE_LIQUIFY:
			fruit->setState( STATE_INVISIBLE );
			
			// retorna se ainda houver frutas na coluna atual se liquefazendo ou explodindo
			for ( int8 r = 0; r < TOTAL_ROWS; ++r ) {
				switch ( getFruit( fruitCell.x, r )->getState() ) {
					case STATE_EXPLODING:
					case STATE_LIQUIFYING_SOAP_1:
					case STATE_LIQUIFYING_SOAP_2:
					case STATE_LIQUIFYING_VANISH:
						return;
				}
			}
			
			// faz todas as frutas acima cairem
			int8 offset;
			offset= -1;
			float yOffset;
			yOffset = -(Random::GetFloat(0, fruit->getHeight() * 0.5f) );
			for ( int r1 = 0; r1 < TOTAL_ROWS; ++r1 ) {
				Fruit * f1 = getFruit( fruitCell.x, r1 );
				switch ( f1->getState() ) {
					case STATE_INVISIBLE:
						for ( int r2 = r1; r2 > 0; --r2 ) {
							fruits[ (int8) fruitCell.x ][ r2 ] = fruits[ (int8) fruitCell.x ][ r2 - 1 ];
							if ( fruitSequence == STATE_EXPLODING )
								fruits[ (int8) fruitCell.x ][ r2 - 1 ]->moveToCell( (int8) fruitCell.x, r2, STATE_FALLING, getFruit( fruitCell.x, r2 - 1 )->getInitialYSpeed() + f1->getRandomDiffYSpeed() );
							else
								fruits[ (int8) fruitCell.x ][ r2 - 1 ]->moveToCell( (int8) fruitCell.x, r2, STATE_FALLING );
						}
						
						// a fruta atual È reposicionada no topo das anteriores
						fruits[ (int8) fruitCell.x ][ 0 ] = f1;
						f1->positionByCell( fruitCell.x, offset, yOffset );
						f1->setType( TYPE_RANDOM );
						
						--offset;
						yOffset -= Random::GetFloat(0, ( f1->getHeight() * 3 ) * 0.5f );
						if ( fruitSequence == SEQUENCE_EXPLODING )
							f1->moveToCell( fruitCell.x, 0, STATE_FALLING, getFruit( fruitCell.x, r1 )->getInitialYSpeed() + f1->getRandomDiffYSpeed() );							
						else
							f1->moveToCell( fruitCell.x, 0, STATE_FALLING );
						break;
				}
			}
			break;
			
		case SEQUENCE_MOVING:
			if ( areFruitsIdle() ) {
				FruitSequenceVec sequence1;
				getSequencesAt( &sequence1, lastCellMoved.x, lastCellMoved.y );
				if ( sequence1.size() > 0 ) {
					makeSequence( sequence1[ 0 ] );
				}

				FruitSequenceVec sequence2;
				getSequencesAt( &sequence2, lastCellMoved.x + lastMove.x, lastCellMoved.y + lastMove.y );
				if ( sequence2.size() > 0 ) {
					makeSequence( sequence2[ 0 ] );
				}

				if ( cursorState == CURSOR_STATE_BLOCKED && sequence1.size() <= 0 && sequence2.size() <= 0 )
					undoLastMove();
				else if ( cursorState == CURSOR_STATE_UNDOING )
					setCursorState( CURSOR_STATE_AVAILABLE );
			}
			break;
			
		case SEQUENCE_FALLING:
			if ( areFruitsIdle() ) {
				FruitSequenceVec sequences;
				getAllSequences( &sequences );
				
				if ( sequences.size() > 0 ) {
					for ( uint8 i = 0; i < sequences.size() ; ++i )
						makeSequence( sequences[ i ] );
					
				} else {
					setCursorState( CURSOR_STATE_AVAILABLE );
				}
			}
			break;
	}
}

void GameScreen::setCursorState( int8 cursorState ) {
	switch ( cursorState ) {
		case CURSOR_STATE_AVAILABLE:
			comboCounter = level;
			break;
	}
	this->cursorState = cursorState;
//	cursorArrows.setVisible( cursorState == CURSOR_STATE_MOVING_FRUIT );
}

bool GameScreen::switchFruits( Point2i cell1, Point2i cell2 ) {
	if ( cell2.x >= 0 && cell2.x < TOTAL_COLUMNS && cell2.y >= 0 && cell2.y < TOTAL_ROWS ) {
		Point2i move = cell2 - cell1;
		fruits[ (int8) cell1.x ][ (int8) cell1.y ]->moveToCell( cell2.x, cell2.y, move.x == 0 ? STATE_SWITCHING_V : STATE_SWITCHING_H );
		fruits[ (int8) cell2.x ][ (int8) cell2.y ]->moveToCell( cell1.x, cell1.y, move.x == 0 ? STATE_SWITCHING_V : STATE_SWITCHING_H );
		
		Fruit * temp = fruits[ (int8) cell1.x ][ (int8) cell1.y ];
		fruits[ (int8) cell1.x ][ (int8) cell1.y ] = fruits[ (int8) cell2.x ][ (int8) cell2.y ];
		fruits[ (int8) cell2.x ][ (int8) cell2.y ] = temp;
		
		lastCellMoved.set( cell1 );
		lastMove.set( move );
		
		return true;
	}
	return false;
}


void GameScreen::undoLastMove() {
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_MOVE_ERROR AtIndex: SOUND_INDEX_MOVE_ERROR Looping: false];
	switchFruits( lastCellMoved, lastCellMoved + lastMove );
	setCursorState( CURSOR_STATE_UNDOING );
}

bool GameScreen::setHintVisible( bool visible ) {
	if ( visible ) {
		//if ( cursorState == CURSOR_STATE_AVAILABLE && !hint.isVisible() ) {
		//	hint.setVisible( true );
		//	hint.setSequence( HINT_SEQUENCE_ANIMATING );
			
			int initialColumn = Random::GetInt(0, TOTAL_COLUMNS - 1 );
			int initialRow = Random::GetInt(0, TOTAL_ROWS - 1 );
			bool traverseColumn = true;
			// mostra uma dica aleatÛria
			for ( int c = initialColumn; c != initialColumn || traverseColumn; c = ( c + 1 ) % TOTAL_COLUMNS ) {
				bool traverseRow = true;
				for ( int r = initialRow; r != initialRow || traverseRow; r = ( r + 1 ) % TOTAL_ROWS ) {
					if ( hasMoveAt( c, r ) ) {
						//hint.setRefPixelPosition( fruitGroup.getPosition().add( getFruit( c, r ).getRefPixelPosition() ) );
						return true;
					}
					traverseRow = false;
				}
				traverseColumn = false;
			}
		//}
	} else {
		//hint.setVisible( false );
		//hint.setSequence( HINT_SEQUENCE_IDLE );
		
		timeToHint = TIME_HINT_INITIAL + ( TIME_HINT_FINAL - TIME_HINT_INITIAL ) * ( LEVEL_MAX - ((level>LEVEL_MAX) ? level : LEVEL_MAX) ) / LEVEL_MAX;
	}
	return false;
}


void GameScreen::makeSequence( FruitSequence sequence ) {
	int specialIndex = -1;
	int secondIndex = -1;
	int8 specialType = -1;
	
	for ( uint16 i = 0; i < sequence.size(); ++i ) {	
		// nao deveria acontecer isso, ja que areFruitIdle() eh chamado antes de chegar nesse metodo, mas de qualquer forma
		// acontece, e esse teste evita que mais de uma animacao de liquefacao seja feita para a mesma sequencia
		Fruit* pF = ( Fruit * )sequence[i];
		if ( !pF->isIdle() ) {
			return;
		}
	}
	
	bool liquifyInPlace = false;
	Point3f destination;
	switch ( sequence.size() ) {
		case 4:   
		case 5:
		case 6:
		{
			Point2i cell1 = sequence[ 0 ]->getCell();
			Point2i cell2 = sequence[ sequence.size() - 1 ]->getCell();
			
			if ( cell1.x == cell2.x ) {
				// vertical
				specialIndex = abs( cell1.y - ( TOTAL_ROWS >> 1 ) ) < abs( cell2.y - ( TOTAL_ROWS >> 1 ) ) ? 0 : sequence.size() - 1;
			} else {
				// horizontal
				specialIndex = abs( cell1.x - ( TOTAL_COLUMNS >> 1 ) ) < abs( cell2.x - ( TOTAL_COLUMNS >> 1 ) ) ? 0 : sequence.size() - 1;
			}
			
			switch ( sequence[ specialIndex ]->getState() ) {
				case STATE_BOMB:
				case STATE_EXPLODING:
					for ( uint8 i = 0; i < sequence.size(); ++i ) {
						switch ( sequence[ i ]->getState() ) {
							case STATE_BOMB:
							case STATE_EXPLODING:
								break;
								
							default:
								specialIndex = i;
								i = sequence.size();
						}
					}
					break;
			}
			
			specialType = STATE_BOMB;
			//MediaPlayer.vibrate( VIBRATION_TIME_DEFAULT * ( sequence.length - 3 ) );
			destination = ( *sequence[ 1 ]->getPosition() + *sequence[ sequence.size() - 1 ]->getPosition() ) * 0.5f;
		}
		break;
			
		default:
			// faz uma prÈ-an·lise da sequÍncia, para detectar explosıes e evitar frutas sendo "sugadas" para dentro da explos„o
			for ( uint8 i = 0; i < sequence.size(); ++i ) {
				switch ( sequence[ i ]->getState() ) {
					case STATE_BOMB:
					case STATE_EXPLODING:
						i = sequence.size();
						liquifyInPlace = true;
						break;
				}
			}
			if ( !liquifyInPlace )
				destination = ( *sequence[ 0 ]->getPosition() + *sequence[ sequence.size() - 1 ]->getPosition() ) * 0.5f;
	}
	secondIndex = ( specialIndex == 0 ) ? 1 : specialIndex - 1;
	
	if ( !liquifyInPlace ) {
			SpriteGroup* liquifySprite = getNextLiquify();
		
		liquifySprite->setPosition( destination.x + fruitGroup->getPosX() + ( ( FRUIT_WIDTH - liquifySprite->getWidth() ) * 0.5f ),
								    destination.y + fruitGroup->getPosY() + ( ( FRUIT_HEIGHT - liquifySprite->getHeight() ) * 0.5f ), 0.0f );
		liquifySprite->animate( static_cast<Fruit*> ( sequence[ 0 ] )->getType() );
	}
	
	for ( uint8 i = 0; i < sequence.size(); ++i ) {
		switch ( sequence[ i ]->getState() ) {
			case STATE_EXPLODING:
				break;
				
			case STATE_BOMB:
				explodeFruitArea( sequence[ i ] );
				break;
				
			default:
				if ( i == specialIndex ) {
					sequence[ i ]->setState( specialType );
				} else {
					if ( liquifyInPlace ) {
						destination = *sequence[ i ]->getPosition();
						
						SpriteGroup* liquifySprite = getNextLiquify();
						liquifySprite->setPosition( destination.x + fruitGroup->getPosX() + ( ( FRUIT_WIDTH - liquifySprite->getWidth() ) * 0.5f ),
												   destination.y + fruitGroup->getPosY() + ( ( FRUIT_HEIGHT - liquifySprite->getHeight() ) * 0.5f ), 0.0f );
						liquifySprite->animate( sequence[ i ]->getType() );
					}
					sequence[ i ]->liquify( destination, liquifyInPlace || ( i == secondIndex ) );
				}
				// fim default
		}
	}
	
	int16 sequenceScore = comboCounter * ( SCORE_SEQUENCE + ( sequence.size() - 3 ) * SCORE_EXTRA_FRUIT );
	getNextScoreLabel()->setScore( *fruitGroup->getPosition() + *sequence[ sequence.size() >> 1 ]->getPosition(), sequenceScore );
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_SEQUENCE AtIndex: SOUND_INDEX_SEQUENCE Looping: false];
	++comboCounter;
	
	changeTime( sequence.size() * currentTimePerFruit );
	
	setHintVisible( false );
}

SpriteGroup* GameScreen::getNextLiquify() {
	for ( int8 i = 0; i < LIQUIFY_TOTAL; ++i ) {
		if ( getLiquifyAt( i )->isIdle() )
			return getLiquifyAt( i );
	}
	
	return getLiquifyAt( Random::GetInt( 0, LIQUIFY_TOTAL - 1 ) );
}

SpriteGroup* GameScreen::getNextExplosion() {
	for ( int8 i = 0; i < EXPLOSIONS_TOTAL; ++i ) {
		if ( getExplosionAt( i )->isIdle() )
			return getExplosionAt( i );
	}
	
	return getExplosionAt( Random::GetInt( 0, EXPLOSIONS_TOTAL - 1 ) );
}


void GameScreen::explodeFruitArea( Fruit* pFruit ) {
	Point2i cell = pFruit->getCell();
	Point2i limitMin( (cell.x - 1 ) > 0 ? cell.x - 1 : 0, (cell.y - 1) > 0 ? cell.y - 1 : 0 );
	Point2i limitMax( (cell.x + 1 < TOTAL_COLUMNS - 1) ? cell.x + 1 : TOTAL_COLUMNS - 1, (cell.y + 1 < TOTAL_ROWS - 1) ? cell.y + 1 : TOTAL_ROWS - 1 );
	
#if DEBUG
	printf( "EXPLODING FROM %d, %d TO %d, %d\n", limitMin.x, limitMin.y, limitMax.x, limitMax.y );
#endif
	
	//MediaPlayer.vibrate( VIBRATION_TIME_EXPLOSION );
	timeScreenVibrating = VIBRATION_TIME_EXPLOSION;
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) vibrate: VIBRATION_TIME_DEFAULT];
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_BOMB AtIndex: SOUND_INDEX_BOMB Looping: false];	
	
	// OLD
//	SpriteGroup * explosion = getNextExplosion();
//	explosion->animate( pFruit->getType() );
//	Point3f pAux;
//	Point3f pPos(*pFruit->getPositionByAnchor(&pAux) + *fruitGroup->getPosition());
//	explosion->setPositionByAnchor( &pPos );
	
	explodeFruit( pFruit );
	++comboCounter;
	for ( int c = limitMin.x; c <= limitMax.x; ++c ) {
		for ( int r = limitMin.y; r <= limitMax.y; ++r ) {
			Fruit * f = getFruit( c, r );
			if ( f != pFruit ) {
				if ( f->getState() == STATE_BOMB )
					explodeFruitArea( f );
				else
					explodeFruit( f );
			}
		}
	}
}


void GameScreen::explodeFruit( Fruit* pFruit ) {
	pFruit->setState( STATE_EXPLODING );
	getNextScoreLabel()->setScore( *fruitGroup->getPosition() + *pFruit->getPosition(), SCORE_SEQUENCE * comboCounter );

	Point3f p;
	pFruit->getPositionByAnchor( &p );
	p += *fruitGroup->getPosition();
	pEmitter->emit( pFruit->getType(), p );
	
	SpriteGroup * explosion = getNextExplosion();
	explosion->animate( pFruit->getType() );
	Point3f pAux;
	Point3f pPos(*pFruit->getPositionByAnchor(&pAux) + *fruitGroup->getPosition());
	explosion->setPositionByAnchor( &pPos );	
	
	changeTime( currentTimePerFruit );
	
	// faz todas as frutas acima pular
	Point2i fruitCell = pFruit->getCell();
	float initialYSpeed = pFruit->getRandomInitialYSpeed();
	for ( int r1 = fruitCell.y - 1; r1 >= 0; --r1 ) {
		Fruit * f1 = getFruit( fruitCell.x, r1 );
		if ( f1->isIdle() ) {
			f1->moveToCell( f1->getCell().x, r1 + 1, STATE_FALLING, initialYSpeed );
			initialYSpeed += f1->getRandomDiffYSpeed();
		}
	}
}


void GameScreen::setBorderState( uint8 state, bool big ) {	
	switch ( state ) {
		case BORDER_STATE_HIDDEN:
		break;

		case BORDER_STATE_APPEARING:
		{
			if ( borderState == BORDER_STATE_VISIBLE )
				return;
			
			pMessageBorder->setBlenFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
			pMessageBorderBig->setBlenFunc( GL_ONE, GL_ONE_MINUS_SRC_ALPHA );
			
			Color c( 0x00000000 );
			pMask->setVertexSetColor( c );
			borderTime = 0.0f;
		}
		break;
		
		case BORDER_STATE_VISIBLE:
		{
			borderTime = TIME_MESSAGE - ( TIME_BORDER_ANIMATION * 2.0f ) - 0.1f;
			
			Color c( 0xffffffff );
			pMessageLabel->setVertexSetColor( c );	
			pMessageBorder->setVertexSetColor( c );	
			pMessageBorderBig->setVertexSetColor( c );	
			
			c.r = 0.0f;
			c.g = 0.0f;
			c.b = 0.0f;
			c.a = 0.5f;
			pMask->setVertexSetColor( c );
		}
		break;
		
		case BORDER_STATE_HIDING:
			if ( borderState == BORDER_STATE_HIDDEN )
				return;
			
			pMessageBorder->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
			pMessageBorderBig->setBlenFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );			
			
			borderTime = TIME_BORDER_ANIMATION;
		break;
	}
	
	borderState = state;	
	
	pMessageLabel->setVisible( state != BORDER_STATE_HIDDEN );
	pMask->setVisible( pMessageLabel->isVisible() );
	if ( big ) {
		pMessageBorder->setVisible( false );
		pMessageBorderBig->setVisible( state != BORDER_STATE_HIDDEN );
	} else {
		pMessageBorder->setVisible( state != BORDER_STATE_HIDDEN );
		pMessageBorderBig->setVisible( false );							
	}	
}

// Indica que a aplicação recebeu um evento de suspend. Quando a aplicação
// chama este método da cena, significa que a cena já parou de ser atualizada.
// Os eventos de update serão retomados após a próxima chamada a resume()
void GameScreen::suspend( void )
{
	if( state == STATE_PLAYING )
	{
		unpressPauseButton();

		[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) pauseAllSounds: true];
	}
}

// Indica que a aplicação irá resumir sua execução. Quando a aplicação
// chama este método da cena, significa que a cena voltará ser atualizada
// através de eventos de update
void GameScreen::resume( void )
{
	if( state == STATE_PLAYING )
		[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) pauseAllSounds: false];
}
