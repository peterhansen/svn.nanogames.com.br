#include "OptionsView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"

#define BT_LANG_ALPHA_SELECTED		1.0f
#define BT_LANG_ALPHA_UNSELECTED	0.5f

// Configurações da barra de volume
#define OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH 9


// Implementação da classe
@implementation OptionsView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

//- ( void )build
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];

	// Modifica a aparência do slider de som
	UIImage *hThumbImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"perolavolume", @"png" ) ];
	[hVolumeSlider setThumbImage: hThumbImg forState: UIControlStateNormal];
	[hVolumeSlider setThumbImage: hThumbImg forState: UIControlStateDisabled];
	
	UIImage *hThumbPressedImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"perolavolume", @"png" ) ];
	[hVolumeSlider setThumbImage: hThumbPressedImg forState: UIControlStateHighlighted];
	[hVolumeSlider setThumbImage: hThumbPressedImg forState: UIControlStateSelected];
	
	UIImage *hMaxAux = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"volume_full", @"png" ) ];
	UIImage *hMaxImg = [hMaxAux stretchableImageWithLeftCapWidth: OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH topCapHeight: 0];
	
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateNormal];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateDisabled];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateHighlighted];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateSelected];
	
	[hMaxImg release];
	
	UIImage *hMinAux = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"volume_empty", @"png" ) ];
	UIImage *hMinImg = [hMinAux stretchableImageWithLeftCapWidth: OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH topCapHeight: 0];
	
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateNormal];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateDisabled];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateHighlighted];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateSelected];
	
	[hMinImg release];
	
	[hVolumeSlider setContinuous: YES];
	
	
	// Se o device não possui vibração, não exibiremos os respectivos controles e temos que centralizar os controles de volume
	if( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) hasVibration] )
	{
		[hVibration setHidden:NO];
		[hBtOn setSelected:YES];
		[hBtOff setSelected:NO];		
	}
	else
	{
		[hVibration setHidden:YES];
		[hBtOn setHidden:YES];
		[hBtOff setHidden:YES];
	}
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}
/*==============================================================================================
 
 MENSAGEM setVibrationStatus
 Determina o estado das checkboxes de vribração.
 
 ==============================================================================================*/

- ( void ) setVibrationStatus:( bool )vibrationOn
{
		[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) setVibrationStatus: vibrationOn];
		
		if( [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) isVibrationOn] )
			[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) vibrate: VIBRATION_TIME_DEFAULT ];	
}
/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM onVolumeChanged
	Indica que o usuário alterou o volume dos sons da aplicação.

==============================================================================================*/

- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) setVolume: [hVolumeSlider value]];
}



// Trata o evento de clique no On
- ( IBAction ) onButtonOn
{	
	[hBtOn setSelected:YES];
	[hBtOff setSelected:NO];
	[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) setVibrationStatus: true];
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}

// Trata o evento de clique no Off
- ( IBAction ) onButtonOff
{
	[hBtOn setSelected:NO];
	[hBtOff setSelected:YES];
	[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) setVibrationStatus: false];
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onExit
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_MAIN_MENU];
}



/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 Método chamado quando a view se torna a view principal da aplicação.
 
 ==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	if( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) isVibrationOn] )
	{
		[hBtOn setSelected:YES];
		[hBtOff setSelected:NO];		
	}
	else
	{
		[hBtOn setSelected:NO];
		[hBtOff setSelected:YES];	
	}
	
	[ hVolumeSlider setValue:[ ( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getAudioVolume ] ];
}


// Fim da implementação da classe
@end
