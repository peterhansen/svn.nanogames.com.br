/*
 *  OptionsView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef OPTIONS_VIEW_H
#define OPTIONS_VIEW_H 1

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface OptionsView : UIView
{
	@private
		// Imagem de background
	IBOutlet UIImageView *hBkg, *hVibration;
	
		// Botões de seleção do tipo de controle
		IBOutlet UIButton *hBtOn, *hBtOff;
	
		// Barra que controla os volume dos sons
		IBOutlet UISlider* hVolumeSlider;
}

// Indica que o usuário alterou o volume dos sons da aplicação
- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;

- ( void ) setVibrationStatus:( bool )vibrationOn;

// Trata o evento de clique no On
- ( IBAction ) onButtonOn;

// Trata o evento de clique no Off
- ( IBAction ) onButtonOff;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;

@end

#endif
