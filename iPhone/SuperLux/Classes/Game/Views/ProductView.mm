#include "ProductView.h"

#include "Config.h"
#include "Macros.h"
#include "PepsiCaeBienoAppDelegate.h"

#if DEBUG
	#include "Tests.h"
#endif

// Início da implementção da classe
@implementation ProductView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

//- ( void )build
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	[hScroll setContentSize:CGSizeMake( 60 * 9, 104)];
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}


/*==============================================================================================

MENSAGEM onBecomeCurrentScreen
	Método chamado quando a view se torna a view principal da aplicação.

==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	[hScroll setContentOffset:(CGPointMake( 60 * 5 , 0 ))];	
	[hScroll setContentOffset:(CGPointMake( (60 * 9 - 320) / 2 , 0 )) animated: YES];
	[self onBtPressed:hBt4];	
}

-( IBAction ) onBtPressed: (UIButton*) hButton
{
	int index = 0;
	
	if (hButton == hBt0)
	{
		index = 0;
	}
	else
	if (hButton == hBt1)
	{
		index = 1;
	}
	else
	if (hButton == hBt2)
	{
		index = 2;
	}
	else
	if (hButton == hBt3)
	{
		index = 3;					
	}
	else
	if (hButton == hBt4)
	{
	index = 4;
	}
	else
	if (hButton == hBt5)
	{
		index = 5;
	}
	else
	if (hButton == hBt6)
	{
		index = 6;
	}
	else
	if (hButton == hBt7)
	{
		index = 7;
	}
	else
	if (hButton == hBt8)
	{
		index = 8;
	}
	
	UIImageView* images[] = { hImg0, hImg1, hImg2, hImg3, hImg4, hImg5, hImg6, hImg7, hImg8 };
	for ( int i = 0; i < PRODUCTS_TOTAL; ++i )
		images[ i ].hidden = ( i == index ? NO : YES );
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	hProductText.text = CHAR_ARRAY_TO_NSSTRING( [ ( ( PepsiCaeBienoAppDelegate*) APP_DELEGATE ) getText:TEXT_INDEX_PRODUCT_0 + index ] );	
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onExit
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

// Fim da implementação da classe
@end

