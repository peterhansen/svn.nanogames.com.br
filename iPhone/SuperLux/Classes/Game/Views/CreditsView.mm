#import "CreditsView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"

#define WEBSITE_BUTTON_NANO_GAMES	0
#define WEBSITE_BUTTON_LUX		1

// Extensão da classe para declarar métodos privados
@interface CreditsView ()

- ( void ) showPopUp;

@end

// Início da implementção da classe
@implementation CreditsView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

//- ( void )build
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Configura o tamanho exato da fonte, já que não podemos fazê-lo pelo Interface Builder de m****
//	hLinkNanoGames.font = [UIFont systemFontOfSize: 10.0f];
//	hLinkPepsi.font = [UIFont systemFontOfSize: 10.0f];
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM onGoToNanoGamesURL
	Exibe o popup de alerta.

==============================================================================================*/

- ( void ) showPopUp
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;

	UIAlertView *hPopUp = [[UIAlertView alloc] initWithTitle: CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_TITLE] ) message: CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_MSG_GOTOURL] ) delegate: self cancelButtonTitle: nil otherButtonTitles: CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_YES] ), CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_NO] ), nil];
	[hPopUp setCancelButtonIndex: 1];
	[hPopUp show];
	[hPopUp release];
}

/*==============================================================================================

MENSAGEM onGoToNanoGamesURL
	Abandona a aplicação e abre o navegador no site da nanogames.

==============================================================================================*/

- ( IBAction ) onGoToNanoGamesURL
{
	webButton = WEBSITE_BUTTON_NANO_GAMES;
	[self showPopUp];
}

/*==============================================================================================

MENSAGEM onGoToPepsiURL
	Abandona a aplicação e abre o navegador no site da pepsi.

==============================================================================================*/

- ( IBAction ) onGoToLuxURL
{
	webButton = WEBSITE_BUTTON_LUX;
	[self showPopUp];
}

/*==============================================================================================

MENSAGEM alertView clickedButtonAtIndex
	Chamada quando o usuário pressiona um dos botões do menu.

==============================================================================================*/

- ( void ) alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	if( buttonIndex == 0 )
	{
		if( webButton == WEBSITE_BUTTON_NANO_GAMES )
			[[UIApplication sharedApplication] openURL: [NSURL URLWithString: @"http://www.nanogames.com.br"]];
		else // if( webButton == WEBSITE_BUTTON_PEPSI )
			[[UIApplication sharedApplication] openURL: [NSURL URLWithString: @"http://www.lux.com.br"]];
	}
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onExit
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

// Fim da implementção da classe
@end
