#include "RecordsView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"

// Define a velocidade ada animação de piscar o label
#define RECORDS_VIEW_BLINK_ANIM_RATE 0.388f

// Extensão da classe para declarar métodos privados
@interface RecordsView ()

- ( void ) blinkRecord;

@end

// Início da implementção da classe
@implementation RecordsView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

//- ( void )build
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	blinkingRecord = -1;;
	blinkingController = 0.0f;

	// Determina o intervalo máximo de atualização de tela
	animationInterval = DEFAULT_ANIMATION_INTERVAL;
	hAnimationTimer = NULL;
}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )dealloc
//{
//	// Repassa a mensagem
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM resume
	Reinicia o processamento da view.

==============================================================================================*/

- ( void ) resume
{
	[self suspend];
	hAnimationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector( blinkRecord ) userInfo:NULL repeats:YES];
}

/*==============================================================================================

MENSAGEM suspend
	Suspende o processamento da view.

==============================================================================================*/

- ( void ) suspend
{
	if( hAnimationTimer )
	{
		[hAnimationTimer invalidate];
		
		// Não precisamos utilizar KILL( hAnimationTimer ), pois [hAnimationTimer invalidate] chama
		// [hAnimationTimer release] automaticamente
		hAnimationTimer = NULL;
	}
}

/*==============================================================================================

MENSAGEM setRecords
	Indica os recordes que devem ser exibidos.

==============================================================================================*/

#define SET_RECORDS_BUFFER_LEN 128

- ( void ) setRecords: ( int32* )pRecords
{
	char buffer[ SET_RECORDS_BUFFER_LEN ];
	snprintf( buffer, SET_RECORDS_BUFFER_LEN, "%d",  static_cast< int32 >( MAX_POINTS ) );
	
	uint8 maxPointsLen = strlen( buffer );
	int32 firstPointsLabelTag = [hFirstPointsLabel tag];

	for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		// 0* => Garante que, no mínimo, pRecords[i] será impresso com 'maxPointsLen' dígitos, preenchendo com números menores com 0s à esquerda
		snprintf( buffer, SET_RECORDS_BUFFER_LEN, "%0*d", maxPointsLen, pRecords[i] > MAX_POINTS ? static_cast< int32 >( MAX_POINTS ) : pRecords[i] );
		
		UILabel* hLabel = ( UILabel* ) [self viewWithTag: firstPointsLabelTag + i];
		hLabel.text = CHAR_ARRAY_TO_NSSTRING( buffer );
	}
}

#undef SET_RECORDS_BUFFER_LEN

/*==============================================================================================

MENSAGEM setBlinkingRecord
	Determina o recorde que deve piscar.

==============================================================================================*/

- ( void ) setBlinkingRecord: ( int8 )index
{
	blinkingRecord = index;
	blinkingController = 0.0f;
	
	int32 firstPointsLabelTag = [hFirstPointsLabel tag];

	for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
		(( UILabel* )[self viewWithTag: firstPointsLabelTag + i] ).hidden = NO;
}

/*==============================================================================================

MENSAGEM blinkRecord
	Faz um recorde piscar.

==============================================================================================*/

- ( void ) blinkRecord
{
	if( blinkingRecord >= 0 )
	{
		blinkingController += animationInterval;
		
		if( blinkingController > RECORDS_VIEW_BLINK_ANIM_RATE )
		{
			blinkingController = fmodf( blinkingController, RECORDS_VIEW_BLINK_ANIM_RATE );

			int32 firstPointsLabelTag = [hFirstPointsLabel tag];
			
			UILabel* hBlinkingLabel = ( UILabel* )[self viewWithTag: firstPointsLabelTag + blinkingRecord];
			hBlinkingLabel.hidden = !hBlinkingLabel.hidden;
		}
	}
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onExit
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

// Fim da implementção da classe
@end
