#include "TipsView.h"

#include "Config.h"
#include "Macros.h"
#include "PepsiCaeBienoAppDelegate.h"

#include <string>

#if DEBUG
	#include "Tests.h"
#endif

// Início da implementção da classe
@implementation TipsView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder:
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM build
	Inicializa a view.

==============================================================================================*/

//- ( void )build
//{
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}


/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 Método chamado quando a view se torna a view principal da aplicação.
 
 ==============================================================================================*/

- ( void ) onBecomeCurrentScreen
{	
	std::string tipsText[] = { "Não fique muito tempo debaixo do chuveiro. Além de não fazer bem à sua pele (especialmente se a água estiver muito quente), não é nada ecológico. 10 a 15 minutos são suficientes para sentir-se relaxada e em paz com o meio ambiente.",
		"Durante o verão, o sol, o sal e a areia acabam com a sua pele. Além de utilizar protetor solar todos os dias, evite uma esfregação rigorosa com bucha. Coloque uma pequena quantidade de sabonete líquido Lux na palma da mão, esfregue-as para formar espuma e espalhe gentilmente pelo corpo. Relaxe, curta o momento e os resultados de uma pele sedutora.",
		"Resista à tentação de tomar banhos super quentes no inverno. Escolha uma toalha grande e felpuda, regule a temperatura morna no chuveiro e capriche no sabonete líquido Lux. O toque aveludado do sabonete garantirá muito conforto e a sensação de toque macio de veludo por muito mais tempo.",
		"Um bom banho seguido de uma noite de sono (pelo menos 8 horas) garante uma pele renovada no dia seguinte. Tome banho com o sabonete líquido Lux de sua fragrância favorita, coloque lençóis de cor clara na sua cama e acenda uma luz indireta, como um abajur. Se quiser, acenda um incenso ou velas. Você irá dormir com os anjos.",
		"Massageie sua pele pelo menos uma vez por semana. Utilize o sabonete líquido Lux durante o banho e, após sair massageie seu rosto e corpo em movimentos circulares utilizando as pontas dos dedos. Isso ativará a circulação deixando-a mais calma e com uma aparência saudável.",
		"Sabonetes líquidos Lux são suaves, indicados para todos os tipos de pele (especialmente as secas) e possuem agentes emolientes e umectantes, garantindo uma pele macia e sedutora. Use sem moderação!",
		"Que tal preparar um delicioso banho de imersão com sabonete líquido? Encha a banheira com água morna e acrescente algumas gotas do seu sabonete líquido Lux favorito. Misture a água para fazer espuma e curta este momento de prazer.",
		"Secas ou a vapor, as saunas são excelentes oportunidades para relaxar enquanto você cuida da beleza. Os poros dilatados devido ao calor aumentam a sudorese, que ajuda a eliminar células mortas, garantindo uma pele renovada e saudável. Um banho com água morna + sabonete líquido Lux completa a sessão de beleza.",
		"O cigarro deixa a pele amarelada, com olheiras fundas e marcas de expressão. Experimente tomar um banho relaxante com sabonete líquido Lux, quando sair, sua pele estará tão macia e perfumada que você irá querer distância de fumaça.",
		"Quer uma dica para refrescar a pele e o corpo? Água gelada! Oito copinhos diários são suficientes para acelerar a circulação sanguínea e melhorar a absorção dos nutrientes. Para garantir uma hidratação completa, tome banhos gelados com sabonete líquido Lux pela manhã. Você começará o dia com uma deliciosa sensação de pele aveludada.",
		"Sinta prazer ao ficar algum tempo diante do espelho diariamente. Vestir-se bem e manter um ritual de beleza e bem estar é direito de toda mulher.",
		"Vestir-se bem e estar na moda não é sinônimo de marcas famosas e caras, muito menos só para estrelas de TV. Criatividade e confiança fazem maravilhas pelo seu guarda-roupa e autoestima. Você também vai se sentir uma celebridade!",
		"Evite alimentos gordurosos, como frituras e alimentos industrializados. Seu organismo demoooora para digerir a gordura e você fica o resto do dia com aquela sensação de ter exagerado.",
		"Alimentos saudáveis + atividades físicas. Uma receita que funciona há séculos e não custa nadinha. Afinal, ninguém paga para andar a pé e alimentos frescos são sempre mais baratos que os industrializados.",
		"A regra é sempre assim: mini-saia + blusinha discreta / blusa com decote + saia comportada. Nada de pernas, barriga e braços expostos ao mesmo tempo. Sensual é insinuar sem mostrar tudo.",
		"Renove-se. Que tal começar um curso novo? Aquele projeto que você esqueceu no passado? O corte de cabelo que sempre quis? O concurso que você gostaria de prestar? A viagem dos sonhos? Sempre é tempo de mudar.",
		"Quer ser sedutora e conquistar alguém? Primeiro passo, acredite no seu potencial, valorize suas qualidades e desenvolva sua autoestima. Ao invés de ficar remoendo seus defeitos, visualize suas qualidades, vitórias e conquistas durante sua vida. Mentalizar positivo é tudo!",
		"Está solteira? Não saia atirando para todos homens à sua volta: colegas de trabalho, gatos na balada, no restaurante, porteiro do prédio ou manobrista do estacionamento. Produza-se e saia para se divertir que a pessoa certa aparece.",
		"A elegância é, antes de tudo, um conjunto harmonioso de gestos, atitudes, expressões, palavras, tom de voz, postura física, bom gosto e bom senso. Precisa dizer mais?",
		"Quer prolongar o bronzeado? Já dizia a vovó: \"Come cenoura, minha filha\". O alimento é rico em betacaroteno, um antioxidante pró-vitamina A que age recuperando a pele e auxilia na formação de melanina, que garante a manutenção da cor do verão. Ahhh, é super saudável e não engorda nem um pouquinho.",
		"Cuide do seu rosto diariamente. Nunca durma sem retirar a maquiagem. Os cosméticos tapam os poros e não permitem que a derme “respire”, o resultado é uma pele oleosa e envelhecimento acelerado. Receitinha básica: sabonete neutro + demaquilante + hidratante. Religiosamente, tá?",
		"Consulte um dermatologista regularmente. O profissional irá diagnosticar sua pele, oferecer opções de tratamento, prevenir eventuais danos e problemas na pele. Além de identificar seu tipo de pele, indicar hidratantes adequados e ensinar como limpar, hidratar e tonificar."};
	
	// getText() retornará o texto já no idioma correto
	//hTipsText.text = CHAR_ARRAY_TO_NSSTRING( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE) getText: TEXT_INDEX_HELP] );]

	hTipsText.text = CHAR_ARRAY_TO_NSSTRING( tipsText[Random::GetInt(0,21)].c_str() );
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_BEAUTY_TIPS AtIndex: SOUND_INDEX_BEAUTY_TIPS Looping: true];	
}

/*==============================================================================================

MENSAGEM onExit
	Chamado quando o usuário deseja sair desta tela.

==============================================================================================*/

- ( IBAction )onNextLevel
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_NEXT_LEVEL];
}

// Fim da implementação da classe
@end

