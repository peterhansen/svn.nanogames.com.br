/*
 *  LanguageView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/28/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef LANGUAGE_VIEW_H
#define LANGUAGE_VIEW_H 1

#import <UIKit/UIKit.h>
#import <UIKit/UIAlert.h>

@interface LanguageView : UIView <UIAlertViewDelegate>
{
	@private
		// Botões de seleção de idioma
		IBOutlet UIButton *hPortuguese, *hSpanish;
}

// Chamado quando o usuário seleciona o idioma desejado
- ( IBAction )onChooseLanguage: ( UIButton* )hButton;

@end

#endif
