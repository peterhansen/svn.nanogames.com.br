#include "LanguageView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"

// Início da implementção da classe
@implementation LanguageView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )dealloc
//{
//	// Repassa a mensagem
//    [super dealloc];
//}

/*==============================================================================================

MENSAGEM onChooseLanguage
	Chamado quando o usuário seleciona o idioma desejado.

==============================================================================================*/

- ( IBAction )onChooseLanguage: ( UIButton* )hButton;
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )APP_DELEGATE;

	if( hButton == hPortuguese )
		[hApplication setLanguage: LANGUAGE_INDEX_PORTUGUESE];
	else
		[hApplication setLanguage: LANGUAGE_INDEX_SPANISH];

	// Verifica se suportamos a versão do iPhoneOS
	if( ![hApplication isSupportedOS] || ![hApplication isSupportedOSVersion] )
	{
		UIAlertView *hPopUp = [[UIAlertView alloc] initWithTitle: CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_TITLE] ) message: CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_MSG_NO_OS_SUPPORT] ) delegate: self cancelButtonTitle: nil otherButtonTitles: CHAR_ARRAY_TO_NSSTRING( [hApplication getText: TEXT_INDEX_POPUP_EXIT] ), nil];
		[hPopUp setCancelButtonIndex: 0];
		[hPopUp show];
		[hPopUp release];
	}
	else
	{
		[hApplication performTransitionToView: VIEW_INDEX_LOADING];
	}	
}
	
/*==============================================================================================

MENSAGEM alertView:clickedButtonAtIndex:
	Chamada quando o usuário responde ao popup.

==============================================================================================*/

- ( void ) alertView:( UIAlertView* )hAlertView clickedButtonAtIndex:( NSInteger )buttonIndex
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE) quit: ERROR_UNSUPPORTED_OS];
}

// Fim da implementção da classe
@end
