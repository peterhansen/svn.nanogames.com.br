//
//  PlayMenuController.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayMenuController : UIViewController
{
}

// Mensagem chamada quando o jogador seleciona a opção "New Game"
-( IBAction )onNewGame;

// Mensagem chamada quando o jogador seleciona a opção "Load Game"
-( IBAction )onLoadGame;

// Mensagem chamada quando o jogador seleciona a opção "Back"
-( IBAction )onBack;

@end
