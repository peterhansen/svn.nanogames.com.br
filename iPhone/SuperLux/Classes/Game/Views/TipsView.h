/*
 *  TipsView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TIPS_VIEW_H
#define TIPS_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface TipsView : UIView
{
	@private
		// Caixa do texto de ajuda
		IBOutlet UITextView* hTipsText;
	
		// Imagem de background
		IBOutlet UIImageView *hBkg;
	
		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hBackImg;
}

// Determina o idioma da interface
- ( void ) onBecomeCurrentScreen;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onNextLevel;

@end

#endif
