/*
 *  HelpView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef HELP_VIEW_H
#define HELP_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface HelpView : UIView
{
	@private
		// Caixa do texto de ajuda
		IBOutlet UITextView* hHelpText;
	
		// Imagem de background
		IBOutlet UIImageView *hBackImg;

		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hTxtBtBack, *hViewTitle;
		
}

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;


@end

#endif
