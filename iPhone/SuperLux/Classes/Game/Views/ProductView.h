/*
 *  ProductView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PRODUCT_VIEW_H
#define PRODUCT_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface ProductView : UIView
{
	@private
		// Caixa do texto de ajuda
		IBOutlet UITextView* hProductText;
	
		// Imagem de background
		IBOutlet UIImageView *hBackImg;
		
		// Imagens dos xampuzoes
		IBOutlet UIImageView *hImg0, *hImg1, *hImg2, *hImg3, *hImg4, *hImg5, *hImg6, *hImg7, *hImg8;
		
		// Objetos que devem ser modificados de acordo com o idioma selecionado
		IBOutlet UIImageView *hTxtBtBack, *hViewTitle;
	
		IBOutlet UIScrollView *hScroll;
		
		IBOutlet UIButton *hBt0, *hBt1, *hBt2, *hBt3, *hBt4, *hBt5, *hBt6, *hBt7, *hBt8;
}

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

// Chamado quando o usuário deseja sair desta tela
- ( IBAction )onExit;

-( IBAction ) onBtPressed: (UIButton*) hButton;


@end

#endif
