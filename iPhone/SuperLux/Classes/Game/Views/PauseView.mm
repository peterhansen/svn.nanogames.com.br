#include "PauseView.h"

#include "Config.h"
#include "PepsiCaeBienoAppDelegate.h"
#include <QuartzCore/CAAnimation.h>
#include "GameScreen.h"

// Configurações da barra de volume
#define OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH 9

#define BKG_MAX_ALPHA 0.4f

@interface PauseView( Private )

-( void ) animationDidStop: (NSString*)animationId finished: (NSNumber*)finished context: (void*) context;

@end

// Início da implementção da classe
@implementation PauseView

/*==============================================================================================

MENSAGEM initWithFrame:
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/
//
//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		// Inicializa o objeto
//		if( ![self buildPauseView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

///*==============================================================================================
//
//MENSAGEM initWithCoder:
//	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.
//
//==============================================================================================*/
//
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self buildPauseView] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}
//
///*==============================================================================================
//
//MENSAGEM build
//	Inicializa a view.
//
//==============================================================================================*/
//
//- ( bool )buildPauseView
//{
//	return true;
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}

/*==============================================================================================
 
 MENSAGEM awakeFromNib
 Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
 links gerados pelo Interface Builder através dos IBOutlets.
 
 ==============================================================================================*/

- ( void )awakeFromNib
{
	[super awakeFromNib];
	
	// Modifica a aparência do slider de som
	UIImage *hThumbImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"perolavolume", @"png" ) ];
	[hVolumeSlider setThumbImage: hThumbImg forState: UIControlStateNormal];
	[hVolumeSlider setThumbImage: hThumbImg forState: UIControlStateDisabled];
	
	UIImage *hThumbPressedImg = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"perolavolume", @"png" ) ];
	[hVolumeSlider setThumbImage: hThumbPressedImg forState: UIControlStateHighlighted];
	[hVolumeSlider setThumbImage: hThumbPressedImg forState: UIControlStateSelected];
	
	UIImage *hMaxAux = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"volume_full", @"png" ) ];
	UIImage *hMaxImg = [hMaxAux stretchableImageWithLeftCapWidth: OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH topCapHeight: 0];
	
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateNormal];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateDisabled];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateHighlighted];
	[hVolumeSlider setMaximumTrackImage: hMaxImg forState: UIControlStateSelected];
	
	[hMaxImg release];
	
	UIImage *hMinAux = [UIImage imageWithContentsOfFile: Utils::GetPathForResource( @"volume_empty", @"png" ) ];
	UIImage *hMinImg = [hMinAux stretchableImageWithLeftCapWidth: OPTIONS_VIEW_SLIDER_LEFT_CAP_WIDTH topCapHeight: 0];
	
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateNormal];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateDisabled];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateHighlighted];
	[hVolumeSlider setMinimumTrackImage: hMinImg forState: UIControlStateSelected];
	
	[hMinImg release];
	
	[hVolumeSlider setContinuous: YES];

	// Se o device não possui vibração, não exibiremos os respectivos controles e temos que centralizar os controles de volume
	if( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) hasVibration] )
	{
		[hVibration setHidden:NO];
		[hBtOn setSelected:YES];
		[hBtOff setSelected:NO];		
	}
	else
	{
		[hVibration setHidden:YES];
		[hBtOn setHidden:YES];
		[hBtOff setHidden:YES];
	}	
	
	[ hFader setUserInteractionEnabled:NO ];
	[ hFader setBackgroundColor: [ UIColor blackColor ] ];	

	hBtQuitNo.hidden = YES;
	hBtQuitYes.hidden = YES;
	hQuit.hidden = YES;	
	hFader.hidden = YES;	
}

/*==============================================================================================
 
 MENSAGEM drawRect
 Renderiza o objeto.
 
 ==============================================================================================*/

// OBS: Descomentar se precisar personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}
/*==============================================================================================
 
 MENSAGEM setVibrationStatus
 Determina o estado das checkboxes de vribração.
 
 ==============================================================================================*/

- ( void ) setVibrationStatus:( bool )vibrationOn
{
	[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) setVibrationStatus: vibrationOn];
	
	if( [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) isVibrationOn] )
		[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) vibrate: VIBRATION_TIME_DEFAULT ];	
}
/*==============================================================================================
 
 MENSAGEM dealloc
 Destrutor.
 
 ==============================================================================================*/

//- ( void )dealloc
//{
//    [super dealloc];
//}


/*==============================================================================================
 
 MENSAGEM onVolumeChanged
 Indica que o usuário alterou o volume dos sons da aplicação.
 
 ==============================================================================================*/

- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) setVolume: [hVolumeSlider value]];
}


// Trata o evento de clique no On
- ( IBAction ) onButtonOn
{	
	[hBtOn setSelected:YES];
	[hBtOff setSelected:NO];
	[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) setVibrationStatus: true];
	
	if( [(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) isVibrationOn] )
		[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) vibrate: VIBRATION_TIME_DEFAULT ];		
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}

// Trata o evento de clique no Off
- ( IBAction ) onButtonOff
{
	[hBtOn setSelected:NO];
	[hBtOff setSelected:YES];
	[(( PepsiCaeBienoAppDelegate* ) APP_DELEGATE ) setVibrationStatus: false];
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}

/*==============================================================================================

MENSAGEM onBackToGame
	Chamado quando o usuário deseja voltar para a tela de jogo.

==============================================================================================*/

- ( IBAction )onJogar
{
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) performTransitionToView: VIEW_INDEX_GAME];
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}

- ( IBAction )onMenu
{
	hBtQuitNo.hidden = NO;
	hBtQuitYes.hidden = NO;
	hQuit.hidden = NO;
	hFader.hidden = NO;	
	
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: 1.0f ];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut ];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector( animationDidStop:finished:context: )];
	
	[ hFader setAlpha: BKG_MAX_ALPHA ];
	
	[UIView commitAnimations];
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}

- ( IBAction )onQuitNo
{
	hBtQuitNo.hidden = YES;
	hBtQuitYes.hidden = YES;
	hQuit.hidden = YES;	
	//hFader.hidden = YES;	
	
	[UIView beginAnimations: nil context: nil];
	[UIView setAnimationDuration: 1.0f ];
	[UIView setAnimationCurve: UIViewAnimationCurveEaseInOut ];
	[UIView setAnimationDelegate: self];
	[UIView setAnimationDidStopSelector: @selector( animationDidStop:finished:context: )];
	
	[ hFader setAlpha: 0.0f ];
	
	[UIView commitAnimations];
	
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}


-( void ) animationDidStop: (NSString*)animationId finished: (NSNumber*)finished context: (void*) context
{
	
}


/*==============================================================================================

MENSAGEM onExitGame
	Chamado quando o usuário deseja voltar para o menu principal.

==============================================================================================*/

- ( IBAction )onQuitYes
{
	GameScreen *pG = [( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) pGameScreen ];
	[( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) onGameOver: pG->getScore() ];
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
}


/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 Método chamado quando a view se torna a view principal da aplicação.
 
 ==============================================================================================*/

-( void ) onBecomeCurrentScreen
{
	hBtQuitNo.hidden = YES;
	hBtQuitYes.hidden = YES;
	hQuit.hidden = YES;	
	hFader.hidden = YES;	
	
	hFader.alpha = 0.0f;
	
	if( [(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) isVibrationOn] )
	{
		[hBtOn setSelected:YES];
		[hBtOff setSelected:NO];		
	}
	else
	{
		[hBtOn setSelected:NO];
		[hBtOff setSelected:YES];		
	}
	[ hVolumeSlider setValue:[ ( ( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) getAudioVolume ] ];
}


// Fim da implementção da classe
@end

