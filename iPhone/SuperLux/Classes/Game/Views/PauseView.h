/*
 *  PauseView.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/30/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef PAUSE_VIEW_H
#define PAUSE_VIEW_H

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface PauseView : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkg, *hQuit, *hVibration;

		// Botões de seleção do tipo de controle
		IBOutlet UIButton *hBtOn, *hBtOff, *hBtQuitYes, *hBtQuitNo;
		
		// Barra que controla os volume dos sons
		IBOutlet UISlider* hVolumeSlider;
	
		IBOutlet UIImageView *hFader;
}


// Indica que o usuário alterou o volume dos sons da aplicação
- ( IBAction ) onVolumeChanged:( UISlider* )hSlider;

- ( void ) setVibrationStatus:( bool )vibrationOn;

// Chamado quando o usuário modifica o tipo de controle da aplicação
//- ( IBAction )onChangeControl: ( UIButton* )hButton;

// Trata o evento de clique no On
- ( IBAction ) onButtonOn;

// Trata o evento de clique no Off
- ( IBAction ) onButtonOff;

// Chamado quando o usuário deseja voltar para a tela de jogo
- ( IBAction )onJogar;

// Chamado quando o usuário deseja voltar para o menu principal
- ( IBAction )onQuitYes;

- ( IBAction )onMenu;
- ( IBAction )onQuitNo;

@end

#endif

