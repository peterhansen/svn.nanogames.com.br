//
//  MainMenuView.mm
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import "MainMenuView.h"

#include "PepsiCaeBienoAppDelegate.h"
#include "Config.h"

#if TARGET_OS_IPHONE

@interface MainMenuView ()

// Impede o flickering no final da exibição do vídeo
- ( void ) playBkg;

@end

#endif

// Início da implementação da classe
@implementation MainMenuView

/*==============================================================================================

MENSAGEM initWithFrame
	Construtor chamado quando carregamos a view via código.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( id )initWithFrame:( CGRect )frame
//{
//    if( ( self = [super initWithFrame:frame] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM initWithCoder
	Construtor chamado quando carregamos a view através de um arquivo NIB/XIB.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( id )initWithCoder:( NSCoder* )decoder
//{
//	if( ( self = [super initWithCoder:decoder] ) )
//	{
//		if( ![self build] )
//			goto Error;
//    }
//    return self;
//	
//	// Tratamento de erros durante a inicialização
//	Error:
//		[self release];
//		return NULL;
//}

/*==============================================================================================

MENSAGEM awakeFromNib
	Inicializa o objeto depois que este acabou de ser carregado. Só aqui podemos utilizar os
links gerados pelo Interface Builder através dos IBOutlets.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )awakeFromNib
//{
//	[super awakeFromNib];
//	
//	// Configurations
//}

/*==============================================================================================

MENSAGEM drawRect
	Renderiza o objeto.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )drawRect:( CGRect )rect
//{
//}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS: Descomentar se for necessçrio personalizar
//- ( void )dealloc
//{
//    [super dealloc];
//}



/*==============================================================================================

MENSAGEM onPlay
	Mensagem chamada quando o jogador seleciona a opção "Play".

==============================================================================================*/

-( IBAction )onPlay
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_NEW_GAME];
}

/*==============================================================================================

MENSAGEM onRanking
	Mensagem chamada quando o jogador seleciona a opção "Ranking".

==============================================================================================*/

-( IBAction )onRanking
{
	PepsiCaeBienoAppDelegate* hApplication = ( PepsiCaeBienoAppDelegate* )[ApplicationManager GetInstance];
	
	[[hApplication hRecordsView] setBlinkingRecord: -1];

	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	
	[hApplication performTransitionToView: VIEW_INDEX_RECORDS];
}

/*==============================================================================================

MENSAGEM onOptions
	Mensagem chamada quando o jogador seleciona a opção "Options".

==============================================================================================*/

-( IBAction )onOptions
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_OPTIONS];
}

/*==============================================================================================

MENSAGEM onHelp
	Mensagem chamada quando o jogador seleciona a opção "Help".

==============================================================================================*/

-( IBAction )onHelp
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_HELP];
}

/*==============================================================================================
 
 MENSAGEM onCredits
 Mensagem chamada quando o jogador seleciona a opção "Credits".
 
 ==============================================================================================*/

-( IBAction )onCredits
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_CREDITS];
}


/*==============================================================================================
 
 MENSAGEM onProduct
 Mensagem chamada quando o jogador seleciona a opção "Product".
 
 ==============================================================================================*/

-( IBAction )onProduct
{
	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_CLICK AtIndex: SOUND_INDEX_CLICK Looping: false];
	[[ApplicationManager GetInstance]performTransitionToView: VIEW_INDEX_PRODUCT];
}


/*==============================================================================================
 
 MENSAGEM onBecomeCurrentScreen
 Método chamado quando a view se torna a view principal da aplicação.
 
 ==============================================================================================*/
-( void ) onBecomeCurrentScreen {
	[NSTimer scheduledTimerWithTimeInterval: 0.45f target:self selector:@selector( playBkg ) userInfo:NULL repeats:NO];
//	[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
}


-( void ) playBkg {
	if ( ![(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) isPlayingAudioWithIndex:SOUND_INDEX_SPLASH ] )
		[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_INDEX_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
}


// Fim da implementação da classe
@end
