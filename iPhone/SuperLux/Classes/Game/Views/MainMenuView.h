//
//  MainMenuView.h
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 3/2/09.
//  Copyright 2009 Nano Games. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "NanoTypes.h" //uint8

@interface MainMenuView : UIView
{
	@private
		// Imagem de background
		IBOutlet UIImageView *hBkg;
	
		// Imagens que representam os botões
		IBOutlet UIImageView *hPlay;
}


// Mensagem chamada quando o jogador seleciona a opção "Play"
-( IBAction )onPlay;

// Mensagem chamada quando o jogador seleciona a opção "Ranking"
-( IBAction )onRanking;

// Mensagem chamada quando o jogador seleciona a opção "Options"
-( IBAction )onOptions;

// Mensagem chamada quando o jogador seleciona a opção "Help"
-( IBAction )onHelp;

// Mensagem chamada quando o jogador seleciona a opção "Credits"
-( IBAction )onCredits;

// Mensagem chamada quando o jogador seleciona a opção "Product"
-( IBAction )onProduct;

@end
