/**
 * ©2008 Nano Games.
 *
 * Created on May 1, 2008 10:53:37 PM.
 */

/** AceleraÁ„o da gravidade. */

#define BORN_MAX_Y_DIFF 30.0f
#define BORN_MAX_HALF_Y_DIFF ( BORN_MAX_Y_DIFF * 0.5f )

#include "DropEmitter.h"
#include <cmath>
#include "Exceptions.h"


DropEmitter::~DropEmitter( void ) {
	for ( int16 i = 0; i < MAX_PARTICLES; ++i ) {
		delete particles[ i ];
	}
}


DropEmitter::DropEmitter() : Object(),
	activeParticles( 0 ),
	MAX_Y_SPEED( 0.0f )
{
	memset(particles, 0, sizeof (Particle *) * MAX_PARTICLES );
	
	setSize( SCREEN_WIDTH, SCREEN_HEIGHT );

	// b = sqrt( -4 * MAX_Y * a ), onde b = velocidade inicial e a = gravidade
	MAX_Y_SPEED = sqrt( fabs( -GRAVITY_ACC * 4.0f * EXPLOSION_MAX_HEIGHT ) );


	for ( int16 i = 0; i < MAX_PARTICLES; ++i ) {
		particles[ i ] = new Particle();
		
		if ( !particles[ i ] )
			goto Error;
		
		particles[ i ]->setFruitListener( this );
	}
	return;
	
	
	// Label de tratamento de erros
	Error:
	// Já que houve um erro no construtor, a responsabilidade de deletar pMovingPiece é do chamador
		
	#if DEBUG
		throw ConstructorException( "Fruit::Fruit( int type ) : Unable To create object" );
	#else
		throw ConstructorException();
	#endif
}


bool DropEmitter::render() {
	for ( int16 i = 0; i < activeParticles; ++i ) {
		particles[ i ]->render();
	}
	
	return true;
}


bool DropEmitter::update( float delta ) {

	int16 previousActiveParticles = activeParticles;
	for ( int16 i = 0; i < activeParticles; ++i )
		particles[ i ]->update( delta );
	
	if ( previousActiveParticles != activeParticles )
		sort();
	
	return true;
}	


void DropEmitter::reset() {
	activeParticles = 0;
}	


void DropEmitter::sort() {
	int16 i = 0;
	int16 j = ( int16 ) ( MAX_PARTICLES - 1 );
	for ( ; i < MAX_PARTICLES; ++i ) {
		if ( !particles[ i ]->isVisible() ) {
			for ( ; j > i; --j ) {
				if ( particles[ j ]->isVisible() ) {
					Particle* pTemp = particles[ i ];
					particles[ i ] = particles[ j ];
					particles[ j ] = pTemp;
					break;
				}
			}
		}
	}
	
	activeParticles = j;
}


void DropEmitter::emit( int8 type, Point3f &pos ) {
	Point3f temp;
	int32 limit = MAX_PARTICLES < EMIT_PARTICLES + activeParticles ? MAX_PARTICLES : EMIT_PARTICLES + activeParticles;
	
	for ( int16 i = activeParticles; i < limit; ++i ) {
		temp.set( pos.x, pos.y - BORN_MAX_HALF_Y_DIFF + Random::GetFloat( BORN_MAX_Y_DIFF ) );
		particles[ i ]->born( type, temp );
	}
	activeParticles = ( int16 ) limit;
}


void DropEmitter::onDie() {
	--activeParticles;
}
	
