/*
 *  TimeBar.mm
 *  FruitLux
 *
 *  Created by Peter Hansen on 9/20/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#include "TimeBar.h"
#include "Exceptions.h"
#include "MathFuncs.h"

#define TIMEBAR_WIDTH 286.0f
#define TIMEBAR_HEIGHT 8.0f
#define TIMEBAR_X ( ( SCREEN_WIDTH - TIMEBAR_WIDTH ) * 0.5f )
#define TIMEBAR_Y 367.0f


TimeBar::TimeBar() : ObjectGroup( 5 ), 
	pLeft( NULL ),
	pRight( NULL ),
	pFill( NULL )
{
	pLeft = new RenderableImage( "tempofull_canto" );
	if ( !pLeft || !insertObject( pLeft ) ) {
		delete pLeft;
		goto Error;
	}
	pLeft->setPosition( 0.0f, 1.0f );
	
	pFill = new RenderableImage( "tempofull_meio" );
	if ( !pFill || !insertObject( pFill ) ) {
		delete pFill;
		goto Error;
	}
	pFill->setPosition( pLeft->getWidth(), 0.0f );
	
	
	pRight = new RenderableImage( pLeft );
	if ( !pRight || !insertObject( pRight ) ) {
		delete pRight;
		goto Error;
	}
	pRight->mirror( MIRROR_HOR );
	
	setPosition( TIMEBAR_X, TIMEBAR_Y );
	
	return;
	
	// Label de tratamento de erros
Error:
	
#if DEBUG
	throw ConstructorException( "Fruit::Fruit( int type ) : Unable To create object" );
#else
	throw ConstructorException();
#endif	
}


void TimeBar::setTime( float time, float total ) {
	time = NanoMath::clamp( time, 0, total );
	
	setSize( time * TIMEBAR_WIDTH / total, TIMEBAR_HEIGHT );

	pFill->setSize( MAX( 0.0f, getWidth() - pLeft->getWidth() - pRight->getWidth() ), pFill->getHeight() );
	pRight->setPosition( MAX( pLeft->getWidth(), getWidth() - pRight->getWidth() ) - 0.3f, 1.0f );
	
	setViewport( static_cast<int32>( TIMEBAR_X ), static_cast<int32>( TIMEBAR_Y ), static_cast<int32>( getWidth() ), static_cast<int32>( getHeight() ) );	
}


