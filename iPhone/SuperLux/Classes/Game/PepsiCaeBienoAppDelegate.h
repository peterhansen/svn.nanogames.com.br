//
//  PepsiCaeBienoAppDelegate.h
//  PepsiCaeBieno
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#ifndef GAME_APP_DELEGATE_H
#define GAME_APP_DELEGATE_H 1

#include "ApplicationManager.h"

#include "Config.h"
#include "CreditsView.h"
#include "EAGLView.h"
#include "HelpView.h"
#include "ProductView.h"
#include "TipsView.h"
#include "MainMenuView.h"
#include "OptionsView.h"
#include "PauseView.h"
#include "RecordsView.h"
#include "SplashGame.h"
#include "SplashNano.h"
#include "GameScreen.h"

#include "AudioSource.h"

// Forward Declarations
class Font;

@interface PepsiCaeBienoAppDelegate : ApplicationManager <UITabBarControllerDelegate>
{
	@private
	
		/************* TELAS DE JOGO *************/

		
		// Views dos menus
		MainMenuView* hMainMenu;
	
		HelpView* hHelpView;
		ProductView* hProductView;
		TipsView* hTipsView;
		CreditsView* hCreditsView;
		OptionsView* hOptionsView;
		PauseView* hPauseView;
		RecordsView* hRecordsView;
		GameScreen* pGameScreen;

		// View do jogo
		EAGLView* hGLView;
		
		// View do splash da nano
		SplashNano* hSplashNano;
		
		// View do splash do jogo
		SplashGame* hSplashGame;
	
		/************* DADOS DA APLICAÇÃO *************/
	
		// Sons do jogo
#if ( APP_N_SOUNDS > 0 ) && SOUND_WITH_OPEN_AL
		AudioSource* appSounds[ APP_N_SOUNDS ];
#endif

		// Textos do jogo
		char* texts[ APP_N_TEXTS ];
	
		// Fontes da aplicação
		Font* appFonts[ APP_N_FONTS ];
	
		// Recordes salvos
		int32 records[ APP_N_RECORDS ];
}

// Gera os getters e setters para as propriedades a seguir
@property ( nonatomic, readonly ) EAGLView* hGLView;
@property ( nonatomic, readonly ) RecordsView* hRecordsView;
@property ( nonatomic, readonly ) GameScreen* pGameScreen;

// Retorna um texto da aplicação
- ( const char* ) getText: ( uint16 ) textIndex;
	
// Retorna uma fonte da aplicação
- ( Font* ) getFont:( uint8 )fontIndex;

// Inicia a reprodução de um som da aplicação
- ( void ) playAudioNamed:( uint8 )audioName AtIndex:( uint8 ) audioIndex Looping:( bool )looping;

// Pára de reproduzir todos os sons
- ( void ) stopAudio;

// Pára de reproduzir o som indicado
- ( void ) stopAudioAtIndex: ( uint8 )index;

// (Des)Pausa todos os sons que estão sendo executados no momento
- ( void ) pauseAllSounds: ( bool )pause;

// Indica se está tocando um som específico
- ( bool )isPlayingAudioWithIndex:( uint8 )index;

// Indica se a aplicação está tocando algum som
- ( bool ) isPlayingAudio;

// Retorna o percentual do volume máximo utilizado na reprodução de sons
- ( float ) getAudioVolume;

// Determina o volume dos sons da aplicação
- ( void ) setVolume:( float )volume;

// Exibe a tela de fim de jogo
- ( void ) onGameOver: ( int32 )score;

// Retorna o lugar da pontuação na tabela de recordes ou um número negativo caso a jogador
// não tenha alcançado um valor que supere o menor recorde
- ( int8 ) isHighScore: ( int32 )score;

// Armazena a pontuação caso esta seja um recorde
- ( int8 ) setHighScore: ( int32 )score;

// Salva a pontuação atual caso esta seja um recorde
-( bool ) saveScoreIfRecord: ( int32 )score;

// Verifica se suportamos o sistema operacional
- ( bool )isSupportedOS;

// Verifica se suportamos a versão do sistema operacional
- ( bool )isSupportedOSVersion;

// Retorna se a versão do sistema operacional é anterior à 3.0
- ( bool )isOlderOSVersion;

#if APP_N_SOUNDS > 0

// Retira da memória os sons já carregados
-( void ) unloadSounds;

#endif

@end

#endif


