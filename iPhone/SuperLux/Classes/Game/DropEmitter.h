#ifndef DROP_EMITTER_H
#define DROP_EMITTER_H 1

#include "Particle.h"
#include "Object.h"
#include "Point3f.h"


class DropEmitter : public Object, FruitListener
{
	public:
	
	DropEmitter( void );
	
	~DropEmitter( void );
	
	void onDie( void );
	
	bool render( void );
	
	bool update( float delta );
	
	void emit( int8 type, Point3f &pos );
	
	void reset( void );
	
	float getMaxYSpeed( void ) {
		return MAX_Y_SPEED;
	}
	
	float getParticleAreaWidth() {
		return getWidth();
	}
	
	float getParticleAreaHeight() {
		return getHeight();
	}
	
	
	private:
	
	float MAX_Y_SPEED;
			
	int16 activeParticles;
	
	Particle* particles[ MAX_PARTICLES ];
	
	void sort( void );

};

#endif