/*
 *  Tests.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/6/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TEST_H
#define TEST_H 1

#if DEBUG

// Testes
#define TEST_NONE						0
#define TEST_APP_TEXTS					1
#define TEST_APP_RECORDS				2
#define TEST_CHECK_WORDS				3
#define TEST_FULL_BOARD					4
#define TEST_GAME_SCREEN				5

#define CURR_TEST	TEST_NONE
#define IS_CURR_TEST( x ) ( CURR_TEST == ( x ) )

#endif

#endif
