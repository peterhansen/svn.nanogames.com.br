//
//  Fruit.m
//  FruitLux
//
//  Created by Hugo on 8/11/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#include "Fruit.h"
#include "RenderableImage.h"
#include "ObjectGroup.h"
#include "Random.h"
#include "Color.h"
#include "ResourceManager.h"
#include "GameScreen.h"
#include "MathFuncs.h"
#include "Config.h"

#include "Exceptions.h"

// Inicializa as variáveis estáticas da classe
Texture2DHandler Fruit::textures[ TOTAL_TYPES ];
Sprite* Fruit::pStaticBrilho = NULL;

/*==============================================================================================
 
 CONSTRUTOR
 
 ==============================================================================================*/

Fruit::Fruit( GameScreen * pGS ) : ObjectGroup( 2 /* fruta e brilhinho */ ),
		pGameScreen(pGS),
		type(0)
{
	{
		pFruit = new RenderableImage();
		if( !pFruit || !insertObject( pFruit ) )
		{
			delete pFruit;
			goto Error;
		}
		
		setSize( FRUIT_WIDTH, FRUIT_HEIGHT );
		setAnchor( ANCHOR_HCENTER | ANCHOR_VCENTER );
		TextureFrame fruitTextureFrame( 0, 0, static_cast<uint32>( FRUIT_WIDTH ), static_cast<uint32>( FRUIT_HEIGHT ), 0, 0 );
		pFruit->setImageFrame( fruitTextureFrame );
		
		pBrilho = new Sprite( pStaticBrilho );
		
		if ( !pBrilho || !insertObject( pBrilho ) ) {
			delete pBrilho;
			goto Error;
		}
		pBrilho->setPosition( ( FRUIT_WIDTH - pBrilho->getWidth() ) * 0.5f, ( FRUIT_HEIGHT - pBrilho->getHeight() ) * 0.5f );
		
		setState( STATE_IDLE );
		
		return;
	}
	
	// Label de tratamento de erros
Error:
	// Já que houve um erro no construtor, a responsabilidade de deletar pMovingPiece é do chamador
	//setObject( PIECE_GROUP_MOVING_PIECE_INDEX, NULL );
	
#if DEBUG
	throw ConstructorException( "Fruit::Fruit( int type ) : Unable To create object" );
#else
	throw ConstructorException();
#endif
}

bool Fruit::LoadImages( void )
{
	char fruitPath[] = { ' ', '\0' };
	for( uint8 i = 0 ; i < TOTAL_TYPES ; ++i ) {
		fruitPath[0] = 'A' + i;
		
		Texture2DHandler hTexture = ResourceManager::GetTexture2D( fruitPath );
		if( !hTexture )
			goto Error;
		
		textures[i] = hTexture;
		
		TextureFrame frame( 0, 0, static_cast<uint32>( FRUIT_WIDTH ), static_cast<uint32>( FRUIT_HEIGHT ), 0, 0 );
		hTexture->changeFrame( 0, &frame );
	}
	
	pStaticBrilho = new Sprite( "brilho_tex", "brilho" );
	if( !pStaticBrilho )
		goto Error;
		
	return true;
	
	// Label de tratamento de erros
	Error:
		ReleaseImages();
	return false;
}

bool Fruit::ReleaseImages( void )
{
	RefCounter< Texture2D > deleter( NULL );
	for( uint8 i = 0 ; i < TOTAL_TYPES ; ++i )
	{
		textures[i] = deleter;
	}
	delete pStaticBrilho;
	return true;
}

void Fruit::reset( int8 column, int8 row, float yOffset ) {
	positionByCell( column, row - TOTAL_ROWS, yOffset );
	moveToCell( column, row, STATE_FALLING );
	setShining( false );
}

void Fruit::positionByCell( int8 column, int8 row, float yOffset ) {
	setPosition( getWidth() * column, ( getHeight() * row ) + yOffset );
	
	refreshId();
}

void Fruit::moveToCell( Point2i cell, int8 moveType ) {
	moveToCell( cell.x, cell.y, moveType );
}

void Fruit::moveToCell( int8 column, int8 row, int8 moveType ) {
	moveToCell( column, row, moveType, getHeight() );
}


void Fruit::moveToCell( int8 column, int8 row, int8 moveType, float initialYSpeed ) {
	Point3f p = Point3f( getWidth() * column, getHeight() * row );
	moveTo( p, moveType, initialYSpeed );
}

void Fruit::moveTo( Point3f destination, int8 moveType, float initialYSpeed ) {
	this->destination.set( destination );
	switch ( moveType ) {
		case STATE_SWITCHING_H:
			speed = (destination.x - getPosX()) / FRUIT_SWITCH_TIME;
			break;
			
		case STATE_FALLING_BOMB:
		case STATE_FALLING:
			if ( getState() == STATE_FALLING || getState() == STATE_FALLING_BOMB )
				break;
			
			moveTime = 0.0f;
			yStart = getPosY();
			this->initialYSpeed = initialYSpeed;
			break;
			
		case STATE_SWITCHING_V:
			speed = (destination.y - getPosY()) / FRUIT_SWITCH_TIME;
			break;
			
		case STATE_LIQUIFYING_VANISH:
		case STATE_LIQUIFYING_SOAP_1:
			moveTime = 0.0f;
			if (NanoMath::feql( destination.x, getPosX() ))
				speed = ( destination.y - getPosY() ) / LIQUIFY_MOVE_TIME;
			else
				speed = ( destination.x - getPosX() ) / LIQUIFY_MOVE_TIME;
			break;
	}
	setState( moveType );
}

int8 Fruit::getState() {
	return state;
}


int8 Fruit::getType() {
	return type;
}

bool Fruit::equals( const Fruit* f ) const{
	return f->type == type;
}

void Fruit::setType( int8 type ) {
	if ( type == TYPE_RANDOM )
		this->type = ( int8 ) Random::GetInt( 0, TOTAL_TYPES - 1 );
	else
		this->type = type;

	pFruit->setImage(textures[this->type]);

	// centraliza a imagem da fruta no grupo
	switch ( this->type ) {
		case 0:
			pFruit->setPosition( ( FRUIT_WIDTH - 37.0f ) * 0.5f, ( FRUIT_HEIGHT - 35.0f ) * 0.5f );			
		break;		
		
		case 1:
			pFruit->setPosition( ( FRUIT_WIDTH - 33.0f ) * 0.5f, ( FRUIT_HEIGHT - 33.0f ) * 0.5f );			
		break;
			
		case 2:
			pFruit->setPosition( ( FRUIT_WIDTH - 34.0f ) * 0.5f, ( FRUIT_HEIGHT - 32.0f ) * 0.5f );			
		break;
			
		case 3:
			pFruit->setPosition( ( FRUIT_WIDTH - 29.0f ) * 0.5f, ( FRUIT_HEIGHT - 36.0f ) * 0.5f );			
		break;
			
		case 4:
			pFruit->setPosition( ( FRUIT_WIDTH - 35.0f ) * 0.5f, ( FRUIT_HEIGHT - 37.0f ) * 0.5f );
		break;
	}
	
	setShining( false );
}


void Fruit::setState( int8 state ) {
	this->state = state;
	setScale( 1.0f, 1.0f );
	pFruit->setVisible( state != STATE_BOMB );

	switch ( state ) {
		case STATE_BOMB:
			setShining( true );
			break;
			
		case STATE_LIQUIFYING_SOAP_2:
			moveTime = LIQUIFY_TIME;
		case STATE_LIQUIFYING_SOAP_1:
		case STATE_LIQUIFYING_VANISH:
		default:
			pFruit->setImage( textures[ type ] );
			break;
	}
	
	// faz mais um switch, dessa vez para definir o estado de visibilidade
	switch ( state ) {
		case STATE_INVISIBLE:
			setVisible( false );
			break;
			
		default:
			pFruit->setVisible( !pBrilho->isVisible() );
			setVisible( true );
	}
}


// atualiza o sprite.getId() de acordo com a cÈlula atual, pois È ele que permite que a tela de jogo
// identifique onde a fruta est· no tabuleiro
void Fruit::refreshId() {
	this->fruit_id = static_cast<int8>( ( getPosY() / getHeight() * TOTAL_COLUMNS ) + ( getPosX() / getWidth() ) );
}


void Fruit::idle() {
	setState( pBrilho->isVisible() ? STATE_BOMB : STATE_IDLE );
}


bool Fruit::update( float delta ) {
	ObjectGroup::update( delta );
	
	switch ( state ) {
		case STATE_SWITCHING_H:
			move( speed * delta, 0.0f );
			if ( ( speed > 0.0f && getPosX() >= destination.x ) || ( speed < 0.0f && getPosX() <= destination.x ) ) {
				setPosition( destination.x, getPosY() );
				refreshId();
				idle();
				pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_MOVING );
			}
			break;
			
		case STATE_FALLING_BOMB:
		case STATE_FALLING:
			moveTime += delta;
			setPosition( getPosX(), yStart + ( initialYSpeed * moveTime ) + ( GRAVITY_ACC * ( moveTime * moveTime ) * 0.5f ) );
			if ( getPosY() >= destination.y ) {
				setPosition( getPosX(), destination.y );
				refreshId();
				
				if ( state == STATE_FALLING )
					idle();
				else
					setState( STATE_BOMB );
				
				pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_FALLING );
			}
			break;
			
		case STATE_SWITCHING_V:
			move( 0.0f, speed * delta );
			if ( ( speed > 0.0f && getPosY() >= destination.y ) || ( speed < 0.0f && getPosY() <= destination.y ) ) {
				setPosition( getPosX(), destination.y );
				refreshId();
				idle();
				pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_MOVING );
			}
			break;
			
		case STATE_LIQUIFYING_SOAP_1:
		case STATE_LIQUIFYING_VANISH:
			moveTime += delta;
			
			if ( NanoMath::feql( destination.x, getPosX() ) ) {
				move( 0.0f, speed * delta );
				
				if ( ( speed > 0 && getPosY() >= destination.y ) || ( speed < 0 && getPosY() <= destination.y ) || moveTime >= LIQUIFY_MOVE_TIME ) {
					setPosition( &destination );
					idle();
					
					if ( state == STATE_LIQUIFYING_VANISH )
						pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_LIQUIFY );
					else
						setState( STATE_LIQUIFYING_SOAP_2 );
				}
			} else {
				move( speed * delta , 0.0f );
				if ( ( speed > 0.0f && getPosX() >= destination.x ) || ( speed < 0.0f && getPosX() <= destination.x ) || moveTime >= LIQUIFY_MOVE_TIME ) {
					setPosition( &destination );
					idle();
					
					if ( state == STATE_LIQUIFYING_VANISH )
						pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_LIQUIFY );
					else
						setState( STATE_LIQUIFYING_SOAP_2 );
				}
			}
			break;
			
		case STATE_LIQUIFYING_SOAP_2:
			moveTime -= delta;
			
			if ( moveTime > 0 ) {
				float currentScale = moveTime / LIQUIFY_TIME;
				setScale( currentScale, currentScale );
				// setScale acaba dando a impressao de movimento na fruta
				float offset = ( 1.0f - currentScale ) * 0.5f;
				setPosition( destination.x + offset * FRUIT_WIDTH, 
							 destination.y + ( 1.0f - currentScale ) * FRUIT_HEIGHT );
			} else {
				pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_LIQUIFY );
			}
			break;
		
		case STATE_EXPLODING:
			pGameScreen->onSequenceEnded( fruit_id, SEQUENCE_EXPLODING );
			break;
		}
	return true;
}

bool Fruit::isIdle() {
	switch ( getState() ) {
		case STATE_IDLE:
		case STATE_BOMB:
		case STATE_INVISIBLE:
			return true;
			
		default:
			return false;
	}
}

float Fruit::getRandomInitialYSpeed() {
	return -getHeight() - Random::GetFloat(0.0f, 3.0f * getHeight() ); 
}


float Fruit::getRandomDiffYSpeed() {
	return -( getHeight() * 0.5f ) - Random::GetFloat(0.0f, getHeight() );
}

Point2i Fruit::getCell() {
	return Point2i( fruit_id % TOTAL_COLUMNS, fruit_id / TOTAL_COLUMNS );
}


void Fruit::liquify( Point3f destination, bool turnToSoap ) {
	moveTo( destination, turnToSoap ? STATE_LIQUIFYING_SOAP_1 : STATE_LIQUIFYING_VANISH, 0 );
}


void Fruit::setShining( bool active ) {
	pBrilho->setVisible( active );
	pBrilho->setAnimSequence( type, true, true );
}
