/*
 *  GameScreen.h
 *  GameScreen
 *
 *  Created by Daniel Lopes Alves on 2/9/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GAMESCREEN_H
#define GAMESCREEN_H 1

#include <vector>

#include "EventListener.h"
#include "OrthoCamera.h"
#include "Scene.h"
#include "Touch.h"
#include "Point2i.h"
#include "RenderableImage.h"
#include "DropEmitter.h"

#include "Config.h"
#include "SpriteGroup.h"
#include "Sprite.h"
#include "TimeBar.h"

// Forward Declarations
class AudioSource;
class Label;
class ScoreLabel;
class Fruit;

typedef std::vector< Fruit* > FruitSequence;
typedef std::vector< FruitSequence > FruitSequenceVec;
typedef std::vector< SpriteGroup* > SpriteGroupVec;

#define BORDER_STATE_HIDDEN 0
#define BORDER_STATE_APPEARING 1
#define BORDER_STATE_VISIBLE 2
#define BORDER_STATE_HIDING 3

// Número máximo de labels 'dinâmicos' que exibem a pontuação de cada jogada no próprio tabuleiro
#define SCORE_LABELS_MAX 5

#define TIME_MESSAGE 3.0f
#define TIME_WAIT_PIECES_FALL 1.0f

#define LIQUIFY_TOTAL 20
#define EXPLOSIONS_TOTAL 10


class GameScreen : public Scene, public EventListener
{
	public:
		// Construtor
		GameScreen( void );
	
		// Destrutor
		virtual ~GameScreen( void );
	
		// Atualiza o objeto
		virtual bool update( float timeElapsed );
	
		// Método que trata os eventos enviados pelo sistema
		virtual bool handleEvent( EventTypes evtType, EventTypesSpecific evtSpecific, const void* pParam );
	
		// Recebe os eventos do acelerômetro
		//virtual void onAccelerate( const Point3f* pRestPosition, const Point3f* pAcceleration );
	
		// Incrementa a pontuação e atualiza as variáveis e labels correspondentes
		void changeScore( int32 diff );
	
		// Inicia a reprodução de uma música do jogo
		void playGameBkgMusic( void );
	
		// Salva a pontuação atual caso esta seja um recorde
		void saveScoreIfRecord( void );
	
		// Retorna a pontuação do jogador
		int32 getScore( void ) const;	
	
		void onSequenceEnded( int16 id, int8 sequence );	
	
		// Carrega a próxima fase
		void prepareNextLevel( void );
	
		// Prepara uma fase
		void prepareLevel( int16 level );	
	
		void resetScore( void ) {
			score = 0;
			scoreShown = 0;
			updateScoreLabel();
		}
		
	// Pára de rastrear os toques atuais
	void cancelTrackedTouches( void );
	
	private:
		// Estados da partida
		enum GameState
		{
			
			STATE_NONE				= 0,
			STATE_BEGIN_LEVEL		= 1,
			STATE_PIECES_FALLING	= 2,
			STATE_PLAYING			= 3,
			STATE_LEVEL_CLEARED		= 4,
			STATE_TIME_UP			= 5,
			STATE_GAME_OVER			= 6,
			STATE_PAUSED			= 7,
			STATE_UNPAUSING			= 8,
			STATE_NEW_RECORD		= 9
		};
	
		// Eventos de input do jogo
		enum GameEvent
		{
			GAME_EVENT_SET_PIECE_ACC = 0,
			GAME_EVENT_MOVE_PIECE_LEFT,
			GAME_EVENT_MOVE_PIECE_RIGHT,
			GAME_EVENT_PAUSE
		};
	
		// Inicializa o objeto
		bool build( void );
	
		// Retorna o índice do toque no array de toques rastreados ou -1 caso este não esteja
		// sendo acompanhado
		int8 isTrackingTouch( const UITouch* hTouch );
		
		// Inicia o rastreamento do toque recebido como parâmetro, se ainda tivermos um slot para
		// fazê-lo
		int8 startTrackingTouch( const UITouch* hTouch );
	
		// Cancela o rastreamento do toque recebido como parâmetro
		void stopTrackingTouch( int8 touchIndex );
	
		// Modifica o estado da partida
		void setState( int8 state, bool hideMessage = true );
		void onStateEnded( void );

		// Trata eventos de início de toque
		void onNewTouch( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de fim de toque
		void onTouchEnded( int8 touchIndex, const Point3f* pTouchPos );
	
		// Trata eventos de movimentação de toques
		void onSingleTouchMoved( int8 touchIndex, const Point3f* pLastPos, const Point3f* pCurrPos );
	
		// Obtém o raio correspondente ao toque para que possamos testar colisão com os objetos do
		// jogo
		void getPickingRay( Ray& ray, const Point3f& p ) const;
	
		// Obtém o nível atual
		int16 getLevel( void ) const;

		// Obtém qual a fase em termos de dificuldade (o jogador pode atingir aa última
		// fase em termos de dificuldade mas continuar jogando)
		int16 getDifficultyLevel( int16 level ) const;
	
		ScoreLabel* getNextScoreLabel( void ) const;
	
		// Atualiza a pontuação que deve ser exibida pelo respectivo label
		void updateScore( float timeElapsed, bool equalize );

		// Atualiza o texto do label que exibe a pontuação do jogador
		void updateScoreLabel( void );
	
		// Atualiza o texto do label que exibe a fase atual
		void updateLevelLabel( void );
	
		// Retorna a pontuação necessária para passar da fase
		int16 getLevelScoreNeeded( int16 lv ) const;
	
		// Indica se a peça que está caindo pode se movimentar
		bool canMove( void ) const;

		// Coloca ou retira o jogo do estado de GAME_STATE_PAUSED
		void pause( bool paused );

		void pressPauseButton( int8 touchIndex );
		void unpressPauseButton( void );
	
		Point2i getCellAt( float x, float y );
		
		void setCursorCell( float column, float row ) ;
	
		void setCursorCell( Point2i * cell );
	
		void moveCursorCell( float columns, float rows );

		bool isAdjacentToCursor( float column, float row ) ;
	
		bool isAdjacentToCursor( Point2i * cell );

		void checkAndMoveCursorCell( float columns, float rows );

		void stopScreenVibration();
	
		void changeTime( float diff );

		void showMessage( const char * text, bool big = true );
	
		float borderTime;
	
		uint8 borderState;
	
		void setBorderState( uint8 state, bool big = true );
	
		// Indica que a aplicação recebeu um evento de suspend. Quando a aplicação
		// chama este método da cena, significa que a cena já parou de ser atualizada.
		// Os eventos de update serão retomados após a próxima chamada a resume()
		virtual void suspend( void );

		// Indica que a aplicação irá resumir sua execução. Quando a aplicação
		// chama este método da cena, significa que a cena voltará ser atualizada
		// através de eventos de update
		virtual void resume( void );

		// Frutinhas da tela
		Fruit * fruits[ TOTAL_COLUMNS ][ TOTAL_ROWS ];
	
		// Grupo das frutas ui ui ui
		ObjectGroup * fruitGroup;
	
		int8 fruitSequence;
		int8 cursorState;
		int8 comboCounter;
	
		int8 lives;
	
		// Texturas das frutas
		//Texture2DHandler fruitTextures[TOTAL_TYPES];


		Point2i lastCellMoved;
		Point2i lastMove;
	
		Point2i cursorCell;
	
		// Estado do jogo
		GameState gameState, gameLastState;
	
		// Variçveis utilizadas para o controle dos gestos realizados no touchscreen
		uint8 nActiveTouches;
		float initDistBetweenTouches;
		Touch trackedTouches[ MAX_TOUCHES ];
	
		// Indica se estamos determinando a posição de repouso do device dinamicamente
		int8 settingRestPos; 
	
		// Fase atual
		int16 level;

		// Controlador de estados de jogo que possuem duração
		float timeToNextState;
	
		// Tempo restante da fase
		float timeLeft;
	
		// Tempo de espera até inserir a nova peça (evita problemas com peças caindo durante animações)
		float timeToNextPiece;
	
		float currentTimeGoal;
		float timeRemaining;
		float timeShown;
		float totalTime;
		float timeSpeed;
		float currentTimePerFruit;
		float timeScreenVibrating;
	
		int8 shining;
	
		// Animacao de liquefacao de morangos
		SpriteGroupVec liquify;
		
		// Animacao de liquefacao de morangos
		SpriteGroupVec explosion;
		
		DropEmitter *pEmitter;
	
		// Label que exibe o tempo restante da fase
		Label* pTimeLabel;

		// Pontuação do jogador
		int32 score;
	
		int8 state;
	
		// índice do toque que está pressionando o botão de pausa
		int8 buttonPressingTouch;

		// Pontuação exibida atualmente (float para poder mudar de forma continua)
		float scoreShown;
	
		// Velocidade de atualização da pontuação
		float scoreSpeed;

		// Label que indica a pontuação e o multiplicador de pontos
		Label* pScoreLabel;
	
		// Label de fim de jogo
		Label* pMessageLabel;

		// Labels que aparecem no tabuleiro para exibir as pontuações feitas por cada palavra terminada
		ScoreLabel* scoreLabels[ SCORE_LABELS_MAX ];
	
		// Barra que controla o tempo de jogo
		TimeBar * pTimeBar;
	
		Sprite* hearts[ LIVES_MAX ];
	
		RenderableImage* pPauseBack;
	
		RenderableImage* pMessageBorder;
		RenderableImage* pMessageBorderBig;
	
		RenderableImage* pMask;
	
		// Música ambiente que está sendo tocada
		uint8 currSoundIndex;
	
		float timeToHint;
		
		/**
		 * Retorna a lista de sequÍncias ˙nicas existentes no tabuleiro.
		 * @return
		 */
		FruitSequenceVec* getAllSequences( FruitSequenceVec* pOut );
			
		bool hasSequenceAt( int8 column, int8 row );
		
		FruitSequenceVec* getSequencesAt( FruitSequenceVec* pOut, int8 column, int8 row );
		
		void setCursorState( int8 cursorState );
		
		bool hasSequenceAt( int8 column, int8 row, int8 diffColumn, int8 diffRow );
		
		/**
		 * Indica se h· pelo menos uma jogada disponÌvel para o jogador com o tabuleiro atual.
		 */
		bool hasSequence();	
		
		bool hasMoveAt( int8 column, int8 row );
		
		void updateTime( float delta, bool equalize );
		
		bool switchFruits( Point2i cell1, Point2i cell2 );
		
		Fruit* getFruit( Point2i pCell );
		
		Fruit* getFruit( int8 column, int8 row );
		
		
		bool areFruitsIdle();
		
		
		void undoLastMove();
		
		
		bool setHintVisible( bool visible );	
		
		void makeSequence( FruitSequence sequence );
		
		
		void explodeFruitArea( Fruit* pFruit );
		
		
		void explodeFruit( Fruit* pFruit );
	
	
		bool intersects( const FruitSequence * pSeq1, const FruitSequence * pSeq2 ) const;
			
		SpriteGroup* getNextLiquify();
		
		SpriteGroup* getLiquifyAt( int8 index ) { 
			return static_cast<SpriteGroup*> (liquify.at( index ) );
		}
		
		SpriteGroup* getNextExplosion();
		
		SpriteGroup* getExplosionAt( int8 index ) { 
			return static_cast<SpriteGroup*> (explosion.at( index ) );
		}
		
};


#endif
