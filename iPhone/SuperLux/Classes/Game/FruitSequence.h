/*
 *  FruitSequence.h
 *  FruitLux
 *
 *  Created by Hugo on 8/22/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef FRUITSEQUENCE_H
#define FRUITSEQUENCE_H

#include "Fruit.h"
#include "Config.h"
#include <vector>

typedef std::vector<Fruit*> Fruits;

class FruitSequence
{
public:
	void addFruit( Fruit* fruit );
	
	Fruits getSequence(void);
	
	bool intersects( FruitSequence* sequence );
	
	int8 size( void ){ return sequence.size(); };

	Fruit* at( int8 ind ){ return sequence.at( ind ); };
	
private:
	Fruits sequence;
};

#endif FRUITSEQUENCE_H
