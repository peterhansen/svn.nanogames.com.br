/*
 *  TimeBar.h
 *  FruitLux
 *
 *  Created by Peter Hansen on 9/20/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TIMEBAR_H
#define TIMEBAR_H 1

#include "Point3f.h"
#include "RenderableImage.h"
#include "Config.h"
#include "ObjectGroup.h"

/**
 * Classe interna que descreve uma partÌcula de fogos de artifÌcio. 
 */
class TimeBar : public ObjectGroup {
	
public:
	
	TimeBar( void );
	
	void setTime( float time, float total );
		
private:
	RenderableImage* pLeft;
	RenderableImage* pRight;
	RenderableImage* pFill;
};

#endif