#include "GameUtils.h"

#include "Config.h"
#include "Macros.h"
#include "MathFuncs.h"

/*==============================================================================================

MÉTODO GetDeviceAngles
	Retorna a angulação do device nos eixos x e y, levando em conta a posição de repouso
escolhida pelo usuçrio.

==============================================================================================*/

void GameUtils::GetDeviceAngles( float* pAngleX, float* pAngleY, const Point3f* pRestPosition, const Point3f* pAcceleration )
{
	float accelModule = pAcceleration->getModule();
	if( NanoMath::feql( accelModule, 0.0f ) )
		return;
	
	Point3f xAxis( 1.0f, 0.0f, 0.0f ), yAxis( 0.0f, 1.0f, 0.0f ), restPos = *pRestPosition;
	float restAngleToX = 0.0f, restAngleToY = 0.0f;
	float resPosModule = restPos.getModule();
	
	if( NanoMath::feql( resPosModule, 0.0f ) )
	{
		// Se chegarmos aqui, teríamos um problema. Então, por via das dúvidas, considera o
		// eixo padrão como posição de repouso
		restPos.set( 0.0f, 1.0f, 1.0f );
	}
	
	// Obtém a angulação do eixo de repouso em relação aos eixos padrão
	// TODOO : É PORCO calcular restAngleToY e restAngleToZ a cada chamada de
	// onAccelerate, já que, uma vez determinada a posição de repouso, 
	// restAngleToY e restAngleToZ se manterão eternamente iguais
	restAngleToX = restPos.getAngle( &xAxis );
	restAngleToY = restPos.getAngle( &yAxis );
	
	// Gera um intervalo significativo, pois, na prática, o usuário não consegue gerar
	// valores que compreendam todo o intervalo [-180º, 180º]
	float minAngleX = restAngleToX - ACCEL_HALF_MOVEMENT_FREEDOM_X, maxAngleX = restAngleToX + ACCEL_HALF_MOVEMENT_FREEDOM_X;
	float minAngleY = restAngleToY - ACCEL_HALF_MOVEMENT_FREEDOM_Y, maxAngleY = restAngleToY + ACCEL_HALF_MOVEMENT_FREEDOM_Y;
	
	// Obtém a angulação da aceleração em relação aos eixos padrão
	float accelAngleToX = pAcceleration->getAngle( &xAxis );
	float accelAngleToY = pAcceleration->getAngle( &yAxis );
	
	// TODOO : Verifica se estamos dando a "volta", pois getAngle() retorna apenas valores
	// no intervalo [0º, 180º] e não no intervalo [-180º, 180º]. O problema ocorre sempre
	// que o sinal de restPos.z fica diferente de pAcceleration->z
	if( (( restPos.z < 0.0f ) && ( pAcceleration->z > 0.0f )) || (( restPos.z > 0.0f ) && ( pAcceleration->z < 0.0f )) )
	{
		//		if( /* ??? */ )
		accelAngleToX = maxAngleX;
		//		else
		//			accelAngleToX = minAngleX;
	}
	
	//	#if DEBUG
	//		LOG( @"\n\nRestP: X = %.3f Y = %.3f Z = %.3f\nAccel: X = %.3f Y = %.3f Z = %.3f\nAngle: X = %.3f Y = %.3f\n\n", restPos.x, restPos.y, restPos.z, pAcceleration->x, pAcceleration->y, pAcceleration->z, accelAngleToX, accelAngleToY );
	//	#endif
	
	// Evita imprecisões de ponto flutuante
	accelAngleToX = NanoMath::clamp( accelAngleToX, minAngleX, maxAngleX );
	accelAngleToY = NanoMath::clamp( accelAngleToY, minAngleY, maxAngleY );
	
	// Obtém a angulação entre a aceleração e o eixo de repouso
	float angleX = -( accelAngleToX - restAngleToX );
	float angleY = accelAngleToY - restAngleToY;
	
	//	#if DEBUG
	//		LOG( @"\n\angleX = %.3f\angleY = %.3f\n\n", angleX, angleY );
	//	#endif
	
	// Evita imprecisões de ponto flutuante
	angleX = NanoMath::clamp( angleX, -ACCEL_HALF_MOVEMENT_FREEDOM_X, ACCEL_HALF_MOVEMENT_FREEDOM_X );
	angleY = NanoMath::clamp( angleY, -ACCEL_HALF_MOVEMENT_FREEDOM_Y, ACCEL_HALF_MOVEMENT_FREEDOM_Y );
	
	// Retorna os valores
	if( pAngleX )
		*pAngleX = angleX;
	
	if( pAngleY )
		*pAngleY = angleY;
	
	//	#if DEBUG
	//		LOG( @"Final Angle: X = %.3f, Y = %.3f", angleX, angleY );
	//	#endif
}

/*==============================================================================================

MÉTODO FormatNumStr
	Formata uma string numérica.

==============================================================================================*/

#define MAX_INT_N_CHARS 10

std::string GameUtils::FormatNumStr( int32 value, int32 nChars, char filler, char separator )
{
	// O maior valor que um int de 4 bytes pode ter é : 2.147.483.648, ou seja, ele terç,
	// no mçximo, 10 caracteres
	char aux[ MAX_INT_N_CHARS ];
	snprintf( aux, MAX_INT_N_CHARS, "%d", static_cast<int32>( value ) );
	std::string valueStr( aux );

	bool hasSeparator = ( separator != '\0' );

	std::string s( !hasSeparator ? nChars : nChars + ( NextMulOf( nChars, 3 ) / 3 ) - 1, '\0' );

	int32 sCounter = s.length() - 1;
	for( int32 i = 0 ; i < nChars ; ++i )
	{
		if( hasSeparator && ( i > 0 ) && ( i % 3 == 0 ) )
			s[ sCounter-- ] = separator;
		
		if( i < valueStr.length() )
			s[ sCounter-- ] = valueStr[ valueStr.length() - i - 1 ];
		else
			s[ sCounter-- ] = filler;
	}

	return s;
}

#undef MAX_INT_N_CHARS

/*==============================================================================================

MÉTODO NextMulOf
	Retorna o primeiro múltiplo de m no intervalo [n, MAX_INT].

==============================================================================================*/

int32 GameUtils::NextMulOf( int32 n, int32 m )
{
	int32 aux = n % m;
	return aux == 0 ? n : n + ( m - aux );
}
