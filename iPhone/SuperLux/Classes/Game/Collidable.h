/*
 *  Collidable.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 7/15/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef COLLIDABLE_H
#define COLLIDABLE_H 1

// Forward Declarations
class Point3f;

class Collidable
{
	public:
		// Checa colisão do objeto com o ponto
		virtual bool checkCollision( const Point3f* pPoint ) = 0;
};

#endif
