/*
 *  ScoreLabel.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 4/23/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef SCORE_LABEL_H
#define SCORE_LABEL_H 1

#include "Label.h"

// Forward Declarations
class GameScreen;

enum ScoreLabelState
{
	SCORE_LABEL_STATE_UNDEFINDED	= -1,
	SCORE_LABEL_STATE_IDLE			=  0,
	SCORE_LABEL_STATE_APPEARING		=  1,
	SCORE_LABEL_STATE_VISIBLE		=  2,
	SCORE_LABEL_STATE_VANISHING		=  3
};

class ScoreLabel : public Label
{
	public:
		// Construtor
		ScoreLabel( GameScreen* pPlayScreen );

		// Determina o estado do objeto
		void setState( ScoreLabelState s );
	
		// Retorna o estado do objeto
		ScoreLabelState getState( void ) const { return state; };

		// Determina a pontuação que deve ser exibida
		void setScore( const Point3f& pos, int32 score );

		// Atualiza o objeto
		virtual bool update( float timeElapsed );

	private:
		// Estado do objeto
		ScoreLabelState state;

		// Tempo pelo qual o label fica exposto antes de desaparecer
		float lifeTime;
	
		// Velocidade de atualização do placar
		float scoreSpeed;

		// Ponteiro para a tela de jogo
		GameScreen* pPlayScreen;
};
	
#endif
