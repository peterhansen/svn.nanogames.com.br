/*
 *  FruitListener.h
 *  FruitLux
 *
 *  Created by Peter Hansen on 9/20/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */
#ifndef FRUIT_LISTENER_H
#define FRUIT_LISTENER_H 1

class FruitListener {

	public:
	
	virtual ~FruitListener() {};
	
	virtual float getMaxYSpeed( void ) = 0;
	
	virtual void onDie( void ) = 0;
	
	virtual float getParticleAreaWidth( void ) = 0;
	
	virtual float getParticleAreaHeight( void ) = 0;
	
};

#endif