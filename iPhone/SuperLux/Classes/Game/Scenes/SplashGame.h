//
//  FreeKickAppDelegate.h
//  FreeKick
//
//  Created by Daniel Lopes Alves on 1/16/09.
//  Copyright Nano Games 2009. All rights reserved.
//

#ifndef SPLASH_GAME_H
#define SPLASH_GAME_H 1

// Apple Foundation
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

// C++
#include <string>

// Components
#include "NanoTypes.h" //uint8

@interface SplashGame : UIImageView
{
	@private
		// Tocador de vídeos
		MPMoviePlayerController* hMoviePlayer;
	
		// Nome do vídeo que esta view irá executar
		std::string movieToPlay;
	
		// Indica se estamos executando o vídeo
		bool isPlaying;
}

// Construtor chamado quando carregamos a view via código
- ( id )initWithFrame:( CGRect )frame Movie:( const std::string& )movieName AndSuportImg:( const std::string& )supportImg;

// Chama a próxima tela do jogo
- ( void ) onEnd;

// Método chamado quando a view se torna a view principal da aplicação
-( void ) onBecomeCurrentScreen;

@end

#endif
