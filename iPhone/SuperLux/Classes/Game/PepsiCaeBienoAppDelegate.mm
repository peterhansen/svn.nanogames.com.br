#include "PepsiCaeBienoAppDelegate.h"
#import <QuartzCore/QuartzCore.h>

#include "Config.h"
#include "Macros.h"
#include "Fruit.h"
#include "Particle.h"

#include "AccelerometerManager.h"
#include "EventManager.h"
#include "Font.h"
#include "LoadingView.h"

#if DEBUG
	#include "Tests.h"
#endif

#if SOUND_WITH_OPEN_AL
	#include "AudioManager.h"
#else
	#include "AppleAudioManager.h"
#endif

// Extensão da classe para declarar métodos privados
@interface PepsiCaeBienoAppDelegate ()

-( bool ) loadMonoThread;

// Carrega as views do jogo
-( void )loadAll:( LoadingView* )hLoadingView;

// Chamado quando uma thread de loading termina
-( void )onLoadEnded:( LoadingView* )hLoadingView;

// Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos da aplicação
-( void ) startLoading;
	
// Carrega os textos da aplicação
- ( bool ) initTexts;

// Carrega as fontes daÏ aplicação
-( bool ) initFonts;

// Libera os textos alocados
- ( void ) cleanTexts;

// Libera os sons alocados
- ( void ) cleanSounds;

// Libera a memória alocada pelas fontes da aplicação
- ( void ) cleanFonts;

// Adiciona um texto ao array de textos da aplicação
-( bool ) addText:( const char* )pText ToLanguage:( uint8 )languageIndex AtIndex:( uint8 )textIndex;

// Retorna o nome do arquivo onde devemos salvar os dados do jogo
-( NSString* )getSaveFileName;

// Carrega possíveis dados salvos
-( bool )loadRecords;

// Salva os dados do jogo
-( bool )saveRecords;

- ( void ) syncNewViewWithApplicationState;

@end

// Implementação da classe
@implementation PepsiCaeBienoAppDelegate

/*==============================================================================================

SYNTHESIZE
	Cria os setters e getters das propriedades declaradas.

==============================================================================================*/

@synthesize hGLView, hRecordsView, pGameScreen;

/*==============================================================================================

MENSAGEM applicationDidFinishLaunching
	Primeira mensagem chamada quando a aplicação é iniciada. Cria e inicializa a janela e as
views da aplicação.

==============================================================================================*/

- ( void )applicationDidFinishLaunching:( UIApplication* )application
{
	// OBS: Apesar de essa informação estar em info.plist, precisamos reseta-la aqui para que as UIAlertViews sejam exibidas corretamente
	// TODOO : Pegar a orientação de Info.plist!!!!!!!!!!!!!!!!!!!!!
	//[application setStatusBarOrientation: UIInterfaceOrientationLandscapeRight animated:NO];
	
	// Inicializa as variáveis da classe
	hMainMenu = NULL;
	hHelpView = NULL;
	hProductView = NULL;
	hTipsView = NULL;
	hCreditsView = NULL;
	hOptionsView = NULL;
	hPauseView = NULL;
	hRecordsView = NULL;
	hGLView = NULL;
	hSplashNano = NULL;
	hSplashGame = NULL;
	pGameScreen = NULL; 

	// Inicializa os membros da superclasse
	[super applicationDidFinishLaunching: application];

	// Inicializa os arrays da classe
#if ( APP_N_SOUNDS > 0 ) && SOUND_WITH_OPEN_AL
	memset( appSounds, 0, sizeof( AudioSource* ) * APP_N_SOUNDS );
#endif
	memset( texts, 0, sizeof( char* ) * APP_N_TEXTS );
	memset( appFonts, 0, sizeof( Font* ) * APP_N_FONTS );
	
	// Salva os recordes
	if( ![self loadRecords] )
		memset( records, 0, sizeof( uint32 ) * APP_N_RECORDS );

	// Cria o controlador de sons da aplicação
#if SOUND_WITH_OPEN_AL
	if( !AudioManager::Create() )
		goto Error;
	AudioManager::GetInstance()->suspend();
#else
	if( !AppleAudioManager::Create( APP_N_SOUNDS ) )
		goto Error;
	AppleAudioManager::GetInstance()->suspend();
#endif
	
	// Cria os singletons que irão controlar os eventos de toque e do acelerômetro
	if( !EventManager::Create() )
		goto Error;
	
	if( ![self initTexts] )
		goto Error;
	
	// Indica que este objeto irá receber os eventos do controlador de telas
	// OBS: hViewManager foi criado quando carregamos MainWindow.xib, o que ocorre logo antes de applicationDidFinishLaunching ser chamada
	[hViewManager setDelegate: self];
	
	// Impede o device de entrar no modo sleep durante o jogo
	[[UIApplication sharedApplication] setIdleTimerDisabled: YES];
	
	[self setVibrationStatus: true];
	
	// OBS: Está quebrando quando tentamos carregar uma UITextView numa thread auxiliar...
//	hHelpView = ( HelpView* )[self loadViewFromXib: "HelpView" ];
//	hProductView = ( ProductView* )[self loadViewFromXib: "ProductView" ];
//	hTipsView = ( TipsView* )[self loadViewFromXib: "TipsView" ];
	
	// TODOO teste [ self loadMonoThread ];
	[ self startLoading ];

	// Exibe a tela de splash nano
//	TODOO??? 
//	[self performTransitionToView: VIEW_INDEX_LOADING];
//	[self performTransitionToView: VIEW_INDEX_SPLASH_NANO];
	
	return;
	
	// Label de tratamento de erros
	Error:
		[self quit: ERROR_ALLOCATING_VIEWS];
		return;
}

/*==============================================================================================

MENSAGEM performTransitionToView
	Executa a transição da view atual para a view de índice passado como parâmetro.
 
 	Possíveis transições:
		- kCATransitionMoveIn
		- kCATransitionPush
		- kCATransitionReveal
		- kCATransitionFade
 
	Transições não-documentadas:
		- @"pageCurl"
		- @"pageUnCurl"
		- @"suckEffect"
		- @"spewEffect"
		- @"cameraIris ( from the Photos application )"
		- @"cameraIrisHollowOpen"
		- @"cameraIrisHollowClose"
		- @"genieEffect ( typically used for deleting garbage )"
		- @"unGenieEffect"
		- @"rippleEffect"
		- @"twist"
		- @"tubey"
		- @"swirl"
		- @"charminUltra"
		- @"zoomyIn"
		- @"zoomyOut"
		- @"oglFlip"
 
	Possíveis direções:
		- kCATransitionFromLeft
		- kCATransitionFromRight
		- kCATransitionFromTop
		- kCATransitionFromBottom
 
==============================================================================================*/

// TODOO : O certo seria chamar respondsToSelector( suspend ) no início do método para view
// atual !!!! No final do método, chamaríamos respondsToSelector( resume )

- ( void )performTransitionToView:( uint8 )newIndex;
{
	// Não interrompe uma transição
	if( [hViewManager isTransitioning] )
		return;
	
	lastViewIndex = currViewIndex;
	
    // TASK : Fazer através de controlers e não diretamente das views
	UIView *hNextView;
	NSString *hTransition = nil, *hDirection = nil;
	
	bool keepCurrView = true, forceDraw = false;

	switch( newIndex )
	{
		case VIEW_INDEX_LOADING:
			[self startLoadingWithTarget: self AndSelector: @selector( loadAll: ) CallingAfterLoad: @selector( onLoadEnded: )];
			currViewIndex = newIndex;
			return;

		case VIEW_INDEX_SPLASH_NANO:
			{
				hNextView = hSplashNano;
				hSplashNano = NULL;
			}
			break;
			
		case VIEW_INDEX_SPLASH_GAME:
			hNextView = hSplashGame;
			hTransition = kCATransitionFade;
			break;
			
		case VIEW_INDEX_MAIN_MENU:
			{ // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
				switch( currViewIndex )
				{
					case VIEW_INDEX_SPLASH_GAME:
						hSplashGame = NULL;
						forceDraw = true;
						
						[ self stopAudio ];
						
						// OBS: NÃO PODE TOCAR O SOM ANTES DE FAZER A TRANSIÇÃO QUE REMOVERÁ O VÍDEO DA TELA!!!! FAZÊ-LO CAUSA FLICKERING DE VÍDEO EM
						// VERSÕES DO IPHONEOS INFERIORES À 3.0!!!!
						// [hMainMenu onBecomeCurrentScreen] ficou responsável por tocar o som do menu
						break;
						
					case VIEW_INDEX_RECORDS:
						[( ( RecordsView* )[[hViewManager subviews] objectAtIndex: 0] ) suspend];
						break;
						
					case VIEW_INDEX_OPTIONS:
						break;

					case VIEW_INDEX_GAME:
						pGameScreen->resetScore();
						
						// Suspende o processamento da tela de jogo
						[hGLView suspend];
						// sem break mesmo
						
					case VIEW_INDEX_PAUSE_SCREEN:
						[ self stopAudio ];
						
						// OBS: Este jogo mantém todas as views alocadas na memória
//						[self releaseGameView];

						// OBS: NÃO PODE TOCAR O SOM ANTES DE FAZER A TRANSIÇÃO QUE REMOVERÁ O VÍDEO DA TELA!!!! FAZÊ-LO CAUSA FLICKERING DE VÍDEO EM
						// VERSÕES DO IPHONEOS INFERIORES À 3.0!!!!
						// [hMainMenu onBecomeCurrentScreen] ficou responsável por tocar o som do menu
						//[(( PepsiCaeBienoAppDelegate* )APP_DELEGATE ) playAudioNamed: SOUND_NAME_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
						break;
				}

				hNextView = hMainMenu;

			} // Evita erros de compilação por causa de declaração de variáveis feitas dentro de cases
			break;

		case VIEW_INDEX_PAUSE_SCREEN:
			// Suspende o processamento da tela de jogo
			[hGLView suspend];

			// Exibe a tela de pause
			hNextView = hPauseView;
			break;
			
		case VIEW_INDEX_RECORDS:
			pGameScreen->resetScore();
			
			if( currViewIndex == VIEW_INDEX_GAME )
			{
				// pára de tocar a música ambiente do jogo
				for ( uint8 i = 0; i < SOUND_AMBIENT_TYPES_TOTAL; ++i )
					[ self stopAudioAtIndex:SOUND_INDEX_AMBIENT_1 + i ];
				
				[hGLView suspend];
				
				[ self playAudioNamed: SOUND_NAME_SPLASH AtIndex: SOUND_INDEX_SPLASH Looping: true];
			}
			
			hNextView = hRecordsView;

			[hRecordsView setRecords: records];
			[hRecordsView resume];

			break;

		case VIEW_INDEX_OPTIONS:
			hNextView = hOptionsView;
			break;
				
		case VIEW_INDEX_HELP:
			hNextView = hHelpView;
			break;
			
		case VIEW_INDEX_PRODUCT:
			hNextView = hProductView;
			break;
			
		case VIEW_INDEX_TIPS:
			hNextView = hTipsView;
			
			// Suspende o processamento da tela de jogo, mantendo-a na memória
			[hGLView suspend];
			
			// Vamos manter a tela de jogo na memória
			keepCurrView = true;
			
			break;
			
		case VIEW_INDEX_CREDITS:
			hNextView = hCreditsView;
			break;
			
		case VIEW_INDEX_NEW_GAME:
		case VIEW_INDEX_NEXT_LEVEL:
			if ( newIndex == VIEW_INDEX_NEXT_LEVEL )
				pGameScreen->prepareNextLevel();
			else
				pGameScreen->prepareLevel( 1 );
			
		case VIEW_INDEX_GAME:
			{
				pGameScreen->cancelTrackedTouches();
				newIndex = VIEW_INDEX_GAME;
				hNextView = hGLView;

				// Renderiza a view do jogo sem o jogador ver, já utilizando as novas opções do menu. Assim
				// garantimos que ele não verá a imagem anterior durante a transição
				[hGLView render];
				
				// Reinicia o processamento da tela de jogo
				[hGLView resume];
			}
			break;
			
		default:
			#if DEBUG
				LOG( @"AppDelegate::performTransitionToView() - Invalid index" );
			#endif
			return;
	}

	if( hTransition != nil )
	{
		// Executa a transiÃ§Ã£o de views
		NSArray* hSubviews = [hViewManager subviews];
		[hViewManager transitionFromSubview: ( [hSubviews count] > 0 ? [hSubviews objectAtIndex: 0] : nil ) toSubview: hNextView keepOldView: keepCurrView freeNewView: true transition: hTransition direction: hDirection duration: DEFAULT_TRANSITION_DURATION ];
	}
	else
	{

		[hViewManager addSubview: hNextView];
		
		if( [hNextView respondsToSelector: @selector( onBecomeCurrentScreen )] )
			[hNextView onBecomeCurrentScreen];
		
		if( forceDraw )
		{
			[hViewManager bringSubviewToFront: hNextView];
			[hNextView drawRect: [[UIScreen mainScreen] bounds]];
			[hNextView setNeedsDisplay];
		}
		
		[hNextView release];
		
		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			
			if( hCurrView != hNextView )
			{
				if( keepCurrView )
					[hCurrView retain];
				
				[hCurrView removeFromSuperview];
			}
		}
		
		[self syncNewViewWithApplicationState];
	}
	
	// Atualiza o índice da view que está sendo exibida
	currViewIndex = newIndex;
}

/*==============================================================================================
 
 MENSAGEM syncNewViewWithApplicationState
 Sincrozina o estado da view atual com o estado da aplicação. Este método serve para contornar
 a possibilidade de recebermos um evento de suspend enquanto estamos transitando entre views.
 
 ==============================================================================================*/

- ( void ) syncNewViewWithApplicationState
{
	if( [self getApplicationState] == APPLICATION_STATE_SUSPENDED )
	{
		NSArray* hSubviews = [hViewManager subviews];
		if( [hSubviews count] > 0 )
		{
			UIView *hCurrView = [hSubviews objectAtIndex:0];
			if( [hCurrView respondsToSelector: @selector( suspend )] )
				[hCurrView suspend];
		}
	}
}

/*==============================================================================================

MENSAGEM transitionDidStart
	Chamada quando uma transição de views está para começar.

==============================================================================================*/

//- ( void )transitionDidStart:( ViewManager* )manager
//{
//}

/*==============================================================================================

MENSAGEM transitionDidFinish
	Chamada quando uma transição de views acabou de terminar.

==============================================================================================*/

- ( void )transitionDidFinish:( ViewManager* )manager
{
	switch( currViewIndex )
	{
		case VIEW_INDEX_GAME:
			// Reinicia o processamento dos componentes do jogo
			[hGLView resume];
			break;
			
		case VIEW_INDEX_MAIN_MENU:
		case VIEW_INDEX_SELECT_LANG:
		case VIEW_INDEX_LOADING:
		case VIEW_INDEX_SPLASH_NANO:
		case VIEW_INDEX_SPLASH_GAME:
		case VIEW_INDEX_RECORDS:
		case VIEW_INDEX_OPTIONS:
		case VIEW_INDEX_HELP:
		case VIEW_INDEX_CREDITS:
		case VIEW_INDEX_PAUSE_SCREEN:
		case VIEW_INDEX_CONFIRM_SCREEN:
		case VIEW_INDEX_TIPS:
		case VIEW_INDEX_NEXT_LEVEL:
			{
				NSArray* hSubviews = [hViewManager subviews];
				if( [hSubviews count] > 0 )
				{
					UIView *hCurrView = [hSubviews objectAtIndex:0];
					if( [hCurrView respondsToSelector: @selector( onBecomeCurrentScreen )] )
						[hCurrView onBecomeCurrentScreen];
				}
			}
			break;
	}
	
	[self syncNewViewWithApplicationState];
}

/*==============================================================================================

MENSAGEM transitionDidCancel
	Chamada quando uma transição de views é cancelada.

==============================================================================================*/

- ( void )transitionDidCancel:( ViewManager* )manager
{
	// TODO : Confirmar se a condição do if é testada desse jeito
	// Se estávamos saindo da tela de jogo, retoma o processamento dos componentes
	if( currViewIndex == VIEW_INDEX_MAIN_MENU )
		[hGLView resume];
}

/*==============================================================================================

MENSAGEM loadAll
	Carrega as views do jogo.

==============================================================================================*/

-( void )loadAll:( LoadingView* )hLoadingView
{		
	[hLoadingView setProgress: 0.05f];

	// Cria as views do jogo
	CGRect screenSize = [[UIScreen mainScreen] bounds];

//	// Carrega o splash da nano
//	hSplashNano = ( SplashNano* )[self loadViewFromXib: "SplashNano" ];
//	[hLoadingView setProgress: 0.15f];
//
//	// Carrega o splash do jogo
//	hSplashGame = [[SplashGame alloc] initWithFrame: screenSize Movie: "intro" AndSuportImg: "intro"];
//	[hLoadingView setProgress: 0.2f];
//	
//	// Cria o menu principal
//	hMainMenu = ( MainMenuView* )[self loadViewFromXib: "MainMenu" ];
//	[hLoadingView setProgress: 0.25f];
//	hCreditsView = ( CreditsView* )[self loadViewFromXib: "CreditsView" ];
//	[hLoadingView setProgress: 0.3f];
//	
//	// OBS: Está quebrando quando tentamos carregar uma UITextView numa thread auxiliar...
//	
//	hOptionsView = ( OptionsView* )[self loadViewFromXib: "OptionsView" ];
//	[hLoadingView setProgress: 0.4f];
//	
//	hPauseView = ( PauseView* )[self loadViewFromXib: "PauseView" ];
//	[hLoadingView setProgress: 0.45f];
//	
//	hRecordsView = ( RecordsView* )[self loadViewFromXib: "RecordsView" ];
//	[hLoadingView setProgress: 0.55f];

	// Cria a view do jogo (view OpenGL)
	hGLView = [[EAGLView alloc] initWithFrame:screenSize];
	[hLoadingView setProgress: 0.65f];
	
	// Verifica se conseguiu alocar todas as views necessárias
	if( /*( hSplashNano == NULL ) || ( hSplashGame == NULL ) || ( hMainMenu == NULL )
	   || ( hHelpView == NULL ) || ( hProductView == NULL ) || ( hCreditsView == NULL ) || ( hOptionsView == NULL )
	   || ( hRecordsView == NULL ) || ( hTipsView == NULL ) || ( hPauseView == NULL )
	   ||*/ ( hGLView == NULL ) )
	{
		// Termina a aplicação
		[hLoadingView setError: ERROR_ALLOCATING_VIEWS];
		return;
	}
	
	// Carrega as imagens das peças e as fontes do jogo
	// OBS: Estes métodos devem ser chamados obrigatoriamente após a inicialização do OpenGL	
	if( ![self initFonts] ) 
	{	
		// Termina a aplicação
		[hLoadingView setError: ERROR_ALLOCATING_DATA];
		return;
	}
	[hLoadingView setProgress: 0.70f];
	
	if( !Fruit::LoadImages() ) 
	{	
		// Termina a aplicação
		[hLoadingView setError: ERROR_ALLOCATING_DATA];
		return;
	}
	[hLoadingView setProgress: 0.80f];
	
	if( !Particle::LoadImages() ) 
	{	
		// Termina a aplicação
		[hLoadingView setError: ERROR_ALLOCATING_DATA];
		return;
	}
	[hLoadingView setProgress: 0.90f];
	

	pGameScreen = new GameScreen();
	
	// Verifica se conseguiu alocar todas as views necessárias
	if( pGameScreen == NULL )
	{
		// Termina a aplicação
		[hLoadingView setError: ERROR_ALLOCATING_DATA];
		return;
	}
	
	// Associa a primeira cena do jogo ao controlador da API gráfica
	[hGLView setCurrScene: pGameScreen];

	[hLoadingView setProgress: 1.00f];
}


-( bool ) loadMonoThread {
	// Cria as views do jogo
	CGRect screenSize = [[UIScreen mainScreen] bounds];
	
	hHelpView = ( HelpView* )[self loadViewFromXib: "HelpView" ];
	hProductView = ( ProductView* )[self loadViewFromXib: "ProductView" ];
	hTipsView = ( TipsView* )[self loadViewFromXib: "TipsView" ];	
	
	// Carrega o splash da nano
	hSplashNano = ( SplashNano* )[self loadViewFromXib: "SplashNano" ];
	
	// Carrega o splash do jogo
	hSplashGame = [[SplashGame alloc] initWithFrame: screenSize Movie: "intro" AndSuportImg: "intro"];
	
	// Cria o menu principal
	hMainMenu = ( MainMenuView* )[self loadViewFromXib: "MainMenu" ];
	hCreditsView = ( CreditsView* )[self loadViewFromXib: "CreditsView" ];
	
	// OBS: Está quebrando quando tentamos carregar uma UITextView numa thread auxiliar...
	
	hOptionsView = ( OptionsView* )[self loadViewFromXib: "OptionsView" ];
	
	hPauseView = ( PauseView* )[self loadViewFromXib: "PauseView" ];
	
	hRecordsView = ( RecordsView* )[self loadViewFromXib: "RecordsView" ];
	
	// Cria a view do jogo (view OpenGL)
	hGLView = [[EAGLView alloc] initWithFrame:screenSize];
	
	// Verifica se conseguiu alocar todas as views necessárias
	if( ( hSplashNano == NULL ) || ( hSplashGame == NULL ) || ( hMainMenu == NULL )
	   || ( hHelpView == NULL ) || ( hProductView == NULL ) || ( hCreditsView == NULL ) || ( hOptionsView == NULL )
	   || ( hRecordsView == NULL ) || ( hTipsView == NULL ) || ( hPauseView == NULL )
	   || ( hGLView == NULL ) )
	{
		// Termina a aplicação
		return false;
	}
	
	// Carrega as imagens das peças e as fontes do jogo
	// OBS: Estes métodos devem ser chamados obrigatoriamente após a inicialização do OpenGL	
	if( ![self initFonts] ) 
	{	
		// Termina a aplicação
		return false;
	}
	
	if( !Fruit::LoadImages() ) 
	{	
		// Termina a aplicação
		return false;
	}
	
	if( !Particle::LoadImages() ) 
	{	
		// Termina a aplicação
		return false;
	}
	
	pGameScreen = new GameScreen();
	
	// Verifica se conseguiu alocar todas as views necessárias
	if( pGameScreen == NULL )
	{
		// Termina a aplicação
		return false;
	}
	
	// Associa a primeira cena do jogo ao controlador da API gráfica
	[hGLView setCurrScene: pGameScreen];
	
	[self performTransitionToView: VIEW_INDEX_SPLASH_NANO];
	
	return true;
}


/*==============================================================================================
 
 MENSAGEM startLoading
 Coloca um indicador de processamento da tela e chama a função que irá carregar os recursos
 da aplicação.
 
 ==============================================================================================*/

-( void ) startLoading
{
	// Cria a tela de loading
	UIActivityIndicatorView* hLoadingIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 32.0f, 32.0f)]; 
	
	if( [[ApplicationManager GetInstance] isOrientationLandscape] )
		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_HEIGHT, HALF_SCREEN_WIDTH)];
	else
		[hLoadingIndicator setCenter:CGPointMake(HALF_SCREEN_WIDTH, HALF_SCREEN_HEIGHT)];
	
	[hLoadingIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge]; 
	[hViewManager addSubview:hLoadingIndicator];
	[hLoadingIndicator startAnimating];
	[hLoadingIndicator release];
	
	// Timer que irá carregar as views do jogo
	[NSTimer scheduledTimerWithTimeInterval:0.001f target:self selector:@selector(loadMonoThread) userInfo:NULL repeats:NO];
}

/*==============================================================================================

MENSAGEM onLoadEnded:
	Chamado quando uma thread de loading termina.

==============================================================================================*/
	
-( void )onLoadEnded:( LoadingView* )hLoadingView
{
	int16 errorCode;
	if( ( errorCode = [hLoadingView getError] ) != ERROR_NONE )
	{
		[self quit: errorCode];
		return;
	}

	if( currViewIndex == VIEW_INDEX_LOADING )
	{
		[self performTransitionToView: VIEW_INDEX_SPLASH_NANO];
	}
}

/*==============================================================================================

MENSAGEM dealloc
	Destrutor.

==============================================================================================*/

// OBS : Descomentar se for necessário personalizar
//- ( void )dealloc
//{	
//	[super dealloc];
//}

/*==============================================================================================
 
 MENSAGEM applicationWillResignActive
 Mensagem chamada quando a aplicação vai ser suspensa.
 
 ==============================================================================================*/

- ( void )applicationWillResignActive:( UIApplication* )application
{
	[super applicationWillResignActive: application];
	
	// Pára de tocar sons
#if SOUND_WITH_OPEN_AL
	AudioManager::GetInstance()->suspend();
#else
	AppleAudioManager::GetInstance()->suspend();
#endif
	
	// Pára de renderizar e atualizar as views e scenes do jogo
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( suspend )] )
			[hCurrView suspend];
	}
}

/*==============================================================================================
 
 MENSAGEM applicationDidBecomeActive
 Mensagem chamada quando a aplicação é reiniciada.
 
 ==============================================================================================*/

- ( void )applicationDidBecomeActive:( UIApplication* )application
{
	[super applicationDidBecomeActive: application];
	
	// Volta a executar os sons
#if SOUND_WITH_OPEN_AL
	AudioManager::GetInstance()->resume();
#else
	AppleAudioManager::GetInstance()->resume();
#endif
		
	// Reinicia a renderização e a atualização das views e scenes do jogo
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView *hCurrView = [hSubviews objectAtIndex:0];
		if( [hCurrView respondsToSelector: @selector( resume )] )
			[hCurrView resume];
	}
}

/*==============================================================================================

MENSAGEM applicationWillTerminate
	Tells the delegate when the application is about to terminate. This method is optional. This
method is the ideal place for the delegate to perform clean-up tasks, such as freeing allocated
memory, invalidating timers, and storing application state.

==============================================================================================*/

- ( void )applicationWillTerminate:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationWillTerminate" );
#endif
	
	// Reabilita o modo sleep
	[[UIApplication sharedApplication] setIdleTimerDisabled: NO];;

	// Suspende o processamento da tela atual
	[self applicationWillResignActive: application];
	
	// Salva os recordes
	[self saveRecords];
	
	// Desaloca os textos, os sons e as fontes da aplica√ß√£o
	[self cleanTexts];
	[self cleanSounds];
	[self cleanFonts];
	Particle::ReleaseImages();
	Fruit::ReleaseImages();
	
	// Impede de desalocarmos duplamente a tela atual
	NSArray* hSubviews = [hViewManager subviews];
	if( [hSubviews count] > 0 )
	{
		UIView* hCurrView = [hSubviews objectAtIndex: 0];
		[hCurrView retain];
	}
	
	// Força a desalocação da cena se estamos na tela de jogo
	if( currViewIndex == VIEW_INDEX_GAME )
	{
		[hGLView setCurrScene: NULL];
	}
	
	// Desaloca as telas da aplicação
	//SAFE_RELEASE( hLanguageView );
	SAFE_RELEASE( hSplashNano );
	SAFE_RELEASE( hSplashGame );
	
	SAFE_RELEASE( hGLView );
	SAFE_RELEASE( hHelpView );
	SAFE_RELEASE( hProductView );
	SAFE_RELEASE( hTipsView );
	SAFE_RELEASE( hOptionsView );
	SAFE_RELEASE( hPauseView );
	SAFE_RELEASE( hCreditsView );
	SAFE_RELEASE( hRecordsView );

	SAFE_RELEASE( hMainMenu );

	// OLD : hViewManager será desalocado automaticamente ao quando abandonarmos a aplicação,  já que o criamos quando carregamos MainWindow.xib
//	// Desaloca o controlador de telas
//	// OBS: Não utiliza SAFE_RELEASE pois obrigatoriamente teremos hViewManager, já que este é criado quando carregamos MainWindow.xib
//	KILL( hViewManager );
	
	// Desaloca os singletons
//	AccelerometerManager::Destroy();
	
	#if SOUND_WITH_OPEN_AL
		AudioManager::Destroy();
	#else
		AppleAudioManager::Destroy();
	#endif
	
	EventManager::Destroy();

	// OLD : hWindow será desalocado automaticamente ao quando abandonarmos a aplicação, já que o criamos quando carregamos MainWindow.xib
//	// Desaloca a janela da aplicação
//	// OBS: Não utiliza SAFE_RELEASE pois obrigatoriamente teremos hWindow, já que este é criado quando carregamos MainWindow.xib
//	KILL( hWindow );
}

/*==============================================================================================

MENSAGEM applicationDidReceiveMemoryWarning
	Tells the delegate when the application receives a memory warning from the system. This
method is optional. In this method, the delegate tries to free up as much memory as possible.
After the method returns (and the delegate then returns from applicationWillTerminate), the
application is terminated.

==============================================================================================*/

- ( void )applicationDidReceiveMemoryWarning:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationDidReceiveMemoryWarning" );
#endif
	[self quit: ERROR_MEMORY_WARNING];
}

/*==============================================================================================

MENSAGEM applicationSignificantTimeChange
	Tells the delegate when there is a significant change in the time. This method is optional.
Examples of significant time changes include the arrival of midnight, an update of the time by
a carrier, and the change to daylight savings time. The delegate can implement this method to
adjust any object of the application displays time or is sensitive to time changes.

==============================================================================================*/

- ( void )applicationSignificantTimeChange:( UIApplication* )application
{
#if DEBUG
	LOG( @"applicationSignificantTimeChange" );
#endif
}

/*==============================================================================================

MENSAGEM getText
	Retorna um texto da aplicação.

==============================================================================================*/

- ( const char* ) getText: ( uint16 ) textIndex
{
	return texts[ textIndex ];
}

/*==============================================================================================

MENSAGEM getFont
	Retorna uma fonte da aplicação.

==============================================================================================*/

- ( Font* ) getFont:( uint8 )fontIndex
{
	return appFonts[ fontIndex ];
}

/*==============================================================================================

MÉTODO initFonts
	Carrega as fontes da aplicação.

==============================================================================================*/

-( bool ) initFonts
{
	char fontNames[] = "fnt_";
	bool monoSpaced[ APP_N_FONTS ] = { false, false };

	for( uint8 i = 0 ; i < APP_N_FONTS ; ++i )
	{
		// Monta o nome da fonte
		fontNames[3] = '0' + i;
		
		// Cria a fonte
		appFonts[ i ] = new Font( fontNames, fontNames, monoSpaced[i] );

		if( !appFonts[ i ] )
		{
			[self cleanFonts];
			return false;
		}
	}
	return true;
}

/*==============================================================================================
 
 MENSAGEM playAudioNamed:AtIndex:Looping:
 Inicia a reprodução de um som da aplicação.
 
 ==============================================================================================*/

- ( void ) playAudioNamed:( uint8 )audioName AtIndex:( uint8 ) audioIndex Looping:( bool )looping
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	// Ignora audioName pois os sons foram carregados em initSounds
	appSounds[ audioIndex ]->setLooping( looping );
	appSounds[ audioIndex ]->play();
#else
	switch ( audioIndex ) {
		case SOUND_INDEX_BEAUTY_TIPS:
		case SOUND_INDEX_TIME_UP:
		case SOUND_INDEX_AMBIENT_1:
		case SOUND_INDEX_AMBIENT_2:
		case SOUND_INDEX_LEVEL_START:
		case SOUND_INDEX_SPLASH:
		case SOUND_INDEX_LEVEL_COMPLETE:
			// no iPod touch isso eh necessario, senao somente o primeiro audio desse tipo sera tocado
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_BEAUTY_TIPS );	
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_TIME_UP );	
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_AMBIENT_1 );	
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_AMBIENT_2 );	
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_LEVEL_START );	
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_SPLASH );	
			AppleAudioManager::GetInstance()->unloadSound( SOUND_INDEX_LEVEL_COMPLETE );	
		break;
	}

	AppleAudioManager::GetInstance()->playSound( audioName, audioIndex, looping, true );
#endif
#endif
}

/*==============================================================================================
 
 MENSAGEM stopAudio
 Pára de reproduzir todos os sons.
 
 ==============================================================================================*/

- ( void ) stopAudio
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
	{
		if( appSounds[i]->getState() == AL_PLAYING )
			appSounds[i]->stop();
	}
#else
	AppleAudioManager::GetInstance()->stopAllSounds();
#endif
#endif
}

/*==============================================================================================
 
 MENSAGEM stopAudioAtIndex
 Pára de reproduzir o som indicado.
 
 ==============================================================================================*/

- ( void ) stopAudioAtIndex: ( uint8 )index
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	if( appSounds[ index ]->getState() == AL_PLAYING )
		appSounds[ index ]->stop();
#else
	AppleAudioManager::GetInstance()->stopSound( index );
#endif
#endif
}

/*==============================================================================================
 
 MENSAGEM pauseAllSounds:
 (Des)Pausa todos os sons que estão sendo executados no momento.
 
 ==============================================================================================*/

- ( void ) pauseAllSounds: ( bool )pause
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
		appSounds[i]->pause();
#else
	AppleAudioManager::GetInstance()->pauseAllSounds( pause );
#endif
#endif
}

/*==============================================================================================
 
 MENSAGEM isPlayingAudioWithIndex:
 Indica se está tocando um som específico.
 
 ==============================================================================================*/

- ( bool )isPlayingAudioWithIndex:( uint8 )index
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	
	return appSounds[ index ]->getState() == AL_PLAYING;
	
#else
	
	return AppleAudioManager::GetInstance()->isPlaying( index );
	
#endif
#else
	return false;
#endif
}

/*==============================================================================================
 
 MENSAGEM isPlayingAudio
 Indica se a aplicação está tocando algum som.
 
 ==============================================================================================*/

- ( bool )isPlayingAudio
{
#if APP_N_SOUNDS > 0
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
	{
		if( [self isPlayingAudioWithIndex: i] )
			return true;
	}
	return false;
#else
	return false;
#endif
}

/*==============================================================================================
 
 MENSAGEM getAudioVolume
 Retorna o percentual do volume máximo utilizado na reprodução de sons.
 
 ==============================================================================================*/

- ( float )getAudioVolume
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	float volume = 1.0f;
	AudioManager::GetInstance()->getPlaybackDevice()->getAudioListener()->getGain( &volume );
	return volume;
#else
	return AppleAudioManager::GetInstance()->getVolume();
#endif
#else
	return -1.0f;
#endif
}

/*==============================================================================================
 
 MENSAGEM setVolume
 Determina o volume dos sons da aplicação.
 
 ==============================================================================================*/

- ( void )setVolume:( float )volume
{
#if APP_N_SOUNDS > 0
#if SOUND_WITH_OPEN_AL
	AudioManager::GetInstance()->getPlaybackDevice()->getAudioListener()->setGain( volume );
#else
	AppleAudioManager::GetInstance()->setVolume( volume );
#endif
#endif
}

/*==============================================================================================
 
 MENSAGEM unloadSounds
 Retira da memória os sons já carregados.
 
 ==============================================================================================*/

#if APP_N_SOUNDS > 0

-( void ) unloadSounds
{
#if SOUND_WITH_OPEN_AL
	// OBS: Este método ainda não está implementado para OpenAl
	assert( 0 );
#else
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
		AppleAudioManager::GetInstance()->unloadSound( i );
#endif
}

#endif

/*==============================================================================================

MENSAGEM initTexts
	Carrega os textos da aplicação.

==============================================================================================*/

- ( bool ) initTexts
{	
	{ // Evita problemas com os gotos

		// Português
		if( ![self addText: "Alerta" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_TITLE] )
			goto Error;
		
		if( ![self addText: "Este botão termina o jogo e inicia o navegador da internet. Deseja sair do jogo?" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_MSG_GOTOURL] )
			goto Error;
		
		if( ![self addText: "Esta versão do jogo não suporta o iPhone OS 3.0 ou superiores. Aguarde uma possível atualização." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_MSG_NO_OS_SUPPORT] )
			goto Error;
		
		if( ![self addText: "Sim" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_YES] )
			goto Error;
		
		if( ![self addText: "Não" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_NO] )
			goto Error;
		
		if( ![self addText: "Sair" ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_POPUP_EXIT] )
			goto Error;

		// textos dos produtos 
		if( ![self addText: 		"BRILHE!\n\nCom brilho dos cristais e gotas hidratantes para proporcionar uma sensação luminosa e uma pele invejável. Disponível em embalagem de 250 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0] )
			goto Error;
		
		if( ![self addText: "ENERGIZE-SE\n\nCom extrato de guaraná e vitamina E para deixar a sua pele revitalizada e cheia de energia. Disponível em embalagem de 250 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 1] )
			goto Error;
		
		if( ![self addText: "SEDUZA-ME\n\nCom creme de chocolate e chantilly hidratante para deixar a sua pele deliciosamente irresistível. Disponível em embalagem de 250 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 2] )
			goto Error;
		
		if( ![self addText: "SURPREENDA-ME\n\nCom extrato de morangos e chantilly hidratante para deixar a sua pele deliciosamente macia. Disponível em embalagens de 250 ml e 150 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 3] )
			goto Error;
		
		if( ![self addText: "SINTA-ME\n\nCom extrato de pêssegos e chantilly hidratante para deixar a sua pele visivelmente aveludada. Disponível em embalagens de 250 ml e 150 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 4] )
			goto Error;
		
		if( ![self addText: "FASHION PINK\n\nUma explosão de perfume e feminilidade. Disponível em embalagem de 250 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 5] )
			goto Error;
		
		if( ![self addText: "PROVE-ME\n\nCom óleo de macadâmia e chantilly hidratante para deixar a sua pele com linda aparência nutrida. Disponível em embalagens de 250 ml e 150 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 6] )
			goto Error;
		
		if( ![self addText: "RECARREGUE-SE\n\nCom extrato de menta e aloe vera para deixar a sua pele renovada e cheia de vida. Disponível em embalagem de 250 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 7] )
			goto Error;
		
		if( ![self addText: "REFRESQUE-SE\n\nCom extrato de algas marinhas e sais minerais para deixar a sua pele linda e fresca. Disponível em embalagem de 250 ml." ToLanguage: LANGUAGE_INDEX_PORTUGUESE AtIndex: TEXT_INDEX_PRODUCT_0 + 8] )
			goto Error;
		
		return true;
	} // Evita problemas com os gotos

	// Label de tratamento de erros
	Error:
		[self cleanTexts];
		return false;
}

/*==============================================================================================

MENSAGEM addText
	Adiciona um texto ao array de textos da aplicação.

==============================================================================================*/

#define MAX_TEXT_LEN 2048

-( bool ) addText:( const char* )pText ToLanguage:( uint8 )languageIndex AtIndex:( uint8 )textIndex
{	
	char buffer[ MAX_TEXT_LEN ];
	snprintf( buffer, MAX_TEXT_LEN, "%s", pText );
	
	// +1 = Espa√ßo extra para conter o '\0' que termina a string
	int32 len = strlen( buffer ) + 1;
	
	texts[ textIndex ] = new  char[ len ];
	if( !texts[ textIndex ] )
		return false;
	
	snprintf( texts[ textIndex ], len, "%s", buffer );
	
	return true;
}

#undef MAX_TEXT_LEN

/*==============================================================================================

MENSAGEM cleanTexts
	Libera os textos alocados.

==============================================================================================*/

- ( void ) cleanTexts
{
		for( uint8 j = 0 ; j < APP_N_TEXTS ; ++j )
		{
			delete[] texts[ j ];
			texts[ j ] = NULL;
		}
}

/*==============================================================================================

MENSAGEM cleanSounds
	Libera os sons alocados.

==============================================================================================*/

- ( void ) cleanSounds
{
	#if ( APP_N_SOUNDS > 0 ) && SOUND_WITH_OPEN_AL
	// OBS: Esse trabalho é feito automaticamente por AudioManager
	for( uint8 i = 0 ; i < APP_N_SOUNDS ; ++i )
	{
		delete appSounds[ i ];
		appSounds[ i ] = NULL;
	}
	#endif
}

/*==============================================================================================

MÉTODO cleanFonts
	Libera a memória alocada pelas fontes da aplicação.

==============================================================================================*/

- ( void ) cleanFonts
{
	for( uint8 i = 0 ; i < APP_N_FONTS ; ++i )
	{
		delete appFonts[ i ];
		appFonts[ i ] = NULL;
	}
}

/*==============================================================================================

MENSAGEM onGameOver
	Exibe a tela de fim de jogo.

==============================================================================================*/

- ( void ) onGameOver: ( int32 )score
{
	if( [self saveScoreIfRecord: score] )
		[self performTransitionToView: VIEW_INDEX_RECORDS];
	else
		[self performTransitionToView: VIEW_INDEX_MAIN_MENU];
}

/*==============================================================================================

MENSAGEM saveScoreIfRecord
	Salva a pontuação atual caso esta seja um recorde.

==============================================================================================*/

-( bool ) saveScoreIfRecord: ( int32 )score
{
	int8 highScoreIndex;
	if( ( highScoreIndex = [self setHighScore: score] ) >= 0 )
	{
		[hRecordsView setBlinkingRecord: highScoreIndex];
		return true;
	}
	return false;
}

/*==============================================================================================

MENSAGEM isHighScore
	Retorna o lugar da pontuação na tabela de recordes ou um número negativo caso a jogador
não tenha alcançado um valor que supere o menor recorde.

==============================================================================================*/

- ( int8 ) isHighScore: ( int32 )score
{
	for( int32 i = 0; i < APP_N_RECORDS ; ++i )
	{
		if( score > records[ i ] )
			return i;
	}	
	return -1;		
}

/*==============================================================================================

MENSAGEM setHighScore
	Armazena a pontuação caso esta seja um recorde.

==============================================================================================*/

-( int8 )setHighScore: ( int32 )score
{
	for( uint8 i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		if( score > records[ i ] )
		{
			for( uint8 j = APP_N_RECORDS - 1 ; j > i ; --j )
				records[ j ] = records[ j - 1 ];
			
			records[ i ] = score;

			[self saveRecords];
			
			return i;
		}
	}
	return -1;
}

/*==============================================================================================

MENSAGEM getSaveFileName
	Retorna o nome do arquivo onde devemos salvar os dados do jogo.

==============================================================================================*/

-( NSString* )getSaveFileName
{
	return @"sv0.sv";
}

/*==============================================================================================

MENSAGEM loadRecords
	Carrega possíveis dados salvos.

==============================================================================================*/

-( bool )loadRecords
{
	// Obtém o nome do arquivo de save
	NSString* hFileName = [self getSaveFileName];

	// Verifica se possuímos um diretório onde podemos criar arquivos
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
		#endif

		return false;
	}

	// Verifica se já salvamos recordes 
	NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
	if( ![[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
		return false;
	
	// Carrega os recordes salvos
	FILE *pFile = fopen( [hFilePath UTF8String], "r" );
	if( pFile == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not open file %s for reading", [hFilePath UTF8String] );
		#endif

		return false;
	}
	
	bool ret = true;
	uint32 nRecordsLoaded = 0;

	if( ( nRecordsLoaded = fread( records, sizeof( uint32 ), APP_N_RECORDS, pFile ) ) < APP_N_RECORDS )
	{
		#if DEBUG
			LOG( @"ERROR: Could load only the first %d records", nRecordsLoaded );
		#endif
		
		ret = false;
	}

	fclose( pFile );

	return ret;
}

/*==============================================================================================

MENSAGEM saveRecords
	Salva os dados do jogo.

==============================================================================================*/

-( bool )saveRecords
{
	// Obtém o nome do arquivo de save
	NSString* hFileName = [self getSaveFileName];
	
	// Verifica se possuímos um diretório onde podemos criar arquivos
	NSString* hDocumentsDirectory = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES ) objectAtIndex:0];
	if( hDocumentsDirectory == NULL )
	{
		#if DEBUG
			LOG( @"ERROR: Could not find Documents directory" );	
		#endif

		return false;
	}

	// TODOO : O ideal seria verificar se os recordes atuais são diferentes dos recordes existentes no início desta execução. Para
	// isso, teríamos que criar um array uint32 recordsAtLastRun[ APP_N_RECORDS ] e copiar nele os dados de 'records' em applicationDidFinishLaunching

	// Verifica se existe algum recorde diferente de 0
	uint8 i;
	for( i = 0 ; i < APP_N_RECORDS ; ++i )
	{
		if( records[i] != 0 )
			break;
	}

	if( i >= APP_N_RECORDS )
		return true;
	
	// Monta o nome do arquivo onde iremos salvar os recordes
	NSString* hFilePath = [hDocumentsDirectory stringByAppendingPathComponent: hFileName];
	
	// Abre / cria o arquivo
	FILE* pFile = fopen( [hFilePath UTF8String], "w" );
	if( pFile == NULL )
	{
		#if DEBUG
			if( [[NSFileManager defaultManager] fileExistsAtPath: hFilePath] )
				LOG( @"ERROR: Could not open file %s for writing", [hFilePath UTF8String] );
			else
				LOG( @"ERROR: Could not create file %s", [hFilePath UTF8String] );
		#endif

		return false;
	}
	
	bool ret = true;
	uint32 nRecordsSaved = 0;

	if( ( nRecordsSaved = fwrite( records, sizeof( uint32 ), APP_N_RECORDS, pFile ) ) < APP_N_RECORDS )
	{
		#if DEBUG
			LOG( @"ERROR: Could save only the first %d records", nRecordsSaved );
		#endif
		
		ret = false;
	}

	// Fecha o arquivo
	fclose( pFile );

	return ret;
}

/*==============================================================================================

MENSAGEM isSupportedOS
	Verifica se suportamos o sistema operacional.

==============================================================================================*/

- ( bool )isSupportedOS
{
	return true;

	// OLD
//	NSString *hOSName = [[UIDevice currentDevice] systemName];
//	return [[hOSName stringByReplacingOccurrencesOfString: @" " withString: @""] caseInsensitiveCompare: @"iPhoneOS"] == NSOrderedSame;
}

/*==============================================================================================

MENSAGEM isSupportedOSVersion
	Verifica se suportamos a versão do sistema operacional.

==============================================================================================*/

#define N_SUPPORTED_OS_VERSIONS 4

- ( bool )isSupportedOSVersion
{
	return true;

	// OLD
//	NSString *hVersion = [[UIDevice currentDevice] systemVersion];
//	NSString *hVersions[ N_SUPPORTED_OS_VERSIONS ] = { @"2.0", @"2.1", @"2.2", @"2.2.1" };
//	
//	for( uint8 i = 0 ; i < N_SUPPORTED_OS_VERSIONS ; ++i )
//	{
//		if( [hVersion compare: hVersions[i] ] == NSOrderedSame )
//			return true;
//	}
//	return false;
}

#undef N_SUPPORTED_OS_VERSIONS

/*==============================================================================================

MENSAGEM isOlderOSVersion
	Retorna se a versão do sistema operacional é anterior à 3.0.

==============================================================================================*/
				   
- ( bool )isOlderOSVersion
{
	return true;
	
	// OLD
//	unichar zero = '0';
//	return ( [[[UIDevice currentDevice] systemVersion] characterAtIndex:0] - zero ) < 3;
}				   

// Fim da implementação da classe
@end
