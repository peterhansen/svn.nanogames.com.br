#ifndef PARTICLE_H
#define PARTICLE_H 1

#include "Point3f.h"
#include "RenderableImage.h"
#include "Random.h"
#include "Config.h"
#include "FruitListener.h"

/**
 * Classe interna que descreve uma partÌcula de fogos de artifÌcio. 
 */
class Particle : public RenderableImage {

	public:
	
	Particle( void );
	
	void setFruitListener( FruitListener *pListener );
	
	// Aloca as imagens estáticas da classe
	static bool LoadImages( void );
	
	// Desaloca as imagens estáticas da classe
	static bool ReleaseImages( void );
	
	void born( int8 type, Point3f& bornPosition );
	
	bool update( float delta );
	
	private:
	
	static Texture2DHandler textures[ TOTAL_TYPES ];
	
	/** Tempo acumulado da partÌcula. */
	float accTime;
	
	float speedX;
	float speedY;
	float rotationSpeed;
		
	Point3f initialPos;
	
	FruitListener* pFruitListener;

};

#endif