/*
 *  Config.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 1/16/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef CONFIG_H
#define CONFIG_H 1

/*=====================================
 
INFORMAÇÔES DO JOGO
 
=====================================*/


// Nível a partir do qual a maior dificuldade do jogo é alcançada
#define MAX_DIFFICULTY_LEVEL 15

// Pontuação máxima que um jogador consegue atingir
#define MAX_POINTS 9999999

/** PontuaÁ„o b·sica dada para uma sequÍncia mÌnima (3 frutas). */
#define SCORE_SEQUENCE	10

#define SCORE_EXTRA_FRUIT (SCORE_SEQUENCE >> 1)

/** Maximo de vidas do jogador. */
#define LIVES_MAX 3

// Tempo total de cada nível: 3 minutos
#define LEVEL_TIME ( 3.0f * 60.0f )

/** Duracao da animacao de transicao da agua. */
#define WATER_TRANSITION_TIME 1.05f

#define TIME_HINT_INITIAL	7.0f

#define TIME_HINT_FINAL	0.60f
#define TIME_INITIAL	120.0f
#define TIME_FINAL	12.0f

#define TIME_PER_FRUIT_INITIAL  (TIME_INITIAL / 119.0f)
#define TIME_PER_FRUIT_FINAL	(TIME_FINAL / 146.0f)

#define TIME_ACCELERATE     140.0f
#define TIME_ACCELERATE_2 ( TIME_ACCELERATE * 0.5f )
#define TIME_ACCELERATE_3 ( TIME_ACCELERATE * 0.25f )

#define VIBRATION_SCREEN_OFFSET 6
#define VIBRATION_SCREEN_HALF_OFFSET ( VIBRATION_SCREEN_OFFSET * 0.5f )

#define HINT_TIME_PENALTY ( TIME_FINAL / -10 )

// Índices dos idiomas
#define LANGUAGE_INDEX_PORTUGUESE	0


/** NÌvel onde a dificuldade È m·xima. */
#define LEVEL_MAX 15

// Duração da animação de desaparecimento da peça
#define VANISH_TIME 0.25f

// Número de colunas no tabuleiro
#define TOTAL_COLUMNS 7

// Número de linhas no tabuleiro
#define TOTAL_ROWS 7

// Número total de frutas no tabuleiro
#define TOTAL_FRUITS ( TOTAL_COLUMNS * TOTAL_ROWS )

// Número total de tipos de frutas diferentes
#define TOTAL_TYPES 5

// Indica uso randomico
#define TYPE_RANDOM -1

// Tempo de duracao da movimentacao de uma troca de fruta
#define FRUIT_SWITCH_TIME 0.333f

// Tempo que o botão de pausa fica visível antes de esmaecer
#define SOFT_KEY_VISIBLE_TIME 3.8f

// Definicao das dimensoes da area das frutas do jogo
#define FRUITGROUP_WIDTH  288.0f
#define FRUITGROUP_HEIGHT 288.0f

// Largura das peças do tabuleiro
#define FRUIT_WIDTH ( FRUITGROUP_WIDTH / TOTAL_COLUMNS )

// Altura das peças ddo tabuleiro
#define FRUIT_HEIGHT ( FRUITGROUP_HEIGHT / TOTAL_ROWS )

#define GRAVITY_ACC ( 10.0f * FRUIT_HEIGHT )
#define HALF_GRAVITY_ACC ( GRAVITY_ACC * 0.5f )

#define EMIT_PARTICLES 12
#define	MAX_PARTICLES ( 15 * EMIT_PARTICLES )
#define EXPLOSION_MAX_SPEED_X ( SCREEN_WIDTH / 2.4f )
#define EXPLOSION_MAX_HEIGHT ( -SCREEN_HEIGHT * 0.225f )


/** Espessura da borda do tabuleiro. */
#define BORDER_THICKNESS 4.0f

/** Espessura da borda do tabuleiro. */
#define BORDER_HALF_THICKNESS ( BORDER_THICKNESS * 0.5f )

// Índices das views da aplicação
#define VIEW_INDEX_SELECT_LANG	 0
#define VIEW_INDEX_LOADING		 1
#define VIEW_INDEX_SPLASH_NANO	 2
#define VIEW_INDEX_SPLASH_GAME	 3
#define VIEW_INDEX_MAIN_MENU	 4
#define VIEW_INDEX_GAME			 5
#define VIEW_INDEX_RECORDS		 6
#define VIEW_INDEX_OPTIONS		 7
#define VIEW_INDEX_HELP			 8
#define VIEW_INDEX_CREDITS		 9
#define VIEW_INDEX_PAUSE_SCREEN	10
#define VIEW_INDEX_CONFIRM_SCREEN	11
#define VIEW_INDEX_TIPS         12
#define VIEW_INDEX_NEXT_LEVEL   13
#define VIEW_INDEX_PRODUCT      14
#define VIEW_INDEX_NEW_GAME     15

// Duração padrão das animações de transição de tela
#define DEFAULT_TRANSITION_DURATION 0.75f

// Duração do splash da nano (em segundos)
#define SPLASH_NANO_DURATION 2.0f

/*=====================================
 
CÓDIGOS DE ERRO
 
======================================*/

// Erros que podem terminar a aplicação
#define ERROR_ALLOCATING_DATA	ERROR_USER
#define ERROR_ALLOCATING_VIEWS	ERROR_USER + 1
#define ERROR_CHANGING_COLORS	ERROR_USER + 2
#define ERROR_NO_FRAME_BUFFER	ERROR_USER + 3
#define ERROR_UNSUPPORTED_OS	ERROR_USER + 4

/*=====================================
 
ACELERÔMETRO
 
======================================*/

// Define qual o modo de interpretação dos valores gerados pelos acelerômetros
#define DEFAULT_ACCELEROMETER_CALC_MODE ACC_FORCE_MODE_AVERAGE

// Intervalo com que os acelerômetros reportam seus valores à aplicação
#define ACCELEROMETER_N_UPDATES_PER_SEC 60.0f // OLD 30.0f 
#define ACCELEROMETER_UPDATE_INTERVAL ( 1.0f / ACCELEROMETER_N_UPDATES_PER_SEC )

// Quantos valores acumulamos para calcular a posição de repouso do device
#define ACCEL_REST_POS_CALC_N_VALUES 32

// Liberdade de movimento (em graus) dada ao usuário
#define ACCEL_MOVEMENT_FREEDOM_X 20.0f// OLD 40.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_X ( ACCEL_MOVEMENT_FREEDOM_X * 0.5f )

#define ACCEL_MOVEMENT_FREEDOM_Y 70.0f // OLD 60.0f
#define ACCEL_HALF_MOVEMENT_FREEDOM_Y ( ACCEL_MOVEMENT_FREEDOM_Y * 0.5f )

// Intervalos nos quais ignoramos a inclinação do device:
// [ -ACCEL_IGNORE_MOVE_INTERVAL_Y, ACCEL_IGNORE_MOVE_INTERVAL_Y ]
#define ACCEL_NO_MOVE_Y_INTERVAL 10.0f// OLD 4.0f

// Define o intervalo aceito para o peso dos valores antigos utilzados no cçlculo da média da
// aceleração reportada pelos acelerômetros
#define MIN_OLD_VALUES_WEIGHT 0.0f
#define MAX_OLD_VALUES_WEIGHT 0.9f

/*=====================================
 
CONFIGURAÇÕES DOS COMPONENTES
 
======================================*/

// Define o número máximo de toques tratados pela aplicação
#define MAX_TOUCHES 1

// Duração padrão da vibração, em segundos
#define VIBRATION_TIME_DEFAULT 0.2f

// Define se utilizaremos um depth buffer na aplicação
#define USE_DEPTH_BUFFER 1

// Define se iremos utilizar um buffer para fazer renderizações fora da tela
#define USE_OFFSCREEN_BUFFER 0

// Intervalo padrão do timer responsável pelas atualizações da aplicação
#define DEFAULT_ANIMATION_INTERVAL ( 1.0f / 32.0f ) // 32 fps

// Tempo máximo (em segundos) que consideramos como transcorrido entre uma atualização e outra 
#define MAX_UPDATE_INTERVAL 0.2f

// Configuração do zoom
#define ZOOM_IN_FACTOR 1.1f
#define ZOOM_OUT_FACTOR ( 1.0f / ZOOM_IN_FACTOR )

#define ZOOM_MIN 0.09f
#define ZOOM_MAX 6.00f

#define ZOOM_MIN_DIST_TO_CHANGE 10.0f

/*=====================================
 
RESOURCES
 
=====================================*/

// Número máximo de recordes salvos pela aplicação
#define APP_N_RECORDS 5

// Número de fontes suportado pela aplicação
#define APP_N_FONTS 2


// Índices das fontes no vetor de fontes da aplicação
#define APP_FONT_NUMBERS_SMALL		0
#define APP_FONT_NUMBERS_BIG		1

#define PRODUCTS_TOTAL 9

// Índices dos textos do jogo
#define TEXT_INDEX_POPUP_TITLE				0
#define TEXT_INDEX_POPUP_MSG_GOTOURL		1
#define TEXT_INDEX_POPUP_MSG_NO_OS_SUPPORT	2
#define TEXT_INDEX_POPUP_YES				3
#define TEXT_INDEX_POPUP_NO					4
#define TEXT_INDEX_POPUP_EXIT				5
#define TEXT_INDEX_PRODUCT_0				6
#define TEXT_INDEX_PRODUCT_LAST				( TEXT_INDEX_PRODUCT_0 + PRODUCTS_TOTAL - 1 )

// Número de textos da aplicação
#define APP_N_TEXTS ( TEXT_INDEX_PRODUCT_LAST + 1 )

// Número de sons da aplicação
#define APP_N_SOUNDS 12

// Índices dos sons do jogo
#define SOUND_INDEX_BEAUTY_TIPS		0
#define SOUND_INDEX_TIME_UP			1
#define SOUND_INDEX_AMBIENT_1		2
#define SOUND_INDEX_AMBIENT_2		3
#define SOUND_INDEX_LEVEL_START		4
#define SOUND_INDEX_SPLASH			5
#define SOUND_INDEX_BOMB			6
#define SOUND_INDEX_LEVEL_COMPLETE	7
#define SOUND_INDEX_CLICK			8
#define SOUND_INDEX_SEQUENCE		9
#define SOUND_INDEX_MOVE_ERROR		10
#define SOUND_INDEX_PIECES_FALL		11

#define SOUND_NAME_BEAUTY_TIPS		0
#define SOUND_NAME_TIME_UP			1
#define SOUND_NAME_AMBIENT_1		2
#define SOUND_NAME_AMBIENT_2		3
#define SOUND_NAME_LEVEL_START		4
#define SOUND_NAME_SPLASH			5
#define SOUND_NAME_GAME_OVER		6
#define SOUND_NAME_LEVEL_COMPLETE	7
#define SOUND_NAME_CLICK			8
#define SOUND_NAME_SEQUENCE			9
#define SOUND_NAME_MOVE_ERROR		10
#define SOUND_NAME_PIECES_FALL		11

// quantidade total de músicas ambiente
#define SOUND_AMBIENT_TYPES_TOTAL 2

/** Õndice da sequÍncia de animaÁ„o do 1∫ tipo de fruta. */
#define SEQUENCE_IDLE 0

/** Õndice da sequÍncia da fruta brilhando. */
#define SEQUENCE_SHINING	(SEQUENCE_IDLE + TOTAL_TYPES)

#define SEQUENCE_EXPLODING	(SEQUENCE_SHINING + TOTAL_TYPES)

/** Õndice da sequÍncia de liquefaÁ„o do 1∫ tipo de fruta. */
#define SEQUENCE_LIQUIFY	(SEQUENCE_EXPLODING + TOTAL_TYPES)

/** Õndice de sequÍncia passado ao PlayScreen ao terminar de mover uma fruta. */
#define SEQUENCE_MOVING		(SEQUENCE_LIQUIFY + TOTAL_TYPES)

/** Õndice de sequÍncia passado ao PlayScreen ao terminar um movimento de queda. */
#define SEQUENCE_FALLING	(SEQUENCE_MOVING + TOTAL_TYPES)
#define SEQUENCE_FALLING_BOMB	(SEQUENCE_FALLING + TOTAL_TYPES)

#define CURSOR_STATE_AVAILABLE	    0
#define CURSOR_STATE_MOVING_FRUIT	1
#define CURSOR_STATE_BLOCKED		2
#define CURSOR_STATE_UNDOING		3

#define STATE_IDLE               0
#define STATE_FALLING            1
#define STATE_SWITCHING_H        2
#define STATE_SWITCHING_V        3
#define STATE_LIQUIFYING_VANISH	 4
#define STATE_LIQUIFYING_SOAP_1	 5
#define STATE_LIQUIFYING_SOAP_2	 6 
#define STATE_INVISIBLE          7
#define STATE_BOMB               8
#define STATE_EXPLODING          9 
#define STATE_FALLING_BOMB       10

#define LIQUIFY_TIME 0.358f

#define LIQUIFY_MOVE_TIME 0.303f


#endif
