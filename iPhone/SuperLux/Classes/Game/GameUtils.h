/*
 *  GameUtils.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 2/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef GAME_UTILS_H
#define GAME_UTILS_H

#include <string>

#include "Point3f.h"

namespace GameUtils
{
	// Retorna a angulação do device nos eixos x e y, levando em conta a posição de repouso
	// escolhida pelo usuçrio
	void GetDeviceAngles( float* pAngleX, float* pAngleY, const Point3f* pRestPosition, const Point3f* pAcceleration );
	
	// Formata uma string numérica
	std::string FormatNumStr( int32 value, int32 nChars, char filler, char separator );
	
	// Retorna o primeiro múltiplo de m no intervalo [n, MAX_INT]
	int32 NextMulOf( int32 n, int32 m );
};

#endif

