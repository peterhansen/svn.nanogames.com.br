//
//  SpriteGroup.mm
//  FruitLux
//
//  Created by Hugo on 9/13/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//
#include "SpriteGroup.h"
#include "Config.h"
#include "Sprite.h"
	
SpriteGroup::SpriteGroup( bool vcenter )
			: ObjectGroup(TOTAL_TYPES),
			type( -1 ),
			vcenter( vcenter )
{
}
	
	
void SpriteGroup::idle() {
	 animate(-1);
}


bool SpriteGroup::isIdle() {
	 return type == -1;
}


void SpriteGroup::animate( int8 type ) {
	 this->type = type;
	
	if ( type >= 0 ) {
		setSize( getObject(type )->getSize() );
		setAnchor( ( vcenter ? ANCHOR_VCENTER : ANCHOR_TOP ) | ANCHOR_HCENTER );
	}
	
	for (int8 i=0; i<TOTAL_TYPES; i++) {
		Sprite *pS = static_cast<Sprite*> (getObject( i ) );
		pS->setAnimSequence( 0, false, true);
		pS->setVisible(i==type);
	}
}


bool SpriteGroup::update( float delta ) {
	 if ( type >= 0 )
		 ( ( Sprite* ) getObject( type ))->update( delta );
	return true;
}

// Chamado quando o sprite termina um sequência de animação
void SpriteGroup::onAnimEnded( Sprite* pSprite ) {
	idle();
}
