/*
 *  Fruit.h
 */

#ifndef FRUIT_H
#define FRUIT_H 1

#include "ObjectGroup.h"
#include "RenderableImage.h"
#include "Config.h"
#include "Texture2D.h"
#include "Sprite.h"

class GameScreen;

class Fruit : public ObjectGroup
	{
	public:
		// Construtor
		// Exceções : ConstructorException
		Fruit( GameScreen * pGameScreen );
		
		// Destrutor
		virtual ~Fruit( void ){};
		
		// Reinicializa o objeto
		void reset( void );
		
		// Aloca as imagens estáticas da classe
		static bool LoadImages( void );

		// Desaloca as imagens estáticas da classe
		static bool ReleaseImages( void );

		bool update( float delta );
		
		void refreshId( void );
		
		void idle( void );
		
		bool isIdle();
		
		void reset( int8 column, int8 row, float yOffset );
		
		void positionByCell( int8 column, int8 row, float yOffset );
	
		void moveToCell( Point2i cell, int8 moveType );
		
		void moveToCell( int8 column, int8 row, int8 moveType );

		void moveToCell( int8 column, int8 row, int8 moveType, float initialYSpeed );
			
		void moveTo( Point3f destination, int8 moveType, float initialYSpeed );
		
		void liquify( Point3f destination, bool turnToSoap );
		
		int8 getState();

		int8 getType();
		
		void setType( int8 type );

		void setState( int8 state );
		
		void setShining( bool active );
		
		inline float getInitialYSpeed() {
			return initialYSpeed;
		};		
		
		float getRandomInitialYSpeed();
		
		float getRandomDiffYSpeed();
		
		bool equals( const Fruit* f ) const;
		
		bool operator== ( const Fruit& dest){return equals( &dest);};
		bool operator!= ( const Fruit& dest){return !(equals( &dest));};
		
		Point2i getCell();
		
	private:
		// Inicializa o objeto
		void build( void );
		
		// Imagens das frutas
		//Texture2DHandler * textures;
		static Texture2DHandler textures[ TOTAL_TYPES ];
		static Sprite* pStaticBrilho;
		
		RenderableImage * pFruit;
		GameScreen * pGameScreen;
		Sprite *pBrilho;
		Point3f destination;
		int8 fruit_id;
		int8 state;
		int8 type;
		int8 moveType;
		float moveTime;
		float yStart;
		float initialYSpeed;
		float speed;
	};

#endif
