//
//  SpriteGroup.h
//  FruitLux
//
//  Created by Hugo on 9/13/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#include "ObjectGroup.h"
#include "Sprite.h"

#ifndef SPRITEGROUP_H
#define SPRITEGROUP_H 1

class SpriteGroup : public ObjectGroup, public SpriteListener
{
public:
			
	int8 type;
	
	SpriteGroup( bool vcenter );
	
	void idle( void );
	
	
	bool isIdle( void );
	
	
	void animate( int8 type );
	
	
	bool update( float delta );
	
	// Chamado quando o sprite termina um sequência de animação
	void onAnimEnded( Sprite* pSprite );
	
private:
	bool vcenter;
	
};

#endif
