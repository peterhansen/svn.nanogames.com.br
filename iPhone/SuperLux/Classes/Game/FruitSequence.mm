/*
 *  FruitSequence.mm
 *  FruitLux
 *
 *  Created by Hugo on 8/22/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include "FruitSequence.h"

void FruitSequence::addFruit( Fruit* pFruit)
{
	if (pFruit)
		sequence.push_back( pFruit );
}

/*std::vector<Fruit> FruitSequence::getSequence()
{
	return sequence;
}*/

bool FruitSequence::intersects( FruitSequence * pOther ) {

	if (pOther) {
		for ( int8 i = 0; i < sequence.size(); ++i ) {
			for ( int8 j = 0; j < pOther->size(); ++j ) {
				if ( at( i ) == pOther->at( j ) )
					return true;
			}
		}
	}
	
	return false;
}

