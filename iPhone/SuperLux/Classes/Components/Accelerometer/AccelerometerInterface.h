/*
 *  AccelerometerInterface.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/15/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACCELEROMETER_INTERFACE_H
#define ACCELEROMETER_INTERFACE_H 1

#import <UIKit/UIKit.h>
#include "AccInterfaceListener.h"

@interface AccelerometerInterface : NSObject <UIAccelerometerDelegate>
{
	@private
		// Objeto que irá receber os eventos dos acelerômetros
		AccInterfaceListener* pListener;
	
		// Indica se o sistema operacional é mais antigo que o iPhoneOS 3.0
		// Nessas versões, desativar e ativar os acelerômetros através de chamadas de UIAccelerometer
		// durante a aplicação pode causar bugs. Um deles é que UIAccelerometerDelegate pára de 
		// receber os eventos accelerometer:didAccelerate: mesmo com [[UIAccelerometer sharedAccelerometer] delegate]
		// sendo válido
		bool osVersionOlderThan3_0;
	
		// Indica se a interface estava ativa antes de recebermos um evento de suspend. Assim saberemos se devemos
		// reativá-la quando recebermos um evento de resume
		bool wasActiveBeforeSuspend;
	
		// Workaround para corrigir o bug descrito acima
		AccInterfaceListener* pActiveListener;
}

@property (nonatomic, assign, setter=setListener) AccInterfaceListener* pListener;

// Construtor
- ( id ) init;

// Determina a frequência com que o usuário deseja receber os eventos do acelerômetro
- ( void ) setUpdateInterval:( float )secs;

// Começa a receber os eventos do acelerômetro
- ( void ) startSendingEvents;

// Pára de receber os eventos do acelerômetro
- ( void ) stopSendingEvents;

// Indica se está enviando eventos para o listener
- ( bool ) isSendingEvents;

// Pára o processamento dos acelerômetros
- ( void ) suspend;

// Reinicia o processamento dos acelerômetros
- ( void ) resume;

@end

#endif

