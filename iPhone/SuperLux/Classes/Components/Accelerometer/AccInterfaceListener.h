/*
 *  AccInterfaceListener.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 2/17/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef ACC_INTERFACE_LISTENER_H
#define ACC_INTERFACE_LISTENER_H 1

#include "Point3f.h"

class AccInterfaceListener
{
	public:
		// Destrutor
		virtual ~AccInterfaceListener( void ){};

		// Recebe a aceleração reportada pelos acelerômetros
		virtual void setAccelerometerEvent( const Point3f* pAcceleration ) = 0;
};

#endif
