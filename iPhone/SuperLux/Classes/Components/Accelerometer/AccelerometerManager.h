/*
 *  AccelerometerManager.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/9/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef ACC_MANAGER_H
#define ACC_MANAGER_H 1

// TASK : Agrupar esta classe em EventManager !!!!!!!!!!

#include "AccelerometerInterface.h"
#include "AccelerometerListener.h"
#include "AccInterfaceListener.h"
#include "NanoTypes.h"

// TODOO : Ser dinâmico !!!!
// Número máximo de listeners que o controlador pode ter
#define ACC_MANAGER_N_LISTENERS 16

// TODOO : Ser dinâmico !!!!
// Define quantos valores de aceleração são utilizados para os cálculos dos modos de
// detecção de movimento
#define ACCELEROMETER_HISTORY_LEN 30

// TODOO
// Número de amostras utilizadas para se calcular a posição de repouso
//#define REST_POS_N_SAMPLES 50

class AccelerometerManager : public AccInterfaceListener
{
	public:
		// Inicializa o singleton. Poderíamos deixar a inicialização para a 1a chamada de GetInstance(). No entanto, isso
		// poderia resultar em exceções disparadas no meio do código da aplicação, o que impactaria
		// o desempenho devido a inserção do código de tratamento necessário. Este método deve ser
		// chamado no início da aplicação, já que o usuário dos componentes parte do princípio que
		// sempre GetInstance() sempre retornará um valor válido 
		static bool Create( float updateInterval );
	
		// Deleta o singleton
		static void Destroy( void );
	
		// Retorna a instância do objeto singleton
		static AccelerometerManager* GetInstance( void ) { return pSingleton; };

		// Determina que um objeto deve começar a receber os eventos do acelerômetro
		int16 insertListener( AccelerometerListener* pNewListener );
	
		// Indica que um objeto deve parar de receber os eventos do acelerômetro
		bool removeListener( int16 index );

		// Determina a frequência com que o usuário deseja receber os eventos do acelerômetro
		void setUpdateInterval( float secs );

		// Começa a receber os eventos do acelerômetro
		void startAccelerometers( void );

		// Pára de receber os eventos do acelerômetro
		void stopAccelerometers( void );
	
		// Tratam eventos de suspend e resume da aplicação
		void resume( void );
		void suspend( void );
	
		// TODOO
//		// Inicia a captura de valores de aceleração que serão utilizados para determinar a
//		// posição de repouso do device
//		void startRestPositionCapture( void );
//	
//		// Pára a captura de valores de aceleração que serão utilizados para determinar a
//		// posição de repouso do device
//		void stopRestPositionCapture( void );
	
		// Determina a posição de repouso
		inline void setRestPosition( const Point3f* pRestPosition ){ restPosition = *pRestPosition; };
	
		// Obtém a posição de repouso
		inline const Point3f* getRestPosition( void ) const { return &restPosition; };
	
	private:
		// Construtor
		AccelerometerManager( float updateInterval );
	
		// Destrutor
		virtual ~AccelerometerManager( void );
	
		// Recebe a aceleração reportada pelos acelerômetros
		virtual void setAccelerometerEvent( const Point3f* pAcceleration );
	
		// Armazena o hitórico dos valores reportados pelos acelerômetros
		void setHistory( uint8 listenerIndex, const Point3f* pNewAccelData );
	
		// Obtém a média histórica dos valores reportados pelos acelerômetros
		Point3f* getHistoryAverage( Point3f* pAverage, uint8 listenerIndex ) const;
	
		// Objeto que irá receber os eventos do acelerômetro
		AccelerometerListener* listeners[ ACC_MANAGER_N_LISTENERS ];
	
		// Intervalo de atualização do acelerômetro
		float updateInterval;

		// Interface da API do acelerômetro
		AccelerometerInterface* hInterface;
	
		// Armazenam os últimos valores recebidos
		uint8 accelHistoryCounter;
		Point3f acceleration[ ACC_MANAGER_N_LISTENERS ];
	
		// É melhor que não seja Point3f!!! Ver setAccelerometerEvent() case ACC_REPORT_MODE_POLINOMIAL_2
		float accelHistory[ ACC_MANAGER_N_LISTENERS ][3][ ACCELEROMETER_HISTORY_LEN ];
	
		// Posição de repouso dos acelerômetros	
		// TODOO : Este é o jeito correto, pois podemos chegar ao valor correto em relação a qualquer tipo de amostragem
//		bool capturingRestPos;
//		uint8 restPosSampleCounter;
//		Point3f restPositionSamples[ REST_POS_N_SAMPLES ];
		Point3f restPosition;// Desse jeito não está correto, pois não leva em consideração os diversos tipos de amostragem
	
		// Única instância da classe
		static AccelerometerManager* pSingleton;
};

#endif

