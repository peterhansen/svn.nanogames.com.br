#include "ResourceManager.h"

#warning ResourceManager TODOs
#warning Criar uma tabela para verificarmos se um resource já está na memória !!!!

/*==============================================================================================

 MÉTODO GetTexture2D
	Cria uma textura 2D. Reaproveita o handler caso um resource com o mesmo identificador
já tenha sido alocado.
 
==============================================================================================*/

Texture2DHandler ResourceManager::GetTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	// TODOO TASK : Verificar se a textura já foi alocada!!!!!!!!!!
	
	Texture2D* pTex = new Texture2D();
	if( !pTex || !pTex->loadTextureInFile( pFileName, pTexDesc ))
	{
		DESTROY( pTex );
		// TODOO : Disparar uma exceção aki!!!!
	}
	return Texture2DHandler( pTex );
}

/*==============================================================================================

 MÉTODO Get16BitTexture2D
	Cria uma textura 2D de 16 bits sem compressão.
 
==============================================================================================*/

Texture2DHandler ResourceManager::Get16BitTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	// TODOO TASK : Verificar se a textura já foi alocada!!!!!!!!!!
	
	Texture2D* pTex = new Texture2D();
	if( !pTex || !pTex->load16BitTexInFile( pFileName, pTexDesc ) )
	{
		DESTROY( pTex );
		// TODOO : Disparar uma exceção aki!!!!
	}
	return Texture2DHandler( pTex );
}

/*==============================================================================================

 MÉTODO GetPVRCTexture2D
	Cria uma textura 2D com compressão. Reaproveita o handler caso um resource com o mesmo identificador
já tenha sido alocado.
 
==============================================================================================*/

Texture2DHandler ResourceManager::GetPVRCTexture2D( const char *pFileName, const TextureFrameDescriptor* pTexDesc )
{
	// TODOO TASK : Verificar se a textura já foi alocada!!!!!!!!!!
	
	Texture2D* pTex = new Texture2D();
	if( !pTex || !pTex->loadPVRCTexInFile( pFileName, pTexDesc ))
	{
		DESTROY( pTex );
		// TODOO : Disparar uma exceção aki!!!!
	}
	return Texture2DHandler( pTex );
}
