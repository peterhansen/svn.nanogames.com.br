#include "Triangle.h"

/*==============================================================================================

CONSTRUTOR
 
==============================================================================================*/

Triangle::Triangle( const Point3f* pV0, const Point3f* pV1, const Point3f* pV2 )
{
	set( pV0, pV1, pV2 );
}

/*==============================================================================================

MÉTODO getWithBarycentricCoords
	Retorna um ponto interno ao triângulo, utilizando as coordenadas baricêntricas u e v.

==============================================================================================*/

Point3f* Triangle::getWithBarycentricCoords( Point3f* pOut, float u, float v )
{
	// T( u, v ) = ( 1 - u - v ) * V0 + u * V1 + v * V2
	*pOut = (( 1.0f - u - v ) * vertexes[0] ) + ( u * vertexes[1] ) + ( v * vertexes[2] );
	return pOut;
}