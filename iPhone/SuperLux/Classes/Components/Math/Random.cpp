#include "Random.h"

#include "MathFuncs.h"
#include "SFMT.h"

#include <cstdlib> // versão inteira de abs()
#include <time.h>

// Inicializa a variável estática da classe
bool Random::seeded = false;

/*===========================================================================================

MÉTODO SetSeed
	Determina o inicializador do gerador de números randômicos.

============================================================================================*/

void Random::SetSeed( int32 seed )
{
	init_gen_rand( seed );
	seeded = true;
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro.

============================================================================================*/

int32 Random::GetInt( void )
{
	if( !seeded )
		SetSeed( GetDefaultSeed() );
	
	return gen_rand32();
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro no intervalo desejado.

============================================================================================*/

int32 Random::GetInt( int32 minValue, int32 maxValue )
{
	return ( abs( static_cast< int32 >( GetInt() ) ) % ( maxValue - minValue + 1 ) ) + minValue;
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo [0.0f, 1.0f].

============================================================================================*/

float Random::GetFloat( void )
{
	return static_cast< float >( to_real1( Random::GetInt() ) );
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo [0.0f, maxValue]
	Equivale a chamar NextDouble( 0.0f, maxValue )

============================================================================================*/

float Random::GetFloat( float maxValue )
{
	return GetFloat( 0.0f, maxValue );
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo definido.

============================================================================================*/

float Random::GetFloat( float minValue, float maxValue )
{
	if( !seeded )
		SetSeed( GetDefaultSeed() );

	// [ 0.0f, 1.0f ]
	const float v = static_cast< float >( genrand_real1() );
	return NanoMath::lerp( v, minValue, maxValue );
}

/*===========================================================================================

MÉTODO GetDefaultSeed
	Retorna um número para ser utilizado na inicialização da máquina de geração de números
aleatórios.

============================================================================================*/

int32 Random::GetDefaultSeed( void )
{
	time_t timeInfo = time( NULL );

	if( timeInfo != ( time_t )-1 )
		return ( int32 )timeInfo;

	// Retorna qualquer coisa
	return 81728193;
}
