/*
 *  Bezier.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 7/24/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BEZIER_CURVE_H
#define BEZIER_CURVE_H 1

#include "Point3f.h"
#include "NanoTypes.h"

#include <stdio.h> // Definição de NULL

/**
 *	Classe utilizada para calcular uma curva de Bezier a partir de 4 pontos
 *	dados. Código adaptado de: http://en.wikipedia.org/wiki/B%C3%A9zier_curve
 */

class BezierCurve
{
	public:
		// Construtor
		BezierCurve( const Point3f* pOrigin = NULL, const Point3f* pControl1 = NULL, const Point3f* pControl2 = NULL, const Point3f* pDestination = NULL );
	
		// Inicializa o objeto
		void set( const Point3f* pOrigin = NULL, const Point3f* pControl1 = NULL, const Point3f* pControl2 = NULL, const Point3f* pDestination = NULL );
		
		// Retorna o ponto da curva referente a determinada porcentagem do movimento
		Point3f* getPointAt( Point3f* pOut, float percent ) const;
	
		// Retorna a coordenada x do ponto da curva referente a determinada porcentagem do movimento.
		float getXAt( float percent ) const;
		
		// Retorna a coordenada y do ponto da curva referente a determinada porcentagem do movimento.
		float getYAt( float percent ) const;

		// Retorna a coordenada z do ponto da curva referente a determinada porcentagem do movimento.
		float getZAt( float percent ) const;
	
		// Métodos get
		Point3f* getOrigin( Point3f* pOut ) const;
		Point3f* getControl1( Point3f* pOut ) const;
		Point3f* getControl2( Point3f* pOut ) const;
		Point3f* getDestination( Point3f* pOut ) const;

	private:	
		// Retorna o valor da componente em um dado momento da curva
		float evaluate( float orig, float ctrl1, float ctrl2, float dest, float percent ) const;

		// Pontos de controle da curva Bezier
		Point3f origin, control1, control2, destination;
};

// Implementação dos métodos inline

inline Point3f* BezierCurve::getOrigin( Point3f* pOut ) const
{
	*pOut = origin;
	return pOut;
}

inline Point3f* BezierCurve::getControl1( Point3f* pOut ) const
{
	*pOut = control1;
	return pOut;
}

inline Point3f* BezierCurve::getControl2( Point3f* pOut ) const
{
	*pOut = control2;
	return pOut;
}

inline Point3f* BezierCurve::getDestination( Point3f* pOut ) const
{
	*pOut = destination;
	return pOut;
}

#endif
