/*
 *  Bitwise.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 5/27/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef BITWISE_H
#define BITWISE_H 1

#include "NanoTypes.h"

namespace Bitwise
{
	// Retorna se x é par
	inline bool isEven( int32 x ){ return ( x & 0x1 ) == 0; };

	// Retorna se x é uma potência de 2
	inline bool isPowOf2( int32 x ){ return !( x & ( x - 1 ) ) && x; };
	
	// Retorna a próxima potência de 2 em relação a n
	int32 getNextPowerOf2( int32 n );
};

#endif
