#include "Random.h"
#include "SFMT.h"
#include "Macros.h"

#define DEFAULT_SEED [[NSDate date] timeIntervalSince1970]

// Inicializa a variável estática da classe
bool Random::seeded = false;

/*===========================================================================================

MÉTODO SetSeed
	Determina o inicializador do gerador de números randômicos.

============================================================================================*/

void Random::SetSeed( uint32 s )
{
	init_gen_rand( s );
	seeded = true;
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro.

============================================================================================*/

int32 Random::GetInt( void )
{
	if( !seeded )
		SetSeed( DEFAULT_SEED );
	
	return gen_rand32();
}

/*===========================================================================================

MÉTODO GetInt
	Obtém um número randômico inteiro no intervalo desejado.

============================================================================================*/

int32 Random::GetInt( int32 minValue, int32 maxValue )
{
	return ( abs( GetInt() ) % ( maxValue - minValue + 1 ) ) + minValue;
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real.

============================================================================================*/

float Random::GetFloat( void )
{
	return ( float )to_real1( Random::GetInt() );
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo [0.0f, maxValue]
	Equivale a chamar NextDouble( 0.0f, maxValue )

============================================================================================*/

float Random::GetFloat( float maxValue )
{
	return GetFloat( 0.0f, maxValue );
}

/*===========================================================================================

MÉTODO GetFloat
	Obtém um número randômico real no intervalo definido.

============================================================================================*/

float Random::GetFloat( float minValue, float maxValue )
{
	if( !seeded )
		SetSeed( DEFAULT_SEED );

	// [ 0.0f, 1.0f ]
	const float v = ( float )genrand_real1();
	return LERP( v, minValue, maxValue );
}
