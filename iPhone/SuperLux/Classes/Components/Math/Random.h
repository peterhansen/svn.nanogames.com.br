/*
 *  Random.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 3/2/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_RANDOM
#define NANO_RANDOM 1

#include "NanoTypes.h"

class Random
{
	public:
		// Determina o inicializador do gerador de números randômicos
		static void SetSeed( int32 seed );
	
		// Obtém um número randômico inteiro
		static int32 GetInt( void );
	
		// Obtém um número randômico inteiro no intervalo desejado
		static int32 GetInt( int32 minValue, int32 maxValue );
	
		// Obtém um número randômico real no intervalo [0.0f, 1.0f]
		static float GetFloat( void );
	
		// Obtém um número randômico real no intervalo [0.0f, maxValue]
		// Equivale a chamar NextDouble( 0.0f, maxValue )
		static float GetFloat( float maxValue );
	
		// Obtém um número randômico real no intervalo definido 
		static float GetFloat( float minValue, float maxValue );
	
	private:
		// Retorna um número para ser utilizado na inicialização da máquina de geração de números aleatórios
		static int32 GetDefaultSeed( void );

		// Indica se um inicializador já foi passado para o gerador de números randômicos
		static bool seeded;
};

#endif
