/*
 *  AudioListener.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 12/4/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef AUDIO_LISTENER_H
#define AUDIO_LISTENER_H 1

#if COMPONENTS_CONFIG_ENABLE_OPENAL

#include <OpenAL/al.h>
#include <OpenAL/alc.h>

#include "Point3f.h"

// Pré-definição da classe AudioDevice para evitar #includes recursivos
class PlaybackAudioDevice;

class AudioListener
{
	public:
		// Destrutor
		~AudioListener( void ){};
	
		// Determina o "ganho" do listener. O valor passado deve ser positivo
		bool setGain( float gain );
	
		// Obtém o "ganho" do listener. O valor retornado será sempre positivo
		bool getGain( float* pGain );
	
		// Determina a posição do listener
		bool setPosition( const Point3f* pPos );
	
		// Obtém a posição do listener
		bool getPosition( Point3f* pPos );
	
		// Determina a velocidade do listener
		bool setSpeed( const Point3f* pSpeed );
	
		// Obtém a velocidade do listener
		bool getSpeed( Point3f* pSpeed );
	
		// Determina a orientação do listener
		bool setOrientation( const Point3f* pOrientation );
	
		// Obtém a orientação do listener
		bool getOrientation( Point3f* pOrientation );
	
	protected:
		// Objetos da classe AudioDevice terão acesso aos métodos e atributos protected de AudioListener
		friend class PlaybackAudioDevice;
	
		// Construtor
		AudioListener( void ){};
		
	private:
		// Métodos auxiliares
		ALint SetListenerFV( ALenum param, const ALfloat* pValues );
		ALint GetListenerFV( ALenum param, ALfloat* pValues );
	
		ALint SetListenerF( ALenum param, ALfloat value );
		ALint GetListenerF( ALenum param, ALfloat* pValue );
};

#endif COMPONENTS_CONFIG_ENABLE_OPENAL

#endif AUDIO_LISTENER_H
