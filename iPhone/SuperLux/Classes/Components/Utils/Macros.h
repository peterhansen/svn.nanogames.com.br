/*
 *  Macros.h
 *  OGLESTest
 *
 *  Created by Daniel Lopes Alves on 9/23/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef MACROS_H
#define MACROS_H 1

#import <UIKit/UIKit.h>
#import <CoreGraphics/CGGeometry.h>

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

#include "ApplicationManager.h"
#include "Utils.h"

// Para utilizarmos em métodos OpenGL nos quais 0 representa o cancelamento da operação, como
// em glBindTexture( GL_TEXTURE_2D, 0 )
#ifndef GL_NONE
	#define GL_NONE 0
#endif

// Deleta um ponteiro e coloca seu valor como NULL
#define DESTROY( p ) { delete p; p = NULL; }
#define DESTROY_VEC( p ) { delete[] p; p = NULL; }

// Testa se um ponteiro é nulo antes de deletá-lo
#define SAFE_DELETE( p ) { if( p ) DESTROY( p ) }
#define SAFE_DELETE_VEC( p ) { if( p ) DESTROY_VEC( p ) }

#ifdef __OBJC__

// Testa se um handler é nulo antes de chamar sua mensagem release
#define KILL( h ) { [h release]; h = NULL; }
#define SAFE_RELEASE( h ) { if( h )KILL( h ) }

// Converte um número inteiro para o tipo NSString
#define INT_TO_NSSTRING( v ) [NSString stringWithFormat:@"%d", static_cast< int32 >( v )]

#define NSSTRING_TO_CHAR_ARRAY( string ) [string UTF8String]

// Converte um array de caracteres para o tipo NSString
#define CHAR_ARRAY_TO_NSSTRING( charArray ) [NSString stringWithUTF8String:charArray]

// Converte std::string para NSString
#define STD_STRING_TO_NSSTRING( string ) [NSString stringWithCString:string.c_str()]

// Obtém a largura da tela
#define SCREEN_WIDTH [(( ApplicationManager* )APP_DELEGATE) getScreenWidth]
#define HALF_SCREEN_WIDTH ( SCREEN_WIDTH * 0.5f )

// Obtém a altura da tela
#define SCREEN_HEIGHT [(( ApplicationManager* )APP_DELEGATE) getScreenHeight]
#define HALF_SCREEN_HEIGHT ( SCREEN_HEIGHT * 0.5f )

// Obtém o viewport default para a aplicação
#define GET_DEFAULT_VIEWPORT() [(( ApplicationManager* )APP_DELEGATE) getDefaultViewport]

// Obtém o delegate da aplicação
#define APP_DELEGATE [ApplicationManager GetInstance]

// Funções de log para as versões debug
// TASK : Passar a usar char array !!!
#if DEBUG
	#define LOG( s, ... ) NSLog( s, ##__VA_ARGS__ )
	#define GLLOG() Utils::glLog( __FILE__, __LINE__ )

	#define LOG_FREE_MEM( string ) LOG( @"Free Mem %s: %.4f", string, Utils::GetFreeMemory() );
#endif

#endif // #ifdef __OBJC__

#endif // #ifndef MACROS_H
