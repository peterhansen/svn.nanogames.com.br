/*
 *  Utils.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 10/16/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef UTILS_H
#define UTILS_H 1

#include "Camera.h"
#include "Line.h"
#include "Matrix4x4.h"
#include "NanoRect.h"
#include "Object.h"
#include "Ray.h"
#include "Triangle.h"
#include "Viewport.h"

#if COMPONENTS_CONFIG_ENABLE_OPENAL
	#include "AudioContext.h"
#endif

class Utils
{
	public:
		// Retorna o tempo transcorrido desde o início da execução do programa
		static double GetElapsedTime( void );
		
		// Escreve a string no arquivo de log padrão
		static bool Log( const char* pFormatStr, ... );

#if DEBUG
		// Loga o erro do openGL na janela console
		static void glLog( const char* pFileName, const unsigned long line );
#endif

		// Retorna se o raio colide com o triângulo. Retorna as coordenadas baricêntricas em u, v e a distância para a origem do raio em t 
		static bool IntersectRayTriangle( bool testCull, const Ray* pRay, const Triangle* pTriangle, float* t, float* u, float* v );
	
		// Cria um raio a partir das coordenadas da tela recebidas
		static Ray* GetPickingRay( Ray* pRay, const Camera* pCamera, int32 x, int32 y );
	
		// Leva as coordenadas do espaço da tela para o espaço local
		static bool Unproject( Point3f* pUnprojected, const Camera* pCamera, const Point3f* pPoint );
	
		// Leva as coordenadas do espaço local para o espaço da tela
		static bool Project( Point3f* pProjected, const Camera* pCamera, const Point3f* pPoint );
	
		// Transforma as coordenadas do raio utilizando a matriz 4x4 apontada por pTransformMtx
		static void TransformRay( Ray* pRay, const Matrix4x4* pTransformMtx );
	
		// Transforma o ponto, pPoint(x, y, z, 1), pela matriz, pMtx, projetando o resultado de volta ao
		// plano w = 1
		static Point3f* TransformCoord( Point3f* pOut, const Point3f* pPoint, const Matrix4x4* pMtx );
	
		// Transforma o vetor, pNormal(x, y, z, 0), pela matriz pMtx
		static Point3f* TransformNormal( Point3f* pOut, const Point3f* pNormal, const Matrix4x4* pMtx );
	
		// Retorna a matriz de projeção que está sendo utilizada pela API gráfica
		static Matrix4x4* GetProjectionMatrix( Matrix4x4* pOut );
	
		// Retorna a matriz de modelo/visão que está sendo utilizada pela API gráfica
		static Matrix4x4* GetModelViewMatrix( Matrix4x4* pOut );
	
		// Retorna o viewport que está sendo utilizado pela API gráfica
		static Viewport* GetViewport( Viewport* pOut );
	
		// Obtém uma matriz de rotação utilizando as funcionalidades da API gráfica
		static Matrix4x4* GetRotationMatrix( Matrix4x4* pOut, float angle, const Point3f* pAxis );
		static Matrix4x4* GetRotationMatrix( Matrix4x4* pOut, float angle, float axisX, float axisY, float axisZ );

		// Supondo um segmento de reta entre os pontos K e L e outro entre os pontos M e N,
		// esta função determina os valores dos parâmentros S (da reta KL) e T(da reta MN)
		// no ponto de intersecção das retas-suporte dos segmentos passados como parâmetro.
		// Se S e T retornados pela função estiverem entre 0 e 1, então a interseção dos
		// segmentos existe
		// s: Valor do parâmetro no ponto de interseção (sobre a reta KL)
		// t: Valor do parâmetro no ponto de interseção (sobre a reta MN) 
		static bool GetLineIntersection2D( float& s, float& t, const Line& kl, const Line& mn );
	
		// Obtém o ângulo e o eixo de rotação entre dois vetores
		static float FindRotationBetweenVectors( Point3f* pOut, const Point3f* pV1, const Point3f* pV2 );
	
#if COMPONENTS_CONFIG_ENABLE_OPENAL

		// Carrega um arquivo de áudio
		static void* GetAudioFile( const char* pFileName, const char* pFileType, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq );
		static void* GetAudioFile( const NSString* pFileName, const NSString* pFileType, ALsizei* pSize, ALenum* pFormat, ALsizei* pFreq );
	
		// Carrega uma fonte de áudio contendo o arquivo passado como parâmetro
		static AudioSource* GetAudioSource( AudioContext* pContext, const char* pFileName, const char* pFileType );
	
#endif

		// Retorna a path do recurso contido no pacote da aplicação
		static NSString* GetPathForResource( const NSString* hResourceName, const NSString* hResourceType );
	
		// Retorna a URL do recurso contido no pacote da aplicação.
		static NSURL* GetURLForResource( const NSString* hResourceName, const NSString* hResourceType );
	
		// Mapeia o ponto de acordo com a orientação do device
		static CGPoint MapPointByOrientation( const CGPoint* pPoint );
		static Point3f MapPointByOrientation( const Point3f* pPoint );
	
		// Retorna se o ponto está dentro do retângulo
		static bool IsPointInsideRect( const Point3f* pPoint, const Object* pObj );
		static bool IsPointInsideRect( const Point3f* pPoint, float x, float y, float width, float height );
	
		//Retorna o tamanho mínimo e o tamanho máximo que o hardware suporta para um ponto
		static void GetPointSizesInfo( float* pMinPointSize, float* pMaxPointSize );
	
		// Retorna a quantidade de memória livre
		static float GetFreeMemory( void );
	
		// Faz o teste de colisão entre dois quads
		static bool CheckRectCollision( const NanoRect *pRect1, const NanoRect *pRect2 );
};

#endif
