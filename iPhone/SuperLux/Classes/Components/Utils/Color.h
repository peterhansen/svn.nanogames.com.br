#ifndef COLOR_H
#define COLOR_H 1

/*
 *  Color.h
 *
 *  Created by Daniel Lopes Alves on 9/30/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#include "NanoTypes.h"

// Cores pré-definidas
typedef enum PreDefinedColors
{
	COLOR_BLACK = 0,
	COLOR_GREY,
	COLOR_WHITE,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_BLUE,
	COLOR_YELLOW,
	COLOR_MAGENTA,
	COLOR_CYAN,
	COLOR_PURPLE,
	COLOR_BROWN,
	COLOR_ORANGE,
	COLOR_TRANSPARENT
}PreDefinedColors;

class Color
{
	public:
		// Componentes da cor
		float r, g, b, a;
		
		// Construtores
		Color( void );
		Color( float red, float green, float blue, float alpha );
		Color( uint8 red, uint8 green, uint8 blue, uint8 alpha );
	
		explicit Color( uint32 color );
		Color( PreDefinedColors color );

		// Obtém a representação em byte do valor da componente R da cor
		inline uint8 getByteR( void ) const { return static_cast< uint8 >( r * 255.0f ); };
	
		// Obtém a representação em byte do valor da componente G da cor
		inline uint8 getByteG( void ) const { return static_cast< uint8 >( g * 255.0f ); };
	
		// Obtém a representação em byte do valor da componente B da cor
		inline uint8 getByteB( void ) const { return static_cast< uint8 >( b * 255.0f ); };
	
		// Obtém a representação em byte do valor da componente A da cor
		inline uint8 getByteA( void ) const { return static_cast< uint8 >( a * 255.0f ); };

		// Inicializa o objeto
		inline void set( float red = 0.0f, float green = 0.0f, float blue = 0.0f, float alpha = 1.0f ) { r = red; g = green; b = blue; a = alpha; };
		inline void set( uint8 red, uint8 green, uint8 blue, uint8 alpha = 255 ) { set( red / 255.0f, green / 255.0f, blue / 255.0f, alpha / 255.0f ); };
		inline void set( uint32 color ) { r = (( color >> 24 ) & 0xFF ) / 255.0f; g = (( color >> 16 ) & 0xFF ) / 255.0f; b = (( color >> 8 ) & 0xFF ) / 255.0f; a = ( color & 0xFF ) / 255.0f; };
		void set( PreDefinedColors color );
	
		// Operadores de comparação
		bool operator == ( const Color& c ) const;
		bool operator != ( const Color& c ) const;	
};

#endif