/*
 *  Touch.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 3/5/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef TOUCH_H
#define TOUCH_H 1

#include "NanoTypes.h"
#include "Point3f.h"

typedef struct Touch
{
	// Construtores
	Touch( void ) : Id( 0 ), startPos(), unmappedStartPos() {};
	Touch( int32 Id, Point3f& startPos, Point3f& unmappedStartPos ) : Id( Id ), startPos( startPos ), unmappedStartPos( unmappedStartPos ) {};

	// Identificação única do toque
	int32 Id;
	
	// Posição na qual o toque foi iniciado
	Point3f startPos;
	
	// Posição na qual o toque foi iniciado, ignorando a correção feita pela orientação do
	// device
	Point3f unmappedStartPos;

} Touch;

#endif
