/*
 *  NanoTypes.h
 *  iBob
 *
 *  Created by Daniel Lopes Alves on 10/29/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_TYPES_H
#define NANO_TYPES_H 1

#include <TargetConditionals.h>

#if defined( TARGET_OS_MAC )
#include <sys/types.h>
#include <stdint.h>
#else
#error Unknown API architecture.
#endif

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _INT64
typedef int64_t int64;
#define _INT64
#endif

#ifndef _UINT64
typedef uint64_t uint64;
#define _UINT64
#endif

#ifndef _INT32
typedef int32_t int32;
#define _INT32
#endif

#ifndef _UINT32
typedef uint32_t uint32;
#define _UINT32
#endif

#ifndef _INT16
typedef int16_t int16;
#define _INT16
#endif

#ifndef _UINT16
typedef uint16_t uint16;
#define _UINT16
#endif

#ifndef _INT8
typedef int8_t int8;
#define _INT8
#endif

#ifndef _UINT8
typedef uint8_t uint8;
#define _UINT8
#endif
	
#ifdef __cplusplus
}
#endif

#endif