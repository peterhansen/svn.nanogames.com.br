/*
 *  LoadingThread.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 8/4/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef LOADING_THREAD_H
#define LOADING_THREAD_H 1

#import <UIKit/UIKit.h>

@interface LoadingThread : NSThread
{
	@private
		// Ponteiro para a função de carregamento do usuário
		id loadingObj;
		SEL loadingFunc;
		id loadingFuncParam;
}

// Configura a thread de carregamento
-( void )loadWithObj:( id )obj Method:( SEL )selector AndParam:( id )param;

@end

#endif
