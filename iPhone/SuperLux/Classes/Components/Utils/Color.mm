#include "Color.h"

#define COLOR_EPSILON 0.001f
#define CCMP( c1, c2 ) ( fabs( ( c1 ) - ( c2 ) ) < COLOR_EPSILON ? 0 : ( ( c1 ) > ( c2 ) ? 1 : -1 ) )

// ==
#define CEQL( c1, c2 ) ( CCMP( c1, c2 ) == 0 )

// !=
#define CDIF( c1, c2 ) ( CCMP( c1, c2 ) != 0 )

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( void ) : r( 0.0f ), g( 0.0f ), b( 0.0f ), a( 0.0f )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( float red, float green, float blue, float alpha ) : r( red ), g( green ), b( blue ), a( alpha )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( uint8 red, uint8 green, uint8 blue, uint8 alpha ) : r( red / 255.0f ), g( green / 255.0f ), b( blue / 255.0f ), a( alpha / 255.0f )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( PreDefinedColors color ) : r( 0.0f ), g( 0.0f ), b( 0.0f ), a( 0.0f )
{
	set( color );
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Color::Color( uint32 color ) : r( (( color >> 24 ) & 0xFF ) / 255.0f ), g( (( color >> 16 ) & 0xFF ) / 255.0f ), b( (( color >> 8 ) & 0xFF ) / 255.0f ), a( ( color & 0xFF ) / 255.0f )
{
}

/*==============================================================================================

MÉTODO set
	Inicializa o objeto.

==============================================================================================*/

void Color::set( PreDefinedColors color )
{
	switch( color )
	{
		case COLOR_GREY:
			set( 0.5f, 0.5f, 0.5f, 1.0f );
			break;
			
		case COLOR_WHITE:
			set( 1.0f, 1.0f, 1.0f, 1.0f );
			break;
			
		case COLOR_RED:
			set( 1.0f, 0.0f, 0.0f, 1.0f );
			break;
			
		case COLOR_GREEN:
			set( 0.0f, 1.0f, 0.0f, 1.0f );
			break;
			
		case COLOR_BLUE:
			set( 0.0f, 0.0f, 1.0f, 1.0f );
			break;
			
		case COLOR_YELLOW:
			set( 1.0f, 1.0f, 0.0f, 1.0f );
			break;
			
		case COLOR_MAGENTA:
			set( 1.0f, 0.0f, 1.0f, 1.0f );
			break;
			
		case COLOR_CYAN:
			set( 0.0f, 1.0f, 1.0f, 1.0f );
			break;
			
		case COLOR_PURPLE:
			set( 0.5f, 0.0f, 0.5f, 1.0f );
			break;
			
		case COLOR_BROWN:
			set( 0.6f, 0.4f, 0.2f, 1.0f );
			break;
			
		case COLOR_ORANGE:
			set( 1.0f, 0.5f, 0.0f, 1.0f );
			break;
			
		case COLOR_TRANSPARENT:
			set( 0.0f, 0.0f, 0.0f, 0.0f );
			break;
			
		case COLOR_BLACK:
		default:
			set();
			break;
	}
}

/*==============================================================================================

OPERATOR ==

==============================================================================================*/

bool Color::operator == ( const Color& c ) const
{
	return	CEQL( r, c.r )
			&& CEQL( g, c.g )
			&& CEQL( b, c.b )
			&& CEQL( a, c.a );
}

/*==============================================================================================

OPERATOR !=

==============================================================================================*/

bool Color::operator != ( const Color& c ) const
{
	return	CDIF( r, c.r )
			|| CDIF( g, c.g )
			|| CDIF( b, c.b )
			|| CDIF( a, c.a );
}

