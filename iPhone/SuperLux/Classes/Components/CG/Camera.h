#ifndef CAMERA_H
#define CAMERA_H 1

#include "Box.h"
#include "Matrix4x4.h"
#include "Point3f.h"

#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

// Tipos de câmera
enum CameraType
{
	CAMERA_TYPE_LAND = 0,
	CAMERA_TYPE_AIR
};

class Camera
{
	public:
		// Construtores
		Camera( CameraType cameraType = CAMERA_TYPE_AIR );
	
		// Destrutor
		virtual ~Camera( void ){};

		// Configura os atributos da câmera na API gráfica
		virtual void place( void );
	
		// Equivalente a gluLookAt
		// - http://www.opengl.org/documentation/specs/man_pages/hardcopy/GL/html/glu/lookat.html
		static void lookAt( const Point3f* pEye, const Point3f* pCenter, const Point3f* pUp );
	
		// Determina o tipo de câmera a ser utilizado
		inline void setCameraType( CameraType cameraType ){ type = cameraType; };
	
		// Obtém o tipo de câmera utilizado
		inline CameraType getCameraType( void ) const { return type; };
	
		// Obtém a posição da câmera
		inline const Point3f* getPosition( void ) const { return &pos; };

		// Determina a posição da câmera
		inline void setPosition( float x, float y, float z ) { pos.set( x, y, z ); };
		inline void setPosition( const Point3f* pPosition ) { pos = *pPosition; };
	
		// Aproxima a cena em x vezes
		void zoom( float x );
	
		// Retorna o fator de zoom aplicado à câmera atualmente
		inline float getZoomFactor( void ) const { return zoomFactor; };
	
		// Rotaciona a câmera ao redor do eixo right
		void pitch( float angle );
	
		// Rotaciona a câmera ao redor do eixo up
		void yaw( float angle );
	
		// Rotaciona a câmera ao redor do eixo look
		void roll( float angle );
	
		// Movimenta a câmera ao longo do eixo look
		void walk( float units );
	
		// Movimenta a câmera ao longo do eixo right
		void strafe( float units );
	
		// Movimenta a câmera ao longo do eixo up
		void fly( float units );
	
		// Movimenta a câmera ao longo dos 3 eixos
		inline void move( float strafeUnits, float flyUnits, float walkUnits = 0.0f ){ strafe( strafeUnits ); fly( flyUnits ); walk( walkUnits ); };
	
		// Direciona o eixo look
		void lookAt( const Point3f* pSpot );
	
		// Obtém o eixo up
		inline Point3f* getUp( Point3f* pOut ) const { *pOut = up; return pOut; };
	
		// Obtém o eixo look
		inline Point3f* getLook( Point3f* pOut ) const { *pOut = look; return pOut; };
	
		// Obtém o eixo right
		inline Point3f* getRight( Point3f* pOut ) const { *pOut = right; return pOut; };
	
		// Retorna o volume de visualização da câmera
		virtual Box* getViewVolume( Box* pViewVolume ) = 0;
	
		// Obtém a matriz de visualização da mera
		virtual Matrix4x4* getModelViewMatrix( Matrix4x4* pOut ) const;
	
		// Obtém a matriz de projeção da câmera
		virtual Matrix4x4* getProjectionMatrix( Matrix4x4* pOut ) const = 0;
	
	protected:
		// Tipo da câmera
		CameraType type;
	
		// Zoom aplicado à cena
		float zoomFactor;

		// Atributos da câmera
		Point3f	pos, right /*X*/, up/*Y*/, look /*Z*/;

	private:
		// Garante que os eixos right, up e look são ortogonais entre si
		static void makeAxesOrthogonal( Point3f* pLook, Point3f* pUp, Point3f* pRight );
};

#endif