/*
 *  PNGReader.h
 *  PepsiCaeBieno
 *
 *  Created by Daniel Lopes Alves on 7/30/09.
 *  Copyright 2009 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_PNG_H
#define NANO_PNG_H 1

#include "NanoTypes.h"

class PNG
{
	public:
		// Construtor
		// Descomprime o PNG e aloca um buffer com o tamanho necessário para conter todos
		// os pixels da imagem
		// TODOO PNG( const uint8* pPNGData );
		PNG( const char* pPNGPath );
	
		// Destrutor
		~PNG( void );
	
		// Libera a memória alocada pela imagem
		void clear( void );
	
		// Retorna um ponteiro para os pixels da imagem
		const uint8* getPixels( void ) const;
	
		// Retorna a largura da imagem
		int32 getWidth( void ) const;
	
		// Retorna a altura da imagem
		int32 getHeight( void ) const;
	
	private:
		// Lê as informações do arquivo da imagem PNG utilizando a libpng
		bool readDataWithLibPNG( const char* pPNGPath );

		// Lê as informações do arquivo da imagem PNG
		bool readData( const uint8* pPNGData );
	
		// Dimensões da imagem
		int32 width, height;
	
		// Outras informações do chunk IHDR
		uint8 bitDepth;
		uint8 colorType;
		uint8 compressionMethod;
		uint8 filterMethod;
		uint8 interlaceMethod;
	
		// Número de bits por pixel
		uint8 bpp;
	
		// Pixels da imagem
		uint8* pPixels;
};

// Implementação dos métodos inline

inline const uint8* PNG::getPixels( void ) const
{
	return pPixels;
}

inline int32 PNG::getWidth( void ) const
{
	return width;
}
	
inline int32 PNG::getHeight( void ) const
{
	return height;
}

#endif
