/*
 *  Sphere.h
 *  Components
 *
 *  Created by Daniel Lopes Alves on 11/25/08.
 *  Copyright 2008 Nano Games. All rights reserved.
 *
 */

#ifndef NANO_SPHERE_H
#define NANO_SPHERE_H 1

#include "VertexSet.h"

class Sphere : public VertexSet
{
	public:
		// Construtor
		// Exceções: ConstructorException
		Sphere( int32 lod );

		// Destrutor
		virtual ~Sphere( void );
	
		// Renderiza o objeto
		//virtual bool render( void );
	
		// Retorna o array de vértices que compõe a forma
		virtual const Vertex* getVertexes( void ) const;
	
		// Retorna quantos vértices que compõem a forma
		virtual uint16 getNVertexes( void ) const;
	
		// Mapeia as coordenadas de textura nos vértices do objeto. O parâmetro pTexCoords
		// deve apontar para um array de 8 floats, representando 4 pares (s,t) que definem
		// uma área quadrada na textura ativa. Os pares devem seguir a ordem: bottom-left,
		// top-left, bottom-right e top-right
		virtual void mapTexCoords( const float* pTexCoords );
	
	private:
		// Inicializa o objeto
		// Exceções: ConstructorException
		void build( void );

		// Nível de detalhamento da esfera
		int32 lod;

		// Array de vértices que descrevem a esfera
		int32 nSphereVertices;
		Vertex* pSphereVertices;
};

// Implementação dos métodos inline
inline const Vertex* Sphere::getVertexes( void ) const
{
	return pSphereVertices;
}

inline uint16 Sphere::getNVertexes( void ) const
{
	return nSphereVertices;
}

#endif