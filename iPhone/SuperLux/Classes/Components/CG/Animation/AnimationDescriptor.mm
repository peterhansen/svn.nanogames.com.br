#include "AnimationDescriptor.h"

#include "Macros.h"

#include <vector>

using namespace std;

// Macro para a conversão de milésimos para segundos
#define MS_TO_SECONDS( x ) ( x * 0.001f )

// Constante que descreve o início de uma sequência com um tempo de duração fixo para todos os frames
#define FRAMESET_DESCRIPTOR_SEQUENCE_FIXED 'f'
#define FRAMESET_DESCRIPTOR_SEQUENCE_FIXED_STR @"f"

// Constante que descreve o início de uma sequência com um tempo de duração diferente para cada frame
#define FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE 'v'
#define FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE_STR @"v"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AnimationDescriptor::AnimationDescriptor( void ) : nSequences( 0 ), pSequences( NULL )
{
}

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

AnimationDescriptor::AnimationDescriptor( const AnimationDescriptor& ref ) : nSequences( ref.nSequences ), pSequences( new AnimSequence[ ref.nSequences ] )
{
	for( uint16 i = 0 ; i < nSequences ; ++i )
		pSequences[i] = ref.pSequences[i];
}
	
/*==============================================================================================

DESTRUTOR

==============================================================================================*/

AnimationDescriptor::~AnimationDescriptor( void )
{
	clean();
}

/*==============================================================================================

MÉTODO readFromFile
	Obtém as informações da textura através da leitura de um arquivo descritor.

==============================================================================================*/

bool AnimationDescriptor::readFromFile( const char* pFileName )
{
	// Obtém um interpretador para o arquivo descritor
	NSString* hSpriteDescriptorPath = Utils::GetPathForResource( CHAR_ARRAY_TO_NSSTRING( pFileName ), @"bin" );
	NSString* hFileContents = [NSString stringWithContentsOfFile: hSpriteDescriptorPath encoding:NSUTF8StringEncoding error: NULL];
	if( hFileContents == NULL )
		return false;

	return readFromScanner( [NSScanner scannerWithString: hFileContents] );
}

/*==============================================================================================

MÉTODO readFromScanner
	Obtém as informações da textura através do parser de arquivos.

==============================================================================================*/

bool AnimationDescriptor::readFromScanner( const NSScanner* hScanner )
{
	// Limpa dados anteriores
	clean();

	// Verifica quantas sequências de animação possuímos
	NSMutableArray* hSequences = [NSMutableArray arrayWithArray:[[[hScanner string] substringFromIndex: [hScanner scanLocation]] componentsSeparatedByString: @"\n"]];
	
	// Retira as strings vazias
	for( uint16 i = 0 ; i < [hSequences count] ; )
	{
		NSString* hCurrSequence = ( NSString* )[hSequences objectAtIndex: i];
		
		// Retira os espaços iniciais e finais da string
		NSString* hTrimmedStr = [hCurrSequence stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];		
		
		// Verifica se sobrou alguma coisa válida
		if( ( [hTrimmedStr length] == 0 ) || (( [hTrimmedStr characterAtIndex:0] != FRAMESET_DESCRIPTOR_SEQUENCE_FIXED ) && ( [hTrimmedStr characterAtIndex:0] != FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE )))
		{
			[hSequences removeObjectAtIndex: i];
		}
		else
		{
			[hSequences replaceObjectAtIndex: i withObject: hTrimmedStr];
			++i;
		}
	}
	
	// Armazena o número de sequências de animação
	nSequences = [hSequences count];
	if( nSequences <= 0 )
		return true;

	// Aloca um array para conter as sequências de animação
	pSequences = new AnimSequence[ nSequences ];
	if( !pSequences )
		return false;
	
	// Lê as sequências de animação
	for( uint16 i = 0 ; i < nSequences ; ++i )
	{
		NSString* hCurrSequence = ( NSString* )[hSequences objectAtIndex: i];
			
		switch( [hCurrSequence characterAtIndex: 0] )
		{
			case FRAMESET_DESCRIPTOR_SEQUENCE_FIXED:
				if( !scanFixedSequence( &( pSequences[i] ), hCurrSequence ) )
					return false;
				break;

			case FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE:
				if( !scanVariableSequence( &( pSequences[i] ), hCurrSequence ) )
					return false;
				break;
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO scanFixedSequence
	Lê do arquivo uma sequência de tempo constante.

==============================================================================================*/

bool AnimationDescriptor::scanFixedSequence( AnimSequence* pSequence, const NSString* hSequence )
{
	// vector pode disparar exceções...
	try
	{
		NSScanner* hScanner = [NSScanner scannerWithString: hSequence];
		
		// Pula o descritor do tipo da sequência
		[hScanner scanString: FRAMESET_DESCRIPTOR_SEQUENCE_FIXED_STR intoString: NULL];
		
		// Lê o tempo da animação constante
		int32 time;
		if( ![hScanner scanInt:&time] )
			return false;
		
		// Lê os índices dos frames
		int32 frameIndex;
		vector<uint16> indexes;
		while( [hScanner isAtEnd] == NO )
		{
			if( ![hScanner scanInt: &frameIndex] )
				return false;

			indexes.push_back( static_cast< uint16 >( frameIndex ) );
		}
		
		// Preenche a sequência com os dados lidos
		return pSequence->set( indexes.size(), MS_TO_SECONDS( time ), &indexes[0] );
	}
	catch( ... )
	{
		return false;
	}
}

/*==============================================================================================

MÉTODO scanVariableSequence
	Lê do arquivo uma sequência de tempo variável.

==============================================================================================*/

bool AnimationDescriptor::scanVariableSequence( AnimSequence* pSequence, const NSString* hSequence )
{
	// vector pode disparar exceções...
	try
	{
		NSScanner* hScanner = [NSScanner scannerWithString: hSequence];
		
		// Pula o descritor do tipo da sequência
		[hScanner scanString: FRAMESET_DESCRIPTOR_SEQUENCE_VARIABLE_STR intoString: NULL];

		vector<float> durations;
		vector<uint16> indexes;

		int32 aux;
		bool readingFrameDuration = true;
		while( [hScanner isAtEnd] == NO )
		{
			if( readingFrameDuration )
			{
				// Lê a duração do frame
				if( ![hScanner scanInt: &aux] )
					return false;

				durations.push_back( MS_TO_SECONDS( aux ) );	
			}
			else
			{
				// Lê o índice do frame
				if( ![hScanner scanInt: &aux] )
					return false;

				indexes.push_back( aux );
			}
			readingFrameDuration = !readingFrameDuration;
		}
		
		// Preenche a sequência com os dados lidos
		return pSequence->set( indexes.size(), &durations[0], &indexes[0] );
	}
	catch( ... )
	{
		return false;
	}
}

/*==============================================================================================

OPERADOR =

==============================================================================================*/

AnimationDescriptor& AnimationDescriptor::operator=( const AnimationDescriptor& ref )
{
	// Evita auto atribuição
	if( this == &ref )
		return *this;
	
	// Evita vazamento de memória
	clean();
	
	// Copia o número de sequências de animação
	nSequences = ref.nSequences;

	// Cria o array de sequências de animação
	pSequences = new AnimSequence[ nSequences ];
	
	// Copia as sequências de animação
	for( uint16 i = 0 ; i < nSequences ; ++i )
		pSequences[i] = ref.pSequences[i];

	return *this;
}

/*==============================================================================================

MÉTODO clean
	Libera os recursos alocados pelo objeto.

==============================================================================================*/

void AnimationDescriptor::clean( void )
{
	DESTROY_VEC( pSequences );
}

