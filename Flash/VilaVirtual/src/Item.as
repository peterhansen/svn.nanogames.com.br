package
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.ByteArrayEx;
	
	import flash.display.MovieClip;
	import flash.utils.getDefinitionByName;
	
	/**
	* 
	* @author Daniel L. Alves
	* 
	*/	
	public class Item
	{
		/**
		* Nome do item 
		*/		
		private var _itemName : String;

		/**
		* ID único do item 
		*/		
		private var _itemID : uint;

		private var _description : String;
		
		private var _flashClass : String;
		

		/**
		* Construtor
		* @param gameEntityClip MovieClip que contém as animações do objeto
		* @param animFps Velocidade de frames por segundo das animações do objeto
		* @param anchorX Âncora de posicionamento - coordenada no eixo x
		* @param anchorY Âncora de posicionamento - coordenada no eixo y
		*/		
		public function Item( itemDescription : Object )
		{
			id = itemDescription.id;
			name = itemDescription.name;
			description = itemDescription.description;
			flashClass = itemDescription.flashClass;
		}
		
		/**
		* Retorna o nome do item
		* @return O nome do item 
		*/		
		public function get name() : String
		{
			return _itemName.substr();
		}
		
		/**
		* Determina o nome do item 
		* @param str O nome do item
		*/		
		public function set name( str : String ) : void
		{
			_itemName = str.substr();
		}
		
		/**
		* Retorna o ID único do item
		* @return O ID único do item
		*/		
		public function get id() : uint
		{
			return _itemID;
		}
		
		/**
		* Determina o ID único do item 
		* @param id O ID único do item
		*/		
		public function set id( id : uint ) : void
		{
			_itemID = id;
		}
		
		/**
		 *  
		 */
		public function get description():String
		{
			return _description;
		}

		/**
		 * @private
		 */
		public function set description(value:String):void
		{
			_description = value;
		}

		public function get flashClass():String
		{
			return _flashClass;
		}

		public function set flashClass(value:String):void
		{
			_flashClass = value;
		}
		
		public function get movieClip() : MovieClip
		{
			var c : Class = Class( getDefinitionByName( flashClass ) );
			
			return new c() as MovieClip;
		}

		
	}
}
