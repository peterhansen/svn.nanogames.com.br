package
{
	import NGC.ASWorkarounds.StaticClass;
	
	import flash.geom.Point;

	public final /* STATIC */ class Constants extends StaticClass
	{
		/**
		* Construtor : Vai disparar uma exceção se for chamado, pois classes estáticas não devem ser instanciadas
		*/		
		public function Constants()
		{
			super();
		}

//--------------------------------------------------------------------------------------------------------------------------------------------		
		// "Diretivas de compilação"
		// TODO : Retirar !!!!
		public static const COMPILE_VERSION_TEST_MARIO : uint	= 0;
		public static const COMPILE_VERSION_FINAL : uint		= 1;
		public static const COMPILE_VERSION : uint 				= COMPILE_VERSION_FINAL;

		public static const TO_SCREEN_CALC_RELATIVE_TO_MAP : uint 		= 0; 
		public static const TO_SCREEN_CALC_RELATIVE_TO_SCREEN : uint 	= 1;
		public static const TO_SCREEN_CALC : uint 						= TO_SCREEN_CALC_RELATIVE_TO_MAP;

		public static const RSC_LOCATION_LOCAL : uint 		= 0; 
		public static const RSC_LOCATION_EXTERN : uint 		= 1;
		public static const RSC_LOCATION : uint 			= RSC_LOCATION_EXTERN;

		public static const MAP_VERSION_SMALL : uint 	= 0; 
		public static const MAP_VERSION_HUGE : uint 	= 1;
		public static const MAP_VERSION : uint 			= MAP_VERSION_HUGE;
		
		public static const DRAW_TILES_BORDERS : Boolean 	= false;
		public static const DRAW_TILES_BORDERS_COLOR : int	= 0xFF0000;
		
		public static const DRAW_TILES_BLOCKS : Boolean 			= false;
		public static const DRAW_TILES_BLOCKS_BORDER_COLOR : int	= 0xFF00FF;
		
		public static const DRAW_TILE_MAP_BORDERS : Boolean		= false;
		public static const DRAW_TILE_MAP_BORDERS_COLOR : int	= 0x00FFFF;
		
		public static const DYNAMIC_TILE_LOAD_ON : Boolean  = true;
		
		public static const CACHE_TILES_AS_BITMAPS : Boolean = false;

//---------------------------------------------------------------------------------------------------------------------------------------------		

		/**
		* Versão da aplicação 
		*/		
		public static const APP_VERSION : String = "1.0.0";
		
		public static const STAGE_WIDTH : uint = 640;
		public static const STAGE_HEIGHT : uint = 480;
		public static const STAGE_FPS : uint = 24;
		
		/**
		* Identificação da aplicação no NanoOnline 
		*/		
		public static const NO_APP_SHORT_NAME : String = "VIVU";
		
		/**
		* Ranking que mede em quanto tempo o jogador conseguiu obter a sua pontuação de felicidade atual
		*/		
		public static const NO_RNK_TYPE_TIME_FOR_HAPPINESS : int = 0;
		
		/**
		* Ranking que mede a quantidade de felicidade obtida por um jogador 
		*/		
		public static const NO_RNK_TYPE_HAPPINESS : int = 1;
		
		/**
		* Ranking do minigame da reciclagem 
		*/		
		public static const NO_RNK_TYPE_RECYCLING : int = 2;
		
		/**
		* Ranking do minigame da dengue 
		*/		
		public static const NO_RNK_TYPE_DENGUE : int = 3;
		
		/**
		* Pontuação máxima permitida no minigame da dengue 
		*/		
		public static const NO_RNK_TYPE_DENGUE_MAX_POINTS : int = 999999999;
		
		/**
		* Pontuação máxima permitida no minigame da reciclagem 
		*/
		public static const NO_RNK_TYPE_RECYCLING_MAX_POINTS : int = 999999999;

		/**
		* Largura da área visível inicial do mapa de tiles 
	 	*/		
		public static const VISIBLE_AREA_INIT_WIDTH_IN_BLOCKS : uint = 4; // OLD 3;

		/**
		* Altura da área visível inicial do mapa de tiles
		*/		
		public static const VISIBLE_AREA_INIT_HEIGHT_IN_BLOCKS : uint = VISIBLE_AREA_INIT_WIDTH_IN_BLOCKS;
		
		/**
		* Tamanho dos blocos de tiles nos quais as layers dinâmicas serão divididas. Assim otimizaremos o looping de renderização
		* Esta idéia vem das técnicas QUADTREE e OCTREE 
		*/		
		public static const LAYERS_BLOCKS_SIZE : uint = 8; // OLD 5;

		/**
		* Duração padrão da movimentação da câmera.
		*/
		public static const CAMERA_MOVE_DEFAULT_TIME : Number = 2.0;
		
		/**
		* Duração padrão da animação de esmaecimento da tela.
		*/
		public static const FADE_TIME_DEFAULT : Number = 1.1;
		
		public static const ICON_BLINK_TIME_DEFAULT : Number = 0.4;
		
		/**
		* Taxa de quadros por segundo padrão dos personagens
		*/
		public static const CHARACTER_DEFAULT_FPS : uint = 30;
		
		/**
		* Velocidade padrão do personagem (em metros por segundo)
		*/		
		public static const CHARACTER_DEFAULT_SPEED : Number = 7.0;
		
		/**
		* Duração em segundos das transições de esconder / revelar a tela 
		*/		
		public static const TRANSITION_DUR : Number = 1.2;
		
		/**
		* Duração em segundos da animação de ir/voltar do estado de espera (loading)
		*/		
		public static const WAIT_ANIM_DUR : Number = 1.0;
		
		/**
		* Número máximo de tiles vizinhos que um tile pode possuir
		* TODO: Esta constante está declarada aqui, ao invés de em ITile.as, somente porque o AS 3.0 não permite que declaremos constantes
		* em uma interface, apenas métodos.
		*/
		public static const MAX_NEIGHBORS : uint = 8;
		
		/**
		* Offset dado no cálcula da área visível do mapa de tiles. Assim evitamos pops de tiles com altura 
		*/
		public static const VISIBLE_AREA_SAFETY_MARGIN : int = 4;
		
		/**
		* Altura da caixa de texto utilizada nos diálogos ( em pixels ) 
		*/		
		public static const TEXT_DIALOG_HEIGHT : uint = 120;
		
		/**
		* Velocidade normal do texto (em caracteres por segundo)
		*/
		public static const TEXT_SPEED_SLOW : Number = 30.0;

		/**
		* Velocidade do texto quando o usuário está pressionando uma das teclas de confirmação (em caracteres por segundo)  
		*/
		public static const TEXT_SPEED_FAST : Number = 70.0;
		
		/**
		* Tempo que a câmera demora para transladar do foco atual até o foco de destino (em segundos) 
		*/		
		public static const CAMERA_FOLLOW_TIME : Number = 1.0;

		/**
		* Movimentação da câmera em pixels por segundo quando estamos movimentando-a via input do usuário
		*/		
		public static const CAMERA_MOVEMENT_ON_INPUT : Number = 30.0;

		/**
		* Path dos recursos que não estão embarcados no swf
		*/        
		// OLD public static const RSC_GLOBAL_PATH : String = RSC_LOCATION == RSC_LOCATION_EXTERN ? "http://jad.nanogames.com.br/vvp/" : "../";
		public static function get RSC_GLOBAL_PATH() : String
		{
			if( RSC_LOCATION == RSC_LOCATION_LOCAL )
			{
				return "../";
			}
			else // if( RSC_LOCATION == RSC_LOCATION_EXTERN )
			{
				// OLD: Agora deixamos o arquivo html determinar a path dos recursos
				//return "http://jad.nanogames.com.br/vvp/";
				
				// Quando o nosso SWF é a principal classe da aplicação, a linha abaixo retorna o parâmetro
				// especificado no arquivo html em flashvars. No entanto, quando estamos rodando o SWF via
				// código MXML, loderinfo será null. Para contornar o problema, criamos um setter e um getter
				// "resourcePath" na classe principal. Utilizamos o método setter no código MXML para inicializar
				// a propriedade
				var aux : String = null;
				try
				{
					aux = Application.GetInstance().root.loaderInfo.parameters[ "rsc_path" ];
				}
				catch( e :Error )
				{
				}
				
				if( aux == null )
					aux = Application.GetInstance().resourcePath;                
				
				return aux;
			}
		} 
		
		// ids dos ícones da interface
		public static const ICON_BACKPACK : int			= 0;
		public static const ICON_MAP : int				= 1;
		public static const ICON_HAPPINESS_BAR : int	= 2;
		public static const ICON_CELL_PHONE : int		= 3;
		public static const ICON_QUEST_BOOK : int 		= 4;
		
		// Posicionamento dos ícones da interface
		public static const INTERFACE_ICONS_DIST_FROM_TOP : int				= 10;
		public static const INTERFACE_ICONS_DIST_FROM_RIGHT : int			= 10;
		public static const INTERFACE_ICONS_SPACE_BETWEEN : int				= 25;
		
		public static const INTERFACE_HAPPINESS_BAR_DIST_FROM_TOP : int		= 10;
		public static const INTERFACE_HAPPINESS_BAR_DIST_FROM_LEFT : int	= 10;
		
		// Strings 
		public static const FONT_NAME_TXT_DIALOG : String 		= "Tahoma";
		public static const FONT_NAME_TOONTOWN : String 		= "Tahoma";
		public static const FONT_NAME_CARTOONERIE : String 		= "Tahoma";
		public static const FONT_NAME_GOOD_GIRL : String 		= "Tahoma";
		public static const FONT_NAME_ADDERVILLE : String 		= "Tahoma";
		
		public static const FONT_COLOR_UNSELECTED : uint = 0xb95c00;
		public static const FONT_COLOR_SELECTED : uint = 0x32b119;
		public static const FONT_COLOR_TEXT : uint = FONT_COLOR_UNSELECTED;
		public static const FONT_COLOR_QUEST_COMPLETED : uint = 0x3219b1;
		
		public static const FONT_TEXT_DIALOG_SIZE : uint = 22;
		
		public static const CURSOR_WIDTH : int = 33;
		public static const CURSOR_OFFSET_Y : int = 16;
		
		// Sons e músicas 		
		public static const SOUND_MUSIC_THEME : uint 			= 0;
		public static const SOUND_FX_GOT_ITEM : uint 			= 1;
		public static const SOUND_FX_INTERACTION_BAD : uint 	= 2;
		public static const SOUND_FX_INTERACTION_MENU : uint 	= 3;
		public static const SOUND_FX_SPEECH_BAD_GUY : uint 		= 4;
		public static const SOUND_FX_SPEECH_FEMALE : uint 		= 5;
		public static const SOUND_FX_SPEECH_MALE : uint 		= 6;
		public static const SOUND_FX_SPEECH_KID : uint 			= 7;
		public static const SOUND_MUSIC_QUEST_COMPLETE : uint 	= 8;
		public static const SOUND_FX_CLICK : uint 				= 9;
		public static const SOUND_FX_WRONG_ACTION : uint 		= 10;
		public static const SOUND_FX_ENDING : uint 				= 11;
		
		public static const SOUNDS_TOTAL : uint = SOUND_FX_ENDING + 1;
		
		public static const TILEMAP_DONT_CHANGE : int = -2;
		public static const TILEMAP_NONE : int = -1;
		public static const TILEMAP_ORIGINAL : int = 1;
		public static const TILEMAP_LANCHONETE : int = 3;
		public static const TILEMAP_LOJA_CDS : int = 4;
		public static const TILEMAP_AMBOS : int = 2;
		public static const TILEMAP_NENHUM : int = 5;
		
		public static const MINIGAME_DENGUE : uint = 0;
		public static const MINIGAME_COLETA : uint = 1;
		
		public static const BACKGROUND_MAP : String	= "map";
		
		public static const BACKGROUND_PREFEITURA1 : String		= "prefeitura1"
		public static const BACKGROUND_PREFEITURA1_H : String	= "Prefeitura1HomemBase"
		public static const BACKGROUND_PREFEITURA1_M : String	= "prefeitura1mulherBase"
		public static const BACKGROUND_PREFEITURA2 : String		= "prefeitura2"	
		public static const BACKGROUND_PREFEITURA2_H : String	= "PrefeituraHomemBase"
		public static const BACKGROUND_PREFEITURA2_M : String	= "PrefeituraMulherBase"
			
		public static const BACKGROUND_CASA_VELHA : String	 	= "casaVelha";
		public static const BACKGROUND_CASA_VELHA_H : String	= "CasaidosaHomemBase";
		public static const BACKGROUND_CASA_VELHA_M : String	= "CasaidosamulherBase";
		
		public static const BACKGROUND_VENDA : String			= "venda";
		public static const BACKGROUND_VENDA_H : String			= "VendahomemBase";
		public static const BACKGROUND_VENDA_M : String			= "VendamulherBase";
			
		public static const BACKGROUND_LOJACDS : String			= "lojacds";
		public static const BACKGROUND_LOJACDS_H : String		= "CDSHomemBase";
		public static const BACKGROUND_LOJACDS_M : String		= "CDSMulherBase";
		
		public static const BACKGROUND_LANCHONETE : String		= "lanchonete";
		public static const BACKGROUND_LANCHONETE_H : String	= "LanchoneteBase";
		public static const BACKGROUND_LANCHONETE_M : String	= "lanchonetemulherBase";
		
		public static const BACKGROUND_BECO : String			= "beco";
		public static const BACKGROUND_BECO_H : String			= "BecoHomemBase";
		public static const BACKGROUND_BECO_M : String			= "BecoMulherBase";
		
		public static const BACKGROUND_BECO_VAZIO : String		= "becovazio";
		public static const BACKGROUND_BECO_VAZIO_H : String	= "BecoHomemBase";
		public static const BACKGROUND_BECO_VAZIO_M : String	= "BecoMulherBase";
		//BG Final
		public static const BACKGROUND_FINAL_POLICEMAN : String = "endPolicemanBase";
		public static const BACKGROUND_FINAL_ATTENDANT : String = "endAttendant";
		public static const BACKGROUND_FINAL_LITTLEBOY : String = "endLittleBoyBase";
		public static const BACKGROUND_FINAL_CHINESE : String 	= "endChineseBase";
		public static const BACKGROUND_FINAL_HIPPIE : String 	= "endHippieBase";
		public static const BACKGROUND_FINAL_BADDIES : String 	= "endbaddiesBase";
		public static const BACKGROUND_FINAL_MAYOR : String 	= "endMayorBase";
		
		
		public static const MAP_BECO : int				= 0; 
		public static const MAP_PADARIA : int			= 1; 
		public static const MAP_PASTELARIA : int		= 2; 
		public static const MAP_CABINE_POLICIA : int	= 3; 
		public static const MAP_BOMBEIRO : int			= 4; 
		public static const MAP_POSTO_GASOLINA : int	= 5; 
		public static const MAP_POSTO_SAUDE : int		= 6; 
		public static const MAP_CACHORRO_QUENTE : int	= 7; 
		public static const MAP_ANITA : int				= 8; 
		public static const MAP_CASA_2 : int			= 9; 
		public static const MAP_DONA_ZEFA : int			= 10; 
		public static const MAP_LOJA_LAMAR : int		= 11; 
		public static const MAP_ONG : int				= 12; 
		public static const MAP_DELEGACIA : int			= 13; 
		public static const MAP_PREFEITURA : int		= 14; 
		public static const MAP_URBLIMP : int			= 15;
	}
}
