package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import de.polygonal.ds.HashMap;
	import de.polygonal.ds.Iterator;
	
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	
	public class QuestScreen extends InfoScreen
	{
		private static const SCROLL_ANIM_DEFAULT_DUR : Number = 0.5;
		
		private static const ENTRIES_SPACING : int = 10;
		
		private static const QUESTS_TITLE_CURSOR_X : int = 20;
		private static const QUESTS_TITLE_WINDOW_X : int = QUESTS_TITLE_CURSOR_X + Constants.CURSOR_WIDTH;
		private static const QUESTS_TITLE_WINDOW_WIDTH : int = 165;
		private static const QUESTS_TITLE_WINDOW_Y : int = 51;
		private static const QUESTS_TITLE_WINDOW_HEIGHT : int = 306;
		
		private static const QUEST_DESCRIPTION_WINDOW_X : int = 270;
		private static const QUEST_DESCRIPTION_WINDOW_Y : int = QUESTS_TITLE_WINDOW_Y;
		private static const QUEST_DESCRIPTION_WINDOW_WIDTH : int = 275;
		private static const QUEST_DESCRIPTION_WINDOW_HEIGHT : int = 200;
		
		private static const QUEST_STATE_WINDOW_X : int = QUEST_DESCRIPTION_WINDOW_X;
		private static const QUEST_STATE_WINDOW_Y : int = 270;
		private static const QUEST_STATE_WINDOW_WIDTH : int = QUEST_DESCRIPTION_WINDOW_WIDTH;
		private static const QUEST_STATE_WINDOW_HEIGHT : int = 120;
		
		private static const SCROLL_SPACEX_AFTER_TITLES : int = 25;
		
		private static const FONT_COLOR_QUEST_COMPLETE : uint = 0xbd840b; 
		
		private var scrollIconUp : ScrollBase;
		private var scrollIconDown : ScrollBase;
		
		/** */
		private var scrollTween : Tween;
		
		/** */
		private var textScrollLimit : int = -QUESTS_TITLE_WINDOW_HEIGHT;
		
		/** */
		private var textTotalHeight : int;
		
		private var textRect : Rectangle;
		
		private var questsLabels : Vector.< TextField > = new Vector.<TextField>;
		
		private var cursor : MovieClipVS;
		
		private var questDescription : TextField;
		private var questState : TextField;

		private var questStart : TextField;
		
		private var questEnd : TextField;
		
		private var currentQuestIndex : uint;
		
		private var quests : Array;
		
		private var textFormatter : TextFormat;
		
		private var visibleQuests : uint;
		
		public function QuestScreen( questsMap : HashMap, onClose : Function, onMouseOver : Function, onMouseOut : Function )
		{
			super();
			
			quests = questsMap.toArray();
			
			scrollIconDown = new ScrollBase();
			scrollIconDown.addEventListener( MouseEvent.MOUSE_DOWN, scrollDown );
			scrollIconDown.x = QUESTS_TITLE_WINDOW_X + QUESTS_TITLE_WINDOW_WIDTH + SCROLL_SPACEX_AFTER_TITLES;
			scrollIconDown.y = QUESTS_TITLE_WINDOW_Y + QUESTS_TITLE_WINDOW_HEIGHT;
			scrollIconDown.buttonMode = true;
			scrollIconDown.addEventListener( MouseEvent.MOUSE_UP, stopScroll );
			addChild( scrollIconDown );
			
			scrollIconUp = new ScrollBase();
			scrollIconUp.scaleY *= -1;
			scrollIconUp.x = scrollIconDown.x;
			scrollIconUp.y = QUESTS_TITLE_WINDOW_Y;
			scrollIconUp.buttonMode = true;
			scrollIconUp.addEventListener( MouseEvent.MOUSE_DOWN, scrollUp );
			scrollIconUp.addEventListener( MouseEvent.MOUSE_UP, stopScroll );
			addChild( scrollIconUp );
			
			textRect = new Rectangle( 0, 0, QUESTS_TITLE_WINDOW_WIDTH, QUESTS_TITLE_WINDOW_HEIGHT );
			
			textFormatter = new TextFormat();
			textFormatter.font = Constants.FONT_NAME_CARTOONERIE;
			textFormatter.size = 18;
			textFormatter.color = Constants.FONT_COLOR_UNSELECTED;
			
			for ( var i : int = 0; i < quests.length; ++i )
			{
				var quest : Quest = quests[ i ];
				
				var questTitle : TextField = new TextField();
				questTitle.width = QUESTS_TITLE_WINDOW_WIDTH;
				questTitle.text = quest.name;
				questTitle.embedFonts = true;
				questTitle.selectable = false;
				questTitle.antiAliasType = AntiAliasType.ADVANCED;
				questTitle.gridFitType = GridFitType.PIXEL;
				questTitle.thickness = 0;
				questTitle.wordWrap = true;
				questTitle.multiline = true;
				questTitle.setTextFormat( textFormatter );
				questTitle.height = questTitle.textHeight + ENTRIES_SPACING;
				
				questTitle.x = QUESTS_TITLE_WINDOW_X;
				questTitle.y = QUESTS_TITLE_WINDOW_Y;
				questTitle.addEventListener( MouseEvent.CLICK, onQuestClick );
				
				questsLabels.push( questTitle );	
				addChild( questTitle );
				
				// O valor questTitle.height já inclui ENTRIES_SPACING
				textScrollLimit += questTitle.height;
			}
			// Retira o último ENTRIES_SPACING
			textScrollLimit -= ENTRIES_SPACING;
			
			cursor = new CursorBase();
			cursor.x = QUESTS_TITLE_CURSOR_X;
			cursor.y = QUESTS_TITLE_WINDOW_Y;
			addChild( cursor );
			
			questDescription = new TextField();
			questDescription.embedFonts = true;
			questDescription.selectable = false;
			questDescription.antiAliasType = AntiAliasType.ADVANCED;
			questDescription.gridFitType = GridFitType.PIXEL;
			questDescription.thickness = 0;
			questDescription.wordWrap = true;
			questDescription.multiline = true;
			questDescription.x = QUEST_DESCRIPTION_WINDOW_X;
			questDescription.y = QUEST_DESCRIPTION_WINDOW_Y;
			questDescription.width = QUEST_DESCRIPTION_WINDOW_WIDTH;
			questDescription.height = QUEST_DESCRIPTION_WINDOW_HEIGHT;
			addChild( questDescription );
			
			questState = new TextField();
			questState.embedFonts = true;
			questState.selectable = false;
			questState.antiAliasType = AntiAliasType.ADVANCED;
			questState.gridFitType = GridFitType.PIXEL;
			questState.thickness = 0;
			questState.wordWrap = true;
			questState.multiline = true;
			questState.x = QUEST_STATE_WINDOW_X;
			questState.y = QUEST_STATE_WINDOW_Y;
			questState.width = QUEST_DESCRIPTION_WINDOW_WIDTH;
			questState.height = QUEST_DESCRIPTION_WINDOW_HEIGHT;
			addChild( questState );
			
			TagBase;
			addTagAndButton( "TagBase", onClose, onMouseOver, onMouseOut );
		}
		
		
		
		private function onQuestClick( e : Event ) : void
		{
			var questIndex : int = questsLabels.indexOf( e.target );
			if ( questIndex >= 0 && questIndex < questsLabels.length )
				setCurrentQuest( questIndex );
		}
			
		public function getTotalXP() : Number
		{
			var total : Number = 0;
			for ( var i : int = 0; i < quests.length; ++i )
				total += quests[ i ].xp;
			
			return total;
		}
		
		public function onKeyDown( e : KeyboardEvent ) : void
		{
			// Se já estamos animando uma seleção, resume a animação
			if( scrollTween != null )
			{
				scrollTween.fforward();
				stopScrollTween( null );
			}

			switch( e.keyCode )
			{
				case Keyboard.UP:
					setCurrentQuest( currentQuestIndex - 1, true, true );
					break;
				
				case Keyboard.DOWN:
					setCurrentQuest( currentQuestIndex + 1, true, false );
					break;
			}
		}

		/**
		 * Seleciona uma opção de resposta 
		 * @param answerIndex O índice da opção de resposta a ser selecionada
		 */		
		private function setCurrentQuest( questIndex : int, forceDir : Boolean = false, goingUp : Boolean = false ) : void
		{
			var previousQuestIndex : int = currentQuestIndex;
			
			// O índice questIndex pode vir igual a -1, por isso PRECISAMOS somar questsLabels.length na operação abaixo
			currentQuestIndex = ( questsLabels.length + questIndex ) % questsLabels.length;
			
			// Se quest para qual estamos indo ainda não foi descoberta...
			if( !questsLabels[ currentQuestIndex ].visible )
			{
				var nextVisibleQuest : int = -1;
				
				// Se está subindo na lista de quests
				// ( forceDir && goingUp ) => Forma rápida de consertar "está no 1o, vai para o último"
				if( ( previousQuestIndex > currentQuestIndex ) || ( forceDir && goingUp ) )
				{
					// Volta até achar uma quest visível
					for( var index2 : int = ( quests.length + currentQuestIndex - 1 ) % quests.length; index2 != currentQuestIndex; index2 = ( quests.length + index2 - 1 ) % quests.length )
					{
						if( questsLabels[ index2 ].visible )
						{
							nextVisibleQuest = index2;
							break;
						}
					}	
				}
				// Se está descendo na lista de quests
				else
				{
					// Avança até achar uma quest visível
					for ( var index : int = ( currentQuestIndex + 1 ) % quests.length; index != currentQuestIndex; index = ( index + 1 ) % quests.length )
					{
						if ( questsLabels[ index ].visible )
						{
							nextVisibleQuest = index;
							break;
						}
					}									
				}
				
				// Só temos uma quest visível
				if( nextVisibleQuest < 0 )
					return;
				
				previousQuestIndex = currentQuestIndex;
				currentQuestIndex = nextVisibleQuest;
				
				// OLD
				//setCurrentQuest( nextVisibleQuest );
			}
			
			for( var i : int = 0; i < questsLabels.length; ++i )
				questsLabels[ i ].textColor = quests[ i ].state == QuestState.QUEST_STATE_COMPLETE ? FONT_COLOR_QUEST_COMPLETE : Constants.FONT_COLOR_UNSELECTED;
				
			questsLabels[ currentQuestIndex ].textColor = Constants.FONT_COLOR_SELECTED;
			
			var r : Rectangle = questsLabels[ currentQuestIndex ].scrollRect;
			var relativeAnswerY : int = -r.y;
			if( ( relativeAnswerY + questsLabels[ currentQuestIndex ].textHeight > textRect.height ) || ( relativeAnswerY < 0 ) )
			{
				scrollTo( relativeAnswerY + textRect.y );
			}
			// O texto já está completamente visível
			//else
			//{
			//}

			// Move o cursor para selecionar a opção
			cursor.scrollRect = new Rectangle( /* r.x */ - ( Constants.CURSOR_WIDTH / 2 ), r.y - Constants.CURSOR_OFFSET_Y, Constants.CURSOR_WIDTH, r.height );
			
			questDescription.text = quests[ currentQuestIndex ].about;
			questDescription.setTextFormat( textFormatter );
			
			questState.text = "Situação: " + ( quests[ currentQuestIndex ].state == QuestState.QUEST_STATE_COMPLETE ? "completa!" : "incompleta." );
			questState.setTextFormat( textFormatter );
			questState.textColor = quests[ currentQuestIndex ].state == QuestState.QUEST_STATE_COMPLETE ? Constants.FONT_COLOR_SELECTED : Constants.FONT_COLOR_UNSELECTED;
		}
		
		private function scrollTo( y : int, time : Number = SCROLL_ANIM_DEFAULT_DUR, onDoneCallback : Function = null ) : void
		{
			if( y < 0 )
				y = 0;
			else if( y > textScrollLimit )
				y = textScrollLimit;

			stopScroll();
			scrollTween = TweenManager.tween( this, "textScrollY", Regular.easeInOut, textScrollY, y, time );
			if( onDoneCallback != null )
				scrollTween.addEventListener( TweenEvent.MOTION_FINISH, onDoneCallback );
		}

		private function scrollDown( e : MouseEvent = null ) : void
		{
			// Procura a última quest visível
			var lastVisibleQuest : int = -1;
			var nQuests : int = questsLabels.length;
			for( var i : int = 0 ; i < nQuests ; ++i )
			{
				if( questsLabels[i].visible )
					lastVisibleQuest = i;
			}
				
			// Isso não deveria acontecer, mas enfim...
			if( lastVisibleQuest < 0 )
				return;
			
			// Mira sempre a última quest visível
			scrollTo( textScrollLimit - questsLabels[ lastVisibleQuest ].textHeight, SCROLL_ANIM_DEFAULT_DUR, stopScrollTween );
		}
		
		private function stopScroll( e : MouseEvent = null ) : void
		{
			if( scrollTween != null )
			{
				scrollTween.stop();
				scrollTween.removeEventListener( TweenEvent.MOTION_FINISH, stopScroll );
				scrollTween = null;
			}
		}
		
		private function stopScrollTween( e : TweenEvent ) : void
		{
			stopScroll( null );
		}
		
		private function scrollUp( e : MouseEvent = null ) : void
		{
			// Mira sempre o topo! Vai que vai, moleque ambicioso!
			scrollTo( 0, SCROLL_ANIM_DEFAULT_DUR, stopScrollTween );
		}		
		
		public function get textScrollY() : int
		{
			return textRect.y;
		}		

		public function set textScrollY( y : int ) : void
		{
			if( y < 0 )
			{
				y = 0;
				stopScroll();
			}
			else if( y > textScrollLimit )
			{
				y = textScrollLimit;
				stopScroll();
			}
			textRect.y = y;

			for( var i : int = 0; i < questsLabels.length; ++i )
			{
				var answerRect : Rectangle = new Rectangle( textRect.x, y - textTotalHeight, textRect.width, textRect.height );
				var answer : TextField = questsLabels[ i ];
				if( answer.visible )
				{
					answer.scrollRect = answerRect;
					if( i == currentQuestIndex )
						cursor.scrollRect = new Rectangle( /* OLD answerRect.x */ - ( Constants.CURSOR_WIDTH / 2 ), answerRect.y - Constants.CURSOR_OFFSET_Y, Constants.CURSOR_WIDTH /* OLD answerRect.width*/, answerRect.height );

					y -= answer.textHeight + ENTRIES_SPACING;
				}
			}
		}
		
		
		public override function show() : void
		{
			super.show();
			visibleQuests = 0;
			textScrollLimit = 0;
			
			for( var i : int = 0; i < quests.length; ++i )
			{
				var questTitle : TextField = questsLabels[ i ];
				if ( quests[ i ].state != QuestState.QUEST_STATE_UNDISCOVERED )
				{
					++visibleQuests;
					textScrollLimit += questTitle.textHeight + ENTRIES_SPACING;
					questTitle.visible = true;
				}
				else
				{
					questTitle.visible = false;
				}
			}
			// Retira o último ENTRIES_SPACING, pois só queremos espaços ENTRE títulos
			textScrollLimit -= ENTRIES_SPACING;
			
			if( textScrollLimit < QUESTS_TITLE_WINDOW_HEIGHT )
				textScrollLimit = 0;

			scrollIconDown.visible = textScrollLimit > 0;
			scrollIconUp.visible = scrollIconDown.visible;
			
			cursor.visible = visibleQuests > 0;
			
			textScrollY = 0;
			setCurrentQuest( 0 );
		}
	}
}