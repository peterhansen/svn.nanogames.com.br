package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class GameState extends Enum
	{
		// "Construtor" estático
		{ initEnum( GameState ); }
		
		// Possíveis estados de jogo
		public static const GAME_STATE_UNDEFINED : GameState	= new GameState(); // 0
		public static const GAME_STATE_LOADING : GameState		= new GameState(); // 1
		public static const GAME_STATE_IDLE : GameState			= new GameState(); // 2
		public static const GAME_STATE_MOVING : GameState		= new GameState(); // 3
		public static const GAME_STATE_TALKING : GameState		= new GameState(); // 4
		public static const GAME_STATE_ITEM_SCREEN : GameState	= new GameState(); // 5
		public static const GAME_STATE_QUEST_SCREEN : GameState	= new GameState(); // 6
		public static const GAME_STATE_MAP : GameState			= new GameState(); // 7
		public static const GAME_STATE_MINIGAME : GameState		= new GameState(); // 8
		public static const GAME_STATE_QUIT_POPUP : GameState	= new GameState(); // 9
		public static const GAME_STATE_SAVING : GameState		= new GameState(); // 10
		public static const GAME_STATE_SAVE_N_QUIT : GameState	= new GameState(); // 11
	}
}
