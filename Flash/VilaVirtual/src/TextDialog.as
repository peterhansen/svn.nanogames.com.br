package
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.NanoSounds.SoundManager;
	
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	public final class TextDialog extends MovieClipVS
	{	
		/**
		* Campo de texto onde iremos exibir os diálogos 
		*/		
		private var _txtField : TextField;
		
		/**
		* Formatador do texto exibido 
		*/		
		private var _txtFormatter : TextFormat;

		/**
		* Formatador do texto exibido 
		*/		
		private var _txtFormatterAnswers : TextFormat;
		
		/**
		* Armazena o texto completo que devemos exibir. Afinal, exibimos os textos um caractere por vez 
		*/		
		private var _textToShow: String;
		
		/**
		* A velocidade com que o texto é exibido (em caracteres por segundo)
		*/		
		private var _textSpeed : Number;
		
		/**
		* Contador que controla a animação do texto 
		*/		
		private var _charsDiffCounter : Number;
		
		/**
		* Índiced da resposta atualmente selecionada. Somente para diálogos com que oferecem opções de resposta 
		*/		
		private var _currAnswerIndex : uint;
		
		/**
		* Possíveis respostas do diálogo. Somente para diálogos com que oferecem opções de resposta
		*/		
		private var _answers : Vector.< TextField >;
		
		/**
		 * Callbacks chamadas para cada resposta escolhida.
		 */ 
		private var _answersCallbacks : Vector.< Function >;
		
		/**
		* Cursor utilizado para informar a opção de resposta atualmente. Somente para diálogos com que oferecem opções de resposta
		*/		
		private var _answerCursor : MovieClipVS;
		
		/**
		* Callback que deve ser chamada quando acabamos de exibir o texto de um diálogo 
		*/		
		private var _onTextEndedCallback : Function;
		
		private static const TEXT_OFFSET : int = 14;
		private static const CURSOR_X : int = TEXT_OFFSET;
		private static const TIME_APPEAR : Number = 1.0;
		private static const ANSWER_X: int = CURSOR_X + Constants.CURSOR_WIDTH;
		private static const TIME_SCROLL : Number = 0.3;
		private static const TXT_DIST_FROM_SCROLL_CURSOR : Number = ( TEXT_OFFSET * 1.5 );

		private static var textInternalWidth : uint;
		
		private var textBase : BoxBase;
		
		private var scrollIconUp : ScrollBase;
		private var scrollIconDown : ScrollBase;
		
		/** */
		private var _scrollTween : Tween;
		
		/** */
		private var _textScrollLimit : int;
		
		/** */
		private var _textTotalHeight : int;
		
		/** */
		private var _characterFace : MovieClipVS;
		
		private var _textRect : Rectangle;
		
		private var _voiceIndex : uint = Constants.SOUND_FX_SPEECH_MALE;
		
		private var _timeTextShown : int = 0;
		
		private static const MIN_TEXT_TIME : uint = 600;
		

		/**
		* Construtor 
		* @param w Largura da caixa de texto
		* @param h Altura da caixa de texto
		*/		
		public function TextDialog( w : int, h : int )
		{
			super();
			
			textBase = new BoxBase();
			textBase.width = w;
			textBase.height = h;
			//OLD textBase.y = 8; // ???
			addChild( textBase );
			
			width = w;
			height = h;
			
			mouseEnabled = true;
			mouseChildren = true;
			
			scrollIconDown = new ScrollBase();
			scrollIconDown.buttonMode = true;
			// OLD
//			scrollIconDown.addEventListener( MouseEvent.MOUSE_DOWN, scrollDown );
//			scrollIconDown.addEventListener( MouseEvent.MOUSE_UP, stopScroll );
			scrollIconDown.addEventListener( MouseEvent.MOUSE_OVER, scrollDown );
			scrollIconDown.addEventListener( MouseEvent.MOUSE_OUT, stopScroll );
			addChild( scrollIconDown );

			scrollIconUp = new ScrollBase();
			scrollIconUp.buttonMode = true;
			scrollIconUp.rotation = 180;
			// OLD
//			scrollIconUp.addEventListener( MouseEvent.MOUSE_DOWN, scrollUp );
//			scrollIconUp.addEventListener( MouseEvent.MOUSE_UP, stopScroll );
			scrollIconUp.addEventListener( MouseEvent.MOUSE_OVER, scrollUp );
			scrollIconUp.addEventListener( MouseEvent.MOUSE_OUT, stopScroll );
			addChild( scrollIconUp );
			
			_txtFormatter  = null;
			_txtFormatterAnswers  = null;
			_textToShow = "";
			_textSpeed  = 1.0;
			_charsDiffCounter  = 0;
			_currAnswerIndex  = 0;
			_answers = new Vector.< TextField >();
			_answersCallbacks = new Vector.< Function >();
			_onTextEndedCallback = null;
			
			_answerCursor = new CursorBase();
			_answerCursor.visible = false;
			_answerCursor.x = CURSOR_X;
			addChild( _answerCursor );
			
			_txtField = new TextField();
			_txtField.embedFonts = true;
			_txtField.selectable = false;
			_txtField.antiAliasType = AntiAliasType.ADVANCED;
			_txtField.gridFitType = GridFitType.PIXEL;
			_txtField.thickness = 0;
			_txtField.mouseWheelEnabled = false;
			_txtField.wordWrap = true;
			_txtField.multiline = true;
			_txtField.mouseEnabled = false;

			_txtField.autoSize = TextFieldAutoSize.NONE;
			
			scrollIconDown.x = textBase.width - scrollIconDown.width;
			scrollIconUp.y = TEXT_OFFSET + scrollIconUp.height;
			
			scrollIconUp.x = scrollIconDown.x;
			scrollIconDown.y = textBase.height - scrollIconDown.height;
			
			textInternalWidth = scrollIconUp.x - TXT_DIST_FROM_SCROLL_CURSOR;
			
			_txtField.width = textInternalWidth;

			_textRect = new Rectangle( 0, TEXT_OFFSET, textInternalWidth, h - ( TEXT_OFFSET << 1 ) );
			_txtField.scrollRect = _textRect;
			
			addChild( _txtField );
			
			_txtField.x = TEXT_OFFSET;
			_txtField.y = TEXT_OFFSET;
			
			hide();
		}
		
		private function scrollTo( y : int, time : Number = TIME_SCROLL, onDoneCallback : Function = null ) : void
		{
			stopScroll();
			_scrollTween = TweenManager.tween( this, "textScrollY", Regular.easeInOut, textScrollY, y, time );
			if ( onDoneCallback != null )
				_scrollTween.addEventListener( TweenEvent.MOTION_FINISH, onDoneCallback );	
			_scrollTween.start();
		}
		
		
		private function scrollDown( e : MouseEvent = null ) : void
		{
			scrollTo( textScrollY + ( _textRect.height ), 1.0, stopScrollTween );
		}
		
		private function stopScrollTween( e : TweenEvent ) : void
		{
			stopScroll( null );	
		}
		
		private function stopScroll( e : MouseEvent = null ) : void
		{
			if ( _scrollTween != null ) {
				_scrollTween.stop();
				_scrollTween = null;
			}
		}
		
		private function scrollUp( e : MouseEvent = null ) : void
		{
			scrollTo( textScrollY - ( _textRect.height ), 1.0, stopScrollTween );
		}
		
		public function get textScrollY() : int
		{
			return _textRect.y;
		}		
		
		public function set textScrollY( y : int ) : void {
			if ( y < 0 ) {
				y = 0;
				stopScroll();
			} else if ( y > _textScrollLimit ) {
				y = _textScrollLimit;
				stopScroll();
			}
			_textRect.y = y;
			_txtField.scrollRect = _textRect;

			if ( hasAnswers() ) {
				for ( var i : int = 0; i < _answers.length; ++i ) {
					var answerRect : Rectangle = new Rectangle( _textRect.x, y - _textTotalHeight - ( TEXT_OFFSET * 1.5 ), _textRect.width, _textRect.height + ( TEXT_OFFSET * 1.5 ) );
					var answer : TextField = _answers[ i ];
					answer.scrollRect = answerRect;
					if ( i == _currAnswerIndex ) {
						_answerCursor.scrollRect = new Rectangle( -Constants.CURSOR_WIDTH / 2, answerRect.y - Constants.CURSOR_OFFSET_Y, Constants.CURSOR_WIDTH /*answerRect.width*/, answerRect.height );
					}
					y -= answer.textHeight;
				}
			}		
		}
		
		/**
		* Determina o formatador que devemos utilizar no texto exibido 
		* @param formatter O formatador a ser utilizado 
		*/		
		public function setTextFormatter( formatter : TextFormat ) : void
		{
			_txtFormatter = formatter;
			_txtField.setTextFormat( _txtFormatter );
			_txtField.defaultTextFormat = _txtFormatter;
		}
		
		/**
		* Determina o formatador que devemos utilizar no texto exibido 
		* @param formatter O formatador a ser utilizado 
		*/		
		public function setAnswersFormatter( formatter : TextFormat ) : void
		{
			_txtFormatterAnswers = formatter;
			for each ( var a : TextField in _answers ) {
				a.setTextFormat( _txtFormatterAnswers );
			}
		}
		
		/**
		* Determina se devemos colorir o background da caiza de texto e, em caso positivo, que cor devemos utilizar 
		* @param useBkgColor Indica se devemos preencher o background da caixa de texto
		* @param color A cor com a qual devemos preencher a caixa de texto. Se useBkgColor for false, este parâmetro é ignorado 
		*/		
		public function setBkgColor( useBkgColor : Boolean, color : int ) : void
		{
			_txtField.background = useBkgColor;
			
			if( useBkgColor )
				_txtField.backgroundColor = color;
		}
		
		/**
		* Determina o texto que deve ser apresentado 
		* @param str O texto a ser exibido
		* @param textEndedCallback Callback que deve ser chamada quando o diálogo terminar 
		*/		
		public function setText( str : String, textEndedCallback : Function = null, answers : Array = null, embedFonts : Boolean = true ) : void
		{
			_txtField.embedFonts = embedFonts;
			_txtField.text = str;
			
			// Gambiarra para exbirmos o texto em chinês!!!!!!!!!!!
			if( !embedFonts )
			{
				// Usa uma fonte BEM padrão
				var currFont : String = _txtFormatter.font;
				_txtFormatter.font = "Arial";
				_txtField.setTextFormat( _txtFormatter );
				_txtFormatter.font = currFont;
			}
			else
			{
				_txtField.setTextFormat( _txtFormatter );
			}
			
			_textTotalHeight = _txtField.textHeight;
			_txtField.height = _textTotalHeight + 10;
			_txtField.text = "";
			
			_charsDiffCounter = 0.0;
			_textToShow = str;
			
			_onTextEndedCallback = textEndedCallback;
			
			// Trata possíveis perguntas no texto
			for each ( var a : TextField in _answers ) {
				removeChild( a );
			}
			_answers.splice( 0, _answers.length );
			_answersCallbacks.splice( 0, _answersCallbacks.length );
			
			var answersTotalHeight : int = 0;
			
			if ( answers != null && answers.length > 0 ) {
				for ( var i : int = 0; i < answers.length; i += 2 ) {
					if( answers[ i ] != null ) {
						var answer : TextField = new TextField();
						answer.text = answers[ i ];
						answer.width = _txtField.width - _answerCursor.width - ( TXT_DIST_FROM_SCROLL_CURSOR * 0.5 );
						answer.setTextFormat( _txtFormatterAnswers );
						answer.visible = false;
						answer.embedFonts = true;
						answer.selectable = false;
						answer.antiAliasType = AntiAliasType.ADVANCED;
						answer.gridFitType = GridFitType.PIXEL;
						answer.thickness = 0;
						answer.wordWrap = true;
						answer.multiline = true;
						answer.x = _txtField.x;
						answer.addEventListener( MouseEvent.CLICK, onAnswerClick );
						answer.mouseWheelEnabled = false;
						
						// Só para debug
						//answer.background = true;
						//answer.backgroundColor = 0xFF00FF;
						
						answersTotalHeight += answer.textHeight;
						
						addChild( answer );
						_answers.push( answer );
						
						_answersCallbacks.push( answers[ i + 1 ] );
					}
				}
			}
			_answerCursor.visible = false;
			
			_currAnswerIndex = 0;
			
			if ( _textTotalHeight + answersTotalHeight > _textRect.height )
				_textScrollLimit = _textTotalHeight + answersTotalHeight - _textRect.height + 6;
			else
				_textScrollLimit = 0;
			
			scrollIconDown.visible = _textScrollLimit > 0;
			scrollIconUp.visible = scrollIconDown.visible;
			
			textScrollY = 0;
		}
		
		private function onAnswerClick( e : Event ) : void
		{
			var answerIndex : int = _answers.indexOf( e.target );
			if ( answerIndex >= 0 && answerIndex < _answers.length ) {
				if ( answerIndex == _currAnswerIndex ) {
					// TODO confirma a opção atual
				} else {
					selectAnswer( answerIndex );
				}
			}
		}
		
		private function showAnswers() : void
		{
			if ( hasAnswers() ) {
				for ( var i : int = 0; i < _answers.length; ++i ) {
					var answer : TextField = _answers[ i ];
					answer.visible = true;
				}
				// posiciona as respostas 
				textScrollY = textScrollY;
				_answerCursor.visible = true;
				
				selectAnswer( 0 );
			}		
		}
		
		/**
		* Determina a imagem que representa o personagem que está falando
		* @param talkerImg Imagem para representar o personagem que está falando
		* @param atLeft Indica se a imagem que representa o personagem que está falando deve ficar à esquerda (true) ou à direita (false) 
		*/		
		public function setTalkerImg( talkerImg : MovieClipVS, atLeft : Boolean ) : void
		{
			if ( _characterFace != null ) {
				removeChild( _characterFace );
			}
			_characterFace = talkerImg;
			if ( _characterFace != null ) {
				_characterFace.x = 10;
				// OLD _characterFace.y = -_characterFace.height + TEXT_OFFSET;
				_characterFace.y = -_characterFace.height;
				addChildAt( _characterFace, 0 );
			}
		}
		
		/**
		* Retorna se o texto já foi completamente exibido
		* @param considerMinTime indica se o tempo mínimo de exibição do texto completo deve ser respeitado.
		* @return true se o texto foi completamente exibido, false caso contrário 
		*/		
		public function textShown( considerMinTime : Boolean = true ) : Boolean
		{
			const timeOk : Boolean = ( !considerMinTime || getTimer() - _timeTextShown >= MIN_TEXT_TIME );
			return ( _txtField.length == _textToShow.length ) && timeOk; 
		}
		
		/**
		* Atualiza o objeto
		* @param timeElapsed Tempo transcorrido desde a última atualização
		*/	
		public function update( timeElapsed : Number ) : void
		{
			if( !textShown( false ) )
			{
				// Verifica quantos caracteres devemos exibir
				const temp : Number = ( timeElapsed * _textSpeed ) + _charsDiffCounter;
				const nChars : int = int( temp );
				_charsDiffCounter = temp - nChars;
				
				if( nChars > 0 )
				{
					// Toca o som de fala
					SoundManager.GetInstance().playSoundAtIndex( _voiceIndex );

					// Acrescenta o texto ao label
					_txtField.appendText( _textToShow.substr( _txtField.length, nChars ) );					
					_txtField.setTextFormat( _txtFormatter );
					
					if ( _txtField.textHeight + _textRect.y > _textRect.height ) {
						scrollTo( _textRect.y + _txtField.textHeight );
					}
					
					// Se exibimos o texto por completo, envia o evento correspondente
					if( textShown( false ) ) {
						_timeTextShown = getTimer();
						showAnswers();
					}
				}
			}
		}
		
		/**
		* Seleciona a opção de resposta anterior à opção atualmente selecionada. Se não estamos exibindo
		* um diálogo que oferece opções de resposta, não faz nada  
		*/		
		public function previousAnswer() : void
		{
			selectAnswer( ( _currAnswerIndex + ( _answers.length - 1 ) ) % _answers.length );
		}
		
		/**
		* Seleciona a opção de resposta posterior à opção atualmente selecionada. Se não estamos exibindo
		* um diálogo que oferece opções de resposta, não faz nada  
		*/
		public function nextAnswer() : void
		{
			selectAnswer( ( _currAnswerIndex + 1 ) % _answers.length );
		}
		
		
		public function show() : void
		{
			visible = true;
		}
		
		
		/**
		* Confirma a opção de resposta atualmente selecionada (caso haja respostas disponíveis).
		*/		
		public function hide() : void
		{
			visible = false;
			if( hasAnswers() )
			{
				if( _answersCallbacks[ _currAnswerIndex ] != null )
				{
					_answersCallbacks[ _currAnswerIndex ]();
					
					// Garante que não vamos chamar esta callback novamente
					_answersCallbacks[ _currAnswerIndex ] = null;
				}
				
				GameScript.instance.nextGameEvent();
			}
			
			if( _onTextEndedCallback != null )
			{
				// por essa classe ser um singleton dentro de VilaVirtual, o encadeamento de várias telas de diálogo podia causar
				// inconsistência no valor de _onTextEndedCallback. Atribuindo o valor a uma variável temporária, a callback pode
				// ser chamada sem causar problemas em chamadas futuras.
				var f : Function = _onTextEndedCallback;
				_onTextEndedCallback = null;
				f();
			}			
		}
		
		/**
		* Seleciona uma opção de resposta 
		* @param answerIndex O índice da opção de resposta a ser selecionada
		*/		
		private function selectAnswer( answerIndex : uint ) : void
		{
			_currAnswerIndex = answerIndex;
			
			for ( var i : int = 0; i < _answers.length; ++i ) {
				if ( i == answerIndex ) {
					_answers[ i ].setTextFormat( _txtFormatterAnswers );
				} else {
					_answers[ i ].setTextFormat( _txtFormatter );
				}
				_answers[ i ].x = ANSWER_X;
			}
			
			var relativeAnswerY : int = -_answers[ answerIndex ].scrollRect.y;
			if( ( relativeAnswerY + _answers[ answerIndex ].textHeight > _textRect.height ) || ( relativeAnswerY < 0 ) )
			{	
				scrollTo( relativeAnswerY + _txtField.scrollRect.y );
			}
			// O texto está completamente visível
//			else
//			{
//			}

			// Move o cursor para selecionar a opção
			_answerCursor.scrollRect = new Rectangle( -Constants.CURSOR_WIDTH / 2, _answers[ _currAnswerIndex ].scrollRect.y - Constants.CURSOR_OFFSET_Y, /* OLD _answers[ _currAnswerIndex ].scrollRect.width*/Constants.CURSOR_WIDTH,  _answers[ _currAnswerIndex ].scrollRect.height );
		}
		
		/**
		* Determina a velocidade com que o texto é exibido (em caracteres por segundo)
		* @param speed A velocidade com que o texto é exibido (em caracteres por segundo)
		*/		
		public function setTextSpeed( speed : Number ) : void
		{
			_textSpeed = speed;
		}
		
		/**
		* Retorna se o diálogo que estamos exibindo vai apresentar perguntas
		* @return true se o diálogo que estamos exibindo vai apresentar perguntas, false caso contrário
		*/		
		public function hasAnswers() : Boolean
		{
			return _answers != null && _answers.length > 0;
		}
		
		/**
		* Retorna se estamos esperando o usuário responder a uma pergunta
		* @return true se estamos esperando o usuário responder a uma pergunta, false caso contrário 
		*/		
		public function showingQuestion() : Boolean
		{
			return hasAnswers() && textShown();
		}
		
		
		public function setVoice( voiceIndex : uint ) : void
		{
			this._voiceIndex = voiceIndex;
		}
	}
}
