package
{
	import NGC.ASWorkarounds.Enum;
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.NanoSounds.SoundManager;
	import NGC.ScheduledTask;
	import NGC.Utils;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;

	public final class GameScript
	{
		private var _vars : Object = new Object();
		
		private var _quests : Array;
		
		private var _characters : Array;
		
		private var _items : Array;
		
		private var _gameEvents : Vector.< GameEvent >;
		
		/**
		 * IDs únicos de cada item.
		 */
		private static const ITEM_LUVAS			: int = 1;
		private static const ITEM_TOUCA			: int = 2;
		private static const ITEM_LIVROBRAILE	: int = 3;
		
		/**
		 * IDs únicos de cada portal.  
		 */
		
		public static const PORTAL_WISE_1 : Point				= new Point(  32,  98 );
		public static const PORTAL_WISE_2 : Point				= new Point(  33,  98 );
		public static const PORTAL_WISE_3 : Point				= new Point( 108, 107 );
		public static const PORTAL_WISE_4 : Point				= new Point( 108, 106 );
		public static const PORTAL_WISE_5 : Point				= new Point( 108,  60 );
		public static const PORTAL_WISE_6 : Point				= new Point( 108,  59 );
		
		public static const PORTAL_GUARDA_1 : Point				= new Point( 82, 87 );
		public static const PORTAL_ASSISTENTE : Point			= new Point( 76, 49 );
		public static const PORTAL_BOMBEIRO : Point				= new Point( 99, 75 );
		public static const PORTAL_BOMBEIRO2 : Point			= new Point( 103, 68 );
		public static const PORTAL_BOMBEIRO3 : Point			= new Point( 103, 66 );
		public static const PORTAL_BANCA : Point				= new Point( 57, 44 );
		public static const PORTAL_BANCA2 : Point				= new Point( 57, 43 );
		public static const PORTAL_PARQUE1 : Point				= new Point( 72, 61 );
		public static const PORTAL_FUTEBOL1 : Point				= new Point( 120, 65 );
		public static const PORTAL_FUTEBOL2 : Point				= new Point( 120, 67 );
		public static const PORTAL_FUTEBOL3 : Point				= new Point( 120, 69 );
		public static const PORTAL_FUTEBOL4 : Point				= new Point( 127, 73 );
		public static const PORTAL_VELHA1 : Point				= new Point( 66, 59 );
		public static const PORTAL_VELHA2 : Point				= new Point( 59, 58 );
		public static const PORTAL_VELHA3 : Point				= new Point( 58, 51 );
		public static const PORTAL_VELHA4: Point				= new Point( 46, 48 );
		public static const PORTAL_CHARVELHA1 : Point			= new Point( 60, 58 );
		public static const PORTAL_CHARVELHA2 : Point			= new Point( 57, 51 );
		public static const PORTAL_FILA1 : Point				= new Point( 35, 63 );
		public static const PORTAL_FILA2 : Point				= new Point( 35, 62 );
		public static const PORTAL_FILA3 : Point				= new Point( 35, 61 );
		public static const PORTAL_FILA4 : Point				= new Point( 35, 60 );
		public static const PORTAL_FILA5 : Point				= new Point( 35, 57 );
		public static const PORTAL_POSTO1 : Point				= new Point( 50, 119 );
		public static const PORTAL_POSTO2 : Point				= new Point( 59, 109 );
		public static const PORTAL_POSTO3 : Point				= new Point( 54, 117 );
		public static const PORTAL_POLICIA1 : Point				= new Point( 93, 115 );
		public static const PORTAL_POLICIA2 : Point				= new Point( 102, 117 );
		public static const PORTAL_CHINA1 : Point				= new Point( 81, 95 );
		public static const PORTAL_CHINA2 : Point				= new Point( 83, 94 );
		public static const PORTAL_CHINA3 : Point				= new Point( 81, 92 );
		public static const PORTAL_CHINA_LADO : Point			= new Point( 83, 95 );
		public static const PORTAL_CHINA_LADO_1 : Point			= new Point( 83, 96 );
		public static const PORTAL_ESCOLA1 : Point				= new Point( 73, 97 );
		public static const PORTAL_ESCOLA2 : Point				= new Point( 76, 98 );
		public static const PORTAL_ESCOLA3 : Point				= new Point( 71, 98 );
		public static const PORTAL_ESCOLA4 : Point				= new Point( 79, 105 );
		public static const PORTAL_ESCOLA5 : Point				= new Point( 78, 105 );
		public static const PORTAL_ESCOLA6 : Point				= new Point( 82, 90 );
		public static const PORTAL_ESCOLA7 : Point				= new Point( 74, 97 );
		public static const PORTAL_CEGO1 : Point				= new Point( 34, 51 );
		public static const PORTAL_CEGO2 : Point				= new Point( 34, 57 );
		public static const PORTAL_CHARCEGO : Point				= new Point( 34, 58 );
		public static const PORTAL_ONG1 : Point					= new Point( 96, 29 );
		public static const PORTAL_ONG2 : Point					= new Point( 91, 29 );
		public static const PORTAL_ONG3 : Point					= new Point( 96, 28 );
		public static const PORTAL_BECO1 : Point				= new Point( 35, 20 );
		public static const PORTAL_PARQUE2 : Point				= new Point( 78, 66 );
		public static const PORTAL_PARQUE3 : Point				= new Point( 72, 72 );
		public static const PORTAL_PARQUE4 : Point				= new Point( 72, 73 );
		public static const PORTAL_PARQUE5 : Point				= new Point( 72, 67 );
		public static const PORTAL_PARQUE6 : Point				= new Point( 79, 70 );
		public static const PORTAL_PARQUE7 : Point				= new Point( 78, 63 );
		public static const PORTAL_PSAUDE1 : Point				= new Point( 35, 63 );
		public static const PORTAL_PSAUDE2 : Point				= new Point( 34, 89 );
		public static const PORTAL_PSAUDE3 : Point				= new Point( 34, 97 );
		public static const PORTAL_PADARIA1 : Point				= new Point( 59, 92 );
		public static const PORTAL_PADARIA2 : Point				= new Point( 55, 97 );
		public static const PORTAL_PADARIA3 : Point				= new Point( 59, 94 );
		public static const PORTAL_PADARIA4 : Point				= new Point( 59, 89 );
		public static const PORTAL_URBLIMP1 : Point				= new Point( 130, 47 );
		public static const PORTAL_GARI1 : Point				= new Point( 84, 48 );
		public static const PORTAL_ENTULHOQUEST : Point			= new Point( 84, 49 );
		public static const PORTAL_VENDAENTULHO : Point			= new Point( 83, 49 );
		public static const PORTAL_MUSICA1 : Point				= new Point( 59, 27 );
		public static const PORTAL_MUSICA2 : Point				= new Point( 58, 30 );
		public static const PORTAL_MUSICA3 : Point				= new Point( 68, 29 );
		public static const PORTAL_LANCHONETE1 : Point			= new Point( 74, 114 );
		public static const PORTAL_LANCHONETE2 : Point			= new Point( 82, 114 );
		public static const PORTAL_LANCHONETE3 : Point			= new Point( 73, 105 );
		public static const PORTAL_LANCHONETE4 : Point			= new Point( 74, 105 );
		public static const PORTAL_LANCHONETE5 : Point			= new Point( 75, 114 );
		public static const PORTAL_GARI2 : Point				= new Point( 72, 106 );
		public static const PORTAL_VENDA1 : Point				= new Point( 59, 76 );
		public static const PORTAL_VENDA2 : Point				= new Point( 52, 76 );
		public static const PORTAL_VENDA3 : Point				= new Point( 59, 75 );
		public static const PORTAL_VENDA4 : Point				= new Point( 53, 76 );
		public static const PORTAL_DELEGACIA1 : Point			= new Point( 92, 118 );
		public static const PORTAL_HIPPIE : Point				= new Point( 59, 23 );
		public static const PORTAL_HIPPIE2 : Point				= new Point( 48, 28 );
		public static const PORTAL_HIPPIE3 : Point				= new Point( 71, 30 );
		public static const PORTAL_QUADRA1 : Point				= new Point( 100, 50 );
		public static const PORTAL_QUADRA2 : Point				= new Point( 117, 63 );
		public static const PORTAL_QUADRA3 : Point				= new Point( 123, 61 );
		public static const PORTAL_QUADRA4 : Point				= new Point( 125, 67 );
		public static const PORTAL_RUA1 : Point					= new Point( 88, 54 );
		public static const PORTAL_RUA2 : Point					= new Point( 87, 54 );
		public static const PORTAL_RUA3 : Point					= new Point( 86, 54 );
		public static const PORTAL_RUA4 : Point					= new Point( 87, 53 );
		public static const PORTAL_RUA5 : Point					= new Point( 86, 53 );
		public static const PORTAL_RUA6 : Point					= new Point( 87, 55 );
		public static const PORTAL_RUA7 : Point					= new Point( 105, 98 );
		public static const PORTAL_CACHORRO0 : Point			= new Point( 59, 39 );
		public static const PORTAL_CACHORRO1 : Point			= new Point( 76, 29 );
		public static const PORTAL_CACHORRO2 : Point			= new Point( 106, 28 );
		public static const PORTAL_CACHORRO3 : Point			= new Point( 107, 15 );
		public static const PORTAL_CACHORRO4 : Point			= new Point( 104, 7 );
		public static const PORTAL_CACHORRO5 : Point			= new Point( 83, 89 );
		public static const PORTAL_PREFEITURA1 : Point			= new Point( 77, 51 );
		public static const PORTAL_PREFEITURA2 : Point			= new Point( 78, 51 );
		public static const PORTAL_PREFEITURA3 : Point			= new Point( 84, 36 );
		public static const PORTAL_PREFEITURA4 : Point			= new Point( 77, 50 );
		public static const PORTAL_PARTE1F : Point				= new Point( 77, 49 );
		public static const PORTAL_PARTE1P : Point				= new Point( 76, 50 );
		public static const PORTAL_P2POLITICO : Point			= new Point( 22, 40 );
		public static const PORTAL_P2GANGSTER : Point			= new Point( 21, 40 );
		public static const PORTAL_P2GUARDA : Point				= new Point( 23, 40 );
		public static const PORTAL_P2DELEGADO : Point			= new Point( 22, 42 );
		public static const PORTAL_P2PLAYER : Point				= new Point( 23, 43 );
		public static const PORTAL_P2PREFEITO : Point			= new Point( 25, 42 );
		public static const PORTAL_CADEIRANTE1 : Point			= new Point( 107, 50 );
		public static const PORTAL_CARRO1 : Point				= new Point( 105, 55 );
		public static const PORTAL_CARRO2 : Point				= new Point( 118, 56 );
		public static const PORTAL_CADEIRANTE2 : Point			= new Point( 107, 59 );
		public static const PORTAL_CHARCADEIRANTE : Point		= new Point( 107, 58 );
		public static const PORTAL_CEGOP3 : Point				= new Point( 82, 114 );
		public static const PORTAL_CHARCEGOP3 : Point			= new Point( 82, 115 );

		public static const PORTAL_CARRO3 : Point				= new Point( 78,103 );
		
		public static const PORTAL_CARRO4 : Point				= new Point( 102,53 );
		
		public static const PORTAL_CARRO5 : Point				= new Point( 30,56 );
		public static const PORTAL_CARRO6 : Point				= new Point( 30,100 );
		
		public static const PORTAL_CARRO7 : Point				= new Point( 47,118 );
		public static const PORTAL_CARRO8 : Point				= new Point( 93,108 );
		public static const PORTAL_CARRO9 : Point				= new Point( 80,117 );
		
		public static const PORTAL_CARRO10 : Point				= new Point( 126,53 );
		
		public static const PORTAL_CARRO11 : Point				= new Point( 93,109 );
		public static const PORTAL_CARRO12 : Point				= new Point( 93,111 );
		public static const PORTAL_CARRO13 : Point				= new Point( 26,119 );
		public static const PORTAL_CARRO14 : Point				= new Point( 116,46 );
		public static const PORTAL_CARRO15 : Point				= new Point( 116,48 );
		public static const PORTAL_CARRO16 : Point				= new Point( 116,50 );
		public static const PORTAL_CARRO17 : Point				= new Point( 116,86 );
		public static const PORTAL_CARRO18 : Point				= new Point( 116,88 );
		public static const PORTAL_CARRO19 : Point				= new Point( 116,90 );
		public static const PORTAL_CARRO20 : Point				= new Point( 119,86 );
		public static const PORTAL_CARRO21 : Point				= new Point( 119,88 );
		public static const PORTAL_CARRO22 : Point				= new Point( 119,90 );
		//PARTE 3
		public static const PORTAL_CHARDENGUE1 : Point				= new Point( 99, 76 );
		public static const PORTAL_PERGDENGUE1 : Point				= new Point( 98, 76 );
		public static const PORTAL_CHARDENGUE2 : Point				= new Point( 28, 18 );
		public static const PORTAL_PERGDENGUE2 : Point				= new Point( 28, 20 );
		public static const PORTAL_BOMBEIRODENGUE2 : Point			= new Point( 27, 19 );
		public static const PORTAL_BECOENTRADA : Point				= new Point( 30, 34 );
		
		public static const PORTAL_P3GANG1 : Point				= new Point( 31, 31 );
		public static const PORTAL_P3GANG2 : Point				= new Point( 59, 23 );
		public static const PORTAL_CHARGANG2 : Point			= new Point( 60,25 );
		public static const PORTAL_SAVEGANG2 : Point			= new Point( 59,25 );
		
		public static const PORTAL_P3GANG3 : Point				= new Point( 115, 115 );
		public static const PORTAL_CHARGANG3 : Point			= new Point( 115, 114 );
		public static const PORTAL_SAVEGANG3 : Point			= new Point( 114, 115 );
		
		public static const PORTAL_P3GANG4 : Point				= new Point( 105, 26 );
		public static const PORTAL_P3GANG5 : Point				= new Point( 105, 27 );
		public static const PORTAL_CHARGANG5 : Point			= new Point( 106, 26 );
		public static const PORTAL_SAVEGANG5 : Point			= new Point( 106, 27 );
		
		public static const PORTAL_TRAFICANTE1 : Point			= new Point( 30, 117 );
		public static const PORTAL_TRAFICANTE2 : Point			= new Point( 126, 20 );
		public static const PORTAL_TRAFICANTE3 : Point			= new Point( 103, 7 );
		
		public static const PORTAL_DELEGADO1 : Point			= new Point( 30, 118 );
		public static const PORTAL_DELEGADO2 : Point			= new Point( 127, 20 );
		public static const PORTAL_DELEGADO3 : Point			= new Point( 104, 7 );
		
		public static const PORTAL_P3PLAYER1 : Point				= new Point ( 31, 119 );
		public static const PORTAL_P3PLAYER2 : Point				= new Point ( 128, 21 );
		public static const PORTAL_P3PLAYER3 : Point				= new Point ( 105, 8 );
		
		public static const PORTAL_MUTIRAO1  : Point			= new Point ( 86, 52 );
		public static const PORTAL_MUTIRAO2  : Point			= new Point ( 85, 53 );
		public static const PORTAL_MUTIRAO3  : Point			= new Point ( 88, 55 );
		public static const PORTAL_MUTIRAO4  : Point			= new Point ( 87, 56 );
		public static const PORTAL_MUTIRAO5  : Point			= new Point ( 89, 54 );
		public static const PORTAL_MUTIRAO6  : Point			= new Point ( 88, 53 );
		public static const PORTAL_MUTIRAO7  : Point			= new Point ( 86, 55 );
		public static const PORTAL_MUTIRAO8  : Point			= new Point ( 86, 53 );
		public static const PORTAL_MUTIRAO9  : Point			= new Point ( 87, 52 );
		public static const PORTAL_MUTIRAO10 : Point			= new Point ( 88, 54 );
		public static const PORTAL_MUTIRAO11 : Point			= new Point ( 86, 54 );
		public static const PORTAL_MUTIRAO12 : Point			= new Point ( 87, 55 );
	
		
		private static const MUTIRAO_TOTAL : int = 3; // OLD 5;
		
		/**
		 * IDs únicos de cada personagem.  
		 */
		public static const CHARACTER_JOGADOR		: int = 0;
				// PRINCIPAIS
		private static const CHARACTER_PREFEITO		: int = 1;
		private static const CHARACTER_GUARDA		: int = 2;
		private static const CHARACTER_BOMBEIRO		: int = 3;
		private static const CHARACTER_VELHINHA		: int = 4;
		private static const CHARACTER_VENDEDORA	: int = 5;
		private static const CHARACTER_HIPPIE		: int = 6;
		private static const CHARACTER_FRENTISTA	: int = 7;
		private static const CHARACTER_PERGUNTADOR	: int = 8;
		private static const CHARACTER_CEGO			: int = 9;
		private static const CHARACTER_CADEIRANTE	: int = 10;
		private static const CHARACTER_CHINA		: int = 11;
		private static const CHARACTER_DELEGADO		: int = 12;
		private static const CHARACTER_POLITICO		: int = 13;
		private static const CHARACTER_TRAFICANTE	: int = 41;
		private static const CHARACTER_GARI			: int = 42;
		
		private static const CHARACTER_VICIADO1		: int = 43;
		private static const CHARACTER_VICIADO2		: int = 44;
		private static const CHARACTER_VICIADO3		: int = 45;
		private static const CHARACTER_VICIADO4		: int = 46;
		private static const CHARACTER_VICIADO5		: int = 47;
		
		private static const CHARACTER_CACHORRO		: int = 48;
		private static const CHARACTER_PREFEITO2	: int = 50;
				// ANIMADOS
		private static const CHARACTER_HOMEM1		: int = 14;
		private static const CHARACTER_HOMEM2		: int = 15;
		private static const CHARACTER_HOMEM3		: int = 16;
		private static const CHARACTER_MULHER1		: int = 17;
		private static const CHARACTER_MULHER2		: int = 18;
				//OUTROS
		private static const CHARACTER_HOMEM4		: int = 19;
		private static const CHARACTER_HOMEM5		: int = 20;
		private static const CHARACTER_HOMEM6		: int = 21;
		private static const CHARACTER_HOMEM7		: int = 22;
		private static const CHARACTER_HOMEM8		: int = 23;
		private static const CHARACTER_HOMEM9		: int = 24;
		private static const CHARACTER_HOMEM10		: int = 25;
		private static const CHARACTER_HOMEM11		: int = 26;
		
		private static const CHARACTER_MULHER3		: int = 27;
		private static const CHARACTER_MULHER4		: int = 28;
		private static const CHARACTER_MULHER5		: int = 29;
		private static const CHARACTER_MULHER6		: int = 30;
		private static const CHARACTER_MULHER7		: int = 31;
		private static const CHARACTER_MULHER8		: int = 32;
		private static const CHARACTER_MULHER9		: int = 33;
		private static const CHARACTER_MULHER10		: int = 34;
		
		private static const CHARACTER_CRIANCA1		: int = 35;
		private static const CHARACTER_CRIANCA2		: int = 36;
		private static const CHARACTER_CRIANCA3		: int = 37;
		private static const CHARACTER_CRIANCA4		: int = 38;
		private static const CHARACTER_CRIANCA5		: int = 39;
		private static const CHARACTER_CRIANCA6		: int = 40;
		
		private static const CHARACTER_IRMAO_LARISSA: int = 80;
		
		// Personagems informam sobre CGU, MPU, blablabla
		private static const CHARACTER_WISE_1		: int = 100;
		private static const CHARACTER_WISE_2		: int = 101;
		private static const CHARACTER_WISE_3		: int = 102;
		private static const CHARACTER_WISE_4		: int = 103;
		private static const CHARACTER_WISE_5		: int = 104;
		private static const CHARACTER_WISE_6		: int = 105;
		
			// Interatividade
		private static const CHARACTER_OBJ_ENTULHO1	: int = 51;
		private static const CHARACTER_OBJ_ENTULHO2	: int = 52;
		private static const CHARACTER_OBJ_ENTULHO3	: int = 53;
		private static const CHARACTER_OBJ_ENTULHO4	: int = 54;
		private static const CHARACTER_OBJ_ENTULHO5	: int = 55;
		private static const CHARACTER_OBJ_ENTULHO6	: int = 56;
		
		public static const CHARACTER_OBJ_CAR_1		: int = 57;
		public static const CHARACTER_OBJ_CAR_2		: int = 58;
		public static const CHARACTER_OBJ_CAR_3		: int = 59;
		public static const CHARACTER_OBJ_CAR_4		: int = 60;
		public static const CHARACTER_OBJ_CAR_5		: int = 61;
		public static const CHARACTER_OBJ_CAR_6		: int = 62;
		public static const CHARACTER_OBJ_CAR_7		: int = 63;
		public static const CHARACTER_OBJ_CAR_8		: int = 64;
		public static const CHARACTER_OBJ_CAR_9		: int = 65;
		public static const CHARACTER_OBJ_CAR_10	: int = 66;
		public static const CHARACTER_OBJ_CAR_11	: int = 67;
		public static const CHARACTER_OBJ_CAR_12	: int = 68;
		public static const CHARACTER_OBJ_CAR_13	: int = 69;
		public static const CHARACTER_OBJ_CAR_14	: int = 70;
		public static const CHARACTER_OBJ_CAR_15	: int = 71;
		public static const CHARACTER_PLAYBOYCAR	: int = 72;
		
		// Músicas disponíveis para ouvir na loja do Lamar
		public static const  SOUND_LAMAR_GOTAS : int =  0;
		public static const  SOUND_LAMAR_ENIGMA : int = 1;
		public static const  SOUND_LAMAR_BOUNDS : int = 2;
		
		/**
		 * IDs únicos de cada quest.  
		 */		
			// Parte 1
		private static const QUEST_VELHARUA		: int = 1;
		private static const QUEST_ENTULHO		: int = 2;
		private static const QUEST_SACOLAS		: int = 3;
		private static const QUEST_CACHORRO		: int = 4;
		private static const QUEST_INCENDIO		: int = 5;
			// Parte 2
		private static const QUEST_CADEIRANTE	: int = 6;
		private static const QUEST_HIPPIE		: int = 7;
		private static const QUEST_CEGORUA		: int = 8;
		private static const QUEST_COLETA		: int = 9;
		private static const QUEST_POLITICO		: int = 10;
		private static const QUEST_CHINA		: int = 15;
			//Parte 3
		private static const QUEST_CEGOBRAILE	: int = 11;
		private static const QUEST_DENGUE		: int = 12;
		private static const QUEST_TRAFICANTE	: int = 13;
		private static const QUEST_ONG			: int = 14;

		public static const PARTE_1				: int = 1;
		public static const PARTE_2				: int = 2;
		public static const PARTE_3				: int = 3;
		public static const PARTE_FINAL			: int = 4;
		
		private static var _instance : GameScript; 
		
		private var _genericSpeeches : Array = new Array();
		
		private var _part : int;
		
		public function get part() : int
		{
			return _part;
		}
		
		
		public function GameScript()
		{
			_instance = this;
			
			_gameEvents = new Vector.< GameEvent >();
			
			// Ignore esta linha, mas não retire ela daqui! Assim garantimos que o compilador vai reconhecer o uso destas classes no código
			NPCman1BaseRmk, NPCman2BaseRmk, NPCman3BaseRmk, NPCwoman1BaseRmk, NPCwoman2BaseRmk;
			
			////////////////////////////
			// ATRIBUTOS DOS ITENS
			// workaround pro maldito Flash
			itemglovesBase;
			itemcapBase;
			itembookBase;
			_items = [
				{
					id: ITEM_LUVAS,
					name: 'Luvas',
					flashClass: 'itemglovesBase',
					description: 'Luvas plásticas descartáveis. Preserva os alimentos e previne que germes se espalhem pela comida.'
				},
				
				{ 
					id: ITEM_TOUCA,
					name: 'Touca',
					flashClass: 'itemcapBase',
					description: 'Touca descartável. Evita que caia cabelo na comida.'
				},
				
				{ 
					id: ITEM_LIVROBRAILE,
					name: 'Livro "Aprendendo Braille"',
					flashClass: 'itembookBase',
					description: 'Livro ensinando o alfabeto Braille.'
				},
			];
			
			////////////////////////////
			// ATRIBUTOS DOS PERSONAGENS
			_characters = [
				{
					id: CHARACTER_VELHINHA,
					flashClass: 'OldWomanBase',
					name: 'Anita',
					face: 'GrandmaFaceBase',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				// PRINCIPAIS
				{ 
					id: CHARACTER_PREFEITO,
					flashClass: 'MayorTuristBase',
					name: 'Heitor Lopes',
					face: 'MayortouristFaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_PREFEITO2,
					flashClass: 'MayorBase',
					name: 'Heitor Lopes',
					face: 'MayorfaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				
				{ 
					id: CHARACTER_GUARDA,
					flashClass: 'GuardBase',
					name: 'Amaral',
					face: 'PolicemanfaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				
				{ 
					id: CHARACTER_BOMBEIRO,
					flashClass: 'Fireman',
					name: 'Gustavo',
					face: 'FiremanFaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				
				{ 
					id: CHARACTER_VENDEDORA,
					flashClass: 'StorekeeperBase',
					name: 'Dona Zefa',
					face: 'StorekeeperfaceBase',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				
				{ 
					id: CHARACTER_HIPPIE,
					flashClass: 'HippieBase',
					name: 'Lamar',
					face: 'Hippiefacebase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				
				{ 
					id: CHARACTER_FRENTISTA,
					flashClass: 'AttendantBase',
					name: 'Juca',
					face: 'AttendantfaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},

				{ 
					id: CHARACTER_PERGUNTADOR,
					flashClass: 'LittleBoyBase',
					name: 'Enzo',
					face: 'LittleBoyFaceBase',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				
				{ 
					id: CHARACTER_CEGO,
					flashClass: 'Blind',
					name: 'Alcebiades',
					face: 'BlindFacebase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				
				{ 
					id: CHARACTER_CADEIRANTE,
					flashClass: 'WheelchairBase',
					name: 'Larissa', 
					face: 'WheelfaceBase',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{
					id: CHARACTER_IRMAO_LARISSA,
					flashClass:'Child3Base',
					name: 'Pedro',
					voice: Constants.SOUND_FX_SPEECH_KID
					
				},
				{ 
					id: CHARACTER_CHINA,
					flashClass: 'ChineseBase',
					name: 'Wong' ,
					face: 'ChinesefaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},

				{ 
					id: CHARACTER_DELEGADO,
					flashClass: 'DelegateBase',
					name: 'Delegado Magalhães', 
					face: 'DelegateFaceBase',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				
				{ 
					id: CHARACTER_POLITICO,
					flashClass: 'politicianBase',
					name: 'Patrício',
					face: 'PoliticianFaceBase',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},

				{ 
					id: CHARACTER_TRAFICANTE,
					flashClass: 'GangsterBase',
					name: 'Xandinho' ,
					face: 'GangsterFaceBase',
					voice: Constants.SOUND_FX_SPEECH_BAD_GUY
				},

				{ 
					id: CHARACTER_GARI,
					flashClass: 'StreetSweeper',
					name: 'Kleber', 
					face: 'StreetSweeperfaceBase',

					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_CACHORRO,
					flashClass: 'Dog01Base',
					voice: Constants.SOUND_FX_SPEECH_BAD_GUY
				},
				
				{ 
					id: CHARACTER_PLAYBOYCAR,
					flashClass: 'NPCCarBase',
					type: 'car',
					voice: Constants.SOUND_FX_SPEECH_BAD_GUY
				},
				
					//RANDOM
				{ 
					id: CHARACTER_HOMEM1,
					flashClass: 'NPCman1BaseRmk',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM2,
					flashClass: 'NPCman2BaseRmk',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM3,
					flashClass: 'NPCman3BaseRmk',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_MULHER1,
					flashClass: 'NPCwoman1BaseRmk',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER2,
					flashClass: 'NPCwoman2BaseRmk',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_HOMEM4,
					flashClass: 'Man01Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM5,
					flashClass: 'Man02Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM6,
					flashClass: 'Man03Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM7,
					flashClass: 'Man04Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM8,
					flashClass: 'Man05Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},

				{ 
					id: CHARACTER_HOMEM9,
					flashClass: 'Man06Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM10,
					flashClass: 'Man07Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_HOMEM11,
					flashClass: 'Man08Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_MULHER3,
					flashClass: 'Woman01Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER4,
					flashClass: 'Woman02Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER5,
					flashClass: 'Woman03Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER6,
					flashClass: 'Woman04Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER7,
					flashClass: 'Woman05Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER8,
					flashClass: 'Woman06Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER9,
					flashClass: 'Woman07Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_MULHER10,
					flashClass: 'Woman08Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{ 
					id: CHARACTER_VICIADO1,
					flashClass: 'Gang01Base',
					name: 'Dan' ,
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_VICIADO2,
					flashClass: 'Gang02Base',
					name: 'Beto' ,
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_VICIADO3,
					flashClass: 'Gang03',
					name: 'Junior' ,
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_VICIADO4,
					flashClass: 'Gang04',
					name: 'Piteco' ,
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_VICIADO5,
					flashClass: 'Gang05Base',
					name: 'Silas' ,
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{ 
					id: CHARACTER_CRIANCA1,
					flashClass: 'Child1Base',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				{ 
					id: CHARACTER_CRIANCA2,
					flashClass: 'Child2Base',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				{ 
					id: CHARACTER_CRIANCA3,
					flashClass: 'Child3Base',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				{ 
					id: CHARACTER_CRIANCA4,
					flashClass: 'Child4Base',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				{ 
					id: CHARACTER_CRIANCA5,
					flashClass: 'Child5Base',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				{ 
					id: CHARACTER_CRIANCA6,
					flashClass: 'Child6Base',
					voice: Constants.SOUND_FX_SPEECH_KID
				},
				// Personagens com as falas didáticas (os chatos...)
				{
					id: CHARACTER_WISE_1,
					flashClass: 'Woman01Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{
					id: CHARACTER_WISE_2,
					flashClass: 'Man01Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{
					id: CHARACTER_WISE_3,
					flashClass: 'Woman03Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{
					id: CHARACTER_WISE_4,
					flashClass: 'Man03Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				{
					id: CHARACTER_WISE_5,
					flashClass: 'Woman05Base',
					voice: Constants.SOUND_FX_SPEECH_FEMALE
				},
				{
					id: CHARACTER_WISE_6,
					flashClass: 'Man05Base',
					voice: Constants.SOUND_FX_SPEECH_MALE
				},
				//OBJETOS INTERATIVOS
				{ 
					id: CHARACTER_OBJ_ENTULHO1,
					flashClass: 'GarbageBase'
				},
				
				{ 
					id: CHARACTER_OBJ_ENTULHO2,
					flashClass: 'GarbageBase'
				},
				
				{ 
					id: CHARACTER_OBJ_ENTULHO3,
					flashClass: 'GarbageBase'
				},
				
				{ 
					id: CHARACTER_OBJ_ENTULHO4,
					flashClass: 'GarbageBase'
				},
				
				{ 
					id: CHARACTER_OBJ_ENTULHO5,
					flashClass: 'GarbageBase'
				},
				
				{ 
					id: CHARACTER_OBJ_ENTULHO6,
					flashClass: 'GarbageBase'
				},
				
				{ 
					id: CHARACTER_OBJ_CAR_1,
					type: 'car',
					flashClass: 'Car1Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_2,
					type: 'car',
					flashClass: 'Car2Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_3,
					type: 'car',
					flashClass: 'Car3Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_4,
					type: 'car',
					flashClass: 'Car4Base'
				},
				
				{ 
					id: CHARACTER_OBJ_CAR_5,
					type: 'car',
					flashClass: 'TaxiBase'
				},
				{ 
					id: CHARACTER_OBJ_CAR_6,
					type: 'car',
					flashClass: 'Car1Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_7,
					type: 'car',
					flashClass: 'Car2Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_8,
					type: 'car',
					flashClass: 'Car3Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_9,
					type: 'car',
					flashClass: 'Car4Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_10,
					type: 'car',
					flashClass: 'TaxiBase'
				},
				{ 
					id: CHARACTER_OBJ_CAR_11,
					type: 'car',
					flashClass: 'Car1Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_12,
					type: 'car',
					flashClass: 'Car2Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_13,
					type: 'car',
					flashClass: 'Car3Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_14,
					type: 'car',
					flashClass: 'Car4Base'
				},
				{ 
					id: CHARACTER_OBJ_CAR_15,
					type: 'car',
					flashClass: 'TaxiBase'
				},
			];
			
			////////////////////////////
			// ATRIBUTOS DAS QUESTS
			_quests = [
				//Parte 1
				{
					id: QUEST_VELHARUA,
					title: 'Do Lado de Cá',
					description: 'Anita precisa de sua ajuda para atravessar a rua. Seja um bom cidadão e ajude-a.',
					xp: 250
				},
				
				{
					id: QUEST_ENTULHO,
					title: 'Mutirão para Recolher o Lixo',
					description: 'A URBLIMP não está conseguindo retirar o lixo na mesma velocidade com que ele é gerado. Mobilize os moradores para formar um mutirão de coleta.',
					xp: 1000
				},
				
				{
					id: QUEST_SACOLAS,
					title: 'Sacolas Retornáveis',
					description: 'Convença Dona Zefa a trocar as sacolas de sua venda por sacolas retornáveis para ajudar o meio ambiente.',
					xp: 250
				},
				
				{
					id: QUEST_CACHORRO,
					title: '4 Patas Correm Mais Que 2',
					description: 'Se o guarda Amaral estivesse na sua juventude, ele conseguiria pegar aquele cachorro, mas agora ele precisa de ajuda! Ajude o guarda a pegar o cachorro fujão.',
					xp: 500
				},
				
				{
					id: QUEST_DENGUE,
					title: 'Surto de Dengue!',
					description: 'A população da Vila Virtual está adoecendo por causa da dengue. Ajude a acabar com os focos do mosquito!',
					xp: 500
				},
				
				// Parte 2
				{
					id: QUEST_CADEIRANTE,
					title: 'Ajude a Larissa!',
					description: 'Larissa não consegue atravessar a rua porque um carro está parado na frente do acesso para cadeirantes. Ajude-a falando com o motorista. Insista se necessário.',
					xp: 250
				},
				
				{
					id: QUEST_HIPPIE,
					title: 'Comércio Irregular!',
					description: 'Caso Lamar não consiga regularizar seu comércio, ele será fechado! Informe-o e ajude-o a regularizar!\nO prefeito pode dar uma ajudinha!',
					xp: 500
				},
				
				{
					id: QUEST_CHINA,
					title: 'Dor de Barriga!',
					description: 'Parece que a higiene do Sr. Wong não é muito boa na hora de fazer os pastéis que todo mundo gosta. Ajude o Sr. Wong a conseguir luvas e touca plásticas para tornar os pastéis saudáveis e assim acabar com a dor de barriga do prefeito!',
					xp: 500
				},
				
				{
					id: QUEST_CEGORUA,
					title: 'Ajude Alcebiades!',
					description: 'Alcebiades é cego, e fica inseguro de atravessar a rua sozinho. Dê o apoio que ele precisa.',
					xp: 250
				},
				
				{
					id: QUEST_COLETA,
					title: 'Selecione Seu Lixo',
					description: 'A coleta seletiva está disponível para os moradores da Vila Virtual, mas eles não sabem sobre ela. Explique para alguns moradores como ela funciona para divulgar o serviço e ajudar o meio-ambiente.',
					xp: 1000
				},
				
				{
					id: QUEST_POLITICO,
					title: 'Corrupção! Crianças Sem Merenda',
					description: 'Patrício, o assistente do prefeito, foi flagrado conversando com a pessoa suspeita de desviar as merendas das escolas! E ele te afugentou, o que torna tudo mais suspeito ainda! Alerte as autoridades!',
					xp: 1000
				},
				
				//Parte 3
				{
					id: QUEST_CEGOBRAILE,
					title: 'Ler Sem Ver',
					description: 'O Senhor Alcebíades quer conseguir pedir comida na lanchonete sem dificuldades. Ajude Wong a implementar o cardápio em Braille. Talvez na biblioteca tenha um livro sobre isso',
					xp: 500
				},
				
				{
					id: QUEST_TRAFICANTE,
					title: 'Fugitivo!',
					description: 'O homem que ajudava o Patrício a desviar a merenda escolar ainda está desaparecido. Ele não deve estar longe! Ajude a encontrá-lo!',
					xp: 1000
				},
				
				{
					id: QUEST_ONG,
					title: 'Evasão Escolar',
					description: 'Você descobriu que alguns garotos da Vila Virtual estão deixando de ir às aulas para ficar de bobeira no beco. Será que você não consegue arrumar um motivo extra para eles voltarem à escola?',
					xp: 1000
				},
			];				
			
			////////////////////////////
			// VARIÁVEIS GLOBAIS DO JOGO
			vars.falouVovo = false;
			vars.falouPolitico = false;
			vars.falouHippie = false;
			vars.falouVenda = false;
			vars.mutirao = 0;
			vars.mutirao1 = false;
			vars.mutirao2 = false;
			vars.mutirao3 = false;
			
			// Deixou de existir
			vars.mutirao4 = true;
			
			vars.mutirao5 = false;
			vars.mutirao6 = false;	
			vars.part1quests = 0;
			vars.part2quests = 0;
			vars.cachorro = 0;
			vars.traficantequestatividade = false;
			vars.prendeutraficante = false;  
			vars.contadorong = 0;
			vars.danquest = 0;
			vars.betoquest = 0;
			vars.juniorquest = 0;
			vars.pitecosilasquest = 0;
			vars.traficanteTile = 1;
			vars.denguePlay = false;
			vars.playboy_talks = 0;
			vars.china_braille = false;
		}
		
		private function oldLadyHintsPart2() : void
		{
			var aux : int = 0;
			var incompleteQuests : Array = new Array();

			if( getQuestStatus(QUEST_CADEIRANTE) == QuestState.QUEST_STATE_INCOMPLETE )
			{
				++aux;
				incompleteQuests.push( "A Larissa não consegue atravessar a rua...", function() : void
																					 {
																							// Workaround - Estava pulando o 1o evento
																							wait( 0.5 );
																							
																							showMessage( "Você já tentou falar com o menino do carro? Vai ver ele não sabe que o acesso é para cadeirantes.\nSe não deu certo, insista.", CHARACTER_VELHINHA );
																							queueGameEvent( instance, oldLadyHintsPart2, true );
																					 } );
			}
			
			if( getQuestStatus(QUEST_HIPPIE) == QuestState.QUEST_STATE_INCOMPLETE )
			{
				++aux;
				incompleteQuests.push( "O Lamar ainda não regularizou a venda dos CDs e vai acabar perdendo a mercadoria",  function() : void
																															{
																																// Workaround - Estava pulando o 1o evento
																																wait( 0.5 );
																				
																																showMessage("Oh, que pena! O Lamar é um menino de bom coração... O prefeito devia olhar o pedido dele.", CHARACTER_VELHINHA);
																																if (getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_INCOMPLETE){
																																	showMessage("O prefeito está passando mal? Ora, então podemos fazer alguma coisa sobre os pastéis!", CHARACTER_VELHINHA );
																																} else {
																																	showMessage("Ele já está melhor do desarranjo? Então vamos esperar.", CHARACTER_VELHINHA);
																																}
																																queueGameEvent( instance, oldLadyHintsPart2, true );
																															} );
			}			
			
			if( getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_INCOMPLETE )
			{
				++aux;
				incompleteQuests.push( "Tem gente passando mal com os pastéis do Sr. Wong", function() : void
																							{
																								// Workaround - Estava pulando o 1o evento
																								wait( 0.5 );
																								
																								showMessage("O Sr. Wong não faz por mal. Ele só não sabe sobre as normas de higiene.\nA Josefina da venda é muito limpa. Talvez ela possa te ajudar.", CHARACTER_VELHINHA);
																								queueGameEvent( instance, oldLadyHintsPart2, true );
																							} );
			}
			
			// Não faz muito sentido fornecer dicas sobre missões que são resolvidas com uma simples resposta "Sim"
//			if( getQuestStatus(QUEST_CEGORUA) == QuestState.QUEST_STATE_INCOMPLETE )
//			{
//				++aux;
//				incompleteQuests.push( "Ceguin", 	function() : void
//													{
//														// Workaround - Estava pulando o 1o evento
//														wait( 0.5 );
//														
//														showMessage("Ora, o Sr.Alcebiades realmente é cego! Ele não está mentindo!\nVocê deveria ajudá-lo!", CHARACTER_VELHINHA);
//														queueGameEvent( instance, oldLadyHintsPart2, true );
//													} );
//			}
//			
//			if( getQuestStatus(QUEST_COLETA) == QuestState.QUEST_STATE_INCOMPLETE )
//			{
//				++aux;
//				incompleteQuests.push( "Coleta", 	function() : void
//													{
//														// Workaround - Estava pulando o 1o evento
//														wait( 0.5 );
//														
//														showMessage("Coleta seletiva? O que é isso?\nSe o Kleber se ofereceu para te explicar, aceite! E depois venha me contar do que se trata!", CHARACTER_VELHINHA);
//														queueGameEvent( instance, oldLadyHintsPart2, true );
//													} );
//			}
			
			if( getQuestStatus(QUEST_POLITICO) == QuestState.QUEST_STATE_INCOMPLETE )
			{
				++aux;
				incompleteQuests.push( "O Patrício não deixa eu falar com o prefeito... E anda falando com pessoas estranhas...", 	function() : void
																																			{
																																				// Workaround - Estava pulando o 1o evento
																																				wait( 0.5 );
																																				
																																				showMessage("O Patrício não é daqui. Nunca gostei muito dele.\nVocê deveria alertar o Delegado Magalhães. Isso pode ser sujeira!", CHARACTER_VELHINHA);
																																				setCharacterExpression( CHARACTER_DELEGADO, Character.EXPRESSION_EXCLAMATION );
																																				queueGameEvent( instance, oldLadyHintsPart2, true );
																																			} );
			}
			
			if( aux > 0 )
			{
				// Início do diálogo
				incompleteQuests.unshift( "Sobre o quê deseja falar?", CHARACTER_VELHINHA );
					
				// Opção de saída do diálogo
				incompleteQuests.push( "Deixa para lá", function() : void
														{
															// Workaround - Estava pulando o 1o evento
															wait( 0.5 );
															
															showMessage("Acho muito estranho o Sr. Lopes ser tão negligente. Ele costumava ser muito bondoso e empolgado quando era mais novo.", CHARACTER_VELHINHA);
															showMessage( "Espero ter sido de alguma ajuda!", CHARACTER_VELHINHA );
															setBackground(Constants.BACKGROUND_MAP);
															lockInput( false );
														} );
				
				lockInput( true );
				setBackground(Constants.BACKGROUND_CASA_VELHA);
				
				// Equivale a:
				// showMessage( "Sobre qual assunto você deseja falar?", CHARACTER_VELHINHA, incompleteQuests );
				showMessage.apply( instance, incompleteQuests );
			}
			else
			{
				showMessage( "O quê? Você já resolveu todas as tarefas que descobriu até o momento e passou aqui apenas para me contar? Que ótimo! É sempre bom lembrar dos amigos." );
			}
		}
		
		/**
		* WOW!!!!!!!!! 
		*/		
		private function dogWow( row : int, column : int, startFrom : Direction ) : void
		{
			setCharacterExpression( CHARACTER_CACHORRO, Character.EXPRESSION_NONE );
			var tiles : Vector.< Point > = new Vector.< Point >();
			switch( startFrom )
			{
				case Direction.DIRECTION_UP:
					tiles.push( new Point( row, column - 1 ), new Point( row + 1, column ), new Point( row, column + 1 ), new Point( row - 1, column ) );
					break;
				
				case Direction.DIRECTION_DOWN:
					tiles.push( new Point( row, column + 1 ), new Point( row - 1, column ), new Point( row, column - 1 ), new Point( row + 1, column ) );
					break;
				
				case Direction.DIRECTION_LEFT:
					tiles.push( new Point( row + 1, column ), new Point( row, column + 1 ), new Point( row - 1, column ), new Point( row, column - 1 ) );
					break;
				
				case Direction.DIRECTION_RIGHT:
					tiles.push( new Point( row - 1, column ), new Point( row, column - 1 ), new Point( row + 1, column ), new Point( row, column + 1 ) );
					break;
			}

			var dogNCirclesAroundPlayer : int = 5;
			
			var circleDur : Vector.< Number > = new Vector.< Number >();
			circleDur.push( 0.5, 0.25, 0.1, 0.05, 0.03 );
			
			var currCircleDur : Number;
			for( var n : int = 0 ; n < dogNCirclesAroundPlayer ; ++n )
			{
				currCircleDur = circleDur[n];
				
				setCharacterTile( CHARACTER_CACHORRO, tiles[0].x, tiles[0].y );
				setCharacterDirection( CHARACTER_CACHORRO, Enum.FromIndex( Direction, ( startFrom.Index + 3 ) & 3 ) as Direction );
				setCharacterDirection( CHARACTER_JOGADOR, Enum.FromIndex( Direction, ( startFrom.Index + 1 ) & 3 ) as Direction );
				wait( currCircleDur );
				
				setCharacterTile( CHARACTER_CACHORRO, tiles[1].x, tiles[1].y );
				setCharacterDirection( CHARACTER_CACHORRO, Enum.FromIndex( Direction, ( startFrom.Index + 4 ) & 3 ) as Direction );
				setCharacterDirection( CHARACTER_JOGADOR, Enum.FromIndex( Direction, ( startFrom.Index + 2 ) & 3 )  as Direction );
				wait( currCircleDur );
				
				setCharacterTile( CHARACTER_CACHORRO, tiles[2].x, tiles[2].y );
				setCharacterDirection( CHARACTER_CACHORRO, Enum.FromIndex( Direction, ( startFrom.Index + 5 ) & 3 ) as Direction );
				setCharacterDirection( CHARACTER_JOGADOR, Enum.FromIndex( Direction, ( startFrom.Index + 3 ) & 3 )  as Direction );
				wait( currCircleDur );
				
				setCharacterTile( CHARACTER_CACHORRO, tiles[3].x, tiles[3].y );
				setCharacterDirection( CHARACTER_CACHORRO, Enum.FromIndex( Direction, ( startFrom.Index + 6 ) & 3 ) as Direction );
				setCharacterDirection( CHARACTER_JOGADOR, Enum.FromIndex( Direction, ( startFrom.Index + 4 ) & 3 )  as Direction );
				wait( currCircleDur );
			}
			setCharacterExpression( CHARACTER_CACHORRO, Character.EXPRESSION_EXCLAMATION );
		}
		
		public function onTalk( characterId : int ) : void
		{
			// indica se, no caso de não haver mudança nos eventos com essa interação, o personagem falará uma das frases genéricas.
			var sayGenericSpeech : Boolean = true;
			var previousQueuedEvents : uint = _gameEvents.length;
			
			switch ( _part ) {
				case PARTE_1:
					switch ( characterId ) {
						////////////////////////////////////////////////////////
						// XIMENES BEGIN                                      //
						////////////////////////////////////////////////////////
						case CHARACTER_FRENTISTA:
							showMessage("Onde será que andam meus irmãos?", CHARACTER_FRENTISTA);
							break;
						
						case CHARACTER_CHINA:
							showMessage("Quer pastel?", CHARACTER_CHINA );
							break;
							
						case CHARACTER_MULHER1:
							showMessage("Sempre ajudo a Sra. Anita a atravessar a rua, mas hoje tenho que cuidar da minha irmã.\nSerá que ela está bem?", CHARACTER_MULHER1 );
							break;
						
						case CHARACTER_VELHINHA:
							if ( getQuestStatus(QUEST_VELHARUA) != QuestState.QUEST_STATE_COMPLETE) {
								if ( vars.falouVovo == false ) {
									showMessage("Bom dia. Será que você poderia me ajudar?\nAs minhas pernas e olhos não são mais os mesmos, e eu fico insegura de atravessar a rua sozinha.", CHARACTER_VELHINHA);
									startQuest(QUEST_VELHARUA);
								}
								showMessage("Será que você poderia me ajudar?", CHARACTER_VELHINHA,
									"Sim", function() : void {
										lockInput(true);
										setCharacterExpression( CHARACTER_VELHINHA, Character.EXPRESSION_NONE );
										
										fadeOut();
										setCharacterTileByPortal( CHARACTER_VELHINHA, PORTAL_VELHA2);
										setCharacterDirection (CHARACTER_VELHINHA, Direction.DIRECTION_DOWN);
										setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_CHARVELHA1 );
										setCharacterDirection (CHARACTER_JOGADOR, Direction.DIRECTION_UP);
										fadeIn();
										showMessage( "Muito obrigada!\nSó mais uma rua agora...", CHARACTER_VELHINHA );
										fadeOut();
										setCharacterTileByPortal(CHARACTER_VELHINHA, PORTAL_VELHA3);
										setCharacterDirection (CHARACTER_VELHINHA, Direction.DIRECTION_UP);
										setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_CHARVELHA2);
										setCharacterDirection (CHARACTER_JOGADOR, Direction.DIRECTION_DOWN);
										fadeIn();
										showMessage(" Meu nome é Anita, sou a moradora mais antiga desta vila.\nQuando precisar de ajuda com qualquer coisa é só ir até a minha casa. Ela é logo ali, aquela com um jardim na frente.", CHARACTER_VELHINHA);
										endQuest(QUEST_VELHARUA);
										vars.part1quests ++;
										checkPart1Quests();
										vars.falouVovo = true;
										fadeOut();
										setCharacterTileByPortal(CHARACTER_VELHINHA, PORTAL_VELHA4);
										setCharacterDirection(CHARACTER_VELHINHA, Direction.DIRECTION_DOWN);
										fadeIn();
										lockInput(false);
									},
									"Não", function() : void {
										showMessage("Me desculpe! Você deve estar com pressa.", CHARACTER_VELHINHA);
										vars.falouVovo = true;
									} );
							} else {
								showMessage("Você é muito gentil! Apareça a qualquer hora, jovem!", CHARACTER_VELHINHA);
							}
							lockInput(false);
							break;
						
						case CHARACTER_MULHER8:
							if( vars.cachorro == 2 ) {
								setCharacterExpression( CHARACTER_MULHER8, Character.EXPRESSION_NONE );
								showMessage( "Kiko? Pensando bem, eu vi um cachorro passar por aqui... Ele foi pra esquerda!", CHARACTER_MULHER8 );								
							}
						break;
						
						case CHARACTER_BOMBEIRO:
							if( getQuestStatus (QUEST_DENGUE) == QuestState.QUEST_STATE_UNDISCOVERED )
							{
								showMessage( "Bom dia. Sou Gustavo, chefe dos bombeiros.", CHARACTER_BOMBEIRO);
								showMessage( "A Vila está tendo alguns casos de dengue. Isso é preocupante em um lugar com tão poucas pessoas. Precisamos controlar esse surto!", CHARACTER_BOMBEIRO);
								showMessage( "Por isso, se encontrar focos de mosquito aedes aegypti, me avise.", CHARACTER_BOMBEIRO,
											 "Pfff... Não vou gastar meu tempo com isso. Vou apenas passar um repelente.", function() : void 
											 {
												 wait( 0.1 );
												 showMessage( "........................", CHARACTER_BOMBEIRO );
												 showMessage( "Você é muito jovem para pensar desta forma. Se todos nos ajudarmos, combateremos nossos problemas de uma forma mais eficaz. Reflita mais um pouco e volte aqui depois.", CHARACTER_BOMBEIRO );
											 },
											 "Claro! Vamos acabar com eles!", function() : void
											 {
												 setCharacterExpression( CHARACTER_BOMBEIRO, Character.EXPRESSION_NONE );
												 wait( 0.1 );
												 showMessage( "Esse é o espírito, jovem!", CHARACTER_BOMBEIRO );
												 showMessage( "Como você vai saber se é o mosquito certo? Na dúvida, me avise sobre qualquer foco de mosquitos.", CHARACTER_BOMBEIRO );
												 showMessage( "Nós não estamos sozinhos nesta busca. Um outro garoto está nos ajudando. Talvez ele até já tenha descoberto alguma coisa.", CHARACTER_BOMBEIRO );
												 showMessage( "O nome dele é Enzo. Ele é um garotinho ainda pequeno, que pergunta sobre tudo. Ele também estava vestindo uma camisa laranja.", CHARACTER_BOMBEIRO );
												 setCharacterExpression( CHARACTER_PERGUNTADOR, Character.EXPRESSION_EXCLAMATION );
												 startQuest( QUEST_DENGUE );				 
											 }
										   );
								
							}
							else if( getQuestStatus (QUEST_DENGUE) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								if( vars.denguePlay )
								{
									showMessage("Querem me ajudar? Vamos limpar alguns focos de dengue e eliminar os mosquitos. São só 6 níveis!", CHARACTER_BOMBEIRO,
										"Sim", function():void
										{
											wait( 0.1 );
											scheduleMiniGame( Constants.MINIGAME_DENGUE );
										},
										"Não", function():void
										{
											wait( 0.1 );
											showMessage("Me avise quando quiser.", CHARACTER_BOMBEIRO);
										});
								}
								else
								{
									showMessage( "Encontrou alguma coisa? Geralmente esses focos aparecem em locais mal-cuidados", CHARACTER_BOMBEIRO );
								}
							}
							else
							{
								showMessage( "Obrigado pela ajuda. Talvez agora estejamos mais seguros.", CHARACTER_BOMBEIRO);
								showMessage( "Mas não se esqueça, é sempre melhor prevenir do que remediar! Nunca deixe água parada em qualquer recipiente.", CHARACTER_BOMBEIRO);
							}
							break;
						
						case CHARACTER_PERGUNTADOR:
							if( vars.denguePlay == false && getQuestStatus(QUEST_DENGUE) != QuestState.QUEST_STATE_COMPLETE )
							{	
								showMessage( "Eu vi uma casa cheia de mosquitos perto do casarão abandonado.", CHARACTER_PERGUNTADOR );
								if( getQuestStatus( QUEST_DENGUE ) == QuestState.QUEST_STATE_INCOMPLETE )
								{
									setCharacterExpression( CHARACTER_PERGUNTADOR, Character.EXPRESSION_NONE );
									lockInput( true );
									showMessage("O bombeiro pediu para você avisar a ele? Então avise!", CHARACTER_PERGUNTADOR);
									fadeOut();
									setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_CHARDENGUE1 );
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_LEFT);
									setCharacterTileByPortal( CHARACTER_PERGUNTADOR, PORTAL_PERGDENGUE1 );
									setCharacterDirection( CHARACTER_PERGUNTADOR, Direction.DIRECTION_LEFT );
									setCharacterDirection( CHARACTER_BOMBEIRO, Direction.DIRECTION_RIGHT );
									fadeIn();
									showMessage("O quê? Perto do casarão? Vamos lá imediatamente.", CHARACTER_BOMBEIRO);
									fadeOut();
									setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_CHARDENGUE2 );
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_UP);
									setCharacterTileByPortal( CHARACTER_PERGUNTADOR, PORTAL_PERGDENGUE2 );
									setCharacterDirection( CHARACTER_PERGUNTADOR, Direction.DIRECTION_UP);
									setCharacterTileByPortal( CHARACTER_BOMBEIRO, PORTAL_BOMBEIRODENGUE2 );
									setCharacterDirection( CHARACTER_BOMBEIRO, Direction.DIRECTION_UP);
									fadeIn();
									
									showMessage("É aqui? Nossa, tem muitos mosquitos. Vocês querem me ajudar? Dessa forma vamos demorar menos e vocês podem aprender mais!", CHARACTER_BOMBEIRO);
									showMessage("O que me dizem, querem ajudar? São só 6 níveis!", CHARACTER_BOMBEIRO,
										"Sim", function(): void
										{
											lockInput( false );
											setCharacterExpression( CHARACTER_PERGUNTADOR, Character.EXPRESSION_NONE );
											scheduleMiniGame( Constants.MINIGAME_DENGUE );
										},
										"Não", function() : void
										{
											showMessage( "Me digam quando puderem ajudar, senão vai levar muito tempo para limpar isso aqui!", CHARACTER_BOMBEIRO );
											vars.denguePlay = true;
											lockInput(false );
										});
								}
							} else if( vars.denguePlay == true && getQuestStatus (QUEST_DENGUE) != QuestState.QUEST_STATE_COMPLETE) {
								showMessage("Vamos tentar ajudar a combater a dengue!", CHARACTER_PERGUNTADOR);
							} else {
								showMessage("Que bom que conseguimos avisar o Bombeiro Gustavo. A dengue é uma doença muito perigosa.", CHARACTER_PERGUNTADOR);
							}
							break;
																		
						case CHARACTER_VENDEDORA:
							showMessage("Bom dia! Precisa de alguma coisa?", CHARACTER_VENDEDORA,
							"Sim", function() : void
							{
								lockInput( true );
								setBackground( Constants.BACKGROUND_VENDA );
								if( getQuestStatus( QUEST_SACOLAS ) == QuestState.QUEST_STATE_UNDISCOVERED )
								{
									wait( 0.1 );
									showMessage( "Está procurando alguma coisa específica?", CHARACTER_VENDEDORA );
									showMessage( "Todas as frutas, legumes e verduras aqui são frescas e limpas.", CHARACTER_VENDEDORA );
									showMessage( "O quê? Balas e chocolate?", CHARACTER_VENDEDORA );
									showMessage( ".....................", CHARACTER_VENDEDORA );
									showMessage( "Rá Rá Rá Rá", CHARACTER_VENDEDORA );
									showMessage( "Esses jovens! Volte aqui quando quiser comprar um legume, uma fruta ou uma verdura!", CHARACTER_VENDEDORA );
									setBackground( Constants.BACKGROUND_MAP );
									lockInput( false );
								}
								else if( getQuestStatus( QUEST_SACOLAS ) == QuestState.QUEST_STATE_INCOMPLETE )
								{
									wait( 0.1 );
									showMessage( "Está procurando alguma coisa específica?", CHARACTER_VENDEDORA );
									showMessage( "O quê? Trocar as sacolas de plástico?", CHARACTER_VENDEDORA );
									showMessage( "Por que eu deveria trocar?", CHARACTER_VENDEDORA,
			/////////////////////////////////// ARGUMENTO 1 ////////////////////////////////////////////
												 "Elas prejudicam o meio ambiente. Demoram até 300 anos para se degradar.", function() : void
												 {
			
													/////////////////////////////////// ARGUMENTO 2 ////////////////////////////////////////////
													wait( 0.1 );
													showMessage( "Nunca recebi reclamações das pessoas, e a Vila é um lugar pequeno.", CHARACTER_VENDEDORA,
																 "Por que a senhora não pensa melhor sobre isso?", function() : void
																 {
																	wait( 0.1 );
																	showMessage( "Por favor, me dê licença que preciso trabalhar.", CHARACTER_VENDEDORA );
																	setBackground( Constants.BACKGROUND_MAP );
																	lockInput( false );
																 },
																 "Além do problema ambiental, as sacolas são perigosas para as crianças que podem se asfixiar ao brincar com elas.", function() : void
																 { 
																	/////////////////////////////////// ARGUMENTO 3	 ////////////////////////////////////////////
																	wait( 0.1 );
																	showMessage( "Eu poderia até trocar, se conhecesse alternativas, mas no momento não tenho tempo para ver isso!", CHARACTER_VENDEDORA,
																				 "A senhora poderia se informar mais sobre o assunto.", function() : void
																				 {
																					 wait( 0.1 );
																					showMessage( "Por favor, me dê licença que preciso trabalhar.", CHARACTER_VENDEDORA );
																					setBackground( Constants.BACKGROUND_MAP );
																					lockInput( false );
																				 },
																				 "A senhora poderia vender sacolas reutilizáveis para os clientes.", function() : void
																				 {
																					wait( 0.1 );
																					showMessage( "Hum....", CHARACTER_VENDEDORA );
																					showMessage( "Essa é uma boa idéia.", CHARACTER_VENDEDORA );
																					showMessage( "É, você está certo mesmo. Vou comprar sacolas reutilizáveis para vender aos meus clientes.\nTambém farei cartazes com essas informações que você me deu. Obrigada!", CHARACTER_VENDEDORA );
																					setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_NONE );
																					endQuest( QUEST_SACOLAS );
																					setBackground( Constants.BACKGROUND_MAP );
																					lockInput( false );
																				 },
																				 "Com licença então, vou deixar a senhora trabalhar.", function() : void
																				 {
																					 wait( 0.1 );
																					showMessage( "Sim, vá. Estou muito ocupada aqui.", CHARACTER_VENDEDORA );
																					setBackground( Constants.BACKGROUND_MAP );
																					lockInput( false );
																				 });
																 },
																 "É verdade. Me desculpe pelo incômodo", function() : void
																 {
																	wait( 0.1 );
																	showMessage( "Tudo bem. Volte quando precisar de algo", CHARACTER_VENDEDORA );
																	setBackground( Constants.BACKGROUND_MAP );
																	lockInput( false );
																 });
												},
												"A senhora está gastando dinheiro à toa com essas sacolas.", function() : void
												{
													wait( 0.1 );
													showMessage( "O dinheiro é considerável, mas como os meus clientes levariam suas compras para casa?", CHARACTER_VENDEDORA );
													showMessage( "Está vendo? Agora me dê licença, pois preciso trabalhar.", CHARACTER_VENDEDORA );
													setBackground( Constants.BACKGROUND_MAP );
													lockInput( false );
												},
												"Me Desculpe, não incomodarei mais a senhora. Obrigado.", function() : void
												{
													wait( 0.1 );
													showMessage( "Até mais então.", CHARACTER_VENDEDORA );
													setBackground( Constants.BACKGROUND_MAP );
													lockInput( false );
												});								
								}
								else
								{
									wait( 0.1 );
									showMessage( "Que bom que troquei as sacolas! Sem isso o lixão continuaria a crescer!", CHARACTER_VENDEDORA );
									showMessage( "Ainda bem que eu tive essa idéia!", CHARACTER_VENDEDORA );
									showMessage( ".................................", CHARACTER_VENDEDORA );
									showMessage( "Huahuahua", CHARACTER_VENDEDORA );
									showMessage( "Estou brincando com você! A idéia foi sua, é claro! E lhe agradeço muito!", CHARACTER_VENDEDORA );
									showMessage( "Lhe darei um desconto quando você vier fazer compras!", CHARACTER_VENDEDORA );
									setBackground( Constants.BACKGROUND_MAP );
									lockInput( false );
								}
							},
							"Não", function(): void
							{
								wait( 0.1 );
								showMessage( "Se precisar de algo, pode pedir.", CHARACTER_VENDEDORA );
							});
							break;
						
						case CHARACTER_POLITICO:
							if( vars.part1quests >= 3 )
							{
								showMessage("Você já fez o que o prefeito lhe pediu? Já completou todas as missões que encontrou?", CHARACTER_POLITICO,
									"Sim", function() : void {
										setCharacterExpression( CHARACTER_POLITICO, Character.EXPRESSION_NONE );
										wait( 0.1 );
										lockInput(true);
										showMessage("Vou avisá-lo assim que estiver livre. Pode ir agora...", CHARACTER_POLITICO );
										fadeOut();
										wait( 1.0 );
										setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_PARTE1F);
										setCharacterDirection(CHARACTER_JOGADOR , Direction.DIRECTION_UP);
										setCharacterTileByPortal(CHARACTER_PERGUNTADOR, PORTAL_PARTE1P);
										setCharacterDirection(CHARACTER_PERGUNTADOR , Direction.DIRECTION_LEFT);
										setCharacterDirection(CHARACTER_POLITICO , Direction.DIRECTION_RIGHT);
										fadeIn();
										showMessage( "Senhor Patrício!", CHARACTER_PERGUNTADOR);
										showMessage( "O que houve, garoto?", CHARACTER_POLITICO);
										showMessage( "Senhor Patrício, o prefeito está passando mal na frente da escola!", CHARACTER_PERGUNTADOR);
										showMessage( "Oh, céus... Tudo bem. Vou lá ver o que está acontecendo...", CHARACTER_POLITICO);
										showMessage( "Você. Espere no gabinete enquanto eu trago ele de volta para cá.", CHARACTER_POLITICO);
										queueNextPart(PARTE_2);
									},
									"Não", function() : void {
										wait( 0.1 );
										showMessage("Então vá fazer e me avise quando terminar.", CHARACTER_POLITICO );
									} );
							}
							// OLD : Vamos deixar o Patrício falar uma de suas falas genéricas, determinadas no método prepareToPart
//							else
//							{
//								showMessage( "O que foi agora?", CHARACTER_POLITICO );
//							}
							break;
						
						case CHARACTER_GUARDA:
							if( getQuestStatus ( QUEST_CACHORRO ) == QuestState.QUEST_STATE_UNDISCOVERED )
							{
								showMessage( "Arf... Arf... Arf...", CHARACTER_GUARDA );
								showMessage( "Olá! Desculpe! Não te vi chegando!\nEstou tentando pegar um cachorro fujão... De novo.", CHARACTER_GUARDA );
								showMessage( "Me faça um favor? Se você o vir e ele gostar de você, traga-o até mim!" , CHARACTER_GUARDA,
											 "Pode deixar!", function() : void
											 {
								 				wait( 0.1 );
								 				showMessage( "Obrigado!!!", CHARACTER_GUARDA );
												setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );
												setCharacterExpression( CHARACTER_CACHORRO, Character.EXPRESSION_EXCLAMATION );
												startQuest( QUEST_CACHORRO );
											 },
											 "Não posso, sou alérgico...", function() : void
									   	  	 {
												wait( 0.1 );
												showMessage( "É mesmo? Eu sou alérgico a camarão... Uma vez fiquei todo empolado depois de comer um bobó. Fico todo arrepiado só de lembrar.", CHARACTER_GUARDA );
										  	 },
											 "De jeito nenhum! Ele deve ter pulgas!", function() : void
											 {
												wait( 0.1 );
												showMessage( "O Kiko é bem limpinho! Ele vai a uma loja pet shop toda semana para tomar banho.", CHARACTER_GUARDA );
											 }
										  );

							}
							else if( getQuestStatus ( QUEST_CACHORRO ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								showMessage( "Arf... Arf... Arf...", CHARACTER_GUARDA );
								showMessage( "Minhas pernas doem...", CHARACTER_GUARDA );
							}
							else
							{
								showMessage( "Lembre-se: sempre que um animal desconhecido aparecer pelas redondezas, chame o controle de zoonoses. \nSó ajude quando for um cachorro conhecido, como o Kiko.", CHARACTER_GUARDA );
							}
							break;
						
						case CHARACTER_HOMEM8:
							if( getQuestStatus ( QUEST_CACHORRO ) == QuestState.QUEST_STATE_UNDISCOVERED )
							{
								showMessage( "O meu cachorro, Kiko, fugiu! O guarda Amaral nunca consegue pegá-lo. Ele corre muito rápido!", CHARACTER_HOMEM8 );
							}
							else if( getQuestStatus ( QUEST_CACHORRO ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								showMessage( "Então você está ajudando o guarda Amaral? Não sei como agradecer! O Kiko costuma ficar perto da prefeitura, mas vira e mexe foge para a mata do outro lado da cidade, será que ele fugiu para lá de novo?", CHARACTER_HOMEM8 );
							}
							else
							{
								showMessage( "Muito obrigado por achar o Kiko! Não sei o que eu faria sem ele...", CHARACTER_HOMEM8 );
							}
							break;
						
						case CHARACTER_CACHORRO:
							if( getQuestStatus ( QUEST_CACHORRO ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								setCharacterExpression( CHARACTER_CACHORRO, Character.EXPRESSION_NONE );
								if ( vars.cachorro == 0 )
								{
									wait( 0.1 );
									lockInput(true);
									showMessage("Au! Au! Au!", CHARACTER_CACHORRO );
									vars.cachorro ++;
									fadeOut();
									setCharacterTile( CHARACTER_JOGADOR, PORTAL_CACHORRO0.x + 1, PORTAL_CACHORRO0.y );
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_UP );
									fadeIn();
									dogWow( PORTAL_CACHORRO0.x + 1, PORTAL_CACHORRO0.y, Direction.DIRECTION_UP );
									setCharacterTile( CHARACTER_CACHORRO, PORTAL_CACHORRO1.x, PORTAL_CACHORRO1.y );
									wait(0.5);
									showMessage( "!!!!!!!!!!!!!!!!" );
									showMessage( "Ele sumiu! Você terá que achá-lo novamente..." );
									lockInput(false);
									
								}
								else if ( vars.cachorro == 1 )
								{
									wait( 0.1 );
									lockInput(true);
									showMessage("Au! Auuu!!", CHARACTER_CACHORRO );
									vars.cachorro ++;
									fadeOut();
									setCharacterTile( CHARACTER_JOGADOR, PORTAL_CACHORRO1.x, PORTAL_CACHORRO1.y + 1 );
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_LEFT );
									fadeIn();
									dogWow( PORTAL_CACHORRO1.x, PORTAL_CACHORRO1.y + 1, Direction.DIRECTION_LEFT );
									setCharacterTile( CHARACTER_CACHORRO, PORTAL_CACHORRO2.x, PORTAL_CACHORRO2.y );
									setCharacterExpression( CHARACTER_MULHER8, Character.EXPRESSION_EXCLAMATION );
									wait(0.5);
									showMessage( "De novo!!!!!! Para onde será que ele foi agora?" );
									lockInput(false);
									
								}
								else if( vars.cachorro == 2 )
								{
									wait( 0.1 );
									lockInput(true);
									showMessage("Au! Woof! Au...", CHARACTER_CACHORRO );
									vars.cachorro ++;
									fadeOut();
									setCharacterTile( CHARACTER_JOGADOR, PORTAL_CACHORRO2.x, PORTAL_CACHORRO2.y + 1 );
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_LEFT );
									fadeIn();
									dogWow( PORTAL_CACHORRO2.x, PORTAL_CACHORRO2.y + 1, Direction.DIRECTION_LEFT );
									setCharacterTile( CHARACTER_CACHORRO, PORTAL_CACHORRO3.x, PORTAL_CACHORRO3.y );
									setCharacterExpression( CHARACTER_MULHER8, Character.EXPRESSION_NONE );
									wait(0.5);
									showMessage( "Mas que danado!!! Ele não deve estar longe!" );
									lockInput(false);
									
								}
								else if ( vars.cachorro == 3 )
								{
									wait( 0.1 );
									lockInput(true);
									showMessage("Woof... Woof... Au...", CHARACTER_CACHORRO );
									vars.cachorro ++;
									fadeOut();
									setCharacterTile( CHARACTER_JOGADOR, PORTAL_CACHORRO3.x + 1, PORTAL_CACHORRO3.y );
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_UP );
									fadeIn();
									dogWow( PORTAL_CACHORRO3.x + 1, PORTAL_CACHORRO3.y, Direction.DIRECTION_UP );
									setCharacterTile( CHARACTER_CACHORRO, PORTAL_CACHORRO4.x, PORTAL_CACHORRO4.y );
									wait(0.5);
									showMessage( "Tenho certeza que da próxima vez ele não escapa..." );
									lockInput(false);
									
								}
								else if (vars.cachorro == 4 )
								{
									wait( 0.1 );
									lockInput(true);
									setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );
									setCharacterExpression( CHARACTER_HOMEM8, Character.EXPRESSION_NONE );
									
									showMessage("Woof...\nWoof...\nArf... arf... arf...", CHARACTER_CACHORRO);
									showMessage( "Ele não tem para onde fugir!" );
									showMessage( "Agora você pode levá-lo de volta para o guarda Amaral." );
									fadeOut();
									setCharacterTile( CHARACTER_CACHORRO, PORTAL_CACHORRO5.x, PORTAL_CACHORRO5.y );
									setCharacterDirection( CHARACTER_CACHORRO, Direction.DIRECTION_LEFT )
									setCharacterTile( CHARACTER_HOMEM8, 82, 89 );
									setCharacterDirection( CHARACTER_HOMEM8, Direction.DIRECTION_LEFT )
									setCharacterTile( CHARACTER_JOGADOR, 83, 87);
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT )
									setCharacterDirection( CHARACTER_GUARDA, Direction.DIRECTION_RIGHT)
									wait(1.5);
									fadeIn();
									wait(0.5);
									showMessage("Woof! Woof!", CHARACTER_CACHORRO);
									showMessage("Aqui está seu cachorro.", CHARACTER_GUARDA);
									showMessage("Kiko!", CHARACTER_HOMEM8);
									showMessage("Obrigado Guarda Amaral!", CHARACTER_HOMEM8);
									showMessage("Não há de que, garoto.", CHARACTER_GUARDA);
									fadeOut();
									removeCharacter(CHARACTER_HOMEM8);
									removeCharacter(CHARACTER_CACHORRO);
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_UP );
									setCharacterDirection( CHARACTER_GUARDA, Direction.DIRECTION_DOWN );
									wait(0.5);
									fadeIn();
									showMessage( "Obrigado por me ajudar! Não teria conseguido pegar aquele cachorro sozinho. Ele é muito rápido.\nO delegado vai ficar sabendo que você nos ajudou! Obrigado!", CHARACTER_GUARDA);
									endQuest(QUEST_CACHORRO);
									vars.part1quests++;
									checkPart1Quests();
									lockInput(false);
								}
							} else {
								showMessage( "Woof, woof!", CHARACTER_CACHORRO );
							}
							break;
						
						case CHARACTER_GARI:
							if( getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_UNDISCOVERED )
							{
								showMessage( "Olá! Meu nome é Kleber.\nTrabalho para a URBLIMP, a companhia de coleta de lixo da Vila Virtual. Será que você pode me ajudar?", CHARACTER_GARI,
											 "Sim", function() : void 
											 {
												 wait( 0.1 );
												 showMessage( "Recentemente a cidade vem produzindo mais lixo do que a URBLIMP consegue recolher. Isso vem prejudicando a passagem das pessoas pela rua.\nSem contar o mau cheiro!", CHARACTER_GARI );
												 showMessage( "Estou tentando avisar as pessoas, para montarmos um mutirão!\nSe você puder encontrar mais pessoas dispostas a ajudar pela cidade eu ficaria muito agradecido!", CHARACTER_GARI );
												 showMessage( "Nosso ponto de encontro será aqui, entre a prefeitura e a praça.", CHARACTER_GARI );
												 startQuest ( QUEST_ENTULHO );	
												 
												 setCharacterExpression( CHARACTER_HOMEM4, Character.EXPRESSION_EXCLAMATION );
												 setCharacterExpression( CHARACTER_HOMEM9, Character.EXPRESSION_EXCLAMATION );
												 setCharacterExpression( CHARACTER_HOMEM11, Character.EXPRESSION_EXCLAMATION );
												 setCharacterExpression( CHARACTER_MULHER4, Character.EXPRESSION_EXCLAMATION );
												 setCharacterExpression( CHARACTER_MULHER5, Character.EXPRESSION_EXCLAMATION );
											 },
											"Não", function() : void
											{
												wait( 0.1 );
												showMessage( "......................", CHARACTER_GARI );
												showMessage( "Você deve estar preocupado com outras coisas... Mas entenda que o meio ambiente é responsabilidade de todos.", CHARACTER_GARI );
											});
							}
							else if ( getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								if( vars.mutirao == MUTIRAO_TOTAL - 1 )
								{
									showMessage( "Parece que você está indo bem! Só falta 1 pessoa agora!", CHARACTER_GARI );
									showMessage( "Nosso ponto de encontro será aqui, entre a prefeitura e a praça.", CHARACTER_GARI );
								}
								else
								{
									showMessage( "E aí? Como vai a missão? Ainda precisamos de mais " + ( MUTIRAO_TOTAL - vars.mutirao ) + " pessoas para retirar o lixo.", CHARACTER_GARI );
									showMessage( "Nosso ponto de encontro será aqui, entre a prefeitura e a praça.", CHARACTER_GARI );
								}
							}
							else
							{
								showMessage( "Muito obrigado pela ajuda! Tomara que as pessoas se conscientizem e produzam menos lixo de agora em diante...", CHARACTER_GARI );
							}
							break;
						
						case CHARACTER_CEGO:
							showMessage("Olá!\nTem alguém aí?", CHARACTER_CEGO);
							break;
						
						case CHARACTER_DELEGADO:
							showMessage("Ai, ai...\nComo eu adoro essa cidade...", CHARACTER_DELEGADO );
							break;
						
						case CHARACTER_VICIADO5:
							lockInput( true );
							setBackground( Constants.BACKGROUND_BECO );
							showMessage( "Quem te convidou? Esse beco é nosso.", CHARACTER_VICIADO5 );
							setBackground( Constants.BACKGROUND_MAP );
							lockInput( false );
							break;
				//QUEST_ENTULHO
						case CHARACTER_OBJ_ENTULHO1:
						case CHARACTER_OBJ_ENTULHO2:
						case CHARACTER_OBJ_ENTULHO3:
						case CHARACTER_OBJ_ENTULHO4:
						case CHARACTER_OBJ_ENTULHO5:
						case CHARACTER_OBJ_ENTULHO6:
							if(getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_UNDISCOVERED){
								showMessage("É impossível recolher todo este lixo sozinho.");								
							} else {
								if ( vars.mutirao == MUTIRAO_TOTAL - 1 )
									showMessage( "Que lixo fedorento... Falta apenas 1 pessoa para retirá-lo daqui!" );
								else
									showMessage( "Ainda faltam " + ( MUTIRAO_TOTAL - vars.mutirao ) + " pessoas para conseguir retirar este lixo." );
							}
							break;
						
		/////////////////////////////////////////////////////[ PESSOAS DA QUEST_ENTULHO ] ///////////////////////////////////////////////////
						case CHARACTER_HOMEM4:
							if(getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_UNDISCOVERED)
							{
								showMessage("Essa cidade está muito suja. Onde estão os trabalhadores da URBLIMP?", CHARACTER_HOMEM4);
							}
							else if(getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE)
							{
								if (vars.mutirao1 == false)
								{
									showMessage( "Cadê esses trabalhadores da URBLIMP! Será que eles estão se esforçando realmente? O cheiro está insuportável!", CHARACTER_HOMEM4,
												 "Por mais que eles trabalhem duro, a população continua sujando tudo!", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "É verdade? Nunca tinha pensado por esse lado...", CHARACTER_HOMEM4 );
													 showMessage( "Acho que eu tenho que parar de reclamar e começar a ajudar. Você sabe como eu poderia começar?", CHARACTER_HOMEM4,
																 "Sim, o Kleber está criando um mutirão.", function() : void
																 {
																	 wait( 0.1 );
																	 showMessage("O Kleber está organizando um mutirão? Pode contar comigo então!",CHARACTER_HOMEM4);
																	 vars.mutirao ++;
																	 vars.mutirao1 = true;
																	 setCharacterExpression( CHARACTER_HOMEM4, Character.EXPRESSION_NONE );

																	 SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
																	 
																	 getMutirao();
																 },
																 "Não, mas você poderia olhar na internet.", function() : void
																 {
																	 wait( 0.1 );
																	 showMessage( "Tudo bem! Vou olhar! Mas se você ficar sabendo de algo mais, me avise!", CHARACTER_HOMEM4 );
																 });
												 },
												 "Podemos usar pregadores no nariz!", function() : void
												 {
													 wait( 0.1 );
													 showMessage("..............................",CHARACTER_HOMEM4);
													 showMessage("Não adianta... Já tentei isto na semana passada... Me deu uma baita falta de ar! Precisamos de uma solução mais permanente e para todos.",CHARACTER_HOMEM4);
												 } );
								}
								else
								{
									showMessage( "Pode contar comigo! Quando as pessoas se juntarem eu estarei lá!",CHARACTER_HOMEM4);
								}
							}
							else // if(getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_COMPLETE)
							{
								showMessage( "Nossa! A nossa vila está muito melhor sem todo aquele lixo! Tinha me esquecido como o ar limpo é bom!",CHARACTER_HOMEM4);
							}
							break;

						case CHARACTER_HOMEM11:
							if (getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE) {
								if (vars.mutirao2 == false) {
									showMessage("Eu acho que a cidade está muito suja, especialmente perto da praça.\nTemos que parar de jogar lixo na rua!",CHARACTER_HOMEM11,
												"Estamos organizando um mutirão", function() : void
																				  {
																						wait( 0.1 );
																						showMessage( "Sério? Onde vocês estão combinando de se encontrar?", CHARACTER_HOMEM11,
																									 "Perto da praça",	 function() : void
																														 {
																										 					 wait( 0.1 );
																															 showMessage("Vamos lá!",CHARACTER_HOMEM11);
																															 vars.mutirao ++;
																															 vars.mutirao2 = true;
																															 setCharacterExpression( CHARACTER_HOMEM11, Character.EXPRESSION_NONE );
																															 
																															 SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
																															 
																															 getMutirao();
																														 },
																									 "Perto do posto de gasolina", function() : void
																									 								  {
																										 												 wait( 0.1 );
																										 									    showMessage( "Tem certeza? O Juca costuma manter tudo limpinho por lá... Você poderia confirmar o local e depois voltar aqui para me avisar?", CHARACTER_HOMEM11 );
																									 								  },
																									 "Não sei...",	 function() : void
																													 {
																										 				 wait( 0.1 );
																														 showMessage( "Às vezes eu também sou meio esquecido! Por isso comecei a anotar os meus compromissos mais importantes e inadiáveis! Por exemplo:\nHoje, 20:00, passar da última fase do meu novo jogo de videogame!", CHARACTER_HOMEM11 );
																														 showMessage( "O quê? Isso não é tão importante?", CHARACTER_HOMEM11);
																														 showMessage( "Minha namorada sempre fala a mesma coisa... Enfim, volte aqui souber o lugar.", CHARACTER_HOMEM11);
																													 });
																				  },
												"Poderíamos ter mais lixeiras pela vila", function() : void
																							{
																								wait( 0.1 );
																								showMessage( "Concordo. Isso ajudaria muito. Mas, de qualquer jeito, as pessoas não deveriam achar que a falta de lixeiras dá a permissão para elas jogarem lixo no chão.", CHARACTER_HOMEM7 );
																							});
								} else {
									showMessage("Fico chateado com as pessoas que jogam lixo nas ruas.", CHARACTER_HOMEM7);
									showMessage("Além de complicar o trabalho dos garis, o lixo no chão da rua ainda pode prejudicar o escoamento dos bueiros e causar enchentes!", CHARACTER_HOMEM7);
									showMessage("Estou indo para lá! Pode deixar!",CHARACTER_HOMEM7);
								}
								
							} else if (getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_COMPLETE){
								showMessage("Sabia que as sacolas plásticas podem sufocar?",CHARACTER_HOMEM7);
								showMessage("É perigoso deixar as crianças perto delas. Elas acabam brincando de colocar a cabeça dentro delas e isso pode deixá-las sem ar!",CHARACTER_HOMEM7);
							} else {
								showMessage("Fico chateado com as pessoas que jogam lixo nas ruas.", CHARACTER_HOMEM7);
								showMessage("Além de complicar o trabalho dos garis, o lixo no chão da rua ainda pode prejudicar o escoamento dos bueiros e causar enchentes!", CHARACTER_HOMEM7);
							} 
							break;
						
						case CHARACTER_HOMEM9:
							if (getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE) {
								if (vars.mutirao3 == false) {
									showMessage("Mutirão para limpar as ruas? Isso é trabalho da URBLIMP!", CHARACTER_HOMEM9);
									showMessage("Eu pago meus impostos para manter a Vila limpa. Não quero colocar a minha mão no lixo se estou pagando para outra pessoa fazer isso.", CHARACTER_HOMEM9,
										"Se conseguissemos organizar o mutirão seria muito mais rápido.", function() : void
										{
											wait( 0.1 );
											showMessage("Se o lixo não tivesse acumulado não precisariamos fazer um mutirão. A responsabilidade é da URBLIMP.", CHARACTER_HOMEM9);
										},
										"A rua está assim porque estamos produzindo muito lixo e também jogando na rua. É nossa responsabilidade cuidar do patrimônio público.", function() : void
										{
											wait( 0.1 );
											showMessage("Tem razão. Se cada um fizesse sua parte tudo ficaria mais fácil para eles.", CHARACTER_HOMEM9);
											showMessage("Estou envergonhado. Vou ajudar. Pode me encontrar lá.", CHARACTER_HOMEM9);
											vars.mutirao ++;
											vars.mutirao3 = true;
											setCharacterExpression( CHARACTER_HOMEM9, Character.EXPRESSION_NONE );
											
											SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
											
											getMutirao();
										},
										"Tem razão. Desculpe incomodar.", function() : void
										{
											// Vazio mesmo
										} 
									);
									
								} else {
									showMessage("Você está certo. Temos que cuidar da nossa cidade. Se cada um fizer sua parte fica tudo mais fácil.", CHARACTER_HOMEM9);
									showMessage("Pode me encontrar lá.", CHARACTER_HOMEM9);
								}
							} else {
								showMessage("Tem muitos mosquitos naquela casa.",CHARACTER_HOMEM9);
							}
							break;
						
						case CHARACTER_MULHER4:
							if (getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE) {
								if (vars.mutirao5 == false){
									showMessage("O Kleber está organizando um mutirão?",CHARACTER_MULHER4);
									showMessage("Ótimo! Topo qualquer coisa para agilizar essa limpeza.\nNão aguento mais o cheiro!", CHARACTER_MULHER4);
									vars.mutirao5 = true;
									vars.mutirao ++;
									setCharacterExpression( CHARACTER_MULHER4, Character.EXPRESSION_NONE );
									
									SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
									
									getMutirao();
								} else {
									showMessage("Topo qualquer coisa para agilizar essa limpeza.\nNão aguento mais esse cheiro!", CHARACTER_MULHER4);
								}
							} else if (getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_UNDISCOVERED) {
								showMessage("O lixo está deixando a praça impossível de se frequentar. O cheiro é muito ruim! ECA!", CHARACTER_MULHER4);
							} else {
								showMessage("Que alívio. Ainda bem que limpamos aquilo tudo!", CHARACTER_MULHER4);
							}
							break;
						
						case CHARACTER_MULHER5:
							if (getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE) {
								if ( vars.mutirao6 == false ) {
										showMessage("Mutirão? Está tão ruim assim?", CHARACTER_MULHER7);
										showMessage("Me dê um bom motivo para não deixar a URBLIMP tomar conta disso.", CHARACTER_MULHER7,
										"Além do mal cheiro, pode ajudar na proliferação de ratos, baratas e outros vetores de doença. Também pode provocar enchentes.", function() : void
										{
											wait( 0.1 );
											showMessage("Nossa! Eu não sabia disso!", CHARACTER_MULHER7);
											showMessage("Com certeza vou ajudar.", CHARACTER_MULHER7);
											vars.mutirao6 = true;
											vars.mutirao ++;
											setCharacterExpression( CHARACTER_MULHER5, Character.EXPRESSION_NONE );
											
											SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
											
											getMutirao();
										},
										"Vamos conseguir solucionar o problema muito mais rápido se todos ajudarem.", function() : void
										{
											wait( 0.1 );
											showMessage("Não estou convencida. Acho que o lixo ainda não está insuportável.",CHARACTER_MULHER7);
										},
										"É verdade. Desculpe incomodar.", function() : void
										{
											// Vazio mesmo
										});
								} else {
										showMessage("Detesto ratos e baratas. Tenho nojo. Sem contar as doenças!", CHARACTER_MULHER7);
										showMessage("Vou ajudar, com certeza!", CHARACTER_MULHER7);
									
								}
							}
							// Vamos utilizar os discursos genéricos determinados em prepareToPart
//							else
//							{
//							}
							break;
						

					};
				break;
/////////////////////////////////////////////////////////////////////////
//////////////////////////////////[  PARTE2  ]///////////////////////////
/////////////////////////////////////////////////////////////////////////
				case PARTE_2:
					switch( characterId )
					{
						case CHARACTER_BOMBEIRO:
							showMessage( "Vi um cara estranho passando por aqui na direção do casarão... Será que eu deveria avisar ao Magalhães?", CHARACTER_BOMBEIRO );
							break;
						
						case CHARACTER_FRENTISTA:
							if( vars.part2quests >= 3 )
							{
								if( getQuestStatus( QUEST_POLITICO ) == QuestState.QUEST_STATE_UNDISCOVERED )
								{
									showMessage( "Ihhhhhhh!!!!", CHARACTER_FRENTISTA );
									showMessage( "Nossa! Você apareceu do nada!", CHARACTER_FRENTISTA );
									showMessage( "Desculpe-me, nesses últimos dias eu ando bastante estressado.", CHARACTER_FRENTISTA,
												 "Você deveria relaxar, fazer yoga ou algo assim", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Yoga? Não... Fiz durante 2 anos. Não aguento mais... Mas vou procurar outra coisa. Obrigado.", CHARACTER_FRENTISTA );
												 },
												 "Me falaram que você anda bem preocupado. Não sabia que era tanto!", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Sim, é verdade. O problema é que não sei a quem recorrer.", CHARACTER_FRENTISTA,
														 		  "Qual o seu problema? Talvez eu possa ajudar", function() : void
																  {
																	  wait( 0.1 );
																	  showMessage( "É mesmo?", CHARACTER_FRENTISTA );
																	  showMessage( "........................", CHARACTER_FRENTISTA );
																	  showMessage( "Tudo bem, você me parece legal, acho que posso confiar em você.", CHARACTER_FRENTISTA );
																	  showMessage( "Há pouco tempo atrás um cara estranho abasteceu um caminhão aqui no posto.", CHARACTER_FRENTISTA );
																	  showMessage( "Só que esse caminhão é o que distribui a merenda nas escolas na vila!", CHARACTER_FRENTISTA );
																	  showMessage( "O que isso tem de errado? Até aí realmente nada... O problema é que o caminhão ainda estava cheio! E o motorista estava indo para fora da cidade, e não entrando nela!", CHARACTER_FRENTISTA );
																	  showMessage( "Me preocupa também o fato de meu irmão mais novo estar reclamando da falta de merenda na escola.", CHARACTER_FRENTISTA );
																	  showMessage( "O que eu acho é o seguinte: a merenda não está sendo distribuída, está sendo roubada!", CHARACTER_FRENTISTA );
																	  showMessage( "Para piorar eu vi esse mesmo cara do caminhão conversando com o Patrício! Entendeu o meu drama?!", CHARACTER_FRENTISTA );
																	  showMessage( "O QUE EU FAÇO?!?!?!?!?!", CHARACTER_FRENTISTA,
																	  			   "Podemos falar com o guarda Amaral", function() : void
																				   {
																						wait( 0.1 );
																					  	showMessage( "O Guarda Amaral?", CHARACTER_FRENTISTA );
																						
																						if( getQuestStatus( QUEST_CACHORRO ) == QuestState.QUEST_STATE_COMPLETE )
																						{
																							showMessage( "É verdade! Ele já ajudou a recuperar o cachorro Kiko várias vezes!", CHARACTER_FRENTISTA );
																							showMessage( "Talvez ele possa me ajudar também!", CHARACTER_FRENTISTA );
																						}
																						else
																						{
																							showMessage( "É... Pode ser uma boa idéia...", CHARACTER_FRENTISTA );
																						}
																						showMessage( "Você pode falar com ele para mim? Não posso sair do posto durante o trabalho, senão acabo perdendo o emprego.", CHARACTER_FRENTISTA );
																					    setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_NONE );
																						setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_EXCLAMATION );
																						setCharacterExpression( CHARACTER_VELHINHA, Character.EXPRESSION_EXCLAMATION );
																					  	startQuest( QUEST_POLITICO );
																				   },
																	  			   "Podemos falar com a Dona Zefa", function() : void
																				   {
																					   wait( 0.1 );
																					   showMessage( "Sim, a Dona Zefa é sábia... Talvez ela saiba o que fazer...", CHARACTER_FRENTISTA );
																					   showMessage( "Você pode falar com ela para mim? Não posso sair do posto durante o trabalho, senão acabo perdendo o emprego.", CHARACTER_FRENTISTA );
																					   setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_EXCLAMATION );
																					   setCharacterExpression( CHARACTER_VELHINHA, Character.EXPRESSION_EXCLAMATION );
																					   setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_NONE );
																					   startQuest( QUEST_POLITICO );
																				   },
																	  			   "É rapaz, sujou, não sei o que fazer não... Boa sorte!", function() : void
																				   {
																					   wait( 0.1 );
																					   showMessage( "Entendo... Sei que é uma situação realmente complicada... Mas tenho que resolver isso.", CHARACTER_FRENTISTA );
																					   showMessage( "Vou tentar fazê-lo sozinho, não vou mais lhe encomodar.", CHARACTER_FRENTISTA );
																				   });
																  },
													 			  "Ligue para o Disque Denúncia!", function() : void
																  {
																	  wait( 0.1 );
																	  showMessage( "........................", CHARACTER_FRENTISTA );
																  });
												 },
												 "Desculpa, não queria te assustar, voltarei outra hora", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Tudo bem... Até mais...", CHARACTER_FRENTISTA );
												 });
								}
								else if( getQuestStatus( QUEST_POLITICO ) == QuestState.QUEST_STATE_INCOMPLETE )
								{
									showMessage( "....................", CHARACTER_FRENTISTA );
								}
								else
								{
									showMessage( "Valeu mesmo! Eu já estava me descabelando! Agora estou muito mais tranquilo.", CHARACTER_FRENTISTA );
								}
							}
							else
							{
								showMessage( "Ahh!!!!!!", CHARACTER_FRENTISTA );
								showMessage( "..............", CHARACTER_FRENTISTA );
								showMessage( "Nossa! Você me deu um susto!", CHARACTER_FRENTISTA );
								showMessage( "Desculpe-me, estou apenas um pouco preocupado.", CHARACTER_FRENTISTA );
							}
							break;
						
						case CHARACTER_VELHINHA:
							if( getQuestStatus (QUEST_VELHARUA) == QuestState.QUEST_STATE_COMPLETE )
							{
								showMessage( "Bom dia, jovem! É um dia bonito, não?", CHARACTER_VELHINHA);
								showMessage( "Estou assando biscoitos. Enquanto esperamos eles ficarem prontos, você quer falar sobre alguma coisa?", CHARACTER_VELHINHA,
											"Sim", function() : void
											{
												oldLadyHintsPart2();
											},
											"Não", function() : void
											{
												wait( 0.1 );
												showMessage( "Se precisar de alguma coisa, não precisa fazer cerimônia!", CHARACTER_VELHINHA );
											});
							}
							else
							{
								showMessage( "Bom dia, jovem.", CHARACTER_VELHINHA );
							}
							break;
												
						case CHARACTER_VICIADO5:
							lockInput( true );
							setBackground( Constants.BACKGROUND_BECO );
							showMessage( "Quem te convidou? Esse beco é nosso.", CHARACTER_VICIADO5 );
							setBackground( Constants.BACKGROUND_MAP );
							lockInput( false );
							break;
						
						case CHARACTER_GARI:
							if( getQuestStatus( QUEST_COLETA)  == QuestState.QUEST_STATE_COMPLETE )
							{
								showMessage( "Não se esqueça de divulgar o que aprendeu sobre coleta seletiva. Poucas pessoas conhecem.", CHARACTER_GARI);
							}
							else
							{
								if( getQuestStatus( QUEST_COLETA ) == QuestState.QUEST_STATE_UNDISCOVERED )
								{
									if( getQuestStatus( QUEST_ENTULHO ) == QuestState.QUEST_STATE_COMPLETE )
										showMessage( "Olá! Obrigado de novo pela sua ajuda! Hoje queria conversar sobre outro problema que temos aqui na Vila Virtual. Não vai ser problema para você.",CHARACTER_GARI);
									else
										showMessage( "Olá. Será que você poderia ajudar a resolver um outro problema que temos aqui na Vila Virtual?",CHARACTER_GARI);
										
									showMessage( "Já tem um tempo que a URBLIMP se equipou para disponibilizar um serviço de Coleta Seletiva para os moradores.\nVocê poderia me ajudar?",CHARACTER_GARI, 
												 "Não, estou com pressa agora.", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "OK. Volte uma outra hora então.", CHARACTER_GARI );
												 },
												 "Mas o que é coleta seletiva do lixo?", function() : void
												 {
													 wait( 0.1);
													 showMessage( "Pois bem, Coleta Seletiva é separar o lixo em tipos, para que seja possível reciclar tudo da forma certa! \nSepara-se vidro, papel, metal, material orgânico, dentre outros.",CHARACTER_GARI);
													 showMessage( "Os modos de se reciclar mudam de acordo com o material. O vidro e o papel, por exemplo, são completamente diferentes.",CHARACTER_GARI);
													 showMessage( "Falando em vidro, você sabia que ainda não descobriram quanto tempo ele demora para se degradar?! Agora pense em todas as garrafas de vidro que as pessoas jogam fora sem separar corretamente.", CHARACTER_GARI);
													 showMessage( "Se o vidro for reciclado, ele pode ser utilizado novamente, o que também diminui os gastos com a produção de garrafas novas.",CHARACTER_GARI);
													 showMessage( "Por que tem que separar o lixo em casa? Não tem como deixar o trabalho de separar todo o lixo de uma cidade nas mãos da companhia de lixo! É lixo demais!",CHARACTER_GARI);
													 showMessage( "Separar o lixo da forma correta permite que ele seja reciclado mais facilmente. O aterro que tem na saída da cidade, por exemplo, seria bem menor se reciclassem o máximo possível.",CHARACTER_GARI);
													 startMinigameRecycling( true );
												 });
								}
								else
								{
									startMinigameRecycling( false );
								}
							}
							break;
						
						case CHARACTER_GUARDA:
							var copTalkedPart2 : Boolean = false;
							if( getQuestStatus (QUEST_HIPPIE) == QuestState.QUEST_STATE_UNDISCOVERED )
							{
								lockInput(true);
								setCharacterDirection(CHARACTER_GUARDA, Direction.DIRECTION_90_CLOCKWISE);
								setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );
								showMessage("Ahh... Que droga...", CHARACTER_GUARDA);
								wait(1);
								setCharacterDirection(CHARACTER_GUARDA, Direction.DIRECTION_90_COUNTER_CLOCKWISE);
								showMessage("Ah! Você está aí!\nEstou com problemas...", CHARACTER_GUARDA);
								showMessage("Você já conhece o Lamar? Ele vende CDs piratas.\nEntão... Isso é comércio ilegal. Eu e o delegado somos amigos dele tem bastante tempo, e sempre tentamos ajudá-lo...", CHARACTER_GUARDA);
								showMessage("Ele vende bastante. Já deveria ter dinheiro para regularizar o comércio e começar a vender produtos originais. Fazer as coisas do jeito certo, sabe?", CHARACTER_GUARDA);
								showMessage("Se ele não conseguir se regularizar até o mês que vem, vou ter que apreender a mercadoria dele de novo.", CHARACTER_GUARDA);
								showMessage("Não quero que isso aconteça... Será que você pode convencê-lo a regularizar seu negócio?", CHARACTER_GUARDA);
								
								setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_EXCLAMATION );
								startQuest(QUEST_HIPPIE);
								lockInput(false);
								
								copTalkedPart2 = true;
							}
							else if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								showMessage( "E então? Conseguiu falar com o Lamar?", CHARACTER_GUARDA);
								
								copTalkedPart2 = true;
							}
							
							if( getQuestStatus( QUEST_POLITICO ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );
								showMessage( "O quê? O assessor do prefeito está conversando com um cara estranho?\nVou averiguar isso... Se puder, avise o Delegado Magalhães, por favor.", CHARACTER_GUARDA );
								setCharacterExpression( CHARACTER_DELEGADO, Character.EXPRESSION_EXCLAMATION );
								copTalkedPart2 = true;
							}
							
							if( !copTalkedPart2 )
							{
								if( getQuestStatus( QUEST_CACHORRO ) == QuestState.QUEST_STATE_COMPLETE )
									showMessage( "Olá!", CHARACTER_GUARDA );
								else
									showMessage( "As pessoas não reconhecem o meu trabalho...", CHARACTER_GUARDA );
							}
							
							break;
						
						case CHARACTER_HIPPIE:
								if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_INCOMPLETE )
								{
									if( vars.falouHippie == false )
									{
										setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_NONE );
										showMessage("Olá. Está procurando algum CD?", CHARACTER_HIPPIE);
										showMessage("Vão tomar os meus Cds? Então estou ficando sem tempo!\nNa verdade, eu já deixei um pedido para o prefeito autorizar que eu regularize meu comércio, mas ele nunca olha! É muita burocracia.",CHARACTER_HIPPIE);
										showMessage("Sem comprovar que sou um comerciante legalizado eu não tenho como conseguir um fornecedor! E para conseguir legalizar o meu comércio eu preciso da autorização da prefeitura!", CHARACTER_HIPPIE);
										showMessage("O que vou fazer agora?", CHARACTER_HIPPIE);
										vars.falouHippie = true;
										setCharacterExpression( CHARACTER_PREFEITO, Character.EXPRESSION_EXCLAMATION );
									}
									else
									{
										showMessage( "O que vou fazer agora...? Só o prefeito pode me ajudar, mas onde será que ele está?", CHARACTER_HIPPIE );
									}
								}
								else if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_UNDISCOVERED )
								{
									showMessage("Olá. Está procurando algum CD?", CHARACTER_HIPPIE);
								}
								else
								{
									setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_NONE );
									showMessage( "Ah! É você! O prefeito me disse que você falou com ele sobre mim! Muito obrigado! Agora talvez eu consiga regularizar o meu negócio!", CHARACTER_HIPPIE );
								}
							break;
						
						case CHARACTER_CADEIRANTE:
								if ( getQuestStatus (QUEST_CADEIRANTE) == QuestState.QUEST_STATE_UNDISCOVERED)
								{
									showMessage( "Olá! Meu nome é Larissa! Você pode me ajudar? Quero ir até o campo de futebol encontrar meu irmão. Mas tem um problema...\nVê aquele carro do outro lado da rua?",CHARACTER_CADEIRANTE);
									showMessage( "Além de ele estar estacionado em cima da faixa de pedestres, o que é contra a lei, ele está bloqueando o acesso de cadeirantes...\nO acesso é aquela rampa ali. Sem ela fica difícil para subir na calçada." ,CHARACTER_CADEIRANTE);
									showMessage( "Você pode pedir para ele estacionar em outro lugar para mim?", CHARACTER_CADEIRANTE,
												 "Sim, é claro!", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Oh! Muito obrigada! Gostaria que existissem mais pessoas como você.", CHARACTER_CADEIRANTE );
													 startQuest( QUEST_CADEIRANTE );
												 },
												 "É um absurdo! Vou lá falar com ele agora mesmo!", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Oh! Muito obrigada! Às vezes o egoísmo das pessoas me irrita também. Mas de repente ele nem sabe para que servem as rampas das calçadas.", CHARACTER_CADEIRANTE );
													 startQuest( QUEST_CADEIRANTE );
												 },
												 "Ele me parece meio violento... Desculpe.", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "......................", CHARACTER_CADEIRANTE );
													 showMessage( "É uma pena, não vou poder ver meu irmão jogar futebol...", CHARACTER_CADEIRANTE );
												 }
											   );
								}
								else if ( getQuestStatus (QUEST_CADEIRANTE) == QuestState.QUEST_STATE_INCOMPLETE)
								{
									showMessage("E então, você conseguiu falar com ele? Quem sabe se você insistir um pouco...",CHARACTER_CADEIRANTE);
								}
								else
								{
									showMessage( "Obrigada novamente!", CHARACTER_CADEIRANTE );
								}
							break;
						
						case CHARACTER_IRMAO_LARISSA:
							if( getQuestStatus( QUEST_CADEIRANTE ) == QuestState.QUEST_STATE_COMPLETE )
							{
								showMessage( "Legal! A minha irmã veio me ver jogar futebol!", CHARACTER_IRMAO_LARISSA );
							}
							else
							{
								showMessage( "Cadê a minha irmã? Ela falou que estava vindo me ver jogar futebol...", CHARACTER_IRMAO_LARISSA );
							}
							break;
						
						case CHARACTER_PLAYBOYCAR:
							if(getQuestStatus(QUEST_CADEIRANTE) == QuestState.QUEST_STATE_INCOMPLETE) {
								switch ( vars.playboy_talks ) {
									case 0:
										showMessage("O quê? Tirar o carro daqui? A rua é pública, posso parar onde quiser!", CHARACTER_PLAYBOYCAR);
										vars.playboy_talks++;
									break;
									
									case 1:
										showMessage("Você de novo? Qual o problema de parar na faixa de pedestre? Me deixa quieto com meu possante aqui, rapaz!", CHARACTER_PLAYBOYCAR);
										vars.playboy_talks++;
									break;
									
									case 2:
										showMessage("Tem tanto lugar para atravessar a rua por aí, você quer atravessar logo na faixa? Não consegue pular o meio-fio? Rá-rá-rá!", CHARACTER_PLAYBOYCAR);
										vars.playboy_talks++;
									break;
									
									case 3:
										showMessage("Você é realmente insistente... Mas qual o problema da Larissa atravessar em outro lugar?", CHARACTER_PLAYBOYCAR);
										vars.playboy_talks++;
									break;
									
									case 4:
										showMessage("Ahhhhhh! Então essas rampas são para cadeirantes? Nunca havia reparado como o meio-fio é alto para as cadeiras de roda. Pra isso que as rampas servem então! Não sabia...", CHARACTER_PLAYBOYCAR);
										vars.playboy_talks++;
									break;
									
									default:
										setCharacterExpression( CHARACTER_CADEIRANTE, Character.EXPRESSION_NONE );
										lockInput( true );
										showMessage( "É, você me convenceu, vou sair daqui. Desculpe minha atitude, fui realmente um mané...", CHARACTER_PLAYBOYCAR );
										showMessage( "Peça desculpas a ela por mim!", CHARACTER_PLAYBOYCAR );
										fadeOut();
										setCharacterTileByPortal(CHARACTER_PLAYBOYCAR, PORTAL_CARRO2);
										setCharacterTileByPortal(CHARACTER_CADEIRANTE, PORTAL_CADEIRANTE2);
										setCharacterDirection(CHARACTER_CADEIRANTE, Direction.DIRECTION_LEFT);
										setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_CHARCADEIRANTE);
										setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT);
										fadeIn();
										showMessage("Obrigada!\nA maioria das pessoas não sabe que aquela rampa é acesso para cadeirantes e acaba parando na frente.", CHARACTER_CADEIRANTE);
										fadeOut();
										setCharacterTileByPortal(CHARACTER_CADEIRANTE, PORTAL_FUTEBOL1);
										setCharacterDirection(CHARACTER_CADEIRANTE, Direction.DIRECTION_DOWN);
										vars.part2quests++;
										fadeIn();
										endQuest(QUEST_CADEIRANTE);
										checkPart2( CHARACTER_CADEIRANTE );
										lockInput( false );
								}
							} else {
								showMessage("Yeah!", CHARACTER_PLAYBOYCAR);
							}
							break;
						
						case CHARACTER_CEGO:
							if( getQuestStatus( QUEST_CEGORUA ) == QuestState.QUEST_STATE_UNDISCOVERED )
							{
								showMessage( "Olá? Alguém aí?\nAh, olá! Como você pode perceber, eu sou cego.", CHARACTER_CEGO );
								showMessage( "Preciso atravessar a rua até o quarteirão do posto de saúde. Para que lado fica?", CHARACTER_CEGO,
											 "Eu também não sei onde fica o posto de saúde.", function() : void
											 {
												wait( 0.1 );
												showMessage( "Ah! Já sei! Você é cego também?", CHARACTER_CEGO );
											 },
											 "Eu te levo. Vamos lá.", function() : void
											 {
												 wait( 0.1 );
												 startQuest( QUEST_CEGORUA );
											 }
										   );
							}
							else if( getQuestStatus( QUEST_CEGORUA ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								showMessage("Oh! Achei que você tinha ido embora. Vamos?", CHARACTER_CEGO,
											"Sim", function() : void
											{
												wait( 0.1 );
												lockInput( true );
												setCharacterExpression( CHARACTER_CEGO, Character.EXPRESSION_NONE );
												fadeOut();
												setCharacterTileByPortal(CHARACTER_CEGO, PORTAL_CEGO2);
												setCharacterDirection(CHARACTER_CEGO,Direction.DIRECTION_RIGHT);
												setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_CHARCEGO);
												setCharacterDirection(CHARACTER_JOGADOR,Direction.DIRECTION_LEFT);
												fadeIn();
												showMessage("Muito obrigado! Aqueles moleques passaram aqui e me giraram, então eu perdi a direção!\nTrombadinhas!", CHARACTER_CEGO );
												showMessage("Me chamo Alcebíades. Obrigado novamente pela ajuda!",CHARACTER_CEGO);
												endQuest(QUEST_CEGORUA);
												vars.part2quests++;
												checkPart2( CHARACTER_CEGO );
												lockInput( false );
											},
											"Não", function() : void
											{
												wait( 0.1 );
												showMessage("Tudo bem então, eu dou um jeito, se aqueles moleques não me atrapalharem de novo...",CHARACTER_CEGO);
											}
										);
							}
							// Deixa o personagem falar um de seus discursos genéricos
//							else
//							{
//								showMessage( "Muito obrigado pela ajuda!", CHARACTER_CEGO );
//							}
							break;
						
						case CHARACTER_PERGUNTADOR:
							if(vars.part2quests >= 3) {
								showMessage("Ví o Patrício indo na direção do casarão abandonado.", CHARACTER_PERGUNTADOR);
							} else {
								showMessage("O que é vigilância sanitária?\nPor que será que quando eles vêm a Dona Zefa está sempre sorrindo?\nE por que o Sr. Wong fica com aquela cara estranha?", CHARACTER_PERGUNTADOR);
							}
							break;
						
						case CHARACTER_VENDEDORA:
							showMessage( "Precisa de alguma coisa?", CHARACTER_VENDEDORA,
										"Sim", function() : void
										{
											// Prepara as respostas disponíveis
											var dialog : Array = new Array();
											
											dialog.push( "Peito de frango.", function() : void
															{
																wait( 0.1 );
																showMessage( "Peito de frango? Ora! Eu não vendo isso aqui! Além do mais, eu sou vegetariana. Não como carne de animais.", CHARACTER_VENDEDORA );
																showMessage( "Mas você pode procurar um açougue.", CHARACTER_VENDEDORA );
																showMessage( "Volte quando quiser um legume ou verdura para uma boa salada!", CHARACTER_VENDEDORA );
																setBackground( Constants.BACKGROUND_MAP );
																lockInput( false );
															} );
											
											if( ( getQuestStatus( QUEST_CHINA ) == QuestState.QUEST_STATE_INCOMPLETE ) && hasItem( ITEM_LUVAS ) == false )
											{
												dialog.push( "Luvas e toucas para o Sr. Wong.", function() : void
																{
																	setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_NONE );	
																	setCharacterExpression( CHARACTER_CHINA, Character.EXPRESSION_EXCLAMATION );
																	wait( 0.1 );
																	showMessage("Ora, o Sr. Wong precisa de luvas e touca? Eu tenho!\nSempre uso para colocar as verduras e os legumes nas bancadas.",CHARACTER_VENDEDORA);
																	showMessage("Pode levar! Diga que se precisar de mais delas posso vender pacotes para ele.\nE deseje boa sorte com a vigilância sanitária! É só fazer as coisas do jeito certo que não tem dor de cabeça.",CHARACTER_VENDEDORA);
																	addItem(ITEM_LUVAS);
																	addItem(ITEM_TOUCA);
																	setBackground( Constants.BACKGROUND_MAP );
																	lockInput( false );
																} );
											}
											
											dialog.push( "Na verdade, nem eu sei.", function() : void
														{
															wait( 0.1 );
															showMessage( "....................", CHARACTER_VENDEDORA );
															showMessage( "Rá Rá Rá Rá", CHARACTER_VENDEDORA );
															showMessage( "Essa juventude! Volte quando souber então.", CHARACTER_VENDEDORA );
															setBackground( Constants.BACKGROUND_MAP );
															lockInput( false );
														});
											
											// Exibe a cena
											lockInput( true );
											setBackground( Constants.BACKGROUND_VENDA );
											
											// As linhas abaixo equivalem a: showMessage( "O que você está procurando?", CHARACTER_VENDEDORA, answers );
											dialog.unshift( "O que você está procurando?", CHARACTER_VENDEDORA );
											showMessage.apply( instance, dialog );
										},
										"Não", function() : void
										{
											showMessage( "Se precisar, é só falar.", CHARACTER_VENDEDORA );
										});
							break;
						
						case CHARACTER_CHINA:
							if( getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								if( hasItem(ITEM_LUVAS) && hasItem(ITEM_TOUCA) )
								{
									setCharacterExpression( CHARACTER_CHINA, Character.EXPRESSION_NONE );
									vars.part2quests++;
									
									lockInput( true );
									showMessage( "Muito obrigado. Vou colocá-las agora mesmo!", CHARACTER_CHINA);
									removeItem( ITEM_LUVAS );
									removeItem( ITEM_TOUCA );
									endQuest( QUEST_CHINA );
									// evita que o prefeito fique no mesmo tile que o Patricio
									if ( vars.part2quests < 3 ) {
										removeCharacter( CHARACTER_POLITICO );
									}
									setCharacterTileByPortal( CHARACTER_PREFEITO, PORTAL_ASSISTENTE );
									setCharacterDirection( CHARACTER_PREFEITO, Direction.DIRECTION_RIGHT );
									
									fadeOut();
									var guardPreviousExpression : int = getCharacter( CHARACTER_GUARDA ).expression;
									setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );
									setCharacterTileByPortal( CHARACTER_GUARDA, PORTAL_CHINA_LADO );
									setCharacterDirection( CHARACTER_GUARDA, Direction.DIRECTION_RIGHT );
									setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_CHINA_LADO_1 );									
									setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_LEFT );
									
									fadeIn();
									showMessage( "Ainda bem que te encontrei! Falei com o Lamar há pouco e ele me disse que, se recolhermos os CDs dele, ele vai acabar saindo da cidade!", CHARACTER_GUARDA );
									showMessage( "Já falou com ele? Temos que ver isso rapidamente! O Lamar comentou alguma coisa sobre licença, prefeito, alvará... Não lembro direito. É melhor falar com ele mesmo, o quanto antes!", CHARACTER_GUARDA );
									fadeOut();
									setCharacterTileByPortal( CHARACTER_GUARDA, PORTAL_GUARDA_1 );
									setCharacterExpression( CHARACTER_GUARDA, guardPreviousExpression );
									fadeIn();
									
									checkPart2( CHARACTER_CHINA );
									lockInput( false );
								}
								else
								{
									setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_EXCLAMATION );
									setCharacterExpression( CHARACTER_CHINA, Character.EXPRESSION_NONE );
									showMessage("Infelizmente não sei onde achar touca e luvas, e como estou sempre trabalhando acabo não tendo tempo para pesquisar. Você pode consegui-las para mim?", CHARACTER_CHINA);
									showMessage("Me ajudaria até com os problemas que venho tendo com a vigilância sanitária.\nQuem sabe assim eu não consigo uma licença para abrir minha lanchonete...", CHARACTER_CHINA);
								}
							}
							else
							{
								showMessage( "Vai querer um pastel de quê?", CHARACTER_CHINA );
							}
							break;
						
						case CHARACTER_PREFEITO:
							if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_INCOMPLETE && vars.falouHippie )
							{
								setCharacterExpression( CHARACTER_PREFEITO, Character.EXPRESSION_NONE );
								showMessage( "O quê? O Lamar precisa regularizar o seu comércio? Ele está tentando falar comigo há algum tempo e nunca consegue?", CHARACTER_PREFEITO );
								showMessage( "Estranho... O Patrício deveria ter me falado sobre isso... Ele deve ter se esquecido.", CHARACTER_PREFEITO );
								showMessage( "Mas pode deixar. Não vou me esquecer do Lamar. É obrigação da prefeitura atender a um pedido de um cidadão seu!", CHARACTER_PREFEITO );
								vars.part2quest++;
								endQuest( QUEST_HIPPIE );
								checkPart2( CHARACTER_PREFEITO );
							}
							else
							{
								showMessage( "Ah, me sinto melhor...\nVocê viu o Patrício?", CHARACTER_PREFEITO );
							}
							break;
						
						case CHARACTER_POLITICO:
							if( vars.part2quests >= 3 )
							{
								showMessage("Saia daqui. Estou tratando de assuntos importantes.",CHARACTER_POLITICO);	
							}
							else
							{
								lockInput(true);
								showMessage("O prefeito está no toalete no momento...", CHARACTER_POLITICO);
								wait(1);
								showMessage("...hehehe...", CHARACTER_POLITICO);
								lockInput(false);
							}
							break;
						
						case CHARACTER_DELEGADO:
							if (getQuestStatus(QUEST_POLITICO) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								lockInput(true);
								setCharacterExpression( CHARACTER_DELEGADO, Character.EXPRESSION_NONE );
								showMessage("O quê? Quem? O assessor do Sr. Lopes? Nunca fui com a cara desse tal de Patrício... Ele está aqui desde o mandato do último prefeito. Onde ele está?",CHARACTER_DELEGADO);
								fadeOut();
								setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_P2DELEGADO);
								setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_LEFT);
								setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_P2PLAYER);
								setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_LEFT);
								setCharacterTileByPortal(CHARACTER_GUARDA, PORTAL_P2GUARDA);
								setCharacterDirection(CHARACTER_GUARDA, Direction.DIRECTION_UP);
								removeCharacter(CHARACTER_TRAFICANTE);
								setCharacterDirection(CHARACTER_POLITICO, Direction.DIRECTION_RIGHT);
								fadeIn();
								showMessage("Delegado, consegui deter o Patrício, mas o outro homem fugiu.\nTemo que nossas suspeitas sejam verdade.",CHARACTER_GUARDA);
								showMessage("Isso é preocupante... Então é possível que a pessoa que está desviando as merendas seja de nossa própria Vila Virtual?",CHARACTER_DELEGADO);
								showMessage("Exatamente, Senhor.",CHARACTER_GUARDA);
								showMessage("Então é por isso que o Sr. Lopes está sempre sorrindo, mesmo com todos os problemas da cidade.\nVocê não informa o prefeito o que realmente está acontecendo, não é Patrício?",CHARACTER_DELEGADO);
								showMessage("Não tenho nada a declarar.",CHARACTER_POLITICO);
								showMessage("Você nunca mais vai colocar os pés nessa cidade sem algemas depois desse flagrante.",CHARACTER_GUARDA);
								showMessage("Calma, Amaral. Só podemos mantê-lo em custódia preventiva por enquanto, mas eu tenho um palpite sobre como provar isso tudo.",CHARACTER_DELEGADO);
								//Prefeito in
								fadeOut();
								setCharacterTileByPortal(CHARACTER_PREFEITO, PORTAL_P2PREFEITO);
								setCharacterDirection(CHARACTER_PREFEITO, Direction.DIRECTION_UP);
								setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_DOWN);
								setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_DOWN);
								fadeIn();
								showMessage("Como assim? Quer dizer que o Patrício estava escondendo de mim as reclamações de toda a Vila?",CHARACTER_PREFEITO);
								showMessage("Exatamente!",CHARACTER_DELEGADO);
								showMessage("Deve haver algum engano. Ele sempre foi muito prestativo. Vocês têm alguma prova?",CHARACTER_PREFEITO);
								showMessage("Você ficou sabendo sobre a falta de remédios no posto de saúde?",CHARACTER_GUARDA);
								showMessage("Não.",CHARACTER_PREFEITO);
								
								if( getQuestStatus( QUEST_HIPPIE ) != QuestState.QUEST_STATE_COMPLETE )
								{
									showMessage("Sobre o pedido do Lamar para legalizar o comércio dele?",CHARACTER_GUARDA);
									showMessage("Não.",CHARACTER_PREFEITO);
								}
								
								showMessage("Do problema com alguns orelhões da cidade que não funcionam?",CHARACTER_GUARDA);
								showMessage("Não.",CHARACTER_PREFEITO);	
								showMessage("Sobre o pedido do Sr. Wong para abrir a lanchonete dele próximo à estrada?",CHARACTER_GUARDA);
								showMessage("Não.",CHARACTER_PREFEITO);
								showMessage("Exatamente... todos eles alegaram que o seu assessor mencionou que você estava ocupado e que ele iria avisá-lo assim que possível.",CHARACTER_GUARDA);
								showMessage("Minha nossa!\nIsso é verdade Patrício?!",CHARACTER_PREFEITO);
								showMessage("Não tenho nada a declarar.\n\nPosso falar com meu advogado agora?",CHARACTER_POLITICO);
								showMessage("Isso é um absurdo! Quantos problemas não chegaram ao meu conhecimento até agora!?\nPreciso voltar para o meu escritório e começar a trabalhar imediatamente!",CHARACTER_PREFEITO);
								//Deleta prefeito, guarda e político
								fadeOut();
								removeCharacter(CHARACTER_PREFEITO);
								removeCharacter(CHARACTER_GUARDA);
								removeCharacter(CHARACTER_POLITICO);
								setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_RIGHT);
								setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_LEFT);
								fadeIn();
								showMessage("O homem que ajudava o Patrício não deve estar longe. Ele não vai escapar!",CHARACTER_DELEGADO);
								showMessage("Você fez um bom trabalho. Toda a cidade é grata por sua atitude!",CHARACTER_DELEGADO);
								endQuest(QUEST_POLITICO);
								
								if (getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_COMPLETE && getQuestStatus(QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE ){
									queueNextPart(PARTE_3, Constants.TILEMAP_AMBOS);
								} else if (getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_COMPLETE){
									queueNextPart(PARTE_3, Constants.TILEMAP_LANCHONETE);
								} else if (getQuestStatus(QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE) {
									queueNextPart(PARTE_3, Constants.TILEMAP_LOJA_CDS);
								} else {
									queueNextPart(PARTE_3, Constants.TILEMAP_NENHUM);
								}
							} else {
								showMessage("Qualquer problema, estou aqui para ajudar.", CHARACTER_DELEGADO);
							}
							//ADICIONAR EVENTO DA QUEST
							break;
						
					};
					break;
				
//////////////////////////////////////////////////////////////////////////
/////////////////////////////[ PARTE 3 ]//////////////////////////////////
//////////////////////////////////////////////////////////////////////////
			case PARTE_3:
					switch ( characterId )
					{
						case CHARACTER_CEGO:
							if( getQuestStatus (QUEST_CHINA) == QuestState.QUEST_STATE_COMPLETE)
							{
								if( getQuestStatus( QUEST_CEGOBRAILE) == QuestState.QUEST_STATE_UNDISCOVERED)
								{
									showMessage("Olá. Quem está aí?", CHARACTER_CEGO );
									showMessage("Oh, é você. Como vai?", CHARACTER_CEGO );
									showMessage("Mais ou menos!\nAdorava os pasteis do Sr. Wong, mas evitava comê-los pela falta de higiene na sua preparação. Queria voltar a comer seus pasteis, já que agora ele obedece às normas da vigilância sanitária.", CHARACTER_CEGO );
									showMessage("Mas, infelizmente, eu não consigo ler o cardápio! Eu sou cego, pamonhas!", CHARACTER_CEGO );
									showMessage("Eu recomendei que ele fizesse um cardápio em Braille. Sabe do que se trata?", CHARACTER_CEGO);
									showMessage("É um alfabeto para cegos. Nós podemos ler passando os dedos nas folhas, entende? Nós sentimos pequenos furinhos ou bolinhas no papel e conseguimos ler o que está escrito."	, CHARACTER_CEGO);
									showMessage("O Sr. Wong disse que está muito ocupado para cuidar disso agora.\nQuero só ver quanto tempo ele vai demorar para providenciar isso...", CHARACTER_CEGO);
									showMessage("Ei, você pode falar com ele para mim? Talvez ele te ouça.", CHARACTER_CEGO,
												"Sim", function() : void
												{
													wait( 0.1 );
													setCharacterExpression( CHARACTER_CEGO, Character.EXPRESSION_NONE );
													setCharacterExpression( CHARACTER_CRIANCA5, Character.EXPRESSION_EXCLAMATION );
													startQuest( QUEST_CEGOBRAILE );
													showMessage( "Você é uma pessoa realmente legal! Mal posso esperar para comer um pastel.", CHARACTER_CEGO );
												},
												"Não", function() : void
												{
													wait( 0.1 );
													showMessage( "Rooooooooooonc" );
													showMessage( "Ai... Meu estômago está pedindo um pastel!", CHARACTER_CEGO );
												});
									
								}
								else if( getQuestStatus (QUEST_CEGOBRAILE) == QuestState.QUEST_STATE_INCOMPLETE)
								{
									showMessage("Conseguiu falar com ele?", CHARACTER_CEGO);
								}
								else
								{
									showMessage("Obrigado! Você comeu algum pastel?\nEles são deliciosos!", CHARACTER_CEGO); 
								}
							}
							else
							{
								showMessage("Droga...o médico me proibiu de comer os pastéis do Sr.Wong.\nEle disse que eu podia ter algum problema de saúde por causa da limpeza.", CHARACTER_CEGO);
							}
							break;
						
						case CHARACTER_HOMEM5: // PORTAL_LANCHONETE5
							showMessage( "Cara... Eu não consigo parar de comer nessa lanchonete! Que pastel maravilhoso! Meu favorito é o pastel mágico!", CHARACTER_HOMEM5 ); 
							break;
							
						case CHARACTER_CHINA:
							if( getQuestStatus( QUEST_CHINA ) != QuestState.QUEST_STATE_COMPLETE )
							{
								showMessage( "Estou vendendo cada vez menos. O que será que eu estou fazendo de errado?", CHARACTER_CHINA );
							}
							else
							{
								showMessage( "Olá! Já conhece nossa nova lanchonete? Fique à vontade para visitá-la!", CHARACTER_CHINA,
											 "Quero conhecê-la.", function() : void
											 {
												 wait( 0.1 );
												 lockInput( true );
												 setBackground( Constants.BACKGROUND_LANCHONETE );
												 showMessage( "Olá! Fique à vontade para escolher de nosso cardápio!", CHARACTER_CHINA );
												 if( getQuestStatus( QUEST_CEGOBRAILE ) == QuestState.QUEST_STATE_INCOMPLETE )
												 {
												   if ( vars.china_braille == false ) {
													   showMessage( "Um cardápio em braille?", CHARACTER_CHINA );
													   showMessage( "Oh, claro, o Sr. Alcebíades já falou comigo sobre isso, mas eu não posso resolver agora.", CHARACTER_CHINA);
													   showMessage( "Também não sei onde posso conseguir alguém que faça o cardápio para mim, nem onde conseguir um livro ensinando isso.", CHARACTER_CHINA);
													   showMessage( "Não gosto de segregar o Sr. Alcebíades, e ele fica chateado quando eu me ofereço para ler o cardápio para ele. Além disso, com a lanchonete na beira da estrada, imagino que eu vá precisar disso em algum momento.", CHARACTER_CHINA);
													   showMessage( "Se tivesse como você falar com alguém sobre isso na escola, já me ajudaria muito. Eles devem ter informações sobre braille, já que têm alunos cegos estudando lá.", CHARACTER_CHINA );
													   
													   vars.china_braille = true;
													 } else {
													   showMessage( "E aí, conseguiu alguma informação sobre livros em braille na escola?", CHARACTER_CHINA );
													 }
													 
													 if( hasItem(ITEM_LIVROBRAILE) == true)
													 {
														 setCharacterExpression( CHARACTER_CHINA, Character.EXPRESSION_NONE );
														 showMessage("Ora, você encontrou o livro!", CHARACTER_CHINA);
														 removeItem( ITEM_LIVROBRAILE );
														 showMessage("Vou procurar alguém que possa fazer esse tipo de cardápio para mim...\nOu talvez eu possa dar um jeito sozinho.", CHARACTER_CHINA);
														 showMessage("O quê? Esse livro é da biblioteca da escola?\nTudo bem. Vou devolvê-lo quando acabar de usar.", CHARACTER_CHINA);
														 fadeOut();
														 setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_CHARCEGOP3);
														 setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_LEFT);
														 setCharacterDirection(CHARACTER_CEGO, Direction.DIRECTION_RIGHT);
														 setBackground( Constants.BACKGROUND_MAP, false );
														 fadeIn();
														 showMessage("Você conseguiu?",CHARACTER_CEGO);
														 showMessage("Tudo bem. Já é meio caminho andado para ele. Sem isso ele nem começaria\nObrigado!",CHARACTER_CEGO);
														 endQuest( QUEST_CEGOBRAILE );
														 lockInput( false );
													 }
													 else
													 {
														 showChinaMenu();
													 }
												 }
												 else
												 {
													 showChinaMenu();
												 }
											 },
											 "Agora não. Obrigado.", null );
							}
							break;
						
						case CHARACTER_HOMEM9: // Beto
							showMessage( "Conhece o menino do beco que sempre está de casaco cinza? O Beto. Ele gosta muito de música.", CHARACTER_HOMEM9 );
							showMessage( "Eu costumava ter uma banda com ele. Mas isso já faz muito tempo. Eu não quis seguir a carreira de músico, mas acho que ele não gostaria de ter parado de tocar.", CHARACTER_HOMEM9 );
							if( vars.betoquest == 0 ) {
								setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_EXCLAMATION );
								vars.betoquest = 1;
							}
							break;
						
						case CHARACTER_MULHER3: // Junior
							showMessage( "A Dona Zefa tem um filho, Junior. Ele tem matado as aulas da escola com aquele pessoal do beco... Antigamente ele costumava ser um ótimo aluno e muito obediente.", CHARACTER_MULHER3 );
							if( vars.juniorquest == 0 )
								vars.juniorquest = 1;
							break;

						case CHARACTER_MULHER5: // Piteco, Silas
							showMessage( "O Juca, do posto, trabalha pela mãe idosa e pelos dois irmãos. Acho que eles deveriam ajudar, mas eles só vivem naquele beco.", CHARACTER_MULHER5 );
							if( vars.pitecosilasquest == 0 )
								vars.pitecosilasquest = 1;
							break;
						
						case CHARACTER_HOMEM3: // Dan
							showMessage( "Aquele menino do gorro está sempre seguindo os outros garotos, mas eles não dão bola para ele.", CHARACTER_HOMEM3 );
							if( vars.danquest == 0 ) {
								vars.danquest = 1;
								setCharacterExpression( CHARACTER_VICIADO1, Character.EXPRESSION_EXCLAMATION );
						  }
							break;
						
						case CHARACTER_TRAFICANTE:
							lockInput( true );
							vars.traficantequestatividade = true;
							
							setCharacterExpression( CHARACTER_TRAFICANTE, Character.EXPRESSION_NONE );
							showMessage( "O que é que você está olhando, moleque?", CHARACTER_TRAFICANTE );
							wait( 0.5 );
							setCharacterDirection( CHARACTER_TRAFICANTE, Direction.DIRECTION_UP );
							wait( 0.5 );
							setCharacterDirection( CHARACTER_TRAFICANTE, Direction.DIRECTION_RIGHT );
							wait( 0.5 );
							setCharacterDirection( CHARACTER_TRAFICANTE, Direction.DIRECTION_LEFT );
							wait( 0.5 );
							setCharacterDirection( CHARACTER_TRAFICANTE, Direction.DIRECTION_DOWN );
							wait( 0.5 );
							setCharacterDirection( CHARACTER_TRAFICANTE, getOpositeDirection( getCharacterDirection( CHARACTER_JOGADOR ) ) );
							showMessage( "Esse cara é bastante estranho, parece estar escondendo alguma coisa... Melhor avisar o delegado!" );
							
							setCharacterExpression( CHARACTER_DELEGADO, Character.EXPRESSION_EXCLAMATION );
							lockInput( false );
							break;
						
						case CHARACTER_GUARDA:
							if( getQuestStatus( QUEST_ONG ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								showMessage( "Como está indo com os garotos? O Delegado Magalhães pode te ajudar.", CHARACTER_GUARDA );
								setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );

								if( getQuestStatus( QUEST_VELHARUA ) == QuestState.QUEST_STATE_COMPLETE ) {
									showMessage( "Talvez a Dona Anita também possa lhe ajudar com algumas dicas.", CHARACTER_GUARDA );
								}
							}
							else
							{
								showMessage( "Nós temos que achar aquele fugitivo!", CHARACTER_GUARDA );
							}
							break;
						
						case CHARACTER_DELEGADO:
							// O quest da ONG começa apenas quando a quest do traficante jpa terminou. Logo não podemos mudar a ordem destes testes!!!
							if( getQuestStatus( QUEST_ONG ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
							  if ( vars.contadorong > 4 || ( vars.contadorong == 4 && getQuestStatus( QUEST_HIPPIE ) != QuestState.QUEST_STATE_COMPLETE ) ) {
							    endOngQuest();
							  } else {
								  showMessage( "E então? Já fez o que podia para resolver os problemas daqueles meninos?", CHARACTER_DELEGADO,
											   "Sim", function() : void
											   {
												   wait( 0.1 );
												   lockInput( true );
												   
												   endOngQuest();		 
											   },
											   "Não", function() : void
											   {
												   wait( 0.1 );
												   showMessage( "Tudo bem. Me diga quando decidir que já fez todo o possível.", CHARACTER_DELEGADO ); 
											   },
											   "O que preciso fazer mesmo?", function() : void
											   {
												   wait( 0.1 );
												   showMessage( "Precisamos motivar os meninos para que eles voltem à escola, e parem de matar aula.", CHARACTER_DELEGADO ); 
												   showMessage( "O ideal seria trazermos todos eles de volta aos estudos, mas sabemos que isso nem sempre é fácil.", CHARACTER_DELEGADO ); 
												   switch ( vars.contadorong ) {
											       case 4:
											        showMessage( "Só tem mais um menino precisando de ajuda...", CHARACTER_DELEGADO );
											       break;
											       
											       default:
											        	showMessage( "Ainda há " + ( ( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE ? 5 : 4 ) - vars.contadorong ) + " meninos precisando de ajuda.", CHARACTER_DELEGADO );
												   }
												   showMessage( "Tente o seu melhor, e volte para falar comigo, que sua missão estará encerrada! Mesmo que você não consiga falar com todos, já estamos mais que orgulhosos do seu trabalho no programa Jovem Cidadão.", CHARACTER_DELEGADO ); 
											   });
										}
							}
							else if( vars.traficantequestatividade == true )
							{
								lockInput( true );
								setCharacterExpression( CHARACTER_DELEGADO, Character.EXPRESSION_NONE );
								showMessage( "O quê? O fugitivo? Onde?", CHARACTER_DELEGADO );
								delegadoGo();
								showMessage( "Finalmente te encontramos!", CHARACTER_DELEGADO );
								showMessage( "Quem?", CHARACTER_TRAFICANTE );
								showMessage( "Você mesmo. Nem adianta se fingir de bobo. Você será levado para a delegacia agora mesmo!", CHARACTER_DELEGADO);
								showMessage( "Vocês estão prendendo a pessoa errada. Eu não tenho nada a ver com essa história.", CHARACTER_TRAFICANTE);
								traficanteGo( true, false );
								removeCharacter( CHARACTER_TRAFICANTE );
								endQuest( QUEST_TRAFICANTE );
								setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_POLICIA2);
								setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_RIGHT);
								setCharacterTile(CHARACTER_JOGADOR, 102, 118);
								setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_LEFT);
								fadeIn();
								if( vars.prendeutraficante == false )
								{
								  setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_NONE );
									vars.prendeutraficante = true;
									setCharacterTile( CHARACTER_GUARDA, 103, 117 );
									setCharacterDirection( CHARACTER_GUARDA, Direction.DIRECTION_RIGHT );
									showMessage( "Obrigado pela ajuda. Infelizmente, não podemos mantê-lo preso só por ser suspeito. Precisamos de provas!", CHARACTER_DELEGADO );
									showMessage( "Mas agora o trabalho é comigo e com o Amaral. Não vamos descansar enquanto este caso não estiver resolvido!", CHARACTER_DELEGADO );
									showMessage( "Exatamente, pode contar com a gente! Você já nos ajudou muito, fazendo muito mais do que era esperado pelo programa Jovem Cidadão.", CHARACTER_GUARDA );
									showMessage( "Sim, é verdade. Você nos ajudou muito! Muito obrigado! Nos procure quando quiser nos visitar!", CHARACTER_DELEGADO );
									wait( 2.0 );
									showMessage( "O quê? Você quer continuar ajudando?", CHARACTER_DELEGADO );
									showMessage( "Hum... Você sabe de alguma coisa que está acontecendo na cidade, Amaral?", CHARACTER_DELEGADO );
									showMessage( "Bom... Existem aqueles moleques que vivem matando aula lá no beco. Não sei mais o que fazer com eles. Talvez se arrumássemos uma motivação para eles voltarem às aulas...", CHARACTER_GUARDA );
									showMessage( "Já sei! A ONG está oferecendo vários cursos para jovens!", CHARACTER_GUARDA );
									showMessage( "E o que isso tem a ver com matar aulas da escola?", CHARACTER_DELEGADO );
									showMessage( "A ONG exige que os jovens matriculados nos cursos estejam frequentando a escola! Se existirem cursos que agradem a esses garotos, resolveremos o problema!", CHARACTER_GUARDA );
									showMessage( "Boa idéia Amaral! Podemos tentar. Você resolveria mais esse problema para nós?", CHARACTER_DELEGADO,
												 "Sim", function() : void
												 {
													 wait( 0.1 );
													 startQuest( QUEST_ONG );
													 showMessage( "Fale com as pessoas da vila para descobrir os problemas daqueles garotos. É um ótimo jeito de começar.", CHARACTER_GUARDA );
													 showMessage( "Você me dá orgulho, jovem! Gostaria que mais pessoas fossem iguais a você.", CHARACTER_DELEGADO );
													 showMessage( "Veja quantos dos meninos você consegue levar de volta à escola. Não precisa ser todos eles, mas se esforce ao máximo.", CHARACTER_DELEGADO );
													 fadeOut();
													 setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_EXCLAMATION );
													 setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_EXCLAMATION );
													 setCharacterExpression( CHARACTER_VICIADO1, Character.EXPRESSION_EXCLAMATION );
													 
													 setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_EXCLAMATION );
													 setCharacterExpression( CHARACTER_DELEGADO, Character.EXPRESSION_EXCLAMATION );
													 setCharacterTileByPortal(CHARACTER_GUARDA, PORTAL_GUARDA_1);
													 
													 setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_EXCLAMATION );
													 
													 fadeIn();
													 lockInput( false );
												 },
												 "Não", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Tudo bem, você já fez muita coisa. Resolveremos este problema por nossa conta.", CHARACTER_DELEGADO );
													 showMessage( "Não sei quando teremos tempo, pois nossa prioridade é o Patrício e seu ajudante. Mas daremos um jeito.", CHARACTER_GUARDA );

													 // Perdeu a missão da ONG, playboy!!!
													 queueNextPart( PARTE_FINAL, Constants.TILEMAP_DONT_CHANGE );
												 });
								}
							}
							else
							{
								showMessage( "Não se esqueça de nos avisar se ver o fugitivo.", CHARACTER_DELEGADO );
								lockInput( false );
							} 
							break;
						
						case CHARACTER_VELHINHA:
							if( getQuestStatus( QUEST_VELHARUA ) == QuestState.QUEST_STATE_COMPLETE )
							{
								showMessage( "Como está, jovem?" );
								if( getQuestStatus( QUEST_ONG ) == QuestState.QUEST_STATE_INCOMPLETE )
								{
									showMessage( "Entre, vamos conversar...", CHARACTER_VELHINHA, 
									"Sim", function() : void 
									{
										wait( 0.1 );
										lockInput( true );
										setBackground( Constants.BACKGROUND_CASA_VELHA );
										showMessage( "Então você quer ajudar aqueles meninos que matam aula lá no beco?", CHARACTER_VELHINHA );
										showMessage( "Hum... Eu também estou preocupada com eles.\n E você precisa de um jeito de mandá-los para aquela ONG, certo?", CHARACTER_VELHINHA );
										showMessage( "Bom, o Junior é filho da Dona Zefa, da venda. Ele sempre foi muito obediente, e se está fazendo algo assim é porque a Dona Zefa ainda não ficou sabendo.", CHARACTER_VELHINHA);
										showMessage( "Fale com ela, e aposto que ela pode convencê-lo.", CHARACTER_VELHINHA);
										showMessage( "Piteco e Silas são irmãos do Juca, o frentista do posto. O Juca trabalha muito para ajudar a família. Talvez Juca possa convencê-los a fazer o mesmo.", CHARACTER_VELHINHA);
										showMessage( "O Beto é um caso um pouco mais complicado. Ele tem uma família difícil. Os pais nunca cuidaram muito dele.", CHARACTER_VELHINHA);
										showMessage( "A última coisa que ele quer é ficar em casa. Talvez se ele encontrasse algo mais construtivo para fazer... Ouvi dizer que ele gosta muito de música. A única pessoa que trabalha com música por aqui é o Lamar.", CHARACTER_VELHINHA);
										showMessage( "Já o Dan é o mais novo de todos. Ele sempre tentou fazer o que os outros faziam.", CHARACTER_VELHINHA);
										showMessage( "Acho que se você convencer todos a ir, o Dan também irá. Ele costuma ficar perto do Beco, procure lá!", CHARACTER_VELHINHA);

										if (vars.juniorquest == 0 ){
											vars.juniorquest = 1 ;
											setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_EXCLAMATION );
										}
										if(vars.pitecosilasquest == 0){
											vars.pitecosilasquest = 1 ;
										}
										if(vars.betoquest == 0 ){
											setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_EXCLAMATION );
											vars.betoquest = 1;
										}
										if(vars.danquest == 0){
											vars.danquest = 1;
											setCharacterExpression( CHARACTER_VICIADO1, Character.EXPRESSION_EXCLAMATION );
										}
										setBackground(Constants.BACKGROUND_MAP);
										lockInput( false );
									},
									"Talvez uma outra hora", function() : void
									{
										wait( 0.1 );
										showMessage( "Até mais, então!", CHARACTER_VELHINHA );
									} );
								}
								else
								{
									showMessage( "Eu também estou ótima! Tive uma ideia no outro dia. Estou pensando em entrar em uma faculdade. Mas ainda não resolvi que área cursar..." );
								}
							}
							else
							{
								showMessage( "Bom dia." , CHARACTER_VELHINHA );
							}
							break;
						
						case CHARACTER_HIPPIE:
							// TODO
//							if( getQuestStatus( QUEST_HIPPIE ) != QuestState.QUEST_STATE_COMPLETE )
//							{
//								showMessage( "Olá. Está procurando algum CD?", CHARACTER_HIPPIE );
//								showMessage( "Aproveite que todos estão em promoção. Não consegui regularizar o meu trabalho, então estou tendo que liquidar o estoque.", CHARACTER_HIPPIE );
//								showMessage( "Com o que eu vou trabalhar? Não faço idéia... Mas tenho que arrumar alguma coisa logo, senão não vou conseguir pagar minhas contas. Talvez eu mude de cidade...", CHARACTER_HIPPIE );
//							}
//							else
							{
								showMessage("Olha quem está aqui! Tudo bem? Quer conhecer a loja nova?", CHARACTER_HIPPIE, 
											"Sim! É claro!", function() : void
											{
												wait( 0.1 );
												lockInput( true );
												setBackground( Constants.BACKGROUND_LOJACDS );
												if( getQuestStatus( QUEST_ONG ) == QuestState.QUEST_STATE_INCOMPLETE && vars.betoquest <= 1 )
												{
													setCharacterExpression( CHARACTER_HIPPIE, Character.EXPRESSION_NONE );
													
													showMessage( "O quê? O Beto não está indo às aulas? É uma pena... Sem estudo fica difícil de conseguir algo na vida.", CHARACTER_HIPPIE);
													showMessage( "An? Ele tinha uma banda? E entende bastante de música? Hum... Talvez se eu conversasse com ele...", CHARACTER_HIPPIE );
													setCharacterTileByPortal( CHARACTER_HIPPIE, PORTAL_SAVEGANG2);
													setCharacterDirection( CHARACTER_HIPPIE, Direction.DIRECTION_LEFT );
													setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_CHARGANG2);
													setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_LEFT );
													setCharacterDirection( CHARACTER_VICIADO2, Direction.DIRECTION_RIGHT );
													setBackground( Constants.BACKGROUND_MAP );
													showMessage( "E aí, Beto!", CHARACTER_HIPPIE );
													showMessage( "Bom dia, Sr. Lamar...", CHARACTER_VICIADO2 );
													showMessage( "Queria conversar com você.", CHARACTER_HIPPIE );
													showMessage( "Pode falar...", CHARACTER_VICIADO2 );
													showMessage( "Preciso de um ajudante que entenda de música.\nVocê está interessado?",CHARACTER_HIPPIE);
													showMessage( "Nossa! É... É sério!?", CHARACTER_VICIADO2 );
													showMessage( "Claro que é! E então, o que me diz? Topa ou não topa?", CHARACTER_HIPPIE );
													showMessage( "Que legal! Claro que topo! Ia ser ótimo!", CHARACTER_VICIADO2 );
													showMessage( "Mas tem uma coisa... Fiquei sabendo que você anda matando as aulas da escola.", CHARACTER_HIPPIE );
													showMessage( "...................", CHARACTER_VICIADO2 );
													showMessage( "Quero propor um acordo:\nO emprego é seu, se você prometer voltar às aulas.", CHARACTER_HIPPIE );
													showMessage( "...................", CHARACTER_VICIADO2 );
													showMessage( "E tem mais. A ONG está com um projeto para novos músicos. Vou pagar a mensalidade para você, como parte de seu salário. Mas eles também exigem que você esteja estudando e seja um bom aluno.", CHARACTER_HIPPIE );
													showMessage( "Realizar o seu sonho de ser músico só vai depender de você. E então? Ainda interessado?", CHARACTER_HIPPIE );
													wait(3);
													showMessage( "Cara... Senhor Lamar... Não tenho como agradecer! É claro que aceito! Vou seguir o seu exemplo e trabalhar bastante! Vou voltar a estudar e serei o melhor músico da Vila Virtual! Vocês vão ver! Posso começar hoje mesmo?", CHARACTER_VICIADO2 );
													showMessage( "É assim que eu gosto, garoto. Pode começar hoje sim. Vamos lá que temos muito trabalho.", CHARACTER_HIPPIE );
													fadeOut();
													removeCharacter(CHARACTER_VICIADO2);
													setCharacterTileByPortal(CHARACTER_HIPPIE, PORTAL_MUSICA1);
													setCharacterDirection(CHARACTER_HIPPIE, Direction.DIRECTION_DOWN);
													vars.betoquest = 2 ;
													vars.contadorong ++;
													fadeIn();
													lockInput(false);
												}
												else
												{
													showMessage( "E aí, gostou da nova loja? Agora temos não só um ambiente melhor, como um repertório muito mais variado! Aproveite para dar uma olhada, você é sempre bem vindo!", CHARACTER_HIPPIE );	
												}
											},
											"Vou deixar para outra hora, Lamar", function() : void
											{
												wait( 0.1 );
												showMessage( "Volte quando quiser! Você é sempre bem vindo!", CHARACTER_HIPPIE );
											});
							}
							break;
								
						case CHARACTER_FRENTISTA:
							if( getQuestStatus( QUEST_ONG ) == QuestState.QUEST_STATE_INCOMPLETE )
							{
								if( vars.pitecosilasquest <= 1 )
								{
									showMessage( "O quê? Você está dizendo que meus irmãos estão matando aula?", CHARACTER_FRENTISTA );
									showMessage( "Hum... Eu já suspeitava... Sempre que perguntava como estavam indo no colégio, eles mudavam de assunto. Isso vai deixar nossa mãe bastante chateada...", CHARACTER_FRENTISTA);
									showMessage( "Antes de falar com ela, vou conversar com eles. Você pode vir junto? Estão falando muito bem de você na cidade, então sua presença pode ser um incentivo.", CHARACTER_FRENTISTA, 
												 "OK, vamos lá", function() : void
												 {
													 wait( 0.1 );
													 setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_NONE );
													 
													 // Bom garoto!
													 vars.contadorong++;
													 vars.contadorong++;
													 vars.pitecosilasquest = 2;
													 
													 lockInput(true);
													 fadeOut();
													 setCharacterTileByPortal(CHARACTER_FRENTISTA, PORTAL_SAVEGANG5);
													 setCharacterDirection(CHARACTER_FRENTISTA, Direction.DIRECTION_UP);
													 setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_CHARGANG5);
													 setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_UP);
													 setCharacterDirection(CHARACTER_VICIADO4, Direction.DIRECTION_DOWN);
													 setCharacterDirection(CHARACTER_VICIADO5, Direction.DIRECTION_DOWN);
													 fadeIn();
													 showMessage( "Piteco e Silas, é verdade o que me contaram?\nVocês estão matando todas as aulas, né?", CHARACTER_FRENTISTA );
													 showMessage( "O que vocês acham que a mãe vai pensar? Ela já está muito velha para isso! Vocês deveriam ser mais responsáveis!", CHARACTER_FRENTISTA);
													 showMessage( "Sabe... Temos que agir como uma família! Não estamos passando por bons bocados... Vocês não se sentem mal por não estar ajudando?", CHARACTER_FRENTISTA );
													 showMessage( "Mas o que a gente pode fazer, Juca?", CHARACTER_VICIADO4 );
													 showMessage( "É! Ninguém quer aceitar a gente em trabalho nenhum!", CHARACTER_VICIADO5 );
													 showMessage( "É claro que não! Como vocês esperam arrumar alguma coisa sem estudo? A escola abre vários caminhos! Se ficarem no beco, vendo o tempo passar, esperando uma boa oportunidade cair do céu, não vão conseguir nada mesmo! Vocês têm que se esforçar, correr atrás!", CHARACTER_FRENTISTA);
													 showMessage( "Se vocês não começarem a mudar a atitude de vocês agora, o futuro vai ficar difícil!", CHARACTER_FRENTISTA );
													 showMessage( "Então o que a gente pode fazer?", CHARACTER_VICIADO4 );
													 showMessage( "Vocês podem começar voltando às aulas. A ONG está procurando jovens instrutores de skate e, até onde eu sei, vocês são os melhores nisso em toda a Vila Virtual.", CHARACTER_FRENTISTA );
													 showMessage( "Só que para eles aceitarem vocês, teremos que apresentar boletins com boas notas. Será uma função que vocês irão adorar, mas terão que ir bem na escola! Quem sabe vocês não começam a competir também? Mas esse sonho só depende de vocês...", CHARACTER_FRENTISTA );
													 showMessage( "Nossa Juca, eu nunca tinha pensado nisso... Eu adoro ensinar os outros a andar de skate...", CHARACTER_VICIADO4 );
													 showMessage( "E sempre foi meu sonho competir! Ser melhor que o melhor do mundo!", CHARACTER_VICIADO5 );
													 showMessage( "Nossa, até senti vontade de voltar a estudar.", CHARACTER_VICIADO5 );
													 showMessage( "Irmão, não vou deixar você desistir! Podemos nos ajudar, um ao outro.", CHARACTER_VICIADO4 );
													 showMessage( "Muito bem! É assim que uma família funciona! Todos se ajudando.", CHARACTER_FRENTISTA );
													 showMessage( "Muito obrigado! Você tem servido de exemplo para muitos na Vila Virtual. Inclusive para mim. Você me deu exemplo e me motivou a resolver esse problema com os meus irmãos.", CHARACTER_FRENTISTA );
													 fadeOut();
													 setCharacterTileByPortal( CHARACTER_FRENTISTA, PORTAL_POSTO1 );
													 removeCharacter(CHARACTER_VICIADO4);
													 removeCharacter(CHARACTER_VICIADO5);
													 fadeIn();
													 lockInput(false);
												 },
												 "Não gosto de me meter em problemas de família", function() : void
												 {
													 setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_NONE );
													 wait( 0.1 );
													 
													 // Perdeu playboy
													 vars.pitecosilasquest = 2;
													 
													 showMessage( "Imaginei que você fosse responder isso...", CHARACTER_FRENTISTA );
													 showMessage( "Não sei se vou conseguir resolver esse problema sozinho, mas vou tentar", CHARACTER_FRENTISTA );
												 },
												 "Tá doido! Fui!", function() : void
												 {
													 setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_NONE );
													 wait( 0.1 );
													 
													 // Perdeu playboy
													 vars.pitecosilasquest = 2;
													 
													 showMessage( "Pois bem, esperava outra resposta...", CHARACTER_FRENTISTA );
													 showMessage( "Mas não posso exigir nada...", CHARACTER_FRENTISTA );
												 });
								}
								else// if( vars.pitecosilasquest == 2 )
								{
									showMessage( "É uma pena que eles tenham ficado tanto tempo sem ir às aulas... Mas agora eles tirarão o atraso.", CHARACTER_FRENTISTA);
									showMessage( "Obrigado por ter me contado.", CHARACTER_FRENTISTA );
								}
							}
							else
							{
								if( getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_COMPLETE )
									showMessage ("Muitos viajantes agora param para lanchar no estabelecimento do Sr. Wong! Isto tem ajudado o posto também.", CHARACTER_FRENTISTA);
								else
									showMessage("A cada dia a situação melhora um pouco.", CHARACTER_FRENTISTA);
							}
							break;
						
						case CHARACTER_VENDEDORA:
							if( getQuestStatus( QUEST_ONG ) != QuestState.QUEST_STATE_UNDISCOVERED &&  vars.juniorquest >= 0 )
							{
								if( vars.juniorquest <= 1 )
								{
									showMessage( "Olá! Como você está? Precisa de alguma coisa?", CHARACTER_VENDEDORA,
												 "Preciso conversar sobre o seu filho", function() : void
												 {
													 setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_NONE );
													 vars.contadorong++;
													 
													 wait( 0.1 );
													 lockInput( true );
													 showMessage( "Sobre o meu filho? Bom, vamos conversar em particular então.", CHARACTER_VENDEDORA);
													 setBackground( Constants.BACKGROUND_VENDA );
													 showMessage( "O QUÊ?!?!?!?!?!", CHARACTER_VENDEDORA );
													 showMessage( "Ele está matando aula?!", CHARACTER_VENDEDORA );
													 showMessage( "Isso não pode ficar assim! Acho que me descuidei! Vou falar com ele agora mesmo!", CHARACTER_VENDEDORA );
													 setCharacterTileByPortal(CHARACTER_VENDEDORA, PORTAL_SAVEGANG3);
													 setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_CHARGANG3);
													 setCharacterDirection(CHARACTER_VENDEDORA, Direction.DIRECTION_DOWN);
													 setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT);
													 setCharacterDirection(CHARACTER_VICIADO3, Direction.DIRECTION_UP);
													 setBackground(Constants.BACKGROUND_MAP);
													 showMessage( "Filho, é verdade que tem matado aula durante todo esse tempo?", CHARACTER_VENDEDORA );
													 showMessage( "Mãe, eu...", CHARACTER_VICIADO3 );
													 showMessage( "Nada de 'Mãe eu...'. Você sabe que eu só consegui o que consegui porque batalhei bastante. Hoje em dia, graças a Deus, você pode estar na escola ao invés de ter que ficar trabalhando, como aconteceu comigo.", CHARACTER_VENDEDORA );
													 showMessage( "Eu sabia que aqueles seus amigos eram má influência! Você tem que pensar por você mesmo, e não agir pelas opiniões dos outros!", CHARACTER_VENDEDORA );
													 showMessage( "Mas eles não têm culpa! Eu...", CHARACTER_VICIADO3 );
													 showMessage( "Você...?", CHARACTER_VENDEDORA );
													 showMessage( "Eu fico passando o tempo aqui com eles até eu poder ir jogar futebol! Os jogos no campo começam no meio da manhã!", CHARACTER_VICIADO3 );
													 showMessage( "Mas será que você não pode estudar e jogar futebol?", CHARACTER_VENDEDORA );
													 showMessage( "Eu até gosto de estudar... Mas gosto muito mais de futebol!", CHARACTER_VICIADO3 );
													 showMessage( "Ah, meu filho... Eu sei que seu sonho é ser jogador profissional. Mas quando você for um jogador famoso, como vai ser se você não tiver educação? Vai deixar que as pessoas te enganem nos contratos com os clubes? Vai se deixar influenciar pelos seus amigos jogadores?", CHARACTER_VENDEDORA );
													 showMessage( "E se quando você for mais velho, seu sonho mudar? E se você quiser virar um advogado, um médico, engenheiro? Sem estudo você não conseguirá nada disso...", CHARACTER_VENDEDORA );
													 showMessage( ".....................", CHARACTER_VICIADO3 );
													 showMessage( "Eu... Eu nunca tinha pensado nessas coisas... Você está certa mãe... Como sempre...", CHARACTER_VICIADO3 );
													 showMessage( "Mas a única outra coisa que gosto de fazer além de jogar futebol e estudar é de mexer em computadores.", CHARACTER_VICIADO3 );
													 showMessage( "Se não me engano, tem um curso de informática na ONG. Você gostaria de começar? Mas terá que ser um bom aluno para se manter no curso.", CHARACTER_VENDEDORA );
													 showMessage( "Claro! Quero sim! Mas não quero largar o futebol...", CHARACTER_VICIADO3 );
													 showMessage( "Não tem problema, filho. Você pode fazer os dois. É só se organizar.", CHARACTER_VENDEDORA );
													 vars.juniorquest = 2;
													 fadeOut();
													 removeCharacter( CHARACTER_VICIADO3 );
													 setCharacterTileByPortal( CHARACTER_VENDEDORA, PORTAL_VENDA4 );
													 fadeIn();
													 lockInput( false );
												 },
												 "Só passei para dar um \"oi\"", function() : void
												 {
													 wait( 0.1 );
													 showMessage( "Oh! Que ótimo! Venha quando quiser!", CHARACTER_VENDEDORA );
												 });
								}
								else if( vars.juniorquest == 2)
								{
									showMessage( "Agora estou menos preocupada. Imagine só! Meu filho matando aula! Mas confio nele. Se ele disse que vai mudar, é porque vai mudar.\nObrigada por ter me alertado!", CHARACTER_VENDEDORA );
								}
							}
							else
							{
								showMessage( "O movimento na cidade está aumentando. Tomara que continue assim! É ótimo para os negócios!", CHARACTER_VENDEDORA );
							}
							break;
						
						case CHARACTER_CRIANCA5:
							showMessage( "Adoro a biblioteca! Lá tem livros sobre muitas coisas! Mas tipo, muuuuitas coisas mesmo!", CHARACTER_CRIANCA5 );
							showMessage( "Livros sobre história, livros sobre bichos, livros sobre livros, e até livros para cegos! Eles são cheios de bolinhas. Mas como será que eles lêem essas coisas?", CHARACTER_CRIANCA5 );
							
							if( getQuestStatus(QUEST_CEGOBRAILE) == QuestState.QUEST_STATE_INCOMPLETE && hasItem( ITEM_LIVROBRAILE ) == false )
							{
								showMessage( "Meu amigo falou que tem um livro que explica como funciona. Tenho curiosidade em saber sobre isso.", CHARACTER_CRIANCA5,
											 "Opa! Eu quero esse livro!", function() : void
											 {
												 wait( 0.1 );
												 showMessage( "Quer, é? Posso pegar para você. Mas você tem que prometer que vai me devolver!", CHARACTER_CRIANCA5,
												 			  "Eu devolvo, pode deixar...", function() : void
															  {
																  wait( 0.1 );
																  showMessage( "Não sei não... Não senti firmeza na sua promessa!", CHARACTER_CRIANCA5 );  
															  },
												 			  "Como se eu não cumprisse com o prometido!", function() : void
															  {
																  setCharacterExpression( CHARACTER_CRIANCA5, Character.EXPRESSION_NONE );
																  wait( 0.1 );
																  lockInput( true );
																  showMessage( "Ah, então tá! Calma aí!", CHARACTER_CRIANCA5 );
																  fadeOut();
																  wait(1.0);
																  fadeIn();
																  showMessage( "Aqui! Está meio velho, mas acho que serve, né? Só não esqueça de me devolver!", CHARACTER_CRIANCA5);
																  showMessage( "O quê? É para o Tio Wong? Sei... Já mudou tudo, né? Mas se você prometeu, eu acredito. Não esqueça de pedir para ele me devolver, então!", CHARACTER_CRIANCA5 );
																  addItem( ITEM_LIVROBRAILE );
																  setCharacterExpression( CHARACTER_CHINA, Character.EXPRESSION_EXCLAMATION );
																  lockInput( false );
															  },
												 			  "Ih... jurar é complicado...", function() : void
															  {
																  wait( 0.1 );
																  showMessage( "Então eu não pego o livro para você!", CHARACTER_CRIANCA5 );
															  });
											 },
											 "Ê criança danada de curiosa! Continue assim, pois ler é muito bom!", function() : void
											 {
												 wait( 0.1 );
												 showMessage( "Você percebe que a criança ficou feliz com o elogio!" );
											 });
							}
							else if(getQuestStatus(QUEST_CEGOBRAILE) == QuestState.QUEST_STATE_COMPLETE )
							{
								showMessage( "Está gostando do livro? Ele ensina tudo mesmo? Não se esquece de me devolver, hein?", CHARACTER_CRIANCA5 );
							}
							break;
						
						case CHARACTER_CADEIRANTE:
							showMessage( "Quer dizer que o cara que ajudou o Patrício está andando pela vila? Com certeza avisarei ao delegado se encontrar com ele.", CHARACTER_CADEIRANTE );
							break;
						
						case CHARACTER_IRMAO_LARISSA:
							showMessage( "Adoro jogar futebol!", CHARACTER_IRMAO_LARISSA );
							break;
						
						case CHARACTER_VICIADO1: // Dan
							if( vars.contadorong >= 4 || ( vars.contadorong == 3 && getQuestStatus( QUEST_HIPPIE ) != QuestState.QUEST_STATE_COMPLETE ) )
							{
								vars.contadorong++;

								lockInput( true );
								showMessage( "Ei! Você por acaso viu o... Ahn?", CHARACTER_VICIADO1 );
								showMessage( "Eles todos voltaram a estudar e estão fazendo cursos na ONG?", CHARACTER_VICIADO1 );
								showMessage( "Acho que vou atrás deles...",CHARACTER_VICIADO1 );
								fadeOut();
								removeCharacter( CHARACTER_VICIADO1 );
								fadeIn();
								lockInput( false );
							}
							else
							{
								showMessage( "Ei! Você por acaso você viu o Beto?", CHARACTER_VICIADO1 );
							}
							break;
						
						case CHARACTER_VICIADO2:
							//BETO
							if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE )
								showMessage( "A loja do Lamar tem muito mais coisa agora.", CHARACTER_VICIADO2 );
							else
								showMessage( "Droga... O Lamar teve que parar de vender CDs.", CHARACTER_VICIADO2 );
							break;
						
						case CHARACTER_VICIADO3:
							//JUNIOR
							showMessage( "Só não conte para minha mãe que estou aqui.",CHARACTER_VICIADO3);
							break;
						
						case CHARACTER_VICIADO4:
							// SILAS
							showMessage( "Eu e meu irmão somos os melhores da cidade no skate!", CHARACTER_VICIADO4);
							break;
						
						case CHARACTER_VICIADO5:
							//PITECO
							showMessage( "Acho que vou andar de skate...",CHARACTER_VICIADO5);
							break;
						
					}
					break;
			}

			////////////////////////////////////////////////////////
			// XIMENES END                                        //
			////////////////////////////////////////////////////////
			// se não houve mudanças nos eventos, mostra mensagem genérica do personagem
			if( previousQueuedEvents == _gameEvents.length )
			{
				if ( sayGenericSpeech ) {
					try {
						var characterSpeeches : Object = _genericSpeeches[ characterId ];
						if( characterSpeeches && characterSpeeches.speeches )
						{
							showMessage( characterSpeeches.speeches[ characterSpeeches.currentSpeech ], characterId );
							nextGameEvent();
							
							characterSpeeches.currentSpeech = ( characterSpeeches.currentSpeech + 1 ) % characterSpeeches.speeches.length
						} 
					} catch ( e : Error ) {
						trace( e );
					}
				}
			} else {
				nextGameEvent();
			}
		} // fim do método onTalk()

		private function unloadSounds() : void
		{
			queueGameEvent( SoundManager.GetInstance(), SoundManager.GetInstance().unloadAllSounds, true );
		}
		
		private function reloadSounds() : void
		{
			queueGameEvent( instance, doReloadSounds, true );
		}

		private function playLamarSound( soundIndex : int ) : void
		{
			queueGameEvent( instance, doPlayLamarSound, true, soundIndex );
		}
		
		private function doReloadSounds() : void
		{
			SoundManager.GetInstance().unloadAllSounds();
			VilaVirtual.GetInstance().loadSounds();
			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_MUSIC_THEME, 0.0, SoundManager.INFINITY_LOOP );
		}
		
		private function doPlayLamarSound( soundIndex : int ) : void
		{
			var c : Class;
			switch( soundIndex )
			{
				case SOUND_LAMAR_GOTAS:
					c = Class( getDefinitionByName( "SoundGotas" ) );
					break;
				
				case SOUND_LAMAR_ENIGMA:
					c = Class( getDefinitionByName( "SoundJimmy" ) );
					break;
				
				case SOUND_LAMAR_BOUNDS:
					c = Class( getDefinitionByName( "SoundFarmer" ) );
					break;
			}
			SoundManager.GetInstance().playSoundAtIndex( SoundManager.GetInstance().pushSound( new c() as Sound ) );
		}
		
		/**
		* Rotaciona a cena do jogo em torno de seu centro 
		* @param degrees Determina a rotação da cena em graus
		*/		
		private function rotateScene( degrees : Number ) : void
		{			
			// Porco!!!!!!!!!! Deveríamos ter um método getBkg em VilaVirtual!!!!
			var BKG_INDEX : int = 0;
			var scene : DisplayObject = getGameScene().getChildAt( BKG_INDEX );
			
			// Zera a transformação atual
			var m : Matrix = scene.transform.matrix;
			m.identity();
			scene.transform.matrix = m;

			// Obtém as coordenadas do centro da cena no movieclip pai
			var point : Point = new Point( scene.x + ( scene.width * 0.5 ), scene.y + ( scene.height * 0.5 ));
			
			// Faz a rotação em torno do centro da cena
			var m3 : Matrix = scene.transform.matrix;
			m3.tx -= point.x;
			m3.ty -= point.y;
			m3.rotate( degrees * ( Math.PI / 180 ) );
			m3.tx += point.x;
			m3.ty += point.y;
			scene.transform.matrix = m3;
		}
		
		/**
		* Cria a sequência de eventos da loja de pastel 
		*/		
		private function showChinaMenu() : void
		{
			wait( 0.1 );
			lockInput( true );
			showMessage( "Então, o que você vai querer?", CHARACTER_CHINA,
				"Pastel Mágico", function() : void
				{
					wait( 0.1 );
					showMessage( "Saindo!", CHARACTER_CHINA );
					showMessage( "..................." );
					
					var steps : Number = 20;
					var singleStep : Number = 360 / steps;
					for( var stepCounter : int = 0 ; stepCounter < steps ; ++stepCounter )
					{
						queueGameEvent( instance, rotateScene, true, ( stepCounter + 1 ) * singleStep );
						wait( 0.05 );
					}
					queueGameEvent( instance, rotateScene, true, 0 );
					
					showMessage( "WOW!!!!!!!!!!!!" );
					showMessage( "Volte sempre!", CHARACTER_CHINA );
					setBackground( Constants.BACKGROUND_MAP );
					lockInput( false );
				},
				"Pastel da Sorte", function() : void
				{
					wait( 0.1 );
					showMessage( "Saindo!", CHARACTER_CHINA );
					showMessage( "Sua sorte é:" );
					showChineseMessage( getLucky() ); 
					showMessage( "..................." );
					showMessage( "Infelizmente o texto está em chinês." );
					showMessage( "Volte sempre!", CHARACTER_CHINA );
					setBackground( Constants.BACKGROUND_MAP );
					lockInput( false );
				},
				"Mega Wong", function() : void
				{
					wait( 0.1 );
					showMessage( "Saindo!", CHARACTER_CHINA );
					
					var nBlinks : int = 6;
					for( var blink : int = 0 ; blink < nBlinks ; ++blink )
					{
						fadeOut( 0.1 );
						fadeIn( 0.1 );
					}
					showMessage( "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" );
					showMessage( "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" );
					showMessage( "F-A-N-T-Á-S-T-I-C-O!!!!!!!!!!!!!!" );
					showMessage( "Agora você se sente mais feliz!" );
					showMessage( "Volte sempre!", CHARACTER_CHINA );
					setBackground( Constants.BACKGROUND_MAP );
					lockInput( false );
				},
				"Nenhum", function() : void
				{
					wait( 0.1 );
					showMessage( "Volte sempre!", CHARACTER_CHINA );
					setBackground( Constants.BACKGROUND_MAP );
					lockInput( false );
				});
		}
		
		/**
		* Devolve a sorte contida no pastel da sorte do Sr. Wong. as traduções foram feitas utilizando o Google Translate 
		* @return A sorte contida no pastel da sorte do Sr. Wong
		*/		
		private function getLucky() : String
		{
			var sayings : Vector.< String > = new Vector.<String>();
			
			// Jogos educativos são uma ótima maneira de ensinar
			sayings.push( "教育游戏是一个伟大的方式来教" );
			
			// Se você quer trabalhar com jogos eletrônicos, mande o seu currículo para a Nano Games
			sayings.push( "如果你想使用電子遊戲，請發送簡歷到納米運動會" );
			
			// Já diria Peppy no Star Fox 64: "Never give up, trust your instincts". Se ele fosse chinês, diria:
			sayings.push( "永遠不要放棄，相信自己的直覺" );
			
			// Biscoitinho do china in box: "Não tenha medo de ir devagar, tenha medo de não fazer nada"
			sayings.push( "不要害怕去慢慢地，怕什麼都不做" );
			
			// Parabéns, você acaba de ganhar um vale macarrão para o ano de 2200!!!
			sayings.push( "恭喜你，你剛剛贏得了一個山谷麵條的2200年！" );

			// Pela documentação, o random de Flash devolve o número no intervalo 0 <= n < 1. Ou seja, nunca
			// retorna 1. Por isso utilizamos sayings.length, e não sayings.length - 1
			var random : int = Math.floor( Math.random() * sayings.length );
			
			// Só para o caso de a documentação estar errada
			if( random >= sayings.length )
				random = sayings.length - 1;
			
			return sayings[ random ];
		}
		
		private function startMinigameRecycling( firstTime : Boolean ) : void
		{
			wait( 0.1 );
			showMessage("Você quer que eu te ensine a separar o lixo da forma certa?",CHARACTER_GARI,
						"Sim", function() : void
						{
							wait( 0.1 );
							wait( 0.1 );
							
							if( firstTime )
							{
								setCharacterExpression( CHARACTER_GARI, Character.EXPRESSION_NONE );
								startQuest( QUEST_COLETA );
								wait( 0.1 );
							}
							
							showMessage( "Então vamos lá! Você terá que completar 7 fases! Não se preocupe, elas são rápidas!", CHARACTER_GARI );
							wait( 0.1 );
							scheduleMiniGame( Constants.MINIGAME_COLETA );
						},
						"Não", function() : void
						{
							wait( 0.1 );
							showMessage("Tem certeza? Tudo bem então. Quando quiser, basta falar comigo.", CHARACTER_GARI);
						} );	
		}
		
		private function delegadoGo(): void {
			fadeOut();
			if ( vars.traficanteTile == 1 ){
				setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_DELEGADO1);
				setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_LEFT);
				setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_P3PLAYER1);
				setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_LEFT);
			} else if ( vars.traficanteTile == 2 ) {
				setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_DELEGADO2);
				setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_UP);
				setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_P3PLAYER2);
				setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_UP);
			} else if ( vars.traficanteTile == 3 ) {
				setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_DELEGADO3);
				setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_UP);
				setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_P3PLAYER3);
				setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_UP);
			}
			fadeIn();
		}
		
		private function traficanteGo( doFadeOut : Boolean = true, doFadeIn : Boolean = true ): void
		{
			if( doFadeOut )
				fadeOut();
			
			if ( vars.traficanteTile == 3 ){
				vars.traficanteTile = 1;
				setCharacterTileByPortal(CHARACTER_TRAFICANTE, PORTAL_TRAFICANTE1);
			} else if ( vars.traficanteTile == 1 ) {
				vars.traficanteTile = 2;
				setCharacterTileByPortal(CHARACTER_TRAFICANTE, PORTAL_TRAFICANTE2);
			} else if ( vars.traficanteTile == 2 ) {
				vars.traficanteTile = 3;
				setCharacterTileByPortal(CHARACTER_TRAFICANTE, PORTAL_TRAFICANTE3);
			}
			
			if( doFadeIn )
				fadeIn();
		}
		
		private function checkPart2( idTalker : int ) : void
		{
			if( vars.part2quests == 3 )
			{
				showMessage( "Você costuma ajudar as pessoas não é? Talvez você possa ajudar o Juca então!", idTalker );
				showMessage( "Na verdade eu não sei o que é, mas ele me parece bastante preocupado ultimamente. Fale com ele.", idTalker );
				
				setCharacterExpression( CHARACTER_FRENTISTA, Character.EXPRESSION_EXCLAMATION );
				setCharacterTileByPortal(CHARACTER_POLITICO, PORTAL_P2POLITICO);
				setCharacterDirection(CHARACTER_POLITICO, Direction.DIRECTION_UP );
				setCharacterTileByPortal(CHARACTER_TRAFICANTE, PORTAL_P2GANGSTER);
				setCharacterDirection(CHARACTER_TRAFICANTE, Direction.DIRECTION_DOWN );
			}
		}
		
		private function getMutirao() : void {
			if(getQuestStatus(QUEST_ENTULHO) == QuestState.QUEST_STATE_INCOMPLETE){
				if (vars.mutirao >= MUTIRAO_TOTAL )
				{
					lockInput(true);
					wait( 0.1 );
					fadeOut();
					
					setCharacterExpression( CHARACTER_GARI, Character.EXPRESSION_NONE );
					setCharacterExpression( CHARACTER_HOMEM4, Character.EXPRESSION_NONE );
					setCharacterExpression( CHARACTER_HOMEM9, Character.EXPRESSION_NONE );
					setCharacterExpression( CHARACTER_HOMEM11, Character.EXPRESSION_NONE );
					setCharacterExpression( CHARACTER_MULHER4, Character.EXPRESSION_NONE );
					setCharacterExpression( CHARACTER_MULHER5, Character.EXPRESSION_NONE );
					
					setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_ENTULHOQUEST);
					setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT);
					
					setCharacterTileByPortal( CHARACTER_MULHER5, PORTAL_BANCA2 );
					setCharacterDirection(CHARACTER_MULHER5, Direction.DIRECTION_RIGHT);
					
				//////////////////////////////////////////////[    ANIMAÇÃO DO FIM DA QUEST   ]////////////////////////////////////////
					setCharacterTileByPortal( CHARACTER_HOMEM4, PORTAL_MUTIRAO1 );
					setCharacterDirection(CHARACTER_HOMEM4, Direction.DIRECTION_RIGHT);
					
					setCharacterTileByPortal( CHARACTER_MULHER4, PORTAL_MUTIRAO2 );
					setCharacterDirection(CHARACTER_MULHER4, Direction.DIRECTION_DOWN);
					
					setCharacterTileByPortal( CHARACTER_HOMEM9, PORTAL_MUTIRAO3 );
					setCharacterDirection(CHARACTER_HOMEM9, Direction.DIRECTION_UP);
					
					setCharacterTileByPortal( CHARACTER_HOMEM11, PORTAL_MUTIRAO4 );
					setCharacterDirection(CHARACTER_HOMEM11, Direction.DIRECTION_LEFT);
					fadeIn()
					wait(0.3);
					fadeOut();
					wait( 0.1 );
					removeCharacter(CHARACTER_OBJ_ENTULHO5);
					removeCharacter(CHARACTER_OBJ_ENTULHO6);
					
					setCharacterTileByPortal(CHARACTER_HOMEM11, PORTAL_MUTIRAO5);
					setCharacterDirection(CHARACTER_HOMEM11, Direction.DIRECTION_UP);
					
					setCharacterTileByPortal(CHARACTER_MULHER5, PORTAL_MUTIRAO6);
					setCharacterDirection(CHARACTER_MULHER5, Direction.DIRECTION_RIGHT);
					
					setCharacterTileByPortal(CHARACTER_MULHER4, PORTAL_MUTIRAO7);
					setCharacterDirection(CHARACTER_MULHER4, Direction.DIRECTION_LEFT);
					
					setCharacterTileByPortal(CHARACTER_HOMEM4, PORTAL_MUTIRAO8)
					setCharacterDirection(CHARACTER_HOMEM4, Direction.DIRECTION_RIGHT);
					fadeIn();
					wait(0.3);
					fadeOut();
					wait( 0.1 );
					removeCharacter(CHARACTER_OBJ_ENTULHO1);
					removeCharacter(CHARACTER_OBJ_ENTULHO3);
					
					setCharacterTileByPortal(CHARACTER_HOMEM11, PORTAL_MUTIRAO9);
					setCharacterDirection(CHARACTER_HOMEM11, Direction.DIRECTION_RIGHT);
					
					setCharacterTileByPortal(CHARACTER_MULHER4, PORTAL_MUTIRAO11);
					setCharacterDirection(CHARACTER_MULHER4, Direction.DIRECTION_DOWN);
					
					setCharacterTileByPortal(CHARACTER_HOMEM4, PORTAL_MUTIRAO12);
					setCharacterDirection(CHARACTER_HOMEM4, Direction.DIRECTION_LEFT);
					
					setCharacterTileByPortal(CHARACTER_MULHER5, PORTAL_MUTIRAO6);
					setCharacterDirection(CHARACTER_MULHER5, Direction.DIRECTION_UP);
					
					setCharacterTileByPortal(CHARACTER_HOMEM9, PORTAL_MUTIRAO8);
					setCharacterDirection(CHARACTER_HOMEM9, Direction.DIRECTION_DOWN);
					fadeIn();
					wait(0.3);
					fadeOut();
					wait( 0.1 );
					removeCharacter(CHARACTER_OBJ_ENTULHO4);
					removeCharacter(CHARACTER_OBJ_ENTULHO2);			
					setCharacterTileByPortal(CHARACTER_HOMEM4, PORTAL_DELEGACIA1);
					setCharacterTileByPortal(CHARACTER_HOMEM9, PORTAL_BECO1);
					setCharacterTileByPortal(CHARACTER_HOMEM11, PORTAL_HIPPIE3);
					setCharacterTileByPortal(CHARACTER_MULHER4, PORTAL_RUA7);
					setCharacterTileByPortal(CHARACTER_MULHER5, PORTAL_BANCA2);					
					setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_LEFT );
					setCharacterDirection( CHARACTER_GARI, Direction.DIRECTION_RIGHT );
					fadeIn();
					wait( 0.1 );
					showMessage( "Você conseguiu juntar pessoas o suficiente para limpar a cidade! Que bom!\nMeu nariz agradece, e eu acho que o resto dos moradores também estão gratos!", CHARACTER_GARI );
					showMessage( "Me preocupa que boa parte do lixo foram sacolas de plástico de mercado. As sacolas de lixo pretas grandes são feitas para colocar lixo para fora e reduzir a quantidade de plástico.", CHARACTER_GARI );
					showMessage( "Plástico demora muito tempo para se degradar, e aumenta muito a quantidade de lixo no aterro, prejudicando a cidade.", CHARACTER_GARI );
					showMessage( "Essas sacolas de plástico demoram até 300 anos para se degradar!", CHARACTER_GARI );
					showMessage( "Essas sacolas provavelmente vêm da venda. Acha que pode fazer alguma coisa a respeito disso?\nQuem sabe a Dona Zefa não encontra uma solução.", CHARACTER_GARI);
					showMessage( "Bom, um problema de cada vez, certo? Obrigado novamente.", CHARACTER_GARI );
					endQuest(QUEST_ENTULHO);
					startQuest(QUEST_SACOLAS);
					setCharacterExpression( CHARACTER_VENDEDORA, Character.EXPRESSION_EXCLAMATION );
					vars.part1quests ++;
					checkPart1Quests();
					wait(0.3);
					lockInput(false);
				}
			}
		}
		
		
		private function setMapIconVisible( index : int, visible : Boolean = true ) : void {
			VilaVirtual.getMapScreen().setIconVisible( index, visible );
		}
		
		
		private function setIconBlinking( iconId : int, blink : Boolean ) : void {
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.setIconBlinking, true, iconId, blink );
		}
		
		
		private function scheduleMiniGame( miniGameIndex : uint ) : void
		{
			queueGameEvent( instance, showMiniGame, false, miniGameIndex );
		}
		
		
		private function showMiniGame( miniGameIndex : uint ) : void
		{
			var state : ApplicationState;
			switch ( miniGameIndex ) {
				case Constants.MINIGAME_COLETA:
					state = ApplicationState.APP_STATE_MINIGAME_RECYCLING_FROM_VILA;
				break;
				
				case Constants.MINIGAME_DENGUE:
					state = ApplicationState.APP_STATE_MINIGAME_DENGUE_FROM_VILA;
				break;
				
				default:
					trace( "ERRO: MINI GAME INDEX NÃO CONHECIDO: " + miniGameIndex );
					return;
			}
			VilaVirtual.GetInstance().showMiniGame( miniGameIndex );
			Application.GetInstance().setState( state );
		}
		
		/**
		* Chamada quando o jogador está saindo de um minigame
		* @param minigameAppState O estado da aplicação que representa o minigame jogado. Assim podemos saber a que minigame os outros parâmetros correspondem
		* @param gameCompleted <code>true</code> se o jogador terminou o minigame, <code>false</code> caso contrário
		* @param score O score feito pelo usuário ou -1 caso o usuário tenha abortado o minigame sem realizar uma partida
		*/	
		public function onMiniGameEnd( minigameAppState : ApplicationState, gameCompleted : Boolean, score : int ) : void
		{
			trace( ">>>>> Minigame status - state: " + minigameAppState.Name + "; completed: " + gameCompleted + "; score: " + score );
			
			// Garante que não teremos bugs por causa da área visível
			VilaVirtual.GetInstance().resetVisibleArea();

			// Volta a tocar a música do jogo
			playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );

			// Empilha os eventos do jogo de acordo com o resultado do jogo
			switch( minigameAppState )
			{
				case ApplicationState.APP_STATE_MINIGAME_RECYCLING_FROM_VILA:
					if( gameCompleted )
					{
						vars.part2quests++;
						
						wait( 0.1 );
						showMessage( "Nossa, você aprendeu rápido! Você fez " + score + " pontos! Uau! Parabéns! Não se esqueça de ensinar para quem quiser saber!\nQuem sabe a gente não consegue fazer com que a Vila Virtual comece a reciclar mais.", CHARACTER_GARI );
						endQuest( QUEST_COLETA );
						checkPart2( CHARACTER_GARI );
					}
					else
					{
						wait( 0.1 );
						showMessage("Poxa, que pena. Demora para aprender qual lixo jogar em qual lixeira, mas você se acostuma com o tempo.", CHARACTER_GARI);
						showMessage("Se eu fosse você, tentaria de novo. Quer tentar?", CHARACTER_GARI,
							"Sim", function() : void
							{
								wait( 0.1 );
								scheduleMiniGame( Constants.MINIGAME_COLETA );
							},
							"Não", function() : void
							{
								wait( 0.1 );
								showMessage( "É uma pena... Fale comigo se quiser tentar de novo.", CHARACTER_GARI );
							} );
					}
					break;

				case ApplicationState.APP_STATE_MINIGAME_DENGUE_FROM_VILA:
					if( gameCompleted )
					{
						vars.part1quests++;
						checkPart1Quests();
						
						wait( 0.1 );
						setCharacterExpression( CHARACTER_BOMBEIRO, Character.EXPRESSION_NONE );
						showMessage( "Para prevenir a dengue não basta matar os mosquitos. Você deve impedir que eles se reproduzam. Eles se multiplicam mais rápido do que você pode matar todos eles.", CHARACTER_BOMBEIRO );
						showMessage( "O mosquito da dengue coloca os ovos em água parada limpa, por isso evite acúmulos de água. Lugares onde a água da chuva pode ficar presa também devem ser cobertos, esvaziados diariamente ou, se possível, retirados.", CHARACTER_BOMBEIRO );
						showMessage( "Existem também larvicidas,  produtos que podem ser colocados na água para matar as larvas.", CHARACTER_BOMBEIRO );
						showMessage( "Obrigado pelo aviso, vocês dois. Lembrem-se que a melhor arma contra a dengue é a prevenção!", CHARACTER_BOMBEIRO );
						endQuest( QUEST_DENGUE );
					}
					else
					{
						wait( 0.1 );
						setCharacterExpression( CHARACTER_BOMBEIRO, Character.EXPRESSION_EXCLAMATION );
						showMessage( "Que pena, não conseguimos desta vez. É importante terminar com esse foco de dengue. Dessa forma ela não pode se espalhar para o resto da cidade.", CHARACTER_BOMBEIRO);
						showMessage( "Quer tentar de novo?",  CHARACTER_BOMBEIRO,
							"Sim", function() : void
							{
								wait( 0.1 );
								scheduleMiniGame( Constants.MINIGAME_DENGUE );
							},
							"Não", function() : void
							{
								wait( 0.1 );
								showMessage( "Me diga quando quiser tentar de novo. É muito importante!", CHARACTER_BOMBEIRO);
								vars.denguePlay = true;
							});
					}
					break;
				
				default:
					trace( ">>>>> onMiniGameEnd - Invalid application state: " + minigameAppState );
					return;
			}
		}
		
		
		private function queueNextPart( part : int, tileMapIndex : int = Constants.TILEMAP_ORIGINAL ) : void
		{
			queueGameEvent( this, prepareToPart, false, part, tileMapIndex );
		}
		
		
		/**
		 * Prepara as variáveis, personagens e outros elementos para uma parte do jogo.
		 */
		public function prepareToPart( part : int, tileMapIndex : int = Constants.TILEMAP_ORIGINAL, isContinuing : Boolean = false ) : void
		{
			trace( "\n------------------------------------\n" );
			trace( "GameScript::prepareToPart => Iniciando prepare to part " + part );
			trace( "\n------------------------------------\n" );

			// Retirar!!!!!!!!!!!! Somente para teste!!!
//			if ( part < PARTE_3 )
//				part = PARTE_3;
			//tileMapIndex = Constants.TILEMAP_AMBOS;
			
			if ( tileMapIndex != Constants.TILEMAP_DONT_CHANGE && !setTileMap( tileMapIndex, prepareToPart, part, tileMapIndex, isContinuing ) ) {
				return;
			}
			
			this._part = part;
			
			// Mata todos os eventos "residuais"
			_gameEvents.splice( 0, _gameEvents.length );
			
			if( nextGameEventScheduler != null )
				nextGameEventScheduler.invalidate();
			
			// por padrão, remove todos os personagens do cenário ao começar uma nova parte
			removeAllCharacters();
			
			switch( part )
			{
				case PARTE_1:
					// Falas didáticas
					setCharacterSpeeches( CHARACTER_WISE_1,
					[
						"Cidadania é o que torna alguém um cidadão. E para uma pessoa ser cidadã ela deve ter a garantia de uma série de direitos e cumprir seus deveres.",
						"Uma das obrigações da prefeitura é informar a população, com clareza, como gasta o dinheiro público.",
						"A Prefeitura deve prestar contas à Câmara de Vereadores, que fiscaliza como está sendo gasto o dinheiro público. E os cidadãos devem fiscalizar todos, prefeitura e câmara."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_2,
					[
						"O que garante os nossos direitos são as leis. A maior de todas elas é a Constituição Federal de 1988. Nela estão garantidos os direitos e registrados os deveres de todos os cidadãos brasileiros, inclusive os das crianças e dos adolescentes.",
						"Como todo o dinheiro vem do povo e deve ser gasto para o bem da população, é direito de cada cidadão participar e saber o que é feito com esse recurso."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_3,
					[
						"A Constituição Federal garantiu a criação de uma lei para cuidar especialmente dos direitos das crianças e dos adolescentes. Essa lei foi feita em 1990 e é chamada de Estatuto da Criança e do Adolescente (ECA).",
						"O ECA é uma segurança para que todas as crianças e os adolescentes possam crescer de forma livre, saudável e com respeito."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_4,
					[
						"Você sabia que existe um órgão do governo chamado Controladoria Geral da União (CGU)?",
						"A CGU fica de olho no trabalho dos administradores públicos para saber se o dinheiro do povo está sendo bem utilizado, prevenindo e combatendo a corrupção."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_5,
					[
						"Além do controle dos recursos públicos federais, a CGU também se esforça para ensinar aos servidores públicos a forma correta de usar o dinheiro da população brasileira. Mas para não perder nada de vista, a CGU também investiga servidores que podem ter cometido erros no uso do dinheiro público e, nos casos necessários, aplica as punições.",							
						"Mas a CGU não está sozinha na missão de fiscalizar os gastos públicos: seus grandes aliados são os cidadãos. É por isso que a CGU também procura estimular todas as pessoas a participarem desse controle.",
						"Os conselhos municipais foram criados para ajudar a prefeitura na tarefa de utilizar bem o dinheiro público."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_6,
					[
						"A CGU também recebe e investiga denúncias sobre o mau uso dos recursos públicos federais e ainda tem um canal para receber reclamações, elogios e sugestões sobre as ações do Poder Executivo Federal.",
						"O orçamento é a lei na qual os governos (municipal, estadual ou federal) deixam claro o que pretendem fazer com o dinheiro público. Nos municípios, essa lei é votada uma vez por ano na câmara municipal."
					]);

					// BEGIN Pessoas da quest da dengue
					setCharacterSpeeches( CHARACTER_MULHER6, [
						"O posto está movimentado hoje...",
						"Minha filha está de cama com dor no corpo, febre e enjôo. O que será isso?"	
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER7, [
						"Você viu a fila do posto de saúde? Parece que todos adoeceram ao mesmo tempo."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER2, [	
						"Nosso posto de saúde está em péssimo estado. Falta médicos, enfermeiros, aparelhos e medicamentos...",
						"Já tentei reclamar na prefeitura, mas até agora nada foi feito.",
						"Mas você já viu a quantidade de gente lá na frente? Será que é uma epidemia?",
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM6, [
						"Uh...uh... uwaaaa... Que dor!"
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM7, [
						"Não estou nada bem..."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM1, [
						"É... Me manda..ram...vir aqui, mas eu estou be...ugh.."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER10, [
						"Uuuuugh..."
					] );
					
					// END Pessoas da quest da dengue
					
					setCharacterSpeeches(CHARACTER_POLITICO, [
						"Meu nome é Patrício. Sou assistente do Sr. Lopes e respondo por ele quando ele não está disponível.",
						"O prefeito está muito ocupado. Você pode se reportar a mim, e repassarei para ele assim que possível.",
						"O que foi agora?"
					] );					

					setCharacterSpeeches(CHARACTER_HIPPIE, [
						"Boa tarde. Meu nome é Lamar. Eu vendo CDs de música.\nVê algo que te interessa?",
						"Pode olhar à vontade. Estou aqui para ajudar.\nSe estiver procurando algo específico é só me dizer que tento trazer amanhã.",
						"Tenho CDs de samba, rock, funk, forró, música clássica, pagode, romântica, o que você quiser! Tudo pelo melhor preço da região!"
					] );

					setCharacterSpeeches( CHARACTER_HOMEM2, [
						"O cachorro, Kiko, vive fugindo.",
						"Ele gosta de ir para o mato atrás da cidade."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM3, [
						"Sabia que hoje existem sacolas recicláveis?",
						"Você pode usar ela todas as vezes que for no supermercado. Normalmente são feitas de pano.",
						"Alguns mercados da cidade grande já estão vendendo como alternativa às sacolas de plástico.",
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM5, [
						"O Lamar sempre consegue os CDs que eu peço." ,
						"Eles são bem mais baratos que nas lojas perto do meu trabalho. Por que será?"			
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM10, [
						"Desisto! Quase nenhum telefone público daqui funciona.",
						"Ei... Você tem algum cartão para me emprestar?"
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA1, [
						"No recreio sempre me encontro com meus amigos para trocar figurinhas.",
						"Estou quase completando meu álbum!"
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA2, [
						"Quando crescer vou eleger o meu herói Capitão Sabão! Meu pai disse que, talvez, até ele veja mais os problemas da Vila Virtual do que o prefeito Lopes.", 
						"Sabia que você já pode votar com 16 anos? Só aos 18 que é obrigatório."
					] );
						
					setCharacterSpeeches( CHARACTER_CRIANCA3, [
						"Vi o Kiko indo na direção da floresta atrás da cidade."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA4, [
						"Final de semana vou para a pracinha. Não vejo a hora." ,
						"Quando brinco no balanço me sinto mais alta do que todo mundo!"
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA5, [
						"Finalmente minha irmã teve tempo para me trazer aqui."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA6, [
						"O meu professor de Educação Física disse que sempre tenho que tomar muita água."
					] );
					
					setCharacterSpeeches( CHARACTER_CADEIRANTE, [
						"Gosto muito de aproveitar um bonito dia de sol como hoje aqui no campinho."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER3, [
						"O Sr. Gustavo é um excelente bombeiro. Ele sempre me ajuda com informações.",
						"O Enzo tinha me falado sobre uma casa que está cheia de mosquitos.",					
						"Ele parecia bem preocupado... Ele disse que esta casa está abandonada há muito tempo.",
						"Fale com ele, assim você saberá onde essa casa fica."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER5, [
						"A Dona Anita é quem mais sabe sobre as pessoas da Vila. Mais até do que o prefeito!",
						"Eu adoro aquela música! É mais ou menos assim: LA - LAAAAA - LALALALA - LA - LUUUUUU!!!"
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER8, [
						"Esta ONG foi criada para oferecer os mais diversos cursos para jovens. Estes vão desde capoeira e skate até idiomas, como inglês e chinês, além de vários cursos de informática.",
						"Mesmo um governo sério e honesto precisa da nossa ajuda. Cada um deve fazer sua parte!"
					] );

					setCharacterSpeeches( CHARACTER_MULHER9, [
						"Não acredito que o prefeito não vê o estado dessa cidade!",
						"Outro dia vi um vendedor de CDs piratas perto da ONG."
					] );
					
					if( isContinuing )
					{
						wait( 0.1 );
						lockInput( true );
						playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );
						setBackground( Constants.BACKGROUND_MAP );
						lockInput( false );
					}
					else
					{
						wait( 0.1 );
						lockInput( true );
					
						// Especiais
						setCharacterTileByPortal( CHARACTER_GUARDA, PORTAL_GUARDA_1 )
						setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_BOMBEIRO, PORTAL_BOMBEIRO );
						setCharacterExpression( CHARACTER_BOMBEIRO, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_VELHINHA, PORTAL_VELHA1 );
						setCharacterExpression( CHARACTER_VELHINHA, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_HIPPIE, PORTAL_HIPPIE );
						setCharacterTileByPortal( CHARACTER_FRENTISTA, PORTAL_POSTO1 );
						
						setCharacterTileByPortal( CHARACTER_PERGUNTADOR, PORTAL_PARQUE2 );
						
						setCharacterTileByPortal( CHARACTER_CEGO, PORTAL_PARQUE1 );
						setCharacterDirection(CHARACTER_CEGO, Direction.DIRECTION_RIGHT);
						setCharacterTileByPortal( CHARACTER_CADEIRANTE, PORTAL_FUTEBOL1 );
						setCharacterTileByPortal( CHARACTER_CHINA, PORTAL_CHINA1 );
						setCharacterTileByPortal( CHARACTER_DELEGADO, PORTAL_POLICIA2 );
						setCharacterTileByPortal( CHARACTER_POLITICO, PORTAL_ASSISTENTE );
						
						setCharacterTileByPortal( CHARACTER_GARI, PORTAL_GARI1 );
						setCharacterExpression( CHARACTER_GARI, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_VENDEDORA, PORTAL_VENDA4 );
						
						// Pessoas "didáticas"
						
						setCharacterTileByPortal( CHARACTER_WISE_1, PORTAL_WISE_1 );
						setCharacterDirection( CHARACTER_WISE_1, Direction.DIRECTION_DOWN );
						setCharacterTileByPortal( CHARACTER_WISE_2, PORTAL_WISE_2 );
						setCharacterDirection( CHARACTER_WISE_2, Direction.DIRECTION_UP );
						
						setCharacterTileByPortal( CHARACTER_WISE_3, PORTAL_WISE_3 );
						setCharacterDirection( CHARACTER_WISE_3, Direction.DIRECTION_LEFT );
						setCharacterTileByPortal( CHARACTER_WISE_4, PORTAL_WISE_4 );
						setCharacterDirection( CHARACTER_WISE_4, Direction.DIRECTION_RIGHT );
						
						setCharacterTileByPortal( CHARACTER_WISE_5, PORTAL_WISE_5 );
						setCharacterDirection( CHARACTER_WISE_5, Direction.DIRECTION_LEFT );
						setCharacterTileByPortal( CHARACTER_WISE_6, PORTAL_WISE_6 );
						setCharacterDirection( CHARACTER_WISE_6, Direction.DIRECTION_RIGHT );
						
						// BEGIN Pessoas da quest da dengue
						
						setCharacterTileByPortal( CHARACTER_HOMEM1, PORTAL_FILA4 );
						setCharacterDirection( CHARACTER_HOMEM1, Direction.DIRECTION_DOWN );
						
						setCharacterTileByPortal( CHARACTER_HOMEM6, PORTAL_FILA1 );
						setCharacterDirection( CHARACTER_HOMEM6, Direction.DIRECTION_UP);
						
						setCharacterTileByPortal( CHARACTER_HOMEM7, PORTAL_FILA2 );
						setCharacterDirection( CHARACTER_HOMEM7, Direction.DIRECTION_RIGHT);
						
						setCharacterTileByPortal( CHARACTER_MULHER2, PORTAL_QUADRA1 );
						setCharacterDirection( CHARACTER_MULHER2, Direction.DIRECTION_RIGHT );
						
						setCharacterTileByPortal( CHARACTER_MULHER6, PORTAL_PSAUDE2 );
						setCharacterDirection( CHARACTER_MULHER6, Direction.DIRECTION_DOWN );
						
						setCharacterTileByPortal( CHARACTER_MULHER7, PORTAL_FILA5 );
						setCharacterDirection( CHARACTER_MULHER7, Direction.DIRECTION_RIGHT );
						
						setCharacterTileByPortal( CHARACTER_MULHER10, PORTAL_FILA3 );
						setCharacterDirection( CHARACTER_MULHER10, Direction.DIRECTION_RIGHT);
						
						// END Pessoas da quest da dengue
						
						// BEGIN Pessoas da quest do entulho
						
						setCharacterTileByPortal( CHARACTER_HOMEM4, PORTAL_DELEGACIA1 );
						
						setCharacterTileByPortal( CHARACTER_HOMEM9, PORTAL_BECO1 );
						setCharacterDirection( CHARACTER_HOMEM9, Direction.DIRECTION_UP);
						
						setCharacterTileByPortal( CHARACTER_HOMEM11, PORTAL_HIPPIE3 );
							
						setCharacterTileByPortal( CHARACTER_MULHER4, PORTAL_RUA7 );
						
						setCharacterTileByPortal( CHARACTER_MULHER5, PORTAL_BANCA2 );
						setCharacterDirection( CHARACTER_MULHER5, Direction.DIRECTION_RIGHT);
						
						// END Pessoas da quest do entulho
						
						
						// Homens genéricos
						setCharacterTileByPortal( CHARACTER_HOMEM2, PORTAL_BANCA );
						setCharacterTileByPortal( CHARACTER_HOMEM3, PORTAL_PADARIA3 );
						setCharacterTileByPortal( CHARACTER_HOMEM5, PORTAL_HIPPIE2 );
						
						setCharacterTileByPortal( CHARACTER_HOMEM8, PORTAL_PARQUE5 );
						
						setCharacterTileByPortal( CHARACTER_HOMEM10, PORTAL_PADARIA4 );
						setCharacterDirection(CHARACTER_HOMEM10, Direction.DIRECTION_UP);
						
						// Mulheres genéricas
						setCharacterTileByPortal( CHARACTER_MULHER1, PORTAL_PARQUE3 );
						setCharacterTileByPortal( CHARACTER_MULHER3, PORTAL_BOMBEIRO2 );						
						setCharacterTileByPortal( CHARACTER_MULHER8, PORTAL_ONG3 );
						setCharacterTileByPortal( CHARACTER_MULHER9, PORTAL_BOMBEIRO3 );
						setCharacterDirection(CHARACTER_MULHER9, Direction.DIRECTION_RIGHT);
						
						// Crianças genéricas
						setCharacterTileByPortal( CHARACTER_CRIANCA1, PORTAL_ESCOLA1 );
						setCharacterTileByPortal( CHARACTER_CRIANCA2, PORTAL_ESCOLA2 );
						setCharacterDirection(CHARACTER_CRIANCA2, Direction.DIRECTION_UP);
						setCharacterTileByPortal( CHARACTER_CRIANCA3, PORTAL_CHINA2 );
						setCharacterDirection(CHARACTER_CRIANCA3, Direction.DIRECTION_LEFT);
						setCharacterTileByPortal( CHARACTER_CRIANCA4, PORTAL_CHINA3 );
						setCharacterTileByPortal( CHARACTER_CRIANCA5, PORTAL_PARQUE4 );
						setCharacterDirection(CHARACTER_CRIANCA5, Direction.DIRECTION_LEFT);
						setCharacterTileByPortal( CHARACTER_CRIANCA6, PORTAL_ESCOLA6 );
						setCharacterDirection(CHARACTER_CRIANCA6, Direction.DIRECTION_RIGHT);
						
						// Entulhos - Quest entulho
						setCharacterTileByPortal( CHARACTER_OBJ_ENTULHO1, PORTAL_RUA1 );
						setCharacterTileByPortal( CHARACTER_OBJ_ENTULHO2, PORTAL_RUA2 );
						setCharacterTileByPortal( CHARACTER_OBJ_ENTULHO3, PORTAL_RUA3 );
						setCharacterTileByPortal( CHARACTER_OBJ_ENTULHO4, PORTAL_RUA4 );
						setCharacterTileByPortal( CHARACTER_OBJ_ENTULHO5, PORTAL_RUA5 );
						setCharacterTileByPortal( CHARACTER_OBJ_ENTULHO6, PORTAL_RUA6 );
						
						// Kiko - Quest cachorro
						setCharacterTileByPortal( CHARACTER_CACHORRO, PORTAL_CACHORRO0 );
						
						// Garotos do beco
						setCharacterTileByPortal(CHARACTER_VICIADO5, PORTAL_BECOENTRADA);
					
						// Carros
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_1, PORTAL_CARRO11 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_2, PORTAL_CARRO12 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_3, PORTAL_CARRO13 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_4, PORTAL_CARRO14 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_5, PORTAL_CARRO15 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_9, PORTAL_CARRO16 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_10, PORTAL_CARRO17 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_11, PORTAL_CARRO18 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_12, PORTAL_CARRO19 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_13, PORTAL_CARRO20 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_14, PORTAL_CARRO21 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_15, PORTAL_CARRO22 );
					
						playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );
						showMessage( "Você foi escolhido pela sua escola para participar do projeto Jovem Cidadão, que busca estimular a cidadania e participação popular dos estudantes da vila.\n(APERTE ESPAÇO OU ENTER PARA AVANÇAR)" );
						showMessage( "Nesse projeto, um estudante é escolhido em cada escola para atuar no dia a dia da vila, ajudando a melhorar as condições de vida dos moradores e, ao mesmo tempo, fiscalizar a prefeitura para que os serviços públicos tenham boa qualidade." );
						
						setBackgroundEx( Constants.BACKGROUND_PREFEITURA1, false, true );
						showMessage("Bem-vindo! Então é você o selecionado pela Escola Einstein para ser o nosso Jovem Cidadão, certo?",CHARACTER_PREFEITO);
						showMessage("Pois bem, aqui na Vila Virtual, eu sou o chefe do poder executivo, meu papel é cumprir as leis criadas pelos vereadores e fazer funcionar os serviços públicos necessários aos moradores, como saúde e educação.",CHARACTER_PREFEITO);
						showMessage("Além dos vereadores, a própria população pode fiscalizar o uso do dinheiro público, afinal, como o próprio nome diz, o dinheiro público é do povo!",CHARACTER_PREFEITO);
						showMessage("Pelo que Patrício, meu assessor, me disse, parece que a vila tem poucos problemas sérios.", CHARACTER_PREFEITO);
						showMessage("Mas é para isso que o projeto serve: você precisa ir para a rua e conhecer os problemas da população, sempre tentando ajudar a resolvê-los.",CHARACTER_PREFEITO);
						
						setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_PARTE1P);
						setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT);	
						setBackground( Constants.BACKGROUND_MAP );
						
						showMessage( "Esta é a sua primeira visita à Vila Virtual?", -1,
							"Sim, me explique como funciona!", function() : void {
								wait( 0.1 );
								showMessage( "Use as setas direcionais do seu teclado para andar pela vila. Para falar com as pessoas, aperte a tecla ESPAÇO ou ENTER." );
								
								setIconBlinking( Constants.ICON_BACKPACK, true );
								showMessage( "Algumas pessoas podem te dar itens para te ajudar. Clique na mochila para ver sua lista de itens." );
								showMessage( "Seus itens serão usados automaticamente quando for preciso!" );
								
								setIconBlinking( Constants.ICON_QUEST_BOOK, true );
								showMessage( "Veja aqui no seu caderno o estado das missões que você iniciou, e dicas de como concluí-las." );
								
								setIconBlinking( Constants.ICON_MAP, true );
								showMessage( "O mapa te ajudará a se orientar pela Vila Virtual. Sua posição é mostrada pelo ícone que fica piscando. Assim não tem como se perder!" );
								showMessage( "As pessoas que podem te ajudar a completar as missões também são mostradas aqui. Fale com elas, tudo fica mais fácil quando se pede ajuda!" );
								
								setIconBlinking( Constants.ICON_CELL_PHONE, true );
								showMessage( "Se quiser fazer uma pausa, é só clicar no celular!" );
								
								setIconBlinking( Constants.ICON_HAPPINESS_BAR, true );
								showMessage( "Esta barra indica seu progresso. Quanto maior a parte listrada, maior a felicidade geral da Vila Virtual e mais perto do final do jogo você está." );
								setIconBlinking( Constants.ICON_HAPPINESS_BAR, false );
								
								showMessage( "Preste atenção ao ícone de exclamação sobre a cabeça de algumas pessoas!" );
								setCharacterExpression( CHARACTER_POLITICO, Character.EXPRESSION_EXCLAMATION );
								wait( 2.5 );
								showMessage( "A exclamação indica que a pessoa tem dicas importantes para te ajudar a completar suas missões." );
								setCharacterExpression( CHARACTER_POLITICO, Character.EXPRESSION_NONE );
								showMessage( "Mesmo quando a exclamação não aparece, fale com as pessoas! Há sempre muito a aprender com a experiência dos outros." );
								
								showMessage( "Chegamos ao fim deste tutorial. Obrigado pela visita!\n\nBom jogo!" );
								
								lockInput( false );
							},
							"Não, já visitei a Vila Virtual antes e sei como jogar. Vamos logo para o jogo!", function() : void {
								showMessage( "Então seja bem-vindo(a) de volta!" );
								showMessage( "Lembre-se: utilize as setas direcionais para andar pela vila. Para falar com as pessoas, use as teclas ESPAÇO ou ENTER.\n\nBom jogo!" );								
								lockInput( false );
							} 
						);
					}
					break; // FIM DA PARTE 1

				case PARTE_2:
					// Falas didáticas
					setCharacterSpeeches( CHARACTER_WISE_1,
					[
						"Além da CGU, existe outro órgão responsável por defender os interesses dos cidadãos: o Ministério Público (MP) brasileiro.",
						"O Conselho de Alimentação Escolar controla o dinheiro destinado à merenda. Parte dessa verba vem do Governo Federal e parte vem da prefeitura. O Conselho verifica se o que a prefeitura comprou está chegando nas escolas, analisa a qualidade da merenda comprada e olha se os alimentos estão bem guardados e conservados.",
						"O Conselho Municipal de Saúde controla o dinheiro da saúde. Ele deve acompanhar as verbas que chegam pelo Sistema Único de Saúde (SUS) e os repasses de programas federais, participar da elaboração das metas para a saúde e controlar a execução das ações na saúde, devendo reunir-se pelo menos uma vez por mês."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_2,
					[
						"O Ministério Público (MP) deve defender os cidadãos contra eventuais abusos e omissões do Poder Público.",
						"O MP também deve proteger o patrimônio público contra ataques de particulares de má-fé.",
						"O Conselho do Fundeb acompanha e controla a aplicação dos recursos destinados à educação, quanto chegou e como está sendo gasto, comunicando ao FNDE a ocorrência de irregularidades."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_3,
					[
						"Cabe ao Ministério Público defender os direitos sociais e individuais de que nenhuma pessoa pode abrir mão, como o direito à vida, à dignidade, à liberdade, etc.",
						"A maior parte da verba do Fundeb, no mínimo 60%, é destinada ao pagamento dos salários dos professores que lecionam na educação básica. O restante é para pagar funcionários da escola e para comprar equipamentos escolares (mesas, cadeiras, quadros-negros etc.)"
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_4,
					[
						"A Controladoria Geral da União (CGU) criou o Programa Olho Vivo no Dinheiro Público, que visita vários municípios durante o ano e ensina aos cidadãos como fiscalizar os recursos públicos.",
						"As pessoas aprendem com os auditores da CGU a ficar de olho no dinheiro do povo para saber se ele está sendo gasto de forma correta, sem corrupção."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_5,
					[
						"Você sabia que a CGU tem um site informativo para crianças? O endereço é www.portalzinho.cgu.gov.br .",
						"A CGU não atua sozinha no controle do uso de dinheiro público. O papel da CGU é verificar se o dinheiro está sendo usado adequadamente ou se está sendo desviado para outras finalidades; mas a CGU nem sempre pode julgar e punir os responsáveis por irregularidades. Esse papel cabe à Justiça, que pode ser acionada por qualquer cidadão, por meio do Ministério Público (promotor)."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_6,
					[
						"Um governo democrático deve oferecer aos cidadãos meios para estes apresentarem suas opiniões, dúvidas e reivindicações.",
						"A administração pública deve ser transparente, para que as pessoas possam acompanhar e participar das decisões sobre o uso do dinheiro e do patrimônio público.",
						"Todo cidadão tem o direito de saber onde e como é gasto o dinheiro público."
					]);

					// Genéricos
					setCharacterSpeeches( CHARACTER_HOMEM1, [
						"Nos postos de saúde, o atendimento é gratuito.",
						"Se você tiver algum problema eles te atenderão, mas sempre respeitando quem tem mais urgência."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM2, [
						"Aproveito as horas vagas para ir até a praça. Lá sempre tem pessoas agradáveis de se conversar."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM3, [
						"Fiquei sabendo que a escola está caindo aos pedaços por dentro e quase sem aulas."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM4, [
						"O Gustavo é um ótimo bombeiro.",
						"Teve uma vez que caiu um balão no matagal aqui do lado e ele acabou com o fogo em dois segundos.",
						"Que perigo, soltar balões! Será que ninguém pensa nas conseqüências?"
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM5, [
						"Adoro biscoitos.",
						"Outro dia fiz uma receita de biscoito com aveia e mel."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM6, [
						"Já tentei morar na cidade grande, mas gosto muito mais daqui.",
						"Adoro ouvir a cantoria dos pássaros.",
						"Mesmo com seus problemas, a Vila é um lugar muito tranquilo."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM7, [
						"Com a coleta seletiva, mais materiais são reciclados.",
						"Você já ouviu falar do banco de garrafa PET? Eles pegam as do lixo e transformam em algo novamente útil."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM8, [
						"Com a substituição das sacolas plásticas na venda, não tenho mais tanta praticidade para limpar o cocô do Kiko na rua.",
						"Mas em compensação o lixo está reduzindo muito. É tão bom ver tudo limpo novamente!"
					] );
					
					if( getQuestStatus( QUEST_DENGUE ) == QuestState.QUEST_STATE_COMPLETE )
					{
						setCharacterSpeeches( CHARACTER_HOMEM9, [
							"A dengue é um problema sério. Não podemos brincar com esses mosquitos.",					
							"Pelo menos estou vendo que todos estão tomando precauções. Aquela casa ao lado do beco, por exemplo, agora não é mais um grande foco."
						] );
					}
					else
					{
						setCharacterSpeeches( CHARACTER_HOMEM9, [
							"A dengue é um problema sério. Não podemos brincar com esses mosquitos.",					
							"Estou ficando preocupado, pois não estamos combatendo este problema como deveríamos. Vide aquela casa perto do beco."
						] );
					}
					
					setCharacterSpeeches( CHARACTER_HOMEM10, [
						"Este daqui também não funciona... Alguém quebra os telefones públicos, e não pensa que outras pessoas precisam disso. Depois não adianta reclamar, né?",
						"Preciso ligar para minha família. Eles estão muito longe."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM11, [
						"Ando ouvindo muitos murmurinhos naquele beco ultimamente.",	
						"Mais os dias passam e mais aqueles desocupados não me deixam dormir."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA1, [
						"Aqui na escola estamos aprendendo sobre os direitos das crianças e adolescentes.",
						"Toda criança tem direito a se expressar livremente.",
						"As crianças não devem ser exploradas por trabalho.",
						"Todas as crianças são iguais e têm direito à boa saúde.",
						"São tantos direitos que esqueci o resto, vou perguntar para professora depois."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA2, [
						"Até eu que sou criança vejo os problemas da cidade muito melhor que o prefeito Lopes.", 
						"Sabia que você já pode votar com 16 anos? Só aos 18 que é obrigatório. Eu quero muito fazer 16 logo e ajudar a escolher os representantes da comunidade!"			
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA3, [
						"Quando crescer quero jogar futebol!",
						"Uhuuuu!"
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA4, [
						"Eu sei que minha mãe não gosta que eu coma os pastéis do Sr. Wong. Mas eles são deliciosos!"
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA5, [
						"Meu amigo disse que o Sr. Patrício é mau."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA6, [
						"A gente está quase sem aula na escola porque estão faltando professores.",				
					] );
					
					setCharacterSpeeches( CHARACTER_CACHORRO, [
						"Woof, woof!",
						"Woof!",
						"WOOF! Woff!",
						"Woooooff!",
						"Grrr...",
						"WOOF! WOOF! WOOF!",
						"..."
					] );
					
					setCharacterSpeeches(CHARACTER_CEGO, [
						"O posto de saúde nunca tem os remédios que eu preciso!",
						"Isso é um absurdo! O prefeito deveria fazer algo a respeito!",
						"Pra onde vão os meus impostos afinal?",
						"Eu tenho direitos!",
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER1, [				
						"Já provou os biscoitos que a Dona Anita faz? São deliciosos!"
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER2, [
						"Todos os dias caminho pela vila para manter a forma."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER3, [
						"Adoro suco! É tão refrescante e, o melhor, saudável."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER4, [
						"Finalmente consegui trazer meu filho na quadra. Ele está tão feliz!",
						"Ele sempre me pede para trazê-lo aqui e é muito perigoso deixar que ele ande sozinho."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER5, [
						"A Dona Anita é quem mais sabe sobre as pessoas da Vila. Mais até do que o prefeito!"
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER6, [
						"Vi o Juca andando um dia por aí. Ele estava meio triste.",
						"Ouvi falar que são problemas familiares."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER7, [
						"Todo o lixo daqui vai para um aterro que fica na saída da cidade."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER8, [
						"Você viu aqueles desocupados no beco? Aqueles jovens costumavam ser bons meninos antigamente.",
						"Ouvi dizer que eles estão sempre matando aulas... E as famílias achando que eles estão levando os estudos a sério..."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER9, [
						"Sinto falta de música nessa vila. O prefeito deveria investir mais nisso. O máximo que temos é aquele vendedor de CDs piratas que ainda está no mesmo lugar.",
						"Música é cultura!"
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER10, [
						"Esta cidade está ficando cada dia melhor, com a participação de todos."
					] );
					
					if( isContinuing )
					{
						wait( 0.1 );
						lockInput( true );
						playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );
						setBackground( Constants.BACKGROUND_MAP );
						lockInput( false );
					}
					else
					{
						wait( 0.1 );
						lockInput( true );
						playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );

						setBackground( Constants.BACKGROUND_PREFEITURA1 );
						wait( 0.1 );
						showMessage( "Aaah... Minha barriga dói...\nPatrício me disse que você tem feito algum progresso. Estou orgulhoso. Que bom que o programa está mostrando algum resultado.", CHARACTER_PREFEITO );
						showMessage( "Não sei como você conseguiu ajudar tantas pessoas numa cidade com tão poucos problemas! Meus parabéns!", CHARACTER_PREFEITO );
						showMessage( "Continue se reportando ao Patrício... Minha barriga dói...", CHARACTER_PREFEITO );
						showMessage( "Aquele Sr.Wong deveria ser mais higiênico ao preparar os pastéis.", CHARACTER_PREFEITO );
						showMessage( "Você podia falar com ele sobre isso... Urgh... Tenho que voltar ao toalete... Fui!", CHARACTER_PREFEITO );
						startQuest(QUEST_CHINA);
						
						// Especiais
						setCharacterTileByPortal( CHARACTER_POLITICO, PORTAL_ASSISTENTE );
						
						setCharacterTileByPortal( CHARACTER_CEGO, PORTAL_CEGO1 );
						setCharacterDirection( CHARACTER_CEGO, Direction.DIRECTION_RIGHT );
						setCharacterExpression( CHARACTER_CEGO, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_JOGADOR, PORTAL_PARTE1F );
						setCharacterDirection( CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT );
						
						setCharacterTileByPortal( CHARACTER_GUARDA, PORTAL_GUARDA_1 );
						setCharacterExpression( CHARACTER_GUARDA, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_BOMBEIRO, PORTAL_BOMBEIRO );
						
						setCharacterTileByPortal( CHARACTER_VELHINHA, PORTAL_VELHA4 );
						
						// a velhinha so ajuda caso tenha sido ajudada no começo do jogo
						if ( getQuestStatus (QUEST_VELHARUA) == QuestState.QUEST_STATE_COMPLETE )
							setCharacterExpression( CHARACTER_VELHINHA, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_HIPPIE, PORTAL_HIPPIE );
						
						setCharacterTileByPortal( CHARACTER_FRENTISTA, PORTAL_POSTO1 );
						
						setCharacterTileByPortal( CHARACTER_CADEIRANTE, PORTAL_CADEIRANTE1 );
						setCharacterExpression( CHARACTER_CADEIRANTE, Character.EXPRESSION_EXCLAMATION );
						setCharacterTileByPortal( CHARACTER_IRMAO_LARISSA, PORTAL_QUADRA4 )

						setCharacterTileByPortal( CHARACTER_CHINA, PORTAL_CHINA1 );
						setCharacterExpression( CHARACTER_CHINA, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_DELEGADO, PORTAL_POLICIA2 );
						
						setCharacterTileByPortal( CHARACTER_GARI, PORTAL_GARI1 );
						setCharacterExpression( CHARACTER_GARI, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_VENDEDORA, PORTAL_VENDA4 );
						
						setCharacterTileByPortal( CHARACTER_PLAYBOYCAR, PORTAL_CARRO1 );
						setCharacterDirection( CHARACTER_PLAYBOYCAR, Direction.DIRECTION_DOWN );
						
						if ( getQuestStatus( QUEST_CACHORRO ) == QuestState.QUEST_STATE_COMPLETE )
							setCharacterTileByPortal( CHARACTER_CACHORRO, PORTAL_PARQUE7 );
						
						setCharacterTileByPortal( CHARACTER_PERGUNTADOR, PORTAL_PREFEITURA4 );
						setCharacterDirection ( CHARACTER_PERGUNTADOR, Direction.DIRECTION_LEFT);
						
						setCharacterTileByPortal(CHARACTER_VICIADO5, PORTAL_BECOENTRADA);
						
						// Pessoas didáticas 
						setCharacterTileByPortal( CHARACTER_WISE_1, PORTAL_WISE_1 );
						setCharacterDirection( CHARACTER_WISE_1, Direction.DIRECTION_DOWN );
						setCharacterTileByPortal( CHARACTER_WISE_2, PORTAL_WISE_2 );
						setCharacterDirection( CHARACTER_WISE_2, Direction.DIRECTION_UP );
						
						setCharacterTileByPortal( CHARACTER_WISE_3, PORTAL_WISE_3 );
						setCharacterDirection( CHARACTER_WISE_3, Direction.DIRECTION_LEFT );
						setCharacterTileByPortal( CHARACTER_WISE_4, PORTAL_WISE_4 );
						setCharacterDirection( CHARACTER_WISE_4, Direction.DIRECTION_RIGHT );
						
						setCharacterTileByPortal( CHARACTER_WISE_5, PORTAL_WISE_5 );
						setCharacterDirection( CHARACTER_WISE_5, Direction.DIRECTION_LEFT );
						setCharacterTileByPortal( CHARACTER_WISE_6, PORTAL_WISE_6 );
						setCharacterDirection( CHARACTER_WISE_6, Direction.DIRECTION_RIGHT );

						// Homens genéricos
						setCharacterTileByPortal( CHARACTER_HOMEM1, PORTAL_DELEGACIA1 );
						setCharacterTileByPortal( CHARACTER_HOMEM2, PORTAL_PADARIA3 );
						setCharacterTileByPortal( CHARACTER_HOMEM3, PORTAL_BANCA );
						setCharacterTileByPortal( CHARACTER_HOMEM4, PORTAL_PSAUDE1);
						setCharacterTileByPortal( CHARACTER_HOMEM5, PORTAL_HIPPIE2 );
						setCharacterTileByPortal( CHARACTER_HOMEM6, PORTAL_PARQUE3 );
						setCharacterDirection(CHARACTER_HOMEM6, Direction.DIRECTION_UP);
						setCharacterTileByPortal( CHARACTER_HOMEM7, PORTAL_HIPPIE3);
						setCharacterTileByPortal( CHARACTER_HOMEM8, PORTAL_PARQUE5 );
						setCharacterTileByPortal( CHARACTER_HOMEM9, PORTAL_PARQUE6);
						setCharacterDirection(CHARACTER_HOMEM9, Direction.DIRECTION_LEFT);
						setCharacterTileByPortal( CHARACTER_HOMEM10, PORTAL_PADARIA4 );
						setCharacterDirection(CHARACTER_HOMEM10, Direction.DIRECTION_UP);
						setCharacterTileByPortal( CHARACTER_HOMEM11, PORTAL_POSTO3 );
						
						// Mulheres genéricas
						setCharacterTileByPortal( CHARACTER_MULHER1, PORTAL_POSTO2 );
						setCharacterTileByPortal( CHARACTER_MULHER2, PORTAL_QUADRA1 );
						setCharacterTileByPortal( CHARACTER_MULHER7, PORTAL_BOMBEIRO2 );
						setCharacterTileByPortal( CHARACTER_MULHER4, PORTAL_QUADRA2 );
						setCharacterTileByPortal( CHARACTER_MULHER5, PORTAL_BANCA2 );
						setCharacterDirection(CHARACTER_MULHER5, Direction.DIRECTION_RIGHT);
						setCharacterTileByPortal( CHARACTER_MULHER6, PORTAL_PSAUDE2 );
						setCharacterTileByPortal( CHARACTER_MULHER2, PORTAL_LANCHONETE3 );
						setCharacterDirection(CHARACTER_MULHER7, Direction.DIRECTION_LEFT);
						setCharacterTileByPortal( CHARACTER_MULHER8, PORTAL_ONG3 );
						setCharacterTileByPortal( CHARACTER_MULHER9, PORTAL_BOMBEIRO3 );
						setCharacterDirection(CHARACTER_MULHER9, Direction.DIRECTION_RIGHT);
						setCharacterTileByPortal( CHARACTER_MULHER10, PORTAL_PSAUDE3);
						setCharacterDirection(CHARACTER_MULHER10, Direction.DIRECTION_RIGHT);
						
						// Crianças genéricas
						setCharacterTileByPortal( CHARACTER_CRIANCA1, PORTAL_ESCOLA1 );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA2, PORTAL_ESCOLA2 );
						setCharacterDirection( CHARACTER_CRIANCA2, Direction.DIRECTION_UP );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA3, PORTAL_QUADRA3 );
						setCharacterDirection( CHARACTER_CRIANCA3, Direction.DIRECTION_LEFT );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA4, PORTAL_CHINA3 );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA5, PORTAL_ESCOLA7 );
						setCharacterDirection( CHARACTER_CRIANCA5, Direction.DIRECTION_UP );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA6, PORTAL_ESCOLA6 );
						setCharacterDirection( CHARACTER_CRIANCA6, Direction.DIRECTION_RIGHT );
					
						// Carros
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_1, PORTAL_CARRO11 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_2, PORTAL_CARRO12 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_3, PORTAL_CARRO13 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_4, PORTAL_CARRO14 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_5, PORTAL_CARRO15 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_9, PORTAL_CARRO16 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_10, PORTAL_CARRO17 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_11, PORTAL_CARRO18 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_12, PORTAL_CARRO19 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_13, PORTAL_CARRO20 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_14, PORTAL_CARRO21 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_15, PORTAL_CARRO22 );
						
						setBackground(Constants.BACKGROUND_MAP);
						showMessage( "O Kleber está te procurando!", CHARACTER_PERGUNTADOR );
						fadeOut();
						setCharacterDirection(CHARACTER_PERGUNTADOR, Direction.DIRECTION_UP);
						setCharacterTileByPortal( CHARACTER_PERGUNTADOR, PORTAL_PARQUE2 );					
						fadeIn();
						lockInput(false);
					}
					break;
				
				case PARTE_3:
					// Falas didáticas		
					setCharacterSpeeches( CHARACTER_WISE_1,
					[
						"Um órgão legislativo é uma entidade formada por pessoas eleitas para representar o povo e fazer as leis.",
						"A câmara de vereadores é o órgão legislativo dos municípios, sendo formado por uma assembleia de representantes dos cidadãos que moram ali.",						
						"Entre as funções da câmara dos vereadores está a de fiscalizar o poder executivo (a prefeitura e suas secretarias), votar o orçamento e, em situações específicas, julgar determinadas pessoas, como os próprios vereadores e o prefeito."						
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_2,
					[
						"Em uma democracia, o Presidente é escolhido pelo voto e permanece no poder por um período limitado. Esse período é chamado de mandato, que no Brasil é de quatro anos.",
						"Em uma república, a responsabilidade de buscar o bem da sociedade é compartilhada pelos poderes Executivo, Legislativo e Judiciário.",
						"Ao poder executivo cabe a função administrativa, como executar programas, prestar serviços públicos e aplicar a lei. Para isso, o Presidente tem o apoio dos ministros e de vários outros órgãos, que compõem o Governo Federal."
					]);
					
					setCharacterSpeeches( CHARACTER_WISE_3,
					[
						"Nos Estados, o chefe do Poder Executivo é o Governador, que recebe ajuda dos secretários estaduais. Já nos municípios, a tarefa cabe ao prefeito, com a colaboração dos secretários municipais.",
						"Em nível nacional, o Poder Legislativo é exercido pelo Senado Federal e pela Câmara dos Deputados, que, juntos, formam o Congresso Nacional. Nos Estados, é função das Assembleias Legislativas, e nos municípios, das Câmaras de Vereadores.",
						"O Poder Judiciário tem a responsabilidade de aplicar as leis às situações concretas do dia a dia das pessoas. Essa função é exercida pelos juízes e tribunais."
					]);
								
					setCharacterSpeeches( CHARACTER_WISE_4,
					[
						"Os bens públicos devem sempre atender ao interesse coletivo, enquanto os bens privados podem ser usados por seus donos como acharem melhor.",
						"Em uma democracia representativa, em vez de decidirmos diretamente o que fazer, escolhemos representantes, por meio das eleições. No município, escolhemos o prefeito e os vereadores. Nos Estados, escolhemos o governador e os deputados estaduais. E todos no país escolhem o presidente, os senadores e os deputados federais.",
						"Os vereadores possuem mandato de 4 anos, assim como o prefeito. Ambos são escolhidos pelo povo no mesmo processo eleitoral."
					]);
						
					setCharacterSpeeches( CHARACTER_WISE_5,
					[
						"Em uma democracia, embora a vontade da maioria deva prevalecer, devemos sempre respeitar as minorias, sem qualquer tipo de discriminação ou de censura.",
						"O número de vereadores nas câmaras é proporcional à população do município. Ou seja, o número de vereadores varia de cidade para cidade, ou seja, quanto maior a população, maior o número de vereadores.",
						"Os representantes do povo exercem suas funções por períodos definidos e podem ser substituídos se não fizerem um bom trabalho. Basta votar com responsabilidade."
					]);
						
					setCharacterSpeeches( CHARACTER_WISE_6,
					[
						"As pessoas também podem participar das decisões políticas apresentando sugestões e cobrando resultados de seus representantes. É o que chamamos de democracia participativa.",
						"Um governo democrático deve oferecer aos cidadãos meios para estes apresentarem suas opiniões, dúvidas e reivindicações.",
						"Para concorrer ao mandato de vereador, a idade mínima é de dezoito anos."
					]);
											
					// BEGIN Pessoas que foram da quest da dengue
					setCharacterSpeeches( CHARACTER_MULHER2, [
						"Sempre compro frutas frescas na venda da Dona Zefa."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER6, [
						"Você sabia que o Juca do posto tem dois irmãos?"
					] );
					
					if( getQuestStatus( QUEST_ENTULHO ) == QuestState.QUEST_STATE_COMPLETE )
					{
						setCharacterSpeeches( CHARACTER_MULHER7, [
							"Depois daquele mutirão para catar o lixo, nossa vila ficou muito melhor! Adoro o cheiro de ar puro!",
							"O mutirão me lembrou aquele velho ditado: a união faz a força!"
						] );
					}
					else
					{
						setCharacterSpeeches( CHARACTER_MULHER7, [
							"Nossa vila ainda está muito suja! Alguém tem que falar com o Kleber sobre isso."
						] );
					}
					
					setCharacterSpeeches( CHARACTER_MULHER10, [
						"Você já falou com aquele garoto na praça? Ele vive me perguntando tudo. Ô menino curioso!",
						"Meu filho disse que ele é muito bom aluno."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM1, [
						"Nossa! O posto de saúde melhorou muito depois que o Patrício foi descoberto.",
						"Ainda bem que agora o nosso prefeito está atendendo as vontades do povo. Ele é um bom homem. Estava apenas sendo enganado."
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM6, [
						"As sacolas de plástico são perigosas. Crianças podem se sufocar brincando com elas.",
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM7, [
						"Todo o lixo daqui vai para um aterro que fica na saída da cidade."
					] );
					
					// END Pessoas que foram da quest da dengue
					
					setCharacterSpeeches( CHARACTER_HOMEM2, [
						"Fiquei sabendo que agora a escola está com as salas sendo reformadas. Não é ótimo?",
						"Outra coisa boa também é que os pontos de coleta seletiva pela cidade aumentaram."	
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM4, [
						"Você sabia que os bombeiros fazem outras coisas além de apagar incêndio?",
						"Eles ajudam em desastres naturais e também fazem resgates aquáticos."	
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM8, [
						"O Kiko adora vir aqui todos os dias.",
						"Um dia vou abrir um abrigo de animais. Tem tantos abandonados por aí precisando de atenção..."			
					] );
							
					setCharacterSpeeches( CHARACTER_HOMEM10, [
						"Apesar de terem algumas dificuldades, as pessoas com deficiência física não devem ser tratadas com preconceito.",			
						"Você sabia que elas não gostam de ser tratados com pena? E com razão!"
					] );
					
					setCharacterSpeeches( CHARACTER_HOMEM11, [	
						"Olá, Tudo bem?"
					] );
							
					setCharacterSpeeches( CHARACTER_CRIANCA1, [
						"Aqui na escola estamos aprendendo sobre os direitos das crianças.",
						"Toda criança tem direito a se expressar livremente",
						"As crianças não devem ser exploradas por trabalho.",
						"Todas as crianças são iguais e têm direito à boa saúde",
						"São tantos direitos que esqueci o resto, vou perguntar para a professora depois."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA2, [
						"Quero chegar aos 16 anos logo para votar no melhor candidato..."	
					] );
								
					setCharacterSpeeches( CHARACTER_CRIANCA3, [
						"Quando crescer quero jogar futebol!",
						"Vou fazer vários gols!"
					] );
							
					setCharacterSpeeches( CHARACTER_CRIANCA4, [
						"Que bom, agora estamos com mais professores, antes tínhamos muito poucas aulas."
					] );
					
					setCharacterSpeeches( CHARACTER_CRIANCA5, [
						"Eu sabia! Aquele Patrício nunca me enganou! Era por causa dele que a escola estava daquele jeito."
					] );
							
					setCharacterSpeeches( CHARACTER_CRIANCA6, [						
						"Daqui a pouco vai tocar a sineta e vou voltar para a sala."
					] );
							
					setCharacterSpeeches( CHARACTER_MULHER1, [	
						"A Dona Anita é quem mais sabe sobre as pessoas da Vila. Mais até do que o prefeito!"
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER4, [
						"Finalmente consegui trazer meu filho na quadra. Ele está tão feliz!",
						"Ele sempre me pede para trazê-lo aqui e é perigoso deixar que ele ande sozinho."
					] );
					
					setCharacterSpeeches( CHARACTER_MULHER8, [
						"O ajudante do Patrício está solto na cidade. Temos que achá-lo antes que ele fuja!",
						"O prefeito deveria ter sido mais atento e percebido que o Patrício não é um cara legal."
					] );
					
					if ( getQuestStatus(QUEST_HIPPIE)== QuestState.QUEST_STATE_COMPLETE ){
						setCharacterSpeeches( CHARACTER_MULHER9, [
							"Sinto falta de música nessa vila. O prefeito deveria investir mais nisso. O máximo que tínhamos era aquele vendedor de CD's piratas, mas apreenderam a mercadoria dele.",
							"Música é cultura!"
						] );
					} else {
						setCharacterSpeeches( CHARACTER_MULHER9, [
							"Sinto falta de música nessa vila. O prefeito deveria investir mais nisso. Tomara que o Lamar consiga incentivar isso com a loja nova.",
							"Música é cultura!"
						] );
					}
					
					setCharacterSpeeches( CHARACTER_GARI, [
						"Cidade limpa não é a que mais se varre, e sim a que menos se suja!",
						"Ainda bem que todos estão aprendendo a fazer a sua parte... Se o povo não tiver educação, não tem exército de garis que dê conta de tanta sujeira!",
						"Você sabia que limpeza não deixa só a cidade bonita e evita doenças, mas é também uma questão de segurança? O lixo espalhado pelas ruas entope o esgoto e causa enchente na época de chuvas.",
					] );

					setCharacterSpeeches( CHARACTER_PERGUNTADOR, [
						"A cidade está bem melhor agora! Que pena que o prefeito Lopes não tenha sabido de tudo desde o início... Talvez já fossemos uma metrópole!!! hehehehe"
					] );
					
					if( isContinuing )
					{
						wait( 0.1 );
						lockInput( true );
						playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );
						setBackground(Constants.BACKGROUND_MAP);
						lockInput( false );
					}
					else
					{
						wait( 0.1 );
						lockInput( true );
						
						playSound( Constants.SOUND_MUSIC_THEME, 0, SoundManager.INFINITY_LOOP );
						
						fadeOut();
						setCharacterTileByPortal(CHARACTER_TRAFICANTE, PORTAL_TRAFICANTE1);
						setCharacterExpression( CHARACTER_TRAFICANTE, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal(CHARACTER_VICIADO1, PORTAL_P3GANG1);
						setCharacterTileByPortal(CHARACTER_VICIADO2, PORTAL_P3GANG2);
						setCharacterDirection(CHARACTER_VICIADO2, Direction.DIRECTION_LEFT);
						setCharacterTileByPortal(CHARACTER_VICIADO3, PORTAL_P3GANG3);
						setCharacterDirection(CHARACTER_VICIADO3, Direction.DIRECTION_UP);
						setCharacterTileByPortal(CHARACTER_VICIADO4, PORTAL_P3GANG4);
						setCharacterDirection(CHARACTER_VICIADO4, Direction.DIRECTION_UP);
						setCharacterTileByPortal(CHARACTER_VICIADO5, PORTAL_P3GANG5);
						setCharacterDirection(CHARACTER_VICIADO5, Direction.DIRECTION_DOWN);
						setCharacterTileByPortal( CHARACTER_GUARDA, PORTAL_GUARDA_1 );
						
						setCharacterTileByPortal( CHARACTER_BOMBEIRO, PORTAL_BOMBEIRO );
						setCharacterExpression( CHARACTER_BOMBEIRO, Character.EXPRESSION_NONE );
						
						setCharacterTileByPortal( CHARACTER_VELHINHA, PORTAL_VELHA4 );
						if ( getQuestStatus (QUEST_VELHARUA) == QuestState.QUEST_STATE_COMPLETE )
							setCharacterExpression( CHARACTER_VELHINHA, Character.EXPRESSION_EXCLAMATION );
						
						setCharacterTileByPortal( CHARACTER_FRENTISTA, PORTAL_POSTO1 );
						setCharacterTileByPortal( CHARACTER_PERGUNTADOR, PORTAL_PARQUE5 );
						setCharacterTileByPortal( CHARACTER_VENDEDORA, PORTAL_VENDA4 );
						
						// se o cachorro nao foi capturado, ele nao aparece mais
						if ( getQuestStatus( QUEST_CACHORRO ) == QuestState.QUEST_STATE_COMPLETE )
							setCharacterTileByPortal( CHARACTER_CACHORRO, PORTAL_PARQUE7 );
						
						setCharacterTileByPortal( CHARACTER_CADEIRANTE, PORTAL_FUTEBOL1 );
						setCharacterTileByPortal( CHARACTER_IRMAO_LARISSA, PORTAL_QUADRA4 )
						
						
						setCharacterTileByPortal( CHARACTER_DELEGADO, PORTAL_POLICIA2 );
						setCharacterTileByPortal( CHARACTER_GARI, PORTAL_GARI1 );
						
						// Personagens "didáticos"
						setCharacterTileByPortal( CHARACTER_WISE_1, PORTAL_WISE_1 );
						setCharacterDirection( CHARACTER_WISE_1, Direction.DIRECTION_DOWN );
						setCharacterTileByPortal( CHARACTER_WISE_2, PORTAL_WISE_2 );
						setCharacterDirection( CHARACTER_WISE_2, Direction.DIRECTION_UP );
						
						setCharacterTileByPortal( CHARACTER_WISE_3, PORTAL_WISE_3 );
						setCharacterDirection( CHARACTER_WISE_3, Direction.DIRECTION_LEFT );
						setCharacterTileByPortal( CHARACTER_WISE_4, PORTAL_WISE_4 );
						setCharacterDirection( CHARACTER_WISE_4, Direction.DIRECTION_RIGHT );
						
						setCharacterTileByPortal( CHARACTER_WISE_5, PORTAL_WISE_5 );
						setCharacterDirection( CHARACTER_WISE_5, Direction.DIRECTION_LEFT );
						setCharacterTileByPortal( CHARACTER_WISE_6, PORTAL_WISE_6 );
						setCharacterDirection( CHARACTER_WISE_6, Direction.DIRECTION_RIGHT );
						
						
						// BEGIN Pessoas que foram as pessoas da dengue
						
						setCharacterTileByPortal( CHARACTER_HOMEM1, PORTAL_PSAUDE1);
						setCharacterTileByPortal( CHARACTER_HOMEM6, PORTAL_VENDA3 );
						setCharacterTileByPortal( CHARACTER_HOMEM7, PORTAL_HIPPIE3 );
						
						setCharacterTileByPortal( CHARACTER_MULHER2, PORTAL_QUADRA1 );
						setCharacterTileByPortal( CHARACTER_MULHER6, PORTAL_LANCHONETE3 );
						setCharacterTileByPortal( CHARACTER_MULHER7, PORTAL_PSAUDE2 );
						setCharacterTileByPortal( CHARACTER_MULHER10, PORTAL_PSAUDE3 );
						
						// END  Pessoas que foram as pessoas da dengue
						
						// Pessoas que dão as dicas sobre os desocupados
						setCharacterTileByPortal( CHARACTER_HOMEM3, PORTAL_PADARIA3 );
						
						setCharacterTileByPortal( CHARACTER_HOMEM9, PORTAL_FUTEBOL4 );
						setCharacterDirection( CHARACTER_HOMEM9, Direction.DIRECTION_LEFT );
						
						setCharacterTileByPortal( CHARACTER_MULHER3, PORTAL_BOMBEIRO2 );
						
						setCharacterTileByPortal( CHARACTER_MULHER5, PORTAL_PADARIA4 );
						setCharacterDirection( CHARACTER_MULHER5, Direction.DIRECTION_RIGHT );

						// Genéricos sem importância
						setCharacterTileByPortal( CHARACTER_HOMEM2, PORTAL_BANCA );
						
						setCharacterTileByPortal( CHARACTER_HOMEM4, PORTAL_DELEGACIA1 );
						
						setCharacterTileByPortal( CHARACTER_HOMEM8, PORTAL_PARQUE2 );

						setCharacterTileByPortal( CHARACTER_HOMEM10, PORTAL_BANCA2 );
						setCharacterDirection( CHARACTER_HOMEM10, Direction.DIRECTION_UP );
						
						setCharacterTileByPortal( CHARACTER_HOMEM11, PORTAL_LANCHONETE3 );
						setCharacterDirection( CHARACTER_HOMEM11, Direction.DIRECTION_DOWN );
												
						setCharacterTileByPortal( CHARACTER_MULHER1, PORTAL_POSTO2 );

						setCharacterTileByPortal( CHARACTER_MULHER4, PORTAL_QUADRA2 );
						
						setCharacterTileByPortal( CHARACTER_MULHER8, PORTAL_ONG3 );
						
						setCharacterTileByPortal( CHARACTER_MULHER9, PORTAL_BOMBEIRO3 );
						setCharacterDirection(CHARACTER_MULHER9, Direction.DIRECTION_RIGHT);
						
						// Crianças
						setCharacterTileByPortal( CHARACTER_CRIANCA1, PORTAL_ESCOLA1 );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA2, PORTAL_ESCOLA2 );
						setCharacterDirection(CHARACTER_CRIANCA2, Direction.DIRECTION_UP);
						
						setCharacterTileByPortal( CHARACTER_CRIANCA3, PORTAL_QUADRA3 );
						setCharacterDirection(CHARACTER_CRIANCA3, Direction.DIRECTION_LEFT);
						
						setCharacterTileByPortal( CHARACTER_CRIANCA4, PORTAL_CHINA3 );
						
						setCharacterTileByPortal( CHARACTER_CRIANCA5, PORTAL_ESCOLA7 );
						setCharacterDirection(CHARACTER_CRIANCA5, Direction.DIRECTION_UP);
						
						setCharacterTileByPortal( CHARACTER_CRIANCA6, PORTAL_ESCOLA6 );
						setCharacterDirection(CHARACTER_CRIANCA6, Direction.DIRECTION_RIGHT);
					
						// Carros
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_1, PORTAL_CARRO3 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_2, PORTAL_CARRO4 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_3, PORTAL_CARRO5 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_4, PORTAL_CARRO6 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_5, PORTAL_CARRO7 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_6, PORTAL_CARRO8 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_7, PORTAL_CARRO9 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_8, PORTAL_CARRO10 );
						setCharacterTileByPortal( CHARACTER_OBJ_CAR_9, PORTAL_CARRO16 );
									
						// INTRODUÇÃO:
						setBackground(Constants.BACKGROUND_PREFEITURA2);
						showMessage("Ah, olá, você veio se reportar?\nEstou um pouco chocado, mas foi ótimo que descobrimos esse problema com o Patrício. Quem sabe o que poderia ter acontecido caso demorássemos mais!?", CHARACTER_PREFEITO2);
						showMessage("Ando muito ocupado com todos os problemas que chegaram às minhas mãos na última semana, mas estou feliz por estar podendo ajudar finalmente.\nÉ estranho descobrir que você não sabia nada sobre a cidade que você está governando.", CHARACTER_PREFEITO2);						

						if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE  )
						{
							showMessage( "Agradeço muito o fato de você ter me avisado sobre o Lamar! Imagina se esse rapaz não conseguisse legalizar a loja... Acho que eu nunca me perdoaria!", CHARACTER_PREFEITO2 );
						}
						else
						{
							showMessage( "Infelizmente não conseguimos legalizar a loja do Lamar... Quando o pedido chegou até mim, ele já havia deixado a cidade... coitado dele e de sua família...", CHARACTER_PREFEITO2 );
							showMessage( "..............", CHARACTER_PREFEITO2 );
						}

						showMessage( "Enfim, vamos trabalhar!", CHARACTER_PREFEITO2 );
						showMessage("Sua tarefa continua sendo a mesma! Você é muito bom nela, aliás! Pelo que andei observando, sua atitude vem contagiando as pessoas e todos estão ajudando uns aos outros.", CHARACTER_PREFEITO2);
						showMessage("Parece que o delegado Magalhães está lhe procurando. Fale com ele e depois continue a sua missão!", CHARACTER_PREFEITO2);
						setCharacterTileByPortal(CHARACTER_JOGADOR, PORTAL_PREFEITURA4);
						setCharacterDirection(CHARACTER_JOGADOR, Direction.DIRECTION_RIGHT);
						setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_PREFEITURA1);
						setCharacterDirection(CHARACTER_DELEGADO, Direction.DIRECTION_LEFT);
						setBackground(Constants.BACKGROUND_MAP);
						showMessage("Bom dia! Faz um tempo, não? Estamos todos trabalhando duro para provar de uma vez por todas que o Patrício é culpado, mas o problema ainda não foi inteiramente resolvido.",CHARACTER_DELEGADO);
						showMessage("Aquele ajudante que roubava o caminhão fornecedor das merendas fugiu quando prendemos o Patrício. Como já disse antes, ele ainda está solto na cidade.",CHARACTER_DELEGADO);
						showMessage("Se você por acaso o avistar, por favor nos avise. Precisamos encontrá-lo para provar que ambos são culpados.",CHARACTER_DELEGADO);
						startQuest(QUEST_TRAFICANTE);
						
						fadeOut();
						setCharacterTileByPortal(CHARACTER_DELEGADO, PORTAL_POLICIA2);
						if( getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE  )
						{
							setCharacterTileByPortal(CHARACTER_HIPPIE, PORTAL_PREFEITURA1);
							setCharacterDirection(CHARACTER_HIPPIE, Direction.DIRECTION_LEFT);
							fadeIn();
							showMessage("Olá! Quanto tempo!", CHARACTER_HIPPIE);
							showMessage("Vim agradecer o que fez por mim. Graças a você, o Sr. Lopes conseguiu ver meu pedido a tempo! Agora estou com uma loja!", CHARACTER_HIPPIE);
							showMessage("Venha me visitar, qualquer hora. Ela fica no mesmo lugar onde eu vendia os CDs piratas. Quem sabe eu não posso até te fazer um desconto!", CHARACTER_HIPPIE);
							fadeOut();
							setCharacterTileByPortal( CHARACTER_HIPPIE, PORTAL_MUSICA1 );
							setCharacterDirection(CHARACTER_HIPPIE, Direction.DIRECTION_RIGHT);
						}
						else
						{
							removeCharacter( CHARACTER_HIPPIE );
						}

						if( getQuestStatus( QUEST_CHINA ) == QuestState.QUEST_STATE_COMPLETE )
						{
							setCharacterTileByPortal(CHARACTER_CHINA, PORTAL_PREFEITURA1);
							setCharacterDirection(CHARACTER_CHINA, Direction.DIRECTION_LEFT);
							fadeIn();
							showMessage( "Bom dia!", CHARACTER_CHINA );
							showMessage( "Tenho boas notícias. A vigilância sanitária aprovou minha barraquinha e eu consegui autorização para abrir a lanchonete!", CHARACTER_CHINA );
							showMessage( "Fica na quadra ao lado do posto. Venha visitar. Meus pastéis estão ainda melhores!", CHARACTER_CHINA );
							showMessage( "Obrigado pela ajuda!", CHARACTER_CHINA );
							fadeOut();

							setCharacterTileByPortal( CHARACTER_CHINA, PORTAL_LANCHONETE1 );
							setCharacterDirection( CHARACTER_CHINA, Direction.DIRECTION_RIGHT );
							
							setCharacterTileByPortal( CHARACTER_HOMEM5, PORTAL_LANCHONETE5 );
							setCharacterDirection( CHARACTER_HOMEM5, Direction.DIRECTION_RIGHT );
							
							setCharacterTileByPortal( CHARACTER_CEGO, PORTAL_LANCHONETE2 );
							setCharacterDirection(CHARACTER_CEGO, Direction.DIRECTION_RIGHT);
							setCharacterExpression( CHARACTER_CEGO, Character.EXPRESSION_EXCLAMATION );
						}
						else
						{
							setCharacterTileByPortal( CHARACTER_CHINA, PORTAL_CHINA1 );
							
							setCharacterTileByPortal( CHARACTER_CEGO, PORTAL_PARQUE1 );
							setCharacterDirection(CHARACTER_CEGO, Direction.DIRECTION_RIGHT);
						}
						
						fadeIn();
						lockInput( false );
					}
					break;
				 	
				case PARTE_FINAL:
					wait( 0.1 );
					lockInput( true );
					playSound( Constants.SOUND_FX_ENDING, 0, SoundManager.INFINITY_LOOP );
					setBackground(Constants.BACKGROUND_PREFEITURA2);
					
					if ( getQuestStatus( QUEST_ONG ) == QuestState.QUEST_STATE_COMPLETE )
						showMessage("O delegado Magalhães me disse que você convenceu os meninos a voltar a estudar. Sem contar as matrículas nos cursos da ONG! Ótimo trabalho! Acho que falo por toda a Vila Virtual quando digo que estou muito agradecido.",CHARACTER_PREFEITO2);
					
					showMessage("Com a sua importante ajuda, a Vila voltou a ser um lugar seguro e alegre.",CHARACTER_PREFEITO2);
					showMessage("Sua participação no programa Jovem Cidadão tem sido excepcional, mas infelizmente teremos que abrir espaço para outras pessoas se inscreverem, para que elas aprendam o mesmo que você.",CHARACTER_PREFEITO2);
					showMessage("Não se esqueça que você não precisa estar inserido em nenhum programa para continuar fazendo boas ações. Mas eu aposto que você já sabia disso e vai continuar a fazer o que aprendeu aqui.",CHARACTER_PREFEITO2);
					showMessage("Tente sempre passar adiante tudo o que você aprendeu nesse período.",CHARACTER_PREFEITO2);
					showMessage("Parabéns!",CHARACTER_PREFEITO2);
					
					if ( vars.silaspitecoquest >= 2 ){
						setBackground(Constants.BACKGROUND_FINAL_ATTENDANT);
						showMessage("Juca conseguiu estabelecer uma família estável com seus irmãos. Eles, por sua vez, montaram uma escola de skate e estão competindo nos torneios internacionais." );
					}
					
					setBackground(Constants.BACKGROUND_FINAL_POLICEMAN);
					showMessage("O Policial Amaral foi condecorado e continuou seu bom trabalho. Agora as pessoas acreditam em seu trabalho." );
					
					setBackground(Constants.BACKGROUND_FINAL_LITTLEBOY);
					showMessage("Enzo agora está no ensino médio com ótimas notas. Continua curioso e pergunta sobre tudo.");
//					
					if (getQuestStatus(QUEST_CHINA) == QuestState.QUEST_STATE_COMPLETE) {
						setBackground(Constants.BACKGROUND_FINAL_CHINESE);
						showMessage("A lanchonete do Sr. Wong virou parada obrigatória de todos os viajantes que passam perto da Vila Virtual, fazendo-a ficar cada vez mais conhecida.");
					}
//					
					if (getQuestStatus(QUEST_HIPPIE)==QuestState.QUEST_STATE_COMPLETE) {
						setBackground(Constants.BACKGROUND_FINAL_HIPPIE);
						showMessage("A loja de Lamar prosperou, trazendo mais cultura à vila.");	
					}
					
					setBackground(Constants.BACKGROUND_FINAL_BADDIES);
					showMessage( "Graças ao trabalho do delegado Magalhães e do guarda Amaral, Patrício e seu ajudante se reencontraram na cadeia e ninguém mais sabe o que houve com eles." );
					
					setBackground( Constants.BACKGROUND_FINAL_MAYOR );
					showMessage( "E o prefeito Heitor Lopes?" );
					showMessage( "Bem..." );
					showMessage( "Ele foi reeleito e cumpriu mais um mandato." );
					showMessage( "Desta vez, acompanhando todos os problemas de perto." );
					fadeOut();
					
					showMessage( "Você conseguiu " + getGameScene().getPlayerScore() + " dos pontos." );
					showMessage( "Obrigado por jogar! :)");
					backToMainMenu();
					break;
			}
			
			refreshMapIcons();
			
			nextGameEvent();
		} // fim do método prepareToPart()	
		
		
		private function refreshMapIcons() : void {
			setMapIconVisible( Constants.MAP_ANITA, getQuestStatus( QUEST_VELHARUA ) == QuestState.QUEST_STATE_COMPLETE );
			setMapIconVisible( Constants.MAP_LOJA_LAMAR, getQuestStatus( QUEST_HIPPIE ) == QuestState.QUEST_STATE_COMPLETE );
			setMapIconVisible( Constants.MAP_PASTELARIA, part == 3 && getQuestStatus( QUEST_CHINA ) == QuestState.QUEST_STATE_COMPLETE );
			setMapIconVisible( Constants.MAP_CACHORRO_QUENTE, getQuestStatus( QUEST_CHINA ) != QuestState.QUEST_STATE_COMPLETE );
			setMapIconVisible( Constants.MAP_CASA_2, false );
		}
		
		
		private function getQuest( questId : Number ) : Object
		{
			for ( var i : int = 0; i < quests.length; ++i )
			{
				if ( quests[ i ].id == questId )
					return quests[ i ];
			}
			
			trace( "getQuest: id inválido: " + Number );
			
			return null;
		}
		
		public function getGameScene() : VilaVirtual
		{
			return VilaVirtual.GetInstance();
		}
		
		/**
		 * Encerra uma quest.
		 * @questId id da quest a ser encerrada.
		 */
		private function endQuest( questId : Number ) : void
		{
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.endQuest, true, questId );
			queueGameEvent( this, refreshMapIcons, true );
			showMessage( "Missão \"" + getQuest( questId ).title + "\" completa!" );
		}
		
		
		/**
		 * Inicia uma quest.
		 * @questId id da quest a ser iniciada.
		 */
		private function startQuest( questId : Number ) : void
		{
			var vilaVirtual : VilaVirtual = VilaVirtual.GetInstance();
			queueGameEvent( vilaVirtual, vilaVirtual.startQuest, true, questId );
			showMessage( "Nova missão: " + getQuest( questId ).title );
		}
		
		
		private function getCharacter( characterId: int ) : Character
		{
			try {
				return getGameScene().getCharacterById( characterId );
			} catch ( e : Error ) {
				trace( "ERROR on GameScript.getCharacter: characterId #" + characterId + " not found." + "\n" + e );
			}
			return null;
		}
		
		
		public static function get instance():GameScript
		{
			return _instance;
		}
		
		public function get vars():Object
		{
			return _vars;
		}
		
		public function set vars( value : Object ) : void
		{
			// Está confiando que o chamador passará um object com a estrutura correta...
			_vars = Utils.Clone( value );
		}	
		
		public function get quests():Array
		{
			return _quests;
		}
		
		
		public function get characters():Array
		{
			return _characters;
		}
		
		
		public function get items():Array
		{
			return _items;
		}		
		
		
		/**
		 * Define o tile de um personagem.
		 * @param characterId
		 * @param row
		 * @param line
		 */
		private function setCharacterTile( characterId: Number, row : Number, line : Number ) : void
		{
			var c : Character = getCharacter( characterId );
			if( c )
			{
				var tile : ITile = getGameScene().tileMap.getTile( 0, row, line );
				queueGameEvent( tile, tile.insertTileObj, true, c );
				
				if( characterId == CHARACTER_JOGADOR )
				{
					// workaround para o bug da câmera (área visível em torno do tile do jogador ficava inválida após
					// alterar a posição do jogador sem ser movendo-o)
					var vilaVirtual : VilaVirtual = VilaVirtual.GetInstance();
					queueGameEvent( vilaVirtual, vilaVirtual.forceVisibleAreaResetOnUpdate, true );
					queueGameEvent( vilaVirtual, vilaVirtual.onPlayerCharacterTileChanged, true, null );
				}
			}
			else
			{
				trace( "setCharacterTile ERROR: " + characterId + ", " + row + ", " + line );
			}
		}
		
		
		/**
		 * Define o tile de um personagem.
		 * @param characterId
		 * @param portalId
		 * @param line
		 */
		private function setCharacterTileByPortal( characterId : int, portal : Point ) : void
		{
			setCharacterTile( characterId, portal.x, portal.y );
		}
		
		
		/**
		 * Define as falas genéricas de um personagem. Essas falas são usadas quando o jogador fala com um personagem e
		 * não há nenhuma ação definida. A cada vez que o jogador falar com o personagem é mostrado o próximo item da lista,
		 * até chegar ao último, que é repetido até que suas falas sejam reconfiguradas.
		 * @param characterId id do personagem
		 * @param speeches lista de falas do personagem. A lista deve ser um array, ou seja, deve estar entre colchetes, com seus
		 * itens separados por vírgula. Exemplo válido: [ "Alô", "Oi, tudo bom?", "Texto 3", "Esse vai se repetir..." ]
		 */
		private function setCharacterSpeeches( characterId : int, speeches : Array ) : void
		{
			_genericSpeeches[ characterId ] = new Object();
			_genericSpeeches[ characterId ].speeches = speeches;
			_genericSpeeches[ characterId ].currentSpeech = 0;
		}
		
		
		/**
		 * Remove um personagem do cenário.
		 */
		private function removeCharacter( characterId: int ) : void
		{
			var c : Character = getCharacter( characterId );
			if ( c ) {
				queueGameEvent( c, c.removeSelf, true );
			} else {
				trace( "removeCharacter ERROR: " + characterId );
			}			
		}
		
		
		/**
		 * Remove todos os personagens do cenário.
		 */
		private function removeAllCharacters() : void
		{
			var vilaVirtual : VilaVirtual = VilaVirtual.GetInstance();
			queueGameEvent( vilaVirtual, vilaVirtual.removeAllCharacters, true );
		}
		
		
		/**
		 * Move um personagem um tile.
		 * @characterId id do personagem.
		 * @direction direção do movimento.
		 * @autoStep indica se o próximo evento do script será executado automaticamente, ou se irá esperar essa animação se encerrar antes.
		 */
		private function setCharacterMovement( characterId: int, directions : Array, autoStep : Boolean = true ) : void
		{
			var c : Character = getCharacter( characterId );
			
			var movement : Movement;
			var directionsVector : Vector.< Direction > = new Vector.< Direction >();
			for each ( var d : Direction in directions ) {
				directionsVector.push( d );
			}
			movement = new Movement( directionsVector, nextGameEvent );
			queueGameEvent( c, c.setMovement, autoStep, movement );
		}
		
		
		/**
		 * Faz o fade in da tela.
		 * @param time duração da animação, em segundos.
		 * @autoStep indica se o próximo evento do script será executado automaticamente, ou se irá esperar essa animação se encerrar antes.
		 */	
		private function fadeIn( time : Number = Constants.FADE_TIME_DEFAULT, autoStep : Boolean = false, unlockAfterFadeIn : Boolean = false ) : void
		{
			trace( "\n------------------------------------\n" );
			trace( "GameScipt::fadeIn => Agendando fade in" );
			trace( "GameScipt::fadeIn => time = " + time );
			trace( "GameScipt::fadeIn => autoStep = " + autoStep );
			trace( "GameScipt::fadeIn => unlockAfterFadeIn = " + unlockAfterFadeIn );
			trace( "\n------------------------------------\n" );

 			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.fadeScreen, autoStep, true, time, autoStep ? null : nextGameEvent );
			if( unlockAfterFadeIn )
				lockInput( false );
		}
		
		
		/**
		 * Faz o fade out da tela.
		 * @param time duração da animação, em segundos.
		 * @autoStep indica se o próximo evento do script será executado automaticamente, ou se irá esperar essa animação se encerrar antes.
		 */
		private function fadeOut( time : Number = Constants.FADE_TIME_DEFAULT, autoStep : Boolean = false, lockBeforeFadeOut : Boolean = true ) : void
		{
			trace( "\n------------------------------------\n" );
			trace( "GameScipt::fadeOut => Agendando fade out" );
			trace( "GameScipt::fadeOut => time = " + time );
			trace( "GameScipt::fadeOut => autoStep = " + autoStep );
			trace( "GameScipt::fadeOut => lockBeforeFadeOut = " + lockBeforeFadeOut );
			trace( "\n------------------------------------\n" );
			
			var vilaVirtual : VilaVirtual = getGameScene();
			if( lockBeforeFadeOut )
				lockInput( true );
			queueGameEvent( vilaVirtual, vilaVirtual.fadeScreen, autoStep, false, time, autoStep ? null : nextGameEvent );
		}
		
		
		private function addItem( itemId : Number ) : void
		{
			for ( var i : int = 0; i < items.length; ++i ) {
				if ( items[ i ].id == itemId ) {
					var item : Item = new Item( items[ i ] );
					var vilaVirtual : VilaVirtual = getGameScene(); 
					queueGameEvent( vilaVirtual, vilaVirtual.putItemIntoBackpack, true, item ); 
					showMessage( "Você ganhou um item: " + item.name );
					return;
				}
			}
			
			trace( "ERRO em GameScript.addItem: itemId não encontrado( " + itemId + ")" );
		}
		
		
		private function removeItem( itemId : Number ) : void
		{
			var backpack : Backpack = getGameScene().backpack; 
			queueGameEvent( backpack, backpack.removeItem, true, itemId ); 
		}
		
		
		private function hasItem( itemId : Number ) : Boolean
		{
			return getGameScene().backpack.hasItem( itemId );
		}
		
		
		/**
		 * Espera um tempo até a execução do próximo evento. 
		 * @param time tempo de espera, em segundos.
		 */
		private function wait( time : Number ) : void
		{
			var t : Timer = new Timer( int( time * 1000 ), 0 );
			t.addEventListener( TimerEvent.TIMER, waitEnded );
			queueGameEvent( t, t.start, false );
		}
		
		private function waitEnded( event : TimerEvent ) : void
		{
			var timer : Timer = ( event.target as Timer );
			timer.stop();
			timer.removeEventListener( TimerEvent.TIMER, waitEnded );
			timer = null;
			nextGameEvent();
		}
		
		/**
		 * Define a direção de um personagem.
		 * @param characterId id do personagem.
		 * @param direction
		 * @autoStep indica se o próximo evento do script será executado automaticamente, ou se irá esperar essa animação se encerrar antes.
		 */
		private function setCharacterDirection( characterId : Number, direction : Direction ) : void
		{
			var c : Character = getCharacter( characterId );
			queueGameEvent( c, c.setDirection, true, direction );
		}
		
		
		/**
		 * Define a direção de um personagem.
		 * @param characterId id do personagem.
		 * @param direction
		 * @autoStep indica se o próximo evento do script será executado automaticamente, ou se irá esperar essa animação se encerrar antes.
		 */ 
		private function turnCharacter( characterId : Number, direction : Direction ) : void
		{
			var c : Character = getCharacter( characterId );
			queueGameEvent( c, c.turn, true, direction );
		}
		
		/**
		 * Define a expressão de um personagem.
		 * @param characterId id do personagem.
		 * @param expressionId id da expressão.
		 */
		private function setCharacterExpression( characterId : Number, expressionId : int ) : void
		{
			var c : Character = getCharacter( characterId );
			queueGameEvent( c, c.setExpression, true, expressionId );
		}		
		
		
		/**
		 * Mostra uma mensagem na tela.
		 * @param message
		 * @param characterId id do personagem. Valores negativos indicam que nenhum personagem será exibido.
		 * @param answers pares de opções para o jogador responder: primeiro é uma string, indicando a resposta; depois é a ação (ou 
		 * conjunto de ações) executados case o jogador selecione essa opção.
		 */ 
		private function showMessage( message : String, characterId : Number = -1, ...answers ) : void
		{
			var talkerImg : MovieClipVS = null;
			var voiceIndex : uint = Constants.SOUND_FX_SPEECH_MALE;
			
			if ( characterId >= 0 ) {
				talkerImg = getCharacter( characterId ).characterFaceImg;
				
				for ( var i : int = 0; i < _characters.length; ++i ) {
					if ( _characters[ i ].id == characterId ) {
						if ( _characters[ i ].name )
							message = _characters[ i ].name + ": " + message;
						
						if ( _characters[ i ].voice )
							voiceIndex = _characters[ i ].voice;
						break;
					}
				}
			}
			
			// se houver respostas, o próximo evento a ser executado é indicado pelo parâmetro Function de cada resposta possível
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.startTalk, false, message, talkerImg, voiceIndex, Direction.DIRECTION_LEFT, answers.length == 0 ? nextGameEvent : null, answers );
		}
		
		private function showChineseMessage( message : String ) : void
		{
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.startTalk, false, message, null, Constants.SOUND_FX_SPEECH_MALE, Direction.DIRECTION_LEFT, nextGameEvent, null, false );	
		}
		
		/**
		 * Move a câmera para um determinado tile.
		 * @param row coluna do tile.
		 * @param line linha do tile.
		 * @param time (opcional) tempo do movimento da câmera, em segundos. Por padrão, é usado o valor definido em Constants.CAMERA_MOVE_DEFAULT_TIME.
		 * @param autoStep (opcional) indica se o próximo evento do script será executado automaticamente, ou se irá esperar essa animação se encerrar antes. O valor padrão é true.
		 */
		private function moveCamera( row : int, line : int, time : Number = Constants.CAMERA_MOVE_DEFAULT_TIME, autoStep : Boolean = true ) : void
		{
			// TODO moveCamera
		}
		
		
		private function checkPart1Quests() : void {
			if ( vars.part1quests >= 3 ) {
				setCharacterExpression( CHARACTER_POLITICO, Character.EXPRESSION_EXCLAMATION );
			}
		}
		
		
		/**
		 * Toca um som.
		 * @param soundIndex índice do som a ser tocado. Os valores válidos estão definidos na classe Constants.
		 * @param startTime (opcional) a posição inicial, em milissegundos, na qual a reprodução deve começar.
		 * @param loopCount (opcional) número de repetições do som.
		 */
		private function playSound( soundIndex : int, startTime : Number = 0.0, loopCount : int = 1 ) : void
		{
			var s : SoundManager = SoundManager.GetInstance();  
			
			// workaround para impedir que 2 músicas tema toquem simultaneamente
			if ( soundIndex == Constants.SOUND_MUSIC_THEME || soundIndex == Constants.SOUND_FX_ENDING )
				queueGameEvent( s, s.stopAllSounds, true );
				
			queueGameEvent( s, s.playSoundAtIndex, true, soundIndex, startTime, loopCount );
		}
		
		
		/**
		 * Pára a execução de um som. AINDA NÃO IMPLEMENTADO.
		 * @param soundIndex índice do som a ser tocado. Os valores válidos estão definidos na classe Constants.
		 */
		private function stopSound( soundIndex : int ) : void
		{
			// TODO stopSound
		}
		
		
		/**
		 * Obtém o tile atual de um personagem. O valor retornado é um Point, cujo x é a coluna, e y é a linha.
		 * @param characterId id do personagem.
		 */
		private function getCharacterPosition( characterId : int ) : Point
		{
			var c : Character = getCharacter( characterId );
			
			return new Point( c.tile.row, c.tile.column );
		}
		
		private function getOpositeDirection( dir : Direction ) : Direction
		{
			return  Enum.FromIndex( Direction, ( dir.Index + 2 ) & 3 ) as Direction;	
		}
		
		/**
		 * Obtém a direção para onde um personagem está virado atualmente.
		 * @param characterId id do personagem.
		 */
		private function getCharacterDirection( characterId : int ) : Direction
		{
			var c : Character = getCharacter( characterId );
			return c.direction();
		}		
		
		
		/**
		 * Obtém o estado de uma quest. Os valores válidos de resposta estão definidos na classe QuestState.
		 * @param questId id da quest.
		 */
		private function getQuestStatus( questId : int ) : QuestState
		{
			return getGameScene().queryQuest( questId );
		}
		
		
		/**
		 * Bloqueia ou desbloqueia a entrada de eventos do jogador.
		 * @param locked 
		 */
		private function lockInput( locked : Boolean ) : void
		{
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.lockInput, true, locked );
		}
		
		
		/**
		 * Agenda uma volta ao menu principal.
		 */
		private function backToMainMenu() : void
		{
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.backToMainMenu, false );
		}
		
		
		/**
		 * Executa o próximo evento do jogo.
		 */
		
		private var nextGameEventScheduler : ScheduledTask = null;
		
		public function nextGameEvent( e : Event = null ) : void
		{
			// TODO : Somente para teste !!!
//			if( nextGameEventScheduler == null )
//			{
//				// Garante que vamos conseguir terminar a path de execução atual sem que um novo evento interrompa
//				nextGameEventScheduler = new ScheduledTask( 0.05, onNextGameEvent );
//				nextGameEventScheduler.call();
//			}
//			else
//			{
//				throw ArgumentError( "Chamou nextGameEvent duas vezes consecutivas" );
//			}
			
			onNextGameEvent();
		}
		
		private function onNextGameEvent() : void
		{
			nextGameEventScheduler = null;

			// remove o primeiro elemento da fila
			if( _gameEvents.length > 0 )
			{
				var nextEvent : GameEvent = _gameEvents.shift();
				
				trace( new Date() + " -> GameScript.nextGameEvent[" + _gameEvents.length + "]: " + nextEvent );
				
				if ( nextEvent )
				{
					// executa o evento, chamando o próprio método nextGameEvent após acabar a execução
					nextEvent.run( nextGameEvent );
				}
			}
		}
		
		
		/**
		* Adiciona um evento do jogo à fila de execução.
		* @param object objeto cujo método será chamado (pode ser null)
		* @param gameEvent função a ser chamada
		* @param autoStep indica se o método nextEvent deve ser chamado automaticamente
		* @param ...params parâmetros da função chamada (pode ser null)
		*/
		private function queueGameEvent( object : Object, gameEvent : Function, autoStep : Boolean, ...params ) : void
		{
			_gameEvents.push( new GameEvent( object, gameEvent, params, autoStep ) );
			trace( "GameScript.queueGameEvent[" + _gameEvents.length + "]: " + object + ", " + gameEvent + ", " + autoStep + ", " + params );
		}
		
		
		/**
		* Troca o fundo de tela. Os valores válidos estão definidos na classe Contants.
		* @param bkg
		* @param fadeScreen indica se já deve fazer a animação de fade out/in automaticamente. O padrão é true.
		*/
		private function setBackground( bkg : String, fadeScreen : Boolean = true, manageInputLocks : Boolean = false ) : void
		{
			setBackgroundEx( bkg, fadeScreen, fadeScreen, manageInputLocks );
		}
		
		/**
		* Troca o fundo de tela. Os valores válidos estão definidos na classe Contants.
		* @param bkg
		* @param fadeScreen indica se já deve fazer a animação de fade out/in automaticamente. O padrão é true.
		*/
		private function setBackgroundEx( bkg : String, fadeOutScreen : Boolean, fadeInScreen : Boolean, manageInputLocks : Boolean = false ) : void
		{
			trace( "\n------------------------------------\n" );
			trace( "GameScipt::setBackground => Agendando set background" );
			trace( "GameScipt::setBackground => bkg = " + bkg );
			trace( "GameScipt::setBackground => fadeOutScreen = " + fadeOutScreen );
			trace( "GameScipt::setBackground => fadeInScreen = " + fadeInScreen );
			trace( "GameScipt::setBackground => manageInputLocks = " + manageInputLocks );
			trace( "\n------------------------------------\n" );
			
			if( manageInputLocks )
				lockInput( true );

			if( fadeOutScreen )
				fadeOut();
			
			var vilaVirtual : VilaVirtual = getGameScene();
			queueGameEvent( vilaVirtual, vilaVirtual.setBackground, true, bkg );
			
			if( fadeInScreen )
				fadeIn();
			
			if( manageInputLocks )
				lockInput( false );
		}
		
		
		private function endOngQuest() : void {
// Faltaram 2...
			if( vars.contadorong <= 3 )
			{
				showMessage( "Você se esforçou bastante. Mas eu entendo, é muito difícil resolver todos os problemas da Vila Virtual.", CHARACTER_DELEGADO );
				showMessage( "Mesmo assim, repito, parabéns!", CHARACTER_DELEGADO );
			}
			// Faltou somente um
			else if( vars.contadorong == 4 )
			{
				showMessage( "Nossa! Você realmente se saiu bem! Praticamente todos os meninos voltaram a estudar! Eu diria:", CHARACTER_DELEGADO );
				showMessage( "FANTÁSTICO!!!!!!!!", CHARACTER_DELEGADO );
			}
			// Completou tudo
			else // if( vars.contadorong >= 4 )
			{
				showMessage( "Grande jovem! Você é brilhante! Melhor seria impossível! Todos os meninos voltaram a estudar! Eu diria:", CHARACTER_DELEGADO );
				showMessage( "E-S-P-E-T-A-C-U-L-A-R!!!!!!!!!!!!!!!", CHARACTER_DELEGADO );
				endQuest( QUEST_ONG );
			}
			
			queueNextPart( PARTE_FINAL, Constants.TILEMAP_DONT_CHANGE );				
		}
		
		/**
		 * 
		 */
		private function setTileMap( tileMapIndex : int, onDone : Function, ...params ) : Boolean
		{
			
//			lockInput( true );
//			
//			fadeOut();
//			
			//var vilaVirtual : VilaVirtual = getGameScene();
			//vilaVirtual.fadeScreen( false );
			return getGameScene().setTileMap( tileMapIndex, onDone, params );
//			queueGameEvent( vilaVirtual, vilaVirtual.setTileMap, true, tileMapIndex );
//			
//			fadeIn();
//			
//			lockInput( false );
		}
		
	}
}
