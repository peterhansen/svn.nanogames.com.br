package
{
	public final class GameEvent
	{
		private var _object : Object;
		private var _event : Function;
		private var _params : *;
		private var _autoStep : Boolean;	
		
		public function GameEvent( object : Object, gameEvent : Function, params : *, autoStep : Boolean = true ) : void
		{
			_object = object;
			_event = gameEvent;
			_params = params;
			_autoStep = autoStep;
		}
		
		public function get event():Function
		{
			return _event;
		}

		public function get params():*
		{
			return _params;
		}

		public function get autoStep():Boolean
		{
			return _autoStep;
		}
		
		
		public function run( onEnd : Function ) : void
		{
			trace( "GameEvent.run:\n\tobject: " + _object + "\n\tevent: " + _event + "\n\tparams: " + _params );
			
			// Deve-se usar apply() em vez de call() porque o número de parâmetros é arbitrário
			_event.apply( _object, _params );
			
			if ( autoStep && onEnd != null )
				onEnd();
		}

	}
}