package
{
	import fl.controls.listClasses.CellRenderer;
	import fl.controls.listClasses.ICellRenderer;
	import fl.controls.listClasses.ListData;
	
	import flash.text.AntiAliasType;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	// http://blogs.adobe.com/pdehaan/2007/06/alternating_background_colors.html
	// http://blogs.adobe.com/pdehaan/2007/06/setting_a_flash_data_grids_bac_1.html
	// http://forums.adobe.com/thread/167048?decorator=print&displayFullThread=true
	// http://www.actionscript.org/resources/articles/887/1/Change-text-formating-of-cells-in-DataGrid/Page1.html
	// http://www.adobe.ca/devnet/flash/quickstart/datagrid_pt2/
	// http://www.adobe.com/livedocs/flash/9.0/ActionScriptLangRefV3/fl/controls/SelectableList.html#setRendererStyle%28%29

	/**
	* This class sets the upSkin style based on the current item's index in a list
	* Make sure the class is marked "public" and, in the case of our custom cell renderer,
	* extends the CellRenderer class and implements the ICellRenderer interface
	*/
	public class DataGridAlternatingRowColors extends CellRenderer implements ICellRenderer
	{
		/**
		* Constructor
		*/
		public function DataGridAlternatingRowColors() : void
		{
			super();
		}
		
		/**
		* This method returns the style definition object from the CellRenderer class
		*/
		public static function getStyleDefinition() : Object
		{
			return CellRenderer.getStyleDefinition();
		}
		
		/**
		* Modifica a fonte das entradas da tabela 
		*/		
		override public function set listData( value : ListData ) : void
		{
			super.listData = value
			setStyle( "textFormat", getCellTextFormat() );
			setStyle( "embedFonts", true );
		}
		
		/**
		* Retorna o formatador de texto deste formatador de células 
		* @return O formatador de texto deste formatador de células
		*/
		protected function getCellTextFormat() : TextFormat
		{
			// TODO : laCartoonerie não possui algumas letras, como: ú, í, õ, ...
			//var fontToUse : FontLaCartoonerie = new FontLaCartoonerie();
			var fontToUse : Tahoma = new Tahoma();
			var txtFormat : TextFormat = new TextFormat();
			txtFormat.font = fontToUse.fontName;
			txtFormat.size = 16;
			txtFormat.color = 0xB95C00;
			txtFormat.bold = true;
			return txtFormat;
		}
		
		/**
		* Modifica a qualidade do texto 
		*/		
		override protected function drawLayout() : void
		{
			textField.antiAliasType = AntiAliasType.ADVANCED;
			super.drawLayout();
		}
		
		/**
		* This method overrides the inherited drawBackground() method and sets the renderer's
		* upSkin style based on the row's current index. For example, if the row index is an
		* odd number, the upSkin style is set to the CellRenderer_upSkinDarkGray linkage in the
		* library. If the row index is an even number, the upSkin style is set to the
		* CellRenderer_upSkinGray linkage in the library
		*/
		override protected function drawBackground() : void
		{
			var aux : Class = ( ( _listData.index & 1 ) == 0 ? CellRenderer_upSkinDark : CellRenderer_upSkinLight );
			setStyle( "upSkin", aux );
			setStyle( "downSkin", aux );
			setStyle( "overSkin", aux );
			setStyle( "disabledSkin", aux );
			setStyle( "selectedUpSkin", aux );
			setStyle( "selectedDownSkin", aux );
			setStyle( "selectedOverSkin", aux );
			setStyle( "selectedDisabledSkin", aux );
			
			super.drawBackground();
		}
	}
}
