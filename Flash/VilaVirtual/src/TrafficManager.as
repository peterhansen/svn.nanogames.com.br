package
{
	import flash.geom.Point;
	import flash.utils.getTimer;

	public final class TrafficManager
	{
		private static const TOTAL_CARS : uint = 5;
		
		private static const POINT_START : Point = new Point( 1, 122 );
		private static const POINT_END : Point = new Point( 123, 122 );
		
		private const distances : Array = [ -0.0845, -0.15554, -0.29888, -0.44444, -0.63977 ];
		
		private var cars : Array;
		
		private var accTime : Number = 0;
		
		private static const TRAVERSE_TIME : Number = 20.0;
		
		
		public function TrafficManager()
		{
			cars = new Array();
			
			// MEGA macetado
			cars[ 0 ] = new Car1Base();
			cars[ 1 ] = new Car2Base();
			cars[ 2 ] = new Car3Base();
			cars[ 3 ] = new Car4Base();
			cars[ 4 ] = new TaxiBase();
//			for ( var i : int = 0; i < TOTAL_CARS; ++i ) {
////				cars[ i ] = new Car( -1, new Car1Base() );
//				cars[ i ] = new Car1Base();
//			}
		}
		
		
		public function insertCars( tileMap : TiledEditorTileMap ) : void
		{
			for ( var i : int = 0; i < TOTAL_CARS; ++i ) {
				tileMap.addChild( cars[ i ] );
			}
		}
		
		public function update( time : Number ) : void
		{
			accTime += time;
//			if ( accTime >= TRAVERSE_TIME )
//				accTime = 0;
			
			const percent : Number = accTime / TRAVERSE_TIME;
			
			// a porcentagem passa de 1.0 para deixar um intervalo
			if ( percent >= 1.8 )
				accTime = 0;

			var vila : VilaVirtual = VilaVirtual.GetInstance();
			var tileStart : ITile = vila.tileMap.getTile( 0, POINT_START.x, POINT_START.y );
			var tileEnd : ITile = vila.tileMap.getTile( 0, POINT_END.x, POINT_END.y );
			
			var tileScreenPosStart : PixelPos = TileGameUtils.GetDisplayObjPosAtDisplayObj( ( tileStart as TiledEditorTile ), ( tileStart.tileLayer.tileMap as TiledEditorTileMap ) );
			var tileScreenPosEnd : PixelPos = TileGameUtils.GetDisplayObjPosAtDisplayObj( ( tileEnd as TiledEditorTile ), ( tileStart.tileLayer.tileMap as TiledEditorTileMap ) );
			
			const DIFF_X : Number = tileScreenPosEnd.x - tileScreenPosStart.x;
			const DIFF_Y : Number = tileScreenPosEnd.y - tileScreenPosStart.y;
			
			for ( var i : int = 0; i < TOTAL_CARS; ++i ) {
				cars[ i ].x = tileScreenPosStart.x + ( percent + distances[ i ] ) * DIFF_X;
				cars[ i ].y = tileScreenPosStart.y + ( percent + distances[ i ] ) * DIFF_Y;
			}
		}
	}
}