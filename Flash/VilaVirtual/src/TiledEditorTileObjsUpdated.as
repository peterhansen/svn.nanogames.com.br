package
{
	import flash.events.Event;
	
	public class TiledEditorTileObjsUpdated extends Event
	{
		public static const OBJECT_ADDED : String = "tileObjAdded";
		public static const OBJECT_REMOVED : String = "tileObjRemoved";
		public static const OBJECTS_Z_SORTED : String = "tileObjsZSorted";

		private var _tile : ITile;
		private var _tileObj : ITileObject;

		public function TiledEditorTileObjsUpdated( type : String, t : ITile, o : ITileObject )
		{
			super( type, false, false );
			
			_tile = t;
			_tileObj = o;
		}
		
		public function get tile() : ITile
		{
			return _tile;
		}
		
		public function get tileObject() : ITileObject
		{
			return _tileObj;
		}
	}
}
