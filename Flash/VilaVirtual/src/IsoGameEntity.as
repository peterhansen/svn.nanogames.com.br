package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.geom.Vector3D;

	public class IsoGameEntity extends MovieClipVS implements ITileObject
	{
		// Identificadores de eventos específicos da classe
		public static const ISO_GAME_ENTITY_POS_CHANGED : String = "isoGameEntityPosChanged";
		public static const ISO_GAME_ENTITY_TILE_CHANGED : String = "isoGameEntityTileChanged";
		
		// Coordenada Y da entidade no mundo isométrico. Precisamos ter uma das coordenadas armazenadas para conseguirmos fazer as conversões
		// de isométrico para pixel através dos métodos auxiliares de IsoGlobals. Escolhemos Y pois a maior parte dos objetos não sairá do chão
		// (ou seja, y = 0 )
		private var _worldY : Number;
		
		// Tile no qual o objeto está situado
		private var _tile : ITile;
		private var _lastTile : ITile;
 
		/**
		* Posição de referência através do qual o objeto é posicionado
		*/		
		private var _anchor : Point;

		/**
		* Indica se o objeto deve sofrer atualizações ou ou não
		*/		
		private var _dynamic : Boolean;
		
		// OLD : Optamos por não possuir vários objetos por tile nesta versão dos componentes. Logo, não
		// precisaremos de áreas de colisão
//
//		// Volume de colisão da entidade. Não podemos utilizar o bounding rect por causa da 
//		// perspectiva isométrica. A propriedade "hitArea" fornece a possibilidade de 
//		// criar uma área de colisão separada, mas essa seria sempre retangular. Além
//		// do mais, "hitArea" utiliza uma estrutura muita complexa (um Sprite) para
//		// fazer um trabalho muito simples
//		private var _boundingVolume : Circle;
//		
//		// Utilizado para podermos renderizar a área de colisão
//		private var _drawableCollisionArea : Shape;
		
		/**
		* Construtor
		*/		
		function IsoGameEntity( isDynamic : Boolean = false )
		{
			super();
			
			_worldY = 0.0;
			
			_tile = null;
			_lastTile = null;
			
			_anchor = new Point();
			
			_dynamic = isDynamic;
			
			// OLD : Optamos por não possuir vários objetos por tile nesta versão dos componentes. Logo, não
			// precisaremos de áreas de colisão
//			_boundingVolume = null;
//			_drawableCollisionArea = new Shape();
//			addChild( _drawableCollisionArea );
		}
		
		/**
		* Cria um IsoGameEntity a partir de um Sprite
		* @param sprite O sprite que servirá como base deste IsoGameEntity
		* @return O novo IsoGameEntity 
		*/		
		public static function FromSprite( sprite : Sprite ) : IsoGameEntity
		{
			const ret : IsoGameEntity = new IsoGameEntity();			
			ret.addChild( sprite );
			ret.width = sprite.width;
			ret.height = sprite.height;
			ret.visible = sprite.visible;
			
			sprite.x = 0.0;
			sprite.y = 0.0;
			
			return ret;
		}
		
		
		public function setTile( t : ITile ) : void
		{
			tile = t;
		}
		
		
		/**
		* Determina o tile no qual o objeto está  
		* @param t Tile a qual o objeto deve pertencer
		*/		
		public function set tile( t : ITile ) : void
		{
			if( t == _tile )
				return;
			
			if( _tile != null )
				_lastTile = _tile;
			
			_tile = t;
			
			if( ( _tile != null ) && ( _lastTile != null ) && ( _lastTile != _tile ) )
			{
				// Conserta a posição da entidade, pois a posição é relativa à coleção a que ele pertence,
				// e não à tela
				var moveDirection : Vector3D;
				const pathType : TileMapTilePath = TileGameUtils.GetTilePathType( _lastTile, _tile );
				switch( pathType )
				{
					case TileMapTilePath.TILE_PATH_UP_DOWN:
						moveDirection = IsoGlobals.WORLD_UP_VECTOR;
						break;
					
					case TileMapTilePath.TILE_PATH_DOWN_UP:
						moveDirection = IsoGlobals.WORLD_DOWN_VECTOR;
						break;
					
					case TileMapTilePath.TILE_PATH_RIGHT_LEFT:
						moveDirection = IsoGlobals.WORLD_RIGHT_VECTOR;
						break;
					
					case TileMapTilePath.TILE_PATH_LEFT_RIGHT:
						moveDirection = IsoGlobals.WORLD_LEFT_VECTOR;
						break;
					
					// Não precisamos declarar estes cases, pois as condições do if que envolve este comando switch
					// já garante que não iremos receber tais valores
					//case TileMapTilePath.TILE_PATH_NO_PATH:
					//case TileMapTilePath.TILE_PATH_SAME_TILE:
					//default:
					//	break;
				}
				moveDirection.scaleBy( IsoGlobals.TILE_IMG_ISO_WIDTH_IN_PIXELS );
			
				// TODO : Precisa disso aqui?
//				super.x = super.x + moveDirection.x;
//				super.y = super.y + moveDirection.y;
			}
			
			if( _tile != null )
				dispatchEvent( new Event( ISO_GAME_ENTITY_TILE_CHANGED ) );
		}
		
		/**
		* Retorna o tile no qual o objeto está 
		* @return O tile no qual o objeto está
		*/		
		public function get tile() : ITile
		{
			return _tile;
		}
		
		/**
		* Retorna o tile onde a entidade estava antes de ir para o tile atual
		* @return O tile onde a entidade estava antes de ir para o tile atual
		*/		
		public function get lastTile() : ITile
		{
			return _lastTile;
		}
		
		public function set lastTile( t : ITile ) : void
		{
			_lastTile = t;
		}
		
		// Determina a posição x da âncora de posicionamento
		public function set imgAnchorX( newX : int ) : void
		{
			_anchor.x = newX;
		}
		
		// Retorna a posição x da âncora de posicionamento
		public function get imgAnchorX() : int
		{
			return _anchor.x;
		}
		
		// Determina a posição x da âncora de posicionamento
		public function set imgAnchorY( newY : int ) : void
		{
			_anchor.y = newY;
		}
		
		// Retorna a posição y da âncora de posicionamento
		public function get imgAnchorY() : int
		{
			return _anchor.y;
		}
		
		// Retorna a largura da imagem da entidade
		public function get imgWidth() : Number
		{
			return width;
		}
		
		// Retorna a altura da imagem da entidade
		public function get imgHeight() : Number
		{
			return height;
		}
		
		// Retorna true se o objeto deve sofrer atualizações ou false caso contrário
		public function get dynamic() : Boolean
		{
			return _dynamic;
		}
		
		// Determina se o objeto deve sofrer atualizações ou ou não
		public function set dynamic( b : Boolean ) : void
		{
			_dynamic = b;
		}
		
		/**
		* Trata a colisão deste objeto com outras entidades 
		* @param collisionData Informações sobre a colisão sofrida
		*/
		public function handleCollision( collisionData : TileObjectCollisionData ) : void
		{
			// A implementação padrão é vazia
		}
		
		/**
		* Atualiza o objeto
		* @param timeElapsed Tempo transcorrido desde a última atualização
		*/	
		public function update( timeElapsed : Number ) : void
		{
			// A implementação padrão é vazia
		}
		
		// Sobrescreve os setters de x e y pois não queremos que o usuário determine a posição das entidades através destes. Esta tarefa
		// deverá ser realizada pelos médotos das famílias screenPosition e worldPosition. No entanto, se os getters não são bloqueados,
		// então o usuário ainda pode obter a posição das entidades dentro de seus containers pais
		override public function set x( n : Number ) : void
		{
			throw new Error( "Use method inTilePos" );
			//super.x = n;
			
			// OLD: Não chamamos mais este evento aqui, pois quando o usuário alterava
			// as coordenadas x e y em duas linhas separadas, executávamos as rotinas
			// de colisão duas vezes. O usuário deve chamar setPosition ou move
			// para alterar as coordenadas da entidade
			//dispatchEvent( new Event( IsoGameEntity.TILE_OBJ_POS_CHANGED ) );
		}
		
		// Sobrescreve os setters de x e y pois não queremos que o usuário determine a posição das entidades através destes. Esta tarefa
		// deverá ser realizada pelos médotos das famílias tilePosition, screenPosition e worldPosition. No entanto, os getters não são
		// bloqueados, então o usuário ainda pode obter a posição das entidades dentro de seus containers pais
		override public function set y( n : Number ) : void
		{
			throw new Error( "Use method inTilePos" );
			//super.y = n;
			
			// OLD: Não chamamos mais este evento aqui, pois quando o usuário alterava
			// as coordenadas x e y em duas linhas separadas, executávamos as rotinas
			// de colisão duas vezes. O usuário deve chamar setPosition ou move
			// para alterar as coordenadas da entidade
			//dispatchEvent( new Event( IsoGameEntity.TILE_OBJ_POS_CHANGED ) );
		}
		
		/**
		* Determina a posição da entidade dentro de seu tile
		* @param pos A posição da entidade dentro de seu tile
		*/		
		public function set inTilePosition( pos : Point ) : void
		{
			super.x = pos.x;
			super.y = pos.y;
			
			dispatchEvent( new Event( IsoGameEntity.ISO_GAME_ENTITY_POS_CHANGED ) );
		}

		/**
		* Retorna a posição da entidade dentro de seu tile
		* @return A posição da entidade dentro de seu tile
		*/		
		public function get inTilePosition() : Point
		{
			return new Point( super.x, super.y );
		}
		
		/**
		* Movimenta a entidade a partir de sua posição dentro de seu tile
		* @param d O deslocamento a ser aplicado à posição da entidade 
		*/		
		public function moveInTilePosition( d : Point ) : void
		{
			inTilePosition += d;
		}
		
		/**
		* Retorna a posição da entidade, a partir de sua âncora de posicionamento, dentro de seu tile 
		* @return A posição da entidade a partir de sua âncora de posicionamento
		*/		
		public function get inTilePositionByAnchor() : Point
		{
			return inTilePosition.add( _anchor ); 
		}
		
		/**
		* Determina a posição da entidade, a partir de sua âncora de posicionamento, dentro de seu tile 
		* @param pos A posição da entidade a partir de sua âncora de posicionamento
		*/		
		public function set inTilePositionByAnchor( pos : Point ) : void
		{
			inTilePosition = pos.subtract( _anchor );
		}
		
		/**
		* Movimenta a entidade a partir de sua posição dentro de seu tile
		* @param d O deslocamento a ser aplicado à posição da entidade
		*/		
		public function moveInTilePositionByAnchor( d : Point ) : void
		{
			inTilePositionByAnchor += d;
		}

		/**
		* Determina a posição da entidade na tela 
		* @param pos A nova posição da entidade
		*/		
		public function set pixelPos( pos : PixelPos ) : void
		{
			// TODO : Teoricamente este objeto não deveria ter que conhecer TiledEditorTile !!!
			var tileScreenPos : PixelPos;
			if( Constants.TO_SCREEN_CALC == Constants.TO_SCREEN_CALC_RELATIVE_TO_MAP )
			{
				tileScreenPos = TileGameUtils.GetDisplayObjPosAtDisplayObj( ( tile as TiledEditorTile ), ( tile.tileLayer.tileMap as TiledEditorTileMap ) );
				
			}
			else
			{
				tileScreenPos = TileGameUtils.GetDisplayObjScreenPos( ( tile as TiledEditorTile ) );
			}
			inTilePosition = pos.subtract( tileScreenPos );
		}

		/**
		* Retorna a posição da entidade na tela
		* @return A posição da entidade na tela 
		*/		
		public function get pixelPos() : PixelPos
		{
			// TODO : Teoricamente este objeto não deveria ter que conhecer TiledEditorTile nem TiledEditorTileMap!!!

			var tilePosAtTileMap : Point;
			if( Constants.TO_SCREEN_CALC == Constants.TO_SCREEN_CALC_RELATIVE_TO_MAP )
			{
				tilePosAtTileMap = TileGameUtils.GetDisplayObjPosAtDisplayObj( ( tile as TiledEditorTile ), ( tile.tileLayer.tileMap as TiledEditorTileMap ) );
			}
			else
			{
				tilePosAtTileMap = TileGameUtils.GetDisplayObjScreenPos( ( tile as TiledEditorTile ) );
			}
			const temp : Point = tilePosAtTileMap.add( inTilePosition );
			return new PixelPos( temp.x, temp.y );
		}

		/**
		* Movimenta o objeto a partir de sua posição atual
		* @param d Deslocamento a ser aplicado à posição atual
		*/		
		public function movePixelPos( d : PixelPos ) : void
		{
			const temp : Point = pixelPos.add( d );
			pixelPos = new PixelPos( temp.x, temp.y );
		}
		
		/**
		* Posiciona a entidade a partir de sua âncora de posicionamento 
		* @param pos A nova posição
		*/		
		public function set pixelPosByAnchor( pos : PixelPos ) : void
		{
			pixelPos = new PixelPos( pos.x - _anchor.x, pos.y - _anchor.y );
		}
		
		/**
		* Retorna a posição da entidade baseada em sua âncora de posicionamento
		* @return A posição da entidade baseada em sua âncora de posicionamento
		*/		
		public function get pixelPosByAnchor() : PixelPos
		{
			const temp : Point = pixelPos;
			return new PixelPos( temp.x + _anchor.x, temp.y + _anchor.y );
		}
		 
		/**
		* Movimenta a entidade a partir de sua âncora de posicionamento
		* @param d Deslocamento a ser aplicado à entidade
		*/		
		public function movePixelPosByAnchor( d : PixelPos ) : void
		{
			const temp : Point = pixelPosByAnchor.add( d );
			pixelPosByAnchor = new PixelPos( temp.x, temp.y );
		}
		
		/**
		* Retorna a posição da entidade no sistema de coordenadas do mundo
		* @return A posição da entidade no sistema de coordenadas do mundo
		*/		
		public function get worldPos() : WorldPos
		{
			return IsoGlobals.pixelCoordToWorldCoordFixingY( pixelPosByAnchor, _worldY );
		}
		
		/**
		* Determina a posição da entidade no sistema de coordenadas do mundo
		* @param pos Nova posição da entidade
		*/		
		public function set worldPos( pos : WorldPos ) : void
		{
			_worldY = pos.y;
			pixelPosByAnchor = IsoGlobals.worldCoordToPixelCoord( pos );
		}
		
		/**
		* Movimenta o objeto a partir de sua posição atual, dentro de seu container, utilizando o sistema de coordenadas do mundo
		* @param d Deslocamento sofrido pelo objeto
		*/
		public function moveWorldPos( d : WorldPos ) : void
		{
			const temp : Vector3D = worldPos.add( d );
			worldPos = new WorldPos( temp.x, temp.y, temp.z );
		}
		
		/**
		* Evento recebido pelo objeto quando um jogador executa uma ação de checagem sobre o mesmo 
		* @param checkDirection Direção da qual veio a checagem 
		*/		
		public function onCheck( checkDirection : Direction ) : void
		{
			// A implementação padrão é vazia
		}
		
		
		public function removeSelf() : void
		{
			if ( tile ) {
				tile.removeTileObj( this );
				tile = null;
			}
		}
		
		
		// TODO : Se quisermos habilitar vários objetos por tile, teremos que implementar estes métodos
		//// Retorna o volume de colisão da entidade
		//public function get boundingCircle() : Circle
		//{
		//	return _boundingVolume;
		//}
		//		
		//// Determina o volume de colisão da entidade
		//public function set boundingCircle( bounding : Circle ) : void
		//{
		//	_boundingVolume = bounding;
		//	
		//	// TODO : Retirar!!!
		//	const g : Graphics = _drawableCollisionArea.graphics;
		//	g.clear();
		//	g.beginFill( 0xFFFFFF, 0.7 );
		//	g.drawCircle( _boundingVolume.center.x, _boundingVolume.center.y, _boundingVolume.radius );
		//	g.endFill();
		//}
	}
}