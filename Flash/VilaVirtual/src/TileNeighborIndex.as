package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class TileNeighborIndex extends Enum
	{
		// "Construtor" estático
		{ initEnum( TileNeighborIndex ); }
		
		// Índices dos tiles vizinhos. Estes valores devem ser utilizados com os métodos getNeighbors e pushNeighbor
		// OBS: Só estamos utilizando esta enum porque AS 3.0 não permite declararmos constantes em interfaces. Ver
		// o comentário no arquivo ITile
		public static const NEIGHBOR_TOP : TileNeighborIndex 			= new TileNeighborIndex(); // 0
		public static const NEIGHBOR_BOTTOM : TileNeighborIndex			= new TileNeighborIndex(); // 1
		public static const NEIGHBOR_LEFT : TileNeighborIndex 			= new TileNeighborIndex(); // 2
		public static const NEIGHBOR_RIGHT : TileNeighborIndex 			= new TileNeighborIndex(); // 3
		
		public static const NEIGHBOR_TOP_LEFT : TileNeighborIndex 		= new TileNeighborIndex(); // 4
		public static const NEIGHBOR_TOP_RIGHT : TileNeighborIndex 		= new TileNeighborIndex(); // 5
		public static const NEIGHBOR_BOTTOM_LEFT : TileNeighborIndex 	= new TileNeighborIndex(); // 6
		public static const NEIGHBOR_BOTTOM_RIGHT : TileNeighborIndex	= new TileNeighborIndex(); // 7
	}
}
