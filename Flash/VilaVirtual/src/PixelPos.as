package
{
	import flash.geom.Point;
	
	/**
	* 
	* @author Daniel L. Alves
	* 
	*/	
	public final class PixelPos extends Point
	{
		/**
		* Construtor  
		* @param x Coordenada x de uma entidade no sistema de coordenadas da tela
		* @param y Coordenada y de uma entidade no sistema de coordenadas da tela
		*/		
		public function PixelPos( x : Number = 0.0, y : Number = 0.0 )
		{
			super( x, y );
		}
		
		override public function clone() : Point
		{
			return new PixelPos( x, y );
		}
	}
}