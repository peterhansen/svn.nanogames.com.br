package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public final class HappinessBar extends MovieClipVS
	{
		private static const BKG_OUTLINE_WIDTH : Number = 3.0;

		/**
		* Quantidade mínima de felicidade que o jogador pode obter
		*/		
		private var _minHappiness : Number;
		
		/**
		* Quantidade máxima de felidade que o jogador pode obter
		*/		
		private var _maxHappiness : Number;

		/**
		* Quantidade atual de felicidade
		*/		
		private var _currHappiness : Number;

		private var _barBkg : HappinessBackground;		
		private var _barFiller : HappinessBarFiller;
		private var _barIcon : HappinessIcon;

		/**
		* Construtor 
		*/		
		public function HappinessBar( min : Number, max : Number )
		{
			super();
			
			_minHappiness = min;
			_maxHappiness = max;
			_currHappiness = min;

			_barBkg = new HappinessBackground();
			_barFiller = new HappinessBarFiller();
			_barIcon = new HappinessIcon();
			
			addChild( _barBkg );
			addChild( _barFiller );
			addChild( _barIcon );
			
			_barBkg.x = 38.0;
			_barBkg.y = 25.1;
			
			_barFiller.x = 80.15;
			_barFiller.y = 25.25;
			
			_barIcon.x = 0.2;
			_barIcon.y = 0.2;
			
			setHappiness( 0.0 );
		}
		
		private function changeHappinessImg( percent : Number ) : void
		{
			// Coloca percent no intervalo [0.0, 1.0]
			percent = Math.min( Math.max( percent, 0.0 ), 1.0 );
			
			_barFiller.width = ( 1.0 - percent ) * _barBkg.width; 
			_barFiller.x = _barBkg.x + _barBkg.width - _barFiller.width - BKG_OUTLINE_WIDTH;
		}
		
		/**
		* Adiciona um valor, positivo ou negativo, à barra de felicidade
		* @param amount Valor a ser adicionado à barra de felicidade
		*/		
		public function addHappiness( amount : Number ) : void
		{
			setHappiness( getHappiness() + amount );
		}
		
		/**
		* Determina o valor absoluto a ser representado pela barra de felicidade
		* @param value O valor absoluto a ser representado pela barra de felicidade
		*/		
		public function setHappiness( value : Number ) : void
		{
			_currHappiness = value;

			if( _currHappiness < _minHappiness )
				_currHappiness = _minHappiness;
			else if( _currHappiness > _maxHappiness )
				_currHappiness = _maxHappiness;

			changeHappinessImg( getHappinessPercent() );
		}
		
		/**
		* Retorna a quantidade de felicidade acumulada
		* @return a quantidade de felicidade acumulada
		*/		
		public function getHappiness() : Number
		{
			return _currHappiness;
		}
		
		/**
		* Retorna o percentual de felicidade que o jogador já alcançou 
		* @return O percentual de felicidade que o jogador já alcançou
		*/		
		public function getHappinessPercent() : Number
		{
			return ( _currHappiness / _maxHappiness );
		}
	}
}
