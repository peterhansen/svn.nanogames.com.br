package
{
	import fl.controls.DataGrid;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.controls.listClasses.ListData;
	
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class DataGridAlternatingRowColorsRightAlign extends DataGridAlternatingRowColors
	{
		/**
		* Construtor 
		*/		
		public function DataGridAlternatingRowColorsRightAlign()
		{
			super();
		}
		
		/**
		* Retorna o formatador de texto deste formatador de células 
		* @return O formatador de texto deste formatador de células
		*/		
		override protected function getCellTextFormat() : TextFormat
		{
			var txtFormat : TextFormat = super.getCellTextFormat();
			txtFormat.align = TextFormatAlign.RIGHT;
			return txtFormat;
		}
		
		/**
		* Modifica a qualidade do texto 
		*/		
		override protected function drawLayout() : void
		{
			// Se não executarmos essas duas linhas, o textfield fica exatamente do tamanho do texto. Logo, TextFormatAlign.RIGHT
			// não serviria de nada
			textField.width = (( listData.owner as DataGrid ).columns[ listData.column ] as DataGridColumn ).width - 10.0;		
			textField.autoSize = TextFieldAutoSize.NONE;
			
			super.drawLayout();
		}
	}
}