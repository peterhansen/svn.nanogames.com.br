package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class PlayerCharacterType extends Enum
	{
		// "Construtor" estático
		{ initEnum( PlayerCharacterType ); }
		
		// Possíveis estados de movimentação dos objetos não estáticos
		public static const CHARACTER_BOY : PlayerCharacterType		= new PlayerCharacterType();
		public static const CHARACTER_GIRL : PlayerCharacterType	= new PlayerCharacterType();
	}
}