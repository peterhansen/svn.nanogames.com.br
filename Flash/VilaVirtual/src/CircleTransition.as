package
{
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.*;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public final class CircleTransition
	{
		/**
		* O objeto que sofrerá a transição 
		*/		
		private var _target : DisplayObject;
		
		/**
		* A duração da transição em segundos
		*/		
		private var _duration : Number;
		
		/**
		* Raio do círculo da transição 
		*/		
		private var _radius : Number;
		
		/**
		* Callback do usuário chamada quando a transição termina 
		*/		
		private var _onDoneCallback : Function;
		
		/**
		* Parâmetros que devem ser passados para a callback do usuário 
		*/		
		private var _onDoneCallbackParams : *;
		
		/**
		* Objeto dono (this) do método _onDoneCallback ou <code>null</code> caso _onDoneCallback seja um método estático 
		*/		
		private var _onDoneCallbackOwner : *;
		
		/**
		* Indica se a transição deve fechar ou abrir o círculo sobre o alvo 
		*/		
		private var _closing : Boolean;

		/**
		* Objeto responsável pela atualização da transição 
		*/		
		private var _tweener : Tween;
		
		/**
		* Objeto responsável pela atualização da transição (Tween às vezes trava, parando de atualizar a tela e não enviando TweenEvent.MOTION_FINISH)
		*/		
		private var _timerTweener : Timer;
		
		/**
		* O quanto do raio é modificado a cada iteração do timer 
		*/		
		private var _timerStep : Number;
		
		private var _timerEndValue : Number;
		
		private var _timerBeginValue : Number;
		
		/**
		* Estratégia de animação da transição 
		*/		
		private const _strategy : uint = 2;
		
		/**
		* Timer para verificar se _tweener travou. Se travou, termina a transição na marra 
		*/		
		private var _tweenerBreaker : Timer;
		
		/**
		* Frame rate da aplicação 
		*/		
		private var _appFps : uint;

		/**
		* Cosntrutor 
		* @param target O objeto que sofrerá a transição
		* @param duration A duração da transição em segundos
		* @param appFps O frame rate da aplicação
		* @param <code>true</code> se a animação deve ser um círculo fechando, <code>false</code> se deve ser um círculo abrindo
		*/		
		public function CircleTransition( transTarget : DisplayObject, transDuration : Number, appFps : uint, closing : Boolean = true )
		{
			_target = transTarget;
			_duration = transDuration;
			_radius = 0.0;
			_closing = closing;
			_appFps = appFps;

			_tweener = null;
			
			_timerStep = 0.0;
			_timerEndValue = 0.0;
			_timerBeginValue = 0.0;
			_timerTweener = null;
			
			_tweenerBreaker = null;
			
			_onDoneCallbackOwner = null;
			_onDoneCallback = null;
			_onDoneCallbackParams = null;
		}
		
		/**
		* Retorna o raio do círculo necessário para envolver o alvo da transição 
		* @return O raio do círculo necessário para envolver o alvo da transição
		*/		
		private function calcBoundingCircleRadius() : Number
		{
			// O raio será igual à metade da diagonal do retângulo
			var w : Number = 0.0;
			var h : Number = 0.0;
			if( _target != null )
			{
				w = _target.stage.stageWidth;
				h = _target.stage.stageHeight;
			}
			return Math.sqrt( ( w * w ) + ( h * h ) ) * 0.5;
		}
		
		/**
		* Inicia a transição 
		*/				
		public function start( startDelay : Number, callbackOwner : *, onDoneCallback : Function, ...callbackParams ) : void
		{
			_onDoneCallbackOwner = callbackOwner;
			_onDoneCallback = onDoneCallback;
			_onDoneCallbackParams = callbackParams;
			
			switch( _strategy )
			{
				case 2:
					// 100% a mais do tempo total da transição
					_tweenerBreaker = new Timer( ( _duration * 2 ) * 1000, 1 );
					_tweenerBreaker.addEventListener( TimerEvent.TIMER, breaker );
					_tweenerBreaker.start();
					// Sem break mesmo

				case 0:
					if( _closing )
					{
						_timerEndValue = 0.0;
						_tweener = TweenManager.tween( this, "radius", Regular.easeInOut, calcBoundingCircleRadius(), _timerEndValue, _duration );
					}
					else
					{
						_timerEndValue = calcBoundingCircleRadius();
						_tweener = TweenManager.tween( this, "radius", Regular.easeInOut, 0.0, _timerEndValue, _duration );
					}
		
					_tweener.addEventListener( TweenEvent.MOTION_FINISH, onTransitionEnded );
					_tweener.start();
					break;
				
				case 1:
					var millis : Number = ( _duration / _appFps ) * 1000;
					_timerTweener = new Timer( millis );
					_timerStep = calcBoundingCircleRadius() / _appFps;
					
					if( _closing )
					{
						_timerEndValue = 0.0;
						_timerBeginValue = calcBoundingCircleRadius();
						_timerTweener.addEventListener( TimerEvent.TIMER, onCloseStep );
					}
					else
					{
						_timerBeginValue = 0.0;
						_timerEndValue = calcBoundingCircleRadius();
						_timerTweener.addEventListener( TimerEvent.TIMER, onOpenStep );
					}
					
					radius = _timerBeginValue;
					
					_timerTweener.start();
					break;
			}
		}
		
		private function onCloseStep( e : TimerEvent ) : void
		{
			if( radius < _timerEndValue )
			{
				onTransitionEnded( null );
			}
			else
			{
				radius -= _timerStep;
			}
		}
		
		private function onOpenStep( e : TimerEvent ) : void
		{
			if( radius > _timerEndValue )
			{
				onTransitionEnded( null );
			}
			else
			{
				radius += _timerStep;
			}
		}
		
		private function breaker( e : TimerEvent ) : void
		{
			radius = _timerEndValue;
			onTransitionEnded( null );
		}
		
		/**
		* Callback chamada quando a transição termina 
		*/		
		private function onTransitionEnded( e : TweenEvent ) : void
		{
			switch( _strategy )
			{
				case 2:
					_tweenerBreaker.stop();
					_tweenerBreaker.removeEventListener( TimerEvent.TIMER, breaker );
					_tweenerBreaker = null;
					// Sem break mesmo

				case 0:
					_tweener.stop();
					_tweener.removeEventListener( TweenEvent.MOTION_FINISH, onTransitionEnded );
					_tweener = null;
					break;
					
				case 1:
					_timerTweener.stop();
		
					if( _closing )
						_timerTweener.removeEventListener( TimerEvent.TIMER, onCloseStep );
					else
						_timerTweener.removeEventListener( TimerEvent.TIMER, onOpenStep );
		
					_timerTweener = null;
					break;
			}
			
			// OLD
			//_target.mask = null;

			if( _onDoneCallback != null )  
				_onDoneCallback.apply( _onDoneCallbackOwner, _onDoneCallbackParams );	
		}

		public function set radius( r : Number ) : void
		{
			_radius = r;

			var shape : Shape = new Shape();
			var g : Graphics = shape.graphics;
			g.beginFill( 0xFF0000, 1.0 );
			g.drawCircle( 0.0, 0.0, ( _radius < 0.0 ? 0.0 : _radius ) );
			g.endFill();
			
			if( _target != null )
			{
				_target.mask = shape;
				shape.x = _target.stage.stageWidth * 0.5;
				shape.y = _target.stage.stageHeight * 0.5;
			}
			
			trace( "Transition circle radius = " + _radius );
		}

		public function get radius() : Number
		{
			return _radius;
		}
		
		/**
		* Retorna o objeto que manipulado pela transição
		* @return O objeto que manipulado pela transição
		*/		
		public function getTarget() : DisplayObject
		{
			return _target;
		}
	}
}