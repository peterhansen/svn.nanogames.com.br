package
{
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoOnline.NOTextsIndexes;
	import NGC.ScheduledTask;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public final class SceneRecyclingEx extends AppScene implements INOListener
	{
		/**
		* Delay aplicado à _scheduledMinigameEnd
		*/		
		private static const DELAY_BEFORE_END : int = 100;
		
		/**
		* O jogo propriamente dito 
		*/		
		private var _minigameRecycling : SceneRecycling;
		
		/**
		* Indica se estamos jogando para conseguir uma nova pontuação para o NanoOnline ou se estamos jogando no modo história  
		*/		
		private var _rankingModeOn : Boolean;
		
		/**
		* Timer responsável por encerrar os minigames. Assim garantimos que não estaremos executando código do minigame quando este for encerrado 
		*/		
		private var _scheduledMinigameEnd : ScheduledTask;
		
		/**
		* Callback a ser chamada quando o jogo terminar 
		*/		
		private var _onEndCallback : Function;
		
		/**
		* Objeto que faz a integração com o NanoOnline 
		*/		
		private var _noRnk : NORanking;
		
		/**
		* Perfil que está logado ou null caso o usuário esteja jogando sem fazer login/cadastrar-se 
		*/		
		private var _loggedCustomer : NOCustomer;
		
		/**
		* Indica se o usuário zerou o jogo. Quando _rankingModeOn for <code>true</code>, esta variável será sempre <code>false</code>
		*/		
		private var _finishedGame : Boolean;
		
		/**
		* Construtor 
		*/
		public function SceneRecyclingEx( customer : NOCustomer, rankingModeOn : Boolean, onEndCallback : Function, useTransition : Boolean  = false )
		{			
			super( new SceneRecycling(), true, useTransition, false );

			_minigameRecycling = ( innerScene as SceneRecycling );
			_minigameRecycling.stop();
			
			// Garante que as classes instanciadas por ClassDefinitionByName vão existir
			metal_atum;metal_cano;metal_coca;metal_oleo;metal_prego;
			organico_banana;organico_folha;organico_maca;organico_peixe;
			organico_pernil;papel_desenho;papel_envelope;papel_jornal;
			papel_papelao;papel_revista;pilha_1;pilha_2;pilha_3;pilha_4;
			plastico_biscoito;plastico_canudo;papel_copo;plastico_pet;
			plastico_sacola;plastico_sorvete;vidro_casco;vidro_casco_2;
			vidro_copo;vidro_copo_2;vidro_vinho;composto_iogurte;
			composto_coca;composto_copo;composto_geleia;

			_rankingModeOn = rankingModeOn;
			_onEndCallback = onEndCallback;
			_loggedCustomer = customer;
			_finishedGame = false;
			
			// OLD: Só não iremos submeter os recordes 
			//if( _rankingModeOn && ( _loggedCustomer == null ) )
			//	throw new ArgumentError( "Cannot run with ranking mode on without a logged customer" );
			
			_noRnk = new NORanking( Constants.NO_RNK_TYPE_RECYCLING, Constants.NO_RNK_TYPE_RECYCLING_MAX_POINTS );
			
			addEventListener( Event.ADDED, onMinigameAdded );
		}
		
		/**
		 * Callback chamada assim que o minigame é adicionado à displaylist da aplicação 
		 * @param e O objeto que encapsula os dados do evento 
		 */		
		private function onMinigameAdded( e : Event ) : void
		{
			removeEventListener( Event.ADDED, onMinigameAdded );
			
			// Avisa ao pai que acabamos de carregar a cena
			if( asynchronousLoading )
				dispatchEvent( new Event( AppScene.APPSCENE_LOADING_COMPLETE ) );
			
			_minigameRecycling.run( onMinigameRunning );
		}
		
		/**
		 * Callback chamada assim que o minigame começa a executar 
		 * @param minigame O minigame que começou a executar
		 */		
		private function onMinigameRunning( minigame : SceneRecycling ) : void
		{
			_minigameRecycling.setSairCallback( onScheduleMinigameDengueEnd );
			_minigameRecycling.setIsStoryMode( !_rankingModeOn );
			
			if( _rankingModeOn )
			{
				_minigameRecycling.setCheckAndSubmitRecordsCallback( onMinigameGameOver );
			}
			else
			{
				_minigameRecycling.setGameStateNotifierCallback( onMinigameChangedState );
			}
		}
		
		/**
		 * Callback chamada toda vez que o minigame muda de estado
		 * @param newState O estado no qual o jogo acabou de entrar 
		 */		
		private function onMinigameChangedState( newState : int ) : void
		{
			if( newState == jogo.GAMESTATE_WIN )
				_finishedGame = true;
		}
		
		/**
		 * Callback chamada quando o usuário morre 
		 * @param score A pontuação alcançada pelo usuário 
		 */		
		private function onMinigameGameOver( score : int ) : Boolean
		{
			if( !_rankingModeOn || ( score <= 0 ) || ( _loggedCustomer == null ) )
			{
				resetGame();
			}
			else
			{
				_noRnk.clear();
				
				var entriesToSubmit : Vector.< NORankingEntry > = new Vector.<NORankingEntry >();
				entriesToSubmit.push( new NORankingEntry( _loggedCustomer.profileId, score, _loggedCustomer.nickname ) );
				_noRnk.localEntries = entriesToSubmit;
				
				// Submete a pontuação para o NanoOnline
				if( !NORanking.SendSubmitRequest( _noRnk, this ) )
				{
					( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), resetGame );
				}
			}
			return false;
		}
		
		/**
		* Manda o jogo de volta para a sua tela inicial 
		*/		
		private function resetGame() : void
		{
			// Vazia
		}
		
		/**
		* Retorna se o usuário zerou o jogo. Quando _rankingModeOn for <code>true</code>, esta variável será sempre <code>false</code>
		* @return <code>true</code> se o usuário zerou o jogo, <code>false</code> caso contrário
		*/		
		public function getFinishedGame() : Boolean
		{
			return _finishedGame;
		}
		
		/**
		* Retorna a pontuação feita pelo jogador 
		* @return A pontuação feita pelo jogador
		*/		
		public function getScore() : int
		{
			return _minigameRecycling.getScore();
		}
		
		/**
		* Callback chamada sempre que o minigame deva ser finalizado 
		*/		
		private function onScheduleMinigameDengueEnd() : void
		{
			_minigameRecycling.setSairCallback( null );
			_minigameRecycling.setGameStateNotifierCallback( null );
			
			_scheduledMinigameEnd = new ScheduledTask( DELAY_BEFORE_END, _onEndCallback );
			_scheduledMinigameEnd.call();
		}
		
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		public function onNOSuccessfulResponse():void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_RECORDS_SUBMITTED ),
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), resetGame );
		}
		
		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_SUBMIT_RECORD )+ ":\n" + errorStr,
																	 NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), resetGame );
		}
		
		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/	
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}