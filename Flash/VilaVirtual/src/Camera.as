package
{
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.*;
	
	import flash.display.BlendMode;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;

	/**
	* 
	* @author Daniel L. Alves
	* 
	*/	
	public final class Camera extends EventDispatcher
	{
		public static const CAMERA_ANIM_MODE_MANUAL : int 		= 0;
		public static const CAMERA_ANIM_MODE_TWEENER : int 		= 1;
		public static const CAMERA_ANIM_MODE : int 				= CAMERA_ANIM_MODE_TWEENER; 
		
		// Identificadores de eventos específicos da classe
		public static const CAMERA_CHANGED_POS : String = "cameraChangedPos";

		/**
		* Cena que a câmera está filmando 
		*/		
		private var _scene : Sprite;
		
		/**
		* Tempo que a câmera vai levar para sair do ponto atual e ir até o ponto de destino 
		*/		
		private var _animTime : Number;
		
		/**
		* Indica se a câmera está no meio de uma animação 
		*/		
		private var _animating : Boolean;
		
		/**
		* Variável auxiliar para controle das animações  
		*/		
		private var _animTimeCounter : Number;
		
		/**
		* Ponto de destino da animação 
		*/				
		private var _animDestPos : Point;
		
		/**
		* Velocidade de movimentação da câmera 
		*/		
		private var _animSpeed : Point;

		/**
		* Posição atual da câmera (foco)
		*/		
		private var _pos : Point;
		
		/**
		* A largura da lente da câmera 
		*/		
		private var _lensWidth : Number;
		
		/**
		* A altura da lente da câmera 
		*/		
		private var _lensHeight : Number;
		
		/**
		* Objeto responsável pela animação de movimentação da câmera no eixo X
		*/		
		private var _tweenerX : Tween;
		
		/**
		* Objeto responsável pela animação de movimentação da câmera no eixo Y
		*/		
		private var _tweenerY : Tween;
		
		/**
		* Construtor
		* @param cameraScene Cena que a câmera vai filmar
		*/		
		public function Camera( cameraLensWidth : Number, cameraLensHeight : Number, cameraScene : Sprite = null )
		{
			_animating = false;
			
			_animTime = 0.0;
			_animTimeCounter = 0.0;
			
			_animSpeed = new Point();
			_animDestPos = new Point();
			
			_scene = cameraScene;
			
			_lensWidth = cameraLensWidth;
			_lensHeight = cameraLensHeight;
			
			_pos = new Point();
			
			 _tweenerX = null;
			 _tweenerY = null;
		}
		
		/**
		* Determina a cena que a câmera deve filmar
		* @param cameraScene Cena que a câmera vai filmar 
		*/		
		public function setCameraScene( cameraScene : Sprite ) : void
		{
			_scene = cameraScene;
		}
	
		/**
		* Determina uma animação de movimenta da câmera, de sua posição atual até a posição de destino, que será 
		* executada em um determinado intervalo de tempo
		* @param destPos Posição de destino da câmera
		* @param time Tempo que a câmera vai levar até chegar ao ponto de destino
		*/		
		public function setAnim( destPos : Point, time : Number ) : void
		{
			if( CAMERA_ANIM_MODE == CAMERA_ANIM_MODE_MANUAL )
			{
				_animTime = time;
				changeCurrAnimDest( destPos );
			}
			else
			{
				if( _pos.x != destPos.x )
				{
					if( _tweenerX == null || !_tweenerX.isPlaying )
					{
						_tweenerX = TweenManager.tween( this, "posX", Regular.easeOut, _pos.x, destPos.x, time );
						//_tweenerX = TweenManager.tween( this, "posX", None.easeNone, _pos.x, destPos.x, time, true );
						_tweenerX.addEventListener( TweenEvent.MOTION_FINISH, onAnimEnded );
					}
					else
					{
						_tweenerX.continueTo( destPos.x, time );
					}
				}
				
				if( _pos.y != destPos.y )
				{
					if( _tweenerY == null || !_tweenerY.isPlaying )
					{
						_tweenerY = TweenManager.tween( this, "posY", Regular.easeOut, _pos.y, destPos.y, time );
						//_tweenerY = TweenManager.tween( this, "posY", None.easeNone, _pos.y, destPos.y, time, true );
						_tweenerY.addEventListener( TweenEvent.MOTION_FINISH, onAnimEnded );
					}
					else
					{
						_tweenerY.continueTo( destPos.y, time );
					}
				}
			}
		}
		
		private function onAnimEnded( e : TweenEvent ) : void
		{
			if( e.target == _tweenerX )
			{
				_tweenerX.stop();
				_tweenerX = null;
			}
			else if( e.target == _tweenerY )
			{
				_tweenerY.stop();
				_tweenerY = null;
			}
		}
		
		/**
		* Apenas para a utilização do tweener. Não queremos utilizar propriedades - preferimos utilizar métodos 
		*/		
		public function get posX() : Number
		{
			return getPosition().x;
		}
		
		/**
		* Apenas para a utilização do tweener. Não queremos utilizar propriedades - preferimos utilizar métodos 
		*/
		public function set posX( pX : Number ) : void
		{
			var temp : Point = getPosition();
			temp.x = pX;
			setPosition( temp );
		}
		
		/**
		* Apenas para a utilização do tweener. Não queremos utilizar propriedades - preferimos utilizar métodos 
		*/		
		public function get posY() : Number
		{
			return getPosition().y;
		}
		
		/**
		* Apenas para a utilização do tweener. Não queremos utilizar propriedades - preferimos utilizar métodos 
		*/
		public function set posY( pY : Number ) : void
		{
			var temp : Point = getPosition();
			temp.y = pY;
			setPosition( temp );
		}

		/**
		* Inicia a animação configurada por setAnim
		*/		
		public function startAnim() : void
		{
			if( CAMERA_ANIM_MODE == CAMERA_ANIM_MODE_MANUAL )
			{
				if( _animating )
					endAnim();
				
				_animTimeCounter = 0.0;		
				_animating = true;
			}
		}

		/**
		* Termina a animação de movimentação da câmera. Se a animação ainda não estiver
		* completa, a câmera é levada diretamente para a sua posição final
		*/		
		public function endAnim() : void
		{
			if( CAMERA_ANIM_MODE == CAMERA_ANIM_MODE_MANUAL )
			{
				if( isAnimating() )
				{
					_animating = false;
					
					setPosition( _animDestPos );
					
					// TODO
					//dispatchEvent( new Event( CAMERA_EVENT_ANIM_ENDED ) );
				}
			}
		}
		
		/**
		* Cancela a animação, deixando a câmera em sua posição atual
		*/		
		public function cancelAnim() : void
		{
			if( CAMERA_ANIM_MODE == CAMERA_ANIM_MODE_MANUAL )
			{
				if( isAnimating() )
				{
					_animating = false;
					
					// TODO
					//dispatchEvent( new Event( CAMERA_EVENT_ANIM_ENDED ) );
				}
			}
		}
		
		
		public function setAlpha( alpha : Number ) : void
		{
			this.alpha = alpha;
		}
		
		
		public function get alpha() : Number
		{
			return _scene.alpha;
		}
		
		
		public function set alpha( alpha : Number ) : void
		{
			_scene.blendMode = BlendMode.LAYER;
			_scene.alpha = alpha;
		}
		

		/**
		* Altera o destino da animação atual sem iniciar uma nova animação
		* @param newDestPos O novo ponto de destino da animação
		*/		
		public function changeCurrAnimDest( newDestPos : Point ) : void
		{
			if( CAMERA_ANIM_MODE == CAMERA_ANIM_MODE_MANUAL )
			{
				_animDestPos.x = newDestPos.x;
				_animDestPos.y = newDestPos.y;
	
				// As 3 linhas abaixo equivalem a: ( dest - pos ) / animTime
				_animSpeed = _animDestPos.subtract( getPosition() );
				_animSpeed.x /= _animTime;
				_animSpeed.y /= _animTime;
			}
		}

		/**
		* Retorna se a câmera está no meio de uma animação
		* @return Se a câmera está no meio de uma animação
		*/		
		public function isAnimating() : Boolean
		{
			if( CAMERA_ANIM_MODE == CAMERA_ANIM_MODE_MANUAL )
			{
				return _animating;
			}
			else
			{
				var temp : Boolean = ( _tweenerX == null ? false : _tweenerX.isPlaying );
				return ( temp || ( _tweenerY == null ? false : _tweenerY.isPlaying ) );
			}
		}

		/**
		* Atualiza o objeto
		* @param timeElapsed Tempo transcorrido desde a última atualização
		*/
		public function update( timeElapsed : Number ) : void
		{
			if( CAMERA_ANIM_MODE != CAMERA_ANIM_MODE_MANUAL )
				return;

			if( isAnimating() )
			{
				_animTimeCounter += timeElapsed;
				
				// TODO : Usar um 'FloatGreaterEqual' !!!
				if( _animTimeCounter >= _animTime )
				{
					endAnim();
				}
				else
				{
					move( _animSpeed.x * timeElapsed, _animSpeed.y * timeElapsed );
				}
			}
		}
		
		/**
		* Determina a posição atual da câmera (foco) 
		* @param p A nova posição da câmera
		*/
		public function setPosition( p : Point ) : void
		{
			_pos.x = p.x;
			_pos.y = p.y;
			
			// Está dizendo que não reconhece a propriedade position...
//			if( _tweenerX != null )
//				_tweenerX.position = _pos.x;
//					
//			if( _tweenerY != null )
//				_tweenerY.position = _pos.y;
			
			// Faz o efeito de câmera deslocando a cena que estamos filmando
			if( _scene != null )
			{
				_scene.x = ( _lensWidth * 0.5 ) - _pos.x;
				_scene.y = ( _lensHeight * 0.5 ) - _pos.y;
			}
			
			dispatchEvent( new Event( CAMERA_CHANGED_POS ) );
		}

		/**
		* Retorna a posição atual da câmera 
		* @return A posição atual da câmera
		*/		
		public function getPosition() : Point
		{
			return _pos.clone();
		}
		
		/**
		* Movimenta a posição da câmera (foco) a partir de sua posição atual 
		* @param dx Deslocamento da câmera no eixo x
		* @param dy Deslocamento da câmera no eixo y
		*/
		public function move( dx : Number, dy : Number ) : void
		{
			var temp : Point = getPosition();
			
			// Se a câmera está indo para direita, a cena deve ir para a esquerda (e vice-versa)
			// Se a câmera está indo para cima, a cena deve ir para baixo (e vice-versa)
			// Por isso invertemos os sinais de dx e dy
			temp.offset( dx, dy );
			setPosition( temp );
		}
		
		/**
		* Retorna a largura da lente da câmera
		* @return A largura da lente da câmera
		*/		
		public function getLensWidth() : Number
		{
			return _lensWidth;
		}
		
		/**
		* Retorna a altura da lente da câmera
		* @return A altura da lente da câmera
		*/		
		public function getLensHeight() : Number
		{
			return _lensHeight;
		}
		
		/**
		* Determina a largura da lente da câmera
		* @param A largura da lente da câmera
		*/		
		public function setLensWidth( w : Number ) : void
		{
			_lensWidth = w;
		}
		
		/**
		* Determina a altura da lente da câmera
		* @param A altura da lente da câmera
		*/		
		public function setLensHeight( h : Number ) : void
		{
			_lensHeight = h;
		}
	}
}
