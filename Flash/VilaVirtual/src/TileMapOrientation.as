package
{
	import NGC.ASWorkarounds.Enum;

	public final class TileMapOrientation extends Enum
	{
		// "Construtor" estático
		{ initEnum( TileMapOrientation ); }
		
		// Possíveis caminhos entre tiles
		public static const ORIENTATION_ORTHO : TileMapOrientation			= new TileMapOrientation();
		public static const ORIENTATION_ISO : TileMapOrientation			= new TileMapOrientation();
	}
}
