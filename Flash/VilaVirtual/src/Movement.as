package
{
	public final class Movement
	{
		// sentidos de varredura do array de direções
		private static const TRAVERSE_DIR_INCREASE	: int = 2;
		private static const TRAVERSE_DIR_DECREASE	: int = 4;
		
		private var traverseDirection : int = TRAVERSE_DIR_INCREASE;
		
		// modos de varredura do array de direções
		/**
		 * normal - ao chegar ao final do array, muda para modo decrescente, e ao chegar ao início do array, muda
		 * para modo crescente
		 */
		public static const TRAVERSE_MODE_NORMAL	: int = 8;
		/** circular - ao chegar ao final do array, volta a percorrê-lo do início (modo de varredura muda somente
		 * no caso de um botão pressionado).
		 */ 
		public static const TRAVERSE_MODE_CIRCULAR	: int = 16;
		
		private var _traverseMode : int = TRAVERSE_MODE_CIRCULAR;
		
		private var _loopCount : int = -1;
		
		private var _directions : Vector.<Direction>;
		
		private var _directionsIndex : int;
		
		private var _active : Boolean = true;
		
		private var _lastDirection : Direction;	
		
		/** A posição x é usada como base para definir se o elemento chegou à sua posição de destino. */
		private var _destinationX : int;
		
		private var destinationTile : ITile;
		
		private var _onDone : Function;
		
		
		public function Movement( directions : Vector.< Direction >, onDone : Function = null, traverseMode : int = TRAVERSE_MODE_NORMAL, loopCount : int = -1 ) : void
		{
			// copia o array de direções e verifica se ele forma um caminho circular (caso a varredura seja
			// circular)
			_directions = directions;
			var directionsRead : Array = new Array();
			directionsRead[ Direction.DIRECTION_UP ] = 0;
			directionsRead[ Direction.DIRECTION_DOWN ] = 0;
			directionsRead[ Direction.DIRECTION_LEFT ] = 0;
			directionsRead[ Direction.DIRECTION_RIGHT ] = 0;
			
			_onDone = onDone;
			
			trace( "DIRECTIONS:\n-----" );
			for each ( var d : Direction in directions ) {
				trace( d );
				( directionsRead[ d ] ) ++;
			}
			trace( "------" );
			
			if ( traverseMode == TRAVERSE_MODE_CIRCULAR ) {
				// para uma trajetória ser circular, basta que o número de direções opostas seja igual
				if ( directionsRead[ Direction.DIRECTION_UP ]		!= directionsRead[ Direction.DIRECTION_DOWN ]
					|| directionsRead[ Direction.DIRECTION_LEFT ]	!= directionsRead[ Direction.DIRECTION_RIGHT ] )
					throw new Error( "path is not circular." );
			}			
		}
		
		
		public function prepareMove( character : Character ) : void {
			if ( active ) {
				var dir : Direction = traverseDirection == TRAVERSE_DIR_INCREASE ? directions[ directionsIndex ] : TileGameUtils.GetOppositeDirection( directions[ directionsIndex ] );
				
				lastDirection = dir;
				switch ( dir ) {
					case Direction.DIRECTION_DOWN:
						character.movementState = MovementState.MOVE_STATE_MOVING_DOWN;
					break;
					
					case Direction.DIRECTION_LEFT:
						character.movementState = MovementState.MOVE_STATE_MOVING_LEFT;
					break;
					
					case Direction.DIRECTION_UP:
						character.movementState = MovementState.MOVE_STATE_MOVING_UP;
					break;
					
					case Direction.DIRECTION_RIGHT:
						character.movementState = MovementState.MOVE_STATE_MOVING_RIGHT;
					break;
				}
				var t : ITile = character.tile;
				var t2 : ITile = character.tile.getNeighborAtDirection( dir );
				var w : WorldPos = TileGameUtils.GetTileCenterWorldPos( t2 );
//				var w : WorldPos = TileGameUtils.GetTileCenterWorldPos( character.tile );
				_destinationX = IsoGlobals.worldCoordToPixelCoord( w ).x;
				destinationTile = t2;
				
				trace( "MOVING " + dir + ", " + directionsIndex + " -> " + character.pixelPos.x + " / " + _destinationX );
				
				if ( traverseDirection == TRAVERSE_DIR_INCREASE ) {
					++directionsIndex;
					if ( directionsIndex >= directions.length ) {
						if ( traverseMode == TRAVERSE_MODE_NORMAL ) {
							directionsIndex = directions.length - 1;
							traverseDirection = TRAVERSE_DIR_DECREASE;
						} else {
							// modo circular; volta ao início do array
							directionsIndex = 0;
						}
						checkEnd( character );
					} // fim if ( directionsIndex >= directions.length )
				} else {
					--directionsIndex;
					if ( directionsIndex < 0 ) {
						if ( traverseMode == TRAVERSE_MODE_NORMAL ) {
							directionsIndex = 0;
							traverseDirection = TRAVERSE_DIR_INCREASE;
						} else {
							// modo circular; volta ao final do array
							directionsIndex = directions.length - 1;
						}						
						checkEnd( character );
					} // fim if ( directionsIndex < 0 )
				}
			} // fim if ( active )			
		}
		
		
		private function checkEnd( character : Character ) : void
		{
			trace( "CHECK END: " + loopCount );
			if ( loopCount > 0 ) {
				--loopCount;
				if ( loopCount == 0 ) {
					active = false;
					character.movement = null;
//					character.movementState = MovementState.
					if ( _onDone != null )
						_onDone();								
				}
			}			
		}
		
		
		public function refresh( character : Character ) : void {
			if ( active ) {
				var moveDone : Boolean = false;
				
				switch ( directions[ directionsIndex ] ) {
					case Direction.DIRECTION_DOWN:
					case Direction.DIRECTION_LEFT:
//						moveDone = character.tile == destinationTile;
						moveDone = character.pixelPos.x <= _destinationX;
					break;
					
					case Direction.DIRECTION_UP:
					case Direction.DIRECTION_RIGHT:
//						moveDone = character.tile == destinationTile;
						moveDone = character.pixelPos.x >= _destinationX;
					break;
				}
				
//				trace( character.pixelPos.x + " / " + _destinationX );
				
				if ( moveDone ) {
					// terminou o movimento atual e chegou à posição de destino
//					TileGameUtils.CenterTileObjInTile( character );
					prepareMove( character );
				}
			}
		}
		
		
		private function get directions():Vector.<Direction>
		{
			return _directions;
		}

		private function set directions(value:Vector.<Direction>):void
		{
			_directions = value;
		}

		private function get directionsIndex():int
		{
			return _directionsIndex;
		}

		private function set directionsIndex(value:int):void
		{
			_directionsIndex = value;
		}

		public function get active():Boolean
		{
			return _active;
		}

		public function set active(value:Boolean):void
		{
			_active = value;
		}

		private function get lastDirection():Direction
		{
			return _lastDirection;
		}

		private function set lastDirection(value:Direction):void
		{
			_lastDirection = value;
		}

		private function get traverseMode():int
		{
			return _traverseMode;
		}

		private function set traverseMode(value:int):void
		{
			_traverseMode = value;
		}

		public function get loopCount():int
		{
			return _loopCount;
		}

		public function set loopCount(value:int):void
		{
			_loopCount = value;
		}


 // fim do método update()
		
	}
}