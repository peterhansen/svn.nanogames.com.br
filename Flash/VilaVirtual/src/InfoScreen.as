package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	public class InfoScreen extends MovieClipVS
	{
		protected const WIDTH : Number = 575;
		protected const HEIGHT : Number = 383;
		
		protected const BORDER_WIDTH : Number = 15.0;
		protected const BORDER_HEIGHT : Number = 15.0;
		
		protected const TITLE_OFFSET_Y : int = -12;
		protected const CLOSE_OFFSET_X : int = 12;		
		
		protected var border : MovieClip; 
		
		protected var tag : MovieClip;
		
		public function InfoScreen()
		{
			super();
			
			x = ( 640 - WIDTH ) / 2;
			y = ( 480 - HEIGHT ) / 2;	
			
			border = new boxinterfacesBase();
			border.width = WIDTH;
			border.height = HEIGHT;
			border.x = ( WIDTH / 2 ) + 11;
			border.y = ( HEIGHT / 2 ) + 35;
			addChild( border );	
		}
		
		
		protected function addTagAndButton( tagClass : String, onClose : Function, onMouseOver : Function, onMouseOut : Function ) : void
		{
			var c : Class = Class( getDefinitionByName( tagClass ) );
			tag = new c() as MovieClip;
			tag.x = tag.width / 2;
			tag.y = ( tag.height / 2 ) + TITLE_OFFSET_Y;
			addChild( tag );	
			
			var close : BoxBtBase = new BoxBtBase();
			close.mouseChildren = false;
			close.buttonMode = true;
			close.addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			close.addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			close.addEventListener( MouseEvent.CLICK, onClose );
			close.x = WIDTH - ( close.width / 2 ) - BORDER_WIDTH - CLOSE_OFFSET_X;
			close.y = tag.y + ( ( tag.height - close.height ) / 2 );
			addChild( close );			
		}
		
		public function show() : void{
			visible = true;
		}
		
		public function hide() : void{
			visible = false;
		}
	}
}