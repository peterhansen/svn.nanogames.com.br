package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class MovementState extends Enum
	{
		// "Construtor" estático
		{ initEnum( MovementState ); }
		
		// Possíveis estados de movimentação dos objetos não estáticos
		public static const MOVE_STATE_STOPPED_UP : MovementState		= new MovementState();
		public static const MOVE_STATE_STOPPED_DOWN : MovementState		= new MovementState();
		public static const MOVE_STATE_STOPPED_LEFT : MovementState		= new MovementState();
		public static const MOVE_STATE_STOPPED_RIGHT : MovementState	= new MovementState();
		
		public static const MOVE_STATE_MOVING_UP : MovementState 		= new MovementState();
		public static const MOVE_STATE_MOVING_DOWN : MovementState		= new MovementState();
		public static const MOVE_STATE_MOVING_LEFT : MovementState		= new MovementState();
		public static const MOVE_STATE_MOVING_RIGHT : MovementState		= new MovementState();
	}
}
