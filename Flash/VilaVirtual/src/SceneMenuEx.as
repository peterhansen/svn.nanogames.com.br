package
{
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOAppCustomerData;
	import NGC.NanoOnline.NOConnection;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NOTextsIndexes;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public final class SceneMenuEx extends AppScene implements INOListener
	{
		/**
		* Objeto que realiza a integração com o NanoOnline
		*/		
		private var _noCustomerAppData : NOAppCustomerData;
		
		/**
		* Construtor 
		*/		
		public function SceneMenuEx(  sceneAnchorAtCenter : Boolean = true, needsAsynchronousLoading : Boolean = false, dispatchesProgressEvents : Boolean = true )
		{
			super( new SceneMenu(), sceneAnchorAtCenter, needsAsynchronousLoading, dispatchesProgressEvents );
			
			_noCustomerAppData = new NOAppCustomerData();
			
			innerScene.addEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
		}
		
		/**
		 * Callback chamada quando a cena propriamente dita acaba de ser construída 
		 * @param e O objeto que encapsula os dados do evento 
		 */		
		private function onSceneBuilt( e : Event ) : void
		{
			innerScene.removeEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
			
			var temp : SceneMenu = ( innerScene as SceneMenu );
			temp.getBtContinue().addEventListener( MouseEvent.CLICK, onLoadPreviousGame );
		}
		
		/**
		 * Callback chamada quando o usuário clica no botão de enviar o email
		 * @param e Objeto que encapsula os dados do evento  
		 */		
		public function onLoadPreviousGame( e : MouseEvent ) : void
		{			
			try
			{
				// Verifica se o jogador está logado
				if( NOConnection.GetInstance().loggedCustomerId == NOCustomer.CUSTOMER_PROFILE_ID_NONE )
				{
					( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_MUST_LOGIN_TO_SAVE ),
														NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
					return;
				}
				
				_noCustomerAppData.ownerCustomerId = NOConnection.GetInstance().loggedCustomerId;
				
				// Envia a requisição para o NanoOnline
				if( !NOAppCustomerData.LoadData( _noCustomerAppData, this ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
			}
			catch( ex : Error )
			{
				trace( ">>> SceneMenuEx::onLoadPreviousGame - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			( root as IApplication ).showWaitFeedback();
		}
		
		public function get loadedData() : NOAppCustomerData
		{
			return _noCustomerAppData;
		}
		
		/**
		 * Indica que uma requisição foi enviada para o NanoOnline 
		 */
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		 * Indica que a requisição foi cancelada pelo usuário 
		 */
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		 * Indica que uma requisição foi respondida e terminada com sucesso
		 */
		public function onNOSuccessfulResponse() : void
		{
			( root as IApplication ).hideWaitFeedback();
			
			// Não precisamos de confirmação em casos de sucesso
//			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_SENT ),
//												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), onDataLoaded );
			
			( root as IApplication ).setState( ApplicationState.APP_STATE_CONTINUE );
		}
		
//		/**
//		 * Callback chamada quando o usuário clica no botão de 'OK' do popup que confirma
//		 * o carregamento de dados
//		 */	
//		private function onDataLoaded() : void
//		{
//			( root as IApplication ).setState( ApplicationState.APP_STATE_CONTINUE );;
//		}
		
		/**
		 * Sinaliza erros ocorridos nas operações do NanoOnline
		 * @param errorCode O código do erro ocorrido
		 * @param errorStr Descrição do erro ocorrido
		 */
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_LOAD )+ ":\n" + errorStr,
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
		}
		
		/**
		 * Indica o progresso da requisição atual
		 * @param currBytes A quantidade de bytes que já foi transferida
		 * @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		 */	
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}