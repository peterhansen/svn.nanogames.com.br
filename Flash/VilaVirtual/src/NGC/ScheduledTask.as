package NGC
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	/**
	* Classe utitilizada para agendar a execução de métodos e funções após um dado delay 
	* @author Daniel Alves
	*/	
	public class ScheduledTask
	{
		/**
		* Timer utilizado para fornecer o delay antes da chamada do método 
		*/		
		private var _timer : Timer;
		
		/**
		* O método ou função que vamos invocar
		*/		
		private var _func : Function;
		
		/**
		* Os parâmetros do método ou função que vamos invocar
		*/		
		private var _funcParams : Array;
		
		/**
		* Construtor
		* @param delay O tempo que devemos esperar, em milissegundos, até executar o método
		* @param f O método ou função que vamos invocar
		* @param fParams Os parâmetros do método ou função que vamos invocar
		*/		
		public function ScheduledTask( delay : Number, f : Function, ...fParams )
		{
			super();
			
			_timer = new Timer( delay, 1 );
			_timer.addEventListener( TimerEvent.TIMER, onDelayCompleted );
			
			_func = f;
			_funcParams = fParams;
		}
		
		/**
		* Invoca o método
		*/		
		public function call() : void
		{
			trace( "\n------------------------------------\n" );
			trace( "ScheduledTask::call" );
			trace( "\n------------------------------------\n" );
			
			invalidate();

			_timer.addEventListener( TimerEvent.TIMER, onDelayCompleted );
			_timer.reset();
			_timer.start();
		}
		
		public function invalidate() : void
		{
			_timer.stop();
			_timer.removeEventListener( TimerEvent.TIMER, onDelayCompleted );
		}
		
		/**
		* Callback chamada quando o delay termina
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onDelayCompleted( e : TimerEvent ) : void
		{
			trace( "\n------------------------------------\n" );
			trace( "ScheduledTask::onDelayCompleted" );
			trace( "\n------------------------------------\n" );
			
			_timer.stop();

			if( _funcParams.length > 0 )
				_func( _funcParams );
			else
				_func();
		}
	}
}