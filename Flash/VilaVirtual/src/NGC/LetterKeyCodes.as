package NGC
{
	import NGC.ASWorkarounds.StaticClass;
	
	/**
	* 
	* @author Daniel L. Alves
	* 
	*/	
	public final class LetterKeyCodes extends StaticClass
	{
		/**
		* Construtor
		* Classes estáticas não devem ser instanciadas. O construtor irá disparar uma exceção se chamado.
		*/
		public function LetterKeyCodes()
		{
			super();
		}
		
		// Códigos das teclas de letras. A classe Keyboard não define estas para nós...
		// http://www.adobe.com/livedocs/flash/9.0/main/wwhelp/wwhimpl/common/html/wwhelp.htm?context=LiveDocs_Parts&file=00001136.html
		public static const KEYCODE_A : uint	= 65;
		public static const KEYCODE_B : uint	= 66;
		public static const KEYCODE_C : uint	= 67;
		public static const KEYCODE_D : uint	= 68;
		public static const KEYCODE_E : uint	= 69;
		public static const KEYCODE_F : uint	= 70;
		public static const KEYCODE_G : uint	= 71;
		public static const KEYCODE_H : uint	= 72;
		public static const KEYCODE_I : uint	= 73;
		public static const KEYCODE_J : uint	= 74;
		public static const KEYCODE_K : uint	= 75;
		public static const KEYCODE_L : uint	= 76;
		public static const KEYCODE_M : uint	= 77;
		public static const KEYCODE_N : uint	= 78;
		public static const KEYCODE_O : uint	= 79;
		public static const KEYCODE_P : uint	= 80;
		public static const KEYCODE_Q : uint	= 81;
		public static const KEYCODE_R : uint	= 82;
		public static const KEYCODE_S : uint	= 83;
		public static const KEYCODE_T : uint	= 84;
		public static const KEYCODE_U : uint	= 85;
		public static const KEYCODE_V : uint	= 86;
		public static const KEYCODE_W : uint	= 87;
		public static const KEYCODE_X : uint	= 88;
		public static const KEYCODE_Y : uint	= 89;
		public static const KEYCODE_Z : uint	= 90;
	}
}
