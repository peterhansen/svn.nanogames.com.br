package NGC.NanoGeom
{
	import flash.geom.Point;
	import flash.geom.Vector3D;
	
	// Documentação
	// http://en.wikipedia.org/wiki/Plane_(geometry)
	// http://gamedeveloperjourney.blogspot.com/2009/04/point-plane-collision-detection.html

	public class Plane
	{	
		// Vetores que definem o plano
		private var _top : Vector3D;
		private var _right : Vector3D;
		
		// Normal do plano
		private var _normal : Vector3D;
		
		// Deslocamento do plano em relação à origem (0,0,0)
		private var _d : Number;

		// Construtor
		// Equação do plano = ax + by + cz + d = 0, onde (a,b,c) é um vetor que define a normal do plano e d é a distância
		// deste plano à origem (0,0,0) na direção definida por sua normal
		public function Plane( top : Vector3D, right : Vector3D, pointInPlane : Vector3D )
		{
			// Armazena a normal do plano
			setNormal( top, right );
			
			// Calcula a distância do plano à origem
			d = Math.sqrt( ( pointInPlane.x * pointInPlane.x ) + ( pointInPlane.y * pointInPlane.y ) );
		}
		
		// Retorna a distância do ponto ao plano
		// A distância será positiva se p estiver do lado do plano para o qual a normal aponta. Caso contrário, será negativa.
		// Obviamente, se a distância for 0, o ponto encontra-se no plano
		// D = ( n.x * p.x + n.y * p.y + n.z * p.z + d ) / n.length
		// Como n (a normal) está normaliza:
		// D = ( n.x * p.x + n.y * p.y + n.z * p.z + d )
		// Ou
		// D = ( n . p ) + d
		public function distanceFrom( point : Vector3D ) : Number
		{
			return point.dotProduct( normal ) + d;
		}
		
		// Define a normal do plano através dos vetores que o definem
		public function setNormal( top : Vector3D, right : Vector3D ) : void
		{
			// Armazena top e right
			_top = top;
			_top.normalize();

			_right = right;
			_right.normalize();

			_normal = right.crossProduct( top );
			// TODO : Acho que não precisamos normalizar o produto vetorial de dois vetores já normalizados...
			_normal.normalize();
		}
		
		// "Operador" de igualdade
		public function equals( other : Plane ) : Boolean
		{
			return ( d == other.d ) && ( normal.equals( other.normal ) );
		}
		
		// Getters e Setters
		public function get normal() : Vector3D
		{
			// readonly - Por isso retorna uma cópia
			return _normal.clone();
		}
		
		public function get top() : Vector3D
		{
			// readonly - Por isso retorna uma cópia
			return _top.clone();
		}
		
		public function get right() : Vector3D
		{
			// readonly - Por isso retorna uma cópia
			return _right.clone();
		}
		
		public function get d() : Number
		{
			return _d;
		}
		
		public function set d( x : Number ) : void
		{
			_d = x;
		}
	}
}
