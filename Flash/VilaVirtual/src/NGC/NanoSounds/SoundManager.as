package NGC.NanoSounds
{
	import de.polygonal.ds.HashMap;
	
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	
	public final class SoundManager extends EventDispatcher
	{
		/**
		* Constante utilizada para determinar loopings infinitos de músicas (até o AS3, o flash não seguia o "padrão" de -1 representar loopings infinitos) 
		*/		
		public static const INFINITY_LOOP : int = 999999999;

		/**
		* Número máximo de sons simultâneos. Valor definido na documentação do AS3:
		* http://www.adobe.com/livedocs/flash/9.0/ActionScriptLangRefV3/flash/media/Sound.html#play() 
		*/		
		private static const MAX_SOUND_CHANNELS : uint = 32;

		/**
		* Sons que podem ser reproduzidos por SoundManager
	 	*/		
		private var _sounds : Vector.< Sound >;
		
		/**
		* Canais de reprodução de sons 
		*/
		private var _soundChannels : Vector.< SoundManagerChannel >;
		// OLD : A classe HashMap da biblioteca de estruturas de dados que utilizamos infelizmente não suporta chaves duplicatas...
		//private var _soundChannelsMap : HashMap;

		/**
		* Única instância da classe
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript
		*/		
		private static var _singleton : SoundManager = new SoundManager();
		
		/**
		* Construtor 
		*/		
		public function SoundManager()
		{
			super();
			
			// Se já construímos a variável estática...
			if( _singleton != null )
				throw new Error( "This is a singleton class. You should call GetInstance() method." );
			
			_sounds = new Vector.< Sound >();
			
			// OBS: Classes internas dão problema com a forma que estamos utilizando para implementar o padrão Singleton
			// Alocaremos este container quando formos tocar um som
			//_soundChannels = new Vector.< SoundManagerChannel >();
			
			// OLD : A classe HashMap da biblioteca de estruturas de dados que utilizamos infelizmente não suporta chaves duplicatas...
			//_soundChannelsMap = new HashMap( MAX_SOUND_CHANNELS );
		}
		
		/**
		* Retorna a única instância da classe
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript 
		* @return A única instância da classe 
		*/		
		public static function GetInstance() : SoundManager
		{
			return _singleton;
		}
		
		/**
		* Acrescenta um som à lista de sons de SoundManager
		* @param snd O som a ser acrescentado à lista
		* @return O índice no qual o som foi inserido 
		*/		
		public function pushSound( snd : Sound ) : uint
		{
			_sounds.push( snd );
			return _sounds.length - 1;
		}
		
		/**
		* Pára a execução de todos os sons em reprodução 
		*/		
		public function stopAllSounds() : void
		{
			if( _soundChannels != null )
			{
				var nChannels : int = _soundChannels.length;
				for( var i : int = 0 ; i < nChannels ; ++i )
					_soundChannels[i].soundChannel.stop();
				
				_soundChannels.splice( 0, nChannels );
				_soundChannels = null;
			}
		}
		
		/**
		* Remove todos os sons atualmente carregados da memória 
		*/		
		public function unloadAllSounds() : void
		{
			stopAllSounds();

			if( _sounds != null )
			{
				var nSounds : int = _sounds.length;
				for( var i : int = 0 ; i < nSounds ; ++i )
				{
					if( _sounds[i].url != null )
						_sounds[i].close();
				}
				_sounds.splice( 0, nSounds );
			}
		}
		
//		public function playSoundChainAtIndex( index : uint, chain : SoundChain ) : Boolean
//		{
//			// Verifica se o índice do som é válido
//			if( index >= _sounds.length )
//				throw new ArgumentError( "Invalid sound index sent to SoundManager" );
//			
//			// OBS: Classes internas dão problema com a forma que estamos utilizando para implementar o padrão Singleton
//			// Por isso alocamos o container aqui, e não no construtor
//			if( _soundChannels == null )
//				_soundChannels = new Vector.< SoundManagerChannel >();
//			
//			// Toca o 1o som da sequência
//			var playData : SoundPlayData
//			
//			const channel : SoundChannel = _sounds[ index ].play( startTime, loopCount, transform );
//			if( channel != null )
//			{
//				_soundChannels.push( new SoundManagerChannel( index, channel ) );
//				return true;
//			}
//			
//			return false; 
//		}
		
		/**
		* Inicia a reprodução de um som
		* @param index Índice do som no array de sons
		* @param startTime A posição inicial, em milissegundos, na qual a reprodução deve começar
		* @param loopCount Quantas vezes devemos tocar o som
		* @param transform Objeto de transformação de áudio
		* @param reuseSoundChannel Indica se devemos reutilizar um canal de som existente caso este mesmo som já esteja sendo reproduzido
		* @return Se conseguiu alocar um novo canal de som para tocar o som especificado 
		*/		
		public function playSoundAtIndex( index : uint, startTime : Number = 0.0, loopCount : int = 0, transform : SoundTransform = null, reuseSoundChannel : Boolean = false ) : Boolean
		{
			// Verifica se o índice do som é válido
			if( index >= _sounds.length )
			{
				return true;
				
				// TODO : Colocar condicional: se debug, dispara exceção
				//throw new ArgumentError( "Invalid sound index sent to SoundManager" );
			}

			// OLD : A classe HashMap da biblioteca de estruturas de dados que utilizamos infelizmente não suporta chaves duplicatas...
//			if( reuseSoundChannel )
//			{
//				// Verifica se já estamos tocando este som
//				_soundChannelsMap.
//				const usedChannel : SoundChannel = _soundChannelsMap.remove( index );
//				if( usedChannel != null )
//					usedChannel.stop();
//			}
//
//			// Armazena o canal de som
//			const channel : SoundChannel = _sounds[ index ].play( startTime, loopCount, transform );
//			if( channel != null )
//			{
//				if( !_soundChannelsMap.insert( index, channel ) )
//				{
//					channel.stop();
//					return false;
//				}
//
//				return true;
//			}
//			
//			return false;
			
			// OBS: Classes internas dão problema com a forma que estamos utilizando para implementar o padrão Singleton
			// Por isso alocamos o container aqui, e não no construtor
			if( _soundChannels == null )
				_soundChannels = new Vector.< SoundManagerChannel >();
			
			if( reuseSoundChannel )
			{
				// Verifica se já estamos tocando este som
				// OBS: Da forma como o código é organizado, iremos sempre desalocar o som que está a mais tempo na memória quando
				// reutilizarmos um canal. Isto é altamente desejado
				for( var i : uint = 0 ; i < _soundChannels.length ; ++i )
				{
					if( _soundChannels[i].soundIndex == index )
					{
						_soundChannels[i].soundChannel.stop();
						_soundChannels.splice( i, 1 );
					}
				}
			}
			
			// Armazena o canal de som
			const channel : SoundChannel = _sounds[ index ].play( startTime, loopCount, transform );
			if( channel != null )
			{
				_soundChannels.push( new SoundManagerChannel( index, channel ) );
				return true;
			}
			
			return false; 
		}
	}
}

import flash.media.SoundChannel;

internal class SoundManagerChannel
{
	/**
	* Canal de áudio que está tocando o som
	*/
	private var _sndChannel : SoundChannel

	/**
	* índice do som no vettor de sons 
	*/	
	private var _sndIndex : uint;

	/**
	* Construtor 
	*/	
	function SoundManagerChannel( referencedIndex : uint, channel : SoundChannel )
	{
		_sndIndex = referencedIndex;
		_sndChannel = channel;
	}
	
	public function get soundChannel() : SoundChannel
	{
		return _sndChannel;
	}
	
	public function get soundIndex() : uint
	{
		return _sndIndex;
	}
}
