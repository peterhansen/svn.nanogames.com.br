package NGC.NanoSounds
{
	import flash.events.Event;

	public final class SoundChain
	{
		private var _currSound : int;
		
		private var _sounds : Vector.< SoundPlayData >;
		
		public function SoundChain( sounds : Vector.< SoundPlayData > )
		{
			_sounds = sounds;
			_currSound = -1;
		}

		private function onSoundCompleted( e : Event ) : void
		{
			
		}
	}
}