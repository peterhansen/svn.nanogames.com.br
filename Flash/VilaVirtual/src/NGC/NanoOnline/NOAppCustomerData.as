package NGC.NanoOnline
{
	import NGC.ByteArrayEx;
	import NGC.StringRef;
	import NGC.Utils;
	
	import de.polygonal.ds.HashMap;
	
	import mx.utils.StringUtil;

	/**
	* Os métodos que enviam as requisições são estáticos pois foram herdados de C++. O objetivo desta solução é não precisarmos ter que armazenar
	* o objeto alvo da requisição em um objeto persistente ou globalmente para fazer as requisições. É possível criar um objeto localmente em
	* um étodo, enviar a requisição e ainda ter seus dados na callback de resposta: isto acontece pois uma cópia (em as3, uma referência) é
	* armazenada em um NORequestHolder. Se não o fizéssemos, em C++, o objeto local seria destruído ao fim de método, e a callback seria chamada
	* posteriormente sobre uma área de memória inválida.
	*/
	public final class NOAppCustomerData extends Object
	{
		// IDs dos parâmetros das requisições
		private static const NANO_ONLINE_APPCUSTOMERDATA_PARAM_PROFILE_ID : int 	= 0;
		private static const NANO_ONLINE_APPCUSTOMERDATA_PARAM_DATA : int 			= 1;
		private static const NANO_ONLINE_APPCUSTOMERDATA_PARAM_OVERWRITE : int 		= 2;
		private static const NANO_ONLINE_APPCUSTOMERDATA_PARAM_SLOT : int 			= 3;
		private static const NANO_ONLINE_APPCUSTOMERDATA_PARAM_SUBTYPE : int 		= 4;
		private static const NANO_ONLINE_APPCUSTOMERDATA_PARAM_END : int 			= 5;
		
		// Códigos de retorno relativos à requisição de registro do usuário
		private static const RC_ERROR_COULDNT_SAVE : int			= 1;
		private static const RC_ERROR_NEEDS_OVERWRITING : int		= 2;
		private static const RC_ERROR_NO_DATA_TO_LOAD : int			= 3;
		private static const RC_ERROR_COULDNT_LOAD : int			= 4;
		private static const RC_ERROR_INVALID_SLOT : int			= 5;
		
		private var _ownerCustomerId : int;

		/**
		* O ID do perfil do usuário dono destes dados
		*/
		public function get ownerCustomerId() : int
		{
			return _ownerCustomerId;
		}

		/**
		* @private
		*/
		public function set ownerCustomerId( value : int ) : void
		{
			_ownerCustomerId = value;
		}

		private var _data : ByteArrayEx;

		/**
		* Os dados a serem gravados (save) ou os dados que foram carregados (load)
		*/
		public function get data() : ByteArrayEx
		{
			return _data;
		}

		/**
		* @private
		*/
		public function set data( value : ByteArrayEx ) : void
		{
			_data = value;
		}
		
		private var _overwrite : Boolean;

		/**
		* Indica se devemos sobrescrever um save anterior ao enviarmos um novo save
		*/
		public function get overwrite() : Boolean
		{
			return _overwrite;
		}

		/**
		* @private
		*/
		public function set overwrite( value : Boolean ) : void
		{
			_overwrite = value;
		}
		
		private var _slot : uint;

		/**
		* Slot onde iremos salvar os dados
		*/
		public function get slot() : uint
		{
			return _slot;
		}

		/**
		* @private
		*/
		public function set slot( value : uint ) : void
		{
			_slot = value;
		}
		
		private var _subtype : uint;

		/**
		* O subtipo da informação alvo da operação de load/save. Pode ser usado para dividir as informações, permitindo
		* que não tenhamos que enviar muitos dados de uma única vez quando desejamos alterar/obter apenas uma pequena
		* parte 
		*/
		public function get subtype() : uint
		{
			return _subtype;
		}

		/**
		* @private
		*/
		public function set subtype( value : uint ) : void
		{
			_subtype = value;
		}

		/**
		* Construtor 
		* @param ownerId O ID do perfil do usuário dono destes dados
		* @param dataToSave Os dados binários a serem salvos
		* @param overwritePrevious Indica se devemos sobrescrever um save anterior ao enviarmos um novo save
		* @param dataSlot O slot onde devemos salvar os dados
		* @param dataSubtype O subtipo da informação alvo da operação de load/save
		*/				
		public function NOAppCustomerData( ownerId : int = -1, dataToSave : ByteArrayEx = null, overwritePrevious : Boolean  = true, dataSlot : uint = 0, dataSubtype : uint = 0 )
		{
			_ownerCustomerId = ownerId;
			_data = dataToSave;
			if( _data == null )
				_data = new ByteArrayEx();

			_overwrite = overwritePrevious;
			_slot = dataSlot;
			_subtype = dataSubtype;
		}

		/**
		* Envia uma requisição de salvar dados para o NanoOnline
		* @param data Os dados que desejamos salvar no servidor
		* @param listener O objeto que receberá os eventos de feedback do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário  
		*/		
		public static function SaveData( data : NOAppCustomerData, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/app_customer_data/save_data", new NORequestHolder( data, data.save, data.onSaveResponse ) );
		}
		
		/**
		* Envia uma requisição de carregar dados para o NanoOnline
		* @param data Os dados que desejamos salvar no servidor
		* @param listener O objeto que receberá os eventos de feedback do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário  
		*/
		public static function LoadData( data : NOAppCustomerData, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/app_customer_data/load_data", new NORequestHolder( data, data.load, data.onLoadResponse ) );
		}

		/**
		* Escreve na stream de requisição as informações da requisição de salvar dados
		* @param stream Array de dados onde iremos escrever as informações da requisição
		* @return <code>true</code> se conseguiu escrever os dados da requisição, <code>false</code> caso contrário
		*/
		private function save( stream : ByteArrayEx ) : Boolean
		{
			// Não precisamos passar o ID da aplicação, pois este já é enviado pela conexão
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_PROFILE_ID );
			stream.writeInt( _ownerCustomerId );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_DATA );
			stream.writeUnsignedInt( _data.length );
			stream.writeBytes( _data, 0, _data.length );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_OVERWRITE );
			stream.writeBoolean( _overwrite );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_SLOT );
			stream.writeByte( _slot );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_SUBTYPE );
			stream.writeByte( _subtype );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_END );
			
			return true;
		}
		
		/**
		* Trata a resposta à requisição de salvar dados
		* @param table Tabela de parâmetros de resposta
		* @param errorStr String que irá conter as informações sobre o erro ocorrido em casos de falha
		* @return <code>true</code> se a requisição foi bem sucedida, ou <code>false</code> em casos de erros
		*/
		private function onSaveResponse( table : HashMap, errorStr : StringRef ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					return true;

				case NOAppCustomerData.RC_ERROR_COULDNT_SAVE:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_SAVE );
					return false;
					
				case NOAppCustomerData.RC_ERROR_NEEDS_OVERWRITING:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_SAVE_NEEDS_OVERWRITING );
					return false;
					
				case NOAppCustomerData.RC_ERROR_INVALID_SLOT:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_SLOT );
					return false;
					
				default:
					trace( ">>> Unrecognized param return code sent to onSaveResponse in response to save request" );
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
			return true;
		}
			
		/**
		* Escreve na stream de requisição as informações da requisição de carregar dados
		* @param stream Array de dados onde iremos escrever as informações da requisição
		* @return <code>true</code> se conseguiu escrever os dados da requisição, <code>false</code> caso contrário
		*/
		private function load( stream : ByteArrayEx ) : Boolean
		{
			// Não precisamos passar o ID da aplicação, pois este já é enviado pela conexão
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_PROFILE_ID );
			stream.writeInt( _ownerCustomerId );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_SLOT );
			stream.writeByte( _slot );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_SUBTYPE );
			stream.writeByte( _subtype );
			
			stream.writeByte( NANO_ONLINE_APPCUSTOMERDATA_PARAM_END );
			
			return true;
		}
		
		/**
		* Trata a resposta à requisição de carregar dados
		* @param table Tabela de parâmetros de resposta
		* @param errorStr String que irá conter as informações sobre o erro ocorrido em casos de falha
		* @return <code>true</code> se a requisição foi bem sucedida, ou <code>false</code> em casos de erros
		*/
		private function onLoadResponse( table : HashMap, errorStr : StringRef ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					{
						var dataStream : ByteArrayEx = table.find( NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA );
						
						// Joga NANO_ONLINE_APPCUSTOMERDATA_PARAM_DATA fora
						dataStream.readByte();
						
						// Lê o tamanho dos dados
						var nBytes : uint = dataStream.readUnsignedInt();

						// Armazena os dados
						_data.clear();
						dataStream.readBytes( _data, 0, nBytes );
					}
					return true;
				
				case RC_ERROR_COULDNT_LOAD:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_LOAD );
					return false;
					
				case RC_ERROR_NO_DATA_TO_LOAD:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NO_GAME_TO_LOAD );
					return false;
					
				case RC_ERROR_INVALID_SLOT:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_SLOT );
					return false;
					
				default:
					trace( ">>> Unrecognized param return code sent to onLoadResponse in response to load request" );
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
			return true;
		}
	}
}