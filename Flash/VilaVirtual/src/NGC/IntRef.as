package NGC
{
	public final class IntRef
	{
		/**
		* Número contido pela referência
		*/
		private var _value : int;

		/**
		* Construtor
		*/		
		public function IntRef() 
		{
		}
		
		public function get content() : int
		{
			return _value;
		}
		
		public function set content( n : int ) : void
		{
			_value = n;	
		}
	}
}