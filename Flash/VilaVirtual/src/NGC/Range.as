package NGC
{
	public final class Range
	{
		private var _min : Number;
		private var _max : Number;

		// Construtor
		public function Range( rangeMin : Number, rangeMax : Number )
		{
			min = rangeMin;
			max = rangeMax;
		}
		
		// Getters
		public function get min() : Number
		{
			return _min;
		}
		
		public function get max() : Number
		{
			return _max;
		}
		
		// Setters
		public function set min( n : Number ) : void
		{
			_min = n;
		}
		
		public function set max( n : Number ) : void
		{
			_max = n; 
		}
	}
}