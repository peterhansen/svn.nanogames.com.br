package
{
	import NGC.ASWorkarounds.Enum;
	import NGC.ByteArrayEx;
	import NGC.NanoOnline.ISerializable;
	import NGC.Utils;
	
	public final class GameData implements ISerializable
	{
		private var _part : int;
		
		private var _tilemapIndex : int;
		
		private var _characterType : PlayerCharacterType;
		
		private var _itemsIds : Vector.< int >;
		
		private var _questsIds : Vector.< int >;
		private var _questsStates : Vector.< QuestState >;
		
		private var _flags : Object;
		
		private var _charactersData : Vector.< CharacterSaveInfo >;
		
		public function GameData()
		{
			_part = -1;
			_tilemapIndex = Constants.TILEMAP_NONE;
			_characterType = null;
			
			_itemsIds = new Vector.< int >();
			
			_questsIds = new Vector.< int >();
			_questsStates = new Vector.< QuestState >();
			
			_flags = new Object();
			
			_charactersData = new Vector.< CharacterSaveInfo >();
		}
		
		public function addItem( itemId : int ) : void
		{
			_itemsIds.push( itemId );
		}
		
		public function getItemsIds() : Vector.< int >
		{
			return _itemsIds;
		}
		
		public function addQuest( questId : int, state : QuestState ) : void
		{
			_questsIds.push( questId );
			_questsStates.push( state );
		}
		
		public function getQuestsIds() : Vector.< int >
		{
			return _questsIds;
		}
		
		public function getQuestsStates() : Vector.< QuestState >
		{
			return _questsStates;
		}
		
		public function set part( value : int ) : void
		{
			_part = value;
		}
		
		public function get part() : int
		{
			return _part;
		}
		
		public function set characterType( value : PlayerCharacterType ) : void
		{
			_characterType = value;
		}
		
		public function get characterType() : PlayerCharacterType
		{
			return _characterType;
		}
		
		public function set tilemapIndex( value : int ) : void
		{
			_tilemapIndex = value;
		}
		
		public function get tilemapIndex() : int
		{
			return _tilemapIndex;
		}
		
		public function get flags() : Object
		{
			return _flags; 	
		}
		
		public function set flags( value : Object ) : void
		{
			_flags = Utils.Clone( value );	
		}

		public function addCharacter( characterId : int, row : uint, column : uint, movementState : MovementState, expression : int ) : void
		{
			_charactersData.push( new CharacterSaveInfo( characterId, row, column, movementState, expression ) );
		}
		
		public function getCharacters() : Vector.< CharacterSaveInfo >
		{
			return _charactersData;
		}
		
		public function serialize( stream : ByteArrayEx ) : void
		{
			// Versão da serialização
			stream.writeUTF( Constants.APP_VERSION );
			
			// Personagem
			stream.writeByte( _characterType.Index );
			
			// Parte do jogo
			stream.writeByte( _part );
			
			// Tilemap
			stream.writeByte( _tilemapIndex );

			// Itens
			stream.writeByte( _itemsIds.length );
			for each( var itemId : uint in _itemsIds )
				stream.writeByte( itemId );
			
			// Quests
			var nQuests : uint = _questsIds.length;
			stream.writeShort( nQuests );
			for( var i : uint = 0 ; i < nQuests ; ++i )
			{
				stream.writeShort( _questsIds[i] );
				stream.writeByte( _questsStates[i].Index );
			}
			
			// Flags
			stream.writeObject( _flags );
			
			// Personagens
			stream.writeShort( _charactersData.length );
			for each( var c : CharacterSaveInfo in _charactersData )
			{
				stream.writeShort( c.characterId );
				stream.writeShort( c.row );
				stream.writeShort( c.column );
				stream.writeByte( c.movementState.Index );
				stream.writeInt( c.expression );
			}
		}
		
		public function unserialize( stream : ByteArrayEx ) : void
		{
			// Versão da serialização
			stream.readUTF();
			
			// Personagem
			_characterType = ( Enum.FromIndex( PlayerCharacterType, stream.readByte() ) as PlayerCharacterType );
			
			// Parte do jogo
			_part = stream.readByte();
			
			// Tilemap
			_tilemapIndex = stream.readByte();

			// Itens
			_itemsIds.splice( 0, _itemsIds.length );
			var nItems : int = stream.readByte();
			for( var itemCounter : int = 0 ; itemCounter < nItems ; ++itemCounter )
				_itemsIds.push( stream.readByte() );
			
			// Quests
			_questsIds.splice( 0, _questsIds.length );
			_questsStates.splice( 0, _questsStates.length );
			var nQuests : int = stream.readShort();
			for( var questCounter : int = 0 ; questCounter < nQuests ; ++questCounter )
			{
				_questsIds.push( stream.readShort() );
				_questsStates.push( ( Enum.FromIndex( QuestState, stream.readByte() ) as QuestState ) );
			}
			
			// Flags
			_flags = stream.readObject();
			
			// Personagens
			_charactersData.splice( 0, _charactersData.length );

			var nCharacters : int = stream.readShort();
			for( var characterCounter : int = 0 ; characterCounter < nCharacters ; ++characterCounter )
				_charactersData.push( new CharacterSaveInfo( stream.readShort(), stream.readShort(), stream.readShort(), Enum.FromIndex( MovementState, stream.readByte() ) as MovementState, stream.readInt() ) );
		}
	}
}
