package
{
	import NGC.ByteArrayEx;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOAppCustomerData;
	import NGC.NanoOnline.NOConnection;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoSounds.SoundManager;
	import NGC.ScheduledTask;
	import NGC.Utils;
	
	import fl.controls.Button;
	import fl.controls.ProgressBar;
	import fl.controls.ProgressBarDirection;
	import fl.controls.ProgressBarMode;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.*;
	
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.external.ExternalInterface;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.system.System;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	// TODO: Carregar parâmetros do JAVASCRIPT!!!!
	// - Ou alterar arquiv .html gerado para o programa 
	// - Ou http://www.adobe.com/livedocs/flash/9.0/ActionScriptLangRefV3/flash/external/ExternalInterface.html

	// Metatags que definem as propriedades padrão do stage
	//[SWF( width= "1024", height = "768", backgroundColor = "0x000000", frameRate = "32" )]
	//[SWF( width= "800", height = "600", backgroundColor = "0x000000", frameRate = "32" )]
	//[SWF( width= "640", height = "480", backgroundColor = "0x000000", frameRate = "24" )]
	[SWF( width= "640", height = "480", backgroundColor = "0x000000", frameRate = "60" )]
	public final class Application extends MovieClip implements IApplication, INOListener
	{
		// Valores de alpha de cena utilizados quando estamos mostrando o feedback de espera
		private static const SCENE_WAIT_NO_ALPHA : Number = 0.3;
		private static const SCENE_WAIT_FULL_ALPHA : Number = 1.0;
		
		/**
		* Perfil que está logado ou null caso o usuário esteja jogando sem fazer login/cadastrar-se 
		*/		
		private var _loggedProfile : NOCustomer;
		
		/**
		* Barra de progresso 
		*/		
		private var _loadingBar : ProgressBar;
		
		/**
		* Indica o estado (ponto atual) da aplicação 
		*/		
		private var _appState : ApplicationState;

		/**
		* Objeto que controla as transições do feedback de espera 
		*/		
		private var _waitFeedbackTweener : Tween;
		
		/**
		* Popup utilizado para exibir mensagens para o usuário 
		*/		
		private var _popup : PopUp;
		
		/**
		* Objeto utilizado para podermos executar um método assincronamente
		*/		
		private var _backToMenuScheduler: ScheduledTask;
		
		/**
		* Única instância da classe
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript
		*/
		private static var _singleton : Application;
		// OLD : Esta implementação de singleton não funciona com a classe que representa o jogo...
		//private static var _singleton : Application = new Application();
		
		/**
		* Construtor
		* OBS: AS3 não permite construtores privados e protegidos. Logo, apesar de se tratar de uma classe SINGLETON, temos
		* que manter o construtor público 
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript
		*/	
		public function Application()
		{
			super();
			
			// Se já construímos a variável estática...
			if( _singleton != null )
				throw new Error( "This is a singleton class. You should call GetInstance() method." );
			
			Security.loadPolicyFile( NOGlobals.NANO_ONLINE_URL );
			
			_loggedProfile = null;
			
			_singleton = this;
			
			tabChildren = false;
			tabEnabled = false;
			
			loadFonts();
			
			// Só quando formos adicionados ao stage é que poderemos utilizar a propriedade 'stage' do DisplayObject. Para resolver este
			// problema, utilizamos o evento ADDED_TO_STAGE. Iremos realizar as rotinas que deveriam estar no construtor apenas na
			// callback chamada pelo evento
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}
		
		/**
		* Ver Constants.as: "set RSC_GLOBAL_PATH"
		*/
		public function get resourcePath() : String
		{
			// Esta função é necessária por estarmos encapsulando o SWF da aplicação em código MXML, o que
			// torna flashvars inacessível
			return ExternalInterface.call( "GetResourcePath" );
		}

		/**
		* Retorna a única instância da classe
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript 
		* @return A única instância da classe 
		*/		
		public static function GetInstance() : Application
		{
			return _singleton;
		}

		/**
		* Evento chamado quando este DisplayObject é adicionado ao stage
		* @param evt Objeto que encapsula os dados do evento
		*/
		private function onAddedToStage( e : Event ) : void
		{
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			// Configura a barra de progresso
			_loadingBar = new ProgressBar();			
			_loadingBar.direction = ProgressBarDirection.RIGHT;
			_loadingBar.mode = ProgressBarMode.MANUAL;
			_loadingBar.minimum = 0.0;
			_loadingBar.maximum = 1.0;
			
			// Configura o popup
			//var font : FontLaCartoonerie = new FontLaCartoonerie();
			var font : Tahoma = new Tahoma();
			_popup = new PopUp( stage, new BoxInterfaceTopLeft(), true, 15, 15, 15, 15, font.fontName, 20, 0xB95C00, 0.5 );

			// Configura o NanoOnline
			NOConnection.GetInstance().host = NOGlobals.NANO_ONLINE_URL;
			NOConnection.GetInstance().appVersion = Constants.APP_VERSION;
			NOConnection.GetInstance().appShortName = Constants.NO_APP_SHORT_NAME;
			
			// OLD: Somente para agilizar os testes
			//setState( ApplicationState.APP_STATE_START );
//			setState( ApplicationState.APP_STATE_CUSTOM); // TODO
			setState( ApplicationState.APP_STATE_SPLASH );

			// OLD : Somente para testarmos a DialogBox sem termos que entrar no jogo
//			var bla : TextDialog = new TextDialog( stage.stageWidth, Constants.TEXT_DIALOG_HEIGHT );
//			addChild( bla );
//			bla.x = 0.0;
//			bla.y = stage.stageHeight - bla.height;
//			
//			bla.setBkgColor( false, 0x0 );
//			bla.setTextSpeed( Constants.TEXT_SPEED_FAST );
//			
//			var formatter : TextFormat = new TextFormat();
//			formatter.font = "Arial";
//			formatter.size = Constants.FONT_TEXT_DIALOG_SIZE;
//			formatter.color = Constants.FONT_COLOR_TEXT;
//			bla.setTextFormatter( formatter );
//			
//			var formatterAnswers : TextFormat = new TextFormat();
//			formatterAnswers.font = Constants.FONT_NAME_CARTOONERIE;
//			formatterAnswers.size = Constants.FONT_TEXT_DIALOG_SIZE;
//			formatterAnswers.color = Constants.FONT_COLOR_SELECTED;
//			bla.setAnswersFormatter( formatterAnswers );
//
//			var text: String = "Eu poderia até trocar, se conhecesse alternativas, mas no momento não tenho tempo para ver isso. Tenho prioridades!";
//			var answers : Array = [ "A senhora poderia se informar mais sobre o assunto.", null,
//									"A senhora poderia vender sacólas reutilizáveis para os clientes.", null,
//									"Com licença então, vou deixar a senhora trabalhar.", null
//								  ];
//
//			var text: String = "Eu pago meus impostos para manter a Vila limpa. Não quero colocar a minha mão no lixo se estou pagando pra outra pessoa fazer isso.";
//			var answers : Array = [ "Se conseguissemos organizar o mutirão seria muito mais rápido.", null,
//									"A rua está assim porque estamos produzindo muito lixo e também jogando na rua. É nossa responsabilidade cuidar do patrimônio público.", null,
//									"Tem razão. Desculpe incomodar.", null
//								  ];
//			
//			bla.setText( text, null, answers );
//			bla.show();
//			
//			bla.update( 1000000000 );
		}
		
		/**
		 * Indica que uma requisição foi enviada para o NanoOnline 
		 */
		public function onNORequestSent() : void
		{
			
		}
		
		/**
		 * Indica que a requisição foi cancelada pelo usuário 
		 */
		public function onNORequestCancelled() : void
		{
			
		}
		
		/**
		 * Indica que uma requisição foi respondida e terminada com sucesso
		 */
		public function onNOSuccessfulResponse() : void
		{
			trace( "OK" );
		}
		
		/**
		 * Sinaliza erros ocorridos nas operações do NanoOnline
		 * @param errorCode O código do erro ocorrido
		 * @param errorStr Descrição do erro ocorrido
		 */		
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			trace( "Error: " + errorCode + " => " + errorStr );
		}
		
		/**
		 * Indica o progresso da requisição atual
		 * @param currBytes A quantidade de bytes que já foi transferida
		 * @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		 */		
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			
		}
		
		/**
		* Retorna uma referência para a janela da aplicação 
		* @return Uma referência para a janela da aplicação
		*/		
		public function getStage() : Stage
		{
			return stage;
		}
		
		/**
		* Retorna o perfil que está logado ou <code>null</code> caso o usuário esteja jogando sem fazer login/cadastrar-se
		* @return O perfil que está logado ou <code>null</code> caso o usuário esteja jogando sem fazer login/cadastrar-se
		*/		
		public function getLoggedProfile() : NOCustomer
		{
			return 	_loggedProfile;
		}
		
		/**
		* Determina o estado da aplicação 
		* @param newState O novo estado da aplicação
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/		
		public function setState( newState : ApplicationState, frame : Object = null ) : void
		{
			trace( "setState( " + newState + ", " + frame + " )" );
			var sm : SoundManager = SoundManager.GetInstance();
			
			var tempProfile : NOCustomer;
			if( _appState == ApplicationState.APP_STATE_FORM )
			{
				tempProfile = ( getCurrScene() as SceneFormEx ).createdProfile;
			}
			
			// alteração para ignorar profiles
			//else if( _appState == ApplicationState.APP_STATE_LOGIN )
			//{
			//	tempProfile = ( getCurrScene() as SceneLoginEx ).loggedProfile;
			//}
			
			if( tempProfile != null ) 
				_loggedProfile = tempProfile;
			
			var nextScene : AppScene = null;
			switch( newState )
			{
				case ApplicationState.APP_STATE_START:
					nextScene = new AppScene( new SceneStart() );
					
					// alteração para esconder botão de login e cadastro
					var scene = nextScene.getChildAt( 0 );
					var loginButton = scene.getChildByName( "mcStartNow" );
					loginButton.visible = false;					
					break;
				
				case ApplicationState.APP_STATE_SPLASH:
					playMenuTheme();
					nextScene = new AppScene( new SceneSplash() );
					break;
				
				// alteração para esconder botão de login e cadastro
				//case ApplicationState.APP_STATE_LOGIN:
					//nextScene = new SceneLoginEx( _loggedProfile );
					//break;
				
				case ApplicationState.APP_STATE_FROM_VILA_TO_MENU:
					sm.unloadAllSounds();
					VilaVirtual.Seal();
					nextScene = new SceneMenuEx( true, true, false );
					
					// alteração para esconder botão de login e cadastro
					var scene = nextScene.getChildAt( 0 );
					var continueButton = scene.getChildByName( "mcContinue" );
					continueButton.visible = false;
					
					nextScene.addEventListener( Event.ADDED, onFromVilaToMenu );
					playMenuTheme();
					break;
					
				// alteração para esconder botão de login e cadastro
				case ApplicationState.APP_STATE_LOGIN:
				case ApplicationState.APP_STATE_MENU:
					nextScene = new SceneMenuEx();
					
					var scene = nextScene.getChildAt( 0 );
					var continueButton = scene.getChildByName( "mcContinue" );
					continueButton.visible = false;
					
					break;
				
				case ApplicationState.APP_STATE_SUBMENU:
					nextScene = new AppScene( new SceneSubMenu() );
					
					// alteração para esconder botão de login e cadastro
					var scene = nextScene.getChildAt( 0 );
					var rankingMenu = scene.getChildAt( 4 );
					var rankingButton = rankingMenu.getChildByName( "mcRanking" );
					rankingButton.visible = false;
					break;
				
				case ApplicationState.APP_STATE_ABOUT:
					nextScene = new AppScene( new SceneAbout() );
					break;
				
				case ApplicationState.APP_STATE_RANKING:
					nextScene = new SceneRankingEx();
					break;
				
				case ApplicationState.APP_STATE_HELP:
					nextScene = new AppScene( new SceneHelp() );
					break;
				
				case ApplicationState.APP_STATE_CUSTOM:
					nextScene = new SceneCustomizeEx();
					break;
					
				case ApplicationState.APP_STATE_CREDITS:
					nextScene = new AppScene( new SceneCredits() );
					break;
				
				case ApplicationState.APP_STATE_RECOMMEND:
					// TODO : Retirar o return, descomentar e implementar, dentro da classe SceneRecommendEx, a função de envio de email
					//nextScene = new SceneRecommendEx();
					//break;
					return;
				
				case ApplicationState.APP_STATE_FORM:
					nextScene = new SceneFormEx();
					break;
				
				case ApplicationState.APP_STATE_PASSWORD:
					nextScene = new ScenePasswordEx();
					break;

				case ApplicationState.APP_STATE_MINIGAME_DENGUE:
					sm.unloadAllSounds();					
					nextScene = new SceneDengueEx( _loggedProfile, true, onMinigameFromMenuEnded );
					break;
				
				case ApplicationState.APP_STATE_MINIGAME_DENGUE_FROM_VILA:
					sm.unloadAllSounds();					
					nextScene = new SceneDengueEx( null, false, onMinigameFromVilaEnded, true );
					break;
				
				case ApplicationState.APP_STATE_MINIGAME_RECYCLING:
					sm.unloadAllSounds();
					nextScene = new SceneRecyclingEx( _loggedProfile, true, onMinigameFromMenuEnded );
					break;
					
				case ApplicationState.APP_STATE_MINIGAME_RECYCLING_FROM_VILA:
					sm.unloadAllSounds();
					nextScene = new SceneRecyclingEx( null, false, onMinigameFromVilaEnded, true );
					break;
				
				case ApplicationState.APP_STATE_MINIGAMES:
					if( ( _appState == ApplicationState.APP_STATE_MINIGAME_DENGUE )
						|| ( _appState == ApplicationState.APP_STATE_MINIGAME_RECYCLING ) )
					{
							playMenuTheme();
					}
					nextScene = new AppScene( new SceneMinigames() );
					break;
				
				case ApplicationState.APP_STATE_CONTINUE:
					sm.unloadAllSounds();
					nextScene = new VilaVirtual( true, null, ( getCurrScene() as SceneMenuEx ).loadedData.data );
					break;
				
				case ApplicationState.APP_STATE_NEWGAME:
					sm.unloadAllSounds();
					nextScene = new VilaVirtual( false, ( getCurrScene() as SceneCustomizeEx ).choseBoy() ? PlayerCharacterType.CHARACTER_BOY : PlayerCharacterType.CHARACTER_GIRL );
					break;
				
				case ApplicationState.APP_STATE_BACK_TO_GAME:
					sm.stopAllSounds();
					nextScene = VilaVirtual.GetInstance();
					break;
				
				case ApplicationState.APP_STATE_FROM_MINIGAME_BACK_TO_VILA:
					sm.unloadAllSounds();

					var minigameScore : int = -1;
					var gameFinished : Boolean = false;
					var mainGame : VilaVirtual = VilaVirtual.GetInstance();

					if( _appState == ApplicationState.APP_STATE_MINIGAME_DENGUE_FROM_VILA )
					{
						gameFinished = ( getCurrScene() as SceneDengueEx ).getFinishedGame();
						minigameScore = ( getCurrScene() as SceneDengueEx ).getScore();
					}
					else //if( _appState == ApplicationState.APP_STATE_MINIGAME_RECICLING_FROM_VILA )
					{
						gameFinished = ( getCurrScene() as SceneRecyclingEx ).getFinishedGame();
						minigameScore = ( getCurrScene() as SceneRecyclingEx ).getScore();
					}

					mainGame.setMinigameResult( _appState, gameFinished, minigameScore );
					nextScene = mainGame;
					break;
				
				default:
					throw new ArgumentError( "Invalid Application State" );
					break;
			}
			
			trace( "Indo para a cena " + newState );
			
			// TODO : Fazer isso de uma forma mais esperta (com estados)
			// Garante que a tela não está travada na animação de espera
			if( getCurrScene() != null )
			{
				cancelWaitTween();
				hideWaitFeedback();
				cancelWaitTween();
			}

			changeCurrScene( nextScene, frame );
			_appState = newState;
		}
		
		/**
		* Callback chamada quando devemos ir para o menu, vindo do jogo
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onFromVilaToMenu( e : Event ) : void
		{
			_backToMenuScheduler = new ScheduledTask( 50, onFromVilaToMenuAfterDelay );
			_backToMenuScheduler.call();
		}
		
		/**
		* Callback chamada, após um delay, para de fato exibirmos a tela do menu após sairmos da tela de jogo 
		*/		
		private function onFromVilaToMenuAfterDelay() : void
		{
			_backToMenuScheduler = null;
			
			var currScene : AppScene = getCurrScene();
			
			// No modo Release vem null...
			if( currScene != null )
				currScene.removeEventListener( Event.ADDED, onFromVilaToMenu );
			
			VilaVirtual.KillSingleton( false );
			
			// No modo Release vem null...
			getCurrScene().dispatchEvent( new Event( AppScene.APPSCENE_LOADING_COMPLETE ) );
		}

		/**
		* Volta para a cena anterior (o menu)
		*/
		private function onMinigameFromMenuEnded() : void
		{
			Mouse.cursor = MouseCursor.AUTO;
			setState( ApplicationState.APP_STATE_MINIGAMES, "LabelChooseMinigame" );
		}
		
		/**
		* Volta para a cena anterior (a tela de jogo)
		*/
		private function onMinigameFromVilaEnded() : void
		{	
			Mouse.cursor = MouseCursor.AUTO;	
			setState( ApplicationState.APP_STATE_FROM_MINIGAME_BACK_TO_VILA );	
		}
		
		/**
		* Inicia a reprodução da música do menu  
		*/		
		public function playMenuTheme() : void
		{
			var sm : SoundManager = SoundManager.GetInstance();
			var musicMenuTheme : MenuTheme = new MenuTheme();
			sm.playSoundAtIndex( sm.pushSound( musicMenuTheme ) );
		}
		
		/**
		* Trava o tratamento de input da cena atual e exibe um feedback de espera para o usuário 
		*/		
		public function showWaitFeedback( cancelable : Boolean = false, onCancelCallback : Function = null ) : void
		{
			if( cancelable )
			{
				// TODO
				//showPopUp();
			}

			doWaitAnimTween( SCENE_WAIT_FULL_ALPHA, SCENE_WAIT_NO_ALPHA, onStartWaitAnimEnded );
		}
		
		/**
		* Força a animação de feedback de espera a terminar   
		*/		
		private function cancelWaitTween() : void
		{
			if( _waitFeedbackTweener != null )
			{
				_waitFeedbackTweener.fforward();
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onStartWaitAnimEnded );
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onHideWaitAnimEnded );
			}
		}

		/**
		* Modifica o valor de alpha da cena atual utilizando animação 
		* @param begin O valor que a propriedade alpha deve possuir antes do início da animação
		* @param end O valor que a propriedade alpha deverá ter ao término da animação
		* @param onDoneCallback A callback que deve ser chamada quando a animação acabar
		*/		
		private function doWaitAnimTween( begin : Number, end : Number, onDoneCallback : Function ) : void
		{
			var currScene : AppScene = ( getCurrScene() as AppScene );
			currScene.blendMode = BlendMode.LAYER;
			
			cancelWaitTween();
			
			_waitFeedbackTweener = TweenManager.tween( currScene, "alpha", Regular.easeInOut, begin, end, Constants.WAIT_ANIM_DUR );
			_waitFeedbackTweener.addEventListener( TweenEvent.MOTION_FINISH, onDoneCallback );
			_waitFeedbackTweener.start();
			
			currScene.mouseEnabled = false;
			currScene.mouseChildren = false;
		}
	
		/**
		* Callback chamada quando acabamos de fazer a animação de exibir o feedback de espera 
		* @param e O objeto que encapsula os dados do evento
		*/
		private function onStartWaitAnimEnded( e : TweenEvent ) : void
		{
			if( _waitFeedbackTweener != null )
			{
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onStartWaitAnimEnded );
				_waitFeedbackTweener = null;
			}
		}
		
		/**
		* Callback chamada quando acabamos de fazer a animação de esconder o feedback de espera 
		* @param e O objeto que encapsula os dados do evento
		*/		
		private function onHideWaitAnimEnded( e : TweenEvent ) : void
		{
			if( _waitFeedbackTweener != null )
			{
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onHideWaitAnimEnded );
				_waitFeedbackTweener = null;
			}
			
			var currScene : AppScene = ( getCurrScene() as AppScene );
			currScene.blendMode = BlendMode.NORMAL;
			currScene.mouseEnabled = true;
			currScene.mouseChildren = true;
		}
		
		/**
		* Esconde o feedback de espera e libera o tratamento de input da cena atual
		*/		
		public function hideWaitFeedback() : void
		{
			doWaitAnimTween( SCENE_WAIT_NO_ALPHA, SCENE_WAIT_FULL_ALPHA, onHideWaitAnimEnded ); 
		}
		
		/**
		* Exibe um popup para o usuário travando o tratamento de input da cena atual enquanto
		* o mesmo estiver visível
		*/		
		public function showPopUp( msg : String, ...btsTitlesAndCallbacks ) : void
		{
			var args : Array = btsTitlesAndCallbacks;
			args.unshift( msg );
			_popup.show.apply( null, args );
		}
		
		/**
		* Retorna a cena atual da aplicação 
		* @return A cena atual da aplicação
		*/		
		public function getCurrScene() : AppScene	
		{
			if( numChildren == 0 )
				return null;
			return ( getChildAt( 0 ) as AppScene );
		}
		
		/**
		* Retira a cena atual da memória 
		*/		
		private function deleteCurrScene() : void
		{
			var nChildren : int = numChildren;
			for( var i : int = 0 ; i < nChildren ; ++i )
			{
				var temp : AppScene = ( getChildAt( 0 ) as AppScene );
				if( temp != null )
					temp.stop();

				removeChildAt( 0 );
			}
		}
		
		/**
		* Modifica a cena atual da aplicação de acordo com suas configurações 
		* @param nextScene A cena para qual devemos transitar
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/		
		private function changeCurrScene( nextScene : AppScene, frame : Object = null ) : void
		{			
			trace( "changeCurrScene( " + nextScene + ", " + frame + " ) -> " + nextScene.asynchronousLoading );
			if( nextScene.asynchronousLoading )
			{
				// Faz a transição
				var currScene : AppScene = getCurrScene();
				currScene.mouseChildren = false;
				currScene.mouseEnabled = false;
				currScene.stop();
				
				var transition : CircleTransition = new CircleTransition( currScene, Constants.TRANSITION_DUR, Constants.STAGE_FPS );
				transition.start( 0.0, this, onCloseTransitionEnded, nextScene, frame );
			}
			else
			{
				setCurrScene( nextScene, frame );
			}
		}
		
		/**
		* Determina a cena principal da aplicação 
		* @param nextScene A nova cena principal
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/
		private function setCurrScene( nextScene : AppScene, frame : Object = null ) : void
		{
			deleteCurrScene();

			if( frame != null )
			{
				nextScene.stop();
				nextScene.gotoAndPlay( frame );
			}
			
			addChildAt( nextScene, 0 );

			if( nextScene.anchorAtCenter )
			{
				nextScene.x = stage.stageWidth * 0.5;
				nextScene.y = stage.stageHeight * 0.5;
			}
			else
			{
				nextScene.x = 0.0;
				nextScene.y = 0.0;
			}
		}

		/**
		* Callback chamada quando a transição escurece a tela por completo 
		* @param args Os parâmetros da callback 
		*/		
		private function onCloseTransitionEnded( ...args ) : void
		{
			trace( "onCloseTransitionEnded" + args );
			var nextScene : AppScene = args[0];
			nextScene.addEventListener( AppScene.APPSCENE_LOADING_COMPLETE, onSceneLoaded );
			nextScene.addEventListener( ProgressEvent.PROGRESS, onLoadingProgressChanged );

			// Começa a carregar a cena
			nextScene.visible = false;
			setCurrScene( nextScene, args[1] );

			if( nextScene.usesProgressEvent )
				showProgressBar();
		}
		
		/**
		* Exibe a barra de progresso  
		*/		
		private function showProgressBar() : void
		{
			trace( "showProgressBar()" ); 
			_loadingBar.reset();
			_loadingBar.setProgress( 0.0, 1.0 );
			
			addChild( _loadingBar );

			_loadingBar.x = ( stage.stageWidth - _loadingBar.width ) * 0.5;
			_loadingBar.y = ( stage.stageHeight - _loadingBar.height ) * 0.5;
		}
		
		/**
		* Esconde a barra de progresso  
		*/		
		private function hideProgressBar() : void
		{
			if( _loadingBar.parent == this )
				removeChild( _loadingBar );
		}
		
		/**
		* Callback chamada quando uma cena de carregamento assíncrono acaba de ser carregada 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onSceneLoaded( e : Event ) : void
		{
			trace( "onSceneLoaded" );
			var currScene : AppScene = getCurrScene();
			currScene.removeEventListener( AppScene.APPSCENE_LOADING_COMPLETE, onSceneLoaded );
			currScene.removeEventListener( ProgressEvent.PROGRESS, onLoadingProgressChanged );
			
			hideProgressBar();

			var transition : CircleTransition = new CircleTransition( currScene, Constants.TRANSITION_DUR, Constants.STAGE_FPS, false );
			transition.start( 0.0, this, onOpenTransitionEnded );
			
			currScene.visible = true;
		}
		
		/**
		* Carrega as fontes utilizadas no jogo
		* Além de linkar a biblioteca (.SWC) que contém a classe da fonte com o projeto, é necessário instanciar cada fonte utilizada ao menos uma vez. Sem fazê-lo, o programa
		* não irá reconhecer as fontes embarcadas. Isto acontece porque as fontes são registradas na run-time somente quando invocamos seus construtores.
		*/		
		public static function loadFonts() : void
		{
			var tahomaFont : Tahoma = new Tahoma();
			
			var laCartoonerieFont : FontLaCartoonerie = new FontLaCartoonerie();
			var chinaCatFont : FontChinaCat = new FontChinaCat();
			var toonTownIndustrialFontReg : FontToonTownIndustrialCond = new FontToonTownIndustrialCond();
			var toonTownIndustrialFontCond : FontToonTownIndustrialRegular = new FontToonTownIndustrialRegular();
			var addervilleFont : FontAddervilleBook = new FontAddervilleBook();
		}
		
		/**
		* Callback chamada quando a transição que torna a cena visível acaba 
		* @param args Os parâmetros da callback
		*/		
		private function onOpenTransitionEnded( ...args ) : void
		{
			trace( "terminou opening" );

			var currScene : AppScene = getCurrScene();
			
			// Gambiarra!!!!!!!!!!!!!!!!
			if( Utils.GetObjClass( currScene ) == VilaVirtual )
			{
				var gameScene : VilaVirtual = ( currScene as VilaVirtual );
				gameScene.startRunning();
			}

			currScene.mouseChildren = true;
			currScene.mouseEnabled = true;
		}
		
		/**
		* Evento que informa quando o progresso de carregamento do arquivo SWF muda
		* @param e Objeto que encapsula os dados do evento
		*/		
		private function onLoadingProgressChanged( e : ProgressEvent ) : void
		{
			var percent : Number = e.bytesLoaded / e.bytesTotal;
			_loadingBar.setProgress( percent, _loadingBar.maximum );
		}
	}
}
