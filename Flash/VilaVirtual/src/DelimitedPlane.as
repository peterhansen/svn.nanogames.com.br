package
{
	import flash.geom.Vector3D;
	import NGC.NanoGeom.Plane;
	
	public class DelimitedPlane extends Plane
	{
		// Dimensões do plano
		private var _width : Number;
		private var _height : Number;
		
		// Posição da quina superior esquerda do plano
		private var _pos : Vector3D;

		// Construtor
		public function DelimitedPlane( top : Vector3D, right : Vector3D, pos : Vector3D, w : Number, h : Number )
		{
			super( top, right, pos );

			position = pos;
			width = w;
			height = h;
		}
		
		// Getters e Setters
		public function get width() : Number
		{
			return _width;
		}
		
		public function set width( w : Number ) : void
		{
			_width = w;
		}
		
		public function get height() : Number
		{
			return _height;
		}
		
		public function set height( h : Number ) : void
		{
			_height = h;
		}
		
		public function get position() : Vector3D
		{
			return _pos;
		}
		
		public function set position( p : Vector3D ) : void
		{
			_pos = p;
		}
	}
}