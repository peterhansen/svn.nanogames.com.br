package
{
	import Dengue.Dengue;
	import ElementosMundoDeJogo.DengueJogo;
	
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoOnline.NOTextsIndexes;
	import NGC.ScheduledTask;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public final class SceneDengueEx extends AppScene implements INOListener
	{
		/**
		* Delay aplicado à _scheduledMinigameEnd
		*/		
		private static const DELAY_BEFORE_END : int = 100;

		/**
		* O jogo propriamente dito 
		*/		
		private var _minigameDengue : Dengue;
		
		/**
		* Indica se estamos jogando para conseguir uma nova pontuação para o NanoOnline ou se estamos jogando no modo história  
		*/		
		private var _rankingModeOn : Boolean;
		
		/**
		* Timer responsável por encerrar os minigames. Assim garantimos que não estaremos executando código do minigame quando este for encerrado 
		*/		
		private var _scheduledMinigameEnd : ScheduledTask;
		
		/**
		* Callback a ser chamada quando o jogo terminar 
		*/		
		private var _onEndCallback : Function;
		
		/**
		* Objeto que faz a integração com o NanoOnline 
		*/		
		private var _noRnk : NORanking;
		
		/**
		* Perfil que está logado ou null caso o usuário esteja jogando sem fazer login/cadastrar-se 
		*/		
		private var _loggedCustomer : NOCustomer;

		/**
		* Indica se o usuário zerou o jogo. Quando _rankingModeOn for <code>true</code>, esta variável será sempre <code>false</code>
		*/		
		private var _finishedGame : Boolean;
		
		/**
		* Construtor 
		*/		
		public function SceneDengueEx( customer : NOCustomer, rankingModeOn : Boolean, onEndCallback : Function, useTransition : Boolean  = false )
		{
			super( new Dengue(), false, useTransition, false  );
			
			_minigameDengue = ( innerScene as Dengue );
			
			_rankingModeOn = rankingModeOn;
			_onEndCallback = onEndCallback;
			_loggedCustomer = customer;
			_finishedGame = false;
			
			// OLD: Só não iremos submeter os recordes 
//			if( _rankingModeOn && ( _loggedCustomer == null ) )
//				throw new ArgumentError( "Cannot run with ranking mode on without a logged customer" );
			
			_noRnk = new NORanking( Constants.NO_RNK_TYPE_DENGUE, Constants.NO_RNK_TYPE_DENGUE_MAX_POINTS );
			
			addEventListener( Event.ADDED, onMinigameDengueAdded );
		}
		
		/**
		* Callback chamada assim que o minigame é adicionado à displaylist da aplicação 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onMinigameDengueAdded( e : Event ) : void
		{
			removeEventListener( Event.ADDED, onMinigameDengueAdded );
			_minigameDengue.run( onDengueRunning );
		}
		
		/**
		* Callback chamada assim que o minigame começa a executar 
		* @param minigame O minigame que começou a executar
		*/		
		private function onDengueRunning( minigame : Dengue ) : void
		{
			_minigameDengue.setSairCallback( onScheduleMinigameDengueEnd );
			_minigameDengue.setIsStoryMode( !_rankingModeOn );
			
			if( _rankingModeOn )
			{
				_minigameDengue.setCheckAndSubmitRecordsCallback( onDengueMinigameGameOver );
			}
			else
			{
				_minigameDengue.setGameStateNotifier( onDengueMinigameChangedState );
			}
			
			// Avisa ao pai que acabamos de carregar a cena
			if( asynchronousLoading )
				dispatchEvent( new Event( APPSCENE_LOADING_COMPLETE ) );
		}
		
		/**
		* Callback chamada toda vez que o minigame muda de estado
		* @param newState O estado no qual o jogo acabou de entrar 
		*/		
		private function onDengueMinigameChangedState( newState : int ) : void
		{
			if( newState == DengueJogo.GAMESTATE_WIN )
				_finishedGame = true;
		}
		
		/**
		* Callback chamada quando o usuário morre 
		* @param score A pontuação alcançada pelo usuário 
		*/		
		private function onDengueMinigameGameOver( score : int ) : Boolean
		{
			if( !_rankingModeOn || ( score <= 0 ) || ( _loggedCustomer == null ) )
			{
				resetGame();
			}
			else
			{
				// TODO : Poderíamos utilizar o parâmetro recebido, mas temos que testar se está vindo correto... getScore() está 100% correto
				var newScore : int = _minigameDengue.getScore();
				
				_noRnk.clear();
				
				var entriesToSubmit : Vector.<NORankingEntry > = new Vector.<NORankingEntry >();
				entriesToSubmit.push( new NORankingEntry( _loggedCustomer.profileId, newScore, _loggedCustomer.nickname ) );
				_noRnk.localEntries = entriesToSubmit;
				
				// Submete a pontuação para o NanoOnline
				if( !NORanking.SendSubmitRequest( _noRnk, this ) )
				{
					( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), resetGame );
				}
			}
			return false;
		}
		
		/**
		* Manda o jogo de volta para a sua tela inicial 
		*/		
		private function resetGame() : void
		{
			_minigameDengue.startNewGame();
		}
		
		/**
		* Retorna se o usuário zerou o jogo. Quando _rankingModeOn for <code>true</code>, esta variável será sempre <code>false</code>
		* @return <code>true</code> se o usuário zerou o jogo, <code>false</code> caso contrário
		*/		
		public function getFinishedGame() : Boolean
		{
			return _finishedGame;
		}
		
		/**
		* Retorna a pontuação feita pelo jogador 
		* @return A pontuação feita pelo jogador
		*/		
		public function getScore() : int
		{
			return _minigameDengue.getScore();
		}
		
		/**
		* Callback chamada sempre que o minigame deva ser finalizado 
		*/		
		private function onScheduleMinigameDengueEnd() : void
		{
			_minigameDengue.setSairCallback( null );
			_minigameDengue.setGameStateNotifier( null );
			
			_scheduledMinigameEnd = new ScheduledTask( DELAY_BEFORE_END, _onEndCallback );
			_scheduledMinigameEnd.call();
		}
		
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		public function onNOSuccessfulResponse():void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_RECORDS_SUBMITTED ),
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), resetGame );
		}
		
		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_SUBMIT_RECORD )+ ":\n" + errorStr,
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), resetGame );
		}
		
		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/	
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}