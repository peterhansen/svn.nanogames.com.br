package
{
	public final class SceneCustomizeEx extends AppScene
	{
		public function SceneCustomizeEx()
		{
			super( new SceneCustomize );
		}
		
		public function choseBoy() : Boolean
		{
			return ( innerScene as SceneCustomize ).choseBoy();
		}
	}
}