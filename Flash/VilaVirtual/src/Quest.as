package
{
	import flash.events.Event;
	import flash.events.EventDispatcher;

	/**
	* Representa uma missão que deve ser realizada pelo usuário
	* @author Daniel L. Alves
	*/	
	public final class Quest extends EventDispatcher
	{
		// Identificadores de eventos específicos da classe
		public static const EVENT_QUEST_DISCOVERED : String = "questDiscovered";
		public static const EVENT_QUEST_CLOSED : String = "questClosed";
		
		/**
		* ID única da quest 
		*/		
		private var _id : uint;
		
		/**
		* Nome da missão 
		*/		
		private var _name : String;
		
		/**
		* Descrição detalhada da missão 
		*/		
		private var _about : String;
		
		/**
		 * Experiência fornecida ao se completar a missão 
		 */		
		private var _xp : uint; 
		
		/**
		* Indica o estado da missão 
		*/		
		private var _state : QuestState;
		
		/**
		* Data na qual a missão foi descoberta 
		*/		
		private var _startDate : Date;
		
		/**
		* Data na qual a missão foi completada 
		*/		
		private var _endDate : Date;

		/**
		* Construtor 
		*/		
		public function Quest( questId : uint = 0, questName : String = null, questAbout : String = null, questXp : uint = 0 )
		{
			// Utiliza os setters destes atributos
			id = questId;
			name = questName;
			about = questAbout;
			xp = questXp;
				
			// Não utiliza os setters destes atributos
			_state = QuestState.QUEST_STATE_UNDISCOVERED;
			_startDate = null;
			_endDate = null;
		}
		
		public function set id( i : uint ) : void
		{
			_id = i;
		}
		
		public function get id() : uint
		{
			return _id;
		}
		
		public function set name( str : String ) : void
		{
			_name = str;
		}

		public function get name() : String
		{
			return _name;
		}
		
		public function set about( str : String ) : void
		{
			_about = str;
		}
		
		public function get about() : String
		{
			return _about;
		}
		
		public function set xp( xpAmount : uint ) : void
		{
			_xp = xpAmount;
		}
		
		public function get xp() : uint
		{
			return _xp;
		}
		
		public function set state( s : QuestState ) : void
		{
			const currState : QuestState = state;

			if( s != currState )
			{
				switch( s )
				{
					case QuestState.QUEST_STATE_UNDISCOVERED:
						throw Error( "Cannot set quest to QUEST_STATE_UNDISCOVERED state" );
						break;

					case QuestState.QUEST_STATE_INCOMPLETE:
						if( currState != QuestState.QUEST_STATE_UNDISCOVERED )
						{
							// OLD: Dava problema quando estávamos carregando um jogo
							//throw Error( "Quest already closed" );
							trace( "Quest already closed" );
						}

						_state = s;

						// 'new Date' sem parâmetros retorna o momento atual
						_startDate = new Date();

						dispatchEvent( new Event( Quest.EVENT_QUEST_DISCOVERED ) );
						break;

					case QuestState.QUEST_STATE_COMPLETE:
						if( currState != QuestState.QUEST_STATE_INCOMPLETE )
						{
							// OLD: Dava problema quando estávamos carregando um jogo
							//throw Error( "Quest is not opened" );
							trace( "Quest is not opened" );
						}

						_state = s;

						// 'new Date' sem parâmetros retorna o momento atual
						_endDate = new Date();

						dispatchEvent( new Event( Quest.EVENT_QUEST_CLOSED ) );
						break;
					
					default:
						throw new Error( "Invalid quest state" );
				}
			}
		}
		
		public function get state() : QuestState
		{
			return _state;
		}
		
		public function get startDate() : Date
		{
			return _startDate;
		}
		
		public function get endDate() : Date
		{
			return _endDate;
		}
	}
}
