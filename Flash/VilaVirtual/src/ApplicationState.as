package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class ApplicationState extends Enum
	{
		// "Construtor" estático
		{ initEnum( ApplicationState ); }
		
		// Possíveis estados de movimentação dos objetos não estáticos
		public static const APP_STATE_START : ApplicationState							= new ApplicationState();
		public static const APP_STATE_SPLASH : ApplicationState							= new ApplicationState();
		public static const APP_STATE_LOGIN : ApplicationState							= new ApplicationState();
		public static const APP_STATE_MENU : ApplicationState							= new ApplicationState();
		public static const APP_STATE_MINIGAMES : ApplicationState						= new ApplicationState();
		public static const APP_STATE_MINIGAME_DENGUE : ApplicationState				= new ApplicationState();
		public static const APP_STATE_MINIGAME_RECYCLING : ApplicationState				= new ApplicationState();
		public static const APP_STATE_SUBMENU : ApplicationState						= new ApplicationState();
		public static const APP_STATE_ABOUT : ApplicationState							= new ApplicationState();
		public static const APP_STATE_RANKING : ApplicationState						= new ApplicationState();
		public static const APP_STATE_HELP : ApplicationState							= new ApplicationState();
		public static const APP_STATE_CUSTOM : ApplicationState							= new ApplicationState();
		public static const APP_STATE_CREDITS : ApplicationState						= new ApplicationState();
		public static const APP_STATE_RECOMMEND : ApplicationState						= new ApplicationState();
		public static const APP_STATE_FORM : ApplicationState							= new ApplicationState();
		public static const APP_STATE_PASSWORD : ApplicationState						= new ApplicationState();
		public static const APP_STATE_CONTINUE : ApplicationState						= new ApplicationState();
		public static const APP_STATE_NEWGAME : ApplicationState						= new ApplicationState();
		public static const APP_STATE_MINIGAME_DENGUE_FROM_VILA : ApplicationState		= new ApplicationState();
		public static const APP_STATE_MINIGAME_RECYCLING_FROM_VILA : ApplicationState	= new ApplicationState();
		public static const APP_STATE_FROM_MINIGAME_BACK_TO_VILA : ApplicationState		= new ApplicationState();
		public static const APP_STATE_BACK_TO_GAME : ApplicationState					= new ApplicationState();
		public static const APP_STATE_FROM_VILA_TO_MENU : ApplicationState				= new ApplicationState();
	}
}
