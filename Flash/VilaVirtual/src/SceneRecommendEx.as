package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public final class SceneRecommendEx extends AppScene
	{
		/**
		* Formulário que o usuário deve preencher antes de enviar a indicação 
		*/		
		private var _recommendForm : RecommendText;
		
		/**
		* Construtor 
		*/		
		public function SceneRecommendEx()
		{
			super( new SceneRecommend() );
			
			_recommendForm = ( innerScene as SceneRecommend ).getRecommendForm();
			_recommendForm.getBtSend().addEventListener( MouseEvent.CLICK, onSend );
		}
		
		/**
		* Callback chamada quando o usuário clica no botão de enviar indicação 
		* @param e O objeto que encapsula os dados do evento
		*/		
		private function onSend( e : MouseEvent ) : void
		{
			// Obtém os dados
			var name : String = _recommendForm.getFriendName();
			var email : String = _recommendForm.getFriendEmail();
				
			// Envia o email
			// TODO
		}
	}
}