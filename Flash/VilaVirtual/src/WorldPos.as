package
{
	import flash.geom.Vector3D;
	
	/**
	* 
	* @author Daniel L. Alves
	* 
	*/	
	public final class WorldPos extends Vector3D
	{
		/**
		* Construtor 
		* @param x Coordenada x de uma entidade no sistema de coordenadas no mundo dos tiles
		* @param y Coordenada y de uma entidade no sistema de coordenadas no mundo dos tiles
		* @param z Coordenada z de uma entidade no sistema de coordenadas no mundo dos tiles
		*/		
		public function WorldPos( x : Number = 0.0, y : Number = 0.0, z : Number = 0.0 )
		{
			super( x, y, z, 0.0 );
		}
		
		override public function clone() : Vector3D
		{
			return new WorldPos( x, y, z );
		}
	}
}