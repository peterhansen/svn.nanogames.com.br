package
{
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOConnection;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NOTextsIndexes;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public final class SceneLoginEx extends AppScene implements INOListener
	{
		/**
		* Formulário onde o usuário irá fornecer seus dados 
		*/				
		private var _noForm : LoginForm;
		
		/**
		* Objeto que realiza a integração com o NanoOnline
		*/		
		private var _noCustomer : NOCustomer;
		
		/**
		* Construtor
		*/		
		public function SceneLoginEx( alreadyLoggedProfile : NOCustomer )
		{
			super( new SceneLogin() );
			
			var temp : SceneLogin = ( innerScene as SceneLogin );
			_noForm = temp.getLoginForm();
			temp.getBtLogin().addEventListener( MouseEvent.CLICK, onLogin );
			temp.getBtPlayWithoutLogin().addEventListener( MouseEvent.CLICK, onPlayWithoutLogin );
			
			if( alreadyLoggedProfile == null )
			{
				_noCustomer = new NOCustomer();
			}
			else
			{
				_noCustomer = alreadyLoggedProfile
					
				_noForm.setNickname( _noCustomer.nickname );
				_noForm.setPassword( _noCustomer.password );
			}
		}
		
		private function onPlayWithoutLogin( e : MouseEvent ) : void
		{
			var btPlayWithoutLogin : PlayNow = ( innerScene as SceneLogin ).getBtPlayWithoutLogin();
			btPlayWithoutLogin.mouseEnabled = false;
			btPlayWithoutLogin.removeEventListener( MouseEvent.CLICK, onPlayWithoutLogin );
			
			innerScene.gotoAndPlay( "Exit" );
		}
		
		/**
		* Callback chamada quando o usuário clica no botão de login (formulário de
		* login do NanoOnline)
		* @param e Objeto que encapsula os dados do evento  
		*/		
		private function onLogin( e : MouseEvent ) : void
		{
			try
			{
				_noCustomer.nickname = _noForm.getNickname();
				_noCustomer.password = _noForm.getPassword();

				if( !NOCustomer.SendLoginRequest( _noCustomer, this ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
			}
			catch( ex : Error )
			{
				trace( ">>> SceneLoginEx::onLogin - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			( root as IApplication ).showWaitFeedback();
		}
		
		/**
		* Retorna os dados do perfil que fez login. Caso o o login não enha sido realizado, retorna <code>null</code>
		* @return Os dados do perfil que fez login. Caso o o login não enha sido realizado, retorna <code>null</code>
		*/		
		public function get loggedProfile() : NOCustomer
		{
			if( _noCustomer.profileId < 0 )
				return null;
			return _noCustomer;
		}
		
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		public function onNOSuccessfulResponse() : void
		{
			( root as IApplication ).hideWaitFeedback();

			// OLD: Não precisamos de feedback positivo de login. Afinal, o usuário só seguirá em frente caso faça login
			// com sucesso...
//			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_LOGGED_IN ),
//												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );

			onPlayWithoutLogin( null );
		}
		
		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/	
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_LOGIN ) + ":\n" + errorStr,
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
		}
		
		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}