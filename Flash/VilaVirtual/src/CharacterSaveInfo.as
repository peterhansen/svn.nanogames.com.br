package
{
	public final class CharacterSaveInfo
	{
		public var characterId : int;
		
		public var row : int;
		public var column : int;
		
		public var movementState : MovementState;
		
		public var expression : int;
		
		public function CharacterSaveInfo( id : int, tileRow : int, tileColumn : int, movState : MovementState, exp : int )
		{
			characterId = id;
			
			row  = tileRow;
			column = tileColumn;
			
			movementState = movState;
			expression = exp;
		}
	}
}
