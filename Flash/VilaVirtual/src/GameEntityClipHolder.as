package
{
	import NGC.ASWorkarounds.MovieClipVS;

	// TODO : Inverter essa herança. Assim ClipHolder ficaria muito mais útil!!!
	public class GameEntityClipHolder extends IsoGameEntity
	{
		/**
		* Movieclip que corresponde à parte gráfica da entidade 
		*/		
		private var _asset : MovieClipVS;
		
		/**
		* Construtor
		* @param gameEntityClip MovieClip que contém as animações do objeto
		* @param animFps Velocidade de frames por segundo das animações do objeto
		* @param anchorX Âncora de posicionamento - coordenada no eixo x
		* @param anchorY Âncora de posicionamento - coordenada no eixo y
		*/		
		public function GameEntityClipHolder( gameEntityClip : MovieClipVS, animFps : uint = Constants.CHARACTER_DEFAULT_FPS, anchorX : int = 0, anchorY : int = 0 )
		{
			super();
			
			_asset = gameEntityClip;

			imgAnchorX = anchorX;
			imgAnchorY = anchorY;

			addChild( gameEntityClip );
			width = gameEntityClip.width;
			height = gameEntityClip.height;
			gameEntityClip.x = 0;
			gameEntityClip.y = 0;
			
			gameEntityClip.fps = animFps;
			//fps = animFps;
		}
		
		/**
		* Retorna o movieclip que corresponde à parte gráfica da entidade
		* @return O movieclip que corresponde à parte gráfica da entidade
		*/		
		public function get asset() : MovieClipVS
		{
			return _asset;
		}
		
		// Alteramos o fps do asset que contemos, nunca o fps deste container. Assim obtemos o efeito esperado		
		override public function set fps( p_fps : uint ) : void
		{
			_asset.fps = p_fps;
		}
	}
}
