package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import flash.display.Shape;
	
	public class Car extends Character
	{
		private var parts : Vector.<Character >;
		
		// outros pedaços do carro. O objeto principal é a parte traseira direita, e cada pedaço extra ocupa um tile.
		private static const PART_FRONT_LEFT : uint = 0;
		private static const PART_FRONT_RIGHT : uint = 4;
		private static const PART_MIDDLE_LEFT : uint = 1;
		private static const PART_MIDDLE_RIGHT : uint = 2;
		private static const PART_REAR_LEFT : uint = 3;
		
		private static const TOTAL_PARTS : uint = 5;
		
		private static const ANCHOR_X : int = 96;
		private static const ANCHOR_Y : int = 44;
		
		
		public function Car(characterId:int, gameEntityClip:MovieClipVS, onCheckCallback:Function=null, animFps:uint=Constants.CHARACTER_DEFAULT_FPS)
		{
			super(characterId, gameEntityClip, onCheckCallback, animFps, ANCHOR_X, ANCHOR_Y );
			
			parts = new Vector.<Character>();
			for ( var i : uint = 0; i < TOTAL_PARTS; ++i ) { 
				// é usado um Character dummy para tratar colisões e eventos
				parts[ i ] = new Character( -1, new GenMan1Base(), carCheckCallback, animFps );
				parts[ i ].visible = false;
			}
		}
		
		
		private function carCheckCallback( c : int ) : void {
			if ( _onCheckCallback != null )
				_onCheckCallback( characterId );
		}
		
		
		public override function setDirection( facingDir : Direction ) : void
		{
		}
		
		public override function set movementState( newState : MovementState ) : void
		{
		}
	
		public override function removeSelf() : void
		{
			if ( tile ) {
				tile.removeTileObj( this );
				for each ( var p : Character in parts ) {
					p.removeSelf();
				}
			}
		}
		
		
		public override function set tile( t : ITile ) : void
		{
			removeSelf();
			
			super.tile = t;
			if ( t ) {
				// lado esquerdo
				t.getNeighborAtDirection( Direction.DIRECTION_LEFT ).insertTileObj( parts[ PART_REAR_LEFT ] );
				parts[ PART_REAR_LEFT ].tile.getNeighborAtDirection( Direction.DIRECTION_DOWN ).insertTileObj( parts[ PART_MIDDLE_LEFT ] );
				parts[ PART_MIDDLE_LEFT ].tile.getNeighborAtDirection( Direction.DIRECTION_DOWN ).insertTileObj( parts[ PART_FRONT_LEFT ] );
				
				// lado direito
				t.getNeighborAtDirection( Direction.DIRECTION_DOWN ).insertTileObj( parts[ PART_MIDDLE_RIGHT ] );
				parts[ PART_MIDDLE_RIGHT ].tile.getNeighborAtDirection( Direction.DIRECTION_DOWN ).insertTileObj( parts[ PART_FRONT_RIGHT ] );
			} else {
				for each ( var p : Character in parts ) {
					p.tile = t;
				}
			}
		}
		
	}	
}