package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class QuestState extends Enum
	{
		// "Construtor" estático
		{ initEnum( QuestState ); }
		
		// Possíveis estados das quests
		public static const QUEST_STATE_UNDISCOVERED : QuestState	= new QuestState();
		public static const QUEST_STATE_INCOMPLETE : QuestState		= new QuestState();
		public static const QUEST_STATE_COMPLETE : QuestState		= new QuestState();
	}
}
