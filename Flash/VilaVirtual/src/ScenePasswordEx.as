package
{
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NOTextsIndexes;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public final class ScenePasswordEx extends AppScene implements INOListener
	{
		/**
		* Objeto que realiza a integração com o NanoOnline
		*/		
		private var _noCustomer : NOCustomer;
		
		/**
		* Construtor 
		*/		
		public function ScenePasswordEx()
		{
			super( new ScenePassword() );
			
			_noCustomer = new NOCustomer();
			
			innerScene.addEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
		}
		
		/**
		* Callback chamada quando a cena propriamente dita acaba de ser construída 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onSceneBuilt( e : Event ) : void
		{
			innerScene.removeEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
			
			var temp : ScenePassword = ( innerScene as ScenePassword );
			temp.getBtSendEmail().addEventListener( MouseEvent.CLICK, onSendEmail );
		}
		
		/**
		* Callback chamada quando o usuário clica no botão de enviar o email
		* @param e Objeto que encapsula os dados do evento  
		*/		
		public function onSendEmail( e : MouseEvent ) : void
		{			
			try
			{
				// Faz as pré-validações dos campos do formulário através dos setters
				var temp : ScenePassword = ( innerScene as ScenePassword );
				_noCustomer.email = temp.getEmail();
				
				// Envia a requisição para o NanoOnline
				if( !NOCustomer.SendRememberPasswordRequest( _noCustomer, this ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
			}
			catch( ex : Error )
			{
				trace( ">>> ScenePasswordEx::onSendEmail - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			( root as IApplication ).showWaitFeedback();
		}
		
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		public function onNOSuccessfulResponse() : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_SENT ).replace( "%s", _noCustomer.email ),
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), onEmailSentPopUpOk );
		}
		
		/**
		* Callback chamada quando o usuário clica no botão de 'OK' do popup que confirma
		* o envio do email 
		*/	
		private function onEmailSentPopUpOk() : void
		{
			gotoAndPlay( "ExitToLogin" );
		}
		
		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_SEND_EMAIL )+ ":\n" + errorStr,
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
		}
		
		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/	
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}