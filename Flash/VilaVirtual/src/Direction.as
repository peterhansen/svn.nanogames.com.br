package
{
	import NGC.ASWorkarounds.Enum;

	public final class Direction extends Enum
	{
		// "Construtor" estático
		{ initEnum( Direction ); }
		
		// Possíveis direções de movimentação
		public static const DIRECTION_UP : Direction		= new Direction(); // 0
		public static const DIRECTION_LEFT : Direction		= new Direction(); // 1
		public static const DIRECTION_DOWN : Direction		= new Direction(); // 2
		public static const DIRECTION_RIGHT : Direction		= new Direction(); // 3
		
		public static const DIRECTION_90_CLOCKWISE : Direction		= new Direction();
		public static const DIRECTION_90_COUNTER_CLOCKWISE : Direction		= new Direction();
	}
}
