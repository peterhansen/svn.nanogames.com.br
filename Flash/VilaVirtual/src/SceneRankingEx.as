package
{
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoOnline.NOTextsIndexes;
	import NGC.ScheduledTask;
	
	import fl.controls.DataGrid;
	import fl.controls.ScrollPolicy;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.data.DataProvider;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	public final class SceneRankingEx extends AppScene implements INOListener
	{
		// Possíveis estados desta cena
		private static const SCENERNK_STATE_INIT : uint 						= 0;
		private static const SCENERNK_STATE_DOWNLOADING_RECICLING_RNK : uint 	= 1;
		private static const SCENERNK_STATE_DOWNLOADING_DENGUE_RNK : uint 		= 2;
		private static const SCENERNK_STATE_IDLE : uint 						= 3;
		
		/**
		* Número de linhas visíveis nas tabelas dos recordes. Para ver as outras linhas o usuário
		* deverá utilizar a barra de rolagem
		*/		
		private static const RNK_TABLES_N_VISIBLE_ROWS : uint = 10;
		
		/**
		* Tabela que exibe os rankings do minigame da dengue 
		*/		
		private var _dgDengue : DataGrid;
		
		/**
		* Tabela que exibe os rankings do minigame da reciclagem
		*/		
		private var _dgRecicling : DataGrid;
		
		/**
		* Ranking do minigame da dengue 
		*/		
		private var _noRnkDengue : NORanking
		
		/**
		* Ranking do minigame da reciclagem
		*/		
		private var _noRnkRecicling : NORanking;
		
		/**
		* Estado atual da cena 
		*/		
		private var _currState : uint;
		
		/**
		* Objeto utilizado para agendar métodos. Assim evitamos problemas com o singleton NOConnection ao concatenarmos
		* requisições ao NanoOnline 
		*/		
		private var _scheduledTask : ScheduledTask;
		
		/**
		* Construtor 
		*/		
		public function SceneRankingEx() : void
		{
			super( new SceneRanking() );

			_dgDengue = null;
			_dgRecicling = null;
			_scheduledTask = null;
			
			_noRnkDengue = new NORanking( Constants.NO_RNK_TYPE_DENGUE, Constants.NO_RNK_TYPE_DENGUE_MAX_POINTS );
			_noRnkRecicling = new NORanking( Constants.NO_RNK_TYPE_RECYCLING, Constants.NO_RNK_TYPE_RECYCLING_MAX_POINTS );
			
			setState( SCENERNK_STATE_INIT );
			innerScene.addEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
		}
		
		/**
		* Callback chamada quando a cena propriamente dita acaba de ser construída 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onSceneBuilt( e : Event ) : void
		{
			innerScene.removeEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
			
			var temp : SceneRanking = ( innerScene as SceneRanking );
			
			_dgDengue  = temp.getDengueDg();
			_dgDengue.visible = false;
			
			_dgRecicling  = temp.getReciclingDg();
			_dgRecicling.visible = false;
			
			setState( SCENERNK_STATE_DOWNLOADING_RECICLING_RNK );
		}
		
		/**
		* Determina o estado da cena 
		* @param newState O novo estado da cena
		*/		
		private function setState( newState : uint ) : void
		{
			switch( newState )
			{
				case SCENERNK_STATE_INIT:
					if( _dgRecicling != null )
						_dgRecicling.visible = false;
					
					if( _dgDengue != null )
						_dgDengue.visible = false;
					break;
				
				case SCENERNK_STATE_DOWNLOADING_RECICLING_RNK:
					if( !onDownloadReciclingRnk( true ) )
					{
						_currState = SCENERNK_STATE_INIT;
						return;
					}
					break;
				
				case SCENERNK_STATE_DOWNLOADING_DENGUE_RNK:
					if( !onDownloadDengueRnk( false ) )
					{
						_currState = SCENERNK_STATE_INIT;
						return;
					}
					break;
				
				case SCENERNK_STATE_IDLE:
					ConfigureDataGrid( _dgDengue, GetRnkDataProvider( _noRnkDengue ) );
					ConfigureDataGrid( _dgRecicling, GetRnkDataProvider( _noRnkRecicling ) );
					break;
			}
			
			_currState = newState;
		}
		
		/**
		* Faz o download do ranking do minigame da reciclagem
		*/		
		private function onDownloadReciclingRnk( doWaitFeedbackAnim : Boolean ) : Boolean
		{			
			try
			{
				// Envia a requisição para o NanoOnline
				if( !NORanking.SendHighScorestRequest( _noRnkRecicling, this ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
			}
			catch( ex : Error )
			{
				trace( ">>> SceneFormEx::onSubmit - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return false;
			}
			
			if( doWaitFeedbackAnim )
				( root as IApplication ).showWaitFeedback();
			
			return true;
		}
		
		/**
		* Faz o download do ranking do minigame da dengue  
		*/		
		private function onDownloadDengueRnk( doWaitFeedbackAnim : Boolean ) : Boolean
		{			
			try
			{
				// Envia a requisição para o NanoOnline
				if( !NORanking.SendHighScorestRequest( _noRnkDengue, this ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
			}
			catch( ex : Error )
			{
				trace( ">>> SceneFormEx::onSubmit - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return false;
			}
			
			if( doWaitFeedbackAnim )
				( root as IApplication ).showWaitFeedback();

			return true;
		}
		
		/**
		* Retorna um objeto DataProvider contendo os dados de um ranking online
		* @param rnk O ranking de qual vamos extrair os dados
		* @return Um objeto DataProvider contendo os dados de um ranking online
		*/		
		private static function GetRnkDataProvider( rnk : NORanking ) : DataProvider
		{
			var dp : DataProvider = new DataProvider();
			
			var i : int;
			var rnkEntries : Vector.< NORankingEntry > = rnk.onlineEntries;
			var nEntries : int = rnkEntries.length;

			for( i = 0 ; i < nEntries ; ++i )
			{
				var entry : NORankingEntry = rnkEntries[i];
				dp.addItem( { rnk: entry.rankingPos + 1, nick: entry.nickname, pts: entry.score } );
			}
			
			if( nEntries < RNK_TABLES_N_VISIBLE_ROWS )
			{
				for( i = nEntries ; i < RNK_TABLES_N_VISIBLE_ROWS ; ++i )
					dp.addItem( { rnk: i+1, nick: "", pts: "" } );
			}
			
			return dp;
		}
		
		/**
		* Determina as configurações padrão de uma DataGrid e a popula com os dados do DataProvider 
		* @param dg A DataGrid que vamos configurar
		* @param dp O objeto que contém os dados que serão utilizados para popular a DataGrid
		*/		
		private static function ConfigureDataGrid( dg : DataGrid, dp : DataProvider ) : void
		{
			if( dp.length > 0 )
			{
				dg.addColumn( "rnk" );
				dg.addColumn( "nick" );
				dg.addColumn( "pts" );
				dg.dataProvider = dp;
				dg.rowCount = RNK_TABLES_N_VISIBLE_ROWS;
				dg.rowHeight = 25;
				dg.editable = false;
				dg.showHeaders = false;
				dg.sortableColumns = false;
				dg.allowMultipleSelection = false;
				dg.selectable = false;
				
				// ScrollPolicy.AUTO não está funcionando aqui...
				dg.horizontalScrollPolicy = ScrollPolicy.ON;

				// Deixa o separador de colunas branco
				dg.setStyle( "skin", DataGrid_skinWhiteBkg );
				
				// Alterna a cor das linhas da tabela
				// OLD: Agora setamos o formatador por coluna. Assim podemos ter comportamentos diferentes
				//dg.setStyle( "cellRenderer", DataGridAlternatingRowColors );
				( dg.columns[0] as DataGridColumn ).cellRenderer = DataGridAlternatingRowColors;
				( dg.columns[1] as DataGridColumn ).cellRenderer = DataGridAlternatingRowColors;
				( dg.columns[2] as DataGridColumn ).cellRenderer = DataGridAlternatingRowColorsRightAlign;
				
				// TODO : Não está implementada. Logo estamos determinando as larguras das colunas na mão
				//AutoFitDataGridColumns( dg );
				( dg.columns[0] as DataGridColumn ).width = 50;
				
				dg.visible = true;
			}
		}
		
		private static function AutoFitDataGridColumns( dg : DataGrid ) : void
		{
			// TODO : Percorres todas as entradas de uma coluna, verificar qual o maior texto e, então,
			// determina a largura da coluna como a largura do maior texto + offset
//			var nRows : int = 0; /* TODO */
//			var cols : Array = dg.columns;
//			for each( var column : DataGridColumn in cols )
//			{
//				var maxItemWidth : Number = 0.0;
//				
//				for( var r : int = 0 ; r < nRows ; ++r )
//				{
//					if( cell)
//						maxItemWidth;
//				}
//				
//				column.width = maxItemWidth;
//			}
//
//			// Código de ajuda: Deriva de DataGrid...
//			private function columnResizeDoubleClickHandler(event:MouseEvent):void
//			{
//				// check if the ADG is enabled and the columns are resizable
//				if (!enabled || !resizableColumns)
//					return;
//				
//				var target:DisplayObject = DisplayObject(event.target);
//				var index:int = target.parent.getChildIndex(target);
//				// get the columns array
//				var optimumColumns:Array = getOptimumColumns();
//				
//				// check for resizable column
//				if (!optimumColumns[index].resizable)
//					return;
//				
//				// calculate the maxWidth - we can optimize this calculation
//				if(listItems)
//				{
//					var len:int = listItems.length;
//					var maxWidth:int = 0;
//					for(var i:int=0;i<len;i++)
//					{
//						if(listItems[i][index] is IDropInListItemRenderer)
//						{
//							var lineMetrics:TextLineMetrics = measureText(IDropInListItemRenderer(listItems[i][index]).listData.label);
//							if(lineMetrics.width > maxWidth)
//								maxWidth = lineMetrics.width ;
//						}
//					}
//				}
//				
//				// set the column's width
//				optimumColumns[index].width = maxWidth + getStyle("paddingLeft") + 
//					getStyle("paddingRight") + 8;
//			}
		}
		
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		public function onNOSuccessfulResponse() : void
		{
			switch( _currState )
			{
				case SCENERNK_STATE_DOWNLOADING_RECICLING_RNK:
					// OLD: Não podemos chamar isso daqui, pois estamos dentro da callback do NanoOnline. Se fizermos a chamada
					// abaixo, estaremos iniciando uma nova conexão, o que acarretaria em erros no singleton NOConnection. Logo,
					// vamos utilizar um timer
					//setState( SCENERNK_STATE_DOWNLOADING_DENGUE_RNK );
					_scheduledTask = new ScheduledTask( 100, setState, SCENERNK_STATE_DOWNLOADING_DENGUE_RNK );
					_scheduledTask.call();
					break;
				
				case SCENERNK_STATE_DOWNLOADING_DENGUE_RNK:
					( root as IApplication ).hideWaitFeedback();
					setState( SCENERNK_STATE_IDLE );
					break;
				
				case SCENERNK_STATE_INIT:
				case SCENERNK_STATE_IDLE:
				default:
					break;
			}
		}
		
		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_DOWNLOAD_ERROR )+ ":\n" + errorStr,
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
		}
		
		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/	
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}