package
{
	import NGC.ASWorkarounds.Enum;
	import NGC.NanoOnline.Gender;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NOTextsIndexes;
	import NGC.Utils;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Utils3D;

	public final class SceneFormEx extends AppScene implements INOListener
	{
		/**
		* Formulário onde o usuário irá fornecer seus dados 
		*/				
		private var _noForm : FormText;

		/**
		* Objeto que realiza a integração com o NanoOnline
		*/		
		private var _noCustomer : NOCustomer;

		/**
		* Construtor 
		*/		
		public function SceneFormEx()
		{
			super( new SceneForm() );

			_noCustomer = new NOCustomer();
			innerScene.addEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
		}
		
		/**
		* Callback chamada quando a cena propriamente dita acaba de ser construída 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onSceneBuilt( e : Event ) : void
		{
			innerScene.removeEventListener( AppScene.APPSCENE_SWCINIT_COMPLETE, onSceneBuilt );
			
			var temp : SceneForm = ( innerScene as SceneForm );
			_noForm = temp.getRegisterForm();
			temp.getBtSubmitProfile().addEventListener( MouseEvent.CLICK, onSubmit );
		}
		
		/**
		* Retorna o perfil criado pelo usuário. Caso nenhum perfil tenha sido criado, retorna <code>null</code>
		* @return O perfil criado pelo usuário. Caso nenhum perfil tenha sido criado, retorna <code>null</code>
		*/		
		public function get createdProfile() : NOCustomer
		{
			if( _noCustomer.profileId < 0 )
				return null;
			return _noCustomer;
		}
		
		/**
		* Callback chamada quando o usuário clica no botão de submeter dados (formulário de
		* cadastro do NanoOnline)
		* @param e Objeto que encapsula os dados do evento  
		*/		
		private function onSubmit( e : MouseEvent ) : void
		{
			// Verifica se os campos obrigatórios foram preenchidos
			var nickname : String = _noForm.getNickname();
			if( nickname.length == 0 )
			{
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICKNAME_MANDATORY ),
													NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}

			var email : String = _noForm.getEmail();
			if( email.length == 0 )
			{
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_MANDATORY ),
													NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			var password : String = _noForm.getPassword();
			if( password.length == 0 )
			{
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_PASSWORD_MANDATORY ),
													NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			else if( password != _noForm.getPasswordConfirm() )
			{
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PASSWORD_CONFIRM ),
													NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			var birthYear : int = _noForm.getBirthYear();
			var birthMonth : int = _noForm.getBirthMonth();
			var birthDay : int = _noForm.getBirthDay();
//			if( ( birthYear == 0 ) || ( birthMonth == 0 ) || ( birthDay == 0 ) )
//			{
//				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BIRTHDAY_MANDATORY ),
//													NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
//				return;
//			}
			
			try
			{
				// Faz as pré-validações dos campos do formulário através dos setters
				_noCustomer.password = password;
				_noCustomer.nickname = nickname;
				_noCustomer.firstName = _noForm.getFirstName();
				_noCustomer.lastName = _noForm.getLastName();
				_noCustomer.email = email;
				_noCustomer.phone = _noForm.getPhoneNumber();
				_noCustomer.setBirthday( birthDay, birthMonth, birthYear );
				_noCustomer.gender = ( Enum.FromIndex( Gender, _noForm.getGender() ) as Gender );

				// Envia a requisição para o NanoOnline
				if( !NOCustomer.SendCreateRequest( _noCustomer, this ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
			}
			catch( ex : Error )
			{
				trace( ">>> SceneFormEx::onSubmit - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			( root as IApplication ).showWaitFeedback();
		}
		
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		public function onNORequestSent() : void
		{
			// Vazia
		}
		
		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		public function onNORequestCancelled() : void
		{
			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
			// cancelar...
		}
		
		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		public function onNOSuccessfulResponse() : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_PROFILE_CREATED ),
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), onProfileCreatedPopUpOk );
		}
		
		/**
		* Callback chamada quando o usuário clica no botão de 'OK' do popup que confirma
		* a criação de perfil 
		*/		
		private function onProfileCreatedPopUpOk() : void
		{
			innerScene.gotoAndPlay( "CaBackToMenu" );
		}
		
		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/		
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
		{
			( root as IApplication ).hideWaitFeedback();
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_PROFILE )+ ":\n" + errorStr,
												NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
		}
		
		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/		
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
		{
			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
			// feedback...
			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
		}
	}
}