package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.*;
	import flash.utils.Timer;


	public class MapScreen extends InfoScreen
	{
		private const MAP_HEIGHT : int = 557;
		
		protected var mouse : Point = new Point();
		
		private var dragging : Boolean = false;
		
		private var map : MapBgBase;
		
		private var mapRect : Rectangle;
		private var mapMask : Rectangle;
		
		private var children : Array;
		
		private var _txtField : TextField;
		
		private var square : Sprite;
		
		private var playerPosition : MovieClipVS;
		
		private var otherCharactersList : MovieClipVS;
		
		
		public function MapScreen( characterType : PlayerCharacterType, onClose : Function, onMouseOver : Function, onMouseOut : Function )
		{
			super();
			
			mapRect = new Rectangle( 0, BORDER_WIDTH, WIDTH - ( BORDER_WIDTH * 2 ), HEIGHT - ( BORDER_HEIGHT * 2 ) );
			
			map = new MapBgBase();
			map.addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			map.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			map.addEventListener( MouseEvent.MOUSE_OUT, onMouseUp );
			map.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			map.scrollRect = mapRect;
			addChild( map );
			map.x = BORDER_WIDTH;
			map.y = BORDER_HEIGHT;
			
			children = [
				[ new MapAlleyBase(), "Casarão abandonado", null ],
				[ new MapBakeryBase(), "Padaria", null ],
				[ new MapChinacafeBase(), "Pastelaria", null ],
				[ new MapDelegacyBase(), "Delegacia de Polícia", null ],
				[ new MapFiredeptBase(), "Corpo de Bombeiros", null ],
				[ new MapGasolineBase(), "Posto de gasolina", null ],
				[ new MapHealthcenterBase(), "Posto de Saúde", null ],
				[ new MapHotDogBase(), "Pastel do Sr. Wong", null ],
				[ new MapHouse1Base(), "Casa da Dona Anita", null ],
				[ new MapHouse2Base(), "", null ],
				[ new MapMarketBase(), "Mercado da Dona Zefa", null ],
				[ new MapMusicstoreBase(), "Loja de música do Lamar", null ],
				[ new MapOngBase(), "ONG", null ],
				[ new MapPoliceBase(), "Cabine de Polícia", null ],
				[ new MapPrefectureBase(), "Prefeitura", null ],
				[ new MapUrbLimp(), "URBLIMP", null ],
			];
			
			for ( var i : int = 0; i < children.length; ++i ) {
				var c : MovieClip = children[ i ][ 0 ];
				var bmapData:BitmapData = new BitmapData(c.width, c.height, true, 0x00000000);
				bmapData.draw(c, new Matrix());
				children[ i ][ 2 ] = bmapData;
				
				map.addChild( c );
			}
			
			otherCharactersList = new MovieClipVS();
			map.addChild( otherCharactersList );
			
			playerPosition = characterType == PlayerCharacterType.CHARACTER_BOY ? new GenMan1Base() : new GenWoman1Base();
			playerPosition.gotoAndStop( "mr" );
			playerPosition.nextFrame();
			map.addChild( playerPosition );
			
			square = new Sprite();
			map.addChild(square);
			square.visible = false;
			
			var t : Timer = new Timer( 500 );
			t.addEventListener( TimerEvent.TIMER, blink );
			t.start();
			
			_txtField = new TextField();
			_txtField.embedFonts = true;
			_txtField.selectable = false;
			_txtField.antiAliasType = AntiAliasType.ADVANCED;
			_txtField.gridFitType = GridFitType.PIXEL;
			_txtField.thickness = 0;
			_txtField.mouseWheelEnabled = false;
			_txtField.wordWrap = false;
			_txtField.multiline = false;
			_txtField.mouseEnabled = false;
			
			_txtField.autoSize = TextFieldAutoSize.CENTER;
			
			var formatter : TextFormat = new TextFormat();
			formatter.font = Constants.FONT_NAME_CARTOONERIE;
			formatter.size = Constants.FONT_TEXT_DIALOG_SIZE;
			formatter.color = Constants.FONT_COLOR_TEXT;
			_txtField.setTextFormat( formatter );
			_txtField.defaultTextFormat = formatter;
			
			_txtField.visible = false;
			
			map.addChild( _txtField );
			
			mapMask = new Rectangle( map.x, map.y, map.width, MAP_HEIGHT );
			
			TagMapBase;
			addTagAndButton( "TagMapBase", onClose, onMouseOver, onMouseOut );
			
			setScroll( scrollLimitRight / 2, scrollLimitDown / 2 );
		}
		
		
		private function blink ( e : TimerEvent ) : void {
			playerPosition.visible = !playerPosition.visible;
		}
		
		
		public function setIconVisible( index : int, v : Boolean = true ) : void {
			children[ index ][ 0 ].visible = v;
		}
		
		
		private function onMouseDown( e : MouseEvent ) : void
		{
			dragging = true;
			mouse.x = e.stageX;
			mouse.y = e.stageY;
		}
		
		private function onMouseUp( e : MouseEvent ) : void
		{
			dragging = false;
			mouse.x = e.stageX;
			mouse.y = e.stageY;
		}
		
		
		/**
		 * main enteframe function
		 * check mouse position and
		 * scroll the image
		 */
		private function onMouseMove( e : MouseEvent ) : void
		{
			var i : int = 0;
			for ( ; i < children.length; ++i ) {
				var c : MovieClip = ( children[ i ][ 0 ] as MovieClip );
				
				if ( c.visible && c.hitTestPoint( e.stageX, e.stageY, true ) ) {
					var bmapData:BitmapData = children[ i ][ 2 ];
					
					var point : Point = new Point( e.stageX, e.stageY ); 
					var returnVal:Boolean = bmapData.hitTest(new Point(0,0), 0, c.globalToLocal(point));
					
					if ( returnVal ) {
						_txtField.visible = true;
						_txtField.text = children[ i ][ 1 ];
						_txtField.width = _txtField.textWidth;
						if ( point.x + _txtField.width > WIDTH )
							_txtField.x = WIDTH - _txtField.width + mapRect.x - ( _txtField.width >> 1 );
						else
							_txtField.x = point.x + mapRect.x - ( _txtField.width >> 1 );
						
						_txtField.y = point.y + mapRect.y - 100;
						
						square.graphics.clear();
						square.graphics.beginFill( 0xFFFFFF, 0.85 );
						square.graphics.drawRoundRect( 0, 0, _txtField.width, _txtField.height, 16 );
						square.graphics.endFill();
						square.x = _txtField.x;
						square.y = _txtField.y;
						square.visible = true;
						
						break;	
					}
				}
			}
			if ( i == children.length ) {
				_txtField.visible = false;
				square.visible = false;				
			}
				
			if ( dragging ) {
				var diff : Point = new Point( mouse.x - e.stageX, mouse.y - e.stageY );
				mouse.x = e.stageX;
				mouse.y = e.stageY;
				
				setScroll( mapRect.x + diff.x, mapRect.y + diff.y );
			}
		}
		
		
		private function setScroll( scrollX : Number, scrollY : Number ) : void
		{
			if ( scrollX > scrollLimitRight )
				scrollX = scrollLimitRight;
			else if ( scrollX < 0 )
				scrollX = 0;
			
			if ( scrollY > scrollLimitDown )
				scrollY = scrollLimitDown;
			else if ( scrollY < 0 )
				scrollY = 0;
			
			mapRect.x = scrollX;
			mapRect.y = scrollY;
			
			map.scrollRect = mapRect;
		}
		
		
		private function get scrollLimitDown() : Number
		{
			return mapMask.height - HEIGHT;
		}
		
		
		private function get scrollLimitRight() : Number
		{
			return mapMask.width - WIDTH;
		}
		
		
		public override function show() : void {
			super.show();
			var player : Character = VilaVirtual.GetInstance().getCharacterById( GameScript.CHARACTER_JOGADOR );
			var tile : ITile = player.tile;
			var p : Point = getMapPosition( tile );
			playerPosition.x = p.x - ( playerPosition.width >> 1 );
			playerPosition.y = p.y - ( playerPosition.height );
			
			try {
				while ( otherCharactersList.getChildAt( 0 ) )
					otherCharactersList.removeChildAt( 0 );
			} catch ( e : Error ) {
			}
			
			// adiciona icones dos personagens com textos importantes
			var characters : Array = VilaVirtual.GetInstance().getCharacters();
			for ( var i : int = 0; i < characters.length; ++i ) {
				var c : Character = characters[ i ];
				if ( c.expression == Character.EXPRESSION_EXCLAMATION ) {
					var characterClass : Class = Class( getDefinitionByName( getClassName( c.asset ) ) );
					var n : MovieClipVS = new characterClass() as MovieClipVS;
					n.gotoAndStop( "sd" );
					
					// o tile do personagem pode ser nulo, no caso de estar fora do mapa
					if ( c.tile ) {
						p = getMapPosition( c.tile );
						n.x = p.x - ( n.width >> 1 );
						n.y = p.y - ( n.height );
						otherCharactersList.addChild( n );
					}
				} 
			}
		}
		
		
		private function getMapPosition( t : ITile ) : Point {
			return new Point( 560 + ( t.row * -4.51 ) + ( t.column * 4.51 ),
							  -45 + ( t.row * 2.16 ) + ( t.column * 2.8 ) );
		}

		
		public function getClassName(o:Object):String {
			var fullClassName:String = getQualifiedClassName(o);
			return fullClassName.slice(fullClassName.lastIndexOf("::") + 1);
	     }
		
	}
}