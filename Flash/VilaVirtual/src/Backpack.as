package
{
	import NGC.ASWorkarounds.MovieClipVS;
	
	import de.polygonal.ds.HashMap;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	public class Backpack extends InfoScreen
	{
		private const ITEM_ICON_Y : uint = 64;
		private const ITEM_DESCRIPTION_Y : uint = 230;
		private const ITEM_DESCRIPTION_HEIGHT : uint = 132;
		
		private var backpack : HashMap;
		
		private var itemsGroup : MovieClip;
		
		
		public function Backpack( onClose : Function, onMouseOver : Function, onMouseOut : Function )
		{
			super();
			
			backpack = new HashMap();	
			
			TagInventaryBase;
			addTagAndButton( "TagInventaryBase", onClose, onMouseOver, onMouseOut );
		}
		
		
		public override function show() : void 
		{
			super.show();
			
			if ( itemsGroup != null ) {
				removeChild( itemsGroup );
				itemsGroup = null;
			}
			
			if ( backpack.size > 0 ) {
				itemsGroup = new MovieClip();
				var a : Array = backpack.toArray();
				
				var formatter : TextFormat = new TextFormat();
				formatter.font = Constants.FONT_NAME_CARTOONERIE;
				formatter.size = 14;
				formatter.color = Constants.FONT_COLOR_TEXT;
				
				var slotWidth : int = ( WIDTH - BORDER_WIDTH * 2 ) / a.length;
				for ( var i : int = 0; i < a.length; ++i ) {
					var item : Item = a[ i ];
					var m : MovieClip = item.movieClip;
					
					m.x = BORDER_WIDTH + i * slotWidth + ( ( slotWidth - m.width ) / 2 );
					m.y = ITEM_ICON_Y;
					
					var description : TextField = new TextField();
					description.y = ITEM_DESCRIPTION_Y;
					description.text = item.description;
					description.embedFonts = true;
					description.selectable = false;
					description.antiAliasType = AntiAliasType.ADVANCED;
					description.gridFitType = GridFitType.PIXEL;
					description.thickness = 0;
					description.wordWrap = true;
					description.multiline = true;
					description.setTextFormat( formatter );
					description.x = m.x;
					description.width = m.width;
					description.height = ITEM_DESCRIPTION_HEIGHT;
					itemsGroup.addChild( description );
					
					itemsGroup.addChild( m );
				}
				
				addChild( itemsGroup );
			}
		}
		
		
		public function insertItem( item : Item ) : void
		{
			backpack.insert( item.id, item );
		}
		
		
		/**
		 * Retira um item do container de itens
		 * @param itemID O ID único do item que deve ser retirado do container
		 * @return O item retirado do container ou null caso o item não esteja no container
		 */				
		public function removeItem( itemId : int ) : *
		{
			return backpack.remove( itemId );
		}
		
		
		/**
		 * Retorna se o jogador possui ou não o item de ID passado como parâmetro
		 * @param itemID O ID único do item
		 * @return true se o jogador possui o item, ou false caso contrário
		 */			
		public function hasItem( itemId : int ) : Boolean
		{
			return backpack.find( itemId ) != null;
		}
		
		
		public function getItem( itemId : int ) : Item
		{
			return backpack.find( itemId );
		}
		
		public function getItems() : Array
		{
			return backpack.toArray();
		}
	}
}