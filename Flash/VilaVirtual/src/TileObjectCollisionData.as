package
{
	public class TileObjectCollisionData
	{
		// Objeto com o qual o objeto que está se movendo colidiu
		private var _collisionObject : ITileObject;
		
		// Quanto os volumes de colisão se sobreporam na direção da colisão
		private var _intersectionVecLen : Number;

		// Construtor
		public function TileObjectCollisionData( collObj : ITileObject = null, intersecVecLen : Number = 0.0 )
		{
			collisionObject = collObj;
			intersectionVecLen = intersecVecLen;
		}
		
		public function set intersectionVecLen( len : Number ) : void
		{
			_intersectionVecLen = len;
		}
		
		public function get intersectionVecLen() : Number
		{
			return _intersectionVecLen;
		}
		
		public function set collisionObject( obj : ITileObject ) : void
		{
			_collisionObject = obj;
		}
		
		public function get collisionObject() : ITileObject
		{
			return _collisionObject;
		}
	}
}