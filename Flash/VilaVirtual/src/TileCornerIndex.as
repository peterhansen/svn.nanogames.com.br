package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class TileCornerIndex extends Enum
	{
		// "Construtor" estático
		{ initEnum( TileCornerIndex ); }
		
		// Índices das quinas de um tile. Estes valores devem ser utilizados com os métodos ITile.setCorner e ITile.getCorners
		// OBS: Só estamos utilizando esta enum porque AS 3.0 não permite declararmos constantes em interfaces. Ver
		// o comentário no arquivo ITile
		public static const TILE_CORNER_TOP_LEFT : TileCornerIndex			= new TileCornerIndex(); // 0
		public static const TILE_CORNER_TOP_RIGHT : TileCornerIndex	 		= new TileCornerIndex(); // 1
		public static const TILE_CORNER_BOTTOM_LEFT : TileCornerIndex		= new TileCornerIndex(); // 2
		public static const TILE_CORNER_BOTTOM_RIGHT : TileCornerIndex		= new TileCornerIndex(); // 3
	}
}
