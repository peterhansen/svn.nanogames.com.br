package NGC
{
	import NGC.ASWorkarounds.StaticClass;

	public final class Utils extends StaticClass
	{
		/**
		* Construtor : Vai disparar uma exceção se for chamado, pois classes estáticas não devem ser instanciadas 
		*/		
		public function Utils()
		{
			super();
		}

		/**
		* Retorna o índice da primeira ocorrência em 'str' de um dos caracteres contidos em 'search', a partir de 'startIndex'
		* @param str A string onde a busca será feita
		* @param search Os caracteres que estamos procurando
		* @param startIndex O índice de str a partir de qual vamos começar a busca
		* @return O índice da primeira ocorrência em 'str' de um dos caracteres contidos em 'search', a partir de 'startIndex' ou -1 caso nenhum dos caracteres tenha sido encontrado
		*/		
		public static function FindFirstOf( str : String, search : String , startIndex : uint = 0 ) : int
		{
			if( str.length > startIndex )
			{
				var searchLen : uint = search.length;
				for( var i : uint = 0 ; i < searchLen ; ++i )
				{
					var ret : int = str.indexOf( search.charAt( i ), startIndex );
					if( ret != -1 )
						return ret;
				}
			}
			return -1;
		}
		
		/**
		* Retorna o índice da primeira ocorrência em 'str' de um caractere não contido em 'search', a partir de 'startIndex'
		* @param str A string onde a busca será feita
		* @param search Os caracteres permitidos
		* @param startIndex O índice de str a partir de qual vamos começar a busca
		* @return O índice da primeira ocorrência em 'str' de um caractere não contido em 'search', a partir de 'startIndex' ou -1 caso todos os caracteres sejam permitidos
		*/		
		public static function FindFirstNotOf( str : String, search : String , startIndex : uint = 0 ) : int
		{
			var strLen : uint = str.length;
			if( ( strLen > startIndex ) && ( search.length > 0 ) )
			{
				for( var i : uint = startIndex ; i < strLen ; ++i )
				{
					var ret : int = search.indexOf( str.charAt( i ) );
					if( ret == -1 )
						return ret;
				}
			}
			return -1;
		}
		
		/**
		* Verifica se o caractere passado como parâmetro é um número 
		* @param char O caractere que deve ser testado
		* @return <code>true</code> se o caractere é um número ou <code>false</code> caso contrário
		*/	
		public static function IsDigit( char : String ) : Boolean
		{
			var charCode : int = int( char.charCodeAt(0) );
			return ( charCode >= 48 /* 0 */ ) && ( charCode <= 57 /* 9 */ );
		}
		
		/**
		* Verifica se o caractere passado como parâmetro é uma letra 
		* @param char O caractere que deve ser testado
		* @return <code>true</code> se o caractere é uma letra ou <code>false</code> caso contrário
		*/		
		public static function IsAlpha( char : String ) : Boolean
		{
			var charCode : int = int( char.charCodeAt(0) );
			return (( charCode >= 65 /* A */ ) && ( charCode <= 90 /* Z */ )) || (( charCode >= 97 /* a */ ) && ( charCode <= 122 /* z */ ));
		}
		
		/**
		* Verifica se o ano é bissexto. Para isso, utilizamos as regras:
		* 1. De 4 em 4 anos é ano bissexto
		* 2. De 100 em 100 anos não é ano bissexto 
		* 3. De 400 em 400 anos é ano bissexto
		* 4. Prevalece as últimas regras sobre as primeiras
		* Para entender melhor:
		* São bissextos todos os anos múltiplos de 400, p.ex: 1600, 2000, 2400, 2800
		* Não são bissextos todos os múltiplos de 100 e não de 400, p.ex: 1700, 1800, 1900, 2100, 2200, 2300, 2500...
		* São bissextos todos os múltiplos de 4 e não múltiplos de 100, p.ex: 1996, 2004, 2008, 2012, 2016...
		* Não são bissextos todos os demais anos 
		* @param year O ano que deseja-se saber se é bissexto
		* @return <code>true</code> se o ano é bissexto ou <code>false</code> caso contrário
		*/		
		public static function IsLeapYear( year : uint ) : Boolean
		{
			if( year % 100 == 0 )
				year /= 100;
			
			// &3 == %4
			return ( year & 3 ) == 0;
		}
		
		/**
		* Retorna o momento atual na representação UTC
		* @return O momento atual na representação UTC
		*/		
		public static function GetTimeStamp() : Number
		{
			var now : Date = new Date();
			return now.getTime();
		}
		
		/**
		* Transforma uma data UTC em um objeto Data
		* @param time A data em formato UTC
		* @return Um objeto Date que representa a data UTC passada como parâmetro
		*/		
		public static function DecodeDate( time : Number ) : Date
		{
			var d : Date = new Date();
			d.setTime( time );
			return d;
		}
		
		/**
		* Creates a UTC Date
		* @param year An unsigned integer representing the year
		* @param month An unsigned integer from 1 (January) to 12 (December)
		* @param day An unsigned integer from 1 to 31
		* @return The UTC Date
		*/		
		public static function UTCDate( year : uint, month : uint, day : uint ) : Date
		{
			var date : Date = new Date();
			date.setTime( Date.UTC( year, month - 1 /* Para o AS3, janeiro é 0 */, day ) );
			return date;
		}
	}
}