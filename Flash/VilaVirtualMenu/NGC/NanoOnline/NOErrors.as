package NGC.NanoOnline
{
	import NGC.ASWorkarounds.Enum;

	public final class NOErrors extends Enum
	{
		// "Construtor" estático
		{ initEnum( NOErrors ); }
		
		// Possíveis caminhos entre tiles
		public static const NO_ERROR_NONE : NOErrors					= new NOErrors();
		public static const NO_ERROR_INTERNAL : NOErrors				= new NOErrors();
		public static const NO_ERROR_NETWORK_ERROR : NOErrors			= new NOErrors();
		public static const NO_ERROR_INVALID_RESPONSE_FORMAT : NOErrors	= new NOErrors();
		public static const NO_ERROR_INVALID_DATA_SUBMITED : NOErrors	= new NOErrors();
	}
}