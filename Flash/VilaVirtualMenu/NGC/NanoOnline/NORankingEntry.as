package NGC.NanoOnline
{
	import NGC.ByteArrayEx;
	import NGC.Utils;

	public final class NORankingEntry extends Object implements ISerializable
	{
		// Id do perfil do jogador
		private var _profileId : int;
		
		// Pode parecer redundante, afinal, poderíamos ordenar NORankingEntries de acordo
		// com o campo score e utilizar a posição no conjunto para indicar a posição no
		// ranking. Mas quando estamos tratando um conjunto de NORankingEntries
		// retornados pelo servidor, podemos ter saltos entre entradas adjacentes. Isto
		// acontece quando queremos exibir pontuações de perfis registrados no device
		// que ficam fora dos 'n' 1os. Estes, por sua vez, sempre são mostrados
		private var _rankingPos : int;
		
		// Pontuação
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		private var _score : int;
		
		// Data em que a pontuação foi alcançada
		private var _timeStamp : Number;
		
		// Apelido do jogador
		private var _nickname : String;
		
		// Construtor
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function NORankingEntry( newProfileId : int = 0, newScore : int = 0, newNickname : String = "", rankPos : int = -1 )
		{
			profileId = newProfileId;
			score = newScore;
			nickname = newNickname;
			rankingPos = rankPos;
			timeStamp = Utils.GetTimeStamp();
		}
		
		// Getters
		public function get profileId() : int
		{
			return _profileId;
		}
		
		public function get rankingPos() : int
		{
			return _rankingPos;
		}

		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function get score() : int
		{
			return _score;
		}
		
		public function get timeStamp() : Number
		{
			return _timeStamp;
		}
		
		public function get nickname() : String
		{
			return _nickname.substr( 0, _nickname.length );
		}
		
		// Setters
		public function set profileId( newProfileId : int ) : void
		{
			_profileId = newProfileId; 
		}
		
		public function set rankingPos( pos : int ) : void
		{
			_rankingPos = pos;
		}
		
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function set score( newScore : int ) : void
		{
			_score = newScore;
		}
			
		public function set timeStamp( stamp : Number ) : void
		{
			_timeStamp = stamp;
		}
		
		public function set nickname( newNickname : String ) : void
		{
			_nickname = newNickname.substr( 0, newNickname.length );
		}
		
		// Lê o objeto de uma stream
		public function serialize( stream : ByteArrayEx ) : void
		{
			stream.writeInt( profileId );
			stream.writeInt( rankingPos );
			stream.writeLongCheated( score );
			stream.writeUTCTime( timeStamp );
			stream.writeUTF( nickname );
		}
		
		// Escre o objeto em uma stream 
		public function unserialize( stream : ByteArrayEx ) : void
		{
			profileId = stream.readInt();
			rankingPos = stream.readInt();
			score = stream.readLongCheated();
			timeStamp = stream.readUTCTime();
			nickname = stream.readUTF();
		}
	}
}