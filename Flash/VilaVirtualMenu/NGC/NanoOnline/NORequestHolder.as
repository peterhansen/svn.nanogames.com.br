package NGC.NanoOnline
{
	import NGC.StringRef;
	
	import de.polygonal.ds.HashMap;
	
	import flash.utils.ByteArray;

	public final class NORequestHolder
	{
		// Objeto que irá tratar a requisição
		private var _requester : Object;
		
		// Método de '_requester' que preenche a parte específica de uma requisição a ser enviada para o NanoOnline
		// Deve ter a assinatura "function ( stream : ByteArray ) : Boolean"
		private var _requestHandler : Function;
		
		// Método de '_requester' que trata a resposta de uma requisição feita ao NanoOnline
		// Deve ter a assinatura "function ( map : HashMap, outErrorStr : String ) : Boolean"
		private var _responseHandler : Function;
		
		// Construtor
		public function NORequestHolder( obj : Object = null, requestHandler : Function = null, responseHandler : Function = null )
		{
			_requester = obj;
			_requestHandler = requestHandler;
			_responseHandler = responseHandler;
		}
		
		public function onRequest( stream : ByteArray ) : Boolean
		{
			if( _requester != null )
				return _requestHandler.call( _requester, stream );
			return false;
		}
		
		public function onResponse( table : HashMap, outErrorStr : StringRef ) : Boolean
		{
			if( _requester != null )
				return _responseHandler.call( _requester, table, outErrorStr );
			return false;
		}
		
		public function clone() : NORequestHolder
		{
			if( _requester != null )
				return new NORequestHolder( _requester, _requestHandler, _responseHandler );
			return new NORequestHolder();
		}
	}
}