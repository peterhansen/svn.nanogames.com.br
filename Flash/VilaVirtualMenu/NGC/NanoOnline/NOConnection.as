package NGC.NanoOnline
{
	import NGC.ByteArrayEx;
	import NGC.Crypto.BigInt.BigInteger;
	import NGC.StringRef;
	import NGC.Utils;
	
	import de.polygonal.ds.HashMap;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLStream;
	import flash.system.Capabilities;
	import flash.utils.ByteArray;

	public final class NOConnection
	{
		// Identificação do usuário que está logado no NanoOnline
		private var _currCustomerId : int;
		
		// Identificação do idioma utilizado no NanoOnline
		private var _languageId : int;
		
		// Objeto que está fazendo a requisição atual
		// OBS: Poderia virar um vector.< NORequestHolder > para termos várias requisições simultâneas
		private var _currRequester : NORequestHolder;
		
		// Objeto que irá receber os eventos de NanoOnlineManager
		private var _noListener : INOListener;
		
		// Identificador da aplicação
		private var _appShortName : String;
		
		// Versão da aplicação
		private var _appVersion : String;
		
		// Path do host
		private var _hostUrl : String;
		
		// Objeto que trata o envio e o recebimento de informações
		private var _requestStream : URLStream;
		
		/**
		* Única instância da classe
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript
		*/		
		private static var _singleton : NOConnection = new NOConnection();
		
		/**
		* Construtor 
		*/		
		public function NOConnection()
		{
			// Se já construímos a variável estática...
			if( _singleton != null )
				throw new Error( "This is a singleton class. You should call GetInstance() method." );
			
			_currCustomerId = NOCustomer.CUSTOMER_PROFILE_ID_NONE;
			_currRequester = null;
			_requestStream = null;

			noListener = null;
			languageId = -1;
			appShortName = "";
			appVersion = "";
			host = "";
		}
		
		/**
		* Retorna a única instância da classe
		* @return A única instância da classe
		*/		
		public static function GetInstance() : NOConnection
		{
			return _singleton;
		}

		// Armazena o host da conexão
		public function set host( url : String ) : void
		{
			_hostUrl = url.substr( 0, url.length );
		}
		
		public function get host() : String
		{
			return _hostUrl.substr( 0, _hostUrl.length );
		}

		// Determina o idioma do NanoOnline
		public function set languageId( id : int ) : void
		{
			_languageId = id;
		}
		
		public function get languageId() : int
		{
			return _languageId;
		}

		// Determina o identificador do usuário que está se conectando ao NanoOnline		
		public function set customerId( id : int ) : void
		{
			_currCustomerId = id;
		}
		
		public function get customerId() : int
		{
			return _currCustomerId;
		}

		// Determina o identificador da aplicação
		public function set appShortName( name : String ) : void
		{
			_appShortName = name.substr( 0, name.length );
		}
		
		public function get appShortName() : String
		{
			return _appShortName.substr( 0, _appShortName.length );
		}

		// Determina a versão da aplicação
		public function set appVersion( version : String ) : void
		{
			_appVersion = version.substr( 0, version.length );
		}
		
		public function get appVersion() : String
		{
			return _appVersion.substr( 0, _appVersion.length );
		}

		// Determina um objeto para receber e tratar os eventos do NanoOnline
		public function set noListener( listener : INOListener ) : void
		{
			_noListener = listener;
		}
		
		public function get noListener() : INOListener
		{
			return _noListener;
		}

		/**
		* Reinicializa o objeto
		*/		
		public function clean() : void
		{
			cancelPendingRequest();
			noListener = null;
		}

		/**
		* Cancela a requisição pendente. Caso não haja uma requisição pendente, não faz nada
		*/		
		public function cancelPendingRequest() : void
		{
			if( _requestStream != null )
			{
				_requestStream.close();
				_requestStream = null;
			}
			_currRequester = null;
		}
		
		// Envia uma requisição para o NanoOnline
		public function sendRequest( commandURL : String, resquester : NORequestHolder ) : Boolean
		{
			var ret : Boolean = false;
			
			if( noListener == null )
			{
				trace( ">>> NOConnection => There must be a listener set for a request to be sent" );
				return ret;
			}
			
			try
			{
				// Configura a requisição
				var request : URLRequest = new URLRequest( host + commandURL );
				request.method = URLRequestMethod.POST;
				// TODO request.contentType = "multipart/form-data";
				
				// OLD: Se fizermos a atribuição abaixo, receberemos o seguinte erro em URLStream.load():
				// "The HTTP request header User-Agent cannot be set via ActionScript"
				//request.requestHeaders = new Array( new URLRequestHeader( "User-Agent", GetDeviceUserAgent() ) );
				request.requestHeaders = new Array( new URLRequestHeader( "Flash-User-Agent", GetDeviceUserAgent() ) );
				
				// Determina o corpo da mensagem (geralmente para requisições POST)
				var nanoOnlineData : ByteArrayEx = new ByteArrayEx();
				writeHeader( nanoOnlineData, NOCustomer.CUSTOMER_PROFILE_ID_NONE );
				
				if( resquester.onRequest( nanoOnlineData ) )
				{
					// Anexa os dados à requisição
					nanoOnlineData.writeByte( NOGlobals.NANO_ONLINE_ID_MACOS_RAILS_BUG_FIX );
					request.data = nanoOnlineData;
					
					trace( ">>> NOConnection => POST Data Size = " + nanoOnlineData.length );
					
					// Envia a requisição
					_requestStream = new URLStream();
					configureURLStreamListeners();
					_requestStream.load( request );
					
					// Armazena o objeto que está fazendo a requisição
					_currRequester = resquester;
					
					// Indica que tudo está OK
					ret = true;
				}
				else
				{
					nanoOnlineData.clear();
				}
			}
			catch( error : Error )
			{
				trace( ">>> NOConnection => Unable to load requested URL: " + error.message );
			}
			finally
			{
				return ret;
			}
		}

		/**
		* Configura os eventos de conexão que desejamos observar 
		*/		
		private function configureURLStreamListeners() : void
		{
			_requestStream.addEventListener( Event.COMPLETE, onNetworkRequestCompleted );
			_requestStream.addEventListener( HTTPStatusEvent.HTTP_STATUS, onHTTPStatusChanged );
			_requestStream.addEventListener( IOErrorEvent.IO_ERROR, onNetworkError );
			_requestStream.addEventListener( Event.OPEN, onConnectionOpened );
			_requestStream.addEventListener( ProgressEvent.PROGRESS, onNetworkProgressChanged );
			_requestStream.addEventListener( SecurityErrorEvent.SECURITY_ERROR, onSecurityError );
		}
		
		/**
		* Callback chamada assim que a conexão HTTP é aberta 
		* @param e O objeto que encapsula os dados do evento 
		*/
		private function onConnectionOpened( e : Event ) : void
		{
			trace( ">>> NOConnection => onConnectionOpened" );
		}
		
		/**
		* Callback chamada quando recebemos um erro de segurança 
		* @param e O objeto que encapsula os dados do evento 
		*/
		private function onSecurityError( e : SecurityErrorEvent ) : void
		{
			trace( ">>> NOConnection => Security Error: " + e.text );
			
			noListener.onNOError( NOErrors.NO_ERROR_NETWORK_ERROR, e.text );			
			clean();
		}
		
		/**
		* Callback chamada sempre que o estado da conexão HTTP é modificado 
		* @param e O objeto que encapsula os dados do evento 
		*/
		private function onHTTPStatusChanged( e : HTTPStatusEvent ) : void
		{
			// OK
			if(( e.status == 200 ) || ( e.status == 0 ))
			{
				trace( ">>> NOConnection => StatusChanged - HTTP OK" );
			}
//			// Verifica se o código de erro indica redirecionamento
//			else if(( e.status >= 300 ) && ( e.status <= 307 ))
//			{
//				// Verifica se já houve um redirecionamento
//				if()
//				{
//					// Indica um erro
//				}
//				else
//				{
//					// Faz o redirecionamento
//				}
//			}
			// Outros
			else
			{
				trace( ">>> NOConnection => StatusChanged - Unhandled Status Code (" + e.status + ") = " + e.toString() );
				
				noListener.onNOError( NOErrors.NO_ERROR_NETWORK_ERROR, "Internal error" );			
				clean();
			}
		}

		/**
		* Callback chamada quando a requisição HTTP é finalizada com sucesso 
		* @param e O objeto que encapsula os dados do evento 
		*/
		private function onNetworkRequestCompleted( e : Event ) : void
		{
			trace( ">>> NOConnection => Request Completed; Response size = " + _requestStream.bytesAvailable );
			
			// Faz o parse dos dados
			var dataStream : ByteArrayEx = new ByteArrayEx();
			_requestStream.readBytes( dataStream );

			var table : HashMap = ReadHeader( dataStream );
			dataStream.clear();
			
			var error : StringRef = new StringRef();
			if( table.size == 0 )
			{
				error.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_RESPONSE );
				noListener.onNOError( NOErrors.NO_ERROR_INVALID_RESPONSE_FORMAT, error.content );
			}
			else if( table.containsKey( NOGlobals.NANO_ONLINE_ID_ERROR_MESSAGE ) )
			{
				error.content = table.find( NOGlobals.NANO_ONLINE_ID_ERROR_MESSAGE );
				noListener.onNOError( NOErrors.NO_ERROR_INVALID_DATA_SUBMITED, error.content );
			}
			else
			{
				var returnCode : int = NOGlobals.NANO_ONLINE_RC_OK;
				if( table.containsKey( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
					returnCode = table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE );

				if( returnCode < NOGlobals.NANO_ONLINE_RC_OK )
				{
					switch( returnCode )
					{
						case NOGlobals.NANO_ONLINE_RC_DEVICE_NOT_SUPPORTED:
							error.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_DEVICE_UNSUPPORTED );
							break;
						
						case NOGlobals.NANO_ONLINE_RC_APP_NOT_FOUND:
							error.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_APP_UNSUPPORTED );
							break;
						
						case NOGlobals.NANO_ONLINE_RC_SERVER_INTERNAL_ERROR:
						default:
							error.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_SERVER_INTERNAL_ERROR );
							break;
					}
					noListener.onNOError( NOErrors.NO_ERROR_INTERNAL, error.content );
				}
				else
				{
					if( _currRequester.onResponse( table, error ) )
						noListener.onNOSuccessfulResponse();
					else
						noListener.onNOError( NOErrors.NO_ERROR_INVALID_RESPONSE_FORMAT, error.content );
				}
			}
			
			clean();
		}
		
		// Indica que ocorreu um erro na requisição
		private function onNetworkError( e : IOErrorEvent ) : void
		{			
			trace( ">>> NOConnection => Network Error: " + e.text + "\n" );

			noListener.onNOError( NOErrors.NO_ERROR_NETWORK_ERROR, e.text );			
			clean();
		}

		// Indica o progresso atual da requisição
		private function onNetworkProgressChanged( e : ProgressEvent ) : void
		{
			trace( ">>> NOConnection => Curr progress: " + e.bytesLoaded + " / " + e.bytesTotal + "\n" );

			noListener.onNOProgressChanged( e.bytesLoaded, e.bytesTotal );
		}
		
		// Indica que a requisição foi cancelada pelo usuário
		private function onNetworkRequestCancelled() : void
		{
			trace( ">>> NOConnection => Request Cancelled\n" );
			
			noListener.onNORequestCancelled();
			clean();
		}
		
		private static function WriteSignature( stream : ByteArrayEx ) : void
		{
			// Assinatura do NanoOnline
			stream.writeByte( 137 );
			stream.writeByte(  78 );
			stream.writeByte(  65 );
			stream.writeByte(  78 );
			stream.writeByte(  79 );
			stream.writeByte(  13 );
			stream.writeByte(  10 );
			stream.writeByte(  26 );
			stream.writeByte(  10 );			
		}

		// Retorna o user agent que utilizaremos nas conexões
		private static function GetDeviceUserAgent() : String
		{
			var userAgent : String = Capabilities.os + "##" + Capabilities.language + "##" + Capabilities.playerType + "##"
								   + Capabilities.version + "##" + Capabilities.screenResolutionX + "x" + Capabilities.screenResolutionY;
			
			trace( ">>> NanoOnline - User Agent: " + userAgent + "\n" );
			return userAgent;
		}

		// Recebe o cabeçalho genérico de uma conexão ao NanoOnline
		private static function ReadHeader( input : ByteArrayEx ) : HashMap
		{
			trace( ">>> NOConnection => Parsing response\n" );
			
			var tempTable : HashMap = new HashMap( 10 );
			
			var ret : Boolean = true;
			while( ( input.bytesAvailable > 0 ) && ret )
			{
				var id : int = input.readByte();
				trace( ">>> NOConnection => Leu ID: " + id + "\n" );

				switch( id )
				{
					case NOGlobals.NANO_ONLINE_ID_APP:
					case NOGlobals.NANO_ONLINE_ID_RETURN_CODE:
						{
							var int16 : int = input.readShort();
							tempTable.insert( id, int16 );
							trace( ">>> NOConnection => Read int16: " + int16 + "\n\n" );
						}
						break;
					
					case NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID:
						{
							var int32 : int = input.readInt();
							tempTable.insert( id, int32 );
							trace( ">>> NOConnection => Read int32: " + int32 + "\n\n" );
						}
						break;
					
					case NOGlobals.NANO_ONLINE_ID_LANGUAGE:
						{
							var int8 : int = input.readByte();
							tempTable.insert( id, int8 );			
							trace( ">>> NOConnection => Read byte: " + int8 + "\n\n" );
						}
						break;
					
					case NOGlobals.NANO_ONLINE_ID_APP_VERSION:
					case NOGlobals.NANO_ONLINE_ID_NANO_ONLINE_VERSION:
					case NOGlobals.NANO_ONLINE_ID_ERROR_MESSAGE:
						{
							var str : String = input.readUTF();
							if( str.length == 0 )
							{
								ret = false;
							}
							else
							{
								tempTable.insert( id, str );
								trace( ">>> NOConnection => Read string: " + str + "\n\n" );		
							}
						}
						break;
					
					case NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA:
						{
							var specificData : ByteArrayEx = new ByteArrayEx();
							input.readBytes( specificData );

							if( specificData.bytesAvailable == 0 )
							{
								ret = false;
							}
							else
							{
								tempTable.insert( id, specificData );
								trace( ">>> NOConnection => Read byte array\n\n" );
							}
						}
						break;
					
					default:
						trace( ">>> NOConnection => ReadHeader( table : HashMap, input : ByteArrayEx ) : HashMap - Unknown field id in response header" );
						ret = false;
						break;
				}
			}
			
			if( !ret )
				tempTable.clear();
			
			return tempTable;
		}

		// Envia o cabeçalho genérico de uma conexão ao NanoOnline
		private function writeHeader( output : ByteArrayEx, customerId : int ) : void
		{
			// Escreve a assinatura dos pacotes
			WriteSignature( output );
			
			// Escreve a versão do cliente
			output.writeByte( NOGlobals.NANO_ONLINE_ID_NANO_ONLINE_VERSION );
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_NANO_ONLINE_VERSION: " + NOGlobals.NANO_ONLINE_ID_NANO_ONLINE_VERSION + "\n" );

			var clientVersion : String = NOGlobals.NANO_ONLINE_CLIENT_VERSION;;
			output.writeUTF( clientVersion );
			trace( "Value: " + clientVersion + "\n\n" );			
			
			// Escreve o identificador da aplicação
			output.writeByte( NOGlobals.NANO_ONLINE_ID_APP );
			output.writeUTF( appShortName );
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_APP: " + NOGlobals.NANO_ONLINE_ID_APP + "\nValue: " + appShortName + "\n\n" );
			
			// Escreve o identificador do usuário
			output.writeByte( NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID );
			output.writeInt( customerId );
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_CUSTOMER_ID: " + NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID + "\nValue: " + customerId + "\n\n" );
			
			// Escreve o identificador do idioma
			output.writeByte( NOGlobals.NANO_ONLINE_ID_LANGUAGE );
			output.writeByte( languageId );
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_LANGUAGE: " + NOGlobals.NANO_ONLINE_ID_LANGUAGE + "\nValue: " + languageId + "\n\n" );

			// Escreve a versão da aplicação
			output.writeByte( NOGlobals.NANO_ONLINE_ID_APP_VERSION );
			output.writeUTF( appVersion );			
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_APP_VERSION: " + NOGlobals.NANO_ONLINE_ID_APP_VERSION + "\nValue: " + appVersion + "\n\n" );

			// Escreve o horário local do cliente (o servdor espera um int64, mas nós não temos uma forma fácil de converter de Number para BigInteger...)
			output.writeByte( NOGlobals.NANO_ONLINE_ID_CLIENT_LOCAL_TIME );
			var ts : Number = Utils.GetTimeStamp();
			output.writeUTCTime( ts );
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_CLIENT_LOCAL_TIME: " + NOGlobals.NANO_ONLINE_ID_CLIENT_LOCAL_TIME + "\nValue: " + ts + "\n\n" );

			// Escreve o identificador dos dados específicos
			output.writeByte( NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA );			
			trace( ">>> NOConnection => Wrote ID NANO_ONLINE_ID_SPECIFIC_DATA: " + NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA + "\nValue: Empty\n\n" );
		}
	}
}
