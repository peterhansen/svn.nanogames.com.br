package NGC.NanoOnline
{
	import NGC.ByteArrayEx;

	public interface ISerializable
	{
		/**
		* Serializa um objeto para em uma stream 
		* @param stream A stream para onde vamos serializar o objeto 
		*/		
		function serialize( stream : ByteArrayEx ) : void;

		/**
		* Desserializa um objeto a partir de uma stream 
		* @param stream A stream que contém o objeto serializado 
		*/ 
		function unserialize( stream : ByteArrayEx ) : void;
	}
}