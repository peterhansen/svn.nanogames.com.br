package
{
	import flash.display.MovieClip;
	
	public class AppScene extends MovieClip
	{
		/**
		* IDs únicos dos eventos da classe 
		*/		
		public static const APPSCENE_LOADING_COMPLETE : String = "appSceneLoadingComplete";
		public static const APPSCENE_SWCINIT_COMPLETE : String = "appSceneSWCInitComplete";

		/**
		* <code>true</code> se a âncora do MovieClip da cena está em seu centro, <code>false</code> caso seja top-left  
		*/		
		private var _anchorAtCenter : Boolean;
		
		/**
		* Indica se a cena fará seu carregamento assincronamente. Nestes casos, a cena deverá disparar um evento APPSCENE_LOADING_COMPLETE
		*/		
		private var _asynchronousLoading : Boolean;
		
		/**
		* Cena propriamente dita. Utilizamos composição ao invés de herança para contornar bugs intermitentes do Flash, como:
		* - Error #1009: Cannot access a property or method of a null object reference
		* - Error #1046: Type was not found or was not a compile-time constant
		* Esses erros só acontecem quando estamos importando movieclips complexos, gerados no FLashIDE, através de um arquivo SWC.
		* Há também o ganho de não termos que gerar uma classe para toda cena do jogo
		*/		
		private var _scene : MovieClip;
		
		/**
		* Indica se a cena dispara eventos de progresso quando realiza carregamento assíncrono 
		*/		
		private var _usesProgressEvent : Boolean;
		
		/**
		* Construtor 
		* @param sceneAnchorAtCenter <code>true</code> caso a âncora de posicionamento da cena esteja em seu centro, <code>false</code> caso seja top-left
		 * @param needsAsynchronousLoading
		*/		
		public function AppScene( sceneMovieClip : MovieClip, sceneAnchorAtCenter : Boolean = true, needsAsynchronousLoading : Boolean = false, dispatchesProgressEvents : Boolean = true )
		{
			super();
			
			_anchorAtCenter = sceneAnchorAtCenter;
			_asynchronousLoading = needsAsynchronousLoading;
			_usesProgressEvent = dispatchesProgressEvents;
			
			_scene = sceneMovieClip;
			if( _scene )
				addChild( sceneMovieClip );
		}
		
		/**
		* Retorna <code>true</code> se a âncora do MovieClip da cena está em seu centro, <code>false</code> caso seja top-left
		* @return <code>true</code> se a âncora do MovieClip da cena está em seu centro, <code>false</code> caso seja top-left
		*/		
		public function get anchorAtCenter() : Boolean
		{
			return _anchorAtCenter;
		}

		/**
		* Retorna se a cena dispara eventos de progresso quando realiza carregamento assíncrono
		* @return Se a cena dispara eventos de progresso quando realiza carregamento assíncrono
		*/		
		public function get usesProgressEvent() : Boolean
		{
			return _usesProgressEvent;
		}
		
		/**
		* Retorna se a cena fará seu carregamento assincronamente. Nestes casos, a cena deverá disparar um evento APPSCENE_LOADING_COMPLETE
		* @return Se a cena fará seu carregamento assincronamente. Nestes casos, a cena deverá disparar um evento APPSCENE_LOADING_COMPLETE
		*/		
		public function get asynchronousLoading() : Boolean
		{
			return _asynchronousLoading;
		}
		
		public function set asynchronousLoading( a : Boolean ) : void {
			_asynchronousLoading = a;
		}
		
		override public function gotoAndPlay( frame : Object, scene : String = null ) : void
		{
			if( _scene )
				_scene.gotoAndPlay( frame, scene );
		}
		
		override public function gotoAndStop( frame : Object, scene : String = null ) : void
		{
			if( _scene )
				_scene.gotoAndStop( frame, scene );
		}
		
		override public function stop() : void
		{
			if( _scene )
				_scene.stop();
		}
		
		override public function play() : void
		{
			if( _scene )
				_scene.play();	
		}
		
		/**
		* Retorna a cena propriamente dita
		* @return A cena propriamente dita
		*/		
		public function get innerScene() : MovieClip
		{
			return _scene;
		}
	}
}