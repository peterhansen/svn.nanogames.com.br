package
{
	import flash.display.Stage;

	/**
	* Esta classe é utilizada para não termos que expor toda a classe concreta Application para os FLAs externos que precisam controlar
	* o fluxo da aplicação. Assim conseguimos menos dependências entre arquivos distintos, pois podemos modificar a implementação dos
	* métodos sem que tenhamos que regerar os SWCs
	* @author Daniel L. Alves 
	*/	
	public interface IApplication
	{
		/**
		* Determina o estado da aplicação 
		* @param newState O novo estado da aplicação
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/			
		function setState( newState : ApplicationState, frameLabel : Object = null ) : void;
		
		/**
		* Trava o tratamento de input da cena atual e exibe um feedback de espera para o usuário 
		*/		
		function showWaitFeedback( cancelable : Boolean = false, onCancelCallback : Function = null ) : void;
		
		/**
		* Esconde o feedback de espera e libera o tratamento de input da cena atual
		*/		
		function hideWaitFeedback() : void;
		
		/**
		* Exibe um popup para o usuário travando o tratamento de input da cena atual enquanto
		* o mesmo estiver visível
		*/		
		function showPopUp( msg : String, ...btsTitlesAndCallbacks ) : void;
		
		/**
		* Retorna uma referência para a janela da aplicação 
		* @return Uma referência para a janela da aplicação
		*/		
		function getStage() : Stage
	}
}