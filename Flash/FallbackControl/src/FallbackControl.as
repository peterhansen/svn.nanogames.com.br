package
{
	import flash.display.Sprite;
	import flash.external.ExternalInterface;
	import flash.net.Socket;
	import flash.events.ProgressEvent;
	import flash.events.*;
	import flash.net.*;
	
	public class FallbackControl extends Sprite
	{
		
		private var serverSock: Socket;
		
		public function FallbackControl()
		{
			ExternalInterface.addCallback("send", onReceive);
			serverSock = new Socket();
			serverSock.connect("192.168.1.12", 8080 );
			
			

			serverSock.addEventListener(ProgressEvent.SOCKET_DATA, progressEventHandler );
			serverSock.addEventListener( ProgressEvent.SOCKET_DATA, send );

		}
		
		///veio da pagina de joystick
		public function onReceive( data : String ) : void {
			trace( "sending " + data );
			///envia para o jogo o comando.
			serverSock.writeUTFBytes( data );
		}
		
		public function progressEventHandler( progressEvent : ProgressEvent ) : void {
		}
		
		///veio do flash de corrida
		public function send( dataEvent:ProgressEvent ) : void {
			var msg : String = serverSock.readUTFBytes( dataEvent.bytesLoaded );
			ExternalInterface.call( "send", msg );
		}		
	}
}