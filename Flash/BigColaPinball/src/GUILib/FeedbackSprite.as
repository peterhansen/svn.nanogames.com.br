package GUILib
{
	///flash
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	
	
	public class FeedbackSprite extends Sprite
	{	
		///----------------------------------------------------------------------------------------------------------			
		/**
		 * id do sprite num dado grupo
		 */
		private var id:int;
		/**
		 * callback de click de mouse
		 */
		private var onMouseClickCallback:Function;
		/**
		 * callback de mouse clicado e segurado
		 */
		private var onMouseDownCallback:Function;
		/**
		 * callback de botao do mouse solto
		 */
		private var onMouseUpCallback:Function;
		/**
		 * callback de mouse sobre do sprite
		 */
		private var onMouseOverCallback:Function;
		/**
		 * callback de mouse se movendo sobre o sprite
		 */
		private var onMouseMoveCallback:Function;
		/**
		 * callback de mouse saindo do sprite
		 */
		private var onMouseOutCallback:Function;
		/**
		 * callback do scroll do mouse sendo movido
		 */
		private var onMouseScrollCallback:Function;
		/**
		 * sprite habilitado ou não
		 */
		private var enabled:Boolean;
		///----------------------------------------------------------------------------------------------------------	
		/**
		 * isEnabled
		 * @return retorna se o sprite esta habilitado ou não
		 * 
		 */
		public function isEnabled():Boolean
		{
			return enabled;				
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setEnabledState
		 * @param s define se o sprite responde ou não aos eventos
		 * 
		 */
		public function setEnabledState(s:Boolean):void
		{
			enabled=s;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setDisabled
		 * desabilita o tratamento de eventos de feedback
		 */
		public function setDisabled():void
		{
			enabled=false;
		}
		///----------------------------------------------------------------------------------------------------------						
		/**
		 * setEnabled
		 * habilita a resposta a eventos que fazem feedback
		 */
		public function setEnabled():void
		{
			enabled=true;
		}
		///----------------------------------------------------------------------------------------------------------			
		/**
		 * construtor
		 * inicializa os callbacks de eventos
		 */
		public function FeedbackSprite()
		{
			super();
			setEnabled();
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);	
			this.addEventListener(MouseEvent.CLICK, onMouseClick);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			this.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseScroll);
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setBrightness
		 * @param brillo qual deve ser o brilho [-1, 1]
		 * @comment descaradamente copiado de algum forum ai da vida
		 */
		private function setBrightness(brillo:Number):void
		{ 
			// changes the brightness
			// mc is the movieclip you want to change
			if (brillo > 1) brillo=1;
			if (brillo < -1) brillo=-1;
			
			var colorTrans:ColorTransform= new ColorTransform();
			//value between -1 and 1
			var trans:Transform=new Transform(this);
			colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = 1 - Math.abs (brillo); // color percent
			colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= (brillo > 0) ? brillo * 256 : 0; // color offset
			trans.colorTransform=colorTrans;
		}
		///----------------------------------------------------------------------------------------------------------			
		/**
		 * setConstrast
		 * @param contraste qual deve ser o contraste [-1, 1]
		 * @comment descaradamente copiado de algum forum por ai
		 */
		private function setContrast(contraste:Number):void
		{ 
			if (contraste > 1) contraste=1;
			if (contraste < -1) contraste=-1;
			/// contraste between -1 and 1
			
			var colorTrans:ColorTransform= new ColorTransform();
			//val entre -1 y 1
			var trans:Transform=new Transform(this);
			
			colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = contraste; // color percent
			colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= 128 - (128 * contraste); // color offset		
			trans.colorTransform=colorTrans;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setNormalHue
		 * volta sprite ao seu brilho normal
		 */
		public function setNormalHue():void
		{
			setBrightness(0);
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setLightHue
		 * define que a sprite deve ficar ligeiramente iluminada
		 */
		public function setLightHue():void
		{
			setBrightness(0.25);
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setDarkHue
		 * define que a sprite deve ficar ligeiramente escurecida
		 */
		public function setDarkHue():void
		{
			setBrightness(-0.25);
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseClickCallback
		 * @param func função a ser definida para ser executada conforme o click do mouse no Sprite
		 * 
		 */
		public function setOnMouseClickCallback( func:Function):void
		{
			onMouseClickCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseScrollCallback
		 * @param func função a ser definida para ser executada conforme o scroll do mouse for mexido em cima do Sprite
		 * 
		 */
		public function setOnMouseScrollCallback( func:Function):void
		{
			onMouseScrollCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseDownCallback
		 * @param func função a ser definida para ser executada conforme o click do mouse (sem mouseUp) no Sprite
		 * 
		 */
		public function setOnMouseDownCallback( func:Function):void
		{
			onMouseDownCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseUpCallback
		 * @param func função a ser definida para ser executada conforme o botão do mouse for levantado em cima do Sprite
		 * 
		 */
		public function setOnMouseUpCallback( func:Function):void
		{
			onMouseUpCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseOverCallback
		 * @param func função a ser executada quando o mouse se encontrar sobre o Sprite
		 * 
		 */
		public function setOnMouseOverCallback( func:Function):void
		{
			onMouseOverCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseMoveCallback
		 * @param func função a ser executada quando o mouse se mover sobre o Sprite
		 * 
		 */
		public function setOnMouseMoveCallback( func:Function):void
		{
			onMouseMoveCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setOnMouseOutCallback
		 * @param func função a ser executada quando o mouse sair de cima do Sprite
		 * 
		 */
		public function setOnMouseOutCallback( func:Function):void
		{
			onMouseOutCallback=func;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * onMouseDown
		 * tratador interno de descida do botão do mouse
		 * @param e
		 * 
		 */
		public function onMouseDown(e:MouseEvent):void
		{
			if (onMouseDownCallback!=null && enabled)
				onMouseDownCallback();
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * onMouseUp
		 * tratador interno de levante do botão do mouse
		 * @param e
		 * 
		 */
		public function onMouseUp(e:MouseEvent):void
		{
			if (onMouseUpCallback!=null  && enabled)
				onMouseUpCallback();
			
		}
		///----------------------------------------------------------------------------------------------------------			
		/**
		 * onMouseClick
		 * tratador interno de click do mouse no Sprite
		 * @param e
		 * 
		 */
		public function onMouseClick(e:MouseEvent):void
		{
			if (onMouseClickCallback!=null)
				onMouseClickCallback();
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * onMouseOver
		 * tratador interno do cursor do mouse sobre a sprite
		 * @param e
		 * 
		 */
		public function onMouseOver(e:MouseEvent):void
		{
			if (onMouseOverCallback!=null  && enabled)
				onMouseOverCallback();
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * onMouseMove
		 * tratador interno de movimento do cursor do mouse sobre a sprite
		 * @param e
		 * 
		 */
		public function onMouseMove(e:MouseEvent):void
		{
			if (onMouseMoveCallback!=null  && enabled)
				onMouseMoveCallback();
		}
		///----------------------------------------------------------------------------------------------------------			
		/**
		 * onMouseOut
		 * tratador interno do cursor do mouse saindo do Sprite
		 * @param e
		 * 
		 */
		public function onMouseOut(e:MouseEvent):void
		{
			if (onMouseOutCallback!=null  && enabled)
				onMouseOutCallback();
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * onMouseScroll
		 * tratador interno do scroll do mouse movido enquanto o cursor do mouse estiver em cima do Sprite
		 * @param e
		 * 
		 */
		public function onMouseScroll(e:MouseEvent):void
		{
			if (onMouseScrollCallback!=null  && enabled)
				onMouseScrollCallback();
		}
		///----------------------------------------------------------------------------------------------------------			
		public function setX(X:int):void
		{
			x=X;
		}
		///----------------------------------------------------------------------------------------------------------			
		public function setY(Y:int):void
		{
			y=Y;
		}
		///----------------------------------------------------------------------------------------------------------			
		public function setW(W:int):void
		{
			width=W;
		}
		///----------------------------------------------------------------------------------------------------------			
		public function setH(H:int):void
		{
			height=H;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * set
		 * @param X top
		 * @param Y left
		 * @param W largura
		 * @param H altura
		 * 
		 */
		public function set(X:int,Y:int,W:int,H:int):void
		{
			x=X;
			y=Y;
			width=W;
			height=H;
		}
		///----------------------------------------------------------------------------------------------------------			
		public function getX():int
		{
			return x;
		}
		///----------------------------------------------------------------------------------------------------------
		public function getY():int
		{
			return y;
		}
		///----------------------------------------------------------------------------------------------------------			
		public function getW():int
		{
			return width;
		}
		///----------------------------------------------------------------------------------------------------------						
		public function setId(Id:int):void
		{
			id=Id;
		}
		///----------------------------------------------------------------------------------------------------------
		public function getId():int
		{
			return id;
		}
		///----------------------------------------------------------------------------------------------------------
		public function getH():int
		{
			return height;
		}
		///----------------------------------------------------------------------------------------------------------						
	}
}