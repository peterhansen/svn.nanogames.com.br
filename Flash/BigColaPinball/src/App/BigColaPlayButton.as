package App {
	import GUILib.FadableSprite;
	
	import NGC.ASWorkarounds.TweenManager;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.events.MouseEvent;

	public class BigColaPlayButton extends FadableSprite {
		
		private var enabled:Boolean;
		private var play: BtPlay;
		
		public function BigColaPlayButton() {
			enabled = false;
			play = new BtPlay();
			play.buttonMode = true;
			addChild( play );
			play.gotoAndStop( 0 );
		}
		
		public function getEnabled():Boolean {
			return enabled;
		}
		
		override public function onMouseClick(e:MouseEvent):void {
			if ( !enabled ) {
				enabled = true;
				play.gotoAndStop( 1 );
				TweenManager.tween( play, "alpha", Regular.easeInOut, 1.0 , 0.0 , 0.5 /** GUIConstants.SECOND*/ );
				TweenManager.tween( play, "scaleX", Regular.easeInOut, 1.0 , 0.85 , 0.5 /** GUIConstants.SECOND*/ );
				TweenManager.tween( play, "scaleY", Regular.easeInOut, 1.0 , 0.85 , 0.5 /** GUIConstants.SECOND*/ );
			}
		}
	}
}