/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package App {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	/**
	 *
	 * @author caiolima
	 */
	public class ParticleEmitter extends MovieClip {
		// <editor-fold defaultstate="collapsed" desc="Constantes Estáticas">
		/** Aceleração da gravidade. */
		public static const HALF_GRAVITY_ACC : Number = 8;
	
		public var spriteConstructor : Function;
		private var activeParticles : int = 0;
		// </editor-fold>   
	
		private var MAX_PARTICLES : int = 0;
		public var NUMBER_OF_PARTICLES : int = 0;
		private var particles : Vector.< Particle > = new Vector.<Particle>;
	    private var isActive : Boolean = false;
	    public var freeFall : Boolean = false;
	
	    private var accTime : Number = 0;
	    private var timeBetweenParticles : Number = 0;
	
		
		public function ParticleEmitter( particlesPerSecond : Number, freeFall : Boolean, spriteConstructor : Function, width : Number, height : Number, numberOfParticles : Number, maxParticles : Number ) {
			MAX_PARTICLES = maxParticles;
	
			NUMBER_OF_PARTICLES = numberOfParticles;
	
	        setParticlesPerSecond( particlesPerSecond );
	        isActive = true;
	
	        this.spriteConstructor = spriteConstructor;
			for ( var i : int = 0; i < MAX_PARTICLES; ++i ) {
				particles.push( new Particle(this) );
				addChild( particles[ i ] );
			}
	
	        this.freeFall = freeFall;
		}
	
		
		public function setParticlesPerSecond( nOfParticles : Number ) : void {
			if(nOfParticles!=0) 
				timeBetweenParticles = 1 / nOfParticles;
			else 
				timeBetweenParticles = 1;
		}
	
	
		public function update( delta : Number ) : void {
	        if(isActive) {
	            accTime += delta;
	            if(accTime>=timeBetweenParticles) {
					const numberParticles : Number = accTime / timeBetweenParticles; // Calcula o numero de particulas a emitir
	                accTime -= numberParticles * timeBetweenParticles; // Retira do tempo acumulado o Numberervalo entre n particulas
	                while( numberParticles > 0 ) { // Emite o numero de particulas desejadas
	                    emit(); 
	                    numberParticles--;
	                }
	            }
	        }
	
			var i : int = 0;
			while ( i < activeParticles ) {
				particles[ i ].update( delta );
				if ( particles[ i ].visible ) { 
					++i; 
				}														// Se continuar visivel passa para proxima posição
				else {													// Senão
					--activeParticles;									// Decresce o número de particulas
					const p : Particle = particles[ i ];				// E troca a posição da partícula com a última
					particles[ i ] = particles[ activeParticles ];
					particles[ activeParticles ] = p;
				}
			}
		}
	
	
		public function reset() : void {
			activeParticles = 0;
		}
		
		
		public function setEmitting( a : Boolean ) : void {
			isActive = a;
		}
		
		
		public function isEmitting() : Boolean {
			return isActive;
		}
	
	
	    public function RandSpeed() : Point {
			throw new Error( "ABSTRACT RandSpeed" );
		}
		
		
	    public function RandPosition() : Point {
			throw new Error( "ABSTRACT RandPosition" );
		}
		
		
	    public function RandLifeTime() : Number {
			throw new Error( "ABSTRACT RandLifeTime" );
		}
	
	
		public function emit( quantity : int = 1 ) : void {
			for ( var i : int = 0; i < quantity; ++i ) {
			     if ( activeParticles < MAX_PARTICLES - 1 )
		             particles[ activeParticles++ ].born(RandPosition(), RandSpeed(), RandLifeTime() );
			}
		}
	}
		
		
		
		
}