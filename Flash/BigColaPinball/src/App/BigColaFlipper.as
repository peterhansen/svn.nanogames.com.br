package App
{
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Dynamics.*;
	import Box2D.Dynamics.Joints.b2RevoluteJoint;
	import Box2D.Dynamics.Joints.b2RevoluteJointDef;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Transform;
	
	public class BigColaFlipper extends Constants
	{
		public static const FLIPPER_WIDTH:int = 120;
		public static const PIVOT_WIDTH:int = 25; 
		public static const FLIPPER_HEIGHT: int = 50;
		public static const FLIPPER_SMALLER_HEIGHT: int = 32.5;
		public static const FLIPPER_MIN_ANGLE:Number = 0.3;
		public static const FLIPPER_MAX_ANGLE:Number = 0.6;
		
		private var flip:Boolean;
		private var kind:int;
		private var bodyDef:b2BodyDef;
		private var body:b2Body;
		private var rjd:b2RevoluteJointDef;
		private var appearance:MovieClip;
		private var rjb:b2RevoluteJoint;
		
		public function BigColaFlipper( _kind:int, _x:int, _y:int )
		 {
			flip = false;
			kind = _kind;	
			x = _x;
			y = _y;
			rjd = new b2RevoluteJointDef();
			var localCenter:b2Vec2;
			
			
			//pivot
			var pivotBodyDef:b2BodyDef;
			var pivotBody:b2Body;
			pivotBodyDef = new b2BodyDef();	
			var pivotBoxDef:b2CircleDef = new b2CircleDef();
			
			if ( kind == 0) {
				pivotBodyDef.position.Set( ( _x + 					  ( PIVOT_WIDTH / 2 ) ) / physScale, ( _y + ( FLIPPER_HEIGHT / 8 ) ) / physScale );				
			} else {
				pivotBodyDef.position.Set( ( _x + ( FLIPPER_WIDTH + (  PIVOT_WIDTH / 2 ) ) ) / physScale, ( _y + ( FLIPPER_HEIGHT / 8 ) ) / physScale );				
			}
							
			pivotBoxDef.friction = 0;
			pivotBoxDef.restitution = 0.00;
			pivotBoxDef.density = 0;
			pivotBoxDef.radius = 0.25;
			
			pivotBody = Main.getPhysicalWorld().CreateBody(pivotBodyDef);
			pivotBody.CreateShape(pivotBoxDef);
			pivotBody.SetMassFromShapes();
			
			
			
			//flipper
			bodyDef = new b2BodyDef();
			
			
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 4;
			
			if ( kind == 0 ) {
				( boxDef.vertices[ 0 ] as b2Vec2 ).Set(                     PIVOT_WIDTH / physScale,              0 / physScale );
				( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( FLIPPER_WIDTH + PIVOT_WIDTH ) / physScale,             ( ( FLIPPER_HEIGHT - FLIPPER_SMALLER_HEIGHT ) / 2 ) / physScale );
				( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( FLIPPER_WIDTH + PIVOT_WIDTH ) / physScale,             ( ( FLIPPER_HEIGHT + FLIPPER_SMALLER_HEIGHT ) / 2 ) / physScale );
				( boxDef.vertices[ 3 ] as b2Vec2 ).Set(                     PIVOT_WIDTH / physScale, FLIPPER_HEIGHT / physScale );
				localCenter = new b2Vec2(					  ( PIVOT_WIDTH / 2 ) / physScale, ( FLIPPER_HEIGHT / 2 ) / physScale );
			} else {
				( boxDef.vertices[ 0 ] as b2Vec2 ).Set(                 ( 0 ) / physScale, 			   ( ( FLIPPER_HEIGHT - FLIPPER_SMALLER_HEIGHT ) / 2 ) / physScale );
				( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( FLIPPER_WIDTH /*+ PIVOT_WIDTH*/ ) / physScale, 				0 / physScale );
				( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( FLIPPER_WIDTH /*+ PIVOT_WIDTH*/ ) / physScale, FLIPPER_HEIGHT / physScale );
				( boxDef.vertices[ 3 ] as b2Vec2 ).Set(                 ( 0 ) / physScale,             ( ( FLIPPER_HEIGHT + FLIPPER_SMALLER_HEIGHT ) / 2 ) / physScale );				
				localCenter = new b2Vec2( ( FLIPPER_WIDTH + (PIVOT_WIDTH ) ) / physScale, ( FLIPPER_HEIGHT / 2 ) / physScale );
			}
			
			bodyDef.position.Set( _x / physScale, _y / physScale );
			boxDef.friction = 0;
			boxDef.restitution = 0.00;
			boxDef.density = 1;
			bodyDef.userData = this;
			body = Main.getPhysicalWorld().CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
			
			bodyDef.allowSleep = true;

			/// a junta
			
			localCenter = body.GetWorldPoint( localCenter );
			
			rjd.Initialize( pivotBody, body, localCenter ); 	
			rjd.maxMotorTorque = 10.0;
			
			if ( kind == 0 ) {
				rjd.upperAngle =   FLIPPER_MIN_ANGLE;
				rjd.lowerAngle = - FLIPPER_MAX_ANGLE;	
			} else {
				rjd.upperAngle =   FLIPPER_MAX_ANGLE;
				rjd.lowerAngle = - FLIPPER_MIN_ANGLE;
			}
			
			rjd.enableLimit = true;
			rjd.motorSpeed = 0.0;
			rjd.enableMotor = true;
			
			Main.getPhysicalWorld().CreateJoint( rjd );

			///sprite de aparencia

			if ( kind != 0 ) {
				appearance = new flipper_maior_dir();
			} else {				
				appearance = new flipper_maior_esq();
			}
			
			
			addChild( appearance );
//			appearance.alpha = 0.5;
			appearance.y += PIVOT_WIDTH;
			
			if ( kind != 0 )
				appearance.x += FLIPPER_WIDTH + PIVOT_WIDTH;

			appearance.width = width - PIVOT_WIDTH;
			
		}
		
		public function doFlip():void {
			flip = !flip;

			if ( kind != 0 )
				body.ApplyTorque( 3000 );
			else
				body.ApplyTorque( -1500 );
			
		}
		
		public function unflip():void {		
			
			if ( kind != 0 )
				body.ApplyTorque( -1500 );
			else
				body.ApplyTorque(  3000 );
		}
		
		public function getPhysicalBody():b2Body {
			return body;
		}
	}
}