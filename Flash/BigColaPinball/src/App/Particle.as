package App
{
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;
	
	import NGC.ASWorkarounds.TweenManager;

	/**
	 * Classe Numbererna que descreve uma part�cula de fogos de artif�cio.
	 */
	public class Particle extends MovieClip {
		
		/** Tempo acumulado da part�cula. */
		public var accTime : Number;
		
		public var speedX : Number = 0;
		public var speedY : Number = 0;
		
		public const initialPos : Point = new Point();
		
		public var emitter : ParticleEmitter;
		
		public static const CYCLES_PER_SECOND : int = 3;
		public static const MILLISECONDS_PER_CYCLES : Number = 1 / CYCLES_PER_SECOND;
		
		public var halfRadius : Number = 0;
		public var seed : int = 0;
		
		public var frameMod : int = 0;
		public var lifeTime : Number = 0;
		
		
		public function Particle( p : ParticleEmitter ) {
			emitter = p;
			visible = false;
			
			addChild( p.spriteConstructor.call() );
		}
		
		
		public function born( bornPosition : Point, bornSpeed : Point, LifeTime : Number ) : void { 
			accTime = 0;
			frameMod = 0;
			lifeTime = LifeTime;
			
			speedX = bornSpeed.x;
			speedY = bornSpeed.y;
			visible = true;
			seed = Math.random() * 360;
			
			halfRadius = speedY / 2;
			
			if ( bornPosition ) {
				initialPos.x = bornPosition.x;
				initialPos.y = bornPosition.y;
			} else {
				initialPos.x = 0;
				initialPos.y = 0;
			}
			updatePosition();
			
			TweenManager.tween( this, "alpha", Regular.easeInOut, 0.666, 0, LifeTime ); // demôin
		}
		
		
		public function updatePosition() : void {
			if( emitter.freeFall ) {
				x = initialPos.x + speedX * accTime;
				y = initialPos.y + ( speedY * accTime ) + ( ParticleEmitter.HALF_GRAVITY_ACC * ( accTime * accTime ) );
			} else {
				x = initialPos.x + halfRadius * Math.cos( seed + ( accTime / MILLISECONDS_PER_CYCLES ) );
				y = initialPos.y + speedY * accTime;
			}
		}
		
		
		public function updateVisibility() : void {
			if( emitter.freeFall) {
				//if ( x <= 0 || x >= parent.width || y >= parent.height )
				//	visible = false;
				
				if( accTime > lifeTime) 
					visible = false;
				else 
					frameMod = Math.min( ( ( ( accTime * accTime) / (lifeTime * lifeTime) ) * emitter.NUMBER_OF_PARTICLES ), ( emitter.NUMBER_OF_PARTICLES - 1 ) );
			} else {
				//if ( y >= parent.getHeight() )
				//	visible = false;
				
				frameMod = Math.sin( seed + ( accTime / MILLISECONDS_PER_CYCLES ) );
				
				if( frameMod > 0 )
					frameMod = 2;
				else {
					if ( frameMod < -50000 ) 
						frameMod = 1;
					else
						frameMod = 0;
				}	
			}
			gotoAndStop( frameMod );
		}
		
		
		public function update( delta : Number ) : void {
			accTime += delta;
			
			updatePosition();
			updateVisibility();
		}
		
	}
}