package App
{
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoSounds.SoundManager;
	
	import fl.motion.easing.Bounce;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;

	/**
	 * @author caiolima
	 */
	public final class SparkEmitter extends ParticleEmitter {
		private var BORN_MAX_Y_DIFF : Number;
		private var BORN_MAX_HALF_Y_DIFF : Number;
		
		private var BORN_MAX_X_DIFF : Number;
		private var BORN_MAX_HALF_X_DIFF : Number;

		private var ANGLE_MIN : Number;
		private var ANGLE_DIFF : Number;
		private var SPEED_MIN : Number;
		private var SPEED_DIFF : Number;
		private var LIFE_MIN : Number;
		private var LIFE_DIFF : Number;
		private var emissivePoint : Point;


		public function SparkEmitter( emissivePoint : Point, sparksPerSecond : Number, minAngle : Number, maxAngle : Number, minSpeed : Number, speedRange : Number, dispersion : Number, minLife : Number, diffLife : Number, maxParticles : Number ) {
		    super(sparksPerSecond, true, getParticle, 0, 0, 10, maxParticles );

		    this.emissivePoint = emissivePoint;

		    ANGLE_MIN = minAngle;
		    ANGLE_DIFF = maxAngle - minAngle;
		    SPEED_MIN = minSpeed;
		    SPEED_DIFF = speedRange;
			LIFE_MIN = minLife;
			LIFE_DIFF = diffLife;

		    BORN_MAX_Y_DIFF = dispersion;
		    BORN_MAX_HALF_Y_DIFF = dispersion / 2;
		    
			BORN_MAX_X_DIFF = dispersion;
		    BORN_MAX_HALF_X_DIFF = dispersion / 2;
		}
		
		
		private static function getParticle() : DisplayObject {
			var bubble : Bubbles = new Bubbles();
			var frame:int = Math.random() * bubble.totalFrames;
			var scale:int = 10 + ( Math.random() * 30 );
			bubble.gotoAndStop( frame );
			
			bubble.scaleX /= scale;
			bubble.scaleY /= scale;
			return bubble;
		}


		public function setEmissivePosition( x : int, y : int) : void {
			emissivePoint.x = x;
			emissivePoint.y = y;
		}


		public override function RandSpeed() : Point {
		    const speed : Number = ( Math.random() * SPEED_DIFF + SPEED_MIN);
		    const angle : Number = ( Math.random() * ANGLE_DIFF + ANGLE_MIN);
		    return new Point( Math.cos( angle * Math.PI / 180 ) * speed, Math.sin( angle * Math.PI / 180) * -speed );
		}


		public override function RandPosition() : Point {
		    return new Point( - BORN_MAX_HALF_X_DIFF + Math.random() * BORN_MAX_X_DIFF, 0 );
//							  - BORN_MAX_HALF_Y_DIFF + Math.random() * BORN_MAX_Y_DIFF );
		}


		public override function RandLifeTime() : Number {
		    return ( Math.random() * LIFE_DIFF + LIFE_MIN );
		}
		
	}
}
