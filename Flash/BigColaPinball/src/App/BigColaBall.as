package App 
{
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Dynamics.*;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	/**
	 * 
	 * 
	 */
	public class BigColaBall extends Constants implements Updatable
	{
		/**
		 * 
		 */
		private var physicalBall:b2Body;
		/**
		 * 
		 */		
		private var radius:Number;
		/**
		 * 
		 */
		private var bumped:Boolean;
		
		
		/**
		 * 
		 * 
		 */
		public function getPhysicalBall():b2Body {
			return physicalBall;
		}
		
	
		
		/**
		 * 
		 */
		public function BigColaBall( _x: int , _y:int)
		{
			
			radius = 20;			
			x =  _x;
			y =  _y;

			var ball:bola = new bola();
			
			addChild( ball );
			
			ball.x -= ball.width / 2;
			ball.y -= ball.height / 2;
			
			
			width =  radius * 2;
			height = radius * 2;
	
						
			var bodyDef : b2BodyDef = new b2BodyDef();
			var circleDef : b2CircleDef = new b2CircleDef();
			var area : Number = Math.floor (radius * radius * Math.PI / 25 ) / 100;
			var body : b2Body;
			
			circleDef.radius = Math.abs( radius ) / physScale;
			circleDef.density = 1;
			circleDef.restitution = 0.1; //elasticidade
			circleDef.friction = 1;
			
			bodyDef.position.Set( x / physScale, y / physScale );
			bodyDef.userData = this;
			bodyDef.isBullet = true;
			body = Main.getPhysicalWorld().CreateBody( bodyDef );
			physicalBall = body;
			body.CreateShape( circleDef );
			body.SetMassFromShapes();
			bumped = false;
			
			
		}
		
		
		/**
		 * 
		 */
		public function update():void {
			
			
			if ( y > Main.TABLE_HEIGHT - 300 ) {
				alpha = 1 - ( ( y - ( Main.TABLE_HEIGHT - 300 ) )  / 25 );
			} else 
				alpha = 1.0;
			
			if ( physicalBall != null && physicalBall.IsSleeping() ) {
				physicalBall.ApplyForce( new b2Vec2( -5 , 0), new b2Vec2( 0 ,0 ));
			}
//			if ( physicalBall != null && physicalBall.GetWorldCenter().y > 18 && !bumped ) {
////				physicalBall.ApplyForce( new b2Vec2( 0, - 1000 ),  new b2Vec2( 20, 40 ) ) ;
//				bumped = true; 
//			} else {
//				bumped = false;
//			}
		}
	}
}