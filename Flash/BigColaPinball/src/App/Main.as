package App  {
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Dynamics.*;
	
	import fl.transitions.Squeeze;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.ui.Keyboard;
	
	/**
	 * 
	 */
//	[SWF(width='700', height='700', backgroundColor='#0000FF', frameRate='30')]
	[SWF(width='700', height='700', backgroundColor='#000000', frameRate='30')]
	public class Main extends Constants {
		private var emitter1:Sprite;
		private var emitter2:Sprite;
		private var emitter3:Sprite;
		private var emitter4:Sprite;
		private var emitter5:Sprite;

		private var sparkEmmiter1:SparkEmitter;
		private var sparkEmmiter2:SparkEmitter;
		private var sparkEmmiter3:SparkEmitter;
		private var sparkEmmiter4:SparkEmitter;
		private var sparkEmmiter5:SparkEmitter;
		
		private var emitter6:Sprite;
		private var emitter7:Sprite;
		private var emitter8:Sprite;
		private var emitter9:Sprite;

		private var sparkEmmiter6:SparkEmitter;
		private var sparkEmmiter7:SparkEmitter;
		private var sparkEmmiter8:SparkEmitter;
		private var sparkEmmiter9:SparkEmitter;
		/**
		 * 
		 */
		public static const TABLE_TOP:int = -100;
		/**
		 * 
		 */
		public static const TABLE_WIDTH:int = 660;
		/**
		 * 
		 */
		public static const TABLE_HEIGHT:int = 945;
		/**
		 * 
		 */
		public static const TABLE_NUM_BUMPERS:int = 3;
		/**
		 * 
		 */
		public static const TABLE_SLOPE:int = 1;
		
		
		private static const MAX_PARTICLES : int = 150;
		private static const MIN_ANGLE : int = 90;
		private static const MAX_ANGLE : int = 90;
		private static const SPEED_DIFF : int = 300;
		private static const MIN_SPEED : int = 80;
		private static const DISPERSION : int = 30;
		private static const SPARKS_PER_SECOND : Number = 21;
		private static const MIN_LIFE : Number = 1.05;
		private static const LIFE_DIFF : Number = 1;
		
		/**
		 * 
		 */
		private static var instance:Main;
		/**
		 * O mundo dinâmico-físico do sprite
		 */
		private var world:b2World;
		/**
		 * A bola em sí
		 */		
		private var ball:BigColaBall;
		/**
		 * Os flippers (palhetas de controle), que ficam na parte de baixo da mesa.
		 */
		private var flippers:Array;
		/**
		 * Os Bumpers ( alvos ) são as caixinhas contendo um sprite que quando acertadas
		 * se iluminam. Apenas sua parte de cima é colidivel. (será que não vai ficar confuso pro usuário não?)
		 */
		private var bumpers:Array;
		/**
		 * 
		 */
		private var pressedKeys : Array = [ false, false, false, false ];
		/**
		 * 
		 */
		private var painel: BigColaSquareBumper;
		
		private var playButtom:BigColaPlayButton;
		/**
		 * Construtor do jogo - apenas instancia os elementos do mundo, mas não inicia
		 * O jogo ainda.
		 */
		public function Main() {
			sparkEmmiter1 = new SparkEmitter( new Point( 0 , TABLE_HEIGHT ), SPARKS_PER_SECOND, MIN_ANGLE, MAX_ANGLE, MIN_SPEED, SPEED_DIFF, TABLE_WIDTH * 0.9, MIN_LIFE, LIFE_DIFF, MAX_PARTICLES );
			sparkEmmiter1.x = TABLE_WIDTH * 0.5;
			sparkEmmiter1.y = TABLE_HEIGHT * 0.70;
			addChild( sparkEmmiter1 );
			
			instance = this;
			playButtom = new BigColaPlayButton();
			addChild( playButtom );

			//cria o mundo físico
			var gravity:b2Vec2 = new b2Vec2( 0, 9.8 * TABLE_SLOPE );
			var worldAABB:b2AABB = new b2AABB();
			worldAABB.lowerBound.Set( -2000 / physScale ,-2000 / physScale);
			worldAABB.upperBound.Set(  2000 / physScale , 2000 / physScale);
			world = new b2World(worldAABB,gravity,true);
			buildScene();
			
			//cria a bola
			ball = new BigColaBall( TABLE_WIDTH / 2, TABLE_HEIGHT / 4 );
			addChild( ball );
			
			//cria a mesa
			painel = new BigColaSquareBumper( 75, 25 );
			addChild( painel );
			
			//cria os flippers
			flippers = new Array();
			var flipper:BigColaFlipper;
			
			flipper = new BigColaFlipper(0, 115, TABLE_HEIGHT - 355 );
			flippers.push( flipper );
			flipper.unflip();
			addChild( flipper );
			
			flipper = new BigColaFlipper(1, TABLE_WIDTH - 255, TABLE_HEIGHT - 355 );
			flippers.push( flipper );
			flipper.unflip();
			addChild( flipper );
			
			//cria os bumpers
			//tres caixas sólidas
			bumpers = new Array();
			var bumper:BigColaBumper;
			var leftSpace:int = ( TABLE_WIDTH - ( TABLE_NUM_BUMPERS ) * BigColaBumper.BUMPER_WIDTH ) / ( TABLE_NUM_BUMPERS + 1 );
			
			for ( var b:int = 0; b < TABLE_NUM_BUMPERS; ++b ) {
				bumper = new  BigColaBumper( b, ( BigColaBumper.BUMPER_WIDTH / 2 ) + ( ( b + 1 ) * leftSpace ) + ( ( b ) * BigColaBumper.BUMPER_WIDTH ), TABLE_HEIGHT - 510  );			
				bumpers.push( bumper );
				addChild( bumper );
			}
			
			var paramObj : Object = LoaderInfo( this.root.loaderInfo ).parameters;
			var i : int;
			for ( i = 1; i <= 3; ++i )
				setBumperCallback( i - 1, String( paramObj[ "bumperUrl" + i ] ), String( paramObj[ "bumperWindow" + i ] ) );
			
			for ( i = 1; i <= 2; ++i )
				setPainelCallback( i - 1, String( paramObj[ "destaqueUrl" + i ] ), String( paramObj[ "destaqueWindow" + i ] ) );
			
			//event handlers
			//atualiza o mundo
			///debug draw
			
//			var dbdraw:b2DebugDraw = new b2DebugDraw();
//			dbdraw.m_sprite = this;
//			dbdraw.m_drawScale = physScale;
//			dbdraw.SetFlags( b2DebugDraw.e_shapeBit |  b2DebugDraw.e_centerOfMassBit )
//			dbdraw.m_lineThickness = 2;
//			dbdraw.m_fillAlpha = 0.5;		
//			world.SetDebugDraw( dbdraw );
			
			
			/**
			 * altinha - http://www.bigcola.com.br/altinhaimaginaria
arrote - http://www.bigcola.com.br/arrotegrande
bigcola - bigcola
guarana - bigguarana
zerocafeina - bigcolazerocafeina
barcelona - fc_barcelona*/
			
//			this.painel.alpha = 0.3;

			addEventListener(Event.ENTER_FRAME, update);
//			ExternalInterface.addCallback( "setBumperCallback", setBumperCallback );
//			ExternalInterface.addCallback( "setPainelCallback", setPainelCallback );
			playButtom.x = ( ( TABLE_WIDTH  ) / 2 );
			playButtom.y = height - playButtom.height;
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		
		/**
		 * 
		 */
		public function addedToStage(e:Event):void
		{
//			width = stage.stageWidth;
//			height = stage.stageHeight - 40;

			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown );
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp );
			ExternalInterface.call( "flashLoaded" );
		}	
		
		
		/**
		 * 
		 */
		public static function getPhysicalWorld() : b2World {
			return Main.instance.world;
		}
		
		
		/**
		 * 
		 */
		public function update(e:Event) : void {
			sparkEmmiter1.update( timeStep );
			
			if ( playButtom.getEnabled() )
				world.Step( timeStep, iterations );
			
			for (var bb : b2Body = world.m_bodyList; bb; bb = bb.m_next ) {
				
				if (bb.m_userData is Sprite) {
					
					if ( ( playButtom.getEnabled() ) && ( bb.m_userData is BigColaBall || bb.m_userData is BigColaFlipper) ) {
						bb.m_userData.x = bb.GetPosition().x * physScale;
						bb.m_userData.y = bb.GetPosition().y * physScale;
						bb.m_userData.rotation = bb.GetAngle() * 180 / Math.PI;
					}
					
					if (bb.m_userData is Updatable )
						( bb.m_userData as Updatable ).update();
				}
			}
			
			if ( !playButtom.getEnabled() )
				return;
			
			if ( pressedKeys[ 0 ] )
				( flippers[ 0 ] as BigColaFlipper ).doFlip();
			else
				( flippers[ 0 ] as BigColaFlipper ).unflip();
			
			if ( pressedKeys[ 2 ] )
				( flippers[ 1 ] as BigColaFlipper ).doFlip();
			else
				( flippers[ 1 ] as BigColaFlipper ).unflip();
		}		
		
		public function buildScene(): void {
			addStaticBox(                0,         -100, TABLE_WIDTH,                 10 );
			addStaticBox(                0, TABLE_HEIGHT, TABLE_WIDTH,                 10, 7 );
			addStaticBox(                0,          -90,          10,  TABLE_HEIGHT + 90 );
			addStaticBox( TABLE_WIDTH - 10,          -90,          10,  TABLE_HEIGHT + 90 );
			
			addRamp1();
			addRamp2();
			addRamp3();
			addRamp4();
			addRamp5();
		}
		
		
		/**
		 * 
		 */
		public function addRamp1( ) : void {
			var bodyDef:b2BodyDef = new b2BodyDef();
			var body:b2Body;
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 4;
			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( 10 / physScale, ( TABLE_HEIGHT - 590 ) / physScale );
			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( 110 / physScale, ( TABLE_HEIGHT - 520 ) / physScale );
			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( 110 / physScale, ( TABLE_HEIGHT - 150 ) / physScale );
			( boxDef.vertices[ 3 ] as b2Vec2 ).Set( 10 / physScale, ( TABLE_HEIGHT  - 150 ) / physScale );
			boxDef.friction = 0.1;
			boxDef.restitution = 0.25;
			boxDef.density = 0;
			
			
			bodyDef.position.Set( 0 / physScale, 150 / physScale );
			body = Main.getPhysicalWorld().CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
			
			
			bodyDef.allowSleep = true;
			
			var appearance : MovieClip = new flipper_stand();
			addChild( appearance );
			appearance.y = TABLE_HEIGHT - 445;
			appearance.width = 140;
//			appearance.alpha = 0.5;
		}		
		
		
		/**
		 * 
		 */
		public function addRamp2( ) : void {
			var bodyDef:b2BodyDef = new b2BodyDef();
			var body:b2Body;
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 4;
			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( ( TABLE_WIDTH - 110 ) / physScale, ( TABLE_HEIGHT - 520 ) / physScale );
			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( TABLE_WIDTH -  10 ) / physScale, ( TABLE_HEIGHT - 590 ) / physScale );
			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( TABLE_WIDTH -  10 ) / physScale, ( TABLE_HEIGHT - 150 ) / physScale );
			( boxDef.vertices[ 3 ] as b2Vec2 ).Set( ( TABLE_WIDTH - 110 ) / physScale, ( TABLE_HEIGHT  - 150 ) / physScale );
			boxDef.friction = 0.1;
			boxDef.restitution = 0.25;
			boxDef.density = 0;
			
			
			bodyDef.position.Set( 0 / physScale, 150 / physScale );
			body = Main.getPhysicalWorld().CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
			
			
			bodyDef.allowSleep = true;
			
			var appearance : MovieClip = new flipper_stand();
			addChild( appearance );
			appearance.y = TABLE_HEIGHT - 445;
			appearance.width = 140;
			appearance.x = TABLE_WIDTH - appearance.width;
			flipHorizontal( appearance );
//			appearance.alpha = 0.5;
			
		}		
		/**
		 * 
		 */
		public function addRamp3( ) : void {
			var bodyDef:b2BodyDef = new b2BodyDef();
			var body:b2Body;
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 4;
			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( 110 / physScale, ( TABLE_HEIGHT  - 420 )  / physScale );
			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( ( TABLE_WIDTH / 2 ) - 60 + 15 ) / physScale, ( TABLE_HEIGHT  - 200 )  / physScale );
			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( ( TABLE_WIDTH / 2 ) - 60 ) / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			( boxDef.vertices[ 3 ] as b2Vec2 ).Set( 110 / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			boxDef.friction = 0.01;
			boxDef.restitution = 0.0;
			boxDef.density = 0;
			
			
			bodyDef.position.Set( 0 / physScale, 150 / physScale );
			body = Main.getPhysicalWorld().CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
			
			
			bodyDef.allowSleep = true;
		}		
		
		
		/**
		 * 
		 */
		public function addRamp4( ) : void {
			var bodyDef:b2BodyDef = new b2BodyDef();
			var body:b2Body;
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 4;
			
			
//			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( 292.5 / physScale, ( TABLE_HEIGHT  - 200 )  / physScale );
//			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( 390 / physScale, ( TABLE_HEIGHT  - 250 )  / physScale );
//			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( 390 / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
//			( boxDef.vertices[ 3 ] as b2Vec2 ).Set( 300 / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			
			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( ( TABLE_WIDTH - ( ( TABLE_WIDTH / 2 ) - 60 + 15 ) ) / physScale, ( TABLE_HEIGHT  - 200 )  / physScale );
			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( TABLE_WIDTH - 110 ) / physScale, ( TABLE_HEIGHT  - 420 )  / physScale );
			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( TABLE_WIDTH - 110 ) / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			( boxDef.vertices[ 3 ] as b2Vec2 ).Set( ( TABLE_WIDTH - ( ( TABLE_WIDTH / 2 ) - 60 ) ) / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			boxDef.friction = 0.01;
			boxDef.restitution = 0.0;
			boxDef.density = 0;
			
			
			bodyDef.position.Set( 0 / physScale, 150 / physScale );
			body = Main.getPhysicalWorld().CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
			
			
			bodyDef.allowSleep = true;
		}		
		
		
		/**
		 * 
		 */
		public function addRamp5( ) : void {
			var bodyDef:b2BodyDef = new b2BodyDef();
			var body:b2Body;
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 3;
//			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( 250 / physScale, ( TABLE_HEIGHT  - 200 )  / physScale );
//			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( 251.25 / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
//			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( 246.25 / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( ( ( TABLE_WIDTH / 2 ) ) / physScale, ( TABLE_HEIGHT  - 170 )  / physScale );
			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( ( TABLE_WIDTH / 2 ) + 12 ) / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( ( TABLE_WIDTH / 2 ) - 12 ) / physScale, ( TABLE_HEIGHT  - 150 )  / physScale );
			boxDef.friction = 0.01;
			boxDef.restitution = 0.0;
			boxDef.density = 0;
			
			
			bodyDef.position.Set( 0 / physScale, 150 / physScale );
			body = Main.getPhysicalWorld().CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
			
			
			bodyDef.allowSleep = true;
		}		
		
		
		/**
		 * 
		 */
		public function addStaticBox(x:Number, y:Number, halfWidth:Number, halfHeight:Number, _restitution: Number = 0.3) : void {
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.position.Set( ( x + halfWidth / 2 ) / physScale, ( y + halfHeight / 2 ) / physScale );
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.SetAsBox( halfWidth / ( 2 * physScale ) , halfHeight / (  2* physScale ) );
			boxDef.friction = 0.5;
			boxDef.restitution = _restitution;
			boxDef.density = 0.0;
			var body:b2Body = world.CreateBody(bodyDef);
			body.CreateShape(boxDef);
			body.SetMassFromShapes();
		}
		
		private function onKeyDown( e : KeyboardEvent  ) : void
		{
			const lineIndex : int = getFlipperIndexForKey( e.keyCode );
			
			if ( lineIndex >= 0 && !pressedKeys[ lineIndex ] ) {
				pressedKeys[ lineIndex ] = true;
			}
		}
		
		
		private function onKeyUp( e : KeyboardEvent  ) : void
		{
			const lineIndex : int = getFlipperIndexForKey( e.keyCode );
			
			if ( lineIndex >= 0 && pressedKeys[ lineIndex ] ) {
				pressedKeys[ lineIndex ] = false;
			}
		}		
		
		private function flipHorizontal(dsp:DisplayObject):void {
			var matrix:Matrix=dsp.transform.matrix;
			matrix.transformPoint(new Point(dsp.width/2,dsp.height/2));
			if (matrix.a>0) {
				matrix.a=-1*matrix.a;
				matrix.tx=dsp.width+dsp.x;
			} else {
				matrix.a=-1*matrix.a;
				matrix.tx=dsp.x-dsp.width;
			}
			dsp.transform.matrix=matrix;
		}
		
		private function getFlipperIndexForKey( key : uint ) : int {
			
			var k:Keyboard;
			
			switch ( /*String.fromCharCode( key ).toLowerCase() */ key) {
				//case 'a': case 'h': case ',': case '<': 
				case 37:
					return 0;
					break;			
				
//				case 's': case 'j':
//					return 1;
//					break;				
//				
//				case 'd': case 'k': case '.': case '>': 
				case 39:
					return 2;
					break;				
				
//				case 'f': case 'l':
//					return 3;
//					break;				
//				
				default:
					return -1;
			}
		} 	
		
		public function setBumperCallback( index : int, url : String, method : String ):void {
			( bumpers[ index ] as BigColaBumper ).setURL( url, method );
		}

		public function setPainelCallback( index:int, url : String, method : String ):void {
			painel.setURL( index, url, method );
		}
		
	}
}	