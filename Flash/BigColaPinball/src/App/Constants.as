package App {
	import flash.display.Sprite;
	import GUILib.FadableSprite;
	
	public class Constants extends FadableSprite {
		/**
		 * Escala do mundo físico para a tela
		 */
		static public const physScale : int = 30;
		static public const timeStep:Number = 1.0/30.0;
		static public const iterations:Number = 10.0;

	}
}