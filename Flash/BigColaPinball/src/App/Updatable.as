package App {
	public interface Updatable {
		function update() : void; 
	}
}