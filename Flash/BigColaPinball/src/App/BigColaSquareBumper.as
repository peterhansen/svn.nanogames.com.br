package App {
	
	
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.*;
	
	import GUILib.FadableSprite;
	import GUILib.GUIConstants;
	
	import NGC.ASWorkarounds.TweenManager;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.*;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public class BigColaSquareBumper  extends Constants implements Updatable {
		public static const PAINEL_BIGCOLA: int = 2;
		public static const PAINEL_BARCELONA: int = 1;
		
		public static const TOTAL_STATES: int = 2;
		public static const TIME_HIGHLIGHT: int = 8; // * GUIConstants.SECONDS;
		
		private static const PANEL_NAV_SCALE : Number = 1;
		
		public static const BUMPER_WIDTH:int = 470;
		public static const BUMPER_HEIGHT:int = 150;
		
		public static const BORDER_X:int = 40;
		public static const BORDER_Y:int = 100;
		
		private var smoke: smoke_container;		
		private var painel: destaques;
		private var painelSprite:FadableSprite;
		private var body:b2Body;
		private var accTime:Number = 0.0;
		private var currentState: int = 0;
		private var callbacks : Array;
		private var windows : Array;
		private var tsX:Tween;
		private var tsY:Tween;
		
		public function BigColaSquareBumper( _x:int, _y:int ) {
			x = _x;
			y = _y;
			
			callbacks = new Array();
			windows = new Array();
			
			painelSprite = new FadableSprite();
			///instancia os paineis 
			painel = new destaques();
//			painel.alpha = 0.5;
//			addChild( painel );
			painelSprite.addChild( painel );
//			painel.scaleX = 0.98;
//			painel.scaleY = 0.98;
			addChild( painelSprite );
			smoke = new smoke_container();
			smoke.scaleX = 0.98;
			smoke.scaleY = 0.98;
			
			addChild( smoke );
			updateApperance();
			
			///modelo físico
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.allowSleep = true;
			
			var boxDef:b2PolygonDef = new b2PolygonDef();
			boxDef.vertexCount = 4;
			( boxDef.vertices[ 0 ] as b2Vec2 ).Set( _x / physScale,( Main.TABLE_TOP - 20)/ physScale );
			( boxDef.vertices[ 1 ] as b2Vec2 ).Set( ( BUMPER_WIDTH + _x) / physScale, (Main.TABLE_TOP - 20) / physScale );
			( boxDef.vertices[ 2 ] as b2Vec2 ).Set( ( BUMPER_WIDTH + _x) / physScale, BUMPER_HEIGHT / physScale );
			( boxDef.vertices[ 3 ] as b2Vec2 ).Set( _x / physScale, BUMPER_HEIGHT / physScale );
			boxDef.friction = 0.5;
			boxDef.restitution = 0.8;
			boxDef.density = 0;
			bodyDef.userData = this;
			
			
			bodyDef.position.Set( BORDER_X / physScale, BORDER_Y / physScale );
			body = Main.getPhysicalWorld().CreateBody( bodyDef );
			body.CreateShape( boxDef );
			body.SetMassFromShapes();
			
			
			body.PutToSleep();
			painel.nav02.buttonMode = true;
			painel.nav02.addEventListener( MouseEvent.CLICK, onNav02 );
			painel.nav03.buttonMode = true;
			painel.nav03.addEventListener( MouseEvent.CLICK, onNav03 );
			painel.gotoAndStop( 1 );
			
			painel.nav02.scaleX = PANEL_NAV_SCALE;
			painel.nav02.scaleY = PANEL_NAV_SCALE;
			painel.nav03.scaleX = PANEL_NAV_SCALE;
			painel.nav03.scaleY = PANEL_NAV_SCALE;
			
//			painel.chamadaGrande_01.addEventListener( MouseEvent.CLICK, click );
			painel.chamadaGrande_01.buttonMode = true;
			painel.gotoAndStop( 2 );
			painel.addEventListener( MouseEvent.CLICK, click );
			painel.chamadaGrande_02.buttonMode = true;
			painel.gotoAndStop( 1 );
			painel.buttonMode = true;
		}
		
		private function onNav02( e:MouseEvent ): void {
			currentState = 0;
			accTime = 0;
			updateApperance()
		}

		private function onNav03( e:MouseEvent ): void {
			currentState = 1;
			accTime = 0;
			updateApperance()
		}
		
		
		private function click( e:MouseEvent ):void {
			if (!( e.target is navegacao )) 
			if ( callbacks[ currentState ] != null ){
				trace("squareBumper::Click");
				trace ( e.target )

				try {
					var targetURL : URLRequest = new URLRequest( callbacks[ currentState ] );
					trace( "----->" + targetURL + ", " + windows[ currentState ] );
					navigateToURL( targetURL, windows[ currentState ] );
//					ExternalInterface.call( "route", callbacks[ currentState ]);
				} catch (e:Error) {
					trace("Ops! URL não existe ou está fora do ar!");
				}
			}
		}
		
		
		public function setURL( index:int, Url:String, window : String ):void {
			callbacks[ index ] = Url; 
			windows[ index ] = window; 
		}
		
		private function updateApperance(): void {
			painel.gotoAndStop( currentState + 1 );
			smoke.gotoAndStop( currentState + 1 );
			
			painel.nav02.gotoAndStop( ( currentState == 0 )? "on" : "off" );
			painel.nav03.gotoAndStop( ( currentState == 1 )? "on" : "off" );

		}
		
		public function update():void {
			
			accTime += timeStep;
			if ( accTime >= TIME_HIGHLIGHT ) {
				currentState = ( ( currentState + 1 ) % ( TOTAL_STATES  ) );
				accTime = 0;
				updateApperance();				
			}
			
			if ( body.IsSleeping() ) {
				painelSprite.setNormalHue();
				scaleX = 1.0;
				scaleY = 1.0;
			} else {
				painelSprite.setLightHue();
				tsX = TweenManager.tween( this, "scaleX", Regular.easeInOut, 1.01 , 1.0 , 0.1 /** GUIConstants.SECOND*/ );
				tsY = TweenManager.tween( this, "scaleY", Regular.easeInOut, 1.01 , 1.0 , 0.1 /** GUIConstants.SECOND*/ );
				body.PutToSleep();
			}
		}
	}
}