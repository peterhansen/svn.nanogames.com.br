package App 
{
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.*;
	
	import GUILib.FadableSprite;
	
	import NGC.ASWorkarounds.TweenManager;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public class BigColaBumper extends Constants implements Updatable
	{
		public static const BUMPER_WIDTH:int = 150;
		public static const BUMPER_HEIGHT:int = 100;
		
		private var appearance : FadableSprite;
		private var body:b2Body;
		private var url:String;
		private var window:String;
		
		public function BigColaBumper( kind:int, _x:int, _y:int )
		{
			
			
			x = _x;
			y = _y;
			var radius : Number = 35;
			appearance = new FadableSprite();
			
			switch (kind ) {
				case 0:
					appearance.addChild( new bumper_arroteGrande() );
					appearance.x -= BUMPER_WIDTH / 10;
					break;
				case 1:
					appearance.addChild( new bumper_zeroCafeina() );
					break;
				case 2:
					appearance.addChild( new bumper_altinhaImaginaria() );
					appearance.x += BUMPER_WIDTH / 10;
					break;
			}
			
			appearance.alpha = 0.95;
			addChild(appearance);
			appearance.buttonMode = true;
				
			appearance.x -= ( appearance.width - ( BUMPER_WIDTH / physScale ) ) / 2;
			appearance.y -= ( appearance.height - ( BUMPER_HEIGHT / physScale ) ) / 2;
//			appearance.alpha = 0.5;
			
			var bodyDef:b2BodyDef = new b2BodyDef();

			bodyDef.allowSleep = true;
			
			var circleDef : b2CircleDef = new b2CircleDef();
			var area : Number = Math.floor (radius * radius * Math.PI / 25 ) / 100;
			
			circleDef.radius = Math.abs( radius ) / physScale;
			circleDef.density = 0;
			circleDef.restitution = 1.2; //elasticidade
			circleDef.friction = 0.5;
			bodyDef.position.Set( _x / physScale, _y / physScale );			
			circleDef.userData = this;
			bodyDef.userData = this;
			body = Main.getPhysicalWorld().CreateBody( bodyDef );
			body.CreateShape( circleDef );
			body.SetMassFromShapes();
			this.setEnabledState( true );
			this.setEnabled();
			body.PutToSleep();
		}
		
	    public function setURL( Url:String, window : String ) : void {
			appearance.setOnMouseClickCallback( navigate );
			url = Url;
			this.window = window;
		}
		
		private function navigate():void {
//			var myRequest:URLRequest = new URLRequest( url );
			try {
//				navigateToURL(myRequest, "_blank");
				trace( "----->" + url + ", " + window );
				var targetURL : URLRequest = new URLRequest( url );
				navigateToURL( targetURL, window );
				
//				ExternalInterface.call( "route", url); TODO
			} catch (e:Error) {
				trace("Ops! URL não existe ou está fora do ar!");
			}
		}
		
		public function update():void {
			if ( body != null ) {
				if ( body.IsSleeping() ) {
					this.setNormalHue();	
				} else {
					TweenManager.tween( this, "scaleX", Regular.easeInOut, 1.1 , 1.0 , 0.1 /** GUIConstants.SECOND*/ );
					TweenManager.tween( this, "scaleY", Regular.easeInOut, 1.1 , 1.0 , 0.1 /** GUIConstants.SECOND*/ );					
					this.setLightHue();
					body.PutToSleep();
				}
			}
		}
	}
}