package NGC
{
	
	import NGC.Crypto.BigInt.BigInteger;
	
	import flash.utils.ByteArray;
	
	public class ByteArrayEx extends ByteArray
	{
		// Construtor
		public function ByteArrayEx()
		{
			super();
		}
		
		public function readBigInt() : BigInteger
		{
			const longAsArray : ByteArray = new ByteArray();
			readBytes( longAsArray, 0, 8 );	
			return new BigInteger( longAsArray );
		}
		
		public function writeBigInt( bi : BigInteger ) : void
		{
			const longAsArray : ByteArray = bi.toByteArray();
			writeBytes( longAsArray, 0, 8 );
		}
		
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function readLongCheated() : int
		{
			readInt();
			return readInt();
		}
		
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function writeLongCheated( l : int ) : void
		{
			writeInt( 0 );
			writeInt( l );
		}
		
		public function readUTCTime() : Number
		{
			// + temp => Heurística que consegue consertar os casos em que a data volta às 23:59:59 do dia anterior
			var temp : int = readInt();
			return ( temp * uint.MAX_VALUE ) + readUnsignedInt() + temp;
		}
		
		public function writeUTCTime( n : Number ) : void
		{
			writeInt( int( n / uint.MAX_VALUE ) );
			writeUnsignedInt( n % uint.MAX_VALUE );
		}
		
		public function readChar() : String
		{
			return String.fromCharCode( readShort() );
		}
		
		public function writeChar( char : String ) : void
		{
			writeShort( int( char.charCodeAt(0) ) );
		}
	}
}