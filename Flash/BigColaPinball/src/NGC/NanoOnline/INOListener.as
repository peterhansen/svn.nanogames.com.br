package NGC.NanoOnline
{
	public interface INOListener
	{
		/**
		* Indica que uma requisição foi enviada para o NanoOnline 
		*/
		function onNORequestSent() : void;

		/**
		* Indica que a requisição foi cancelada pelo usuário 
		*/
		function onNORequestCancelled() : void;

		/**
		* Indica que uma requisição foi respondida e terminada com sucesso
		*/
		function onNOSuccessfulResponse() : void;

		/**
		* Sinaliza erros ocorridos nas operações do NanoOnline
		* @param errorCode O código do erro ocorrido
		* @param errorStr Descrição do erro ocorrido
		*/		
		function onNOError( errorCode : NOErrors, errorStr : String ) : void;

		/**
		* Indica o progresso da requisição atual
		* @param currBytes A quantidade de bytes que já foi transferida
		* @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		*/		
		function onNOProgressChanged( currBytes : int, totalBytes : int ) : void;
	}
}