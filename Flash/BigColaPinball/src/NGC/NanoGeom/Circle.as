package NGC.NanoGeom
{
	import flash.geom.Vector3D;

	public final class Circle
	{
		// Raio do círculo
		private var _radius : Number;

		// Centro do círculo
		private var _center : Vector3D;

		// Construtor
		public function Circle( centerPos : Vector3D, r : Number )
		{
			radius = r;
			center = centerPos;
		}
		
		// Métodos
		public function clone() : Circle
		{
			return new Circle( center, radius );
		}
		
		// Retorna zero se não houve colisão ou um número que indica o quanto os raios estão sobrepostos
		public function checkCollision( otherCircle : Circle ) : Number
		{
			const distBetweenCenters : Number = Vector3D.distance( otherCircle.center, center );
			const radiusesSum : Number = radius + otherCircle.radius;
			if( distBetweenCenters >= radiusesSum )
				return 0.0;
			return radiusesSum - distBetweenCenters;
		}
		
		// Getters e Setters
		public function get center() : Vector3D
		{
			return _center;
		}
		
		public function set center( v : Vector3D ) : void
		{
			_center = v;
		}
		
		public function get radius() : Number
		{
			return _radius;
		}
		
		public function set radius( r : Number ) : void
		{
			_radius = r;
		}
	}
}