package{		import flash.display.*;	import flash.events.*; 	import flash.utils.*;		public class Recycling_mc_cronometro extends MovieClip
		{
//----------------------------------------------------------------------------------------------------------------
//      Variaveis
//----------------------------------------------------------------------------------------------------------------
	private var jogoRef:jogo;	private var segundos:uint;	private var controlador_intervalos:Timer;	private var iniciado:Boolean=false;	//tempo que cada fase dura	private var tempoMaximo:uint=99;
	private var paused:Boolean;//----------------------------------------------------------------------------------------------------------------
//      Funcoes
//----------------------------------------------------------------------------------------------------------------
//================================================================================================================
//DM evita que o antes da fase comecar, o cronometro apareca com o tempo da fasea anterior
	public function apagaContador():void
		{			contador.text="";																				   			}
//================================================================================================================
	public function togglePause():void
		{		paused=!paused;
		if (paused)
			{
				if (controlador_intervalos!=null)
					controlador_intervalos.stop();
			}
			else
			{
				if (controlador_intervalos!=null)
					controlador_intervalos.start();
			}		}			
//================================================================================================================
	private function contaSegundo(p_evt:TimerEvent):void
		{			contador.text=converteStringTempo(segundos--);			jogoRef.decrementaTempo();		}
//================================================================================================================
	private function completaTempo(p_evt:TimerEvent)
		{			///oops			jogoRef.terminaJogoPorPonto("tempo_acabou");					}//================================================================================================================
	private function converteStringTempo(p_segundos:uint):String
		{			var segundos:uint=p_segundos%60;			var str_segundos:String=segundos.toString();
						if(segundos<10)
				str_segundos="0"+str_segundos;
							var minutos:uint=Math.floor(p_segundos/60);			return (minutos.toString()+":"+str_segundos.toString());		}
//================================================================================================================
	public function Recycling_mc_cronometro()
		{			segundos=tempoMaximo;			inicia();
			apagaContador();		}//================================================================================================================
	public function set pai(p_main):void
		{			jogoRef=p_main;		}//================================================================================================================
	public function zera():void
		{
			para();			segundos=tempoMaximo;			controlador_intervalos.reset();
		}//================================================================================================================
	public function para():void
		{						controlador_intervalos.stop();			
		}
//================================================================================================================
	public function reseta()
		{		if(iniciado)
			{
				paused=false;				zera();				controlador_intervalos.start();			}		}//================================================================================================================
	public function inicia():void
		{						controlador_intervalos = new Timer(1000, segundos+2);			controlador_intervalos.addEventListener(TimerEvent.TIMER, contaSegundo);			controlador_intervalos.addEventListener(TimerEvent.TIMER_COMPLETE, completaTempo);
			//DM faz com que o cronometro nao dispare logo de cara///			//controlador_intervalos.start();                      //
			/////////////////////////////////////////////////////////
			//DM evita que o primeiro segundo nao apareca ///////////
			 reiniciaContador();						           //
			/////////////////////////////////////////////////////////			iniciado=true;		}
//================================================================================================================
	public function reiniciaContador():void
		{
		//DM evita que o primeiro segundo nao apareca ///////////
		contador.text=converteStringTempo(segundos);           //
		/////////////////////////////////////////////////////////
		}		
//================================================================================================================
	public function reinicia():void
		{			zera();			inicia();		}
//================================================================================================================
	public function get tempo_restante():Number
		{			return (tempoMaximo-segundos);		}//================================================================================================================
	}}