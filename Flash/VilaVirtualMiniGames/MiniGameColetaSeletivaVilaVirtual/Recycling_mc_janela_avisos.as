﻿package{
	
	import flash.display.*;
	import caurina.transitions.Tweener;
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.*;

	public class Recycling_mc_janela_avisos extends MovieClip
		{
//----------------------------------------------------------------------------------------------------------------
//      Variaveis
//----------------------------------------------------------------------------------------------------------------

////////variaveis criadas em timeline (incluidas aqui apenas por motivos de clareza)//////////////////////////////
//	private var mc_aviso:mc_aviso_inicio_fase;
//  private var bt_sair_x;
//  private var janela_avisos:Recycling_mc_janela_avisos;

	///nao e' nem de longe a melhor das solucoes...mas..
	private var playerGlobalScore:Number;
	//deveria ter tipo, mas tem horas que _pai e' jogo, tem horas que _pai e' main...#FAIL
	private var _pai;
	///mas eu vou contornar isso...no momento que eu conseguir uma referencia, eu guardo...
	private var mainRef:SceneRecycling;
	private var jogoRef:jogo;
	private var avisoRef:mc_aviso_inicio_fase;
	//para dar aquele tempinho antes de mostrar a janela laranja...	
	private var showTimer:Timer;
	//variavel que guarda temporariamente seo jogo é para ser reiniciao ou nao ao chamar o inicio da fase nova
	private var reinicia:Boolean=false;		
	private const corLixo:Array=new Array("0x004CBC","0xff0000","0x009900","0xFF9800","0x653D00","0x793976");
	private var trashCan:lixeira;
	private var textField:TextField;
	private var icones:Array;
	private var book:RecyclingAddervilleBook;
	private var tipoAtual:int;
	private var tipoLixo:Array;
	private var tipContent:Sprite;
	private var lixoTiposStrings:Array;

//----------------------------------------------------------------------------------------------------------------
//      Funcoes
//----------------------------------------------------------------------------------------------------------------
//================================================================================================================
	private function getJogoRef():jogo
	{
			trace("mc_janela_avisos::callSair()");
			
			if (jogoRef!=null)
				return jogoRef;
			else
			{
				if (mainRef!=null && mainRef.getJogo()!=null)
				{
					trace("mc_janela_avisos::callSair() >> minha referencia inicial era nula");
					return mainRef.getJogo();
				}
				else
				{
					if (_pai!=null && _pai.getJogo()!=null)
						{
							trace("mc_janela_avisos::callSair() >> minhas referencias diretas eram nula");
							return _pai.getJogo();;
						}
						else
						{
							trace("mc_janela_avisos::callSair() >> nao tenho referencia para jogo!");
							return null;
						}
				}					
			}
		return null;		
	}

	private function callSair():void
		{
			getJogoRef().sair();
		}

	public function setGlobalScore(Score:Number):void
		{
		playerGlobalScore=Score;
		}
//================================================================================================================
	private function iniciaProximoNivel():void
		{
			_pai.iniciaProximoNivel();
		}
//================================================================================================================
	private function acaoBtFecha(p_evt:MouseEvent):void
		{
			trace("#1");
			Sounds.playInteracaoSound();			
			play();
			Sounds.playMusic();			
		}
//================================================================================================================
	private function acaoBtFechaX(p_evt:MouseEvent):void
		{			
			trace("#2");			
			Sounds.playInteracaoSound();			
			play();
			Sounds.playMusic();					
			trace(_pai);
			_pai.mc_jogo.barra_coleta.mouseEnabled=true;
			_pai.mc_jogo.barra_coleta.mouseChildren=true;

		}
//================================================================================================================
///nao era melhor dar apenas stop ao inves de gotoAndStop(1)?
///porque mesmo que se use apenas play depois, depender disso e' horrivel. se depender do play depois, deveria usar
///gotoAndPlay
	private function acaoTermonouAnimacaoAvisoNovaFase():void
		{
		if(reinicia)
			{
				_pai.reiniciaJogoZero();
				reinicia=false;
			}
			else
				iniciaProximoNivel();
				
			gotoAndStop(1);				
		}
		
	public function setScoreAndHighScore(s:String,b:Boolean):void		
		{
		if (txtPlacar!=null)
			txtPlacar.text=s;
			
		if (txtHighScore!=null)
			txtHighScore.visible=b;			
		}
//================================================================================================================
//?????
	private function acaoBtSairFimJogoGanho(p_evt:MouseEvent):void
		{
			Sounds.playInteracaoSound();		
			callSair();	
		}
//================================================================================================================
	private function acaoReiniciaJogo(p_evt:MouseEvent):void
		{
			tipoAtual=0;
			Sounds.playInteracaoSound();			
			//DM: chega - e' tanto bacalhau que so' trapaceando mesmo!
			if (avisoRef!=null)
				avisoRef.gotoAndStop(1);
				
			reinicia=true;
			trace("mc_janela_avisos::acaoReiniciaJogo() >>clicou no bt de reiniciar jogo");			
			play();
		}
//================================================================================================================
//santo bacalhau, batman...so' tentando corrigir um bug...
	/*private*/
		public function PreparaQuadroInicioFase():void
		{
			avisoRef=mc_aviso;			
		}
		
	public function posicionaQuadroInicioFase():void
		{
			/*
			if (!reinicia)
				mc_aviso.gotoAndStop(_pai.nivel+1);
			else
				mc_aviso.gotoAndStop(1);
				*/
			if (_pai==null) trace("_pai e' nulo");
			if (mc_aviso==null) trace("mc_aviso e' nulo");
			if (mainRef==null) trace("mainRef e' nulo");			
			if (windowTitle==null) trace("windowTitle e' nulo");
				
			if (!reinicia)
				windowTitle.setText("Nível "+(_pai.nivel+1).toString());
			else
				windowTitle.setText("Nível 1");
				
			windowTitle.mouseEnabled=false;
			windowTitle.mouseChildren=false;
		}
//================================================================================================================
	private function acaoBtSair(p_evt:MouseEvent):void
		{
			Sounds.playInteracaoSound();			
			trace("mc_janela_avisos::acaoBtSair() >>clicou bt sair");
			callSair();
		}
//================================================================================================================
	public function acaoInicioFase():void
		{	
			gotoAndPlay("inicio_fase");
		}
//================================================================================================================
	public function Recycling_mc_janela_avisos()
		{
			tipoAtual=0;
			stop();
			//bt_fechar.addEventListener(MouseEvent.CLICK, acaoBtFecha);
			
			
			/*DM: infelizmente, tive que duplicar, porque nao consegui pegar a mesma tabela da classe jogo**/
			tipoLixo=new Array();
			tipoLixo.push(new Array("metal_atum"         , 3));
			tipoLixo.push(new Array("metal_cano"         , 3));
			tipoLixo.push(new Array("metal_coca"         , 3));
			tipoLixo.push(new Array("metal_oleo"         , 3));
			tipoLixo.push(new Array("metal_prego"        , 3));
			tipoLixo.push(new Array("organico_banana"    , 4));
			tipoLixo.push(new Array("organico_folha"     , 4));
			tipoLixo.push(new Array("organico_maca"      , 4));
			tipoLixo.push(new Array("organico_peixe"     , 4));
			tipoLixo.push(new Array("organico_pernil"    , 4));
			tipoLixo.push(new Array("papel_desenho"     , 0));
			tipoLixo.push(new Array("papel_envelope"    , 0));
			tipoLixo.push(new Array("papel_jornal"      , 0));
			tipoLixo.push(new Array("papel_papelao"     , 0));
			tipoLixo.push(new Array("papel_revista"     , 0));
			tipoLixo.push(new Array("pilha_1"           , 5));
			tipoLixo.push(new Array("pilha_2"           , 5));
			tipoLixo.push(new Array("pilha_3"           , 5));
			tipoLixo.push(new Array("pilha_4"		    , 5));
			tipoLixo.push(new Array("plastico_biscoito" , 1));
			tipoLixo.push(new Array("plastico_canudo"   , 1));
			tipoLixo.push(new Array("papel_copo"        , 1));
			tipoLixo.push(new Array("plastico_pet"      , 1));
			tipoLixo.push(new Array("plastico_sacola"   , 1));
			tipoLixo.push(new Array("plastico_sorvete"  , 1));
			tipoLixo.push(new Array("vidro_casco"       , 2));	
			tipoLixo.push(new Array("vidro_casco_2"     , 2));
			tipoLixo.push(new Array("vidro_copo"        , 2));
			tipoLixo.push(new Array("vidro_copo_2"      , 2));
			tipoLixo.push(new Array("vidro_vinho"       , 2));	
			lixoTiposStrings=new Array();
			lixoTiposStrings[0]=GameStrings.LIXO_PAPEL;
			lixoTiposStrings[1]=GameStrings.LIXO_PLASTICO;
			lixoTiposStrings[2]=GameStrings.LIXO_VIDRO;
			lixoTiposStrings[3]=GameStrings.LIXO_METAL;
			lixoTiposStrings[4]=GameStrings.LIXO_BIOLOGICO;
			lixoTiposStrings[5]=GameStrings.LIXO_TOXICO;														
			icones=new Array();					
			tipContent=new Sprite();
			addChild(tipContent);
			
		}
//================================================================================================================
	public function set pai(p_main):void
		{
			_pai=p_main;
			trace("mc_janela_avisos::set pai() >>"+"associou PAI");
			trace("mc_janela_avisos::set pai() >>"+_pai.toString());
			if (p_main is jogo)
				jogoRef=p_main;
			else
			if (p_main is SceneRecycling)
				mainRef=p_main;
		}
//================================================================================================================
	private function scheduleAbre(e:TimerEvent):void
		{
		if (showTimer!=null)
			showTimer.stop();
		showTimer=null;
		
		play();
		Tweener.addTween(this,{
								scaleX:1, scaleY:1, alpha:1, time:.4, transition:"easeOutBack", 
								onStart:function()
											{
											this.visible=true
											}
								}
						);
		}
//================================================================================================================
///DM TODO: colocar a musica de level up
	public function abre(p_destino:String):void
		{
		gotoAndStop(p_destino);		
		



		showTimer=new Timer(150);
		showTimer.addEventListener(TimerEvent.TIMER,scheduleAbre);
		showTimer.start();
		
		
		//scheduleAbre();
		
			/*
			
		gotoAndPlay(p_destino);
		Tweener.addTween(this,{
								scaleX:1, scaleY:1, alpha:1, time:.4, transition:"easeOutBack", 
								onStart:function()
											{
											this.visible=true
											}
								}
						);
						*/
		}
//================================================================================================================
	public function fecha():void
		{
		Tweener.addTween(this,{
							  scaleX:.7, scaleY:.7, alpha:0, time:.4, transition:"easeInBack",
	  				 		  onComplete:function()
									 	{
									 	this.visible=false
									 	}
								}
						);
		}
//================================================================================================================
	public function limpaTudo():void
		{
			_pai.limpaLixosTela();
			_pai.limpaLixeirasTela();	
		}
//================================================================================================================
	public function escondeTudo():void
		{
		if (_pai is SceneRecycling)
			{
			if (_pai.getJogo()==null)	
				return;
		
			if (_pai.getJogo().lista_lixos==null)
				return;
		
			if (_pai.getJogo().lista_lixeiras==null)
				return;
		
			for(var i:uint=0; i<_pai.getJogo().lista_lixos.length; i++)
				_pai.getJogo().lista_lixos[i].visible=false;
	
			for(var d:uint=0; d<_pai.getJogo().lista_lixeiras.length; d++)
				_pai.getJogo().lista_lixeiras[d].visible=false;
		
			trace("mc_janela_avisos::escondeTudo() >>"+"escondeu tudo");
			}
		else
			trace("mc_janela_avisos::escondeTudo() >>"+"escondeu nada");
		}
//================================================================================================================
//DM TODO: estudar a possibilidade de usar lixo::some(), ja que ele desativa o lixo.
	public function mostraTudo():void
		{
		return;

		if (_pai is SceneRecycling)
			{
	
			if (_pai.getJogo()==null)	
				return;
		
			if (_pai.getJogo().lista_lixos==null)
				return;
		
			if (_pai.getJogo().lista_lixeiras==null)
				return;
	
			for(var i:uint=0; i<_pai.getJogo().lista_lixos.length; i++)
				_pai.getJogo().lista_lixos[i].visible=true;
				
			for (var h:uint=0;h<_pai.getJogo().numChildren;h++)
				if (_pai.getJogo().childAt(h) is lixo)
					_pai.getJogo().childAt(h).visible=false;
	
			for(var d:uint=0; d<_pai.getJogo().lista_lixeiras.length; d++)
				_pai.getJogo().lista_lixeiras[d].visible=true;
		
			trace("mc_janela_avisos::mostraTudo() >>"+"mostrou tudo");
			}
		else
			trace("mc_janela_avisos::mostraTudo() >>"+"mostrou nada");
		}
//================================================================================================================
	public function preencheResumo():void
		{
			mc_texto_nivel.txt_nivel.text=_pai.mc_jogo.getNivelAtual()+"ª fase completa!" ;
			
			mc_texto_nivel.txt_pontos_fase.text="Pontos: "
			+_pai.mc_jogo.quadro_pontos.getPontosFase(_pai.mc_jogo.getNivelAtual());
			
		//	mc_texto_nivel.txt_bonus_fase.text="Bônus: "
		//	+_pai.mc_jogo.quadro_pontos.getBonusFase(_pai.mc_jogo.getNivelAtual());		
			
			///preciso somar localmente o bonus, porque o unico ponto em que eu consigo pegar o score local ainda 
			//nao contabiliza o bonus. mas se eu pedi-lo, ele faz mais que isso, e o resto do sistema depende desse 
			//efeito colateral
			mc_texto_nivel.txt_pontos.text="Pontos Acumulados: "+(playerGlobalScore).toString();
			//_pai.mc_jogo.quadro_pontos.getPontosTotal();
		}		
//================================================================================================================
	
		///DM: tive que roubar de jogo. nao deu pra re-aproveitar	
	private function criaObjetoDeClasse(p_nome_classe:String):Object
		{
		var referencia_classe:Class= getDefinitionByName(p_nome_classe) as Class;
		var objeto_temp:Object=new referencia_classe();
		return objeto_temp;	
		}
	
	private function showTip():void
		{
			mouseChildren=true;
			mouseEnabled=true;
			this.setChildIndex(tipContent,this.numChildren-1);

			if (trashCan==null)
			{
				trashCan=new lixeira(this,"","0xFF0000",false)
				trashCan.x=myX(15);
				trashCan.y=myY(25);
				tipContent.addChild(trashCan);				
			}

			
			if (textField==null)
			{
				
				var s:Shape=new Shape();
				tipContent.addChild(s);
				s.alpha=0.5;
				
				textField=new TextField();
				textField.embedFonts=true;
				tipContent.addChild(textField);				
				textField.x=trashCan.x+trashCan.width - 10;
				textField.x+=5;
				textField.y=trashCan.y+20;
				textField.y+=5;
				textField.width=myWidth()-trashCan.width-10-15;
				textField.width-=5;
				textField.height=trashCan.height-25;
				textField.height-=5;
				textField.wordWrap=true;
				textField.multiline=true;
				var textFormat:TextFormat;				
				textFormat=new TextFormat();
				book=new RecyclingAddervilleBook();
				textFormat.font=book.fontName;
				textFormat.size=14;
				textFormat.color=0x8B4500;
				textField.mouseEnabled=false;
				textField.selectable=false;				
				textField.setTextFormat(textFormat);
				textField.defaultTextFormat=textFormat;
				s.graphics.beginFill(0xFFFFFF);
				s.graphics.drawRect( textField.x-5,textField.y-5,textField.width+10,textField.height+10);
				s.graphics.endFill();		
			}
			trace (lixoTiposStrings[tipoAtual].length);
			trace (Math.random() * lixoTiposStrings[tipoAtual].length);
			trace (tipoAtual);
			
			var tipIndex:int;
			
			if (_pai.mc_jogo.isStoryMode())
					tipIndex=0
				else
					tipIndex=int(Math.random() * lixoTiposStrings[tipoAtual].length);

			textField.text = lixoTiposStrings[tipoAtual][ tipIndex ];
			trashCan.pinta(corLixo[tipoAtual]);			
			
			
			
			var inserir:Array=new Array();
			var padding:int=0;
			
			for (var d:int=0;d<tipoLixo.length;d++)			
				if (tipoLixo[d][1]==tipoAtual)
					inserir.push(d);
			
			///6= o maior numero de lixo numa categoria
			///57 = 50 + 7 -> 50 = tamanho do icone ; 7 espacamento padrao minimo
			padding=(6-inserir.length)*57/(inserir.length-1);
			
			for (var e:int=0;e<inserir.length;e++)
			{				
				var p:Sprite=new Sprite();
				s=new Shape();
				var m:MovieClip;
				tipContent.addChild(p);				
				p.addChild(s);
				icones[e]=p;
				
				s.graphics.beginFill(0xFFFFFF);
				s.graphics.drawRect( 0,0,50,50);
				s.graphics.endFill();
				
				s.alpha=0.5;
				
				m=MovieClip(criaObjetoDeClasse(String(tipoLixo[inserir[e]][0])));
				
				if (m.width>m.height)
					{
						m.height=50*m.height/m.width;
						m.width=50;
					}
					else
					{
						m.width=50*m.width/m.height;						
						m.height=50;
					}					
				p.addChild(m);
				
				m.x=(50-m.width)/2;
				m.y=(50-m.height)/2;
				p.x=( trashCan.x + ((50 + 7+padding) * e));
				p.y=( myY(0)+myHeight()-15-50 );
			}
			tipContent.mouseEnabled=true;			
			tipoAtual++;
			tipoAtual=tipoAtual%corLixo.length;
			tipContent.visible=true;
		}
//================================================================================================================
	private function hideTip():void
		{
			mouseEnabled=false;
			mouseChildren=false;
			for (var c:int=0;c<icones.length;c++)
			{
				if (icones[c]!=null)
				{
				tipContent.removeChild(icones[c]);
				icones[c].visible=false;
				icones[c]=null;
				}
			}
			tipContent.mouseEnabled=false;
			tipContent.visible=false;
			
		}				
		
	private function myWidth():Number
		{
			return 368.95;	
		}
	
	private function myHeight():Number
		{
			return 222.95;
		}
	
	private function myY(y:Number):Number
		{
			return -(myHeight()/*height*/ /2)+y;	
		}
	private function myX(x:Number):Number
		{
			return -(myWidth()/*width*/ /2)+x;	
		}
		
//================================================================================================================
	}
}