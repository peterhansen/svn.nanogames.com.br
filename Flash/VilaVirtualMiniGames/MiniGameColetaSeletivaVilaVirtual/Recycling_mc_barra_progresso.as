package {	import flash.display.*;	import caurina.transitions.Tweener;	import flash.events.MouseEvent;	public class Recycling_mc_barra_progresso extends MovieClip 
		{//----------------------------------------------------------------------------------------------------------------
//      Funcoes
//----------------------------------------------------------------------------------------------------------------

	private var jogoRef:jogo;	private var largura_total:Number;	private var largura_atual:Number;	private var pontuacao_contabilizada:Number;
		//----------------------------------------------------------------------------------------------------------------
//      Funcoes
//----------------------------------------------------------------------------------------------------------------
//================================================================================================================
	public function Recycling_mc_barra_progresso() 
		{		//reinicia();		play();		}//================================================================================================================
	public function reinicia():void
		{		largura_total = borda.width;		barra.width = largura_atual = largura_total / 3;		pontuacao_contabilizada = 0;		}
//================================================================================================================	
	public function set pai(p_main):void 
		{		jogoRef = p_main;		}//================================================================================================================
	public function contaPonto(p_pontuacao, p_coeficiente_divisao):void
		{		largura_atual=largura_atual+((largura_total/p_coeficiente_divisao)*(p_pontuacao/100-pontuacao_contabilizada));		trace("=========largura atual da barra= "+largura_atual);		pontuacao_contabilizada = p_pontuacao/100;
				if (largura_atual <= 0) 
			{			largura_atual = 0;			jogoRef.terminaJogoPorPonto("perdeu");			}
					if (largura_atual >= largura_total) 
			{			largura_atual = largura_total;			jogoRef.terminaJogoPorPonto("ganhou");			}
					Tweener.addTween(barra,{width:largura_atual/*<DM>*/ -5 /*</DM>*/, time:1, transition:"easeOutCirc"});		}//================================================================================================================	public function diminuiPorTempo():void
		{		largura_atual--;		Tweener.addTween(barra,{width:largura_atual, time:1, transition:"easeOutCirc"});		}
//================================================================================================================
///DM: reduz para punir jogador que colocou lixo na lixeira errada	public function diminuiPorPunicao(Delta:int):void
		{
		trace("mc_barra_progresso::diminuiPorPunicao() >> diminuiu por"+ Delta.toString());		largura_atual-=Delta*jogoRef.getNivelAtual();		Tweener.addTween(barra,{width:largura_atual, time:1, transition:"easeOutCirc"});		}		//================================================================================================================	}}