package{		import flash.display.*;	import flash.display.Bitmap; 	import flash.display.BitmapData; 	import flash.geom.*;	import flash.utils.*; 	import caurina.transitions.Tweener;	import flash.events.*;		public class Recycling_lixo extends MovieClip
		{//----------------------------------------------------------------------------------------------------------------
//      Variaveis
//----------------------------------------------------------------------------------------------------------------
	///DM = garante que, enquanto pausado, ele nao desapareca, nem que seja manipulavel pelo jogador
	private var paused:Boolean;


	private var lista_lixeiras:Array;	private var jogoRef:jogo;	private var tipo:String;	private var id:uint;	private var lista_lixos_interna:Array;	private var xAnterior:Number;	private var yAnterior:Number;	private var tempo_validade;	private var lixo_ativado:Boolean;	//guarda se o objeto esta sendo arrastado	private var arrastando:Boolean;
	private var timeoutTimer:Timer;//----------------------------------------------------------------------------------------------------------------
//      Funcoes
//----------------------------------------------------------------------------------------------------------------
	public function togglePause():void
		{
		
		paused=!paused;
		if (timeoutTimer!=null)		
			{
			if (paused)	
				{
					timeoutTimer.stop();
				}
				else
				{
					timeoutTimer.start();
				}
			}
		}
//================================================================================================================
	public function Recycling_lixo(JogoRef:jogo, p_id:uint, p_tipo:Array, p_array_lixeiras:Array, p_tempo_validade:uint):void
		{					jogoRef=JogoRef;					arrastando=false;	
		paused=false;				tempo_validade=p_tempo_validade;							montaLixo(p_id, p_tipo, p_array_lixeiras);					associaAcoesDosBotoes();		}//================================================================================================================	private function montaLixo (p_identificador:uint, p_tipo:Array, p_array_lixeiras:Array):void
		{		id=p_identificador;		lista_lixos_interna=p_tipo;		//trace("+++++++++"+lista_lixos_interna);					tipo=p_tipo[1];					var temp=p_tipo[0];		//trace ("criou um lixo da classe= "+temp);		//trace("tipo= "+temp);		this.addChild(temp);		lista_lixeiras=p_array_lixeiras.slice();		//Redimensiona o lixo se ele for grane de mais		/*if(this.width>lista_lixeiras[0].width-15){			var proporcao=this.width/this.height;			this.width=lista_lixeiras[0].width-15;			this.height=this.width*proporcao;			trace("========Redimensionou!!!!!!");		}*/		}
//================================================================================================================
	public function aparece(p_habilita_arremesso:Boolean):void
		{		//ativa o lixo pra que ele seja válido para sumisso automático		lixo_ativado=true;		this.visible=true;
				if(p_habilita_arremesso)			arremeca();				}//================================================================================================================	public function some():void
		{		this.visible=false;		lixo_ativado=false;		}
//================================================================================================================
	public function isAtivado():Boolean
		{
		return lixo_ativado;	
		}//================================================================================================================	private function associaAcoesDosBotoes():void
		{		mouseChildren = false;		doubleClickEnabled=true;		this.addEventListener(MouseEvent.DOUBLE_CLICK, acaoSeparaObjeto);		//this.addEventListener(MouseEvent.MOUSE_DOWN, acaoSeparaObjeto);		//this.addEventListener(MouseEvent.MOUSE_UP, acaoSeparaObjeto);				this.addEventListener(MouseEvent.MOUSE_DOWN, acaoArrastaObjeto);		this.addEventListener(MouseEvent.MOUSE_UP, acaoSoltaObjeto);				}//================================================================================================================	private function acaoArrastaObjeto(p_evt:MouseEvent):void
		{
		if (paused) return;
		var objeto=p_evt.target;		arrastaLixo();		}//================================================================================================================
/// - objeto???	private function acaoSoltaObjeto(p_evt:MouseEvent):void
		{
		if (paused) return;			
		var objeto=p_evt.target;		soltaObjeto();		}//================================================================================================================
	private function acaoSeparaObjeto(p_evt:MouseEvent):void
		{
		if (paused) return;			trace("lixo::acaoSeparaObjeto() >>"+"SEPARA");		
		if(tipo!="multi")						jogoRef.puneLixoDesmembradoErrado();			else
			{			some();			//lista_lixos_interna.splice(0, 2);			trace ("lixo::acaoSeparaObjeto() >>"+"--------"+lista_lixos_interna);			jogoRef.separalixos(id, lista_lixos_interna.slice(2));			}		}//================================================================================================================	private function arrastaLixo():void
		{		xAnterior=this.x;		yAnterior=this.y;		this.startDrag();		this.addEventListener(Event.ENTER_FRAME, testaLixeiras);		this.arrastando=true;		}//================================================================================================================	private function soltaObjeto():void
		{		this.stopDrag();		this.removeEventListener(Event.ENTER_FRAME, testaLixeiras);
				if((this.y<jogoRef.altura_final_lixo_calculada-30) || (this.y>jogoRef.altura_tela-90))
			this.y=yAnterior;
		else
			verificaColisao();		trace("lixo::soltaObjeto() >>"+"largou");		this.arrastando=false;		jogoRef.soltaObjetosPorSeguranca();		}//================================================================================================================	public function soltaObjetoSeguranca():void
		{		if(arrastando)
			{				this.stopDrag();				this.removeEventListener(Event.ENTER_FRAME, testaLixeiras);				trace ("lixo::soltaObjetoSeguranca() >>"+"largou por seguranca");				this.arrastando=false;			}		}//================================================================================================================	private function testaLixeiras(p_evt:Event):void
		{		for (var i:uint=0; i<lista_lixeiras.length; i++)
			{				//if(this.hitTestObject(lista_lixeiras[i])){				if(hitTeste(lista_lixeiras[i]))
									lista_lixeiras[i].abre();				else					lista_lixeiras[i].fecha();			}		}//================================================================================================================	private function verificaColisao():Number
		{		for (var i:uint=0; i<lista_lixeiras.length; i++)
			{				//if(this.hitTestObject(lista_lixeiras[i])){			if(hitTeste(lista_lixeiras[i]))
				{				trace("lixo::verificaColisao() >>"+"colidiu e vai tratar");				jogoRef.trataColisao(i, tipo);				some();				return i;				}			}		return -1;		}//================================================================================================================
//menos um trace enchendo o saco
/// - porque nao hitTestObject?	private function hitTeste(p_objeto):Boolean
		{		if(   (jogoRef.mouseX>p_objeto.x)
			&&(jogoRef.mouseX<p_objeto.x+p_objeto.width)
			&&(jogoRef.mouseY>p_objeto.y)
			&&(jogoRef.mouseY<p_objeto.y+p_objeto.height)
			)
	
			{	///		trace("lixo::hitTeste() >>"+"objeto em colisao");			return true;			}			//trace("objeto fora de colisao");		return false;		}//================================================================================================================	private function arremeca():void
		{
		Sounds.playArremecoSound();		this.rotation=-40;		this.x=Math.random()*10000%(jogoRef.largura_tela-this.width);		this.y=jogoRef.altura_arremeco_calculada-this.height;		this.scaleX=this.scaleY=0.4;				var altura_bezier:uint=jogoRef.altura_arremeco_calculada+100;		var x_randomico:uint=150+(Math.random()*10000%(400));		var largura_bezier:uint=Math.abs(this.x-x_randomico);					var altura_final:uint=jogoRef.altura_final_lixo_calculada-this.height/2+(Math.random()*300%20);		var rotacao_temp:Number=Math.random()*90;					Tweener.addTween(this,{
							  y:altura_final, rotation:rotacao_temp,_bezier:{
																			x:largura_bezier, y:-100
					    													},
						  													time:1, transition:"easeOutInQuad"
							  }
						);
								Tweener.addTween(this,{
							  x:x_randomico, scaleY:1, scaleX:1, time:1, transition:"linear"
							  }
					    );						iniciaTimeout();		}
//================================================================================================================		public function iniciaTimeout():void
		{
		//	setTimeout(somePorTempo,tempo_validade*1000);				
		//DM
		timeoutTimer=new Timer(tempo_validade*1000,1);
		timeoutTimer.addEventListener(TimerEvent.TIMER,somePorTempo);
		timeoutTimer.start();		
		}
//================================================================================================================		public function somePorTempo(e:TimerEvent):void
		{		if(lixo_ativado)
			{			Tweener.addTween(this,{
								  alpha:0, time:3, onComplete:function()
								  								{																this.visible=false; 																jogoRef.contabilizaLixoSumiu();															    }
								   }
							 );			}		}//================================================================================================================	}}