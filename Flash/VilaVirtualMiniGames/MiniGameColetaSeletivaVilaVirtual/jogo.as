﻿package{
	
	import flash.display.*;
	import flash.display.Bitmap; 
	import flash.display.BitmapData;
	import flash.events.TimerEvent; 
	import flash.utils.*;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.geom.Utils3D;

	public class jogo extends MovieClip
		{
//----------------------------------------------------------------------------------------------------------------
//      Constantes
//----------------------------------------------------------------------------------------------------------------
	///1..9!!
	static public const NUMEROFASES:int=7;		
	//altura do arremeço do lixo
	static public const altura_arremeco:uint=290;
	//altura final do lixo
	static public const altura_final_lixo:uint=270;
	//altura das lixeiras
	static public const altura_lixeiras:uint=155;
	//altura em que o caminhão de coleta passa
	static public const altura_caminhao:uint=50;
	private var scoreCorreto:int;
	//public var notAccountedScore:int=0;

	//listar os tipos possíveis de acordo com a ordem de prioridade de aparecimento nos níveis
	/////////////////////////////////////////////////////////////////
	//                                       TipoLixeira | CorHexa  |
	/////////////////////////////////////////////////////////////////
	private var tipos_lixo:Array=new Array(	
		["papel"    , "0x004CBC"]
		,["plastico" , "0xff0000"]
		,["vidro"    , "0x009900"]
		,["metal"    , "0xFF9800"]
		,["organico" , "0x653D00"]
		,["pilha"    , "0x793976"]
		);			
			 


	//listar ordem intervalos do arremesso dos lixos em cada fase
	private const ordem_intervalos_arremessos:Array=new Array(3, 2.9, 2.8, 2.7, 2.6, 2.5, 2.4, 2.3, 2.2, 2.1);
	
	//tempo que cada lixo fica na tela - a posição do valor no vetor equivale a fase
	private const tempo_validade_lixo:Array=new Array(8,8,8,8,8,8,8,7,7,6);
	
	//listar pontuação dos combos de pontuação
	//private const ordem_pontuacao:Array=new Array(0,1,3,5,8,11,15,18,25);
		
//----------------------------------------------------------------------------------------------------------------
//      Variaveis
//----------------------------------------------------------------------------------------------------------------
	///DM: qual e' o numero da fase que aparece ao jogador, mesmo que internamente seja sempre a 10a fase.
	private var nivelInterface:int;
	//DM:um contador global local. nao sei porque, mas pontuacao nao colabora, zerando o contador de pontos toda hora.	
	private var playerGlobalScore:int;
	// DM: controla se o jogo e' ilimitado ou nao
	private var	storyMode:Boolean=true;
	// DM: mantem o estado do jogo (rodando/pausado)
	private var paused:Boolean;
	// DM: controla se o jogador pode ou nao pausar o jogo
	private var allowedToPause:Boolean;

	private var jogoIniciado:Boolean=false;
	private var vetor_tipo_lixeiras_temp:Array;
	private var caminhao:Recycling_coleta;
	private var controlador_intervalos_lixo_aparece:Timer;
	private var numero_lixo_colocar_tela:uint;
	private var nivel_atual:Number=0;
	private var nivel_insercao_lixos:uint;
	//guarda a pontuacao que será somada a total na proxima coleta
	private var total_lixos_certos_temporario:Number;
	//guarda a total de pontos de punição por lixo entregue errado
	//private var total_punicao_lixo_errado:Number;
	private var total_lixo_errado_temporario:Number;
	private var _pai:SceneRecycling;
	private var janela_avisos;
	private var lista_lixos_ordenados:Array=new Array();
	//guarda o indice maximo pra cada tipo de lixo
	private var indices_lixos:Array=new Array();
	//quantidade de lixeiras por fase
	private var lixeiras_por_fase:Array=new Array(2,2,3,4,5,6,6,6,6,6);
	//fase a partir da qual os lixos compostos começam a aparecer
	private var tipo_adimite_lixo_composto:uint=6;
	//lista de lixos de 1 PARTE
	private var lista_lixos_1_partes:Array=new Array();
	//lista de lixos de 2 PARTES
	private var lista_lixos_2_partes:Array=new Array();
		
	private var checkerAndSubmitterCallback:Function;
	private var gameStateNotifierCallback:Function;
	private var sairCallback:Function;
	

	public var lista_lixeiras:Array;
	public var lista_lixos:Array;

		static public const GAMESTATE_INIT_OK:int=0;
		static public const GAMESTATE_PAUSED:int=1;
		static public const GAMESTATE_RUNNING:int=2;
		static public const GAMESTATE_GAMEOVER:int=3;
		static public const GAMESTATE_WIN:int=4;		
		static public const MAIORPONTUACAOPOSSIVEL:int=999999999;
	
	private var janelaPause:Recycling_JanelaPause;
	
	//Características que mudarão em cada fase:
	//-Velocidade de arremeço dos lixos;
	//-Tipos de lixo (vidro, plástico, metal...)
	//-Complexidade dos lixos
	//-Quantidade de pontos dos lixos
	//-Velocidade da coleta
	
//----------------------------------------------------------------------------------------------------------------
//      Funcoes
//----------------------------------------------------------------------------------------------------------------


		public function isStoryMode():Boolean
		{
			return storyMode;
		}
		
		public function setSairCallback(Func:Function):void
		{
			sairCallback=(Func);
		}
		
		public function setCheckAndSubmitRecordsCallback(Func:Function):void
		{
			checkerAndSubmitterCallback=(Func);
		}
		
		public function getGlobalScore():int
		{
			if (playerGlobalScore > MAIORPONTUACAOPOSSIVEL)
				playerGlobalScore = MAIORPONTUACAOPOSSIVEL;

			return playerGlobalScore;
		}
		
		public function getScore():int
		{
			
			playerGlobalScore+=quadro_pontos.getPontosFase(nivel_atual)/*+quadro_pontos.getBonusFase(getNivelAtual())*/;	

			
			if (playerGlobalScore > MAIORPONTUACAOPOSSIVEL)
				playerGlobalScore = MAIORPONTUACAOPOSSIVEL;

			return playerGlobalScore;
		}
			
		
		public function setGameStateNotifierCallback(f:Function):void
		{
			gameStateNotifierCallback=(f);
		}
		
		public function sair():void
			{
			if (sairCallback!=null)
				sairCallback();
				
				_pai.sair();
			}


////////Funcoes de inicializacao//////////////////////////////////////////////////////////////////////////////////

//================================================================================================================
	private function montaListaLixosUmaParte():void
		{
			
		//tipos possíveis de lixo: papel, vidro, metal, plastico, organico e perigoso
		////////////////////////////////////////////////////////////////////
		//                                 NomeLixo            | TipoLixo  |
		////////////////////////////////////////////////////////////////////
		lista_lixos_1_partes[0]=new Array("metal_atum"         , "metal"   );
		lista_lixos_1_partes[1]=new Array("metal_cano"         , "metal"   );
		lista_lixos_1_partes[2]=new Array("metal_coca"         , "metal"   );
		lista_lixos_1_partes[3]=new Array("metal_oleo"         , "metal"   );
		lista_lixos_1_partes[4]=new Array("metal_prego"        , "metal"   );
		lista_lixos_1_partes[5]=new Array("organico_banana"    , "organico");
		lista_lixos_1_partes[6]=new Array("organico_folha"     , "organico");
		lista_lixos_1_partes[7]=new Array("organico_maca"      , "organico");
		lista_lixos_1_partes[8]=new Array("organico_peixe"     , "organico");
		lista_lixos_1_partes[9]=new Array("organico_pernil"    , "organico");
		lista_lixos_1_partes[10]=new Array("papel_desenho"     , "papel"   );
		lista_lixos_1_partes[11]=new Array("papel_envelope"    , "papel"   );
		lista_lixos_1_partes[12]=new Array("papel_jornal"      , "papel"   );
		lista_lixos_1_partes[13]=new Array("papel_papelao"     , "papel"   );
		lista_lixos_1_partes[14]=new Array("papel_revista"     , "papel"   );
		lista_lixos_1_partes[15]=new Array("pilha_1"           , "pilha"   );
		lista_lixos_1_partes[16]=new Array("pilha_2"           , "pilha"   );
		lista_lixos_1_partes[17]=new Array("pilha_3"           , "pilha"   );
		lista_lixos_1_partes[18]=new Array("pilha_4"		   , "pilha"   );
		lista_lixos_1_partes[19]=new Array("plastico_biscoito" , "plastico");
		lista_lixos_1_partes[20]=new Array("plastico_canudo"   , "plastico");
		lista_lixos_1_partes[21]=new Array("papel_copo"        , "plastico");
		lista_lixos_1_partes[22]=new Array("plastico_pet"      , "plastico");
		lista_lixos_1_partes[23]=new Array("plastico_sacola"   , "plastico");
		lista_lixos_1_partes[24]=new Array("plastico_sorvete"  , "plastico");
		lista_lixos_1_partes[25]=new Array("vidro_casco"       , "vidro"   );
		lista_lixos_1_partes[26]=new Array("vidro_casco_2"     , "vidro"   );
		lista_lixos_1_partes[27]=new Array("vidro_copo"        , "vidro"   );
		lista_lixos_1_partes[28]=new Array("vidro_copo_2"      , "vidro"   );
		lista_lixos_1_partes[29]=new Array("vidro_vinho"       , "vidro"   );
	
	
	
		///////////////////////////////////////////////////////////////////////////////////////////////
		//NomeLixoComposto  |tipo    | NomeSimples1        | Tipo1   |  NomeSimples2         | Tipo2  |
		///////////////////////////////////////////////////////////////////////////////////////////////
		lista_lixos_2_partes[0]=new Array(
  		  "composto_iogurte", "multi", "metal_tampa_iogurte", "metal","plastico_iogurte_pote", "plastico");
		
		lista_lixos_2_partes[1]=new Array(
		  "composto_coca"   , "multi", "metal_coca"         , "metal","plastico_canudo"      , "plastico");
		
		lista_lixos_2_partes[2]=new Array(
		  "composto_copo"   , "multi", "vidro_copo_2"       , "vidro","plastico_canudo"      , "plastico");
		
		lista_lixos_2_partes[3]=new Array(
		  "composto_geleia"  , "multi", "vidro_geleia"      , "vidro","metal_tampa_geleia"   , "metal"   );
		}
		
//================================================================================================================
	public function togglePause():void
		{
			
			if (!allowedToPause)
				return;
			
			paused=!paused;
			if (this.paused)
			{
				trace ("pausou");
				controlador_intervalos_lixo_aparece.stop();	
				Sounds.stopMusic();				
				pessoa1.visible=false;
				pessoa2.visible=false;
				pessoa3.visible=false;
				pessoa4.visible=false;									
				pessoa5.visible=false;	
				pessoa6.visible=false;
				pessoa7.visible=false;					
				pessoa1.stop();
				pessoa2.stop();
				pessoa3.stop();
				pessoa4.stop();
				pessoa5.stop();
				pessoa6.stop();
				pessoa7.stop();
				if (gameStateNotifierCallback!=null)
					gameStateNotifierCallback.call(null,GAMESTATE_PAUSED);
				janelaPause.startShowAnimation();
				this.setChildIndex(janelaPause,this.numChildren-1);				
			}
			else
			{
				trace ("despausou");				
				Sounds.playMusic();				
				controlador_intervalos_lixo_aparece.start();				
				pessoa1.play();
				pessoa2.play();
				pessoa3.play();
				pessoa4.play();
				pessoa5.play();
				pessoa6.play();
				pessoa7.play();				
				pessoa1.visible=true;				
				pessoa2.visible=true;
				pessoa3.visible=true;
				pessoa4.visible=true;									
				pessoa5.visible=true;	
				pessoa6.visible=true;
				pessoa7.visible=true;					
				if (gameStateNotifierCallback!=null)
					gameStateNotifierCallback.call(null,GAMESTATE_RUNNING);
				janelaPause.startHideAnimation();
			}

			barra_coleta.togglePause();
			cronometro.togglePause();
			for (var c:int=0;c<lista_lixos.length;c++)			
				if (Recycling_lixo(lista_lixos[c]).isAtivado())			
				{
					Recycling_lixo(lista_lixos[c]).visible=!paused;	
					Recycling_lixo(lista_lixos[c]).togglePause();
				}

		}		
//================================================================================================================
	public function iniciaProximoNivel():void
		{
			//notAccountedScore=0;
			paused=false;
			allowedToPause=true;
			total_lixo_errado_temporario=0;
			barra_progresso.reinicia();
			limpaLixosTela();
			limpaLixeirasTela();
			nivel_atual++;
			nivelInterface++;
			trace("quem e' meu pai agora?"+_pai.toString());
			
			if (nivel_atual>NUMEROFASES)
				nivel_atual=NUMEROFASES;
			
			trace("jogo::iniciaProximoNivel() >>"+"iniciou nivel "+nivel_atual);
			montaLixeirasFase(nivel_atual);
			iniciaLixos();	
			contaTempoAparreceLixo(ordem_intervalos_arremessos[nivel_atual]);
			//cronometro.reinicia();	
			//inicia a pontuacao acumulada temporaria
			total_lixos_certos_temporario=total_lixo_errado_temporario=0;
			quadro_pontos.inicia();
			cronometro.reseta();
			barra_coleta.reinicia();
			jogoIniciado=true;
			cronometro.reiniciaContador();
			
			if (gameStateNotifierCallback!=null)
					gameStateNotifierCallback.call(null,GAMESTATE_RUNNING);

		}
//================================================================================================================
	public function setJanelaAvisosHighScore():void
		{
		trace("get scores");
		playerGlobalScore=getGlobalScore();
		
		
		var rtn:Boolean=false;			
		if (checkerAndSubmitterCallback!=null)
			rtn=checkerAndSubmitterCallback.call(null,playerGlobalScore);
			
			
		scoreCorreto=playerGlobalScore;
		if (janela_avisos!=null && playerGlobalScore.toString()!="NaN")
			janela_avisos.setScoreAndHighScore("Total de Pontos: "+scoreCorreto.toString(),rtn);			
		}
		
	public function terminaJogoPorPonto(p_evento:String):void
		{
			
		paraJogo();
		switch(p_evento)
			{
			case "ganhou":
			{
				contabilizaBonusFase();
				
				if(!(storyMode && nivel_atual==NUMEROFASES))
				{				
				if (quadro_pontos !=null )		
					playerGlobalScore+=quadro_pontos.getPontosFase(nivel_atual)
									  +quadro_pontos.getBonusFase(getNivelAtual());	
				}
				
				if (playerGlobalScore > MAIORPONTUACAOPOSSIVEL)
					playerGlobalScore = MAIORPONTUACAOPOSSIVEL;
			
				if (janela_avisos!=null)
					janela_avisos.setGlobalScore(playerGlobalScore);
				
				Sounds.stopMusic();
				Sounds.playLevelUpSound();				
				
				////DM: o jogo e' infinito
				if(storyMode && nivel_atual==NUMEROFASES)
				{
				
					if (gameStateNotifierCallback!=null)
						gameStateNotifierCallback.call(null,GAMESTATE_WIN);
					trace("fim de jogo com score:" + getScore());
					janela_avisos.abre("jogo_completo");
				}
				else
				{				
					allowedToPause=false;
					janela_avisos.abre("venceu_fase");
					
				if (gameStateNotifierCallback!=null)
					gameStateNotifierCallback.call(null,GAMESTATE_PAUSED);
					
				}
			}
			break;			
			case "tempo_acabou":
			case "perdeu":
			{
				//contabilizaPontosColeta();
						
				if (gameStateNotifierCallback!=null)
					gameStateNotifierCallback.call(null,GAMESTATE_GAMEOVER);
						
				

				
				if (playerGlobalScore > MAIORPONTUACAOPOSSIVEL)
					playerGlobalScore = MAIORPONTUACAOPOSSIVEL;
		
					
				Sounds.stopMusic();				
				Sounds.playGameOverSound();
				janela_avisos.abre("tempo_acabou");		
				
			}
			break;			
			default:			
			{
			}
			break;
			}			
		}
//================================================================================================================
	public function paraJogo():void
		{
		controlador_intervalos_lixo_aparece.stop();
		cronometro.para();
		barra_coleta.para();
		cronometro.apagaContador();
		trace("jogo::paraJogo() >>"+"chamou fução parar jogo");
		}
//================================================================================================================
/**
- de onde vem numero_lixo_colocar_tela?
- nao tem tratamento de excessao
*/
	public function limpaLixosTela():void
		{
		for(var i:uint=0; i<numero_lixo_colocar_tela; i++)
			{
			this.removeChild(lista_lixos[i]);
			trace("jogo::limpaLixosTela() >>"+"tirou da tela o objeto "+i);
			}
		}
//================================================================================================================
	public function limpaLixeirasTela():void
		{
		if(jogoIniciado)
			{
			for(var i:uint=0; i<lista_lixeiras.length; i++)
				{
				this.removeChild(lista_lixeiras[i]);
				//trace("tirou da tela o objeto "+i);
				}
			}
		}
//================================================================================================================
	public function reiniciaJogoZero():void
		{
		//notAccountedScore=0;
		playerGlobalScore=0;
		trace ("jogo::reiniciaJogoZero() >>"+"chamou reinicia");
		nivel_atual=0;
		nivelInterface=0;
		iniciaProximoNivel();
		///<DM comment="aparentemente isso ja e' chamado na funcao logo acima - investigar">
		quadro_pontos.inicia();
		///</DM>
		}
//================================================================================================================

	
	
	public function jogo():void
		{
			
		////////////DM////////////////////////////////
		janelaPause=new Recycling_JanelaPause(); 	//
		janelaPause.visible=false;	   				//
		janelaPause.x=stage.stageWidth/2;			//
		janelaPause.y=stage.stageHeight/2;			//			
		addChild(janelaPause);         				//
		//////////////////////////////////////////////

		
		
		///na inicializacao, o jogo faz nivel_atual++	
		
		nivel_atual=0;				
		playerGlobalScore=0;
		nivelInterface=nivel_atual;
		
		montaListaLixosUmaParte();		
		arrumaVetorLixos();
		
		cronometro.pai=this;
		barra_progresso.pai=this;
		barra_coleta.pai=this;
		quadro_pontos.inicia();
		}
//================================================================================================================
	private function arrumaVetorLixos():void
		{
		trace("arruma vetor lixos");
		
		for(var i:uint=0; i<tipos_lixo.length; i++)
			{
				
			if(i==tipo_adimite_lixo_composto-1)
				{
				
				for(var k:uint=0; k<lista_lixos_2_partes.length; k++)
					{
						lista_lixos_ordenados[lista_lixos_ordenados.length]=lista_lixos_2_partes[k].concat();
						indices_lixos[i]=lista_lixos_ordenados.length;
					}
					trace("jogo::arrumaVetorLixos() >>"+"inseriu os MULTI");
				}
				
			for(var j:uint=0; j<lista_lixos_1_partes.length; j++)
				{						
  				if( lista_lixos_1_partes[j][1] == tipos_lixo[i][0] )
					{
					lista_lixos_ordenados[lista_lixos_ordenados.length]=new Array(lista_lixos_1_partes[j][0],lista_lixos_1_partes[j][1]);
					indices_lixos[i]=lista_lixos_ordenados.length;
					}
				}				
			}
	
		}
//================================================================================================================
	private function iniciaLixos():void
		{
		lista_lixos=new Array();
		
		trace("jogo::iniciaLixos() >>"+"nivel atual= "+nivel_atual);
		trace("jogo::iniciaLixos() >>"+"lixeiras por fase nesta fase= "+(lixeiras_por_fase[nivel_atual-1]));
		trace("jogo::iniciaLixos() >>"+"indice= "+indices_lixos[lixeiras_por_fase[nivel_atual-1]-1]);
		
		for(var i:uint=0; i<=99; i++)
			{
			//escolhe randomicamente um TIPO DE LIXO pra ser colocado na tela
			var numero_randomico:uint=
			Math.round(Math.random() * 100 )% (indices_lixos[lixeiras_por_fase[nivel_atual-1]-1]);
			//trace("numero randomico= "+numero_randomico);
			//trace("numero de lixeiras disponiveis= "+lixeiras_por_fase[nivel_atual]);
			//cria uma variavel pra guardar a referencia pra definição desse lixo
			var copia_referencia_definicao_lixo_temp:Array=new Array();
			//guarda a definição do lixo escolhido randomicamente
			copia_referencia_definicao_lixo_temp=lista_lixos_ordenados[numero_randomico].concat();
			//trace("----tipo randomico= "+lista_lixos_ordenados[numero_randomico]);
			
			//substitui as STRING's de nome do objeto visual pelo objeto visual correspondente
			for(var k:uint=0; k<copia_referencia_definicao_lixo_temp.length; k=k+2)
				{
				//transforma a String em um interpretação como CLASSE e cria um novo objeto dessa classe
				//Substitui o nome pelo objeto
				copia_referencia_definicao_lixo_temp[k]=
					criaObjetoDeClasse(copia_referencia_definicao_lixo_temp[k]);
				//trace("---********-tipo randomico= "+copia_referencia_definicao_lixo_temp[k]);
				}				
			//var vetor_lixo_temp:Array=new Array(objeto_visual_temp,lista_lixos_1_partes[1]);;
			criaLixo(i, copia_referencia_definicao_lixo_temp, lista_lixeiras);
			}
		numero_lixo_colocar_tela=0;
		nivel_insercao_lixos=numChildren;
		colocaLixoTela(lista_lixos[numero_lixo_colocar_tela], nivel_insercao_lixos,true);
		}
//================================================================================================================
	private function criaObjetoDeClasse(p_nome_classe:String):Object
		{
		//trace("recebeu na função o tipo= "+p_nome_classe);
		var referencia_classe:Class= getDefinitionByName(p_nome_classe) as Class;
		var objeto_temp:Object=new referencia_classe();//new referencia_classe();
		return objeto_temp;
		}
//================================================================================================================
	private function criaLixo(p_id:uint, p_referencia_definicao_lixo:Array, p_lista_lixeiras,
							  p_inclui_na_lista:Boolean=true):Recycling_lixo
		{
		var lixo_temp=new Recycling_lixo(this, p_id, p_referencia_definicao_lixo, p_lista_lixeiras,
							   tempo_validade_lixo[nivel_atual]);
								   
		if(p_inclui_na_lista)				
			lista_lixos[p_id]=lixo_temp;
		else
			trace("jogo::criaLixo() >>"+"NAO colocou o lixo na lista");
		
		return lixo_temp;
		}
//================================================================================================================
	public function colocaLixoTela(p_objeto_inserir, p_depth:Number, p_automatico:Boolean, p_x:Number=0,
								   p_y:Number=0):void
		{
		this.addChildAt(p_objeto_inserir,p_depth);
			
		//addChild(p_objeto_inserir);
		p_objeto_inserir.aparece(p_automatico);
		if(p_automatico)
			numero_lixo_colocar_tela++;
		else
			{
				p_objeto_inserir.x=p_x;
				p_objeto_inserir.y=p_y;
				var rotacao_temp:Number=Math.random()*60;
				p_objeto_inserir.rotation=rotacao_temp;
			}
			
		}
//================================================================================================================
///DM: porque em criaLixo, e' usado false e nao true?

	public function separalixos(p_id_excluir:uint, p_lista_lixos_novos:Array):void
		{
		var x_temp:Number=lista_lixos[p_id_excluir].x;
		var y_temp:Number=lista_lixos[p_id_excluir].y;
		trace ("jogo::separalixos() >>"+"entrou na função do main de separar lixo= "+p_lista_lixos_novos);
		for(var i:uint=0; i<p_lista_lixos_novos.length; i=i+2)
			{
			trace(p_lista_lixos_novos[i+1]);			
			var lixo_temp=criaLixo(lista_lixos.length, [p_lista_lixos_novos[i],p_lista_lixos_novos[i+1]],
								   lista_lixeiras, /*false*/ true);
								   
			colocaLixoTela(lixo_temp, nivel_insercao_lixos, false, x_temp, y_temp);
			lixo_temp.iniciaTimeout();
			x_temp+=lixo_temp.width+5;
			}
		}
		
//================================================================================================================
//função pra montar as lixeiras pedidas
	public function montaLixeiras(p_vetor_lixeiras:Array):void
		{
		lista_lixeiras=new Array();
		//trace("tamanho do vetor de lixeiras ANTES da criação das lixeiras = "+lista_lixeiras.length);
		for(var i:uint=0; i<p_vetor_lixeiras.length; i++)
			{
			//var lixeira_temp=new lixeira;
			lista_lixeiras[i]=new lixeira(this,p_vetor_lixeiras[i][0],p_vetor_lixeiras[i][1]);
			addChild(lista_lixeiras[i]);
			//lista_lixeiras[i].x=100+(largura_tela-200)/p_vetor_lixeiras.length*i;
			lista_lixeiras[i].x=
				60+((largura_tela-120)/(p_vetor_lixeiras.length+1))*(i+1)-lista_lixeiras[i].width/2;
			lista_lixeiras[i].y=altura_tela-altura_lixeiras;				
			}
			//trace("tamanho do vetor de lixeiras DEPOIS da criação das lixeiras = "+lista_lixeiras.length);
		}
//================================================================================================================
	private function montaLixeirasFase(p_fase:Number):void
		{
		montaLixeiras(tipos_lixo.slice(0,lixeiras_por_fase[p_fase-1]));
		}
//================================================================================================================
	public function fazColeta():void
		{
		barra_coleta.fazColeta();
		}
//================================================================================================================
/// - tirar essa funcao anonima...isso deixa o codigo pouco claro - alem de tirar o controle sobre o que esta
/// acontecendo
	private function contaTempoAparreceLixo(p_intervalo:uint):void
		{
		controlador_intervalos_lixo_aparece = new Timer(p_intervalo*1000, 60);
		controlador_intervalos_lixo_aparece.addEventListener(TimerEvent.TIMER, 
														 function(e:TimerEvent)
															{
															colocaLixoTela(lista_lixos[numero_lixo_colocar_tela], 
											   							   nivel_insercao_lixos, true);
															}
														);
		controlador_intervalos_lixo_aparece.start();
		//controlador_intervalos=new setInterval(p_funcao, p_intervalo*1000);
		}
//================================================================================================================
	public function contabilizaPontosColeta():void
		{

		var pontos_temp:Number=quadro_pontos.contabilizaPonto(total_lixos_certos_temporario,
															  total_lixo_errado_temporario, nivel_atual);
			
		if (pontos_temp>0)
				Sounds.playPontuacaoSound();						
		else
				Sounds.playAcaoErradaSound();			
				
		//--------R:SUBSTITUIR SEGUNDO PARAMETRO POR COEFICIENTE PARA DIVISAO (dificuldade do jogo)
		barra_progresso.contaPonto(quadro_pontos.getPontosFase(nivel_atual), 2*(NUMEROFASES+1)+2*getNivelAtual());
		barra_coleta.mostraPontos(pontos_temp);
		//notAccountedScore+=pontos_temp;
		total_lixos_certos_temporario=/*total_lixo_errado_temporario=*/0;
		trace("jogo::contabilizaPontosColeta() >>"+"zerou lixos certos");
		}
//================================================================================================================
	public function contabilizaLixoSumiu():void
		{
		total_lixo_errado_temporario++;
		///DM: infelizmente nao pode, porque o lixo da fase anterior pode punir o jogador sem que ele entenda
		///porque. Se fosse visivel, ainda seria razoavel considerar isso. Mas nao e'.
		
		//barra_progresso.diminuiPorPunicao(total_lixo_errado_temporario);		
		}
//================================================================================================================
	public function contabilizaBonusFase():void
		{
		quadro_pontos.contabilizaBonus(cronometro.tempo_restante, nivel_atual);
		}
//================================================================================================================
	public function trataColisao(p_numero_lixeira:uint, p_tipo_lixo:String):void
		{
		trace(lista_lixeiras[p_numero_lixeira].nome);
		
		if(lista_lixeiras[p_numero_lixeira].trataColisao(p_tipo_lixo)>0)
				total_lixos_certos_temporario++;
		else
				puneLixoEnlatadoErrado();				
		}
//================================================================================================================
	public function puneLixoDesmembradoErrado():void
		{
		puneLixoEnlatadoErrado();
		}
//===============================================================================================================
	public function puneLixoEnlatadoErrado():void
		{
		Sounds.playAcaoErradaSound();
		//total_punicao_lixo_errado+=ordem_punicao[nivel_atual];
		total_lixo_errado_temporario++;
		barra_progresso.diminuiPorPunicao(total_lixo_errado_temporario);
		trace("jogo::puneLixoEnlatadoErrado() >>"+"lixos enlatados errados = "+total_lixo_errado_temporario);
		}
//================================================================================================================
	public function decrementaTempo():void
		{
		barra_progresso.diminuiPorTempo();
		}
//================================================================================================================
	public function soltaObjetosPorSeguranca():void
		{
		for(var i:uint=0; i<lista_lixos.length; i++)
			lista_lixos[i].soltaObjetoSeguranca();
		}
//================================================================================================================
	public function setIsStoryMode(IsStoryMode:Boolean):void
		{
			storyMode=IsStoryMode;
		}		
//================================================================================================================
//GETS E SETS
//================================================================================================================
	public function get largura_tela():uint
		{
		return stage.stageWidth;
		}
//================================================================================================================
	public function get altura_tela():uint
		{
		return stage.stageHeight;
		}
//================================================================================================================
	public function get altura_arremeco_calculada():uint
		{
		return altura_tela-altura_arremeco;
		}
//================================================================================================================
	public function get altura_final_lixo_calculada():uint
		{
		return altura_tela-altura_final_lixo;
		}
//================================================================================================================
	public function set pai(p_main:SceneRecycling):void
		{
		_pai=p_main;
		if (_pai!=null && _pai.janela_avisos!=null)
			janela_avisos=_pai.janela_avisos;
		}
//================================================================================================================
//DM: fazendo a experiencia...nao sei o que mais depende do numero de fases. 	
	public function getNivelAtual():Number
		{
	//	return nivel_atual;
		return nivelInterface;
		}
//================================================================================================================
	}
}