package  {		import flash.display.MovieClip;	import flash.display.*;	import flash.display.Bitmap; 	import flash.display.BitmapData;	import flash.events.TimerEvent; 	import flash.utils.*;	import flash.events.MouseEvent;	import flash.events.KeyboardEvent;	import flash.events.Event;		public class SceneRecycling extends MovieClip {							
		private	var loadNotifier:Function;
		private var sairFunc:Function;
			/*
		public function gameNotify(i:int):void
		{
			trace("***CALLBACK*** game notified me of this:"+i.toString());
		}

		public function nanoOnline(i:int):Boolean
		{
			trace("***CALLBACK*** nano online:"+i.toString());
			return true;
		}
		
		public function quitter():void
		{
			trace("***CALLBACK*** quitting!");
		}
		*/
						
			//----------------------------------------------------------------------------------------------------------------//      Variaveis//----------------------------------------------------------------------------------------------------------------//////////variaveis criadas em timeline (incluidas aqui apenas por motivos de clareza)////////////////////////////////	private var mc_jogo:jogo;//	  private var _bt_jogar:bt_jogar;// (vai entender?! - por acaso, nao tem uma classe especifica, e' um botao)	  private var _janela_avisos:Recycling_mc_janela_avisos;////----------------------------------------------------------------------------------------------------------------//      Funcoes//----------------------------------------------------------------------------------------------------------------//================================================================================================================	private function acaoBtJogar(p_evt:MouseEvent):void		{			
		trace("bt jogar");
		Sounds.playInteracaoSound();								play();
		}
		
	private function acaoBtSairIntro(p_evt:MouseEvent):void
		{
			trace("SceneRecycling::acaoBtSairIntro() >> saindo!");
			Sounds.playInteracaoSound();
			if (sairFunc!=null)
				sairFunc();
		}
		
		
	private function finishedLoadingGame():void
		{
			trace("finished loading the game");			
			if (loadNotifier!=null)
				loadNotifier.call(null,this);			
		}
		
	public function get janela_avisos():Recycling_mc_janela_avisos
		{
			return _janela_avisos;	
		}
		
	public function set janela_avisos(j:Recycling_mc_janela_avisos):void
		{
			_janela_avisos=j;
		}
			private function keyboardPause(e:KeyboardEvent):void	{	var char:String=String.fromCharCode(e.charCode);	if (char==' '  || char=='p' || char=='P' )		getJogo().togglePause();	}
		
		//================================================================================================================	///lembrando que a cena associada a essa classe ja tem play automatico em seu timeline. Portanto, enquanto isso e'///executado, a interface vai ser exibida ou ja estara' visivel. 	public function SceneRecyling()		{
			stop();			
		}
		
	public function setNotifier(f:Function):void
		{
			loadNotifier=f;
		}
						
	public function run(notifier:Function):void 	
		{			stage.addEventListener(KeyboardEvent.KEY_DOWN,keyboardPause);
			setNotifier(notifier);
			play();
		}//================================================================================================================
	public function setIsStoryMode(IsStoryMode:Boolean):void		{
			mc_jogo.setIsStoryMode(IsStoryMode);			}//================================================================================================================	public function iniciaProximoNivel():void		{			mc_jogo.iniciaProximoNivel();		}//================================================================================================================	public function reiniciaJogoZero():void		{		trace("main::reiniciaJogoZero() >>-chegou no main pra chamar reinicia");		mc_jogo.reiniciaJogoZero();		}//================================================================================================================	public function get nivel():Number		{		return mc_jogo.getNivelAtual();		}//================================================================================================================	public function getJogo():jogo		{		return mc_jogo;			}//================================================================================================================// so' o poder do bacalhau pra resolver outro bacalhau	public function updateLevelSign():void		{		if (janela_avisos!=null)			{			janela_avisos.posicionaQuadroInicioFase();				}		}
		
		
		public function setSairCallback(Func:Function):void
		{
			getJogo().setSairCallback(Func);
			sairFunc=Func;
		}
		
		public function btSairJogoClick()
		{
			Sounds.playInteracaoSound();			
			if (sairFunc!=null)	
				sairFunc();
		}				public function sair():void		{			stage.removeEventListener(KeyboardEvent.KEY_DOWN,keyboardPause);		}		
		
		public function setCheckAndSubmitRecordsCallback(Func:Function):void
		{
			getJogo().setCheckAndSubmitRecordsCallback(Func);
		}
		
		public function getScore():int
		{
			return getJogo().getGlobalScore();
		}
			
		
		public function setGameStateNotifierCallback(f:Function):void
		{
			getJogo().setGameStateNotifierCallback(f);
		}
			}	}