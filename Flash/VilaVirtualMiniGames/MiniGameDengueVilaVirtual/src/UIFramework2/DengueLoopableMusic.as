package UIFramework2
{
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	
	public class DengueLoopableMusic
	{
		private var soundChannel:SoundChannel;
		private var sample:Sound; 
		private var loop:Boolean;		
		
		public function DengueLoopableMusic(Sample:Sound)
		{
			sample=Sample;
			soundChannel=null;
			loop=false;
		}
		
		public function play():void
		{
			playLoop();
			loop=false;
		}
		
		public function stop():void
		{
			if (soundChannel!=null)
			{
				soundChannel.stop();
				soundChannel.removeEventListener(Event.SOUND_COMPLETE, loopMusic);
			}
		}
		
		
		public function playLoop():void
		{
			try
			{
			soundChannel = sample.play();
			soundChannel.addEventListener(Event.SOUND_COMPLETE, loopMusic);
			loop=true;
			}
			catch (e:Error)				
			{
			loop=false;
			}
		}

		private function loopMusic(e:Event):void
		{
			if (soundChannel != null)
			{
				soundChannel.removeEventListener(Event.SOUND_COMPLETE, loopMusic);
				if (loop)
					playLoop();
			}
		}		
	}
}
