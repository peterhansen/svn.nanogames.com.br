package UIFramework2
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.MouseEvent;

	public class DengueNormalGroupSprite extends DengueFadableSprite
	{
		public function DengueNormalGroupSprite()
		{
			super();
		}

		public function addMovieClip(mc:MovieClip): DisplayObject
		{
			addChild(mc);
			//mc.width=100;
			//mc.height=100;
			return mc;
		}

		public function bringElementToFront(obj:DisplayObject):void
		{
			this.setChildIndex(obj,this.numChildren-1);
		}
		
		public function hideAllPanes():void
		{
			for (var c:int=0;c<this.numChildren-1;c++)
				if (getChildAt(c) is DengueNormalGroupSprite)
					getChildAt(c).visible=false;
		}
		
		public function showContent():void
		{
			for (var c:int=0;c<this.numChildren-1;c++)
			{
					getChildAt(c).visible=true;
					if (getChildAt(c) is DengueFadableSprite)
						DengueFadableSprite(getChildAt(c)).startShowAnimation();
			}
		}
		
		public function showAllPanes():void
		{
			for (var c:int=0;c<this.numChildren-1;c++)
				if (getChildAt(c) is DengueNormalGroupSprite)
					getChildAt(c).visible=true;
		}
		
		/*
		public function getNormalBounds():Shape
		{
			var shape:Shape=new Shape();
			var obj:DisplayObject;
			for (var c:int=0;c<this.numChildren-1;c++)
			{
				obj=this.getChildAt(c);
				
				if (shape.x>obj.x)
					shape.x=obj.x;
				
				if (shape.y>obj.y)
					shape.y=obj.y;

				if (shape.x+shape.width<obj.x+obj.width)
					shape.width=(obj.x+obj.width)-shape.x;

				if (shape.y+shape.height<obj.y+obj.height)
					shape.height=(obj.y+obj.height)-shape.y;

			}
			return shape;
		}
		*/
		
	}
}