package UIFramework2
{
	
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import Dengue.DengueUtils;
	
	public class DengueFadableSprite extends DengueNormalSprite
	{
		private var timer:Timer;
		private var alphaFinal:Number;
		private var tickStep:Number;
		private var onShow:Function;
		private var onHide:Function;
///----------------------------------------------------------------------------------------------------------
		public function DengueFadableSprite()
		{
			super();
			timer=new Timer(DengueUtils.FADE_INTERVAL);
			timer.addEventListener(TimerEvent.TIMER,this.tickAnim);
			onShow=null;
			onHide=null;
		}
		
		public function setOnShow(OnShow:Function):void
		{
			onShow=OnShow;
		}
		
		public function setOnHide(OnHide:Function):void
		{
			onHide=OnHide;
		}
		
///----------------------------------------------------------------------------------------------------------		
		public function startShowAnimation(OnShow:Function=null):void		
		{			
			this.visible=true;
			this.alpha=0;
			alphaFinal=1;
			tickStep=alphaFinal/DengueUtils.FADE_FRAMES;
			timer.start();
			
			if (OnShow!=null)
				onShow=OnShow;
		}
///----------------------------------------------------------------------------------------------------------		
		public function startHideAnimation(OnHide:Function=null):void
		{		
			this.alpha=1;
			alphaFinal=1;
			tickStep=-alphaFinal/DengueUtils.FADE_FRAMES;
			alphaFinal=0;
			timer.start();
			
			if (OnHide!=null)
				onHide=OnHide;
		}
///----------------------------------------------------------------------------------------------------------		
		public function stopAllAnimations():void
		{
			timer.stop();
			this.alpha=1;
		}
///----------------------------------------------------------------------------------------------------------
		public function setVisible():void
		{
			stopAllAnimations();
			this.visible=true;
		}
///----------------------------------------------------------------------------------------------------------
		public function setinvisible():void
		{
			timer.stop();
			this.alpha=1;
			this.visible=false;
		}
///----------------------------------------------------------------------------------------------------------				
		private function tickAnim(e:TimerEvent):void
		{
			var tmp:Number=this.alpha+tickStep;
			
			if (tmp<1.0 && tmp>0.0)			
				this.alpha=tmp;
			else
			{
				timer.stop();
				if (tickStep>0)
				{
					this.alpha=1.0;
					if (onShow!=null)
						onShow();
				}
				else
					this.alpha=0.0;
			}
			
			if (this.alpha==0)
			{
				this.visible=false;
				this.alpha=1;
				if (onHide!=null)
					onHide();
			}
		}
///----------------------------------------------------------------------------------------------------------		
	}
}