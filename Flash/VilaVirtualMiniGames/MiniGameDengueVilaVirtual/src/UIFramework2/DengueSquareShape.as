package UIFramework2
{
	import flash.display.Shape;
	import flash.display.Sprite;

	public class DengueSquareShape extends DengueFadableSprite
	{
		private var s:Shape;
		private var color:uint;
		public function DengueSquareShape(Color:uint=0xFF0000)
		{
			s=new Shape();			
			addChild(s);
			color=Color;
		}
		
		public function recalcBody():void			
		{
			s.graphics.clear();
			s.graphics.beginFill(color);
			s.graphics.drawRect(x,y,width,height);
			s.graphics.endFill();
		}
	}
}