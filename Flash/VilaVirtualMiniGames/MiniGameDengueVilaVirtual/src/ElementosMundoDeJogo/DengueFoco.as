package ElementosMundoDeJogo
{
	import Dengue.DengueUtils;
	
	import UIFramework2.DengueFeedbackButtom;
	
	import flash.display.MovieClip;
	import flash.display.Shader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	public class DengueFoco extends DengueFeedbackButtom//Sprite
	{
		private var focoAtivo:Boolean;
		private var mosquitosPousados:int;
		private var body:MovieClip;		
		private var fumaca:FUMACA;
		private var fumacaTimer:Timer;
		private var limpezaSound:DesinfeccaoSound;
		private var infecaoSound:FocoInfectadoSound;
///----------------------------------------------------------------------------------------------------------
		public function getBody():MovieClip
		{
			return body;
		}
///----------------------------------------------------------------------------------------------------------
		public function endFumacaAnimation(e:TimerEvent):void
		{
			fumacaTimer.stop();			
			fumaca.stop();
			fumaca.visible=false;
		}

///----------------------------------------------------------------------------------------------------------
		
		private function stub():void
		{
		
		}
		
		public function DengueFoco()
		{
			super(null,false,false);
			super.setOnMouseDownCallback(stub);
			this.mouseChildren=false;
			var s:Shape=new Shape();
		//	this.addChild(s);
			
			infecaoSound=new FocoInfectadoSound();
			limpezaSound=new DesinfeccaoSound();
			focoAtivo=false;
			mosquitosPousados=0;		
			fumacaTimer=new Timer(DengueUtils.FUMACA_ANIMATION_INTERVAL);
			fumacaTimer.addEventListener(TimerEvent.TIMER,endFumacaAnimation);
			var rnd:int=Math.random()*6;
			switch(rnd)
			{
				case 0:
					body=(new engradado());
					break;
				case 1:
					body=(new garrafa());
					break;
				case 2:
					body=(new sacolixo());
					break;
				case 3:					
					body=(new pneu());
					break;					
				case 4:
					body=(new tonel());
					break;
				case 5:
					body=(new vaso());
					break;
			}			
			addChild(body);
			
			
			
			fumaca=new FUMACA();	
			fumaca.scaleX=0.25;
			fumaca.scaleY=0.25;
			addChild(fumaca);
			
		//	fumaca.x+=fumaca.width/4;
		//	fumaca.y+=fumaca.height/4;			
		//	fumaca.scaleY=(90/480)*(Utils.STAGE_SIZE_Y/fumaca.height);
		//	fumaca.scaleX=(105/640)*(Utils.STAGE_SIZE_X/fumaca.width)
		//	fumaca.scaleX=0.2;
		//	fumaca.scaleY=0.2;
			fumaca.visible=false;
			//this.x=0;
			//this.y=0;
			///<ajuste manual - feio, mas necessario>////
			/*
			switch(rnd)
			{
			case 1:
				body.x-=2*body.width;
				body.y+=body.height/10;
				break;
			case 2:
			case 3:
				body.x-=body.width/2;
				body.y-=body.height/2;				
				break;			
			case 4:
				body.x-=body.width/3.5;
				body.y-=body.height/8;				
				break;
			}*/
			
			
			/////////</ajuste manual>/////////////////////
			/*
			graphics.clear();
			graphics.beginFill(0xFFFFFF*Math.random());
			graphics.drawRect(0,0,width,height);
			graphics.endFill();
			*/

			
			s.graphics.beginFill(0xFFFFFF*Math.random());
			s.graphics.drawRect(body.x,body.y,body.width,body.height);
			s.graphics.endFill();
			
			body.x+=(body.width)/2;
			body.y+=(body.height)/2;			


			fumaca.x+=(fumaca.width)/2;
			fumaca.y+=(fumaca.height)/2;			
			
			
		}
///----------------------------------------------------------------------------------------------------------		
		public function setAtivo(Ativo:Boolean):void
		{
			focoAtivo=Ativo;
			visible=Ativo;
		}
///----------------------------------------------------------------------------------------------------------		
		public function getAtivo():Boolean
		{
			return focoAtivo;
		}
///----------------------------------------------------------------------------------------------------------		
		public function addMosquitoPousado():void
		{
			mosquitosPousados++;
			update();
			
			if (infecaoSound == null) 
				return;
			
			try 
			{
				infecaoSound.play();
			}
			catch (e:Error)
			{
			
			}
		}
///----------------------------------------------------------------------------------------------------------		
		public function update():void
		{
			if (mosquitosPousados+1 < 4)
				body.gotoAndStop(mosquitosPousados+1);
		}
///----------------------------------------------------------------------------------------------------------		
		public function getMosquitosPousados():int
		{
			return mosquitosPousados;
		}
///----------------------------------------------------------------------------------------------------------		
		public function reset():void
		{
			mosquitosPousados=0;
			update();	
		}
///----------------------------------------------------------------------------------------------------------		
		public function  makeupStartPosition(area:flash.geom.Rectangle):void
		{
			this.x=area.x+ (area.width)-width/2;
			this.y=area.y-(area.height/2)-height/2;
			///coloca um ruido
			
		//	this.x+=Math.random()*area.width/2
	//		this.y+=Math.random()*area.height/2
			
		}
///----------------------------------------------------------------------------------------------------------
		public function activateSmoke():void
		{		
			fumaca.visible=true;			
			fumaca.gotoAndPlay(1);
			fumacaTimer.start();
			
			if (limpezaSound == null)
				return;
			
			try 
			{
				limpezaSound.play();
			}
			catch (e:Error)
			{
				
			}
		}
///----------------------------------------------------------------------------------------------------------		
	}
}