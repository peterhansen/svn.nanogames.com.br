package ElementosMundoDeJogo
{
	import Dengue.DengueUtils;
	
	import UIFramework2.*;
	
	import com.greensock.*;
	import com.greensock.data.TweenMaxVars;
	import com.greensock.easing.*;
	import com.greensock.events.TweenEvent;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.system.System;
	import flash.text.TextField;
	import flash.utils.Timer;

	
	public class DengueMosquito extends DengueNormalSprite
	{
		private var focoAlvo:DengueFoco;
		private var alive:Boolean;

		private var mos:mosquitoanimation;
		private var raio:mosquito_raio;
		private var tween:TweenMax;
		private var dyingTimer:Timer;		
		private var raioTimer:Timer;
///----------------------------------------------------------------------------------------------------------		
		public function DengueMosquito()
		{
			focoAlvo=null;
			
			mos=new mosquitoanimation();
			raio=new mosquito_raio();
			addChild(mos);			
			addChild(raio);
			mos.x+=mos.width/2;
			mos.y+=mos.height/2;
			raio.x+=raio.width/2;
			raio.y+=raio.height/2;
			
			this.width=mos.width;
			this.height=mos.height;
			/*
			this.scaleY=(20/480)*(Utils.STAGE_SIZE_Y/this.height);
			this.scaleX=(23/640)*(Utils.STAGE_SIZE_X/this.width)
			*/	
			reset();
			dyingTimer=new Timer(DengueUtils.MOSQUITO_RAIO_TIME);
			dyingTimer.addEventListener(TimerEvent.TIMER,dyingTimerTick);
			
			/*
			graphics.beginFill(0x0F0F0F);
			graphics.drawRect(0,0,width,height);
			graphics.endFill();
			*/
		}
///----------------------------------------------------------------------------------------------------------	
		public function reset():void
		{
			mos.visible=true;
			raio.visible=false;
			alive=true;
			mos.gotoAndPlay("Flying");
			this.alpha=1;
		}
///----------------------------------------------------------------------------------------------------------		
		public function dyingTimerTick(e:TimerEvent):void
		{			
			if (this.alpha==1)			
				mos.gotoAndStop("Fading");
			
			this.alpha-=0.01;
			
			if (alpha<0.6 && alpha>0.2)
			{
				mos.visible=true;
				raio.visible=false;
				this.y+=2;
			}
			
			if (alpha<0.2)
			{
				mos.visible=true;
				raio.visible=false;
				visible=false;
				dyingTimer.stop();				
				tween.pause();
			}
		}
///----------------------------------------------------------------------------------------------------------			
		public function isAlive():Boolean
		{
			return alive;
		}
///----------------------------------------------------------------------------------------------------------		
		public function setAlive(Alive:Boolean=true):void
		{			
			alive=Alive;				
		}
///----------------------------------------------------------------------------------------------------------		
		public function isHit(raquete:flash.display.DisplayObject):Boolean
		{
			if (!visible || alpha<1)
				return false;
			
			///hit test
			alive= !(raquete.hitTestObject(this));
			
				
			if (!alive)
			{
				setAlive(false);
				alpha=1;
				tween.pause();								
				dyingTimer.start();
				mos.visible=false;
				raio.visible=true;
				raio.gotoAndPlay(0);
			}
			return !alive;
		}
///----------------------------------------------------------------------------------------------------------		
		public function setPousado():void
		{
			visible=false;
			tween.pause();
			alive=false;
		}
///----------------------------------------------------------------------------------------------------------			
		public function getFoco():DengueFoco
		{
			return focoAlvo;
		}
///----------------------------------------------------------------------------------------------------------		
		public function setFoco(novoAlvo:DengueFoco):void
		{
			focoAlvo=novoAlvo;
		}		
///----------------------------------------------------------------------------------------------------------		
		public function  makeupTurnPoints(n:int, tempo:int):void
		{
			
			var points:Array;
			var _x:int;
			var _y:int;
			
			points=new Array(DengueUtils.MAX_POINTS_MOSQUITO_PATH);
//////////////////////pontos intermediarios////////////////////////		
			for (var c:int=0;c<n;c++)
			{
				
				_x=Math.random()*(DengueUtils.STAGE_SIZE_X-0.05*DengueUtils.STAGE_SIZE_X);
				_y=Math.random()*(DengueUtils.STAGE_SIZE_Y-0.05*DengueUtils.STAGE_SIZE_Y-focoAlvo.y);
				
				points.push({x:_x,y:_y});
				
			}
			
//////////////////////ponto final///////////////////////////////////			
			_x=focoAlvo.x+(focoAlvo.width/6);
			_y=focoAlvo.y+(focoAlvo.height/4);
			points.push({x:_x,y:_y});				
/////////////////////tween//////////////////////////////////////////
			var bezier:Object=new Object();
			bezier.bezier=points;			
			tween=new TweenMax(this,tempo,bezier);
			tween.pause();
			
		}
///----------------------------------------------------------------------------------------------------------		
		public function getTween():TweenMax
		{
			return tween;
		}
///----------------------------------------------------------------------------------------------------------		
		public function voa():void
		{
			try
			{
			tween.play();
			}
			catch (e:Error)
			{
			
			}
		}
///----------------------------------------------------------------------------------------------------------		
		public function  makeupStartPosition():void
		{
			this.x=Math.random()*(DengueUtils.STAGE_SIZE_X-0.05*DengueUtils.STAGE_SIZE_Y);
			this.y=-5*this.height+Math.random()*(-5*this.height);			
		}
///----------------------------------------------------------------------------------------------------------
	}
}

