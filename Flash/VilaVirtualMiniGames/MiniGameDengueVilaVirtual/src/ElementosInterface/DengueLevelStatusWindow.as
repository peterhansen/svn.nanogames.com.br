package ElementosInterface
{
	import UIFramework2.*;
	import UIFramework2.DengueShadowTextField;
	import UIFramework2.DengueWindowSprite;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import Dengue.DengueUtils;
	
	
	public class DengueLevelStatusWindow extends DengueWindowSprite
	{
		private var fundo:mc_janela_avisos;
		private var btok:DengueFeedbackButtom;
		private var currentPane:int;
		
		private var antiMosquito:DengueNormalSprite;
		private var boxSplash_mc:BoxDescription;
		private var boxSplash:DengueNormalSprite;		
		private var splashText:DengueShadowTextField;
		private var btIniciar:DengueFeedbackButtom;
		private var btSair:DengueFeedbackButtom;		
		private var onIniciarCallback:Function;
	

		private var venceuText:DengueShadowTextField;
		private var highScoreText:DengueShadowTextField;
		private var onVenceuCallback:Function;
		private var btRecords:DengueFeedbackButtom;
		private var onSairCallback:Function;
		
		
		
		public function DengueLevelStatusWindow(Fundo:DisplayObject=null)
		{
			fundo=new mc_janela_avisos();
			fundo.gotoAndStop(2);
			super(fundo);		
			
			x=0;
			y=0;
			width=1*DengueUtils.STAGE_SIZE_X;
			height=1*DengueUtils.STAGE_SIZE_Y;			
			fundo.x+=fundo.width/2;
			fundo.y+=fundo.height/2;			
			
			
			/////////////configuracao das subviews///////////////
			var panel1:DengueNormalGroupSprite=new DengueNormalGroupSprite();
			var panel2:DengueNormalGroupSprite=new DengueNormalGroupSprite();	
			var dengueFont:AddervilleBook=new AddervilleBook();
			this.addPanel(panel1);
			this.addPanel(panel2);
			panel1.x=0;
			panel1.y=0;
			////////////////////////////////////////////////////
			
			//////view 1: inicio de jogo
			{
				
				var antiMosquito_mc:limpafoco=new limpafoco();
				antiMosquito=new DengueNormalSprite();
				antiMosquito.addChild(antiMosquito_mc);
				//antiMosquito.height=140;
				//antiMosquito.width=140;
				antiMosquito.x=(DengueUtils.STAGE_SIZE_X-antiMosquito_mc.width)/2;
				antiMosquito_mc.x-=antiMosquito_mc.width/2;
				antiMosquito.y=45;				
				antiMosquito_mc.scaleX=2;
				antiMosquito_mc.scaleY=2;
				getPane(DengueOrangeWindowViewIds.INTRO).addChild(antiMosquito);
				//getDefaultPane().addChild(antiMosquito);
				
///////////////////				
				splashText=new DengueShadowTextField(dengueFont,0,2,34,0xFFFFFF,0xFF6600,false);
				splashText.setText(DengueUtils.MSG_SPLASH);								
				splashText.x=DengueUtils.STAGE_SIZE_X/2;				
				splashText.y=antiMosquito.y+antiMosquito.height+DengueUtils.DEFAULT_VERTICAL_SPACING;
				getPane(DengueOrangeWindowViewIds.INTRO).addChild(splashText);
/////////////////////			
				
				var btIniciar_mc:PlayButton=new PlayButton();
				btIniciar=new DengueFeedbackButtom();
				btIniciar.addChild(btIniciar_mc);				
				btIniciar.x=DengueUtils.STAGE_SIZE_X/2;
				btIniciar.y=385+btIniciar_mc.height/2;
				btIniciar.setOnMouseClickCallback(onIniciar);				
				getPane(DengueOrangeWindowViewIds.INTRO).addChild(btIniciar);				
				

				
				var btSair_mc:bt_sair_x=new bt_sair_x();
				btSair=new DengueFeedbackButtom();
				btSair.addChild(btSair_mc);				
				btSair.x=DengueUtils.STAGE_SIZE_X-(DengueUtils.DEFAULT_HORIZONTAL_SPACING*0.8)-btSair.width/2;
				btSair.y=(DengueUtils.DEFAULT_VERTICAL_SPACING*0.8)+btSair.height/2;
				btSair.setOnMouseClickCallback(this.onSair);				
				//getPane(OrangeWindowViewIds.INTRO).addChild(btSair);
				getDefaultPane().addChild(btSair);
				btSair_mc.scaleX=0.8;
				btSair_mc.scaleY=0.8;
				
				boxSplash_mc=new BoxDescription();
				boxSplash_mc.gotoAndStop(15);				
				boxSplash=new DengueNormalSprite();
				boxSplash.x=90;
				boxSplash.y=245;
				getPane(DengueOrangeWindowViewIds.INTRO).addChild(boxSplash);				
				boxSplash.addChild(boxSplash_mc);
				boxSplash.width=465;
				boxSplash.height=130;
				boxSplash_mc.x+=boxSplash_mc.width/2;
				boxSplash_mc.y+=boxSplash_mc.height/2;
			}
			//////view 2: venceu o jogo
			{		
				var antiMosquito2_mc:limpafoco=new limpafoco();
				antiMosquito=new DengueNormalSprite();
				antiMosquito.addChild(antiMosquito2_mc);
				//antiMosquito.height=140;
				//antiMosquito.width=140;
				antiMosquito.x=(DengueUtils.STAGE_SIZE_X-antiMosquito2_mc.width)/2;
				antiMosquito2_mc.x-=antiMosquito2_mc.width/2;
				antiMosquito.y=50;				
				antiMosquito2_mc.scaleX=2;
				antiMosquito2_mc.scaleY=2;				
				getPane(DengueOrangeWindowViewIds.FINISHED).addChild(antiMosquito);
				
				///////////////////				
				venceuText=new DengueShadowTextField(dengueFont,0,2,50,0xFFFFFF,0xFF6600,false);
				venceuText.setText(DengueUtils.MSG_WIN);								
				venceuText.x=DengueUtils.STAGE_SIZE_X/2;				
				venceuText.y=3*DengueUtils.STAGE_SIZE_Y/5;
				getPane(DengueOrangeWindowViewIds.FINISHED).addChild(venceuText);
				/////////////////////
				
				///////////////////				
				
				highScoreText=new DengueShadowTextField(dengueFont,0,2,30,0xFFFFFF,0xFF6600,false);
				highScoreText.setText(DengueUtils.MSG_HIGHSCORE);
				highScoreText.x=DengueUtils.STAGE_SIZE_X/2;				
				highScoreText.y=4*DengueUtils.STAGE_SIZE_Y/5;
				getPane(DengueOrangeWindowViewIds.FINISHED).addChild(highScoreText);
				/////////////////////			
				
				/*
				var btRecords_mc:BtRecords=new BtRecords();
				btRecords=new FeedbackButtom();
				btRecords.addChild(btRecords_mc);				
				btRecords.x=Utils.STAGE_SIZE_X/2;
				btRecords.y=355+btIniciar_mc.height/2;
				btRecords.setOnMouseClickCallback(onVenceu);				
				getPane(OrangeWindowViewIds.FINISHED).addChild(btRecords);				
				*/
				
				/*
				var btSair_mc:bt_sair_x=new bt_sair_x();
				btSair=new FeedbackButtom();
				btSair.addChild(btSair_mc);				
				btSair.x=Utils.STAGE_SIZE_X-(Utils.DEFAULT_HORIZONTAL_SPACING*0.8)-btSair.width/2;
				btSair.y=(Utils.DEFAULT_VERTICAL_SPACING*0.8)+btSair.height/2;
				btSair.setOnMouseClickCallback(sair);				
				getPane(OrangeWindowViewIds.FINISHED).addChild(btSair);
				btSair_mc.scaleX=0.8;
				btSair_mc.scaleY=0.8;
				
				boxSplash_mc=new BoxDescription();
				boxSplash_mc.gotoAndStop(15);				
				boxSplash=new NormalSprite();
				boxSplash.x=90;
				boxSplash.y=245;
				getPane(OrangeWindowViewIds.FINISHED).addChild(boxSplash);			
				boxSplash.addChild(boxSplash_mc);
				boxSplash.width=465;
				boxSplash.height=130;
				boxSplash_mc.x+=boxSplash_mc.width/2;
				boxSplash_mc.y+=boxSplash_mc.height/2;
				*/
			}			
			setView(DengueOrangeWindowViewIds.INTRO);
			
		}
		
		
		override public function startHideAnimation(OnHide:Function=null):void
		{
			boxSplash_mc.gotoAndPlay("ExitBox");
			super.startHideAnimation(OnHide);
		}
		
		override public function startShowAnimation(OnShow:Function=null):void
		{
			boxSplash_mc.gotoAndPlay("EntryBox");
			super.startShowAnimation(OnShow);		
		}
		
		
		public function setVenceuCallback(func:Function):void
		{
			this.onVenceuCallback=func;
		}
		
		private function onVenceu():void
		{
			if (this.onVenceuCallback!=null)
				onVenceuCallback();
		}
		
		
		private function onIniciar():void
		{
			startHideAnimation();
			if (onIniciarCallback!=null)
				onIniciarCallback();
		}
		
		
		public function setIniciarCallback(func:Function):void
		{
			onIniciarCallback=func;
		}
		
		private function onSair():void
		{
		//	startHideAnimation();
			if (onSairCallback!=null)
				onSairCallback();
		}
		
		
		public function setOnSairCallback(func:Function):void
		{
			onSairCallback=func;
		}
		
		
		
		public function setView(viewId:int):void
		{
			currentPane=viewId;
			getPane(DengueOrangeWindowViewIds.INTRO).visible=(viewId==DengueOrangeWindowViewIds.INTRO);
			getPane(DengueOrangeWindowViewIds.FINISHED).visible=(viewId==DengueOrangeWindowViewIds.FINISHED);
			getDefaultPane().showContent();
			getPane(viewId).showContent();
			
		}

		
		public function setIsHighScore(val:Boolean):void
		{
			this.highScoreText.visible=val;
		}
	}
}