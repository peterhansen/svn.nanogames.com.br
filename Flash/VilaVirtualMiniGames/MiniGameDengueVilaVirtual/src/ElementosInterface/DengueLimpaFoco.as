package ElementosInterface
{
	
	import UIFramework2.*;
	import UIFramework2.DengueFadableSprite;
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import Dengue.DengueUtils;
	
	public class DengueLimpaFoco extends DengueFeedbackButtom
	{
		private var state:Boolean;
		private var text:TextField;
		private var remaining:int;
		private var limpaFoco:limpafoco;
		private var font:AddervilleHeavy;
		private var s:Shape;
		
		public function DengueLimpaFoco()
		{
			super(null,true);
			font=new AddervilleHeavy();
			var textFormat:TextFormat;
			text=new TextField;				
			text.alwaysShowSelection=false;
			text.mouseEnabled=false;
			text.mouseWheelEnabled=false;
			text.selectable=false;
			text.tabEnabled=false;
			textFormat=new TextFormat();
			textFormat.font=font.fontName;
			textFormat.size=26;
			text.embedFonts=true;
			textFormat.color=0xFFFFFF;
			text.defaultTextFormat     = textFormat;
			text.setTextFormat(textFormat);			
			addChild(text);
			remaining=DengueUtils.MAX_CLEAN_FOCUS;
			update();
			
			limpaFoco=new limpafoco();	
			
			s=new Shape();
			s.alpha=0.001;
			s.graphics.beginFill(0xFF6D00);
			s.graphics.drawRect(0,0,100,100);
			s.graphics.endFill();			
			addChild(s);s			
			
			addChild(limpaFoco);
			//limpaFoco.x= (width-limpaFoco.width)/2;
			//limpaFoco.y= (height-limpaFoco.height)/2;
			text.textColor=0xFF9900;			
			setActive(false);
			
			/*
			this.width=0.17*Utils.STAGE_SIZE_X;
			this.height=0.20*Utils.STAGE_SIZE_Y;
			*/			
			
			//this.x=0.10*Utils.STAGE_SIZE_X;//Utils.DEFAULT_HORIZONTAL_SPACING;
			//this.y=0.90*Utils.STAGE_SIZE_Y;//Utils.STAGE_SIZE_Y-limpaFoco.height-Utils.DEFAULT_VERTICAL_SPACING;
			update();
		}
		
		
		public function update():void
		{
			if (limpaFoco!=null)
			{
				text.text="x"+remaining.toString();
				text.x=limpaFoco.x+limpaFoco.width+8;
				text.y=35;
				//text.width=text.textWidth;
				//text.height=text.textHeight;
			}
		}
		
		public function setActive(active:Boolean):void
		{
			state=active;
		}
		
		public function useObject():void
		{
			if (remaining>0)
				remaining--;
			update();			
		}
		
		public function getRemaining():int
		{
			return remaining;
		}
		
		
		public function getState():Boolean
		{
			return state;
		}
		
		public function setMaxActivations(activations:int):void
		{
			remaining=activations;
			update();
		}
		
	}
}