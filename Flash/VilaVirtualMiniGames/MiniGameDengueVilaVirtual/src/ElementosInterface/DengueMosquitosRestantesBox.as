

package ElementosInterface
{
	
	import UIFramework2.*;
	
	import flash.display.Sprite;
	import Dengue.DengueUtils;
	
	public class DengueMosquitosRestantesBox extends DengueFadableSprite
	{
		private var mosquitosFaltando:int;
		private var mosquitosTotal:int;
		private var textMessage:DengueShadowTextField;
		
		public function DengueMosquitosRestantesBox()
		{
			super();
			
			var indicador:MosquitosNumbers=new MosquitosNumbers();		
			addChild(indicador);
			
			
			mosquitosFaltando=mosquitosTotal=DengueUtils.MAX_MOSQUITOS;
			textMessage=new DengueShadowTextField(new Font1(),2,2,/*22*/34,0xFFFFFF,0xFF6D00,false,false);
			addChild(textMessage);
			//um peteleco
			textMessage.y--;
			textMessage.x-=3;
			
			
			x=DengueUtils.STAGE_SIZE_X-(95/2)-10;
			y=(55/2)+(10);
			width=95;
			height=55;	
		}
		
		public function setMosquitosValues(faltando:int,total:int,adjustBox:Boolean=false):void
		{
			mosquitosTotal=total;
			mosquitosFaltando=faltando;
			textMessage.setText(DengueUtils.MSG_MOSQUITOS+getTextWithDigits(mosquitosFaltando)+"/"+getTextWithDigits(mosquitosTotal));
		}
		
		public function getTextWithDigits(x:int):String
		{
			var tmp:String="";
			var tmp2:String=x.toString();
			for (var c:int=0;c<DengueUtils.DEFAULT_MOSQUITOBOX_DIGITS-tmp2.length;c++)
				tmp+="0";
			
			tmp+=tmp2;		
			
			if (tmp.length>DengueUtils.DEFAULT_MOSQUITOBOX_DIGITS)
			{
				tmp="";
				for (var d:int=0;d<DengueUtils.DEFAULT_MOSQUITOBOX_DIGITS;d++)
					tmp+="9";
			}
			return tmp;
		}
	}
}