package Dengue
{		
	import ElementosInterface.DengueLimpaFoco;
	
	import ElementosMundoDeJogo.DengueJogo;
	
	import UIFramework2.*;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	//[SWF( width= "640", height = "480", backgroundColor = "0x000000", frameRate = "20" )]	
	public class Dengue extends MovieClip
	{
		private var fase:ElementosMundoDeJogo.DengueJogo;
		private var sairFunc:Function;
		///---------------------------------------------------------------------------------------------------------------------------	
		public function Dengue()
		{
			super();
			fase=new DengueJogo();
			addChild(fase);		
			this.addEventListener(Event.ADDED_TO_STAGE,addKbdHandler);		
	//		run(loadNotify);
		}
		///---------------------------------------------------------------------------------------------------------------------------
		private function loadNotify(dengueRef:Dengue):void
		{
			//dengueRef.visible=false;
			dengueRef.setIsStoryMode(true);
			dengueRef.setCheckAndSubmitRecordsCallback(nanoOnline);
		}
		///---------------------------------------------------------------------------------------------------------------------------	
		private function nanoOnline(i:int):Boolean
		{			
			return false;
		}
		///---------------------------------------------------------------------------------------------------------------------------		
		private function addKbdHandler(e:Event):void
		{
			stage.addEventListener(KeyboardEvent.KEY_DOWN,togglePause);
		}
		///---------------------------------------------------------------------------------------------------------------------------
		public function run(f:Function):void
		{
			if (f!=null)
				f.call(null,this);
			
			fase.run();
		}
		///---------------------------------------------------------------------------------------------------------------------------
		public function setIsStoryMode(s:Boolean):void
		{
			fase.setStoryMode(s);
		}
		///---------------------------------------------------------------------------------------------------------------------------
		private function togglePause(e:KeyboardEvent):void
		{
			var char:String=String.fromCharCode(e.charCode);
			if ((char==DengueUtils.PAUSE_KEY1 || char==DengueUtils.PAUSE_KEY2 || char==DengueUtils.PAUSE_KEY3 ) && !fase.isPaused())
				fase.togglePause();
		}
		///---------------------------------------------------------------------------------------------------------------------------
		public function setSairCallback(Func:Function):void
		{
			sairFunc=Func;
			fase.setSairFunction(doQuit);
		}
		///---------------------------------------------------------------------------------------------------------------------------		
		private function doQuit():void
		{
			fase.stopAllSounds();
			stage.removeEventListener(KeyboardEvent.KEY_DOWN,togglePause);
			if (sairFunc!=null)
				sairFunc();
		}
		///---------------------------------------------------------------------------------------------------------------------------
		public function setCheckAndSubmitRecordsCallback(Func:Function):void
		{
			fase.setScoreCheckerCallback(Func);
		}
		///---------------------------------------------------------------------------------------------------------------------------		
		public function getScore():int
		{
			return fase.getScore();
		}
		///---------------------------------------------------------------------------------------------------------------------------	
		public function setGameStateNotifier(f:Function):void
		{
			fase.setGameStateNotifier(f);
		}
		///---------------------------------------------------------------------------------------------------------------------------
		public function startNewGame():void
		{
			fase.startNewGame();
		}
		///---------------------------------------------------------------------------------------------------------------------------
	}
}