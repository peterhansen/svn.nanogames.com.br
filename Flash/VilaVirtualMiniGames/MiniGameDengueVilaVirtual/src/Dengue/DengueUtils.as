// ActionScript file
package Dengue 
{
	import ElementosInterface.DengueDicaTextoImagem;
	
	public class DengueUtils
	{
		///espaco pausa o jogo
		static public const PAUSE_KEY1:String=" ";
		static public const PAUSE_KEY2:String="p";
		static public const PAUSE_KEY3:String="P";
		static public const MILISECOND:int=1;
		static public const MILISECONDS:int=MILISECOND;
		static public const SECOND:int=1000*MILISECONDS;
		static public const SECONDS:int=SECOND;
		static public const MIN_TIME_MOSQUITO_PATH:int=8;
		static public const MAX_TIME_MOSQUITO_PATH:int=7;
		static public const MIN_POINTS_MOSQUITO_PATH:int=8;
		static public const MAX_POINTS_MOSQUITO_PATH:int=10;
		static public const MAX_FOCUS_AREAS:int=6;
		static public const MIN_FOCUS_AREAS:int=2;
		static public const MAX_MOSQUITOS:int=25;
		static public const MIN_MOSQUITOS:int=10;
		static public const MAX_LEVEL_DIFFICULTY:int=6;
		static public const MAX_ENERGIA:int=100;
		static public const MIN_ENERGIA:int=0;
		static public const MAX_MOSQUITOS_UNTIL_TAINTED_FOCUS:int=3;		
		static public const MAX_CLEAN_FOCUS:int=4;

		//static public const STAGE_SIZE_X:int=500;
		//static public const STAGE_SIZE_Y:int=375;
		static public const STAGE_SIZE_X:int=640;
		static public const STAGE_SIZE_Y:int=480;

		
		static public const PLANE_BACKGROUND:int=0;
		static public const PLANE_FOCUS:int=1;
		static public const PLANE_MOSQUITOS:int=2;
		static public const PLANE_RACKET:int=3;
		static public const PLANE_UI:int=4;
		static public const DESIRED_FPS:int=20;
		static public const TICK_INTERVAL:int=1*SECOND/DESIRED_FPS;
		static public const MOSQUITO_RELEASE_INTERVAL:int=1*SECOND;
		static public const MOSQUITO_RELEASE_MIN_TIME:int=0.1*SECOND;
		static public const MOSQUITO_RELEASE_MAX_TIME:int=1.5*SECONDS;
		static public const MOSQUITO_RAIO_TIME:int=10*MILISECONDS;
		static public const BOX_ANIMATION_INTERVAL:Number=5*SECONDS;
		static public const FADE_INTERVAL:Number=20*MILISECONDS;
		static public const FADE_FRAMES:int=16;
		static public const RAQUET_RELOAD_TIME:int=4;
		static public const RAQUET_UNLOAD_TIME:int=8;
		static public const RAQUET_PENALTY_TIME:int=3*SECONDS;
		static public const FUMACA_ANIMATION_INTERVAL:int=700*MILISECONDS;
		static public const FUMACA_ANIMATION_FRAMES:int=17;
		static public const BOX_MESSAGE_DROP_SHADOW:int=1;
		static public const MSG_PTBR_NEW_GAME_STARTING:String="Novo jogo!";
		static public const MSG_PTBR_MOSQUITOS:String="";
		static public const MSG_PTBR_GAMEOVER:String="Fim de jogo!";
		
		
		static public const MSG_WIN:String="Você venceu!";
		
		static public const MSG_PTBR_NEXT_LEVEL:String="Novo nível!\n";
		static public const MSG_NEW_GAME_STARTING:String=MSG_PTBR_NEW_GAME_STARTING;
		static public const MSG_MOSQUITOS:String=MSG_PTBR_MOSQUITOS;
		static public const MSG_GAMEOVER:String=MSG_PTBR_GAMEOVER;
		static public const MSG_PTBR_PAUSE:String="Pausado";
		static public const MSG_PAUSE:String=MSG_PTBR_PAUSE;
		static public const MSG_NEXT_LEVEL:String=MSG_PTBR_NEXT_LEVEL;
		static public const DEFAULT_HORIZONTAL_SPACING:int=10;
		static public const DEFAULT_VERTICAL_SPACING:int=10;
		static public const DEFAULT_SCOREBOX_DIGITS:int=15;
		static public const DEFAULT_MOSQUITOBOX_DIGITS:int=2;
		static public const MULTIPLIER_COMBO:int=2;
		static public const BASE_SCORE_MULTIPLIER:int=100;
		static public const MSG_SPLASH:String="Combate à dengue";
		static public const MSG_HIGHSCORE:String="Novo recorde!";

		static public const MSG_PTBR_DO_PAUSE:String="Pausar";
		static public const MSG_PTBR_DO_UNPAUSE:String="Despausar";

		static public const MSG_DO_PAUSE:String=MSG_PTBR_DO_PAUSE;
		static public const MSG_DO_UNPAUSE:String=MSG_PTBR_DO_UNPAUSE;
		static public const MAX_SCORE:int=999999999;
		
		static public const NUMFOCUS:int=6;
		static public const MSG_DENGUE_TIPS_TOTAL:int=9;
		
		
		
		static public const MSG_DENGUE_TIP0:DengueDicaTextoImagem=new DengueDicaTextoImagem(4,"Tampe os grandes depósitos de água!\nA boa vedação de tampas em recipientes como caixas d'água, tanques, tinas, poços e fossas impedirão que os mosquitos depositem seus ovos.");
		static public const MSG_DENGUE_TIP1:DengueDicaTextoImagem=new DengueDicaTextoImagem(2,"Remova todo o lixo!\nO acúmulo de lixo e de detritos em volta das casas pode servir como excelente meio de coleta de água de chuva. Por isso evite deixar o lixo acumulado no quital, se for preciso solicite a remoção pelo serviço de limpeza pública.");	
		static public const MSG_DENGUE_TIP2:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"Você sabia que existem larvicidas que podem ser colocados nos recipientes de água para matar as larvas em desenvolvimento? Os agentes de controle da dengue fazem esse serviço.");
		static public const MSG_DENGUE_TIP3:DengueDicaTextoImagem=new DengueDicaTextoImagem(5,"Limpe os recipientes de água!\nNão basta apenas trocar a água do vaso de planta ou usar um produto para esterilizar a água, como a água sanitária. É preciso lavar as laterais e as bordas do recipiente com bucha!");
		static public const MSG_DENGUE_TIP4:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"Use repelente!\nUse um repelente de insetos na pele exposta para repelir os mosquitos, mas cuidado ao usá-lo em crianças pequenas e idosos, em virtude da maior sensibilidade da pele.");
		static public const MSG_DENGUE_TIP5:DengueDicaTextoImagem=new DengueDicaTextoImagem(1,"Vasos com plantas aquáticas devem ter a água trocada e ser limpos com freqüência, de preferência com uma bucha ou escova. Já plantas que não necessitam de água podem ser transferidas para vasos com terra.");
		static public const MSG_DENGUE_TIP6:DengueDicaTextoImagem=new DengueDicaTextoImagem(1,"A pessoa estiver com a dengue deve manter-se em repouso, beber muito líquido (inclusive soro caseiro) e só usar medicamentos prescritos pelo médico, para aliviar as dores e a febre.");
		static public const MSG_DENGUE_TIP7:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"Na dengue hemorrágica o quadro clínico se agrava rapidamente, apresentando sinais de insuficiência circulatória e choque, podendo levar a pessoa à morte. A pessoa que estiver com os sintomas da dengue deve procurar um médico o mais rápido possível.");
		static public const MSG_DENGUE_TIP8:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"Existem duas formas de dengue: a clássica e a hemorrágica. Os sintomas geralmente são febre, dor de cabeça, no corpo, nas articulações e por trás dos olhos. Na dengue hemorrágica além dos sintomas da clássica, é possível ocorrer sangramento.");	
		
		static public const MSG_DENGUE_TIP9:DengueDicaTextoImagem=new DengueDicaTextoImagem(3,"Entregue seus pneus velhos ao serviço de limpeza urbana ou guarde-os sem água em local coberto e abrigados da chuva. Mesmo que furados ou rasgados, eles ainda podem acumular água e eventualmente servir de foco para mosquitos.");
		static public const MSG_DENGUE_TIPA:DengueDicaTextoImagem=new DengueDicaTextoImagem(0,"Guarde garrafas sempre tampadas ou de cabeça para baixo. Mesmo garrafas de boca ou corpo estreito podem servir de focos para o mosquito Aedes Aegypti.");
		static public const MSG_DENGUE_TIPB:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"A melhor forma de se evitar a dengue é combater os focos de acúmulo de água, locais propícios para a criação do mosquito transmissor da doença.");
		static public const MSG_DENGUE_TIPC:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"A primeira vez que jogar este jogo, sua vitória é obrigatória. No entanto, você pode voltar aqui a qualquer momento e tentar um novo recorde para ser o primeiro colocado no ranking. Capriche e não deixe os mosquitos vencerem!");
		static public const MSG_DENGUE_TIPD:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"Matar mosquitos sem errar aumenta seu multiplicador de bônus especial. Além disso, matar mosquitos numa mesma rajada rende bônus extra, num segundo multiplicador. Mas tome cuidado para não descarregar sua raquete!");
		static public const MSG_DENGUE_TIPE:DengueDicaTextoImagem=new DengueDicaTextoImagem(6,"Após 3 infecções em algum foco, o jogo invariavelmente termina. Não deixe o mosquito da dengue vencer! Em todas as fases, o seu \"limpa-focos\" será re-abastecido com 3 utilizações. Faça bom uso desse - especialmente quando a pilha de sua raquete elétrica estiver descarregada.");

		static public const POPUP_X:int=0.20*DengueUtils.STAGE_SIZE_X;
		static public const POPUP_Y:int=0.25*DengueUtils.STAGE_SIZE_Y;
		static public const POPUP_H:int=0.5*DengueUtils.STAGE_SIZE_Y;
		static public const POPUP_W:int=0.58*DengueUtils.STAGE_SIZE_X;
		static public const POPUP_BORDER_THICKNESS:int=6;
	}

}