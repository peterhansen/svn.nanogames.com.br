package ElementosMundoDeJogo
{
	import UIFramework2.*;
	import UIFramework2.DengueFadableSprite;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.sampler.Sample;
	import flash.text.TextField;
	import flash.utils.Timer;
	import Dengue.DengueUtils;
	
	public class DengueRaquete extends DengueFadableSprite
	{
		private var energiaRestante:int
		private var stateAtivada:Boolean;
		private var sparkSound:RaquetadaSound2;
		private var raqueteSpark:raquete;
		private var limpaFocos:limpafoco;
		private var reloading:Boolean;
		private var penaltyTimer:Timer;
		private var collisionArea:DengueNormalSprite;
/////////////	
		private var multiplier:int;
		private var mosquitoCounter:int;
		private var mosquitoCombo:int;		
		private var comboBreaked:Boolean;
		private var partialTally:int;
/////////////	
		public function getMultiplier():int
		{
			return multiplier;
		}
		
		public function getPartialTally():int
		{
			return partialTally;
		}
		
		public function playSoundHitSound():void
		{
			sparkSound.play();
		}
		
		public function DengueRaquete()
		{
			super();
			this.mouseChildren=false;
			sparkSound=new RaquetadaSound2();
			raqueteSpark=new raquete();
			addChild(raqueteSpark);

			limpaFocos=new limpafoco();
			addChild(limpaFocos);
			limpaFocos.visible=false;
			
			/*
			raqueteSpark.scaleY=(15/48)*(Utils.STAGE_SIZE_Y/raqueteSpark.height);
			raqueteSpark.scaleX=(8/64)*(Utils.STAGE_SIZE_X/raqueteSpark.width)
			limpaFocos.scaleY=(75/480)*(Utils.STAGE_SIZE_Y/limpaFocos.height);
			limpaFocos.scaleX=(83/640)*(Utils.STAGE_SIZE_X/limpaFocos.width);
			*/
			raqueteSpark.x+=raqueteSpark.width/2;
			raqueteSpark.y+=raqueteSpark.height/2;
			/*
			raqueteSpark.mouseChildren=false;
			
			limpaFocos.x+=limpaFocos.width/2;
			limpaFocos.y+=limpaFocos.height/2;
			limpaFocos.mouseChildren=false;
			*/
			////necessario para verificar a colisao. sera apadrinhado pela fase			
			collisionArea=new DengueNormalSprite();
			collisionArea.graphics.beginFill(0xAAAAAA);
			collisionArea.graphics.drawRect(0,0,raqueteSpark.width,raqueteSpark.height/2);
			collisionArea.graphics.endFill();
			collisionArea.visible=false;
			////////////////////////////////////////////////////////////////////
			
			reset();
			///combo breaker nao deve fazer parte de reset, porque o jogador pode ter um combo da fase anterior
			comboBreaker();			
			
		}		
///////////////////////////	
		
		
		public function comboBreaker():void
		{
			multiplier=1;	
			resetMultiplier();
		}
		
		public function resetMultiplier():void
		{
			mosquitoCombo=DengueUtils.MULTIPLIER_COMBO*multiplier;
			mosquitoCounter=0;
			partialTally=0;
		}
		
		public function COMBO():void
		{
			multiplier++;
			resetMultiplier();			
		}
		
		public function notifyHit():void
		{
			comboBreaked=false;
			mosquitoCounter++;
			partialTally++;
			if (mosquitoCombo==mosquitoCounter)
				COMBO();
		}
		
/////////////////////		
		private function unLocking(e:TimerEvent):void
		{
			reloading=true;
			penaltyTimer.stop();
		}
		
		
		public function getState():Boolean
		{
			return stateAtivada;
		}
		
		public function reset():void
		{
			reloading=true;
			energiaRestante=200;
			stateAtivada=false;
			//////////////////
			comboBreaked=false;
			//////////////////
			desativa();			
		}
		
		private function startPenalty():void
		{
			penaltyTimer=new Timer(DengueUtils.RAQUET_PENALTY_TIME,1);
			penaltyTimer.addEventListener(TimerEvent.TIMER, unLocking);
			penaltyTimer.start();
		}
		
		public function tick():void			
		{		
			
			if (stateAtivada)
				energiaRestante-=DengueUtils.RAQUET_UNLOAD_TIME;
			else
				if (energiaRestante<200 && reloading)
				energiaRestante+=DengueUtils.RAQUET_RELOAD_TIME;
			
			if (energiaRestante<=0)
			{
				desativa();
				reloading=false;
				startPenalty();
			}
			
		}
		
		public function useObject():void
		{
			if (getEnergiaRestante()<10)
				return;
			
			stateAtivada=true;
			raqueteSpark.gotoAndPlay(3);
			//////////////
			comboBreaked=true;
			partialTally=0;
			//////////////
		}
		
		public function desativa():void
		{
			
			stateAtivada=false;
			raqueteSpark.gotoAndStop(1);
			///////////////////////
			if (comboBreaked && partialTally==0)
				comboBreaker();
			//mosquitoCounter=0;
			partialTally=0;
			////////////////////
		}
		
		public function getEnergiaRestante():int
		{
			return energiaRestante;
		}
		
		public function getCollisionArea():DengueNormalSprite
		{
			return collisionArea;			
		}
		
		
		public function setLimpaFocos():void
		{
			raqueteSpark.visible=false;
			limpaFocos.visible=true;
		}
		
		public function setRaquete():void
		{
			raqueteSpark.visible=true;
			limpaFocos.visible=false;
		}
	}
}