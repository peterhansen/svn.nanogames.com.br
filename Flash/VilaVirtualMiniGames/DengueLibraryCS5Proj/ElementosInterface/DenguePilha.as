package ElementosInterface
{
	
	import flash.display.Shape;
	import flash.display.Sprite;
	import UIFramework2.DengueFadableSprite;
	import Dengue.DengueUtils;
	
	public class DenguePilha extends DengueFadableSprite		
	{
		private var nivelAtual:int; ///[0..200]
		private var nivelGrafico:Shape;
		private var bateria:Bateria;
		public function DenguePilha():void
		{
			nivelAtual=200;
			nivelGrafico=new Shape();
			bateria=new Bateria();
			this.addChild(bateria);
			this.addChild(nivelGrafico);
			this.x=DengueUtils.STAGE_SIZE_X-(bateria.width/4)-(2*DengueUtils.DEFAULT_HORIZONTAL_SPACING);
			this.y=DengueUtils.STAGE_SIZE_Y-(bateria.height/4)-(2*DengueUtils.DEFAULT_VERTICAL_SPACING);
		}
		
		public function setNivelAtual(novoNivel:int):void
		{
			nivelAtual=novoNivel;
			nivelGrafico.graphics.clear();
			nivelGrafico.graphics.beginFill(0xFFFFFF);
			nivelGrafico.graphics.drawRect(-(0.42*bateria.width)+0.05*bateria.width,-(0.57*bateria.height)+bateria.height*0.22,0.76*bateria.width,((200-nivelAtual)*bateria.height)/288 );
			nivelGrafico.graphics.endFill();
			nivelGrafico.alpha=0.8;
		}		
	}
}