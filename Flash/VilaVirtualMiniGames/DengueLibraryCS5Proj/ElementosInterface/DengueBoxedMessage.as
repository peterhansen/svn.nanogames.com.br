package ElementosInterface
{
	
	import UIFramework2.*;
	import UIFramework2.DengueFadableSprite;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	public class DengueBoxedMessage extends DengueFadableSprite
	{
		private var textField:DengueShadowTextField;
///----------------------------------------------------------------------------------------------------------
		public function DengueBoxedMessage(fontSize:int=12,text:String="",Fundo:DisplayObject=null)
		{
			
			var b:PointsBar=new PointsBar();
			var n:DengueNormalSprite=new DengueNormalSprite();
			addChild(n);
			n.addChild(b);				
			//n.width=225;
			//n.height=44;
			b.x+=b.width/2;
			b.y+=b.height/2;			
			textField=new DengueShadowTextField(new Font1(),2,2,/*26*/34,0xFFFFFF,0xFF6D00,false,false);
			textField.x=225/2.05;
			textField.y=44/2;
			addChild(textField);
								
		}
///----------------------------------------------------------------------------------------------------------		
		public function setMessage(Text:String):void
		{
			textField.setText(Text);
		}
///----------------------------------------------------------------------------------------------------------		
	}
}