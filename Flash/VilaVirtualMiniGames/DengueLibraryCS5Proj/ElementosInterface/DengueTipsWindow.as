package ElementosInterface
{
	import Dengue.DengueUtils;
	
	import UIFramework2.*;
	import UIFramework2.DengueShadowTextField;
	import UIFramework2.DengueWindowSprite;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.MouseEvent;
	import flash.text.Font;
	
	
	public class DengueTipsWindow extends DengueWindowSprite
	{
		///estrutura basica da janela
		private var fundo:mc_janela_popup;
		private var btFechar:DengueFeedbackButtom;
		
		private var currentPane:int;
		///view: tip view
		private var tipText:DengueShadowTextField;
		private var dengueTipsMessages:Array;
		///view: pause view
		private var pauseText:DengueShadowTextField;
		private var btSair:DengueFeedbackButtom;
		///view: game over view
		private var gameOverText:DengueShadowTextField;
		private var scoreText:DengueShadowTextField;
		private var highScoreText:DengueShadowTextField;
		private var scoresButtom:DengueFeedbackButtom;
		private var focusArray:DengueNormalSprite;
		
		private var onHideExternalCallback:Function;
		private var sairCallback:Function;
		private var windowTitle:DengueShadowTextField;	
		
		public function DengueTipsWindow(Fundo:DisplayObject=null)
		{
			var obj:DengueNormalSprite;			
			///pane basica
			currentPane=-1;
			fundo=new mc_janela_popup();			
			fundo.gotoAndStop(18);
			super(fundo);
			var dengueFont:AddervilleBook=new AddervilleBook();
			
			width=DengueUtils.POPUP_W;
			height=DengueUtils.POPUP_H;
			x=DengueUtils.POPUP_X;
			y=DengueUtils.POPUP_Y;
			
			
			fundo.x+=fundo.width/2;
			fundo.y+=fundo.height/2;
			
			///------------------------
			var btfechar_mc:bt_sair_x=new bt_sair_x();			
			btFechar=new DengueFeedbackButtom();

			
			getDefaultPane().addChild(btFechar);
			btFechar.addChild(btfechar_mc);		
/*
			btfechar_mc.width=100;
			btfechar_mc.height=100;
			btfechar_mc.x=0;
			btfechar_mc.y=0;
			btfechar_mc.x+=btfechar_mc.width/2;
			btfechar_mc.y+=btfechar_mc.height/2;
*/				
			btFechar.setOnMouseClickCallback(this.startHideAnimation);			

			btFechar.x=DengueUtils.POPUP_W- btfechar_mc.width/2-DengueUtils.DEFAULT_VERTICAL_SPACING;
			btFechar.y=DengueUtils.DEFAULT_VERTICAL_SPACING+ btfechar_mc.height/2;
			btfechar_mc.scaleX=0.8;
			btfechar_mc.scaleY=0.75;

			/*
			btFechar.width=7;
			btFechar.height=10;
			*/			
			///-----------------------------------
			
			windowTitle=new DengueShadowTextField(new AddervilleHeavy(),2,2,26,0xFFFFFF,0xFF6D00,false,false,false);
			windowTitle.setText("Combate à dengue");			
			windowTitle.x=DengueUtils.POPUP_W/2;
			windowTitle.y=DengueUtils.POPUP_BORDER_THICKNESS+1.5*DengueUtils.DEFAULT_VERTICAL_SPACING;
			getDefaultPane().addChild(windowTitle);
			windowTitle.y+=windowTitle.height/4;
			////////////////////////////

			
			/////////////configuracao das subviews///////////////
			var panel1:DengueNormalGroupSprite=new DengueNormalGroupSprite();
			var panel2:DengueNormalGroupSprite=new DengueNormalGroupSprite();
			var panel3:DengueNormalGroupSprite=new DengueNormalGroupSprite();
			
			this.addPanel(panel1);
			this.addPanel(panel2);
			this.addPanel(panel3);			
			////////////////////////////////////////////////////
			
			///////////////view 1: dicas contra dengue//////////////			
			{
				tipText=new DengueShadowTextField(dengueFont,0,0,8,0x8b4500,0,true,false,true);
				getPane(DenguePopUpWindowViewIds.TIPS).addChild(tipText);
				tipText.setAnchorOnCenter(false);
				loadAllTips();

/*								
				obj=new NormalSprite();
				obj.addChild(new limpafoco());
				getPane(PopUpWindowViewIds.TIPS).addChild(obj);
				obj.x=(Utils.POPUP_W-obj.width)/2;
				obj.y=Utils.POPUP_H-20-obj.height;
				obj.visible=false;
*/			
				
				var mc:MovieClip;
				
				
				
				focusArray=new DengueNormalSprite();				
				obj=new DengueNormalSprite();
				mc=new engradado();
				obj.addChild(mc);
				focusArray.addChild(obj);				
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
			//	obj.width=20;
			//	obj.height=20;				
				obj.visible=false;
			//	obj.scaleY=(105/480);
			//	obj.scaleX=(12/64);
					
				
				obj=new DengueNormalSprite();
				mc=new garrafa();
				mc.x+=20;
				obj.addChild(mc);
				focusArray.addChild(obj);				
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
			//	obj.width=20;
			//	obj.height=20;				
				obj.visible=false;
			//	obj.scaleY=(110/480);				
			//	obj.scaleX=(80/640);
				
				
				obj=new DengueNormalSprite();
				mc=new sacolixo();
				mc.x-=20;
				obj.addChild(mc);
				focusArray.addChild(obj);
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
			//	obj.width=20;
			//	obj.height=20;				
				obj.visible=false;
			//	obj.scaleY=(128/480);
			//	obj.scaleX=(140/640);

				
				obj=new DengueNormalSprite();
				mc=new pneu();
				mc.x-=30;
				obj.addChild(mc);				
				focusArray.addChild(obj);
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
			//	obj.width=20;
			//	obj.height=20;				
				obj.visible=false;
			//	obj.scaleY=(120/480);
			//	obj.scaleX=(160/640);

				
				obj=new DengueNormalSprite();
				mc=new tonel();
				obj.addChild(mc);
				focusArray.addChild(obj);				
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
				obj.getChildAt(0).y-=10;				
			//	obj.width=20;
			//	obj.height=20;				
				obj.visible=false;
			//	obj.scaleY=(16/48);
			//	obj.scaleX=(11/64);
				
			
				obj=new DengueNormalSprite();
				mc=new vaso();
				obj.addChild(mc);
				focusArray.addChild(obj);
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
			//	obj.width=20;
			//	obj.height=20;				
				obj.visible=false;
			//	obj.scaleY=(87/480);
			//	obj.scaleX=(80/640);
				
				obj=new DengueNormalSprite();
				mc=new limpafoco();
				mc.x-=20;
				obj.addChild(mc);
				obj.getChildAt(0).x-=65;
				obj.getChildAt(0).y-=50;
				
				obj.getChildAt(0).scaleX=1.5;
				obj.getChildAt(0).scaleY=1.5;
				
				focusArray.addChild(obj);
				obj.x=(DengueUtils.POPUP_W-obj.width)/2;
				obj.y=DengueUtils.POPUP_H-20-obj.height;
				//	obj.width=20;
				//	obj.height=20;				
				obj.visible=false;
				//	obj.scaleY=(87/480);
				//	obj.scaleX=(80/640);

			
				getPane(DenguePopUpWindowViewIds.TIPS).addChild(focusArray);
				setCurrentTip(0);
				
			}			
			///////////////////////////////////////////////////////
			
			///////////////view 2: pause//////////////			
			{
				pauseText=new DengueShadowTextField(dengueFont,2,2,37,0xFFFFFF,0xFF6D00,false,false);
				pauseText.x=DengueUtils.POPUP_W/2;
				pauseText.y=DengueUtils.POPUP_H/5;				
				pauseText.setText(DengueUtils.MSG_PAUSE);
				//pauseText.fitTo(Utils.POPUP_W,Utils.POPUP_H/2,Utils.POPUP_W,Utils.POPUP_H);
				getPane(DenguePopUpWindowViewIds.PAUSE).addChild(pauseText);
				
				var btSair_mc:BtSair=new BtSair();
				btSair=new DengueFeedbackButtom();
				btSair.addChild(btSair_mc);
				btSair.x=DengueUtils.POPUP_W/2;
				btSair.y=(DengueUtils.POPUP_H)-btSair.height/2-(4*1.5)*DengueUtils.DEFAULT_VERTICAL_SPACING-DengueUtils.POPUP_BORDER_THICKNESS;
				/*
				btSair.width=20;
				btSair.height=30;
				*/
				btSair.setOnMouseClickCallback(sair);
				getPane(DenguePopUpWindowViewIds.PAUSE).addChild(btSair);
			}			
			///////////////////////////////////////////////////////

			///////////////view 3: game over//////////////			
			{
				gameOverText=new DengueShadowTextField(dengueFont,2,2,37,0xFFFFFF,0xFF6D00,false,false);
				gameOverText.x=DengueUtils.POPUP_W/2;
				gameOverText.y=DengueUtils.POPUP_H/4;				
				gameOverText.setText(DengueUtils.MSG_GAMEOVER);			
				getPane(DenguePopUpWindowViewIds.GAMEOVER).addChild(gameOverText);
				

				scoreText=new DengueShadowTextField(dengueFont,2,2,20,0xFFFFFF,0xFF6D00,false,false);
				scoreText.x=DengueUtils.POPUP_W/2;
				scoreText.y=DengueUtils.POPUP_H/2;				
				scoreText.setText(DengueUtils.MSG_HIGHSCORE);
				getPane(DenguePopUpWindowViewIds.GAMEOVER).addChild(scoreText);
				
				
				highScoreText=new DengueShadowTextField(dengueFont,2,2,20,0xFFFFFF,0xFF6D00,false,false);
				highScoreText.x=DengueUtils.POPUP_W/2;
				highScoreText.y=3*DengueUtils.POPUP_H/4;
				highScoreText.setText(DengueUtils.MSG_HIGHSCORE);
				getPane(DenguePopUpWindowViewIds.GAMEOVER).addChild(highScoreText);
				highScoreText.visible=false;


			}			
			///////////////////////////////////////////////////////
			///configure a janela em si'			
			setView(DenguePopUpWindowViewIds.TIPS);
			setOnHide(onHideCallback);
			
		}
		
		public function setWindowTitle(Text:String):void
		{
			windowTitle.setText(Text);
		}
		
		override public function startHideAnimation(OnHide:Function=null):void
		{
			super.startHideAnimation(OnHide);
			fundo.gotoAndPlay("ExitPopup");			
		}
		
		override public function startShowAnimation(OnShow:Function=null):void
		{		
			super.startShowAnimation(showCurrentView);
			fundo.gotoAndPlay("EntryPopup");			
		}
		
		
		public function setOkCallback(func:Function):void
		{
			onHideExternalCallback=func;
		}
		
		public function onHideCallback():void
		{
		if (onHideExternalCallback!=null)
			onHideExternalCallback();
		}
		
		private function loadAllTips():void
		{
			this.dengueTipsMessages=new Array();
			this.dengueTipsMessages[0]=DengueUtils.MSG_DENGUE_TIP0;
			this.dengueTipsMessages[1]=DengueUtils.MSG_DENGUE_TIP1;
			this.dengueTipsMessages[2]=DengueUtils.MSG_DENGUE_TIP2;
			this.dengueTipsMessages[3]=DengueUtils.MSG_DENGUE_TIP3;
			this.dengueTipsMessages[4]=DengueUtils.MSG_DENGUE_TIP4;
			this.dengueTipsMessages[5]=DengueUtils.MSG_DENGUE_TIP5;
			this.dengueTipsMessages[6]=DengueUtils.MSG_DENGUE_TIP6;
			this.dengueTipsMessages[7]=DengueUtils.MSG_DENGUE_TIP7;
			this.dengueTipsMessages[8]=DengueUtils.MSG_DENGUE_TIP8;
			this.dengueTipsMessages[9]=DengueUtils.MSG_DENGUE_TIP9;
			this.dengueTipsMessages[10]=DengueUtils.MSG_DENGUE_TIPA;
			this.dengueTipsMessages[11]=DengueUtils.MSG_DENGUE_TIPB;
			this.dengueTipsMessages[12]=DengueUtils.MSG_DENGUE_TIPC;
			this.dengueTipsMessages[13]=DengueUtils.MSG_DENGUE_TIPD;
			this.dengueTipsMessages[14]=DengueUtils.MSG_DENGUE_TIPE;			
		}
		
		public function setCurrentTip(Tip:int):void
		{			
			var focusImage:DengueNormalSprite;
			
			Tip=Math.random()*(DengueUtils.MAX_LEVEL_DIFFICULTY -1);
			var dica:DengueDicaTextoImagem=dengueTipsMessages[Tip% this.dengueTipsMessages.length];
			
			
			
			

			for (var c:int=0;c<focusArray.numChildren;c++)
				DengueNormalSprite(focusArray.getChildAt(c)).visible=false;
			focusImage=	DengueNormalSprite(focusArray.getChildAt(dica.img));
			focusImage.visible=true;
			
					
			
			tipText.x=DengueUtils.POPUP_BORDER_THICKNESS+  (2)*DengueUtils.DEFAULT_HORIZONTAL_SPACING;
			tipText.width=160;
			this.tipText.setText(dica.text);
			
			tipText.y=(windowTitle.y+windowTitle.height)+DengueUtils.DEFAULT_VERTICAL_SPACING/4;
			tipText.height=DengueUtils.POPUP_H - (1.5*3*DengueUtils.DEFAULT_VERTICAL_SPACING-DengueUtils.POPUP_BORDER_THICKNESS) -  (windowTitle.height+windowTitle.y) ;

			//tipText.fitTo(Utils.POPUP_X,Utils.POPUP_Y,Utils.POPUP_W/2,tipText.height);
			tipText.setScrollY(0);
			
			focusImage.x=tipText.x+tipText.width+DengueUtils.DEFAULT_HORIZONTAL_SPACING*4+focusImage.width/2;
			focusImage.y=(windowTitle.y+windowTitle.height)+DengueUtils.DEFAULT_VERTICAL_SPACING/2+(focusImage.height/2);

		}
		
		public function showCurrentView():void
		{
			if (currentPane==-1)
				currentPane=0;
			
			setView(currentPane);
		}
		public function setView(viewId:int):void
		{
			currentPane=viewId;
			getPane(DenguePopUpWindowViewIds.GAMEOVER).visible=(viewId==DenguePopUpWindowViewIds.GAMEOVER);
			getPane(DenguePopUpWindowViewIds.PAUSE).visible=(viewId==DenguePopUpWindowViewIds.PAUSE);
			getPane(DenguePopUpWindowViewIds.TIPS).visible=(viewId==DenguePopUpWindowViewIds.TIPS);	
			getDefaultPane().showContent();
			getPane(viewId).showContent();
			windowTitle.visible=(viewId==DenguePopUpWindowViewIds.TIPS);
		}
		
		private function sair():void
		{
			if (sairCallback!=null)
				sairCallback();
		}
		
		public function setSairCallback(Func:Function):void
		{
			sairCallback=Func;
		}
		
		public function setHighScore(IsHigh:Boolean):void
		{
			highScoreText.visible=IsHigh;
		}
		
		public function setScoreText(ScoreText:String):void
		{
			scoreText.setText(ScoreText);
			/*
			scoreText.x=35-(scoreText.getText().length/1.2);
			scoreText.getTextField().width=scoreText.getText().length*7;
			scoreText.getTextShadow().width=scoreText.getText().length*7;
			*/
		}

	}
}