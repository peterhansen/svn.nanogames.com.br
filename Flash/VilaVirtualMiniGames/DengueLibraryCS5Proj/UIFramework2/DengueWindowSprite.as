package UIFramework2
{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.ui.Mouse;

	public class DengueWindowSprite extends DengueNormalGroupSprite
	{
		private var fundo:DisplayObject;
		private var panels:Array;
		
		
	
		public function DengueWindowSprite(Fundo:DisplayObject=null)
		{		
			super();
			
			if (Fundo==null)
			{
				/*
				var shape:Shape=new Shape();
				shape.graphics.beginFill(0xAAAAAA);
				shape.graphics.drawRect(0,0,100,100);
				shape.graphics.endFill();
				Fundo=shape;
				*/
			}

			fundo=Fundo;
			
			if (Fundo is MovieClip)
				addMovieClip(MovieClip(Fundo));
			else
				addChild(fundo);
			
			panels=new Array();
			addPanel(new DengueNormalGroupSprite());
//			width=100;
//			height=100;
		}
		
		override public function startShowAnimation(OnShow:Function=null):void
		{
			super.startShowAnimation(OnShow);
			getDefaultPane().showContent();
			flash.ui.Mouse.show();
		}
		
		override public function startHideAnimation(OnHide:Function=null):void
		{
			flash.ui.Mouse.show();
			super.startHideAnimation(OnHide);
		}
		
		public function addPanel(Panel:DengueNormalGroupSprite):void
		{
			panels[totalPanels()]=Panel;	
			addChild(Panel);
		}
		/*
		public function attachPanelToBottom(Panel:NormalGroupSprite):void
		{
			addPanel(Panel);
			var shape:Shape=getNormalBounds();
			Panel.x=shape.x;
			Panel.y=shape.y;
		}
		*/
		
		public function getDefaultPane():DengueNormalGroupSprite
		{
			return getPane(0);
		}
		
		public function getPane(index:int):DengueNormalGroupSprite
		{
			return panels[index];
		}
		
		public function totalPanels():int
		{
			return panels.length;
		}
		/*
		public function maximize():void
		{
			saveShape();
			this.x=0;
			this.y=0;
			this.width=parent.width;
			this.height=parent.height;
		}
		
		public function minimize():void
		{
			saveShape();
			
			this.width=this.minimumWidth;
			this.height=this.minimumHeight;

		}
		
		public function saveShape(Target:Shape=null):void
		{
			var target:Shape;
			
			if (Target==null)
				target=lastShape;
			
			target.x=this.x;
			target.y=this.y;
			target.width=this.width;
			target.height=this.height;
		}
		
   		public function restore():void
		{
			var shape:Shape=new Shape();
			saveShape(shape);
			
			this.x=lastShape.x;
			this.y=lastShape.y;
			this.width=lastShape.width;
			this.height=lastShape.height;
			
			lastShape.x=shape.x;
			lastShape.y=shape.y;
			lastShape.width=shape.width;
			lastShape.height=shape.height;
		}
		*/
	}
}