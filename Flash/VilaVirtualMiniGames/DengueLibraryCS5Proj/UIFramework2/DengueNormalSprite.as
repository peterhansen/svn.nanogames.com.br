package UIFramework2
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	

	public class DengueNormalSprite extends Sprite
	{
		private var shape:Shape;
		
		private var onMouseClickCallback:Function;
		private var onMouseDownCallback:Function;
		private var onMouseUpCallback:Function;
		
		private var onMouseOverCallback:Function;
		private var onMouseMoveCallback:Function;
		private var onMouseOutCallback:Function;
		private var onMouseScrollCallback:Function;
		
		public function DengueNormalSprite()
		{
			super();
			
			/*
			shape=new Shape();		
			shape.visible=false;			
			addChild(shape);
			recalcBody();
			*/
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);	
			this.addEventListener(MouseEvent.CLICK, onMouseClick);
			this.addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			this.addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			this.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseScroll);
		}
		/*
		public function _setVisible():void
		{
			shape.visible=true;
		}
		
		public function _setInvisible():void			
		{
			shape.visible=false;
		}
		*/
	/*	
		public function recalcBody():void
		{
			var ratioH:Number;
			var ratioV:Number;
			ratioH=width/Utils.STAGE_SIZE_X;
			ratioV=height/Utils.STAGE_SIZE_Y;
	
			shape.graphics.clear();
			shape.graphics.beginFill(0xFF0000);			
			shape.graphics.drawRect(0,0,100*ratioH,100*ratioV);
			shape.graphics.endFill();
		}
	*/	
		
		private function setBrightness(brillo:Number):void
		{ 
			// changes the brightness
			// mc is the movieclip you want to change
			if (brillo > 1) brillo=1;
			if (brillo < -1) brillo=-1;
			
			var colorTrans:ColorTransform= new ColorTransform();
			//value between -1 and 1
			var trans:Transform=new Transform(this);
			colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = 1 - Math.abs (brillo); // color percent
			colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= (brillo > 0) ? brillo * 256 : 0; // color offset
			trans.colorTransform=colorTrans;
		}
		
		
		private function setContrast(contraste:Number):void
		{ 
		if (contraste > 1) contraste=1;
		if (contraste < -1) contraste=-1;
		/// contraste between -1 and 1

		var colorTrans:ColorTransform= new ColorTransform();
		//val entre -1 y 1
		var trans:Transform=new Transform(this);
		
		colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = contraste; // color percent
		colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= 128 - (128 * contraste); // color offset		
		trans.colorTransform=colorTrans;
		}

		
		
		
		public function setNormalHue():void
		{
			setBrightness(0);
		}
		
		public function setLightHue():void
		{
			setBrightness(0.5);
		}
		
		public function setDarkHue():void
		{
			setBrightness(-0.5);
		}
		
		public function setOnMouseClickCallback( func:Function):void
		{
			onMouseClickCallback=func;
		}
		

		public function setOnMouseScrollCallback( func:Function):void
		{
			onMouseScrollCallback=func;
		}
		
		
		public function setOnMouseDownCallback( func:Function):void
		{
			onMouseDownCallback=func;
		}

		public function setOnMouseUpCallback( func:Function):void
		{
			onMouseUpCallback=func;
		}

		public function setOnMouseOverCallback( func:Function):void
		{
			onMouseOverCallback=func;
		}

		public function setOnMouseMoveCallback( func:Function):void
		{
			onMouseMoveCallback=func;
		}

		public function setOnMouseOutCallback( func:Function):void
		{
			onMouseOutCallback=func;
		}

		
		public function onMouseDown(e:MouseEvent):void
		{
			if (onMouseDownCallback!=null)
				onMouseDownCallback();
		}
		
		public function onMouseUp(e:MouseEvent):void
		{
			if (onMouseUpCallback!=null)
				onMouseUpCallback();

		}
		
		public function onMouseClick(e:MouseEvent):void
		{
			if (onMouseClickCallback!=null)
				onMouseClickCallback();
		}

		public function onMouseOver(e:MouseEvent):void
		{
			if (onMouseOverCallback!=null)
				onMouseOverCallback();
		}

		public function onMouseMove(e:MouseEvent):void
		{
			if (onMouseMoveCallback!=null)
				onMouseMoveCallback();
		}

		public function onMouseOut(e:MouseEvent):void
		{
			if (onMouseOutCallback!=null)
				onMouseOutCallback();
		}


		public function onMouseScroll(e:MouseEvent):void
		{
			if (onMouseScrollCallback!=null)
				onMouseScrollCallback();
		}

		
		
		public function setX(X:int):void
		{
			x=X;
		}
		
		public function setY(Y:int):void
		{
			y=Y;
		}

		public function setW(W:int):void
		{
			width=W;
		}

		
		public function setH(H:int):void
		{
			height=H;
		}

		public function set(X:int,Y:int,W:int,H:int):void
		{
			x=X;
			y=Y;
			width=W;
			height=H;
		}
		
		public function getX():int
		{
			return x;
		}
		
		public function getY():int
		{
			return y;
		}
		
		public function getW():int
		{
			return width;
		}
		
		
		public function getH():int
		{
			return height;
		}
		
		
	}
}