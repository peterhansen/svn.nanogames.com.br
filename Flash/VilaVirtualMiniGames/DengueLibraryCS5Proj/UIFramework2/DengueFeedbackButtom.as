package UIFramework2
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;

	public class DengueFeedbackButtom extends DengueFadableSprite
	{
		
		private var controlCursor:Boolean;
		private var feedbackSound:InteracaoSound;
		private var highlightEnabled:Boolean;
		
		public function DengueFeedbackButtom(mc:MovieClip=null,ControlCursor:Boolean=false,MakeSound:Boolean=true)
		{
			super();
			if (mc!=null)
				addChild(mc);
			
			setOnMouseOverCallback(onOver);
			setOnMouseOutCallback(onOut);
			setOnMouseDownCallback(setDarkHue);
			setOnMouseUpCallback(setNormalHue);
				
			if (MakeSound)
				{
				feedbackSound=new InteracaoSound();
				this.addEventListener(MouseEvent.CLICK,feedbackSound.play);
				}
			this.highlightEnabled=true;
			controlCursor=ControlCursor;
			this.mouseChildren=false;
			this.buttonMode=true;
		}
		
		public function setHighlight(Set:Boolean):void
		{
			highlightEnabled=Set;
		}
		
		public function onOver():void
		{
			if (!highlightEnabled) return;
			
			setLightHue();			
			if (controlCursor)
				flash.ui.Mouse.show();
		}
		
		public function onOut():void
		{
			
			setNormalHue();
			/*
			if (controlCursor)
				flash.ui.Mouse.hide();
			*/
		}
	}
}