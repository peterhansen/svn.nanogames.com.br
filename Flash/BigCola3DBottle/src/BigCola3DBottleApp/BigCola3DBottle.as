package BigCola3DBottleApp
{
	import GUILib.GUIConstants;
	
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.filters.*;
	import flash.geom.Point;
	import flash.utils.Timer;
	
	import org.osmf.events.LoaderEvent;
	import org.papervision3d.core.math.Number3D;
	import org.papervision3d.core.proto.MaterialObject3D;
	import org.papervision3d.core.render.data.RenderHitData;
	import org.papervision3d.lights.PointLight3D;
	import org.papervision3d.materials.BitmapFileMaterial;
	import org.papervision3d.materials.BitmapMaterial;
	import org.papervision3d.materials.shadematerials.PhongMaterial;
	import org.papervision3d.materials.shaders.GouraudShader;
	import org.papervision3d.materials.shaders.ShadedMaterial;
	import org.papervision3d.materials.utils.MaterialsList;
	import org.papervision3d.objects.DisplayObject3D;
	import org.papervision3d.objects.parsers.Collada;
	import org.papervision3d.objects.parsers.DAE;
	import org.papervision3d.view.BasicView;
	
	
	/**
	 * 
	 * @author Daniel "Monty" Monteiro
	 */
	[SWF( frameRate = "60" )]
	public class BigCola3DBottle extends BasicView
	{
		/**
		 * 
		 */
		public static const TWEEN_TIME: Number = 2.0; 		
		/**
		 * 
		 */
		protected var gira1:DisplayObject3D;
		/**
		 * 
		 */
		protected var naogira1:DisplayObject3D;
		/**
		 * 
		 */
		private var pressed:Boolean;
		/**
		 * 
		 */
		private var diff:Point = new Point( 0,0 );
		/**
		 * 
		 */
		private var lastDiff:Point = new Point( 0,0 );
		/**
		 * 
		 */
		private var drag:Point = new Point( 0,0 );
		/**
		 * 
		 */
		public var pitch:Number;
		/**
		 * 
		 */
		public var roll:Number;
		/**
		 * 
		 */
		private var zoom: Number;
		/**
		 * 
		 */
		private var materialList:MaterialsList;
		/**
		 * 
		 */
		private var mat:MaterialObject3D;
		/**
		 * 
		 */
		private var filter:GlowFilter = new GlowFilter (0x000000,1,5,5,50,1,false,false);
		/**
		 * 
		 */
		private var phong:PhongMaterial;
		/**
		 * 
		 */
		private var light:PointLight3D;
		/**
		 * 
		 */
		private var angle:Number = new Number(0);
		/**
		 * 
		 */
		private var shadedMaterial:ShadedMaterial;
		/**
		 * 
		 */
		private var partsReady:int = 0;
		
		
		/**
		 * 
		 */
		public function BigCola3DBottle()
		{
			super();
			pitch = 0;
			roll = 0;
			zoom = 1;
			pressed = false;
			createChildren();
			startRendering();
			viewport.filters = [filter];
		}
		
		
		/**
		 * 
		 */
		public function createChildren():void 
		{
			
			gira1 = new Collada( "gira.dae", null, 7.0/5.0 , true);
			naogira1 = new Collada( "naogira.dae", null , 7.0/5.0 , true);
			
			//Set some properties
			gira1.moveDown( 200 );
			naogira1.moveDown( 200 );
			viewport.interactive = true;
			viewport.mouseEnabled = true;
			
			//Add to scene
			scene.addChild( gira1 );
			scene.addChild( naogira1 );
			gotoLabel();
			ExternalInterface.addCallback( "rotateTo", rotateTo );
			ExternalInterface.addCallback( "gotoTeam", gotoTeam );
			ExternalInterface.addCallback( "gotoInfo", gotoInfo );
			ExternalInterface.addCallback( "gotoLabel", gotoLabel );
			addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			stage.addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			stage.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			stage.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
		}
		
		
		/**
		 * 
		 */
		private function loadComplete( e:Event ):void {
			trace("load complete");
		}
		
		
		/**
		 * 
		 */
		private function startDebugTimers(): void {
			var timer:Timer;
			timer = new Timer( 10 * GUILib.GUIConstants.SECONDS, 1 );
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, func1 );
			timer.start();
			
			timer = new Timer( 20 * GUILib.GUIConstants.SECONDS, 1 );
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, func2 );
			timer.start();
			
			timer = new Timer( 30 * GUILib.GUIConstants.SECONDS, 1 );
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, func3 );
			timer.start();
		}
		
		
		/**
		 *
		 */
		private function func1( e:Event ) : void {
			trace( "func1" );
			e.target.stop();
			gotoTeam();
		}
		
		
		/**
		 * 
		 */
		private function func2( e:Event ) : void {
			trace( "func2" );
			e.target.stop();
			gotoInfo();
		}
		
		
		/**
		 * 
		 */
		private function func3( e:Event ) : void {
			trace( "func3" );
			e.target.stop();
			gotoLabel();
			startDebugTimers();
		}
		
		
		/**
		 * 
		 */
		override protected function onRenderTick(event:Event = null ) : void {
			
			super.onRenderTick( event );
			
			gira1.rotationX = 270;
			naogira1.rotationX = 270;
			gira1.pitch( pitch );
			gira1.roll( roll );
			naogira1.pitch( pitch );
		}
		
		
		/**
		 * 
		 */
		public function rotateTo( location:String ):void {
			
			if ( location == "team" )
				gotoTeam();
			else if ( location == "label" ) 
				gotoLabel();
			else if ( location == "info" )
				gotoInfo();
			
		}
		
		
		/**
		 * 
		 */
		public function gotoLabel():void {
			TweenLite.to( this, TWEEN_TIME, { roll: 0,  pitch: 0 } );
		}
		
		
		/**
		 * 
		 */
		public function gotoInfo():void {
			TweenLite.to( this, TWEEN_TIME, { roll: 133, pitch: 0 } );
		}
		
		
		/**
		 * 
		 */
		public function gotoTeam():void {
			TweenLite.to( this, TWEEN_TIME, { pitch: 0, roll: -109 } );
		}			
		
		
		/**
		 * 
		 */
		public function onMouseDown( e:MouseEvent ):void {
			if ( !pressed ) {
				resetDrag();
				diff.x = stage.mouseX;
				diff.y = stage.mouseY;

			}
			
			pressed = true;
		}		
		
		
		/**
		 * 
		 */
		public function onMouseOver( e:MouseEvent ):void {
			resetDrag();
		}
		
		
		/**
		 * 
		 */
		public function onMouseMove( e:MouseEvent ):void {
			if ( pressed ) {
				
				lastDiff.x = stage.mouseX - diff.x; 
				lastDiff.y = stage.mouseY - diff.y; 
				drag.x +=  lastDiff.x;
				drag.y +=  lastDiff.y;
				diff.x = stage.mouseX;
				diff.y = stage.mouseY;
								
				roll += clamp( -22.5, ( - ( drag.x * 180 ) / stage.stageWidth ), 11.25 );
				pitch += clamp( -22.5, ( - ( drag.y  * 90 ) / stage.stageHeight ), 11.25 );
				
			} else {
				resetDrag();
			}

		}
		
		
		/**
		 * 
		 */
		private function clamp( lim1:Number, n:Number, lim2:Number) : Number {
			var toReturn:Number;
			
			toReturn = n;
			
			if ( toReturn > lim2 )
				toReturn = lim2;
			
			if ( toReturn < lim1 )
				toReturn = lim1;

			
			return toReturn;
		}
		
		
		/**
		 * 
		 */
		public function onMouseUp( e:MouseEvent ):void {
			resetDrag();
		}
		
		
		/**
		 * 
		 */
		private function resetDrag():void {
			pressed = false;
			diff.x = 0;
			diff.y = 0;
			drag.x = 0;
			drag.y = 0;
			lastDiff.x = 0;
			lastDiff.y = 0;
		}
	}
}
