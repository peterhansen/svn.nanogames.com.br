package {
	import flash.display.BlendMode;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.external.ExternalInterface;
	import flash.media.Sound;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.text.AntiAliasType;
	import flash.text.Font;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.utils.getDefinitionByName;
	
	// TODO: Carregar parâmetros do JAVASCRIPT!!!!
	// - Ou alterar arquiv .html gerado para o programa 
	// - Ou http://www.adobe.com/livedocs/flash/9.0/ActionScriptLangRefV3/flash/external/ExternalInterface.html

	// Metatags que definem as propriedades padrão do stage
	//[SWF( width= "1024", height = "768", backgroundColor = "0x000000", frameRate = "32" )]
	//[SWF( width= "800", height = "600", backgroundColor = "0x000000", frameRate = "32" )]
	//[SWF( width= "640", height = "480", backgroundColor = "0x000000", frameRate = "24" )]
	[SWF( width= "800", height = "640", backgroundColor = "0x0d2166", frameRate = "60" )]
	public final class PreloaderApp extends MovieClip 
	{
		private var myMovie : Sprite;
		private var label : TextField;
		private var textFormat : TextFormat;
		private var l : Loader = new Loader();
		private var pl : Preloader;
	
		
		public function PreloaderApp() {
			super();
			//			initializingLabel = "Inicializando";
			//			downloadingLabel = "Carregando";
			
			textFormat = new TextFormat( "Verdana", 15, 0xffffff );
			//textFormat.align = "center";
			
			label = new TextField();
			label.defaultTextFormat = textFormat;
			addChild(label);
			
			pl = new Preloader();
			addChild( pl );
			pl.gotoAndPlay( 0 );
			pl.x = -110 + ( ( 800 ) / 2 );
			pl.y = -125 + ( ( 640 ) / 2 );
			label.y = -124 + ( ( 640 + pl.height - label.textHeight ) / 2 );
			
			l.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, loop, false, 0, true);
			l.contentLoaderInfo.addEventListener(Event.COMPLETE, done, false, 0, true);
			
			var fileRequest:URLRequest = new URLRequest( "http://186.202.60.210/swf/show_de_bola/ShowDeBola.swf" );
			
			l.load( fileRequest );
		}
		
		private function loop(e:ProgressEvent) : void {
			var perc:Number = e.bytesLoaded / e.bytesTotal;
			label.text = int(perc*100) + "%";
			
			label.x = -110 + ( ( 800 - label.textWidth ) / 2 );
			
			//			percent.text = Math.ceil(perc*100).toString();
		}
		
		
		private function done( e : Event ) : void {
			//			removeChild(percent); // was removeChildAt(0) but it make more sense this way
			//			percent = null;
			
			myMovie = Sprite( l.content );
			
			removeChild(label);
			removeChild(pl);
			addChild(myMovie);
			
			l.contentLoaderInfo.removeEventListener( ProgressEvent.PROGRESS, loop);
			l.contentLoaderInfo.removeEventListener(Event.COMPLETE, done);
			l = null;
		}
	}
}
