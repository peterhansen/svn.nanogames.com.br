require 'rubygems'
require 'hpricot'
require 'enumerator'

TILES_PER_LINE = 200

unless ( ARGV.size >= 2 )
  puts "Uso: map_parser DIRETORIO_DE_SAIDA ARQUIVO_DE_ENTRADA_1 ARQUIVO_DE_ENTRADA_2 ARQUIVO_DE_ENTRADA_3 ..."
  return 0
end

TOTAL_INPUT_FILES = ARGV.size - 1

#workaround_text = "\n\t\t// Workaround para o AS3: � necess�rio existir refer�ncias para as classes criadas dinamicamente, ou o compilador as exclui."
#for i in 1..TOTAL_INPUT_FILES do
#  workaround_text += "\n\t\tprivate var workaround#{i} : TilesConstants#{i} = new TilesConstants#{i}();"
#end

for i in 1..TOTAL_INPUT_FILES do
	puts( "\nProcessando arquivos: #{ARGV[ i ]} -> #{ARGV[0]}/TilesConstants#{i}.as" )
	doc = Hpricot.parse( File.read( ARGV[ i ] ) )
	tiles = []

	out = File.new( "#{ARGV[0]}/TilesConstants#{i}.as", 'w+' )

	out.puts "package\n{\n\tpublic final class TilesConstants#{i}\n\t{\n\t\t//AUTO-GENERATED_FILE: re-run map_parser script to re-create the file.\n\n\t\tpublic function TilesConstants#{i}() {\n\t\t}"
	  
	(doc/:map).each do |map|
	  #out.puts workaround_text
	  out.puts "\n\t\tpublic const version : String = \'#{map.attributes[ 'version' ]}\';"
	  out.puts "\n\t\tpublic const orientation : TileMapOrientation = TileMapOrientation.#{map.attributes[ 'orientation' ] == 'isometric' ? 'ORIENTATION_ISO' : 'ORIENTATION_ORTHO' };"
	  out.puts "\n\t\tpublic const nRows : uint = #{map.attributes[ 'height' ]};"
	  out.puts "\n\t\tpublic const nColumns :uint = #{map.attributes[ 'width' ]};"
	  out.puts "\n\t\tpublic const mapTileWidth : uint = #{map.attributes[ 'tilewidth' ]};"
	  out.puts "\n\t\tpublic const mapTileHeight : uint = #{map.attributes[ 'tileheight' ]};"

	  out.puts "\n\t\tpublic var mapTilesets : Array = ["
	  
	  (doc/:tileset).each do |tileset|
		puts "lendo tileset #{tileset['name']}"
		
		out.puts "\t\t\t{ name: \'#{tileset['name']}\', firstGID: #{tileset['firstgid']}, tileWidth: #{tileset['tilewidth']}, tileHeight: #{tileset['tileheight']}, imgSrc: \'#{(tileset/:image)[0]['source'].gsub( '../', 'rsc/' ) }\', tiles: [ "
		
		tiles = []
		
		(tileset/:tile).each do |tile|
		  s = "{ id: #{tile.attributes['id'].to_i}, "
		  (tile/:properties).each do |p|
			props = []
			read_props = {}
			(p/:property).each do |prop|
			  # evita duplica��o de atributos (workaround para bug do Tiled)
			  unless read_props[ prop[ 'name' ] ]
			    read_props[ prop[ 'name' ] ] = true
			    props << "#{prop['name']}: #{prop['value'] == '1' }" 
			  end
			end
			s += "#{props.join( ', ' ) } }\n\t\t\t\t\t"
		  end
		  tiles << s
		end
		out.puts "\t\t\t\t#{ tiles.join( ', ' ) }\n\t\t\t] },"
	  end
	  out.puts "\t\t];"

	  out.puts "\n\t\tpublic var layers : Array = ["
	  layers = []
	  portals = []
	  
	  # tiles de diferentes layers que ser�o unificados 
	  layers_tiles = {}
	  layers_to_merge = {}
	  
	  (doc/:layer).each do |layer|
	    (layer/:properties/:property).each do |p|
		  if ( p[ 'name' ] == 'mergewith' )
		    layer[ 'mergewith' ] = p[ 'value' ]
		  end
		end
		puts "lendo layer #{layers.length}: #{layer['name']} -> #{layer['mergewith']}}"
		s = "\t\t\t{ name:\'#{layer['name']}\', width:#{layer['width']}, height:#{layer['height']}, mergewith:#{layer['mergewith'].nil? ? 'null' : "\'#{layer['mergewith']}\'" }, data: [ "
		tiles = []
		count = 0
		
		layers_tiles[ layer['name'] ] = []
		
		(layer/:data/:tile).each do |tile|
		  tiles << tile['gid']
		  layers_tiles[ layer[ 'name' ] ][ count ] = tile[ 'gid' ]
		  
		  if ( tile[ 'portal_id' ] )
			portals << "{ name: \'#{ tile[ 'portal_id' ] }\', index: #{ count } }"
		  end
		  count += 1
		end
		
		tiles.each_slice( TILES_PER_LINE ) { |a|
		  s += "#{a.join(', ') },\n\t\t\t\t"
		}	
		layers << s + "]\n\t\t\t}"
		
		if ( layer['mergewith'] )
		  layers_to_merge[ layer['name'] ] = layer['mergewith']
		end
	  end
	  out.puts "#{layers.join( ",\n" )}\n\t\t];\n"
	  
	  out.puts "\n\t\tpublic var portals : Array = ["
	  out.puts "\t\t\t#{portals.join( ",\n" )}\n\t\t];\n"
	  
	  if ( layers_to_merge )
		out.puts "\n\t\tpublic var layersToMerge : Array = ["
		layers_to_merge.each { |k, v|
		  puts "merging #{k} and #{v}"
		  tiles = []
		  layers_tiles[ k ].each_with_index { |t, i|
			if ( t.to_i != 0 )
			  #tiles << "[ #{i}, #{t} ]"
			  tiles << i
			end
		  }

		  puts "#{layers_tiles[k].length} tiles na layer"
		  puts "#{tiles.length} tiles mesclados"
		  s = "\t\t\t{ layerName: \'#{k}\', otherLayer: \'#{v}\', tiles: ["
		  
		  tiles.each_slice( TILES_PER_LINE ) { |a|
			s += "\n\t\t\t\t#{a.join(', ') },"
		  }	
		  
		  out.puts "#{s} ]\n\t\t\t},"
		}
		out.puts "\t\t];\n"
	  end
	end

	out.puts "\t}\n}"
end



