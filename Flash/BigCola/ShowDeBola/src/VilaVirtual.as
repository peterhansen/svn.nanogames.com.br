package
{
//	import NGC.ASWorkarounds.MovieClipVS;
//	import NGC.ByteArrayEx;
//	import NGC.LetterKeyCodes;
//	import NGC.NanoOnline.INOListener;
//	import NGC.NanoOnline.NOAppCustomerData;
//	import NGC.NanoOnline.NOCustomer;
//	import NGC.NanoOnline.NOErrors;
//	import NGC.NanoOnline.NOGlobals;
//	import NGC.NanoOnline.NOTextsIndexes;
//	import NGC.NanoSounds.SoundManager;
//	import NGC.ScheduledTask;
//	import NGC.Utils;
//	
//	import de.polygonal.ds.HashMap;
//	
//	import fl.transitions.Tween;
//	import fl.transitions.TweenEvent;
//	import fl.transitions.easing.Regular;
//	
//	import flash.display.BlendMode;
//	import flash.display.MovieClip;
//	import flash.display.StageAlign;
//	import flash.display.StageDisplayState;
//	import flash.display.StageQuality;
//	import flash.display.StageScaleMode;
//	import flash.events.Event;
//	import flash.events.KeyboardEvent;
//	import flash.events.MouseEvent;
//	import flash.events.ProgressEvent;
//	import flash.events.TimerEvent;
//	import flash.geom.Point;
//	import flash.media.Sound;
//	import flash.text.TextFormat;
//	import flash.ui.Keyboard;
//	import flash.utils.Timer;
//	import flash.utils.describeType;
//	import flash.utils.getDefinitionByName;
//	import flash.utils.getTimer;
//	
//	import org.osmf.events.TimeEvent;
//
	public final class VilaVirtual {	
//		// Constantes
//		
//		// Intervalo do looping de atualização do jogo
//		private static const TIMER_INTERVAL_MILIS : uint = 20;
//		
//		// Intervalo máximo de atualização permitido
//		private static const MAX_UPDATE_INTERVAL_MILIS : int = 125;
//
//		/**
//		* Tilemap atual 
//		*/		
//		private var _tileMap : TiledEditorTileMap;
//		
//		private var onTileMapLoadedCallback : Function;
//		private var onTileMapLoadedCallbackParams : Array;
//		
//		/** Fundo de tela exibido atualmente. */
//		private var currentBkg : MovieClip;
//		
//		/**
//		* Controla o looping de atualização do jogo 
//		*/		
//		private var _gameLoopTimer : Timer;
//		
//		/**
//		* Personagem do jogador 
//		*/		
//		private var _playerCharacter : Character;
//		
//		/**
//		* Personagens existentes no mapa excluindo o personagem do jogador (NPCs) 
//		*/		
//		private var _characters : HashMap;
//		
//		/**
//		* Container que armazena os eventos ativos no mapa de tiles 
//		*/		
//		private var _tileEvents : HashMap;
//		
//		/**
//		* Container que armazena as quests do jogo 
//		*/		
//		private var _questLog : HashMap;
//		
//		/**
//		* Momento da última atualização (em milissegundos) 
//		*/		
//		private var _lastUpdateTime : int;
//		
//		/**
//		* Intervalo máximo de atualização permitido (em milissegundos) 
//		*/		
//		private var _maxUpdateIntervalAllowed : int;
//
//		/**
//		* Estado atual do jogo 
//		*/		
//		private var _currGameState : GameState;
//		
//		/**
//		 * Momento da última troca de estado do jogo.
//		 */
//		private var _lastGameStateTime : int;
//		
//		/**
//		 * Intervalo mínimo entre 2 conversas.
//		 */
//		private const TALK_MIN_INTERVAL : int = 1300;
//		
//		/**
//		* Estado anterior ao estado atual 
//		*/		
//		private var _lastGameState : GameState;
//
//		/**
//		* Componente utilizado para exibir os textos dos diálogos 
//		*/		
//		private var _talkDialog : TextDialog;
//		
//		/**
//		* Barra de felicidade da população 
//		*/		
//		private var _happinessBar : HappinessBar;
//
//		/**
//		* Ícone mochila. Quando clicado, exibe os itens que o jogador possui 
//		*/		
//		private var _iconBackpack : MovieClipVS;
//		
//		/**
//		* Ícone mapa. Quando clicado, exibe o mapa da cidade
//		*/		
//		private var _iconMap : MovieClipVS;
//		
//		/**
//		* Ícone livro. Quando clicado, exibe as quests do usuário 
//		*/		
//		private var _iconBook : MovieClipVS;
//		
//		/**
//		* Ícone menu. Quando clicado, exibe o menu de opções 
//		*/		
//		private var _iconMenu : MovieClipVS;
//		
//		/**
//		* Container que agrupa os elementos da interface do usuário
//		*/
//		private var menuTop : MovieClipVS;
//		
//		private var mapScreen : MapScreen;
//		private var _backpack : Backpack;
//		private var questScreen : QuestScreen;
//		
//		private var trafficManager : TrafficManager;
//
//		/**
//		* Câmera do jogo 
//		*/		
//		private var _gameCamera : Camera;
//		
//		/**
//		* Indica se o input deve ser utilizado para movimentar o personagem ou para movimentar a câmera 
//		*/		
//		private var _movingCamera : Boolean;
//		
//		/**
//		* Controla a movimentação da câmera por input do usuário 
//		*/		
//		private var _cameraMovement : Point;
//		
//		/**
//		* Indica se já calculamos os tiles visíveis 
//		*/		
//		private var _tileMapVisibleAreaSet : Boolean;
//		
//		/**
//		* Indica se o teclado está bloqueado.
//		*/
//		private var _keyLocked : Boolean;
//		
//		// variáveis de controle da animação dos ícones
//		private var iconBlinkTweener : Tween;
//		private var currentIcon : MovieClipVS;
//		
//		/**
//		* Dados do save que foi carregado ou <code>null</code> caso estejamos jogando a partir do início 
//		*/		
//		private var _loadedData : GameData;
//		
//		private var _backFromMinigameWorkaround : ScheduledTask;
//		private var _mapLoadedScriptLink : ScheduledTask;
//		private var _tempTileMapIndex : int;
//		private var _tempTileMapOnDone : Function;
//		private var _tempTileMapParams : Array;
//		
//		/**
//		* Singleton da classe 
//		*/		
//		private static var _instance : VilaVirtual;
//
//		/**
//		* Construtor
//		* OBS: AS3 não permite construtores privados e protegidos. Logo, apesar de se tratar de uma classe SINGLETON, temos
//		* que manter o construtor público 
//		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript
//	 	*/		
//		public function VilaVirtual( isContinuing : Boolean, characterType : PlayerCharacterType, loadedRawData : ByteArrayEx = null )
//		{
//			// null => Esta classe será a cena. Estamos subvertendo o sistema de composição para usarmos o
//			// sistema de herança! Assim temos que alterar menos código lol (obs: não faça isso em casa)
//			super( null, false, true );
//			
//			if( _instance != null )
//				KillSingleton();
//			
//			_lastGameState = GameState.GAME_STATE_UNDEFINED;
//			_currGameState = GameState.GAME_STATE_LOADING;
//
//			_instance = this;
//
//			trace( "VilaVirtual.construtor( início )" );
//
//			// Só queremos receber input do teclado depois que tudo tiver sido carregado
//			lockInput( true );
//			
//			_movingCamera = false;
//			_cameraMovement  = new Point();
//
//			_questLog = new HashMap();
//			_characters = new HashMap();
//			
//			_lastUpdateTime = 0;
//			_maxUpdateIntervalAllowed = VilaVirtual.MAX_UPDATE_INTERVAL_MILIS;
//			
//			loadSounds();
//			
//			// Cria e configura o componente que irá exibir os textos dos diálogos
//			_talkDialog = new TextDialog( Constants.STAGE_WIDTH, Constants.TEXT_DIALOG_HEIGHT );
//			addChild( _talkDialog );
//			_talkDialog.x = 0.0;
//			_talkDialog.y = Constants.STAGE_HEIGHT - _talkDialog.height; 
//			_talkDialog.visible = false;
//
//			var formatter : TextFormat = new TextFormat();
//			formatter.font = Constants.FONT_NAME_CARTOONERIE;
//			formatter.size = Constants.FONT_TEXT_DIALOG_SIZE;
//			formatter.color = Constants.FONT_COLOR_TEXT;
//			_talkDialog.setBkgColor( false, 0x0 );
//			_talkDialog.setTextFormatter( formatter );
//			
//			var formatterAnswers : TextFormat = new TextFormat();
//			formatterAnswers.font = Constants.FONT_NAME_CARTOONERIE;
//			formatterAnswers.size = Constants.FONT_TEXT_DIALOG_SIZE;
//			formatterAnswers.color = Constants.FONT_COLOR_SELECTED;
//			_talkDialog.setAnswersFormatter( formatterAnswers );
//			
//			// Configura a cena de jogo (deve vir antes das telas de mapa, itens e quests, para que elas usem os valores lidos aqui)
//			loadScripts();
//			
//			mapScreen = new MapScreen( characterType, onMapClose, onIconMouseOver, onIconMouseOut );
//			mapScreen.hide();
//			addChild( mapScreen );
//			
//			_backpack = new Backpack( onMapClose, onIconMouseOver, onIconMouseOut ); 
//			_backpack.hide();
//			addChild( _backpack );
//			
//			questScreen = new QuestScreen( _questLog, onMapClose, onIconMouseOver, onIconMouseOut );
//			questScreen.hide();
//			addChild( questScreen );
//			
//			trafficManager = new TrafficManager();
//			
//			// Carrega os ícones da interface
//			buildInterface();
//			
//			if( isContinuing )
//			{
//				// Carrega os dados salvos. As flags serão setadas apenas após carregarmos o mapa
//				// Ver método onAddedToStage
//				var temp : GameData = new GameData();
//				temp.unserialize( loadedRawData );
//				
//				trace( "Loaded data Flags: " + temp.flags );
//				
//				_loadedData = temp;
//				characterType = _loadedData.characterType;
//						
//				var items : Array = GameScript.instance.items;
//				var itemsIds : Vector.< int > = _loadedData.getItemsIds();
//				var nItems : int = items.length;
//				for each( var itemId : int in itemsIds )
//				{	
//					for( var itemCounter : int = 0; itemCounter < nItems ; ++itemCounter )
//					{
//						if( items[ itemCounter ].id == itemId )
//							putItemIntoBackpack( new Item( items[ itemCounter ] ) );
//					}
//				}
//				
//				var questsIds : Vector.< int > = _loadedData.getQuestsIds();
//				var questsStates : Vector.< QuestState > = _loadedData.getQuestsStates();
//				var nQuests : int = questsIds.length;
//				for( var questCounter : int = 0 ; questCounter < nQuests ; ++questCounter )
//				{
//					var q : Quest = _questLog.find( questsIds[ questCounter] );
//					if( q != null )
//					{
//						// Modifica o estado da quest
//						q.state = questsStates[ questCounter ];
//						
//						if( q.state == QuestState.QUEST_STATE_COMPLETE )
//							_happinessBar.addHappiness( q.xp );
//					}
//				}
//			}
//			else
//			{
//				_loadedData = null;
//			}
//			
//			// Este setter irá inicializar a variável _playerCharacter
//			playerCharacterType = characterType;
//			
//			// Configura o personagem
//			_playerCharacter.setDirection( Direction.DIRECTION_DOWN );
//			_playerCharacter.speed = Constants.CHARACTER_DEFAULT_SPEED;
//			
//			// Só quando formos adicionados ao stage é que poderemos utilizar a propriedade 'stage' do DisplayObject. Para resolver este
//			// problema, utilizamos o evento ADDED_TO_STAGE. Iremos realizar as outras rotinas que deveriam estar no construtor apenas na
//			// callback chamada pelo evento
//			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
//
//			trace( "VilaVirtual -> construtor( final )" );
//		}
//		
//		public static function Seal() : void
//		{
//			_instance.lockInput( true );
//			_instance.onRemovedFromStage( null );
//			_instance.removeEventListener( Event.ADDED_TO_STAGE, _instance.onAddedToStage );
//		}
//		
//		/**
//		* Destrói a única instância da classe 
//		*/		
//		public static function KillSingleton( seal : Boolean = true ) : void
//		{
//			if( seal )
//				Seal();
//
//			_instance = null;
//		}
//		
//		/**
//		* Retorna a única instância da classe 
//		* @return A única instância da classe
//		*/		
//		public static function GetInstance() : VilaVirtual
//		{
//			return _instance;
//		}
//		
//		public static function getMapScreen() : MapScreen {
//			return GetInstance().mapScreen;
//		}
//		
//		
//		/**
//		* Modifica o tilemap atual
//		* @param tileMapIndex O índice do tilemap que desejamos carregar
//		* @param onDone A callback que deve ser chamada quando o tilemap acabar de ser carregado
//		* @param params Os parâmetros que devem ser passados para a callback onDone
//		*/
//		public function setTileMap( tileMapIndex : int, onDone : Function = null, ...params ) : Boolean
//		{
//			trace( "setTileMap( " + tileMapIndex + ", " + onDone + ", " + params );
//			
//			if( _tileMap != null && _tileMap.tileMapId == tileMapIndex )
//				return true;
//			
//			// Na primeira vez que executamos este método, estaremos carregando o jogo. Já
//			// possuiremos um listener para ADDED_TO_STAGE (setado no construtor), então não
//			// podemos executar as linhas abaixo, pois chamaríamos a callback onAddedToStage 2x.
//			// Quando o jogo está carregando, a transição de tela já executou e a barra de progresso
//			// já está visível, então não devemos chamar setState
//			if( gameState != GameState.GAME_STATE_LOADING )
//			{
//				_tempTileMapIndex = tileMapIndex;
//				_tempTileMapOnDone = onDone;
//				_tempTileMapParams = params;
//					
//				addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
//				Application.GetInstance().setState( ApplicationState.APP_STATE_BACK_TO_GAME );
//			}
//			else
//			{	
//				startTileMapLoading( tileMapIndex, onDone, params );
//			}
//			return false;
//		}
//		
//		/**
//		* Inicia o carregamento de um tilemap
//		* @param tileMapIndex O índice do tilemap que desejamos carregar
//		* @param onDone A callback que deve ser chamada quando o tilemap acabar de ser carregado
//		* @param params Os parâmetros que devem ser passados para a callback onDone
//		*/		
//		private function startTileMapLoading( tileMapIndex : int, onDone : Function = null, params : Array = null ) : void
//		{
//			if( _tileMap != null )
//			{
//				removeChild( _tileMap );
//
//				if( currentBkg == _tileMap )
//					currentBkg = null;
//				
//				_tileMap = null;
//			}
//			
//			removeAllCharacters();
//			_tileMap = new TiledEditorTileMap( tileMapIndex, Constants.LAYERS_BLOCKS_SIZE, Constants.LAYERS_BLOCKS_SIZE );
//			_tileMap.addEventListener( Event.COMPLETE, onMapLoaded );
//			_tileMap.addEventListener( ProgressEvent.PROGRESS, onLoadingProgressChanged );
//			
//			onTileMapLoadedCallback = onDone;
//			onTileMapLoadedCallbackParams = params;
//		}
//		
//		/**
//		* Evento chamado quando este DisplayObject é adicionado ao stage
//		* @param evt Objeto que encapsula os dados do evento
//		*/		
//		private function onAddedToStage( e : Event ) : void
//		{
//			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
//			
//			addEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
//
//			stage.align = StageAlign.TOP_LEFT;
//			stage.scaleMode = StageScaleMode.NO_SCALE;
//			stage.quality = StageQuality.HIGH;
//			stage.displayState = StageDisplayState.NORMAL;
//			
//			stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
//			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
//			
//			_gameLoopTimer = new Timer( TIMER_INTERVAL_MILIS );
//			_gameLoopTimer.addEventListener( TimerEvent.TIMER, gameLoop );
//
//			mouseEnabled = true;
//			mouseChildren = true;
//			
//			if( gameState == GameState.GAME_STATE_MINIGAME )
//			{
//				// OLD: Não podemos fazer isso aqui, senão estaremos mudando uma disparando o evento de cena completa
//				// antes de application estar esperando por ele
//				//dispatchEvent( new Event( APPSCENE_LOADING_COMPLETE ) );
//				
//				_backFromMinigameWorkaround = new ScheduledTask( 50, onBackFromMinigameWorkaround );
//				_backFromMinigameWorkaround.call();
//			}
//			else
//			{
//				// 1o mapa
//				if( tileMap == null )
//				{
//					// Se estamos carregando um jogo
//					if( _loadedData != null )
//					{
//						GameScript.instance.prepareToPart( _loadedData.part, _loadedData.tilemapIndex, true );
//						GameScript.instance.vars = _loadedData.flags;
//					}
//					else
//					{
//						GameScript.instance.prepareToPart( GameScript.PARTE_1, Constants.TILEMAP_ORIGINAL );
//					}
//				}
//				else
//				{
//					startTileMapLoading( _tempTileMapIndex, _tempTileMapOnDone, _tempTileMapParams );
//					
//					_tempTileMapIndex = Constants.TILEMAP_NONE;
//					_tempTileMapOnDone = null;
//					_tempTileMapParams = null;
//				}
//			}
//		}
//		
//		private function onBackFromMinigameWorkaround() : void
//		{
//			_backFromMinigameWorkaround = null;
//			dispatchEvent( new Event( APPSCENE_LOADING_COMPLETE ) );
//		}
//		
//		private function onRemovedFromStage( e : Event ) : void
//		{
//			if( _gameLoopTimer )
//			{
//				_gameLoopTimer.stop();
//				_gameLoopTimer.removeEventListener( TimerEvent.TIMER, gameLoop );
//				_gameLoopTimer = null;
//			}
//			
//			mouseEnabled = false;
//			mouseChildren = false;
//
//			stage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
//			stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
//
//			removeEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
//			
//			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
//		}
//		
//		/**
//		* Evento que informa quando o progresso de carregamento do arquivo SWF muda
//		* @param e Objeto que encapsula os dados do evento
//		*/		
//		private function onLoadingProgressChanged( e : ProgressEvent ) : void
//		{
//			dispatchEvent( e );
//		}
//
//		/**
//		* Tilemap atual 
//		*/
//		public function get tileMap() : TiledEditorTileMap
//		{
//			return _tileMap;
//		}
//
//		/**
//		* Lista de personagens do jogo. A chave é seu character
//		*/
//		public function get characters() : HashMap
//		{
//			return _characters;
//		}
//		
//		/**
//		* Carrega os sons utilizados no jogo 
//		*/		
//		public function loadSounds() : void
//		{			
//			for ( var i : uint = 0; i < Constants.SOUNDS_TOTAL; ++i ) {
//				var c : Class = Class( getDefinitionByName( "Snd" + i ) );
//				SoundManager.GetInstance().pushSound( new c() as Sound );
//			}
//		}
//		
//		/**
//		* Indica qual foi o resultado do minigame
//		* @param minigameAppState O estado da aplicação que representa o minigame jogado. Assim podemos saber a que minigame os outros parâmetros correspondem
//		* @param gameCompleted <code>true</code> se o jogador terminou o minigame, <code>false</code> caso contrário
//		* @param score O score feito pelo usuário ou -1 caso o usuário tenha abortado o minigame sem realizar uma partida
//		*/		
//		public function setMinigameResult( minigameAppState : ApplicationState, gameCompleted : Boolean, score : int ) : void
//		{
//			GameScript.instance.onMiniGameEnd( minigameAppState, gameCompleted, score );
//		}
//
//		/**
//		* Método chamado assim que o conseguirmos carregar o arquivo que descreve o tilemap do jogo 
//		* @param e Os dados do evento 
//		*/		
//		private function onMapLoaded( e : Event ) : void
//		{
//			trace( "Map loaded" );
//			
//			_tileMap.removeEventListener( Event.COMPLETE, onMapLoaded );
//			_tileMap.removeEventListener( ProgressEvent.PROGRESS, onLoadingProgressChanged );
//
//			// Coloca o mapa na cena
//			_tileMap.blendMode = BlendMode.LAYER;
//			_tileMap.x = 0;
//			_tileMap.y = 0;
//			_tileMapVisibleAreaSet = false;
//			
//			trafficManager.insertCars( _tileMap );
//			
//			var initPortal : ITile;
//
//			
//			// Cria a câmera do jogo
//			_gameCamera = new Camera( Constants.STAGE_WIDTH, Constants.STAGE_HEIGHT, _tileMap );
//			
//			// Se estamos carregando um jogo...
//			if( _loadedData != null )
//			{
//				// Posiciona os personagens
//				var charactersData : Vector.< CharacterSaveInfo > = _loadedData.getCharacters();
//				for each( var charData : CharacterSaveInfo in charactersData )
//				{
//					var c : Character = ( charData.characterId == GameScript.CHARACTER_JOGADOR ? _playerCharacter : _characters.find( charData.characterId ) );
//					if( c )
//					{
//						var tile : ITile = tileMap.getTile( 0, charData.row, charData.column );
//						tile.insertTileObj( c, true );
//						
//						c.movementState = charData.movementState;
//						c.expression = charData.expression;
//						
//						trace( ">>> Loading character " + charData.characterId + " into tile [" + charData.row + "," + charData.column + "]" );
//					}
//				}
//				
//				// Garante que tudo está ok para esta posição do personagem
//				resetVisibleArea();
//				onPlayerCharacterTileChanged( null );
//				
//				// Não temos mais nada para carregar, então libera a memória ocupada por _loadedData
//				_loadedData = null;
//			}
//
//			// Agenda uma chamada assíncrona para avisar ao script que carregamos o tilemap
//			if( onTileMapLoadedCallback != null )
//			{
//				_mapLoadedScriptLink = new ScheduledTask( 50, onMapLoadedScriptLink );
//				_mapLoadedScriptLink.call();
//			}
//
//			// Avisa ao pai que acabamos de carregar a cena
//			gameState = GameState.GAME_STATE_IDLE;
//			dispatchEvent( new Event( APPSCENE_LOADING_COMPLETE ) );
//		}
//		
//		/**
//		* Callback que avisa ao sistema de script que carregamos o tilemap 
//		*/		
//		public function onMapLoadedScriptLink() : void
//		{
//			_mapLoadedScriptLink = null;
//
//			if( onTileMapLoadedCallback != null )
//			{
//				onTileMapLoadedCallback.apply( GameScript.instance, onTileMapLoadedCallbackParams[ 0 ] );
//				onTileMapLoadedCallback = null;
//				onTileMapLoadedCallbackParams = null;
//			}			
//		}
//		
//		/**
//		* Inicia o looping de atualização do jogo  
//		*/		
//		public function startRunning() : void
//		{
//			if( gameState == GameState.GAME_STATE_MINIGAME )
//			{
//				loadSounds();
//				gameState = _lastGameState;	
//				GameScript.instance.nextGameEvent();
//			}
//			else if( gameState == GameState.GAME_STATE_IDLE )
//			{
//				gameState = GameState.GAME_STATE_MOVING;
//			}
//
//			// Inicia o looping do jogo
//			_gameLoopTimer.start();
//		}
//		
//		public function showMiniGame( miniGameIndex : uint ) : void
//		{
//			gameState = GameState.GAME_STATE_MINIGAME;	
//		}
//		
//		/**
//		* Cria os ícones da interface 
//		*/		
//		private function buildInterface() : void
//		{
//			// Cria os ícones do menu
//			var icons : Vector.< MovieClip > = new Vector.< MovieClip >();
//			
//			_iconBackpack = new MochilaBase();
//			_iconBackpack.addEventListener( MouseEvent.CLICK, onIconBackpackClicked );
//			
//			_iconMap = new MapaBase();
//			_iconMap.addEventListener( MouseEvent.CLICK, onIconMapClicked );
//			
//			_iconBook = new LivroBase();
//			_iconBook.addEventListener( MouseEvent.CLICK, onIconBookClicked );
//			
//			_iconMenu = new MenuBase();
//			_iconMenu.addEventListener( MouseEvent.CLICK, onIconMenuClicked );
//			
//			menuTop = new MovieClipVS();
//			menuTop.mouseEnabled = true;
//			menuTop.mouseChildren = true;
//			menuTop.visible = false;
//			addChild( menuTop );
//			
//			// Posiciona os ícones do menu
//			icons.push( _iconBackpack, _iconBook, _iconMap, _iconMenu );
//			var startPosX : int = Constants.STAGE_WIDTH - Constants.INTERFACE_ICONS_DIST_FROM_RIGHT;
//			for( var i : int = icons.length - 1 ; i >= 0 ; --i )
//			{
//				menuTop.addChild( icons[i] );
//				
//				icons[i].addEventListener( MouseEvent.MOUSE_OVER, onIconMouseOver );
//				icons[i].addEventListener( MouseEvent.MOUSE_OUT, onIconMouseOut );
//				
//				icons[i].buttonMode = true;
//				icons[i].mouseChildren = false;
//				icons[i].cacheAsBitmap = false;
//
//				icons[i].x = startPosX - icons[i].width
//				icons[i].y = Constants.INTERFACE_ICONS_DIST_FROM_TOP;
//				startPosX = icons[i].x - Constants.INTERFACE_ICONS_SPACE_BETWEEN; 
//			}
//			
//			// Cria e posiciona a barra de felicidade
//			_happinessBar = new HappinessBar( 0, questScreen.getTotalXP() );
//			menuTop.addChild( _happinessBar );
//			
//			_happinessBar.x = Constants.INTERFACE_HAPPINESS_BAR_DIST_FROM_LEFT;
//			_happinessBar.y = Constants.INTERFACE_HAPPINESS_BAR_DIST_FROM_TOP;
//		}
//		
//		/**
//		* Retorna se a cena dispara eventos de progresso quando realiza carregamento assíncrono
//		* @return Se a cena dispara eventos de progresso quando realiza carregamento assíncrono
//		*/		
//		override public function get usesProgressEvent() : Boolean
//		{
//			// Se está voltando de um minigame, significa que o jogo já está carregado, então não
//			// precisaremos de eventos de progresso
//			trace( "GameState = " + gameState );
//			if( gameState == GameState.GAME_STATE_MINIGAME )
//				return false;
//
//			return true;
//		}
//		
//		private function onIconBackpackClicked( e : MouseEvent ) : void
//		{
//			gameState = GameState.GAME_STATE_ITEM_SCREEN;
//		}
//		
//		private function onIconMapClicked( e : MouseEvent ) : void
//		{
//			gameState = GameState.GAME_STATE_MAP;
//		}
//		
//		private function onIconBookClicked( e : MouseEvent ) : void
//		{
//			gameState = GameState.GAME_STATE_QUEST_SCREEN;
//		}
//		
//		private function onIconMenuClicked( e : MouseEvent ) : void
//		{
//			gameState = GameState.GAME_STATE_QUIT_POPUP;
//			mouseEnabled = false;
//			mouseChildren = false;
//
//			// Se o jogador está logado, deixaremos ele salvar o jogo
//			var app : Application = Application.GetInstance()
//				
//			// OLD
//			//app.showPopUp( "Deseja sair do jogo?", "Sim", ( app.getLoggedProfile() != null ? onSaveGamePopUp : onQuit ), "Não", onContinuePlaying );
//
//			if( app.getLoggedProfile() == null )
//			{
//				app.showPopUp( "Deseja sair do jogo?", "Sim", onQuit, "Não", onContinuePlaying );
//			}
//			else
//			{
//				app.showPopUp( "O que você deseja fazer?", "Salvar", onSaveGame, "Salvar e Sair", onSaveAndQuit, "Sair", onQuit, "Cancelar", onContinuePlaying );
//			}
//		}
//		
//		private function onSaveAndQuit() : void
//		{
//			gameState = GameState.GAME_STATE_SAVE_N_QUIT;
//			
//			save( this );
//			Application.GetInstance().showWaitFeedback();
//		}
//		
//		private function onSaveGame() : void
//		{
//			gameState = GameState.GAME_STATE_SAVING
//			
//			save( this );
//			Application.GetInstance().showWaitFeedback();
//		}
//		
//		public function save( listener : INOListener ) : void
//		{
//			var dataToSave : ByteArrayEx = new ByteArrayEx();
//			packGameData().serialize( dataToSave );
//			
//			// Envia a requisição
//			var temp : NOAppCustomerData = new NOAppCustomerData( Application.GetInstance().getLoggedProfile().profileId, dataToSave );
//			NOAppCustomerData.SaveData( temp, listener );	
//		}
//		
//		private function onQuit() : void
//		{
//			backToMainMenu();
//		}
//
//		private function onContinuePlaying() : void
//		{
//			mouseEnabled = true;
//			mouseChildren = true;
//			gameState = GameState.GAME_STATE_MOVING;
//		}
//		
//		private function onMapClose( e : MouseEvent ) : void
//		{
//			gameState = GameState.GAME_STATE_MOVING;
//		}
//		
//		private function onIconMouseOver( e : MouseEvent ) : void
//		{
//			var interfaceIcon : MovieClip = ( e.target as MovieClip );
//			interfaceIcon.scaleX = 1.2;
//			interfaceIcon.scaleY = 1.2;
//		}
//		
//		private function onIconMouseOut( e : MouseEvent ) : void
//		{
//			var interfaceIcon : MovieClip = ( e.target as MovieClip );
//			interfaceIcon.scaleX = 1.0;
//			interfaceIcon.scaleY = 1.0;
//		}
//		
//		/**
//		* Carrega o script do jogo, com descrições dos itens, personagens, quests, etc.
//		*/
//		private function loadScripts() : void
//		{
//			var reader : GameScript = new GameScript();
//			var c : Class;
//			
//			// cria os objetos dos personagens, de acordo com os nomes de classes informados no script (flashClass)
//			for each( var character : Object in reader.characters )
//			{
//				trace( "loadScripts: loading character: \n\tid: " + character.id + "\n\tname: " + character.name + "\n\tflash class: " + character.flashClass );
//				c = Class( getDefinitionByName( character.flashClass ) );
//				
//				var characterObj : Character;
//				if ( character.hasOwnProperty( 'type' ) && character.type == 'car' )
//				{
//					characterObj = new Car( character.id, new c() as MovieClipVS, reader.onTalk );
//				}
//				else
//				{
//					var tempMC : MovieClipVS = new c() as MovieClipVS;
//					
//					// Gambiarra para consertar o posicionamento de alguns movieclips!!!!!
//					if( ( tempMC is NPCman1BaseRmk ) || ( tempMC is NPCman2BaseRmk ) || ( tempMC is NPCman3BaseRmk ) || ( tempMC is NPCwoman1BaseRmk ) || ( tempMC is NPCwoman2BaseRmk ) )
//					{
//						characterObj = new Character( character.id, tempMC, reader.onTalk, Constants.CHARACTER_DEFAULT_FPS, 5, ( tempMC.height * 0.85 ) );
//					}
//					else
//					{
//						characterObj = new Character( character.id, tempMC, reader.onTalk );
//					}
//				}
//
//				if ( character.hasOwnProperty( "face" ) ) {
//					var faceClass : Class = Class( getDefinitionByName( character.face ) );
//					characterObj.characterFaceImg = new faceClass() as MovieClipVS;
//				}
//				characters.insert( character.id, characterObj );
//				trace( "... ok" );
//			}
//
//			// cria os objetos dos items, de acordo com os nomes de classes informados no script (flashClass)
//			for each( var quest : Object in reader.quests )
//			{
//				trace( "loadScripts: loading quest:\n\tid: " + quest.id + "\n\ttitle: " + quest.title + "\n\tdescription: " + quest.description + "\n\txp: " + quest.xp );
//				_questLog.insert( quest.id, new Quest( quest.id, quest.title, quest.description, quest.xp ) );
//				trace( "... ok" );
//			}
//		}
//		
//		
//		/**
//		* Exibe o balão de diálogo e começa a exibir o texto, caractere a caractere
//		* @param text Texto que será exibido
//		* @param talkerImg Imagem do personagem que vai responder ao evento de diálogo
//		* @param talkStarterFacingDir Direção para qual o evento de check foi enviado
//		* @param onTalkEndedCallback Callback que deve ser chamada quando o diálogo terminar
//		*/		
//		public function startTalk( text : String, talkerImg : MovieClipVS, voiceIndex : uint, talkStarterFacingDir : Direction, onTalkEndedCallback : Function, questions : Array, embedFonts : Boolean = true ) : void
//		{
//			gameState = GameState.GAME_STATE_TALKING;
//			showTextDialog( text, talkerImg, voiceIndex, talkStarterFacingDir, onTalkEndedCallback, questions, embedFonts );
//		}
//		
//		/**
//		* Termina a conversa iniciada pela última chamada de startTalk 
//		*/		
//		public function endTalk() : void
//		{
//			hideTextDialog();
//		}
//		
//		/**
//		* Esconde o balão de diálogo, caso haja algum visível, e retorna para o estado anterior do jogo 
//		*/		
//		private function hideTextDialog() : void
//		{
//			// Restaura o estado anterior do jogo
////			gameState = _lastGameState; TODO
//			if ( currentBkg == _tileMap )
//				gameState = GameState.GAME_STATE_MOVING;
//			
//			// Fecha a caixa de diálogo, confirmando a resposta atual, caso haja
//			_talkDialog.hide();
//		}
//		
//		/**
//		* Exibe a caixa de diálogo 
//		* @param text Texto que será exibido
//		* @param talkerImg Imagem do personagem que vai responder ao evento de diálogo
//		* @param talkStarterFacingDir Direção para qual o evento de check foi enviado
//		* @param onTalkEndedCallback Callback que deve ser chamada quando o diálogo terminar
//		*/			
//		private function showTextDialog( text : String, talkerImg : MovieClipVS = null, voiceIndex : uint = Constants.SOUND_FX_SPEECH_MALE, talkStarterFacingDir : Direction = null, onTalkEndedCallback : Function = null, questions : Array = null, embedFonts : Boolean = true ) : void
//		{
//			// Verifica se o rosto do personagem que está falando deve aparecer à esquerda ou à direita do balão
//			var atLeft : Boolean = true;
//			if( talkerImg != null && talkStarterFacingDir != null )
//			{
//				switch( talkStarterFacingDir )
//				{
//					case Direction.DIRECTION_UP:
//					case Direction.DIRECTION_LEFT:
//						atLeft = false;
//						break;
//					
//					// O valor de inicialização de atLeft já é true
////					case Direction.DIRECTION_DOWN:
////					case Direction.DIRECTION_RIGHT:
////					default:
////						atLeft = true;
////						break;
//				}
//			}
//			
//			_talkDialog.setTextSpeed( Constants.TEXT_SPEED_SLOW );
//			_talkDialog.setTalkerImg( talkerImg, atLeft );
//			_talkDialog.setText( text, onTalkEndedCallback, questions, embedFonts );
//			_talkDialog.setVoice( voiceIndex );
//			_talkDialog.show();
//		}
//		
//		/**
//		* Coloca o item encontrado no container de itens e exibe para o jogador uma caixa de texto indicando qual foi o item obtido
//		* @param newItem O item que recebeu o evento de check
//		*/		
//		public function putItemIntoBackpack( newItem : Item ) : void
//		{
//			// Insere o item na mochila
//			_backpack.insertItem( newItem );
//
//			// Toca o som de "pegou item"
//			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
//			
//			// Exibe uma caixa de diálogo para informar ao usuário qual item ele pegou
////			showTextDialog( "Você ganhou " + newItem.name ); TODO
//		}
//		
//		// Variáveis de controle do fade
//		private var fadeScreenCallback : Function = null;
//		private var fadeScreenScheduler : ScheduledTask = null;
//		private var fadeTweener : Tween = null;
//		private var fadeTimerWorkaround : Timer = null;
//		
//		
//		/**
//		 * Realiza uma animação de esmaecimento da tela.
//		 * 
//		 * @param appear indica se é uma animação de fade in (true) ou fade out (false).
//		 * @param time duração da animação de esmaecimento (em segundos).
//		 * @param onDone função a ser chamada quando a animação de esmaecimento acabar. 
//		 */
//		public function fadeScreen( appear : Boolean, time: Number = Constants.FADE_TIME_DEFAULT, onDone : Function = null ) : void
//		{
//			trace( "\n--------------------------------------\n" );
//			trace( "Iniciando tweener de " + ( appear ? "fadeIn" : "fadeOut" ) );
//			trace( "VilaVirtual::fadeScreen -> bkg = " + currentBkg );
//			trace( "VilaVirtual::fadeScreen -> time = " + time );
//			trace( "VilaVirtual::fadeScreen -> onDoneCallback = " + onDone );
//			trace( "\n--------------------------------------\n" );
//			
//			if( fadeTweener != null )
//			{
//				trace( "Oooooooooops! Erro!!! Dois fades ocorrendo ao mesmo tempo!!!" );
//				deleteFader();
//			}
//			
//			fadeScreenCallback = onDone;
//
//			if( currentBkg && time > 0 )
//			{
//				currentBkg.blendMode = BlendMode.LAYER;
//				currentBkg.alpha = appear ? 0 : 1;
//
//				fadeTweener = new Tween( currentBkg, "alpha", Regular.easeInOut, currentBkg.alpha, appear ? 1.0 : 0.0, time, true );
//				fadeTweener.addEventListener( TweenEvent.MOTION_FINISH, fadeScreenEnd );
//				fadeTweener.start();
//				
//				// Algumas vezes o fade trava. Colocamos este timer para contornar a situação, pulando o efeito de fade
//				fadeTimerWorkaround = new Timer( ( time * 1000 ) * 2 );
//				fadeTimerWorkaround.addEventListener( TimerEvent.TIMER, fadeScreenWorkaround );
//				fadeTimerWorkaround.start();
//			}
//			// Classe Tween não se entende bem com animação de tempo zero
//			else
//			{
//				if( currentBkg )
//					currentBkg.alpha = appear ? 1.0 : 0.0;
//
//				fadeScreenEnd( null );
//			}
// 		}
//		
//		/**
//		* Algumas vezes o fade trava. Colocamos este método para contornar a situação, pulando o efeito de fade
//		* @param e Objeto que encapdula os dados do evento 
//		*/		
//		private function fadeScreenWorkaround( e : TimerEvent ) : void
//		{
//			trace( "\n---------------------------------------\n" );
//			trace( "Tween workaround" );
//			trace( "\n---------------------------------------\n" );
//
//			// "Termina" a animação
//			currentBkg.alpha = fadeTweener.finish;
//			
//			// Chama a callback de fim de animação
//			fadeScreenEnd( null );
//		}
//		
//		private function deleteFader() : void
//		{
//			if( fadeTweener != null )
//			{
//				fadeTweener.stop();
//				fadeTweener.removeEventListener( TweenEvent.MOTION_FINISH, fadeScreenEnd );
//				fadeTweener = null;
//			}
//			
//			if( fadeTimerWorkaround != null )
//			{
//				fadeTimerWorkaround.stop();
//				fadeTimerWorkaround.removeEventListener( TimerEvent.TIMER, fadeScreenWorkaround );
//				fadeTimerWorkaround = null;
//			}
//		}
//		
//		private function fadeScreenEnd( e : TweenEvent ) : void
//		{
//			trace( "\n------------------------------------\n" );
//			trace( "Fim de fade screen" );
//			trace( "\n------------------------------------\n" );
//			
//			deleteFader();
//
//			if( currentBkg != null )
//				currentBkg.blendMode = BlendMode.LAYER;
//				
//			if( fadeScreenCallback != null )
//			{
//				fadeScreenScheduler = new ScheduledTask( 0.05, fadeScreenCallback );
//				fadeScreenCallback = null;
//				fadeScreenScheduler.call();
//			}
//		}
//		
//		/**
//		* Inicia uma nova quest
//		* @param quest Os dados sobre a nova quest
//		*/		
//		public function startQuest( questId : int ) : void
//		{
//			var quest : Quest = _questLog.find( questId );
//			
//			// Modifica o estado da quest
//			quest.state = QuestState.QUEST_STATE_INCOMPLETE;
//			
//			// Toca o som de nova missão
//			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_FX_GOT_ITEM );
//		}
//		
//		/**
//		* Retorna o estado de uma quest
//		* @param questID O ID único da quest da qual desejamos o estado
//		* @return O estado da quest cujo ID foi passado como parâmetro
//		*/		
//		public function queryQuest( questID : uint ) : QuestState
//		{
//			const q : Quest = _questLog.find( questID );
//			if( q != null )
//				return q.state;
//			return QuestState.QUEST_STATE_UNDISCOVERED;
//		}
//		
//		/**
//		* Termina a quest cujo ID foi passado como parâmetro
//		* @param questID O ID único da quest que foi terminada
//		*/		
//		public function endQuest( questID : uint ) : void
//		{
//			// TODO : Se as missões sempre existirem, o if será desnecessário
//			const q : Quest = _questLog.find( questID );
//			if( ( q != null ) && ( q.state == QuestState.QUEST_STATE_INCOMPLETE ) )
//			{
//				// Modifica o estado da quest
//				q.state = QuestState.QUEST_STATE_COMPLETE;
//				
//				// Aumenta a felicidade da cidade
//				_happinessBar.addHappiness( q.xp );
//
//				// Toca o som de missão completa
//				SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_MUSIC_QUEST_COMPLETE );
//			}
//		}
//		
//		/**
//		* Informa se devemos tratar ou não os eventos de teclado
//		* @param lock Se true, o objeto pára de receber os eventos de teclado. Se false, o objeto começa a receber e tratar os eventos de teclado 
//		*/		
//		public function lockInput( lock : Boolean ) : void
//		{
//			_keyLocked = lock;
//			
//			if( menuTop != null )
//			{
//				menuTop.mouseEnabled = !lock;
//				menuTop.mouseChildren = !lock;
//				menuTop.visible = !lock;
//			}
//			
//			// OLD: Não queremos parar de receber os eventos de tecla em alguns estados do jogo
////			if( lock )
////			{
////				stage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
////				stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
////			}
////			else
////			{
////				if( !stage.hasEventListener( KeyboardEvent.KEY_UP ) )
////					stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
////				
////				if( !stage.hasEventListener( KeyboardEvent.KEY_DOWN ) )
////					stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
////			}
//		}
//		
//		/**
//		* Faz com que a posição passada como parâmetro seja focalizada no centro da tela
//		* @param worldPos A posição que a câmera deve focalizar
//		* @param time O tempo que a câmera demora transladando do foco atual até o próximo foco. Tempos <= 0 fazem a transação imediatamente  
//		*/		
//		public function moveCameraToPos( worldPos : WorldPos, time : Number = 0.0 ) : void
//		{
//			// TODO : Descobrir pq a gambiarra abaixo só funciona da 1a vez (este código é chamado em onMapLoaded)
//			
//			// TODO : Retirar quando descobrirmos porque o trecho de código comentado abaixo impede que o personagem ande
//			// As duas linhas abaixo quebram o galho por enquanto
//			var finalCameraPos : Point = IsoGlobals.worldCoordToPixelCoord( worldPos );
//			_gameCamera.setPosition( finalCameraPos );
//
//			// Configura e inicia a animação da câmera
////			var finalCameraPos : Point = IsoGlobals.worldCoordToPixelCoord( worldPos );
////			
////			const worldWidth : Number = _tileMap.mapNColumns * _tileMap.mapTileWidth;
////			const worldHeight : Number = _tileMap.mapNRows * _tileMap.mapTileHeight;
////			
////			const halfLensWidth : Number = _gameCamera.getLensWidth() * 0.5;
////			if( finalCameraPos.x < halfLensWidth )
////				finalCameraPos.x = halfLensWidth;
////			else if( finalCameraPos.x > worldWidth - halfLensWidth )
////				finalCameraPos.x = worldWidth - halfLensWidth;
////			
////			const halfLensHeight : Number = _gameCamera.getLensHeight() * 0.5;
////			if( finalCameraPos.y < halfLensHeight )
////				finalCameraPos.y = halfLensHeight;
////			else if( finalCameraPos.y > worldHeight - halfLensHeight )
////				finalCameraPos.y = worldHeight - halfLensHeight;
////			
////			_gameCamera.setAnim( finalCameraPos, time );
////			_gameCamera.startAnim();
////			
////			const currCameraPos : Point = _gameCamera.getPosition();
////			if( ( time <= 0.0 ) || ( currCameraPos.equals( finalCameraPos ) ) )
////				_gameCamera.endAnim();
//		}
//		
//		/**
//		* Faz com que o centro do tile passado como parâmetro seja focalizado no centro da tela
//		* @param tile O tile que a câmera deve focalizar
//		* @param time O tempo que a câmera demora transladando do foco atual até o próximo foco. Tempos <= 0 fazem a transação imediatamente  
//		*/			
//		public function moveCameraToTile( tile : ITile, time : Number = 0.0 ) : void
//		{
//			const tileWorldPos : WorldPos = TileGameUtils.GetTileCenterWorldPos( tile );
//			moveCameraToPos( tileWorldPos, time );
//		}
//		
//		// TODO: Estamos chamando este método na mão ao invés de estarmos utilizando um listener do evento de IsoGameEntity. Temos que verificar
//		// se já podemos fazer da 2a maneira (que é mais limpa e correta) ou se ainda há bugs
//		/**
//		* Evento disparado toda vez que o personagem do jogador muda de posição. Aproveitamos para tratar
//		* o carregamento dinâmico de fileiras e colunas de tiles
//		* @param e O objeto que encapsula os dados do evento 
//		*/
//		private function onPlayerCharacterPosChanged( e : Event = null ) : void
//		{
//			// TODO : Descobrir porque isso não está funcionando!!!!
//			//const tile : ITile = _playerCharacter.tile;
//			//			
//			//if( tile != null )
//			//{
//			//	trace( "Curr player tile => " + tile );
//			//	moveCameraToTile( tile, 0.0 );
//			//	
//			//}
//			//// TODO : Retirar !!!
//			//else
//			//{
//			//	trace( "Curr player tile => null!!!" );
//			//}
//			
//			const destPos : Point = _playerCharacter.pixelPosByAnchor;
//			
//			if( Camera.CAMERA_ANIM_MODE == Camera.CAMERA_ANIM_MODE_MANUAL )
//				_gameCamera.setPosition( destPos );
//			else
//				_gameCamera.setAnim( destPos, Constants.CAMERA_FOLLOW_TIME ); 
//		}
//		
//		// TODO: Estamos chamando este método na mão ao invés de estarmos utilizando um listener do evento de IsoGameEntity. Temos que verificar
//		// se já podemos fazer da 2a maneira (que é mais limpa e correta) ou se ainda há bugs
//		/**
//		* Evento disparado toda vez que o personagem do jogador muda de tile. Aproveitamos para tratar
//		* o carregamento dinâmico de fileiras e colunas de tiles
//		* @param e O objeto que encapsula os dados do evento 
//		*/		
//		public function onPlayerCharacterTileChanged( e : Event = null ) : void
//		{
//			if( ( Constants.COMPILE_VERSION == Constants.COMPILE_VERSION_TEST_MARIO ) || ( !Constants.DYNAMIC_TILE_LOAD_ON ) )
//				return;
//			
//			var currTile : ITile = _playerCharacter.tile;
//			var lastTile : ITile = _playerCharacter.lastTile;
//			
//			trace( "\n--------------------------------\n" ); 
//			trace( ">>>>> Player curr tile = " + currTile );
//			trace( "\n--------------------------------\n" );
//			
//			// Da 1a vez (ou se fizemos uma mudança brusca de posição) temos que realizar o algoritmo pesado
//			if( !_tileMapVisibleAreaSet || ( ( lastTile != null ) && !lastTile.isNeighborOf( currTile ) ) )
//			{
//				_playerCharacter.lastTile = currTile;
//				resetVisibleArea();
//				_tileMapVisibleAreaSet = true;
//			}
//			// Das outras vezes podemos alterar apenas as diferenças conforme o personagem vai andando
//			else if( lastTile != null )
//			{
//				_tileMap.changeVisibleArea( TileGameUtils.DirectionFromMovementState( _playerCharacter.movementState ) );
//			}
//			
//			if( _gameCamera )
//			{
//				onPlayerCharacterPosChanged( null ); 
//			}
//		}
//		
//		public function forceVisibleAreaResetOnUpdate() : void
//		{
//			_tileMapVisibleAreaSet = false;
//		}
//		
//		public function resetVisibleArea() : void
//		{
//			trace( "VilaVirtal::resetVisibleArea => player tile: " + _playerCharacter.tile );
//			trace( "VilaVirtal::resetVisibleArea => player last tile: " + _playerCharacter.lastTile );
//			_tileMap.setVisibleArea( _playerCharacter.tile, Constants.VISIBLE_AREA_INIT_WIDTH_IN_BLOCKS, Constants.VISIBLE_AREA_INIT_HEIGHT_IN_BLOCKS );
//		}
//		
//		// TODO : Estamos focando apenas o personagem. Isto impede que uma movimentação da câmera pelo cenário
//		// utilize o carregamento dinâmico de tiles. No entanto, para esta 1a versão, servirá
//		/**
//		* Evento disparado toda vez que o tile do personagem é alterado. Aproveitamos para tratar
//		* o carregamento dinâmico de fileiras e colunas de tiles 
//		* @param e O objeto que encapsula os dados do evento
//		*/		
////		private function onCameraPosChanged( e : Event ) : void
////		{
////			if( !Constants.DYNAMIC_TILE_LOAD_ON )
////				return;
////			
////			var currTile : ITile = _playerCharacter.tile;
////			var lastTile : ITile = _playerCharacter.lastTile;
////			
////			// Da 1a vez temos que realizar o algoritmo pesado
////			if( !_tileMapVisibleAreaSet || ( ( lastTile != null ) && !lastTile.isNeighborOf( currTile ) ) )
////			{
////				setTileMapVisibleArea( currTile );
////			}
////			// Das outras vezes podemos alterar apenas as diferenças conforme o personagem vai andando
////			else
////			{
////				// Verifica a diferença entre tiles
////				_tileMap.changeVisibleArea( TileGameUtils.DirectionFromPath( lastTile, currTile ) );
////			}
////		}
//		
//		/**
//		* Retorna a posição da cena do jogo na tela
//		*/		
//		public function getScenePos() : Point
//		{
//			// TODO : Deletar este método!!!!
//			//return new Point( _tileMap.x, _tileMap.y );
//			return new Point( 0.0, 0.0 );
//		}
//		
//		
//		public function setIconBlinking( iconId : int, blink : Boolean ) : void {
//			trace( "----------> SET ICON: " + iconId + " , " + blink );
//			var icon : MovieClipVS = null;
//			
//			switch ( iconId ) {
//				case Constants.ICON_BACKPACK:
//					icon = _iconBackpack;
//					break;
//				case Constants.ICON_MAP:
//					icon = _iconMap;
//					break;
//				case Constants.ICON_HAPPINESS_BAR:
//					icon = _happinessBar;
//					break;
//				case Constants.ICON_CELL_PHONE:
//					icon = _iconMenu;
//					break;
//				case Constants.ICON_QUEST_BOOK:
//					icon = _iconBook;
//					break;
//				default:
//					return;
//			}
//			
//			if ( blink ) {
//				if ( currentIcon ) {
//					currentIcon.scale = 1.0;
//					deleteIconBlinkTween();
//				}
//				currentIcon = icon;
//				menuTop.visible = true;
//				blinkIcon( currentIcon.scale <= 1.0 );
//			} else {
//				icon.scale = 1.0;
//				currentIcon = null;
//				deleteIconBlinkTween();
//			}
//		}
//		
//		
//		/**
//		 * Realiza uma animação de esmaecimento da tela.
//		 * 
//		 * @param appear indica se é uma animação de fade in (true) ou fade out (false).
//		 * @param time duração da animação de esmaecimento (em segundos).
//		 * @param onDone função a ser chamada quando a animação de esmaecimento acabar. 
//		 */
//		public function blinkIcon( appear : Boolean, time: Number = Constants.ICON_BLINK_TIME_DEFAULT ) : void
//		{
//			trace( "----------> BLINK: " + appear + ", " + time );
//			if( iconBlinkTweener != null )
//			{
//				deleteIconBlinkTween();
//			}
//			
//			iconBlinkTweener = new Tween( currentIcon, "scale", Regular.easeInOut, currentIcon.scale, appear ? 1.15 : 0.9, time, true );
//			iconBlinkTweener.addEventListener( TweenEvent.MOTION_FINISH, blinkIconEnd );
//			iconBlinkTweener.start();
//		}
//		
//		private function deleteIconBlinkTween() : void
//		{
//			trace( "----------> DELETE" );
//			if( iconBlinkTweener != null )
//			{
//				iconBlinkTweener.stop();
//				iconBlinkTweener.removeEventListener( TweenEvent.MOTION_FINISH, blinkIconEnd );
//				iconBlinkTweener = null;
//				
//				if ( currentIcon != null ) {
////					currentIcon.scale = 1.0;
//				}
//			}
//		}
//		
//		
//		private function blinkIconEnd( e : TweenEvent ) : void
//		{
//			trace( "----------> END" );
//			deleteIconBlinkTween();
//			
//			if( currentIcon != null )
//				blinkIcon( currentIcon.scaleX <= 1.0 );
//			else
//				currentIcon = null;
//		}		
//		
//		
//		/**
//		* Determina o estado do jogo
//		* @param state O novo estado do jogo 
//		*/		
//		private function set gameState( state : GameState ) : void
//		{
//			// Armazena o último estado
//			_lastGameState = gameState;
//			_lastGameStateTime = getTimer();
//			
//			// Configura o novo estado
//			switch( state )
//			{
//				case GameState.GAME_STATE_MOVING:
//					menuTop.visible = true;
//					mapScreen.hide();
//					_backpack.hide();
//					questScreen.hide();
//					_tileMap.alpha = 1;
//					break
//				
//				case GameState.GAME_STATE_TALKING:
//					menuTop.visible = ( currentIcon != null );
//					break;
//				
//				case GameState.GAME_STATE_ITEM_SCREEN:
//					menuTop.visible = false;
//					mapScreen.hide();
//					_backpack.show();
//					questScreen.hide();
//					_tileMap.alpha = 0.3;
//					break;
//
//				case GameState.GAME_STATE_MAP:
//					menuTop.visible = false;
//					mapScreen.show();
//					_backpack.hide();
//					questScreen.hide();
//					_tileMap.alpha = 0.3;
//					break;
//
//				case GameState.GAME_STATE_QUEST_SCREEN:
//					menuTop.visible = false;
//					mapScreen.hide();
//					_backpack.hide();
//					questScreen.show();
//					_tileMap.alpha = 0.3;
//					break;
//
//				case GameState.GAME_STATE_UNDEFINED:
//				case GameState.GAME_STATE_LOADING:
//				case GameState.GAME_STATE_IDLE:
//				case GameState.GAME_STATE_MINIGAME:
//					break;
//			}
//			
//			// Altera o estado atual do jogo
//			_currGameState = state;
//		}
//		
//		/**
//		* Retorna o estado atual do jogo
//		* @return O estado atual do jogo 
//		*/		
//		private function get gameState() : GameState
//		{
//			return _currGameState;
//		}	
//		
//		// TODO
////		private var _fps : Number = 0;
////		private var _fpsTimeCounter : Number = 0;
//		
//		/**
//		* Looping do jogo. Atualiza objetos e trata colisões. A renderização é feita pela AVM. 
//		* @param e Dados sobre o evento do timer que controla o looping
//		*/		
//		private function gameLoop( e : TimerEvent ) : void
//		{
//			if(	_lastUpdateTime == 0 )
//				_lastUpdateTime = getTimer();
//			
//			// Calcula o tempo transcorrido real
//			var currTime : int = getTimer();
//			var timeElapsed : int = currTime - _lastUpdateTime;
//			
//			// TODO
////			_fpsTimeCounter += timeElapsed;
////			if( _fpsTimeCounter > 1.0 )
////			{
////				trace( "FPS : " + _fps / _fpsTimeCounter );
////				_fpsTimeCounter = 0.0;
////			}
//			
//			if( timeElapsed > _maxUpdateIntervalAllowed )
//				timeElapsed = _maxUpdateIntervalAllowed;
//			
//			// Atualiza os objetos da cena
//			update( ( timeElapsed as Number ) / 1000.0 );
//			
//			_lastUpdateTime = getTimer();
//		}
//		
//		
//		
//		/**
//		* Atualiza os objetos da cena do jogo
//		* @param timeElapsed Tempo transcorrido dede a última atualização 
//		*/
//		private function update( timeElapsed : Number ) : void
//		{
//			// trace( "TimeElapsed: " + timeElapsed );
//			
//			// TODO : Seria melhor e mais otimizado se utilizássemos o padrão STRATEGY. Poderíamos armazenar os métodos
//			// de atualização em ponteiros para função / functors e modificar o método de atualização atual apenas alterando
//			// o valor de uma variável em setState. Assim não teríamos que executar este switch toda vez qua chamamos update
//			switch( gameState )
//			{
//				case GameState.GAME_STATE_MOVING:
//					updateStateMoving( timeElapsed );
//					break;
//				
//				case GameState.GAME_STATE_TALKING:
//				case GameState.GAME_STATE_ITEM_SCREEN:
//					updateStateTalking( timeElapsed );
//					break;
//			}
//		}
//
//		/**
//		* Atualiza os objetos da cena do jogo quando estamos no estado GAME_STATE_MOVING
//		* @param timeElapsed Tempo transcorrido dede a última atualização 
//		*/
//		private function updateStateMoving( timeElapsed : Number ) : void
//		{
//			// Atualiza a câmera
//			if( _movingCamera )
//			{
//				_gameCamera.move( _cameraMovement.x, _cameraMovement.y );
//			}
//			else
//			{
//				_gameCamera.update( timeElapsed );
//			}
//			
//			trafficManager.update( timeElapsed );
//
//			// Atualiza os objetos das layers
//			const nLayers : uint = _tileMap.nLayers;
//			for( var layerIndex : uint = 0 ; layerIndex < nLayers ; ++layerIndex )
//			{
//				var layer : ITileLayer = _tileMap.getLayer( layerIndex );
//				var layerObjs : Vector.< ITileObject > = layer.dynamicObjects;
//				var nObjs : uint = layerObjs.length;
//
//				for( var i : uint = 0 ; i < nObjs ; ++i )
//				{
//					// Obtém o objeto. O cast é permitido pois todos os objetos do jogo isométrico são IsoGameEntities
//					var currObj : IsoGameEntity = ( layerObjs[i] as IsoGameEntity );
//
//					// Move o objeto
//					var oldPos : WorldPos = currObj.worldPos;
//					currObj.update( timeElapsed );
//					var newPos : WorldPos = currObj.worldPos;
//					
//					// Se não saiu do lugar, não precisamos fazer mais nada para este objeto
//					if( !newPos.equals( oldPos ) )
//					{
//						// Verifica se newPos está em um tile válido. Se não está, traz o objeto de volta
//						// para o último tile válido na direção do movimento. Assim tratamos as bordas
//						// do mapa.
//						// Testamos, primeiro, se a nova posição continua no tile atual ou foi para algum de seus vizinhos. Somente
//						// quando fora deste caso, testaremos com todos os tiles da layer
//						var destTile : ITile = TileGameUtils.getTileForWorldPosUsingNeighbors( currObj.tile, newPos );
//						if( destTile == null )
//							destTile = TileGameUtils.getLayerTileForWorldPos( layer, newPos ); 
//
//						if( destTile == null )
//						{
//							// TODO : O certo é checar qual aresta atravessamos!!! Assim acontece de, por exemplo, 
//							// estarmos andando na borda de cima do tilemap, da esquerda para a direita, e sairmos
//							// pelo lado de cima do tile. no entanto, tentaremos achar a interseção do movimento
//							// com o lado direito do tile, o que será nulo...
//							var pathType : TileMapTilePath;
//							switch( TileGameUtils.DirectionFromMovementState( _playerCharacter.movementState ) )
//							{
//								case Direction.DIRECTION_UP:
//									pathType = TileMapTilePath.TILE_PATH_UP_DOWN;
//									break;
//								
//								case Direction.DIRECTION_DOWN:
//									pathType = TileMapTilePath.TILE_PATH_DOWN_UP;
//									break;
//								
//								case Direction.DIRECTION_LEFT:
//									pathType = TileMapTilePath.TILE_PATH_LEFT_RIGHT;
//									break;
//								
//								case Direction.DIRECTION_RIGHT:
//									pathType = TileMapTilePath.TILE_PATH_RIGHT_LEFT;
//									break;
//								
//								default:
//									// Não deveria acontecer...
//									throw new Error( "Invalid Movement Direction" );
//							}
//							
//							TileGameUtils.bringTileObjBackToTileBorder2( currObj, pathType, oldPos, IsoGlobals.SAFETY_MARGIN_IN_METERS );
//						}
//						// TODO : Liberar o teste de colisão para todos os objetos quando estes se movimentarem
//						else if( currObj == _playerCharacter )
//						{
//							// Checa e trata colisão para os objetos que se movimentam
//							handleCollision( currObj, oldPos, destTile, newPos );
//						}
//					}
//				}
//			}
//		}
//		
//		/**
//		* Verifica se o objeto que está se movendo irá colidir com outro objeto ao longo de sua trajetória. Se for colidir, posiciona o
//		* objeto no ponto de colisão e invoca o método onCollision dos objetos envolvidos no evento 
//		* @param movingObj O objeto que está se movendo
//		* @param isoWorldFrom A posição inicial da trajetória do objeto
//		* @param destTile O tile de destino
//		* @param isoWorldTo A posição final da trajetória do objeto
//		*/		
//		private function handleCollision( movingObj : ITileObject, isoWorldFrom : WorldPos, destTile : ITile, isoWorldTo : WorldPos ) : void
//		{
//			const originTile : ITile = movingObj.tile;
//			const currTileLayer : ITileLayer = originTile.tileLayer;
//			
//			// Se destTile é um vizinho na diagonal, escolhe um dos vizinhos adjacentes (nesta versão não permitimos
//			// movimentação na diagonal)
//			if( ( destTile.row != originTile.row ) && ( destTile.column != originTile.column ) )
//				destTile = _tileMap.getTile( destTile.tileLayer.layerIndex, destTile.row, originTile.column );
//			
//			// Obtém os tiles cruzados por esta trajetória
//			const crossedTiles : Vector.< ITile > = currTileLayer.getTilesInTrajectory( originTile, destTile );
//			if( crossedTiles == null )
//				return;
//			
//			// Percorre os tiles cortados pela trajetória, checando colisão com os objetos contidos em cada tile
//			// do percurso
//			const nCrossedTiles : uint = crossedTiles.length;
//			for( var crossedTileIndex : uint = 0 ; crossedTileIndex < nCrossedTiles ; ++crossedTileIndex )
//			{
//				// Já estamos nesse tile, então, antes de vermos se conseguimos sair dele, testamos colisão com seus outros objetos
//				var currTile : ITile = crossedTiles[ crossedTileIndex ];
//				
//				// TODO : Optamos por apenas um objeto por tile nesta versão. Logo, estes testes seriam inúteis. Para uma versão futura dos componentes podemos habilitá-los.
//				//				var collisionInfo : TileObjectCollisionData = new TileObjectCollisionData();
//				//				var collisionPos : Vector3D = currTile.checkObjsCollisionWithTrajectory( movingObj, isoWorldFrom, isoWorldTo, collisionInfo );
//				//				if( collisionPos != null )
//				//				{
//				//					// Coloca o objeto que está se movendo no ponto de sua trajetória em que houve a colisão
//				//					collisionPos = TileGameUtils.screenPosToTilePos( NanoMath.vector3DToPoint( collisionPos ), ( currTile  as TiledEditorTile ) );
//				//					movingObj.setPositionByAnchor( collisionPos.x, collisionPos.y );
//				//
//				//					// Deixa os objetos envolvidos na colisão tratarem a colisão por si próprios
//				//					movingObj.handleCollision( collisionInfo );
//				//					
//				//					var otherObj : ITileObject = collisionInfo.collisionObject;
//				//					collisionInfo.collisionObject = movingObj;
//				//					otherObj.handleCollision( collisionInfo );
//				//					
//				//					var currObjNewTile : ITile = TileGameUtils.getLayerTileForScreenPos( currTileLayer, movingObj.getPositionByAnchor() );
//				//					var collisionObjNewTile : ITile = TileGameUtils.getLayerTileForScreenPos( currTileLayer, otherObj.getPositionByAnchor() );
//				//					
//				//					// Quando inserimos um objeto em um novo tile, ele é retirado automaticamente de seu antigo
//				//					// tile
//				//					if( currObjNewTile != movingObj.tile )
//				//						currObjNewTile.insertTileObj( movingObj );
//				//					
//				//					if( collisionObjNewTile != otherObj.tile )
//				//						collisionObjNewTile.insertTileObj( otherObj );
//				//					
//				//					// Já tratamos este objeto
//				//					return;
//				//				}
//				
//				// TODO : Utilizar os eventos de IsoGameEntity seria mais bonito e mais correto do que fazer esta chamada na mão
//				onPlayerCharacterPosChanged( null );
//				
//				// Se não temos mais nenhum tile na trajetória, o objeto pode permanecer na posição final de seu movimento
//				if( ( crossedTileIndex + 1 ) >= crossedTiles.length )
//					return;
//				
//				// Não colidimos com nenhum objeto do tile atual, então talvez consigamos ir para
//				// o próximo tile da trajetória. Logo, verifica se podemos passar deste tile para
//				// o próximo
//				var pathAllowed : Boolean = false;
//				var pathType : TileMapTilePath = TileMapTilePath.TILE_PATH_NO_PATH;
//				var nextTile : ITile = crossedTiles[ crossedTileIndex + 1 ];
//				
//				// Verifica a posição relativa entre os tiles
//				pathType = TileGameUtils.GetTilePathType( currTile, nextTile );
//				switch( pathType )
//				{
//					case TileMapTilePath.TILE_PATH_UP_DOWN:
//						pathAllowed = currTile.allowsDown && nextTile.allowsUp;
//						break;
//					
//					case TileMapTilePath.TILE_PATH_DOWN_UP:
//						pathAllowed = currTile.allowsUp && nextTile.allowsDown;
//						break;
//					
//					case TileMapTilePath.TILE_PATH_RIGHT_LEFT:
//						pathAllowed = currTile.allowsLeft && nextTile.allowsRight;
//						break;
//					
//					case TileMapTilePath.TILE_PATH_LEFT_RIGHT:
//						pathAllowed = currTile.allowsRight && nextTile.allowsLeft;
//						break;
//					
//					case TileMapTilePath.TILE_PATH_NO_PATH:
//					case TileMapTilePath.TILE_PATH_SAME_TILE:
//					default:
//						// Vizinho na diagonal, ou alguma situação inesperada.
//						// NUNCA DEVERIA ACONTECER !!!
//						pathType = TileMapTilePath.TILE_PATH_NO_PATH;
//						break;
//				}
//				
//				// OBS: Nesta versão só estamos permitindo um objeto por tile!!!
//				// Colocamos este if após o switch, e não englobando-o, pois queremos obter o valor de pathType
//				if( nextTile.getTileObjs().length > 0 )
//					pathAllowed = false;
//				
//				if( pathAllowed )
//				{
//					// Coloca o objeto que está se movendo na borda do próximo tile. Quando inserimos um objeto em 
//					// um novo tile, ele é retirado automaticamente de seu antigo
//					( nextTile as TiledEditorTile ).insertTileObj( movingObj, false );
//					
//					// TODO : Utilizar os eventos de IsoGameEntity seria mais bonito e mais correto do que fazer esta chamada na mão
//					onPlayerCharacterTileChanged( null );
//				}
//				else
//				{
//					if( pathType == TileMapTilePath.TILE_PATH_NO_PATH )
//					{
//						// Ocorreu alguma situação inesperada. Ou não conseguimos mapear a nova posição em um tile, ou
//						// estamos indo para algum tile que não deveríamos. Então colocamos o objeto de volta em sua posição
//						// anterior
//						movingObj.worldPos = isoWorldFrom;
//					}
//					else
//					{
//						// A movimentação não é permitida, então coloca o objeto no limiar deste tile
//						TileGameUtils.bringTileObjBackToTileBorder2( movingObj, pathType, isoWorldFrom, IsoGlobals.SAFETY_MARGIN_IN_METERS );
//					}
//				}
//			}
//		}
//		
////		private function handleCollisionMultiple( movingObj : ITileObject, isoWorldFrom : WorldPos, destTile : ITile, isoWorldTo : WorldPos ) : void
////		{
////			const originTile : ITile = movingObj.tile;
////			const currTileLayer : ITileLayer = originTile.tileLayer;
////			
////			// Se destTile é um vizinho na diagonal, escolhe um dos vizinhos adjacentes (nesta versão não permitimos
////			// movimentação na diagonal)
////			if( ( destTile.row != originTile.row ) && ( destTile.column != originTile.column ) )
////				destTile = _tileMap.getTile( destTile.tileLayer.layerIndex, destTile.row, originTile.column );
////			
////			// Obtém os tiles cruzados por esta trajetória
////			const crossedTiles : Vector.< ITile > = currTileLayer.getTilesInTrajectory( originTile, destTile );
////			if( crossedTiles == null )
////				return;
////			
////			// Percorre os tiles cortados pela trajetória, checando colisão com os objetos contidos em cada tile
////			// do percurso
////			const nCrossedTiles : uint = crossedTiles.length;
////			for( var crossedTileIndex : uint = 0 ; crossedTileIndex < nCrossedTiles ; ++crossedTileIndex )
////			{
////				// Já estamos nesse tile, então, antes de vermos se conseguimos sair dele, testamos colisão com seus outros objetos
////				var currTile : ITile = crossedTiles[ crossedTileIndex ];
////				var collisionInfo : TileObjectCollisionData = new TileObjectCollisionData();
////				var collisionPos : Vector3D = currTile.checkObjsCollisionWithTrajectory( movingObj, isoWorldFrom, isoWorldTo, collisionInfo );
////				if( collisionPos != null )
////				{
////					// Coloca o objeto que está se movendo no ponto de sua trajetória em que houve a colisão
////					collisionPos = TileGameUtils.screenPosToTilePos( NanoMath.vector3DToPoint( collisionPos ), ( currTile  as TiledEditorTile ) );
////					movingObj.setPositionByAnchor( collisionPos.x, collisionPos.y );
////
////					// Deixa os objetos envolvidos na colisão tratarem a colisão por si próprios
////					movingObj.handleCollision( collisionInfo );
////					
////					var otherObj : ITileObject = collisionInfo.collisionObject;
////					collisionInfo.collisionObject = movingObj;
////					otherObj.handleCollision( collisionInfo );
////					
////					var currObjNewTile : ITile = TileGameUtils.getLayerTileForScreenPos( currTileLayer, movingObj.getPositionByAnchor() );
////					var collisionObjNewTile : ITile = TileGameUtils.getLayerTileForScreenPos( currTileLayer, otherObj.getPositionByAnchor() );
////					
////					// Quando inserimos um objeto em um novo tile, ele é retirado automaticamente de seu antigo
////					// tile
////					if( currObjNewTile != movingObj.tile )
////						currObjNewTile.insertTileObj( movingObj );
////					
////					if( collisionObjNewTile != otherObj.tile )
////						collisionObjNewTile.insertTileObj( otherObj );
////					
////					// Já tratamos este objeto
////					return;
////				}
////				
////				// TODO : Utilizar os eventos de IsoGameEntity seria mais bonito e mais correto do que fazer esta chamada na mão
////				onPlayerCharacterPosChanged( null );
////				
////				// Se não temos mais nenhum tile na trajetória, o objeto pode permanecer na posição final de seu movimento
////				if( ( crossedTileIndex + 1 ) >= crossedTiles.length )
////					return;
////				
////				// Não colidimos com nenhum objeto do tile atual, então talvez consigamos ir para
////				// o próximo tile da trajetória. Logo, verifica se podemos passar deste tile para
////				// o próximo
////				var pathAllowed : Boolean = false;
////				var pathType : TileMapTilePath = TileMapTilePath.TILE_PATH_NO_PATH;
////				var nextTile : ITile = crossedTiles[ crossedTileIndex + 1 ];
////				
////				// Verifica a posição relativa entre os tiles
////				pathType = TileGameUtils.GetTilePathType( currTile, nextTile );
////				switch( pathType )
////				{
////					case TileMapTilePath.TILE_PATH_UP_DOWN:
////						pathAllowed = currTile.allowsDown && nextTile.allowsUp;
////						break;
////					
////					case TileMapTilePath.TILE_PATH_DOWN_UP:
////						pathAllowed = currTile.allowsUp && nextTile.allowsDown;
////						break;
////					
////					case TileMapTilePath.TILE_PATH_RIGHT_LEFT:
////						pathAllowed = currTile.allowsLeft && nextTile.allowsRight;
////						break;
////					
////					case TileMapTilePath.TILE_PATH_LEFT_RIGHT:
////						pathAllowed = currTile.allowsRight && nextTile.allowsLeft;
////						break;
////					
////					case TileMapTilePath.TILE_PATH_NO_PATH:
////					case TileMapTilePath.TILE_PATH_SAME_TILE:
////					default:
////						// Vizinho na diagonal, ou alguma situação inesperada.
////						// NUNCA DEVERIA ACONTECER !!!
////						pathType = TileMapTilePath.TILE_PATH_NO_PATH;
////						break;
////				}
////				
////				// OBS: Nesta versão só estamos permitindo um objeto por tile!!!
////				// Colocamos este if após o switch, e não englobando-o, pois queremos obter o valor de pathType
////				if( nextTile.getTileObjs().length > 0 )
////					pathAllowed = false;
////				
////				if( pathAllowed )
////				{
////					// Coloca o objeto que está se movendo na borda do próximo tile. Quando inserimos um objeto em 
////					// um novo tile, ele é retirado automaticamente de seu antigo
////					( nextTile as TiledEditorTile ).insertTileObj( movingObj, false );
////					
////					// TODO : Utilizar os eventos de IsoGameEntity seria mais bonito e mais correto do que fazer esta chamada na mão
////					onPlayerCharacterTileChanged( null );
////				}
////				else
////				{
////					if( pathType == TileMapTilePath.TILE_PATH_NO_PATH )
////					{
////						// Ocorreu alguma situação inesperada. Ou não conseguimos mapear a nova posição em um tile, ou
////						// estamos indo para algum tile que não deveríamos. Então colocamos o objeto de volta em sua posição
////						// anterior
////						movingObj.worldPos = isoWorldFrom;
////					}
////					else
////					{
////						// A movimentação não é permitida, então coloca o objeto no limiar deste tile
////						TileGameUtils.bringTileObjBackToTileBorder2( movingObj, pathType, isoWorldFrom, IsoGlobals.SAFETY_MARGIN_IN_METERS );
////					}
////				}
////			}
////		}
//		
//		/**
//		* Atualiza os objetos da cena do jogo quando estamos no estado GAME_STATE_TALKING
//		* @param timeElapsed Tempo transcorrido dede a última atualização 
//		*/
//		private function updateStateTalking( timeElapsed : Number ) : void
//		{
//			_gameCamera.update( timeElapsed );
//			_talkDialog.update( timeElapsed );
//		}		
// 
//		/**
//		* Callback chamada quando o jogador pára de pressionar uma tecla. Utilizada em resposta ao evento KeyboardEvent.KEY_UP
//		* @param e Dados sobre o evento do teclado 
//		*/		
//		private function onKeyUp( e : KeyboardEvent  ) : void
//		{
//			// TODO : Seria melhor e mais otimizado se utilizássemos o padrão STRATEGY. Poderíamos armazenar os métodos
//			// de tratamento de tecla em ponteiros para função / functors e modificar o método de tratamento atual apenas alterando
//			// o valor de uma variável em setState. Assim não teríamos que executar este switch toda vez qua chamamos onKeyUp
//			switch( gameState )
//			{
//				case GameState.GAME_STATE_MOVING:
//					if( !_keyLocked )
//						handleStateMovingKeyUp( e );
//					break
//				
//				case GameState.GAME_STATE_TALKING:
//				case GameState.GAME_STATE_ITEM_SCREEN:
//					handleStateTalkingKeyUp( e );
//					break;
//			}
//		}
//		
//		/**
//		* Trata eventos KeyboardEvent.KEY_UP quando estamos no estado de jogo GAME_STATE_MOVING
//		* @param e Dados sobre o evento do teclado
//		*/
//		private function handleStateMovingKeyUp( e : KeyboardEvent  ) : void
//		{
//			if( _movingCamera )
//			{
//				// TODO : Somente para teste!!!!
////				switch( e.keyCode )
////				{
////					case Keyboard.LEFT:
////					case Keyboard.RIGHT:
////						_cameraMovement.x = 0;
////						break;
////					
////					case Keyboard.UP:
////					case Keyboard.DOWN:
////						_cameraMovement.y = 0;
////						break;
////				}
//			}
//			else
//			{
//				trace( "\n--------------------------------------\n" );
//				trace( "Recebeu evento de tecla KEY_UP" );
//				trace( "\n--------------------------------------\n" );
//				
//				switch( e.keyCode )
//				{
//					case Keyboard.LEFT:
//						if( _playerCharacter.movementState == MovementState.MOVE_STATE_MOVING_LEFT )
//							_playerCharacter.movementState = MovementState.MOVE_STATE_STOPPED_LEFT;
//						break;
//					
//					case Keyboard.RIGHT:
//						if( _playerCharacter.movementState == MovementState.MOVE_STATE_MOVING_RIGHT )
//							_playerCharacter.movementState = MovementState.MOVE_STATE_STOPPED_RIGHT;
//						break;
//					
//					case Keyboard.UP:
//						if( _playerCharacter.movementState == MovementState.MOVE_STATE_MOVING_UP )
//							_playerCharacter.movementState = MovementState.MOVE_STATE_STOPPED_UP;
//						break;
//					
//					case Keyboard.DOWN:
//						if( _playerCharacter.movementState == MovementState.MOVE_STATE_MOVING_DOWN )
//							_playerCharacter.movementState = MovementState.MOVE_STATE_STOPPED_DOWN;
//						break;
//				}
//			}
//		}
//		
//		/**
//		* Trata eventos KeyboardEvent.KEY_UP quando estamos no estado de jogo GAME_STATE_TALKING
//		* @param e Dados sobre o evento do teclado
//		*/
//		private function handleStateTalkingKeyUp( e : KeyboardEvent  ) : void
//		{
//			switch( e.keyCode )
//			{
//				case Keyboard.ENTER:
//				case Keyboard.NUMPAD_ENTER:
//				case Keyboard.SPACE:
//					if( !_talkDialog.textShown() )
//						_talkDialog.setTextSpeed( Constants.TEXT_SPEED_SLOW );
//					break;
//			}
//		}
//
//		/**
//		* Callback chamada quando o jogador começa a pressionar uma tecla. Utilizada em resposta ao evento KeyboardEvent.KEY_DOWN
//		* @param e Dados sobre o evento do teclado 
//		*/		
//		private function onKeyDown( e : KeyboardEvent  ) : void
//		{
//			// TODO : Seria melhor e mais otimizado se utilizássemos o padrão STRATEGY. Poderíamos armazenar os métodos
//			// de tratamento de tecla em ponteiros para função / functors e modificar o método de tratamento atual apenas alterando
//			// o valor de uma variável em setState. Assim não teríamos que executar este switch toda vez qua chamamos onKeyDown
//			switch( gameState )
//			{
//				case GameState.GAME_STATE_MOVING:
//					if ( !_keyLocked && currentBkg == _tileMap )
//						handleStateMovingKeyDown( e );
//					break
//				
//				case GameState.GAME_STATE_TALKING:
//				case GameState.GAME_STATE_ITEM_SCREEN:
//					handleStateTalkingKeyDown( e );
//					break;
//				
//				case GameState.GAME_STATE_QUEST_SCREEN:
//					questScreen.onKeyDown( e );
//					break;
//			}
//		}
//		
//		/**
//		* Trata eventos KeyboardEvent.KEY_DOWN quando estamos no estado de jogo GAME_STATE_MOVING
//		* @param e Dados sobre o evento do teclado
//		*/		
//		private function handleStateMovingKeyDown( e : KeyboardEvent  ) : void
//		{
//			// TODO : Somente para teste!!!!
////			if( e.keyCode == LetterKeyCodes.KEYCODE_C )
////			{
////				_movingCamera = !_movingCamera;
////			}
//			
//			if( _movingCamera )
//			{
////				switch( e.keyCode )
////				{
////					case Keyboard.LEFT:
////						_cameraMovement.x = -Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////					
////					case Keyboard.RIGHT:
////						_cameraMovement.x = Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////					
////					case Keyboard.UP:
////						_cameraMovement.y = -Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////					
////					case Keyboard.DOWN:
////						_cameraMovement.y = Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////				}
////				
////				var camDestPos : Point = _gameCamera.getPosition();
////				switch( e.keyCode )
////				{
////					case Keyboard.LEFT:
////						camDestPos.x -= Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////					
////					case Keyboard.RIGHT:
////						camDestPos.x += Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////					
////					case Keyboard.UP:
////						camDestPos.y -= Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////					
////					case Keyboard.DOWN:
////						camDestPos.y += Constants.CAMERA_MOVEMENT_ON_INPUT;
////						break;
////				}
////				_gameCamera.setAnim( camDestPos, Constants.CAMERA_FOLLOW_TIME );
//				
//			}
//			else
//			{
//				switch( e.keyCode )
//				{
//					// Anda para a esquerda
//					case Keyboard.LEFT:
//						_playerCharacter.movementState = MovementState.MOVE_STATE_MOVING_LEFT;
//						break;
//					
//					// Anda para a direita
//					case Keyboard.RIGHT:
//						_playerCharacter.movementState = MovementState.MOVE_STATE_MOVING_RIGHT;
//						break;
//					
//					// Anda para cima
//					case Keyboard.UP:
//						_playerCharacter.movementState = MovementState.MOVE_STATE_MOVING_UP;
//						break;
//					
//					// Anda para baixo
//					case Keyboard.DOWN:
//						_playerCharacter.movementState = MovementState.MOVE_STATE_MOVING_DOWN;
//						break;
//					
//					// Checa tile na direção para qual o personagem está olhando
//					case Keyboard.SPACE:
//					case Keyboard.ENTER:
//					case Keyboard.NUMPAD_ENTER:
//						trace( "\n--------------------------------------\n" );
//						trace( "Recebeu evento de tecla KEY_DOWN" );
//						trace( "\n--------------------------------------\n" );
//						
//						const facing : Direction = TileGameUtils.DirectionFromMovementState( _playerCharacter.movementState );
//						const checkedTile : ITile = _playerCharacter.tile.getNeighborAtDirection( facing );
//						if( checkedTile != null )
//						{
//							const tileObjs : Vector.< ITileObject > = checkedTile.getTileObjs();
//							
//							// OBS: Nesta versão só estamos permitindo 1 objeto por tile
//							if( ( tileObjs.length > 0 ) && ( getTimer() - _lastGameStateTime >= TALK_MIN_INTERVAL ) )
//							{
//								_playerCharacter.stopMoving();
//								tileObjs[0].onCheck( facing );
//							}
//						}
//						break;
//				}
//			}
//		}
//		
//		/**
//		* Trata eventos KeyboardEvent.KEY_DOWN quando estamos no estado de jogo GAME_STATE_TALKING
//		* @param e Dados sobre o evento do teclado
//		*/	
//		private function handleStateTalkingKeyDown( e : KeyboardEvent  ) : void
//		{
//			switch( e.keyCode )
//			{
//				case Keyboard.UP:
//				case Keyboard.LEFT:
//					if( _talkDialog.showingQuestion() )
//						_talkDialog.previousAnswer();
//					break;
//				
//				case Keyboard.DOWN:
//				case Keyboard.RIGHT:
//					if( _talkDialog.showingQuestion() )
//						_talkDialog.nextAnswer();
//					break;
//
////				case Keyboard.ESCAPE:
////				case Keyboard.BACKSPACE:
////				case Keyboard.DELETE:
////					// Se tem uma pergunta no diálogo, não poderá cancelar a conversa. Assim garantimos
////					// que o usuário respondeu alguma coisa
////					if( !_talkDialog.hasAnswers() )
////						endTalk();
////					break;
//				
//				case Keyboard.ENTER:
//				case Keyboard.NUMPAD_ENTER:
//				case Keyboard.SPACE:
//					if( _talkDialog.visible )
//					{
//						if( _talkDialog.textShown() )
//						{
//							endTalk();
//						}
//						else
//						{
//							if( _talkDialog.showingQuestion() )
//							{
//								_talkDialog.hide();
//							}
//							else
//							{
//								_talkDialog.setTextSpeed( Constants.TEXT_SPEED_FAST );
//							}
//						}
//					}
//					break;
//			}
//		}
//		
//		public function getPlayerScore() : String
//		{
//			return int( Math.round( _happinessBar.getHappinessPercent() * 100.0 ) ) + "%";
//		}
//		
//		/**
//		 * 
//		 * @return 
//		 * 
//		 */		
//		public function getCharacters() : Array {
//			return characters.toArray();
//		}
//		
//		/**
//		 * 
//		 * @param characterId
//		 */
//		public function getCharacterById( characterId : int ) : Character
//		{
//			if ( characterId == GameScript.CHARACTER_JOGADOR )
//				return _playerCharacter;
//				
//			return characters.find( characterId );
//		}
//		
//		public function get playerCharacterType() : PlayerCharacterType
//		{
//			if( _playerCharacter == null )
//				return null;
//			
//			var playerCharacterClass : Class = Utils.GetObjClass( _playerCharacter.asset );
//			return playerCharacterClass == GenMan1Base ? PlayerCharacterType.CHARACTER_BOY : PlayerCharacterType.CHARACTER_GIRL;
//		}
//		
//		public function set playerCharacterType( characterType : PlayerCharacterType ) : void
//		{
//			switch( characterType )
//			{
//				case PlayerCharacterType.CHARACTER_BOY:
//					_playerCharacter = new Character( 0, new GenMan1Base() );
//					break;
//				
//				case PlayerCharacterType.CHARACTER_GIRL:
//					_playerCharacter = new Character( 0, new GenWoman1Base() );
//					break;
//				
//				default:
//					trace( ">>>>> VilaVirtual::playerCharacterType - Carregou o personagem errado => Type: " + characterType.Name + "; Index: " + characterType.Index );
//					throw ArgumentError( "Invalid character type" );
//					break;
//			}
//		}
//		
//		public function packGameData() : GameData
//		{
//			var data : GameData = new GameData();
//
//			// Parte do jogo
//			data.part = GameScript.instance.part;
//			
//			// Personagem utilizado
//			data.characterType = playerCharacterType;
//			
//			// Id do mapa de tiles utilizado
//			data.tilemapIndex = _tileMap.tileMapId;
//			
//			// Itens
//			var gameItems : Array = _backpack.getItems();
//			for each( var it : Item in gameItems )
//				data.addItem( it.id );
//			
//			// Quests 
//			var gameQuests : Array = _questLog.toArray();
//			for each( var qt : Quest in gameQuests )
//				data.addQuest( qt.id, qt.state )
//			
//			// Flags
//			data.flags = GameScript.instance.vars;
//			
//			// Posições e direções dos personagens
//			var savedPlayerCharacter : Boolean = false;
//			var gameCharacters : Array = _characters.toArray();
//			for each( var character : Character in gameCharacters )
//			{
//				var tile : ITile = character.tile;
//				if( tile != null )
//					data.addCharacter( character.characterId, tile.row, tile.column, character.movementState, character.expression );
//				
//				// O compilador do flash está reclamando da linha abaixo..... Triste...
//				//savedPlayerCharacter |= ( character.characterId == GameScript.CHARACTER_JOGADOR );
//				if( character.characterId == GameScript.CHARACTER_JOGADOR )
//					savedPlayerCharacter = true;
//			}
//			
//			if( !savedPlayerCharacter )
//				data.addCharacter( GameScript.CHARACTER_JOGADOR, _playerCharacter.tile.row, _playerCharacter.tile.column, _playerCharacter.movementState, _playerCharacter.expression );
//			
//			return data;
//		}
//		
//		/**
//		 * Remove todos os personagens do cenário.
//		 */		
//		public function removeAllCharacters() : void
//		{
//			for each ( var c : Character in characters ) {
//				c.removeSelf();
//			}
//		}
//		
//		public function backToMainMenu() : void
//		{
//			Application.GetInstance().setState( ApplicationState.APP_STATE_FROM_VILA_TO_MENU );
//		}
//		
//		/**
//		 * 
//		 */
//		public function setBackground( bkg : String ) : void
//		{
//			if ( currentBkg ) {
//				removeChild( currentBkg );
//				currentBkg = null;
//			}
//			
//			switch ( bkg ) {
//				case Constants.BACKGROUND_MAP:
//					currentBkg = _tileMap;
//					gameState = GameState.GAME_STATE_MOVING;
//				break;
//				case Constants.BACKGROUND_LOJACDS:	
//				
//				case Constants.BACKGROUND_BECO:
//				case Constants.BACKGROUND_BECO_VAZIO:	
//				case Constants.BACKGROUND_CASA_VELHA:	
//				case Constants.BACKGROUND_LANCHONETE:	
//				case Constants.BACKGROUND_PREFEITURA1:	
//				case Constants.BACKGROUND_PREFEITURA2:	
//				case Constants.BACKGROUND_VENDA:
//					gameState = GameState.GAME_STATE_TALKING;
//					if ( _playerCharacter.asset is GenMan1Base ) {
//						switch ( bkg ) {
//							case Constants.BACKGROUND_BECO:
//								bkg = Constants.BACKGROUND_BECO_H;
//							break;
//							case Constants.BACKGROUND_LOJACDS:	
//								bkg = Constants.BACKGROUND_LOJACDS_H;
//							break;
//							case Constants.BACKGROUND_BECO_VAZIO:	
//								bkg = Constants.BACKGROUND_BECO_VAZIO_H;
//							break;
//							case Constants.BACKGROUND_CASA_VELHA:	
//								bkg = Constants.BACKGROUND_CASA_VELHA_H;
//							break;
//							case Constants.BACKGROUND_LANCHONETE:	
//								bkg = Constants.BACKGROUND_LANCHONETE_H;
//							break;
//							case Constants.BACKGROUND_PREFEITURA1:	
//								bkg = Constants.BACKGROUND_PREFEITURA1_H;
//							break;
//							case Constants.BACKGROUND_PREFEITURA2:	
//								bkg = Constants.BACKGROUND_PREFEITURA2_H;
//							break;
//							case Constants.BACKGROUND_VENDA:
//								bkg = Constants.BACKGROUND_VENDA_H;
//							break;
//						}
//					} else {
//						switch ( bkg ) {
//							case Constants.BACKGROUND_BECO:
//								bkg = Constants.BACKGROUND_BECO_M;
//								break;
//							case Constants.BACKGROUND_LOJACDS:	
//								bkg = Constants.BACKGROUND_LOJACDS_M;
//								break;
//							case Constants.BACKGROUND_BECO_VAZIO:	
//								bkg = Constants.BACKGROUND_BECO_VAZIO_M;
//								break;
//							case Constants.BACKGROUND_CASA_VELHA:	
//								bkg = Constants.BACKGROUND_CASA_VELHA_M;
//								break;
//							case Constants.BACKGROUND_LANCHONETE:	
//								bkg = Constants.BACKGROUND_LANCHONETE_M;
//								break;
//							case Constants.BACKGROUND_PREFEITURA1:	
//								bkg = Constants.BACKGROUND_PREFEITURA1_M;
//								break;
//							case Constants.BACKGROUND_PREFEITURA2:	
//								bkg = Constants.BACKGROUND_PREFEITURA2_M;
//								break;
//							case Constants.BACKGROUND_VENDA:
//								bkg = Constants.BACKGROUND_VENDA_M;
//								break;
//						}						
//					}
//				
//				default:
//					var c : Class = Class( getDefinitionByName( bkg ) );
//					currentBkg = ( new c() as MovieClip );
//			}
//			
//			if ( currentBkg ) {
//				addChildAt( currentBkg, 0 );
//			}
//		}
//
//		public function get backpack() : Backpack
//		{
//			return _backpack;
//		}
//
//		/**
//		 * Indica que uma requisição foi enviada para o NanoOnline 
//		 */
//		public function onNORequestSent() : void
//		{
//			// Vazia
//		}
//		
//		/**
//		 * Indica que a requisição foi cancelada pelo usuário 
//		 */
//		public function onNORequestCancelled() : void
//		{
//			// TODO: Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de
//			// cancelar...
//		}
//		
//		/**
//		* Indica que uma requisição foi respondida e terminada com sucesso
//		*/
//		public function onNOSuccessfulResponse() : void
//		{
//			var app : Application = Application.GetInstance();
//			app.hideWaitFeedback();
//
//			switch( gameState )
//			{
//				case GameState.GAME_STATE_SAVING:
//					app.showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_GAME_SAVED ),
//								   NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), onContinuePlaying );
//					break;
//				
//				case GameState.GAME_STATE_SAVE_N_QUIT:
//					app.showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_GAME_SAVED ),
//														NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), onQuit );
//					break;
//			}
//		}
//		
//		/**
//		 * Sinaliza erros ocorridos nas operações do NanoOnline
//		 * @param errorCode O código do erro ocorrido
//		 * @param errorStr Descrição do erro ocorrido
//		 */		
//		public function onNOError( errorCode : NOErrors, errorStr : String ) : void
//		{
//			var app : Application = Application.GetInstance();
//			app.hideWaitFeedback();
//			
//			app.showPopUp( errorStr + "\n" + NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_RETRY ),
//						   NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_YES ), ( gameState == GameState.GAME_STATE_SAVING ? onSaveGame : onSaveAndQuit ),
//						   NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NO ), onQuit );
//		}
//		
//		/**
//		 * Indica o progresso da requisição atual
//		 * @param currBytes A quantidade de bytes que já foi transferida
//		 * @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
//		 */		
//		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void
//		{
//			// TODO : Vamos implementar? O NanoOnline é MUITO rápido via web... O usuário nem teria tempo de ver o
//			// feedback...
//			trace( "Progresso: " + ( ( currBytes / totalBytes ) * 100.0 ) + "%" );
//		}
	}
}
