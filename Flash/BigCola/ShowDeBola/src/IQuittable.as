package {
	public interface IQuittable	{
		function quit() : void;
	}
}