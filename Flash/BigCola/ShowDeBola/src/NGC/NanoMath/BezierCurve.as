package NGC.NanoMath
{
	import flash.geom.Vector3D;

	/**
	 *	Classe utilizada para calcular uma curva de Bezier a partir de 4 pontos
	 *	dados. Código adaptado de: http://en.wikipedia.org/wiki/B%C3%A9zier_curve
	 */
	public final class BezierCurve
	{
		// Pontos de controle da curva Bezier
		private var _origin : Vector3D;
		private var _controlPoint1 : Vector3D;
		private var _controlPoint2 : Vector3D;
		private var _destination : Vector3D;
		
		// Construtor
		public function BezierCurve( orig : VEctor3D, ctrl1 : Vector3D, ctrl2 : Vector3D, dest : Vector3D )
		{
			origin = orig;
			controlPoint1 = ctrl1;
			controlPoint2 = ctrl2;
			destination = dest;
		}
		
		// Getters
		public function get origin() : Vector3D
		{
			return _origin.clone();
		}
		
		public function get controlPoint1() : Vector3D
		{
			return _controlPoint1.clone();
		}
		
		public function get controlPoint2() : Vector3D
		{
			return _controlPoint2.clone();
		}
		
		public function get destination() : Vector3D
		{
			return _destination.clone();
		}
		
		// Setters
		public function set origin( v : Vector3D ) : void
		{
			_origin = v.clone();
		}
		
		public function set controlPoint1( v : Vector3D ) : void
		{
			_controlPoint1 = v.clone();
		}
		
		public function set controlPoint2( v : Vector3D ) : void
		{
			_controlPoint2 = v.clone();
		}
		
		public function set destination( v : Vector3D ) : void
		{
			_destination = v.clone();
		}
		
		// Retorna o ponto da curva referente a determinada porcentagem do movimento
		public function getPointAt( percent : Number ) : Vector3D
		{
			percent = clamp( percent, 0.0, 1.0 );
			return new Vector3D( 	evaluate( origin.x, controlPoint1.x, controlPoint2.x, destination.x, percent ),
									evaluate( origin.y, controlPoint1.y, controlPoint2.y, destination.y, percent ),
									evaluate( origin.z, controlPoint1.z, controlPoint2.z, destination.z, percent ) );
		}
		
		// Retorna a coordenada x do ponto da curva referente a determinada porcentagem do movimento.
		public function getXAt( percent : Number ) : Number
		{
			return evaluate( origin.x, controlPoint1.x, controlPoint2.x, destination.x, clamp( percent, 0.0, 1.0 ) );
		}
		
		// Retorna a coordenada y do ponto da curva referente a determinada porcentagem do movimento.
		public function getYAt( percent : Number ) : Number
		{
			return evaluate( origin.y, controlPoint1.y, controlPoint2.y, destination.y, clamp( percent, 0.0, 1.0 ) );
		}
		
		// Retorna a coordenada z do ponto da curva referente a determinada porcentagem do movimento.
		public function getZAt( percent : Number ) : Number
		{
			return evaluate( origin.z, controlPoint1.z, controlPoint2.z, destination.z, clamp( percent, 0.0, 1.0 ) );
		}
	
		// Retorna o valor da componente em um dado momento da curva
		private function evaluate( orig : Number, ctrl1 : Number, ctrl2 : Number, dest : Number, percent : Number ) : Number
		{
			const c : Number = 3.0 * ( ctrl1 - orig );
			const b : Number = 3.0 * ( ctrl2 - ctrl1 ) - c;
			const a : Number = dest - orig - c - b;
			
			return ( a * ( percent * percent * percent ) ) + ( b * ( percent * percent ) ) + ( c * percent ) + orig;
		}
	}
}