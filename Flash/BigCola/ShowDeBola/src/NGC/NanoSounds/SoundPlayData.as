package NGC.NanoSounds
{
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;

	public final class SoundPlayData
	{
		/**
		* Índice do som no array de sons do SoundManager 
		*/
		private var _index : int;
		
		/**
		* A posição inicial, em milissegundos, na qual a reprodução do som deve começar 
		*/
		private var _startTime : Number;
		
		/**
		* Quantas vezes devemos repetir a execução do som. SoundManager.INFINITY_LOOP deve ser utilizado quando um looping infinito for necessário  
		*/
		private var _loopCount : uint;
		
		/**
		* Objeto que define a transformação de áudio a ser aplicada sobre o som original 
		*/
		private var _transform : SoundTransform;
		
		/**
		* Indica se devemos reutilizar um canal de som existente caso este mesmo som já esteja sendo reproduzido
		*/
		private var _reuseSoundChannel : Boolean;

		/**
		* Construtor 
		*/		
		public function SoundPlayData( index : uint, startTime : Number = 0.0, loopCount : int = 0, transform : SoundTransform = null, reuseSoundChannel : Boolean = false )
		{
			_index = index;
			_startTime = startTime;
			_loopCount = loopCount;
			_transform = transform;
			_reuseSoundChannel = reuseSoundChannel;
		}

		/**
		* Retorna o índice do som no array de sons do SoundManager 
		*/
		public function getIndex() : int
		{
			return _index;
		}

		/**
		* Determina o índice do som no array de sons do SoundManager
		*/
		public function setIndex( value : int ) : void
		{
			_index = value;
		}

		/**
		* Retorna a posição inicial, em milissegundos, na qual a reprodução do som deve começar 
		*/
		public function getStartTime() : Number
		{
			return _startTime;
		}

		/**
		* Determina a posição inicial, em milissegundos, na qual a reprodução do som deve começar
		*/
		public function setStartTime( value : Number ) : void
		{
			_startTime = value;
		}

		/**
		* Retorna quantas vezes devemos repetir a execução do som. SoundManager.INFINITY_LOOP deve ser utilizado quando um looping infinito for necessário  
		*/
		public function getLoopCount() : uint
		{
			return _loopCount;
		}

		/**
		* Determina quantas vezes devemos repetir a execução do som. SoundManager.INFINITY_LOOP deve ser utilizado quando um looping infinito for necessário
		*/
		public function setLoopCount( value : uint ) : void
		{
			_loopCount = value;
		}

		/**
		* Retorna o objeto que define a transformação de áudio a ser aplicada sobre o som original 
		*/
		public function getTransform() : SoundTransform
		{
			return _transform;
		}

		/**
		* Determina o objeto que define a transformação de áudio a ser aplicada sobre o som original
		*/
		public function setTransform( value : SoundTransform ) : void
		{
			_transform = value;
		}

		/**
		* Retorna se devemos reutilizar um canal de som existente caso este mesmo som já esteja sendo reproduzido
		*/
		public function isReusingSoundChannel() : Boolean
		{
			return _reuseSoundChannel;
		}

		/**
		* Determina se devemos reutilizar um canal de som existente caso este mesmo som já esteja sendo reproduzido
		*/
		public function setReuseSoundChannel( value : Boolean ) : void
		{
			_reuseSoundChannel = value;
		}
	}
}