package {
	import NGC.ASWorkarounds.Enum;
	import NGC.NanoOnline.Gender;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NOTextsIndexes;
	import NGC.Utils;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class NanoOnlineListener implements INOListener {
		private var root : IApplication;
		private var exit : Function;
		private var fail : Function;
		private var successfulText : uint;
		private var errorfulText : uint;
		
		public function NanoOnlineListener( root : IApplication, exit : Function, successfulText: uint, errorfulText: uint, fail : Function = null ) {
			this.root = root;
			this.exit = exit;
			this.fail = fail;
			this.successfulText = successfulText;
			this.errorfulText = errorfulText;
		}
		
		public function onNORequestSent() : void { }				
		public function onNORequestCancelled() : void { if( fail != null ) fail(); }		
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void	{ }		
		
		public function onNOSuccessfulResponse() : void {
			root.hideWaitFeedback();
			root.showPopUp( NOGlobals.GetNOText( successfulText /*NOTextsIndexes.NO_TXT_LOGGED_IN*/ ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), exit );
		}
		
		
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void {
			root.hideWaitFeedback();
			root.showPopUp( NOGlobals.GetNOText( errorfulText /*NOTextsIndexes.NO_TXT_LOGGED_IN, NOTextsIndexes.NO_TXT_COULDNT_LOGIN*/ )+ ":\n" + errorStr,
				NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
			if( fail != null ) fail();
		}
	}
}