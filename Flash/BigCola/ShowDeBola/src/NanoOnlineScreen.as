package {
	import NGC.ASWorkarounds.Enum;
	import NGC.NanoOnline.Gender;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOConnection;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NOTextsIndexes;
	import NGC.Utils;
	
	import fl.managers.FocusManager;
	import fl.transitions.easing.Regular;
	
	import flash.display.InteractiveObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.profiler.profile;
	
	import flashx.textLayout.edit.SelectionFormat;
	import flashx.textLayout.edit.SelectionManager;
	import flashx.textLayout.edit.SelectionState;
	import flashx.textLayout.events.SelectionEvent;
	import flashx.textLayout.utils.CharacterUtil;
	
	public class NanoOnlineScreen extends AppScene implements INOListener {		
		private var formText : FormText;
		private var loginForm : LoginForm;
		private var reminder : Reminder;
		private var btCancel : GenericButton;
		private var btSend : GenericButton;
		private var btLogin : GenericButton;
		private var btRemind : GenericButton;
		private var btBack : GenericButton;
		
		/** Objeto que realiza a integração com o NanoOnline */		
		private var nanoCustomer : NOCustomer;
		
		/** Formulário onde o usuário irá fornecer seus dados */				
		private var nanoForm : FormText;
		
		public function NanoOnlineScreen() {
			super( new FormText() );
			//super( new SceneForm() );
		}
		
		
		protected override function onAddedToStage( e : Event ) : void {
			trace( "\n\n-------------------------------------------------------------------\n\n") ;
			
			super.onAddedToStage( e );
			
			formText = ( innerScene as FormText );
			
			loginForm = new LoginForm();
			reminder = new Reminder();
			reminder.visible = false;
			reminder.alpha = 0;
			
			addChild( loginForm );
			
			addChild( reminder );
			
			formText.x = 60;
			formText.y = 40;
			loginForm.x = formText.x - 180;
			loginForm.y = Constants.STAGE_TOP + 120;
			
			nanoCustomer = new NOCustomer();
			nanoForm = formText;
			
			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			
			btCancel = new GenericButton( "Cancelar", 0, cancel );
			btCancel.x = Constants.STAGE_RIGHT - ( ( btCancel.width / 2 ) + Constants.SAFE_MARGIN );
			btCancel.y = Constants.STAGE_BOTTOM - ( ( btCancel.height / 2 ) + Constants.SAFE_MARGIN );
			addChild( btCancel );
						
			loginForm.btForgotPassword.addEventListener( MouseEvent.CLICK, goToRemindMode );
			
			reminder.mcPassReminder.removeChild( reminder.mcPassReminder.btForgotPassSend );
			btRemind = new GenericButton( "Enviar", 1, remind, GenericButton.BUTTON_PURPLE );
			reminder.mcPassReminder.addChild( btRemind );
			btRemind.x = reminder.mcPassReminder.btForgotPassSend.x;
			btRemind.y = reminder.mcPassReminder.btForgotPassSend.y;
			
			reminder.x = -50;
			reminder.y = -65;
			
			formText.removeChild( formText.btSend );
			btSend = new GenericButton( "Cadastrar", 1, createAccount );
			formText.addChild( btSend );
			btSend.x = formText.btSend.x;
			btSend.y = formText.btSend.y;
			
			loginForm.removeChild( loginForm.btPlay );
			btLogin = new GenericButton( "Entrar", 2, login, GenericButton.BUTTON_PURPLE );
			loginForm.addChild( btLogin );
			btLogin.x = loginForm.btPlay.x;
			btLogin.y = loginForm.btPlay.y;
			
			btBack = new GenericButton( "Voltar", 3, backNormalMode );
			btBack.x = Constants.STAGE_RIGHT - ( ( btBack.width / 2 ) + Constants.SAFE_MARGIN );
			btBack.y = Constants.STAGE_BOTTOM - ( ( btBack.height / 2 ) + Constants.SAFE_MARGIN );
			btBack.visible = false;
			addChild( btBack );
		}

		
		private function onKeyDown( e : KeyboardEvent  ) : void {
			trace( "onKeyDown" );
			
			switch ( e.keyCode ) {
				case 9: //TAB
					trace( "Pressed Tab: " );
				break;
				
				case 13: //ENTER
					trace( "Pressed Enter" );
				break;
			}
		}
		
		
		protected override function onRemovedFromStage( e : Event ) : void {
			trace( "\n\n******************************************************************\n\n") ;
			
			super.onRemovedFromStage( e );
			formText = null;
			loginForm = null;
			reminder = null;
			nanoCustomer = null;
			nanoForm = null;
		}
		
		
		public function cancel( clickedBt : GenericButton ) : void { 
			quit(); 
		}		
		
		
		public function quit() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
		}
		
		
		public function backNormalMode( clickedBt : GenericButton = null ) : void { 
			setRemindMode( false ); 
		}
		
		
		public function goToRemindMode( evt : Event ) : void {
			setRemindMode( true ); 
		}
		
		
		public function remind( clickedBt : GenericButton ) : void { 
			/*fazer interação com servidor*/
			const customer : NOCustomer = new NOCustomer();
			try {
				customer.email = reminder.mcPassReminder.tbForgotPassEmail.text;
				NOCustomer.SendRememberPasswordRequest( customer, this );
				btRemind.setActivity( false );
			} catch ( e : Error ) {
				( root as IApplication ).showPopUp( e.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
			}
		}
		
		
		private function setRemindMode( b : Boolean ) : void { 
			if( b ) 
				loginForm.btForgotPassword.removeEventListener( MouseEvent.CLICK, goToRemindMode );
			else 
				loginForm.btForgotPassword.addEventListener( MouseEvent.CLICK, goToRemindMode );
			
			const initial : Number = b ? 0 : 1;
			const final : Number = b ? 1 : 0;
			
			reminder.visible = reminder.mouseEnabled = reminder.buttonMode = b;
			formText.visible = formText.mouseEnabled = formText.buttonMode = !b;			
			loginForm.visible = loginForm.mouseEnabled = loginForm.buttonMode = !b;
			btRemind.setActivity( true );
			
			btSend.visible = btSend.mouseEnabled = btSend.buttonMode = !b;			
			btLogin.visible = btLogin.mouseEnabled = btLogin.buttonMode = !b;			
			btCancel.visible = btCancel.mouseEnabled = btCancel.buttonMode = !b;			
			btBack.visible = btBack.mouseEnabled = btBack.buttonMode = b;			
			btRemind.visible = btRemind.mouseEnabled = btRemind.buttonMode = b;			
			
			TweenManager.tween( reminder, "alpha", Regular.easeInOut, initial, final, 1 );
			TweenManager.tween( btBack, "alpha", Regular.easeInOut, initial, final, 1 );
			TweenManager.tween( formText, "alpha", Regular.easeInOut, final, initial, 1 );
			TweenManager.tween( loginForm, "alpha", Regular.easeInOut, final, initial, 1 );
			TweenManager.tween( btSend, "alpha", Regular.easeInOut, final, initial, 1 );
			TweenManager.tween( btLogin, "alpha", Regular.easeInOut, final, initial, 1 );
			TweenManager.tween( btCancel, "alpha", Regular.easeInOut, final, initial, 1 );
		}
		
		
		public function login( clickedBt : GenericButton ) : void {
			var nickname : String = loginForm.getNickname();
			if( nickname.length == 0 ) {
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICKNAME_MANDATORY ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			
			var password : String = loginForm.getPassword();
			if( password.length == 0 ) {
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_PASSWORD_MANDATORY ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			try {
				nanoCustomer = new NOCustomer();
				nanoCustomer.nickname = loginForm.getNickname();
				nanoCustomer.password = loginForm.getPassword();
				if( !NOCustomer.SendLoginRequest( nanoCustomer, new NanoOnlineListener( root as IApplication, loginQuit, NOTextsIndexes.NO_TXT_LOGGED_IN, NOTextsIndexes.NO_TXT_COULDNT_LOGIN, loginFail ) ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_SEND_REQUEST_TO_SERVER ) );
				btLogin.setActivity( false );
				
			} catch( ex : Error ) {
				trace( ">>> SceneFormEx::onSubmit - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			( root as IApplication ).showWaitFeedback();
		}		
		
		
		public function loginFail() : void {
			btLogin.setActivity( true );
		}
		
		
		public function loginQuit() : void {
			( Application.GetInstance() as Application ).setLoggedProfile( nanoCustomer );
			btLogin.setActivity( true );
			quit();
		}
		

		private function createAccount( clickedBt : GenericButton ) : void {
			// Verifica se os campos obrigatórios foram preenchidos
			var nickname : String = nanoForm.getNickname();
			if( nickname.length == 0 ) {
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICKNAME_MANDATORY ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			var email : String = nanoForm.getEmail();
			if( email.length == 0 ) {
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_MANDATORY ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			var password : String = nanoForm.getPassword();
			if( password.length == 0 ) {
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_PASSWORD_MANDATORY ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			} else if( password != nanoForm.getPasswordConfirm() ) {
				( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PASSWORD_CONFIRM ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			var birthYear : int = nanoForm.getBirthYear();
			var birthMonth : int = nanoForm.getBirthMonth();
			var birthDay : int = nanoForm.getBirthDay();
			
			try
			{
				// Faz as pré-validações dos campos do formulário através dos setters
				nanoCustomer.password = password;
				nanoCustomer.nickname = nickname;
				nanoCustomer.firstName = nanoForm.getFirstName();
				nanoCustomer.lastName = nanoForm.getLastName();
				nanoCustomer.email = email;
				nanoCustomer.phone = nanoForm.getPhoneNumber();
				nanoCustomer.setBirthday( birthDay, birthMonth, birthYear );
				nanoCustomer.gender = ( Enum.FromIndex( Gender, nanoForm.getGender() ) as Gender );
				
				// Envia a requisição para o NanoOnline
				if( !NOCustomer.SendCreateRequest( nanoCustomer, new NanoOnlineListener( root as IApplication, loginQuit, NOTextsIndexes.NO_TXT_PROFILE_CREATED, NOTextsIndexes.NO_TXT_COULDNT_LOGIN ) ) )
					throw new Error( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_COULDNT_CREATE_REQUEST ) );
				//TODO criar eventListener de logger
			}
			catch( ex : Error )
			{
				trace( ">>> SceneFormEx::onSubmit - Exception: " + ex.message );
				( root as IApplication ).showPopUp( ex.message, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), null );
				return;
			}
			
			( root as IApplication ).showWaitFeedback();
			
		}
		
		
		public function onNORequestSent() : void {
			trace( "NanoOnlineScreen.onNORequestSent()" );
		}
		
		
		public function onNORequestCancelled() : void {
			trace( "NanoOnlineScreen.onNORequestCancelled()" );
		}
		
		
		public function onNOSuccessfulResponse() : void {
			trace( "NanoOnlineScreen.onNOSuccessfulResponse()" );
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_SENT ), NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), backNormalMode );
			btLogin.setActivity( true );
		}
		
		
		public function onNOError(errorCode:NOErrors, errorStr:String) : void {
			trace( "NanoOnlineScreen.onNOError( " + errorCode + ", " + errorStr + " )" );
			( root as IApplication ).showPopUp( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_ERROR_OCCURED ) + ":\n" + errorStr, NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_OK ), backNormalMode );
			btLogin.setActivity( true );
		}
		
		
		public function onNOProgressChanged(currBytes:int, totalBytes:int) : void {
			trace( "NanoOnlineScreen.onNOProgressChanged( " + currBytes + ", " + totalBytes + " )" );
		}
	}
}