package {
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.NanoSounds.SoundManager;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.utils.Dictionary;

	public final class Player extends DaniAlves implements BallListener {
		
		private static const LABEL_LOOP : String 		= "_LOOP";
		private static const LABEL_BACK : String 		= "_BACK";
		private static const LABEL_START : String 		= "_START";
		private static const LABEL_HIT : String 		= "_HIT";
		private static const LABEL_END : String 		= "_END";
		
		public static const LABEL_RAISE_BALL : String 		= "levantada";
		public static const LABEL_BASIC_RIGHT : String 		= "emb";
		public static const LABEL_BASIC_LEFT : String 		= "emb_esq";
		public static const LABEL_THIGH_RIGHT : String 		= "coxada";
		public static const LABEL_THIGH_LEFT : String 		= "coxada_esq";
		public static const LABEL_HEADER : String 			= "cabeceada";
		public static const LABEL_SPECIAL : String 			= "especial";
		public static const LABEL_SPECIAL_LEFT : String 	= "especial_esquerda";
		public static const LABEL_SPECIAL_LEFT_1 : String 	= "especial_esq";
		public static const LABEL_SPECIAL_RIGHT_1 : String	= "especial_dir";
		public static const LABEL_SPECIAL_LEFT_2 : String 	= "especial_esq2";
		public static const LABEL_SPECIAL_RIGHT_2 : String	= "especial_dir2";
		public static const LABEL_GOOD : String 			= "winning";
		public static const LABEL_BAD : String 				= "losing";
		
		private var animationLabels : Array = [
			LABEL_RAISE_BALL,
			LABEL_BASIC_RIGHT,
			LABEL_BASIC_LEFT,
			LABEL_THIGH_RIGHT,
			LABEL_THIGH_LEFT,
			LABEL_HEADER,
			LABEL_SPECIAL_LEFT_1,
			LABEL_SPECIAL_RIGHT_1,
			LABEL_SPECIAL_LEFT_2,
			LABEL_SPECIAL_RIGHT_2,
			LABEL_SPECIAL,
			LABEL_GOOD,
			LABEL_BAD,
		];
		
		private var anchors : Array = [
			new Point( -50, Constants.BALL_START_Y ), // RAISE
			new Point( -50, 170 ), // BASIC_RIGHT
			new Point( 30, 170 ), // BASIC_LEFT
			new Point( -40, 70 ), // THIGH_RIGHT
			new Point( 20, 70 ), // THIGH_LEFT
			new Point( -15, -97 ), // HEADER
			new Point( 20, 170 ), // SPECIAL_LEFT_1
			new Point( -50, 170 ), // SPECIAL_RIGHT_1
			new Point( 20, 170 ), // SPECIAL_LEFT_2
			new Point( -50, 170 ), // SPECIAL_RIGHT_2
			new Point( 30, 170 ), // SPECIAL
			new Point( 30, 70 ), // GOOD
			new Point( 20, 70 ), // BAD
		]; 
		
		private static const LABEL_COMMON_FIRST_INDEX : uint = 1;
		private static const LABELS_COMMON_TOTAL : uint = 5;
		
		private static const LABEL_SPECIAL_FIRST_INDEX : uint = LABEL_COMMON_FIRST_INDEX + LABELS_COMMON_TOTAL;
		private static const LABELS_SPECIAL_TOTAL : uint = 2;
		
		private static const LABEL_HEADER_INDEX : uint = 5;
		
		private static const REPEAT_RATE : Number = 0.28;
		
		private static const HEAD_INDEX : uint = 6;
		
		private static const TARGET_BEHAVIOR_HIT	: int = 0;
		private static const TARGET_BEHAVIOR_RESET	: int = 1;
		private static const TARGET_BEHAVIOR_STOP	: int = 2;
		
		private var ball : Ball;
		
		private var targetBehavior : int;
		
		private var target : Shape;
		
		private var accTime : Number = 0;
		
		private var playing : Boolean = false;
		
		private var currentAnimation : String;
		
		private var nextAnimation : String;
		
		private var nextTargetIndex : int;
		
		private var lastIndex : int = 0;
		
		private var isHeader : Boolean;
		
		private var checkHit : Boolean = false;
		
		private var framesIndexes : Dictionary = new Dictionary();
		
		private var frameHitIndexes : Dictionary = new Dictionary();
		
		private var getRecentPerformance : Function;
		
		private var head : MovieClip;
		
		private var headManager : HeadManager;
		
		private var shadow : dani_shadow;
		

		public function Player( getPerformanceCallback : Function ) {
			stop();
			
			getRecentPerformance = getPerformanceCallback;
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}
		
		
		private function onAddedToStage( e : Event ) : void
		{
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			addEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
			
			x = Constants.PLAYER_START_X;
			y = Constants.PLAYER_START_Y;
		
			target = new Shape();
//			target.graphics.beginFill( 0xff0000 );
//			target.graphics.drawCircle( 0, 0, 15 );
//			target.graphics.endFill();
//			target.alpha = 0.4;
//			addChild( target );
			
			shadow = new dani_shadow();
			addChildAt( shadow, 0 );
			shadow.x = -shadow.width / 2;
			shadow.y = Constants.SHADOW_Y;
			
			ball = new Ball();
			ball.setListener( this );
			addChild( ball );
			
			head = getChildAt( HEAD_INDEX ) as MovieClip;
			head.stop();
			
			headManager = new HeadManager( head );
			
			for ( var c : uint = 0; c < numChildren; ++c ) {
				trace( "child[ " + c + " ]: " + getChildAt( c ) + " -> " + getChildAt( c ).name );
			}
			
			for ( var i : uint = 0; i < currentLabels.length; ++i ) {
				trace( "label[ " + i + " ]: " + currentLabels[ i ].name + ", " + currentLabels[ i ].frame );
			}
			
			for ( var j : uint = 0; j < head.currentLabels.length; ++j ) {
				trace( "head label[ " + j + " ]: " + head.currentLabels[ j ].name + ", " + head.currentLabels[ j ].frame );
			}
		}
		
		
		private function onRemovedFromStage( e : Event ) : void {
			removeEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			shadow = null;
			if ( ball ) {
				ball.cancelAlphaTween();
				ball.setListener( null );
				ball = null;
			}
			head = null;
			headManager = null;
			
			while ( numChildren > 0 )
				removeChildAt( 0 );
		}
		
		
		/**
		 * 
		 */
		public function update( delta : Number ) : void {
			setChildIndex( ball, numChildren - 1 );
			
			if ( playing ) {
				accTime += delta;
				if ( accTime >= Constants.STAGE_FRAME_TIME ) {
					accTime %= Constants.STAGE_FRAME_TIME;
					nextFrame();
					onFrameChanged();
				}
			}
			
			ball.update( delta );
			headManager.update( delta );
			
			if ( targetBehavior == TARGET_BEHAVIOR_HIT && ball.getPosY() < Constants.BALL_START_Y && !ball.isGoingUp() ) {
				const started : Boolean = currentFrame != getFrameIndex( getLabelString( currentAnimation, true ) );
				const framesToHit : int = getFramesToHit( !started );
				const timeToTarget : Number = ball.getTimeToTarget() - 0.025;
				
				if ( isHeader ) {
					if ( !headManager.isDoingHeader() && HeadManager.FRAMES_TO_HEADER_HIT * Constants.STAGE_FRAME_TIME >= timeToTarget )
						headManager.doHeader();
				}
				
				if ( framesToHit >= 0 && framesToHit * Constants.STAGE_FRAME_TIME >= timeToTarget ) {
//					trace( "TIME TO TARGET: " + (framesToHit * Constants.STAGE_FRAME_TIME) + " / " + ball.getTimeToTarget() );
					// prepara animação para próximo toque na bola
					if ( !playing ) 
						setPlaying( true );
					
					const l : String = getLabelString( currentAnimation, !started );
					if ( currentLabel != l )
						setSequence( l );	
				}
			}
		}
		
		
		private function getLabelString( label : String, start : Boolean ) : String {
			switch ( label ) {
				case LABEL_HEADER:
					return label + LABEL_LOOP;
				
				default:
					return label + ( start ? LABEL_START : LABEL_LOOP );
			}
		}
		
		
		/**
		 * 
		 */
		private function getFramesToHit( fromStart : Boolean ) : int {
			const l : String = currentAnimation + ( fromStart ? LABEL_START : LABEL_LOOP );
			
			return ( ( getFrameHitIndex( currentAnimation ) - getFrameIndex( currentAnimation + LABEL_LOOP ) ) + 
					 ( fromStart ? ( getFrameIndex( currentAnimation + LABEL_START + LABEL_END ) - getFrameIndex( currentAnimation + LABEL_START ) ) < 0 : 0 ) );
		}
		
		
		private function randomNewMove( firstTime : Boolean = false ) : void {
			var index : uint;
			
			if ( firstTime ) {
				index = 0;
				nextTargetIndex = 1;
				currentAnimation = LABEL_RAISE_BALL;
				nextAnimation = LABEL_BASIC_RIGHT;
				target.x = anchors[ index ].x;
				target.y = anchors[ index ].y;
				ball.setTarget( target );
				
				trace( "FIRST" );
				targetBehavior = TARGET_BEHAVIOR_HIT;
			} else if ( currentAnimation ) {
				const performance : Number = getRecentPerformance();
				
				trace( "RECENT PERFORMANCE: " + performance + " -> " + currentAnimation );
				
				if ( performance == Constants.PRECISION_BIG && Math.random() > 0.6 ) {
					if ( Math.random() < REPEAT_RATE )
						index = lastIndex;
					else
						index = LABEL_SPECIAL_FIRST_INDEX + Math.floor( Math.random() * LABELS_SPECIAL_TOTAL ) as uint;

					nextAnimation = animationLabels[ index ];
					nextTargetIndex = index;
					
					trace( "BIG: " + nextAnimation + ", " + nextTargetIndex );
					targetBehavior = TARGET_BEHAVIOR_HIT;
				} else if ( performance >= Constants.PRECISION_GOOD ) {
					if ( Math.random() < REPEAT_RATE )
						index = lastIndex;
					else
						index = LABEL_COMMON_FIRST_INDEX + Math.floor( Math.random() * LABELS_COMMON_TOTAL ) as uint;
					
					nextAnimation = animationLabels[ index ];
					nextTargetIndex = index;
					
					trace( "GOOD" );
					targetBehavior = TARGET_BEHAVIOR_HIT;
				} else {
//					nextAnimation = currentAnimation;
					endCurrentSequence();
					headManager.setNextAnimation( HeadManager.FRAME_ANGRY_START );
					nextAnimation = getLabelString( LABEL_BAD, true );
					
					if ( Math.random() < 0.75 ) {
						ball.setTarget( null );
					} else {
						const xOff : Number = ( Constants.STAGE_WIDTH / 2 ) + ball.getInternalWidth() + Math.random() * Constants.STAGE_WIDTH / 2;
						target.x = Math.random() < 0.5 ? -xOff : xOff;
						target.y = -Constants.STAGE_HEIGHT + Math.random() * Constants.STAGE_HEIGHT;
						ball.setTarget( target );
						ball.setActive( true );
					}
					nextTargetIndex = -1;
					
//					SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_BALL_MISS );
					
					targetBehavior = TARGET_BEHAVIOR_RESET;
				}
			}
			lastIndex = nextTargetIndex;
			
			trace( "animation: " + currentAnimation + " -> " + nextAnimation );
		}
		
		
		public function setExpression( happy : Boolean ) : void {
			headManager.setNextAnimation( happy ? HeadManager.FRAME_SMILE_START : HeadManager.FRAME_ANGRY_START );
		}
		
		
		private function endCurrentSequence() : void {
			if ( currentLabel != currentAnimation + LABEL_BACK && currentLabel != currentAnimation + LABEL_BACK + LABEL_END )
				setSequence( currentAnimation + LABEL_BACK );
		}
		
		
		private function setSequence( label : String, play : Boolean = true ) : void {
			trace( "setSequence: " + label + ", " + play );
			gotoAndStop( label );
			setPlaying( play );
			onFrameChanged();
		}
		
		
		private function raiseBall() : void {
			randomNewMove( true );
		}
		
		
		public function resetBall( raise : Boolean ) : void {
			ball.stopMoving();
		}
		
		
		/**
		 * 
		 */
		public function onTargetHit( ball : Ball ) : void {
			if ( checkHit ) {
				// TODO 
			}
			
			trace( "onTargetHit: " + targetBehavior );
			if ( targetBehavior == TARGET_BEHAVIOR_RESET ) {
				resetBall( true );
			}
		}
		
		
		public function onAppear( ball : Ball ) : void {
			setSequence( LABEL_RAISE_BALL );
			raiseBall();
		}
		
		
		public function onStop( ball : Ball ) : void {
			if ( targetBehavior == TARGET_BEHAVIOR_RESET )
				resetBall( true );
		}
		
		
		/**
		 * 
		 */
		public function setBigMode( b : Boolean ) : void {
			ball.setBigMode( b );
		}
		
		
		public function stopMoving( hideBall : Boolean = false, nextAnimation : String = null, killBall : Boolean = false ) : void {
			ball.setTarget( null );
			if ( killBall )
				ball.setListener( null );
			
			targetBehavior = TARGET_BEHAVIOR_STOP;
			endCurrentSequence();
			
			if ( nextAnimation )
				this.nextAnimation = getLabelString( nextAnimation, true );
			else
				this.nextAnimation = null;
			
			if ( hideBall )
				ball.hide();
		}
		
		
		/**
		 * 
		 */
		private function onFrameChanged() : void {
			if ( currentFrame == getFrameHitIndex( currentAnimation ) ) {
				// acertou a bola
				trace( "HIT BALL" );
				if ( nextAnimation == currentAnimation ) {
					switchAnimations( currentAnimation == LABEL_RAISE_BALL, false );
				} else {
					var back : Boolean;
					switch ( currentAnimation ) {
						case LABEL_RAISE_BALL:
						case LABEL_HEADER:
							back = false;
						break;
						
						default:
							back = true;
					}
					if ( back )
						setSequence( currentAnimation + LABEL_BACK );
					
					switchAnimations( !back, false );
				}
			} else if ( playing && currentLabel.indexOf( LABEL_END ) >= 0 ) {
				trace( "END -> " + currentLabel + ", " + targetBehavior );
				setPlaying( false );
				
				switch ( targetBehavior ) {
					case TARGET_BEHAVIOR_HIT:
						if ( currentLabel.indexOf( LABEL_BACK ) >= 0 ) {
							// acabou animação de fim do movimento anterior
							setSequence( getLabelString( currentAnimation, true ), false );
						} else if ( currentLabel.indexOf( LABEL_START ) >= 0 ) {
							setSequence( currentAnimation + LABEL_LOOP );
							setPlaying( false );
						}
					break;
					
					case TARGET_BEHAVIOR_STOP:
						if ( nextAnimation ) {
							setSequence( nextAnimation );
							nextAnimation = null;
						}
					break;
				}
			}
		}
		
		
		/**
		 * 
		 */
		private function switchAnimations( start : Boolean, startSequence : Boolean ) : void {
			if ( startSequence )
				setSequence( getLabelString( nextAnimation, start ), false );
			
			currentAnimation = nextAnimation;
			nextAnimation = null;
			
			if ( nextTargetIndex >= 0 && targetBehavior == TARGET_BEHAVIOR_HIT ) {
				target.x = anchors[ nextTargetIndex ].x;
				target.y = anchors[ nextTargetIndex ].y;
				
				isHeader = nextTargetIndex == LABEL_HEADER_INDEX;
				
				switch ( currentAnimation ) {
					case LABEL_SPECIAL:
					case LABEL_SPECIAL_LEFT:
					case LABEL_SPECIAL_LEFT_1:
					case LABEL_SPECIAL_LEFT_2:
						ball.setTarget( target, Ball.SPEED_INDEX_SPECIAL_LEFT );
					break;
					
					case LABEL_SPECIAL_RIGHT_1:
					case LABEL_SPECIAL_RIGHT_2:
						ball.setTarget( target, Ball.SPEED_INDEX_SPECIAL_RIGHT );
					break;
					
					default:
						ball.setTarget( target );
				}
				ball.setActive( true );
				nextTargetIndex = -1;
			}
			
			randomNewMove();
		}
		
		
		/**
		 * 
		 */
		private function getFrameHitIndex( label : String ) : int { 
			if ( label ) {
				const f : int = frameHitIndexes[ label ]; 
				if ( f )
					return f;
				
				for ( var i : uint = 0; i < currentLabels.length; ++i ) {
					if ( currentLabels[ i ].name.indexOf( label ) >= 0 && currentLabels[ i ].name.indexOf( LABEL_HIT ) >= 0 ) {
						frameHitIndexes[ label ] = currentLabels[ i ].frame;
						return currentLabels[ i ].frame;
					}
				}
			}
			
			return -1;
		}
		
		
		/**
		 * 
		 */
		private function getFrameIndex( label : String ) : int { 
			if ( label ) {
				const f : int = framesIndexes[ label ]; 
				if ( f )
					return f;
				
				for ( var i : uint = 0; i < currentLabels.length; ++i ) {
					if ( currentLabels[ i ].name == label ) {
						framesIndexes[ label ] = currentLabels[ i ].frame;
						return currentLabels[ i ].frame;
					}
				}
			}
			
			framesIndexes[ label ] = -1;
			return -1;
		}
		
		
		/**
		 * 
		 */
		public function setPlaying( p : Boolean ) : void {
			playing = p;
		}
		
		
	}
	
	
}




import flash.display.MovieClip;

internal class HeadManager {
	
	public static const FRAME_BLINK_START : uint		= 1;
	public static const FRAME_BLINK_END : uint 			= 8;
	
	public static const FRAME_SMILE_START : uint 		= 9;
	public static const FRAME_SMILE_END : uint 			= 12;
	public static const FRAME_SMILE_BACK : uint 		= 13;
	public static const FRAME_SMILE_BACK_END : uint 	= 16;
	
	public static const FRAME_ANGRY_START : uint 		= 17;
	public static const FRAME_ANGRY_END : uint 			= 21;
	public static const FRAME_ANGRY_BACK : uint 		= 22;
	public static const FRAME_ANGRY_BACK_END : uint 	= 26;
	
	public static const FRAME_HEADER_START : uint 		= 27;
	public static const FRAME_HEADER_END : uint 		= 41;
	
	public static const FRAMES_TO_HEADER_HIT : uint		= ( FRAME_HEADER_END - FRAME_HEADER_START ) / 2;
	
	private static const BLINK_MIN_TIME : Number = 0.01;
	
	private static const BLINK_MAX_TIME : Number = 5;
	
	private static const EXPRESSION_MIN_TIME : Number = 3;
	
	private static const EXPRESSION_MAX_TIME : Number = 12;
	
	private var nextAnimationTime : Number = 1;
	
	private var head : MovieClip;
	
	private var accTime : Number = 0;
	
	private var playing : Boolean = true;
	
	private var nextAnimation : int = -1;
	
	private var _isDoingHeader : Boolean;
	
	
	public function HeadManager( head : MovieClip ) {
		this.head = head;
	}
	
	
	public function setHead( h : MovieClip ) : void {
		if ( h ) {
			const previousHead : MovieClip = head;
			head = h;
			head.stop();
			if ( previousHead )
				head.gotoAndStop( previousHead.currentFrame );
		}
	}
	
	
	public function update( delta : Number ) : void {
		head.stop();
		if ( playing ) {
			accTime += delta;
			if ( accTime >= Constants.STAGE_FRAME_TIME ) {
				accTime %= Constants.STAGE_FRAME_TIME;
				head.nextFrame();
				onFrameChanged();
			}
		} else {
			if ( nextAnimationTime > 0 ) {
				nextAnimationTime -= delta;
				
				if ( nextAnimationTime <= 0 ) {
					head.gotoAndStop( nextAnimation < 0 ? head.currentFrame + 1 : nextAnimation );
					nextAnimation = -1;
					setPlaying( true );
				}
			}
		}
	}
	
	
	public function doHeader() : void {
		setNextAnimation( FRAME_HEADER_START );
	}
	
	
	public function isDoingHeader() : Boolean {
		return _isDoingHeader;
	}
	
	
	private function onFrameChanged() : void {
		switch ( head.currentFrame ) {
			case FRAME_SMILE_END:
			case FRAME_ANGRY_END:
				nextAnimation = -1;
				nextAnimationTime = EXPRESSION_MIN_TIME + Math.random() * ( EXPRESSION_MAX_TIME - EXPRESSION_MIN_TIME );
				setPlaying( false );
			break;
			
			case FRAME_HEADER_END:
				_isDoingHeader = false;
			case FRAME_BLINK_END:
			case FRAME_SMILE_BACK_END:
			case FRAME_ANGRY_BACK_END:
				nextAnimation = ( nextAnimation < 0 ? FRAME_BLINK_START : nextAnimation );
				nextAnimationTime = BLINK_MIN_TIME + Math.random() * ( BLINK_MAX_TIME - BLINK_MIN_TIME );
				setPlaying( false );
			break;
		}
	}
	
	
	public function setNextAnimation( next : uint ) : void {
		_isDoingHeader = next == FRAME_HEADER_START;
		setPlaying( false );
		nextAnimation = next;
		nextAnimationTime = 0.00001;
	}
	
	
	/**
	 * 
	 */
	public function setPlaying( p : Boolean ) : void {
		playing = p;
	}
	
}




