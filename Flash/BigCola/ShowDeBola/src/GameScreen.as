package
{
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoSounds.SoundManager;
	
	import fl.controls.Button;
	import fl.motion.Color;
	import fl.motion.easing.Bounce;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import org.osmf.events.TimeEvent;
	import org.osmf.media.MediaPlayer;
	
	public final class GameScreen extends AppScene {
		// Intervalo do looping de atualização do jogo
		private static const TIMER_INTERVAL_MILIS : uint = 20;
		
		// Intervalo máximo de atualização permitido
		private static const MAX_UPDATE_INTERVAL_MILIS : int = 125;
		
		/***/
		private static const LINES_TOTAL : uint = 4;

		/***/
		public static const LINES_X_PERCENT : Array = [ -0.45, -0.35, -0.25, -0.15 ];

		/***/
		private static const SCORES : Array = [ 0, 10, 20, 50, 100, 200 ];
		
		/** Termo inicial da PA que regula o crescimento do multiplicador bônus */
		private static const BONUS_PA_A1 : uint = 5;
		
		/** Razão da PA que regula o crescimento do multiplicador bônus. Quanto menor o termo, mais rápido cresce o multiplicador.  */
		private static const BONUS_PA_R : Number = 5;

		/** Variação no marcador Big de acordo com cada resultado de jogada. */
		private static const BIG_LEVEL_DIFF : Array = [ -4, -2, -1, 1, 2, 3 ];
		
		private static const BIG_LEVEL_WIDTH_MAX : uint = 100;
		
		/***/
		private static const DIFFICULTY_LEVEL_MAX : uint = 18;
		
		/***/
		private static const MOVES_PER_LEVEL_EASY : uint = 10;
		
		/***/
		private static const MOVES_PER_LEVEL_HARD : uint = 150;
		
		/***/
		private static const SIMULTANEOUS_CHANCE_EASY : Number = 0.2;
		
		/***/
		private static const SIMULTANEOUS_CHANCE_HARD : Number = 0.5;
		
		private static const LINES_COLOR : Array = [ 0xff0000, 0x00ff00, 0xffffff, 0xffff00 ];
		
		private static const TIME_MESSAGE_DEFAULT : uint = 3;
		
		private static const MAX_SCORE : int = 999999999;
		
		private static const SCORE_CHANGE_TIME : Number = 1.9;
		
		private static const BIG_LEVEL_CHANGE_TIME : Number = 0.4;
		
		private static const BIG_LEVEL_MIN : Number = 0;
		
		private static const BIG_LEVEL_MAX : Number = 100;
		
		private static const BIG_LEVEL_BAR_X : Number = 300;
		
		private static const BIG_LEVEL_BAR_Y : Number = -150;
		
		private static const BOARD_X : Number = Constants.STAGE_RIGHT - 30;
		
		private static const BOARD_Y : Number = Constants.STAGE_TOP + 15;
		
		private static const SIMULTANEOUS_REPEAT_EASY : Number = 1.0;
		
		private static const SIMULTANEOUS_REPEAT_HARD : Number = 0.44;
		
		private static const BIG_LEVEL_SPECIAL : Number = BIG_LEVEL_MIN + ( BIG_LEVEL_MAX - BIG_LEVEL_MIN ) * 0.3;
		
		private static const BIG_MODE_TIME : Number = 15;
		
		private static const MAX_BONUS_MULIPLIER : uint = 99;
		
		private static const RECENT_MIN_MOVES : uint = 7; 
		
		private static const BIG_MODE_MULTIPLIER : Number = 2;
		
		private static const ALPHA_TRANSITION_TIME : Number = 1.0;
		
		private static const GAME_OVER_MESSAGE_TIME : Number = 1600;
		
		private static const COMBO_LABEL_SCALE : Number = 1.3;
		
		private static const COMBO_LABEL_ANIMATION_TIME : Number = 0.3;
		
		private static const PAUSE_BUTTON_WIDTH : Number = 50;

		private static const PAUSE_BUTTON_HEIGHT : Number = 50;
		
		private static const DIFFICULTY_BUTTON_WIDTH : Number = 100;

		private static const DIFFICULTY_BUTTON_HEIGHT : Number = 100;
		
		private static const PAUSE_BUTTON_X : Number = Constants.STAGE_RIGHT - PAUSE_BUTTON_WIDTH - 20;
		
		private static const PAUSE_BUTTON_Y : Number = Constants.STAGE_BOTTOM - PAUSE_BUTTON_HEIGHT - 20;
		
		private static const MESSAGE_BOX_WIDTH : Number = 600 * 0.6;
		
		private static const MESSAGE_BOX_HEIGHT : Number = 168.7 * 0.6;
		
		
		private var gameState : GameState;
		
		private var lines : Vector.<Line>;
		
		private var activeLines : uint;
		
		private var difficultyOffset : uint = 0;
		
		/**
		 * Controla o looping de atualização do jogo 
		 */		
		private var _gameLoopTimer : Timer;
		
		/***/
		private var messageTimer : Timer;
		
		private var messageOnEndCallback : Function;
		
		private var messageOnEndCallbackParams : *;
		
		/**
		 * Momento da última atualização (em milissegundos) 
		 */		
		private var _lastUpdateTime : int;
		
		/***/
		
		private var soundTimer : Timer;
		
		private var alphaTimer : Timer;
		
		/***/
		private var scoreShown : Number = 0;
		
		/***/
		private var scoreSpeed : Number = 0;
		
		/***/
		private var scoreLabel : TextField;
		
		/***/
		private var bonusMultiplier : uint = 1;
		
		/***/
		private var bonusLabel : TextField;
		
		/** Indica quando devemos aumentar o multiplicador bônus */
		private var bonusMultiplierCounter : uint;
		
		private var pressedLines : Array = [ false, false, false, false ];
		
		private var hits : uint = 0;
		
		private var bigLevelBar : MovieClip;
		
		private var bigLevel : Number = 0;
		
		private var bigLevelShown : Number = 0;
		
		private var bigLevelSpeed : Number = 0;
		
		private var _bigMode : Boolean;
		
		private var bigModeRemainingTime : Number;
		
		private var messageLabel : TextField;
		private var messageTween : Tween;
		
		private var hitsLabel : TextField;
		
		private var player : Player;
		
		private var uncheckedMoves : Vector.< uint > = new Vector.< uint >();

		private var paused : Boolean = false;
		
		private var pauseCountdown : int;
		
		private var pauseButton : MovieClip;
		
		private var difficultyBox : MovieClip;
		
		private var board : MovieClip;
		
		private var levelComplete : MovieClip;
		
		private var gameOver : MovieClip;
		
		
		private var levelLabel : TextField;
		
		private var scoreFont : TextFormat;
		private var bonusFont : TextFormat;
		private var hitsFont : TextFormat;
		private var messageFont : TextFormat;
		private var levelFont : TextFormat;		
		
		private var pausePopUp : PopupPause;
		private var btPauseYes : GenericButton;
		private var btPauseNo : GenericButton;
		private var btPauseOn : GenericButton;
		private var btPauseOff : GenericButton;	
		
		private var level : uint;
		private var biggestCombo : uint = 0;
		private var perfectMoves : uint = 0;
		private var score : Number = 0;	
		
		private static var static_score : Number;
		private static var static_biggestCombo : uint;
		private static var static_perfectMoves : uint;
		private static var static_level : uint;
		private static var recorSended : Boolean = true;
		
		public function GameScreen() {
			super( null, false, true );
		}		
		
		private function onClickHandler( e : Event ) : void {
			if ( gameState == GameState.PLAYING )
				setPaused( !paused );
		}
		
		
		private function getDifficultyBox() : MovieClip {
			const d : MovieClip = new Dificuldade();
			
			for ( var i : int = 0; i < 3; ++i ) {
				var b : DisplayObject;
				switch ( i ) {
					case 0:
						b = new BotaoEasy();
						b.addEventListener(MouseEvent.CLICK, onStartEasy );
					break;
					
					case 1:
						b = new BotaoMedium();
						b.addEventListener(MouseEvent.CLICK, onStartMedium );
					break; 
				
					case 2:
						b = new BotaoHard();
						b.addEventListener(MouseEvent.CLICK, onStartHard );
					break;
				}				
				
				b.width = DIFFICULTY_BUTTON_WIDTH;
				b.height = DIFFICULTY_BUTTON_HEIGHT;
				
				b.x = ( ( d.width - b.width ) / 2 ) - 21;
				b.y = 100 + ( i * 1.1 * b.height );
				d.addChild( b );
			}
			
			d.scaleX = 0.7;
			d.scaleY = 0.7;
			
			return d;
		}
		
		
		private function onStartEasy( e : Event = null ) : void {
			difficultyOffset = 0;
			start();
		}
		
		
		private function onStartMedium( e : Event = null ) : void {
			difficultyOffset = DIFFICULTY_LEVEL_MAX * 0.33;
			start();
		}
		
		
		private function onStartHard( e : Event = null ) : void {
			difficultyOffset = DIFFICULTY_LEVEL_MAX * 0.66;
			start();
		}
		
		
		private function start() : void {
			difficultyBox.mouseEnabled = false;
			difficultyBox.mouseChildren = false;
			
			TweenManager.tween( difficultyBox, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			TweenManager.tween( board, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
			TweenManager.tween( player, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
			TweenManager.tween( pauseButton, "alpha", Regular.easeOut, 0, 1, ALPHA_TRANSITION_TIME );
			prepare( 1 );
			startRunning();
		}
		
		
		protected override function onAddedToStage( e : Event ) : void {
			super.onAddedToStage( e );
			
			_gameLoopTimer = new Timer( TIMER_INTERVAL_MILIS );
			_gameLoopTimer.addEventListener( TimerEvent.TIMER, gameLoop );
			
			mouseEnabled = true;
			mouseChildren = true;
			
			stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			
			lines = new Vector.<Line>();
			for ( var i : uint = 0; i < LINES_TOTAL; ++i ) {
				var line : Line = new Line( i, onHit ); 
				line.x = Constants.STAGE_WIDTH * LINES_X_PERCENT[ i ];
				line.y = -Constants.STAGE_HEIGHT / 2;
				
				lines.push( line );
				addChild( line );
			}
			
			player = new Player( getRecentPerformance );
			player.alpha = 0;
			addChild( player );
			player.x = 120;
			//player.y = player.y + 30;
			
			pauseButton = new BotaoPause();
			pauseButton.x = PAUSE_BUTTON_X;
			pauseButton.y = PAUSE_BUTTON_Y;
			pauseButton.width = PAUSE_BUTTON_WIDTH;
			pauseButton.height = PAUSE_BUTTON_HEIGHT;
			pauseButton.buttonMode = true;
			pauseButton.useHandCursor = true;
			pauseButton.addEventListener(MouseEvent.CLICK, onClickHandler);
			pauseButton.alpha = 0;
			addChild( pauseButton );
			
			bigLevelBar = new MovieClip();
			bigLevelBar.stop();
			bigLevelBar.x = BIG_LEVEL_BAR_X;
			bigLevelBar.y = BIG_LEVEL_BAR_Y;
			addChild( bigLevelBar );
			
			board = new Placar();
			board.alpha = 0;
			board.x = BOARD_X;
			board.y = BOARD_Y;
			addChild( board );
			
			var florida : FloridaProjectOne = new FloridaProjectOne();
			
			scoreFont = new TextFormat();
			scoreFont.font = florida.fontName;
			scoreFont.size = ( 30 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
			scoreFont.align = TextFormatAlign.RIGHT;
			
			scoreLabel = getLabel();
			scoreLabel.defaultTextFormat = scoreFont;
			scoreLabel.embedFonts = true;
			scoreLabel.antiAliasType = AntiAliasType.ADVANCED;
			board.addChild( scoreLabel );
			
			bonusFont = new TextFormat();
			//bonusFont.color = 0x77eaff;
			bonusFont.font = florida.fontName;
			bonusFont.align = TextFormatAlign.RIGHT;
			bonusFont.size = ( 40 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );

			bonusLabel = getLabel();
			bonusLabel.defaultTextFormat = bonusFont;
			bonusLabel.embedFonts = true;
			bonusLabel.antiAliasType = AntiAliasType.ADVANCED;
			board.addChild( bonusLabel );
			
			setBonusMultiplier( 0 );
			
			hitsFont = new TextFormat();
			hitsFont.font = florida.fontName;
			hitsFont.size = ( 40 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
			hitsFont.align = TextFormatAlign.RIGHT;
			
			hitsLabel = getLabel();
			hitsLabel.defaultTextFormat = hitsFont;
			hitsLabel.embedFonts = true;
			hitsLabel.antiAliasType = AntiAliasType.ADVANCED;
			board.addChild( hitsLabel );
						
			levelFont = new TextFormat();
			levelFont.font = florida.fontName;
			levelFont.size = ( 45 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
			levelFont.align = TextFormatAlign.CENTER;
			
			levelLabel = getLabel();
			levelLabel.x = -( 85 * board.scaleX );
			levelLabel.y = ( 13 * board.scaleY );
			levelLabel.width = ( 75 * board.scaleX );
			levelLabel.defaultTextFormat = levelFont;
			levelLabel.embedFonts = true;
			levelLabel.antiAliasType = AntiAliasType.ADVANCED;
			board.addChild( levelLabel );
			
			messageFont = new TextFormat();
			messageFont.font = florida.fontName;
			messageFont.size = 50;
			messageFont.align = TextFormatAlign.CENTER;
			
			messageLabel = getLabel();
			messageLabel.defaultTextFormat = messageFont;
			messageLabel.embedFonts = true;
			messageLabel.width = Constants.STAGE_WIDTH;
			messageLabel.x = Constants.STAGE_LEFT;
			messageLabel.antiAliasType = AntiAliasType.ADVANCED;
			addChild( messageLabel );
			
			levelComplete = new QuadroGanhou();
			levelComplete.width = MESSAGE_BOX_WIDTH;
			levelComplete.height = MESSAGE_BOX_HEIGHT;
			levelComplete.alpha = 0;
			levelComplete.x = -levelComplete.width / 2;
			levelComplete.y = -levelComplete.height / 2;
			addChild( levelComplete );
			
			gameOver = new QuadroPerdeu();
			gameOver.width = MESSAGE_BOX_WIDTH;
			gameOver.height = MESSAGE_BOX_HEIGHT;
			gameOver.alpha = 0;
			gameOver.x = -gameOver.width / 2;
			gameOver.y = -gameOver.height / 2;
			addChild( gameOver );
			
			pausePopUp = new PopupPause();
			addChild( pausePopUp );			
			pausePopUp.visible = false;
			
			pausePopUp.removeChild( pausePopUp.BtSounds );
			btPauseOn = new GenericButton( "Ligado", 1, disableSound, GenericButton.BUTTON_GREEN );
			btPauseOff = new GenericButton( "Desligado", 1, enableSound, GenericButton.BUTTON_PURPLE );
			pausePopUp.addChild( btPauseOn );
			pausePopUp.addChild( btPauseOff );
			btPauseOff.x = btPauseOn.x = pausePopUp.BtSounds.x;
			btPauseOff.y = btPauseOn.y = pausePopUp.BtSounds.y;
			updateSound();
			
			pausePopUp.removeChild( pausePopUp.BtYes );
			btPauseYes = new GenericButton( "Sim", 1, exitGame, GenericButton.BUTTON_GREEN );
			pausePopUp.addChild( btPauseYes );
			btPauseYes.x = pausePopUp.BtYes.x;
			btPauseYes.y = pausePopUp.BtYes.y;
			
			pausePopUp.removeChild( pausePopUp.BtNo );
			btPauseNo = new GenericButton( "Não", 1, backToGame, GenericButton.BUTTON_GREEN );
			pausePopUp.addChild( btPauseNo );
			btPauseNo.x = pausePopUp.BtNo.x;
			btPauseNo.y = pausePopUp.BtNo.y;
			
			pausePopUp.btClose.addEventListener( MouseEvent.CLICK, backToGameEvent );
			
			setState( GameState.CHOOSE_DIFFICULTY );
		}
		
		
		private function backToGameEvent( e : Event ) : void { backToGame( null ); }
		private function backToGame( clickedBt : GenericButton ) : void { setPaused( false ); }
		
		
		private function disableSound( clickedBt : GenericButton ) : void {
			SoundManager.GetInstance().setMute( true );
			updateSound();
		}
		
		
		private function enableSound( clickedBt : GenericButton ) : void {
			SoundManager.GetInstance().setMute( false );
			updateSound();
		}
		
		
		private function exitGame( clickedBt : GenericButton ) : void {
			prepareRanking();
			
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
		}
		
		/** Se o jogador está logado tenta submeter novos recordes */
		public static function submitRanking() : void {
			if ( Application.GetInstance().getLoggedProfile() != null && !recorSended ) {
				Application.submitRecords( static_score, static_biggestCombo, static_perfectMoves, static_level );
				recorSended = true;
			}
		}
		
		private function prepareRanking() : void {
			recorSended = false;
			static_score = score;
			static_biggestCombo = biggestCombo;
			static_perfectMoves = perfectMoves;
			static_level = level;
			
			if( Application.GetInstance().getLoggedProfile() != null ) {
				submitRanking();
			}
		}
		
		
		private function updateSound() : void {
			var b : Boolean = SoundManager.GetInstance().isMute();
			btPauseOn.visible = !b;
			btPauseOff.visible = b;
		}
		
		
		protected override function onRemovedFromStage( e : Event ) : void {
			super.onRemovedFromStage( e );
			
			for ( var i : uint = 0; i < LINES_TOTAL; ++i )
				lines[ i ] = null;
			lines = null;
			
			player = null;
			pauseButton = null;
			bigLevelBar = null;
			board = null;
			scoreFont = null;
			scoreLabel = null;
			bonusFont = null;
			bonusLabel = null;
			hitsFont = null;
			hitsLabel = null;
			levelFont = null;
			levelLabel = null;
			messageFont = null;
			messageLabel = null; 
			levelComplete = null;
			gameOver = null;
			
			if( _gameLoopTimer ) {
				_gameLoopTimer.stop();
				_gameLoopTimer.removeEventListener( TimerEvent.TIMER, gameLoop );
				_gameLoopTimer = null;
			}
			
			mouseEnabled = false;
			mouseChildren = false;
			
			stage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
		}
		
		
		private function showMessage( message : String, time : Number = TIME_MESSAGE_DEFAULT, onEndCallback : Function = null, onEndCallbackParams : * = null ) : void {
			if ( messageTimer )
				onMessageTimeEnded();

			if ( time > 0 ) {
				messageOnEndCallback = onEndCallback;
				messageOnEndCallbackParams = onEndCallbackParams;
				
				messageTimer = new Timer( time * 1000 );
				messageTimer.addEventListener( TimerEvent.TIMER, onMessageTimeEnded );
				messageTimer.start();
			}
			
			messageLabel.text = message;
			doMessageTween( 1 );
		}
		
		
		private function cancelMessageTween() : void {
			if ( messageTween != null ) {
				messageTween.stop();
				messageTween = null;
			}
		} 
		
		
		private function doMessageTween( finalValue : Number ) : void {
			cancelMessageTween();
			if( messageLabel != null ) 
				messageTween = TweenManager.tween( messageLabel, "alpha", Regular.easeInOut, messageLabel.alpha, finalValue, ALPHA_TRANSITION_TIME );
		}
		
		
		private function onMessageTimeEnded( e : TimerEvent = null ) : void {
			doMessageTween( 0 );
			
			if ( messageTimer ) {
				messageTimer.stop();
				messageTimer.removeEventListener( TimerEvent.TIMER, onMessageTimeEnded );
				messageTimer = null;
				
				if ( messageOnEndCallback != null ) {
					const p : * = messageOnEndCallbackParams;
					const f : Function = messageOnEndCallback;
					
					messageOnEndCallback = null;
					messageOnEndCallbackParams = null;
					
					f.call( this, p );
				}
			}
		}
		
		
		/**
		 * Inicia o looping de atualização do jogo  
		 */		
		public function startRunning() : void {
			// Inicia o looping do jogo
			_gameLoopTimer.start();
		}
		
		
		private function changeScore( points : Number ) : void {
			// Estourou a precisão, pois nesse jogo a pontuação nunca diminui
			const nextScore : Number = score + points;
			if( nextScore < score ) {
				score = MAX_SCORE;
			} else {
				score = nextScore;
				
				if( score > MAX_SCORE )
					score = MAX_SCORE;
				else if( score < 0 )
					score = 0;
			}
			
			const diff : Number = score - scoreShown;
			
			// Opa! Houve um erro...
			if( diff < 0 )
				return;
			
			scoreSpeed = diff / SCORE_CHANGE_TIME;
		}		
		
		
		private function changeBigLevel( diff : Number ) : void {
			if ( !bigMode ) {
				bigLevel = MathFuncs.clamp( bigLevel + diff, BIG_LEVEL_MIN, BIG_LEVEL_MAX );
				
				if ( bigLevel >= BIG_LEVEL_MAX ) {
					// ativa modo Big
					bigLevel = BIG_LEVEL_SPECIAL;
					bigMode = true;
					player.setExpression( true );
					bigLevelSpeed = ( bigLevel - bigLevelShown ) / BIG_MODE_TIME;
				} else {
					bigLevelSpeed = ( bigLevel - bigLevelShown ) / BIG_LEVEL_CHANGE_TIME;
				}
			}
		}
		
		
		/**
		 * Atualiza o label da pontuação
		 * @param delta Tempo decorrido desde a última chama ao método
		 * @param changeNow Indica se a pontuação mostrada deve ser automaticamente igualada à pontuação real
		 */
		public function updateScore( delta : Number, changeNow : Boolean ) : void {
			if( scoreShown != score || changeNow ) {
				if( changeNow ) {
					scoreShown = score;
				} else {
					const dp : Number = scoreSpeed * delta;
					
					// Opa! Houve um erro...
					if( dp < 0 ) {
						scoreShown = score;
					} else {
						const nextScoreShown : Number = scoreShown + dp;
						
						if( ( nextScoreShown < scoreShown ) || ( nextScoreShown >= score ) )
							scoreShown = score;
						else
							scoreShown = nextScoreShown;
					}
				}
				scoreLabel.text = Math.floor( scoreShown ).toString();
				scoreLabel.x = -( 360 * board.scaleX );
				scoreLabel.y = -( 3 * board.scaleY );
				scoreLabel.width = ( 260 * board.scaleX );
				scoreLabel.height = ( 30 * board.scaleY );
//				scoreLabel.x = left + scoreLabel.textWidth / 2; 
//				scoreLabel.y = ( -Constants.STAGE_HEIGHT / 2 ) + scoreLabel.height / 2; 
			}
		}
		
		
		public function updateBigLevel( delta : Number, changeNow : Boolean ) : void {
			if ( bigMode ) {
				for ( var bla : int = 0; bla < activeLines; ++bla )
					lines[ bla ].setBigLevel( 1 );
				
				bigModeRemainingTime -= delta;
				if ( bigModeRemainingTime <= 0 )
					bigMode = false;
			} else if ( bigLevelShown != bigLevel || changeNow ) {
				if ( changeNow ) {
					bigLevelShown = bigLevel;
				} else {
					const dp : Number = bigLevelSpeed * delta;
					
					const nextBigLevelShown : Number = bigLevelShown + dp;
					if ( ( bigLevel > bigLevelShown && nextBigLevelShown >= bigLevel ) || ( bigLevel < bigLevelShown && nextBigLevelShown <= bigLevel ) ) {
						bigLevelShown = bigLevel;
					} else {
						bigLevelShown = nextBigLevelShown;
					}
				}
				const bigPercent : Number = MathFuncs.clamp( bigLevelShown / BIG_LEVEL_MAX, 0, 1 );
				for ( var i : int = 0; i < activeLines; ++i )
					lines[ i ].setBigLevel( bigPercent );
			}
		}
		
		
		public function onHit( lineIndex : uint, precisionLevel : uint, moveHits : uint = 1 ) : void {
			if ( bigMode )
				moveHits *= BIG_MODE_MULTIPLIER;
			
			changeScore( SCORES[ precisionLevel ] * currentBonusMultiplier() * moveHits );
			uncheckedMoves.push( precisionLevel );
			
			switch ( precisionLevel ) {
				case Constants.PRECISION_MISSED:
				case Constants.PRECISION_TERRIBLE:
					setHits( 0 );
					bonusMultiplierCounter = 0;
					setBonusMultiplier( 1 );
				case Constants.PRECISION_BAD:
				break;
				
				case Constants.PRECISION_PERFECT:
					++perfectMoves;
					// No Breaks
				default:
					setHits( hits + 1 );
					incBonusCounter( precisionLevel * ( bigMode ? BIG_MODE_MULTIPLIER : 1 ) );
			}
			
			changeBigLevel( BIG_LEVEL_DIFF[ precisionLevel ] );
			
			if ( gameState == GameState.PLAYING && bigLevel <= BIG_LEVEL_MIN ) {
				setState( GameState.GAME_OVER_MESSAGE );
			}
		}
		
		
		public function getRecentPerformance() : Number {
			if ( bigMode )
				return Constants.PRECISION_BIG;
			
			const total : uint = uncheckedMoves.length;
			
			if ( total >= RECENT_MIN_MOVES ) {
				var sum : Number = 0;
				while ( uncheckedMoves.length > 0 )
					sum += uncheckedMoves.shift();
				
				return sum / total;
			}
			
			return Constants.PRECISION_GOOD;
		}
		
		
		private function setHits( hits : uint ) : void {
			this.hits = hits;
			hitsLabel.visible = hits > 0;
			hitsLabel.text = hits.toString();
			hitsLabel.x = -( 230 * board.scaleX );
			hitsLabel.y = ( 35 * board.scaleY );
			hitsLabel.width = ( 120 * board.scaleX );
			//var color : AnimatedColor = new AnimateColor();
			hitsFont.color = hitsColor( hits );
			hitsLabel.defaultTextFormat = hitsFont;
			
			if ( hits > biggestCombo ) 
				biggestCombo = hits;
		}
		
		
		private function hitsColor( value : uint ) : uint {
			return ( ( ( value < 150 ) ? ( 250 - ( Math.abs( value - 50 ) * 250 / 100 ) ) : 0 ) << 16 ) | // calculando vermelho
					( ( ( value < 150 ) ? ( 255 - ( value * 255 / 150 ) ) : 0 ) << 8 ) | // calculando fator verde
						( ( ( value < 75 ) ? ( 75 - value ) : 0 ) ); // calculando fator azul
		}
		
		
		private function reduceBonusLabel( e : Event ) : void {
			TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, COMBO_LABEL_SCALE, 1, COMBO_LABEL_ANIMATION_TIME );
			TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, COMBO_LABEL_SCALE, 1, COMBO_LABEL_ANIMATION_TIME );
		}
		
		private function raiseBonusLabel( e : Event ) : void {
			TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, 2 - ( COMBO_LABEL_SCALE ), 1, COMBO_LABEL_ANIMATION_TIME );
			TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, 2 - ( COMBO_LABEL_SCALE ), 1, COMBO_LABEL_ANIMATION_TIME );
		}
		
		
		private function incBonusCounter( hitStatus : uint ) : void {
			trace( "GameScreen.incBonusCounter: " + hitStatus + ", " + bonusMultiplier + "x" );
			if ( bonusMultiplier < MAX_BONUS_MULIPLIER ) {
				if ( hitStatus > 0 ) {
					bonusMultiplierCounter += hitStatus;
					const nextBonusMultiplier : uint = getPATerm( BONUS_PA_A1, BONUS_PA_R, bonusMultiplier );
					if (bonusMultiplierCounter > nextBonusMultiplier) {
						bonusMultiplierCounter -= nextBonusMultiplier;
						setBonusMultiplier(bonusMultiplier + 1);
					}
				}
			}
		}
		
		
		private function getPATerm( a1 : uint, r : uint, n : uint ) : uint {
			return a1 + ( ( n - 1 ) * r );
		}
		
		
		private function updateBonusText() : void {
			if( bigMode ) {
				bonusFont.color = 0x963118;
			} else {
				bonusFont.color = 0xffffff;				
			}
			bonusLabel.defaultTextFormat = bonusFont;
			bonusLabel.text = currentBonusMultiplier().toString();
		}
		
		
		private function repositionBonusLabel() : void {
			bonusLabel.x = -( 455 * board.scaleX );
			bonusLabel.y = ( 38 * board.scaleY );
			bonusLabel.width = ( 48 * board.scaleX );
		}
		
		
		private function currentBonusMultiplier() : uint {
			return bonusMultiplier * ( ( bigMode ) ? BIG_MODE_MULTIPLIER : 1 );
		}
		
		
		/** Determina um novo multiplicador bônus */
		public function setBonusMultiplier( newBonusMultiplier : uint ) : void {
			if ( newBonusMultiplier <= 1)  {
				if (bonusMultiplier != newBonusMultiplier) {
					bonusMultiplier =  newBonusMultiplier;
					repositionBonusLabel();
					
					var t1 : Tween = TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, 1, 2 - ( COMBO_LABEL_SCALE ), COMBO_LABEL_ANIMATION_TIME );
					t1.addEventListener( TweenEvent.MOTION_FINISH, raiseBonusLabel );
					TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, 1, 2 - ( COMBO_LABEL_SCALE ), COMBO_LABEL_ANIMATION_TIME );
				}
				bonusMultiplier = 1;
				updateBonusText();
				
			} else {
				if (newBonusMultiplier > MAX_BONUS_MULIPLIER) {
					newBonusMultiplier = MAX_BONUS_MULIPLIER;
				}
				
				if (bonusMultiplier != newBonusMultiplier) {
					bonusMultiplier =  newBonusMultiplier;
					updateBonusText();
					repositionBonusLabel();
				}
				
				var t2 : Tween = TweenManager.tween( bonusLabel, "scaleX", Regular.easeOut, 1, COMBO_LABEL_SCALE, COMBO_LABEL_ANIMATION_TIME );
				t2.addEventListener( TweenEvent.MOTION_FINISH, reduceBonusLabel );
				TweenManager.tween( bonusLabel, "scaleY", Regular.easeOut, 1, COMBO_LABEL_SCALE, COMBO_LABEL_ANIMATION_TIME );
			}
		}
		
		
		/**
		 * 
		 */
		private function setState( newState : GameState ) : void {
			trace( "GameScreen.setState: " + gameState + " -> " + newState );
			
			gameState = newState;
			switch ( newState ) {
				case GameState.CHOOSE_DIFFICULTY:
					difficultyBox = getDifficultyBox();
					difficultyBox.sacleX = 0.75;
					difficultyBox.sacleY = 0.75;
					difficultyBox.x = ( -difficultyBox.width/2 ) - 115;
					difficultyBox.y = ( -difficultyBox.height/2 ) - 40;
					TweenManager.tween( difficultyBox, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
					addChild( difficultyBox );
				break;
				
				case GameState.GAME_OVER_MESSAGE:
					for ( var i : int = 0; i < activeLines; ++i )
						lines[ i ].gameOver();
					
					player.stopMoving( false, Player.LABEL_BAD, true );
					player.setExpression( false );
					bigMode = false;
		
					var nextState : GameState;					
					const profile : NOCustomer = Application.GetInstance().getLoggedProfile();
					var f : Function;
					
					prepareRanking();
					
					if ( profile != null ) {
						f = gameOverTimer;
					} else {
						f = loginMessageTimer;
					}
					
					const gameOverTween : Tween = TweenManager.tween( gameOver, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
					gameOverTween.addEventListener( TweenEvent.MOTION_FINISH, f );
					
					TweenManager.tween( board, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
					TweenManager.tween( pauseButton, "alpha", Regular.easeOut, 1, 0, ALPHA_TRANSITION_TIME );
					
					SoundManager.GetInstance().stopSound( Constants.SOUND_MUSIC_GAME );
					SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_GAME_OVER );
				break;
				
				case GameState.LOGIN_MESSAGE:
					Application.GetInstance().showPopUp( "Que tal enviar seus pontos e competir com os melhores jogadores do mundo? É rápido e fácil!", "Show de bola!", onLoginButtonYes, "Quem sabe outra hora...", onLoginButtonNo );
				break;
				
				case GameState.GAME_OVER:
					Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
				break;

				case GameState.LEVEL_COMPLETE:
					bigMode = false;
					player.stopMoving( true, Player.LABEL_GOOD, false );
					player.setExpression( true );
					
					const levelCompleteTween : Tween = TweenManager.tween( levelComplete, "alpha", Regular.easeInOut, 0, 1, ALPHA_TRANSITION_TIME );
					levelCompleteTween.addEventListener( TweenEvent.MOTION_FINISH, prepareNextLevel );
					
					SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_LEVEL_COMPLETE );
				break;
				
				case GameState.PLAYING:
					setPaused( false );
					for ( var l : uint = 0; l < LINES_TOTAL; ++l )
						pressedLines[ l ] = false;
					
					playTheme();
				break;
				
				case GameState.START_LEVEL:
					showMessage( "Nível " + level, level <= 1 ? TIME_MESSAGE_DEFAULT * 1.5 : TIME_MESSAGE_DEFAULT, setState, GameState.PLAYING );
					player.resetBall( true );
					playTheme();
				break;
			}
		}
		
		
		private function playTheme() : void {
			if ( !SoundManager.GetInstance().isPlaying( Constants.SOUND_MUSIC_GAME ) )
				SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_MUSIC_GAME, 0, 9999 );
		}
		
		
		private function gameOverTimer( e : Event = null ) : void {
			if ( alphaTimer != null ) {
				alphaTimer.stop();
				alphaTimer = null;
			}
			
			alphaTimer = new Timer( GAME_OVER_MESSAGE_TIME );
			alphaTimer.addEventListener( TimerEvent.TIMER, setGameOverState );
			alphaTimer.start();
		}
		
		
		private function loginMessageTimer( e : Event = null ) : void {
			if ( alphaTimer != null ) {
				alphaTimer.stop();
				alphaTimer = null;
			}
			
			alphaTimer = new Timer( GAME_OVER_MESSAGE_TIME );
			alphaTimer.addEventListener( TimerEvent.TIMER, setLoginMessageState );
			alphaTimer.start();
		}
		
		
		private function setGameOverState( e : Event = null ) : void {
			alphaTimer.stop();
			
			const t : Tween = TweenManager.tween( gameOver, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			t.addEventListener( TweenEvent.MOTION_FINISH, function() : void {
				setState( GameState.GAME_OVER );
			} );
		}
		
		
		private function setLoginMessageState( e : Event = null ) : void {
			alphaTimer.stop();

			const t : Tween = TweenManager.tween( gameOver, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			t.addEventListener( TweenEvent.MOTION_FINISH, function() : void {
				setState( GameState.LOGIN_MESSAGE );
			} );
		}
		
		private function goToLoginScreen() : void { }
		
		
		public function getState() : GameState {
			return gameState;
		}
		
		
		public function onLoginButtonYes() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_LOGIN );
		}
		
		
		public function onLoginButtonNo() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH ); 
		}
		
		
		private function onKeyDown( e : KeyboardEvent  ) : void
		{
			switch ( gameState ) {
				case GameState.PLAYING:
					switch ( String.fromCharCode( e.charCode ).toLowerCase() ) {						
						case 'p':
							setPaused( !paused );
							break;
					}
					
					if ( !paused ) {
						const lineIndex : int = getLineIndexForKey( e.charCode );
						
						if ( lineIndex >= 0 && !pressedLines[ lineIndex ] ) {
							pressedLines[ lineIndex ] = true;
							lines[ lineIndex ].onPressed();
						}
					}
				break;
			}
		}
		
		
		private function onKeyUp( e : KeyboardEvent  ) : void
		{
			switch ( gameState ) {
				case GameState.PLAYING:
					if ( !paused ) {
						const lineIndex : int = getLineIndexForKey( e.charCode );
						
						if ( lineIndex >= 0 && pressedLines[ lineIndex ] ) {
							pressedLines[ lineIndex ] = false;
							lines[ lineIndex ].onReleased();
						}
					}
				break;
			}
		}
		
		
		private function onPauseTimer( e : TimerEvent ) : void {
			--pauseCountdown;
			if ( pauseCountdown <= 0 ) {
				paused = false;
				pauseButton.visible = true;
			} else {
				showMessage( String( pauseCountdown ) + "...", 1, onPauseTimer );
			}
		}
		
		
		private function setPaused( p : Boolean ) : void {
			if ( p != paused ) {
				if ( p ) {
					paused = true;
					pausePopUp.visible = true;
					pauseButton.visible = false;
					SoundManager.GetInstance().stopSound( Constants.SOUND_MUSIC_GAME );
				} else {
					playTheme();
					pauseButton.visible = false;
					pausePopUp.visible = false;
					pauseCountdown = 3;
					showMessage( String( pauseCountdown ) + "...", 1, onPauseTimer );
				}
			}
		}
		
		
		/**
		 * Looping do jogo. Atualiza objetos e trata colisões. A renderização é feita pela AVM. 
		 * @param e Dados sobre o evento do timer que controla o looping
		 */		
		private function gameLoop( e : TimerEvent ) : void
		{
			if(	_lastUpdateTime == 0 )
				_lastUpdateTime = getTimer();
			
			// Calcula o tempo transcorrido real
			var currTime : int = getTimer();
			var timeElapsed : int = currTime - _lastUpdateTime;
			
			if( timeElapsed > Constants.MAX_UPDATE_INTERVAL_MILIS )
				timeElapsed = Constants.MAX_UPDATE_INTERVAL_MILIS;
			
			// Atualiza os objetos da cena
			update( ( timeElapsed as Number ) / 1000.0 );
			
			_lastUpdateTime = getTimer();
		}
		
		
		private function prepareNextLevel( e : Event = null ) : void {
			TweenManager.tween( levelComplete, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
			prepare( level + 1 );
		}
		
		
		private function prepare( level : uint ) : void {
			this.level = level;
			levelLabel.text = level.toString();
			const difficultyLevel : Number = MathFuncs.clamp( ( level - 1 + difficultyOffset ) / DIFFICULTY_LEVEL_MAX, 0, 1 );
			
			const previousActiveLines : uint = activeLines;
			activeLines = MathFuncs.lerp( 1.8, LINES_TOTAL, Math.min( difficultyLevel * 1.45, 1.0 ) );
			
			for ( var i : uint = 0; i < activeLines; ++i )
				lines[ i ].prepare( difficultyLevel );
			
			for ( i = previousActiveLines; i < activeLines; ++i )
				lines[ i ].start();
			
			const totalMoves : uint = MathFuncs.lerp( MOVES_PER_LEVEL_EASY, MOVES_PER_LEVEL_HARD, difficultyLevel );
			
			const maxSimultaneous : uint = Math.min( activeLines, MathFuncs.lerp( 1.7, LINES_TOTAL + 0.5, difficultyLevel ) );
			
			for ( var l : uint = 0; l < LINES_TOTAL; ++l ) {
				lines[ l ].visible = l < activeLines;
			}
			
			const maxHits : Number = MathFuncs.lerp( Move.HITS_MIN, Move.HITS_MAX, difficultyLevel );
			
			const repeatSimultaneous : Number = MathFuncs.lerp( SIMULTANEOUS_REPEAT_EASY, SIMULTANEOUS_REPEAT_HARD, difficultyLevel );
			
			const simultaneousChance : Number = MathFuncs.lerp( SIMULTANEOUS_CHANCE_EASY, SIMULTANEOUS_CHANCE_HARD, difficultyLevel );
			
			trace( "PREPARING FOR LEVEL " + level + " -> " + ( difficultyLevel * 100.0 ) + "%\nMAX LINES: " + activeLines + ", MAX SIM: " + maxSimultaneous );
			
			var y : int = 0;
			for ( var m : uint = 0; m < totalMoves; ) {
				const quantity : uint = ( Math.random() <= simultaneousChance ? Math.ceil( Math.random() * maxSimultaneous ) : 1 ) as uint;
				trace( "simultâneos: " + quantity );
				var yMax : Number = y;
				
				var usedLines : Array = [ false, false, false, false ];
				var moves : Array = new Array();
				for ( var current : uint = 0; current < quantity; ++current ) {
					var lineIndex : uint = Math.floor( Math.random() * activeLines ) as uint;
					while ( usedLines[ lineIndex ] ) {
						lineIndex = ( lineIndex + 1 ) % activeLines;
					}
					usedLines[ lineIndex ] = true;
					
					trace( m + " -> " + lineIndex + ": " + y );
					
					var move : Move;
					if ( current > 0 && Math.random() < repeatSimultaneous ) {
						const copyIndex : uint = Math.floor( Math.random() * current );
						
						move = new Move( lines[ lineIndex ], LINES_COLOR[ lineIndex ], moves[ copyIndex ].getType(), moves[ copyIndex ].getTotalHits(), difficultyLevel );
					} else {
						move = new Move( lines[ lineIndex ], LINES_COLOR[ lineIndex ], Move.MOVE_TYPE_RANDOM, Move.HITS_MIN + Math.random() * maxHits, difficultyLevel );
					}
					moves.push( move );
					yMax = Math.max( yMax, lines[ lineIndex ].insertMove( move, y ) );
				}
				
				y = yMax;
				m += quantity;
			}
			
			changeBigLevel( ( ( BIG_LEVEL_MAX - BIG_LEVEL_MIN ) / 2 ) - bigLevel );
			updateScore( 0, true );
			setState( GameState.START_LEVEL );
		}
		
		
		private function set bigMode( b : Boolean ) : void {
			if( _bigMode != b ) {
				for ( var i : uint = 0; i < activeLines; ++i ) {
					lines[i].setBigMode( b );
				}
				_bigMode = b;
				
				if ( b ) {
					bigModeRemainingTime = BIG_MODE_TIME;
					soundTimer = new Timer( 1000 );
					soundTimer.addEventListener( TimerEvent.TIMER, bigSound );
					soundTimer.start();
				}
				updateBonusText();
			}
			
			player.setBigMode( b );
		}
		
		
		private function bigSound( e : Event ) : void {
			SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_BIG_MODE );
			soundTimer.stop();
		}
		
		
		private function get bigMode() : Boolean {
			return _bigMode;
		}
		
		
		/**
		 * Atualiza os objetos da cena do jogo
		 * @param timeElapsed Tempo transcorrido dede a última atualização 
		 */
		private function update( timeElapsed : Number ) : void
		{
			if ( !paused ) {
				// trace( "TimeElapsed: " + timeElapsed );
				
				// TODO : Seria melhor e mais otimizado se utilizássemos o padrão STRATEGY. Poderíamos armazenar os métodos
				// de atualização em ponteiros para função / functors e modificar o método de atualização atual apenas alterando
				// o valor de uma variável em setState. Assim não teríamos que executar este switch toda vez qua chamamos update
				switch( gameState )
				{
					case GameState.PLAYING:
					case GameState.GAME_OVER:
					case GameState.GAME_OVER_MESSAGE:
						var over : uint = 0;
						for ( var i : uint = 0; i < activeLines; ++i ) {
							lines[ i ].update( timeElapsed );
							if ( lines[ i ].isOver() )
								++over;
						}
						
						if ( gameState == GameState.PLAYING && over >= activeLines && gameState == GameState.PLAYING )
							setState( GameState.LEVEL_COMPLETE );
					break;
				}
				
				player.update( timeElapsed );
				
				updateScore( timeElapsed, false );
				updateBigLevel( timeElapsed, false );
			}
		}
		
		
		private function getLineIndexForKey( key : uint ) : int {
			switch ( String.fromCharCode( key ).toLowerCase() ) {
				case 'a': case 'h': return 0;
				case 's': case 'j': return 1;
				case 'd': case 'k': return 2;
				case 'f': case 'l': return 3;
				default: return -1;
			}
		} 
		
		
		private function getLabelFormatter() : TextFormat {
			var _txtFormat : TextFormat = new TextFormat();
			_txtFormat.font = Constants.FONT_NAME_CARTOONERIE;
			_txtFormat.size = 20;
			_txtFormat.color = 0xffffff;
			_txtFormat.align = TextFormatAlign.CENTER;
			
			return _txtFormat;
		}
		
		
		private function getLabel() : TextField {
			var label : TextField = new TextField();
			label.width = 100;
			label.height = 100;
			label.embedFonts = false;
			label.antiAliasType = AntiAliasType.ADVANCED;
			label.mouseEnabled = false;
			label.multiline = false;
			label.wordWrap = true;
			label.border = false;
			label.selectable = false;
			label.autoSize = TextFieldAutoSize.CENTER;
			label.gridFitType = GridFitType.SUBPIXEL;
			label.defaultTextFormat = getLabelFormatter();
			
			return label;
		}
		
	}
}