package
{
	import NGC.ASWorkarounds.StaticClass;
	
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	
	import flash.events.Event;

	public final class TweenManager extends StaticClass
	{
		private static var tweens : Vector.< Tween > = new Vector.<Tween>();
		
		
		public function TweenManager()
		{
		}
		
		
		public static function tween( source : Object, attribute : String, func: Function, initialValue : Number, finalValue : Number, time : Number ) : Tween {
			const t : Tween = new Tween( source, attribute, func, initialValue, finalValue, time, true );
			t.addEventListener(TweenEvent.MOTION_FINISH, onTweenEnd );
			tweens.push( t );
			
			return t;
		}
		
		
		private static function onTweenEnd( e : Event ) : void {
			e.target.removeEventListener( TweenEvent.MOTION_FINISH, onTweenEnd );
			removeTween( e.target );
		} 
		
		
		private static function removeTween( t : Object ) : void {
			for ( var i : int = tweens.length - 1; i >= 0; i-- ) {
				if ( tweens[ i ] == t ) {
					tweens.splice( i, 1 );
					return;
				}
			}
		}

	}
}