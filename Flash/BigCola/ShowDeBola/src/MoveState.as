package
{
	import NGC.ASWorkarounds.Enum;

	public final class MoveState extends Enum
	{
		// "Construtor" estático
		{ initEnum( MoveState ); }
		
		public static const ACTIVE		: MoveState	= new MoveState(); // 0
		public static const COMPLETE	: MoveState	= new MoveState(); // 1
		public static const MISSED		: MoveState	= new MoveState(); // 2
		public static const PARTIAL		: MoveState	= new MoveState(); // 3
		public static const NONE		: MoveState	= new MoveState(); // 3 ? 4
	}
}