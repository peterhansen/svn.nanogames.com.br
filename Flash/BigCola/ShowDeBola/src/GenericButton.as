package
{
	import fl.text.TLFTextField;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	
	public class GenericButton extends BtGeneric2 {
		public static const BUTTON_GREEN : int = 0;
		public static const BUTTON_PURPLE : int = 1;
		
		private static const BKG_INDEX : int = 0;
		
		/**
		 * Espaço, em pixels, entre o label e as bordas verticais da imagem de fundo do botão 
		 */	
		private static const BKG_BORDER : int = 10;
		
		
		/**
		 * Identificador do botão 
		 */	
		private var _tag : int;
		
		/**
		 * Callback que deve ser chamada quando o botão recebe um clique 
		 */	
		private var _onClickCallback : Function;
		
		private var isActive : Boolean;
		private var type : int;
		
		/**
		 * Construtor 
		 * @param title O texto que irá aparecer no botão
		 * @param btTag O identificador do botão
		 * @param onBtClick Callback que deverá ser chamada quando o botão receber um clique
		 */			
		public function GenericButton( title : String, btTag : int, onBtClick : Function, buttonType : int = BUTTON_GREEN, fontSize : int = 20, isAcitve : Boolean = true ) {
			buttonMode = true;
			mouseEnabled = true;
			mouseChildren = false;
			
			type = buttonType;
			_tag = btTag;
			_onClickCallback = onBtClick;
			
			addEventListener( MouseEvent.CLICK, onClick );

			setActivity( isAcitve );		
			
			const _txtFormat : TextFormat = new TextFormat();
			_txtFormat.font = ( new FloridaProjectOne() ).fontName;
			_txtFormat.size = fontSize;
			
			switch( type ) {
				case BUTTON_GREEN: 
					_txtFormat.color = 0xcfe94f;
				break;
				
				case BUTTON_PURPLE: 
					_txtFormat.color = 0xfb4e9f;
				break;
			}
			
			_txtFormat.align = TextFormatAlign.LEFT;
			
			var Buts : TextField = new TextField();
			
			Buts.embedFonts = true;
			Buts.antiAliasType = AntiAliasType.ADVANCED;
			Buts.mouseEnabled = false;
			Buts.multiline = false;
			Buts.wordWrap = false;
			Buts.border = false;
			Buts.selectable = false;
			Buts.autoSize = TextFieldAutoSize.LEFT;
			Buts.gridFitType = GridFitType.PIXEL;
			Buts.text = title;
			Buts.setTextFormat( _txtFormat );
			
			addChild( Buts );
			
			var txtWidth : int = Buts.textWidth + ( BKG_BORDER * 2 );
			
			addEventListener( MouseEvent.ROLL_OVER, onMouseRollOver );
			addEventListener( MouseEvent.ROLL_OUT, onMouseRollOut );
			addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			
			bkg.visible = false;
			
			const rr:Shape = new Shape();
			addChild( rr );
			//bkg.scaleY = 30 / bkg.height ;
			Buts.x = - Buts.textWidth / 2 - ( BKG_BORDER / 3 );
			Buts.y = - Buts.height / 2;
					
			switch( type ) {
				case BUTTON_GREEN:
					rr.graphics.beginFill( 0x003300 ); //003300
					break;
				
				case BUTTON_PURPLE:
					rr.graphics.beginFill( 0x4b1f38 ); //4b1f38
					break;
			}
			rr.graphics.drawRoundRect( 0, 0, txtWidth, 30, 10, 10 );
			rr.graphics.endFill();
			rr.x = -txtWidth / 2;
			rr.y = -30 / 2;	
			
			bkg.width = txtWidth;
			
			removeChild( Buts );
			addChild( Buts );
		}
		
		/**
		 * Callback chamada quando o botão recebe um clique 
		 * @param e O objeto que encapsula os dados do evento 
		 */	
		private function onClick( e : MouseEvent ) : void { 
			if( _onClickCallback != null && isActive ) 
				_onClickCallback( this );
		}
		
		
		public function setActivity( b : Boolean ) : void {
			buttonMode = b;
			mouseEnabled = b;
			isActive = b;
			if( !b ) setBrightness( -0.6 );
			else onMouseRollOut( null );
		}
		
		
		private function onMouseDown( e : MouseEvent ) : void { if( isActive ) setBrightness( -0.4 ); }
		private function onMouseRollOut( e : MouseEvent ) : void { if( isActive ) setBrightness( 0.0 ); }
		private function onMouseRollOver( e : MouseEvent ) : void { if( isActive ) setBrightness( 0.4 ); }
		
		
		private function setBrightness( brillo : Number ):void
		{ 
			// changes the brightness
			// mc is the movieclip you want to change
			if (brillo > 1) brillo=1;
			if (brillo < -1) brillo=-1;
			
			var colorTrans:ColorTransform= new ColorTransform();
			//value between -1 and 1
			var trans:Transform=new Transform( this );
			colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = 1 - Math.abs (brillo); // color percent
			colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= (brillo > 0) ? brillo * 256 : 0; // color offset
			trans.colorTransform=colorTrans;
		}
		
		
		/**
		 * Retorna o identificador do botão
		 * @return O identificador do botão
		 */	
		public function get tag() : int {
			return _tag
		}
		
		public function get btWidth() : Number {
			return getChildAt( BKG_INDEX ).width;
		}
	}
}