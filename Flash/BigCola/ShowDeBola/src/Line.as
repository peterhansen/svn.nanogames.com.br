package
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoSounds.SoundManager;
	
	import fl.motion.easing.Bounce;
	import fl.motion.easing.Elastic;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	import fl.video.VolumeBarAccImpl;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.geom.Point;
	import flash.media.SoundTransform;
	import flash.sampler.NewObjectSample;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public final class Line extends MovieClipVS
	{
		/** Velocidade mínima da linha. */
		private static const SPEED_MIN : Number = -75.0;
		
		/** Velocidade máxima da linha. */
		private static const SPEED_MAX : Number = -260.0;
		
		private static const DISTANCE_MIN_EASY : Number = 35.0;
		
		private static const DISTANCE_MIN_HARD : Number = 5.0;
		
		private static const DISTANCE_MAX_EASY : Number = 140.0;
		
		private static const DISTANCE_MAX_HARD : Number = 80.0;
		
		private static const FEEDBACK_TIME : Number = 0.8;
		
		private static const STOP_ANIMATION_TIME : Number = 2.8;
		
		private static const VANISH_ANIMATION_TIME : Number = STOP_ANIMATION_TIME * 1.5;
		
		private static const PRECISION_LEVEL_LABELS : Array = [ "Bola murcha", "Canelada", "Ops...", "Bom!", "Show!", "Perfeito!" ];
		
		private var currentMinDistance : Number;
		private var currentMaxDistance : Number;
		
		public var speed : Number = 0;
		
		private var moves : Array;
		
		private var currentMoveIndex : int = -1;
		
		private var target : MovieClip;
		
		private var bottle : MovieClip;
		
		private var movesGroup : MovieClip;
		
		private var onHitCallback : Function;
		
		private var index : uint;
		
		private var movePressed : Move;
		
		private var feedbackTween : Tween;
		
//		private var feedbackLabel : TextField;
		
		private var lastVisibleMoveIndex : int;
		
		private var maskNeck : Mask;
		
//		private var particleEmitter : SparkEmitter;
		
		private var bottleSpecial : EspecialGarrafas;
		
		private static const LINE_MASK_Y : Number = Constants.BOTTLE_Y + 130;
		
		private static const TARGET_STOP : uint = 0;
		private static const TARGET_HIT : uint = 1;
		private static const TARGET_LOOP : uint = 2;
		
		private var soundTrans : SoundTransform;
		
		private var activeLabels : Vector.< DisplayObject > = new Vector.< DisplayObject >;
		
		
		public function Line( index : uint, onHitCallback : Function ) {
			this.onHitCallback = onHitCallback;
			this.index = index;
			
			soundTrans = new SoundTransform();
			soundTrans.volume = Constants.VOLUME_SOUND_MOVE_HIT;
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}
		
		
		public function onAddedToStage( e : Event ) : void {
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			addEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
			
			var lineTop : MovieClip = getLineEnd( index );
			lineTop.x = -lineTop.width / 2;
			addChild( lineTop );
			
			var l : MovieClip = getLine( index );
			l.x = -l.width / 2;
			l.y = lineTop.height;
			l.height = LINE_MASK_Y - lineTop.height;
			addChild( l );
			
			movesGroup = new MovieClip();
			addChild( movesGroup );
			
			const emptyBottle : MovieClip = getEmptyBottle( index );
			emptyBottle.stop();
			emptyBottle.x = -emptyBottle.width / 2;
			emptyBottle.y = Constants.BOTTLE_Y;
			addChild( emptyBottle );
			
			bottle = getBottle( index );
			bottle.stop();
			bottle.x = -bottle.width / 2;
			bottle.y = Constants.BOTTLE_Y;
			addChild( bottle );
						
			const maskGroup : MovieClip = new MovieClip();
			maskNeck = new Mask();
			maskNeck.x = ( 0.5 + GameScreen.LINES_X_PERCENT[ index ] ) * Constants.STAGE_WIDTH;
			maskNeck.y = Constants.BOTTLE_Y;
			maskGroup.addChild( maskNeck );
			
			const bottleMask : MovieClip = getBottle( index );
			bottleMask.x = ( 0.5 + GameScreen.LINES_X_PERCENT[ index ] ) * Constants.STAGE_WIDTH - bottleMask.width / 2;
			bottleMask.y = Constants.BOTTLE_Y;
			maskGroup.addChild( bottleMask );
		
			const linesMask : Shape = new Shape();
			linesMask.graphics.beginFill( 0xffffff );
			linesMask.graphics.drawRect( 0, 0, 1000, Constants.BOTTLE_Y - maskNeck.height );
			linesMask.graphics.endFill();
			maskGroup.addChild( linesMask );			
			movesGroup.mask = maskGroup;
			
			//			particleEmitter = new SparkEmitter( new Point(), 10, 75, 105, 150, 200, 15, 3, 8, 100 );
			//			particleEmitter.alpha = 0;
			//			particleEmitter.scaleX = 0;
			//			particleEmitter.scaleY = 0;
			//			particleEmitter.y = Constants.BOTTLE_Y;
			//			addChild( particleEmitter );
			
			bottleSpecial = new EspecialGarrafas();
			bottleSpecial.alpha = 0;
			bottleSpecial.scaleY = 0;
			bottleSpecial.scaleX = 0;
			bottleSpecial.y = Constants.BOTTLE_Y + 14;
			addChild( bottleSpecial );
			
			target = getTarget( index );
			target.stop();
			addChild( target );
			target.x = -target.width / 2;
			target.y = Constants.TARGET_Y;
			
			alpha = 0;
		}
		
		
		protected function onRemovedFromStage( e : Event ) : void {
			removeEventListener( Event.REMOVED_FROM_STAGE, onRemovedFromStage );
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			removeAllChildren();
			
			movesGroup = null;
			bottle = null;
			target = null;
//			particleEmitter = null;
			bottleSpecial = null;
		}
		
		
		public function getIndex() : uint {
			return index;
		}
		
		
		public function set animationStatus( n : Number ) : void {
			bottleSpecial.alpha = n * 0.6;
			bottleSpecial.scaleX = n;
			bottleSpecial.scaleY = n;
		}
		
		
		public function setBigMode( b : Boolean ) : void {
			TweenManager.tween( this, "animationStatus", Regular.easeInOut, b ? 0 : 1, b ? 1 : 0, 2 );
		}
		
		
		private function getLabel( targetAnimation : uint ) : String {
			const labels : Array = [ "AlvoRed", "AlvoYellow", "AlvoBlack", "AlvoGreen" ];
			const anims : Array = [ "", "Hit", "Continuos" ];
			
			return labels[ index ] + anims[ targetAnimation ];
		}
		
		
		private static function getTarget( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new alvo_BLACK();
					
				case Constants.COLOR_GREEN:
					return new alvo_GREEN();
					
				case Constants.COLOR_RED:
					return new alvo_RED();
					
				case Constants.COLOR_YELLOW:
					return new alvo_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getEmptyBottle( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new garrafa_empty_BLACK();
					
				case Constants.COLOR_GREEN:
					return new garrafa_empty_GREEN();
					
				case Constants.COLOR_RED:
					return new garrafa_empty_RED();
					
				case Constants.COLOR_YELLOW:
					return new garrafa_empty_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getBottle( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new garrafa_BLACK();
					
				case Constants.COLOR_GREEN:
					return new garrafa_GREEN();
					
				case Constants.COLOR_RED:
					return new garrafa_RED();
					
				case Constants.COLOR_YELLOW:
					return new garrafa_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getLine( index : uint ) : MovieClip {
			switch ( index ) {
				case 0:
					return new canudo_base_RED;
					
				case 1:
					return new canudo_base_YELLOW;				
				
				case 2:
					return new canudo_base_BLACK;				
				
				case 3:
					return new canudo_base_GREEN;
					
				default:
					return null;
			}
		} 
		
		
		private static function getLineEnd( index : uint ) : MovieClip {
			switch ( index ) {
				case 0:
					return new CanudoEndRed;
					
				case 1:
					return new CanudoEndYellow;				
				
				case 2:
					return new CanudoEndBlack;				
				
				case 3:
					return new CanudoEndGreen;
					
				default:
					return null;
			}
		} 
		
		
		public function prepare( difficultyLevel : Number ) : void {
			difficultyLevel = MathFuncs.clamp( difficultyLevel, 0.0, 1.0 );
			
			speed = MathFuncs.lerp( SPEED_MIN, SPEED_MAX, difficultyLevel );
			
			activeLabels.splice( 0, activeLabels.length );
			
			movePressed = null;
			
			lastVisibleMoveIndex = 0;
			
			moves = new Array();
			while ( movesGroup.numChildren > 0 )
				movesGroup.removeChildAt( 0 );
			currentMoveIndex = -1;
			movesGroup.y = Constants.STAGE_HEIGHT;
			
			currentMinDistance = MathFuncs.lerp( DISTANCE_MIN_EASY, DISTANCE_MIN_HARD, difficultyLevel );
			currentMaxDistance = MathFuncs.lerp( DISTANCE_MAX_EASY, DISTANCE_MAX_HARD, difficultyLevel );
		}
		
		
		public function start() : void {
			TweenManager.tween( this, "alpha", Regular.easeInOut, 0, 1, VANISH_ANIMATION_TIME );
		}
		
		
		public function gameOver() : void {
			TweenManager.tween( this, "speed", Regular.easeOut, speed, 0, STOP_ANIMATION_TIME );
			TweenManager.tween( this, "alpha", Elastic.easeInOut, alpha, 0, VANISH_ANIMATION_TIME );
		}
		
		
		public function insertMove( move : Move, currentY : Number ) : Number {
			moves.push( move );
			
			move.x = -move.width / 2;
			move.top = currentY;
			return currentY + ( move.getLength() + currentMinDistance + ( Math.random() * ( currentMaxDistance - currentMinDistance ) ) );
		}
		
		
		public function onPressed() : void {
			trace( "Line[ " + index + " ] PRESSED" );
			target.gotoAndPlay( getLabel( TARGET_HIT ) );
			
			if ( currentMoveIndex >= 0 && currentMoveIndex < moves.length ) {
				const move : Move = moves[ currentMoveIndex ];
				
				if ( move.top + movesGroup.y + Constants.DISTANCE_APROXIMATION > getTargetYBottom() ) {
					// apertou tecla antes do movimento estar na área da mira
					trace( "Line[ " + index + " ] APERTOU ANTES -> " + currentMoveIndex );
					movePressed = null;
					onHitCallback( index, Constants.PRECISION_MISSED );
				} else if ( move.bottom + movesGroup.y + Constants.DISTANCE_APROXIMATION >= getTargetYTop() ) {
					// apertou dentro da área da mira
					trace( "Line[ " + index + " ] ACERTOU LINHA " + index + " -> "+ currentMoveIndex );
					movePressed = move;
					const newHit : Boolean = move.onPressed( getTargetYCenter() );
					
					switch ( move.getType() ) {
						case Move.MOVE_TYPE_CONTINUOUS:
							target.gotoAndPlay( getLabel( TARGET_LOOP ) );
						break;
						
						case Move.MOVE_TYPE_MULTIPLE:
							if ( newHit ) {
								onHitCallback( index, Constants.PRECISION_GOOD, 1 );
								showFeedback( Constants.PRECISION_GOOD );
							} else {
								// se o jogador for muito "apressadinho" e apertar demais, considera como erro
								onHitCallback( index, Constants.PRECISION_TERRIBLE, 1 );
								showFeedback( Constants.PRECISION_TERRIBLE );
							}
						break;
					}
					
					if ( move.getState() == MoveState.COMPLETE ) {
						++currentMoveIndex;
						move.visible = false;
						trace( "LINE #" + index + " REMOVE " + lastVisibleMoveIndex + " / " + movesGroup.numChildren );
						movesGroup.removeChild( move );
						trace( "-->" + movesGroup.contains( move ) );
						
						switch ( move.getType() ) {
							case Move.MOVE_TYPE_MULTIPLE:
							case Move.MOVE_TYPE_DEFAULT:
								onHitCallback( index, move.getPrecisionLevel(), move.getTotalHits() );
								showFeedback( move.getPrecisionLevel() );
							break;
						}
					}
				} else {
					// apertou depois da peça passar
					trace( "Line[ " + index + " ] APERTOU DEPOIS -> "+ currentMoveIndex );
					movePressed = null;
					onHitCallback( index, Constants.PRECISION_MISSED );
				}
			}
		}
		
		
		public function onReleased() : void {
			trace( "Line[ " + index + " ] RELEASED" );
			
			if ( movePressed ) {
				movePressed.onReleased( target.y );
				
				switch ( movePressed.getType() ) {
					case Move.MOVE_TYPE_CONTINUOUS:
						target.gotoAndStop( getLabel( TARGET_STOP ) );
						if ( movePressed.getState() == MoveState.COMPLETE ) {
							onHitCallback( index, movePressed.getPrecisionLevel(), movePressed.getTotalHits() );							
							showFeedback( movePressed.getPrecisionLevel() );
						}
					break;
				}
				
				movePressed = null;
			}
		}
		
		
		private function getTargetYCenter() : Number {
			return target.y + ( target.height / 2 );
		}
		
		
		private function getTargetYTop() : Number {
			return target.y;
		}
		
		
		private function getTargetYBottom() : Number {
			return target.y + target.height;
		}
		
		
		private function get currentMove() : Move {
			if ( currentMoveIndex >= 0 && currentMoveIndex < moves.length )
				return moves[ currentMoveIndex ];
			
			return null;
		}
		
		
		public function update( delta : Number ) : void {
			movesGroup.y += speed * delta;
			
//			particleEmitter.update( delta );
			
			if ( lastVisibleMoveIndex < moves.length ) {
				const lastMove : Move = moves[ lastVisibleMoveIndex ];
				if ( currentMoveIndex >= lastVisibleMoveIndex || movesGroup.y + lastMove.bottom >= 0 ) {
					trace( "LINE #" + index + " ADD " + lastVisibleMoveIndex + " / " + movesGroup.numChildren );
					movesGroup.addChild( lastMove );
					++lastVisibleMoveIndex;
					
					if ( currentMoveIndex < 0 )
						currentMoveIndex = 0;
				}
			}
			
			if ( currentMoveIndex >= 0 && currentMoveIndex < moves.length ) {
				const m : Move = moves[ currentMoveIndex ];
				if ( isCurrentMoveInTarget() ) {
					m.update( delta );
				} else {
					++currentMoveIndex;
					m.onExit();
					
					switch ( m.getState() ) {
						case MoveState.COMPLETE:
						case MoveState.PARTIAL:
							switch ( m.getType() ) {
								case Move.MOVE_TYPE_CONTINUOUS:
									// se o movimento foi solto, já foi dado o feedback em onReleased
									if ( movePressed ) {
										movePressed.onReleased( target.y );
										movePressed = null;	
										target.gotoAndStop( getLabel( TARGET_STOP ) );
									} else {
										break;
									}
									
								case Move.MOVE_TYPE_MULTIPLE:
									onHitCallback( index, m.getPrecisionLevel(), m.getTotalHits() );
									showFeedback( m.getPrecisionLevel() ); 
								break;
							}
						break;
						
						default:
//							SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_BALL_MISS );
							showFeedback( Constants.PRECISION_MISSED ); 
							onHitCallback( index, Constants.PRECISION_MISSED, m.getTotalHits());
					}
				}
			}
		}
		
		
		private function isCurrentMoveInTarget() : Boolean {
			switch ( currentMove.getType() ) {
				case Move.MOVE_TYPE_CONTINUOUS:
					switch ( currentMove.getState() ) {
						case MoveState.PARTIAL:
						case MoveState.COMPLETE:
							return currentMove.bottom >= getTargetYTop() - movesGroup.y + Constants.DISTANCE_APROXIMATION;
						
						default:
							return currentMove.top + Constants.MOVE_CONTINUOUS_HIT_AREA >= getTargetYTop() - movesGroup.y + Constants.DISTANCE_APROXIMATION;
					}
				
				default:
					return currentMove.bottom + movesGroup.y >= getTargetYTop() + Constants.DISTANCE_APROXIMATION;
			}
		}
		
		
		public function setBigLevel( percent : Number ) : void {
			const h : Number = MathFuncs.lerp( 0, bottle.height, percent );
			
			const bottleMask : Shape = new Shape();
			bottleMask.graphics.beginFill( 0xffffff );
			bottleMask.graphics.drawRect( 0, Constants.BOTTLE_Y + bottle.height - h, 1000, 1000 );
			bottleMask.graphics.endFill();
			bottle.mask = bottleMask;
		}
		
		
		public function isOver() : Boolean {
			return currentMoveIndex >= moves.length;
		}
		
		
		protected function showFeedback( type : uint ) : void {			
			var obj : DisplayObject;
			var soundIndex : uint = Constants.SOUND_MOVE_HIT;
			
			switch( type ) {
				case Constants.PRECISION_MISSED:
					obj = new ScoreErrou();
					soundIndex = Constants.SOUND_MOVE_MISS;
				break;
				
				case Constants.PRECISION_TERRIBLE:
				case Constants.PRECISION_BAD:
					obj = new ScoreRuim();
					soundIndex = Constants.SOUND_MOVE_MISS;
				break;
				
				case Constants.PRECISION_GOOD:
					obj = new ScoreBom();
				break;
				
				case Constants.PRECISION_GREAT:
					obj = new ScoreOtimo();
				break;
				
				case Constants.PRECISION_PERFECT:
					obj = new ScoreShow();
				break;
				
				case Constants.PRECISION_BIG:
				case Constants.PRECISION_LEVEL_TOTAL:
					var square:Shape = new Shape();
					square.graphics.beginFill(0x990000);
					square.graphics.drawRect(0, 0, 200, 100);
					square.graphics.endFill();
					square.width = 200;
					square.height = 100;
					obj = square;
				break;
			}

			addChild( obj );
			
			activeLabels.push( obj );
			
			const t : Tween = TweenManager.tween( obj, "y", Regular.easeInOut, target.y - 30, target.y - 40, FEEDBACK_TIME );
			t.addEventListener( TweenEvent.MOTION_FINISH, onFeedbackEnd );
			
			TweenManager.tween( obj, "scaleX", Regular.easeInOut, 0.25, 0.5, FEEDBACK_TIME );
			TweenManager.tween( obj, "scaleY", Regular.easeInOut, 0.25, 0.5, FEEDBACK_TIME );			
			TweenManager.tween( obj, "alpha", Regular.easeInOut, 1.0, 0.0, FEEDBACK_TIME );

			SoundManager.GetInstance().playSoundAtIndex( soundIndex, 0, 0, soundTrans );
		}
		
		
		private function onFeedbackEnd( e : Event ) : void {
			if ( activeLabels.length > 0 ) {
				removeChild( activeLabels.shift() );
			}
		}
		
	}
	
}