package {
	import NGC.NanoOnline.NOCustomer;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Mouse;
	
	import org.osmf.logging.Log;
	
	public class SplashScreen extends AppScene
	{
		private var flash : Flash;
		private var glow : Glow;
		private var tsdb : TitShowdeBola;
		private var text : Text;
		private var keys : Keys;
		private var btPlay : BtPlay;
		private var player : Player;
		private var ball : Ball_base;
		private var perfil : Perfil;
		private var btLogin : GenericButton;
		private var nick : TextField;
		private var lg : logo;
		
		public function SplashScreen() {
			super( null, false, true );
		}
		
	
		protected override function onAddedToStage( e : Event ) : void {
			super.onAddedToStage( e );
			
			flash = new Flash();
			glow = new Glow();
			tsdb = new TitShowdeBola();
			text = new Text();
			keys = new Keys();
			btPlay = new BtPlay();
			player = new Player( getRecentPerformance );
			ball = new Ball_base();
			
			nick = new TextField();
			
			lg = new logo();
						
			addChild( flash );
			addChild( glow );
			addChild( tsdb );
			addChild( ball );
			addChild( player );
			addChild( text );
			addChild( keys );
			addChild( btPlay );
			addChild( lg );
			
			perfil = new Perfil();
			addChild( perfil );
			perfil.addChild( nick );
			perfil.removeChild( perfil.tbNickname );
			nick.x = perfil.tbNickname.x;
			nick.y = perfil.tbNickname.y;
			nick.width = perfil.tbNickname.width;
			nick.height = perfil.tbNickname.height;
			
			flash.y = Constants.STAGE_TOP + ( flash.height / 2 );
			
			glow.x = Constants.STAGE_LEFT + 310;
			glow.y = Constants.STAGE_TOP + 235;
			
			perfil.x = -200;
			perfil.y = 120;
			
			perfil.removeChild( perfil.btLogin );
			updatePerfil();
			
			flash.x = glow.x;
			
			tsdb.x = glow.x - 30;
			tsdb.y = glow.y - 120;
			
			ball.x = -137;
			ball.y = 100;
			
			text.x = Constants.STAGE_RIGHT / 2;
			text.y = Constants.STAGE_TOP / 2;
			
			keys.x = Constants.STAGE_RIGHT / 2;
			
			btPlay.x = Constants.STAGE_RIGHT / 2;
			btPlay.y = Constants.STAGE_BOTTOM / 2;
			btPlay.addEventListener( MouseEvent.CLICK, exit );
									
			player.x = glow.x + 25;
			player.y = glow.y + 25;
			
			lg.x = perfil.x - 102;
			lg.y = btPlay.y - 18;
			lg.addEventListener( MouseEvent.CLICK, goToNano );
			lg.mouseEnabled = true;
			lg.buttonMode = true;
			
			//player.play();
		}
		
		
		public function goToNano( e : Event ) : void {
			navigateToURL( new URLRequest("http://www.nanostudio.com.br/"), "_blank" );
		}
		
		
		public function entar( clickedBt : GenericButton ) : void { 
			Application.GetInstance().setState( ApplicationState.APP_STATE_LOGIN ); 
		}
		
		
		public function sair( clickedBt : GenericButton ) : void { 
			Application.GetInstance().logOut(); updatePerfil(); 
		}
		
		
		public function updatePerfil() : void {
			const login : NOCustomer = Application.GetInstance().getLoggedProfile();
			
			if( btLogin != null ) perfil.removeChild( btLogin );
			btLogin = new GenericButton( ( login != null ) ? "Sair" : "Logar", 2, ( login != null ) ? sair : entar, GenericButton.BUTTON_PURPLE, 16 );
			perfil.addChild( btLogin );
			btLogin.x = perfil.btLogin.x;
			btLogin.y = perfil.btLogin.y;
						
			const _txtFormat : TextFormat = new TextFormat();
			_txtFormat.font = ( new FloridaProjectOne() ).fontName;
			_txtFormat.size = 16;
			_txtFormat.color = 0xffffff;
			_txtFormat.align = TextFormatAlign.CENTER;			
						
			nick.embedFonts = true;
			nick.antiAliasType = AntiAliasType.ADVANCED;
			nick.mouseEnabled = false;
			nick.multiline = false;
			nick.wordWrap = false;
			nick.border = false;
			nick.selectable = false;
			nick.autoSize = TextFieldAutoSize.LEFT;
			nick.gridFitType = GridFitType.PIXEL;
			nick.text = ( login != null ) ? login.nickname : "Nenhum";
			
			nick.setTextFormat( _txtFormat );
		}
		
		
		protected override function onRemovedFromStage( e : Event ) : void {
			super.onRemovedFromStage( e );
			
			flash = null;
			glow = null;
			tsdb = null;
			text = null;
			keys = null;
			btPlay = null;
			player = null;
			ball = null;
			perfil = null;
		}
		
		public function getRecentPerformance() : Number {
			return Constants.PRECISION_GOOD;
		}
		
		
		public function exit( evt : Event ) : void {
			btPlay.removeEventListener( MouseEvent.CLICK, exit );
			Application.GetInstance().setState( ApplicationState.APP_STATE_NEWGAME );
		}
	}
}