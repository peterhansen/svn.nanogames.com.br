package {
	public interface BallListener {
		function onTargetHit( ball : Ball ) : void;
		
		function onAppear( ball : Ball ) : void;
		
		function onStop( ball : Ball ) : void;
		
	}
}