package
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoSounds.SoundManager;
	
	import fl.transitions.Tween;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.filters.BlurFilter;
	import flash.geom.Rectangle;
	import flash.media.SoundTransform;

	public final class Move extends MovieClipVS {
		public static const MOVE_TYPE_RANDOM		: int	= -1;
		
		public static const MOVE_TYPE_DEFAULT		: uint	= 0;
		public static const MOVE_TYPE_CONTINUOUS	: uint	= 1;
		public static const MOVE_TYPE_MULTIPLE		: uint	= 2;
		
		private static const MOVE_TYPES_PERCENT		: Array = [ 0.75, 0.17, 0.082 ];
		
		private static const MOVE_TYPES_TOTAL		: uint = 3;
		
		/** Distância máxima considerada para um acerto. */
		private static const MAX_DISTANCE : uint = 20;
		
		private static const MAX_HALF_DISTANCE : uint = MAX_DISTANCE / 2;
		
		public static const HITS_MIN : uint = 2;
		
		private static const HITS_CONTINUOUS_MIN : uint = 5;
		
		public static const HITS_MAX : uint = 25;

		private static const MULTIPLE_SPACING_EASY : Number = 0;
		
		private static const MULTIPLE_SPACING_HARD : Number = 0;
		
		private static const CONTINUOUS_PRECISION_PERFECT : Number = 0.95;
		
		private static const ALPHA_TRANSITION_TIME : Number = 0.8;
		
		private static const ALPHA_LEVEL_MISSED : Number = 0.4;
		
		private var type : uint = MOVE_TYPE_DEFAULT;
		
		private var state : MoveState = MoveState.NONE;
		
		private var precision : Number = 0;
		
		private var length : Number = 0;
		
		private var hits : uint = 0;
		
		private var totalHits : uint = 1;
		
		private var yPressed : Number = 0;
		
		private var line : Line;
		
		private var soundTrans : SoundTransform;
		
		public function Move( parent : Line, color : uint, moveType : int = MOVE_TYPE_RANDOM, moveTotalHits : int = -1, difficultyLevel : Number = -1 ) {
//			moveType = MOVE_TYPE_MULTIPLE; // teste
			
			soundTrans = new SoundTransform();
			soundTrans.volume = Constants.VOLUME_SOUND_MOVE_HIT;
			
			if ( moveType == MOVE_TYPE_RANDOM ) {
				const r : Number = Math.random();
				var total : Number = 0;
				for ( var bla : int = 0; bla < MOVE_TYPES_TOTAL; ++bla ) {
					total += MOVE_TYPES_PERCENT[ bla ];
					if ( r <= total ) { 
						type = bla;
						break;
					}
				}
			} else {
				type = moveType;
			}
			
			setState( MoveState.ACTIVE );
			trace( "type: " + type );
			
			line = parent;
			
			switch ( type ) {
				case MOVE_TYPE_CONTINUOUS:
					if ( moveTotalHits <= 0 )
						totalHits = ( HITS_CONTINUOUS_MIN + Math.random() * HITS_MAX );
					else
						totalHits = HITS_CONTINUOUS_MIN + moveTotalHits;
					
					var roundRectFill : MovieClip = getFill( line.getIndex() );
					const t : MovieClip = getTop( line.getIndex() );
					const b : MovieClip = getBottom( line.getIndex() );
					
					length = totalHits * roundRectFill.height;
					
					roundRectFill.y = t.height;
					roundRectFill.height = length - t.height - b.height;
					addChild( roundRectFill );
					
					addChild( t );

					b.y = roundRectFill.y + roundRectFill.height - 1;
					addChild( b );
				break;
				
				case MOVE_TYPE_DEFAULT:
					totalHits = 1;
					
					const unit : MovieClip = getDefaultMove( line.getIndex() );
					addChild( unit );
					
					length = unit.height;
				break;
				
				case MOVE_TYPE_MULTIPLE:
					if ( moveTotalHits <= 0 )
						totalHits = HITS_MIN + Math.random() * HITS_MAX;
					else
						totalHits = moveTotalHits;
					
					const currentSpacing : Number = MathFuncs.lerp( MULTIPLE_SPACING_EASY, MULTIPLE_SPACING_HARD, difficultyLevel );
					
					var y : Number = 0;
					for ( var i : uint = 0; i < totalHits; ++i ) {
						var m : MovieClip = getDefaultMove( line.getIndex() );
						m.y = y;
						trace( i + " -> " + y );
						addChild( m );
						
						y += m.height + currentSpacing;
					}
					length = y;
				break;
			}
		}
		
		
		private static function getTop( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_top_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_top_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_top_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_top_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getBottom( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_bottom_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_bottom_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_bottom_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_bottom_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getFill( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_center_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_center_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_center_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_center_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private static function getDefaultMove( index : uint ) : MovieClip {
			switch ( index ) {
				case Constants.COLOR_BLACK:
					return new canudo_unidade_BLACK();
				
				case Constants.COLOR_GREEN:
					return new canudo_unidade_GREEN();
				
				case Constants.COLOR_RED:
					return new canudo_unidade_RED();
					
				case Constants.COLOR_YELLOW:
					return new canudo_unidade_YELLOW();
					
				default:
					return null;
			}
		}
		
		
		private function setState( state : MoveState ) : void {
			const previousState : MoveState = this.state;
			
			this.state = state;
			switch ( state ) {
				case MoveState.ACTIVE:
				break;

				case MoveState.COMPLETE:
					//if ( type == MOVE_TYPE_CONTINUOUS )
					TweenManager.tween( this, "alpha", Regular.easeInOut, alpha, 0, 0.5 );
				break;
				
				case MoveState.MISSED:
					// o múltiplo já trata os erros parciais
					if ( type != MOVE_TYPE_MULTIPLE )
						TweenManager.tween( this, "alpha", Regular.easeInOut, alpha, ALPHA_LEVEL_MISSED, ALPHA_TRANSITION_TIME );
				break;
				
				case MoveState.PARTIAL:
				break;
			}
		}
		
		
		public function getState() : MoveState {
			return state;
		}
		
		
		public function getLength() : Number {
			return length;
		}
		
		
		public function getType() : Number {
			return type;
		}
		
		
		public function getTotalHits() : uint {
			return totalHits;
		}
		
		
		public function onPressed( targetYCenter : Number ) : Boolean {
			trace( this + ".onPressed início: " + state );
			var newHit : Boolean = true;
			
			switch ( type ) {
				case MOVE_TYPE_CONTINUOUS:
					switch ( state ) {
						case MoveState.ACTIVE:
							setState( MoveState.PARTIAL );
							yPressed = Math.max( parent.y + top, targetYCenter );
						break;
					}
				break;

				case MOVE_TYPE_DEFAULT:
					var distance : Number = getDistance( targetYCenter, true );
					precision = 1.0 - ( distance / MAX_DISTANCE );
					trace( "move.hit: " + ( precision * 100.0 ) + "%" );
					setState( MoveState.COMPLETE );
				break;
				
				case MOVE_TYPE_MULTIPLE:
					trace( "DISTANCE: " + getDistance( targetYCenter, false ) + " / " + MAX_DISTANCE + " -> " + height );
					const hitY : Number = ( parent.y + currentTop );
					
					switch ( getState() ) {
						case MoveState.MISSED:
						break;
						
						case MoveState.ACTIVE:
							setState( MoveState.PARTIAL );
						default:
							trace( "HITS: " + hits + " / " + totalHits + " -> " + hitY );
							if ( hits < totalHits ) {
								var i : int = 0;
								for ( ; i < numChildren; ++i ) {
									const c : DisplayObject = getChildAt( i );
									if ( hitY + c.y + ( c.height ) >= Constants.TARGET_Y - MAX_HALF_DISTANCE && hitY + c.y - ( c.height ) <= Constants.TARGET_Y + MAX_HALF_DISTANCE ) {
										trace( "REMOVE " + i + " / " + numChildren + " -> " + height + ", " + c.y );
										++hits;
										removeChildAt( i );
										//SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_MOVE_HIT, 0, 0, soundTrans );
										break;
									}
								}
								newHit = i != numChildren || numChildren == 0; 
								
								if ( hits >= totalHits )
									setState( MoveState.COMPLETE );
							} else {
								hits = Math.max( hits - 1, 0 );
							}
							precision = MathFuncs.clamp( hits / totalHits, 0, 1.0 );
						break;
					}
				break;
			}
			
			trace( this + ".onPressed fim: " + state );
			
			return newHit;
		}
		
		
		private function get topCap() : DisplayObject {
			return getChildAt( 1 );
		}
		
		
		private function get bottomCap() : DisplayObject {
			return getChildAt( 2 );
		}
		
		
		private function get continuousArea() : DisplayObject {
			return getChildAt( 0 );
		}
		
		
		public function onReleased( y : Number ) : void {
			switch ( type ) {
				case MOVE_TYPE_CONTINUOUS:
					switch ( state ) {
						case MoveState.PARTIAL:
							checkContinuousPrecision();
							setState( MoveState.COMPLETE );
						break;
					}
				break;

				case MOVE_TYPE_DEFAULT:
				break;
				
				case MOVE_TYPE_MULTIPLE:
				break;
			}
		}
		
		
		private function checkContinuousPrecision() : void {
			precision = MathFuncs.clamp( ( length - continuousArea.height ) / length, 0, 1 );
			if ( precision >= CONTINUOUS_PRECISION_PERFECT )
				precision = 1;
			
			trace( "PRECISION: " + ( precision * 100.0 ) + "%" );
		}
		
		
		public function update( delta : Number ) : void {
			switch ( type ) {
				case MOVE_TYPE_CONTINUOUS:
					switch ( state ) {
						case MoveState.PARTIAL:
							if ( bottom > yPressed ) {
								const diff : Number = Math.abs( top + parent.y - yPressed );
								const c : DisplayObject = continuousArea;
								c.height = Math.max( 0, length - diff - topCap.height - bottomCap.height );
								c.y = ( length - c.height - topCap.height );
								topCap.y = c.y - topCap.height;
								bottomCap.y = c.y + c.height;
								
								if ( c.height <= 0 ) {
									precision = 1;
									setState( MoveState.COMPLETE );
								}
							} else {
								precision = 1;
								setState( MoveState.COMPLETE );
							}
						break;
					}
				break;
				
				case MOVE_TYPE_MULTIPLE:
					const parentY : Number = currentTop + parent.y;
					
					for ( var i : int = 0; i < numChildren; ++i ) {
						const child : DisplayObject = getChildAt( i );
						if ( child.alpha == 1.0 && ( parentY + child.y + ( child.height ) ) < Constants.TARGET_Y ) {
							// se está fora do alcance, não pode mais ser atingido
							TweenManager.tween( child, "alpha", Regular.easeInOut, child.alpha, ALPHA_LEVEL_MISSED, ALPHA_TRANSITION_TIME );
						}
//						else if ( child.y - ( child.height ) >= hitY - Constants.DISTANCE_APROXIMATION ){ TODO
//							break;
//						}
					} 
				break;
			}
		}
		
		
		public function get currentTop() : Number {
			return y;// TODO - ( height / 2 );
		}
		
		
		public function get currentBottom() : Number {
//			return y + ( height / 2 );
			return y + height;
		}
		
		
		public function getPrecisionLevel() : uint {
			if ( precision < 0.15 ) {
				return Constants.PRECISION_MISSED;
			} else if ( precision < 0.45 ) {
				return Constants.PRECISION_TERRIBLE;
			} else if ( precision < 0.6 ) {
				return Constants.PRECISION_BAD;
			} else if ( precision < 0.75 ) {
				return Constants.PRECISION_GOOD;
			} else if ( precision < 0.9 ) {
				return Constants.PRECISION_GREAT;
			} else {
				return Constants.PRECISION_PERFECT;
			}
		}
		
		
		/**
		 * Método chamado quando o movimento sai da área da mira da linha.
		 */
		public function onExit() : void {
			switch ( type ) {
				case MOVE_TYPE_CONTINUOUS:
					switch ( state ) {
						case MoveState.PARTIAL:
							checkContinuousPrecision();
						break;
						
						case MoveState.ACTIVE:
							setState( MoveState.MISSED );
						break;
					}
				break;
				
				case MOVE_TYPE_DEFAULT:
					if ( state != MoveState.COMPLETE )
						setState( MoveState.MISSED );
				break;
				
				case MOVE_TYPE_MULTIPLE:
					for ( var i : int = numChildren - 1; i >= 0; --i )
						TweenManager.tween( getChildAt( i ), "alpha", Regular.easeInOut, getChildAt( i ).alpha, ALPHA_LEVEL_MISSED, ALPHA_TRANSITION_TIME );
					
					if ( hits <= 0 )
						setState( MoveState.MISSED );
				break;
			}
		}
		
		
		private function getDistance( targetY : Number, center : Boolean ) : Number {
			return Math.max( 0, Math.abs( parent.y + yCenter - targetY ) - Constants.DISTANCE_APROXIMATION );
		}
		
		
		public function get yCenter() : Number {
			return y + ( length / 2 );
		}
		
		
		public override function get top() : Number {
			return y; // - ( length / 2 );
		}
		
		
		public override function get bottom() : Number {
//			return y + ( length / 2 );
			return y + length;
		}
		
		
		public override function get left() : Number {
			return x;
		}
		
		
		public override function get right() : Number {
			return x + width;
		}
		
		
		public override function set bottom( b : Number ) : void {
			y = b - height;
		}
		
		
		public override function set top( t : Number ) : void {
			y = t;
		}
		
	}
}