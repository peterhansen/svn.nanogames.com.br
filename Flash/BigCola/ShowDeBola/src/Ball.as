package
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoSounds.SoundManager;
	
	import fl.motion.easing.Back;
	import fl.motion.easing.Bounce;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public final class Ball extends MovieClipVS {
		
		/** Aceleração da gravidade, em pixels por segundo ao quadrado. */
		private static const GRAVITY : Number = 1400;
		
		private static const MAX_ANGULAR_SPEED : Number = 500;
		
		private static const MIN_Y_SPEED : Number = GRAVITY * -0.35;
		
		private static const MAX_SPEED_PERCENT : Number = 0.7;
		
		private static const DEFORM_ANIMATION_TIME : Number = 0.666;
		
		private static const APPEAR_ANIMATION_TIME : Number = 1.5;
		
		private static const HIDE_ANIMATION_TIME : Number = APPEAR_ANIMATION_TIME * 2;
		
		private static const HIT_SCALE_X : Number = 1.4;
		
		private static const HIT_SCALE_Y : Number = 0.7;
		
		private static const MAX_PREVIOUS_POSITIONS : uint = 10;
		
		private static const FRICTION_LEVEL : Number = 0.85;
		
		private static const BOUNCE_LEVEL : Number = 0.67;
		
		private static const BOUNCE_MIN_Y_SPEED : Number = MIN_Y_SPEED * -0.09;
		
		public static const SPEED_INDEX_SPECIAL_LEFT : int = 0;
		public static const SPEED_INDEX_SPECIAL_RIGHT : int = 1;
		
		private static const SPEED_SPECIAL : Array = [ GRAVITY * -0.27, GRAVITY * -0.27 ];
		
		private var angularSpeed : Number = 0;
		
		private var v0 : Number = 0;
		
		private var y0 : Number = Constants.BALL_START_Y;
		
		private var t : Number = 0;
		
		private var xSpeed : Number = 0;
		
		private var totalTime : Number = 0;
		
		private var target : Object;
		
		private var listener : BallListener;
		
		private var active : Boolean = false;
		
		private var group : MovieClip;
		
		private var previousPositions : Vector.< Point >;
		
		private var previousGroup : MovieClip;
		
		private var bigMode : Boolean;
		
		private var alphaTween : Tween;
		

		public function Ball() {
			previousPositions = new Vector.< Point >;
			previousGroup = new MovieClip();
			addChild( previousGroup );
			
			for ( var i : uint = 0; i < MAX_PREVIOUS_POSITIONS; ++i ) {
				const b : MovieClip = getBall();
				b.alpha = MathFuncs.lerp( 0.05, 0.7, i / MAX_PREVIOUS_POSITIONS );
				b.visible = false;
				previousGroup.addChild( b );
			}
			
			group = getBall();
			group.alpha = 0;
			addChild( group );
			
			setBigMode( false );
		}
		
		
		private static function getBall() : MovieClip {
			var container2:MovieClip = new MovieClip();
			
			var container:MovieClip = new MovieClip(); 
			container.x = 0; 
			container.y = 0; 
			container2.addChild(container); 
			
			var conChild:Ball_base = new Ball_base(); 
			container.addChild(conChild); 
			conChild.x = -container.width/2; 
			conChild.y = -container.height/2;
			
			return container2;
		}
		
		
		public function setListener( listener : BallListener ) : void {
			this.listener = listener;
		}
		
		
		public function update( delta : Number ) : void {
			if ( active ) {
				if ( bigMode ) {
					previousPositions.push( new Point( group.x, group.y ) );
					if ( previousPositions.length > MAX_PREVIOUS_POSITIONS )
						previousPositions.shift();
				}

				if ( previousPositions.length > 0 ) {
					for ( var i : uint = 0; i < MAX_PREVIOUS_POSITIONS; ++i ) {
						const c : DisplayObject = previousGroup.getChildAt( i );
						if ( i < previousPositions.length ) {
							c.visible = true;
							c.x = previousPositions[ i ].x;
							c.y = previousPositions[ i ].y;
						} else {
							c.visible = false;
						}
					}
					
					if ( !bigMode ) {
						previousPositions.shift();
						previousGroup.getChildAt( 0 ).visible = false;
					}
				}
				
				t += delta;
				group.x += xSpeed * delta;
				group.y = y0 + v0 * t + GRAVITY * t * t / 2;
				const ySpeed : Number = v0 + GRAVITY * t;
			
				if ( ySpeed > 0 && group.y >= Constants.BALL_START_Y ) {
					group.y = Constants.BALL_START_Y;
					setActive( active );
					
					if ( Math.abs( ySpeed ) > BOUNCE_MIN_Y_SPEED ) {
						angularSpeed *= FRICTION_LEVEL;
						xSpeed *= FRICTION_LEVEL;
						v0 = Math.max( -ySpeed, v0 ) * BOUNCE_LEVEL;
						y0 = Constants.BALL_START_Y;
						t = 0;
					} else {
						active = false;
						if ( listener )
							listener.onStop( this );
					}
				}
				group.getChildAt( 0 ).rotation += angularSpeed * delta; 
				
				if ( target && ( t >= totalTime * 0.5 ) && getPosY() >= target.y ) {
					if ( listener )
						listener.onTargetHit( this );
				}
			}
		}
		
		
		public function setBigMode( b : Boolean ) : void {
			bigMode = b;
		}
		
		
		public function getPosY() : Number {
			return group.y;
		}
		
		
		public function getTimeToTarget() : Number {
			return totalTime - t;
		}
		
		
		public function isGoingUp() : Boolean {
			return v0 + ( GRAVITY * t ) < 0;
		}
		
		
		public function isNearTarget() : Boolean {
			return target && target.y - y < ( group.height / 2 );
		}
		
		
		public function getInternalWidth() : Number {
			return group.width;
		}
		
		
		public function stopMoving() : void {
			angularSpeed = 0;
			xSpeed = 0;
			group.x = Constants.BALL_START_X;
			group.y = Constants.BALL_START_Y;
			active = false;
			previousPositions.slice();
			
			cancelAlphaTween();
			alphaTween = TweenManager.tween( group, "alpha", Bounce.easeInOut, 0, 1, APPEAR_ANIMATION_TIME );
			alphaTween.addEventListener( TweenEvent.MOTION_FINISH, onAppearAnimationEnd );
		}
		
		
		public function cancelAlphaTween() : void {
			if ( alphaTween != null ) {
				alphaTween.stop();
				alphaTween = null;
			}
		} 
		
		
		public function hide() : void {
			cancelAlphaTween();
			alphaTween = TweenManager.tween( group, "alpha", Bounce.easeInOut, 1, 0, HIDE_ANIMATION_TIME );
		}
		
		
		private function onAppearAnimationEnd( e : Event ) : void {
			if ( listener )
				listener.onAppear( this );
		}
		
		
		private function getMinSpeedY( y0 : Number, y1 : Number ) : Number {
			if ( y1 <= y0 )
				return Math.min( MIN_Y_SPEED, 1.1 * -Math.sqrt( ( y1 - y0 ) * ( GRAVITY * -2 - 1 ) ) );
			
			return MIN_Y_SPEED;
		}
		
		
		public function setTarget( target : Object, speedIndex : int = -1 ) : void {
			this.target = target;
			
			if ( target ) {
				t = 0;
				y0 = group.y;
				angularSpeed = -MAX_ANGULAR_SPEED + ( Math.random() * MAX_ANGULAR_SPEED * 2 );
				
				if ( speedIndex < 0 )
					v0 = getMinSpeedY( y0, target.y ) * ( 1 + Math.random() * MAX_SPEED_PERCENT );
				else
					v0 = SPEED_SPECIAL[ speedIndex ];
				
				totalTime = getTime( y0, target.y, v0 );
				xSpeed = ( target.x - group.x ) / totalTime;
				
				SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_BALL_HIT );
			}
		}
		
		
		public function setActive( b : Boolean ) : void {
			active = b;
			const sx : Number = Math.min( 1, v0 <= MIN_Y_SPEED ? 1 : 0.7 + ( 0.3 * ( v0 / MIN_Y_SPEED ) ) );
			TweenManager.tween( group, "scaleX", Back.easeOut, HIT_SCALE_X * sx, 1, DEFORM_ANIMATION_TIME );
			
			if ( v0 <= MIN_Y_SPEED )
				TweenManager.tween( group, "scaleY", Back.easeInOut, HIT_SCALE_Y, 1, DEFORM_ANIMATION_TIME / 2 );
		}
		
		
		private function getTime( initialY : Number, finalY : Number, speedY : Number ) : Number {
			var delta : Number = speedY * speedY - 2 * GRAVITY * ( initialY - finalY );
			
			return ( Math.sqrt( delta ) - speedY ) / GRAVITY;
		}

		

	}
}