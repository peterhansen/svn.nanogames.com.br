package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class GameState extends Enum
	{
		// "Construtor" estático
		{ initEnum( GameState ); }
		
		// Possíveis estados de jogo
		public static const NONE : GameState				= new GameState(); 
		public static const START_LEVEL : GameState			= new GameState(); 
		public static const PLAYING : GameState				= new GameState(); 
		public static const GAME_OVER : GameState			= new GameState(); 
		public static const LEVEL_COMPLETE : GameState		= new GameState(); 
		public static const GAME_OVER_MESSAGE : GameState	= new GameState(); 
		public static const LOGIN_MESSAGE : GameState		= new GameState(); 
		
	}
}
