package
{
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOErrors;
	
	public class RankingListener implements INOListener {
		private var caller : String;
		
		public function RankingListener( s : String ) {
			caller = s;
		}
		
		public function onNORequestSent() : void {
			trace( caller + "----onNORequestSent()" );
		}
		
		public function onNORequestCancelled() : void {
			trace( caller + "----onNORequestCancelled()" );
		}
		
		public function onNOSuccessfulResponse() : void {
			trace( caller + "----onNOSuccessfulResponse()" );
		}
		
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void {
			trace( caller + "----onNOError()" );
		}
		
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void {
			trace( caller + "----onNOProgressChanged()" );
		}
	}
}