package
{
	import NGC.ASWorkarounds.Enum;
	
	public final class ApplicationState extends Enum
	{
		// "Construtor" estático
		{ initEnum( ApplicationState ); }
		
		public static const APP_STATE_SPLASH : ApplicationState							= new ApplicationState();
		public static const APP_STATE_LOGIN : ApplicationState							= new ApplicationState();
		public static const APP_STATE_NEWGAME : ApplicationState						= new ApplicationState();
		public static const APP_STATE_BACK_TO_GAME : ApplicationState					= new ApplicationState();
		
	}
}
