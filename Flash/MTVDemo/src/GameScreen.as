package
{
	import JSONLite.JSON;
	
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoSounds.SoundManager;
	import NGC.TweenManager;
	
	import fl.controls.Button;
	import fl.motion.Color;
	import fl.motion.easing.Bounce;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.net.Socket;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.ui.Keyboard;
	import flash.utils.Timer;
	import flash.utils.getDefinitionByName;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import flashx.textLayout.formats.Direction;
	
	import game.GameBomb;
	import game.Map;
	import game.Obstacle;
	import game.Race;
	import game.Racer;
	import game.SmokePuff;
	import game.TrackObject;
	
	import org.osmf.events.TimeEvent;
	import org.osmf.media.MediaPlayer;
	import org.osmf.proxies.ListenerProxyElement;
	
	public final class GameScreen extends AppScene {
		// Intervalo do looping de atualização do jogo
		private static const TIMER_INTERVAL_MILIS : uint = 20;
		
		// Intervalo máximo de atualização permitido
		private static const MAX_UPDATE_INTERVAL_MILIS : int = 125;
		
		private static const TIME_MESSAGE_DEFAULT : uint = 3;
		
		private static const MAX_SCORE : int = 999999999;
		
		private static const SCORE_CHANGE_TIME : Number = 1.9;
		
		private static const BOARD_X : Number = Constants.STAGE_RIGHT - 30;
		
		private static const BOARD_Y : Number = Constants.STAGE_TOP + 15;
		
		private static const ALPHA_TRANSITION_TIME : Number = 1.0;
		
		private static const GAME_OVER_MESSAGE_TIME : Number = 1600;
		
		private static const COMBO_LABEL_SCALE : Number = 1.3;
		
		private static const COMBO_LABEL_ANIMATION_TIME : Number = 0.3;
		
		private static const MESSAGE_BOX_WIDTH : Number = 600 * 0.6;
		
		private static const MESSAGE_BOX_HEIGHT : Number = 168.7 * 0.6;
		
		private var gameState : GameState;
		
		private var difficultyOffset : uint = 0;
		
		/**
		 * Controla o looping de atualização do jogo 
		 */		
		private var _gameLoopTimer : Timer;
		
		/***/
		private var messageTimer : Timer;
		
		private var messageOnEndCallback : Function;
		
		private var messageOnEndCallbackParams : *;
		
		/**
		 * Momento da última atualização (em milissegundos) 
		 */		
		private var _lastUpdateTime : int;
		
		/***/
		
		private var soundTimer : Timer;
		
		private var alphaTimer : Timer;
		
		/***/
		private var scoreSpeed : Number = 0;
		
		/***/
		private var scoreLabel : TextField;
		
		private var messageLabel : TextField;
		private var messageTween : Tween;
		
		private var hitsLabel : TextField;
		
		private var levelLabel : TextField;
		
		private var scoreFont : TextFormat;
		private var bonusFont : TextFormat;
		private var messageFont : TextFormat;
		private var levelFont : TextFormat;		
		
		private var level : uint;
		private var score : Number = 0;	
		
		private var race : Race;
		
		private var serverSock: Socket;
		
		private var playerId : Array = new Array();
		
		private var accTime : Number;
		
		private var countLabel : CountdownLabel;
		
		
		
		public function GameScreen() {
			super( null, false, true );
			accTime = 0;
		}		
		
		
		private function start() : void {
			prepare( 1 );
			startRunning();
		}
		
		
		
		
		protected override function onAddedToStage( e : Event ) : void {
			super.onAddedToStage( e );
			
			_gameLoopTimer = new Timer( TIMER_INTERVAL_MILIS );
			_gameLoopTimer.addEventListener( TimerEvent.TIMER, gameLoop );
			
			mouseEnabled = true;
			mouseChildren = true;
			
			stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			
			var florida : ArialRegular = new ArialRegular();
			
			scoreFont = new TextFormat();
			scoreFont.font = florida.fontName;
//			scoreFont.size = ( 30 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
			scoreFont.align = TextFormatAlign.RIGHT;
			
			scoreLabel = getLabel();
			scoreLabel.defaultTextFormat = scoreFont;
			scoreLabel.embedFonts = true;
			scoreLabel.antiAliasType = AntiAliasType.ADVANCED;
//			board.addChild( scoreLabel );
			
			bonusFont = new TextFormat();
			//bonusFont.color = 0x77eaff;
			bonusFont.font = florida.fontName;
			bonusFont.align = TextFormatAlign.RIGHT;
//			bonusFont.size = ( 40 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );

			levelFont = new TextFormat();
			levelFont.font = florida.fontName;
//			levelFont.size = ( 45 * ( ( board.scaleX < board.scaleY ) ? board.scaleX : board.scaleY ) );
			levelFont.align = TextFormatAlign.CENTER;
			
			levelLabel = getLabel();
//			levelLabel.x = -( 85 * board.scaleX );
//			levelLabel.y = ( 13 * board.scaleY );
//			levelLabel.width = ( 75 * board.scaleX );
			levelLabel.defaultTextFormat = levelFont;
			levelLabel.embedFonts = true;
			levelLabel.antiAliasType = AntiAliasType.ADVANCED;
//			board.addChild( levelLabel );
			
			messageFont = new TextFormat();
			messageFont.font = florida.fontName;
			messageFont.size = 50;
			messageFont.align = TextFormatAlign.CENTER;
			
			messageLabel = getLabel();
			messageLabel.defaultTextFormat = messageFont;
			messageLabel.embedFonts = true;
			messageLabel.width = Constants.STAGE_WIDTH;
			messageLabel.x = Constants.STAGE_LEFT;
			messageLabel.antiAliasType = AntiAliasType.ADVANCED;
			addChild( messageLabel );
			
			updateSound();
			
			race = new Race();
			addChild( race );
			
			for ( var c : uint = 0; c < 10; ++c )
				placeRandomObstable();
			
			//Monta a interface de jogo
			
			var interfaceBox : InterfaceBox = new InterfaceBox();
			addChild( interfaceBox );
			
			
			interfaceBox.x = -stage.stageWidth / 2 + ( width - interfaceBox.width ) / 4;
			interfaceBox.y = -stage.stageHeight / 2;
			
			serverSock = new Socket();
			serverSock.connect("192.168.1.12", 9090 );
				

				serverSock.addEventListener( Event.CONNECT, gotConnected );
				serverSock.addEventListener(ProgressEvent.SOCKET_DATA, progressEventHandler );
				serverSock.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onNetError );
				serverSock.addEventListener(Event.CLOSE, closeNetConnection );
				serverSock.addEventListener( ProgressEvent.SOCKET_DATA, dataHandler );
				trace("connected?");

			
			setState( GameState.START_LEVEL );
			start();	
			
			countLabel = new CountdownLabel();
			addChild( countLabel );			
			countLabel.start();
			setChildIndex( countLabel, numChildren - 1 );
			
		}
		
		
		public function progressEventHandler( progressEvent : ProgressEvent ) : void {
			trace("mestre mandando:" + progressEvent.toString() );
		}
		
		public function dataHandler( dataEvent:ProgressEvent ) : void {
			var msg : String = serverSock.readUTFBytes( dataEvent.bytesLoaded );
			trace("mestre mandou:" + msg );
			
			var obj : Object = JSONLite.JSON.deserialize( msg );
			var indexOf : int = -1;
			var id : int = 1;			
			var str:String;
			
			str = obj.data.action_name;
			
			if ( str == null )
				str = obj.data.result;
			
			if ( obj.id != null ) {
				
				indexOf = playerId.indexOf( obj.id )
					
				if ( indexOf == -1 ) {
					id = playerId.length;
					playerId.push( obj.id );	
				} else {
					id = indexOf;					
				}
				
			} else {
				return;
			}
				
			
			
			switch ( str ) {
				case "bomb_player_1":
					bombPlayer( 0 );
					break;
				case "bomb_player_2":
					bombPlayer( 1 );
					break;
				case "bomb_player_3":
					bombPlayer( 2 );
					break;
				case "bomb_player_4":
					bombPlayer( 3 );
					break;
				case "obstacle":
					placeRandomObstable();
					break;
				
				case "up":
						race.setActiveRacer( id );
						race.getRacer( id ).setAsPlayable();
						race.getRacer( id ).move( Racer.DIRECTION_UP );
					break;
				case "down":

						race.setActiveRacer( id );
						race.getRacer( id ).setAsPlayable();
						race.getRacer( id ).move( Racer.DIRECTION_DOWN );

					break;
				case "brake":

						race.setActiveRacer( id );
						race.getRacer( id ).setAsPlayable();
						race.getRacer( id ).setMoveState( Racer.MOVE_STATE_BRAKE );

					break;
				
				case "brake1":
					race.setActiveRacer( 0 );
					race.getRacer( 0 ).setAsPlayable();
					race.getRacer( id ).setMoveState( Racer.MOVE_STATE_BRAKE );
					break;
				case "brake2":
					race.setActiveRacer( 1 );
					race.getRacer( 1 ).setAsPlayable();
					race.getRacer( 1 ).setMoveState( Racer.MOVE_STATE_BRAKE );
					break;
				case "brake3":
					race.setActiveRacer( 2 );
					race.getRacer( 2 ).setAsPlayable();
					race.getRacer( 2 ).setMoveState( Racer.MOVE_STATE_BRAKE );
					break;
				case "brake4":
					race.setActiveRacer( 3 );
					race.getRacer( 3 ).setAsPlayable();
					race.getRacer( 3 ).setMoveState( Racer.MOVE_STATE_BRAKE );
					break;
				
				case "up1":
					race.setActiveRacer( 0 );
					race.getRacer( 0 ).setAsPlayable();
					race.getRacer( 0 ).move( Racer.DIRECTION_UP );
					break;
				case "down1":
					race.setActiveRacer( 0 );
					race.getRacer( 0 ).setAsPlayable();
					race.getRacer( 0 ).move( Racer.DIRECTION_DOWN );
					break;
				case "up2":
					race.setActiveRacer( 1 );
					race.getRacer( 1 ).setAsPlayable();
					race.getRacer( 1 ).move( Racer.DIRECTION_UP );
					break;
				case "down2":
					race.setActiveRacer( 1 );
					race.getRacer( 1 ).setAsPlayable();
					race.getRacer( 1 ).move( Racer.DIRECTION_DOWN );
					break;
				case "up3":
					race.setActiveRacer( 2 );
					race.getRacer( 2 ).setAsPlayable();
					race.getRacer( 2 ).move( Racer.DIRECTION_UP );
					break;
				case "down3":
					race.setActiveRacer( 2 );
					race.getRacer( 2 ).setAsPlayable();
					race.getRacer( 2 ).move( Racer.DIRECTION_DOWN );
					break;
				case "up4":
					race.setActiveRacer( 3 );
					race.getRacer( 3 ).setAsPlayable();
					race.getRacer( 3 ).move( Racer.DIRECTION_UP );
					break;
				case "down4":
					race.setActiveRacer( 3 );
					race.getRacer( 3 ).setAsPlayable();
					race.getRacer( 3 ).move( Racer.DIRECTION_DOWN );
					break;
			}
			
			trace( str ); 
		}
		
		public function bombPlayer( id : int ) : void {
			var point : Point = new Point();	
			point.x = race.getRacer( id ).trackX + 600;
			point.y = race.getRacer( id ).trackY;
			race.addBomb( new GameBomb(), point );			
		}
		
		public function placeRandomObstable() : void {
			var point : Point = new Point();
			point.x = width + race.getCameraPosition() + ( Math.random() * ( Map.length - race.getCameraPosition() - width ) );
			point.y = - ( Map.trackWidth / 2 ) + ( Math.random() * Map.trackWidth );
			race.addObstacle( Obstacle.CACTUS, point );			
		}
		
		public function closeNetConnection( e:Event ) : void {
			trace("SOCK CLOSED");
		}
		
		public function onNetError( e:Event ) : void {
			trace( e.toString() );
		}
		
		public function gotConnected( e:Event ) : void {		
		}
		
		
		private function disableSound( clickedBt : GenericButton ) : void {
			SoundManager.GetInstance().setMute( true );
			updateSound();
		}
		
		
		private function enableSound( clickedBt : GenericButton ) : void {
			SoundManager.GetInstance().setMute( false );
			updateSound();
		}
		
		
		private function exitGame( clickedBt : GenericButton ) : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
		}
		
		private function updateSound() : void {
		}
		
		
		protected override function onRemovedFromStage( e : Event ) : void {
			super.onRemovedFromStage( e );
			
			scoreFont = null;
			scoreLabel = null;
			bonusFont = null;
			hitsLabel = null;
			levelFont = null;
			levelLabel = null;
			messageFont = null;
			messageLabel = null; 
			
			if( _gameLoopTimer ) {
				_gameLoopTimer.stop();
				_gameLoopTimer.removeEventListener( TimerEvent.TIMER, gameLoop );
				_gameLoopTimer = null;
			}
			
			mouseEnabled = false;
			mouseChildren = false;
			
			stage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
		}
		
		
		private function showMessage( message : String, time : Number = TIME_MESSAGE_DEFAULT, onEndCallback : Function = null, onEndCallbackParams : * = null ) : void {
			if ( messageTimer )
				onMessageTimeEnded();

			if ( time > 0 ) {
				messageOnEndCallback = onEndCallback;
				messageOnEndCallbackParams = onEndCallbackParams;
				
				messageTimer = new Timer( time * 1000 );
				messageTimer.addEventListener( TimerEvent.TIMER, onMessageTimeEnded );
				messageTimer.start();
			}
			
			messageLabel.text = message;
			doMessageTween( 1 );
		}
		
		
		private function cancelMessageTween() : void {
			if ( messageTween != null ) {
				messageTween.stop();
				messageTween = null;
			}
		} 
		
		
		private function doMessageTween( finalValue : Number ) : void {
			cancelMessageTween();
			if( messageLabel != null ) 
				messageTween = TweenManager.tween( messageLabel, "alpha", Regular.easeInOut, messageLabel.alpha, finalValue, ALPHA_TRANSITION_TIME );
		}
		
		
		private function onMessageTimeEnded( e : TimerEvent = null ) : void {
			doMessageTween( 0 );
			
			if ( messageTimer ) {
				messageTimer.stop();
				messageTimer.removeEventListener( TimerEvent.TIMER, onMessageTimeEnded );
				messageTimer = null;
				
				if ( messageOnEndCallback != null ) {
					const p : * = messageOnEndCallbackParams;
					const f : Function = messageOnEndCallback;
					
					messageOnEndCallback = null;
					messageOnEndCallbackParams = null;
					
					f.call( this, p );
				}
			}
		}
		
		
		/**
		 * Inicia o looping de atualização do jogo  
		 */		
		public function startRunning() : void {
			// Inicia o looping do jogo
			_gameLoopTimer.start();
		}
		
		
		private function changeScore( points : Number ) : void {
		}		
		
		
		/**
		 * Atualiza o label da pontuação
		 * @param delta Tempo decorrido desde a última chama ao método
		 * @param changeNow Indica se a pontuação mostrada deve ser automaticamente igualada à pontuação real
		 */
		public function updateScore( delta : Number, changeNow : Boolean ) : void {
		}
		
		
		/**
		 * 
		 */
		private function setState( newState : GameState ) : void {
			trace( "GameScreen.setState: " + gameState + " -> " + newState );
			
			gameState = newState;
			switch ( newState ) {
				case GameState.GAME_OVER_MESSAGE:
					var nextState : GameState;					
					const profile : NOCustomer = Application.GetInstance().getLoggedProfile();
					var f : Function;
					
					if ( profile != null ) {
						f = gameOverTimer;
					} else {
						f = loginMessageTimer;
					}
					
					TweenManager.tween( race, "alpha", Regular.easeInOut, 1, 0, ALPHA_TRANSITION_TIME );
					
					SoundManager.GetInstance().stopSound( Constants.SOUND_MUSIC_GAME );
					SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_GAME_OVER );
				break;
				
				case GameState.LOGIN_MESSAGE:
					Application.GetInstance().showPopUp( "Que tal enviar seus pontos e competir com os melhores jogadores do mundo? É rápido e fácil!", "Show de bola!", onLoginButtonYes, "Quem sabe outra hora...", onLoginButtonNo );
				break;
				
				case GameState.GAME_OVER:
					Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH );
				break;

				case GameState.LEVEL_COMPLETE:
					SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_LEVEL_COMPLETE );
				break;
				
				case GameState.PLAYING:
					playTheme();
				break;
				
				case GameState.START_LEVEL:
					showMessage( "Nível " + level, level <= 1 ? TIME_MESSAGE_DEFAULT * 1.5 : TIME_MESSAGE_DEFAULT, setState, GameState.PLAYING );
					playTheme();
				break;
			}
		}
		
		
		private function playTheme() : void {
			if ( !SoundManager.GetInstance().isPlaying( Constants.SOUND_MUSIC_GAME ) )
				SoundManager.GetInstance().playSoundAtIndex( Constants.SOUND_MUSIC_GAME, 0, 9999 );
		}
		
		
		private function gameOverTimer( e : Event = null ) : void {
			if ( alphaTimer != null ) {
				alphaTimer.stop();
				alphaTimer = null;
			}
			
			alphaTimer = new Timer( GAME_OVER_MESSAGE_TIME );
			alphaTimer.addEventListener( TimerEvent.TIMER, setGameOverState );
			alphaTimer.start();
		}
		
		
		private function loginMessageTimer( e : Event = null ) : void {
			if ( alphaTimer != null ) {
				alphaTimer.stop();
				alphaTimer = null;
			}
			
			alphaTimer = new Timer( GAME_OVER_MESSAGE_TIME );
			alphaTimer.addEventListener( TimerEvent.TIMER, setLoginMessageState );
			alphaTimer.start();
		}
		
		
		private function setGameOverState( e : Event = null ) : void {
			alphaTimer.stop();
		}
		
		
		private function setLoginMessageState( e : Event = null ) : void {
			alphaTimer.stop();
		}
		
		private function goToLoginScreen() : void { }
		
		
		public function getState() : GameState {
			return gameState;
		}
		
		
		public function onLoginButtonYes() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_LOGIN );
		}
		
		
		public function onLoginButtonNo() : void {
			Application.GetInstance().setState( ApplicationState.APP_STATE_SPLASH ); 
		}
		
		
 		private function onKeyDown( e : KeyboardEvent  ) : void {
			switch ( gameState ) {
				case GameState.PLAYING:
					switch ( e.keyCode ) {
						case Keyboard.ENTER:
							bombPlayer( 0 );
						break;
						
						case Keyboard.UP:
							race.setActiveRacer( 0 );
							race.getRacer( 3 ).setAsAI();
							race.getRacer( 0 ).setAsPlayable();
							race.getRacer( 0 ).move( Racer.DIRECTION_UP );
						break;
						
						case Keyboard.DOWN:
							race.setActiveRacer( 0 );
							race.getRacer( 3 ).setAsAI();
							race.getRacer( 0 ).setAsPlayable();
							race.getRacer( 0 ).move( Racer.DIRECTION_DOWN );
						break;

						
						case Keyboard.LEFT:
							race.setActiveRacer( 3 );
							race.getRacer( 0 ).setAsAI();
							race.getRacer( 3 ).setAsPlayable();
							race.getRacer( 3 ).move( Racer.DIRECTION_UP );
							break;
						
						case Keyboard.RIGHT:
							race.setActiveRacer( 3 );
							race.getRacer( 0 ).setAsAI();
							race.getRacer( 3 ).setAsPlayable();
							race.getRacer( 3 ).move( Racer.DIRECTION_DOWN );
							break;
						
						case Keyboard.SPACE:
							race.getRacer( 0 ).setMoveState( Racer.MOVE_STATE_BRAKE );
						break;
					}
				break;
			}
		}
		
		
		private function onKeyUp( e : KeyboardEvent  ) : void
		{
			switch ( gameState ) {
				case GameState.PLAYING:
					race.getRacer( race.myRacer ).move( Racer.DIRECTION_NONE );
					switch ( e.keyCode ) {
						case Keyboard.SPACE:
							race.getRacer( 0 ).setMoveState( Racer.MOVE_STATE_THROTTLE );
							break;
					}
					
				break;
			}
		}
		
		/**
		 * Looping do jogo. Atualiza objetos e trata colisões. A renderização é feita pela AVM. 
		 * @param e Dados sobre o evento do timer que controla o looping
		 */		
		private function gameLoop( e : TimerEvent ) : void {
			if(	_lastUpdateTime == 0 )
				_lastUpdateTime = getTimer();
			
			// Calcula o tempo transcorrido real
			var currTime : int = getTimer();
			var timeElapsed : int = currTime - _lastUpdateTime;
			
			if( timeElapsed > Constants.MAX_UPDATE_INTERVAL_MILIS )
				timeElapsed = Constants.MAX_UPDATE_INTERVAL_MILIS;
			
			// Atualiza os objetos da cena
			update( ( timeElapsed as Number ) / 1000.0 );
			
			_lastUpdateTime = getTimer();
		}
		
		
		private function prepareNextLevel( e : Event = null ) : void {
			prepare( level + 1 );
		}
		
		
		private function prepare( level : uint ) : void {
			this.level = level;
			levelLabel.text = level.toString();
			
			updateScore( 0, true );
			setState( GameState.START_LEVEL );
		}
		
		
		/**
		 * Atualiza os objetos da cena do jogo
		 * @param timeElapsed Tempo transcorrido dede a última atualização 
		 */
		private function update( timeElapsed : Number ) : void
		{
			accTime += timeElapsed;
			// trace( "TimeElapsed: " + timeElapsed );
			
			// TODO : Seria melhor e mais otimizado se utilizássemos o padrão STRATEGY. Poderíamos armazenar os métodos
			// de atualização em ponteiros para função / functors e modificar o método de atualização atual apenas alterando
			// o valor de uma variável em setState. Assim não teríamos que executar este switch toda vez qua chamamos update
			switch( gameState )
			{
				case GameState.PLAYING:
				case GameState.GAME_OVER:
				case GameState.GAME_OVER_MESSAGE:
					race.update( timeElapsed );
					var over : uint = 0;
					
//					if ( gameState == GameState.PLAYING )
//						setState( GameState.LEVEL_COMPLETE );
				break;
			}
			
//			if ( accTime > 1 ) {
//				trace( "updated " + accTime );
//				accTime = 0;
//				var p : Point = new Point();
//
//					for ( var c : int = 0; c < race.getTotalRacers(); ++c ) {
//					
//					p.x = race.getRacer( c ).trackX;
//					p.y = race.getRacer( c ).trackX;
//					
//					race.addSmokePuff( p );
//				}				
//			}
			
			updateScore( timeElapsed, false );
		}
		
		
		private function getLabelFormatter() : TextFormat {
			
			var _txtFormat : TextFormat = new TextFormat();
			_txtFormat.font = Constants.FONT_NAME_CARTOONERIE;
			_txtFormat.size = 20;
			_txtFormat.color = 0xffffff;
			_txtFormat.align = TextFormatAlign.CENTER;
			
			return _txtFormat;
		}
		
		
		private function getLabel() : TextField {
			var label : TextField = new TextField();
			label.width = 100;
			label.height = 100;
			label.embedFonts = false;
			label.antiAliasType = AntiAliasType.ADVANCED;
			label.mouseEnabled = false;
			label.multiline = false;
			label.wordWrap = true;
			label.border = false;
			label.selectable = false;
			label.autoSize = TextFieldAutoSize.CENTER;
			label.gridFitType = GridFitType.SUBPIXEL;
			label.defaultTextFormat = getLabelFormatter();
			
			return label;
		}
		
	}
}