/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package {
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.geom.Point;

	
	/**
	 *
	 * @author caiolima
	 */
	public class ParticleEmitter extends MovieClip {
		// <editor-fold defaultstate="collapsed" desc="Constantes Estáticas">
		/** Aceleração da gravidade. */
		public static const HALF_GRAVITY_ACC : Number = 200;
	
		public var spriteConstructor : Function;
		private var activeParticles : int = 0;
		// </editor-fold>   
	
		private var MAX_PARTICLES : int = 0;
		public var NUMBER_OF_PARTICLES : int = 0;
		private var particles : Vector.< Particle > = new Vector.<Particle>;
	    private var isActive : Boolean = false;
	    public var freeFall : Boolean = false;
	
	    private var accTime : Number = 0;
	    private var timeBetweenParticles : Number = 0;
	
		
		public function ParticleEmitter( particlesPerSecond : Number, freeFall : Boolean, spriteConstructor : Function, width : Number, height : Number, numberOfParticles : Number, maxParticles : Number ) {
			MAX_PARTICLES = maxParticles;
	
			NUMBER_OF_PARTICLES = numberOfParticles;
	
	        setParticlesPerSecond( particlesPerSecond );
	
	        this.spriteConstructor = spriteConstructor;
			for ( var i : int = 0; i < MAX_PARTICLES; ++i ) {
				particles.push( new Particle(this) );
				addChild( particles[ i ] );
			}
	
	        this.freeFall = freeFall;
		}
	
		
		public function setParticlesPerSecond( nOfParticles : Number ) : void {
			if(nOfParticles!=0) 
				timeBetweenParticles = 1 / nOfParticles;
			else 
				timeBetweenParticles = 1;
		}
	
	
		public function update( delta : Number ) : void {
	        if( isActive ) {
	            accTime += delta;
	            if( accTime >= timeBetweenParticles ) {
					const numberParticles : Number = accTime / timeBetweenParticles; // Calcula o numero de particulas a emitir
	                accTime -= numberParticles * timeBetweenParticles; // Retira do tempo acumulado o Numberervalo entre n particulas
	                while( numberParticles > 0 ) { // Emite o numero de particulas desejadas
	                    emit(); 
	                    numberParticles--;
	                }
	            }
	        }
			
			var i : int = 0;
			while ( i < activeParticles ) {
				particles[ i ].update( delta );
				if ( particles[ i ].visible ) { 
					++i; 
				}														// Se continuar visivel passa para proxima posição
				else {													// Senão
					--activeParticles;									// Decresce o número de particulas
					const p : Particle = particles[ i ];				// E troca a posição da partícula com a última
					particles[ i ] = particles[ activeParticles ];
					particles[ activeParticles ] = p;
				}
			}
		}
	
	
		public function reset() : void {
			activeParticles = 0;
		}
		
		
		public function setEmitting( a : Boolean ) : void {
			isActive = a;
		}
		
		
		public function isEmitting() : Boolean {
			return isActive;
		}
	
	
	    public function RandSpeed() : Point {
			throw new Error( "ABSTRACT RandSpeed" );
		}
		
		
	    public function RandPosition() : Point {
			throw new Error( "ABSTRACT RandPosition" );
		}
		
		
	    public function RandLifeTime() : Number {
			throw new Error( "ABSTRACT RandLifeTime" );
		}
	
	
		public function emit( quantity : int = 1 ) : void {
			for ( var i : int = 0; i < quantity; ++i ) {
			     if ( activeParticles < MAX_PARTICLES - 1 )
		             particles[ activeParticles++ ].born(RandPosition(), RandSpeed(), RandLifeTime() );
			}
		}
	}
	
	
}


import fl.transitions.Tween;
import fl.transitions.easing.Regular;

import flash.display.MovieClip;
import flash.geom.Point;
import NGC.TweenManager;

	/**
	 * Classe Numbererna que descreve uma part�cula de fogos de artif�cio.
	 */
	internal class Particle extends MovieClip {
		
		/** Tempo acumulado da part�cula. */
		public var accTime : Number;
		
		public var speedX : Number = 0;
		public var speedY : Number = 0;
		
		public const initialPos : Point = new Point();
		
		public var emitter : ParticleEmitter;
		
		public static const CYCLES_PER_SECOND : int = 3;
		public static const MILLISECONDS_PER_CYCLES : Number = 1 / CYCLES_PER_SECOND;
		
		public var halfRadius : Number = 0;
		public var seed : int = 0;
		
		public var frameMod : int = 0;
		public var lifeTime : Number = 0;
		
		
		public function Particle( p : ParticleEmitter ) {
			emitter = p;
			visible = false;
			
			addChild( p.spriteConstructor.call() );
		}
		
		
		public function born( bornPosition : Point, bornSpeed : Point, LifeTime : Number ) : void { 
			accTime = 0;
			frameMod = 0;
			lifeTime = LifeTime;
			
			speedX = bornSpeed.x;
			speedY = bornSpeed.y;
			visible = true;
			seed = Math.random() * 360;
			
			halfRadius = speedY / 2;
			
			initialPos.x = 0;
			initialPos.y = 0;
			updatePosition();
			
			TweenManager.tween( this, "alpha", Regular.easeInOut, 1, 0, LifeTime );
		}
		
		
		public function updatePosition() : void {
			if( emitter.freeFall ) {
				x = initialPos.x + speedX * accTime;
				y = initialPos.y + ( speedY * accTime ) + ( ParticleEmitter.HALF_GRAVITY_ACC * ( accTime * accTime ) );
			} else {
				x = initialPos.x + halfRadius * Math.cos( seed + ( accTime / MILLISECONDS_PER_CYCLES ) );
				y = initialPos.y + speedY * accTime;
			}
		}
		
		
		public function updateVisibility() : void {
			if( emitter.freeFall) {
				//if ( x <= 0 || x >= parent.width || y >= parent.height )
				//	visible = false;
				
				if( accTime > lifeTime) 
					visible = false;
				else 
					frameMod = Math.min( ( ( ( accTime * accTime) / (lifeTime * lifeTime) ) * emitter.NUMBER_OF_PARTICLES ), ( emitter.NUMBER_OF_PARTICLES - 1 ) );
			} else {
				//if ( y >= parent.getHeight() )
				//	visible = false;
				
				frameMod = Math.sin( seed + ( accTime / MILLISECONDS_PER_CYCLES ) );
				
				if( frameMod > 0 )
					frameMod = 2;
				else {
					if ( frameMod < -50000 ) 
						frameMod = 1;
					else
						frameMod = 0;
				}	
			}
			gotoAndStop( frameMod );
		}
		
		
		public function update( delta : Number ) : void {
			accTime += delta;
			
			updatePosition();
			updateVisibility();
		}
		
	}
