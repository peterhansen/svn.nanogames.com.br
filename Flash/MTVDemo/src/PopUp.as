package
{
	import NGC.Utils;
	
	import fl.motion.easing.Elastic;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.text.AntiAliasType;
	import flash.text.Font;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import flashx.textLayout.formats.TextAlign;
	import NGC.TweenManager;
	
	public final class PopUp extends Sprite
	{
		/**
		* Número de pixels entre os botões do popup
		* TODO: Transformar numa variável do objeto 
		*/		
		private static const DIST_BETWEEN_BTS : int = 35;
		
		/**
		* Número de pixels entre o texto e os botões
		* TODO: Transformar numa variável do objeto
		*/		
		private static const DIST_BETWEEN_TXT_N_BTS : int = 15;
		
		/**
		* Largura mínima do popup
		* TODO: Transformar numa variável do objeto
		*/		
		private static const MIN_WIDTH : int = 80;
	
		/**
		* Imagem de fundo 
		*/		
		private var _bkg : DisplayObject;
		
		/**
		* Caixa de texto 
		*/		
		private var _label : TextField
		
		/**
		* Formatador do texto que será exibido 
		*/		
		private var _txtFormat : TextFormat;
		
		/**
		* Controlador das animações 
		*/		
		private var _tweener : Tween;
		
		/**
		* Duração em segundos das animações de exibir e esconder o popup 
		*/		
		private var _animTime : Number;
		
		/**
		* Botões do popup 
		*/		
		private var _bts : Vector.< GenericButton >;
		
		/**
		* Callbacks associadas aos botões do popup 
		*/		
		private var _btsCallbacks : Vector.< Function >;
		
		/**
		* Índice do botão de resposta selecionado pelo usuário 
		*/		
		private var _currAnswer : int;
		
		/**
		* Indica se a âncora de posicionamento da imagem de fundo é TOP_LEFT ou CENTER 
		*/		
		private var _topLeftAnchor : Boolean;
		
		/**
		* Indica se um DisplayObject estava recebendo eventos de mouse 
		*/		
		private var _wasMouseEnabled : Vector.< Boolean >;
		
		/**
		* Indica se um DisplayObject estava repassando eventos de mouse para os seus filhos 
		*/		
		private var _wasMouseChildren : Vector.< Boolean >;
		
		/**
		* Referência para a janela principal da aplicação (infelizmente este objeto não é acessível globalmente, então temos que
		* armazená-lo numa variável) 
		*/		
		private var _stage : Stage;
		
		private var _leftMargin : int;
		private var _rightMargin : int;
		private var _topMargin : int;
		private var _bottomMargin : int;
		
		/**
		* Construtor
		* @param appStage Referência para a janela principal da aplicação (infelizmente este objeto não é acessível globalmente, então temos que armazená-lo numa variável)
		* @param bkgImg A imagem de fundo
		* @param topLeftAnchor Indica se a âncora de posicionamento da imagem de fundo é TOP_LEFT ou CENTER
		* @param topMargin A margem superior da caixa de texto em relação à imagem de fundo
		* @param leftMargin A margem à esquerda da caixa de texto em relação à imagem de fundo
		* @param bottomMargin A margem inferior da caixa de texto em relação à imagem de fundo
		* @param rightMargin A margem à direita da caixa de texto em relação à imagem de fundo
		* @param fontName O nome da fonte utilizada nos textos
		* @param fontSize O tamanho da fonte utilizada nos textos
		* @param fontColor A cor da fonte utilizada nos textos
		* @param animTime A duração em segundos das animações de exibir e esconder o popup 
		*/
		public function PopUp( appStage : Stage, bkgImg : DisplayObject, topLeftAnchor : Boolean, topMargin : int, leftMargin : int, bottomMargin : int, rightMargin : int, fontName : String, fontSize : int, fontColor : uint, animTime : Number ) {
			super();
			
			if( appStage == null )
				throw new ArgumentError( "Invalid parameter appStage: " + appStage );
			if( bkgImg == null )
				throw new ArgumentError( "Invalid parameter bkgImg: " + bkgImg );
			
			lockInput( true );
			_tweener = null;
			
			_bkg = bkgImg;
			addChild( _bkg );
			
			_txtFormat = new TextFormat();
			_txtFormat.font = fontName;
			_txtFormat.size = fontSize;
			_txtFormat.color = fontColor;
			// OLD _txtFormat.align = TextFormatAlign.JUSTIFY;
			_txtFormat.align = TextFormatAlign.CENTER;
			
			_label = new TextField();
			_label.x = leftMargin;
			_label.y = topMargin;
			_label.width = _bkg.width - rightMargin - leftMargin;
			_label.height = _bkg.height - bottomMargin - topMargin;
			_label.embedFonts = true;
			_label.antiAliasType = AntiAliasType.ADVANCED;
			_label.mouseEnabled = false;
			_label.multiline = true;
			_label.wordWrap = true;
			_label.border = false;
			_label.selectable = false;
			_label.autoSize = TextFieldAutoSize.CENTER;
			_label.gridFitType = GridFitType.PIXEL;
			_label.setTextFormat( _txtFormat );
			addChild( _label );
			
			_currAnswer = -1;
			_animTime = animTime;
			_stage = appStage;
			_topLeftAnchor = topLeftAnchor;
			
			_topMargin = topMargin;
			_leftMargin = leftMargin;
			_rightMargin = rightMargin;
			_bottomMargin = bottomMargin;
			
			_bts = new Vector.<GenericButton>();
			_btsCallbacks = new Vector.<Function>();
		}
		
		/**
		* Restaura o estado de interação dos objetos 
		* @see disableStageObjects
		*/		
		private function restoreStageObjects() : void {
			var nChildren : int = _stage.numChildren;
			
			for( var childInd : int = 0 ; childInd < nChildren ; ++childInd )
			{
				var child : DisplayObject = _stage.getChildAt( childInd );
				if( child is DisplayObjectContainer )
				{
					var temp : DisplayObjectContainer = ( child as DisplayObjectContainer );
					temp.mouseEnabled = _wasMouseEnabled[ childInd ];
					temp.mouseChildren = _wasMouseChildren[ childInd ];
				}
			}
		}
		
		/**
		* Desabilita a interação do mouse com os objetos, armazenando o estado de interação dos mesmo 
		* para posterior restauração
		* @see restoreStageObjects
		*/		
		private function disableStageObjects() : void {
			var nChildren : int = _stage.numChildren;
			
			_wasMouseEnabled = new Vector.< Boolean >();		
			_wasMouseChildren  = new Vector.< Boolean >();

			for( var childInd : int = 0 ; childInd < nChildren ; ++childInd )
			{
				var child : DisplayObject = _stage.getChildAt( childInd );
				if( child is DisplayObjectContainer )
				{
					var temp : DisplayObjectContainer = ( child as DisplayObjectContainer );
					_wasMouseEnabled.push( temp.mouseEnabled );
					_wasMouseChildren.push( temp.mouseChildren );
					
					temp.mouseEnabled = false;
					temp.mouseChildren = false;
				}
			}
		}
		
		/**
		* Exibe o popup 
		*/		
		public function show( text : String, ...btsTitlesAndCallbacks ) : void {
			alpha = 0.0;
			blendMode = BlendMode.LAYER;
			visible = true;
			
			disableStageObjects();
				
			_stage.addChild( this );
			
			// Obtém os botões e suas callbacks
			var ellipsis : Array = ( btsTitlesAndCallbacks as Array );
			var nArgs : int = ellipsis.length;
			if( nArgs & 1 != 0 )
				throw new ArgumentError( "[PopUp::show]: Buttons titles and callbacks are expected in pairs" );

			removeBts();
			var bt : GenericButton;
			for( var argc : int = 0 ; argc < nArgs ; argc += 2 )
			{
				bt = new GenericButton(( ellipsis[argc] as String ), argc >> 1, onBtClicked );
				addChild( bt )

				_bts.push( bt );
				_btsCallbacks.push( ( ellipsis[argc+1] as Function ) );
			}
			
			// Determina o texto
			var POPUP_MAX_WIDTH : Number = ( stage.stageWidth * 0.8 );
			var MAX_TXT_WIDTH : Number = POPUP_MAX_WIDTH - _rightMargin - _leftMargin;
			_label.width = MAX_TXT_WIDTH;
			_label.text = text;
			_label.setTextFormat( _txtFormat );
			
			// Calcula a largura do texto
			// + 8.0 => Parece que o Flash não calcula a largura dos textos corretamente, então às vezes quebrávamos a linha
			// por causa de um único caractere
			var textWidth : Number = _label.textWidth + 8.0;
			_label.width = textWidth > MAX_TXT_WIDTH ? MAX_TXT_WIDTH : textWidth;
			
			// Calcula as dimensões do conjunto de botões
			var nBts : int = _bts.length;
			var totalBtsWidth : Number = 0;
			var btWidth : Number = 0;
			var btHeight : Number = 0;
			var btsY : Number = 0;
			if( nBts > 0 )
			{				
				btHeight = _bts[0].height;
				
				for each( bt in _bts )
					totalBtsWidth += bt.btWidth;

				totalBtsWidth += ( ( nBts - 1 ) * PopUp.DIST_BETWEEN_BTS );
				
				btsY = _topMargin + _label.textHeight + PopUp.DIST_BETWEEN_TXT_N_BTS + ( btHeight / 2 );
			}
			
			// Define o tamanho final do popup
			_bkg.width = ( _label.width > totalBtsWidth ? _label.width : totalBtsWidth ) + _leftMargin + _rightMargin;
			if( _bkg.width < PopUp.MIN_WIDTH )
				_bkg.width = PopUp.MIN_WIDTH;
			else if( _bkg.width > POPUP_MAX_WIDTH )
				_bkg.width = POPUP_MAX_WIDTH;
			
			_bkg.height = btsY + ( btHeight / 2 ) + _bottomMargin;
			
			_bkg.x = _bkg.width / 2;
			_bkg.y = _bkg.height / 2;
			
			// OLD : Não precisamos destas 2 linhas
			//width = _bkg.width;
			//height = _bkg.height;
			
			// Posiciona os botões
			var btsStartX : Number = ( ( _bkg.width - totalBtsWidth ) * 0.5 );
			for( var currBt : int = 0 ; currBt < nBts ; ++currBt ) {
				bt = _bts[ currBt ];				
				bt.y = btsY;
				
				bt.x = btsStartX + ( bt.btWidth / 2 );
				btsStartX += bt.btWidth + PopUp.DIST_BETWEEN_BTS;
			}
			
			// Posiciona o popup na tela
			if( _topLeftAnchor )
			{
				x = ( stage.stageWidth - width ) * 0.5;
				y = ( stage.stageHeight - height ) * 0.5;
			}
			else
			{
				x = stage.stageWidth >> 1;
				y = stage.stageHeight >> 1;
			}
			
			// Nenhuma resposta foi selecionada ainda
			_currAnswer = -1;

			// Inicia a animação
			_tweener = TweenManager.tween( this, "alpha", Elastic.easeOut, 0.0, 1.0, _animTime );
			_tweener.addEventListener( TweenEvent.MOTION_FINISH, onPopUpShown );
			_tweener.start();
		}
		
		/**
		* Callback chamada assim que a animação de exibir o popup termina 
		*/		
		private function onPopUpShown( e : TweenEvent ) : void {
			_tweener.removeEventListener( TweenEvent.MOTION_FINISH, onPopUpShown );
			
			alpha = 1.0;
			blendMode = BlendMode.NORMAL;
			
			lockInput( false );
		}
		
		/**
		* Esconde o popup 
		*/		
		public function hide() : void {
			alpha = 1.0;
			blendMode = BlendMode.LAYER;
			lockInput( true );

			// TODO : Deixar o usuário escolher quais etapas deseja animar!!!
			if( false )
			{
				_tweener = TweenManager.tween( this, "alpha", Elastic.easeIn, 1.0, 0.0, _animTime );
				_tweener.addEventListener( TweenEvent.MOTION_FINISH, onPopUpHidden );
				_tweener.start();
			}
			else
			{
				onPopUpHidden( null );
			}
		}
		
		/**
		* Callback chamada assim que a animação de esconder o popup termina 
		*/		
		private function onPopUpHidden( e : TweenEvent ) : void {
			_tweener.removeEventListener( TweenEvent.MOTION_FINISH, onPopUpHidden );
			visible = false;
			
			alpha = 0.0;
			blendMode = BlendMode.NORMAL;
			
			_stage.removeChild( this );
			
			restoreStageObjects();

			if( _btsCallbacks[ _currAnswer ] != null )
				_btsCallbacks[ _currAnswer ]();
		}
		
		/**
		* Bloqueia / Libera a interação com o popup 
		*/		
		private function lockInput( b : Boolean ) : void {
			mouseEnabled = !b;
			mouseChildren = !b;
		}
		
		/**
		* Callback chamada quando o usuário clica em um dos botões do menu 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onBtClicked( clickedBt : GenericButton ) : void {
			_currAnswer = clickedBt.tag;
			hide();
		}
		
		/**
		* Deleta todos os botões do popup 
		*/		
		private function removeBts() : void {
			var nBts : int = _bts.length;
			for( var i : int = 0 ; i < nBts ; ++i )
				removeChild( _bts[i] );
			
			_bts.splice( 0, _bts.length );
			_btsCallbacks.splice( 0, _btsCallbacks.length );
		}
	}
}
