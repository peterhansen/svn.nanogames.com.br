package game
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.Crypto.prng.Random;
	import NGC.NanoMath.MathFuncs;
	import NGC.TweenManager;
	
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Transform;
	import flash.utils.Timer;
	
	public class Racer extends TrackObject {
		
		private var g1 : Sprite;
		private var shadow: Sprite;
		public static const STATE_NONE : uint = 0;
		public static const STATE_LINING : uint = 1;
		public static const STATE_COUNTDOWN : uint = 2;
		public static const STATE_RACING : uint = 3;
		public static const STATE_FINISHED : uint = 4;
		public static const STATE_COLLIDE : uint = 5;
		private var s : Shape;
		private var state : uint = STATE_NONE;
		
		private static const OFFSET_Y_MAX : Number = 5;
		private var decisionTimer : Timer;
		private var offsetY : Number = 0;
		private var isPlayable : Boolean;
		public static const MOVE_STATE_NONE : uint = 0;
		public static const MOVE_STATE_THROTTLE : uint = 1;
		public static const MOVE_STATE_BRAKE : uint = 2;
		public static const MOVE_STATE_TURBO : uint = 3;
		public static const MOVE_COLLIDE_STATE_JUMP : uint = 4;
		public static const MOVE_COLLIDE_STATE_LAND : uint = 5;
		
		
		
		private var moveState : uint = MOVE_STATE_NONE;
		
		private static const SPEED_MAX : Number = 500;
		
		private static const SPEED_TURBO : Number = SPEED_MAX * 2;
		
		private static const TURBO_DURATION : Number = 5;
		
		private static const THROTTLE_TIME : Number = 5;
		
		private static const COLLIDE_PENALTY_TIME : Number = 2;
		
		private var speedTween : Tween;
		
		private var _speed : Number = 0;
		
		private static const SPEED_Y : Number = 80;
		
		private var speedY : Number = 0;
		
		public static const DIRECTION_NONE : uint = 0;
		public static const DIRECTION_UP : uint = 1;
		public static const DIRECTION_DOWN : uint = 2;
		
		
		
		
		public var stateTime : Number;
		
		public function Racer( index : uint ) {
			super();
			
			switch ( index ) {
				case 0:
					g1 = new car1();
					shadow = new car1();
				break;

				case 1:
					g1 = new car2();
					shadow = new car2();
				break;
				
				case 2:
					g1 = new car3();
					shadow = new car3();
				break;
				
				case 3:
					g1 = new car4();
					shadow = new car4();
				break;
			}
		
			shadow.scaleY = 0.25;
			var brillo : Number =  -1;
			var colorTrans:ColorTransform= new ColorTransform();
			var trans:Transform=new Transform( shadow );
			colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = 1 - Math.abs (brillo); // color percent
			colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= (brillo > 0) ? brillo * 256 : 0; // color offset
			trans.colorTransform=colorTrans;
			shadow.alpha = 0.6;
			shadow.y += g1.height * 0.75;
			
			addChild( shadow );
			addChild( g1 );
			
			s = new Shape();
			addChild( s );
				
			
		}
		
		public function setAsPlayable() : void {
			
			if ( decisionTimer != null ) {
				decisionTimer.reset();
				decisionTimer = null;				
			}
			isPlayable = true;
			
			s.graphics.clear();
			s.graphics.lineStyle( 5, 0x0000FF, 0.5 );
			s.graphics.beginFill( 0xFF0000, 0.0 );
			s.graphics.drawRect( 0, 0, width, height );
			s.graphics.endFill();
			
		} 

		
		public function setAsAI() : void {
			
			
			if ( state == STATE_RACING )
				takeDecision( null );
			
			isPlayable = false;
			
			s.graphics.clear();			
		} 
		
		public function set speedYProperty( x : Number ) : void {
			speedY = x;
		}
		
		public function get speedYProperty() : Number {
			return speedY;
		}
		
		public function takeDecision( e : Event ) : void {
			
			if ( decisionTimer != null )
				decisionTimer.reset();
			
			if ( state != STATE_RACING )
				return;
			
			var time : Number = 5000 + ( Math.random() * 3 );
			var direction : int = Math.random() * 2;
			decisionTimer = new Timer( time, 1 );
			decisionTimer.addEventListener( TimerEvent.TIMER_COMPLETE, takeDecision );
			decisionTimer.start();
			
			if ( direction == 0 ) {
				TweenManager.tween( this, "speedYProperty", Regular.easeInOut, -SPEED_Y, 0, 1 );
		 	} else {
				TweenManager.tween( this, "speedYProperty", Regular.easeInOut, SPEED_Y, 0, 1 );
			}
			
		}
		
		
		public override function update( delta : Number ) : void {
			// faz o "motor" tremer
			const oldOffsetY : Number = offsetY;
			if ( Math.random() < 0.7 )
				offsetY = ( OFFSET_Y_MAX / -2 ) + Math.random() * OFFSET_Y_MAX;
			
			trackPosition.y = MathFuncs.clamp( trackPosition.y + offsetY - oldOffsetY + speedY * delta, -150, 300 );
			shadow.y = ( g1.height * 0.75 ) + trackHeight;

			switch ( state ) {
				case STATE_LINING:
				break;
				
				case STATE_COUNTDOWN:
				break;
				
				
				
				
				case STATE_COLLIDE:
					
					trackHeight = ( 500 * stateTime ) - ( 500 * stateTime * stateTime  );
					
					if ( trackHeight < 0 )
						trackHeight = 0;
					
					if ( stateTime == 0 && moveState != MOVE_COLLIDE_STATE_JUMP ) {
						setMoveState( MOVE_COLLIDE_STATE_JUMP );
					} else if ( stateTime > COLLIDE_PENALTY_TIME / 2 && moveState == MOVE_COLLIDE_STATE_JUMP ) {
						setMoveState( MOVE_COLLIDE_STATE_LAND );
					}				
					
					if ( stateTime >= COLLIDE_PENALTY_TIME && moveState == MOVE_COLLIDE_STATE_LAND ) {						
						setMoveState( MOVE_STATE_THROTTLE );
						setState( STATE_RACING );
					}
				break;
				
				case STATE_FINISHED:
				case STATE_RACING:
				default:
					trackPosition.x += speed * delta;
				break;
			}
				
			stateTime += delta;
			
		}
		
		
		public function getState() : uint {
			return state;
		}
		
		
		public function isRacing() : Boolean {
			switch ( state ) {
				case STATE_RACING:
					return true;
				break;
				
				default:
					return false;
			}
		}
		
		
		public function move( direction : uint ) : void {
			switch ( direction ) {
				case DIRECTION_NONE:
					speedY = 0;
				break;
				
				case DIRECTION_DOWN:
//					if ( state == STATE_RACING )
//						speedY = SPEED_Y;
					TweenManager.tween( this, "speedYProperty", Regular.easeInOut, SPEED_Y, 0, 1 );		
				break;
				
				case DIRECTION_UP:
//					if ( state == STATE_RACING )
//						speedY = -SPEED_Y;
					TweenManager.tween( this, "speedYProperty", Regular.easeInOut, -SPEED_Y, 0, 1 );
				break;
			}
		}
		
		
		public function get speed() : Number {
			return _speed;
		}
		
		
		public function set speed( s : Number ) : void {
			_speed = s;
		}
		
		
		public function setState( state : uint ) : void {
			this.state = state;
			
			stateTime = 0;
			
			switch ( state ) {
				case STATE_LINING:
				break;
				
				case STATE_COUNTDOWN:
				break;
				
				case STATE_COLLIDE:
					setMoveState( MOVE_STATE_BRAKE );
				break;
				
				case STATE_RACING:
					decisionTimer = new Timer( 1, 1 );

					if ( !isPlayable )
						takeDecision( null );

					setMoveState( MOVE_STATE_THROTTLE );
				break;
				
				case STATE_FINISHED:
					setMoveState( MOVE_STATE_NONE );
				break;
			}
		}
		
		
		public function prepare( position : Point ) : void {
			trackPosition.x = position.x - Race.RACE_LENGTH;
			trackPosition.y = position.y;
			
			TweenManager.tween( this, "trackX", Regular.easeOut, x, position.x, Race.TIME_LINING );
			
			setState( STATE_LINING );
		}
		
		
		public function setMoveState( moveState : uint ) : void {
			if ( this.moveState != moveState ) {
				cancelSpeedTween();
				
				g1.rotationZ = 0.0;
				
				switch ( moveState ) {
					case MOVE_STATE_NONE:

						if ( decisionTimer != null ) {
							decisionTimer.reset();
							decisionTimer = null;				
						}
						isPlayable = false;

						speedTween = TweenManager.tween( this, "speed", Regular.easeInOut, speed, 0, ( speed / SPEED_MAX ) * THROTTLE_TIME );
					break;

					case MOVE_STATE_THROTTLE:
						speedTween = TweenManager.tween( this, "speed", Regular.easeIn, speed, SPEED_MAX, ( 1 - speed / SPEED_MAX ) * THROTTLE_TIME );
					break;
					
					case MOVE_STATE_BRAKE:
						speedTween = TweenManager.tween( this, "speed", Regular.easeOut, speed, 0, ( speed / SPEED_MAX ) * THROTTLE_TIME );
					break;
					

					case MOVE_COLLIDE_STATE_JUMP:
						g1.rotationZ = -15.0;
					break;
					
					
					
					
					case MOVE_STATE_TURBO:
					break;
				}
				
				if ( speedTween != null )
					speedTween.addEventListener( TweenEvent.MOTION_FINISH, onSpeedTweenEnd );
				
				this.moveState = moveState;
			}
		}
		
		
		private function cancelSpeedTween() : void {
			if ( speedTween != null ) {
				speedTween.stop();
				speedTween = null;
			}
		}
		
		
		private function onSpeedTweenEnd( e : Event ) : void {
			cancelSpeedTween();
		}
		
	}
}