package game
{
	import NGC.NanoMath.MovingRange;
	
	import flash.display.MovieClip;
	import flash.display.Shape;
	
	public class Obstacle extends TrackObject {
		
		public static const CACTUS : int = 1;
		
		private var sprite : MovieClip;
		
		public function Obstacle() {
			super();
			
			zLength = new MovingRange( - 10, 5 );
			
			var rand : uint = Math.random() * 99;
			rand = rand % 3;
			
			switch ( rand ) {
				case 1:
//					sprite = new Hole();
//				break;
				case 2:
//					sprite = new Ramp();
//				break;
				case 0:
					sprite = new Mine();
				break;
			}
			addChild( sprite );
			
			sprite.y += sprite.height / 2;
			
		}
	}
}