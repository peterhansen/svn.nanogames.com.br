package game
{
	import fl.motion.Color;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	public class Map extends MovieClip {
		
		public static const length : Number = 10000;
		
		public static const trackWidth : Number = 300;
		
		private var roadLeft : Sprite;
		
		private var roadRight : Sprite;
		
		private var skyLeft : Sprite;
		
		private var skyRight : Sprite;
		
		private var cactusLeft : Sprite;
		
		private var cactusRight : Sprite;
		
		private static const PARALLAX_1 : Number = 0.555;
		
		private static const PARALLAX_2 : Number = 0.2222;
		
		private const objects : Vector.<TrackObject> = new Vector.<TrackObject>;
		
		private var firstRacerIndex : int = 0;
		
		
		public function Map() {
			skyRight = new sky();
			addChild( skyRight );
			
			skyLeft = new sky();
			addChild( skyLeft );
			
			roadRight = new road();
			addChild( roadRight );
			
			roadLeft = new road();
			addChild( roadLeft );
			
			cactusRight = new cactus();
			addChild( cactusRight );
			
			cactusLeft = new cactus();
			addChild( cactusLeft );
			
			cactusRight.y = ( roadRight.y - ( roadRight.height ) / 2 ) + 20;
			cactusLeft.y = cactusRight.y;
			
			skyRight.y = ( cactusRight.y - ( cactusRight.height + skyRight.height ) / 2 ) + 250;
			skyLeft.y = skyRight.y;
		}
		
		
		public function addObject( o : TrackObject ) : void {
			o.visible = false;
			addChild( o );
			objects.push( o );
		}
		
		public function update( delta : Number, cameraPosition : Point ) : void {
			for ( var i : int = 0; i < objects.length; ++i ) {
				const o1 : TrackObject = objects[ i ];
				
				o1.update( delta );
				o1.refreshScreenPosition( cameraPosition );
				
				if ( o1 is Lane )
					continue;
				
				for ( var j : int = 1; j < objects.length; ++j ) {
					const o2 : TrackObject = objects[ j ];
					
					if ( o1 is Racer && ( o1 as Racer ).getState() != Racer.STATE_RACING || 
						o2 is Racer && ( o2 as Racer ).getState() != Racer.STATE_RACING ||
						o1 is Racer && ( o1 as Racer ).getState() == Racer.STATE_COLLIDE || 
						o2 is Racer && ( o2 as Racer ).getState() == Racer.STATE_COLLIDE
					) continue;
					
					if ( o1 != o2 && o1.checkCollision( o2 ) ) {
						
						if ( o1 is Obstacle && o2 is Obstacle ) {
							o2.x += 2 * ( o1.width + o2.width );
							continue;
						} 

						o1.trackY -= o1.zLength.length();
							
						if ( o2 is Racer ) {
							o2.trackY += o2.zLength.length();
							( o2 as Racer ).setState( Racer.STATE_COLLIDE );							
						} else if ( o2.trackX > o1.trackX ) {								
							o2.trackX += o2.width / 2;
						}
							
						if ( o1 is Racer )
							( o1 as Racer ).setState( Racer.STATE_COLLIDE );
					} 
				}
			}  
		}
		
		
		public function getLength() : Number {
			return length;
		}
		
		
		public function setPosition( x : Number ) : void {
			roadLeft.x = ( -x % roadRight.width ) - roadLeft.width / 2;
			roadRight.x = roadLeft.x + roadLeft.width;
			
			cactusLeft.x = ( -x * PARALLAX_1 % cactusLeft.width ) - cactusLeft.width / 2;
			cactusRight.x = cactusLeft.x + cactusLeft.width;
			
			skyLeft.x = ( -x * PARALLAX_2 % skyLeft.width ) - skyLeft.width / 2;
			skyRight.x = skyLeft.x + skyLeft.width;
		}
		
	}
}