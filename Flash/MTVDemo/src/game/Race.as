package game
{
	import NGC.TweenManager;
	
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.Regular;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;

	public class Race extends MovieClip {
		
		private var map : Map;
		
		private const racers : Vector.<Racer> = new Vector.<Racer>;

		private const puffs : Vector.<SmokePuff> = new Vector.<SmokePuff>;
		
		private const sortedRacers : Array = new Array();
		
		
		public static const TIME_LINING : Number = 3;
		
		public static const TIME_COUNTDOWN : Number = 3;
		public static const RACE_LENGTH : Number = 400;
		private static const TOTAL_RACERS : uint = 4;
		public var myRacer : int = 0;
		private static const STATE_NONE : uint = 0;
		private static const STATE_LINING : uint = 1;
		private static const STATE_COUNTDOWN : uint = 2;
		private static const STATE_RACING : uint = 3;
		private static const STATE_FINISHING : uint = 4;
		private static const STATE_FINISHED : uint = 5;
		
		private var state : uint;
		
		private var _cameraPosition : Number =  0;
		
		private static const CAMERA_FOLLOW_TIME : Number = 1.0;
		
		private var cameraTween : Tween;
		
		private var cameraSpeed : Number = 0;
		
		private var accTime : Number;
		
		/***/
		private const finalPlaces : Vector.<Racer> = new Vector.<Racer>; 
		private var msgBox : MessageBox;
		
		public function setActiveRacer( n : int ) : void {
			myRacer = n;
		}
		
		
		public function addSmokePuff( p : Point ) : void {
			var s : SmokePuff = new SmokePuff( p );
			map.addObject( s );
			sortedRacers.push( s );
			puffs.push( s );
		}
		
		public function getCameraPosition() : Number {
			return _cameraPosition;
		}
		
		public function Race() {
			map = new Map();
			addChild( map );
		

			var finalLane : Lane = new Lane( 0x0 );
			
			finalLane.trackX = Map.length;
			finalLane.trackY = - ( Map.trackWidth / 2 ) - 10;
		
			sortedRacers.push( finalLane );
			map.addObject( finalLane );
			
			
			var initialLane : Lane = new Lane( 0x00FF00 );
			
			initialLane.trackX = ( width ) / 2;
			initialLane.trackY = - ( Map.trackWidth / 2 ) - 10;
		
			sortedRacers.push( initialLane );
			map.addObject( initialLane );
			
			for ( var i : int = 0; i < TOTAL_RACERS; ++i ) {
				const r : Racer = new Racer( i % 4 );
				racers.push( r );
				sortedRacers.push( r );
				map.addObject( r );
				
				if ( i != 0 )
					r.setAsAI();
			}

			msgBox = new MessageBox();
			msgBox.setMessage("Fim de corrida!");
			addChild( msgBox );
			msgBox.alpha = 0;

			setState( STATE_LINING );
		}
		
		public function addObstacle( obstacleKind: int, position: Point ) : void {
			
			var tmp : Obstacle = new Obstacle();
			
			tmp.trackX = position.x;
			tmp.trackY = position.y;
			
			map.addObject( tmp );
			sortedRacers.push( tmp );
		}
		
		public function addBomb( bomb:GameBomb, position: Point ) : void {
			
			bomb.trackX = position.x;
			bomb.trackY = position.y;
			
			map.addObject( bomb );
			sortedRacers.push( bomb );
		}
		
		public function getTotalRacers() : int {
			return racers.length;
		}
		
		public function update( delta : Number ) : void {
			accTime += delta;
			
			map.update( delta, new Point( cameraPosition, 0 ) );
			
			var r : int = 0;
			updateCamera( delta );

			sortedRacers.sortOn([ "trackY", "trackX" ], Array.DESCENDING | Array.NUMERIC);
			sortedRacers.reverse();
			for( var i:uint = 0; i < sortedRacers.length; i++ ) {
				var item : Sprite = sortedRacers[i];
				map.setChildIndex( item, 6 + i );
			}
			
			switch ( state ) {
				case STATE_NONE:
					break;
				
				case STATE_LINING:
					if ( accTime >= TIME_LINING )
						setState( STATE_COUNTDOWN );
					break;
				
				case STATE_COUNTDOWN:
					if ( accTime >= TIME_COUNTDOWN )
						setState( STATE_RACING );
					break;
				
				case STATE_RACING:
					for ( r = 0; r < racers.length; ++r ) {
						if ( racers[ r ].trackX >= map.getLength() ) {
							// 1º competidor cruzou a linha de chegada - temos um vencedor
							racers[ r ].setState( Racer.STATE_FINISHED );
							finalPlaces.push( racers[ r ] );							
							trace( "RACER #" + r + " WON THE RACE." );
							setState( STATE_FINISHING );
						}
					}
					
					var s : SmokePuff;
					
					for ( r = 0; r < puffs.length; ++r ) {
						s = puffs[ r ];
	
						if ( !s.visible )
							removeChild( s );
					}
					
					break;
				
				case STATE_FINISHING:
					for ( r = 0; r < racers.length; ++r ) {
						if ( racers[ r ].isRacing() && racers[ r ].trackX >= map.getLength() ) {
							// mais um competidor cruzou a linha de chegada
							racers[ r ].setState( Racer.STATE_FINISHED );
							finalPlaces.push( racers[ r ] );
							trace( "RACER #" + r + " HAS FINISHED IN POSITION " + finalPlaces.length + "." );
						}
					}
					
					if ( finalPlaces.length >= racers.length )
						setState( STATE_FINISHED );
				break;
				
				case STATE_FINISHED:
					break;
			}
		}
		
		
		public function setState( state : uint ) : void {
			this.state = state;
			accTime = 0;
			
			switch ( state ) {
				case STATE_NONE:
				break;
				
				case STATE_LINING:
					for ( var i : int = 0; i < TOTAL_RACERS; ++i ) {
						racers[ i ].prepare( new Point( ( ( i / -3 ) ) * 120, ( ( i - 1 ) % 3 ) * 70 ) );
					}
				break;
				
				case STATE_COUNTDOWN:
				break;
				
				case STATE_RACING:
					
					for ( var r : int = 0; r < racers.length; ++r ) {
						
						if ( myRacer  == r )
							racers[ r ].setAsPlayable();
						else
							racers[ r ].setAsAI();
						
						racers[ r ].setState( Racer.STATE_RACING );
					}
					
				break;
				
				case STATE_FINISHING:
				break;
				
				case STATE_FINISHED:
					msgBox.fadeIn();
				break;
			}
		}
		
		
		public function get cameraPosition() : Number {
			return _cameraPosition;
		}
		
		
		public function set cameraPosition( cp : Number ) : void {
			_cameraPosition = cp;
		}
		
		
		private function updateCamera( delta : Number ) : void {
			cancelCameraTween();
			
			cameraPosition += cameraSpeed * delta;
			cameraSpeed = ( racers[ myRacer ].trackX + racers[ myRacer ].width / 2 + racers[ myRacer ].speed - cameraPosition ) / CAMERA_FOLLOW_TIME;
			
//			const newCameraPosition : Number = Math.max( 0, 300 + racers[ 0 ].trackX + racers[ 0 ].speed * 2 );
//			cameraTween = TweenManager.tween( this, "cameraPosition", Regular.easeInOut, cameraPosition, newCameraPosition, CAMERA_FOLLOW_TIME );
//			cameraTween.addEventListener( TweenEvent.MOTION_FINISH, onCameraFollow );
			map.setPosition( cameraPosition );
		}
		
		
		private function cancelCameraTween() : void {
			if ( cameraTween != null ) {
				cameraTween.stop();
				cameraTween = null;
			}
		}
		
		
		private function onCameraFollow( e : Event ) : void {
			updateCamera( 0 );
		}
		
		
		public function getRacer( index : uint ) : Racer {
			return racers[ index ];
		}
		
	}
}