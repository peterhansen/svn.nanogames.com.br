package game
{
	import flash.display.Sprite;
	
	public class Lane extends TrackObject
	{
		public function Lane( color : uint )
		{
			super();
			const bla : Sprite = new Sprite();
			bla.graphics.beginFill( color );
			bla.graphics.drawRect( 0, 210, 20, 450 );
			bla.graphics.endFill();
			addChild( bla );			
		}
		
		override public function checkCollision( o : TrackObject ) : Boolean {
			return false;
		}
	}
}