package game
{
	import NGC.ASWorkarounds.MovieClipVS;
	import NGC.NanoMath.MovingRange;
	import NGC.Range;
	
	import flash.geom.Point;

	public class TrackObject extends MovieClipVS {
		
		protected const trackPosition : Point = new Point();
		public var zLength : MovingRange;
		private var trackHeightProperty : Number;
		
		public function TrackObject() {
			zLength = new MovingRange( -10, 1 );
			trackHeight = 0;
		}
		
		
		public function get trackX() : Number{
			return trackPosition.x;
		}
		
		
		public function get trackY() : Number{
			return trackPosition.y;
		}
		
		
		public function set trackX( tx : Number ) : void {
			trackPosition.x = tx;
		}
		
		public function set trackY( ty : Number ) : void {
			trackPosition.y = ty;
		}
		
		public function refreshScreenPosition( screenPosition : Point ) : void {
			visible = true;
			x = trackPosition.x - screenPosition.x;
			y = trackPosition.y - screenPosition.y - trackHeight;
			zLength.updateOffset( y );
		}
		
		public function get trackHeight() : Number{
			return trackHeightProperty;
		}
		
		
		public function set trackHeight( ty : Number ) : void {
			trackHeightProperty = ty;
		}

		
		public function getZLength() : MovingRange {
			return zLength;
		}
		
		public function checkCollision( o : TrackObject ) : Boolean {
			return o.hitTestObject( this ) && zLength.intersect( o.getZLength() );
		} 
		
		
		public function update( delta : Number ) : void {
			
		}
		
	}
}