package game
{
	
	import flash.display.MovieClip;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.geom.Transform;

	import mx.core.MovieClipAsset;
	
	public class GameBomb extends TrackObject {
		
		private var timeToExplode : Number;
		
		private var sprite : MovieClip;
		private var explosion : Explosion;
		private var shadow : MovieClip;
		
		public static const EXPLOSION_COUNT_TIME: Number = 4;
		
		private var showState : uint;
		
		private static const STATE_PULSATING: uint = 1;
		private static const STATE_BLOWING: uint = 2;
		private static const STATE_INACTIVE: uint = 3;
		private static const EXPLOSION_TIME: Number = 2;
		
		
		public function GameBomb() {
			super();
			
			sprite = null;
			shadow = null;
			timeToExplode = EXPLOSION_COUNT_TIME;
			setShowState( STATE_PULSATING );
			trackHeight = 600;
		}
		
		public function setShowState( newState: uint ) : void {
			
			if ( showState == newState )
				return;
				
			if ( sprite != null ) {
				removeChild( sprite );				
			}
			
			if ( shadow != null ) {
				removeChild ( shadow ); 
			}
			
			showState = newState;
			
			switch ( newState ) {
				case STATE_PULSATING:
					sprite = new Bomb();
					shadow = new Bomb();
					addChild( shadow );
					addChild( sprite );
					sprite.y += 2 * sprite.height;
					shadow.y += 2 * shadow.height;
					break;
				case STATE_BLOWING:
					sprite = new Explosion();
					shadow = new Explosion();
					addChild( shadow );
					addChild( sprite );		
					break;
				case STATE_INACTIVE:
					visible = false;
					sprite = null;
					shadow = null;
					break;
			}
			
			if ( shadow != null ) {
				shadow.scaleY = 0.25;
				var brillo : Number =  -1;
				var colorTrans:ColorTransform= new ColorTransform();
				var trans:Transform=new Transform( shadow );
				colorTrans.redMultiplier = colorTrans.greenMultiplier = colorTrans.blueMultiplier = 1 - Math.abs (brillo); // color percent
				colorTrans.redOffset=colorTrans.greenOffset=colorTrans.blueOffset= (brillo > 0) ? brillo * 256 : 0; // color offset
				trans.colorTransform=colorTrans;
				shadow.alpha = 0.6;
				shadow.y = ( sprite.height * 2.5 ) + trackHeight;
			}
		}
		
		
		public override function update( delta : Number ) : void {
			
			if ( visible == false || sprite == null )
				return;
			
			if ( shadow != null )
				shadow.y = ( sprite.height * 2.5 ) + trackHeight;			
			
			var deltaT : Number = ( EXPLOSION_COUNT_TIME - timeToExplode )
			trackHeight-= 5 * 10 * ( deltaT * deltaT );
			
			if ( trackHeight <= 0 )
				trackHeight = 0;
			
			timeToExplode -= delta;
			sprite.scaleX = 1 - 0.1 * Math.sin( 2 * timeToExplode % ( 2 * Math.PI ) );
				
			if ( timeToExplode <= 0 ) {
					if ( timeToExplode >= - EXPLOSION_TIME ) {
						setShowState( STATE_BLOWING );
					} else {
						setShowState( STATE_INACTIVE );
					}
			}
		}
		
		
		public override function checkCollision( o : TrackObject ) : Boolean {
			if ( timeToExplode >= 0 ) {
				var collided : Boolean = super.checkCollision( o ); 
				if ( collided ) {
					timeToExplode = 0;
					setShowState( STATE_BLOWING );
				}
				return collided;
			}
			
			return false;
		}
	}
}