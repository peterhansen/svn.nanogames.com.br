package {
	import NGC.ByteArrayEx;
	import NGC.NanoOnline.INOListener;
	import NGC.NanoOnline.NOAppCustomerData;
	import NGC.NanoOnline.NOConnection;
	import NGC.NanoOnline.NOCustomer;
	import NGC.NanoOnline.NOErrors;
	import NGC.NanoOnline.NOGlobals;
	import NGC.NanoOnline.NORanking;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.NanoSounds.SoundManager;
	import NGC.ScheduledTask;
	import NGC.Utils;
	
	import fl.controls.ProgressBar;
	import fl.controls.ProgressBarDirection;
	import fl.controls.ProgressBarMode;
	import fl.transitions.Tween;
	import fl.transitions.TweenEvent;
	import fl.transitions.easing.*;
	
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.external.ExternalInterface;
	import flash.media.Sound;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.system.Security;
	import flash.system.SecurityDomain;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.utils.getDefinitionByName;
	import NGC.TweenManager;
	
	// TODO: Carregar parâmetros do JAVASCRIPT!!!!
	// - Ou alterar arquiv .html gerado para o programa 
	// - Ou http://www.adobe.com/livedocs/flash/9.0/ActionScriptLangRefV3/flash/external/ExternalInterface.html

	// Metatags que definem as propriedades padrão do stage
	[SWF( width= "1024", height = "640", backgroundColor = "0x0d2166", frameRate = "60" )]
	public final class Application extends MovieClip implements IApplication, INOListener
	{
		// Valores de alpha de cena utilizados quando estamos mostrando o feedback de espera
		private static const SCENE_WAIT_NO_ALPHA : Number = 0.3;
		private static const SCENE_WAIT_FULL_ALPHA : Number = 1.0;
		
		/** Perfil que está logado ou null caso o usuário esteja jogando sem fazer login/cadastrar-se */		
		private var _loggedProfile : NOCustomer;
		
		private var _userId : int = -1;
		
		private var _userNickname : String = null;
		
		/** Barra de progresso */		
		private var _loadingBar : ProgressBar;
		
		/** Indica o estado (ponto atual) da aplicação */		
		private var _appState : ApplicationState;

		/** Objeto que controla as transições do feedback de espera */		
		private var _waitFeedbackTweener : Tween;
		
		/** Popup utilizado para exibir mensagens para o usuário */		
		private var _popup : PopUp;
		
		/** Objeto utilizado para podermos executar um método assincronamente */		
		private var _backToMenuScheduler: ScheduledTask;
		
		/** Única instância da classe http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript */
		private static var _singleton : Application;
		// OLD : Esta implementação de singleton não funciona com a classe que representa o jogo...
		//private static var _singleton : Application = new Application();
		
		private var _nextScene : AppScene;
		
		/**
		* Construtor
		* OBS: AS3 não permite construtores privados e protegidos. Logo, apesar de se tratar de uma classe SINGLETON, temos
		* que manter o construtor público 
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript
		*/	
		public function Application()
		{
			super();
			
			// Se já construímos a variável estática...
			if( _singleton != null )
				throw new Error( "This is a singleton class. You should call GetInstance() method." );
			
			Security.loadPolicyFile( NOGlobals.NANO_ONLINE_URL );
			
			_loggedProfile = null;
			
			_singleton = this;
			
			tabChildren = false;
			tabEnabled = false;
			
			loadSounds();
			
			loadFonts();
			
			// Só quando formos adicionados ao stage é que poderemos utilizar a propriedade 'stage' do DisplayObject. Para resolver este
			// problema, utilizamos o evento ADDED_TO_STAGE. Iremos realizar as rotinas que deveriam estar no construtor apenas na
			// callback chamada pelo evento
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
		}
		
		
		/**
		 * Carrega os sons utilizados no jogo 
		 */		
		public function loadSounds() : void {
		}
		
		/**
		 * Ver Constants.as: "set RSC_GLOBAL_PATH"
		 */
		public function get resourcePath() : String {
			// Esta função é necessária por estarmos encapsulando o SWF da aplicação em código MXML, o que
			// torna flashvars inacessível
			return ExternalInterface.call( "GetResourcePath" );
		}	
		
		
		/**
		* Retorna a única instância da classe
		* http://en.wikipedia.org/wiki/Singleton_pattern#Flash_ActionScript 
		* @return A única instância da classe 
		*/		
		public static function GetInstance() : Application {
			return _singleton;
		}

		
		/**
		* Evento chamado quando este DisplayObject é adicionado ao stage
		* @param evt Objeto que encapsula os dados do evento
		*/
		private function onAddedToStage( e : Event ) : void
		{
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			// Configura a barra de progresso
			_loadingBar = new ProgressBar();			
			_loadingBar.direction = ProgressBarDirection.RIGHT;
			_loadingBar.mode = ProgressBarMode.MANUAL;
			_loadingBar.minimum = 0.0;
			_loadingBar.maximum = 1.0;
			
			// Configura o popup
			//var font : FontLaCartoonerie = new FontLaCartoonerie();
			var font : ArialRegular = new ArialRegular();
			_popup = new PopUp( stage, new Popup(), true, 15, 15, 15, 15, font.fontName, 20, 0xFFFFFF, 0.5 );

			// Configura o NanoOnline
			NOConnection.GetInstance().host = NOGlobals.NANO_ONLINE_URL;
			NOConnection.GetInstance().appVersion = Constants.APP_VERSION;
			NOConnection.GetInstance().appShortName = Constants.NO_APP_SHORT_NAME;
			
			const savedProfile : SharedObject = SharedObject.getLocal( "customerId" );
			trace( "SAVED NANO ONLINE PROFILE: " + savedProfile.data.customerId );
			if ( savedProfile.data.customerId != null && savedProfile.data.customerId != -1 ) {
				const customer : NOCustomer = new NOCustomer();
				
				trace( "ID: " + savedProfile.data.customerId );
				trace( "FIRST_NAME: " + savedProfile.data.firstName );
				trace( "NICKNAME: " + savedProfile.data.nickname );
				
				customer.profileId = savedProfile.data.customerId;
				customer.firstName = savedProfile.data.firstName;
				customer.nickname = savedProfile.data.nickname;
				
				setLoggedProfile( customer );
			}
			
			// OLD: Somente para agilizar os testes
			setState( ApplicationState.APP_STATE_NEWGAME );
//			setState( ApplicationState.APP_STATE_SPLASH );
//			setState( ApplicationState.APP_STATE_LOGIN );
		}
		
		
		/** Indica que uma requisição foi enviada para o NanoOnline */
		public function onNORequestSent() : void { }
		
		/** Indica que a requisição foi cancelada pelo usuário */
		public function onNORequestCancelled() : void { }
		
		/** Indica que uma requisição foi respondida e terminada com sucesso */
		public function onNOSuccessfulResponse() : void {
			trace( "OK" );
		}
		
		/**
		 * Sinaliza erros ocorridos nas operações do NanoOnline
		 * @param errorCode O código do erro ocorrido
		 * @param errorStr Descrição do erro ocorrido
		 */		
		public function onNOError( errorCode : NOErrors, errorStr : String ) : void {
			trace( "Error: " + errorCode + " => " + errorStr );
		}
		
		/**
		 * Indica o progresso da requisição atual
		 * @param currBytes A quantidade de bytes que já foi transferida
		 * @param totalBytes A quantidade de bytes total que deve ser transferida na requisição 
		 */		
		public function onNOProgressChanged( currBytes : int, totalBytes : int ) : void {
		}
		
		/**
		* Retorna uma referência para a janela da aplicação 
		* @return Uma referência para a janela da aplicação
		*/		
		public function getStage() : Stage {
			return stage;
		}
		
		/**
		* Retorna o perfil que está logado ou <code>null</code> caso o usuário esteja jogando sem fazer login/cadastrar-se
		* @return O perfil que está logado ou <code>null</code> caso o usuário esteja jogando sem fazer login/cadastrar-se
		*/		
		public function getLoggedProfile() : NOCustomer {
			return 	_loggedProfile;
		}
		
		
		public function logOut() : void {
			setLoggedProfile( null );
		}
		
		
		public function setLoggedProfile( profile : NOCustomer ) : void {
			const savedProfile : SharedObject = SharedObject.getLocal( "customerId" );
			
			if ( profile != null && profile.profileId != -1 ) {
				savedProfile.data.customerId = profile.profileId;
				savedProfile.data.email = profile.email;
				savedProfile.data.firstName = profile.firstName;
				savedProfile.data.gender = profile.gender;
				savedProfile.data.nickname = profile.nickname;
				
//				var url : String = Constants.URL_BIG_COLA + "/session_storage";
//				var urlLoader : URLLoader;
//				var urlReq : URLRequest;
//				
//				try {
//					urlLoader = new URLLoader();
//					//					urlLoader.addEventListener( IOErrorEvent.IO_ERROR, errorHandlerListener );
//					//					urlLoader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, errorHandlerListener );
//					
//					if ( urlLoader == null ) 
//						return;
//					
//					urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
//					
//					urlReq = new URLRequest( url );
//					urlReq.method = URLRequestMethod.POST;
//					urlReq.data = "uid=" + profile.profileId;
//					
//					if ( urlReq == null )	
//						return;
//					
//					urlLoader.load(urlReq);
//					
//					//if (f != null) urlLoader.addEventListener( Event.COMPLETE, f );
//				} catch (e:Error) {
//					trace( e.getStackTrace() );
//				}
				
				_loggedProfile = profile;
			} else {
				_loggedProfile = null;
				savedProfile.data.customerId = -1;
			}
			
			trace( "SAVING NANO ONLINE PROFILE: " + savedProfile.data.customerId );
			savedProfile.flush();
			
			// atualiza no javascript o id do perfil logado
			ExternalInterface.call( "setProfileId", savedProfile.data.customerId );
		}
		
		
		/**
		* Determina o estado da aplicação 
		* @param newState O novo estado da aplicação
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/		
		public function setState( newState : ApplicationState, frame : Object = null ) : void
		{
			trace( "setState( " + newState + ", " + frame + " )" );
			var sm : SoundManager = SoundManager.GetInstance();
			
//			var tempProfile : NOCustomer;
//			if( _appState == ApplicationState.APP_STATE_FORM ) TODO
//			{
//				tempProfile = ( getCurrScene() as SceneFormEx ).createdProfile;
//			}
//			else if( _appState == ApplicationState.APP_STATE_LOGIN )
//			{
//				tempProfile = ( getCurrScene() as SceneLoginEx ).loggedProfile;
//			}
//			
//			if( tempProfile != null ) 
//				_loggedProfile = tempProfile;
			
			var nextScene : AppScene = null;
			switch( newState )
			{				
				case ApplicationState.APP_STATE_SPLASH:
				break;
				
				case ApplicationState.APP_STATE_LOGIN:
					nextScene = new AppScene( new NanoOnlineScreen() );
				break;
				
				case ApplicationState.APP_STATE_NEWGAME:
					nextScene = new AppScene( new GameScreen() );
				break;
				
				case ApplicationState.APP_STATE_BACK_TO_GAME:
					sm.stopAllSounds();
				break;
				
				default:
					throw new ArgumentError( "Invalid Application State" );
				break;
			}
			
			trace( "Indo para a cena " + newState );
			
			// TODO : Fazer isso de uma forma mais esperta (com estados)
			// Garante que a tela não está travada na animação de espera
			if( getCurrScene() != null )
			{
				cancelWaitTween();
				hideWaitFeedback();
				cancelWaitTween();
			}

			changeCurrScene( nextScene, frame );
			_appState = newState;
		}
		
		/**
		* Callback chamada quando devemos ir para o menu, vindo do jogo
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onFromVilaToMenu( e : Event ) : void
		{
			_backToMenuScheduler = new ScheduledTask( 50, onFromVilaToMenuAfterDelay );
			_backToMenuScheduler.call();
		}
		
		/**
		* Callback chamada, após um delay, para de fato exibirmos a tela do menu após sairmos da tela de jogo 
		*/		
		private function onFromVilaToMenuAfterDelay() : void
		{
			_backToMenuScheduler = null;
			
			var currScene : AppScene = getCurrScene();
			
			// No modo Release vem null...
			if( currScene != null )
				currScene.removeEventListener( Event.ADDED, onFromVilaToMenu );
			
			// No modo Release vem null...
			getCurrScene().dispatchEvent( new Event( AppScene.APPSCENE_LOADING_COMPLETE ) );
		}
		
		/**
		* Inicia a reprodução da música do menu  
		*/		
		public function playMenuTheme() : void
		{
			var sm : SoundManager = SoundManager.GetInstance();
//			var musicMenuTheme : MenuTheme = new MenuTheme(); TODO
//			sm.playSoundAtIndex( sm.pushSound( musicMenuTheme ) );
		}
		
		/**
		* Trava o tratamento de input da cena atual e exibe um feedback de espera para o usuário 
		*/		
		public function showWaitFeedback( cancelable : Boolean = false, onCancelCallback : Function = null ) : void
		{
			if( cancelable )
			{
				// TODO
				//showPopUp();
			}

			doWaitAnimTween( SCENE_WAIT_FULL_ALPHA, SCENE_WAIT_NO_ALPHA, onStartWaitAnimEnded );
		}
		
		/**
		* Força a animação de feedback de espera a terminar   
		*/		
		private function cancelWaitTween() : void
		{
			if( _waitFeedbackTweener != null )
			{
				_waitFeedbackTweener.fforward();
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onStartWaitAnimEnded );
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onHideWaitAnimEnded );
			}
		}

		/**
		* Modifica o valor de alpha da cena atual utilizando animação 
		* @param begin O valor que a propriedade alpha deve possuir antes do início da animação
		* @param end O valor que a propriedade alpha deverá ter ao término da animação
		* @param onDoneCallback A callback que deve ser chamada quando a animação acabar
		*/		
		private function doWaitAnimTween( begin : Number, end : Number, onDoneCallback : Function ) : void
		{
			var currScene : AppScene = ( getCurrScene() as AppScene );
			currScene.blendMode = BlendMode.LAYER;
			
			cancelWaitTween();
			
			_waitFeedbackTweener = TweenManager.tween( currScene, "alpha", Regular.easeInOut, begin, end, Constants.WAIT_ANIM_DUR );
			_waitFeedbackTweener.addEventListener( TweenEvent.MOTION_FINISH, onDoneCallback );
			_waitFeedbackTweener.start();
			
			currScene.mouseEnabled = false;
			currScene.mouseChildren = false;
		}
	
		/**
		* Callback chamada quando acabamos de fazer a animação de exibir o feedback de espera 
		* @param e O objeto que encapsula os dados do evento
		*/
		private function onStartWaitAnimEnded( e : TweenEvent ) : void
		{
			if( _waitFeedbackTweener != null )
			{
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onStartWaitAnimEnded );
				_waitFeedbackTweener = null;
			}
		}
		
		/**
		* Callback chamada quando acabamos de fazer a animação de esconder o feedback de espera 
		* @param e O objeto que encapsula os dados do evento
		*/		
		private function onHideWaitAnimEnded( e : TweenEvent ) : void
		{
			if( _waitFeedbackTweener != null )
			{
				_waitFeedbackTweener.removeEventListener( TweenEvent.MOTION_FINISH, onHideWaitAnimEnded );
				_waitFeedbackTweener = null;
			}
			
			var currScene : AppScene = ( getCurrScene() as AppScene );
			currScene.blendMode = BlendMode.NORMAL;
			currScene.mouseEnabled = true;
			currScene.mouseChildren = true;
		}
		
		/**
		* Esconde o feedback de espera e libera o tratamento de input da cena atual
		*/		
		public function hideWaitFeedback() : void
		{
			doWaitAnimTween( SCENE_WAIT_NO_ALPHA, SCENE_WAIT_FULL_ALPHA, onHideWaitAnimEnded ); 
		}
		
		/**
		* Exibe um popup para o usuário travando o tratamento de input da cena atual enquanto
		* o mesmo estiver visível
		*/		
		public function showPopUp( msg : String, ...btsTitlesAndCallbacks ) : void
		{
			var args : Array = btsTitlesAndCallbacks;
			args.unshift( msg );
			_popup.show.apply( null, args );
			_popup.x = ((Constants.STAGE_WIDTH - _popup.width)/2)-50;			
			_popup.y = ((Constants.STAGE_HEIGHT - _popup.height)/2)-65;			
		}
		
		/**
		* Retorna a cena atual da aplicação 
		* @return A cena atual da aplicação
		*/		
		public function getCurrScene() : AppScene	
		{
			if( numChildren == 0 )
				return null;
			return ( getChildAt( 0 ) as AppScene );
		}
		
		/**
		* Retira a cena atual da memória 
		*/		
		private function deleteCurrScene() : void
		{
			var nChildren : int = numChildren;
			for( var i : int = 0 ; i < nChildren ; ++i )
			{
				var temp : AppScene = ( getChildAt( 0 ) as AppScene );
				if( temp != null )
					temp.stop();

				removeChildAt( 0 );
			}
		}
		
		/**
		* Modifica a cena atual da aplicação de acordo com suas configurações 
		* @param nextScene A cena para qual devemos transitar
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/		
		private function changeCurrScene( nextScene : AppScene, frame : Object = null ) : void
		{			
			trace( "changeCurrScene( " + nextScene + ", " + frame + " ) -> " + nextScene.asynchronousLoading );
			_nextScene = nextScene;
			// Faz a transição
			var currScene : AppScene = getCurrScene();
			if ( currScene ) {
				currScene.mouseChildren = false;
				currScene.mouseEnabled = false;
				currScene.stop();
				
				var t : Tween = TweenManager.tween( currScene, "alpha", Regular.easeInOut, currScene.alpha, 0, 1.1 );
				t.addEventListener( TweenEvent.MOTION_FINISH, startTransitionToNextScene );
			} else {
				startTransitionToNextScene();
			}
		}
		
		
		public function startTransitionToNextScene( e : Event = null ) : void {
			setCurrScene( _nextScene );
			var t2 : Tween = TweenManager.tween( _nextScene, "alpha", Regular.easeInOut, 0, 1, 1.1 );
			_nextScene = null;
		}
		

		/**
		* Determina a cena principal da aplicação 
		* @param nextScene A nova cena principal
		* @param frame Ou um número representando o número do frame, ou uma string representando o label do frame. Se null, a cena começa do frame 0
		*/
		private function setCurrScene( nextScene : AppScene, frame : Object = null ) : void
		{
			deleteCurrScene();

			if( frame != null )
			{
				nextScene.stop();
				nextScene.gotoAndPlay( frame );
			}
			
			addChildAt( nextScene, 0 );

			if( nextScene.anchorAtCenter )
			{
				nextScene.x = stage.stageWidth * 0.5;
				nextScene.y = stage.stageHeight * 0.5;
			}
			else
			{
				nextScene.x = 0.0;
				nextScene.y = 0.0;
			}
		}

		/**
		* Callback chamada quando a transição escurece a tela por completo 
		* @param args Os parâmetros da callback 
		*/		
		private function onCloseTransitionEnded( ...args ) : void
		{
			trace( "onCloseTransitionEnded" + args );
			var nextScene : AppScene = args[0];
			nextScene.addEventListener( AppScene.APPSCENE_LOADING_COMPLETE, onSceneLoaded );
			nextScene.addEventListener( ProgressEvent.PROGRESS, onLoadingProgressChanged );

			// Começa a carregar a cena
			nextScene.visible = false;
			setCurrScene( nextScene, args[1] );

			if( nextScene.usesProgressEvent )
				showProgressBar();
		}
		
		/**
		* Exibe a barra de progresso  
		*/		
		private function showProgressBar() : void
		{
			trace( "showProgressBar()" ); 
			_loadingBar.reset();
			_loadingBar.setProgress( 0.0, 1.0 );
			
			addChild( _loadingBar );

			_loadingBar.x = ( stage.stageWidth - _loadingBar.width ) * 0.5;
			_loadingBar.y = ( stage.stageHeight - _loadingBar.height ) * 0.5;
		}
		
		/**
		* Esconde a barra de progresso  
		*/		
		private function hideProgressBar() : void
		{
			if( _loadingBar.parent == this )
				removeChild( _loadingBar );
		}
		
		/**
		* Callback chamada quando uma cena de carregamento assíncrono acaba de ser carregada 
		* @param e O objeto que encapsula os dados do evento 
		*/		
		private function onSceneLoaded( e : Event ) : void
		{
			trace( "onSceneLoaded" );
			var currScene : AppScene = getCurrScene();
			currScene.removeEventListener( AppScene.APPSCENE_LOADING_COMPLETE, onSceneLoaded );
			currScene.removeEventListener( ProgressEvent.PROGRESS, onLoadingProgressChanged );
			
			hideProgressBar();

//		TODO teste	var transition : CircleTransition = new CircleTransition( currScene, Constants.TRANSITION_DUR, Constants.STAGE_FPS, false );
//			transition.start( 0.0, this, onOpenTransitionEnded );
			
			currScene.visible = true;
		}
		
		/**
		* Carrega as fontes utilizadas no jogo
		* Além de linkar a biblioteca (.SWC) que contém a classe da fonte com o projeto, é necessário instanciar cada fonte utilizada ao menos uma vez. Sem fazê-lo, o programa
		* não irá reconhecer as fontes embarcadas. Isto acontece porque as fontes são registradas na run-time somente quando invocamos seus construtores.
		*/		
		public static function loadFonts() : void
		{
		}
		
		/**
		* Callback chamada quando a transição que torna a cena visível acaba 
		* @param args Os parâmetros da callback
		*/		
		private function onOpenTransitionEnded( ...args ) : void
		{
			trace( "terminou opening" );

			var currScene : AppScene = getCurrScene();
			
			// Gambiarra!!!!!!!!!!!!!!!!
//			if( Utils.GetObjClass( currScene ) == VilaVirtual )
//			{
//				var gameScene : VilaVirtual = ( currScene as VilaVirtual );
//			TODO	gameScene.startRunning();
//			}

			currScene.mouseChildren = true;
			currScene.mouseEnabled = true;
		}
		
		/**
		* Evento que informa quando o progresso de carregamento do arquivo SWF muda
		* @param e Objeto que encapsula os dados do evento
		*/		
		private function onLoadingProgressChanged( e : ProgressEvent ) : void
		{
			var percent : Number = e.bytesLoaded / e.bytesTotal;
			_loadingBar.setProgress( percent, _loadingBar.maximum );
		}
		
		
		public static function submitRecords( score : Number, biggestCombo : uint, perfectMoves : uint, level : uint ) : void {
			const profile : NOCustomer = GetInstance().getLoggedProfile();
			
			if ( profile != null ) {
	//			const rankings : Array = new Array();
				var ranking : NORanking = new NORanking( Constants.RANKING_TYPE_HIGHEST_SCORE );
				var entriesToSubmit : Vector.<NORankingEntry > = new Vector.<NORankingEntry >();
				entriesToSubmit.push( new NORankingEntry( profile.profileId, score, profile.nickname ) );
				ranking.localEntries = entriesToSubmit;
				NORanking.SendSubmitRequest( ranking, new RankingListener( "RANKING_TYPE_HIGHEST_SCORE" ) );
				
	//			// pontuação acumulada
	//			ranking = new NORanking( Constants.RANKING_TYPE_CUMULATIVE_SCORE );
	//			entriesToSubmit = new Vector.<NORankingEntry >();
	//			entriesToSubmit.push( new NORankingEntry( profile.profileId, score, profile.nickname ) );
	//			ranking.localEntries = entriesToSubmit;
	//			NORanking.SendSubmitRequest( ranking, new RankingListener( "RANKING_TYPE_CUMULATIVE_SCORE" ) );
	////			
	////			// maior combo
	//			ranking = new NORanking( Constants.RANKING_TYPE_BIGGEST_COMBO );
	//			entriesToSubmit = new Vector.<NORankingEntry >();
	//			entriesToSubmit.push( new NORankingEntry( profile.profileId, biggestCombo, profile.nickname ) );
	//			ranking.localEntries = entriesToSubmit;
	//			NORanking.SendSubmitRequest( ranking, new RankingListener( "RANKING_TYPE_BIGGEST_COMBO" ) );
	////			
	////			// quantidade de movimentos perfeitos
	//			ranking = new NORanking( Constants.RANKING_TYPE_PERFECT_MOVES );
	//			entriesToSubmit = new Vector.<NORankingEntry >();
	//			entriesToSubmit.push( new NORankingEntry( profile.profileId, perfectMoves, profile.nickname ) );
	//			ranking.localEntries = entriesToSubmit;
	//			NORanking.SendSubmitRequest( ranking, new RankingListener( "RANKING_TYPE_PERFECT_MOVES" ) );
	////			
	////			// nível máximo alcançado
	//			ranking = new NORanking( Constants.RANKING_TYPE_HIGHEST_LEVEL );
	//			entriesToSubmit = new Vector.<NORankingEntry >();
	//			entriesToSubmit.push( new NORankingEntry( profile.profileId, level, profile.nickname ) );
	//			ranking.localEntries = entriesToSubmit;
	//			NORanking.SendSubmitRequest( ranking, new RankingListener( "RANKING_TYPE_HIGHEST_LEVEL" ) );
			}
		}
	}
}
