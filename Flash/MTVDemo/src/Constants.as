package
{
	import NGC.ASWorkarounds.StaticClass;
	
	import flash.geom.Point;

	public final /* STATIC */ class Constants extends StaticClass
	{
		/**
		* Construtor : Vai disparar uma exceção se for chamado, pois classes estáticas não devem ser instanciadas
		*/		
		public function Constants()
		{
			super();
		}

		/**
		* Versão da aplicação 
		*/		
		public static const APP_VERSION : String = "1.0.0";
		
		public static const STAGE_WIDTH : uint = 800;
		public static const STAGE_HEIGHT : uint = 640;
		public static const STAGE_FPS : uint = 30;
		public static const STAGE_FRAME_TIME : Number = 1.0 / 24.0;
		
		public static const STAGE_LEFT : int = STAGE_WIDTH / -2;
		public static const STAGE_TOP : int = STAGE_HEIGHT / -2;
		public static const STAGE_RIGHT: int = STAGE_WIDTH / 2;
		public static const STAGE_BOTTOM : int = STAGE_HEIGHT / 2;
		
		public static const SAFE_MARGIN : int = 20;
		
		// Intervalo máximo de atualização permitido
		public static const MAX_UPDATE_INTERVAL_MILIS : int = 125;
		
		public static const URL_BIG_COLA : String = "http://186.202.60.210/";
		
		/**
		* Identificação da aplicação no NanoOnline 
		*/		
		public static const NO_APP_SHORT_NAME : String = "SHDB";
		
		public static const RANKING_TYPE_HIGHEST_SCORE : uint = 0;
		public static const RANKING_TYPE_BIGGEST_COMBO : uint = 1;
		public static const RANKING_TYPE_HIGHEST_LEVEL : uint = 2;
		public static const RANKING_TYPE_PERFECT_MOVES : uint = 3;
		public static const RANKING_TYPE_CUMULATIVE_SCORE : uint = 4;
		
		/**
		* Duração padrão da animação de esmaecimento da tela.
		*/
		public static const FADE_TIME_DEFAULT : Number = 1.1;
		
		public static const ICON_BLINK_TIME_DEFAULT : Number = 0.4;
		
		/**
		* Duração em segundos das transições de esconder / revelar a tela 
		*/		
		public static const TRANSITION_DUR : Number = 1.2;
		
		/**
		* Duração em segundos da animação de ir/voltar do estado de espera (loading)
		*/		
		public static const WAIT_ANIM_DUR : Number = 1.0;
		
		public static const COLOR_RED : uint = 0;
		public static const COLOR_YELLOW : uint = 1;
		public static const COLOR_BLACK : uint = 2;
		public static const COLOR_GREEN : uint = 3;
		
		public static const COLORS_SUFFIX : Array = [ "_RED", "_YELLOW", "_BLACK", "_GREEN" ];
		
		public static const PRECISION_MISSED	: uint = 0;
		public static const PRECISION_TERRIBLE	: uint = 1;
		public static const PRECISION_BAD 		: uint = 2;
		public static const PRECISION_GOOD		: uint = 3;
		public static const PRECISION_GREAT		: uint = 4;
		public static const PRECISION_PERFECT	: uint = 5;
		
		/** Precisão "dummy" usada para indicar ao jogador que o modo big está ativado. */
		public static const PRECISION_BIG	: uint = 6;
		
		public static const PRECISION_LEVEL_TOTAL : uint = 6;
		
		/** Distância dada de "bônus" para o jogador. */
		public static const DISTANCE_APROXIMATION : uint = 2;
		
		public static const MOVE_CONTINUOUS_HIT_AREA : uint = 30;
		
		public static const PLAYER_START_X : Number = 0;
		public static const PLAYER_START_Y : Number = 70;
		
		public static const BALL_START_X : Number = PLAYER_START_X - 56;
		public static const BALL_START_Y : Number = PLAYER_START_Y + 120;
		
		public static const SHADOW_Y : Number = 200;
		
		public static const TARGET_Y : Number = 56;
		
		public static const BOTTLE_Y : Number = STAGE_HEIGHT - 143;


		/**
		* Path dos recursos que não estão embarcados no swf
		*/        
		// OLD public static const RSC_GLOBAL_PATH : String = RSC_LOCATION == RSC_LOCATION_EXTERN ? "http://jad.nanogames.com.br/vvp/" : "../";
		public static function get RSC_GLOBAL_PATH() : String
		{
//			if( RSC_LOCATION == RSC_LOCATION_LOCAL )
//			{
				return "../";
//			}
//			else // if( RSC_LOCATION == RSC_LOCATION_EXTERN )
//			{
//				// OLD: Agora deixamos o arquivo html determinar a path dos recursos
//				//return "http://jad.nanogames.com.br/vvp/";
//				
//				// Quando o nosso SWF é a principal classe da aplicação, a linha abaixo retorna o parâmetro
//				// especificado no arquivo html em flashvars. No entanto, quando estamos rodando o SWF via
//				// código MXML, loderinfo será null. Para contornar o problema, criamos um setter e um getter
//				// "resourcePath" na classe principal. Utilizamos o método setter no código MXML para inicializar
//				// a propriedade
//				var aux : String = null;
//				try
//				{
//					aux = Application.GetInstance().root.loaderInfo.parameters[ "rsc_path" ];
//				}
//				catch( e :Error )
//				{
//				}
//				
//				if( aux == null )
//					aux = Application.GetInstance().resourcePath;                
//				
//				return aux;
//			}
		} 
		
		// Strings 
		public static const FONT_NAME_TXT_DIALOG : String 		= "Tahoma";
		public static const FONT_NAME_TOONTOWN : String 		= "Tahoma";
		public static const FONT_NAME_CARTOONERIE : String 		= "Tahoma";
		public static const FONT_NAME_GOOD_GIRL : String 		= "Tahoma";
		public static const FONT_NAME_ADDERVILLE : String 		= "Tahoma";
		
		public static const FONT_COLOR_UNSELECTED : uint = 0xb95c00;
		public static const FONT_COLOR_SELECTED : uint = 0x32b119;
		public static const FONT_COLOR_TEXT : uint = FONT_COLOR_UNSELECTED;
		public static const FONT_COLOR_QUEST_COMPLETED : uint = 0x3219b1;
		
		public static const FONT_TEXT_DIALOG_SIZE : uint = 22;
		
		public static const CURSOR_WIDTH : int = 33;
		public static const CURSOR_OFFSET_Y : int = 16;
		
		// Sons e músicas 		
		public static const SOUND_MUSIC_GAME : uint 			= 0;
//		public static const SOUND_BALL_MISS : uint 				= 1;
		public static const SOUND_MOVE_MISS : uint 				= 4;
		public static const SOUND_MOVE_HIT : uint 				= 5;
		public static const SOUND_BALL_HIT : uint			 	= 3;
		public static const SOUND_LEVEL_COMPLETE : uint		 	= 1;
		public static const SOUND_GAME_OVER : uint 				= 6;
		public static const SOUND_NEW_BOTTLE : uint 			= 2;
		public static const SOUND_BIG_MODE : uint 				= 7;
		
		public static const VOLUME_SOUND_MOVE_HIT : Number 		= 0.4;
		
		public static const SOUND_NAMES : Array = [ "principal", "passa_fase", "nova_garrafa", "impacto_bola", "erro", "acerto", "game_over" ];
		
		public static const SOUNDS_TOTAL : uint = SOUND_GAME_OVER + 1;
		
		
	}
}
