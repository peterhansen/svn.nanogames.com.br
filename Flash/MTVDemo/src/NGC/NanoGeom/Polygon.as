package NGC.NanoGeom
{
	import flash.display.Sprite;
	import flash.geom.Vector3D;
	import NGC.NanoMath.MathFuncs;
	
	/* Examples:
		drawPoly( 30,  6,  50, 50, 0 ); // Hexagon
		drawPoly( 30,  3, 150, 50, 0 ); // Triangle
		drawPoly( 30,  4, 250, 50, 0 ); // Square
		drawPoly( 30,  8, 350, 50, 0 ); // Octagon
		drawPoly( 30, 64, 450, 50, 0 ); // Almost a circle
	*/
	// http://www.flepstudio.org/forum/tutorials/990-drawing-polygons-actionscript-3-0-a.html
	
	public class Polygon extends Sprite
	{
		// PROPERTIES
		private var _points : Vector.< Vector3D >;
		
		// CONSTRUCTOR
		public function Polygon( radius : Number, segments : uint, center : Vector3D )
		{
			_points = new Vector.< Vector3D >();
			
			const ratio : Number = 360.0 / segments;
			const top : Number = center.y - radius;

			for( var i : uint = 0 ; i <= 360 ; i += ratio )
			{
				_points.push( new , Vector3D(	center.x + Math.sin( MathFuncs.degreesToRadians( i ) ) * radius,
												top + ( radius - Math.cos( MathFuncs.degreesToRadians( i ) ) * radius ),
												0.0 ) );
			}
		}
		
		// METHODS
		public function drawPolygon() : void
		{
			if( _points.length > 0 )
			{
				graphics.clear();
				graphics.beginFill( 0xFF0000, 1.0 );
				graphics.lineStyle( 1, 0x000000, 1.0 );
				graphics.moveTo( _points[ 0 ].x, _points[ 0 ].y );
	
				for( var i : uint = 1 ; i < _points.length ; ++i )
					graphics.lineTo( _points[ i ].x, _points[ i ].y );
				
				graphics.endFill();
			}
		}
	}
}
