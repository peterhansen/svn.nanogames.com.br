package NGC.ASWorkarounds
{
	import flash.utils.getQualifiedClassName;

	public class StaticClass
	{
		/**
		* Construtor
		* Classes estáticas não devem ser instanciadas. O construtor irá disparar uma exceção se chamado.
		*/		
		public function StaticClass()
		{
			var typeName : String = getQualifiedClassName( this );
			throw new Error( typeName + " is a static class and should not be instantiated" );
		}
	}
}