package NGC
{
	public final class NumberRef
	{
		/**
		* Número contido pela referência
		*/
		private var _value : Number;

		/**
		* Construtor
		*/
		public function NumberRef() 
		{
		}
		
		public function get content() : Number
		{
			return _value;
		}
		
		public function set content( n : Number ) : void
		{
			_value = n;	
		}
	}
}