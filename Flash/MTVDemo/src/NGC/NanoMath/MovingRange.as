package NGC.NanoMath
{
	/**
	 * 
	 **/
	public final class MovingRange
	{
		/* começo do intervalo */
		private var begin: Number;
		private var isBeginClosed: Boolean;
		
		/* fim do intervalo*/	
		private var end: Number;
		private var isEndClosed: Boolean;
		
		private var offset : Number;
		
		public function length() : Number {
			return end - begin;
		}
		
		/**
		 * 
		 * 
		 **/
		public function MovingRange( i0 : Number, i1: Number ) {
			begin = i0;
			end = i1;
			isBeginClosed = true;
			isEndClosed = true;
			offset = 0;
		}
		
		public function updateOffset( newOffset : Number ) : void {
			offset = newOffset;
		}
		
		public function getBegin() : Number {
			return begin + offset;
		}
		
		public function getEnd() : Number {
			return end + offset;
		}

		
		/**
		 * 
		 * 
		 **/
		public static function makeMovingRangeWithCloseness( i0 : Number, ib : Boolean, i1 : Number, ie : Boolean ) : MovingRange {
			
			var instance : MovingRange = new MovingRange( i0, i1 );
			
			instance.isBeginClosed = !ib;
			instance.isEndClosed = !ie;
			
			return instance;
		}
		
		/**
		 *
		 **/
		public function isInside( num : Number ) : Boolean {			
			return ( num > ( begin + offset ) && num < ( end + offset ) ) || ( num == ( begin + offset ) && isBeginClosed ) || ( num == ( end + offset ) && isEndClosed );
		}
		
		/**
		 * 
		 * 
		 **/
		public function isRangeInside( range : MovingRange ) : Boolean {
			return isInside( range.getBegin() ) && isInside( range.getEnd() );
		}
		
		/**
		 * 
		 * 
		 **/
		public function intersect( range : MovingRange ) : Boolean {
			return isInside( range.getBegin() ) || isInside( range.getEnd() ) || isRangeInside( range );
		}
	}
}