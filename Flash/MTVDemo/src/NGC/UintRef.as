package NGC
{
	public final class UintRef
	{
		/**
		* Número contido pela referência
		*/		
		private var _value : uint;
		
		/**
		* Construtor
		*/
		public function UintRef() 
		{
		}
		
		public function get content() : uint
		{
			return _value;
		}
		
		public function set content( n : uint ) : void
		{
			_value = n;	
		}
	}
}