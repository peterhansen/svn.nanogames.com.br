package NGC.NanoOnline
{
	import NGC.ASWorkarounds.Enum;
	import NGC.ByteArrayEx;
	import NGC.NanoOnline.Gender;
	import NGC.NanoOnline.NOGlobals;
	import NGC.ScheduledTask;
	import NGC.StringRef;
	import NGC.UintRef;
	import NGC.Utils;
	
	import de.polygonal.ds.HashMap;

	/**
	* Os métodos que enviam as requisições são estáticos pois foram herdados de C++. O objetivo desta solução é não precisarmos ter que armazenar
	* o objeto alvo da requisição em um objeto persistente ou globalmente para fazer as requisições. É possível criar um objeto localmente em
	* um étodo, enviar a requisição e ainda ter seus dados na callback de resposta: isto acontece pois uma cópia (em as3, uma referência) é
	* armazenada em um NORequestHolder. Se não o fizéssemos, em C++, o objeto local seria destruído ao fim de método, e a callback seria chamada
	* posteriormente sobre uma área de memória inválida.
	*/
	public final class NOCustomer extends Object implements ISerializable
	{
		// Parâmetros das requisições de cadastro e edição
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME : int 		= 0;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME : int 		= 1;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME : int 		= 2;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER : int 	= 3;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL : int 			= 4;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF : int 				= 5;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD : int 		= 6;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD_CONFIRM : int = 7;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END : int 		= 8;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENDER : int 			= 9;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY : int 		= 10;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID : int 		= 11;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP : int 		= 12;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_VALIDATE_BEFORE : int 	= 13;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_VALIDATED_AT : int 	= 14;
		
		// O Facebook pode retornar datas incompletas (contendo apenas dd/mm ) de acordo
		// com a política de privacidade escolhida pelo usuário. Assim, retornamos apenas
		// a data parcial e deixamos a aplicação cliente requisitar a informação que falta
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY_STR : int 	= 15;
		
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FB_UID : int 				= 25;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FB_SESSION_KEY : int 		= 26;
		private static const NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FB_SESSION_SECRET : int	= 27;
		
		// Parâmetros da requisição de login
		private static const NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME : int = 0;
		private static const NANO_ONLINE_CUSTOMER_PARAM_LOGIN_PASSWORD : int = 1;
		
		// Códigos de retorno relativos à requisição de registro do usuário
		private static const RC_ERROR_NICKNAME_IN_USE : int			= 1;
		private static const RC_ERROR_NICKNAME_FORMAT : int			= 2;
		private static const RC_ERROR_NICKNAME_LENGTH : int			= 3;
		private static const RC_ERROR_EMAIL_FORMAT : int			= 4;
		private static const RC_ERROR_EMAIL_LENGTH : int			= 5;
		private static const RC_ERROR_PASSWORD_WEAK : int			= 6;
		private static const RC_ERROR_PASSWORD_CONFIRMATION : int	= 7;
		private static const RC_ERROR_FIRST_NAME_LENGTH : int		= 8;
		private static const RC_ERROR_LAST_NAME_LENGTH : int		= 9;   
		private static const RC_ERROR_GENDER : int					= 10;
		private static const RC_ERROR_GENDER_LENGTH : int			= 11;
		private static const RC_ERROR_PHONE_NUMBER_FORMAT : int		= 12;
		private static const RC_ERROR_PHONE_NUMBER_LENGTH : int		= 13;
		private static const RC_ERROR_CPF_FORMAT : int				= 14;
		private static const RC_ERROR_EMAIL_ALREADY_IN_USE : int	= 15;				
		
		// Códigos de retorno relativos à requisição de login
		private static const RC_ERROR_LOGIN_NICKNAME_NOT_FOUND : int	= 1;
		private static const RC_ERROR_LOGIN_PASSWORD_INVALID : int		= 2;
		
		// Define os comprimentos mínimo e máximo dos campos string
		public static const CUSTOMER_PASSWORD_MIN_LEN : uint = 6;
		public static const CUSTOMER_PASSWORD_MAX_LEN : uint = 20;
		
		public static const CUSTOMER_EMAIL_MIN_LEN : uint = 6;
		public static const CUSTOMER_EMAIL_MAX_LEN : uint = 40;
		
		public static const CUSTOMER_NICKNAME_MIN_LEN : uint = 2;
		public static const CUSTOMER_NICKNAME_MAX_LEN : uint = 12;
		
		public static const CUSTOMER_FIRSTNAME_MIN_LEN : uint = 0;
		public static const CUSTOMER_FIRSTNAME_MAX_LEN : uint = 30;
		
		public static const CUSTOMER_LASTNAME_MIN_LEN : uint = 0;
		public static const CUSTOMER_LASTNAME_MAX_LEN : uint = 30;

		public static const CUSTOMER_PHONENUM_MIN_LEN : uint = 8;
		public static const CUSTOMER_PHONENUM_MAX_LEN : uint = 20;

		// Idades máxima e mínima permitidas pelo NanoOnline
		public static const NO_MAX_AGE : uint = 120; 	// Se alguém com mais de 120 anos estiver jogando, merece um prêmio
		public static const NO_MIN_AGE : uint = 2;		// 2 anos é pouco, mas os pais podem estar criando um perfil para o filho, o cachorro, ...
		
		// Id do usuário "dummy", ou seja, id utilizado quando ainda não há um usuário registrado
		// OBS: Mantemos o CUSTOMER_PROFILE_ID_NONE como -1 para manter a compatibilidade com o
		// código JAVA. Mas o mais correto, e menos propenso a erros, seria NOCustomerId ser
		// unsigned e CUSTOMER_PROFILE_ID_NONE ser igual a 0. Afinal, todo id negativo é
		// inválido, então estamos disperdiçando muitos ids...
		public static const CUSTOMER_PROFILE_ID_NONE : int = -1;

		// Id do usuário que está utilizando o NanoOnline
		private var _profileId : int;
		
		// Indica se devemos armazenar este usuário localmente
		private var _rememberMe : Boolean;
		
		// Indica se devemos armazenar a senha deste usuário
		private var _rememberPassword : Boolean;
		
		// Dados pessoais do usuário
		private var _gender : Gender;
		private var _encodedBirthday : Number;
		private var _timeStamp : Number;
		private var _email : String;
		private var _password : String;
		private var _nickname : String;
		private var _firstName : String;
		private var _lastName : String;
		private var _phoneNumber : String;
		private var _cpf : String;
		
		/**
		* Agenda a rotina de logout, simulando o delay da rede
		* O logout é apenas uma simulação para arrumar os dados da conxão. Não precisamos nos conectar ao NanoOnline
		* para fazê-lo
		*/		
		private var _logoutSim : ScheduledTask;
		
		// Construtor
		public function NOCustomer()
		{
			_profileId = NOCustomer.CUSTOMER_PROFILE_ID_NONE;
			_encodedBirthday = -1.0;
			_timeStamp = 0.0;
			
			_rememberMe = true;
			_rememberPassword = false;
			_gender = Gender.GENDER_N;
			_email = "";
			_password = "";
			_nickname = "";
			_firstName = "";
			_lastName = "";
			_phoneNumber  = "";
			_cpf  = "";
			
			_logoutSim = new ScheduledTask( 50, logoutResponse );
		}
		
		// Indica se algum usuário está registrado
		public function isRegistered() : Boolean
		{
			return _profileId > 0;
		}

		public function get profileId() : int
		{
			return _profileId;
		}
		
		
		public function set profileId( i : int ) : void {
			_profileId = i;
		}
		
		public function get rememberMe() : Boolean
		{
			return _rememberMe;
		}
		
		public function set rememberMe( b : Boolean ) : void
		{
			_rememberMe = b;
		}

		public function get rememberPassword() : Boolean
		{
			return _rememberPassword;
		}
		
		public function set rememberPassword( b : Boolean ) : void
		{
			_rememberPassword = b;
		}
		
		public function get gender() : Gender
		{
			return _gender;
		}
		
		public function set gender( g : Gender ) : void
		{
			_gender = g;
		}
		
		public function get phone() : String
		{
			return _phoneNumber.substr( 0, _phoneNumber.length );
		}
		
		public function set phone( customerPhoneNum : String ) : void
		{
			if( customerPhoneNum.length > 0 )
			{
				// Valida o tamanho da string
				var min : UintRef = new UintRef(), max : UintRef = new UintRef();
				GetPhoneNumSupportedLen( min, max );
				
				var outError : StringRef = new StringRef();
				var aux : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_PHONE_NUM );
				if( !CheckStrFieldLen( aux, customerPhoneNum.length, min.content, max.content, outError ) )
					throw new ArgumentError( outError );
				
				// Valida o conteúdo da string
				if( !IsValidPhoneNum( customerPhoneNum, outError ) )
					throw new ArgumentError( outError );
				
				_phoneNumber = customerPhoneNum.substr( 0, customerPhoneNum.length );
			}
			else
			{
				_phoneNumber = "";
			}
		}
		
		public function get cpf() : String
		{
			return _cpf.substr( 0, _cpf.length );
		}
		
		public function set cpf( str : String ) : void
		{
			// Deixamos a validação do cpf para o servidor
			_cpf = str.substr( 0, str.length );
		}

		/**
		* Retorna a data de nascimento do usuário 
		* @return A data de nascimento do usuário ou <code>null</code> caso esta não tenha sido determinada
		*/		
		public function getBirthday() : Date
		{
			if( _encodedBirthday == -1.0 )
				return null;

			var temp : Date = new Date();
			temp.setTime( _encodedBirthday );
			return temp;
		}
		
		/**
		* Determina a data de nascimento do usuário 
		* @param day O dia da data de nascimento. Deve estar no intervalo [1,31]
		* @param month O mês da data de nascimento. Deve estar no intervalo [1,12]
		* @param year O ano da data de nascimento
		* @param setInvalid Indica se devemos ignorar colocar um valor inválido para a data de nascimento. Quando este parâmetro
		* é <code>true</code>, os valores dos outros argumentos são ignorados
		*/		
		public function setBirthday( day : uint, month : uint, year : uint, setInvalid : Boolean = true ) : void
		{
			if( !setInvalid )
			{
				if( ( day == 0 ) || ( day > 31 ) || ( month == 0 ) || ( month > 12 ) )
					throw new ArgumentError( NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_BIRTHDAY ) );
				
				var outError : StringRef = new StringRef();
				if( !IsValidNOBirthday( day, month, year, outError ) )
					throw new ArgumentError( outError );
				
				_encodedBirthday = Utils.UTCDate( year, month, day ).getTime();
			}
			else
			{
				_encodedBirthday = -1;
			}
		}
		
		public function get email() : String
		{
			return _email.substr( 0, _email.length );
		}
		
		public function set email( customerEmail : String ) : void
		{
			// Valida o tamanho da string
			var min : UintRef = new UintRef(), max : UintRef = new UintRef();
			GetEmailSupportedLen( min, max );
			
			var outError : StringRef = new StringRef();
			var aux : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL );
			if( !CheckStrFieldLen( aux, customerEmail.length, min.content, max.content, outError ) )
				throw new ArgumentError( outError );
			
			// Valida o conteúdo da string
			if( !IsValidEmail( customerEmail, outError ) )
				throw new ArgumentError( outError );
			
			_email = customerEmail;
		}
		
		public function get password() : String
		{
			return _password.substr( 0, _password.length );
		}
		
		public function set password( customerPassword : String ) : void
		{
			// Valida o tamanho da string
			var min : UintRef = new UintRef(), max : UintRef = new UintRef();
			GetPasswordSupportedLen( min, max );
			
			var outError : StringRef = new StringRef();
			var aux : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_PASSWORD );
			if( !CheckStrFieldLen( aux, customerPassword.length, min.content, max.content, outError ) )
				throw new ArgumentError( outError );
			
			// Valida o conteúdo da string
			if( !IsValidPassword( customerPassword, outError ) )
				throw new ArgumentError( outError );
			
			_password = customerPassword;
		}
		
		public function get nickname() : String
		{
			return _nickname.substr( 0, _nickname.length );
		}
		
		public function set nickname( customerNickname : String ) : void
		{
			// Valida o tamanho da string
			var min : UintRef = new UintRef(), max : UintRef = new UintRef();
			GetNicknameSupportedLen( min, max );
			
			var outError : StringRef = new StringRef();
			var aux : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICKNAME );
			if( !CheckStrFieldLen( aux, customerNickname.length, min.content, max.content, outError ) )
				throw new ArgumentError( outError );
			
			_nickname = customerNickname;
		}
		
		public function get firstName() : String
		{
			return _firstName.substr( 0, _firstName.length );
		}
		
		public function set firstName( customerFirstName : String ) : void
		{
			// Valida o tamanho da string
			var min : UintRef = new UintRef(), max : UintRef = new UintRef();
			GetFirstNameSupportedLen( min, max );
			
			var outError : StringRef = new StringRef();
			var aux : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_FIRST_NAME );
			if( !CheckStrFieldLen( aux, customerFirstName.length, min.content, max.content, outError ) )
				throw new ArgumentError( outError );
			
			_firstName = customerFirstName;
		}
		
		public function get lastName() : String
		{
			return _lastName.substr( 0, _lastName.length );
		}	

		public function set lastName( customerLastName : String ) : void
		{
			// Valida o tamanho da string
			var min : UintRef = new UintRef(), max : UintRef = new UintRef();
			GetLastNameSupportedLen( min, max );
			
			var outError : StringRef = new StringRef();
			var aux : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_LAST_NAME );
			if( !CheckStrFieldLen( aux, customerLastName.length, min.content, max.content, outError ) )
				throw new ArgumentError( outError );
			
			_lastName = customerLastName;
		}

		/**
		* Serializa um objeto para em uma stream 
		* @param stream A stream para onde vamos serializar o objeto 
		*/
		public function serialize( stream : ByteArrayEx ) : void
		{
			// Escreve a versão do NanoOnline
			stream.writeUTF( NOGlobals.NANO_ONLINE_CLIENT_VERSION );

			stream.writeInt( _profileId );

			stream.writeUTF( _nickname );
			stream.writeUTF( _firstName );
			stream.writeUTF( _lastName );
			stream.writeUTF( _email );
			stream.writeUTF( _phoneNumber );
			stream.writeUTF( _cpf );
			
			// No disco não precisamos seguir o padrão do NanoOnline,
			// então podemos salvar o gênero como um byte
			stream.writeByte( _gender.Index );
			
			stream.writeUTCTime( _encodedBirthday );
			stream.writeUTCTime( _timeStamp );
			
			stream.writeBoolean( _rememberPassword );
			if( _rememberPassword )
			{
				stream.writeUTF( _password );
			}
		}
		
		/**
		* Desserializa um objeto a partir de uma stream 
		* @param stream A stream que contém o objeto serializado 
		*/ 
		public function unserialize( stream : ByteArrayEx ) : void
		{
			// Lê a versão do NanoOnline
			var version : String = stream.readUTF();
			
			_profileId = stream.readInt();
			
			nickname = stream.readUTF();
			firstName = stream.readUTF();
			lastName = stream.readUTF();
			email = stream.readUTF();
			phone = stream.readUTF();
			cpf = stream.readUTF();
			
			// No disco não precisamos seguir o padrão do NanoOnline,
			// então podemos salvar o gênero como um byte
			gender = ( Enum.FromIndex( Gender, int( stream.readChar().charCodeAt(0) ) ) as Gender );

			_encodedBirthday = stream.readUTCTime();
			_timeStamp = stream.readUTCTime();
			
			rememberPassword = stream.readBoolean();
			if( rememberPassword )
			{
				password = stream.readUTF();
			}
		}
		
		// Operadores
		public function equals( other : NOCustomer ) : Boolean
		{
			// Vários pontos do código do NanoOnline se baseiam nessa comparação feita apenas
			// por _profileId. Na verdade, esta comparação não é uma má escolha, pois um usuário
			// cadastrado neste aparelho pode voltar mais tarde e baixar as atualizações feitas
			// em seu perfil através de outro device. Neste caso, _profileId seria igual, mas todos
			// os outros campos poderiam conter valores diferentes, só que o usuário continua
			// sendo o mesmo
			return _profileId == other._profileId;
		}

		// Tratam o login do usuário no NanoOnline
		private function login( stream : ByteArrayEx ) : Boolean
		{
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME );
			stream.writeUTF( _nickname );
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_PASSWORD );
			stream.writeUTF( password );
			
			return true;
		}

		private function loginResponse( table : HashMap, errorStr : StringRef ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					{
						// O login é a única ação na qual o ID do usuário vem fora de NANO_ONLINE_ID_SPECIFIC_DATA
						if( !table.containsKey( NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID ) )
						{
							errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_RESPONSE );
							return false;
						}
						else
						{
							// Armazena o _profileId do usuário
							_profileId = table.find( NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID );
							
							// Determina que este usuário está utilizando a conexão
							NOConnection.GetInstance().loggedCustomerId = _profileId;
						}
					}
					return true;
				
				case RC_ERROR_LOGIN_PASSWORD_INVALID:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_WRONG_PASSWORD );
					return false;
				
				case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_NICKNAME );
					return false;
				
				default:
					trace( ">>> Unrecognized param return code sent to loginResponse in response to login request" );
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
		}
		
		/**
		* Trata a resposta de uma requisição de logout 
		* O logout é apenas uma simulação para arrumar os dados da conxão. Não precisamos nos conectar ao NanoOnline para fazê-lo
		*/		
		private function logoutResponse() : void
		{
			var conn : NOConnection = NOConnection.GetInstance();
			var listener : INOListener = conn.noListener;
			
			conn.clean();
			conn.loggedCustomerId = CUSTOMER_PROFILE_ID_NONE;
			
			listener.onNOSuccessfulResponse();
		}

		// Tratam o cadastro de perfis no NanoOnline
		private function createProfile( stream : ByteArrayEx ) : Boolean
		{
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME );
			stream.writeUTF( _nickname );
			
			if( _firstName.length > 0 )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME );
				stream.writeUTF( _firstName );
			}
			
			if( _lastName.length > 0 )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME );
				stream.writeUTF( _lastName );
			}
			
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL );
			stream.writeUTF( _email );
			
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD );
			stream.writeUTF( _password );
			
			if( _gender != Gender.GENDER_N )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENDER );
				stream.writeChar( _gender == Gender.GENDER_M ? "m" : "f" );
			}
			
			if( _encodedBirthday != -1 )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY );
				stream.writeUTCTime( _encodedBirthday );
			}
			
			if( _phoneNumber.length > 0 )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER );
				stream.writeUTF( _phoneNumber );
			}
			
			if( _cpf.length > 0 )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF );
				stream.writeUTF( _cpf );
			}
			
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END );
			
			return true;
		}

		private function createProfileResponse( table : HashMap, errorStr : StringRef ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					{
						var dataStream : ByteArrayEx = table.find( NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA );
						return readCreateProfileData( dataStream, errorStr );
					}
					return true;
				
				case RC_ERROR_NICKNAME_IN_USE:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICK_IN_USE );
					return false;
				
				case RC_ERROR_NICKNAME_FORMAT:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICK_BAD_FORMAT );
					return false;
				
				case RC_ERROR_NICKNAME_LENGTH:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_NICK_BAD_LENGTH );
					return false;
				
				case RC_ERROR_EMAIL_FORMAT:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_BAD_FORMAT );
					return false;
				
				case RC_ERROR_EMAIL_LENGTH:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_BAD_LENGTH );
					return false;
				
				case RC_ERROR_PASSWORD_WEAK:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_WEAK_PASSWORD );
					return false;
				
				case RC_ERROR_PASSWORD_CONFIRMATION:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PASSWORD_CONFIRM );
					return false;
				
				case RC_ERROR_FIRST_NAME_LENGTH:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_FIRST_NAME_LEN );
					return false;
				
				case RC_ERROR_LAST_NAME_LENGTH: 
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_LAST_NAME_LEN );
					break;
				
				case RC_ERROR_GENDER:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_GENDER );
					return false;
				
				case RC_ERROR_GENDER_LENGTH:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_GENDER_LEN );
					return false;
				
				case RC_ERROR_PHONE_NUMBER_FORMAT:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PHONE_NUM_FORMAT );
					return false;
					
				case RC_ERROR_PHONE_NUMBER_LENGTH:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PHONE_NUM_LEN );
					return false;
					
				case RC_ERROR_CPF_FORMAT:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_CPF_FORMAT );
					return false;
					
				case RC_ERROR_EMAIL_ALREADY_IN_USE:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_EMAIL_ALREADY_IN_USE );
					break;
				
				default:
					trace( ">>> Unrecognized param return code sent to createProfileResponse in response to createProfile request" );
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
			return false;
		}

		private function readCreateProfileData( dataStream : ByteArrayEx, errorStr : StringRef ) : Boolean
		{
			while( dataStream.bytesAvailable > 0 )
			{
				switch( dataStream.readByte() )
				{
					case NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID:
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID:
						_profileId = dataStream.readInt();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP:
						_timeStamp = dataStream.readUTCTime();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END:
						return true;
					
					// Não sabe ler o campo, então ignora
					default:
						trace( ">>> NOCustomer::readCreateProfileData => Unknown parameter" );
						break;
				}
			}
			return true;
		}

		// Atualiza os dados do perfil do usuário
		private function updateProfile( stream : ByteArrayEx ) : Boolean
		{
			// Tentar atualizar um perfil que não possui um ID é um erro de lógica. A interface
			// do NanoOnline não deveria permitir que isso acontecesse...
			if( _profileId == CUSTOMER_PROFILE_ID_NONE )
				return false;
			
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID );
			stream.writeInt( _profileId );
			
			return createProfile( stream );
		}

		// Tratam o download de perfis do NanoOnline
		private function download( stream : ByteArrayEx ) : Boolean
		{
			if( login( stream ) )
			{
				stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP );
				stream.writeUTCTime( _timeStamp );
				return true;
			}
			return false;
		}

		private function downloadResponse( table : HashMap, errorStr : StringRef ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					{
						var dataStream : ByteArrayEx = table.find( NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA );
						return readDownloadData( dataStream, errorStr );
					}
					break; // Nunca alcançará este break
				
				case RC_ERROR_LOGIN_PASSWORD_INVALID:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_WRONG_PASSWORD );
					return false;
				
				case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_NICKNAME );
					return false;
				
				default:
					trace( ">>> Unrecognized param return code sent to downloadResponse in response to download request" );
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
		}

		private function readDownloadData( dataStream : ByteArrayEx, errorStr : StringRef ) : Boolean
		{
			while( dataStream.bytesAvailable > 0 )
			{
				switch( dataStream.readByte() )
				{
					case NOGlobals.NANO_ONLINE_ID_CUSTOMER_ID:
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CUSTOMER_ID:
						_profileId = dataStream.readInt();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_NICKNAME:
						_nickname = dataStream.readUTF();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_FIRST_NAME:
						_firstName = dataStream.readUTF();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_LAST_NAME:
						_lastName = dataStream.readUTF();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL:
						_email = dataStream.readUTF();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_GENDER:
						{
							var c : String = dataStream.readChar();
							if( ( c.charAt() == "m" ) || ( c.charAt() == "M" ) )
								_gender = Gender.GENDER_M;
							else if( ( c.charAt() == "f" ) || ( c.charAt() == "F" ) )
								_gender = Gender.GENDER_F;
							else
								_gender = Gender.GENDER_N;
						}	
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_BIRTHDAY:
						_encodedBirthday = dataStream.readUTCTime();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_TIMESTAMP:
						_timeStamp = dataStream.readUTCTime();
						break;
					
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PHONE_NUMBER:
						_phoneNumber = dataStream.readUTF();
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_CPF:
						_cpf = dataStream.readUTF();
						break;
					
					// Ignora esses campos
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_PASSWORD_CONFIRM:
						{
							var dummy : String = dataStream.readUTF();
						}
						break;
					
					case NANO_ONLINE_CUSTOMER_PARAM_REGISTER_INFO_END:
						return true;
					
					// Não sabe ler o campo, então ignora
					default:
						trace( ">>> NOCustomer::readDownloadData => Unknown parameter" );
						break;
				}
			}
			return true;
		}
		
		private function sendRememberPassEmail( stream : ByteArrayEx ) : Boolean
		{
			stream.writeByte( NANO_ONLINE_CUSTOMER_PARAM_LOGIN_NICKNAME );

			if( _nickname.length > 0 )
				stream.writeUTF( _nickname );
			else if( _email.length > 0 )
				stream.writeUTF( _email );
			else
				return false;

			return true;
		}
		
		private function sendRememberPassEmailResponse( table : HashMap, errorStr : StringRef ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					{
						var dataStream : ByteArrayEx = table.find( NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA );
						if( ( dataStream == null ) || ( dataStream.bytesAvailable == 0 ) || ( dataStream.readByte() != NANO_ONLINE_CUSTOMER_PARAM_REGISTER_EMAIL ) )
						{
							errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_RESPONSE );
							return false;
						}
						
						// Lê o endereço para qual foi enviado o email com a lembrança da senha
						_email = dataStream.readUTF();
					}
					break; // Nunca alcançará este break

				case RC_ERROR_LOGIN_NICKNAME_NOT_FOUND:
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_USER );
					return false;
					
				default:
					trace( ">>> Unrecognized param return code sent to downloadResponse in response to download request" );
					errorStr.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
			return true;
		}
		
		/**
		* Envia uma requisição de criação de usuário para o NanoOnline 
		* @param customer O objeto que contém os dados do novo usuário
		* @param listener O objeto que receberá os eventos de resposta do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário
		*/		
		public static function SendCreateRequest( customer : NOCustomer, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/customers/add", new NORequestHolder( customer, customer.createProfile, customer.createProfileResponse ) );
		}
		
		/**
		* Envia uma requisição de edição de usuário para o NanoOnline 
		* @param customer O objeto que contém os dados editados do usuário
		* @param listener O objeto que receberá os eventos de resposta do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário
		*/
		public static function SendEditRequest( customer : NOCustomer, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/customers/refresh", new NORequestHolder( customer, customer.updateProfile, customer.createProfileResponse ) );
		}
		
		/**
		* Envia uma requisição de download de usuário para o NanoOnline 
		* @param customer O objeto que contém a identificação do usuário do qual desejamos obter os dados
		* @param listener O objeto que receberá os eventos de resposta do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário
		*/
		public static function SendDownloadRequest( customer : NOCustomer, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/customers/import", new NORequestHolder( customer, customer.download, customer.downloadResponse ) );
		}
		
		/**
		* Envia uma requisição de login de usuário para o NanoOnline 
		* @param customer O objeto que contém os dados do usuário que deseja se logar ao NanoOnline
		* @param listener O objeto que receberá os eventos de resposta do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário
		*/
		public static function SendLoginRequest( customer : NOCustomer, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/customers/login", new NORequestHolder( customer, customer.login, customer.loginResponse ) );
		}
		
		/**
		* Envia uma requisição de logout de usuário para o NanoOnline 
		* @param customer O objeto que contém os dados do usuário que deseja fazer logout
		* @param listener O objeto que receberá os eventos de resposta do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário
		*/
		public static function SendLogoutRequest( customer : NOCustomer, listener : INOListener ) : Boolean
		{
			// O logout é apenas uma simulação para arrumar os dados da conxão. Não precisamos nos conectar ao NanoOnline
			// para fazê-lo
			NOConnection.GetInstance().noListener = listener;
			customer._logoutSim.call();
			return true;
		}
		
		/**
		* Envia uma requisição de lembrança de senha para o NanoOnline 
		* @param customer O objeto que contém o email e/ou o nickname do usuário para quem vamos mandar o email com o novo password 
		* @param listener O objeto que receberá os eventos de resposta do NanoOnline
		* @return <code>true</code> se conseguiu criar e enviar a requisição ou <code>false</code> em caso contrário
		*/
		public static function SendRememberPasswordRequest( customer : NOCustomer, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/customers/remember_password", new NORequestHolder( customer, customer.sendRememberPassEmail, customer.sendRememberPassEmailResponse ) );
		}
		
		/**
		* Retorna as quantidades mínima e máxima de caracteres que o campo de texto pode ter
		* @param min Retorna o tamanho mínimo que o campo de texto deve ter
		* @param max Retorna o tamanho máximo que o campo de texto pode ter 
		*/		
		public static function GetNicknameSupportedLen( min : UintRef, max : UintRef ) : void
		{
			min.content = CUSTOMER_NICKNAME_MIN_LEN;
			max.content = CUSTOMER_NICKNAME_MAX_LEN;
		}
		
		/**
		* Retorna as quantidades mínima e máxima de caracteres que o campo de texto pode ter
		* @param min Retorna o tamanho mínimo que o campo de texto deve ter
		* @param max Retorna o tamanho máximo que o campo de texto pode ter 
		*/
		public static function GetFirstNameSupportedLen( min : UintRef, max : UintRef ) : void
		{
			min.content = CUSTOMER_FIRSTNAME_MIN_LEN;
			max.content = CUSTOMER_FIRSTNAME_MAX_LEN;
		}
		
		/**
		* Retorna as quantidades mínima e máxima de caracteres que o campo de texto pode ter
		* @param min Retorna o tamanho mínimo que o campo de texto deve ter
		* @param max Retorna o tamanho máximo que o campo de texto pode ter 
		*/
		public static function GetLastNameSupportedLen( min : UintRef, max : UintRef ) : void
		{
			min.content = CUSTOMER_LASTNAME_MIN_LEN;
			max.content = CUSTOMER_LASTNAME_MAX_LEN;
		}
		
		/**
		* Retorna as quantidades mínima e máxima de caracteres que o campo de texto pode ter
		* @param min Retorna o tamanho mínimo que o campo de texto deve ter
		* @param max Retorna o tamanho máximo que o campo de texto pode ter 
		*/
		public static function GetPasswordSupportedLen( min : UintRef, max : UintRef ) : void
		{
			min.content = CUSTOMER_PASSWORD_MIN_LEN;
			max.content = CUSTOMER_PASSWORD_MAX_LEN;
		}
		
		/**
		* Retorna as quantidades mínima e máxima de caracteres que o campo de texto pode ter
		* @param min Retorna o tamanho mínimo que o campo de texto deve ter
		* @param max Retorna o tamanho máximo que o campo de texto pode ter 
		*/
		public static function GetEmailSupportedLen( min : UintRef, max : UintRef ) : void
		{
			min.content = CUSTOMER_EMAIL_MIN_LEN;
			max.content = CUSTOMER_EMAIL_MAX_LEN;
		}
		
		/**
		* Retorna as quantidades mínima e máxima de caracteres que o campo de texto pode ter
		* @param min Retorna o tamanho mínimo que o campo de texto deve ter
		* @param max Retorna o tamanho máximo que o campo de texto pode ter 
		*/
		public static function GetPhoneNumSupportedLen( min : UintRef, max : UintRef ) : void
		{
			min.content = CUSTOMER_PHONENUM_MIN_LEN;
			max.content = CUSTOMER_PHONENUM_MAX_LEN;
		}

		/**
		* Retorna se o caractere é um caractere especial válido de emails
		* @param c O caractere que deve ser testado
		* @return <code>true</code> se o parâmetro é um caractere especial válido de emails ou <code>false</code> caso contrário 
		*/		
		private static function IsEmailSpecialChar( c : String ) : Boolean
		{
			var specialChars : String = "!#$%&'*+-/=?ˆ_{}|~";
			return Utils.FindFirstOf( c, specialChars ) == -1 ? false : true;
		}

		// Indica se a parte do _email fornecido é válida. Caso não o seja, retorna o erro encontrado
		// em outError
		private static function IsValidEmailPart( emailPart : String, outError : StringRef, needDots : Boolean ) : Boolean
		{
			if( emailPart.charAt( 0 ) == '.' )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_BEGIN_W_DOT );
				return false;
			}
			else if( emailPart.charAt( emailPart.length - 1 ) == '.' )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_END_W_DOT );
				return false;
			}
			
			var char : String;
			var hasDots : Boolean = false;
			var lastChar : String = "";
			
			for( var i : uint = 0 ; i < emailPart.length ; ++i )
			{
				char = emailPart.charAt(i);
				if( char == '.' )
				{
					hasDots = true;
					
					if( lastChar == '.' )
					{
						outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_DOUBLE_DOT );
						return false;
					}
				}
				else if( ( Utils.IsDigit( char ) == 0 ) && ( Utils.IsAlpha( char ) == 0 ) && ( IsEmailSpecialChar( char ) == 0 ) )
				{
					var str1 : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_CHAR_PART1 );
					var str2 : String = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_CHAR_PART2 );
					
					outError.content = str1 + char + str2;
					return false;
				}
				
				lastChar = char;
			}
			
			if( needDots && !hasDots )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_NO_DOTS );
				return false;
			}
			
			return true;
		}

		// Indica se o _email fornecido é válido. Caso não o seja, retorna o erro encontrado em outError
		private static function IsValidEmail( _email : String, outError : StringRef ) : Boolean
		{
			// Valida a localização da '@'
			var emailLen : int = _email.length;
			var i : int = Utils.FindFirstOf( _email, "@" );
			if( i == 0 )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_BEGIN_W_AT_SYMBOL );
				return false;
			}
			else if( i == emailLen - 1 )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_END_W_AT_SYMBOL );
				return false;
			}
			else if( i == -1 )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_NO_AT_SYMBOL );
				return false;
			}
			else
			{
				var aux : int = Utils.FindFirstOf( _email, "@", i+1 );
				if( aux != -1 )
				{
					outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_EMAIL_MORETHAN1_AT_SYMBOL );
					return false;
				}
			}
			
			// Valida a primeira parte do _email
			var firstPart : String = _email.substr( 0, i );
			if( !IsValidEmailPart( firstPart, outError, false ) )
				return false;
			
			// Valida a segunda parte do _email
			var secondPart : String = _email.substr( i + 1 );
			return IsValidEmailPart( secondPart, outError, true );
		}

		// Indica se a senha fornecida é válida. Caso não o seja, retorna o erro encontrado em outError
		private static function IsValidPassword( password : String, outError : StringRef ) : Boolean
		{
			if( ( Utils.FindFirstOf( password, "0123456789", 0 ) == -1 ) || ( Utils.FindFirstOf( password, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", 0 ) == -1 ) )
			{
				outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PASSWORD );
				return false;
			}
			
			return true;
		}
		
		// Verifica se a data passada é uma data válida para representar o aniversário de
		// um usuário do NanoOnline
		private static function IsValidNOBirthday( day : uint, month : uint, year : uint, outError : StringRef ) : Boolean
		{
			var today : Date = new Date();
			var currYear : uint = today.getUTCFullYear();

			var minYear : uint = currYear - NO_MAX_AGE;
			var maxYear : uint = currYear - NO_MIN_AGE;
			
			var temp : String = "";
			if( ( year < minYear ) || ( year > maxYear ) )
			{
				temp = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_YEARS_VALID_INTERVAL );
				outError.content = temp + minYear + ", " + maxYear + "]";
				
				return false;
			}
			
			var aux : String;
			if( month == 2 )
			{
				var maxFebDays : uint = Utils.IsLeapYear( year ) ? 29 : 28;
				
				if( day > maxFebDays )
				{
					aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_FEBRUARY_OF_YEAR );
					temp += aux + year;
					
					aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_HAS_ONLY );
					temp += aux + maxFebDays;
					
					aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_DAYS );
					temp += aux;
					
					outError.content = temp;
					
					return false;
				}
			}
			else
			{
				var maxDay : uint;
				if( ( month == 4 ) || ( month == 6 ) || ( month == 9 ) || ( month == 11 ) )
					maxDay = 30;
				else
					maxDay = 31;
				
				if( day > maxDay )
				{
					aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_MONTH );
					temp += aux + month;
					
					aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_HAS_ONLY );
					temp += aux + maxDay;
					
					aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_DAYS );
					temp += aux;
					
					outError.content = temp;
					
					return false;
				}
			}
			return true;
		}

		// Verifica se o comprimento do campo string é válido
		private static function CheckStrFieldLen( fieldName : String, len : uint, min : uint, max : uint, outError : StringRef ) : Boolean
		{
			var aux : String;
			var temp : String = "";
			if( len < min )
			{				
				aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_FIELD );
				temp += aux + " (" + fieldName + ")";
				
				aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_MUST_BE_AT_LEAST );
				temp += aux + min;
				
				aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_CHARS_LONG );
				temp += aux;
				
				outError.content = temp;

				return false;
			}
			
			if( len > max )
			{	
				aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_INVALID_FIELD );
				temp = aux + fieldName;
				
				aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_MUST_HAVE );
				temp += aux + min;
				
				aux = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_CHARS_MAXIMUM );
				temp += aux;
				
				outError.content = temp;

				return false;
			}
			
			return true;
		}
		
		/**
		* Retorna se o número de telefone fornecido é válido. Caso não o seja, retorna o erro encontrado em outError 
		* @param phoneNum O número de telefone que deve ser validado
		* @param outError O erro encontrado no número de telefone, caso haja
		* @return <code>true</code> se o número de telefone for válido ou <code>false</code> caso contrário
		*/		
		private static function IsValidPhoneNum( phoneNum : String, outError : StringRef ) : Boolean
		{
			// Verifica se possuímos apenas caracteres válidos
			if( Utils.FindFirstNotOf( phoneNum, "+() -0123456789" ) == -1 )
				return true;
			
			outError.content = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_BAD_PHONE_NUM_FORMAT );
			
			return false;
		}
		
		public function toString() : String
		{
			var d : Date = getBirthday();
			return new String( "[NOCustomer]\n- ID: " + _profileId
							   + "\n- Nickname: " + _nickname 
							   + "\n- First Name: " + _firstName 
							   + "\n- Last Name: " + _lastName 
							   + "\n- Gender: " + _gender 
							   + "\n- Email: " + _email 
							   + "\n- Birthday: " + ( d == null ? "<not filled>" : d.toUTCString() ) 
							   + "\n- Phone: " + _phoneNumber 
							   + "\n- CPF: " + _cpf );
		}
	}
}