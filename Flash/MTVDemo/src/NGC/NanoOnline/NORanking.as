package NGC.NanoOnline
{
	import NGC.ByteArrayEx;
	import NGC.NanoOnline.NORankingEntry;
	import NGC.Utils;
	
	import de.polygonal.ds.HashMap;

	/**
	* Os métodos que enviam as requisições são estáticos pois foram herdados de C++. O objetivo desta solução é não precisarmos ter que armazenar
	* o objeto alvo da requisição em um objeto persistente ou globalmente para fazer as requisições. É possível criar um objeto localmente em
	* um étodo, enviar a requisição e ainda ter seus dados na callback de resposta: isto acontece pois uma cópia (em as3, uma referência) é
	* armazenada em um NORequestHolder. Se não o fizéssemos, em C++, o objeto local seria destruído ao fim de método, e a callback seria chamada
	* posteriormente sobre uma área de memória inválida.
	*/
	public final class NORanking extends Object implements ISerializable
	{
		// Parâmetros utilizados quando estamos submetendo um ranking
		private static const NANO_ONLINE_RANKING_PARAM_N_ENTRIES : int	= 0;
		private static const NANO_ONLINE_RANKING_PARAM_SUB_TYPE : int	= 1;
		
		// Parâmetros utilizados quando estamos pedindo para o NanoOnline as melhores pontuações de um ranking
		private static const NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES : int	= 2;
		private static const NANO_ONLINE_RANKING_PARAM_END : int				= 3;
		private static const NANO_ONLINE_RANKING_PARAM_LIMIT : int				= 4;
		private static const NANO_ONLINE_RANKING_PARAM_OFFSET : int				= 5;
		
		// Melhor pontuação que pode ser atingida neste ranking
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		private var _maxPoints : int;
		
		// Tipo do ranking
		private var _rnkType : int;
		
		// Indica se o ranking está na forma crescente ou decrescente
		private var _decrescent : Boolean;
		
		// Número de entradas do Ranking Online que iremos exibir neste ranking
		private var _onlineMaxEntries : int;
		
		// Perfis existentes neste device
		private var _highlightedProfilesIds : Vector.< int >;
		
		// Pontuações dos rankings local e online
		private var _localEntries : Vector.< NORankingEntry >;
		private var _onlineEntries : Vector.< NORankingEntry >;
		
		// Construtor
		public function NORanking( type : int, rnkMaxPoints : int = int.MAX_VALUE, isDecrescent : Boolean = true, onlineRnkMaxEntries : uint = 100 )
		{
			_highlightedProfilesIds = new Vector.< int >;
			_localEntries = new Vector.< NORankingEntry >;
			_onlineEntries = new Vector.< NORankingEntry >;
			
			_rnkType = type;
			maxPoints = rnkMaxPoints;
			decrescent = isDecrescent;
			onlineMaxEntries = onlineRnkMaxEntries;
		}

		// Cópia
		public function clone() : NORanking
		{
			var copy : NORanking = new NORanking( _rnkType, maxPoints, decrescent, onlineMaxEntries );
			
			return copy;
		}
		
		// Determina se o ranking está na forma crescente ou decrescente
		public function set decrescent( b : Boolean ) : void
		{
			_decrescent = b;
		}

		// Indica se o ranking está na forma crescente ou decrescente
		public function get decrescent() : Boolean
		{
			return _decrescent;
		}
		
		// Retornam as entradas deste ranking
		public function get localEntries() : Vector.< NORankingEntry >
		{
			return _localEntries.slice( 0, _localEntries.length );
		}
		
		public function set localEntries( entries : Vector.< NORankingEntry > ) : void
		{
			_localEntries = entries.slice( 0, entries.length );
		}

		public function get onlineEntries() : Vector.< NORankingEntry >
		{
			return _onlineEntries.slice( 0, _onlineEntries.length );
		}
		
		// Troca as entradas online de dois rankings
		public function swapOnlineEntries( other : NORanking ) : void
		{
			var temp : Vector.< NORankingEntry > = _onlineEntries.slice( 0, _onlineEntries.length );
			_onlineEntries = other._onlineEntries.slice( 0, other._onlineEntries.length );
			other._onlineEntries = temp;
		}
		
		// Limpa todas as entradas (locais e online) de um ranking
		public function clear() : void
		{
			// Apaga todas as entradas dos vetores
			_localEntries.splice( 0, _localEntries.length );
			_onlineEntries.splice( 0, _onlineEntries.length );
			_highlightedProfilesIds.splice( 0, _highlightedProfilesIds.length );
		}
		
		// Retorna as 3 melhores pontuações existentes no NanoOnline para este ranking
		public function get bestTrio() : Vector.< NORankingEntry >
		{
			const out : Vector.< NORankingEntry > = new Vector.< NORankingEntry >();

			// Logo após o lançamento de um jogo, é possível que ainda não tenhamos 3 recordes...
			const nOnlineEntries : uint = _onlineEntries.length
			const max : uint = nOnlineEntries < 3 ? nOnlineEntries : 3;
			
			var i : uint;
			for( i = 0 ; i < max ; ++i )
				out.push( _onlineEntries[ i ] );

			for( i = max ; i < 3 ; ++i )
				out.push( new NORankingEntry() );
			
			return out;
		}
		
		// Determina a melhor pontuação que pode ser feita neste ranking
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function set maxPoints(  max : int ) : void
		{
			_maxPoints = max;
		}
		
		// Retorna a melhor pontuação que pode ser feita neste ranking
		// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
		// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
		public function get maxPoints() : int
		{
			return _maxPoints;
		}
		
		// Determina quantas entradas do Ranking Online queremos exibir neste ranking
		public function set onlineMaxEntries( nEntries : int ) : void
		{
			_onlineMaxEntries = nEntries;
		}
		
		// Retorna quantas entradas do Ranking Online iremos exibir neste ranking
		public function get onlineMaxEntries() : int
		{
			return _onlineMaxEntries;
		}
		
		// Determina os perfis que ranking deve focar, ou seja, aqueles que terão
		// suas pontuações exibidas mesmo se estiverem fora dos "OnlineMaxEntries"
		// 1os do ranking
		public function setHighlightedProfilesIds( profilesIds : Vector.< int > ) : void
		{
			_highlightedProfilesIds = profilesIds;
		}
		
		// Retorna quais perfis o ranking está focando, ou seja, aqueles que terão
		// suas pontuações exibidas mesmo se estiverem fora dos "OnlineMaxEntries"
		// 1os do ranking
		public function getHighlightedProfilesIds() : Vector.< int >
		{
			return _highlightedProfilesIds.slice( 0, _highlightedProfilesIds.length );
		}
		
		// Ordena as entradas do ranking de acordo com a configuração do mesmo
		// A comparison function should take two arguments to compare. Given the elements A and B, the result of compareFunction
		// can have a negative, 0, or positive value: 
		// A negative return value specifies that A appears before B in the sorted sequence. 
		// A return value of 0 specifies that A and B have the same sort order. 
		// A positive return value specifies that A appears after B in the sorted sequence. 
		public function sort() : void
		{
			if( decrescent )
			{
				_localEntries.sort( rnkSortDesc );
				_onlineEntries.sort( rnkSortDesc );
			}
			else
			{
				_localEntries.sort( rnkSortAsc );
				_onlineEntries.sort( rnkSortAsc );
			}
		}

		private static function rnkSortDesc( x : NORankingEntry, y : NORankingEntry ) : Number
		{
			return y.score - x.score;
		}
		
		private static function rnkSortAsc( x : NORankingEntry, y : NORankingEntry ) : Number
		{
			return x.score - y.score;
		}
		
		// Lê o objeto de uma stream
		public function serialize( stream : ByteArrayEx ) : void
		{
			// Escreve a versão do NanoOnline
			stream.writeUTF( NOGlobals.NANO_ONLINE_CLIENT_VERSION );

			// Escreve o número de entradas do ranking local
			const nLocalEntries : uint = _localEntries.length;
			stream.writeByte( nLocalEntries );
			
			// Escreve as entradas do ranking local
			var i : uint;
			for( i = 0 ; i < nLocalEntries ; ++i )
				_localEntries[i].serialize( stream );
			
			// Escreve o número de entradas do ranking online
			const nOnlineEntries : uint = _onlineEntries.length;
			stream.writeByte( nOnlineEntries );
			
			// Escreve as entradas do ranking online
			for( i = 0 ; i < nOnlineEntries ; ++i )
				_onlineEntries[i].serialize( stream );
		}
		
		// Escre o objeto em uma stream 
		public function unserialize( stream : ByteArrayEx ) : void
		{
			// Lê a versão do NanoOnline
			const version : String = stream.readUTF();
			
			// Lê o número de entradas do ranking local
			var nEntries : uint = stream.readByte();
			
			// Lê as entradas do ranking local
			var i : uint;
			var temp : NORankingEntry;
			for( i = 0 ; i < nEntries ; ++i )
			{
				temp = new NORankingEntry();
				temp.unserialize( stream );
				_localEntries.push( temp );
			}
			
			// Lê o número de entradas do ranking online
			nEntries = stream.readByte();
			
			// Lê as entradas do ranking online
			for( i = 0 ; i < nEntries ; ++i )
			{
				temp = new NORankingEntry();
				temp.unserialize( stream );
				_onlineEntries.push( temp );
			}
		}

		// Requisições
		public static function SendSubmitRequest( ranking : NORanking, listener : INOListener ) : Boolean {
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/ranking_entries/submit", new NORequestHolder( ranking, ranking.submitScores, ranking.submitScoresResponse ) );
		}
		
		public static function SendHighScorestRequest( ranking : NORanking, listener : INOListener ) : Boolean
		{
			NOConnection.GetInstance().noListener = listener;
			return NOConnection.GetInstance().sendRequest( "/ranking_entries/submit", new NORequestHolder( ranking, ranking.highScores, ranking.submitScoresResponse ) );
		}
		
		// Pede as melhores pontuações do rankingOnline
		private function highScores( stream : ByteArrayEx ) : Boolean
		{
			// Número de entradas que estamos enviando. Apenas para manter o protocolo
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_N_ENTRIES );
			stream.writeShort( 0 );
			
			// Tipo do ranking. Caso seja um ranking crescente, envia um valor negativo para indicá-lo
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_SUB_TYPE );
			stream.writeByte( decrescent ? _rnkType : -_rnkType );
			
			// Indica de quais perfis desejamos saber as pontuações
			if( _highlightedProfilesIds.length > 0 )
			{
				stream.writeByte( NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES );
				
				var nProfiles : int = _highlightedProfilesIds.length;
				stream.writeShort( nProfiles );
				
				while( --nProfiles >= 0 )
					stream.writeInt( _highlightedProfilesIds[ nProfiles ] );
			}
			
			// Número de entradas que queremos receber de volta
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_LIMIT );
			stream.writeShort( _onlineMaxEntries );
			
			// Queremos sempre os "onlineMaxEntries" 1os, então indicamos um offset nulo
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_OFFSET );
			stream.writeInt( 0 );
			
			// Fim da requisição
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_END );
			
			return true;
		}
		
		// Envia a pontuação do jogador para o NanoOnline
		private function submitScores( stream : ByteArrayEx ) : Boolean
		{
			// Número de entradas que estamos enviando
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_N_ENTRIES );
			const nLocalEntries : uint = _localEntries.length;
			stream.writeShort( nLocalEntries );
			
			// Tipo do ranking. Caso seja um ranking crescente, envia um valor negativo para indicá-lo
			stream.writeByte( NANO_ONLINE_RANKING_PARAM_SUB_TYPE );
			stream.writeByte( _decrescent ? _rnkType : -_rnkType );

			trace( "NORanking::submitScores >>> Pontuações a serem enviadas:\n" );

			for( var i : uint = 0 ; i < nLocalEntries ; ++i )
			{
				stream.writeInt( _localEntries[i].profileId );
				stream.writeLongCheated( _localEntries[i].score );
				stream.writeUTCTime( _localEntries[i].timeStamp );

				trace( "NORanking::submitScores >>> Jogador " + _localEntries[i].profileId + ", Pontuação " + _localEntries[i].score + " feita em " + Utils.DecodeDate( _localEntries[i].timeStamp ) );
			}

			trace( "\n" );
			
			return true;
		}
		
		// Trata a resposta do envio da pontuação do jogador para o NanoOnline
		private function submitScoresResponse( table : HashMap, errorStr : String ) : Boolean
		{
			switch( table.find( NOGlobals.NANO_ONLINE_ID_RETURN_CODE ) )
			{
				case NOGlobals.NANO_ONLINE_RC_OK:
					{
						// Lê o ranking online enviado como resposta
						const dataStream : ByteArrayEx = table.find( NOGlobals.NANO_ONLINE_ID_SPECIFIC_DATA );						
						return readSubmitResponseData( dataStream, errorStr );
					}
					return true;
				
				default:
					trace( "Unrecognized param return code sent to createProfileResponse in response to createProfile request" );
					errorStr = NOGlobals.GetNOText( NOTextsIndexes.NO_TXT_UNKNOWN_ERROR );
					return false;
			}
			
			return false;
		}
		
		// Lê as entradas do ranking enviadas pelo NanoOnline
		private function readSubmitResponseData( dataStream : ByteArrayEx, errorStr : String ) : Boolean
		{
			// Limpa o vetor
			_onlineEntries.splice( 0, _onlineEntries.length );
			
			while( dataStream.bytesAvailable > 0 )
			{
				var id : int = dataStream.readByte();

				trace( ">>> NORanking::readSubmitResponseData => Read attrib id = " + id + "\n" );
				
				if( id == NANO_ONLINE_RANKING_PARAM_END )
					break;
				
				switch( id )
				{
					case NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES:
					case NANO_ONLINE_RANKING_PARAM_N_ENTRIES:
						{
							var totalEntries : int = dataStream.readShort();
							
							if( totalEntries > 0 )
							{
								trace( "\n\nNORanking::readSubmitResponseData\n\n" );
								
								for( var i : uint = 0 ; i < totalEntries ; ++i )
								{
									var tempId : int = dataStream.readInt();
									
									// TODO: Deveria ser um int64, mas flash não oferece suporte nato a inteiros de 64 bits. A classe BigInteger também não é tão fácil de utilizar,
									// o que dificulta nossa vida. Como o VilaVirtual não possui pontuações maiores do que um uint32, vamos tratar as pontuações como sendo desse tipo
									var tempScore : int = dataStream.readLongCheated();
									
									var tempNick : String = dataStream.readUTF();
	
									trace( "User " + tempNick + " (id = " + tempId + ") => " + tempScore + " points\n" );
									
									var temp : NORankingEntry = new NORankingEntry( tempId, tempScore, tempNick, id == NANO_ONLINE_RANKING_PARAM_N_OTHER_PROFILES ? dataStream.readInt() : i );
									_onlineEntries.push( temp );
								}
	
								trace( "\n" );
							}
						}
						break;
					
					// Não sabe ler o campo, então ignora
					default:
						trace( ">>> NORanking::readSubmitResponseData => Unknown parameter" );
						break;
				}
			}
			
			// Ordena os recordes
			sort();
			
			return true;
		}
	}
}