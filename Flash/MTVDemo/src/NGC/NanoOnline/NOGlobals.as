package NGC.NanoOnline
{
	import NGC.ASWorkarounds.StaticClass;
	
	public final class NOGlobals extends StaticClass
	{
		/**
		 * Endereço raiz do Nano Online
		 */		
		//#if DEBUG
		//public static const NANO_ONLINE_URL : String = "http://localhost:3000";
		//#else
		// TODO 
		public static const NANO_ONLINE_URL : String = "http://online.nanogames.com.br";
		//#endif
		//public static const NANO_ONLINE_URL : String = "http://staging.nanogames.com.br";

		//#if DEBUG
		/**
		* Id de aplicativo dummy (usado para testes e debug)
		*/		
		public static const NANO_ONLINE_ID_APP_DUMMY : String = "DMMY";
		//#endif
 
		/**
		* Versão do cliente Nano Online
		*/		
		public static const NANO_ONLINE_CLIENT_VERSION : String = "0.0.3";
		
		/**
		* Textos do NanoOnline
		* TODO: Assim é feio!!! Deveria estar num arquivo de recursos separado...
		*/		
		private static var _texts : Array = [ "Formato inválido de resposta do servidor",
											  "Aparelho não suportado",
											  "Esta aplicação não é mais suportada",
											  "Erro interno do servidor",
											  "Email inválido: O '.' não pode ser o primeiro caractere de uma das partes de um email",
											  "Email inválido: Não pode terminar em '.'",
											  "Email inválido: Dois '.' consecutivos",
											  "Email inválido: O caractere ",
											  " não é válido para emails",
											  "Email inválido: Não há um caractere '.' depois da '@'",
											  "Email inválido: O primeiro caractere não pode ser '@'",
											  "Email inválido: Não contém '@'",
											  "Senha inválida: Sua senha deve conter letras e números",
											  "Meses devem estar no intervalo [1, 12]",
											  "Anos devem estar no intervalo [",
											  "\"Dia\" deve conter um valor maior do que zero",
											  "Fevereiro do ano ",
											  " possui apenas ",
											  " dias",
											  "Mês ",
											  "Campo inválido",
											  ": Deve possuir, pelo menos, ",
											  " caracteres",
											  ": Deve possuir ",
											  " caracteres no máximo",
											  "Apelido",
											  "Nome",
											  "Sobrenome",
											  "Email",
											  "Senha",
											  "Erro desconhecido",
											  "Este apelido já está sendo utilizado",
											  "Formato de apelido inválido",
											  "Tamanho de apelido inválido",
											  "Formato de email inválido",
											  "Tamanho de email inválido",
											  "Senha fraca. Deve possuir ao menos 6 caracteres e conter letras e números",
											  "Confirmação de senha inválida",
											  "Tamanho de nome inválido",
											  "Tamanho de sobrenome inválido",
											  "Gênero inválido",
											  "Tamanho de gênero inválido",
											  "Erro",
											  "Ocorreu um erro",
											  "Clique aqui para saber mais",
											  "Ajuda",
											  "Oi, bem-vindo ao Nano Online. Este é um serviço grátis que lhe permite criar um perfil único para utilizar em todos os nossos jogos, armazenar suas melhores pontuações e interagir com outros jogadores ao redor do mundo!\n\nVocê pode verificar os rankings dos jogos diretamente do seu telefone ou através de nosso web site.\n\nSe este é o seu primeiro acesso ao sistema, o primeiro passo que você deve tomar é criar um perfil. Você deverá preencher informações simples, como o seu apelido, sexo, nome e data de nascimento. O seu email é necessário para que possamos enviar uma nova senha caso você perca a antiga.\n\nSe você já criou um perfil através de nosso web site ou em um de nossos jogos, basta selecionar a opção \"baixar perfil\" e digitar seu apelido e sua senha.\n\nSUBMETENDO PONTUAÇÕES\n\nA melhor pontuação de cada perfil é armazenada no telefone. Quando você acessa a opção \"Submeter Recordes\", haverá uma lista com tais pontuações. Através dela você poderá escolher os recordes que deseja submeter para o ranking online. A partir deste momento, todos poderão ver sua pontuação através do NanoOnline, no telefone ou em nosso web site!\n\nCUSTOS\n\nA Nano Games não cobra pelo serviço. Dependendo de sua operadora e tipo de conexão (Wi-Fi ou rede da operadora), você poderá ser cobrado pela transferência de dados. Contate a sua operadora para saber mais sobre os custos aplicados.\n\nGeralmente, os custos dependem da quantidade de dados transferida, e não do tempo de conexão.\n\nVisando oferecer um serviço mais rápido e barato, todas as ações, como criar um perfil, submeter recordes e verificar o ranking online, transferem apenas pequenas quantidades de dados.\n\nCriar um perfil, por exemplo, transfere em torno de 250 bytes, o que significa que seria necessário criar 4 perfis para transferir 1 kilobyte, ou 4.000 perfis para atingir 1 megabyte! Enviar suas pontuações para o ranking online geralmente não transfere mais do que 200 bytes.\n\nLogo, se sua operadora cobra US$ 10,00 por megabyte, significa que você pagará US$ 0,01 (1 centavo de dólar) a cada 5 submissões! Não é tão caro, certo? Se você possuir dúvidas, acesse www.nanogames.com.br para saber mais.",
											  "Perfil",
											  "Login",
											  "Download",
											  "Sim",
											  "Não",
											  "Não foi possível criar a requisição",
											  "Não foi possível criar o perfil",
											  "Login realizado com sucesso",
											  "Perfil baixado",
											  "Ok",
											  "Criar",
											  "Editar",
											  "M",
											  "F",
											  "Sincronizando com o servidor",
											  "Alerta",
											  "Desculpe-nos, não foi possível sincronizar com o servidor",
											  "A senha e sua confirmação devem ser iguais",
											  "Perfil criado com sucesso",
											  "Perfil editado",
											  "Perfil sincronizado",
											  "Admin. de Perfis",
											  "Não foi possível deletar o perfil",
											  "Não há perfis salvos. Você deve criar ou fazer o download de um perfil",
											  "Perfis corrompidos. Você deve criar ou fazer o download de um perfil",
											  "Não foi possível carregar a lista de perfis",
											  "Logout",
											  "Ranking",
											  "Marcar Todos",
											  "Desmarcar Todos",
											  "Submeter",
											  "Local",
											  "Global",
											  "Baixando o Ranking Online",
											  "Não foi possível enviar a requisição para o servidor",
											  "Não há um ranking online para este jogo",
											  "Você tem certeza de que deseja deletar estes recordes?",
											  "Nenhum recorde selecionado. Você deve selecionar as caixas de marcação dos recordes que você deseja deletar antes de clicar no botão 'deletar'",
											  "Nenhum recorde selecionado. Você deve selecionar as caixas de marcação dos recordes que você deseja submeter antes de clicar no botão 'submeter'",
											  "Recordes submetidos",
											  "Desculpe-nos, erro de download. Por favor, tente novamente mais tarde",
											  "Erro Inesperado",
											  "Não há recordes disponíveis para a submissão",
											  "Você não possui mais recordes para submeter",
											  "Fez Logout",
											  "Este botão apenas deleta o perfil do aparelho. Este perfil continuará existindo no servidor. Deseja deletar este perfil?",
											  "OBS: Este perfil está logado atualmente. Você fará logout automaticamente",
											  "Senha incorreta",
											  "Apelido inválido",
											  "Sair",
											  "Selecionar",
											  "Submeter Recordes",
											  "Ranking Online",
											  "Lembrar-me",
											  "Lembrar Senha",
											  "Confirmação de Senha",
											  "Aniversário (dd/mm/aaaa)",
											  "Sexo",
											  "Por Favor Aguarde",
											  "Deletar",
											  "Você precisa informar sua data de nascimento",
											  "O número de telefone contém caracteres inválidos",
											  "O número de telefone contém uma quantidade inválida de caracteres",
											  "O número de CPF informado não é válido",
											  "Este email já está cadastrado para outro usuário",
											  "Telefone",
											  "Não foi possível realizar login",
											  "O campo apelido é obrigatório",
											  "O campo email é obrigatório",
											  "O campo senha é obrigatório",
											  "Email inválido: Não pode terminar em '@'",
											  "Email inválido: Mais de uma '@'",
											  "Data de aniversário inválida",
											  "Não foi possível enviar o email",
											  "Um email com sua nova senha foi enviado. Verifique sua caixa de entrada.",
											  "Usuário não encontrado. Certifique-se de que os dados estão corretos",
											  "Não foi possível submeter o recorde",
											  "Não foi possível salvar o jogo",
											  "Já existe um jogo salvo neste slot",
											  "Índice de slot inválido",
											  "Não foi possível carregar o jogo",
											  "Não existe um jogo salvo neste slot",
											  "Jogo salvo",
											  "Deseja tentar novamente?",
											  "Você deve fazer login para continuar um jogo salvo anteriormente"
											];
		
		// Os ids de chaves globais são negativos, para evitar possíveis confusões com ids específicos de
		// um serviço (registro de usuário, ranking online, multiplayer, propagandas, etc.). Este devem
		// utilizar ids positivos
		
		public static const NANO_ONLINE_ID_APP					: int = -128;		// Id de chave global: aplicativo/jogo. Tipo do valor: String
		public static const NANO_ONLINE_ID_APP_VERSION			: int = -127;		// Id de chave global: versão do aplicativo/jogo. Tipo do valor: string
		public static const NANO_ONLINE_ID_LANGUAGE				: int = -126;		// Id de chave global: idioma atual do aplicativo. Tipo do valor: byte
		public static const NANO_ONLINE_ID_NANO_ONLINE_VERSION	: int = -125;		// Id de chave global: versão do Nano Online utilizada no aplicativo/jogo. Tipo do valor: string
		public static const NANO_ONLINE_ID_RETURN_CODE			: int = -124;		// Id de chave global: código de retorno. Tipo do valor: short
		public static const NANO_ONLINE_ID_CUSTOMER_ID			: int = -123;		// Id de chave global: usuário. Tipo do valor: int
		public static const NANO_ONLINE_ID_ERROR_MESSAGE		: int = -122;		// Id de chave global: mensagem de erro. Tipo do valor: string
		public static const NANO_ONLINE_ID_SPECIFIC_DATA		: int = -121;		// Id de chave global: início de dados específicos. Tipo do valor: byte[] (tamanho variável)
		public static const NANO_ONLINE_ID_FEEDER_DATA			: int = -120;		// Id de chave global: dados do servidor de notícias
		public static const NANO_ONLINE_ID_AD_SERVER_DATA		: int = -119;		// Id de chave global: dados do servidor de anúncios
		public static const NANO_ONLINE_ID_CLIENT_LOCAL_TIME	: int = -118;		// Id de chave global: horário local do cliente
		
		public static const NANO_ONLINE_ID_MACOS_RAILS_BUG_FIX	: int = -100;		// Id de chave global: Byte extra enviado nas requisições POST feitas via MacOS e iPhoneOS. Sem fazer isso, o Rails ignora o último byte do corpo de uma mensagem quando seu valor é 0, causando bugs estranhos
		
		// Códigos de erro. Assim como as chaves, os valores de retorno globais possuem valores negativos para
		// que não haja confusão com os valores utilizados internamente em cada serviço
		public static const NANO_ONLINE_RC_OK						: int =  0;
		public static const NANO_ONLINE_RC_SERVER_INTERNAL_ERROR	: int = -1;
		public static const NANO_ONLINE_RC_DEVICE_NOT_SUPPORTED		: int = -2;
		public static const NANO_ONLINE_RC_APP_NOT_FOUND			: int = -3;
		
		/**
		* Função auxiliar para obter textos ligados ao NanoOnline
		* @param textIndex O índice do texto desejado (ver NOTextsIndexes)
		* @return O texto desejado no idioma atual
		*/		
		public static function GetNOText( textIndex : uint ) : String
		{
			var txt : String = ( _texts[ textIndex ] as String );
			return txt.substr( 0, txt.length );
		}

		/**
		* Construtor : Vai disparar uma exceção se for chamado, pois classes estáticas não devem ser instanciadas 
		*/		
		public function NOGlobals()
		{
			super();
		}
	}
}