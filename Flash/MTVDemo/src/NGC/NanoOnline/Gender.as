package NGC.NanoOnline
{
	import NGC.ASWorkarounds.Enum;

	public final class Gender extends Enum
	{
		// "Construtor" estático
		{ initEnum( Gender ); }
		
		// Possíveis opções de sexo (por mais incrível que pareça, na Austrália existe um caso de sexo 'netro'...)
		public static const GENDER_N : Gender = new Gender();
		public static const GENDER_M : Gender = new Gender();
		public static const GENDER_F : Gender = new Gender();
	}
}