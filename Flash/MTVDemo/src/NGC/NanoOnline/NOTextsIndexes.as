package NGC.NanoOnline
{
	import NGC.ASWorkarounds.StaticClass;
	
	public final class NOTextsIndexes extends StaticClass
	{
		/**
		* Construtor: Vai disparar uma exceção se for chamado, pois classes estáticas não devem ser instanciadas 
		*/		
		public function NOTextsIndexes()
		{
			super();
		}
		
		// Índices dos textos utilizados no NanoOnline. v -> utilizados nesta versão, x -> não utilizados nesta
		// versão (eram utilizados na versão a partir de qual fizemos este código)
		public static const NO_TXT_INVALID_RESPONSE : uint				= 0; //v
		public static const NO_TXT_DEVICE_UNSUPPORTED : uint			= 1; //v
		public static const NO_TXT_APP_UNSUPPORTED : uint				= 2; //v
		public static const NO_TXT_SERVER_INTERNAL_ERROR : uint			= 3; //v
		public static const NO_TXT_BAD_EMAIL_BEGIN_W_DOT : uint			= 4; //v
		public static const NO_TXT_BAD_EMAIL_END_W_DOT : uint			= 5; //v
		public static const NO_TXT_BAD_EMAIL_DOUBLE_DOT : uint			= 6; //v
		public static const NO_TXT_BAD_EMAIL_CHAR_PART1 : uint			= 7; //v
		public static const NO_TXT_BAD_EMAIL_CHAR_PART2 : uint			= 8; //v
		public static const NO_TXT_BAD_EMAIL_NO_DOTS : uint				= 9; //v
		public static const NO_TXT_BAD_EMAIL_BEGIN_W_AT_SYMBOL : uint	= 10; //v
		public static const NO_TXT_BAD_EMAIL_NO_AT_SYMBOL : uint		= 11; //v
		public static const NO_TXT_BAD_PASSWORD : uint					= 12; //v
		public static const NO_TXT_MONTHS_VALID_INTERVAL : uint			= 13; //x
		public static const NO_TXT_YEARS_VALID_INTERVAL : uint			= 14; //v
		public static const NO_TXT_DAYS_LESS_OR_EQUAL_ZERO : uint		= 15; //x
		public static const NO_TXT_FEBRUARY_OF_YEAR : uint				= 16; //v
		public static const NO_TXT_HAS_ONLY : uint						= 17; //v
		public static const NO_TXT_DAYS : uint							= 18; //v
		public static const NO_TXT_MONTH : uint							= 19; //v
		public static const NO_TXT_INVALID_FIELD : uint					= 20; //v
		public static const NO_TXT_MUST_BE_AT_LEAST : uint				= 21; //v
		public static const NO_TXT_CHARS_LONG : uint					= 22; //v
		public static const NO_TXT_MUST_HAVE : uint						= 23; //v
		public static const NO_TXT_CHARS_MAXIMUM : uint					= 24; //v
		public static const NO_TXT_NICKNAME : uint						= 25; //v
		public static const NO_TXT_FIRST_NAME : uint					= 26; //v
		public static const NO_TXT_LAST_NAME : uint						= 27; //v
		public static const NO_TXT_EMAIL : uint							= 28; //v
		public static const NO_TXT_PASSWORD : uint						= 29; //v
		public static const NO_TXT_UNKNOWN_ERROR : uint					= 30; //v
		public static const NO_TXT_NICK_IN_USE : uint					= 31; //v
		public static const NO_TXT_NICK_BAD_FORMAT : uint				= 32; //v
		public static const NO_TXT_NICK_BAD_LENGTH : uint				= 33; //v
		public static const NO_TXT_EMAIL_BAD_FORMAT : uint				= 34; //v
		public static const NO_TXT_EMAIL_BAD_LENGTH : uint				= 35; //v
		public static const NO_TXT_WEAK_PASSWORD : uint					= 36; //v
		public static const NO_TXT_BAD_PASSWORD_CONFIRM : uint			= 37; //v
		public static const NO_TXT_BAD_FIRST_NAME_LEN : uint			= 38; //v
		public static const NO_TXT_BAD_LAST_NAME_LEN : uint				= 39; //v
		public static const NO_TXT_BAD_GENDER : uint					= 40; //v
		public static const NO_TXT_BAD_GENDER_LEN : uint				= 41; //v
		public static const NO_TXT_ERROR : uint							= 42; //x
		public static const NO_TXT_ERROR_OCCURED : uint					= 43; //x
		public static const NO_TXT_CLICK_FOR_INFO : uint				= 44; //x
		public static const NO_TXT_HELP : uint							= 45; //x
		public static const NO_TXT_NANO_ONLINE_HELP : uint				= 46; //x
		public static const NO_TXT_PROFILE : uint						= 47; //x
		public static const NO_TXT_LOGIN : uint							= 48; //x
		public static const NO_TXT_DOWNLOAD : uint						= 49; //x
		public static const NO_TXT_YES : uint							= 50; //v
		public static const NO_TXT_NO : uint							= 51; //v
		public static const NO_TXT_COULDNT_CREATE_REQUEST : uint		= 52; //v
		public static const NO_TXT_COULDNT_CREATE_PROFILE : uint		= 53; //v
		public static const NO_TXT_LOGGED_IN : uint						= 54; //v
		public static const NO_TXT_PROFILE_DOWNLOADED : uint			= 55; //x
		public static const NO_TXT_OK : uint							= 56; //v
		public static const NO_TXT_CREATE : uint						= 57; //x
		public static const NO_TXT_EDIT : uint							= 58; //x
		public static const NO_TXT_GENDER_M : uint						= 59; //x
		public static const NO_TXT_GENDER_F : uint						= 60; //x
		public static const NO_TXT_SYNCH_W_SERVER : uint				= 61; //x
		public static const NO_TXT_ALERT : uint							= 62; //x
		public static const NO_TXT_COULDNT_SYNCH : uint					= 63; //x
		public static const NO_TXT_PASSWORD_DIFFS_FROM_CONFIRM : uint	= 64; //x
		public static const NO_TXT_PROFILE_CREATED : uint				= 65; //v
		public static const NO_TXT_PROFILE_EDITTED : uint				= 66; //x
		public static const NO_TXT_PROFILE_SYNCHED : uint				= 67; //x
		public static const NO_TXT_PROFILE_MANAGER : uint				= 68; //x
		public static const NO_TXT_COULDNT_DELETE_PROFILE : uint		= 69; //x
		public static const NO_TXT_NO_PROFILES_SAVED : uint				= 70; //x
		public static const NO_TXT_PROFILES_CORRUPTED : uint			= 71; //x
		public static const NO_TXT_COULDNT_LOAD_PROFILES_LIST : uint	= 72; //x
		public static const NO_TXT_LOGOUT : uint						= 73; //x
		public static const NO_TXT_RANKING : uint						= 74; //x
		public static const NO_TXT_CHECK_ALL : uint						= 75; //x
		public static const NO_TXT_UNCHECK_ALL : uint					= 76; //x
		public static const NO_TXT_SUBMIT : uint						= 77; //x
		public static const NO_TXT_LOCAL : uint							= 78; //x
		public static const NO_TXT_GLOBAL : uint						= 79; //x
		public static const NO_TXT_DOWNLOAD_ONLINE_RANKING : uint		= 80; //x
		public static const NO_TXT_COULDNT_SEND_REQUEST_TO_SERVER : uint= 81; //x
		public static const NO_TXT_NO_ONLINE_RANKING : uint				= 82; //x
		public static const NO_TXT_SURE_DELETE_RECORDS : uint			= 83; //x
		public static const NO_TXT_SHOULD_CHECK_TO_DELETE : uint		= 84; //x
		public static const NO_TXT_SHOULD_CHECK_TO_SUBMIT : uint		= 85; //x
		public static const NO_TXT_RECORDS_SUBMITTED : uint				= 86; //x
		public static const NO_TXT_DOWNLOAD_ERROR : uint				= 87; //x
		public static const NO_TXT_UNEXPECTED_ERROR : uint				= 88; //x
		public static const NO_TXT_NO_RECORDS_AVAILABLE : uint			= 89; //x
		public static const NO_TXT_NO_MORE_RECORDS_TO_SUBMIT : uint		= 90; //x
		public static const NO_TXT_LOGGED_OUT : uint					= 91; //x
		public static const NO_TXT_DELETES_FROM_DEVICE_ONLY : uint		= 92; //x
		public static const NO_TXT_PS_DELETING_LOGGED_PROFILE : uint	= 93; //x
		public static const NO_TXT_WRONG_PASSWORD : uint				= 94; //v
		public static const NO_TXT_BAD_NICKNAME : uint					= 95; //v
		public static const NO_TXT_MAIN_MENU_EXIT : uint				= 96; //x
		public static const NO_TXT_PROFILE_ACTION_MENU_SELECT : uint	= 97; //x
		public static const NO_TXT_LOCAL_RECORDS : uint					= 98; //x
		public static const NO_TXT_ONLINE_RANKING : uint				= 99; //x
		public static const NO_TXT_REMEMBER_ME : uint					= 100; //x
		public static const NO_TXT_REMEMBER_PASSWORD : uint				= 101; //x
		public static const NO_TXT_PASSWORD_CONFIRM : uint				= 102; //x
		public static const NO_TXT_BIRTHDAY : uint						= 103; //v
		public static const NO_TXT_GENDER : uint						= 104; //v
		public static const NO_TXT_PLEASE_WAIT : uint					= 105; //x
		public static const NO_TXT_DELETE : uint						= 106; //x
		public static const NO_TXT_BIRTHDAY_MANDATORY : uint			= 107; //v
		public static const NO_TXT_BAD_PHONE_NUM_FORMAT : uint			= 108; //v
		public static const NO_TXT_BAD_PHONE_NUM_LEN : uint				= 109; //v
		public static const NO_TXT_BAD_CPF_FORMAT : uint				= 110; //v
		public static const NO_TXT_EMAIL_ALREADY_IN_USE : uint			= 111; //v
		public static const NO_TXT_PHONE_NUM : uint						= 112; //v
		public static const NO_TXT_COULDNT_LOGIN : uint					= 113; //v
		public static const NO_TXT_NICKNAME_MANDATORY : uint			= 114; //v
		public static const NO_TXT_EMAIL_MANDATORY : uint				= 115; //v
		public static const NO_TXT_PASSWORD_MANDATORY : uint			= 116; //v
		public static const NO_TXT_BAD_EMAIL_END_W_AT_SYMBOL : uint		= 117; //v
		public static const NO_TXT_BAD_EMAIL_MORETHAN1_AT_SYMBOL : uint = 118; //v
		public static const NO_TXT_INVALID_BIRTHDAY : uint				= 119; //v
		public static const NO_TXT_COULDNT_SEND_EMAIL : uint			= 120; //v
		public static const NO_TXT_EMAIL_SENT : uint					= 121; //v
		public static const NO_TXT_BAD_USER : uint						= 122; //v
		public static const NO_TXT_COULDNT_SUBMIT_RECORD : uint			= 123; //v
		public static const NO_TXT_COULDNT_SAVE : uint					= 124; //v
		public static const NO_TXT_SAVE_NEEDS_OVERWRITING : uint		= 125; //v 
		public static const NO_TXT_INVALID_SLOT : uint					= 126; //v
		public static const NO_TXT_COULDNT_LOAD : uint					= 127; //v
		public static const NO_TXT_NO_GAME_TO_LOAD : uint				= 128; //v
		public static const NO_TXT_GAME_SAVED : uint					= 129; //v
		public static const NO_TXT_RETRY : uint							= 130; //v
		public static const NO_TXT_MUST_LOGIN_TO_SAVE : uint			= 131; //v
	}
};
