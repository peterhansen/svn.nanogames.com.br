package NGC
{
	public final class StringRef
	{
		/**
		* String contida pela referência
		*/		
		private var _value : String;
		
		/**
		* Construtor
		*/
		public function StringRef( str : String = null ) 
		{
			content = str;
		}
		
		public function get content() : String
		{
			if( _value == null )
				return null;
			return _value.substr( 0, _value.length );
		}
		
		public function set content( str : String ) : void
		{
			if( str != null ) 
				_value = str.substr( 0, str.length );
			else
				_value = null;
		}
		
		public function toString() : String
		{
			return content;
		}
	}
}