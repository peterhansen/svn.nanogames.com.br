/**
 * ArrayUtil
 * 
 * A class that allows to compare two ByteArrays.
 * Copyright (c) 2007 Henri Torgemane
 * 
 * See LICENSE.txt for full license information.
 */
package NGC.Crypto.CryptoUtil
{
	import flash.utils.ByteArray;	
	
	public class ArrayUtil
	{	
		public static function equals( a1 : ByteArray , a2 : ByteArray ) : Boolean
		{
			var l : int = a1.length;
			if( l != a2.length )
				return false;

			for( var i : int = 0 ; i < l ; ++i )
			{
				if( a1[i] != a2[i] )
					return false;
			}
			return true;
		}
	}
}