package GUILib
{
	/**
	 * GUIConstants
	 * Algumas constantes uteis 
	 * @author Daniel Monteiro
	 * 
	 */
	public class GUIConstants
	{
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * 1 milisegundo
		 * (santa frescurada...)
		 */
		static public const MILISECOND:Number=1;
		/**
		 * 1 milisegundos = 1 milisegundo
		 */
		static public const MILISECONDS:Number=MILISECOND;
		/**
		 * 1 segundo = 1000 milisegundos
		 */
		static public const SECOND:Number=1000*MILISECONDS;
		/**
		 * 1 segundos = 1 segundo
		 */
		static public const SECONDS:Number=SECOND;		
		/**
		 * intervalo entre passos de fade 20 ms
		 */
		static public const FADE_INTERVAL:Number=20*MILISECONDS;
		/**
		 * quantidade de frames desejada (não sei mais se ainda é usado)
		 */
		static public const FADE_FRAMES:int=16;
		/**
		 * espaçamento horizontal padrão (deve mudar a cada projeto)
		 */
		static public const DEFAULTTEXTHORIZONTALSPACING:int=13;
		/**
		 * espaçamento vertical padrão (deve mudar a cada projeto) 
		 */
		static public const DEFAULTVERTICALSPACING:int=5;		
		///----------------------------------------------------------------------------------------------------------		
	}
}