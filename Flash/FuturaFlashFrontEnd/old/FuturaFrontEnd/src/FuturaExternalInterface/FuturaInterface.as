package FuturaExternalInterface
{
	//flash
	import flash.events.*;
	import flash.external.ExternalInterface;
	
	/**
	 * FuturaInterface
	 * comunicação com o servidor do Futura CDF
	 * @author Yuri Mello
	 * @comment modificado por Daniel Monteiro
	 * */
	public class FuturaInterface extends FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * callback para quando usuário é removido
		 */
		private var removeUserCallback:Function;
		/**
		 * callback para resposta à reposta do usuário
		 */
		private var newQuestionCallback:Function;
		/**
		 * callback para obter a lista de jogadores
		 */
		private var userListCallback:Function;
		/**
		 * callback para identificar o jogador local
		 */
		private var setLocalIdCallback:Function
		private var gameEndedCallback:Function;
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * FuturaInterface
		 * */
		public function FuturaInterface(removeUserFunc:Function, takeUserListFunc:Function, setMyIDFunc:Function, newQuestionFunc:Function, gameEndedFunc:Function)
		{
			super(removeUserFunc,takeUserListFunc,setMyIDFunc,newQuestionFunc,gameEndedFunc);
			///checagem dos parametros
			
			if (ExternalInterface==null || removeUserFunc==null || takeUserListFunc==null || setMyIDFunc==null || newQuestionFunc==null )
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///preenche os callbacks
			gameEndedCallback=gameEndedFunc;
			userListCallback=takeUserListFunc;
			setLocalIdCallback=setMyIDFunc;
			removeUserCallback = removeUserFunc;
			newQuestionCallback=newQuestionFunc;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * call
		 * @param cmd comando a ser executado pelo servidor
		 * 
		 */
		override public function call(cmd:String):Object
		{
			if (ExternalInterface == null || cmd == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);	
			
			return ExternalInterface.call(cmd);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		override public function init():void
		{
			if (ExternalInterface==null ||userListCallback==null || removeUserCallback==null || newQuestionCallback==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			

			
			///Javascript <- Flash	
			ExternalInterface.addCallback("leaveGame", userLeaving);	
			ExternalInterface.addCallback("newQuestion", newQuestionCallback);

			///Flash -> Javascript
			userListCallback(ExternalInterface.call("requestUsersList"));
			setLocalIdCallback(ExternalInterface.call("getMyID"));

		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * userLeaving
		 * aviso de que um usuário saiu do jogo
		 * @param String userId Id externo do jogador a ser retirado
		 * */
		private function userLeaving(userId:String):void
		{
			///checagem dos parametros
			if (userId==null || userId=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			removeUserCallback(userId);
		}

		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * verifyAnswer
		 *
		 * @param String answer a resposta do usuário [a..d | - ]
		 * @param String questionId Id externo da pergunta
		 * 
		 * */
		override public function sendAnswer(answer:String, questionId:String):void
		{
			///checagem dos parametros
			if (answer==null || answer=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			if (questionId==null || questionId=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			ExternalInterface.call("getAnswer", answer, questionId);
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function setCallback(externalName:String, internalFunc:Function):void
		{
			ExternalInterface.addCallback(externalName, internalFunc);
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function gameEnded():void
		{
			if (gameEndedCallback != null)
				gameEndedCallback();
		}
	}
}