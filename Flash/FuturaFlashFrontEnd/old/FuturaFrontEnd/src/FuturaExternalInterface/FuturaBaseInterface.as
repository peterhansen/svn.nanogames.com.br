package FuturaExternalInterface
{
	///som
	/**
	 * som de interação com o botão 
	 */
	import botao.mp3;
	
	/**
	 * FuturaBaseInterface
	 * base abstrata de comunicação com servidor
	 * @author Daniel Monteiro
	 * */
	public class FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * FuturaBaseInterface
		 * construtor que recebe callbacks
		 * @comment baseada na interface criada por Yuri Mello
		 * */
		public function FuturaBaseInterface(removeUser:Function, takeUserList:Function, setMyID:Function, newQuestion:Function, gameEnded:Function):void
		{		
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		public function init():void
		{
			
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * */
		public function getQuestionsFromServer(question:Object, previousIsCorrect:Boolean, previousAnswer:String):void
		{
			
		}

		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * Chama rotina no servior 
		 * @param cmd O comenado a ser executada
		 * @return  retorno da função, se houver.
		 * 
		 */
		public function call(cmd:String):Object
		{
			return null;	
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function sendAnswer(answer:String, questionId:String):void
		{
		
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function setCallback(externalName:String, internalFunc:Function):void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function gameEnded():void
		{
			
		}
	}
}