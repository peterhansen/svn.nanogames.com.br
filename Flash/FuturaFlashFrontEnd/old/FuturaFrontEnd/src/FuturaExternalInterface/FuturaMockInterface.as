package FuturaExternalInterface
{
	/**
	 * FuturaBaseInterface
	 * base abstrata de comunicação com servidor
	 * @author Daniel Monteiro
	 * */
	public class FuturaMockInterface extends FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		
		/**
		 * callback para responder se a resposta estava correta
		 */
		private var questionCallback:Function;
		
		/**
		 * uma variável apenas pra saber qual deve ser a próxima resposta a ser dada ao sistema
		 */
		private var lastAnswer:Boolean;
		
		/**
		 * callback que passa a lista de usuários pro jogo
		 */
		private var userList:Function;
		
		/**
		 * callback que avisa quem é o jogador local
		 */
		private var whoAmI:Function;
		
		private var removeUserCallback:Function;
		private var gameEndedCallback:Function;
		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * FuturaMockInterface
		 * construtor que recebe callbacks
		 * @comment baseada na interface criada por Yuri Mello
		 * */
		public function FuturaMockInterface(removeUser:Function, takeUserList:Function, setMyID:Function, newQuestion:Function, gameEndedFunc:Function):void
		{	
			super(removeUser,takeUserList,setMyID,newQuestion, gameEndedFunc);
			gameEndedCallback=gameEndedFunc;
			lastAnswer=true;
			questionCallback=newQuestion;
			userList=takeUserList;		
			whoAmI=setMyID;
			removeUserCallback=removeUser;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		override public function init():void
		{
			sendUserList();
			sendLocalUserNotify();
			sendQuestion(null,false,"");
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * @returns Object a pergunta
		 * */
		public function sendQuestion(question:Object, previousIsCorrect:Boolean, previousAnswer:String):void
		{		
			if (questionCallback != null)
			{
				/// variáveis usadas pela função
				var pergunta:Object;
				
				///criação do objeto de pergunta
				pergunta=new Object();
				
				///verificando se não conseguiu alocar o objeto
				if (pergunta==null)
					FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
				
				///preenche o objeto
				pergunta.title="O Sony Ericsson XPeria X10 Mini da Maria Chuchu das graças é Android de verdade? Desconfio que ele seja um Android paraguaio...";
				pergunta.a="não...Pode ser qualquer coisa e eu poderia deliberar por horas a fio a sua simples razão de existência, mas não é o caso para tal";
				pergunta.b="sim";
				pergunta.c="talvez?";
				pergunta.d="e o MotoBlur por acaso é? Alguem precisa avisar praquele povo que o Vanilla Android é mais que o suficiente para satisfazer todas as nossas vontades";
				pergunta.nextPlayer = "1";
				pergunta.id="437";
				lastAnswer = !lastAnswer;
				questionCallback(pergunta, 1 , "a");
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * sendUserList 
		 * Pede lista de usuários
		 */
		private function sendUserList():void
		{
			trace("FuturaMockInterface::sendUserList()");				
			var a:Array=new Array();
			var user:Object;
			
			user=new Object();
			user.id=1;
			user.nickname="joão1";			
			user.color="1";
			a.push(user);
			
			user=new Object();
			user.id=2;
			user.color="2";
			user.nickname="joão2";				
			a.push(user);
			
			user=new Object();
			user.id=3;
			user.color="3";
			user.nickname="joão3";				
			a.push(user);
			
			user=new Object();
			user.id=4;
			user.color="4";
			user.nickname="joão4";				
			a.push(user);
			
			if (userList!=null)
				userList(a);
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function sendLocalUserNotify():void
		{
			trace("FuturaMockInterface::sendLocalUserNotify()");
			if (whoAmI!=null)
				whoAmI("1");
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		}
		///--------------------------------------------------------------------------------------------------------------------
		///aqui o jogador falso nunca envia send, então nunca é disparada uma terceira pergunta
		override public function sendAnswer(answer:String, questionId:String):void
		{
			sendQuestion(null,true,"b");
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function setCallback(externalName:String, internalFunc:Function):void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function gameEnded():void
		{
			if (gameEndedCallback != null)
				gameEndedCallback();

		}

	}
}