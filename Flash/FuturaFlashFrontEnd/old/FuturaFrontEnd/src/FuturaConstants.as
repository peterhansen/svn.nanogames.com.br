package  
{
	/**
	 * @author Daniel Monteiro
	 * @brief Constantes de jogo
	 * */
	public class FuturaConstants
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * numero de peças no tabuleiro
		 */
		static public const NUMPIECES:int=24;
		/**
		 * numero de cores disponíveis 
		 */
		static public const NUMCOLORS:int=6;
		/**
		 * numero de turnos mínimos
		 */
		static public const NUMTURNS:int=NUMPIECES-1;
		/**
		 * mínimo de jogadores
		 */
		static public const MINPLAYERS:int=2;
		/**
		 * máximo de jogadores
		 */
		static public const MAXPLAYERS:int=6;
		/**
		 * cor: verde
		 */
		static public const COLORGREENINDEX:int=1;
		/**
		 * cor: amarelo
		 */
		static public const COLORYELLOWINDEX:int=2;
		/**
		 * cor: azul
		 */
		static public const COLORBLUEINDEX:int=3;
		/**
		 * cor:laranja 
		 */
		static public const COLORORANGEINDEX:int=4;
		/**
		 * cor: roxo
		 */
		static public const COLORPURPLEINDEX:int=5;
		/**
		 * cor:vermelho
		 */
		static public const COLORREDINDEX:int=6;
		/**
		 * constante hexa para cor branca
		 */
		static public const COLORWHITEVALUE:uint=0xFFFFFF;
		/**
		 * constante indicando o comprimento em pixels de uma resposta
		 */
		static public const ANSWER_LINE_WIDTH:int=200;
		/**
		 * o usuário não deu resposta válida ou não respondeu a tempo = 5
		 */
		static public const NOANSWER:int=4;
		/**
		 * tempo para jogo normal
		 * (61 para permitir ao jogador ver o ultimo segundo no relógio)
		 */
		static public const TIMENORMAL:int=61;
		/**
		 * tempo para jogo turbo
		 */
		static public const TIMETURBO:int=30;
		/**
		 * numero de frames no relógio
		 */
		static public const CLOCKFRAMES:int=60;
		/**
		 * quantos pontos por acerto o jogador ganha 
		 */
		static public const POINTSPERRIGHTANSWER:int=10;
		/**
		 * indicação de que a pergunta foi uma carta bonus 
		 */
		static public const BONUSCARDID:String="-4";
		/**
		 * indicação de que a pergunta foi uma carta penalidade
		 */
		static public const PENALTYCARDID:String="-5";
		///--------------------------------------------------------------------------------------------------------------------
	}
}