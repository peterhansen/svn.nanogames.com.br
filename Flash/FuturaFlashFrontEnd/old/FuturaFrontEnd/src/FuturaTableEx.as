package 
{
	///flash
	import FuturaExternalInterface.FuturaBaseInterface;
	import FuturaExternalInterface.FuturaInterface;
	import FuturaExternalInterface.FuturaMockInterface;
	
	import FuturaGUI.FuturaMessageWindow;
	import FuturaGUI.FuturaQuestionWindow;
	import FuturaGUI.FuturaScoreBox;
	import FuturaGUI.FuturaScoreWindow;
	import FuturaGUI.FuturaSpecialTurnWindow;
	
	import FuturaGameEngine.FuturaPlayer;
	
	import GUILib.GUIConstants;
	
	import carta_bonus.mp3;
	
	import carta_penalti.mp3;
	
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.geom.ColorTransform;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import resposta_correta.mp3;
	
	import resposta_errada.mp3;	
	
	public class FuturaTableEx extends Sprite
	{
		///--------------------------------------------------------------------------------------------------------------------		
		private var gameState:int;
		private var currentPlayer:int;
		private var localPlayerInternalId:int;
		private var players:Array;
		//private var playersToRemove:Array;
		private var nextQuestion:Object;
		/**
		 * o jogo esta em andamento?
		 */
		private var gameStopped:Boolean;			
		/**
		 * posições das peças no tabuleiro (uma por frame, por jogada acertada)
		 */
		private var piecesPosition:PiecesPosition;
		/**
		 * posições das setas no tabuleiro (uma por frame, por jogada acertada)
		 */
		private var arrowPosition:ArrowPosition;
		/**
		 * tabuleiro em sí
		 */
		private var board:Board;
		/**
		 * cartinhas empilhadas (enfeite)
		 */
		private var cards:Cards;
		/**
		 * fundo da mesa
		 */
		private var background:Background;
		private var scoreBox:FuturaScoreBox;
		/**
		 * janela de placar
		 */
		private var scoreWindow:FuturaScoreWindow;
		/**
		 * janela de perguntas
		 */
		private var questionWindow:FuturaQuestionWindow;
		/**
		 * janela de mensagens genéricas
		 */
		private var msgWindow:FuturaMessageWindow;
		/**
		 * janela de cartas de bônus e penalidade
		 */
		private var specialWindow:FuturaSpecialTurnWindow;
		/**
		 * som correto
		 */
		private var correctAnswer:resposta_correta.mp3;
		/**
		 * som errado
		 */
		private var wrongAnswer:resposta_errada.mp3;
		/**
		 * interface de comunicação externa
		 */
		private var futuraInterface:FuturaBaseInterface;	
		private var activePlayers:int;
		private var currentTurn:int;
		/**
		 * Timer para quando deve começar o novo turno
		 */
		private var timerToNextTurn:Timer;
	//	private var timerSetPiece:Timer;
		//private var timerOutcome:Timer;
		private var questionId:String;
		private var currentPiecePosition:int;
		private var lastOutcome:Boolean;
		private var lastAnswer:String;
		private var remotePlayerJustQuit:Boolean;
		///--------------------------------------------------------------------------------------------------------------------		
		public function FuturaTableEx()
		{
			///incializações triviais
			gameState=-1;
			enteringState(FuturaGameStates.HANDSHAKE_PREINIT);		
			remotePlayerJustQuit=false;	
			currentPlayer=-1;
			localPlayerInternalId=-1;
			
			///inicializações de objetos
			players=new Array();
			nextQuestion=null;
			
			
			correctAnswer= new resposta_correta.mp3();
			wrongAnswer = new resposta_errada.mp3();
			
			
			///aloca elementos da UI
			background=new Background();
			board=new Board();
			piecesPosition=new PiecesPosition();
			arrowPosition=new ArrowPosition();
			cards= new Cards();
			
			timerToNextTurn=new Timer(1.15*GUIConstants.SECOND);
//			timerSetPiece=new Timer(1.15*GUIConstants.SECOND);
//
//			timerOutcome=new Timer(1.15*GUIConstants.SECOND);
//			
//			timerOutcome.addEventListener(TimerEvent.TIMER, tickOutcome);
//			
			timerToNextTurn.addEventListener(TimerEvent.TIMER, tickNextTurn);
//			timerSetPiece.addEventListener(TimerEvent.TIMER, tickSetPiece);
			
			
			///janelas
			scoreWindow=new FuturaScoreWindow();			
			questionWindow=new FuturaQuestionWindow();								
			msgWindow=new FuturaMessageWindow();
			specialWindow=new FuturaSpecialTurnWindow();
			scoreBox=new FuturaScoreBox();			
			
			players=new Array();
			//futuraInterface = new FuturaMockInterface(
			futuraInterface = new FuturaInterface(
				CALLBACK_removeUser, 
				CALLBACK_takeUserListFromServer, 
				CALLBACK_setMyId, 
				CALLBACK_newQuestion,
				endGame
			);
			
			futuraInterface.setCallback("getLocalPlayerFuturaId", DEBUG_returnMyFuturaId);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function initGameTable():void
		{
			for (var c:int=0;c<piecesPosition.numChildren;c++)
				if (piecesPosition.getChildAt(c) is Piece)
				{
					(piecesPosition.getChildAt(c) as Piece).visible=false;
				}
			
			for (var d:int=0;d<arrowPosition.numChildren;d++)
				if (arrowPosition.getChildAt(d) is Arrow)
				{
					(arrowPosition.getChildAt(d) as Arrow).visible=false;
				}
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function initUI():void
		{
			var url:String=null;
			
			/// fundo /////////////////////////////////////////////////////////////////////
			background.cacheAsBitmap=true;
			addChild(background);
			background.x=(stage.stageWidth)/2;
			background.y=(stage.stageHeight)/2;			
			
			/// tabuleiro /////////////////////////////////////////////////////////////////
			board.cacheAsBitmap=true;
			addChild(board);			
			board.x=150+ board.width/2;
			board.y=15+board.height/2;
			
			/// peças /////////////////////////////////////////////////////////////////////			
			piecesPosition.cacheAsBitmap=true;
			board.addChild(piecesPosition);		
			
			/// seta /////////////////////////////////////////////////////////////////////
			arrowPosition.cacheAsBitmap=true;
			board.addChild(arrowPosition);
			arrowPosition.x=0;
			arrowPosition.y=0;			
			
			/// cartas /////////////////////////////////////////////////////////////////////
			cards.cacheAsBitmap=true;
			addChild(cards);
			cards.x=cards.width/2;
			cards.y=height-(cards.height);
			
			/// box de pontuação/////////////////////////////////////////////////////////////////////
			addChild(scoreBox);
			scoreBox.x=7;
			scoreBox.y=7;
			///janelas
			/// janela de placar geral //////////////////////////////////////////////////////
			addChild(scoreWindow);
			scoreWindow.initWindow();
			scoreWindow.visible=false;
			scoreWindow.setOkCallback(leave);
			
			/// janela de perguntas //////////////////////////////////////////////////////			
			addChild(questionWindow);
			questionWindow.initWindow();			
			questionWindow.visible=false;			
			questionWindow.setAnsweredCallback(sendAnswer);
			
			/// janela genérica de mensagens //////////////////////////////////////////////////////	
			addChild(msgWindow);
			msgWindow.initWindow();
			msgWindow.visible=false;
			
			/// janela de jogada especial //////////////////////////////////////////////////////	
			addChild(specialWindow);
			specialWindow.initWindow();
			specialWindow.visible=false;
			
			
			///pega fundo da mesa
			//url=futuraInterface.call("getBackgroundURL") as String;
			if (url != null)
			{
				var loader:Loader;
				var req:URLRequest;
				req=new URLRequest(url);
				loader=new Loader();
				loader.load(req);
				addChildAt(loader,0);
			}
				///fundo não disponível - usando imagem failsafe
			else
			{
				background=new Background();
				background.x=background.width/2;
				background.y=background.height/2;
				
				addChildAt(background,0);
			}
			initGameTable();	
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function initEngine():void
		{
			futuraInterface.init();
			if (futuraInterface is FuturaMockInterface)
				currentPlayer=0;
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function init():void
		{
			enteringState(FuturaGameStates.HANDSHAKE_INIT);
			initUI();
			initEngine();
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function start():void
		{
			enteringState(FuturaGameStates.HANDSHAKE_START);
			gameStopped=false;
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function takePlayerList(playersFromServer:Array):void
		{
			enteringState(FuturaGameStates.HANDSHAKE_PLAYERLIST);
			
			var player:FuturaPlayer;
			
			for each (var user:Object in playersFromServer)
			{
				trace(">"+user.nickname+">"+user.id);
				CALLBACK_addUser(user);
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function setMyId(myId:int):void
		{
			enteringState(FuturaGameStates.HANDSHAKE_MYID);
			localPlayerInternalId=myId;
			futuraInterface.call("gameReady");
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getArrowPosition():int
		{
			return currentPiecePosition;
		}
		///--------------------------------------------------------------------------------------------------------------------	
		public function readyForNewTurn():void
		{
			if (gameStopped)
				return;
			
			enteringState(FuturaGameStates.STARTTURN_READYFORNEWTURN);
			++currentTurn;
			
			if (getArrowPosition() < arrowPosition.numChildren)			
				placeArrow(getArrowPosition(),getCurrentPlayer().getColorIndex());
			
			showQuestion();
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getNewQuestion():void
		{
			enteringState(FuturaGameStates.STARTTURN_GETNEWQUESTION);
		}

		///--------------------------------------------------------------------------------------------------------------------
		public function showQuestion():void
		{			
			if (gameStopped)
				return;
			
			if (localPlayerIsCurrentPlayer())
			{
				enteringState(FuturaGameStates.MIDTURN_SHOWMYNEWQUESTION);
			}
			else
			{
				enteringState(FuturaGameStates.MIDTURN_SHOWOTHERNEWQUESTION);
			}
			
			//nextQuestion.kind = -1;
			
			if (localPlayerIsCurrentPlayer() && nextQuestion.kind!=0 && nextQuestion.kind!=null)
			{
				
				if (nextQuestion.kind > 0)
				{
					specialWindow.setHappyIcon();
					specialWindow.showModalMessage("Bônus ( "+getCurrentPlayer().getName()+" jogando )","Parabéns!\n\nVocê acaba de receber os pontos\nde uma pergunta como bônus!",sendAnswer);
					questionId=FuturaConstants.BONUSCARDID;
				}
				else
				{
					specialWindow.setSadIcon();
					specialWindow.showModalMessage("Penalidade ( "+getCurrentPlayer().getName()+" jogando )","Que azar!\n\nVocê acaba de ser penalizado e\nperder os pontos de uma pergunta.",sendAnswer);
					questionId=FuturaConstants.PENALTYCARDID;
				}
				
				return;
			}
			
			var array:Array;
			
			///internaliza os dados recebidos e validados
			this.questionId = nextQuestion.id;
			
			///preenche os campos de passagem de pergunta para a janela de perguntas.
			///um campo não preenchido aqui não significa necessariamente um erro.
			array=new Array();
			
			///1º opção
			if (nextQuestion.a!=null && (nextQuestion.a as String).length>0)
				array.push(nextQuestion.a);
			
			///2º opção
			if (nextQuestion.b!=null && (nextQuestion.b as String).length>0)			
				array.push(nextQuestion.b);
			
			///3º opção	
			if (nextQuestion.c!=null && (nextQuestion.c as String).length>0)			
				array.push(nextQuestion.c);
			
			///4º opção			
			if (nextQuestion.d!=null && (nextQuestion.d as String).length>0)			
				array.push(nextQuestion.d);
			
			///...mas se não houver pelo menos duas opções válidas, a pergunta é inválida
			//			if (array.length < 2)
			//				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///por fim, podemos fazer a pergunta.
			questionWindow.newQuestion(nextQuestion.title,array,20, localPlayerIsCurrentPlayer(), getCurrentPlayer().getName());
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getCurrentPlayer():FuturaPlayer
		{
			return getPlayer(currentPlayer);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getLocalPlayer():FuturaPlayer
		{
			return getPlayer(localPlayerInternalId);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getPlayer(index:int):FuturaPlayer
		{
			return players[index] as FuturaPlayer;
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function sendAnswer():void
		{
			enteringState(FuturaGameStates.MIDTURN_SENDANSWER);
			if (!localPlayerIsCurrentPlayer())
				return;
			
			///variaveis usadas pela função
			var ans:Object;
			var answer:int;
			
			///verifica se é carta bonus ou penalidade
			if (questionId==FuturaConstants.BONUSCARDID)
			{
				getCurrentPlayer().playerDidHit();
				return;
			}
			
			if (questionId==FuturaConstants.PENALTYCARDID)
			{
				getCurrentPlayer().addPenalty();
				return;
			}
			
			
			///valida a reposta a ser enviada
			if (questionWindow!=null)
				answer=	questionWindow.getAnswer();
			else
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			if (answer<0 || answer > 4)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///envia a resposta no formato esperado pelo servidor. (também não gostei disso, mas...)
			ans={0: "a", 1:"b", 2:"c", 3:"d", 4:"-"};
			futuraInterface.sendAnswer(ans[questionWindow.getAnswer()], questionId);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function waitForServerAnswer():void
		{
			if (gameState==FuturaGameStates.MIDTURN_SHOWOTHERNEWQUESTION)
				enteringState(FuturaGameStates.ENDTURN_WAITINGFOROTHERANSWER);
			else
				enteringState(FuturaGameStates.ENDTURN_WAITINGFORMYANSWER);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function showAnswer(currentIsCorrect:Boolean, currentAnswer:String):void
		{
			enteringState(FuturaGameStates.ENDTURN_SHOWINGANSWER);
//			timerOutcome.start();
			lastOutcome=currentIsCorrect;
			lastAnswer=currentAnswer;
//		}
//		///--------------------------------------------------------------------------------------------------------------------
//		public function tickOutcome(e:Event):void
//		{
//			timerOutcome.reset();
			
			if (remotePlayerJustQuit)
			{
				return;
			}
			
			if (currentTurn != 0)
			{
				if (lastOutcome)
					playerAnsweredCorrectly();
				else
					playerAnsweredWrongly();
				
				if (!localPlayerIsCurrentPlayer())
					questionWindow.setDisplayedAnswer(letterToIndex(lastAnswer));
			}
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * askForNewTurn
		 * pede o começo dum novo turno (deve acontecer ~2 segundos depois)
		 */
		public function askForNewTurn():void
		{
			timerToNextTurn.start();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * tickNextTurn
		 * inicia o novo turno de fato
		 * @param e uso interno do timer
		 */
		public function tickNextTurn(e:TimerEvent):void
		{
			timerToNextTurn.reset();
			
			if (!gameStopped)
				startNextTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function startNextTurn():void
		{
			readyForNewTurn();
		}	
		///--------------------------------------------------------------------------------------------------------------------
		public function updateScoreBox():void
		{
			///checagem de consistência padrão
			//checkGameConsistencyFor("updatePointsBox");
			
			///variáveis usadas na função
			var player:FuturaPlayer;
			
			///atualiza a caixa de score
			player=getPlayer(localPlayerInternalId);				
			scoreBox.setDisplayItems(player.getHits(),player.getErrors(),player.getScore());
		}
		///--------------------------------------------------------------------------------------------------------------------		
		public function endTurn():void
		{
//			if (playersToRemove != null)
//			{
//				for (var c:int=0;c<playersToRemove.length;c++)
//				{
//					CALLBACK_removeUser((playersToRemove[c] as int).toString());	
//				}
//			}
//			playersToRemove=null;
				
			questionWindow.setInvisible();
//			timerSetPiece.start();	
//		}
//		
//		///--------------------------------------------------------------------------------------------------------------------		
//		public function tickSetPiece(e:Event):void
//		{
//			timerSetPiece.reset();
			updateScoreBox();
			
			if (nextQuestion != null && nextQuestion.nextPlayer != null )
				currentPlayer = getLocalIdFromFuturaId(nextQuestion.nextPlayer as String);
			else
				setNextPlayer();
			
			/*
			if (!gameStopped && currentPiecePosition <= FuturaConstants.NUMPIECES-1)
				askForNewTurn();
			else
				endGame();
			*/
			if (!gameStopped && currentPiecePosition <= FuturaConstants.NUMPIECES-1)
				askForNewTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function hideAllWindows():void
		{
			scoreWindow.setInvisible();
			questionWindow.setInvisible();
			msgWindow.setInvisible();
			specialWindow.setInvisible();
			scoreBox.setInvisible();
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function endGame():void
		{
			hideAllWindows();
			
			gameStopped=true;
			
			scoreWindow.updatePlayerList(players);
			scoreWindow.startShowAnimation();
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setNextPlayerandGetIt
		 * define o próximo jogador
		 * @returns FuturaPlayer o (novo) jogador atual
		 * */
		public function setNextPlayer():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("setNextPlayer");		
			trace ("setNextPlayer");
			///variaveis usadas
			var player:FuturaPlayer;
			var nextIndex:int;
			///conta quantas vezes ja percorremos a lista em busca de um jogador ativo. Evita loops infinitos
			var cycled:int;
			
			/// começamos do jogador atual
			nextIndex=currentPlayer;
			
			/// e sem ciclos tentados
			cycled=0;
			
			///buscando o próximo
			do
			{
				///incrementa
				++nextIndex;
				
				///verifica overflow e trata
				if (nextIndex >= players.length)
				{
					nextIndex=0;
					++cycled;
				}
				///obtem a referencia, para verificar se ele esta ativo ou não
				player=getPlayer(nextIndex);
				
			} ///e se ele não estiver ativo, continuamos tentando - a não ser que ja tenhamos tentado demais...
			while (!player.getActive() && cycled < 3);
			
			///caso tenhamos tentado o suficiente, sem conseguir um, temos uma situação de erro.
			if (cycled >= 3)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			currentPlayer=nextIndex
			trace ("proximo jogador é:"+ currentPlayer.toString());
		}		
		///--------------------------------------------------------------------------------------------------------------------
		public function localPlayerIsCurrentPlayer():Boolean
		{
			return currentPlayer==localPlayerInternalId;
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getLocalIdFromFuturaId(FuturaId:String):int
		{
			var player:FuturaPlayer;
			
			for (var c:int=0;c<players.length;++c)
			{
				player=players[c] as FuturaPlayer;
				
				if (player.getFuturaId() == FuturaId)
					return c; 
			}
			
			return -1;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_newQuestion(nextQuestionFromServer:Object, previousIsCorrect:String, previousAnswer:String):void
		{
			getNewQuestion();
			var previousOutcomeCandidate:Boolean;
			
			nextQuestion=nextQuestionFromServer;
			//playersToRemove=nextQuestion.playersToRemove;
			
			if (previousIsCorrect != null && previousAnswer != null)
			{
				previousOutcomeCandidate = Boolean(parseInt( previousIsCorrect ));
				showAnswer(previousOutcomeCandidate,previousAnswer);
			}
			endTurn();
		}
		
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_takeUserListFromServer(userList:Array):void
		{
			takePlayerList(userList);
		}	
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_setMyId(MyId:String):void
		{
			var myIdCandidate:int=-1;
			
			myIdCandidate=getLocalIdFromFuturaId(MyId);
			///TODO: checagem de erros!!!
			trace("eu sou:"+MyId+"("+myIdCandidate.toString()+")");
			setMyId(myIdCandidate);
			
		}			
		///--------------------------------------------------------------------------------------------------------------------
		private function enteringState(NewGameState:int):void
		{
			trace(FuturaGameStates.toString(gameState)+" -> "+FuturaGameStates.toString(NewGameState));
			gameState=NewGameState;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function CALLBACK_addUser(user:Object):void
		{
			var player:FuturaPlayer;
			
//			static public const COLORGREENINDEX:int=1;
//			static public const COLORYELLOWINDEX:int=2;
//			static public const COLORBLUEINDEX:int=3;
//			static public const COLORORANGEINDEX:int=4;
//			static public const COLORPURPLEINDEX:int=5;
//			static public const COLORREDINDEX:int=6;


			
			
			
			player=new FuturaPlayer();
			player.setActive(true);
			player.setName(user.nickname);
			//player.setColorIndex( parseInt(user.color));		
			player.setColorIndex((players.length%FuturaConstants.NUMCOLORS)+1);
			player.setFuturaId(user.id);			
			players.push(player);
			++activePlayers;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_removeUser(FuturaId:String):void
		{
			var player:FuturaPlayer;
			var internalId:int;
			
			internalId= getLocalIdFromFuturaId(FuturaId);
			
			player = players[internalId];
			remotePlayerJustQuit=true;
			player.setActive(false);
			--activePlayers;
			
			if (currentPlayer == internalId 
//			&& 
//					( 
//						gameState ==  FuturaGameStates.MIDTURN_SHOWMYNEWQUESTION ||
//						gameState ==  FuturaGameStates.MIDTURN_SHOWOTHERNEWQUESTION ||
//						gameState ==  FuturaGameStates.MIDTURN_SENDANSWER ||
//						gameState ==  FuturaGameStates.ENDTURN_WAITINGFORMYANSWER ||
//						gameState ==  FuturaGameStates.ENDTURN_WAITINGFOROTHERANSWER ||
//						gameState ==  FuturaGameStates.ENDTURN_SHOWINGANSWER ||
//						gameState ==  FuturaGameStates.ENDTURN_WAITINGFORNEWTURN
//				
//					)
				)
			{
				endTurn();
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function letterToIndex(letter:String):int
		{
			if (letter == "a") return 0;
			if (letter == "b") return 1;
			if (letter == "c") return 2;
			if (letter == "d") return 3;
			
			return FuturaConstants.NOANSWER;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function indexToLetter(index:int):String
		{
			var letters:String="abcd-";
			return letters[index];				
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function leaveInError():void
		{
			FuturaSafeGuard.fatalError("");
		}
		///-------------------------------------------------------------------------------------------------------------------
		private function leave():void
		{
			futuraInterface.call("exitGame");
			visible=false;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function fatalError(msg:String):void
		{
			msgWindow.showModalMessage("Erro",msg,leaveInError);
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function nonFatalError(msg:String):void
		{
			FuturaSafeGuard.nonFatalError(msg);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setPiece
		 * coloca uma peça no tabuleiro, indicando acerto do jogador
		 * @param index posição da peça no tabuleiro [1..FuturaConstants.NUMPIECES]
		 * @param color indice da cor da seta [1..6]
		 * */		
		private function setPiece(index:int, color:int):void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("setPiece");		
			
			///checagem de consistência de parametros////
			/// a posição da peça deve ser válida
			///NÃO USAR >= , POIS FLASH É [1..NUMCHILDREN]
			if (index > FuturaConstants.NUMPIECES || index < 1)
			{
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			}
			
			/// a cor deve ser válida também 
			if (color < FuturaConstants.COLORGREENINDEX || color > FuturaConstants.COLORREDINDEX)
			{
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			}				
			/////////////////////////////////////////////			
			
			///variáveis usadas na função
			var piece:Piece;
			
			///obtem a peça, torna visível e colore
			piece=piecesPosition.getChildAt(index) as Piece;
			piece.visible=true;
			piece.gotoAndStop(color);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function nextPiecePosition():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("nextPiecePosition");		
			
			///checagem de consistência de elementos da função
			if (currentPiecePosition > FuturaConstants.NUMPIECES || currentPiecePosition < 0)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+"(currentPiecePosition="+ currentPiecePosition.toString()+")");
			}
			
			///incrementa a posição da próxima peça, mantendo a consistência
			currentPiecePosition++;
			if (currentPiecePosition>FuturaConstants.NUMPIECES)
				currentPiecePosition=1;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerAnsweredCorrectly
		 * indica que o jogador respondeu corretamente a pergunta
		 * @comment Aqui, não apenas a jogada é encerrada, mas a rodada também
		 * @see playerAnsweredWrongly
		 * */
		public function playerAnsweredCorrectly():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("playerAnsweredCorrectly");		
			
			///variáveis usadas na função
			var actor:FuturaPlayer;
			
			///notifica ao jogador do acerto
			actor=getCurrentPlayer();
			actor.playerDidHit();
//			if (correctAnswer!=null && questionId!="-1")
//				correctAnswer.play();

			///incrementa e coloca sua peça no tabuleiro, se válido			
			if (currentPiecePosition<=piecesPosition.numChildren)
				setPiece(currentPiecePosition,actor.getColorIndex());
			nextPiecePosition();
			
			
			///indica que a jogada esta encerrada (rodada também?)
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerAnsweredWrongly
		 * indica que o jogador responder erroneamente
		 * @see playerAnsweredCorrectly
		 * @comment Vale notar que aqui a rodada não é encerrada, apenas a jogada
		 * */
		public function playerAnsweredWrongly():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("playerAnsweredWrongly");		
			
			///variaveis usadas na função			
			var actor:FuturaPlayer;
//			if (wrongAnswer!=null && questionId!="-1")
//				wrongAnswer.play();
			
			///notifica o jogador de seu erro
			actor=getCurrentPlayer();
			actor.playerDidError();
			
			///e finaliza a jogada
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * placeArrow
		 * coloca seta na posicao do tabuleiro
		 * @param index posicao da seta no tabuleiro [1..FuturaConstants.NUMPIECES]
		 * @param color indice da cor da seta [1..6]
		 * */
		private function placeArrow(index:int,color:int):void
		{
			///checagem de consistência padrão
			//checkGameConsistencyFor("placeArrow");
			
			///checagem de consistência de parametros////
			/// a posição da peça deve ser válida
			if (index >= arrowPosition.numChildren || index >= FuturaConstants.NUMPIECES || index < 0)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( index="+index.toString()+"/"+arrowPosition.numChildren.toString()+")");
			}
			
			/// a cor deve ser válida também 
			if (color < FuturaConstants.COLORGREENINDEX || color > FuturaConstants.COLORREDINDEX)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			}			
			/////////////////////////////////////////////
			
			///variáveis usadas na função
			var arrow:Arrow;
			
			///apagando todas outras possíveis setas visíveis
			for (var c:int=0;c<index;c++)
				if (arrowPosition.getChildAt(c) is Arrow)
				{
					arrow=arrowPosition.getChildAt(c) as Arrow;
					arrow.visible=false;
				}
			
			///obtendo a seta atual, tornando-a visível e colorida
			arrow=arrowPosition.getChildAt(index) as Arrow;
			arrow.visible=true;
			arrow.gotoAndStop(color);			
		}		
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * checkGameConsistencyFor
		 * checa para ver se o jogo esta válido antes de prosseguir
		 * @param String funcName nome da função que pediu a checagem
		 * @comment nem todas as checagens são executadas, pois essa checagem pode esbarrar num caso em que algo
		 * @comment testado ainda não esta pronto e este é seu estado esperado
		 * */
		public function checkGameConsistencyFor(funcName:String):void
		{
			/////////////<checagem padrão de consistência de jogo
			///jogador atual é válido?
			if (players==null)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 1 em FuturaTable::"+funcName+" )");

			///uma pequena leniencia com o sistema ainda não totalmente inicializado
			if(currentPlayer != -1)
			{

				if (players[currentPlayer]==null)
					fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 2 em FuturaTable::"+funcName+" )");
			
				///jogador atual é ativo?
				if (!(players[currentPlayer] as FuturaPlayer).getActive())
					nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 3 em FuturaTable::"+funcName+" )");
			}
			
			
			if (localPlayerInternalId < 0 || localPlayerInternalId > players.length)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 4 em FuturaTable::"+funcName+" )");
			
			if (players[localPlayerInternalId]!=null && !players[localPlayerInternalId].getActive())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 5 em FuturaTable::"+funcName+" )");
			
			if (currentPlayer != -1 )
			{
				if (currentPlayer < 0 || currentPlayer > players.length)
					fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 6 em FuturaTable::"+funcName+" )");
			}
			
			///se é minha vez, minha pergunta é válida? (o id)
			if ( currentPlayer==localPlayerInternalId && ( questionId==null || questionId=="" || questionId==" ") )
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 7 em FuturaTable::"+funcName+" )");
			
			///o jogo esta em andamento? (não é erro, mas deve gerar um warning no log)
			if (gameStopped)
			{
				warnDeveloper(FuturaStrings.CALLS_DURING_STOPPED_GAME+" ( teste 8 em FuturaTable::"+funcName+" )");
				return;
			}
			//tudo certo
		}
		///------------------------------------------------------------------------------------------------------------------
		public function warnDeveloper(msg:String):void
		{
			trace("WARNING(DEVLEVEL):"+msg);
		}
		///------------------------------------------------------------------------------------------------------------------
		public function DEBUG_returnMyFuturaId():String
		{
			if (localPlayerInternalId > 0 && localPlayerInternalId < activePlayers)
				return getPlayer(localPlayerInternalId).getFuturaId();
			else
				return null;
		}
	}
}