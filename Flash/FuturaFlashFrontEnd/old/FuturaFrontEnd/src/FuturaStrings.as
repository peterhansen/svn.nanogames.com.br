package  
{
	/**
	 * @author Daniel Monteiro
	 * Lista de strings do jogo
	 *  
	 * */
	public class FuturaStrings
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * o jogo se encontra em estado inconsistente
		 */
		public static const INCONSISTENT_GAME_STATE:String="Erro: Jogo em estado inconsistente.\n Por favor, teste mais tarde\n";
		/**
		 * os dados enviados pelo servidor não são válidos
		 */
		public static const INCONSISTENT_DATA_RECEIVED:String="Erro: Dados recebidos pelo servidor.";
		/**
		 * erro alocando objeto
		 */
		public static const ALLOC_ERROR:String="Erro: Não foi possível alocar objeto.";
		/**
		 * erro alocando objeto
		 */
		public static const INTERNAL_ERROR:String="Erro: Estado inválido do sistema.";
		/**
		 * Aconteceu uma chamada de jogo enquanto o jogo ja estava parada.
		 */
		public static const CALLS_DURING_STOPPED_GAME:String="Aviso: Uma chamada de jogo foi feita enquanto o jogo estava interrompido.";
		/**
		 * Texto de acertos
		 */
		public static const HITS_TEXT_PLURAL:String="Acertos";
		/**
		 * texto de pontos
		 */
		public static const POINTS_TEXT_PLURAL:String="Pontos";
		/**
		 * texto de erros
		 */
		public static const MISSES_TEXT_PLURAL:String="Erros";
		/**
		 * Texto de acertos
		 */
		public static const HITS_TEXT_SINGULAR:String="Acerto";
		/**
		 * texto de pontos
		 */
		public static const POINTS_TEXT_SINGULAR:String="Ponto";
		/**
		 * texto de erros
		 */
		public static const MISSES_TEXT_SINGULAR:String="Erro";
		///--------------------------------------------------------------------------------------------------------------------
	}
}