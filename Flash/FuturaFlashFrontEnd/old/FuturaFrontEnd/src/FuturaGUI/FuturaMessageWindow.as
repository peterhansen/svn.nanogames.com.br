package FuturaGUI
{
	///GUILib		
	import GUILib.FadableSprite;
	import GUILib.ClickableCheckBox;
	import GUILib.ClickableFadableSprite;
	import GUILib.FadableSprite;
	import GUILib.FadableText;
	import GUILib.HorizontalLineSeparatorSprite;
	import GUILib.SpriteAndTextSprite;
	import GUILib.SpriteHorizontalList;
	import GUILib.SpriteVerticalList;
	
	///som
	import botao.mp3;
	
	
	/**
	 * FuturaMessageWindow
	 * implementa uma janela de mensagens genéricas
	 * @author Daniel Monteiro
	 * */
	public class FuturaMessageWindow extends FuturaBaseWindow
	{
		/**
		 * botão de fechar a janela
		 */
		private var buttonSprite:ClickableFadableSprite;
		
		/**
		 * texto de mensagem 
		 */
		private var msgText:FadableText;
		
		/**
		 * texto de título
		 */
		private var titleText:FadableText;
		
		/**
		 * lista items visuais 
		 */
		private var listPanes:SpriteVerticalList;
		
		/**
		 * callback de saida
		 */
		private var dismissCallback:Function;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * construtor
		 * */
		public function FuturaMessageWindow()
		{
			super();
			
			///conteúdo meramente de teste
			titleText=new FadableText("Sabe duma coisa?",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getTitleTextFormat());
			msgText=new FadableText("Daniel é sinistrão!",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());			
			buttonSprite=new ClickableFadableSprite();
			listPanes=new SpriteVerticalList();
			
			///checagem de qualidade de inicialização
			if (titleText==null || msgText == null || buttonSprite == null || listPanes == null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * initWindow
		 * inicialização do layout básico da janela
		 * */
		override public function initWindow():void
		{
			super.initWindow();
			
			///checagem de qualidade de inicialização
			if (titleText==null || msgText == null || buttonSprite == null || listPanes == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///variáveis de montagem de layout
			var separator:HorizontalLineSeparatorSprite;
			var button:OkButton;
			
			///alocações
			separator=new HorizontalLineSeparatorSprite(0,width-60-65);			
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			button=new OkButton();			
			if (button==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			///titulo janela>
			listPanes.addItem(titleText);		
			
			listPanes.x=65;
			listPanes.y=50;
			
			///separador
			listPanes.addItem(separator,7);			
			
			///texto
			listPanes.addItem(msgText,10);		
			
			///botão
			buttonSprite.addChild(button);
			listPanes.addItem(buttonSprite);			
			
			button.cacheAsBitmap=true;
			buttonSprite.y=height-50-button.height/2-listPanes.y;
			buttonSprite.x=-listPanes.x+(width)/2;
			setButtonCallback(myCallback);
			///deu tudo certo
			addWidget(listPanes);
		}			
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * showMessage
		 * pequeno açucar sintático para exibir a janela de uma vez
		 * @param String t título da janela
		 * @param String m mensagem em sí
		 * @comment aqui não tem problema em a janela não ter título ou mensagem, desde que não sejam nulos
		 * */
		public function showMessage(t:String,m:String):void
		{
			///checagem de qualidade de inicialização
			if (titleText==null || msgText == null || buttonSprite == null || listPanes == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///checagem dos parametros
			if (t==null || m==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			setTitle(t);
			setMessage(m);
			startShowAnimation();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * showModalMessage
		 * mostra a janela, mas dessa vez, tenta ter um comportamento modal (mas nada é garantido)
		 * @param String t título da janela
		 * @param String m mensagem em sí
		 * @param Function f função para ser chamada quando for pra fechar a janela e liberar o resto do sistema 
		 * @see showMessage
		 * */
		public function showModalMessage(t:String, m:String, f:Function):void
		{
			///checagem de qualidade de inicialização
			if (titleText==null || msgText == null || buttonSprite == null || listPanes == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///checagem dos parametros
			if (t==null || m==null || f==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			showMessage(t,m);
			dismissCallback=f;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setButtonCallback
		 * setta callback para quando clicar no botão
		 * @param f a função de callback
		 * 
		 */
		public function setButtonCallback(f:Function):void
		{
			buttonSprite.setOnMouseClickCallback(f);
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * getMyDissmissCallback
		 * @return a função callback de fechamento da janela 
		 * 
		 */
		public function getMyDismissCallback():Function
		{
			return dismissCallback;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * myCallback
		 * O callback interno que organiza a ordem dos eventos
		 */
		private function myCallback():void
		{
			super.callButtonSound();
			
			if (dismissCallback!=null)
				dismissCallback();
			
			setInvisible();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setMessage
		 * define qual é a mensagem para ser exibida
		 * @param String Text a Mensagem, que pode ser vazia, mas não nula
		 * @see setTitle
		 * */
		public function setMessage(Text:String):void
		{
			///checagem de qualidade de inicialização
			if (titleText==null || msgText == null || buttonSprite == null || listPanes == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///checagem dos parametros
			if (Text==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			msgText.setText(Text);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setTitle
		 * define qual é o título da janela
		 * @param String Text O título da janela, que pode ser vazio, mas não nulo
		 * @see setMessage
		 * */
		public function setTitle(Text:String):void
		{
			///checagem de qualidade de inicialização
			if (titleText==null || msgText == null || buttonSprite == null || listPanes == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			if (Text==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			titleText.setText(Text);
		}
		///--------------------------------------------------------------------------------------------------------------------	
	}
}