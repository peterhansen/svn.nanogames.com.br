package FuturaGUI
{
	///flash
	import flash.display.Sprite;
	import flash.text.TextFormat;
	
	///GUILib
	import GUILib.FadableSprite;
	
	///som
	import botao.mp3;
	
	/**
	 * FuturaBaseWindow
	 * janela seguindo a identidade visual do resto do jogo
	 * @author Daniel Monteiro
	 * */
	public class FuturaBaseWindow extends FadableSprite
	{
		/**
		 * callback de quando ela começa a aparecer
		 */
		private var OnMyStartShowAnimationCallback:Function;
		
		/**
		 * callback de quando começa a esconder a janela
		 */
		private var OnMyStartHideAnimationCallback:Function;	
		
		/**
		 * fundo da janela
		 */
		private var background:FadableSprite;
		
		/**
		 * controles gráficos visiveis na janela
		 */
		private var widgets:Array;
		
		/**
		 * container dos controles gráficos da janela
		 */
		private var widgetsHolder:FadableSprite;
		
		///para manter a unidade visual, todo mundo vai pedir à janela base o formato de texto a ser usado.
		///(se mudar, só tem que mudar em um unico lugar)
		
		/**
		 * formato de texto para título de janela
		 */
		private static var titleTextFormat:TextFormat;
		
		/**
		 * formato de texto para texto regular de janela
		 */
		private static var regularTextFormat:TextFormat;
		
		/**
		 * som de interação 
		 */
		private var interaction:botao.mp3;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * FuturaBaseWindow
		 * construtor
		 * */
		public function FuturaBaseWindow()
		{
			super();
			
			widgets=new Array;
			widgetsHolder=new FadableSprite();
			background=new FadableSprite();
			
			///checa se a construção deu certo ou não
			if (widgets==null || widgetsHolder==null || background==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			interaction=new mp3();
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * initWindow 
		 * inicializa a janela - para ser chamada quando ja houver um parent
		 */
		public function initWindow():void
		{
			if (background==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///incialização prematura, desenvolvedor!
			if (parent==null)
			{
				FuturaSafeGuard.traceMsg("FuturaBaseWindow::initWindow","Desenvolvedor, mais atenção! inicialização fora de hora!");
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			}
			
			///variaveis
			var popUpMc:Popup;
			
			///aloca o fundo da janela e verifica se deu certo
			popUpMc=new Popup();
			
			if (popUpMc==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			///adições e hierarquias
			addChild(background);
			addChild(widgetsHolder);
			background.addChild(popUpMc);
			
			///tenta otimizar contra os artifacts de Flash em FF3 com chamadas para JS
			popUpMc.cacheAsBitmap=true;
			background.cacheAsBitmap=true;						
			
			///centraliza a janela			
			/// não foi muito bonito, mas centralizou a janela.		
			x=3;
			y=3;
			background.x=width*0.5;
			background.y=height*0.5;
			
			///esconde por enquanto
			setBGOpacity(0.95);
			background.visible=false;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getRegularTextFormat
		 * obtem singleton de formatação padrão de texto comum
		 * @returns TextFormat formato de texto para TextField
		 * */
		public static function getRegularTextFormat():TextFormat		
		{
			if (regularTextFormat==null)
			{
				regularTextFormat=new TextFormat();
				regularTextFormat.size=14;
				regularTextFormat.font="Arial";
			}
			return regularTextFormat;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getTitleTextFormat
		 * obtem singleton de formatação padrão de texto de título de janela
		 * @returns TextFormat formato de texto para TextField
		 * */
		public static function getTitleTextFormat():TextFormat		
		{
			if (titleTextFormat==null)
			{
				titleTextFormat=new TextFormat();
				titleTextFormat.size=22;
				titleTextFormat.font="Arial";
			}
			return titleTextFormat;
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * addWidget
		 * Adiciona um controle gráfico na janela
		 * @param FadableSprite sprite
		 * @comment Tudo tem que ser baseado em FadableSprite, para ter certeza de que o controle vai saber sumir e 
		 * @comment aparecer no mesmo passo da janela
		 * */
		public function addWidget(sprite:FadableSprite):void
		{
			///verifica o parametro
			if (sprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);				
			
			///verifica estado interno
			if (widgets==null || widgetsHolder==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			widgets.push(sprite);
			widgetsHolder.addChild(sprite);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * OnMyStartShowAnimation
		 * implementa meu comportamento de primeiro aparecer o fundo, depois os widgets
		 * */
		private function OnMyStartShowAnimation():void
		{			
			///verifica estado interno
			if (widgets==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///variaveis
			var sprite:FadableSprite;
			
			for (var c:int=0;c<widgets.length;c++)
			{
				///obtem o elemento visual
				sprite=widgets[c];
				
				///e verifica se ele é válido
				if (sprite == null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
				///e vamos começar a exibi-lo
				sprite.startShowAnimation();										
			}
			
			///e se houver um callback de animação, executa-lo (não ter não é um erro)
			if (OnMyStartShowAnimationCallback!=null)
				OnMyStartShowAnimationCallback.call(this);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * startShowAnimation
		 * minha noção de como ser a animação de exibição desta janela.
		 * @param Function f callback opcional a ser chamado depois que a animação terminar
		 * @comment no começo da animação, só o fundo fica visível. Depois que o fundo esta visível, ai sim surgem os
		 * @comment controles. Não tem problema o parametro ser nulo.
		 * */
		override public function startShowAnimation(f:Function=null):void
		{			
			super.startShowAnimation();
			
			///verifica o estado interno
			if (background==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			OnMyStartShowAnimationCallback=f;			
			background.startShowAnimation();
			OnMyStartShowAnimation();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * OnMyStartHideAnimation
		 * comportamento extra para quando a janela esta para sumir
		 * @comment só depois que os widgets foram escondidos que o fundo some, bem como a janela em sí
		 * */
		private function OnMyStartHideAnimation():void
		{
			///verifica o estado interno
			if (background==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			OnMyStartHideAnimationCallback();
			background.startHideAnimation();			
			super.startHideAnimation();			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * startHideAnimation
		 * animação de esconder para a janela
		 * @param Function f callback a ser chamado quando a janela termina de ser escondida
		 * @comment Dessa vez, queremos esconder primeiro os widgets, e só depois que eles estão escondidos que iremos 
		 * @comment esconder o fundo e todo o resto. Não tem problema se o parametro for nulo.
		 * */
		override public function startHideAnimation(f:Function=null):void		
		{
			OnMyStartHideAnimationCallback=f;
			
			///se os elementos visuais ainda não foram alocados e o programa continuou, é porque algo muito errado
			///aconteceu...
			if (widgets==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			////variavel só usada nela linha de complexidade ciclomática
			var sprite:FadableSprite;
			
			///do primeiro ao penultimo...
			for (var c:int=0;c<widgets.length-1;c++)
			{
				sprite=widgets[c];
				
				///verifica o estado interno
				if (sprite == null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
				
				sprite.startHideAnimation();
			}
			
			///condicionando a chamada do callback ao "escondimento" do ultimo elemento visual
			if (widgets.length()>0)
			{
				sprite=widgets[widgets.length()-1];
				
				///verifica o estado interno
				if (sprite == null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
				
				sprite.startHideAnimation(OnMyStartHideAnimation);
			}
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * callButtonSound 
		 * toca o som de interação padrão
		 */
		public function callButtonSound():void
		{
			if (interaction!=null)
				interaction.play();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setBGOpacity 
		 * @param alpha define a opacidade do fundo
		 * 
		 */
		public function setBGOpacity(alpha:Number):void
		{
			this.background.getChildAt(0).alpha=alpha;
		}
		///--------------------------------------------------------------------------------------------------------------------		
	}
}