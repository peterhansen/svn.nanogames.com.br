package communication
{
	import flash.events.*;
	import flash.external.ExternalInterface;
	
	
	
	public class FuturaInterface
	{
		private var addUser:Function;
		private var removeUser:Function;
		private var newTurn:Function;
		private var answerResponseReceived:Function;
		
		
		public function FuturaInterface(addUserCallback:Function,removeUserCallback:Function, displayUsersList:Function, setMyID:Function, setNewTurn:Function, answerResponseReceivedCallback:Function)
		{
			addUser = addUserCallback;
			removeUser = removeUserCallback;
			newTurn = setNewTurn;
			answerResponseReceived = answerResponseReceivedCallback;
			
			
		
			//Flash calling Javascript function
			setMyID(ExternalInterface.call("getMyID"));
			displayUsersList(ExternalInterface.call("requestUsersList"));
			
			
			//Javascript calling Flash function	
			ExternalInterface.addCallback("joinGame", userJoined);
			ExternalInterface.addCallback("leaveGame", userLeaving);
			ExternalInterface.addCallback("nextTurn", nextTurn);	
			ExternalInterface.addCallback("sendAnswerResponse", sendAnswerResponse);
		}
		
		//Callback function
		private function sendAnswerResponse(response:String):void
		{
			var is_correct:Boolean = Boolean(parseInt(response));
			answerResponseReceived(is_correct);
			
		}
		private function nextTurn(turn:int):void
		{
			newTurn(turn);
		}
		private function userJoined(user:Object):void
		{
			addUser(user);
		}
		private function userLeaving(user_id:String):void
		{
			trace("Callback: " + user_id);
			removeUser(user_id);
		}
		public function getQuestionsFromServer():Object
		{
			return ExternalInterface.call("getQuestion");
		}
		public function askForNextTurn(turn:int):void
		{
			ExternalInterface.call("askForNewTurn", turn);
		}
		
		public function verifyAnswer(answer:String, question_id:String):void
		{
			ExternalInterface.call("getAnswer", answer, question_id);
		}
		
		
	}
}