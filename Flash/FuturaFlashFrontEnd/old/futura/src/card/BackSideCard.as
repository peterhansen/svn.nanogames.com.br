package card
{
	import fl.controls.Button;
	public class BackSideCard extends Card
	{
		private var button:Button;
		public function BackSideCard()
		{
			
			button = new Button();
			button.move(X(),Y());
			button.setSize(WIDTH(), HEIGHT());
			button.label = "Nova carta";
			addChild(button);
		}
	}
}