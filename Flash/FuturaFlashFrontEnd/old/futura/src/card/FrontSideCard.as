package card
{
	import fl.controls.Button;
	import fl.controls.Label;
	import fl.controls.LabelButton;
	import fl.controls.RadioButton;
	import fl.controls.RadioButtonGroup;
	
	import flash.display.DisplayObject;
	import flash.display.Graphics;
	import flash.display.Shape;
	import flash.display.Sprite;

	public class FrontSideCard extends Card
	{
		private var size:uint         = 80;
		private var bgColor:uint      = 0xFFFFFF;
		private var borderColor:uint  = 0x666666;
		private var borderSize:uint   = 0;
		public var answers:RadioButtonGroup;
		private var answer_a:RadioButton;
		private var answer_b:RadioButton;
		private var answer_c:RadioButton;
		private var answer_d:RadioButton;
		private var question_title:Label;
		public var button:Button;

		public function FrontSideCard()
		{
			var card_rect:Shape = new Shape();
			card_rect.graphics.beginFill(bgColor);
			card_rect.graphics.lineStyle(borderSize, borderColor);
			card_rect.graphics.drawRect(X(), Y(), WIDTH(), HEIGHT());
			card_rect.graphics.endFill();
						
			question_title = new Label();
			
						
			answers = new RadioButtonGroup("answers");
			answer_a = new RadioButton();
			answer_b = new RadioButton();
			answer_c = new RadioButton();
			answer_d = new RadioButton();
			
			answer_a.group = answers;
			answer_b.group = answers;
			answer_c.group = answers;
			answer_d.group = answers;
			
			answer_a.value="a";
			answer_b.value="b";
			answer_c.value="c";
			answer_d.value="d";
			
			button = new Button();
			button.setSize(50, 20);
			
			button.label="Responder"
			question_title.text="Questão";
			answer_a.label="Resposta A";
			answer_b.label="Resposta B";
			answer_c.label="Resposta C";
			answer_d.label="Resposta D";
			
			question_title.move(X() + 2,Y() + 10);
			answer_a.move(X() + 2,Y() + 40);
			answer_b.move(X() + 2,Y() + 55);
			answer_c.move(X() + 2,Y() + 70);
			answer_d.move(X() + 2,Y() + 85);
			button.move( ( X() + WIDTH() - ( button.width + 3 ) ), ( Y() + HEIGHT() - ( button.height + 3 ) ) )
			
			addChild(card_rect);
			addChild(question_title);
			addChild(answer_a);
			addChild(answer_b);
			addChild(answer_c);
			addChild(answer_d);
			addChild(button);
		}
		public function setQuestionCard(question:Object):void
		{
			question_title.text = question.title;
			answer_a.label = question.a;
			answer_b.label = question.b;
			answer_c.label = question.c;
			answer_d.label = question.d;
		}
		
	}
}