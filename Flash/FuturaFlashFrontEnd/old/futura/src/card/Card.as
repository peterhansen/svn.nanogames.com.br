package card
{
	import flash.display.Sprite;

	public class Card extends Sprite
	{
		private var _x:Number;
		private var _y:Number;
		private var _width:Number;
		private var _height:Number;
		public function Card()
		{
			_x = Number(210);			
			_y = Number(0);
			_width = Number(100);
			_height = Number(150);
		}
		public function X():Number
		{
			return _x;
		}
		public function Y():Number
		{
			return _y;
		}
		public function WIDTH():Number
		{
			return _width;
		}
		public function HEIGHT():Number
		{
			return _height;
		}
	}
}