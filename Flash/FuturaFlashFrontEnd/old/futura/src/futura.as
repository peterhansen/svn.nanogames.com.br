package
{
	import card.*;
	
	import communication.FuturaInterface;
	
	import flash.display.Sprite;
	import flash.events.*;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	
	public class futura extends Sprite
	{
		
		private var output:TextField;
		private var usersListView:TextField;
		private var timeView:TextField;
		private var back_side_card:BackSideCard;
		private var front_side_card:FrontSideCard;
		private var futura_interface:FuturaInterface;
		private var users:Array;
		private var my_id:String;
		private var my_turn:int;
		private var question_id:String;
		private var game_turn:int;
		
		

		public function futura()
		{
			

			game_turn = 0;
			
			
			front_side_card = new FrontSideCard();
			output = new TextField();
			output.appendText("Messages\n");
			output.border=true;
			output.width=200;
			addChild(output);
			
			usersListView = new TextField();
			usersListView.border=true;
			usersListView.text="UsersList\n";
			usersListView.y=110;
			
			addChild(usersListView);
			
			timeView = new TextField();
			timeView.x=150;
			timeView.y=10;
			
			
			
			// starts the timer ticking
			
			
			back_side_card = new BackSideCard();
			//addChild(back_side_card);
			
			back_side_card.addEventListener(MouseEvent.CLICK, getQuestion);
			front_side_card.button.addEventListener(MouseEvent.CLICK, sendAnswer);
			
			//Setting up communication with javascript
			futura_interface = new FuturaInterface(addUserCallback,removeUserCallback, displayUsersList, setMyID, setNewTurn,answerResponseReceivedCallback);
			my_turn = getTurn(my_id);
			
			
			
			updateDisplay();
			
		}
		
		public function answerResponseReceivedCallback(is_correct:Boolean):void
		{
			if(is_correct)
			{
				output.appendText("Certa Resposta!\n");
			}
			else
			{
				output.appendText("Errada Resposta!\n");
			}
		}
		public function setNewTurn(turn:int):void
		{
			game_turn = turn;
			updateDisplay();
		}
		private function updateDisplay():void
		{
			if(my_turn == game_turn)
			{
				addChild(back_side_card);
				addChild(timeView);
							
			}
			else
			{
				if(back_side_card.parent != null)
				{
					removeChild(back_side_card);
				}
				if(front_side_card.parent != null)
				{
					removeChild(front_side_card);
				}
				if(timeView.parent != null)
				{
					removeChild(timeView);
				}
			}
			output.appendText("Turno do:" + users[game_turn].nickname + "\n");
		}
		private function getTurn(id:String):int
		{
			var count:int;
			var turn:int;
			for each(var user:Object in users)
			{
				if(user.id == id)
				{
					turn = count;	
				}
				count = count + 1;
			}
			return turn;
		}
		public function addUserCallback(user:Object):void
		{
			
			
			if(hasUserInGame(users, user))
			{
				
				trace('Usurario: ' + user.nickname + ' já estava na sala!');
			}
			else
			{
				users.push(user);
				output.appendText(user.nickname + ' entrou no jogo!\n');
				usersListView.appendText(user.nickname + "\n");
				trace('Usurario: ' + user.nickname + ' acaba de entrar na sala!');
			}
		}
		public function removeUserCallback(user_id:String):void
		{
			var users_list:Array;
			users_list = new Array();
			trace('Parametro: ' + user_id);
			for each(var user:Object in users)
			{
				trace('Array: ' + user_id);
				if(user.id != user_id)
				{
					users_list.push(user);
				}
				else
				{
					output.appendText(user.nickname + " saiu do jogo!\n");
				}
			}
			users = users_list;
			my_turn = getTurn(my_id);
			displayUsersList(users);
			futura_interface.askForNextTurn(game_turn);
			
		}
		public function displayUsersList(users_list:Array):void
		{
			usersListView.text="UsersList\n"
			users = users_list;
			for each(var user:Object in users)
			{
				var identify_me:String;
				if(user.id == my_id)
				{
					identify_me = "+ ";
				}
				else
				{
					identify_me = "";
				}
				
				usersListView.appendText(identify_me + user.nickname + "\n");
				trace(user.nickname);	
			}
			
		}
		public function setMyID(id:String):void
		{
			my_id = id;
		}
		private function hasUserInGame(users_list:Array, user:Object):Boolean
		{
			var has:Boolean = false;
			for each(var user_item:Object in users_list)
			{
				if(user_item.id == user.id)
				{
					has = true;
				}
			}
			return has
		}
		public function getQuestion(e:Event):void
		{
			if(back_side_card.parent != null)
			{
				removeChild(back_side_card);	
			}
			
			var question:Object = futura_interface.getQuestionsFromServer();
			question_id = question.id;
			front_side_card.setQuestionCard(question);
			addChild(front_side_card);
		}
		public function sendAnswer(e:Event):void
		{
			
			
			futura_interface.verifyAnswer(front_side_card.answers.selectedData.toString(), question_id);
			
			removeChild(front_side_card);
			
			futura_interface.askForNextTurn(game_turn);
			
		}
		
	}
	
	
}

