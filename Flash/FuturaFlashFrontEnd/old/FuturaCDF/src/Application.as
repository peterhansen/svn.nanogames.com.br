package 
{
	///flash
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * FuturaDriver
	 * Stub de aplicação do jogo
	 * @author Daniel
	 * */
	[SWF( width= "530", height = "410", backgroundColor = "0x000000", frameRate = "20" )]	
	public class Application extends Sprite
	{	
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * instância da mesa de jogo
		 */
		private var futuraTable:FuturaTable;
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * construtor
		 * */
		public function Application()
		{
			super();
			
			///aloca a mesa de jogo
			futuraTable=new FuturaTable();
			
			if (futuraTable==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			///inicializa e inicia o jogo
			addChild(futuraTable);
			addEventListener(Event.ADDED_TO_STAGE,addedToStage);
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * addedToStage
		 * para evitar problemas de stage nulo 
		 * @param monteiro
		 * @return 
		 * 
		 */
		public function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE,addedToStage);
			futuraTable.initTable();
			futuraTable.startNewGame();
		}
		///--------------------------------------------------------------------------------------------------------------------				
	}
}