package FuturaGameEngine
{	
	/**
	 * FuturaPlayer
	 * representação local de todos os jogadores
	 * @author Daniel Monteiro 
	 * */
	public class FuturaPlayer
	{
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * nome de exibição do jogador
		 */
		private var name:String;		
		/**
		 * ID externo do jogador
		 */
		private var myFuturaId:String;		
		/**
		 * indice da cor do jogador
		 */
		private var colorIndex:int;		
		/**
		 * quantos acertos
		 */
		private var hits:int;		
		/**
		 * quantos erros
		 */
		private var errors:int;
		
		/**
		 * ainda em jogo? 
		 */
		private var active:Boolean;		
		/**
		 * quantas penalidades o jogador sofreu 
		 */
		private var penalties:int;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * construtor
		 * */
		public function FuturaPlayer()
		{
			///inicializações triviais
			myFuturaId=""
			colorIndex=0;
			hits=0;
			errors=0;
			penalties=0;
			active=false;
			
			///definições temporárias
			setName("jogador Futura");
			setColorIndex(FuturaConstants.COLORBLUEINDEX);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getScore
		 * obtem o score do jogador
		 * @comment calculo temporário
		 * @returns int O score neste momento
		 * */
		public function getScore():int
		{
			return (hits-penalties)* FuturaConstants.POINTSPERRIGHTANSWER;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getActive
		 * diz se o jogador ainda esta em jogo ou não
		 * @returns Boolean se o jogador ainda estiver em jogo, retorna-rá true, senão, false.
		 * */
		public function getActive():Boolean
		{
			return active;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setActive
		 * @param Boolean s novo estado do jogador
		 * @comment não precisa de checagem de parametro, porque Boolean nunca poderá ser nada além de true ou false
		 * @comment no entanto, se o mesmo jogador for notificado duas vezes, isso pode significar um erro de coerencia
		 * @comment do jogo, pois pode-se estar tentando notificar o jogador errado
		 * */
		public function setActive(s:Boolean):void
		{
			///checagem de parametros
			if (s==active)
				FuturaSafeGuard.nonFatalError(FuturaStrings.INTERNAL_ERROR);
			
			active=s;
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getFuturaId
		 * obtem o Id do jogoador no servidor mestre de jogo
		 * @returns String Id externa do jogador
		 * */
		public function getFuturaId():String
		{
			return myFuturaId;			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setFuturaId
		 * faz a ligação entre a representação local do jogador e o jogador no servidor
		 * @param String id externo do jogador no servidor 
		 * */
		public function setFuturaId(id:String):void
		{
			///checagem de validade de parametros
			if (id==null || id=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			myFuturaId=id;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerDidHit
		 * sinaliza que o jogador acertou uma pergunta
		 * */
		public function playerDidHit():void
		{
			hits++;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerDidError
		 * indica que o jogador errou uma pergunta
		 * */
		public function playerDidError():void
		{
			errors++;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getHits
		 * obtem quantos acertos o jogador teve
		 * @returns int quantas perguntas ele acertou
		 * */
		public function getHits():int
		{
			return hits;
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function addPenalty():void
		{
			this.penalties++;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getErrors
		 * quantos items o jogador erro
		 * @returns int quantidade de erros
		 * */
		public function getErrors():int
		{
			return errors;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getName 
		 * @return o nome de exibição do jogador
		 * 
		 */
		public function getName():String
		{
			return name;		
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setName
		 * define nome de exibição do jogador
		 * @param String name nome recebido do servidor
		 * */
		public function setName(Name:String):void
		{
			///checagem de validade de parametros
			if (Name==null || Name=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			name=Name;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setColorIndex
		 * define qual vai ser o indice da cor do jogador - isso é suficiente para atribuir uma cor a ele
		 * @param int c Qual é a cor do jogador
		 * */
		public function setColorIndex(c:int):void
		{
			///checagem de validade de parametros
			if (c< FuturaConstants.COLORGREENINDEX || c > FuturaConstants.COLORREDINDEX)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			colorIndex=c;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getColorIndex
		 * obtem qual é o indice de cor do usuário - é justamente aqui que se faz a coloração do jogador
		 * @returns int indice de cor do usuário
		 * */
		public function getColorIndex():int
		{
			return colorIndex;
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}