package FuturaExternalInterface
{
	//flash
	import flash.events.*;
	import flash.external.ExternalInterface;
	
	/**
	 * FuturaInterface
	 * comunicação com o servidor do Futura CDF
	 * @author Yuri Mello
	 * @comment modificado por Daniel Monteiro
	 * */
	public class FuturaInterface extends FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 *callback para quando usuário é adicionado 
		 */
		private var addUser:Function;
		/**
		 * callback para quando usuário é removido
		 */
		private var removeUser:Function;
		/**
		 * callback para iniciar o novo turno
		 */
		private var newTurn:Function;
		/**
		 * callback para resposta à reposta do usuário
		 */
		private var answerResponseReceived:Function;
		/**
		 * callback para obter a lista de jogadores
		 */
		private var userList:Function;
		/**
		 * callback para identificar o jogador local
		 */
		private var setLocalId:Function		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * FuturaInterface
		 * @param Function addUser função a ser chamada quando um novo usuário é adicionado
		 * @param Function removeUser função a ser chamada quando um usuário é removido
		 * @param Function takeUserList função a ser chamada para receber a lista de usuários
		 * @param Function setMyID função para avisar que quem sou eu na lista de usuários
		 * @param Function setNewTurn função para sincronizar as jogadas
		 * @param Function answerResponseReceived função para reportar se a resposta dada pelo jogador é certo ou errado
		 * */
		public function FuturaInterface(addUserCallback:Function,removeUserCallback:Function, takeUserList:Function, setMyID:Function, setNewTurn:Function, answerResponseReceivedCallback:Function)
		{
			super(addUser,removeUser,takeUserList,setMyID,setNewTurn,answerResponseReceived);
			///checagem dos parametros
			
			if (ExternalInterface==null ||addUserCallback==null || removeUserCallback==null || takeUserList==null || setMyID==null || setNewTurn==null || answerResponseReceivedCallback==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///preenche os callbacks
			userList=takeUserList;
			setLocalId=setMyID;
			addUser = addUserCallback;
			removeUser = removeUserCallback;
			newTurn = setNewTurn;
			answerResponseReceived = answerResponseReceivedCallback;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * call
		 * @param cmd comando a ser executado pelo servidor
		 * 
		 */
		override public function call(cmd:String):Object
		{
			if (ExternalInterface == null || cmd == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);	
			
			return ExternalInterface.call(cmd);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		override public function init():void
		{
			if (ExternalInterface==null ||userJoined==null || userLeaving==null || nextTurn==null || sendAnswerResponse==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///Flash -> Javascript
			userList(ExternalInterface.call("requestUsersList"));
			setLocalId(ExternalInterface.call("getMyID"));
			
			///Javascript <- Flash	
			ExternalInterface.addCallback("joinGame", userJoined);
			ExternalInterface.addCallback("leaveGame", userLeaving);
			ExternalInterface.addCallback("nextTurn", nextTurn);	
			ExternalInterface.addCallback("sendAnswerResponse", sendAnswerResponse);		
			askForNextTurn(0);
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * sendAnswerResponse
		 * obtem se a resposta do usuário é correta ou não
		 * @param String response A resposta do usuário
		 * */
		private function sendAnswerResponse(response:String, answer:String):void
		{
			///checagem dos parametros
			if (response==null || response=="" || response==" ")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///variáveis
			var isCorrect:Boolean;
			
			///processamento
			isCorrect= Boolean(parseInt(response));
			answerResponseReceived(isCorrect,answer);			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * nextTurn
		 * inicia novo turno
		 * @param int turn Em qual turno estamos
		 * */
		private function nextTurn(turn:int):void
		{
			///checagem de validade dos parametros
			if (turn < 0 )
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			newTurn(turn);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * userJoined
		 * adiciona um usuário ao jogo
		 * @param Object user dados do usuário recem adicionado ao jogo 
		 * @author Yuri Mello
		 * */
		private function userJoined(user:Object):void
		{
			///checagem de validade dos parametros
			if (user==null || user.nickname==null || user.nickname=="" || user.id==null || user.id=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			addUser(user);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * userLeaving
		 * aviso de que um usuário saiu do jogo
		 * @param String userId Id externo do jogador a ser retirado
		 * */
		private function userLeaving(userId:String):void
		{
			///checagem dos parametros
			if (userId==null || userId=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			removeUser(userId);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * @returns Object a pergunta vinda do servidor
		 * @comment DM: provavelmente bloqueante, certo? Isso pode ser problemático para ser usado em flash
		 * */
		override public function getQuestionsFromServer():Object
		{
			///variáveis
			var obj:Object;
			
			///obtem a pergunta o servidor
			obj=ExternalInterface.call("getQuestion");
			
			///verifica se o objeto é válido ou não
			if (obj==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///tudo certo, o objeto pode ser enviado
			return obj;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * askForNextTurn
		 * Pede para que se comece um novo turno
		 * @param int turn turno a ser começado
		 * */
		override public function askForNextTurn(turn:int):void
		{
			///checagem de validade dos parametros
			if (turn < 0 )
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			ExternalInterface.call("askForNewTurn", turn);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * verifyAnswer
		 *
		 * @param String answer a resposta do usuário [a..d | - ]
		 * @param String questionId Id externo da pergunta
		 * 
		 * */
		override public function verifyAnswer(answer:String, questionId:String):void
		{
			///checagem dos parametros
			if (answer==null || answer=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			if (questionId==null || questionId=="")
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			ExternalInterface.call("getAnswer", answer, questionId);
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}