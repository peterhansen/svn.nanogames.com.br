package FuturaExternalInterface
{
	/**
	 * FuturaBaseInterface
	 * base abstrata de comunicação com servidor
	 * @author Daniel Monteiro
	 * */
	public class FuturaMockInterface extends FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * callback de iniciar novo turno
		 */
		private var setNewTurnCallback:Function;
		
		/**
		 * callback para responder se a resposta estava correta
		 */
		private var answerResponseReceivedCallback:Function;
		
		/**
		 * uma variável apenas pra saber qual deve ser a próxima resposta a ser dada ao sistema
		 */
		private var lastAnswer:Boolean;
		
		/**
		 * callback que passa a lista de usuários pro jogo
		 */
		private var userList:Function;
		
		/**
		 * callback que avisa quem é o jogador local
		 */
		private var whoAmI:Function;
		
		/**
		 * callback que pede um novo turno
		 */
		private var drumTick:Function;
		
		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * FuturaMockInterface
		 * construtor que recebe callbacks
		 * @param Function addUser função a ser chamada quando um novo usuário é adicionado
		 * @param Function removeUser função a ser chamada quando um usuário é removido
		 * @param Function takeUserList função a ser chamada para receber a lista de usuários
		 * @param Function setMyID função para avisar que quem sou eu na lista de usuários
		 * @param Function setNewTurn função para sincronizar as jogadas
		 * @param Function answerResponseReceived função para reportar se a resposta dada pelo jogador é certo ou errado
		 * @comment baseada na interface criada por Yuri Mello
		 * */
		public function FuturaMockInterface(addUser:Function,removeUser:Function, takeUserList:Function, setMyID:Function, setNewTurn:Function, answerResponseReceived:Function):void
		{	
			super(addUser,removeUser,takeUserList,setMyID,setNewTurn,answerResponseReceived);
			
			lastAnswer=true;
			drumTick=setNewTurn;
			answerResponseReceivedCallback=answerResponseReceived;
			setNewTurnCallback=setNewTurn;
			userList=takeUserList;		
			whoAmI=setMyID;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		override public function init():void
		{
			sendUserList();
			sendLocalUserNotify();
			startTurn();				
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * @returns Object a pergunta
		 * */
		override public function getQuestionsFromServer():Object
		{				
			/// variáveis usadas pela função
			var pergunta:Object;
			
			///criação do objeto de pergunta
			pergunta=new Object();
			
			///verificando se não conseguiu alocar o objeto
			if (pergunta==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			///preenche o objeto
			pergunta.title="O Sony Ericsson XPeria X10 Mini da Maria Chuchu das graças é Android de verdade? Desconfio que ele seja um Android paraguaio...";
			pergunta.a="não...Pode ser qualquer coisa e eu poderia deliberar por horas a fio a sua simples razão de existência, mas não é o caso para tal";
			pergunta.b="sim";
			pergunta.c="talvez?";
			pergunta.d="e o MotoBlur por acaso é? Alguem precisa avisar praquele povo que o Vanilla Android é mais que o suficiente para satisfazer todas as nossas vontades";
			pergunta.id="437";
			
			return pergunta;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * askForNextTurn
		 * pede ao servidor para sinalizar um novo começo de turno
		 * @param int turn o turno atual
		 * */
		override public function askForNextTurn(turn:int):void
		{
			if (setNewTurnCallback!=null)
				setNewTurnCallback(1);
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * verifyAnswer
		 * verifica se uma resposta a uma dada pergunta esta correta. Apenas pede a mensagem.
		 * @param String answer a resposta
		 * @param String questionId a pergunta
		 * */
		override public function verifyAnswer(answer:String, questionId:String):void
		{
			///inverte a ultima resposta
			lastAnswer=!lastAnswer;
			
			if (answerResponseReceivedCallback!=null)
				answerResponseReceivedCallback(lastAnswer,"a");
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		}	
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * sendUserList 
		 * Pede lista de usuários
		 */
		private function sendUserList():void
		{
			trace("FuturaMockInterface::sendUserList()");				
			var a:Array=new Array();
			var user:Object;
			
			user=new Object();
			user.id=1;
			user.nickname="joão1";				
			a.push(user);
			
			user=new Object();
			user.id=2;
			user.nickname="joão2";				
			a.push(user);
			
			user=new Object();
			user.id=3;
			user.nickname="joão3";				
			a.push(user);
			
			user=new Object();
			user.id=4;
			user.nickname="joão4";				
			a.push(user);
			
			if (userList!=null)
				userList(a);
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function sendLocalUserNotify():void
		{
			trace("FuturaMockInterface::sendLocalUserNotify()");
			if (whoAmI!=null)
				whoAmI("1");
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function startTurn():void
		{
			trace("FuturaMockInterface::startTurn()");				
			if (drumTick!=null)
				drumTick(1);
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);					
		}
		///--------------------------------------------------------------------------------------------------------------------			
	}
}