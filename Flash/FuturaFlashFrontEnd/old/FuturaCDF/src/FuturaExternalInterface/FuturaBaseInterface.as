package FuturaExternalInterface
{
	///som
	/**
	 * som de interação com o botão 
	 */
	import botao.mp3;
	
	/**
	 * FuturaBaseInterface
	 * base abstrata de comunicação com servidor
	 * @author Daniel Monteiro
	 * */
	public class FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * FuturaBaseInterface
		 * construtor que recebe callbacks
		 * @param Function addUser função a ser chamada quando um novo usuário é adicionado
		 * @param Function removeUser função a ser chamada quando um usuário é removido
		 * @param Function takeUserList função a ser chamada para receber a lista de usuários
		 * @param Function setMyID função para avisar que quem sou eu na lista de usuários
		 * @param Function setNewTurn função para sincronizar as jogadas
		 * @param Function answerResponseReceived função para reportar se a resposta dada pelo jogador é certo ou errado
		 * @comment baseada na interface criada por Yuri Mello
		 * */
		public function FuturaBaseInterface(addUser:Function,removeUser:Function, takeUserList:Function, setMyID:Function, setNewTurn:Function, answerResponseReceived:Function):void
		{		
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		public function init():void
		{
			
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * @returns Object a pergunta
		 * */
		public function getQuestionsFromServer():Object
		{
			return null;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * askForNextTurn
		 * pede ao servidor para sinalizar um novo começo de turno
		 * @param int turn o turno atual
		 * */
		public function askForNextTurn(turn:int):void
		{
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * verifyAnswer
		 * verifica se uma resposta a uma dada pergunta esta correta. Apenas pede a mensagem.
		 * @param String answer a resposta
		 * @param String questionId a pergunta
		 * */
		public function verifyAnswer(answer:String, questionId:String):void
		{
		}	
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * Chama rotina no servior 
		 * @param cmd O comenado a ser executada
		 * @return  retorno da função, se houver.
		 * 
		 */
		public function call(cmd:String):Object
		{
			return null;	
		}
		///--------------------------------------------------------------------------------------------------------------------	 
	}
}