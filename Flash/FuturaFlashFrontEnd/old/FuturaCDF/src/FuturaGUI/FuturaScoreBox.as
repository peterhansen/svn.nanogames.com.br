package FuturaGUI
{
	///GUILib
	import GUILib.FadableSprite;
	
	/**
	 * FuturaScoreBox
	 * caixa de placar
	 * @author Daniel Monteiro
	 * */
	public class FuturaScoreBox extends FadableSprite
	{
		/**
		 * caixa de score
		 */
		private var pointsBox:PointsBox;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * FuturaScoreBox
		 * construtor
		 * */
		public function FuturaScoreBox()
		{
			super();
			pointsBox=new PointsBox();
			addChild(pointsBox);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setDisplayItems
		 * define os textos a serem exibidos na caixa de scores
		 * @param int hits acertos
		 * @param int misses erros
		 * @param int overall score final
		 * */
		public function setDisplayItems(hits:int, misses:int, overall:int):void
		{
			pointsBox.Points.text= overall.toString() + " " + FuturaStrings.POINTS_TEXT  ;
			pointsBox.Hits.text= hits.toString() + " "+ FuturaStrings.HITS_TEXT  ;
			pointsBox.Errors.text= misses.toString()+ " "+ FuturaStrings.MISSES_TEXT  ;		
		}
		///--------------------------------------------------------------------------------------------------------------------		
	}
}