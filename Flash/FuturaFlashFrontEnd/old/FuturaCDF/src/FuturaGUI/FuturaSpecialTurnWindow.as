package FuturaGUI
{
	/**
	 * FuturaSpecialTurnWindow
	 * Janela para quando acontece uma jogada especial
	 * @author Daniel Monteiro
	 */
	public class FuturaSpecialTurnWindow extends FuturaMessageWindow
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * imagem para carta de penalidade
		 */
		private var sad:Penalty;
		/**
		 * imagem para carta de bonus
		 */
		private var gift:Bonus;
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * construtor
		 * apenas define o callback da classe mãe
		 */
		public function FuturaSpecialTurnWindow()
		{
			super();
			this.setButtonCallback(myOk);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setHappyIcon
		 * Define que a unica carta visível deve ser carta de bonus
		 */
		public function setHappyIcon():void
		{
			if (gift==null) return;
			if (sad==null) return;
			
			gift.visible=true;
			sad.visible=false;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setSadIcon
		 * Define que a unica carta visível deve ser carta de penalidade
		 */
		public function setSadIcon():void
		{
			if (gift==null) return;
			if (sad==null) return;
			
			gift.visible=false;
			sad.visible=true;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * myOk
		 * Meu comportamento personalizado de quando aperto o botão de OK
		 * (toca som e fecha)
		 */
		private function myOk():void
		{
			this.callButtonSound();
			if (super.getMyDismissCallback()!=null)
				(super.getMyDismissCallback())();
			setInvisible();
		}
		///--------------------------------------------------------------------------------------------------------------------	
		/**
		 * initWindow
		 * cria os icones e os posiciona
		 */
		override public function initWindow():void
		{
			super.initWindow();
			
			sad=new Penalty();
			gift=new Bonus();
			
			addChild(gift);			
			addChild(sad);
			sad.cacheAsBitmap=true;
			gift.cacheAsBitmap=true;
			gift.visible=false;
			sad.visible=false;
			
			sad.y =90;		
			sad.y+=sad.height/2;
			sad.x=width-sad.width-25;		
			
			gift.y =90;
			gift.y+=gift.height/2;
			gift.x=width-gift.width;
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}