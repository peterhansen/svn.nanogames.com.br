package FuturaGUI
{
	///flash
	import FuturaGameEngine.FuturaPlayer;
	
	import GUILib.ClickableCheckBox;
	import GUILib.ClickableFadableSprite;
	import GUILib.FadableSprite;
	import GUILib.FadableText;
	import GUILib.HorizontalLineSeparatorSprite;
	import GUILib.SpriteAndTextSprite;
	import GUILib.SpriteHorizontalList;
	import GUILib.SpriteVerticalList;
	
	import flash.display.Sprite;
	import flash.text.TextFormat;
	
	/**
	 * FuturaScoreWindow
	 * janela de scores finais
	 * @author Daniel Monteiro
	 * */
	public class FuturaScoreWindow extends FuturaBaseWindow
	{
		/**
		 * botão para fechar a janela (e o jogo)
		 */
		private var buttonSprite:ClickableFadableSprite;
		/**
		 * lista de nomes dos jogadores
		 */
		private var namesList:SpriteVerticalList;
		/**
		 * lista de conteúdo da janela
		 */
		private var listPanes:SpriteVerticalList;
		/**
		 * qual desses é o jogador
		 */
		private var myId:int;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setOkCallback
		 * define o callback de "fechar a janela"
		 * @param Function f callback para quando se clica em Ok
		 * */		
		public function setOkCallback(f:Function):void
		{
			if (buttonSprite==null || f==null || namesList==null )
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			buttonSprite.setOnMouseClickCallback(f);			
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * updatePlayerList
		 * atualiza a lista visível de jogadores
		 * @author Daniel Monteiro
		 * @param playerList lista de jogadores (instancias de FuturaPlayer)
		 * */		
		public function updatePlayerList(playerList:Array):void
		{			
			trace("updatePlayerList");
			
			///verifica a validade do parametro enviado
			if (playerList==null || playerList.length==0 /*|| myId <0*/)			
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			if (namesList == null)
				return;
			
			
			
			///variáveis usadas pela função
			var player:FuturaPlayer;
			
			///qual é o maior dos scores não selecionados ainda?
			var tmp:int=0;
			
			///lista dos que ja foram selecionados
			var pos:Array=new Array();
			
			///inicializada de forma que nem deles ja tenha sido selecionado
			for (var y:int=0;y<playerList.length;y++)
			{
				player=playerList[y];
				
				if (player==null || !(player is FuturaPlayer))
					FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
				
				if (player.getActive())
					pos.push(y);
			}
			
			///ye ol'bubblesort
			//eu até prefiro heapsoft, mas aqui não iria ser mais rápido, nem mais prático de implementar.
			for (var z:int=0;z<pos.length;z++)
			{
				player=playerList[pos[z]];
				
				for (var c:int=z+1;c<pos.length;c++)
				{
					if (player.getScore() < playerList[pos[c]].getScore())
					{
						///troca
						tmp=pos[z];
						pos[z]=pos[c];
						pos[c]=tmp;
						c=pos.length;
					}
				}
			}
			
			for (var v:int=0;v<pos.length;v++)
			{
				player=playerList[pos[v]];
				addPlayer(player.getName(), player.getScore());
			}
			
			namesList = null;
		}

		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * removePlayer
		 * remove da lista um jogador especifico 
		 * @param p id do jogador a ser removido
		 * 
		 */
		public function removePlayer(p:int):void
		{
			namesList.getItemAt(p).visible=false;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * addPlayer
		 * adiciona um novo jogador na lista 
		 * @param Name nome do jogador
		 * @param Score placar do jogador
		 */
		public function addPlayer(Name:String, Score:int):void
		{
			trace ("addPlayer:"+Name);
			///variaveis
			var name:SpriteHorizontalList;
			var status:Status			
			var statusSprite:FadableSprite;
			var fadableText:FadableText;
			
			///alocações
			name=new SpriteHorizontalList();
			if (name==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			statusSprite=new FadableSprite();
			if (statusSprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			status=new Status();
			if (status==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///configurações dos items
			status.y+=status.height/2;				
			status.gotoAndStop("Out");
			statusSprite.addChild(status);	
			name.addItem(statusSprite);
			
			///nome
			fadableText=new FadableText(Name+" : ",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (fadableText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			name.addItem(fadableText,5);
			
			///placar
			fadableText=new FadableText(Score.toString(),FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (fadableText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			name.addItem(fadableText);
			
			///por fim, se deu tudo certo, adiciona o nome
			namesList.addItem(name);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * @param p
		 * 
		 */
		public function setCurrentPlayer(p:int):void
		{
			var name:SpriteHorizontalList;
			var scoreText:FadableText;
			
			name =	namesList.getItemAt(p) as SpriteHorizontalList;			
			scoreText = name. getItemAt(1) as FadableText;			
			scoreText.setText("*"+scoreText.getText());
		}
		///--------------------------------------------------------------------------------------------------------------------
		//		/**
		//		 * 
		//		 * @param p
		//		 * @param s
		//		 * 
		//		 */
		//		public function setPlayerScore(p:int,s:int):void
		//		{
		//			///checagem de parametros
		//			if (p < 0 || s < 0)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);				
		//			
		//			///variaveis
		//			var name:SpriteHorizontalList;
		//			var status:Status;				
		//			var statusSprite:FadableSprite;			
		//			var scoreText:FadableText;
		//			
		//			name =	namesList.getItemAt(p) as SpriteHorizontalList;		
		//			if (name == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			statusSprite = name.getItemAt(0);
		//			if (statusSprite == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			scoreText = name. getItemAt(2) as FadableText;
		//			if (scoreText == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			status = statusSprite.getChildAt(0) as Status;
		//			if (status == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			status.gotoAndStop("Ready");			
		//			scoreText.setText(s.toString());			
		//		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * 
		 */
		public function FuturaScoreWindow()
		{
			super();
			
			myId=-1;
			buttonSprite=new ClickableFadableSprite();			
			namesList=new SpriteVerticalList();			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * 
		 */
		override public function initWindow():void
		{
			super.initWindow();
			
			var separator:HorizontalLineSeparatorSprite;
			listPanes=new SpriteVerticalList();
			
			this.addWidget(listPanes);
			
			///<formatos de texto>
			var titleTextFormat:TextFormat=new TextFormat();
			var regularTextFormat:TextFormat=new TextFormat();
			var countTextFormat:TextFormat=new TextFormat();
			
			titleTextFormat.size=22;
			regularTextFormat.size=12;
			countTextFormat.size=18;			
			
			
			titleTextFormat.font="Arial";
			regularTextFormat.font="Arial";
			countTextFormat.font="Arial";
			
			countTextFormat.bold=true;			
			
			var white:uint=0xFFFFFF;
			///</formatos de texto>
			
			
			///<titulo janela>
			var titleText:FadableText=new FadableText("Fim de jogo",white,titleTextFormat);
			separator=new HorizontalLineSeparatorSprite(0,width-60-65);
			listPanes.addItem(titleText);
			listPanes.addItem(separator,7);		
			listPanes.x=65;
			listPanes.y=50;
			///<titulo janela>
			
			///<contagem regressiva>
			
			var countDescText:FadableText=new FadableText("Resultados finais desta mesa:",white,regularTextFormat);
			
			
			listPanes.addItem(countDescText,25);
			///<contagem regressiva>			
			
			
			///<lista de nomes>
			listPanes.addItem(namesList,20);						
			///</lista de nomes>
			
			///<botao>
			var button:OkButton=new OkButton();			
			buttonSprite.addChild(button);			
			listPanes.addItem(buttonSprite);
			buttonSprite.y=height-30-button.height/2-listPanes.y;
			buttonSprite.x=(width-2*button.width)/2;
			///</botao>
			
		}	
		///--------------------------------------------------------------------------------------------------------------------		
	}
}