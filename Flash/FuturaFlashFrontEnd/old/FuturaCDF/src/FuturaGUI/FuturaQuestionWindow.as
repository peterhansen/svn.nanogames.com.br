package FuturaGUI
{
	///flash
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	///GUILib
	import GUILib.ClickableCheckBox;
	import GUILib.ClickableFadableSprite;
	import GUILib.FadableSprite;
	import GUILib.FadableText;
	import GUILib.GUIConstants;
	import GUILib.HorizontalLineSeparatorSprite;
	import GUILib.SpriteAndTextSprite;
	import GUILib.SpriteVerticalList;
	
	///sons
	import resposta_correta.mp3;
	import resposta_errada.mp3;
	
	/**
	 * FuturaQuestionWindow
	 * Janela de perguntas
	 * @author Daniel Monteiro
	 * */
	public class FuturaQuestionWindow extends FuturaBaseWindow
	{
		/**
		 * texto da pergunta 
		 */
		private var questionText:FadableText;
		
		/**
		 * lista de opções de resposta (precisa estar no escopo da classe, para que se possa reposicionar os items) 
		 */
		private var listOptions:SpriteVerticalList;
		
		/**
		 * timer de tempo de resposta (isso deve mudar) 
		 */
		private var timeOutTimer:Timer;
		
		/**
		 * tempo para os outros verem a minha resposta (e vice-e-versa) 
		 */
		private var showAnswerTimer:Timer;
		
		/**
		 * relógio de animação de tempo restante 
		 */
		private var clock:Clock;
		
		/**
		 * função para indicar que a pergunta foi respondida 
		 */
		private var answeredCallback:Function;
		
		/**
		 * qual foi a resposta 
		 */
		private var answer:int;
		
		/**
		 * checkbox da 1º pergunta 
		 */
		private var check1:ClickableCheckBox;
		
		/**
		 * checkbox da 2º pergunta 
		 */
		private var check2:ClickableCheckBox;
		
		/**
		 * checkbox da 3º pergunta 
		 */
		private var check3:ClickableCheckBox;
		
		/**
		 * checkbox da 4º pergunta 
		 */
		private var check4:ClickableCheckBox;		
		
		/**
		 * item (texto + checkbox) da 1º pergunta 
		 */
		private var sprite1:SpriteAndTextSprite;
		
		/**
		 * item (texto + checkbox) da 2º pergunta 
		 */
		private var sprite2:SpriteAndTextSprite;
		
		/**
		 * item (texto + checkbox) da 3º pergunta 
		 */
		private var sprite3:SpriteAndTextSprite;
		
		/**
		 * item (texto + checkbox) da 4º pergunta 
		 */
		private var sprite4:SpriteAndTextSprite;
		
		/**
		 * sou eu que vou responder? 
		 */
		private var amIAnswering:Boolean;
		
		/**
		 * botão de responders 
		 */
		private var buttonSprite:ClickableFadableSprite;
		
		/**
		 * É minha vez? 
		 */
		private var isItMyTurn:Boolean;
		
		/**
		 * tempo para cada resposta
		 */
		private var speed:int;
		
		/**
		 * texto de título
		 */
		private var titleText:FadableText;
		
		/**
		 * timer para tickar o relógio (tick, tock..)
		 */
		private var clockTickTimer:Timer;
		
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * checkIntegrityFor
		 * centraliza as verificações de erro, de modo a poupar trabalho e garantir uma melhor cobertura contra
		 * erros
		 * @param String m 
		 * */
		public function checkIntegrityFor(forWhat:String):void
		{
			if (questionText == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (listOptions == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (clock == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			/*
			if (answeredCallback == null)
			FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			*/
			
			if (showAnswerTimer == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (check1 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (check2 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (check3 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (check4 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (sprite1 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (sprite2 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (sprite3 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
			if (sprite4 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			
		}		
		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setGameSpeed 
		 * @param time Qual vai ser a velocidade do jogo
		 * 
		 */
		public function setGameSpeed(time:int=FuturaConstants.TIMENORMAL):void
		{
			this.speed=time;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getAnswer
		 * retorna a reposta do usuário para a ultima pergunta
		 * @returns int a dada pelo usuário [0..5]
		 * @comment 5 é dado quando o jogador não responde nenhuma, ou não responde a tempo
		 * */
		public function getAnswer():int
		{
			checkIntegrityFor("getAnswer");
			
			return answer;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check1Clicked
		 * 1º checkbox foi clicada
		 * */
		private function check1Clicked():void
		{
			checkIntegrityFor("check1Clicked");
			
			if (!isItMyTurn)
			{
				check1.setState(false);
				return
			}
			
			if (check1.getState())
			{
				answer=0;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}
			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}
			
			///marca uma exclusivamente
			check2.setState(false);				
			check3.setState(false);
			check4.setState(false);	
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check2Clicked
		 * 2º checkbox foi clicada
		 * */		
		private function check2Clicked():void
		{		
			checkIntegrityFor("check2Clicked");
			
			if (!isItMyTurn)
			{
				check2.setState(false);
				return
			}
			
			
			if (check2.getState())
			{
				answer=1;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}

			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}

			
			///marca uma exclusivamente
			check1.setState(false);
			check3.setState(false);
			check4.setState(false);	
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check3Clicked
		 * 3º checkbox foi clicada
		 * */		
		private function check3Clicked():void
		{
			checkIntegrityFor("check3Clicked");
			
			if (!isItMyTurn)
			{
				check3.setState(false);
				return
			}
			
			
			if (check3.getState())
			{
				answer=2;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}

			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}

			
			///marca uma exclusivamente
			check1.setState(false);
			check2.setState(false);
			check4.setState(false);	
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check4Clicked
		 * 4º checkbox foi clicada
		 * */
		private function check4Clicked():void
		{
			checkIntegrityFor("check4Clicked");			
			
			if (!isItMyTurn)
			{
				check4.setState(false);
				return
			}
			
			
			if (check4.getState())
			{
				answer=3;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}

			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}

			
			///marca uma exclusivamente
			check1.setState(false);
			check2.setState(false);
			check3.setState(false);	
			
		}
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setDisplayedAnswer 
		 * @param ans qual deve se a resposta a ser exibida como marcada
		 * 
		 */
		public function setDisplayedAnswer(ans:int	):void
		{
			stopTimer();
			
			check1.setState(false);
			check2.setState(false);
			check3.setState(false);
			check4.setState(false);
			
			switch (ans)
			{
				case 0:
					check1.setVisible();
					check1.setState(true);
					break;
				case 1:
					check2.setVisible();
					check2.setState(true);
					break;
				case 2:
					check3.setVisible();
					check3.setState(true);
					break;
				case 3:
					check4.setVisible();
					check4.setState(true);
					break;
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * buttonClick
		 * o botão de resposta foi clicado
		 * @comment não preciso verificar o callback...
		 * */
		private function buttonClick():void
		{
			
			trace("***BUTTONCLICK");
			stopTimer();
			checkIntegrityFor("buttonClick");
			
			if (!isItMyTurn)
				return;
			
			
			///esconde rapidamente a janela, para evitar artefatos...
			setInvisible();			
			
			///limpa o timer
			
			///e para o relógio...
			clock.gotoAndStop(2);
			
			///haverá necessariamente uma função registrada para saber que a pergunta foi respondida
			answeredCallback();			
		}
		///--------------------------------------------------------------------------------------------------------------------	
		/**
		 * startTimer 
		 * inicia o timer para a resposta
		 */
		private function startTimer():void
		{
			trace("***TIMER STARTED");
			timeOutTimer=new Timer(speed*GUIConstants.SECONDS,1);
			clockTickTimer=new Timer(1*GUIConstants.SECOND,FuturaConstants.CLOCKFRAMES);
			clockTickTimer.addEventListener(TimerEvent.TIMER,tickClock);
			timeOutTimer.addEventListener(TimerEvent.TIMER,timeOutCallback);
			clock.gotoAndStop(2);
			clockTickTimer.start();
			timeOutTimer.start();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * tickClock 
		 * evento para avançar a frame do relógio
		 * @param e uso interno do timer
		 * 
		 */
		private function tickClock(e:Event):void
		{
			clock.gotoAndStop(clock.currentFrame+1);
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * stopTimer 
		 * Para o timer de resposta
		 */
		private function stopTimer():void
		{
			if (timeOutTimer==null)
			{
				trace("***TIMER STOPPED (null)");
				return;
			}
			trace("***TIMER STOPPED");
			timeOutTimer.stop();
			timeOutTimer.reset();
			timeOutTimer.removeEventListener(TimerEvent.TIMER,timeOutCallback);
			timeOutTimer=null;
			
			clockTickTimer.stop();
			clockTickTimer.reset();
			clockTickTimer.removeEventListener(TimerEvent.TIMER,tickClock);
			clockTickTimer=null;
			clock.gotoAndStop(2);
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * newQuestion
		 * Temos uma nova pergunta para ser exibida
		 * @param String mainText O texto da pergunta em sí
		 * @param Array options As opções de resposta
		 * @param int time Quanto tempo será dado para a resposta
		 * */
		public function newQuestion(mainText:String,options:Array,time:int, myTurn:Boolean=true, playerName:String=""):void
		{
			checkIntegrityFor("newQuestion");
			
			if (!myTurn )
			{
				if (playerName!="")
					this.titleText.setText("Pergunta ("+playerName+" respondendo)");
				else
					this.titleText.setText("Pergunta");
			}
			else
				this.titleText.setText("Pergunta");
			
			///checa a integridade dos parametros			
			if (mainText==null || mainText=="" || options == null || time <=0)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			if (2*options.length > listOptions.numChildren)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///variaveis
			var sprite:FadableSprite;
			var firstIdToHide:int;
			
			
			///limpa as opções da pergunta anterior
			check1.setState(false);
			check2.setState(false);	
			check3.setState(false);
			check4.setState(false);	
			
			
			
			///sem resposta por enquanto
			answer=FuturaConstants.NOANSWER;		
			
			///para cada opção
			for (var c:int=0;c<options.length;c++)
			{
				sprite=listOptions.getItemAt(2*c);
				
				///aqui, não há garantias que tudo é válido
				if (sprite==null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
				
				///define um novo texto	e o torna visível
				if (!(sprite is FadableSprite))
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
				
				(sprite as SpriteAndTextSprite).setText(options[c] as String);
				(sprite as SpriteAndTextSprite).getTextSprite().setDim(2,0,0,FuturaConstants.ANSWER_LINE_WIDTH);
				
				
				
				sprite.visible=true;
			}
			
			///o item visual seguinte à ultima pergunta feita visível
			firstIdToHide=(2*options.length)-1;			
			trace("recalculate positions");
			///recalcula as posições de acordo com  as novas posições
			listOptions.recalculatePositions();
			
			///e torna todo o resto invisivel
			for (var d:int=firstIdToHide;d<listOptions.numChildren;d++)
			{
				if (listOptions.getItemAt(d)==null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
				
				listOptions.getItemAt(d).visible=false;		
			}
			
			questionText.setText(mainText);
			questionText.setDim(2,0,0,FuturaConstants.ANSWER_LINE_WIDTH+2*GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			clock.gotoAndStop(2);
			
			if (myTurn)
			{
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
				this.buttonSprite.setVisible();
				setBGOpacity(0.95);	
				check1.setVisible();
				check2.setVisible();
				check3.setVisible();
				check4.setVisible();	
				check1.setEnabled();
				check2.setEnabled();
				check3.setEnabled();
				check4.setEnabled();
				
			}
			else
			{
				this.buttonSprite.setInvisible();
				setBGOpacity(0.8);
				check1.setInvisible();
				check2.setInvisible();
				check3.setInvisible();
				check4.setInvisible();	
				check1.setDisabled();
				check2.setDisabled();
				check3.setDisabled();
				check4.setDisabled();
				
			}
			isItMyTurn=myTurn;
			
			startShowAnimation(startTimer);			
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * timeOutCallback
		 * evento para "tempo acabou"
		 * @param TimerEvent e uso interno do Timer 
		 * */
		private function timeOutCallback(e:TimerEvent):void
		{
			///primeiro de tudo, parar o timer
			stopTimer();
			///verificar se o sistema esta ok
			checkIntegrityFor("timeOutCallback");
			///esconder a janela
			setInvisible();
			///e passar a resposta ao jogo
			answeredCallback();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setAnsweredCallback
		 * define a função de callback
		 * @comment não pode haver checagem de integridade, pois ainda não há callback registrado
		 * */		
		public function setAnsweredCallback(f:Function):void
		{
			///se o callback não for válido, reclame
			if (f==null)
				FuturaSafeGuard.nonFatalError(FuturaStrings.INTERNAL_ERROR);
			
			answeredCallback=f;
		}
		///--------------------------------------------------------------------------------------------------------------------	
		/**
		 * FuturaQuestionWindow
		 * construtor
		 * */
		public function FuturaQuestionWindow()
		{
			super();
			timeOutTimer=null;
			
			showAnswerTimer=new Timer(2*GUIConstants.SECONDS,1);
			if (showAnswerTimer==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			questionText=new FadableText("texto da pergunta",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (questionText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			check1=new ClickableCheckBox();
			if (check1==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			sprite1=new SpriteAndTextSprite(check1,"teste de item 1",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite1==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			check2=new ClickableCheckBox();
			if (check2==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			sprite2=new SpriteAndTextSprite(check2,"teste de item 2",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite2==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			check3=new ClickableCheckBox();
			if (check3==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			sprite3=new SpriteAndTextSprite(check3,"teste de item 3",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite3==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			check4=new ClickableCheckBox();
			if (check4==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			sprite4=new SpriteAndTextSprite(check4,"teste de item 4",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite4==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			listOptions=new SpriteVerticalList();
			if (listOptions==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			clock=new Clock();
			if (clock==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * initWindow
		 * inicializa a janela
		 * */
		override public function initWindow():void
		{
			super.initWindow();
			
			checkIntegrityFor("initWindow");
			
			///variaveis
			var separator:HorizontalLineSeparatorSprite;
			var listPanes:SpriteVerticalList;
			var listLeftPane:SpriteVerticalList;
			var listRightPane:SpriteVerticalList;
			var clockSprite:FadableSprite;
			var button:AnswerButton;
			
			///alocações			
			listPanes=new SpriteVerticalList();
			if (listPanes==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			titleText=new FadableText("Pergunta",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getTitleTextFormat());
			if (titleText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			
			
			titleText.getRawTextField().defaultTextFormat.size=22;
			
			listLeftPane=new SpriteVerticalList();
			if (listLeftPane==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			listRightPane=new SpriteVerticalList();
			if (listRightPane==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			clockSprite=new FadableSprite();
			if (clockSprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			button=new AnswerButton();
			if (button==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			buttonSprite=new ClickableFadableSprite();				
			if (buttonSprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			addWidget(listPanes);			 
			
			///titulo
			separator=new HorizontalLineSeparatorSprite(0,width-60-65);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			listPanes.addItem(titleText);						
			listPanes.addItem(separator,7);		
			listPanes.x=65;
			listPanes.y=50;			
			
			///painel esquerdo			
			listPanes.addItem(listLeftPane);	
			listLeftPane.y=100-listPanes.y;
			
			///texto pergunta
			listLeftPane.addItem(questionText);
			
			///lista de opções			
			listLeftPane.addItem(listOptions,35);
			
			///-------------------------
			
			///1º opção
			listOptions.addItem(sprite1);
			
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_LINE_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			listOptions.addItem(separator,7);
			
			///2º opção			
			listOptions.addItem(sprite2,7);
			///
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_LINE_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			listOptions.addItem(separator,7);
			
			///3º opção			
			listOptions.addItem(sprite3,7);
			
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_LINE_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);		
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			
			listOptions.addItem(separator,7);
			
			///4º opção			
			listOptions.addItem(sprite4,7);		
			
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_LINE_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR);
			separator.visible=false;
			
			listOptions.addItem(separator,7);			
			
			
			///-------------------------
			
			
			///painel direito
			listPanes.addItem(listRightPane);	
			listRightPane.x=listLeftPane.x+listLeftPane.width;
			listRightPane.y=listLeftPane.y;
			
			///relogio
			clock.cacheAsBitmap=true;
			clock.x+=clock.width/2;
			clock.y+=clock.height/2;	
			clock.x+=20;
			clockSprite.addChild(clock);
			listRightPane.addItem(clockSprite);
			
			///botao 'responder'
			buttonSprite.addChild(button);			
			button.y+=button.height/2;
			button.x+=button.width/2;				
			buttonSprite.y=0;
			listRightPane.addItem(buttonSprite,10);				
			listRightPane.x=listPanes.width-listRightPane.width;
			
			///callbacks			
			check1.setToggleCallback(this.check1Clicked);			
			check2.setToggleCallback(this.check2Clicked);			
			check3.setToggleCallback(this.check3Clicked);			
			check4.setToggleCallback(this.check4Clicked);
			buttonSprite.setOnMouseClickCallback(buttonClick);
			
			///inicializa a velocidade do timer
			setGameSpeed(FuturaConstants.TIMENORMAL);
		}		
		///--------------------------------------------------------------------------------------------------------------------		
	}
}