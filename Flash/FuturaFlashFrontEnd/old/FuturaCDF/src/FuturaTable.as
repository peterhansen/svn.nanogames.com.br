/****
 *  Apenas para explicar melhor:
 * 	[ Jogo ] <*>----[Turno]<*>----[Jogada]  
 * TODO: 
 * nova callback para quando só tem um jogador:
 * - forceExitRoom
 * */
package 
{
	///--------------------------------------------------------------------------------------------------------------------	
	///<imports>
	
	///flash
	import flash.display.Loader;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.geom.ColorTransform;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	///interface com servidor
	import FuturaExternalInterface.FuturaBaseInterface;
	import FuturaExternalInterface.FuturaInterface;
	import FuturaExternalInterface.FuturaMockInterface;
	
	//GUILib
	import GUILib.GUIConstants;
	
	///interface gráfica
	import FuturaGUI.FuturaMessageWindow;
	import FuturaGUI.FuturaQuestionWindow;
	import FuturaGUI.FuturaScoreBox;
	import FuturaGUI.FuturaScoreWindow;
	import FuturaGUI.FuturaSpecialTurnWindow;
	
	///engine de jogo
	import FuturaGameEngine.FuturaPlayer;
	
	///sons
	import carta_bonus.mp3;
	import carta_penalti.mp3;
	import resposta_correta.mp3;
	import resposta_errada.mp3;	
	
	///</imports>
	///--------------------------------------------------------------------------------------------------------------------
	/**
	 * FuturaTable - Mesa de jogo FuturaCDF.
	 * Encapsula o jogo em sí.
	 * @author Daniel Monteiro
	 * */
	[SWF( width= "530", height = "410", backgroundColor = "0x000000", frameRate = "20" )]	
	public class FuturaTable extends Sprite
	{
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * lista de jogadores que entraram no jogo em primeiro passo
		 */
		private var players:Array; ///<FuturaPlayer>
		/**
		 * qual é o jogador jogando atualmente
		 */
		private var currentPlayerIndex:int;
		/**
		 * numero de jogadores ativos no momento (aceleradora para não ter que contar quais estão ativos)
		 */
		private var activePlayers:int;
		/**
		 * o jogo esta em andamento?
		 */
		private var gameStopped:Boolean;			
		/**
		 * posições das peças no tabuleiro (uma por frame, por jogada acertada)
		 */
		private var piecesPosition:PiecesPosition;
		/**
		 * posições das setas no tabuleiro (uma por frame, por jogada acertada)
		 */
		private var arrowPosition:ArrowPosition;
		/**
		 * tabuleiro em sí
		 */
		private var board:Board;
		/**
		 * cartinhas empilhadas (enfeite)
		 */
		private var cards:Cards;
		/**
		 * fundo da mesa
		 */
		private var background:Background;
		/**
		 * jogada atual
		 */
		private var currentTurn:int;
		/**
		 * rodada atual
		 */
		private var currentCycle:int;
		/**
		 * qual é a posição da próxima peça a ser colocada no tabuleiro (corresponde ao turno atual)
		 */
		private var currentPiecePosition:int;		
		/**
		 * Caixa de scores 
		 */
		private var scoreBox:FuturaScoreBox;
		/**
		 * janela de placar
		 */
		private var scoreWindow:FuturaScoreWindow;
		/**
		 * janela de perguntas
		 */
		private var questionWindow:FuturaQuestionWindow;
		/**
		 * janela de mensagens genéricas
		 */
		private var msgWindow:FuturaMessageWindow;
		/**
		 * janela de cartas de bônus e penalidade
		 */
		private var specialWindow:FuturaSpecialTurnWindow;
		/**
		 * som correto
		 */
		private var correctAnswer:resposta_correta.mp3;
		/**
		 * som errado
		 */
		private var wrongAnswer:resposta_errada.mp3;
		/**
		 * interface de comunicação externa
		 */
		private var futuraInterface:FuturaBaseInterface;
		/**
		 * id externo da pergunta atual
		 */
		private var questionId:String;
		/**
		 * posicao do jogador local no array de jogadores
		 */
		private var localPlayerId:int;
		/**
		 * Timer para quando deve começar o novo turno
		 */
		private var timerToNextTurn:Timer;
		/**
		 * som de bonus 
		 */
		private var bonusSound:carta_bonus.mp3;
		/**
		 * som de penalidade
		 */
		private var penaltySound:carta_penalti.mp3;			
		///</Campos>		
		///--------------------------------------------------------------------------------------------------------------------	 
		/**
		 * Constructor
		 * Ainda não posso fazer muito, pois não sei muito do meu pai ainda.
		 * */
		public function FuturaTable():void
		{
			///incializando, então, bloquear tudo por enquanto /////
			gameStopped=true;
			/////////////////////////////////////////////////////////			
			
			///inicialização do motor de jogo
			questionId="";
			currentPiecePosition=1;		
			currentCycle=-1;
			players = new Array();			
			localPlayerId=-1;
			///elementos gráficos
			
			///janelas
			scoreWindow=new FuturaScoreWindow();			
			questionWindow=new FuturaQuestionWindow();								
			msgWindow=new FuturaMessageWindow();
			scoreBox=new FuturaScoreBox();
			specialWindow=new FuturaSpecialTurnWindow();
			
			///elementos de mesa de jogo
			background=new Background();
			board=new Board();
			piecesPosition=new PiecesPosition();
			arrowPosition=new ArrowPosition();
			cards=new Cards();	
			timerToNextTurn=new Timer(1*GUIConstants.SECOND);
			timerToNextTurn.addEventListener(TimerEvent.TIMER, tickNextTurn);
			correctAnswer=new resposta_correta.mp3();
			wrongAnswer=new resposta_errada.mp3();
			bonusSound=new carta_bonus.mp3();
			penaltySound=new carta_penalti.mp3();
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * askForNewTurn
		 * pede o começo dum novo turno (deve acontecer ~2 segundos depois)
		 */
		private function askForNewTurn():void
		{
			timerToNextTurn.start();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * tickNextTurn
		 * inicia o novo turno de fato
		 * @param e uso interno do timer
		 */
		private function tickNextTurn(e:TimerEvent):void
		{
			timerToNextTurn.reset();
			this.startNextTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * initTable
		 * inicializa a interface gráfica do jogo
		 * */
		public function initTable():void
		{
			///inicialização da interface visual do jogo
			/// fundo /////////////////////////////////////////////////////////////////////
			background.cacheAsBitmap=true;
			addChild(background);
			background.x=(stage.stageWidth)/2;
			background.y=(stage.stageHeight)/2;			
			
			/// tabuleiro /////////////////////////////////////////////////////////////////
			board.cacheAsBitmap=true;
			addChild(board);			
			board.x=150+ board.width/2;
			board.y=15+board.height/2;
			
			/// peças /////////////////////////////////////////////////////////////////////			
			piecesPosition.cacheAsBitmap=true;
			board.addChild(piecesPosition);		
			
			/// seta /////////////////////////////////////////////////////////////////////
			arrowPosition.cacheAsBitmap=true;
			board.addChild(arrowPosition);
			arrowPosition.x=0;
			arrowPosition.y=0;			
			
			/// cartas /////////////////////////////////////////////////////////////////////
			cards.cacheAsBitmap=true;
			addChild(cards);
			cards.x=cards.width/2;
			cards.y=height-(cards.height);
			
			/// box de pontuação/////////////////////////////////////////////////////////////////////
			addChild(scoreBox);
			scoreBox.x=7;
			scoreBox.y=7;
			
			///janelas
			/// janela de placar geral //////////////////////////////////////////////////////
			addChild(scoreWindow);
			scoreWindow.initWindow();
			scoreWindow.visible=false;
			scoreWindow.setOkCallback(leave);
			
			/// janela de perguntas //////////////////////////////////////////////////////			
			addChild(questionWindow);
			questionWindow.initWindow();			
			questionWindow.visible=false;			
			questionWindow.setAnsweredCallback(sendAnswer);
			
			/// janela genérica de mensagens //////////////////////////////////////////////////////	
			addChild(msgWindow);
			msgWindow.initWindow();
			msgWindow.visible=false;
			
			/// janela de jogada especial //////////////////////////////////////////////////////	
			addChild(specialWindow);
			specialWindow.initWindow();
			specialWindow.visible=false;
			
			////tudo pronto////
			gameStopped=false;
			///////////////////
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * updatePointsBox
		 * Atualiza a caixa de placar do jogador local
		 */	
		
		private function updatePointsBox():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("updatePointsBox");
			
			///variáveis usadas na função
			var player:FuturaPlayer;
			
			///atualiza a caixa de score
			player=getPlayer(localPlayerId);				
			scoreBox.setDisplayItems(player.getHits(),player.getErrors(),player.getScore());
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * placeArrow
		 * coloca seta na posicao do tabuleiro
		 * @param index posicao da seta no tabuleiro [1..FuturaConstants.NUMPIECES]
		 * @param color indice da cor da seta [1..6]
		 * */
		private function placeArrow(index:int,color:int):void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("placeArrow");
			
			///checagem de consistência de parametros////
			/// a posição da peça deve ser válida
			if (index >= arrowPosition.numChildren || index >= FuturaConstants.NUMPIECES || index < 0)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( index="+index.toString()+"/"+arrowPosition.numChildren.toString()+")");
			}
			
			/// a cor deve ser válida também 
			if (color < FuturaConstants.COLORGREENINDEX || color > FuturaConstants.COLORREDINDEX)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			}			
			/////////////////////////////////////////////
			
			///variáveis usadas na função
			var arrow:Arrow;
			
			///apagando todas outras possíveis setas visíveis
			for (var c:int=0;c<index;c++)
				if (arrowPosition.getChildAt(c) is Arrow)
				{
					arrow=arrowPosition.getChildAt(c) as Arrow;
					arrow.visible=false;
				}
			
			///obtendo a seta atual, tornando-a visível e colorida
			arrow=arrowPosition.getChildAt(index) as Arrow;
			arrow.visible=true;
			arrow.gotoAndStop(color);			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setPiece
		 * coloca uma peça no tabuleiro, indicando acerto do jogador
		 * @param index posição da peça no tabuleiro [1..FuturaConstants.NUMPIECES]
		 * @param color indice da cor da seta [1..6]
		 * */		
		private function setPiece(index:int, color:int):void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("setPiece");		
			
			///checagem de consistência de parametros////
			/// a posição da peça deve ser válida
			///NÃO USAR >= , POIS FLASH É [1..NUMCHILDREN]
			if (index > FuturaConstants.NUMPIECES || index < 1)
			{
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			}
			
			/// a cor deve ser válida também 
			if (color < FuturaConstants.COLORGREENINDEX || color > FuturaConstants.COLORREDINDEX)
			{
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			}				
			/////////////////////////////////////////////			
			
			///variáveis usadas na função
			var piece:Piece;
			
			///obtem a peça, torna visível e colore
			piece=piecesPosition.getChildAt(index) as Piece;
			piece.visible=true;
			piece.gotoAndStop(color);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * nextPiecePosition
		 * calcula a próxima posição de peça de jogo
		 * */
		private function nextPiecePosition():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("nextPiecePosition");		
			
			///checagem de consistência de elementos da função
			if (currentPiecePosition > FuturaConstants.NUMPIECES || currentPiecePosition < 0)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+"(currentPiecePosition="+ currentPiecePosition.toString()+")");
			}
			
			///incrementa a posição da próxima peça, mantendo a consistência
			currentPiecePosition++;
			if (currentPiecePosition>FuturaConstants.NUMPIECES)
				currentPiecePosition=1;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * addPlayer
		 * adiciona um jogador ao jogo.
		 * @param Name nome de exibição do jogador
		 * @param FuturaId Id externo do jogador (pelo qual ele será referenciado no mundo externo
		 * */
		private function addPlayer(Name:String,FuturaId:String):void
		{
			if (this.players==null)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///checagem de consistência dos parametros
			if (Name==null || Name == "" || FuturaId == null || FuturaId == "")
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			//////////////////////////////////////////
			
			///variáveis usadas na função
			var player:FuturaPlayer;
			
			///criação e inclusão do jogador
			player=new FuturaPlayer();
			
			if (player==null)
				fatalError(FuturaStrings.ALLOC_ERROR);
			
			///configurando o jogador
			player.setActive(true);
			player.setName(Name);
			player.setColorIndex((players.length%FuturaConstants.NUMCOLORS)+1);		
			player.setFuturaId(FuturaId);			
			players.push(player);
			
			//	updatePlayerList();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * updatePlayerList
		 * atualiza a lista de jogadores a todos os interessados
		 * */
		private function updatePlayerList():void			
		{
			if (players == null || players.length == 0 )
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			

			
			///de fato, avisa aos interessados que a lista foi atualizada
			scoreWindow.updatePlayerList(players);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * startNextCycle
		 * começa uma nova rodada de jogo
		 * */
		private function startNextCycle():void
		{
			
			traceMessage("<cycle>");
			///checagem de consistência padrão
			checkGameConsistencyFor("startNextCycle");		
			
			///checagem de consistência de elementos da função
			if (currentPiecePosition > FuturaConstants.NUMPIECES || currentPiecePosition < 1)
			{
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" (currentPiecePosition="+currentPiecePosition.toString()+")");
			}
			
			++currentCycle;			
			
			///variáveis usadas pela função
			var player:FuturaPlayer;
			
			///obtenção do jogador atual, para se configurar a jogada
			player=getCurrentPlayer();
			
			///jogador atual é válido?
			if (player==null || !player.getActive())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * startNextTurn
		 * começa uma jogada
		 * */
		public function startNextTurn():void
		{
			if (currentCycle==-1)
			{
				startNextCycle();
				//	return;
			}
			
			traceMessage("<turn>");
			traceMessage("currentPlayerIndex="+currentPlayerIndex.toString());
			
			///checagem de consistência padrão
			checkGameConsistencyFor("startNextTurn");			
			
			///variáveis usadas pela função
			var player:FuturaPlayer;
			
			///obtenção do jogador atual, para se configurar a jogada
			player=setNextPlayerandGetIt();
			
			///jogador atual é válido?
			if (player==null || !player.getActive())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///posiciona a seta do jogador, se válida
			if (getArrowPosition() < arrowPosition.numChildren)
			{
				placeArrow(getArrowPosition(),player.getColorIndex());
				getQuestion();
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * endCycle
		 * finaliza rodada de jogadas
		 * */
		public function endCycle():void
		{
			traceMessage("</cycle>");
			///checagem de consistência padrão
			checkGameConsistencyFor("endCycle");
			
			///se a ultima peça foi conquistada...
			if (getArrowPosition()>FuturaConstants.NUMPIECES)
			{
				///fim de jogo				
				endGame();
			}			
			else
				startNextCycle();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getArrowPosition
		 * pega a posicao atual da seta
		 * @returns int posicao da seta
		 * */
		public function getArrowPosition():int
		{
			return currentPiecePosition;
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setNextPlayerandGetIt
		 * define o próximo jogador
		 * @returns FuturaPlayer o (novo) jogador atual
		 * */
		public function setNextPlayerandGetIt():FuturaPlayer
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("setNextPlayerandGetIt");		
			
			///variaveis usadas
			var player:FuturaPlayer;
			var nextIndex:int;
			///conta quantas vezes ja percorremos a lista em busca de um jogador ativo. Evita loops infinitos
			var cycled:int;
			
			/// começamos do jogador atual
			nextIndex=currentPlayerIndex;
			
			/// e sem ciclos tentados
			cycled=0;
			
			///buscando o próximo
			do
			{
				///incrementa
				++nextIndex;
				
				///verifica overflow e trata
				if (nextIndex >= players.length)
				{
					nextIndex=0;
					++cycled;
				}
				///obtem a referencia, para verificar se ele esta ativo ou não
				player=getPlayer(nextIndex);
				
			} ///e se ele não estiver ativo, continuamos tentando - a não ser que ja tenhamos tentado demais...
			while (!player.getActive() && cycled < 3);
			
			///caso tenhamos tentado o suficiente, sem conseguir um, temos uma situação de erro.
			if (cycled >= 3)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			currentPlayerIndex=nextIndex
			traceMessage ("proximo jogador é:"+ currentPlayerIndex.toString());
			return player;
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * endTurn
		 * finaliza jogada
		 * @comment Eu poderia manter um controle rígido sobre turnos, mas não é necessário. Desde que a rodada termine
		 * @comment quando algum jogador responder corretamente, é suficiente.
		 * */
		private function endTurn():void
		{
			traceMessage("</turn>");
			///checagem de consistência padrão
			checkGameConsistencyFor("endTurn");		
			
			///avanço de turno
			++currentTurn;
			
			
			///atualiza score
			updatePointsBox();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * endGame
		 * fim de jogo
		 * */
		private function endGame():void
		{
			traceMessage("</game>");
			///checagem de consistência padrão
			checkGameConsistencyFor("endGame");		
			if (gameStopped)
				return;
			///e o jogo para!
			gameStopped=true;
			
			updatePlayerList();
			
			
			questionWindow.setInvisible()
			specialWindow.setInvisible();
			msgWindow.setInvisible();
			
			///e por fim, podemos exibir o score final do jogo.
			scoreWindow.startShowAnimation();
			
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * startNewGame
		 * inicia um novo jogo
		 * @see startNextCycle 
		 * */
		public function startNewGame() :void
		{			
			traceMessage("<Game>");
			
			if (players==null)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///variaveis usadas pela função
			var piece:Piece;
			var arrow:Arrow;
			var c:int;
			
			///inicializando os dados gerais de jogo
			currentTurn=0;
			currentPlayerIndex=0;
			currentPiecePosition=0; //não é válido no momento, mas vai ser.
			currentCycle=-0;
			
			///reconfigurando o tabuleiro
			for (c=0;c<piecesPosition.numChildren;c++)
			{
				piece=piecesPosition.getChildAt(c) as Piece;									
				piece.visible=false;
				arrow=arrowPosition.getChildAt(c) as Arrow;
				arrow.visible=false;					
			}
			
			///atualizando a caixa de placar
			//updatePointsBox();
			
			///inicializando a interface com o servidor
			futuraInterface= new FuturaInterface(addUserCallback,removeUserCallback, takeUserList, setMyId, setNewTurn,answerResponseReceivedCallback);
			//futuraInterface= new FuturaMockInterface(addUserCallback,removeUserCallback, takeUserList, setMyId, setNewTurn,answerResponseReceivedCallback);			
			futuraInterface.init();
			
			var url:String;
			url=futuraInterface.call("getBackgroundURL") as String;
			trace(url);
			if (url != null)
			{
				var loader:Loader;
				var req:URLRequest;
				req=new URLRequest(url);
				loader=new Loader();
				loader.load(req);
				
				this.removeChild(background);
				this.addChildAt(loader,0);
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getNumberOfInitialPlayers
		 * obtem o numero de jogadores registrados
		 * @returns int Numero de jogadores que iniciaram a partida (e que vão continuar registrados até o fim do jogo)
		 * @see getNumberOfActivePlayers
		 * */
		public function getNumberOfInitialPlayers():int
		{
			if (players == null)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			return players.length;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getNumberOfActivePlayers
		 * obtem o numero de jogadores atualmente jogando
		 * @returns int Numero de jogadores ainda conectados no jogo
		 * @see getNumberOfInitialPlayers
		 * */
		public function getNumberOfActivePlayers():int
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("getNumberOfActivePlayers");		
			
			return activePlayers;			
		}			
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerAnsweredCorrectly
		 * indica que o jogador respondeu corretamente a pergunta
		 * @comment Aqui, não apenas a jogada é encerrada, mas a rodada também
		 * @see playerAnsweredWrongly
		 * */
		private function playerAnsweredCorrectly():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("playerAnsweredCorrectly");		
			
			///variáveis usadas na função
			var actor:FuturaPlayer;
			
			///notifica ao jogador do acerto
			actor=getCurrentPlayer();
			actor.playerDidHit();
			
			///incrementa e coloca sua peça no tabuleiro, se válido			
			if (currentPiecePosition<=piecesPosition.numChildren)
				setPiece(currentPiecePosition,actor.getColorIndex());
			nextPiecePosition();
			
			
			///indica que a jogada esta encerrada (rodada também?)
			endTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerAnsweredWrongly
		 * indica que o jogador responder erroneamente
		 * @see playerAnsweredCorrectly
		 * @comment Vale notar que aqui a rodada não é encerrada, apenas a jogada
		 * */
		private function playerAnsweredWrongly():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("playerAnsweredWrongly");		
			
			///variaveis usadas na função			
			var actor:FuturaPlayer;
			
			///notifica o jogador de seu erro
			actor=getCurrentPlayer();
			actor.playerDidError();
			
			///e finaliza a jogada
			endTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * removePlayer
		 * notifica sistema que um jogador saiu do jogo
		 * @param String FuturaId Id externo do jogador que saiu.
		 * @see addPlayer
		 * @comment Nós apenas fingimos que o retiramos. Para evitar que o castelo de cartas desabe, melhor apenas
		 * @comment ignorar o jogador.
		 * */
		private function removePlayer(FuturaId:String):void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("removePlayer");		
			
			///verificando validade do parametro
			if (FuturaId==null || FuturaId=="" || FuturaId==" ")
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///variaveis usadas pela função			
			var player:FuturaPlayer;
			var gameId:int;
			
			///descobre id interno do jogador que saiu
			gameId= getIdWithFuturaId( FuturaId );
			
			///verifica a validade desse id interno obtido
			if (gameId < 0 || gameId > getNumberOfInitialPlayers())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///obtem a referencia ao jogador
			player= getPlayer(gameId);
			
			///verifica se o jogador é válido ou se ja havia sido retirado
			if (player==null || !player.getActive())
				warnDeveloper(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///"retiramos" o jogador...
			player.setActive(false);
			
			///e avisamos a quem possa interessar... 
			//updatePlayerList();		
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * answerResponseReceivedCallback
		 * recebe feedback sobre a resposta dada pelo jogador
		 * @param Boolean isCorrect diz se a resposta dada foi correta ou não
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */
		public function answerResponseReceivedCallback(isCorrect:Boolean, Answer:String):void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("answerResponseReceivedCallback");		
			
			///notifica o jogador de sua resposta
			if (isCorrect)
			{
				
				if (correctAnswer!=null && questionId!="-1")
					correctAnswer.play();
				
				
				playerAnsweredCorrectly();
				endCycle();
			}
			else
			{
				if (wrongAnswer!=null && questionId!="-1")
					wrongAnswer.play();
				
				playerAnsweredWrongly();
			}
			
			var answer:int;
			
			if (Answer=="a") answer=0;
			if (Answer=="b") answer=1;
			if (Answer=="c") answer=2;
			if (Answer=="d") answer=3;	
			
			
			if (!localPlayerPlaying())
				questionWindow.setDisplayedAnswer(answer);
			
			
			///e atualiza a caixa de placar
			updatePointsBox();
			futuraInterface.askForNextTurn(currentTurn);
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setNewTurn
		 * notificação do servidor de que é hora de começar uma nova jogada
		 * @param int turn numero do turno atual
		 * @see startNextTurn
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * TODO:
		 * - fazer uma verificação extra de consistência com o valor de turno recebido 
		 * */
		public function setNewTurn(turn:int):void
		{
			
			traceMessage("FuturaTable::setNewTurn()");
			
			///checagem de consistência padrão
			checkGameConsistencyFor("setNewTurn");		
			
			traceMessage("meu valor de turno:"+currentTurn.toString()+" valor de turno recebido:"+turn.toString());
			
			///se não tem um ciclo começado, comecemos um:
			
			///...e começa a nova jogada, se for o caso
			if (currentPiecePosition<FuturaConstants.NUMPIECES)
				//startNextTurn();
				askForNewTurn();
			else 
				endGame();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setMyId
		 * Notifica ao cliente qual dos jogadores da lista inicial é o jogador local
		 * @param String FuturaId Id externo do jogador
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */		
		public function setMyId(FuturaId:String):void
		{
			traceMessage("FuturaTable::setMyId()");
			
			if (players == null || players.length == 0)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///verifica consistência dos parametro
			if (FuturaId==null || FuturaId=="" || FuturaId==" ")
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///variaveis usadas pela função
			var player:FuturaPlayer;
			var gameId:int;
			
			///obtem o Id
			gameId= getIdWithFuturaId(FuturaId);
			
			///verifica validade do Id
			if (gameId < 0 || gameId >= getNumberOfInitialPlayers())
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///e verifica por fim se o jogador é válido
			if (!getPlayer(gameId).getActive())
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			///efetiva o Id de jogador local
			localPlayerId=gameId;
			
			traceMessage("my FuturaId:"+FuturaId);
			traceMessage("my GameId:"+gameId.toString());
			
			//	startNextTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestion
		 * pede uma pergunta ao servidor 
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */
		public function getQuestion():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("getQuestion");		
			
			if (futuraInterface == null)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///variaveis usadas pela função (não gosto de usar Object, mas...)			
			var question:Object;
			var array:Array
			
			///obtem o objeto da pergunta
			question = futuraInterface.getQuestionsFromServer();
			
			///verifica a validade do object			
			if (question==null || question.id==null)
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			if (question.title==null || (question.title as String).length<1)		
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);			
			
			///internaliza os dados recebidos e validados
			this.questionId = question.id;
			
			
			///preenche os campos de passagem de pergunta para a janela de perguntas.
			///um campo não preenchido aqui não significa necessariamente um erro.
			array=new Array();
			
			///1º opção
			if (question.a!=null && (question.a as String).length>0)
				array.push(question.a);
			
			///2º opção
			if (question.b!=null && (question.b as String).length>0)			
				array.push(question.b);
			
			///3º opção	
			if (question.c!=null && (question.c as String).length>0)			
				array.push(question.c);
			
			///4º opção			
			if (question.d!=null && (question.d as String).length>0)			
				array.push(question.d);
			
			///...mas se não houver pelo menos duas opções válidas, a pergunta é inválida
			if (array.length < 2)
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED);
			
			//	question.kind=1;
			
			///se a pergunta tiver alguma bonificação ou penalidade...
			if (question.kind!=null)
			{
				if (question.kind!=0)
				{
						
					///esta deve ser informada ao usuário.
					///penalidade
					if (question.kind<0)
					{
						if (penaltySound!=null)
							penaltySound.play();
					
						this.questionId="-2";
						specialWindow.setSadIcon();					
						specialWindow.showModalMessage("Penalidade ( "+getCurrentPlayer().getName()+" jogando )","Que azar!\n\nVocê acaba de ser penalizado e\nperder os pontos de uma pergunta.",sendAnswer);
					}
					else
						///bonus
					{
						if (bonusSound!=null)
							bonusSound.play();
						
						this.questionId="-3";	
						specialWindow.setHappyIcon();
						specialWindow.showModalMessage("Bônus ( "+getCurrentPlayer().getName()+" jogando )","Parabéns!\n\nVocê acaba de receber os pontos\nde uma pergunta como bônus!",sendAnswer);
					}					
					return;
				}
			}
			
			///por fim, podemos fazer a pergunta.			
			questionWindow.newQuestion(question.title,array,20, this.localPlayerPlaying(), getCurrentPlayer().getName());
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * sendAnswer
		 * envia a pergunta ao servidor 
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */
		public function sendAnswer():void
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("sendAnswer");	
			
			if (!localPlayerPlaying())
				return;
			
			///variaveis usadas pela função
			var ans:Object;
			var answer:int;
			
			///verifica se é carta bonus ou penalidade
			if (questionId==FuturaConstants.BONUSCARDID)
			{
				getCurrentPlayer().playerDidHit();
				return;
			}
			
			if (questionId==FuturaConstants.PENALTYCARDID)
			{
				getCurrentPlayer().addPenalty();
				return;
			}
 
			
			///valida a reposta a ser enviada
			if (questionWindow!=null)
				answer=	questionWindow.getAnswer();
			else
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			if (answer<0 || answer > 4)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///envia a resposta no formato esperado pelo servidor. (também não gostei disso, mas...)
			ans={0: "a", 1:"b", 2:"c", 3:"d", 4:"-"};
			futuraInterface.verifyAnswer(ans[questionWindow.getAnswer()], questionId);
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * addUserCallback
		 * aviso do servidor de que um novo usuário entrou no jogo
		 * @param Object user usuário do jogo 
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */
		public function addUserCallback(user:Object):void
		{
			traceMessage("FuturaTable::addUserCallback()");			
			if (players==null)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			///Object...¬¬
			addPlayer(user.nickname, user.id);
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * removeUserCallback
		 * aviso do servidor de que um novo usuário entrou no jogo
		 * @param Object user usuário do jogo 
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */
		public function removeUserCallback(userId:String):void		
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("removeUserCallback");	
			
			///variaveis usadas pela função
			var player:FuturaPlayer;
			var playerId:int;
			
			///decrementa a quantidade de jogadores ativos
			activePlayers--;
			
			///o jogo continua válido?
			if (activePlayers < FuturaConstants.MINPLAYERS || activePlayers >= FuturaConstants.MAXPLAYERS)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///obtem o id local do jogador a ser invalidado
			playerId=getIdWithFuturaId(userId);
			
			//verifica a consistência do id obtido
			if (playerId < 0 || playerId > getNumberOfInitialPlayers())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			//obtem a referencia para o jogador a ser invalidado
			player=getPlayer(playerId);
			
			///verifica se o jogador é válido ou se não foi retirado do jogo
			if (player==null || !player.getActive())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///desativa o jogador
			player.setActive(false);
			
			///e se o jogador era o mesmo jogando, sua jogada é encerrada automáticamente
			if (currentPlayerIndex==playerId)
				endTurn();
		}	
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * takeUserList
		 * recebe a lista de usuários do servidor
		 * @param Array userList lista de usuários do jogo 
		 * @comment criada com base no protocolo definido por Yuri Mello
		 * */
		public function takeUserList(userList:Array):void
		{	
			traceMessage("FuturaTable::takeUserList()");			
			
			if (players == null)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///valida o parametro recebido
			if (userList==null || userList.length ==0 || userList.length <  FuturaConstants.MINPLAYERS || userList.length > FuturaConstants.MAXPLAYERS)
				fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED+" (userList="+userList+")");
			
			///para cada jogador recebido, adiciona-lo à lista de jogadores
			traceMessage("user list:");
			
			for each(var user:Object in userList)
			{
				addUserCallback(user);
				traceMessage("<"+user.nickname+","+user.id+">");
			}
			
			///atualiza a quantidade de jogadores válidos
			activePlayers=userList.length;
			currentPlayerIndex=activePlayers-1;
			
			
			//updatePlayerList();
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * leave
		 * jogador local esta deixando a mesa de jogo
		 * @comment não há checagem de válidade do jogo, pois esta pode ser uma rota de saída do jogo em erro
		 * */
		public function leave():void
		{
			gameStopped=true;
			traceMessage("*******ENDGAME********");
			ExternalInterface.call("endGame");
			this.visible=false;
		}
		///------------------------------------------------------------------------------------------------------------------		
		/**
		 * fatalError
		 * açucar sintático para sinalizar um erro fatal
		 * @param String msg Mensagem de erro
		 * @comment não há checagem de válidade do jogo, pois esta é uma rota de saída do jogo em erro 
		 * */
		public function fatalError(msg:String):void
		{
			///antes de qualquer coisa, bloqueia qualquer comportamento do jogo
			gameStopped=true;
			this.visible=false;
			///esconde qualquer janela visivel
			questionWindow.visible=false;
			scoreWindow.visible=false;
			
			///propaga o erro
			
			msgWindow.showModalMessage("Erro","Por favor, tente mais tarde",onErrorAbend);
			FuturaSafeGuard.nonFatalError("FuturaTable::fatalError() >> "+msg);
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * onErrorAbend 
		 * Saindo em caso de erro
		 */
		public function onErrorAbend():void
		{
			trace("abend");
			futuraInterface.call("leave");
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * getPlayer
		 * obtem uma referência segura ao jogador desejado
		 * @param id interno do jogador na mesa local
		 * @return referencia ao jogador pedido
		 * */
		private function getPlayer(id:int):FuturaPlayer
		{
			if (players == null || players.length == 0 )
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///valida parametro recebido
			if (id < 0 || id > getNumberOfInitialPlayers())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///variavel usada pela função
			var player:FuturaPlayer;
			
			///obtem o ponteiro
			player=players[id];
			
			//valida o ponteiro
			if (player==null) 
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			///retorno do jogador pedido
			return player;
		}
		///------------------------------------------------------------------------------------------------------------------		
		/**
		 * getCurrentPlayer
		 * açucar sintático para se obter o ponteiro para o jogador atual
		 * @returns FuturaPlayer referencia ao jogador que esta jogando
		 * */
		private function getCurrentPlayer():FuturaPlayer
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("getCurrentPlayer");	
			
			///variaveis usadas pela função
			var player:FuturaPlayer;
			
			player = getPlayer (currentPlayerIndex);
			
			if (player==null || !player.getActive())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			
			return player;
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * traceMessageMessage
		 * exibe uma string no console de debug
		 * @param String msg mensagem a ser exibida no console de debug
		 * @comment não tem checagem de erro pois ela pode ja estar sendo usada para reportar um erro
		 * */		
		public function traceMessage(msg:String):void
		{
			trace ("jogador"+localPlayerId.toString()+"::"+msg);		
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * nonFatalError
		 * propaga um erro não fatal, mas indicando seu ponto de ocorrência
		 * @param String msg mensagem de erro
		 * */
		public function nonFatalError(msg:String):void
		{
			///propaga o erro
			FuturaSafeGuard.nonFatalError("FuturaTable::nonFatalError() >> "+msg);
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * warnDeveloper
		 * propaga um warning, mas indicando seu ponto de ocorrência
		 * @param String msg mensagem de warning
		 * */
		public function warnDeveloper(msg:String):void
		{
			///propaga o warning
			FuturaSafeGuard.warnDeveloper("FuturaTable::warnDeveloper() >> "+msg);
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * checkGameConsistencyFor
		 * checa para ver se o jogo esta válido antes de prosseguir
		 * @param String funcName nome da função que pediu a checagem
		 * @comment nem todas as checagens são executadas, pois essa checagem pode esbarrar num caso em que algo
		 * @comment testado ainda não esta pronto e este é seu estado esperado
		 * */
		public function checkGameConsistencyFor(funcName:String):void
		{
			/////////////<checagem padrão de consistência de jogo
			///jogador atual é válido?
			if (players==null)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			if (players[currentPlayerIndex]==null)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			///jogador atual é ativo?
			if (!players[currentPlayerIndex].getActive())
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			if (localPlayerId < 0 || localPlayerId > players.length)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			if (players[localPlayerId]!=null && !players[localPlayerId].getActive())
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			if (currentPlayerIndex < 0 || currentPlayerIndex > players.length)
				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			
			///se é minha vez, minha pergunta é válida? (o id)
			if ( currentPlayerIndex==localPlayerId && ( questionId==null || questionId=="" || questionId==" ") )
				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( FuturaTable::"+funcName+")");
			
			///o jogo esta em andamento? (não é erro, mas deve gerar um warning no log)
			if (gameStopped)
			{
				warnDeveloper(FuturaStrings.CALLS_DURING_STOPPED_GAME+" ( FuturaTable::"+funcName+")");
				return;
			}
			//tudo certo
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * localPlayerPlaying
		 * apenas um açucar sintático
		 * @returns Boolean indicando se o jogador atual é o jogador local
		 * */
		public function localPlayerPlaying():Boolean
		{
			///checagem de consistência padrão
			checkGameConsistencyFor("localPlayerPlaying");	
			
			///o teste em sí. os valores individuais ja foram validados acima
			return localPlayerId==currentPlayerIndex;
		}
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * getIdWithFuturaId
		 * dado um Id externo, qual é o seu Id interno?
		 * @param String FuturaId externo
		 * @return int Id interno
		 * */
		public function getIdWithFuturaId(FuturaId:String):int
		{
			if (players == null || players.length == 0)
				fatalError(FuturaStrings.INTERNAL_ERROR);
			
			///variaveis usadas pela função
			var player:FuturaPlayer;
			var c:int;
			
			for (c=0; c< players.length;c++)
			{
				///obtem o ponteiro para o jogador candidato
				player=getPlayer(c);
				
				///verifica se não há inconcistência
				if (player==null)
					fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
				
				///e se o jogador tiver o Id desejado, retornar
				if (player.getFuturaId()==FuturaId)
					return c;
			}
			
			fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			return 0;
		}
		///--------------------------------------------------------------------------------------------------------------------		
	}
}