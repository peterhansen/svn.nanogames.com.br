package GUILib
{
	/**
	 * HorizontalLineSeparator
	 * Um simples separador entre items de uma lista 
	 * @author Daniel Monteiro
	 * 
	 */
	public class HorizontalLineSeparatorSprite extends FadableSprite
	{
		/**
		 * Construtor 
		 * @param x1 começo em X da linha
		 * @param x2 fim da linha, em X
		 * @param stroke grossura da linha
		 * @param color cor da linha
		 * @param alpha sua opacidade
		 * 
		 */
		public function HorizontalLineSeparatorSprite(x1:int,x2:int,stroke:int=1,color:uint=0xFFFFFF,alpha:Number=1.0)
		{
			super();
			
			graphics.lineStyle(stroke, color, alpha);
			graphics.moveTo(x1,0);
			graphics.lineTo(x2,0);
		}
	}
}