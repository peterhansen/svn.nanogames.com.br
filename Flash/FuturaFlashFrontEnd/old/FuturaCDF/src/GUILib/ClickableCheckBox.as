package GUILib
{
	///flash
	import flash.events.MouseEvent;
	
	///som
	import checkbox.mp3;
	
	public class ClickableCheckBox extends ClickableFadableSprite
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * o movieclip do checkbox em si
		 */
		private var checkBox:CheckBox;
		/**
		 * estou ativado ou não?
		 */
		private var activated:Boolean;
		/**
		 * o que deve ser função chamada quando houver troca de valor
		 */
		private var onToggleCallback:Function;
		/**
		 * som de quando a checkbox é clicada
		 */
		private var checkboxSound:checkbox.mp3;	
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * frame de checkbox desativada
		 */
		private static const CHECKBOX_DEACTIVATED_FRAME:int=1;
		/**
		 * frame de checkbox ativada
		 */
		private static const CHECKBOX_ACTIVATED_FRAME:int=2;
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * construtor 
		 * 
		 */
		public function ClickableCheckBox()
		{
			super();
			checkboxSound=new checkbox.mp3();
			onToggleCallback=null;
			checkBox=new CheckBox();
			checkBox.cacheAsBitmap=true;
			addChild(checkBox);
			setState(false);		
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * updateAppearance 
		 * atualiza a aparência do checkbox de acordo com seu estado interno
		 */
		private function updateAppearance():void
		{
			if (activated)
				checkBox.gotoAndStop(CHECKBOX_ACTIVATED_FRAME);
			else
				checkBox.gotoAndStop(CHECKBOX_DEACTIVATED_FRAME);
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setToggleCallback
		 * @param f A função a ser chamada no momento em que o estado for trocado
		 * 
		 */
		public function setToggleCallback(f:Function):void
		{
			onToggleCallback=f;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setState 
		 * @param s o novo estado interno do checkbox
		 * 
		 */
		public function setState(s:Boolean):void
		{
			activated=s;
			updateAppearance();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getState 
		 * @return obtem o estado interno do checkbox
		 * 
		 */
		public function getState():Boolean
		{
			return activated;			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * onMouseClick 
		 * trata click do mouse
		 * @param e uso interno do event dispatcher
		 * 
		 */
		override public function onMouseClick(e:MouseEvent):void
		{
			if (!isEnabled()) return;
			
			if (checkboxSound!=null)
				checkboxSound.play();
			
			activated=!activated;
			updateAppearance();
			
			if (onToggleCallback!=null)
				onToggleCallback();
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
	}
}