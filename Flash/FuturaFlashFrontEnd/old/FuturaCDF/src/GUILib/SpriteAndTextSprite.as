package GUILib
{
	///flash
	import flash.text.TextFormat;
	
	/**
	 * SpriteAndTextSprite
	 * Uma sprite com icone a esquerda
	 * ex: "# texto" 
	 * @author monteiro
	 * 
	 */
	public class SpriteAndTextSprite extends FadableSprite
	{
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * o icone
		 */
		private var icon:FadableSprite;
		/**
		 * o texto
		 */
		private var text:FadableText;
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * Construtor
		 * @param sprite O icone
		 * @param Text O texto que tem que aparecer do lado do icone
		 * @param color cor do texto
		 * @param format formatação do texto
		 * 
		 */
		public function SpriteAndTextSprite(sprite:FadableSprite,Text:String,color:uint=0,format:TextFormat=null)
		{
			super();
			
			icon=sprite;
			text=new FadableText(Text,color,format);
			addChild(text);
			addChild(icon);
			icon.x=0;
			icon.y=0;
			text.y=icon.y;	
			text.x=icon.x+icon.width+GUIConstants.DEFAULTTEXTHORIZONTALSPACING;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getHeight
		 * @return busca sempre o maior das alturas, para evitar que um icone trepe no outro
		 * 
		 */
		override public function getHeight():Number
		{
			if (icon.getHeight()>text.getHeight())
				return icon.getHeight();
			else
				return text.getHeight();
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getSprite
		 * @return A sprite de icone
		 * 
		 */
		public function getSprite():FadableSprite
		{
			return icon;			
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getTextSprite
		 * @return A sprite de texto
		 * 
		 */
		public function getTextSprite():FadableText
		{
			return text;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getText
		 * @return a string contida na sprite de texto
		 * 
		 */
		public function getText():String
		{
			return text.getText();
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setText
		 * define uma string para ser exibido na sprite de texto
		 * @param Text
		 * 
		 */
		public function setText(Text:String):void
		{
			text.setText(Text);
		}
		///----------------------------------------------------------------------------------------------------------
	}
}