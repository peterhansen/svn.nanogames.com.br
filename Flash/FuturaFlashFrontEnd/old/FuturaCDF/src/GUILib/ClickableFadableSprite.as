package GUILib
{
	///flash
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.ui.MouseCursor;
	
	/**
	 * ClickableFadableSprite
	 * Sprite especial que responde ao click do mouse 
	 * @author Daniel Monteiro
	 * @comment surgiu no mini-game da dengue, do vila virtual
	 */
	public class ClickableFadableSprite extends FadableSprite
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * temos controle sobre o cursos?
		 */
		private var controlCursor:Boolean;
		/**
		 * posso fazer alterações nas cores?
		 */
		private var highlightEnabled:Boolean;
		/**
		 * a função que deve ser chamada quando houver um click
		 */
		private var clickCallback:Function;
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * Construtor 
		 * 
		 */
		public function ClickableFadableSprite()
		{
			super();
			
			setOnMouseOverCallback(onOver);
			setOnMouseOutCallback(onOut);
			setOnMouseDownCallback(setDarkHue);
			setOnMouseUpCallback(setNormalHue);
			
			
			this.highlightEnabled=true;
			this.mouseChildren=false;
			this.buttonMode=true;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setHighlight 
		 * @param Set habilita ou não alterações de tonalidade
		 * 
		 */
		public function setHighlight(Set:Boolean):void
		{
			highlightEnabled=Set;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * onOver 
		 * O que fazer quando o mouse passar por cima do botão
		 * @comment precisa ser verificada. Aparentemente, não esta sendo chamada
		 */
		public function onOver():void
		{	
			if (!highlightEnabled) return;
			if (!isEnabled()) return;
			
			
			this.buttonMode=true;
			
			setLightHue();			
			if (controlCursor)
				flash.ui.Mouse.show();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * onOut 
		 * O que fazer quando o mouse sair de cima do botão
		 * @comment precisa ser verificada, pois não esta sendo chamada, ao que me parece
		 */
		public function onOut():void
		{
			this.buttonMode=false;
			setNormalHue();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setDisabled 
		 * sobrecarrega porque mesmo desabilitando o botão, ele ja começa com buttonMode, e no entanto, ele nunca é desabilitado.
		 */
		override public function setDisabled():void
		{
			super.setDisabled();
			this.buttonMode=false;
		}
	}
}