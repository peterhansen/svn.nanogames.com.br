package FuturaGUI
{
	///flash
	import FuturaGameEngine.FuturaPlayer;
	
	import GUILib.ClickableCheckBox;
	import GUILib.ClickableFadableSprite;
	import GUILib.FadableSprite;
	import GUILib.FadableText;
	import GUILib.GUIConstants;
	import GUILib.HorizontalLineSeparatorSprite;
	import GUILib.SpriteAndTextSprite;
	import GUILib.SpriteHorizontalList;
	import GUILib.SpriteVerticalList;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	/**
	 * FuturaScoreWindow
	 * janela de scores finais
	 * @author Daniel Monteiro
	 * */
	public class FuturaScoreWindow extends FuturaBaseWindow
	{
		/**
		 * botão para fechar a janela (e o jogo)
		 */
		private var buttonCloseSprite:ClickableFadableSprite;
		private var buttonReplaySprite:ClickableFadableSprite;
		private var buttonLayout:SpriteHorizontalList;
		/**
		 * lista de nomes dos jogadores
		 */
		private var namesList:SpriteVerticalList;
		/**
		 * lista de conteúdo da janela
		 */
		private var listPanes:SpriteVerticalList;
		/**
		 * qual desses é o jogador
		 */
		private var myId:int;
		//private var subTitleText:FadableText;
		private var winAnim:MovieClipWrapper;		
		private var animSprite:FadableSprite;
		private var showAnimTimer:Timer;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setOkCallback
		 * define o callback de "fechar a janela"
		 * @param Function f callback para quando se clica em Ok
		 * */		
		public function setOkCallback(f:Function):void
		{
			if (buttonCloseSprite==null || f==null || namesList==null )
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::setOkCallback");
			
			buttonCloseSprite.setOnMouseClickCallback(f);			
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * updatePlayerList
		 * atualiza a lista visível de jogadores
		 * @author Daniel Monteiro
		 * @param playerList lista de jogadores (instancias de FuturaPlayer)
		 * */		
		public function updatePlayerList(playerList:Array, localPlayerIsWinner:Boolean):void
		{			
			//trace("updatePlayerList");
			
			///verifica a validade do parametro enviado
			if (playerList==null || playerList.length==0 /*|| myId <0*/)			
				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_GAME_STATE,"FuturaScoreWindow::updatePlayerList");
			
			if (namesList == null)
				return;
			
			///variáveis usadas pela função
			var player:FuturaPlayer;			

			playerList.sort(sortBetweenTwoItems);
			
			for (y=playerList.length -1;y >= 0;y--)
			{
				player=playerList[y];
				
				player.setShowTime(true);
				
				if (player==null || !(player is FuturaPlayer))
					FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_GAME_STATE,"FuturaScoreWindow::updatePlayerList");
				
//				if (!player.getActive())
					addPlayer(player );
			}			
			
			
			namesList = null;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function sortBetweenTwoItems(a:FuturaPlayer, b:FuturaPlayer):Number
		{			
			if (a.getActive() == b.getActive())
			{
				if (a.getScore() == b.getScore())
				{
					
					if (a.getTime() < b.getTime())
						return 1;
					else if (a.getTime() > b.getTime())
						return -1;						
					else
						return 0;
				} 
				else if (a.getScore() > b.getScore())
					return 1;			
			}
			else if (a.getActive() && !b.getActive())
				return 1;
			
			return -1;
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * removePlayer
		 * remove da lista um jogador especifico 
		 * @param p id do jogador a ser removido
		 * 
		 */
		public function removePlayer(p:int):void
		{
			namesList.getItemAt(p).visible=false;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * addPlayer
		 * adiciona um novo jogador na lista 
		 * @param Name nome do jogador
		 * @param Score placar do jogador
		 */
		public function addPlayer(player:FuturaPlayer, win:Boolean = false):void
		{
			//trace ("addPlayer:"+Name);
			///variaveis
			var name:SpriteHorizontalList;
			var nameString:String;
			
			var statusSprite:FadableSprite;
			var fadableText:FadableText;
			
			///alocações
			name=new SpriteHorizontalList();
			if (name==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::addPlayer");
			
			statusSprite=new FadableSprite();
			if (statusSprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::addPlayer");
			
			
			///configurações dos items
			var star:Star;
			star=new Star();
			if (star==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::addPlayer");				
			star.y+=star.height/2;				
			star.x+=star.width/2;
			statusSprite.addChild(star);				
			star.visible = win;
			
			name.addItem(statusSprite);
			
			///nome
			nameString = (namesList.numChildren+1).toString()+"° ";
			nameString += player.getName();
			if (!player.getActive())
				nameString += " (Abandonou)";
			fadableText=new FadableText(nameString+" - ",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getBoldTextFormat());
			if (fadableText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::addPlayer");
			
			name.addItem(fadableText,5);
			
			///placar
			var pointsText:String;
			if (player.getScore() == 1)
				pointsText = " ponto";
			else
				pointsText = " pontos";
			fadableText=new FadableText(player.getScore().toString()+ pointsText,FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (fadableText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::addPlayer");
			name.addItem(fadableText);
			
			///tempo
			//	if (player.getShowTime())
			{
				var minutos:int=player.getTime()/60;
				var segundos:int=player.getTime()- (minutos * 60);
				var fullTime:String="";
				
				if (minutos != 0 || segundos != 0)
					fullTime += " em ";				
				
				if ( minutos > 0)
				{
					fullTime += minutos.toString();					
					fullTime += "min";
				}
				
				if (minutos != 0 && segundos != 0)
					fullTime += " e ";
				
				if ( segundos > 0)				
				{					
					fullTime += segundos.toString();					
					fullTime += "s";
				}
				
				fadableText=new FadableText(fullTime,FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
				
				if (fadableText==null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaScoreWindow::addPlayer");
				
				name.addItem(fadableText);
			}
			
			///por fim, se deu tudo certo, adiciona o nome
			namesList.addItem(name,15);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * @param p
		 * 
		 */
		public function setCurrentPlayer(p:int):void
		{
			var name:SpriteHorizontalList;
			var scoreText:FadableText;
			
			name =	namesList.getItemAt(p) as SpriteHorizontalList;			
			scoreText = name. getItemAt(1) as FadableText;			
			scoreText.setText("*"+scoreText.getText());
		}
		///--------------------------------------------------------------------------------------------------------------------
		//		/**
		//		 * 
		//		 * @param p
		//		 * @param s
		//		 * 
		//		 */
		//		public function setPlayerScore(p:int,s:int):void
		//		{
		//			///checagem de parametros
		//			if (p < 0 || s < 0)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);				
		//			
		//			///variaveis
		//			var name:SpriteHorizontalList;
		//			var status:Status;				
		//			var statusSprite:FadableSprite;			
		//			var scoreText:FadableText;
		//			
		//			name =	namesList.getItemAt(p) as SpriteHorizontalList;		
		//			if (name == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			statusSprite = name.getItemAt(0);
		//			if (statusSprite == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			scoreText = name. getItemAt(2) as FadableText;
		//			if (scoreText == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			status = statusSprite.getChildAt(0) as Status;
		//			if (status == null)
		//				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR);
		//			
		//			status.gotoAndStop("Ready");			
		//			scoreText.setText(s.toString());			
		//		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * 
		 */
		public function FuturaScoreWindow()
		{
			super();
			
			myId=-1;
			buttonReplaySprite=new ClickableFadableSprite();			
			buttonCloseSprite=new ClickableFadableSprite();
			buttonLayout= new SpriteHorizontalList();
			namesList=new SpriteVerticalList();			
			try
			{
				winAnim = new MovieClipWrapper(FuturaConstants.SERVER_BASE_URL+"/swf/animacao_vencedor.swf");		
				animSprite = new FadableSprite();
			}
			catch (e:Error)
			{
			
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * 
		 */
		override public function initWindow():void
		{
			super.initWindow();
			
			var separator:HorizontalLineSeparatorSprite;
			listPanes=new SpriteVerticalList();
			
			this.addWidget(listPanes);
			addChild(animSprite);
			///<formatos de texto>
			var titleTextFormat:TextFormat=new TextFormat();
			var regularTextFormat:TextFormat=new TextFormat();
			var countTextFormat:TextFormat=new TextFormat();
			
			titleTextFormat.size=22;
			regularTextFormat.size=12;
			countTextFormat.size=18;			
			
			
			titleTextFormat.font="Arial";
			regularTextFormat.font="Arial";
			countTextFormat.font="Arial";
			
			countTextFormat.bold=true;			
			
			var white:uint=0xFFFFFF;
			///</formatos de texto>
			
			
			///<titulo janela>
			var titleText:FadableText=new FadableText("Fim de jogo",white,titleTextFormat);
			separator=new HorizontalLineSeparatorSprite(0,width-60-65);
			listPanes.addItem(titleText);
			listPanes.addItem(separator,7);		
			listPanes.x=65;
			listPanes.y=45;
			///<titulo janela>
			
			///<contagem regressiva>
			
			//		subTitleText=new FadableText("Resultados finais desta mesa:",white,regularTextFormat);
			
			
			//		listPanes.addItem(subTitleText,25);
			///<contagem regressiva>			
			
			
			///<lista de nomes>
			listPanes.addItem(namesList,20);						
			///</lista de nomes>
			
			///<botao>			
			listPanes.addItem(buttonLayout, 180 + 50);
			var buttonReplay:PlayAgain=new PlayAgain();			
			buttonReplaySprite.addChild(buttonReplay);
			buttonReplay.y-=buttonReplay.height/2;
			buttonLayout.addItem(buttonReplaySprite);
			
			var buttonClose:Close=new Close();			
			buttonCloseSprite.addChild(buttonClose);
			buttonLayout.addItem(buttonCloseSprite,/*-35*/90);			
			
			buttonLayout.recalculatePositions();
			
			//buttonLayout.x = 95;
			//buttonLayout.x = 35;
			///</botao>
			
		}	
		
		public function setReplayCallback(f:Function):void
		{
			buttonReplaySprite.setOnMouseClickCallback(f);
		}
		
		///--------------------------------------------------------------------------------------------------------------------
		override public function startShowAnimation(f:Function=null, alphaInitial:Number=0, targetAlpha:Number=1.0):void
		{
			super.startShowAnimation(f,alphaInitial,targetAlpha);
			listPanes.setInvisible();
			showAnimTimer = new Timer(FuturaConstants.FINAL_ANIMATION_TIME * GUIConstants.SECONDS);
			showAnimTimer.addEventListener(TimerEvent.TIMER, animCompleteHandler);
			showAnimTimer.start();
			animSprite.addChild(winAnim.getMc());
			animSprite.startShowAnimation();
			winAnim.getMc().x=80;
			winAnim.getMc().y=10;
			winAnim.getMc().play();
		}
		
		private function animCompleteHandler(e:Event):void
		{
			showAnimTimer.stop();
			showAnimTimer.reset();
			showAnimTimer = null;
			
			winAnim.getMc().stop();
			animSprite.setInvisible();
			listPanes.setVisible();
			
		}
	}
}
