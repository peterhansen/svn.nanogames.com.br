package FuturaGUI
{
	import flash.display.MovieClip;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	
	public class MovieClipWrapper
	{
		private var mc:MovieClip;
		private var loadedCallback:Function;
		
		public function getMc():MovieClip
		{
			return mc;
		}
		
		public function MovieClipWrapper(url:String, callback:Function = null)
		{
			mc = null;			
			var mLoader:Loader = new Loader();
			var mRequest:URLRequest = new URLRequest(url);
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteHandler);
			loadedCallback = callback; 
			mLoader.load(mRequest);			
		}
		
		private function onCompleteHandler(loadEvent:Event):void
		{
			mc = (loadEvent.currentTarget.content as MovieClip);
			if (loadedCallback != null)
				loadedCallback(mc);
		}		
	}
}