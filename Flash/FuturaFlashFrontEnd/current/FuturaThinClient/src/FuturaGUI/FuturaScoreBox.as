package FuturaGUI
{
	///GUILib
	import GUILib.FadableSprite;
	
	/**
	 * FuturaScoreBox
	 * caixa de placar
	 * @author Daniel Monteiro
	 * */
	public class FuturaScoreBox extends FadableSprite
	{
		/**
		 * caixa de score
		 */
		private var pointsBox:PointsBox;
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * FuturaScoreBox
		 * construtor
		 * */
		public function FuturaScoreBox()
		{
			super();
			pointsBox=new PointsBox();
			addChild(pointsBox);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setDisplayItems
		 * define os textos a serem exibidos na caixa de scores
		 * @param int hits acertos
		 * @param int misses erros
		 * @param int overall score final
		 * */
		public function setDisplayItems(hits:int, misses:int, overall:int):void
		{
			if (overall == 1)
				pointsBox.Points.text= overall.toString() + " " + FuturaStrings.POINTS_TEXT_SINGULAR  ;
			else
				pointsBox.Points.text= overall.toString() + " " + FuturaStrings.POINTS_TEXT_PLURAL  ;
			
			if (hits == 1)
				pointsBox.Hits.text= hits.toString() + " "+ FuturaStrings.HITS_TEXT_SINGULAR  ;
			else
				pointsBox.Hits.text= hits.toString() + " "+ FuturaStrings.HITS_TEXT_PLURAL  ;
			
			if (misses == 1)
				pointsBox.Errors.text= misses.toString()+ " "+ FuturaStrings.MISSES_TEXT_SINGULAR  ;
			else
				pointsBox.Errors.text= misses.toString()+ " "+ FuturaStrings.MISSES_TEXT_PLURAL  ;		
		}
		///--------------------------------------------------------------------------------------------------------------------		
	}
}