package FuturaGUI
{
	///flash
	import GUILib.ClickableCheckBox;
	import GUILib.ClickableFadableSprite;
	import GUILib.FadableSprite;
	import GUILib.FadableText;
	import GUILib.GUIConstants;
	import GUILib.HorizontalLineSeparatorSprite;
	import GUILib.SpriteAndTextSprite;
	import GUILib.SpriteVerticalList;
	
	import checkbox.mp3;
	
	import clock_ticks.wav;
	
	import clock_ticks_02.wav;
	
	import flash.display.DisplayObject;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	import resposta_correta.mp3;
	
	import resposta_errada.mp3;
	
	/**
	 * FuturaQuestionWindow
	 * Janela de perguntas
	 * @author Daniel Monteiro
	 * */
	public class FuturaQuestionWindow extends FuturaBaseWindow
	{
		/**
		 * texto da pergunta 
		 */
		private var questionText:FadableText;
		
		/**
		 * lista de opções de resposta (precisa estar no escopo da classe, para que se possa reposicionar os items) 
		 */
		private var listOptions:SpriteVerticalList;
		
		/**
		 * timer de tempo de resposta (isso deve mudar) 
		 */
		private var timeOutTimer:Timer;
		
		/**
		 * tempo para os outros verem a minha resposta (e vice-e-versa) 
		 */
		private var showAnswerTimer:Timer;
		
		/**
		 * relógio de animação de tempo restante 
		 */
		private var clock:Clock;
		
		/**
		 * função para indicar que a pergunta foi respondida 
		 */
		private var answeredCallback:Function;
		
		/**
		 * qual foi a resposta 
		 */
		private var answer:int;
		
		/**
		 * checkbox da 1º pergunta 
		 */
		private var check1:ClickableCheckBox;
		
		/**
		 * checkbox da 2º pergunta 
		 */
		private var check2:ClickableCheckBox;
		
		/**
		 * checkbox da 3º pergunta 
		 */
		private var check3:ClickableCheckBox;
		
		/**
		 * checkbox da 4º pergunta 
		 */
		private var check4:ClickableCheckBox;		
		
		/**
		 * item (texto + checkbox) da 1º pergunta 
		 */
		private var sprite1:SpriteAndTextSprite;
		
		/**
		 * item (texto + checkbox) da 2º pergunta 
		 */
		private var sprite2:SpriteAndTextSprite;
		
		/**
		 * item (texto + checkbox) da 3º pergunta 
		 */
		private var sprite3:SpriteAndTextSprite;
		
		/**
		 * item (texto + checkbox) da 4º pergunta 
		 */
		private var sprite4:SpriteAndTextSprite;
		
		/**
		 * sou eu que vou responder? 
		 */
		private var amIAnswering:Boolean;
		
		/**
		 * botão de responders 
		 */
		private var buttonSprite:ClickableFadableSprite;
		
		/**
		 * É minha vez? 
		 */
		private var isItMyTurn:Boolean;
		
		/**
		 * tempo para cada resposta
		 */
		private var speed:int;
		
		/**
		 * texto de título
		 */
		private var titleText:FadableText;
		
		/**
		 * timer para tickar o relógio (tick, tock..)
		 */
		private var clockTickTimer:Timer;
		
		/**
		 * tempo levado para dar a ultima resposta 
		 */
		private var elapsedTime:int;
		/**
		 * painel esquerdo 
		 */
		private var listLeftPane:SpriteVerticalList;
		
		private var clockRedMask:Shape;
		private var clockSprite:FadableSprite;
		private var answering:Boolean;
		private var playedSoundForThisRound:Boolean;
		private var alarmSound:clock_ticks_02.wav;
		private var currentAlarmSoundChannel:SoundChannel;
		///--------------------------------------------------------------------------------------------------------------------
		public function getElapsedTime():int
		{
			return elapsedTime;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * checkIntegrityFor
		 * centraliza as verificações de erro, de modo a poupar trabalho e garantir uma melhor cobertura contra
		 * erros
		 * @param String m 
		 * */
		public function checkIntegrityFor(forWhat:String):void
		{
			if (questionText == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (listOptions == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (clock == null)			
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			/*
			if (answeredCallback == null)
			FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")");
			*/
			
			if (showAnswerTimer == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (check1 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (check2 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (check3 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (check4 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (sprite1 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (sprite2 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (sprite3 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
			if (sprite4 == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR+"("+forWhat+")","FuturaQuestionWindow::checkIntegrityFor");
			
		}		
		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * setGameSpeed 
		 * @param time Qual vai ser a velocidade do jogo
		 * 
		 */
		public function setGameSpeed(time:int=FuturaConstants.TIMENORMAL):void
		{
			this.speed=time;
//			if ( time == FuturaConstants.TIMENORMAL )
				clockRedMask.alpha = (15)/(60.0);
//			else
//				clockRedMask.alpha = (15)/(30.0);		
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getAnswer
		 * retorna a reposta do usuário para a ultima pergunta
		 * @returns int a dada pelo usuário [0..5]
		 * @comment 5 é dado quando o jogador não responde nenhuma, ou não responde a tempo
		 * */
		public function getAnswer():int
		{
			checkIntegrityFor("getAnswer");
			
			return answer;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check1Clicked
		 * 1º checkbox foi clicada
		 * */
		private function check1Clicked():void
		{
			checkIntegrityFor("check1Clicked");
			
			if (!isItMyTurn)
			{
				check1.setState(false);
				return
			}
			
			if (check1.getState())
			{
				answer=0;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}
			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}
			
			///marca uma exclusivamente
			check2.setState(false);				
			check3.setState(false);
			check4.setState(false);	
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check2Clicked
		 * 2º checkbox foi clicada
		 * */		
		private function check2Clicked():void
		{		
			checkIntegrityFor("check2Clicked");
			
			if (!isItMyTurn)
			{
				check2.setState(false);
				return
			}
			
			
			if (check2.getState())
			{
				answer=1;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}

			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}

			
			///marca uma exclusivamente
			check1.setState(false);
			check3.setState(false);
			check4.setState(false);	
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check3Clicked
		 * 3º checkbox foi clicada
		 * */		
		private function check3Clicked():void
		{
			checkIntegrityFor("check3Clicked");
			
			if (!isItMyTurn)
			{
				check3.setState(false);
				return
			}
			
			
			if (check3.getState())
			{
				answer=2;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}

			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}

			
			///marca uma exclusivamente
			check1.setState(false);
			check2.setState(false);
			check4.setState(false);	
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * check4Clicked
		 * 4º checkbox foi clicada
		 * */
		private function check4Clicked():void
		{
			checkIntegrityFor("check4Clicked");			
			
			if (!isItMyTurn)
			{
				check4.setState(false);
				return
			}
			
			
			if (check4.getState())
			{
				answer=3;
				this.buttonSprite.setEnabled();
				this.buttonSprite.setNormalHue();
			}

			else
			{
				answer=FuturaConstants.NOANSWER;
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
			}

			
			///marca uma exclusivamente
			check1.setState(false);
			check2.setState(false);
			check3.setState(false);	
			
		}
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setDisplayedAnswer 
		 * @param ans qual deve se a resposta a ser exibida como marcada
		 * 
		 */
		public function setDisplayedAnswer(ans:int , isCorrect:Boolean	):void
		{
			stopTimer();
			
			//clock.visible=false;
			buttonSprite.visible=false;
			
			check1.setState(false);
			check2.setState(false);
			check3.setState(false);
			check4.setState(false);
			
			
			var tmp:FadableSprite;
			var right:Right;
			var wrong:Wrong;

			tmp = sprite1.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			right.visible = false;
			wrong.visible = false;
			
			tmp = sprite2.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			right.visible = false;
			wrong.visible = false;
			
			
			tmp = sprite3.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			right.visible = false;
			wrong.visible = false;
			
			
			tmp = sprite4.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			right.visible = false;
			wrong.visible = false;
	
			switch (ans)
			{
				case 0:
					check1.setVisible();
					check1.setState(true);
					
					tmp = sprite1.getChildAt(2) as FadableSprite;
					right = tmp.getChildAt(0) as Right;
					wrong = tmp.getChildAt(1) as Wrong;
					
					if (isCorrect)
						right.visible = true;
					else
						wrong.visible = true;
					
					break;
				case 1:
					check2.setVisible();
					check2.setState(true);
					
					tmp = sprite2.getChildAt(2) as FadableSprite;
					right = tmp.getChildAt(0) as Right;
					wrong = tmp.getChildAt(1) as Wrong;
					
					if (isCorrect)
						right.visible = true;
					else
						wrong.visible = true;
					
					break;
				case 2:
					check3.setVisible();
					check3.setState(true);
					
					tmp = sprite3.getChildAt(2) as FadableSprite;
					right = tmp.getChildAt(0) as Right;
					wrong = tmp.getChildAt(1) as Wrong;
					
					if (isCorrect)
						right.visible = true;
					else
						wrong.visible = true;
					
					break;
				case 3:
					check4.setVisible();
					check4.setState(true);
					
					tmp = sprite4.getChildAt(2) as FadableSprite;
					right = tmp.getChildAt(0) as Right;
					wrong = tmp.getChildAt(1) as Wrong;
					
					if (isCorrect)
						right.visible = true;
					else
						wrong.visible = true;
					
					break;
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * buttonClick
		 * o botão de resposta foi clicado
		 * @comment não preciso verificar o callback...
		 * */
		private function buttonClick():void
		{
			if (!isItMyTurn)
				return;
			
		//	trace("***BUTTONCLICK");
			if (clockTickTimer != null)
				elapsedTime=clockTickTimer.currentCount;
			else
				elapsedTime = speed;
			
			stopTimer();
			checkIntegrityFor("buttonClick");
			buttonSprite.setDisabled();
			buttonSprite.setDarkHue();
			
			check1.setDisabled();
			check1.setDarkHue();
			
			check2.setDisabled();
			check2.setDarkHue();

			check3.setDisabled();
			check3.setDarkHue();

			check4.setDisabled();
			check4.setDarkHue();
			
			///esconde rapidamente a janela, para evitar artefatos...
			//setInvisible();			
			
			///limpa o timer
			
			
			///haverá necessariamente uma função registrada para saber que a pergunta foi respondida
			var x:int = 0;
			
			for (x = 0; x < 3; x++)
			{
				try
				{
					answeredCallback();
					return;
				}
				catch (e:Error)
				{
				
				}
			}
			
			buttonSprite.setEnabled();
			buttonSprite.setNormalHue();
			
			check1.setEnabled();
			check1.setNormalHue();
			
			check2.setEnabled();
			check2.setNormalHue();
			
			check3.setEnabled();
			check3.setNormalHue();
			
			check4.setEnabled();
			check4.setNormalHue();
			
		}
		///--------------------------------------------------------------------------------------------------------------------	
		/**
		 * startTimer 
		 * inicia o timer para a resposta
		 */
		private function startTimer():void
		{
			stopTimer();
			//trace("***TIMER STARTED");
			timeOutTimer=new Timer(speed*GUIConstants.SECONDS,1);			
			clockTickTimer=new Timer(1*GUIConstants.SECOND,FuturaConstants.CLOCKFRAMES);			
			clockTickTimer.addEventListener(TimerEvent.TIMER,tickClockEvent);
			timeOutTimer.addEventListener(TimerEvent.TIMER,timeOutCallback);
			clock.alpha = 1.0;
			clockTickTimer.start();
			timeOutTimer.start();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * tickClock 
		 * evento para avançar a frame do relógio
		 * @param e uso interno do timer
		 * 
		 */
		private function tickClockEvent(e:Event):void
		{
			tickClock();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function tickClock():void
		{
			setClockTimeElapsed( getClockTimeElapsed() + 1);				
			
			if (speed == FuturaConstants.TIMETURBO)
			{
				if (getClockTimeElapsed() >= FuturaConstants.TIMETURBO - FuturaConstants.HURRY_UP_TIME && !playedSoundForThisRound)
				{
					playedSoundForThisRound = true;
					
					if (alarmSound != null)
						currentAlarmSoundChannel = alarmSound.play();
				}			
			}
			else
				if (getClockTimeElapsed() >= FuturaConstants.TIMENORMAL - FuturaConstants.HURRY_UP_TIME && !playedSoundForThisRound)
				{
					playedSoundForThisRound = true;
					
					if (alarmSound != null)
						currentAlarmSoundChannel = alarmSound.play();
				}



			if (clockRedMask.alpha < 0.7 && getClockTimeElapsed() > 15)
				clockRedMask.alpha += 0.7/(60.0-10.0);
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * stopTimer 
		 * Para o timer de resposta
		 */
		public function stopTimer():void
		{
			trace ("currentAlarmSoundChannel@stopTimer:"+currentAlarmSoundChannel);
			
			if (currentAlarmSoundChannel != null)
				currentAlarmSoundChannel.stop();

			currentAlarmSoundChannel = null;
			
		//	trace("***TIMER STOPPING");
			if (timeOutTimer != null)
			{
				elapsedTime = clockTickTimer.currentCount;

				timeOutTimer.stop();
				timeOutTimer.removeEventListener(TimerEvent.TIMER,timeOutCallback);
				timeOutTimer=null;

			}

			if (clockTickTimer != null)
			{
				clockTickTimer.stop();
				clockTickTimer.removeEventListener(TimerEvent.TIMER,tickClock);
				clockTickTimer=null;
			}	
		
		//	clock.gotoAndStop(2);
		}		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * newQuestion
		 * Temos uma nova pergunta para ser exibida
		 * @param String mainText O texto da pergunta em sí
		 * @param Array options As opções de resposta
		 * @param int time Quanto tempo será dado para a resposta
		 * */
		public function newQuestion(mainText:String,options:Array,time:int, myTurn:Boolean=true, playerName:String="", timeElapsed:String="2"):void
		{
			answering = myTurn;
			playedSoundForThisRound = false;
			checkIntegrityFor("newQuestion");
			buttonSprite.setEnabled();
			questionText.setText(mainText);
			questionText.setDim(FuturaConstants.NUMLINESQUESTION,0,0,FuturaConstants.QUESTION_BOX_WIDTH+2*GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);

//			sprite1.width = FuturaConstants.ANSWER_BOX_WIDTH;
//			sprite2.width = FuturaConstants.ANSWER_BOX_WIDTH;
//			sprite3.width = FuturaConstants.ANSWER_BOX_WIDTH;
//			sprite4.width = FuturaConstants.ANSWER_BOX_WIDTH;
			
			
			
			buttonSprite.setEnabled();
			buttonSprite.setNormalHue();
			
			listLeftPane.recalculatePositions();
			
			clock.visible=true;
			
			
			if (!myTurn )
			{
				if (playerName!="")
					this.titleText.setText(playerName+" respondendo");
				else
					this.titleText.setText("Pergunta");
			}
			else
				this.titleText.setText("É a sua vez!");
			
			///checa a integridade dos parametros			
			if (mainText==null || mainText=="" || options == null || time <=0)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaQuestionWindow::newQuestion");
			
			if (2 * options.length > listOptions.numChildren)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaQuestionWindow::newQuestion");
			
			///variaveis
			var sprite:FadableSprite;
			var firstIdToHide:int;
			
			
			///limpa as opções da pergunta anterior
			check1.setState(false);
			check2.setState(false);	
			check3.setState(false);
			check4.setState(false);
			check1.setEnabled();
			check1.setNormalHue();
			
			check2.setEnabled();
			check2.setNormalHue();
			
			check3.setEnabled();
			check3.setNormalHue();
			
			check4.setEnabled();
			check4.setNormalHue();

			
			///sem resposta por enquanto
			answer=FuturaConstants.NOANSWER;		
			
			///para cada opção
			for (var c:int=0;c<options.length;c++)
			{
				sprite=listOptions.getItemAt(2*c);
				
				///aqui, não há garantias que tudo é válido
				if (sprite==null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaQuestionWindow::newQuestion");
				
				///define um novo texto	e o torna visível
				if (!(sprite is FadableSprite))
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaQuestionWindow::newQuestion");
				
				(sprite as SpriteAndTextSprite).setText(options[c] as String);
				(sprite as SpriteAndTextSprite).getTextSprite().setDim(FuturaConstants.NUMLINESANSWER,0,0,FuturaConstants.QUESTION_BOX_WIDTH);
				
				if ( ( (2*c)-1 ) > 0)
					listOptions.getItemAt((2*c)-1).visible=true;
				
				sprite.visible=true;
			}
			
			///o item visual seguinte à ultima pergunta feita visível
			firstIdToHide=(2*options.length)-1;			
			//trace("recalculate positions");
			///recalcula as posições de acordo com  as novas posições
			listOptions.recalculatePositions();
			
			///e torna todo o resto invisivel
			for (var d:int=firstIdToHide;d<listOptions.numChildren;d++)
			{
				if (listOptions.getItemAt(d)==null)
					FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaQuestionWindow::newQuestion");
				
				listOptions.getItemAt(d).visible=false;		
			}
			
			//clock.gotoAndStop(2);
			
			if (myTurn)
			{
				this.buttonSprite.setDisabled();
				this.buttonSprite.setDarkHue();
				this.buttonSprite.setVisible();
				setBGOpacity(0.95);	
				check1.setVisible();
				check2.setVisible();
				check3.setVisible();
				check4.setVisible();	
				check1.setEnabled();
				check2.setEnabled();
				check3.setEnabled();
				check4.setEnabled();
				
			}
			else
			{
				this.buttonSprite.setInvisible();
				setBGOpacity(0.8);
				check1.setInvisible();
				check2.setInvisible();
				check3.setInvisible();
				check4.setInvisible();	
				check1.setDisabled();
				check2.setDisabled();
				check3.setDisabled();
				check4.setDisabled();
				
			}
			isItMyTurn=myTurn;
			
			sprite1.getTextSprite().setDim(FuturaConstants.NUMLINESANSWER,sprite1.getTextSprite().x,sprite1.getTextSprite().y,sprite1.getTextSprite().x + FuturaConstants.ANSWER_BOX_WIDTH); 
			sprite2.getTextSprite().setDim(FuturaConstants.NUMLINESANSWER,sprite2.getTextSprite().x,sprite2.getTextSprite().y,sprite2.getTextSprite().x + FuturaConstants.ANSWER_BOX_WIDTH);
			sprite3.getTextSprite().setDim(FuturaConstants.NUMLINESANSWER,sprite3.getTextSprite().x,sprite3.getTextSprite().y,sprite3.getTextSprite().x + FuturaConstants.ANSWER_BOX_WIDTH);
			sprite4.getTextSprite().setDim(FuturaConstants.NUMLINESANSWER,sprite4.getTextSprite().x,sprite4.getTextSprite().y,sprite4.getTextSprite().x + FuturaConstants.ANSWER_BOX_WIDTH);
			
			sprite1.adjustSpacing();
			sprite2.adjustSpacing();
			sprite3.adjustSpacing();
			sprite4.adjustSpacing();
			
			var tmp:FadableSprite;
			var right:Right;
			var wrong:Wrong;
			
			tmp = sprite1.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			tmp.x= FuturaConstants.ANSWER_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2 /*+  GUIConstants.DEFAULTTEXTHORIZONTALSPACING*/;
			right.visible=false;
			wrong.visible=false;

			tmp = sprite2.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			tmp.x= FuturaConstants.ANSWER_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2 /*+  GUIConstants.DEFAULTTEXTHORIZONTALSPACING*/;
			right.visible=false;
			wrong.visible=false;

			
			tmp = sprite3.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			tmp.x= FuturaConstants.ANSWER_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2 /*+  GUIConstants.DEFAULTTEXTHORIZONTALSPACING*/;
			right.visible=false;
			wrong.visible=false;

			
			tmp = sprite4.getChildAt(2) as FadableSprite;
			right = tmp.getChildAt(0) as Right;
			wrong = tmp.getChildAt(1) as Wrong;
			tmp.x= FuturaConstants.ANSWER_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2 /*+  GUIConstants.DEFAULTTEXTHORIZONTALSPACING*/;
			right.visible=false;
			wrong.visible=false;
			listOptions.recalculatePositions();
			//clock.gotoAndStop(2);
			startShowAnimation(startTimer);			
			clock.gotoAndStop(parseInt(timeElapsed));
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * timeOutCallback
		 * evento para "tempo acabou"
		 * @param TimerEvent e uso interno do Timer 
		 * */
		private function timeOutCallback(e:TimerEvent):void
		{
			///primeiro de tudo, parar o timer
			elapsedTime = clockTickTimer.currentCount;
			
			if (timeOutTimer != e.target)
			{
				trace("erro de consistência! callback de timer chamado com timer nulo! ou não correspondente");
			}
			
			stopTimer();

			
			
			///verificar se o sistema esta ok
			checkIntegrityFor("timeOutCallback");
			///esconder a janela
			setInvisible();
			///e passar a resposta ao jogo
			answeredCallback();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setAnsweredCallback
		 * define a função de callback
		 * @comment não pode haver checagem de integridade, pois ainda não há callback registrado
		 * */		
		public function setAnsweredCallback(f:Function):void
		{
			///se o callback não for válido, reclame
			if (f==null)
				FuturaSafeGuard.nonFatalError(FuturaStrings.INTERNAL_ERROR,"FuturaQuestionWindow::setAnsweredCallback");
			
			answeredCallback=f;
		}
		///--------------------------------------------------------------------------------------------------------------------	
		/**
		 * FuturaQuestionWindow
		 * construtor
		 * */
		public function FuturaQuestionWindow()
		{
			super();
			
			var tmp:FadableSprite;
			var right:Right;
			var wrong:Wrong;		
			
			currentAlarmSoundChannel = null;
			answering = false;
			elapsedTime=0;
			timeOutTimer=null;
			
			showAnswerTimer=new Timer(2*GUIConstants.SECONDS,1);
			if (showAnswerTimer==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			questionText=new FadableText("texto da pergunta",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (questionText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			check1=new ClickableCheckBox();
			if (check1==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			sprite1=new SpriteAndTextSprite(check1,"teste de item 1",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite1==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");

			tmp = new FadableSprite();
			right = new Right();
			right.scaleX=0.5;
			right.scaleY=0.5;
			right.x+=right.width/2;
			right.y+=right.height/2;
			tmp.addChild( right);
			wrong = new Wrong();
			wrong.x+=right.width/2;
			wrong.y+=right.height/2;			
			tmp.addChild( wrong);
			tmp.x= FuturaConstants.QUESTION_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2;
			sprite1.addChild(tmp);	
			
			check2=new ClickableCheckBox();
			if (check2==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			sprite2=new SpriteAndTextSprite(check2,"teste de item 2",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite2==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
		
			tmp = new FadableSprite();
			right = new Right();
			right.scaleX=0.5;
			right.scaleY=0.5;
			right.x+=right.width/2;
			right.y+=right.height/2;
			tmp.addChild( right);
			wrong = new Wrong();
			wrong.x+=right.width/2;
			wrong.y+=right.height/2;			
			tmp.addChild( wrong);
			tmp.x= FuturaConstants.QUESTION_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2;
			sprite2.addChild(tmp);
			
			
			check3=new ClickableCheckBox();
			if (check3==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			sprite3=new SpriteAndTextSprite(check3,"teste de item 3",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite3==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");

			tmp = new FadableSprite();
			right = new Right();
			right.scaleX=0.5;
			right.scaleY=0.5;
			right.x+=right.width/2;
			right.y+=right.height/2;
			tmp.addChild( right);
			wrong = new Wrong();
			wrong.x+=right.width/2;
			wrong.y+=right.height/2;			
			tmp.addChild( wrong);
			tmp.x= FuturaConstants.QUESTION_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2;
			sprite3.addChild(tmp);
			
			
			check4=new ClickableCheckBox();
			if (check4==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			sprite4=new SpriteAndTextSprite(check4,"teste de item 4",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getRegularTextFormat());
			if (sprite4==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");

			tmp = new FadableSprite();
			right = new Right();
			right.scaleX=0.5;
			right.scaleY=0.5;
			right.x+=right.width/2;
			right.y+=right.height/2;
			tmp.addChild( right);
			wrong = new Wrong();
			wrong.x+=right.width/2;
			wrong.y+=right.height/2;			
			tmp.addChild( wrong);
			tmp.x= FuturaConstants.QUESTION_BOX_WIDTH;
			tmp.x+=(right.width + wrong.width)/2;
			sprite4.addChild(tmp);
			
			
			listOptions=new SpriteVerticalList();
			if (listOptions==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			clock = new Clock();
			
			if (clock == null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::FuturaQuestionWindow");
			
			var part:Object = clock.getChildAt(0) as DisplayObject;
			
			clockRedMask = new Shape();
			clockRedMask.graphics.beginFill( 0xFF0000 , 1.0);
			clockRedMask.graphics.drawCircle(part.x, part.y, (clock.width / 2) * 0.9);
			clockRedMask.graphics.endFill();				
			clock.addChildAt(clockRedMask,1);
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * initWindow
		 * inicializa a janela
		 * */
		override public function initWindow():void
		{
			super.initWindow();
			
			checkIntegrityFor("initWindow");
			
			///variaveis
			var separator:HorizontalLineSeparatorSprite;
			var listPanes:SpriteVerticalList;
			var listRightPane:SpriteVerticalList;
			var button:AnswerButton;
			
			///alocações			
			listPanes=new SpriteVerticalList();
			if (listPanes==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			titleText=new FadableText("Pergunta",FuturaConstants.COLORWHITEVALUE,FuturaBaseWindow.getTitleTextFormat());
			if (titleText==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			
			alarmSound = new clock_ticks_02.wav();
			
			titleText.getRawTextField().defaultTextFormat.size=22;
			
			listLeftPane=new SpriteVerticalList();
			if (listLeftPane==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			listRightPane=new SpriteVerticalList();
			if (listRightPane==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			clockSprite=new FadableSprite();
			if (clockSprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			button=new AnswerButton();
			if (button==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			buttonSprite=new ClickableFadableSprite();				
			if (buttonSprite==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			addWidget(listPanes);			 
			
			///titulo
			separator=new HorizontalLineSeparatorSprite(0,/*width-60-65*/445);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			listPanes.addItem(titleText);						
			listPanes.addItem(separator,10);		
			listPanes.x=40;
			listPanes.y=40;			
			
			///painel esquerdo			
			listPanes.addItem(listLeftPane);	
			listLeftPane.y=100-listPanes.y;
			
			///texto pergunta
			listLeftPane.addItem(questionText);
			
			///lista de opções			
			listLeftPane.addItem(listOptions,17);
			
			///-------------------------
			
			///1º opção
			listOptions.addItem(sprite1);
			
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_BOX_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			listOptions.addItem(separator,5);
			
			///2º opção			
			listOptions.addItem(sprite2,7);
			///
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_BOX_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			listOptions.addItem(separator,5);
			
			///3º opção			
			listOptions.addItem(sprite3,7);
			
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_BOX_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);		
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			
			listOptions.addItem(separator,5);
			
			///4º opção			
			listOptions.addItem(sprite4,7);		
			
			separator=new HorizontalLineSeparatorSprite(0,FuturaConstants.ANSWER_BOX_WIDTH+GUIConstants.DEFAULTTEXTHORIZONTALSPACING+check1.width);
			if (separator==null)
				FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"FuturaQuestionWindow::initWindow");
			separator.visible=false;
			
			listOptions.addItem(separator,5);			
			
			
			///-------------------------
			
			
			///painel direito
			listPanes.addItem(listRightPane);	
			listRightPane.x=listLeftPane.x+listLeftPane.width-35;
			listRightPane.y=listLeftPane.y;
			
			///relogio
			clock.x+=clock.width/2;
			clock.scaleX=0.8;
			clock.scaleY=0.8;
			
			clock.y+=clock.height/2;	
			//clock.x+=20;
			clockSprite.addChild(clock);
			listRightPane.addItem(clockSprite);

			
			///botao 'responder'
			button.scaleX=0.8;
			button.scaleY=0.8;
			buttonSprite.addChild(button);			
			button.y+=button.height/2;
			button.x+=button.width/2;				
			buttonSprite.y=0;
			listRightPane.addItem(buttonSprite,10);				
			listRightPane.x=listPanes.width-listRightPane.width;
			
			///callbacks			
			check1.setToggleCallback(this.check1Clicked);			
			check2.setToggleCallback(this.check2Clicked);			
			check3.setToggleCallback(this.check3Clicked);			
			check4.setToggleCallback(this.check4Clicked);
			buttonSprite.setOnMouseClickCallback(buttonClick);
			
			///inicializa a velocidade do timer
			setGameSpeed(FuturaConstants.TIMENORMAL);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getClockTimeElapsed():int
		{
			if ( speed ==  FuturaConstants.TIMETURBO )
				return clock.currentFrame / 2;
			else
				return clock.currentFrame;
		}		
		///--------------------------------------------------------------------------------------------------------------------
		public function setServerTime(timeElapsed:String):void
		{
			
			var serverTime : int = parseInt( timeElapsed );
			var localTime : int = getClockTimeElapsed();
			
			if (Math.abs( localTime - serverTime) > FuturaConstants.TIMER_MAX_TOLERANCE)
				setClockTimeElapsed(serverTime);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function setClockTimeElapsed(timeElapsed:int):void
		{
			if ( speed ==  FuturaConstants.TIMETURBO )
				clock.gotoAndStop(2 * timeElapsed);
			else
				clock.gotoAndStop(timeElapsed);
		}		
		///--------------------------------------------------------------------------------------------------------------------
		override public function setDimOpacity():void
		{
			super.setDimOpacity();
			clockSprite.startHideAnimation(null, clockSprite.alpha,0.15);
			buttonSprite.startHideAnimation(null, buttonSprite.alpha,0.15);
		}	
		///--------------------------------------------------------------------------------------------------------------------
		override public function setFullOpacity():void
		{
			super.setFullOpacity();
			clockSprite.startShowAnimation(null, clockSprite.alpha,1.0);
			if (answering)
				buttonSprite.startShowAnimation(null, buttonSprite.alpha,1.0);
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function startShowAnimation(f:Function=null, alphaInitial:Number=0, targetAlpha:Number=1.0):void
		{
			super.startShowAnimation(f,alphaInitial,targetAlpha);
			clockSprite.startShowAnimation(null, clockSprite.alpha,1.0);
			if (answering)
				buttonSprite.startShowAnimation(null, buttonSprite.alpha,1.0);
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function setVisible():void
		{
			super.setVisible();
			clockSprite.setVisible();
			if (answering)
				buttonSprite.setVisible();
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}