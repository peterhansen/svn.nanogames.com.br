package FuturaGUI
{
	import flash.display.*;
	import flash.utils.*;
	import flash.text.*;
	/**
	 * FuturaSpecialTurnWindow
	 * Janela para quando acontece uma jogada especial
	 * @author Daniel Monteiro
	 */
	public class FuturaSpecialTurnWindow extends FuturaMessageWindow
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * imagem para carta de penalidade
		 */
		private var sad:Penalty;
		/**
		 * imagem para carta de bonus
		 */
		private var gift:Bonus;
		private var happyTexts:Array;
		private var sadTexts:Array;
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * construtor
		 * apenas define o callback da classe mãe
		 */
		public function FuturaSpecialTurnWindow()
		{
			super();
			this.setButtonCallback(myOk);
			happyTexts = new Array();
			sadTexts = new Array();
			
			/*1 - */
			happyTexts.push(" Você chegou no seu trabalho hoje e descobriu que tinha sido promovido. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*2 - */
			happyTexts.push(" É dia de eleição e o tempo está ótimo para ir à praia. Você vai votar primeiro, para ficar tranquilo na praia depois. Você é um cidadão consciente e ainda soube organizar o seu dia. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*3 - */
			happyTexts.push(" Um candidato a deputado prometeu lhe dar um emprego em troca do seu voto. Você foi ético, não aceitou e ainda desistiu de dar seu voto para ele. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*4 - */
			happyTexts.push(" O prefeito da sua cidade se envolveu em corrupção. Você convence seus amigos a fazerem um abaixo-assinado pedindo a apuração do caso. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*5 - */
			happyTexts.push(" A empregada da sua casa tem direitos trabalhistas. Você assina a carteira de trabalho dela e cumpre a lei, garantindo os benefícios. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*6 - */
			happyTexts.push(" O seu amigo é deputado e lhe ofereceu um emprego fantasma no gabinete dele. Você foi ético, recusou e continuou no seu emprego. Muito bem! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*7 - */
			happyTexts.push(" Você convence seus amigos a assinarem um documento exigindo que a Prefeitura invista em melhorias no seu bairro. A participação da sociedade é fundamental! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*8 - */
			happyTexts.push(" Você mandou reformar a sua casa e o serviço não ficou bom. Você tinha um contrato assinado com o pintor e exigiu que o trabalho fosse refeito. Você conhece os seus direitos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*9 - */
			happyTexts.push(" Sua TV LCD pifou um dia depois do término da garantia. Você exigiu a troca do aparelho pelo fabricante, pois é um caso de vício oculto. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*10 - */
			happyTexts.push(" Você conhece o Código de Defesa do Consumidor e todas as leis que garantem os seus direitos e exige que eles sejam respeitados. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*11 - */
			happyTexts.push(" Você não vai conseguir entregar um produto no prazo prometido. Você procura o comprador, explica a situação e negocia um novo prazo. Deixar o cliente sem satisfação não é legal! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*12 - */
			happyTexts.push(" Você contratou um pacote de TV por assinatura. Ele não é instalado, mas é cobrado. Você não paga e procura a Defesa do Consumidor. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*13 - */
			happyTexts.push(" O seu voo foi cancelado e você tem um compromisso importante. Você exige que a companhia o acomode em outro voo, mesmo que seja de outra empresa. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*14 - */
			happyTexts.push(" Você estava tendo problemas respiratórios por causa da baixa umidade do ar e o dia amanheceu com muita chuva. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*15 - */
			happyTexts.push(" Você comprou uma passagem e não vai poder viajar. Você comunica a desistência à empresa três horas antes da partida para receber o seu dinheiro de volta. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*16 - */
			happyTexts.push(" Você tirou o seu melhor amigo no amigo oculto de fim de ano. Capriche no presente! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*17 - */
			happyTexts.push(" A chuva alagou a sua casa e você precisou comprar algumas coisas novas. Felizmente, você tinha reservado dinheiro para o caso de emergências. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*18 - */
			happyTexts.push(" Você recusou uma oferta da farmácia para comprar, com desconto, uma caixa de remédios danificada. Medicamentos conservados em más condições podem ser prejudiciais à sua saúde. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*19 - */
			happyTexts.push(" Você recusou a sugestão do balconista da farmácia para trocar o medicamento que o médico lhe receitou por outro mais barato. Balconista não é médico e não pode prescrever medicamentos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*20 - */
			happyTexts.push(" Você estava viajando sentado quando um idoso entrou no ônibus. Como não havia lugares vagos, você cedeu o seu. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*21 - */
			happyTexts.push(" A sua avó vive sozinha e você a visita toda semana. Quando não pode ir, telefona. Muito bem. É preciso cuidar dos idosos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*22 - */
			happyTexts.push(" Uma ventania derrubou um galho de árvore e ele caiu a dez centímetros do seu carro. Que susto, hein! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*23 - */
			happyTexts.push(" Você conhece crianças que estão sendo maltratadas e denuncia o caso, porque acha que a responsabilidade não é apenas dos pais, mas de toda a sociedade. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*24 - */
			happyTexts.push(" Você foi fazer compras e percebeu que tinha perdido a carteira. Voltou pelo mesmo caminho e a encontrou logo, intacta! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*25 - */
			happyTexts.push(" Você foi tomar um chope com os amigos e deixou o carro em casa para não dirigir depois de beber. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*26 - */
			happyTexts.push(" Você perdeu seus documentos e alguém encontrou e ligou para devolver. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*27 - */
			happyTexts.push(" Você foi à praia e, quando chegou lá, ela tinha acabado de ser limpa por um grupo de ambientalistas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*28 - */
			happyTexts.push(" Você é cabo eleitoral e no dia da eleição se recusa a fazer boca de urna. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*29 - */
			happyTexts.push(" O flanelinha estava ameaçando você e a polícia chegou no momento exato. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*30 - */
			happyTexts.push(" Um candidato a vereador organizou um comício no seu bairro e levou um artista. Você não foi ao “show” e o denunciou ao TRE, porque showmício é proibido. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*31 - */
			happyTexts.push(" Você chegou atrasado no trabalho e já estava preparado para a bronca do chefe. Mas ele também se atrasou e chegou depois de você. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*32 - */
			happyTexts.push(" Você deixou de comprar uma roupa porque o comerciante não quis manter o desconto de 10% para pagamento à vista quando viu que a compra seria feita com cartão de crédito. Você ganhou pontos e avançou uma peça no tabuleiro."); 
			
			/*33 - */
			happyTexts.push(" A lista de material escolar do seu filho inclui itens como produtos de higiene e limpeza. Você não compra e questiona a escola, porque isso é proibido. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*34 - */
			happyTexts.push(" A sua operadora de telefonia fixa não tem prestado um bom serviço. Você usa seu direito à portabilidade e muda de operadora. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*35 - */
			happyTexts.push(" Você foi comprar ingressos para um show e ganhou dois ingressos numa promoção-relâmpago. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*36 - */
			happyTexts.push(" Você perdeu seus documentos e fez o Boletim de Ocorrência imediatamente. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*37 - */
			happyTexts.push(" Você foi ao banco e não tinha fila! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*38 - */
			happyTexts.push(" Sua mãe viajou e pediu pra você molhar o jardim. Não foi preciso, pois choveu o tempo todo. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*39 - */
			happyTexts.push(" Você cuida bem das cédulas e moedas. Elas são patrimônio público e não devem ser danificadas! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*40 - */
			happyTexts.push(" Você não sabe se o seu patrão está depositando o seu Fundo de Garantia. Você procura a Caixa Econômica Federal e pede um extrato da sua conta. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*41 - */
			happyTexts.push(" Você estava de olho numa roupa e não comprou porque achou o preço muito alto. Hoje você passou pela loja e ela estava em promoção, pela metade do preço. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*42 - */
			happyTexts.push(" Você está irritado por problemas pessoais, mas se controla para não ser grosseiro com o flanelinha que não o ajudou a estacionar. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*43 - */
			happyTexts.push(" Seu amigo jogou uma latinha na rua e você pediu para que ele a colocasse no lixo. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*44 - */
			happyTexts.push(" O carro que está à sua frente tem o símbolo de surdez no vidro traseiro. Você usa os faróis para se comunicar com o motorista. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*45 - */
			happyTexts.push(" Você foi procurar seu título de eleitor e encontrou junto com ele o relógio que você achava que tinha perdido. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*46 - */
			happyTexts.push(" Você jamais abre uma carta que não é para você, pois correspondência é inviolável. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*47 - */
			happyTexts.push(" Você aproveitou o fim de semana para ler uma revista e na segunda-feira seu professor deu um trabalho exatamente sobre um assunto que você tinha lido. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*48 - */
			happyTexts.push(" Você esbarrou num carro que estava estacionado e o amassou. Você deixou um bilhete no para-brisa com seus contatos para resolver a situação depois. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*49 - */
			happyTexts.push(" Você sempre guarda os documentos do seu carro junto com a chave, para não correr o risco de esquecê-los em casa. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*50 - */
			happyTexts.push(" Sua empregada vai sair de férias e você pagou a ela o terço a mais de salário e metade do 13º. São direitos garantidos por lei! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*51 - */
			happyTexts.push(" Você alugou uma casa nova e comprometeu no máximo 30% de sua renda para não correr riscos e fazer o salário durar até o fim do mês. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*52 - */
			happyTexts.push(" Ao fazer compras, você prefere escolher os produtos e embalagens que não agridem o meio ambiente. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*53 - */
			happyTexts.push(" Você não tinha dinheiro para ver o show que queria. No dia do show, sua avó apareceu na sua casa e lhe deu R$ 200,00 de presente. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*54 - */
			happyTexts.push(" Você é um cidadão consciente e acompanha o trabalho do deputado que ajudou a eleger! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*55 - */
			happyTexts.push(" Você acha um absurdo qualquer tipo de discriminação e protesta ao ver alguém sendo discriminado! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*56 - */
			happyTexts.push(" Você mantém em lugar visível, em casa e no trabalho, todos os números de telefones de emergência. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*57 - */
			happyTexts.push(" Você conhece bem as manifestações do folclore brasileiro e até já participou de festejos populares. É importante conhecer a nossa cultura. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*58 - */
			happyTexts.push(" Seu time perdeu e, mesmo irritado, você não aceitou a provocação dos torcedores do time rival. É melhor levar tudo na brincadeira, né? Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*59 - */
			happyTexts.push(" Você adora pescar e ganhou num sorteio uma viagem para o Pantanal. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*60 - */
			happyTexts.push(" Você estava em casa no maior tédio e seu amigo ligou e o convidou para ir a uma festa com ele. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*61 - */
			happyTexts.push(" Você comprou uma rifa numa festa beneficente e ganhou duas passagens para a Europa. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*62 - */
			happyTexts.push(" Você achava bobagem a proibição de símbolos como a suástica, mas depois que aprendeu sobre o nazismo entendeu os motivos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*63 - */
			happyTexts.push(" Você já viu vários filmes do Cinema Novo e entendeu por que esse movimento foi tão importante para o Brasil. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*64 - */
			happyTexts.push(" Você conhecia pouco a história do Brasil e passou a ler mais porque entendeu que é importante você conhecer as suas origens. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*65 - */
			happyTexts.push(" Você assistiu ao filme O Auto da Compadecida, adorou e procurou outras obras de Ariano Suassuna para ler. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*66 - */
			happyTexts.push(" Você estava de olho naquele belo emprego e conseguiu a vaga disputando com um monte de candidatos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*67 - */
			happyTexts.push(" Você faz doações de sangue com regularidade. É importante ser solidário. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*68 - */
			happyTexts.push(" Você conversou com seus amigos para que eles não chamassem um colega por apelidos depreciativos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*69 - */
			happyTexts.push(" Você chegou em casa morrendo de fome e sua mãe tinha feito aquela comida que você adora. Que delícia! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*70 - */
			happyTexts.push(" Você foi de carro ao centro da cidade e conseguiu uma vaga para estacionar o carro bem em frente ao prédio em que você precisava ir. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*71 - */
			happyTexts.push(" Você saiu de casa atrasado para ir ao teatro, mas chegou a tempo, porque o trânsito estava bem mais tranquilo do que de costume. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*72 - */
			happyTexts.push(" Você estava disputando um emprego com outro candidato. Na última hora ele recebeu outra proposta e decidiu aceitar. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*73 - */
			happyTexts.push(" Você participou de uma promoção de seu cartão de crédito e vai ganhar dez mil reais por mês durante um ano. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*74 - */
			happyTexts.push(" Você sempre quis passar uma temporada no exterior. Agora sua empresa lhe propôs uma transferência para o escritório de Londres. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*75 - */
			happyTexts.push(" Você sempre fez trabalhos sociais e por isso conseguiu aquele emprego que queria. A solidariedade estava entre os fatores observados pela empresa. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*76 - */
			happyTexts.push(" Depois de ver o filme Carandiru, você procurou se informar sobre o ocorrido e refletir sobre o sistema carcerário brasileiro. Acompanhar o que acontece no país e no mundo é fundamental. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*77 - */
			happyTexts.push(" Você acha um absurdo que processos contra corrupção política acabem em pizza. Você sempre faz campanha a favor de investigações rigorosas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*78 - */
			happyTexts.push(" Você sempre ajuda uma senhora idosa que mora no seu prédio e agora descobriu que ela é a mãe do seu chefe. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*79 - */
			happyTexts.push(" O Cristo Redentor é uma das sete maravilhas do mundo moderno e você foi visitá-lo nestas férias. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*80 - */
			happyTexts.push(" Você estudou bastante para um concurso e, na hora da prova, sabia todas as respostas! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*81 - */
			happyTexts.push("Você declara seu imposto de renda corretamente e dentro do prazo. Sonegar é crime! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*82 - */
			happyTexts.push(" Você estava no supermercado quando houve uma promoção-relâmpago e você ganhou um carrinho cheio de mercadorias. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*83 - */
			happyTexts.push(" Toda vez que o seu time ganha você comemora muito, mas tem o cuidado de não incomodar os vizinhos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*84 - */
			happyTexts.push(" Você sempre se envolve em projetos de trabalho voluntário. Muito bem! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*85 - */
			happyTexts.push(" A TV que você comprou veio com defeito. Você reclamou e a loja trocou o aparelho por outro e ainda deu um DVD para compensar o aborrecimento. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*86 - */
			happyTexts.push(" Alguém lhe ofereceu um equipamento eletrônico por um preço muito baixo, mas você não comprou porque deduziu que devia ser roubado. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*87 - */
			happyTexts.push(" Você não compra produtos de empresas que não têm certificados ambientais. Você colabora com a preservação da natureza. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*88 - */
			happyTexts.push(" Seu amigo quis sair do restaurante onde vocês estavam porque um casal gay sentou na mesa ao lado. Você o convenceu a ficar e a respeitar as diferenças. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*89 - */
			happyTexts.push(" Durante a Copa do Mundo na África do Sul você se interessou em conhecer a história do país. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*90 - */
			happyTexts.push(" Você está jogando e prefere perder a fazer trapaça para ganhar. Isso demonstra espírito esportivo! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*91 - */
			happyTexts.push(" Você comprou um pacote de biscoitos e dentro tinha um vale-brinde de R$ 1.000,00. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*92 - */
			happyTexts.push(" Na eleição para presidente, você escolheu o seu candidato porque o achava mais capacitado e não porque ele estava à frente nas pesquisas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*93 - */
			happyTexts.push(" A candidata em quem você votou na última eleição foi cassada por corrupção. Se ela se candidatar de novo quando puder, não terá mais seu voto! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*94 - */
			happyTexts.push(" Você procurou conhecer a história de Ghandi porque queria saber como é possível mudar a história de um país de forma pacífica. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*95 - */
			happyTexts.push(" Você foi pela primeira vez a Belém e lá conheceu uma pessoa que lhe mostrou toda a cidade e o levou a vários lugares que nenhum turista conhece. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*96 - */
			happyTexts.push(" Você foi comer uma ostra na praia e tinha uma pérola dentro dela. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*97 - */
			happyTexts.push(" Você estava cavando um buraco para plantar uma árvore e achou um pequeno baú com várias moedas antigas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*98 - */
			happyTexts.push(" Você parou para ajudar uma família cujo carro quebrou na estrada. Em agradecimento, o homem lhe ofereceu um bom emprego. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*99 - */
			happyTexts.push(" Você descobriu a senha da sua irmã na internet. Mas não acessou nada pessoal e deu dicas de como fazer uma senha mais segura. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*100 - */
			happyTexts.push(" Você não fornece seus dados pessoais a desconhecidos, porque sabe que isso é perigoso. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*101 - */
			happyTexts.push(" Você nunca tentou quebrar o sistema de segurança de um software mesmo tendo conhecimento suficiente para isso. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*102 - */
			happyTexts.push(" Seu amigo convidou você para passar o réveillon em Paris. Você fez as contas e descobriu que o dinheiro que economizou dá para a viagem e ainda sobra. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*103 - */
			happyTexts.push(" Você comprou um produto fabricado na Amazônia e ganhou uma viagem para assistir à Festa do Boi em Parintins. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*104 - */
			happyTexts.push(" Sempre que você compra pela internet, procura informações sobre a empresa e toma todos os cuidados para garantir sua segurança. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*105 - */
			happyTexts.push(" Você se inscreveu num curso de inglês e ganhou uma bolsa para ficar seis meses estudando em Nova Iorque. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*106 - */
			happyTexts.push(" Você fala com seus amigos pelas redes sociais, mas também faz questão de encontrá-los pessoalmente. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*107 - */
			happyTexts.push(" Você não acredita em tudo que vê na internet. Nem tudo o que circula em sites ou e-mails é verdadeiro. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*108 - */
			happyTexts.push(" Você não abre mensagens suspeitas recebidas por e-mail. Podem ser vírus! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*109 - */
			happyTexts.push(" Você sabe identificar uma tentativa de golpe pela internet e presta atenção nos endereços das mensagens que recebe. Cuidado com remetentes suspeitos! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*110 - */
			happyTexts.push(" Você foi comprar os livros que precisava e na livraria conheceu uma pessoa que tinha todos eles e os ofereceu a você. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*111 - */
			happyTexts.push(" Você não repassa mensagens que maltratam e difamam outras pessoas e alerta ao remetente que esse comportamento pode trazer más consequências. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*112 - */
			happyTexts.push(" Você conhece e pratica as normas da “netiqueta”, a boa educação na internet. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*113 - */
			happyTexts.push(" Você tomou um refrigerante na rua e, quando foi colocar a lata no lixo, achou uma nota de cem reais. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*114 - */
			happyTexts.push(" Você configurou seu perfil na rede social para proteger sua privacidade. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*115 - */
			happyTexts.push(" Você não conta suas senhas para ninguém. Vale se esforçar pela segurança! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*116 - */
			happyTexts.push(" Você não fala nas redes sociais sobre situações da sua empresa e preza pela discrição com os assuntos de trabalho. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*117 - */
			happyTexts.push(" Você fica atento ao volume dos fones porque sabe que música alta pode provocar problemas auditivos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*118 - */
			happyTexts.push(" Você defendeu um homem negro que estava sendo vítima de racismo e foi convidado a visitar o país do qual ele era embaixador. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*119 - */
			happyTexts.push(" Você não acessa seus dados bancários em computadores públicos, pois sabe que isso é arriscado. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*120 - */
			happyTexts.push(" Você recebe uma mensagem com um título que desperta sua curiosidade, mas, como o remetente é suspeito, apaga a mensagem sem abrir. Você ganhou pontos e avançou uma peça no tabuleiro..");
			
			/*121 - */
			happyTexts.push(" Você conheceu uma pessoa pela internet, escolheu um lugar público para o encontro e levou um amigo. Sua segurança é fundamental. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*122 - */
			happyTexts.push(" Você precisou usar seu pen drive num computador público. Quando chegou em casa, a primeira coisa que fez foi passá-lo pelo antivírus. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*123 - */
			happyTexts.push(" Você troca suas senhas regularmente e observa as dicas de segurança ao criar uma nova. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*124 - */
			happyTexts.push(" Você quer manter a forma e se esforça em ter uma alimentação balanceada, praticar exercícios, e foge de dietas milagrosas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*125 - */
			happyTexts.push(" Você está louco para trocar de emprego e conseguiu um outro bem melhor ao conhecer um convidado da festa de aniversário do seu sogro. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*126 - */
			happyTexts.push(" Você terminou o seu curso em primeiro lugar. Como prêmio, ganhou uma bolsa para fazer pós-graduação nos Estados Unidos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*127 - */
			happyTexts.push(" Você foi arrumar o seu quarto e encontrou no meio da bagunça 500 dólares que havia perdido. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*128 - */
			happyTexts.push(" Você acha que políticos que facilitam negócios ilegais em troca de dinheiro cometem crime e precisam ser julgados. Sem contar que eles nunca terão seu voto! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*129 - */
			happyTexts.push(" Você não acha normal um político desrespeitar a lei eleitoral. Você é ético e exige que ele também seja! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*130 - */
			happyTexts.push(" Você pratica alguma atividade física pelo menos três vezes por semana. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*131 - */
			happyTexts.push(" Você acha errado um jogador de futebol ser desleal em campo, mesmo que ele seja do seu time. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*132 - */
			happyTexts.push(" Você torce para o seu time vencer o jogo, mas quer que ele respeite as regras. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*133 - */
			happyTexts.push(" Você recebeu uma herança milionária de um vizinho com bom coração. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*134 - */
			happyTexts.push(" Você está aprendendo a cozinhar, assim não depende de ninguém para comer. Parabéns. É bom ser independente! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*135 - */
			happyTexts.push(" Todos os seus eletrodomésticos foram danificados por um pique de energia durante uma tempestade. A concessionária pagou seu prejuízo assim que você reclamou. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*136 - */
			happyTexts.push(" Você sempre experimenta uma comida antes de decidir que não gosta mesmo dela. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*137 - */
			happyTexts.push(" Você foi a Manaus e aproveitou para experimentar vários pratos típicos. Culinária também e cultura! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*138 - */
			happyTexts.push(" Você adora aquela água quente no inverno, mas sempre fecha o chuveiro enquanto se ensaboa, para economizar água. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*139 - */
			happyTexts.push(" Uma onda de calor está provocando vários incêndios na sua cidade. Hoje chegou uma frente fria e começou a chover. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*140 - */
			happyTexts.push(" Você não sabia o que era mangá, mas leu alguns para conhecer. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*141 - */
			happyTexts.push(" O novo prefeito da sua cidade começou a reflorestar todas as áreas que tinham sido destruídas. Todos os jogadores avançam uma peça. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*142 - */
			happyTexts.push(" O governo do seu Estado baixou todos os impostos e você vai economizar um bom dinheiro a partir de agora. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*143 - */
			happyTexts.push(" Você sempre desliga da tomada os aparelhos eletrônicos que não estão sendo usados. Mesmo no stand-by eles gastam energia! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*144 - */
			happyTexts.push(" Você sempre colabora nas campanhas de doação de agasalhos e alimentos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*145 - */
			happyTexts.push(" Você fez várias compras em São Paulo e agora descobriu que o governo paulista tem um programa que devolve parte do que foi pago em impostos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*146 - */
			happyTexts.push(" Você descobriu que aquela pessoa especial também está jogando o CDF! Não perca tempo. Na próxima partida, convide ele ou ela para jogar. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*147 - */
			happyTexts.push(" Você gosta de viajar e sempre que possível aproveita para conhecer um pouco mais do Brasil. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*148 - */
			happyTexts.push(" Você conhece todas as lendas do nosso folclore. É legal se interessar pela cultura brasileira! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*149 - */
			happyTexts.push(" Você conheceu um gatinho ou gatinha dos Estados Unidos e arrasou no inglês! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*150 - */
			happyTexts.push(" Você gosta de acampar em parques e sempre toma cuidados: não faz fogueira onde não deve e não deixa lixo para trás. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*151 - */
			happyTexts.push(" Você aprendeu uma receita nova e fez sucesso com seus amigos. Agora eles sempre pedem para almoçar na sua casa! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*152 - */
			happyTexts.push(" Você quer fazer uma grande viagem de férias e tomou o primeiro passo: começou a juntar o dinheiro necessário. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*153 - */
			happyTexts.push(" Você não é consumista e não compra por impulso. E ainda ensinou a sua mãe a levar uma lista ao mercado com os itens para comprar. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*154 - */
			happyTexts.push(" Você ajuda nas tarefas domésticas e ainda faz pequenos reparos quando é preciso. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*155 - */
			happyTexts.push(" Você ajudou a fazer o orçamento da sua casa e é o responsável pelo cumprimento dele. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*156 - */
			happyTexts.push(" Você abriu uma poupança e, quando recebe seu salário, aplica parte dele antes de se comprometer com outras despesas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*157 - */
			happyTexts.push(" Você estabeleceu como meta comprar um carro zero em dois anos. Já começou a economizar e fazer trabalhos extras para ganhar um pouco mais. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*158 - */
			happyTexts.push(" Você quer crescer profissionalmente e, embora não tenha muito tempo, está fazendo mais um curso de capacitação. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*159 - */
			happyTexts.push(" Você não entende muito de aplicação financeira. Então conversou com o gerente da sua conta no banco e descobriu o melhor investimento para você. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*160 - */
			happyTexts.push(" Você trocou o seu cartão de crédito por um que não tem anuidade e só usa quando é necessário. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*161 - */
			happyTexts.push(" Você conversou com o síndico do seu prédio e pediu que ele orientasse os zeladores a não lavar a calçada com água corrente. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*162 - */
			happyTexts.push(" Seu time de futebol venceu o clássico mais difícil do ano e assumiu a liderança do campeonato! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*163 - */
			happyTexts.push(" Você só compra roupas e sapatos quando precisa e sempre espera as liquidações. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*164 - */
			happyTexts.push(" Na sua casa só se usa a máquina de lavar roupas quando tem roupa suficiente para enchê-la. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*165 - */
			happyTexts.push(" Você tem amigos de várias religiões e todos se respeitam, procurando conhecer um pouco de cada uma. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*166 - */
			happyTexts.push(" Você mantém seu carro sempre bem regulado para diminuir a poluição que ele emite. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*167 - */
			happyTexts.push(" Você espantou a preguiça e decidiu finalmente se matricular em uma academia de ginástica. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*168 - */
			happyTexts.push(" Você ganhou uma festa surpresa de aniversário. Que gostoso rever os amigos! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*169 - */
			happyTexts.push(" Você sempre exige nota fiscal nas suas compras e não adquire nada de procedência duvidosa. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*170 - */
			happyTexts.push(" Suas merecidas férias chegaram! Aproveite para descansar e curtir a vida. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*171 - */
			happyTexts.push(" Você passou no vestibular e ganhou aquele abraço gostoso dos pais para comemorar. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*172 - */
			happyTexts.push(" Você não toma remédios sem receita médica, porque sabe que é perigoso se automedicar. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*173 - */
			happyTexts.push(" Sua irmã vai viajar para a Europa e decidiu levar você junto com ela. Que sorte, hein! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*174 - */
			happyTexts.push(" Você pagou suas contas em dia e escapou de pagar juros por atrasos. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*175 - */
			happyTexts.push(" Você gabaritou a última prova e ainda ajudou seus amigos a estudarem para fazer bonito na matéria também. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*176 - */
			happyTexts.push(" Você respeita as leis de trânsito para não colocar em risco a sua vida e a de pessoas inocentes. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*177 - */
			happyTexts.push(" Você quer muito ir a um show, mas não vai comprar ingressos de cambistas. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*178 - */
			happyTexts.push(" Seu ídolo finalmente vai fazer um show na sua cidade. E o melhor: de graça! Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*179 - */
			happyTexts.push(" Você jamais estaciona em cima da calçada. Você é um cidadão consciente e sabe que a calçada é para pedestres. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*180 - */
			happyTexts.push(" Você sai para passear com o seu cachorro e, quando ele faz cocô, apanha a sujeira com uma sacolinha e coloca no lixo. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			/*181 - */
			happyTexts.push(" Seu cachorro é de uma raça considerada violenta. Você sabe que ele é um fofo, mas mesmo assim não se descuida: ele só sai na rua de focinheira. Você ganhou pontos e avançou uma peça no tabuleiro.");
			
			
			/*1 - */
			sadTexts.push("Depois da eleição, você descobriu que o seu candidato está respondendo a quatro processos por corrupção. Você votou sem pesquisar o histórico dele. Você perdeu sua vez e não joga esta rodada.");
			
			/*2 - */
			sadTexts.push("Você estava quase zerando o jogo mais difícil da sua vida quando alguém esbarrou na tomada e lá se foi a sua partida. Você perdeu sua vez e não joga esta rodada.");
			
			/*3 - */
			sadTexts.push("Um candidato a deputado prometeu lhe dar um emprego se você votar nele. Você aceitou. Isso é venda de voto! Você perdeu sua vez e não joga esta rodada.");
			
			/*4 - */
			sadTexts.push("O prefeito da sua cidade é acusado de corrupção, mas, como ele parece ser bom administrador, você deixa isso para lá. Ser honesto é obrigação dele! Você perdeu sua vez e não joga esta rodada.");
			
			/*5 - */
			sadTexts.push("A empregada da sua casa trabalha diariamente, sem carteira assinada e só ganha meio salário. Isso é trabalho escravo! Você perdeu sua vez e não joga esta rodada.");
			
			/*6 - */
			sadTexts.push("O seu tio é deputado e arranjou um emprego bem tranquilo para você no gabinete dele. Isso é nepotismo! Você perdeu sua vez e não joga esta rodada.");
			
			/*7 - */
			sadTexts.push("Você foi acampar com seus amigos e, quando chegou lá, choveu o tempo todo! Você perdeu sua vez e não joga esta rodada.");
			
			/*8 - */
			sadTexts.push("Você mandou consertar o telhado de sua casa e o trabalho não ficou bom. Você não fez um contrato para o serviço e agora ficou no prejuízo. Você perdeu sua vez e não joga esta rodada.");
			
			/*9 - */
			sadTexts.push("Sua TV nova pifou um dia depois do término da garantia. Você pagou para consertar, mas deveria ter informado o fabricante, pois pode ser um caso de vício oculto. Você perdeu sua vez e não joga esta rodada. Você perdeu sua vez e não joga esta rodada.");
			
			/*10 - */
			sadTexts.push("Você não conhece o Código de Defesa do Consumidor. Deveria conhecer, pois são os seus direitos que estão nele. Você perdeu sua vez e não joga esta rodada.");
			
			/*11 - */
			sadTexts.push("Você foi viajar no fim de semana e o seu carro quebrou na estrada. Você perdeu sua vez e não joga esta rodada.");
			
			/*12 - */
			sadTexts.push("Você contratou um pacote de TV por assinatura e internet, mas fica sem TV porque falta trocar um aparelho. Não permita que desrespeitem os seus direitos. Você perdeu sua vez e não joga esta rodada.");
			
			/*13 - */
			sadTexts.push("O seu voo foi cancelado e você perdeu um compromisso importante. Você perdeu sua vez e não joga esta rodada.");
			
			/*14 - */
			sadTexts.push("Você vai fazer uma viagem de cinco horas e não consegue ficar sem fumar. Você então se senta no fundo do ônibus e acende um cigarro. É proibido! Você perdeu sua vez e não joga esta rodada.");
			
			/*15 - */
			sadTexts.push("Você comprou uma passagem e não pôde viajar. No dia seguinte, procura a empresa para pegar seu dinheiro de volta. Devia ter avisado que não iria três horas antes da partida. Você perdeu sua vez e não joga esta rodada.");
			
			/*16 - */
			sadTexts.push("Faltou luz na sua casa bem na hora em que você ia assistir à final do campeonato! Torça para ela voltar logo ou procure a casa de um amigo para não perder o jogão. Você perdeu sua vez e não joga esta rodada.");
			
			/*17 - */
			sadTexts.push("Você e seu amigos fizeram a maior farra no ônibus durante uma viagem. Vocês perturbaram a tranquilidade dos outros passageiros. Você perdeu sua vez e não joga esta rodada.");
			
			/*18 - */
			sadTexts.push("Você aceitou comprar uma caixa de remédios suja e rasgada porque a farmácia deu desconto. Medicamentos conservados em más condições podem ser prejudiciais. Você perdeu sua vez e não joga esta rodada.");
			
			/*19 - */
			sadTexts.push("Você trocou o medicamento que o médico lhe receitou por um mais barato que o balconista da farmácia lhe indicou. Balconista não é médico. Você perdeu sua vez e não joga esta rodada.");
			
			/*20 - */
			sadTexts.push("Você estava viajando sentado quando um idoso entrou no ônibus, que já estava cheio. Você fingiu que não viu. Você perdeu sua vez e não joga esta rodada.");
			
			/*21 - */
			sadTexts.push("A sua avó vive sozinha e tem mais de dois meses que você não vai visitá-la. Que falta de consideração! Você perdeu sua vez e não joga esta rodada.");
			
			/*22 - */
			sadTexts.push("Você e seus amigos conhecem um homem que explora crianças. Vocês não o denunciam porque acham que o problema não é de vocês. Isso é conivência com o crime. Você perdeu sua vez e não joga esta rodada.");
			
			/* 23 - */
			sadTexts.push("Você conhece crianças que estão sendo exploradas mas não faz nada, porque acredita que a família é a única responsável. A responsabilidade é de toda a sociedade. Você perdeu sua vez e não joga esta rodada.");
			
			/*24 - */
			sadTexts.push("Você acha que um marido que espanca a mulher está no direito dele, porque em briga de marido e mulher ninguém mete a colher. Denuncie! Você perdeu sua vez e não joga esta rodada.");
			
			/*25 - */
			sadTexts.push("Você dirigiu depois de beber e deu dinheiro ao policial para ele o liberar. Isso é crime. Não estimule a corrupção. Você perdeu sua vez e não joga esta rodada.");
			
			/*26 - */
			sadTexts.push("Você não gosta quando os empregados domésticos usam o elevador social do seu prédio. Isso é discriminação! Você perdeu sua vez e não joga esta rodada.");
			
			/*27 - */
			sadTexts.push("Sua colega de trabalho foi promovida para um cargo de chefia. Você não gostou, porque agora você vai ser chefiado por uma mulher. Isso é discriminação e machismo! Você perdeu sua vez e não joga esta rodada.");
			
			/*28 - */
			sadTexts.push("Você é cabo eleitoral e no dia da eleição vai fazer campanha no local de votação no seu bairro. Isso é boca de urna. Você perdeu sua vez e não joga esta rodada.");
			
			/*29 - */
			sadTexts.push("Você não faz nada para melhorar a qualidade de vida no seu país, porque isso é obrigação dos políticos. Isso é problema de toda a sociedade! Você perdeu sua vez e não joga esta rodada.");
			
			/*30 - */
			sadTexts.push("Um candidato a vereador organizou um comício no seu bairro e levou um artista. E você foi...Era de graça, né? Showmício é proibido. Você perdeu sua vez e não joga esta rodada.");
			
			/*31 - */
			sadTexts.push("Você recebeu uma fatura do cartão de crédito e o valor atingiu o limite máximo. Só aí você se lembra que perdeu o cartão e não pediu o bloqueio dele. Você perdeu sua vez e não joga esta rodada.");
			
			/*32 - */
			sadTexts.push("Você foi comprar uma roupa e o comerciante não quis manter o desconto de 10% para pagamento à vista porque você ia usar o cartão de crédito. Você comprou assim mesmo. Você perdeu sua vez e não joga esta rodada.");
			
			/*33 - */
			sadTexts.push("A lista de material escolar do seu filho inclui itens como produtos de higiene e limpeza. Não pode, mas você compra sem reclamar! Você perdeu sua vez e não joga esta rodada.");
			
			/*34 - */
			sadTexts.push("A sua operadora de telefonia fixa está agindo de forma abusiva. E você está aceitando. Portabilidade existe para casos como esse. Você perdeu sua vez e não joga esta rodada.");
			
			/*35 - */
			sadTexts.push("Seu amigo lhe mandou um presente do exterior e você não recebeu, pois ele foi extraviado no caminho. Você perdeu sua vez e não joga esta rodada.");
			
			/*36 - */
			sadTexts.push("Você perdeu seus documentos e não fez o Boletim de Ocorrência. Você perdeu sua vez e não joga esta rodada.");
			
			/*37 - */
			sadTexts.push("O caixa da padaria quis dar a você o troco de R$10,00 em moedas e você não aceitou e ainda brigou com ele. Moeda é dinheiro! Você perdeu sua vez e não joga esta rodada.");
			
			/*38 - */
			sadTexts.push("Você foi a uma festa e um convidado esbarrou em você. Sua roupa novinha ficou cheia de refrigerante! Você perdeu sua vez e não joga esta rodada.");
			
			/*39 - */
			sadTexts.push("Você escreveu um número de telefone numa nota de R$ 2,00 porque não tinha papel na hora. Não estrague patrimônio público! Você perdeu sua vez e não joga esta rodada.");
			
			/*40 - */
			sadTexts.push("Você não sabe se o seu patrão está depositando o seu Fundo de Garantia e nem se preocupou em conferir. Você perdeu sua vez e não joga esta rodada.");
			
			/*41 - */
			sadTexts.push("Você não está gostando de trabalhar e pede ao seu empregador para demiti-lo, pois assim você terá direito ao seguro-desemprego. Que vergonha! Você perdeu sua vez e não joga esta rodada.");
			
			/*42 - */
			sadTexts.push("Você critica os políticos corruptos mas dá dinheiro ao policial para se livrar de uma multa. Só existem corruptos porque existem corruptores. Você perdeu sua vez e não joga esta rodada.");
			
			/*43 - */
			sadTexts.push("Você está irritado e é grosseiro com o porteiro do seu prédio. Aprenda a se controlar e a respeitar as pessoas. Você perdeu sua vez e não joga esta rodada.");
			
			/*44 - */
			sadTexts.push("Seu amigo jogou uma latinha na rua e você não falou nada. Da próxima vez, dê um toque nele! Você perdeu sua vez e não joga esta rodada.");
			
			/*45 - */
			sadTexts.push("O carro que está à sua frente tem o símbolo de surdez no vidro traseiro. Mesmo assim, você insiste em ficar buzinando. Você perdeu sua vez e não joga esta rodada.");
			
			/*46 - */
			sadTexts.push("Você está pressionando a sua empregada para ela votar no seu candidato nas eleições para prefeito. Isso não se faz! Você perdeu sua vez e não joga esta rodada.");
			
			/*47 - */
			sadTexts.push("Você abriu uma carta que era para sua irmã. Correspondência é inviolável. Você perdeu sua vez e não joga esta rodada.");
			
			/*48 - */
			sadTexts.push("Você comeu alimentos estragados na rua e acabou com uma infecção intestinal. Que droga! Você perdeu sua vez e não joga esta rodada.");
			
			/*49 - */
			sadTexts.push("Você esbarrou num carro que estava estacionado e o amassou. Muito esperto, você se mandou antes que o dono aparecesse. Assuma a responsabilidade por seus atos! Você perdeu sua vez e não joga esta rodada.");
			
			/*50 - */
			sadTexts.push("Você saiu de carro e deixou os documentos em casa. Sabia que você pode ser multado? Você perdeu sua vez e não joga esta rodada.");
			
			/*51 - */
			sadTexts.push("Sua empregada vai sair de férias e você não quer pagar a ela o terço a mais de salário. A lei que vale para você também vale para ela! Você perdeu sua vez e não joga esta rodada.");
			
			/*52 - */
			sadTexts.push("Você alugou uma casa nova e comprometeu 50% de sua renda. Não seria melhor ter procurado mais e comprometido menos o seu salário? Você perdeu sua vez e não joga esta rodada.");
			
			/*53 - */
			sadTexts.push("Ao fazer compras, você nunca se preocupa em escolher os produtos que não agridem o meio ambiente. Você perdeu sua vez e não joga esta rodada.");
			
			/*54 - */
			sadTexts.push("Você se esqueceu em quem votou nas últimas eleições. Você perdeu sua vez e não joga esta rodada. .");
			
			/*55 - */
			sadTexts.push("Você não tem a menor ideia sobre o que anda fazendo o deputado que você ajudou a eleger. Você não é um cidadão consciente! Você perdeu sua vez e não joga esta rodada..");
			
			/*56 - */
			sadTexts.push("Você viu duas ciganas na rua e atravessou para não cruzar com elas. Isso é discriminação! Você perdeu sua vez e não joga esta rodada.");
			
			/*57 - */
			sadTexts.push("Você estacionou seu carro limpinho embaixo de uma árvore. Os passarinhos fizeram a festa e o deixaram todo sujo quando você retornou. Você perdeu sua vez e não joga esta rodada.");
			
			/*58 - */
			sadTexts.push("Começou um incêndio na casa do seu vizinho e você não chamou os bombeiros porque não sabia o número. Tenha os números de emergência sempre à mão! Você perdeu sua vez e não joga esta rodada.");
			
			/*59 - */
			sadTexts.push("Você precisou de uma ambulância e perdeu muito tempo procurando o número do telefone de emergência. Deixe-o em um lugar visível! Você perdeu sua vez e não joga esta rodada.");
			
			/*60 - */
			sadTexts.push("Você não sabe o número de emergência da Defesa Civil. Você pode precisar dele de repente! Você perdeu sua vez e não joga esta rodada.");
			
			/*61 - */
			sadTexts.push("Você não sabe o que é Folia de Reis. É importante conhecer a nossa cultura. Você perdeu sua vez e não joga esta rodada.");
			
			/*62 - */
			sadTexts.push("Você imagina que o Abaporu é um animal. Que tal se informar melhor? Você perdeu sua vez e não joga esta rodada.");
			
			/*63 - */
			sadTexts.push("Seu time perdeu e você brigou com torcedores do time rival. Que vergonha! Você perdeu sua vez e não joga esta rodada.");
			
			/*64 - */
			sadTexts.push("Você não gosta de filmes antigos e por isso não conhece os clássicos do cinema. Você não sabe o que está perdendo. Você perdeu sua vez e não joga esta rodada.");
			
			/*65 - */
			sadTexts.push("Para você, a Guerra dos Farrapos foi uma brincadeira de rasgar as roupas uns dos outros. Que desinformado! Você perdeu sua vez e não joga esta rodada.");
			
			/*66 - */
			sadTexts.push("Você não sabe nada sobre a África. Então você não conhece as raízes de seu país. Você perdeu sua vez e não joga esta rodada.");
			
			/*67 - */
			sadTexts.push("Você não sabe nada sobre o nazismo, por isso acha bobagem a proibição de símbolos como a suástica. Você perdeu sua vez e não joga esta rodada.");
			
			/*68 - */
			sadTexts.push("Você nunca viu um filme do Cinema Novo. Aproveite a pausa para conhecer melhor este movimento. Você perdeu sua vez e não joga esta rodada.");
			
			/*69 - */
			sadTexts.push("Você conhece pouco a história do Brasil. É a sua própria história! Você perdeu sua vez e não joga esta rodada."); 
			
			/*70 - */
			sadTexts.push("Você não sabe quem é Ariano Suassuna. Você não sabe o que está perdendo! Você perdeu sua vez e não joga esta rodada.");
			
			/*71 - */
			sadTexts.push("Você nunca ouviu uma música de Chiquinha Gonzaga. Chegou a hora! Você perdeu sua vez e não joga esta rodada.");
			
			/*72 - */
			sadTexts.push("Você não sabe quem foram Giuseppe e Anita Garibaldi. Que tal se informar melhor? Você perdeu sua vez e não joga esta rodada.");
			
			/*73 - */
			sadTexts.push("Você não conhece a história de Chico Mendes, embora a luta dele tenha beneficiado nosso país. Você perdeu sua vez e não joga esta rodada.");
			
			/*74 - */
			sadTexts.push("Você nunca doou nada a quem tem menos do que você. Você é muito egoísta! Você perdeu sua vez e não joga esta rodada.");
			
			/*75 - */
			sadTexts.push("Você colocou um apelido depreciativo em um colega da escola. Isso é bullying. Você perdeu sua vez e não joga esta rodada..");
			
			/*76 - */
			sadTexts.push("Você não conhece nada sobre o trabalho de Chico Xavier e só se refere a ele como o curandeiro. Ignorância gera preconceito. Você perdeu sua vez e não joga esta rodada."); 
			
			/*77 - */
			sadTexts.push("Você não conhece a história de Cuba antes de Fidel Castro. Você perdeu sua vez e não joga esta rodada..");
			
			/*78 - */
			sadTexts.push("Você acredita que Cuba é uma democracia. Você deve viver no mundo da Lua! Você perdeu sua vez e não joga esta rodada..");
			
			/*79 - */
			sadTexts.push("Você deixou cair o vaso de cristal preferido da sua mãe... Perca dez pontos e torça para não ficar de castigo! Você perdeu sua vez e não joga esta rodada.");
			
			/*80 - */
			sadTexts.push("Você não conhece nada sobre a obra de Celso Furtado. Aproveite melhor o seu tempo e aprenda. Você perdeu sua vez e não joga esta rodada..");
			
			/* 81 - */
			sadTexts.push("Você foi tomar banho de cachoeira e depois foi embora deixando todo o lixo para trás. Porcalhão! Você perdeu sua vez e não joga esta rodada. .");
			
			/*82 - */
			sadTexts.push("Você derramou café na sua roupa enquanto estava trabalhando. Tome mais cuidado da próxima vez! Você perdeu sua vez e não joga esta rodada.");
			
			/*83 - */
			sadTexts.push("Seu time não ganha um campeonato há muito tempo! Você perdeu sua vez e não joga esta rodada. .");
			
			/*84 - */
			sadTexts.push("Você comprou uma caixa de morangos e não percebeu que metade deles estava podre. Fique atento à melhor época para comprar cada fruta. Você perdeu sua vez e não joga esta rodada.");
			
			/*85 - */
			sadTexts.push("Você nunca ouviu falar no poeta Manoel de Barros. Você perdeu sua vez e não joga esta rodada. .");
			
			/*86 - */
			sadTexts.push("Você acha que a história do filme Carandiru é ficção. Acompanhar o que acontece no país e no mundo é fundamental. Você perdeu sua vez e não joga esta rodada.");
			
			/*87 - */
			sadTexts.push("Você acha que é normal os processos contra corrupção política acabarem em pizza. Você perdeu sua vez e não joga esta rodada.");
			
			/*88 - */
			sadTexts.push("Você convidou seus amigos para jantar e resolveu experimentar uma receita nova. Ficou horrível! Você perdeu sua vez e não joga esta rodada.");
			
			/*89 - */
			sadTexts.push("Você é brasileiro, mas não sabia que o Cristo Redentor é uma das sete maravilhas do mundo moderno. Você perdeu sua vez e não joga esta rodada.");
			
			/*90 - */
			sadTexts.push("Você comprou um par de sapatos para ir a uma festa. Eles eram tão desconfortáveis que seus pés ficaram com bolhas. Você perdeu sua vez e não joga esta rodada.");
			
			/*91 - */
			sadTexts.push("Você sonegou Imposto de Renda! Isso é crime, sabia? Você perdeu sua vez e não joga esta rodada.");
			
			/*92 - */
			sadTexts.push("Choveu muito no fim de semana e sua casa ficou alagada. Você perdeu sua vez e não joga esta rodada.");
			
			/*93 - */
			sadTexts.push("Faltou luz na sua casa bem na hora do jogo de futebol do seu time. Perca dez pontos e torça para ela voltar logo! Você perdeu sua vez e não joga esta rodada.");
			
			/*94 - */
			sadTexts.push("Você acha que mulher não entende nada de futebol. Você se esqueceu da Marta e é muito machista! Você perdeu sua vez e não joga esta rodada..");
			
			/*95 - */
			sadTexts.push("Você nunca se ofereceu como voluntário para nada. Ajudar faz bem! Você perdeu sua vez e não joga esta rodada.");
			
			/*96 - */
			sadTexts.push("Você reuniu um grupo de amigos em casa e aquele vizinho inconveniente resolveu aparecer sem ser convidado. Que chato, hein... Você perdeu sua vez e não joga esta rodada..");
			
			/*97 - */
			sadTexts.push("Você comprou uma joia muito barata que lhe ofereceram e nem se preocupou em saber a procedência dela. Ela pode ter sido roubada. Você perdeu sua vez e não joga esta rodada..");
			
			/*98 - */
			sadTexts.push("Você comprou um produto de uma empresa sabendo que ela não tem nenhum cuidado ambiental. Você colaborou com a destruição da natureza. Você perdeu sua vez e não joga esta rodada..");
			
			/*99 - */
			sadTexts.push("O alarme de um carro disparou em frente à sua casa bem no finalzinho do filme a que você estava assistindo e não deu para entender o final. Você perdeu sua vez e não joga esta rodada.");
			
			/*100 - */
			sadTexts.push("Você estava num restaurante e foi embora porque entrou um casal gay. Xô, preconceito! Você perdeu sua vez e não joga esta rodada.");
			
			/*101 - */
			sadTexts.push("Você perdeu a chave de casa. Quando voltou, todos tinham saído e você teve que ficar um tempão esperando alguém chegar. Você perdeu sua vez e não joga esta rodada.");
			
			/*101 - */
			sadTexts.push("Você está jogando e acha que vale tudo para ganhar. Isso é falta de espírito esportivo! Você perdeu sua vez e não joga esta rodada..");
			
			/*102 - */
			sadTexts.push("Você acha que grafite e rap são coisas de marginal. Pare para pensar e se informe melhor. Você perdeu sua vez e não joga esta rodada.");
			
			/*103 - */
			sadTexts.push("Você acha que os humoristas devem ser proibidos de fazer piadas sobre políticos. Isso é censura! Você perdeu sua vez e não joga esta rodada.");
			
			/*104 - */
			sadTexts.push("Na eleição para presidente você escolheu o candidato só porque ele estava à frente nas pesquisas. Isso não é critério para uma escolha tão importante! Você perdeu sua vez e não joga esta rodada.");
			
			/*105 - */
			sadTexts.push("A candidata em quem você votou na última eleição foi cassada por corrupção. Quando ela puder se candidatar de novo você vai votar nela! Cometer o mesmo erro duas vezes é burrice. Você perdeu sua vez e não joga esta rodada.");
			
			/*106 - */
			sadTexts.push("Você nunca viu o filme Casablanca. É importante conhecer os clássicos do cinema! Você perdeu sua vez e não joga esta rodada.");
			
			/*107 - */
			sadTexts.push("Você não conhece a história de Ghandi. Então não deve saber que é possível mudar a história de um país de forma pacífica. Você perdeu sua vez e não joga esta rodada.");
			
			/*108 - */
			sadTexts.push("Você pegou um ônibus e ele estava cheio de baratas. Que nojo! Você perdeu sua vez e não joga esta rodada.");
			
			/*109 - */
			sadTexts.push("Você deixou a geladeira da casa de praia cheia porque ia voltar na semana seguinte. Quando voltou, a cidade tinha ficado dois dias sem luz e a comida estragou. Você perdeu sua vez e não joga esta rodada.");
			
			/*110 - */
			sadTexts.push("Você acha que não existem políticos como Odorico Paraguaçu, de O Bem-Amado. É melhor você começar a ler jornal... Você perdeu sua vez e não joga esta rodada.");
			
			/*111 - */
			sadTexts.push("Você acha o Agostinho Carrara, de A Grande Família, uma pessoa correta. É hora de rever seus valores! Você perdeu sua vez e não joga esta rodada.");
			
			/*112 - */
			sadTexts.push("Você tinha certeza de que o seu time ia ganhar o jogo contra um time bem mais fraco. Deu zebra! Você perdeu sua vez e não joga esta rodada.");
			
			/*113 - */
			sadTexts.push("Você acha que o Sol é uma estrela gigante. Aproveite para estudar um pouco de astronomia. Você perdeu sua vez e não joga esta rodada.");
			
			/*114 - */
			sadTexts.push("Você acha que big bang é um filme de faroeste. Que tal se informar melhor? Você perdeu sua vez e não joga esta rodada.");
			
			/*115 - */
			sadTexts.push("Você descobriu a senha da sua irmã na internet e pretende xeretar as coisas dela. Que absurdo! Você perdeu sua vez e não joga esta rodada.");
			
			/*116 - */
			sadTexts.push("Você forneceu seus dados pessoais a uma pessoa que você não conhece. Isso é perigoso. Você perdeu sua vez e não joga esta rodada.");
			
			/*117 - */
			sadTexts.push("Você quebrou o sistema de segurança de um software só para mostrar que era capaz. Você perdeu sua vez e não joga esta rodada.");
			
			/*118 - */
			sadTexts.push("Você gosta de repassar mensagens idiotas e incômodas. Aproveite seu tempo na internet para aprender alguma coisa. Você perdeu sua vez e não joga esta rodada.");
			
			/*119 - */
			sadTexts.push("Você usa o computador do seu trabalho para falar mal do seu chefe. Você perdeu sua vez e não joga esta rodada.");
			
			/*120 - */
			sadTexts.push("Você comprou pela internet no site de uma empresa que você nunca ouviu falar e acabou levando prejuízo.Tome mais cuidado da próxima vez. Você perdeu sua vez e não joga esta rodada.");
			
			/*121 - */
			sadTexts.push("Você é a favor do controle da internet pelos governos. Isso é censura. Você perdeu sua vez e não joga esta rodada.");
			
			/*122 - */
			sadTexts.push("Você fica o dia inteiro falando com seus amigos pelas redes sociais. Aproveite seu tempo para sair de casa e se exercitar um pouco. Você perdeu sua vez e não joga esta rodada.");
			
			/*123 - */
			sadTexts.push("Você acredita em tudo que lê e vê na internet. Tome cuidado! Você perdeu sua vez e não joga esta rodada.");
			
			/*124 - */
			sadTexts.push("Você recebeu uma mensagem suspeita por e-mail, não a apagou e ainda mandou para um amigo. Fique esperto da próxima vez. Você perdeu sua vez e não joga esta rodada.");
			
			/*125 - */
			sadTexts.push("Você não sabe identificar uma tentativa de phishing. Fique atento e aprenda a ter segurança na internet. Você perdeu sua vez e não joga esta rodada.");
			
			/*126 - */
			sadTexts.push("Você não sabe se o site de compras em que encomendou seus presentes de Natal é confiável. Mais cuidado da próxima vez! Você perdeu sua vez e não joga esta rodada.");
			
			/*127 - */
			sadTexts.push("Você recebeu uma mensagem cujo conteúdo era cyberbullying e a repassou. Isso não se faz. Você perdeu sua vez e não joga esta rodada.");
			
			/*128 - */
			sadTexts.push("Você está ajudando a divulgar boatos pela internet. Você é um “cyberchato”. Você perdeu sua vez e não joga esta rodada.");
			
			/*129 - */
			sadTexts.push("Você usa linguagem grosseira na internet. Que feio! Você perdeu sua vez e não joga esta rodada.");
			
			/*130 - */
			sadTexts.push("Você aceitou um presente de uma empresa para falar bem dela em uma rede social. Isso não é ético. Você perdeu sua vez e não joga esta rodada.");
			
			/*131 - */
			sadTexts.push("Você conta para todo mundo na rede que marcou um encontro com seus amigos em um bar e dá o endereço. Você não sabe cuidar da sua segurança. Você perdeu sua vez e não joga esta rodada..");
			
			/*132 - */
			sadTexts.push("Você deixou suas fotos abertas para qualquer um ver em seu perfil em uma rede social. Isso não é seguro. Você perdeu sua vez e não joga esta rodada.");
			
			/*133 - */
			sadTexts.push("Você compartilhou seu endereço e telefone em uma rede social. Torça para não receber visitas indesejadas. Você perdeu sua vez e não joga esta rodada.");
			
			/*134 - */
			sadTexts.push("Você tirou a maior onda na rede se fazendo passar por milionário. Cuidado!, pois internet não é lugar para falar sobre vida financeira. Você perdeu sua vez e não joga esta rodada.");
			
			/*135 - */
			sadTexts.push("Você deu a senha do seu perfil em uma rede social para sua melhor amiga. Se vocês brigarem, eu não quero estar na sua pele! Você perdeu sua vez e não joga esta rodada.");
			
			/*136 - */
			sadTexts.push("Você colocou uma dica de sua senha no mural do seu perfil Orkut. Você gosta de viver perigosamente... Você perdeu sua vez e não joga esta rodada.");
			
			/*137 - */
			sadTexts.push("Você colocou na rede os planos de expansão da empresa onde você trabalha. Tenha mais cuidado da próxima vez e reze para não ser demitido! Você perdeu sua vez e não joga esta rodada.");
			
			/*138 - */
			sadTexts.push("Você criou uma pequena empresa e não sabe colocar informações sobre ela na internet. Você precisa se informar direito! Você perdeu sua vez e não joga esta rodada.");
			
			/*139 - */
			sadTexts.push("Você passa o dia inteiro ouvindo música bem alto no seu MP3 player. Se prepare para ter problemas auditivos. Você perdeu sua vez e não joga esta rodada.");
			
			/*140 - */
			sadTexts.push("Você não sabe por que alguns e-mails têm letras e símbolos sem qualquer significado no endereço. Vá procurar dicas sobre segurança na internet. Você perdeu sua vez e não joga esta rodada.");
			
			/*141 - */
			sadTexts.push("Você acha que um site com o símbolo do cadeado na barra de endereços é seguro. Você esqueceu que o cadeado tem que estar no navegador. Você perdeu sua vez e não joga esta rodada."); 
			
			/*142 - */
			sadTexts.push("Você acessou sua conta bancária num computador de um cybercafé. Deveria saber que isso é arriscado. Você perdeu sua vez e não joga esta rodada.");
			
			/*143 - */
			sadTexts.push("Você abriu uma mensagem com um título suspeito que despertou sua curiosidade e descobriu que era um vírus. Tome mais cuidado da próxima vez! Você perdeu sua vez e não joga esta rodada.");
			
			/*144 - */
			sadTexts.push("Você marcou encontro com uma pessoa que conheceu pela internet sem tomar o menor cuidado! Você perdeu sua vez e não joga esta rodada.");
			
			/*145 - */
			sadTexts.push("A tecla Caps Lock do seu computador estava ativada e você nem prestou atenção quando mandou uma mensagem. Não grite com as pessoas! Você perdeu sua vez e não joga esta rodada.");
			
			/*146 - */
			sadTexts.push("Você usou seu pen drive em um computador público e não passou o antivírus nele depois. Sabia que você colocou seu computador em risco? Você perdeu sua vez e não joga esta rodada.");
			
			/*147 - */
			sadTexts.push("Você está usando a mesma senha há uns dois anos. Isso não é seguro. Você perdeu sua vez e não joga esta rodada.");
			
			/*148 - */
			sadTexts.push("Você colocou como senha a data do seu aniversário. Isso não é seguro! Você perdeu sua vez e não joga esta rodada.");
			
			/*149 - */
			sadTexts.push("Você está bastante acima do peso e não quer saber de exercícios. Você perdeu sua vez e não joga esta rodada.");
			
			/*150 - */
			sadTexts.push("Você se perdeu e rodou 50 quilômetros a mais por ter vergonha de pedir informações. Você perdeu sua vez e não joga esta rodada.");
			
			/*151 - */
			sadTexts.push("Você acha que não tem problema um político renunciar ao mandato para escapar de uma cassação. Que vergonha! Você perdeu sua vez e não joga esta rodada.");
			
			/*152 - */
			sadTexts.push("Você acredita que político tem que aproveitar o mandato para juntar bastante dinheiro. Que absurdo! Você perdeu sua vez e não joga esta rodada.");
			
			/*153 - */
			sadTexts.push("Você comeu bastante camarão e ficou todo inchado! Só aí descobriu que era alérgico a frutos do mar. Você perdeu sua vez e não joga esta rodada.");
			
			/*154 - */
			sadTexts.push("Para você, é normal um político desrespeitar a lei eleitoral. Você não é ético! Você perdeu sua vez e não joga esta rodada..");
			
			/*155 - */
			sadTexts.push("Para você, um político que trabalha bem está fazendo um favor à sociedade. Isso é obrigação dele. Você perdeu sua vez e não joga esta rodada."); 
			
			/*156 - */
			sadTexts.push("Você não pratica nenhuma atividade física. Deixe de ser preguiçoso! Você perdeu sua vez e não joga esta rodada.");
			
			/*157 - */
			sadTexts.push("Você acha que fazer uma caminhada três vezes por semana não vai melhorar sua saúde e prefere ser sedentário. Você perdeu sua vez e não joga esta rodada.");
			
			/*158 - */
			sadTexts.push("Você acha normal um jogador de futebol ser desleal em campo. Que coisa feia! Você perdeu sua vez e não joga esta rodada.");
			
			/*159 - */
			sadTexts.push("Você acha que o seu time tem que vencer o jogo de qualquer maneira, nem que seja com gol de mão. Você perdeu sua vez e não joga esta rodada.");
			
			/*160 - */
			sadTexts.push("Você gosta de levar vantagem em tudo. Que tal ter um pouco mais de ética? Você perdeu sua vez e não joga esta rodada.");
			
			/*161 - */
			sadTexts.push("Você enfrentou a maior fila para ver o show da sua banda favorita. Quando estava chegando a sua vez, os ingressos acabaram. Que pena! Você perdeu sua vez e não joga esta rodada.");
			
			/*162 - */
			sadTexts.push("Seu ídolo no futebol tentou fazer uma defesa e acabou marcando um gol contra na final do campeonato. Você perdeu sua vez e não joga esta rodada.");
			
			/*163 - */
			sadTexts.push("Você não come salada de jeito nenhum. Aprenda a consumir alimentos saudáveis. Você perdeu sua vez e não joga esta rodada..");
			
			/*164 - */
			sadTexts.push("Você diz não gostar de um tipo de comida que nunca experimentou. Aproveite para conhecer novos sabores! Você perdeu sua vez e não joga esta rodada.");
			
			/*165 - */
			sadTexts.push("Você foi a Belém e não experimentou nenhum prato típico de lá. Deixou de conhecer parte da cultura local. Você perdeu sua vez e não joga esta rodada..");
			
			/*166 - */
			sadTexts.push("Você só come arroz com feijão. Aprenda a se alimentar. Você perdeu sua vez e não joga esta rodada..");
			
			/*167 - */
			sadTexts.push("Você é mais preguiçoso que o Garfield. Mexa-se! Você perdeu sua vez e não joga esta rodada..");
			
			/*168 - */
			sadTexts.push("Você não gosta muito de tomar banho! Credo! Você perdeu sua vez e não joga esta rodada..");
			
			/*169 - */
			sadTexts.push("Você nunca leu uma revista em quadrinhos. Elas também fazem parte da nossa cultura. Você perdeu sua vez e não joga esta rodada..");
			
			/*170 - */
			sadTexts.push("Você foi fazer um jantar especial para a sua sogra e deixou a comida queimar. Pratique mais da próxima vez. Você perdeu sua vez e não joga esta rodada.");
			
			/*171 - */
			sadTexts.push("Você não sabe o que é mangá. Que tal se informar melhor? Você perdeu sua vez e não joga esta rodada.");
			
			/*172 - */
			sadTexts.push("Você não vê desenhos animados porque acha que eles são para crianças. Está perdendo uma diversão e tanto! Você perdeu sua vez e não joga esta rodada.");
			
			/*173 - */
			sadTexts.push("Você sempre chega atrasado nos seus compromissos. Seja educado e aprenda que pontualidade é importante. Você perdeu sua vez e não joga esta rodada.");
			
			/*174 - */
			sadTexts.push("Sua gata teve um monte de filhotes e você não quer ficar com eles. Você os abandona em um lugar bem longe da sua casa. Você perdeu sua vez e não joga esta rodada..");
			
			/*175 - */
			sadTexts.push("Você acha que Schindler é apenas a marca de um elevador. Veja o filme A Lista de Schindler.  Você perdeu sua vez e não joga esta rodada.");
			
			/*176 - */
			sadTexts.push("Para você, supernova é só aquela roupa que comprou ontem. Cuidado para não ser deportado para o espaço! Você perdeu sua vez e não joga esta rodada.");
			
			/*177 - */
			sadTexts.push("Você acha que as imagens que vê na Lua são de São Jorge e seu cavalo. Fala sério! Você perdeu sua vez e não joga esta rodada.");
			
			/*178 - */
			sadTexts.push("Você acredita que um eclipse é sinal de catástrofe. Tome cuidado para não encontrar com o Lobisomem por aí! Você perdeu sua vez e não joga esta rodada.");
			
			/*179 - */
			sadTexts.push("Você estava lendo um livro ótimo e teve uma surpresa e tanto no final. Estavam faltando as últimas duas páginas! Você perdeu sua vez e não joga esta rodada.");
			
			/*180 - */
			sadTexts.push("Alguém lhe disse que, se você passar a noite toda na beira do mar olhando o céu, vai ver a Aurora Boreal. O pior é que você acreditou! Você perdeu sua vez e não joga esta rodada.");
			
			/*181 - */
			sadTexts.push("Você acredita que se apontar o dedo para uma estrela nascem verrugas em sua mão. Você perdeu sua vez e não joga esta rodada.");
			
			/*182 - */
			sadTexts.push("Você nunca viu a Constelação do Cruzeiro do Sul. Aproveite a dica para localizá-la no céu. Você perdeu sua vez e não joga esta rodada.");
			
			/*183 - */
			sadTexts.push("Você acha que Caxemira é o material daqueles casacos quentinhos e leves... Você perdeu sua vez e não joga esta rodada.");
			
			/*184 - */
			sadTexts.push("Você se refere aos turcos como árabes. Que feio! Você perdeu sua vez e não joga esta rodada.");
			
			/*185 - */
			sadTexts.push("Você comprou uma roupa linda, mas ela ficou toda manchada logo na primeira lavagem. Você perdeu sua vez e não joga esta rodada.");
			
			/*186 - */
			sadTexts.push("Você acha que Kim Jong-Il, da Coreia do Norte, é chamado de “querido líder” porque é adorado pelo povo. Você perdeu sua vez e não joga esta rodada.");
			
			/*187 - */
			sadTexts.push("Para você, tigre asiático é só aquele animal listrado... Ler jornal é importante, sabia? Você perdeu sua vez e não joga esta rodada.");
			
			/*188 - */
			sadTexts.push("Você vai viajar para a Europa e comprou dólares. Devia ter comprado euros. Você perdeu sua vez e não joga esta rodada.");
			
			/*189 - */
			sadTexts.push("Você marcou um encontro com uma pessoa especial e o seu desodorante estava vencido! Tomara que você seja bom de conversa... Você perdeu sua vez e não joga esta rodada.");
			
			/*190 - */
			sadTexts.push("Você saiu e deixou o seu computador ligado. Aprenda a economizar energia! Você perdeu sua vez e não joga esta rodada.");
			
			/*191 - */
			sadTexts.push("Você nunca colaborou numa campanha de doação de alimentos. Aprenda a ser solidário. Você perdeu sua vez e não joga esta rodada.");
			
			/*192 - */
			sadTexts.push("Você acha a participação do Brasil em missões de paz da ONU um desperdício de dinheiro. Ajudar a manter a paz é dever de todos. Você perdeu sua vez e não joga esta rodada.");
			
			/*193 - */
			sadTexts.push("Você esqueceu de abastecer o carro e parou no meio da estrada por falta de combustível. Você perdeu sua vez e não joga esta rodada.");
			
			/*194 - */
			sadTexts.push("Você acha que o holocausto dos judeus na Segunda Guerra Mundial não aconteceu. Você perdeu sua vez e não joga esta rodada.");
			
			/*195 - */
			sadTexts.push("Você não conhece Ouro Preto. Aprenda mais sobre essa joia de Minas Gerais! Você perdeu sua vez e não joga esta rodada.");
			
			/*196 - */
			sadTexts.push("Você nunca ouviu falar do Monstro do Lago Ness. Cuidado, senão ele vai pegar você! Você perdeu sua vez e não joga esta rodada.");
			
			/*197 - */
			sadTexts.push("Você pensa que a Golden Gate é uma ponte de ouro. Você perdeu sua vez e não joga esta rodada..");
			
			/*198 - */
			sadTexts.push("Você acha que o Taj Mahal fica na China. Você perdeu sua vez e não joga esta rodada.");
			
			/*199 - */
			sadTexts.push("Para você, Machu Picchu é um pokémon. Você perdeu sua vez e não joga esta rodada.");
			
			/*200 - */
			sadTexts.push("Você quer atravessar o Eurotúnel de carro. Desista desse plano absurdo! Você perdeu sua vez e não joga esta rodada.");
			
			/*201 - */
			sadTexts.push("Você pensa que não existe vida no deserto. Estude mais um pouco! Você perdeu sua vez e não joga esta rodada.");
			
			/*202 - */
			sadTexts.push("Para você, a cidade do fim do mundo é o cemitério. Estude Geografia. Você perdeu sua vez e não joga esta rodada.");
			
			/*203 - */
			sadTexts.push("Pangeia, para você, é o nome de uma doença. Você perdeu sua vez e não joga esta rodada.");
			
			/*204 - */
			sadTexts.push("Você pensa que Gondwana é um lugar que aparece nos filmes do Tarzan. Você perdeu sua vez e não joga esta rodada.");
			
			/*205 - */
			sadTexts.push("Você acredita que os Estados Unidos são maiores que o Canadá. Não confunda poder com extensão territorial. Você perdeu sua vez e não joga esta rodada.");
			
			/*206 - */
			sadTexts.push("Você acredita que o deserto do Saara fica no Egito. Está precisando estudar mais! Você perdeu sua vez e não joga esta rodada.");
			
			/*207 - */
			sadTexts.push("Você jura que o Chile faz fronteira com o Brasil. Você perdeu sua vez e não joga esta rodada.");
			
			/*208 - */
			sadTexts.push("Para você, Caiena é só o nome de uma pimenta. Que tal se informar melhor? Você perdeu sua vez e não joga esta rodada.");
			
			/*209 - */
			sadTexts.push("Para você, Lençóis Maranhenses são uma bela roupa de cama. Você perdeu sua vez e não joga esta rodada.");
			
			/*210 - */
			sadTexts.push("Você nunca ouviu falar do Parque de Inhotim. Você é muito pouco informado, sabia? Você perdeu sua vez e não joga esta rodada.");
			
			/*211 - */
			sadTexts.push("Lagoa dos Patos não é aquela lagoa cheia de aves como você acredita.Vá conhecer o sul do Brasil. Você perdeu sua vez e não joga esta rodada.");
			
			/*212 - */
			sadTexts.push("Você não conhece a lenda do boto. Procure conhecer o nosso folclore. Você perdeu sua vez e não joga esta rodada.");
			
			/*213 - */
			sadTexts.push("Você não sabe de onde vem o poder do Saci Pererê. Você perdeu sua vez e não joga esta rodada.");
			
			/*214 - */
			sadTexts.push("A única cuca que você conhece é um bolo de banana. Você perdeu sua vez e não joga esta rodada.");
			
			/*215 - */
			sadTexts.push("Você cortou uma árvore! Não agrida a natureza. Você perdeu sua vez e não joga esta rodada.");
			
			/*216 - */
			sadTexts.push("Você não conhece a lenda do Curupira. Você perdeu sua vez e não joga esta rodada.");
			
			/*217 - */
			sadTexts.push("Você viu a Mula-sem-cabeça e não sabia como desfazer o encantamento. Precisa conhecer o folclore brasileiro. Você perdeu sua vez e não joga esta rodada.");
			
			/*218 - */
			sadTexts.push("Você perdeu seu relógio e não consegue encontrá-lo. Se conhecesse o nosso folclore, saberia que é só pedir ajuda ao Negrinho do Pastoreio. Você perdeu sua vez e não joga esta rodada.");
			
			/*219 - */
			sadTexts.push("Você provocou um incêndio na mata. Isso é crime ambiental! Você perdeu sua vez e não joga esta rodada.");
			
			/*220 - */
			sadTexts.push("Você acha que Boitatá é uma dança. Você perdeu sua vez e não joga esta rodada.");
			
			/*221 - */
			sadTexts.push("Você nunca leu um conto de fadas para uma criança. Você perdeu sua vez e não joga esta rodada..");
			
			/*222 - */
			sadTexts.push("O crescimento desordenado está acabando com as áreas verdes da sua cidade. Por isso, micos e gambás invadiram sua casa atrás de comida. Você perdeu sua vez e não joga esta rodada.");
			
			/*223 - */
			sadTexts.push("Você acha que Jaci é só nome de gente. Precisa conhecer a cultura dos índios brasileiros. Você perdeu sua vez e não joga esta rodada.");
			
			/*224 - */
			sadTexts.push("Um período de seca deixa a umidade do ar muito baixa na sua cidade e você tem dificuldades para respirar. Você perdeu sua vez e não joga esta rodada.");
			
			/*225 - */
			sadTexts.push("Você fez um empréstimo para uma viagem de férias. Da próxima vez, economize para juntar o dinheiro necessário. Você perdeu sua vez e não joga esta rodada.");
			
			/*226 - */
			sadTexts.push("Você comprou um objeto inútil, só porque achou bonito. Não seja consumista. Você perdeu sua vez e não joga esta rodada.");
			
			/*227 - */
			sadTexts.push("Você foi ao supermercado sem uma lista e comprou mais do que precisava. Você perdeu sua vez e não joga esta rodada.");
			
			/*228 - */
			sadTexts.push("Você compra tudo que tem vontade, sem estabelecer prioridades. Dinheiro não cai do céu, sabia? Você perdeu sua vez e não joga esta rodada.");
			
			/*229 - */
			sadTexts.push("Você não ensina as crianças a fazerem escolhas financeiras. É importante aprender essas coisas desde pequeno. Você perdeu sua vez e não joga esta rodada.");
			
			/*230 - */
			sadTexts.push("Você gasta dinheiro sem preocupações. Até para gastar dinheiro é preciso ter ética. Você perdeu sua vez e não joga esta rodada.");
			
			/*231 - */
			sadTexts.push("Você vive enchendo seu sobrinho de presentes. Assim, ele vai ficar muito mimado! Você perdeu sua vez e não joga esta rodada.");
			
			/*232 - */
			sadTexts.push("Você quer um dinheiro extra dos seus pais porque tirou boas notas nas provas. Você perdeu sua vez e não joga esta rodada.");
			
			/*233 - */
			sadTexts.push("Seus avós lhe dão dinheiro todo mês e você torra tudo... Aprenda a poupar. Você perdeu sua vez e não joga esta rodada.");
			
			/*234 - */
			sadTexts.push("Você não trocou as lâmpadas da sua casa por fluorescentes. Está jogando dinheiro no lixo, sabia? Você perdeu sua vez e não joga esta rodada.");
			
			/*235 - */
			sadTexts.push("Você foi tomar banho e deixou a TV ligada. Aprenda a desligar os aparelhos quando não estiverem em uso. Você perdeu sua vez e não joga esta rodada.");
			
			/*236 - */
			sadTexts.push("Você ganhou uma viagem num sorteio. Quando chegou lá, o hotel estava cheio de pulgas e carrapatos. Cuidado com as coceiras! Você perdeu sua vez e não joga esta rodada.");
			
			/*237 - */
			sadTexts.push("Você esqueceu de pagar suas contas e gastou mais dinheiro em multas e juros. Lembre-se sempre de que pagar em dia é mais barato. Você perdeu sua vez e não joga esta rodada.");
			
			/*238 - */
			sadTexts.push("Você tem preguiça de pesquisar antes de comprar. Aprenda a ser um consumidor consciente. Você perdeu sua vez e não joga esta rodada.");
			
			/*239 - */
			sadTexts.push("Você não tem ideia de como gasta o seu dinheiro. Você precisa anotar os seus gastos. Você perdeu sua vez e não joga esta rodada.");
			
			/*240 - */
			sadTexts.push("Você gasta todo o dinheiro que recebe. Aprenda a poupar! Você perdeu sua vez e não joga esta rodada.");
			
			/*241 - */
			sadTexts.push("Você esperou até quase o fim do mês para aplicar o dinheiro que ia poupar. Como gastou demais, claro que não sobrou nada. Você perdeu sua vez e não joga esta rodada.");
			
			/*242 - */
			sadTexts.push("Você nunca estabeleceu nenhuma meta para sua vida. Assim você não vai chegar a lugar algum. Você perdeu sua vez e não joga esta rodada.");
			
			/*243 - */
			sadTexts.push("Você quer melhorar sua posição no trabalho, mas não faz nada para isso. Você perdeu sua vez e não joga esta rodada.");
			
			/*244 - */
			sadTexts.push("Você não tem um orçamento doméstico. Você perdeu sua vez e não joga esta rodada.");
			
			/*245 - */
			sadTexts.push("Você não sabe o quanto paga de luz e condomínio. Faça já um orçamento. Você perdeu sua vez e não joga esta rodada.");
			
			/*246 - */
			sadTexts.push("Você não sabe quanto gasta por mês no supermercado. Passe a guardar as notas até descobrir. Você perdeu sua vez e não joga esta rodada.");
			
			/*247 - */
			sadTexts.push("Você prefere guardar as moedas no cofrinho em vez de trocá-las. Sabia que faltam moedas para troco no mercado? Você perdeu sua vez e não joga esta rodada.");
			
			/*248 - */
			sadTexts.push("Você não sabe qual é a melhor aplicação financeira para o seu caso e acabou perdendo dinheiro. Peça conselhos ao gerente do seu banco. Você perdeu sua vez e não joga esta rodada.");
			
			/*249 - */
			sadTexts.push("Você já tem pouco dinheiro e ainda precisa pagar a anuidade do cartão de crédito. Sabia que existem cartões sem anuidade? Você perdeu sua vez e não joga esta rodada.");
			
			/*250 - */
			sadTexts.push("Você comprou mais do que devia e vai ter que parcelar a dívida do cartão. Você perdeu sua vez e não joga esta rodada.");
			
			/*251 - */
			sadTexts.push("Você limpou a calçada da sua casa com água. A calçada deve ser limpa com a vassoura. Você perdeu sua vez e não joga esta rodada.");
			
			/*252 - */
			sadTexts.push("Você comprou um monte de frutas e verduras para não ter que ir ao mercado todo dia e acabou tendo que jogar muita coisa no lixo. Não desperdice! Você perdeu sua vez e não joga esta rodada.");
			
			/*253 - */
			sadTexts.push("Você só usa o telefone celular, mesmo para falar com números de telefone fixos. Confira as tarifas e economize! Você perdeu sua vez e não joga esta rodada.");
			
			/*254 - */
			sadTexts.push("Você fica o dia inteiro no telefone com seus amigos. Se você gosta tanto de falar com eles, faça isso pessoalmente. Você perdeu sua vez e não joga esta rodada.");
			
			/*255 - */
			sadTexts.push("Você nunca conferiu seu extrato bancário. E se estiverem lhe fazendo cobranças indevidas? Você perdeu sua vez e não joga esta rodada.");
			
			/*256 - */
			sadTexts.push("Você compra roupas todo mês. Que tal passar a comprar só o que você precisa? Você perdeu sua vez e não joga esta rodada.");
			
			/*257 - */
			sadTexts.push("Você nunca vai ao teatro porque os ingressos são caros, mas não deixa de gastar com supérfluos. Você perdeu sua vez e não joga esta rodada.");
			
			/*258 - */
			sadTexts.push("Você não sabe como fazer um bom programa sem gastar muito dinheiro. Seja criativo! Você perdeu sua vez e não joga esta rodada.");
			
			/*259 - */
			sadTexts.push("Você entrou numa loja para comprar uma roupa que estava em promoção e acabou comprando mais duas que não estavam. Você perdeu sua vez e não joga esta rodada.");
			
			/*260 - */
			sadTexts.push("Você ganhou um DVD do filme que queria. Quando abriu a caixa, descobriu que era pirata! Você perdeu sua vez e não joga esta rodada.");
			
			/*261 - */
			sadTexts.push("Você estava com fome e resolveu passar no supermercado. Comprou coisas que você não vai conseguir comer. Da próxima vez, faça um lanche antes das compras. Você perdeu sua vez e não joga esta rodada.");
			
			/*262 - */
			sadTexts.push("Você lava roupa na máquina todo dia, mesmo em pouca quantidade. Não desperdice energia. Espere até ter roupa para encher a máquina de lavar. Você perdeu sua vez e não joga esta rodada.");
			
			/*263 - */
			sadTexts.push("A torneira da sua cozinha está pingando há uma semana. Você perdeu sua vez e não joga esta rodada.");
			
			/*264 - */
			sadTexts.push("Você comprou uma sanduicheira de uma marca desconhecida porque ela era mais barata e acabou levando prejuízo. Da próxima vez, procure se informar sobre o fabricante. Você perdeu sua vez e não joga esta rodada.");
			
			/*265 - */
			sadTexts.push("Mesmo tendo pouco dinheiro, você tem conta em dois bancos diferentes. Resultado: paga tarifas nos dois! Você perdeu sua vez e não joga esta rodada.");
			
			/*266 - */
			sadTexts.push("Você não sai sem o cartão de crédito. Aprenda a só sair com ele quando for realmente necessário. Você perdeu sua vez e não joga esta rodada.");
			
			/*267 - */
			sadTexts.push("Você acha que a sua religião é a única certa e vive discutindo com os outros. Aprenda a respeitar as outras religiões. Você perdeu sua vez e não joga esta rodada.");
			
			/*268 - */
			sadTexts.push("Você acha que algumas religiões são coisa do demônio. Conheça os princípios de todas as religiões para aprender a respeitá-las. Você perdeu sua vez e não joga esta rodada.");
			
			/*269 - */
			sadTexts.push("Você acha que algumas religiões deviam ser proibidas. A liberdade religiosa é um direito de todos. Você perdeu sua vez e não joga esta rodada.");
			
			/*270 - */
			sadTexts.push("Você acha que não existe nada em comum entre as várias religiões. Você perdeu sua vez e não joga esta rodada.");
			
			/*271 - */
			sadTexts.push("Você tem opinião sobre todas as religiões, mas não conhece nenhuma delas. Não dá para formar opinião sem conhecer, né? Você perdeu sua vez e não joga esta rodada.");
			
			/*272 - */
			sadTexts.push("Você acha que essa história de chuva ácida é bobagem. Você perdeu sua vez e não joga esta rodada.");
			
			/*273 - */
			sadTexts.push("Você acha que os esportes não têm nada a ver com a ciência. Você precisa se informar melhor sobre os dois assuntos. Você perdeu sua vez e não joga esta rodada.");
			 
			/*274 - */
			sadTexts.push("Você acredita que os acontecimentos da natureza são imprevisíveis e não se preocupa com o meio ambiente. Procure se informar! Você perdeu sua vez e não joga esta rodada.");
			
			/*275 - */
			sadTexts.push("Você não sabe o que tem em comum com um sapo ou um pernilongo. Você perdeu sua vez e não joga esta rodada."); 
			/*276 - */
			sadTexts.push("As autoridades fizeram um alerta por causa do tempo seco e você saiu para caminhar ao meio-dia. Isso pode provocar danos à sua saúde. Você perdeu sua vez e não joga esta rodada.");
			
			/*277 - */
			sadTexts.push("Você dorme até tarde e só vai à praia por volta do meio-dia. Não se deve tomar sol entre meio-dia e três da tarde. Você perdeu sua vez e não joga esta rodada.");
			
			/*278 - */
			sadTexts.push("Você não usa camisinha porque não faz parte de grupos de risco. Grupo de risco inclui todo mundo que não usa camisinha! Você perdeu sua vez e não joga esta rodada.");
			
			/*279 - */
			sadTexts.push("Você não usa camisinha porque só faz sexo com o seu parceiro. Grande parte dos casos de Aids surge entre parceiros estáveis. Você perdeu sua vez e não joga esta rodada.");
			
			/*280 - */
			sadTexts.push("Você acredita que nunca comeu um alimento transgênico. Isso mostra que você não lê os rótulos dos produtos que consome. Você perdeu sua vez e não joga esta rodada.");
			
			/*281 - */
			sadTexts.push("Você está entre as pessoas que podem doar sangue, mas nunca o fez. Isso é que é falta de solidariedade! Você perdeu sua vez e não joga esta rodada.");
			
			/*282 - */
			sadTexts.push("Você não sabe qual é o seu tipo sanguíneo. Faça um exame para descobrir o mais rápido possível. Você perdeu sua vez e não joga esta rodada.");
			
			/*283 - */
			sadTexts.push("Você nunca visitou um museu. Você perdeu sua vez e não joga esta rodada.");
			
			/*284 - */
			sadTexts.push("Você entrou no elevador e logo depois a luz acabou. Para piorar, sua vizinha chata estava junto e não parou de falar um minuto! Você perdeu sua vez e não joga esta rodada.");
			
			/*285 - */
			sadTexts.push("Você coloca pilhas usadas no lixo. Da próxima vez, procure um posto de coleta de pilhas. Você perdeu sua vez e não joga esta rodada.");
			
			/*286 - */
			sadTexts.push("Seu computador pifou e você o colocou no lixo. Da próxima vez, entre em contato com o fabricante. Você perdeu sua vez e não joga esta rodada.");
			
			/*287 - */
			sadTexts.push("Você não gosta de nenhum passatempo que o faça pensar. É preciso exercitar os neurônios. Você perdeu sua vez e não joga esta rodada.");
			
			/*288 - */
			sadTexts.push("Você acha que a elevação do nível do mar é balela dos defensores do meio ambiente. Você perdeu sua vez e não joga esta rodada.");
			
			/*289 - */
			sadTexts.push("Um deficiente visual que estava ao seu lado queria atravessar a rua e você não o ajudou porque estava com pressa. Que coisa feia! Você perdeu sua vez e não joga esta rodada.");
			
			/*290 - */
			sadTexts.push("Você fumou em um restaurante e o proprietário levou uma multa por causa disso. Além de prejudicar a sua saúde, ainda prejudica a dos outros. Você perdeu sua vez e não joga esta rodada.");
			
			/*291 - */
			sadTexts.push("Você planejou uma superfesta no seu aniversário e, quando chegou o grande dia, amanheceu doente. Você perdeu sua vez e não joga esta rodada.");
			
			/*292 - */
			sadTexts.push("Você saiu para ir a um show que estava esperando há um mês, ficou preso num megaengarrafamento e não chegou a tempo. Você perdeu sua vez e não joga esta rodada.");
			
			/*293 - */
			sadTexts.push("Você não exige nota fiscal e depois reclama da sonegação de impostos. Você perdeu sua vez e não joga esta rodada.");
			
			/*294 - */
			sadTexts.push("Você reclama dos roubos de carro, mas compra peças de procedência duvidosa quando precisa consertar o seu. Você perdeu sua vez e não joga esta rodada.");
			
			/*295 - */
			sadTexts.push("Uma ventania muito forte derrubou um galho de árvore em cima do seu carro. Que prejuízo! Você perdeu sua vez e não joga esta rodada.");
			
			/*296 - */
			sadTexts.push("Você compra produtos sabendo que eles são falsificados. Não estimule o crime. Você perdeu sua vez e não joga esta rodada..");
			
			/*297 - */
			sadTexts.push("Você já comprou produtos sabendo que eles provavelmente eram roubados. Você perdeu sua vez e não joga esta rodada.");
			
			/*298 - */
			sadTexts.push("Você ficou doente. Em vez de ir ao médico, foi à farmácia e pediu um remédio ao balconista. Tomar medicamentos sem receita é perigoso. Você perdeu sua vez e não joga esta rodada.");
			
			/*299 - */
			sadTexts.push("Você acha que a maioria das pessoas que moram em favelas são criminosas. Não seja preconceituoso. Você perdeu sua vez e não joga esta rodada.");
			
			/*300 - */
			sadTexts.push("Você reclama dos roubos, mas tem um ponto pirata de TV a cabo. Isso também é roubo! Você perdeu sua vez e não joga esta rodada.");
			
			/*301 - */
			sadTexts.push("Depois de meses longe da praia, finalmente você decide colocar o bronzeado em dia. Quando você chega lá, o mar está imundo e impróprio para o banho. Você perdeu sua vez e não joga esta rodada.");
			
			/*302 - */
			sadTexts.push("Você critica a existência de pessoas pedindo esmolas, mas dá dinheiro a elas. Você perdeu sua vez e não joga esta rodada.");
			
			/*303 - */
			sadTexts.push("Você acha um absurdo a atuação dos flanelinhas, mas paga a eles por uma vaga. Você perdeu sua vez e não joga esta rodada.");
			
			/*304 - */
			sadTexts.push("Sempre que a chuva alaga a cidade você xinga o prefeito, mas não deixa de jogar lixo na rua. Cuidar bem da cidade também é seu dever. Você perdeu sua vez e não joga esta rodada.");
			
			/*305 - */
			sadTexts.push("Você sempre reclama do trânsito, mas fecha cruzamentos. Você perdeu sua vez e não joga esta rodada.");
			
			/*306 - */
			sadTexts.push("Você avançou o sinal vermelho. Colocou em risco a vida de inocentes! Você perdeu sua vez e não joga esta rodada..");
			
			/*307- */
			sadTexts.push("Você bebeu demais e deu vexame no bar. Que vergonha! Você perdeu sua vez e não joga esta rodada.");
			
			/*308 - */
			sadTexts.push("Você acha um absurdo a atuação de cambistas, mas compra deles. Se ninguém comprasse, eles não existiriam. Você perdeu sua vez e não joga esta rodada.");
			
			/*309 - */
			sadTexts.push("Seu amigo pretende votar em um político. Você sabe que ele é corrupto, mas não diz nada. Você perdeu sua vez e não joga esta rodada.");
			
			/*310 - */
			sadTexts.push("Você participou de um trote violento nos calouros da sua faculdade. Que coisa feia! Você perdeu sua vez e não joga esta rodada.");
			
			/*311 - */
			sadTexts.push("Você sempre para o seu carro em fila dupla. Respeite as leis de trânsito! Você perdeu sua vez e não joga esta rodada.");
			
			/*312 - */
			sadTexts.push("Você estacionou em cima da calçada. Calçada é para pedestres, sabia? Você perdeu sua vez e não joga esta rodada.");
			
			/*313 - */
			sadTexts.push("Você sai com o seu cachorro sem focinheira e sem coleira. Além disso, ainda deixa o cocô dele na rua! Você perdeu sua vez e não joga esta rodada.");
			
			/*314 - */
			sadTexts.push("Você deixa seu cachorro usar a calçada como banheiro e não apanha a sujeira. Que nojo! Você perdeu sua vez e não joga esta rodada.");
			
			/*315 - */
			sadTexts.push("Você trata os mais humildes de maneira grosseira. Aprenda a ser gentil com todos. Você perdeu sua vez e não joga esta rodada.");
			
			/*316 - */
			sadTexts.push("Você não participa das reuniões do seu condomínio e depois reclama das decisões que foram tomadas. Você perdeu sua vez e não joga esta rodada.");
			
			/*317 - */
			sadTexts.push("Há várias semanas não chove na sua cidade e isso afetou o abastecimento de água. Você perdeu sua vez e não joga esta rodada.");
			
			/*318 - */
			sadTexts.push("Você perdeu uma hora na fila do banco e não reclamou do descumprimento da lei. Você perdeu sua vez e não joga esta rodada.");
			
			/*319 - */
			sadTexts.push("Você só dirige ouvindo música no volume mais alto. Cuidado para não causar um acidente! Você perdeu sua vez e não joga esta rodada.");
			
			/*320 - */
			sadTexts.push("Você espalhou para todo mundo um segredo que seu amigo lhe contou. Fofoqueiro! Você perdeu sua vez e não joga esta rodada.");
			
			/*321 - */
			sadTexts.push("O seu quarto é uma bagunça e você nunca o arruma. Aprenda a ser organizado. Você perdeu sua vez e não joga esta rodada.");
			
			/*322 - */
			sadTexts.push("Você esqueceu o aniversário do seu pai. Tomara que ele também esqueça o seu! Você perdeu sua vez e não joga esta rodada.");
			
			/*323 - */
			sadTexts.push("Você deu uma festa, deixou que seus amigos fizessem a maior bagunça e ainda brigou com o vizinho que reclamou. Mal-educado! Você perdeu sua vez e não joga esta rodada.");
			
			/*324 - */
			sadTexts.push("Você estacionou seu carro na porta de uma garagem enquanto ia à padaria. Respeite as leis do trânsito! Você perdeu sua vez e não joga esta rodada.");
			
			/*325 - */
			sadTexts.push("Você foi caminhar na praia, mas tinha tanto lixo que não dava para ficar lá. Você perdeu sua vez e não joga esta rodada.");
			
			/*326 - */
			sadTexts.push("Você não estuda, mas está usando uma carteira de estudante falsa para pagar meia-entrada em teatros e cinemas. Você perdeu sua vez e não joga esta rodada.");
			
			/*327 - */
			sadTexts.push("Você adora passar trotes no Corpo de Bombeiros. Isso não é nada divertido, sabia? Você perdeu sua vez e não joga esta rodada..");
			
			/*328 - */
			sadTexts.push("Um motorista “fechou” o seu carro no trânsito. Com raiva, você o perseguiu e esbarrou no carro dele de propósito! Você perdeu sua vez e não joga esta rodada.");
			
			/*329 - */
			sadTexts.push("Você jamais pede “por favor” e diz “obrigado” ao garçom que o atende. Aprenda a ser educado. Você perdeu sua vez e não joga esta rodada.");
			
			/*330 - */
			sadTexts.push("Você adora “fechar” os outros motoristas só para vê-los irritados. Seja civilizado e entenda que o trânsito não é lugar para brincadeiras. Você perdeu sua vez e não joga esta rodada.");
			
			/*331 - */
			sadTexts.push("Você usou termos racistas para falar sobre alguém. Racismo é crime! Você perdeu sua vez e não joga esta rodada.");
			
			/*332 - */
			sadTexts.push("Você fala ao celular enquanto dirige. Pare com esse hábito, você pode provocar um acidente. Você perdeu sua vez e não joga esta rodada.");
			
			/*333 - */
			sadTexts.push("Você foi buscar um amigo na casa dele e ficou buzinando na porta do prédio. Você perdeu sua vez e não joga esta rodada.");
			
			/*334 - */
			sadTexts.push("Você leva seu cachorro para passear e o deixa fazer suas necessidades no parquinho onde as crianças brincam. Aprenda a ser consciente! Você perdeu sua vez e não joga esta rodada.");
			
			/*335 - */
			sadTexts.push("Você joga frescobol na praia lotada e não tem medo de machucar os outros. Pense um pouco nos seus semelhantes! Você perdeu sua vez e não joga esta rodada.");
			
			/*336 - */
			sadTexts.push("Você comeu pipoca no cinema e deixou o pacote no chão. Porcalhão! Lugar de lixo é na lixeira! Você perdeu sua vez e não joga esta rodada.");
			
			/*337 - */
			sadTexts.push("Você está passando perto de um hospital e buzina com toda a força. Você perdeu sua vez e não joga esta rodada.");
			
			/*338 - */
			sadTexts.push("O estacionamento do shopping está lotado e você para em local irregular, bloqueando a saída de outros carros. Você perdeu sua vez e não joga esta rodada.");
			
			
			
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getHappyText(index:int):String
		{
			//	return "Parabéns!\n\nVocê acaba de receber os pontos\nde uma pergunta como bônus!"
			var val:int = 0 ;
			
			try
			{
				if (!isNaN(index))
					val = index % happyTexts.length;
				else
					val = int(Math.random() * happyTexts.length);
			}
			catch (e:Error)
			{
				val = 0;
			}
			
			return happyTexts[ val ];
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getSadText(index:int):String
		{
			//	return "Que azar!\n\nVocê acaba de ser penalizado e\nperdeu uma pergunta.";
			var val:int = 0 ;
			
			try
			{
				if (!isNaN(index))
					val =  (-index % sadTexts.length);
				else
					val = int(Math.random() * sadTexts.length);
			}
			catch (e:Error)
			{
				val = 0;
			}
			
			return sadTexts[ val  ];
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setHappyIcon
		 * Define que a unica carta visível deve ser carta de bonus
		 */
		public function setHappyIcon(local:Boolean,name:String,f:Function, cardNum:int):void
		{
			if (gift==null) return;
			if (sad==null) return;
			
			gift.visible=true;
			sad.visible=false;
			
			setMultiline(false,20);
			if (local)
			{
				///*showModalMessage*/showMessage("Bônus","Parabéns!\n\nVocê acaba de receber os pontos\nde uma pergunta como bônus!"/*,f*/);
				showTimedMessage("Você tirou uma carta de bônus",getHappyText(cardNum),f);
			}
			else
			{
				//				showMessage(name+" tirou uma carta de bônus",name+" acaba de receber os pontos\nde uma pergunta como bônus!");
				showTimedMessage(name+" tirou uma carta de bônus",getHappyText(cardNum),f);				
			}
			
			setMultiline(true,20);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setSadIcon
		 * Define que a unica carta visível deve ser carta de penalidade
		 */
		public function setSadIcon(local:Boolean,name:String,f:Function, cardNum:int):void
		{
			if (gift==null) return;
			if (sad==null) return;
			
			gift.visible=false;
			sad.visible=true;
			setMultiline(false,20);
			if (local)
			{
				//	/*showModalMessage*/showMessage("Penalidade","Que azar!\n\nVocê acaba de ser penalizado e\nperdeu uma pergunta."/*,f*/);
				showTimedMessage("Você tirou uma carta de penalidade",getSadText(cardNum),f);
			}	
			else
			{
				//			showMessage(name + " tirou uma carta de penalidade", name+" foi penalizado e errou a pergunta.");
				showTimedMessage(name + " tirou uma carta de penalidade",getSadText(cardNum),f);				
			}
			setMultiline(true,20);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * myOk
		 * Meu comportamento personalizado de quando aperto o botão de OK
		 * (toca som e fecha)
		 */
		private function myOk():void
		{
			this.callButtonSound();
			if (super.getMyDismissCallback()!=null)
				(super.getMyDismissCallback())();
			setInvisible();
		}
		///--------------------------------------------------------------------------------------------------------------------	
		/**
		 * initWindow
		 * cria os icones e os posiciona
		 */
		override public function initWindow():void
		{
			super.initWindow();
			
			sad=new Penalty();
			gift=new Bonus();
			
			addChild(gift);			
			addChild(sad);
			gift.visible=false;
			sad.visible=false;
			
			sad.y =90;		
			//sad.y+=sad.height/2;
			sad.x=width-sad.width	-	100;		
			
			gift.y =90;
			//gift.y+=gift.height/2;
			gift.x=width-gift.width -	100;
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}