package   
{
	import GUILib.GUIConstants;

	/**
	 * @author Daniel Monteiro
	 * @brief Constantes de jogo
	 * */
	public class FuturaConstants
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * numero de peças no tabuleiro
		 */
		static public const NUMPIECES:int=24;
		/**
		 * numero de cores disponíveis 
		 */
		static public const NUMCOLORS:int=6;
		/**
		 * numero de turnos mínimos
		 */
//		static public const NUMTURNS:int=NUMPIECES-1;
		/**
		 * mínimo de jogadores
		 */
		static public const MINPLAYERS:int=2;
		/**
		 * máximo de jogadores
		 */
		static public const MAXPLAYERS:int=6;
		/**
		 * cor: verde
		 */
		static public const COLORGREENINDEX:int=1;
		/**
		 * cor: amarelo
		 */
		static public const COLORYELLOWINDEX:int=2;
		/**
		 * cor: azul
		 */
		static public const COLORBLUEINDEX:int=3;
		/**
		 * cor:laranja 
		 */
		static public const COLORORANGEINDEX:int=4;
		/**
		 * cor: roxo
		 */
		static public const COLORPURPLEINDEX:int=5;
		/**
		 * cor:vermelho
		 */
		static public const COLORREDINDEX:int=6;
		/**
		 * constante hexa para cor branca
		 */
		static public const COLORWHITEVALUE:uint=0xFFFFFF;
		/**
		 * constante indicando o comprimento em pixels de uma resposta
		 */
		static public const ANSWER_BOX_WIDTH:int=220;
		/**
		 * constante indicando o comprimento em pixels de uma resposta
		 */
		static public const QUESTION_BOX_WIDTH:int=270;		
		/**
		 * o usuário não deu resposta válida ou não respondeu a tempo = 5
		 */
		static public const NOANSWER:int=4;
		/**
		 * tempo para jogo normal
		 * (61 para permitir ao jogador ver o ultimo segundo no relógio)
		 */
		static public const TIMENORMAL:int=61;
		/**
		 * tempo para jogo turbo
		 */
		static public const TIMETURBO:int=30;
		/**
		 * numero de frames no relógio
		 */
		static public const CLOCKFRAMES:int=60;
		/**
		 * quantos pontos por acerto o jogador ganha 
		 */
//		static public const POINTSPERRIGHTANSWER:int=10;
		/**
		 * indicação de que a pergunta foi uma carta bonus 
		 */
		static public const BONUSCARDID:int=-4;
		/**
		 * indicação de que a pergunta foi uma carta penalidade
		 */
		static public const PENALTYCARDID:int=-5;
		/**
		 * pergunta normal 
		 */
		static public const NORMALQUESTION:int=0;
		/**
		 * id de jogador não inicializado 
		 */
		static public const NONINITPLAYERID:int=-1;
		/**
		 * Quantidade de linhas de texto possíveis numa pergunta 
		 */
		static public const NUMLINESQUESTION:int=4;
		/**
		 * Quantidade de linhas de texto possíveis numa resposta
		 */
		static public const NUMLINESANSWER:int=4;
		/**
		 * tempo limite (duração do som de "tempo acabando")
		 * */
		static public const HURRY_UP_TIME:int=15;
		static public const FINAL_ANIMATION_TIME:int = 7;
		
		static public const ISDEBUG:Boolean = false;
		static public const ISLOGGING:Boolean = false;
		static public const POLLING_INTERVAL :int = 2 * GUIConstants.SECONDS;		
		static public const TIMER_MAX_TOLERANCE : int = 2;
		static public const SERVER_BASE_URL :String = "http://staging.futura.nanogames.com.br";		
		//static public const SERVER_BASE_URL:String = "http://futura.nanogames.com.br";
		//static public const SERVER_BASE_URL:String = "http://187.84.228.69";
		//static public const SERVER_BASE_URL:String = "http://www.cdf.org.br";		
		///--------------------------------------------------------------------------------------------------------------------
	}
}