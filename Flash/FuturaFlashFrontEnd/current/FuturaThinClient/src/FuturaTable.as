///UI_showAnswer pode resolver o problema se for chamado no pollServer
package  
{
	//===================================================================================================================================	
	///flash
	import FuturaExternalInterface.FuturaBaseInterface;
	import FuturaExternalInterface.FuturaInterface;
	import FuturaExternalInterface.FuturaMockInterface;
	
	import FuturaGUI.FuturaMessageWindow;
	import FuturaGUI.FuturaOutcomeWindow;
	import FuturaGUI.FuturaQuestionWindow;
	import FuturaGUI.FuturaScoreBox;
	import FuturaGUI.FuturaScoreWindow;
	import FuturaGUI.FuturaSpecialTurnWindow;
	import FuturaGUI.MovieClipWrapper;
	
	import FuturaGameEngine.FuturaPlayer;
	
	import GUILib.FadableSprite;
	import GUILib.FadableText;
	import GUILib.GUIConstants;
	
	import carta_bonus.mp3;
	
	import carta_penalti.mp3;
	
	import checkbox.mp3;
	
	import enter_room.wav;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.geom.ColorTransform;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.utils.Timer;
	
	import org.osmf.composition.SerialElement;
	
	import player_turn.wav;
	
	import resposta_correta.mp3;
	
	import resposta_errada.mp3;
	
	/**
	 * FuturaTable
	 * @author Daniel Monteiro
	 * @comment Antiga FuturaTableEx
	 * Representa o tabuleiro de jogo. Deve ser capaz de obter estados de jogo, perguntas e fazer o turno acontecer.
	 */
	public class FuturaTable extends Sprite
	{
		//===================================================================================================================================		
		/**
		 * Guarda qual é o estado de jogo atual 
		 */
		private var gameState:int;
		/**
		 * Qual é o jogador atualmente jogando. É apenas um atalho pra não ter que interpretar a string toda vez. 
		 */
		private var currentPlayer:int;
		/**
		 * Qual é o indice do jogador local no vetor de jogadores 
		 */
		private var localPlayerInternalId:int;
		/**
		 * Dados de todos os jogadores. Inclusive os que ja saíram 
		 */
		private var players:Array;
		/**
		 * Estado do jogo para o próximo turno. 
		 */
		private var nextQuestion:Object;
		/**
		 * o jogo esta em andamento?
		 */
		private var gameStopped:Boolean;			
		/**
		 * posições das peças no tabuleiro (uma por frame, por jogada acertada)
		 */
		private var piecesPosition:PiecesPosition;
		/**
		 * posições das setas no tabuleiro (uma por frame, por jogada acertada)
		 */
		private var arrowPosition:ArrowPosition;
		/**
		 * tabuleiro em sí
		 */
		private var board:Board;
		/**
		 * cartinhas empilhadas (enfeite)
		 */
		private var cards:Cards;
		/**
		 * fundo da mesa
		 */
		private var background:Background;
		/**
		 * Caixa de scores sempre visível na tela durante a partida 
		 */
		private var scoreBox:FuturaScoreBox;
		/**
		 * janela de placar
		 */
		private var scoreWindow:FuturaScoreWindow;
		/**
		 * janela de perguntas
		 */
		private var questionWindow:FuturaQuestionWindow;
		/**
		 * janela de mensagens genéricas
		 */
		private var msgWindow:FuturaMessageWindow;
		/**
		 * janela de cartas de bônus e penalidade
		 */
		private var specialWindow:FuturaSpecialTurnWindow;
		/**
		 * som  de jogada correta
		 */
		private var correctAnswer:resposta_correta.mp3;
		/**
		 * som de jogada errada
		 */
		private var wrongAnswer:resposta_errada.mp3;
		/**
		 * interface de comunicação externa
		 */
		private var futuraInterface:FuturaBaseInterface;	
		/**
		 * Turno atual. (TODO: usar para manter o controle de coerência de pacotes recebidos. Senão, retirar) 
		 */
		private var currentTurn:int;
		/**
		 * Timer para quando deve começar o novo turno
		 */
		private var timerToNextTurn:Timer;
		/**
		 * Timer para colocar uma pecinha, caso o jogador tenha acertado 
		 */
		private var timerSetPiece:Timer;
		/**
		 *  Timer para marcar o tempo entre ver a resposta do jogador e colocar a pecinha 
		 */
		private var timerOutcome:Timer;
		/**
		 * Timer para mostrar o resultado marcado pelo jogador 
		 */
		private var answerTimer:Timer;
		/**
		 * Timer marcar o tempo entre colocar a pecinha e atualizar a seta 
		 */
		private var setArrowTimer:Timer;
		/**
		 * Qual é o tipo de pergunta?
		 */
		private var questionType:int;
		/**
		 * A ultima resposta foi correta? 
		 */
		private var lastOutcome:Boolean;
		/**
		 * O que foi respondido da ultima vez? 
		 */
		private var lastAnswer:String;
		/**
		 * Janela mostrando o resultado da jogada
		 */
		private var debugMsgs:TextField;
		//	private var outcomeWindow:FuturaOutcomeWindow;
		private var pollingTimer:Timer;
		private var currentServerTurn:String;
		private var gameTableId:String;
		private var myLocalId:String;
		private var startCycleSound:player_turn.wav
		private var startGameSound:enter_room.wav;
		private var endGameSound:checkbox.mp3;
		private var dummyPiecesPosition:PiecesPosition;
		private var tableSprite:FadableSprite;

		//===================================================================================================================================		
		/**
		 * Construtor 
		 */
		public function FuturaTable()
		{
			currentServerTurn = "-1";
			gameTableId = "-1";
			///algum estado o sistema precisa ter ao iniciar
			gameState=FuturaGameStates.SYSTEM_ALPHA;
			GAME_enteringState(FuturaGameStates.HANDSHAKE_PREINIT);
			
			///incializações triviais
			currentPlayer=FuturaConstants.NONINITPLAYERID;
			localPlayerInternalId=FuturaConstants.NONINITPLAYERID;
			
			///inicializações de objetos
			players=new Array();
			nextQuestion=null;
			correctAnswer= new resposta_correta.mp3();
			wrongAnswer = new resposta_errada.mp3();
			startCycleSound = new player_turn.wav();
			startGameSound = new enter_room.wav();
			endGameSound = new checkbox.mp3();
			
			///aloca elementos da UI
			background=new Background();
			board=new Board();
			piecesPosition=new PiecesPosition();
			dummyPiecesPosition=new PiecesPosition();
			tableSprite = new FadableSprite();
			arrowPosition=new ArrowPosition();
			UI_initGameTable();
			cards= new Cards();
			
			tableSprite.addChild(dummyPiecesPosition);
			
			timerToNextTurn=new Timer(0.8*GUIConstants.SECOND);
			timerSetPiece=new Timer(0.4*GUIConstants.SECOND);
			timerOutcome=new Timer(0.4*GUIConstants.SECOND);
			answerTimer=new Timer(0.8*GUIConstants.SECOND);
			setArrowTimer=new Timer(0.5*GUIConstants.SECOND);
			
			pollingTimer = new Timer(FuturaConstants.POLLING_INTERVAL);
			
			setArrowTimer.addEventListener(TimerEvent.TIMER, UI_setArrowTicker);
			answerTimer.addEventListener(TimerEvent.TIMER, UI_showAnswerTicker);
			timerOutcome.addEventListener(TimerEvent.TIMER, UI_tickOutcome);
			timerToNextTurn.addEventListener(TimerEvent.TIMER, GAME_tickNextTurn);
			timerSetPiece.addEventListener(TimerEvent.TIMER, UI_tickSetPiece);
			
			pollingTimer.addEventListener(TimerEvent.TIMER,GAME_pollServer);
			
			///janelas
			scoreWindow=new FuturaScoreWindow();			
			questionWindow=new FuturaQuestionWindow();								
			msgWindow=new FuturaMessageWindow();
			specialWindow=new FuturaSpecialTurnWindow();
			scoreBox=new FuturaScoreBox();			
			//	outcomeWindow=new FuturaOutcomeWindow();
			lastAnswer = "-";
			questionWindow.setInvisible();
			players=new Array();
			//futuraInterface = new FuturaMockInterface
			futuraInterface = new FuturaInterface
				(
					CALLBACK_takeUserListFromServer, ///obter lista de jogadores 
					CALLBACK_setMyId,  ///quem sou eu na lista
					CALLBACK_newQuestion, ///recebe estado de jogo
					GAME_end ///fim de jogo
				);
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function GAME_pollServer(e:TimerEvent):void
		{
			DEBUG_warnDeveloper("consultei o servidor...");
			try
			{
				var id:String;
				
				if (GAME_getLocalPlayer() == null || GAME_getLocalPlayer().getFuturaId() == null)
					id = "-1";
				else
				{
					id = GAME_getLocalPlayer().getFuturaId();
				}
				
				if (futuraInterface != null)
					futuraInterface.notifyTick(currentServerTurn, gameTableId, CALLBACK_serverPoll, id);
			}
			catch (e:Error)
			{
				DEBUG_warnDeveloper("falha na consulta");
			}
		}
		
		
		private function CALLBACK_serverPoll(e:/*Event*/Object):void
		{
			
			DEBUG_warnDeveloper("...e recebi resposta!");	
			var obj:Object;
			
			if (e != null && e.target != null && e.target.data != null)
			{
				DEBUG_warnDeveloper(e.target.data);
				try 
				{
					obj = FuturaBaseInterface.parseJSON(e.target.data);
				}
				catch (e:Error)
				{
					DEBUG_warnDeveloper("o JSON não esta vindo legal...","JSONLITE (CALLBACK_serverPoll)");
					return;
				}
			}
			else
				return;
			
			
			if (obj != null)
			{
				if (gameTableId == "-1" && obj.game_id != null && obj.game_id != "-1")
				{
					gameTableId = obj.game_id;

					var url:String = futuraInterface.call("getBackgroundURL") as String;
					
					if (url != null)
					{
						
						var loader:Loader;
						var req:URLRequest;
						req=new URLRequest(url);
						
						if (req == null)
							return;
						loader=new Loader();
						
						if (loader == null)
							return;
						
						loader.load(req);						
						addChildAt(loader,1);
						removeChildAt(0);
					}
				}
			}
			
			
			
			
			if (obj != null && obj.end_game != null)
			{
				var correct:Boolean = true ;
				var answer:String = "-";
				
				if (obj.end_game is String)
				{
					if (obj.end_game == "true")
					{
						
						
						
						if (obj.answer_response != null && obj.answer_response.correct != null)
							correct = obj.answer_response.correct;
						
						if (obj.answer_response != null && obj.answer_response.answer != null)
							answer = obj.answer_response.answer;
						
						pollingTimer.stop();
						nextQuestion = obj;
						
						if (nextQuestion.players != null)
							GAME_updatePlayerList(nextQuestion.players);						
						
						UI_showAnswer(correct, answer);
						return;
					}
				}
				else
					if (obj.end_game is Boolean)
					{
						if (obj.end_game)
						{
							
							if (obj.answer_response != null && obj.answer_response.correct != null)
								correct = obj.answer_response.correct;
							
							if (obj.answer_response != null && obj.answer_response.answer != null)
								answer = obj.answer_response.answer;
							
							pollingTimer.stop();
							nextQuestion = obj;
							
							if (nextQuestion.players != null)
								GAME_updatePlayerList(nextQuestion.players);	
							
							UI_showAnswer(correct, answer);
							return;
						}
					}
			}			
			
			
			if (obj != null && obj.new_question != null)
			{
				DEBUG_warnDeveloper(obj.toString());
				
				if (currentServerTurn != obj.turn_id || (currentServerTurn == "-1" && obj.turn_id && obj.turn_id != "-1"))				
					CALLBACK_newQuestion(obj);
				
				if (obj.time_elapsed != null &&   gameState == FuturaGameStates.MIDTURN_SHOWMYNEWQUESTION || gameState == FuturaGameStates.MIDTURN_SHOWOTHERNEWQUESTION)
					questionWindow.setServerTime(obj.time_elapsed);
			}
			else
				DEBUG_warnDeveloper("...resposta nula");
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * SYS_init 
		 * inicialização básica do sistema
		 */
		public function SYS_init():void
		{
			GAME_enteringState(FuturaGameStates.HANDSHAKE_INIT);
			UI_init();
			GAME_init();
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_start 
		 * começo da espera dos dados de jogo vindos do servidor
		 */
		public function GAME_start():void
		{
			//		GAME_enteringState(FuturaGameStates.HANDSHAKE_START);
			msgWindow.showMessage("Preparando a mesa...", "A diversão já vai começar!");			
			gameStopped=false;
			pollingTimer.start();
			GAME_pollServer(null);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * UI_initGameTable
		 * Atualiza o visual do jogo para o de uma partida limpa 
		 */
		private function UI_initGameTable():void	
		{
			///limpa todas as pecinhas
			for (var c:int=0;c<piecesPosition.numChildren;c++)
				if (piecesPosition.getChildAt(c) is Piece)
				{
					(piecesPosition.getChildAt(c) as Piece).visible=false;
				}
			
			///limpa todas as setas
			for (var d:int=0;d<arrowPosition.numChildren;d++)
				if (arrowPosition.getChildAt(d) is Arrow)
				{
					(arrowPosition.getChildAt(d) as Arrow).visible=false;
				}
			
			for (var e:int = 0; e < FuturaConstants.NUMPIECES ; e++ )
			{
				dummyPiecesPosition.getChildAt(e).visible=false;
			}
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * UI_setTableState
		 * @param state estado do tabuleiro
		 * Atualiza estado do tabuleiro, ignorando quais peças estavam nele antes.
		 */
		private function UI_setTableState(state:Array):void
		{
			///limpa as peças
			for (var c:int=0;c<FuturaConstants.NUMPIECES;c++)
			{
				piecesPosition.getChildAt(c).visible=false;
			}
			
			///atualiza o estado das peças
			for (var d:int=0;d<state.length;d++)
			{
				piecesPosition.getChildAt(d).visible=true;
				this.UI_setPiece(d,parseInt(state[d]));
			}
			
			///qual deve ser a cor da seta?
			var index:int;
			
			if (nextQuestion != null && nextQuestion.current_player_id != null)
			{
				var localId:int = GAME_getLocalIdFromFuturaId(nextQuestion.current_player_id);
				var player:FuturaPlayer = GAME_getPlayer(localId);
				
				if (player == null)
				{
					DEBUG_warnDeveloper("player é null","UI_setTableState");
					return;
				}
				
				index = player.getColorIndex();
			}
			else
				index = 0 ;
			
			///coloca a seta
			if (UI_getArrowPosition() < FuturaConstants.NUMPIECES )
				UI_placeArrow(UI_getArrowPosition(),index);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * UI_init
		 * inicializa interface gráfica
		 */
		private function UI_init():void
		{
			var url:String=null;
			
			
			addChild(background);
			background.x=background.width/2;
			background.y=background.height/2;
			
			/// tabuleiro /////////////////////////////////////////////////////////////////
			addChild(board);			
			board.x=150+ board.width/2;
			board.y=15+board.height/2;
			
			/// peças /////////////////////////////////////////////////////////////////////			
			board.addChild(piecesPosition);		
			board.addChild(tableSprite);
			/// seta /////////////////////////////////////////////////////////////////////
			board.addChild(arrowPosition);
			arrowPosition.x=0;
			arrowPosition.y=0;			
			
			/// cartas /////////////////////////////////////////////////////////////////////
			addChild(cards);
			cards.x=cards.width/2;
			cards.y=height-(cards.height)+15;
			/// box de pontuação/////////////////////////////////////////////////////////////////////
			//addChild(scoreBox);
			scoreBox.x=7;
			scoreBox.y=7;
			
			///janelas
			/// janela de placar geral //////////////////////////////////////////////////////
			addChild(scoreWindow);
			scoreWindow.initWindow();
			scoreWindow.visible=false;
			scoreWindow.setOkCallback(GAME_leave);
			scoreWindow.setReplayCallback(GAME_replay);			
			/// janela de perguntas //////////////////////////////////////////////////////			
			addChild(questionWindow);
			questionWindow.initWindow();			
			questionWindow.visible=false;			
			questionWindow.setAnsweredCallback(GAME_sendAnswer);
			
			/// janela genérica de mensagens //////////////////////////////////////////////////////	
			addChild(msgWindow);
			msgWindow.initWindow();
			msgWindow.visible=false;
			
			/// janela de jogada especial //////////////////////////////////////////////////////	
			addChild(specialWindow);
			specialWindow.initWindow();
			specialWindow.visible=false;
			
			///pega fundo da mesa
			UI_initGameTable();	
			
			if (FuturaConstants.ISLOGGING)
			{
								debugMsgs = new TextField();
								addChild(debugMsgs);
								debugMsgs.wordWrap=true;
								debugMsgs.multiline=true;		
								debugMsgs.autoSize = TextFieldAutoSize.NONE;
								debugMsgs.textColor = 0xFF0000;
								debugMsgs.x=width- debugMsgs.width;
								debugMsgs.y=0;			
								debugMsgs.height=height;
								debugMsgs.backgroundColor=0xFFFFFF;
								debugMsgs.background=true;
								debugMsgs.alpha = 0.8;
								debugMsgs.addEventListener(MouseEvent.CLICK , dummyClick);
			}
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function dummyClick(e:Event):void
		{
			if (debugMsgs.alpha > 0.6)
				debugMsgs.alpha = 0.2;
			else
				debugMsgs.alpha = 0.8;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_init
		 * inicializa entidades de jogo
		 */
		private function GAME_init():void
		{
			///criando as conexões com o servidor
			futuraInterface.init();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_takePlayerList
		 * @param playersFromServer Lista de jogadores vinda do servidor
		 * 
		 */
		private function GAME_takePlayerList(playersFromServer:Array):void
		{
			//DEBUG_warnDeveloper("lista inicial vinda do servidor");
			GAME_enteringState(FuturaGameStates.HANDSHAKE_PLAYERLIST);
			
			var player:FuturaPlayer;
			
			for each (var user:Object in playersFromServer)
			{
				//DEBUG_warnDeveloper(">"+user.nickname+">"+user.id);
				CALLBACK_addUser(user);
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_setMyId
		 * Define quem eu sou na lista de jogadores 
		 * @param myId FuturaId do jogador local
		 */
		private function GAME_setMyId(myId:int):void
		{
			GAME_enteringState(FuturaGameStates.HANDSHAKE_MYID);
			localPlayerInternalId=myId;
			GAME_enteringState(FuturaGameStates.STARTTURN_READYFORNEWTURN);
			futuraInterface.call("gameReady");
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * UI_getArrowPosition
		 * @return Qual é a posição da seta no tabuleiro 
		 * 
		 */
		private function UI_getArrowPosition():int
		{
			return nextQuestion.table_state.length;
		}
		///--------------------------------------------------------------------------------------------------------------------	
		private function GAME_readyForNewTurn():void
		{
			///se o jogo ja terminou, não tem porque vir pra cá.
			if (gameStopped)
			{
				DEBUG_warnDeveloper("o jogo ja acabou, mas eu recebi uma pergunta @GAME_readyForNewTurn");
				return;
			}
			
			///trata de fim de jogo
			if (nextQuestion != null && nextQuestion.end_game != null)
			{
				if (nextQuestion.end_game is String)
				{
					if (nextQuestion.end_game == "true")
					{
						GAME_end();
						return;
					}
				}
				else
					if (nextQuestion.end_game is Boolean)
					{
						if (nextQuestion.end_game)
						{
							GAME_end();
							return;
						}
					}
			}
			
			///se não é fim de jogo, novo turno é iniciado
			GAME_enteringState(FuturaGameStates.STARTTURN_READYFORNEWTURN);
			
			currentPlayer = GAME_getLocalIdFromFuturaId(nextQuestion.current_player_id);
			//			//DEBUG_warnDeveloper("nextQuestion.current_player_id:"+nextQuestion.current_player_id);
			//			//DEBUG_warnDeveloper("currentPlayer:"+currentPlayer);
			
			///...e mostre a pergunta atual, Lombardi!
			UI_showQuestion();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * UI_showQuestion
		 * Exibe na tela a pergunta atual
		 */
		private function UI_showQuestion():void
		{
			DEBUG_warnDeveloper("vou mostrar a pergunta");
			
			////se o jogo terminou ou não temos pergunta, chora!
			if (gameStopped || nextQuestion == null || GAME_getCurrentPlayer() == null || nextQuestion.new_question.title == null || nextQuestion.time_elapsed == null)
			{
				//drastico??
				DEBUG_fatalError("tentativa de mostrar nova pergunta quando não há uma ou quando o jogo ja terminou","UI_showQuestion");
				return;
			}
			
			msgWindow.setInvisible();
			
			if (nextQuestion.game_speed != null && nextQuestion.game_speed == "turbo")
				questionWindow.setGameSpeed(FuturaConstants.TIMETURBO);
			else
				questionWindow.setGameSpeed(FuturaConstants.TIMENORMAL);
			
			
			///em qual estado estamos?
			if (GAME_localPlayerIsCurrentPlayer())
				GAME_enteringState(FuturaGameStates.MIDTURN_SHOWMYNEWQUESTION);
			else
				GAME_enteringState(FuturaGameStates.MIDTURN_SHOWOTHERNEWQUESTION);
			
			///pra testar cartas bonus e cartas de penalidade. pro jogo seguir normalmente, deixar desabilitado
			//nextQuestion.new_question.kind = -1;
			questionType = FuturaConstants.NORMALQUESTION;
			
			///se for carta bonus...
			if (/*GAME_localPlayerIsCurrentPlayer() &&*/ nextQuestion.new_question.kind!=null && nextQuestion.new_question.kind!=0)
			{
				questionWindow.setInvisible();
				
				if (nextQuestion.new_question.kind > 0)
				{
					specialWindow.setHappyIcon(GAME_localPlayerIsCurrentPlayer(),GAME_getCurrentPlayer().getName(),GAME_sendAnswer, nextQuestion.new_question.kind);
					questionType=FuturaConstants.BONUSCARDID;
				}
				else
				{
					specialWindow.setSadIcon(GAME_localPlayerIsCurrentPlayer(),GAME_getCurrentPlayer().getName(),GAME_sendAnswer, nextQuestion.new_question.kind);
					questionType=FuturaConstants.PENALTYCARDID;
				}
				return;
			}
			
			///sendo uma pergunta normal, vamos preparar sua exibição
			var array:Array;
			
			///preenche os campos de passagem de pergunta para a janela de perguntas.
			///um campo não preenchido aqui não significa necessariamente um erro.
			array=new Array();
			
			///1º opção
			if (nextQuestion.new_question.options.a!=null && (nextQuestion.new_question.options.a as String).length>0)
				array.push(nextQuestion.new_question.options.a);
			
			///2º opção
			if (nextQuestion.new_question.options.b!=null && (nextQuestion.new_question.options.b as String).length>0)			
				array.push(nextQuestion.new_question.options.b);
			
			///3º opção	
			if (nextQuestion.new_question.options.c!=null && (nextQuestion.new_question.options.c as String).length>0)			
				array.push(nextQuestion.new_question.options.c);
			
			///4º opção			
			//			if (nextQuestion.new_question.options.d!=null && (nextQuestion.new_question.options.d as String).length>0)			
			//				array.push(nextQuestion.new_question.options.d);
			

			//			//DEBUG_warnDeveloper ("nextQuestion:"+nextQuestion);
			//			//DEBUG_warnDeveloper ("nextQuestion.new_question:"+nextQuestion.new_question);
			//			//DEBUG_warnDeveloper ("nextQuestion.new_question.title:"+nextQuestion.new_question.title);
			//			//DEBUG_warnDeveloper ("array:"+array);
			//			//DEBUG_warnDeveloper ("localPlayerIsCurrentPlayer():"+localPlayerIsCurrentPlayer());
			//			//DEBUG_warnDeveloper ("getCurrentPlayer():"+getCurrentPlayer());
			//			//DEBUG_warnDeveloper ("getCurrentPlayer().getName():"+getCurrentPlayer().getName());
			if (startCycleSound != null && GAME_localPlayerIsCurrentPlayer())
				startCycleSound.play();
			else
				if (parseInt(nextQuestion.current_turn) == 1 && startGameSound != null)
						startGameSound.play();


			questionWindow.newQuestion(nextQuestion.new_question.title,array,20, GAME_localPlayerIsCurrentPlayer(), GAME_getCurrentPlayer().getName(), nextQuestion.time_elapsed);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_getCurrentPlayer
		 * @return O jogador atualmente jogando 
		 * 
		 */
		private function GAME_getCurrentPlayer():FuturaPlayer
		{
			//DEBUG_warnDeveloper("currentPlayer:"+currentPlayer);
			return GAME_getPlayer(currentPlayer);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_getLocalPlayer
		 * @return O jogador atual 
		 * @comment Vale lembrar que jogador local != jogador atual em pelo menos 1/2 do tempo
		 */
		private function GAME_getLocalPlayer():FuturaPlayer
		{
			return GAME_getPlayer(localPlayerInternalId);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_getPlayer
		 * @param index ID local (indice do vetor)
		 * @return jogador do id local passado 
		 * 
		 */
		private function GAME_getPlayer(index:int):FuturaPlayer
		{
			return players[index] as FuturaPlayer;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * GAME_sendAnswer
		 * Envia a resposta selecionada pelo jogador ao servidor
		 */
		private function GAME_sendAnswer():void
		{
			GAME_enteringState(FuturaGameStates.MIDTURN_SENDANSWER);
			
			//	questionWindow.setInvisible();
			
			if (!GAME_localPlayerIsCurrentPlayer())
				return;
			
			if (GAME_getCurrentPlayer() == null)
			{
				DEBUG_warnDeveloper("GAME_getCurrentPlayer retornou nulo", "GAME_sendAnswer");
				return;
			}
			
			///variaveis usadas pela função
			var ans:Object;
			var answer:int;
			
			///verifica se é carta bonus ou penalidade
			if (questionType==FuturaConstants.BONUSCARDID)
			{
				GAME_getCurrentPlayer().playerDidHit();
				futuraInterface.sendAnswer("-",0);
				return;
			}
			
			if (questionType==FuturaConstants.PENALTYCARDID)
			{
				futuraInterface.sendAnswer("-",0);
				return;
			}
			
			///valida a reposta a ser enviada
			if (questionWindow!=null)
				answer=	questionWindow.getAnswer();
			else
				DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE,"GAME_sendAnswer");
			
			if (answer<0 || answer > 4)
				DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE,"GAME_sendAnswer");
			
			///envia a resposta no formato esperado pelo servidor. (também não gostei disso, mas...)
			ans={0: "a", 1:"b", 2:"c", 3:"d", 4:"-"};
			DEBUG_warnDeveloper ("tempo! "+questionWindow.getElapsedTime().toString());
			DEBUG_warnDeveloper ("enviando...");
			futuraInterface.sendAnswer(ans[answer],questionWindow.getElapsedTime() );			
			GAME_enteringState(FuturaGameStates.STARTTURN_READYFORNEWTURN);
//			pollingTimer.start();
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_waitForServerAnswer():void
		{
			if (gameState == FuturaGameStates.MIDTURN_SHOWOTHERNEWQUESTION)
				GAME_enteringState(FuturaGameStates.ENDTURN_WAITINGFOROTHERANSWER);
			else
				GAME_enteringState(FuturaGameStates.ENDTURN_WAITINGFORMYANSWER);
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function UI_showAnswer(currentIsCorrect:Boolean, currentAnswer:String):void
		{
			GAME_enteringState(FuturaGameStates.ENDTURN_SHOWINGANSWER);
			
			lastOutcome=currentIsCorrect;
			lastAnswer=currentAnswer;
			
			DEBUG_warnDeveloper ("--->"+currentAnswer);
			
			if ( questionType != FuturaConstants.BONUSCARDID && questionType != FuturaConstants.PENALTYCARDID && currentAnswer!=null && currentAnswer!="-" )
			{
				if (GAME_localPlayerIsCurrentPlayer())				
				{
					DEBUG_warnDeveloper ("Vou mostrar a resposta. Sou jogador local"+currentAnswer);
					questionWindow.setDisplayedAnswer(GAME_letterToIndex(lastAnswer), currentIsCorrect);
					questionWindow.setVisible();
				}
				else
				{
					DEBUG_warnDeveloper ("não vou mostrar a resposta. não sou jogador local"+currentAnswer);					
					questionWindow.stopTimer();
				}
			}
			
			if (lastOutcome)
			{
				if (correctAnswer != null)
					correctAnswer.play();
				
			}
			else
			{	
				if (wrongAnswer != null )
					wrongAnswer.play();
				
			}
			
			answerTimer.start();			
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function UI_showAnswerTicker(e:Event):void
		{
			answerTimer.reset();
			//			questionWindow.setInvisible();
			//			outcomeWindow.setInvisible();
			specialWindow.setInvisible();
			timerOutcome.start();
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function UI_tickOutcome(e:Event):void
		{
			timerOutcome.reset();
			if (currentTurn != 0)
			{
				if (lastOutcome)
					UI_playerAnsweredCorrectly();
				else
					UI_playerAnsweredWrongly();
			}
			setArrowTimer.start();			
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function UI_setArrowTicker(e:Event):void
		{
			setArrowTimer.reset();
			GAME_endTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * askForNewTurn
		 * pede o começo dum novo turno (deve acontecer ~2 segundos depois)
		 */
		private function GAME_askForNewTurn():void
		{
			timerToNextTurn.start();
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * tickNextTurn
		 * inicia o novo turno de fato
		 * @param e uso interno do timer
		 */
		private function GAME_tickNextTurn(e:TimerEvent):void
		{
			timerToNextTurn.reset();
			
			
			if (!gameStopped)
				GAME_startNextTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_startNextTurn():void
		{
			
			GAME_readyForNewTurn();
		}	
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * 
		 * 
		 */
		private function UI_updateScoreBox():void
		{
			///checagem de consistência padrão
			//checkGameConsistencyFor("updatePointsBox");
			
			///variáveis usadas na função
			var player:FuturaPlayer;
			
			///atualiza a caixa de score
			player=GAME_getPlayer(localPlayerInternalId);
			
			if (player == null)
			{
				DEBUG_warnDeveloper("player é nulo","UI_updateScoreBox");
				return;
			}
			
			scoreBox.setDisplayItems(player.getHits(),player.getErrors(),player.getScore());
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function GAME_endTurn():void
		{
			
			questionWindow.setInvisible();
			UI_updateScoreBox();
			timerSetPiece.start();
			currentTurn++;
			
		}
		
		///--------------------------------------------------------------------------------------------------------------------		
		private function UI_tickSetPiece(e:Event):void
		{
			timerSetPiece.reset();
			//DEBUG_warnDeveloper("tickSetPiece");
			
			
			if (nextQuestion.table_state != null)
			{
				for (var c:int = 0; c < FuturaConstants.NUMPIECES ; c++ )
				{
					dummyPiecesPosition.getChildAt(c).visible=false;
				}
				
				if (nextQuestion.table_state.length > 0 && (nextQuestion.table_state.length - 1) < dummyPiecesPosition.numChildren)
				{
					dummyPiecesPosition.getChildAt(nextQuestion.table_state.length - 1).visible=true;
					(dummyPiecesPosition.getChildAt(nextQuestion.table_state.length - 1) as MovieClip).gotoAndStop(nextQuestion.table_state[nextQuestion.table_state.length - 1]);
					tableSprite.startShowAnimation(UI_blingEnded);
				}
				else
					UI_setTableState(nextQuestion.table_state);
			}
			
			
			if (nextQuestion != null && nextQuestion.current_player_id != null )
				currentPlayer = GAME_getLocalIdFromFuturaId(nextQuestion.current_player_id);
			
			if (!gameStopped)
				GAME_askForNewTurn();
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function UI_blingEnded():void			
		{
			tableSprite.setInvisible();
			UI_setTableState(nextQuestion.table_state);
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function UI_hideAllWindows():void
		{
			scoreWindow.setInvisible();
			questionWindow.setInvisible();
			msgWindow.setInvisible();
			specialWindow.setInvisible();
			scoreBox.setInvisible();
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_end():void
		{
			DEBUG_warnDeveloper("fim de jogo","GAME_end");
			gameStopped=true;
			pollingTimer.stop();
			GAME_enteringState(FuturaGameStates.ENDGAME_SHOWINGRESULTS);	
			//DEBUG_warnDeveloper ("recebi endGame");
			UI_hideAllWindows();
			
			if (endGameSound != null)
				endGameSound.play();
			
			scoreWindow.updatePlayerList(players, /*GAME_whosWinning() == localPlayerInternalId*/ false);
			scoreWindow.startShowAnimation();			
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_localPlayerIsCurrentPlayer():Boolean
		{
			return currentPlayer==localPlayerInternalId;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_getLocalIdFromFuturaId(FuturaId:String):int
		{
			var player:FuturaPlayer;
			//DEBUG_warnDeveloper ("FuturaId:"+FuturaId);
			for (var c:int=0;c<players.length;++c)
			{
				player=players[c] as FuturaPlayer;
				//DEBUG_warnDeveloper("player.getFuturaId():"+player.getFuturaId());	
				if (player.getFuturaId() == FuturaId)
				{
					//DEBUG_warnDeveloper("c:"+c);
					return c;
				}
			}
			return FuturaConstants.NONINITPLAYERID;;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_newQuestion(nextQuestionFromServer:Object):void
		{
			var previousOutcomeCandidate:Boolean;
			var previousServerTurn:String;
			DEBUG_warnDeveloper("got new question");
			
			if (gameState != FuturaGameStates.ENDTURN_WAITINGFORNEWTURN && gameState != FuturaGameStates.STARTTURN_READYFORNEWTURN && gameState != FuturaGameStates.ENDTURN_WAITINGFORNEWTURN )
				DEBUG_nonFatalError("começo de turno recebido antes da hora ("+gameState+")","CALLBACK_newQuestion");
			
			GAME_enteringState(FuturaGameStates.STARTTURN_GETNEWQUESTION);
			
			GAME_validateState(nextQuestionFromServer);
			
			nextQuestion = nextQuestionFromServer;
			previousServerTurn = currentServerTurn;
			currentServerTurn = nextQuestion.turn_id;
			gameTableId = nextQuestion.game_id;
			
			DEBUG_warnDeveloper ("turn for question:"+nextQuestion.current_turn);
			
			//DEBUG_warnDeveloper("players:"+nextQuestion.players);
			
			if (nextQuestion.players != null)
				GAME_updatePlayerList(nextQuestion.players);
			
			try	
			{
				/*
				if (isNaN(parseInt(previousServerTurn)) ||  isNaN(parseInt(currentServerTurn)))	
				{
				GAME_endTurn();
				return;
				}
				*/
				
				DEBUG_warnDeveloper("nextQuestion.answer_response.correct: "+ nextQuestion.answer_response.correct +" nextQuestion.answer_response.answer:"+nextQuestion.answer_response.answer);
				if ( /*parseInt(previousServerTurn) == (parseInt(currentServerTurn) - 1) && */(nextQuestion.answer_response.correct != null && nextQuestion.answer_response.answer != null))
				{
					previousOutcomeCandidate = nextQuestion.answer_response.correct/*Boolean(parseInt( nextQuestion.answer_response.correct ))*/;
					DEBUG_warnDeveloper("will show answer:"+nextQuestion.answer_response.correct);
					UI_showAnswer(previousOutcomeCandidate,nextQuestion.answer_response.answer);
				}
				else
				{					
					DEBUG_warnDeveloper("will NOT show answer (correct:"+nextQuestion.answer_response.correct+"); (answer:"+nextQuestion.answer_response.answer+")");
					GAME_endTurn();
				}				
			}
			
			catch (e:Error)
			{
				GAME_endTurn();
				return;
			}			
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_updatePlayerList(players:Array):void
		{
			for each (var user:Object  in players)
			GAME_updatePlayer(user);
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_updatePlayer(user:Object):void
		{
			if (user == null)
			{
				DEBUG_warnDeveloper("user é null!","GAME_updatePlayer");
				DEBUG_warnDeveloper("olha teu código!!","GAME_updatePlayer");
				return;
			}
			
			
			
			
			//DEBUG_warnDeveloper ("updatePlayer");
			var player:FuturaPlayer;
			var id:int;
			
			//DEBUG_warnDeveloper ("user.futura_id:" + user.futura_id);			
			
			id= GAME_getLocalIdFromFuturaId(user.futura_id);
			
			if (id == FuturaConstants.NONINITPLAYERID)
			{
				CALLBACK_addUser(user);
			}
			
			
			DEBUG_warnDeveloper ("id:"+ id,"UI_playerAnsweredCorrectly");
			
			
			player=GAME_getPlayer(id);
			
			
			if (player == null)
			{
				DEBUG_warnDeveloper("player é null!","");
				return;
			}			
			
			//DEBUG_warnDeveloper ("nome atualizado:"+user.display_name)
			//DEBUG_warnDeveloper ("active:"+user.active);
			
			if (user.active != null )
			{
				if (user.active is String)
					player.setActive(user.active == "true");
				else
					if (user.active is Boolean)
					{
						
						//DEBUG_warnDeveloper ("player = " + player);
						player.setActive(user.active);
					}
			}
			else
				DEBUG_warnDeveloper("usuário veio com active == null!","GAME_updatePlayer");
			
			player.setTime(parseInt(user.time));
			player.setColorIndex(parseInt(user.color_id));
			player.setHits(parseInt(user.hits));
			//DEBUG_warnDeveloper ("hits:"+user.hits);
			player.setMisses(parseInt(user.misses));
			//DEBUG_warnDeveloper ("misses:"+user.misses);
			player.setScore(parseInt(user.score));
			//DEBUG_warnDeveloper ("score:"+user.score);
			player.setName(user.display_name);
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_takeUserListFromServer(userList:Array):void
		{
			//	GAME_takePlayerList(userList);
		}	
		///--------------------------------------------------------------------------------------------------------------------
		private function CALLBACK_setMyId(MyId:String):void
		{
			DEBUG_warnDeveloper ("setMyId");
			myLocalId = MyId;
			var myIdCandidate:int=FuturaConstants.NONINITPLAYERID;;
			myIdCandidate=GAME_getLocalIdFromFuturaId(MyId);
			
			if (myIdCandidate == FuturaConstants.NONINITPLAYERID)
			{
				DEBUG_warnDeveloper("id não pronto", "CALLBACK_setMyId");
				return;
			}
			
			GAME_setMyId(myIdCandidate);
		}			
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_enteringState(NewGameState:int):void
		{
			if (FuturaConstants.ISDEBUG)
			{
				//				if (futuraInterface != null)
				//					futuraInterface.reportError(FuturaGameStates.toString(gameState)+" -> "+FuturaGameStates.toString(NewGameState));
				//				else
				//					DEBUG_warnDeveloper(FuturaGameStates.toString(gameState)+" -> "+FuturaGameStates.toString(NewGameState));
				//				
				//				DEBUG_warnDeveloper("\n"+FuturaGameStates.toString(gameState)+" -> "+FuturaGameStates.toString(NewGameState),"entering state");
			}
			
			gameState=NewGameState;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function CALLBACK_addUser(user:Object):void
		{
			if (
				user.display_name == null ||
				user.color_id == null ||
				user.misses == null ||
				user.futura_id == null ||
				user.score == null ||
				user.hits == null				
			)
			{
				DEBUG_warnDeveloper("tentativa de adicionar um jogador inválido","CALLBACK_addUser");
				return;
			}
			
			
			var player:FuturaPlayer;
			player=new FuturaPlayer();
			player.setActive(true);
			player.setName(user.display_name);
			
			if (user.hits != 0)
				player.setHits(parseInt(user.hits));
			
			if (user.misses != 0)
				player.setMisses(parseInt(user.misses));
			
			if (user.score != 0)
				player.setScore(parseInt(user.score));
			
			player.setTime(0);
			player.setColorIndex( parseInt(user.color_id));		
			player.setFuturaId(user.futura_id);		
			players.push(player);
			
			if (user.futura_id == myLocalId)
			{
				var myIdCandidate:int=FuturaConstants.NONINITPLAYERID;
				
				myIdCandidate=GAME_getLocalIdFromFuturaId(user.futura_id);
				
				if (myIdCandidate == FuturaConstants.NONINITPLAYERID)
				{
					DEBUG_warnDeveloper("id não pronto", "CALLBACK_addUser");
					return;
				}
				
				GAME_setMyId(myIdCandidate);				
			}
			
			
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_letterToIndex(letter:String):int
		{
			if (letter == "a") return 0;
			if (letter == "b") return 1;
			if (letter == "c") return 2;
			if (letter == "d") return 3;
			
			return FuturaConstants.NOANSWER;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function GAME_indexToLetter(index:int):String
		{
			var letters:String="abcd-";
			return letters[index];				
		}
		///--------------------------------------------------------------------------------------------------------------------		
		private function GAME_leaveInError():void
		{
			msgWindow.setInvisible();
			FuturaSafeGuard.fatalError("LEAVE_GAME_IN_ERROR","FuturaTable::GAME_leaveInError");
		}
		///-------------------------------------------------------------------------------------------------------------------
		private function GAME_leave():void
		{
			GAME_enteringState(FuturaGameStates.SYSTEM_OMEGA);
			futuraInterface.call("exitGame");
			visible=false;
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function DEBUG_fatalError(msg:String,where:String):void
		{
			futuraInterface.reportError(msg+":"+where);
			
			if (FuturaConstants.ISDEBUG)
				msgWindow.showModalMessage("Erro(@"+where+")",msg,GAME_leaveInError);			
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function DEBUG_nonFatalError(msg:String,where:String):void
		{
			FuturaSafeGuard.nonFatalError(msg,where);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setPiece
		 * coloca uma peça no tabuleiro, indicando acerto do jogador
		 * @param index posição da peça no tabuleiro [1..FuturaConstants.NUMPIECES]
		 * @param color indice da cor da seta [1..6]
		 * */		
		private function UI_setPiece(index:int, color:int):void
		{
			///checagem de consistência padrão
			//			checkGameConsistencyFor("setPiece");		
			
			///checagem de consistência de parametros////
			/// a posição da peça deve ser válida
			///NÃO USAR >= , POIS FLASH É [1..NUMCHILDREN]
			//			if (index > FuturaConstants.NUMPIECES || index < 1)
			//			{
			//				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			//			}
			//			
			//			/// a cor deve ser válida também 
			//			if (color < FuturaConstants.COLORGREENINDEX || color > FuturaConstants.COLORREDINDEX)
			//			{
			//				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			//			}				
			/////////////////////////////////////////////			
			
			///variáveis usadas na função
			var piece:Piece;
			
			///obtem a peça, torna visível e colore
			piece=piecesPosition.getChildAt(index) as Piece;
			piece.visible=true;
			piece.gotoAndStop(color);
		}
		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerAnsweredCorrectly
		 * indica que o jogador respondeu corretamente a pergunta
		 * @comment Aqui, não apenas a jogada é encerrada, mas a rodada também
		 * @see playerAnsweredWrongly
		 * */
		private function UI_playerAnsweredCorrectly():void
		{
			///checagem de consistência padrão
			GAME_checkConsistencyFor("playerAnsweredCorrectly");		
			
			///variáveis usadas na função
			var actor:FuturaPlayer;
			
			///notifica ao jogador do acerto
			actor=GAME_getCurrentPlayer();
			
			if (actor == null)
			{
				DEBUG_warnDeveloper("GAME_getCurrentPlayer retornou nulo", "UI_playerAnsweredCorrectly");
				return;	
			}
			
			actor.playerDidHit();
			//			if (correctAnswer!=null && (!GAME_localPlayerIsCurrentPlayer() || questionType == FuturaConstants.BONUSCARDID))
			//				correctAnswer.play();
			
			///incrementa e coloca sua peça no tabuleiro, se válido			
			
			
			///indica que a jogada esta encerrada (rodada também?)
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * playerAnsweredWrongly
		 * indica que o jogador responder erroneamente
		 * @see playerAnsweredCorrectly
		 * @comment Vale notar que aqui a rodada não é encerrada, apenas a jogada
		 * */
		private function UI_playerAnsweredWrongly():void
		{
			///checagem de consistência padrão
			GAME_checkConsistencyFor("playerAnsweredWrongly");		
			
			///variaveis usadas na função			
			var actor:FuturaPlayer;
			//			if (wrongAnswer!=null && (!GAME_localPlayerIsCurrentPlayer() || questionType == FuturaConstants.PENALTYCARDID))
			//				wrongAnswer.play();
			
			///notifica o jogador de seu erro
			actor=GAME_getCurrentPlayer();
			
			if (actor == null)
			{
				DEBUG_warnDeveloper("GAME_getCurrentPlayer retornou nulo", "UI_playerAnsweredWrongly");
				return;	
			}
			
			actor.playerDidError();
			
			///e finaliza a jogada
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * placeArrow
		 * coloca seta na posicao do tabuleiro
		 * @param index posicao da seta no tabuleiro [1..FuturaConstants.NUMPIECES]
		 * @param color indice da cor da seta [1..6]
		 * */
		private function UI_placeArrow(index:int,color:int):void
		{
			///checagem de consistência padrão
			//checkGameConsistencyFor("placeArrow");
			
			///checagem de consistência de parametros////
			/// a posição da peça deve ser válida
			//			if (index >= arrowPosition.numChildren || index >= FuturaConstants.NUMPIECES || index < 0)
			//			{
			//				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( index="+index.toString()+"/"+arrowPosition.numChildren.toString()+")");
			//			}
			//			
			//			/// a cor deve ser válida também 
			//			if (color < FuturaConstants.COLORGREENINDEX || color > FuturaConstants.COLORREDINDEX)
			//			{
			//				fatalError(FuturaStrings.INCONSISTENT_GAME_STATE);
			//			}			
			/////////////////////////////////////////////
			
			///variáveis usadas na função
			var arrow:Arrow;
			
			///apagando todas outras possíveis setas visíveis
			for (var c:int=0;c<index;c++)
				if (arrowPosition.getChildAt(c) is Arrow)
				{
					arrow=arrowPosition.getChildAt(c) as Arrow;
					arrow.visible=false;
				}
			
			///obtendo a seta atual, tornando-a visível e colorida
			arrow=arrowPosition.getChildAt(index) as Arrow;
			arrow.visible=true;
			arrow.gotoAndStop(color);			
		}		
		///------------------------------------------------------------------------------------------------------------------
		/**
		 * checkGameConsistencyFor
		 * checa para ver se o jogo esta válido antes de prosseguir
		 * @param String funcName nome da função que pediu a checagem
		 * @comment nem todas as checagens são executadas, pois essa checagem pode esbarrar num caso em que algo
		 * @comment testado ainda não esta pronto e este é seu estado esperado
		 * */
		private function GAME_checkConsistencyFor(funcName:String):void
		{
			/////////////<checagem padrão de consistência de jogo
			///jogador atual é válido?
			if (players==null)
				DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 1 em FuturaTable::"+funcName+" )","GAME_checkConsistencyFor");
			
			///uma pequena leniencia com o sistema ainda não totalmente inicializado
			if(currentPlayer != FuturaConstants.NONINITPLAYERID)
			{
				
				if (players[currentPlayer]==null)
					DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 2 em FuturaTable::"+funcName+" )","GAME_checkConsistencyFor");
				
				//				///jogador atual é ativo?
				//				if (!(players[currentPlayer] as FuturaPlayer).getActive() )
				//					nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 3 em FuturaTable::"+funcName+" )");
			}
			
			
			if (localPlayerInternalId < 0 || localPlayerInternalId > players.length)
				DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 4 em FuturaTable::"+funcName+" )","GAME_checkConsistencyFor");
			
			if (players[localPlayerInternalId]!=null && !players[localPlayerInternalId].getActive())
				DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 5 em FuturaTable::"+funcName+" )","GAME_checkConsistencyFor");
			
			if (currentPlayer != FuturaConstants.NONINITPLAYERID )
			{
				if (currentPlayer < 0 || currentPlayer > players.length)
					DEBUG_fatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 6 em FuturaTable::"+funcName+" )","GAME_checkConsistencyFor");
			}
			
			///se é minha vez, minha pergunta é válida? (o id)
			//			if ( currentPlayer==localPlayerInternalId && ( questionId==null || questionId=="" || questionId==" ") )
			//				nonFatalError(FuturaStrings.INCONSISTENT_GAME_STATE+" ( teste 7 em FuturaTable::"+funcName+" )");
			
			///o jogo esta em andamento? (não é erro, mas deve gerar um warning no log)
			if (gameStopped)
			{
				DEBUG_warnDeveloper(FuturaStrings.CALLS_DURING_STOPPED_GAME+" ( teste 8 em FuturaTable::"+funcName+" )","GAME_checkConsistencyFor");
				return;
			}
			//tudo certo
		}
		///------------------------------------------------------------------------------------------------------------------
		private function GAME_whosWinning():int
		{
			var maior:int;
			var maior_score:int;
			var player:FuturaPlayer;
			
			
			for (var d:int=0;d<players.length;d++)
			{
				if (players[d].getActive())
				{
					maior=d;
					maior_score= players[d].getScore();
				}
			}		
			
			for (var c:int=0;c<players.length;c++)
			{
				player=players[c];
				if (player.getActive() && player.getScore() > maior_score )
				{
					maior=c;
					maior_score= players[c].getScore();
				}
			}
			
			return maior;	
		}
		///------------------------------------------------------------------------------------------------------------------
		private function GAME_validateState(gameState:Object):void
		{
			///verifica se algum campo obrigatório é nulo
			
			if (gameState.table_state == null)
				DEBUG_fatalError("estado de tabuleiro inválido","GAME_validateState");
			
			if (gameState.players == null)
				DEBUG_fatalError("lista de jogadores inválida","GAME_validateState");
			
			if (gameState.current_player_id == null && !gameState.end_game )
				DEBUG_fatalError("id de jogador atual inválido","GAME_validateState");
			
			if (gameState.new_question == null && !gameState.end_game )
				DEBUG_fatalError("nova pergunta inválida","GAME_validateState");
			
			
			
			//			//DEBUG_warnDeveloper("pergunta:"+gameState.new_question.title);
			//			//DEBUG_warnDeveloper("a:"+gameState.new_question.options.a);
			//			//DEBUG_warnDeveloper("b:"+gameState.new_question.options.b);
			//			//DEBUG_warnDeveloper("c:"+gameState.new_question.options.c);
			//			//DEBUG_warnDeveloper("d:"+gameState.new_question.options.d);
			//			//DEBUG_warnDeveloper("current:"+gameState.current_player_id);
			
		}
		
		///------------------------------------------------------------------------------------------------------------------
		private function DEBUG_warnDeveloper(msg:String,where:String=""):void
		{
			var localInternalId:String;
			var localExternalId:String;
			
			localInternalId = localPlayerInternalId.toString();
			
			if (GAME_getLocalPlayer() == null)
				localExternalId = "não disponivel";
			else
				localExternalId = GAME_getLocalPlayer().getFuturaId(); 
			
			trace("localId:"+ localInternalId +"id externo:"+ localExternalId +","+msg+"@"+where);
			
			if (debugMsgs!=null)
			{
				
				if (debugMsgs.text.length > 15000)
					debugMsgs.text = debugMsgs.text.substring( debugMsgs.text.length - 15000, debugMsgs.text.length);
				
								
				debugMsgs.appendText("\n\n>"+"localId:"+ localInternalId +"id externo:"+ localExternalId +","+msg+"@"+where);
				
				var rect:Rectangle = debugMsgs.scrollRect;
				
				if (rect == null)
				{
					rect = new Rectangle();
					rect.width = debugMsgs.width;
					rect.height = debugMsgs.height;
					//rect.x = debugMsgs.x; 
				}
				
				var bla : int = debugMsgs.textHeight  -400;
				
				if ( bla > 0 )
				{
					rect.y = bla;
					trace ("rect.y: "+ rect.y + " textheight: "+ debugMsgs.textHeight);
					debugMsgs.scrollRect = rect;
					debugMsgs.height = debugMsgs.textHeight;
				}
				else
				{
					
				}
			}
			//			if (debugMsgs!= null && debugMsgs.text.length > 5250)
			//				debugMsgs.text= "";
			
			
		}
		
		private function GAME_replay():void
		{
			if (futuraInterface != null)
				futuraInterface.askForReplay(gameTableId);
			UI_hideAllWindows();
		}
			
		///------------------------------------------------------------------------------------------------------------------
		//		private function DEBUG_returnMyFuturaId():String
		//		{
		//			if (localPlayerInternalId > 0 && localPlayerInternalId < activePlayers)
		//				return getPlayer(localPlayerInternalId).getFuturaId();
		//			else
		//				return null;
		//		}
		//===================================================================================================================================		
	}
}