package GUILib
{
	///flash
	import flash.display.Sprite;
	
	/**
	 * SpriteHorizontalList
	 * Lista horizontal
	 * @author Daniel Monteiro
	 * 
	 */
	public class SpriteHorizontalList extends FadableSprite
	{
		///----------------------------------------------------------------------------------------------------------
		/**
		 * contem os items a ser listados. Preferencialmente, SpriteAndTextSprite, mas FadableSprite é o básico
		 */
		private var items:Array; ///<SpriteAndTextSprite>
		/**
		 * lista contendo os espaços entre cada item.
		 */
		private var spaces:Array;
		/**
		 * qual foi o ultimo item adicionado. É apenas um atalho.
		 */
		private var lastItem:FadableSprite;
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * construtor
		 */
		public function SpriteHorizontalList()
		{		
			super();
			spaces=new Array();
			
			items=new Array();
			lastItem=null;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * recalculatePositions
		 * Re-distribui os items, pro caso de algum item ter sido alterado
		 * 
		 */
		public function recalculatePositions():void
		{
			var sprite:FadableSprite;
			var withSpacing:int;
			
			lastItem=null;
			
			for (var c:int;c<items.length;c++)
			{
				sprite=items[c];
				withSpacing=spaces[c];
				
				if (lastItem!=null)
				{
					sprite.x=lastItem.x+lastItem.getWidth()+withSpacing;				
				}
				lastItem=sprite;
			}
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * addItem
		 * @param sprite O sprite em sí a ser adicionado na lista
		 * @param withSpacing O espaço em relação ao ultimo item
		 * 
		 */
		public function addItem(sprite:FadableSprite,withSpacing:int=0):void
		{
			items.push(sprite);
			addChild(sprite);
			if (lastItem!=null)
			{
				sprite.x=lastItem.x+lastItem.getWidth()+withSpacing;				
			}
			lastItem=sprite;
			spaces.push(withSpacing);
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getItemAt
		 * @param n Indice do item
		 * @return O item a ser retornado
		 * 
		 */
		public function getItemAt(n:int):FadableSprite
		{
			return items[n];
		}
		///----------------------------------------------------------------------------------------------------------		
	}
}