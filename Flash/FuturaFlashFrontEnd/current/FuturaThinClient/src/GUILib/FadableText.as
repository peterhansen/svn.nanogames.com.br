package GUILib
{
	///flash
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * FadableText
	 * Campo de texto especial, com suporte a feedback, animações de fade, etc.
	 * @author Daniel Monteiro
	 * @comment Aproveitada do Dengue, onde foi uma grande dor de cabeça. Aqui também foi, mas foi bem menor.
	 */
	public class FadableText extends FadableSprite
	{
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * O campo de texto em sí 
		 */
		private var text:TextField;
		/**
		 * Estamos no modo de texto multi-linhas? 
		 */
		private var multiLine:Boolean;
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * Construtor 
		 * @param Text O texto exibido
		 * @param color A cor do texto
		 * @param textFormat Formato de texto (fontes, etc)
		 * @comment Flash tem umas cagadas homéricas que fazem essa classe realmente necessária
		 */
		public function FadableText(Text:String,color:uint=0,textFormat:TextFormat=null)
		{
			super();
			
			text=new TextField();
			
			
			setMultiline(false);
			setText(Text);
			
			addChild(text);
			text.autoSize=flash.text.TextFieldAutoSize.LEFT;
			
			text.alwaysShowSelection=false;
			text.selectable=false;
			text.tabEnabled=false;
			
			if (textFormat!=null)
			{
				text.defaultTextFormat=textFormat;
				text.setTextFormat(textFormat);
			}
			
			text.textColor=color;	
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setDim
		 * Um dos maiores solucionadores de problema 
		 * @param numLines numero de linhas de texto desejadas
		 * @param x0 top do top-left
		 * @param y0 left do top-left
		 * @param x1 limite horizontal da caixa de texto
		 * @comment função extremamente amarrada nos requisitos do projeto, mas funciona.
		 */
		public function setDim(numLines:int, x0:int, y0:int, x1:int):void
		{
			var rect:Rectangle;
			setMultiline(true);
			
			rect=text.scrollRect;
			if (rect==null)
				rect=new Rectangle();
			
			rect.top=y0;
			rect.left=0;
			rect.bottom=y0+numLines*(text.textHeight/text.numLines)+GUIConstants.DEFAULTVERTICALSPACING/numLines;
			rect.right=x1;
			text.width=x1-x0;
			
			if (text.numLines>1)
				text.height=rect.height;
			
			text.scrollRect=rect;
			text.height=rect.bottom=y0+numLines*(text.textHeight/text.numLines)+GUIConstants.DEFAULTVERTICALSPACING/numLines;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * getHeight
		 * Segundo grande solucionador de problemas. Essa função retorna a altura de texto realmente ocupada pelo texto.
		 * Flash costuma deixar um espaço enorme 
		 * @return O espaço usado só pelo texto em sí
		 * @comment porque o resultado, tendo uma scrollRect, haveria de ser diferente do tamanho da scrollRect?
		 */
		override public function getHeight():Number
		{
			
			if (text.scrollRect!=null)
				return text.scrollRect.height;
			else
				return height;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setRect
		 * Tenta forçar um tamanho pro texto 
		 * @param x0 
		 * @param y0
		 * @param x1
		 * @param y1
		 */
		public function setRect(x0:int, y0:int, x1:int, y1:int):void
		{
			var rect:Rectangle;
			setMultiline(true);
			text.width=x1-x0;
			text.height=y1-y0;
			
			rect=text.scrollRect;
			if (rect==null)
				rect=new Rectangle();
			rect.top=y0;
			rect.left=x0;
			rect.bottom=y1;
			rect.right=x1;
			text.scrollRect=rect;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * getRawTextField 
		 * @return o TextField do flash
		 * 
		 */
		public function getRawTextField():TextField
		{
			return text;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setMultiline 
		 * @param m define se estamos ou não no modo multi-linhas
		 * 
		 */
		public function setMultiline(m:Boolean):void
		{
			multiLine=m;
			text.wordWrap=m;			
			text.multiline=m;			
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getText 
		 * @return a String contida no TextField interno do flash 
		 * 
		 */
		public function getText():String
		{
			return text.text;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setText 
		 * @param Text O texto a ser exibido no textfield
		 * 
		 */
		public function setText(Text:String):void
		{
			text.text=Text;
			if (multiLine)
			{
				text.width=text.textWidth;
			}
		}
		///----------------------------------------------------------------------------------------------------------
	}
}