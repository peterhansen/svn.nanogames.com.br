package GUILib
{
	///flash
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * FadableSprite
	 * Sprite especial, com animações de fade in e fade out, e que ja tem suporte a feedback do mouse
	 * @author Daniel Monteiro
	 * @comment surgiu no mini-game da dengue, no vila virtual
	 */
	public class FadableSprite extends FeedbackSprite
	{
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * timer de animação de fade in e fade out
		 */
		private var timer:Timer;
		/**
		 * opacidade final desejada
		 */
		private var alphaFinal:Number;
		private var alphaStart:Number;
		/**
		 * o quanto deve ser acrescido na opacidade a cada passo
		 */
		private var tickStep:Number;
		/**
		 * função a ser chamada ao fim da animação de exibição
		 */
		private var onShow:Function;
		/**
		 * função a ser chamada ao fim da animação de ocultação
		 */
		private var onHide:Function;
		///----------------------------------------------------------------------------------------------------------
		/**
		 * construtor 
		 * apenas inicializa e instancia, mas não inicia nada
		 */
		public function FadableSprite()
		{
			super();
			timer=new Timer(GUIConstants.FADE_INTERVAL);
			timer.addEventListener(TimerEvent.TIMER,this.tickAnim);
			onShow=null;
			onHide=null;
		}
		///----------------------------------------------------------------------------------------------------------
		public function setAlphaFinal(targetAlpha:Number):void
		{
			alphaFinal = targetAlpha;		
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * setOnShow 
		 * @param OnShow função definida para ser chamada quando ocorrer a animação de exibição
		 * 
		 */
		public function setOnShow(OnShow:Function):void
		{
			onShow=OnShow;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * setOnHide 
		 * @param OnHide função definida para ser chamada quando ocorrer a animação de ocultação
		 * 
		 */
		public function setOnHide(OnHide:Function):void
		{
			onHide=OnHide;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * startShowAnimation
		 * inicia animação de exibição 
		 * @param OnShow função opcional (que é guardada), para quando a animação terminar
		 * 
		 */
		public function startShowAnimation(OnShow:Function=null, alphaInitial:Number = 0.0, targetAlpha:Number = 1.0):void		
		{			
			visible=true;
			alphaStart = alpha =alphaInitial;
			alphaFinal=targetAlpha;
			tickStep=alphaFinal/GUIConstants.FADE_FRAMES;
			timer.start();
			
			if (OnShow!=null)
				onShow=OnShow;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * startHideAnimation
		 * inicia animação de ocultação
		 * @param OnHide função opcional (que é guardada), para quando a animação terminar
		 * 
		 */
		public function startHideAnimation(OnHide:Function=null, alphaInitial:Number = 1.0, targetAlpha:Number = 1.0):void
		{		
			alphaStart = alpha = alphaInitial;
			alphaFinal= targetAlpha;
			tickStep= - alphaFinal/GUIConstants.FADE_FRAMES;
			//alphaFinal=0;
			timer.start();
			
			if (OnHide!=null)
				onHide=OnHide;
		}
		///----------------------------------------------------------------------------------------------------------		
		/**
		 * stopAllAnimations 
		 * encerra todas as animações em andamento e torna a janela totalmente opaca, mas não altera sua 
		 * visibilidade
		 */
		public function stopAllAnimations():void
		{
			timer.stop();
			alpha=1;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setVisible 
		 * torna o sprite totalmente visivel e opaca
		 */
		public function setVisible():void
		{
			stopAllAnimations();
			visible=true;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * setInvisible 
		 * torna o sprite invisivel, mas opaca
		 */
		public function setInvisible():void
		{
			timer.stop();
			alpha=1;
			visible=false;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getHeight 
		 * @return altura do sprite
		 * 
		 */
		public function getHeight():Number
		{
			return height;
		}
		///----------------------------------------------------------------------------------------------------------
		/**
		 * getWidth 
		 * @return largura do sprite
		 * 
		 */
		public function getWidth():Number
		{
			return width;
		}
		///----------------------------------------------------------------------------------------------------------				
		/**
		 * tickAnim
		 * Da andamento na animação atual. Se houverem callbacks definidos, estes são chamados.
		 * Se a animação chegar em suas situações limite, esta é interrompida 
		 * @param e parametros do timer
		 * @comment não tenho grande orgulho dessa função, mas ela tem se mostrado bastante sólida.
		 */
		private function tickAnim(e:TimerEvent):void
		{
			///executa o passo da animação em sí
			var tmp:Number=alpha+tickStep;
			var finished:Boolean = false;
			
			if (tickStep>0)
			{
				if (tmp < alphaFinal)	
					this.alpha=tmp;
				else
					finished=true;
			} 
			else
			{
				if (tmp > alphaFinal)	
					this.alpha=tmp;
				else
					finished=true;
			}
			
			
			///e verifica as situações limite
//			if (tmp<alphaFinal && tmp>alphaStart) ///tudo normal, atribuimos e seguimos a vida			
//				this.alpha=tmp;
//			else
			if (finished)
			{
				///chegamos a alguma situação limite. Em todo caso, temos que parar o timer
				timer.stop();
				
				///a animação era de exibição
				if (tickStep>0)
				{
					///tornamos a janela visível
					alpha=alphaFinal;
					if (onShow!=null)
						onShow();
				}
				else ///não era o caso. Vamos apenas forçar para que ela fique invisivel de uma vez
					this.alpha=alphaFinal;
			}
			
			///sendo o caso de ter se tornado invisível, vamos torna-la invisivel mas opaca. chamar callbacks também
			if (this.alpha==0)
			{
				visible=false;
				alpha=1;
				
				if (onHide!=null)
					onHide();
			}
		}
		///----------------------------------------------------------------------------------------------------------		
	}	
}