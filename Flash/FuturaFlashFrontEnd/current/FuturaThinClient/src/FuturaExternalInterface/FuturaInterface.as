package FuturaExternalInterface
{
	//flash
	import GUILib.GUIConstants;
	
	import flash.events.*;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	import flash.utils.getTimer;

	/**
	 * FuturaInterface
	 * comunicação com o servidor do Futura CDF
	 * @author Yuri Mello
	 * @comment modificado por Daniel Monteiro
	 * */
	public class FuturaInterface extends FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * callback para resposta à reposta do usuário
		 */
		private var newQuestionCallback:Function;
		/**
		 * callback para obter a lista de jogadores
		 */
		private var userListCallback:Function;
		/**
		 * callback para identificar o jogador local
		 */
		private var setLocalIdCallback:Function
		private var gameEndedCallback:Function;
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * FuturaInterface
		 * */
		public function FuturaInterface(takeUserListFunc:Function, setMyIDFunc:Function, newQuestionFunc:Function, gameEndedFunc:Function)
		{
			super(takeUserListFunc,setMyIDFunc,newQuestionFunc,gameEndedFunc);
			///checagem dos parametros
			
			if (ExternalInterface==null || takeUserListFunc==null || setMyIDFunc==null || newQuestionFunc==null )
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaInterface::FuturaInterface");
			
			///preenche os callbacks
			gameEndedCallback=gameEndedFunc;
			userListCallback=takeUserListFunc;
			setLocalIdCallback=setMyIDFunc;
			newQuestionCallback=newQuestionFunc;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * call
		 * @param cmd comando a ser executado pelo servidor
		 * 
		 */
		override public function call(cmd:String):Object
		{
			if (ExternalInterface == null || cmd == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaInterface::call");	
			
			return ExternalInterface.call(cmd);
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		override public function init():void
		{
			if (ExternalInterface==null ||userListCallback==null || newQuestionCallback==null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaInterface::init");
			

			
			///Javascript <- Flash	
			ExternalInterface.addCallback("newQuestion", newQuestionCallback);

			///Flash -> Javascript
			userListCallback(ExternalInterface.call("requestUsersList"));
			setLocalIdCallback(ExternalInterface.call("getMyID"));
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * call
		 * @param cmd comando a ser executado pelo servidor
		 * 
		 */
		override public function reportError(reason:String):void
		{
			if (ExternalInterface == null || reason == null)
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"FuturaInterface::reportError");	
			
			//ExternalInterface.call("reportError",reason);
			loadURL(FuturaConstants.SERVER_BASE_URL + "/error?what="+reason);
		}	
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * verifyAnswer
		 *
		 * @param String answer a resposta do usuário [a..d | - ]
		 * @param String questionId Id externo da pergunta
		 * 
		 * */
		override public function sendAnswer(answer:String, time:int):void
		{
			
			var sent:Boolean = false;
			var time0:int = getTimer();
			var time1:int;
			
			while (!sent)
			{
				try
				{
					///checagem dos parametros
					if (answer==null || answer=="")
						FuturaSafeGuard.fatalError(FuturaStrings.INCONSISTENT_DATA_RECEIVED,"FuturaInterface::sendAnswer");
					
					
				//	ExternalInterface.call("getAnswer", answer,time);
					loadURL(FuturaConstants.SERVER_BASE_URL + "/games/verify_answer?answer="+answer+"&time="+time);
					sent = true;
				}
				catch (e:Error)
				{
					trace("falha no envio");
					time1 = getTimer();
					
					if ((time1-time0) > 60*GUIConstants.SECONDS)
					{
						trace("cansei de tentar");
						return;
					}
				}
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function setCallback(externalName:String, internalFunc:Function):void
		{
			ExternalInterface.addCallback(externalName, internalFunc);
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function gameEnded():void
		{
			if (gameEndedCallback != null)
				gameEndedCallback();
		}
		///--------------------------------------------------------------------------------------------------------------------		
		override public function notifyTick(serverTick:String, gameId:String, f:Function, userId:String):void
		{
			try
			{
				super.notifyTick(serverTick, gameId,f, userId);
				//return ExternalInterface.call("getStateUpdate", serverTick, gameId);
				loadURL(FuturaConstants.SERVER_BASE_URL+"/games/state_update/?game_turn="+serverTick+"&game_id="+gameId+"&user_id="+userId,f);
			}
			catch (e:Error)
			{
				//fica para o próximo turno
				trace("Falha na recepção de dados");				
			}
		}		
		///--------------------------------------------------------------------------------------------------------------------
		override public function gameReady():void
		{
			loadURL(FuturaConstants.SERVER_BASE_URL + "/games/game_ready");
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function exitGame():void
		{
			loadURL(FuturaConstants.SERVER_BASE_URL + "/games/exit_and_redirect");
		}		
		///--------------------------------------------------------------------------------------------------------------------
		private function loadURL(url:String, f:Function = null):void
		{
			var urlLoader:URLLoader;
			var urlReq:URLRequest;
			
			try
			{
				urlLoader = new URLLoader();
				urlLoader.addEventListener ( IOErrorEvent.IO_ERROR, errorHandlerListener );
				urlLoader.addEventListener ( SecurityErrorEvent.SECURITY_ERROR, errorHandlerListener );
				
				if (urlLoader == null)
					return;
				
				urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
				
				urlReq = new URLRequest(url);
				
				if (urlReq == null)
					return;
				
				urlLoader.load(urlReq);
				
				if (f != null)
					urlLoader.addEventListener(Event.COMPLETE, f);
			}
			catch (e:Error)
			{
				errorHandler();
			}
		}
		///--------------------------------------------------------------------------------------------------------------------
		private function errorHandler():void
		{
			trace ("falha na comunicação");
		}
		///--------------------------------------------------------------------------------------------------------------------		
		override public function askForReplay(gameId:String):void
		{
			if (ExternalInterface != null)
				ExternalInterface.call("replay", gameId);
		}	
		///--------------------------------------------------------------------------------------------------------------------
		private function errorHandlerListener(e:Event):void
		{
			errorHandler();
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}