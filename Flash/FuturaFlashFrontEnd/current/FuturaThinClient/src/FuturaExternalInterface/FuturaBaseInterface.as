package FuturaExternalInterface
{
	///som
	/**
	 * som de interação com o botão 
	 */
	import JSONLite.JSON;
	
	import botao.mp3;

	/**
	 * FuturaBaseInterface
	 * base abstrata de comunicação com servidor
	 * @author Daniel Monteiro
	 * */
	public class FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * FuturaBaseInterface
		 * construtor que recebe callbacks
		 * @comment baseada na interface criada por Yuri Mello
		 * */
		public function FuturaBaseInterface(takeUserList:Function, setMyID:Function, newQuestion:Function, gameEnded:Function):void
		{		
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		public function init():void
		{
			
		}		
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * */
		public function getQuestionsFromServer(question:Object, previousIsCorrect:Boolean, previousAnswer:String):void
		{
			
		}

		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * Chama rotina no servior 
		 * @param cmd O comenado a ser executada
		 * @return  retorno da função, se houver.
		 * 
		 */
		public function call(cmd:String):Object
		{
			return null;	
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function sendAnswer(answer:String, time:int):void
		{
		
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function setCallback(externalName:String, internalFunc:Function):void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		public function reportError(reason:String):void
		{
		
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function gameEnded():void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		public function notifyTick(serverTick:String, gameId:String, f:Function, userId:String):void
		{
		
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function getBackgroundUrl():String
		{
			return "";
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function gameReady():void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		public function exitGame():void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------		
		public function askForReplay(gameId:String):void
		{
		}		
		///--------------------------------------------------------------------------------------------------------------------
		public static function parseJSON(str:String):Object
		{
			var obj:Object;
			
			if (str != null && str != "")
				return JSON.deserialize(str);
			else
				return new Object();
		}
	}
}