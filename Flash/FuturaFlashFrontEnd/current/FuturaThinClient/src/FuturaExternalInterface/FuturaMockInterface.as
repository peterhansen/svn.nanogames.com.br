package FuturaExternalInterface
{
	import JSONLite.JSON;
	
	import flash.events.Event;
	
	import org.osmf.composition.SerialElement;
	/**
	 * FuturaBaseInterface
	 * base abstrata de comunicação com servidor
	 * @author Daniel Monteiro
	 * */
	public class FuturaMockInterface extends FuturaBaseInterface
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * callback para responder se a resposta estava correta
		 */
		private var questionCallback:Function;
		/**
		 * uma variável apenas pra saber qual deve ser a próxima resposta a ser dada ao sistema
		 */
		private var lastAnswer:Boolean;
		/**
		 * callback que passa a lista de usuários pro jogo
		 */
		private var userList:Function;
		/**
		 * callback que avisa quem é o jogador local
		 */
		private var whoAmI:Function;
		/**
		 * callback de fim de jogo
		 */
		private var gameEndedCallback:Function;
		/**
		 * numero da questão 
		 */
		private var qnum:int;
		private var serverTurn:int;
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * FuturaMockInterface
		 * construtor que recebe callbacks
		 * @comment baseada na interface criada por Yuri Mello
		 * */
		public function FuturaMockInterface(takeUserList:Function, setMyID:Function, newQuestion:Function, gameEndedFunc:Function):void
		{	
			super(takeUserList,setMyID,newQuestion, gameEndedFunc);
			gameEndedCallback=gameEndedFunc;
			lastAnswer=true;
			questionCallback=newQuestion;
			userList=takeUserList;		
			whoAmI=setMyID;
			serverTurn=0;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * init
		 * inicializa o jogo com o servidor
		 * */
		override public function init():void
		{
			qnum=0;
			sendUserList();
			sendLocalUserNotify();
			//sendQuestion(null,false,"");
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * getQuestionsFromServer
		 * obtem uma pergunta do servidor
		 * @returns Object a pergunta
		 * */
		public function sendQuestion(question:Object, previousIsCorrect:Boolean, previousAnswer:String):Object
		{		
			return getQuestionObject();	
		}
		
		public function getQuestionObject():Object
		{
			if (questionCallback != null)
			{
				/// variáveis usadas pela função
				var pergunta:Object;
				var a:Array=new Array();
				var user:Object;
				var gameState:Object=new Object();
				
				gameState.game_speed = "turbo";
				
//				user=new Object();
//				user.futura_id=1;
//				user.score=270;
//				user.hits=436;
//				user.misses=0;
//				user.display_name="joão1";			
//				user.color_id="1";
//				user.active="true";
//				user.time="211";
//				a.push(user);
//				
//				user=new Object();
//				user.futura_id=2;
//				user.hits=46;
//				user.score=70;
//				user.misses=0;
//				user.color_id="2";
//				user.display_name="joão2";
//				user.active="true";
//				user.time="312";
//				a.push(user);
//				
//				user=new Object();
//				user.futura_id=3;
//				user.score=40;
//				user.hits=36;
//				user.misses=0;
//				user.color_id="3";
//				user.display_name="joão3";
//				user.active="true";
//				user.time="413";
//				a.push(user);
				
				for ( var i : int = 0; i < 6; ++i ) {
					user=new Object();
					user.futura_id= i + 1;
					user.score=Math.random() * 10;
					user.hits=0;
					user.misses=0;
					user.time=""+ int(Math.random() * 90);
					trace ("id:"+ user.futura_id + " time: "+ user.time);
					user.active="true";
					user.color_id="" + ( i + 1 );
					user.display_name="joão" + ( i + 1 );
					a.push(user);				
				}
				
//				user=new Object();
//				user.futura_id=5;
//				user.score=3;
//				user.hits=0;
//				user.misses=0;
//				user.time="0";
//				user.color_id="5";
//				user.active="true";
//				user.display_name="joão5";
//				a.push(user);
//
//				
//				user=new Object();
//				user.futura_id=6;
//				user.score=0;
//				user.hits=0;
//				user.misses=0;
//				user.time="570";
//				user.color_id="6";
//				user.active="true";
//				user.display_name="joão6";
//				a.push(user);
				
				
				gameState.players=a;
				var _lastAnswer:Object=new Object();
				
				if (qnum > 0)
				{
					
					
					if (lastAnswer)
						_lastAnswer.correct=1;
					else
						_lastAnswer.correct=false;
					
					_lastAnswer.answer="a";
					_lastAnswer.player_id="1";
					
				}
				
				gameState.answer_response=_lastAnswer;
				gameState.time_elapsed = "0";
				
				

//				if (qnum == 6)
//				{
			//		gameState.end_game = true;
//				}
//				else
				{
					///criação do objeto de pergunta
					pergunta=new Object();
					
					pergunta.options=new Object();
					///verificando se não conseguiu alocar o objeto
					if (pergunta==null)
						FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"mock");
					
					///preenche o objeto
					pergunta.options.a=null;
					pergunta.options.b=null;
					pergunta.options.c=null;
					pergunta.options.d=null;

					///3 x 34
					//pergunta.title="O XPeria X10 Mini da Maria Chuchu é Android de verdade? Eu prefiro meu Motorola Milestone, só com a UI Padrão do Android! Muito mais Android do que iOS e Windows Phone 7! Que pena, não é, Maemo? Eu gostava";
					  pergunta.title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum tellus in nibh cursus pellentesque. Nullam ut tincidunt ipsum. Nulla facilisi. Sed eros lorem, porta in consectetur rutrum, porta sit amet ipsum. Aliquam posuere arcu a nibh gravida auctor. Fusce nunc felis, facilisis id luctus quis, sollicitudin sed massa. Donec vitae sem est, auctor vestibulum arcu. Proin ultricies justo quis mauris vulputate tincidunt. Nulla sodales dui sed neque consectetur imperdiet. Suspendisse augue risus, rutrum luctus suscipit at, feugiat et nunc. Proin ornare ornare orci. Suspendisse suscipit feugiat porta. Suspendisse quis tincidunt libero.";
					//pergunta.title="1";
//					switch (qnum+4)
//					{
//						case 4:
//						case 8:
					  //22 x 2
						//	pergunta.options.a="não...Pode ser qualquer coisa e eu poderia deliberar por horas a fio a sua simples razão de existência, mas não é o caso para tal";
						pergunta.options.a="manter o número de telefone ao mudar de plano de serviço , ou seja, pode passar de um plano pré-pago para um pós-pago e vice-versa;";					
//						case 3:
//						case 7:
//						case 5:
//						case 9:
							pergunta.options.b="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum tellus in nibh cursus pellentesque. Nullam ut tincidunt ipsum. Nulla facilisi. Sed eros lorem, porta in consectetur rutrum, porta sit amet ipsum.";
//						case 6:
//						case 2:
//						case 1:
//						case 0:
//						case 10:
							pergunta.options.c="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec bibendum tellus in nibh cursus pellentesque. Nullam ut tincidunt ipsum. Nulla facilisi. Sed eros lorem, porta in consectetur rutrum, porta sit amet ipsum.";
						//	pergunta.options.d="Lorem ipsum dolor sit amet, ";//consectetur adipiscing elit. Donec bibendum tellus in nibh cursus pellentesque. Nullam ut tincidunt ipsum. Nulla facilisi. Sed eros lorem, porta in consectetur rutrum, porta sit amet ipsum.";
//					}
			
							
					//if (qnum == 3 || qnum == 9)
					pergunta.kind = 1;
			
	
					gameState.new_question=pergunta
					
					
					gameState.current_player_id = "1";
					gameState.turn_id = serverTurn.toString();
						
					qnum++;
					
					
					
					
					
					lastAnswer = !lastAnswer;
				}
				gameState.background_url="http://info.abril.com.br/noticias/blogs/gadgets/files/2010/11/Android-Google-SO.jpg";
				
				switch (qnum++)
				{
					case 1:
						gameState.table_state=["1","3","2","1","6","2","1","5"];
						break;
					case 2:
						gameState.table_state=["1","3","2","1","6","2","1","5","2"];
						break;
					case 3:
						gameState.table_state=["1","3","2","1","6","2","1","5","3","2"];
						break;
				
				}
				//questionCallback(gameState);				
			}
			return gameState;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * sendUserList 
		 * Pede lista de usuários
		 */
		private function sendUserList():void
		{
			trace("FuturaMockInterface::sendUserList()");				
			var a:Array=new Array();
			var user:Object;
			
//			user=new Object();
//			user.id=1;
//			user.nickname="joão1";			
//			user.color="1";
//			user.active="true";
//			user.time="10";
//			a.push(user);
//			
//			user=new Object();
//			user.id=2;
//			user.color="2";
//			user.nickname="joão2";
//			user.time="10";
//			user.active="true";
//			a.push(user);
//			
//			user=new Object();
//			user.id=3;
//			user.color="3";
//			user.nickname="joão3";
//			user.time="230";
//			user.active="true";
//			a.push(user);
//			
//			user=new Object();
//			user.id=4;
//			user.color="4";
//			user.nickname="joão4";
//			user.time="120";
//			user.active="true";
//			a.push(user);
//
//			
//			user=new Object();
//			user.id=5;
//			user.color="5";
//			user.nickname="joão5";
//			user.time="120";
//			user.active="true";
//			a.push(user);
//
//			user=new Object();
//			user.id=6;
//			user.color="6";
//			user.nickname="joão6";
//			user.time="120";
//			user.active="true";
//			a.push(user);
			
			if (userList!=null)
				userList(a);
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"mock");
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * sendLocalUserNotify
		 * 
		 */
		public function sendLocalUserNotify():void
		{
			trace("FuturaMockInterface::sendLocalUserNotify()");
			if (whoAmI!=null)
				whoAmI("1");
			else
				FuturaSafeGuard.fatalError(FuturaStrings.INTERNAL_ERROR,"mock");
		}
		///--------------------------------------------------------------------------------------------------------------------
		///aqui o jogador falso nunca envia send, então nunca é disparada uma terceira pergunta
		/**
		 * sendAnswer
		 * @param answer
		 * @param time
		 * 
		 */
		override public function sendAnswer(answer:String, time:int):void
		{
			serverTurn++;
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * setCallback
		 * @param externalName
		 * @param internalFunc
		 * 
		 */
		override public function setCallback(externalName:String, internalFunc:Function):void
		{
			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * gameEnded
		 * 
		 */
		override public function gameEnded():void
		{
			if (gameEndedCallback != null)
				gameEndedCallback();
		}
		///--------------------------------------------------------------------------------------------------------------------
		override public function call(cmd:String):Object
		{
			if (cmd == "getBackgroundUrl")
				return "http://www.imotion.com.br/imagens/data/media/81/11457f-16.jpg";
			
			return null;
		}
		///--------------------------------------------------------------------------------------------------------------------		
		override public function askForReplay(gameId:String):void
		{
			trace("REPLAY!");
		}	
		///--------------------------------------------------------------------------------------------------------------------		
		override public function notifyTick(clientServerTick:String, gameId:String, f:Function, id:String):void
		{
			super.notifyTick(clientServerTick, gameId, f, id);

			var e:Object;
			var obj:Object;
			var json:String;
			e = new Object()
			trace(e);
			obj = getQuestionObject();
			trace(obj);
			json = JSON.serialize(obj);
			trace(json);
			e.target=new Object;
			e.target.data = json;
			
			f(e);
		}		
		///--------------------------------------------------------------------------------------------------------------------		
	}
}