package  
{
	/**
	 * constantes de estado de jogo 
	 * @author Daniel Monteiro
	 * 
	 */
	public class FuturaGameStates
	{
		/// inicio de tudo
		public static const SYSTEM_ALPHA:int=-1;
		
		///estados de inicialização de jogo		
		public static const HANDSHAKE_PREINIT:int=1;
		public static const HANDSHAKE_INIT:int=2;
//		public static const HANDSHAKE_START:int=3;
		public static const HANDSHAKE_PLAYERLIST:int=4;
		public static const HANDSHAKE_MYID:int=5;
		public static const HANDSHAKE_GAMEREADY:int=6;
		
		///estados de inicio de turno
		public static const STARTTURN_READYFORNEWTURN:int=10;
		public static const STARTTURN_GETNEWQUESTION:int=11;
		
		///estados de meio de turno
		public static const MIDTURN_SHOWMYNEWQUESTION:int=20;
		public static const MIDTURN_SHOWOTHERNEWQUESTION:int=21;
		public static const MIDTURN_NEWQUESTION:int=22;
		public static const MIDTURN_SENDANSWER:int=23;
		
		///fim de turno e aguardo do começo de um novo turno
		public static const ENDTURN_WAITINGFORMYANSWER:int=30;
		public static const ENDTURN_WAITINGFOROTHERANSWER:int=31;
		public static const ENDTURN_SHOWINGANSWER:int=32;
		public static const ENDTURN_WAITINGFORNEWTURN:int=33;
		
		///fim de tudo
		public static const SYSTEM_OMEGA:int=-999;
		
		///fim de jogo
		public static const ENDGAME_SHOWINGRESULTS:int=40;
		
		
		public static function toString(State:int):String
		{
			var toReturn:String=State.toString();

			return toReturn;
		}
	}
}