package  
{
	/**
	 * FuturaSafeGuard
	 * Centraliza os tratamentos de erro
	 * @author Daniel Monteiro
	 * */
	public class FuturaSafeGuard
	{
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * forceAbend 
		 * Força a saída do jogo
		 */
		public static function forceAbend():void
		{
			throw new Error("Force Abend");
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * fatalError
		 * erro fatal - aplicação é encerrada
		 * @param String msg mensagem descrevendo o erro
		 * */
		public static function fatalError(msg:String,where:String):void
		{
			traceMsg("FATAL_ERROR","(@"+where+")"+msg);			
			forceAbend();
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * nonFatalError
		 * erro não fatal - o jogo pode continuar
		 * @param String msg mensagem descrevendo o erro
		 * */
		public static function nonFatalError(msg:String,where:String):void
		{
			traceMsg("NON_FATAL_ERROR","(@"+where+")"+msg);			
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * warnDeveloper
		 * aviso para o desenvolvedor
		 * @param String msg mensagem descrevendo o aviso
		 * */
		public static function warnDeveloper(msg:String,where:String):void
		{
			traceMsg("WARNING!","(@"+where+")"+msg);	
		}
		///--------------------------------------------------------------------------------------------------------------------
		/**
		 * warnDeveloper
		 * aviso para o desenvolvedor
		 * @param String token Um marcador para definir no confuso console do flashbug de qual dos browsers saiu a mensagem 
		 * @param String msg mensagem descrevendo o aviso
		 * */
		public static function traceMsg(token:String,msg:String):void
		{
			trace("[ FuturaSafeGuard "+token+","+msg+"]");	
		}
		///--------------------------------------------------------------------------------------------------------------------		
	}
}