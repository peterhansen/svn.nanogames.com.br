package  
{
	///flash
	
	
	import FuturaGUI.MovieClipWrapper;
	
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;

	/**
	 * FuturaDriver
	 * Stub de aplicação do jogo
	 * @author Daniel
	 * */
	[SWF( width= "530", height = "410", backgroundColor = "0x000000", frameRate = "30" )]	
	public class Application extends Sprite
	{	
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * instância da mesa de jogo
		 */
		private var futuraTable:FuturaTable;
		
		private static var hasSound:Boolean;
		
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * construtor
		 * */
		public function Application()
		{
			super();
			///configura o flash
			
			
			Application.setSoundState(true);
			
						///aloca a mesa de jogo
						futuraTable=new FuturaTable();
						
						if (futuraTable==null)
							FuturaSafeGuard.fatalError(FuturaStrings.ALLOC_ERROR,"Application::Application");
						
						///inicializa e inicia o jogo
						addEventListener(Event.ADDED_TO_STAGE,addedToStage);
						addChild(futuraTable);
		}
		///--------------------------------------------------------------------------------------------------------------------		
		/**
		 * addedToStage
		 * para evitar problemas de stage nulo 
		 * @param monteiro
		 * @return 
		 * 
		 */
		public function addedToStage(e:Event):void
		{
			stage.tabChildren=false;
			stage.quality=StageQuality.BEST;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			removeEventListener(Event.ADDED_TO_STAGE,addedToStage);
			futuraTable.SYS_init();
			futuraTable.GAME_start();
		}
		///--------------------------------------------------------------------------------------------------------------------
		public static function setSoundState(s:Boolean):void
		{
			hasSound=s;
		}
		///--------------------------------------------------------------------------------------------------------------------
		public static function getSoundState():Boolean
		{
			return hasSound;
		}
		///--------------------------------------------------------------------------------------------------------------------
	}
}