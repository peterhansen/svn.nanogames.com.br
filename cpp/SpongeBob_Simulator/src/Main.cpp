//#ifdef _WIN32
//#include <windows.h>
//#endif
//
//
//#include <time.h>
//#include <math.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <GL/glut.h>
//
//#include "Main.h"
//
///* Display mode */
//static bool help = true;  /* help message on/off */
//
///* Global camera parameters */
//static float fovy = 50.0f;
//static float matrix[16] = {1.0f, 0.0f, 0.0f, 0.0f,
//                           0.0f, 1.0f, 0.0f, 0.0f,
//                           0.0f, 0.0f, 1.0f, 0.0f,
//                           0.0f, 0.0f, 0.0f, 1.0f
//                          };
//
//
//static int quantMovimentoEmX = 0, velhoX = 0;
//
//
///*============= Elementos da cena ==========================*/
//static Scene *pScene;// ponteiro para a cena atual
//static PerspectiveCamera *pCamera;
//static Emitter	*pMainEmitter, // emissor-base (lan�a fogos para cima)
//				*pUpEmitter, // emissor da subida
//				*pExplodeEmitter; // emissor da explos�o final
//static Particle *pUpParticle, // part�cula-base da subida
//				*pExplodeParticle; // part�cula-base da explos�o final
//
///*==========================================================*/
//
//
//
//void mouse( int botao, int estado, int x, int y ) {
//	if ( estado == GLUT_DOWN ) {
//		switch ( botao ) {
//			case GLUT_LEFT_BUTTON:
//			velhoX = x;           // armazena a posicao x do mouse
//			default:                // no momento em que o botao foi pressionado
//			break;
//		}
//	}
//}
//
//void mouseMotion( int x, int y) {
//  quantMovimentoEmX = velhoX - x;    // a diferenca entre o valor antigo
//                                     // do mouse e o novo nos fornece
//                                     // a distancia percorrida pelo mouse
//  velhoX = x;
//} 
//
//static void drawscene () {
//	pScene->render();
//
//	//desenha a mesa
//	glColor3f(0.8f,0.5f,0.0f);
//	glNormal3f(0.0f,1.0f,0.0f);
//	glBegin( GL_QUADS );
//
//	float width = 30;
//	float height = 30;
//	float floorHeight = 2*DFLOOR;
//	float floorWidth = 2*DFLOOR;
//	for( float i=0 ; i*height < floorHeight ; i++ )
//	{
//		for( float j=0 ; j*width < floorWidth ; j++ )
//		{
//			glVertex3f( -DFLOOR + ( j*width )        , 0.0f, -DFLOOR + ( i*height ) );
//			glVertex3f( -DFLOOR + ( j*width ) + width, 0.0f, -DFLOOR + ( i*height ) );
//			glVertex3f( -DFLOOR + ( j*width ) + width, 0.0f, -DFLOOR + ( i*height ) + height );
//			glVertex3f( -DFLOOR + ( j*width )        , 0.0f, -DFLOOR + ( i*height ) + height );
//		}
//	}
//	glEnd();
//}
//
//
///* Display help message */
//static void showhelp () {
//	Util::putstring("b: enable/disable spot light",-0.95,-0.43);
//	Util::putstring("PageUp,PageDown,Home,End: rotate camera", -0.95, -0.51);
//	Util::putstring("z,x: enable/disable wireframe view", -0.95, -0.59);  
//	Util::putstring("w,s,a,d,r,f: move camera",-0.95,-0.67);
//	Util::putstring("arrow keys: move Luxor Jr.",-0.95,-0.75);
//	Util::putstring("q,e: roll camera",-0.95,-0.83);
//	Util::putstring("+,-: zoom in/out",-0.95,-0.91);
//	Util::putstring("ESC: exit",-0.95,-0.99);
//}
//
//// Reshape callback
//static void reshape (int w, int h) {
//	float white[4] = {1.0f, 1.0f, 1.0f, 1.0f};
//	glViewport(0,0,w,h);
//	glClearColor(0,0,0,1); 
//	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_LIGHTING);
//	glEnable(GL_COLOR_MATERIAL);
//	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
//	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
//	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50.0);
//	glLightModeli(GL_LIGHT_MODEL_AMBIENT, GL_TRUE);
// }
//
//// Display callback
//static void display (void) {
//	//float	lpos[4] = {0.0f, 0.0f, 0.0f, 1.0f},
//	//		ldir[3] = {0.0f, 0.0f, -1.0f};
//	//int vp[4]; glGetIntegerv(GL_VIEWPORT,vp);
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	pCamera->place();
//	//glMatrixMode(GL_PROJECTION);
//	//glLoadIdentity();
//
//	//gluPerspective(fovy,(float)vp[2]/vp[3],1.0,3000.0);
//	//glMatrixMode(GL_MODELVIEW);
//	//glLoadIdentity( );
// 
//	//glMultMatrixf(matrix);
//	//gluLookAt( 380, 20, 110, 0, 0, 0, 0, 1, 0);
//
//	// atualiza a anima��o do Luxor
//	quantMovimentoEmX = 0;
//
//	// draw scene
//	pScene->render();
//
//	// display help message
//	if (help)
//		showhelp();
//
//	glutSwapBuffers();
//} // fim do m�todo display()
//
///* Idle callback */
//static void idle (void) {
//	pScene->update();
//	display();
//}
//
//// Keyboard callback
//static void keyboard (unsigned char key, int x, int y) {
//	float dx = 0.0f;
//	float dy = 0.0f;
//	float dz = 0.0f;
//	float rz = 0.0f;
//	float lx = 0.0f;
//	float ly = 0.0f;
//	float lz = 0.0f;
//
//	switch (key) {
//		case 'j': glEnable(GL_FOG); break;
//		case 'k': glDisable(GL_FOG); break;
//		case 's': dz -= DL; break;
//		case 'w': dz += DL; break;
//		case 'd': dx -= DL; break;
//		case 'a': dx += DL; break;
//		case 'r': dy -= DL; break;
//		case 'f': dy += DL; break;
//		case 'e': rz -= DA; break;
//		case 'q': rz += DA; break;
//		case 'z': glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
//		case 'x': glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;
//		case '+': fovy -= DZ; break;
//		case '-': fovy += DZ; break;
//		case 'h': case 'H': help ^= 1; break;
//		case 27: exit(0); break;
//	}
//	// accumulate transformation using OpenGL matrix operations
//	glPushMatrix();
//	glLoadIdentity();
//	glTranslatef(dx,dy,dz);
//	glRotatef(rz,0.0f,0.0f,1.0f);
//	glMultMatrixf(matrix);
//	glGetFloatv(GL_MODELVIEW_MATRIX,matrix);
//	glPopMatrix();
//}
//
//// Special keyboard callback
//static void special (int key, int x, int y)
//{
//	float rx = 0.0f;
//	float ry = 0.0f;
//	switch (key) {
//		case GLUT_KEY_HOME:			rx += DA; break;
//		case GLUT_KEY_END:			rx -= DA; break;
//		case GLUT_KEY_PAGE_DOWN:	ry += DA; break;
//		case GLUT_KEY_PAGE_UP:		ry -= DA; break;
//		case GLUT_KEY_UP:    break;
//		case GLUT_KEY_DOWN:  break;
//		case GLUT_KEY_RIGHT: break;
//		case GLUT_KEY_LEFT:  break;
//		default: return;
//	}
//	// accumulate transformation using OpenGL matrix operations
//	glPushMatrix();
//	glLoadIdentity();
//	glRotatef(rx,1.0f,0.0f,0.0f);
//	glRotatef(ry,0.0f,1.0f,0.0f);
//	glMultMatrixf(matrix);
//	glGetFloatv(GL_MODELVIEW_MATRIX,matrix);
//	glPopMatrix();
//
//	display();
//}
//
//int main (int argc, char* argv[]) {
//	// open GLUT 
//	glutInit(&argc, argv);
//	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
//	glutInitWindowSize(W,H); 
//
//	// create window
//	glutCreateWindow ("Peter Hansen");
//	glutReshapeFunc(reshape); 
//	glutDisplayFunc(display); 
//	glutKeyboardFunc(keyboard);
//	glutMouseFunc( mouse );
//	glutMotionFunc( mouseMotion ); 
//	glutSpecialFunc(special);
//	glutIdleFunc(idle);
//
//	if( !init() )
//		return 0;
//
//	// interact... 
//	glutMainLoop();
//
//	delete pScene;
//	delete pCamera;
//	
//	return 0;
//}
//
//bool init() {
//	pUpParticle = new Particle();
//	if (!pUpParticle)
//		goto error;
//
//	pExplodeParticle = new Particle();
//	if (!pExplodeParticle)
//		goto error;
//
//	pExplodeEmitter = new Emitter();
//	if (!pExplodeEmitter)
//		goto error;
//
//	pUpEmitter = new Emitter();
//	if (!pUpEmitter)
//		goto error;
//	pUpEmitter->setBaseElement(pUpParticle);
//
//	pMainEmitter = new Emitter();
//	if (!pMainEmitter)
//		goto error;
//	pMainEmitter->setBaseElement(pUpEmitter);
//
//	Environment *environment = new Environment();
//	if (!environment)
//		goto error;
//
//	World *world = new World();
//	if (!world || world->insertEnvironment(environment) < 0)
//		goto error;
//
//	Point3f	*pCameraCenter = new Point3f(),
//			*pCameraUp = new Point3f(0.0f, 1.0f, 0.0f),
//			*pCameraEye = new Point3f(0.0f, 0.0f, 10.0f);
//
//	if (!pCameraCenter || !pCameraUp || !pCameraEye)
//		goto error;
//
//	pCamera = new PerspectiveCamera(pCameraCenter, pCameraEye, pCameraUp);
//	if (!pCamera)
//		goto error;
//
//	pScene = new Scene();
//	if (!pScene || pScene->insertWorld(world) < 0)
//		goto error;
//	pScene->setCurrentCamera(pCamera);
//
//	return true;
//
//	// label de tratamento de aloca��o de mem�ria
//	error:
//		SAFE_DELETE(pMainEmitter);
//		SAFE_DELETE(pUpEmitter);
//		SAFE_DELETE(pExplodeEmitter);
//		SAFE_DELETE(pUpParticle);
//		SAFE_DELETE(pExplodeParticle);
//		SAFE_DELETE(pCameraCenter);
//		SAFE_DELETE(pCameraUp);
//		SAFE_DELETE(pCameraEye);
//		SAFE_DELETE(pCamera);
//		SAFE_DELETE(pScene);
//		return false;
//} // fim do m�todo init()