#pragma once

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers

#ifdef _WIN32
	#include <windows.h>
#endif


#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <tchar.h>

#include "Cache.h"

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
	#include "tolua.h"
}


#include "base/Scene.h"
#include "base/PerspectiveCamera.h"
#include "base/Emitter.h"
#include "base/Particle.h"
#include "base/Force.h"
#include "base/World.h"
#include "base/Environment.h"
#include "base/Shape.h"
#include "base/Texture2D.h"
#include "base/Sprite.h"
#include "base/Sphere.h"
#include "base/LineEmitter.h"
#include "base/AreaEmitter.h"
#include "base/SoftCube.h"

#include "base/lua/luaAreaEmitter.h"
#include "base/lua/luaCamera.h"
#include "base/lua/luaClock.h"
#include "base/lua/luaColor.h"
#include "base/lua/luaCube.h"
#include "base/lua/luaCubeParticle.h"
#include "base/lua/luaCylinder.h"
#include "base/lua/luaDynamicVector.h"
#include "base/lua/luaEmitter.h"
#include "base/lua/luaEnvironment.h"
#include "base/lua/luaForce.h"
#include "base/lua/luaLight.h"
#include "base/lua/luaLineEmitter.h"
#include "base/lua/luaMain.h"
#include "base/lua/luaMesh.h"
#include "base/lua/luaNode.h"
#include "base/lua/luaParticle.h"
#include "base/lua/luaPerspectiveCamera.h"
#include "base/lua/luaPlane.h"
#include "base/lua/luaPoint.h"
#include "base/lua/luaPoint3f.h"
#include "base/lua/luaQuaternion.h"
#include "base/lua/luaScene.h"
#include "base/lua/luaShape.h"
#include "base/lua/luaSphere.h"
#include "base/lua/luaSoftCube.h"
#include "base/lua/luaSpringCoil.h"
#include "base/lua/luaSpotLight.h"
#include "base/lua/luaSprite.h"
#include "base/lua/luaTexture.h"
#include "base/lua/luaTexture1D.h"
#include "base/lua/luaTexture2D.h"
#include "base/lua/luaWorld.h"


// varia��o na velocidade a cada tecla apertada
#define SPEED_INC 5.0

/* Initial screen dimension */
#define W 640
#define H 480

/* Global camera controls */
#define DL 6.0f     /* linear increment */
#define DA 4.0f     /* angular increment */
#define DZ 0.3f     /* zoom increment */

/* Floor geometry (at y=0 plane) */
#define DFLOOR 1000.0f

bool init( const char *luaFile );

class Main {
	public: 
		static void setScene( Scene *scene, SoftCube *cube );
};