#pragma once

#include "Environment.h"
#include "DynamicVector.h"

class World {
protected:
	DynamicVector	*environments;
	bool buildEnvironmentsVector( void );
public:
	World( void );
	UINT16 insertEnvironment(Environment *environment);
	bool removeEnvironmentAt(UINT16 index);
	Environment *getEnvironmentAt( UINT16 index );
	void update(double time);
	void render( void );
}; // fim da classe World