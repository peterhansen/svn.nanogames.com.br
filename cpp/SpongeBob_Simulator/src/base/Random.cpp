#include "Random.h"

void Random::setSeed(UINT32 s) {
	seed = s;
}

UINT32 Random::getSeed( void ) {
	return seed;
}

double Random::nextDouble( void ) {
	 return (double)rand() / ((double)(RAND_MAX) + 1.0);
}

double Random::nextDouble(double maxValue, double minValue) {
	return minValue + (maxValue - minValue) * (double)rand() / ((double)(RAND_MAX) == 0 ? 1 : (double)(RAND_MAX));
}
