#include "Cylinder.h"

Cylinder::Cylinder( Color *myColor, float mySize, float myHeight, float myBaseRadius, float myTopRadius, int myVertices, Texture *myTexture, UINT8 myLidType )
		: Shape( myColor, mySize ),
		vertices( myVertices ), displayListIndex( -1 ), lidType( myLidType ),
		height( myHeight ), baseRadius( myBaseRadius ), topRadius( myTopRadius )
{
	texture = myTexture;
}

bool Cylinder::build( float initialAngle, float endAngle ) {
	bool success;
	displayListIndex = glGenLists(1);
	glNewList( displayListIndex, GL_COMPILE );
	success = createCylinder( initialAngle, endAngle );
	glEndList();

	if ( !success )
		glDeleteLists( displayListIndex, 1 );

	return success;
}

void Cylinder::draw( void ) {
	if ( texture )
		texture->load();

	glPushMatrix();
	glMatrixMode( GL_MODELVIEW );
	glColor4f( color.r, color.g, color.b, color.a );
	glCallList( displayListIndex );
	glPopMatrix();

	if ( texture )
		texture->unload();
}



bool Cylinder::createCylinder( double theta1, double theta2 )
{
	double	angleDiff = theta2 - theta1;

	double	theta,
			cosTheta,
			sinTheta;

	// TODO otimizar c�lculos, calculando theta, seno e coseno apenas uma vez e armazenando

	glBegin( GL_TRIANGLE_STRIP );
	for ( int i = 0; i <= vertices; i++ ) {
		theta = theta1 + i * angleDiff / vertices;
		cosTheta = cos( theta );
		sinTheta = sin( theta );

		glNormal3f( cosTheta, 0.0, sinTheta );
		glTexCoord2f( i / (double)vertices, 1.0 );
		glVertex3f( cosTheta * topRadius, height, sinTheta * topRadius );

		glNormal3f( cosTheta, 0.0, sinTheta );
		glTexCoord2f( i / (double) vertices, 0.0 );
		glVertex3f( cosTheta * baseRadius, 0, sinTheta * baseRadius );
	}
	glEnd();

	if ( lidType & LID_TOP ) {
		glBegin( GL_TRIANGLE_FAN );
		glNormal3f( 0.0, 1.0, 0.0 );
		glTexCoord2f( 0.5, 0.5 );
		glVertex3f( 0.0, height, 0.0 );

		for ( int i = 0 ; i <= vertices; i++ ) {
			theta = theta1 + i * angleDiff / vertices;
			cosTheta = cos( theta );
			sinTheta = sin( theta );

			glNormal3f( 0.0, 1.0, 0.0 );
			glTexCoord2f( 0.5 + cosTheta / 2.0, 0.5 + sinTheta / 2.0 );
			glVertex3f( cosTheta * topRadius, height, sinTheta * topRadius );
		}
		glEnd();
	}

	if ( lidType & LID_BASE ) {
		glBegin( GL_TRIANGLE_FAN );
		glNormal3f( 0.0, 1.0, 0.0 );
		glTexCoord2f( 0.5, 0.5 );
		glVertex3f( 0.0, 0.0, 0.0 );

		for ( int i = 0 ; i <= vertices; i++ ) {
			theta = theta1 + i * angleDiff / vertices;
			cosTheta = cos( theta );
			sinTheta = sin( theta );

			glNormal3f( 0.0, 1.0, 0.0 );
			glTexCoord2f( 0.5 + cosTheta / 2.0, 0.5 + sinTheta / 2.0 );
			glVertex3f( cosTheta * baseRadius, 0.0, sinTheta * baseRadius );
		}
		glEnd();
	}

	return true;
}


Cylinder::~Cylinder( void ) {
}


Shape * Cylinder::getCopy( void ) {
	Cylinder *s = new Cylinder( &color, size, height, baseRadius, topRadius, vertices, texture );
	if ( s ) {
		s->setDisplayListIndex( displayListIndex );
	}

	return s;
}

void Cylinder::setDisplayListIndex( int myDisplayListIndex ) {
	displayListIndex = myDisplayListIndex;
}

void Cylinder::setLidType( UINT8 myLidType ) {
	lidType = myLidType;
}

UINT8 Cylinder::getLidType( void ) {
	return lidType;
}