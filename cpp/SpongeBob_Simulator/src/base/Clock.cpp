#include "Clock.h"

Clock::Clock( float newXPos, float newYPos )
	:	visible( false ), accTime( 0 ), frames ( 0 ), timeMode( CLOCK_NORMAL_TIME ),
		fixedFrameTime( DEFAULT_FIXED_TIME ), paused( false ), maxFrameTime( -1 )
{
	setPos( newXPos, newYPos );
	_ftime64(&initialTime);
	for ( int i = 0; i < 3 && i < FPS_CHARS; i++ )
		fpsString[i] = '-';
	fpsString[3] = '\0';
} // fim do construtor Clock::Clock()

void Clock::update( void ) {
	if ( !paused ) {
		switch ( timeMode ) {
			case CLOCK_NORMAL_TIME: 
				finalTime = initialTime;
				_ftime64( &initialTime );

				partial = ( initialTime.time - finalTime.time ) 
						+ ( ( initialTime.millitm - finalTime.millitm ) / 1000.0 );

				break;
			case CLOCK_FIXED_TIME:
				partial = fixedFrameTime;
				break;
		}

		if ( maxFrameTime > 0.0 && partial > maxFrameTime )
			partial = maxFrameTime;

		if ( ( accTime += partial ) >= 1.0 ) {
			fps = frames / accTime;
			if (visible)
				_itoa( fps, fpsString, 10 );

			accTime = 0.0;
			frames = 0.0;
		} // fim if ( ( accTime += partial ) >= 1000)

		frames++;
	}
} // fim do m�todo Clock::update()

void Clock::draw( void ) {
	Util::putstring( fpsString, xPos, yPos );
}


inline void Clock::setPos( float newXPos, float newYPos ) {
	xPos = newXPos;
	yPos = newYPos;
}

void Clock::getPos( float *retXPos, float *retYPos ) {
	*retXPos = xPos;
	*retYPos = yPos;
}

void Clock::setVisible( bool v ) {
	visible = v;
}

bool Clock::isVisible( void ) {
	return visible;
}

float Clock::getTime( void ) {
	return partial;
}

void Clock::pause( void ) {
	frames = 0;
	partial = 0;
	paused = true;
}

void Clock::resume( void ) {
	paused = false;
}


void Clock::setTimeMode( UINT8 myTimeMode )
{
	timeMode = myTimeMode;
}

UINT8 Clock::getTimeMode( void ) {
	return timeMode;
}

void Clock::setFixedFrameTime( float fixedTime )
{
	fixedFrameTime = fixedTime;
}

float Clock::getFixedFrameTime( void ) {
	return fixedFrameTime;
}

void Clock::setMaxFrameTime( float maxTime )
{
	maxFrameTime = maxTime;
}

float Clock::getMaxFrameTime( void ) {
	return maxFrameTime;
}