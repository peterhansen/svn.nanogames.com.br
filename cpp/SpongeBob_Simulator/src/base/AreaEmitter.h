#pragma once

#include "Emitter.h"
#include "Shape.h"
#include "Point3f.h"
#include "Plane.h"

class AreaEmitter : public Emitter {
private:
	Plane	plane;	// plano de emiss�o
	float	width,  // largura da �rea de emiss�o
			height;  // profundidade da �rea de emiss�o (a �rea de emiss�o forma um plano, definido pelo �ngulo do emissor)
public:
	AreaEmitter( float emissionRate = 1.0f, Shape *shape = NULL, Point3f *position = NULL, float life = -1.0, float size = 1.0f, float mass = 1.0f, Point3f *speed = NULL, float angle = 0.0, Point3f *axis = NULL, bool active = true );

	virtual bool emit( UINT16 quantity );

	virtual inline void setPlane( Plane *plane, float width, float height );
	virtual inline Plane getPlane( void );

	virtual inline void setWidth( float width );
	virtual inline float getWidth( void );
	virtual inline void setHeight( float height );
	virtual inline float getHeight( void );

}; // fim da classe AreaEmitter