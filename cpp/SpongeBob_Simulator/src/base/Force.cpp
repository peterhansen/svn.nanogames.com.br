#include "Force.h"

Force::Force( Point3f myDirection ) {
	direction = myDirection;
}

void Force::update( double time ) {
}


Point3f * Force::getDirection( void ) {
	return &direction;
}

void Force::setDirection( Point3f *newDirection ) {
	direction = *newDirection;
}

inline Particle * Force::getCopy( void ) {
	return new Force( direction );
}

inline bool Force::setBasedOn( Particle *baseForce ) {
	//direction = ( ( Force * ) baseForce )->getDirection;

	return true;
}