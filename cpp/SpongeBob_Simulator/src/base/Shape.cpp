#include "Shape.h"

Shape::Shape( Color *myColor, float mySize ) {
	setColor( myColor );

	size = mySize;
}

Color Shape::getColor() {
	return color;
}

/*void Shape::draw() {
	glPointSize( size );
	glColor4f( color.r, color.g, color.b, color.a );

	glBegin( GL_POINTS );
	glVertex3f( 0.0, 0.0, 0.0 );
	glEnd();
}*/


void Shape::setSize(float newSize) {
	size = newSize;
}

inline float Shape::getSize( void ) {
	return size;
}

void Shape::setColor( Color *myColor ) {
	if ( myColor )
		color = *myColor;
	else
		color.setColor();
}

/*Shape * Shape::getCopy() {
	return new Shape( &color, size );
}*/


inline void Shape::setTexture( Texture * myTexture) {
	texture = myTexture;
}

inline Texture * Shape::getTexture( void ) {
	return texture;
}