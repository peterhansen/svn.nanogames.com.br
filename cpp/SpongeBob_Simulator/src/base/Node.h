#pragma once 

/*==============================================================================================
CLASSE Node
	Classe que representa um elemento de cena.
==============================================================================================*/

#include "Point3f.h"
#include "..\Cache.h"

class Node {
protected: 
	Point3f	position,
			speed;
	Node *	pivot;

	void loadTransform( void );
	void unloadTransform( void );

public:
	Node( Point3f *myPosition = NULL, Point3f *mySpeed = NULL );

	virtual inline Point3f getPosition( void );
	virtual inline void setPosition( Point3f *position );

	virtual inline Point3f getSpeed( void );
	virtual inline void setSpeed( Point3f *speed );

	virtual void attach( Node *pivot );
	virtual void detach( void );

	virtual inline Node *getPivot( void );
};