#include "Environment.h"

Environment::Environment( void ) {
	forces = NULL;
	particles = NULL;
}

UINT16 Environment::insertParticle( Particle *particle ) {
	if ( !particles ) {
		if ( !buildParticlesVector() ) {
			return -1;
		}
	}
	particles->insert( particle );

	return particles->getUsedLength();
}
bool Environment::removeParticleAt( UINT16 index ) {
	return particles->remove( index );
}


UINT16 Environment::insertForce( Force *force ) {
	if ( !forces ) {
		if ( !buildForcesVector() ) {
			return -1;
		}
	}
	forces->insert( force );

	return forces->getUsedLength();
}

bool Environment::removeForceAt( UINT16 index ) {
	return forces->remove( index );
}


bool Environment::buildParticlesVector( void ) {
	particles = new DynamicVector();

	if ( !particles )
		return false;

	return true;
} // fim do m�todo Environment::buildParticlesVector()

bool Environment::buildForcesVector( void ) {
	forces = new DynamicVector();

	if ( !forces )
		return false;

	return true;
} // fim do m�todo Environment::buildForcesVector()


void Environment::update( double time ) {
	UINT16	particlesLength = particles != NULL ? particles->getUsedLength() : 0,
			forcesLength = forces != NULL ? forces->getUsedLength() : 0;

	// atualiza for�as e calcula a for�a resultante
	Force *f;
	for ( UINT16 i = 0; i < forcesLength; i++ ) {
		f = ( Force * ) forces->getData( i );
		f->update( time );
	}

	// atualiza part�culas
	Particle *p;
	UINT16 activeParticles = particles != NULL ? particles->getUsedLength() : 0;
	for ( UINT16 i = 0; i < particlesLength; i++ ) {
		p = (Particle *) particles->getData(i);
		if ( p && !p->update( time, forces ) ) {
			particles->switchSlots( i, activeParticles -1 );
			activeParticles--;
		} // fim if ( !p->update( time, forces ) )
	} // fim for ( UINT16 i = 0; i < particlesLength; i++ )
} // fim do m�todo World::update()


void Environment::render( void ) {
	UINT16	particlesLength = particles != NULL ? particles->getUsedLength() : 0;

	// renderiza part�culas
	for ( UINT16 i = 0; i < particlesLength; i++ ) {
		Particle *p = ( Particle * ) particles->getData( i );
		if ( p )
			p->draw();
	}

}