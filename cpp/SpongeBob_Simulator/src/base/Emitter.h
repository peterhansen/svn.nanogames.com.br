#pragma once

#include "Particle.h"
#include "DynamicVector.h"
#include "Force.h"

/*
 * Classe que define um emissor pontual de part�culas. Na verdade, � um emissor de 
 * elementos, pois um emissor pode emitir outros emissores.
 */
class Emitter : public Particle {
protected:
	float			emissionRate, // taxa de emiss�o por segundo
					elementsToEmit; // quantidade atual de elementos a ser emitida
	DynamicVector	*elements; // lista de elementos
	UINT16			currentActiveElements; // n�mero de elementos ativos atualmente
	Particle		*baseElement; // elemento-base (modelo) para os elementos emitidos

public:
	Emitter( float emissionRate = 1.0f, Shape *shape = NULL, Point3f *position = NULL, float life = -1.0, float size = 1.0f, float mass = 1.0f, Point3f *speed = NULL, float angle = 0.0, Point3f *axis = NULL, bool active = true );
	virtual ~Emitter( void );
	virtual void draw( void ); // desenha um emissor
	virtual bool update( double time, DynamicVector *forces ); // atualiza o estado emissor ap�s "time" segundos
	virtual bool emit( UINT16 quantity ); // emite "quantity" elementos baseados em "baseElement"

	virtual bool setBaseElement( Particle *baseElement );
	virtual inline Particle * getBaseElement( void );

	virtual void setEmissionRate( float emissionRate );
	virtual inline float getEmissionRate( void );

	virtual bool setBasedOn( Particle *baseElement );
	virtual Particle * getCopy( void );

	virtual inline UINT16 getCurrentActiveElements( void );

}; // fim da classe Emitter
