#pragma once

#include "CubeParticle.h"

#define NO_BREACH -1 // indica que a mola n�o se arrebenta
#define EPSILON	0.005 // erro na precis�o de float aceito

class SpringCoil
{
	public:
	Particle	*p1,
				*p2;
	bool	immortal,
			maintainDirection;
	float	tightness,
			dampingFactor,
			restLength, // comprimento da mola quando est� em repouso
			maxLength,	// comprimento m�ximo da mola (arrebenta caso o ultrapasse)
			life;

	Point3f originalDirection;

	
	SpringCoil( Particle *p1 = NULL, Particle *p2 = NULL, bool maintainDirection = true, float tightness = 0, float dampingFactor = 0, float maxLength = NO_BREACH, float life = -1 );

	bool update( double time ); // retorna se a mola arrebentou ou n�o
	void set( Particle *particle1, Particle *particle2, float tightness, float dampingFactor );

};