#include "Point.h"

Point::Point( Color *myColor, float mySize ) : Shape( myColor, mySize )
{
}

void Point::draw() {
	glPointSize( size );
	glColor4f( color.r, color.g, color.b, color.a );

	glBegin( GL_POINTS );
	glVertex3f( 0.0, 0.0, 0.0 );
	glEnd();
}




Shape * Point::getCopy() {
	return new Point( &color, size );
}