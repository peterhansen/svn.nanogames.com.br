#pragma once

/*==============================================================================================
CLASSE Sphere
	Fornece meios para se desenhar uma esfera.
==============================================================================================*/

#define _USE_MATH_DEFINES
#include <math.h>

#include "Shape.h"
#include "Point3f.h"
#include "Texture.h"

// resolu��o padr�o da esfera
#define SPHERE_RESOLUTION 32

// raio padr�o da esfera
#define SPHERE_DEFAULT_RADIUS 0.5f

#define TWOPI	6.283185307179586476925286766559
#define PID2	1.5707963267948966192313216916398

class Sphere : public Shape {
	protected:
		int displayListIndex;

		int		vertices;

		void createSphere( void );

	public:
		Sphere( float size = SPHERE_DEFAULT_RADIUS, int vertices = SPHERE_RESOLUTION, Texture *texture = NULL );
		virtual ~Sphere( void );

		virtual void draw( void );

		bool build( void );

		Shape *getCopy( void );

		void setDisplayListIndex( int displayListIndex );

};