#pragma once

/*==============================================================================================
CLASSE Light
	Classe para defini��o de luzes ambientes, difusas e especulares.
==============================================================================================*/

#include "Color.h"
#include "Point3f.h"
#include "..\Cache.h"
#include "Node.h"

class Light : public Node {
protected:

	//cor da luz
	Color color;

	//identidade da luz
	UINT16 id;

	//tipo da luz
	GLenum type;
public:
	Light( Point3f *pos, Color color, GLenum lightId, GLenum lightType );
	virtual ~Light( void );

	// determina a cor da luz
	void setColor( Color color );

	// determina o id da luz
	void setId( GLenum lightId );

	// retorna a cor da luz
	Color getColor( void );

	// retorna o id da luz
	GLenum getId( void );

	// habilita a luz
	virtual void enable( void );
};