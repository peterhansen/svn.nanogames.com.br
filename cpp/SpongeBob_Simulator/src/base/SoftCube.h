#ifndef SOFT_CUBE
#define SOFT_CUBE

#include "Node.h"
#include "DynamicVector.h"
#include "CubeParticle.h"
#include "SpringCoil.h"
#include "Cube.h"

//modos de desenho do cubo
#define DRAW_MODE_WIRED 0
#define DRAW_MODE_QUADS 1
#define DRAW_MODE_PARTICLES 2

class SoftCube : public Particle
{
	public:
		//construtor
		SoftCube( Point3f *myPosition = NULL, Point3f *mySpeed = NULL );
		~SoftCube( void );

		//inicializa o componente
		bool build( float dimension, UINT8 nPointsByEdge, float tightness, float damping, float stickingFactor );

		virtual void draw( void );

		virtual bool update( double time, DynamicVector *externForces );

		virtual inline UINT16 insertLimitPlane( Plane * limitPlane );

		void setDrawMode( UINT8 mode );
		UINT8 getDrawMode( void );

		//determina / obt�m a ader�ncia do cubo � superf�cies externas( valores ser�o limitados entre 0 e 1 )
		inline void setStickingFactor( float factor );
		inline float getStickingFactor( void );

		virtual inline void setSpeed( Point3f *speed );

		virtual inline Point3f getPosition( void );
		virtual inline void setPosition( Point3f *position );
		virtual inline void reset( void );

		bool drawTempCoils;

	protected:
		//quantos pontos por aresta nosso cubo ter�
		UINT8 nPointsByEdge;

		//vertices que formam as faces do cubo
		DynamicVector	*pCubeVertices,
						*pTempCoils,
						*pCubeOriginalPositions; //posi��es originais dos n�s para podermos reiniciar o cubo

		//part�culas que v�o causar a ader�ncia do cubo
		CubeParticle **pFacesParticles;

		//array de molas
		SpringCoil	*pCoils;

		UINT16	nParticles,
				numberOfCoils,
				nFacesVertices,
				dimension;

		// cria molas tempor�rias entre os n�s do cubo e center
		bool createTemporaryCoils( CubeParticle *pParticle );

	private:
		//indica o modo de desenho
		UINT8 drawMode;
};

#endif