#pragma once

/*==============================================================================================
CLASSE SpotLight
	Classe para utiliza��o de luz de spot.
==============================================================================================*/

#include "Light.h"

class SpotLight : public Light {
	public:
		SpotLight( Point3f *pos, Color color, GLenum lightId, Point3f *direction, int angle,
			       int atenuationExponent );
		virtual ~SpotLight( void );

		// habilita a luz
		virtual void enable( void );

	private:
		// dire��o da luz de spot
		Point3f direction;

		// �ngulo de abertura da luz
		int cutoffAngle;

		// fator de atenua��o da luz
		int atenuationFactor;
};
