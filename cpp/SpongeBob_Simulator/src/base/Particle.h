#pragma once

#include "Point3f.h"
#include "Shape.h"
#include "..\Cache.h"
#include "DynamicVector.h"
#include "Random.h"
#include "Plane.h"
#include "Node.h"

#define MAX_DEFAULT_LIFE 30000.0
#define MAX_DEFAULT_MASS    80.0
#define MAX_DEFAULT_SIZE    6.0

class Particle : public Node {
protected:
	float		life,		// tempo de vida atual
				size,		// tamanho
				mass;		// massa
	float		angleValue;	// �ngulo da rota��o
	Point3f		angleAxis;	// eixo da rota��o
	bool		active,		// indica se a part�cula est� ativa
				visible,	// indica se a part�cula est� vis�vel
				immortal,	// indica se a part�cula � imortal (n�o morre quando tempo de vida se esgota, mas morre ao ultrapassar o limitPlane, caso esteja definido)
				immune;		// indica se a part�cula � imune a for�as
	Shape		*pShape;	// forma da part�cula (caso seja nula a part�cula n�o ser� desenhada)
	DynamicVector *pLimitPlanes; // planos de validade da part�cula - part�cula morre caso o ultrapasse

	virtual inline Plane * isOutsidePlanes( void );

public:
	Point3f force; // for�a resultante

	Particle( Shape *shape = NULL, Point3f *position = NULL, float life = MAX_DEFAULT_LIFE, float size = 1.0f, float mass = 1.0f, Point3f *speed = NULL, float angle = 0.0, Point3f *axis = NULL, bool active = true );
	virtual ~Particle( void );

	virtual bool update( double time, DynamicVector *forces );
	virtual void draw( void );

	virtual inline void setActive( bool active );
	virtual inline bool isActive( void );

	virtual inline void setShape( Shape *pMyShape );
	virtual inline Shape* getShape( void );

	virtual inline float getLife( void );
	virtual inline void setLife( float life );

	virtual inline float getSize( void );
	virtual inline void setSize( float size );

	virtual inline float getMass( void );
	virtual inline void setMass( float mass );

	virtual inline float getAngle( void );
	virtual inline void setAngle( float angle );

	virtual inline Point3f getAxis( void );
	virtual inline void setAxis( Point3f *axis );

	virtual inline bool isImmune( void );
	virtual inline void setImmune ( bool immune );

	virtual inline bool isImmortal( void );
	virtual inline void setImmortal ( bool immortal );

	virtual inline void setVisible( bool visible );
	virtual inline bool isVisible( void );

	virtual bool setBasedOn( Particle *baseParticle );
	virtual inline Particle * getCopy( void );

	virtual inline Plane * getLimitPlaneAt( UINT16 index );
	virtual inline bool removeLimitPlaneAt( UINT16 index );
	virtual inline UINT16 insertLimitPlane( Plane * limitPlane );
	virtual inline void setLimitPlanes( DynamicVector *limitPlanes );
}; // fim da classe Particle