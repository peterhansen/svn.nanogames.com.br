#include "Cube.h"

Cube::Cube( float width, float height, float depth, Texture *myTexture ) : Shape() {
	texture = myTexture;
	myDisplayListIndex = -1;

	myWidth = width;
	myHeight = height;
	myDepth = depth;

	generate();
}

Cube::~Cube()
{
	if( myDisplayListIndex != -1 )
		glDeleteLists( myDisplayListIndex, 1 );
	SAFE_DELETE( texture );
}

void Cube::draw( void ) {
	if( !myWidth && !myHeight && !myDepth )
		return;

	if ( texture ) {
		texture->load();

		glEnable( GL_BLEND );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	}
	glShadeModel( GL_FLAT );
	glColor4f( color.r, color.g, color.b, color.a );
	glScalef( myWidth, myHeight, myDepth );
	glCallList( myDisplayListIndex );
	glShadeModel( GL_SMOOTH );

	if ( texture )
		texture->unload();
} // fim do m�todo Cube::draw()

void Cube::generate( void )
{
	myDisplayListIndex = glGenLists(1);
	glNewList( myDisplayListIndex, GL_COMPILE );

	float x, y, z, inc = 1.0;
	y = 0.5;
	// cima
	glNormal3f(0.0f, 1.0f, 0.0f);
	for (x = -0.5f; x < 0.5f; x += inc) {
		glBegin(GL_TRIANGLE_STRIP);
		for (z = -0.5f; z < 0.5f; z += inc) {
			glTexCoord2f (0.0, 0.0); glVertex3f(x, y, z);
			glTexCoord2f (1.0, 0.0); glVertex3f(x + inc, y, z);
			glTexCoord2f (0.0, 1.0); glVertex3f(x, y, z + inc);
			glTexCoord2f (1.0, 1.0); glVertex3f(x + inc, y, z + inc);
		}
		glEnd();
	}
	
	// baixo
	y = -0.5f;
	glNormal3f(0.0f, -1.0f, 0.0f);
	for (x = -0.5f; x < 0.5f; x += inc) {
		glBegin(GL_TRIANGLE_STRIP);
		for (z = -0.5f; z < 0.5f; z += inc) {
			glTexCoord2f (0.0, 0.0); glVertex3f(x, y, z);
			glTexCoord2f (0.0, 1.0); glVertex3f(x + inc, y, z);
			glTexCoord2f (1.0, 0.0); glVertex3f(x, y, z + inc);
			glTexCoord2f (1.0, 1.0); glVertex3f(x + inc, y, z + inc);
		}
		glEnd();
	}

	// frente
	z = -0.5f;
	glNormal3f(0.0f, 0.0f, -1.0f);
	for (x = -0.5f; x < 0.5f; x += inc) {
		glBegin(GL_TRIANGLE_STRIP);
		for (y = -0.5f; y < 0.5f; y += inc) {
			glTexCoord2f (0.0, 0.0); glVertex3f(x, y, z);
			glTexCoord2f (1.0, 0.0); glVertex3f(x + inc, y, z);
			glTexCoord2f (0.0, 1.0); glVertex3f(x, y + inc, z);
			glTexCoord2f (1.0, 1.0); glVertex3f(x + inc, y + inc, z);
		}
		glEnd();
	}
	
	// tr�s
	z = 0.5f;
	glNormal3f(0.0f, 0.0f, 1.0f);
	for (x = -0.5f; x < 0.5f; x += inc) {
		glBegin(GL_TRIANGLE_STRIP);
		for (y = -0.5f; y < 0.5f; y += inc) {
			glTexCoord2f (0.0, 0.0); glVertex3f(x, y, z);
			glTexCoord2f (0.0, 1.0); glVertex3f(x + inc, y, z);
			glTexCoord2f (1.0, 0.0); glVertex3f(x, y + inc, z);
			glTexCoord2f (1.0, 1.0); glVertex3f(x + inc, y + inc, z);
		}
		glEnd();
	}
	
	// esquerda
	x = -0.5f;
	glNormal3f(-1.0f, 0.0f, 0.0f);
	for (y = -0.5f; y < 0.5f; y += inc) {
		glBegin(GL_TRIANGLE_STRIP);
		for (z = -0.5f; z < 0.5f; z += inc) {
			glTexCoord2f (0.0, 0.0); glVertex3f(x, y, z);
			glTexCoord2f (1.0, 0.0); glVertex3f(x, y, z + inc);
			glTexCoord2f (0.0, 1.0); glVertex3f(x, y + inc, z);
			glTexCoord2f (1.0, 1.0); glVertex3f(x, y + inc, z + inc);
		}
		glEnd();
	}
	
	// direita
	x = 0.5f;
	glNormal3f(1.0f, 0.0f, 0.0f);
	for (y = -0.5f; y < 0.5f; y += inc) {
		glBegin(GL_TRIANGLE_STRIP);
		for (z = -0.5f; z < 0.5f; z += inc) {
			glTexCoord2f (0.0, 0.0); glVertex3f(x, y, z);
			glTexCoord2f (1.0, 0.0); glVertex3f(x, y, z + inc);
			glTexCoord2f (0.0, 1.0); glVertex3f(x, y + inc, z);
			glTexCoord2f (1.0, 1.0); glVertex3f(x, y + inc, z + inc);
		}
		glEnd();
	}


	glEndList();

}


Shape * Cube::getCopy() {
	Cube *cube = new Cube( myWidth, myHeight, myDepth, texture );
	if ( cube )
		return cube;

	return NULL;
}