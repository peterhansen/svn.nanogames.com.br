#include "Texture1D.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Texture1D::Texture1D( void ) : Texture()
{
	pData = NULL;
	size = 0;

	if( !counter )
	{
		glDisable( GL_TEXTURE_1D );
	}
	counter++;
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Texture1D::~Texture1D( void )
{
	SAFE_DELETE( pData );

	counter--;
	if( !counter )
	{
		glDisable( GL_TEXTURE_1D );
	}
}

/*==============================================================================================

FUN��O setTexture
	Determina o array que representar� a textura 1D.

==============================================================================================*/

bool Texture1D::setTexture( void *pDataArray, UINT arraySize, size_t dataSize )
{
	//se j� possu�a textura carregada, deleta
	if( pData )
		delete pData;

	UINT sizeInBytes = arraySize * dataSize;
	pData = malloc( sizeInBytes );
	if( !pData )
		return false;

	//copia os dados do array passado como par�metro
	memmove( pData, pDataArray, sizeInBytes );

	//obt�m um id para a textura
	glGenTextures( 1, ( UINT* )&myId );

	return true;
}

/*==============================================================================================

FUN��O getSize
	Retorna o tamanho do array da textura 1D.

==============================================================================================*/

UINT Texture1D::getSize( void )
{
	return size;
}

/*==============================================================================================

FUN��O getTextureData
	Retorna o array da textura 1D.

==============================================================================================*/

const void* Texture1D::getTextureData( void )
{
	return pData;
}

/*==============================================================================================

FUN��O load
	Carrega a textura.

==============================================================================================*/

void Texture1D::load( void )
{
	glBindTexture( GL_TEXTURE_1D, getTextureId() );
	glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ); 
	glTexParameteri( GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexImage1D( GL_TEXTURE_1D, 0, GL_RGBA, size, 0, GL_RGBA, GL_FLOAT, pData );
}

/*==============================================================================================

FUN��O unload
	Retira a textura.

==============================================================================================*/

void Texture1D::unload( void )
{
	glBindTexture( GL_TEXTURE_1D, NULL );
}


Texture * Texture1D::getCopy() {
	Texture1D *t = new Texture1D();
	if ( t ) {
		t->setTexture( pData, size, sizeof( dataType ) );
	}

	return t;
}