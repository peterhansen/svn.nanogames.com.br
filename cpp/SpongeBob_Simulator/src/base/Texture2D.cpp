#include "Texture2D.h"

/*==============================================================================================

CONSTRUTOR

==============================================================================================*/

Texture2D::Texture2D( void ) : Texture() {
	width = 0;
	height = 0;

	pData = NULL;

	if( !counter() )
	{
		glEnable( GL_TEXTURE_2D );
	}
	counter( 1 );
}

/*==============================================================================================

DESTRUTOR

==============================================================================================*/

Texture2D::~Texture2D( void )
{
	SAFE_DELETE( pData );

	counter( -1 );
	if( !counter() ) {
		glDisable( GL_TEXTURE_2D );
	}
}

/*==============================================================================================

FUN��O LoadTexture
	Carrega a textura bitmap de um arquivo.

==============================================================================================*/

//bool Texture2D::loadTexture( char *pFileName )
//{
//	//abre o arquivo para leitura
//	FILE *pFile;
//	pFile = fopen( pFileName, "rb" );
//	if( pFile == NULL )
//		return false;
//
//	//l� o cabe�alho do bitmap
//	BITMAPFILEHEADER bmpFileHeader;
//	fread( &bmpFileHeader, sizeof( bmpFileHeader ), 1, pFile );
//
//	//verifica se o arquivo � realmente um bitmap
//	if( bmpFileHeader.bfType != BITMAP_ID )
//	{
//		fclose( pFile );
//		return false;
//	}
//
//	//l� a parte de informa��o do bitmap
//	BITMAPINFOHEADER bmpInfoHeader;
//	fread( &bmpInfoHeader, sizeof( BITMAPINFOHEADER ), 1, pFile );
//
//	//obt�m a altura e largura da imagem
//	width = bmpInfoHeader.biWidth;
//	height = bmpInfoHeader.biHeight;
//
//	//move o ponteiro de arquivo para o in�cio dos dados dos pixels
//	fseek( pFile, bmpFileHeader.bfOffBits, SEEK_SET );
//
//	//se existir mem�ria alocada para o buffer, desaloca-a
//	if( pData )
//	{
//		delete pData;
//		pData = NULL;
//	}
//
//	DWORD sizeInBytes = width * height * 3;
//
//	//aloca mem�ria para conter uma c�pia dos dados da imagem
//	pData = new BYTE[ sizeInBytes ];
//	if( pData == NULL )
//	{
//		fclose( pFile );
//		return false;
//	}
//
//	//preenche o buffer
//	if( fread( pData, sizeof( BYTE ), sizeInBytes, pFile ) != sizeInBytes )
//	{
//		fclose( pFile );
//		return false;
//	}
//
//	fclose( pFile );
//
//	//j� que est� armazenado em BGR, inverte os bytes
//	BYTE aux;
//	for( UINT i=0 ; i < sizeInBytes ; i+=3 )
//	{
//		aux = pData[i];
//		pData[i] = pData[i+2];
//		pData[i+2] = aux;
//	}
//	
//	//obt�m um id para a textura
//	glGenTextures( 1, ( UINT* )&myId );
//
//	return true;
//}

/*==============================================================================================
FUN��O getTextureData
	Retorna o array de cores da textura.
==============================================================================================*/

const void* Texture2D::getTextureData( void ) {
	return pData;
}

/*==============================================================================================
FUN��O getSize
	Retorna a largura e a altura da textura.
==============================================================================================*/

void Texture2D::getSize( UINT *pWidth, UINT *pHeight )
{
	if( pWidth )
		*pWidth = width;
	if( pHeight )
		*pHeight = height;
}

/*==============================================================================================
FUN��O load
	Carrega a textura.
==============================================================================================*/

void Texture2D::load( void ) {
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glBindTexture( GL_TEXTURE_2D, myId );
	//glBindTexture( GL_TEXTURE_2D, getTextureId() );
	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	//glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	//glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, pData );
}

/*==============================================================================================
FUN��O unload
	Retira a textura.
==============================================================================================*/

void Texture2D::unload( void ) {
	glBindTexture( GL_TEXTURE_2D, NULL );
	glDisable( GL_BLEND );
}

UINT16 Texture2D::counter( UINT16 change ) {
	static UINT16 counter = 0;
	UINT16 ret = counter;
	counter += change;
	
	return ret;
}


//-----------------------------------------------------------------------------
// Name: loadTexture()
// Desc: 
//-----------------------------------------------------------------------------
bool Texture2D::loadTexture( char *filename, Color *transparent ) {
	if ( transparent )
		transparentColor = *transparent;

    AUX_RGBImageRec *pImage_RGB = auxDIBImageLoad( filename );
    unsigned char *pImage_RGBA = NULL;

    if( pImage_RGB != NULL ) {
        int imageSize_RGB  = pImage_RGB->sizeX * pImage_RGB->sizeY * 3;
        int imageSize_RGBA = pImage_RGB->sizeX * pImage_RGB->sizeY * 4;

        // allocate buffer for a RGBA image
        pImage_RGBA = new unsigned char[imageSize_RGBA];
		if ( pImage_RGBA ) {

			//
			// Loop through the original RGB image buffer and copy it over to the 
			// new RGBA image buffer setting each pixel that matches the key color
			// transparent.
			//

			int i, j;

			for( i = 0, j = 0; i < imageSize_RGB; i += 3, j += 4 ) {
				// Does the current pixel match the selected color key?
				if( transparent && (
						pImage_RGB->data[i]   == transparentColor.r &&
						pImage_RGB->data[i+1] == transparentColor.g &&
						pImage_RGB->data[i+2] == transparentColor.b )
					)
				{
					pImage_RGBA[j+3] = 0;   // If so, set alpha to fully transparent.
				} else {
					pImage_RGBA[j+3] = 255; // If not, set alpha to fully opaque.
				}

				pImage_RGBA[j]   = pImage_RGB->data[i];
				pImage_RGBA[j+1] = pImage_RGB->data[i+1];
				pImage_RGBA[j+2] = pImage_RGB->data[i+2];
			}

			glGenTextures( 1, &myId );
			glBindTexture( GL_TEXTURE_2D, myId );
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			// Don't forget to use GL_RGBA for our new image data... we support Alpha transparency now!
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, pImage_RGB->sizeX, pImage_RGB->sizeY, 0,
						GL_RGBA, GL_UNSIGNED_BYTE, pImage_RGBA );
		}
	} else {
		return false;
	}

    if( pImage_RGB ) {
        if( pImage_RGB->data )
            SAFE_DELETE( pImage_RGB->data );

        SAFE_DELETE( pImage_RGB );
    }

    if( pImage_RGBA )
        SAFE_DELETE( pImage_RGBA );

	return true;
}

Texture * Texture2D::getCopy() {
	Texture2D *t = new Texture2D();
	if ( t ) {
		t->setSize( width, height );
		t->myId = myId;

	}

	return t;
}

void Texture2D::setSize( UINT myWidth, UINT myHeight ) {
	width = myWidth;
	height = myHeight;
}