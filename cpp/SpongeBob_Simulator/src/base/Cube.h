#pragma once

/*==============================================================================================
CLASSE Cube
	Fornece meios para se desenhar um cubo.
==============================================================================================*/

#include "..\Cache.h"
#include "Shape.h"
#include "Point3f.h"
#include "Texture.h"

// tamanho padr�o da aresta do cubo
#define CUBE_DEFAULT_SIZE 1.0

class Cube : public Shape {
	public:
		Cube( float width = CUBE_DEFAULT_SIZE, float height = CUBE_DEFAULT_SIZE, float depth = CUBE_DEFAULT_SIZE, Texture *texture = NULL );
		virtual ~Cube( void );

		virtual void draw( void );

		virtual Shape *getCopy();

	protected:
		float myWidth, myHeight, myDepth;

		Point3f cubeVertexes[14];
		Point3f cubeNormals[14];

		int myDisplayListIndex;

		void generate( void );
};