#pragma once

#include "..\Cache.h"

/*==============================================================================================
CLASSE Texture
	Superclasse de texturas.
==============================================================================================*/

class Texture {
	public:
		Texture( void );
		virtual ~Texture( void );

		UINT getTextureId( void );

		virtual const void* getTextureData( void )=0;

		// carrega a textura
		virtual void load( void ) = 0;

		// retira a textura
		virtual void unload( void ) = 0;

		virtual Texture *getCopy( void ) = 0;

	protected:

		UINT myId;
};
