#pragma once

#include "Shape.h"
#include "Texture.h"

class Sprite : public Shape {
	public:
		static Sprite *createSprite( Texture *texture, float width, float height );
		virtual ~Sprite( void );
		virtual void draw( void );

		virtual Shape *getCopy();

		inline void setWidth( float width );
		inline void setHeight( float height );
		float getWidth( void );
		float getHeight( void );

	private:
		Sprite();

		float width, height;
};