#include "Camera.h"

/*==============================================================================================
CONSTRUTOR
==============================================================================================*/

Camera::Camera( Point3f *myCenter, Point3f *myEye, Point3f *myUp ) 
		: Node( myCenter )
{
	eye = *myEye;
	up = *myUp;
}

/*==============================================================================================
DESTRUTOR
==============================================================================================*/

Camera::~Camera( void )
{
}

/*==============================================================================================
FUN��O place
	Seta os atributo da c�mera determinando sua posi��o na cena.
==============================================================================================*/

void Camera::place( void ) {
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	if ( pivot ) {
		Point3f pivotPosition = pivot->getPosition();
		switch ( attachType ) {
			case POSITION:
				pivotPosition += position;
				gluLookAt( eye.x, eye.y, eye.z, pivotPosition.x, pivotPosition.y, pivotPosition.z, up.x, up.y, up.z );
				break;
			case EYE:
				pivotPosition += eye;
				gluLookAt( pivotPosition.x, pivotPosition.y, pivotPosition.z, position.x, position.y, position.z, up.x, up.y, up.z );
				break;
			case UP:
				pivotPosition += up;
				gluLookAt( eye.x, eye.y, eye.z, position.x, position.y, position.z, pivotPosition.x, pivotPosition.y, pivotPosition.z );
				break;
		}
	} else {
		gluLookAt( eye.x, eye.y, eye.z, position.x, position.y, position.z, up.x, up.y, up.z );
	}
}


void Camera::move(Point3f *offset) {
	eye.x += offset->x;
	eye.y += offset->y;
	eye.z += offset->z;
	position.x += offset->x;
	position.y += offset->y;
	position.z += offset->z;
}

void Camera::rotate( Point3f *angle ) {
	position.x -= angle->x;
	position.y += angle->y;
	position.z -= angle->z;
} // fim do m�todo Camera::rotate()


void Camera::attach( Node *pivot, UINT16 myAttachType ) {
	Node::attach( pivot );

	attachType = myAttachType;
}