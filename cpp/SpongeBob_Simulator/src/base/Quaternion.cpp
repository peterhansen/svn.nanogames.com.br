#include "Quaternion.h"

Quaternion :: ~Quaternion() {
}

Quaternion::Quaternion( float newW, float newX, float newY, float newZ ) {
	angle = newW;
	x = newX;
	y = newY;
	z = newZ;
}



void Quaternion::normalize( void ) {
	const float norm = (x * x) + (y * y) + (z * z) + (angle * angle);
	x /= norm;
	y /= norm;
	z /= norm;
	angle /= norm;
}

void Quaternion::EulerToQuaternion(float rot_x, float rot_y, float rot_z) {
	float rx, ry, rz, ti, tj, tk;
	static Quaternion	qx,
						qy,
						qz,
						qf;
	// ANGLES TO RADIANS
	rx = (rot_x * (float)M_PI) / (360 / 2);
	ry = (rot_y * (float)M_PI) / (360 / 2);
	rz = (rot_z * (float)M_PI) / (360 / 2);
	// HALF ANGLES
	ti = rx * (float)0.5;
	tj = ry * (float)0.5;
	tk = rz * (float)0.5;
	qx.x = ((float)sin(ti));
	qx.y = ( 0.0);
	qx.z = ( 0.0);
	qx.angle = ((float)cos(ti));
	qy.x = ( 0.0 );
	qy.y = ( (float)sin(tj));
	qy.z = ( 0.0 );
	qy.angle = ((float)cos(tj));
	qz.x = ( 0.0 );
	qz.y = ( 0.0 );
	qz.z = ( (float)sin(tk));
	qz.angle = ( (float)cos(tk));
	qf = qx * qy * qz;
	*this *= qf;
	normalize();
} // fim do m�todo EulerToQuaternion()

void Quaternion::interpolate_slerp(Quaternion *quat1,Quaternion *quat2, float slerp) {
	/*double omega,cosom,sinom,scale0,scale1;
	// Encontrando o cos entre os quaternios
	cosom = quat1->x * quat2->x +	quat1->y * quat2->y +	quat1->z * quat2->z +	quat1->angle * quat2->angle;
	if ((1.0 + cosom) > 0.0001)	{
		// Evitando divisao por zero
		if ((1.0 - cosom) > 0.0001) {
			//SLERP
			omega = acos(cosom);
			sinom = sin(omega);
			scale0 = sin((1.0 - slerp) * omega) / sinom;
			scale1 = sin(slerp * omega) / sinom;
		} else {
			//LERP Diferenca muito pequena
			scale0 = 1.0 - slerp;
			scale1 = slerp;
		}
		this->x =  ( (float) (scale0 * quat1->x + scale1 * quat2->x));
		this->y =  ( (float) (scale0 * quat1->y + scale1 * quat2->y));
		this->z =  ( (float) (scale0 * quat1->z + scale1 * quat2->z));
		this->angle = ( (float) (scale0 * quat1->angle + scale1 * quat2->angle));
	} else {
		// praticamente opostos
		// fazendo slerp pelo perpendicular
		this->x = (-quat2->y);
		this->y = ( quat2->x);
		this->z = (-quat2->angle);
		this->angle = ( quat2->z);
		scale0 = sin((1.0 - slerp) * (float)HALF_PI);
		scale1 = sin(slerp * (float)HALF_PI);
		this->x = ( (float) (scale0 * quat1->x + scale1 * this->x));
		this->y = ( (float) (scale0 * quat1->y + scale1 * this->y));
		this->z = ( (float) (scale0 * quat1->z + scale1 * this->z));
		this->angle = ( (float) (scale0 * quat1->angle + scale1 * this->angle));
	}*/

	float cos_theta, theta, sin_theta;

	*quat1 = algQuatUnit(*quat1);
	*quat2 = algQuatUnit(*quat2);
	  
	cos_theta  = quat1->angle*quat2->angle + quat1->x*quat2->x + quat1->y*quat2->y + quat1->z*quat2->z; 

	if ( cos_theta < 0 )
	{
		cos_theta = -cos_theta;
		*quat2 *= -1;
	}

	theta      = (float) acos(cos_theta);
	sin_theta  = (float) sin(theta);

	if ( (1 - cos_theta) > 1e-6 ) {
		 Quaternion ret =	*quat1 * ((float) sin((1 - slerp) * theta) / sin_theta) +
							*quat2 * ((float) sin(slerp * theta) / sin_theta);

		 this->set_Quaternion(ret.angle, ret.x, ret.y, ret.z );
	} else { /* theta � quase 0 */
		/* Evita divis�o por valor muito pequeno, aproveitando-se de 
		que sin(theta) � aprox. igual a theta, com theta muito pequeno.. */
		
		Quaternion ret = *quat1 * (1 - slerp) + (*quat2 * slerp);
		this->set_Quaternion(ret.angle, ret.x, ret.y, ret.z );
	}
}


Quaternion Quaternion::algQuatUnit(Quaternion q1) {
  Quaternion q=q1;
  float n = (float) sqrt(q1.angle * q1.angle + q1.x * q1.x + q1.y * q1.y + q1.z * q1.z);
  if (n > 1e-9) {
      q.angle = q1.angle / n;
      q.x = q1.x / n;
      q.y = q1.y / n;
      q.z = q1.z / n;
  }
  return q;
}


void Quaternion::toEuler( float *eulerAngle, Point3f *axis ) {
	const float acosAngle = acos(angle),
				scale = sin(acosAngle); //outra possibilidade: scale = sqrt(x * x + y * y + z * z) (a primeira foi escolhida por realizar menos c�lculos)
				
	//angle = 2 * acosAngle;
	//axis->x = x / scale;
	//axis->y = y / scale;
	//axis->z = z / scale;

	*eulerAngle = 2 * acosAngle;
	axis->x = x / scale;
	axis->y = y / scale;
	axis->z = z / scale;

}


/*==============================================================================================
	M�todos get e set
==============================================================================================*/
void Quaternion::set_Quaternion(float w, float x, float y, float z) {
   angle = w ;
   x = x;
   y = y;
   z = z;
}

void Quaternion::set_Axis(float x, float y, float z) {
   x = x;
   y = y;
   z = z;
}

/*==============================================================================================
	Sobrecargas de operadores
==============================================================================================*/
bool Quaternion::operator == (const Quaternion &q) {
	return ( (this->x == q.x) && (this->y == q.y) && (this->z == q.z) && (this->angle == q.angle));
}

bool Quaternion::operator != (const Quaternion &q) {
	return ( (this->x != q.x) || (this->y != q.y) || (this->z != q.z) || (this->angle != q.angle));
}

Quaternion& Quaternion::operator *= ( const Quaternion &q ) {
	x = (angle * q.x + x * q.angle + y * q.z - z * q.y);
	y = (angle * q.y + y * q.angle + z * q.x - x * q.z);
	z = (angle * q.z + z * q.angle + x * q.y - y * q.x);
	angle = (angle * q.angle - x * q.x - y * q.y - z * q.z);

	return *this;
}

Quaternion Quaternion::operator * ( const Quaternion &q ) {
	// c�digo do Gattass
	//angle = (quat1->angle *quat2->angle  - quat1->x *quat2->x  - quat1->y *quat2->y  - quat1->z *quat2->z);
	//x = (quat1->angle *quat2->x  + quat2->angle *quat1->x  + quat1->y *quat2->z  - quat1->z *quat2->y);
	//y = (quat1->angle *quat2->y  + quat2->angle *quat1->y  + quat1->z *quat2->x  - quat1->x *quat2->z);
	//z = (quat1->angle *quat2->z  + quat2->angle *quat1->z  + quat1->x *quat2->y  - quat1->y *quat2->x);
	Quaternion ret;

	ret.x = (angle * q.x + x * q.angle + y * q.z - z * q.y);
	ret.y = (angle * q.y + y * q.angle + z * q.x - x * q.z);
	ret.z = (angle * q.z + z * q.angle + x * q.y - y * q.x);
	ret.angle = (angle * q.angle - x * q.x - y * q.y - z * q.z);

	return ret;
}

Quaternion& Quaternion::operator += ( const Quaternion &q ) {
	angle += q.angle;
	x += q.x;
	y += q.y;
	z += q.z;

	return *this;
}

Quaternion Quaternion::operator + (const Quaternion &q) {
	Quaternion ret;

	ret.angle = angle + q.angle;
	ret.x = x + q.x;
	ret.y = y + q.y;
	ret.z = z + q.z;

	return ret;
}

Quaternion& Quaternion::operator *= (float d) {
	angle *= d;
	x *= d;
	y *= d;
	z *= d;

	return *this;
}

Quaternion Quaternion::operator * (float d) {
	Quaternion ret;

	ret.angle = angle * d;
	ret.x = x * d;
	ret.y = y * d;
	ret.z = z * d;

	return ret;
}