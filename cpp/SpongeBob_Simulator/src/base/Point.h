#pragma once

#include "Shape.h"

#define DEFAULT_SIZE 3.0

class Point : public Shape {
public:
	Point( Color *color = NULL, float size = DEFAULT_SIZE );
	virtual void draw( void );

	virtual Shape * getCopy();
}; // fim da classe Shape
