#include "DynamicVector.h"

/*===========================================================================================

CONSTRUTOR

============================================================================================*/

DynamicVector::DynamicVector( UINT16 initialSlots, float growTax )
{
	nUsedSlots = 0;

	nSlots = initialSlots;
	myGrowTax = growTax;

	pVector = NULL;
}

/*===========================================================================================

DESTRUTOR

============================================================================================*/

DynamicVector::~DynamicVector( void )
{
	if( pVector )
	{
		for( int i=0 ; i < nUsedSlots ; i++ )
		{
			SAFE_DELETE( pVector[i] );
		}
		delete[] pVector;
	}
}

/*===========================================================================================

FUN��O insert
	Insere os dados passados no slot com o �ndice passado como par�metro. deleteIfUsed indica
se os dados devem ser deletados caso o slot j� esteja ocupado ou se o n�mero de slots do vetor
deve ser aumentado.

============================================================================================*/

bool DynamicVector::insert( void *pData )
{
	if ( !pData )
		return false;

	//se � a primeira inser��o...
	if( !pVector )
	{
		//aloca o vetor com o n�mero inicial de slots
		pVector = new void*[ nSlots ];
		if( !pVector )
			return false;

		//inicializa o vetor
		SecureZeroMemory( pVector, sizeof( void* ) * nSlots );
	}

	//se o �ndice for maior do que o �ltimo, utiliza realloc
	if( nUsedSlots >= nSlots )
	{
		// corre��o no c�lculo no caso de o vetor possuir apenas 1 elemento e growTax < 2.0, pois (int) (nUsedSlots * myGrowTax) ainda retornava 1.
		const int newSlotsNumber = ((int) (nUsedSlots * myGrowTax)) == nSlots ? nSlots + 1 : (int) (nUsedSlots * myGrowTax);

		void **pTemp;
		pTemp = ( void ** ) realloc( pVector, sizeof( void* ) * newSlotsNumber );
		if( !pTemp )
			return false;

		pVector = pTemp;
		nSlots = newSlotsNumber;
	}
	
	//insere os dados no vetor
	pVector[nUsedSlots] = pData;

	//incrementa o n�mero de slots utilizados
	nUsedSlots++;

	return true;
}

/*===========================================================================================

FUN��O remove
	Remove os dados do slot com o �ndice passado como par�metro.

============================================================================================*/

bool DynamicVector::remove( UINT16 index )
{
	if( !pVector || index >= nUsedSlots )
		return false;

	//esvazia o slot
	SAFE_DELETE( pVector[ index ] );

	//copia o �ltimo elemento no slot removido
	pVector[ index ] = pVector[ nUsedSlots - 1 ];
	pVector[ nUsedSlots - 1 ] = NULL;

	//atualiza o n�mero de slots usados do vetor
	nUsedSlots--;

	return true;
}

/*===========================================================================================

FUN��O switchSlots
	Troca os slots do vetor passados como par�metro de lugar.

============================================================================================*/

bool DynamicVector::switchSlots( UINT16 slot1, UINT16 slot2 )
{
	if( slot1 >= nUsedSlots || slot2 >= nUsedSlots )
		return false;

	void *pTemp = pVector[ slot1 ];
	pVector[ slot1 ] = pVector[ slot2 ];
	pVector[ slot2 ] = pTemp;

	return true;
}

/*===========================================================================================

FUN��O getData
	Retorna os dados do slot com o �ndice passado como par�metro.

============================================================================================*/

void* DynamicVector::getData( UINT16 index )
{
	if( !pVector || index >= nUsedSlots )
		return NULL;
	return pVector[index];
}

/*===========================================================================================

FUN��O getLength
	Retorna o n�mero total de slots do vetor.

============================================================================================*/

UINT16 DynamicVector::getLength( void )
{
	return nSlots;
}

/*===========================================================================================

FUN��O getUsedLength
	Retorna o n�mero de slots utilizados do vetor.

============================================================================================*/

UINT16 DynamicVector::getUsedLength( void )
{
	return nUsedSlots;
}