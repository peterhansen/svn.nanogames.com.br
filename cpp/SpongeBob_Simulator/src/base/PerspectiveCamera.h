#pragma once

/*==============================================================================================
CLASSE PerspectiveCamera
	Define a implementa��o de uma c�mera perspectiva.
==============================================================================================*/

#include "Camera.h"

#define DEFAULT_FOVY	50.0
#define DEFAULT_ZNEAR	1.0
#define DEFAULT_ZFAR	3000.0

class PerspectiveCamera : public Camera {
	public:

		PerspectiveCamera( Point3f *myCenter, Point3f *myEye, Point3f *myUp, float fovy = DEFAULT_FOVY, 
			               float zNear = DEFAULT_ZNEAR, float zFar = DEFAULT_ZFAR);

		virtual ~PerspectiveCamera( void );

		// define uma c�mera para o grafo de cena. Retorna se criou ou n�o uma c�mera.
		virtual void place( void );

	protected:
		// atributos da c�mera
		float fovy, zNear, zFar;
};