#pragma once

/*==============================================================================================
CLASSE Camera
	Define a implementa��o da c�mera da cena.
==============================================================================================*/

#include "Point3f.h"
#include "..\Cache.h"
#include "Node.h"

enum {
	POSITION,
	EYE,
	UP
};

class Camera : public Node {
	public:
		Camera( Point3f *myCenter, Point3f *myEye, Point3f *myUp );
		virtual ~Camera( void );

		// define os atributos da c�mera determinando sua posi��o na cena
		virtual void place( void );
		virtual void move( Point3f *offset );
		virtual void rotate( Point3f *angle );

		virtual void attach( Node *pivot, UINT16 attachType );

	protected:
		// atributos da c�mera
		Point3f	eye,
				up;

		UINT16 attachType;
};
