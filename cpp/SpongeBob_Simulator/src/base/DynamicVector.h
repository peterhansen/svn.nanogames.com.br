#ifndef DYNAMICVECTOR_H
#define DYNAMICVECTOR_H

#include <windows.h>
#include "..\Cache.h"

#define DEFAULT_GROW_TAX 2.0f

/*===========================================================================================
CLASSE DynamicVector
	Implementa um vetor din�mico gen�rico n�o ordenado. S� � permitido inserir novos
elementos no fim do vetor.
============================================================================================*/

class DynamicVector
{
	public:
		DynamicVector( UINT16 initialSlots = 1, float growTax = DEFAULT_GROW_TAX);
		virtual ~DynamicVector( void );

		// insere os dados passados no �ltimo slot do vetor
		bool insert( void *pData );

		// remove os dados do slot com o �ndice passado como par�metro
		bool remove( UINT16 index );

		// troca os slots do vetor passados como par�metro de lugar
		bool switchSlots( UINT16 slot1, UINT16 slot2 );

		// retorna os dados do slot com o �ndice passado como par�metro
		void* getData( UINT16 index );

		// retorna o n�mero de slots do vetor
		UINT16 getLength( void );

		// retorna o n�mero de slots utilizados do vetor
		UINT16 getUsedLength( void );

	protected:

		//ponteiro para o vetor din�mico
		void **pVector;

		//n�mero de slots usados do vetor
		UINT16 nUsedSlots;

		//n�mero total de slots do vetor
		UINT16 nSlots;

		// indica a taxa de crescimento dos slots
		float myGrowTax;
};

#endif