#pragma once

#include "Point3f.h"
#include "Texture.h"
#include "..\Cache.h"
#include "Shape.h"

#include <string>
#include <fstream>
using namespace std;


class Mesh : public Shape {
protected:
	Texture	*texture;

	bool loadMesh( char *filename );
	Mesh( Point3f *center );

public:
	Point3f	center;
	float	*vertices;
	float	*normals;
	int		*trianglesIndex;
	
	int nVertices,
		nNormals,
		nTriangles;

	static Mesh *createMesh( char *filename, Point3f *center = NULL );
	~Mesh( void );

	virtual Shape *getCopy();

	virtual void draw( void );
};
