#include "Node.h"

Node::Node( Point3f *myPosition, Point3f *mySpeed ) : pivot( NULL )
{
	if ( mySpeed )
		speed = *mySpeed;
	else
		speed.setPoint();

	if ( myPosition )
		position = *myPosition;
	else
		position.setPoint();
}

inline Point3f Node::getPosition( void ) {
	return position;
}

inline void Node::setPosition( Point3f *newPosition ) {
	position = *newPosition;
}

inline Point3f Node::getSpeed( void ) {
	return speed;
}

inline void Node::setSpeed( Point3f *newSpeed ) {
	speed = *newSpeed;
}

void Node::attach( Node *newPivot ) {
	pivot = newPivot;
}

void Node::detach( void ) {
	pivot = NULL;
}

inline Node * Node::getPivot( void ) {
	return pivot;
}

void Node::loadTransform( void ) {
	if ( pivot ) {
		glPushMatrix();
		const Point3f pivotPosition = pivot->getPosition();
		glTranslatef( pivotPosition.x, pivotPosition.y, pivotPosition.z );
		//glTranslatef( rotationRef.x, rotationRef.y, rotationRef.z );
		//glRotatef( angle, axis.x, axis.y, axis.z );
		//glRotatef( angle, 1.0f, 0.0f, 0.0f );
		//glTranslatef( -rotationRef.x, -rotationRef.y, -rotationRef.z );
	}
}

void Node::unloadTransform( void ) {
	if ( pivot ) {
		glPopMatrix();
	}
}