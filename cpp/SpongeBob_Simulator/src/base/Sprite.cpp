#include "Sprite.h"

Sprite *Sprite::createSprite( Texture *texture, float myWidth, float myHeight ) {
	if ( texture ) {
		Sprite * sprite = new Sprite();
		if ( sprite ) {
			sprite->setTexture( texture );
			sprite->setWidth( myWidth );
			sprite->setHeight( myHeight );
			
			return sprite;
		} // fim if ( sprite )
	} // fim if ( texture )

	return NULL;
} // fim do "construtor" createSprite


Sprite::Sprite() : Shape() {
	texture = NULL;
}

Sprite::~Sprite() {
	SAFE_DELETE( texture );
}

void Sprite::draw( void ) {
	if( !width && !height )
		return;

	if ( texture )
 		texture->load();

	glShadeModel( GL_FLAT );
	glColor4f( color.r, color.g, color.b, color.a );
	glScalef( width, height, 1.0 );

	glBegin( GL_QUADS );

	glTexCoord2f (0.0, 0.0); glVertex3f( -0.5, -0.5, 0.0 );
	glTexCoord2f (0.0, 1.0); glVertex3f( -0.5, 0.5, 0.0 );
	glTexCoord2f (1.0, 1.0); glVertex3f( 0.5, 0.5, 0.0 );
	glTexCoord2f (1.0, 0.0); glVertex3f( 0.5, -0.5, 0.0 );

	glEnd();

	glShadeModel( GL_SMOOTH );

	if ( texture )
		texture->unload();
}

inline void Sprite::setWidth( float myWidth ) {
	width = myWidth;
}

inline void Sprite::setHeight( float myHeight ) {
	height = myHeight;
}

float Sprite::getWidth( void ) {
	return width;
}

float Sprite::getHeight( void ) {
	return height;
}


Shape * Sprite::getCopy( void ) {
	Sprite *s = createSprite( texture, width, height );

	if ( s ) {
		s->setColor( &color );
		s->setSize( size );
	}

	return s;
}