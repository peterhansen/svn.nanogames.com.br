#pragma once

#include "Shape.h"
#include "Point3f.h"
#include "..\Cache.h"
#include "Particle.h"

class Force : public Particle {
protected:
	Point3f	direction;
public:
	Force( Point3f direction );
	void update( double time );

	void setDirection( Point3f *direction );
	Point3f * getDirection( void );
	virtual inline Particle * getCopy( void );
	virtual inline bool setBasedOn( Particle *baseForce );
}; // fim da classe Force