#include "Plane.h"

Plane::Plane( Point3f *myTop, Point3f *myRight, float d ) : d( d ) {
	if ( myRight )
		right = *myRight;
	else
		right.setPoint( 1.0 );

	if ( myTop )
		top = *myTop;
	else
		top.setPoint( 0.0, 0.0, -1.0 );

	normal = right % top;
	normal.normalize();

}