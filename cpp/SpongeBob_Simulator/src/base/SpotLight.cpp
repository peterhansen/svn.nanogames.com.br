#include "SpotLight.h"

/*===========================================================================================
CONSTRUTOR
============================================================================================*/

SpotLight::SpotLight( Point3f *pos, Color color, GLenum lightId, Point3f *myDirection, int angle,
			          int atenuationExponent ) 
		: Light( pos, color, lightId, GL_DIFFUSE ),
		cutoffAngle( angle ), atenuationFactor( atenuationExponent )
{
	if ( myDirection )
		direction = *myDirection;
	else
		direction.setPoint( 0.0, -1.0 ); // o padr�o � luz virada para baixo

	direction.normalize();
}

/*===========================================================================================
DESTRUTOR
============================================================================================*/

SpotLight::~SpotLight( void ) {
}

/*===========================================================================================
CONSTRUTOR
	Habilita a luz.
============================================================================================*/

void SpotLight::enable( void ) {
	Light::enable();

	// determina a dire��o da luz de spot
	float d[3] = { direction.x, direction.y, direction.z };
	glLightfv( id, GL_SPOT_DIRECTION, d );

	// determina a distribui��o da intensidade da luz
	glLighti( id, GL_SPOT_EXPONENT, atenuationFactor );

	// graus de abertura da ilumina��o
	glLighti( id, GL_SPOT_CUTOFF, cutoffAngle );

	unloadTransform();
}