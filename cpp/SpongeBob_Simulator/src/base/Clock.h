#pragma once

#include <sys/timeb.h>
#include "..\Util.h"

#define DEFAULT_XPOS	-0.95
#define DEFAULT_YPOS	0.8
#define FPS_CHARS		10

#define DEFAULT_FIXED_TIME	0.07 // tempo fixo padr�o (em segundos)

#define CLOCK_FIXED_TIME	0
#define CLOCK_NORMAL_TIME	1

class Clock {
	/* usados para marcar o tempo */
protected:
    struct __timeb64	initialTime,
						finalTime;
	double	accTime,
			partial,
			frames;
	float	fps,
			xPos,			// posi��o x onde � escrito o contador de fps (0 a 1)
			yPos,			// posi��o y onde � escrito o contador de fps (0 a 1)
			fixedFrameTime,	// tempo de cada frame (caso esteja no modo fixo)
			maxFrameTime;	// maior passo de tempo poss�vel de ser dado a cada frame
	char	fpsString[FPS_CHARS];

	bool	visible, // indica se o contador de fps � mostrado na tela
			paused;
	UINT8	timeMode; // indica se o tempo de cada frame � fixo ou de acordo com a velocidade de processamento
public:
	Clock( float xPos = DEFAULT_XPOS, float yPos = DEFAULT_YPOS );
	float getTime(void );
	void pause( void );
	void resume( void );
	void update( void );
	void draw( void );

	inline void setPos( float xPos, float yPos );
	void getPos( float *xPos, float *yPos );

	void setTimeMode( UINT8 timeMode );
	UINT8 getTimeMode( void );

	void setFixedFrameTime( float fixedFrameTime );
	float getFixedFrameTime( void );

	void setMaxFrameTime( float maxFrameTime );
	float getMaxFrameTime( void );

	void setVisible( bool visible );
	bool isVisible( void );
}; // fim da classe Clock