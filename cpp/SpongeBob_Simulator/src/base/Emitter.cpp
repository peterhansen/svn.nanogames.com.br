#include "Emitter.h"

Emitter::Emitter( float myEmissionRate, Shape *pMyShape, Point3f *myPosition, float myLife, float mySize, float myMass, Point3f *mySpeed, float myAngle, Point3f *myAxis, bool myActive ) 
				: Particle( pMyShape, myPosition, myLife, mySize, myMass, mySpeed, myAngle, myAxis, myActive ),
				emissionRate( myEmissionRate ), elements( NULL ), baseElement( NULL ), currentActiveElements( 0 ),
				elementsToEmit( 0.0 )
{
}

Emitter::~Emitter( void ) {
	SAFE_DELETE( elements );
	SAFE_DELETE( baseElement );
}

bool Emitter::emit( UINT16 quantity ) {
	if ( baseElement ) {
		for( UINT16 i = 0 ; i < quantity ; i++ ) {
			if ( currentActiveElements + 1 >= elements->getUsedLength() ) {
				// n�mero de part�culas ativas � maior do que o tamanho m�ximo atual do vetor -> aumenta tamanho m�ximo do vetor
				Particle *pCopy = baseElement->getCopy( );
				if ( !pCopy || !elements->insert( pCopy ) )	{
					SAFE_DELETE( pCopy );
					return false;
				}	
			}
			Particle *pParticle = ( Particle* )elements->getData( currentActiveElements );
			if( pParticle ) {
				currentActiveElements++;
				pParticle->setBasedOn( baseElement );
				pParticle->setPosition( &position );
				//pParticle->setSpeed( &( pParticle->getSpeed() + speed ) );
				//pParticle->getSpeed() += speed;
			}
		}

		elementsToEmit -= quantity;
		if ( elementsToEmit < 0.0 )
			elementsToEmit = 0.0;

		return true;
	} // fim if ( baseElement )
	return false;
}

bool Emitter::update( double time, DynamicVector *forces ) {
	if ( !Particle::update( time, forces ) ) {
		// emissor p�ra de emitir novas part�culas
		elementsToEmit = 0.0;
		emissionRate = 0.0;
		if ( currentActiveElements == 0 ) // se todas suas part�culas-filhas morrerem, emissor morre
			return false;
	}
	if ( baseElement ) {
		Particle *e;
		for( int i = 0; i < currentActiveElements; /* sem i++ */ ) {
			e = (Particle *) elements->getData(i);
			if ( !e->update( time, forces ) ) {
				// elemento morreu; reorganiza lista
				elements->switchSlots( i, currentActiveElements -1 );
				currentActiveElements--;
			} else {
				i++;
			}
		}

		elementsToEmit += time * emissionRate;
		if ( elementsToEmit >= 1.0 )
			if ( !emit( (UINT16) elementsToEmit ) )
				return false;
	}

	return true;
}

void Emitter::draw( void ) {
	Particle::draw();

	for ( UINT16 i = 0; i < currentActiveElements; i++ ) {
		Particle *p = (Particle *) elements->getData(i);
		if ( p )
			p->draw();
	}
}

bool Emitter::setBaseElement( Particle *newBaseElement ) {
	if ( newBaseElement ) {
		baseElement = newBaseElement;
		
		elements = new DynamicVector();
		if( !elements || !elements->insert( baseElement->getCopy() ) )
			return false;

		return true;
	}

	return false;
}

inline Particle * Emitter::getBaseElement( void ) {
	return baseElement;
}

void Emitter::setEmissionRate( float newEmissionRate ) {
	emissionRate = newEmissionRate;
}

inline float Emitter::getEmissionRate( void ) {
	return emissionRate;
}

Particle * Emitter::getCopy( void ) 
{
	Emitter *pCopy = new Emitter( emissionRate, NULL, &position, getLife(), size, mass, &speed, angleValue, &angleAxis, active );
	if ( pCopy ) {
		pCopy->setBaseElement( baseElement );
		pCopy->setShape( pShape->getCopy() );
	}

	return pCopy;
}

bool Emitter::setBasedOn( Particle *baseParticle ) {
	Particle::setBasedOn( baseParticle );

	emissionRate = ( ( Emitter* )baseParticle )->getEmissionRate();
	elementsToEmit = 0.0;
	currentActiveElements = 0;

	return true;
}

inline UINT16 Emitter::getCurrentActiveElements( void ) {
	return currentActiveElements;
}
