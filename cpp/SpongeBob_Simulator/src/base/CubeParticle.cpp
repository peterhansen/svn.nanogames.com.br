#include "CubeParticle.h"
#include <stdio.h>

#define MIN_NEIGHBORS 6

CubeParticle::CubeParticle( Point3f *myPosition, Point3f *mySpeed, float stickingFactor ) : 
			Particle( ), nTempCoils( 0 )
{
	if ( mySpeed )
		speed = *mySpeed;
	else
		speed.setPoint();

	if ( myPosition )
		position = *myPosition;
	else
		position.setPoint();

	mass = 10.0;

	this->stickingFactor = stickingFactor;
}

CubeParticle::~CubeParticle( void ) {
	SAFE_DELETE( pNeighbors );
}

bool CubeParticle::build( void ) {
	pNeighbors = new DynamicVector( MIN_NEIGHBORS );

	return ( pNeighbors != NULL );
}


bool CubeParticle::update( double time, DynamicVector *externForces ) {
	
	bool collided = false;

	position += speed * time;

	if ( !immune ) {
		if ( externForces ) { // se part�cula n�o � imune a for�as, sofre a a��o das for�as recebidas como par�metro
			const UINT16 length = externForces->getUsedLength();
			Force *f;
			for ( UINT16 i = 0; i < length; i++ ) { // varre o vetor de for�as
				f = (Force *) externForces->getData( i );
				if ( f->isActive() ) {
					speed += *( f->getDirection() ) * ( time / mass );
				}
			}
		}
		speed += force * ( time / mass );
	}

	Plane *p = isOutsidePlanes();
	if ( p ) {
		position -= p->normal * ( ( position ^ p->normal ) - p->d );
		speed.setPoint();
		collided = true;
	}

	// zera a for�a resultante
	force.setPoint();

	return collided;
}

inline UINT16 CubeParticle::getNumberOfNeighbors( void ) {
	if ( pNeighbors )
		return pNeighbors->getUsedLength();

	return 0;
}

inline bool CubeParticle::insertNeighbor( CubeParticle * pParticle ) {
	if ( pNeighbors ) {
		return pNeighbors->insert( pParticle );
	}

	return false;
}


inline bool CubeParticle::removeNeighbor( UINT16 index ) {
	if ( pNeighbors ) {
		return pNeighbors->remove( index );
	}

	return false;
}

inline CubeParticle* CubeParticle::getNeighborAt( UINT16 index )
{
	if ( pNeighbors ) {
		return ( CubeParticle* )pNeighbors->getData( index );
	}

	return NULL;
}

inline void CubeParticle::increaseTempCoils( void )
{
	nTempCoils++;
}

inline void CubeParticle::decreaseTempCoils( void )
{
	//se o n�mero de molas tempor�rias chegou a 0, volta a considerar as for�as aplicadas
	if ( nTempCoils >= 1 ) {
		nTempCoils--;
		if ( nTempCoils == 0 )
			setImmune( false );
	}
}

inline UINT8 CubeParticle::getNTempCoils( void )
{
	return nTempCoils;
}

inline void CubeParticle::setStickingFactor( float factor )
{
	//limita os valores de factor entre 0 e 1
	if( factor < 0 ) factor = 0;
	else if( factor > 1 ) factor = 1;
	
	stickingFactor = factor;
}

inline float CubeParticle::getStickingFactor( void )
{
	return stickingFactor;
}
