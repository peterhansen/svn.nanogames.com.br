#include "World.h"

World::World( void ) {
	environments = NULL;
} // fim do construtor World::World()

UINT16 World::insertEnvironment(Environment *environment) {
	if (!environments) {
		if (!buildEnvironmentsVector()) {
			return -1;
		}
	}
	environments->insert(environment);

	return environments->getUsedLength();
} // fim do m�todo World::insertEnvironment()


bool World::buildEnvironmentsVector( void ) {
	environments = new DynamicVector();

	if (!environments)
		return false;

	return true;
} // fim do m�todo World::buildEnvironmentsVector()

void World::update(double time) {
	UINT16 length = environments->getUsedLength();
	for (UINT16 i = 0; i < length; i++) {
		( (Environment *) environments->getData(i) )->update(time);
	}
} // fim do m�todo World::update()

void World::render( void ) {
	UINT16 length = environments->getUsedLength();
	for (UINT16 i = 0; i < length; i++) {
		( (Environment *) environments->getData(i) )->render();
	}

}

bool World::removeEnvironmentAt( UINT16 index ) {
	return environments->remove( index );
}

Environment *World::getEnvironmentAt( UINT16 index ) {
	return ( Environment * ) environments->getData( index );
}