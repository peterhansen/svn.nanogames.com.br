#pragma once

#include <math.h>

/*==============================================================================================
	CLASSE Point3f
		Define um ponto no espa�o 3D e suas opera��es.
==============================================================================================*/

class Point3f
{
	public:
		float x, y, z;

		//construtor
		Point3f( float x = 0.0f, float y = 0.0f, float z = 0.0f );

		//inicializa um ponto
		inline void setPoint( float x = 0.0f, float y = 0.0f, float z = 0.0f );

		// retorna o m�dulo
		float getModule( void );

		void normalize( void );

		//operadores bin�rios
		Point3f operator + ( const Point3f& ) const;
		Point3f operator - ( const Point3f& ) const;
		Point3f operator * ( float ) const;
		Point3f operator / ( float ) const;
		Point3f operator % ( const Point3f& ) const;
		float	operator ^ ( const Point3f& ) const;

		//operadores de atribui��o
		Point3f& operator += ( const Point3f& );
		Point3f& operator -= ( const Point3f& );
		Point3f& operator *= ( float );
		Point3f& operator /= ( float );

		//operadores l�gicos
		bool operator == ( const Point3f& ) const;
		bool operator != ( const Point3f& ) const;
};