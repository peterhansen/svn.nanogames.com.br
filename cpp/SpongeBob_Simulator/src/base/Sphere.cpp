#include "Sphere.h"

Sphere::Sphere( float mySize, int myVertices, Texture *myTexture ) 
		: Shape(),
		vertices( myVertices ), displayListIndex( -1 )
{
	size = mySize;
	texture = myTexture;
}

bool Sphere::build( void ) {
	displayListIndex = glGenLists(1);
	glNewList( displayListIndex, GL_COMPILE );
	createSphere();
	glEndList();

	return true;
}

void Sphere::draw( void ) {
	if ( texture )
		texture->load();

	glPushMatrix();
	glMatrixMode( GL_MODELVIEW );
	glColor4f( color.r, color.g, color.b, color.a );
	glCallList( displayListIndex );
	glPopMatrix();

	if ( texture )
		texture->unload();
}



void Sphere::createSphere( void ) {
	float r = 1.0f;
	int i, j;
	double theta1, theta2, theta3;
	Point3f e, p;

	// se vertices < 4, desenha um ponto
	if (vertices < 0)
		vertices = -vertices;
	if (vertices < 4) {
		glBegin(GL_POINTS);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glEnd();
		return;
	}

	for (j = 0; j < vertices/2; j++) {
      theta1 = j * TWOPI / vertices - PID2;
      theta2 = (j + 1) * TWOPI / vertices - PID2;

	  glBegin(GL_TRIANGLE_STRIP);
      for (i = 0; i <= vertices; i++) {
         theta3 = i * TWOPI / vertices;

         e.x = cos(theta2) * cos(theta3);
         e.y = sin(theta2);
         e.z = cos(theta2) * sin(theta3);
         p.x = r * e.x;
         p.y = r * e.y;
         p.z = r * e.z;

         glNormal3f(e.x, e.y, e.z);
         glTexCoord2f(i / (double) vertices, 2*(j+1) / (double) vertices);
         glVertex3f(p.x, p.y, p.z);

         e.x = cos(theta1) * cos(theta3);
         e.y = sin(theta1);
         e.z = cos(theta1) * sin(theta3);
         p.x = r * e.x;
         p.y = r * e.y;
         p.z = r * e.z;

         glNormal3f(e.x, e.y, e.z);
         glTexCoord2f(i / (double) vertices, 2*j / (double) vertices);
         glVertex3f(p.x, p.y, p.z);
      }
      glEnd();
   }
}


Sphere::~Sphere( void ) {
}


Shape * Sphere::getCopy( void ) {
	Sphere *s = new Sphere( size, vertices, texture );
	if ( s ) {
		s->setDisplayListIndex( displayListIndex );
		s->setColor( &color );
	}

	return s;
}

void Sphere::setDisplayListIndex( int myDisplayListIndex ) {
	displayListIndex = myDisplayListIndex;
}