#pragma once

/*==============================================================================================
CLASSE Texture1D
	Classe para utiliza��o de texturas 1D.
==============================================================================================*/

#include "Texture.h"

class Texture1D : public Texture {
	public:
		Texture1D( void );
		virtual ~Texture1D( void );

		// determina o array que representar� a textura 1D
		bool setTexture( void *pDataArray, UINT arraySize, size_t dataSize );

		// retorna o tamanho do array da textura 1D
		UINT getSize( void );

		// retorna o array da textura 1D
		virtual const void* getTextureData( void );

		// carrega a textura
		virtual void load( void );

		// retira a textura
		virtual void unload( void );

		virtual Texture *getCopy( void );

	private:
		// vari�vel de controle de texturas 1D
		// TODO counter deve ser static 
		UINT16 counter;

		// array da textura 1D
		void *pData;
		GLenum dataType;

		// tamanho do array da textura 1D
		UINT size;
};

// inicializa a vari�vel est�tica da classe
//UINT16 Texture1D::counter = 0;
