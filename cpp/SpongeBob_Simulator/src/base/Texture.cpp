#include "Texture.h"

Texture::Texture( void )
{
	myId = -1;
}

Texture::~Texture( void )
{
	if( myId != -1 )
		glDeleteTextures( 1, &myId );
}

UINT Texture::getTextureId( void )
{
	return myId;
}