#include "PerspectiveCamera.h"

/*==============================================================================================
CONSTRUTOR
==============================================================================================*/

PerspectiveCamera::PerspectiveCamera( Point3f *myCenter, Point3f *myEye, Point3f *myUp, float fovy,
									  float zNear, float zFar ) : Camera( myCenter, myEye, myUp ) {
	this->fovy = fovy;
	this->zNear = zNear;
	this->zFar = zFar;
}

/*==============================================================================================
DESTRUTOR
==============================================================================================*/

PerspectiveCamera::~PerspectiveCamera( void ) {
}

/*==============================================================================================
FUN��O setup
	Define uma c�mera para o grafo de cena.
==============================================================================================*/

void PerspectiveCamera::place( void ) {
	// obt�m o viewport( x, y, width, height )
	static int viewport[4];
	glGetIntegerv( GL_VIEWPORT, viewport );

	// define a c�mera
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if ( viewport[3] ) // evita problemas de divis�o por zero ao minimizar (todos os valores de viewport s�o zerados quando janela � minimizada)
		gluPerspective( fovy, (float) viewport[2] / viewport[3], zNear, zFar );

	Camera::place();
}