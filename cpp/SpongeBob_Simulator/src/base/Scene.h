#pragma once

#include "World.h"
#include "Clock.h"
#include "DynamicVector.h"
#include "..\Cache.h"
#include "Camera.h"
#include "Clock.h"
#include "Light.h"

class Scene {
protected:
	DynamicVector	*worlds,
					*lights;
	bool buildWorldsVector( void );
	bool buildLightsVector( void );
	Clock clock;
	Camera *currentCamera;
public:
	Scene( void );
	~Scene( void );

	UINT16 insertWorld( World *world );
	bool removeWorldAt( UINT16 index );
	World * getWorldAt( UINT16 index );

	UINT16 insertLight( Light *light );
	bool removeLightAt( UINT16 index );
	Light * getLightAt( UINT16 index );

	virtual void render( void );
	virtual void update( void );

	Clock *getClock( void );

	void setCurrentCamera( Camera *camera );
	Camera * getCurrentCamera( void );


}; // fim da classe Scene