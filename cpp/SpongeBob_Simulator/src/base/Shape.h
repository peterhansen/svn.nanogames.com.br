#pragma once

#include "Color.h"
#include "Point3f.h"
#include "..\Cache.h"
#include "Random.h"
#include "Texture.h"

#define DEFAULT_SIZE 3.0

class Shape {
protected:
	Color color;
	float size;
	Texture *texture;

public:
	Shape( Color *color = NULL, float size = DEFAULT_SIZE );
	virtual void draw( void ) = 0;

	virtual Shape *getCopy() = 0;

	virtual Color getColor( void );
	virtual void setColor( Color *color = NULL );
	virtual void setSize( float size );
	virtual inline void setTexture( Texture * texture);
	virtual inline Texture *getTexture( void );

	virtual inline float getSize( void );
}; // fim da classe Shape
