#pragma once

#define M_PI	3.1415926535897932384626433832795
#define HALF_PI	1.5707963267948966192313216916398

#include <Math.h>

#include "Point3f.h"

#define DEFAULT_X 0.0
#define DEFAULT_Y 0.0
#define DEFAULT_W 0.0
#define DEFAULT_Z 0.9999999999

class Quaternion {

public:
	Quaternion( float w = DEFAULT_W, float x = DEFAULT_X, float y = DEFAULT_Y, float z = DEFAULT_Z );
	~Quaternion();

	void set_Axis(float x, float y, float z);
	void set_Quaternion( float w = DEFAULT_W, float x = DEFAULT_X, float y = DEFAULT_Y, float z = DEFAULT_Z );

	// sobrecargas de operadores
	bool operator == (const Quaternion&);
	bool operator != (const Quaternion&);

	Quaternion& operator *= (const Quaternion&);
	Quaternion operator * (const Quaternion&);
	Quaternion& operator *= (float);
	Quaternion operator * (float);
	Quaternion& operator += (const Quaternion&);
	Quaternion operator + (const Quaternion&);

	void interpolate_slerp(Quaternion *quat1, Quaternion *quat2, float slerp);
	void EulerToQuaternion(float rot_x, float rot_y, float rot_z);
	void normalize( void );

	void toEuler(float *angle, Point3f *axis);

	float	x, /**x = ux *	sen(angulo/2)*/
			y, /**y = uy *	sen(angulo/2)*/
			z, /**z = uz *	sen(angulo/2)*/
			angle; /**w = cos( angulo/2 )*/
private:
	Quaternion algQuatUnit(Quaternion q1); 
};