#pragma once


#include "..\Cache.h"
#include "Math.h"
#include <cstdlib>

class Random {
protected:
	UINT32 seed;
public:
	void setSeed(UINT32 s);
	UINT32 getSeed( void );

	static double nextDouble( void );
	static double nextDouble(double maxValue, double minValue = 0.0);
}; // fim da classe Random