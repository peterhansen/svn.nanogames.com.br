#pragma once

#include "Emitter.h"

class LineEmitter : public Emitter {
private:
	Point3f		endPoint; // ponto de fim da linha (ponto de in�cio � dado por position)
public:
	LineEmitter( float emissionRate = 1.0f, Shape *shape = NULL, Point3f *position = NULL, float life = -1.0, float size = 1.0f, float mass = 1.0f, Point3f *speed = NULL, float angle = 0.0, Point3f *axis = NULL, bool active = true );
	virtual bool emit( UINT16 quantity );

	void setEndPoint( const Point3f endPoint );
	Point3f getEndPoint( void );
}; // fim da classe LineEmitter