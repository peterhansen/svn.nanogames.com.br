#pragma once

/*==============================================================================================
CLASSE Cylinder
	Fornece meios para se desenhar um cilindro ou cone.
==============================================================================================*/

#define _USE_MATH_DEFINES
#include <math.h>

#include "Shape.h"
#include "Point3f.h"
#include "Texture.h"

// resolu��o padr�o do cilindro
#define CYLINDER_RESOLUTION 12

// raio padr�o do cilindro
#define CYLINDER_DEFAULT_RADIUS 1.0
#define CYLINDER_DEFAULT_HEIGHT 2.0
#define CYLINDER_DEFAULT_SIZE 1.0
#define CYLINDER_DEFAULT_INITIAL_ANGLE 0.0
#define CYLINDER_DEFAULT_END_ANGLE TWOPI

#define TWOPI	6.283185307179586476925286766559
#define PID2	1.5707963267948966192313216916398


#define LID_NONE	0
#define LID_BASE	1
#define LID_TOP		2
#define LID_ALL		3

class Cylinder : public Shape {
	public:
		Cylinder( Color *color = NULL, float size = CYLINDER_DEFAULT_SIZE, float height = CYLINDER_DEFAULT_HEIGHT, float baseRadius = CYLINDER_DEFAULT_RADIUS, float topRadius = CYLINDER_DEFAULT_RADIUS, int vertices = CYLINDER_RESOLUTION, Texture *texture = NULL, UINT8 lidType = LID_ALL );
		virtual ~Cylinder( void );

		virtual void draw( void );

		bool build( float initialAngle = CYLINDER_DEFAULT_INITIAL_ANGLE, float endAngle = CYLINDER_DEFAULT_END_ANGLE );

		Shape *getCopy( void );

		void setDisplayListIndex( int displayListIndex );

		void setLidType( UINT8 lidType = LID_ALL );
		UINT8 getLidType( void );

	protected:
		float	height,
				baseRadius,
				topRadius;

		UINT8	lidType;

		int displayListIndex;

		int		vertices;

		bool createCylinder( double theta1, double theta2 );
};