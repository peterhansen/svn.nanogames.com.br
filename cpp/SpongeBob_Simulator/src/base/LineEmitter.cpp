#include "LineEmitter.h"

LineEmitter::LineEmitter( float myEmissionRate, Shape *pMyShape, Point3f *myPosition, float myLife, float mySize, float myMass, Point3f *mySpeed, float myAngle, Point3f *myAxis, bool myActive ) 
			:Emitter( myEmissionRate, pMyShape, myPosition, myLife, mySize, myMass, mySpeed, myAngle, myAxis, myActive )
{
}

bool LineEmitter::emit( UINT16 quantity ) {
	if ( baseElement ) {
		for( UINT16 i = 0 ; i < quantity ; i++ ) {
			if ( currentActiveElements + 1 >= elements->getUsedLength() ) {
				// n�mero de part�culas ativas � maior do que o tamanho m�ximo atual do vetor -> aumenta tamanho m�ximo do vetor
				Particle *pCopy = baseElement->getCopy( );
				if ( !pCopy || !elements->insert( pCopy ) )	{
					SAFE_DELETE( pCopy );
					return false;
				}	
			}
			Particle *pParticle = ( Particle* )elements->getData( currentActiveElements );
			if( pParticle ) {
				currentActiveElements++;
				pParticle->setBasedOn( baseElement );
				Point3f direction = position - endPoint;
				float directionModule = direction.getModule();
				direction /= directionModule; // normaliza o vetor

				// posi��o � aleat�ria na linha
				pParticle->setPosition( &( position + ( direction * Random::nextDouble( directionModule ) ) ) );
				pParticle->getSpeed() += speed;
			}
		}

		elementsToEmit -= quantity;
		if ( elementsToEmit < 0.0 )
			elementsToEmit = 0.0;

		return true;
	} // fim if ( baseElement )
	return false;
}

void LineEmitter::setEndPoint( const Point3f newEndPoint ) {
	endPoint = newEndPoint;
}

Point3f LineEmitter::getEndPoint( void ) {
	return endPoint;
}