#pragma once

#include "Point3f.h"
#include "..\Cache.h"

class Plane {
public:
	Point3f	right,	// vetor apontando para a "direita" do plano
			top,	// vetor apontando para o "topo" do plano
			normal; // normal do plano
	float	d;		// deslocamento

	Plane( Point3f *top = NULL, Point3f *right = NULL, float d = 0.0 );

};