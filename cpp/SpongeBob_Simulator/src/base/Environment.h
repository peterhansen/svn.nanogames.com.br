#pragma once

#include "DynamicVector.h"
#include "Force.h"
#include "Particle.h"

class Environment {
protected:
	DynamicVector	*forces,
					*particles;
	bool buildParticlesVector( void );
	bool buildForcesVector( void );
public:
	Environment( void );
	UINT16 insertParticle( Particle *particle );
	bool removeParticleAt( UINT16 index );
	UINT16 insertForce( Force *force );
	bool removeForceAt( UINT16 index );
	void update( double time );
	void render( void );
}; // fim da classe Environment