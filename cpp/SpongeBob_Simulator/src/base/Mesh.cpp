#include "Mesh.h"

// indica o estado atual do parser
#define NEWLINE		0
#define TRIANGLE	1
#define VERTEX		2
#define NORMAL		3

Mesh::Mesh( Point3f *myCenter ) :
	vertices( NULL ), normals ( NULL ), trianglesIndex( NULL ), texture( NULL )
{
	if ( myCenter )
		center = *myCenter;
	else
		center.setPoint();
}

Mesh *Mesh :: createMesh( char *filename, Point3f *myCenter ) {
	Mesh *m = new Mesh( myCenter );

	if ( m ) {
		if ( !m->loadMesh( filename ) ) {
			SAFE_DELETE( m );
			return NULL;
		}
	}

	return m;
}

Mesh :: ~Mesh( void ) {
	SAFE_DELETE( vertices );
	SAFE_DELETE( normals );
	SAFE_DELETE( trianglesIndex );
	SAFE_DELETE( texture );
}


void Mesh :: draw( void ) {
	if ( texture )
		texture->load();

	glEnableClientState( GL_NORMAL_ARRAY );
	glEnableClientState( GL_VERTEX_ARRAY );
	glColor4f( color.r, color.g, color.b, color.a );
	glNormalPointer( GL_FLOAT, 0, normals );
	glVertexPointer( 3, GL_FLOAT, 0, vertices );

	// glDrawElements deve estar fora de glBegin/glEnd.
	glDrawElements( GL_TRIANGLES, nTriangles,  GL_UNSIGNED_INT, trianglesIndex ); 

	if ( texture )
		texture->unload();
}

/* carrega malha de tri�ngulos de um arquivo.
 * retorna true caso n�o tenha ocorrido erro.
 */
bool Mesh :: loadMesh(char *filename) {
	char buffer[64];
	int	i = 0, // posi��o atual do buffer
		j = 0, // �ndice do n�mero lido (x, y ou z)
		n = 0, // n�mero de normais lidas
		v = 0, // n�mero de v�rtices lidos
		t = 0; // n�mero de tri�ngulos lidos

	bool	valueStarted = false; // indica se j� foi iniciada a leitura de um valor

	char	c,
			mode = NEWLINE;
	float p[3];
	ifstream in(filename);

	// l� n�mero de v�rtices e normais
	in.getline(buffer, sizeof(buffer));
	nNormals = nVertices = ( atoi(buffer) * 3 ) / 2;

	// l� n�mero de triangulos
	in.getline(buffer, sizeof(buffer));
	nTriangles = 3 * atoi(buffer);

	vertices = new float[nVertices*3];// (Point3f *) malloc(sizeof(Point3f) * nVertices);
	normals =  new float[nNormals*3];//(Point3f *) malloc(sizeof(Point3f) * nNormals);
	trianglesIndex = new int[nTriangles];//(int *) malloc(sizeof(int) * (nTriangles));

	if( !vertices || !normals || !trianglesIndex )
		return false;

	while (!in.eof()) {
		c = in.get();
		switch (c) {
			case '\n':
			case ' ':
				buffer[i] = '\0';
				i = 0;
				
				// leu um valor - grava na estrutura correspondente
				switch (mode) {
					case NEWLINE:
						break;
					case TRIANGLE:
						if (valueStarted)
							trianglesIndex[t++] = atoi(buffer);
						break;
					case VERTEX:
						if (valueStarted) {
							p[j] = (float) atof(buffer);
							j = (j + 1) % 3;

							/*if (j == 0)
								vertices[v++].setPoint( p[0], p[1], p[2] );*/

							if (j == 0){
								vertices[v++] = p[0];
								vertices[v++] = p[1];
								vertices[v++] = p[2];
							}
						}
						break;
					case NORMAL:
						if (valueStarted) {
							p[j] = (float) atof(buffer);
							j = (j + 1) % 3;

							/*if (j == 0)
								normals[n++].setPoint( p[0], p[1], p[2] );*/

							if (j == 0){
								normals[n++] = p[0];
								normals[n++] = p[1];
								normals[n++] = p[2];
							}
						}
						break;
				}

				if (c == '\n')
					mode = NEWLINE;
				valueStarted = false;
				break;
			case 'T': // linha possui �ndices de um tri�ngulo
				mode = TRIANGLE;
				valueStarted = false;
				break;
			case '-':
				if (mode == NEWLINE) { // indica o divisor de �reas do arquivo: "--"
					j = 0;
					valueStarted = false;
				} else { // indica in�cio de n�mero negativo
					buffer[i++] = c;
					valueStarted = true;
				}
				break;
			case 'N': // linha possui valores de uma normal
				valueStarted = false;
				mode = NORMAL;
				break;
			case 'V': // linha possui valores de um v�rtice
				valueStarted = false;
				mode = VERTEX;
				break;
			default: // leu parte de um valor
				valueStarted = true;
				buffer[i++] = c;
		} // fim switch (c)
	} // fim while (!in.eof())

	in.close();
	return true;
}

Shape * Mesh::getCopy() {
	Texture *t = texture->getCopy();
	if ( t ) {
		Mesh *mesh = new Mesh( &center );
		if ( mesh ) {
			mesh->setTexture( t );
			mesh->normals = normals;
			mesh->nNormals = nNormals;
			mesh->vertices = vertices;
			mesh->nVertices = nVertices;
			mesh->trianglesIndex = trianglesIndex;
			mesh->nTriangles = nTriangles;
			mesh->center = center;

			mesh->setSize( size );
			mesh->setColor( &color );

			return mesh;
		}
		SAFE_DELETE( t );
	}

	return NULL;
}