#include "Scene.h"

Scene::Scene( void ) : worlds( NULL ), lights( NULL )
{
	clock = Clock();
	clock.setVisible( true );
}

UINT16 Scene::insertWorld( World *world ) {
	if ( !worlds ) {
		if (!buildWorldsVector())
			return -1;
	}

	worlds->insert( world );

	return worlds->getUsedLength();
} // fim do m�todo Scene::insertWorld()

bool Scene::removeWorldAt( UINT16 index ) {
	return worlds->remove( index );
} // fim do m�todo Scene::removeWorldAt()

World * Scene::getWorldAt( UINT16 index ) {
	return (World *) worlds->getData( index );
} // fim do m�todo Scene::getWorldAt()

bool Scene::buildWorldsVector( void ) {
	worlds = new DynamicVector();

	if ( !worlds )
		return false;

	return true;
} // fim do m�todo Scene::buildWorldsVector()

UINT16 Scene::insertLight( Light *light ) {
	if ( !lights ) {
		if ( !buildLightsVector() )
			return -1;
	}

	lights->insert( light );

	return lights->getUsedLength();
} // fim do m�todo Scene::insertLight()

bool Scene::removeLightAt( UINT16 index ) {
	return lights->remove( index );
} // fim do m�todo Scene::removeLightAt()

Light * Scene::getLightAt( UINT16 index ) {
	return (Light *) lights->getData( index );
} // fim do m�todo Scene::getLightAt()

bool Scene::buildLightsVector( void ) {
	lights = new DynamicVector();

	if ( !lights )
		return false;

	return true;
} // fim do m�todo Scene::buildLightsVector()



Scene::~Scene( void ) {
	SAFE_DELETE( worlds );
}

void Scene::render( void ) {
	UINT16 lightsLength = lights != NULL ? lights->getUsedLength() : 0;

	for ( UINT16 i = 0; i < lightsLength; i++ ) {
		( (Light *) ( lights->getData(i) ) )->enable();
	}


	UINT16 worldsLength = worlds != NULL ? worlds->getUsedLength() : 0;

	for ( UINT16 i = 0; i < worldsLength; i++ ) {
		( (World *) ( worlds->getData(i) ) )->render();
	}

	if ( clock.isVisible() )
		clock.draw();
}

void Scene::setCurrentCamera( Camera *camera ) {
	currentCamera = camera;
}

Camera * Scene::getCurrentCamera( void ) {
	return currentCamera;
}

void Scene::update( void ) {
	clock.update();
	float frameTime = clock.getTime();

	if ( frameTime > 0.0 ) {

		UINT16 length = worlds->getUsedLength();
		for ( UINT16 i = 0; i < length; i++) {
			( (World *) worlds->getData(i) )->update(frameTime);
		}
	}
}

Clock *Scene::getClock( void ) {
	return &clock;
}