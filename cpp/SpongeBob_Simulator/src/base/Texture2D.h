#ifndef TEXTURE2D_H
#define TEXTURE2D_H

/*==============================================================================================
CLASSE Texture2D
	Classe para utiliza��o de texturas 2D.
==============================================================================================*/

#include <GL/glaux.h>
#include "Texture.h"
#include "Point3f.h"
#include "Color.h"
#include "..\Util.h"

#define BITMAP_ID 0x4d42

class Texture2D : public Texture {
	public:
		Texture2D( void );
		virtual ~Texture2D( void );

		// carrega a textura bitmap de um arquivo.
		bool loadTexture( char *filename, Color *transparentColor = NULL );

		// retorna a largura e a altura da textura
		void getSize( UINT *pWidth, UINT *pHeight );

		// define a largura e a altura da textura
		void setSize( UINT width, UINT height );

		// retorna o array de cores da textura
		virtual const void* getTextureData( void );

		// carrega a textura
		virtual void load( void );

		// retira a textura
		virtual void unload( void );

		virtual Texture *getCopy( void );

	private:
		// vari�vel de controle de texturas 2D
		static UINT16 counter( UINT16 change = 0 );

		// ponteiro para o array de cores da textura
		BYTE *pData;

		// largura e altura da textura
		UINT width, height;

		Color transparentColor;
};

// inicializa a vari�vel est�tica da classe
//UINT16 Texture2D::counter = 0;

#endif