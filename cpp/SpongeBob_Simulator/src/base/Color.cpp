#include "Color.h"

Color::Color( float r, float g, float b, float a ) {
	setColor( r, g, b, a );
}


void Color::setColor( float myR, float myG, float myB, float myA ) {
	r = myR;
	g = myG;
	b = myB;
	a = myA;
}