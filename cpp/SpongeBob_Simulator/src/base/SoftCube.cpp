#include "SoftCube.h"
#ifdef _DEBUG
	#include <stdio.h>
#endif

SoftCube::SoftCube( Point3f *myPosition, Point3f *mySpeed )
				  : pCubeVertices( NULL ), 
				  pCoils( NULL ), numberOfCoils( 0 ), pTempCoils( NULL ), drawMode( DRAW_MODE_QUADS ), pFacesParticles( NULL )
{
	if ( mySpeed )
		speed = *mySpeed;
	else
		speed.setPoint();

	if ( myPosition )
		position = *myPosition;
	else
		position.setPoint();

	drawTempCoils = false;
}

SoftCube::~SoftCube( void )
{
	SAFE_DELETE( pFacesParticles );
	SAFE_DELETE( pCoils );
	SAFE_DELETE( pCubeVertices );
	SAFE_DELETE( pCubeOriginalPositions );
	SAFE_DELETE( pTempCoils );
}

bool SoftCube::build( float myDimension, UINT8 myPointsByEdge, float tightness, float damping, float stickingFactor )
{
	dimension = myDimension;
	if ( dimension <= 0.0 )
		dimension = 50.0;

	nPointsByEdge = myPointsByEdge;

	float halfSize = dimension / 2.0;
	float spaceBetweenPoints = dimension / (float) ( nPointsByEdge - 1 ); 

	if ( nPointsByEdge < 2 ) // anti-erro
		nPointsByEdge = 2;

	//aloca um vetor para armazenar os v�rtices que formar�o o cubo
	UINT16 nFaceVertices = nPointsByEdge*nPointsByEdge;
	nParticles = nFaceVertices*nPointsByEdge;
	pCubeVertices = new DynamicVector( nParticles );
	if( !pCubeVertices )
		return false;

	pCubeOriginalPositions = new DynamicVector( nParticles );
	if( !pCubeOriginalPositions )
		return false;

	//aloca um vetor para armazenar as molas tempor�rias criadas para fazer o grude com os planos de colis�o
	pTempCoils = new DynamicVector( nParticles );
	if( !pTempCoils )
		return false;

	//aloca um vetor para armazenar refer�ncias para os n�s das faces do cubo
	UINT16 facesVerticesCounter = 0;
	nFacesVertices = 2*( ( nFaceVertices  ) + ( nPointsByEdge*( nPointsByEdge-2 ) ) + ( ( nPointsByEdge-2 )*( nPointsByEdge-2 ) ) );
	pFacesParticles = new CubeParticle*[ nFacesVertices ];
	if( !pFacesParticles )
		return false;

	//gera as faces da frente e de tr�s
	CubeParticle *pParticle;
	for( UINT16 z=0 ; z < nPointsByEdge ; z++ )
	{
		for( UINT16 y=0 ; y < nPointsByEdge ; y++ )
		{
			for( UINT16 x=0 ; x < nPointsByEdge ; x++ )
			{
				Point3f *pos = new Point3f( position.x -halfSize+( x*spaceBetweenPoints ), position.y + halfSize - ( y*spaceBetweenPoints ), position.z - halfSize + ( z*spaceBetweenPoints ) );
				if ( pos ) {
					pCubeOriginalPositions->insert( pos );
					pParticle = new CubeParticle( pos, NULL, stickingFactor );

					if( !pParticle || !pParticle->build() || !pCubeVertices->insert( pParticle ) )
					{
						SAFE_DELETE( pParticle );
						return false;
					}
					else
					{
						if( ( z == 0 ||  z == nPointsByEdge-1 )
							|| ( y == 0 ||  y == nPointsByEdge-1 )
							|| ( x == 0 ||  x == nPointsByEdge-1 ) )
							pFacesParticles[ facesVerticesCounter++ ] = pParticle;
					}
				}
				else
				{
					return false;
				}
			}
		}
	}

	//aloca um vetor para armazenar as molas
	numberOfCoils = ( ( nPointsByEdge-1 )*( nPointsByEdge )*( nPointsByEdge )*3 )+( 6*( nPointsByEdge-1 )*( nPointsByEdge-1 )*( nPointsByEdge ) );
	pCoils = new SpringCoil[ numberOfCoils ];
	if( !pCoils )
		return false;

	//coloca as molas entre os v�rtices adjacentes
	UINT16	aux, i = 0;
	CubeParticle *p1, *p2;
	//coloca as molas do eixo x
	for( UINT16 z=0 ; z < nPointsByEdge ; z++ )
	{
		for( UINT16 y=0 ; y < nPointsByEdge ; y++ )
		{
			for( UINT16 x=0 ; x < nPointsByEdge-1 ; x++ )
			{
				aux = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

				p1 = ( CubeParticle* )pCubeVertices->getData( aux );
				p2 = ( CubeParticle* )pCubeVertices->getData( aux+1 );
				pCoils[i++].set( p1, p2, tightness, damping );
				p1->insertNeighbor( p2 );
				p2->insertNeighbor( p1 );

				if ( y < nPointsByEdge - 1 )
				{
					p1 = ( CubeParticle* )pCubeVertices->getData( aux );
					p2 = ( CubeParticle* )pCubeVertices->getData( aux+1+nPointsByEdge );
					pCoils[i++].set( p1, p2, tightness, damping );
					p1->insertNeighbor( p2 );
					p2->insertNeighbor( p1 );


					p1 = ( CubeParticle* )pCubeVertices->getData( aux+nPointsByEdge );
					p2 = ( CubeParticle* )pCubeVertices->getData( aux+1 );
					pCoils[i++].set( p1, p2, tightness, damping );
					p1->insertNeighbor( p2 );
					p2->insertNeighbor( p1 );
				}	
			}
		}
	}
	//coloca as molas do eixo y
	for( UINT16 z=0 ; z < nPointsByEdge ; z++ )
	{
		for( UINT16 y=0 ; y < nPointsByEdge-1 ; y++ )
		{
			for( UINT16 x=0 ; x < nPointsByEdge ; x++ )
			{
				aux = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

				p1 = ( CubeParticle* )pCubeVertices->getData( aux );
				p2 = ( CubeParticle* )pCubeVertices->getData( aux+nPointsByEdge );
				pCoils[i++].set( p1, p2, tightness, damping );
				p1->insertNeighbor( p2 );
				p2->insertNeighbor( p1 );

				if ( z < nPointsByEdge - 1 )
				{
					p1 = ( CubeParticle* )pCubeVertices->getData( aux );
					p2 = ( CubeParticle* )pCubeVertices->getData( aux+nPointsByEdge+nFaceVertices );
					pCoils[i++].set( p1, p2, tightness, damping );
					p1->insertNeighbor( p2 );
					p2->insertNeighbor( p1 );

					p1 = ( CubeParticle* )pCubeVertices->getData( aux+nPointsByEdge );
					p2 = ( CubeParticle* )pCubeVertices->getData( aux+nFaceVertices );
					pCoils[i++].set( p1, p2, tightness, damping );
					p1->insertNeighbor( p2 );
					p2->insertNeighbor( p1 );
				}
			}
		}
	}
	//coloca as molas do eixo z
	for( UINT16 z=0 ; z < nPointsByEdge-1 ; z++ )
	{
		for( UINT16 y=0 ; y < nPointsByEdge ; y++ )
		{
			for( UINT16 x=0 ; x < nPointsByEdge ; x++ )
			{
				aux = ( z*nFaceVertices )+( y*nPointsByEdge )+x;

				p1 = ( CubeParticle* )pCubeVertices->getData( aux );
				p2 = ( CubeParticle* )pCubeVertices->getData( aux+nFaceVertices );
				pCoils[i++].set( p1, p2, tightness, damping );
				p1->insertNeighbor( p2 );
				p2->insertNeighbor( p1 );

				if ( x < nPointsByEdge - 1 )
				{
					p1 = ( CubeParticle* )pCubeVertices->getData( aux );
					p2 = ( CubeParticle* )pCubeVertices->getData( aux+nFaceVertices+1 );
					pCoils[i++].set( p1, p2, tightness, damping );
					p1->insertNeighbor( p2 );
					p2->insertNeighbor( p1 );

					p1 = ( CubeParticle* )pCubeVertices->getData( aux+nFaceVertices );
					p2 = ( CubeParticle* )pCubeVertices->getData( aux+1 );
					pCoils[i++].set( p1, p2, tightness, damping );
					p1->insertNeighbor( p2 );
					p2->insertNeighbor( p1 );
				}

			}
		}
	}
	return true;
}

void SoftCube::draw( void ) {

	switch( drawMode )
	{
		case DRAW_MODE_WIRED:
		{
			glPointSize( 5 );
			glColor3f( 1.0, 1.0, 1.0 );
			glBegin( GL_POINTS );
			int length = pCubeVertices->getUsedLength();

			Point3f pos;
			for ( int i = 0; i < length; i++ ) {
				pos = ( (Particle *) pCubeVertices->getData( i ) )->getPosition();
				glVertex3f( pos.x, pos.y, pos.z );
			}

			glEnd(); // fim de GL_POINTS

			// desenha molas
			Point3f p1,
					p2;

			glColor3f( 1.0, 0.0, 0.0 );
			glLineWidth( 1 );
			glBegin( GL_LINES );
			for ( int i = 0; i < numberOfCoils; i++ ) {
				p1 = pCoils[i].p1->getPosition();
				p2 = pCoils[i].p2->getPosition();

				glVertex3f( p1.x, p1.y, p1.z );
				glVertex3f( p2.x, p2.y, p2.z );
			}
			glEnd();

		}
		break;

		case DRAW_MODE_PARTICLES: 
		{
			UINT16 length = pCubeVertices->getUsedLength();
			for ( int i = 0; i < length; i++ ) {
				( ( Particle * ) pCubeVertices->getData( i ) )->draw();
			}
		}
		break;

		case DRAW_MODE_QUADS:
		{
			if ( pShape && pShape->getTexture() )
				pShape->getTexture()->load();

			Node *pAux;
			Point3f vertice,
					normal;
			UINT16 temp, nFaceVertices = nPointsByEdge*nPointsByEdge;
			float xOffset = 0.25 / ( nPointsByEdge - 1 ),
				  yOffset = 1.0 / ( nPointsByEdge - 1 ),
				  offset = 0.5;

			glColor4f( 1.0, 1.0, 1.0, 1.0 );
			glBegin( GL_QUADS );
			for ( UINT16 z=0 ; z < nPointsByEdge ; z+= nPointsByEdge-1 )
			{
				if ( z == 0 )
					normal.setPoint( 0.0, 0.0, -1.0 );
				else
					normal.setPoint( 0.0, 0.0, 1.0 );

				for( UINT16 y=0 ; y < nPointsByEdge-1 ; y++ )
				{
					for( UINT16 x=0 ; x < nPointsByEdge-1 ; x++ )
					{
						temp = ( z*nFaceVertices )+( y*nPointsByEdge )+x;
						glNormal3f( normal.x, normal.y, normal.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( x * xOffset ), y * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+1 );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( ( x + 1 ) * xOffset ), y * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nPointsByEdge+1 );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( ( x + 1 ) * xOffset ), ( y + 1 ) * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nPointsByEdge );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( x * xOffset ), ( y + 1 ) * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );
					} // fim do for (x)
				} // fim do for (y)
				offset -= 0.5;
			}

			for ( UINT16 y=0 ; y < nPointsByEdge ; y+= nPointsByEdge-1 )
			{
				if ( y == 0 )
				{
					glColor4f( 1.0, 0.95, 0.32, 1.0 );
					normal.setPoint( 0.0, 1.0, 0.0 );
				}
				else
				{
					glColor4f( 0.82, 0.56, 0.12, 1.0 );
					normal.setPoint( 0.0, -1.0, 0.0 );
				}

				for( UINT16 z=0 ; z < nPointsByEdge-1 ; z++ )
				{
					for( UINT16 x=0 ; x < nPointsByEdge-1 ; x++ )
					{
						temp = ( z*nFaceVertices )+( y*nPointsByEdge )+x;
						glNormal3f( normal.x, normal.y, normal.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp );
						vertice = pAux->getPosition();
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nFaceVertices );
						vertice = pAux->getPosition();
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nFaceVertices+1 );
						vertice = pAux->getPosition();
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+1 );
						vertice = pAux->getPosition();
						glVertex3f( vertice.x, vertice.y, vertice.z );
					} // fim do for (x)
				} // fim do for (y)
			}

			offset = 0.25;
			glColor4f( 1.0, 1.0, 1.0, 1.0 );
			for ( UINT16 x=0 ; x < nPointsByEdge ; x+= nPointsByEdge-1 )
			{
				if ( x == 0 )
					normal.setPoint( -1.0, 0.0, 0.0 );
				else
					normal.setPoint( 1.0, 0.0, 0.0 );

				for( UINT16 z=0 ; z < nPointsByEdge-1 ; z++ )
				{
					for( UINT16 y=0 ; y < nPointsByEdge-1 ; y++ )
					{
						temp = ( z*nFaceVertices )+( y*nPointsByEdge )+x;
						glNormal3f( normal.x, normal.y, normal.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( z * xOffset ), y * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nFaceVertices );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( ( z + 1 ) * xOffset ), y * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nFaceVertices+nPointsByEdge );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( ( z + 1 ) * xOffset ), ( y + 1 ) * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );

						pAux = ( CubeParticle* )pCubeVertices->getData( temp+nPointsByEdge );
						vertice = pAux->getPosition();
						glTexCoord2f ( offset + ( z * xOffset ), ( y + 1 ) * yOffset );
						glVertex3f( vertice.x, vertice.y, vertice.z );
					} // fim do for (x)
				} // fim do for (y)
				offset += 0.5;
			}
			glEnd();
		if ( pShape && pShape->getTexture() )
				pShape->getTexture()->unload();
		}
		break;
	}

	// desenha molas
	if( drawTempCoils )
	{
		Point3f p1,
				p2;
		SpringCoil *pTempCoil;

		glColor3f( 0.0, 1.0, 0.0 );
		glLineWidth( 5 );
		UINT16 nTempCoils = pTempCoils->getUsedLength();
		glBegin( GL_LINES );
		for ( int i = 0; i < nTempCoils; i++ ) {
			pTempCoil = ( SpringCoil * ) pTempCoils->getData( i );
			p1 =  pTempCoil->p1->getPosition();
			p2 =  pTempCoil->p2->getPosition();

			glVertex3f( p1.x, p1.y, p1.z );
			glVertex3f( p2.x, p2.y, p2.z );
		}
		glEnd();
		glLineWidth( 1 );
	}
}

bool SoftCube::update( double time, DynamicVector *externForces ) {

	//atualiza as molas do cubo
	for ( UINT16 i = 0; i < numberOfCoils; i++ ) {
		pCoils[i].update( time );
	}

	//atualiza as molas tempor�rias que causam a ader�ncia do cubo
	SpringCoil *pCoil;
	UINT16 nCoils = pTempCoils->getUsedLength();
	for ( UINT16 i = 0; i < nCoils ; ) {
		pCoil = ( SpringCoil * ) pTempCoils->getData( i );
		//se a mola arrebentou...
		if( pCoil->update( time ) )
		{
			//decrementa o n�mero de molas ligadas �s part�culas
			( ( CubeParticle* )pCoil->p1 )->decreaseTempCoils();
			( ( CubeParticle* )pCoil->p2 )->decreaseTempCoils();

			//a retira do vetor de molas tempor�rias
			pTempCoils->remove( i );
			nCoils--;
		}
		else
			i++;
	}

	//verifica se deve criar novas molas tempor�rias
	CubeParticle *p;
	UINT16 length = pCubeVertices->getUsedLength();
	for ( UINT16 i = 0; i < length; i++ ) {
		p = ( CubeParticle * ) pCubeVertices->getData( i );
		//se colidiu...
		if( p->update( time, externForces ) )
		{
			//cria a mola tempor�ria
			createTemporaryCoils( p );
		}
	}

	return true;
}

inline UINT16 SoftCube::insertLimitPlane( Plane * myLimitPlane ) {
	if ( pCubeVertices ) {
		UINT16 length = pCubeVertices->getUsedLength();

		Particle *p;
		for ( UINT16 i = 0; i < length; i++ ) {
			p = ( Particle * ) pCubeVertices->getData( i );
			p->insertLimitPlane( myLimitPlane );
		}
	}

	return Particle::insertLimitPlane( myLimitPlane );
}

UINT8 SoftCube::getDrawMode( void )
{
	return drawMode;
}

void SoftCube::setDrawMode( UINT8 mode )
{
	drawMode = mode;
}

inline void SoftCube::setStickingFactor( float factor )
{
	if( pCubeVertices )
	{
		UINT16 length = pCubeVertices->getUsedLength();

		CubeParticle *p;
		for ( UINT16 i = 0; i < length; i++ ) {
			p = ( CubeParticle * ) pCubeVertices->getData( i );
			p->setStickingFactor( factor );
		}
	}
}

inline float SoftCube::getStickingFactor( void )
{
	if( pCubeVertices )
		return ( ( CubeParticle * )pCubeVertices->getData( 0 ))->getStickingFactor();
	return 0;
}

bool SoftCube::createTemporaryCoils( CubeParticle *pParticle )
{
	if( pParticle )
	{
		for( UINT16 i=0 ; i < nFacesVertices ; i++ )
		{
			if( pFacesParticles[i] == pParticle )
			{
				UINT16 nTempCoils = pParticle->getNTempCoils();
				UINT16 nNeighbors = pParticle->getNumberOfNeighbors();
				for( UINT16 i=0 ; i < nNeighbors && nTempCoils < MAX_TEMP_COILS ; i++ )
				{
					float speed = ( pParticle->getSpeed() ).getModule();
					float stickness = speed * pParticle->getStickingFactor();
					SpringCoil *pNewCoil = new SpringCoil( pParticle, pParticle->getNeighborAt( i ), false, stickness, stickness, ( dimension / ( nPointsByEdge - 1 ) )*1.5, 5.0 * pParticle->getStickingFactor() );
					if( !pNewCoil || !pTempCoils->insert( pNewCoil ) )
					{
						SAFE_DELETE( pNewCoil );
						return false;
					}
					else
					{
						pParticle->increaseTempCoils();
						nTempCoils = pParticle->getNTempCoils();
					}
				}
				//n�o tem problema se n�o entrarmos no for e deixarmos a particula imune, pois n�o entrar no for significa
				//que j� existem molas tempor�rias, logo a part�cula est� imune
				pParticle->setImmune( true );
			}
		}
	}
	return pParticle != NULL;
}

inline void SoftCube::setSpeed( Point3f *speed ) {
	if( pCubeVertices )
	{
		UINT16 length = pCubeVertices->getUsedLength();

		CubeParticle *p;
		for ( UINT16 i = 0; i < length; i++ ) {
			p = ( CubeParticle * ) pCubeVertices->getData( i );
			p->setSpeed( speed );
		}
	}
}

inline Point3f SoftCube::getPosition( void ) {
	return ( ( CubeParticle * ) pCubeVertices->getData( 0 ) )->getPosition();
}

inline void SoftCube::setPosition( Point3f *position ) {
	if( pCubeVertices )
	{
		UINT16 length = pCubeVertices->getUsedLength();

		CubeParticle *p;
		Point3f *pos,
				zero = Point3f();
		for ( UINT16 i = 0; i < length; i++ ) {
			p = ( CubeParticle * ) pCubeVertices->getData( i );
			pos = ( Point3f * ) pCubeOriginalPositions->getData( i );
			p->setPosition( pos );
			p->setSpeed( &zero );
		}
	}
}

inline void SoftCube::reset( void ) {
	setPosition( &Point3f() );
	while ( pTempCoils->getUsedLength() > 0 )
		pTempCoils->remove( 0 );

	if( pCubeVertices )
	{
		UINT16 length = pCubeVertices->getUsedLength();

		CubeParticle *p;
		for ( UINT16 i = 0; i < length; i++ ) {
			p = ( CubeParticle * ) pCubeVertices->getData( i );
			p->force.setPoint();
			p->setSpeed( &Point3f() );
			while ( p->getNTempCoils() > 0 )
				p->decreaseTempCoils();
		}
	}
}