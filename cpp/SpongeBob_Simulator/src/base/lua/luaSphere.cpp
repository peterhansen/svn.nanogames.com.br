/*
** Lua binding: luaSphere
** Generated automatically by tolua 5.0a on 11/14/05 01:33:56.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaSphere_open (lua_State* tolua_S);

#include "..\Shape.h"
#include "..\Point3f.h"
#include "..\Texture.h"
#include "..\Sphere.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Sphere (lua_State* tolua_S)
{
 Sphere* self = (Sphere*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Sphere");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Texture");
}

/* method: new of class  Sphere */
static int tolua_luaSphere_Sphere_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Sphere",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"Texture",1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float size = ((float)  tolua_tonumber(tolua_S,2,SPHERE_DEFAULT_RADIUS));
  int vertices = ((int)  tolua_tonumber(tolua_S,3,SPHERE_RESOLUTION));
  Texture* texture = ((Texture*)  tolua_tousertype(tolua_S,4,NULL));
 {
  Sphere* tolua_ret = (Sphere*)  new Sphere(size,vertices,texture);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Sphere");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Sphere */
static int tolua_luaSphere_Sphere_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Sphere",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Sphere* self = (Sphere*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Sphere */
static int tolua_luaSphere_Sphere_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Sphere",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Sphere* self = (Sphere*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: build of class  Sphere */
static int tolua_luaSphere_Sphere_build00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Sphere",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Sphere* self = (Sphere*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'build'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->build();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'build'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Sphere */
static int tolua_luaSphere_Sphere_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Sphere",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Sphere* self = (Sphere*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Shape* tolua_ret = (Shape*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Shape");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* method: setDisplayListIndex of class  Sphere */
static int tolua_luaSphere_Sphere_setDisplayListIndex00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Sphere",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Sphere* self = (Sphere*)  tolua_tousertype(tolua_S,1,0);
  int displayListIndex = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setDisplayListIndex'",NULL);
#endif
 {
  self->setDisplayListIndex(displayListIndex);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setDisplayListIndex'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaSphere_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"SPHERE_RESOLUTION",SPHERE_RESOLUTION);
 tolua_constant(tolua_S,"SPHERE_DEFAULT_RADIUS",SPHERE_DEFAULT_RADIUS);
 tolua_constant(tolua_S,"TWOPI",TWOPI);
 tolua_constant(tolua_S,"PID2",PID2);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Sphere","Sphere","Shape",tolua_collect_Sphere);
#else
 tolua_cclass(tolua_S,"Sphere","Sphere","Shape",NULL);
#endif
 tolua_beginmodule(tolua_S,"Sphere");
 tolua_function(tolua_S,"new",tolua_luaSphere_Sphere_new00);
 tolua_function(tolua_S,"delete",tolua_luaSphere_Sphere_delete00);
 tolua_function(tolua_S,"draw",tolua_luaSphere_Sphere_draw00);
 tolua_function(tolua_S,"build",tolua_luaSphere_Sphere_build00);
 tolua_function(tolua_S,"getCopy",tolua_luaSphere_Sphere_getCopy00);
 tolua_function(tolua_S,"setDisplayListIndex",tolua_luaSphere_Sphere_setDisplayListIndex00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
