/*
** Lua binding: luaScene
** Generated automatically by tolua 5.0a on 11/14/05 02:44:59.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaScene_open (lua_State* tolua_S);

#include "..\World.h"
#include "..\Clock.h"
#include "..\DynamicVector.h"
#include "..\Camera.h"
#include "..\Clock.h"
#include "..\Scene.h"
#include "..\Light.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Scene (lua_State* tolua_S)
{
 Scene* self = (Scene*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Camera");
 tolua_usertype(tolua_S,"Scene");
 tolua_usertype(tolua_S,"Clock");
 tolua_usertype(tolua_S,"Light");
 tolua_usertype(tolua_S,"World");
}

/* method: new of class  Scene */
static int tolua_luaScene_Scene_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Scene* tolua_ret = (Scene*)  new Scene();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Scene");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Scene */
static int tolua_luaScene_Scene_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: insertWorld of class  Scene */
static int tolua_luaScene_Scene_insertWorld00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"World",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  World* world = ((World*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertWorld'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertWorld(world);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertWorld'.",&tolua_err);
 return 0;
#endif
}

/* method: removeWorldAt of class  Scene */
static int tolua_luaScene_Scene_removeWorldAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  int index = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeWorldAt'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeWorldAt(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeWorldAt'.",&tolua_err);
 return 0;
#endif
}

/* method: getWorldAt of class  Scene */
static int tolua_luaScene_Scene_getWorldAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  int index = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getWorldAt'",NULL);
#endif
 {
  World* tolua_ret = (World*)  self->getWorldAt(index);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"World");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getWorldAt'.",&tolua_err);
 return 0;
#endif
}

/* method: insertLight of class  Scene */
static int tolua_luaScene_Scene_insertLight00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Light",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  Light* light = ((Light*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertLight'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertLight(light);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertLight'.",&tolua_err);
 return 0;
#endif
}

/* method: removeLightAt of class  Scene */
static int tolua_luaScene_Scene_removeLightAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeLightAt'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeLightAt(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeLightAt'.",&tolua_err);
 return 0;
#endif
}

/* method: getLightAt of class  Scene */
static int tolua_luaScene_Scene_getLightAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getLightAt'",NULL);
#endif
 {
  Light* tolua_ret = (Light*)  self->getLightAt(index);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Light");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getLightAt'.",&tolua_err);
 return 0;
#endif
}

/* method: render of class  Scene */
static int tolua_luaScene_Scene_render00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'render'",NULL);
#endif
 {
  self->render();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'render'.",&tolua_err);
 return 0;
#endif
}

/* method: setCurrentCamera of class  Scene */
static int tolua_luaScene_Scene_setCurrentCamera00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Camera",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
  Camera* camera = ((Camera*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setCurrentCamera'",NULL);
#endif
 {
  self->setCurrentCamera(camera);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setCurrentCamera'.",&tolua_err);
 return 0;
#endif
}

/* method: getCurrentCamera of class  Scene */
static int tolua_luaScene_Scene_getCurrentCamera00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCurrentCamera'",NULL);
#endif
 {
  Camera* tolua_ret = (Camera*)  self->getCurrentCamera();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Camera");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCurrentCamera'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  Scene */
static int tolua_luaScene_Scene_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  self->update();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: getClock of class  Scene */
static int tolua_luaScene_Scene_getClock00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Scene",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* self = (Scene*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getClock'",NULL);
#endif
 {
  Clock* tolua_ret = (Clock*)  self->getClock();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Clock");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getClock'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaScene_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Scene","Scene","",tolua_collect_Scene);
#else
 tolua_cclass(tolua_S,"Scene","Scene","",NULL);
#endif
 tolua_beginmodule(tolua_S,"Scene");
 tolua_function(tolua_S,"new",tolua_luaScene_Scene_new00);
 tolua_function(tolua_S,"delete",tolua_luaScene_Scene_delete00);
 tolua_function(tolua_S,"insertWorld",tolua_luaScene_Scene_insertWorld00);
 tolua_function(tolua_S,"removeWorldAt",tolua_luaScene_Scene_removeWorldAt00);
 tolua_function(tolua_S,"getWorldAt",tolua_luaScene_Scene_getWorldAt00);
 tolua_function(tolua_S,"insertLight",tolua_luaScene_Scene_insertLight00);
 tolua_function(tolua_S,"removeLightAt",tolua_luaScene_Scene_removeLightAt00);
 tolua_function(tolua_S,"getLightAt",tolua_luaScene_Scene_getLightAt00);
 tolua_function(tolua_S,"render",tolua_luaScene_Scene_render00);
 tolua_function(tolua_S,"setCurrentCamera",tolua_luaScene_Scene_setCurrentCamera00);
 tolua_function(tolua_S,"getCurrentCamera",tolua_luaScene_Scene_getCurrentCamera00);
 tolua_function(tolua_S,"update",tolua_luaScene_Scene_update00);
 tolua_function(tolua_S,"getClock",tolua_luaScene_Scene_getClock00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
