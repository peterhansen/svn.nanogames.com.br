/*
** Lua binding: luaWorld
** Generated automatically by tolua 5.0a on 11/27/05 12:24:44.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaWorld_open (lua_State* tolua_S);

#include "..\World.h"
#include "..\Environment.h"
#include "..\DynamicVector.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Environment");
 tolua_usertype(tolua_S,"World");
}

/* method: new of class  World */
static int tolua_luaWorld_World_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"World",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  World* tolua_ret = (World*)  new World();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"World");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: insertEnvironment of class  World */
static int tolua_luaWorld_World_insertEnvironment00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"World",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Environment",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  World* self = (World*)  tolua_tousertype(tolua_S,1,0);
  Environment* environment = ((Environment*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertEnvironment'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertEnvironment(environment);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertEnvironment'.",&tolua_err);
 return 0;
#endif
}

/* method: removeEnvironmentAt of class  World */
static int tolua_luaWorld_World_removeEnvironmentAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"World",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  World* self = (World*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeEnvironmentAt'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeEnvironmentAt(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeEnvironmentAt'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  World */
static int tolua_luaWorld_World_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"World",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  World* self = (World*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  self->update(time);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: render of class  World */
static int tolua_luaWorld_World_render00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"World",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  World* self = (World*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'render'",NULL);
#endif
 {
  self->render();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'render'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaWorld_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"World","World","",NULL);
 tolua_beginmodule(tolua_S,"World");
 tolua_function(tolua_S,"new",tolua_luaWorld_World_new00);
 tolua_function(tolua_S,"insertEnvironment",tolua_luaWorld_World_insertEnvironment00);
 tolua_function(tolua_S,"removeEnvironmentAt",tolua_luaWorld_World_removeEnvironmentAt00);
 tolua_function(tolua_S,"update",tolua_luaWorld_World_update00);
 tolua_function(tolua_S,"render",tolua_luaWorld_World_render00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
