/*
** Lua binding: luaTexture
** Generated automatically by tolua 5.0a on 11/14/05 01:33:43.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaTexture_open (lua_State* tolua_S);

#include "..\Texture.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_UINT (lua_State* tolua_S)
{
 UINT* self = (UINT*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"UINT");
 tolua_usertype(tolua_S,"Texture");
}

/* method: getTextureId of class  Texture */
static int tolua_luaTexture_Texture_getTextureId00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture* self = (Texture*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureId'",NULL);
#endif
 {
  UINT tolua_ret =  self->getTextureId();
 {
#ifdef __cplusplus
 void* tolua_obj = new UINT(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_UINT),"UINT");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(UINT));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"UINT");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureId'.",&tolua_err);
 return 0;
#endif
}

/* method: getTextureData of class  Texture */
static int tolua_luaTexture_Texture_getTextureData00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture* self = (Texture*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureData'",NULL);
#endif
 {
  void* tolua_ret = (void*)  self->getTextureData();
 tolua_pushuserdata(tolua_S,(void*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureData'.",&tolua_err);
 return 0;
#endif
}

/* method: load of class  Texture */
static int tolua_luaTexture_Texture_load00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture* self = (Texture*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'load'",NULL);
#endif
 {
  self->load();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'load'.",&tolua_err);
 return 0;
#endif
}

/* method: unload of class  Texture */
static int tolua_luaTexture_Texture_unload00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture* self = (Texture*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'unload'",NULL);
#endif
 {
  self->unload();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'unload'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Texture */
static int tolua_luaTexture_Texture_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture* self = (Texture*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Texture* tolua_ret = (Texture*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Texture");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaTexture_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Texture","Texture","",NULL);
 tolua_beginmodule(tolua_S,"Texture");
 tolua_function(tolua_S,"getTextureId",tolua_luaTexture_Texture_getTextureId00);
 tolua_function(tolua_S,"getTextureData",tolua_luaTexture_Texture_getTextureData00);
 tolua_function(tolua_S,"load",tolua_luaTexture_Texture_load00);
 tolua_function(tolua_S,"unload",tolua_luaTexture_Texture_unload00);
 tolua_function(tolua_S,"getCopy",tolua_luaTexture_Texture_getCopy00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
