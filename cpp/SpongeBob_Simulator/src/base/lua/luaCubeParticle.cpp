/*
** Lua binding: luaCubeParticle
** Generated automatically by tolua 5.0a on 12/06/05 19:50:58.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaCubeParticle_open (lua_State* tolua_S);

#include "..\Particle.h"
#include "..\CubeParticle.h"
#include "..\Force.h"
#include "..\Point3f.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_UINT16 (lua_State* tolua_S)
{
 UINT16* self = (UINT16*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}

static int tolua_collect_CubeParticle (lua_State* tolua_S)
{
 CubeParticle* self = (CubeParticle*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"DynamicVector");
 tolua_usertype(tolua_S,"UINT16");
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"CubeParticle");
 tolua_usertype(tolua_S,"Particle");
}

/* method: new of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* myPosition = ((Point3f*)  tolua_tousertype(tolua_S,2,NULL));
  Point3f* mySpeed = ((Point3f*)  tolua_tousertype(tolua_S,3,NULL));
 {
  CubeParticle* tolua_ret = (CubeParticle*)  new CubeParticle(myPosition,mySpeed);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"CubeParticle");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  CubeParticle* self = (CubeParticle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: build of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_build00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  CubeParticle* self = (CubeParticle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'build'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->build();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'build'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  CubeParticle* self = (CubeParticle*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
  DynamicVector* externForces = ((DynamicVector*)  tolua_tousertype(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->update(time,externForces);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: getNumberOfNeighbors of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_getNumberOfNeighbors00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  CubeParticle* self = (CubeParticle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getNumberOfNeighbors'",NULL);
#endif
 {
  UINT16 tolua_ret =  self->getNumberOfNeighbors();
 {
#ifdef __cplusplus
 void* tolua_obj = new UINT16(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_UINT16),"UINT16");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(UINT16));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"UINT16");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getNumberOfNeighbors'.",&tolua_err);
 return 0;
#endif
}

/* method: insertNeighbor of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_insertNeighbor00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"CubeParticle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  CubeParticle* self = (CubeParticle*)  tolua_tousertype(tolua_S,1,0);
  CubeParticle* pParticle = ((CubeParticle*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertNeighbor'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->insertNeighbor(pParticle);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertNeighbor'.",&tolua_err);
 return 0;
#endif
}

/* method: removeNeighbor of class  CubeParticle */
static int tolua_luaCubeParticle_CubeParticle_removeNeighbor00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"CubeParticle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  CubeParticle* self = (CubeParticle*)  tolua_tousertype(tolua_S,1,0);
  unsigned short index = ((unsigned short)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeNeighbor'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeNeighbor(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeNeighbor'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaCubeParticle_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"CubeParticle","CubeParticle","Particle",tolua_collect_CubeParticle);
#else
 tolua_cclass(tolua_S,"CubeParticle","CubeParticle","Particle",NULL);
#endif
 tolua_beginmodule(tolua_S,"CubeParticle");
 tolua_function(tolua_S,"new",tolua_luaCubeParticle_CubeParticle_new00);
 tolua_function(tolua_S,"delete",tolua_luaCubeParticle_CubeParticle_delete00);
 tolua_function(tolua_S,"build",tolua_luaCubeParticle_CubeParticle_build00);
 tolua_function(tolua_S,"update",tolua_luaCubeParticle_CubeParticle_update00);
 tolua_function(tolua_S,"getNumberOfNeighbors",tolua_luaCubeParticle_CubeParticle_getNumberOfNeighbors00);
 tolua_function(tolua_S,"insertNeighbor",tolua_luaCubeParticle_CubeParticle_insertNeighbor00);
 tolua_function(tolua_S,"removeNeighbor",tolua_luaCubeParticle_CubeParticle_removeNeighbor00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
