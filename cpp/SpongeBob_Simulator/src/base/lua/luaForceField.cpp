/*
** Lua binding: luaForceField
** Generated automatically by tolua 5.0a on 11/09/05 00:08:56.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaForceField_open (lua_State* tolua_S);

#include "..\Force.h"
#include "..\ForceField.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Force");
 tolua_usertype(tolua_S,"ForceField");
}

/* Open function */
TOLUA_API int tolua_luaForceField_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"ForceField","ForceField","Force",NULL);
 tolua_beginmodule(tolua_S,"ForceField");
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
