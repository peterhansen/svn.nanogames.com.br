/*
** Lua binding: luaPerspectiveCamera
** Generated automatically by tolua 5.0a on 11/27/05 02:13:27.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaPerspectiveCamera_open (lua_State* tolua_S);

#include "..\Camera.h"
#include "..\PerspectiveCamera.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_PerspectiveCamera (lua_State* tolua_S)
{
 PerspectiveCamera* self = (PerspectiveCamera*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Camera");
 tolua_usertype(tolua_S,"PerspectiveCamera");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  PerspectiveCamera */
static int tolua_luaPerspectiveCamera_PerspectiveCamera_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"PerspectiveCamera",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"Point3f",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,8,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* myCenter = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
  Point3f* myEye = ((Point3f*)  tolua_tousertype(tolua_S,3,0));
  Point3f* myUp = ((Point3f*)  tolua_tousertype(tolua_S,4,0));
  float fovy = ((float)  tolua_tonumber(tolua_S,5,DEFAULT_FOVY));
  float zNear = ((float)  tolua_tonumber(tolua_S,6,DEFAULT_ZNEAR));
  float zFar = ((float)  tolua_tonumber(tolua_S,7,DEFAULT_ZFAR));
 {
  PerspectiveCamera* tolua_ret = (PerspectiveCamera*)  new PerspectiveCamera(myCenter,myEye,myUp,fovy,zNear,zFar);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"PerspectiveCamera");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  PerspectiveCamera */
static int tolua_luaPerspectiveCamera_PerspectiveCamera_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"PerspectiveCamera",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  PerspectiveCamera* self = (PerspectiveCamera*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: place of class  PerspectiveCamera */
static int tolua_luaPerspectiveCamera_PerspectiveCamera_place00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"PerspectiveCamera",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  PerspectiveCamera* self = (PerspectiveCamera*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'place'",NULL);
#endif
 {
  self->place();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'place'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaPerspectiveCamera_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"DEFAULT_FOVY",DEFAULT_FOVY);
 tolua_constant(tolua_S,"DEFAULT_ZNEAR",DEFAULT_ZNEAR);
 tolua_constant(tolua_S,"DEFAULT_ZFAR",DEFAULT_ZFAR);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"PerspectiveCamera","PerspectiveCamera","Camera",tolua_collect_PerspectiveCamera);
#else
 tolua_cclass(tolua_S,"PerspectiveCamera","PerspectiveCamera","Camera",NULL);
#endif
 tolua_beginmodule(tolua_S,"PerspectiveCamera");
 tolua_function(tolua_S,"new",tolua_luaPerspectiveCamera_PerspectiveCamera_new00);
 tolua_function(tolua_S,"delete",tolua_luaPerspectiveCamera_PerspectiveCamera_delete00);
 tolua_function(tolua_S,"place",tolua_luaPerspectiveCamera_PerspectiveCamera_place00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
