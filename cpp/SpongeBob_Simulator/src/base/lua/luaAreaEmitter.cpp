/*
** Lua binding: luaAreaEmitter
** Generated automatically by tolua 5.0a on 11/15/05 13:50:56.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaAreaEmitter_open (lua_State* tolua_S);

#include "..\AreaEmitter.h"
#include "..\Emitter.h"
#include "..\Plane.h"
#include "..\Point3f.h"
#include "..\Shape.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Plane (lua_State* tolua_S)
{
 Plane* self = (Plane*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"AreaEmitter");
 tolua_usertype(tolua_S,"Emitter");
 tolua_usertype(tolua_S,"Plane");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Shape",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,8,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,9,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,10,"Point3f",1,&tolua_err) ||
 !tolua_isboolean(tolua_S,11,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,12,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float emissionRate = ((float)  tolua_tonumber(tolua_S,2,1.0f));
  Shape* shape = ((Shape*)  tolua_tousertype(tolua_S,3,NULL));
  Point3f* position = ((Point3f*)  tolua_tousertype(tolua_S,4,NULL));
  float life = ((float)  tolua_tonumber(tolua_S,5,-1.0));
  float size = ((float)  tolua_tonumber(tolua_S,6,1.0f));
  float mass = ((float)  tolua_tonumber(tolua_S,7,1.0f));
  Point3f* speed = ((Point3f*)  tolua_tousertype(tolua_S,8,NULL));
  float angle = ((float)  tolua_tonumber(tolua_S,9,0.0));
  Point3f* axis = ((Point3f*)  tolua_tousertype(tolua_S,10,NULL));
  bool active = ((bool)  tolua_toboolean(tolua_S,11,true));
 {
  AreaEmitter* tolua_ret = (AreaEmitter*)  new AreaEmitter(emissionRate,shape,position,life,size,mass,speed,angle,axis,active);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"AreaEmitter");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: emit of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_emit00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
  unsigned int quantity = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'emit'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->emit(quantity);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'emit'.",&tolua_err);
 return 0;
#endif
}

/* method: setPlane of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_setPlane00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Plane",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
  Plane* plane = ((Plane*)  tolua_tousertype(tolua_S,2,0));
  float width = ((float)  tolua_tonumber(tolua_S,3,0));
  float height = ((float)  tolua_tonumber(tolua_S,4,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setPlane'",NULL);
#endif
 {
  self->setPlane(plane,width,height);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setPlane'.",&tolua_err);
 return 0;
#endif
}

/* method: getPlane of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_getPlane00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getPlane'",NULL);
#endif
 {
  Plane tolua_ret =  self->getPlane();
 {
#ifdef __cplusplus
 void* tolua_obj = new Plane(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Plane),"Plane");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Plane));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Plane");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getPlane'.",&tolua_err);
 return 0;
#endif
}

/* method: setWidth of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_setWidth00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
  float width = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setWidth'",NULL);
#endif
 {
  self->setWidth(width);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setWidth'.",&tolua_err);
 return 0;
#endif
}

/* method: getWidth of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_getWidth00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getWidth'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getWidth();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getWidth'.",&tolua_err);
 return 0;
#endif
}

/* method: setHeight of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_setHeight00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
  float height = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setHeight'",NULL);
#endif
 {
  self->setHeight(height);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setHeight'.",&tolua_err);
 return 0;
#endif
}

/* method: getHeight of class  AreaEmitter */
static int tolua_luaAreaEmitter_AreaEmitter_getHeight00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"AreaEmitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  AreaEmitter* self = (AreaEmitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getHeight'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getHeight();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getHeight'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaAreaEmitter_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"AreaEmitter","AreaEmitter","Emitter",NULL);
 tolua_beginmodule(tolua_S,"AreaEmitter");
 tolua_function(tolua_S,"new",tolua_luaAreaEmitter_AreaEmitter_new00);
 tolua_function(tolua_S,"emit",tolua_luaAreaEmitter_AreaEmitter_emit00);
 tolua_function(tolua_S,"setPlane",tolua_luaAreaEmitter_AreaEmitter_setPlane00);
 tolua_function(tolua_S,"getPlane",tolua_luaAreaEmitter_AreaEmitter_getPlane00);
 tolua_function(tolua_S,"setWidth",tolua_luaAreaEmitter_AreaEmitter_setWidth00);
 tolua_function(tolua_S,"getWidth",tolua_luaAreaEmitter_AreaEmitter_getWidth00);
 tolua_function(tolua_S,"setHeight",tolua_luaAreaEmitter_AreaEmitter_setHeight00);
 tolua_function(tolua_S,"getHeight",tolua_luaAreaEmitter_AreaEmitter_getHeight00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
