/*
** Lua binding: luaPlane
** Generated automatically by tolua 5.0a on 12/07/05 00:34:47.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaPlane_open (lua_State* tolua_S);

#include "..\Point3f.h"
#include "..\Plane.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"Plane");
}

/* get function: right of class  Plane */
static int tolua_get_Plane_Plane_right(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'right'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)&self->right,"Point3f");
 return 1;
}

/* set function: right of class  Plane */
static int tolua_set_Plane_Plane_right(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'right'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->right = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* get function: top of class  Plane */
static int tolua_get_Plane_Plane_top(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'top'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)&self->top,"Point3f");
 return 1;
}

/* set function: top of class  Plane */
static int tolua_set_Plane_Plane_top(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'top'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->top = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* get function: normal of class  Plane */
static int tolua_get_Plane_Plane_normal(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'normal'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)&self->normal,"Point3f");
 return 1;
}

/* set function: normal of class  Plane */
static int tolua_set_Plane_Plane_normal(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'normal'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->normal = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* get function: d of class  Plane */
static int tolua_get_Plane_Plane_d(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'd'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->d);
 return 1;
}

/* set function: d of class  Plane */
static int tolua_set_Plane_Plane_d(lua_State* tolua_S)
{
  Plane* self = (Plane*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'd'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->d = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* method: new of class  Plane */
static int tolua_luaPlane_Plane_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Plane",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* right = ((Point3f*)  tolua_tousertype(tolua_S,2,NULL));
  Point3f* top = ((Point3f*)  tolua_tousertype(tolua_S,3,NULL));
  float d = ((float)  tolua_tonumber(tolua_S,4,0.0));
 {
  Plane* tolua_ret = (Plane*)  new Plane(right,top,d);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Plane");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaPlane_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Plane","Plane","",NULL);
 tolua_beginmodule(tolua_S,"Plane");
 tolua_variable(tolua_S,"right",tolua_get_Plane_Plane_right,tolua_set_Plane_Plane_right);
 tolua_variable(tolua_S,"top",tolua_get_Plane_Plane_top,tolua_set_Plane_Plane_top);
 tolua_variable(tolua_S,"normal",tolua_get_Plane_Plane_normal,tolua_set_Plane_Plane_normal);
 tolua_variable(tolua_S,"d",tolua_get_Plane_Plane_d,tolua_set_Plane_Plane_d);
 tolua_function(tolua_S,"new",tolua_luaPlane_Plane_new00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
