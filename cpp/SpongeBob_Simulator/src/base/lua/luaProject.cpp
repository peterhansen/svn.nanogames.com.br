/*
** Lua binding: Project
** Generated automatically by tolua 5.0a on 11/08/05 23:29:18.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_Project_open (lua_State* tolua_S);

#include "..\Project.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Project");
}

/* Open function */
TOLUA_API int tolua_Project_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Project","Project","",NULL);
 tolua_beginmodule(tolua_S,"Project");
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
