/*
** Lua binding: luaParticle
** Generated automatically by tolua 5.0a on 12/04/05 02:20:04.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaParticle_open (lua_State* tolua_S);

#include "..\Point3f.h"
#include "..\Shape.h"
#include "..\DynamicVector.h"
#include "..\Random.h"
#include "..\Particle.h"
#include "..\Plane.h"
#include "..\Node.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Particle (lua_State* tolua_S)
{
 Particle* self = (Particle*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}

static int tolua_collect_Point3f (lua_State* tolua_S)
{
 Point3f* self = (Point3f*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"DynamicVector");
 tolua_usertype(tolua_S,"Plane");
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Particle");
 tolua_usertype(tolua_S,"Node");
}

/* method: new of class  Particle */
static int tolua_luaParticle_Particle_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Shape",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,7,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,8,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,9,"Point3f",1,&tolua_err) ||
 !tolua_isboolean(tolua_S,10,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,11,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Shape* shape = ((Shape*)  tolua_tousertype(tolua_S,2,NULL));
  Point3f* position = ((Point3f*)  tolua_tousertype(tolua_S,3,NULL));
  float life = ((float)  tolua_tonumber(tolua_S,4,MAX_DEFAULT_LIFE));
  float size = ((float)  tolua_tonumber(tolua_S,5,1.0f));
  float mass = ((float)  tolua_tonumber(tolua_S,6,1.0f));
  Point3f* speed = ((Point3f*)  tolua_tousertype(tolua_S,7,NULL));
  float angle = ((float)  tolua_tonumber(tolua_S,8,0.0));
  Point3f* axis = ((Point3f*)  tolua_tousertype(tolua_S,9,NULL));
  bool active = ((bool)  tolua_toboolean(tolua_S,10,true));
 {
  Particle* tolua_ret = (Particle*)  new Particle(shape,position,life,size,mass,speed,angle,axis,active);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Particle");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Particle */
static int tolua_luaParticle_Particle_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  Particle */
static int tolua_luaParticle_Particle_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
  DynamicVector* forces = ((DynamicVector*)  tolua_tousertype(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->update(time,forces);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Particle */
static int tolua_luaParticle_Particle_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: setActive of class  Particle */
static int tolua_luaParticle_Particle_setActive00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  bool active = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setActive'",NULL);
#endif
 {
  self->setActive(active);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setActive'.",&tolua_err);
 return 0;
#endif
}

/* method: isActive of class  Particle */
static int tolua_luaParticle_Particle_isActive00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isActive'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->isActive();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isActive'.",&tolua_err);
 return 0;
#endif
}

/* method: setShape of class  Particle */
static int tolua_luaParticle_Particle_setShape00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Shape",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  Shape* pMyShape = ((Shape*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setShape'",NULL);
#endif
 {
  self->setShape(pMyShape);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setShape'.",&tolua_err);
 return 0;
#endif
}

/* method: getShape of class  Particle */
static int tolua_luaParticle_Particle_getShape00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getShape'",NULL);
#endif
 {
  Shape* tolua_ret = (Shape*)  self->getShape();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Shape");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getShape'.",&tolua_err);
 return 0;
#endif
}

/* method: getLife of class  Particle */
static int tolua_luaParticle_Particle_getLife00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getLife'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getLife();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getLife'.",&tolua_err);
 return 0;
#endif
}

/* method: setLife of class  Particle */
static int tolua_luaParticle_Particle_setLife00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  float life = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setLife'",NULL);
#endif
 {
  self->setLife(life);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setLife'.",&tolua_err);
 return 0;
#endif
}

/* method: getSize of class  Particle */
static int tolua_luaParticle_Particle_getSize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSize'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getSize();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSize'.",&tolua_err);
 return 0;
#endif
}

/* method: setSize of class  Particle */
static int tolua_luaParticle_Particle_setSize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  float size = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setSize'",NULL);
#endif
 {
  self->setSize(size);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setSize'.",&tolua_err);
 return 0;
#endif
}

/* method: getMass of class  Particle */
static int tolua_luaParticle_Particle_getMass00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getMass'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getMass();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getMass'.",&tolua_err);
 return 0;
#endif
}

/* method: setMass of class  Particle */
static int tolua_luaParticle_Particle_setMass00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  float mass = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setMass'",NULL);
#endif
 {
  self->setMass(mass);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setMass'.",&tolua_err);
 return 0;
#endif
}

/* method: getAngle of class  Particle */
static int tolua_luaParticle_Particle_getAngle00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getAngle'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getAngle();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getAngle'.",&tolua_err);
 return 0;
#endif
}

/* method: setAngle of class  Particle */
static int tolua_luaParticle_Particle_setAngle00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  float angle = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setAngle'",NULL);
#endif
 {
  self->setAngle(angle);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setAngle'.",&tolua_err);
 return 0;
#endif
}

/* method: getAxis of class  Particle */
static int tolua_luaParticle_Particle_getAxis00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getAxis'",NULL);
#endif
 {
  Point3f tolua_ret =  self->getAxis();
 {
#ifdef __cplusplus
 void* tolua_obj = new Point3f(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Point3f),"Point3f");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Point3f));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Point3f");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getAxis'.",&tolua_err);
 return 0;
#endif
}

/* method: setAxis of class  Particle */
static int tolua_luaParticle_Particle_setAxis00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  Point3f* axis = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setAxis'",NULL);
#endif
 {
  self->setAxis(axis);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setAxis'.",&tolua_err);
 return 0;
#endif
}

/* method: isImmune of class  Particle */
static int tolua_luaParticle_Particle_isImmune00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isImmune'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->isImmune();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isImmune'.",&tolua_err);
 return 0;
#endif
}

/* method: setImmune of class  Particle */
static int tolua_luaParticle_Particle_setImmune00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  bool immune = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setImmune'",NULL);
#endif
 {
  self->setImmune(immune);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setImmune'.",&tolua_err);
 return 0;
#endif
}

/* method: isImmortal of class  Particle */
static int tolua_luaParticle_Particle_isImmortal00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isImmortal'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->isImmortal();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isImmortal'.",&tolua_err);
 return 0;
#endif
}

/* method: setImmortal of class  Particle */
static int tolua_luaParticle_Particle_setImmortal00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  bool immortal = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setImmortal'",NULL);
#endif
 {
  self->setImmortal(immortal);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setImmortal'.",&tolua_err);
 return 0;
#endif
}

/* method: setVisible of class  Particle */
static int tolua_luaParticle_Particle_setVisible00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  bool visible = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setVisible'",NULL);
#endif
 {
  self->setVisible(visible);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setVisible'.",&tolua_err);
 return 0;
#endif
}

/* method: isVisible of class  Particle */
static int tolua_luaParticle_Particle_isVisible00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isVisible'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->isVisible();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isVisible'.",&tolua_err);
 return 0;
#endif
}

/* method: setBasedOn of class  Particle */
static int tolua_luaParticle_Particle_setBasedOn00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  Particle* baseParticle = ((Particle*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setBasedOn'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->setBasedOn(baseParticle);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setBasedOn'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Particle */
static int tolua_luaParticle_Particle_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Particle* tolua_ret = (Particle*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Particle");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* method: getLimitPlaneAt of class  Particle */
static int tolua_luaParticle_Particle_getLimitPlaneAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getLimitPlaneAt'",NULL);
#endif
 {
  Plane* tolua_ret = (Plane*)  self->getLimitPlaneAt(index);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Plane");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getLimitPlaneAt'.",&tolua_err);
 return 0;
#endif
}

/* method: removeLimitPlaneAt of class  Particle */
static int tolua_luaParticle_Particle_removeLimitPlaneAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeLimitPlaneAt'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeLimitPlaneAt(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeLimitPlaneAt'.",&tolua_err);
 return 0;
#endif
}

/* method: insertLimitPlane of class  Particle */
static int tolua_luaParticle_Particle_insertLimitPlane00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Plane",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  Plane* limitPlane = ((Plane*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertLimitPlane'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertLimitPlane(limitPlane);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertLimitPlane'.",&tolua_err);
 return 0;
#endif
}

/* method: setLimitPlanes of class  Particle */
static int tolua_luaParticle_Particle_setLimitPlanes00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* self = (Particle*)  tolua_tousertype(tolua_S,1,0);
  DynamicVector* limitPlanes = ((DynamicVector*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setLimitPlanes'",NULL);
#endif
 {
  self->setLimitPlanes(limitPlanes);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setLimitPlanes'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaParticle_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"MAX_DEFAULT_LIFE",MAX_DEFAULT_LIFE);
 tolua_constant(tolua_S,"MAX_DEFAULT_MASS",MAX_DEFAULT_MASS);
 tolua_constant(tolua_S,"MAX_DEFAULT_SIZE",MAX_DEFAULT_SIZE);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Particle","Particle","Node",tolua_collect_Particle);
#else
 tolua_cclass(tolua_S,"Particle","Particle","Node",NULL);
#endif
 tolua_beginmodule(tolua_S,"Particle");
 tolua_function(tolua_S,"new",tolua_luaParticle_Particle_new00);
 tolua_function(tolua_S,"delete",tolua_luaParticle_Particle_delete00);
 tolua_function(tolua_S,"update",tolua_luaParticle_Particle_update00);
 tolua_function(tolua_S,"draw",tolua_luaParticle_Particle_draw00);
 tolua_function(tolua_S,"setActive",tolua_luaParticle_Particle_setActive00);
 tolua_function(tolua_S,"isActive",tolua_luaParticle_Particle_isActive00);
 tolua_function(tolua_S,"setShape",tolua_luaParticle_Particle_setShape00);
 tolua_function(tolua_S,"getShape",tolua_luaParticle_Particle_getShape00);
 tolua_function(tolua_S,"getLife",tolua_luaParticle_Particle_getLife00);
 tolua_function(tolua_S,"setLife",tolua_luaParticle_Particle_setLife00);
 tolua_function(tolua_S,"getSize",tolua_luaParticle_Particle_getSize00);
 tolua_function(tolua_S,"setSize",tolua_luaParticle_Particle_setSize00);
 tolua_function(tolua_S,"getMass",tolua_luaParticle_Particle_getMass00);
 tolua_function(tolua_S,"setMass",tolua_luaParticle_Particle_setMass00);
 tolua_function(tolua_S,"getAngle",tolua_luaParticle_Particle_getAngle00);
 tolua_function(tolua_S,"setAngle",tolua_luaParticle_Particle_setAngle00);
 tolua_function(tolua_S,"getAxis",tolua_luaParticle_Particle_getAxis00);
 tolua_function(tolua_S,"setAxis",tolua_luaParticle_Particle_setAxis00);
 tolua_function(tolua_S,"isImmune",tolua_luaParticle_Particle_isImmune00);
 tolua_function(tolua_S,"setImmune",tolua_luaParticle_Particle_setImmune00);
 tolua_function(tolua_S,"isImmortal",tolua_luaParticle_Particle_isImmortal00);
 tolua_function(tolua_S,"setImmortal",tolua_luaParticle_Particle_setImmortal00);
 tolua_function(tolua_S,"setVisible",tolua_luaParticle_Particle_setVisible00);
 tolua_function(tolua_S,"isVisible",tolua_luaParticle_Particle_isVisible00);
 tolua_function(tolua_S,"setBasedOn",tolua_luaParticle_Particle_setBasedOn00);
 tolua_function(tolua_S,"getCopy",tolua_luaParticle_Particle_getCopy00);
 tolua_function(tolua_S,"getLimitPlaneAt",tolua_luaParticle_Particle_getLimitPlaneAt00);
 tolua_function(tolua_S,"removeLimitPlaneAt",tolua_luaParticle_Particle_removeLimitPlaneAt00);
 tolua_function(tolua_S,"insertLimitPlane",tolua_luaParticle_Particle_insertLimitPlane00);
 tolua_function(tolua_S,"setLimitPlanes",tolua_luaParticle_Particle_setLimitPlanes00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
