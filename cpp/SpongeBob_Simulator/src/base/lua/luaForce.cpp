/*
** Lua binding: luaForce
** Generated automatically by tolua 5.0a on 11/27/05 12:25:03.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaForce_open (lua_State* tolua_S);

#include "..\Force.h"
#include "..\Shape.h"
#include "..\Point3f.h"
#include "..\Particle.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Force");
 tolua_usertype(tolua_S,"Particle");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  Force */
static int tolua_luaForce_Force_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Force",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f direction = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
 {
  Force* tolua_ret = (Force*)  new Force(direction);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Force");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  Force */
static int tolua_luaForce_Force_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Force",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Force* self = (Force*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  self->update(time);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: setDirection of class  Force */
static int tolua_luaForce_Force_setDirection00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Force",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Force* self = (Force*)  tolua_tousertype(tolua_S,1,0);
  Point3f* direction = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setDirection'",NULL);
#endif
 {
  self->setDirection(direction);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setDirection'.",&tolua_err);
 return 0;
#endif
}

/* method: getDirection of class  Force */
static int tolua_luaForce_Force_getDirection00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Force",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Force* self = (Force*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getDirection'",NULL);
#endif
 {
  Point3f* tolua_ret = (Point3f*)  self->getDirection();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Point3f");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getDirection'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Force */
static int tolua_luaForce_Force_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Force",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Force* self = (Force*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Particle* tolua_ret = (Particle*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Particle");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* method: setBasedOn of class  Force */
static int tolua_luaForce_Force_setBasedOn00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Force",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Force* self = (Force*)  tolua_tousertype(tolua_S,1,0);
  Particle* baseForce = ((Particle*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setBasedOn'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->setBasedOn(baseForce);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setBasedOn'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaForce_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Force","Force","Particle",NULL);
 tolua_beginmodule(tolua_S,"Force");
 tolua_function(tolua_S,"new",tolua_luaForce_Force_new00);
 tolua_function(tolua_S,"update",tolua_luaForce_Force_update00);
 tolua_function(tolua_S,"setDirection",tolua_luaForce_Force_setDirection00);
 tolua_function(tolua_S,"getDirection",tolua_luaForce_Force_getDirection00);
 tolua_function(tolua_S,"getCopy",tolua_luaForce_Force_getCopy00);
 tolua_function(tolua_S,"setBasedOn",tolua_luaForce_Force_setBasedOn00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
