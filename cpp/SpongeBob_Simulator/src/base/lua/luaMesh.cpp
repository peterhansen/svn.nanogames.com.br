/*
** Lua binding: luaMesh
** Generated automatically by tolua 5.0a on 11/14/05 01:40:17.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaMesh_open (lua_State* tolua_S);

#include "..\Point3f.h"
#include "..\Texture.h"
#include "..\Shape.h"
#include "..\Mesh.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Mesh (lua_State* tolua_S)
{
 Mesh* self = (Mesh*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Mesh");
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Texture");
}

/* get function: center of class  Mesh */
static int tolua_get_Mesh_Mesh_center(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'center'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)&self->center,"Point3f");
 return 1;
}

/* set function: center of class  Mesh */
static int tolua_set_Mesh_Mesh_center(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'center'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->center = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* get function: nVertices of class  Mesh */
static int tolua_get_Mesh_Mesh_nVertices(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'nVertices'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->nVertices);
 return 1;
}

/* set function: nVertices of class  Mesh */
static int tolua_set_Mesh_Mesh_nVertices(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'nVertices'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->nVertices = ((int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: nNormals of class  Mesh */
static int tolua_get_Mesh_Mesh_nNormals(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'nNormals'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->nNormals);
 return 1;
}

/* set function: nNormals of class  Mesh */
static int tolua_set_Mesh_Mesh_nNormals(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'nNormals'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->nNormals = ((int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: nTriangles of class  Mesh */
static int tolua_get_Mesh_Mesh_nTriangles(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'nTriangles'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->nTriangles);
 return 1;
}

/* set function: nTriangles of class  Mesh */
static int tolua_set_Mesh_Mesh_nTriangles(lua_State* tolua_S)
{
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'nTriangles'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->nTriangles = ((int)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* method: createMesh of class  Mesh */
static int tolua_luaMesh_Mesh_createMesh00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Mesh",0,&tolua_err) ||
 !tolua_isstring(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  char* filename = ((char*)  tolua_tostring(tolua_S,2,0));
  Point3f* center = ((Point3f*)  tolua_tousertype(tolua_S,3,NULL));
 {
  Mesh* tolua_ret = (Mesh*)  Mesh::createMesh(filename,center);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Mesh");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'createMesh'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Mesh */
static int tolua_luaMesh_Mesh_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Mesh",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: setTexture of class  Mesh */
static int tolua_luaMesh_Mesh_setTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Mesh",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Texture",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
  Texture* t = ((Texture*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setTexture'",NULL);
#endif
 {
  self->setTexture(t);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setTexture'.",&tolua_err);
 return 0;
#endif
}

/* method: getTexture of class  Mesh */
static int tolua_luaMesh_Mesh_getTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Mesh",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTexture'",NULL);
#endif
 {
  Texture* tolua_ret = (Texture*)  self->getTexture();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Texture");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTexture'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Mesh */
static int tolua_luaMesh_Mesh_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Mesh",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Mesh* self = (Mesh*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaMesh_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Mesh","Mesh","Shape",tolua_collect_Mesh);
#else
 tolua_cclass(tolua_S,"Mesh","Mesh","Shape",NULL);
#endif
 tolua_beginmodule(tolua_S,"Mesh");
 tolua_variable(tolua_S,"center",tolua_get_Mesh_Mesh_center,tolua_set_Mesh_Mesh_center);
 tolua_variable(tolua_S,"nVertices",tolua_get_Mesh_Mesh_nVertices,tolua_set_Mesh_Mesh_nVertices);
 tolua_variable(tolua_S,"nNormals",tolua_get_Mesh_Mesh_nNormals,tolua_set_Mesh_Mesh_nNormals);
 tolua_variable(tolua_S,"nTriangles",tolua_get_Mesh_Mesh_nTriangles,tolua_set_Mesh_Mesh_nTriangles);
 tolua_function(tolua_S,"createMesh",tolua_luaMesh_Mesh_createMesh00);
 tolua_function(tolua_S,"delete",tolua_luaMesh_Mesh_delete00);
 tolua_function(tolua_S,"setTexture",tolua_luaMesh_Mesh_setTexture00);
 tolua_function(tolua_S,"getTexture",tolua_luaMesh_Mesh_getTexture00);
 tolua_function(tolua_S,"draw",tolua_luaMesh_Mesh_draw00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
