/*
** Lua binding: luaSpotLight
** Generated automatically by tolua 5.0a on 11/27/05 02:01:52.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaSpotLight_open (lua_State* tolua_S);

#include "..\SpotLight.h"
#include "..\Light.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_SpotLight (lua_State* tolua_S)
{
 SpotLight* self = (SpotLight*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Color");
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"SpotLight");
 tolua_usertype(tolua_S,"Light");
}

/* method: new of class  SpotLight */
static int tolua_luaSpotLight_SpotLight_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"SpotLight",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Color",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,5,"Point3f",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,8,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* pos = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
  Color color = *((Color*)  tolua_tousertype(tolua_S,3,0));
  unsigned int lightId = ((unsigned int)  tolua_tonumber(tolua_S,4,0));
  Point3f* direction = ((Point3f*)  tolua_tousertype(tolua_S,5,0));
  int angle = ((int)  tolua_tonumber(tolua_S,6,0));
  int atenuationExponent = ((int)  tolua_tonumber(tolua_S,7,0));
 {
  SpotLight* tolua_ret = (SpotLight*)  new SpotLight(pos,color,lightId,direction,angle,atenuationExponent);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"SpotLight");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  SpotLight */
static int tolua_luaSpotLight_SpotLight_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SpotLight",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SpotLight* self = (SpotLight*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: enable of class  SpotLight */
static int tolua_luaSpotLight_SpotLight_enable00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SpotLight",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SpotLight* self = (SpotLight*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'enable'",NULL);
#endif
 {
  self->enable();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'enable'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaSpotLight_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"SpotLight","SpotLight","Light",tolua_collect_SpotLight);
#else
 tolua_cclass(tolua_S,"SpotLight","SpotLight","Light",NULL);
#endif
 tolua_beginmodule(tolua_S,"SpotLight");
 tolua_function(tolua_S,"new",tolua_luaSpotLight_SpotLight_new00);
 tolua_function(tolua_S,"delete",tolua_luaSpotLight_SpotLight_delete00);
 tolua_function(tolua_S,"enable",tolua_luaSpotLight_SpotLight_enable00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
