/*
** Lua binding: luaCamera
** Generated automatically by tolua 5.0a on 11/27/05 02:09:33.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaCamera_open (lua_State* tolua_S);

#include "..\Point3f.h"
#include "..\Camera.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Camera (lua_State* tolua_S)
{
 Camera* self = (Camera*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Camera");
 tolua_usertype(tolua_S,"Node");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  Camera */
static int tolua_luaCamera_Camera_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Camera",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* myCenter = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
  Point3f* myEye = ((Point3f*)  tolua_tousertype(tolua_S,3,0));
  Point3f* myUp = ((Point3f*)  tolua_tousertype(tolua_S,4,0));
 {
  Camera* tolua_ret = (Camera*)  new Camera(myCenter,myEye,myUp);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Camera");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Camera */
static int tolua_luaCamera_Camera_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Camera",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Camera* self = (Camera*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: place of class  Camera */
static int tolua_luaCamera_Camera_place00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Camera",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Camera* self = (Camera*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'place'",NULL);
#endif
 {
  self->place();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'place'.",&tolua_err);
 return 0;
#endif
}

/* method: move of class  Camera */
static int tolua_luaCamera_Camera_move00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Camera",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Camera* self = (Camera*)  tolua_tousertype(tolua_S,1,0);
  Point3f* offset = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'move'",NULL);
#endif
 {
  self->move(offset);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'move'.",&tolua_err);
 return 0;
#endif
}

/* method: rotate of class  Camera */
static int tolua_luaCamera_Camera_rotate00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Camera",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Camera* self = (Camera*)  tolua_tousertype(tolua_S,1,0);
  Point3f* angle = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'rotate'",NULL);
#endif
 {
  self->rotate(angle);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'rotate'.",&tolua_err);
 return 0;
#endif
}

/* method: attach of class  Camera */
static int tolua_luaCamera_Camera_attach00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Camera",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Node",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Camera* self = (Camera*)  tolua_tousertype(tolua_S,1,0);
  Node* pivot = ((Node*)  tolua_tousertype(tolua_S,2,0));
  unsigned int attachType = ((unsigned int)  tolua_tonumber(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'attach'",NULL);
#endif
 {
  self->attach(pivot,attachType);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'attach'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaCamera_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"POSITION",POSITION);
 tolua_constant(tolua_S,"EYE",EYE);
 tolua_constant(tolua_S,"UP",UP);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Camera","Camera","Node",tolua_collect_Camera);
#else
 tolua_cclass(tolua_S,"Camera","Camera","Node",NULL);
#endif
 tolua_beginmodule(tolua_S,"Camera");
 tolua_function(tolua_S,"new",tolua_luaCamera_Camera_new00);
 tolua_function(tolua_S,"delete",tolua_luaCamera_Camera_delete00);
 tolua_function(tolua_S,"place",tolua_luaCamera_Camera_place00);
 tolua_function(tolua_S,"move",tolua_luaCamera_Camera_move00);
 tolua_function(tolua_S,"rotate",tolua_luaCamera_Camera_rotate00);
 tolua_function(tolua_S,"attach",tolua_luaCamera_Camera_attach00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
