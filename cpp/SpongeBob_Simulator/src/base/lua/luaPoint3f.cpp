/*
** Lua binding: luaPoint3f
** Generated automatically by tolua 5.0a on 11/25/05 21:11:59.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaPoint3f_open (lua_State* tolua_S);

#include "..\Point3f.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Point3f (lua_State* tolua_S)
{
 Point3f* self = (Point3f*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  Point3f */
static int tolua_luaPoint3f_Point3f_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float x = ((float)  tolua_tonumber(tolua_S,2,0.0f));
  float y = ((float)  tolua_tonumber(tolua_S,3,0.0f));
  float z = ((float)  tolua_tonumber(tolua_S,4,0.0f));
 {
  Point3f* tolua_ret = (Point3f*)  new Point3f(x,y,z);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Point3f");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: setPoint of class  Point3f */
static int tolua_luaPoint3f_Point3f_setPoint00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
  float x = ((float)  tolua_tonumber(tolua_S,2,0.0f));
  float y = ((float)  tolua_tonumber(tolua_S,3,0.0f));
  float z = ((float)  tolua_tonumber(tolua_S,4,0.0f));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setPoint'",NULL);
#endif
 {
  self->setPoint(x,y,z);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setPoint'.",&tolua_err);
 return 0;
#endif
}

/* method: getModule of class  Point3f */
static int tolua_luaPoint3f_Point3f_getModule00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getModule'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getModule();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getModule'.",&tolua_err);
 return 0;
#endif
}

/* method: normalize of class  Point3f */
static int tolua_luaPoint3f_Point3f_normalize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'normalize'",NULL);
#endif
 {
  self->normalize();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'normalize'.",&tolua_err);
 return 0;
#endif
}

/* method: operator+ of class  Point3f */
static int tolua_luaPoint3f_Point3f_operator_add00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
  Point3f* tolua_var_1 = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator+'",NULL);
#endif
 {
  Point3f tolua_ret =  self->operator+(*tolua_var_1);
 {
#ifdef __cplusplus
 void* tolua_obj = new Point3f(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Point3f),"Point3f");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Point3f));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Point3f");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function '.add'.",&tolua_err);
 return 0;
#endif
}

/* method: operator- of class  Point3f */
static int tolua_luaPoint3f_Point3f_operator_sub00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
  Point3f* tolua_var_2 = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator-'",NULL);
#endif
 {
  Point3f tolua_ret =  self->operator-(*tolua_var_2);
 {
#ifdef __cplusplus
 void* tolua_obj = new Point3f(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Point3f),"Point3f");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Point3f));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Point3f");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function '.sub'.",&tolua_err);
 return 0;
#endif
}

/* method: operator* of class  Point3f */
static int tolua_luaPoint3f_Point3f_operator_mul00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
  float tolua_var_3 = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator*'",NULL);
#endif
 {
  Point3f tolua_ret =  self->operator*(tolua_var_3);
 {
#ifdef __cplusplus
 void* tolua_obj = new Point3f(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Point3f),"Point3f");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Point3f));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Point3f");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function '.mul'.",&tolua_err);
 return 0;
#endif
}

/* method: operator/ of class  Point3f */
static int tolua_luaPoint3f_Point3f_operator_div00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point3f",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* self = (Point3f*)  tolua_tousertype(tolua_S,1,0);
  float tolua_var_4 = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator/'",NULL);
#endif
 {
  Point3f tolua_ret =  self->operator/(tolua_var_4);
 {
#ifdef __cplusplus
 void* tolua_obj = new Point3f(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Point3f),"Point3f");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Point3f));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Point3f");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function '.div'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaPoint3f_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Point3f","Point3f","",tolua_collect_Point3f);
#else
 tolua_cclass(tolua_S,"Point3f","Point3f","",NULL);
#endif
 tolua_beginmodule(tolua_S,"Point3f");
 tolua_function(tolua_S,"new",tolua_luaPoint3f_Point3f_new00);
 tolua_function(tolua_S,"setPoint",tolua_luaPoint3f_Point3f_setPoint00);
 tolua_function(tolua_S,"getModule",tolua_luaPoint3f_Point3f_getModule00);
 tolua_function(tolua_S,"normalize",tolua_luaPoint3f_Point3f_normalize00);
 tolua_function(tolua_S,".add",tolua_luaPoint3f_Point3f_operator_add00);
 tolua_function(tolua_S,".sub",tolua_luaPoint3f_Point3f_operator_sub00);
 tolua_function(tolua_S,".mul",tolua_luaPoint3f_Point3f_operator_mul00);
 tolua_function(tolua_S,".div",tolua_luaPoint3f_Point3f_operator_div00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
