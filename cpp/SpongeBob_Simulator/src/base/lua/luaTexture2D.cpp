/*
** Lua binding: luaTexture2D
** Generated automatically by tolua 5.0a on 11/14/05 01:33:48.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaTexture2D_open (lua_State* tolua_S);

#include "..\Texture.h"
#include "..\Point3f.h"
#include "..\Color.h"
#include "..\Texture2D.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Texture2D (lua_State* tolua_S)
{
 Texture2D* self = (Texture2D*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Color");
 tolua_usertype(tolua_S,"Texture2D");
 tolua_usertype(tolua_S,"Texture");
}

/* method: new of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Texture2D* tolua_ret = (Texture2D*)  new Texture2D();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Texture2D");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: loadTexture of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_loadTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isstring(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Color",1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
  char* filename = ((char*)  tolua_tostring(tolua_S,2,0));
  Color* transparentColor = ((Color*)  tolua_tousertype(tolua_S,3,NULL));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'loadTexture'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->loadTexture(filename,transparentColor);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'loadTexture'.",&tolua_err);
 return 0;
#endif
}

/* method: getSize of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_getSize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
  unsigned int pWidth = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
  unsigned int pHeight = ((unsigned int)  tolua_tonumber(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSize'",NULL);
#endif
 {
  self->getSize(&pWidth,&pHeight);
 tolua_pushnumber(tolua_S,(lua_Number)pWidth);
 tolua_pushnumber(tolua_S,(lua_Number)pHeight);
 }
 }
 return 2;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSize'.",&tolua_err);
 return 0;
#endif
}

/* method: setSize of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_setSize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
  unsigned int width = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
  unsigned int height = ((unsigned int)  tolua_tonumber(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setSize'",NULL);
#endif
 {
  self->setSize(width,height);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setSize'.",&tolua_err);
 return 0;
#endif
}

/* method: getTextureData of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_getTextureData00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureData'",NULL);
#endif
 {
  void* tolua_ret = (void*)  self->getTextureData();
 tolua_pushuserdata(tolua_S,(void*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureData'.",&tolua_err);
 return 0;
#endif
}

/* method: load of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_load00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'load'",NULL);
#endif
 {
  self->load();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'load'.",&tolua_err);
 return 0;
#endif
}

/* method: unload of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_unload00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'unload'",NULL);
#endif
 {
  self->unload();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'unload'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Texture2D */
static int tolua_luaTexture2D_Texture2D_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture2D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture2D* self = (Texture2D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Texture2D* tolua_ret = (Texture2D*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Texture2D");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaTexture2D_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"BITMAP_ID",BITMAP_ID);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Texture2D","Texture2D","Texture",tolua_collect_Texture2D);
#else
 tolua_cclass(tolua_S,"Texture2D","Texture2D","Texture",NULL);
#endif
 tolua_beginmodule(tolua_S,"Texture2D");
 tolua_function(tolua_S,"new",tolua_luaTexture2D_Texture2D_new00);
 tolua_function(tolua_S,"delete",tolua_luaTexture2D_Texture2D_delete00);
 tolua_function(tolua_S,"loadTexture",tolua_luaTexture2D_Texture2D_loadTexture00);
 tolua_function(tolua_S,"getSize",tolua_luaTexture2D_Texture2D_getSize00);
 tolua_function(tolua_S,"setSize",tolua_luaTexture2D_Texture2D_setSize00);
 tolua_function(tolua_S,"getTextureData",tolua_luaTexture2D_Texture2D_getTextureData00);
 tolua_function(tolua_S,"load",tolua_luaTexture2D_Texture2D_load00);
 tolua_function(tolua_S,"unload",tolua_luaTexture2D_Texture2D_unload00);
 tolua_function(tolua_S,"getCopy",tolua_luaTexture2D_Texture2D_getCopy00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
