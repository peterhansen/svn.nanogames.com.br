/*
** Lua binding: luaLineEmitter
** Generated automatically by tolua 5.0a on 11/15/05 13:50:51.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaLineEmitter_open (lua_State* tolua_S);

#include "..\LineEmitter.h"
#include "..\Emitter.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Point3f (lua_State* tolua_S)
{
 Point3f* self = (Point3f*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"LineEmitter");
 tolua_usertype(tolua_S,"Emitter");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  LineEmitter */
static int tolua_luaLineEmitter_LineEmitter_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"LineEmitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Shape",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,8,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,9,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,10,"Point3f",1,&tolua_err) ||
 !tolua_isboolean(tolua_S,11,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,12,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float emissionRate = ((float)  tolua_tonumber(tolua_S,2,1.0f));
  Shape* shape = ((Shape*)  tolua_tousertype(tolua_S,3,NULL));
  Point3f* position = ((Point3f*)  tolua_tousertype(tolua_S,4,NULL));
  float life = ((float)  tolua_tonumber(tolua_S,5,-1.0));
  float size = ((float)  tolua_tonumber(tolua_S,6,1.0f));
  float mass = ((float)  tolua_tonumber(tolua_S,7,1.0f));
  Point3f* speed = ((Point3f*)  tolua_tousertype(tolua_S,8,NULL));
  float angle = ((float)  tolua_tonumber(tolua_S,9,0.0));
  Point3f* axis = ((Point3f*)  tolua_tousertype(tolua_S,10,NULL));
  bool active = ((bool)  tolua_toboolean(tolua_S,11,true));
 {
  LineEmitter* tolua_ret = (LineEmitter*)  new LineEmitter(emissionRate,shape,position,life,size,mass,speed,angle,axis,active);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"LineEmitter");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: emit of class  LineEmitter */
static int tolua_luaLineEmitter_LineEmitter_emit00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"LineEmitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  LineEmitter* self = (LineEmitter*)  tolua_tousertype(tolua_S,1,0);
  unsigned int quantity = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'emit'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->emit(quantity);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'emit'.",&tolua_err);
 return 0;
#endif
}

/* method: setEndPoint of class  LineEmitter */
static int tolua_luaLineEmitter_LineEmitter_setEndPoint00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"LineEmitter",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  LineEmitter* self = (LineEmitter*)  tolua_tousertype(tolua_S,1,0);
  Point3f endPoint = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setEndPoint'",NULL);
#endif
 {
  self->setEndPoint(endPoint);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setEndPoint'.",&tolua_err);
 return 0;
#endif
}

/* method: getEndPoint of class  LineEmitter */
static int tolua_luaLineEmitter_LineEmitter_getEndPoint00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"LineEmitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  LineEmitter* self = (LineEmitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getEndPoint'",NULL);
#endif
 {
  Point3f tolua_ret =  self->getEndPoint();
 {
#ifdef __cplusplus
 void* tolua_obj = new Point3f(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Point3f),"Point3f");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Point3f));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Point3f");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getEndPoint'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaLineEmitter_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"LineEmitter","LineEmitter","Emitter",NULL);
 tolua_beginmodule(tolua_S,"LineEmitter");
 tolua_function(tolua_S,"new",tolua_luaLineEmitter_LineEmitter_new00);
 tolua_function(tolua_S,"emit",tolua_luaLineEmitter_LineEmitter_emit00);
 tolua_function(tolua_S,"setEndPoint",tolua_luaLineEmitter_LineEmitter_setEndPoint00);
 tolua_function(tolua_S,"getEndPoint",tolua_luaLineEmitter_LineEmitter_getEndPoint00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
