/*
** Lua binding: luaRandom
** Generated automatically by tolua 5.0a on 11/09/05 00:10:14.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaRandom_open (lua_State* tolua_S);

#include "..\Random.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"UINT32");
 tolua_usertype(tolua_S,"Random");
}

/* method: setSeed of class  Random */
static int tolua_luaRandom_Random_setSeed00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Random",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"UINT32",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Random* self = (Random*)  tolua_tousertype(tolua_S,1,0);
  UINT32 s = *((UINT32*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setSeed'",NULL);
#endif
 {
  self->setSeed(s);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setSeed'.",&tolua_err);
 return 0;
#endif
}

/* method: getSeed of class  Random */
static int tolua_luaRandom_Random_getSeed00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Random",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Random* self = (Random*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSeed'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->getSeed();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSeed'.",&tolua_err);
 return 0;
#endif
}

/* method: nextDouble of class  Random */
static int tolua_luaRandom_Random_nextDouble00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Random",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  double tolua_ret = (double)  Random::nextDouble();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'nextDouble'.",&tolua_err);
 return 0;
#endif
}

/* method: nextDouble of class  Random */
static int tolua_luaRandom_Random_nextDouble01(lua_State* tolua_S)
{
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Random",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
 {
  double maxValue = ((double)  tolua_tonumber(tolua_S,2,0));
  double minValue = ((double)  tolua_tonumber(tolua_S,3,0.0));
 {
  double tolua_ret = (double)  Random::nextDouble(maxValue,minValue);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
tolua_lerror:
 return tolua_luaRandom_Random_nextDouble00(tolua_S);
}

/* Open function */
TOLUA_API int tolua_luaRandom_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Random","Random","",NULL);
 tolua_beginmodule(tolua_S,"Random");
 tolua_function(tolua_S,"setSeed",tolua_luaRandom_Random_setSeed00);
 tolua_function(tolua_S,"getSeed",tolua_luaRandom_Random_getSeed00);
 tolua_function(tolua_S,"nextDouble",tolua_luaRandom_Random_nextDouble00);
 tolua_function(tolua_S,"nextDouble",tolua_luaRandom_Random_nextDouble01);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
