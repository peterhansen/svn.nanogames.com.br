/*
** Lua binding: luaClock
** Generated automatically by tolua 5.0a on 12/03/05 17:51:18.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaClock_open (lua_State* tolua_S);

#include "..\Clock.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_UINT8 (lua_State* tolua_S)
{
 UINT8* self = (UINT8*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Clock");
 tolua_usertype(tolua_S,"UINT8");
}

/* method: new of class  Clock */
static int tolua_luaClock_Clock_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float tolua_var_1 = ((float)  tolua_tonumber(tolua_S,2,DEFAULT_XPOS));
  float tolua_var_2 = ((float)  tolua_tonumber(tolua_S,3,DEFAULT_YPOS));
 {
  Clock* tolua_ret = (Clock*)  new Clock(tolua_var_1,tolua_var_2);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Clock");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: getTime of class  Clock */
static int tolua_luaClock_Clock_getTime00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTime'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getTime();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTime'.",&tolua_err);
 return 0;
#endif
}

/* method: pause of class  Clock */
static int tolua_luaClock_Clock_pause00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'pause'",NULL);
#endif
 {
  self->pause();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'pause'.",&tolua_err);
 return 0;
#endif
}

/* method: resume of class  Clock */
static int tolua_luaClock_Clock_resume00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'resume'",NULL);
#endif
 {
  self->resume();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'resume'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  Clock */
static int tolua_luaClock_Clock_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  self->update();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Clock */
static int tolua_luaClock_Clock_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: setPos of class  Clock */
static int tolua_luaClock_Clock_setPos00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
  float tolua_var_3 = ((float)  tolua_tonumber(tolua_S,2,0));
  float tolua_var_4 = ((float)  tolua_tonumber(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setPos'",NULL);
#endif
 {
  self->setPos(tolua_var_3,tolua_var_4);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setPos'.",&tolua_err);
 return 0;
#endif
}

/* method: getPos of class  Clock */
static int tolua_luaClock_Clock_getPos00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
  float tolua_var_5 = ((float)  tolua_tonumber(tolua_S,2,0));
  float tolua_var_6 = ((float)  tolua_tonumber(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getPos'",NULL);
#endif
 {
  self->getPos(&tolua_var_5,&tolua_var_6);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_var_5);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_var_6);
 }
 }
 return 2;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getPos'.",&tolua_err);
 return 0;
#endif
}

/* method: setTimeMode of class  Clock */
static int tolua_luaClock_Clock_setTimeMode00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
  unsigned char timeMode = ((unsigned char)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setTimeMode'",NULL);
#endif
 {
  self->setTimeMode(timeMode);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setTimeMode'.",&tolua_err);
 return 0;
#endif
}

/* method: getTimeMode of class  Clock */
static int tolua_luaClock_Clock_getTimeMode00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTimeMode'",NULL);
#endif
 {
  UINT8 tolua_ret =  self->getTimeMode();
 {
#ifdef __cplusplus
 void* tolua_obj = new UINT8(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_UINT8),"UINT8");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(UINT8));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"UINT8");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTimeMode'.",&tolua_err);
 return 0;
#endif
}

/* method: setFixedFrameTime of class  Clock */
static int tolua_luaClock_Clock_setFixedFrameTime00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
  float fixedFrameTime = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setFixedFrameTime'",NULL);
#endif
 {
  self->setFixedFrameTime(fixedFrameTime);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setFixedFrameTime'.",&tolua_err);
 return 0;
#endif
}

/* method: getFixedFrameTime of class  Clock */
static int tolua_luaClock_Clock_getFixedFrameTime00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getFixedFrameTime'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getFixedFrameTime();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getFixedFrameTime'.",&tolua_err);
 return 0;
#endif
}

/* method: setMaxFrameTime of class  Clock */
static int tolua_luaClock_Clock_setMaxFrameTime00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
  float maxFrameTime = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setMaxFrameTime'",NULL);
#endif
 {
  self->setMaxFrameTime(maxFrameTime);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setMaxFrameTime'.",&tolua_err);
 return 0;
#endif
}

/* method: getMaxFrameTime of class  Clock */
static int tolua_luaClock_Clock_getMaxFrameTime00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getMaxFrameTime'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getMaxFrameTime();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getMaxFrameTime'.",&tolua_err);
 return 0;
#endif
}

/* method: setVisible of class  Clock */
static int tolua_luaClock_Clock_setVisible00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isboolean(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
  bool visible = ((bool)  tolua_toboolean(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setVisible'",NULL);
#endif
 {
  self->setVisible(visible);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setVisible'.",&tolua_err);
 return 0;
#endif
}

/* method: isVisible of class  Clock */
static int tolua_luaClock_Clock_isVisible00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Clock",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Clock* self = (Clock*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'isVisible'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->isVisible();
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'isVisible'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaClock_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"DEFAULT_XPOS",DEFAULT_XPOS);
 tolua_constant(tolua_S,"DEFAULT_YPOS",DEFAULT_YPOS);
 tolua_cclass(tolua_S,"Clock","Clock","",NULL);
 tolua_beginmodule(tolua_S,"Clock");
 tolua_function(tolua_S,"new",tolua_luaClock_Clock_new00);
 tolua_function(tolua_S,"getTime",tolua_luaClock_Clock_getTime00);
 tolua_function(tolua_S,"pause",tolua_luaClock_Clock_pause00);
 tolua_function(tolua_S,"resume",tolua_luaClock_Clock_resume00);
 tolua_function(tolua_S,"update",tolua_luaClock_Clock_update00);
 tolua_function(tolua_S,"draw",tolua_luaClock_Clock_draw00);
 tolua_function(tolua_S,"setPos",tolua_luaClock_Clock_setPos00);
 tolua_function(tolua_S,"getPos",tolua_luaClock_Clock_getPos00);
 tolua_function(tolua_S,"setTimeMode",tolua_luaClock_Clock_setTimeMode00);
 tolua_function(tolua_S,"getTimeMode",tolua_luaClock_Clock_getTimeMode00);
 tolua_function(tolua_S,"setFixedFrameTime",tolua_luaClock_Clock_setFixedFrameTime00);
 tolua_function(tolua_S,"getFixedFrameTime",tolua_luaClock_Clock_getFixedFrameTime00);
 tolua_function(tolua_S,"setMaxFrameTime",tolua_luaClock_Clock_setMaxFrameTime00);
 tolua_function(tolua_S,"getMaxFrameTime",tolua_luaClock_Clock_getMaxFrameTime00);
 tolua_function(tolua_S,"setVisible",tolua_luaClock_Clock_setVisible00);
 tolua_function(tolua_S,"isVisible",tolua_luaClock_Clock_isVisible00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
