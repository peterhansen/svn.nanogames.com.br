/*
** Lua binding: luaEmitter
** Generated automatically by tolua 5.0a on 11/27/05 12:25:11.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaEmitter_open (lua_State* tolua_S);

#include "..\Particle.h"
#include "..\DynamicVector.h"
#include "..\Force.h"
#include "..\Emitter.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Emitter (lua_State* tolua_S)
{
 Emitter* self = (Emitter*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"DynamicVector");
 tolua_usertype(tolua_S,"Emitter");
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"Particle");
 tolua_usertype(tolua_S,"Shape");
}

/* method: new of class  Emitter */
static int tolua_luaEmitter_Emitter_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Shape",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,8,"Point3f",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,9,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,10,"Point3f",1,&tolua_err) ||
 !tolua_isboolean(tolua_S,11,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,12,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float emissionRate = ((float)  tolua_tonumber(tolua_S,2,1.0f));
  Shape* shape = ((Shape*)  tolua_tousertype(tolua_S,3,NULL));
  Point3f* position = ((Point3f*)  tolua_tousertype(tolua_S,4,NULL));
  float life = ((float)  tolua_tonumber(tolua_S,5,-1.0));
  float size = ((float)  tolua_tonumber(tolua_S,6,1.0f));
  float mass = ((float)  tolua_tonumber(tolua_S,7,1.0f));
  Point3f* speed = ((Point3f*)  tolua_tousertype(tolua_S,8,NULL));
  float angle = ((float)  tolua_tonumber(tolua_S,9,0.0));
  Point3f* axis = ((Point3f*)  tolua_tousertype(tolua_S,10,NULL));
  bool active = ((bool)  tolua_toboolean(tolua_S,11,true));
 {
  Emitter* tolua_ret = (Emitter*)  new Emitter(emissionRate,shape,position,life,size,mass,speed,angle,axis,active);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Emitter");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Emitter */
static int tolua_luaEmitter_Emitter_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Emitter */
static int tolua_luaEmitter_Emitter_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  Emitter */
static int tolua_luaEmitter_Emitter_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
  DynamicVector* forces = ((DynamicVector*)  tolua_tousertype(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->update(time,forces);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: emit of class  Emitter */
static int tolua_luaEmitter_Emitter_emit00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
  unsigned int quantity = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'emit'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->emit(quantity);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'emit'.",&tolua_err);
 return 0;
#endif
}

/* method: setBaseElement of class  Emitter */
static int tolua_luaEmitter_Emitter_setBaseElement00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
  Particle* baseElement = ((Particle*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setBaseElement'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->setBaseElement(baseElement);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setBaseElement'.",&tolua_err);
 return 0;
#endif
}

/* method: getBaseElement of class  Emitter */
static int tolua_luaEmitter_Emitter_getBaseElement00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getBaseElement'",NULL);
#endif
 {
  Particle* tolua_ret = (Particle*)  self->getBaseElement();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Particle");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getBaseElement'.",&tolua_err);
 return 0;
#endif
}

/* method: setEmissionRate of class  Emitter */
static int tolua_luaEmitter_Emitter_setEmissionRate00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
  float emissionRate = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setEmissionRate'",NULL);
#endif
 {
  self->setEmissionRate(emissionRate);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setEmissionRate'.",&tolua_err);
 return 0;
#endif
}

/* method: getEmissionRate of class  Emitter */
static int tolua_luaEmitter_Emitter_getEmissionRate00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getEmissionRate'",NULL);
#endif
 {
  float tolua_ret = (float)  self->getEmissionRate();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getEmissionRate'.",&tolua_err);
 return 0;
#endif
}

/* method: setBasedOn of class  Emitter */
static int tolua_luaEmitter_Emitter_setBasedOn00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
  Particle* baseElement = ((Particle*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setBasedOn'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->setBasedOn(baseElement);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setBasedOn'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Emitter */
static int tolua_luaEmitter_Emitter_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Particle* tolua_ret = (Particle*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Particle");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* method: getCurrentActiveElements of class  Emitter */
static int tolua_luaEmitter_Emitter_getCurrentActiveElements00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Emitter",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Emitter* self = (Emitter*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCurrentActiveElements'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->getCurrentActiveElements();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCurrentActiveElements'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaEmitter_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Emitter","Emitter","Particle",tolua_collect_Emitter);
#else
 tolua_cclass(tolua_S,"Emitter","Emitter","Particle",NULL);
#endif
 tolua_beginmodule(tolua_S,"Emitter");
 tolua_function(tolua_S,"new",tolua_luaEmitter_Emitter_new00);
 tolua_function(tolua_S,"delete",tolua_luaEmitter_Emitter_delete00);
 tolua_function(tolua_S,"draw",tolua_luaEmitter_Emitter_draw00);
 tolua_function(tolua_S,"update",tolua_luaEmitter_Emitter_update00);
 tolua_function(tolua_S,"emit",tolua_luaEmitter_Emitter_emit00);
 tolua_function(tolua_S,"setBaseElement",tolua_luaEmitter_Emitter_setBaseElement00);
 tolua_function(tolua_S,"getBaseElement",tolua_luaEmitter_Emitter_getBaseElement00);
 tolua_function(tolua_S,"setEmissionRate",tolua_luaEmitter_Emitter_setEmissionRate00);
 tolua_function(tolua_S,"getEmissionRate",tolua_luaEmitter_Emitter_getEmissionRate00);
 tolua_function(tolua_S,"setBasedOn",tolua_luaEmitter_Emitter_setBasedOn00);
 tolua_function(tolua_S,"getCopy",tolua_luaEmitter_Emitter_getCopy00);
 tolua_function(tolua_S,"getCurrentActiveElements",tolua_luaEmitter_Emitter_getCurrentActiveElements00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
