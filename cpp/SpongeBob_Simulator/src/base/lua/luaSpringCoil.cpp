/*
** Lua binding: luaSpringCoil
** Generated automatically by tolua 5.0a on 12/06/05 23:56:11.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaSpringCoil_open (lua_State* tolua_S);

#include "..\CubeParticle.h"
#include "..\Particle.h"
#include "..\Point3f.h"
#include "..\SpringCoil.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"SpringCoil");
 tolua_usertype(tolua_S,"Particle");
 tolua_usertype(tolua_S,"Point3f");
}

/* get function: p1 of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_p1_ptr(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'p1'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)self->p1,"Particle");
 return 1;
}

/* set function: p1 of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_p1_ptr(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'p1'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->p1 = ((Particle*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* get function: p2 of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_p2_ptr(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'p2'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)self->p2,"Particle");
 return 1;
}

/* set function: p2 of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_p2_ptr(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'p2'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->p2 = ((Particle*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* get function: tightness of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_tightness(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'tightness'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->tightness);
 return 1;
}

/* set function: tightness of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_tightness(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'tightness'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->tightness = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: dampingFactor of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_dampingFactor(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'dampingFactor'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->dampingFactor);
 return 1;
}

/* set function: dampingFactor of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_dampingFactor(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'dampingFactor'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->dampingFactor = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: restLength of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_restLength(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'restLength'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->restLength);
 return 1;
}

/* set function: restLength of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_restLength(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'restLength'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->restLength = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: maxLength of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_maxLength(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'maxLength'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->maxLength);
 return 1;
}

/* set function: maxLength of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_maxLength(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'maxLength'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->maxLength = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: originalDirection of class  SpringCoil */
static int tolua_get_SpringCoil_SpringCoil_originalDirection(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'originalDirection'",NULL);
#endif
 tolua_pushusertype(tolua_S,(void*)&self->originalDirection,"Point3f");
 return 1;
}

/* set function: originalDirection of class  SpringCoil */
static int tolua_set_SpringCoil_SpringCoil_originalDirection(lua_State* tolua_S)
{
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'originalDirection'",NULL);
 if (!tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->originalDirection = *((Point3f*)  tolua_tousertype(tolua_S,2,0));
 return 0;
}

/* method: new of class  SpringCoil */
static int tolua_luaSpringCoil_SpringCoil_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"SpringCoil",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Particle",1,&tolua_err) ||
 !tolua_isboolean(tolua_S,4,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,8,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Particle* p1 = ((Particle*)  tolua_tousertype(tolua_S,2,NULL));
  Particle* p2 = ((Particle*)  tolua_tousertype(tolua_S,3,NULL));
  bool maintainDirection = ((bool)  tolua_toboolean(tolua_S,4,true));
  float tightness = ((float)  tolua_tonumber(tolua_S,5,0));
  float dampingFactor = ((float)  tolua_tonumber(tolua_S,6,0));
  float maxLength = ((float)  tolua_tonumber(tolua_S,7,0));
 {
  SpringCoil* tolua_ret = (SpringCoil*)  new SpringCoil(p1,p2,maintainDirection,tightness,dampingFactor,maxLength);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"SpringCoil");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  SpringCoil */
static int tolua_luaSpringCoil_SpringCoil_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SpringCoil",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->update(time);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: set of class  SpringCoil */
static int tolua_luaSpringCoil_SpringCoil_set00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SpringCoil",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Particle",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,6,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SpringCoil* self = (SpringCoil*)  tolua_tousertype(tolua_S,1,0);
  Particle* particle1 = ((Particle*)  tolua_tousertype(tolua_S,2,0));
  Particle* particle2 = ((Particle*)  tolua_tousertype(tolua_S,3,0));
  float tightness = ((float)  tolua_tonumber(tolua_S,4,0));
  float dampingFactor = ((float)  tolua_tonumber(tolua_S,5,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'set'",NULL);
#endif
 {
  self->set(particle1,particle2,tightness,dampingFactor);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'set'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaSpringCoil_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"SpringCoil","SpringCoil","",NULL);
 tolua_beginmodule(tolua_S,"SpringCoil");
 tolua_variable(tolua_S,"p1",tolua_get_SpringCoil_SpringCoil_p1_ptr,tolua_set_SpringCoil_SpringCoil_p1_ptr);
 tolua_variable(tolua_S,"p2",tolua_get_SpringCoil_SpringCoil_p2_ptr,tolua_set_SpringCoil_SpringCoil_p2_ptr);
 tolua_variable(tolua_S,"tightness",tolua_get_SpringCoil_SpringCoil_tightness,tolua_set_SpringCoil_SpringCoil_tightness);
 tolua_variable(tolua_S,"dampingFactor",tolua_get_SpringCoil_SpringCoil_dampingFactor,tolua_set_SpringCoil_SpringCoil_dampingFactor);
 tolua_variable(tolua_S,"restLength",tolua_get_SpringCoil_SpringCoil_restLength,tolua_set_SpringCoil_SpringCoil_restLength);
 tolua_variable(tolua_S,"maxLength",tolua_get_SpringCoil_SpringCoil_maxLength,tolua_set_SpringCoil_SpringCoil_maxLength);
 tolua_variable(tolua_S,"originalDirection",tolua_get_SpringCoil_SpringCoil_originalDirection,tolua_set_SpringCoil_SpringCoil_originalDirection);
 tolua_function(tolua_S,"new",tolua_luaSpringCoil_SpringCoil_new00);
 tolua_function(tolua_S,"update",tolua_luaSpringCoil_SpringCoil_update00);
 tolua_function(tolua_S,"set",tolua_luaSpringCoil_SpringCoil_set00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
