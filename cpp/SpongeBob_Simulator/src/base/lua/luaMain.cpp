/*
** Lua binding: luaMain
** Generated automatically by tolua 5.0a on 12/07/05 02:21:38.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaMain_open (lua_State* tolua_S);

#include "..\..\Main.h"
#include "..\Scene.h"
#include "..\SoftCube.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Main");
 tolua_usertype(tolua_S,"Scene");
 tolua_usertype(tolua_S,"SoftCube");
}

/* method: setScene of class  Main */
static int tolua_luaMain_Main_setScene00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Main",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Scene",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"SoftCube",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Scene* scene = ((Scene*)  tolua_tousertype(tolua_S,2,0));
  SoftCube* cube = ((SoftCube*)  tolua_tousertype(tolua_S,3,0));
 {
  Main::setScene(scene,cube);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setScene'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaMain_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Main","Main","",NULL);
 tolua_beginmodule(tolua_S,"Main");
 tolua_function(tolua_S,"setScene",tolua_luaMain_Main_setScene00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
