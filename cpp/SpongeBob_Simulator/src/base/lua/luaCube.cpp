/*
** Lua binding: luaCube
** Generated automatically by tolua 5.0a on 11/14/05 01:33:59.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaCube_open (lua_State* tolua_S);

#include "..\Shape.h"
#include "..\Point3f.h"
#include "..\Texture.h"
#include "..\Cube.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Cube (lua_State* tolua_S)
{
 Cube* self = (Cube*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Cube");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Texture");
}

/* method: new of class  Cube */
static int tolua_luaCube_Cube_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Cube",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,5,"Texture",1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,6,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float width = ((float)  tolua_tonumber(tolua_S,2,CUBE_DEFAULT_SIZE));
  float height = ((float)  tolua_tonumber(tolua_S,3,CUBE_DEFAULT_SIZE));
  float depth = ((float)  tolua_tonumber(tolua_S,4,CUBE_DEFAULT_SIZE));
  Texture* texture = ((Texture*)  tolua_tousertype(tolua_S,5,NULL));
 {
  Cube* tolua_ret = (Cube*)  new Cube(width,height,depth,texture);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Cube");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Cube */
static int tolua_luaCube_Cube_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cube",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cube* self = (Cube*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Cube */
static int tolua_luaCube_Cube_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cube",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cube* self = (Cube*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Cube */
static int tolua_luaCube_Cube_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cube",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cube* self = (Cube*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Shape* tolua_ret = (Shape*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Shape");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaCube_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"CUBE_DEFAULT_SIZE",CUBE_DEFAULT_SIZE);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Cube","Cube","Shape",tolua_collect_Cube);
#else
 tolua_cclass(tolua_S,"Cube","Cube","Shape",NULL);
#endif
 tolua_beginmodule(tolua_S,"Cube");
 tolua_function(tolua_S,"new",tolua_luaCube_Cube_new00);
 tolua_function(tolua_S,"delete",tolua_luaCube_Cube_delete00);
 tolua_function(tolua_S,"draw",tolua_luaCube_Cube_draw00);
 tolua_function(tolua_S,"getCopy",tolua_luaCube_Cube_getCopy00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
