/*
** Lua binding: luaLight
** Generated automatically by tolua 5.0a on 11/23/05 22:22:10.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaLight_open (lua_State* tolua_S);

#include "..\Color.h"
#include "..\Point3f.h"
#include "..\Light.h"
#include "..\Node.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Color (lua_State* tolua_S)
{
 Color* self = (Color*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}

static int tolua_collect_Light (lua_State* tolua_S)
{
 Light* self = (Light*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Color");
 tolua_usertype(tolua_S,"Point3f");
 tolua_usertype(tolua_S,"Node");
 tolua_usertype(tolua_S,"Light");
}

/* method: new of class  Light */
static int tolua_luaLight_Light_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Color",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,6,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* pos = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
  Color color = *((Color*)  tolua_tousertype(tolua_S,3,0));
  int lightId = ((int)  tolua_tonumber(tolua_S,4,0));
  int lightType = ((int)  tolua_tonumber(tolua_S,5,0));
 {
  Light* tolua_ret = (Light*)  new Light(pos,color,lightId,lightType);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Light");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Light */
static int tolua_luaLight_Light_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Light* self = (Light*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: setColor of class  Light */
static int tolua_luaLight_Light_setColor00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Color",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Light* self = (Light*)  tolua_tousertype(tolua_S,1,0);
  Color color = *((Color*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setColor'",NULL);
#endif
 {
  self->setColor(color);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setColor'.",&tolua_err);
 return 0;
#endif
}

/* method: setId of class  Light */
static int tolua_luaLight_Light_setId00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Light* self = (Light*)  tolua_tousertype(tolua_S,1,0);
  int lightId = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setId'",NULL);
#endif
 {
  self->setId(lightId);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setId'.",&tolua_err);
 return 0;
#endif
}

/* method: getColor of class  Light */
static int tolua_luaLight_Light_getColor00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Light* self = (Light*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getColor'",NULL);
#endif
 {
  Color tolua_ret =  self->getColor();
 {
#ifdef __cplusplus
 void* tolua_obj = new Color(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Color),"Color");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Color));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Color");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getColor'.",&tolua_err);
 return 0;
#endif
}

/* method: getId of class  Light */
static int tolua_luaLight_Light_getId00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Light* self = (Light*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getId'",NULL);
#endif
 {
  int tolua_ret = (int)  self->getId();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getId'.",&tolua_err);
 return 0;
#endif
}

/* method: enable of class  Light */
static int tolua_luaLight_Light_enable00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Light",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Light* self = (Light*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'enable'",NULL);
#endif
 {
  self->enable();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'enable'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaLight_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Light","Light","Node",tolua_collect_Light);
#else
 tolua_cclass(tolua_S,"Light","Light","Node",NULL);
#endif
 tolua_beginmodule(tolua_S,"Light");
 tolua_function(tolua_S,"new",tolua_luaLight_Light_new00);
 tolua_function(tolua_S,"delete",tolua_luaLight_Light_delete00);
 tolua_function(tolua_S,"setColor",tolua_luaLight_Light_setColor00);
 tolua_function(tolua_S,"setId",tolua_luaLight_Light_setId00);
 tolua_function(tolua_S,"getColor",tolua_luaLight_Light_getColor00);
 tolua_function(tolua_S,"getId",tolua_luaLight_Light_getId00);
 tolua_function(tolua_S,"enable",tolua_luaLight_Light_enable00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
