/*
** Lua binding: luaEnvironment
** Generated automatically by tolua 5.0a on 11/27/05 12:25:08.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaEnvironment_open (lua_State* tolua_S);

#include "..\DynamicVector.h"
#include "..\Force.h"
#include "..\Particle.h"
#include "..\Environment.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Force");
 tolua_usertype(tolua_S,"Particle");
 tolua_usertype(tolua_S,"Environment");
}

/* method: new of class  Environment */
static int tolua_luaEnvironment_Environment_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Environment* tolua_ret = (Environment*)  new Environment();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Environment");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: insertParticle of class  Environment */
static int tolua_luaEnvironment_Environment_insertParticle00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Particle",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Environment* self = (Environment*)  tolua_tousertype(tolua_S,1,0);
  Particle* particle = ((Particle*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertParticle'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertParticle(particle);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertParticle'.",&tolua_err);
 return 0;
#endif
}

/* method: removeParticleAt of class  Environment */
static int tolua_luaEnvironment_Environment_removeParticleAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Environment* self = (Environment*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeParticleAt'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeParticleAt(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeParticleAt'.",&tolua_err);
 return 0;
#endif
}

/* method: insertForce of class  Environment */
static int tolua_luaEnvironment_Environment_insertForce00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Force",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Environment* self = (Environment*)  tolua_tousertype(tolua_S,1,0);
  Force* force = ((Force*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertForce'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertForce(force);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertForce'.",&tolua_err);
 return 0;
#endif
}

/* method: removeForceAt of class  Environment */
static int tolua_luaEnvironment_Environment_removeForceAt00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Environment* self = (Environment*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'removeForceAt'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->removeForceAt(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'removeForceAt'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  Environment */
static int tolua_luaEnvironment_Environment_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Environment* self = (Environment*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  self->update(time);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: render of class  Environment */
static int tolua_luaEnvironment_Environment_render00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Environment",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Environment* self = (Environment*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'render'",NULL);
#endif
 {
  self->render();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'render'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaEnvironment_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"Environment","Environment","",NULL);
 tolua_beginmodule(tolua_S,"Environment");
 tolua_function(tolua_S,"new",tolua_luaEnvironment_Environment_new00);
 tolua_function(tolua_S,"insertParticle",tolua_luaEnvironment_Environment_insertParticle00);
 tolua_function(tolua_S,"removeParticleAt",tolua_luaEnvironment_Environment_removeParticleAt00);
 tolua_function(tolua_S,"insertForce",tolua_luaEnvironment_Environment_insertForce00);
 tolua_function(tolua_S,"removeForceAt",tolua_luaEnvironment_Environment_removeForceAt00);
 tolua_function(tolua_S,"update",tolua_luaEnvironment_Environment_update00);
 tolua_function(tolua_S,"render",tolua_luaEnvironment_Environment_render00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
