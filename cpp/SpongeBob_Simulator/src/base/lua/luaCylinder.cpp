/*
** Lua binding: luaCylinder
** Generated automatically by tolua 5.0a on 11/26/05 01:55:35.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaCylinder_open (lua_State* tolua_S);

#define _USE_MATH_DEFINES
#include <math.h>
#include "..\Shape.h"
#include "..\Point3f.h"
#include "..\Texture.h"
#include "..\Cylinder.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Cylinder (lua_State* tolua_S)
{
 Cylinder* self = (Cylinder*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Cylinder");
 tolua_usertype(tolua_S,"Color");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Texture");
}

/* method: new of class  Cylinder */
static int tolua_luaCylinder_Cylinder_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Color",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,7,1,&tolua_err) ||
 !tolua_isusertype(tolua_S,8,"Texture",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,9,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,10,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Color* color = ((Color*)  tolua_tousertype(tolua_S,2,NULL));
  float size = ((float)  tolua_tonumber(tolua_S,3,CYLINDER_DEFAULT_SIZE));
  float height = ((float)  tolua_tonumber(tolua_S,4,CYLINDER_DEFAULT_HEIGHT));
  float baseRadius = ((float)  tolua_tonumber(tolua_S,5,CYLINDER_DEFAULT_RADIUS));
  float topRadius = ((float)  tolua_tonumber(tolua_S,6,CYLINDER_DEFAULT_RADIUS));
  int vertices = ((int)  tolua_tonumber(tolua_S,7,CYLINDER_RESOLUTION));
  Texture* texture = ((Texture*)  tolua_tousertype(tolua_S,8,NULL));
  unsigned char lidType = ((unsigned char)  tolua_tonumber(tolua_S,9,LID_ALL));
 {
  Cylinder* tolua_ret = (Cylinder*)  new Cylinder(color,size,height,baseRadius,topRadius,vertices,texture,lidType);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Cylinder");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Cylinder */
static int tolua_luaCylinder_Cylinder_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Cylinder */
static int tolua_luaCylinder_Cylinder_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: build of class  Cylinder */
static int tolua_luaCylinder_Cylinder_build00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
  float initialAngle = ((float)  tolua_tonumber(tolua_S,2,CYLINDER_DEFAULT_INITIAL_ANGLE));
  float endAngle = ((float)  tolua_tonumber(tolua_S,3,CYLINDER_DEFAULT_END_ANGLE));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'build'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->build(initialAngle,endAngle);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'build'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Cylinder */
static int tolua_luaCylinder_Cylinder_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Shape* tolua_ret = (Shape*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Shape");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* method: setDisplayListIndex of class  Cylinder */
static int tolua_luaCylinder_Cylinder_setDisplayListIndex00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
  int displayListIndex = ((int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setDisplayListIndex'",NULL);
#endif
 {
  self->setDisplayListIndex(displayListIndex);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setDisplayListIndex'.",&tolua_err);
 return 0;
#endif
}

/* method: setLidType of class  Cylinder */
static int tolua_luaCylinder_Cylinder_setLidType00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
  unsigned char lidType = ((unsigned char)  tolua_tonumber(tolua_S,2,LID_ALL));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setLidType'",NULL);
#endif
 {
  self->setLidType(lidType);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setLidType'.",&tolua_err);
 return 0;
#endif
}

/* method: getLidType of class  Cylinder */
static int tolua_luaCylinder_Cylinder_getLidType00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Cylinder",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Cylinder* self = (Cylinder*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getLidType'",NULL);
#endif
 {
  unsigned char tolua_ret = (unsigned char)  self->getLidType();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getLidType'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaCylinder_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"CYLINDER_RESOLUTION",CYLINDER_RESOLUTION);
 tolua_constant(tolua_S,"CYLINDER_DEFAULT_RADIUS",CYLINDER_DEFAULT_RADIUS);
 tolua_constant(tolua_S,"CYLINDER_DEFAULT_HEIGHT",CYLINDER_DEFAULT_HEIGHT);
 tolua_constant(tolua_S,"CYLINDER_DEFAULT_SIZE",CYLINDER_DEFAULT_SIZE);
 tolua_constant(tolua_S,"CYLINDER_DEFAULT_INITIAL_ANGLE",CYLINDER_DEFAULT_INITIAL_ANGLE);
 tolua_constant(tolua_S,"CYLINDER_DEFAULT_END_ANGLE",CYLINDER_DEFAULT_END_ANGLE);
 tolua_constant(tolua_S,"TWOPI",TWOPI);
 tolua_constant(tolua_S,"PID2",PID2);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Cylinder","Cylinder","Shape",tolua_collect_Cylinder);
#else
 tolua_cclass(tolua_S,"Cylinder","Cylinder","Shape",NULL);
#endif
 tolua_beginmodule(tolua_S,"Cylinder");
 tolua_function(tolua_S,"new",tolua_luaCylinder_Cylinder_new00);
 tolua_function(tolua_S,"delete",tolua_luaCylinder_Cylinder_delete00);
 tolua_function(tolua_S,"draw",tolua_luaCylinder_Cylinder_draw00);
 tolua_function(tolua_S,"build",tolua_luaCylinder_Cylinder_build00);
 tolua_function(tolua_S,"getCopy",tolua_luaCylinder_Cylinder_getCopy00);
 tolua_function(tolua_S,"setDisplayListIndex",tolua_luaCylinder_Cylinder_setDisplayListIndex00);
 tolua_function(tolua_S,"setLidType",tolua_luaCylinder_Cylinder_setLidType00);
 tolua_function(tolua_S,"getLidType",tolua_luaCylinder_Cylinder_getLidType00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
