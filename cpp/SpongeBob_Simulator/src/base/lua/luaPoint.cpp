/*
** Lua binding: luaPoint
** Generated automatically by tolua 5.0a on 11/14/05 02:06:23.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaPoint_open (lua_State* tolua_S);

#include "..\Shape.h"
#include "..\Point.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Color");
 tolua_usertype(tolua_S,"Shape");
 tolua_usertype(tolua_S,"Point");
}

/* method: new of class  Point */
static int tolua_luaPoint_Point_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Point",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Color",1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Color* color = ((Color*)  tolua_tousertype(tolua_S,2,NULL));
  float size = ((float)  tolua_tonumber(tolua_S,3,DEFAULT_SIZE));
 {
  Point* tolua_ret = (Point*)  new Point(color,size);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Point");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  Point */
static int tolua_luaPoint_Point_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point* self = (Point*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Point */
static int tolua_luaPoint_Point_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Point",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point* self = (Point*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Shape* tolua_ret = (Shape*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Shape");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaPoint_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"DEFAULT_SIZE",DEFAULT_SIZE);
 tolua_cclass(tolua_S,"Point","Point","Shape",NULL);
 tolua_beginmodule(tolua_S,"Point");
 tolua_function(tolua_S,"new",tolua_luaPoint_Point_new00);
 tolua_function(tolua_S,"draw",tolua_luaPoint_Point_draw00);
 tolua_function(tolua_S,"getCopy",tolua_luaPoint_Point_getCopy00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
