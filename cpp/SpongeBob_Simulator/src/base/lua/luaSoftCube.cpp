/*
** Lua binding: luaSoftCube
** Generated automatically by tolua 5.0a on 12/07/05 00:29:57.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaSoftCube_open (lua_State* tolua_S);

#include "..\Node.h"
#include "..\SpringCoil.h"
#include "..\DynamicVector.h"
#include "..\CubeParticle.h"
#include "..\Cube.h"
#include "..\SoftCube.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_SoftCube (lua_State* tolua_S)
{
 SoftCube* self = (SoftCube*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"DynamicVector");
 tolua_usertype(tolua_S,"SoftCube");
 tolua_usertype(tolua_S,"Plane");
 tolua_usertype(tolua_S,"Particle");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",1,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Point3f* myPosition = ((Point3f*)  tolua_tousertype(tolua_S,2,NULL));
  Point3f* mySpeed = ((Point3f*)  tolua_tousertype(tolua_S,3,NULL));
 {
  SoftCube* tolua_ret = (SoftCube*)  new SoftCube(myPosition,mySpeed);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"SoftCube");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SoftCube* self = (SoftCube*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: build of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_build00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,6,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,7,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SoftCube* self = (SoftCube*)  tolua_tousertype(tolua_S,1,0);
  float dimension = ((float)  tolua_tonumber(tolua_S,2,0));
  unsigned int nPointsByEdge = ((unsigned int)  tolua_tonumber(tolua_S,3,0));
  float tightness = ((float)  tolua_tonumber(tolua_S,4,0));
  float damping = ((float)  tolua_tonumber(tolua_S,5,0));
  float stickingFactor = ((float)  tolua_tonumber(tolua_S,6,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'build'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->build(dimension,nPointsByEdge,tightness,damping,stickingFactor);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'build'.",&tolua_err);
 return 0;
#endif
}

/* method: draw of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_draw00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SoftCube* self = (SoftCube*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'draw'",NULL);
#endif
 {
  self->draw();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'draw'.",&tolua_err);
 return 0;
#endif
}

/* method: update of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_update00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SoftCube* self = (SoftCube*)  tolua_tousertype(tolua_S,1,0);
  double time = ((double)  tolua_tonumber(tolua_S,2,0));
  DynamicVector* externForces = ((DynamicVector*)  tolua_tousertype(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'update'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->update(time,externForces);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'update'.",&tolua_err);
 return 0;
#endif
}

/* method: insertLimitPlane of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_insertLimitPlane00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Plane",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SoftCube* self = (SoftCube*)  tolua_tousertype(tolua_S,1,0);
  Plane* limitPlane = ((Plane*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insertLimitPlane'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->insertLimitPlane(limitPlane);
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insertLimitPlane'.",&tolua_err);
 return 0;
#endif
}

/* method: setSpeed of class  SoftCube */
static int tolua_luaSoftCube_SoftCube_setSpeed00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"SoftCube",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  SoftCube* self = (SoftCube*)  tolua_tousertype(tolua_S,1,0);
  Point3f* speed = ((Point3f*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setSpeed'",NULL);
#endif
 {
  self->setSpeed(speed);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setSpeed'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaSoftCube_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"SoftCube","SoftCube","Particle",tolua_collect_SoftCube);
#else
 tolua_cclass(tolua_S,"SoftCube","SoftCube","Particle",NULL);
#endif
 tolua_beginmodule(tolua_S,"SoftCube");
 tolua_function(tolua_S,"new",tolua_luaSoftCube_SoftCube_new00);
 tolua_function(tolua_S,"delete",tolua_luaSoftCube_SoftCube_delete00);
 tolua_function(tolua_S,"build",tolua_luaSoftCube_SoftCube_build00);
 tolua_function(tolua_S,"draw",tolua_luaSoftCube_SoftCube_draw00);
 tolua_function(tolua_S,"update",tolua_luaSoftCube_SoftCube_update00);
 tolua_function(tolua_S,"insertLimitPlane",tolua_luaSoftCube_SoftCube_insertLimitPlane00);
 tolua_function(tolua_S,"setSpeed",tolua_luaSoftCube_SoftCube_setSpeed00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
