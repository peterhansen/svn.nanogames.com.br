/*
** Lua binding: luaConicEmitter
** Generated automatically by tolua 5.0a on 11/09/05 00:06:34.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaConicEmitter_open (lua_State* tolua_S);

#include "..\Emitter.h"
#include "..\ConicEmitter.h"

/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"ConicEmitter");
 tolua_usertype(tolua_S,"Emitter");
}

/* Open function */
TOLUA_API int tolua_luaConicEmitter_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_cclass(tolua_S,"ConicEmitter","ConicEmitter","Emitter",NULL);
 tolua_beginmodule(tolua_S,"ConicEmitter");
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
