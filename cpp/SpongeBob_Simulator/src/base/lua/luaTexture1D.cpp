/*
** Lua binding: luaTexture1D
** Generated automatically by tolua 5.0a on 11/14/05 01:39:03.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaTexture1D_open (lua_State* tolua_S);

#include "..\Texture.h"
#include "..\Texture1D.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Texture1D (lua_State* tolua_S)
{
 Texture1D* self = (Texture1D*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Texture1D");
 tolua_usertype(tolua_S,"UINT");
 tolua_usertype(tolua_S,"size_t");
 tolua_usertype(tolua_S,"Texture");
}

/* method: new of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
 {
  Texture1D* tolua_ret = (Texture1D*)  new Texture1D();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Texture1D");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: setTexture of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_setTexture00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isuserdata(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"UINT",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,4,"size_t",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
  void* pDataArray = ((void*)  tolua_touserdata(tolua_S,2,0));
  UINT arraySize = *((UINT*)  tolua_tousertype(tolua_S,3,0));
  size_t dataSize = *((size_t*)  tolua_tousertype(tolua_S,4,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'setTexture'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->setTexture(pDataArray,arraySize,dataSize);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'setTexture'.",&tolua_err);
 return 0;
#endif
}

/* method: getSize of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_getSize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getSize'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->getSize();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getSize'.",&tolua_err);
 return 0;
#endif
}

/* method: getTextureData of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_getTextureData00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getTextureData'",NULL);
#endif
 {
  void* tolua_ret = (void*)  self->getTextureData();
 tolua_pushuserdata(tolua_S,(void*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getTextureData'.",&tolua_err);
 return 0;
#endif
}

/* method: load of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_load00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'load'",NULL);
#endif
 {
  self->load();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'load'.",&tolua_err);
 return 0;
#endif
}

/* method: unload of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_unload00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'unload'",NULL);
#endif
 {
  self->unload();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'unload'.",&tolua_err);
 return 0;
#endif
}

/* method: getCopy of class  Texture1D */
static int tolua_luaTexture1D_Texture1D_getCopy00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Texture1D",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Texture1D* self = (Texture1D*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getCopy'",NULL);
#endif
 {
  Texture* tolua_ret = (Texture*)  self->getCopy();
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Texture");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getCopy'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaTexture1D_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Texture1D","Texture1D","Texture",tolua_collect_Texture1D);
#else
 tolua_cclass(tolua_S,"Texture1D","Texture1D","Texture",NULL);
#endif
 tolua_beginmodule(tolua_S,"Texture1D");
 tolua_function(tolua_S,"new",tolua_luaTexture1D_Texture1D_new00);
 tolua_function(tolua_S,"delete",tolua_luaTexture1D_Texture1D_delete00);
 tolua_function(tolua_S,"setTexture",tolua_luaTexture1D_Texture1D_setTexture00);
 tolua_function(tolua_S,"getSize",tolua_luaTexture1D_Texture1D_getSize00);
 tolua_function(tolua_S,"getTextureData",tolua_luaTexture1D_Texture1D_getTextureData00);
 tolua_function(tolua_S,"load",tolua_luaTexture1D_Texture1D_load00);
 tolua_function(tolua_S,"unload",tolua_luaTexture1D_Texture1D_unload00);
 tolua_function(tolua_S,"getCopy",tolua_luaTexture1D_Texture1D_getCopy00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
