/*
** Lua binding: luaDynamicVector
** Generated automatically by tolua 5.0a on 11/09/05 00:07:43.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaDynamicVector_open (lua_State* tolua_S);

#include "..\DynamicVector.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_DynamicVector (lua_State* tolua_S)
{
 DynamicVector* self = (DynamicVector*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"DynamicVector");
}

/* method: new of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  unsigned int initialSlots = ((unsigned int)  tolua_tonumber(tolua_S,2,1));
  float growTax = ((float)  tolua_tonumber(tolua_S,3,DEFAULT_GROW_TAX));
 {
  DynamicVector* tolua_ret = (DynamicVector*)  new DynamicVector(initialSlots,growTax);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"DynamicVector");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: insert of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_insert00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isuserdata(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
  void* pData = ((void*)  tolua_touserdata(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'insert'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->insert(pData);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'insert'.",&tolua_err);
 return 0;
#endif
}

/* method: remove of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_remove00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'remove'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->remove(index);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'remove'.",&tolua_err);
 return 0;
#endif
}

/* method: switchSlots of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_switchSlots00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
  unsigned int slot1 = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
  unsigned int slot2 = ((unsigned int)  tolua_tonumber(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'switchSlots'",NULL);
#endif
 {
  bool tolua_ret = (bool)  self->switchSlots(slot1,slot2);
 tolua_pushboolean(tolua_S,(bool)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'switchSlots'.",&tolua_err);
 return 0;
#endif
}

/* method: getData of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_getData00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
  unsigned int index = ((unsigned int)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getData'",NULL);
#endif
 {
  void* tolua_ret = (void*)  self->getData(index);
 tolua_pushuserdata(tolua_S,(void*)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getData'.",&tolua_err);
 return 0;
#endif
}

/* method: getLength of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_getLength00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getLength'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->getLength();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getLength'.",&tolua_err);
 return 0;
#endif
}

/* method: getUsedLength of class  DynamicVector */
static int tolua_luaDynamicVector_DynamicVector_getUsedLength00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"DynamicVector",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  DynamicVector* self = (DynamicVector*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'getUsedLength'",NULL);
#endif
 {
  unsigned int tolua_ret = (unsigned int)  self->getUsedLength();
 tolua_pushnumber(tolua_S,(lua_Number)tolua_ret);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'getUsedLength'.",&tolua_err);
 return 0;
#endif
}

/* Open function */
TOLUA_API int tolua_luaDynamicVector_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
 tolua_constant(tolua_S,"DEFAULT_GROW_TAX",DEFAULT_GROW_TAX);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"DynamicVector","DynamicVector","",tolua_collect_DynamicVector);
#else
 tolua_cclass(tolua_S,"DynamicVector","DynamicVector","",NULL);
#endif
 tolua_beginmodule(tolua_S,"DynamicVector");
 tolua_function(tolua_S,"new",tolua_luaDynamicVector_DynamicVector_new00);
 tolua_function(tolua_S,"delete",tolua_luaDynamicVector_DynamicVector_delete00);
 tolua_function(tolua_S,"insert",tolua_luaDynamicVector_DynamicVector_insert00);
 tolua_function(tolua_S,"remove",tolua_luaDynamicVector_DynamicVector_remove00);
 tolua_function(tolua_S,"switchSlots",tolua_luaDynamicVector_DynamicVector_switchSlots00);
 tolua_function(tolua_S,"getData",tolua_luaDynamicVector_DynamicVector_getData00);
 tolua_function(tolua_S,"getLength",tolua_luaDynamicVector_DynamicVector_getLength00);
 tolua_function(tolua_S,"getUsedLength",tolua_luaDynamicVector_DynamicVector_getUsedLength00);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
