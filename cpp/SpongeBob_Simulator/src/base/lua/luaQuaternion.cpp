/*
** Lua binding: luaQuaternion
** Generated automatically by tolua 5.0a on 11/09/05 00:41:52.
*/

#ifndef __cplusplus
#include "stdlib.h"
#endif
#include "string.h"

#include "tolua.h"

/* Exported function */
TOLUA_API int tolua_luaQuaternion_open (lua_State* tolua_S);

#include "..\Point3f.h"
#include "..\Quaternion.h"

/* function to release collected object via destructor */
#ifdef __cplusplus

static int tolua_collect_Quaternion (lua_State* tolua_S)
{
 Quaternion* self = (Quaternion*) tolua_tousertype(tolua_S,1,0);
 delete self;
 return 0;
}
#endif


/* function to register type */
static void tolua_reg_types (lua_State* tolua_S)
{
 tolua_usertype(tolua_S,"Quaternion");
 tolua_usertype(tolua_S,"Point3f");
}

/* method: new of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_new00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertable(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,1,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,1,&tolua_err) ||
 !tolua_isnoobj(tolua_S,6,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  float w = ((float)  tolua_tonumber(tolua_S,2,0.0));
  float x = ((float)  tolua_tonumber(tolua_S,3,0.0));
  float y = ((float)  tolua_tonumber(tolua_S,4,0.0));
  float z = ((float)  tolua_tonumber(tolua_S,5,0.999999));
 {
  Quaternion* tolua_ret = (Quaternion*)  new Quaternion(w,x,y,z);
 tolua_pushusertype(tolua_S,(void*)tolua_ret,"Quaternion");
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'new'.",&tolua_err);
 return 0;
#endif
}

/* method: delete of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_delete00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'delete'",NULL);
#endif
 delete self;
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'delete'.",&tolua_err);
 return 0;
#endif
}

/* method: set_Axis of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_set_Axis00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  float x = ((float)  tolua_tonumber(tolua_S,2,0));
  float y = ((float)  tolua_tonumber(tolua_S,3,0));
  float z = ((float)  tolua_tonumber(tolua_S,4,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'set_Axis'",NULL);
#endif
 {
  self->set_Axis(x,y,z);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'set_Axis'.",&tolua_err);
 return 0;
#endif
}

/* method: set_Quaternion of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_set_Quaternion00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,5,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,6,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  float w = ((float)  tolua_tonumber(tolua_S,2,0));
  float x = ((float)  tolua_tonumber(tolua_S,3,0));
  float y = ((float)  tolua_tonumber(tolua_S,4,0));
  float z = ((float)  tolua_tonumber(tolua_S,5,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'set_Quaternion'",NULL);
#endif
 {
  self->set_Quaternion(w,x,y,z);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'set_Quaternion'.",&tolua_err);
 return 0;
#endif
}

/* method: operator* of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_operator_mul00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Quaternion",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  Quaternion* tolua_var_1 = ((Quaternion*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator*'",NULL);
#endif
 {
  Quaternion tolua_ret =  self->operator*(*tolua_var_1);
 {
#ifdef __cplusplus
 void* tolua_obj = new Quaternion(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Quaternion),"Quaternion");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Quaternion));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Quaternion");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function '.mul'.",&tolua_err);
 return 0;
#endif
}

/* method: operator* of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_operator_mul01(lua_State* tolua_S)
{
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  float tolua_var_2 = ((float)  tolua_tonumber(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator*'",NULL);
#endif
 {
  Quaternion tolua_ret =  self->operator*(tolua_var_2);
 {
#ifdef __cplusplus
 void* tolua_obj = new Quaternion(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Quaternion),"Quaternion");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Quaternion));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Quaternion");
#endif
 }
 }
 }
 return 1;
tolua_lerror:
 return tolua_luaQuaternion_Quaternion_operator_mul00(tolua_S);
}

/* method: operator+ of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_operator_add00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Quaternion",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,3,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  Quaternion* tolua_var_3 = ((Quaternion*)  tolua_tousertype(tolua_S,2,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'operator+'",NULL);
#endif
 {
  Quaternion tolua_ret =  self->operator+(*tolua_var_3);
 {
#ifdef __cplusplus
 void* tolua_obj = new Quaternion(tolua_ret);
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,tolua_collect_Quaternion),"Quaternion");
#else
 void* tolua_obj = tolua_copy(tolua_S,(void*)&tolua_ret,sizeof(Quaternion));
 tolua_pushusertype(tolua_S,tolua_clone(tolua_S,tolua_obj,NULL),"Quaternion");
#endif
 }
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function '.add'.",&tolua_err);
 return 0;
#endif
}

/* method: interpolate_slerp of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_interpolate_slerp00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,2,"Quaternion",0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  Quaternion* quat1 = ((Quaternion*)  tolua_tousertype(tolua_S,2,0));
  Quaternion* quat2 = ((Quaternion*)  tolua_tousertype(tolua_S,3,0));
  float slerp = ((float)  tolua_tonumber(tolua_S,4,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'interpolate_slerp'",NULL);
#endif
 {
  self->interpolate_slerp(quat1,quat2,slerp);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'interpolate_slerp'.",&tolua_err);
 return 0;
#endif
}

/* method: EulerToQuaternion of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_EulerToQuaternion00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,3,0,&tolua_err) ||
 !tolua_isnumber(tolua_S,4,0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,5,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  float rot_x = ((float)  tolua_tonumber(tolua_S,2,0));
  float rot_y = ((float)  tolua_tonumber(tolua_S,3,0));
  float rot_z = ((float)  tolua_tonumber(tolua_S,4,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'EulerToQuaternion'",NULL);
#endif
 {
  self->EulerToQuaternion(rot_x,rot_y,rot_z);
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'EulerToQuaternion'.",&tolua_err);
 return 0;
#endif
}

/* method: normalize of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_normalize00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,2,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'normalize'",NULL);
#endif
 {
  self->normalize();
 }
 }
 return 0;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'normalize'.",&tolua_err);
 return 0;
#endif
}

/* method: toEuler of class  Quaternion */
static int tolua_luaQuaternion_Quaternion_toEuler00(lua_State* tolua_S)
{
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (
 !tolua_isusertype(tolua_S,1,"Quaternion",0,&tolua_err) ||
 !tolua_isnumber(tolua_S,2,0,&tolua_err) ||
 !tolua_isusertype(tolua_S,3,"Point3f",0,&tolua_err) ||
 !tolua_isnoobj(tolua_S,4,&tolua_err)
 )
 goto tolua_lerror;
 else
#endif
 {
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
  float angle = ((float)  tolua_tonumber(tolua_S,2,0));
  Point3f* axis = ((Point3f*)  tolua_tousertype(tolua_S,3,0));
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in function 'toEuler'",NULL);
#endif
 {
  self->toEuler(&angle,axis);
 tolua_pushnumber(tolua_S,(lua_Number)angle);
 }
 }
 return 1;
#ifndef TOLUA_RELEASE
 tolua_lerror:
 tolua_error(tolua_S,"#ferror in function 'toEuler'.",&tolua_err);
 return 0;
#endif
}

/* get function: x of class  Quaternion */
static int tolua_get_Quaternion_Quaternion_x(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'x'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->x);
 return 1;
}

/* set function: x of class  Quaternion */
static int tolua_set_Quaternion_Quaternion_x(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'x'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->x = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: y of class  Quaternion */
static int tolua_get_Quaternion_Quaternion_y(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'y'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->y);
 return 1;
}

/* set function: y of class  Quaternion */
static int tolua_set_Quaternion_Quaternion_y(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'y'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->y = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: z of class  Quaternion */
static int tolua_get_Quaternion_Quaternion_z(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'z'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->z);
 return 1;
}

/* set function: z of class  Quaternion */
static int tolua_set_Quaternion_Quaternion_z(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'z'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->z = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* get function: angle of class  Quaternion */
static int tolua_get_Quaternion_Quaternion_angle(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'angle'",NULL);
#endif
 tolua_pushnumber(tolua_S,(lua_Number)self->angle);
 return 1;
}

/* set function: angle of class  Quaternion */
static int tolua_set_Quaternion_Quaternion_angle(lua_State* tolua_S)
{
  Quaternion* self = (Quaternion*)  tolua_tousertype(tolua_S,1,0);
#ifndef TOLUA_RELEASE
 tolua_Error tolua_err;
 if (!self) tolua_error(tolua_S,"invalid 'self' in accessing variable 'angle'",NULL);
 if (!tolua_isnumber(tolua_S,2,0,&tolua_err))
 tolua_error(tolua_S,"#vinvalid type in variable assignment.",&tolua_err);
#endif
  self->angle = ((float)  tolua_tonumber(tolua_S,2,0));
 return 0;
}

/* Open function */
TOLUA_API int tolua_luaQuaternion_open (lua_State* tolua_S)
{
 tolua_open(tolua_S);
 tolua_reg_types(tolua_S);
 tolua_module(tolua_S,NULL,0);
 tolua_beginmodule(tolua_S,NULL);
#ifdef __cplusplus
 tolua_cclass(tolua_S,"Quaternion","Quaternion","",tolua_collect_Quaternion);
#else
 tolua_cclass(tolua_S,"Quaternion","Quaternion","",NULL);
#endif
 tolua_beginmodule(tolua_S,"Quaternion");
 tolua_function(tolua_S,"new",tolua_luaQuaternion_Quaternion_new00);
 tolua_function(tolua_S,"delete",tolua_luaQuaternion_Quaternion_delete00);
 tolua_function(tolua_S,"set_Axis",tolua_luaQuaternion_Quaternion_set_Axis00);
 tolua_function(tolua_S,"set_Quaternion",tolua_luaQuaternion_Quaternion_set_Quaternion00);
 tolua_function(tolua_S,".mul",tolua_luaQuaternion_Quaternion_operator_mul00);
 tolua_function(tolua_S,".mul",tolua_luaQuaternion_Quaternion_operator_mul01);
 tolua_function(tolua_S,".add",tolua_luaQuaternion_Quaternion_operator_add00);
 tolua_function(tolua_S,"interpolate_slerp",tolua_luaQuaternion_Quaternion_interpolate_slerp00);
 tolua_function(tolua_S,"EulerToQuaternion",tolua_luaQuaternion_Quaternion_EulerToQuaternion00);
 tolua_function(tolua_S,"normalize",tolua_luaQuaternion_Quaternion_normalize00);
 tolua_function(tolua_S,"toEuler",tolua_luaQuaternion_Quaternion_toEuler00);
 tolua_variable(tolua_S,"x",tolua_get_Quaternion_Quaternion_x,tolua_set_Quaternion_Quaternion_x);
 tolua_variable(tolua_S,"y",tolua_get_Quaternion_Quaternion_y,tolua_set_Quaternion_Quaternion_y);
 tolua_variable(tolua_S,"z",tolua_get_Quaternion_Quaternion_z,tolua_set_Quaternion_Quaternion_z);
 tolua_variable(tolua_S,"angle",tolua_get_Quaternion_Quaternion_angle,tolua_set_Quaternion_Quaternion_angle);
 tolua_endmodule(tolua_S);
 tolua_endmodule(tolua_S);
 return 1;
}
