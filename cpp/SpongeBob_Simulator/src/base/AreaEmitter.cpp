#include "AreaEmitter.h"

AreaEmitter::AreaEmitter( float myEmissionRate, Shape *pMyShape, Point3f *myPosition, float myLife, float mySize, float myMass, Point3f *mySpeed, float myAngle, Point3f *myAxis, bool myActive ) 
			:Emitter( myEmissionRate, pMyShape, myPosition, myLife, mySize, myMass, mySpeed, myAngle, myAxis, myActive ),
			width( 0.0 ), height( 0.0 )
{
}


inline void AreaEmitter::setPlane( Plane *myPlane, float width, float height ) {
	plane = *myPlane;

	setWidth( width );
	setHeight( height );

}

inline Plane AreaEmitter::getPlane( void ) {
	return plane;
}

bool AreaEmitter::emit( UINT16 quantity ) {
	if ( baseElement ) {
		for( UINT16 i = 0 ; i < quantity ; i++ ) {
			if ( currentActiveElements + 1 >= elements->getUsedLength() ) {
				// n�mero de part�culas ativas � maior do que o tamanho m�ximo atual do vetor -> aumenta tamanho m�ximo do vetor
				Particle *pCopy = baseElement->getCopy( );
				if ( !pCopy || !elements->insert( pCopy ) )	{
					SAFE_DELETE( pCopy );
					return false;
				}	
			}
			Particle *pParticle = ( Particle* )elements->getData( currentActiveElements );
			if( pParticle ) {
				currentActiveElements++;
				pParticle->setBasedOn( baseElement );
				const float halfWidth	= width / 2.0,
							halfHeight	= height / 2.0;
				pParticle->setPosition( &( position + 
							plane.top * Random::nextDouble( -halfWidth, halfWidth ) +
							plane.right * Random::nextDouble( -halfHeight, halfHeight ) ) );
				pParticle->getSpeed() += speed;
			}
		}

		elementsToEmit -= quantity;
		if ( elementsToEmit < 0.0 )
			elementsToEmit = 0.0;

		return true;
	} // fim if ( baseElement )
	return false;
}


inline void AreaEmitter::setWidth( float myWidth ) {
	width = myWidth;
}

inline float AreaEmitter::getWidth( void ) {
	return width;
}

inline void AreaEmitter::setHeight( float myHeight ) {
	height = myHeight;
}

inline float AreaEmitter::getHeight( void ) {
	return height;
}