#include "SpringCoil.h"

SpringCoil::SpringCoil( Particle *particle1, Particle *particle2, bool maintainDirection, float tightness, float dampingFactor, float myMaxLength, float myLife ) :
					    p1( particle1 ), p2( particle2 ), tightness( tightness ), dampingFactor( dampingFactor ),
							maxLength( myMaxLength ), maintainDirection( maintainDirection )
{
	life = myLife;
	if ( life >= 0 )
		immortal = false;
	else
		immortal = true;
	set( particle1, particle2, tightness, dampingFactor );
}

bool SpringCoil::update( double time )
{
	// F = -k(|x|-d)(x/|x|) - bv
	// k = constante da mola (tightness)
	// x = vetor dist�ncia atual entre as part�culas
	// d = comprimento de repouso da mola (restLength)
	// v = vetor da velocidade relativa entre as part�culas ligadas pela mola
	// b = fator de amortecimento (dampingFactor)

	if ( immortal || ( ( life -= time ) > 0 ) ) {

		Point3f distance = p1->getPosition() - p2->getPosition();
		Point3f relativeSpeed = p1->getSpeed() - p2->getSpeed();

		float distanceModule = distance.getModule();
		Point3f distanceUnit = distance / distanceModule;

		//garante o comprimento m�nimo da mola
		float minLength = restLength * 0.2;
		if ( distanceModule < minLength ) {
			Point3f offset = distanceUnit * ( ( minLength - distanceModule)/2.0 );
			p1->setPosition( &( p1->getPosition() + offset ) );
			p2->setPosition( &( p2->getPosition() - offset ) );

			distance = p1->getPosition() - p2->getPosition();

			//conserta o erro de precis�o do float
			distanceModule = minLength;

			distanceUnit = distance / distanceModule;
		}

		float d = distanceModule - restLength;

		if ( abs( distanceModule ) >= 1 ) {

			// para manter o formato do cubo, a f�rmula original de molas foi alterada para poder levar em
			// conta a dire��o original das molas, assim mantendo o formato do cubo
			Point3f force = ( ( distanceUnit * ( -tightness * d ) ) - ( relativeSpeed * dampingFactor ) );

			if ( maintainDirection )
				force = ( force + ( originalDirection * ( -tightness * d ) - ( relativeSpeed * dampingFactor ) ) ) / 2.0;

			p1->force += force;
			p2->force -= force;
		}

		if ( maxLength == NO_BREACH )
			return false;

		if ( ( maxLength - distanceModule ) < EPSILON )
			return true;

		return false;
	}

	return true; // part�cula � imortal e seu tempo de vida � menor ou igual a zero
}

void SpringCoil::set( Particle *particle1, Particle *particle2, float tightness, float dampingFactor )
{
	if ( particle1 && particle2 ) {
		originalDirection = particle1->getPosition() - particle2->getPosition();
		restLength = originalDirection.getModule();

		if ( restLength > 0.0 )
			originalDirection.normalize();
	}

	this->p1 = particle1;
	this->p2 = particle2;
	this->tightness = tightness;
	this->dampingFactor = dampingFactor;
}

