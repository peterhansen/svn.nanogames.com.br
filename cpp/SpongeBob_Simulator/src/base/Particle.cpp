#include "Particle.h"
#include "Force.h"

Particle::Particle( Shape *pMyShape, Point3f *myPosition, float myLife, float mySize, float myMass, Point3f *mySpeed, float myAngle, Point3f *myAxis, bool myActive )
		: Node( myPosition, mySpeed ),
		pShape( pMyShape ), size( mySize ), mass( myMass ), pLimitPlanes( NULL ),
		active( myActive ), visible( true ), immune( false ), angleValue( myAngle )
{
	if ( myAxis )
		angleAxis = *myAxis;
	else
		angleAxis.setPoint( 1.0 );

	force.setPoint();

	setLife( myLife );
}

Particle::~Particle( void ) {
	SAFE_DELETE( pShape );
}

// atualiza a part�cula
bool Particle::update( double time, DynamicVector *forces ) {
	if ( active ) {
		if ( ( !immortal && ( life -= time ) < 0.0 ) ||
				isOutsidePlanes() ) {
			// part�cula morreu
			active = false;
			visible = false;

			return false;
		}

		position += speed * time;
		if ( !immune && forces ) { // se part�cula n�o � imune a for�as, sofre a a��o das for�as recebidas como par�metro
			const UINT16 length = forces->getUsedLength();
			Force *f;
			for ( UINT16 i = 0; i < length; i++ ) { // varre o vetor de for�as
				f = (Force *) forces->getData(i);
				if ( f->isActive() ) {
					speed += *( f->getDirection() ) * ( time * time );
				}
			}
		}

		static Point3f zero;
		if ( speed != zero )
			angleAxis = speed / speed.getModule();

		return true;
	} else { // part�cula n�o est� ativa; apenas retorna false
		return false;
	}
}

void Particle::draw( void ) {
	if ( visible && pShape ) {
		loadTransform(); // carrega matrix de transforma��o de seu piv�, caso haja um

		glPushMatrix();

		glTranslatef( position.x, position.y, position.z );
		glRotatef( angleValue, angleAxis.x, angleAxis.y, angleAxis.z );

		glScalef( size, size, size );
		pShape->draw();

		/*angleAxis *= 2.0;
		glBegin( GL_LINES );
		glColor3f( 1.0, 1.0, 1.0 );
		glVertex3f( 0.0, 0.0, 0.0 );
		glVertex3f( angleAxis.x, angleAxis.y, angleAxis.z );
		glEnd();*/

		glPopMatrix();

		unloadTransform(); // descarrega matrix de transforma��o de seu piv�, caso haja um
	}
}

inline void Particle::setActive( bool active ) {
	this->active = active;
} // fim do m�todo setActive()

inline bool Particle::isActive( void ) {
	return this->active;
} // fim do m�todo isActive()

inline void Particle::setShape( Shape *pMyShape ) {
	pShape = pMyShape;
}

inline Shape* Particle::getShape( void ) {
	return pShape;
}


bool Particle::setBasedOn( Particle *baseElement ) {
	setLife( Random::nextDouble( baseElement->getLife() ) );
	mass = Random::nextDouble( baseElement->getMass() );
	size = Random::nextDouble( baseElement->getSize() );

	if ( pShape ) {
		pShape->setSize( Random::nextDouble( size ) );
		//Color c;
		//c.a = Random::nextDouble(1.0);
		//c.r = Random::nextDouble(1.0);
		//c.g = Random::nextDouble(1.0);
		//c.b = Random::nextDouble(1.0);
		//pShape->setColor( c );
	}
	position = baseElement->getPosition();
	Point3f baseSpeed = baseElement->getSpeed();
	speed.setPoint(Random::nextDouble(baseSpeed.x, -baseSpeed.x), Random::nextDouble(baseSpeed.y), Random::nextDouble(baseSpeed.z, -baseSpeed.z));

	active = true;
	visible = true;

	return true;
}


inline float Particle::getLife( void ) {
	return life;
}
inline void Particle::setLife(const float newLife) {
	life = newLife;
	if ( newLife < 0.0 )
		immortal = true;
	else
		immortal = false;
}

inline float Particle::getSize( void ) {
	return size;
}
inline void Particle::setSize( float newSize ) {
	size = newSize;
}

inline float Particle::getMass( void ) {
	return mass;
}

inline void Particle::setMass( float newMass ) {
	mass = newMass;
}

inline float Particle::getAngle( void ) {
	return angleValue;
}

inline void Particle::setAngle( float angle ) {
	angleValue = angle;
}

inline Point3f Particle::getAxis( void ) {
	return angleAxis;
}

inline void Particle::setAxis( Point3f *axis ) {
	angleAxis = *axis;
}

inline bool Particle::isImmune( void ) {
	return immune;
}

inline void Particle::setImmune ( bool myImmune ) {
	immune = myImmune;
}

inline bool Particle::isImmortal( void ) {
	return immortal;
}

inline void Particle::setImmortal ( bool myImmortal ) {
	immortal = myImmortal;
}


inline Particle * Particle::getCopy( void ) {
	Particle *p = new Particle( NULL, &position, life, size, mass, &speed, angleValue, &angleAxis, active );

	if ( p ) {
		Shape * s = pShape->getCopy();
		if ( s )
			p->setShape( s );

		p->setImmune( immune );
		p->setLimitPlanes( pLimitPlanes );
	}
	
	return p;
}

inline void Particle::setVisible( bool isVisible ) {
	visible = isVisible;
}

inline bool Particle::isVisible( void ) {
	return visible;
}


inline Plane * Particle::getLimitPlaneAt( UINT16 index ) {
	if ( pLimitPlanes ) {
		return ( Plane * ) pLimitPlanes->getData( index );
	}

	return NULL;
}

inline UINT16 Particle::insertLimitPlane( Plane * myLimitPlane ) {
	if ( !pLimitPlanes ) {
		pLimitPlanes = new DynamicVector();
		if ( !pLimitPlanes )
			return -1;
	}

	return pLimitPlanes->insert( myLimitPlane );
}

inline bool Particle::removeLimitPlaneAt( UINT16 index ) {
	if ( pLimitPlanes ) {
		return pLimitPlanes->remove( index );
	}

	return false;
}

inline void Particle::setLimitPlanes( DynamicVector *limitPlanes ) {
	pLimitPlanes = limitPlanes;
}

// retorna o primeiro plano em que a part�cula est� fora
inline Plane * Particle::isOutsidePlanes( void ) {
	if ( pLimitPlanes ) {
		UINT16 length = pLimitPlanes->getUsedLength();
		Plane *p;

		for ( UINT16 i = 0; i < length; i++ ) {
			p = ( Plane * ) pLimitPlanes->getData( i );
			if ( ( position ^ p->normal ) < p->d )
				return p;
		}
	}
	
	return NULL;
}

