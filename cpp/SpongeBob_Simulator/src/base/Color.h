#pragma once

/*==============================================================================================

ESTRUTURA Color
	Define a estrutura de uma cor.

==============================================================================================*/

class Color {
public:
	float r, g, b, a;

	Color( float r = 1.0, float g = 1.0, float b = 1.0, float a = 1.0 );
	inline void setColor( float r = 1.0, float g = 1.0, float b = 1.0, float a = 1.0 );

};