#pragma once 

#define BASE

#ifdef _WIN32
#include <windows.h>
#endif

//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <GL/glut.h>
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"

enum TIME_UNITS {
	PER_SECOND,
	PER_SECOND_SQUARE,
	PER_FRAME,
	PER_FRAME_SQUARE
};



// macro para evitar o uso de delete em ponteiros nulos
#define SAFE_DELETE(x)	if ( (x) != NULL ) delete (x);	\
						(x) = NULL

#define ABS(x) (x < 0 ? -x : x)
