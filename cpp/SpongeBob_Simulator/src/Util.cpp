#include "Util.h"


void Util::putstring (char* s, float x, float y) {
	int i;
	glDisable( GL_TEXTURE_2D );
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glColor3f(1.0f, 1.0f, 1.0f);
	glRasterPos3d(x, y, 0.8);
	for (i = 0; s[i]; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, s[i]);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_TEXTURE_2D );
}

// Reshape callback
static void reshape (int w, int h) {
	static float white[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	glViewport(0, 0, w, h);
	glClearColor(0, 0, 0, 1); 
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50.0);
	glLightModeli(GL_LIGHT_MODEL_AMBIENT, GL_TRUE);
 }