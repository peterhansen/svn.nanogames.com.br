print( "Editor de emissores de particulas" )
print( "Peter Hansen\n" )

--------
-- cores
local defaultColor	= Color:new()
local bubbleColor	= Color:new( 0.9, 0.9, 1.0, 0.6 )
local liquidColor	= Color:new( 0.95, 0.8, 0.9, 0.6 )
local bucketColor	= Color:new( 0.85, 0.15, 0.15, 1.0 )
		
-----------
-- texturas
skyTexture = Texture2D:new()
skyTexture:loadTexture( "./textures/sky.bmp" )

groundTexture = Texture2D:new()
groundTexture:loadTexture( "./textures/wood.bmp" )

bucketTexture = Texture2D:new()
bucketTexture:loadTexture( "./textures/foam.bmp" )

liquidTexture = Texture2D:new()
liquidTexture:loadTexture( "./textures/water3.bmp" )

bubbleTexture = Texture2D:new()
bubbleTexture:loadTexture( "./textures/fog.bmp" )


----------
-- esferas
local skySphere

skySphere = Sphere:new( 1.0, 16, skyTexture )
skySphere:build()


local bubbleSphere

bubbleSphere = Sphere:new( 1.0, 12, bubbleTexture )
bubbleSphere:build()
bubbleSphere:setColor( bubbleColor )


------------
-- cilindros
local bucketCylinder = Cylinder:new( nil, 1.0, 200.0, 180.0, 200.0, 50, bucketTexture, 1 )
bucketCylinder:build()
bucketCylinder:setColor( bucketColor )

local liquidCylinder = Cylinder:new( nil, 1.0, 160.0, 179.0, 189.0, 40, liquidTexture, 2 )
liquidCylinder:build()
liquidCylinder:setColor( liquidColor )


---------
-- sprites
local groundSprite

groundSprite = Sprite:createSprite( groundTexture, 1.0, 1.0 )


----------------------------------------------
-- plano limite inferior das part�culas (ch�o)
local limitPlane = Plane:new() -- plano padr�o � x/z

-------------
-- part�culas

local bubbleParticle
local ambientParticle
local groundParticle
	
bubbleParticle = Particle:new( bubbleSphere, nil, 3.6, 16 )
local bubbleSpeed = Point3f:new( 20, 140, 20 )
bubbleParticle:setSpeed( bubbleSpeed )
bubbleParticle:setLimitPlane( limitPlane )

local bucket = Particle:new( bucketCylinder, nil, -1 )
bucket:setImmune( true )

local liquid = Particle:new( liquidCylinder, nil, -1 )
liquid:setImmune( true )


ambientParticle = Particle:new( skySphere, nil, -1 )
ambientParticle:setImmune( true )
ambientParticle:setSize( 1000.0 )

groundPos = Point3f:new( 0.0, -2 )
groundParticle = Particle:new( groundSprite, groundPos, -1 )
groundParticle:setSize( 2000.0 )
groundParticle:setImmune( true )
groundParticle:setAngle( 90.0 )


------------
-- emissores
local bubbleEmitter
		
local bubbleEmitterPos = Point3f:new( 0.0, 10.0 )
bubbleEmitter = AreaEmitter:new( 1000.0, nil, bubbleEmitterPos )
bubbleEmitter:setBaseElement( bubbleParticle )
bubbleEmitter:setImmune( true )
local bubblePlaneTop = Point3f:new( 1.0 )
local bubblePlaneRight = Point3f:new( 0.0, 0.0, 1.0 )
local bubblePlane = Plane:new( bubblePlaneTop, bubblePlaneRight )
bubbleEmitter:setPlane( bubblePlane, 200, 200 )
bubbleEmitter:setVisible( false )


------------
-- gravidade
local gravity
local gravityDirection
		
gravityDirection = Point3f:new( 0.0, -6 )
gravity = Force:new( gravityDirection )


-----------
-- ambiente
local environment

environment = Environment:new()
environment:insertParticle( ambientParticle )
environment:insertParticle( groundParticle )
environment:insertParticle( bubbleEmitter )
environment:insertParticle( bucket )
environment:insertParticle( liquid )
environment:insertForce( gravity )


--------
-- mundo
local world = World:new()
world:insertEnvironment( environment )


--------
-- luzes
local lightPos = Point3f:new( )
local lightColor = Color:new( 0.65, 0.5, 0.5, 1.0 )
local GL_LIGHT0 = 16384
local GL_AMBIENT = 4608
local light = Light:new( lightPos, lightColor, GL_LIGHT0, GL_AMBIENT )

local spotlightPos = Point3f:new( 0, 400, -300 )
local spotlightDir = Point3f:new( 0.0, -0.5, 0.50 )
local spotlightColor = Color:new( 0.45, 0.9, 0.9, 1.0 )
local GL_LIGHT1 = 16385
local spotlight = SpotLight:new( spotlightPos, spotlightColor, GL_LIGHT1, spotlightDir, 50, 0 )


---------
-- c�mera
local CameraCenter	= Point3f:new( 0.0, 60.0, 0.0 )
local CameraEye		= Point3f:new( 0.0, 840.0, -500.0 )
local CameraUp		= Point3f:new( 0.0, 1.0, 0.0 )
local camera = PerspectiveCamera:new( CameraCenter, CameraEye, CameraUp )


-------
-- cena
local scene = Scene:new()
scene:insertWorld( world )
scene:insertLight( light )
scene:insertLight( spotlight )
scene:setCurrentCamera( camera ) -- define c�mera atual
local clock = scene:getClock()
clock:setVisible( true ) -- mostra contador de frames

Main:setScene( scene )

print( "CENA INICIALIZADA COM SUCESSO!" )