print( "Editor de emissores de particulas" )
print( "Peter Hansen\n" )

--------
-- cores
local lavaColor		= Color:new( 1.0, 0.8, 0.8, 0.6 )
local rockColor		= Color:new( 0.7, 0.55, 0.55, 1.0 )
local grassColor	= Color:new( 0.9, 0.9, 0.9, 1.0 )
local smokeColor	= Color:new( 0.7, 0.6, 0.6, 0.3 )
		
-----------
-- texturas
skyTexture = Texture2D:new()
skyTexture:loadTexture( "./textures/sky2.bmp" )

groundTexture = Texture2D:new()
groundTexture:loadTexture( "./textures/grass.bmp" )

rockTexture = Texture2D:new()
rockTexture:loadTexture( "./textures/rock.bmp" )

rock2Texture = Texture2D:new()
rock2Texture:loadTexture( "./textures/rock2.bmp" )

smokeTexture = Texture2D:new()
smokeTexture:loadTexture( "./textures/fog.bmp" )

lavaTexture = Texture2D:new()
lavaTexture:loadTexture( "./textures/lava.bmp" )

----------
-- esferas
local skySphere
skySphere = Sphere:new( 1.0, 16, skyTexture )
skySphere:build()


local rockSphere
rockSphere = Sphere:new( 1.0, 8, rockTexture )
rockSphere:build()
rockSphere:setColor( rockColor )

local smokeSphere
smokeSphere = Sphere:new( 1.0, 8, smokeTexture )
smokeSphere:build()
smokeSphere:setColor( smokeColor )


------------
-- cilindros
local volcanoHeight = 230
local volcanoCylinder = Cylinder:new( nil, 1.0, volcanoHeight, 175.0, 35.0, 20, rock2Texture, 0 )
volcanoCylinder:build()
volcanoCylinder:setColor( grassColor )

local lavaCylinder = Cylinder:new( nil, 1.0, volcanoHeight - 10, 165.0, 30.0, 10, lavaTexture, 2 )
lavaCylinder:build()
lavaCylinder:setColor( lavaColor )


---------
-- sprites
local groundSprite

groundSprite = Sprite:createSprite( groundTexture, 1.0, 1.0 )


----------------------------------------------
-- plano limite inferior das part�culas (ch�o)
local limitPlane = Plane:new() -- plano padr�o � x/z

-------------
-- part�culas

local sky = Particle:new( skySphere, nil, -1 )
sky:setImmune( true )
sky:setSize( 1200 )

local volcano = Particle:new( volcanoCylinder, nil, -1 )
volcano:setImmune( true )

local lava = Particle:new( lavaCylinder, nil, -1 )
lava:setImmune( true )


local ground = Particle:new( groundSprite )
ground:setSize( 2000.0 )
ground:setImmune( true )
ground:setAngle( 90.0 )

local smoke =  Particle:new( smokeSphere, nil, 2 )
local smokeSpeed = Point3f:new( 5, 15, 5 )
smoke:setSpeed( smokeSpeed )
smoke:setSize( 30 )
smoke:setImmune( true )
smoke:setLimitPlane( limitPlane )

------------
-- emissores
local rockEmitter
rockEmitter = Emitter:new( 60, rockSphere )
rockEmitter:setVisible( true )
rockEmitter:setSize( 23 )
rockEmitter:setBaseElement( smoke )
local rockEmitterSpeed = Point3f:new( 80, 600, 80 )
rockEmitter:setSpeed( rockEmitterSpeed )
rockEmitter:setLife( 15 )
rockEmitter:setLimitPlane( limitPlane )


local volcanoEmitter
local volcanoEmitterPos = Point3f:new( 0.0, volcanoHeight - 10 )
volcanoEmitter = AreaEmitter:new( 10, nil, volcanoEmitterPos )
volcanoEmitter:setVisible( false )
volcanoEmitter:setBaseElement( rockEmitter )
volcanoEmitter:setImmune( true )
local rockPlaneTop = Point3f:new( 1.0 )
local rockPlaneRight = Point3f:new( 0.0, 0.0, 1.0 )
local rockPlane = Plane:new( rockPlaneTop, rockPlaneRight )
volcanoEmitter:setPlane( rockPlane, 5, 5 )


------------
-- gravidade
local gravity
local gravityDirection
		
gravityDirection = Point3f:new( 0.0, -9800 )
gravity = Force:new( gravityDirection )


-----------
-- ambiente
local environment

environment = Environment:new()
environment:insertParticle( volcano )
environment:insertParticle( ground )
environment:insertParticle( lava )
environment:insertParticle( sky )
environment:insertParticle( volcanoEmitter )
environment:insertForce( gravity )


--------
-- mundo
local world = World:new()
world:insertEnvironment( environment )


--------
-- luzes
local lightPos = Point3f:new( )
local lightColor = Color:new( 0.4, 0.4, 0.4, 1.0 )
local GL_LIGHT0 = 16384
local GL_AMBIENT = 4608
local light = Light:new( lightPos, lightColor, GL_LIGHT0, GL_AMBIENT )

---------
-- c�mera
local CameraCenter	= Point3f:new( 0.0, 60.0, 0.0 )
local CameraEye		= Point3f:new( 500.0, 880.0, -600.0 )
local CameraUp		= Point3f:new( 0.0, 1.0, 0.0 )
local camera = PerspectiveCamera:new( CameraCenter, CameraEye, CameraUp )


-------
-- cena
local scene = Scene:new()
scene:insertWorld( world )
scene:insertLight( light )
scene:setCurrentCamera( camera ) -- define c�mera atual
local clock = scene:getClock()
clock:setVisible( true ) -- mostra contador de frames

Main:setScene( scene )

print( "CENA INICIALIZADA COM SUCESSO!" )