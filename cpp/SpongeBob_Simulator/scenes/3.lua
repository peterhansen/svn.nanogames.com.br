print( "Editor de emissores de particulas" )
print( "Peter Hansen\n" )

--------
-- cores
local defaultColor	= Color:new()
local bubbleColor	= Color:new( 0.4, 0.4, 0.4, 0.6 )
local liquidColor	= Color:new( 0.0, 0.0, 0.0 , 0.7 )
local glassColor	= Color:new( 0.9, 0.9, 0.9, 0.9 )
local iceColor		= Color:new( 0.7, 0.7, 1.0, 0.6 )
		
-----------
-- texturas
skyTexture = Texture2D:new()
skyTexture:loadTexture( "./textures/sky.bmp" )

groundTexture = Texture2D:new()
groundTexture:loadTexture( "./textures/wood.bmp" )

glassTexture = Texture2D:new()
glassTexture:loadTexture( "./textures/pepsi.bmp" )

liquidTexture = Texture2D:new()
liquidTexture:loadTexture( "./textures/water3.bmp" )

bubbleTexture = Texture2D:new()
bubbleTexture:loadTexture( "./textures/fog.bmp" )


----------
-- esferas
local skySphere

skySphere = Sphere:new( 1.0, 16, skyTexture )
skySphere:build()


local bubbleSphere

bubbleSphere = Sphere:new( 1.0, 6, bubbleTexture )
bubbleSphere:build()
bubbleSphere:setColor( bubbleColor )


------------
-- cilindros
local glassCylinder = Cylinder:new( nil, 1.0, 300.0, 80.0, 100.0, 50, glassTexture, 1 )
glassCylinder:build()
glassCylinder:setColor( glassColor )

local liquidCylinder = Cylinder:new( nil, 1.0, 230.0, 75.0, 95.0, 50, liquidTexture, 2 )
liquidCylinder:build()
liquidCylinder:setColor( liquidColor )


---------
-- sprites
local groundSprite

groundSprite = Sprite:createSprite( groundTexture, 1.0, 1.0 )


----------------------------------------------
-- plano limite inferior das part�culas (ch�o)
local limitPlane = Plane:new() -- plano padr�o � x/z

-------------
-- part�culas

local bubbleParticle
local ambientParticle
local groundParticle
	
bubbleParticle = Particle:new( bubbleSphere, nil, 3.8, 5 )
local bubbleSpeed = Point3f:new( 9, 65, 9 )
bubbleParticle:setSpeed( bubbleSpeed )
bubbleParticle:setLimitPlane( limitPlane )

local glass = Particle:new( glassCylinder, nil, -1 )
glass:setImmune( true )

local liquid = Particle:new( liquidCylinder, nil, -1 )
liquid:setImmune( true )


ambientParticle = Particle:new( skySphere, nil, -1 )
ambientParticle:setImmune( true )
ambientParticle:setSize( 1000.0 )

groundPos = Point3f:new( 0.0, -2 )
groundParticle = Particle:new( groundSprite, groundPos, -1 )
groundParticle:setSize( 2000.0 )
groundParticle:setImmune( true )
groundParticle:setAngle( 90.0 )

cubePos = Point3f:new( 0, 220, 10 )
cube = Cube:new( 1, 1, 1, liquidTexture )
cube:setColor( iceColor )
iceCube = Particle:new( cube, cubePos, -1, 50 )
iceCube:setAngle( 147 )
iceCube:setImmune( true )

cube2Pos = Point3f:new( 30, 206, -43 )
cube2 = Cube:new( 0.5, 0.6, 1.2, liquidTexture )
cube:setColor( iceColor )
iceCube2 = Particle:new( cube, cube2Pos, -1, 50 )
iceCube2Axis = Point3f:new( 1, 1, 0 )
iceCube2:setAxis( iceCube2Axis )
iceCube2:setAngle( 81 )
iceCube2:setImmune( true )

cube3Pos = Point3f:new( 50, 210, 22 )
cube3 = Cube:new( 0.5, 0.6, 1.2, liquidTexture )
cube:setColor( iceColor )
iceCube3 = Particle:new( cube, cube3Pos, -1, 50 )
iceCube3Axis = Point3f:new( 0, 1, 1 )
iceCube3:setAxis( iceCube3Axis )
iceCube3:setAngle( 308 )
iceCube3:setImmune( true )


------------
-- emissores
local bubbleEmitter
		
local bubbleEmitterPos = Point3f:new( 0.0, 10.0 )
bubbleEmitter = AreaEmitter:new( 300.0, nil, bubbleEmitterPos )
bubbleEmitter:setBaseElement( bubbleParticle )
bubbleEmitter:setImmune( true )
local bubblePlaneTop = Point3f:new( 1.0 )
local bubblePlaneRight = Point3f:new( 0.0, 0.0, 1.0 )
local bubblePlane = Plane:new( bubblePlaneTop, bubblePlaneRight )
bubbleEmitter:setPlane( bubblePlane, 92, 92 )
bubbleEmitter:setVisible( false )


------------
-- gravidade
local gravity
local gravityDirection
		
gravityDirection = Point3f:new( 0.0, -6, 100 )
gravity = Force:new( gravityDirection )


-----------
-- ambiente
local environment

environment = Environment:new()
--environment:insertParticle( ambientParticle )
environment:insertParticle( groundParticle )
environment:insertParticle( bubbleEmitter )
environment:insertParticle( glass )
environment:insertParticle( liquid )
environment:insertParticle( iceCube )
environment:insertParticle( iceCube2 )
environment:insertParticle( iceCube3 )
environment:insertForce( gravity )


--------
-- mundo
local world = World:new()
world:insertEnvironment( environment )


--------
-- luzes
local lightPos = Point3f:new( )
local lightColor = Color:new( 0.4, 0.4, 0.4, 1.0 )
local GL_LIGHT0 = 16384
local GL_AMBIENT = 4608
local light = Light:new( lightPos, lightColor, GL_LIGHT0, GL_AMBIENT )

local spotlightPos = Point3f:new( 0, 400, -300 )
local spotlightDir = Point3f:new( 0.0, -0.4, 0.3 )
local spotlightColor = Color:new( 6.0, 5.9, 5.9, 1.0 ) -- valores de cor acima do padr�o para refor�ar spot
local GL_LIGHT1 = 16385
local spotlight = SpotLight:new( spotlightPos, spotlightColor, GL_LIGHT1, spotlightDir, 30, 0 )

---------
-- c�mera
local CameraCenter	= Point3f:new( 0.0, 60.0, 0.0 )
local CameraEye		= Point3f:new( 0.0, 840.0, -500.0 )
local CameraUp		= Point3f:new( 0.0, 1.0, 0.0 )
local camera = PerspectiveCamera:new( CameraCenter, CameraEye, CameraUp )

spotlight:attach( camera )


-------
-- cena
local scene = Scene:new()
scene:insertWorld( world )
scene:insertLight( light )
scene:insertLight( spotlight )
scene:setCurrentCamera( camera ) -- define c�mera atual
local clock = scene:getClock()
clock:setVisible( true ) -- mostra contador de frames

Main:setScene( scene )

print( "CENA INICIALIZADA COM SUCESSO!" )