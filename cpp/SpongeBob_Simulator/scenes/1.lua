print( "SpongeBob Simulator" )
print( "by Fatty & Bifekibe\n" )

local WORLD_DIMENSION = 500


-----------
-- texturas
local skyTexture
local groundTexture
local bobTexture
		
skyTexture = Texture2D:new()
skyTexture:loadTexture( "./textures/sky.bmp" )

bobTexture = Texture2D:new()
bobTexture:loadTexture( "./textures/bob.bmp" )

groundTexture = Texture2D:new()
groundTexture:loadTexture( "./textures/grass.bmp" )

bubbleTexture = Texture2D:new()
bubbleTexture:loadTexture( "./textures/fog.bmp" )

----------
-- esferas
local skySphere
skySphere = Sphere:new( 1.0, 16, skyTexture )
skySphere:build()

local bubbleColor = Color:new( 0.9, 0.9, 1.0, 0.2 )
local bubbleSphere
bubbleSphere = Sphere:new( 1.0, 8, bubbleTexture )
bubbleSphere:build()
bubbleSphere:setColor( bubbleColor )


---------
-- sprites
local groundSprite

groundSprite = Sprite:createSprite( groundTexture, 1.0, 1.0 )

----------------------------------------------
-- planos limite das part�culas
local floorPlane = Plane:new( ) -- plano padr�o � x/z

up = Point3f:new( 0, 0, 1.0 )
right = Point3f:new( 0.0, 1.0, 0 )
local leftWallPlane = Plane:new( up, right, -WORLD_DIMENSION / 2 )

up:setPoint( 0, 0, -1 )
right:setPoint( 0, 1, 0 )
local rightWallPlane = Plane:new( up, right, -WORLD_DIMENSION / 2 )

up:setPoint( 0, 0, 1 )
right:setPoint( 1, 0, 0 )
local topPlane = Plane:new( up, right, -WORLD_DIMENSION )

up:setPoint( 1, 0, 0 )
right:setPoint( 0, 1, 0 )
local rearPlane = Plane:new( up, right, -WORLD_DIMENSION / 2 )

up:setPoint( -1, 0, 0 )
right:setPoint( 0, 1, 0 )
local frontPlane = Plane:new( up, right, -WORLD_DIMENSION / 2 )


------------
-- emissores
local skyEmitter -- dummy (cen�rio, n�o emite part�culas)
local groundEmitter -- dummy (cen�rio, n�o emite part�culas)
local pos = Point3f:new()
		
skyEmitter = Emitter:new( 0.0, skySphere )
skyEmitter:setSize( 1000.0 )
skyEmitter:setImmune( true )

groundEmitter = Emitter:new( 0.0, groundSprite )
groundEmitter:setSize( WORLD_DIMENSION )
groundEmitter:setImmune( true )
groundEmitter:setAngle( 90.0 )

topEmitter = Emitter:new( 0.0, groundSprite )
topEmitter:setSize( WORLD_DIMENSION )
pos:setPoint( 0, WORLD_DIMENSION, 0 )
topEmitter:setPosition( pos )
topEmitter:setImmune( true )
topEmitter:setAngle( 270.0 )

rightEmitter = Emitter:new( 0.0, groundSprite )
rightEmitter:setSize( WORLD_DIMENSION )
rightEmitter:setImmune( true )
axis = Point3f:new( 0.0, 1.0, 0.0 )
pos:setPoint( WORLD_DIMENSION / 2, WORLD_DIMENSION / 2, 0 )
rightEmitter:setPosition( pos )
rightEmitter:setAxis( axis )
rightEmitter:setAngle( 270.0 )

leftEmitter = Emitter:new( 0.0, groundSprite )
leftEmitter:setSize( WORLD_DIMENSION )
leftEmitter:setImmune( true )
axis = Point3f:new( 0.0, 1.0, 0.0 )
pos:setPoint( -WORLD_DIMENSION / 2, WORLD_DIMENSION / 2, 0 )
leftEmitter:setPosition( pos )
leftEmitter:setAxis( axis )
leftEmitter:setAngle( 270.0 )

rearEmitter = Emitter:new( 0.0, groundSprite )
rearEmitter:setSize( WORLD_DIMENSION )
rearEmitter:setImmune( true )
axis = Point3f:new( 0.0, 1.0, 0.0 )
pos:setPoint( 0, WORLD_DIMENSION / 2, -WORLD_DIMENSION / 2 )
rearEmitter:setPosition( pos )
rearEmitter:setAxis( axis )
rearEmitter:setAngle( 180.0 )

frontEmitter = Emitter:new( 0.0, groundSprite )
frontEmitter:setSize( WORLD_DIMENSION )
frontEmitter:setImmune( true )
axis = Point3f:new( 0.0, 1.0, 0.0 )
pos:setPoint( 0, WORLD_DIMENSION / 2, WORLD_DIMENSION / 2 )
frontEmitter:setPosition( pos )
frontEmitter:setAxis( axis )
frontEmitter:setAngle( 180.0 )


------------------------
-- part�culas das bolhas
local bubbleParticle
	
bubbleParticle = Particle:new( bubbleSphere, nil, 50, 26 )
local bubbleSpeed = Point3f:new( 20, 140, 20 )
bubbleParticle:setSpeed( bubbleSpeed )
bubbleParticle:setImmune( true )
bubbleParticle:insertLimitPlane( topPlane )


------------
-- emissores
local bubbleEmitter
		
bubbleEmitter = AreaEmitter:new( 10 )
bubbleEmitter:setBaseElement( bubbleParticle )
bubbleEmitter:setImmune( true )
local bubblePlaneTop = Point3f:new( 1.0 )
local bubblePlaneRight = Point3f:new( 0.0, 0.0, 1.0 )
local bubblePlane = Plane:new( bubblePlaneTop, bubblePlaneRight )
bubbleEmitter:setPlane( bubblePlane, WORLD_DIMENSION, WORLD_DIMENSION )
bubbleEmitter:setVisible( false )


---------------
-- Bob Esponja!
local cubeSize = 100
local cubePosition	= Point3f:new( 0, cubeSize )

local sprite = Sprite:createSprite( bobTexture, 1, 1 )

cube = SoftCube:new( cubePosition )
cube:build( cubeSize, 5, 700.0, 23.0, 0.5 )
cube:insertLimitPlane( floorPlane )
cube:insertLimitPlane( leftWallPlane )
cube:insertLimitPlane( rightWallPlane )
cube:insertLimitPlane( topPlane )
cube:insertLimitPlane( rearPlane )
cube:insertLimitPlane( frontPlane )
cube:setShape( sprite )


------------
-- gravidade
local gravity
local gravityDirection
		
gravityDirection = Point3f:new( 0.0, -598.0 )
gravity = Force:new( gravityDirection )


-----------
-- ambiente
local environment

environment = Environment:new()
environment:insertParticle( groundEmitter )
environment:insertParticle( rightEmitter )
environment:insertParticle( leftEmitter )
--environment:insertParticle( frontEmitter )
environment:insertParticle( cube )
environment:insertParticle( rearEmitter )
environment:insertParticle( topEmitter )
environment:insertParticle( skyEmitter )
environment:insertParticle( bubbleEmitter )
environment:insertForce( gravity )

--------
-- mundo
local world = World:new()
world:insertEnvironment( environment )


--------
-- luzes
local lightPos = Point3f:new( )
local lightColor = Color:new( 0.65, 0.75, 0.95, 1.0 )
local GL_LIGHT0 = 16384
local GL_AMBIENT = 4608
local light = Light:new( lightPos, lightColor, GL_LIGHT0, GL_AMBIENT )


---------
-- c�mera
local CameraCenter	= Point3f:new( 0.0, 0.0, 0.0 )
local CameraEye		= Point3f:new( 0.0, 450.0, 500.0 )
local CameraUp		= Point3f:new( 0.0, 1.0, 0.0 )
local camera = PerspectiveCamera:new( CameraCenter, CameraEye, CameraUp )

camera:attach( cube, 0 )


-------
-- cena
local scene = Scene:new()
scene:insertWorld( world )
scene:insertLight( light )
scene:setCurrentCamera( camera ) -- define c�mera atual
local clock = scene:getClock()
clock:setVisible( true ) -- mostra contador de frames
clock:setMaxFrameTime( 0.02 )

Main:setScene( scene, cube )

print( "CENA INICIALIZADA COM SUCESSO!" )