#include "src/Main.h"

/* Display mode */
static bool help = true;  /* help message on/off */

static int	quantMovimentoEmX = 0, velhoX = 0,
			quantMovimentoEmY = 0, velhoY = 0;


/*============= Elementos da cena ==========================*/
static Scene *pScene;// ponteiro para a cena atual
static SoftCube *pCube;// ponteiro para o cubo gelatinoso
static Point3f speed( 0, 300, 0 );
/*==========================================================*/



void mouse( int botao, int estado, int x, int y ) {
	if ( estado == GLUT_DOWN ) {
		switch ( botao ) {
			case GLUT_LEFT_BUTTON:
			velhoX = x;           // armazena a posicao x do mouse
			velhoY = y;
			default:                // no momento em que o botao foi pressionado
			break;
		}
	}
}

void mouseMotion( int x, int y) {
	quantMovimentoEmX = velhoX - x;    // a diferenca entre o valor antigo
										// do mouse e o novo nos fornece
										// a distancia percorrida pelo mouse
	quantMovimentoEmY = velhoY - y;
	velhoX = x;
	velhoY = y;
	pScene->getCurrentCamera()->rotate( &Point3f( quantMovimentoEmX, quantMovimentoEmY ) );
} 

static void drawscene () {
	if ( pScene )
		pScene->render();
}


/* Display help message */
static void showhelp () {
	static char text[256];
	sprintf( text, "t: drawTempCoils(%s)", pCube->drawTempCoils ? "true" : "false" );
	Util::putstring( text, -0.95, -0.43 );
	sprintf( text, "m: drawMode(%s)", pCube->getDrawMode() == DRAW_MODE_WIRED ? "wire" : "quads" );
	Util::putstring( text, -0.95, -0.51 );
	sprintf( text, "Spacebar: launch at speed (%.2f, %.2f, %.2f)", speed.x, speed.y, speed.z );
	Util::putstring( text, -0.95, -0.59 );
	Util::putstring( "Mouse left button + movement: rotate camera", -0.95, -0.67 );
	Util::putstring( "z,x: enable/disable wireframe view", -0.95, -0.75 );
	Util::putstring( "w,s,a,d,r,f: move camera",-0.95,-0.83 );
	Util::putstring( "ESC: exit",-0.95,-0.91 );
}

// Reshape callback
static void reshape (int w, int h) {
	static float white[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	glViewport(0,0,w,h);
	glClearColor(0,0,0,1); 
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100.0);
	glLightModeli(GL_LIGHT_MODEL_AMBIENT, GL_TRUE);
 }

// Display callback
static void display ( void ) {
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	if ( pScene ) {
		Camera *pCamera = pScene->getCurrentCamera();
		if ( pCamera ) {
			pCamera->place();
		}
	}

	// atualiza a anima��o do Luxor
	quantMovimentoEmX = 0;

	// draw scene
	drawscene();

	// display help message
	if (help)
		showhelp();

	glutSwapBuffers();

} // fim do m�todo display()

/* Idle callback */
static void idle ( void ) {
	if ( pScene )
		pScene->update();

	display();
}

// Keyboard callback
static void keyboard (unsigned char key, int x, int y) {
	Point3f d = Point3f();

	switch (key) {
		case '1': speed.x -= SPEED_INC; break;
		case '2': speed.x = 0.0; break;
		case '3': speed.x += SPEED_INC; break;
		case '4': speed.y -= SPEED_INC; break;
		case '5': speed.y = 0.0; break;
		case '6': speed.y += SPEED_INC; break;
		case '7': speed.z -= SPEED_INC; break;
		case '8': speed.z = 0.0; break;
		case '9': speed.z += SPEED_INC; break;
		case ' ': 
			pCube->reset();
			pCube->setSpeed( &speed );
			break;

		case 't': pCube->drawTempCoils = !pCube->drawTempCoils;	break;

		case 'm': pCube->setDrawMode( ( pCube->getDrawMode() + 1 ) % 2 ); break;
		case 'j': glEnable(GL_FOG); break;
		case 'k': glDisable(GL_FOG); break;
		case 's': d.z = DL; break;
		case 'w': d.z = -DL; break;
		case 'd': d.x = DL; break;
		case 'a': d.x = -DL; break;
		case 'r': d.y = DL; break;
		case 'f': d.y = -DL; break;
		case 'z': glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
		case 'x': glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;
		case 'h': case 'H': help ^= 1; break;
		case 27: exit(0); break;
	}
	if ( pScene ) {
		Camera *pCamera = pScene->getCurrentCamera();
		if ( pCamera ) {
			pCamera->move(&d);
		}
	}
}

// Special keyboard callback
static void special (int key, int x, int y)
{
}


int _tmain(int argc, _TCHAR* argv[]) {
	/*printf( "argc: %d\n", argc );
	for ( int i = 0; i < argc; i++ )
		printf( "argv[%d]: %s\n", i, argv[i] );*/
	if ( argc < 2 ) {
		printf( "Uso: %s <arquivo de entrada>\n", argv[0] );
		return -1;
	}

	// open GLUT 
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB);
	glutInitWindowSize(W,H); 
    
	// create window
	glutCreateWindow ("Simula��o F�sica - Trabalho Final - Daniel Alves e Peter Hansen");
	glutReshapeFunc(reshape); 
	glutDisplayFunc(display); 
	glutKeyboardFunc(keyboard);
	glutMouseFunc( mouse );
	glutMotionFunc( mouseMotion ); 
	glutSpecialFunc(special);
	glutIdleFunc(idle);

	//glutFullScreen();

	if( !init( argv[1] ) )
		return 0;

	// interact... 
	glutMainLoop();

	delete pScene;
	
	return 0;
}

bool init( const char *luaFile )
{
	lua_State *L = lua_open();
	lua_baselibopen( L );
	lua_iolibopen( L );
	lua_strlibopen( L );
	lua_mathlibopen( L );

	tolua_luaParticle_open( L );
	tolua_luaEmitter_open( L );

	tolua_luaAreaEmitter_open( L );
	tolua_luaCamera_open( L );
	tolua_luaClock_open( L );
	tolua_luaColor_open( L );
	tolua_luaCube_open( L );
	tolua_luaCubeParticle_open( L );
	tolua_luaCylinder_open( L );
	tolua_luaEnvironment_open( L );
	tolua_luaForce_open( L );
	tolua_luaLight_open( L );
	tolua_luaLineEmitter_open( L );
	tolua_luaMain_open( L );
	tolua_luaNode_open( L );
	tolua_luaPerspectiveCamera_open( L );
	tolua_luaPlane_open( L );
	tolua_luaPoint_open( L );
	tolua_luaPoint3f_open( L );
	tolua_luaQuaternion_open( L );
	tolua_luaScene_open( L );
	tolua_luaShape_open( L );
	tolua_luaSoftCube_open( L );
	tolua_luaSpringCoil_open( L );
	tolua_luaSphere_open( L );
	tolua_luaSpotLight_open( L );
	tolua_luaSprite_open( L );
	tolua_luaTexture_open( L );
	tolua_luaTexture1D_open( L );
	tolua_luaTexture2D_open( L );
	tolua_luaWorld_open( L );

	lua_dofile( L, luaFile );

	lua_close( L );

	glEnable(GL_FOG);
	glFogi (GL_FOG_MODE, GL_LINEAR);
	glFogi (GL_FOG_INDEX, NUMCOLORS);
	glFogf (GL_FOG_START, 1.0);
	glFogf (GL_FOG_END, 3500.0);
	glHint (GL_FOG_HINT, GL_NICEST);

	return true;
} // fim do m�todo init()


void Main::setScene( Scene *scene, SoftCube *cube ) {
	pScene = scene;
	pCube = cube;
}