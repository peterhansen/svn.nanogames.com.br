How does MySQL++ compare to MySQL’s Connector/C++?

Connector/C++ is a new C++ wrapper for the MySQL C API library, fully developed under the MySQL corporate aegis. By contrast, MySQL++ has a long, complex development history. MySQL++ started out as a third-party library, was maintained and enhanced by MySQL for several years, then got spun back out again, this time probably for good. MySQL does still host our mailing list, for which we thank them, but they don’t control the hosting or development of MySQL++ any more.

MySQL decided to create a competing library for two reasons. First, having the entire thing to themselves mean they can do the same dual-licensing thing they do with the C API library. Second, after Sun bought MySQL, they wanted to put MySQL support into OpenOffice, and wanted a JDBC style API for that support.

By contrast with Connector/C++’s Java-style database API, MySQL++ is very much a native C++ library: we use STL and other Standard C++ features heavily. If you are a Java developer or simply admire its database interface design, you may prefer Connector/C++.

Another consideration is that Connector/C++ is new and therefore perhaps less crufty, while MySQL++ is mature and perhaps showing some age.

MySQL Connector/C++ supports MySQL 5.1 and later:
http://dev.mysql.com/doc/refman/5.5/en/connector-cpp.html

MySQL++ works best with MySQL version 4.1 or higher, simply because this is the oldest version that it’s regularly tested against during development.
MySQL++ is a MySQL API for C++. Warren Young has taken over this project. More information can be found at:
http://tangentsoft.net/mysql++/

http://www.drdobbs.com/184401949

>>> EXCEPTION => C++ exception caught in main( int, char* ): basic_string::substr

- MySQL++ Modo DEBUG
http://lists.mysql.com/plusplus/7236

Graham Reitz wrote:
> 
> When you create a C++ Command Line Tool project in Xcode 3.0 it adds the 
> following preprocessor defines:
> 
> _GLIBCXX_DEBUG=1 _GLIBCXX_DEBUG_PEDANTIC=1
> 
> When these are there it crashes in debug mode.

The mighty Google says these flags turn on internal debugging within the 
standard libraries, or at least within STL.

It may be that you have to use these flags for all modules within the 
project, which wouldn't happen if you're building MySQL++ using the 
standard configure && make && make install pattern.

If you did indeed build MySQL++ this way, try this:

    $ cd /mysqlpp/directory
    $ make clean
    $ ./configure CXXFLAGS="_GLIBCXX_DEBUG=1 _GLIBCXX_DEBUG_PEDANTIC=1"
    $ make && sudo make install

This would be the "debug version" you sought after earlier, though there 
is no way to distinguish it from a release version from the outside.

> Even if I set the to =0 it still crashes.

Clearly some code is using #if defined(X) or #ifdef X instead of #if X 
to decide whether this flag is "on".

Então: CXXFLAGS="-D _GLIBCXX_DEBUG=1 -D _GLIBCXX_DEBUG_PEDANTIC=1 -g -fno-default-inline -fno-inline-functions -fno-inline -O0"

/* Gerados automaticamente pelo Xcode:

	ALWAYS_SEARCH_USER_PATHS = NO;
	COPY_PHASE_STRIP = NO;
	GCC_DYNAMIC_NO_PIC = NO;						( -fno-pic e -mdynamic-no-pic )
	GCC_ENABLE_FIX_AND_CONTINUE = YES;
	GCC_MODEL_TUNING = G5;							( -mtune=G5 ou -mcpu=G5 )
V	GCC_OPTIMIZATION_LEVEL = 0;						(-O0 )
V	GCC_PREPROCESSOR_DEFINITIONS = (
		"_GLIBCXX_DEBUG=1",
		"_GLIBCXX_DEBUG_PEDANTIC=1",
	);
*/

Então: CXXFLAGS="-D _GLIBCXX_DEBUG=1 -D _GLIBCXX_DEBUG_PEDANTIC=1 -g -O0 -D COPY_PHASE_STRIP=0 -D GCC_DYNAMIC_NO_PIC=0 -D GCC_ENABLE_FIX_AND_CONTINUE=1 -D GCC_MODEL_TUNING=G5"

// Compilando Wireshark

sudo port install git

http://www.gnu.org/software/gettext/
http://live.gnome.org/GTK%2B/OSX/BuildInstructions
http://autoconf.darwinports.com/
http://gtk2.darwinports.com/
http://macdevcenter.com/pub/a/mac/2007/06/22/graphical-tool-kits-for-apples-os-x-gtk2.html


// SQL

-- Acha os campos de senha
SELECT SUBSTR( hex( tcpdata ), INSTR( hex( tcpdata ), '1D60' ) ), idConversation, packetseqnum  FROM tcppackets WHERE hex( tcpdata ) LIKE '%1d60%';

-- Obtém todas as conversas que realmente começam do início da conversa TCP
SELECT *
FROM tcpConversations
WHERE idConversations IN 	( 
								SELECT idConversation
								FROM tcpPackets
								WHERE ( tcpsyn = 1 ) AND ( packetsqnum = 0 ) AND ( direction = 0 )
							)
