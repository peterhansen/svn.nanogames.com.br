/*
 *  Utils.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/13/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef UTILS_H
#define UTILS_H

// C++
#include <string>
#include <vector>

// Oi
#include "oidefs.h"

namespace Oi
{
	namespace Utils
	{
		// Divide uma string em n strings separadas pelo caracter 'separator'
		std::vector< std::string >& DivideStrBy( std::vector< std::string >& out, const std::string& str, const std::string& separators );
		
		// Obtém um campo, a partir de 'currCharIndex', cujo fim é determinado pelo primeiro caractere
		// de 'delimiters' encontrado. Retorna o conteúdo do campo em 'outFieldStr'. O valor de retorno da
		// função indica a partir de que índice devemos começar a procurar um novo campo
		std::string::size_type ParseField( std::string& outFieldStr, const std::string& str, const std::string& delimiters, std::string::size_type currCharIndex );
	
		// Acha um campo, a partir de 'currCharIndex', contendo os caracteres 'fieldChars' na string
		// 'str'. Retorna o conteúdo do campo em 'outFieldStr'. O valor de retorno da função indica a partir
		// de que índice devemos começar a procurar um novo campo
		std::string::size_type ParseFieldContaining( std::string& outFieldStr, const std::string& str, const std::string& fieldChars, std::string::size_type currCharIndex );
		
		// Acha um campo, a partir de 'currCharIndex', delimitado pelos caracteres 'delimiters' na string
		// 'str'. Retorna o conteúdo do campo em 'outFieldStr'. O valor de retorno da função indica a partir
		// de que índice devemos começar a procurar um novo campo
		std::string::size_type ParseFieldNotContaining( std::string& outFieldStr, const std::string& str, const std::string& delimiters, std::string::size_type currCharIndex );
		
		// Converte uma string hexadecimal para um vetor de bytes
		ByteArray& HexStrToByteVector( const std::string& hexStr, ByteArray& byteArray );
		
		// Converte uma string EBCDIC para uma string ASCII
		std::string& EBCDICStrToASCIIStr( std::string& out, const ByteArray& strEBCDIC, ByteArray::size_type start, ByteArray::size_type end );
		
		// Retorna se o arquivo especificado existe
		bool FExists( const std::string& filePath );
		
		// Retorna se o diretório especificado existe
		bool DirExists( const std::string& dirPath );
		
		// Retorna os valores de dia, mês e ano relativos a um epochTime. Em casos de erro, retorna 0 nos valores de dia, mês e ano, e 255
		// nos valores de horas, minutos e segundos. OBS: Por causa dos métodos utilizados internamente, o valor dos segundos, quando válido,
		// estará no intervalo [0,60], e não no intervalo [0,59]...
		void GetDateForEpochTime( double epochTime, uint16_t* pYear, uint8_t* pMonth, uint8_t* pDay, uint8_t* pHour = NULL, uint8_t* pMin = NULL, uint8_t* pSec = NULL );
		
		// Retorna uma string no formato yyy_mm_dd correspondente à data indicada por epochTime. Caso a conversão não possa ser feita, retorna defaultString
		std::string& GetDateStrForEpochTime( std::string& aux, double epochTime, const std::string& defaultString );
		
		// Lê todo o conteúdo do arquivo para o buffer recebido como parâmetro
		bool ReadAllFiletoMem( std::string& buffer, const std::string& filePath );
		
		// Retorna o próximo comando SQL contido na string 'sql' delimitado pelo delimitador 'currDelimiter'. Começa a procurar pelo comando a partir do
		// caractere 'startIndex'.
		// Retorna o comando SQL em 'statement', o último delimitador utilizado em 'currDelimiter' e o índice a partir do qual devemos procurar novos
		// comandos como valor de retorno da função.
		// OBS: Ignora erros de SQL.
		std::string::size_type GetStatement( std::string& statement, std::string& currDelimiter, std::string::size_type startIndex, const std::string& sql );
	}
}

#endif
