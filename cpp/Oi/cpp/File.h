/*
 *  File.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 6/11/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef FILE_H
#define FILE_H

// C++
#include <cstdio>
#include <string>

namespace Oi
{
	class File
	{
		public:
			// Construtor e destrutor
			File( void );
			~File( void );
		
			// Abre o arquivo. Retorna se a operação obteve sucesso
			bool open( const std::string& path, const std::string& openMode );
		
			// Fecha o arquivo explicitamente
			void close( void );
		
			// Converte de File para FILE*
			operator FILE*(){ return pFile; };
			
		private:
			// Handler do arquivo
			FILE* pFile;
	};
}

#endif
