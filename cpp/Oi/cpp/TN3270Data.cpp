#include "TN3270Data.h"

namespace Oi
{

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

TN3270Data::TN3270Data( void )
				  : aid( mysqlpp::null ),
					cursorX( mysqlpp::null ),
					cursorY( mysqlpp::null ),
					cursorRowOffset( mysqlpp::null ),
					cursorColumnOffset( mysqlpp::null ),
					fieldData( mysqlpp::null )
{
}
	
/*==============================================================================================

MÉTODO clear
	Limpa as estruturas do pacote.

===============================================================================================*/

void TN3270Data::clear( void )
{
	aid = mysqlpp::null;
	cursorX = mysqlpp::null;
	cursorY = mysqlpp::null;
	cursorRowOffset = mysqlpp::null;
	cursorColumnOffset = mysqlpp::null;
	fieldData = mysqlpp::null;

	// Limpa os vetores
	fieldData.data.clear();
}

} // namespace Oi
