/*
 *  Mutex.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 6/8/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef MUTEX_H
#define MUTEX_H

// Unix
#include <pthread.h>

// C++
#include <stdexcept>

namespace Oi
{
	class invalid_threadop_exception : std::runtime_error
	{
		public:
			invalid_threadop_exception( const std::string& arg );
		
			virtual ~invalid_threadop_exception( void ) throw();
	};
	
	class Mutex
	{
		public:
			// Construtor
			Mutex( void );
		
			// Destrutor
			~Mutex( void );
		
			// Locks mutex. If the mutex is already locked, the calling thread will block until the mutex becomes available.
			// Throws deadlock_exception if a deadlock would occur if the thread blocked waiting for mutex.
			void lock( void );
		
			// If the current thread holds the lock on mutex, then the unlock() function unlocks mutex.
			// Throws invalid_threadop_exception if the current thread does not hold a lock on mutex.
			void unlock( void );
		
			// The tryLock() function locks mutex. If the mutex is already locked, tryLock() will not block waiting for the
			// mutex, but will return false. Otherwise, it returns true.
			bool tryLock( void );

		private:
			// Indica se o objeto foi inicializado
			bool initialized;

			// Semáforo propriamente dito
			pthread_mutex_t semaphore;
	};

} // namespace Oi

#endif
