//#include "Thread.h"
//
///*==============================================================================================
//
//FUNÇÃO ESTÁTICA
//
//===============================================================================================*/
//
//static void* RunThread( void* pArg )
//{
//	if( pArg != NULL )
//		return static_cast< Thread* >( pArg )->run();
//
//	return NULL;
//}
//
///*==============================================================================================
//
//CONSTRUTOR
//
//===============================================================================================*/
//
//Thread::Thread ( int p )
//        : running( false ), threadNo( p )
//{
//}
//
///*==============================================================================================
//
//DESTRUTOR
//
//===============================================================================================*/
//
//Thread::~Thread( void )
//{
//	if( running )
//		cancel();
//}
//
///*==============================================================================================
//
//MÉTODO on_proc
//
//===============================================================================================*/
//
//bool Thread::on_proc( int p ) const
//{
//	return ( ( p == -1 ) || ( threadNo == -1 ) || ( p == threadNo ) );
//}
//
///*==============================================================================================
//
//MÉTODO create
//
//===============================================================================================*/
//
//Thread::create( bool detached, bool sscope )
//{
//	if( !running )
//	{
//		pthread_attr_init( &threadAttr );
//
//		if( detached )
//			pthread_attr_setdetachstate( &threadAttr, PTHREAD_CREATE_DETACHED );
//
//		if( sscope )
//			pthread_attr_setscope( & threadAttr, PTHREAD_SCOPE_SYSTEM );
//
//		pthread_create( &threadId, &threadAttr, RunThread, this );
//
//		running = true;
//	}
//	else
//	{
//		throw invalid_threadop_exception( "Thread is already running" );
//	}
//}
