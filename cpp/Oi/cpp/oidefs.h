/*
 *  oidefs.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/18/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef OI_DEFS_H
#define OI_DEFS_H

// C++
#include <stdint.h>
#include <vector>

#define PROGRAM_VERSION "1.0.0"

#define DEFAULT_DEPENDENCIES_FILEPATH "oitcpmapperconf.txt"

#define BUFFER_FILE_PATH "./parsebuffer.bin"

#define KBYTE 1024
#define MBYTE ( 1024 * KBYTE )
#define GBYTE ( 1024 * MBYTE )

#define DEFAULT_OUTPUT_BUFFER_MIN_MEM (( uint64_t )( 8 * KBYTE ))
#define DEFAULT_OUTPUT_BUFFER_MAX_MEM (( uint64_t )( 800 * MBYTE ))

namespace Oi
{

#ifndef IP_PORT
#define IP_PORT
typedef uint16_t IpPort;
#endif
	
#ifndef BYTE_ARRAY
#define BYTE_ARRAY
typedef std::vector< uint8_t > ByteArray;
#endif

} // namespace Oi

#endif
