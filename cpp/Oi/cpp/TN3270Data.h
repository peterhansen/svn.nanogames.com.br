/*
 *  TN3270Data.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 6/4/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef TN3270DATA_H
#define TN3270DATA_H

// MySQL++
#include <mysql++.h>

namespace Oi
{
	struct TN3270Data
	{
		// Construtor
		TN3270Data( void );

		// Limpa as estruturas do pacote.
		void clear( void );
		
		// Attention Identification
		mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > aid;
		
		// Posição do cursor na tela
		mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorX;
		mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorY;
		mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorRowOffset;
		mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorColumnOffset;

		// Dados do campo TN3270
		mysqlpp::Null< mysqlpp::sql_tinytext > fieldData;
	};
}

#endif
