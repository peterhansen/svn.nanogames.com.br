#include "Application.h"

// Unix
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

// Precisamos incluir este define para o compilador reconhecer 'PRIu64'
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

// C++
#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>

// Oi
#include "ConversationPacket.h"
#include "Utils.h"
#include "PacketTN3270Data.h"
#include "TcpConversation.h"
#include "TcpPacket.h"
#include "TcpConversationCapFile.h"

// MySQL++
#include <mysql++.h>

// Definições úteis
#define PIPE_READ_END_FD 0
#define PIPE_WRITE_END_FD 1

#define PIPE_READ_BUFFER_LEN ( MBYTE )

// Definições dos blocos TN3270
#define TN3270_DATA_BLOCK_AID_IND 5
#define TN3270_DATA_BLOCK_CURSORX_IND 6
#define TN3270_DATA_BLOCK_CURSORY_IND 7
#define TN3270_DATA_BLOCK_FIELD_DATA_BEGIN 8

// Possíveis retornos do método createDatabase
#define CREATE_DATABASE_RET_COULDNT_CREATE	0
#define CREATE_DATABASE_RET_DB_CREATED		1
#define CREATE_DATABASE_RET_ALREADY_EXISTS	2

// Macro para imprimir o feedback no console no modo verbose
#define VERBOSE( s, ... ) if( verboseModeOn ){ fprintf( stdout, s, ##__VA_ARGS__ ); }
#define STATIC_VERBOSE( s, ... ) if( Oi::Application::GetInstance()->isVerboseModeOn() ){ fprintf( stdout, s, ##__VA_ARGS__ ); }

namespace Oi
{

/*==============================================================================================

INICIALIZAÇÃO DAS VARIÁVEIS ESTÁTICAS DA CLASSE

===============================================================================================*/

Application* Application::pSingleton = NULL;
	
/*==============================================================================================

CONSTRUTOR

===============================================================================================*/
	
Application::Application( const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, bool appendConvs )
			: nPackets( 0 ),
			  conversations(),
			  capFilePath( capfile ),
			  dbHost( dbHost ),
			  dbUser( dbUser ),
			  dbPass( dbPassword ),
			  dbDatabase( dbDatabase ),
			  confFilePath( confFilePath ),
			  capinfosPath(),
			  tsharkPath(),
			  tsharkProfilesPath(),
			  tsharkProfileToUse(),
			  oiDBCreationFilePath(),
			  outputBuffMaxMem( outputBufferMaxMem ),
			  verboseModeOn( verbose ),
			  appendConvsOn( appendConvs )
{
}

/*==============================================================================================

MÉTODO ESTÁTICO GetInstance
	 Retorna a única instância da classe

===============================================================================================*/

Application* Application::GetInstance( void )
{
	return pSingleton;
}

/*==============================================================================================

MÉTODO ESTÁTICO Run
	 Executa a aplicação. Retorna EXIT_FAILURE em casos de erro e EXIT_SUCCESS quando bem
sucedido.

===============================================================================================*/

int32_t Application::Run( const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, bool appendConvs )
{
	if( pSingleton == NULL )
		pSingleton = new Application( capfile, dbHost, dbUser, dbPassword, dbDatabase, confFilePath, outputBufferMaxMem, verbose, appendConvs );
	return pSingleton->process();
}

/*==============================================================================================

MÉTODO checkDependencies
	 Verifica se todos os arquivos e programas dos quais dependemos estão onde deveriam estar.

===============================================================================================*/

bool Application::checkDependencies( void )
{
	// Lê o arquivo de configuração do programa
	if( !Utils::FExists( confFilePath ) )
	{
		std::cerr << "Erro: O arquivo de configuração do programa oitcpmapper não foi encontrado" << std::endl;
		return false;
	}

	std::string line;
	std::vector< std::string > dependenciesPaths;
	std::ifstream stream( confFilePath.c_str() );

	while( !getline( stream, line ).eof() )
		dependenciesPaths.push_back( line );
	
	stream.close();
	line.clear();

	try
	{
		capinfosPath = dependenciesPaths.at( 0 );
		tsharkPath = dependenciesPaths.at( 1 );
		tsharkProfilesPath = dependenciesPaths.at( 2 );
		tsharkProfileToUse = dependenciesPaths.at( 3 );
		oiDBCreationFilePath = dependenciesPaths.at( 4 );
	}
	catch( ... )
	{
		// Os erros do arquivo de configuração serão pegos abaixo
	}
	dependenciesPaths.clear();

	// Verifica se o programa capinfos existe
	if( capinfosPath.empty() )
	{
		std::cerr << "Erro: O caminho para o programa capinfos não foi especificado no arquivo de configuração do programa" << std::endl;
		return false;
	}

	if( !Utils::FExists( capinfosPath ) )
	{
		std::cerr << "Erro: O programa capinfos não foi encontrado no caminho " << capinfosPath << std::endl;
		return false;
	}
	
	// Verifica se o programa tshark existe
	if( tsharkPath.empty() )
	{
		std::cerr << "Erro: O caminho para o programa tsharkPath não foi especificado no arquivo de configuração do programa" << std::endl;
		return false;
	}

	if( !Utils::FExists( tsharkPath ) )
	{
		std::cerr << "Erro: O programa tshark não foi encontrado no caminho " << tsharkPath << std::endl;
		return false;
	}

	// Verifica se o arquivo de configuração do tshark existe
	if( tsharkProfilesPath.empty() )
	{
		std::cerr << "Erro: O caminho para o diretório de perfis do programa tshark não foi especificado no arquivo de configuração do programa" << std::endl;
		return false;
	}
	if( tsharkProfileToUse.empty() )
	{
		std::cerr << "Erro: O perfil a ser utilizado com o programa tshark não foi especificado no arquivo de configuração do programa" << std::endl;
		return false;
	}

	std::string temp( tsharkProfilesPath );
	temp += tsharkProfileToUse + "/";
	if( !Utils::DirExists( temp ) )
	{
		std::cerr << "Erro: O perfil " << tsharkProfileToUse << " do programa tshark não foi encontrado no caminho " << tsharkProfilesPath << std::endl;
		return false;
	}
	
	// Verifica se o arquivo de criação do BD existe
	if( oiDBCreationFilePath.empty() )
	{
		std::cerr << "Erro: O caminho para o arquivo de criação do BD não foi especificado no arquivo de configuração do programa" << std::endl;
		return false;
	}

	if( !Utils::FExists( oiDBCreationFilePath ) )
	{
		std::cerr << "Erro: O arquivo de criação do BD não foi encontrado no caminho " << oiDBCreationFilePath << std::endl;
		return false;
	}

	// Verifica se o arquivo .cap existe
	// OBS: Não precisamos testar se a string é vazia, pois isto já é feito na verificação dos parâmetros do programa
	if( !Utils::FExists( capFilePath ) )
	{
		std::cerr << "Erro: O arquivo .cap " << capFilePath << " não existe" << std::endl;
		return false;
	}

	return true;
}

/*==============================================================================================

MÉTODO process
	 Inicia o processamento da aplicação.Retorna EXIT_FAILURE em casos de erro e EXIT_SUCCESS
quando bem sucedido

===============================================================================================*/

int32_t Application::process( void )
{
	// Verifica se todos os arquivos e programas dos quais dependemos estão onde deveriam estar
	if( !checkDependencies() )
		return EXIT_FAILURE;

	// Obtém o número de pacotes contidos no arquivo .cap
	nPackets = readNPackets();
	if( nPackets < 0 )
	{
		std::cerr << "Erro: Não conseguiu ler o número de pacotes contidos no arquivo " << capFilePath << std::endl;
		return EXIT_FAILURE;
	}

	if( nPackets == 0 )
	{
		std::cerr << "Erro: Não há pacotes para serem lidos no arquivo " << capFilePath << std::endl;
		return EXIT_FAILURE;
	}
	
	VERBOSE( "nPackets = %zu\n\nParsing conversations\n", static_cast< size_t >( nPackets ) );

	// Obtém as conversas de rede realizadas entre os ips descritos pelos pacotes contidos no arquivo .cap
	if( !parseConversations() || conversations.empty() )
	{
		std::cerr << "Erro: Não conseguiu achar conversas TCP no arquivo " << capFilePath << std::endl;
		return EXIT_FAILURE;
	}

	VERBOSE( "nConversations =  %zu\n\nParsing packets\n", conversations.size() );
	
	if( !parsePackets() )
	{
		std::cerr << "Erro: Não pôde fazer o parse das conversas TCP contidas no arquivo " << capFilePath << std::endl;
		return EXIT_FAILURE;
	}
	
	VERBOSE( "Packets parsed\n\n" );

	if( !commitAllConversations() )
	{
		std::cerr << "Erro: Não pôde submeter as conversas TCP para " << dbHost << " utilizando o usuário " << dbUser << " e a senha " << ( dbPass.empty() ? "<vazia>" : dbPass.c_str() ) << std::endl;
		return EXIT_FAILURE;
	}

	VERBOSE( "Done\n\n" );

	// No errors
	return EXIT_SUCCESS;
}

/*==============================================================================================

MÉTODO getCapInfosPathAndArgs
	Retorna as informações necessárias para rodarmos o programa capinfos.

===============================================================================================*/
	
void Application::getCapInfosPathAndArgs( std::string* pPath, std::vector< std::string >* pArgs ) const
{
	std::string name( "capinfos" );
	
	if( pPath )
		*pPath = capinfosPath;
	
	if( pArgs )
	{
		// capinfos -c file.cap
		( *pArgs ).push_back( name );
		( *pArgs ).push_back( "-c" );
		( *pArgs ).push_back( capFilePath );
	}
}

/*==============================================================================================

MÉTODO getTSharkPathAndArgsForConvs
	Configura os parâmetros do programa tshark para obtermos as conversas existentes em um
arquivo .cap.

	- http://www.wireshark.org/docs/man-pages/tshark.html

===============================================================================================*/

// TODOO : Talvez tenhamos que restringir o número de pacotes que lemos por vez!!!
	
void Application::getTSharkPathAndArgsForConvs( std::string* pPath, std::vector< std::string >* pArgs ) const
{
	std::string name( "tshark" );
	
	if( pPath )
		*pPath = tsharkPath;
	
	if( pArgs )
	{
		// tshark -r file.cap -n -q -l -z conv,tcp
		( *pArgs ).push_back( name );
		( *pArgs ).push_back( "-r" );
		( *pArgs ).push_back( capFilePath );
		( *pArgs ).push_back( "-nql" );
		( *pArgs ).push_back( "-z" );
		( *pArgs ).push_back( "conv,tcp" );
		( *pArgs ).push_back( "-C" );
		( *pArgs ).push_back( tsharkProfileToUse );
	}
}

/*==============================================================================================

MÉTODO getTSharkPathAndArgsForPacketsInfo
	Configura os parâmetros do programa tshark para obtermos as informações dos pacotes
relacionados a uma conversa.

	- http://www.wireshark.org/docs/man-pages/tshark.html
 
	IP Fields
	- http://www.wireshark.org/docs/dfref/i/ip.html

	Telnet Fields
	- http://www.wireshark.org/docs/dfref/t/telnet.html
	- http://wiki.wireshark.org/TN3270

	TCP Fields
	- http://www.wireshark.org/docs/dfref/t/tcp.html

	Data Fields (Não está funcionando)
	- http://www.wireshark.org/docs/dfref/d/data.html

	Frame Fields
	- http://www.wireshark.org/docs/dfref/f/frame.html

	Display Fields Reference
	- http://www.wireshark.org/docs/dfref/

===============================================================================================*/

// TODOO : Talvez tenhamos que restringir o número de pacotes que lemos por vez!!!
	
void Application::getTSharkPathAndArgsForPacketsInfo( std::string* pPath, std::vector< std::string >* pArgs ) const
{
	std::string name( "tshark" );
	
	if( pPath )
		*pPath = tsharkPath;
	
	if( pArgs )
	{
		// tshark -r small.cap -T fields -e tcp.seq -e tcp.len -e tcp.flags.fin -e tcp.flags.syn -e tcp.flags.reset -e ip.src -e tcp.srcport -e ip.dst -e tcp.dstport -e frame.time_epoch -e data -C profile
		( *pArgs ).push_back( name );
		( *pArgs ).push_back( "-r" );
		( *pArgs ).push_back( capFilePath );
		( *pArgs ).push_back( "-nl" );
		( *pArgs ).push_back( "-T" );
		( *pArgs ).push_back( "fields" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.seq" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.len" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.flags.fin" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.flags.syn" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.flags.reset" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "ip.src" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.srcport" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "ip.dst" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "tcp.dstport" );
		( *pArgs ).push_back( "-e" );
		( *pArgs ).push_back( "frame.time_epoch" );
		( *pArgs ).push_back( "-e" );
		// OLD: "data.data" retorna os códigos hexa separados por :, somente "data" retorna os códigos hexa sem separadores
		//( *pArgs ).push_back( "data.data" );
		( *pArgs ).push_back( "data" );
		( *pArgs ).push_back( "-C" );
		( *pArgs ).push_back( tsharkProfileToUse );
	}
#if DEBUG
	else
	{
		std::cerr << "Processo Filho - Erro ao executar tshark para obter os pacotes das conversas: Sem argumentos" << std::endl;
	}
#endif
}

/*==============================================================================================

MÉTODO ESTÁTICO RunCapinfos
	Executa o programa capinfos. Irá retorna EXIT_FAILURE em casos de erro. Caso contrário, nunca
retorna.
	- http://www.wireshark.org/docs/man-pages/capinfos.html

===============================================================================================*/
	
int	Application::RunCapinfos( const intptr_t *pParam )
{
	std::string progPath;
	std::vector< std::string > progArgs;
	GetInstance()->getCapInfosPathAndArgs( &progPath, &progArgs );
	return RunProgram( progPath.c_str(), progArgs );
}
	
/*==============================================================================================

MÉTODO ESTÁTICO RunTSharkForConvs
	Executa o programa tshark para obter informações sobre as conversas de ip. Irá retornar
EXIT_FAILURE em casos de erro. Caso contrário, nunca retorna.

===============================================================================================*/

int	Application::RunTSharkForConvs( const intptr_t *pParam )
{
	std::string progPath;
	std::vector< std::string > progArgs;
	GetInstance()->getTSharkPathAndArgsForConvs( &progPath, &progArgs );
	return RunProgram( progPath.c_str(), progArgs );
}
	
/*==============================================================================================

MÉTODO ESTÁTICO RunTSharkForPacketsInfo
	Executa o programa tshark para obter informações sobre os pacotes de uma determinada conversa
ip. Irá retornar EXIT_FAILURE em casos de erro. Caso contrário, nunca retorna.

===============================================================================================*/

int	Application::RunTSharkForPacketsInfo( const intptr_t *pParam )
{
	std::string progPath;
	std::vector< std::string > progArgs;
	GetInstance()->getTSharkPathAndArgsForPacketsInfo( &progPath, &progArgs );
	return RunProgram( progPath.c_str(), progArgs );
}
	
/*==============================================================================================

MÉTODO ESTÁTICO RunProgram
	Executa o programa localizado em path utilizando o array de parâmetros args. O primeiro
parâmetro SEMPRE deverá ser o nome do programa. Irá retorna EXIT_FAILURE em casos de erro. Caso
contrário, nunca retorna.

===============================================================================================*/
	
int Application::RunProgram( const std::string& path, const std::vector< std::string >& args )
{
	// OBS: Esta memória nunca será desalocada se execv for bem sucedida... No entanto, como estamos
	// em um processo filho gerado a partir de um fork, sendo que este irá morrer se a chamada for
	// bem sucedida, creio que o SO se encarregará de liberar a memória. Se isto continuar sendo
	// um problema, podemos transformar pTemp em uma variável estática da classe Application
	size_t nArgs = args.size();
	const char **pTemp = new const char*[ nArgs + 1 ];
	for( size_t i = 0 ; i < nArgs ; ++i )
		pTemp[ i ] = args[i].c_str();

	pTemp[ nArgs ] = NULL;

	// OLD: A função execvP ("P" maiúsculo) não existe em todos os sistemas. Então mudamos para a função execvp ("p" minúsculo)
	//int retVal = execvP( pTemp[0], path.c_str(), static_cast< char*const* >( const_cast< char** >( pTemp ) ) );
    std::string aux = path;
    aux.append( pTemp[0] );
    int retVal = execvp( aux.c_str(), static_cast< char*const* >( const_cast< char** >( pTemp ) ) );
	
	delete[] pTemp;
	
	if( retVal == -1 )
	{
		// Possíveis erros:
		// E2BIG   
		// EACCES
		// EFAULT
		// EIO
		// ELOOP
		// ENAMETOOLONG
		// ENOENT       
		// ENOEXEC
		// ENOMEM
		// ENOTDIR
		// ETXTBSY
		std::cerr << "Não pôde executar " << args[0] << " no caminho " << path << std::endl << "\tErrno = " << errno << std::endl;

		return EXIT_FAILURE;
	}
	
	// Nunca executará esta linha
	return EXIT_SUCCESS;
}

/*==============================================================================================

MÉTODO ESTÁTICO RunAndGetOutput
	Executa uma aplicação e retorna sua saída como uma string. Caso a saída seja muito grande,
seu conteúdo será salvo em um arquivo. Neste caso, 'atFile' conterá o valor 'true' e 'output'
o nome do arquivo.

===============================================================================================*/

// TODOO : Seria melhor se utilizássemos um objeto 'std::tr1::function' genérico ao invés de utilizarmos
// um ponteiro para uma função + um argumento genérico. Assim todas as operações seriam type safe e não
// precisaríamos dos 'reinterpret_cast's

int Application::RunAndGetOutput( RunProgramFunc func, const intptr_t *pParam, const uint64_t& outputBuffMaxMem, std::string& output, bool& atFile )
{
	atFile = false;
	output.clear();

	// Cria o pipe que será nossa via de comunicação com o processo filho
	int	pipeFds[2];
	if( pipe( pipeFds ) )
	{
		std::cerr << "Não conseguiu criar o pipe" << std::endl;
		return EXIT_FAILURE;
	}
	
	// Tenta fazer um fork no processo
	pid_t childProcessId = fork();
	if( childProcessId == -1 )
	{
		std::cerr << "Não conseguiu fazer o fork" << std::endl;
		return EXIT_FAILURE;
	}

	// Um PID positivo indica que estamos no processo pai
	if( childProcessId )
	{
		// Fecha o lado do pipe que não iremos utilizar
		int retVal = close( pipeFds[ PIPE_WRITE_END_FD ] );
		if( retVal == -1 )
		{
			std::cerr << "Processo pai - Não conseguiu fechar o lado de escrita do pipe" << std::endl;
			return EXIT_FAILURE;
		}

		// Enquanto o filho não terminar, fica lendo do pipe
		pid_t temp = 0;
		uint64_t totalBytes = 0, nBytesRead;
		int childExitStatus;

		std::ofstream outputFile;
		char buffer[ PIPE_READ_BUFFER_LEN ];
		
		uint32_t printCounter = 0, processCounter = 0;

		while( temp == 0 )
		{
			temp = waitpid( childProcessId, &childExitStatus, WNOHANG );

			// Lê a informação enviada pelo pipe
			// -1 => sempre queremos poder colocar o \0 no final
			while( ( nBytesRead = read( pipeFds[ PIPE_READ_END_FD ], buffer, ( sizeof( char ) * PIPE_READ_BUFFER_LEN ) - 1 ) ) > 0 )
			{
				totalBytes += nBytesRead;
				buffer[ nBytesRead ] = '\0';
				
				// Vamos passar do limite que podemos utilizar, então salvaremos em um arquivo
				if( totalBytes > outputBuffMaxMem )
				{
					if( !atFile )
					{
						// Joga todo o conteúdo do buffer em memória para um arquivo
						atFile = true;
						
						outputFile.open( BUFFER_FILE_PATH, std::ios_base::out );
						if( !outputFile )
						{
							output.clear();
							temp = -1;
							std::cerr << "Processo Pai - Não foi possível criar o arquivo temporário " << BUFFER_FILE_PATH << std::endl;
							break;
						}
						
						// Passa todo conteúdo da memória para o arquivo
						outputFile.write( output.c_str(), output.length() );
						output.clear();
						
						// Sinaliza que o buffer está em um arquivo
						output = BUFFER_FILE_PATH;
					}
					
					// Salva os dados no arquivo
					outputFile.write( buffer, static_cast< ptrdiff_t >( nBytesRead ) );
					outputFile.flush();
				}
				else
				{
					output.append( buffer );
				}
				
				// Fornece o feedback de processamento
				++processCounter;
                if( processCounter % 100 == 0 )
                {
					// OLD: Como este método é genérico, processCounter não significaria nada de concreto. Ele funcionaria
					// apenas quando este método está recebendo os dados do pacote, pois aí sim recebemos de 1 em 1
					// STATIC_VERBOSE( "\rProcessing %u ", processCounter);
					
					processCounter = 0;

					std::string ellipsis( printCounter, '.' ); 
					// &3 == %4
					printCounter = ( printCounter + 1 ) & 3;

					// Apaga e reescreve a mensagem ciclando o número de pontos
					STATIC_VERBOSE( "\rProcessing%s", ellipsis.c_str() );
                }
			}
		}
		
		STATIC_VERBOSE( "\nIntermediate process finished\n" );
		// OLD: Como este método é genérico, processCounter não significaria nada de concreto. Ele funcionaria
		// apenas quando este método está recebendo os dados do pacote, pois aí sim recebemos de 1 em 1
		//STATIC_VERBOSE( "\nIntermediate process finished Count= %u\n", processCounter );
					   
		// OLD: Alteramos a estrutura do while acima, então não precisamos mais deste comando
//		// Lê informações que podem ter sobrado no pipe
//		// -1 => sempre queremos poder colocar o \0 no final
//		while( ( nBytesRead = read( pipeFds[ PIPE_READ_END_FD ], buffer, ( sizeof( char ) * PIPE_READ_BUFFER_LEN ) - 1 ) ) > 0 )
//		{
//			buffer[ nBytesRead ] = '\0';
//			output.append( buffer );
//		}
		
		if( atFile )
			outputFile.close();
			  
		if( temp == -1 ) 
		{
			std::cerr << "Processo Pai - Processo filho falhou" << std::endl;
			return EXIT_FAILURE;
		}
		
		// Fecha o outro lado do pipe
		retVal = close( pipeFds[ PIPE_READ_END_FD ] );
		if( retVal == -1 )
		{
			std::cerr << "Processo Pai - Não conseguiu fechar o lado de escrita do pipe" << std::endl;
			return EXIT_FAILURE;
		}

		if( !WIFEXITED( childExitStatus ) )
		{
			std::cerr << "Processo Pai - Processo filho não obteve sucesso" << std::endl;

			if( WIFSIGNALED( childExitStatus ) )
			{
				// OBS: Se colocarmos breakpoints nos processos rodados pelos filhos, receberemos SIGSEGV ou SIGILL randomicamente...
				std::cerr << "Processo Pai - Processo filho terminou devido ao recebimento de um sinal (número = " << WTERMSIG( childExitStatus ) << "). Gerou um arquivo dump? " << ( WCOREDUMP( childExitStatus ) ? "sim" : "não" ) << std::endl;
			}
			// Não iremos precisar deste else:
			// This macro can be true only if the wait call specified the WUNTRACED option or if the child process is being traced (see ptrace(2))
//			else if( WIFSTOPPED( childExitStatus ) )
//			{
//			}			
			
			return EXIT_FAILURE;
		}
		
		return EXIT_SUCCESS;
	}
	// Um PID igual a zero indica que estamos no processo filho
	else
	{			
		// Fecha o lado do pipe que não iremos utilizar
		int retVal = close( pipeFds[ PIPE_READ_END_FD ] );	
		if( retVal == -1 )
		{
			std::cerr << "Não conseguiu fechar o lado de leitura do pipe" << std::endl;
			return EXIT_FAILURE;
		}
		
		// Substitui stdout com o lado de escrita do pipe
		retVal = dup2( pipeFds[ PIPE_WRITE_END_FD ], STDOUT_FILENO );
		if( retVal == -1 )
		{
			std::cerr << "Não conseguiu substituir o lado de escrita do pipe" << std::endl;
			return EXIT_FAILURE;
		}

		// Substitui este processo por um processo que irá executar o programa capinfos
		// Só irá retornar em casos de erro
		if( func( pParam ) == EXIT_FAILURE )
		{
			std::cerr << "Filho - Erro ao executar o programa" << std::endl;
			return EXIT_FAILURE;
		}
	}
	// Nunca vai chegar aqui, mas fazendo esta declaração impedimos certos warnings
	return EXIT_FAILURE;
}

/*==============================================================================================

MÉTODO readNPackets
	Retorna o número de pacotes contidos no arquivo .cap ou -1 em casos de erro.

===============================================================================================*/
	
int64_t Application::readNPackets( void ) const
{
	bool atFile;
	std::string output;
	if( RunAndGetOutput( RunCapinfos, NULL, outputBuffMaxMem, output, atFile ) == EXIT_SUCCESS )
	{
		// OBS: O output deste programa sempre irá caber em DEFAULT_OUTPUT_BUFFER_MIN_MEM, então não
		// precisamos checar o valor de atFile
		
		// Procura a informação desejada
		std::string strToFind( "Number of packets:" );
		size_t index = output.find( strToFind );
		if( index == std::string::npos )
			return -1;

		// Retorna o número de pacotes
		std::string nPacketsStr = output.substr( index + strToFind.size() );
		return atoll( nPacketsStr.c_str() );
	}
	return -1;
}

/*==============================================================================================

MÉTODO parseConversations
	Obtém as conversas de rede realizadas entre os ips descritos pelos pacotes contidos no
arquivo .cap.

===============================================================================================*/
	
bool Application::parseConversations( void )
{
	bool atFile;
	std::string output;
	if( RunAndGetOutput( RunTSharkForConvs, NULL, outputBuffMaxMem, output, atFile ) == EXIT_SUCCESS )
	{
		// Cria uma stream a partir do buffer de output
		std::ifstream filestream;
		std::istringstream memstream;
		std::istream* pStream;
		if( !atFile )
		{
			memstream.str( output );
			output.clear();
			
			pStream = &memstream;
		}
		else
		{
			filestream.open( output.c_str(), std::ios_base::in );
			if( !filestream )
			{
				std::cerr << "Não pode abrir o arquivo " << output.c_str() << ". Abortando." << std::endl;
				return false;
			}
			
			pStream = &filestream;
		}

		// Pula as linhas de formatação de output (5)
		int64_t nLinesToSkip = 5;

		std::string line;
		while( --nLinesToSkip >= 0 )
			std::getline( *pStream, line );

		// Armazena a descrição das conversas
		std::string numbers = "0123456789";
		std::string ipChars = numbers;
		ipChars.append( ".:" );

		std::string fieldStr;
		std::string::size_type currCharIndex;

		Ipv4Address client, host;
		uint64_t nFramesFromClientToHost, nBytesFromClientToHost, nFramesFromHostToClient, nBytesFromHostToClient;
		
		while( !std::getline( *pStream, line ).eof() )
		{
			#if DEBUG
				// Na versão release, esta impressão consumiria muito tempo
				VERBOSE( "%s\n", line.c_str() );
			#endif

			// A linha que determina o fim da tabela é repleta de caracteres '=' (além de ser a única que contém esse caractere)
			if( line[0] != '=' )
			{
				// Faz o parse da linha da tabela
				currCharIndex = Utils::ParseFieldContaining( fieldStr, line, ipChars, 0 );
				client.set( fieldStr );

				currCharIndex = Utils::ParseFieldContaining( fieldStr, line, ipChars, currCharIndex );
				host.set( fieldStr );

				currCharIndex = Utils::ParseFieldContaining( fieldStr, line, numbers, currCharIndex );
				nFramesFromHostToClient = atoll( fieldStr.c_str() );
				
				currCharIndex = Utils::ParseFieldContaining( fieldStr, line, numbers, currCharIndex );
				nBytesFromHostToClient = atoll( fieldStr.c_str() );
				
				currCharIndex = Utils::ParseFieldContaining( fieldStr, line, numbers, currCharIndex );
				nFramesFromClientToHost = atoll( fieldStr.c_str() );
				
				currCharIndex = Utils::ParseFieldContaining( fieldStr, line, numbers, currCharIndex );
				nBytesFromClientToHost = atoll( fieldStr.c_str() );

				NetworkConversation netConv( client, host, nFramesFromClientToHost, nBytesFromClientToHost, nFramesFromHostToClient, nBytesFromHostToClient, capFilePath );
				conversations.push_back( netConv );
			}
			else
			{
				break;
			}
		}
		
		if( atFile )
			remove( output.c_str() );

		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO parsePackets
	Obtém as informações dos pacotes de todas as conversas contidas no arquivo.

===============================================================================================*/
	
bool Application::parsePackets( void )
{
	bool atFile;
	std::string output;
	if( RunAndGetOutput( RunTSharkForPacketsInfo, NULL, outputBuffMaxMem, output, atFile ) == EXIT_SUCCESS )
	{
		// Cria uma stream a partir do buffer de output
		std::ifstream filestream;
		std::istringstream memstream;
		std::istream* pStream;
		if( !atFile )
		{
			memstream.str( output );
			output.clear();
			
			pStream = &memstream;
		}
		else
		{
			filestream.open( output.c_str(), std::ios_base::in );
			if( !filestream )
			{
				std::cerr << "Não pode abrir o arquivo " << output.c_str() << ". Abortando." << std::endl;
				return false;
			}
			
			pStream = &filestream;
		}

		// Armazena as descrições dos pacotes da conversa
		std::string terminalType, terminalName, line, fieldStr, temp, delimiters = "\t";
		std::string::size_type currCharIndex;

		uint64_t packetSeqNum = 0;
		ConversationPacket packet;

		while( !getline( *pStream, line ).eof() )
		{
			#if DEBUG
				// Na versão release, esta impressão consumiria muito tempo
				VERBOSE( "Packet:\n%s\n\n", line.c_str() );
			#endif
			
			// Incrementa o número único do pacote na conversa
			packet.packetSeqNum = packetSeqNum++;
			
			// Faz o parse do output do pacote

			// TCP Sequence
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, 0 );
			packet.tcpSeq =  atoi( fieldStr.c_str() );
			
			// TCP Length
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			packet.tcpLength =  atoi( fieldStr.c_str() );
			
			// TCP Flags Fin
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			packet.tcpFlags.fin = fieldStr[0] == '0' ? 0 : 1;

			// TCP Flags Syn
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			packet.tcpFlags.syn = fieldStr[0] == '0' ? 0 : 1;

			// TCP Flags Reset
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			packet.tcpFlags.reset = fieldStr[0] == '0' ? 0 : 1;

			// IP Src + Src Port
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			temp.swap( fieldStr );
			temp.append( ":" );
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			temp.append( fieldStr );
			Ipv4Address ipSrc( temp );

			// Ip Dst + Dst Port
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			temp.swap( fieldStr );
			temp.append( ":" );
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			temp.append( fieldStr );
			Ipv4Address ipDst( temp );
			temp.clear();

			// Packet Epoch Time
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			packet.epochTime = atof( fieldStr.c_str() );

			// Tcp Data
			currCharIndex = Utils::ParseField( fieldStr, line, delimiters, currCharIndex );
			if( currCharIndex != std::string::npos )
			{			
				temp.swap( fieldStr );
				
				// OLD: O filtro "data.data" retorna os códigos hexa separados por ':', somente "data" retorna os códigos hexa sem separadores
//				std::string::size_type found = 0;
//				while( ( found = temp.find_first_of( ":", found ) ) != std::string::npos )
//					temp.erase( found, 1 );

				Utils::HexStrToByteVector( temp, packet.tcpData );
				temp.clear();
			}

			// Verifica a que conversa este pacote pertence
			ConversationsContainer::iterator itPacketConversation = std::find( conversations.begin(), conversations.end(), NetworkConversation( ipSrc, ipDst ) );
			if( itPacketConversation != conversations.end() )
			{
				packet.direction = ( ipSrc == itPacketConversation->ipClient ? CLIENT_TO_HOST : HOST_TO_CLIENT );
				parseTN3270Data( packet.tn3270DataBlocks, terminalType, terminalName, packet.tcpData, packet.direction );

				if( !itPacketConversation->hasTerminalTypeAndName() && ( !terminalName.empty() || !terminalType.empty() ) )
				{
					itPacketConversation->setTerminal( terminalType, terminalName );
					terminalType.clear();
					terminalName.clear();
				}
				itPacketConversation->packets.push_back( packet );
			}
			// Não deveria acontecer, pois puxamos anteriormente uma lista de todas as conversas existentes no .cap
//			else
//			{
//			}

			packet.clear();
		}
		
		if( atFile )
			remove( output.c_str() );
		
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO parseTN3270Data
	Obtém os blocos de dados TN3270 a partir dos dados contidos no array bytes passado como
parâmetro.  Se existir a informação, retorna também o tipo de terminal TN3270 utilizado na
conversa em 'terminalType'

===============================================================================================*/

ConversationPacket::TN3270DataBlocksContainer& Application::parseTN3270Data( ConversationPacket::TN3270DataBlocksContainer& out, std::string& terminalType, std::string& terminalName, const ByteArray& tcpData, ConversationPacketDir direction )
{
	try
	{
		TN3270Data dataBlock;
		mysqlpp::sql_tinytext temp;

		ByteArray::size_type nBytes = tcpData.size();
		for( ByteArray::size_type i = 0 ; i < nBytes ; ++i )
		{
			// Verifica se estamos lendo um pacote de dados ou um pacote especial
			if( direction == CLIENT_TO_HOST )
			{
				if( tcpData.at( i ) == 0x00 )
				{
					uint8_t aid = tcpData.at( TN3270_DATA_BLOCK_AID_IND );
					if( isValidAID( aid ) )
					{
						dataBlock.aid = aid;

						dataBlock.cursorX = tcpData.at( TN3270_DATA_BLOCK_CURSORX_IND );
						dataBlock.cursorY = tcpData.at( TN3270_DATA_BLOCK_CURSORY_IND );
						
						out.push_back( dataBlock );
						dataBlock.clear();

						ByteArray::size_type bytesToRead = tcpData.size();
						for( ByteArray::size_type currByte = TN3270_DATA_BLOCK_FIELD_DATA_BEGIN ; currByte < bytesToRead ; )
						{
							if( ( tcpData.at( currByte ) == 0x11 ) )
							{
								++currByte;
								dataBlock.cursorX = tcpData.at( currByte++ );
								dataBlock.cursorY = tcpData.at( currByte++ );

								ByteArray::size_type fieldEnd;
								for( fieldEnd = currByte ; fieldEnd < bytesToRead ; ++fieldEnd )
								{
									if( ( tcpData.at( fieldEnd ) == 0x11 ) || ( tcpData.at( fieldEnd ) == 0xFF && tcpData.at( fieldEnd + 1 ) == 0xEF ) )
										break;
								}

								// Não podemos passar 'dataBlock.fieldData.data' como parâmetro de Utils::EBCDICStrToASCIIStr(), pois assim o valor de 'is_null'
								// ficaria incorreto (poderíamos consertar isso alterando o código da classe NULL da biblioteca MySQL++). Como workaround, utilizamos
								// uma variável temporária
								Utils::EBCDICStrToASCIIStr( temp, tcpData, currByte, fieldEnd );
								dataBlock.fieldData = temp;

								out.push_back( dataBlock );
								dataBlock.clear();

								currByte = fieldEnd;
							}
							else
							{
							   ++currByte;
							}
						}
					}
				}
			}
			else if( /*( direction == HOST_TO_CLIENT ) &&*/ tcpData.at( i ) == 0xFF && tcpData.at( i + 1 ) == 0xFA )
			{
				// - http://www.faqs.org/rfcs/rfc1647.html
				//      Only the server may send this command.  This command is used to accept a client's DEVICE-TYPE REQUEST
				// command and to return the server-defined device-name:
				//
				//		0xFF	0xFA			0x02		0x04					0x01					0xFF	0xF0
				//		IAC		SB		TN3270E DEVICE-TYPE IS		<device-type>	CONNECT <device-name>	IAC		SE

				// Procura pelo campo que inicia o nome do terminal
				ByteArray::size_type j = i + 2;
				ByteArray::const_iterator begin = tcpData.begin();
				ByteArray::const_iterator end = tcpData.end();
				
				// TN3270
				if( tcpData.at( j ) == 0x18 )
				{
					// Pula "my terminal type is" == 0x00
					std::advance( begin, j + 2 );
					
					ByteArray::const_iterator found = std::find( begin, end, 0xFF );
					if( found != end )
						terminalType.assign( begin, found );
				}
				// TN3270-E
				else if( tcpData.at( j ) == 0x28 )
				{
					// Pula 'DEVICE-TYPE' e 'IS'
					std::advance( begin, j + 3 );

					ByteArray::const_iterator found = std::find( begin, end, 0x01 );
					if( found != end )
					{
						terminalType.assign( begin, found );
						
						begin = found;
						std::advance( begin, 1 );
						
						found = std::find( begin, end, 0xFF );
						if( found != end )
							terminalName.assign( begin, found );
					}
					else
					{
						found = std::find( begin, end, 0xFF );
						if( found != end )
							terminalType.assign( begin, found );
					}
				}
				// Opa, não deveria acontecer
				else
				{
					// Não faz nada
				}
			}
		}
	}
	catch( const std::out_of_range& ex )
	{
		// Não faz nada. Armazenaremos apenas os dados já contidos no vetor de saída
	}
	catch( ... )
	{
		// Só para garantir que não teremos problemas
	}
	return out;
}

/*==============================================================================================

MÉTODO isValidAID
	Retorna se o Attention ID (AID) do protocolo TN3270 é um dos que estamos observando.
	A lista é:
		- Enter
		- Clear
		- Sysreq
 		- PF1 a PF24
		- PA1 a PA3

===============================================================================================*/
	
bool Application::isValidAID( uint8_t aid )
{
	return ( aid >= 0xF0 && aid <= 0xF9 ) || ( aid >= 0x7A && aid <= 0x7D ) || ( aid >= 0xC1 && aid <= 0xC9 ) || ( aid >= 0x4A && aid <= 0x4C ) || ( aid >= 0x6A && aid <= 0x6D );
}

/*==============================================================================================

MÉTODO divideConvsByDate
	Retorna as conversas separadas por bases de dados em função de suas datas de início.

===============================================================================================*/

void Application::divideConvsByDate( DatabaseMap& databases )
{
	// Se o programa não recebeu um nome de base explícitamente, cria um no formato yyyy_mm_dd, relacionado
	// às datas de início das conversas parseadas
	if( dbDatabase.empty() )
	{
		std::string aux;
		ConversationsContainer::const_iterator end = conversations.end();	
		for( ConversationsContainer::iterator itCurrConv = conversations.begin() ; itCurrConv != end ; ++itCurrConv )
		{
			DatabaseMap::iterator itDb = databases.find( Utils::GetDateStrForEpochTime( aux, itCurrConv->getEpochBeginTime(), "xxxx_xx_xx" ) );
			if( itDb == databases.end() )
				itDb = databases.insert( databases.end(), make_pair( aux, DatabaseMap::mapped_type() ) );

			itDb->second.insert( itDb->second.end(), itCurrConv );

			aux.clear();
		}
	}
	else
	{
		// Todas as conversas pertencem a uma única base de dados
		DatabaseMap::mapped_type temp;
		ConversationsContainer::const_iterator endConvs = conversations.end();
		for( ConversationsContainer::iterator it = conversations.begin() ; it != endConvs ; ++it )
			temp.insert( temp.end(), it );

		databases.insert( make_pair( dbDatabase, temp ) );
	}
}

/*==============================================================================================

MÉTODO commitAllConversations
	Submete todas as conversas para o BD.

===============================================================================================*/

bool Application::commitAllConversations( void )
{
	VERBOSE( "Buffering databases names:\n" );
	
	// Se o programa não recebeu um nome de base explícitamente, cria um no formato yyyy_mm_dd, relacionado
	// às datas de início das conversas parseadas
	DatabaseMap databases;
	divideConvsByDate( databases );
	
	VERBOSE( "Commiting conversations\n\n" );

	DatabaseMap::iterator endDatabases = databases.end();
	for( DatabaseMap::iterator itCurrDatabase = databases.begin() ; itCurrDatabase != endDatabases ; ++itCurrDatabase )
	{
		VERBOSE( "Creating database %s\n", itCurrDatabase->first.c_str() );
		
		int8_t ret = execQuery( std::tr1::bind( &Application::createDatabase, this, itCurrDatabase->first ) );
		if( ret <= 0 )
			return false;
		
		bool databaseWasAlreadyCreated = ( ret == CREATE_DATABASE_RET_ALREADY_EXISTS );

		VERBOSE( "Commiting conversations for this database\n\n" );

		// Percorre as conversas da base de dados
		// OBS: Estamos percorrendo um vetor de iteradores, logo teremos iteradores apontando para os iteradores finais. Daí as construções
		// não-convencionais "*( *itToConvIt )" e "( *itToConvIt )->"
		DatabaseMap::mapped_type::iterator endConvs = itCurrDatabase->second.end();
		for( DatabaseMap::mapped_type::iterator itToConvIt = itCurrDatabase->second.begin() ; itToConvIt != endConvs ; ++itToConvIt )
		{
			if( execQuery( std::tr1::bind( &Application::commitConversation, this, itCurrDatabase->first, *( *itToConvIt ) ) ) > 0 )
			{
				// Libera a memória ocupada pelos pacotes da conversa, pois conseguimos subir tudo para o BD
				( *itToConvIt )->packets.clear();
			}
			else
			{
				if( !databaseWasAlreadyCreated )
				{
					// Operações feitas por esta conexão não lançarão exceções
					mysqlpp::Connection conn( false );
					conn.connect( NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str() );
					conn.drop_db( itCurrDatabase->first );
				}
				return false;
			}
		}
	}
	return true;
}

/*==============================================================================================

MÉTODO execQuery
	Executa um comando no banco de dados. Retorna < 0 em casos de exceção, 0 em casos de falha e
> 0 em casos de sucesso.

===============================================================================================*/

int8_t Application::execQuery( std::tr1::function< int8_t() > functor )
{
	int8_t ret = -1;
	try
	{
		ret = functor();
	}
	catch( const mysqlpp::ConnectionFailed& ex )
	{
		std::cerr << "A conexão MySQL falhou: " << ex.what() << std::endl; 
	}
    catch( const mysqlpp::BadQuery& ex )
	{ 
        std::cerr << "Erro de query: " << ex.what() << std::endl; 
    } 
    catch( const mysqlpp::BadConversion& ex )
	{
        std::cerr << "Erro de conversão: " << ex.what() << std::endl << "\tTamanho dos dados retornados: " << ex.retrieved <<  ", tamanho esperado: " << ex.actual_size << std::endl;
    } 
    catch( const mysqlpp::Exception& ex )
	{ 
        std::cerr << "Erro: " << ex.what() << std::endl;
    }
	catch( const std::exception& ex )
	{
		// Segundo a documentação da versão 3.0.9, há uma remota chance de código mysql++ disparar uma exceção
		// que não seja uma mysqlpp::Exception. Logo, por via das dúvidas, resolvi colocar este catch aqui
		std::cerr << ">>> EXCEÇÃO => Exceção não-MySQL++ capturada em código MySQL++: " << ex.what() << std::endl;
	}
	catch( ... )
	{
		// Segundo a documentação da versão 3.0.9, há uma remota chance de código mysql++ disparar uma exceção
		// que não seja uma mysqlpp::Exception. Logo, por via das dúvidas, resolvi colocar este catch aqui
		std::cerr << ">>> EXCEÇÃO => Objeto inesperado capturado em execQuery" << std::endl;
	}
	return ret;
}

/*==============================================================================================

MÉTODO commitConversation
	Submete os dados de uma conversa tcp e de seus pacotes para o BD.

===============================================================================================*/

int8_t Application::commitConversation( const std::string& database, const NetworkConversation& conv ) const
{
	std::string ipClient, ipHost, terminalType, terminalName;
	
	conv.ipClient.toString( ipClient, NULL );
	conv.ipHost.toString( ipHost, NULL );

	// Pela documentação, o valor dos segundos estará no intervalo [0,60], e não no intervalo [0,59]
	// Isto poderia gerar várias dores de cabeça, pois:
	//		1) Caso sec == 60, poderíamos colocar sec = 0 e somar 1 ao valor dos minutos. Mas..
	//		2) Caso min == 60, teríamos que somar 1 ao valor de horas. Mas...
	//		3) Assim por diante, até somarmos 1 ao valor do ano, no pior caso. Porém teríamos que
	// checar se o ano é bisexto e etc.
	// Logo, vale mais a pena perder 1 segundo, até porque o valor correto ficará gravado no pacote.
	// Também não utilizamos o construtor que recebe um time_t como parâmetro, porque mysqlpp::DateTime
	// aplica as modificações do horário local, e nós queremos o horário universal (UTC)
	uint16_t year;
	uint8_t sec, min, hour, day, month;
	Utils::GetDateForEpochTime( conv.getEpochBeginTime(), &year, &month, &day, &hour, &min, &sec );
	mysqlpp::DateTime beginTime( year, month, day, hour, min, sec == 60 ? 59 : sec );
	
	Utils::GetDateForEpochTime( conv.getEpochEndTime(), &year, &month, &day, &hour, &min, &sec );
	mysqlpp::DateTime endTime( year, month, day, hour, min, sec == 60 ? 59 : sec );
	
	conv.getTerminal( terminalType, terminalName );
	
	// Podemos inserir sempre 0 no ID, já que este será gerado automaticamente
	TcpConversations dbConversation( 0, ipClient, conv.ipClient.ipPort, ipHost, conv.ipHost.ipPort, conv.framesFromClientToHost,
									conv.bytesFromClientToHost, conv.framesFromHostToClient, conv.bytesFromHostToClient,
									terminalType, terminalName, beginTime, endTime );
	
	// Cria a conexão com a base de dados
	mysqlpp::Connection conn( database.c_str(), dbHost.c_str(), dbUser.c_str(), dbPass.c_str() );
	mysqlpp::Query query = conn.query();
	
	// Verifica se a conversa já existe no banco de dados
	// TODO : Fazer esta query num array, e não no banco de dados (no método commitAllConversations)
	bool convExists = false;
	uint64_t packetSeqOffset = 0;
	mysqlpp::sql_bigint_unsigned convExistingId = 0;
	if( appendConvsOn )
	{
		query << "SELECT IdConversation FROM TcpConversations WHERE ClientIp = " << mysqlpp::quote << ipClient << " AND ClientPort = " << conv.ipClient.ipPort << " AND HostIp = " << mysqlpp::quote << ipHost << " AND HostPort = " << conv.ipHost.ipPort << " LIMIT 1";
		mysqlpp::StoreQueryResult res = query.store();

		convExists = ( res.num_rows() >= 1 );
		if( convExists )
		{
			convExistingId = res[0][ "IdConversation" ];
			VERBOSE( "\rConversation [%s:%u - %s:%u](ID = %"PRIu64") already exists. Appending. ", ipClient.c_str(), conv.ipClient.ipPort, ipHost.c_str(), conv.ipHost.ipPort, convExistingId );
			
			query.reset();
			query << "SELECT MAX( PacketSeqNum ) AS MaxSeqNum FROM TcpPackets WHERE IdConversation = " << convExistingId;
			res = query.store();
			if( res. num_rows() > 0 )
			{
				packetSeqOffset = res[0][ "MaxSeqNum" ] + 1;
				VERBOSE( "Next packet sequence number = %"PRIu64" ", packetSeqOffset );
			}
		}
	}

	// Insere todos os pacotes relativos à conversa
	if( conv.packets.empty() )
	{
		if( !appendConvsOn || !convExists )
		{
			mysqlpp::Transaction trans( conn );
			query.insert( dbConversation ); 
			query.execute();
			trans.commit();
		}
	}
	else
	{
		mysqlpp::String dummy;
		TcpPackets dbTcpPacket;
		PacketsTN3270Data dbTN3270Block;
		NetworkConversation::PacketsContainer::const_iterator packetsEnd = conv.packets.end(), currPacket = conv.packets.begin();

		while( currPacket != packetsEnd )
		{
			mysqlpp::Transaction trans( conn );
			
			// Submete a conversa
			mysqlpp::sql_bigint_unsigned idConversation;
			if( !appendConvsOn || !convExists )
			{	
				query.insert( dbConversation ); 
				query.execute();
				
				// Obtém o id da conversa criada no banco
				// OBS: Se insert_id() não funcionar, podemos chamar a função LAST_INSERT_ID() SQL na mão
				idConversation = query.insert_id();
			}
			else
			{
				idConversation = convExistingId;
			}

			for( uint64_t packetSeqNum = 0; currPacket != packetsEnd ; ++currPacket, ++packetSeqNum )
			{
				// Verifica se o tshark não se confundiu, juntando várias conversas consecutivas entre a mesma origem e o
				// mesmo destino em uma única conversa
				if( ( packetSeqNum != 0 ) && ( currPacket->tcpFlags.syn == 1 ) && ( currPacket->direction == CLIENT_TO_HOST ) )
				{
					packetSeqOffset = 0;
					convExists = false;
					break;
				}

				// Podemos inserir sempre 0 no ID, já que este será gerado automaticamente
				dbTcpPacket = TcpPackets( 0, idConversation, packetSeqNum + packetSeqOffset, currPacket->tcpSeq, currPacket->tcpLength,
										  currPacket->tcpFlags.fin,
										  currPacket->tcpFlags.syn,
										  currPacket->tcpFlags.reset,
										  currPacket->epochTime,
										  currPacket->direction,
										  ( currPacket->tcpData.empty() ? dummy : mysqlpp::String( reinterpret_cast< const char* >( &( currPacket->tcpData[0] ) ), currPacket->tcpData.size() ) ) );
				
				// Submete o pacote
				query.insert( dbTcpPacket ); 
				query.execute();
				
				// Obtém o id do pacote criado no banco
				// OBS: Se insert_id() não funcionar, podemos chamar a função LAST_INSERT_ID() SQL na mão
				mysqlpp::sql_bigint_unsigned idPacket = query.insert_id();

				// Blocos de informações TN3270 contidos no pacote
				ConversationPacket::TN3270DataBlocksContainer::const_iterator end = currPacket->tn3270DataBlocks.end();
				for( ConversationPacket::TN3270DataBlocksContainer::const_iterator it = currPacket->tn3270DataBlocks.begin() ; it != end ; ++it )
				{
					// Podemos inserir sempre 0 no ID, já que este será gerado automaticamente
					dbTN3270Block = PacketsTN3270Data( 0, idPacket, it->aid, it->cursorX, it->cursorY, it->cursorRowOffset, it->cursorColumnOffset, it->fieldData );
					query.insert( dbTN3270Block ); 
					query.execute();
				}
			}

			trans.commit();
		}
	}
	return 1;
}

/*==============================================================================================

MÉTODO databaseExists
	Verifica se a base de dados existe no BD com o qual estamos realizando conexões.

===============================================================================================*/
	
bool Application::databaseExists( const std::string& databaseName ) const
{
	// Cria a conexão com a base de dados
	mysqlpp::Connection conn( NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str() );
	mysqlpp::Query query = conn.query();
	query << "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = " << mysqlpp::quote_only << databaseName;
	query.store();
	return query.affected_rows() == 1;
}

/*==============================================================================================

MÉTODO createDatabase
	Cria uma nova base de dados a partir da base modelo. Caso a base já exista, não faz nada.

===============================================================================================*/

int8_t Application::createDatabase( const std::string& databaseName ) const
{
	// Verifica se a base de dados já existe
	if( databaseExists( databaseName ) )
		return CREATE_DATABASE_RET_ALREADY_EXISTS;

	// Verifica se o arquivo de criação do banco existe e lê seu conteúdo
	std::string sql;
	if( !Utils::FExists( oiDBCreationFilePath ) || !Utils::ReadAllFiletoMem( sql, oiDBCreationFilePath ) )
	{
		std::cerr << "Erro: O arquivo que contém as queries para a criação da base de dados não foi encontrado" << std::endl;
		return CREATE_DATABASE_RET_COULDNT_CREATE;
	}

	// Cria a conexão com o BD
	bool databaseCreated = false;
	try
	{
		mysqlpp::Connection conn;
		conn.set_option( new mysqlpp::MultiResultsOption( true ) );
		conn.set_option( new mysqlpp::MultiStatementsOption( true ) );
		if( !conn.connect( NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str() ) )
			throw mysqlpp::ConnectionFailed( "Connection failed" );

		mysqlpp::Transaction trans( conn );

		// Cria a base de dados
		if( !conn.create_db( databaseName ) )
		{
			std::cerr << "Erro: Não foi possível criar a base de dados " << databaseName << " para esta importação" << std::endl;
			return CREATE_DATABASE_RET_COULDNT_CREATE;
		}
		
		// A partir deste ponto não retornaremos mais false, mas lançaremos uma exceção
		databaseCreated = true;
		
		if( !conn.select_db( databaseName ) )
		{
			std::string aux( "Could not select database " );
			aux += databaseName;
			throw mysqlpp::DBSelectionFailed( aux.c_str() );
		}

		mysqlpp::Query query = conn.query();
		
		// OBS: Não estamos conseguindo realizar múltiplos comandos de uma só vez com o MySQL++...
//		query << sql;
//		mysqlpp::StoreQueryResult res = query.store(); 
//		while( query.more_results() )
//		{
//			res = query.store_next();
//			if( !res )
//				throw std::runtime_error( "Could not execute multiple queries" );
//		}

		// Workaround por não conseguirmos realizar múltiplos comandos de uma só vez com o MySQL++
		std::string currDelimiter( ";" ), statement;
		std::string::size_type startIndex = 0;
		while( startIndex != std::string::npos )
		{
			startIndex = Utils::GetStatement( statement, currDelimiter, startIndex, sql );
			if( !statement.empty() )
			{
				query.reset();
				query << statement;
				mysqlpp::SimpleResult res = query.execute();
				if( !res )
					throw std::runtime_error( "Could not execute multiple queries" );
				
				statement.clear();
			}
		}

		// Tudo OK
		trans.commit();
	}
	catch( ... )
	{
		// Por algum motivo estranho o RAII de transaction não faz rollback em CREATE DATABASE, então:
		// Faz o drop da base de dados criada. Não checa o resultado, pois se não conseguirmos executar o comando,
		// não há mais o que fazer. O usuário terá que fazer o drop na mão
		if( databaseCreated )
		{
			// Operações feitas por esta conexão não lançarão exceções
			mysqlpp::Connection conn( false );
			conn.connect( NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str() );
			conn.drop_db( databaseName );
		}
		
		// Repassa a exceção para um tratador mais externo
		throw;
	}
	return CREATE_DATABASE_RET_DB_CREATED;
}

} // namespace Oi
