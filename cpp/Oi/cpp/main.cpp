/*
 *  main.cpp
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/10/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

// C++
#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>

// Precisamos incluir este define para o compilador reconhecer 'PRIu64'
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

// App
#include "oidefs.h"
#include "Application.h"

// Unix
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdint.h>

/*==============================================================================================

FUNÇÃO myTerminate()
	Terminate handler function. A terminate handler function is a function automatically called
when the exception handling process has to be abandoned for some reason. This happens when a
handler cannot be found for a thrown exception, or for some other exceptional circumstance that
makes impossible to continue the handling process.

===============================================================================================*/

void myTerminate( void )
{
	std::cerr << "Chamou std::terminate(). Abortando aplicação." << std::endl;

	// Terminar com exit() não dispara SIGABRT. Isso não deixa explícito que o programa terminou com erro
	// (apesar de passarmos EXIT_FAILURE como parâmetro e fazermos o log acima). Por isso manteremos abort()
	//exit( EXIT_FAILURE );
	abort();
}

/*==============================================================================================

FUNÇÃO myUnexpected()
	Unexpected handler function. An unexpected handler function is a function automatically called
when a function throws an exception that is not in its exception-specification (i.e., in its throw
specifier). It may also be called directly by the program. The unexpected handler function can
handle the exception and shall end either by teminating (calling terminate or cstdlib's exit or abort)
or by throwing an exception (even rethrowing the same exception again). If the exception thrown
(or rethrown) is not in the function's exception-specification but bad_exception is,
bad_exception is thrown. Otherwise, if the new exception is not in the exception-specification either,
terminate is automatically called. The unexpected handler by default calls terminate.

===============================================================================================*/

void myUnexpected( void )
{
	std::cerr << ">>> EXCEÇÃO => Uma exceção inesperada de C++ foi lançada. Terminando aplicação." << std::endl;
	std::terminate();
}

/*==============================================================================================

FUNÇÃO printHelp
	 Imprime a ajuda do programa no console.

===============================================================================================*/

void printHelp( const char* pProgramPath )
{
	std::string programName( pProgramPath );
	std::string::size_type index = programName.find_last_of( '/' );
	if( index != std::string::npos )
		programName.erase( 0, index + 1 );

	fprintf( stdout, "\n\n-----------------------------------------------------------\n\n" );
	fprintf( stdout, "Oi TCP Conversation Mapper - por Workers Informática©\n" );
	fprintf( stdout, "Versão %s\n\n", PROGRAM_VERSION );
	fprintf( stdout, "O programa deve ser utilizado da seguinte forma:\n\n" );
	fprintf( stdout, "%s -i <capfile> -h <dbhost> -u <dbuser> [-p <dbpassword>] [-d <dbdatabase>] [-c <arquivo de configuração>] [-v]\n\n", programName.c_str() );
	fprintf( stdout, "-i <capfile> - Arquivo .cap/.pcap a ser mapeado e exportado para o banco de dados\n\n" );
	fprintf( stdout, "-h <dbhost> - Endereço do banco de dados\n\n" );
	fprintf( stdout, "-u <dbuser> - Usuário utilizado para se conectar ao banco de dados\n\n" );
	fprintf( stdout, "-p <dbpassword> - Senha do usuário utilizado para se conectar ao banco de dados. Se não for especificada, utiliza-se uma senha vazia\n\n" );
	fprintf( stdout, "-d <dbdatabase> - Nome da base na qual os dados do arquivo .cap serão importados. Se não for especificado, o nome da base será a data dos pacotes no formato yyyy_mm_dd\n\n" );
	fprintf( stdout, "-c <arquivo de configuração> - Arquivo de configuração do programa, contendo as paths das dependências utilizadas. O padrão é %s\n\t\tO arquivo de configuração deve estar no seguinte formato:\n\n\t\t-1a linha: Caminho para o programa capinfos\n\t\t-2a linha: Caminho para o programa tshark\n\t\t-3a linha: Caminho para o diretório de perfis do programa thsark\n\t\t-4a linha: Nome do perfil do programa tshark a ser utilizado\n\t\t-5a linha: Caminho e nome do arquivo de criação do BD\n\n", DEFAULT_DEPENDENCIES_FILEPATH );
	fprintf( stdout, "-m <quantidade em bytes> - Determina o máximo de memória, em bytes, que podemos utilizar quando estamos armazenando o output dos programas auxiliares. O valor padrão é %"PRIu64". O valor mínimo é %"PRIu64"\n\n", DEFAULT_OUTPUT_BUFFER_MAX_MEM, DEFAULT_OUTPUT_BUFFER_MIN_MEM );
	fprintf( stdout, "-a - Faz append de conversas quando o usuário especifica uma base de dados que já existe. O padrão é false\n\n" );
	fprintf( stdout, "-v - Modo verbose. Imprime feedback no console. Certos feedbacks serão impressos apenas na compilação DEBUG\n\n" );
	fprintf( stdout, "-----------------------------------------------------------\n\n" );
}

/*==============================================================================================

FUNÇÃO int main( int, char** )
	 Ponto de entrada da aplicação.

===============================================================================================*/

int main( int argc, char* argv[] )
{
	int retVal = EXIT_FAILURE;

	try
	{
		// Lê os parâmetros do programa
		char ch;
		bool verbose = false, appendConvs = false;
		uint64_t maxMem = DEFAULT_OUTPUT_BUFFER_MAX_MEM;
		std::string capfile, dbHost, dbUser, dbPassword, dbDatabase, confFilepath( DEFAULT_DEPENDENCIES_FILEPATH );
		while( ( ch = getopt( argc, argv, "avi:h:u:p:d:c:m:" ) ) != -1 )
		{
			switch( ch )
			{
				// Imprime feedback no console
				case 'v':
					verbose = true;
					break;
					
				case 'a':
					appendConvs = true;
					break;

				// Arquivo de input
				case 'i':
					capfile.append( optarg );
					break;

				// Endereço IP do banco de dados
				case 'h':
					dbHost.append( optarg );
					break;
					
				// Usuário do banco de dados
				case 'u':
					dbUser.append( optarg );
					break;
					
				// Password do usuário do banco de dados
				case 'p':
					dbPassword.append( optarg );
					break;
					
				// Nome da base de dados
				case 'd':
					dbDatabase.append( optarg );
					break;
				
				// Path do arquivo de configuração do programa
				case 'c':
					confFilepath = optarg;
					break;

				// Determina o máximo de memória, em bytes, que podemos utilizar quando estamos armazenando o output dos programas auxiliares
				case 'm': 
					maxMem = ( uint64_t )strtoll( optarg, NULL, 10 );
					if( maxMem < DEFAULT_OUTPUT_BUFFER_MIN_MEM )
						maxMem = DEFAULT_OUTPUT_BUFFER_MIN_MEM;
					break;

				// Argumento faltando
				case ':':
				case '?':
					std::cerr << "Erro: A opção " << static_cast< char >( optopt ) << " exige um parâmetro" << std::endl;
					printHelp( argv[0] );
					return EXIT_SUCCESS;

				// Opção desconhecida
				default:
					std::cerr << "Erro: Opção desconhecida" << std::endl;
					printHelp( argv[0] );
					return EXIT_SUCCESS;
			}
		}

		// Verifica se os argumentos obrigatórios foram passados
		if( capfile.empty() || dbHost.empty() || dbUser.empty() )
		{
			std::cerr << "Erro: Parâmetro obrigatório não recebido" << std::endl;
			printHelp( argv[0] );
			retVal = EXIT_SUCCESS;
		}
		else
		{
			// Aumenta a quantidade de memória que este processo pode requisitar
			struct rlimit memLimit;
            memLimit.rlim_cur = RLIM_INFINITY - ( ( double )RLIM_INFINITY * 0.30 );
            memLimit.rlim_max = RLIM_INFINITY - ( ( double )RLIM_INFINITY * 0.15 );
            if( setrlimit( RLIMIT_AS, &memLimit ) != 0 )
				std::cout << "Atenção: Não foi possível aumentar o limite de uso de memória do processo" << std::endl;

			// Fornece alguns handlers de C++ que nos permitirão fazer logs adicionais em casos de erro
			std::set_terminate( myTerminate );
			std::set_unexpected( myUnexpected );

			// Inicia a execução da aplicação
			retVal = Oi::Application::Run( capfile, dbHost, dbUser, dbPassword, dbDatabase, confFilepath, maxMem, verbose, appendConvs );
		}
	}
	catch( const std::exception& ex )
	{
		std::cerr << ">>> EXCEÇÃO => Uma exceção de C++ foi capturada em main( int, char* ): " << ex.what() << std::endl;
	}
	catch( ... )
	{
		std::cerr << ">>> EXCEÇÃO => Um objeto indefinido foi capturado em main( int, char* )" << std::endl;
	}
    return retVal;
}
