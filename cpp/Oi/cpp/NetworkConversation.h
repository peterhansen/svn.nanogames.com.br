/*
 *  NetworkConversation.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/13/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef NETWORK_CONVERSATION_H
#define NETWORK_CONVERSATION_H

// C++
#include <vector>

// Oi
#include "Ipv4Address.h"
#include "ConversationPacket.h"

namespace Oi
{

class NetworkConversation
{
	public:
		// Tipo do container que armazena os pacotes de uma conversa
		typedef std::vector< ConversationPacket > PacketsContainer;

		// Construtor
		NetworkConversation( const Ipv4Address& src, const Ipv4Address& dst );
		NetworkConversation( const Ipv4Address& src, const Ipv4Address& dst, uint64_t nFramesFromClientToHost,
							uint64_t nBytesFromClientToHost, uint64_t nFramesFromHostToClient, uint64_t nBytesFromHostToClient, const std::string& capFileName = "" );

		// Retorna o número total de frames trocados na conversa
		uint64_t getTotalFrames( void ) const;
	
		// Retorna o número total de bytes trocados na conversa
		uint64_t getTotalBytes( void ) const;
	
		// Retorna o momento de início da conversa
		double getEpochBeginTime( void ) const;
	
		// Retorna o momento em que a conversa terminou
		double getEpochEndTime( void ) const;
	
		// Retorna o nome do arquivo .cap que contém as informações dos pacotes relativos à conversa
		std::string& getConvCapFileName( std::string& out ) const;
	
		// Determina o tipo de terminal utilizado na conversa
		void setTerminal( const std::string& type, const std::string& name );
	
		// Retorna o tipo de terminal utilizado na conversa
		void getTerminal( std::string& type, std::string& name ) const;
	
		// Indica se esta conversa já possui um tipo de terminal especificado
		bool hasTerminalTypeAndName( void ) const;
	
		// Operadores
		bool operator==( const NetworkConversation& rho ) const;

		// Dados públicos
		Ipv4Address ipClient;
		Ipv4Address ipHost;
		
		uint64_t framesFromClientToHost;
		uint64_t bytesFromClientToHost;
		
		uint64_t framesFromHostToClient;
		uint64_t bytesFromHostToClient;
	
		// Pacotes desta conversa
		PacketsContainer packets;

	private:
		// Path do arquivo que descreve esta conversa
		std::string convCapFile;
	
		// Descrição do terminal utilizado na conversa
		std::string terminalType, terminalName;
};

/*==============================================================================================

IMPLEMENTAÇÃO DOS MÉTODOS INLINE

===============================================================================================*/

inline uint64_t NetworkConversation::getTotalFrames( void ) const
{
	return framesFromClientToHost + framesFromHostToClient;
}

inline uint64_t NetworkConversation::getTotalBytes( void ) const
{
	return bytesFromClientToHost + bytesFromHostToClient;
}

inline double NetworkConversation::getEpochBeginTime( void ) const
{
	return packets.empty() ? -1.0 : packets[0].epochTime;
}

inline double NetworkConversation::getEpochEndTime( void ) const
{
	return packets.empty() ? -1.0 : packets[ packets.size() - 1 ].epochTime;
}
	
inline std::string& NetworkConversation::getConvCapFileName( std::string& out ) const
{
	out = convCapFile;
	return out;
}
	
inline void NetworkConversation::setTerminal( const std::string& type, const std::string& name )
{
	terminalType = type;
	terminalName = name;
}
	
inline void NetworkConversation::getTerminal( std::string& type, std::string& name ) const
{
	type = terminalType;
	name = terminalName;
}
	
inline bool NetworkConversation::hasTerminalTypeAndName( void ) const
{
	return !terminalType.empty() || !terminalName.empty();
}
	
} // namespace Oi

#endif
