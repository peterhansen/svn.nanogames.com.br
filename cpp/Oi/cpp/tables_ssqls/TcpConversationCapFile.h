/*
 *  TcpConversationCapFile.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/21/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef TCPCONVERSATIONSCAPFILES_TABLE_SSQLS_H
#define TCPCONVERSATIONSCAPFILES_TABLE_SSQLS_H

#include <mysql++.h>
#include <ssqls.h>

/*
	CREATE TABLE TcpConversationsCapFiles
	(
		IdCapFile BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY		COMMENT 'Identificação única de um arquivo .cap no BD',
		IdConversation BIGINT UNSIGNED NOT NULL								COMMENT 'Identificação única da conversa tcp a qual os pacotes descritos neste arquivo .cap pertencem',
		CapFile LONGBLOB NOT NULL											COMMENT 'Arquivo .cap que contém pacotes relativos a uma única conversa tcp, descrita por IdConversation',
		
		CONSTRAINT FOREIGN KEY( IdConversation )
			REFERENCES TcpConversations( IdConversation )
			ON UPDATE CASCADE
			ON DELETE RESTRICT

	)ENGINE=INNODB;
*/

sql_create_3(	TcpConversationsCapFiles, 1, 3,
				mysqlpp::sql_bigint_unsigned, IdCapFile,
				mysqlpp::sql_bigint_unsigned, IdConversation,
				mysqlpp::sql_longblob, CapFile )

#endif
