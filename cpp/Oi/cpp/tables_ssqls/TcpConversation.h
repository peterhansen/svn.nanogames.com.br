/*
 *  TcpConversation.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/21/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef TCPCONVERSATIONS_TABLE_SSQLS_H
#define TCPCONVERSATIONS_TABLE_SSQLS_H

#include <mysql++.h>
#include <ssqls.h>

/*
	CREATE TABLE TcpConversations
	(
	IdConversation BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY 		COMMENT 'Identificação única de uma conversa tcp no BD',
	ClientIp VARCHAR(15) NOT NULL											COMMENT 'Endereço IP do cliente da coversa',
	ClientPort SMALLINT(5) UNSIGNED NOT NULL								COMMENT 'Porta do cliente da conversa',
	HostIp VARCHAR(15) NOT NULL												COMMENT 'Endereço IP do host da conversa',
	HostPort SMALLINT(5) UNSIGNED NOT NULL									COMMENT 'Porta do host da conversa',
	FramesFromClientToHost BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de frames transferidos do cliente para o host',
	BytesFromClientToHost BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de bytes transferidos do cliente para o host',
	FramesFromHostToClient BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de frames transferidos do host para o cliente',
	BytesFromHostToClient BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de bytes transferidos do host para o cliente',
	TerminalType VARCHAR(64) NOT NULL DEFAULT 'Undefined'					COMMENT 'Tipo do terminal TN3270 utilizado na conversa',
	TerminalName VARCHAR(64) NOT NULL DEFAULT 'Undefined'					COMMENT 'Nome do terminal TN3270 utilizado na conversa',
	BeginTime DATETIME NOT NULL												COMMENT 'Data de início da conversa',
	EndTime DATETIME NOT NULL												COMMENT 'Data de término da conversa',

		CONSTRAINT UNIQUE KEY socket_n_begintime( ClientIp, ClientPort, HostIp, HostPort, BeginTime ),
		CONSTRAINT UNIQUE KEY socket_n_endtime( ClientIp, ClientPort, HostIp, HostPort, EndTime )

	)ENGINE=INNODB;
*/

sql_create_13(	TcpConversations, 1, 13,
				mysqlpp::sql_bigint_unsigned, IdConversation,
				mysqlpp::sql_varchar, ClientIp,
				mysqlpp::sql_smallint_unsigned, ClientPort,
				mysqlpp::sql_varchar, HostIp,
				mysqlpp::sql_smallint_unsigned, HostPort,
				mysqlpp::sql_bigint_unsigned, FramesFromClientToHost,
				mysqlpp::sql_bigint_unsigned, BytesFromClientToHost,
				mysqlpp::sql_bigint_unsigned, FramesFromHostToClient,
				mysqlpp::sql_bigint_unsigned, BytesFromHostToClient,
			    mysqlpp::sql_varchar, TerminalType,
			    mysqlpp::sql_varchar, TerminalName,
				mysqlpp::sql_datetime, BeginTime,
				mysqlpp::sql_datetime, EndTime )

#endif
