/*
 *  PacketTN3270Data.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 6/4/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef PACKETSTN3270DATA_TABLE_SSQLS_H
#define PACKETSTN3270DATA_TABLE_SSQLS_H

#include <mysql++.h>
#include <ssqls.h>

/*
	CREATE TABLE PacketsTN3270Data
	(
		IdTN3270Data BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY 	COMMENT 'Identificação única do conjunto de dados TN3270 no BD',
		IdPacket BIGINT UNSIGNED NOT NULL 									COMMENT 'Identificação única de um pacote tcp NO BD',
		Aid TINYINT UNSIGNED DEFAULT NULL									COMMENT 'TN3270: Attention Identification',
		CursorX TINYINT UNSIGNED DEFAULT NULL								COMMENT 'TN3270: Posição x DO CURSOR na tela DO mainframne',
		CursorY TINYINT UNSIGNED DEFAULT NULL 								COMMENT 'TN3270: Posição y DO CURSOR na tela DO mainframne',
		CursorRowOffset TINYINT UNSIGNED DEFAULT NULL						COMMENT 'TN3270: Deslocamento em linhas da posição inicial DO CURSOR na tela DO mainframne',
		CursorColumnOffset TINYINT UNSIGNED DEFAULT NULL					COMMENT 'TN3270: Deslocamento em colunas da posição inicial DO CURSOR na tela DO mainframne',
		FieldData TINYTEXT DEFAULT NULL										COMMENT 'TN3270: Dados enviados pelo usuário para o campo atual',
		
		CONSTRAINT FOREIGN KEY( IdPacket )
			REFERENCES TcpPackets( IdPacket )
			ON UPDATE CASCADE
			ON DELETE RESTRICT

	)ENGINE=INNODB;
*/

sql_create_8(	PacketsTN3270Data, 1, 8,
				mysqlpp::sql_bigint_unsigned, IdTN3270Data,
				mysqlpp::sql_bigint_unsigned, IdPacket,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, Aid,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorX,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorY,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorRowOffset,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorColumnOffset,
				mysqlpp::Null< mysqlpp::sql_tinytext >, FieldData )

#endif
