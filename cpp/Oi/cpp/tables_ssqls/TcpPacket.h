/*
 *  TcpPacket.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/21/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef TCPPACKETS_TABLE_SSQLS_H
#define TCPPACKETS_TABLE_SSQLS_H

#include <mysql++.h>
#include <ssqls.h>

/*
	CREATE TABLE TcpPackets
	(
		IdPacket BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY 		COMMENT 'Identificação única de um pacote tcp no BD',
		IdConversation BIGINT UNSIGNED NOT NULL								COMMENT 'Identificação única da conversa TCP a qual o pacote pertence',
		PacketSeqNum BIGINT UNSIGNED NOT NULL								COMMENT 'Número de sequência deste pacote na conversa TCP da qual faz parte',
		TcpSeq INT UNSIGNED NOT NULL										COMMENT 'Registro da sequência TCP',
		TcpLength INT UNSIGNED NOT NULL										COMMENT 'Tamanho dos dados transportados pelo pacote TCP',
		TcpFin BIT NOT NULL													COMMENT 'Flag TCP',
		TcpSyn BIT NOT NULL													COMMENT 'Flag TCP',
		TcpReset BIT NOT NULL												COMMENT 'Flag TCP',
		EpochTime DOUBLE NOT NULL											COMMENT 'Data UTC do pacote: o tempo transcorrido em segundos desde a data de referência (Jan 1, 1970 00:00:00). Ex: 1265936889.410223',
		Direction BIT NOT NULL												COMMENT 'Direção de transmissão do pacote: 0 = CLIENT_TO_HOST, 1 = HOST_TO_CLIENT',
		TcpData LONGBLOB NOT NULL											COMMENT 'Dados transportados pelo pacote TCP' ,

		CONSTRAINT UNIQUE KEY idconv_n_pktseq( IdConversation, PacketSeqNum ),
		CONSTRAINT UNIQUE KEY idconv_n_epoch( IdConversation, EpochTime ),

		CONSTRAINT FOREIGN KEY( IdConversation )
			REFERENCES TcpConversations( IdConversation )
			ON UPDATE CASCADE
			ON DELETE RESTRICT

	)ENGINE=INNODB;
*/

sql_create_11(	TcpPackets, 1, 11,
				mysqlpp::sql_bigint_unsigned, IdPacket,
				mysqlpp::sql_bigint_unsigned, IdConversation,
				mysqlpp::sql_bigint_unsigned, PacketSeqNum,
				mysqlpp::sql_int_unsigned, TcpSeq,
				mysqlpp::sql_int_unsigned, TcpLength,
				mysqlpp::sql_bool, TcpFin,
				mysqlpp::sql_bool, TcpSyn,
				mysqlpp::sql_bool, TcpReset,
				mysqlpp::sql_double, EpochTime,
				mysqlpp::sql_bool, Direction,
				mysqlpp::sql_longblob, TcpData )

#endif
