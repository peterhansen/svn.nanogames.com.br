/*
 *  ConversationPacket.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/17/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef CONVERSATION_PACKET_H
#define CONVERSATION_PACKET_H

// C++
#include <string>
#include <vector>

// Oi
#include "Ipv4Address.h"
#include "oidefs.h"
#include "TN3270Data.h"

namespace Oi
{
	enum ConversationPacketDir
	{
		CLIENT_TO_HOST = 0,
		HOST_TO_CLIENT = 1
	};

	struct ConversationPacket
	{
		// Tipo do container que armazena os blocos TN3270
		typedef std::vector< TN3270Data > TN3270DataBlocksContainer;

		// Construtor
		ConversationPacket( void );

		// Limpa as estruturas do pacote
		void clear( void );
		
		// Número de sequência do pacote na conversa
		uint64_t packetSeqNum;
		
		// TCP Sequence Number
		uint32_t tcpSeq;
				
		// TCP Segment Len
		uint32_t tcpLength;
				
		// TCP Flags
		struct
		{
			uint8_t fin : 1;
			uint8_t syn: 1;
			uint8_t reset : 1;
		} tcpFlags;
				
		// Packet Epoch Time in seconds
		double epochTime;

		// Direção de transmissão do pacote
		ConversationPacketDir direction;

		// Dados hexadecimais do pacote tcp
		ByteArray tcpData;
		
		// Blocos de informações TN3270 contidos no pacote
		TN3270DataBlocksContainer tn3270DataBlocks;
	};
	
} // namespace Oi

#endif
