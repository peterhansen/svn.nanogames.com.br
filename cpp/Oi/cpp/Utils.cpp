#include "Utils.h"

// C++
#include <stdexcept>
#include <dirent.h>

namespace Oi
{

namespace Utils
{

/*==============================================================================================

MÉTODO ExtendHomeWildcard
	Troca o caractere especial '~' pelo seu valor por extenso.

===============================================================================================*/

static std::string& ExtendHomeWildcard( std::string& str )
{
	std::string::size_type found = str.find( "~" );
	if( found != std::string::npos )
	{
		const char *pHomeDirPath = getenv( "HOME" );
		str.replace( found, 1, pHomeDirPath );
	}
	return str;
}

/*==============================================================================================

MÉTODO DivideStrBy
	Divide uma string em n strings separadas pelo caracter 'separator'.

===============================================================================================*/

std::vector< std::string >& DivideStrBy( std::vector< std::string >& out, const std::string& str, const std::string& separators )
{
	out.clear();

	std::string::size_type lastFound = 0, index;
	while( ( index = str.find_first_of( separators, lastFound ) ) != std::string::npos )
	{
		out.push_back( str.substr( lastFound, index - lastFound ) );
		lastFound = index + 1;
	}
	
	std::string::size_type strLen = str.length();
	if( lastFound < strLen )
		out.push_back( str.substr( lastFound, strLen ) );
	
	return out;
}

/*==============================================================================================

MÉTODO ParseField
	Obtém um campo, a partir de 'currCharIndex', cujo fim é determinado pelo primeiro caractere
de 'delimiters' encontrado. Retorna o conteúdo do campo em 'outFieldStr'. O valor de retorno da
função indica a partir de que índice devemos começar a procurar um novo campo.

===============================================================================================*/

std::string::size_type ParseField( std::string& outFieldStr, const std::string& str, const std::string& delimiters, std::string::size_type currCharIndex )
{
	std::string::size_type strLen = str.length();
	std::string::size_type nextFieldBegin = std::string::npos;
	
	if( currCharIndex < strLen )
	{
		nextFieldBegin = str.find_first_of( delimiters, currCharIndex );
		if( nextFieldBegin == std::string::npos )
		{
			nextFieldBegin = strLen;
			outFieldStr = str.substr( currCharIndex, nextFieldBegin - currCharIndex );
		}
		else
		{
			outFieldStr = str.substr( currCharIndex, nextFieldBegin - currCharIndex );
			++nextFieldBegin;
		}
	}
	return nextFieldBegin;
}

/*==============================================================================================

MÉTODO ParseFieldContaining
	Acha um campo, a partir de 'currCharIndex', contendo os caracteres 'fieldChars' na string
'str'. Retorna o conteúdo do campo em 'outFieldStr'. O valor de retorno da função indica a partir
de que índice devemos começar a procurar um novo campo.

===============================================================================================*/

std::string::size_type ParseFieldContaining( std::string& outFieldStr, const std::string& str, const std::string& fieldChars, std::string::size_type currCharIndex )
{
	std::string::size_type strLen = str.length();
	std::string::size_type fieldEnd = std::string::npos;

	if( currCharIndex < strLen )
	{
		currCharIndex = str.find_first_of( fieldChars, currCharIndex );
		if( currCharIndex == std::string::npos )
		{
			outFieldStr.clear();
			return currCharIndex;
		}
		
		fieldEnd = str.find_first_not_of( fieldChars, currCharIndex );
		if( fieldEnd == std::string::npos )
			fieldEnd = strLen;
		
		outFieldStr = str.substr( currCharIndex, fieldEnd - currCharIndex );
	}
	return fieldEnd;
}

/*==============================================================================================

MÉTODO ParseFieldNotContaining
	Acha um campo, a partir de 'currCharIndex', delimitado pelos caracteres 'delimiters' na string
'str'. Retorna o conteúdo do campo em 'outFieldStr'. O valor de retorno da função indica a partir
de que índice devemos começar a procurar um novo campo.

===============================================================================================*/

std::string::size_type ParseFieldNotContaining( std::string& outFieldStr, const std::string& str, const std::string& delimiters, std::string::size_type currCharIndex )	
{
	std::string::size_type strLen = str.length();
	std::string::size_type fieldEnd = std::string::npos;
	
	if( currCharIndex < strLen )
	{
		currCharIndex = str.find_first_not_of( delimiters, currCharIndex );
		if( currCharIndex == std::string::npos )
		{
			outFieldStr.clear();
			return currCharIndex;
		}
		
		fieldEnd = str.find_first_of( delimiters, currCharIndex );
		if( fieldEnd == std::string::npos )
			fieldEnd = strLen;

		outFieldStr = str.substr( currCharIndex, fieldEnd - currCharIndex );
	}

	return fieldEnd;
}

/*==============================================================================================

MÉTODO HexStrToByteVector
	Converte uma string hexadecimal para um vetor de bytes.

===============================================================================================*/

uint8_t HexDigitToByte( char c )
{
	if( isdigit( c ) )
		return c - '0';

	if( c >= 'A' && c <= 'F' )
		return c - 'A' + 10;

	if( c >= 'a' && c <= 'f' )
		return c - 'a' + 10;

	throw std::invalid_argument( "HexStrToByteVector - Invalid character in hex string" );
}
	
ByteArray& HexStrToByteVector( const std::string& hexStr, ByteArray& byteArray )
{
	std::string aux = hexStr;
	std::string::size_type length = aux.length();

	// Verfica se o comprimento da string é par (afinal, códigos hexadecimais vêm em pares)
	if( length & 1 != 0 )
	{
		aux.insert( aux.begin(), '0' );
		++length;
	}

	// Reserva a memória necessária para o array de saída
	byteArray.reserve( length >> 1 );

	// Converte
	for( std::string::size_type i = 0 ; i < length ; i += 2 )
		byteArray.push_back( ( HexDigitToByte( aux[i] ) * 16 ) + HexDigitToByte( aux[i+1] ) );
	
	return byteArray;
}

/*==============================================================================================

MÉTODO EBCDICStrToASCIIStr
	Converte uma string EBCDIC para uma string ASCII.

===============================================================================================*/

#define EBCDIC_EIGHT_ONES 0xFF

std::string& EBCDICStrToASCIIStr( std::string& out, const ByteArray& strEBCDIC, ByteArray::size_type start, ByteArray::size_type end )
{
	// Tabela de conversão
	// - http://en.wikipedia.org/wiki/ISO/IEC_8859-1
	// - http://en.wikipedia.org/wiki/EBCDIC_037
	// - http://en.wikipedia.org/wiki/Eight_Ones
	uint8_t conversionTable[] =
	{
		//		  0    1    2    3    4    5    6    7    8    9    10    11    12    13    14   15
		/* 0 */	 32,   1,   2,   3, 156,   9, 134, 127, 151, 141,  142,   11,   12,   13,   14,  15,
		/* 1 */	 16,  17,  18,  19, 157, 133,   8, 135,  24,  25,  146,  143,   28,   29,   30,  31,
		/* 2 */	128, 129, 130, 131, 132,  10,  23,  27, 136, 137,  138,  139,  140,    5,    6,   7,
		/* 3 */	144, 145,  22, 147, 148, 149, 150,   4, 152, 153,  154,  155,   20,   21,  158,  26,
		/* 4 */	 32, 160, 161, 162, 163, 164, 165, 166, 167, 168,   91,   46,   60,   40,   43,  33,
		/* 5 */	 38, 169, 170, 171, 172, 173, 174, 175, 176, 177,   93,   36,   42,   41,   59,  94,
		/* 6 */	 45,  47, 178, 179, 180, 181, 182, 183, 184, 185,  124,   44,   37,   95,   62,  63,
		/* 7 */	186, 187, 188, 189, 190, 191, 192, 193, 194,  96,   58,   35,   64,   39,   61, 34,
		/* 8 */	195,  97,  98,  99, 100, 101, 102, 103, 104, 105,  196,  197,  198,  199,  200, 201,
		/* 9 */	202, 106, 107, 108, 109, 110, 111, 112, 113, 114,  203,  204,  205,  206,  207, 208,
		/*10 */	209, 126, 115, 116, 117, 118, 119, 120, 121, 122,  210,  211,  212,  213,  214, 215,
		/*11 */	216, 217, 218, 219, 220, 221, 222, 223, 224, 225,  226,  227,  228,  229,  230, 231,
		/*12 */	123,  65,  66,  67,  68,  69,  70,  71,  72,  73,  232,  233,  234,  235,  236, 237,
		/*13 */	125,  74,  75,  76,  77,  78,  79,  80,  81,  82,  238,  239,  240,  241,  242, 243,
		/*14 */	 92, 159,  83,  84,  85,  86,  87,  88,  89,  90,  244,  245,  246,  247,  248, 249,
		/*15 */	 48,  49,  50,  51,  52,  53,  54,  55,  56,  57, 0xB3, 0xDB, 0xDC, 0xD9, 0xDA, EBCDIC_EIGHT_ONES
	};

	out.clear();

	for( ByteArray::size_type i = start ; i < end ; ++i )
		out.append( 1, static_cast< std::string::value_type >( conversionTable[ strEBCDIC[i] ] ) );

	return out;
}
	
#undef EBCDIC_EIGHT_ONES

/*==============================================================================================

MÉTODO FExists
	Retorna se o arquivo especificado existe.

===============================================================================================*/

bool FExists( const std::string& filePath )
{
	std::string temp( filePath );
	FILE* pFile = fopen( ExtendHomeWildcard( temp ).c_str(), "r" );
	if( pFile != NULL )
	{
		fclose( pFile );
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO DirExists
	Retorna se o diretório especificado existe.

===============================================================================================*/
	
bool DirExists( const std::string& dirPath )
{
	std::string temp( dirPath );
	DIR* pDir = opendir( ExtendHomeWildcard( temp ).c_str() );
	if( pDir != NULL )
	{
		closedir( pDir );
		return true;
	}
	return false;
}

/*==============================================================================================

MÉTODO GetDateForEpochTime
	Retorna os valores de dia, mês e ano relativos a um epochTime. Em casos de erro, retorna 0
nos valores de dia, mês e ano, e 255 nos valores de horas, minutos e segundos.
	OBS: Por causa dos métodos utilizados internamente, o valor dos segundos, quando válido,
estará no intervalo [0,60], e não no intervalo [0,59]...

===============================================================================================*/

void GetDateForEpochTime( double epochTime, uint16_t* pYear, uint8_t* pMonth, uint8_t* pDay, uint8_t* pHour, uint8_t* pMin, uint8_t* pSec )
{
	uint8_t day = 0, month = 0, sec = 255, min = 255, hour = 255;
	uint16_t year = 0;

	if( epochTime != -1.0 )
	{
		struct tm decoded;
		memset( &decoded, 0, sizeof( struct tm ));

		time_t encoded = static_cast< time_t >( epochTime );
		gmtime_r( &encoded, &decoded );
		
		year = decoded.tm_year + 1900;
		month = decoded.tm_mon + 1;
		day = decoded.tm_mday;
		
		hour = decoded.tm_hour;
		min = decoded.tm_min;
		sec = decoded.tm_sec;
	}
	
	if( pDay )
		*pDay = day;
	
	if( pMonth )
		*pMonth = month;
	
	if( pYear )
		*pYear = year;
	
	if( pHour )
		*pHour = hour;
	
	if( pMin )
		*pMin = min;
	
	if( pSec )
		*pSec = sec;
}

/*==============================================================================================

MÉTODO GetDateStrForEpochTime
	Retorna uma string no formato yyyy_mm_dd correspondente à data indicada por epochTime. Caso
a conversão não possa ser feita, retorna defaultString.

===============================================================================================*/

std::string& GetDateStrForEpochTime( std::string& aux, double epochTime, const std::string& defaultString )
{
	if( epochTime == -1.0 )
	{
		aux = defaultString;
	}
	else
	{
		uint16_t year;
		uint8_t day, month;
		GetDateForEpochTime( epochTime, &year, &month, &day );

#define BUFFER_LEN 16

		std::string::value_type buffer[ BUFFER_LEN ];
		snprintf( buffer, sizeof( std::string::value_type ) * BUFFER_LEN, "%04d_%02d_%02d", static_cast< int32_t >( year ), static_cast< int32_t >( month ), static_cast< int32_t >( day ) );

#undef BUFFER_LEN

		aux.assign( buffer );
	}
	return aux;
}

/*==============================================================================================

MÉTODO GetDateStrForEpochTime
	Lê todo o conteúdo do arquivo para o buffer recebido como parâmetro.

===============================================================================================*/

bool ReadAllFiletoMem( std::string& buffer, const std::string& filePath )
{
	FILE* pFile = fopen( filePath.c_str(), "rt" );
	if( pFile == NULL )
		return false;
	
	bool ret = false;

#define BUFFER_LEN ( 5 * 1024 )

	char aux[ BUFFER_LEN + 1 ];
	while( feof( pFile ) == 0 )
	{
		size_t nRead = fread( aux, sizeof( char ), BUFFER_LEN, pFile );
		
		if( ferror( pFile ) )
		{
			buffer.clear();
			goto End;
		}

		aux[ nRead ] = '\0';
		buffer.append( aux );
	}
	
#undef BUFFER_LEN
	
	ret = true;

	End:
		fclose( pFile );
		return ret;
}

/*======================================================================================================

MÉTODO GetStatement
	Retorna o próximo comando SQL contido na string 'sql' delimitado pelo delimitador 'currDelimiter'.
Começa a procurar pelo comando a partir do caractere 'startIndex'.
	Retorna o comando SQL em 'statement', o último delimitador utilizado em 'currDelimiter' e o índice
a partir do qual devemos procurar novos comandos como valor de retorno da função.
	OBS: Ignora erros de SQL.

========================================================================================================*/

std::string::size_type GetStatement( std::string& statement, std::string& currDelimiter, std::string::size_type startIndex, const std::string& sql )
{
	statement.clear();

	std::string keyword = "DELIMITER";
	std::string::size_type delimiterIndex, keywordIndex;

	for( ; ; )
	{
		delimiterIndex = sql.find_first_of( currDelimiter, startIndex );
		keywordIndex = sql.find( keyword, startIndex );
		
		// Verifica se mudamos o delimitador de comandos
		if( keywordIndex < delimiterIndex )
		{
			// Procura a string delimitadora
			startIndex = keywordIndex + keyword.length();
			std::string::size_type temp = sql.find_first_not_of( " \r\n", startIndex );
			if( temp != std::string::npos )
			{
				startIndex = temp;
				temp = sql.find_first_of( " \r\n", startIndex );
				if( temp != std::string::npos )
				{				
					currDelimiter = sql.substr( startIndex, temp - startIndex );
					startIndex += temp - startIndex;
					continue;
				}
			}

			// Delimitador muito longo
			startIndex = std::string::npos;
			return startIndex;
		}
		else if( keywordIndex > delimiterIndex )
		{
			std::string::size_type len = delimiterIndex - startIndex/* + currDelimiter.length()*/;
			statement = sql.substr( startIndex, len );
			startIndex += len + currDelimiter.length();
			return startIndex;
		}
		else
		{
			// Isto só acontecerá caso não encontremos o que estamos procurando ou caso o usuário utilize a letra D
			// como delimitador (o que acho que não é válido, e se é, não deveria ser). Logo, retorna erro
			startIndex = std::string::npos;
			return startIndex;
		}
	}
}

} // namespace Utils

} // namespace Oi
