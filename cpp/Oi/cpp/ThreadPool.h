///*
// *  ThreadPool.h
// *  Oi
// *
// *  Created by Daniel Lopes Alves on 6/8/10.
// *  Copyright 2010 Nano Games. All rights reserved.
// *
// */
//
//#ifndef THREAD_POOL_H
//#define THREAD_POOL_H
//
//#include <stdint.h>
//
//namespace Oi
//{
//	class ThreadPool
//	{
//		public:
//			// Constructor and Destructor
//			explicit ThreadPool( uint8_t maxThreads );
//			~ThreadPool( void );
//
//			// Access local variables
//			uint8_t maxThreadsAllowed( void ) const { return maxThreads; };
//		  
//			// Run, stop and synchronise with job
//			void run( const ThreadJob& job );
//			void sync( ThreadJob* job );
//			void syncAll( void );
//
//			// Return an idle thread form pool
//			TPoolThr* getIdle( void );
//
//			// Insert an idle thread into pool
//			void appendIdle( TPoolThr* t );
//
//		private:
//			// Maximum degree of parallelism
//			uint8_t maxThreads;
//
//			// Array of threads, handled by pool
//			std::vector< PoolThread* > threads;
//
//			// List of idle threads, mutexes and conditions for it
//			TSLL< TPoolThr * > _idle_threads;
//			TMutex _idle_mutex, _list_mutex;
//			TCondition _idle_cond;
//	};
//	
//} // namespace Oi
//
//#endif
//
