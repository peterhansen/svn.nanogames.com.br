#include "ConversationPacket.h"

namespace Oi
{

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

ConversationPacket::ConversationPacket( void )
				  : packetSeqNum( 0 ),
					tcpSeq( 0 ),
					tcpLength( 0 ),
					epochTime( -1.0 ),
					direction( CLIENT_TO_HOST ),
					tcpData(),
					tn3270DataBlocks()
{
	tcpFlags.fin = 0;
	tcpFlags.syn = 0;
	tcpFlags.reset = 0;
}
	
/*==============================================================================================

MÉTODO clear
	Limpa as estruturas do pacote.

===============================================================================================*/

void ConversationPacket::clear( void )
{
	// Limpa os vetores
	tcpData.clear();
	tn3270DataBlocks.clear();
}

} // namespace Oi
