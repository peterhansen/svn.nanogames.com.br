//#include "ThreadPool.h"
//
//// http://www.hlnum.org/english/projects/tools/threadpool/doc.html
//// http://books.google.com/books?id=gyCVkL0itPkC&pg=PA98&lpg=PA98&dq=pthread+thread+pool&source=bl&ots=i5c2oC-Tfo&sig=Zc0KLhzqm05QKwdJp72IFaFBGEU&hl=en&ei=AsAOTNy-KIKOuAfP7fGvDQ&sa=X&oi=book_result&ct=result&resnum=3&ved=0CBwQ6AEwAg#v=onepage&q&f=false
//
///*==============================================================================================
//
//CONSTRUTOR
//
//===============================================================================================*/
//
//ThreadPool::ThreadPool( uint8_t maxThreads ) : maxThreads( maxThreads ), threads()
//{
//    threads.reserve( maxThreads );
//
//    for( uint8_t i = 0 ; i < maxThreads ; ++i )
//    {
//        threads[i].push_back( new TPoolThr( i, this ) );
//
//		idlethreads.append( threads[i] );
//
//        threads[i]->start( true, true );
//    }
//}
//
///*==============================================================================================
//
//DESTRUTOR
//
//===============================================================================================*/
//
//ThreadPool::~ThreadPool( void )
//{
//    syncAll();
//
//	std::vector< PoolThread* >::size_type i, nThreads = threads.size();
//    for( i = 0; i < nThreads ; ++i )
//    {
//        threads[i]->syncMutex().lock();
//
//        threads[i]->set_end( true );
//        threads[i]->setJob( NULL, NULL );
//        
//        threads[i]->workMutex().lock();
//        threads[i]->workCond().signal();
//        threads[i]->workMutex().unlock();
//        
//        threads[i]->syncMutex().unlock();
//    }
//    
//    // Cancel still pending threads and delete them all
//    for( i = 0; i < nThreads; ++i )
//    {
//        threads[i]->del_mutex().lock();
//        delete threads[i];
//    }
//}
//
///*==============================================================================================
//
//MÉTODO run
//
//===============================================================================================*/
//
//void ThreadPool::run( const ThreadJob& job )
//{
//	TPoolThr *pThread = getIdle();
//
//	// And start the job
//	pThread->syncMutex().lock();
//	pThread->setJob( job );
//
//	// Attach thread to job
//	job->setPoolThread( pThread );
//
//	pThread->workMutex().lock();
//	pThread->workCond().signal();
//	pThread->workMutex().unlock();
//}
//
///*==============================================================================================
//
//MÉTODO sync
//
//===============================================================================================*/
//
//void ThreadPool::sync( ThreadJob * job )
//{
//	if( job == NULL )
//		return;
//
//	TPoolThr* pThread = job->poolThread();
//
//	// check if job is already released
//	if( pThread == NULL )
//		return;
//
//	// look if thread is working and wait for signal
//	pThread->syncMutex().lock();
//	pThread->setJob( NULL, NULL );
//	pThread->syncMutex().unlock();
//
//	// detach job and thread
//	job->setPoolThread( NULL );
//}
//
///*==============================================================================================
//
//MÉTODO syncAll
//
//===============================================================================================*/
//
//void ThreadPool::syncAll( void )
//{
//	for ( uint8_t i = 0; i < maxThreads; i++ )
//	{
//		if ( threads[i]->syncMutex().trylock() )
//			threads[i]->syncMutex().lock();
//
//		threads[i]->syncMutex().unlock();
//	}
//}
//
///*==============================================================================================
//
//MÉTODO getIdle
//
//===============================================================================================*/
//
//ThreadPool::TPoolThr * ThreadPool::getIdle( void )
//{
//	while ( true )
//    {
//        // wait for an idle thread
//        _idle_mutex.lock();
//    
//        while ( idlethreads.size() == 0 )
//            _idle_cond.wait( _idle_mutex );
//    
//        _idle_mutex.unlock();
//
//        // get first idle thread
//        _list_mutex.lock();
//        
//        if ( idlethreads.size() > 0 )
//        {
//            TPoolThr * pThread = idlethreads.behead();
//
//            _list_mutex.unlock();
//            
//            return pThread;
//        }
//
//        _list_mutex.unlock();
//    }
//}
//
///*==============================================================================================
//
//MÉTODO appendIdle
//
//===============================================================================================*/
//
//void ThreadPool::appendIdle( ThreadPool::TPoolThr * pThread )
//{
//	_list_mutex.lock();
//
//	// check if given thread is already in list and only append if not so
//	TSLL< TPoolThr * >::TIterator iter = idlethreads.first();
//
//	while( !iter.eol() )
//	{
//		if ( iter() == pThread )
//		{
//			_list_mutex.unlock();
//			return;
//		}
//		++iter;
//	}
//
//	idlethreads.append( pThread );
//	_list_mutex.unlock();
//
//	_idle_mutex.lock();
//	_idle_cond.broadcast();
//	_idle_mutex.unlock();
//}



///* create threads */ 
//65   for (i = 0; i != num_worker_threads; i++) { 
//    if ((rtn = pthread_create( &(tpool−>threads[i]), 
//      &attributes, 
//      tpool_thread, 
//      (void *)tpool)) != 0) 
//70       fprintf(stderr,"pthread_create %d",rtn), exit(1); 
//  } 
//  *tpoolp = tpool; 
//  (void) printf("thread−pool−init done\n"); 
//75 } 
