/*
 *  Ipv4Address.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/13/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef IPV4_ADDRESS_H
#define IPV4_ADDRESS_H

// C++
#include <string>

// Oi
#include "oidefs.h"

namespace Oi
{
	struct Ipv4Address
	{
		// Construtor
		explicit Ipv4Address( const std::string& ip );
		explicit Ipv4Address( uint8_t ipOctet0 = 0, uint8_t ipOctet1 = 0, uint8_t ipOctet2 = 0, uint8_t ipOctet3 = 0, IpPort port = 0 );
		
		// Igualdade
		bool operator==( const Ipv4Address& rho ) const;

		// Obtém o endereço ip formatado. Ex: "127.0.0.1:23"
		std::string& toString( std::string& out ) const;
		void toString( std::string& outIp, std::string* pOutPort ) const;
		
		// Determina o endereço ip
		void set( const std::string& ip );
		void set( uint8_t ipOctet0 = 0, uint8_t ipOctet1 = 0, uint8_t ipOctet2 = 0, uint8_t ipOctet3 = 0, IpPort port = 0 );

		// Octetos que formam o endereço ip
		uint8_t ipOctets[4];
		
		// Porta da conexão ip
		IpPort ipPort;
	};
}

#endif
