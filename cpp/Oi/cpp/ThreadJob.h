/*
 *  ThreadJob.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 6/9/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef THREAD_JOB_H
#define THREAD_JOB_H

class ThreadJob
{
	public:
		// Tipo dos métodos que podem ser associados a um job
		typedef void*( *ThreadJobFuncType )( void* );
	
		// Construtor
		// OBS: É responsabilidade do usuário destruir o parâmetro caso necessário
		ThreadJob( ThreadJobFuncType f, void *pParam );
	
		// Função a ser executada pelo job
		ThreadJobFuncType func;
	
		// Parâmetro a ser passado para a função a ser executada pelo job
		void *pFuncParam;
};

#endif
