///*
// *  Thread.h
// *  Oi
// *
// *  Created by Daniel Lopes Alves on 6/9/10.
// *  Copyright 2010 Nano Games. All rights reserved.
// *
// */
//
//#ifndef THREAD_H
//#define THREAD_H
//
//// Unix
//#include <pthread.h>
//
//class Thread
//{ 
//	public: 
//		// Constructor and Destructor
//		Thread( int p = -1 );
//		virtual ~Thread( void );
//
//		// Access local data
//		int  thread_no( void ) const { return _thread_no; };
//		void set_thread_no( int p ){ _thread_no = p; };
//		bool on_proc( int p ) const; 
//
//		// User interface
//		virtual void run( void ) = 0;      
//		virtual void start( bool d = false, bool s = false ){ create( d, s ); };
//		virtual void stop( void  ){ cancel(); };
//
//		// Thread management
//		void create( bool d = false, bool s = false );
//		void join( void );
//		void cancel( void );
//	
//	protected:
//		// Thread's ID
//		pthread_t threadId;
//	
//		// Thread attributes
//		pthread_attr_t threadAttr;
//	
//		// True if thread is running
//		bool running;
//	
//		// Optional thread's number
//		int threadNo;    
//};
//
//#endif
