#include "Mutex.h"

// C++
#include <errno.h>

namespace Oi
{

static const char* MUTEX_UNEXPECTED = "Unexpected error";
	
static const char* MUTEX_NOT_INITIALIZED = "Mutex not initialized";

static const char* MUTEX_DEADLOCK = "A deadlock would occur if the thread blocked waiting for mutex";

static const char* MUTEX_LACKS_RESOURCES = "The system temporarily lacks the resources to create another mutex";

static const char* MUTEX_THREAD_ISNT_OWNER = "The current thread does not hold a lock on mutex";

/*==============================================================================================

EXCEÇÕES

===============================================================================================*/

invalid_threadop_exception::invalid_threadop_exception( const std::string& arg ) : std::runtime_error( arg )
{
}
		
invalid_threadop_exception::~invalid_threadop_exception( void ) throw()
{
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

Mutex::Mutex( void ) : initialized( false )
{
	switch( pthread_mutex_init( &semaphore, NULL ) )
	{
		// The system temporarily lacks the resources to create another mutex
		case EAGAIN:
			throw invalid_threadop_exception( MUTEX_LACKS_RESOURCES );
			break;
			
		// The value specified by attr is invalid (como especificamos NULL, nunca deveria acontecer)
		case EINVAL:
			throw std::runtime_error( MUTEX_UNEXPECTED );
			break;
			
		// The process cannot allocate enough memory to create another mutex
		case ENOMEM:
			throw std::bad_alloc();
			break;
	}
	
	initialized = true;
}
		
/*==============================================================================================

DESTRUTOR

===============================================================================================*/

Mutex::~Mutex( void )
{
	if( initialized )
	{
		// TODOO: Isso pode causar problemas. O mais certo seria matar a thread que detém o mutex e então
		// destruí-lo
		lock();
		pthread_mutex_destroy( &semaphore );
		initialized = false;
	}
}	

/*==============================================================================================

MÉTODO lock
	Locks mutex. If the mutex is already locked, the calling thread will block until the mutex
becomes available. Throws deadlock_exception if a deadlock would occur if the thread blocked
waiting for mutex.

===============================================================================================*/

void Mutex::lock( void )
{
	if( !initialized )
		throw invalid_threadop_exception( MUTEX_NOT_INITIALIZED );

	switch( pthread_mutex_lock( &semaphore ) )
	{
		case EDEADLK:
			throw invalid_threadop_exception( MUTEX_DEADLOCK );
			break;
		
		// Nunca deveria acontecer, pois garantimos que semaphore está válido no início do método
		case EINVAL:
			break;
			
		// Ok
		case 0:
			break;
	}
}
		
/*==============================================================================================

MÉTODO unlock
	If the current thread holds the lock on mutex, then the unlock() function unlocks mutex. Throws
invalid_threadop_exception if the current thread does not hold a lock on mutex.

===============================================================================================*/

void Mutex::unlock( void )
{
	if( !initialized )
		throw invalid_threadop_exception( MUTEX_NOT_INITIALIZED );

	switch( pthread_mutex_unlock( &semaphore ) )
	{
		case EPERM:
			throw invalid_threadop_exception( MUTEX_THREAD_ISNT_OWNER );
			break;
		
		// Nunca deveria acontecer, pois garantimos que semaphore está válido no início do método
		case EINVAL:
			break;
			
		// Ok
		case 0:
			break;
	}
}

/*==============================================================================================

MÉTODO tryLock
	The tryLock() function locks mutex. If the mutex is already locked, tryLock() will not block
waiting for the mutex, but will return false. Otherwise, it returns true.

===============================================================================================*/

bool Mutex::tryLock( void )
{
	if( !initialized )
		throw invalid_threadop_exception( MUTEX_NOT_INITIALIZED );
	
	switch( pthread_mutex_trylock( &semaphore ) )
	{		
		// Nunca deveria acontecer, pois garantimos que semaphore está válido no início do método
		case EINVAL:
			break;
			
		// Ok
		case 0:
			return true;
		
		// Mutex is already locked
		case EBUSY:
			return false;
	}
	return false;
}

} // namespace Oi