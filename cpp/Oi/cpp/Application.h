/*
 *  Application.h
 *  Oi
 *
 *  Created by Daniel Lopes Alves on 5/10/10.
 *  Copyright 2010 Nano Games. All rights reserved.
 *
 */

#ifndef CLASS_APPLICATION_H
#define CLASS_APPLICATION_H

// C++
#include <map>
#include <string>
#include <vector>
#include <tr1/functional>

// Oi
#include "NetworkConversation.h"

namespace Oi
{
	class Application
	{
		public:
			// Executa a aplicação. Retorna EXIT_FAILURE em casos de erro e EXIT_SUCCESS quando bem sucedido
			static int32_t Run( const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, bool appendConvs );
		
			// Retorna a única instância da classe
			static Application* GetInstance( void );
		
			// Retorna se o mode verbose está ligado (para usar em métodos estáticos)
			bool isVerboseModeOn( void ){ return verboseModeOn; };
			
		private:
			// Tipo das funções que executam aplicações
			typedef int( *RunProgramFunc )( const intptr_t* );
		
			// Tipo do container que armazena as conversas
			typedef std::vector< NetworkConversation > ConversationsContainer;
		
			// Associação "nome da base de dados -> conversas que pertencem a esta base de dados"
			typedef std::map< std::string, std::vector< ConversationsContainer::iterator > > DatabaseMap;

			// This class represents a Singleton, so no copies, public ctors or attributions
			// TODOO
			Application( const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, bool appendConvs );
		
			// Inicia o processamento da aplicação. Retorna EXIT_FAILURE em casos de erro e EXIT_SUCCESS quando bem sucedido
			int32_t process( void );
		
			// Verifica se todos os arquivos e programas dos quais dependemos estão onde deveriam estar
			bool checkDependencies( void );
		
			// Retorna o número de pacotes contidos no arquivo .cap ou -1 em casos de erro
			int64_t readNPackets( void ) const;
		
			// Obtém as conversas de rede realizadas entre os ips descritos pelos pacotes contidos no arquivo .cap
			bool parseConversations( void );

			// Obtém as informações dos pacotes de todas as conversas contidas no arquivo
			bool parsePackets( void );
		
			// Obtém os blocos de dados TN3270 a partir dos dados contidos no array bytes passado como parâmetro. Se existir a informação, retorna também o
			// tipo de terminal TN3270 utilizado na conversa em 'terminalType'
			ConversationPacket::TN3270DataBlocksContainer& parseTN3270Data( ConversationPacket::TN3270DataBlocksContainer& out, std::string& terminalType, std::string& terminalName, const ByteArray& tcpData, ConversationPacketDir direction );
		
			// Retorna se o Attention ID (AID) do protocolo TN3270 é um dos que estamos observando
			bool isValidAID( uint8_t aid );
		
			// Retorna as conversas separadas por bases de dados em função de suas datas de início
			void divideConvsByDate( DatabaseMap& databases );
		
			// Submete todas as conversas para o BD
			bool commitAllConversations( void );
		
			// Executa um comando no banco de dados. Retorna -1 em casos de exceção, 0 em casos de falha e 1 em casos de sucesso
			int8_t execQuery( std::tr1::function< int8_t() > functor );
		
			// Submete os dados de uma conversa TCP e de seus pacotes para o BD
			int8_t commitConversation( const std::string& database, const NetworkConversation& conv ) const;
		
			// Verifica se a base de dados existe no BD com o qual estamos realizando conexões
			bool databaseExists( const std::string& databaseName ) const;
		
			// Cria uma nova base de dados a partir da base modelo. Caso a base já exista, não faz nada	
			int8_t createDatabase( const std::string& databaseName ) const;

			// Retorna as informações necessárias para rodarmos o programa capinfos
			void getCapInfosPathAndArgs( std::string* pPath, std::vector< std::string >* pArgs ) const;
		
			// Configura os parâmetros do programa tshark para obtermos as conversas existentes em um arquivo .cap
			void getTSharkPathAndArgsForConvs( std::string* pPath, std::vector< std::string >* pArgs ) const;
		
			// Configura os parâmetros do programa tshark para obtermos as informações dos pacotes relacionados a uma conversa
			void getTSharkPathAndArgsForPacketsInfo( std::string* pPath, std::vector< std::string >* pArgs ) const;
		
			// Executa o programa capinfos. Irá retorna EXIT_FAILURE em casos de erro. Caso contrário, nunca retorna
			static int RunCapinfos( const intptr_t* pParam );
		
 			// Executa o programa tshark para obter informações sobre as conversas de ip. Irá retornar
			// EXIT_FAILURE em casos de erro. Caso contrário, nunca retorna
			static int RunTSharkForConvs( const intptr_t* pParam );

			// Executa o programa tshark para obter informações sobre os pacotes de uma determinada conversa
			// ip. Irá retornar EXIT_FAILURE em casos de erro. Caso contrário, nunca retorna.
			static int RunTSharkForPacketsInfo( const intptr_t* pParam );
		
			// Executa o programa localizado em path utilizando o array de parâmetros args. O primeiro
			// parâmetro SEMPRE deverá ser o nome do programa. Irá retorna EXIT_FAILURE em casos de erro. Caso
			// contrário, nunca retorna
			static int RunProgram( const std::string& path, const std::vector< std::string >& args );
		
			// Executa uma aplicação e retorna sua saída como uma string. Caso a saída seja muito grande,
			// seu conteúdo será salvo em um arquivo. Neste caso, 'atFile' conterá o valor 'true' e 'output'
			// o nome do arquivo
			static int RunAndGetOutput( RunProgramFunc func, const intptr_t *pParam, const uint64_t& outputBuffMaxMem, std::string& output, bool& atFile );
		
			// Número de pacotes contidos no arquivo .cap
			int64_t nPackets;
		
			// Conversas realizadas
			ConversationsContainer conversations;
		
			// Path do arquivo .cap que iremos parsear
			std::string capFilePath;
		
			// Configurações do banco de dados
			std::string dbHost, dbUser, dbPass, dbDatabase;
		
			// Arquivo de configuração
			std::string confFilePath;
		
			// Caminhos das dependências do programa
			std::string capinfosPath, tsharkPath, tsharkProfilesPath, tsharkProfileToUse;
		
			// Base de dados modelo a partir da qual criaremos as bases diárias
			std::string oiDBCreationFilePath;
		
			// Determina o máximo de memória, em bytes, que podemos utilizar quando estamos armazenando o output dos programas auxiliares
			uint64_t outputBuffMaxMem;
		
			// Indica se estamos rodando no modo verbose
			bool verboseModeOn;
		
			// Indica se devemos fazer append das conversas a partir de múltiplas submissões
			bool appendConvsOn;

			// A única instância dessa classe
			static Application *pSingleton;
	};	
}

#endif
