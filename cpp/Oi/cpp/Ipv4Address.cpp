#include "Ipv4Address.h"

// C++
#include <vector>

// Oi
#include "Utils.h"

namespace Oi
{

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

Ipv4Address::Ipv4Address( const std::string& ip ) : ipPort( 0 )
{
	set( ip );
}

/*==============================================================================================

CONSTRUTOR

===============================================================================================*/

Ipv4Address::Ipv4Address( uint8_t ipOctet0, uint8_t ipOctet1, uint8_t ipOctet2, uint8_t ipOctet3, IpPort port ) : ipPort( port )
{
	set( ipOctet0, ipOctet1, ipOctet2, ipOctet3, port );
}

/*==============================================================================================

OPERADOR ==

===============================================================================================*/
	
bool Ipv4Address::operator==( const Ipv4Address& rho ) const
{
	return ( ipPort == rho.ipPort ) && ( ipOctets[0] == rho.ipOctets[0] ) && ( ipOctets[1] == rho.ipOctets[1] ) &&
		   ( ipOctets[2] == rho.ipOctets[2] ) && ( ipOctets[3] == rho.ipOctets[3] );
}
	
/*==============================================================================================

MÉTODO toString
	Obtém o endereço ip formatado. Ex: "127.0.0.1".

===============================================================================================*/

// MaxIp = 255.255.255.255 = 15 caracteres
// MaxPort = :65535 = 6 caracteres
#define IPV4ADDR_MAX_BUFFERSIZE 32
	
std::string& Ipv4Address::toString( std::string& out ) const
{
	char buffer[ IPV4ADDR_MAX_BUFFERSIZE ];
	snprintf( buffer, IPV4ADDR_MAX_BUFFERSIZE, "%d.%d.%d.%d:%d", ipOctets[0], ipOctets[1], ipOctets[2], ipOctets[3], ipPort );

	out.clear();
	out.append( buffer );
	return out;
}

/*==============================================================================================

MÉTODO toString
	Obtém o endereço ip formatado. Ex: "127.0.0.1".

===============================================================================================*/
	
void Ipv4Address::toString( std::string& outIp, std::string* pOutPort ) const
{
	char buffer[ IPV4ADDR_MAX_BUFFERSIZE ];

	snprintf( buffer, IPV4ADDR_MAX_BUFFERSIZE, "%d.%d.%d.%d", ipOctets[0], ipOctets[1], ipOctets[2], ipOctets[3] );
	outIp.clear();
	outIp.append( buffer );

	if( pOutPort )
	{
		snprintf( buffer, IPV4ADDR_MAX_BUFFERSIZE, "%d", ipPort );
		pOutPort->clear();
		pOutPort->append( buffer );
	}
}
	
#undef IPV4ADDR_MAX_BUFFERSIZE

/*==============================================================================================

MÉTODO set
	Determina o endereço ip.

===============================================================================================*/

void Ipv4Address::set( const std::string& ip )
{
	// Zera todos os octetos
	set();

	// Preenche os octetos que conseguir retirar da string
	std::vector< std::string > temp;
	Utils::DivideStrBy( temp, ip, ".:" );

	std::vector< std::string >::size_type nPartsFound = temp.size();
	for( std::vector< std::string >::size_type i = 0 ; i < nPartsFound ; ++i )
		ipOctets[i] = atoi( temp[i].c_str() );
	
	if( nPartsFound > 4 )
		ipPort = atoi( temp[4].c_str() );
}

/*==============================================================================================

MÉTODO set
	Determina o endereço ip.

===============================================================================================*/
	
void Ipv4Address::set( uint8_t ipOctet0, uint8_t ipOctet1, uint8_t ipOctet2, uint8_t ipOctet3, IpPort port )
{
	ipOctets[0] = ipOctet0;
	ipOctets[1] = ipOctet1;
	ipOctets[2] = ipOctet2;
	ipOctets[3] = ipOctet3;
	ipPort = port;
}

} // namespace OI
