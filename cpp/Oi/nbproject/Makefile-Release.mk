#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=
AS=

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_CONF=Release
CND_DISTDIR=dist

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Application.o \
	${OBJECTDIR}/NetworkConversation.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/File.o \
	${OBJECTDIR}/ConversationPacket.o \
	${OBJECTDIR}/Ipv4Address.o \
	${OBJECTDIR}/Utils.o \
	${OBJECTDIR}/TN3270Data.o


# C Compiler Flags
CFLAGS=-m64

# CC Compiler Flags
CCFLAGS=-m64 -Wall -O3
CXXFLAGS=-m64 -Wall -O3

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/lib64/mysql -L/usr/local/lib -lmysqlclient -lmysqlpp

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-Release.mk dist/Release/GNU-Linux-x86/oitcpmapper

dist/Release/GNU-Linux-x86/oitcpmapper: ${OBJECTFILES}
	${MKDIR} -p dist/Release/GNU-Linux-x86
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/oitcpmapper ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/Application.o: Application.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Application.o Application.cpp

${OBJECTDIR}/NetworkConversation.o: NetworkConversation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/NetworkConversation.o NetworkConversation.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/File.o: File.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/File.o File.cpp

${OBJECTDIR}/ConversationPacket.o: ConversationPacket.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/ConversationPacket.o ConversationPacket.cpp

${OBJECTDIR}/Ipv4Address.o: Ipv4Address.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Ipv4Address.o Ipv4Address.cpp

${OBJECTDIR}/Utils.o: Utils.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/Utils.o Utils.cpp

${OBJECTDIR}/TN3270Data.o: TN3270Data.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -O2 -s -DRELEASE -I/usr/local/include/mysql++ -I/usr/include/mysql -I. -Itables_ssqls -I/usr/include/c++/4.1.2 -MMD -MP -MF $@.d -o ${OBJECTDIR}/TN3270Data.o TN3270Data.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/Release
	${RM} dist/Release/GNU-Linux-x86/oitcpmapper

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
