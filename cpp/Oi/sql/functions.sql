DELIMITER $$

CREATE FUNCTION epochTimeInSecsToDate( epochseconds DOUBLE )
RETURNS DATETIME
LANGUAGE SQL
-- Em versões mais antigas do MySQL, os comentários podiam ter apenas 64 caracteres. Em versões mais recentes, basta descomentar a linha abaixo
-- COMMENT "Converts an epoch time, in seconds, based on the reference date (1970-01-01 00:00:00) to a datetime"
BEGIN
	RETURN DATE_ADD( CAST( '1970-01-01 00:00:00' AS DATETIME ), INTERVAL CAST( epochseconds AS CHAR ) SECOND_MICROSECOND );
END $$

DELIMITER ;
