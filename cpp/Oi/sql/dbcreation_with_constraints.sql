CREATE TABLE TcpConversations
(
	`IdConversation` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT				COMMENT 'Identificação única de uma conversa tcp no BD',
	`ClientIp` VARCHAR(15) NOT NULL											COMMENT 'Endereço IP do cliente da coversa',
	`ClientPort` SMALLINT(5) UNSIGNED NOT NULL								COMMENT 'Porta do cliente da conversa',
	`HostIp` VARCHAR(15) NOT NULL											COMMENT 'Endereço IP do host da conversa',
	`HostPort` SMALLINT(5) UNSIGNED NOT NULL								COMMENT 'Porta do host da conversa',
	`FramesFromClientToHost` BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de frames transferidos do cliente para o host',
	`BytesFromClientToHost` BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de bytes transferidos do cliente para o host',
	`FramesFromHostToClient` BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de frames transferidos do host para o cliente',
	`BytesFromHostToClient` BIGINT UNSIGNED NOT NULL DEFAULT 0				COMMENT 'Número de bytes transferidos do host para o cliente',
	`TerminalType` VARCHAR(64) NOT NULL DEFAULT 'Undefined'					COMMENT 'Tipo do terminal TN3270 utilizado na conversa',
	`TerminalName` VARCHAR(64) NOT NULL DEFAULT 'Undefined'					COMMENT 'Nome do terminal TN3270 utilizado na conversa',
	`BeginTime` DATETIME NOT NULL											COMMENT 'Data de início da conversa',
	`EndTime` DATETIME NOT NULL												COMMENT 'Data de término da conversa',
	
	PRIMARY KEY( `IdConversation` )

)ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE TcpConversationsCapFiles
(
	`IdCapFile` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT		COMMENT 'Identificação única de um arquivo .cap no BD',
	`IdConversation` BIGINT UNSIGNED NOT NULL				COMMENT 'Identificação única da conversa tcp a qual os pacotes descritos neste arquivo .cap pertencem',
	`CapFile` LONGBLOB NOT NULL								COMMENT 'Arquivo .cap que contém pacotes relativos a uma única conversa tcp, descrita por IdConversation',
	
	PRIMARY KEY( `IdCapFile` ),

	INDEX( `IdConversation` ),
	
	CONSTRAINT FOREIGN KEY( `IdConversation` )
		REFERENCES TcpConversations( `IdConversation` )
      	ON UPDATE CASCADE
		ON DELETE RESTRICT

)ENGINE=INNODB DEFAULT CHARSET=latin1;
	
CREATE TABLE TcpPackets
(
	`IdPacket` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT							COMMENT 'Identificação única de um pacote tcp no BD',
	`IdConversation` BIGINT UNSIGNED NOT NULL									COMMENT 'Identificação única da conversa TCP a qual o pacote pertence',
	`PacketSeqNum` BIGINT UNSIGNED NOT NULL										COMMENT 'Número de sequência deste pacote na conversa TCP da qual faz parte',
	`TcpSeq` INT UNSIGNED NOT NULL												COMMENT 'Registro da sequência TCP',
	`TcpLength` INT UNSIGNED NOT NULL											COMMENT 'Tamanho dos dados transportados pelo pacote TCP',
	`TcpFin` BIT NOT NULL														COMMENT 'Flag TCP',
	`TcpSyn` BIT NOT NULL														COMMENT 'Flag TCP',
	`TcpReset` BIT NOT NULL														COMMENT 'Flag TCP',
	`EpochTime` DOUBLE NOT NULL													COMMENT 'Data UTC do pacote: o tempo transcorrido em segundos desde a data de referência (Jan 1, 1970 00:00:00). Ex: 1265936889.410223',
	`Direction` BIT NOT NULL													COMMENT 'Direção de transmissão do pacote: 0 = CLIENT_TO_HOST, 1 = HOST_TO_CLIENT',
	`TcpData` LONGBLOB NOT NULL													COMMENT 'Dados transportados pelo pacote TCP' ,
	
	PRIMARY KEY( `IdPacket` ),

	INDEX( `IdConversation` ),
	INDEX( `PacketSeqNum` ),

	CONSTRAINT UNIQUE KEY idconv_n_pktseq( `IdConversation`, `PacketSeqNum` ),

	CONSTRAINT FOREIGN KEY( `IdConversation` )
		REFERENCES TcpConversations( `IdConversation` )
      	ON UPDATE CASCADE
		ON DELETE RESTRICT

)ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE PacketsTN3270Data
(
	`IdTN3270Data` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT				COMMENT 'Identificação única do conjunto de dados TN3270 no BD',
	`IdPacket` BIGINT UNSIGNED NOT NULL 								COMMENT 'Identificação única de um pacote tcp NO BD',
	`Aid` TINYINT UNSIGNED DEFAULT NULL									COMMENT 'TN3270: Attention Identification',
	`CursorX` TINYINT UNSIGNED DEFAULT NULL								COMMENT 'TN3270: Posição x DO CURSOR na tela DO mainframne',
	`CursorY` TINYINT UNSIGNED DEFAULT NULL 							COMMENT 'TN3270: Posição y DO CURSOR na tela DO mainframne',
	`CursorRowOffset` TINYINT UNSIGNED DEFAULT NULL						COMMENT 'TN3270: Deslocamento em linhas da posição inicial DO CURSOR na tela DO mainframne',
	`CursorColumnOffset` TINYINT UNSIGNED DEFAULT NULL					COMMENT 'TN3270: Deslocamento em colunas da posição inicial DO CURSOR na tela DO mainframne',
	`FieldData` TINYTEXT DEFAULT NULL									COMMENT 'TN3270: Dados enviados pelo usuário para o campo atual',
	
	PRIMARY KEY( `IdTN3270Data` ),

	INDEX( `IdPacket` ),
	
	CONSTRAINT FOREIGN KEY( `IdPacket` )
		REFERENCES TcpPackets( `IdPacket` )
      	ON UPDATE CASCADE
		ON DELETE RESTRICT

)ENGINE=INNODB DEFAULT CHARSET=latin1;

DELIMITER $$

CREATE FUNCTION epochTimeInSecsToDate( epochseconds DOUBLE )
RETURNS DATETIME
LANGUAGE SQL
BEGIN
	RETURN DATE_ADD( CAST( '1970-01-01 00:00:00' AS DATETIME ), INTERVAL CAST( epochseconds AS CHAR ) SECOND_MICROSECOND );
END $$

DELIMITER ;
