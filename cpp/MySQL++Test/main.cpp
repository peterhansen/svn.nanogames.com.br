#include <mysql++.h>

/*
1) Write your dylib source code normally. Use an IDE (Xcode, or whatever), or use a text editor (emacs, textwrangler, whatever). This is usually just a class or a collection of related functions. Just normal c, c++, or obj-c, or whatever.
2) Compile using gcc, and in addition to whatever flags you normally use, add the flags -dynamiclib -o /full/path/to/dylib/final/destination/libname_of_dylib.dylib.
3) In your .profile, add the path to your dylib (but not the name of the dylib) into the environment variable LIBRARY_PATH.
4) If you don't want to have to type -I/full/path/to/the/header/file/of/your/dylib every time you compile something that uses your new dylib, you can add the path to its header into the environment variable CPLUS_INCLUDE_PATH (again, in .profile).
5) Now when you write something that uses the dylib, do an #include <name_of_dylib_header_file.h> in your source file, and at compile time, use the flag -lname_of_dylib (that's a lowercase "L" after the "-").


A couple of footnotes:
You want to prepend the dylib with lib, but don't use it when you link (-lname_of_dylib). The same goes for the suffix .dylib.

If you compile your dylib, but want to move it later, then it will break unless you use the flag -install_name /full/path/to/dylib/final/destination/libname_of_dylib.dylib. This is necessary for the linker to work properly. This writes a reference in the header part of the dylib so the linker knows where to look for it (I know, it seems like if the linker found it in the first place, it should know where to find it, no? There is probably a good reason that this is necessary, I just don't know what it is). If you do not use the -install_name flag, it just writes whatever you put after -o in this position in the head of the dylib.

You can exclude step 3. If you do, then you will need to use the flag -L/full/path/to/dylib/final/destination when you compile you application that uses the dylib (again, excluding the name of the dylib).

I suggest reading the man pages for: gcc, otool, and ld.
*/

int main( int argc, char * const argv[] )
{
    mysqlpp::String greeting( "Hello, world!" ); 
    std::cout << greeting << std::endl; 
    return 0; 
}
