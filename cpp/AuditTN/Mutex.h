/*
 *  Mutex.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#ifndef MUTEX_H
#define MUTEX_H
#include <pthread.h>
#include <stdexcept>

namespace DB
{
	class invalid_threadop_exception : std::runtime_error
	{
		public:
			invalid_threadop_exception( const std::string& arg );
			virtual ~invalid_threadop_exception( void ) throw();
	};

	class Mutex
	{
		public:
			Mutex( void );
			~Mutex( void );
			void lock( void );
			void unlock( void );
			bool tryLock( void );

		private:
			bool initialized;
			pthread_mutex_t semaphore;
	};

}

#endif
