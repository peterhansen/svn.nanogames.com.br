/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#include "TN3270Data.h"

namespace DB {

    TN3270Data::TN3270Data(void)
    : aid(mysqlpp::null),
    cursorX(mysqlpp::null),
    cursorY(mysqlpp::null),
    cursorRowOffset(mysqlpp::null),
    cursorColumnOffset(mysqlpp::null),
    fieldData(mysqlpp::null) {
    }

    void TN3270Data::clear(void) {
        aid = mysqlpp::null;
        cursorX = mysqlpp::null;
        cursorY = mysqlpp::null;
        cursorRowOffset = mysqlpp::null;
        cursorColumnOffset = mysqlpp::null;
        fieldData = mysqlpp::null;
        fieldData.data.clear();
    }

}
