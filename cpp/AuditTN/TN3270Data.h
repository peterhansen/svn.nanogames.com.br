/*
 *  TN3270Data.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#ifndef TN3270DATA_H
#define TN3270DATA_H
#include <mysql++.h>

namespace DB {

    struct TN3270Data {
        TN3270Data(void);
        void clear(void);
        mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > aid;
        mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorX;
        mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorY;
        mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorRowOffset;
        mysqlpp::Null< mysqlpp::sql_tinyint_unsigned > cursorColumnOffset;
        mysqlpp::Null< mysqlpp::sql_tinytext > fieldData;
    };
}

#endif
