/*
 *  TcpConversationCapFile.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef TCPCONVERSATIONSCAPFILES_TABLE_SSQLS_H
#define TCPCONVERSATIONSCAPFILES_TABLE_SSQLS_H
#include <mysql++.h>
#include <ssqls.h>
sql_create_3(	TcpConversationsCapFiles, 1, 3,
				mysqlpp::sql_bigint_unsigned, IdCapFile,
				mysqlpp::sql_bigint_unsigned, IdConversation,
				mysqlpp::sql_longblob, CapFile )

#endif
