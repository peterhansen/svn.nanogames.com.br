/*
 *  TcpPacket.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef TCPPACKETS_TABLE_SSQLS_H
#define TCPPACKETS_TABLE_SSQLS_H
#include <mysql++.h>
#include <ssqls.h>
sql_create_11(	TcpPackets, 1, 11,
				mysqlpp::sql_bigint_unsigned, ClientIp,
                                mysqlpp::sql_int_unsigned, ClientPort,
				mysqlpp::sql_double, EpochTime,
                                mysqlpp::sql_bigint_unsigned, Host,
				mysqlpp::sql_int_unsigned, TcpSeq,
				mysqlpp::sql_int_unsigned, TcpLength,
				mysqlpp::sql_bool, TcpFin,
				mysqlpp::sql_bool, TcpSyn,
				mysqlpp::sql_bool, TcpReset,
				mysqlpp::sql_bool, Direction,
				mysqlpp::Null< mysqlpp::sql_text >, FieldData)

#endif
