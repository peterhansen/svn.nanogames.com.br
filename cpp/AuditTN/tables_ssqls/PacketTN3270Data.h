/*
 *  PacketTN3270Data.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef PACKETSTN3270DATA_TABLE_SSQLS_H
#define PACKETSTN3270DATA_TABLE_SSQLS_H
#include <mysql++.h>
#include <ssqls.h>
sql_create_8(	PacketsTN3270Data, 1, 8,
				mysqlpp::sql_bigint_unsigned, IdTN3270Data,
				mysqlpp::sql_bigint_unsigned, IdPacket,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, Aid,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorX,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorY,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorRowOffset,
				mysqlpp::Null< mysqlpp::sql_tinyint_unsigned >, CursorColumnOffset,
				mysqlpp::Null< mysqlpp::sql_tinytext >, FieldData )

#endif
