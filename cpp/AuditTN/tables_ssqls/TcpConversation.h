/*
 *  TcpConversation.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef TCPCONVERSATIONS_TABLE_SSQLS_H
#define TCPCONVERSATIONS_TABLE_SSQLS_H
#include <mysql++.h>
#include <ssqls.h>
sql_create_13(	TcpConversations, 1, 13,
				mysqlpp::sql_bigint_unsigned, IdConversation,
				mysqlpp::sql_varchar, ClientIp,
				mysqlpp::sql_smallint_unsigned, ClientPort,
				mysqlpp::sql_varchar, HostIp,
				mysqlpp::sql_smallint_unsigned, HostPort,
				mysqlpp::sql_bigint_unsigned, FramesFromClientToHost,
				mysqlpp::sql_bigint_unsigned, BytesFromClientToHost,
				mysqlpp::sql_bigint_unsigned, FramesFromHostToClient,
				mysqlpp::sql_bigint_unsigned, BytesFromHostToClient,
			    mysqlpp::sql_varchar, TerminalType,
			    mysqlpp::sql_varchar, TerminalName,
				mysqlpp::sql_datetime, BeginTime,
				mysqlpp::sql_datetime, EndTime )

#endif
