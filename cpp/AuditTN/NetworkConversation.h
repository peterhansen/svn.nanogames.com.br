/*
 *  NetworkConversation.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef NETWORK_CONVERSATION_H
#define NETWORK_CONVERSATION_H
#include <vector>
#include "Ipv4Address.h"
#include "ConversationPacket.h"

namespace DB
{

class NetworkConversation
{
	public:
		typedef std::vector< ConversationPacket > PacketsContainer;
		NetworkConversation( const Ipv4Address& src, const Ipv4Address& dst );
		NetworkConversation( const Ipv4Address& src, const Ipv4Address& dst, uint64_t nFramesFromClientToHost,
							uint64_t nBytesFromClientToHost, uint64_t nFramesFromHostToClient, uint64_t nBytesFromHostToClient, const std::string& capFileName = "" );
		uint64_t getTotalFrames( void ) const;
		uint64_t getTotalBytes( void ) const;
		double getEpochBeginTime( void ) const;
		double getEpochEndTime( void ) const;
		std::string& getConvCapFileName( std::string& out ) const;
		void setTerminal( const std::string& type, const std::string& name );
		void getTerminal( std::string& type, std::string& name ) const;
		bool hasTerminalTypeAndName( void ) const;
		bool operator==( const NetworkConversation& rho ) const;
		Ipv4Address ipClient;
		Ipv4Address ipHost;
		uint64_t framesFromClientToHost;
		uint64_t bytesFromClientToHost;
		uint64_t framesFromHostToClient;
		uint64_t bytesFromHostToClient;
		PacketsContainer packets;

	private:
		std::string convCapFile;
		std::string terminalType, terminalName;
};

inline uint64_t NetworkConversation::getTotalFrames( void ) const
{
	return framesFromClientToHost + framesFromHostToClient;
}

inline uint64_t NetworkConversation::getTotalBytes( void ) const
{
	return bytesFromClientToHost + bytesFromHostToClient;
}

inline double NetworkConversation::getEpochBeginTime( void ) const
{
	return packets.empty() ? -1.0 : packets[0].epochTime;
}

inline double NetworkConversation::getEpochEndTime( void ) const
{
	return packets.empty() ? -1.0 : packets[ packets.size() - 1 ].epochTime;
}

inline std::string& NetworkConversation::getConvCapFileName( std::string& out ) const
{
	out = convCapFile;
	return out;
}

inline void NetworkConversation::setTerminal( const std::string& type, const std::string& name )
{
	terminalType = type;
	terminalName = name;
}

inline void NetworkConversation::getTerminal( std::string& type, std::string& name ) const
{
	type = terminalType;
	name = terminalName;
}

inline bool NetworkConversation::hasTerminalTypeAndName( void ) const
{
	return !terminalType.empty() || !terminalName.empty();
}

}

#endif
