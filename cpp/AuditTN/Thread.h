
/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
// 
//#ifndef THREAD_H
//#define THREAD_H
////#include <pthread.h>
//
//class Thread
//{ 
//	public: 
//		Thread( int p = -1 );
//		virtual ~Thread( void );
//		int  thread_no( void ) const { return _thread_no; };
//		void set_thread_no( int p ){ _thread_no = p; };
//		bool on_proc( int p ) const; 
//		virtual void run( void ) = 0;      
//		virtual void start( bool d = false, bool s = false ){ create( d, s ); };
//		virtual void stop( void  ){ cancel(); };
//		void create( bool d = false, bool s = false );
//		void join( void );
//		void cancel( void );
//	protected:
//		pthread_t threadId;
//		pthread_attr_t threadAttr;
//		bool running;
//		int threadNo;    
//};
//
//#endif
