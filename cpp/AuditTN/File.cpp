#include "File.h"
/*
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
*/
namespace DB
{
File::File( void ) : pFile( NULL )
{
}

File::~File( void )
{
	close();
}
bool File::open( const std::string& path, const std::string& openMode )
{
	pFile = fopen( path.c_str(), openMode.c_str() );
	return pFile != NULL;
}
void File::close( void )
{
	if( pFile != NULL )
	{
		fclose( pFile );
		pFile = NULL;
	}
}

}
