/*
 *  Utils.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#ifndef UTILS_H
#define UTILS_H
#include <string>
#include <vector>
#include "DBdefs.h"
namespace DB {
    namespace Utils {
        std::vector< std::string >& DivideStrBy(std::vector< std::string >& out, const std::string& str, const std::string& separators);
        std::string::size_type ParseField(std::string& outFieldStr, const std::string& str, const std::string& delimiters, std::string::size_type currCharIndex);
        std::string::size_type ParseFieldContaining(std::string& outFieldStr, const std::string& str, const std::string& fieldChars, std::string::size_type currCharIndex);
        std::string::size_type ParseFieldNotContaining(std::string& outFieldStr, const std::string& str, const std::string& delimiters, std::string::size_type currCharIndex);
        uint64_t dottedIpAsInteger(std::string& ip);
        ByteArray& HexStrToByteVector(const std::string& hexStr, ByteArray& byteArray);
        std::string& EBCDICStrToASCIIStr(std::string& out, const ByteArray& strEBCDIC, ByteArray::size_type start, ByteArray::size_type end);
        std::string::value_type EBCDICCharToASCIIChar(const uint8_t charEBCDIC);
        bool FExists(const std::string& filePath);
        bool DirExists(const std::string& dirPath);
        void GetDateForEpochTime(double epochTime, uint16_t* pYear, uint8_t* pMonth, uint8_t* pDay, uint8_t* pHour = NULL, uint8_t* pMin = NULL, uint8_t* pSec = NULL);
        std::string& GetDateStrForEpochTime(std::string& aux, double epochTime, const std::string& defaultString);
        bool ReadAllFiletoMem(std::string& buffer, const std::string& filePath);
        std::string::size_type GetStatement(std::string& statement, std::string& currDelimiter, std::string::size_type startIndex, const std::string& sql);
    }
}

#endif
