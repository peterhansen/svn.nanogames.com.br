/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
//#include "ThreadPool.h"
//
//ThreadPool::ThreadPool( uint8_t maxThreads ) : maxThreads( maxThreads ), threads()
//{
//    threads.reserve( maxThreads );
//
//    for( uint8_t i = 0 ; i < maxThreads ; ++i )
//    {
//        threads[i].push_back( new TPoolThr( i, this ) );
//		  idlethreads.append( threads[i] );
//        threads[i]->start( true, true );
//    }
//}
//
//ThreadPool::~ThreadPool( void )
//{
//    syncAll();
//
//	std::vector< PoolThread* >::size_type i, nThreads = threads.size();
//    for( i = 0; i < nThreads ; ++i )
//    {
//        threads[i]->syncMutex().lock();
//        threads[i]->set_end( true );
//        threads[i]->setJob( NULL, NULL );
//        threads[i]->workMutex().lock();
//        threads[i]->workCond().signal();
//        threads[i]->workMutex().unlock();
//        threads[i]->syncMutex().unlock();
//    }
//    for( i = 0; i < nThreads; ++i )
//    {
//        threads[i]->del_mutex().lock();
//        delete threads[i];
//    }
//}
//void ThreadPool::run( const ThreadJob& job )
//{
//	TPoolThr *pThread = getIdle();
//	pThread->syncMutex().lock();
//	pThread->setJob( job );
//	job->setPoolThread( pThread );
//	pThread->workMutex().lock();
//	pThread->workCond().signal();
//	pThread->workMutex().unlock();
//}
//
//void ThreadPool::sync( ThreadJob * job )
//{
//	if( job == NULL )
//		return;
//	TPoolThr* pThread = job->poolThread();
//	if( pThread == NULL )
//		return;
//	pThread->syncMutex().lock();
//	pThread->setJob( NULL, NULL );
//	pThread->syncMutex().unlock();
//	job->setPoolThread( NULL );
//}
//
//void ThreadPool::syncAll( void )
//{
//	for ( uint8_t i = 0; i < maxThreads; i++ )
//	{
//		if ( threads[i]->syncMutex().trylock() )
//			threads[i]->syncMutex().lock();
//
//		threads[i]->syncMutex().unlock();
//	}
//}
//
//ThreadPool::TPoolThr * ThreadPool::getIdle( void )
//{
//	while ( true )
//    {
//        // wait for an idle thread
//        _idle_mutex.lock();
//        while ( idlethreads.size() == 0 )
//            _idle_cond.wait( _idle_mutex );
//        _idle_mutex.unlock();
//        _list_mutex.lock();
//        
//        if ( idlethreads.size() > 0 )
//        {
//            TPoolThr * pThread = idlethreads.behead();
//            _list_mutex.unlock();
//            return pThread;
//        }
//
//        _list_mutex.unlock();
//    }
//}
//
//void ThreadPool::appendIdle( ThreadPool::TPoolThr * pThread )
//{
//	_list_mutex.lock();
//	TSLL< TPoolThr * >::TIterator iter = idlethreads.first();
//	while( !iter.eol() )
//	{
//		if ( iter() == pThread )
//		{
//			_list_mutex.unlock();
//			return;
//		}
//		++iter;
//	}
//	idlethreads.append( pThread );
//	_list_mutex.unlock();
//
//	_idle_mutex.lock();
//	_idle_cond.broadcast();
//	_idle_mutex.unlock();
//}
//65   for (i = 0; i != num_worker_threads; i++) { 
//    if ((rtn = pthread_create( &(tpool−>threads[i]), 
//      &attributes, 
//      tpool_thread, 
//      (void *)tpool)) != 0) 
//70       fprintf(stderr,"pthread_create %d",rtn), exit(1); 
//  } 
//  *tpoolp = tpool; 
//  (void) printf("thread−pool−init done\n"); 
//75 } 
