/*
 *  Application.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef CLASS_APPLICATION_H
#define CLASS_APPLICATION_H
#include <map>
#include <string>
#include <vector>
#include <tr1/functional>
#include "NetworkConversation.h"

namespace DB {

    class Application {
    public:
        static int32_t Run(const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, int tamLote, uint tamOut, bool hexa);
        static Application* GetInstance(void);
        bool isVerboseModeOn(void) {
            return verboseModeOn;
        };

    private:
        typedef int( *RunProgramFunc)(const intptr_t*);
        typedef std::vector< NetworkConversation > ConversationsContainer;
        typedef std::vector< ConversationPacket > PacketsContainer;
        typedef std::map< std::string, std::vector< ConversationsContainer::iterator > > DatabaseMap;
        Application(const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, int tamLote, uint tamOut, bool hexa);
        int32_t process(void);
        bool checkDependencies(void);
        bool parsePackets(void);
        bool parsePacketsDataX(void);
        void parseTN3270Data(std::string& out, const ByteArray& tcpData, ConversationPacketDir direction);
        bool isValidAID(uint8_t aid);
        bool prepareDatabase(void);
        int8_t execQuery(std::tr1::function< int8_t() > functor);
        int8_t commitPacket(const std::string& database) const;
        int8_t commitPacketDataX(const std::string& database) const;
        bool databaseExists(const std::string& databaseName) const;
        int8_t createDatabase(const std::string& databaseName) const;
        void getTSharkPathAndArgsForPacketsInfo(std::string* pPath, std::vector< std::string >* pArgs) const;
        static int RunTSharkForPacketsInfo(const intptr_t* pParam);
        static int RunProgram(const std::string& path, const std::vector< std::string >& args);
        static int RunAndGetOutput(RunProgramFunc func, const intptr_t *pParam, const uint64_t& outputBuffMaxMem, std::string& output, bool& atFile);
        int64_t nPackets;
        PacketsContainer packets;
        std::string capFilePath;
        std::string dbHost, dbUser, dbPass, dbDatabase;
        std::string confFilePath;
        std::string capinfosPath, tsharkPath, tsharkProfilesPath, tsharkProfileToUse;
        std::string DBDBCreationFilePath;
        uint64_t outputBuffMaxMem;
        bool verboseModeOn;
        bool appendConvsOn;
        int nTamLote;
        uint32_t nTamOut;
        bool hexaOn;
        bool databaseWasAlreadyCreated;
        static Application *pSingleton;
    };
}

#endif
