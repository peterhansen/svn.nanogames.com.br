/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#include "ConversationPacket.h"
namespace DB
{
ConversationPacket::ConversationPacket( void )
				  : packetSeqNum( 0 ),
					tcpSeq( 0 ),
					tcpLength( 0 ),
					epochTime( -1.0 ),
					direction( CLIENT_TO_HOST ),
					tcpData(),
					tn3270DataBlocks()
{
	tcpFlags.fin = 0;
	tcpFlags.syn = 0;
	tcpFlags.reset = 0;
}

void ConversationPacket::clear( void )
{
	tcpData.clear();
	tn3270DataBlocks.clear();
}

}
