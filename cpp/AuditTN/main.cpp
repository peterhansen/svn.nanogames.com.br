/*
 *  main.cpp
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 */

#include <iostream>
#include <string>
#include <vector>
#include <stdexcept>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "DBdefs.h"
#include "Application.h"
#include <unistd.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <stdint.h>
void myTerminate( void )
{
	std::cerr << "Chamou std::terminate(). Abortando aplicação." << std::endl;
	abort();
}
void myUnexpected( void )
{
	std::cerr << ">>> EXCEÇÃO => Uma exceção inesperada de C++ foi lançada. Terminando aplicação." << std::endl;
	std::terminate();
}

void printHelp( const char* pProgramPath )
{
	std::string programName( pProgramPath );
	std::string::size_type index = programName.find_last_of( '/' );
	if( index != std::string::npos )
		programName.erase( 0, index + 1 );
	fprintf( stdout, "\n-----------------------------------------------------------\n" );
	fprintf( stdout, "DB TCP Conversation Mapper - por Workers Informática©\n" );
	fprintf( stdout, "Versão %s\n", PROGRAM_VERSION );
	fprintf( stdout, "Analiza conteudo de .cap e cria base MySql com dados de TN3270 para pesquisa e Replay\n\n" );
    fprintf( stdout, "O programa deve ser utilizado da seguinte forma:\n\n" );
	fprintf( stdout, "%s -i <capfile> -h <dbhost> -u <dbuser> [-p <dbpassword>] [-d <dbdatabase>] [-c <arquivo de configuração>] [-v] [-x] [-o <tamOut>] -l <tamLote>\n\n", programName.c_str() );
	fprintf( stdout, "-i <capfile> - Arquivo .cap/.pcap a ser mapeado e exportado para o banco de dados\n\n" );
	fprintf( stdout, "-h <dbhost> - Endereço do banco de dados\n\n" );
	fprintf( stdout, "-u <dbuser> - Usuário utilizado para se conectar ao banco de dados\n\n" );
	fprintf( stdout, "-p <dbpassword> - Senha do usuário utilizado para se conectar ao banco de dados. Se não for especificada, utiliza-se uma senha vazia\n\n" );
	fprintf( stdout, "-d <dbdatabase> - Nome da base na qual os dados do arquivo .cap serão importados. Se não for especificado, o nome da base será a data dos pacotes no formato yyyy_mm_dd\n\n" );
	fprintf( stdout, "-c <arquivo de configuração> - Arquivo de configuração do programa, contendo as paths das dependências utilizadas. O padrão é %s\n\t\tO arquivo de configuração deve estar no seguinte formato:\n\n\t\t-1a linha: Caminho para o programa capinfos\n\t\t-2a linha: Caminho para o programa tshark\n\t\t-3a linha: Caminho para o diretório de perfis do programa thsark\n\t\t-4a linha: Nome do perfil do programa tshark a ser utilizado\n\t\t-5a linha: Caminho e nome do arquivo de criação do BD\n\n", DEFAULT_DEPENDENCIES_FILEPATH );
	fprintf( stdout, "-m <quantidade em bytes> - Determina o máximo de memória, em bytes, que podemos utilizar quando estamos armazenando o output dos programas auxiliares. O valor padrão é %"PRIu64". O valor mínimo é %"PRIu64"\n\n", DEFAULT_OUTPUT_BUFFER_MAX_MEM, DEFAULT_OUTPUT_BUFFER_MIN_MEM );
	fprintf( stdout, "-v - Modo verbose. Imprime feedback no console. Certos feedbacks serão impressos apenas na compilação DEBUG\n\n" );
	fprintf( stdout, "-x - Grava apenas o formato em HEXA dos registros para futuro REPLAY. Roda separado para melhor performance\n\n" );
    fprintf( stdout, "-o <tamOut> - Tamanho mínimo dos strings de dados de saída considerados para consulta que serão armazenados em formato texto, default 8\n\n" );
    fprintf( stdout, "-l <tamLote> - Tamanho do lote de pacotes para serem gravados no banco de uma só vez, default 5000 registros\n\n" );
    fprintf( stdout, "-----------------------------------------------------------\n\n" );
}

int main( int argc, char* argv[] )
{
	int retVal = EXIT_FAILURE;
	try
	{
		char ch;
		bool verbose = false, hexa = false;
		uint64_t maxMem = DEFAULT_OUTPUT_BUFFER_MAX_MEM;
                int tamLote = 5000;
                int tamOut  = 8;
		std::string capfile, dbHost, dbUser, dbPassword, dbDatabase, confFilepath( DEFAULT_DEPENDENCIES_FILEPATH );
		while( ( ch = getopt( argc, argv, "avxi:h:u:p:d:c:m:l:o:" ) ) != -1 )
		{
			switch( ch )
			{
				case 'v':
					verbose = true;
					break;
				case 'i':
					capfile.append( optarg );
					break;
				case 'h':
					dbHost.append( optarg );
					break;
				case 'u':
					dbUser.append( optarg );
					break;
				case 'p':
					dbPassword.append( optarg );
					break;
				case 'd':
					dbDatabase.append( optarg );
					break;
				case 'c':
					confFilepath = optarg;
					break;
				case 'm':
					maxMem = ( uint64_t )strtoll( optarg, NULL, 10 );
					if( maxMem < DEFAULT_OUTPUT_BUFFER_MIN_MEM )
						maxMem = DEFAULT_OUTPUT_BUFFER_MIN_MEM;
					break;
                case 'l':
					tamLote = atol( optarg );
					break;
                case 'o':
					tamOut = atol( optarg );
					break;
                case 'x':
					hexa = true;
					break;
				case ':':
				case '?':
					std::cerr << "Erro: A opção " << static_cast< char >( optopt ) << " exige um parâmetro" << std::endl;
					printHelp( argv[0] );
					return EXIT_SUCCESS;
				default:
					std::cerr << "Erro: Opção desconhecida" << std::endl;
					printHelp( argv[0] );
					return EXIT_SUCCESS;
			}
		}
		if( capfile.empty() || dbHost.empty() || dbUser.empty() )
		{
			std::cerr << "Erro: Parâmetro obrigatório não recebido" << std::endl;
			printHelp( argv[0] );
			retVal = EXIT_SUCCESS;
		}
		else
		{
			struct rlimit memLimit;
            memLimit.rlim_cur = RLIM_INFINITY - ( ( double )RLIM_INFINITY * 0.30 );
            memLimit.rlim_max = RLIM_INFINITY - ( ( double )RLIM_INFINITY * 0.15 );
            if( setrlimit( RLIMIT_AS, &memLimit ) != 0 )
				std::cout << "Atenção: Não foi possível aumentar o limite de uso de memória do processo" << std::endl;
			std::set_terminate( myTerminate );
			std::set_unexpected( myUnexpected );
			retVal = DB::Application::Run( capfile, dbHost, dbUser, dbPassword, dbDatabase, confFilepath, maxMem, verbose, tamLote, tamOut, hexa );
		}
	}
	catch( const std::exception& ex )
	{
		std::cerr << ">>> EXCEÇÃO => Uma exceção de C++ foi capturada em main( int, char* ): " << ex.what() << std::endl;
	}
	catch( ... )
	{
		std::cerr << ">>> EXCEÇÃO => Um objeto indefinido foi capturado em main( int, char* )" << std::endl;
	}
    return retVal;
}
