/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#include "Application.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <cstdio>
#include <cstdlib>
#include <cerrno>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <string>
#include "ConversationPacket.h"
#include "Utils.h"
#include "PacketTN3270Data.h"
#include "TcpConversation.h"
#include "TcpPacket.h"
#include "TcpData.h"
#include "TcpConversationCapFile.h"
#include <mysql++.h>
#define PIPE_READ_END_FD 0
#define PIPE_WRITE_END_FD 1
#define PIPE_READ_BUFFER_LEN ( MBYTE )
#define TN3270_DATA_BLOCK_AID_IND 5
#define TN3270_DATA_BLOCK_CURSORX_IND 6
#define TN3270_DATA_BLOCK_CURSORY_IND 7
#define TN3270_DATA_BLOCK_FIELD_DATA_BEGIN_HTC 7
#define TN3270_DATA_BLOCK_FIELD_DATA_BEGIN_CTH 8
#define CREATE_DATABASE_RET_COULDNT_CREATE	0
#define CREATE_DATABASE_RET_DB_CREATED		1
#define CREATE_DATABASE_RET_ALREADY_EXISTS	2
#define VERBOSE( s, ... ) if( verboseModeOn ){ fprintf( stdout, s, ##__VA_ARGS__ ); }
#define STATIC_VERBOSE( s, ... ) if( DB::Application::GetInstance()->isVerboseModeOn() ){ fprintf( stdout, s, ##__VA_ARGS__ ); }

namespace DB {

    Application* Application::pSingleton = NULL;
    Application::Application(const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, int tamLote, uint tamOut, bool hexa)
    : nPackets(0),
    capFilePath(capfile),
    dbHost(dbHost),
    dbUser(dbUser),
    dbPass(dbPassword),
    dbDatabase(dbDatabase),
    confFilePath(confFilePath),
    capinfosPath(),
    tsharkPath(),
    tsharkProfilesPath(),
    tsharkProfileToUse(),
    DBDBCreationFilePath(),
    outputBuffMaxMem(outputBufferMaxMem),
    verboseModeOn(verbose),
    nTamLote(tamLote),
    nTamOut(tamOut),
    hexaOn(hexa) {
    }

    Application* Application::GetInstance(void) {
        return pSingleton;
    }

    int32_t Application::Run(const std::string& capfile, const std::string& dbHost, const std::string& dbUser, const std::string& dbPassword, const std::string& dbDatabase, const std::string& confFilePath, const uint64_t& outputBufferMaxMem, bool verbose, int tamLote, uint tamOut, bool hexa) {
        if (pSingleton == NULL)
            pSingleton = new Application(capfile, dbHost, dbUser, dbPassword, dbDatabase, confFilePath, outputBufferMaxMem, verbose, tamLote, tamOut, hexa);
        return pSingleton->process();
    }

    bool Application::checkDependencies(void) {
        if (!Utils::FExists(confFilePath)) {
            std::cerr << "Erro: O arquivo de configuração do programa DBTcpMapper não foi encontrado" << std::endl;
            return false;
        }

        std::string line;
        std::vector< std::string > dependenciesPaths;
        std::ifstream stream(confFilePath.c_str());

        while (!getline(stream, line).eof())
            dependenciesPaths.push_back(line);

        stream.close();
        line.clear();

        try {
            capinfosPath = dependenciesPaths.at(0);
            tsharkPath = dependenciesPaths.at(1);
            tsharkProfilesPath = dependenciesPaths.at(2);
            tsharkProfileToUse = dependenciesPaths.at(3);
            DBDBCreationFilePath = dependenciesPaths.at(4);
        } catch (...) {
        }
        dependenciesPaths.clear();
        if (capinfosPath.empty()) {
            std::cerr << "Erro: O caminho para o programa capinfos não foi especificado no arquivo de configuração do programa" << std::endl;
            return false;
        }

        if (!Utils::FExists(capinfosPath)) {
            std::cerr << "Erro: O programa capinfos não foi encontrado no caminho " << capinfosPath << std::endl;
            return false;
        }

        if (tsharkPath.empty()) {
            std::cerr << "Erro: O caminho para o programa tsharkPath não foi especificado no arquivo de configuração do programa" << std::endl;
            return false;
        }

        if (!Utils::FExists(tsharkPath)) {
            std::cerr << "Erro: O programa tshark não foi encontrado no caminho " << tsharkPath << std::endl;
            return false;
        }

        if (tsharkProfilesPath.empty()) {
            std::cerr << "Erro: O caminho para o diretório de perfis do programa tshark não foi especificado no arquivo de configuração do programa" << std::endl;
            return false;
        }
        if (tsharkProfileToUse.empty()) {
            std::cerr << "Erro: O perfil a ser utilizado com o programa tshark não foi especificado no arquivo de configuração do programa" << std::endl;
            return false;
        }

        std::string temp(tsharkProfilesPath);
        temp += tsharkProfileToUse + "/";
        if (!Utils::DirExists(temp)) {
            std::cerr << "Erro: O perfil " << tsharkProfileToUse << " do programa tshark não foi encontrado no caminho " << tsharkProfilesPath << std::endl;
            return false;
        }

        if (DBDBCreationFilePath.empty()) {
            std::cerr << "Erro: O caminho para o arquivo de criação do BD não foi especificado no arquivo de configuração do programa" << std::endl;
            return false;
        }

        if (!Utils::FExists(DBDBCreationFilePath)) {
            std::cerr << "Erro: O arquivo de criação do BD não foi encontrado no caminho " << DBDBCreationFilePath << std::endl;
            return false;
        }

        if (!Utils::FExists(capFilePath)) {
            std::cerr << "Erro: O arquivo .cap " << capFilePath << " não existe" << std::endl;
            return false;
        }

        return true;
    }

    int32_t Application::process(void) {
        if (!checkDependencies())
            return EXIT_FAILURE;

        if (!prepareDatabase()) {
            std::cerr << "Erro: Não pôde submeter as conversas TCP para " << dbHost << " utilizando o usuário " << dbUser << " e a senha " << (dbPass.empty() ? "<vazia>" : dbPass.c_str()) << std::endl;
            return EXIT_FAILURE;
        }


        if (hexaOn) {

            VERBOSE("Iniciando parser dos pacotes e campo Hexadecimal\n\n");

            if (!parsePacketsDataX()) {
                std::cerr << "Erro: Não pôde fazer o parse das conversas TCP com dados em Hexa contidas no arquivo " << capFilePath << std::endl;
                return EXIT_FAILURE;
            }

        } else {

            VERBOSE("Iniciando parser dos pacotes\n\n");

            if (!parsePackets()) {
                std::cerr << "Erro: Não pôde fazer o parse das conversas TCP contidas no arquivo " << capFilePath << std::endl;
                return EXIT_FAILURE;
            }

        }

        VERBOSE("Done\n\n");

        return EXIT_SUCCESS;
    }

    void Application::getTSharkPathAndArgsForPacketsInfo(std::string* pPath, std::vector< std::string >* pArgs) const {
        std::string name("tshark");

        if (pPath)
            *pPath = tsharkPath;

        if (pArgs) {
            (*pArgs).push_back(name);
            (*pArgs).push_back("-r");
            (*pArgs).push_back(capFilePath);
            (*pArgs).push_back("-nl");
            (*pArgs).push_back("-T");
            (*pArgs).push_back("fields");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.seq");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.len");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.flags.fin");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.flags.syn");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.flags.reset");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("ip.src");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.srcport");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("ip.dst");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("tcp.dstport");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("frame.time_epoch");
            (*pArgs).push_back("-e");
            (*pArgs).push_back("data");
            (*pArgs).push_back("-C");
            (*pArgs).push_back(tsharkProfileToUse);
        }
#if DEBUG
        else {
            std::cerr << "Processo Filho - Erro ao executar tshark para obter os pacotes das conversas: Sem argumentos" << std::endl;
        }
#endif
    }

    int Application::RunTSharkForPacketsInfo(const intptr_t *pParam) {
        std::string progPath;
        std::vector< std::string > progArgs;
        GetInstance()->getTSharkPathAndArgsForPacketsInfo(&progPath, &progArgs);
        return RunProgram(progPath.c_str(), progArgs);
    }

    int Application::RunProgram(const std::string& path, const std::vector< std::string >& args) {
        size_t nArgs = args.size();
        const char **pTemp = new const char*[ nArgs + 1 ];
        for (size_t i = 0; i < nArgs; ++i)
            pTemp[ i ] = args[i].c_str();

        pTemp[ nArgs ] = NULL;

        std::string aux = path;
        aux.append(pTemp[0]);
        int retVal = execvp(aux.c_str(), static_cast<char*const*> (const_cast<char**> (pTemp)));

        delete[] pTemp;

        if (retVal == -1) {
            std::cerr << "Não pôde executar " << args[0] << " no caminho " << path << std::endl << "\tErrno = " << errno << std::endl;

            return EXIT_FAILURE;
        }
        return EXIT_SUCCESS;
    }

    int Application::RunAndGetOutput(RunProgramFunc func, const intptr_t *pParam, const uint64_t& outputBuffMaxMem, std::string& output, bool& atFile) {
        atFile = false;
        output.clear();
        int pipeFds[2];
        if (pipe(pipeFds)) {
            std::cerr << "Não conseguiu criar o pipe" << std::endl;
            return EXIT_FAILURE;
        }
        pid_t childProcessId = fork();
        if (childProcessId == -1) {
            std::cerr << "Não conseguiu fazer o fork" << std::endl;
            return EXIT_FAILURE;
        }
        if (childProcessId) {
            int retVal = close(pipeFds[ PIPE_WRITE_END_FD ]);
            if (retVal == -1) {
                std::cerr << "Processo pai - Não conseguiu fechar o lado de escrita do pipe" << std::endl;
                return EXIT_FAILURE;
            }
            pid_t temp = 0;
            uint64_t totalBytes = 0, nBytesRead;
            int childExitStatus;

            std::ofstream outputFile;
            char buffer[ PIPE_READ_BUFFER_LEN ];

            uint32_t printCounter = 0, processCounter = 0;

            while (temp == 0) {
                temp = waitpid(childProcessId, &childExitStatus, WNOHANG);

                while ((nBytesRead = read(pipeFds[ PIPE_READ_END_FD ], buffer, (sizeof ( char) * PIPE_READ_BUFFER_LEN) - 1)) > 0) {
                    totalBytes += nBytesRead;
                    buffer[ nBytesRead ] = '\0';
                    if (totalBytes > outputBuffMaxMem) {
                        if (!atFile) {
                            atFile = true;

                            outputFile.open(BUFFER_FILE_PATH, std::ios_base::out);
                            if (!outputFile) {
                                output.clear();
                                temp = -1;
                                std::cerr << "Processo Pai - Não foi possível criar o arquivo temporário " << BUFFER_FILE_PATH << std::endl;
                                break;
                            }

                            outputFile.write(output.c_str(), output.length());
                            output.clear();
                            output = BUFFER_FILE_PATH;
                        }
                        outputFile.write(buffer, static_cast<ptrdiff_t> (nBytesRead));
                        outputFile.flush();
                    } else {
                        output.append(buffer);
                    }
                    ++processCounter;
                    if (processCounter % 100 == 0) {
                        std::string ellipsis(printCounter, '.');
                        printCounter = (printCounter + 1) % 4;
                        STATIC_VERBOSE("\rProcessing %u ", processCounter);
                    }
                }
            }

            STATIC_VERBOSE("\nIntermediate process finished Count= %u\n", processCounter);
            if (atFile)
                outputFile.close();

            if (temp == -1) {
                std::cerr << "Processo Pai - Processo filho falhou" << std::endl;
                return EXIT_FAILURE;
            }
            retVal = close(pipeFds[ PIPE_READ_END_FD ]);
            if (retVal == -1) {
                std::cerr << "Processo Pai - Não conseguiu fechar o lado de escrita do pipe" << std::endl;
                return EXIT_FAILURE;
            }

            if (!WIFEXITED(childExitStatus)) {
                std::cerr << "Processo Pai - Processo filho não obteve sucesso" << std::endl;

                if (WIFSIGNALED(childExitStatus)) {
                    std::cerr << "Processo Pai - Processo filho terminou devido ao recebimento de um sinal (número = " << WTERMSIG(childExitStatus) << "). Gerou um arquivo dump? " << (WCOREDUMP(childExitStatus) ? "sim" : "não") << std::endl;
                }
                return EXIT_FAILURE;
            }

            return EXIT_SUCCESS;
        } else {
            int retVal = close(pipeFds[ PIPE_READ_END_FD ]);
            if (retVal == -1) {
                std::cerr << "Não conseguiu fechar o lado de leitura do pipe" << std::endl;
                return EXIT_FAILURE;
            }
            retVal = dup2(pipeFds[ PIPE_WRITE_END_FD ], STDOUT_FILENO);
            if (retVal == -1) {
                std::cerr << "Não conseguiu substituir o lado de escrita do pipe" << std::endl;
                return EXIT_FAILURE;
            }
            if (func(pParam) == EXIT_FAILURE) {
                std::cerr << "Filho - Erro ao executar o programa" << std::endl;
                return EXIT_FAILURE;
            }
        }
        return EXIT_FAILURE;
    }

    bool Application::parsePackets(void) {
        bool atFile;
        std::string output;

        std::string hostPortDefault("23");

        if (RunAndGetOutput(RunTSharkForPacketsInfo, NULL, outputBuffMaxMem, output, atFile) == EXIT_SUCCESS) {
            std::ifstream filestream;
            std::istringstream memstream;
            std::istream* pStream;
            if (!atFile) {
                memstream.str(output);
                output.clear();

                pStream = &memstream;
            } else {
                filestream.open(output.c_str(), std::ios_base::in);
                if (!filestream) {
                    std::cerr << "Não pode abrir o arquivo " << output.c_str() << ". Abortando." << std::endl;
                    return false;
                }

                pStream = &filestream;
            }

            std::string terminalType, terminalName, line, fieldStr, temp, delimiters = "\t";
            std::string::size_type currCharIndex;

            uint64_t packetSeqNum = 0;
            int contador = 0;
            ConversationPacket packet;

            std::string primeiroIp, primeiraPorta, segundoIp, segundaPorta;


            bool hasNext = !getline(*pStream, line).eof();



#ifdef DEMO_VERSION
            const int maxInserts = 1000;
            int insertCounter = 0;
            while (hasNext && maxInserts > insertCounter) {
#else
            while (hasNext) {
#endif
            
                packet.packetSeqNum = packetSeqNum++;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, 0);
                packet.tcpSeq = atoi(fieldStr.c_str());
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpLength = atoi(fieldStr.c_str());
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpFlags.fin = fieldStr[0] == '0' ? 0 : 1;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpFlags.syn = fieldStr[0] == '0' ? 0 : 1;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpFlags.reset = fieldStr[0] == '0' ? 0 : 1;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                primeiroIp = fieldStr;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                primeiraPorta = fieldStr;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                segundoIp = fieldStr;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                segundaPorta = fieldStr;

                if (primeiraPorta.compare(hostPortDefault) == 0) {
                    packet.direction = HOST_TO_CLIENT;

                    packet.clientIp = Utils::dottedIpAsInteger(segundoIp);
                    packet.clientPort = atoi(segundaPorta.c_str());
                    packet.host = (Utils::dottedIpAsInteger(primeiroIp) * 100000) + atoi(primeiraPorta.c_str());


                } else {
                    packet.direction = CLIENT_TO_HOST;

                    packet.clientIp = Utils::dottedIpAsInteger(primeiroIp);
                    packet.clientPort = atoi(primeiraPorta.c_str());
                    packet.host = (Utils::dottedIpAsInteger(segundoIp) * 100000) + atoi(segundaPorta.c_str());

                }

                primeiroIp.clear();
                primeiraPorta.clear();
                segundoIp.clear();
                segundaPorta.clear();
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.epochTime = atof(fieldStr.c_str());
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                if (currCharIndex != std::string::npos) {
                    temp.swap(fieldStr);
                    Utils::HexStrToByteVector(temp, packet.tcpData);
                    temp.clear();
                }

                std::string saidaParse;
                parseTN3270Data(saidaParse, packet.tcpData, packet.direction);

                packet.fieldData = saidaParse;
                saidaParse.clear();

                if ((!terminalType.empty()) && packet.terminalType.empty()) {
                    packet.terminalType = terminalType;
                    terminalType.clear();

                }

                if ((!terminalName.empty()) && packet.terminalName.empty()) {
                    packet.terminalName = terminalName;
                    terminalName.clear();
                }

                packets.push_back(packet);
                contador++;
                packet.clear();

                hasNext = !getline(*pStream, line).eof();

                if ((contador == nTamLote) || ((contador > 0) && (!hasNext))) {

                    contador = 0;

                    if (!execQuery(std::tr1::bind(&Application::commitPacket, this, dbDatabase)) > 0) {

                        if (!databaseWasAlreadyCreated) {
                            mysqlpp::Connection conn(false);
                            conn.connect(NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str());
                            conn.drop_db(dbDatabase);
                        }
                        return false;
                    }

#ifdef DEMO_VERSION
                    insertCounter++;
#endif
                    packets.clear();
                }
            }

            if (atFile) remove(output.c_str());

            return true;
        }
        return false;
    }

    bool Application::parsePacketsDataX(void) {
        bool atFile;
        std::string output;
        std::string hostPortDefault("23");
        if (RunAndGetOutput(RunTSharkForPacketsInfo, NULL, outputBuffMaxMem, output, atFile) == EXIT_SUCCESS) {
            std::ifstream filestream;
            std::istringstream memstream;
            std::istream* pStream;
            if (!atFile) {
                memstream.str(output);
                output.clear();

                pStream = &memstream;
            } else {
                filestream.open(output.c_str(), std::ios_base::in);
                if (!filestream) {
                    std::cerr << "Não pode abrir o arquivo " << output.c_str() << ". Abortando." << std::endl;
                    return false;
                }

                pStream = &filestream;
            }

            std::string terminalType, terminalName, line, fieldStr, temp, delimiters = "\t";
            std::string::size_type currCharIndex;

            uint64_t packetSeqNum = 0;
            int contador = 0;
            ConversationPacket packet;

            std::string primeiroIp, primeiraPorta, segundoIp, segundaPorta;

            bool hasNext = !getline(*pStream, line).eof();

            while (hasNext) {
                packet.packetSeqNum = packetSeqNum++;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, 0);
                packet.tcpSeq = atoi(fieldStr.c_str());
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpLength = atoi(fieldStr.c_str());
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpFlags.fin = fieldStr[0] == '0' ? 0 : 1;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpFlags.syn = fieldStr[0] == '0' ? 0 : 1;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.tcpFlags.reset = fieldStr[0] == '0' ? 0 : 1;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                primeiroIp = fieldStr;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                primeiraPorta = fieldStr;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                segundoIp = fieldStr;
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                segundaPorta = fieldStr;

                if (primeiraPorta.compare(hostPortDefault) == 0) {
                    packet.direction = HOST_TO_CLIENT;

                    packet.clientIp = Utils::dottedIpAsInteger(segundoIp);
                    packet.clientPort = atoi(segundaPorta.c_str());
                    packet.host = (Utils::dottedIpAsInteger(primeiroIp) * 100000) + atoi(primeiraPorta.c_str());


                } else {
                    packet.direction = CLIENT_TO_HOST;

                    packet.clientIp = Utils::dottedIpAsInteger(primeiroIp);
                    packet.clientPort = atoi(primeiraPorta.c_str());
                    packet.host = (Utils::dottedIpAsInteger(segundoIp) * 100000) + atoi(segundaPorta.c_str());

                }

                primeiroIp.clear();
                primeiraPorta.clear();
                segundoIp.clear();
                segundaPorta.clear();
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                packet.epochTime = atof(fieldStr.c_str());
                currCharIndex = Utils::ParseField(fieldStr, line, delimiters, currCharIndex);
                if (currCharIndex != std::string::npos) {
                    temp.swap(fieldStr);
                    Utils::HexStrToByteVector(temp, packet.tcpData);
                    temp.clear();
                }

                packets.push_back(packet);
                contador++;
                packet.clear();

                hasNext = !getline(*pStream, line).eof();

                if ((contador == nTamLote) || ((contador > 0) && (!hasNext))) {

                    contador = 0;
                    if (!execQuery(std::tr1::bind(&Application::commitPacketDataX, this, dbDatabase)) > 0) {

                        if (!databaseWasAlreadyCreated) {
                            mysqlpp::Connection conn(false);
                            conn.connect(NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str());
                            conn.drop_db(dbDatabase);
                        }
                        return false;
                    }
                    packets.clear();
                }
            }

            if (atFile) remove(output.c_str());

            return true;
        }
        return false;
    }

    void Application::parseTN3270Data(std::string& out, const ByteArray& tcpData, ConversationPacketDir direction) {
        try {

            bool flushData = 0;
            int iniByte = 0;
            std::string localString;

            ByteArray::size_type nBytes = tcpData.size();
            if (tcpData.at(0) == 0x00) {

                if (direction == HOST_TO_CLIENT) {
                    iniByte = TN3270_DATA_BLOCK_FIELD_DATA_BEGIN_HTC;
                } else {
                    iniByte = TN3270_DATA_BLOCK_FIELD_DATA_BEGIN_CTH;
                }
            }

            for (ByteArray::size_type currByte = iniByte; currByte < nBytes; ++currByte) {

                if ((tcpData.at(currByte) == 0x11) || (tcpData.at(currByte) == 0x28)) {
                    currByte += 2;
                    flushData = 1;
                } else if (tcpData.at(currByte) == 0x1D) {
                    currByte += 1;
                    flushData = 1;
                } else if ((tcpData.at(currByte) == 0x13) || (tcpData.at(currByte) == 0x05) || (tcpData.at(currByte) == 0x6D)) {
                    flushData = 1;
                    continue;
                } else if (tcpData.at(currByte) == 0x3C) {
                    currByte += 3;
                    flushData = 1;
                } else if ((tcpData.at(currByte) == 0x29) || (tcpData.at(currByte) == 0x2C)) {
                    flushData = 1;
                    currByte = currByte + 1 + (2 * tcpData.at(currByte + 1));
                } else if (tcpData.at(currByte) == 0xFF && tcpData.at(currByte + 1) == 0xEF) {
                    break;
                } else if (tcpData.at(currByte) == 0xFF
                        && tcpData.at(currByte + 1) == 0xFA
                        && nBytes - currByte > 13
                        && direction == HOST_TO_CLIENT) {
                    for (ByteArray::size_type currByte2 = currByte; currByte2 < nBytes; ++currByte2) {
                        int i = 0;

                        if (tcpData.at(currByte2) == 0x18 || tcpData.at(currByte2) == 0x28) {
                            char termType[15];
                            currByte2 += 2;
                            if (tcpData.at(currByte2) != 0x49) {
                                currByte2++;
                            }
                            for (i = 0; i < 15; i++) {
                                if (tcpData.at(currByte2) > 0x5a || tcpData.at(currByte2) < 0x20) {
                                    break;
                                } else {
                                    termType[i] = tcpData.at(currByte2);
                                }
                                currByte2++;
                            }
                            if (i > 9) {
                                out.append("TTyp:");
                                out.append(termType, i);
                                out.append("*");
                            }
                            if (tcpData.at(currByte2) == 0x01) {
                                char termName[8];
                                currByte2++;
                                for (i = 0; i < 8; i++) {
                                    if (tcpData.at(currByte2) > 0x5a || tcpData.at(currByte2) < 0x20) {
                                        break;
                                    } else {
                                        termName[i] = tcpData.at(currByte2);
                                    }
                                    currByte2++;
                                }

                                if (i > 1) {
                                    out.append(" LUnm:");
                                    out.append(termName, i);
                                    out.append("*");
                                }
                            }
                        }
                        currByte = currByte2;
                    }
                    localString.clear();

                } else {
                    if (tcpData.at(currByte) > 63) {

                        if (flushData == 1 || tcpData.at(currByte) == 64) {
                            if (localString.length() > 3 && direction == CLIENT_TO_HOST
                                    || (direction == HOST_TO_CLIENT && localString.length() > nTamOut)) {
                                out.append(localString + " ");
                            }
                            localString.clear();
                            flushData = 0;
                        }

                        if (tcpData.at(currByte) > 64 && tcpData.at(currByte) < 250) {
                            localString.append(1, Utils::EBCDICCharToASCIIChar(tcpData.at(currByte)));
                        }
                    }
                }
            }

            if ((localString.length() > 3 && direction == CLIENT_TO_HOST) ||
                    (direction == HOST_TO_CLIENT && localString.length() > nTamOut)) {
                out.append(localString);
            }
            localString.clear();
        } catch (const std::out_of_range& ex) {
        } catch (...) {
        }

    }

    bool Application::isValidAID(uint8_t aid) {
        return ( aid >= 0xF0 && aid <= 0xF9) || (aid >= 0x7A && aid <= 0x7D) || (aid >= 0xC1 && aid <= 0xC9) || (aid >= 0x4A && aid <= 0x4C) || (aid >= 0x6A && aid <= 0x6D);
    }

    bool Application::prepareDatabase(void) {

        VERBOSE("Creating database %s\n", dbDatabase.c_str());

        int8_t ret = execQuery(std::tr1::bind(&Application::createDatabase, this, dbDatabase));
        if (ret <= 0)
            return false;

        databaseWasAlreadyCreated = (ret == CREATE_DATABASE_RET_ALREADY_EXISTS);

        return true;
    }

    int8_t Application::execQuery(std::tr1::function< int8_t() > functor) {
        int8_t ret = -1;
        try {
            ret = functor();
        } catch (const mysqlpp::ConnectionFailed& ex) {
            std::cerr << "A conexão MySQL falhou: " << ex.what() << std::endl;
        } catch (const mysqlpp::BadQuery& ex) {
            std::cerr << "Erro de query: " << ex.what() << std::endl;
        } catch (const mysqlpp::BadConversion& ex) {
            std::cerr << "Erro de conversão: " << ex.what() << std::endl << "\tTamanho dos dados retornados: " << ex.retrieved << ", tamanho esperado: " << ex.actual_size << std::endl;
        } catch (const mysqlpp::Exception& ex) {
            std::cerr << "Erro: " << ex.what() << std::endl;
        } catch (const std::exception& ex) {
            std::cerr << ">>> EXCEÇÃO => Exceção não-MySQL++ capturada em código MySQL++: " << ex.what() << std::endl;
        } catch (...) {
            std::cerr << ">>> EXCEÇÃO => Objeto inesperado capturado em execQuery" << std::endl;
        }
        return ret;
    }

    int8_t Application::commitPacket(const std::string& database) const {
        mysqlpp::Connection conn(database.c_str(), dbHost.c_str(), dbUser.c_str(), dbPass.c_str());
        mysqlpp::Query query = conn.query();

        mysqlpp::String dummy;
        TcpPackets dbTcpPacket;

        NetworkConversation::PacketsContainer::const_iterator packetsEnd = packets.end(), currPacket = packets.begin();

        if (currPacket != packetsEnd) {
            mysqlpp::Transaction trans(conn);

            std::vector< TcpPackets > dbPackets;
            uint64_t packetSeqNum = 0;

            for (packetSeqNum = 0; (currPacket != packetsEnd); ++currPacket, ++packetSeqNum) {

                if (currPacket->fieldData.length() > 0 ||
                        ((currPacket->tcpFlags.fin + currPacket->tcpFlags.syn + currPacket->tcpFlags.reset) > 0)) {

                    dbTcpPacket = TcpPackets(currPacket->clientIp,
                            currPacket->clientPort,
                            currPacket->epochTime,
                            currPacket->host,
                            currPacket->tcpSeq,
                            currPacket->tcpLength,
                            currPacket->tcpFlags.fin,
                            currPacket->tcpFlags.syn,
                            currPacket->tcpFlags.reset,
                            currPacket->direction,
                            currPacket->fieldData);

                    dbPackets.push_back(dbTcpPacket);
                }
            }


            if (!dbPackets.empty()) {
                query.insert(dbPackets.begin(), dbPackets.end());
                query.execute();
            }

            trans.commit();
            dbPackets.clear();
        }
        return 1;
    }

    int8_t Application::commitPacketDataX(const std::string& database) const {
        mysqlpp::Connection conn(database.c_str(), dbHost.c_str(), dbUser.c_str(), dbPass.c_str());
        mysqlpp::Query query = conn.query();

        mysqlpp::String dummy;
        TcpDatax dbTcpPacketData;

        NetworkConversation::PacketsContainer::const_iterator packetsEnd = packets.end(), currPacket = packets.begin();

        if (currPacket != packetsEnd) {
            mysqlpp::Transaction trans(conn);

            std::vector< TcpDatax > dbPacketsDatas;
            uint64_t packetSeqNum = 0;
            for (packetSeqNum = 0; (currPacket != packetsEnd); ++currPacket, ++packetSeqNum) {

                if (currPacket->tcpFlags.fin > 0 || currPacket->tcpFlags.syn > 0 ||
                        currPacket->tcpFlags.reset > 0 || currPacket->tcpLength > 0) {

                    dbTcpPacketData = TcpDatax(currPacket->clientIp,
                            currPacket->clientPort,
                            currPacket->epochTime,
                            currPacket->host,
                            currPacket->tcpSeq,
                            currPacket->tcpLength,
                            currPacket->tcpFlags.fin,
                            currPacket->tcpFlags.syn,
                            currPacket->tcpFlags.reset,
                            currPacket->direction,
                            (currPacket->tcpData.empty() ? dummy : mysqlpp::String(reinterpret_cast<const char*> (&(currPacket->tcpData[0])), currPacket->tcpData.size())));

                    dbPacketsDatas.push_back(dbTcpPacketData);

                }
            }


            if (!dbPacketsDatas.empty()) {
                query.insert(dbPacketsDatas.begin(), dbPacketsDatas.end());
                query.execute();
            }

            trans.commit();
            dbPacketsDatas.clear();
        }
        return 1;
    }

    bool Application::databaseExists(const std::string& databaseName) const {
        mysqlpp::Connection conn(NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str());
        mysqlpp::Query query = conn.query();
        query << "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = " << mysqlpp::quote_only << databaseName;
        query.store();
        return query.affected_rows() == 1;
    }

    int8_t Application::createDatabase(const std::string& databaseName) const {
        if (databaseExists(databaseName))
            return CREATE_DATABASE_RET_ALREADY_EXISTS;
        std::string sql;
        if (!Utils::FExists(DBDBCreationFilePath) || !Utils::ReadAllFiletoMem(sql, DBDBCreationFilePath)) {
            std::cerr << "Erro: O arquivo que contém as queries para a criação da base de dados não foi encontrado" << std::endl;
            return CREATE_DATABASE_RET_COULDNT_CREATE;
        }
        bool databaseCreated = false;
        try {
            mysqlpp::Connection conn;
            conn.set_option(new mysqlpp::MultiResultsOption(true));
            conn.set_option(new mysqlpp::MultiStatementsOption(true));
            if (!conn.connect(NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str()))
                throw mysqlpp::ConnectionFailed("Connection failed");

            mysqlpp::Transaction trans(conn);
            if (!conn.create_db(databaseName)) {
                std::cerr << "Erro: Não foi possível criar a base de dados " << databaseName << " para esta importação" << std::endl;
                return CREATE_DATABASE_RET_COULDNT_CREATE;
            }
            databaseCreated = true;

            if (!conn.select_db(databaseName)) {
                std::string aux("Could not select database ");
                aux += databaseName;
                throw mysqlpp::DBSelectionFailed(aux.c_str());
            }

            mysqlpp::Query query = conn.query();
            std::string currDelimiter(";"), statement;
            std::string::size_type startIndex = 0;
            while (startIndex != std::string::npos) {
                startIndex = Utils::GetStatement(statement, currDelimiter, startIndex, sql);
                if (!statement.empty()) {
                    query.reset();
                    query << statement;
                    mysqlpp::SimpleResult res = query.execute();
                    if (!res)
                        throw std::runtime_error("Could not execute multiple queries");

                    statement.clear();
                }
            }

            trans.commit();
        } catch (...) {
            if (databaseCreated) {
                mysqlpp::Connection conn(false);
                conn.connect(NULL, dbHost.c_str(), dbUser.c_str(), dbPass.c_str());
                conn.drop_db(databaseName);
            }
            throw;
        }
        return CREATE_DATABASE_RET_DB_CREATED;
    }

}
