/*
 *
 *  ConversationPacket.h
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef CONVERSATION_PACKET_H
#define CONVERSATION_PACKET_H
#include <string>
#include <vector>
#include "Ipv4Address.h"
#include "DBdefs.h"
#include "TN3270Data.h"

namespace DB
{
	enum ConversationPacketDir
	{
		CLIENT_TO_HOST = 1,
		HOST_TO_CLIENT = 0
	};

	struct ConversationPacket
	{
		typedef std::vector< TN3270Data > TN3270DataBlocksContainer;
		ConversationPacket( void );
		void clear( void );
		uint64_t packetSeqNum;
		uint32_t tcpSeq;
		uint32_t tcpLength;
                uint64_t clientIp, host;
                uint32_t clientPort;
		struct
		{
			uint8_t fin : 1;
			uint8_t syn: 1;
			uint8_t reset : 1;
		} tcpFlags;
		double epochTime;
		ConversationPacketDir direction;
		ByteArray tcpData;
		TN3270DataBlocksContainer tn3270DataBlocks;
		std::string terminalType, terminalName, fieldData;
	};

}
#endif

