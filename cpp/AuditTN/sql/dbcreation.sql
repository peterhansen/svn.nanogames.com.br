CREATE TABLE TcpPackets
(
	`ClientIp` BIGINT(16) UNSIGNED NOT NULL COMMENT 'Endereço IP do cliente da coversa',
	`ClientPort` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Porta do cliente da coversa',
	`EpochTime` DOUBLE UNSIGNED NOT NULL COMMENT 'Data UTC do pacote: o tempo transcorrido em segundos desde a data de referência (Jan 1, 1970 00:00:00). Ex: 1265936889.410223',	
	`Host` BIGINT(16) UNSIGNED NOT NULL COMMENT 'Endereço IP do host da conversa',
	`TcpSeq` INT UNSIGNED NOT NULL COMMENT 'Registro da sequência TCP',
	`TcpLength` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Tamanho dos dados transportados pelo pacote TCP',
	`TcpFin` BIT NOT NULL COMMENT 'Flag TCP',
	`TcpSyn` BIT NOT NULL COMMENT 'Flag TCP',
	`TcpReset` BIT NOT NULL COMMENT 'Flag TCP',
	`Direction` BIT NOT NULL COMMENT 'Direção de transmissão do pacote: 0 = CLIENT_TO_HOST, 1 = HOST_TO_CLIENT',
	`FieldData` TEXT DEFAULT NULL COMMENT 'TN3270: Dados enviados pelo usuário para o campo atual',
	
	PRIMARY KEY( `ClientIp`, `EpochTime` )

)ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE TcpDatax
(
	`ClientIp` BIGINT(16) UNSIGNED NOT NULL COMMENT 'Endereço IP do cliente da coversa',
	`ClientPort` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Porta do cliente da coversa',
	`EpochTime` DOUBLE UNSIGNED NOT NULL COMMENT 'Data UTC do pacote: o tempo transcorrido em segundos desde a data de referência (Jan 1, 1970 00:00:00). Ex: 1265936889.410223',	
	`Host` BIGINT(16) UNSIGNED NOT NULL COMMENT 'Endereço IP do host da conversa',
	`TcpSeq` INT UNSIGNED NOT NULL COMMENT 'Registro da sequência TCP',
	`TcpLength` SMALLINT(5) UNSIGNED NOT NULL COMMENT 'Tamanho dos dados transportados pelo pacote TCP',
	`TcpFin` BIT NOT NULL COMMENT 'Flag TCP',
	`TcpSyn` BIT NOT NULL COMMENT 'Flag TCP',
	`TcpReset` BIT NOT NULL COMMENT 'Flag TCP',
	`Direction` BIT NOT NULL COMMENT 'Direção de transmissão do pacote: 0 = CLIENT_TO_HOST, 1 = HOST_TO_CLIENT',

	`TcpData` LONGBLOB NOT NULL COMMENT 'Dados transportados pelo pacote TCP',
	
	PRIMARY KEY( `ClientIp`, `EpochTime` )

)ENGINE=INNODB DEFAULT CHARSET=latin1;

SET GLOBAL max_allowed_packet = 1000000000;

DELIMITER $$

CREATE FUNCTION epochTimeInSecsToDate( epochseconds DOUBLE )
RETURNS DATETIME
LANGUAGE SQL
BEGIN
	RETURN DATE_ADD( CAST( '1970-01-01 00:00:00' AS DATETIME ), INTERVAL CAST( epochseconds AS CHAR ) SECOND_MICROSECOND );
END $$

DELIMITER ;
