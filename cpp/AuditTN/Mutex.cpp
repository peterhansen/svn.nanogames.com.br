/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#include "Mutex.h"
#include <errno.h>

namespace DB
{
static const char* MUTEX_UNEXPECTED = "Unexpected error";
static const char* MUTEX_NOT_INITIALIZED = "Mutex not initialized";
static const char* MUTEX_DEADLOCK = "A deadlock would occur if the thread blocked waiting for mutex";
static const char* MUTEX_LACKS_RESOURCES = "The system temporarily lacks the resources to create another mutex";
static const char* MUTEX_THREAD_ISNT_OWNER = "The current thread does not hold a lock on mutex";

invalid_threadop_exception::invalid_threadop_exception( const std::string& arg ) : std::runtime_error( arg )
{
}

invalid_threadop_exception::~invalid_threadop_exception( void ) throw()
{
}

Mutex::Mutex( void ) : initialized( false )
{
	switch( pthread_mutex_init( &semaphore, NULL ) )
	{
		case EAGAIN:
			throw invalid_threadop_exception( MUTEX_LACKS_RESOURCES );
			break;
		case EINVAL:
			throw std::runtime_error( MUTEX_UNEXPECTED );
			break;
		case ENOMEM:
			throw std::bad_alloc();
			break;
	}

	initialized = true;
}

Mutex::~Mutex( void )
{
	if( initialized )
	{
		lock();
		pthread_mutex_destroy( &semaphore );
		initialized = false;
	}
}

void Mutex::lock( void )
{
	if( !initialized )
		throw invalid_threadop_exception( MUTEX_NOT_INITIALIZED );

	switch( pthread_mutex_lock( &semaphore ) )
	{
		case EDEADLK:
			throw invalid_threadop_exception( MUTEX_DEADLOCK );
			break;
		case EINVAL:
			break;
		case 0:
			break;
	}
}

void Mutex::unlock( void )
{
	if( !initialized )
		throw invalid_threadop_exception( MUTEX_NOT_INITIALIZED );

	switch( pthread_mutex_unlock( &semaphore ) )
	{
		case EPERM:
			throw invalid_threadop_exception( MUTEX_THREAD_ISNT_OWNER );
			break;

		// Nunca deveria acontecer, pois garantimos que semaphore está válido no início do método
		case EINVAL:
			break;

		// Ok
		case 0:
			break;
	}
}

bool Mutex::tryLock( void )
{
	if( !initialized )
		throw invalid_threadop_exception( MUTEX_NOT_INITIALIZED );

	switch( pthread_mutex_trylock( &semaphore ) )
	{
		case EINVAL:
			break;
		case 0:
			return true;
		case EBUSY:
			return false;
	}
	return false;
}

}
