/*
 *  ThreadPool.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
//#ifndef THREAD_POOL_H
//#define THREAD_POOL_H
//#include <stdint.h>
//namespace DB
//{
//	class ThreadPool
//	{
//		public:
//			explicit ThreadPool( uint8_t maxThreads );
//			~ThreadPool( void );
//			uint8_t maxThreadsAllowed( void ) const { return maxThreads; };
//			void run( const ThreadJob& job );
//			void sync( ThreadJob* job );
//			void syncAll( void );
//			TPoolThr* getIdle( void );
//			void appendIdle( TPoolThr* t );
//
//		private:
//			uint8_t maxThreads;
//			std::vector< PoolThread* > threads;
//			TSLL< TPoolThr * > _idle_threads;
//			TMutex _idle_mutex, _list_mutex;
//			TCondition _idle_cond;
//	};
//
//}
//
//#endif
//
