/*
 *  ThreadJob.h
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */

#ifndef THREAD_JOB_H
#define THREAD_JOB_H

class ThreadJob
{
	public:
		typedef void*( *ThreadJobFuncType )( void* );
		ThreadJob( ThreadJobFuncType f, void *pParam );
		ThreadJobFuncType func;
		void *pFuncParam;
};

#endif
