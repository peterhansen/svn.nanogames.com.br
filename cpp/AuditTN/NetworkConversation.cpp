/*
 *
 *  Copyright 2010 Workers Informatica Ltda. All rights reserved.
 *
 */
#include "NetworkConversation.h"

namespace DB
{

NetworkConversation::NetworkConversation( const Ipv4Address& src, const Ipv4Address& dst )
					: ipClient( src ), ipHost( dst ), framesFromClientToHost( 0 ), bytesFromClientToHost( 0 ),
					  framesFromHostToClient( 0 ), bytesFromHostToClient( 0 ), packets(), convCapFile(),
					  terminalType(), terminalName()
{
}

NetworkConversation::NetworkConversation( const Ipv4Address& src, const Ipv4Address& dst, uint64_t nFramesFromClientToHost,
										  uint64_t nBytesFromClientToHost, uint64_t nFramesFromHostToClient, uint64_t nBytesFromHostToClient, const std::string& capFileName )
					: ipClient( src ), ipHost( dst ), framesFromClientToHost( nFramesFromClientToHost ), bytesFromClientToHost( nBytesFromClientToHost ),
					  framesFromHostToClient( nFramesFromHostToClient ), bytesFromHostToClient( nBytesFromHostToClient ), packets(), convCapFile( capFileName ),
					  terminalType(), terminalName()
{
}

bool NetworkConversation::operator==( const NetworkConversation& rho ) const
{
	return (( ipClient == rho.ipClient ) && ( ipHost == rho.ipHost )) || (( ipClient == rho.ipHost ) && ( ipHost == rho.ipClient ));
}

}
