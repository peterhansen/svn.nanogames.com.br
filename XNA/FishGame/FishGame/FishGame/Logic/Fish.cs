﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FishGame.Constants;

namespace FishGame.Logic {
    enum FishState { Idle, Follow }

    public class Fish {
        #region Static Fields
        private static Random rand = new Random();
        private static Vector2 textureOrigin = new Vector2();
        private static Texture2D texture;
        private static Viewport viewport;
        #endregion


        #region Fields
            private FishState state = FishState.Idle;
            private SpriteEffects sFx = SpriteEffects.FlipHorizontally;
            private Vector2 position = new Vector2();
            private Vector2 speed = new Vector2();
            private Vector2 realSize = new Vector2();
            private Fish follower;
            private Fish follow;
            private float speedModifier;
            private float seed;
            private float scale;
            private int id;
        #endregion


        #region Properties
            protected float Speed { get { return speedModifier; } set { speedModifier = value; if( speedModifier < 0 ) sFx = SpriteEffects.None; else if( speedModifier > 0 ) sFx = SpriteEffects.FlipHorizontally; } }
            public Vector2 Position { get { return position; } }
            public bool IsIdle { get { return state == FishState.Idle; } }
        #endregion


        #region Static Methods
            public static void setTexture( Texture2D text ) { texture = text; textureOrigin.X = text.Width / 2; textureOrigin.Y = text.Height / 2; }
            public static void setViewport( Viewport view ) { viewport = view; }
        #endregion


        #region Initialization
            public Fish( int id, float totalFishes, bool isPlayer = false ) {
                this.id = id;
                speedModifier = Limits.MIN_FISH_SPEED + (float)( rand.NextDouble() * Limits.FISH_SPEED_RANGE );
                scale = Limits.MIN_FISH_SIZE + (float)( ( id / totalFishes ) * Limits.FISH_SIZE_RANGE );
                realSize.X = ( texture.Width * scale );
                realSize.Y = ( texture.Height * scale );
                ResetPosition();
                seed = (float)(rand.NextDouble() * Math.PI);
                if( isPlayer ) { follow = this; }
            }
        #endregion


            public void setFollow( Fish fish ) { if( fish.follow != null ) { state = FishState.Follow; follow = fish; speedModifier = follow.Speed; if( follower != null ) follower.setFollow( this ); } }
        public void setFollower( Fish fish ) { follower = fish; }


        public void ResetPosition() {
            position.X = ( realSize.X / 2 ) + (float)( ( viewport.Width - realSize.X ) * rand.NextDouble() );
            position.Y = ( realSize.Y / 2 ) + (float)( ( viewport.Height - realSize.Y ) * rand.NextDouble() );
        }


        public void Update( float delta, bool fallForce ) {
            speed.Y += delta * Constants.Limits.GRAVITY;
            speed.Y = MathHelper.Min( speed.Y, ( fallForce ? Constants.Limits.MAX_FALL_SPEED * 3 : Constants.Limits.MAX_FALL_SPEED ) );
            position += delta * speed;
            position.X = MathHelper.Clamp( position.X, realSize.X / 2, viewport.Width - realSize.X / 2 );
            if( position.Y > viewport.Height - realSize.Y / 2 ) {
                position.Y = viewport.Height - realSize.Y / 2;
                speed.Y = 0;
            } else if( position.Y < realSize.Y / 2 ) {
                position.Y = realSize.Y / 2;
                speed.Y = 0;
            }
        }


        public void setSpeedX( float speedX ) {
            speed.X = speedX;
            if( speed.X < 0 ) sFx = SpriteEffects.None;
            else if( speed.X > 0) sFx = SpriteEffects.FlipHorizontally;
        }
        public void Pull() { speed.Y = -Constants.Limits.PULL_SPEED; }


        public void Think( float delta ) {
            switch( state ) {
                case FishState.Idle:
                    position.X += ( delta * speedModifier );
                    if( position.X > ( viewport.Width + realSize.X / 2 ) ) position.X = -realSize.X / 2;
                    if( position.X < ( -realSize.X / 2 ) ) position.X = viewport.Width + realSize.X / 2;
                break;

                case FishState.Follow:
                    if( Vector2.Distance( follow.Position, position ) > Constants.Limits.MIN_FOLOW_DISTANCE * scale ) position += ( delta * speedModifier * ( Vector2.Normalize( follow.Position - position ) ) );
                    if( follow.position.X < position.X ) sFx = SpriteEffects.None;
                    else if( follow.position.X > position.X ) sFx = SpriteEffects.FlipHorizontally;
                    if( position.X > ( viewport.Width + realSize.X / 2 ) ) position.X = -realSize.X / 2;
                    if( position.X < ( -realSize.X / 2 ) ) position.X = viewport.Width + realSize.X / 2;
                break;
            }
        }


        public void Draw( SpriteBatch sprite ) {
            sprite.Draw( texture, position, null, Color.White, 0, textureOrigin, scale, sFx, 0 );
        }
    }
}
