using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FishGame.Logic;
using Components.Input;

namespace FishGame {
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<Fish> fishes;
        Fish myFish;
        Texture2D waterfallTexture;
        Effect refractionEffect;
        Rectangle bkgArea = new Rectangle();
        Texture2D bkg;
        SoundEffect sndBluk;


        private const int BKG_NO = 2;


        public Game1() {
            graphics = new GraphicsDeviceManager( this );
            Content.RootDirectory = "Content";

            //graphics.PreferredBackBufferWidth = 1440;
            //graphics.PreferredBackBufferHeight = 900;
            //graphics.PreferredBackBufferWidth = 1920;
            //graphics.PreferredBackBufferHeight = 1080;
            ////graphics.PreferredBackBufferWidth = graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width;
            ////graphics.PreferredBackBufferHeight = graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height;
            //graphics.IsFullScreen = true;

            fishes = new List<Fish>();
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            DisplayMode displayMode = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode;
            graphics.PreferredBackBufferWidth = displayMode.Width;
            graphics.PreferredBackBufferHeight = displayMode.Height;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            base.Initialize();
        }


        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch( GraphicsDevice );

            refractionEffect = Content.Load<Effect>( Constants.Paths.EFFECT + "refraction" );

            Fish.setTexture( Content.Load<Texture2D>( Constants.Paths.VISUAL + "sushie" ) );
            Fish.setViewport( GraphicsDevice.Viewport );

            waterfallTexture = Content.Load<Texture2D>( Constants.Paths.VISUAL + "waterfall" );
            bkg = Content.Load<Texture2D>( Constants.Paths.VISUAL + "bkg" + BKG_NO );

            bkgArea.Width = Math.Max( bkg.Width, GraphicsDevice.Viewport.Width );
            bkgArea.Height = Math.Max( bkg.Height, GraphicsDevice.Viewport.Height );

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play( Content.Load<Song>( Constants.Paths.AUDIO + "preview") );

            sndBluk = Content.Load<SoundEffect>( Constants.Paths.AUDIO + "bluk" );

            myFish = new Fish( 0, Constants.Total.FISHES, true );
            for( int i = 1; i < Constants.Total.FISHES; i++ ) fishes.Add( new Fish( i, Constants.Total.FISHES ) );
        }


        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent() {
        }


        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update( GameTime gameTime ) {
            InputReader.Update();

            float delta = (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            myFish.Update( delta, InputReader.isPressed( Keys.Down ) || InputReader.isPressed( Buttons.LeftThumbstickDown ) || InputReader.isPressed( Buttons.DPadDown ) );
            foreach( Fish f in fishes ) f.Think( (float)gameTime.ElapsedGameTime.TotalSeconds );

            if( fishes[ 0 ].IsIdle && Vector2.Distance( fishes[ 0 ].Position, myFish.Position ) < Constants.Limits.MAX_FOLOW_DISTANCE ) {
                fishes[ 0 ].setFollow( myFish );
                myFish.setFollower( fishes[ 0 ] );
            }

            for( int i = 1; i < fishes.Count; i++ ) {
                if( fishes[ i ].IsIdle && Vector2.Distance( fishes[ i ].Position, fishes[ i - 1 ].Position ) < Constants.Limits.MAX_FOLOW_DISTANCE ) {
                    fishes[ i ].setFollow( fishes[ i - 1 ] );
                    fishes[ i - 1 ].setFollower( fishes[ i ] );
                }
            }

            if( InputReader.isNewPressed( Keys.Up ) || InputReader.isNewPressed( Buttons.LeftThumbstickUp ) || InputReader.isNewPressed( Buttons.DPadUp ) ) {
                myFish.Pull();
                sndBluk.Play();
            }
            
            bool right = ( InputReader.isPressed( Keys.Right ) || InputReader.isPressed( Buttons.LeftThumbstickRight ) || InputReader.isPressed( Buttons.DPadRight ) );
            bool left = ( InputReader.isPressed( Keys.Left ) || InputReader.isPressed( Buttons.LeftThumbstickLeft ) || InputReader.isPressed( Buttons.DPadLeft ) );

            if( right ^ left ) {
                if( right ) myFish.setSpeedX( Constants.Limits.SWIM_SPEED );
                if( left ) myFish.setSpeedX( -Constants.Limits.SWIM_SPEED );
            } else {
                myFish.setSpeedX( 0 );
            }

            base.Update( gameTime );
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw( GameTime gameTime ) {
            GraphicsDevice.Clear( Color.SkyBlue );

            //spriteBatch.Begin();
            // Set an effect parameter to make the
            // displacement texture scroll in a giant circle.
            refractionEffect.Parameters[ "DisplacementScroll" ].SetValue( MoveInCircle( gameTime, 0.2f ) );

            // Set the displacement texture.
            graphics.GraphicsDevice.Textures[ 1 ] = waterfallTexture;

            // Begin the sprite batch.
            spriteBatch.Begin( 0, null, null, null, null, refractionEffect );

            spriteBatch.Draw( bkg, bkgArea, Color.White );
            spriteBatch.End();

            spriteBatch.Begin();
            myFish.Draw( spriteBatch );
            foreach( Fish f in fishes ) f.Draw( spriteBatch );
            spriteBatch.End();

            base.Draw( gameTime );
        }


        /// <summary>
        /// Helper for moving a value around in a circle.
        /// </summary>
        static Vector2 MoveInCircle( GameTime gameTime, float speed ) {
            double time = gameTime.TotalGameTime.TotalSeconds * speed;

            float x = (float)Math.Cos( time );
            float y = (float)Math.Sin( time );

            return new Vector2( x, y );
        }
    }
}
