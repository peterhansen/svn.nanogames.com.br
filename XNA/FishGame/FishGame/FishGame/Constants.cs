﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FishGame.Constants {
    public static class Paths {
        public static string VISUAL = @"visual/";
        public static string AUDIO = @"audio/";
        public static string EFFECT = @"fx/";
    }


    public static class Total {
        public static int FISHES = 10;
    }


    public static class Limits {
        public static float GRAVITY = 98 / 100000f;

        public static float PULL_SPEED = 500 / 1000f;
        public static float SWIM_SPEED = 250 / 1000f;

        public static float MAX_FALL_SPEED = 100 / 1000f;

        public static float MIN_FISH_SPEED = 200;
        public static float MAX_FISH_SPEED = 200;
        public static float FISH_SPEED_RANGE = MAX_FISH_SPEED - MIN_FISH_SPEED;

        public static float MIN_FISH_SIZE = .3f;
        public static float MAX_FISH_SIZE = 1;
        public static float FISH_SIZE_RANGE = MAX_FISH_SIZE - MIN_FISH_SIZE;

        public static float MAX_FOLOW_DISTANCE = 200;
        public static float MIN_FOLOW_DISTANCE = 120;
    }
}
