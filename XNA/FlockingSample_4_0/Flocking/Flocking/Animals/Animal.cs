#region File Description
//-----------------------------------------------------------------------------
// Animal.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
#endregion

namespace Flocking {
    /// <summary> { ( no type ) , ( flies around and reacts ), ( controled by teh thumbstick, birds flee from it ) } </summary>
    public enum AnimalType { Generic, Bird, Cat }

    /// <summary> base class for moveable, drawable critters onscreen </summary>
    public abstract class Animal {
        protected static Texture2D texture; //Texture drawn to represent this animal
        protected static Vector2 textureCenter; //Center of the draw texture

        #region Fields
            protected Color color = Color.White; //Tint color to draw the texture with
            protected float moveSpeed; //Movement speed in pixels/second
            protected Dictionary<AnimalType, Behaviors> behaviors; //All the behavior that this animal has
            protected AnimalType animaltype = AnimalType.Generic;
            protected float reactionDistance;
            protected Vector2 reactionLocation;
            protected bool fleeing = false;
            protected int boundryWidth;
            protected int boundryHeight;
            protected Vector2 direction;
            protected Vector2 location;
        #endregion


        #region Properties
            public AnimalType AnimalType { get {  return animaltype; } }
            public float ReactionDistance { get { return reactionDistance; } }
            public Vector2 ReactionLocation { get { return reactionLocation; } }
            public bool Fleeing { get { return fleeing; } set { fleeing = value; } }
            public int BoundryWidth { get { return boundryWidth; } }
            public int BoundryHeight { get { return boundryHeight; } }
            public Vector2 Direction { get { return direction; } }
            public Vector2 Location { get { return location; } set { location = value; } } // Location on screen
        #endregion


        #region Initialization
            public static void setTexture( Texture2D tex ) {
                if( tex != null ) {
                    texture = tex;
                    textureCenter = new Vector2( texture.Width / 2, texture.Height / 2 );
                }
            }

            /// <summary> Sets the boundries the animal can move in the texture used in Draw </summary>
            /// <param name="tex"> Texture to use </param> <param name="screenSize"> Size of the sample screen </param>
            public Animal( int screenWidth, int screenHeight ) {
                boundryWidth = screenWidth;
                boundryHeight = screenHeight;
                moveSpeed = 0.0f;

                behaviors = new Dictionary<AnimalType, Behaviors>();
            }
        #endregion


        #region Update and Draw
            /// <summary> Empty update function </summary>
            /// <param name="gameTime"></param>
            public virtual void Update( GameTime gameTime ) {
            }


            /// <summary> Draw the Animal with the specified SpriteBatch </summary> 
            /// <param name="spriteBatch"></param> <param name="gameTime"></param>
            public virtual void Draw( SpriteBatch spriteBatch, GameTime gameTime ) {
                float rotation = (float)Math.Atan2( direction.Y, direction.X );
                spriteBatch.Draw( texture, location, null, color, rotation, textureCenter, 1.0f, SpriteEffects.None, 0.0f );
            }
        #endregion
    }
}