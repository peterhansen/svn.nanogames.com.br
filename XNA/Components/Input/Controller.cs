﻿#region File Description
//-----------------------------------------------------------------------------
// Author: Caio Cesa Lima
// This class Controller is original from Input from K10Engine
//-----------------------------------------------------------------------------
#endregion

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Components.Input {
    public class Controller {
        #region Fields
        GamePadState lastGamePadState;
        GamePadState newGamePadState;
        bool hasProfile;
        #endregion //end of Fields region

        #region Properties
        public bool HasProfile { get { return hasProfile; } }
        public Vector2 RightStick { get { return new Vector2( newGamePadState.ThumbSticks.Right.X, newGamePadState.ThumbSticks.Right.Y ); } }
        public Vector2 LeftStick { get { return new Vector2( newGamePadState.ThumbSticks.Left.X, newGamePadState.ThumbSticks.Left.Y ); } }
        public float LeftTrigger { get { return newGamePadState.Triggers.Left; } }
        public float RightTrigger { get { return newGamePadState.Triggers.Right; } }
        public bool ButtonIsPressed( Buttons button ) { return ( newGamePadState.IsButtonDown( button ) ); }
        public bool ButtonIsNewPress( Buttons button ) { return ( newGamePadState.IsButtonDown( button ) && lastGamePadState.IsButtonUp( button ) ); }
        public bool ButtonIsReleased( Buttons button ) { return ( lastGamePadState.IsButtonDown( button ) && newGamePadState.IsButtonUp( button ) ); }
        #endregion //end of Properties region

        #region Initialization
        public Controller() {
            lastGamePadState = new GamePadState();
            newGamePadState = new GamePadState();
            hasProfile = false;
        }
        #endregion //end of Initialization region

        #region Public Methods
        public bool Link() { hasProfile = true; return hasProfile; }
        public void Update( GamePadState gamePadState ) {
            lastGamePadState = newGamePadState;
            newGamePadState = gamePadState;
        }
        #endregion //end of Public Methods region
    }
}
