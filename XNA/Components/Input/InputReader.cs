﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace Components.Input {
    public class InputReader {
        private static Controller[] controller = new Controller[] { new Controller(), new Controller(), new Controller(), new Controller() };
        public static Controller[] Controllers { get { return controller; } }
        public static Controller Controller( PlayerIndex playerIndex ) {
            switch( playerIndex ) {
                case PlayerIndex.One: return controller[ 0 ];
                case PlayerIndex.Two: return controller[ 1 ];
                case PlayerIndex.Three: return controller[ 2 ];
                case PlayerIndex.Four: return controller[ 3 ];
                default: return controller[ 0 ];
            }
        }

        private static Vector2 cursor;
        private static bool cursorMoved;

        public static Vector2 LeftThumbs { get { return controller[ 0 ].LeftStick + controller[ 1 ].LeftStick + controller[ 2 ].LeftStick + controller[ 3 ].LeftStick; } }
        public static Vector2 RightThumbs { get { return controller[ 0 ].RightStick + controller[ 1 ].RightStick + controller[ 2 ].RightStick + controller[ 3 ].RightStick; } }
        public static float LeftTriggers { get { return controller[ 0 ].LeftTrigger + controller[ 1 ].LeftTrigger + controller[ 2 ].LeftTrigger + controller[ 3 ].LeftTrigger; } }
        public static float RightTriggers { get { return controller[ 0 ].RightTrigger + controller[ 1 ].RightTrigger + controller[ 2 ].RightTrigger + controller[ 3 ].RightTrigger; } }

        public static Vector2 Cursor { get { return cursor; } }

        private static KeyboardState lastKeyboardState = new KeyboardState();
        private static KeyboardState newKeyboardState = new KeyboardState();
        private static MouseState lastMouseState = new MouseState();
        private static MouseState newMouseState = new MouseState();

        public static bool isPressed( Keys key ) { return newKeyboardState.IsKeyDown( key ); }
        public static bool isReleased( Keys key ) { return newKeyboardState.IsKeyUp( key ); }
        public static bool isNewPressed( Keys key ) { return newKeyboardState.IsKeyDown( key ) && lastKeyboardState.IsKeyUp( key ); }
        public static bool isReleasedNow( Keys key ) { return newKeyboardState.IsKeyUp( key ) && lastKeyboardState.IsKeyDown( key ); }

        public static bool isPressed( Buttons button ) { return controller[ 0 ].ButtonIsPressed( button ) || controller[ 1 ].ButtonIsPressed( button ) || controller[ 2 ].ButtonIsPressed( button ) || controller[ 3 ].ButtonIsPressed( button ); }
        public static bool isReleased( Buttons button ) { return !controller[ 0 ].ButtonIsPressed( button ) || !controller[ 1 ].ButtonIsPressed( button ) || !controller[ 2 ].ButtonIsPressed( button ) || !controller[ 3 ].ButtonIsPressed( button ); }
        public static bool isNewPressed( Buttons button ) { return controller[ 0 ].ButtonIsNewPress( button ) || controller[ 1 ].ButtonIsNewPress( button ) || controller[ 2 ].ButtonIsNewPress( button ) || controller[ 3 ].ButtonIsNewPress( button ); }
        public static bool isReleasedNow( Buttons button ) { return controller[ 0 ].ButtonIsReleased( button ) || controller[ 1 ].ButtonIsReleased( button ) || controller[ 2 ].ButtonIsReleased( button ) || controller[ 3 ].ButtonIsReleased( button ); }


        private InputReader() { }


        public static void Update() {
            controller[ 0 ].Update( GamePad.GetState( PlayerIndex.One ) );
            controller[ 1 ].Update( GamePad.GetState( PlayerIndex.Two ) );
            controller[ 2 ].Update( GamePad.GetState( PlayerIndex.Three ) );
            controller[ 3 ].Update( GamePad.GetState( PlayerIndex.Four ) );

            lastKeyboardState = newKeyboardState;
            newKeyboardState = Keyboard.GetState();
            lastMouseState = newMouseState;
            newMouseState = Mouse.GetState();
        }
    }
}
